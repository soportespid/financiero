<?php
    header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");

	require 'comun.inc';
	require 'funciones.inc';

	session_start();
    date_default_timezone_set("America/Bogota");

?>

<html lang="en">
    <head>
        <meta charset="utf-8"/>
        <meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1"/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>:: IDEAL 10 - Presupuesto</title>
        <link href="favicon.ico" rel="shortcut icon"/>

        <link href="css/css2.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
        <link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
        <link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
        <script type="text/javascript" src="css/programas.js"></script>
        <script type="text/javascript" src="css/calendario.js"></script>
        <script type="text/javascript" src="JQuery/jquery-2.1.4.min.js"></script>

        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">

        <style>
            [v-cloak]{
                display : none;
            }

            label{
                font-size:13px;
            }

            input{
                height: calc(1em + 0.6rem + 0.5px) !important; 
                font-size: 14px !important; 
                /* margin-top: 4px !important; */
            }
        </style>

    </head>
    <body>
        <IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
        <span id="todastablas2"></span>

        <header>
            <table>
                <tr><script>barra_imagenes("ccpet");</script><?php cuadro_titulos();?></tr>
            </table>
        </header>
        <form name="form2" method="post" action="">
            <section id="myapp" v-cloak>
                <nav>
                    <table>
                        <tr><?php menu_desplegable("ccpet");?></tr>
                        <tr>
                            <td colspan="3" class="cinta">
                                <img src="imagenes/add2.png" v-on:Click="location.href='ccp-auxiliarporcomprobante.php'" class="mgbt" title="Nuevo" >
                                <img src="imagenes/guardad.png" title="Guardar" class="mgbt1">
                                <img src="imagenes/buscad.png" class="mgbt" title="Buscar" v-on:Click="location.href='ccp-auxiliarporcomprobante.php'">
                                <a onClick="mypop=window.open('ccp-principal.php','','');mypop.focus();" class="mgbt"><img src="imagenes/nv.png" title="Nueva ventana"></a>
                                <img src="imagenes/excel.png" title="Excel" class="mgbt" v-on:click="excel">
                            </td>
                        </tr>
                    </table>
                </nav>
                <article>
                    <table class="inicio ancho">
                        <tr>
                            <td class="titulos" colspan="9" >Auxiliar por comprobante {{ tipo_comp }}</td>
                            <td class="cerrar" style="width:7%" onClick="location.href='ccp-principal.php'">Cerrar</td>
                        </tr>
                        <tr>
                            <td class = "textoNewR" style = "width: 8%;">
                                <label class="labelR">
                                    Fecha inicial:
                                </label>
                            </td>
                            <td style = "width: 10%;">
                                <input type="text" style = "width: 90%;" v-model="fechaIni" name="fechaIni" readonly>
                                <input type="hidden" name="tipo_comp" v-model="tipo_comp">
                            </td>
                            <td class = "textoNewR" style = "width: 8%;">
                                <label class="labelR">
                                    Fecha final:
                                </label>
                            </td>
                            <td style = "width: 10%;">
                                <input type="text" style = "width: 90%;" v-model="fechaFin" name="fechaFin" readonly>
                            </td>
                            <td class = "textoNewR" style = "width: 11%;">
                                <label class="labelR">
                                    Sec. Presupuestal:
                                </label>
                            </td>
                            <td>
                                <input type="text" style = "width: 90%;" v-model="seccion_presupuestal" readonly>
                            </td>
                            <td class = "textoNewR" style = "width: 8%;">
                                <label class="labelR">
                                    Medio pago:
                                </label>
                            </td>
                            <td style = "width: 10%;">
                                <input type="text" style = "width: 90%;" v-model="medio_pago" readonly>
                            </td>
                            <td class = "textoNewR" style = "width: 10%;">
                                <label class="labelR">
                                    Vigencia gasto:
                                </label>
                            </td>
                            <td style = "width: 10%;">
                                <input type="text" style = "width: 90%;" v-model="vigencia_gasto" readonly>
                            </td>
                        </tr>
                        <tr>
                            <td class = "textoNewR">
                                <label class="labelR">
                                    Fuente:
                                </label>
                            </td>
                            <td>
                                <input type="text" style = "width: 90%;" v-model="fuente_f" readonly>
                            </td>
                            <td class = "textoNewR">
                                <label class="labelR">
                                    Cuenta:
                                </label>
                            </td>
                            <td>
                                <input type="text" style = "width: 90%;" v-model="cuenta_f" readonly>
                            </td>
                            <td class = "textoNewR">
                                <label class="labelR">
                                    BPIM:
                                </label>
                            </td>
                            <td colspan="2">
                                <input type="text" style = "width: 94%;" v-model="bpim_inv" readonly>
                            </td>
                            <td class = "textoNewR">
                                <label class="labelR">
                                    Program&aacute;tico:
                                </label>
                            </td>
                            <td colspan="2">
                                <input type="text" style = "width: 95%;" v-model="programatico_inv" readonly>
                            </td>
                        </tr>
                    </table>
                    <div class='subpantalla' style='height:60vh; margin-top:0px; overflow:hidden'>
                        <table class='tablamvR' id="tableId">
                            <thead>
                                <tr style="text-align:Center;">
                                    <th class="titulosnew00" style="width:5%;">Num. Comprobante</th>
                                    <th class="titulosnew00" style="width:5%;">Fecha</th>
                                    <th class="titulosnew00" style="width:15%;">Objeto</th>
                                    <th class="titulosnew00" style="width:5%;">Tipo mov</th>
                                    <th class="titulosnew00" style="width:6%;">Cuenta CCPET</th>
                                    <th class="titulosnew00" style="width:5%;">CPC</th>
                                    <th class="titulosnew00" style="width:5%;">Valor</th>
                                    <th class="titulosnew00" style="width:5%;">Valor reversado</th>
                                    <th class="titulosnew00" style="width:5%;">CDP</th>
                                    <th class="titulosnew00" style="width:5%;">RP</th>
                                    <th class="titulosnew00" style="width:5%;">CXP</th>
                                    <th class="titulosnew00" style="width:5%;">Egreso</th>
                                    <th class="titulosnew00" style="width:0.6%;"></th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr v-for="(row,index) in detalles" :key="index" v-bind:class="index % 2 ? 'contenidonew00' : 'contenidonew01'">
                                    <td style="width:5%; text-align:center;">{{ row[0] }}</td>
                                    <td style="width:5%;">{{ row[1] }}</td>
                                    <td style="width:15%;">{{ row[2] }}</td>
                                    <td style="width:5%; text-align:center;">{{ row[3] }}</td>
                                    <td style="width:6%;">{{ row[4] }}</td>
                                    <td style="width:5%;">{{ row[5] }}</td>
                                    <td style="width:5%; text-align:right;">{{ formatonumero(row[6]) }}</td>
                                    <td style="width:5%; text-align:right;">{{ formatonumero(row[7]) }}</td>
                                    <td style="width:5%; text-align:center;">
                                        <span v-for="(cdp, i) in row[8]"><a class="clickComp" @click = "redirectCdp(cdp)">{{ cdp }}</a> <strong v-if = "i != row[8].length-1"> - </strong></span>
                                    </td>
                                    <td style="width:5%; text-align:center;">
                                        <span v-for="(rp, i) in row[9]"><a class="clickComp" @click = "redirectRp(rp)">{{ rp }}</a> <strong v-if = "i != row[9].length-1"> - </strong></span>
                                    </td>
                                    <td v-if = "row[10].length > 0" style="width:5%; text-align:center;">
                                        <span v-for="(cxp, i) in row[10]"><a class="clickComp" @click = "redirectCxp(cxp)">{{ cxp }}</a> <strong v-if = "i != row[10].length-1"> - </strong></span>
                                    </td>
                                    <td v-else = "row[11].length > 0" style="width:5%; text-align:center;">
                                        <span v-for="(cxpN, i) in row[11]"><a class="clickComp" @click = "redirectCxpN(cxpN)">{{ cxpN }}</a> <strong v-if = "i != row[11].length-1"> - </strong></span>
                                    </td>
                                    <td v-if = "row[12].length > 0" style="width:5%; text-align:center;">
                                        <span v-for="(egreso, i) in row[12]"><a class="clickComp" @click = "redirectEgreso(egreso)">{{ egreso }}</a> <strong v-if = "i != row[12].length-1"> - </strong></span>
                                    </td>
                                    <td v-else = "row[13].length > 0" style="width:5%; text-align:center;">
                                        <span v-for="(egresoN, i) in row[13]"><a class="clickComp" @click = "redirectEgresoN(egresoN)">{{ egresoN }}</a> <strong v-if = "i != row[13].length-1"> - </strong></span>
                                    </td>

                                    <input type="hidden" name='numComprobante[]' v-model="row[0]">
                                    <input type="hidden" name='fecha[]' v-model="row[1]">
                                    <input type="hidden" name='objeto[]' v-model="row[2]">
                                    <input type="hidden" name='tipoMov[]' v-model="row[3]">
                                    <input type="hidden" name='cuenta[]' v-model="row[4]">
                                    <input type="hidden" name='cpc[]' v-model="row[5]">
                                    <input type="hidden" name='valor[]' v-model="row[6]">
                                    <input type="hidden" name='valorReversado[]' v-model="row[7]">
                                    <input type="hidden" name='cdp[]' v-model="row[8]">
                                    <input type="hidden" name='rp[]' v-model="row[9]">
                                    <input type="hidden" name='cxp[]' v-model="row[11]">
                                    <input type="hidden" name='egreso[]' v-model="row[13]">
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    
                    <div id="cargando" v-if="loading" class="loading">
                        <span>Cargando...</span>
                    </div>
                </article>
            </section>
        </form>
            
        <script type="module" src="./presupuesto_ccpet/AuxiliarGastos/ccp-auxiliarporcomprobante.js"></script>
        <script src="Librerias/vue/axios.min.js"></script>
        <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/vue/3.2.37/vue.global.min.js"></script> -->
        <!-- <script src="https://unpkg.com/vue@3"></script> -->
        <script src="https://unpkg.com/vue@3/dist/vue.global.js"></script>
    </body>
</html>

