<?php

    header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require 'comun.inc';
	require 'funciones.inc';
	session_start();
    if(empty($_SESSION)){
        header("location: index.php");
    }
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html lang=es>
	<head>
    <meta charset="utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1"/>
		<meta name="viewport" content="user-scalable=no">
		<title>:: IDEAL 10 - Tesorería</title>
		<link href="favicon.ico" rel="shortcut icon"/>
		<link href="css/css2z.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
        <link href="css/tabs.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
        <link href="css/style.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" href="sweetalert2/dist/sweetalert2.min.css">
        <script type="text/javascript" src="css/programas.js"></script>
        <script src="sweetalert2/dist/sweetalert2.min.js"></script>
		<script src="vue/vue.min.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script src="css/calendario.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
	</head>
	<body>
		<header>
			<table>
				<tr><script>barra_imagenes("teso");</script><?php cuadro_titulos();?></tr>
			</table>
		</header>

		<form name="form2" method="post" action="">
			<section id="myapp" v-cloak >
                <div class="loading-container" v-show="isLoading" >
                    <p class="text-loading"data-loading="IDEAL 10">IDEAL 10</p>
                </div>
				<nav>
					<table>
						<tr><?php menu_desplegable("teso");?></tr>
					</table>
                    <div class="bg-white group-btn p-1" id="newNavStyle">
                        <button type="button" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center" @click="save()">
                            <span>Guardar</span>
                            <svg xmlns="http://www.w3.org/2000/svg"  viewBox="0 -960 960 960" ><path d="M840-680v480q0 33-23.5 56.5T760-120H200q-33 0-56.5-23.5T120-200v-560q0-33 23.5-56.5T200-840h480l160 160Zm-80 34L646-760H200v560h560v-446ZM480-240q50 0 85-35t35-85q0-50-35-85t-85-35q-50 0-85 35t-35 85q0 50 35 85t85 35ZM240-560h360v-160H240v160Zm-40-86v446-560 114Z"/></svg>
                        </button>
                        <button type="button" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center" @click="window.location.href='teso-acuerdosPredialBuscar'">
                            <span>Buscar</span>
                            <svg class="fill-black group-hover:fill-white w-5 h-5" xmlns="http://www.w3.org/2000/svg"  viewBox="0 -960 960 960" ><path d="M784-120 532-372q-30 24-69 38t-83 14q-109 0-184.5-75.5T120-580q0-109 75.5-184.5T380-840q109 0 184.5 75.5T640-580q0 44-14 83t-38 69l252 252-56 56ZM380-400q75 0 127.5-52.5T560-580q0-75-52.5-127.5T380-760q-75 0-127.5 52.5T200-580q0 75 52.5 127.5T380-400Z"/></svg>
                        </button>
                        <button type="button" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center" @click="mypop=window.open('teso-principal.php','',''); mypop.focus();">
                            <span>Nueva ventana</span>
                            <svg class="fill-black group-hover:fill-white w-5 h-5" xmlns="http://www.w3.org/2000/svg"  viewBox="0 -960 960 960" ><path d="M320-120v-80h80v-80H160q-33 0-56.5-23.5T80-360v-400q0-33 23.5-56.5T160-840h640q33 0 56.5 23.5T880-760v400q0 33-23.5 56.5T800-280H560v80h80v80H320ZM160-360h640v-400H160v400Zm0 0v-400 400Z"/></svg>
                        </button>
                        <button type="button" class="btn btn-success d-flex justify-between align-items-center" @click="window.location.href='teso-acuerdosPredialBuscar'">
                            <span>Atrás</span>
                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960"><path d="M760-200v-160q0-50-35-85t-85-35H273l144 144-57 56-240-240 240-240 57 56-144 144h367q83 0 141.5 58.5T840-360v160h-80Z"/></svg>
                        </button>
                    </div>
				</nav>
				<article>
                    <!--TABS-->
                    <div ref="rTabs" class="nav-tabs bg-white p-1">
                        <div class="nav-item active" @click="showTab(1)">Acuerdo predial</div>
                        <div class="nav-item exclude btn btn-success"  @click="isModal=true" v-show="objData.codigo_catastro || objTotales.total_liquidacion > 0">Asignar cuotas</div>
                        <div class="nav-item exclude" @click="isModalCuenta=true">Estados de cuenta</div>
                    </div>
                     <!--CONTENIDO TABS-->
                     <div ref="rTabsContent" class="nav-tabs-content bg-white">
                        <div class="nav-content active">
                            <p class="ms-2 m-0">Para realizar un acuerdo predial, debe generar un estado de cuenta al día. Si no lo ha realizado, de click <a href="teso-ams-estadocuentas" class="fs-3 fw-normal text-primary">aquí</a></p>
                            <div class="d-flex">
                                <div class="form-control">
                                    <div class="d-flex">
                                        <input type="search" v-model="txtSearch"  class="w-50" placeholder="Buscar por código catastral, documento, nombre o dirección">
                                        <button type="button" @click="search()" class="btn btn-primary">Buscar</button>
                                    </div>
                                </div>
                            </div>
                            <div v-show="objData.codigo_catastro">
                                <h2 class="titulos m-0">Información predial</h2>
                                <div class="d-flex">
                                    <div class="form-control d-flex flex-row">
                                        <label class="form-label fw-bold">Código catastral:</label>
                                        <p class="m-0">{{objData.codigo_catastro ? objData.codigo_catastro : ""}}</p>
                                    </div>
                                    <div class="form-control">
                                        <label class="form-label fw-bold">Nro Documento:</label>
                                        <p class="m-0">{{objData.codigo_catastro ? objData.documento :""}}</p>
                                    </div>
                                    <div class="form-control">
                                        <label class="form-label fw-bold">Nombre propietario:</label>
                                        <p class="m-0">{{objData.codigo_catastro ? objData.nombre_propietario :""}}</p>
                                    </div>
                                    <div class="form-control">
                                        <label class="form-label fw-bold">Área del terreno:</label>
                                        <p class="m-0">{{objData.codigo_catastro ? objData.hectareas+" ha "+objData.metros_cuadrados+" m²" : ""}}</p>
                                    </div>
                                    <div class="form-control">
                                        <label class="form-label fw-bold">Área construida:</label>
                                        <p class="m-0">{{objData.codigo_catastro ? objData.area_construida+" m²" : ""}}</p>
                                    </div>
                                    <div class="form-control">
                                        <label class="form-label fw-bold">Dirección:</label>
                                        <p class="m-0">{{objData.codigo_catastro ? objData.direccion : ""}}</p>
                                    </div>
                                </div>
                                <div class="d-flex">
                                    <div class="form-control">
                                        <label class="form-label fw-bold">Total:</label>
                                        <p class="m-0">{{objData.codigo_catastro ? objData.total :""}}</p>
                                    </div>
                                    <div class="form-control">
                                        <label class="form-label fw-bold">Orden:</label>
                                        <p class="m-0">{{objData.codigo_catastro ? objData.orden : ""}}</p>
                                    </div>
                                    <div class="form-control">
                                        <label class="form-label fw-bold">Vigencia:</label>
                                        <p class="m-0">{{objData.codigo_catastro ? arrVigencias[0].vigencia : ""}}</p>
                                    </div>
                                    <div class="form-control">
                                        <label class="form-label fw-bold">Avaluo vigente:</label>
                                        <p class="m-0">{{objData.codigo_catastro ? formatNumero(objData.valor_avaluo) : ""}}</p>
                                    </div>
                                    <div class="form-control">
                                        <label class="form-label fw-bold">Tipo predio:</label>
                                        <p class="m-0">{{objData.codigo_catastro ? objData.predio_tipo+" ( Codigo: "+objData.predio_tipo_codigo+")" : ""}}</p>
                                    </div>
                                    <div class="form-control">
                                        <label class="form-label fw-bold">Destino económico:</label>
                                        <p class="m-0">{{objData.codigo_catastro ? objData.destino_economico+" ( Codigo: "+objData.codigo_destino_economico+")" : ""}}</p>
                                    </div>
                                </div>
                            </div>
                            <div v-show="objData.codigo_catastro">
                                <h2 class="titulos m-0">Periodos a liquidar:  </h2>
                                <div class="table-responsive" >
                                    <table  class="table fw-normal">
                                        <thead>
                                            <tr>
                                                <th class="d-flex justify-center">
                                                    <div class="d-flex align-items-center ">
                                                        <label for="labelCheckAll" class="form-switch">
                                                            <input type="checkbox" id="labelCheckAll" v-model="isCheckAll" checked @change="changeAll()">
                                                            <span></span>
                                                        </label>
                                                    </div>
                                                </th>
                                                <th>Vigencia</th>
                                                <th>Avaluo</th>
                                                <th>Tasa x mil</th>
                                                <th>Valor predial</th>
                                                <th>Descuento incentivo</th>
                                                <th>Recaudo predial</th>
                                                <th>Intereses predial</th>
                                                <th>Descuento intereses predial</th>
                                                <th>Bomberil</th>
                                                <th>Intereses bomberil</th>
                                                <th>Ambiental</th>
                                                <th>Intereses ambiental</th>
                                                <th>Alumbrado</th>
                                                <th>Liquidación</th>
                                                <th>Dias de mora</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr v-for="(data,index) in arrVigencias" :key="index">
                                                <td class="d-flex justify-center">
                                                    <div class="d-flex align-items-center ">
                                                        <label :for="'labelCheckName'+index" class="form-switch">
                                                            <input type="checkbox" :id="'labelCheckName'+index" @change="changeStatus(index)" :checked="data.is_checked">
                                                            <span></span>
                                                        </label>
                                                    </div>
                                                </td>
                                                <td>{{data.vigencia}}</td>
                                                <td>{{formatNumero(data.valor_avaluo)}}</td>
                                                <td>{{data.tasa_por_mil}}</td>
                                                <td>{{formatNumero(data.predial+data.predial_descuento)}}</td>
                                                <td>{{formatNumero(data.predial_descuento)}}</td>
                                                <td>{{formatNumero(data.predial)}}</td>
                                                <td>{{formatNumero(data.predial_intereses)}}</td>
                                                <td>{{formatNumero(data.predial_descuento_intereses)}}</td>
                                                <td>{{formatNumero(data.bomberil)}}</td>
                                                <td>{{formatNumero(data.bomberil_intereses)}}</td>
                                                <td>{{formatNumero(data.ambiental)}}</td>
                                                <td>{{formatNumero(data.ambiental_intereses)}}</td>
                                                <td>{{formatNumero(data.alumbrado)}}</td>
                                                <td>{{formatNumero(data.total_liquidacion)}}</td>
                                                <td>{{data.dias_mora}}</td>
                                            </tr>
                                            <tr class="bg-white fw-bold">
                                                <td colspan="2">Totales</td>
                                                <td>{{formatNumero(objTotales.total_avaluo)}}</td>
                                                <td></td>
                                                <td>{{formatNumero(objTotales.total_predial)}}</td>
                                                <td>{{formatNumero(objTotales.total_incentivo)}}</td>
                                                <td>{{formatNumero(objTotales.total_recaudo)}}</td>
                                                <td>{{formatNumero(objTotales.total_predial_intereses)}}</td>
                                                <td>{{formatNumero(objTotales.total_predial_descuento_intereses)}}</td>
                                                <td>{{formatNumero(objTotales.total_bomberil)}}</td>
                                                <td>{{formatNumero(objTotales.total_intereses_bomberil)}}</td>
                                                <td>{{formatNumero(objTotales.total_ambiental)}}</td>
                                                <td>{{formatNumero(objTotales.total_intereses_ambiental)}}</td>
                                                <td>{{formatNumero(objTotales.total_alumbrado)}}</td>
                                                <td>{{formatNumero(objTotales.total_liquidacion)}}</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- MODALES -->
                    <div v-show="isModal" class="modal">
                        <div class="modal-dialog modal-lg" >
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title">Cuotas</h5>
                                    <button type="button" @click="isModal=false;" class="btn btn-close"><div></div><div></div></button>
                                </div>
                                <div class="modal-body">
                                    <div class="d-flex">
                                        <div class="form-control">
                                            <label class="form-label">Total a liquidar:</label>
                                            <p class="m-0 fw-bold">{{formatNumero(objTotales.total_liquidacion)}}</p>
                                        </div>
                                        <div class="form-control">
                                            <label class="form-label">Nro de cuotas:</label>
                                            <div class="d-flex">
                                                <input type="number"  v-model="txtCuotas" class="text-center">
                                                <button type="button" class="btn btn-primary" @click="getCuotas" >Generar</button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="overflow-auto max-vh-50 overflow-x-hidden " >
                                        <table class="table fw-normal p-2">
                                            <thead>
                                                <tr>
                                                    <th class="text-center">Cuota</th>
                                                    <th>Fecha</th>
                                                    <th>Valor</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr v-for="(data,index) in arrCuotas" :key="index">
                                                    <td class="text-center">{{ index+1 }}</td>
                                                    <td><div class="d-flex"><div class="form-control"><input type="date" v-model="data.fecha"></div></div></td>
                                                    <td v-if="index < arrCuotas.length-1"><div class="d-flex"><div class="form-control"><input type="number" v-model="data.valor" @keyup="updateCuotas()"></div></div></td>
                                                    <td v-else><div class="d-flex"><div class="form-control"><input type="number" v-model="data.valor" disabled></div></div></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div v-show="isModalCuenta" class="modal">
                        <div class="modal-dialog modal-lg" >
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title">Estados de cuenta</h5>
                                    <button type="button" @click="isModalCuenta=false;" class="btn btn-close"><div></div><div></div></button>
                                </div>
                                <div class="modal-body">
                                    <div class="d-flex flex-column">
                                        <div class="form-control m-0 mb-3">
                                            <input type="search" v-model="txtSearch" @keyup="search('modal')" placeholder="Buscar por código catastral, documento, nombre o dirección">
                                        </div>
                                        <div class="form-control m-0 p-2">
                                            <label class="form-label" for="labelInputName">Resultados: <span class="fw-bold">{{txtResultados}}</span></label>
                                        </div>
                                    </div>
                                    <div class="overflow-auto max-vh-50 overflow-x-hidden" >
                                        <table class="table table-hover fw-normal">
                                            <thead>
                                                <tr>
                                                    <th class="text-center">Código catastral</th>
                                                    <th>Nombre</th>
                                                    <th>Documento</th>
                                                    <th>Dirección</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr v-for="(data,index) in arrDataCopy" :key="index" @dblclick="selectItem(data)">
                                                    <td>{{ data.codigo_catastro}}</td>
                                                    <td>{{ data.nombre_propietario}}</td>
                                                    <td>{{ data.documento}}</td>
                                                    <td>{{ data.direccion}}</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
				</article>
			</section>
		</form>

		<script src="node_modules/read-excel-file/bundle/read-excel-file.min.js"></script>
		<script src="node_modules/xlsx/dist/xlsx.full.min.js"></script>
        <script src="Librerias/vue/vue.min.js"></script>
		<script src="Librerias/vue/axios.min.js"></script>
		<script type="module" src="tesoreria/predial_acuerdos/crear/teso-acuerdosPredialCrear.js?<?= date('d_m_Y_h_i_s');?>"></script>

	</body>
</html>
