<?php
require"comun.inc";
require"funciones.inc";
require"serviciospublicos.inc";
session_start();
$linkbd=conectar_bd();
?>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html" />
<meta http-equiv="X-UA-Compatible" content="IE=9" />
<title>:: SPID - Servicios Publicos</title>
<script>
//************* ver reporte ************
function verep(idfac)
{document.form1.oculto.value=idfac;document.form1.submit();}
//************* genera reporte ************
function genrep(idfac)
{document.form2.oculto.value=idfac;document.form2.submit();}

function buscacta(e)
 {
	if (document.form2.cuenta.value!="")
	{document.form2.bc.value='1';document.form2.submit();}
 }

function validar()
{	
	if (isNaN(form2.mes.value))
		{alert ("Solo se pueden digitar numeros");
			form2.numero.focus();}
	if (form2.vigencias.value=="")
		{alert ("Por favor digite un Ciclo");
			form2.numero.focus();}						
	if(form2.vigencias.value!="")
		{document.form2.oculto.value='1'
		document.form2.submit();}
}

function buscaciclo(e)
 {
	if (document.form2.numero.value!="")
	{document.form2.bt.value='1';
		document.form2.submit();}
 }

function agregardetalle()
{
if(document.form2.numero.value!="" &&  document.form2.valor.value>0 &&  document.form2.banco.value!=""  )
{ 
				document.form2.agregadet.value=1;
	//			document.form2.chacuerdo.value=2;
				document.form2.submit();
 }
 else {
 alert("Falta informacion para poder Agregar");
 }
}

function eliminar(variable)
{
if (confirm("Esta Seguro de Eliminar"))
  {
document.form2.elimina.value=variable;
//eli=document.getElementById(elimina);
vvend=document.getElementById('elimina');
//eli.value=elimina;
vvend.value=variable;
document.form2.submit();
}
}
//************* genera reporte ************
function guardar()
{

if (document.form2.fecha.value!='')
  {
	if (confirm("Esta Seguro de Guardar"))
  	{document.form2.oculto.value=2;document.form2.submit();}
  }
  else{
  alert('Faltan datos para completar el registro');
  	document.form2.fecha.focus();
  	document.form2.fecha.select();
  }
}

function pdf()
{
document.form2.action="pdfsubsidios.php";
document.form2.target="_BLANK";
document.form2.submit(); 
document.form2.action="";
document.form2.target="";
}
</script>
<script type="text/javascript" src="css/programas.js"></script>
<script type="text/javascript" src="css/calendario.js"></script>
<link href="css/css2.css" rel="stylesheet" type="text/css" />
<link href="css/css3.css" rel="stylesheet" type="text/css" />
</head>
<body>
<IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
<span id="todastablas2"></span>
<table>
	<tr><script>barra_imagenes("serv");</script><?php cuadro_titulos();?></tr>	 
	<tr><?php menu_desplegable("serv");?></tr>
<tr>
  <td colspan="3" class="cinta"><a href="serv-reportesubs.php" ><img src="imagenes/add.png" alt="Nuevo" /></a> <img src="imagenes/guardad.png" alt="Guardar" /> <img src="imagenes/buscad.png" alt="Buscar" /></a> <a href="#" onClick="mypop=window.open('serv-principal.php','','');mypop.focus();"><img src="imagenes/nv.png" alt="nueva ventana"></a><a href="serv-reporecaudoscsv.php?vigencias=<?php echo $_POST[vigencias] ?>&mes=<?php echo $_POST[mes] ?>&tpserv=<?php echo $_POST[tpserv] ?>&estrato=<?php echo $_POST[estrato] ?>" target="_blank"><img src="imagenes/csv.png"  alt="csv"></a><a href="#" onClick="pdf()"><img src="imagenes/print.png" alt="imprimir"></a></td></tr>	
</table><tr><td colspan="3" class="tablaprin"> 
 <form name="form2" method="post" action="serv-reporecaudos.php">
 <?php
 $meses=array('','Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre');
$vigusu=vigencia_usuarios($_SESSION[cedulausu]);
$vigencia=$vigusu;
  $vact=$vigusu;  
 ?>
<table  class="inicio" align="center" >
      <tr >
        <td class="titulos" colspan="10">:. Reporte de Recaudos - Usuarios </td>
        <td width="139" class="cerrar" ><a href="serv-principal.php">Cerrar</a></td>
      </tr>
      <tr >
     	<td class="saludo1">Vigencia:</td>
       	<td >
			<select name="vigencias" id="vigencias" onChange="validar()">
      			<option value="">Sel..</option>
	  			<?php	  
      			for($x=$vact;$x>=$vact-2;$x--){
		 			$i=$x;  
		 			echo "<option  value=$x ";
		 			if($i==$_POST[vigencias]){
				 		echo " SELECTED";
				 	}
					echo " >".$x."</option>";	    
				}
	  			?>
      		</select> 
		</td>
     	<td class="saludo1">Mes:</td>
		<td>
       		<select name="mes" onChange="validar()">
       			<option value="">Seleccione ...</option>
         		<?php
		   		for($x=1;$x<=12;$x++){
		 		?>
					<option value="<?php  echo $x ?>" <?php if($_POST[mes]==$x) echo "  SELECTED"?>><?php echo $meses[$x] ?></option>
           		<?php       
				}
		   		?>  
         	</select> 
         	<input name="oculto" type="hidden" value="0">  
       	</td>  
        <td>
		</td>    
    </tr>                      
      <tr >
     	<td class="saludo1">Tipo de Servicio:</td>
       	<td >
			<select name="tpserv" id="tpserv" onChange="validar()">
      			<option value="">Seleccione..</option>
	  			<?php	  
				$sqls="select codigo, nombre from servservicios order by codigo";
				$rs=mysql_query($sqls, $linkbd);
      			while($rws=mysql_fetch_array($rs)){
		 			echo "<option  value=$rws[0] ";
		 			if($rws[0]==$_POST[tpserv]){
				 		echo " SELECTED";
				 	}
					echo " >".$rws[1]."</option>";	    
				}
	  			?>
      		</select> 
		</td>
     	<td class="saludo1">Estrato:</td>
		<td>
       		<select name="estrato" id="estrato" onChange="validar()">
       			<option value="">Seleccione ...</option>
	  			<?php	  
				$sqls="select id, descripcion, tipo from servestratos where id>6 order by id";
				$rs=mysql_query($sqls, $linkbd);
      			while($rws=mysql_fetch_array($rs)){
		 			echo "<option  value=$rws[0] ";
		 			if($rws[0]==$_POST[estrato]){
				 		echo " SELECTED";
				 	}
					echo " >".$rws[2]." - ".$rws[1]."</option>";	    
				}
	  			?>
         	</select> 
         	<input name="oculto" type="hidden" value="1">  
       	</td>  
        <td>
			<input type="button" name="buscar" id="buscar" value="  Buscar  " onClick="validar()">
		</td>    
    </tr>                      
   </table>     
	<div class="subpantalla" style="height:70%">
      <?php
	  $vigusu=vigencia_usuarios($_SESSION[cedulausu]);
	  $oculto=$_POST[oculto];
	  if($_POST[oculto]=='1'){
      	$con=1;
		if($_POST['tpserv']!="")
			$crit2 = " and servliquidaciones_det.servicio = ".$_POST['tpserv'];
		else
			$crit2="";
		if($_POST['estrato']!="")
			$crit3 = " and servliquidaciones_det.estrato = ".$_POST['estrato'];
		else
			$crit3="";

		$crit4 = "WHERE servliquidaciones.vigencia = ".$_POST[vigencias]." and (servliquidaciones.mes=$_POST[mes] or  servliquidaciones.mesfin=$_POST[mes])";

		$sqlr1="SELECT servliquidaciones_det.servicio, servservicios.nombre, servestratos.tipo, servestratos.descripcion, SUM(servliquidaciones_det.tarifa), SUM(servliquidaciones_det.subsidio), SUM(servliquidaciones_det.valorliquidacion), SUM(servliquidaciones_det.saldo), COUNT(servliquidaciones_det.estrato), servestratos.id FROM servliquidaciones INNER JOIN ((servliquidaciones_det INNER JOIN servservicios ON servliquidaciones_det.servicio=servservicios.codigo) INNER JOIN servestratos ON servliquidaciones_det.estrato=servestratos.id) ON servliquidaciones_det.id_liquidacion=servliquidaciones.id_liquidacion ".$crit4." ".$crit2."  ".$crit3." GROUP BY servliquidaciones_det.servicio, servliquidaciones_det.estrato";		
		$resp1 = mysql_query($sqlr1,$linkbd);
		$ntr = mysql_num_rows($resp1);
//echo  "sq:".$sqlr1;
echo "<table class='inicio' align='center' >
	<tr>
		<td colspan='9' class='titulos'>.: Resultados Busqueda:</td>
	</tr>
	<tr>
		<td colspan='9'>Registros Encontrados: $ntr</td>
	</tr>
	<tr>
		<td width='150' class='titulos2' style='border-top: 1px solid; border-bottom: 1px solid; border-left: 1px solid;'>Tipo de Servicio</td>
		<td width='150' class='titulos2' style='border-top: 1px solid; border-bottom: 1px solid;'>Estrato</td>
		<td width='150' class='titulos2' style='border-top: 1px solid; border-bottom: 1px solid; border-right: 1px solid;'>Usuarios</td>
		<td width='50' class='titulos2' style='border-top: 1px solid; border-bottom: 1px solid; border-left: 1px solid;'>Tarifa</td>
		<td width='50' class='titulos2' style='border-top: 1px solid; border-bottom: 1px solid;'>Subsidio</td>
		<td width='50' class='titulos2' style='border-top: 1px solid; border-bottom: 1px solid;'>Contribución</td>
		<td width='50' class='titulos2' style='border-top: 1px solid; border-bottom: 1px solid;'>Saldos Anteriores</td>
		<td width='50' class='titulos2' style='border-top: 1px solid; border-bottom: 1px solid; border-right: 1px solid;'>Valor Facturado</td>
		<td width='50' class='titulos2' style='border-top: 1px solid; border-bottom: 1px solid; border-left: 1px solid;'>Tarifa</td>
		<td width='50' class='titulos2' style='border-top: 1px solid; border-bottom: 1px solid;'>Subsidio</td>
		<td width='50' class='titulos2' style='border-top: 1px solid; border-bottom: 1px solid;'>Contribución</td>
		<td width='50' class='titulos2' style='border-top: 1px solid; border-bottom: 1px solid;'>Saldos Anteriores</td>
		<td width='50' class='titulos2' style='border-top: 1px solid; border-bottom: 1px solid; border-right: 1px solid;'>Valor Recaudado</td>
	</tr>";	
	$iter='zebra1';
	$iter2='zebra2';
	$totusu=0;
	$totftar=0;
	$totfsub=0;
	$totfcon=0;
	$totfant=0;
	$totfval=0;
	$totrtar=0;
	$totrsub=0;
	$totrcon=0;
	$totrant=0;
	$totrval=0;
 	while ($row =mysql_fetch_row($resp1)) {	
		//echo $sqlr;
		$contrib=$row[4]-$row[5];
		echo "<tr class='$iter' style='text-transform:uppercase' onMouseOver=\"anterior=this.style.backgroundColor;this.style.backgroundColor='#40b3ff';\" onMouseOut=\"this.style.backgroundColor=anterior\">
			<td >$row[0] $row[1]</td>
			<td >$row[2] - $row[3]</td>
			<td align='right' style='border-right: 1px solid;'>".number_format($row[8],0,",",".")."</td>
			<td align='right'>".number_format($row[4],2,",",".")."</td>
			<td align='right'>".number_format($row[5],2,",",".")."</td>
			<td align='right'>".number_format($contrib,2,",",".")."</td>
			<td align='right'>".number_format($row[7],2,",",".")."</td>
			<td align='right' style='border-right: 1px solid;'>".number_format($row[6],2,",",".")."</td>";
			$sqlr="SELECT SUM(servreciboscaja.valor), SUM(servreciboscaja_det.tarifa), SUM(servreciboscaja_det.subsidio), SUM(servreciboscaja_det.saldoanterior) FROM (servreciboscaja INNER JOIN servreciboscaja_det ON servreciboscaja_det.id_recibos=servreciboscaja.id_recibos) INNER JOIN (servliquidaciones INNER JOIN servliquidaciones_det ON servliquidaciones_det.id_liquidacion=servliquidaciones.id_liquidacion) ON servreciboscaja.id_recaudo=servliquidaciones.id_liquidacion WHERE servliquidaciones.vigencia = ".$_POST[vigencias]." and (servliquidaciones.mes=$_POST[mes] or  servliquidaciones.mesfin=$_POST[mes]) AND servliquidaciones_det.servicio=$row[0] AND servliquidaciones_det.estrato=$row[9]";		
			$rsr = mysql_query($sqlr,$linkbd);
			$wr=mysql_fetch_array($rsr);
			$rcont=$wr[1]-$wr[2];
			echo"<td align='right'>".number_format($wr[1],2,",",".")."</td>
			<td align='right'>".number_format($wr[2],2,",",".")."</td>
			<td align='right'>".number_format($rcont,2,",",".")."</td>
			<td align='right'>".number_format($wr[3],2,",",".")."</td>
			<td align='right' style='border-right: 1px solid;'>".number_format($wr[0],2,",",".")."</td>";

		echo"</tr>";	
	 	$con+=1;
	 	$aux=$iter;
	 	$iter=$iter2;
	 	$iter2=$aux;
	 	$totusu+=$row[8];
	 	$totftar+=$row[4];
	 	$totfsub+=$row[5];
	 	$totfcon+=$contrib;
	 	$totfant+=$row[7];
	 	$totfval+=$row[6];
	 	$totrtar+=$wr[1];
	 	$totrsub+=$wr[2];
	 	$totrcon+=$rcont;
	 	$totrant+=$wr[3];
	 	$totrval+=$wr[0];
 	}
 	echo "<tr>
		<td colspan='2'></td>
		<td align='right' style='border-right: 1px solid;'>".number_format($totusu,0,",",".")."</td>
		<td align='right'>".number_format($totftar,2,",",".")."</td>
		<td align='right'>".number_format($totfsub,2,",",".")."</td>
		<td align='right'>".number_format($totfcon,2,",",".")."</td>
		<td align='right'>".number_format($totfant,2,",",".")."</td>
		<td align='right' style='border-right: 1px solid;'>".number_format($totfval,2,",",".")."</td>
		<td align='right'>".number_format($totrtar,2,",",".")."</td>
		<td align='right'>".number_format($totrsub,2,",",".")."</td>
		<td align='right'>".number_format($totrcon,2,",",".")."</td>
		<td align='right'>".number_format($totrant,2,",",".")."</td>
		<td align='right' style='border-right: 1px solid;'>".number_format($totrval,2,",",".")."</td>
	</tr>";
 	echo"</table>";
}
?></div>
 <br><br></form>
</td></tr>     
</table>
</body>
</html>