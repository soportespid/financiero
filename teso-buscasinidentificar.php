<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require "comun.inc";
	require "funciones.inc";
	session_start();
	$linkbd=conectar_v7();	
	$linkbd -> set_charset("utf8");		
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
	$_POST['oculto']="2";
?>
<!DOCTYPE >
<html lang="es">
	<head>
	<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="ie=edge"/>
		<meta name="viewport" content="user-scalable=no">
		<title>:: IDEAL 10 - Tesorer&iacute;a</title>	
		<link href="favicon.ico" rel="shortcut icon"/>
		<link href="css/css2.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link rel="stylesheet" href="sweetalert2/dist/sweetalert2.min.css">
		<script src="sweetalert2/dist/sweetalert2.min.js"></script>
		<script type="text/javascript" src="jquery-1.11.0.min.js"></script> 
        <script type="text/javascript" src="css/programas.js"></script>
        <script type="text/javascript" src="css/calendario.js"></script>
		<script>
			//************* ver reporte ************
			//***************************************
			$(window).load(function () { $('#cargando').hide();});
		
			function buscar()
			{
				document.form2.submit(); 
			}
	
			function verUltimaPos(idcta){
				location.href="teso-editasinidentificar.php?idrecaudo="+idcta+"#";
			}
			function buscarbotonfiltro()
            {
                if((document.form2.fechaini.value != "" && document.form2.fechafin.value == "") || (document.form2.fechaini.value == "" && document.form2.fechafin.value != "")){
                    alert("Falta digitar fecha");
                }else{
                    document.getElementById('numpos').value=0;
                    document.getElementById('nummul').value=0;
                    document.form2.submit();
                }
                
            }
			function crearexcel(){
				document.form2.action="teso-ingresossinidentificarexcel.php";
				document.form2.target="_BLANK";
				document.form2.submit();
				document.form2.action="";
				document.form2.target="";
			}
			function anular(idr, fechaAnular){
				Swal.fire({
					icon: 'question',
					title: '¿Seguro que quiere anular el ingreso por identificar?',
					showDenyButton: true,
					confirmButtonText: 'Anular',
					confirmButtonColor: '#01CC42',
					denyButtonText: 'Cancelar',
					denyButtonColor: '#FF121A',
				}).then(
					(result) => {
						if (result.isConfirmed){
							document.form2.var1.value = idr;
							document.form2.var2.value = fechaAnular;
							document.form2.submit();
						}
						else if (result.isDenied){
							Swal.fire({
								icon: 'info',
								title: 'No se anulo el ingreso',
								confirmButtonText: 'Continuar',
								confirmButtonColor: '#FF121A',
								timer: 2500
							});
						}
					}
				)
			}
		</script>
		<?php
			if(isset($_GET['fini']) && isset($_GET['ffin'])){
				if(!empty($_GET['fini']) && !empty($_GET['ffin'])){
					$_POST['fecha']=$_GET['fini'];
					$_POST['fecha2']=$_GET['ffin'];
				}
			}
			$fech1 = explode("/", $_POST['fechaini']);
			$fech2 = explode("/", $_POST['fechafin']);
			$f1=$fech1[2]."-".$fech1[1]."-".$fech1[0];
			$f2=$fech2[2]."-".$fech2[1]."-".$fech2[0];

			$scrtop=$_GET['scrtop'];
			if($scrtop=="") $scrtop=0;
			echo"<script>
				window.onload=function(){
					$('#divdet').scrollTop(".$scrtop.")
				}
			</script>";
			$gidcta=$_GET['idcta'];
			if(isset($_GET['filtro']))
				$_POST['nombre']=$_GET['filtro'];
		?>
	</head>
	<body>
	<IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
	<span id="todastablas2"></span>
	<table>
		<tr><script>barra_imagenes("teso");</script><?php cuadro_titulos();?></tr>	 
		<tr><?php menu_desplegable("teso");?></tr>
		<tr>
	  		<td colspan="3" class="cinta">
	  			<a href="teso-sinidentificar.php" class="mgbt"><img src="imagenes/add.png" title="Nuevo"/></a>
				<a class="mgbt1"><img src="imagenes/guardad.png"  title="Guardar"/></a>
	  			<a class="mgbt"><img src="imagenes/busca.png" title="Buscar" /></a> 
				<a onClick="mypop=window.open('plan-agenda.php','','');mypop.focus()" class="mgbt"><img src="imagenes/agenda1.png" title="Agenda"/></a>  
	  			<a onClick="mypop=window.open('teso-principal.php','','');mypop.focus();" class="mgbt"><img src="imagenes/nv.png" title="Nueva ventana"></a>
				<a onClick="mypop=window.open('<?php echo $url2; ?>','','');mypop.focus();" class="tooltip bottom mgbt"><img src="imagenes/duplicar_pantalla.png" title="Duplicar pesta&ntilde;a"></a>  
				<a onclick="crearexcel()" class="mgbt"><img src="imagenes/excel.png" title="Excel"></a>
	  		</td>
	  	</tr>	
	</table>
	<form name="form2" method="post" action="teso-buscasinidentificar.php">
		<div class="loading" id="divcarga"><span>Cargando...</span></div>
		<input type="hidden" name="oculto" id="oculto"  value = '1'  >
		<?php
			if($_GET['numpag']!="")
			{
				$oculto=$_POST['oculto'];
				if($oculto!=2)
				{
				$_POST['numres']=$_GET['limreg'];
				$_POST['numpos']=$_GET['limreg']*($_GET['numpag']-1);
				$_POST['nummul']=$_GET['numpag']-1;
				}
			}
			else
			{
				if($_POST['nummul']=="")
				{
					$_POST['numres']=10;
					$_POST['numpos']=0;
					$_POST['nummul']=0;
				}
			}
		?>

		<table class="inicio ancho" >
			<tr>
				<td class="titulos" colspan="16">:. Buscar Ingresos sin Identificar</td>
				<td width="70" class="cerrar" ><a href="teso-principal.php"> Cerrar</a></td>
			</tr>
			<tr >
				<td class="tamano01" style="width:3.6cm;">Numero recibo:</td>
				<td colspan="4"><input type="search" name="numero" id="numero" value="<?php echo $_POST['numero'];?>" style="width:100%;"/></td>
				<td class="tamano01" style="width:3.6cm;">Concepto:</td>
				<td colspan="4"><input type="search" name="nombre" id="nombre" value="<?php echo $_POST['nombre'];?>" style="width:100%;"/></td>
				<td class="tamano01">Fecha inicial: </td>
				<td style="width:10%;"><input type="search" name="fechaini" id="fc_1198971545" title="DD/MM/YYYY" placeholder="DD/MM/YYYY" value="<?php echo $_POST['fechaini'];?>" onKeyUp="return tabular(event,this)" onKeyDown="mascara(this,'/',patron,true)" maxlength="10" onchange="" style="width:75%">&nbsp;<img src="imagenes/calendario04.png" onClick="displayCalendarFor('fc_1198971545');" class="icobut" title="Calendario"></td>
				<td class="tamano01" >Fecha final: </td>
				<td style="width:10%;"><input type="search" name="fechafin"  id="fc_1198971546" title="DD/MM/YYYY" placeholder="DD/MM/YYYY" value="<?php echo $_POST['fechafin'];?>" onKeyUp="return tabular(event,this) " onKeyDown="mascara(this,'/',patron,true)" maxlength="10" onchange="" style="width:75%"/>&nbsp;<img src="imagenes/calendario04.png" onClick="displayCalendarFor('fc_1198971546');"  class="icobut" title="Calendario"></td>  
				<td style="padding-bottom:1px"><em class="botonflecha" onClick="buscarbotonfiltro();">Buscar</em></td>
			</tr>                  
		</table>   
		<input type="hidden" name="var1" id="var1"  value=<?php echo $_POST['var1'];?>>
		<input type="hidden" name="var2" id="var2"  value=<?php echo $_POST['var2'];?>>
		<input type="hidden" name="fecham1"  id="fecham1" value="<?php echo $_POST['fecham1']; ?>"/>
		<input type="hidden" name="fecham2" id="fecham2" value="<?php echo $_POST['fecham2']; ?>"/>
		<input type="hidden" name="numres" id="numres" value="<?php echo $_POST['numres'];?>"/>
		<input type="hidden" name="numpos" id="numpos" value="<?php echo $_POST['numpos'];?>"/>
		<input type="hidden" name="nummul" id="nummul" value="<?php echo $_POST['nummul'];?>"/>

		<div class="subpantallap" style="height:68.5%; width:99.6%; " id="divdet">
			<?php
				$oculto=$_POST['oculto'];

				if($_POST['oculto']==2)
				{
					if($_POST['var1'] != ''){
						/* preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST['var2'],$fecha);
						$fechaf=$fecha[3]."-".$fecha[2]."-".$fecha[1]; */
						$bloq=bloqueos($_SESSION['cedulausu'],$_POST['var2']);	
						if($bloq>=1)
						{
							$sqlr="UPDATE comprobante_cab SET total_debito = 0, total_credito = 0, estado = '0' WHERE tipo_comp = 27 and numerotipo='".$_POST['var1']."'";
							mysqli_query($linkbd,$sqlr);
							$sqlr="update tesosinidentificar set estado='N' where id_recaudo = '".$_POST['var1']."'";
							mysqli_query($linkbd,$sqlr);
							
						}else{
							echo "<script>
								Swal.fire({
									icon: 'warning',
									title: 'No tiene permisos para anular este documento.',
									confirmButtonText: 'Continuar',
									confirmButtonColor: '#FF121A',
									timer: 3000
								});
							</script>";
						}
						echo "<script>document.form2.var1.value = ''</script>";
						echo "<script>document.form2.var2.value = ''</script>";
					}
					$oculto=$_POST['oculto'];

					$crit1=" ";
					$crit2=" ";
					$crit3=" ";

					if ($_POST['numero']!="")
					{
						$crit1=" AND tesosinidentificar.id_recaudo LIKE '".$_POST['numero']."' ";
					}
					
					if ($_POST['nombre']!="")
					{
						$crit2=" AND tesosinidentificar.concepto LIKE '".$_POST['nombre']."'  ";
					}
					
					if($_POST['fechaini']!='')
					{
						preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/",$_POST['fechaini'],$fecha);
						$fechai="$fecha[3]-$fecha[2]-$fecha[1]";
						if($_POST['fechafin']!='')
						{
							preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/",$_POST['fechafin'],$fecha);
							$fechaf="$fecha[3]-$fecha[2]-$fecha[1]";
							$crit3 = "AND fecha BETWEEN '$fechai' AND '$fechaf'";
						}
						else
						{
							$fechaf=date("Y-m-d");
							$crit3 = "AND fecha BETWEEN '$fechai' AND '$fechaf'";
						}
					}

					$sqlr="SELECT * FROM tesosinidentificar WHERE tesosinidentificar.id_recaudo>-1 AND tipo_mov='201' ".$crit1.$crit2.$crit3." ORDER BY tesosinidentificar.id_recaudo DESC";
					$resp = mysqli_query($linkbd,$sqlr);
					$ntr = mysqli_num_rows($resp);
					
					$_POST['numtop']=$ntr;

					$nuncilumnas=ceil($_POST['numtop']/$_POST['numres']);
					
					$cond2="";

					if ($_POST['numres'] != "-1"){ $cond2="LIMIT $_POST[numpos], $_POST[numres]";}

					$sqlr="SELECT * FROM tesosinidentificar WHERE tesosinidentificar.id_recaudo>-1 AND tipo_mov='201' ".$crit1.$crit2.$crit3." ORDER BY tesosinidentificar.id_recaudo DESC $cond2";
					$resp = mysqli_query($linkbd,$sqlr);

					$numcontrol=$_POST['nummul']+1;
                   
					if(($nuncilumnas==$numcontrol)||($_POST['numres']=="-1")){
						$imagenforward="<img src='imagenes/forward02.png' style='width:17px'>";
						$imagensforward="<img src='imagenes/skip_forward02.png' style='width:16px' >";
					}
					else{
						$imagenforward="<img src='imagenes/forward01.png' style='width:17px' title='Siguiente' onClick='numsiguiente()'>";
						$imagensforward="<img src='imagenes/skip_forward01.png' style='width:16px' title='Fin' onClick='saltocol(\"$nuncilumnas\")'>";
					}
					if(($_POST['numpos']==0)||($_POST['numres']=="-1")){
						$imagenback="<img src='imagenes/back02.png' style='width:17px'>";
						$imagensback="<img src='imagenes/skip_back02.png' style='width:16px'>";
					}
					else{
						$imagenback="<img src='imagenes/back01.png' style='width:17px' title='Anterior' onClick='numanterior();'>";
						$imagensback="<img src='imagenes/skip_back01.png' style='width:16px' title='Inicio' onClick='saltocol(\"1\")'>";
					}

					$con=1;

					echo "<table class='inicio' align='center' >
							<tr>
								<td colspan='7' class='titulos'>.: Resultados Busqueda:</td>
								<td class='submenu'>
									<select name='renumres' id='renumres' onChange='cambionum();' style='width:100%'>
										<option value='10'"; if ($_POST['renumres']=='10'){echo 'selected';} echo ">10</option>
										<option value='20'"; if ($_POST['renumres']=='20'){echo 'selected';} echo ">20</option>
										<option value='30'"; if ($_POST['renumres']=='30'){echo 'selected';} echo ">30</option>
										<option value='50'"; if ($_POST['renumres']=='50'){echo 'selected';} echo ">50</option>
										<option value='100'"; if ($_POST['renumres']=='100'){echo 'selected';} echo ">100</option>
										<option value='-1'"; if ($_POST['renumres']=='-1'){echo 'selected';} echo ">Todos</option>
									</select>
                            	</td>
							</tr>
							<tr>
								<td colspan='2'>Ingresos encontrados: $ntr2</td>
							</tr>
							<tr class='titulos2'>
								<td width='5%' >Codigo</td>
								<td >Nombre</td>
								<td >Fecha</td>
								<td >Contribuyente</td>
								<td >Valor</td>
								<td >Estado</td>
								<td >Anular</td>
								<td width='5%'><center>Ver</td>
							</tr>";	
					
							$iter='saludo1a';
							$iter2='saludo2';
							$filas=1;

						if($_POST['fechaini'] == '' && $_POST['fechafin'] == '' && $_POST['numero'] == '' && $_POST['nombre'] == '')
						{
							echo "
							<table class='inicio'>
								<tr>
									<td class='saludo1' style='text-align:center;width:100%;font-size:25px'>Utilice el filtro de busqueda</td>
								</tr>
							</table>";
							$nuncilumnas = 0;
						}
						elseif(mysqli_num_rows($resp) == 0 || mysqli_num_rows($resp) == '0')
						{
							echo "
							<table class='inicio'>
								<tr>
									<td class='saludo1' style='text-align:center;width:100%;font-size:25px'>No hay resultados de su busqueda.</td>
								</tr>
							</table>";
						}
						else
						{
							while ($row =mysqli_fetch_row($resp)) 
							{
								$ntr2 = $ntr;

								echo "<script>document.getElementById('RecEnc').innerHTML = 'Ingresos encontrados: $ntr2'</script>";

								$nter=buscatercero($row[7]);
								if($row[10]=='S'){
									$imgsem="src='imagenes/sema_verdeON.jpg' title='Activo'";
								}
								if($row[10]=='N'){
									$imgsem="src='imagenes/sema_rojoON.jpg' title='Inactivo'";
								}
								if($row[10]=='R'){
									$imgsem="src='imagenes/reversado.png' title='Inactivo'";
								}else{
									$imgsem="src='imagenes/sema_verdeON.jpg' title='Activo'"; 
								}	 		
								if($gidcta!=""){
									if($gidcta==$row[0]){
										$estilo='background-color:yellow';
									}
									else{
										$estilo="";
									}
								}
								else{
									$estilo="";
								}		 	
								$idcta="'".$row[0]."'";
								$numfil="'".$filas."'";
								$filtro="'".$_POST['nombre']."'";

								echo"
								<input type='hidden' name='codigoE[]' value='".$row[0]."'>
								<input type='hidden' name='nombreE[]' value='".$row[6]."'>
								<input type='hidden' name='fechaE[]' value='".$row[2]."'>
								<input type='hidden' name='nomContribuyenteE[]' value='".$nter."'>
								<input type='hidden' name='valorE[]' value='".number_format($row[9],2)."'>
								";
								switch($row[10]){
									case 'S':{
										echo"
										<input type='hidden' name='estadoE[]' value='ACTIVO'>";
										$clase = 'garbageX';
										$titulo = 'Anular';
									}break;
									case 'N':{
										echo"
										<input type='hidden' name='estadoE[]' value='INACTIVO'>";
										$clase = 'garbageY';
										$titulo = 'Ya esta Anulado';
									}break;
									case 'R':{
										echo"
										<input type='hidden' name='estadoE[]' value='INACTIVO'>";
										$clase = 'garbageY';
										$titulo = 'Esta Reversado';
									}break;
									case 'I':{
										echo"
										<input type='hidden' name='estadoE[]' value='INACTIVO'>";
										$clase = 'garbageY';
										$titulo = 'Ya esta Identificado';
									}break;
								}
								$opcionAnular = '';
								$bloq=bloqueos($_SESSION['cedulausu'],$row[2]);
								if($bloq>=1 && $row[10] == 'S')
								{
									$opcionAnular = "onclick=\"anular('".$row[0]."', '".$row[2]."')\"";
								}elseif ($row[10] != 'N'){
									$clase = "";
								}
								echo "<tr class='$iter' onDblClick=\"verUltimaPos($row[0])\" style='text-transform:uppercase; $estilo' >
										<td>$row[0]</td>
										<td>$row[6]</td>
										<td>$row[2]</td>
										<td>$nter</td>
										<td>".number_format($row[9],2)."</td>
										<td style='text-align:center;'><img $imgsem style='width:18px' ></td>
										<td style='font: 110% sans-serif; display: flex; justify-content: center;' $opcionAnular>
											<div class='$clase' title='$titulo'>
										</td>
										<td><a href='teso-editasinidentificar.php?idrecaudo=$row[0]'><center><img src='imagenes/lupa02.png' style='width:18px;'></center></a></td>
									</tr>";
									$con+=1;
									$aux=$iter;
									$iter=$iter2;
									$iter2=$aux;
									$filas++;
							}
						}

						echo"</table>
						<table class='inicio'>
							<tr>
								<td style='text-align:center;'>
									<a href='#'>$imagensback</a>&nbsp;
									<a href='#'>$imagenback</a>&nbsp;&nbsp;";
									if($nuncilumnas<=9){$numfin=$nuncilumnas;}
									else{$numfin=9;}
									for($xx = 1; $xx <= $numfin; $xx++){
										if($numcontrol<=9){$numx=$xx;}
										else{$numx=$xx+($numcontrol-9);}
										if($numcontrol==$numx){echo"<a href='#' onClick='saltocol(\"$numx\")'; style='color:#24D915'> $numx </a>";}
										else {echo"<a href='#' onClick='saltocol(\"$numx\")'; style='color:#000000'> $numx </a>";}
									}
									echo "		&nbsp;&nbsp;<a href='#'>$imagenforward</a>
												&nbsp;<a href='#'>$imagensforward</a>
								</td>
							</tr>
						</table>";

						echo"</table><script>document.getElementById('divcarga').style.display='none';</script>";
				}	
			?>
	    	
		</div>
	</form> 
</body>
</html>