<?php
    require_once '../../../comun.inc';
    require '../../../funciones.inc';
    require '../../../funcionesSP.inc.php';
    /*ini_set('display_errors', '1');
    ini_set('display_startup_errors', '1');
    error_reporting(E_ALL);*/
    session_start();
    if($_POST){
        $obj = new Plantilla();
        if($_POST['action']=="search"){
            $obj->search($_POST['search']);
        }else if($_POST['action']=="get"){
            $obj->getData($_POST['codigo']);
        }else if($_POST['action'] == "save"){
            $obj->save($_POST['data']);
        }
    }

    class Plantilla{
        private $linkbd;
        private $arrData;
        private $strVigencia;
        public function __construct() {
            $this->linkbd = conectar_v7();
            $this->linkbd->set_charset("utf8");
            $this->strVigencia = date("Y");
        }
        public function search(string $search){
            if(!empty($_SESSION)){
                $request['codigos'] = $this->selectCodigos($search);
                $request['terceros'] = $this->selectTerceros($search);
                echo json_encode($request,JSON_UNESCAPED_UNICODE);
            }
            die();
        }
        public function getData($id){
            if(!empty($_SESSION)){
                $contribuyente = $this->selectContribuyente($id);
                if(!empty($contribuyente)){
                    $request = $contribuyente;
                    $request['codigos'] = $this->selectCodigos();
                    $request['terceros'] = $this->selectTerceros();
                    $arrResponse = array("status"=>true,"data"=>$request);
                }else{
                    $arrResponse = array("status"=>false);
                }
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
            die();
        }

        public function save($data){
            if(!empty($_SESSION)){
                $arrData = json_decode($data,true);
                if(empty($arrData['cabecera'])){
                    $arrResponse = array("status"=>false,"msg"=>"Error de datos");
                }else{
                    $rutas = $arrData['rutas'];

                    if($_FILES){
                        if(isset($_FILES['pdf_comercio'])){
                            if($rutas['comercio'] != "")deleteFile($rutas['comercio']);
                            $rutas['comercio']='comercio_'.$arrData['cabecera']['consecutivo'].'_'.bin2hex(random_bytes(6)).'.pdf';
                        }
                        if(isset($_FILES['pdf_inscripcion'])){
                            if($rutas['inscripcion'] != "")deleteFile($rutas['inscripcion']);
                            $rutas['inscripcion']='inscripcion_'.$arrData['cabecera']['consecutivo'].'_'.bin2hex(random_bytes(6)).'.pdf';
                        }
                        if(isset($_FILES['pdf_documento'])){
                            if($rutas['documento'] != "")deleteFile($rutas['documento']);
                            $rutas['documento']='documento_'.$arrData['cabecera']['consecutivo'].'_'.bin2hex(random_bytes(6)).'.pdf';
                        }
                        if(isset($_FILES['pdf_rut'])){
                            if($rutas['ruta'] != "")deleteFile($rutas['ruta']);
                            $rutas['rut']='rut_'.$arrData['cabecera']['consecutivo'].'_'.bin2hex(random_bytes(6)).'.pdf';
                        }
                    }
                    $arrData['rutas'] = $rutas;
                    $flag = true;
                    if($arrData['cabecera']['estado'] == 'N'){
                        $flag = $this->validarPazYSalvo($arrData['cabecera']['fecha_registro'],$arrData['cabecera']['representante']);
                    }
                    if($flag){
                        $request = $this->updateData($arrData);
                        if(is_numeric($request) && $request > 0){
                            if(isset($_FILES['pdf_comercio']))uploadFile($_FILES['pdf_comercio'],$rutas['comercio']);
                            if(isset($_FILES['pdf_inscripcion']))uploadFile($_FILES['pdf_inscripcion'],$rutas['inscripcion']);
                            if(isset($_FILES['pdf_documento']))uploadFile($_FILES['pdf_documento'],$rutas['documento']);
                            if(isset($_FILES['pdf_rut']))uploadFile($_FILES['pdf_rut'],$rutas['rut']);
                            $arrResponse = array("status"=>true,"msg"=>"Datos guardados");
                        }else if($request=="existe"){
                            $arrResponse = array("status"=>false,"msg"=>"El contribuyente ya se ha inscrito, intente con otro.");
                        }else{
                            $arrResponse = array("status"=>false,"msg"=>"Ha ocurrido un error, no se ha podido guardar.");
                        }
                    }else{
                        $arrResponse = array(
                            "status"=>false,
                            "msg"=>"El inscrito no se encuentra al día con industria y comercio. No se puede cancelar la matrícula."
                        );
                    }
                }
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
            die();
        }
        public function selectDataRepresentante($cedula){
            $sql = "SELECT id_tercero,cedulanit as documento,direccion,tipodoc as tipo_documento,telefono,celular,email as correo,persona as tipo, regimen,
            CASE WHEN razonsocial IS NULL OR razonsocial = ''
            THEN CONCAT(nombre1,' ',nombre2,' ',apellido1,' ',apellido2)
            ELSE razonsocial END AS nombre FROM terceros WHERE cedulanit = $cedula";
            $request = mysqli_query($this->linkbd,$sql)->fetch_assoc();
            return $request;
        }
        public function selectDataCodigo($codigo){
            $sql = "SELECT nombre FROM codigosciiu WHERE porcentaje<>'' AND codigo = '$codigo'";
            $request = mysqli_query($this->linkbd,$sql)->fetch_assoc()['nombre'];
            return $request;
        }
        public function selectContribuyente($id){

            $sql = "SELECT *,DATE_FORMAT(fecha,'%d/%m/%Y') as fecha,
            DATE_FORMAT(fecha_inicio,'%d/%m/%Y') as fecha_inicio,
            DATE_FORMAT(fecha_matricula,'%d/%m/%Y') as fecha_matricula,
            DATE_FORMAT(fecha_novedad,'%d/%m/%Y') as fecha_novedad
            FROM tesorepresentantelegal WHERE id = $id";

            $request['contribuyente'] = mysqli_query($this->linkbd,$sql)->fetch_assoc();

            $request['contribuyente']['datos'] =  $this->selectDataRepresentante($request['contribuyente']['cedulanit']);
            $representantes = mysqli_fetch_all(mysqli_query($this->linkbd,"SELECT * FROM tesorepresentantesecundario WHERE representante_id = $id"),MYSQLI_ASSOC);
            $actividades = mysqli_fetch_all(mysqli_query(
                $this->linkbd,
                "SELECT *,DATE_FORMAT(fecha,'%d/%m/%Y') as fecha FROM tesoestablecimientociiu WHERE idrepresentantelegal = $id"),MYSQLI_ASSOC);
            $establecimientos = mysqli_fetch_all(mysqli_query($this->linkbd,
            "SELECT *,razonsocial as nombre, local as locales,valoractivos as activos,
            DATE_FORMAT(fechainicio,'%d/%m/%Y') as fecha_apertura FROM tesoestablecimiento WHERE representante_id = $id"
            ),MYSQLI_ASSOC);

            if(!empty($representantes)){
                $total = count($representantes);
                for ($i=0; $i < $total ; $i++) {
                    $info = $this->selectDataRepresentante($representantes[$i]['cedulanit']);
                    $representantes[$i]['nombre'] = $info['nombre'];
                    $representantes[$i]['tipo_documento'] = $info['tipo_documento'];
                    $representantes[$i]['documento'] = $info['documento'];
                    $representantes[$i]['telefono'] = $info['telefono'];
                    $representantes[$i]['celular'] = $info['celular'];
                }
            }

            if(!empty($actividades)){
                $total = count($actividades);
                for ($i=0; $i < $total ; $i++) {
                    $actividades[$i]['nombre'] = $this->selectDataCodigo($actividades[$i]['ciiu']);
                    $actividades[$i]['codigo'] = $actividades[$i]['ciiu'];
                }
            }
            $request['contribuyente']['representantes'] = $representantes;
            $request['contribuyente']['actividades'] = $actividades;
            $request['contribuyente']['establecimientos'] = $establecimientos;
            $sql = "SELECT * FROM tesorepresentantearchivos WHERE representante_id = $id";
            $request['contribuyente']['archivos'] = mysqli_query($this->linkbd,$sql)->fetch_assoc();

            return $request;
        }
        public function validarPazYSalvo($fecha,$cedula){
            $flag = false;
            $año = explode("/",$fecha)[2];
            $arrAños = array();
            //Obtengo los años liquidados
            $sql = "SELECT ageliquidado, estado
            FROM tesoindustria
            WHERE tercero = '$cedula' AND ageliquidado >= '$año' AND ageliquidado <= '$this->strVigencia' AND estado != 'N'
            ORDER BY ageliquidado";
            $request = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
            //Obtengo la cantidad de años que se deben liquidar desde la fecha de registro
            for ($i=$año; $i <= $this->strVigencia; $i++) {
                array_push($arrAños,$i);
            }
            $totalAños = count($arrAños);
            for ($i=0; $i < $totalAños; $i++) {
                //Valido si existe el año liquidado y si está en los años a liquidar
                if(isset($request[$i]) && $request[$i]['ageliquidado'] == $arrAños[$i]){
                    if($request[$i]['estado'] == "N"){
                        $flag = false;
                        break;
                    }else{
                        $flag = true;
                    }
                }else{
                    $flag = false;
                    break;
                }
            }
            return $flag;
        }
        public function updateData(array $data){
            $this->arrData = $data;
            $cabecera = $this->arrData['cabecera'];
            $arrFechaMatricula = explode("/",$cabecera['fecha_matricula']);
            $arrFechaInicio = explode("/",$cabecera['fecha_inicio']);
            $arrFechaNovedad = explode("/",$cabecera['fecha_novedad']);
            $cabecera['fecha_matricula'] = date_format(date_create($arrFechaMatricula[2]."-".$arrFechaMatricula[1]."-".$arrFechaMatricula[0]),"Y-m-d");
            $cabecera['fecha_inicio'] = date_format(date_create($arrFechaInicio[2]."-".$arrFechaInicio[1]."-".$arrFechaInicio[0]),"Y-m-d");
            $cabecera['fecha_novedad'] = date_format(date_create($arrFechaNovedad[2]."-".$arrFechaNovedad[1]."-".$arrFechaNovedad[0]),"Y-m-d");
            $revisor = $cabecera['revisor'];
            $representantes = $this->arrData['representantes'];
            $actividades = $this->arrData['actividades'];
            $establecimientos = $this->arrData['establecimientos'];
            $rutas = $this->arrData['rutas'];
            $sql = "SELECT * FROM tesorepresentantelegal WHERE id = $cabecera[consecutivo] AND cedulanit != $cabecera[representante]";
            $request = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
            if(empty($request)){
                $sql = "UPDATE tesorepresentantelegal SET cedulanit=$cabecera[representante],
                fecha_novedad='$cabecera[fecha_novedad]',
                estado='$cabecera[estado]',
                estado_temporal=$cabecera[temporal],
                matricula='$cabecera[matricula]',
                fecha_matricula='$cabecera[fecha_matricula]',
                fecha_inicio='$cabecera[fecha_inicio]',
                fiscal_tipo_documento='$revisor[tipo]',
                fiscal_documento='$revisor[documento]',
                fiscal_nombre='$revisor[nombre]',
                estado_retenedor=$cabecera[retendor],
                estado_autoretenedor=$cabecera[auto],
                estado_gran=$cabecera[gran],
                estado_alumbrado=$cabecera[alumbrado],
                tipo_matricula = $cabecera[tipo_matricula]
                WHERE id = $cabecera[consecutivo]";
                $request = intval(mysqli_query($this->linkbd,$sql));
                $this->insertActividad($cabecera['consecutivo'],$actividades);
                $this->updateRuta($cabecera['consecutivo'],$rutas);
                $this->insertRepresentante($cabecera['consecutivo'],$representantes);
                $this->insertEstablecimiento($cabecera['consecutivo'],$establecimientos);
            }else{
                $request = "existe";
            }
            return $request;
        }
        public function updateRuta($id,$data){
            $sql="";
            $request = mysqli_query($this->linkbd,"SELECT * FROM tesorepresentantearchivos WHERE representante_id = $id")->fetch_assoc();
            if(!empty($request)){
                if($data['inscripcion'] != "" || $data['documento'] != ""  || $data['rut'] != "" || $data['comercio'] != "" ){
                    $sql = "UPDATE tesorepresentantearchivos SET ruta_inscripcion='$data[inscripcion]',
                    ruta_documento = '$data[documento]',
                    ruta_rut = '$data[rut]',
                    ruta_comercio = '$data[comercio]'
                    WHERE representante_id = $id";
                }
            }else{
                if($data['inscripcion'] != "" || $data['documento'] != ""  || $data['rut'] != "" || $data['comercio'] != "" ){
                    $sql = "INSERT INTO tesorepresentantearchivos(representante_id,ruta_inscripcion,ruta_documento,ruta_rut,ruta_comercio)
                    VALUES($id,'$data[inscripcion]','$data[documento]','$data[rut]','$data[comercio]')";
                    mysqli_query($this->linkbd,$sql);
                }
            }
            mysqli_query($this->linkbd,$sql);
        }
        public function insertRepresentante($id,$data){
            mysqli_query($this->linkbd,"DELETE FROM tesorepresentantesecundario WHERE representante_id = $id");
            if(!empty($data)){
                foreach ($data as $d) {
                    $sql = "INSERT INTO tesorepresentantesecundario(representante_id,cedulanit,tipo,estado)
                    VALUES($id,'$d[documento]','$d[tipo]','S')";
                    mysqli_query($this->linkbd,$sql);
                }
            }
        }
        public function insertActividad($id,$data){
            mysqli_query($this->linkbd,"DELETE FROM tesoestablecimientociiu WHERE idrepresentantelegal = $id");
            foreach ($data as $d) {
                $arrDate = explode("/",$d['fecha']);
                $fecha = date_format(date_create($arrDate[2]."-".$arrDate[1]."-".$arrDate[0]),"Y-m-d");
                $sql = "INSERT INTO tesoestablecimientociiu(idrepresentantelegal,ciiu,tipo,fecha,estado)
                VALUES($id,'$d[codigo]','$d[tipo]','$fecha','S')";
                mysqli_query($this->linkbd,$sql);
            }
        }
        public function insertEstablecimiento($id,$data){
            mysqli_query($this->linkbd,"DELETE FROM tesoestablecimiento WHERE representante_id = $id");
            if(!empty($data)){
                foreach ($data as $d) {
                    $arrDate = explode("/",$d['fecha_apertura']);
                    $fecha = date_format(date_create($arrDate[2]."-".$arrDate[1]."-".$arrDate[0]),"Y-m-d");
                    $sql = "INSERT INTO tesoestablecimiento(representante_id,razonsocial,direccion,fechainicio,telefono,correo,valoractivos,empleados,estado)
                    VALUES($id,'$d[nombre]','$d[direccion]','$fecha','$d[telefono]','$d[correo]',$d[activos],$d[empleados],'S')";
                    mysqli_query($this->linkbd,$sql);
                }
            }
        }
        public function selectTerceros($search=""){
            $s="";
            if($search !=""){
                $s="WHERE nombre1 LIKE '$search%' OR nombre2 LIKE '$search%'
                OR apellido1 LIKE '$search%' OR apellido2 LIKE '$search%' OR razonsocial LIKE '$search%'
                OR cedulanit LIKE '$search%'";
            }
            $sql="SELECT id_tercero,cedulanit as documento,direccion,tipodoc as tipo_documento,telefono,celular,email as correo,persona as tipo, regimen,
            CASE WHEN razonsocial IS NULL OR razonsocial = ''
            THEN CONCAT(nombre1,' ',nombre2,' ',apellido1,' ',apellido2)
            ELSE razonsocial END AS nombre FROM terceros $s ORDER BY id_tercero";
            $request = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
            $request['results'] = mysqli_query($this->linkbd,"SELECT count(*) as total FROM terceros $s")->fetch_assoc()['total'];
            return $request;
        }
        public function selectCodigos($search=""){
            $s="";
            if($search !=""){
                $s="AND (nombre LIKE '$search%' OR codigo LIKE '$search%')";
            }
            $sql="SELECT * FROM codigosciiu WHERE porcentaje<>'' $s ORDER BY id";
            $request = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
            $request['results'] = mysqli_query($this->linkbd,"SELECT count(*) as total FROM codigosciiu WHERE porcentaje<>'' $s")->fetch_assoc()['total'];
            return $request;
        }
    }
?>
