const URL ='tesoreria/inscripcion_industria/editar/teso-inscripcionEditar.php';

var app = new Vue({
    el:"#myapp",
    data() {
        return {
            isLoading:false,
            isModal:false,
            isModalActividad:false,
            arrTerceros:[],
            arrCodigosModal:[],
            arrTercerosModal:[],
            arrCodigos:[],
            arrEstablecimientos:[],
            objActividad:{codigo:"",nombre:"",fecha:""},
            objTerceroSec:{
                tipo_documento:"",
                documento:"",
                nombre:"",
                tipo:"",
                telefono:"",
                celular:"",
            },
            objTerceroPrin:{
                tipo_documento:"",
                documento:"",
                nombre:"",
                tipo:1,
                regimen:1,
                direccion:"",
                telefono:"",
                celular:"",
                correo:"",
            },
            objRevisor:{tipo:"",documento:"",nombre:""},
            selectDocModal:"Cedula de ciudadania",
            selectTipoRepresentante:"Principal",
            selectActividad:"Principal",
            selectRepMode:1,
            selectNovedad:"",
            selectMatricula:"",
            txtDocModal:"",
            txtNombreModal:"",
            txtCelModal:"",
            txtCorreoModal:"",
            txtIdRepModal:-1,
            txtSearchTercero:"",
            txtSearchCodigo:"",
            txtResults: 0,
            txtResultsTerceros:0,
            txtEstNombre:"",
            txtEstDireccion:"",
            txtEstCorreo:"",
            txtEstEmpleados:"",
            txtEstLocales:"",
            txtEstTelefono:"",
            txtEstActivos:"",
            txtConsecutivo:"",
            txtMatricula:"",
            checkGran:false,
            checkAlumbrado:false,
            checkRetenedor:false,
            checkAuto:false,
            checkTemporal:false,
            pdfInscripcion:"",
            pdfDocumento:"",
            pdfRut:"",
            pdfComercio:"",
            rutaInscripcion:"",
            rutaDocumento:"",
            rutaRut:"",
            rutaComercio:""
        }
    },
    mounted() {
        this.getData();
    },
    methods: {

        getData: async function(){
            let codigo = new URLSearchParams(window.location.search).get('id');
            const formData = new FormData();
            formData.append("action","get");
            formData.append("codigo",codigo);
            const response = await fetch(URL,{method:"POST", body:formData});
            const objData = await response.json();
            if(objData.status){
                let data = objData.data;
                let contribuyente = data.contribuyente;
                this.arrCodigosModal = data.codigos;
                this.arrTercerosModal = data.terceros;
                this.txtResults = data.codigos.results;
                this.txtResultsTerceros = data.terceros.results;
                this.txtConsecutivo = contribuyente.id;
                this.txtMatricula = contribuyente.matricula;
                this.selectMatricula = contribuyente.tipo_matricula;
                document.querySelector("#fechaReg").value = contribuyente.fecha;
                document.querySelector("#fechaMatricula").value=contribuyente.fecha_matricula;
                document.querySelector("#fechaInicio").value=contribuyente.fecha_inicio;
                document.querySelector("#fechaNov").value = contribuyente.fecha_novedad;
                this.objTerceroPrin = {
                    tipo_documento:contribuyente.datos.tipo_documento,
                    documento:contribuyente.datos.documento,
                    nombre:contribuyente.datos.nombre,
                    tipo:contribuyente.datos.tipo,
                    regimen:contribuyente.datos.regimen,
                    direccion:contribuyente.datos.direccion,
                    telefono:contribuyente.datos.telefono,
                    celular:contribuyente.datos.celular,
                    correo:contribuyente.datos.correo,
                }
                this.objRevisor={
                    tipo:contribuyente.fiscal_tipo_documento,
                    documento:contribuyente.fiscal_documento,
                    nombre:contribuyente.fiscal_nombre
                };
                this.checkGran=contribuyente.estado_gran;
                this.checkAlumbrado=contribuyente.estado_alumbrado;
                this.checkRetenedor=contribuyente.estado_retenedor;
                this.checkAuto=contribuyente.estado_autoretenedor;
                this.checkTemporal=contribuyente.estado_temporal;
                this.arrTerceros = contribuyente.representantes;
                this.arrCodigos = contribuyente.actividades;
                this.arrEstablecimientos = contribuyente.establecimientos;

                if(contribuyente.archivos != null){
                    this.rutaInscripcion=contribuyente.archivos.ruta_inscripcion;
                    this.rutaDocumento=contribuyente.archivos.ruta_documento;
                    this.rutaRut=contribuyente.archivos.ruta_rut;
                    this.rutaComercio=contribuyente.archivos.ruta_comercio;
                    this.selectNovedad = contribuyente.estado;
                }
            }else{
                window.location.href="teso-inscripcionIndustriaBuscar.php";
            }

        },
        search: async function(option=""){

            let search ="";
            if(option=="modal_actividad")search = this.txtSearchCodigo;
            if(option=="modal_terceros")search = this.txtSearchTercero;

            const formData = new FormData();
            formData.append("action","search");
            formData.append("search",search);
            this.isLoading=true;

            const response = await fetch(URL,{method:"POST", body:formData});
            const objData = await response.json();

            if(option =="modal_actividad"){
                this.arrCodigosModal = objData.codigos;
                this.txtResults = objData.codigos.results;
            }else if(option =="modal_terceros"){
                this.arrTercerosModal = objData.terceros;
                this.txtResultsTerceros = objData.terceros.results;
            }
            this.isLoading=false;
        },
        save:async function(){
            let fechaRegistro = document.querySelector("#fechaReg").value;
            let fechaMatricula = document.querySelector("#fechaMatricula").value;
            let fechaInicio = document.querySelector("#fechaInicio").value;
            let fechaNov = document.querySelector("#fechaNov").value;
            let actPrincipal = this.arrCodigos.filter(el => el.tipo == "Principal");
            if(this.selectNovedad ==""){
                Swal.fire("Error","Seleccione el tipo de novedad","error");
                return false;
            }
            if(fechaNov=="" || fechaMatricula=="" || fechaInicio==""){
                Swal.fire("Error","La fecha de novedad, inicio de actividades y matrícula son obligatorias","error");
                return false;
            }
            if(this.txtMatricula ==""){
                Swal.fire("Error","La matrícula no puede estar vacía","error");
                return false;
            }
            if(this.objTerceroPrin.documento ==""){
                Swal.fire("Error","Debe asignar al representante","error");
                return false;
            }
            if(actPrincipal.length == 0){
                Swal.fire("Error","Debe asignar la actividad económica principal","error");
                return false;
            }
            let data = {
                cabecera:{
                    consecutivo:this.txtConsecutivo,
                    fecha_matricula: fechaMatricula,
                    fecha_inicio: fechaInicio,
                    fecha_novedad:fechaNov,
                    fecha_registro:fechaRegistro,
                    matricula:this.txtMatricula,
                    representante:this.objTerceroPrin.documento,
                    gran:this.checkGran ? 1 : 0,
                    alumbrado:this.checkAlumbrado ? 1 : 0,
                    auto:this.checkAuto ? 1 : 0,
                    retendor:this.checkRetenedor ? 1 : 0,
                    temporal:this.checkTemporal ? 1 : 0,
                    revisor:this.objRevisor,
                    estado: this.selectNovedad,
                    tipo_matricula:this.selectMatricula
                },
                rutas:{
                    inscripcion:this.rutaInscripcion,
                    documento:this.rutaDocumento,
                    rut:this.rutaRut,
                    comercio:this.rutaComercio
                },
                representantes:this.arrTerceros,
                actividades:this.arrCodigos,
                establecimientos:this.arrEstablecimientos
            }
            let formData = new FormData();
            formData.append("action","save");
            formData.append("data",JSON.stringify(data));
            formData.append("pdf_comercio",this.pdfComercio);
            formData.append("pdf_inscripcion",this.pdfInscripcion);
            formData.append("pdf_documento",this.pdfDocumento);
            formData.append("pdf_rut",this.pdfRut);

            Swal.fire({
                title:"¿Estás segur@ de guardar?",
                text:"",
                icon: 'warning',
                showCancelButton:true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText:"Sí, guardar",
                cancelButtonText:"No, cancelar"
            }).then(async function(result){
                if(result.isConfirmed){
                    const response = await fetch(URL,{method:"POST",body:formData});
                    const objData = await response.json();
                    if(objData.status){
                        Swal.fire("Guardado",objData.msg,"success");
                    }else{
                        Swal.fire("Error",objData.msg,"error");
                    }
                }
            });

        },
        uploadFile:function(element,type){
            if(type=="pdfInscripcion")this.pdfInscripcion = element.event.target.files[0];
            if(type=="pdfDocumento")this.pdfDocumento = element.event.target.files[0];
            if(type=="pdfRut")this.pdfRut = element.event.target.files[0];
            if(type=="pdfComercio")this.pdfComercio = element.event.target.files[0];
        },
        selectItemModal: async function(item,type){
            if(type == "actividad"){
                this.objActividad.codigo = item.codigo;
                this.objActividad.nombre = item.nombre;
                this.isModalActividad = false;
            }else if(type =="tercero"){
                if(this.selectRepMode==2){
                    this.objTerceroSec = item;
                }else{
                    this.objTerceroPrin = item;
                }
                this.isModal = false;
            }
        },
        addEst: function(){
            let fechaApertura = document.querySelector("#fechaApertura").value;
            if(this.txtEstNombre == "" || this.txtEstDireccion=="" || this.txtEstTelefono =="" || fechaApertura ==""){
                Swal.fire("Error","Los campos con (*) son obligatorios","error");
                return false;
            }
            let total = this.arrEstablecimientos.length;
            if(total > 0){
                if(this.arrEstablecimientos.findIndex(el=>el.nombre==this.txtEstNombre) >=0){
                    Swal.fire("Error","El establecimiento ya fue añadido, pruebe con otro nombre.","error");
                    return false;
                }
            }
            const obj = {
                fecha_apertura:fechaApertura,
                nombre:this.txtEstNombre,
                direccion: this.txtEstDireccion,
                correo: this.txtEstCorreo != "" ? this.txtEstCorreo : "N/A",
                telefono:this.txtEstTelefono,
                empleados:this.txtEstEmpleados =="" ? 0 :this.txtEstEmpleados ,
                locales:this.txtEstLocales,
                activos:this.txtEstActivos =="" ? 0 :this.txtEstActivos
            }
            this.arrEstablecimientos.push(obj);
            document.querySelector("#fechaApertura").value ="";
            this.txtEstNombre = "";
            this.txtEstDireccion = "";
            this.txtEstCorreo = "";
            this.txtEstTelefono = "";
            this.txtEstEmpleados = "";
        },
        addTercero: function(){
            if(this.objTerceroSec.documento == ""){
                Swal.fire("Error","Debe seleccionar un tercero","error");
                return false;
            }
            let total = this.arrTerceros.length;
            if(total > 0){
                if(this.arrTerceros.findIndex(el=>el.documento==this.objTerceroSec.documento) >=0){
                    Swal.fire("Error","El tercero ya fue añadido, intente con otro.","error");
                    return false;
                }
                if(this.arrTerceros.findIndex(el=>el.tipo==this.selectTipoRepresentante)>=0 && this.selectTipoRepresentante =="Principal"){
                    Swal.fire("Error","El representante principal ya fue agregado.","error");
                    return false;
                }
            }
            const obj = {
                tipo_documento:this.objTerceroSec.tipo_documento,
                documento:this.objTerceroSec.documento,
                nombre:this.objTerceroSec.nombre,
                telefono:this.objTerceroSec.celular != "" ? this.objTerceroSec.celular : this.objTerceroSec.telefono,
                tipo:this.selectTipoRepresentante
            }
            this.arrTerceros.push(obj);
            //this.objTerceroSec.nombre="";
            //this.objTerceroSec.documento="";
        },
        addActividad: function(){
            let fecha = document.querySelector("#fechaInicioAct").value;
            if(this.objActividad.codigo == ""){
                Swal.fire("Error","Debe seleccionar una actividad","error");
                return false;
            }
            if(fecha ==""){
                Swal.fire("Error","Debe asignar la fecha de inicio de la actividad","error");
                return false;
            }
            let total = this.arrCodigos.length;
            if(total > 0){
                if(this.arrCodigos.findIndex(el=>el.codigo==this.objActividad.codigo) >=0){
                    Swal.fire("Error","La actividad ya fue añadida, intente con otra.","error");
                    return false;
                }
                if(this.arrCodigos.findIndex(el=>el.tipo==this.selectActividad)>=0 && this.selectActividad =="Principal"){
                    Swal.fire("Error","La actividad principal ya fue agregada.","error");
                    return false;
                }
            }
            const obj = {
                codigo:this.objActividad.codigo,
                nombre:this.objActividad.nombre,
                fecha:fecha,
                tipo:this.selectActividad
            }
            this.arrCodigos.push(obj);
            this.objActividad.nombre="";
            this.objActividad.codigo="";
            document.querySelector("#fechaInicioAct").value="";
        },
        delItem:function(index,type){
            if(type=="tercero")this.arrTerceros.splice(index,1);
            if(type=="actividad")this.arrCodigos.splice(index,1);
            if(type=="establecimiento")this.arrEstablecimientos.splice(index,1);
        },
        formatNumero: function(valor){
            return new Intl.NumberFormat("en-US", {style: "currency", currency: "USD"}).format(valor);
        },
        printCertificado:function(){
            window.open("teso-inscripcionIndustriaCertificadoPdf.php?id="+this.txtConsecutivo);
        },
        printPDF:function(){
            let fechaRegistro = document.querySelector("#fechaReg").value;
            let fechaNovedad = document.querySelector("#fechaNov").value;
            let fechaMatricula = document.querySelector("#fechaMatricula").value;
            let fechaInicio = document.querySelector("#fechaInicio").value;
            let actPrincipal = this.arrCodigos.filter(el => el.tipo == "Principal");
            if(this.selectNovedad ==""){
                Swal.fire("Error","Seleccione el tipo de novedad","error");
                return false;
            }
            if(fechaNovedad=="" || fechaMatricula=="" || fechaInicio==""){
                Swal.fire("Error","La fecha de novedad, inicio de actividades y matrícula son obligatorias","error");
                return false;
            }
            if(this.txtMatricula ==""){
                Swal.fire("Error","La matrícula no puede estar vacía","error");
                return false;
            }
            if(this.objTerceroPrin.documento ==""){
                Swal.fire("Error","Debe asignar al representante","error");
                return false;
            }
            if(actPrincipal.length == 0){
                Swal.fire("Error","Debe asignar la actividad económica principal","error");
                return false;
            }
            let data = {
                cabecera:{
                    consecutivo:this.txtConsecutivo,
                    fecha_registro: fechaRegistro,
                    fecha_matricula: fechaMatricula,
                    fecha_inicio: fechaInicio,
                    fecha_novedad:fechaNovedad,
                    matricula:this.txtMatricula,
                    contribuyente:this.objTerceroPrin,
                    gran:this.checkGran,
                    alumbrado:this.checkAlumbrado,
                    auto:this.checkAuto,
                    retendor:this.checkRetenedor,
                    temporal:this.checkTemporal,
                    revisor:this.objRevisor,
                    estado: this.selectNovedad,
                    tipo_matricula:this.selectMatricula
                },
                representantes:this.arrTerceros,
                actividades:this.arrCodigos,
                establecimientos:this.arrEstablecimientos
            }
            window.open("teso-inscripcionIndustriaPdf.php?data="+JSON.stringify(data));
        },
        validInteger:function(valor){
            valor = new String(valor);
            if(valor.includes(".") || valor.includes(",")){
                Swal.fire("Error","La cantidad debe ser un número entero.","error");
            }
        },
        showTab:function(tab){
            let tabs = this.$refs.rTabs.children;
            let tabsContent = this.$refs.rTabsContent.children;
            for (let i = 0; i < tabs.length; i++) {
                if(!tabs[i].classList.contains("exclude")){
                    tabs[i].classList.remove("active");
                    tabsContent[i].classList.remove("active")
                }
            }
            tabs[tab-1].classList.add("active");
            tabsContent[tab-1].classList.add("active")
        }
    },
    computed:{

    }
})
