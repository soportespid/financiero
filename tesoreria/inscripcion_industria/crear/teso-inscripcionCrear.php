<?php
    require_once '../../../comun.inc';
    require '../../../funciones.inc';
    require '../../../funcionesSP.inc.php';
    /*ini_set('display_errors', '1');
    ini_set('display_startup_errors', '1');
    error_reporting(E_ALL);*/
    session_start();
    //dep($_POST);exit;
    if($_POST){
        $obj = new Plantilla();
        if($_POST['action']=="search"){
            $obj->search($_POST['search']);
        }else if($_POST['action']=="get"){
            $obj->getData();
        }else if($_POST['action'] =="clasificador"){
            $obj->selectClasificador($_POST['cuenta']);
        }else if($_POST['action'] == "save"){
            $obj->save($_POST['data']);
        }
    }

    class Plantilla{
        private $linkbd;
        private $arrData;
        public function __construct() {
            $this->linkbd = conectar_v7();
            $this->linkbd->set_charset("utf8");
        }
        public function search(string $search){
            if(!empty($_SESSION)){
                $request['codigos'] = $this->selectCodigos($search);
                $request['terceros'] = $this->selectTerceros($search);
                echo json_encode($request,JSON_UNESCAPED_UNICODE);
            }
            die();
        }
        public function getData(){
            if(!empty($_SESSION)){
                $request['consecutivo'] = $this->selectConsecutivo();
                $request['codigos'] = $this->selectCodigos();
                $request['terceros'] = $this->selectTerceros();
                echo json_encode($request,JSON_UNESCAPED_UNICODE);
            }
            die();
        }

        public function save($data){
            if(!empty($_SESSION)){
                $arrData = json_decode($data,true);
                if(empty($arrData['cabecera'])){
                    $arrResponse = array("status"=>false,"msg"=>"Error de datos");
                }else{
                    $rutas = array("comercio"=>"","inscripcion"=>"","documento"=>"","rut"=>"");
                    if($_FILES){
                        if(isset($_FILES['pdf_comercio']))$rutas['comercio']='comercio_'.$arrData['cabecera']['consecutivo'].'_'.bin2hex(random_bytes(6)).'.pdf';
                        if(isset($_FILES['pdf_inscripcion']))$rutas['inscripcion']='inscripcion_'.$arrData['cabecera']['consecutivo'].'_'.bin2hex(random_bytes(6)).'.pdf';
                        if(isset($_FILES['pdf_documento']))$rutas['documento']='documento_'.$arrData['cabecera']['consecutivo'].'_'.bin2hex(random_bytes(6)).'.pdf';
                        if(isset($_FILES['pdf_rut']))$rutas['rut']='rut_'.$arrData['cabecera']['consecutivo'].'_'.bin2hex(random_bytes(6)).'.pdf';
                    }
                    $arrData['rutas'] = $rutas;
                    $request = $this->insertData($arrData);
                    if(is_numeric($request) && $request > 0){
                        if($rutas['comercio'] !="")uploadFile($_FILES['pdf_comercio'],$rutas['comercio']);
                        if($rutas['inscripcion'] !="")uploadFile($_FILES['pdf_inscripcion'],$rutas['inscripcion']);
                        if($rutas['documento'] !="")uploadFile($_FILES['pdf_documento'],$rutas['documento']);
                        if($rutas['rut'] !="")uploadFile($_FILES['pdf_rut'],$rutas['rut']);
                        $arrResponse = array("status"=>true,"msg"=>"Datos guardados");
                    }else if($request=="existe"){
                        $arrResponse = array("status"=>false,"msg"=>"El contribuyente ya se ha inscrito, intente con otro.");
                    }else{
                        $arrResponse = array("status"=>false,"msg"=>"Ha ocurrido un error, no se ha podido guardar.");
                    }
                }
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
            die();
        }
        public function insertData(array $data){
            $this->arrData = $data;
            $cabecera = $this->arrData['cabecera'];
            $arrFechaRegistro = explode("/",$cabecera['fecha_registro']);
            $arrFechaMatricula = explode("/",$cabecera['fecha_registro']);
            $arrFechaInicio = explode("/",$cabecera['fecha_registro']);
            $cabecera['fecha_registro'] = date_format(date_create($arrFechaRegistro[2]."-".$arrFechaRegistro[1]."-".$arrFechaRegistro[0]),"Y-m-d");
            $cabecera['fecha_matricula'] = date_format(date_create($arrFechaMatricula[2]."-".$arrFechaMatricula[1]."-".$arrFechaMatricula[0]),"Y-m-d");
            $cabecera['fecha_inicio'] = date_format(date_create($arrFechaInicio[2]."-".$arrFechaInicio[1]."-".$arrFechaInicio[0]),"Y-m-d");
            $revisor = $cabecera['revisor'];
            $representantes = $this->arrData['representantes'];
            $actividades = $this->arrData['actividades'];
            $establecimientos = $this->arrData['establecimientos'];
            $rutas = $this->arrData['rutas'];
            $sql = "SELECT * FROM tesorepresentantelegal WHERE cedulanit = '$cabecera[representante]'";
            $request = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);

            if(empty($request)){
                $sql = "INSERT INTO tesorepresentantelegal(cedulanit,fecha,estado,estado_temporal,matricula,
                fecha_matricula,fecha_inicio,fiscal_tipo_documento,fiscal_documento,fiscal_nombre,
                estado_retenedor,estado_autoretenedor,estado_gran,estado_alumbrado,fecha_novedad,tipo_matricula) VALUES(
                    $cabecera[representante],
                    '$cabecera[fecha_registro]',
                    'S',
                    $cabecera[temporal],
                    '$cabecera[matricula]',
                    '$cabecera[fecha_matricula]',
                    '$cabecera[fecha_inicio]',
                    '$revisor[tipo]',
                    '$revisor[documento]',
                    '$revisor[nombre]',
                    $cabecera[retendor],
                    $cabecera[auto],
                    $cabecera[gran],
                    $cabecera[alumbrado],
                    '$cabecera[fecha_registro]',
                    $cabecera[tipo_matricula]
                )";
                $request = intval(mysqli_query($this->linkbd,$sql));
                $this->insertActividad($cabecera['consecutivo'],$actividades);
                $this->insertRuta($cabecera['consecutivo'],$rutas);
                if(!empty($representantes))$this->insertRepresentante($cabecera['consecutivo'],$representantes);
                if(!empty($establecimientos))$this->insertEstablecimiento($cabecera['consecutivo'],$establecimientos);
            }else{
                $request = "existe";
            }
            return $request;
        }
        public function insertRuta($id,$data){
            if($data['inscripcion'] != "" || $data['documento'] != ""  || $data['rut'] != "" || $data['comercio'] != "" ){
                $sql = "INSERT INTO tesorepresentantearchivos(representante_id,ruta_inscripcion,ruta_documento,ruta_rut,ruta_comercio)
                VALUES($id,'$data[inscripcion]','$data[documento]','$data[rut]','$data[comercio]')";
                mysqli_query($this->linkbd,$sql);
            }
        }
        public function insertRepresentante($id,$data){
            foreach ($data as $d) {
                $sql = "INSERT INTO tesorepresentantesecundario(representante_id,cedulanit,tipo,estado)
                VALUES($id,'$d[documento]','$d[tipo]','S')";
                mysqli_query($this->linkbd,$sql);
            }
        }
        public function insertActividad($id,$data){
            foreach ($data as $d) {
                $arrDate = explode("/",$d['fecha']);
                $fecha = date_format(date_create($arrDate[2]."-".$arrDate[1]."-".$arrDate[0]),"Y-m-d");
                $sql = "INSERT INTO tesoestablecimientociiu(idrepresentantelegal,ciiu,tipo,fecha,estado)
                VALUES($id,'$d[codigo]','$d[tipo]','$fecha','S')";
                mysqli_query($this->linkbd,$sql);
            }
        }
        public function insertEstablecimiento($id,$data){
            foreach ($data as $d) {
                $arrDate = explode("/",$d['fecha_apertura']);
                $fecha = date_format(date_create($arrDate[2]."-".$arrDate[1]."-".$arrDate[0]),"Y-m-d");
                $sql = "INSERT INTO tesoestablecimiento(representante_id,razonsocial,direccion,fechainicio,telefono,correo,valoractivos,empleados,estado)
                VALUES($id,'$d[nombre]','$d[direccion]','$fecha','$d[telefono]','$d[correo]',$d[activos],$d[empleados],'S')";
                mysqli_query($this->linkbd,$sql);
            }
        }
        public function selectConsecutivo(){
            $sql = "SELECT MAX(id) as id FROM tesorepresentantelegal";
            $request = mysqli_query($this->linkbd,$sql)->fetch_assoc()['id']+1;
            return $request;
        }
        public function selectTerceros($search=""){
            $s="";
            if($search !=""){
                $s="WHERE nombre1 LIKE '$search%' OR nombre2 LIKE '$search%'
                OR apellido1 LIKE '$search%' OR apellido2 LIKE '$search%' OR razonsocial LIKE '$search%'
                OR cedulanit LIKE '$search%'";
            }
            $sql="SELECT id_tercero,cedulanit as documento,direccion,tipodoc as tipo_documento,telefono,celular,email as correo,persona as tipo, regimen,
            CASE WHEN razonsocial IS NULL OR razonsocial = ''
            THEN CONCAT(nombre1,' ',nombre2,' ',apellido1,' ',apellido2)
            ELSE razonsocial END AS nombre FROM terceros $s ORDER BY id_tercero";
            $request = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
            $request['results'] = mysqli_query($this->linkbd,"SELECT count(*) as total FROM terceros $s")->fetch_assoc()['total'];
            return $request;
        }
        public function selectCodigos($search=""){
            $s="";
            if($search !=""){
                $s="AND (nombre LIKE '$search%' OR codigo LIKE '$search%')";
            }
            $sql="SELECT * FROM codigosciiu WHERE porcentaje<>'' $s ORDER BY id";
            $request = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
            $request['results'] = mysqli_query($this->linkbd,"SELECT count(*) as total FROM codigosciiu WHERE porcentaje<>'' $s")->fetch_assoc()['total'];
            return $request;
        }
    }
?>
