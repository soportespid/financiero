<?php
    require_once '../../../comun.inc';
    require '../../../funciones.inc';
    require '../../../funcionesSP.inc.php';
    /*ini_set('display_errors', '1');
    ini_set('display_startup_errors', '1');
    error_reporting(E_ALL);*/
    session_start();
    //dep($_POST);exit;
    if($_POST){
        $obj = new Plantilla();
        if($_POST['action']=="search"){
            $currentPage = intval($_POST['page']);
            $obj->search($_POST['search'],$currentPage);
        }else if($_POST['action']=="get"){
            $obj->getData();
        }
    }

    class Plantilla{
        private $linkbd;
        private $arrData;
        public function __construct() {
            $this->linkbd = conectar_v7();
            $this->linkbd->set_charset("utf8");
        }
        public function search(string $search,int $currentPage){
            if(!empty($_SESSION)){
                $request = $this->selectData($search,$currentPage);
                echo json_encode($request,JSON_UNESCAPED_UNICODE);
            }
            die();
        }
        public function getData(){
            if(!empty($_SESSION)){
                $request = $this->selectData();
                echo json_encode($request,JSON_UNESCAPED_UNICODE);
            }
            die();
        }
        public function selectData($search="",$currentPage=1){
            $perPage = 200;
            $startRows = ($currentPage-1) * $perPage;
            $totalRows = 0;
            $s="";
            if($search !=""){
                $s="WHERE id LIKE '$search%' OR cedulanit LIKE '$search%'";
            }
            $sql = "SELECT id,cedulanit, estado,  DATE_FORMAT(fecha,'%d/%m/%Y') as fecha
            FROM tesorepresentantelegal $s
            ORDER BY id DESC LIMIT $startRows, $perPage";
            $arrData = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
            $total = count($arrData);
            for ($i=0; $i < $total; $i++) {
                $cedula = $arrData[$i]['cedulanit'];
                $arrData[$i]['nombre'] = mysqli_query($this->linkbd,"SELECT CONCAT(razonsocial,' ',nombre1,' ',nombre2,' ',apellido1,' ',apellido2) as nombre
                FROM terceros WHERE cedulanit = $cedula")->fetch_assoc()['nombre'];
            }
            $totalRows = mysqli_query($this->linkbd,"SELECT count(*) as total FROM tesorepresentantelegal $s")->fetch_assoc()['total'];
            $totalPages = $totalRows > 0 ? ceil($totalRows/$perPage) : 1;
            $arrResponse = array("status"=>true,"data"=>$arrData,"total"=>$totalRows,"total_pages"=>$totalPages);
            return $arrResponse;
        }
    }
?>
