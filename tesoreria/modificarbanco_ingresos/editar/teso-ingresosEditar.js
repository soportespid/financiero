const URL ='tesoreria/modificarbanco_ingresos/editar/teso-ingresosEditar.php';

var app = new Vue({
    el:"#myapp",
    data() {
        return {
            isLoading:false,
            isModal:false,
            arrModalCuentas:[],
            objCuenta:{},
            objCuentaAnterior:{},
            objRecibo:{},
            arrModalCuentas:[],
            arrDetalle:[],
            strCausaCambio:"",
            txtNombre:"",
            txtSearch:"",
            txtResults: 0,
            txtConsecutivo:0,
            txtMax:0,
            txtType:0,
            intId:0,
        }
    },
    mounted() {
        this.getData();
    },
    methods: {
        getData: async function(){
            let codigo = new URLSearchParams(window.location.search).get('id');
            this.txtConsecutivo = parseInt(codigo);
            const formData = new FormData();
            formData.append("action","get");
            formData.append("codigo",codigo);
            const response = await fetch(URL,{method:"POST", body:formData});
            const objData = await response.json();
            console.log(objData);
            if(objData.status){
                this.objRecibo = objData.data.recibo;
                this.arrModalCuentas = objData.data.cuentas.data;
                this.txtResults = objData.data.cuentas.results;
                this.objCuenta = {...this.arrModalCuentas.filter(e=>e.cuenta==this.objRecibo.cuentabanco)[0]};
                this.objCuentaAnterior = {...this.arrModalCuentas.filter(e=>e.cuenta==this.objRecibo.cuentabanco)[0]};
                this.txtMax = objData.data.consecutivo;
                this.arrDetalle = this.objRecibo.detalle;
            }else{
                this.txtMax = objData.data
                window.location.href="teso-modificarBancoIngresoEditar.php?id="+this.txtMax;
            }

        },
        search: async function(option){
            let search="";
            if(option=="modal_cuenta")search=this.txtSearch;
            if(option=="codigo_cuenta")search=this.objCuenta.ncuentaban;
            const formData = new FormData();
            formData.append("action","search");
            formData.append("search",search);
            formData.append("option",option);
            this.isLoading=true;
            const response = await fetch(URL,{method:"POST", body:formData});
            const objData = await response.json();
            if(option=="modal_cuenta"){
                this.arrModalCuentas = objData.data;
                this.txtResults = objData.results;
            }else if(option=="codigo_cuenta"){
                if(objData!=null){
                    this.objCuenta = objData;
                }else{
                    this.objCuenta = {...this.arrModalCuentas.filter(e=>e.cuenta==this.objRecibo.cuentabanco)[0]};
                    Swal.fire("Error","La cuenta no existe","error");
                }
            }
            this.isLoading=false;
        },
        editItem:function(id){
            window.location.href= 'teso-modificarBancoIngresoEditar?id='+id;
        },
        save:async function(){
            let cleanCausaCambio = this.strCausaCambio.replace(" ","");
            if(this.objCuenta =="" || cleanCausaCambio == ""){
                Swal.fire("Error","Los campos con (*) son obligatorios","error");
                return false;
            }
            let formData = new FormData();
            const obj = {
                "viejo":this.objCuentaAnterior,
                "nuevo":this.objCuenta,
                "razon":this.strCausaCambio,
                "id":this.objRecibo.id_recibos
            }
            formData.append("action","save");
            formData.append("data",JSON.stringify(obj));
            Swal.fire({
                title:"¿Estás segur@ de guardar?",
                text:"",
                icon: 'warning',
                showCancelButton:true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText:"Sí, guardar",
                cancelButtonText:"No, cancelar"
            }).then(async function(result){
                if(result.isConfirmed){
                    const response = await fetch(URL,{method:"POST",body:formData});
                    const objData = await response.json();
                    if(objData.status){
                        Swal.fire("Guardado",objData.msg,"success");
                        setTimeout(function(){
                            window.location.reload();
                        },2000);
                    }else{
                        Swal.fire("Error",objData.msg,"error");
                    }
                }
            });
        },
        selectItemModal: async function(item,type){
            if(type =="cuenta"){
                this.objCuenta = {...item};
                this.isModal = false;
            }
        },
        formatNumero: function(valor){
            return new Intl.NumberFormat("en-US", {style: "currency", currency: "USD"}).format(valor);
        },
        validInteger:function(valor){
            valor = new String(valor);
            if(valor.includes(".") || valor.includes(",")){
                Swal.fire("Error","La cantidad debe ser un número entero.","error");
            }
        },
    },
    computed:{

    }
})
