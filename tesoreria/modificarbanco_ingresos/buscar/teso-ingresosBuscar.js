const URL ='tesoreria/modificarbanco_ingresos/buscar/teso-ingresosBuscar.php';

var app = new Vue({
    el:"#myapp",
    data() {
        return {
            isLoading:false,
            arrData:[],
            arrHistorial:[],
            txtResults:0,
            intTotalPages:1,
            intPage:1,
            txtSearch:"",
            txtSearchHistorial:"",
            intTotalPagesHistorial:1,
            intPageHistorial:1,
            txtResultsHistorial:0,
        }
    },
    mounted() {
        this.getData();
    },
    methods: {

        getData: async function(){
            const formData = new FormData();
            formData.append("action","get");
            this.isLoading=true;
            const response = await fetch(URL,{method:"POST", body:formData});
            const objData = await response.json();
            //Recibos
            this.arrData = objData.recibos.data;
            this.txtResults = objData.recibos.total;
            this.intTotalPages = objData.recibos.total_pages;
            //Historial
            this.arrHistorial = objData.historial.data;
            this.txtResultsHistorial = objData.historial.total;
            this.intTotalPagesHistorial = objData.historial.total_pages;
            this.isLoading=false;
        },
        search: async function(option,page=1){
            let search ="";
            if(option=="recibos")search = this.txtSearch;
            if(option=="historial")search = this.txtSearchHistorial;
            if(page <= 0){
                page = 1;
                if(option=="recibos")this.intPage = page;
                if(option=="historial")this.intPageHistorial = page;
            }else if(page > this.intTotalPages){
                page = this.intTotalPages;
                if(option=="recibos")this.intPage = page;
                if(option=="historial")this.intPageHistorial = page;
            }
            const formData = new FormData();
            formData.append("action","search");
            formData.append("search",search);
            formData.append("option",option);
            formData.append("page",page);
            this.isLoading=true;

            const response = await fetch(URL,{method:"POST", body:formData});
            const objData = await response.json();

            if(option=="recibos"){
                this.arrData = objData.data;
                this.txtResults = objData.total;
                this.intTotalPages = objData.total_pages;
            }else if(option=="historial"){
                this.arrHistorial = objData.data;
                this.txtResultsHistorial = objData.total;
                this.intTotalPagesHistorial = objData.total_pages;
            }

            this.isLoading=false;
        },
        editItem:function(id){
            window.location.href= 'teso-modificarBancoIngresoEditar.php?id='+id;
        },
        showTab:function(tab){
            let tabs = this.$refs.rTabs.children;
            let tabsContent = this.$refs.rTabsContent.children;
            for (let i = 0; i < tabs.length; i++) {
                tabs[i].classList.remove("active");
                tabsContent[i].classList.remove("active")
            }
            tabs[tab-1].classList.add("active");
            tabsContent[tab-1].classList.add("active")
        }
    },
    computed:{

    }
})
