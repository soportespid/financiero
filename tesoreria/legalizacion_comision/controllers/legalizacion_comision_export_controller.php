<?php
    require_once '../../../Librerias/core/Helpers.php';
    require_once '../../../tcpdf/tcpdf.php';
    require_once '../../../Librerias/core/Mysql.php';
    session_start();
    // ini_set('display_errors', '1');
    // ini_set('display_startup_errors', '1');
    // error_reporting(E_ALL);
    class legalizacionComisionExportController {
        public function exportPdf(){

            if(!empty($_SESSION)){
                $con = new Mysql();        
                $data = json_decode($_POST["data"], true);
                $items = json_decode($_POST["items"], true);
                $tipoComprobante = 48;
                $consecutivo = $data["consecutivo"];
                $vigencia = DateTime::createFromFormat('Y-m-d', $data["fecha"])->format('Y');

                $sql_comprobantes = "SELECT cuenta, detalle, valdebito, valcredito FROM comprobante_det WHERE tipo_comp = $tipoComprobante AND numerotipo = $consecutivo ORDER BY id_det ASC";
                $row_comprobantes = $con->select_all($sql_comprobantes);

                foreach ($row_comprobantes as $key => $comprobante) {
                    $sql_cuenta = "SELECT nombre FROM cuentasnicsp WHERE cuenta = '$comprobante[cuenta]'";
                    $row_cuenta = $con->select($sql_cuenta);
                    $row_comprobantes[$key]["nombre_cuenta"] = $row_cuenta["nombre"] != "" ? $row_cuenta["nombre"] : "Cuenta no creada";
                }

                $pdf = new MYPDF('P','mm','Letter', true, 'iso-8859-1', false);
                $pdf->SetDocInfoUnicode (true);
                // set document information
                $pdf->SetCreator(PDF_CREATOR);
                $pdf->SetAuthor('IDEALSAS');
                $pdf->SetTitle('Legalización de comisión');
                $pdf->SetSubject('Legalización de comisión');
                $pdf->SetKeywords('TCPDF, PDF, example, test, guide');
                $pdf->SetMargins(10, 38, 10);// set margins
                $pdf->SetHeaderMargin(38);// set margins
                $pdf->SetFooterMargin(17);// set margins
                $pdf->SetAutoPageBreak(TRUE, 20);// set auto page breaks
                // set some language-dependent strings (optional)
                if (@file_exists(dirname(__FILE__).'/lang/spa.php'))
                {
                    require_once(dirname(__FILE__).'/lang/spa.php');
                    $pdf->setLanguageArray($l);
                }
                $pdf->SetFillColor(255,255,255);
                $pdf->AddPage();
                $pdf->SetFont('helvetica','B',9);
                $pdf->SetFillColor(153,221,255);
                $pdf->MultiCell(190,5,"INFORMACIÓN GENERAL","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
                $pdf->ln();
                $pdf->SetFillColor(255,255,255);
                $pdf->SetTextColor(0,0,0);
                $pdf->SetFont('Helvetica','',8);
                $pdf->MultiCell(20,5,"CONSECUTIVO:","LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
                $pdf->MultiCell(15,5,$data["consecutivo"],"LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
                $pdf->MultiCell(25,5,"FECHA:","LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
                $pdf->MultiCell(25,5,$data["fecha"],"LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
                $pdf->MultiCell(25,5,"COD OBLIGACIÓN:","LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
                $pdf->MultiCell(25,5,$data["cod_obligacion"],"LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
                $pdf->MultiCell(20,5,"VALOR:","LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
                $pdf->MultiCell(35,5,"$".number_format($data["valor_legalizacion"], 2),"LRBT",'R',true,0,'','',true,0,false,true,0,'M',true);
                $pdf->ln();
                $pdf->MultiCell(35,5,"BENEFICIARIO:","LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
                $pdf->MultiCell(155,5,"$data[tercero] - $data[nomTercero]","LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
                $pdf->ln();
                $pdf->MultiCell(35,10,"OBJETO DE OBLIGACIÓN:","LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
                $pdf->MultiCell(155,10,$data["objeto"],"LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
                $pdf->ln();
                $pdf->MultiCell(35,10,"DETALLE LEGALIZACIÓN:","LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
                $pdf->MultiCell(155,10,$data["detalle"],"LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
                    
                if (!empty($items)) {
                    $pdf->ln();
                    $pdf->SetFont('helvetica','B',9);
                    $pdf->SetFillColor(153,221,255);
                    $pdf->MultiCell(190,5,"DESAGREACIÓN","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->ln();
                    $pdf->MultiCell(70,8,"Tercero","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->MultiCell(70,8,"Concepto","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->MultiCell(25,8,"Cuenta contable","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->MultiCell(25,8,"Valor","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->ln();
                    $pdf->SetFont('helvetica','',8);
                    $pdf->SetFillColor(255,255,255);

                    foreach ($items as $item) {
                        $height = $pdf->getNumLines($item['nombre'])*5;
                        $pdf->MultiCell(70,$height,"$item[documento] - $item[nombre]","LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
                        $pdf->MultiCell(70,$height,"$item[codigo] - $item[concepto]","LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
                        $pdf->MultiCell(25,$height,"$item[cuenta]","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
                        $pdf->MultiCell(25,$height,"$".number_format($item["valor"], 2),"LRBT",'R',true,0,'','',true,0,false,true,0,'M',true);
                        $pdf->ln();

                        $getY = $pdf->getY();
                        if ($getY > 200) {
                            $pdf->AddPage();
                        }
                    }
                }
                    
                if (!empty($row_comprobantes)) {
                    $pdf->SetFont('helvetica','B',8);
                    $pdf->SetFillColor(153,221,255);
                    $pdf->MultiCell(190,5,"DISCRIMINACIÓN CONTABLE","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->ln();
                    $pdf->MultiCell(30,10,"Cuenta","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->MultiCell(40,10,"Nombre","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->MultiCell(60,10,"Detalle","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->MultiCell(30,10,"Debito","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->MultiCell(30,10,"Credito","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->ln();
                    $pdf->SetFont('helvetica','',8);
                    $pdf->SetFillColor(255,255,255);
                    $totalDebito = $totalCredito = 0;
                    foreach ($row_comprobantes as $comprobante) {
                        $saldoCuenta = round($comprobante["valdebito"],2) - round($comprobante["valcredito"], 2);
                        if($saldoCuenta < 0){
                            $ValDebito = 0;
                            $ValCredito = $comprobante["valcredito"];
                        }else{
                            $ValDebito = $comprobante["valdebito"];
                            $ValCredito = 0;
                        }
                        $height = $pdf->getNumLines($comprobante['detalle'])*7;
                        $pdf->MultiCell(30,$height,"$comprobante[cuenta]","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
                        $pdf->MultiCell(40,$height,"$comprobante[nombre_cuenta]","LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
                        $pdf->MultiCell(60,$height,"$comprobante[detalle]","LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
                        $pdf->MultiCell(30,$height,"$".number_format($ValDebito, 2),"LRBT",'R',true,0,'','',true,0,false,true,0,'M',true);      
                        $pdf->MultiCell(30,$height,"$".number_format($ValCredito, 2),"LRBT",'R',true,0,'','',true,0,false,true,0,'M',true);      
            
                        $totalDebito += $ValDebito;
                        $totalCredito += $ValCredito;
                        $pdf->ln();
                        $getY = $pdf->getY();
                        if ($getY > 200) {
                            $pdf->AddPage();
                        }
                    }
            
                    $pdf->SetFont('helvetica','B',8);
                    $pdf->MultiCell(130,5,"TOTALES:","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->MultiCell(30,5,"$".number_format($totalDebito, 2),"LRBT",'R',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->MultiCell(30,5,"$".number_format($totalCredito, 2),"LRBT",'R',true,0,'','',true,0,false,true,0,'M',true);
                }
                $pdf->ln(30);

                /* Firmas de documento */
                $firmas = [];
                $sql_firmas = "SELECT id_cargo, id_comprobante FROM pptofirmas WHERE id_comprobante = '28' AND vigencia = '$vigencia'";
                $row_firmas = $con->select_all($sql_firmas);
        
                foreach ($row_firmas as $firma) {
                    if ($firma["id_cargo"] == 0) {
                        $data = [
                            "nombre" => buscatercero($_POST['tercero']),
                            "cargo" => "Beneficiario"
                        ];
        
                        array_push($firmas, $data);
                    } else {
                        $sql_funcionario ="SELECT cedulanit,(SELECT nombrecargo FROM planaccargos WHERE codcargo = '$firma[id_cargo]') AS cargo FROM planestructura_terceros where codcargo = '$firma[id_cargo]' AND estado = 'S'";
                        $row_funcionario = $con->select($sql_funcionario);

                        $sql_empleado = "SELECT concat(t.nombre1,' ',t.nombre2,' ',t.apellido1,' ',t.apellido2) AS nombre FROM terceros t, planestructura_terceros pt, planaccargos pl WHERE pt.estado='S' AND pt.cedulanit = t.cedulanit AND pl.codcargo = pt.codcargo AND t.cedulanit LIKE '%$row_funcionario[cedulanit]%' ORDER BY t.apellido1, t.apellido2, t.nombre1, t.nombre2";
                        $row_empleado = $con->select($sql_empleado);

                        $data = [
                            "nombre" => $row_empleado["nombre"],
                            "cargo" => $row_funcionario["cargo"]
                        ];
                        array_push($firmas, $data);
                    }
                }
        
                $cantidadFirmas = count($firmas);
                $anchoMaximo = 190;
        
                $pdf->setY(240);
        
                if ($cantidadFirmas == 1) {
                    $anchoFirma = min($anchoMaximo, 80); // Ancho máximo de 95 si hay una firma
                } elseif ($cantidadFirmas == 2) {
                    $anchoFirma = min($anchoMaximo / 2, 80); // Ancho máximo de 95 para cada firma si hay dos firmas
                } else {
                    $anchoFirma = $anchoMaximo / $cantidadFirmas; // Distribuir el ancho equitativamente si hay más de dos firmas
                }
        
                $pdf->SetFont('helvetica','B',6);
                $pdf->SetFillColor(153,221,255);
        
                if ($cantidadFirmas == 1) {
                    $pdf->setX(60);
                } else if ($cantidadFirmas == 2) {
                    $pdf->setX(20);
                }
        
                foreach ($firmas as $firma) {
                    $pdf->Cell($anchoFirma, 5, "Datos personales", 1, 0, 'C', true);
                }
        
                $pdf->Ln();
                $pdf->SetFont('helvetica','B',6);
                $pdf->SetFillColor(255,255,255);
        
                if ($cantidadFirmas == 1) {
                    $pdf->setX(60);
                } else if ($cantidadFirmas == 2) {
                    $pdf->setX(20);
                }
        
                foreach ($firmas as $firma) {
                    $pdf->Cell($anchoFirma, 5, " Nombre: $firma[nombre]", 1, 0, 'L', true);
                }
        
                $pdf->Ln();
        
                if ($cantidadFirmas == 1) {
                    $pdf->setX(60);
                } else if ($cantidadFirmas == 2) {
                    $pdf->setX(20);
                }
        
                foreach ($firmas as $firma) {
                    $pdf->Cell($anchoFirma, 5, " Cargo: $firma[cargo]", 1, 0, 'L', true);
                }
        
                $pdf->Ln();
        
                if ($cantidadFirmas == 1) {
                    $pdf->setX(60);
                } else if ($cantidadFirmas == 2) {
                    $pdf->setX(20);
                }
        
                foreach ($firmas as $firma) {
                    $pdf->cell($anchoFirma,15,'Firma','LRTB',0,'C',1);
                }

                $strFile = 'legalizacion_comision_'.$data["consecutivo"];
                $pdf->Output($strFile.'.pdf', 'I');
            }
            die();
        }

    }
    class MYPDF extends TCPDF {

		public function Header(){
            $data = json_decode($_POST["data"], true);
			$request = configBasica();
            $strNit = $request['nit'];
            $strRazon = $request['razonsocial'];

			//Parte Izquierda
			$this->Image('../../../imagenes/escudo.jpg',  13, 13, 20, 20, 'JPG', '', 'T', true, 300, '', false, false, 0, false, false, false);// Logo
			$this->SetFont('helvetica','B',8);
			$this->SetY(10);
			$this->RoundedRect(10, 10, 190, 25, 1,'');
			$this->Cell(0.1);
			$this->Cell(26,25,'','R',0,'L');
			$this->SetY(8);
			$this->SetX(40);
			$this->SetFont('helvetica','B',9);
			$this->Cell(140,15,strtoupper("$strRazon"),0,0,'C');
			$this->SetFont('helvetica','B',7);
			$this->SetY(12);
			$this->SetX(40);
			$this->Cell(140,15,'NIT: '.$strNit,0,0,'C');
			//*****************************************************************************************************************************
			$this->SetFont('helvetica','B',9);
			$this->SetY(23);
			$this->SetX(36);
            $this->Cell(164,12,"LEGALIZACION DE COMISIÓN",'T',0,'C');

			//**********************************************************
			$this->SetFont('times','B',10);
			$this->ln(12);
           
			//**********************************************************
            if($data["estado"] != "S"){
                $img_file = "../../../assets/img/anulado.png";
                $this->SetAlpha(0.35);
                $this->Image($img_file, 0, 20, 250, 280, '', '', '', false, 300, '', false, false, 300);
                $this->SetAlpha(1);
            }
		}
		public function Footer(){
            $con = new Mysql();        
			$request = configBasica();
            $data = json_decode($_POST["data"], true);
            $strDireccion = $request['direccion'] != "" ? "Dirección: ".strtoupper($request['direccion']) : "";
            $strWeb = $request['web'] != "" ? "Pagina web: ".strtoupper($request['web']) :"";
            $strEmail = $request['email'] !="" ? "Email: ".strtoupper($request['email']) :"";
            $strTelefono = $request['telefono'] != "" ? "Telefonos: ".$request['telefono'] : "";
            $sql_funcion = "SELECT id FROM teso_funciones WHERE nombre = 'Legalizacion Comision'";
            $row_funcion = $con->select($sql_funcion);
            $sql_auditoria = "SELECT usuario FROM teso_auditoria WHERE teso_funciones_id = $row_funcion[id] AND consecutivo = '$data[consecutivo]'";
            $row_auditoria = $con->select($sql_auditoria);
            if ($row_auditoria["usuario"] != "") {
                $strUsuario = $row_auditoria["usuario"];
            } else {
                $strUsuario = searchUser($_SESSION['cedulausu'])['nom_usu'];
            }
			$strNick = $_SESSION['nickusu'];
			$strFecha = date("d/m/Y H:i:s");
			$strIp = $_SERVER['REMOTE_ADDR'];

            $this->setY(280);
			$this->SetFont('helvetica', 'I', 8);
			$txt = <<<EOD
			$strDireccion $strTelefono
			$strEmail $strWeb
			EOD;
			$this->SetFont('helvetica', 'I', 6);
			$this->Cell(190,10,'','T',0,'T');
			$this->ln(2);
			$this->Write(0, $txt , '', 0, 'C', true, 0, false, false, 0);

			$this->Cell(50, 10, 'Hecho por: '.$strUsuario, 00, false, 'C', 0, '', 0, false, 'T', 'M');
			$this->Cell(30, 10, 'Impreso por: '.$strNick, 0, false, 'C', 0, '', 0, false, 'T', 'M');
			$this->Cell(30, 10, 'IP: '.$strIp, 0, false, 'C',0, '', 0, false, 'T', 'M');
			$this->Cell(30, 10, 'Fecha: '.$strFecha, 0, false, 'C', 0, '', 0, false, 'T', 'M');
			$this->Cell(30, 10, 'IDEAL.10 S.A.S', 0, false, 'C', 0, '', 0, false, 'T', 'M');
            $this->Cell(20, 10, 'Pagina '.$this->getAliasNumPage().'/'.$this->getAliasNbPages(), 0, false, 'C', 0, '', 0, false, 'T', 'M');
		}
	}

    if($_POST){
        $obj = new legalizacionComisionExportController();
        if($_POST['action'] == "pdf"){
            $obj->exportPdf();
        }
    }

?>
