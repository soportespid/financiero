<?php
    require_once '../../../Librerias/core/Helpers.php';
    require_once '../models/legalizacion_comision_model.php';
    session_start();
    header('Content-Type: application/json');
    class legalizacionController extends legalizacionModel {
        /* crear */
        public function get() {
            if (!empty($_SESSION)) {
                $arrResponse = array(
                   "terceros" => getTerceros(),
                   "obligaciones" => $this->get_obligaciones(),
                   "conceptos" => $this->get_conceptos_contables()
                );
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);    
            }
            die();
        }

        public function getCuentaContable() {
            if (!empty($_SESSION)) {
                $arrResponse = array(
                   "cuenta" => $this->get_cuenta_contable($_POST["codigo"]),
                );
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);    
            }
            die();
        }

        public function save() {
            if (!empty($_SESSION)) {
                $fecha = $_POST["fecha"] != "" ? $_POST["fecha"] : date("Y-m-d");
                $codObligacion = $_POST["codObligacion"] != "" ? $_POST["codObligacion"] : "";
                $valorObligacion = $_POST["valorObligacion"] != "" ? $_POST["valorObligacion"] : 0;
                $detale = $_POST["detalle"] != "" ? $_POST["detalle"] : "";
                $valorLegalizacion = $_POST["valorLegalizacion"] != "" ? $_POST["valorLegalizacion"] : 0;
                $docBeneficiario = $_POST["docBeneficiario"] != "" ? $_POST["docBeneficiario"] : "";
                $codRP = $_POST["codRp"] != "" ? $_POST["codRp"] : "";
                $vigencia = $_POST["vigencia"] != "" ? $_POST["vigencia"] : date("Y");
                $items = json_decode($_POST["items"], true);

                $form = array(
                    "fecha" => $fecha,
                    "codObligacion" => $codObligacion,
                    "valorObligacion" => $valorObligacion,
                    "detalle" => $detale,
                    "valorLegalizacion" => $valorLegalizacion
                );

                $result = $this->insert_cab($form);
                if ($result["id"] > 0) {
                    $this->insert_det($items, $result["consecutivo"], $codRP, $vigencia, $docBeneficiario, $form["codObligacion"]);
                    $arrResponse = array(
                        "id" => $result["consecutivo"],
                        "status" => true,
                        "msg" => "Guardado existoso con el consecutivo $result[consecutivo]",
                    );
                }
                else {
                    $arrResponse = array(
                        "status" => false,
                        "msg" => "Error al guardar!",
                    );
                }
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);    
            }
            die();
        }

        /* buscar */
        public function search() {
            if (!empty($_SESSION)) {

                $arrResponse = array(
                    "data" => $this->get_data_legalizacion(),
                    "fechaBloqueo" => getFechaBlock($_SESSION["cedulausu"])
                );
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);    
            }
            die();
        }

        public function anular() {
            if (!empty($_SESSION)) {
                $fechaBloqueo = getFechaBlock($_SESSION["cedulausu"]);
                $consecutivo = $_POST["consecutivo"];
                $fecha = $_POST["fecha"];
                if ($fecha > $fechaBloqueo) {
                    $request = $this->update_status($consecutivo, "N");

                    if ($request) {
                        $arrResponse = array(
                            "status" => true,
                            "msg" => "Anulado existoso!",
                        );
                    } else {
                        $arrResponse = array(
                            "status" => false,
                            "msg" => "Error al anular!",
                        );
                    }
                } else {
                    $arrResponse = array(
                        "status" => false,
                        "msg" => "Fecha de documento ha sido bloqueda, no se puede anular!"
                    );
                }
                
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);    
            }
            die();
        }

        /* visualizar */
        public function visualizar() {
            if (!empty($_SESSION)) {
                $consectivo = $_POST["consecutivo"];
                $arrResponse = array(
                    "data" => $this->get_data_visualizar($consectivo),
                    "items" => $this->get_data_visualizar_items($consectivo),
                    "consecutivos" => getConsecutivos('teso_legalizacion', 'consecutivo')
                );
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);    
            }
            die();
        }
    }

    if($_POST){
        $obj = new legalizacionController();
        $accion = $_POST["action"];
        
        if ($accion == "get") {
            $obj->get();
        } else if ($accion == "cuentaContable") {
            $obj->getCuentaContable();
        } else if ($accion == "save") {
            $obj->save();
        } else if ($accion == "search") {
            $obj->search();
        } else if ($accion == "anular") {
            $obj->anular();
        } else if ($accion == "visualizar") {
            $obj->visualizar();
        }
    }
?>