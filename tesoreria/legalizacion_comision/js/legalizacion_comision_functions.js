const URL = "tesoreria/legalizacion_comision/controllers/legalizacion_comision_controller.php";
const URLPDF = "tesoreria/legalizacion_comision/controllers/legalizacion_comision_export_controller.php"; 

var app = new Vue({
    el:"#myapp",
    data() {
        return {
            /* general */
            isLoading: false,
            txtSearch: '',
            id: 0,
            tabs: '1',

            /* Crear */
            fecha: '',
            txtDetalle: '',
            codObligacion: '',
            objetoObligacion: '',
            valorObligacion: '',
            beneficiario: '',
            docBeneficiario: '',

            /* Detalle */
            txtDocTercero: '',
            txtNameTercero: '',
            txtCodConcepto: '',
            txtNameConcepto: '',
            txtCuentaContable: '',
            txtValor: '',
            txtValorFormat: '',
            arrDesagregacion: [],
            totalDesagrergacion: 0,
            
            /* modal obligacion */
            modalObligacion: false,
            arrObligaciones: [], arrObligacionesCopy: [],
            txtSearchObligacion: '',
            codRp: '',
            vigencia: '',

            /* modal terceros */
            modalTercero: false,
            arrTerceros: [], arrTercerosCopy: [],
            txtSearchTercero: '',

            /* modal conceptos */
            modalConcepto: false,
            arrConceptos: [], arrConceptosCopy: [],
            txtSearchConcepto: '',

            /* Buscar */
            arrSearch: [], arrSearchCopy: [],
            txtSearch: '',
            fechaBloqueo: '',

            /* Visualizar */
            arrVisualizar: [],
            arrConsecutivos: [],
        }
    },
    mounted() {
        this.intPageVal = this.$refs.pageType.value;
        if(this.intPageVal == 1){
            this.getData();
        }else if(this.intPageVal == 2){
            this.getSearchData();
        }else if(this.intPageVal == 3){
            this.getVisualizar();
        }
    },
    methods: {
        /* metodos para traer información */
        async getData() {
            const formData = new FormData();
            formData.append("action","get");
            this.isLoading = true;
            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();
            this.arrObligaciones = this.arrObligacionesCopy = objData.obligaciones;
            this.arrTercerosCopy = objData.terceros;
            this.arrConceptos = this.arrConceptosCopy = objData.conceptos;
            this.isLoading = false;
        },

        async searchCuentaContable(codigo) {
            const formData = new FormData();
            formData.append("action","cuentaContable");
            formData.append("codigo",codigo);
            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();
            this.txtCuentaContable = objData.cuenta;
        },

        async getSearchData() {
            const formData = new FormData();
            formData.append("action","search");
            this.isLoading = true;
            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();
            this.arrSearch = this.arrSearchCopy = objData.data;
            this.fechaBloqueo = objData.fechaBloqueo;
            this.isLoading = false;
        },

        async getVisualizar() {
            this.id = new URLSearchParams(window.location.search).get('id');
            const formData = new FormData();
            formData.append("action","visualizar");
            formData.append("consecutivo",this.id);
            this.isLoading = true;
            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();
            this.arrVisualizar = objData.data;
            this.arrDesagregacion = objData.items;
            this.arrConsecutivos = objData.consecutivos;
            this.isLoading = false;
        },

        /* metodos para procesar información */
        modalFilter(option) {
            let search = "";

            switch (option) {
                case "obligacion":
                    search = this.txtSearchObligacion.toLowerCase();
                    this.arrObligaciones = [...this.arrObligacionesCopy.filter(e=>e.id_orden.toLowerCase().includes(search) || e.conceptorden.toLowerCase().includes(search) || e.tercero.toLowerCase().includes(search) || e.nombre.toLowerCase().includes(search))];
                    break;

                case "tercero":
                    if (this.txtSearchTercero == "") {
                        this.arrTerceros = [];
                    } else {
                        search = this.txtSearchTercero.toLowerCase();
                        this.arrTerceros = [...this.arrTercerosCopy.filter(e=>e.cedulanit.toLowerCase().includes(search) || e.nombre.toLowerCase().includes(search))];
                    }
                    break;
                    
                case "concepto":
                    search = this.txtSearchConcepto.toLowerCase();
                    this.arrConceptos = [...this.arrConceptosCopy.filter(e=>e.codigo.toLowerCase().includes(search) || e.nombre.toLowerCase().includes(search) )];
                    break;

                case "search":
                    search = this.txtSearch.toLowerCase();
                    this.arrSearch = [...this.arrSearchCopy.filter(e=>e.consecutivo.toLowerCase().includes(search) || e.cod_obligacion.toLowerCase().includes(search) || e.tercero.toLowerCase().includes(search) || e.nomTercero.toLowerCase().includes(search) )];
                    break;

                default:
                    console.log("Error, opción no encontrada");
                    break;
            }    
        },

        inputFilter(option)  {
            let search = "";
            let data = {};
            switch (option) {
                case "obligacion":
                    search = this.codObligacion.toLowerCase();
                    data = {};
                    data = JSON.parse(JSON.stringify(this.arrObligacionesCopy.filter(e=>e.id_orden==search)));       
                    if (data.length > 0) {
                        this.selectItem('obligacion', data[0]);
                    } else {
                        Swal.fire("Atención!","Obligación no encontrada","warning");
                        this.codObligacion = this.objetoObligacion = this.valorObligacion = this.beneficiario = "";
                    }         
                    
                    break;

                case "tercero":
                    search = this.txtDocTercero.toLowerCase();
                    data = {};
                    data = JSON.parse(JSON.stringify(this.arrTercerosCopy.filter(e=>e.cedulanit==search)));    
                    if (data.length > 0) {
                        this.selectItem('tercero', data[0]);
                    } else {
                        Swal.fire("Atención!","El documento del tercero no fue encontrado","warning");
                        this.txtDocTercero = this.txtNameTercero = "";
                    }               
                    break;

                case "concepto":
                    search = this.txtCodConcepto.toLowerCase();
                    data = {};
                    data = JSON.parse(JSON.stringify(this.arrConceptosCopy.filter(e=>e.codigo==search)));       
                    if (data.length > 0) {
                        this.selectItem('concepto', data[0]);
                    } else {
                        Swal.fire("Atención!","Concepto no encontrado","warning");
                        this.txtCodConcepto = this.txtnamConcepto = this.txtCuentaContable = "";
                    }         
                    break;
                
                default:
                    console.log("Error, opción no encontrada");
                    break;
            }    
        },

        selectItem(option, item) {
            switch (option) {
                case "obligacion":
                    this.codObligacion = item.id_orden;
                    this.objetoObligacion = item.conceptorden;
                    this.valorObligacion = item.valor;
                    this.beneficiario = item.tercero + " - " + item.nombre;
                    this.docBeneficiario = item.tercero;
                    this.codRp = item.id_rp;
                    this.vigencia = item.vigencia;
                    this.txtSearchObligacion = "";
                    this.modalObligacion = false;
                    this.arrObligaciones = this.arrObligacionesCopy;
                    break;

                case "tercero":
                    this.txtDocTercero = item.cedulanit;
                    this.txtNameTercero = item.nombre;
                    this.txtSearchTercero = "";
                    this.modalTercero = false;
                    this.arrTerceros = [];
                    break;

                case "concepto":
                    this.txtCodConcepto = item.codigo;
                    this.txtNameConcepto = item.nombre;
                    this.searchCuentaContable(item.codigo);
                    this.txtSearchConcepto = "";
                    this.modalConcepto = false;
                    this.arrConceptos = this.arrConceptosCopy;
                    break;
                default:
                    console.log("Error, opción no encontrada");
                    break;
            }
        },

        add() {
            if (this.txtDocTercero == "" || this.txtNameTercero == "" || this.txtCodConcepto == "" || this.txtNameConcepto == "" || this.txtCuentaContable == "" || this.txtValor == "") {
                Swal.fire("Atención!","Todos los campos son obligatorios","warning");
                return;
            }

            this.arrDesagregacion.push({
                documento: this.txtDocTercero,
                nombre: this.txtNameTercero,
                codigo: this.txtCodConcepto,
                concepto: this.txtNameConcepto,
                cuenta: this.txtCuentaContable,
                valor: this.txtValor
            });

            this.txtDocTercero = this.txtNameTercero = this.txtCodConcepto = this.txtNameConcepto = this.txtCuentaContable = this.txtValor = this.txtValorFormat = "";

            this.totalDesagrergacion = this.arrDesagregacion.reduce((acc, item) => acc + parseFloat(item.valor), 0);
        },

        delItem(index) {
            this.arrDesagregacion.splice(index, 1);
            this.totalDesagrergacion = this.arrDesagregacion.reduce((acc, item) => acc + parseFloat(item.valor), 0);
        },
        /* Metodos para guardar o actualizar información */
        async save() {
            if (this.fecha == "" || this.codObligacion == "" || this.objetoObligacion == "" || this.valorObligacion == "" || this.beneficiario == "" || this.txtDetalle == "") {
                Swal.fire("Atención!","Todos los campos son obligatorios","warning");
                return;
            }

            if (this.arrDesagregacion.length == 0) {
                Swal.fire("Atención!","Debe agregar al menos un item a la desagregación","warning");
                return;
            }

            if (parseFloat(this.totalDesagrergacion) > parseFloat(this.valorObligacion)) {
                Swal.fire("Atención!","El valor total de la desagregación no puede ser mayor al valor de la obligación","warning");
                return;
            }

            const formData = new FormData();
            formData.append("action","save");
            formData.append("fecha",this.fecha);
            formData.append("codObligacion",this.codObligacion);
            formData.append("docBeneficiario",this.docBeneficiario);
            formData.append("valorObligacion",this.valorObligacion);
            formData.append("detalle",this.txtDetalle);
            formData.append("valorLegalizacion", this.totalDesagrergacion);
            formData.append("codRp", this.codRp);
            formData.append("vigencia", this.vigencia);
            formData.append("items", JSON.stringify(this.arrDesagregacion));

            Swal.fire({
                title:"¿Estás segur@ de guardar?",
                text:"",
                icon: 'warning',
                showCancelButton:true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText:"Sí, guardar",
                cancelButtonText:"No, cancelar"
            }).then(async function(result) {
                if(result.isConfirmed) {
                    const response = await fetch(URL,{method:"POST",body:formData});
                    const objData = await response.json();

                    if (objData.status) {
                        Swal.fire("Guardado",objData.msg,"success");
                        if(objData.id){
                            setTimeout(function(){
                                window.location.href="teso-visualizarLegalizacionComision"+'?id='+objData.id;
                                // window.location.reload();
                            },1500);
                        }
                    }
                    else {
                        Swal.fire("Error!",objData.msg,"warning");
                    }
                }
            });
        },

        async anulaDoc (index, consecutivo, fecha) {
            const formData = new FormData();
            formData.append("action","anular");
            formData.append("consecutivo",consecutivo);
            formData.append("fecha",fecha);

            Swal.fire({
                title:"¿Estás segur@ de anular el documento?",
                text:"",
                icon: 'warning',
                showCancelButton:true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText:"Sí, anular",
                cancelButtonText:"No, cancelar"
            }).then(async function(result) {
                if(result.isConfirmed) {
                    const response = await fetch(URL,{method:"POST",body:formData});
                    const objData = await response.json();
                    if (objData.status) {
                        Swal.fire("Guardado",objData.msg,"success");
                        app.arrSearch[index]["estado"] = "N";
                        app.arrSearchCopy[index]["estado"] = "N";
                    }
                    else {
                        Swal.fire("Error!",objData.msg,"warning");
                    }
                }
            });
        },

        /* Formatos para mostrar información */
        formatInputNumber(conFormato, sinFormato) {
            this[conFormato] = this.formatNumber(this[conFormato]);
            this[sinFormato] = this[conFormato].replace(/[^0-9.]/g, "");
        },

        formatNumber(number) {
            if (!number) return ""; // Si no hay entrada, retorna vacío
    
            // Elimina caracteres no válidos (excepto números y el punto decimal)
            number = number.replace(/[^0-9.]/g, "");
    
            // Divide la parte entera y decimal
            const [integerPart, decimalPart] = number.split(".");
    
            // Formatear la parte entera con separador de miles
            const formattedInteger = integerPart.replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    
            // Si hay parte decimal, la une; si no, devuelve solo la parte entera
            return decimalPart !== undefined
                ? `${formattedInteger}.${decimalPart.slice(0, 2)}` // Limita a 2 decimales si es necesario
                : formattedInteger;
        },

        viewFormatNumber: function(valor){
            return new Intl.NumberFormat("en-US", {style: "currency", currency: "USD"}).format(valor);  
        },

        formatFecha: function(fecha) {
            // Separar la fecha en componentes
            const parts = fecha.split('-');
            if (parts.length !== 3) {
                throw new Error('Formato de fecha inválido. Use Y-m-d.');
            }
            const year = parts[0];
            const month = parts[1];
            const day = parts[2];
        
            // Retornar la fecha en el formato deseado
            return `${day}/${month}/${year}`;
        },

        nextItem:function(type){
            let id = this.id;
            let index = this.arrConsecutivos.findIndex(function(e){return e.id == id});
            if(type=="next" && this.arrConsecutivos[++index]){
                id = this.arrConsecutivos[index++].id;
            }else if(type=="prev" && this.arrConsecutivos[--index]){
                id = this.arrConsecutivos[index--].id;
            }
            window.location.href="teso-visualizarLegalizacionComision"+'?id='+id;
        },

        /* enviar informacion excel o pdf */
        pdf: function() {
            const form = document.createElement("form");
            form.method = "post";
            form.target = "_blank";
            form.action = URLPDF;       
            
            function addField(name,value){
                const input = document.createElement("input");
                input.type="hidden";
                input.name=name;
                input.value = value;
                form.appendChild(input);
            }


            addField("action", "pdf");
            addField("data", JSON.stringify(this.arrVisualizar));
            addField("items", JSON.stringify(this.arrDesagregacion));
            
            document.body.appendChild(form);
            form.submit();
            document.body.removeChild(form);
        },
    },
    computed:{

    }
})
