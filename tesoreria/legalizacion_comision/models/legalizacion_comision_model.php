<?php
    require_once '../../../Librerias/core/Helpers.php';
    require_once '../../../Librerias/core/Mysql.php';
    require_once '../../../funciones.inc';
    session_start();
    // ini_set('display_errors', '1');
    // ini_set('display_startup_errors', '1');
    // error_reporting(E_ERROR);
    class legalizacionModel extends Mysql {
        public $modulo = 4;
        public $tipo = 'LE';
        public $tipoComprobante = 48;
        private $cedula;
        private $vigencia;

        function __construct(){
            parent::__construct();
            $this->cedula = $_SESSION["cedulausu"];
            $this->vigencia = getVigenciaUsuario($this->cedula);
        }

        /* Read data */
        public function get_obligaciones() {
            $sql = "SELECT orden.id_orden, 
            orden.tercero, 
            orden.conceptorden, 
            orden.valorpagar - orden.valorretenciones AS valor,
            orden.vigencia,
            orden.id_rp,
            CASE WHEN t.razonsocial IS NULL OR t.razonsocial = ''
            THEN CONCAT(t.nombre1,' ',t.nombre2,' ',t.apellido1,' ',t.apellido2)
            ELSE t.razonsocial END AS nombre
            FROM tesoordenpago AS orden
            INNER JOIN terceros AS t
            ON orden.tercero = t.cedulanit
            LEFT JOIN teso_legalizacion AS l ON orden.id_orden = l.cod_obligacion AND l.estado != 'N'
            WHERE orden.tipo_mov = '201'
            AND orden.estado != 'R'
            AND l.cod_obligacion IS NULL";   
            $data = $this->select_all($sql);
            return $data;
        }

        public function get_conceptos_contables() {
            $sql = "SELECT codigo, nombre FROM conceptoscontables WHERE modulo = $this->modulo AND tipo = '$this->tipo' ORDER BY CAST(codigo AS INT)";
            $data = $this->select_all($sql);
            return $data;
        }   

        public function get_cuenta_contable($codigo) {
            $sql = "SELECT cuenta FROM conceptoscontables_det WHERE codigo = '$codigo' AND modulo = $this->modulo AND tipo = '$this->tipo'";
            $data = $this->select($sql);
            return $data["cuenta"];
        }

        public function get_data_legalizacion() {
            $sql = "SELECT l.id, l.consecutivo, l.fecha, l.cod_obligacion, l.valor_obligacion, l.valor_legalizacion, l.detalle, l.estado, orden.tercero
            FROM teso_legalizacion AS l
            INNER JOIN tesoordenpago AS orden
            ON l.cod_obligacion = orden.id_orden
            ORDER BY l.consecutivo DESC";
            $data = $this->select_all($sql);
            foreach ($data as $key => $value) {
                $data[$key]["nomTercero"] = getNombreTercero($value["tercero"]);
            }
            return $data;
        }

        public function get_data_visualizar($consecutivo) {
            $sql = "SELECT l.consecutivo, l.fecha, l.cod_obligacion, orden.conceptorden AS objeto, l.valor_obligacion, l.valor_legalizacion, orden.tercero, l.detalle, l.estado
            FROM teso_legalizacion AS l
            INNER JOIN tesoordenpago AS orden
            ON l.cod_obligacion = orden.id_orden
            WHERE l.consecutivo = $consecutivo";
            $data = $this->select($sql);
            $data["nomTercero"] = getNombreTercero($data["tercero"]);
            return $data;
        }

        public function get_data_visualizar_items($consecutivo) {
            $sql = "SELECT tercero AS documento, concepto AS codigo, cuenta_contable AS cuenta, valor FROM teso_legalizacion_det WHERE consec_legalizacion = $consecutivo";
            $data = $this->select_all($sql);
            foreach ($data as $key => $value) {
                $data[$key]["nombre"] = getNombreTercero($value["documento"]);
                $data[$key]["concepto"] = $this->get_name_concepto_contable($value["codigo"]);
            }
            return $data;
        }

        private function get_name_concepto_contable($codigo) {
            $sql = "SELECT nombre FROM conceptoscontables WHERE modulo = $this->modulo AND tipo = '$this->tipo' AND codigo = '$codigo'";
            $data = $this->select($sql);
            return $data["nombre"];
        }

        /* Create data */
        public function insert_cab($form) {
            $consecutivo = searchConsec("teso_legalizacion", "consecutivo");
            $sql = "INSERT INTO teso_legalizacion (consecutivo, fecha, cod_obligacion, valor_obligacion, valor_legalizacion, detalle, estado) VALUES (?, ?, ?, ?, ?, ?, ?)";
            $data = array($consecutivo, $form["fecha"], $form["codObligacion"], $form["valorObligacion"], $form["valorLegalizacion"], $form["detalle"], "S");
            $id = $this->insert($sql, $data);
            if ($id > 0) {
                $this->insert_cont_cab($consecutivo, $form["fecha"], $form["detalle"]);
                $this->create_auditoria("Crear", $consecutivo);
                return array("id" => $id, "consecutivo" => $consecutivo);
            } else {
                return false;
            }
        }

        public function insert_det($items, $consecutivo, $codRp, $vigencia, $beneficiario, $obligacion) {
            $sql = "INSERT INTO teso_legalizacion_det (consec_legalizacion, tercero, concepto, cuenta_contable, valor) VALUES (?, ?, ?, ?, ?)";
            foreach ($items as $item) {
                $data = array($consecutivo, $item["documento"], $item["codigo"], $item["cuenta"], $item["valor"]);
                $id = $this->insert($sql, $data);
                if ($id > 0) {
                    $this->insert_cont_det($item["documento"], $item["cuenta"], $item["concepto"], $item["valor"], $consecutivo, $codRp, $vigencia, $beneficiario, $obligacion);
                }
            }
        }

        private function insert_cont_cab($consecutivo, $fecha, $detalle) {
            $sql = "INSERT INTO comprobante_cab (numerotipo, tipo_comp, fecha, concepto, estado) VALUES (?, ?, ?, ?, ?)";   
            $data = array($consecutivo, $this->tipoComprobante, $fecha, $detalle, "1");
            $this->insert($sql, $data);
        }

        private function insert_cont_det($tercero, $cuenta, $concepto, $valor, $consecutivo, $rp, $vigencia, $docBeneficiario, $obligacion) {
            $sqlrObligacion = "SELECT cuenta_deb FROM tesoordenpago_cuenta WHERE id_orden = $obligacion and estado = 'S'";
            //$sql_radicar_destino = "SELECT cuenta_debito, seccion_presupuestal FROM ccpetdc_detalle WHERE idrp = $rp AND vigencia = $vigencia";

            $row_radicar_destino = $this->select($sqlrObligacion);
            $cuentaCred = $row_radicar_destino["cuenta_deb"];

            $sql_centro_costo = "SELECT id_cc FROM centrocostos_seccionpresupuestal WHERE id_sp  = '$row_radicar_destino[seccion_presupuestal]'";
            $row_centro_costo = $this->select($sql_centro_costo);
            $cc = $row_centro_costo["id_cc"];

            $sql = "INSERT INTO comprobante_det (id_comp, cuenta, tercero, centrocosto, detalle, valdebito, valcredito, estado, vigencia) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";

            //Debito
            $dataDeb = array("$this->tipoComprobante $consecutivo", $cuenta, $tercero, "$cc", "Legalizacion comision por concepto $concepto", $valor, 0, "S", "$this->vigencia");
            // Credito
            $dataCred = array("$this->tipoComprobante $consecutivo", $cuentaCred, $docBeneficiario, "$cc", "Legalizacion comision por concepto $concepto", 0, $valor, "S", "$this->vigencia");

            $this->insert($sql, $dataDeb);
            $this->insert($sql, $dataCred);
        }

        private function create_auditoria(string $accion, int $consecutivo) {
            $sql_funcion = "SELECT id FROM teso_funciones WHERE nombre = 'Legalizacion Comision'";
            $funcion = $this->select($sql_funcion);

            insertAuditoria("teso_auditoria","teso_funciones_id",$funcion["id"],$accion,$consecutivo);
        }
        /* Update data */
        public function update_status($consecutivo, $status) {
            $sql = "UPDATE teso_legalizacion SET estado = ? WHERE consecutivo = $consecutivo";
            $request = $this->update($sql, ["$status"]);

            if ($status == "N") {
                $sql_cont = "UPDATE comprobante_cab SET estado = ? WHERE numerotipo = $consecutivo AND tipo_comp = $this->tipoComprobante";
                $this->update($sql_cont, ["0"]);
            }

            $this->create_auditoria("Anular", $consecutivo);

            return $request;
        }
        
        /* Delate data */
       
    }
?>