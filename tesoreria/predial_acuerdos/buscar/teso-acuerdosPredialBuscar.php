<?php
    require_once '../../../comun.inc';
    require '../../../funciones.inc';
    require '../../../funcionesSP.inc.php';
    /*ini_set('display_errors', '1');
    ini_set('display_startup_errors', '1');
    error_reporting(E_ALL);*/
    session_start();
    //dep($_POST);exit;
    if($_POST){
        $obj = new Plantilla();
        if($_POST['action']=="get"){
            $obj->getData();
        }else if($_POST['action']=="change"){
            $obj->delData();
        }
    }

    class Plantilla{
        private $linkbd;
        private $arrData;
        public function __construct() {
            $this->linkbd = conectar_v7();
            $this->linkbd->set_charset("utf8");
        }
        public function getData(){
            if(!empty($_SESSION)){
                $request = $this->selectData();
                echo json_encode($request,JSON_UNESCAPED_UNICODE);
            }
            die();
        }
        public function delData(){
            if(!empty($_SESSION)){
                $id = intval($_POST['id']);
                $estado = $_POST['estado'];

                $request = $this->selectTotalAbonos($id);
                if($request == 0){
                    $sql = "UPDATE tesoacuerdopredial SET estado = '$estado' WHERE idacuerdo = $id";
                    $request = mysqli_query($this->linkbd,$sql);
                    insertAuditoria("teso_auditoria","teso_funciones_id",4,"Anular",$request);
                    $arrResponse = array("status"=>true);
                }else{
                    $arrResponse = array("status"=>false,"msg"=>"Este acuerdo ya tiene abonos, deberá reversarlos antes de anular este acuerdo.");
                }
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
            die();
        }
        public function selectData(){
            $sql = "SELECT *,DATE_FORMAT(fecha_acuerdo,'%d/%m/%Y') as fecha FROM tesoacuerdopredial ORDER BY idacuerdo DESC";
            $arrData = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
            $total = count($arrData);
            if($total > 0){
                for ($i=0; $i < $total ; $i++) {
                    $cont = 0;
                    $totalCuota = 0;
                    $abono = $this->selectTotalAbonos($arrData[$i]['idacuerdo']);
                    $arrCuotas = $this->selectCuotas($arrData[$i]['idacuerdo']);
                    $cuotas = count($arrCuotas);
                    foreach ($arrCuotas as $cuota) {
                        $totalCuota+=$cuota['valor_pago'];
                        if($abono >=$totalCuota){
                            $cont++;
                        }
                    }
                    $arrData[$i]['cuotas'] = $cuotas > $arrData[$i]['cuotas'] ? $cuotas :  $arrData[$i]['cuotas'];
                    $arrData[$i]['cuota_pagada'] = $cont;
                    $arrData[$i]['abono'] = $abono;
                }
            }
            $arrResponse = array("status"=>true,"data"=>$arrData,"total"=>$total);
            return $arrResponse;
        }
        public function selectCuotas($id){
            $sql = "SELECT cuota,valor_pago FROM tesoacuerdopredial_pagos WHERE idacuerdo = $id";
            $request = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
            return $request;
        }
        public function selectTotalAbonos($id){
            $sql = "SELECT COALESCE(sum(valortotal),0) as total FROM tesoabono WHERE idacuerdo=$id AND tipomovimiento = '201' AND estado = 'S'";
            $request = mysqli_query($this->linkbd,$sql)->fetch_assoc()['total'];
            return $request;
        }
    }
?>
