const URL ='tesoreria/predial_acuerdos/buscar/teso-acuerdosPredialBuscar.php';

var app = new Vue({
    el:"#myapp",
    data() {
        return {
            isLoading:false,
            arrData:[],
            arrDataCopy:[],
            txtResults:0,
            txtSearch:"",
        }
    },
    mounted() {
        this.getData();
    },
    methods: {
        getData: async function(){
            const formData = new FormData();
            formData.append("action","get");
            this.isLoading=true;
            const response = await fetch(URL,{method:"POST", body:formData});
            const objData = await response.json();
            this.arrData = objData.data;
            this.arrDataCopy = objData.data;
            this.txtResults = objData.total;
            this.isLoading=false;
        },
        search:function(){
            let search = this.txtSearch.toLowerCase();
            this.arrDataCopy = [...this.arrData.filter(e=>e.tercero.toLowerCase().includes(search) ||
                e.codcatastral.toLowerCase().includes(search) || e.idacuerdo.toLowerCase().includes(search))];
            this.txtResults = this.arrDataCopy.length;
        },
        editItem:function(id){
            window.location.href='teso-acuerdosPredialEditar.php?id='+id;
        },
        changeStatus:function(item){
            const vueThis = this;
            Swal.fire({
                title:"¿Estás segur@ de cambiar el estado?",
                text:"",
                icon: 'warning',
                showCancelButton:true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText:"Sí",
                cancelButtonText:"No, cancelar"
            }).then(async function(result){
                if(result.isConfirmed){
                    const formData = new FormData();
                    formData.append("action","change");
                    formData.append("id",item.idacuerdo);
                    formData.append("estado","N");
                    const response = await fetch(URL,{method:"POST",body:formData});
                    const objData = await response.json();
                    if(objData.status){
                        let index = vueThis.arrData.findIndex(e => e.idacuerdo == item.idacuerdo);
                        vueThis.arrData[index].estado = 'N';
                        vueThis.search();
                        Swal.fire("Estado actualizado","","success");
                    }else{
                        Swal.fire("Error",objData.msg,"error");
                    }
                }
            });

        },
        formatNumero: function(valor){
            return new Intl.NumberFormat("en-US", {style: "currency", currency: "USD"}).format(valor);
        },
    },
    computed:{

    }
})
