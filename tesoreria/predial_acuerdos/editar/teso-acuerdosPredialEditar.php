<?php
    require_once '../../../comun.inc';
    require '../../../funciones.inc';
    require '../../../funcionesSP.inc.php';
    date_default_timezone_set("America/Bogota");
    /*ini_set('display_errors', '1');
    ini_set('display_startup_errors', '1');
    error_reporting(E_ALL);*/
    session_start();
    if($_POST){
        $obj = new Plantilla();
        if($_POST['action']=="get"){
            $obj->getData();
        }else if($_POST['action'] =="save"){
            $obj->save();
        }
    }

    class Plantilla{
        private $linkbd;
        private $arrData;
        public function __construct() {
            $this->linkbd = conectar_v7();
            $this->linkbd->set_charset("utf8");
        }
        public function getData(){
            if(!empty($_SESSION)){
                $id = intval(strClean($_POST['codigo']));
                $request = $this->selectCabecera($id);
                if(!empty($request)){
                    $cabecera = $request;
                    $predio = $this->selectInfoPredio($cabecera['codcatastral'],$cabecera['tercero']);
                    $detalle = $this->selectDetalle($cabecera['idacuerdo'],$predio['id']);
                    $cuotas = $this->selectCuotas($cabecera['idacuerdo']);
                    $arrData = array(
                        "consecutivo"=>$cabecera['idacuerdo'],
                        "consecutivos"=>$this->selectConsecutivos(),
                        "status"=>true,
                        "data"=>array(
                            "cabecera"=>$cabecera,
                            "predio"=>$predio,
                            "detalle"=>$detalle,
                            "cuotas"=>$cuotas
                        )
                    );
                }else{
                    $arrData = array("status"=>false,"consecutivo"=>$this->selectConsecutivo());
                }
                echo json_encode($arrData,JSON_UNESCAPED_UNICODE);
            }
            die();
        }
        public function selectCabecera($id){
            $sql = "SELECT idacuerdo,codcatastral,vigencia,cuotas,valor_pago,estado,tercero, DATE_FORMAT(fecha_acuerdo,'%d/%m/%Y')  as fecha
            FROM tesoacuerdopredial WHERE idacuerdo = $id";
            $request = mysqli_query($this->linkbd,$sql)->fetch_assoc();
            return $request;
        }
        public function selectDetalle($id,$idPredio){
            $sql = "SELECT
            tasa as tasa_por_mil,
            predial,
            descuento as predial_descuento,
            intpredial as predial_intereses,
            descuenint as predial_descuento_intereses,
            bomberil,
            intbomberil as bomberil_intereses,
            ambiente as ambiental,
            intambiente as ambiental_intereses,
            valtotal as total_liquidacion,
            vigencia,
            diasmora as dias_mora,
            estado,
            alumbrado
            FROM tesoacuerdopredial_det
            WHERE idacuerdo = $id ORDER BY vigencia DESC";
            $request = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);

            $total = count($request);
            for ($i=0; $i < $total ; $i++) {
                $sql = "SELECT valor_avaluo FROM predio_avaluos WHERE predio_id = $idPredio AND vigencia = '{$request[$i]['vigencia']}'";
                $request[$i]['valor_avaluo'] = intval(mysqli_query($this->linkbd,$sql)->fetch_assoc()['valor_avaluo']);
                $request[$i]['predial'] = intval($request[$i]['predial']);
                $request[$i]['predial_descuento'] = intval($request[$i]['predial_descuento']);
                $request[$i]['predial_intereses'] = intval($request[$i]['predial_intereses']);
                $request[$i]['bomberil'] = intval($request[$i]['bomberil']);
                $request[$i]['bomberil_intereses'] = intval($request[$i]['bomberil_intereses']);
                $request[$i]['ambiental'] = intval($request[$i]['ambiental']);
                $request[$i]['ambiental_intereses'] = intval($request[$i]['ambiental_intereses']);
                $request[$i]['total_liquidacion'] = intval($request[$i]['total_liquidacion']);
                $request[$i]['alumbrado'] = intval($request[$i]['alumbrado']);
            }
            return $request;
        }
        public function selectCuotas($id){
            $sql = "SELECT *,DATE_FORMAT(fecha_pago,'%d/%m/%Y') as fecha FROM tesoacuerdopredial_pagos WHERE idacuerdo = $id";
            $request = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
            return $request;
        }
        public function selectInfoPredio($codcatastral,$tercero){
            $sql="SELECT
            p.id,
            p.codigo_catastro,
            p.total,
            pp.documento,
            pp.nombre_propietario as nombre_propietario,
            pp.orden,
            pa.valor_avaluo as avaluo,pa.tasa_por_mil as tasa,
            pa.vigencia,
            tp.direccion,tp.hectareas,
            tp.metros_cuadrados,
            tp.area_construida,
            tp.created_at,
            de.nombre as destino_economico,
            cd.codigo as codigo_destino_economico,
            pt.nombre as predio_tipo,
            pt.codigo as predio_tipo_codigo
            FROM predios p
            LEFT JOIN predio_propietarios pp ON p.id = pp.predio_id AND (pp.documento = '$tercero' OR pp.predio_id = p.id)
            INNER JOIN predio_avaluos pa ON p.id = pa.predio_id
            INNER JOIN predio_informacions tp ON p.id = tp.predio_id
            INNER JOIN predio_tipos pt ON tp.predio_tipo_id = pt.id
            INNER JOIN codigo_destino_economicos cd ON cd.id = tp.codigo_destino_economico_id
            INNER JOIN destino_economicos de ON de.id = cd.destino_economico_id
            WHERE p.codigo_catastro = '$codcatastral'
            AND tp.created_at = (SELECT MAX(created_at) FROM predio_informacions WHERE predio_id = p.id)
            AND pa.vigencia = (SELECT MAX(vigencia) FROM predio_avaluos WHERE predio_id = p.id)
            GROUP BY p.id";
            $request = mysqli_query($this->linkbd,$sql)->fetch_assoc();
            return $request;
        }
        public function selectConsecutivo(){
            $sql = "SELECT MAX(idacuerdo) as id FROM tesoacuerdopredial";
            $request = mysqli_query($this->linkbd,$sql)->fetch_assoc()['id'];
            return $request;
        }
        public function selectConsecutivos(){
            $sql = "SELECT idacuerdo FROM tesoacuerdopredial";
            $request = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
            return $request;
        }
    }
?>
