<?php
    require_once '../../../comun.inc';
    require '../../../funciones.inc';
    require '../../../funcionesSP.inc.php';

    require '../../../vendor/autoload.php';

    use Laravel\Route;
    use Laravel\DB;

    date_default_timezone_set("America/Bogota");
    /*ini_set('display_errors', '1');
    ini_set('display_startup_errors', '1');
    error_reporting(E_ALL);*/
    session_start();
    if($_POST){
        $obj = new Plantilla();
        if($_POST['action']=="get"){
            $obj->getData();
        }else if($_POST['action'] =="save"){
            $obj->save();
        }
    }

    class Plantilla{
        private $linkbd;
        private $arrData;
        private $strVigencia;
        public function __construct() {
            $this->linkbd = conectar_v7();
            $this->linkbd->set_charset("utf8");
            $this->strVigencia = date('Y');
        }
        public function save(){
            if(!empty($_SESSION)){
                if(empty($_POST['data'])){
                    $arrResponse = array("status"=>false,"msg"=>"Error de datos");
                }else{
                    $arrData = json_decode($_POST['data'],true);
                    $request = $this->insertData($arrData);
                    if(is_numeric($request) && $request > 0){
                        $arrResponse = array("status"=>true,"msg"=>"Datos guardados","consecutivo"=>$this->selectConsecutivo());
                    }else if($request == "existe" ){
                        $arrResponse = array("status"=>false,"msg"=>"Este predio ya tiene un acuerdo de pago. Anular el anterior si debe generar uno nuevo.");
                    }else{
                        $arrResponse = array("status"=>false,"msg"=>"Ha ocurrido un error, inténtelo de nuevo.");
                    }
                }
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
            die();

        }
        public function getData(){
            if(!empty($_SESSION)){
                $arrResponse = $this->selectEstadosCuentas();
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
            die();
        }
        public function insertData(array $data){
            $arrCuotas = $data['cuotas'];
            $arrVigencias = $data['periodos'];

            $sql = "SELECT * FROM tesoacuerdopredial WHERE codcatastral = '{$data['codigo']}' AND estado = 'S' AND vigencia = '$data[vigencia]'";
            $request = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
            if(empty($request)){
                $sql = "SELECT documento FROM predio_propietarios WHERE predio_id = $data[id_predio] AND orden = 1";
                $data['documento'] = mysqli_query($this->linkbd,$sql)->fetch_assoc()['documento'];
                $date = date("Y-m-d");

                $sql = "INSERT INTO tesoacuerdopredial(codcatastral,vigencia,tipinteres,cuotas,fecha_acuerdo,valor_pago,estado,tercero,recibo,usuario)
                VALUES(
                    '{$data['codigo']}',
                    '{$data['vigencia']}',
                    'C',
                    {$data['total_cuotas']},
                    '{$date}',
                    {$data['total']},
                    'S',
                    '{$data['documento']}',
                    '{$data['estado_cuenta']}',
                    '{$_SESSION['cedulausu']}'
                )";
                mysqli_query($this->linkbd,$sql);
                $request = mysqli_insert_id($this->linkbd);
                $id = $this->selectConsecutivo();
                $this->insertCuotas($id,$arrCuotas);
                $this->insertVigencias($id,$arrVigencias);
                insertAuditoria("teso_auditoria","teso_funciones_id",4,"Crear",$request);
            }else{
                $request = "existe";
            }
            return $request;
        }
        public function insertVigencias($id,$data){
            foreach ($data as $c) {
                $predial = $c['predial']+$c['predial_descuento'];
                $sql = "INSERT INTO tesoacuerdopredial_det(idacuerdo,predial,intpredial,descuenint,bomberil,intbomberil,
                ambiente,intambiente,descuento,valtotal,tasa,vigencia,estado,diasmora,alumbrado)
                VALUES(
                    $id,
                    $predial,
                    {$c['predial_intereses']},
                    {$c['predial_descuento_intereses']},
                    {$c['bomberil']},
                    {$c['bomberil_intereses']},
                    {$c['ambiental']},
                    {$c['ambiental_intereses']},
                    {$c['predial_descuento']},
                    {$c['total_liquidacion']},
                    {$c['tasa_por_mil']},
                    '{$c['vigencia']}',
                    'S',
                    '{$c['dias_mora']}',
                    '{$c['alumbrado']}'
                )";
                mysqli_query($this->linkbd,$sql);
            }
        }
        public function insertCuotas($id,$data){
            foreach ($data as $c) {
                $sql = "INSERT INTO tesoacuerdopredial_pagos(cuota,idacuerdo,fecha_pago,valor_pago,porcentaje_valor,estado)
                VALUES(
                    {$c['cuota']},
                    $id,
                    '{$c['fecha']}',
                    {$c['valor']},
                    1,
                    'S'
                )";
                mysqli_query($this->linkbd,$sql);
            }
        }

        public function selectConsecutivo(){
            $sql = "SELECT MAX(idacuerdo) as id FROM tesoacuerdopredial";
            $request = mysqli_query($this->linkbd,$sql)->fetch_assoc()['id'];
            return $request;
        }
        /* public function selectEstadosCuentas(){
            $arrCuentas = [];
            $arrGrupoCuentas = [];
            $arrIdCuentas = [];
            $arrData = [];
            $fecha_actual = date("Y-m-d");
            $sql = "SELECT id,data,created_at FROM estado_cuentas WHERE DATE(created_at) = '$fecha_actual' ";
            $request = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
            $total = count($request);
            for ($i=0; $i < $total ; $i++) {
                $data = json_decode($request[$i]['data'],true);
                $data['id_estado_cuenta'] = $request[$i]['id'];
                $data['fecha'] = $request[$i]['created_at'];
                $id = $data['id'];
                array_push($arrCuentas,$data);
                if(count($arrIdCuentas) > 0){
                    $arrValid = array_values(array_filter($arrIdCuentas,function($e) use($id){return $e == $id;}));
                    if(empty($arrValid)){
                        array_push($arrIdCuentas,$id);
                    }
                }else{
                    array_push($arrIdCuentas,$id);
                }

            }
            $arrGrupoCuentas = $this->groupCuentas($arrIdCuentas,$arrCuentas);
            $arrData = $this->filterCuentasRecientes($arrGrupoCuentas);
            return $arrData;
        } */

        /**
         * Selecciona y procesa los estados de cuenta del día actual
         *
         * @return array Array con los estados de cuenta procesados
         * @throws \Exception Si hay errores en la base de datos o procesamiento
         */
        public function selectEstadosCuentas(): array
        {
            $tenant_db = null;
            $stmt = null;

            try {
                // Obtener conexión del tenant
                $tenant_db = DB::connectWithDomain(Route::currentDomain());
                
                if (!$tenant_db) {
                    throw new \Exception("Error de conexión con la base de datos.");
                }

                $fecha_actual = date('Y-m-d');
                
                // Usar prepared statement para prevenir SQL injection
                $sql = "SELECT id, data, created_at 
                        FROM estado_cuentas 
                        WHERE DATE(created_at) = ?
                        ORDER BY created_at DESC";
                        
                $stmt = $tenant_db->prepare($sql);
                
                if (!$stmt) {
                    throw new \Exception("Error preparando la consulta: " . $tenant_db->error);
                }
                
                $stmt->bind_param('s', $fecha_actual);
                
                if (!$stmt->execute()) {
                    throw new \Exception("Error ejecutando la consulta: " . $stmt->error);
                }
                
                $result = $stmt->get_result();
                
                if (!$result) {
                    throw new \Exception("Error obteniendo resultados: " . $tenant_db->error);
                }

                $arrCuentas = [];
                $arrIdCuentas = [];

                // Procesar resultados
                while ($row = $result->fetch_assoc()) {
                    $data = json_decode($row['data'], true);
                    
                    if (json_last_error() !== JSON_ERROR_NONE) {
                        error_log("Error decodificando JSON para ID {$row['id']}: " . json_last_error_msg());
                        continue;
                    }

                    // Agregar campos adicionales
                    $data['id_estado_cuenta'] = $row['id'];
                    $data['fecha'] = $row['created_at'];
                    
                    if (!isset($data['id'])) {
                        error_log("ID no encontrado en los datos para estado_cuenta ID {$row['id']}");
                        continue;
                    }

                    $id = $data['id'];
                    $arrCuentas[] = $data;
                    $arrIdCuentas[] = $id;
                }

                // Eliminar IDs duplicados
                $arrIdCuentas = array_values(array_unique($arrIdCuentas));

                // Procesar los datos
                $arrGrupoCuentas = $this->groupCuentas($arrIdCuentas, $arrCuentas);
                $arrData = $this->filterCuentasRecientes($arrGrupoCuentas);

                return $arrData;

            } catch (\Exception $e) {
                error_log("Error en selectEstadosCuentas: " . $e->getMessage());
                throw $e;

            } finally {
                // Liberar recursos
                if ($stmt) {
                    $stmt->close();
                }
                if ($tenant_db) {
                    $tenant_db->close();
                }
            }
        }

        /**
         * Agrupa las cuentas por ID y las ordena por fecha
         *
         * @param array $arrIdCuentas Array de IDs únicos
         * @param array $arrCuentas Array de cuentas
         * @return array Array de cuentas agrupadas y ordenadas
         */
        public function groupCuentas(array $arrIdCuentas, array $arrCuentas): array
        {
            try {
                $arrData = [];

                foreach ($arrIdCuentas as $id) {
                    // Filtrar cuentas por ID
                    $arrTemp = array_values(array_filter(
                        $arrCuentas,
                        fn($e) => $e['id'] == $id
                    ));

                    // Ordenar por fecha descendente
                    usort($arrTemp, fn($a, $b) => 
                        strtotime($b['fecha']) - strtotime($a['fecha'])
                    );

                    $arrData[] = $arrTemp;
                }

                return $arrData;

            } catch (\Exception $e) {
                error_log("Error en groupCuentas: " . $e->getMessage());
                throw $e;
            }
        }

        /**
         * Filtra las cuentas más recientes y añade información adicional
         *
         * @param array $data Array de cuentas agrupadas
         * @return array Array de cuentas procesadas
         * @throws \Exception Si hay errores en la base de datos
         */
        public function filterCuentasRecientes(array $data): array
        {
            $tenant_db = null;
            $stmt = null;

            try {
                $tenant_db = DB::connectWithDomain(Route::currentDomain());
                
                if (!$tenant_db) {
                    throw new \Exception("Error de conexión con la base de datos.");
                }

                $arrData = [];

                // Preparar la consulta una sola vez
                $stmt = $tenant_db->prepare("
                    SELECT id 
                    FROM predios 
                    WHERE codigo_catastro = ?
                ");

                if (!$stmt) {
                    throw new \Exception("Error preparando la consulta: " . $tenant_db->error);
                }

                foreach ($data as $cuentaGroup) {
                    if (empty($cuentaGroup)) {
                        continue;
                    }

                    $cuenta = $cuentaGroup[0];

                    // Validar que existe código catastral
                    if (!isset($cuenta['codigo_catastro'])) {
                        error_log("Código catastral no encontrado para la cuenta");
                        continue;
                    }

                    // Obtener ID del predio
                    $stmt->bind_param('s', $cuenta['codigo_catastro']);
                    
                    if (!$stmt->execute()) {
                        throw new \Exception("Error ejecutando la consulta: " . $stmt->error);
                    }

                    $result = $stmt->get_result();
                    $predio = $result->fetch_assoc();

                    if (!$predio) {
                        error_log("Predio no encontrado para código catastral: " . $cuenta['codigo_catastro']);
                        continue;
                    }

                    $cuenta['id_predio'] = $predio['id'];

                    // Procesar vigencias
                    if (isset($cuenta['liquidacion']['vigencias'])) {
                        $cuenta['liquidacion']['vigencias'] = array_map(
                            fn($vigencia) => array_merge($vigencia, ['is_checked' => true]),
                            $cuenta['liquidacion']['vigencias']
                        );
                    } else {
                        $cuenta['liquidacion']['vigencias'] = [];
                    }

                    $arrData[] = $cuenta;
                }

                return $arrData;

            } catch (\Exception $e) {
                error_log("Error en filterCuentasRecientes: " . $e->getMessage());
                throw $e;

            } finally {
                if ($stmt) {
                    $stmt->close();
                }
                if ($tenant_db) {
                    $tenant_db->close();
                }
            }
        }


        /* public function groupCuentas(array $arrIdCuentas,array $arrCuentas){
            $arrData = [];
            $totalId = count($arrIdCuentas);
            for ($i=0; $i < $totalId ; $i++) {
                $id = $arrIdCuentas[$i];
                $arrTemp = array_values(array_filter($arrCuentas,function($e) use($id){return $e['id'] == $id;}));
                usort($arrTemp, function($a, $b) {
                    return strtotime($b['fecha']) - strtotime($a['fecha']);
                });
                array_push($arrData,$arrTemp);
            }
            return $arrData;
        }
        public function filterCuentasRecientes(array $data){
            $arrData = [];
            $total = count($data);
            for ($i=0; $i < $total; $i++) {
                $cuenta = $data[$i][0];
                $sql = "SELECT id FROM predios WHERE codigo_catastro= '$cuenta[codigo_catastro]'";
                $cuenta['id_predio'] = mysqli_query($this->linkbd,$sql)->fetch_assoc()['id'];
                $vigencias = $cuenta['liquidacion']['vigencias'];
                for ($j=0; $j < count($vigencias); $j++) {
                    $vigencias[$j]['is_checked'] = true;
                }
                $cuenta['liquidacion']['vigencias'] = $vigencias;
                array_push($arrData,$cuenta);
            }
            return $arrData;
        } */
    }
?>
