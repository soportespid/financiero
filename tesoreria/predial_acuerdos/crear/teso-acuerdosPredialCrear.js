const URL ='tesoreria/predial_acuerdos/crear/teso-acuerdosPredialCrear.php';

var app = new Vue({
    el:"#myapp",
    data() {
        return {
            isLoading:false,
            isModal:false,
            isModalCuenta:false,
            arrData:[],
            arrDataCopy:[],
            arrVigencias:[],
            arrCuotas:[],
            txtSearch:"",
            objTotales:{
                total_avaluo: 0,
                total_predial: 0,
                total_incentivo: 0,
                total_recaudo: 0,
                total_predial_intereses: 0,
                total_predial_descuento_intereses:0,
                total_bomberil: 0,
                total_intereses_bomberil: 0,
                total_descuento_intereses_bomberil: 0,
                total_ambiental: 0,
                total_intereses_ambiental: 0,
                total_descuento_intereses_ambiental: 0,
                total_alumbrado:0,
                total_liquidacion:0
            },
            txtCuotas:1,
            txtResultados:0,
            objData:{},
            isCheckAll:true
        }
    },
    mounted() {
        this.getData();
    },
    methods: {
        getData :async function(){
            const formData = new FormData();
            formData.append("action","get");
            this.isLoading = true;
            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();
            this.arrData = objData;
            this.arrDataCopy = objData;
            this.txtResultados = this.arrDataCopy.length;
            this.isLoading = false;
        },
        save: async function(){
            const vueContext = this;
            if(vueContext.objTotales.total_liquidacion == 0){
                Swal.fire("Error","Debe seleccionar al menos una vigencia","error");
                return false;
            }
            if(vueContext.arrCuotas.length == 0){
                Swal.fire("Error","Debe generar las cuotas del acuerdo","error");
                return false;
            }
            if(vueContext.arrCuotas[vueContext.arrCuotas.length-1] <=0){
                Swal.fire("Error","Las cuotas no pueden ser menores o iguales a cero","error");
                return false;
            }
            const periodos = this.arrVigencias.filter(e=>e.is_checked);
            const obj = {
                codigo:this.objData.codigo_catastro,
                vigencia:periodos[0].vigencia,
                total_cuotas:this.txtCuotas,
                fecha: new Date().toISOString().split("T")[0],
                total:this.objTotales.total_liquidacion,
                documento:this.objData.documento,
                periodos:periodos,
                estado_cuenta:this.objData.id_estado_cuenta,
                cuotas:this.arrCuotas,
                id_predio:this.objData.id
            }
            const formData = new FormData();
            formData.append("data",JSON.stringify(obj));
            formData.append("action","save");

            Swal.fire({
                title:"¿Estás segur@ de guardar?",
                text:"",
                icon: 'warning',
                showCancelButton:true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText:"Sí, guardar",
                cancelButtonText:"No, cancelar"
            }).then(async function(result){
                if(result.isConfirmed){
                    const response = await fetch(URL,{method:"POST",body:formData});
                    const objData =  await response.json();
                    vueContext.isLoading = true;
                    if(objData.status){
                        Swal.fire("Guardado",objData.msg,"success");
                        vueContext.isLoading = false;
                        setTimeout(function(){
                            window.location.href='teso-acuerdosPredialEditar.php?id='+objData.consecutivo;
                        },1500);
                    }else{
                        vueContext.isLoading = false;
                        Swal.fire("Error",objData.msg,"error");
                    }
                }
            });
        },
        search:function(type=""){
            this.arrCuotas = [];
            let search = this.txtSearch.toLowerCase();
            if(type == ""){
                this.objData = this.arrData.filter(e=>e.nombre_propietario.toLowerCase().includes(search)
                || e.codigo_catastro.toLowerCase().includes(search) || e.direccion.toLowerCase().includes(search)
                || e.documento.toLowerCase().includes(search))[0];
                if(typeof this.objData !="undefined"){
                    this.arrVigencias = this.objData.liquidacion.vigencias;
                    this.getTotales();
                    this.getCuotas();
                }else{
                    this.objData = {}
                    Swal.fire("","El predio que busca no existe o no se ha generado un estado de cuenta al dia","warning");
                }
            }else{
                this.arrDataCopy = [...this.arrData.filter(e=>e.nombre_propietario.toLowerCase().includes(search)
                    || e.codigo_catastro.toLowerCase().includes(search) || e.direccion.toLowerCase().includes(search)
                    || e.documento.toLowerCase().includes(search))];
                this.txtResultados = this.arrDataCopy.length;
            }

        },
        selectItem:function({...item}){
            this.objData = item;
            this.arrVigencias = this.objData.liquidacion.vigencias;
            this.getTotales();
            this.getCuotas();
            this.isModalCuenta = false;
        },
        getCuotas:function(){
            this.arrCuotas = [];
            let intLiquidacion = this.objTotales.total_liquidacion;
            let fechaInicial = new Date();
            let intCuota = Math.round(intLiquidacion/this.txtCuotas);
            let totalCuotas = 0;
            const cuotas = this.txtCuotas > 1 ? (this.txtCuotas-1) : 1;
            for (let i = 0; i < cuotas; i++) {
                let fechaCuota = i > 0 ? new Date(fechaInicial.setMonth(fechaInicial.getMonth()+1)) : fechaInicial;
                let fechaFormat = fechaCuota.toISOString().split("T")[0];
                this.arrCuotas.push({fecha:fechaFormat,valor:intCuota,cuota:i+1});
            }
            if(this.txtCuotas > 1){
                let fechaCuota = new Date(fechaInicial.setMonth(fechaInicial.getMonth()+1));
                let fechaFormat = fechaCuota.toISOString().split("T")[0];
                this.arrCuotas.forEach(e=>{ totalCuotas+=e.valor});
                let lastCuota = intLiquidacion-totalCuotas;
                this.arrCuotas.push({fecha:fechaFormat,valor:lastCuota,cuota:this.arrCuotas.length+1});
            }
        },
        updateCuotas:function(){
            let lastCuota = 0;
            let totalCuotas = 0;
            for (let i = 0; i < this.txtCuotas; i++) {

                if(i < this.txtCuotas-1){
                    if(this.arrCuotas[i].valor <=0){
                        Swal.fire("Error","La cuota no puede ser menor o igual a cero, debe corregir.","error");
                        return false;
                    }
                    totalCuotas += parseFloat(this.arrCuotas[i].valor);
                }
            }
            lastCuota = this.objTotales.total_liquidacion - totalCuotas;
            if(lastCuota < 0){
                Swal.fire("Error","La última cuota no puede ser menor o igual a cero, debe corregir.","error");
            }
            this.arrCuotas[this.txtCuotas-1].valor = lastCuota;
        },
        getTotales:function(){
            this.objTotales ={
                total_avaluo: 0,
                total_predial: 0,
                total_incentivo: 0,
                total_recaudo: 0,
                total_predial_intereses: 0,
                total_predial_descuento_intereses:0,
                total_bomberil: 0,
                total_intereses_bomberil: 0,
                total_descuento_intereses_bomberil: 0,
                total_ambiental: 0,
                total_intereses_ambiental: 0,
                total_descuento_intereses_ambiental: 0,
                total_alumbrado:0,
                total_liquidacion:0
            }
            this.arrVigencias.forEach(e => {
                if(e.is_checked){
                    this.objTotales.total_avaluo += e.valor_avaluo;
                    this.objTotales.total_predial += e.predial_descuento+e.predial;
                    this.objTotales.total_incentivo += e.predial_descuento;
                    this.objTotales.total_recaudo += e.predial;
                    this.objTotales.total_predial_intereses += e.predial_intereses;
                    this.objTotales.total_predial_descuento_intereses += e.predial_descuento_intereses;
                    this.objTotales.total_bomberil += e.bomberil;
                    this.objTotales.total_intereses_bomberil += e.bomberil_intereses;
                    this.objTotales.total_descuento_intereses_bomberil += e.bomberil_descuento_intereses;
                    this.objTotales.total_ambiental += e.ambiental;
                    this.objTotales.total_intereses_ambiental += e.ambiental_intereses;
                    this.objTotales.total_descuento_intereses_ambiental += e.ambiental_descuento_intereses;
                    this.objTotales.total_alumbrado += e.alumbrado;
                    this.objTotales.total_liquidacion += e.total_liquidacion;
                }
            });
        },
        changeStatus:function(index){
            const vigencia = this.arrVigencias[index];
            vigencia.is_checked = !vigencia.is_checked;
            this.arrVigencias[index] = vigencia;
            const flag = this.arrVigencias.every((e)=>e.is_checked == true);
            this.isCheckAll = flag;
            this.getTotales();
            this.getCuotas();
        },
        changeAll:function(){
            this.arrVigencias.forEach(e => {
                e.is_checked = this.isCheckAll
            });
            this.getTotales();
            this.getCuotas();
        },
        formatNumero: function(valor){
            return new Intl.NumberFormat("en-US", {style: "currency", currency: "USD"}).format(valor);
        },
        showTab:function(tab){
            let tabs = this.$refs.rTabs.children;
            let tabsContent = this.$refs.rTabsContent.children;
            for (let i = 0; i < tabs.length; i++) {
                if(!tabs[i].classList.contains("exclude")){
                    tabs[i].classList.remove("active");
                    tabsContent[i].classList.remove("active")
                }
            }
            tabs[tab-1].classList.add("active");
            tabsContent[tab-1].classList.add("active")
        }
    },
    computed:{

    }
})
