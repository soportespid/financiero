<?php
    require_once '../../../Librerias/core/Helpers.php';
    require_once '../../../PHPExcel/Classes/PHPExcel.php';
    require_once '../../../tcpdf/tcpdf_include.php';
    require_once '../../../tcpdf/tcpdf.php';
    session_start();
    /*ini_set('display_errors', '1');
    ini_set('display_startup_errors', '1');
    error_reporting(E_ALL);*/
    class LiquidacionIngresoExportController {
        public function exportPdf(){
            if(!empty($_SESSION)){
                $arrData = json_decode($_POST['data'],true);
                if(!empty($arrData)){
                    $arrFecha = explode("-",$arrData['fecha']);
                    $strFecha = $arrFecha[2]."/".$arrFecha[1]."/".$arrFecha[0];
                    define("FECHA",$strFecha);
                    define("ID",$arrData['id_recaudo']);
                    define("ESTADO",$arrData['estado']);
                    $arrDet = $arrData['det'];

                    $pdf = new MYPDF('P','mm','Letter', true, 'iso-8859-1', false);
                    $pdf->SetDocInfoUnicode (true);
                    // set document information
                    $pdf->SetCreator(PDF_CREATOR);
                    $pdf->SetAuthor('IDEALSAS');
                    $pdf->SetTitle('RECONOCIMIENTO DE INGRESOS');
                    $pdf->SetSubject('RECONOCIMIENTO DE INGRESOS');
                    $pdf->SetKeywords('TCPDF, PDF, example, test, guide');
                    $pdf->SetMargins(10, 38, 10);// set margins
                    $pdf->SetHeaderMargin(38);// set margins
                    $pdf->SetFooterMargin(17);// set margins
                    $pdf->SetAutoPageBreak(TRUE, 20);// set auto page breaks
                    // set some language-dependent strings (optional)
                    if (@file_exists(dirname(__FILE__).'/lang/spa.php'))
                    {
                        require_once(dirname(__FILE__).'/lang/spa.php');
                        $pdf->setLanguageArray($l);
                    }
                    $pdf->AddPage();
                    $pdf->SetFillColor(51, 153, 204);
                    $pdf->SetTextColor(255,255,255);
                    $pdf->SetFont('Helvetica','b',11);
                    $pdf->MultiCell(40,4,"Contribuyente","LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->SetFillColor(255,255,255);
                    $pdf->SetTextColor(0,0,0);
                    $pdf->SetFont('Helvetica','',11);
                    $pdf->MultiCell(150,4,$arrData['nombre_tercero'],"RBT",'L',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->ln();
                    $pdf->SetFillColor(51, 153, 204);
                    $pdf->SetTextColor(255,255,255);
                    $pdf->SetFont('Helvetica','b',11);
                    $pdf->MultiCell(40,4,"CC o NIT","LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->SetFillColor(255,255,255);
                    $pdf->SetTextColor(0,0,0);
                    $pdf->SetFont('Helvetica','',11);
                    $pdf->MultiCell(150,4,$arrData['tercero'],"RBT",'L',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->ln();
                    $pdf->SetFillColor(51, 153, 204);
                    $pdf->SetTextColor(255,255,255);
                    $pdf->SetFont('Helvetica','b',11);
                    $pdf->MultiCell(40,4,"Total","LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->SetFillColor(255,255,255);
                    $pdf->SetTextColor(0,0,0);
                    $pdf->SetFont('Helvetica','',11);
                    $pdf->MultiCell(150,4,formatNum($arrData['valortotal']),"RBT",'L',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->ln();
                    $pdf->ln();
                    $pdf->SetFillColor(51, 153, 204);
                    $pdf->SetTextColor(255,255,255);
                    $pdf->SetFont('Helvetica','b',11);
                    $pdf->MultiCell(190,4,"Detalle","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->ln();

                    if(!empty($arrDet)){
                        $pdf->SetFillColor(153,221,255);
                        $pdf->SetFont('helvetica','B',10);
                        $pdf->SetTextColor(0,0,0);
                        $pdf->MultiCell(150,4,"Descripcion","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
                        $pdf->MultiCell(40,4,"Valor","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
                        $pdf->ln();
                        $fill = true;
                        foreach ($arrDet as $data) {
                            $pdf->SetFillColor(245,245,245);
                            $pdf->SetFont('helvetica','',9);
                            $height = 5;
                            $pdf->MultiCell(150,$height,$data['codigo_ingreso']." ".$data['nombre_ingreso'],"",'L',$fill,0,'','',true,0,false,true,0,'M',true);
                            $pdf->MultiCell(40,$height,formatNum($data['valor']),"",'R',$fill,0,'','',true,0,false,true,0,'M',true);
                            $pdf->ln();
                            $fill = !$fill;
                            if($pdf->GetY()>230){
                                $pdf->AddPage();
                            }
                        }
                        $pdf->SetFillColor(153,221,255);
                        $pdf->SetFont('helvetica','B',9);
                        $pdf->SetTextColor(0,0,0);
                        $pdf->MultiCell(150,4,"Total","",'R',true,0,'','',true,0,false,true,0,'M',true);

                        $pdf->SetFillColor(255,255,255);
                        $pdf->SetTextColor(0,0,0);
                        $pdf->MultiCell(40,4,formatNum($arrData['valortotal']),"",'R',0,0,'','',true,0,false,true,0,'M',true);
                    }
                    $pdf->ln();
                    $pdf->ln();
                    $pdf->ln();
                    $pdf->ln();
                    $pdf->ln();
                    $pdf->ln();
                    //Campo para recibido y sello
                    $getY = $pdf->getY();
                    $pdf->setX(60);
                    $pdf->SetFont('helvetica','B',9);
                    $pdf->SetFillColor(51, 153, 204);
                    $pdf->SetTextColor(255,255,255);
                    $pdf->cell(95,4,'Recibido y sello','LRTB',0,'C',1);
                    $pdf->ln();
                    $pdf->setX(60);
                    $pdf->SetFont('helvetica','',7);
                    $pdf->SetTextColor(0,0,0);
                    $pdf->SetFillColor(255,255,255);
                    $pdf->cell(95,20,'','LRTB',0,'L',1);
                    $pdf->ln();
                    $pdf->Output('recibo_liquidacion_recaudo.pdf', 'I');
                }
            }
            die();
        }

    }
    class MYPDF extends TCPDF {

		public function Header()
		{
			$request = configBasica();
            $strNit = $request['nit'];
            $strRazon = $request['razonsocial'];

			//Parte Izquierda
			$this->Image('../../../imagenes/escudo.jpg',  13, 13, 20, 20, 'JPG', '', 'T', true, 300, '', false, false, 0, false, false, false);// Logo
			$this->SetFont('helvetica','B',8);
			$this->SetY(10);
			$this->RoundedRect(10, 10, 190, 25, 1,'');
			$this->Cell(0.1);
			$this->Cell(26,25,'','R',0,'L');
			$this->SetY(8);
			$this->SetX(40);
			$this->SetFont('helvetica','B',9);
			$this->Cell(140,15,strtoupper("$strRazon"),0,0,'C');
			$this->SetFont('helvetica','B',7);
			$this->SetY(12);
			$this->SetX(40);
			$this->Cell(140,15,'NIT: '.$strNit,0,0,'C');
			//*****************************************************************************************************************************
			$this->SetFont('helvetica','B',9);
			$this->SetY(23);
			$this->SetX(36);
            $this->Cell(164,12,"RECIBO RECONOCIMIENTO DE INGRESOS",'T',0,'C');


            $this->SetFont('helvetica','B',7);
			$this->SetY(10);
			$this->SetX(167);
			$this->Cell(30,7," NUMERO: ".ID,"L",0,'L');
			$this->SetY(17);
			$this->SetX(167);
            $this->Cell(35,6," FECHA: ". FECHA,"L",0,'L');

			//**********************************************************
			$this->SetFont('times','B',10);
			$this->ln(12);
			//**********************************************************
            if(ESTADO=="R"){
                $img_file = "../../../assets/img/reversado.png";
                $this->SetAlpha(0.35);
                $this->Image($img_file, 0, 20, 250, 280, '', '', '', false, 300, '', false, false, 300);
                $this->SetAlpha(1);
            }
		}
		public function Footer(){

			$request = configBasica();
            $strDireccion = $request['direccion'] != "" ? "Dirección: ".strtoupper($request['direccion']) : "";
            $strWeb = $request['web'] != "" ? "Pagina web: ".strtoupper($request['web']) :"";
            $strEmail = $request['email'] !="" ? "Email: ".strtoupper($request['email']) :"";
            $strTelefono = $request['telefono'] != "" ? "Telefonos: ".$request['telefono'] : "";
            $strUsuario = searchUser($_SESSION['cedulausu'])['nom_usu'];
			$strNick = $_SESSION['nickusu'];
			$strFecha = date("d/m/Y H:i:s");
			$strIp = $_SERVER['REMOTE_ADDR'];

			$this->SetFont('helvetica', 'I', 8);
			$txt = <<<EOD
			$strDireccion $strTelefono
			$strEmail $strWeb
			EOD;
			$this->SetFont('helvetica', 'I', 6);
			$this->Cell(277,10,'','T',0,'T');
			$this->ln(2);
			$this->Write(0, $txt , '', 0, 'C', true, 0, false, false, 0);

			$this->Cell(50, 10, 'Hecho por: '.$strUsuario, 00, false, 'C', 0, '', 0, false, 'T', 'M');
			$this->Cell(30, 10, 'Impreso por: '.$strNick, 0, false, 'C', 0, '', 0, false, 'T', 'M');
			$this->Cell(30, 10, 'IP: '.$strIp, 0, false, 'C',0, '', 0, false, 'T', 'M');
			$this->Cell(30, 10, 'Fecha: '.$strFecha, 0, false, 'C', 0, '', 0, false, 'T', 'M');
			$this->Cell(30, 10, 'IDEAL.10 S.A.S', 0, false, 'C', 0, '', 0, false, 'T', 'M');
            $this->Cell(20, 10, 'Pagina '.$this->getAliasNumPage().'/'.$this->getAliasNbPages(), 0, false, 'C', 0, '', 0, false, 'T', 'M');
		}
	}

    if($_POST){
        $obj = new LiquidacionIngresoExportController();
        if($_POST['action'] == "pdf"){
            $obj->exportPdf();
        }
    }

?>
