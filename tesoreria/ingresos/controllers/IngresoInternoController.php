<?php
    require_once '../../../Librerias/core/Helpers.php';
    require_once '../../../PHPExcel/Classes/PHPExcel.php';
    require_once '../models/IngresoInternoModel.php';
    session_start();

    class IngresoInternoController extends IngresoInternoModel{
        public function initialData(){
            if(!empty($_SESSION)){
                $arrResponse = array(
                    "liquidaciones"=>$this->selectLiquidaciones(),
                    "ingresos_reversar"=>$this->selectIngresosReversar(),
                    "cuentas"=>getCuentasBancos(),
                    "mediosDePago" => getMediosDePago()
                );
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
            die();
        }
        public function save(){
            if(!empty($_SESSION)){
                if($_POST){
                    $strTipoRecaudo = strClean($_POST['tipo']);
                    $strFecha = strClean($_POST['fecha']);
                    $arrData = json_decode($_POST['data'],true);
                    $arrBanco = json_decode($_POST['cuenta'],true);
                    $strMov = strClean($_POST['mov']);
                    $isSsf = $_POST['is_ssf'] == "true" ? 1 : 2;

                    if($strMov == "101"){
                        if(checkBlock($_SESSION['cedulausu'],$_POST['fecha'])){
                            if(!empty($arrData)){
                                $flag = true;
                                if($strTipoRecaudo == "banco"){
                                    if(empty($arrBanco)){
                                        $flag = false;
                                        $arrResponse = array("status"=>false,"msg"=>"Error en cuenta bancaria");
                                    }
                                }
                                if($flag){
                                    $intId = searchConsec("tesosinreciboscaja","id_recibos");
                                    $request = $this->selectIngresoRecaudo($arrData['id_recaudo'],"S","101");
                                    if(empty($request)){
                                        $request = $this->insertComprobanteCab($strFecha,$arrData['concepto'],$arrData['valortotal'],$intId);
                                        if($request > 0){
                                            $requestRecibo = $this->insertReciboCab($request,$strFecha,$arrData['valortotal'],$strTipoRecaudo,$arrData['id_recaudo'],$arrBanco,$intId, $isSsf);
                                            if($requestRecibo > 0){
                                                $requestDet = $this->insertReciboDet($requestRecibo,$arrData['tercero'],$strFecha,$arrData['det'],$arrBanco,$strTipoRecaudo, $isSsf);
                                                if($requestDet > 0){
                                                    $this->updateRecaudo($arrData['codigo']);
                                                    $arrResponse = array("status"=>true,"msg"=>"Datos guardados correctamente.","id"=>$intId);
                                                }else if($requestDet == "comp"){
                                                    $arrResponse = array("status"=>false,"msg"=>"Error en detalle de comprobante, inténtelo de nuevo.");
                                                }else if($requestDet == "ptto"){
                                                    $arrResponse = array("status"=>false,"msg"=>"Error en presupuesto, inténtelo de nuevo.");
                                                }else if($requestDet == "det"){
                                                    $arrResponse = array("status"=>false,"msg"=>"Error en detalle de recibo, inténtelo de nuevo.");
                                                }
                                            }else{
                                                $arrResponse = array("status"=>false,"msg"=>"Error en cabecera de recibo, inténtelo de nuevo.");
                                            }
                                        }else{
                                            $arrResponse = array("status"=>false,"msg"=>"Error en cabecera de comprobante, inténtelo de nuevo.");
                                        }
                                    }else{
                                        $arrResponse = array("status"=>false,"msg"=>"Este ingreso ya se ha guardado, intente con otro.");
                                    }
                                }
                            } else{
                                $arrResponse = array("status"=>false,"msg"=>"Error de datos");
                            }
                        }else{
                            $arrResponse = array("status"=>false,"msg"=>"Usuario, esta fecha se encuentra bloqueda.");
                        }
                    }else{
                        if(checkBlock($_SESSION['cedulausu'],$_POST['fecha_rev'])){
                            if(empty($_POST['fecha_rev']) || empty($_POST['concepto_rev']) || empty($_POST['id_rev'])){
                                $arrResponse = array("status"=>false,"msg"=>"Error de datos");
                            }else{
                                $strFechaRev = strClean($_POST['fecha_rev']);
                                $strConceptoRev = strtoupper(strClean($_POST['concepto_rev']));
                                $intId = floatval($_POST['id_rev']);
                                $request = $this->selectIngreso($intId);
                                if(!empty($request)){
                                    $requestValid = $this->selectIngresoRecaudo($request['recibo']['cab']['id_recaudo'],"R","101",$intId);
                                    if(empty($requestValid)){
                                        $requestRev = $this->insertReversar($intId,$strFechaRev,$strConceptoRev,$request);
                                        if($requestRev > 0){
                                            $arrResponse = array("status"=>true,"msg"=>"Datos guardados correctamente","id"=>$intId);
                                        }else if($request == "recibo_cab"){
                                            $arrResponse = array("status"=>false,"msg"=>"Error al actualizar cabecera del recibo");
                                        }else if($request == "recaudo"){
                                            $arrResponse = array("status"=>false,"msg"=>"Error al actualizar recaudo");
                                        }else if($request == "ptto"){
                                            $arrResponse = array("status"=>false,"msg"=>"Error al eliminar presupuesto");
                                        }else if($request == "recibo"){
                                            $arrResponse = array("status"=>false,"msg"=>"Error al guardar ingreso reversado");
                                        }else if($request == "comp_det"){
                                            $arrResponse = array("status"=>false,"msg"=>"Error al guardar detalle de comprobante de ingreso reversado");
                                        }else if($request == "comp"){
                                            $arrResponse = array("status"=>false,"msg"=>"Error al guardar cabecera de comprobante de ingreso reversado");
                                        }
                                    }else{
                                        $arrResponse = array("status"=>false,"msg"=>"Este ingreso ya se ha reversado, intente con otro.");
                                    }
                                }else{
                                    $arrResponse = array("status"=>false,"msg"=>"El ingreso no existe!");
                                }
                            }
                        }else{
                            $arrResponse = array("status"=>false,"msg"=>"Usuario, esta fecha se encuentra bloqueda.");
                        }
                    }
                    echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
                }
            }
            die();
        }
        public function getEdit(){
            if(!empty($_SESSION)){
                if($_POST){
                    $intId = intval($_POST['codigo']);
                    $request = $this->selectEdit($intId);
                    if(!empty($request)){
                        $arrData['recibo'] = array_values(array_filter($request,function($e){return $e['tipo_mov'] == 101;}))[0];
                        if(count($request) > 1){
                            $arrData['reversado'] = array_values(array_filter($request,function($e){return $e['tipo_mov'] == 301;}))[0];
                        }
                        $arrResponse = array("status"=>true,"data"=>$arrData,"consecutivos"=>getConsecutivos("tesosinreciboscaja","id_recibos"),"cuentas"=>getCuentasBancos(),"mediosDePago" => getMediosDePago());
                    }else{
                        $arrResponse = array("status"=>false,"consecutivo"=>searchConsec("tesosinreciboscaja ","id_recibos")-1);
                    }
                    echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
                }
            }
            die();
        }
        public function getSearch(){
            if(!empty($_SESSION)){
                if($_POST){
                    $strFechaInicial = strClean($_POST['fecha_inicial']);
                    $strFechaFinal = strClean($_POST['fecha_final']);
                    $strSearch = strClean($_POST['search']);
                    $request = $this->selectSearch($strFechaInicial,$strFechaFinal,$strSearch);
                    echo json_encode($request,JSON_UNESCAPED_UNICODE);
                }
            }
            die();
        }
    }

    if($_POST){
        $obj = new IngresoInternoController();
        if($_POST['action'] == "get"){
            $obj->initialData();
        }else if($_POST['action']=="save"){
            $obj->save();
        }else if($_POST['action']=="edit"){
            $obj->getEdit();
        }else if($_POST['action']=="gen"){
            $obj->getSearch();
        }
    }

?>
