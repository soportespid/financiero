const URL ='tesoreria/ingresos/controllers/IngresoInternoController.php';
const URLEXPORT ='tesoreria/ingresos/controllers/IngresoInternoExportController.php';
var app = new Vue({
    el:"#myapp",
    data() {
        return {
            isLoading:false,
            isModalCuenta:false,
            isModalMedioDePago:false,
            isModal:false,
            isSsf:false,
            isModalReversar:false,
            txtSearch:"",
            txtSearchReversar:"",
            txtSearchLiquidacion:"",
            txtSearchCuenta:"",
            txtSearchMediosDePago:"",
            txtResultadosliquidacion:0,
            txtResultadosCuenta:0,
            txtResultadosReversar:0,
            txtResultadosMediosDePago:0,
            txtResultados:0,
            txtTotal:0,
            txtConcepto:"",
            objTercero:{nombre:"",codigo:""},
            objLiquidacion:{nombre:"",codigo:""},
            objCuenta:{cuenta_banco:"",codigo:""},
            objRecibo:{id_recibos:"",valor:""},
            arrCuentasCopy:[],
            arrCuentas:[],
            arrMediosDePago:[],
            arrMediosDePagoCopy:[],
            arrLiquidacion:[],
            arrLiquidacionCopy:[],
            arrIngresosReversar:[],
            arrIngresosReversarCopy:[],
            arrConsecutivos:[],
            arrData:[],
            arrSearchData:[],
            arrSearchDataCopy:[],
            arrExportData:[],
            arrPres:[],
            txtFecha:new Date().toISOString().split("T")[0],
            txtFechaRev:new Date().toISOString().split("T")[0],
            txtFechaInicial:new Date(new Date().getFullYear(), 0, 1).toISOString().split("T")[0],
            txtFechaFinal:new Date().toISOString().split("T")[0],
            txtConceptoRev:"",
            txtConsecutivo: 0,
            txtStatus:"S",
            txtTotal:0,
            txtTotalPres:0,
            selectRecaudo:"banco",
            selectMovimiento:"101",
            recibo:{id_recibos:""},
            reversado:{id_recibos:""},
        }
    },
    mounted() {
        const intPageVal = this.$refs.pageType.value;
        if(intPageVal == 1){
            this.getData();
        }else if(intPageVal == 2){
            this.getEdit();
        }else{
            this.getSearch();
        }
    },
    methods: {
        getData: async function(){
            const formData = new FormData();
            formData.append("action","get");
            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();
            this.arrLiquidacion = objData.liquidaciones;
            this.arrCuentas = objData.cuentas;
            this.arrLiquidacionCopy = objData.liquidaciones;
            this.arrCuentasCopy = objData.cuentas;
            this.arrMediosDePago = objData.mediosDePago;
            this.arrMediosDePagoCopy = objData.mediosDePago;
            this.txtResultadosMediosDePago = this.arrMediosDePagoCopy.length;
            this.arrIngresosReversar = objData.ingresos_reversar;
            this.arrIngresosReversarCopy = objData.ingresos_reversar;
            this.txtResultadosReversar = this.arrLiquidacionCopy.length;
            this.txtResultadosCuenta = this.arrCuentasCopy.length;
            this.txtResultadosliquidacion = this.arrLiquidacionCopy.length;
        },
        getEdit:async function(){
            const codigo = new URLSearchParams(window.location.search).get('id');
            let formData = new FormData();
            formData.append("action","edit");
            formData.append("codigo",codigo);
            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();
            if(objData.status){
                this.arrConsecutivos =Array.from(new Set(objData.consecutivos.map(item=>{return JSON.stringify(item)}))).map(item=>{return JSON.parse(item)});
                this.recibo = objData.data.recibo;
                this.isSsf = this.recibo.mediopago != 1 ? true : false;
                this.selectRecaudo = this.isSsf ? "ssf" : "banco";
                //this.selectRecaudo = this.recibo.recaudado;
                this.arrCuentas = objData.cuentas;
                this.arrMediosDePago = objData.mediosDePago;
                this.txtConsecutivo = this.recibo.id_recibos;
                this.arrPres = this.recibo.pres;

                this.objLiquidacion = {
                    nombre:this.recibo.nombre,
                    tercero:this.recibo.tercero,
                    valortotal:this.recibo.valor,
                    concepto:this.recibo.concepto,
                    det:this.recibo.det
                };
                if(objData.data.reversado){
                    this.reversado = objData.data.reversado;
                    this.txtFechaRev = this.reversado.fecha;
                    this.objRecibo.id_recibos = this.reversado.id_recibos;
                    this.txtConceptoRev = this.reversado.concepto;
                }
                if(this.selectRecaudo == "banco"){
                    const cuenta = this.arrCuentas.filter(function(e){return e.cuenta == app.recibo.cuentabanco});
                    this.objCuenta = cuenta.length > 0 ? cuenta[0] : {cuenta_banco:"",nombre:""};
                }else{
                    const cuenta = this.arrMediosDePago.filter(function(e){return e.medio_pago == app.recibo.mediopago});
                    this.objCuenta = cuenta.length > 0 ? cuenta[0] : {cuenta_banco:"",nombre:""};
                }
                this.arrPres.forEach(e => { app.txtTotalPres+= parseFloat(e.valor)});
            }else{
                window.location.href='teso-ingresoInternoEditar?id='+objData.consecutivo;
            }
        },
        getSearch:async function(){
            const formData = new FormData();
            formData.append("action","gen");
            formData.append("fecha_inicial",this.txtFechaInicial);
            formData.append("fecha_final",this.txtFechaFinal);
            formData.append("search",this.txtSearch);
            this.isLoading = true;
            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();
            this.arrSearchData = objData;
            this.arrSearchDataCopy = objData;
            this.txtResultados = this.arrSearchData.length;
            this.search('internos');
            this.isLoading = false;
        },
        editItem:function(id){
            window.location.href='teso-ingresoInternoEditar?id='+id;
        },
        search:function(type=""){
            let search = "";
            if(type == "modal_liquidacion")search = this.txtSearchLiquidacion.toLowerCase();
            if(type == "modal_cuenta")search = this.txtSearchCuenta.toLowerCase();
            if(type == "modal_medios_de_pago")search = this.txtSearchMediosDePago.toLowerCase();
            if(type == "modal_recibo")search = this.txtSearchReversar.toLowerCase();
            if(type=="cod_liquidacion")search = this.objLiquidacion.codigo;
            if(type=="cod_cuenta")search = this.objCuenta.cuenta_banco;
            if(type=="cod_recibo")search = this.objRecibo.id_recibos;

            if(type=="modal_liquidacion"){
                this.arrLiquidacionCopy = [...this.arrLiquidacion.filter(e=>e.codigo.toLowerCase().includes(search) || e.nombre.toLowerCase().includes(search)
                    || e.concepto.toLowerCase().includes(search))];
                this.txtResultadosliquidacion = this.arrLiquidacionCopy.length;
            }else if(type=="modal_cuenta"){
                this.arrCuentasCopy = [...this.arrCuentas.filter(e=>e.cuenta_banco.toLowerCase().includes(search) || e.nombre.toLowerCase().includes(search))];
                this.txtResultadosCuenta = this.arrCuentasCopy.length;
            }else if(type=="modal_medios_de_pago"){
                this.arrMediosDePagoCopy = [...this.arrMediosDePago.filter(e=>e.nombre.toLowerCase().includes(search) || e.cuenta.toLowerCase().includes(search))];
                this.txtResultadosMediosDePago = this.arrMediosDePagoCopy.length;
            }else if(type=="modal_recibo"){
                this.arrIngresosReversarCopy = [...this.arrIngresosReversar.filter(e=>e.id_recibos.toLowerCase().includes(search))];
                this.txtResultadosReversar = this.arrIngresosReversarCopy.length;
            }else if(type=="cod_liquidacion"){
                const data = [...this.arrLiquidacionCopy.filter(e=>e.codigo == search)];
                this.objLiquidacion = data.length > 0 ? JSON.parse(JSON.stringify(data[0])) : {codigo:"",nombre:""};
            }else if(type=="cod_cuenta"){
                const data = [...this.arrCuentas.filter(e=>e.cuenta_banco == search)];
                this.objCuenta = data.length > 0 ? JSON.parse(JSON.stringify(data[0])) : {cuenta_banco:"",nombre:""};
            }else if(type=="cod_recibo"){
                const data = [...this.arrIngresosReversar.filter(e=>e.id_recibos == search)];
                this.objRecibo = data.length > 0 ? JSON.parse(JSON.stringify(data[0])) : {id_recibos:"",valor:""};
            }else if(type=="internos"){
                search = this.txtSearch.toLowerCase();
                this.arrSearchData = [...this.arrSearchDataCopy.filter(e=>e.id_recibos.toLowerCase().includes(search) || e.concepto.toLowerCase().includes(search) || e.tercero.toLowerCase().includes(search) || e.nombre.toLowerCase().includes(search))];
                
            }
        },
        formatNum: function(valor){
            return new Intl.NumberFormat("en-US", {style: "currency", currency: "USD"}).format(valor);
        },
        selectItem:function ({...data},type=""){
            if(type =="modal_liquidacion"){
                this.isSsf = data.medio_pago == 2 ? true : false;
                this.objCuenta = {};
                this.objLiquidacion = data;
                this.isModal = false;
            }else if(type =="modal_cuenta"){
                this.objCuenta = data;
                this.isModalCuenta = false;
                this.isModalMedioDePago = false;
            }else if(type =="modal_recibo"){
                this.objRecibo = data;
                this.isModalReversar = false;
            }
        },
        save:async function(){
            if(this.selectMovimiento == "101"){
                if(this.selectRecaudo ==  0){
                    Swal.fire("Atención!","Debe seleccionar el método de recaudo","warning");
                    return false;
                }
                if(this.objLiquidacion.codigo == ""){
                    Swal.fire("Atención!","Debe seleccionar la liquidacion","warning");
                    return false;
                }
                if(this.selectRecaudo == "banco" && this.objCuenta.cuenta_banco ==""){
                    Swal.fire("Atención!","Debe seleccionar la cuenta bancaria","warning");
                    return false;
                }
            }else{
                if(this.objRecibo.id_recibos ==""){
                    Swal.fire("Atención!","Debe elegir el ingreso a reversar","warning");
                    return false;
                }
                if(this.txtConceptoRev ==""){
                    Swal.fire("Atención!","La descripción no puede estar vacio","warning");
                    return false;
                }
            }
            Swal.fire({
                title:"¿Estás segur@ de guardar?",
                text:"",
                icon: 'warning',
                showCancelButton:true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText:"Sí, guardar",
                cancelButtonText:"No, cancelar"
            }).then(async function(result){
                if(result.isConfirmed){
                    const formData = new FormData();
                    formData.append("data",JSON.stringify(app.objLiquidacion));
                    formData.append("id_rev",app.objRecibo.id_recibos);
                    formData.append("mov",app.selectMovimiento);
                    formData.append("concepto_rev",app.txtConceptoRev);
                    formData.append("tipo",app.selectRecaudo);
                    formData.append("cuenta",JSON.stringify(app.objCuenta));
                    formData.append("fecha",app.txtFecha);
                    formData.append("fecha_rev",app.txtFechaRev);
                    formData.append("is_ssf",app.isSsf);
                    formData.append("action","save");
                    app.isLoading = true;
                    const response = await fetch(URL,{method:"POST",body:formData});
                    const objData = await response.json();
                    app.isLoading = false;
                    if(objData.status){
                        Swal.fire("Guardado",objData.msg,"success");
                        if(objData.id){
                            setTimeout(function(){
                                window.location.href='teso-ingresoInternoEditar?id='+objData.id;
                            },1500);
                        }
                    }else{
                        Swal.fire("Error",objData.msg,"error");
                    }
                }
            });
        },
        exportData:function(){
            const form = document.createElement("form");
            form.method ="post";
            form.target="_blank";
            form.action=URLEXPORT;

            function addField(name,value){
                const input = document.createElement("input");
                input.type="hidden";
                input.name=name;
                input.value = value;
                form.appendChild(input);
            }
            addField("action","pdf");
            addField("data",JSON.stringify(this.recibo));
            addField("banco",JSON.stringify(this.objCuenta));
            addField("pres",JSON.stringify(this.arrPres));
            addField("totalPres",JSON.stringify(this.txtTotalPres));
            document.body.appendChild(form);
            form.submit();
            document.body.removeChild(form);
        },
        nextItem:function(type){
            let vueContext = this;
            let id = this.txtConsecutivo;
            let index = this.arrConsecutivos.findIndex(function(e){return e.id == id});
            if(type=="next" && vueContext.arrConsecutivos[++index]){
                id = this.arrConsecutivos[index++].id;
            }else if(type=="prev" && vueContext.arrConsecutivos[--index]){
                id = this.arrConsecutivos[index--].id;
            }
           window.location.href='teso-ingresoInternoEditar?id='+id;
        },
        showTab:function(tab){
            let tabs = this.$refs.rTabs.children;
            let tabsContent = this.$refs.rTabsContent.children;
            for (let i = 0; i < tabs.length; i++) {
                tabs[i].classList.remove("active");
                tabsContent[i].classList.remove("active")
            }
            tabs[tab-1].classList.add("active");
            tabsContent[tab-1].classList.add("active")
        },

        modalPago(){
            if(this.isSsf){
                this.isModalMedioDePago = true;
            }else{
                this.isModalCuenta = true;
            }
        }
    },
})
