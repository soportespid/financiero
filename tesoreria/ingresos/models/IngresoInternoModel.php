<?php
    require_once '../../../Librerias/core/Helpers.php';
    require_once '../../../Librerias/core/Mysql.php';
    session_start();
    /*ini_set('display_errors', '1');
    ini_set('display_startup_errors', '1');
    error_reporting(E_ALL);*/
    class IngresoInternoModel extends Mysql{
        private $intConsecutivo;
        private $intIdComp;
        private $strTipo;
        private $strCuentaBanco;
        private $strCuentaCaja;
        private $strTercero;
        private $strConcepto;
        private $floatTotal;
        private $strFecha;
        private $strFechaFinal;
        private $isSsf;
        
        function __construct(){
            parent::__construct();
        }

        public function selectCentroCostos(){
            $sql = "SELECT id_cc as codigo, nombre FROM centrocosto WHERE estado='S'";
            $request = $this->select_all($sql);
            return $request;
        }
        public function selectRecaudo(int $intConsecutivo){
            $this->intConsecutivo = $intConsecutivo;
            $sql = "SELECT * FROM tesosinrecaudos WHERE id_recaudo = $this->intConsecutivo";
            $request = $this->select($sql);
            return $request;
        }
        public function selectIngresoRecaudo(int $intRecaudo,string $strEstado, string $strMov,$intConsecutivo = ""){
            $condicion = "";
            $this->intConsecutivo = $intConsecutivo;
            if($this->intConsecutivo !=""){
                $condicion =" AND id_recibos = $this->intConsecutivo";
            }
            $sql = "SELECT * FROM tesosinreciboscaja
            WHERE id_recaudo = $intRecaudo AND tipo_mov = '$strMov' AND estado = '$strEstado' AND tipo=3 $condicion";
            $request = $this->select($sql);
            return $request;
        }
        public function selectLiquidaciones(){
            $sql = "SELECT *,DATE_FORMAT(fecha,'%d/%m/%Y') as fecha,id_recaudo as codigo
            FROM tesosinrecaudos
            WHERE estado = 'S'
            ORDER BY id_recaudo DESC";
            $request = $this->select_all($sql);
            if(!empty($request)){
                $total = count($request);
                for ($i=0; $i < $total; $i++) {
                    $id = $request[$i]['codigo'];
                    $request[$i]['nombre'] = getNombreTercero($request[$i]['tercero']);
                    $request[$i]['total_format'] = formatNum($request[$i]['valortotal']);
                    $sqlDet = "SELECT DISTINCT
                        det.ingreso as codigo_ingreso,
                        det.valor,
                        det.estado,
                        det.seccion as codigo_cc,
                        COALESCE(cc.nombre,'') as nombre_centro,
                        det.fuente as codigo_fuente,
                        COALESCE(f.nombre,'') as nombre_fuente,
                        COALESCE(i.nombre,'') as nombre_ingreso,
                        det.cuentapres,
                        det.cuenta_clasificadora,
                        idet.concepto
                        FROM tesosinrecaudos_det det
                        LEFT JOIN pptoseccion_presupuestal cc ON det.seccion = cc.id_seccion_presupuestal
                        LEFT JOIN ccpet_fuentes_cuipo f ON f.codigo_fuente = det.fuente
                        LEFT JOIN tesoingresos_det idet ON idet.codigo = det.ingreso
                        LEFT JOIN tesoingresos i ON det.ingreso = i.codigo
                        WHERE id_recaudo = $id
                        GROUP BY det.cuentapres,det.cuenta_clasificadora,det.fuente,det.seccion,det.valor";
                    $requestDet = $this->select_all($sqlDet);
                    $request[$i]['det'] = $requestDet;
                }
            }
            return $request;
        }
        public function selectIngresosReversar(){
            $sql = "SELECT * FROM tesosinreciboscaja WHERE estado = 'S' AND tipo = '3' ORDER BY id_recibos DESC";
            $request = $this->select_all($sql);
            return $request;
        }
        public function insertComprobanteCab(string $strFecha,string $strConcepto,float $floatTotal,int $intConsecutivo,$comp =25,$estado = 1){
            $this->strFecha = $strFecha;
            $this->strConcepto = $strConcepto;
            $this->floatTotal = $floatTotal;
            $this->intConsecutivo = $intConsecutivo;
            $sql="INSERT INTO comprobante_cab (numerotipo,tipo_comp,fecha,concepto,total,total_debito,total_credito,diferencia,estado)
            VALUES (?,?,?,?,?,?,?,?,?)";
            $arrValues = [$this->intConsecutivo,$comp,$this->strFecha,$this->strConcepto,0,$this->floatTotal,$this->floatTotal,0,$estado];
            $request = $this->insert($sql,$arrValues);
            return $request;
        }
        public function insertReciboCab(int $intIdComp,string $strFecha,float $floatTotal, string $strTipo,int $intConsecutivo,array $arrBanco, int $id, int $isSsf){
            insertAuditoria("teso_auditoria","teso_funciones_id",6,"Crear",$id);
            $medio_pago = 1;
            if($isSsf == 1){
                $this->strCuentaBanco = "";
                $this->strCuentaCaja = "";
                $medio_pago = $arrBanco['medio_pago'];
                
            }else{
                $this->strCuentaBanco = $strTipo == "banco" ? $arrBanco['cuenta'] : "";
                $this->strCuentaCaja = $strTipo == "caja" ? getTesoParametros()['cuentacaja'] : "";
            }

            $arrFecha = explode("-",$strFecha);
            $this->strFecha = $strFecha;
            $this->floatTotal = $floatTotal;
            $this->intIdComp = $intIdComp;
            $this->intConsecutivo = $intConsecutivo;
            $this->strTipo = $strTipo;
            $sql="INSERT INTO tesosinreciboscaja (id_comp,fecha,vigencia,id_recaudo,recaudado,cuentacaja,cuentabanco,valor,estado,tipo,mediopago)
            VALUES(?,?,?,?,?,?,?,?,?,?,?)";
            $arrValues = [$this->intIdComp,$this->strFecha,$arrFecha[0],$this->intConsecutivo,$this->strTipo,$this->strCuentaCaja,$this->strCuentaBanco,$this->floatTotal,"S","3", $medio_pago];
            $request = $this->insert($sql,$arrValues);
            return $request;
        }
        public function insertReciboDet(int $intConsecutivo,string $strTercero,string $strFecha, array $arrData,array $arrBanco,string $strTipo, int $isSsf){
            $arrFecha = explode("-",$strFecha);
            $this->isSsf = $isSsf == 1 ? 'SSF' : 'CSF';
            $this->strCuentaBanco = $strTipo == "banco" ? $arrBanco['cuenta'] : "";
            $this->strCuentaCaja = $strTipo == "caja" ? getTesoParametros()['cuentacaja'] : "";
            $this->strTercero = $strTercero;
            $this->strFecha = $strFecha;
            $this->intConsecutivo = $intConsecutivo;
            $return ="";
            foreach ($arrData as $data) {
                $flagPtto = true;
                $flagComp = true;
                $centroCosto = getCentroCosto($data['codigo_cc'])['id_cc'];
                $idConcepto = explode("-",$data['concepto'])[0];
                $arrConcepto = getConceptoDet($idConcepto,4,"C",$centroCosto);
                $valorPtto = $data['valor'];
                if($data['cuentapres'] !="" && $valorPtto > 0){
                    //$intIdSeccion = getSeccionPresupuestoCentro($data['codigo_cc'])['id_sp'];
                    $sqlPtto = "INSERT INTO pptosinrecibocajappto (cuenta, idrecibo, valor, vigencia, fuente, productoservicio, seccion_presupuestal, medio_pago, vigencia_gasto)
                    VALUES(?,?,?,?,?,?,?,?,?)";
                    $arrValuesPtto = [
                        $data['cuentapres'],
                        $this->intConsecutivo,
                        $valorPtto,
                        $arrFecha[0],
                        $data['codigo_fuente'],
                        $data['cuenta_clasificadora'],
                        $data['codigo_cc'],
                        $this->isSsf,
                        1
                    ];
                    $resp = $this->insert($sqlPtto,$arrValuesPtto);
                    if($resp> 0){
                        $flagPtto = true;
                    }else{
                        $flagPtto = false;
                        break;
                    }
                }
                foreach($arrConcepto as $concepto){
                    if($concepto['debito'] == "S" && $concepto['tipocuenta'] == "N"){
                        $debito = 0;
                        $credito=$data['valor'];
                        $sql="INSERT INTO comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,valdebito,valcredito,estado,vigencia)
                        VALUES (?,?,?,?,?,?,?,?,?)";
                        $arrValuesDet = [
                            '25 '.$this->intConsecutivo,
                            $concepto['cuenta'],
                            $this->strTercero,
                            $centroCosto,
                            'Ingreso '.strtoupper($data['nombre_ingreso']),
                            $debito,
                            $credito,
                            1,
                            $arrFecha[0]
                        ];
                        $respComp = $this->insert($sql,$arrValuesDet);
                        if($respComp > 0 ){
                            $flagComp = true;
                        }else{
                            $flagComp = false;
                            break;
                        }
                        $debito = $credito;
                        $credito = 0;
                        $sql="INSERT INTO comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,valdebito,valcredito,estado,vigencia)
                        VALUES (?,?,?,?,?,?,?,?,?)";
                        $arrValuesDet = [
                            '25 '.$this->intConsecutivo,
                            $strTipo == "banco" ? $this->strCuentaBanco : $this->strCuentaCaja,
                            $this->strTercero,
                            $centroCosto,
                            'Ingreso '.strtoupper($data['nombre_ingreso']),
                            $debito,
                            $credito,
                            1,
                            $arrFecha[0]
                        ];
                        $respComp = $this->insert($sql,$arrValuesDet);
                        if($respComp > 0 ){
                            $flagComp = true;
                        }else{
                            $flagComp = false;
                            break;
                        }
                    }
                }

                if(!$flagComp){
                    $return = "comp";
                    break;
                }
                if(!$flagPtto){
                    $return = "ptto";
                    break;
                }
                $sql="INSERT into tesosinreciboscaja_det (id_recibos,ingreso,valor,estado) VALUES(?,?,?,?)";
                $arrValues = [$this->intConsecutivo,$data['codigo_ingreso'],$data['valor'],"S"];
                $respDet = $this->insert($sql,$arrValues);
                if($respDet == 0){
                    $return = "det";
                    break;
                }else{
                    $return = $respDet;
                }
            }
            return $return;
        }
        public function insertReversar(int $intConsecutivo, string $strFecha, string $strConcepto,array $data){
            //dep($data['com']);exit;
            $return ="";
            $this->intConsecutivo = $intConsecutivo;
            $this->strFecha = $strFecha;
            $this->strConcepto = $strConcepto;
            $this->intIdComp = $this->insertComprobanteCab(
                $this->strFecha,
                $this->strConcepto,
                $data['recibo']['cab']['valor'],
                $this->intConsecutivo,
                2025,
                1
            );
            if($this->intIdComp > 0){
                $request = $this->update("UPDATE tesosinreciboscaja SET estado=? WHERE id_recibos='{$data['recibo']['cab']['id_recibos']}'",['R']);
                if($request == 0){
                    $return = "recibo_cab";
                    return $return;
                }
                $request = $this->update("UPDATE tesosinrecaudos SET estado=? WHERE id_recaudo='{$data['recibo']['cab']['id_recaudo']}'",['S']);
                if($request == 0){
                    $return = "recaudo";
                    return $return;
                }
                $request = $this->delete("DELETE FROM pptosinrecibocajappto WHERE idrecibo = '{$data['recibo']['cab']['id_recibos']}'");
                if($request == 0){
                    $return = "ptto";
                    return $return;
                }

                $sqlInsert = "INSERT INTO tesosinreciboscaja(id_recibos,id_comp,fecha,vigencia,id_recaudo,recaudado,cuentacaja,cuentabanco,valor,estado,tipo,tipo_mov)
                VALUES(?,?,?,?,?,?,?,?,?,?,?,?)";
                $arrFecha = explode("-",$this->strFecha);
                $arrValues = [
                    $data['recibo']['cab']['id_recibos'],
                    $this->intIdComp,
                    $this->strFecha,
                    $arrFecha[0],
                    $data['recibo']['cab']['id_recaudo'],
                    $data['recibo']['cab']['recaudado'],
                    $data['recibo']['cab']['cuentacaja'],
                    $data['recibo']['cab']['cuentabanco'],
                    $data['recibo']['cab']['valor'],
                    "R",
                    $data['recibo']['cab']['tipo'],
                    301
                ];
                $requestInsert = $this->insert($sqlInsert,$arrValues);
                if($requestInsert == 0){
                    $return = "recibo";
                    return $return;
                }else{
                    insertAuditoria("teso_auditoria","teso_funciones_id",6,"Reversar",$requestInsert);
                    $arrDetComp = $data['comprobante']['det'];
                    foreach ($arrDetComp as $det) {
                        $debito = $det['valcredito'];
                        $credito = $det['valdebito'];
                        $numeroTipo = $det['numerotipo'];
                        $tipoComp = "20".$det['tipo_comp'];
                        $idComp = $tipoComp." ".$numeroTipo;
                        $detalle = "Reversion ".$det['detalle'];
                        $sqlInsertDet = "INSERT INTO comprobante_det(id_comp,cuenta,tercero,centrocosto,detalle,valdebito,valcredito,estado,vigencia,tipo_comp,numerotipo)
                        VALUES(?,?,?,?,?,?,?,?,?,?,?)";
                        $arrValues = [
                            $idComp,
                            $det['cuenta'],
                            $det['tercero'],
                            $det['centrocosto'],
                            $detalle,
                            $debito,
                            $credito,
                            1,
                            $arrFecha[0],
                            $tipoComp,
                            $numeroTipo
                        ];
                        $requestInsertDet = $this->insert($sqlInsertDet,$arrValues);
                        if($requestInsertDet == 0){
                            $return = "comp_det";
                            return $return;
                        }
                    }
                    $return = $requestInsert;
                }
            }else{
                $return = "comp";
            }
            return $return;
        }
        public function updateRecaudo(int $intConsecutivo){
            $this->intConsecutivo = $intConsecutivo;
            $sql="UPDATE tesosinrecaudos SET estado=? WHERE id_recaudo=$this->intConsecutivo";
            $request = $this->update($sql,["P"]);
            return $request;
        }
        public function selectIngreso(int $intConsecutivo){
            $this->intConsecutivo = $intConsecutivo;
            $sql = "SELECT * FROM tesosinreciboscaja WHERE id_recibos = $this->intConsecutivo";
            $request = $this->select($sql);
            $arrRecibo = [];
            if(!empty($request)){
                $sqlCompCab = "SELECT * FROM comprobante_cab WHERE id_comp ='$request[id_comp]'";
                $arrCompCab = $this->select($sqlCompCab);
                $sqlCompDet = "SELECT * FROM comprobante_det WHERE vigencia ='$request[vigencia]' AND tipo_comp = 25 AND numerotipo = '$arrCompCab[numerotipo]'";
                $arrCompDet = $this->select_all($sqlCompDet);
                $sqlDet = "SELECT * FROM tesosinreciboscaja_det WHERE id_recibos = $this->intConsecutivo";
                $arrRecibo['comprobante']['cab'] = $arrCompCab;
                $arrRecibo['comprobante']['det'] = $arrCompDet;
                $arrRecibo['recibo']['cab'] = $request;
                $arrRecibo['recibo']['det'] = $this->select_all($sqlDet);
            }
            return $arrRecibo;
        }
        public function selectSearch(string $strFecha, string $strFechaFinal,string $search){
            $this->strFecha = $strFecha;
            $this->strFechaFinal = $strFechaFinal;
            $sql = "SELECT tr.id_recibos,
            tr.id_recaudo,
            DATE_FORMAT(tr.fecha,'%d/%m/%Y') as fecha,
            tsr.concepto,
            tr.valor,
            tsr.tercero,
            tr.estado
            FROM tesosinreciboscaja tr
            INNER JOIN tesosinrecaudos tsr ON tsr.id_recaudo = tr.id_recaudo
            WHERE tr.fecha BETWEEN '$this->strFecha' AND '$this->strFechaFinal'
            AND tr.tipo_mov='101'
            ORDER BY tr.id_recibos DESC";
            $request = $this->select_all($sql);
            if(!empty($request)){
                $total = count($request);
                for ($i=0; $i < $total; $i++) {
                    $request[$i]['nombre'] = getNombreTercero($request[$i]['tercero']);
                    $request[$i]['total_format'] = formatNum($request[$i]['valor']);
                }
            }
            return $request;
        }
        public function selectEdit(int $intConsecutivo){
            $this->intConsecutivo = $intConsecutivo;
            $sql = "SELECT tr.id_recibos,
            tr.id_recaudo,
            tr.fecha,
            cab.concepto,
            tr.valor,
            tsr.tercero,
            tr.estado,
            tr.tipo_mov,
            tr.recaudado,
            tr.cuentabanco,
            tr.mediopago
            FROM tesosinreciboscaja tr
            LEFT JOIN comprobante_cab cab ON tr.id_comp = cab.id_comp
            INNER JOIN tesosinrecaudos tsr ON tsr.id_recaudo = tr.id_recaudo
            WHERE tr.id_recibos = $this->intConsecutivo";
            $request = $this->select_all($sql);
            if(!empty($request)){
                $total = count($request);
                for ($i=0; $i < $total; $i++) {
                    $id = $request[$i]['id_recaudo'];
                    $idRecibo = $request[$i]['id_recibos'];
                    $request[$i]['nombre'] = getNombreTercero($request[$i]['tercero']);
                    $request[$i]['total_format'] = formatNum($request[$i]['valor']);
                    $sqlDet = "SELECT
                        det.ingreso as codigo_ingreso,
                        det.valor,
                        det.estado,
                        det.seccion as codigo_cc,
                        COALESCE(cc.nombre,'') as nombre_centro,
                        det.fuente as codigo_fuente,
                        COALESCE(f.nombre,'') as nombre_fuente,
                        COALESCE(i.nombre,'') as nombre_ingreso
                        FROM tesosinrecaudos_det det
                        LEFT JOIN pptoseccion_presupuestal cc ON det.seccion = cc.id_seccion_presupuestal
                        LEFT JOIN ccpet_fuentes_cuipo f ON f.codigo_fuente = det.fuente
                        INNER JOIN tesoingresos i ON det.ingreso = i.codigo
                        WHERE id_recaudo = $id";
                    $sqlPres = "SELECT ptto.cuenta,ccpet.nombre,ptto.valor
                    FROM pptosinrecibocajappto ptto
                    INNER JOIN cuentasingresosccpetseleccionadas ccpet ON ccpet.codigo = ptto.cuenta
                    WHERE ptto.idrecibo = $idRecibo";
                    $request[$i]['det'] = $this->select_all($sqlDet);
                    $request[$i]['pres'] = $this->select_all($sqlPres);
                }
            }
            return $request;
        }
    }
?>
