<?php
    require_once '../../../Librerias/core/Helpers.php';
    require_once '../../../Librerias/core/Mysql.php';
    session_start();
    /*ini_set('display_errors', '1');
    ini_set('display_startup_errors', '1');
    error_reporting(E_ALL);*/
    class LiquidacionIngresoModel extends Mysql{
        private $intConsecutivo;
        private $intIdComp;
        private $strFecha;
        private $strFechaFinal;
        private $strTercero;
        private $strConcepto;
        private $floatTotal;
        private $intCausacion;
        function __construct(){
            parent::__construct();
        }
        public function selectSeccionesPresupuestales(){
            $sql = "SELECT id_seccion_presupuestal as codigo, nombre FROM pptoseccion_presupuestal WHERE estado='S'";
            $request = $this->select_all($sql);
            return $request;
        }
        public function selectTerceros(){
            $sql="SELECT cedulanit as codigo,
            CASE WHEN razonsocial IS NULL OR razonsocial = ''
            THEN CONCAT(nombre1,' ',nombre2,' ',apellido1,' ',apellido2)
            ELSE razonsocial END AS nombre FROM terceros ORDER BY id_tercero";
            $request = $this->select_all($sql);
            return $request;
        }
        public function selectCentroCostos(){
            $sql = "SELECT id_cc as codigo, nombre FROM centrocosto WHERE estado='S'";
            $request = $this->select_all($sql);
            return $request;
        }
        public function selectLiquidaciones(){
            $sql = "SELECT *,id_recaudo as codigo, valortotal as valor FROM tesosinrecaudos WHERE estado = 'S' ORDER BY id_recaudo DESC";
            $request = $this->select_all($sql);
            return $request;
        }
        public function selectIngresos(){
            $sql = "SELECT * FROM tesoingresos WHERE estado='S'";
            $request = $this->select_all($sql);
            if(!empty($request)){
                $total = count($request);
                for ($i=0; $i < $total ; $i++) {
                    $isStatus = false;
                    $codigo = $request[$i]['codigo'];
                    $arrFuentes = [];
                    if($request[$i]['is_cuenta']==2 && $request[$i]['is_tercero'] == 2){
                        if($request[$i]['tipo']=="S"){
                            $sqlTotalCuenta = "SELECT COALESCE(COUNT(cuentapres),0) as total FROM tesoingresos_det WHERE codigo = '$codigo'";
                            $sqlTotalFuente = "SELECT COALESCE(COUNT(*),0) as total FROM tesoingresos_fuentes WHERE codigo = '$codigo' AND estado = 'S'";
                            $sqlFuentes = "SELECT f.codigo_fuente as codigo, f.nombre
                            FROM tesoingresos_fuentes tf
                            INNER JOIN ccpet_fuentes_cuipo f ON tf.fuente = f.codigo_fuente
                            WHERE codigo = '$codigo' AND estado = 'S' ";
                        }else{
                            $sqlTotalCuenta = "SELECT COALESCE(COUNT(cuentapres),0) as total FROM tesoingresos_det WHERE codigo = '$codigo'";
                            $sqlTotalFuente = "SELECT COALESCE(COUNT(*),0) as total FROM tesoingresos_det WHERE codigo = '$codigo' AND estado = 'S'";
                            $sqlFuentes = "SELECT f.codigo_fuente as codigo, f.nombre,tf.porcentaje,
                            tf.seccion,
                            tf.concepto,
                            tf.cuentapres,
                            cuenta_clasificadora
                            FROM tesoingresos_det tf
                            INNER JOIN ccpet_fuentes_cuipo f ON tf.fuente = f.codigo_fuente
                            WHERE codigo = '$codigo' AND estado = 'S' ";
                        }

                        $totalCuentas = $this->select($sqlTotalCuenta)['total'];
                        $totalFuentes = $this->select($sqlTotalFuente)['total'];

                        $arrFuentes = $this->select_all($sqlFuentes);
                        if($request[$i]['tipo']=="S"){
                            $sqlCuenta = "SELECT cuentapres,cuenta_clasificadora,porcentaje,seccion,concepto FROM tesoingresos_det WHERE codigo = '$codigo'";
                            $cuenta = $this->select($sqlCuenta);
                            $arrFuentes[0]['cuentapres'] = $cuenta['cuentapres'];
                            $arrFuentes[0]['porcentaje'] = $cuenta['porcentaje'];
                            $arrFuentes[0]['seccion'] = $cuenta['seccion'];
                            $arrFuentes[0]['concepto'] = $cuenta['concepto'];
                            $arrFuentes[0]['cuenta_clasificadora'] = $cuenta['cuenta_clasificadora'];
                        }
                        if($totalCuentas > 0 && $totalFuentes > 0){
                            $isStatus = true;

                        }
                    }else if($request[$i]['is_cuenta']==1 || $request[$i]['is_tercero'] == 1){
                        $isStatus = true;
                        $request[$i]['concepto'] = $this->select("SELECT concepto FROM tesoingresos_det WHERE codigo = '$codigo'")['concepto'];
                    }
                    $request[$i]['is_status'] = $isStatus;
                    $request[$i]['fuentes'] = $arrFuentes;
                }
            }
            return $request;
        }
        public function insertComprobanteCab(int $intConsecutivo,string $strFecha,string $strConcepto,float $floatTotal,$comp =26,$estado = 1){
            $this->intConsecutivo = $intConsecutivo;
            $this->strFecha = $strFecha;
            $this->strConcepto = $strConcepto;
            $this->floatTotal = $floatTotal;
            $this->intIdComp = $comp;
            $sql = "INSERT INTO comprobante_cab(numerotipo,tipo_comp,fecha,concepto,total,total_debito,total_credito,diferencia,estado)
            VALUES(?,?,?,?,?,?,?,?,?)";
            $arrData = [$this->intConsecutivo,$this->intIdComp,$this->strFecha,$this->strConcepto,0,$this->floatTotal,$this->floatTotal,0,$estado];
            $request = $this->insert($sql,$arrData);
            return $request;
        }
        public function insertLiquidacionCab(int $intConsecutivo,string $strFecha,float $floatTotal,string $strConcepto,string $strTercero, string $strMedioPago){
            insertAuditoria("teso_auditoria","teso_funciones_id",5,"Crear",$intConsecutivo);
            $this->intConsecutivo = $intConsecutivo;
            $this->strFecha = $strFecha;
            $this->strConcepto = $strConcepto;
            $this->floatTotal = $floatTotal;
            $this->strTercero = $strTercero;
            $strYear = explode("-",$strFecha)[0];
            $sql="INSERT INTO tesosinrecaudos (id_comp,fecha,vigencia,tercero,valortotal,concepto,estado,medio_pago)
            VALUES(?,?,?,?,?,?,?,?)";
            $arrData = [$this->intConsecutivo,$this->strFecha,$strYear,$this->strTercero,$this->floatTotal,$this->strConcepto,"S",$strMedioPago];
            $request = $this->insert($sql,$arrData);
            return $request;
        }
        public function insertLiquidacionDet(int $intConsecutivo,array $arrData,int $intCausacion,int $intConsecLiquid,string $strTercero){
            $this->intConsecutivo = $intConsecutivo;
            $this->intCausacion = $intCausacion;
            $this->strTercero = $strTercero;
            $vigencia = date("Y");
            $request = 0;
            foreach ($arrData as $data) {
                $centroCosto = getCentroCosto($data['centro']['codigo'])['id_cc'];
                if($intCausacion == 1){
                    $ingreso = $data['ingreso'];
                    $fuente = $data['fuente'];
                    $indexConcepto = explode("-",$fuente['concepto'])[0];
                    $idConcepto = $indexConcepto!="" ? $indexConcepto : $ingreso['concepto'];
                    $arrConcepto = getConceptoDet($idConcepto,4,"C",$centroCosto);
                    foreach($arrConcepto as $concepto){
                        $debito = $concepto['debito'] == "S" ?$data['valor'] : 0 ;
                        $credito = $concepto['debito'] != "S" ?$data['valor'] : 0 ;
                        $sql="INSERT INTO comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,valdebito,valcredito,estado,vigencia)
                        VALUES (?,?,?,?,?,?,?,?,?)";
                        $arrValuesDet = [
                            '26 '.$intConsecLiquid,
                            $concepto['cuenta'],
                            $this->strTercero,
                            $centroCosto,
                            'Causacion '.strtoupper($ingreso['codigo'])." ".$ingreso['nombre'],
                            $debito,
                            $credito,
                            1,
                            $vigencia
                        ];
                        $this->insert($sql,$arrValuesDet);
                    }
                }
                $sql="INSERT INTO tesosinrecaudos_det (id_recaudo,ingreso,valor,estado,cc,fuente,seccion,cuentapres,cuenta_clasificadora)
                VALUES(?,?,?,?,?,?,?,?,?)";
                $arrValues = [
                    $this->intConsecutivo,
                    $data['ingreso']['codigo'],
                    $data['valor'],'S',
                    $centroCosto,$data['fuente']['codigo'],
                    $data['centro']['codigo'],
                    $data['fuente']['codigo'] != "" ? $data['fuente']['cuentapres'] : "",
                    $data['fuente']['codigo'] != "" ? $data['fuente']['cuenta_clasificadora'] : ""
                ];
                $request = $this->insert($sql,$arrValues);
            }
            return $request;
        }
        public function insertReversar(int $intConsecutivo,string $strFecha,string $strConcepto,array $data){
            $this->intConsecutivo = $intConsecutivo;
            $this->strFecha = $strFecha;
            $this->strConcepto = $strConcepto;
            $strYear = explode("-",$strFecha)[0];
            $sqlValid  = "SELECT * FROM tesosinrecaudos WHERE tipo_mov = 101 AND estado = 'R' AND id_recaudo='{$this->intConsecutivo}'";
            $requestValid = $this->select($sqlValid);
            if(empty($requestValid)){
                $this->intIdComp = $this->insertComprobanteCab(
                    $this->intConsecutivo,
                    $this->strFecha,
                    $this->strConcepto,
                    $data['valortotal'],
                    2026
                );
                if($this->intIdComp > 0){
                    $request = $this->update("UPDATE tesosinrecaudos SET estado=? WHERE id_recaudo='{$this->intConsecutivo}'",['R']);
                    if($request == 0){
                        $return = "recaudo";
                        return $return;
                    }
                    $sql="INSERT INTO tesosinrecaudos (id_recaudo,id_comp,fecha,vigencia,tercero,valortotal,concepto,estado,tipo_mov)
                    VALUES(?,?,?,?,?,?,?,?,?)";
                    $arrData = [$this->intConsecutivo,$this->intIdComp,$this->strFecha,$strYear,$data['tercero'],$data['valortotal'],$this->strConcepto,"R",301];
                    $request = $this->insert($sql,$arrData);
                    if($request == 0){
                        $return = "recaudo_cab";
                        return $return;
                    }else{
                        insertAuditoria("teso_auditoria","teso_funciones_id",5,"Reversar",$this->intConsecutivo);
                        $arrDetComp = $data['comprobante']['det'];
                        if(count($arrDetComp) > 0){
                            foreach ($arrDetComp as $det) {
                                $debito = $det['valcredito'];
                                $credito = $det['valdebito'];
                                $numeroTipo = $det['numerotipo'];
                                $tipoComp = "20".$det['tipo_comp'];
                                $idComp = $tipoComp." ".$numeroTipo;
                                $detalle = "Reversion ".$det['detalle'];
                                $sqlInsertDet = "INSERT INTO comprobante_det(id_comp,cuenta,tercero,centrocosto,detalle,valdebito,valcredito,estado,vigencia,tipo_comp,numerotipo)
                                VALUES(?,?,?,?,?,?,?,?,?,?,?)";
                                $arrValues = [
                                    $idComp,
                                    $det['cuenta'],
                                    $det['tercero'],
                                    $det['centrocosto'],
                                    $detalle,
                                    $debito,
                                    $credito,
                                    1,
                                    $strYear,
                                    $tipoComp,
                                    $numeroTipo
                                ];
                                $requestInsertDet = $this->insert($sqlInsertDet,$arrValues);
                                if($requestInsertDet == 0){
                                    $return = "comp_det";
                                    return $return;
                                }
                            }
                        }
                        $return = $request;
                    }
                }else{
                    $return = "comp";
                }
            }else{
                $return = "existe";
            }
            return $return;
        }
        public function selectRecaudo(int $intConsecutivo){
            $this->intConsecutivo = $intConsecutivo;
            $sql = "SELECT * FROM tesosinrecaudos WHERE id_recaudo = $this->intConsecutivo";
            $request = $this->select($sql);
            if(!empty($request)){
                $sqlCompCab = "SELECT * FROM comprobante_cab WHERE id_comp = {$request['id_comp']}";
                $arrComp = $this->select($sqlCompCab);
                $sqlCompDet = "SELECT * FROM comprobante_det WHERE numerotipo = {$arrComp['numerotipo']} AND tipo_comp = {$arrComp['tipo_comp']}";
                $arrCompDet = $this->select_all($sqlCompDet);
                $request['comprobante']['cab'] = $arrComp;
                $request['comprobante']['det'] = $arrCompDet;
            }
            return $request;
        }
        public function selectSearch(string $strFecha, string $strFechaFinal,string $search){
            $this->strFecha = $strFecha;
            $this->strFechaFinal = $strFechaFinal;
            $sql = "SELECT *,DATE_FORMAT(fecha,'%d/%m/%Y') as fecha FROM tesosinrecaudos
            WHERE fecha BETWEEN '$this->strFecha' AND '$this->strFechaFinal' AND tipo_mov = '101'
            AND (tercero like '$search%' OR concepto like '$search%' OR id_recaudo like '$search%')
            ORDER BY id_recaudo DESC";
            $request = $this->select_all($sql);
            if(!empty($request)){
                $total = count($request);
                for ($i=0; $i < $total; $i++) {
                    $request[$i]['nombre'] = getNombreTercero($request[$i]['tercero']);
                    $request[$i]['total_format'] = formatNum($request[$i]['valortotal']);
                }
            }
            return $request;
        }
        public function selectEdit(int $intConsecutivo){
            $this->intConsecutivo = $intConsecutivo;
            $sql = "SELECT * FROM tesosinrecaudos WHERE id_recaudo = $this->intConsecutivo";
            $request = $this->select_all($sql);
            if(!empty($request)){
                $total = count($request);
                for ($i=0; $i < $total; $i++) {
                    $sqlDet = "SELECT
                    det.ingreso as codigo_ingreso,
                    det.valor,
                    det.estado,
                    det.seccion as codigo_cc,
                    COALESCE(cc.nombre,'') as nombre_centro,
                    det.fuente as codigo_fuente,
                    COALESCE(f.nombre,'') as nombre_fuente,
                    COALESCE(i.nombre,'') as nombre_ingreso
                    FROM tesosinrecaudos_det det
                    LEFT JOIN pptoseccion_presupuestal cc ON det.seccion = cc.id_seccion_presupuestal
                    LEFT JOIN ccpet_fuentes_cuipo f ON f.codigo_fuente = det.fuente
                    LEFT JOIN tesoingresos i ON det.ingreso = i.codigo
                    WHERE id_recaudo = $this->intConsecutivo";

                    if($request[$i]['estado'] == "P"){
                        $sql = "SELECT id_recibos FROM tesosinreciboscaja WHERE estado = 'S' AND id_recaudo = $this->intConsecutivo";
                        $request[$i]['ingreso_interno'] = $this->select($sql)['id_recibos'];
                    }
                    $request[$i]['det'] = $this->select_all($sqlDet);
                    $request[$i]['nombre_tercero'] = getNombreTercero($request[$i]['tercero']);

                }
            }
            return $request;
        }
    }
?>
