<?php
    require_once '../../../comun.inc';
    require '../../../funciones.inc';
    require '../../../funcionesSP.inc.php';
    /*ini_set('display_errors', '1');
    ini_set('display_startup_errors', '1');
    error_reporting(E_ALL);*/
    session_start();
    if($_POST){
        $obj = new Plantilla();
        if($_POST['action']=="get"){
            $obj->getData();
        }else if($_POST['action'] == "save"){
            $obj->save();
        }else if($_POST['action'] =="progreso"){
            $obj->getProgreso();
        }
    }

    class Plantilla{
        private $linkbd;
        private $strVigencia;
        private $intConsecutivo;
        private $intComp;
        private $strTercero;
        private $incremento;
        public function __construct() {
            $this->linkbd = conectar_v7();
            $this->linkbd->set_charset("utf8");
            $this->strVigencia = date("Y");
            $this->intComp = 1;
            $this->strTercero = mysqli_query($this->linkbd, "SELECT nit FROM configbasica WHERE estado='S'")->fetch_assoc()['nit'];
        }
        public function getProgreso(){
            if(!empty($_SESSION)){
                echo json_encode($_SESSION['progreso'],JSON_UNESCAPED_UNICODE);
            }
            die();
        }
        public function getData(){
            if(!empty($_SESSION)){
                $request = $this->selectData();
                echo json_encode($request,JSON_UNESCAPED_UNICODE);
            }
            die();
        }
        public function save(){
            if(!empty($_SESSION)){
                $request = $this->selectData()['predios'];
                if(!empty($request)){
                    $grupos_det = 400;
                    $total = count($request);
                    $total_comp = ceil($total/$grupos_det);
                    $index = 0;
                    for ($i=0; $i < $total_comp ; $i++) {
                        $progreso = (($i+1)/$total_comp)*100;
                        $progreso = intval($progreso);
                        $_SESSION['progreso'] = $progreso;
                        //Guardo comprobante
                        $this->insertComprobante();
                        //Guardo detalle de comprobante por grupos

                        for ($j=0; $j < $grupos_det ; $j++) {
                            if(isset($request[$index])){

                                $concepto_predio = $request[$index]['concepto_predio'];
                                $concepto_ambiental = $request[$index]['concepto_ambiental'];
                                $detalle_predio = substr($request[$index]['codigo_catastro'],0,2) == "00" ? 20 : 21;
                                $detalle_ambiental = "AMBIENTAL";
                                $request[$index]['debito_ambiental'] = getCuentaConcepto("PR",$concepto_ambiental,"debito","4");
                                $request[$index]['credito_ambiental'] = getCuentaConcepto("PR",$concepto_ambiental,"credito","4");
                                $request[$index]['debito_predio'] = getCuentaConcepto("PR",$concepto_predio,"debito","4");
                                $request[$index]['credito_predio'] = getCuentaConcepto("PR",$concepto_predio,"credito","4");

                                //Predial
                                $this->insertCompDetalleDeb(
                                    $request[$index]['codigo_catastro'],
                                    $request[$index]['debito_predio'],
                                    $request[$index]['predial'],
                                    $detalle_predio
                                );
                                $this->insertCompDetalleCred(
                                    $request[$index]['codigo_catastro'],
                                    $request[$index]['credito_predio'],
                                    $request[$index]['predial'],
                                    $detalle_predio
                                );
                                //Ambiental
                                $this->insertCompDetalleDeb(
                                    $request[$index]['codigo_catastro'],
                                    $request[$index]['debito_ambiental'],
                                    $request[$index]['ambiental'],
                                    $detalle_ambiental
                                );
                                $this->insertCompDetalleCred(
                                    $request[$index]['codigo_catastro'],
                                    $request[$index]['credito_ambiental'],
                                    $request[$index]['ambiental'],
                                    $detalle_ambiental
                                );
                            }else{
                                break;
                            }
                            $index++;
                        }
                    }
                    $arrResponse = array("status"=>true,"msg"=>"Reconocimiento predial guardado");
                }else{
                    $arrResponse = array("status"=>false,"msg"=>"No hay predios para reconocer");
                }
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
            die();
        }
        public function selectData(){
            $arrPredios = array();
            $arrPrediosCompDet = array();

            //Traigo información de predios
            $sql="SELECT p.codigo_catastro,
            i.direccion,
            pro.documento,
            pro.nombre_propietario,
            a.valor_avaluo,
            a.tasa_por_mil
            FROM predios as p
            INNER JOIN predio_avaluos a
            ON a.predio_id = p.id
            INNER JOIN predio_informacions i
            ON i.predio_id = p.id
            INNER JOIN predio_propietarios pro
            ON p.id = pro.predio_id
            WHERE YEAR(p.created_at) = '$this->strVigencia' AND YEAR(a.vigencia) = '$this->strVigencia'
            AND YEAR(p.created_at) ='$this->strVigencia' AND p.main_propietario = pro.orden
            AND i.direccion !=''
            GROUP BY p.codigo_catastro ORDER BY  p.id";

            $request = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
            $sql = "SELECT ambiental_predial,ambiental_tarifa,ambiental_tasa FROM estatutos WHERE vigencia_hasta = '$this->strVigencia'";
            $estatuto = mysqli_query($this->linkbd,$sql)->fetch_assoc();
            $total = count($request);

            //Traigo predios registrados en detalle
            $sqlCompDet = "SELECT cedula_catastral FROM comprobante_det
            WHERE vigencia = '$this->strVigencia' AND cedula_catastral != ''";
            $request_comp_det = mysqli_fetch_all(mysqli_query($this->linkbd,$sqlCompDet),MYSQLI_ASSOC);
            $total_det = count($request_comp_det);

            for ($i=0; $i < $total_det ; $i++) {
                $arrPrediosCompDet[$i] = $request_comp_det[$i]['cedula_catastral'];
            }
            $arrPrediosCompDet =  array_values(array_unique($arrPrediosCompDet));
            if($total > 0){
                for ($i=0; $i < $total; $i++) {
                    //Agrego en arrPredios si el predio no se encuentra registrado
                    if(!in_array($request[$i]['codigo_catastro'],$arrPrediosCompDet)){
                        $avaluo = $request[$i]['valor_avaluo'];
                        $tasa = abs($request[$i]['tasa_por_mil']);
                        $predial = ($avaluo *$tasa)/1000;
                        $ambiental = 0;
                        $tasa_ambiental = $estatuto['ambiental_tasa'];
                        if($estatuto['ambiental_predial']){
                            $ambiental = ceil($predial *($tasa_ambiental/100));
                        }else{
                            $ambiental = ceil(($avaluo*$tasa_ambiental)/1000);
                        }

                        $tipoPredio = substr($request[$i]['codigo_catastro'],0,2) == "00" ? 21 : 20;

                        $request[$i]['formato_avaluo'] = number_format($avaluo,0,",",".");
                        $request[$i]['formato_predial'] = number_format($predial,0,",",".");
                        $request[$i]['formato_ambiental'] = number_format($ambiental,0,",",".");
                        $request[$i]['predial'] = $predial;
                        $request[$i]['ambiental'] = $ambiental;
                        $request[$i]['concepto_predio'] = $tipoPredio;
                        $request[$i]['concepto_ambiental'] = 33;
                        $request[$i]['tasa'] = $tasa;
                        array_push($arrPredios,$request[$i]);
                    }
                }
            }
            return array("predios"=>$arrPredios,"resultados"=>count($arrPredios));
        }
        public function selectConsecutivoComp(){
            $sql = "SELECT MAX(numerotipo) as consecutivo FROM comprobante_cab WHERE tipo_comp = 1";
            $request = mysqli_query($this->linkbd,$sql)->fetch_assoc()['consecutivo']+1;
            return $request;
        }
        public function insertComprobante(){
            $this->intConsecutivo = $this->selectConsecutivoComp();
            $strDescripcion = "RECONOCIMIENTO PREDIAL VIGENCIA ".$this->strVigencia;
            $fecha = $this->strVigencia."-01-15";
            $sql = "INSERT INTO comprobante_cab (numerotipo,tipo_comp,fecha,concepto,total,total_debito,total_credito,estado)
            VALUES($this->intConsecutivo,1,'$fecha','$strDescripcion',0,0,0,1)";
            $request = intval(mysqli_query($this->linkbd,$sql));
            return $request;
        }
        public function insertCompDetalleDeb($cedula,$cuenta,$valor,$detalle){
            $idComprobante =  $this->intComp." ".$this->intConsecutivo;
            $detalle = "RECONOCIMIENTO PREDIAL ".$detalle." VIGENCIA ".$this->strVigencia;
            $sql = "INSERT INTO comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,valdebito,valcredito,estado,
            vigencia,tipo_comp,numerotipo,cedula_catastral)
            VALUES('$idComprobante',
                '$cuenta',
                '$this->strTercero',
                '01',
                '$detalle',
                $valor,
                0,
                1,
                '$this->strVigencia',
                $this->intComp,
                $this->intConsecutivo,
                '$cedula'
            )";
            $request = mysqli_query($this->linkbd,$sql);
            return $request;
        }
        public function insertCompDetalleCred($cedula,$cuenta,$valor,$detalle){
            $idComprobante =  $this->intComp." ".$this->intConsecutivo;
            $detalle = "RECONOCIMIENTO PREDIAL ".$detalle." VIGENCIA ".$this->strVigencia;
            $sql = "INSERT INTO comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,valdebito,valcredito,estado,
            vigencia,tipo_comp,numerotipo,cedula_catastral)
            VALUES('$idComprobante',
                '$cuenta',
                '$this->strTercero',
                '01',
                '$detalle',
                0,
                $valor,
                1,
                '$this->strVigencia',
                $this->intComp,
                $this->intConsecutivo,
                '$cedula'
            )";
            $request = mysqli_query($this->linkbd,$sql);
            return $request;
        }
    }
?>
