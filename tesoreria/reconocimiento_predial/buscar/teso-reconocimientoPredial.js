const URL ='tesoreria/reconocimiento_predial/buscar/teso-reconocimientoPredial.php';

var app = new Vue({
	el:"#myapp",
	data() {
		return {
			isLoading:false,
			arrData:[],
            txtVigencia: new Date().getFullYear(),
            txtLimite: 0,
            txtResultados: 0
		}
	},
	mounted() {
		this.getData();
	},
	methods: {

		getData: async function(){
			const formData = new FormData();
			formData.append("action","get");
            this.isLoading = true;
			const response = await fetch(URL,{method:"POST", body:formData});
			const objData = await response.json();
			this.arrData = objData.predios;
            this.txtResultados = objData.resultados;
            this.isLoading = false;
		},
        save: async function(){
            const formData = new FormData();
            formData.append("action","save");
            this.isLoading = true;
            const response = await fetch(URL,{method:"POST", body:formData});
            const objData = await response.json();
            if(objData.status){
                this.isLoading = false;
                this.txtResultados = 0;
                this.arrData = [];
                Swal.fire("Guardado",objData.msg,"success");
            }else{
                this.arrData = [];
                this.txtResultados = 0;
                this.isLoading = false;
                Swal.fire("Error",objData.msg,"error");
            }
        },
		formatNumero: function(valor){
            return new Intl.NumberFormat("en-US", {style: "currency", currency: "USD"}).format(valor);
        },
	},
	computed:{

	}
})
