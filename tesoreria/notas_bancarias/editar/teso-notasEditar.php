<?php
    require_once '../../../comun.inc';
    require '../../../funciones.inc';
    require '../../../funcionesSP.inc.php';
    /*ini_set('display_errors', '1');
    ini_set('display_startup_errors', '1');
    error_reporting(E_ALL);*/
    session_start();
    if($_POST){
        $obj = new Plantilla();
        if($_POST['action']=="search"){
            $obj->search($_POST['search'],$_POST['option']);
        }else if($_POST['action']=="get"){
            $obj->getData($_POST['codigo']);
        }else if($_POST['action'] == "save"){
            $obj->save($_POST['data']);
        }else if($_POST['action'] == "clasificador"){
            $obj->selectClasificador($_POST['cuenta']);
        }
    }

    class Plantilla{
        private $linkbd;
        private $arrData;
        private $strVigencia;
        public function __construct() {
            $this->linkbd = conectar_v7();
            $this->linkbd->set_charset("utf8");
            $this->strVigencia = vigencia_usuarios($_SESSION['cedulausu']);
        }
        public function search(string $search,string $option){
            if(!empty($_SESSION)){
                if($option =="modal_cuenta"){
                    $request['modal_cuentas'] = $this->selectCuentas($search);
                }else if($option=="codigo_cuenta"){
                    $request['modal_cuentas'] = $this->selectCuentas($search,true);
                }
                if($option=="modal_fuente"){
                    $request['modal_fuentes'] = $this->selectFuentes($search);
                }else if($option=="codigo_fuente"){
                    $request['modal_fuentes'] = $this->selectFuentes($search,true);
                }
                if($option=="modal_bien"){
                    $request['modal_bienes'] = $this->selectBienes($search);
                }else if($option=="codigo_bien"){
                    $request['modal_bienes'] = $this->selectBienes($search,true);
                }
                if($option=="modal_servicio"){
                    $request['modal_servicios'] = $this->selectServicios($search);
                }else if($option=="codigo_servicio"){
                    $request['modal_servicios'] = $this->selectServicios($search,true);
                }
                if($option=="modal_complemento"){
                    $request['modal_complementos'] = $this->selectComplementarios($search);
                }else if($option=="codigo_complemento"){
                    $request['modal_complementos'] = $this->selectComplementarios($search,true);
                }
                echo json_encode($request,JSON_UNESCAPED_UNICODE);
            }
            die();
        }
        public function getData($codigo){
            if(!empty($_SESSION)){
                $nota = $this->selectNota($codigo);
                if(!empty($nota)){
                    $request['consecutivo'] = $codigo;
                    $request['conceptos'] = $this->selectConceptos();
                    $request['modal_cuentas'] = $this->selectCuentas();
                    $request['modal_fuentes'] = $this->selectFuentes();
                    $request['modal_bienes'] = $this->selectBienes();
                    $request['modal_servicios'] = $this->selectServicios();
                    $request['modal_complementos'] = $this->selectComplementarios();
                    $request['nota'] = $nota;
                    $request['max'] = $this->selectMax();
                    $arrResponse = array("status"=>true,"data"=>$request);
                }else{
                    $arrResponse = array("status"=>false,"data"=>$this->selectMax());
                }
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
            die();
        }

        public function save($data){
            if(!empty($_SESSION)){
                $arrData = json_decode($data,true);
                //dep($arrData);exit;
                if(empty($arrData['cabecera']) || empty($arrData['detalle'])){
                    $arrResponse = array("status"=>false,"msg"=>"Error de datos");
                }else{
                    $request = $this->updateData($arrData);
                    if(is_numeric($request) && $request > 0){
                        $arrResponse = array("status"=>true,"msg"=>"Datos guardados");
                    }else if($request=="existe"){
                        $arrResponse = array("status"=>false,"msg"=>"La nota bancaria con este nombre ya existe, intente con otro.");
                    }else{
                        $arrResponse = array("status"=>false,"msg"=>"Ha ocurrido un error, no se ha podido guardar.");
                    }
                }
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
            die();
        }
        public function updateData(array $data){
            $this->arrData = $data;
            $cabecera= $this->arrData['cabecera'];
            $nombre = strtoupper($cabecera['nombre']);
            $detalle= $this->arrData['detalle'];
            $sql = "SELECT * FROM tesogastosbancarios  WHERE codigo != '$cabecera[codigo]' AND nombre = '$cabecera[nombre]'";
            $request = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
            if(empty($request)){
                //Cabecera
                $sql = "UPDATE tesogastosbancarios SET nombre='$nombre',tipo='$cabecera[tipo]',estado='$cabecera[estado]' WHERE codigo = '$cabecera[codigo]'";
                mysqli_query($this->linkbd,$sql);
                mysqli_query($this->linkbd,"DELETE FROM tesogastosbancarios_det WHERE codigo = '$cabecera[codigo]' AND vigencia='$this->strVigencia'");
                //Detalle
                $sql = "INSERT INTO tesogastosbancarios_det(codigo,concepto,modulo,tipoconce,cuentapres,estado,vigencia,fuente,cuenta_clasificadora)
                VALUES('$cabecera[codigo]','$detalle[concepto]','4','GB','$detalle[cuenta]','S','$this->strVigencia','$detalle[fuente]','$detalle[clasificador]')";
                $request = intval(mysqli_query($this->linkbd,$sql));
            }else{
                $request = "existe";
            }
            return $request;
        }
        public function selectNota($codigo){
            $sql = "SELECT * FROM tesogastosbancarios WHERE codigo = CAST('$codigo' as int)";
            $request= mysqli_query($this->linkbd,$sql)->fetch_assoc();
            if(!empty($request)){
                $detalle= mysqli_query(
                    $this->linkbd,
                    "SELECT * FROM tesogastosbancarios_det WHERE codigo = CAST('$codigo' as int) ORDER BY vigencia DESC"
                )->fetch_assoc();
                if($request['tipo'] =="I"){
                    $detalle['cuenta'] = mysqli_query(
                        $this->linkbd,
                        "SELECT codigo, nombre FROM cuentasingresosccpetseleccionadas WHERE codigo = '$detalle[cuentapres]'"
                    )->fetch_assoc();
                    $detalle['fuente'] = mysqli_query(
                        $this->linkbd,
                        "SELECT codigo_fuente as codigo, nombre
                        FROM ccpet_fuentes_cuipo
                        WHERE version = '1' AND codigo_fuente = '$detalle[fuente]'"
                    )->fetch_assoc();
                    $clasificador = $this->selectClasificador($detalle['cuentapres'],false);
                    if($clasificador != null){
                        $detalle['clasificador'] = $clasificador;
                        $detalle['nombre_clasificador'] = $this->selectNombreClasificador($clasificador,$detalle['cuenta_clasificadora']);
                    }
                    $request['detalle'] = $detalle;
                }else{
                    $request['detalle'] = $detalle;
                }
            }
            return $request;
        }
        public function selectConsecutivo(){
            $sql = "SELECT  MAX(RIGHT(codigo,2)) as id from tesogastosbancarios";
            $request = mysqli_query($this->linkbd,$sql)->fetch_assoc()['id']+1;
            return "0".$request;
        }
        public function selectConceptos(){
            $sql = "SELECT codigo,nombre,tipo FROM conceptoscontables WHERE modulo='4' AND tipo='GB' ORDER BY codigo";
            $request = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
            return $request;
        }
        public function selectCuentas($search="",$flag = false){
            $tipoBusqueda = "like '$search%'";
            if($flag){
                $tipoBusqueda = "='$search'";
            }
            $sql = "SELECT codigo, nombre, tipo, nivel
            FROM cuentasingresosccpetseleccionadas
            WHERE nombre $tipoBusqueda OR codigo $tipoBusqueda";
            $request['data'] = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
            $request['results'] = mysqli_query(
                $this->linkbd,
                "SELECT count(*) as total
                FROM cuentasingresosccpetseleccionadas
                WHERE nombre $tipoBusqueda OR codigo $tipoBusqueda")->fetch_assoc()['total'];
            return $request;
        }
        public function selectFuentes($search="",$flag=false){
            $tipoBusqueda = "like '$search%'";
            if($flag){
                $tipoBusqueda = "='$search'";
            }
            $sql="SELECT codigo_fuente as codigo, nombre
            FROM ccpet_fuentes_cuipo
            WHERE version = '1' AND
            (codigo_fuente $tipoBusqueda OR nombre $tipoBusqueda)
            ORDER BY codigo_fuente ASC";
            $request['data'] = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
            $request['results'] = mysqli_query(
                $this->linkbd,
                "SELECT count(*) as total
                FROM ccpet_fuentes_cuipo
                WHERE version = '1' AND
                (codigo_fuente $tipoBusqueda OR nombre $tipoBusqueda)"
                )->fetch_assoc()['total'];
            return $request;
        }
        public function selectClasificador($cuenta,$flag=true){
            $sql = "SELECT clasificadores FROM ccpetprogramarclasificadores WHERE cuenta = '$cuenta'";
            $request = mysqli_query($this->linkbd,$sql)->fetch_assoc()['clasificadores'];
            if(!$flag){
                return $request;
            }else{
                echo json_encode($request,JSON_UNESCAPED_UNICODE);
            }
            die();
        }
        public function selectServicios($search="",$flag=false){
            $tipoBusqueda = "like '$search%'";
            if($flag){
                $tipoBusqueda = "='$search'";
            }
            $sql = "SELECT grupo as codigo, titulo as nombre
                    FROM ccpetservicios
                    WHERE version = (SELECT MAX(version) FROM ccpetbienestransportables)
                    AND LENGTH(grupo) = 5 AND (grupo $tipoBusqueda OR titulo $tipoBusqueda)
                    ORDER BY grupo ASC"
            ;
            $request['data'] = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
            $request['results'] = mysqli_query(
                $this->linkbd,
                "SELECT count(*) as total
                FROM ccpetservicios
                WHERE version = (SELECT MAX(version) FROM ccpetbienestransportables)
                AND LENGTH(grupo) = 5 AND (grupo $tipoBusqueda OR titulo $tipoBusqueda)"
                )->fetch_assoc()['total'];
            return $request;

        }
        public function selectBienes($search="",$flag=false){
            $tipoBusqueda = "like '$search%'";
            if($flag){
                $tipoBusqueda = "='$search'";
            }
            $sql = "SELECT grupo as codigo, titulo as nombre
                    FROM ccpetbienestransportables
                    WHERE version = (SELECT MAX(version) FROM ccpetbienestransportables)
                    AND LENGTH(grupo) = 7 AND (grupo $tipoBusqueda OR titulo $tipoBusqueda)
                    ORDER BY grupo ASC"
            ;
            $request['data'] = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
            $request['results'] = mysqli_query(
                $this->linkbd,
                "SELECT count(*) as total
                FROM ccpetbienestransportables
                WHERE version = (SELECT MAX(version) FROM ccpetbienestransportables)
                AND LENGTH(grupo) = 7 AND (grupo $tipoBusqueda OR titulo $tipoBusqueda)"
                )->fetch_assoc()['total'];
            return $request;

        }
        public function selectComplementarios($search="",$flag=false){
            $tipoBusqueda = "like '$search%'";
            if($flag){
                $tipoBusqueda = "='$search'";
            }
            $sql="SELECT  codigo, nombre
            FROM ccpet_catalogocomp_ingresos
            WHERE codigo $tipoBusqueda OR nombre $tipoBusqueda
            ORDER BY codigo ASC";
            $request['data'] = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
            $request['results'] = mysqli_query(
                $this->linkbd,
                "SELECT count(*) as total
                FROM ccpet_catalogocomp_ingresos
                WHERE codigo $tipoBusqueda OR nombre $tipoBusqueda"
                )->fetch_assoc()['total'];
            return $request;
        }
        public function selectNombreClasificador($clasificador,$codigo){
            $sql ="";
            if($clasificador == 1){
                $sql  = "SELECT nombre FROM ccpet_cuin WHERE codigo_cuin = '$codigo'";
            }else if($clasificador == 2){
                $sql="SELECT titulo as nombre FROM ccpetbienestransportables WHERE grupo = '$codigo'";
            }else if($clasificador == 3){
                $sql="SELECT titulo as nombre FROM ccpetservicios WHERE grupo = '$codigo'";
            }
            else if($clasificador == 4){
                $sql="SELECT nombre FROM ccpet_catalogocomp_ingresos WHERE codigo = '$codigo'";
            }
            $request = mysqli_query($this->linkbd,$sql)->fetch_assoc()['nombre'];
            return $request;
        }
        public function selectMax(){
            $sql = "SELECT MAX(CAST(codigo as int)) as id FROM tesogastosbancarios";
            $request = mysqli_query($this->linkbd,$sql)->fetch_assoc()['id'];
            return $request;
        }
    }
?>
