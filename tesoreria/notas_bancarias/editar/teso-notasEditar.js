const URL ='tesoreria/notas_bancarias/editar/teso-notasEditar.php';

var app = new Vue({
    el:"#myapp",
    data() {
        return {
            isLoading:false,
            isModal:false,
            isModalFuente:false,
            isModalServicios:false,
            isModalBienes:false,
            isModalComplemento:false,
            arrConceptos:[],
            arrModalCuentas:[],
            arrModalFuentes:[],
            arrModalServicios:[],
            arrModalBienes:[],
            arrModalComplementos:[],
            objCuenta:{"codigo":"","nombre":""},
            objFuente:{"codigo":"","nombre":""},
            objBien:{"codigo":"","nombre":""},
            objServicio:{"codigo":"","nombre":""},
            objComplemento:{"codigo":"","nombre":""},
            selectNota:"I",
            selectConcepto:"",
            txtNombre:"",
            txtSearch:"",
            txtSearchFuente:"",
            txtSearchBien:"",
            txtSearchServicio:"",
            txtSearchComplemento:"",
            txtResults: 0,
            txtResultsTerceros:0,
            txtConsecutivo:0,
            txtResultsModalCuentas:0,
            txtResultsModalFuentes:0,
            txtResultsModalBienes:0,
            txtResultsModalServicios:0,
            txtResultsModalComplementos:0,
            txtType:0,
            txtMax:0,
            txtCodigo:"",
        }
    },
    mounted() {
        this.getData();
    },
    methods: {

        getData: async function(){
            let codigo = new URLSearchParams(window.location.search).get('id');
            this.txtConsecutivo = parseInt(codigo);
            const formData = new FormData();
            formData.append("action","get");
            formData.append("codigo",codigo);
            const response = await fetch(URL,{method:"POST", body:formData});
            const objData = await response.json();
            if(objData.status){
                let data = objData.data;
                let nota = data.nota;
                this.arrConceptos = data.conceptos;
                this.arrModalCuentas = data.modal_cuentas.data;
                this.arrModalFuentes = data.modal_fuentes.data;
                this.arrModalBienes = data.modal_bienes.data;
                this.arrModalServicios = data.modal_servicios.data;
                this.arrModalComplementos = data.modal_complementos.data;

                this.txtResultsModalCuentas = data.modal_cuentas.results;
                this.txtResultsModalFuentes = data.modal_fuentes.results;
                this.txtResultsModalBienes=data.modal_bienes.results;
                this.txtResultsModalServicios=data.modal_servicios.results;
                this.txtResultsModalComplementos=data.modal_complementos.results;

                this.txtMax = data.max;
                this.txtNombre = nota.nombre;
                this.txtCodigo = nota.codigo;
                this.selectEstado = nota.estado;
                this.selectNota = nota.tipo;
                this.selectConcepto = nota.detalle.concepto;
                if(nota.tipo == "I"){
                    if(nota.detalle.fuente != null)this.objFuente = nota.detalle.fuente;
                    if(nota.detalle.cuenta != null)this.objCuenta = nota.detalle.cuenta;
                    if(nota.detalle.cuenta_clasificadora !=null){
                        this.txtType = nota.detalle.clasificador;
                        if(this.txtType == 2){
                            this.objBienes = {"codigo":nota.detalle.cuenta_clasificadora,"nombre":nota.detalle.nombre_clasificador};
                        }else if(this.txtType == 3){
                            this.objServicio = {"codigo":nota.detalle.cuenta_clasificadora,"nombre":nota.detalle.nombre_clasificador};
                        }else if(this.txtType == 4){
                            this.objComplemento = {"codigo":nota.detalle.cuenta_clasificadora,"nombre":nota.detalle.nombre_clasificador};
                        }
                    }
                }
            }else{
                window.location.href="teso-notasBancariasEditar.php?id="+objData.data;
            }

        },
        search: async function(option=""){
            let search ="";
            if(option=="modal_cuenta")search = this.txtSearch;
            if(option=="modal_fuente")search = this.txtSearchFuente;
            if(option=="modal_bien")search = this.txtSearchBien;
            if(option=="modal_servicio")search = this.txtSearchServicio;
            if(option=="modal_complemento")search = this.txtSearchComplemento;
            if(option=="codigo_cuenta")search= this.objCuenta.codigo;
            if(option=="codigo_fuente")search= this.objFuente.codigo;
            if(option=="codigo_bien")search= this.objBien.codigo;
            if(option=="codigo_servicio")search= this.objServicio.codigo;
            if(option=="codigo_complemento")search= this.objComplemento.codigo;

            const formData = new FormData();
            formData.append("action","search");
            formData.append("option",option);
            formData.append("search",search);
            this.isLoading=true;

            const response = await fetch(URL,{method:"POST", body:formData});
            const objData = await response.json();
            if(option =="modal_cuenta"){
                this.arrModalCuentas = objData.modal_cuentas.data;
                this.txtResultsModalCuentas = objData.modal_cuentas.results;
            }else if(option =="modal_fuente"){
                this.arrModalFuentes = objData.modal_fuentes.data;
                this.txtResultsModalFuentes = objData.modal_fuentes.results;
            }else if(option =="modal_bien"){
                this.arrModalBienes = objData.modal_bienes.data;
                this.txtResultsModalBienes = objData.modal_bienes.results;
            }else if(option =="modal_servicio"){
                this.arrModalServicios = objData.modal_servicios.data;
                this.txtResultsModalServicios = objData.modal_servicios.results;
            }else if(option =="modal_complemento"){
                this.arrModalComplementos = objData.modal_complementos.data;
                this.txtResultsModalComplementos = objData.modal_complementos.results;
            }else if(option == "codigo_cuenta"){
                if(objData.modal_cuentas.data.length > 0){
                    this.objCuenta = objData.modal_cuentas.data[0];
                }else{
                    this.objCuenta.nombre = "No existe";
                }
            }else if(option == "codigo_fuente"){
                if(objData.modal_fuentes.data.length>0){
                    this.objFuente = objData.modal_fuentes.data[0];
                }else{
                    this.objFuente.nombre = "No existe";
                }
            }else if(option == "codigo_bien"){
                if(objData.modal_bienes.data.length > 0){
                    this.objBien = objData.modal_bienes.data[0];
                }else{
                    this.objBien.nombre = "No existe";
                }
            }else if(option == "codigo_servicio"){
                if(objData.modal_servicios.data.length > 0){
                    this.objServicio = objData.modal_servicios.data[0];
                }else{
                    this.objServicio.nombre = "No existe";
                }
            }else if(option == "codigo_complemento"){
                if(objData.modal_complementos.data.length>0){
                    this.objComplemento = objData.modal_complementos.data[0];
                }else{
                    this.objComplemento.nombre = "No existe";
                }
            }
            this.isLoading=false;
        },
        save:async function(){
            let clasificador = "";
            let tipoNombre = "";
            let nombreClasificador ="";
            if(this.selectNota == 'I'){
                if(this.txtNombre == "" || this.objCuenta.codigo =="" || this.objFuente.codigo =="" || this.selectConcepto ==""
                    || this.objCuenta.nombre =="No existe" || this.objFuente.nombre =="No existe"
                    || this.objCuenta.nombre =="" || this.objFuente.nombre ==""
                ){
                    Swal.fire("Error","Todos los campos con (*) Son obligatorios","error");
                    return false;
                }
                if(this.txtType > 0){

                    if(this.txtType == 2){
                        clasificador = this.objBienes.codigo;
                        tipoNombre = "Bien transportable";
                        nombreClasificador = this.objBienes.nombre;
                    }else if(this.txtType == 3){
                        clasificador = this.objServicio.codigo;
                        tipoNombre = "Servicio";
                        nombreClasificador = this.objServicio.nombre;
                    }else if(this.txtType == 4){
                        clasificador = this.objComplemento.codigo;
                        tipoNombre = "Complemento de ingreso";
                        nombreClasificador = this.objComplemento.nombre;
                    }
                    if(clasificador == ""){
                        Swal.fire("Error","Debe seleccionar un "+tipoNombre,"error");
                        return false;
                    }
                }
                if(this.txtType !=null && (clasificador ==""  || nombreClasificador == "No existe" || nombreClasificador == "")){
                    Swal.fire("Error","Debe seleccionar un "+tipoNombre,"error");
                    return false;
                }
            }else{
                if(this.txtNombre == "" || this.selectConcepto ==""){
                    Swal.fire("Error","Todos los campos con (*) Son obligatorios","error");
                    return false;
                }
            }
            let data = {
                cabecera:{
                    codigo:this.txtCodigo,
                    nombre:this.txtNombre,
                    tipo:this.selectNota,
                    estado:this.selectEstado
                },
                detalle:{
                    cuenta:this.objCuenta.codigo,
                    fuente:this.objFuente.codigo,
                    concepto:this.selectConcepto,
                    clasificador:clasificador
                }
            }
            let formData = new FormData();
            formData.append("action","save");
            formData.append("data",JSON.stringify(data));
            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();
            if(objData.status){
                Swal.fire("Guardado",objData.msg,"success");
            }else{
                Swal.fire("Error",objData.msg,"error");
            }
        },
        selectItemModal: async function({...item},type){
            if(type == "cuenta"){
                this.objCuenta = item;
                let formData = new FormData();
                formData.append("action","clasificador");
                formData.append("cuenta",this.objCuenta.codigo);
                this.isLoading = true;
                const response = await fetch(URL,{method:"POST",body:formData});
                this.txtType = await response.json();
                this.isLoading = false;
                this.isModal = false;
            }else if(type =="fuente"){
                this.objFuente = item;
                this.isModalFuente = false;
            }else if(type =="bien"){
                this.objBien = item;
                this.isModalBienes = false;
            }else if(type =="servicio"){
                this.objServicio = item;
                this.isModalServicios = false;
            }else if(type =="complemento"){
                this.objComplemento = item;
                this.isModalComplemento = false;
            }
        },
        editItem:function(id){
            window.location.href= 'teso-notasBancariasEditar.php?id='+id;
        },
        formatNumero: function(valor){
            return new Intl.NumberFormat("en-US", {style: "currency", currency: "USD"}).format(valor);
        },
        validInteger:function(valor){
            valor = new String(valor);
            if(valor.includes(".") || valor.includes(",")){
                Swal.fire("Error","La cantidad debe ser un número entero.","error");
            }
        },
    },
    computed:{

    }
})
