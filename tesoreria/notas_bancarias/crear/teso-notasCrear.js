const URL ='tesoreria/notas_bancarias/crear/teso-notasCrear.php';

var app = new Vue({
    el:"#myapp",
    data() {
        return {
            isLoading:false,
            isModal:false,
            isModalFuente:false,
            isModalServicios:false,
            isModalBienes:false,
            isModalComplemento:false,
            arrConceptos:[],
            arrModalCuentas:[],
            arrModalFuentes:[],
            arrModalServicios:[],
            arrModalBienes:[],
            arrModalComplementos:[],
            objCuenta:{"codigo":"","nombre":""},
            objFuente:{"codigo":"","nombre":""},
            objBien:{"codigo":"","nombre":""},
            objServicio:{"codigo":"","nombre":""},
            objComplemento:{"codigo":"","nombre":""},
            selectNota:"I",
            selectConcepto:"",
            txtNombre:"",
            txtSearch:"",
            txtSearchFuente:"",
            txtSearchBien:"",
            txtSearchServicio:"",
            txtSearchComplemento:"",
            txtResults: 0,
            txtResultsTerceros:0,
            txtConsecutivo:0,
            txtResultsModalCuentas:0,
            txtResultsModalFuentes:0,
            txtResultsModalBienes:0,
            txtResultsModalServicios:0,
            txtResultsModalComplementos:0,
            txtType:0,
        }
    },
    mounted() {
        this.getData();
    },
    methods: {

        getData: async function(){
            const formData = new FormData();
            formData.append("action","get");
            const response = await fetch(URL,{method:"POST", body:formData});
            const objData = await response.json();
            this.arrConceptos = objData.conceptos;
            this.arrModalCuentas = objData.modal_cuentas.data;
            this.arrModalFuentes = objData.modal_fuentes.data;
            this.arrModalBienes = objData.modal_bienes.data;
            this.arrModalServicios = objData.modal_servicios.data;
            this.arrModalComplementos = objData.modal_complementos.data;

            this.txtResultsModalCuentas = objData.modal_cuentas.results;
            this.txtResultsModalFuentes = objData.modal_fuentes.results;
            this.txtResultsModalBienes=objData.modal_bienes.results;
            this.txtResultsModalServicios=objData.modal_servicios.results;
            this.txtResultsModalComplementos=objData.modal_complementos.results;
            this.txtConsecutivo = objData.consecutivo;
        },
        search: async function(option=""){
            let search ="";
            if(option=="modal_cuenta")search = this.txtSearch;
            if(option=="modal_fuente")search = this.txtSearchFuente;
            if(option=="modal_bien")search = this.txtSearchBien;
            if(option=="modal_servicio")search = this.txtSearchServicio;
            if(option=="modal_complemento")search = this.txtSearchComplemento;
            if(option=="codigo_cuenta")search= this.objCuenta.codigo;
            if(option=="codigo_fuente")search= this.objFuente.codigo;
            if(option=="codigo_bien")search= this.objBien.codigo;
            if(option=="codigo_servicio")search= this.objServicio.codigo;
            if(option=="codigo_complemento")search= this.objComplemento.codigo;

            const formData = new FormData();
            formData.append("action","search");
            formData.append("option",option);
            formData.append("search",search);
            this.isLoading=true;

            const response = await fetch(URL,{method:"POST", body:formData});
            const objData = await response.json();
            if(option =="modal_cuenta"){
                this.arrModalCuentas = objData.modal_cuentas.data;
                this.txtResultsModalCuentas = objData.modal_cuentas.results;
            }else if(option =="modal_fuente"){
                this.arrModalFuentes = objData.modal_fuentes.data;
                this.txtResultsModalFuentes = objData.modal_fuentes.results;
            }else if(option =="modal_bien"){
                this.arrModalBienes = objData.modal_bienes.data;
                this.txtResultsModalBienes = objData.modal_bienes.results;
            }else if(option =="modal_servicio"){
                this.arrModalServicios = objData.modal_servicios.data;
                this.txtResultsModalServicios = objData.modal_servicios.results;
            }else if(option =="modal_complemento"){
                this.arrModalComplementos = objData.modal_complementos.data;
                this.txtResultsModalComplementos = objData.modal_complementos.results;
            }else if(option == "codigo_cuenta"){
                if(objData.modal_cuentas.data.length > 0){
                    this.objCuenta = objData.modal_cuentas.data[0];
                }else{
                    this.objCuenta.nombre = "No existe";
                }
            }else if(option == "codigo_fuente"){
                if(objData.modal_fuentes.data.length>0){
                    this.objFuente = objData.modal_fuentes.data[0];
                }else{
                    this.objFuente.nombre = "No existe";
                }
            }else if(option == "codigo_bien"){
                if(objData.modal_bienes.data.length > 0){
                    this.objBien = objData.modal_bienes.data[0];
                }else{
                    this.objBien.nombre = "No existe";
                }
            }else if(option == "codigo_servicio"){
                if(objData.modal_servicios.data.length > 0){
                    this.objServicio = objData.modal_servicios.data[0];
                }else{
                    this.objServicio.nombre = "No existe";
                }
            }else if(option == "codigo_complemento"){
                if(objData.modal_complementos.data.length>0){
                    this.objComplemento = objData.modal_complementos.data[0];
                }else{
                    this.objComplemento.nombre = "No existe";
                }
            }
            this.isLoading=false;
        },
        save:async function(){
            let clasificador = "";
            let tipoNombre = "";
            let nombreClasificador ="";
            if(this.selectNota == 'I'){
                if(this.txtNombre == "" || this.objCuenta.codigo =="" || this.objFuente.codigo =="" || this.selectConcepto ==""
                    || this.objCuenta.nombre =="No existe" || this.objFuente.nombre =="No existe"
                ){
                    Swal.fire("Error","Todos los campos con (*) Son obligatorios","error");
                    return false;
                }
                if(this.txtType > 0){
                    if(this.txtType == 2){
                        clasificador = this.objBienes.codigo;
                        tipoNombre = "Bien transportable";
                        nombreClasificador = this.objBienes.nombre;
                    }else if(this.txtType == 3){
                        clasificador = this.objServicio.codigo;
                        tipoNombre = "Servicio";
                        nombreClasificador = this.objServicio.nombre;
                    }else if(this.txtType == 4){
                        clasificador = this.objComplemento.codigo;
                        tipoNombre = "Complemento de ingreso";
                        nombreClasificador = this.objComplemento.nombre;
                    }
                    if(clasificador == ""){
                        Swal.fire("Error","Debe seleccionar un "+tipoNombre,"error");
                        return false;
                    }
                }
            }else{
                if(this.txtNombre == "" || this.selectConcepto ==""){
                    Swal.fire("Error","Todos los campos con (*) Son obligatorios","error");
                    return false;
                }
            }
            let data = {
                cabecera:{
                    codigo:this.txtConsecutivo,
                    nombre:this.txtNombre,
                    tipo:this.selectNota
                },
                detalle:{
                    cuenta:this.objCuenta.codigo,
                    fuente:this.objFuente.codigo,
                    concepto:this.selectConcepto,
                    clasificador: clasificador
                }
            }
            let formData = new FormData();
            formData.append("action","save");
            formData.append("data",JSON.stringify(data));
            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();
            if(objData.status){
                Swal.fire("Guardado",objData.msg,"success");
                setTimeout(function(){
                    window.location.reload();
                },1500);
            }else{
                Swal.fire("Error",objData.msg,"error");
            }
        },
        selectItemModal: async function({...item},type){
            if(type == "cuenta"){
                this.objCuenta = item;
                let formData = new FormData();
                formData.append("action","clasificador");
                formData.append("cuenta",this.objCuenta.codigo);
                this.isLoading = true;
                const response = await fetch(URL,{method:"POST",body:formData});
                this.txtType = await response.json();
                this.isLoading = false;
                this.isModal = false;
            }else if(type =="fuente"){
                this.objFuente = item;
                this.isModalFuente = false;
            }else if(type =="bien"){
                this.objBien = item;
                this.isModalBienes = false;
            }else if(type =="servicio"){
                this.objServicio = item;
                this.isModalServicios = false;
            }else if(type =="complemento"){
                this.objComplemento = item;
                this.isModalComplemento = false;
            }
        },
        formatNumero: function(valor){
            return new Intl.NumberFormat("en-US", {style: "currency", currency: "USD"}).format(valor);
        },
        validInteger:function(valor){
            valor = new String(valor);
            if(valor.includes(".") || valor.includes(",")){
                Swal.fire("Error","La cantidad debe ser un número entero.","error");
            }
        },
    },
    computed:{

    }
})
