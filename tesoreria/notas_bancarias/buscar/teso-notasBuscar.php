<?php
    require_once '../../../comun.inc';
    require '../../../funciones.inc';
    require '../../../funcionesSP.inc.php';
    /*ini_set('display_errors', '1');
    ini_set('display_startup_errors', '1');
    error_reporting(E_ALL);*/
    session_start();
    //dep($_POST);exit;
    if($_POST){
        $obj = new Plantilla();
        if($_POST['action']=="search"){
            $currentPage = intval($_POST['page']);
            $obj->search($_POST['search'],$currentPage);
        }else if($_POST['action']=="get"){
            $obj->getData();
        }
    }

    class Plantilla{
        private $linkbd;
        private $arrData;
        public function __construct() {
            $this->linkbd = conectar_v7();
            $this->linkbd->set_charset("utf8");
        }
        public function search(string $search,int $currentPage){
            if(!empty($_SESSION)){
                $request = $this->selectData($search,$currentPage);
                echo json_encode($request,JSON_UNESCAPED_UNICODE);
            }
            die();
        }
        public function getData(){
            if(!empty($_SESSION)){
                $request = $this->selectData();
                echo json_encode($request,JSON_UNESCAPED_UNICODE);
            }
            die();
        }
        public function selectData($search="",$currentPage=1){
            $perPage = 200;
            $startRows = ($currentPage-1) * $perPage;
            $totalRows = 0;
            $s="";
            if($search !=""){
                $s="WHERE codigo LIKE '$search%' OR nombre LIKE '$search%'";
            }
            $sql = "SELECT codigo,nombre,tipo,estado
            FROM tesogastosbancarios $s
            ORDER BY CAST(codigo as int) DESC LIMIT $startRows, $perPage";
            $arrData = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);

            $totalRows = mysqli_query($this->linkbd,"SELECT count(*) as total FROM tesogastosbancarios $s")->fetch_assoc()['total'];
            $totalPages = $totalRows > 0 ? ceil($totalRows/$perPage) : 1;
            $arrResponse = array("status"=>true,"data"=>$arrData,"total"=>$totalRows,"total_pages"=>$totalPages);
            return $arrResponse;
        }
    }
?>
