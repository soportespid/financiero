<?php
    trait ReciboTrait{
        public function revertReciboCaja($con,int $id,array $arrAbono){
            $arrAbono['codigo'] = $arrAbono['codcatastral'];
            $sql = "UPDATE tesoreciboscaja SET estado = 'N' WHERE id_recibos = $id";
            mysqli_query($con,$sql);
            $this->updatePrediosAvaluos($con,$arrAbono,false);
            $sql = "SELECT descripcion,valor FROM tesoreciboscaja WHERE id_recibos = $id";
            $request = mysqli_query($con,$sql)->fetch_assoc();
            return $request;
        }
        public function insertReciboCaja($con,int $idAbono,int $id, string $concepto, string $cuenta,int $total,string $recaudado,array $arrAcuerdo){
            $anio = date("Y");
            $user=$_SESSION['nickusu'];
            $sql = "INSERT INTO tesoreciboscaja (fecha,vigencia,id_recaudo,recaudado,cuentabanco, valor,estado,tipo,descripcion,usuario)
            VALUES(NOW(),'$anio','$id','$recaudado','$cuenta','$total','S',1,'$concepto','$user')";
            mysqli_query($con,$sql);
            $idRecibo = mysqli_insert_id($con);
            $sqlAbono = "UPDATE tesoabono SET recibo = $idRecibo WHERE id_abono = $idAbono";
            mysqli_query($con,$sqlAbono);
            $this->updatePrediosAvaluos($con,$arrAcuerdo);
            return $idRecibo;
        }
        public function updatePrediosAvaluos($con,$arrAcuerdo,$flag = true){
            $arrVigencias = $arrAcuerdo['detalle'];
            $sql_predios = "SELECT id FROM predios WHERE codigo_catastro='$arrAcuerdo[codigo]'";
            $idPredio = mysqli_query($con,$sql_predios)->fetch_assoc()['id'];
            foreach ($arrVigencias as $d) {
                $estadoTesoAvaluos = " pago='S'";
                $estadoPredioAvaluos = " pagado=1";
                if(!$flag){
                    $estadoTesoAvaluos = " pago='N'";
                    $estadoPredioAvaluos = " pagado=0";
                }
                $sql = "UPDATE tesoprediosavaluos SET $estadoTesoAvaluos WHERE codigocatastral= '$arrAcuerdo[codigo]' AND vigencia='$d[vigencia]'";
                mysqli_query($con,$sql);
                if($idPredio != null){
                    mysqli_query($con, "UPDATE predio_avaluos SET $estadoPredioAvaluos WHERE predio_id = '$idPredio' AND vigencia = '$d[vigencia]'");
                }
            }
        }
    }
?>
