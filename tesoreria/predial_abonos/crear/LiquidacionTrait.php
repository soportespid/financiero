<?php
    require_once '../../../Librerias/vendor/autoload.php';
    require '../../../vendor/autoload.php';

    use Laravel\Route;
    use Laravel\DB;
    trait LiquidacionTrait{
        /* public function revertLiquidacion($con,$id){
            //ANULO FACTURA
            $sql = "UPDATE factura_predials SET data = (SELECT JSON_REPLACE(data, '$.factura_pagada', false) FROM factura_predials WHERE id =$id),
            deleted_at=NOW() WHERE id =$id";
            mysqli_query($con,$sql);
            //ANULO LIQUIDACIÓN
            $sql = "UPDATE tesoliquidapredial SET estado = 'N' WHERE idpredial = $id";
            mysqli_query($con,$sql);
            $sql = "UPDATE tesoliquidapredial_det SET estado = 'N' WHERE idpredial = $id";
            mysqli_query($con,$sql);
            $sql = "UPDATE tesoliquidapredial_desc SET estado = 'N' WHERE id_predial = $id";
            mysqli_query($con,$sql);

            $sql = "SELECT data FROM factura_predials WHERE id = $id";
            $factura = mysqli_query($this->linkbd,$sql)->fetch_assoc()['data'];
            $factura = json_decode($factura,true);
            $vigencias = $factura['liquidacion']['vigencias'];
            return $vigencias;
        } */

        /**
         * Revierte una liquidación y sus registros relacionados
         *
         * @param mixed $con Conexión a la base de datos (deprecated)
         * @param int $id ID de la factura a revertir
         * @return array Array de vigencias de la factura
         * @throws \Exception Si hay errores en el proceso
         */
        public function revertLiquidacion($con, int $id): array
        {
            $tenant_db = null;

            try {
                // Validar ID
                if ($id <= 0) {
                    throw new \Exception("ID de factura no válido");
                }

                // Obtener conexión del tenant
                $tenant_db = DB::connectWithDomain(Route::currentDomain());
                
                if (!$tenant_db) {
                    throw new \Exception("Error de conexión con la base de datos");
                }

                // Iniciar transacción
                $tenant_db->begin_transaction();

                try {
                    // 1. Actualizar factura_predials
                    $sqlUpdateFactura = "
                        UPDATE factura_predials 
                        SET data = (
                            SELECT JSON_REPLACE(data, '$.factura_pagada', false) 
                            FROM (SELECT data FROM factura_predials WHERE id = ?) AS temp
                        ),
                        deleted_at = NOW() 
                        WHERE id = ?
                    ";

                    $stmt = $tenant_db->prepare($sqlUpdateFactura);
                    if (!$stmt) {
                        throw new \Exception("Error preparando actualización de factura: " . $tenant_db->error);
                    }

                    $stmt->bind_param('ii', $id, $id);
                    if (!$stmt->execute()) {
                        throw new \Exception("Error actualizando factura: " . $stmt->error);
                    }
                    $stmt->close();

                    // 2. Actualizar registros relacionados
                    $tablasActualizar = [
                        'tesoliquidapredial' => 'idpredial',
                        'tesoliquidapredial_det' => 'idpredial',
                        'tesoliquidapredial_desc' => 'id_predial'
                    ];

                    foreach ($tablasActualizar as $tabla => $campoId) {
                        $sqlUpdate = "UPDATE {$tabla} SET estado = 'N' WHERE {$campoId} = ?";
                        
                        $stmt = $tenant_db->prepare($sqlUpdate);
                        if (!$stmt) {
                            throw new \Exception("Error preparando actualización de {$tabla}: " . $tenant_db->error);
                        }

                        $stmt->bind_param('i', $id);
                        if (!$stmt->execute()) {
                            throw new \Exception("Error actualizando {$tabla}: " . $stmt->error);
                        }
                        $stmt->close();
                    }

                    // 3. Obtener datos actualizados de la factura
                    $sqlSelect = "SELECT data FROM factura_predials WHERE id = ?";
                    
                    $stmt = $tenant_db->prepare($sqlSelect);
                    if (!$stmt) {
                        throw new \Exception("Error preparando consulta de factura: " . $tenant_db->error);
                    }

                    $stmt->bind_param('i', $id);
                    if (!$stmt->execute()) {
                        throw new \Exception("Error consultando factura: " . $stmt->error);
                    }

                    $result = $stmt->get_result();
                    $facturaData = $result->fetch_assoc();
                    
                    if (!$facturaData) {
                        throw new \Exception("No se encontró la factura con ID: {$id}");
                    }

                    $factura = json_decode($facturaData['data'], true);
                    
                    if (json_last_error() !== JSON_ERROR_NONE) {
                        throw new \Exception("Error decodificando JSON de factura: " . json_last_error_msg());
                    }

                    if (!isset($factura['liquidacion']['vigencias'])) {
                        throw new \Exception("Estructura de factura inválida: vigencias no encontradas");
                    }

                    // Confirmar transacción
                    $tenant_db->commit();

                    return $factura['liquidacion']['vigencias'];

                } catch (\Exception $e) {
                    // Revertir transacción en caso de error
                    if ($tenant_db) {
                        $tenant_db->rollback();
                    }
                    throw $e;
                }

            } catch (\Exception $e) {
                error_log("Error en revertLiquidacion: " . $e->getMessage());
                throw $e;

            } finally {
                // Cerrar conexión
                if ($tenant_db) {
                    $tenant_db->close();
                }
            }
        }
        
        public function insertLiquidacion($con,array $arrData,string $strFecha){
            $arrVigencias = $arrData['detalle'];
            usort($arrVigencias,function($a,$b){return $a['vigencia'] < $b['vigencia'];});
            $arrPredio = $this->selectInfoPredio($con,$arrData['codigo'],$arrData['documento']);
            $arrLiquidacion = $arrPredio;
            $arrLiquidacion['valor_avaluo'] = $arrPredio['avaluo'];
            $arrLiquidacion['interes_vigente']=  0;
            $arrLiquidacion['descuento_vigente']=  0;
            $arrLiquidacion['liquidacion']['predio_id'] = $arrPredio['id'];
            $arrLiquidacion['liquidacion']['codigo_catastro'] = $arrPredio['codigo_catastro'];
            $arrEstadoCuenta = $this->selectEstadoCuenta($con,$arrData['estado_cuenta']);

            $totalBomberil = 0;
            $totalBomberilIntereses = 0;
            $totalBomberilDescuentoIntereses = 0;
            $totalAlumbrado = 0;
            $totalAmbiental = 0;
            $totalAmbientalIntereses = 0;
            $totalAmbientalDescuentoIntereses = 0;
            $totalIntereses = 0;
            $totalDescuentos = 0;
            $totalLiquidacion = 0;
            $totalPredial = 0;
            $totalPredialDescuento = 0;
            $totalPredialIntereses = 0;
            $totalPredialDescuentoIntereses = 0;
            $totalAvaluo = 0;
            $totalInteresesSinDescuento = 0;
            $totalVigencias = count($arrVigencias);
            if(!empty($arrEstadoCuenta)){
                $arrLiquidacion['interes_vigente']= $arrEstadoCuenta['interes_vigente'];
                $arrLiquidacion['descuento_vigente']=  $arrEstadoCuenta['descuento_vigente'];
                $arrVigenciasCuenta = $arrEstadoCuenta['liquidacion']['vigencias'];
                $totalVigenciasCuenta = count($arrVigenciasCuenta);
                for ($i=0; $i < $totalVigencias; $i++) {
                    for ($j=0; $j < $totalVigenciasCuenta; $j++) {
                        if($arrVigencias[$i]['vigencia'] == $arrVigenciasCuenta[$j]['vigencia']){
                            $arrVigencias[$i]['avaluo_id'] = $arrVigenciasCuenta[$j]['avaluo_id'];
                            $arrVigencias[$i]['estatuto'] = $arrVigenciasCuenta[$j]['estatuto'];
                            $arrVigencias[$i]['valor_avaluo'] = $arrVigenciasCuenta[$j]['valor_avaluo'];
                            $arrVigencias[$i]['predial_descuento_intereses'] = $arrVigenciasCuenta[$j]['predial_descuento_intereses'];
                            $arrVigencias[$i]['bomberil_descuento_intereses'] = $arrVigenciasCuenta[$j]['bomberil_descuento_intereses'];
                            $arrVigencias[$i]['ambiental_descuento_intereses'] = $arrVigenciasCuenta[$j]['ambiental_descuento_intereses'];
                            $arrVigencias[$i]['descuento_intereses'] = $arrVigenciasCuenta[$j]['descuento_intereses'];
                            $arrVigencias[$i]['total_intereses'] = $arrVigenciasCuenta[$j]['total_intereses'];
                            $arrVigencias[$i]['intereses_totales_sin_descuento'] = 0;
                            $arrVigencias[$i]['selected'] = true;
                            break;
                        }
                    }
                    $totalBomberil +=$arrVigencias[$i]['bomberil'];
                    $totalBomberilIntereses +=$arrVigencias[$i]['bomberil_intereses'];
                    $totalBomberilDescuentoIntereses +=$arrVigencias[$i]['bomberil_descuento_intereses'];
                    $totalAmbiental +=$arrVigencias[$i]['ambiental'];
                    $totalAmbientalIntereses += $arrVigencias[$i]['ambiental_intereses'];
                    $totalAmbientalDescuentoIntereses += $arrVigencias[$i]['ambiental_descuento_intereses'];
                    $totalLiquidacion += $arrVigencias[$i]['total_liquidacion'];
                    $totalPredial += $arrVigencias[$i]['predial'];
                    $totalPredialDescuento += $arrVigencias[$i]['predial_descuento'];
                    $totalPredialIntereses += $arrVigencias[$i]['predial_intereses'];
                    $totalPredialDescuentoIntereses += $arrVigencias[$i]['predial_descuento_intereses'];
                    $totalAlumbrado +=$arrVigencias[$i]['alumbrado'];
                    $totalIntereses+=$arrVigencias[$i]['total_intereses'];
                    $totalAvaluo+=$arrVigencias[$i]['valor_avaluo'];
                    $totalInteresesSinDescuento+=$arrVigencias[$i]['intereses_totales_sin_descuento'];

                    $descuentos = $arrVigencias[$i]['predial_descuento_intereses']+$arrVigencias[$i]['ambiental_descuento_intereses']+$arrVigencias[$i]['bomberil_descuento_intereses'];
                    $totalDescuentos += $descuentos;

                }
            }else{
                for ($i=0; $i < $totalVigencias; $i++) {
                    //$sql_avaluo = "SELECT id,valor_avaluo FROM predio_avaluos WHERE predio_id = {$arrPredio['id']} AND vigencia = {$arrVigencias[$i]['vigencia']}";
                    //$request_avaluo = mysqli_query($con,$sql_avaluo)->fetch_assoc();

                    $tenant_db = DB::connectWithDomain(Route::currentDomain());
                    $sql = "
                        SELECT id, valor_avaluo 
                        FROM predio_avaluos 
                        WHERE predio_id = ? AND vigencia = ?
                    ";

                    $stmt = $tenant_db->prepare($sql);
                    // Vincular parámetros
                    $stmt->bind_param('ii', $arrPredio['id'], $arrVigencias[$i]['vigencia']);
                    $result = $stmt->get_result();
                    // Obtener resultado
                    $request_avaluo = $result->fetch_assoc();


                    $arrVigencias[$i]['avaluo_id'] = $request_avaluo['id'];
                    $arrVigencias[$i]['valor_avaluo'] = $request_avaluo['valor_avaluo'];
                    $arrVigencias[$i]['predial_descuento_intereses'] =0;
                    $arrVigencias[$i]['bomberil_descuento_intereses'] = 0;
                    $arrVigencias[$i]['ambiental_descuento_intereses'] =0;
                    $arrVigencias[$i]['total_intereses'] = 0;
                    $arrVigencias[$i]['intereses_totales_sin_descuento'] = 0;
                    $arrVigencias[$i]['selected'] = true;
                    $arrVigencias[$i]['descuento_intereses'] = 0;
                    $arrVigencias[$i]['estatuto'] = array(
                        "nombre" => "",
                        "norma_predial" => 0,
                        "bomberil" => 0,
                        "ambiental" => 0,
                        "alumbrado" => 0,
                        "recibo_caja" => 0
                    );
                    $totalBomberil +=$arrVigencias[$i]['bomberil'];
                    $totalBomberilIntereses +=$arrVigencias[$i]['bomberil_intereses'];
                    $totalAmbiental +=$arrVigencias[$i]['ambiental'];
                    $totalAmbientalIntereses += $arrVigencias[$i]['ambiental_intereses'];
                    $totalLiquidacion += $arrVigencias[$i]['total_liquidacion'];
                    $totalPredial += $arrVigencias[$i]['predial'];
                    $totalPredialDescuento += $arrVigencias[$i]['predial_descuento'];
                    $totalPredialIntereses += $arrVigencias[$i]['predial_intereses'];
                    $totalAlumbrado +=$arrVigencias[$i]['alumbrado'];
                    $totalAvaluo+=$arrVigencias[$i]['valor_avaluo'];
                }
            }
            $arrLiquidacion['totales'] = array(
                "bomberil"=>$totalBomberil,
                "alumbrado"=>$totalAlumbrado,
                "ambiental"=>$totalAmbiental,
                "intereses"=>$totalIntereses,
                "descuentos"=>$totalDescuentos,
                "liquidacion"=>$totalLiquidacion,
                "predial"=>$totalPredial,
                "total_avaluo"=>$totalAvaluo,
                "intereses_totales_sin_descuento"=>$totalInteresesSinDescuento
            );
            $arrLiquidacion['liquidacion']['vigencias'] = $arrVigencias;
            $arrLiquidacion['liquidacion']['total_liquidacion'] =$totalLiquidacion;
            $arrLiquidacion['liquidacion']['total_predial']  = $totalPredial;
            $arrLiquidacion['liquidacion']['total_predial_descuento'] =$totalPredialDescuento;
            $arrLiquidacion['liquidacion']['total_predial_intereses']  = $totalPredialIntereses;
            $arrLiquidacion['liquidacion']['total_predial_descuento_intereses'] =$totalPredialDescuentoIntereses;
            $arrLiquidacion['liquidacion']['total_bomberil'] = $totalBomberil;
            $arrLiquidacion['liquidacion']['total_bomberil_intereses'] = $totalBomberilIntereses;
            $arrLiquidacion['liquidacion']['total_bomberil_descuento_intereses'] = $totalBomberilDescuentoIntereses;
            $arrLiquidacion['liquidacion']['total_ambiental'] =$totalAmbiental;
            $arrLiquidacion['liquidacion']['total_ambiental_intereses'] =$totalAmbientalIntereses;
            $arrLiquidacion['liquidacion']['total_ambiental_descuento_intereses'] = $totalAmbientalDescuentoIntereses;
            $arrLiquidacion['liquidacion']['total_alumbrado'] = $totalAlumbrado;
            $arrLiquidacion['liquidacion']['total_intereses'] = $totalIntereses;
            $arrLiquidacion['factura_pagada']=true;
            $arrLiquidacion['facturado_desde'] = $arrVigencias[count($arrVigencias)-1]['vigencia'];
            $arrLiquidacion['facturado_hasta'] = $arrVigencias[0]['vigencia'];

            $dateTime = new DateTime('now', new DateTimeZone('UTC'));
            $isoDate = $dateTime->format('Y-m-d\TH:i:s.u\Z');
            $arrLiquidacion['pague_hasta']  = $isoDate;
            $arrLiquidacion['private'] = true;
            $arrLiquidacion['barcode'] = $this->selectCodigoBarras($con,$totalLiquidacion,date("Ymd"));
            //dep($arrLiquidacion);exit;
            $data = json_encode($arrLiquidacion,JSON_UNESCAPED_UNICODE);
            $idLiquidacion = $this->insertFactura($con,$data,$this->selectConsecutivo());
            $strConcepto = $this->insertLiquidaPredial($con,$arrLiquidacion,$idLiquidacion);
            return array("liquidacion"=>$idLiquidacion,"concepto"=>$strConcepto);
        }
        public function insertLiquidaPredial($con,array $data,int $idLiquidacion){
            $arrLiquidacion = $data['liquidacion'];
            $arrVigencias = $arrLiquidacion['vigencias'];
            usort($arrVigencias,function($a,$b){return $a['vigencia'] > $b['vigencia']; });
            $strPeriodos = "";
            foreach ($arrVigencias as $e) {
                $strPeriodos.=" ".$e['vigencia'];
            }
            //Cabecera
            $strConcepto = "Años liquidados: ".$strPeriodos;
            $sql = "INSERT INTO tesoliquidapredial(idpredial,codigocatastral,fecha,vigencia,tercero,tasamora,descuento,tasapredial,totaliquida,totalpredial,totalbomb,
            totalmedio,totalinteres,intpredial,intbomb,intmedio,totaldescuentos,concepto,estado,ord,tot)
            VALUES ('$idLiquidacion',
            '{$data['codigo_catastro']}',
            NOW(),
            '{$data['vigencia']}',
            '{$data['documento']}',
            '{$data['interes_vigente']}',
            '{$data['descuento_vigente']}',
            '{$data['tasa']}',
            '{$arrLiquidacion['total_liquidacion']}',
            '{$arrLiquidacion['total_predial']}',
            '{$arrLiquidacion['total_bomberil']}',
            '{$arrLiquidacion['total_ambiental']}',
            '{$arrLiquidacion['total_intereses']}',
            '{$arrLiquidacion['total_predial_intereses']}',
            '{$arrLiquidacion['total_bomberil_intereses']}',
            '{$arrLiquidacion['total_ambiental_intereses']}',
            '{$arrLiquidacion['total_predial_descuento']}',
            '$strConcepto',
            'P',
            '{$data['orden']}',
            '{$data['total']}')";
            mysqli_query($con,$sql);
            //Detalle
            foreach ($arrVigencias as $e) {
                $sql_det = "INSERT INTO tesoliquidapredial_det(idpredial,vigliquidada,avaluo,tasav,predial,intpredial,bomberil,
                intbomb,medioambiente,intmedioambiente,descuentos,totaliquidavig,estado)
                VALUES('$idLiquidacion','{$e['vigencia']}','{$e['valor_avaluo']}','{$e['tasa_por_mil']}','{$e['predial']}',
                '{$e['predial_intereses']}','{$e['bomberil']}','{$e['bomberil_intereses']}','{$e['ambiental']}','{$e['ambiental_intereses']}',
                '{$e['predial_descuento']}','{$e['total_liquidacion']}','S')";
                mysqli_query($con,$sql_det);

                $sql_det = "INSERT INTO tesoliquidapredial_desc(id_predial,cod_catastral,vigencia,descuentointpredial,descuentointbomberil,
                descuentointambiental,val_alumbrado,estado,user)
                VALUES('$idLiquidacion','{$data['codigo_catastro']}','{$e['vigencia']}','{$e['predial_descuento_intereses']}',
                '{$e['bomberil_descuento_intereses']}','{$e['ambiental_descuento_intereses']}',
                '{$e['alumbrado']}','S','$_SESSION[cedulausu]')";
                mysqli_query($con,$sql_det);
            }
            return $strConcepto;
        }

        public function insertFactura($con, string $data, int $idAbono) {
            $ip = getRealIP();
            $idUsuario = $_SESSION['idusuario'] ?? null;
        
            if (!$idUsuario) {
                throw new \Exception("Usuario no autenticado.");
            }

            $tenant_db = DB::connectWithDomain(Route::currentDomain());
        
            // Usar la conexión recibida en lugar de abrir una nueva
            $stmt = $tenant_db->prepare("INSERT INTO factura_predials (ip, data, user_id, created_at, updated_at) VALUES (?, ?, ?, NOW(), NOW())");
        
            if (!$stmt) {
                throw new \Exception("Error en la preparación de la consulta de inserción.");
            }
        
            $stmt->bind_param("ssi", $ip, $data, $idUsuario);
        
            if (!$stmt->execute()) {
                throw new \Exception("Error al insertar en factura_predials.");
            }
        
            $idLiquidacion = $stmt->insert_id; // Obtener el ID de la nueva factura
            $stmt->close();
        
            // Actualizar la tabla tesoabono
            //$stmt = $tenant_db->prepare("UPDATE tesoabono SET cierre = ? WHERE id_abono = ?");
        
            $sqlAbono = "UPDATE tesoabono SET cierre = $idLiquidacion WHERE id_abono = $idAbono";
            mysqli_query($con,$sqlAbono);
        
            return $idLiquidacion;
        }

        public function selectInfoPredio($con,$codcatastral,$tercero){
            /* $sql="SELECT
            p.id,
            p.codigo_catastro,
            p.total,
            pp.documento,
            pp.nombre_propietario as nombre_propietario,
            pp.orden,
            pa.valor_avaluo as avaluo,pa.tasa_por_mil as tasa,
            pa.vigencia,
            tp.direccion,tp.hectareas,
            tp.metros_cuadrados,
            tp.area_construida,
            tp.created_at,
            de.nombre as destino_economico,
            cd.codigo as codigo_destino_economico,
            pt.nombre as predio_tipo,
            pt.codigo as predio_tipo_codigo
            FROM predios p
            LEFT JOIN predio_propietarios pp ON p.id = pp.predio_id AND (pp.documento = '$tercero' OR pp.predio_id = p.id)
            INNER JOIN predio_avaluos pa ON p.id = pa.predio_id
            INNER JOIN predio_informacions tp ON p.id = tp.predio_id
            INNER JOIN predio_tipos pt ON tp.predio_tipo_id = pt.id
            INNER JOIN codigo_destino_economicos cd ON cd.id = tp.codigo_destino_economico_id
            INNER JOIN destino_economicos de ON de.id = cd.destino_economico_id
            WHERE p.codigo_catastro = '$codcatastral'
            AND tp.created_at = (SELECT MAX(created_at) FROM predio_informacions WHERE predio_id = p.id)
            AND pa.vigencia = (SELECT MAX(vigencia) FROM predio_avaluos WHERE predio_id = p.id)
            GROUP BY p.id";
            $request = mysqli_query($con,$sql)->fetch_assoc();
            return $request; */

            $tenant_db = DB::connectWithDomain(Route::currentDomain());

            $stmt = $tenant_db->prepare(<<<EOF
            SELECT
            p.id,
            p.codigo_catastro,
            p.total,
            pp.documento,
            pp.nombre_propietario as nombre_propietario,
            pp.orden,
            pa.valor_avaluo as avaluo,pa.tasa_por_mil as tasa,
            pa.vigencia,
            tp.direccion,tp.hectareas,
            tp.metros_cuadrados,
            tp.area_construida,
            tp.created_at,
            de.nombre as destino_economico,
            cd.codigo as codigo_destino_economico,
            pt.nombre as predio_tipo,
            pt.codigo as predio_tipo_codigo
            FROM predios p
            LEFT JOIN predio_propietarios pp ON p.id = pp.predio_id AND (pp.documento = ? OR pp.predio_id = p.id)
            INNER JOIN predio_avaluos pa ON p.id = pa.predio_id
            INNER JOIN predio_informacions tp ON p.id = tp.predio_id
            INNER JOIN predio_tipos pt ON tp.predio_tipo_id = pt.id
            INNER JOIN codigo_destino_economicos cd ON cd.id = tp.codigo_destino_economico_id
            INNER JOIN destino_economicos de ON de.id = cd.destino_economico_id
            WHERE p.codigo_catastro = ?
            AND tp.created_at = (SELECT MAX(created_at) FROM predio_informacions WHERE predio_id = p.id)
            AND pa.vigencia = (SELECT MAX(vigencia) FROM predio_avaluos WHERE predio_id = p.id)
            GROUP BY p.id
            EOF);

            $stmt->bind_param('ss', $tercero, $codcatastral);

            if (! $stmt->execute()) {
                $tenant_db->close();
                return [];
            }

            $result = $stmt->get_result();
            $data = $result->fetch_assoc();

            $tenant_db->close();

            return $data;
        }
        
        public function selectEstadoCuenta($con, $id) {
            $request = [];

            $tenant_db = DB::connectWithDomain(Route::currentDomain());
        
            if (!empty($id)) {
                // Usa consultas preparadas para evitar inyección SQL
                $stmt = $tenant_db->prepare("SELECT data FROM estado_cuentas WHERE id = ?");
                
                if (!$stmt) {
                    throw new \Exception("Error en la preparación de la consulta.");
                }
        
                $stmt->bind_param("i", $id);
                $stmt->execute();
        
                $result = $stmt->get_result();
        
                if ($result->num_rows > 0) {
                    $data = $result->fetch_assoc()['data'];
                    $request = json_decode($data, true);
                }
        
                $stmt->close();
            }
        
            return $request;
        }
        /* public function selectCodigoBarras($con,$total,$fecha){
            $generator = new Picqer\Barcode\BarcodeGeneratorPNG();
            $id = mysqli_query($this->linkbd,"SELECT MAX(id) as id FROM factura_predials")->fetch_assoc()['id']+1;
            $sql = "SELECT * FROM codigosbarras WHERE tipo = '01' AND estado = 'S'";
            $request = mysqli_query($con,$sql)->fetch_assoc();

            $codeImg = chr(241).$request['codini'].$request['codigo']."802001".
            sprintf('%07d', $id)."111005001".chr(241) . '3900' . sprintf('%010d', $total).chr(241) . '96' .$fecha;

            $codeHuman = "(".$request['codini'].")".$request['codigo']."(8020)01".
            sprintf('%07d', $id)."111005001".'(3900)' . sprintf('%010d', $total).'(96)'.$fecha;
            return array("img"=>base64_encode($generator->getBarcode($codeImg, $generator::TYPE_CODE_128)),"human"=>$codeHuman);
        } */

        public function selectCodigoBarras($con,$total,$fecha){
            $generator = new Picqer\Barcode\BarcodeGeneratorPNG();
            $tenant_db = DB::connectWithDomain(Route::currentDomain());
            $query = sprintf(
                'SELECT MAX(CONVERT(%s, INT)) AS consecutivo FROM %s',
                $tenant_db->real_escape_string('id'),
                $tenant_db->real_escape_string('factura_predials')
            );
            $sql_consecutivo = $tenant_db->prepare($query);

            if (!$sql_consecutivo) {
                throw new \Exception("Error en la preparación de la consulta SELECT.");
            }

            if (!$sql_consecutivo->execute()) {
                throw new \Exception("Error al ejecutar la consulta.");
            }

            $result = $sql_consecutivo->get_result();
            
            if (!$result) {
                throw new \Exception("Error al obtener el resultado.");
            }

            $row = $result->fetch_assoc();
        

            $id = (int)$row['consecutivo'] + 1;

            //$id = $tenant_db->table('factura_predials')->max('id') + 1;
            $sql = "SELECT * FROM codigosbarras WHERE tipo = '01' AND estado = 'S'";
            $request = mysqli_query($con,$sql)->fetch_assoc();
        
            $codeImg = chr(241).$request['codini'].$request['codigo']."802001".
            sprintf('%07d', $id)."111005001".chr(241) . '3900' . sprintf('%010d', $total).chr(241) . '96' .$fecha;
        
            $codeHuman = "(".$request['codini'].")".$request['codigo']."(8020)01".
            sprintf('%07d', $id)."111005001".'(3900)' . sprintf('%010d', $total).'(96)'.$fecha;
            return array("img"=>base64_encode($generator->getBarcode($codeImg, $generator::TYPE_CODE_128)),"human"=>$codeHuman);
        }


    }
?>
