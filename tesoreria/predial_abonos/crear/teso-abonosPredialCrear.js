const URL ='tesoreria/predial_abonos/crear/teso-abonosPredialCrear.php';

var app = new Vue({
    el:"#myapp",
    data() {
        return {
            isLoading:false,
            isModal:false,
            isModalCuenta:false,
            isModalCuotas:false,
            isModalAbono:false,
            isCheckAll:false,
            arrAcuerdos:[],
            arrAcuerdosCopy:[],
            arrAbonos:[],
            arrAbonosCopy:[],
            arrCuentas:[],
            arrCuentasCopy:[],
            arrCuotas:[],
            txtSearch:"",
            txtSearchCuentas:"",
            txtSearchAbono:"",
            txtResultados:0,
            txtResultadosCuentas:0,
            txtResultadosAbono:0,
            txtAbono:0,
            txtDescripcion:"",
            txtTotalCuotas:0,
            txtTotalPendiente:0,
            txtTotalFavor:0,
            txtPagoTotal:0,
            txtPagoTemp:0,
            txtFecha:new Date().toISOString().split("T")[0],
            txtFechaRev:new Date().toISOString().split("T")[0],
            objAcuerdo: {id:"",codigo:"",documento:"",cuotas:[],concepto:""},
            objCuenta: {cuenta:"",banco:""},
            objAbono: {idabono:"",idacuerdo:"",documento:"",total:""},
            selectRecaudado:"banco",
            selectMovimiento:"201"
        }
    },
    mounted() {
        this.getData();
    },
    methods: {
        getData :async function(){
            const formData = new FormData();
            formData.append("action","get");
            this.isLoading = true;
            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();
            this.arrAcuerdos = objData.acuerdos;
            this.arrAcuerdosCopy = objData.acuerdos;
            this.arrCuentas = objData.cuentas;
            this.arrCuentasCopy = objData.cuentas;
            this.arrAbonos = objData.abonos;
            this.arrAbonosCopy = objData.abonos;
            this.txtResultados = this.arrAcuerdosCopy.length;
            this.txtResultadosCuentas = this.arrCuentasCopy.length;
            this.txtResultadosAbono = this.arrAbonosCopy.length;
            this.isLoading = false;
        },
        save: async function(){
            const vueContext = this;
            let obj ={};
            if(vueContext.selectMovimiento==201){
                if(!vueContext.objAcuerdo.id || vueContext.objAcuerdo.id == "" || vueContext.objAcuerdo.concepto == ""){
                    Swal.fire("Error","Todos los campos con (*) son obligatorios","error");
                    return false;
                }
                if(vueContext.selectRecaudado=="banco" && vueContext.objCuenta.cuenta == ""){
                    Swal.fire("Error","Si el recaudo es por banco, debe seleccionar una cuenta bancaria","error");
                    return false;
                }
                if(vueContext.txtPagoTotal <= 0){
                    Swal.fire("Error","El valor del abono no puede ser menor o igual a cero","error");
                    return false;
                }
                if(vueContext.objAcuerdo.debe < vueContext.txtPagoTotal){
                    Swal.fire("Error","El valor del abono no puede superar a la deuda actual.","error");
                    return false;
                }
                obj ={
                    cuenta:this.objCuenta,
                    acuerdo:this.objAcuerdo,
                    abono:this.txtPagoTotal,
                    recaudado:this.selectRecaudado
                }
            }else{
                if(vueContext.objAbono.idabono =="" || vueContext.txtDescripcion ==""){
                    Swal.fire("Error","Todos los campos con (*) son obligatorios","error");
                    return false;
                }
                obj = {
                    id:this.objAbono.idabono,
                    descripcion:this.txtDescripcion
                }
            }
            const formData = new FormData();
            formData.append("data",JSON.stringify(obj));
            formData.append("movimiento",this.selectMovimiento);
            formData.append("fecha",this.txtFecha);
            formData.append("fecha_rev",this.txtFechaRev);
            formData.append("action","save");

            Swal.fire({
                title:"¿Estás segur@ de guardar?",
                text:"",
                icon: 'warning',
                showCancelButton:true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText:"Sí, guardar",
                cancelButtonText:"No, cancelar"
            }).then(async function(result){
                if(result.isConfirmed){
                    const response = await fetch(URL,{method:"POST",body:formData});
                    const objData =  await response.json();
                    vueContext.isLoading = true;
                    if(objData.status){
                        Swal.fire("Guardado",objData.msg,"success");
                        vueContext.isLoading = false;
                        setTimeout(function(){
                            window.location.href='teso-abonosPredialEditar.php?id='+objData.consecutivo;
                        },1500);
                    }else{
                        vueContext.isLoading = false;
                        Swal.fire("Error",objData.msg,"error");
                    }
                }
            });
        },
        search:function(type=""){
            this.arrCuotas = [];
            let search = "";
            if(type == "modal")search = this.txtSearch.toLowerCase();
            if(type == "modal_cuenta")search = this.txtSearchCuentas.toLowerCase();
            if(type == "modal_abono")search = this.txtSearchAbono.toLowerCase();
            if(type == "cod_acuerdo") search = this.objAcuerdo.id;
            if(type == "cod_cuenta") search = this.objCuenta.banco;
            if(type == "cod_abono") search = this.objAbono.idabono;

            if(type=="modal"){
                this.arrAcuerdosCopy = [...this.arrAcuerdos.filter(e=>e.documento.toLowerCase().includes(search)
                || e.codigo.toLowerCase().includes(search) || e.id.toLowerCase().includes(search))];
                this.txtResultados = this.arrAcuerdosCopy.length;
            }else if(type == "modal_cuenta"){
                this.arrCuentasCopy = [...this.arrCuentas.filter(e=>e.cuenta.toLowerCase().includes(search)
                    || e.banco.toLowerCase().includes(search) || e.razonsocial.toLowerCase().includes(search))];
                this.txtResultadosCuentas = this.arrCuentasCopy.length;
            }else if(type == "modal_abono"){
                this.arrAbonosCopy = [...this.arrAbonos.filter(e=>e.idabono.toLowerCase().includes(search)
                    || e.idacuerdo.toLowerCase().includes(search) || e.documento.toLowerCase().includes(search))];
                this.txtResultadosAbono = this.arrAbonosCopy.length;
            }else if(type == "cod_acuerdo"){
                this.objAcuerdo = {};
                this.objAcuerdo = JSON.parse(JSON.stringify(this.arrAcuerdosCopy.filter(e=>e.id==search)[0]));
            }else if(type == "cod_cuenta"){
                this.objCuenta = {};
                this.objCuenta = {...this.arrCuentas.filter(e=>e.banco==search)[0]};
            }else if(type == "cod_abono"){
                this.objAbono = {};
                this.objAbono = {...this.arrAbonos.filter(e=>e.idabono==search)[0]};
                this.txtDescripcion = "Reversión del Abono al acuerdo de pago predial Nro. "+this.objAbono.idacuerdo;
            }
        },
        changeStatus:function(index){
            const cuotas =  this.objAcuerdo.cuotas;
            const cuota = cuotas[index];
            cuota.is_checked = !cuota.is_checked;
            cuota.estado = cuota.is_checked ? "P" : "S";
            cuotas.forEach(e=>{
                if(!e.is_pago){
                    if(e.cuota <= cuota.cuota){
                        e.is_checked = cuota.is_checked;
                        e.estado = cuota.estado;
                    }else{
                        e.is_checked = false;
                        e.estado = "S";
                    }
                }
            });
            this.objAcuerdo.cuotas = cuotas;
            const flag = this.objAcuerdo.cuotas.every((e)=>e.is_checked == true);
            this.isCheckAll = flag;
            this.updateValorCuotas();
        },
        changeAll:function(){
            this.objAcuerdo.cuotas.forEach(e => {
                if(!e.is_pago){
                    e.is_checked = this.isCheckAll
                    e.estado = this.isCheckAll ? "P" : "S";
                }
            });
            this.updateValorCuotas();
        },
        updateValorCuotas:function(){
            const cuotas =  this.objAcuerdo.cuotas;
            this.txtPagoTotal = 0;
            cuotas.forEach(e=>{
                if(e.estado =="P" && !e.is_pago){
                    let valor = e.saldo != e.valor ? e.saldo : e.valor;
                    this.txtPagoTotal+=parseInt(valor)
                }
            });
            if(this.isCheckAll){
                if(this.objAcuerdo.debe > this.txtPagoTotal ){
                    this.txtPagoTotal+=this.objAcuerdo.debe-this.txtPagoTotal;
                }
            }
            this.objAcuerdo.cuotas = cuotas;
            this.updateLiquidacion();
        },
        updateTotal:function(){
            const cuotas =  this.objAcuerdo.cuotas;
            let total = 0;
            cuotas.forEach(e=>{
                if(!e.is_pago){
                    let valor = e.saldo != e.valor ? e.saldo : e.valor;
                    total+=parseInt(valor);
                    if(this.txtPagoTotal >= total){
                        e.is_checked = true;
                        e.estado = "P";
                    }else{
                        e.is_checked = false;
                        e.estado = "S";
                    }
                }
            });
            this.objAcuerdo.cuotas = cuotas;
            const flag = this.objAcuerdo.cuotas.every((e)=>e.is_checked == true);
            this.isCheckAll = flag;
            this.updateLiquidacion();
        },
        updateLiquidacion:function(){
            this.txtPagoTemp = 0;
            if(this.objAcuerdo.detalle){
                let vigencias = this.objAcuerdo.detalle;
                let liquidacion = 0;
                this.txtPagoTemp = parseInt(Math.ceil(this.txtPagoTotal)) + parseInt(Math.ceil(this.objAcuerdo.saldo));
                vigencias.forEach(e => {
                    if(!e.is_pago){
                        liquidacion += parseInt(e.total_liquidacion);
                        if(this.txtPagoTemp >= liquidacion){
                            e.estado = "P";
                        }else{
                            e.estado = "S";
                        }
                    }
                });
                this.objAcuerdo.detalle = vigencias;
            }
        },
        selectItem:function(type="",{...item}){
            if(type == "modal"){
                this.objAcuerdo = item;
                this.isModal = false;
            }else if(type == "modal_cuenta"){
                this.objCuenta = item;
                this.isModalCuenta = false;
            }else if(type == "modal_abono"){
                this.objAbono = item;
                this.isModalAbono = false;
                this.txtDescripcion = "Reversión del Abono al acuerdo de pago predial Nro. "+this.objAbono.idacuerdo;
            }
        },
        formatNumero: function(valor){
            return new Intl.NumberFormat("en-US", {style: "currency", currency: "USD"}).format(valor);
        }
    },
    computed:{

    }
})
