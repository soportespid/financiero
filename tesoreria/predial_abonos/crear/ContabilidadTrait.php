<?php
    trait ContabilidadTrait{
        private $intTipoComprobanteCaja = 5;
        private $intVigenciaActual;
        public function insertContabilidadAbono($con,$data,$strFecha,$movimiento = null){
            $numerotipo = $data['id'];
            $flag = true;
            if($movimiento != null){
                mysqli_query($this->linkbd,"UPDATE comprobante_cab SET estado = 0 WHERE tipo_comp='$data[tipo_comp_abono]' AND numerotipo = '$data[id]'");
                $numerotipo = $data['numerotipo'];
                $flag = false;
            }
            $sql="INSERT INTO comprobante_cab(numerotipo,tipo_comp,fecha,concepto,total,total_debito,total_credito,diferencia,estado)
            VALUES (
            '$numerotipo',
            '$data[tipo_comp]',
            '$strFecha',
            '$data[concepto]',
            '$data[abono]',
            '$data[abono]',
            '$data[abono]',
            '$data[diferencia]',
            '$data[estado]')";
            $request = intval(mysqli_query($con,$sql));
            $this->inserContabilidadAbonoDet($con,$data,"debito",$numerotipo,$flag);
            $this->inserContabilidadAbonoDet($con,$data,"credito",$numerotipo,$flag);
            return $request;
        }
        public function inserContabilidadAbonoDet($con,$data,$type,$numerotipo,$flag=true){
            $cuenta = $type=="debito" ? $data['cuenta'] : $data['cuenta_puente'];
            if($flag){
                $debito = $type=="debito" ? $data['abono'] : 0;
                $credito = $type=="debito" ? 0 : $data['abono'];
            }else{
                $debito = $type=="debito" ? 0 : $data['abono'];
                $credito = $type=="debito" ? $data['abono'] : 0;
            }
            $sql="INSERT INTO comprobante_det(id_comp,cuenta,tercero,centrocosto,detalle,valdebito,valcredito,estado,vigencia,tipo_comp,numerotipo)
            VALUES (
            '$data[str_tipo_comp]',
            '$cuenta',
            '$data[tercero]',
            '$data[centro_costo]',
            '$data[concepto]',
            '$debito',
            '$credito',
            '$data[estado]',
            '$data[vigencia]',
            '$data[tipo_comp]',
            '$numerotipo')";
            $request = intval(mysqli_query($con,$sql));
            return $request;
        }
        public function insertContabilidadCaja($con,$idRecibo,$concepto,$total,$idLiquidacion,$cedulaCatastral,
            $arrTesoParametros,$strCuenta,$strTercero,$arrPredialDet,$flag=true){

            $this->intVigenciaActual = date("Y");
            $sql="INSERT INTO comprobante_cab (numerotipo,tipo_comp,fecha,concepto,total,total_debito, total_credito, diferencia,estado)
            values ($idRecibo,$this->intTipoComprobanteCaja,NOW(),'$concepto',0, '$total','$total',0,1)";
            $request = intval(mysqli_query($con,$sql));
            $intValorCredito = 0;
            $intValorDebito = 0;

            $strCuentaDebito =$strCuenta;
            $strDetalle = "";
            $arrDetalleCaja = [];
            foreach ($arrPredialDet as $d) {
                $intPredial = $d['predial'];
                $intAmbiental = $d['ambiental'];
                $intBomberil = $d['bomberil'];
                $intLiquidacion = $d['total_liquidacion'];
                $intVigencia = $d['vigencia'];
                $intDescuento = $d['predial_descuento'];
                $intPredialIntereses = $d['predial_intereses'];
                $intBomberilIntereses = $d['bomberil_intereses'];
                $intAmbientalIntereses = $d['ambiental_intereses'];
                $intAlumbrado =0;
                //Creo una bandera para tener el control de si es una vigencia actual o no
                $flagVigencia = $intVigencia == $this->intVigenciaActual ? true : false;

                /*En este arreglo almaceno el codigo, los ingresos y detalle de caja que voy a trabajar
                 segun el tipo de predio que sea y también, teniendo en cuenta
                si es vigencia actual o no */

                $arrTipoPredio = $this->selectTipoPredio($con,$cedulaCatastral,$flagVigencia,$intVigencia);
                $arrIngresos = $arrTipoPredio['ingresos'];
                $strCodigo = $arrTipoPredio['codigo'];
                $arrCaja = $arrTipoPredio['caja'];
                $arrCaja['valor'] = $intLiquidacion;
                //Armo el detalle del recibo caja
                array_push($arrDetalleCaja,$arrCaja);

                //Ingreso el detalle contable del recibo caja según el tipo de concepto de los ingresos
                foreach ($arrIngresos as $k) {
                    $arrCuentasContables = $this->selectCuentasContables($con,$k['concepto']);
                    $intValorPptto = 0;
                    if($k['concepto'] == "21" || $k['concepto'] == "20" || $k['concepto'] == "22" || $k['concepto'] == "23"){//Impuesto Predial
                        $intValorCredito = $intPredial+$intDescuento;
                        $intValorDebito = $intPredial;
                        $strDetalle = $k['concepto'] == "21" || $k['concepto']=="20" ? 'Ingreso Impuesto Predial Vigente ' : 'Ingreso Impuesto Predial Otras Vigencias ';
                        $strDetalle = $strDetalle.$intVigencia;
                        if($intValorCredito){
                            if($arrTesoParametros['cobro_ambiental'] =="S"){
                                $intValorPptto = $intValorCredito-$intAmbiental;
                            }
                            foreach ($arrCuentasContables as $cuenta) {
                                if($cuenta['debito']=="S"){
                                    if($flag){
                                        //Crédito
                                        $this->insertContabilidadCajaDet($con,$idRecibo,$cuenta['cuenta'],$strTercero,$strDetalle,0,$intValorCredito);
                                        //Débito
                                        $this->insertContabilidadCajaDet($con,$idRecibo,$strCuentaDebito,$strTercero,$strDetalle,$intValorDebito,0);
                                    }else{
                                        //Débito
                                        $this->insertContabilidadCajaDet($con,$idRecibo,$cuenta['cuenta'],$strTercero,$strDetalle,$intValorCredito,0);
                                        //Crédito
                                        $this->insertContabilidadCajaDet($con,$idRecibo,$strCuentaDebito,$strTercero,$strDetalle,0,$intValorDebito);
                                    }
                                }

                            }
                        }
                    }else if($k['concepto'] == "24" || $k['concepto'] == "25"){ //Ingreso Sobretasa Ambiental
                        $intValorCredito = $intAmbiental;
                        $intValorDebito = $intAmbiental;
                        $intValorPptto = $intAmbiental;
                        $strDetalle = 'Ingreso sobretasa ambiental '.$intVigencia;
                        $strDetalleRec = 'Reconocimiento de Ingreso Sobretasa Ambiental '.$intVigencia;
                        if($intValorCredito>0){
                            foreach ($arrCuentasContables as $cuenta) {
                                $inicioCuenta = substr($cuenta['cuenta'], 0, 1);
                                if($inicioCuenta == '2'){
                                    if($cuenta['credito']=="S"){
                                        if($flag){
                                            //Crédito
                                            $this->insertContabilidadCajaDet($con,$idRecibo,$cuenta['cuenta'],$strTercero,$strDetalle,0,$intValorCredito);
                                            //Débito
                                            $this->insertContabilidadCajaDet($con,$idRecibo,$strCuentaDebito,$strTercero,$strDetalle,$intValorDebito,0);
                                        }else{
                                            //Débito
                                            $this->insertContabilidadCajaDet($con,$idRecibo,$cuenta['cuenta'],$strTercero,$strDetalle,$intValorCredito,0);
                                            //Crédito
                                            $this->insertContabilidadCajaDet($con,$idRecibo,$strCuentaDebito,$strTercero,$strDetalle,0,$intValorDebito);
                                        }
                                    }
                                }else{
                                    if($cuenta['credito']=="S"){
                                        if($flag){
                                            //Crédito reconocimiento
                                            $this->insertContabilidadCajaDet($con,$idRecibo,$cuenta['cuenta'],$strTercero,$strDetalleRec,0,$intValorCredito);
                                        }else{
                                            //Débito reconocimiento
                                            $this->insertContabilidadCajaDet($con,$idRecibo,$cuenta['cuenta'],$strTercero,$strDetalleRec,$intValorCredito,0);
                                        }
                                    }else{
                                        if($flag){
                                            //Débito reconocimiento
                                            $this->insertContabilidadCajaDet($con,$idRecibo,$cuenta['cuenta'],$strTercero,$strDetalleRec,$intValorCredito,0);
                                            //Crédito
                                            $this->insertContabilidadCajaDet($con,$idRecibo,$cuenta['cuenta'],$strTercero,$strDetalle,0,$intValorCredito);
                                            //Débito
                                            $this->insertContabilidadCajaDet($con,$idRecibo,$cuenta['cuenta'],$strTercero,$strDetalle,$intValorDebito,0);
                                        }else{
                                            //Crédito reconocimiento
                                            $this->insertContabilidadCajaDet($con,$idRecibo,$cuenta['cuenta'],$strTercero,$strDetalleRec,0,$intValorCredito);
                                            //Débito
                                            $this->insertContabilidadCajaDet($con,$idRecibo,$cuenta['cuenta'],$strTercero,$strDetalle,$intValorCredito,0);
                                            //Crédito
                                            $this->insertContabilidadCajaDet($con,$idRecibo,$cuenta['cuenta'],$strTercero,$strDetalle,0,$intValorDebito);
                                        }
                                    }
                                }

                            }
                        }
                    }else if($k['concepto'] == "26" || $k['concepto'] == "27"){//Ingreso Sobretasa Bomberil
                        $intValorCredito = $intBomberil;
                        $intValorDebito = $intBomberil;
                        $intValorPptto = $intBomberil;
                        $strDetalle = 'Ingreso Sobretasa Bomberil '.$intVigencia;
                        $strDetalleRec = 'Reconocimiento de Ingreso Sobretasa bomberil '.$intVigencia;
                        if($intValorCredito>0){
                            foreach ($arrCuentasContables as $cuenta) {
                                if($cuenta['credito']=="S"){
                                    if($flag){
                                        //Crédito reconocimiento
                                        $this->insertContabilidadCajaDet($con,$idRecibo,$cuenta['cuenta'],$strTercero,$strDetalleRec,0,$intValorCredito);
                                    }else{
                                        //Débito reconocimiento
                                        $this->insertContabilidadCajaDet($con,$idRecibo,$cuenta['cuenta'],$strTercero,$strDetalleRec,$intValorCredito,0);
                                    }
                                }else{
                                    if($flag){
                                        //Débito reconocimiento
                                        $this->insertContabilidadCajaDet($con,$idRecibo,$cuenta['cuenta'],$strTercero,$strDetalleRec,$intValorCredito,0);
                                        //Crédito
                                        $this->insertContabilidadCajaDet($con,$idRecibo,$cuenta['cuenta'],$strTercero,$strDetalle,0,$intValorCredito);
                                        //Débito
                                        $this->insertContabilidadCajaDet($con,$idRecibo,$cuenta['cuenta'],$strTercero,$strDetalle,$intValorDebito,0);
                                    }else{
                                        //Crédito reconocimiento
                                        $this->insertContabilidadCajaDet($con,$idRecibo,$cuenta['cuenta'],$strTercero,$strDetalleRec,0,$intValorCredito);
                                        //Débito
                                        $this->insertContabilidadCajaDet($con,$idRecibo,$cuenta['cuenta'],$strTercero,$strDetalle,$intValorCredito,0);
                                        //Crédito
                                        $this->insertContabilidadCajaDet($con,$idRecibo,$cuenta['cuenta'],$strTercero,$strDetalle,0,$intValorDebito);
                                    }
                                }
                            }
                        }
                    }else if($k['concepto'] == "31"){ //Descuento Pronto Pago Predial
                        $intValorDebito = $intDescuento;
                        $strDetalle = 'Descuento Pronto Pago Predial '.$intVigencia;
                        if($intValorDebito>0){
                            foreach ($arrCuentasContables as $cuenta) {
                                if($cuenta['credito']=="S"){
                                    if($intValorDebito > 0){
                                        if($flag){
                                            //Crédito
                                            $this->insertContabilidadCajaDet($con,$idRecibo,$cuenta['cuenta'],$strTercero,$strDetalle,$intValorDebito,0);
                                        }else{
                                            //Débito
                                            $this->insertContabilidadCajaDet($con,$idRecibo,$cuenta['cuenta'],$strTercero,$strDetalle,0,$intValorDebito);
                                        }
                                    }
                                }
                            }
                        }
                    }else if($k['concepto'] == "28"){ //Intereses Predial
                        $intValorCredito = $intPredialIntereses;
                        $intValorDebito = $intPredialIntereses;
                        $intValorPptto = $intPredialIntereses;
                        $strDetalle = 'Intereses Predial '.$intVigencia;
                        $strDetalleRec = 'Reconocimiento Intereses Predial '.$intVigencia;
                        if($intValorCredito>0){
                            foreach ($arrCuentasContables as $cuenta) {
                                if($cuenta['credito']=="S"){
                                    if($flag){
                                        //Crédito reconocimiento
                                        $this->insertContabilidadCajaDet($con,$idRecibo,$cuenta['cuenta'],$strTercero,$strDetalleRec,0,$intValorCredito);
                                    }else{
                                        //Débito reconocimiento
                                        $this->insertContabilidadCajaDet($con,$idRecibo,$cuenta['cuenta'],$strTercero,$strDetalleRec,$intValorCredito,0);
                                    }
                                }else{
                                    if($flag){
                                        //Débito reconocimiento
                                        $this->insertContabilidadCajaDet($con,$idRecibo,$cuenta['cuenta'],$strTercero,$strDetalleRec,$intValorCredito,0);
                                        //Crédito
                                        $this->insertContabilidadCajaDet($con,$idRecibo,$cuenta['cuenta'],$strTercero,$strDetalle,0,$intValorCredito);
                                        //Débito
                                        $this->insertContabilidadCajaDet($con,$idRecibo,$cuenta['cuenta'],$strTercero,$strDetalle,$intValorDebito,0);
                                    }else{
                                        //Crédito reconocimiento
                                        $this->insertContabilidadCajaDet($con,$idRecibo,$cuenta['cuenta'],$strTercero,$strDetalleRec,0,$intValorCredito);
                                        //Débito
                                        $this->insertContabilidadCajaDet($con,$idRecibo,$cuenta['cuenta'],$strTercero,$strDetalle,$intValorCredito,0);
                                        //Crédito
                                        $this->insertContabilidadCajaDet($con,$idRecibo,$cuenta['cuenta'],$strTercero,$strDetalle,0,$intValorDebito);
                                    }
                                }
                            }
                        }
                    }else if($k['concepto'] == "30"){ //Intereses Bomberil
                        $intValorCredito = $intBomberilIntereses;
                        $intValorDebito = $intBomberilIntereses;
                        $intValorPptto = $intBomberilIntereses;
                        $strDetalle = 'Intereses Bomberil '.$intVigencia;
                        $strDetalleRec = 'Reconocimiento de Intereses Sobretasa Bomberil '.$intVigencia;
                        if($intValorCredito>0){
                            foreach ($arrCuentasContables as $cuenta) {
                                if($cuenta['credito']=="S"){
                                    if($flag){
                                        //Crédito reconocimiento
                                        $this->insertContabilidadCajaDet($con,$idRecibo,$cuenta['cuenta'],$strTercero,$strDetalleRec,0,$intValorCredito);
                                    }else{
                                        //Débito reconocimiento
                                        $this->insertContabilidadCajaDet($con,$idRecibo,$cuenta['cuenta'],$strTercero,$strDetalleRec,$intValorCredito,0);
                                    }
                                }else{
                                    if($flag){
                                        //Débito reconocimiento
                                        $this->insertContabilidadCajaDet($con,$idRecibo,$cuenta['cuenta'],$strTercero,$strDetalleRec,$intValorCredito,0);
                                        //Crédito
                                        $this->insertContabilidadCajaDet($con,$idRecibo,$cuenta['cuenta'],$strTercero,$strDetalle,0,$intValorCredito);
                                        //Débito
                                        $this->insertContabilidadCajaDet($con,$idRecibo,$cuenta['cuenta'],$strTercero,$strDetalle,$intValorDebito,0);
                                    }else{
                                        //Crédito reconocimiento
                                        $this->insertContabilidadCajaDet($con,$idRecibo,$cuenta['cuenta'],$strTercero,$strDetalleRec,0,$intValorCredito);
                                        //Débito
                                        $this->insertContabilidadCajaDet($con,$idRecibo,$cuenta['cuenta'],$strTercero,$strDetalle,$intValorCredito,0);
                                        //Crédito
                                        $this->insertContabilidadCajaDet($con,$idRecibo,$cuenta['cuenta'],$strTercero,$strDetalle,0,$intValorDebito);
                                    }
                                }
                            }
                        }
                    }else if($k['concepto'] == "29"){ //Intereses Ambiental
                        $intValorCredito = $intAmbientalIntereses;
                        $intValorDebito = $intAmbientalIntereses;
                        $intValorPptto = $intAmbientalIntereses;
                        $strDetalle = 'Intereses sobretasa ambiental '.$intVigencia;
                        $strDetalleRec = 'Reconocimiento de Intereses Sobretasa Ambiental '.$intVigencia;
                        if($intValorCredito>0){
                            foreach ($arrCuentasContables as $cuenta) {
                                $inicioCuenta = substr($cuenta['cuenta'], 0, 1);
                                if($inicioCuenta == '2'){
                                    if($cuenta['credito']=="S"){
                                        if($flag){
                                            //Crédito
                                            $this->insertContabilidadCajaDet($con,$idRecibo,$cuenta['cuenta'],$strTercero,$strDetalle,0,$intValorCredito);
                                            //Débito
                                            $this->insertContabilidadCajaDet($con,$idRecibo,$strCuentaDebito,$strTercero,$strDetalle,$intValorDebito,0);
                                        }else{
                                            //Débito
                                            $this->insertContabilidadCajaDet($con,$idRecibo,$cuenta['cuenta'],$strTercero,$strDetalle,$intValorCredito,0);
                                            //Crédito
                                            $this->insertContabilidadCajaDet($con,$idRecibo,$strCuentaDebito,$strTercero,$strDetalle,0,$intValorDebito);
                                        }
                                    }
                                }else{
                                    if($cuenta['credito']=="S"){
                                        if($flag){
                                            //Crédito reconocimiento
                                            $this->insertContabilidadCajaDet($con,$idRecibo,$cuenta['cuenta'],$strTercero,$strDetalleRec,0,$intValorCredito);
                                        }else{
                                            //Débito reconocimiento
                                            $this->insertContabilidadCajaDet($con,$idRecibo,$cuenta['cuenta'],$strTercero,$strDetalleRec,$intValorCredito,0);
                                        }
                                    }else{
                                        if($flag){
                                            //Débito reconocimiento
                                            $this->insertContabilidadCajaDet($con,$idRecibo,$cuenta['cuenta'],$strTercero,$strDetalleRec,$intValorCredito,0);
                                            //Crédito
                                            $this->insertContabilidadCajaDet($con,$idRecibo,$cuenta['cuenta'],$strTercero,$strDetalle,0,$intValorCredito);
                                            //Débito
                                            $this->insertContabilidadCajaDet($con,$idRecibo,$cuenta['cuenta'],$strTercero,$strDetalle,$intValorDebito,0);
                                        }else{
                                            //Crédito reconocimiento
                                            $this->insertContabilidadCajaDet($con,$idRecibo,$cuenta['cuenta'],$strTercero,$strDetalleRec,0,$intValorCredito);
                                            //Débito
                                            $this->insertContabilidadCajaDet($con,$idRecibo,$cuenta['cuenta'],$strTercero,$strDetalle,$intValorCredito,0);
                                            //Crédito
                                            $this->insertContabilidadCajaDet($con,$idRecibo,$cuenta['cuenta'],$strTercero,$strDetalle,0,$intValorDebito);
                                        }
                                    }
                                }

                            }
                        }
                    }else if($k['concepto'] == "34"){//Alumbrado
                        $intAlumbrado = $this->selectValorAlumbrado($con,$idLiquidacion,$intLiquidacion);
                        if($intAlumbrado > 0){
                            $intValorCredito = $intAlumbrado;
                            $intValorDebito = $intAlumbrado;
                            $intValorPptto = $intAlumbrado;
                            $strDetalle = 'Ingreso Impuesto sobre el Servicio de Alumbrado Publico '.$intVigencia;
                            $strDetalleRec = 'Reconocimiento de Ingreso Impuesto sobre el Servicio de Alumbrado Publico '.$intVigencia;
                            foreach ($arrCuentasContables as $cuenta) {
                                if($cuenta['credito']=="S"){
                                    if($flag){
                                        //Crédito reconocimiento
                                        $this->insertContabilidadCajaDet($con,$idRecibo,$cuenta['cuenta'],$strTercero,$strDetalleRec,0,$intValorCredito);
                                    }else{
                                        //Débito reconocimiento
                                        $this->insertContabilidadCajaDet($con,$idRecibo,$cuenta['cuenta'],$strTercero,$strDetalleRec,$intValorCredito,0);
                                    }
                                }else{
                                    //Crédito reconocimiento
                                    $this->insertContabilidadCajaDet($con,$idRecibo,$cuenta['cuenta'],$strTercero,$strDetalleRec,0,$intValorCredito);
                                    //Débito
                                    $this->insertContabilidadCajaDet($con,$idRecibo,$cuenta['cuenta'],$strTercero,$strDetalle,$intValorCredito,0);
                                    //Crédito
                                    $this->insertContabilidadCajaDet($con,$idRecibo,$cuenta['cuenta'],$strTercero,$strDetalle,0,$intValorDebito);
                                }
                            }
                        }

                    }
                    if($k['concepto'] != "31"){
                        if($intValorPptto > 0){
                            $this->insertPresupuesto($con,$k['cuentapres'],$idRecibo,$intValorPptto,$this->intVigenciaActual,$strCodigo,$k['fuente'],$flag);
                        }
                    }
                }
            }
            $this->insertReciboCajaDet($con,$idRecibo,$arrDetalleCaja);
            return $request;
        }
        public function insertContabilidadCajaDet($con,$idRecibo,$cuenta,$tercero,$detalle,$debito,$credito){
            $sql="insert into comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,valdebito,valcredito,estado,vigencia)
            values (
                '5 $idRecibo',
                '$cuenta',
                '$tercero',
                '01',
                '$detalle',
                '$debito',
                '$credito',
                '1',
                '$this->intVigenciaActual'
            )";
		    mysqli_query($con,$sql);
        }
        public function insertPresupuesto($con,$cuenta,$idRecibo,$valor,$vigencia,$ingreso,$fuente,$flag){
            if($flag){
                $sql="insert into pptorecibocajappto (cuenta,idrecibo,valor,vigencia,tipo,ingreso,fuente, seccion_presupuestal, medio_pago, vigencia_gasto)
                values('$cuenta',$idRecibo,'$valor','$vigencia','N','$ingreso', '$fuente', '16', 'CSF', '1')";
            }else{
                $sql="DELETE FROM pptorecibocajappto WHERE idrecibo = $idRecibo";
            }
			mysqli_query($con,$sql);
        }
        public function insertReciboCajaDet($con,$idRecibo,$data){
            foreach ($data as $d) {
                $sql="INSERT INTO tesoreciboscaja_det (id_recibos,ingreso,valor,estado)
                VALUES($idRecibo,'$d[codigo]','$d[valor]','S')";
                mysqli_query($con,$sql);
            }
        }
        public function selectValorAlumbrado($con,$idPredio,$vigencia){
            $sql = "SELECT val_alumbrado FROM tesoliquidapredial_desc WHERE id_predial = $idPredio AND vigencia = '$vigencia'";
            $request = mysqli_query($con,$sql)->fetch_assoc();
            $intValor = !empty($request) ? $request['val_alumbrado'] : 0;
            return $intValor;
        }
        public function selectTipoPredio($con,$cedulaCatastral,$actual,$vigencia){
            $tipoPredio = substr($cedulaCatastral,0,2);
            $codigo ="";
            $arrCajaDet = array();
            if($actual){
                $codigo = $tipoPredio=="00" ? "03" : "01";
            }else{
                $codigo = $tipoPredio=="00" ? "04" : "02";
            }
            //Datos para armar el detalle de recibo caja
            $sql ="SELECT codigo,nombre FROM tesoingresos_predial WHERE codigo='$codigo'";
            $requestCab = mysqli_query($con,$sql)->fetch_assoc();
            $arrCajaDet = array("codigo"=>$requestCab['codigo'],"concepto"=>$requestCab['nombre']." ".$vigencia);

            $sql="SELECT * FROM tesoingresos_predial_det WHERE codigo='$codigo' AND
            vigencia=(SELECT MAX(vigencia) FROM tesoingresos_predial_det WHERE codigo = '$codigo') GROUP BY concepto ORDER BY concepto ASC";
            $requestIngresos = mysqli_fetch_all(mysqli_query($con,$sql),MYSQLI_ASSOC);
            return array("codigo"=>$codigo,"ingresos"=>$requestIngresos,"caja"=>$arrCajaDet);
        }
        public function selectCuentasContables($con,$codigo){
            $sql = "SELECT * FROM conceptoscontables_det WHERE estado='S' AND modulo='4' AND codigo = '$codigo' AND tipo='PR'
            AND fechainicial = (SELECT MAX(fechainicial) FROM conceptoscontables_det where estado='S' and modulo='4' AND codigo = '21' and tipo='PR')";
            $request = mysqli_fetch_all(mysqli_query($con,$sql),MYSQLI_ASSOC);
            return $request;
        }
    }
?>
