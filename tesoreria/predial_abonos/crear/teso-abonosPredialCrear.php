<?php
    ini_set('display_errors', '1');
    ini_set('display_startup_errors', '1');
    error_reporting(E_ALL);

    date_default_timezone_set("America/Bogota");
    session_start();
    require '../../../Librerias/vendor/autoload.php';
    require '../../../comun.inc';
    require '../../../funciones.inc';
    require '../../../funcionesSP.inc.php';
    require_once 'ContabilidadTrait.php';
    require_once 'LiquidacionTrait.php';
    require_once 'ReciboTrait.php';


    class Plantilla{
        use ContabilidadTrait,LiquidacionTrait,ReciboTrait;
        private $linkbd;
        private $intId;
        private $intTipoComp;
        private $strTipoComp;
        private $intDebito;
        private $intCredito;
        private $intDiferencia;
        private $chrEstado;
        private $chrCentroCosto;
        private $strConcepto;
        private $strCuenta;
        private $strTercero;
        private $intTotal;
        private $strVigencia;
        private $intTipoRevertComp;
        private $intEstadoCont;
        private $strFecha;
        public function __construct() {
            $this->linkbd = conectar_v7();
            $this->linkbd->set_charset("utf8");
            $this->chrEstado = 1;
            $this->intDiferencia = 0;
            $this->intTotal = 0;
            $this->intTipoComp = 34;
            $this->intTipoRevertComp = 2034;
            $this->chrCentroCosto = "01";
            $this->intEstadoCont = 1;
        }
        public function save(){
            if(!empty($_SESSION)){
                if(empty($_POST['data']) || empty($_POST['movimiento'])){
                    $arrResponse = array("status"=>false,"msg"=>"Error de datos");
                }else{
                    $arrData = json_decode($_POST['data'],true);
                    $strUsuario = strClean($_SESSION['cedulausu']);
                    $strMovimiento = strClean($_POST['movimiento']);
                    $strFecha = $_POST['fecha'];
                    $strFechaRev = $_POST['fecha_rev'];

                    if($strMovimiento == "201"){
                        if(empty($arrData) || empty($arrData['cuenta']) || empty($arrData['acuerdo']) || empty($arrData['abono'])){
                            $arrResponse = array("status"=>false,"msg"=>"Los datos están vacíos");
                        }else{
                            $strEstado = "S";
                            $request = $this->insertAbono($strUsuario,$arrData,$strMovimiento,$strEstado,$strFecha);
                            if(is_numeric($request) && $request > 0){
                                $arrResponse = array("status"=>true,"msg"=>"Datos guardados","consecutivo"=>$this->selectConsecutivo());
                            }else{
                                $arrResponse = array("status"=>false,"msg"=>"Ha ocurrido un error, inténtelo de nuevo.");
                            }
                        }
                    }else if($strMovimiento == "401"){
                        if(empty($arrData) || empty($arrData['descripcion']) || empty($arrData['id'])){
                            $arrResponse = array("status"=>false,"msg"=>"Los datos están vacíos");
                        }else{
                            $strEstado = "N";
                            $intIdAbono = intval($arrData['id']);
                            $strDescripcion = ucfirst(replaceChar(strClean($arrData['descripcion'])));
                            $request = $this->revertAbono($intIdAbono,$strUsuario,$strMovimiento,$strEstado,$strDescripcion,$strFechaRev);
                            if(is_numeric($request) && $request > 0){
                                $arrResponse = array("status"=>true,"msg"=>"Documento reversado correctamente.","consecutivo"=>$this->selectConsecutivo());
                            }else{
                                $arrResponse = array("status"=>false,"msg"=>"Ha ocurrido un error, inténtelo de nuevo.");
                            }
                        }
                    }else{
                        $arrResponse = array("status"=>false,"msg"=>"Error de datos, no existe el tipo de movimiento");
                    }
                }
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
            die();

        }
        public function getData(){
            if(!empty($_SESSION)){
                $arrResponse = array(
                    "acuerdos" =>$this->selectAcuerdos(),
                    "cuentas"=>$this->selectCuentas(),
                    "abonos"=>$this->selectAbonos()
                );
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
            die();
        }
        public function revertAbono(int $intIdAbono,string $strUsuario,string $strMovimiento,string $strEstado,string $strDescripcion,string $strFechaRev){
            $intConsecutivo = $this->selectConsecutivo()+1;
            $arrAbono = $this->selectAbono($intIdAbono);
            $arrCuentasPuente = $this->selectCuentaPuente();
            $strCuentaPuente = $arrCuentasPuente['cuentapuente'];
            $arrAbonoDet = $arrAbono['detalle'];
            $sql = "UPDATE tesoabono SET estado = '$strEstado' WHERE tipomovimiento = '201' AND id_abono = $intIdAbono";
            mysqli_query($this->linkbd,$sql);

            //REVIERTE EL ABONO
            $sql = "INSERT INTO tesoabono(id_abono,idacuerdo,fecha,tercero,valortotal,concepto,estado,tipomovimiento,usuario,cierre,recaudado,cuentabanco,recibo)
            VALUES(
                $intConsecutivo,
                {$arrAbono['idacuerdo']},
                '$strFechaRev',
                '{$arrAbono['tercero']}',
                {$arrAbono['valortotal']},
                '$strDescripcion',
                'S',
                '$strMovimiento',
                '$strUsuario',
                '{$arrAbono['cierre']}',
                '{$arrAbono['recaudado']}',
                '{$arrAbono['cuentabanco']}',
                '{$arrAbono['recibo']}'
            )";
            $request = intval(mysqli_query($this->linkbd,$sql));
            if($request>0){
                insertAuditoria("teso_auditoria","teso_funciones_id",3,"Reversar",$intConsecutivo);
                $arrData = array(
                    "id"=>$intIdAbono,
                    "numerotipo"=>$intConsecutivo,
                    "concepto"=>$strDescripcion,
                    "abono"=>$arrAbono['valortotal'],
                    "tipo_comp"=>$this->intTipoRevertComp,
                    "tipo_comp_abono"=>$this->intTipoComp,
                    "diferencia"=>$this->intDiferencia,
                    "estado"=>$this->intEstadoCont,
                    "vigencia"=>$arrAbono['vigencia'],
                    "cuenta"=>$arrAbono['cuentabanco'],
                    "tercero"=>$arrAbono['tercero'],
                    "str_tipo_comp"=>$this->intTipoRevertComp." ".$intConsecutivo,
                    "centro_costo"=>$this->chrCentroCosto,
                    "cuenta_puente"=>$strCuentaPuente
                );
                if(!empty($arrAbonoDet)){
                    $sqlAbonoDet ="UPDATE tesoabono_det SET estado = '$strEstado' WHERE tipomovimiento = '201' AND estado='S' AND id_abono = $intIdAbono";
                    mysqli_query($this->linkbd,$sqlAbonoDet);
                    foreach ($arrAbonoDet as $det) {
                        $sqlAbonoDet = "INSERT INTO tesoabono_det(id_abono,vigencia,codcatastral,valor,estado,tipomovimiento)
                        VALUES($intConsecutivo,'$det[vigencia]','$det[codcatastral]','$det[valor]','S','401')";
                        mysqli_query($this->linkbd,$sqlAbonoDet);
                        //Revertir vigencias pagas en acuerdo predial
                        $sqlAcuerdoDet = "UPDATE tesoacuerdopredial_det SET estado='S' WHERE idacuerdo='$arrAbono[idacuerdo]' AND vigencia='$det[vigencia]'";
                        mysqli_query($this->linkbd,$sqlAcuerdoDet);
                    }
                    $arrVigencias = $this->revertLiquidacion($this->linkbd,$arrAbono['cierre']);
                    $arrRevRecibo = $this->revertReciboCaja($this->linkbd,$arrAbono['recibo'],$arrAbono);
                    //GUARDA RECIBO EN CONTABILIDAD
                    $this->insertContabilidadCaja(
                        $this->linkbd,
                        $arrAbono['recibo'],
                        "Reversión de ".$arrRevRecibo['descripcion'],
                        $arrRevRecibo['valor'],
                        $arrAbono['cierre'],
                        $arrAbono['codcatastral'],
                        $arrCuentasPuente,
                        $strCuentaPuente,
                        $arrAbono['tercero'],
                        $arrVigencias,
                        false
                    );
                }
                //REVIERTE ABONO EN CONTABILIDAD
                $this->insertContabilidadAbono($this->linkbd,$arrData,$strFechaRev,"401");
            }
            return $request;
        }
        public function insertAbono(string $strUsuario,array $arrData,string $strMovimiento,string $strEstado,string $strFecha){
            $arrCuentasPuente = $this->selectCuentaPuente();
            $arrCuenta = $arrData['cuenta'];
            $arrAcuerdo = $arrData['acuerdo'];
            //dep($arrAcuerdo);exit;
            $arrAcuerdo['concepto'] = ucfirst($arrAcuerdo['concepto']);
            $intAbono = $arrData['abono'];
            $strRecaudado = $arrData['recaudado'];
            $strCodCatastral = $arrAcuerdo['codigo'];
            $strCuenta = $strRecaudado == "banco" ? $arrCuenta['cuenta'] : $arrCuentasPuente['cuentacaja'];
            $strCuentaPuente = $arrCuentasPuente['cuentapuente'];
            $intConsecutivo = $this->selectConsecutivo()+1;
            $arrAcuerdoDetalle = array_values(array_filter($arrAcuerdo['detalle'],function($e){return $e['estado'] == "P" && $e['is_pago'] !=1;}));
            $arrAcuerdo['detalle'] = $arrAcuerdoDetalle;
            //GUARDA EN ABONO
            $sql = "INSERT INTO tesoabono(id_abono,idacuerdo,fecha,tercero,valortotal,concepto,estado,tipomovimiento,recaudado,cuentabanco,usuario)
            VALUES(
                $intConsecutivo,
                {$arrAcuerdo['id']},
                '$strFecha',
                '{$arrAcuerdo['documento']}',
                $intAbono,
                '{$arrAcuerdo['concepto']}',
                '$strEstado',
                '$strMovimiento',
                '$strRecaudado',
                '$strCuenta',
                '$strUsuario'
            )";
            $request = intval(mysqli_query($this->linkbd,$sql));
            if($request>0){
                insertAuditoria("teso_auditoria","teso_funciones_id",3,"Crear",$intConsecutivo);
                $arrData = array(
                    "id"=>$intConsecutivo,
                    "concepto"=>$arrAcuerdo['concepto'],
                    "abono"=>$intAbono,
                    "tipo_comp"=>$this->intTipoComp,
                    "diferencia"=>$this->intDiferencia,
                    "estado"=>$this->intEstadoCont,
                    "vigencia"=>$arrAcuerdo['vigencia'],
                    "cuenta"=>$arrCuenta['cuenta'],
                    "tercero"=>$arrAcuerdo['documento'],
                    "str_tipo_comp"=>$this->intTipoComp." ".$intConsecutivo,
                    "centro_costo"=>$this->chrCentroCosto,
                    "cuenta_puente"=>$strCuentaPuente
                );
                if(count($arrAcuerdoDetalle)>0){
                    $totalValorCaja = 0;
                    foreach ($arrAcuerdoDetalle as $k) {
                        $totalValorCaja+=$k['total_liquidacion'];
                    }
                    $this->insertAbonoDet($intConsecutivo,$strCodCatastral,$arrAcuerdoDetalle,$arrAcuerdo['id']);
                    //GUARDA LIQUIDACIÓN
                    $arrLiquidacion = $this->insertLiquidacion($this->linkbd,$arrAcuerdo,$strFecha);
                    //GUARDA RECIBO DE CAJA
                    $idRecibo =$this->insertReciboCaja(
                        $this->linkbd,$this->selectConsecutivo(),
                        $arrLiquidacion['liquidacion'],
                        $arrLiquidacion['concepto'],
                        $strCuenta,
                        $totalValorCaja,
                        $strRecaudado,
                        $arrAcuerdo
                    );
                    //GUARDA RECIBO EN CONTABILIDAD
                    $this->insertContabilidadCaja(
                        $this->linkbd,
                        $idRecibo,
                        $arrLiquidacion['concepto'],
                        $totalValorCaja,
                        $arrLiquidacion['liquidacion'],
                        $arrAcuerdo['codigo'],
                        $arrCuentasPuente,
                        $strCuentaPuente,
                        $arrAcuerdo['documento'],
                        $arrAcuerdo['detalle']
                    );
                }
                //GUARDA ABONO EN CONTABILIDAD
                $this->insertContabilidadAbono($this->linkbd,$arrData,$strFecha);
            }
            return $request;
        }
        public function insertAbonoDet(int $intConsecutivo,string $strCodCatastral,array $arrData,int $intAcuerdo){
            foreach ($arrData as $det) {
                $sql="INSERT INTO tesoabono_det(id_abono,vigencia,codcatastral,valor,estado,tipomovimiento)
                VALUES($intConsecutivo,'$det[vigencia]','$strCodCatastral',$det[total_liquidacion],'S','201')";
                mysqli_query($this->linkbd,$sql);

                //Actualizo vigencias a pagas en detalle del acuerdo
                $sql_acuerdo_det="UPDATE tesoacuerdopredial_det SET estado = 'P' WHERE vigencia='$det[vigencia]' AND idacuerdo =$intAcuerdo";
                mysqli_query($this->linkbd,$sql_acuerdo_det);
            }
        }
        public function selectConsecutivo(){
            $sql = "SELECT MAX(id_abono ) as id FROM tesoabono";
            $request = mysqli_query($this->linkbd,$sql)->fetch_assoc()['id'];
            return $request;
        }
        public function selectConsecutivoAbonoAcuerdo($id){
            $sql = "SELECT COUNT(idacuerdo) as total FROM tesoabono WHERE idacuerdo=$id AND estado='S' AND tipomovimiento='201'";
            $request = mysqli_query($this->linkbd,$sql)->fetch_assoc()['total']+1;
            return $request;
        }
        public function selectCuentas(){
            $sql = "SELECT TB2.razonsocial,TB1.cuenta,TB1.ncuentaban as banco,TB1.tipo
            FROM tesobancosctas TB1
            INNER JOIN terceros TB2
            ON TB1.tercero = TB2.cedulanit
            INNER JOIN cuentasnicsp TB3
            ON TB1.cuenta=TB3.cuenta
            WHERE TB1.estado='S'";
            $request = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
            return $request;
        }
        public function selectAcuerdos(){
            $arrData = [];
            $sql = "SELECT
            idacuerdo as id,
            codcatastral as codigo,
            tercero as documento,
            vigencia,
            recibo as estado_cuenta,
            valor_pago
            FROM tesoacuerdopredial
            WHERE estado = 'S' ORDER BY idacuerdo DESC";
            $request = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
            $total = count($request);
            for ($i=0; $i < $total ; $i++) {
                $abono = $this->selectTotalAbonos($request[$i]['id']);
                $arrCuotas = $this->selectCuotas($request[$i]['id'],$abono);
                $arrDetalle = $this->selectDetalleAcuerdo($request[$i]['id']);
                $strFecha = date("Y");
                $intNumeroAbono = $this->selectConsecutivoAbonoAcuerdo($request[$i]['id']);
                $strConcepto = "Abono Nro. ".$intNumeroAbono." del acuerdo de pago predial Nro. ".$request[$i]['id']." del ".$strFecha;
                $totalVigenciasPagas = 0;
                $totalLiquidacion = 0;
                foreach ($arrDetalle as $d) {
                    $totalLiquidacion+=$d['total_liquidacion'];
                    if($d['is_pago']){
                        $totalVigenciasPagas+=$d['total_liquidacion'];
                    }
                }
                $request[$i]['cuotas']= $arrCuotas;
                $request[$i]['detalle']= $arrDetalle;
                $request[$i]['concepto'] = $strConcepto;
                $request[$i]['abono'] = $abono;
                $request[$i]['saldo'] = $abono-$totalVigenciasPagas;
                $request[$i]['debe'] = $totalLiquidacion-$abono;
                $request[$i]['total_liquidacion'] = $totalLiquidacion;
                if($abono < $request[$i]['valor_pago']){
                    array_push($arrData,$request[$i]);
                }
            }
            //dep($arrData);exit;
            return $arrData;
        }
        public function selectcuotas($id,$abono){
            $sql = "SELECT cuota,
            valor_pago as valor,
            DATE_FORMAT(fecha_pago,'%d/%m/%Y') as fecha,
            estado
            FROM tesoacuerdopredial_pagos WHERE idacuerdo = $id AND estado = 'S'";
            $request = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
            $total = count($request);
            $totalValorCuotas = 0;
            $flag = false;
            for ($i=0; $i < $total ; $i++) {
                $totalValorCuotas+= $request[$i]['valor'];
                $request[$i]['cuota'] = intval($request[$i]['cuota']);
                $request[$i]['is_checked'] = false;
                $request[$i]['is_pago'] = false;
                $request[$i]['saldo'] = $request[$i]['valor'];
                if($abono >= $totalValorCuotas){
                    $request[$i]['estado'] ="P";
                    $request[$i]['is_pago'] =  true;
                    $request[$i]['is_checked'] = true;
                    $request[$i]['saldo'] = 0;
                }else{
                    if(!$flag){
                        if($totalValorCuotas-$abono < $abono){
                            $request[$i]['saldo'] = ceil($totalValorCuotas-$abono);
                            $flag = true;
                        }
                    }
                }

            }
            return $request;
        }
        public function selectAbono($id){
            $sql = "SELECT a.id_abono,a.idacuerdo,a.tercero,a.valortotal,a.cierre,a.recaudado,a.cuentabanco,a.recibo,ac.vigencia,ac.codcatastral
            FROM tesoabono a
            INNER JOIN tesoacuerdopredial ac ON a.idacuerdo = ac.idacuerdo
            WHERE a.tipomovimiento = '201' AND a.estado = 'S' AND a.id_abono = $id";
            $request = mysqli_query($this->linkbd,$sql)->fetch_assoc();
            $sql_det = "SELECT * FROM tesoabono_det WHERE tipomovimiento = '201' AND estado = 'S' AND id_abono = $id";
            $request['detalle'] = mysqli_fetch_all(mysqli_query($this->linkbd,$sql_det),MYSQLI_ASSOC);
            return $request;
        }
        public function selectAbonos(){
            $sql = "SELECT id_abono as idabono,idacuerdo,concepto,tercero as documento, valortotal as total
            FROM tesoabono WHERE tipomovimiento = '201' AND estado = 'S' ORDER BY id_abono DESC";
            $request = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
            return $request;
        }
        public function selectTotalAbonos($id){
            $sql = "SELECT COALESCE(sum(valortotal),0) as total FROM tesoabono WHERE idacuerdo=$id AND tipomovimiento = '201' AND estado = 'S'";
            $request = mysqli_query($this->linkbd,$sql)->fetch_assoc()['total'];
            return $request;
        }
        public function selectDetalleAcuerdo($id){
            $sql = "SELECT
                tasa as tasa_por_mil,
                predial,
                descuento as predial_descuento,
                intpredial as predial_intereses,
                descuenint as predial_descuento_intereses,
                bomberil,
                intbomberil as bomberil_intereses,
                ambiente as ambiental,
                intambiente as ambiental_intereses,
                valtotal as total_liquidacion,
                vigencia,
                diasmora as dias_mora,
                estado,
                alumbrado
                FROM tesoacuerdopredial_det
                WHERE idacuerdo = $id ORDER BY vigencia ASC";
            $request = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
            $total = count($request);
            for ($i=0; $i < $total ; $i++) {
                $request[$i]['predial'] = intval($request[$i]['predial']);
                $request[$i]['predial_descuento'] = intval($request[$i]['predial_descuento']);
                $request[$i]['predial_intereses'] = intval($request[$i]['predial_intereses']);
                $request[$i]['bomberil'] = intval($request[$i]['bomberil']);
                $request[$i]['bomberil_intereses'] = intval($request[$i]['bomberil_intereses']);
                $request[$i]['ambiental'] = intval($request[$i]['ambiental']);
                $request[$i]['ambiental_intereses'] = intval($request[$i]['ambiental_intereses']);
                $request[$i]['total_liquidacion'] = intval($request[$i]['total_liquidacion']);
                $request[$i]['alumbrado'] = intval($request[$i]['alumbrado']);
                $request[$i]['is_pago'] = $request[$i]['estado'] == "P" ? true : false;
            }
            return $request;
        }
        public function selectCuentaPuente(){
            $sql = "SELECT cuentapuente,cuentacaja,cobro_ambiental FROM tesoparametros";
            $request =mysqli_query($this->linkbd,$sql)->fetch_assoc();
            return $request;
        }
    }
    if($_POST){
        $obj = new Plantilla();
        if($_POST['action']=="get"){
            $obj->getData();
        }else if($_POST['action'] =="save"){
            $obj->save();
        }
    }
?>
