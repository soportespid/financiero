<?php
    require_once '../../../comun.inc';
    require '../../../funciones.inc';
    require '../../../funcionesSP.inc.php';
    /*ini_set('display_errors', '1');
    ini_set('display_startup_errors', '1');
    error_reporting(E_ALL);*/
    session_start();
    //dep($_POST);exit;
    if($_POST){
        $obj = new Plantilla();
        if($_POST['action']=="get"){
            $obj->getData();
        }
    }

    class Plantilla{
        private $linkbd;
        private $arrData;
        public function __construct() {
            $this->linkbd = conectar_v7();
            $this->linkbd->set_charset("utf8");
        }
        public function getData(){
            if(!empty($_SESSION)){
                $this->fixRecibos();
                $request = $this->selectData();
                echo json_encode($request,JSON_UNESCAPED_UNICODE);
            }
            die();
        }
        public function selectData(){
            $sql = "SELECT tb.id_abono as idabono,tb.idacuerdo,tb.concepto, tb.valortotal as total,
            tb.cierre,tb.recibo, ta.codcatastral, ta.tercero as documento,
            DATE_FORMAT(tb.fecha,'%d/%m/%Y') as fecha,tb.tipomovimiento
            FROM tesoabono tb
            INNER JOIN tesoacuerdopredial ta ON tb.idacuerdo = ta.idacuerdo
            WHERE tb.estado = 'S'
            ORDER BY tb.id_abono DESC";
            $arrData = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
            $total = count($arrData);
            if($total>0){
                for ($i=0; $i < $total ; $i++) {
                    $documento = strClean($arrData[$i]['documento']);
                    $idAcuerdo = strClean($arrData[$i]['idacuerdo']);
                    $codigoCatastro = strClean($arrData[$i]['codcatastral']);
                    $sql = "SELECT DISTINCT tp.codcatastral,p.id, pp.nombre_propietario as nombre
                    FROM tesoacuerdopredial tp
                    INNER JOIN predios p ON tp.codcatastral = p.codigo_catastro
                    INNER JOIN predio_propietarios pp ON pp.predio_id = p.id
                    WHERE tp.idacuerdo = '$idAcuerdo' AND pp.documento = '$documento' AND p.codigo_catastro = '$codigoCatastro'";
                    $arrData[$i]['nombre'] = mysqli_query($this->linkbd,$sql)->fetch_assoc()['nombre'];
                    $arrData[$i]['cierre'] = $arrData[$i]['cierre'] > 0 ? $arrData[$i]['cierre'] :"N/A";
                    $arrData[$i]['recibo'] = $arrData[$i]['recibo'] > 0 ? $arrData[$i]['recibo'] :"N/A";
                }
            }
            $arrResponse = array("status"=>true,"data"=>$arrData,"total"=>$total);
            return $arrResponse;
        }
        public function fixRecibos(){
            $sql = "SELECT * FROM tesoabono WHERE cierre > 0 AND recibo = 0";
            $request = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
            $total = count($request);
            if($total > 0){
                for ($i=0; $i < $total ; $i++) {
                    $idLiquidacion = $request[$i]['cierre'];
                    $idAbono = $request[$i]['id_abono'];
                    $sql_recibo = "SELECT id_recibos FROM tesoreciboscaja WHERE id_recaudo=$idLiquidacion";
                    $idRecibo = mysqli_query($this->linkbd,$sql_recibo)->fetch_assoc()['id_recibos'];
                    $sql_update = "UPDATE tesoabono SET recibo = $idRecibo WHERE id_abono = $idAbono";
                    mysqli_query($this->linkbd,$sql_update);
                }
            }
            return $request;
        }
    }
?>
