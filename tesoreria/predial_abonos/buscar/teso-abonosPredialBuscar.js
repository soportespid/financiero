const URL ='tesoreria/predial_abonos/buscar/teso-abonosPredialBuscar.php';

var app = new Vue({
    el:"#myapp",
    data() {
        return {
            isLoading:false,
            arrData:[],
            arrDataCopy:[],
            txtResults:0,
            txtSearch:"",
        }
    },
    mounted() {
        this.getData();
    },
    methods: {
        getData: async function(){
            const formData = new FormData();
            formData.append("action","get");
            this.isLoading=true;
            const response = await fetch(URL,{method:"POST", body:formData});
            const objData = await response.json();
            this.arrData = objData.data;
            this.arrDataCopy = objData.data;
            this.txtResults = objData.total;
            this.isLoading=false;
        },
        search:function(){
            let search = this.txtSearch.toLowerCase();
            this.arrDataCopy = [...this.arrData.filter(e=>e.idabono.toLowerCase() == search ||
                e.idacuerdo.toLowerCase() == search || e.documento.toLowerCase().includes(search))];
            this.txtResults = this.arrDataCopy.length;
        },
        editItem:function(id){
            window.location.href='teso-abonosPredialEditar.php?id='+id;
        },
        printExcel:function(){
            window.open('teso-abonosPredialExcel.php',"_blank");
        },
        formatNumero: function(valor){
            return new Intl.NumberFormat("en-US", {style: "currency", currency: "USD"}).format(valor);
        },
    },
    computed:{

    }
})
