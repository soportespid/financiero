<?php
require_once '../../../comun.inc';
require '../../../funciones.inc';
require '../../../funcionesSP.inc.php';
date_default_timezone_set("America/Bogota");
/*ini_set('display_errors', '1');
ini_set('display_startup_errors', '1');
error_reporting(E_ALL);*/
session_start();
if($_POST){
    $obj = new Plantilla();
    if($_POST['action']=="get"){
        $obj->getData();
    }
}
class Plantilla{
    private $linkbd;
    private $arrData;
    public function __construct() {
        $this->linkbd = conectar_v7();
        $this->linkbd->set_charset("utf8");
    }
    public function getData(){
        if(!empty($_SESSION)){
            $id = intval(strClean($_POST['codigo']));
            $request = $this->selectCabecera($id);
            if(!empty($request)){
                $cabecera = $request;
                $detalle = [];
                if($cabecera['id_predio']!=""){
                    $detalle = $this->selectDetalle($cabecera['idacuerdo'],$cabecera['id_predio'],$cabecera['idabono']);
                }
                $arrData = array(
                    "consecutivo"=>$cabecera['idabono'],
                    "consecutivos"=>$this->selectConsecutivos(),
                    "status"=>true,
                    "data"=>array(
                        "cabecera"=>$cabecera,
                        "detalle"=>$detalle
                    )
                );
            }else{
                $arrData = array("status"=>false,"consecutivo"=>$this->selectConsecutivo());
            }
            echo json_encode($arrData,JSON_UNESCAPED_UNICODE);
        }
        die();
    }
    public function selectCabecera($id){

        $sql = "SELECT tb.id_abono as idabono,tb.idacuerdo,tb.concepto, tb.valortotal,tb.recaudado,tb.cuentabanco,tb.cuentacaja,
            tb.cierre as liquidacion,tb.recibo, ta.codcatastral, ta.tercero,cta.ncuentaban as cuenta_banco,t.razonsocial as nombre_banco,
            DATE_FORMAT(tb.fecha,'%d/%m/%Y') as fecha,tb.tipomovimiento
            FROM tesoabono tb
            INNER JOIN tesoacuerdopredial ta ON tb.idacuerdo = ta.idacuerdo
            LEFT JOIN tesobancosctas cta ON tb.cuentabanco = cta.cuenta
            LEFT JOIN terceros t ON cta.tercero = t.cedulanit
            WHERE tb.estado = 'S' AND tb.id_abono = $id";
        $request = mysqli_query($this->linkbd,$sql)->fetch_assoc();
        $documento = strClean($request['tercero']);
        $idAcuerdo = strClean($request['idacuerdo']);
        $codigoCatastro = strClean($request['codcatastral']);
        $sql = "SELECT DISTINCT tp.codcatastral,p.id, pp.nombre_propietario as nombre
                    FROM tesoacuerdopredial tp
                    INNER JOIN predios p ON tp.codcatastral = p.codigo_catastro
                    INNER JOIN predio_propietarios pp ON pp.predio_id = p.id
                    WHERE tp.idacuerdo = '$idAcuerdo' AND pp.documento = '$documento' AND p.codigo_catastro = '$codigoCatastro'";
        $request['nombre'] = mysqli_query($this->linkbd,$sql)->fetch_assoc()['nombre'];

        $request['codigo'] = mysqli_query($this->linkbd,"SELECT codcatastral FROM tesoacuerdopredial WHERE idacuerdo = '$request[idacuerdo]'")->fetch_assoc()['codcatastral'];
        $request['id_predio'] = "";
        $arrDet = mysqli_fetch_all(mysqli_query($this->linkbd,"SELECT codcatastral FROM tesoabono_det WHERE id_abono = $id"),MYSQLI_ASSOC);
        if(!empty($arrDet)){
            $codigo= $request['codigo'];
            $request['id_predio'] = mysqli_query($this->linkbd,"SELECT id FROM predios WHERE codigo_catastro = '$codigo'")->fetch_assoc()['id'];
        }
        return $request;
    }
    public function selectDetalle($id,$idPredio,$idabono){
        $sql = "SELECT
        adet.tasa as tasa_por_mil,
        adet.predial,
        adet.descuento as predial_descuento,
        adet.intpredial as predial_intereses,
        adet.descuenint as predial_descuento_intereses,
        adet.bomberil,
        adet.intbomberil as bomberil_intereses,
        adet.ambiente as ambiental,
        adet.intambiente as ambiental_intereses,
        adet.valtotal as total_liquidacion,
        adet.vigencia,
        adet.diasmora as dias_mora,
        adet.estado,
        adet.alumbrado
        FROM tesoacuerdopredial_det adet
        INNER JOIN tesoabono_det abodet ON adet.vigencia = abodet.vigencia
        WHERE adet.idacuerdo = $id AND adet.estado = 'P' AND abodet.id_abono = $idabono ORDER BY adet.vigencia DESC";
        $request = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
        $total = count($request);
        for ($i=0; $i < $total ; $i++) {
            $sql = "SELECT valor_avaluo FROM predio_avaluos WHERE predio_id = $idPredio AND vigencia = '{$request[$i]['vigencia']}'";
            $request[$i]['valor_avaluo'] = intval(mysqli_query($this->linkbd,$sql)->fetch_assoc()['valor_avaluo']);
            $request[$i]['predial'] = intval($request[$i]['predial']);
            $request[$i]['predial_descuento'] = intval($request[$i]['predial_descuento']);
            $request[$i]['predial_intereses'] = intval($request[$i]['predial_intereses']);
            $request[$i]['bomberil'] = intval($request[$i]['bomberil']);
            $request[$i]['bomberil_intereses'] = intval($request[$i]['bomberil_intereses']);
            $request[$i]['ambiental'] = intval($request[$i]['ambiental']);
            $request[$i]['ambiental_intereses'] = intval($request[$i]['ambiental_intereses']);
            $request[$i]['total_liquidacion'] = intval($request[$i]['total_liquidacion']);
            $request[$i]['alumbrado'] = intval($request[$i]['alumbrado']);
        }
        return $request;
    }
    public function selectConsecutivo(){
        $sql = "SELECT MAX(id_abono) as id FROM tesoabono WHERE estado = 'S'";
        $request = mysqli_query($this->linkbd,$sql)->fetch_assoc()['id'];
        return $request;
    }
    public function selectConsecutivos(){
        $sql = "SELECT id_abono as id FROM tesoabono WHERE estado = 'S'";
        $request = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
        return $request;
    }
}

?>
