const URL ='tesoreria/predial_abonos/editar/teso-abonosPredialEditar.php';

var app = new Vue({
    el:"#myapp",
    data() {
        return {
            isLoading:false,
            isModal:false,
            arrData:[],
            arrVigencias:[],
            arrCuotas:[],
            arrConsecutivos:[],
            objTotales:{
                total_avaluo: 0,
                total_predial: 0,
                total_incentivo: 0,
                total_recaudo: 0,
                total_predial_intereses: 0,
                total_predial_descuento_intereses:0,
                total_bomberil: 0,
                total_intereses_bomberil: 0,
                total_descuento_intereses_bomberil: 0,
                total_ambiental: 0,
                total_intereses_ambiental: 0,
                total_descuento_intereses_ambiental: 0,
                total_alumbrado:0,
                total_liquidacion:0
            },
            txtCuotas:1,
            txtConsecutivo:0,
            objData:{},
            objCab:{},
            objPrintData:{}
        }
    },
    mounted() {
        this.getData();
    },
    methods: {
        getData: async function(){
            let codigo = new URLSearchParams(window.location.search).get('id');
            const formData = new FormData();
            formData.append("action","get");
            formData.append("codigo",codigo);
            this.isLoading = true;
            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();
            if(objData.status){
                const data = objData.data;
                this.objPrintData = data;
                this.txtConsecutivo = objData.consecutivo;
                this.objCab = data.cabecera;
                this.arrCuotas = data.cuotas;
                this.arrVigencias = data.detalle;
                this.arrConsecutivos = objData.consecutivos;
                this.getTotales();
            }else{
                let id = objData.consecutivo;
                window.location.href='teso-abonosPredialEditar.php?id='+id;
            }
            this.isLoading = false;
        },
        getTotales:function(){
            this.objTotales ={
                total_avaluo: 0,
                total_predial: 0,
                total_incentivo: 0,
                total_recaudo: 0,
                total_predial_intereses: 0,
                total_predial_descuento_intereses:0,
                total_bomberil: 0,
                total_intereses_bomberil: 0,
                total_ambiental: 0,
                total_intereses_ambiental: 0,
                total_alumbrado:0,
                total_liquidacion:0
            }
            this.arrVigencias.forEach(e => {
                this.objTotales.total_avaluo += e.valor_avaluo;
                this.objTotales.total_predial += e.predial + e.predial_descuento;
                this.objTotales.total_incentivo += e.predial_descuento;
                this.objTotales.total_recaudo += e.predial;
                this.objTotales.total_predial_intereses += e.predial_intereses;
                this.objTotales.total_predial_descuento_intereses += e.predial_descuento_intereses;
                this.objTotales.total_bomberil += e.bomberil;
                this.objTotales.total_intereses_bomberil += e.bomberil_intereses;
                this.objTotales.total_ambiental += e.ambiental;
                this.objTotales.total_intereses_ambiental += e.ambiental_intereses;
                this.objTotales.total_alumbrado += e.alumbrado;
                this.objTotales.total_liquidacion += e.total_liquidacion;
            });
        },
        formatNumero: function(valor){
            return new Intl.NumberFormat("en-US", {style: "currency", currency: "USD"}).format(valor);
        },
        editItem:function(type){
            let vueContext = this;
            let id = this.txtConsecutivo
            let index = this.arrConsecutivos.findIndex(function(e){return e.id == id});
            if(type=="next" && vueContext.arrConsecutivos[++index]){
                id = this.arrConsecutivos[index++].id;
            }else if(type=="prev" && vueContext.arrConsecutivos[--index]){
                id = this.arrConsecutivos[index--].id;
            }
            window.location.href='teso-abonosPredialEditar.php?id='+id;
        },
        printPDF:function(){
            window.open("teso-abonosPredialPdf.php?data="+JSON.stringify(this.objPrintData),"_blank");
        },
        showTab:function(tab){
            let tabs = this.$refs.rTabs.children;
            let tabsContent = this.$refs.rTabsContent.children;
            for (let i = 0; i < tabs.length; i++) {
                if(!tabs[i].classList.contains("exclude")){
                    tabs[i].classList.remove("active");
                    tabsContent[i].classList.remove("active")
                }
            }
            tabs[tab-1].classList.add("active");
            tabsContent[tab-1].classList.add("active")
        }
    },
    computed:{

    }
})
