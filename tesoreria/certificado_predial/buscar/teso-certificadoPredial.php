<?php
    require_once '../../../comun.inc';
    require '../../../funciones.inc';
    require '../../../funcionesSP.inc.php';
    /*ini_set('display_errors', '1');
    ini_set('display_startup_errors', '1');
    error_reporting(E_ALL);*/
    session_start();

    if($_POST){
        $obj = new Plantilla();
        if($_POST['action']=="search"){
            $currentPage = intval($_POST['page']);
            $obj->search($_POST['search'],$currentPage);
        }else if($_POST['action'] == "get"){
            $obj->getData();
        }else if($_POST['action'] == "save"){
            $obj->save($_POST['codigo']);
        }
    }

    class Plantilla{
        private $linkbd;
        private $arrData;
        public function __construct() {
            $this->linkbd = conectar_v7();
            $this->linkbd->set_charset("utf8");
        }
        public function search(string $search,int $currentPage){
            if(!empty($_SESSION)){
                $request = $this->selectData($search,$currentPage);
                echo json_encode($request,JSON_UNESCAPED_UNICODE);
            }
            die();
        }
        public function getData(){
            if(!empty($_SESSION)){
                $request = $this->selectData();
                echo json_encode($request,JSON_UNESCAPED_UNICODE);
            }
            die();
        }
        public function save(string $codigo){
            if(!empty($_SESSION)){
                $request = $this->insertData($codigo);
                if(!empty($request)){
                    $arrResponse = array("status"=>true,"data"=>$request);
                }else{
                    $arrResponse = array("status"=>false);
                }
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
            die();
        }
        public function insertData(string $codigo){
            $usuario = $_SESSION['cedulausu'];
            $sql = "SELECT codigo_catastral FROM tesopredios_certificados WHERE codigo_catastral = '$codigo'";
            $request = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
            if(empty($request)){
                $ip = getRealIP();
                $sql = "INSERT INTO tesopredios_certificados (codigo_catastral,usuario,ip) VALUES('$codigo','$usuario','$ip')";
                mysqli_query($this->linkbd,$sql);
                $sql = "SELECT codigo_catastral FROM tesopredios_certificados WHERE codigo_catastral = '$codigo'";
                $request = mysqli_query($this->linkbd,$sql);
            }else{
                $sql = "SELECT codigo_catastral FROM tesopredios_certificados WHERE codigo_catastral = '$codigo'";
                $request = mysqli_query($this->linkbd,$sql);
            }
            return $request;
        }
        public function selectData($search="",$currentPage=1){
            $perPage = 200;
            $startRows = ($currentPage-1) * $perPage;
            $totalRows = 0;

            $sql = "SELECT p.id,p.codigo_catastro,
            tp.documento,tp.nombrepropietario,tp.direccion
            FROM predios p
            INNER JOIN tesopredios tp
            ON p.codigo_catastro = tp.cedulacatastral
            WHERE tp.ord ='001' AND (p.codigo_catastro like '$search%' OR tp.documento like '$search%'
            OR tp.nombrepropietario like '$search%' OR tp.direccion like '$search%')
            ORDER BY p.id DESC LIMIT $startRows, $perPage";
            $arrData = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
            if(!empty($arrData)){
                $total = count($arrData);
                for ($i=0; $i < $total; $i++) {
                    $codigo = $arrData[$i]['codigo_catastro'];
                    $sql = "SELECT codigo_catastral FROM tesopredios_certificados WHERE codigo_catastral = '$codigo'";
                    $req = mysqli_query($this->linkbd,$sql)->fetch_assoc();
                    $certificado = !empty($req) ? true : false;
                    $arrData[$i]['certificado'] = $certificado;
                }
            }
            $totalRows = mysqli_query($this->linkbd,"SELECT count(*) as total FROM predios p
            INNER JOIN tesopredios tp
            ON p.codigo_catastro = tp.cedulacatastral
            WHERE tp.ord ='001' AND (p.codigo_catastro like '$search%' OR tp.documento like '$search%'
            OR tp.nombrepropietario like '$search%' OR tp.direccion like '$search%')")->fetch_assoc()['total'];
            $totalPages = $totalRows > 0 ? ceil($totalRows/$perPage) : 1;
            $arrResponse = array("status"=>true,"data"=>$arrData,"total"=>$totalRows,"total_pages"=>$totalPages);
            return $arrResponse;
        }
    }
?>
