const URL ='tesoreria/certificado_predial/buscar/teso-certificadoPredial.php';

var app = new Vue({
	el:"#myapp",
	data() {
		return {
			isLoading:false,
			arrData:[],
			txtResults:0,
			intTotalPages:1,
			intPage:1,
			txtSearch:"",
		}
	},
	mounted() {
		this.getData();
	},
	methods: {

		getData: async function(){
			const formData = new FormData();
			formData.append("action","get");
			const response = await fetch(URL,{method:"POST", body:formData});
			const objData = await response.json();
			this.arrData = objData.data;
			this.txtResults = objData.total;
			this.intTotalPages = objData.total_pages;
		},
		search: async function(page=1,option=""){
			if(page <= 0){
				page = 1;
				this.intPage = page;
			}else if(page > this.intTotalPages){
				page = this.intTotalPages;
				this.intPage = page;
			}
			const formData = new FormData();
			formData.append("action","search");
			formData.append("search",this.txtSearch);
			formData.append("page",page);
			this.isLoading=true;

			const response = await fetch(URL,{method:"POST", body:formData});
			const objData = await response.json();
			this.arrData = objData.data;
			this.txtResults = objData.total;
			this.intTotalPages = objData.total_pages;

			this.isLoading=false;
		},
		save: async function(codigo,index){
			const formData = new FormData();
			formData.append("action","save");
			formData.append("codigo",codigo);

			const response = await fetch(URL,{method:"POST",body:formData});
			const objData = await response.json();

			if(objData.status){
                this.arrData[index]['certificado'] = true;
            }
		},
		printPDF:function(codigo){
			window.open("teso-certificadoPredialPdf.php?codigo="+codigo,"_blank");
		},
		showTab:function(tab){
			let tabs = this.$refs.rTabs.children;
			let tabsContent = this.$refs.rTabsContent.children;
			for (let i = 0; i < tabs.length; i++) {
				tabs[i].classList.remove("active");
				tabsContent[i].classList.remove("active")
			}
			tabs[tab-1].classList.add("active");
			tabsContent[tab-1].classList.add("active")
		}
	},
	computed:{

	}
})
