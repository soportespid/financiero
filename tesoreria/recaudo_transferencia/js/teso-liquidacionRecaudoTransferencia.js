const URL ='tesoreria/recaudo_transferencia/controllers/teso-liquidacionRecaudoTransferencia.php';
const URLEXPORT ='tesoreria/recaudo_transferencia/controllers/RecaudoTransferenciaExportController.php';
var app = new Vue({
    el:"#myapp",
    data() {
        return {
            isLoading:false,
            isModalTercero:false,
            isModalIngreso:false,
            isModalFuente:false,
            isModalRev:false,
            isFuente: false,
            txtSearch:"",
            txtSearchTercero:"",
            txtSearchIngreso:"",
            txtSearchFuente:"",
            txtSearchRev:"",
            txtResultados:0,
            txtResultadosTercero:0,
            txtResultadosIngreso:0,
            txtResultadosFuente:0,
            txtResultadosRev:0,
            txtValor:'',
            txtValorSinFormato:'',
            txtTotal:0,
            txtRecaudoTransferencia:"",
            txtConcepto:"",
            selectCentro:"",
            selectSeccion:"",
            selectCausacion:1,
            selectMedioPago: 1,
            selectMovimiento:101,
            objTercero:{nombre:"",codigo:""},
            objIngreso:{nombre:"",codigo:""},
            objFuente:{nombre:"",codigo:""},
            objRev:{nombre:"",codigo:"",valor:""},
            txtConceptoRev:"",
            arrCentros:[],
            arrSecciones:[],
            arrRev:[],
            arrRevCopy:[],
            arrIngresos:[],
            arrIngresosCopy:[],
            arrFuentes:[],
            arrFuentesCopy:[],
            arrTerceros:[],
            arrTercerosCopy:[],
            arrData:[],
            arrSearchData:[],
            txtFecha:new Date().toISOString().split("T")[0],
            txtFechaRev:new Date().toISOString().split("T")[0],
            txtFechaInicial:new Date(new Date().getFullYear(), 0, 1).toISOString().split("T")[0],
            txtFechaFinal:new Date(new Date().getFullYear(), new Date().getMonth() + 1, 0).toISOString().split("T")[0],
            txtConsecutivo: 0,
            txtStatus:"S",
            arrExportData:[]
        }
    },
    mounted() {
        const intPageVal = this.$refs.pageType.value;
        if(intPageVal == 1){
            this.getData();
        }else if(intPageVal == 2){
            this.getEdit();
        }else{
            this.getSearch();
        }
    },
    methods: {
        getData: async function(){
            const formData = new FormData();
            formData.append("action","get");
            this.isLoading = true;
            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();
            this.isLoading = false;
            this.arrTerceros = objData.terceros;
            this.arrTercerosCopy = objData.terceros;
            this.arrIngresos = objData.ingresos;
            this.arrIngresosCopy = objData.ingresos;
            /* this.arrCentros = objData.centros; */
            this.arrSecciones = objData.secciones;
            this.arrRev = objData.liquidaciones;
            this.arrRevCopy = objData.liquidaciones;
            this.txtResultadosRev = this.arrRevCopy.length;
            /* this.selectCentro = this.arrCentros[0].codigo; */
            this.selectSeccion = this.arrSecciones[0].codigo;
            this.txtResultadosTercero = this.arrTercerosCopy.length;
            this.txtResultadosIngreso = this.arrIngresosCopy.length;
        },
        getEdit:async function(){
            const codigo = new URLSearchParams(window.location.search).get('id');
            let formData = new FormData();
            formData.append("action","edit");
            formData.append("codigo",codigo);
            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();
            if(objData.status){
                const recibo = objData.data.recibo;
                this.txtRecaudoTransferencia = recibo.recaudo_transferencia;
                if(recibo.estado == "R"){
                    const reversado = objData.data.reversado;
                    this.txtFechaRev = reversado.fecha;
                    this.txtConceptoRev = reversado.concepto;
                    this.objRev.codigo = reversado.id_recaudo;
                }
                this.arrConsecutivos = Array.from(new Set(objData.consecutivos.map(e=>{return JSON.stringify(e)}))).map(e=>{return JSON.parse(e)});
                this.txtConsecutivo = recibo.id_recaudo;
                this.objTercero = {codigo:recibo.tercero,nombre:recibo.nombre_tercero};
                this.txtFecha = recibo.fecha;
                this.txtConcepto = recibo.concepto;
                this.txtStatus = recibo.estado;
                this.selectMedioPago = recibo.medio_pago == 1 ? 'CSF' : 'SSF';
                this.arrExportData = recibo;
                const arrDet = recibo.det;
                arrDet.forEach(e => {
                    this.arrData.push({
                        ingreso:{codigo:e.codigo_ingreso,nombre:e.nombre_ingreso},
                        fuente:{codigo:e.codigo_fuente,nombre:e.nombre_fuente},
                        seccion:{codigo:e.codigo_seccion,nombre:e.nombre_seccion},
                        valor:e.valor
                    });
                });
                this.getTotal();

            }else{
                window.location.href='teso-liquidacionRecaudoTransferenciaVer.php?id='+objData.consecutivo;
            }
        },
        getSearch:async function(){
            const formData = new FormData();
            formData.append("action","gen");
            formData.append("fecha_inicial",this.txtFechaInicial);
            formData.append("fecha_final",this.txtFechaFinal);
            formData.append("search",this.txtSearch);
            this.isLoading = true;
            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();
            this.arrSearchData = objData;
            this.txtResultados = this.arrSearchData.length;
            this.isLoading = false;
        },
        editItem:function(id){
            window.location.href='teso-liquidacionRecaudoTransferenciaVer.php?id='+id;
        },
        search:function(type=""){
            let search = "";
            if(type == "modal_tercero")search = this.txtSearchTercero.toLowerCase();
            if(type == "modal_ingreso")search = this.txtSearchIngreso.toLowerCase();
            if(type == "modal_fuente")search = this.txtSearchFuente.toLowerCase();
            if(type == "modal_rev")search = this.txtSearchRev.toLowerCase();
            if(type=="cod_tercero")search = this.objTercero.codigo;
            if(type=="cod_ingreso")search = this.objIngreso.codigo;
            if(type=="cod_fuente")search = this.objFuente.codigo;
            if(type=="cod_rev")search = this.objRev.codigo;

            if(type=="modal_tercero"){
                this.arrTercerosCopy = [...this.arrTerceros.filter(e=>e.codigo.toLowerCase().includes(search) || e.nombre.toLowerCase().includes(search))];
                this.txtResultadosTercero = this.arrTercerosCopy.length;
            }else if(type=="modal_ingreso"){
                this.arrIngresosCopy = [...this.arrIngresos.filter(e=>e.codigo.toLowerCase().includes(search) || e.nombre.toLowerCase().includes(search))];
                this.txtResultadosTercero = this.arrIngresosCopy.length;
            }else if(type=="modal_fuente"){
                this.arrFuentesCopy = [...this.arrFuentes.filter(e=>e.codigo.toLowerCase().includes(search) || e.nombre.toLowerCase().includes(search))];
                this.txtResultadosFuente = this.arrFuentesCopy.length;
            }else if(type=="modal_rev"){
                this.arrRevCopy = [...this.arrRev.filter(e=>e.codigo.toLowerCase().includes(search) || e.concepto.toLowerCase().includes(search))];
                this.txtResultadosRev = this.arrRevCopy.length;
            }else if(type=="cod_tercero"){
                const data = [...this.arrTerceros.filter(e=>e.codigo == search)];
                this.objTercero = data.length > 0 ? JSON.parse(JSON.stringify(data[0])) : {codigo:"",nombre:""};
            }else if(type=="cod_ingreso"){
                const data = [...this.arrIngresos.filter(e=>e.codigo == search)];
                this.objIngreso = data.length > 0 ? JSON.parse(JSON.stringify(data[0])) : {codigo:"",nombre:""};
                this.objFuente = {codigo:"",nombre:""};
                if(this.objIngreso.codigo != ""){
                    if(!this.objIngreso.is_status){
                        this.objIngreso = {codigo:"",nombre:""}
                        Swal.fire("Atención!","Este código no está parametrizado, pruebe con otro.","warning");
                    }
                    this.arrFuentes = [];
                    this.arrFuentesCopy = [];
                    this.txtResultadosFuente =0;
                    this.isFuente = false;
                    if(app.objIngreso.fuentes.length > 0){
                        this.arrFuentes = this.objIngreso.fuentes;
                        this.arrFuentesCopy = this.objIngreso.fuentes;
                        this.txtResultadosFuente = this.objIngreso.fuentes.length;
                        this.isFuente = true;
                    }

                }
            }else if(type=="cod_fuente"){
                const data = [...this.arrFuentes.filter(e=>e.codigo == search)];
                this.objFuente = data.length > 0 ? JSON.parse(JSON.stringify(data[0])) : {codigo:"",nombre:""};
            }else if(type=="cod_rev"){
                const data = [...this.arrRev.filter(e=>e.codigo == search)];
                this.objRev = data.length > 0 ? JSON.parse(JSON.stringify(data[0])) : {codigo:"",concepto:"",valor:""};
            }
        },
        formatNum: function(valor){
            return new Intl.NumberFormat("en-US", {style: "currency", currency: "USD"}).format(valor);
        },
        selectItem:function ({...data},type="tercero"){
            if(type =="tercero"){
                this.objTercero = data;
                this.isModalTercero = false;
            }else if(type =="ingreso"){
                if(!data.is_status){
                    Swal.fire("Atención!","Este código no está parametrizado, pruebe con otro.","warning");
                    return false;
                }
                this.objFuente = {codigo:"",nombre:""};
                this.arrFuentes = [];
                this.arrFuentesCopy = [];
                this.txtResultadosFuente =0;
                this.isFuente = false;
                if(data.fuentes.length > 0){
                    this.arrFuentes = data.fuentes;
                    this.arrFuentesCopy = data.fuentes;
                    this.txtResultadosFuente =data.fuentes.length;
                    if(data.fuentes.length === 1)this.objFuente = data.fuentes[0];
                    this.isFuente = true;
                }
                this.objIngreso = data;
                this.isModalIngreso = false;

            }else if(type =="fuente"){
                this.objFuente = data;
                this.isModalFuente = false;
            }else if(type =="rev"){
                this.objRev = data;
                this.isModalRev = false;
            }
        },
        save:async function(){
            if(this.selectMovimiento == "101"){
                if(this.objTercero.codigo == ""){
                    Swal.fire("Atencion!","Debe seleccionar el tercero.","warning");
                    return false;
                }
                if(this.txtConcepto == ""){
                    Swal.fire("Atencion!","Debe escribir el concepto.","warning");
                    return false;
                }
                if(app.arrData.length == 0){
                    Swal.fire("Atencion!","Debe agregar al menos un código de ingreso.","warning");
                    return false;
                }
            }else{
                if(this.objRev.codigo ==""){
                    Swal.fire("Atención!","Debe elegir el ingreso a reversar","warning");
                    return false;
                }
                if(this.txtConceptoRev ==""){
                    Swal.fire("Atención!","La descripción no puede estar vacio","warning");
                    return false;
                }
            }
            Swal.fire({
                title:"¿Estás segur@ de guardar?",
                text:"",
                icon: 'warning',
                showCancelButton:true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText:"Sí, guardar",
                cancelButtonText:"No, cancelar"
            }).then(async function(result){
                if(result.isConfirmed){
                    const formData = new FormData();
                    formData.append("data",JSON.stringify(app.arrData));
                    formData.append("fecha",app.txtFecha);
                    formData.append("concepto",app.txtConcepto);
                    formData.append("total",app.txtTotal);
                    formData.append("causacion",app.selectCausacion);
                    formData.append("tercero",app.objTercero.codigo);
                    formData.append("id_rev",app.objRev.codigo);
                    formData.append("mov",app.selectMovimiento);
                    formData.append("concepto_rev",app.txtConceptoRev);
                    formData.append("fecha_rev",app.txtFechaRev);
                    formData.append("mediopago", app.selectMedioPago)
                    formData.append("action","save");
                    const response = await fetch(URL,{method:"POST",body:formData});
                    const objData = await response.json();
                    if(objData.status){
                        Swal.fire("Guardado",objData.msg,"success");
                        if(objData.id){
                            setTimeout(function(){
                                window.location.href='teso-liquidacionRecaudoTransferenciaVer?id='+objData.id;
                            },1500);
                        }
                    }else{
                        Swal.fire("Error",objData.msg,"error");
                    }
                }
            });
        },
        add:function(){

            if(this.txtValorSinFormato <= 0 || this.txtValorSinFormato ==""){
                Swal.fire("Atención!","Debe agregar un valor mayor a cero.","warning");
                return false;
            }
            if(this.objIngreso.codigo == ""){
                Swal.fire("Atención!","Debe asignar el código de ingreso","warning");
                return false;
            }
            if(app.arrFuentes.length > 0 && app.objFuente.codigo ==""){
                Swal.fire("Atención!","Debe seleccionar la fuente","warning");
                return false;
            }

            if(app.arrData.length>0){
                let flag = true;
                for (let i = 0; i < app.arrData.length; i++) {
                    const e = app.arrData[i];

                    if(e.fuente.codigo != "" && e.fuente.codigo == app.objFuente.codigo &&
                        e.ingreso.codigo == app.objIngreso.codigo && e.seccion.codigo == app.selectSeccion
                    ){
                        flag = false;
                        break;
                    }
                    if(e.fuente.codigo == "" && e.ingreso.codigo == app.objIngreso.codigo && e.seccion.codigo == app.selectSeccion){
                        flag = false;
                        break;
                    }
                }
                if(flag){
                    this.arrData.push({
                        ingreso:JSON.parse(JSON.stringify(app.objIngreso)),
                        fuente:JSON.parse(JSON.stringify(this.objFuente)),
                        seccion:app.arrSecciones.filter(e=>e.codigo == app.selectSeccion)[0],
                        valor:app.txtValorSinFormato
                    })
                    app.objIngreso = {nombre:"",codigo:""}
                    this.objFuente = {codigo:"",nombre:""}
                    app.txtValor = ''
                    app.txtValorSinFormato = ''
                }else{
                    Swal.fire("Atención!","Este detalle ya fue agregado, pruebe con otro.","warning");
                    return false;
                }
            }else{
                this.arrData.push({
                    ingreso:JSON.parse(JSON.stringify(this.objIngreso)),
                    fuente:JSON.parse(JSON.stringify(this.objFuente)),
                    seccion:app.arrSecciones.filter(e=>e.codigo == app.selectSeccion)[0],
                    valor:app.txtValorSinFormato
                })
                app.objIngreso = {nombre:"",codigo:""}
                this.objFuente = {codigo:"",nombre:""}
                app.txtValor = ''
                app.txtValorSinFormato = ''
            }
            this.getTotal();
        },
        del:function(index){
            this.arrData.splice(index,1);
            this.getTotal();
        },
        getTotal:function(){
            this.txtTotal = 0;
            this.arrData.forEach(e => {
                this.txtTotal+=parseFloat(e.valor);
            });
        },
        exportData:function(){
            const form = document.createElement("form");
            form.method ="post";
            form.target="_blank";
            form.action=URLEXPORT;

            function addField(name,value){
                const input = document.createElement("input");
                input.type="hidden";
                input.name=name;
                input.value = value;
                form.appendChild(input);
            }
            addField("action","pdf");
            addField("data",JSON.stringify(this.arrExportData));
            document.body.appendChild(form);
            form.submit();
            document.body.removeChild(form);
        },
        nextItem:function(type){
            let vueContext = this;
            let id = this.txtConsecutivo
            let index = this.arrConsecutivos.findIndex(function(e){return e.id == id});
            if(type=="next" && vueContext.arrConsecutivos[++index]){
                id = this.arrConsecutivos[index++].id;
            }else if(type=="prev" && vueContext.arrConsecutivos[--index]){
                id = this.arrConsecutivos[index--].id;
            }
            window.location.href='teso-liquidacionRecaudoTransferenciaVer?id='+id;
        },

        /* Formatos para mostrar información */
        formatInputNumber(conFormato, sinFormato) {
            this[conFormato] = this.formatNumber(this[conFormato]);
            this[sinFormato] = this[conFormato].replace(/[^0-9.]/g, "");
        },

        formatNumber(number) {
            if (!number) return ""; // Si no hay entrada, retorna vacío

            // Elimina caracteres no válidos (excepto números y el punto decimal)
            number = number.replace(/[^0-9.]/g, "");

            // Divide la parte entera y decimal
            const [integerPart, decimalPart] = number.split(".");

            // Formatear la parte entera con separador de miles
            const formattedInteger = integerPart.replace(/\B(?=(\d{3})+(?!\d))/g, ",");

            // Si hay parte decimal, la une; si no, devuelve solo la parte entera
            return decimalPart !== undefined
                ? `${formattedInteger}.${decimalPart.slice(0, 2)}` // Limita a 2 decimales si es necesario
                : formattedInteger;
        },
    },
})
