<?php
    require_once '../../../Librerias/core/Helpers.php';
    require_once '../../../Librerias/core/Mysql.php';
    session_start();
    /*ini_set('display_errors', '1');
    ini_set('display_startup_errors', '1');
    error_reporting(E_ALL);*/
    class recaudoTransferenciaModel extends Mysql{
        private $intConsecutivo;
        private $intIdComp;
        private $strTipo;
        private $strCuentaBanco;
        private $terceroBanco;
        private $strTercero;
        private $strConcepto;
        private $floatTotal;
        private $strFecha;
        private $strFechaFinal;
        private $isSsf;

        private $strTerceroContribuyente;

        function __construct(){
            parent::__construct();
        }

        public function selectCentroCostos(){
            $sql = "SELECT id_cc as codigo, nombre FROM centrocosto WHERE estado='S'";
            $request = $this->select_all($sql);
            return $request;
        }
        public function selectRecaudo(int $intConsecutivo){
            $this->intConsecutivo = $intConsecutivo;
            $sql = "SELECT * FROM tesosinrecaudos WHERE id_recaudo = $this->intConsecutivo";
            $request = $this->select($sql);
            return $request;
        }
        public function selectIngresoRecaudo(int $intRecaudo,string $strEstado, string $strMov,$intConsecutivo = ""){
            $condicion = "";
            $this->intConsecutivo = $intRecaudo;
            if($this->intConsecutivo !=""){
                $condicion =" AND idcomp = $this->intConsecutivo";
            }
            $sql = "SELECT * FROM tesorecaudotransferencia
            WHERE tipo_mov = '$strMov' AND estado = '$strEstado' $condicion";
            $request = $this->select($sql);
            return $request;
        }
        public function selectLiquidaciones(){
            $sql = "SELECT TB1.*,DATE_FORMAT(TB1.fecha,'%d/%m/%Y') as fecha,TB1.id_recaudo as codigo
            FROM tesorecaudotransferencialiquidar AS TB1
            LEFT JOIN tesorecaudotransferencia AS TB2 ON TB1.id_recaudo = TB2.idcomp AND TB2.estado = 'S'
            WHERE TB1.estado = 'S'
            AND TB2.idcomp IS NULL
            ORDER BY TB1.id_recaudo DESC";
            $request = $this->select_all($sql);
            if(!empty($request)){
                $total = count($request);
                for ($i=0; $i < $total; $i++) {
                    $id = $request[$i]['codigo'];
                    $request[$i]['nombre'] = getNombreTercero($request[$i]['tercero']);
                    $request[$i]['total_format'] = formatNum($request[$i]['valortotal']);
                    $sqlDet = "SELECT
                        det.ingreso as codigo_ingreso,
                        det.valor,
                        det.estado,
                        det.cc as codigo_seccion,
                        COALESCE(seccion.nombre,'') as nombre_seccion,
                        det.fuente as codigo_fuente,
                        COALESCE(f.nombre,'') as nombre_fuente,
                        COALESCE(i.nombre,'') as nombre_ingreso
                        FROM tesorecaudotransferencialiquidar_det det
                        LEFT JOIN pptoseccion_presupuestal seccion ON det.cc = seccion.id_seccion_presupuestal
                        LEFT JOIN ccpet_fuentes_cuipo f ON f.codigo_fuente = det.fuente
                        INNER JOIN tesoingresos i ON det.ingreso = i.codigo
                        WHERE id_recaudo = $id";
                    $request[$i]['det'] = $this->select_all($sqlDet);
                }
            }
            return $request;
        }
        public function selectIngresosReversar(){
            $sql = "SELECT * FROM tesorecaudotransferencia WHERE estado = 'S' ORDER BY id_recaudo DESC";
            $request = $this->select_all($sql);
            return $request;
        }
        public function insertComprobanteCab(string $strFecha,string $strConcepto,float $floatTotal,int $intConsecutivo, $comp = 14, $estado = 1){
            $this->strFecha = $strFecha;
            $this->strConcepto = $strConcepto;
            $this->floatTotal = $floatTotal;
            $this->intConsecutivo = $intConsecutivo;
            $sql="INSERT INTO comprobante_cab (numerotipo,tipo_comp,fecha,concepto,total,total_debito,total_credito,diferencia,estado)
            VALUES (?,?,?,?,?,?,?,?,?)";
            $arrValues = [$this->intConsecutivo,$comp,$this->strFecha,$this->strConcepto,0,$this->floatTotal,$this->floatTotal,0,$estado];
            $request = $this->insert($sql,$arrValues);
            return $request;
        }
        
        public function insertReciboCab(int $intIdComp,string $strFecha,float $floatTotal, string $strTipo,array $arrData,array $arrBanco, int $id, int $isSsf){
            insertAuditoria("teso_auditoria","teso_funciones_id",9,"Crear",$id, "Recaudo Transferencia");
            $medio_pago = 1;
            if($isSsf == 1){
                $this->strCuentaBanco = "";
                $this->terceroBanco = "";
                $medio_pago = $arrBanco['medio_pago'];
                
            }else{
                $this->strCuentaBanco = $strTipo == "banco" ? $arrBanco['cuenta_banco'] : "";
                $this->terceroBanco = $strTipo == "banco" ? $arrBanco['tercero'] : "";
            }
            $arrFecha = explode("-",$strFecha);
            $this->strFecha = $strFecha;
            $this->floatTotal = $floatTotal;
            $this->intIdComp = $arrData['id_recaudo'];
            $this->intConsecutivo = $id;
            $this->strTipo = $strTipo;
            $this->strConcepto = $arrData['concepto'];
            $this->strTerceroContribuyente = $arrData['tercero'];
            $sql= "INSERT INTO tesorecaudotransferencia (id_recaudo, idcomp, fecha, vigencia, banco, ncuentaban, concepto, tercero, valortotal, estado, presupuesto, mediopago) VALUES (?,?,?,?,?,?,?,?,?,?,?,?)";
            $arrValues = [$this->intConsecutivo, $this->intIdComp, $this->strFecha, $arrFecha[0], $this->terceroBanco, $this->strCuentaBanco, $this->strConcepto, $this->strTerceroContribuyente, $this->floatTotal, "S", 1, $medio_pago];
            /* $arrValues = [$this->intIdComp,$this->strFecha,$arrFecha[0],$this->intConsecutivo,$this->strTipo,$this->strCuentaCaja,$this->strCuentaBanco,$this->floatTotal,"S","3", $medio_pago]; */
            $request = $this->insert($sql,$arrValues);
            return $request;
        }
        public function insertReciboDet(int $intConsecutivo,string $strTercero,string $strFecha, array $arrData,array $arrBanco,string $strTipo, int $isSsf){
            
            $arrFecha = explode("-",$strFecha);
            $this->isSsf = $isSsf == 1 ? 'SSF' : 'CSF';
            $this->strCuentaBanco = $arrBanco['cuenta'];
            //$this->terceroBanco = $strTipo == "banco" ? $arrBanco['tercero'] : "";
            $this->strTercero = $strTercero;
            $this->strFecha = $strFecha;
            $this->intConsecutivo = $intConsecutivo;
            $return ="";
            foreach ($arrData as $data) {
                
                $flagPtto = true;
                $flagComp = true;
                $arrIngreso = getIngresosDet($data['codigo_ingreso']);
                foreach ($arrIngreso as $ingreso) {
                    
                    $centroCosto = getCentroCosto($data['codigo_seccion']);
                    $arrConcepto = getConceptoDet($ingreso['concepto'],4,"C",$centroCosto['id_cc']);
                    $intPorcentaje = $ingreso['porcentaje'];
                    $valorPtto = $data['valor']*($intPorcentaje/100);
                    if($ingreso['cuentapres'] !="" && $valorPtto > 0){
                        $sqlPtto = "INSERT INTO pptoingtranppto (cuenta, idrecibo, valor, vigencia, fuente, productoservicio, seccion_presupuestal, medio_pago, vigencia_gasto)
                        VALUES(?,?,?,?,?,?,?,?,?)";
                        $arrValuesPtto = [
                            $ingreso['cuentapres'],
                            $this->intConsecutivo,
                            $valorPtto,
                            $arrFecha[0],
                            $data['codigo_fuente'],
                            $ingreso['cuenta_clasificadora'],
                            $data['codigo_seccion'],
                            $this->isSsf,
                            1
                        ];
                        $resp = $this->insert($sqlPtto,$arrValuesPtto);
                        if($resp> 0){
                            $flagPtto = true;
                        }else{
                            $flagPtto = false;
                            break;
                        }
                    }
                    foreach($arrConcepto as $concepto){
                        if($concepto['debito'] == "S" && $concepto['tipocuenta'] == "N"){
                            $debito = 0;
                            $credito=$data['valor']*($intPorcentaje/100);
                            $sql="INSERT INTO comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,valdebito,valcredito,estado,vigencia)
                            VALUES (?,?,?,?,?,?,?,?,?)";
                            $arrValuesDet = [
                                '14 '.$this->intConsecutivo,
                                $concepto['cuenta'],
                                $this->strTercero,
                                $centroCosto['id_cc'],
                                'Ingreso '.strtoupper($data['nombre_ingreso']),
                                $debito,
                                $credito,
                                1,
                                $arrFecha[0]
                            ];
                            $respComp = $this->insert($sql,$arrValuesDet);
                            if($respComp > 0 ){
                                $flagComp = true;
                            }else{
                                $flagComp = false;
                                break;
                            }
                            $debito = $credito;
                            $credito = 0;
                            $sql="INSERT INTO comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,valdebito,valcredito,estado,vigencia)
                            VALUES (?,?,?,?,?,?,?,?,?)";
                            $arrValuesDet = [
                                '14 '.$this->intConsecutivo,
                                $this->strCuentaBanco,
                                $this->strTercero,
                                $centroCosto['id_cc'],
                                'Ingreso '.strtoupper($data['nombre_ingreso']),
                                $debito,
                                $credito,
                                1,
                                $arrFecha[0]
                            ];
                            $respComp = $this->insert($sql,$arrValuesDet);
                            if($respComp > 0 ){
                                $flagComp = true;
                            }else{
                                $flagComp = false;
                                break;
                            }
                        }
                    }
                }
                if(!$flagComp){
                    $return = "comp";
                    break;
                }
                if(!$flagPtto){
                    $return = "ptto";
                    break;
                }
                $sql="INSERT into tesorecaudotransferencia_det (id_recaudo,ingreso,valor,estado) VALUES(?,?,?,?)";
                $arrValues = [$this->intConsecutivo,$data['codigo_ingreso'],$data['valor'],"S"];
                $respDet = $this->insert($sql,$arrValues);
                if($respDet == 0){
                    $return = "det";
                    break;
                }else{
                    $return = $respDet;
                }
            }
            return $return;
        }
        public function insertReversar(int $intConsecutivo, string $strFecha, string $strConcepto,array $data){
            $return ="";
            $this->intConsecutivo = $intConsecutivo;
            $this->strFecha = $strFecha;
            $this->strConcepto = $strConcepto;
            $this->intIdComp = $this->insertComprobanteCab(
                $this->strFecha,
                $this->strConcepto,
                $data['recibo']['cab']['valortotal'],
                $this->intConsecutivo,
                2014,
                1
            );
            if($this->intIdComp > 0){
                $request = $this->update("UPDATE tesorecaudotransferencia SET estado=? WHERE id_recaudo='{$data['recibo']['cab']['id_recaudo']}'",['R']);
                if($request == 0){
                    $return = "recibo_cab";
                    return $return;
                }
                /* $request = $this->update("UPDATE tesorecaudotransferencialiquidar SET estado=? WHERE id_recaudo='{$data['recibo']['cab']['idcomp']}'",['S']);
                if($request == 0){
                    $return = "recaudo";
                    return $return;
                } */
                $request = $this->delete("DELETE FROM pptoingtranppto WHERE idrecibo = '{$data['recibo']['cab']['id_recaudo']}'");
                if($request == 0){
                    $return = "ptto";
                    return $return;
                }

                $sqlInsert = "INSERT INTO tesorecaudotransferencia (id_recaudo, idcomp, fecha, vigencia, banco, ncuentaban, concepto, tercero, valortotal, estado, presupuesto, mediopago, tipo_mov) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)";
                $arrFecha = explode("-",$this->strFecha);
                $arrValues = [
                    $data['recibo']['cab']['id_recaudo'],
                    $data['recibo']['cab']['idcomp'],
                    $this->strFecha,
                    $arrFecha[0],
                    $data['recibo']['cab']['banco'],
                    $data['recibo']['cab']['ncuentaban'],
                    $this->strConcepto,
                    $data['recibo']['cab']['tercero'],
                    $data['recibo']['cab']['valortotal'],
                    "R",
                    $data['recibo']['cab']['presupuesto'],
                    $data['recibo']['cab']['mediopago'],
                    "301"
                ];
                
                $requestInsert = $this->insert($sqlInsert,$arrValues);
                if($requestInsert == 0){
                    $return = "recibo";
                    return $return;
                }else{
                    insertAuditoria("teso_auditoria","teso_funciones_id",9,"Reversar",$requestInsert, "Recaudo Transferencia", "teso_funciones");
                    $arrDetComp = $data['comprobante']['det'];
                    foreach ($arrDetComp as $det) {
                        $debito = $det['valcredito'];
                        $credito = $det['valdebito'];
                        $numeroTipo = $det['numerotipo'];
                        $tipoComp = "20".$det['tipo_comp'];
                        $idComp = $tipoComp." ".$numeroTipo;
                        $detalle = "Reversion ".$det['detalle'];
                        $sqlInsertDet = "INSERT INTO comprobante_det(id_comp,cuenta,tercero,centrocosto,detalle,valdebito,valcredito,estado,vigencia,tipo_comp,numerotipo)
                        VALUES(?,?,?,?,?,?,?,?,?,?,?)";
                        $arrValues = [
                            $idComp,
                            $det['cuenta'],
                            $det['tercero'],
                            $det['centrocosto'],
                            $detalle,
                            $debito,
                            $credito,
                            1,
                            $arrFecha[0],
                            $tipoComp,
                            $numeroTipo
                        ];
                        $requestInsertDet = $this->insert($sqlInsertDet,$arrValues);
                        if($requestInsertDet == 0){
                            $return = "comp_det";
                            return $return;
                        }
                    }
                    $return = $requestInsert;
                }
            }else{
                $return = "comp";
            }
            return $return;
        }
        
        public function selectIngreso(int $intConsecutivo){
            $this->intConsecutivo = $intConsecutivo;
            $sql = "SELECT * FROM tesorecaudotransferencia WHERE id_recaudo = $this->intConsecutivo";
            $request = $this->select($sql);
            $arrRecibo = [];
            if(!empty($request)){
                $sqlCompCab = "SELECT * FROM comprobante_cab WHERE numerotipo = $this->intConsecutivo AND tipo_comp = 14";
                $arrCompCab = $this->select($sqlCompCab);
                $sqlCompDet = "SELECT * FROM comprobante_det WHERE numerotipo = $this->intConsecutivo AND tipo_comp = 14";
                $arrCompDet = $this->select_all($sqlCompDet);
                $sqlDet = "SELECT * FROM tesorecaudotransferencia_det WHERE id_recaudo = $this->intConsecutivo";
                $arrRecibo['comprobante']['cab'] = $arrCompCab;
                $arrRecibo['comprobante']['det'] = $arrCompDet;
                $arrRecibo['recibo']['cab'] = $request;
                $arrRecibo['recibo']['det'] = $this->select_all($sqlDet);
            }
            return $arrRecibo;
        }
        public function selectSearch(string $strFecha, string $strFechaFinal,string $search){
            $this->strFecha = $strFecha;
            $this->strFechaFinal = $strFechaFinal;
            $sql = "SELECT tr.id_recaudo,
            tr.idcomp,
            DATE_FORMAT(tr.fecha,'%d/%m/%Y') as fecha,
            tsr.concepto,
            tr.valortotal,
            tsr.tercero,
            tr.estado
            FROM tesorecaudotransferencia tr
            INNER JOIN tesorecaudotransferencialiquidar tsr ON tsr.id_recaudo = tr.idcomp
            WHERE tr.fecha BETWEEN '$this->strFecha' AND '$this->strFechaFinal'
            AND (tsr.concepto like '$search%' OR tr.id_recaudo like '$search%') AND tr.tipo_mov='101'
            ORDER BY tr.id_recaudo DESC";
            $request = $this->select_all($sql);
            if(!empty($request)){
                $total = count($request);
                for ($i=0; $i < $total; $i++) {
                    $request[$i]['nombre'] = getNombreTercero($request[$i]['tercero']);
                    $request[$i]['total_format'] = formatNum($request[$i]['valortotal']);
                }
            }
            return $request;
        }
        public function selectEdit(int $intConsecutivo){
            $this->intConsecutivo = $intConsecutivo;
            $sql = "SELECT tr.id_recaudo,
            tr.idcomp,
            tr.fecha,
            tr.concepto,
            tr.valortotal,
            tsr.tercero,
            tr.estado,
            tr.tipo_mov,
            tr.banco,
            tr.ncuentaban,
            tr.mediopago
            FROM tesorecaudotransferencia tr
            INNER JOIN tesorecaudotransferencialiquidar tsr ON tsr.id_recaudo = tr.idcomp
            WHERE tr.id_recaudo = $this->intConsecutivo";
            $request = $this->select_all($sql);
            if(!empty($request)){
                $total = count($request);
                for ($i=0; $i < $total; $i++) {
                    $id = $request[$i]['idcomp'];
                    $idRecibo = $request[$i]['id_recaudo'];
                    $request[$i]['nombre'] = getNombreTercero($request[$i]['tercero']);
                    $request[$i]['total_format'] = formatNum($request[$i]['valor']);
                    
                    $sqlDet = "SELECT
                        det.ingreso as codigo_ingreso,
                        det.valor,
                        det.estado,
                        det.cc as codigo_seccion,
                        COALESCE(seccion.nombre,'') as nombre_seccion,
                        det.fuente as codigo_fuente,
                        COALESCE(f.nombre,'') as nombre_fuente,
                        COALESCE(i.nombre,'') as nombre_ingreso
                        FROM tesorecaudotransferencialiquidar_det det
                        LEFT JOIN pptoseccion_presupuestal seccion ON det.cc = seccion.id_seccion_presupuestal
                        LEFT JOIN ccpet_fuentes_cuipo f ON f.codigo_fuente = det.fuente
                        INNER JOIN tesoingresos i ON det.ingreso = i.codigo
                        WHERE id_recaudo = $id";
                    $sqlPres = "SELECT ptto.cuenta,ccpet.nombre,ptto.valor, ptto.fuente
                    FROM pptoingtranppto ptto
                    INNER JOIN cuentasingresosccpet ccpet ON ccpet.codigo = ptto.cuenta
                    WHERE ptto.idrecibo = $idRecibo";
                    $request[$i]['det'] = $this->select_all($sqlDet);
                    $request[$i]['pres'] = $this->select_all($sqlPres);
                }
            }
            return $request;
        }
    }
?>
