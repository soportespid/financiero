const URL ='tesoreria/historico_predial/buscar/teso-historicoBuscar.php';

var app = new Vue({
    el:"#myapp",
    data() {
        return {
            isLoading:false,
            arrData:[],
            txtResults:0,
            intTotalPages:1,
            intPage:1,
            txtSearch:"",
            txtCodigo:"",
            txtDocumento:"",
            txtNombre:"",
            txtArea:"",
            txtConstruida:"",
            txtDireccion:"",
            txtTotal:"",
            txtOrden:"",
            txtAvaluo:"",
            txtTipo:"",
            txtDestino:"",
            txtVigencia:"",
            arrPropietarios:[],
        }
    },
    mounted() {

    },
    methods: {
        search: async function(){
            const formData = new FormData();
            formData.append("action","search");
            formData.append("search",this.txtSearch);
            this.isLoading=true;

            const response = await fetch(URL,{method:"POST", body:formData});
            const objData = await response.json();
            if(objData.status){
                let data = objData.data;
                this.txtCodigo = data.codigo_catastro;
                this.txtDocumento = data.documento;
                this.txtNombre = data.nombrepropietario;
                this.txtConstruida = data.areacon+" m²";
                this.txtArea = data.ha+" ha"+" "+data.met2+" m²";
                this.txtDireccion = data.direccion;
                this.txtTotal = data.tot;
                this.txtOrden = data.ord;
                this.txtAvaluo = this.formatNumero(data.avaluo);
                this.txtTipo = data.tipopredio.toUpperCase();
                this.txtDestino = data.destino;
                this.txtVigencia = data.vigencia;
                this.arrData = objData.detalle;
                this.arrPropietarios = objData.propietarios;
            }else{
                this.txtCodigo = "";
                this.txtDocumento = "";
                this.txtNombre = "";
                this.txtConstruida = "";
                this.txtArea = "";
                this.txtDireccion = "";
                this.txtTotal = "";
                this.txtOrden = "";
                this.txtAvaluo = "";
                this.txtTipo = "";
                this.txtDestino = "";
                this.txtVigencia = "";
                this.arrData = [];
            }
            this.isLoading=false;
            console.log(this.arrData);
        },
        printPDF:function(){
            if(this.arrData == 0){
                Swal.fire("Error","El predio no tiene histórico, pruebe con otro.","error");
                return false;
            }
            window.open("teso-historicoPredialPdf.php?search="+this.txtSearch,"_blank");
        },
        formatNumero: function(valor){
            return new Intl.NumberFormat("en-US", {style: "currency", currency: "USD"}).format(valor);
        },
        showTab:function(tab){
            let tabs = this.$refs.rTabs.children;
            let tabsContent = this.$refs.rTabsContent.children;
            for (let i = 0; i < tabs.length; i++) {
                tabs[i].classList.remove("active");
                tabsContent[i].classList.remove("active")
            }
            tabs[tab-1].classList.add("active");
            tabsContent[tab-1].classList.add("active")
        }
    },
    computed:{

    }
})
