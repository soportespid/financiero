<?php
    require_once '../../../comun.inc';
    require '../../../funciones.inc';
    require '../../../funcionesSP.inc.php';
    /*ini_set('display_errors', '1');
    ini_set('display_startup_errors', '1');
    error_reporting(E_ALL);*/
    session_start();
    //dep($_POST);exit;
    if($_POST){
        $obj = new Plantilla();
        if($_POST['action']=="search"){
            $obj->search($_POST['search']);
        }
    }

    class Plantilla{
        private $linkbd;
        private $arrData;
        public function __construct() {
            $this->linkbd = conectar_v7();
            $this->linkbd->set_charset("utf8");
        }
        public function search(string $search){
            if(!empty($_SESSION)){
                $request = $this->selectData($search);
                if(!empty($request)){
                    $detalle = $this->selectDetalle($request['codigo_catastro']);
                    $propietarios = $this->selectPropietarios($request['id'],$request['created_at']);
                    $arrResponse = array("status"=>true,"data"=>$request,"detalle"=>$detalle,"propietarios"=>$propietarios);
                }else{
                    $arrResponse = array("status"=>false);
                }
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
            die();
        }
        public function selectData($search){
            $sql="SELECT
            p.id,p.codigo_catastro,
            p.main_propietario as ord,p.total as tot,
            pp.documento,
            pp.nombre_propietario as nombrepropietario,
            pa.valor_avaluo as avaluo,pa.tasa_por_mil as tasa,
            pa.vigencia
            FROM predios p
            LEFT JOIN predio_propietarios pp ON p.id = pp.predio_id AND p.main_propietario = pp.orden
            LEFT JOIN (
            	SELECT * FROM predio_avaluos
                WHERE (vigencia) IN (
                    SELECT MAX(vigencia) FROM predio_avaluos
                    )
            ) pa ON p.id = pa.predio_id
            WHERE (p.codigo_catastro like '$search%' OR pp.documento like '$search%'
            OR pp.nombre_propietario like '$search%') GROUP BY p.id;";
            $request = mysqli_query($this->linkbd,$sql)->fetch_assoc();

            $sql_info = "SELECT
            tp.direccion,tp.hectareas as ha,
            tp.metros_cuadrados as met2,
            tp.area_construida as areacon,
            tp.created_at,
            CONCAT(de.nombre,' ( Código: ',cd.codigo,')') as destino,
            CONCAT(pt.nombre,' (Código: ',pt.codigo,')') as tipopredio
            FROM predio_informacions tp
            LEFT JOIN predio_tipos pt ON tp.predio_tipo_id = pt.id
            LEFT JOIN codigo_destino_economicos cd ON cd.id = tp.codigo_destino_economico_id
            LEFT JOIN destino_economicos de ON de.id = cd.destino_economico_id
            WHERE tp.predio_id = '$request[id]' AND tp.created_at = (SELECT MAX(created_at) FROM predio_informacions WHERE predio_id = '$request[id]')
            GROUP BY tp.predio_id";
            $request_info = mysqli_query($this->linkbd,$sql_info)->fetch_assoc();

            $request['direccion'] = $request_info['direccion'];
            $request['ha'] = $request_info['ha'];
            $request['met2'] = $request_info['met2'];
            $request['areacon'] = $request_info['areacon'];
            $request['created_at'] = $request_info['created_at'];
            $request['destino'] = $request_info['destino'];
            $request['tipopredio'] = $request_info['tipopredio'];
            return $request;
        }
        public function selectPropietarios($id,$fecha){
            $sql = "SELECT nombre_propietario as nombre, documento, orden
            FROM predio_propietarios WHERE predio_id = $id AND created_at = '$fecha' ORDER BY orden ASC";
            $request = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
            return $request;
        }
        public function selectDetalle($cedula){
            $sql = "SELECT
            t.idpredial,
            t.estado,
            td.vigliquidada,
            DATE_FORMAT(t.fecha,'%d/%m/%Y') as fechali,
            td.avaluo,
            td.tasav,
            td.predial,
            td.intpredial,
            td.bomberil,
            td.intbomb,
            td.medioambiente,
            td.intmedioambiente,
            td.descuentos,
            td.totaliquidavig
            FROM tesoliquidapredial t
            INNER JOIN tesoliquidapredial_det td
            ON t.idpredial = td.idpredial
            WHERE t.codigocatastral = '$cedula'";
            $request = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
            if(!empty($request)){
                $total = count($request);
                for ($i=0; $i < $total ; $i++) {
                    $estado = $request[$i]['estado'];
                    $request[$i]['recibo'] ="";
                    if($estado == "P"){
                        $idPredial = $request[$i]['idpredial'];
                        $request_recibo = mysqli_query(
                            $this->linkbd,
                            "SELECT r.id_recibos, DATE_FORMAT(r.fecha,'%d/%m/%Y') as fecha,
                            b.ncuentaban as cuenta
                            FROM tesoreciboscaja r
                            INNER JOIN tesobancosctas b
                            ON b.cuenta = r.cuentabanco
                            WHERE r.id_recaudo = $idPredial"
                        )->fetch_assoc();
                        $request[$i]['recibo'] = $request_recibo;
                    }
                }
            }
            return $request;
        }
    }
?>
