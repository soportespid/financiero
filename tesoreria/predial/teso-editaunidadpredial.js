
const URL = 'tesoreria/predial/teso-editaunidadpredial.php';

const app = Vue.createApp({
    data() {
      return {
        detalles: [],
        vigencia: '',
        tipoUnidad: '',
        tiposDeUnidad: [],
        valor: 0,
        loading: false,
        mostrarUnidad: false,
        
      }
    },

    mounted() {
		  this.loading = false;
          this.tiposDeUnidadPredial();
          //this.valoresIniciales();
	  },

    computed: {
      
    },

    methods: {
        
        getParametros(name){
            //Captura parametros
            name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
            var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
            results = regex.exec(location.search);
            return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
        },

        async valoresIniciales() {

            /* this.loading = true;

            this.vigencia = this.getParametros('vigenciaCla');

            let formData = new FormData();
            formData.append("vigencia", this.vigencia);
            
            await axios.post(URL, formData)
            .then((response) => {
                
                this.detalles = response.data.detalles;
                
            }).catch((error) => {
                this.error = true;
                console.log(error)
            }).finally(() => {
                this.loading = false;
            });   */   
        },

        //metodos par aeste archivo

        tiposDeUnidadPredial(){
            axios.post(URL)
            .then((response) => {

                this.tiposDeUnidad = response.data.tiposDeUnidad;

                if(response.data.tiposDeUnidad.length > 0){
                    this.tipoUnidad = response.data.tiposDeUnidad[0][0];
                    this.detalles = response.data.detalles;
                }
                
            }).catch((error) => {
                this.error = true;
                console.log(error)
            });
        },

        validarUnidad(){
            if(this.tipoUnidad == 3){
                this.mostrarUnidad = true;
                this.valor = 1;
            }else{
                this.mostrarUnidad = false;
                this.valor = 0;
            }
        },

        nombreUnidad(indice){
            return this.tiposDeUnidad[indice-1][1];
        },


        eliminarDetalle(item){

            var i = this.detalles.indexOf( item );
            
            if ( i !== -1 ) {
                this.detalles.splice( i, 1 );
            }

        },

        agregarDetalle(){
            if(this.vigencia != '' && this.valor > 0){
                let $existeVig = false;
                this.detalles.forEach(element => {
                    if(element[1] == this.vigencia){
                        $existeVig = true;
                    }
                });
                if($existeVig){
                    Swal.fire(
                        'Ya existe Vigencia.',
                        'La vigencia ya fue agregada.',
                        'warning'
                        ).then((result) => {
                            this.vigencia = '';
                        });
                }else{
                    var detallesAgr = [];
                    detallesAgr.push(this.tipoUnidad);
                    detallesAgr.push(this.vigencia);
                    detallesAgr.push(this.valor);
                    
                    this.detalles.push(detallesAgr);

                    this.vigencia = '';
                    this.valor = 0;
                    this.validarUnidad();
                }
                
            }else{
                Swal.fire(
                    'Falta informaci&oacute;n.',
                    'Revise todos los campos y vuelva a intentar.',
                    'warning'
                    ).then((result) => {
                        
                    });
            }
        },

        validarVigencia(){
            
            if(typeof this.vigencia == 'number' && this.getlength(this.vigencia) == 4){

                let $existeVig = false;
                this.detalles.forEach(element => {
                    if(element[1] == this.vigencia){
                        $existeVig = true;
                    }
                });

                if($existeVig){
                    Swal.fire(
                        'Ya existe Vigencia.',
                        'La vigencia ya fue agregada.',
                        'warning'
                        ).then((result) => {
                            this.vigencia = '';
                        });
                } 
                
            }else{
                Swal.fire(
                    'Vigencia digitada incorrecta.',
                    'La vigencia debe ser de tipo numerico y de 4 digitos.',
                    'warning'
                    ).then((result) => {
                        this.vigencia = '';
                    });
            }
        },

        getlength(number) {
            let vig = number+'';
            return vig.length; 
        },

        guardarUnidad(){
            if(this.detalles.length > 0){

                Swal.fire({
                    title: 'Esta seguro de guardar?',
                    text: "Guardar unidad predial en la base de datos, confirmar campos!",
                    icon: 'question',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Si, guardar!'
                }).then((result) => {
                    if (result.isConfirmed) {
                        var formData = new FormData();

                        for(let i=0; i <= this.detalles.length-1; i++){
                            const val = Object.values(this.detalles[i]).length;
                            
                            for(let x = 0; x <= val; x++){
                                formData.append("detallesC["+i+"][]", Object.values(this.detalles[i])[x]);
                            }
                            
                        }

                        formData.append("vigencia", this.vigencia);
                    
                        axios.post(URL + '?action=guardarUnidad', formData)
                        .then((response) => {
                            
                            if(response.data.insertaBien){
                                Swal.fire({
                                    position: 'top-end',
                                    icon: 'success',
                                    title: 'La unidad predial se guard&oacute; con Exito',
                                    showConfirmButton: false,
                                    timer: 1500
                                }).then((response) => {
                                        //this.redireccionar();
                                    });
                            }else{
                            
                                Swal.fire(
                                    'Error!',
                                    'No se pudo guardar.',
                                    'error'
                                );
                            }
                            
                        });
                        
                    }
                });
            }else{
                Swal.fire(
                    'Falta informaci&oacute;n para guardar unidad predial.',
                    'Verifique que todos los campos esten diligenciados.',
                    'warning'
                );
            }
        },

        /* redireccionar(){
            
            datos = "vigenciaCla=" + this.vigencia;

            location.href="teso-editaclasificacionpredial.php?" + datos;

        }, */

        toFormData(obj){
            var form_data = new FormData();
            for(var key in obj){
                form_data.append(key, obj[key]);
            }
            return form_data;
        },

        formatonumero: function(valor){
            return new Intl.NumberFormat("en-US", {style: "currency", currency: "USD"}).format(valor);
        },
    }
})

app.mount('#myapp')