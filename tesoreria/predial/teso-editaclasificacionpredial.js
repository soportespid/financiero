
const URL = 'tesoreria/predial/teso-editaclasificacionpredial.php';

const app = Vue.createApp({
    data() {
      return {
        detalles: [],
        vigencia: '',
        tipoPredio: '',
        tiposDePredios: [],
        clasificadores: [],
        numClasificador: '',
        loading: false,
        
      }
    },

    mounted() {
		  this.loading = false;
          this.tiposDePredio();
          this.valoresIniciales();
	  },

    computed: {
      
    },

    methods: {
        
        getParametros(name){
            //Captura parametros
            name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
            var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
            results = regex.exec(location.search);
            return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
        },

        async valoresIniciales() {

            this.loading = true;

            this.vigencia = this.getParametros('vigenciaCla');

            let formData = new FormData();
            formData.append("vigencia", this.vigencia);
            
            await axios.post(URL, formData)
            .then((response) => {
                
                this.detalles = response.data.detalles;
                
            }).catch((error) => {
                this.error = true;
                console.log(error)
            }).finally(() => {
                this.loading = false;
            });     
        },

        //metodos par aeste archivo

        tiposDePredio(){
            axios.post(URL)
            .then((response) => {

                this.tiposDePredios = response.data.tiposDePredios;
                this.clasificadores = response.data.clasificadores;

                if(response.data.tiposDePredios.length > 0){
                    this.tipoPredio = response.data.tiposDePredios[0][0];
                }
                
            }).catch((error) => {
                this.error = true;
                console.log(error)
            });
        },

        nombrePredio(indice){
            return this.tiposDePredios[indice-1][1];
        },

        nombreClasificador(indice){
            return this.clasificadores[indice-1][1];
        },

        eliminarDetalle(item){

            var i = this.detalles.indexOf( item );
            
            if ( i !== -1 ) {
                this.detalles.splice( i, 1 );
            }

        },

        agregarDetalle(){
            if(this.vigencia != '' && this.numClasificador != ''){
                var detallesAgr = [];
                detallesAgr.push(this.tipoPredio);
                detallesAgr.push(this.numClasificador);
                detallesAgr.push(this.vigencia);
                
                this.detalles.push(detallesAgr);

                this.numClasificador = '';
            }else{
                Swal.fire(
                    'Falta informaci&oacute;n.',
                    'Revise todos los campos y vuelva a intentar.',
                    'warning'
                    ).then((result) => {
                        
                    });
            }
        },

        validarVigencia(){
            axios.post(URL + '?action=validarVigencia&vigencia='+this.vigencia)
            .then((response) => {

                if(response.data.existeVigencia){
                    Swal.fire(
                        'Ya existe Vigencia.',
                        'si ya existe, puede editar la vigencia.',
                        'warning'
                        ).then((result) => {
                            this.vigencia = '';
                        });
                }
                
            }).catch((error) => {
                this.error = true;
                console.log(error)
            });
        },

        guardarClasificador(){
            if(this.detalles.length > 0){

                Swal.fire({
                    title: 'Esta seguro de guardar?',
                    text: "Guardar clasificacion predial en la base de datos, confirmar campos!",
                    icon: 'question',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Si, guardar!'
                }).then((result) => {
                    if (result.isConfirmed) {
                        var formData = new FormData();

                        for(let i=0; i <= this.detalles.length-1; i++){
                            const val = Object.values(this.detalles[i]).length;
                            
                            for(let x = 0; x <= val; x++){
                                formData.append("detallesC["+i+"][]", Object.values(this.detalles[i])[x]);
                            }
                            
                        }

                        formData.append("vigencia", this.vigencia);
                    
                        axios.post(URL + '?action=guardarClasificador', formData)
                        .then((response) => {
                            
                            if(response.data.insertaBien){
                                Swal.fire({
                                    position: 'top-end',
                                    icon: 'success',
                                    title: 'El clasificador predial se guard&oacute; con Exito',
                                    showConfirmButton: false,
                                    timer: 1500
                                }).then((response) => {
                                        //this.redireccionar();
                                    });
                            }else{
                            
                                Swal.fire(
                                    'Error!',
                                    'No se pudo guardar.',
                                    'error'
                                );
                            }
                            
                        });
                        
                    }
                });
            }else{
                Swal.fire(
                    'Falta informaci&oacute;n para guardar clasificaci&oacute;n predial.',
                    'Verifique que todos los campos esten diligenciados.',
                    'warning'
                );
            }
        },

        /* redireccionar(){
            
            datos = "vigenciaCla=" + this.vigencia;

            location.href="teso-editaclasificacionpredial.php?" + datos;

        }, */

        toFormData(obj){
            var form_data = new FormData();
            for(var key in obj){
                form_data.append(key, obj[key]);
            }
            return form_data;
        },

        formatonumero: function(valor){
            return new Intl.NumberFormat("en-US", {style: "currency", currency: "USD"}).format(valor);
        },
    }
})

app.mount('#myapp')