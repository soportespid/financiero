
const URL = 'tesoreria/predial/teso-buscaclasificacionpredial.php';

const app = Vue.createApp({
    data() {
      return {
        detalles: [],
        vigencia: '',
        loading: false,
        
      }
    },

    mounted() {
		  this.loading = false;
          this.cargarDatos();
	  },

    computed: {
      
    },

    methods: {
        
        getParametros(name){
            //Captura parametros
            name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
            var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
            results = regex.exec(location.search);
            return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
        },


        //metodos par aeste archivo

        buscarVig(){
            axios.post(URL + '?action=buscarVig&vigencia=' + this.vigencia)
            .then((response) => {

                this.detalles = response.data.vigencias;
                
            }).catch((error) => {
                this.error = true;
                console.log(error)
            });
        },

        cargarDatos(){
            axios.post(URL)
            .then((response) => {

                this.detalles = response.data.vigencias;
                
            }).catch((error) => {
                this.error = true;
                console.log(error)
            });
        },

        redirectEdit: function(vigencia){

            let datos;
            const vigenciaCla = vigencia;
            
            datos = "vigenciaCla=" + vigenciaCla;

            location.href="teso-editaclasificacionpredial.php?" + datos;
            //mypop.focus();
            
		},


        toFormData(obj){
            var form_data = new FormData();
            for(var key in obj){
                form_data.append(key, obj[key]);
            }
            return form_data;
        },

        formatonumero: function(valor){
            return new Intl.NumberFormat("en-US", {style: "currency", currency: "USD"}).format(valor);
        },
    }
})

app.mount('#myapp')