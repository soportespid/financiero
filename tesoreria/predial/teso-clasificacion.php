<?php

    require_once '../../comun.inc';
    require '../../funciones.inc';
 
    session_start();

	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");

    $out = array('error' => false);

    $action = "show";

    if(isset($_GET['action'])){
        $action = $_GET['action'];
    }
    
    if($action == 'show'){

        $consecutivo = 0;

        $sqlr = "SELECT MAX(id) FROM teso_clasificacion";
        $res = mysqli_query($linkbd, $sqlr);
        $row = mysqli_fetch_row($res);
        $consecutivo = $row[0] + 1;
        
        $out['consecutivo'] = $consecutivo;

    }

    if($action == 'guardarClasificador'){
       
        $user = $_SESSION['nickusu'];
        $nombreClasificador = $_POST['nomClasificacion'];
        $consecutivo = 0;
        $sqlr = "SELECT MAX(id) FROM teso_clasificacion";
        $res = mysqli_query($linkbd, $sqlr);
        $row = mysqli_fetch_row($res);
        $consecutivo = $row[0] + 1;

        $sqlrI = "INSERT INTO teso_clasificacion (id, nombre, estado, user) VALUES (".$consecutivo.", '".$nombreClasificador."', 'S', '".$user."')";
        mysqli_query($linkbd, $sqlrI);
        
        $out['insertaBien'] = true;
        

    }

    header("Content-type: application/json");
    echo json_encode($out);
    die();