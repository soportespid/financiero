<?php

    require_once '../../comun.inc';
    require '../../funciones.inc';
 
    session_start();

	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");

    $out = array('error' => false);

    $action = "show";

    if(isset($_GET['action'])){
        $action = $_GET['action'];
    }
    
    if($action == 'show'){

        $vigencias = [];

        $sqlr = "SELECT vigencia FROM teso_clasificacion_predios GROUP BY vigencia ORDER BY vigencia DESC";
        $res = mysqli_query($linkbd, $sqlr);
        while($row = mysqli_fetch_row($res)){
            array_push($vigencias, $row);
        }

        $out['vigencias'] = $vigencias;

    }

    if($action == 'buscarVig'){

        $vigencias = [];
        $vigencia = $_GET['vigencia'];

        $sqlr = "SELECT vigencia FROM teso_clasificacion_predios WHERE vigencia LIKE '%$vigencia%' GROUP BY vigencia ORDER BY vigencia DESC";
        $res = mysqli_query($linkbd, $sqlr);
        while($row = mysqli_fetch_row($res)){
            array_push($vigencias, $row);
        }

        $out['vigencias'] = $vigencias;
        
    }

    header("Content-type: application/json");
    echo json_encode($out);
    die();