
const URL = 'tesoreria/predial/teso-buscaclasificacion.php';

const app = Vue.createApp({
    data() {
      return {
        detalles: [],
        consecutivo: '',
        loading: false,
        
      }
    },

    mounted() {
		  this.loading = false;
          this.cargarDatos();
	  },

    computed: {
      
    },

    methods: {
        
        getParametros(name){
            //Captura parametros
            name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
            var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
            results = regex.exec(location.search);
            return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
        },


        //metodos par aeste archivo

        buscarClasificador(){
            axios.post(URL + '?action=buscarClasificador&consecutivo=' + this.consecutivo)
            .then((response) => {

                this.detalles = response.data.detalles;
                
            }).catch((error) => {
                this.error = true;
                console.log(error)
            });
        },

        cargarDatos(){
            axios.post(URL)
            .then((response) => {

                this.detalles = response.data.detalles;
                
            }).catch((error) => {
                this.error = true;
                console.log(error)
            });
        },

        redirectEdit(cla){

            let datos;
            const clasificacion = cla;
            
            datos = "clasificacion=" + clasificacion;

            location.href="teso-editaclasificacion.php?" + datos;
            //mypop.focus();
            
		},


        toFormData(obj){
            var form_data = new FormData();
            for(var key in obj){
                form_data.append(key, obj[key]);
            }
            return form_data;
        },

        formatonumero: function(valor){
            return new Intl.NumberFormat("en-US", {style: "currency", currency: "USD"}).format(valor);
        },
    }
})

app.mount('#myapp')