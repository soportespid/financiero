<?php

    require_once '../../comun.inc';
    require '../../funciones.inc';
 
    session_start();

	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");

    $out = array('error' => false);

    $action = "show";

    if(isset($_GET['action'])){
        $action = $_GET['action'];
    }
    
    if($action == 'show'){

        $tiposDePredios = [];
        $clasificadores = [];

        $sqlr = "SELECT * FROM teso_tipos_predio";
        $res = mysqli_query($linkbd, $sqlr);
        while($row = mysqli_fetch_row($res)){
            array_push($tiposDePredios, $row);
        }

        $sqlr = "SELECT id, nombre FROM teso_clasificacion";
        $res = mysqli_query($linkbd, $sqlr);
        while($row = mysqli_fetch_row($res)){
            array_push($clasificadores, $row);
        }

        $out['tiposDePredios'] = $tiposDePredios;
        $out['clasificadores'] = $clasificadores;

    }

    if($action == 'validarVigencia'){
        $existeVig = false;
        $vigencia = $_GET['vigencia'];
        $sqlr = "SELECT COUNT(*) FROM teso_clasificacion_predios WHERE vigencia = '$vigencia'";
        $res = mysqli_query($linkbd, $sqlr);
        $row = mysqli_fetch_row($res);

        if($row[0] > 0){
            $existeVig = true;
        }

        $out['existeVigencia'] = $existeVig;
    }

    if($action == 'guardarClasificador'){
       
        $user = $_SESSION['nickusu'];

        for($x = 0; $x < count($_POST["detallesC"]); $x++){

            $id_tipo_predio = '';
            $clasificacion = '';
            $vigencia = '';
            
            $id_tipo_predio = $_POST["detallesC"][$x][0];
            $clasificacion = $_POST["detallesC"][$x][1];
            $vigencia = $_POST["detallesC"][$x][2];

            $sqlrD = "INSERT INTO teso_clasificacion_predios(id_tipo_predio, clasificacion, vigencia, user) VALUES ('".$id_tipo_predio."', '".$clasificacion."', '".$vigencia."', '".$user."')";
            mysqli_query($linkbd, $sqlrD);
        }
        $out['insertaBien'] = true;
        

    }

    header("Content-type: application/json");
    echo json_encode($out);
    die();