
const URL = 'tesoreria/predial/teso-tiposunidadpredial.php';

const app = Vue.createApp({
    data() {
      return {
        consecutivo: 0,
        unidadPredial: '',
        loading: false,
      }
    },

    mounted() {
		  this.loading = false;
          this.cargarConsecutivo();
	  },

    computed: {
      
    },

    methods: {
        
        getParametros(name){
            //Captura parametros
            name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
            var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
            results = regex.exec(location.search);
            return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
        },

        async cargarConsecutivo() {

            this.loading = true;

            /* this.tipo_comp = this.getParametros('tipo_comprobante');
            let formData = new FormData();
            formData.append("tipo_comp", this.tipo_comp); */
            
            await axios.post(URL)
            .then((response) => {
                
                this.consecutivo = response.data.consecutivo;
                
            }).catch((error) => {
                this.error = true;
                console.log(error)
            }).finally(() => {
                this.loading = false;
            });     
        },

        //metodos par aeste archivo


        guardarTipoUnidad(){
            if(this.unidadPredial.length != ''){

                Swal.fire({
                    title: 'Esta seguro de guardar?',
                    text: "Guardar unidad predial en la base de datos, confirmar campos!",
                    icon: 'question',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Si, guardar!'
                }).then((result) => {
                    if (result.isConfirmed) {
                        var formData = new FormData();

                        formData.append("consecutivo", this.consecutivo);
                        formData.append("unidadPredial", this.unidadPredial);
                    
                        axios.post(URL + '?action=guardarUnidadPredial', formData)
                        .then((response) => {
                            
                            if(response.data.insertaBien){
                                Swal.fire({
                                    position: 'top-end',
                                    icon: 'success',
                                    title: 'Unidad predial se guard&oacute; con Exito',
                                    showConfirmButton: false,
                                    timer: 1500
                                }).then((response) => {
                                        this.redireccionar();
                                    });
                            }else{
                            
                                Swal.fire(
                                    'Error!',
                                    'No se pudo guardar.',
                                    'error'
                                );
                            }
                            
                        });
                        
                    }
                });
            }else{
                Swal.fire(
                    'Falta informaci&oacute;n para guardar unidad predial.',
                    'Verifique que todos los campos esten diligenciados.',
                    'warning'
                );
            }
        },

        redireccionar(){
            
            location.href ="teso-tiposunidadpredial.php";
        },

        toFormData(obj){
            var form_data = new FormData();
            for(var key in obj){
                form_data.append(key, obj[key]);
            }
            return form_data;
        },

        formatonumero: function(valor){
            return new Intl.NumberFormat("en-US", {style: "currency", currency: "USD"}).format(valor);
        },
    }
})

app.mount('#myapp')