
const URL = 'tesoreria/predial/teso-unidadpredial.php';

const app = Vue.createApp({
    data() {
      return {
        detalles: [],
        vigencia: '',
        tipoUnidad: '',
        tiposDeUnidad: [],
        valor: 0,
        loading: false,
        mostrarUnidad: false,
        
      }
    },

    mounted() {
		  this.loading = false;
          this.cargarInformacionInicial();
	  },

    computed: {
      
    },

    methods: {
        
        getParametros(name){
            //Captura parametros
            name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
            var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
            results = regex.exec(location.search);
            return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
        },

        async valoresIniciales() {

            this.loading = true;

            this.tipo_comp = this.getParametros('tipo_comprobante');
            this.fechaIni = this.getParametros('fechaIni');
            this.fechaFin = this.getParametros('fechaFin');
            this.seccion_presupuestal = this.getParametros('seccion_presupuestal');
            this.medio_pago = this.getParametros('medio_pago');
            this.vigencia_gasto = this.getParametros('vigencia_gasto');
            this.vigencia_fiscal = this.getParametros('vigencia_fiscal');
            this.fuente_f = this.getParametros('fuente');
            this.bpim_inv = this.getParametros('bpim_inv');
            this.cuenta_f = this.getParametros('cuenta_f');
            this.programatico_inv = this.getParametros('programatico_inv');

            if(this.seccion_presupuestal == '-1'){
                this.seccion_presupuestal = 'todos';
            }
            if(this.medio_pago == '-1'){
                this.medio_pago = 'todos';
            }
            if(this.vigencia_gasto == '-1'){
                this.vigencia_gasto = 'todos';
            }

            let fechaInicial = '';
            let fechaFinal = '';

            if(this.fechaIni && this.fechaFin){
                let arrFechaInicial = this.fechaIni.split("/");
                fechaInicial = `${arrFechaInicial[2]}-${arrFechaInicial[1]}-${arrFechaInicial[0]}`;
                this.vigencia_fiscal = arrFechaInicial[2];
                let arrFechaFinal = this.fechaFin.split("/");
                fechaFinal = `${arrFechaFinal[2]}-${arrFechaFinal[1]}-${arrFechaFinal[0]}`;
            }else{
                fechaInicial = '';
                fechaFinal = '';
            }

            let formData = new FormData();
            formData.append("tipo_comp", this.tipo_comp);
            formData.append("fechaIni", fechaInicial);
            formData.append("fechaFin", fechaFinal);
            formData.append("seccion_presupuestal", this.seccion_presupuestal);
            formData.append("medio_pago", this.medio_pago);
            formData.append("vigencia_gasto", this.vigencia_gasto);
            formData.append("vigencia_fiscal", this.vigencia_fiscal);
            formData.append("fuente_f", this.fuente_f);
            formData.append("bpim_inv", this.bpim_inv);
            formData.append("cuenta_f", this.cuenta_f);
            formData.append("programatico_inv", this.programatico_inv);
            
            await axios.post(URL, formData)
            .then((response) => {
                
                this.detalles = response.data.detalles;
                
            }).catch((error) => {
                this.error = true;
                console.log(error)
            }).finally(() => {
                this.loading = false;
            });     
        },

        //metodos par aeste archivo

        cargarInformacionInicial(){
            axios.post(URL)
            .then((response) => {

                this.tiposDeUnidad = response.data.tiposDeUnidad;
                console.log(response.data);
                if(response.data.tiposDeUnidad.length > 0){
                    this.tipoUnidad = response.data.tiposDeUnidad[0][0];
                }
                
            }).catch((error) => {
                this.error = true;
                console.log(error)
            });
        },

        nombreUnidad(indice){
            return this.tiposDeUnidad[indice-1][1];
        },


        eliminarDetalle(item){

            var i = this.detalles.indexOf( item );
            
            if ( i !== -1 ) {
                this.detalles.splice( i, 1 );
            }

        },

        agregarDetalle(){
            if(this.vigencia != '' && this.valor > 0){
                var detallesAgr = [];
                detallesAgr.push(this.tipoUnidad);
                detallesAgr.push(this.vigencia);
                detallesAgr.push(this.valor);
                
                this.detalles.push(detallesAgr);

                this.valor = '';
            }else{
                Swal.fire(
                    'Falta informaci&oacute;n.',
                    'Revise todos los campos y vuelva a intentar.',
                    'warning'
                    ).then((result) => {
                        
                    });
            }
        },

        validarVigencia(){
            
            if(typeof this.vigencia == 'number' && this.getlength(this.vigencia) == 4){
                axios.post(URL + '?action=validarVigencia&vigencia='+this.vigencia)
                .then((response) => {

                    if(response.data.existeVigencia){
                        Swal.fire(
                            'Ya existe Vigencia.',
                            'si ya existe, puede editar la vigencia.',
                            'warning'
                            ).then((result) => {
                                this.vigencia = '';
                            });
                    }
                    
                }).catch((error) => {
                    this.error = true;
                    console.log(error)
                });
            }else{
                Swal.fire(
                    'Vigencia digitada incorrecta.',
                    'La vigencia debe ser de tipo numerico y de 4 digitos.',
                    'warning'
                    ).then((result) => {
                        this.vigencia = '';
                    });
            }
            
        },

        getlength(number) {
            let vig = number+'';
            return vig.length; 
        },

        guardarUnidad(){
            if(this.detalles.length > 0){

                Swal.fire({
                    title: 'Esta seguro de guardar?',
                    text: "Guardar unidad predial en la base de datos, confirmar campos!",
                    icon: 'question',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Si, guardar!'
                }).then((result) => {
                    if (result.isConfirmed) {
                        var formData = new FormData();

                        for(let i=0; i <= this.detalles.length-1; i++){
                            const val = Object.values(this.detalles[i]).length;
                            
                            for(let x = 0; x <= val; x++){
                                formData.append("detallesC["+i+"][]", Object.values(this.detalles[i])[x]);
                            }
                            
                        }
                    
                        axios.post(URL + '?action=guardarUnidad', formData)
                        .then((response) => {
                            
                            if(response.data.insertaBien){
                                Swal.fire({
                                    position: 'top-end',
                                    icon: 'success',
                                    title: 'La unidad predial se guard&oacute; con Exito',
                                    showConfirmButton: false,
                                    timer: 1500
                                }).then((response) => {
                                        this.redireccionar();
                                    });
                            }else{
                            
                                Swal.fire(
                                    'Error!',
                                    'No se pudo guardar.',
                                    'error'
                                );
                            }
                            
                        });
                        
                    }
                });
            }else{
                Swal.fire(
                    'Falta informaci&oacute;n para guardar unidad predial.',
                    'Verifique que todos los campos esten diligenciados.',
                    'warning'
                );
            }
        },

        validarUnidad(){
            if(this.tipoUnidad == 3){
                this.mostrarUnidad = true;
                this.valor = 1;
            }else{
                this.mostrarUnidad = false;
                this.valor = 0;
            }
        },

        redireccionar(){
            
            location.href ="teso-unidadpredial.php";
        },

        toFormData(obj){
            var form_data = new FormData();
            for(var key in obj){
                form_data.append(key, obj[key]);
            }
            return form_data;
        },

        formatonumero: function(valor){
            return new Intl.NumberFormat("en-US", {style: "currency", currency: "USD"}).format(valor);
        },

    }
})

app.mount('#myapp')