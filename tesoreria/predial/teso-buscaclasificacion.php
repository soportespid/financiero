<?php

    require_once '../../comun.inc';
    require '../../funciones.inc';
 
    session_start();

	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");

    $out = array('error' => false);

    $action = "show";

    if(isset($_GET['action'])){
        $action = $_GET['action'];
    }
    
    if($action == 'show'){

        $detalles = [];

        $sqlr = "SELECT * FROM teso_clasificacion ORDER BY id DESC";
        $res = mysqli_query($linkbd, $sqlr);
        while($row = mysqli_fetch_row($res)){
            array_push($detalles, $row);
        }

        $out['detalles'] = $detalles;

    }

    if($action == 'buscarClasificador'){

        $detalles = [];
        $consecutivo = $_GET['consecutivo'];

        $sqlr = "SELECT * FROM teso_clasificacion WHERE id LIKE '%$consecutivo%' GROUP BY id DESC ";
        $res = mysqli_query($linkbd, $sqlr);
        while($row = mysqli_fetch_row($res)){
            array_push($detalles, $row);
        }

        $out['detalles'] = $detalles;
        
    }

    header("Content-type: application/json");
    echo json_encode($out);
    die();