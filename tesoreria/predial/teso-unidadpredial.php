<?php

    require_once '../../comun.inc';
    require '../../funciones.inc';
 
    session_start();

	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");

    $out = array('error' => false);

    $action = "show";

    if(isset($_GET['action'])){
        $action = $_GET['action'];
    }
    
    if($action == 'show'){

        $tiposDeUnidad = [];

        $sqlr = "SELECT * FROM teso_tipos_unidad";
        $res = mysqli_query($linkbd, $sqlr);
        while($row = mysqli_fetch_row($res)){
            array_push($tiposDeUnidad, $row);
        }

        $out['tiposDeUnidad'] = $tiposDeUnidad;

    }

    if($action == 'validarVigencia'){
        $existeVig = false;
        $vigencia = $_GET['vigencia'];
        $sqlr = "SELECT COUNT(*) FROM teso_unidades_predial WHERE vigencia = '$vigencia'";
        $res = mysqli_query($linkbd, $sqlr);
        $row = mysqli_fetch_row($res);

        if($row[0] > 0){
            $existeVig = true;
        }

        $out['existeVigencia'] = $existeVig;
    }

    if($action == 'guardarUnidad'){
       
        $user = $_SESSION['nickusu'];

        for($x = 0; $x < count($_POST["detallesC"]); $x++){

            $id_tipo_unidad = '';
            $vigencia = '';
            $valor = 0;
            
            $id_tipo_unidad = $_POST["detallesC"][$x][0];
            $vigencia = $_POST["detallesC"][$x][1];
            $valor = $_POST["detallesC"][$x][2];

            $sqlrD = "INSERT INTO teso_unidades_predial(id_tipo_unidad, vigencia, valor, user) VALUES ('".$id_tipo_unidad."', '".$vigencia."', '".$valor."', '".$user."')";
            mysqli_query($linkbd, $sqlrD);
        }
        $out['insertaBien'] = true;
        

    }

    header("Content-type: application/json");
    echo json_encode($out);
    die();