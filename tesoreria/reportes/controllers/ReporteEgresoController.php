<?php
    require_once '../../../Librerias/core/Helpers.php';
    require_once '../../../PHPExcel/Classes/PHPExcel.php';
    require_once '../models/ReporteEgresoModel.php';
    session_start();

    class ReporteEgresoController extends ReporteEgresoModel{
        public function initialData(){
            if(!empty($_SESSION)){
                $arrResponse = array(
                    "fuentes"=>$this->selectFuentes(),
                    "terceros"=>$this->selectTerceros()
                );
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
            die();
        }
        public function genData(){
            if(!empty($_SESSION)){
                if($_POST){
                    $strFechaInicial = strClean($_POST['fecha_inicial']);
                    $strFechaFinal = strClean($_POST['fecha_final']);
                    $strTercero = strClean($_POST['tercero']);
                    $intEgreso = strClean($_POST['egreso']);
                    $strFuente = strClean($_POST['fuente']);
                    $request = $this->selectData($strFechaInicial,$strFechaFinal,$strTercero,$intEgreso,$strFuente);
                    if(!empty($request)){
                        $arrGastos = array(
                            1=>"1 - Vigencia actual",
                            2=>"2 - Reservas",
                            3=>"3 - Cuentas por pagar",
                            4=>"4 - Vigencias futuras - vigencia actual",
                            5=>"5 - Vigencias futuras - reservas",
                            6=>"6 - Vigencias futuras - cuentas por pagar",
                        );
                        $total = count($request);
                        for ($i=0; $i < $total ; $i++) {
                            $data = $request[$i];
                            $arrFuente = $this->selectFuente($data['fuente']);
                            $arrCuenta = $this->selectCuenta($data['cuenta_presupuestal']);
                            $strRubro = "";
                            $strVigencia = $arrGastos[$data['vigencia']];
                            $strFuente = $data['fuente']."-".$arrFuente['nombre'];
                            $strTercero = $data['tercero']."-".$data['nombre_tercero'];
                            if($data['bpim'] != ""){
                                $programa = substr($data['indicador_producto'],0,4);
                                $strRubro = $data['bpim']."-".$programa."-".
                                $data['indicador_producto']."-".$data['fuente']."-".$data['nombre_proyecto'];
                            }else{
                                $strRubro = $data['cuenta_presupuestal']."-".$arrCuenta['nombre'];
                            }
                            $data['rubro'] = $strRubro;
                            $data['vigencia'] = $strVigencia;
                            $data['fuente'] = $strFuente;
                            $data['tercero'] = $strTercero;
                            $request[$i]= $data;
                        }
                    }
                }
                echo json_encode($request,JSON_UNESCAPED_UNICODE);
            }
            die();
        }
    }

    if($_POST){
        $obj = new ReporteEgresoController();
        if($_POST['action'] == "get"){
            $obj->initialData();
        }else if($_POST['action'] == "gen"){
            $obj->genData();
        }
    }

?>
