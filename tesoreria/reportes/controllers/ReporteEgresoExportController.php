<?php
    require_once '../../../Librerias/core/Helpers.php';
    require_once '../../../PHPExcel/Classes/PHPExcel.php';
    require_once '../../../tcpdf/tcpdf_include.php';
    require_once '../../../tcpdf/tcpdf.php';
    session_start();
    /*ini_set('display_errors', '1');
    ini_set('display_startup_errors', '1');
    error_reporting(E_ALL);*/
    class ReporteEgresoExportController {
        public function exportExcel(){
            if(!empty($_SESSION)){
                $arrData = json_decode($_POST['data'],true);
                if(!empty($arrData)){

                    $arrFechaInicial = explode("-",$_POST['fecha_inicial']);
                    $arrFechaFinal = explode("-",$_POST['fecha_final']);
                    $strFechaInicial = $arrFechaInicial[2]."/".$arrFechaInicial[1]."/".$arrFechaInicial[0];
                    $strFechaFinal = $arrFechaFinal[2]."/".$arrFechaFinal[1]."/".$arrFechaFinal[0];
                    $strTercero = strClean($_POST['tercero']);
                    $strEgreso = strClean($_POST['egreso']);
                    $strFuente = strClean($_POST['fuente']);

                    $objPHPExcel = new PHPExcel();
                    $objPHPExcel->getActiveSheet()->getStyle('A:P')->applyFromArray(
                        array(
                            'alignment' => array(
                                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
                                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
                            )
                        )
                    );
                    $objPHPExcel->getProperties()
                    ->setCreator("IDEAL 10")
                    ->setLastModifiedBy("IDEAL 10")
                    ->setTitle("Exportar Excel con PHP")
                    ->setSubject("Documento de prueba")
                    ->setDescription("Documento generado con PHPExcel")
                    ->setKeywords("usuarios phpexcel")
                    ->setCategory("reportes");
                    //----Cuerpo de Documento----
                    $objPHPExcel->setActiveSheetIndex(0)
                    ->mergeCells('A1:P1')
                    ->mergeCells('A2:P2')
                    ->setCellValue('A1', 'TESORERÍA')
                    ->setCellValue('A2', 'REPORTE DE EGRESOS');

                    $objPHPExcel-> getActiveSheet ()
                    -> getStyle ("A1")
                    -> getFill ()
                    -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
                    -> getStartColor ()
                    -> setRGB ('C8C8C8');
                    $objPHPExcel-> getActiveSheet ()
                    -> getStyle ("A1:A2")
                    -> getFont ()
                    -> setBold ( true )
                    -> setName ( 'Verdana' )
                    -> setSize ( 10 )
                    -> getColor ()
                    -> setRGB ('000000');
                    $objPHPExcel-> getActiveSheet ()
                    -> getStyle ('A1:A2')
                    -> getAlignment ()
                    -> applyFromArray (array ( 'horizontal'  =>  PHPExcel_Style_Alignment :: HORIZONTAL_CENTER ,) );
                    $objPHPExcel-> getActiveSheet ()
                    -> getStyle ('A3:P3')
                    -> getAlignment ()
                    -> applyFromArray (array ( 'horizontal'  =>  PHPExcel_Style_Alignment :: VERTICAL_JUSTIFY ,) );
                    $objPHPExcel-> getActiveSheet ()
                    -> getStyle ("A2")
                    -> getFill ()
                    -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID);

                    $borders = array(
                        'borders' => array(
                            'allborders' => array(
                                'style' => PHPExcel_Style_Border::BORDER_THIN,
                            'color' => array('argb' => 'FF000000'),
                            )
                        ),
                    );
                    $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A3', "Fecha")
                    ->setCellValue('A4', "No Egreso")
                    ->setCellValue('A5', "Tercero")
                    ->setCellValue('A6', "Fuente")
                    ->mergeCells('B3:P3')
                    ->mergeCells('B4:P4')
                    ->mergeCells('B5:P5')
                    ->mergeCells('B6:P6')
                    ->setCellValue('B3', "Desde ".$strFechaInicial." hasta ".$strFechaFinal)
                    ->setCellValue('B4', $strEgreso)
                    ->setCellValue('B5', $strTercero)
                    ->setCellValue('B6', $strFuente);

                    $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A7', 'Nro Egreso')
                    ->setCellValue('B7', "Fecha Egreso")
                    ->setCellValue('C7', "Nro obligación")
                    ->setCellValue('D7', "Fecha obligación")
                    ->setCellValue('E7', "Cuenta")
                    ->setCellValue('F7', "Nombre cuenta")
                    ->setCellValue('G7', "Tercero")
                    ->setCellValue('H7', "Objeto")
                    ->setCellValue('I7', "Valor bruto")
                    ->setCellValue('J7', "Valor retención")
                    ->setCellValue('K7', "Valor neto")
                    ->setCellValue('L7', "Fuente")
                    ->setCellValue('M7', "Rubro")
                    ->setCellValue('N7', "Vigencia del gasto")
                    ->setCellValue('O7', "Medio pago")
                    ->setCellValue('P7', "Valor");
                    $objPHPExcel-> getActiveSheet ()
                        -> getStyle ("A3")
                        -> getFill ()
                        -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
                        -> getStartColor ()
                        -> setRGB ('3399cc');
                    $objPHPExcel-> getActiveSheet ()
                        -> getStyle ("A4")
                        -> getFill ()
                        -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
                        -> getStartColor ()
                        -> setRGB ('3399cc');
                        $objPHPExcel-> getActiveSheet ()
                        -> getStyle ("A5")
                        -> getFill ()
                        -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
                        -> getStartColor ()
                        -> setRGB ('3399cc');
                        $objPHPExcel-> getActiveSheet ()
                        -> getStyle ("A6")
                        -> getFill ()
                        -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
                        -> getStartColor ()
                        -> setRGB ('3399cc');
                    $objPHPExcel-> getActiveSheet ()
                    -> getStyle ("A7:P7")
                    -> getFill ()
                    -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
                    -> getStartColor ()
                    -> setRGB ('99ddff');
                    $objPHPExcel->getActiveSheet()->getStyle('A1:P1')->applyFromArray($borders);
                    $objPHPExcel->getActiveSheet()->getStyle('A2:P2')->applyFromArray($borders);
                    $objPHPExcel->getActiveSheet()->getStyle('A3:P3')->applyFromArray($borders);
                    $objPHPExcel->getActiveSheet()->getStyle('A4:P4')->applyFromArray($borders);
                    $objPHPExcel->getActiveSheet()->getStyle('A5:P5')->applyFromArray($borders);
                    $objPHPExcel->getActiveSheet()->getStyle('A6:P6')->applyFromArray($borders);
                    $objPHPExcel->getActiveSheet()->getStyle('A7:P7')->applyFromArray($borders);

                    $objPHPExcel->getActiveSheet()->getStyle('A3')->getFont()->getColor()->setRGB("ffffff");
                    $objPHPExcel->getActiveSheet()->getStyle('A4')->getFont()->getColor()->setRGB("ffffff");
                    $objPHPExcel->getActiveSheet()->getStyle('A5')->getFont()->getColor()->setRGB("ffffff");
                    $objPHPExcel->getActiveSheet()->getStyle('A6')->getFont()->getColor()->setRGB("ffffff");

                    $objPHPExcel->getActiveSheet()->getStyle("A3")->getFont()->setBold(true);
                    $objPHPExcel->getActiveSheet()->getStyle("A4")->getFont()->setBold(true);
                    $objPHPExcel->getActiveSheet()->getStyle("A5")->getFont()->setBold(true);
                    $objPHPExcel->getActiveSheet()->getStyle("A6")->getFont()->setBold(true);


                    $objPHPExcel->getActiveSheet()->getStyle("A7")->getFont()->setBold(true);
                    $objPHPExcel->getActiveSheet()->getStyle("B7")->getFont()->setBold(true);
                    $objPHPExcel->getActiveSheet()->getStyle("C7")->getFont()->setBold(true);
                    $objPHPExcel->getActiveSheet()->getStyle("D7")->getFont()->setBold(true);
                    $objPHPExcel->getActiveSheet()->getStyle("E7")->getFont()->setBold(true);
                    $objPHPExcel->getActiveSheet()->getStyle("F7")->getFont()->setBold(true);
                    $objPHPExcel->getActiveSheet()->getStyle("G7")->getFont()->setBold(true);
                    $objPHPExcel->getActiveSheet()->getStyle("H7")->getFont()->setBold(true);
                    $objPHPExcel->getActiveSheet()->getStyle("I7")->getFont()->setBold(true);
                    $objPHPExcel->getActiveSheet()->getStyle("J7")->getFont()->setBold(true);
                    $objPHPExcel->getActiveSheet()->getStyle("K7")->getFont()->setBold(true);
                    $objPHPExcel->getActiveSheet()->getStyle("L7")->getFont()->setBold(true);
                    $objPHPExcel->getActiveSheet()->getStyle("M7")->getFont()->setBold(true);
                    $objPHPExcel->getActiveSheet()->getStyle("N7")->getFont()->setBold(true);
                    $objPHPExcel->getActiveSheet()->getStyle("O7")->getFont()->setBold(true);
                    $objPHPExcel->getActiveSheet()->getStyle("P7")->getFont()->setBold(true);
                    $row = 8;
                    foreach ($arrData as $data) {
                        $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValueExplicit ("A$row", $data['id_egreso'], PHPExcel_Cell_DataType :: TYPE_STRING)
                        ->setCellValueExplicit ("B$row", $data['fecha'], PHPExcel_Cell_DataType :: TYPE_STRING)
                        ->setCellValueExplicit ("C$row", $data['id_orden'], PHPExcel_Cell_DataType :: TYPE_STRING)
                        ->setCellValueExplicit ("D$row", $data['fecha'], PHPExcel_Cell_DataType :: TYPE_STRING)
                        ->setCellValueExplicit ("E$row", $data['cuenta_contable'], PHPExcel_Cell_DataType :: TYPE_STRING)
                        ->setCellValueExplicit ("F$row", $data['nombre_cuenta_contable'], PHPExcel_Cell_DataType :: TYPE_STRING)
                        ->setCellValueExplicit ("G$row", $data['tercero'], PHPExcel_Cell_DataType :: TYPE_STRING)
                        ->setCellValueExplicit ("H$row", $data['concepto'], PHPExcel_Cell_DataType :: TYPE_STRING)
                        ->setCellValueExplicit ("I$row", $data['valor_bruto'], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
                        ->setCellValueExplicit ("J$row", $data['valor_retencion'], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
                        ->setCellValueExplicit ("K$row", $data['valor_neto'], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
                        ->setCellValueExplicit ("L$row", $data['fuente'], PHPExcel_Cell_DataType :: TYPE_STRING)
                        ->setCellValueExplicit ("M$row", $data['rubro'], PHPExcel_Cell_DataType :: TYPE_STRING)
                        ->setCellValueExplicit ("N$row", $data['vigencia'], PHPExcel_Cell_DataType :: TYPE_STRING)
                        ->setCellValueExplicit ("O$row", $data['medio_pago'], PHPExcel_Cell_DataType :: TYPE_STRING)
                        ->setCellValueExplicit ("P$row", $data['valor'], PHPExcel_Cell_DataType :: TYPE_NUMERIC);
                        $objPHPExcel->getActiveSheet()->getStyle("A$row:P$row")->applyFromArray($borders);
                        $row++;
                    }
                    $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
                    $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
                    $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
                    $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
                    $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
                    $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
                    $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);
                    $objPHPExcel->getActiveSheet()->getColumnDimension('K')->setAutoSize(true);
                    $objPHPExcel->getActiveSheet()->getColumnDimension('N')->setAutoSize(true);
                    $objPHPExcel->getActiveSheet()->getColumnDimension('O')->setAutoSize(true);
                    $objPHPExcel->getActiveSheet()->getColumnDimension('P')->setAutoSize(true);
                    header('Content-Type: application/vnd.ms-excel');
                    header('Content-Disposition: attachment;filename="reporte_egresos.xlsx"');
                    header('Cache-Control: max-age=0');
                    $objWriter=PHPExcel_IOFactory::createWriter($objPHPExcel,'Excel2007');
                    $objWriter->save('php://output');
                }
            }
            die();
        }
        public function exportPdf(){
            if(!empty($_SESSION)){
                $arrData = json_decode($_POST['data'],true);
                if(!empty($arrData)){
                    $arrFechaInicial = explode("-",$_POST['fecha_inicial']);
                    $arrFechaFinal = explode("-",$_POST['fecha_final']);
                    $strFechaInicial = $arrFechaInicial[2]."/".$arrFechaInicial[1]."/".$arrFechaInicial[0];
                    $strFechaFinal = $arrFechaFinal[2]."/".$arrFechaFinal[1]."/".$arrFechaFinal[0];
                    $strTercero = strClean($_POST['tercero']);
                    $strEgreso = strClean($_POST['egreso']);
                    $strFuente = strClean($_POST['fuente']);

                    $pdf = new MYPDF('L','mm','Letter', true, 'iso-8859-1', false);
                    $pdf->SetDocInfoUnicode (true);
                    $intAltoHead = 5;
                    // set document information
                    $pdf->SetCreator(PDF_CREATOR);
                    $pdf->SetAuthor('IDEALSAS');
                    $pdf->SetTitle('REPORTE DE EGRESOS');
                    $pdf->SetSubject('REPORTE DE EGRESOS');
                    $pdf->SetKeywords('TCPDF, PDF, example, test, guide');
                    $pdf->SetMargins(10, 38, 10);// set margins
                    $pdf->SetHeaderMargin(38);// set margins
                    $pdf->SetFooterMargin(17);// set margins
                    $pdf->SetAutoPageBreak(TRUE, 20);// set auto page breaks
                    $pdf->AddPage();
                    $pdf->SetFillColor(51, 153, 204);
                    $pdf->SetTextColor(255,255,255);
                    $pdf->SetFont('Helvetica','b',9);
                    $pdf->MultiCell(53,4,"Fecha","LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->SetFillColor(255,255,255);
                    $pdf->SetTextColor(0,0,0);
                    $pdf->SetFont('Helvetica','',9);
                    $pdf->MultiCell(224,4,"Desde ".$strFechaInicial." Hasta ".$strFechaFinal,"RBT",'L',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->ln();
                    $pdf->SetFillColor(51, 153, 204);
                    $pdf->SetTextColor(255,255,255);
                    $pdf->SetFont('Helvetica','b',9);
                    $pdf->MultiCell(53,4,"Nro Egreso","LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->SetFillColor(255,255,255);
                    $pdf->SetTextColor(0,0,0);
                    $pdf->SetFont('Helvetica','',9);
                    $pdf->MultiCell(224,4,$strEgreso,"RBT",'L',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->ln();
                    $pdf->SetFillColor(51, 153, 204);
                    $pdf->SetTextColor(255,255,255);
                    $pdf->SetFont('Helvetica','b',9);
                    $pdf->MultiCell(53,4,"Tercero","LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->SetFillColor(255,255,255);
                    $pdf->SetTextColor(0,0,0);
                    $pdf->SetFont('Helvetica','',9);
                    $pdf->MultiCell(224,4,$strTercero,"RBT",'L',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->ln();
                    $pdf->SetFillColor(51, 153, 204);
                    $pdf->SetTextColor(255,255,255);
                    $pdf->SetFont('Helvetica','b',9);
                    $pdf->MultiCell(53,4,"Fuente","LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->SetFillColor(255,255,255);
                    $pdf->SetTextColor(0,0,0);
                    $pdf->SetFont('Helvetica','',9);
                    $pdf->MultiCell(224,4,$strFuente,"RBT",'L',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->ln();
                    $pdf->ln();
                    $pdf->SetFillColor(153,221,255);
                    $pdf->SetFont('helvetica','B',6);
                    $pdf->SetTextColor(0,0,0);
                    $pdf->MultiCell(8,$intAltoHead,"Nro Egreso","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->MultiCell(15,$intAltoHead,"Fecha Egreso","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->MultiCell(8,$intAltoHead,"Nro obligación","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->MultiCell(15,$intAltoHead,"Fecha obligación","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->MultiCell(12,$intAltoHead,"Cuenta","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->MultiCell(25,$intAltoHead,"Nombre cuenta","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->MultiCell(25,$intAltoHead,"Tercero","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->MultiCell(33,$intAltoHead,"Objeto","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->MultiCell(17.5,$intAltoHead,"Valor bruto","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->MultiCell(17.5,$intAltoHead,"Valor retención","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->MultiCell(17.5,$intAltoHead,"Valor neto","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->MultiCell(17.5,$intAltoHead,"Fuente","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->MultiCell(25,$intAltoHead,"Rubro","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->MultiCell(16,$intAltoHead,"Vig. gasto","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->MultiCell(8,$intAltoHead,"Medio pago","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->MultiCell(17,$intAltoHead,"Valor","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->ln();
                    $fill = true;
                    foreach ($arrData as $data) {
                        $pdf->SetFillColor(245,245,245);
                        $pdf->SetFont('helvetica','',5.5);
                        $pdf->SetTextColor(0,0,0);
                        $vigencia = $pdf->getNumLines($data['vigencia'],16);
                        $fuente = $pdf->getNumLines($data['nombre'],17.5);
                        $tercero = $pdf->getNumLines($data['tercero'],25);
                        $cuentaContable = $pdf->getNumLines($data['nombre_cuenta_contable'],25);
                        $concepto = $pdf->getNumLines($data['concepto'],33);
                        $rubro = $pdf->getNumLines($data['rubro'],25);
                        $max = max($vigencia,$fuente,$tercero,$cuentaContable,$concepto,$rubro);
                        $height = $intAltoHead*$max;
                        $pdf->MultiCell(8,$height,$data['id_egreso'],"",'L',$fill,0,'','',true,0,false,true,0,'M',true);
                        $pdf->MultiCell(15,$height,$data['fecha'],"",'L',$fill,0,'','',true,0,false,true,0,'M',true);
                        $pdf->MultiCell(8,$height,$data['id_orden'],"",'L',$fill,0,'','',true,0,false,true,0,'M',true);
                        $pdf->MultiCell(15,$height,$data['fecha'],"",'L',$fill,0,'','',true,0,false,true,0,'M',true);
                        $pdf->MultiCell(12,$height,$data['cuenta_contable'],"",'L',$fill,0,'','',true,0,false,true,0,'M',true);
                        $pdf->MultiCell(25,$height,$data['nombre_cuenta_contable'],"",'L',$fill,0,'','',true,0,false,true,0,'M',true);
                        $pdf->MultiCell(25,$height,$data['tercero'],"",'L',$fill,0,'','',true,0,false,true,0,'M',true);
                        $pdf->MultiCell(33,$height,$data['concepto'],"",'L',$fill,0,'','',true,0,false,true,0,'M',true);
                        $pdf->MultiCell(17.5,$height,formatNum($data['valor_bruto'],0,",",".",true),"",'R',$fill,0,'','',true,0,false,true,0,'M',true);
                        $pdf->MultiCell(17.5,$height,formatNum($data['valor_retencion'],0,",",".",true),"",'R',$fill,0,'','',true,0,false,true,0,'M',true);
                        $pdf->MultiCell(17.5,$height,formatNum($data['valor_neto'],0,",",".",true),"",'R',$fill,0,'','',true,0,false,true,0,'M',true);
                        $pdf->MultiCell(17.5,$height,$data['fuente'],"",'L',$fill,0,'','',true,0,false,true,0,'M',true);
                        $pdf->MultiCell(25,$height,$data['rubro'],"",'L',$fill,0,'','',true,0,false,true,0,'M',true);
                        $pdf->MultiCell(16,$height,$data['vigencia'],"",'L',$fill,0,'','',true,0,false,true,0,'M',true);
                        $pdf->MultiCell(8,$height,$data['medio_pago'],"",'L',$fill,0,'','',true,0,false,true,0,'M',true);
                        $pdf->MultiCell(17,$height,formatNum($data['valor'],0,",",".",true),"",'R',$fill,0,'','',true,0,false,true,0,'M',true);
                        $fill = !$fill;
                        $pdf->ln();
                        if($pdf->GetY()>130){
                            $pdf->AddPage();
                        }
                    }
                    $pdf->Output('reporte_de_egresos.pdf', 'I');
                }
            }
            die();
        }

    }
    class MYPDF extends TCPDF{
        public function Header(){
			$request = configBasica();
            $strNit = $request['nit'];
            $strRazon = $request['razonsocial'];
			//Parte Izquierda
			$this->Image('../../../imagenes/escudo.jpg',  13, 13, 20, 20, 'JPG', '', 'T', true, 300, '', false, false, 0, false, false, false);// Logo
			$this->SetFont('helvetica','B',8);
			$this->SetY(10);
			$this->RoundedRect(10, 10, 277, 25, 1,'');
			$this->Cell(0.1);
			$this->Cell(26,25,'','R',0,'L');
			$this->SetY(8);
			$this->SetX(40);
			$this->SetFont('helvetica','B',9);
			$this->Cell(242,15,strtoupper("$strRazon"),0,0,'C');
			$this->SetFont('helvetica','B',7);
			$this->SetY(12);
			$this->SetX(40);
			$this->Cell(242,15,'NIT: '.$strNit,0,0,'C');
			//*****************************************************************************************************************************
			$this->SetFont('helvetica','B',9);
			$this->SetY(23);
			$this->SetX(36);
            $this->Cell(251,12,"REPORTE DE EGRESOS",'T',0,'C');


            $this->SetFont('helvetica','B',7);
			$this->SetY(10);
			$this->SetX(257);
			$this->Cell(30,7," FECHA: ". date("d/m/Y"),"L",0,'L');
			$this->SetY(17);
			$this->SetX(257);
            $this->Cell(35,6,"","L",0,'L');

			//**********************************************************
			$this->SetFont('times','B',10);
			$this->ln(12);
			//**********************************************************
		}
		public function Footer(){
            $request = configBasica();
            $strDireccion = $request['direccion'] != "" ? "Dirección: ".strtoupper($request['direccion']) : "";
            $strWeb = $request['web'] != "" ? "Pagina web: ".strtoupper($request['web']) :"";
            $strEmail = $request['email'] !="" ? "Email: ".strtoupper($request['email']) :"";
            $strTelefono = $request['telefono'] != "" ? "Telefonos: ".$request['telefono'] : "";
            $strUsuario = searchUser($_SESSION['cedulausu'])['nom_usu'];
			$strNick = $_SESSION['nickusu'];
			$strFecha = date("d/m/Y H:i:s");
			$strIp = $_SERVER['REMOTE_ADDR'];

			$this->SetFont('helvetica', 'I', 8);
			$txt = <<<EOD
			$strDireccion $strTelefono
			$strEmail $strWeb
			EOD;
			$this->SetFont('helvetica', 'I', 6);
			$this->Cell(277,10,'','T',0,'T');
			$this->ln(2);
			$this->Write(0, $txt , '', 0, 'C', true, 0, false, false, 0);

			$this->Cell(46, 10, 'Hecho por: '.$strUsuario, 00, false, 'C', 0, '', 0, false, 'T', 'M');
			$this->Cell(46, 10, 'Impreso por: '.$strNick, 0, false, 'C', 0, '', 0, false, 'T', 'M');
			$this->Cell(46, 10, 'IP: '.$strIp, 0, false, 'C',0, '', 0, false, 'T', 'M');
			$this->Cell(46, 10, 'Fecha: '.$strFecha, 0, false, 'C', 0, '', 0, false, 'T', 'M');
			$this->Cell(46, 10, 'IDEAL.10 S.A.S', 0, false, 'C', 0, '', 0, false, 'T', 'M');
            $this->Cell(46, 10, 'Pagina '.$this->getAliasNumPage().'/'.$this->getAliasNbPages(), 0, false, 'C', 0, '', 0, false, 'T', 'M');
		}
    }


    if($_POST){
        $obj = new ReporteEgresoExportController();
        if($_POST['action'] == "excel"){
            $obj->exportExcel();
        }else if($_POST['action'] == "pdf"){
            $obj->exportPdf();
        }
    }

?>
