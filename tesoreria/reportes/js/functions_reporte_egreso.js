const URL ='tesoreria/reportes/controllers/ReporteEgresoController.php';
const URLEXPORT = 'tesoreria/reportes/controllers/ReporteEgresoExportController.php';

var app = new Vue({
    el:"#myapp",
    data() {
        return {
            isLoading:false,
            isModalTercero:false,
            isModal:false,
            txtSearch:"",
            txtSearchTercero:"",
            txtResultados:0,
            txtResultadosTercero:0,
            txtEgreso:"",
            objTercero:{nombre:"",codigo:""},
            objFuente:{nombre:"",codigo:""},
            arrFuentes:[],
            arrFuentesCopy:[],
            arrTerceros:[],
            arrTercerosCopy:[],
            txtFechaInicial:new Date(new Date().getFullYear(),0,1).toISOString().split("T")[0],
            txtFechaFinal:new Date().toISOString().split("T")[0],
            selectGasto:"",
            arrData: []
        }
    },
    mounted() {
        this.getData();
    },
    methods: {
        getData: async function(){
            const formData = new FormData();
            formData.append("action","get");
            this.isLoading = true;
            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();
            this.isLoading = false;
            this.arrFuentes = objData.fuentes;
            this.arrFuentesCopy = objData.fuentes;
            this.arrTerceros = objData.terceros;
            this.arrTercerosCopy = objData.terceros;
            this.txtResultadosTercero = this.arrTercerosCopy.length;
            this.txtResultados = this.arrFuentesCopy.length;
        },
        search:function(type=""){
            let search = "";
            if(type == "modal")search = this.txtSearch.toLowerCase();
            if(type == "modal_tercero")search = this.txtSearchTercero.toLowerCase();
            if(type=="cod_tercero")search = this.objTercero.codigo;
            if(type=="cod_fuente")search = this.objFuente.codigo;

            if(type=="modal"){
                this.arrFuentesCopy = [...this.arrFuentes.filter(e=>e.codigo.toLowerCase().includes(search) || e.nombre.toLowerCase().includes(search))];
                this.txtResultados = this.arrFuentesCopy.length;
            }else if(type=="modal_tercero"){
                this.arrTercerosCopy = [...this.arrTerceros.filter(e=>e.codigo.toLowerCase().includes(search) || e.nombre.toLowerCase().includes(search))];
                this.txtResultadosTercero = this.arrTercerosCopy.length;
            }else if(type=="cod_tercero"){
                const data = [...this.arrTerceros.filter(e=>e.codigo == search)];
                this.objTercero = data.length > 0 ? JSON.parse(JSON.stringify(data[0])) : {codigo:"",nombre:""};
            }else if(type=="cod_fuente"){
                const data = [...this.arrFuentes.filter(e=>e.codigo == search)];
                this.objFuente = data.length > 0 ? JSON.parse(JSON.stringify(data[0])) : {codigo:"",nombre:""};
            }
        },
        generate: async function(){
            const formData = new FormData();
            formData.append("action","gen");
            formData.append("fecha_inicial",this.txtFechaInicial);
            formData.append("fecha_final",this.txtFechaFinal);
            formData.append("egreso",this.txtEgreso);
            formData.append("tercero",this.objTercero.codigo);
            formData.append("fuente",this.objFuente.codigo);
            this.isLoading = true;
            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();
            this.isLoading = false;
            this.arrData = objData;
        },
        exportData:function(type){
            if(app.arrData.length == 0){
                Swal.fire("Atención!","Debe generar el reporte para generar el excel.","warning");
                return false;
            }
            const form = document.createElement("form");
            form.method ="post";
            form.target="_blank";
            form.action= URLEXPORT;

            function addField(name,value){
                const input = document.createElement("input");
                input.type="hidden";
                input.name=name;
                input.value = value;
                form.appendChild(input);
            }

            addField("fecha_inicial",this.txtFechaInicial);
            addField("fecha_final",this.txtFechaFinal);
            addField("fuente",this.objFuente.codigo+"-"+this.objFuente.nombre);
            addField("tercero",this.objTercero.codigo+"-"+this.objTercero.nombre);
            addField("egreso",this.txtEgreso);
            addField("action",type == 1 ? "pdf" : "excel");
            addField("data",JSON.stringify(this.arrData));
            document.body.appendChild(form);
            form.submit();
            document.body.removeChild(form);
        },
        formatNum: function(valor){
            return new Intl.NumberFormat("en-US", {style: "currency", currency: "USD"}).format(valor);
        },
        selectItem:function ({...data},type="tercero"){
            if(type =="tercero"){
                this.objTercero = data;
                this.isModalTercero = false;
            }else if(type=="fuente"){
                this.objFuente = data;
                this.isModal = false;
            }
        }
    },
})
