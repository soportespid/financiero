<?php
    require_once '../../../Librerias/core/Helpers.php';
    require_once '../../../Librerias/core/Mysql.php';
    session_start();
    /*ini_set('display_errors', '1');
    ini_set('display_startup_errors', '1');
    error_reporting(E_ALL);*/
    class ReporteEgresoModel extends Mysql{
        private $strFechaInicial;
        private $strFechaFinal;
        private $strTercero;
        private $intEgreso;
        private $strFuente;

        function __construct(){
            parent::__construct();
        }

        public function selectData(string $strFechaInicial,string $strFechaFinal,string $strTercero,string $intEgreso,string $strFuente){
            $this->strFechaInicial = $strFechaInicial;
            $this->strFechaFinal = $strFechaFinal;
            $this->strTercero = $strTercero;
            $this->intEgreso = $intEgreso;
            $this->strFuente = $strFuente;
            $sql = "SELECT cab.id_egreso,
            cab.id_orden,
            DATE_FORMAT(cab.fecha,'%d/%m/%Y') as fecha,
            cab.cuentabanco as cuenta_contable,
            cab.tercero,
            cab.valortotal as valor_bruto,
            cab.retenciones as valor_retencion,
            cab.valorpago as valor_neto,
            cab.concepto,
            det.cuentap as cuenta_presupuestal,
            det.bpim,
            det.productoservicio,
            det.indicador_producto,
            det.medio_pago,
            det.codigo_vigenciag as vigencia,
            det.valor,
            det.fuente,
            pro.nombre as nombre_proyecto,
            CASE WHEN t.razonsocial IS NULL OR t.razonsocial = ''
                THEN CONCAT(t.nombre1,' ',t.nombre2,' ',t.apellido1,' ',t.apellido2)
                ELSE t.razonsocial END AS nombre_tercero,
            CASE WHEN tb.razonsocial IS NULL OR tb.razonsocial = ''
                THEN CONCAT(t.nombre1,' ',tb.nombre2,' ',tb.apellido1,' ',tb.apellido2)
                ELSE tb.razonsocial END AS nombre_cuenta_contable
            FROM tesoegresos cab
            INNER JOIN  tesoordenpago_det det ON cab.id_orden =det.id_orden
            LEFT JOIN ccpproyectospresupuesto pro ON pro.codigo = det.bpim
            LEFT JOIN terceros t ON cab.tercero = t.cedulanit
            LEFT JOIN tesobancosctas cta ON cta.ncuentaban = cab.cuentabanco
            LEFT JOIN terceros tb ON cta.tercero = tb.cedulanit
            WHERE det.tipo_mov='201' AND cab.fecha BETWEEN '$this->strFechaInicial' AND '$this->strFechaFinal'
            AND det.vigencia BETWEEN YEAR('$this->strFechaInicial') AND YEAR('$this->strFechaFinal') AND det.estado = 'S' AND cab.estado = 'S'
            AND cab.tipo_mov='201' AND det.fuente like '$this->strFuente%' AND cab.tercero like '$this->strTercero%'
            AND cab.id_egreso like '$this->intEgreso%'
            GROUP BY cab.id_egreso, cab.banco, det.cuentap, det.fuente, det.medio_pago, det.codigo_vigenciag,
            det.bpim,det.indicador_producto
            ORDER BY cab.id_egreso DESC";
            $request = $this->select_all($sql);
            return $request;
        }
        public function selectFuente(string $fuente){
            $this->strFuente = $fuente;
            $sql="SELECT codigo_fuente as codigo, nombre
            FROM ccpet_fuentes_cuipo
            WHERE version = '1' AND codigo_fuente = '$this->strFuente'";
            $request = $this->select($sql);
            return $request;
        }
        public function selectCuenta(string $cuenta){
            $max = $this->select("SELECT MAX(version) as version FROM cuentasccpet")['version'];
            $sql="SELECT codigo, nombre
            FROM cuentasccpet
            WHERE version = $max AND codigo = '$cuenta'";
            $request = $this->select($sql);
            return $request;
        }
        public function selectFuentes(){
            $sql="SELECT codigo_fuente as codigo, nombre
            FROM ccpet_fuentes_cuipo
            WHERE version = '1' ORDER BY codigo_fuente ASC";
            $request = $this->select_all($sql);
            if(!empty($request)){
                $total = count($request);
                for ($i=0; $i < $total; $i++) {
                    $request[$i]['is_checked'] = 1;
                }
            }
            return $request;
        }
        public function selectTerceros(){
            $sql="SELECT cedulanit as codigo,
            CASE WHEN razonsocial IS NULL OR razonsocial = ''
            THEN CONCAT(nombre1,' ',nombre2,' ',apellido1,' ',apellido2)
            ELSE razonsocial END AS nombre FROM terceros ORDER BY id_tercero";
            $request = $this->select_all($sql);
            return $request;
        }
    }
?>
