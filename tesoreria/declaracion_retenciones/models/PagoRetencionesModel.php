<?php
    require_once '../../../Librerias/core/Helpers.php';
    require_once '../../../Librerias/core/Mysql.php';
    session_start();
    /*ini_set('display_errors', '1');
    ini_set('display_startup_errors', '1');
    error_reporting(E_ALL);*/
    class PagoRetencionesModel extends Mysql{

        function __construct(){
            parent::__construct();
        }


        public function selectPagoTerceros($strFechaInicial,$strFechaFinal,$data,$sigla){
            $arrData = [];
            $sql = "SELECT tesopagoterceros_det.movimiento,
            tesopagoterceros_det.valor,
            tesopagoterceros.banco,
            tesopagoterceros.id_pago,
            tesopagoterceros.tercero,
            tesopagoterceros.concepto
            FROM tesopagoterceros_det,
            tesopagoterceros
            WHERE tesopagoterceros.id_pago=tesopagoterceros_det.id_pago
            AND tesopagoterceros.estado='S'
            AND tesopagoterceros_det.tipo='$sigla'
            AND tesopagoterceros.fecha BETWEEN '$strFechaInicial' AND '$strFechaFinal'
            AND tesopagoterceros_det.movimiento='$data[id]'
            ORDER BY tesopagoterceros.id_pago";
            $request = $this->select_all($sql);
            $total = count($request);

            for ($i=0; $i < $total ; $i++) {
                $element = [];
                $arrOrden = $request[$i];
                $element['nombre'] = $data['nombre'];
                $element['valor'] = $arrOrden['valor'];
                $element['consecutivo'] = $arrOrden['id_pago'];
                $element['documento'] = "Pago recaudo para tercero";
                $element['tercero'] = $arrOrden['tercero'];
                $element['nombre_tercero'] = getNombreTercero($arrOrden['tercero']);
                $element['descuentos'] = $arrOrden['movimiento'];
                $element['nombre_descuentos'] = getNombreIngreso($arrOrden['movimiento']);
                $element['concepto'] = $arrOrden['concepto'];
                array_push($arrData,$element);
            }

            return $arrData;
        }

        public function selectOtrosEgresos($strFechaInicial,$strFechaFinal,$data,$sigla){
            $arrData = [];

            $sql = "SELECT tesopagotercerosvigant_det.movimiento,
            tesopagotercerosvigant_det.valor,
            tesopagotercerosvigant.banco,
            tesopagotercerosvigant.id_pago,
            tesopagotercerosvigant.tercero,
            tesopagotercerosvigant.concepto
            FROM tesopagotercerosvigant_det,
            tesopagotercerosvigant
            WHERE tesopagotercerosvigant.id_pago=tesopagotercerosvigant_det.id_pago
            AND tesopagotercerosvigant.estado='S'
            AND tesopagotercerosvigant_det.tipo='$sigla'
            AND tesopagotercerosvigant.fecha BETWEEN '$strFechaInicial' AND '$strFechaFinal'
            AND tesopagotercerosvigant_det.movimiento='$data[id]'
            ORDER BY tesopagotercerosvigant.id_pago";
            $request = $this->select_all($sql);
            $total = count($request);

            for ($i=0; $i < $total ; $i++) {
                $element = [];
                $arrOrden = $request[$i];
                $element['nombre'] = $data['nombre'];
                $element['valor'] = $arrOrden['valor'];
                $element['consecutivo'] = $arrOrden['id_pago'];
                $element['documento'] = "Egreso no presupuestal";
                $element['tercero'] = $arrOrden['tercero'];
                $element['nombre_tercero'] = getNombreTercero($arrOrden['tercero']);
                $element['descuentos'] = $arrOrden['movimiento'];
                $element['nombre_descuentos'] = getNombreRetencion($arrOrden['movimiento']);
                $element['concepto'] = $arrOrden['concepto'];
                array_push($arrData,$element);
            }
            
            return $arrData;
                
        }
        
        
        public function selectData(){
            $arrRetenciones = $this->selectRetenciones();
            $arrIngresos = $this->selectIngresos();
            $arrData = array_merge($arrRetenciones,$arrIngresos);
            return $arrData;
        }
        public function selectRetenciones(){
            $sql = "SELECT id,codigo,nombre,tipo,destino FROM tesoretenciones WHERE estado = 'S' ORDER BY codigo";
            $request = $this->select_all($sql);
            $total = count($request);
            if($total > 0){
                for ($i=0; $i < $total ; $i++) {
                    $request[$i]['opcion_nombre'] = "Retención";
                    $request[$i]['sigla'] = "R";
                    $request[$i]['todo'] = "T";
                }
            }
            return $request;
        }
        public function selectIngresos(){
            $sql = "SELECT codigo,nombre,tipo,terceros as destino FROM tesoingresos WHERE estado = 'S' ORDER BY codigo";
            $request = $this->select_all($sql);
            $total = count($request);
            if($total > 0){
                for ($i=0; $i < $total ; $i++) {
                    $request[$i]['opcion_nombre'] = "Ingreso";
                    $request[$i]['sigla'] = "I";
                    $request[$i]['todo'] = "T";
                }
            }
            return $request;
        }
    }
?>
