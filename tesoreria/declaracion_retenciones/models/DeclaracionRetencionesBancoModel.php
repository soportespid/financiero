<?php
    require_once '../../../Librerias/core/Helpers.php';
    require_once '../../../Librerias/core/Mysql.php';
    session_start();
    /*ini_set('display_errors', '1');
    ini_set('display_startup_errors', '1');
    error_reporting(E_ALL);*/
    class DeclaracionRetencionesBancoModel extends Mysql{

        function __construct(){
            parent::__construct();
        }

        public function selectRecaudosTransferencias($strFechaInicial,$strFechaFinal,$data){
            $arrData = [];
            $sql="SELECT DISTINCT td.ingreso,
            sum(td.valor) as valor,
            tr.banco as tercero_banco,
            tr.ncuentaban as cuenta_banco,
            tr.valortotal,
            tr.id_recaudo,
            tr.tercero
            FROM tesorecaudotransferencia_det td
            INNER JOIN tesorecaudotransferencia tr ON td.id_recaudo=tr.id_recaudo
            WHERE tr.estado='S' AND tr.fecha BETWEEN '$strFechaInicial' AND '$strFechaFinal'
            AND td.ingreso='$data[codigo]' GROUP BY td.ingreso";
            $request = $this->select_all($sql);
            $total = count($request);
            for ($i=0; $i < $total; $i++) {
                $element = [];
                $arrRecaudo = $request[$i];
                $sql_ingreso="SELECT * FROM  tesoingresos_det WHERE codigo='$arrRecaudo[ingreso]'
                AND vigencia=(SELECT MAX(vigencia) FROM tesoingresos_det WHERE codigo = '$arrRecaudo[ingreso]')";
                $request_ingreso = $this->select_all($sql_ingreso);
                $totalRecaudo = count($request_ingreso);
                for ($j=0; $j < $totalRecaudo ; $j++) {
                    $arrIngreso = $request_ingreso[$j];
                    $sql_concepto = "SELECT * FROM conceptoscontables_det WHERE codigo='$arrIngreso[concepto]'
                    AND modulo='4' AND tipo='C' AND fechainicial < '$strFechaFinal' AND cuenta LIKE '2%' ORDER BY fechainicial DESC";
                    $arrConcepto = $this->select_all($sql_concepto);
                    if(!empty($arrConcepto['cuenta'])){
                        $porcentaje = $arrIngreso['porcentaje'];
                        $element['nombre'] = $data['nombre'];
                        $element['valor'] = $arrRecaudo['valor']*($porcentaje/100);
                        $element['cuentas'] = $arrRecaudo['valor'];
                        $element['bancos'] = $arrRecaudo['id_recibos'];
                        $element['cuenta_bancos'] = $arrRecaudo['id_recaudo'];
                        $element['tercero'] = $arrRecaudo['tercero'];
                        $element['nombre_tercero'] = $arrRecaudo['tercero'] != "" ? getNombreTercero($arrRecaudo['tercero']) : "";
                        $element['descuentos'] = $arrRecaudo['ingreso'];
                        $element['nombre_descuentos'] = getNombreIngreso($arrRecaudo['ingreso']);
                        $element['cuenta_banco'] = $arrRecaudo['cuenta_banco'];
                        $element['tercero_banco'] = $arrRecaudo['tercero_banco'] !="" ? getNombreTercero($arrRecaudo['tercero_banco']) : $arrRecaudo['tercero_banco'];
                        array_push($arrData,$element);
                    }
                }
            }
            return $arrData;
        }
        public function selectRecaudosPropios($strFechaInicial,$strFechaFinal,$data){
            $arrData = [];
            $sql="SELECT DISTINCT td.ingreso,
            sum(td.valor) as valor,
            trc.cuentabanco,
            trc.cuentacaja,
            trc.id_recibos,
            tr.tercero,
            tc.ncuentaban as cuenta_banco,
            tc.tercero as tercero_banco
            FROM tesosinreciboscaja_det td
            INNER JOIN tesosinreciboscaja trc ON td.id_recibos=trc.id_recibos
            LEFT JOIN tesosinrecaudos tr ON trc.id_recaudo=tr.id_recaudo
            LEFT JOIN tesobancosctas as tc ON tc.cuenta = trc.cuentabanco
            WHERE trc.estado='S' AND trc.fecha BETWEEN '$strFechaInicial' AND '$strFechaFinal'
            AND td.ingreso='$data[codigo]' GROUP BY trc.id_recibos";
            $request = $this->select_all($sql);
            $total = count($request);
            for ($i=0; $i < $total; $i++) {
                $element = [];
                $arrRecaudo = $request[$i];
                $sql_ingreso="SELECT * FROM  tesoingresos_det WHERE codigo='$arrRecaudo[ingreso]'
                AND vigencia=(SELECT MAX(vigencia) FROM tesoingresos_det WHERE codigo = '$arrRecaudo[ingreso]')";
                $request_ingreso = $this->select_all($sql_ingreso);
                $totalRecaudo = count($request_ingreso);
                for ($j=0; $j < $totalRecaudo ; $j++) {
                    $arrIngreso = $request_ingreso[$j];
                    $sql_concepto = "SELECT * FROM conceptoscontables_det WHERE codigo='$arrIngreso[concepto]'
                    AND modulo='4' AND tipo='C' AND fechainicial < '$strFechaFinal' AND (cuenta LIKE '4%' OR cuenta LIKE '2%'
                    OR cuenta LIKE '4%' OR cuenta LIKE '3%' OR cuenta LIKE '1%')
                    ORDER BY fechainicial DESC";
                    $arrConcepto = $this->select_all($sql_concepto);
                    if(!empty($arrConcepto)){
                        $porcentaje = $arrIngreso['porcentaje'];
                        $element['nombre'] = $data['nombre'];
                        $element['valor'] = $arrRecaudo['valor']*($porcentaje/100);
                        $element['cuentas'] = $arrRecaudo['valor'];
                        $element['bancos'] = $arrRecaudo['id_recibos'];
                        $element['cuenta_bancos'] = $arrRecaudo['id_recaudo'];
                        $element['tercero'] = $arrRecaudo['tercero'];
                        $element['nombre_tercero'] = $arrRecaudo['tercero'] !="" ? getNombreTercero($arrRecaudo['tercero']) : $arrRecaudo['tercero'];
                        $element['descuentos'] = $arrRecaudo['ingreso'];
                        $element['nombre_descuentos'] = getNombreIngreso($arrRecaudo['ingreso']);
                        $element['cuenta_banco'] = $arrRecaudo['cuenta_banco'];
                        $element['tercero_banco'] = $arrRecaudo['tercero_banco'] != "" ? getNombreTercero($arrRecaudo['tercero_banco']) : "";
                        array_push($arrData,$element);
                        break;
                    }
                }
            }
            return $arrData;
        }
        public function selectRecaudos($strFechaInicial,$strFechaFinal,$data){
            $arrData = [];
            $sql="SELECT trd.ingreso,
            trd.valor,
            trc.cuentabanco,
            trc.cuentacaja,
            trc.id_recibos,
            tr.id_recaudo,
            tr.tercero,
            tc.ncuentaban as cuenta_banco,
            tc.tercero as tercero_banco
            FROM tesorecaudos tr
            INNER JOIN tesorecaudos_det trd ON tr.id_recaudo=trd.id_recaudo
            LEFT JOIN tesoreciboscaja trc ON tr.id_recaudo=trc.id_recaudo
            LEFT JOIN tesobancosctas as tc ON tc.cuenta = trc.cuentabanco
            WHERE tr.estado='P' AND trc.fecha BETWEEN '$strFechaInicial' AND '$strFechaFinal'
            AND trd.ingreso='$data[codigo]' AND trc.tipo=3";
            $request = $this->select_all($sql);
            $total = count($request);
            for ($i=0; $i < $total; $i++) {
                $element = [];
                $arrRecaudo = $request[$i];
                $sql_ingreso="SELECT * FROM  tesoingresos_det WHERE codigo='$arrRecaudo[ingreso]'
                AND vigencia=(SELECT MAX(vigencia) FROM tesoingresos_det WHERE codigo = '$arrRecaudo[ingreso]')";
                $request_ingreso = $this->select_all($sql_ingreso);
                $totalRecaudo = count($request_ingreso);
                for ($j=0; $j < $totalRecaudo ; $j++) {
                    $arrIngreso = $request_ingreso[$j];
                    $sql_concepto = "SELECT * FROM conceptoscontables_det WHERE codigo='$arrIngreso[concepto]'
                    AND modulo='4' AND tipo='C' AND fechainicial < '$strFechaFinal' AND cuenta LIKE '2%' ORDER BY fechainicial DESC";
                    $arrConcepto = $this->select_all($sql_concepto);
                    if(!empty($arrConcepto['cuenta'])){
                        $porcentaje = $arrIngreso['porcentaje'];
                        $element['nombre'] = $data['nombre'];
                        $element['valor'] = $arrRecaudo['valor']*($porcentaje/100);
                        $element['cuentas'] = $arrRecaudo['valor'];
                        $element['bancos'] = $arrRecaudo['id_recibos'];
                        $element['cuenta_bancos'] = $arrRecaudo['id_recaudo'];
                        $element['tercero'] = $arrRecaudo['tercero'];
                        $element['nombre_tercero'] = $arrRecaudo['tercero'] != "" ? getNombreTercero($arrRecaudo['tercero']) : "";
                        $element['cuenta_banco'] = $arrRecaudo['cuenta_banco'];
                        $element['tercero_banco'] = $arrRecaudo['tercero_banco'] != "" ? getNombreTercero($arrRecaudo['tercero_banco']) : "";
                        $element['descuentos'] = $arrRecaudo['ingreso'];
                        $element['nombre_descuentos'] = getNombreIngreso($arrRecaudo['ingreso']);
                        array_push($arrData,$element);
                    }
                }
            }
            return $arrData;
        }
        public function selectPagoTerceros($strFechaInicial,$strFechaFinal,$data,$strDestino){
            $arrData = [];
            $sql="SELECT tr.id_retencion,
            tr.valor,
            tp.banco,
            tp.id_pago,
            tr.valor,
            tp.tercero,
            tc.ncuentaban as cuenta_banco,
            tc.tercero as tercero_banco
            FROM tesopagotercerosvigant_retenciones AS tr
            INNER JOIN tesopagotercerosvigant AS tp ON tp.id_pago=tr.id_egreso
            LEFT JOIN tesobancosctas as tc ON tc.cuenta = tp.banco
            WHERE  tp.estado='S' AND tp.fecha BETWEEN '$strFechaInicial' AND '$strFechaFinal'
            AND tr.id_retencion='$data[id]' ORDER BY tp.id_pago";
            $request = $this->select_all($sql);
            $condicion = $data['tipo'] == "C" ? "AND destino='$strDestino' AND conceptoingreso!= '-1'" : "";
            $total = count($request);
            for ($i=0; $i < $total ; $i++) {
                $element = [];
                $arrOrden = $request[$i];
                $sql_ret="SELECT * FROM tesoretenciones_det WHERE codigo='$arrOrden[id_retencion]' $condicion ORDER BY porcentaje DESC";
                $request_ret = $this->select_all($sql_ret);
                $totalRet = count($request_ret);
                for ($j=0; $j < $totalRet ; $j++) {
                    $arrRet = $request_ret[$j];
                    if($arrRet['tipoconce'] == 'C' && $arrRet['destino'] != $strDestino){
                        continue;
                    }
                    $porcentaje = $arrRet['porcentaje'];
                    $element['nombre'] = $data['nombre'];
                    $element['valor'] = $arrOrden['valor'];
                    $element['cuentas'] = $arrOrden['base'];
                    $element['bancos'] = $arrOrden['id_pago'];
                    $element['cuenta_bancos'] = "CXP";
                    $element['cuenta_banco'] = $arrOrden['cuenta_banco'];
                    $element['tercero_banco'] = $arrOrden['tercero_banco'] != "" ? getNombreTercero($arrOrden['tercero_banco']) : "";
                    $element['tercero'] = $arrOrden['tercero'];
                    $element['nombre_tercero'] = $arrOrden['tercero'] != "" ? getNombreTercero($arrOrden['tercero']) : "";
                    $element['descuentos'] = $arrOrden['id_retencion'];
                    $element['nombre_descuentos'] = getNombreRetencion($arrOrden['id_retencion']);
                    if($data['tipo'] == "C"){
                        if($porcentaje <= 100){
                            $element['valor'] = $arrOrden['valor']*($porcentaje/100);
                        }
                    }
                    array_push($arrData,$element);
                }
            }
            return $arrData;
        }
        public function selectOrdenPago($strFechaInicial,$strFechaFinal,$data,$strDestino,$parametros){
            $arrData = [];
            if($parametros == 1){
                $sql="SELECT DISTINCT tr.id_retencion,
                tr.valor,
                tp.base,
                tp.iva,
                tp.id_orden,
                tp.tercero,
                tc.tercero as tercero_banco,
                te.cuentabanco as cuenta_banco,
                te.banco
                FROM tesoordenpago as tp
                INNER JOIN tesoordenpago_retenciones as tr ON tp.id_orden=tr.id_orden
                LEFT JOIN tesoegresos as te ON tr.id_orden = te.id_orden
                LEFT JOIN tesobancosctas as tc ON tc.ncuentaban = te.cuentabanco
                WHERE tp.fecha BETWEEN '$strFechaInicial' AND '$strFechaFinal' AND tr.id_retencion='$data[id]'
                AND tp.estado!='R' AND tp.estado!='N' AND tp.tipo_mov='201'  ORDER BY tp.id_orden";

            }else{
                $sql="SELECT tr.id_retencion,
                tr.valor,
                te.banco,
                te.cuentabanco as cuenta_banco,
                te.id_egreso,
                tp.base,
                tp.iva,
                tp.id_orden,
                te.tercero,
                tc.tercero as tercero_banco
                FROM tesoordenpago AS tp
                INNER JOIN tesoordenpago_retenciones as tr ON tp.id_orden=tr.id_orden
                LEFT JOIN tesoegresos as te ON tp.id_orden = te.id_orden
                LEFT JOIN tesobancosctas as tc ON tc.ncuentaban = te.cuentabanco
                WHERE tp.estado!='N' AND te.fecha BETWEEN '$strFechaInicial' AND '$strFechaFinal'
                AND tr.id_retencion='$data[id]' AND te.estado='S' AND te.tipo_mov='201' AND tp.tipo_mov='201' ORDER BY te.id_egreso";
            }
            $request = $this->select_all($sql);
            $total = count($request);
            for ($i=0; $i < $total ; $i++) {
                $element = [];
                $arrOrden = $request[$i];
                if($parametros == 1){
                    $condicion = $data['tipo'] == "C" ? "AND destino='$strDestino' AND conceptoingreso!= '-1'" : "";
                    $sql_ret="SELECT * FROM tesoretenciones_det WHERE codigo='$arrOrden[id_retencion]' $condicion ORDER BY porcentaje DESC";
                }else{
                    $condicion = $data['tipo'] == "C" ? "AND tesoretenciones_det.destino='$strDestino'" : "";
                    $sql_ret="SELECT * FROM tesoretenciones, tesoretenciones_det  WHERE tesoretenciones_det.codigo='$arrOrden[id_retencion]'
                    AND tesoretenciones.id=tesoretenciones_det.codigo $condicion ORDER BY porcentaje DESC";
                }

                $request_ret = $this->select_all($sql_ret);
                $totalRet = count($request_ret);
                for ($j=0; $j < $totalRet ; $j++) {
                    $arrRet = $request_ret[$j];
                    if($parametros !=1 && $arrRet['tipo'] == 'C' && $arrRet['destino'] != $strDestino){
                        continue;
                    }
                    $porcentaje = $arrRet['porcentaje'];
                    $element['nombre'] = $data['nombre'];
                    $element['valor'] = $arrOrden['valor'];
                    $element['cuentas'] = $arrOrden['base'];
                    $element['bancos'] = $arrOrden['id_orden'];
                    $element['cuenta_bancos'] = "CXP";
                    $element['tercero'] = $arrOrden['tercero'];
                    $element['cuenta_banco'] = $arrOrden['cuenta_banco'];
                    $element['tercero_banco'] = $arrOrden['tercero_banco'] != "" ? getNombreTercero($arrOrden['tercero_banco']) :"";
                    $element['nombre_tercero'] = $arrOrden['tercero'] != "" ? getNombreTercero($arrOrden['tercero']) : "";
                    $element['descuentos'] = $arrOrden['id_retencion'];
                    $element['nombre_descuentos'] = getNombreRetencion($arrOrden['id_retencion']);
                    if($data['tipo'] == "C"){
                        if($porcentaje <= 100){
                            $element['valor'] = $arrOrden['valor']*($porcentaje/100);
                        }
                    }
                    array_push($arrData,$element);
                }
            }
            return $arrData;
        }
        public function selectRete34($strFechaInicial,$strFechaFinal,$data){
            $arrData = [];
            $arrFechaInicial = explode("-",$strFechaInicial);
            $arrFechaFinal = explode("-",$strFechaFinal);
            $strFechaInicial = substr($arrFechaInicial[1],1,1);
            $strFechaFinal = substr($arrFechaFinal[1],1,1);
            $sql="SELECT COALESCE(SUM(valorretencion),0) as total,docfuncionario,nomfuncionario
            FROM hum_retencionesfun
            WHERE tiporetencion='34' AND mes BETWEEN '$strFechaInicial' AND '$strFechaFinal' AND estadopago='N' AND vigencia = $arrFechaFinal[0]";
            $request = $this->select($sql);
            $arrData['nombre'] = $data['nombre'];
            $arrData['valor'] = $request['total'];
            $arrData['cuentas'] = 0;
            $arrData['bancos'] = "";
            $arrData['cuenta_bancos'] = "EGRESO NOMINA";
            $arrData['tercero'] = $request['docfuncionario'];
            $arrData['nombre_tercero'] = $request['nomfuncionario'];
            $arrData['descuentos'] = 35;
            $arrData['nombre_descuentos'] = getNombreRetencion(34);
            return [$arrData];
        }
        public function selectParametros(){
            $sql="SELECT conta_pago FROM tesoparametros";
            $request = $this->select($sql)['conta_pago'];
            return $request;
        }
        public function selectData(){
            $arrRetenciones = $this->selectRetenciones();
            $arrIngresos = $this->selectIngresos();
            $arrData = array_merge($arrRetenciones,$arrIngresos);
            return $arrData;
        }
        public function selectRetenciones(){
            $sql = "SELECT id,codigo,nombre,tipo,destino FROM tesoretenciones WHERE estado = 'S' ORDER BY codigo";
            $request = $this->select_all($sql);
            $total = count($request);
            if($total > 0){
                for ($i=0; $i < $total ; $i++) {
                    $request[$i]['opcion_nombre'] = "Retención";
                    $request[$i]['sigla'] = "R";
                    $request[$i]['todo'] = "T";
                }
            }
            return $request;
        }
        public function selectIngresos(){
            $sql = "SELECT codigo,nombre,tipo,terceros as destino FROM tesoingresos WHERE estado = 'S' ORDER BY codigo";
            $request = $this->select_all($sql);
            $total = count($request);
            if($total > 0){
                for ($i=0; $i < $total ; $i++) {
                    $request[$i]['opcion_nombre'] = "Ingreso";
                    $request[$i]['sigla'] = "I";
                    $request[$i]['todo'] = "T";
                }
            }
            return $request;
        }
    }
?>
