<?php
    require_once '../../../Librerias/core/Helpers.php';
    require_once '../../../Librerias/core/Mysql.php';
    session_start();
    /*ini_set('display_errors', '1');
    ini_set('display_startup_errors', '1');
    error_reporting(E_ALL);*/
    class DeclaracionRetencionesModel extends Mysql{

        function __construct(){
            parent::__construct();
        }

        public function selectRecaudosTransferencias($strFechaInicial,$strFechaFinal,$data){
            $arrData = [];
            $sql="SELECT DISTINCT tesorecaudotransferencia_det.ingreso,
            sum(tesorecaudotransferencia_det.valor) as valor,
            tesorecaudotransferencia.banco,
            tesorecaudotransferencia.ncuentaban,
            tesorecaudotransferencia.valortotal,
            tesorecaudotransferencia.id_recaudo,
            tesorecaudotransferencia.tercero,
            tesorecaudotransferencia.concepto
            FROM tesorecaudotransferencia_det,tesorecaudotransferencia
            WHERE tesorecaudotransferencia.estado='S'
            AND tesorecaudotransferencia.fecha BETWEEN '$strFechaInicial' AND '$strFechaFinal'
            AND tesorecaudotransferencia_det.ingreso='$data[codigo]'
            AND tesorecaudotransferencia_det.id_recaudo=tesorecaudotransferencia.id_recaudo
            GROUP BY tesorecaudotransferencia_det.ingreso";
            $request = $this->select_all($sql);
            $total = count($request);
            for ($i=0; $i < $total; $i++) {
                $element = [];
                $arrRecaudo = $request[$i];
                $sql_ingreso="SELECT * FROM  tesoingresos_det WHERE codigo='$arrRecaudo[ingreso]'
                AND vigencia=(SELECT MAX(vigencia) FROM tesoingresos_det WHERE codigo = '$arrRecaudo[ingreso]')";
                $request_ingreso = $this->select_all($sql_ingreso);
                $totalRecaudo = count($request_ingreso);
                for ($j=0; $j < $totalRecaudo ; $j++) {
                    $arrIngreso = $request_ingreso[$j];
                    $sql_concepto = "SELECT * FROM conceptoscontables_det WHERE codigo='$arrIngreso[concepto]'
                    AND modulo='4' AND tipo='C' AND fechainicial < '$strFechaFinal' AND cuenta LIKE '2%' ORDER BY fechainicial DESC";
                    $arrConcepto =$this->select($sql_concepto);
                    if(!empty($arrConcepto['cuenta'])){
                        $porcentaje = $arrIngreso['porcentaje'];
                        $element['nombre'] = $data['nombre'];
                        $element['valor'] = $arrRecaudo['valor']*($porcentaje/100);
                        $element['cuentas'] = $arrRecaudo['valor'];
                        $element['bancos'] = $arrRecaudo['id_recibos'];
                        $element['cuenta_bancos'] = $arrRecaudo['id_recaudo'];
                        $element['tercero'] = $arrRecaudo['tercero'];
                        $element['nombre_tercero'] = getNombreTercero($arrRecaudo['tercero']);
                        $element['descuentos'] = $arrRecaudo['ingreso'];
                        $element['nombre_descuentos'] = getNombreIngreso($arrRecaudo['ingreso']);
                        $element['concepto'] = $arrRecaudo['concepto'];
                        array_push($arrData,$element);
                    }
                }
            }
            return $arrData;
        }
        public function selectRecaudosPropios($strFechaInicial,$strFechaFinal,$data){
            $arrData = [];
            $sql="SELECT DISTINCT tesosinreciboscaja_det.ingreso,
            sum(tesosinreciboscaja_det.valor) as valor,
            tesosinreciboscaja.cuentabanco,
            tesosinreciboscaja.cuentacaja,
            tesosinreciboscaja.id_recibos,
            tesosinreciboscaja.id_recaudo,
            tesosinrecaudos.tercero,
            tesosinrecaudos.concepto
            FROM tesosinreciboscaja_det,tesosinreciboscaja,tesosinrecaudos
            WHERE tesosinreciboscaja.estado='S'
            AND tesosinreciboscaja.fecha BETWEEN '$strFechaInicial' AND '$strFechaFinal'
            AND tesosinreciboscaja_det.ingreso='$data[codigo]'
            AND tesosinreciboscaja_det.id_recibos=tesosinreciboscaja.id_recibos
            AND tesosinreciboscaja.id_recaudo=tesosinrecaudos.id_recaudo
            GROUP BY tesosinreciboscaja.id_recibos";
            $request = $this->select_all($sql);
            $total = count($request);
            for ($i=0; $i < $total; $i++) {
                $element = [];
                $arrRecaudo = $request[$i];
                $sql_ingreso="SELECT * FROM  tesoingresos_det WHERE codigo='$arrRecaudo[ingreso]'
                AND vigencia=(SELECT MAX(vigencia) FROM tesoingresos_det WHERE codigo = '$arrRecaudo[ingreso]')";
                $request_ingreso = $this->select_all($sql_ingreso);
                $totalRecaudo = count($request_ingreso);
                for ($j=0; $j < $totalRecaudo ; $j++) {
                    $arrIngreso = $request_ingreso[$j];
                    $sql_concepto = "SELECT * FROM conceptoscontables_det WHERE codigo='$arrIngreso[concepto]'
                    AND modulo='4' AND tipo='C' AND fechainicial < '$strFechaFinal' AND (cuenta LIKE '4%' OR cuenta LIKE '2%'
                    OR cuenta LIKE '4%' OR cuenta LIKE '3%' OR cuenta LIKE '1%')
                    ORDER BY fechainicial DESC";
                    $arrConcepto = $this->select($sql_concepto);
                    if(!empty($arrConcepto)){
                        $porcentaje = $arrIngreso['porcentaje'];
                        $element['nombre'] = $data['nombre'];
                        $element['valor'] = $arrRecaudo['valor']*($porcentaje/100);
                        $element['cuentas'] = $arrRecaudo['valor'];
                        $element['bancos'] = $arrRecaudo['id_recibos'];
                        $element['cuenta_bancos'] = $arrRecaudo['id_recaudo'];
                        $element['tercero'] = $arrRecaudo['tercero'];
                        $element['nombre_tercero'] = getNombreTercero($arrRecaudo['tercero']);
                        $element['descuentos'] = $arrRecaudo['ingreso'];
                        $element['nombre_descuentos'] = getNombreIngreso($arrRecaudo['ingreso']);
                        $element['concepto'] = $arrRecaudo['concepto'];
                        array_push($arrData,$element);
                        break;
                    }
                }
            }
            return $arrData;
        }
        public function selectRecaudos($strFechaInicial,$strFechaFinal,$data){
            $arrData = [];
            $sql="SELECT tesorecaudos_det.ingreso,
            tesorecaudos_det.valor,
            tesoreciboscaja.cuentabanco,
            tesoreciboscaja.cuentacaja,
            tesoreciboscaja.id_recibos,
            tesorecaudos.id_recaudo,
            tesorecaudos.concepto,
            tesorecaudos.tercero FROM tesorecaudos, tesorecaudos_det,tesoreciboscaja
            WHERE tesorecaudos.estado='P'
            AND tesoreciboscaja.fecha BETWEEN '$strFechaInicial' AND '$strFechaFinal'
            AND tesorecaudos_det.ingreso='$data[codigo]'
            AND tesorecaudos.id_recaudo=tesorecaudos_det.id_recaudo
            AND tesorecaudos.id_recaudo=tesoreciboscaja.id_recaudo
            AND tesoreciboscaja.tipo=3";
            $request = $this->select_all($sql);
            $total = count($request);
            for ($i=0; $i < $total; $i++) {
                $element = [];
                $arrRecaudo = $request[$i];
                $sql_ingreso="SELECT * FROM  tesoingresos_det WHERE codigo='$arrRecaudo[ingreso]'
                AND vigencia=(SELECT MAX(vigencia) FROM tesoingresos_det WHERE codigo = '$arrRecaudo[ingreso]')";
                $request_ingreso =  $this->select_all($sql_ingreso);
                $totalRecaudo = count($request_ingreso);
                for ($j=0; $j < $totalRecaudo ; $j++) {
                    $arrIngreso = $request_ingreso[$j];
                    $sql_concepto = "SELECT * FROM conceptoscontables_det WHERE codigo='$arrIngreso[concepto]'
                    AND modulo='4' AND tipo='C' AND fechainicial < '$strFechaFinal' AND cuenta LIKE '2%' ORDER BY fechainicial DESC";
                    $arrConcepto = $this->select($sql_concepto);
                    if(!empty($arrConcepto['cuenta'])){
                        $porcentaje = $arrIngreso['porcentaje'];
                        $element['nombre'] = $data['nombre'];
                        $element['valor'] = $arrRecaudo['valor']*($porcentaje/100);
                        $element['cuentas'] = $arrRecaudo['valor'];
                        $element['bancos'] = $arrRecaudo['id_recibos'];
                        $element['cuenta_bancos'] = $arrRecaudo['id_recaudo'];
                        $element['tercero'] = $arrRecaudo['tercero'];
                        $element['nombre_tercero'] = getNombreTercero($arrRecaudo['tercero']);
                        $element['descuentos'] = $arrRecaudo['ingreso'];
                        $element['nombre_descuentos'] = getNombreIngreso($arrRecaudo['ingreso']);
                        $element['concepto'] = $arrRecaudo['concepto'];
                        array_push($arrData,$element);
                    }
                }
            }
            return $arrData;
        }
        public function selectPagoTerceros($strFechaInicial,$strFechaFinal,$data,$strDestino){
            $arrData = [];
            $sql="SELECT tesopagotercerosvigant_retenciones.id_retencion,
            tesopagotercerosvigant_retenciones.valor,
            tesopagotercerosvigant.banco,
            tesopagotercerosvigant.id_pago,
            tesopagotercerosvigant_retenciones.valor,
            tesopagotercerosvigant.tercero,
            tesopagotercerosvigant.concepto
            FROM tesopagotercerosvigant_retenciones,
            tesopagotercerosvigant
            WHERE tesopagotercerosvigant.id_pago=tesopagotercerosvigant_retenciones.id_egreso
            AND tesopagotercerosvigant.estado='S'
            AND tesopagotercerosvigant.fecha BETWEEN '$strFechaInicial' AND '$strFechaFinal'
            AND tesopagotercerosvigant_retenciones.id_retencion='$data[id]'
            ORDER BY tesopagotercerosvigant.id_pago";
            $request = $this->select_all($sql);
            $condicion = $data['tipo'] == "C" ? "AND destino='$strDestino' AND conceptoingreso!= '-1'" : "";
            $total = count($request);
            for ($i=0; $i < $total ; $i++) {
                $element = [];
                $arrOrden = $request[$i];
                $sql_ret="SELECT * FROM tesoretenciones_det WHERE codigo='$arrOrden[id_retencion]' $condicion ORDER BY porcentaje DESC";
                $request_ret = $this->select_all($sql_ret);
                $totalRet = count($request_ret);
                for ($j=0; $j < $totalRet ; $j++) {
                    $arrRet = $request_ret[$j];
                    if($arrRet['tipo'] == 'C' && $arrRet['destino'] != $strDestino){
                        continue;
                    }
                    $porcentaje = $arrRet['porcentaje'];
                    $element['nombre'] = $data['nombre'];
                    $element['valor'] = $arrOrden['valor'];
                    $element['cuentas'] = $arrOrden['base'];
                    $element['bancos'] = $arrOrden['id_orden'];
                    $element['cuenta_bancos'] = "CXP";
                    $element['tercero'] = $arrOrden['tercero'];
                    $element['nombre_tercero'] = getNombreTercero($arrOrden['tercero']);
                    $element['descuentos'] = $arrOrden['id_retencion'];
                    $element['nombre_descuentos'] = getNombreRetencion($arrOrden['id_retencion']);
                    $element['concepto'] = $arrOrden['concepto'];
                    if($data['tipo'] == "C"){
                        if($porcentaje <= 100){
                            $element['valor'] = $arrOrden['valor']*($porcentaje/100);
                        }
                    }
                    array_push($arrData,$element);
                }
            }
            return $arrData;
        }
        public function selectOrdenPago($strFechaInicial,$strFechaFinal,$data,$strDestino,$parametros){
            $arrData = [];
            if($parametros == 1){
                $sql="SELECT tesoordenpago_retenciones.id_retencion,
                tesoordenpago_retenciones.valor,
                tesoordenpago.base,
                tesoordenpago.iva,
                tesoordenpago.id_orden,
                tesoordenpago.tercero,
                tesoordenpago.conceptorden
                FROM tesoordenpago,
                tesoordenpago_retenciones
                WHERE tesoordenpago.fecha BETWEEN '$strFechaInicial' AND '$strFechaFinal'
                AND tesoordenpago_retenciones.id_retencion='$data[id]'
                AND tesoordenpago.id_orden=tesoordenpago_retenciones.id_orden
                AND tesoordenpago.estado!='R'
                AND tesoordenpago.estado!='N'
                AND tesoordenpago.tipo_mov='201'
                AND tesoordenpago_retenciones.valor > 0
                ORDER BY tesoordenpago.id_orden";
            }else{
                $sql="SELECT tesoordenpago_retenciones.id_retencion,
                tesoordenpago_retenciones.valor,
                tesoegresos.banco,
                tesoegresos.cuentabanco,
                tesoegresos.id_egreso,
                tesoordenpago.base,
                tesoordenpago.iva,
                tesoegresos.tercero,
                tesoordenpago.conceptorden
                FROM tesoordenpago, tesoordenpago_retenciones,tesoegresos
                WHERE tesoegresos.id_orden=tesoordenpago.id_orden
                AND tesoegresos.estado='S'
                AND tesoordenpago.estado!='N'
                AND tesoegresos.fecha BETWEEN '$strFechaInicial' AND '$strFechaFinal'
                AND tesoordenpago_retenciones.id_retencion='$data[id]'
                AND tesoordenpago.id_orden=tesoordenpago_retenciones.id_orden
                AND tesoegresos.estado='S'
                AND tesoegresos.tipo_mov='201'
                AND tesoordenpago_retenciones.valor > 0
                AND tesoordenpago.tipo_mov='201' ORDER BY tesoegresos.id_egreso";
            }
            $request = $this->select_all($sql);
            $total = count($request);
            for ($i=0; $i < $total ; $i++) {
                $element = [];
                $arrOrden = $request[$i];
                if($parametros == 1){
                    $condicion = $data['tipo'] == "C" ? "AND destino='$strDestino' AND conceptoingreso!= '-1'" : "";
                    $sql_ret="SELECT * FROM tesoretenciones_det WHERE codigo='$arrOrden[id_retencion]' $condicion ORDER BY porcentaje DESC";
                }else{
                    $condicion = $data['tipo'] == "C" ? "AND tesoretenciones_det.destino='$strDestino'" : "";
                    $sql_ret="SELECT * FROM tesoretenciones, tesoretenciones_det  WHERE tesoretenciones_det.codigo='$arrOrden[id_retencion]'
                    AND tesoretenciones.id=tesoretenciones_det.codigo $condicion ORDER BY porcentaje DESC";
                }

                $request_ret = $this->select_all($sql_ret);
                $totalRet = count($request_ret);
                for ($j=0; $j < $totalRet ; $j++) {
                    $arrRet = $request_ret[$j];
                    if($parametros !=1 && $arrRet['tipo'] == 'C' && $arrRet['destino'] != $strDestino){
                        continue;
                    }
                    $porcentaje = $arrRet['porcentaje'];
                    $element['nombre'] = $data['nombre'];
                    $element['valor'] = $arrOrden['valor'];
                    $element['cuentas'] = $arrOrden['base'];
                    $element['bancos'] = $arrOrden['id_orden'];
                    $element['cuenta_bancos'] = "CXP";
                    $element['tercero'] = $arrOrden['tercero'];
                    $element['nombre_tercero'] = getNombreTercero($arrOrden['tercero']);
                    $element['descuentos'] = $arrOrden['id_retencion'];
                    $element['nombre_descuentos'] = getNombreRetencion($arrOrden['id_retencion']);
                    $element['concepto'] = $arrOrden['conceptorden'];
                    if($data['tipo'] == "C"){
                        if($porcentaje <= 100){
                            $element['valor'] = $arrOrden['valor']*($porcentaje/100);
                        }
                    }
                    array_push($arrData,$element);
                }
            }
            return $arrData;
        }
        public function selectRete34($strFechaInicial,$strFechaFinal,$data){
            $arrData = [];
            $arrFechaInicial = explode("-",$strFechaInicial);
            $arrFechaFinal = explode("-",$strFechaFinal);
            $strFechaInicial = substr($arrFechaInicial[1],1,1);
            $strFechaFinal = substr($arrFechaFinal[1],1,1);
            $sql="SELECT COALESCE(SUM(valorretencion),0) as total,docfuncionario,nomfuncionario
            FROM hum_retencionesfun
            WHERE tiporetencion='34' AND mes BETWEEN '$strFechaInicial' AND '$strFechaFinal' AND estadopago='N' AND vigencia = $arrFechaFinal[0]";
            $request = $this->select($sql);
            $arrData['nombre'] = $data['nombre'];
            $arrData['valor'] = $request['total'];
            $arrData['cuentas'] = 0;
            $arrData['bancos'] = "";
            $arrData['cuenta_bancos'] = "EGRESO NOMINA";
            $arrData['tercero'] = $request['docfuncionario'];
            $arrData['nombre_tercero'] = $request['nomfuncionario'];
            $arrData['descuentos'] = 35;
            $arrData['nombre_descuentos'] = getNombreRetencion(34);
            return [$arrData];
        }
        public function selectParametros(){
            $sql="SELECT conta_pago FROM tesoparametros";
            $request = $this->select($sql)['conta_pago'];
            return $request;
        }
        public function selectData(){
            $arrRetenciones = $this->selectRetenciones();
            $arrIngresos = $this->selectIngresos();
            $arrData = array_merge($arrRetenciones,$arrIngresos);
            return $arrData;
        }
        public function selectRetenciones(){
            $sql = "SELECT id,codigo,nombre,tipo,destino FROM tesoretenciones WHERE estado = 'S' ORDER BY codigo";
            $request = $this->select_all($sql);
            $total = count($request);
            if($total > 0){
                for ($i=0; $i < $total ; $i++) {
                    $request[$i]['opcion_nombre'] = "Retención";
                    $request[$i]['sigla'] = "R";
                    $request[$i]['todo'] = "T";
                }
            }
            return $request;
        }
        public function selectIngresos(){
            $sql = "SELECT codigo,nombre,tipo,terceros as destino FROM tesoingresos WHERE estado = 'S' ORDER BY codigo";
            $request = $this->select_all($sql);
            $total = count($request);
            if($total > 0){
                for ($i=0; $i < $total ; $i++) {
                    $request[$i]['opcion_nombre'] = "Ingreso";
                    $request[$i]['sigla'] = "I";
                    $request[$i]['todo'] = "T";
                }
            }
            return $request;
        }
    }
?>
