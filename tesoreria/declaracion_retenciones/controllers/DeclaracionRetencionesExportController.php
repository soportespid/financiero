<?php
    require_once '../../../Librerias/core/Helpers.php';
    require_once '../../../PHPExcel/Classes/PHPExcel.php';
    require_once '../../../tcpdf/tcpdf_include.php';
    require_once '../../../tcpdf/tcpdf.php';
    session_start();
    /*ini_set('display_errors', '1');
    ini_set('display_startup_errors', '1');
    error_reporting(E_ALL);*/
    class DeclaracionRetencionesExportController {
        public function exportExcel(){
            if(!empty($_SESSION)){
                $arrData = json_decode($_POST['data'],true);
                if(!empty($arrData)){
                    $arrFechaInicial = explode("-",$_POST['fecha_inicial']);
                    $arrFechaFinal = explode("-",$_POST['fecha_final']);
                    $strFechaInicial = $arrFechaInicial[2]."/".$arrFechaInicial[1]."/".$arrFechaInicial[0];
                    $strFechaFinal = $arrFechaFinal[2]."/".$arrFechaFinal[1]."/".$arrFechaFinal[0];
                    $arrDet = $arrData['data'];

                    $objPHPExcel = new PHPExcel();
                    $objPHPExcel->getActiveSheet()->getStyle('A:J')->applyFromArray(
                        array(
                            'alignment' => array(
                                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
                                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
                            )
                        )
                    );
                    $objPHPExcel->getProperties()
                    ->setCreator("IDEAL 10")
                    ->setLastModifiedBy("IDEAL 10")
                    ->setTitle("Exportar Excel con PHP")
                    ->setSubject("Documento de prueba")
                    ->setDescription("Documento generado con PHPExcel")
                    ->setKeywords("usuarios phpexcel")
                    ->setCategory("reportes");

                    //----Cuerpo de Documento----
                    $objPHPExcel->setActiveSheetIndex(0)
                    ->mergeCells('A1:H1')
                    ->mergeCells('A2:H2')
                    ->mergeCells('B3:H3')
                    ->mergeCells('B4:H4')
                    ->mergeCells('B5:H5')
                    ->setCellValue('A1', 'TESORERÍA')
                    ->setCellValue('A2', 'REPORTE DECLARACIÓN RETENCIONES')
                    ->setCellValue('A3', 'FECHA')
                    ->setCellValue('A4', 'NETO A PAGAR')
                    ->setCellValue('A5', 'VALOR EN LETRAS')
                    ->setCellValue('B3', "Desde ".$strFechaInicial." Hasta ".$strFechaFinal)
                    ->setCellValue('B4', "$".number_format($arrData['total_valor'],0))
                    ->setCellValue('B5', $arrData['total_valor_letras']);
                    $objPHPExcel-> getActiveSheet ()
                    -> getStyle ("A3")
                    -> getFill ()
                    -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
                    -> getStartColor ()
                    -> setRGB ('3399cc');
                    $objPHPExcel-> getActiveSheet ()
                    -> getStyle ("A4")
                    -> getFill ()
                    -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
                    -> getStartColor ()
                    -> setRGB ('3399cc');
                    $objPHPExcel-> getActiveSheet ()
                    -> getStyle ("A5")
                    -> getFill ()
                    -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
                    -> getStartColor ()
                    -> setRGB ('3399cc');
                    $objPHPExcel-> getActiveSheet ()
                    -> getStyle ("A1")
                    -> getFill ()
                    -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
                    -> getStartColor ()
                    -> setRGB ('C8C8C8');
                    $objPHPExcel-> getActiveSheet ()
                    -> getStyle ("A1:A2")
                    -> getFont ()
                    -> setBold ( true )
                    -> setName ( 'Verdana' )
                    -> setSize ( 10 )
                    -> getColor ()
                    -> setRGB ('000000');
                    $objPHPExcel-> getActiveSheet ()
                    -> getStyle ('A1:A2')
                    -> getAlignment ()
                    -> applyFromArray (array ( 'horizontal'  =>  PHPExcel_Style_Alignment :: HORIZONTAL_CENTER ,) );
                    $objPHPExcel-> getActiveSheet ()
                    -> getStyle ('A4:j4')
                    -> getAlignment ()
                    -> applyFromArray (array ( 'horizontal'  =>  PHPExcel_Style_Alignment :: VERTICAL_JUSTIFY ,) );
                    $objPHPExcel-> getActiveSheet ()
                    -> getStyle ("A2")
                    -> getFill ()
                    -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID);

                    $borders = array(
                        'borders' => array(
                            'allborders' => array(
                                'style' => PHPExcel_Style_Border::BORDER_THIN,
                            'color' => array('argb' => 'FF000000'),
                            )
                        ),
                    );
                    $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A6', 'Retenciones/ingresos')
                    ->setCellValue('B6', "Tercero")
                    ->setCellValue('C6', "Nombre")
                    ->setCellValue('D6', "No. documento")
                    ->setCellValue('E6', "Tipo documento")
                    ->setCellValue('F6', "Concepto")
                    ->setCellValue('G6', "Base")
                    ->setCellValue('H6', "Valor");
                    $objPHPExcel-> getActiveSheet ()
                        -> getStyle ("A6:H6")
                        -> getFill ()
                        -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
                        -> getStartColor ()
                        -> setRGB ('99ddff');
                    $objPHPExcel->getActiveSheet()->getStyle("A6:H6")->getFont()->setBold(true);
                    $objPHPExcel->getActiveSheet()->getStyle('A1:H1')->applyFromArray($borders);
                    $objPHPExcel->getActiveSheet()->getStyle('A2:H2')->applyFromArray($borders);
                    $objPHPExcel->getActiveSheet()->getStyle('A3:H3')->applyFromArray($borders);
                    $objPHPExcel->getActiveSheet()->getStyle('A4:H4')->applyFromArray($borders);
                    $objPHPExcel->getActiveSheet()->getStyle('A5:H5')->applyFromArray($borders);
                    $objPHPExcel->getActiveSheet()->getStyle('A6:H6')->applyFromArray($borders);

                    $objPHPExcel->getActiveSheet()->getStyle("A3")->getFont()->getColor()->setRGB("ffffff");
                    $objPHPExcel->getActiveSheet()->getStyle("A4")->getFont()->getColor()->setRGB("ffffff");
                    $objPHPExcel->getActiveSheet()->getStyle("A5")->getFont()->getColor()->setRGB("ffffff");

                    $objPHPExcel->getActiveSheet()->getStyle("A3")->getFont()->setBold(true);
                    $objPHPExcel->getActiveSheet()->getStyle("A4")->getFont()->setBold(true);
                    $objPHPExcel->getActiveSheet()->getStyle("A5")->getFont()->setBold(true);

                    $retencion = !empty($arrDet) ? $arrDet[0]['nombre_descuentos'] : "";
                    $acumulador = 0;
                    $baseTotalRete = 0;
                    $cont = 0;
                    $row = 7;
                    $totalData = count($arrDet);
                    foreach ($arrDet as $data) {
                        if($retencion != $data['nombre_descuentos']){
                            $objPHPExcel->setActiveSheetIndex(0)
                            ->mergeCells("A$row:F$row")
                            ->setCellValue("A$row", "Total de retencion/ingreso ".$retencion)
                            ->setCellValue("G$row", $baseTotalRete)
                            ->setCellValue("H$row", $acumulador);
                            $acumulador = 0;
                            $baseTotalRete = 0;
                            $objPHPExcel-> getActiveSheet ()
                            -> getStyle ("A$row:H$row")
                            -> getFill ()
                            -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
                            -> getStartColor ()
                            -> setRGB ('99ddff');
                            $row++;
                        }

                        $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValueExplicit ("A$row", $data['nombre'], PHPExcel_Cell_DataType :: TYPE_STRING)
                        ->setCellValueExplicit ("B$row", $data['tercero'], PHPExcel_Cell_DataType :: TYPE_STRING)
                        ->setCellValueExplicit ("C$row", $data['nombre_tercero'], PHPExcel_Cell_DataType :: TYPE_STRING)
                        ->setCellValueExplicit ("D$row", $data['bancos'], PHPExcel_Cell_DataType :: TYPE_STRING)
                        ->setCellValueExplicit ("E$row", $data['cuenta_bancos'], PHPExcel_Cell_DataType :: TYPE_STRING)
                        ->setCellValueExplicit ("F$row", $data['concepto'], PHPExcel_Cell_DataType :: TYPE_STRING)
                        ->setCellValueExplicit ("G$row", $data['cuentas'], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
                        ->setCellValueExplicit ("H$row", $data['valor'], PHPExcel_Cell_DataType :: TYPE_NUMERIC);
                        $objPHPExcel->getActiveSheet()->getStyle("A$row:H$row")->applyFromArray($borders);
                        $row++;
                        $retencion = $data['nombre_descuentos'];
                        $acumulador += round($data['valor'],0);
                        $baseTotalRete += round($data['cuentas'],0);
                        $cont++;
                        if($cont == $totalData){
                            $objPHPExcel->setActiveSheetIndex(0)
                            ->mergeCells("A$row:F$row")
                            ->setCellValue("A$row", "Total de retencion/ingreso ".$retencion)
                            ->setCellValue("G$row", $baseTotalRete)
                            ->setCellValue("H$row", $acumulador);
                            $acumulador = 0;
                            $baseTotalRete = 0;
                            $objPHPExcel-> getActiveSheet ()
                            -> getStyle ("A$row:H$row")
                            -> getFill ()
                            -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
                            -> getStartColor ()
                            -> setRGB ('99ddff');
                            $row++;
                        }

                    }
                    $objPHPExcel->setActiveSheetIndex(0)
                    ->mergeCells("A$row:F$row")
                    ->setCellValue("A$row", "Total")
                    ->setCellValue("G$row", $arrData['total_cuentas'])
                    ->setCellValue("H$row", $arrData['total_valor']);
                    $objPHPExcel-> getActiveSheet ()
                    -> getStyle ("A$row:F$row")
                    -> getFill ()
                    -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
                    -> getStartColor ()
                    -> setRGB ('99ddff');

                    $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
                    $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
                    $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
                    $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
                    $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
                    $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
                    $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
                    $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);

                    //----Guardar documento----
                    header('Content-Type: application/vnd.ms-excel');
                    header('Content-Disposition: attachment;filename="reporte-declaracion_retenciones.xlsx"');
                    header('Cache-Control: max-age=0');
                    $objWriter=PHPExcel_IOFactory::createWriter($objPHPExcel,'Excel2007');
                    $objWriter->save('php://output');
                }
            }
            die();
        }
        public function exportPdf(){
            if(!empty($_SESSION)){
                $arrData = json_decode($_POST['data'],true);
                if(!empty($arrData)){
                    $arrFechaInicial = explode("-",$_POST['fecha_inicial']);
                    $arrFechaFinal = explode("-",$_POST['fecha_final']);
                    $strFechaInicial = $arrFechaInicial[2]."/".$arrFechaInicial[1]."/".$arrFechaInicial[0];
                    $strFechaFinal = $arrFechaFinal[2]."/".$arrFechaFinal[1]."/".$arrFechaFinal[0];
                    $arrDet = $arrData['data'];

                    $pdf = new MYPDF('P','mm','Letter', true, 'iso-8859-1', false);
                    $pdf->SetDocInfoUnicode (true);
                    // set document information
                    $pdf->SetCreator(PDF_CREATOR);
                    $pdf->SetAuthor('IDEALSAS');
                    $pdf->SetTitle('DECLARACIÓN RETENCIONES');
                    $pdf->SetSubject('DECLARACIÓN RETENCIONES');
                    $pdf->SetKeywords('TCPDF, PDF, example, test, guide');
                    $pdf->SetMargins(10, 38, 10);// set margins
                    $pdf->SetHeaderMargin(38);// set margins
                    $pdf->SetFooterMargin(17);// set margins
                    $pdf->SetAutoPageBreak(TRUE, 20);// set auto page breaks
                    // set some language-dependent strings (optional)
                    if (@file_exists(dirname(__FILE__).'/lang/spa.php'))
                    {
                        require_once(dirname(__FILE__).'/lang/spa.php');
                        $pdf->setLanguageArray($l);
                    }
                    $pdf->AddPage();
                    $pdf->SetFillColor(51, 153, 204);
                    $pdf->SetTextColor(255,255,255);
                    $pdf->SetFont('Helvetica','b',9);
                    $pdf->MultiCell(40,4,"Fecha","LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->SetFillColor(255,255,255);
                    $pdf->SetTextColor(0,0,0);
                    $pdf->SetFont('Helvetica','',9);
                    $pdf->MultiCell(150,4,"Desde ".$strFechaInicial." Hasta ".$strFechaFinal,"RBT",'L',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->ln();
                    $pdf->SetFillColor(51, 153, 204);
                    $pdf->SetTextColor(255,255,255);
                    $pdf->SetFont('Helvetica','b',9);
                    $pdf->MultiCell(40,4,"Neto a pagar","LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->SetFillColor(255,255,255);
                    $pdf->SetTextColor(0,0,0);
                    $pdf->SetFont('Helvetica','',9);
                    $pdf->MultiCell(150,4,"$".number_format($arrData['total_valor'],0),"RBT",'L',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->ln();
                    $pdf->SetFillColor(51, 153, 204);
                    $pdf->SetTextColor(255,255,255);
                    $pdf->SetFont('Helvetica','b',9);
                    $pdf->MultiCell(40,4,"Valor en letras","LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->SetFillColor(255,255,255);
                    $pdf->SetTextColor(0,0,0);
                    $pdf->SetFont('Helvetica','',9);
                    $pdf->MultiCell(150,4,$arrData['total_valor_letras'],"RBT",'L',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->ln();
                    $pdf->ln();
                    $pdf->SetFillColor(51, 153, 204);
                    $pdf->SetTextColor(255,255,255);
                    $pdf->SetFont('Helvetica','b',9);
                    $pdf->MultiCell(190,4,"Detalle","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->ln();
                    if(!empty($arrDet)){
                        $pdf->SetFillColor(153,221,255);
                        $pdf->SetFont('helvetica','B',6);
                        $pdf->SetTextColor(0,0,0);
                        $pdf->MultiCell(40,4,"Retenciones/Ingresos","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
                        $pdf->MultiCell(15,4,"Tercero","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
                        $pdf->MultiCell(40,4,"Nombre","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
                        $pdf->MultiCell(15,4,"No. documento","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
                        $pdf->MultiCell(15,4,"Tipo documento","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
                        $pdf->MultiCell(25,4,"Concepto","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
                        $pdf->MultiCell(20,4,"Base","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
                        $pdf->MultiCell(20,4,"Valor","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
                        $pdf->ln();
                        $fill = true;
                        $retencion = !empty($arrDet) ? $arrDet[0]['nombre_descuentos'] : "";
                        $acumulador = 0;
                        $baseTotalRete = 0;
                        $cont = 0;
                        $totalData = count($arrDet);
                        foreach ($arrDet as $data) {
                            if($retencion != $data['nombre_descuentos']){
                                $pdf->SetFillColor(153,221,255);
                                $pdf->SetFont('helvetica','B',6);
                                $pdf->SetTextColor(0,0,0);
                                $pdf->MultiCell(150,4,"Total de retencion/ingreso ".$retencion,"",'L',true,0,'','',true,0,false,true,0,'M',true);

                                $pdf->MultiCell(20,4,"$".number_format(round($baseTotalRete,0)),"",'R',true,0,'','',true,0,false,true,0,'M',true);
                                $pdf->MultiCell(20,4,"$".number_format(round($acumulador,0),0),"",'R',true,0,'','',true,0,false,true,0,'M',true);
                                $acumulador = 0;
                                $baseTotalRete = 0;
                                $pdf->ln();
                            }
                            $pdf->SetFillColor(245,245,245);
                            $pdf->SetFont('helvetica','',6);
                            $nombre = $pdf->getNumLines($data['nombre'],40);
                            $concepto = $pdf->getNumLines($data['concepto'],25);
                            $nombreTercero = $pdf->getNumLines($data['nombre_tercero'],40);
                            $max = max($nombre,$nombreTercero,$concepto);
                            $height = 3*$max;
                            $pdf->MultiCell(40,$height,$data['nombre'],"",'L',$fill,0,'','',true,0,false,true,0,'M',true);
                            $pdf->MultiCell(15,$height,$data['tercero'],"",'L',$fill,0,'','',true,0,false,true,0,'M',true);
                            $pdf->MultiCell(40,$height,$data['nombre_tercero'],"",'L',$fill,0,'','',true,0,false,true,0,'M',true);
                            $pdf->MultiCell(15,$height,$data['bancos'],"",'C',$fill,0,'','',true,0,false,true,0,'M',true);
                            $pdf->MultiCell(15,$height,$data['cuenta_bancos'],"",'C',$fill,0,'','',true,0,false,true,0,'M',true);
                            $pdf->MultiCell(25,$height,$data['concepto'],"",'L',$fill,0,'','',true,0,false,true,0,'M',true);
                            $pdf->MultiCell(20,$height,"$".number_format($data['cuentas']),"",'R',$fill,0,'','',true,0,false,true,0,'M',true);
                            $pdf->MultiCell(20,$height,"$".number_format($data['valor']),"",'R',$fill,0,'','',true,0,false,true,0,'M',true);
                            $pdf->ln();

                            $retencion = $data['nombre_descuentos'];
                            $acumulador += round($data['valor'],0);
                            $baseTotalRete += round($data['cuentas'],0);
                            $cont++;
                            if($cont == $totalData){
                                $pdf->SetFont('helvetica','B',6);
                                $pdf->SetFillColor(153,221,255);
                                $pdf->SetTextColor(0,0,0);
                                $pdf->MultiCell(150,4,"Total de retencion/ingreso ".$retencion,"",'L',true,0,'','',true,0,false,true,0,'M',true);

                                $pdf->MultiCell(20,4,"$".number_format(round($baseTotalRete,0)),"",'R',true,0,'','',true,0,false,true,0,'M',true);
                                $pdf->MultiCell(20,4,"$".number_format(round($acumulador,0),0),"",'R',true,0,'','',true,0,false,true,0,'M',true);
                                $acumulador = 0;
                                $baseTotalRete = 0;
                                $pdf->ln();
                            }
                            $fill = !$fill;
                            if($pdf->GetY()>230){
                                $pdf->AddPage();
                            }
                        }
                        $pdf->SetFillColor(153,221,255);
                        $pdf->SetFont('helvetica','B',6);
                        $pdf->SetTextColor(0,0,0);
                        $pdf->MultiCell(150,4,"Total","",'R',true,0,'','',true,0,false,true,0,'M',true);

                        $pdf->SetFillColor(255,255,255);
                        $pdf->SetTextColor(0,0,0);
                        $pdf->MultiCell(20,4,"$".number_format($arrData['total_cuentas']),"",'R',0,0,'','',true,0,false,true,0,'M',true);
                        $pdf->MultiCell(20,4,"$".number_format($arrData['total_valor']),"",'R',0,0,'','',true,0,false,true,0,'M',true);
                    }
                    $pdf->ln();
                    $pdf->ln();
                    //Campo para recibido y sello
                    $getY = $pdf->getY();
                    $pdf->setX(60);
                    $pdf->SetFont('helvetica','B',9);
                    $pdf->SetFillColor(51, 153, 204);
                    $pdf->SetTextColor(255,255,255);
                    $pdf->cell(95,4,'Elaboró','LRTB',0,'C',1);
                    $pdf->ln();
                    $pdf->setX(60);
                    $pdf->SetFont('helvetica','',7);
                    $pdf->SetTextColor(0,0,0);
                    $pdf->SetFillColor(255,255,255);
                    $pdf->cell(95,20,'','LRTB',0,'L',1);
                    $pdf->ln();
                    $pdf->setX(60);
                    $pdf->cell(95,4,"Nombre: ".strtoupper( iconv($_SESSION["VERCARPDFINI"], $_SESSION["VERCARPDFFIN"],$_SESSION['usuario'])),'LRTB',0,'L',1);
                    $pdf->ln();
                    $pdf->Output('declaracion_retenciones.pdf', 'I');
                }
            }
            die();
        }

    }
    class MYPDF extends TCPDF {

		public function Header()
		{
			$request = configBasica();
            $strNit = $request['nit'];
            $strRazon = $request['razonsocial'];

			//Parte Izquierda
			$this->Image('../../../imagenes/escudo.jpg',  13, 13, 20, 20, 'JPG', '', 'T', true, 300, '', false, false, 0, false, false, false);// Logo
			$this->SetFont('helvetica','B',8);
			$this->SetY(10);
			$this->RoundedRect(10, 10, 190, 25, 1,'');
			$this->Cell(0.1);
			$this->Cell(26,25,'','R',0,'L');
			$this->SetY(8);
			$this->SetX(40);
			$this->SetFont('helvetica','B',9);
			$this->Cell(140,15,strtoupper("$strRazon"),0,0,'C');
			$this->SetFont('helvetica','B',7);
			$this->SetY(12);
			$this->SetX(40);
			$this->Cell(140,15,'NIT: '.$strNit,0,0,'C');
			//*****************************************************************************************************************************
			$this->SetFont('helvetica','B',9);
			$this->SetY(23);
			$this->SetX(36);
            $this->Cell(164,12,"RETENCIONES E INGRESOS PARA TERCEROS",'T',0,'C');


            $this->SetFont('helvetica','B',7);
			$this->SetY(10);
			$this->SetX(167);
			$this->Cell(30,7," FECHA: ". date("d/m/Y"),"L",0,'L');
			$this->SetY(17);
			$this->SetX(167);
            $this->Cell(35,6,"","L",0,'L');

			//**********************************************************
			$this->SetFont('times','B',10);
			$this->ln(12);
			//**********************************************************
            if(MOV=="401"){
                $img_file = './assets/img/reversado.png';
                $this->Image($img_file, 0, 20, 250, 280, '', '', '', false, 300, '', false, false, 0);
            }
		}
		public function Footer(){

			$request = configBasica();
            $strDireccion = $request['direccion'] != "" ? "Dirección: ".strtoupper($request['direccion']) : "";
            $strWeb = $request['web'] != "" ? "Pagina web: ".strtoupper($request['web']) :"";
            $strEmail = $request['email'] !="" ? "Email: ".strtoupper($request['email']) :"";
            $strTelefono = $request['telefono'] != "" ? "Telefonos: ".$request['telefono'] : "";
            $strUsuario = searchUser($_SESSION['cedulausu'])['nom_usu'];
			$strNick = $_SESSION['nickusu'];
			$strFecha = date("d/m/Y H:i:s");
			$strIp = $_SERVER['REMOTE_ADDR'];

			$this->SetFont('helvetica', 'I', 8);
			$txt = <<<EOD
			$strDireccion $strTelefono
			$strEmail $strWeb
			EOD;
			$this->SetFont('helvetica', 'I', 6);
			$this->Cell(277,10,'','T',0,'T');
			$this->ln(2);
			$this->Write(0, $txt , '', 0, 'C', true, 0, false, false, 0);

			$this->Cell(50, 10, 'Hecho por: '.$strUsuario, 00, false, 'C', 0, '', 0, false, 'T', 'M');
			$this->Cell(30, 10, 'Impreso por: '.$strNick, 0, false, 'C', 0, '', 0, false, 'T', 'M');
			$this->Cell(30, 10, 'IP: '.$strIp, 0, false, 'C',0, '', 0, false, 'T', 'M');
			$this->Cell(30, 10, 'Fecha: '.$strFecha, 0, false, 'C', 0, '', 0, false, 'T', 'M');
			$this->Cell(30, 10, 'IDEAL.10 S.A.S', 0, false, 'C', 0, '', 0, false, 'T', 'M');
            $this->Cell(20, 10, 'Pagina '.$this->getAliasNumPage().'/'.$this->getAliasNbPages(), 0, false, 'C', 0, '', 0, false, 'T', 'M');
		}
	}

    if($_POST){
        $obj = new DeclaracionRetencionesExportController();
        if($_POST['action'] == "excel"){
            $obj->exportExcel();
        }else if($_POST['action'] == "pdf"){
            $obj->exportPdf();
        }
    }

?>
