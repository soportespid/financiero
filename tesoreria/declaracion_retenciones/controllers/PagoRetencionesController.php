<?php
    require_once '../../../Librerias/core/Helpers.php';
    require_once '../../../PHPExcel/Classes/PHPExcel.php';
    require_once '../models/PagoRetencionesModel.php';
    session_start();

    class PagoRetencionesController extends PagoRetencionesModel{
        public function getData(){
            if(!empty($_SESSION)){
                $request =$this->selectData();
                echo json_encode($request,JSON_UNESCAPED_UNICODE);
            }
            die();
        }
        public function calcular(){
            if(!empty($_SESSION)){
                $arrInfo = [];
                $arrData = json_decode($_POST['data'],true);
                $strFechaInicial = $_POST['fecha_inicial'];
                $strFechaFinal = $_POST['fecha_final'];
                $strDestino = $_POST['destino'];
                /* $parametros = $this->selectParametros(); */
                $totalData= count($arrData);
                for ($i=0; $i < $totalData ; $i++) {
                    $data = $arrData[$i];
                        
                    $arrOtrosEgresos = $this->selectOtrosEgresos($strFechaInicial,$strFechaFinal,$data,$data['sigla']);
                    $arrPagoTerceros = $this->selectPagoTerceros($strFechaInicial,$strFechaFinal,$data,$data['sigla']);
                    
                    if(!empty($arrOtrosEgresos)){
                        array_push($arrInfo,$arrOtrosEgresos);
                    }
                    if(!empty($arrPagoTerceros)){
                        array_push($arrInfo,$arrPagoTerceros);
                    }
                    
                }
                $arrInfo = $this->fixArrayInfo($arrInfo);
                $arrResponse = array("data"=>$arrInfo,"html"=>$this->getHtml($arrInfo['data']));
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
            die();
        }
        public function fixArrayInfo(array $arrInfo){
            $arrData = [];
            $total = 0;
            foreach ($arrInfo as $cab) {
                foreach ($cab as $det) {
                    if($det['valor']>0){
                        $total+=$det['valor'];
                        array_push($arrData,$det);
                    }
                }
            }
            return array(
                "total_valor"=>$total,
                "total_valor_letras"=>convertir($total)." PESOS M/CTE",
                "data"=>$arrData
            );
        }
        public function getHtml($data){
            $total = count($data);
            $retencion = !empty($data) ? $data[0]['nombre_descuentos'] : "";
            $acumulador = 0;
            $baseTotalRete = 0;
            $html ="";
            for ($i=0; $i < $total ; $i++) {
                $row = $data[$i];
                if($retencion != $row['nombre_descuentos']){
                    $html.= '
                    <tr class = "bg-success text-white">
                        <td colspan = "6" > Total de retencion/ingreso '.$retencion.'</td>
                        <td>'.number_format(round($acumulador,0),0).'</td>
                    </tr>';
                    $acumulador = 0;
                    $baseTotalRete = 0;
                }
                $html.='
                    <tr>
                        <td class="text-nowrap">'.$row['nombre'].'</td>
                        <td class="w-15">'.$row['tercero'].'</td>
                        <td class="text-nowrap">'.$row['nombre_tercero'].'</td>
                        <td>'.$row['consecutivo'].'</td>
                        <td>'.$row['documento'].'</td>
                        <td>'.$row['concepto'].'</td>
                        <td>'.number_format($row['valor']).'</td>
                    </tr>';
                $retencion = $row['nombre_descuentos'];
                $acumulador += round($row['valor'],0);
                $baseTotalRete += round($row['cuentas'],0);
                if($i == $total - 1){
                    $html.= '<tr class = "bg-success text-white" >
                            <td colspan = "6" >
                                Total de retencion/ingreso '.$retencion.'
                            </td>
                            <td>
                                '.number_format(round($acumulador,0),0).'
                            </td>
                        </tr>';
                    $acumulador = 0;
                    $baseTotalRete = 0;
                }
            }
            return $html;
        }
    }
    
    if($_POST){
        $obj = new PagoRetencionesController();
        if($_POST['action']=="get"){
            $obj->getData();
        }else if($_POST['action']=="calcular"){
            $obj->calcular();
        }
    }

?>
