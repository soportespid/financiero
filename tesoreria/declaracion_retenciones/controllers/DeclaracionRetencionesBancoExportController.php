<?php
    require_once '../../../Librerias/core/Helpers.php';
    require_once '../../../PHPExcel/Classes/PHPExcel.php';
    require_once '../../../tcpdf/tcpdf_include.php';
    require_once '../../../tcpdf/tcpdf.php';
    session_start();
    /*ini_set('display_errors', '1');
    ini_set('display_startup_errors', '1');
    error_reporting(E_ALL);*/
    class DeclaracionRetencionesExportController {
        public function exportExcel(){
            if(!empty($_SESSION)){
                $arrData = json_decode($_POST['data'],true);
                if(!empty($arrData)){
                    $arrFechaInicial = explode("-",$_POST['fecha_inicial']);
                    $arrFechaFinal = explode("-",$_POST['fecha_final']);
                    $strFechaInicial = $arrFechaInicial[2]."/".$arrFechaInicial[1]."/".$arrFechaInicial[0];
                    $strFechaFinal = $arrFechaFinal[2]."/".$arrFechaFinal[1]."/".$arrFechaFinal[0];
                    $arrDet = $arrData['banco'];

                    $objPHPExcel = new PHPExcel();
                    $objPHPExcel->getActiveSheet()->getStyle('A:E')->applyFromArray(
                        array(
                            'alignment' => array(
                                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
                                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
                            )
                        )
                    );
                    $objPHPExcel->getProperties()
                    ->setCreator("IDEAL 10")
                    ->setLastModifiedBy("IDEAL 10")
                    ->setTitle("Exportar Excel con PHP")
                    ->setSubject("Documento de prueba")
                    ->setDescription("Documento generado con PHPExcel")
                    ->setKeywords("usuarios phpexcel")
                    ->setCategory("reportes");

                    //----Cuerpo de Documento----
                    $objPHPExcel->setActiveSheetIndex(0)
                    ->mergeCells('A1:E1')
                    ->mergeCells('A2:E2')
                    ->mergeCells('B3:E3')
                    ->mergeCells('B4:E4')
                    ->mergeCells('B5:E5')
                    ->setCellValue('A1', 'TESORERÍA')
                    ->setCellValue('A2', 'RETENCIONES E INGRESOS PARA TERCEROS - DETALLE DE RETENCIONES POR BANCOS')
                    ->setCellValue('A3', 'FECHA')
                    ->setCellValue('A4', 'NETO A PAGAR')
                    ->setCellValue('A5', 'VALOR EN LETRAS')
                    ->setCellValue('B3', "Desde ".$strFechaInicial." Hasta ".$strFechaFinal['fecha_final'])
                    ->setCellValue('B4', "$".number_format($arrData['total_valor'],0))
                    ->setCellValue('B5', $arrData['total_valor_letras']);
                    $objPHPExcel-> getActiveSheet ()
                    -> getStyle ("A3")
                    -> getFill ()
                    -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
                    -> getStartColor ()
                    -> setRGB ('3399cc');
                    $objPHPExcel-> getActiveSheet ()
                    -> getStyle ("A4")
                    -> getFill ()
                    -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
                    -> getStartColor ()
                    -> setRGB ('3399cc');
                    $objPHPExcel-> getActiveSheet ()
                    -> getStyle ("A5")
                    -> getFill ()
                    -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
                    -> getStartColor ()
                    -> setRGB ('3399cc');
                    $objPHPExcel-> getActiveSheet ()
                    -> getStyle ("A1")
                    -> getFill ()
                    -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
                    -> getStartColor ()
                    -> setRGB ('C8C8C8');
                    $objPHPExcel-> getActiveSheet ()
                    -> getStyle ("A1:A2")
                    -> getFont ()
                    -> setBold ( true )
                    -> setName ( 'Verdana' )
                    -> setSize ( 10 )
                    -> getColor ()
                    -> setRGB ('000000');
                    $objPHPExcel-> getActiveSheet ()
                    -> getStyle ('A1:A2')
                    -> getAlignment ()
                    -> applyFromArray (array ( 'horizontal'  =>  PHPExcel_Style_Alignment :: HORIZONTAL_CENTER ,) );
                    $objPHPExcel-> getActiveSheet ()
                    -> getStyle ('A4:E4')
                    -> getAlignment ()
                    -> applyFromArray (array ( 'horizontal'  =>  PHPExcel_Style_Alignment :: VERTICAL_JUSTIFY ,) );
                    $objPHPExcel-> getActiveSheet ()
                    -> getStyle ("A2")
                    -> getFill ()
                    -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID);

                    $borders = array(
                        'borders' => array(
                            'allborders' => array(
                                'style' => PHPExcel_Style_Border::BORDER_THIN,
                            'color' => array('argb' => 'FF000000'),
                            )
                        ),
                    );
                    $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A6', 'Retenciones/ingresos')
                    ->setCellValue('B6', "Cuenta bancaria")
                    ->setCellValue('C6', "Banco")
                    ->setCellValue('D6', "Base")
                    ->setCellValue('E6', "Valor");
                    $objPHPExcel-> getActiveSheet ()
                        -> getStyle ("A6:E6")
                        -> getFill ()
                        -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
                        -> getStartColor ()
                        -> setRGB ('99ddff');
                    $objPHPExcel->getActiveSheet()->getStyle("A6:E6")->getFont()->setBold(true);
                    $objPHPExcel->getActiveSheet()->getStyle('A1:E1')->applyFromArray($borders);
                    $objPHPExcel->getActiveSheet()->getStyle('A2:E2')->applyFromArray($borders);
                    $objPHPExcel->getActiveSheet()->getStyle('A3:E3')->applyFromArray($borders);
                    $objPHPExcel->getActiveSheet()->getStyle('A4:E4')->applyFromArray($borders);
                    $objPHPExcel->getActiveSheet()->getStyle('A5:E5')->applyFromArray($borders);
                    $objPHPExcel->getActiveSheet()->getStyle('A6:E6')->applyFromArray($borders);

                    $objPHPExcel->getActiveSheet()->getStyle("A3")->getFont()->getColor()->setRGB("ffffff");
                    $objPHPExcel->getActiveSheet()->getStyle("A4")->getFont()->getColor()->setRGB("ffffff");
                    $objPHPExcel->getActiveSheet()->getStyle("A5")->getFont()->getColor()->setRGB("ffffff");

                    $objPHPExcel->getActiveSheet()->getStyle("A3")->getFont()->setBold(true);
                    $objPHPExcel->getActiveSheet()->getStyle("A4")->getFont()->setBold(true);
                    $objPHPExcel->getActiveSheet()->getStyle("A5")->getFont()->setBold(true);

                    $retencion = !empty($arrDet) ? $arrDet[0]['nombre_descuentos'] : "";
                    $acumulador = 0;
                    $baseTotalRete = 0;
                    $cont = 0;
                    $row = 7;
                    $totalData = count($arrDet);
                    foreach ($arrDet as $data) {
                        if($retencion != $data['nombre_descuentos']){
                            $objPHPExcel->setActiveSheetIndex(0)
                            ->mergeCells("A$row:C$row")
                            ->setCellValue("A$row", "Total de retencion/ingreso ".$retencion)
                            ->setCellValue("D$row", $baseTotalRete)
                            ->setCellValue("E$row", $acumulador);
                            $acumulador = 0;
                            $baseTotalRete = 0;
                            $objPHPExcel-> getActiveSheet ()
                            -> getStyle ("A$row:E$row")
                            -> getFill ()
                            -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
                            -> getStartColor ()
                            -> setRGB ('99ddff');
                            $row++;
                        }

                        $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValueExplicit ("A$row", $data['nombre'], PHPExcel_Cell_DataType :: TYPE_STRING)
                        ->setCellValueExplicit ("B$row", $data['cuenta_banco'], PHPExcel_Cell_DataType :: TYPE_STRING)
                        ->setCellValueExplicit ("C$row", $data['tercero_banco'], PHPExcel_Cell_DataType :: TYPE_STRING)
                        ->setCellValueExplicit ("D$row", $data['cuentas'], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
                        ->setCellValueExplicit ("E$row", $data['valor'], PHPExcel_Cell_DataType :: TYPE_NUMERIC);

                        $retencion = $data['nombre_descuentos'];
                        $acumulador += round($data['valor'],0);
                        $baseTotalRete += round($data['cuentas'],0);
                        if($cont == $totalData){
                            $objPHPExcel->setActiveSheetIndex(0)
                            ->mergeCells("A$row:C$row")
                            ->setCellValue("A$row", "Total de retencion/ingreso ".$retencion)
                            ->setCellValue("D$row", $baseTotalRete)
                            ->setCellValue("E$row", $acumulador);
                            $acumulador = 0;
                            $baseTotalRete = 0;
                            $objPHPExcel-> getActiveSheet ()
                            -> getStyle ("A$row:E$row")
                            -> getFill ()
                            -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
                            -> getStartColor ()
                            -> setRGB ('99ddff');
                            $row++;
                        }
                        $row++;
                        $cont++;
                    }
                    $objPHPExcel->setActiveSheetIndex(0)
                    ->mergeCells("A$row:C$row")
                    ->setCellValue("A$row", "Total")
                    ->setCellValue("D$row", $arrData['total_cuentas'])
                    ->setCellValue("E$row", $arrData['total_valor']);
                    $objPHPExcel-> getActiveSheet ()
                    -> getStyle ("A$row:E$row")
                    -> getFill ()
                    -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
                    -> getStartColor ()
                    -> setRGB ('99ddff');

                    $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
                    $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
                    $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
                    $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
                    $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
                    //----Guardar documento----
                    header('Content-Type: application/vnd.ms-excel');
                    header('Content-Disposition: attachment;filename="reporte-declaracion_retenciones_bancos.xlsx"');
                    header('Cache-Control: max-age=0');
                    $objWriter=PHPExcel_IOFactory::createWriter($objPHPExcel,'Excel2007');
                    $objWriter->save('php://output');
                }
            }
            die();
        }
        public function exportExcelDet(){
            if(!empty($_SESSION)){
                $arrData = json_decode($_POST['data'],true);
                if(!empty($arrData)){
                    $arrFechaInicial = explode("-",$_POST['fecha_inicial']);
                    $arrFechaFinal = explode("-",$_POST['fecha_final']);
                    $strFechaInicial = $arrFechaInicial[2]."/".$arrFechaInicial[1]."/".$arrFechaInicial[0];
                    $strFechaFinal = $arrFechaFinal[2]."/".$arrFechaFinal[1]."/".$arrFechaFinal[0];
                    $arrDet = $arrData['banco_det'];

                    $objPHPExcel = new PHPExcel();
                    $objPHPExcel->getActiveSheet()->getStyle('A:E')->applyFromArray(
                        array(
                            'alignment' => array(
                                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
                                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
                            )
                        )
                    );
                    $objPHPExcel->getProperties()
                    ->setCreator("IDEAL 10")
                    ->setLastModifiedBy("IDEAL 10")
                    ->setTitle("Exportar Excel con PHP")
                    ->setSubject("Documento de prueba")
                    ->setDescription("Documento generado con PHPExcel")
                    ->setKeywords("usuarios phpexcel")
                    ->setCategory("reportes");

                    //----Cuerpo de Documento----
                    $objPHPExcel->setActiveSheetIndex(0)
                    ->mergeCells('A1:D1')
                    ->mergeCells('A2:D2')
                    ->mergeCells('B3:D3')
                    ->mergeCells('B4:D4')
                    ->mergeCells('B5:D5')
                    ->setCellValue('A1', 'TESORERÍA')
                    ->setCellValue('A2', 'RETENCIONES E INGRESOS PARA TERCEROS - DETALLE POR BANCOS')
                    ->setCellValue('A3', 'FECHA')
                    ->setCellValue('A4', 'NETO A PAGAR')
                    ->setCellValue('A5', 'VALOR EN LETRAS')
                    ->setCellValue('B3', "Desde ".$strFechaInicial." Hasta ".$strFechaFinal['fecha_final'])
                    ->setCellValue('B4', "$".number_format($arrData['total_valor'],0))
                    ->setCellValue('B5', $arrData['total_valor_letras']);
                    $objPHPExcel-> getActiveSheet ()
                    -> getStyle ("A3")
                    -> getFill ()
                    -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
                    -> getStartColor ()
                    -> setRGB ('3399cc');
                    $objPHPExcel-> getActiveSheet ()
                    -> getStyle ("A4")
                    -> getFill ()
                    -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
                    -> getStartColor ()
                    -> setRGB ('3399cc');
                    $objPHPExcel-> getActiveSheet ()
                    -> getStyle ("A5")
                    -> getFill ()
                    -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
                    -> getStartColor ()
                    -> setRGB ('3399cc');
                    $objPHPExcel-> getActiveSheet ()
                    -> getStyle ("A1")
                    -> getFill ()
                    -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
                    -> getStartColor ()
                    -> setRGB ('C8C8C8');
                    $objPHPExcel-> getActiveSheet ()
                    -> getStyle ("A1:A2")
                    -> getFont ()
                    -> setBold ( true )
                    -> setName ( 'Verdana' )
                    -> setSize ( 10 )
                    -> getColor ()
                    -> setRGB ('000000');
                    $objPHPExcel-> getActiveSheet ()
                    -> getStyle ('A1:A2')
                    -> getAlignment ()
                    -> applyFromArray (array ( 'horizontal'  =>  PHPExcel_Style_Alignment :: HORIZONTAL_CENTER ,) );
                    $objPHPExcel-> getActiveSheet ()
                    -> getStyle ('A4:D4')
                    -> getAlignment ()
                    -> applyFromArray (array ( 'horizontal'  =>  PHPExcel_Style_Alignment :: VERTICAL_JUSTIFY ,) );
                    $objPHPExcel-> getActiveSheet ()
                    -> getStyle ("A2")
                    -> getFill ()
                    -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID);

                    $borders = array(
                        'borders' => array(
                            'allborders' => array(
                                'style' => PHPExcel_Style_Border::BORDER_THIN,
                            'color' => array('argb' => 'FF000000'),
                            )
                        ),
                    );
                    $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A6', "Cuenta bancaria")
                    ->setCellValue('B6', "Banco")
                    ->setCellValue('C6', "Base")
                    ->setCellValue('D6', "Valor");
                    $objPHPExcel-> getActiveSheet ()
                        -> getStyle ("A6:D6")
                        -> getFill ()
                        -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
                        -> getStartColor ()
                        -> setRGB ('99ddff');
                    $objPHPExcel->getActiveSheet()->getStyle("A6:D6")->getFont()->setBold(true);
                    $objPHPExcel->getActiveSheet()->getStyle('A1:D1')->applyFromArray($borders);
                    $objPHPExcel->getActiveSheet()->getStyle('A2:D2')->applyFromArray($borders);
                    $objPHPExcel->getActiveSheet()->getStyle('A3:D3')->applyFromArray($borders);
                    $objPHPExcel->getActiveSheet()->getStyle('A4:D4')->applyFromArray($borders);
                    $objPHPExcel->getActiveSheet()->getStyle('A5:D5')->applyFromArray($borders);
                    $objPHPExcel->getActiveSheet()->getStyle('A6:D6')->applyFromArray($borders);

                    $objPHPExcel->getActiveSheet()->getStyle("A3")->getFont()->getColor()->setRGB("ffffff");
                    $objPHPExcel->getActiveSheet()->getStyle("A4")->getFont()->getColor()->setRGB("ffffff");
                    $objPHPExcel->getActiveSheet()->getStyle("A5")->getFont()->getColor()->setRGB("ffffff");

                    $objPHPExcel->getActiveSheet()->getStyle("A3")->getFont()->setBold(true);
                    $objPHPExcel->getActiveSheet()->getStyle("A4")->getFont()->setBold(true);
                    $objPHPExcel->getActiveSheet()->getStyle("A5")->getFont()->setBold(true);

                    $retencion = !empty($arrDet) ? $arrDet[0]['tercero_banco'] : "";
                    $acumulador = 0;
                    $baseTotalRete = 0;
                    $cont = 0;
                    $row = 7;
                    $totalData = count($arrDet);
                    foreach ($arrDet as $data) {
                        if($retencion != $data['tercero_banco']){
                            $objPHPExcel->setActiveSheetIndex(0)
                            ->mergeCells("A$row:B$row")
                            ->setCellValue("A$row", "Total de ".$retencion)
                            ->setCellValue("C$row", $baseTotalRete)
                            ->setCellValue("D$row", $acumulador);
                            $acumulador = 0;
                            $baseTotalRete = 0;
                            $objPHPExcel-> getActiveSheet ()
                            -> getStyle ("A$row:D$row")
                            -> getFill ()
                            -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
                            -> getStartColor ()
                            -> setRGB ('99ddff');
                            $row++;
                        }

                        $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValueExplicit ("A$row", $data['cuenta_banco'], PHPExcel_Cell_DataType :: TYPE_STRING)
                        ->setCellValueExplicit ("B$row", $data['tercero_banco'], PHPExcel_Cell_DataType :: TYPE_STRING)
                        ->setCellValueExplicit ("C$row", $data['cuentas'], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
                        ->setCellValueExplicit ("D$row", $data['valor'], PHPExcel_Cell_DataType :: TYPE_NUMERIC);

                        $retencion = $data['tercero_banco'];
                        $acumulador += round($data['valor'],0);
                        $baseTotalRete += round($data['cuentas'],0);
                        if($cont == $totalData){
                            $objPHPExcel->setActiveSheetIndex(0)
                            ->mergeCells("A$row:B$row")
                            ->setCellValue("A$row", "Total de ".$retencion)
                            ->setCellValue("C$row", $baseTotalRete)
                            ->setCellValue("D$row", $acumulador);
                            $acumulador = 0;
                            $baseTotalRete = 0;
                            $objPHPExcel-> getActiveSheet ()
                            -> getStyle ("A$row:D$row")
                            -> getFill ()
                            -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
                            -> getStartColor ()
                            -> setRGB ('99ddff');
                            $row++;
                        }
                        $row++;
                        $cont++;
                    }
                    $objPHPExcel->setActiveSheetIndex(0)
                    ->mergeCells("A$row:B$row")
                    ->setCellValue("A$row", "Total")
                    ->setCellValue("C$row", $arrData['total_cuentas'])
                    ->setCellValue("D$row", $arrData['total_valor']);
                    $objPHPExcel-> getActiveSheet ()
                    -> getStyle ("A$row:D$row")
                    -> getFill ()
                    -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
                    -> getStartColor ()
                    -> setRGB ('99ddff');

                    $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
                    $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
                    $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
                    $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
                    //----Guardar documento----
                    header('Content-Type: application/vnd.ms-excel');
                    header('Content-Disposition: attachment;filename="reporte-declaracion_bancos_detalle.xlsx"');
                    header('Cache-Control: max-age=0');
                    $objWriter=PHPExcel_IOFactory::createWriter($objPHPExcel,'Excel2007');
                    $objWriter->save('php://output');
                }
            }
            die();
        }
        public function exportPdf($type){
            if(!empty($_SESSION)){
                define("TITLE",$type == 1 ? "RETENCIONES E INGRESOS PARA TERCEROS - DETALLE DE RETENCIONES POR BANCOS" : "RETENCIONES E INGRESOS PARA TERCEROS - DETALLE POR BANCOS");
                $arrData = json_decode($_POST['data'],true);
                if(!empty($arrData)){
                    $arrDet = $type == 1 ? $arrData['banco'] : $arrData['banco_det'];
                    $strFecha = "Desde ".date_format(date_create($_POST['fecha_inicial']),"d/m/Y")." Hasta ".date_format(date_create($_POST['fecha_final']),"d/m/Y");
                    $pdf = new MYPDF('P','mm','Letter', true, 'iso-8859-1', false);
                    $pdf->SetDocInfoUnicode (true);
                    // set document information
                    $pdf->SetCreator(PDF_CREATOR);
                    $pdf->SetAuthor('IDEALSAS');
                    $pdf->SetTitle($type == 1 ?'DETALLE RETENCIONES POR BANCOS' : "DETALLE POR BANCOS");
                    $pdf->SetSubject($type == 1 ?'DETALLE RETENCIONES POR BANCOS' : "DETALLE POR BANCOS");
                    $pdf->SetKeywords('TCPDF, PDF, example, test, guide');
                    $pdf->SetMargins(10, 38, 10);// set margins
                    $pdf->SetHeaderMargin(38);// set margins
                    $pdf->SetFooterMargin(17);// set margins
                    $pdf->SetAutoPageBreak(TRUE, 20);// set auto page breaks
                    // set some language-dependent strings (optional)
                    if (@file_exists(dirname(__FILE__).'/lang/spa.php'))
                    {
                        require_once(dirname(__FILE__).'/lang/spa.php');
                        $pdf->setLanguageArray($l);
                    }
                    $pdf->AddPage();
                    $pdf->SetFillColor(51, 153, 204);
                    $pdf->SetTextColor(255,255,255);
                    $pdf->SetFont('Helvetica','b',9);
                    $pdf->MultiCell(40,4,"Fecha","LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->SetFillColor(255,255,255);
                    $pdf->SetTextColor(0,0,0);
                    $pdf->SetFont('Helvetica','',9);
                    $pdf->MultiCell(150,4,$strFecha,"RBT",'L',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->ln();
                    $pdf->SetFillColor(51, 153, 204);
                    $pdf->SetTextColor(255,255,255);
                    $pdf->SetFont('Helvetica','b',9);
                    $pdf->MultiCell(40,4,"Neto a pagar","LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->SetFillColor(255,255,255);
                    $pdf->SetTextColor(0,0,0);
                    $pdf->SetFont('Helvetica','',9);
                    $pdf->MultiCell(150,4,"$".number_format($arrData['total_valor'],0),"RBT",'L',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->ln();
                    $pdf->SetFillColor(51, 153, 204);
                    $pdf->SetTextColor(255,255,255);
                    $pdf->SetFont('Helvetica','b',9);
                    $pdf->MultiCell(40,4,"Valor en letras","LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->SetFillColor(255,255,255);
                    $pdf->SetTextColor(0,0,0);
                    $pdf->SetFont('Helvetica','',9);
                    $pdf->MultiCell(150,4,$arrData['total_valor_letras'],"RBT",'L',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->ln();
                    $pdf->ln();
                    $pdf->SetFillColor(51, 153, 204);
                    $pdf->SetTextColor(255,255,255);
                    $pdf->SetFont('Helvetica','b',9);
                    $pdf->MultiCell(190,4,"Detalle","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->ln();
                    if(!empty($arrDet)){
                        if($type == 1){

                            $pdf->SetFillColor(153,221,255);
                            $pdf->SetFont('helvetica','B',7);
                            $pdf->SetTextColor(0,0,0);
                            $pdf->MultiCell(48,4,"Retenciones/Ingresos","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
                            $pdf->MultiCell(28,4,"Cuenta bancaria","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
                            $pdf->MultiCell(38,4,"Banco","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
                            $pdf->MultiCell(38,4,"Base","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
                            $pdf->MultiCell(38,4,"Valor","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
                            $pdf->ln();
                            $fill = true;
                            $retencion = !empty($arrDet) ? $arrDet[0]['nombre_descuentos'] : "";
                            $acumulador = 0;
                            $baseTotalRete = 0;
                            $cont = 0;
                            $totalData = count($arrDet);
                            foreach ($arrDet as $data) {
                                if($retencion != $data['nombre_descuentos']){
                                    $pdf->SetFont('helvetica','B',7);
                                    $pdf->SetFillColor(153,221,255);
                                    $pdf->SetTextColor(0,0,0);
                                    $pdf->MultiCell(114,4,"Total de retencion/ingreso ".$retencion,"",'L',true,0,'','',true,0,false,true,0,'M',true);

                                    $pdf->MultiCell(38,4,"$".number_format(round($baseTotalRete,0)),"",'R',true,0,'','',true,0,false,true,0,'M',true);
                                    $pdf->MultiCell(38,4,"$".number_format(round($acumulador,0),0),"",'R',true,0,'','',true,0,false,true,0,'M',true);
                                    $acumulador = 0;
                                    $baseTotalRete = 0;
                                    $pdf->ln();
                                }
                                $pdf->SetFillColor(245,245,245);
                                $pdf->SetFont('helvetica','',7);
                                $nombre = $pdf->getNumLines($data['nombre'],30);
                                $nombreTercero = $pdf->getNumLines($data['nombre_tercero'],50);
                                $nombreBanco = $pdf->getNumLines($data['tercero_banco'],35);
                                $max = max($nombre,$nombreBanco);
                                $height = 3*$max;
                                $pdf->MultiCell(48,$height,$data['nombre'],"",'L',$fill,0,'','',true,0,false,true,0,'M',true);
                                $pdf->MultiCell(28,$height,$data['cuenta_banco'],"",'L',$fill,0,'','',true,0,false,true,0,'M',true);
                                $pdf->MultiCell(38,$height,$data['tercero_banco'],"",'L',$fill,0,'','',true,0,false,true,0,'M',true);
                                $pdf->MultiCell(38,$height,"$".number_format($data['cuentas']),"",'R',$fill,0,'','',true,0,false,true,0,'M',true);
                                $pdf->MultiCell(38,$height,"$".number_format($data['valor']),"",'R',$fill,0,'','',true,0,false,true,0,'M',true);
                                $pdf->ln();

                                $retencion = $data['nombre_descuentos'];
                                $acumulador += round($data['valor'],0);
                                $baseTotalRete += round($data['cuentas'],0);
                                $cont++;
                                if($cont == $totalData){
                                    $pdf->SetFont('helvetica','B',7);
                                    $pdf->SetFillColor(153,221,255);
                                    $pdf->SetTextColor(0,0,0);
                                    $pdf->MultiCell(114,4,"Total de retencion/ingreso ".$retencion,"",'L',true,0,'','',true,0,false,true,0,'M',true);

                                    $pdf->MultiCell(38,4,"$".number_format(round($baseTotalRete,0)),"",'R',true,0,'','',true,0,false,true,0,'M',true);
                                    $pdf->MultiCell(38,4,"$".number_format(round($acumulador,0),0),"",'R',true,0,'','',true,0,false,true,0,'M',true);
                                    $acumulador = 0;
                                    $baseTotalRete = 0;
                                    $pdf->ln();
                                }
                                $fill = !$fill;
                                if($pdf->GetY()>230){
                                    $pdf->AddPage();
                                }
                            }
                            $pdf->SetFillColor(51, 153, 204);
                            $pdf->SetTextColor(255,255,255);
                            $pdf->SetFont('helvetica','B',7);
                            $pdf->MultiCell(114,4,"Total","",'R',true,0,'','',true,0,false,true,0,'M',true);
                            $pdf->MultiCell(38,4,"$".number_format($arrData['total_cuentas']),"",'R',true,0,'','',true,0,false,true,0,'M',true);
                            $pdf->MultiCell(38,4,"$".number_format($arrData['total_valor']),"",'R',true,0,'','',true,0,false,true,0,'M',true);
                        }else{
                            $pdf->SetFillColor(153,221,255);
                            $pdf->SetFont('helvetica','B',7);
                            $pdf->SetTextColor(0,0,0);
                            $pdf->MultiCell(47.5,4,"Cuenta bancaria","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
                            $pdf->MultiCell(47.5,4,"Banco","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
                            $pdf->MultiCell(47.5,4,"Base","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
                            $pdf->MultiCell(47.5,4,"Valor","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
                            $pdf->ln();
                            $fill = true;
                            $retencion = !empty($arrDet) ? $arrDet[0]['tercero_banco'] : "";
                            $acumulador = 0;
                            $baseTotalRete = 0;
                            $cont = 0;
                            $totalData = count($arrDet);
                            foreach ($arrDet as $data) {
                                if($retencion != $data['tercero_banco']){
                                    $pdf->SetFont('helvetica','B',7);
                                    $pdf->SetFillColor(153,221,255);
                                    $pdf->SetTextColor(0,0,0);
                                    $pdf->MultiCell(95,4,"Total de ".$retencion,"",'L',true,0,'','',true,0,false,true,0,'M',true);

                                    $pdf->MultiCell(47.5,4,"$".number_format(round($baseTotalRete,0)),"",'R',true,0,'','',true,0,false,true,0,'M',true);
                                    $pdf->MultiCell(47.5,4,"$".number_format(round($acumulador,0),0),"",'R',true,0,'','',true,0,false,true,0,'M',true);
                                    $acumulador = 0;
                                    $baseTotalRete = 0;
                                    $pdf->ln();
                                }
                                $pdf->SetFillColor(245,245,245);
                                $pdf->SetFont('helvetica','',7);

                                $height = 4;
                                $pdf->MultiCell(47.5,$height,$data['cuenta_banco'],"",'L',$fill,0,'','',true,0,false,true,0,'M',true);
                                $pdf->MultiCell(47.5,$height,$data['tercero_banco'],"",'L',$fill,0,'','',true,0,false,true,0,'M',true);
                                $pdf->MultiCell(47.5,$height,"$".number_format($data['cuentas']),"",'R',$fill,0,'','',true,0,false,true,0,'M',true);
                                $pdf->MultiCell(47.5,$height,"$".number_format($data['valor']),"",'R',$fill,0,'','',true,0,false,true,0,'M',true);
                                $pdf->ln();

                                $retencion = $data['tercero_banco'];
                                $acumulador += round($data['valor'],0);
                                $baseTotalRete += round($data['cuentas'],0);
                                $cont++;
                                if($cont == $totalData){
                                    $pdf->SetFont('helvetica','B',7);
                                    $pdf->SetFillColor(153,221,255);
                                    $pdf->SetTextColor(0,0,0);
                                    $pdf->MultiCell(95,4,"Total de ".$retencion,"",'L',true,0,'','',true,0,false,true,0,'M',true);

                                    $pdf->MultiCell(47.5,4,"$".number_format(round($baseTotalRete,0)),"",'R',true,0,'','',true,0,false,true,0,'M',true);
                                    $pdf->MultiCell(47.5,4,"$".number_format(round($acumulador,0),0),"",'R',true,0,'','',true,0,false,true,0,'M',true);
                                    $acumulador = 0;
                                    $baseTotalRete = 0;
                                    $pdf->ln();
                                }
                                $fill = !$fill;
                                if($pdf->GetY()>230){
                                    $pdf->AddPage();
                                }
                            }
                            $pdf->SetFillColor(51, 153, 204);
                            $pdf->SetTextColor(255,255,255);
                            $pdf->SetFont('helvetica','B',7);
                            $pdf->MultiCell(95,4,"Total","",'R',true,0,'','',true,0,false,true,0,'M',true);
                            $pdf->MultiCell(47.5,4,"$".number_format($arrData['total_cuentas']),"",'R',true,0,'','',true,0,false,true,0,'M',true);
                            $pdf->MultiCell(47.5,4,"$".number_format($arrData['total_valor']),"",'R',true,0,'','',true,0,false,true,0,'M',true);
                        }
                    }
                    $pdf->ln();
                    $pdf->ln();
                    //Campo para recibido y sello
                    $getY = $pdf->getY();
                    $pdf->setX(60);
                    $pdf->SetFont('helvetica','B',9);
                    $pdf->SetFillColor(51, 153, 204);
                    $pdf->SetTextColor(255,255,255);
                    $pdf->cell(95,4,'Elaboró','LRTB',0,'C',1);
                    $pdf->ln();
                    $pdf->setX(60);
                    $pdf->SetFont('helvetica','',7);
                    $pdf->SetTextColor(0,0,0);
                    $pdf->SetFillColor(255,255,255);
                    $pdf->cell(95,20,'','LRTB',0,'L',1);
                    $pdf->ln();
                    $pdf->setX(60);
                    $pdf->cell(95,4,"Nombre: ".strtoupper( iconv($_SESSION["VERCARPDFINI"], $_SESSION["VERCARPDFFIN"],$_SESSION['usuario'])),'LRTB',0,'L',1);
                    $pdf->ln();
                    $pdf->Output($type==1 ? 'declaracion_retenciones_bancos.pdf' : 'declaracion_detalle_bancos.pdf', 'I');
                }
            }
            die();
        }

    }
    class MYPDF extends TCPDF {

		public function Header()
		{
			$request = configBasica();
            $strNit = $request['nit'];
            $strRazon = $request['razonsocial'];

			//Parte Izquierda
			$this->Image('../../../imagenes/escudo.jpg',  13, 13, 20, 20, 'JPG', '', 'T', true, 300, '', false, false, 0, false, false, false);// Logo
			$this->SetFont('helvetica','B',8);
			$this->SetY(10);
			$this->RoundedRect(10, 10, 190, 25, 1,'');
			$this->Cell(0.1);
			$this->Cell(26,25,'','R',0,'L');
			$this->SetY(8);
			$this->SetX(40);
			$this->SetFont('helvetica','B',9);
			$this->Cell(140,15,strtoupper("$strRazon"),0,0,'C');
			$this->SetFont('helvetica','B',7);
			$this->SetY(12);
			$this->SetX(40);
			$this->Cell(140,15,'NIT: '.$strNit,0,0,'C');
			//*****************************************************************************************************************************
			$this->SetFont('helvetica','B',9);
			$this->SetY(23);
			$this->SetX(36);
            $this->Cell(164,12,TITLE,'T',0,'C');


            $this->SetFont('helvetica','B',7);
			$this->SetY(10);
			$this->SetX(167);
			$this->Cell(30,7," FECHA: ". date("d/m/Y"),"L",0,'L');
			$this->SetY(17);
			$this->SetX(167);
            $this->Cell(35,6,"","L",0,'L');

			//**********************************************************
			$this->SetFont('times','B',10);
			$this->ln(12);
			//**********************************************************
		}
		public function Footer(){

			$request = configBasica();
            $strDireccion = $request['direccion'] != "" ? "Dirección: ".strtoupper($request['direccion']) : "";
            $strWeb = $request['web'] != "" ? "Pagina web: ".strtoupper($request['web']) :"";
            $strEmail = $request['email'] !="" ? "Email: ".strtoupper($request['email']) :"";
            $strTelefono = $request['telefono'] != "" ? "Telefonos: ".$request['telefono'] : "";
            $strUsuario = searchUser($_SESSION['cedulausu'])['nom_usu'];
			$strNick = $_SESSION['nickusu'];
			$strFecha = date("d/m/Y H:i:s");
			$strIp = $_SERVER['REMOTE_ADDR'];

			$this->SetFont('helvetica', 'I', 8);
			$txt = <<<EOD
			$strDireccion $strTelefono
			$strEmail $strWeb
			EOD;
			$this->SetFont('helvetica', 'I', 6);
			$this->Cell(190,10,'','T',0,'T');
			$this->ln(2);
			$this->Write(0, $txt , '', 0, 'C', true, 0, false, false, 0);

			$this->Cell(50, 10, 'Hecho por: '.$strUsuario, 00, false, 'C', 0, '', 0, false, 'T', 'M');
			$this->Cell(30, 10, 'Impreso por: '.$strNick, 0, false, 'C', 0, '', 0, false, 'T', 'M');
			$this->Cell(30, 10, 'IP: '.$strIp, 0, false, 'C',0, '', 0, false, 'T', 'M');
			$this->Cell(30, 10, 'Fecha: '.$strFecha, 0, false, 'C', 0, '', 0, false, 'T', 'M');
			$this->Cell(30, 10, 'IDEAL.10 S.A.S', 0, false, 'C', 0, '', 0, false, 'T', 'M');
            $this->Cell(20, 10, 'Pagina '.$this->getAliasNumPage().'/'.$this->getAliasNbPages(), 0, false, 'C', 0, '', 0, false, 'T', 'M');
		}
	}

    if($_POST){
        $obj = new DeclaracionRetencionesExportController();
        if($_POST['action'] == "excel"){
            if($_POST['type'] == 1){$obj->exportExcel();}else{$obj->exportExcelDet();}
        }else if($_POST['action'] == "pdf"){
            $obj->exportPdf($_POST['type']);
        }
    }

?>
