<?php
    require_once '../../../Librerias/core/Helpers.php';
    require_once '../../../PHPExcel/Classes/PHPExcel.php';
    require_once '../models/DeclaracionRetencionesBancoModel.php';
    session_start();

    class DeclaracionRetencionesBancoController extends DeclaracionRetencionesBancoModel{
        public function getData(){
            if(!empty($_SESSION)){
                $request =$this->selectData();
                echo json_encode($request,JSON_UNESCAPED_UNICODE);
            }
            die();
        }
        public function calcular(){
            if(!empty($_SESSION)){
                $arrInfo = [];
                $arrData = json_decode($_POST['data'],true);
                $strFechaInicial = $_POST['fecha_inicial'];
                $strFechaFinal = $_POST['fecha_final'];
                $strDestino = $_POST['destino'];
                $parametros = $this->selectParametros();
                $totalData= count($arrData);
                for ($i=0; $i < $totalData ; $i++) {
                    $data = $arrData[$i];
                    if($data['sigla']=="R"){
                        if($data['id']=="34"){
                            $arrRete34 = $this->selectRete34($strFechaInicial,$strFechaFinal,$data);
                            array_push($arrInfo,$arrRete34);
                        }
                        $arrOrden = $this->selectOrdenPago($strFechaInicial,$strFechaFinal,$data,$strDestino,$parametros);
                        $arrPagoTerceros = $this->selectPagoTerceros($strFechaInicial,$strFechaFinal,$data,$strDestino);
                        if(!empty($arrOrden)){
                            array_push($arrInfo,$arrOrden);
                        }
                        if(!empty($arrPagoTerceros)){
                            array_push($arrInfo,$arrPagoTerceros);
                        }
                    }else if($data['sigla']=="I"){
                        $arrRecaudo = $this->selectRecaudos($strFechaInicial,$strFechaFinal,$data);
                        $arrRecaudoPropio = $this->selectRecaudosPropios($strFechaInicial,$strFechaFinal,$data);
                        $arrRecaudoTransferencia = $this->selectRecaudosTransferencias($strFechaInicial,$strFechaFinal,$data);
                        if(!empty($arrRecaudo)){
                            array_push($arrInfo,$arrRecaudo);
                        }
                        if(!empty($arrRecaudoPropio)){
                            array_push($arrInfo,$arrRecaudoPropio);
                        }
                        if(!empty($arrRecaudoTransferencia)){
                            array_push($arrInfo,$arrRecaudoTransferencia);
                        }
                    }
                }
                $arrInfo = $this->fixArrayInfo($arrInfo);
                $arrBanks = $this->filterByBanks($arrInfo['data']);
                $arrBanksDet = $this->filterByBanksDet($arrInfo['data']);
                $arrResponse = array(
                    "data"=>$arrInfo,
                    "banco"=>$arrBanks,
                    "banco_det"=>$arrBanksDet,
                    "html"=>$this->getHtmlDetail($arrBanks),
                    "html_bancos"=>$this->getHtmlBancos($arrBanksDet));
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
            die();
        }
        public function filterByBanks($data){
            $arrData = [];
            $total = count($data);
            for ($i=0; $i < $total ; $i++) {
                $totalData = count($arrData);
                if($totalData > 0){
                    $flag = false;
                    for ($j=0; $j < $totalData ; $j++) {
                        if($data[$i]['cuenta_banco'] == $arrData[$j]['cuenta_banco'] &&
                        $data[$i]['nombre'] == $arrData[$j]['nombre']){
                            $arrData[$j]['cuentas']+=$data[$i]['cuentas'];
                            $arrData[$j]['valor']+=$data[$i]['valor'];
                            $flag = true;
                            break;
                        }
                    }
                    if(!$flag){
                        array_push($arrData,$data[$i]);
                    }
                }else{
                    array_push($arrData,$data[$i]);
                }
            }
            return $arrData;
        }
        public function filterByBanksDet($data){
            $arrData = [];
            $total = count($data);
            for ($i=0; $i < $total ; $i++) {
                $totalData = count($arrData);
                if($totalData > 0){
                    $flag = false;
                    for ($j=0; $j < $totalData ; $j++) {
                        if($data[$i]['cuenta_banco'] == $arrData[$j]['cuenta_banco']){
                            $arrData[$j]['cuentas']+=$data[$i]['cuentas'];
                            $arrData[$j]['valor']+=$data[$i]['valor'];
                            $flag = true;
                            break;
                        }
                    }
                    if(!$flag){
                        array_push($arrData,$data[$i]);
                    }
                }else{
                    array_push($arrData,$data[$i]);
                }
            }
            usort($arrData,function($a,$b){return $a['tercero_banco'] > $b['tercero_banco'];});
            return $arrData;
        }
        public function fixArrayInfo(array $arrInfo){
            $arrData = [];
            $total = 0;
            $cuentas = 0;
            foreach ($arrInfo as $cab) {
                foreach ($cab as $det) {
                    if($det['cuentas']>0 || $det['valor']>0){
                        $total+=$det['valor'];
                        $cuentas+=$det['cuentas'];
                        array_push($arrData,$det);
                    }
                }
            }
            return array(
                "total_cuentas"=>$cuentas,
                "total_valor"=>$total,
                "total_valor_letras"=>convertir($total)." PESOS M/CTE",
                "data"=>$arrData
            );
        }
        public function getHtmlBancos($data){
            $total = count($data);
            $retencion = !empty($data) ? $data[0]['tercero_banco'] : "";
            $acumulador = 0;
            $baseTotalRete = 0;
            $html ="";
            for ($i=0; $i < $total ; $i++) {
                $row = $data[$i];
                if($retencion != $row['tercero_banco']){
                    $html.= '
                    <tr class = "bg-success text-white">
                        <td colspan = "2" > Total de '.$retencion.'</td>
                        <td class="text-right">'.number_format(round($baseTotalRete,0),0).'</td>
                        <td class="text-right">'.number_format(round($acumulador,0),0).'</td>
                    </tr>';
                    $acumulador = 0;
                    $baseTotalRete = 0;
                }
                $html.='
                    <tr>
                        <td>'.$row['cuenta_banco'].'</td>
                        <td>'.$row['tercero_banco'].'</td>
                        <td class="text-right">'.number_format($row['cuentas']).'</td>
                        <td class="text-right">'.number_format($row['valor']).'</td>
                    </tr>';
                $retencion = $row['tercero_banco'];
                $acumulador += round($row['valor'],0);
                $baseTotalRete += round($row['cuentas'],0);
                if($i == $total - 1){
                    $html.= '
                    <tr class = "bg-success text-white" >
                        <td colspan = "2" >Total de '.$retencion.'</td>
                        <td class="text-right">'.number_format(round($baseTotalRete,0),0).'</td>
                        <td class="text-right">'.number_format(round($acumulador,0),0).'</td>
                    </tr>';
                    $acumulador = 0;
                    $baseTotalRete = 0;
                }
            }
            return $html;
        }
        public function getHtmlDetail($data){
            $total = count($data);
            $retencion = !empty($data) ? $data[0]['nombre_descuentos'] : "";
            $acumulador = 0;
            $baseTotalRete = 0;
            $html ="";
            for ($i=0; $i < $total ; $i++) {
                $row = $data[$i];
                if($retencion != $row['nombre_descuentos']){
                    $html.= '
                    <tr class = "bg-success text-white">
                        <td colspan = "3" > Total de retencion/ingreso '.$retencion.'</td>
                        <td>'.number_format(round($baseTotalRete,0),0).'</td>
                        <td>'.number_format(round($acumulador,0),0).'</td>
                    </tr>';
                    $acumulador = 0;
                    $baseTotalRete = 0;
                }
                $html.='
                    <tr>
                        <td>'.$row['nombre'].'</td>
                        <td>'.$row['cuenta_banco'].'</td>
                        <td>'.$row['tercero_banco'].'</td>
                        <td>'.number_format($row['cuentas']).'</td>
                        <td>'.number_format($row['valor']).'</td>
                    </tr>';
                $retencion = $row['nombre_descuentos'];
                $acumulador += round($row['valor'],0);
                $baseTotalRete += round($row['cuentas'],0);
                if($i == $total - 1){
                    $html.= '<tr class = "bg-success text-white" >
                            <td colspan = "3" >
                                Total de retencion/ingreso '.$retencion.'
                            </td>
                            <td>
                                '.number_format(round($baseTotalRete,0),0).'
                            </td>
                            <td>
                                '.number_format(round($acumulador,0),0).'
                            </td>
                        </tr>';
                    $acumulador = 0;
                    $baseTotalRete = 0;
                }
            }
            return $html;
        }
    }
    if($_POST){
        $obj = new DeclaracionRetencionesBancoController();
        if($_POST['action']=="get"){
            $obj->getData();
        }else if($_POST['action']=="calcular"){
            $obj->calcular();
        }
    }

?>
