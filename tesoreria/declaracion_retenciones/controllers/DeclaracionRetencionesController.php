<?php
    require_once '../../../Librerias/core/Helpers.php';
    require_once '../../../PHPExcel/Classes/PHPExcel.php';
    require_once '../models/DeclaracionRetencionesModel.php';
    session_start();

    class DeclaracionRetencionesController extends DeclaracionRetencionesModel{
        public function getData(){
            if(!empty($_SESSION)){
                $request =$this->selectData();
                echo json_encode($request,JSON_UNESCAPED_UNICODE);
            }
            die();
        }
        public function calcular(){
            if(!empty($_SESSION)){
                $arrInfo = [];
                $arrData = json_decode($_POST['data'],true);
                $strFechaInicial = $_POST['fecha_inicial'];
                $strFechaFinal = $_POST['fecha_final'];
                $strDestino = $_POST['destino'];
                $parametros = $this->selectParametros();
                $totalData= count($arrData);
                for ($i=0; $i < $totalData ; $i++) {
                    $data = $arrData[$i];
                    if($data['sigla']=="R"){
                        if($data['id']=="34"){
                            $arrRete34 = $this->selectRete34($strFechaInicial,$strFechaFinal,$data);
                            array_push($arrInfo,$arrRete34);
                        }
                        $arrOrden = $this->selectOrdenPago($strFechaInicial,$strFechaFinal,$data,$strDestino,$parametros);
                        $arrPagoTerceros = $this->selectPagoTerceros($strFechaInicial,$strFechaFinal,$data,$strDestino);
                        if(!empty($arrOrden)){
                            array_push($arrInfo,$arrOrden);
                        }
                        if(!empty($arrPagoTerceros)){
                            array_push($arrInfo,$arrPagoTerceros);
                        }
                    }else if($data['sigla']=="I"){
                        $arrRecaudo = $this->selectRecaudos($strFechaInicial,$strFechaFinal,$data);
                        $arrRecaudoPropio = $this->selectRecaudosPropios($strFechaInicial,$strFechaFinal,$data);
                        $arrRecaudoTransferencia = $this->selectRecaudosTransferencias($strFechaInicial,$strFechaFinal,$data);
                        if(!empty($arrRecaudo)){
                            array_push($arrInfo,$arrRecaudo);
                        }
                        if(!empty($arrRecaudoPropio)){
                            array_push($arrInfo,$arrRecaudoPropio);
                        }
                        if(!empty($arrRecaudoTransferencia)){
                            array_push($arrInfo,$arrRecaudoTransferencia);
                        }
                    }
                }
                $arrInfo = $this->fixArrayInfo($arrInfo);
                $arrResponse = array("data"=>$arrInfo,"html"=>$this->getHtml($arrInfo['data']));
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
            die();
        }
        public function fixArrayInfo(array $arrInfo){
            $arrData = [];
            $total = 0;
            $cuentas = 0;
            foreach ($arrInfo as $cab) {
                foreach ($cab as $det) {
                    if($det['cuentas']>0 || $det['valor']>0){
                        $total+=$det['valor'];
                        $cuentas+=$det['cuentas'];
                        array_push($arrData,$det);
                    }
                }
            }
            return array(
                "total_cuentas"=>$cuentas,
                "total_valor"=>$total,
                "total_valor_letras"=>convertir($total)." PESOS M/CTE",
                "data"=>$arrData
            );
        }
        public function getHtml($data){
            $total = count($data);
            $retencion = !empty($data) ? $data[0]['nombre_descuentos'] : "";
            $acumulador = 0;
            $baseTotalRete = 0;
            $html ="";
            for ($i=0; $i < $total ; $i++) {
                $row = $data[$i];
                if($retencion != $row['nombre_descuentos']){
                    $html.= '
                    <tr class = "bg-success text-white">
                        <td colspan = "6" > Total de retencion/ingreso '.$retencion.'</td>
                        <td>'.number_format(round($baseTotalRete,0),0).'</td>
                        <td>'.number_format(round($acumulador,0),0).'</td>
                    </tr>';
                    $acumulador = 0;
                    $baseTotalRete = 0;
                }
                $html.='
                    <tr>
                        <td class="text-nowrap">'.$row['nombre'].'</td>
                        <td class="w-15">'.$row['tercero'].'</td>
                        <td class="text-nowrap">'.$row['nombre_tercero'].'</td>
                        <td>'.$row['bancos'].'</td>
                        <td>'.$row['cuenta_bancos'].'</td>
                        <td>'.$row['concepto'].'</td>
                        <td>'.number_format($row['cuentas']).'</td>
                        <td>'.number_format($row['valor']).'</td>
                    </tr>';
                $retencion = $row['nombre_descuentos'];
                $acumulador += round($row['valor'],0);
                $baseTotalRete += round($row['cuentas'],0);
                if($i == $total - 1){
                    $html.= '<tr class = "bg-success text-white" >
                            <td colspan = "6" >
                                Total de retencion/ingreso '.$retencion.'
                            </td>
                            <td>
                                '.number_format(round($baseTotalRete,0),0).'
                            </td>
                            <td>
                                '.number_format(round($acumulador,0),0).'
                            </td>
                        </tr>';
                    $acumulador = 0;
                    $baseTotalRete = 0;
                }
            }
            return $html;
        }
    }
    if($_POST){
        $obj = new DeclaracionRetencionesController();
        if($_POST['action']=="get"){
            $obj->getData();
        }else if($_POST['action']=="calcular"){
            $obj->calcular();
        }
    }

?>
