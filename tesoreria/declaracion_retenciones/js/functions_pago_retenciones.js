const URL ='tesoreria/declaracion_retenciones/controllers/PagoRetencionesController.php';
const URLEXPORT = 'tesoreria/declaracion_retenciones/controllers/PagoRetencionesExportController.php';
var app = new Vue({
    el:"#myapp",
    data() {
        return {
            isLoading:false,
            txtFechaInicial:new Date(new Date().setDate(1)).toISOString().split("T")[0],
            txtFechaFinal:new Date(new Date().getFullYear(), new Date().getMonth() + 1, 0).toISOString().split("T")[0],
            selectDestino:"",
            selectTipo:"T",
            selectMode:1,
            selectRetenciones:"",
            html:"",
            arrData:[],
            arrDataCopy:[],
            arrRetenciones:[],
            arrInfo:[],
            txtTotal:0,
            txtCuentas:0,
            txtTotalLetras:""
        }
    },
    mounted() {
        this.getData();
    },
    methods: {
        getData: async function(){
            const vueContext = this;
            const formData = new FormData();
            formData.append("action","get");
            formData.append("fecha_inicial",this.txtFechaInicial);
            formData.append("fecha_final",this.txtFechaFinal);
            formData.append("destino",this.selectDestino);
            formData.append("tipo",this.selectTipo);
            formData.append("retenciones",this.selectRetenciones);
            this.isLoading = true;
            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();
            this.arrData = objData;
            this.filter();
            this.isLoading = false;
        },
        filter:function(){
            const vueContext = this;
            this.arrDataCopy = this.arrData.filter(function(e){
                return e.destino == vueContext.selectDestino &&
                vueContext.selectTipo == (vueContext.selectTipo == "T" ? e.todo : e.sigla);
            });
            this.$refs.tableData.innerHTML = "";
            this.txtTotal = 0;
            this.txtTotalLetras = "";
            this.txtCuentas = 0;
            this.arrRetenciones = [];
        },
        add:function(){
            const vueContext = this;
            if(this.selectRetenciones == ""){
                this.arrRetenciones = [...this.arrDataCopy];
            }else{
                if(vueContext.arrRetenciones.length > 0){
                    let flag = true;
                    for (let i = 0; i < vueContext.arrRetenciones.length; i++) {
                        const element = this.arrRetenciones[i];
                        if(element.codigo == this.selectRetenciones){
                            flag = false;
                            break;
                        }
                    }
                    if(flag){
                        this.arrRetenciones.push(
                            vueContext.arrDataCopy.filter(e=>e.codigo == vueContext.selectRetenciones)[0]
                        );
                    }
                }else{
                    this.arrRetenciones.push(
                        vueContext.arrDataCopy.filter(e=>e.codigo == vueContext.selectRetenciones)[0]
                    );
                }
            }
            this.calcular();
        },
        del:function(codigo){
            let index = this.arrRetenciones.findIndex(e=>e.codigo == codigo);
            this.arrRetenciones.splice(index,1);
            this.calcular();
        },
        calcular: async function(){
            const vueContext = this;
            if(vueContext.arrRetenciones.length > 0){
                const formData = new FormData();
                formData.append("action","calcular");
                formData.append("fecha_inicial",this.txtFechaInicial);
                formData.append("fecha_final",this.txtFechaFinal);
                formData.append("destino",this.selectDestino);
                formData.append("data",JSON.stringify(this.arrRetenciones));
                this.isLoading = true;
                const response = await fetch(URL,{method:"POST",body:formData});
                const objData = await response.json();
                this.isLoading = false;
                this.arrInfo = objData.data;

                this.$refs.tableData.innerHTML = objData.html;
                this.txtTotal = this.arrInfo.total_valor;
                this.txtTotalLetras = this.arrInfo.total_valor_letras;
                this.txtCuentas = this.arrInfo.total_cuentas;
            }else{
                this.$refs.tableData.innerHTML = "";
                this.txtTotal = 0;
                this.txtTotalLetras = "";
                this.txtCuentas = 0;
            }
        },
        exportData:function(){
            const form = document.createElement("form");
            form.method ="post";
            form.target="_blank";
            form.action=URLEXPORT;

            function addField(name,value){
                const input = document.createElement("input");
                input.type="hidden";
                input.name=name;
                input.value = value;
                form.appendChild(input);
            }
            addField("action",this.selectMode == 1 ? "pdf" : "excel");
            addField("fecha_inicial",this.txtFechaInicial);
            addField("fecha_final",this.txtFechaFinal);
            addField("data",JSON.stringify(this.arrInfo));
            document.body.appendChild(form);
            form.submit();
            document.body.removeChild(form);
        },
        formatNumero: function(valor){
            return new Intl.NumberFormat("en-US", {style: "currency", currency: "USD"}).format(valor);
        },
        showTab:function(tab){
            let tabs = this.$refs.rTabs.children;
            let tabsContent = this.$refs.rTabsContent.children;
            for (let i = 0; i < tabs.length; i++) {
                tabs[i].classList.remove("active");
                tabsContent[i].classList.remove("active")
            }
            tabs[tab-1].classList.add("active");
            tabsContent[tab-1].classList.add("active")
        }
    },
    computed:{

    }
})
