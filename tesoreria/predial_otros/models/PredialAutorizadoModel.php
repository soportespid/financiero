<?php

require '../../../vendor/autoload.php';

use Laravel\Route;
use Laravel\DB;

    require_once '../../../Librerias/core/Helpers.php';
    require_once '../../../Librerias/core/Mysql.php';
    session_start();
    /*ini_set('display_errors', '1');
    ini_set('display_startup_errors', '1');
    error_reporting(E_ALL);*/
    class PredialAutorizadoModel extends Mysql{
        private $intIdUser;
        private $intId;
        function __construct(){
            parent::__construct();
        }

        /* public function selectEstado(int $intId) {
            $this->intId = $intId;

            //$tenant_db = DB::connectWithDomain(Route::currentDomain());
            $tenant_db = \Laravel\DB::connectWithDomain(\Laravel\Route::currentDomain());

            $stmt = $tenant_db->prepare('SELECT * FROM `estado_cuentas` WHERE `id` = ? LIMIT 1');
            $stmt->bind_param('i', $intId);

            if ((! $stmt->execute()) || $stmt->num_rows == 0) {
                $tenant_db->close();
                return [];
            }

            $result = $stmt->get_result();
            $data = $result->fetch_assoc();

            $tenant_db->close();

            return $data;
        } */

        public function selectEstado(int $intId) {
            $this->intId = $intId;
        
            // Asegurarse de que el método de conexión existe y devuelve un MySQLi válido
            $tenant_db = \Laravel\DB::connectWithDomain(\Laravel\Route::currentDomain());
        
            if (!$tenant_db) {
                throw new \Exception("Error de conexión con la base de datos.");
            }
        
            $stmt = $tenant_db->prepare('SELECT * FROM `estado_cuentas` WHERE `id` = ? LIMIT 1');
        
            if (!$stmt) {
                $tenant_db->close();
                throw new \Exception("Error en la preparación de la consulta.");
            }
        
            $stmt->bind_param('i', $intId);
        
            if (!$stmt->execute()) {
                $stmt->close();
                $tenant_db->close();
                throw new \Exception("Error en la ejecución de la consulta.");
            }
        
            $result = $stmt->get_result();
        
            if (!$result || $result->num_rows === 0) {
                $stmt->close();
                $tenant_db->close();
                return []; // Retorna un array vacío si no encuentra resultados
            }
        
            $data = $result->fetch_assoc();
        
            $stmt->close();
            $tenant_db->close();
        
            return $data;
        }

        /* public function insertFacturaPredial(int $intId, array $data) {
            $this->intId = $intId;
            $this->intIdUser = $_SESSION['idusuario'];

            $tenant_db = DB::connectWithDomain(Route::currentDomain());

            $stmt = $tenant_db->prepare('SELECT * FROM `factura_predials` WHERE `id` = ? LIMIT 1');
            $stmt->bind_param('i', $this->intId);

            if (! $stmt->execute()) {
                $tenant_db->close();
                return false;
            }

            if ($stmt->num_rows <= 0) {
                $stmt = $tenant_db->prepare('INSERT INTO `factura_predials` (`ip`, `data`, `user_id`, `created_at`, `updated_at`) VALUES (?, ?, ?, ?, ?)');
                $stmt->bind_param('ssiss', getRealIP(), json_encode($data, JSON_UNESCAPED_UNICODE), $this->intIdUser, date('Y-m-d H:i:s'), date('Y-m-d H:i:s'));

                if (! $stmt->execute()) {
                    $tenant_db->close();
                    return false;
                }

                $tenant_db->close();
                return '';
            } else {
                $tenant_db->close();
                return 'existe';
            }

            return false;
        } */

        /* public function insertFacturaPredial(int $intId, array $data) {
            $this->intId = $intId;
            $this->intIdUser = $_SESSION['idusuario'] ?? null; // Verificamos que la sesión esté definida
        
            if (!$this->intIdUser) {
                throw new \Exception("Usuario no autenticado.");
            }
        
            $tenant_db = DB::connectWithDomain(Route::currentDomain());
        
            if (!$tenant_db) {
                throw new \Exception("Error de conexión con la base de datos.");
            }
        
            // Verificar si la factura ya existe
            $stmt = $tenant_db->prepare('SELECT * FROM `factura_predials` WHERE `id` = ? LIMIT 1');
        
            if (!$stmt) {
                throw new \Exception("Error en la preparación de la consulta SELECT.");
            }
        
            $stmt->bind_param('i', $this->intId);
            
            if (!$stmt->execute()) {
                $stmt->close();
                $tenant_db->close();
                return false;
            }
        
            $result = $stmt->get_result(); // Obtener el resultado correctamente
            $exists = $result->num_rows > 0; // Contar filas
        
            $stmt->close(); // Cerrar el statement antes de una nueva consulta
        
            if ($exists) {
                $tenant_db->close();
                return 'existe';
            }
        
            // Insertar nueva factura
            $stmt = $tenant_db->prepare('INSERT INTO `factura_predials` (`ip`, `data`, `user_id`, `created_at`, `updated_at`) VALUES (?, ?, ?, ?, ?)');
        
            if (!$stmt) {
                throw new \Exception("Error en la preparación de la consulta INSERT.");
            }
        
            $ip = getRealIP();
            $jsonData = json_encode($data, JSON_UNESCAPED_UNICODE);
            $timestamp = date('Y-m-d H:i:s');
        
            $stmt->bind_param('ssiss', $ip, $jsonData, $this->intIdUser, $timestamp, $timestamp);
        
            if (!$stmt->execute()) {
                $stmt->close();
                $tenant_db->close();
                return false;
            }
            $insertedId = $stmt->insert_id;
        
            $stmt->close();
            $tenant_db->close();
        
            return true; // Ahora retornamos `true` en lugar de `''`
        } */

        /**
         * Inserta una nueva factura predial en la base de datos
         *
         * @param int $intId ID de la factura
         * @param array $data Datos de la factura
         * @return int 'id' de la factura
         * @throws \Exception Si hay errores de autenticación o base de datos
         */
        public function insertFacturaPredial(int $intId, array $data): int 
        {
            // Validación de datos de entrada
            if ($intId <= 0) {
                throw new \InvalidArgumentException("ID de factura no válido");
            }

            if (empty($data)) {
                throw new \InvalidArgumentException("Datos de factura vacíos");
            }

            $this->intId = $intId;
            $this->intIdUser = $_SESSION['idusuario'] ?? null;

            if (!$this->intIdUser) {
                throw new \Exception("Usuario no autenticado.");
            }

            $tenant_db = null;
            $stmt = null;

            try {
                $tenant_db = DB::connectWithDomain(Route::currentDomain());

                if (!$tenant_db) {
                    throw new \Exception("Error de conexión con la base de datos.");
                }

                // Iniciar transacción
                $tenant_db->begin_transaction();

                // Verificar existencia de factura
                $stmt = $tenant_db->prepare('
                    SELECT 1 
                    FROM `factura_predials` 
                    WHERE `id` = ? 
                    LIMIT 1
                ');

                if (!$stmt) {
                    throw new \Exception("Error en la preparación de la consulta SELECT.");
                }

                $stmt->bind_param('i', $this->intId);
                
                if (!$stmt->execute()) {
                    throw new \Exception("Error al ejecutar la consulta de verificación.");
                }

                $result = $stmt->get_result();
                
                if ($result->num_rows > 0) {
                    $tenant_db->rollback();
                    return  'existe';
                }

                $stmt->close();

                // Preparar inserción
                $stmt = $tenant_db->prepare('
                    INSERT INTO `factura_predials` (
                        `id`,
                        `ip`, 
                        `data`, 
                        `user_id`, 
                        `created_at`, 
                        `updated_at`
                    ) VALUES (?, ?, ?, ?, ?, ?)
                ');

                if (!$stmt) {
                    throw new \Exception("Error en la preparación de la consulta INSERT.");
                }

                $ip = getRealIP();
                $jsonData = json_encode($data, JSON_UNESCAPED_UNICODE);
                
                if ($jsonData === false) {
                    throw new \Exception("Error al codificar los datos JSON");
                }

                $timestamp = date('Y-m-d H:i:s');

                $stmt->bind_param('ississ', 
                    $this->intId,
                    $ip, 
                    $jsonData, 
                    $this->intIdUser, 
                    $timestamp, 
                    $timestamp
                );

                if (!$stmt->execute()) {
                    throw new \Exception("Error al insertar la factura: " . $stmt->error);
                }

                // Obtener el ID insertado
                $insertedId = $stmt->insert_id;

                // Si no se usa auto_increment, usar el ID proporcionado
                /* if ($insertedId === 0) {
                    $insertedId = $this->intId;
                } */

                // Confirmar transacción
                $tenant_db->commit();
                
                return $insertedId;

            } catch (\Exception $e) {
                // Revertir transacción en caso de error
                if ($tenant_db) {
                    $tenant_db->rollback();
                }
                throw $e;

            } finally {
                // Liberar recursos
                if ($stmt) {
                    $stmt->close();
                }
                if ($tenant_db) {
                    $tenant_db->close();
                }
            }
        }

        public function insertLiquidaPredial(array $data,int $idLiquidacion){
            $arrInfoAvaluo = $this->selectInfoPredio($data['codigo_catastro'],$data['documento']);
            $this->intId = $idLiquidacion;
            $arrLiquidacion = $data['liquidacion'];
            $arrVigencias = $arrLiquidacion['vigencias'];
            usort($arrVigencias,function($a,$b){return $a['vigencia'] > $b['vigencia']; });
            $strPeriodos = "";
            foreach ($arrVigencias as $e) {
                $strPeriodos.=" ".$e['vigencia'];
            }
            //Cabecera
            $strConcepto = "Años liquidados: ".$strPeriodos;
            $sql = "INSERT INTO tesoliquidapredial(idpredial,codigocatastral,fecha,vigencia,tercero,tasamora,descuento,tasapredial,totaliquida,totalpredial,totalbomb,
            totalmedio,totalinteres,intpredial,intbomb,intmedio,totaldescuentos,concepto,estado,ord,tot)
            VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
            $request = $this->insert($sql,[
                $this->intId,
                $data['codigo_catastro'],
                date("Y-m-d H:i:s"),
                $arrInfoAvaluo['vigencia'],
                $data['documento'],
                $data['interes_vigente'],
                $data['descuento_vigente'],
                $arrInfoAvaluo['tasa'],
                $arrLiquidacion['total_liquidacion'],
                $arrLiquidacion['total_predial'],
                $arrLiquidacion['total_bomberil'],
                $arrLiquidacion['total_ambiental'],
                $arrLiquidacion['total_intereses'],
                $arrLiquidacion['total_predial_intereses'],
                $arrLiquidacion['total_bomberil_intereses'],
                $arrLiquidacion['total_ambiental_intereses'],
                $arrLiquidacion['total_predial_descuento'],
                $strConcepto,
                'S',
                $data['orden'],
                $data['total']
            ]);
            return $request;
        }
        public function insertLiquidaPredialDet($intId,$arrVigencias,$codcatastral){
            $this->intId = $intId;
            foreach ($arrVigencias as $e) {
                $sql_det = "INSERT INTO tesoliquidapredial_det(idpredial,vigliquidada,avaluo,tasav,predial,intpredial,bomberil,
                intbomb,medioambiente,intmedioambiente,descuentos,totaliquidavig,estado)
                VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?)";
                $this->insert($sql_det,[
                    $this->intId,
                    $e['vigencia'],
                    $e['valor_avaluo'],
                    $e['tasa_por_mil'],
                    $e['predial'],
                    $e['predial_intereses'],
                    $e['bomberil'],
                    $e['bomberil_intereses'],
                    $e['ambiental'],
                    $e['ambiental_intereses'],
                    $e['predial_descuento'],
                    $e['total_liquidacion'],
                    'S'
                ]);

                $sql_det = "INSERT INTO tesoliquidapredial_desc(id_predial,cod_catastral,vigencia,descuentointpredial,descuentointbomberil,
                descuentointambiental,val_alumbrado,estado,user)
                VALUES(?,?,?,?,?,?,?,?,?)";
                $this->insert($sql_det,[
                    $this->intId,
                    $codcatastral,
                    $e['vigencia'],
                    $e['predial_descuento_intereses'],
                    $e['bomberil_descuento_intereses'],
                    $e['ambiental_descuento_intereses'],
                    $e['alumbrado'],
                    "S",
                    $_SESSION['cedulausu'],
                ]);
            }
        }

        public function selectInfoPredio($codcatastral, $tercero) {
            $tenant_db = DB::connectWithDomain(Route::currentDomain());

            $stmt = $tenant_db->prepare(<<<EOF
SELECT
p.id,
p.codigo_catastro,
p.total,
pp.documento,
pp.nombre_propietario as nombre_propietario,
pp.orden,
pa.valor_avaluo as avaluo,pa.tasa_por_mil as tasa,
pa.vigencia,
tp.direccion,tp.hectareas,
tp.metros_cuadrados,
tp.area_construida,
tp.created_at,
de.nombre as destino_economico,
cd.codigo as codigo_destino_economico,
pt.nombre as predio_tipo,
pt.codigo as predio_tipo_codigo
FROM predios p
LEFT JOIN predio_propietarios pp ON p.id = pp.predio_id AND (pp.documento = ? OR pp.predio_id = p.id)
INNER JOIN predio_avaluos pa ON p.id = pa.predio_id
INNER JOIN predio_informacions tp ON p.id = tp.predio_id
INNER JOIN predio_tipos pt ON tp.predio_tipo_id = pt.id
INNER JOIN codigo_destino_economicos cd ON cd.id = tp.codigo_destino_economico_id
INNER JOIN destino_economicos de ON de.id = cd.destino_economico_id
WHERE p.codigo_catastro = ?
AND tp.created_at = (SELECT MAX(created_at) FROM predio_informacions WHERE predio_id = p.id)
AND pa.vigencia = (SELECT MAX(vigencia) FROM predio_avaluos WHERE predio_id = p.id)
GROUP BY p.id
EOF);

            $stmt->bind_param('ss', $tercero, $codcatastral);

            if (! $stmt->execute()) {
                $tenant_db->close();
                return [];
            }

            $result = $stmt->get_result();
            $data = $result->fetch_assoc();

            $tenant_db->close();

            return $data;
        }
    }
?>
