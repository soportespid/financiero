<?php
    require_once '../../../Librerias/core/Helpers.php';
    require_once '../../../PHPExcel/Classes/PHPExcel.php';
    require_once '../models/PredialAutorizadoModel.php';
    require_once "../../../Librerias/vendor/autoload.php";
    session_start();

    class PredialAutorizadoController extends PredialAutorizadoModel{
        public function getData(){
            if(!empty($_SESSION)){
                $intId = intval($_POST['codigo']);
                $arrEstado = $this->selectEstado($intId);
                $arrEstado['data']=json_decode($arrEstado['data']);
                echo json_encode($arrEstado,JSON_UNESCAPED_UNICODE);
            }
        }
        public function save(){
            if(!empty($_SESSION)){
                if(empty($_POST['codigo'])){
                    $arrResponse = array("status"=>false,"msg"=>"Error de datos");
                }else{
                    $intId = intval($_POST['codigo']);
                    $arrEstado = $this->selectEstado($intId);
                    if(!empty($arrEstado)){
                        $arrDet = json_decode($_POST['data'],true);
                        $arrVigencias = $arrDet['liquidacion']['vigencias'];
                        if(!empty($arrVigencias)){
                            $id = searchConsecVps("factura_predials","id");
                            $totalBomberil = 0;
                            $totalBomberilIntereses = 0;
                            $totalBomberilDescuentoIntereses = 0;
                            $totalAlumbrado = 0;
                            $totalAmbiental = 0;
                            $totalAmbientalIntereses = 0;
                            $totalAmbientalDescuentoIntereses = 0;
                            $totalIntereses = 0;
                            $totalDescuentos = 0;
                            $totalLiquidacion = 0;
                            $totalPredial = 0;
                            $totalPredialDescuento = 0;
                            $totalPredialIntereses = 0;
                            $totalPredialDescuentoIntereses = 0;
                            $totalAvaluo = 0;
                            $totalInteresesSinDescuento = 0;
                            $totalVigencias = count($arrVigencias);
                            for ($i=0; $i < $totalVigencias ; $i++) {
                                $arrVigencias[$i]['intereses_totales_sin_descuento'] = $arrVigencias[$i]['total_intereses'];
                                $totalBomberil +=$arrVigencias[$i]['bomberil'];
                                $totalBomberilIntereses +=$arrVigencias[$i]['bomberil_intereses'];
                                $totalBomberilDescuentoIntereses +=$arrVigencias[$i]['bomberil_descuento_intereses'];
                                $totalAmbiental +=$arrVigencias[$i]['ambiental'];
                                $totalAmbientalIntereses += $arrVigencias[$i]['ambiental_intereses'];
                                $totalAmbientalDescuentoIntereses += $arrVigencias[$i]['ambiental_descuento_intereses'];
                                $totalLiquidacion += $arrVigencias[$i]['total_liquidacion'];
                                $totalPredial += $arrVigencias[$i]['predial'];
                                $totalPredialDescuento += $arrVigencias[$i]['predial_descuento'];
                                $totalPredialIntereses += $arrVigencias[$i]['predial_intereses'];
                                $totalPredialDescuentoIntereses += $arrVigencias[$i]['predial_descuento_intereses'];
                                $totalAlumbrado +=$arrVigencias[$i]['alumbrado'];
                                $totalIntereses+=$arrVigencias[$i]['total_intereses'];
                                $totalAvaluo+=$arrVigencias[$i]['valor_avaluo'];
                                $totalInteresesSinDescuento+=$arrVigencias[$i]['intereses_totales_sin_descuento'];

                                $descuentos = $arrVigencias[$i]['predial_descuento_intereses']+$arrVigencias[$i]['ambiental_descuento_intereses']+$arrVigencias[$i]['bomberil_descuento_intereses'];
                                $totalDescuentos += $descuentos;
                            }
                            $arrDet['totales'] = array(
                                "bomberil"=>$totalBomberil,
                                "alumbrado"=>$totalAlumbrado,
                                "ambiental"=>$totalAmbiental,
                                "intereses"=>$totalIntereses,
                                "descuentos"=>$totalDescuentos,
                                "liquidacion"=>$totalLiquidacion,
                                "predial"=>$totalPredial,
                                "total_avaluo"=>$totalAvaluo,
                                "intereses_totales_sin_descuento"=>$totalInteresesSinDescuento
                            );
                            $arrDet['liquidacion']['vigencias'] = $arrVigencias;
                            $arrDet['liquidacion']['total_liquidacion'] =$totalLiquidacion;
                            $arrDet['liquidacion']['total_predial']  = $totalPredial;
                            $arrDet['liquidacion']['total_predial_descuento'] =$totalPredialDescuento;
                            $arrDet['liquidacion']['total_predial_intereses']  = $totalPredialIntereses;
                            $arrDet['liquidacion']['total_predial_descuento_intereses'] =$totalPredialDescuentoIntereses;
                            $arrDet['liquidacion']['total_bomberil'] = $totalBomberil;
                            $arrDet['liquidacion']['total_bomberil_intereses'] = $totalBomberilIntereses;
                            $arrDet['liquidacion']['total_bomberil_descuento_intereses'] = $totalBomberilDescuentoIntereses;
                            $arrDet['liquidacion']['total_ambiental'] =$totalAmbiental;
                            $arrDet['liquidacion']['total_ambiental_intereses'] =$totalAmbientalIntereses;
                            $arrDet['liquidacion']['total_ambiental_descuento_intereses'] = $totalAmbientalDescuentoIntereses;
                            $arrDet['liquidacion']['total_alumbrado'] = $totalAlumbrado;
                            $arrDet['liquidacion']['total_intereses'] = $totalIntereses;
                            $arrDet['factura_pagada']=false;
                            $arrDet['facturado_desde'] = $arrVigencias[count($arrVigencias)-1]['vigencia'];
                            $arrDet['facturado_hasta'] = $arrVigencias[0]['vigencia'];

                            $dateTime = new DateTime('now', new DateTimeZone('UTC'));
                            $isoDate = $dateTime->format('Y-m-d\TH:i:s.u\Z');
                            $arrDet['pague_hasta']  = $isoDate;
                            $arrDet['private'] = true;
                            $arrDet['barcode'] = $this->genBarcodePredial($arrDet['totales']['liquidacion'],$id);
                            $request = $this->insertFacturaPredial($id,$arrDet);
                            if($request > 0){
                                insertAuditoria("teso_auditoria","teso_funciones_id",7,"Crear",$request);
                                $request = $this->insertLiquidaPredial($arrDet,$request);
                                if($request > 0){
                                    $this->insertLiquidaPredialDet($request,$arrDet['liquidacion']['vigencias'],$arrDet['codigo_catastro']);
                                    $arrResponse = array("status"=>true,"msg"=>"La factura se ha generado correctamente","id"=>$request);
                                }
                            }else if($request =="existe"){
                                $arrResponse = array("status"=>false,"msg"=>"El consecutivo generado para facturar el estado de cuenta ya existe, intente de nuevo");
                            }else{
                                $arrResponse = array("status"=>false,"msg"=>"No se ha podido guardar la factura, intente de nuevo.");
                            }
                        }else{
                            $arrResponse = array("status"=>false,"msg"=>"Debe seleccionar al menos una vigencia.");
                        }
                    }else{
                        $arrResponse = array("status"=>false,"msg"=>"El estado de cuenta no existe, intente con otro.");
                    }
                }
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
            die();
        }
        public function genBarcodePredial($total,$id){
            $con = new Mysql();
            $generator = new Picqer\Barcode\BarcodeGeneratorPNG();
            $sql = "SELECT * FROM codigosbarras WHERE tipo = '01' AND estado = 'S'";
            $request = $con->select($sql);

            $codeImg = chr(241).$request['codini'].$request['codigo']."802001".
            sprintf('%07d', $id)."111005001".chr(241) . '3900' . sprintf('%010d', $total).chr(241) . '96' .date("Ymd");

            $codeHuman = "(".$request['codini'].")".$request['codigo']."(8020)01".
            sprintf('%07d', $id)."111005001".'(3900)' . sprintf('%010d', $total).'(96)'.date("Ymd");
            return array("img"=>base64_encode($generator->getBarcode($codeImg, $generator::TYPE_CODE_128)),"human"=>$codeHuman);
        }
    }
    if($_POST){
        $obj = new PredialAutorizadoController();
        if($_POST['action']=="save"){
            $obj->save();
        }else if($_POST['action']=="get"){
            $obj->getData();
        }
    }

?>
