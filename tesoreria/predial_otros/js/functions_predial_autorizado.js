const URL ='tesoreria/predial_otros/controllers/PredialAutorizadoController.php';
var app = new Vue({
    el:"#myapp",
    data() {
        return {
            isLoading:false,
            isModal:false,
            txtSearch:"",
            txtCodigo:"",
            txtCodigoFactura:"",
            txtResultados:0,
            objEstado:{codigo_catastro:""},
            objTotales:{
                total_avaluo: 0,
                total_predial: 0,
                total_incentivo: 0,
                total_recaudo: 0,
                total_predial_intereses: 0,
                total_predial_descuento_intereses:0,
                total_bomberil: 0,
                total_intereses_bomberil: 0,
                total_descuento_intereses_bomberil: 0,
                total_ambiental: 0,
                total_intereses_ambiental: 0,
                total_descuento_intereses_ambiental: 0,
                total_alumbrado:0,
                total_liquidacion:0
            },
            isCheckAll:true,
            arrVigencias:[]
        }
    },
    mounted() {

    },
    methods: {
        getData:async function(){
            const formData = new FormData();
            formData.append("action","get");
            formData.append("codigo",this.txtCodigo);
            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();
            this.objEstado = objData.data;
            this.arrVigencias = this.objEstado.liquidacion.vigencias;
            this.getTotales();
            console.log(objData);
        },
        getTotales:function(){
            this.objTotales ={
                total_avaluo: 0,
                total_predial: 0,
                total_incentivo: 0,
                total_recaudo: 0,
                total_predial_intereses: 0,
                total_predial_descuento_intereses:0,
                total_bomberil: 0,
                total_intereses_bomberil: 0,
                total_descuento_intereses_bomberil: 0,
                total_ambiental: 0,
                total_intereses_ambiental: 0,
                total_descuento_intereses_ambiental: 0,
                total_alumbrado:0,
                total_liquidacion:0
            }
            this.arrVigencias.forEach(e => {
                if(e.selected){
                    this.objTotales.total_avaluo += e.valor_avaluo;
                    this.objTotales.total_predial += e.predial_descuento+e.predial;
                    this.objTotales.total_incentivo += e.predial_descuento;
                    this.objTotales.total_recaudo += e.predial;
                    this.objTotales.total_predial_intereses += e.predial_intereses;
                    this.objTotales.total_predial_descuento_intereses += e.predial_descuento_intereses;
                    this.objTotales.total_bomberil += e.bomberil;
                    this.objTotales.total_intereses_bomberil += e.bomberil_intereses;
                    this.objTotales.total_descuento_intereses_bomberil += e.bomberil_descuento_intereses;
                    this.objTotales.total_ambiental += e.ambiental;
                    this.objTotales.total_intereses_ambiental += e.ambiental_intereses;
                    this.objTotales.total_descuento_intereses_ambiental += e.ambiental_descuento_intereses;
                    this.objTotales.total_alumbrado += e.alumbrado;
                    this.objTotales.total_liquidacion += e.total_liquidacion;
                }
            });
        },
        changeStatus:function(index){
            const vigencia = this.arrVigencias[index];
            vigencia.selected = !vigencia.selected;
            this.arrVigencias[index] = vigencia;
            const flag = this.arrVigencias.every((e)=>e.selected == true);
            this.isCheckAll = flag;
            this.getTotales();
        },
        changeAll:function(){
            this.arrVigencias.forEach(e => {
                e.selected = this.isCheckAll
            });
            this.getTotales();
        },
        save:async function(){
            if(app.txtCodigo == ""){
                Swal.fire("Atención!","Debe digitar el número del estado de cuenta.","warning");
                return false;
            }
            const arrVigencias = app.arrVigencias.filter(function(e){return e.selected;});
            if(arrVigencias == 0){
                Swal.fire("Atención!","Debe seleccionar al menos una vigencia.","warning");
                return false;
            }
            Swal.fire({
                title:"¿Estás segur@ de guardar?",
                text:"",
                icon: 'warning',
                showCancelButton:true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText:"Sí, guardar",
                cancelButtonText:"No, cancelar"
            }).then(async function(result){
                if(result.isConfirmed){
                    const estadoCuenta = {...app.objEstado};
                    estadoCuenta.liquidacion.vigencias = arrVigencias;
                    const formData = new FormData();
                    formData.append("action","save");
                    formData.append("codigo",app.txtCodigo);
                    formData.append("data",JSON.stringify(estadoCuenta));
                    app.isLoading = true;
                    const response = await fetch(URL,{method:"POST",body:formData});
                    const objData = await response.json();
                    app.isLoading = false;
                    if(objData.status){
                        app.txtCodigoFactura = "Factura No. "+objData.id +" generado para el estado de cuenta No."+app.txtCodigo;
                        app.txtCodigo = "";
                        app.objEstado={codigo_catastro:""}
                        app.objTotales={
                            total_avaluo: 0,
                            total_predial: 0,
                            total_incentivo: 0,
                            total_recaudo: 0,
                            total_predial_intereses: 0,
                            total_predial_descuento_intereses:0,
                            total_bomberil: 0,
                            total_intereses_bomberil: 0,
                            total_descuento_intereses_bomberil: 0,
                            total_ambiental: 0,
                            total_intereses_ambiental: 0,
                            total_descuento_intereses_ambiental: 0,
                            total_alumbrado:0,
                            total_liquidacion:0
                        },
                        Swal.fire("Guardado",objData.msg,"success");
                    }else{
                        Swal.fire("Error",objData.msg,"error");
                    }
                }
            });
        },
        formatNum: function(valor){
            return new Intl.NumberFormat("en-US", {style: "currency", currency: "USD"}).format(valor);
        },
    },
    computed:{

    }
})
