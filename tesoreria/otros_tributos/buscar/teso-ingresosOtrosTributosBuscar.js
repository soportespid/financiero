const URL ='tesoreria/otros_tributos/buscar/teso-ingresosOtrosTributosBuscar.php';

var app = new Vue({
    el:"#myapp",
    data() {
        return {
            isLoading:false,
            intResults:0,
            intTotalPages:1,
            intPage:1,
            strSearch:"",
            arrData:[]

        }
    },
    mounted() {
        this.search();
    },
    methods: {
        search: async function(page=1){
            if(page <= 0){
                page = 1;
                this.intPage = page;
            }else if(page > this.intTotalPages){
                page = this.intTotalPages;
                this.intPage = page;
            }

            this.isLoading = true;
            const formData = new FormData();
            formData.append("action","search");
            formData.append("search",this.strSearch);
            formData.append("page",page);
            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();
            this.arrData = objData.data;
            this.intResults = objData.total;
            this.intTotalPages = objData.total_pages;
            this.isLoading = false;
        },
        printExcel:function(){
            window.open('teso-ingresosOtrosTributosExcel.php?search='+this.strSearch,"_blank");
        },
        editItem: function(codigo){
            window.location.href="teso-ingresosOtrosTributosEditar.php?id="+codigo;
        }
    },
})
