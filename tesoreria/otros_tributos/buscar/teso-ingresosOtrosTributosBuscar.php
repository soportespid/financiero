<?php
    require_once '../../../comun.inc';
    require '../../../funciones.inc';
    require '../../../funcionesSP.inc.php';
    /*ini_set('display_errors', '1');
    ini_set('display_startup_errors', '1');
    error_reporting(E_ALL);*/
    if($_POST){
        $obj = new Articulo();
        if($_POST['action']=="search"){
            $currentPage = intval($_POST['page']);
            $obj->search($_POST['search'],$currentPage);
        }
    }

    class Articulo{
        private $linkbd;
        private $strCodigoArticulo;
        private $strCodigoGrupo;
        private $strCodigoBien;
        public function __construct() {
            $this->linkbd = conectar_v7();
            $this->linkbd->set_charset("utf8");
        }
        public function search(string $search,int $currentPage){
            $perPage = 200;
            $startRows = ($currentPage-1) * $perPage;
            $totalRows = 0;
            $sql="SELECT i.codigo, i.tipo,i.nombre,d.concepto,UPPER(i.tipoIngreso) as tipoingreso,i.terceros,d.cuentapres,i.is_tercero,is_cuenta FROM tesoingresos i
            INNER JOIN tesoingresos_det d
            ON i.codigo = d.codigo
            WHERE i.estado<> '' AND d.modulo = '4'
            AND d.vigencia = (SELECT MAX(vigencia) FROM tesoingresos_det WHERE codigo = i.codigo)
            AND (i.nombre LIKE '$search%' OR i.codigo LIKE '$search%')
            GROUP BY i.codigo ORDER BY CAST(i.codigo AS INT) ASC LIMIT $startRows, $perPage";

            $request = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
            $totalCount = count($request);
            $opcionTercero = [
                '' => 'OTROS',
                'D' => 'DEPARTAMENTAL',
                'M' => 'MUNICIPAL',
                'N' => 'NACIONAL',
            ];

            for ($i=0; $i < $totalCount ; $i++) {
                $isTercero = $request[$i]['is_tercero'] == 1 ? "TERCEROS" : "PROPIO";
                $request[$i]['terceros'] = $opcionTercero[$request[$i]['terceros']]."-".$isTercero;
                if($request[$i]['tipo'] == "S"){
                    $request[$i]['estado_cuenta'] = $this->validCuenta($request[$i]['cuentapres']);
                    $request[$i]['concepto'] = $this->selectConcepto($request[$i]['concepto']);
                }else{
                    $request[$i]['concepto'] = "-";
                }
            }
            $totalRows = intval(mysqli_query($this->linkbd,
                "SELECT count(*) as total
                FROM tesoingresos i
                INNER JOIN tesoingresos_det d
                ON i.codigo = d.codigo
                WHERE i.estado<> '' AND d.modulo = '4'
                AND d.vigencia = (SELECT MAX(vigencia) FROM tesoingresos_det WHERE codigo = i.codigo)
                AND (i.nombre LIKE '$search%' OR i.codigo LIKE '$search%')
                GROUP BY i.codigo ORDER BY CAST(i.codigo AS INT) ASC")->fetch_assoc()['total']);
            $totalPages = $totalRows > 0 ? ceil($totalRows/$perPage) : 1;
            $arrResponse = array("status"=>true,"data"=>$request,"total"=>$totalRows,"total_pages"=>$totalPages);
            echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            die();
        }
        public function selectConcepto($codigo){
            $string = $codigo."-";
            $sql = "SELECT nombre FROM conceptoscontables WHERE modulo='4' AND tipo='C' AND codigo='$codigo'";
            $string .= mysqli_query($this->linkbd,$sql)->fetch_assoc()['nombre'];
            return $string;
        }
        public function validCuenta($cuenta){
            $sql = "SELECT * FROM cuentasingresosccpetseleccionadas WHERE codigo = '$cuenta'";
            $request = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
            $estado = 1;
            if(empty($request)) $estado = 2;
            return $estado;
        }
    }
?>
