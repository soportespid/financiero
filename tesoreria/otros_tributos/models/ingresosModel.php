<?php
    require_once '../../../Librerias/core/Helpers.php';
    require_once '../../../Librerias/core/Mysql.php';
    session_start();
    /*ini_set('display_errors', '1');
    ini_set('display_startup_errors', '1');
    error_reporting(E_ALL);*/
    class ingresosModel extends Mysql{

        function __construct(){
            parent::__construct();
        }
        public function insertTesoPrecio($codigo,$precio){
            $fecha=date('Y-m-d h:i:s');
			//Desactiva cualquier precio antiguo con este mismo código
            $sql="UPDATE tesoingresos_precios SET estado=? WHERE ingreso='$codigo'";
            $request = $this->update($sql,['N']);

            //Ingresa valores del nuevo precio con este código
            $sql="INSERT INTO tesoingresos_precios (ingreso,precio,fecha,estado) VALUES (?,?,?,?)";
            $request = $this->insert($sql,[$codigo,$precio,$fecha,'S']);
            return $request;
        }
        public function insertTesoFuentes($id,$cabecera,$detalle){
            $fuentes = $detalle['fuentes'];
            $totalFuentes = count($fuentes);
            $sql = "UPDATE tesoingresos_fuentes SET estado='N' WHERE codigo = '$id'";
			$request = $this->update($sql,['N']);
            for ($i=0; $i < $totalFuentes; $i++) {
                $codigo_fuente = $fuentes[$i]['codigo'];
                $sql = "INSERT INTO tesoingresos_fuentes (codigo, fuente, estado) VALUES (?,?,?)";
                $request = $this->update($sql,[$id,$codigo_fuente,'S']);
            }
            return intval($request);
        }
        public function insertTesoIngresoDet($id,$cabecera,$detalle){
            $clasificador ="";
            $cuenta = "";
            $this->delete("DELETE FROM tesoingresos_det WHERE codigo ='$cabecera[codigo]'");
            if($cabecera['terceros'] == 2){
                $cuenta= $detalle['cuenta'];
                $clasificador = $detalle['clasificador'];
            }
            if($cabecera['tipo']=="C"){
                $cuentas = $detalle['cuentas'];
                $totalCuentas = count($cuentas);
                for ($i=0; $i < $totalCuentas ; $i++) {
                    $idConcepto = $cuentas[$i]['id_concepto'];
                    $tipoConcepto = $cuentas[$i]['tipo_concepto'];
                    $porcentaje = $cuentas[$i]['porcentaje'];
                    $nroCuenta = $cuentas[$i]['cuenta'];
                    $clasificador = $cuentas[$i]['cuenta_clasificadora'];
                    $fuente = $cuentas[$i]['fuente'];
                    $seccion = $cuentas[$i]['seccion'];
                    $sql="INSERT INTO tesoingresos_det (codigo,concepto,modulo,tipoconce,porcentaje,cuentapres,cuenta_clasificadora,estado,fuente,seccion)
                    VALUES (?,?,?,?,?,?,?,?,?,?)";
                    $arrData = [
                        $id,
                        $idConcepto."-",
                        '4',
                        $tipoConcepto,
                        $porcentaje,
                        $nroCuenta,
                        $clasificador,
                        'S',
                        $fuente,
                        $seccion
                    ];
                    $request = intval($this->insert($sql,$arrData));
                }
            }else{
                $sql="INSERT INTO tesoingresos_det (codigo,concepto,modulo,tipoconce,porcentaje,cuentapres,cuenta_clasificadora,estado)
                VALUES (?,?,?,?,?,?,?,?)";
                $arrData = [$id,$detalle['concepto'],'4', $detalle['tipo_concepto'], $detalle['porcentaje'],$cuenta,$clasificador,'S'];
                $request = intval($this->insert($sql,$arrData));
            }
            return $request;
        }
        public function insertTesoIngreso($arrCabecera){
            $id = $this->selectConsecutivo();
            insertAuditoria("teso_auditoria","teso_funciones_id",1,"Crear",$id);
            $sql = "INSERT INTO tesoingresos (codigo,nombre,tipo,estado,terceros,tipoIngreso,gravado,is_tercero,is_cuenta) VALUES(?,?,?,?,?,?,?,?,?)";
            $arrData = [
                $id,
                $arrCabecera['nombre'],
                $arrCabecera['tipo'],
                'S',
                $arrCabecera['destino'],
                $arrCabecera['tipo_ingreso'],
                $arrCabecera['contabilizar'],
                $arrCabecera['terceros'],
                $arrCabecera['is_presupuesto']
            ];
            $this->insert($sql,$arrData);
            return $id;
        }
        public function updateTesoIngreso($arrCabecera){
            insertAuditoria("teso_auditoria","teso_funciones_id",1,"Editar",$arrCabecera['codigo']);
            $sql = "UPDATE tesoingresos SET codigo=?,nombre=?,tipo=?,estado=?,terceros=?,is_tercero=?,
            tipoIngreso=?,gravado=?, is_cuenta=?
            WHERE codigo = '$arrCabecera[codigo]'";
            $arrData = [
                $arrCabecera['codigo'],
                $arrCabecera['nombre'],
                $arrCabecera['tipo'],
                'S',
                $arrCabecera['destino'],
                $arrCabecera['terceros'],
                $arrCabecera['tipo_ingreso'],
                $arrCabecera['contabilizar'],
                $arrCabecera['is_presupuesto']
            ];
            $request = intval($this->update($sql,$arrData));
            return $request;
        }
        public function selectConsecutivo(){
            $sql = "SELECT MAX(CAST(codigo AS INT)) as codigo FROM tesoingresos WHERE codigo NOT REGEXP '^[a-z]' ORDER BY codigo DESC";
            $request = $this->select($sql)['codigo']+1;
            return $request;
        }
        public function selectPrecio($codigo){
            $sql ="SELECT precio FROM tesoingresos_precios WHERE estado='S' AND ingreso='$codigo'";
            $request = $this->select($sql)['precio'];
            return $request;
        }
        public function selectConcepto($codigo){
            $string = $codigo."-";
            $sql = "SELECT nombre FROM conceptoscontables WHERE modulo='4' AND tipo='C' AND codigo='$codigo'";
            $string .= $this->select($sql)['nombre'];
            return $string;
        }
        public function selectNombreClasificador($clasificador,$codigo){
            $sql ="";
            if($clasificador == 1){
                $sql  = "SELECT nombre FROM ccpet_cuin WHERE codigo_cuin = '$codigo'";
            }else if($clasificador == 2){
                $sql="SELECT titulo as nombre FROM ccpetbienestransportables WHERE grupo = '$codigo'";

            }else if($clasificador == 3){
                $sql="SELECT titulo as nombre FROM ccpetservicios WHERE grupo = '$codigo'";
            }
            $request = $this->select($sql)['nombre'];
            return $request;
        }
        public function selectIngresoDet($codigo,$flag=false){
            $sql="SELECT
            d.codigo,
            d.concepto,
            d.tipoconce,
            d.porcentaje,
            d.cuentapres,
            d.cuenta_clasificadora,
            d.seccion,
            c.nombre,
            d.fuente
            FROM tesoingresos_det d
            LEFT JOIN cuentasingresosccpetseleccionadas c
            ON d.cuentapres = c.codigo
            WHERE d.codigo = '$codigo'";
            if($flag){
                $request = $this->select_all($sql);
            }else{
                $request = $this->select($sql);
            }
            return $request;
        }
        public function selectIngresoFuentes($codigo){
            $sql = "SELECT f.fuente as codigo, c.nombre
            FROM tesoingresos_fuentes f
            INNER JOIN ccpet_fuentes_cuipo c
            ON c.codigo_fuente = f.fuente
            WHERE f.codigo = '$codigo' AND estado ='S'";
            $request = $this->select_all($sql);
            return $request;
        }
        public function selectIngreso($codigo){
            $sql = "SELECT codigo,nombre,tipo,terceros,tipoIngreso,gravado,is_tercero,is_cuenta FROM tesoingresos WHERE codigo = '$codigo'";
            $request = $this->select($sql);
            return $request;
        }
        public function selectSeccionesPresupuestales(){
            $sql = "SELECT id_seccion_presupuestal as codigo, nombre FROM pptoseccion_presupuestal WHERE estado='S'";
            $request = $this->select_all($sql);
            return $request;
        }
        public function selectServicios($search){
            $sql = "SELECT grupo as codigo, titulo as nombre
                    FROM ccpetservicios
                    WHERE version = (SELECT MAX(version) FROM ccpetbienestransportables) AND LENGTH(grupo) = 5 AND (grupo LIKE '$search%' OR titulo LIKE '%$search%')
                    ORDER BY grupo ASC"
            ;
            $request['data'] = $this->select_all($sql);
            $request['results'] = $this->select("SELECT count(*) as total
                FROM ccpetservicios
                WHERE version = (SELECT MAX(version) FROM ccpetbienestransportables) AND LENGTH(grupo) = 5 AND (grupo LIKE '$search%' OR titulo LIKE '%$search%')"
                )['total'];
            return $request;

        }
        public function selectBienes($search){
            $sql = "SELECT grupo as codigo, titulo as nombre
                    FROM ccpetbienestransportables
                    WHERE version = (SELECT MAX(version) FROM ccpetbienestransportables) AND LENGTH(grupo) = 7 AND (grupo LIKE '$search%' OR titulo LIKE '%$search%')
                    ORDER BY grupo ASC"
            ;
            $request['data'] = $this->select_all($sql);
            $request['results'] = $this->select("SELECT count(*) as total
                FROM ccpetbienestransportables
                WHERE version = (SELECT MAX(version) FROM ccpetbienestransportables) AND LENGTH(grupo) = 7 AND (grupo LIKE '$search%' OR titulo LIKE '%$search%')"
                )['total'];
            return $request;

        }
        public function selectFuentes($search){
            $sql="SELECT codigo_fuente as codigo, nombre
            FROM ccpet_fuentes_cuipo
            WHERE version = '1' AND
            (codigo_fuente LIKE '$search%' OR nombre LIKE '%$search%')
            ORDER BY codigo_fuente DESC";
            $request['data'] = $this->select_all($sql);
            $request['results'] = $this->select("SELECT count(*) as total
                FROM ccpet_fuentes_cuipo
                WHERE version = '1' AND
                (codigo_fuente LIKE '$search%' OR nombre LIKE '%$search%')"
                )['total'];
            return $request;
        }
        public function selectCuin($search){
            $sql = "SELECT id_entidad,
            nit,
            nombre,
            sector,
            subsector,
            tipo,
            supra_regional,
            nivel_territorial,
            depto,municipio,
            consecutivo,
            codigo_cuin as codigo
            FROM ccpet_cuin
            WHERE version=(SELECT MAX(version) FROM ccpet_cuin) AND
            (nit LIKE '%$search%' OR codigo_cuin LIKE '$search%' OR nombre LIKE '%$search%') ORDER BY id"
            ;
            $request['data'] = $this->select_all($sql);
            $request['results'] = $this->select("SELECT count(*) as total
                FROM ccpet_cuin
                WHERE version=(SELECT MAX(version) FROM ccpet_cuin) AND
                (nit LIKE '%$search%' OR codigo_cuin LIKE '$search%' OR nombre LIKE '%$search%')"
                )['total'];
            return $request;
        }
        public function selectClasificador($cuenta,$flag=false){
            $sql = "SELECT clasificadores FROM ccpetprogramarclasificadores WHERE cuenta = '$cuenta'";
            $request = $this->select($sql)['clasificadores'];
            if($flag){
                return $request;
            }else{
                echo json_encode($request,JSON_UNESCAPED_UNICODE);
                die();
            }
        }
        public function selectConceptos(){
            $sql = "SELECT codigo,nombre,tipo FROM conceptoscontables WHERE modulo='4' AND tipo='C' ORDER BY codigo";
            $request = $this->select_all($sql);
            return $request;
        }
        public function selectCuentas($search){
            $sql = "SELECT codigo, nombre, tipo, nivel
            FROM cuentasingresosccpetseleccionadas
            WHERE nombre like '$search%' OR codigo like '$search%'";
            $request['data'] = $this->select_all($sql);
            $request['results'] = $this->select("SELECT count(*) as total
                FROM cuentasingresosccpetseleccionadas
                WHERE nombre like '$search%' OR codigo like '$search%'")['total'];
            return $request;
        }

    }
?>
