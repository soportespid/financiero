const URL ='tesoreria/otros_tributos/controllers/ingresosController.php';

var app = new Vue({
    el:"#myapp",
    data() {
        return {
            isLoading:false,
            isModal:false,
            isModalServicios:false,
            isModalBienes:false,
            isModalCuin:false,
            isModalFuente:false,
            isModalFuenteComp:false,
            selectTerceros:2,
            selectSimple:"S",
            selectConcepto:"",
            selectDestino:"",
            selectTipoIngreso:"normal",
            selectContabilizar:"S",
            selectIsPresupuesto:2,
            selectSeccion:"",
            intType:0,
            intIdConsecutivo:0,
            intResults:0,
            intResultsServicios:0,
            intResultsBienes:0,
            intResultsCuin:0,
            intPorcentaje:100,
            intResultsFuente:0,
            intPrecio:0,
            intTotalPorcentaje:0,
            strSearch:"",
            strNombre:"",
            arrConceptos:[],
            arrModalCuentas:[],
            arrModalServicios:[],
            arrModalBienes:[],
            arrModalCuin:[],
            arrModalFuente:[],
            arrDetalleIngreso:[],
            arrDetalleFuentes:[],
            arrSecciones:[],
            objCuenta:{"codigo":"","nombre":""},
            objServicio:{"codigo":"","nombre":""},
            objBienes:{"codigo":"","nombre":""},
            objCuin:{"codigo":"","nombre":""},
            objFuente:{"codigo":"","nombre":""},
            objFuenteComp:{"codigo":"","nombre":""},
            fuenteComp:"",
        }
    },
    mounted() {
        const intPageVal = this.$refs.pageType.value;
        if(intPageVal == 2){
            this.getEdit();
        }else if(intPageVal == 3){
            this.getSearch();
        }else{
            this.getData();
        }
    },
    methods: {
        getData: async function(){
            let formData = new FormData();
            formData.append("action","get");
            formData.append("search",this.strSearch);
            this.isLoading = true;
            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();
            this.arrConceptos = objData.conceptos;
            this.arrSecciones = objData.secciones;
            this.arrModalCuentas = objData.modal_cuentas.data;
            this.arrModalServicios = objData.modal_servicios.data;
            this.arrModalBienes = objData.modal_bienes.data;
            this.arrModalCuin = objData.modal_cuin.data;
            this.arrModalFuente = objData.modal_fuente.data;
            this.intResults = objData.modal_cuentas.results;
            this.intResultsServicios = objData.modal_servicios.results;
            this.intResultsBienes = objData.modal_bienes.results;
            this.intResultsCuin = objData.modal_cuin.results;
            this.intResultsFuente = objData.modal_fuente.results;
            this.selectConcepto = this.arrConceptos[0].codigo;
            this.selectSeccion = this.arrSecciones[0].codigo;
            this.isLoading = false;

        },
        getEdit: async function(){
            this.intIdCodigo = new URLSearchParams(window.location.search).get('id');
            let formData = new FormData();
            formData.append("action","edit");
            formData.append("codigo",this.intIdCodigo);
            this.isLoading = true;
            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();
            this.isLoading = false;
            if(objData.status){
                const ingreso = objData.ingreso;
                const detalles = objData.ingreso.detalles;
                this.arrConsecutivos = objData.consecutivos;
                this.intIdConsecutivo = objData.consecutivo;
                this.arrConceptos = objData.conceptos;
                this.arrSecciones = objData.secciones;
                this.arrModalCuentas = objData.modal_cuentas.data;
                this.arrModalServicios = objData.modal_servicios.data;
                this.arrModalBienes = objData.modal_bienes.data;
                this.arrModalCuin = objData.modal_cuin.data;
                this.arrModalFuente = objData.modal_fuente.data;
                this.intResults = objData.modal_cuentas.results;
                this.intResultsServicios = objData.modal_servicios.results;
                this.intResultsBienes = objData.modal_bienes.results;
                this.intResultsCuin = objData.modal_cuin.results;
                this.intResultsFuente = objData.modal_fuente.results;

                this.intIdConsecutivo = ingreso.codigo;
                this.strNombre = ingreso.nombre
                this.selectSimple = ingreso.tipo;
                this.selectContabilizar = ingreso.gravado;
                this.selectTipoIngreso = ingreso.tipoIngreso;
                this.selectTerceros = ingreso.is_tercero;
                this.selectDestino = ingreso.terceros;
                this.intPrecio = ingreso.precio;
                this.arrDetalleFuentes = ingreso.fuentes;
                this.intPorcentaje = ingreso.tipo =="S" ? detalles.porcentaje : "";
                this.selectConcepto = detalles.id_concepto;
                this.selectIsPresupuesto = ingreso.is_cuenta == "" || ingreso.is_cuenta == null || ingreso.is_cuenta == 0 ? 2 : ingreso.is_cuenta;
                if(this.selectTerceros == 2 && this.selectIsPresupuesto == 2){
                    this.intType = ingreso.clasificador;
                    this.objCuenta={"codigo":detalles.cuentapres,"nombre":detalles.nombre}
                    if(this.intType == 1){
                        this.objCuin = {"codigo":detalles.cuenta_clasificadora,"nombre":detalles.nombre_clasificadora};
                    }else if(this.intType == 2){
                        this.objBienes = {"codigo":detalles.cuenta_clasificadora,"nombre":detalles.nombre_clasificadora};
                    }else if(this.intType == 3){
                        this.objServicio = {"codigo":detalles.cuenta_clasificadora,"nombre":detalles.nombre_clasificadora};
                    }
                }
                if(this.selectSimple == "C"){
                    this.arrDetalleIngreso = detalles
                    this.arrDetalleIngreso.forEach(e => {
                        let objConcepto = app.arrConceptos.filter(function(f){return f.codigo == e.id_concepto})[0];
                        e.concepto = objConcepto;
                    });
                    this.selectConcepto = "";
                }
                this.getTotalPorcentaje();
            }else{
                let consecutivo = objData.consecutivo;
                window.location.href='teso-ingresosOtrosTributosEditar.php?id='+consecutivo;
            }

        },
        search: async function(option=""){
            let search = "";
            if(option=="modal_cuenta")search = this.strSearch;
            if(option=="modal_servicio")search = this.strSearch;
            if(option=="modal_bienes")search = this.strSearch;
            if(option=="modal_cuin")search = this.strSearch;
            if(option=="modal_fuente")search = this.strSearch;

            let formData = new FormData();
            formData.append("action","search");
            formData.append("search",this.strSearch);

            this.isLoading = true;
            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();

            if(option=="modal_cuenta"){
                this.arrModalCuentas = objData.modal_cuentas.data;
                this.intResults = objData.modal_cuentas.results;
            }else if(option=="modal_servicio"){
                this.arrModalServicios = objData.modal_servicios.data;
                this.intResultsServicios = objData.modal_servicios.results;
            }else if(option=="modal_bienes"){
                this.arrModalBienes = objData.modal_bienes.data;
                this.intResultsBienes = objData.modal_bienes.results;
            }else if(option=="modal_cuin"){
                this.arrModalCuin = objData.modal_cuin.data;
                this.intResultsCuin = objData.modal_cuin.results;
            }else if(option=="modal_fuente"){
                this.arrModalFuente = objData.modal_fuente.data;
                this.intResultsFuente = objData.modal_fuente.results;
            }

            this.isLoading = false;
        },
        selectItem: async function(item,type){
            if(type == "cuenta"){
                this.objCuenta.codigo = item.codigo;
                this.objCuenta.nombre = item.nombre;
                let formData = new FormData();
                formData.append("action","clasificador");
                formData.append("cuenta",this.objCuenta.codigo);
                this.isLoading = true;
                const response = await fetch(URL,{method:"POST",body:formData});
                this.intType = await response.json();
                //this.intType=1;
                this.isLoading = false;
            }else if(type =="servicio"){
                this.objServicio.codigo = item.codigo;
                this.objServicio.nombre = item.nombre;
            }else if(type =="bienes"){
                this.objBienes.codigo = item.codigo;
                this.objBienes.nombre = item.nombre;
            }else if(type =="cuin"){
                this.objCuin.codigo = item.codigo;
                this.objCuin.nombre = item.nombre;
            }else if(type =="fuente"){
                this.objFuente.codigo = item.codigo;
                this.objFuente.nombre = item.nombre;
            }else if(type =="fuenteComp"){
                this.objFuenteComp.codigo = item.codigo;
                this.objFuenteComp.nombre = item.nombre;
            }
        },
        addFuente: function(){
            if(this.objFuente.codigo == ""){
                Swal.fire("Error","Debe elegir una fuente.","error");
                return false;
            }
            const fuente = this.objFuente;
            const arrFuentes = this.arrDetalleFuentes;
            const totalFuente = arrFuentes.length;
            if(totalFuente > 0){
                let flag = true;
                arrFuentes.forEach(element => {
                    if(element.codigo == this.objFuente.codigo){
                        flag = false;
                    }
                });
                if(!flag){
                    Swal.fire("Error","Esta fuente ya ha sido agregada, intente con otra.","error");
                    return false;
                }
            }
            this.arrDetalleFuentes.push(fuente);
            this.objFuente = {"codigo":"","nombre":""}
        },
        delFuente: function(index=-1){
            if(index>=0){
				this.arrDetalleFuentes.splice(index,1);
			}else{
				this.arrDetalleFuentes = [];
			}
        },
        getTotalPorcentaje:function(){
            this.intTotalPorcentaje = 0;
            this.arrDetalleIngreso.forEach(el=>{app.intTotalPorcentaje+=parseFloat(el.porcentaje)});
        },
        add: function(){
            let clasificador = "";
            let cuentaCodigo = this.objCuenta.codigo;
            let cuentaNombre = this.objCuenta.nombre;
            let concepto = this.selectConcepto;
            let tipoNombre = "";
            let arrDetalleCuentas = this.arrDetalleIngreso;
            let porcentaje = parseFloat(this.intPorcentaje);
            if(concepto == ""){
                Swal.fire("Error","Debe elegir un concepto contable","error");
                return false;
            }
            if(this.selectTerceros == 2){
                if(this.intType > 0){
                    if(this.intType == 1){
                        clasificador = this.objCuin.codigo;
                        tipoNombre = "CUIN";
                    }else if(this.intType == 2){
                        clasificador = this.objBienes.codigo;
                        tipoNombre = "Bienes transportables";
                    }else if(this.intType == 3){
                        clasificador = this.objServicio.codigo;
                        tipoNombre = "Servicio";
                    }
                    if(clasificador ==""){
                        Swal.fire("Error","Debe seleccionar un "+tipoNombre,"error");
                        return false;
                    }
                }
                if(cuentaCodigo == ""){
                    Swal.fire("Error","Debe elegir una cuenta presupuestal.","error");
                    return false;
                }
            }
            let totalPorcentaje = porcentaje;
            if(arrDetalleCuentas.length > 0){
                arrDetalleCuentas.forEach(el=>{totalPorcentaje+=parseFloat(el.porcentaje)});
                if(totalPorcentaje > 100){
                    Swal.fire("Error","La suma de porcentajes no debe ser mayor a 100","error");
                    return false;
                }
            }
            let objConcepto = this.arrConceptos.filter(function(e){return e.codigo == app.selectConcepto})[0];
            let objSeccion = JSON.parse(JSON.stringify(app.arrSecciones.filter(function(e){return e.codigo == app.selectSeccion})[0]));
            arrDetalleCuentas.push({
                    "cuenta":this.selectTerceros == 2 ? cuentaCodigo : "",
                    "nombre_cuenta":this.selectTerceros == 2 ? cuentaNombre : "",
                    "concepto":objConcepto,
                    "cuenta_clasificadora":clasificador,
                    "porcentaje":porcentaje,
                    "id_concepto":objConcepto.codigo,
                    "tipo_concepto":this.selectSimple,
                    "fuente":this.objFuenteComp.codigo,
                    "seccion":objSeccion.codigo,
                    "nombre_seccion":objSeccion.nombre,
                    "nombre_fuente":this.objFuenteComp.nombre
                }
            );
            this.arrDetalleIngreso = arrDetalleCuentas;
            this.getTotalPorcentaje();
        },
        del: function(index=-1){
            if(index>=0){
				this.arrDetalleIngreso.splice(index,1);
			}else{
				this.arrDetalleIngreso = [];
			}
            this.getTotalPorcentaje();
        },
        afectaPresupuesto:function(){
            const vueContext = this;
            if(this.selectIsPresupuesto == 1){
                Swal.fire({
                    title:"¿Estás segur@ de que no afecta presupuesto?",
                    text:"",
                    icon: 'warning',
                    showCancelButton:true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText:"Sí",
                    cancelButtonText:"No"
                }).then(async function(result){
                    if(!result.isConfirmed){
                        vueContext.selectIsPresupuesto = 2;
                    }
                });
            }
        },
        save:async function(){

            if(this.strNombre ==""){
                Swal.fire("Error","El nombre no puede estar vacio","error");
                return false;
            }
            if(this.selectTerceros =="" || this.selectTerceros == null){
                Swal.fire("Error","Debe seleccionar si es con terceros o no","error");
                return false;
            }

            let objConcepto = this.arrConceptos.filter(function(e){return e.codigo == app.selectConcepto});
            let idConcepto = objConcepto.length > 0 ? objConcepto[0].codigo : "";
            let cuenta ="";
            let fuentes = [];
            let clasificador = "";
            let cuentas = [];
            if(this.selectIsPresupuesto == 2 && this.selectTerceros == 2){
                if(this.objCuenta.nombre == ""){
                    Swal.fire("Error","Debe elegir la cuenta presupuestal","error");
                    return false;
                }

                let tipoNombre = "";
                if(this.intType > 0){
                    if(this.intType == 1){
                        clasificador = this.objCuin.codigo;
                        tipoNombre = "CUIN";
                    }else if(this.intType == 2){
                        clasificador = this.objBienes.codigo;
                        tipoNombre = "Bienes transportables";
                    }else if(this.intType == 3){
                        clasificador = this.objServicio.codigo;
                        tipoNombre = "Servicio";
                    }
                    if(clasificador ==""){
                        Swal.fire("Error","Debe seleccionar un "+tipoNombre,"error");
                        return false;
                    }
                }
                fuentes = this.arrDetalleFuentes;
                cuenta = this.objCuenta.codigo;
                if(this.selectSimple != "C"){
                    if(fuentes.length <= 0){
                        Swal.fire("Error","Debe agregar al menos una fuente.","error");
                        return false;
                    }
                }

            }
            if(this.selectSimple == "S"){
                if(this.intPorcentaje <= 0 || this.intPorcentaje > 100){
                    Swal.fire("Error","El porcentaje no puede ser igual a 0 o mayor a 100","error");
                    return false;
                }
                if(this.selectConcepto ==""){
                    Swal.fire("Error","Debe elegir el concepto contable","error");
                    return false;
                }
            }
            if(this.selectSimple == "C"){
                let totalCuentas = this.arrDetalleIngreso.length
                if(totalCuentas <=0 ){
                    Swal.fire("Error","Debe agregar al menos una cuenta","error");
                    return false;
                }
                cuentas = this.arrDetalleIngreso;
            }
            let objHead = {
                "cabecera":{
                    "codigo":this.intIdConsecutivo,
                    "nombre":this.strNombre,
                    "precio":this.intPrecio,
                    "destino":this.selectDestino,
                    "tipo":this.selectSimple,
                    "contabilizar":this.selectContabilizar,
                    "tipo_ingreso":this.selectTipoIngreso,
                    "modulo":4,
                    "terceros":this.selectTerceros,
                    "is_presupuesto":this.selectIsPresupuesto
                },
                "detalle":{
                    "concepto":idConcepto,
                    "tipo_concepto":this.selectSimple,
                    "porcentaje":this.intPorcentaje,
                    "cuenta":cuenta,
                    "clasificador":clasificador,
                    "fuentes":fuentes,
                    "cuentas":cuentas
                }
            };
            const vueContext = this;
            let formData = new FormData();
            formData.append("action","save");
            formData.append("data",JSON.stringify(objHead));
            Swal.fire({
                title:"¿Estás segur@ de guardar?",
                text:"",
                icon: 'warning',
                showCancelButton:true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText:"Sí, guardar",
                cancelButtonText:"No, cancelar"
            }).then(async function(result){
                if(result.isConfirmed){
                    vueContext.isLoading = true;
                    const response = await fetch(URL,{method:"POST",body:formData});
                    const objData =  await response.json();
                    if(objData.status){
                        if(objData.id && app.txtConsecutivo == 0){
                            Swal.fire("Guardado",objData.msg,"success");
                            vueContext.isLoading = false;
                            setTimeout(function(){
                                window.location.href="teso-ingresosOtrosTributosEditar.php?id="+objData.consecutivo;
                            },1500);
                        }else{
                            app.getEdit();
                        }
                    }else{
                        vueContext.isLoading = false;
                        Swal.fire("Error",objData.msg,"error");
                    }
                }
            });
        },
        formatNumero: function(valor){
            return new Intl.NumberFormat("en-US", {style: "currency", currency: "USD"}).format(valor);
        },
        validInteger:function(valor){
            valor = new String(valor);
            if(valor.includes(".") || valor.includes(",")){
                Swal.fire("Error","La cantidad debe ser un número entero.","error");
            }
        },
        showTab:function(tab){
            let tabs = this.$refs.rTabs.children;
            let tabsContent = this.$refs.rTabsContent.children;
            for (let i = 0; i < tabs.length; i++) {
                tabs[i].classList.remove("active");
                tabsContent[i].classList.remove("active")
            }
            tabs[tab-1].classList.add("active");
            tabsContent[tab-1].classList.add("active")
        },
        nextItem:function(type){
            let id = this.intIdConsecutivo;
            let index = this.arrConsecutivos.findIndex(function(e){return e.id == id});
            if(type=="next" && app.arrConsecutivos[++index]){
                id = this.arrConsecutivos[index++].id;
            }else if(type=="prev" && app.arrConsecutivos[--index]){
                id = this.arrConsecutivos[index--].id;
            }
           window.location.href='teso-ingresosOtrosTributosEditar?id='+id;
        },
    },
    computed:{

    }
})
