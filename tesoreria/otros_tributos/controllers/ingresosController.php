<?php
    require_once '../../../Librerias/core/Helpers.php';
    require_once '../../../PHPExcel/Classes/PHPExcel.php';
    require_once '../models/ingresosModel.php';
    session_start();

    class ingresosController extends ingresosModel{
        public function getData(){
            if(!empty($_SESSION)){
                $request['consecutivo'] = $this->selectConsecutivo();
                $request['conceptos'] = $this->selectConceptos();
                $request['modal_cuentas'] = $this->selectCuentas($_POST['search']);
                $request['modal_servicios'] = $this->selectServicios($_POST['search']);
                $request['modal_bienes'] = $this->selectBienes($_POST['search']);
                $request['modal_cuin'] = $this->selectCuin($_POST['search']);
                $request['modal_fuente'] = $this->selectFuentes($_POST['search']);
                $request['secciones']=$this->selectSeccionesPresupuestales();
                echo json_encode($request,JSON_UNESCAPED_UNICODE);
            }
            die();
        }
        public function search(){
            if(!empty($_SESSION)){
                $request['modal_cuentas'] = $this->selectCuentas($_POST['search']);
                $request['modal_servicios'] = $this->selectServicios($_POST['search']);
                $request['modal_bienes'] = $this->selectBienes($_POST['search']);
                $request['modal_cuin'] = $this->selectCuin($_POST['search']);
                $request['modal_fuente'] = $this->selectFuentes($_POST['search']);
                echo json_encode($request,JSON_UNESCAPED_UNICODE);
            }
            die();
        }
        public function save(){
            if(!empty($_SESSION)){
                $arrData = json_decode($_POST['data'],true);
                if(empty($arrData['cabecera']) || empty($arrData['detalle'])){
                    $arrResponse = array("status"=>false,"msg"=>"Error de datos");
                }else{
                    $arrCabecera = $arrData['cabecera'];
                    $arrDetalle = $arrData['detalle'];
                    $opcion = "";
                    if($arrCabecera['codigo'] == 0){
                        $opcion = 1;
                        $id = $this->selectConsecutivo()-1;
                        $request = $this->insertTesoIngreso($arrCabecera);
                        $id = $request;
                    }else{
                        $opcion = 2;
                        $request = $this->updateTesoIngreso($arrCabecera);
                        $id = $arrCabecera['codigo'];
                    }
                    if(is_numeric($request) && $request > 0){
                        $this->insertTesoPrecio($id,$arrCabecera['precio']);
                        $request = $this->insertTesoIngresoDet($id,$arrCabecera,$arrDetalle);
                        if(is_numeric($request) && $request > 0){
                            if($opcion == 1){
                                $arrResponse = array("status"=>true,"msg"=>"Datos guardados","consecutivo"=>$id);
                            }else{
                                $arrResponse = array("status"=>true,"msg"=>"Datos actualizados");
                            }
                            if($arrCabecera['terceros'] == 2 && $arrCabecera['is_presupuesto'] == 2){
                                $request = $this->insertTesoFuentes($id,$arrCabecera,$arrDetalle);
                                if(is_numeric($request) && $request > 0){
                                    if($opcion == 1){
                                        $arrResponse = array("status"=>true,"msg"=>"Datos guardados","consecutivo"=>$id);
                                    }else{
                                        $arrResponse = array("status"=>true,"msg"=>"Datos actualizados");
                                    }
                                }else{
                                    if($opcion == 1){
                                        $arrResponse = array("status"=>true,"msg"=>"Datos guardados","consecutivo"=>$id);
                                    }else{
                                        $arrResponse = array("status"=>true,"msg"=>"Datos actualizados");
                                    }
                                }
                            }else{
                                $arrResponse = array("status"=>true,"msg"=>"Datos actualizados");
                            }
                        }else{
                            $arrResponse = array("status"=>false,"msg"=>"Error en ingreso detalle");
                        }
                    }else{
                        $arrResponse = array("status"=>false,"msg"=>"Error en ingreso cabecera");
                    }
                }
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
            die();
        }
        public function getEdit(){
            if(!empty($_SESSION)){
                $codigo = strClean($_POST['codigo']);
                $arrIngreso = $this->selectIngreso($codigo);
                if(!empty($arrIngreso)){
                    $arrIngreso['fuentes'] = $arrIngreso['is_tercero'] == 2 && $arrIngreso['is_cuenta'] == 2 ? $this->selectIngresoFuentes($codigo) : [];
                    $arrSecciones = $this->selectSeccionesPresupuestales();
                    $arrFuentes = $this->selectFuentes("");
                    $fuenteData = $arrFuentes['data'];
                    if($arrIngreso['tipo'] == "S"){
                        $arrDetalles = $this->selectIngresoDet($codigo);
                        if($arrIngreso['is_tercero'] == 2 && $arrIngreso['is_cuenta'] == 2){
                            $cuenta = $arrDetalles['cuentapres'];
                            $clasificador = $this->selectClasificador($cuenta,true);
                            if($clasificador != ""){
                                $cuentaClasificadora = $arrDetalles['cuenta_clasificadora'];
                                $nombreClasificador = $this->selectNombreClasificador($clasificador,$cuentaClasificadora);
                                $arrDetalles['nombre_clasificadora'] =$nombreClasificador;
                            }
                            $arrIngreso['clasificador'] = $clasificador;
                        }
                        $arrDetalles['id_concepto']=$arrDetalles['concepto'];
                        $arrDetalles['concepto'] = $this->selectConcepto($arrDetalles['concepto']);
                    }

                    if($arrIngreso['tipo']=="C"){

                        $arrDetalles = $this->selectIngresoDet($codigo,true);
                        $rows = count($arrDetalles);
                        for ($i=0; $i < $rows ; $i++) {
                            $det = $arrDetalles[$i];
                            if($arrIngreso['terceros'] == ""){
                                $cuenta = $arrDetalles[$i]['cuentapres'];
                                $clasificador = $this->selectClasificador($cuenta,true);
                                if($clasificador != ""){
                                    $cuentaClasificadora = $arrDetalles[$i]['cuenta_clasificadora'];
                                    $nombreClasificador = $this->selectNombreClasificador($clasificador,$cuentaClasificadora);
                                    $arrDetalles[$i]['nombre_clasificadora'] =$nombreClasificador;
                                }
                            }
                            $seccion = array_values(array_filter($arrSecciones,function($e) use($det){return $det['seccion'] ==$e['codigo'];}))[0];
                            $fuente = array_values(array_filter($fuenteData,function($e) use($det){return $det['fuente'] ==$e['codigo'];}))[0];
                            $concepto = explode("-",$arrDetalles[$i]['concepto']);
                            $arrDetalles[$i]['seccion'] = $seccion['codigo'];
                            $arrDetalles[$i]['nombre_seccion'] = $seccion['nombre'];
                            $arrDetalles[$i]['fuente'] = $fuente['codigo'];
                            $arrDetalles[$i]['nombre_fuente'] = $fuente['nombre'];
                            $arrDetalles[$i]['cuenta'] = $arrDetalles[$i]['cuentapres'];
                            $arrDetalles[$i]['nombre_cuenta'] = $arrDetalles[$i]['nombre'];
                            $arrDetalles[$i]['id_concepto'] = $concepto[0];
                            $arrDetalles[$i]['tipo_concepto'] = $arrDetalles[$i]['tipoconce'];
                            $arrDetalles[$i]['concepto'] = $this->selectConcepto($concepto[0]);
                        }
                    }

                    $arrIngreso['precio'] = $this->selectPrecio($codigo);
                    $arrIngreso['detalles'] = $arrDetalles;

                    $request['ingreso'] = $arrIngreso;
                    $request['consecutivo'] = $arrIngreso['codigo'];
                    $request['conceptos'] = $this->selectConceptos();
                    $request['modal_cuentas'] = $this->selectCuentas($_POST['search']);
                    $request['modal_servicios'] = $this->selectServicios($_POST['search']);
                    $request['modal_bienes'] = $this->selectBienes($_POST['search']);
                    $request['modal_cuin'] = $this->selectCuin($_POST['search']);
                    $request['modal_fuente'] = $arrFuentes;
                    $request['consecutivos'] = getConsecutivos("tesoingresos","codigo");
                    $request['secciones']=$arrSecciones;
                    $request['status']=true;

                }else{
                    $data = getConsecutivos("tesoingresos","codigo");
                    $request['status']=false;
                    $request['consecutivo'] = $data[count($data)-1]['id'];
                }
                echo json_encode($request,JSON_UNESCAPED_UNICODE);
            }
            die();
        }
        public function getSearch(){
            if(!empty($_SESSION)){
                if($_POST){
                    $strFechaInicial = strClean($_POST['fecha_inicial']);
                    $strFechaFinal = strClean($_POST['fecha_final']);
                    $strSearch = strClean($_POST['search']);
                    $request = $this->selectSearch($strFechaInicial,$strFechaFinal,$strSearch);
                    echo json_encode($request,JSON_UNESCAPED_UNICODE);
                }
            }
            die();
        }
        public function getClasificador(){
            if(!empty($_SESSION)){
                $arrResponse = $this->selectClasificador($_POST['cuenta']);
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
            die();
        }
    }

    if($_POST){
        $obj = new ingresosController();
        if($_POST['action']=="search"){
            $obj->search();
        }else if($_POST['action']=="get"){
            $obj->getData();
        }else if($_POST['action'] =="clasificador"){
            $obj->getClasificador();
        }else if($_POST['action'] == "save"){
            $obj->save();
        }else if($_POST['action'] == "edit"){
            $obj->getEdit();
        }
    }

?>
