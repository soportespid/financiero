<?php
    require_once '../../../Librerias/core/Helpers.php';
    require_once '../../../Librerias/core/Mysql.php';
    require_once '../../../funciones.inc';
    session_start();
    
    class egresoModel extends Mysql {
        /* Read data */
        public function get_egresos($params) {
            $critConse = $critFecha = $critBeneficiario = "";

            if ($params["consecutivoIni"] && $params["consecutivoFin"]) {
                $critConse = "AND egreso.id_egreso BETWEEN $params[consecutivoIni] AND $params[consecutivoFin] ";
            }

            if ($params["fechaIni"] != "" && $params["fechaFin"]) {
                $critFecha = "AND egreso.fecha BETWEEN '$params[fechaIni]' AND '$params[fechaFin]'";
            }

            if ($params["beneficiario"]) {
                $critBeneficiario = "AND (egreso.tercero LIKE '%{$params['beneficiario']}%' 
                OR t.razonsocial LIKE '%{$params['beneficiario']}%' 
                OR CONCAT(t.nombre1, ' ', t.nombre2, ' ', t.apellido1, ' ', t.apellido2) LIKE '%{$params['beneficiario']}%') ";
            }

            $sql = "SELECT egreso.id_egreso AS consecutivo,
            egreso.vigencia,
            egreso.fecha,
            egreso.id_orden AS cod_obligacion,
            egreso.concepto AS detalle,
            egreso.valortotal,
            egreso.valorpago,
            egreso.retenciones AS valor_retenciones,
            egreso.cuentabanco AS num_cuenta_entidad,
            egreso.pago AS forma_pago,
            egreso.estado,
            egreso.tercero,
            egreso.user AS usuario,
            orden.id_rp AS cod_rp,
            CASE WHEN t.razonsocial IS NULL OR t.razonsocial = ''
            THEN CONCAT(t.nombre1,' ',t.nombre2,' ',t.apellido1,' ',t.apellido2)
            ELSE t.razonsocial END AS nombre
            FROM tesoegresos AS egreso
            INNER JOIN terceros AS t
            ON egreso.tercero = t.cedulanit
            LEFT JOIN tesoordenpago AS orden
            ON orden.id_orden = egreso.id_orden
            WHERE egreso.tipo_mov = '201'
            $critConse $critFecha $critBeneficiario";
            $data = $this->select_all($sql);
            foreach ($data as &$d) {
                $d["cuenta_bancaria_beneficiario"] = $this->search_banco_beneficiario($d["vigencia"], $d["cod_rp"]);
                $d["cuenta_bancaria_entidad"] = $this->search_banco_entidad($d["num_cuenta_entidad"]);

                $dataRet = [];
                $sql_det = "SELECT ret.codigo, ret.nombre, orden.porcentaje, orden.valor FROM tesoordenpago_retenciones AS orden INNER JOIN tesoretenciones AS ret ON orden.id_retencion = ret.id WHERE orden.id_orden = $d[cod_obligacion]";
                $retenciones = $this->select_all($sql_det);

                foreach ($retenciones as $ret) {
                    $arrTemp = [
                        "codigo" => $ret["codigo"],
                        "nombre" => $ret["nombre"],
                        "porcentaje" => $ret["porcentaje"],
                        "valor" => $ret["valor"]
                    ];

                    array_push($dataRet, $arrTemp);
                }

                $d["retenciones"] = $dataRet;
            }
            return $data;
        }

        private function search_banco_beneficiario($vigencia, $rp) {
            $sql_rp_banco = "SELECT banco FROM ccpetrp_banco WHERE vigencia = '$vigencia' AND consvigencia = '$rp'";
            $row_rp_banco = $this->select($sql_rp_banco);

            $sql_tercero_cuenta = "SELECT tipo_cuenta, codigo_banco FROM teso_cuentas_terceros WHERE numero_cuenta = '$row_rp_banco[banco]'";
            $row_tercero_cuenta = $this->select($sql_tercero_cuenta);

            $sql_tipo_banco = "SELECT nombre FROM hum_bancosfun WHERE codigo = '$row_tercero_cuenta[codigo_banco]'";
            $row_tipo_banco = $this->select($sql_tipo_banco);

            $cuentaBancaria = "$row_tipo_banco[nombre] - $row_tercero_cuenta[tipo_cuenta] - $row_rp_banco[banco]";
            return $cuentaBancaria;
        }

        private function search_banco_entidad($cuenta) {
            $sql_banco = "SELECT tercero, tipo FROM tesobancosctas WHERE ncuentaban = '$cuenta'";
            $row_banco = $this->select($sql_banco);
            $nombre = getNombreTercero($row_banco["tercero"]);
            $cuentaBancaria = "$nombre - $row_banco[tipo] - $cuenta";
            return $cuentaBancaria;
        }
        /* Create data */

        /* Update data */

        /* Delate data */
       
    }
?>