<?php
    require_once '../../../Librerias/core/Helpers.php';
    require_once '../../../Librerias/core/Mysql.php';
    require_once '../../../funciones.inc';
    session_start();
    
    class obligacionModel extends Mysql {
        /* Read data */
        public function get_obligaciones($params) {
            $critConse = $critFecha = $critBeneficiario = "";
            if ($params["consecutivoIni"] && $params["consecutivoFin"]) {
                $critConse = "AND orden.id_orden BETWEEN $params[consecutivoIni] AND $params[consecutivoFin] ";
            }

            if ($params["fechaIni"] != "" && $params["fechaFin"]) {
                $critFecha = "AND orden.fecha BETWEEN '$params[fechaIni]' AND '$params[fechaFin]'";
            }

            if ($params["beneficiario"]) {
                $critBeneficiario = "AND (orden.tercero LIKE '%{$params['beneficiario']}%' 
                OR t.razonsocial LIKE '%{$params['beneficiario']}%' 
                OR CONCAT(t.nombre1, ' ', t.nombre2, ' ', t.apellido1, ' ', t.apellido2) LIKE '%{$params['beneficiario']}%') ";
            }

            $sql = "SELECT orden.id_orden AS consecutivo, 
            orden.vigencia, 
            orden.fecha,
            orden.id_rp AS rp,
            orden.conceptorden AS detalle,
            orden.valorpagar AS valor_total,
            orden.valorpagar - orden.valorretenciones AS valor_apagar,
            orden.valorretenciones AS valor_retenciones,
            orden.base AS valor_base,
            orden.iva AS valor_iva,
            orden.tercero AS docBeneficiario,
            orden.estado,
            orden.user AS usuario,
            CASE WHEN t.razonsocial IS NULL OR t.razonsocial = ''
            THEN CONCAT(t.nombre1,' ',t.nombre2,' ',t.apellido1,' ',t.apellido2)
            ELSE t.razonsocial END AS nombre
            FROM tesoordenpago AS orden
            INNER JOIN terceros AS t
            ON orden.tercero = t.cedulanit
            WHERE orden.tipo_mov = '201'
            $critConse $critFecha $critBeneficiario";  
            $data = $this->select_all($sql);

            foreach ($data as &$d) {
                $dataRet = [];
                $sql_det = "SELECT ret.codigo, ret.nombre, orden.porcentaje, orden.valor FROM tesoordenpago_retenciones AS orden INNER JOIN tesoretenciones AS ret ON orden.id_retencion = ret.id WHERE orden.id_orden = $d[consecutivo]";
                $retenciones = $this->select_all($sql_det);

                foreach ($retenciones as $ret) {
                    $arrTemp = [
                        "codigo" => $ret["codigo"],
                        "nombre" => $ret["nombre"],
                        "porcentaje" => $ret["porcentaje"],
                        "valor" => $ret["valor"]
                    ];

                    array_push($dataRet, $arrTemp);
                }

                $d["retenciones"] = $dataRet;
            }

            return $data;
        }
        /* Create data */

        /* Update data */

        /* Delate data */
       
    }
?>