<?php
    require_once '../../../Librerias/core/Helpers.php';
    require_once '../../../Librerias/core/Mysql.php';
    require_once '../../../funciones.inc';
    session_start();
    
    class otroEgresoModel extends Mysql {
        /* Read data */
        public function get_egresos($params) {
            $critConse = $critFecha = $critBeneficiario = "";

            if ($params["consecutivoIni"] && $params["consecutivoFin"]) {
                $critConse = "AND egreso.id_pago BETWEEN $params[consecutivoIni] AND $params[consecutivoFin] ";
            }

            if ($params["fechaIni"] != "" && $params["fechaFin"]) {
                $critFecha = "AND egreso.fecha BETWEEN '$params[fechaIni]' AND '$params[fechaFin]' ";
            }

            if ($params["beneficiario"]) {
                $critBeneficiario = "AND (egreso.tercero LIKE '%{$params['beneficiario']}%' 
                OR t.razonsocial LIKE '%{$params['beneficiario']}%' 
                OR CONCAT(t.nombre1, ' ', t.nombre2, ' ', t.apellido1, ' ', t.apellido2) LIKE '%{$params['beneficiario']}%') ";
            }

            $sql = "SELECT egreso.id_pago AS consecutivo, 
            egreso.tercero AS documento, 
            egreso.banco AS numContableBanco, 
            egreso.cheque, 
            egreso.transferencia, 
            egreso.valor AS valorPago, 
            egreso.concepto AS detalle, 
            egreso.estado,
            egreso.fecha,
            egreso.tercero AS documento,
            (SELECT COALESCE(SUM(valor),0) FROM tesopagotercerosvigant_retenciones WHERE id_egreso = egreso.id_pago) AS valorRetenciones,
            CASE WHEN t.razonsocial IS NULL OR t.razonsocial = ''
            THEN CONCAT(t.nombre1,' ',t.nombre2,' ',t.apellido1,' ',t.apellido2)
            ELSE t.razonsocial END AS nombre
            FROM tesopagotercerosvigant AS egreso
            INNER JOIN terceros AS t
            ON egreso.tercero = t.cedulanit
            WHERE egreso.fecha != ''
            $critConse $critFecha $critBeneficiario
            ORDER BY egreso.id_pago ASC";

            $data = $this->select_all($sql);

            foreach ($data as &$d) {
                $dataRet = [];

                $d["cuenta_bancaria_entidad"] = $this->search_banco_entidad($d["numContableBanco"]);
                $d["valorAPagar"] = $d["valorPago"] - $d["valorRetenciones"];

                $sql_det = "SELECT det.tipo, det.movimiento, det.valor, ret.nombre FROM tesopagotercerosvigant_det AS det INNER JOIN tesoretenciones AS ret ON det.movimiento = ret.codigo WHERE det.id_pago = $d[consecutivo]";
                $row_det = $this->select_all($sql_det);

                foreach ($row_det as $det) {
                    $arrTemp = [
                        "nombre" => "$det[tipo]-$det[movimiento]-$det[nombre]",
                        "valor" => $det["valor"]
                    ];

                    array_push($dataRet, $arrTemp);
                }

                $d["descuentos"] = $dataRet;
            }
            
            return $data;
        }

        private function search_banco_entidad($cuenta) {
            $sql_banco = "SELECT tercero, ncuentaban AS cuenta, tipo FROM tesobancosctas WHERE cuenta = '$cuenta'";
            $row_banco = $this->select($sql_banco);
            $nombre = getNombreTercero($row_banco["tercero"]);
            $cuentaBancaria = "$nombre - $row_banco[tipo] - $row_banco[cuenta]";
            return $cuentaBancaria;
        }
        /* Create data */

        /* Update data */

        /* Delate data */
       
    }
?>