<?php
    require_once '../../../Librerias/core/Helpers.php';
    require_once '../models/otroEgreso_model.php';
    session_start();
    header('Content-Type: application/json');
    class otroEgresoController extends otroEgresoModel {
        /* crear */
        public function get() {
            if (!empty($_SESSION)) {
                $params = array(
                    "consecutivoIni" => $_POST["consecutivoIni"],
                    "consecutivoFin" => $_POST["consecutivoFinal"],
                    "fechaIni" => $_POST["fechaInicial"],
                    "fechaFin" => $_POST["fechaFinal"],
                    "beneficiario" => $_POST["beneficiario"]
                );
                $data = $this->get_egresos($params);
                $arrResponse = array(
                   "egresos" => $data
                );
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);    
            }
            die();
        }
    }

    if($_POST){
        $obj = new otroEgresoController();
        $accion = $_POST["action"];
        
        if ($accion == "get") {
            $obj->get();
        } 
    }
?>