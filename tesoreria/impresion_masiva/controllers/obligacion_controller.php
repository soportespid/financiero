<?php
    require_once '../../../Librerias/core/Helpers.php';
    require_once '../models/obligacion_model.php';
    session_start();
    header('Content-Type: application/json');
    class obligacionController extends obligacionModel {
        /* crear */
        public function get() {
            if (!empty($_SESSION)) {
                $params = array(
                    "consecutivoIni" => $_POST["consecutivoIni"],
                    "consecutivoFin" => $_POST["consecutivoFinal"],
                    "fechaIni" => $_POST["fechaInicial"],
                    "fechaFin" => $_POST["fechaFinal"],
                    "beneficiario" => $_POST["beneficiario"]
                );
                $data = $this->get_obligaciones($params);
                $arrResponse = array(
                   "obligaciones" => $data
                );
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);    
            }
            die();
        }
    }

    if($_POST){
        $obj = new obligacionController();
        $accion = $_POST["action"];
        
        if ($accion == "get") {
            $obj->get();
        } 
    }
?>