const URL = "tesoreria/impresion_masiva/controllers/otroEgreso_controller.php";
const URLPDF = "generaOtrosEgresosPdf.php"; 

var app = new Vue({
    el:"#myapp",
    data() {
        return {
            /* general */
            isLoading: false,
            txtSearch: '',
            id: 0,
            tabs: '1',

            /* pagina buscar */
            arrSearch: [], arrSearchCopy: [],
            conseIni: '',
            conseFin: '',
            fechaIni: '',
            fechaFin: '',
            beneficiario: ''
        }
    },
    mounted() {

    },
    methods: {
        /* metodos para traer información */
        async get_data() {
            if (
                (this.conseIni === "" || this.conseFin === "") &&
                (this.fechaIni === "" || this.fechaFin === "") &&
                this.beneficiario === ""
            ) {
                Swal.fire("Atención", "Debes usar al menos uno de los siguientes conjuntos de filtros: Consecutivo Inicial y Consecutivo Final, Fecha Inicial y Fecha Final, o Beneficiario", "warning");
                return false;
            }

            if (this.conseIni != "" && this.conseFin != "") {
                if (parseInt(this.conseIni) > parseInt(this.conseFin)) {
                    Swal.fire("Atención", "El consecutivo inicial debe ser menor o igual al consecutivo final", "warning");
                    return false;
                }
            }

            if (this.fechaIni != "" && this.fechaFin != "") {
                if (this.fechaIni > this.fechaFin) {
                    Swal.fire("Atención", "La fecha inicial debe ser menor o igual a la fecha final", "warning");
                    return false;
                }
            }

            let consecutivoInicial = this.conseIni != "" ? this.conseIni : "";
            let consecutivoFinal = this.conseFin != "" ? this.conseFin : "";
            let fechaInicial = this.fechaIni != "" ? this.fechaIni : "";
            let fechaFinal = this.fechaFin != "" ? this.fechaFin : "";
            let beneficiario = this.beneficiario != "" ? this.beneficiario : "";
            const formData = new FormData();
            formData.append("action","get");
            formData.append("consecutivoIni", consecutivoInicial);
            formData.append("consecutivoFinal", consecutivoFinal);
            formData.append("fechaInicial", fechaInicial);
            formData.append("fechaFinal", fechaFinal);
            formData.append("beneficiario", beneficiario);
            this.isLoading = true;
            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();
            this.arrSearch = objData.egresos;
            this.isLoading = false;
        },

        /* metodos para procesar información */


        /* Metodos para guardar o actualizar información */

        /* Formatos para mostrar información */

        viewFormatNumber: function(valor){
            return new Intl.NumberFormat("en-US", {style: "currency", currency: "USD"}).format(valor);  
        },

        formatFecha: function(fecha) {
            // Separar la fecha en componentes
            const parts = fecha.split('-');
            if (parts.length !== 3) {
                throw new Error('Formato de fecha inválido. Use Y-m-d.');
            }
            const year = parts[0];
            const month = parts[1];
            const day = parts[2];
        
            // Retornar la fecha en el formato deseado
            return `${day}/${month}/${year}`;
        },

        /* enviar informacion excel o pdf */
        pdf: function() {
            const form = document.createElement("form");
            form.method = "post";
            form.target = "_blank";
            form.action = URLPDF;       
            
            function addField(name,value){
                const input = document.createElement("input");
                input.type="hidden";
                input.name=name;
                input.value = value;
                form.appendChild(input);
            }


            addField("data", JSON.stringify(this.arrSearch));
            
            document.body.appendChild(form);
            form.submit();
            document.body.removeChild(form);
        },
    },
    computed:{

    }
})
