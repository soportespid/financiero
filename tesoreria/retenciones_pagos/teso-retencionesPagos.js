const URL ='tesoreria/retenciones_pagos/ControllerRetenciones.php';

var app = new Vue({
    el:"#myapp",
    data() {
        return {
            isLoading:false,
            isModal:false,
            isModalClasificador:false,
            isClasificador:false,
            isModalFuente:false,
            txtCuentaClasificadora:"",
            txtResultadosClasificador:"",
            txtNombre:"",
            txtRetencion:100,
            txtCompRetencion:0,
            txtResultados:"",
            txtResultadosFuente:"",
            txtSearch:"",
            txtSearchClasificador:"",
            txtTituloModal:"",
            txtSearchFuente:"",
            selectTipo:"S",
            selectDestino:"",
            selectParticipacion:"N",
            checkTerceros:false,
            checkIva:false,
            checkNomina:false,
            selectConceptoCausa:"",
            selectConceptoIngreso:"",
            selectConceptoSgr:"",
            selectCompDestino:"",
            objCuenta:{"nombre":"","codigo":""},
            objClasificador:{"nombre":"","codigo":""},
            objFuente:{"nombre":"","codigo":""},
            arrConceptosCausa:[],
            arrConceptosIngreso:[],
            arrConceptosSgr:[],
            arrCuentas:[],
            arrCuentasCopy:[],
            arrFuentes:[],
            arrFuentesCopy:[],
            arrDetalles:[],
            arrDetallesFuente:[],
            arrClasificadores:[],
            arrMostrarClasificadoras:[],
            arrMostrarClasificadorasCopy:[],
            arrData:[],
            arrDataCopy:[],
            arrConsecutivos:[],
            objCuentasClasificadores:{},
            intPageVal: 0,
            txtConsecutivo:0,
            txtCodigo: 0,
        }
    },
    mounted() {
        this.intPageVal = this.$refs.pageType.value;
        if(this.intPageVal == 1){
            this.getData();
        }else if(this.intPageVal == 2){
            this.getSearchData();
        }else if(this.intPageVal == 3){
            this.getEdit();
        }
    },
    methods: {
        save:async function(){
            if(this.selectTipo == "S"){
                if(this.txtNombre == "" || this.txtRetencion < 0){
                    Swal.fire("Atención!","Los campos con (*) son obligatorios.","warning");
                    return false;
                }
                if(!this.checkTerceros){
                    if(this.objCuenta.codigo == ""){
                        Swal.fire("Atención!","Debe agregar una cuenta presupuestal.","warning");
                        return false;
                    }
                    if(this.objCuenta.codigo != "" && app.arrDetallesFuente.length == 0){
                        Swal.fire("Atención!","Debe agregar al menos una fuente.","warning");
                        return false;
                    }
                }
                let nombreDestino = "Otros";
                if(this.selectCompDestino == "M"){
                    nombreDestino = "Municipal";
                }else if(this.selectCompDestino == "N"){
                    nombreDestino = "Nacional";
                }else if(this.selectCompDestino =="D"){
                    nombreDestino = "Departamental";
                }
                this.arrDetalles[0]={
                    causacion:app.selectConceptoCausa != "" ? app.arrConceptosCausa.filter(function(e){return e.codigo == app.selectConceptoCausa})[0]:{codigo:"",nombre:"",tipo:""},
                    ingreso:app.selectConceptoIngreso != "" ? app.arrConceptosIngreso.filter(function(e){return e.codigo == app.selectConceptoIngreso})[0]:{codigo:"",nombre:"",tipo:""},
                    sgr:app.selectConceptoSgr != "" ? app.arrConceptosSgr.filter(function(e){return e.codigo == app.selectConceptoSgr})[0]:{codigo:"",nombre:"",tipo:""},
                    cuenta:app.objCuenta,
                    cuenta_clasificadora:app.objClasificador,
                    destino:{
                        tipo:app.selectCompDestino,
                        nombre: nombreDestino
                    },
                    porcentaje:100
                };

            }
            if(this.selectTipo == "C"){
                if(this.txtNombre == ""){
                    Swal.fire("Atención!","Los campos con (*) son obligatorios.","warning");
                    return false;
                }
                if(app.arrDetalles.length == 0){
                    Swal.fire("Atención!","Debe agregar al menos un detalle.","warning");
                    return false;
                }
                if(!this.checkTerceros){
                    const flag = app.arrDetalles.every(function(e){return e.cuenta.codigo !=""});
                    if(!flag){
                        Swal.fire("Atención!","El detalle debe tener cuentas presupuestales.","warning");
                        return false;
                    }
                    if(app.arrDetallesFuente.length == 0){
                        Swal.fire("Atención!","Debe agregar al menos una fuente.","warning");
                        return false;
                    }
                }
            }
            const obj = {
                consecutivo:this.txtCodigo,
                nombre:this.txtNombre,
                retencion:this.txtRetencion,
                tipo:this.selectTipo,
                destino:this.selectDestino,
                participacion: this.selectParticipacion,
                is_terceros: this.checkTerceros ? 1 : 0,
                is_iva: this.checkIva ? 1 : 0,
                is_nomina: this.checkNomina ? 1 : 0,
                detalles:this.arrDetalles,
                fuentes:this.arrDetallesFuente
            }
            const formData = new FormData();
            formData.append("action","save");
            formData.append("data",JSON.stringify(obj));
            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();
            Swal.fire({
                title:"¿Estás segur@ de guardar?",
                text:"",
                icon: 'warning',
                showCancelButton:true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText:"Sí, guardar",
                cancelButtonText:"No, cancelar"
            }).then(async function(result){
                if(result.isConfirmed){
                    if(objData.status){
                        Swal.fire("Guardado",objData.msg,"success");
                        if(objData.id){
                            setTimeout(function(){
                                window.location.href='teso-retencionesPagoEditar.php?id='+objData.id;
                            },1500);
                        }
                    }else{
                        Swal.fire("Error",objData.msg,"error");
                    }
                }
            });
        },
        getEdit:async function(){
            const codigo = new URLSearchParams(window.location.search).get('id');
            let formData = new FormData();
            formData.append("action","edit");
            formData.append("codigo",codigo);
            this.isLoading = true;
            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();
            if(objData.status){
                this.arrCuentas = objData.cuentas;
                this.arrCuentasCopy = objData.cuentas;
                this.arrFuentes = objData.fuentes;
                this.arrFuentesCopy = objData.fuentes;
                this.arrConceptosCausa = objData.conceptos.filter(function(e){return e.tipo == "RE"});
                this.arrConceptosIngreso = objData.conceptos.filter(function(e){return e.tipo == "RI"});
                this.arrConceptosSgr = objData.conceptos.filter(function(e){return e.tipo == "SR"});
                this.arrClasificadores = objData.clasificadores;
                this.objCuentasClasificadores = {
                    1:{nombre:"CUIN",cuentas:objData.cuin},
                    2:{nombre:"Bienes transportables",cuentas:objData.bienes},
                    3:{nombre:"Servicios",cuentas:objData.servicios}
                };
                this.arrConsecutivos = objData.consecutivos;
                this.txtConsecutivo = objData.data.codigo;
                this.txtCodigo = objData.data.id;
                this.selectDestino = objData.data.destino;
                this.selectTipo = objData.data.tipo;
                this.txtRetencion = objData.data.retencion;
                this.txtNombre = objData.data.nombre;
                this.checkTerceros = objData.data.terceros == 1 ? true : false;
                this.checkNomina = objData.data.nomina == 1 ? true : false;
                this.checkIva = objData.data.iva == 1 ? true : false;
                const detalle = objData.data.detalle;
                const fuentes = objData.data.fuentes;
                if(detalle.length>0){
                    if(detalle[0].cuentapres != "" && detalle[0].cuentapres != undefined){
                        if(fuentes != null && fuentes.length > 0){
                            fuentes.forEach(function(e){
                                app.arrDetallesFuente.push(
                                    app.arrFuentesCopy.filter(function(f){return e.fuente==f.codigo})[0]
                                )
                            });
                        }
                    }
                }
                if(objData.data.tipo == "S" && detalle.length > 0){
                    this.selectConceptoCausa = detalle[0].conceptocausa == -1 ? "" : detalle[0].conceptocausa;
                    this.selectConceptoIngreso = detalle[0].conceptoingreso == -1 ? "" : detalle[0].conceptoingreso;
                    this.selectConceptoSgr = detalle[0].conceptosgr == -1 ? "" : detalle[0].conceptosgr;
                    if(detalle[0].cuentapres != "" && detalle[0].cuentapres != undefined){
                        let cuentaTemp = this.arrCuentas.filter(e=>e.codigo==detalle[0].cuentapres)[0];

                        if(cuentaTemp != undefined){
                            this.objCuenta = JSON.parse(JSON.stringify(cuentaTemp));
                        }else{
                            this.objCuenta = {"codigo":"","nombre":""};
                        }
                    }
                    if(detalle[0].cuenta_clasificadora != ""){
                        const arrClasificador = this.arrClasificadores.filter(function(e){return e.cuenta == app.objCuenta.codigo});
                        this.selectClasificadores(arrClasificador);
                        this.objClasificador= JSON.parse(JSON.stringify(
                            this.arrMostrarClasificadorasCopy.filter(e=>e.codigo==detalle[0].cuenta_clasificadora)[0]
                        ));
                    }
                }else if(objData.data.tipo == "C" && detalle.length > 0){
                    detalle.forEach(f => {
                        let nombreDestino = "Otros";
                        if(f.destino == "M"){
                            nombreDestino = "Municipal";
                        }else if(f.destino == "N"){
                            nombreDestino = "Nacional";
                        }else if(f.destino =="D"){
                            nombreDestino = "Departamental";
                        }
                        let cuentaPres ={"nombre":"","codigo":""};
                        let cuentaClasificadora ={"nombre":"","codigo":""};
                        if(f.cuentapres != ""){
                            cuentaPres = JSON.parse(JSON.stringify(this.arrCuentasCopy.filter(e=>e.codigo==f.cuentapres)[0]));
                            if(f.cuenta_clasificadora != ""){
                                const arrClasificador = this.arrClasificadores.filter(function(e){return e.cuenta == cuentaPres.codigo});
                                this.selectClasificadores(arrClasificador);
                                cuentaClasificadora= JSON.parse(JSON.stringify(
                                    this.arrMostrarClasificadorasCopy.filter(e=>e.codigo==f.cuenta_clasificadora)[0]
                                ));
                            }
                        }
                        this.arrDetalles.push(
                            {
                                causacion:f.conceptocausa != -1 ? app.arrConceptosCausa.filter(function(e){return e.codigo == f.conceptocausa})[0]:{codigo:"",nombre:"",tipo:""},
                                ingreso:f.conceptoingreso != -1 ? app.arrConceptosIngreso.filter(function(e){return e.codigo == f.conceptoingreso})[0]:{codigo:"",nombre:"",tipo:""},
                                sgr:f.conceptosgr != -1 ? app.arrConceptosSgr.filter(function(e){return e.codigo == f.conceptosgr})[0]:{codigo:"",nombre:"",tipo:""},
                                cuenta:cuentaPres,
                                cuenta_clasificadora:cuentaClasificadora,
                                destino:{
                                    tipo:f.destino,
                                    nombre: nombreDestino
                                },
                                porcentaje:f.porcentaje
                            }
                        );
                    });
                }
            }else{
                const id = objData.consecutivo;
                window.location.href='teso-retencionesPagoEditar.php?id='+id;
            }
            this.isLoading = false;
        },
        getData:async function(){
            const formData = new FormData();
            formData.append("action","get");
            this.isLoading = true;
            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();
            this.arrCuentas = objData.cuentas;
            this.arrCuentasCopy = objData.cuentas;
            this.arrFuentes = objData.fuentes;
            this.arrFuentesCopy = objData.fuentes;
            this.arrConceptosCausa = objData.conceptos.filter(function(e){return e.tipo == "RE"});
            this.arrConceptosIngreso = objData.conceptos.filter(function(e){return e.tipo == "RI"});
            this.arrConceptosSgr = objData.conceptos.filter(function(e){return e.tipo == "SR"});
            this.arrClasificadores = objData.clasificadores;
            this.objCuentasClasificadores = {
                1:{nombre:"CUIN",cuentas:objData.cuin},
                2:{nombre:"Bienes transportables",cuentas:objData.bienes},
                3:{nombre:"Servicios",cuentas:objData.servicios}
            };
            this.isLoading = false;
        },
        search:function(type=""){
            let search = "";
            if(type == "modal")search = this.txtSearch.toLowerCase();
            if(type == "modal_clasificador")search = this.txtSearchClasificador.toLowerCase();
            if(type == "modal_fuente")search = this.txtSearchFuente.toLowerCase();
            if(type == "cod_cuenta") search = this.objCuenta.codigo;
            if(type == "cod_clasificador") search = this.objClasificador.codigo;
            if(type == "cod_fuente") search = this.objFuente.codigo;

            if(type=="modal"){
                this.arrCuentasCopy = [...this.arrCuentas.filter(e=>e.codigo.toLowerCase().includes(search) || e.nombre.toLowerCase().includes(search))];
                this.txtResultados = this.arrCuentasCopy.length;
            }else if(type=="modal_clasificador"){
                this.arrMostrarClasificadorasCopy = [...this.arrMostrarClasificadoras.filter(
                    e=>e.codigo.toLowerCase().includes(search) || e.nombre.toLowerCase().includes(search)
                )];
                this.txtResultadosClasificador = this.arrMostrarClasificadorasCopy.length;
            }else if(type=="modal_fuente"){
                this.arrFuentesCopy = [...this.arrFuentes.filter(
                    e=>e.codigo.toLowerCase().includes(search) || e.nombre.toLowerCase().includes(search)
                )];
                this.txtResultadosFuente = this.arrFuentesCopy.length;
            }else if(type == "cod_cuenta"){
                this.objCuenta = {};
                this.objCuenta = JSON.parse(JSON.stringify(this.arrCuentasCopy.filter(e=>e.codigo==search)[0]));
                const arrClasificador = this.arrClasificadores.filter(function(e){return e.cuenta == app.objCuenta.codigo});
                this.selectClasificadores(arrClasificador);
            }else if(type=="cod_clasificador"){
                this.objClasificador = {};
                this.objClasificador = JSON.parse(JSON.stringify(this.arrMostrarClasificadorasCopy.filter(e=>e.codigo==search)[0]));
            }else if(type=="cod_fuente"){
                this.objFuente = {};
                this.objFuente = JSON.parse(JSON.stringify(this.arrFuentesCopy.filter(e=>e.codigo==search)[0]));
            }
        },
        add:function(){
            let nombreDestino = "Otros";
            if(this.selectCompDestino == "M"){
                nombreDestino = "Municipal";
            }else if(this.selectCompDestino == "N"){
                nombreDestino = "Nacional";
            }else if(this.selectCompDestino =="D"){
                nombreDestino = "Departamental";
            }
            if(app.arrDetalles.length>0){
                if(!this.checkTerceros){
                    const flag = app.arrDetalles.every(function(e){return e.cuenta.codigo !=""});
                    if(!flag){
                        Swal.fire("Atención!","El detalle debe tener cuentas presupuestales.","warning");
                        return false;
                    }
                    if(app.objCuenta.codigo == "" || app.txtCompRetencion < 0){
                        Swal.fire("Atención!","Los campos con (*) son obligatorios.","warning");
                        return false;
                    }
                    if(app.isClasificador){
                        if(app.objClasificador.codigo == ""){
                            Swal.fire("Atención!","Los campos con (*) son obligatorios.","warning");
                            return false;
                        }
                    }
                }
                let flag = true;
                for (let i = 0; i < app.arrDetalles.length; i++) {
                    const element = app.arrDetalles[i];
                    if(app.isClasificador){
                        if(element.causacion.codigo == app.selectConceptoCausa
                            && element.ingreso.codigo == app.selectConceptoIngreso
                            && element.sgr.codigo == app.selectConceptoSgr && element.cuenta.codigo == app.objCuenta.codigo
                            && element.destino.tipo == app.selectCompDestino && element.cuenta_clasificadora.codigo == app.objClasificador.codigo){
                                flag = false;
                                break;
                            }
                    }else{
                        if(element.causacion.codigo == app.selectConceptoCausa && element.ingreso.codigo == app.selectConceptoIngreso
                            && element.sgr.codigo == app.selectConceptoSgr && element.cuenta.codigo == app.objCuenta.codigo
                            && element.destino.tipo == app.selectCompDestino || element.causacion.codigo == app.selectConceptoCausa
                            && element.ingreso.codigo == app.selectConceptoIngreso
                            && element.sgr.codigo == app.selectConceptoSgr && element.cuenta.codigo == app.objCuenta.codigo
                            && element.destino.tipo == app.selectCompDestino && app.isClasificador
                            && element.cuenta_clasificadora.codigo == app.objClasificador.codigo
                        ){
                            flag = false;
                            break;
                        }
                    }
                }
                if(flag){
                    this.arrDetalles.push(
                        {
                            causacion:app.selectConceptoCausa != "" ? app.arrConceptosCausa.filter(function(e){return e.codigo == app.selectConceptoCausa})[0]:{codigo:"",nombre:"",tipo:""},
                            ingreso:app.selectConceptoIngreso != "" ? app.arrConceptosIngreso.filter(function(e){return e.codigo == app.selectConceptoIngreso})[0]:{codigo:"",nombre:"",tipo:""},
                            sgr:app.selectConceptoSgr != "" ? app.arrConceptosSgr.filter(function(e){return e.codigo == app.selectConceptoSgr})[0]:{codigo:"",nombre:"",tipo:""},
                            cuenta:app.objCuenta,
                            cuenta_clasificadora:app.objClasificador,
                            destino:{
                                tipo:app.selectCompDestino,
                                nombre: nombreDestino
                            },
                            porcentaje:app.txtCompRetencion
                        }
                    );
                }else{
                    Swal.fire("Atención!","Este detalle ya fue agregado, debe tener al menos un dato distinto.","warning");
                    return false;
                }
            }else{
                if(!this.checkTerceros){
                    const flag = app.arrDetalles.every(function(e){return e.cuenta.codigo !=""});
                    if(!flag){
                        Swal.fire("Atención!","El detalle debe tener cuentas presupuestales.","warning");
                        return false;
                    }
                    if(app.objCuenta.codigo == "" || app.txtCompRetencion < 0){
                        Swal.fire("Atención!","Los campos con (*) son obligatorios.","warning");
                        return false;
                    }
                    if(app.isClasificador){
                        if(app.objClasificador.codigo == ""){
                            Swal.fire("Atención!","Los campos con (*) son obligatorios.","warning");
                            return false;
                        }
                    }
                }
                this.arrDetalles.push(
                    {
                        causacion:app.selectConceptoCausa != "" ? app.arrConceptosCausa.filter(function(e){return e.codigo == app.selectConceptoCausa})[0]:{codigo:"",nombre:"",tipo:""},
                        ingreso:app.selectConceptoIngreso != "" ? app.arrConceptosIngreso.filter(function(e){return e.codigo == app.selectConceptoIngreso})[0]:{codigo:"",nombre:"",tipo:""},
                        sgr:app.selectConceptoSgr != "" ? app.arrConceptosSgr.filter(function(e){return e.codigo == app.selectConceptoSgr})[0]:{codigo:"",nombre:"",tipo:""},
                        cuenta:app.objCuenta,
                        cuenta_clasificadora:app.objClasificador,
                        destino:{
                            tipo:app.selectCompDestino,
                            nombre: nombreDestino
                        },
                        porcentaje:app.txtCompRetencion
                    }
                );
            }
        },
        addFuente:function(){
            if(app.objFuente.codigo == ""){
                Swal.fire("Atención!","Los campos con (*) son obligatorios.","warning");
                return false;
            }
            if(app.arrDetallesFuente.length>0){
                let flag = true;
                for (let i = 0; i < app.arrDetallesFuente.length; i++) {
                    const element = app.arrDetallesFuente[i];
                    if(element.codigo == app.objFuente.codigo ){
                        flag = false;
                        break;
                    }
                }
                if(flag){
                    this.arrDetallesFuente.push(app.objFuente);
                }else{
                    Swal.fire("Atención!","Esta fuente ya fue agregada, intente con otra.","warning");
                    return false;
                }
            }else{
                this.arrDetallesFuente.push(app.objFuente);
            }
        },
        del:function(index){
            this.arrDetalles.splice(index,1);
        },
        delFuente:function(index){
            this.arrDetallesFuente.splice(index,1);
        },
        selectItem:function({...item},type=""){
            if(type ==""){
                this.objCuenta = item;
                this.isModal = false;
                const arrClasificador = this.arrClasificadores.filter(function(e){return e.cuenta == app.objCuenta.codigo});
                this.selectClasificadores(arrClasificador);
            }else if(type=="clasificador"){
                this.objClasificador = item;
                this.isModalClasificador = false;
            }else if(type =="fuentes"){
                this.objFuente = item;
                this.isModalFuente = false;
            }
        },
        selectClasificadores:function(arrClasificador){
            this.isClasificador = false;
            if(arrClasificador.length > 0){
                const clasificador = arrClasificador[0].clasificadores;
                this.isClasificador = true;
                const objCuentaClasificador = this.objCuentasClasificadores[clasificador];
                this.txtCuentaClasificadora = objCuentaClasificador.nombre;
                this.txtTituloModal = objCuentaClasificador.nombre;
                this.arrMostrarClasificadoras = objCuentaClasificador.cuentas;
                this.arrMostrarClasificadorasCopy = objCuentaClasificador.cuentas;
                this.txtResultadosClasificador = this.arrMostrarClasificadorasCopy.length;
                this.objClasificador={"nombre":"","codigo":""};
            }
        },
        changeTipo:function(){
            this.arrDetalles = [];
        },
        getSearchData: async function(){
            const formData = new FormData();
            formData.append("action","all");
            this.isLoading = true;
            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();
            this.arrData = objData;
            this.arrDataCopy = objData;
            this.txtResults = this.arrData.length;
            this.isLoading = false;
        },
        searchData:function(){
            let search = this.txtSearch.toLowerCase();
            this.arrDataCopy = [...this.arrData.filter(e=>e.nombre.toLowerCase().includes(search)
            || e.codigo.toLowerCase().includes(search))];
            this.txtResults = this.arrDataCopy.length;
        },
        editItem:function(id){
            window.location.href='teso-retencionesPagoEditar.php?id='+id;
        },
        changeStatus:function(item){
            const vueThis = this;
            Swal.fire({
                title:"¿Estás segur@ de cambiar el estado?",
                text:"",
                icon: 'warning',
                showCancelButton:true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText:"Sí",
                cancelButtonText:"No, cancelar"
            }).then(async function(result){
                if(result.isConfirmed){
                    const formData = new FormData();
                    formData.append("action","change");
                    formData.append("id",item.codigo);
                    formData.append("estado",item.estado =='S' ? 'N' : 'S');
                    const response = await fetch(URL,{method:"POST",body:formData});
                    const objData = await response.json();
                    if(objData.status){
                        let index = vueThis.arrData.findIndex(e => e.codigo == item.codigo);
                        vueThis.arrData[index].estado = item.estado =='S' ? 'N' : 'S';
                        vueThis.arrData[index].is_status = vueThis.arrData[index].estado =='S' ? 1 : 0;
                        vueThis.searchData();
                        Swal.fire("Estado actualizado","","success");
                    }else{
                        Swal.fire("Error",objData.msg,"error");
                    }
                }else{
                    vueThis.searchData();
                }
            });

        },
        nextItem:function(type){
            let vueContext = this;
            let id = this.txtConsecutivo
            let index = this.arrConsecutivos.findIndex(function(e){return e.id == id});
            if(type=="next" && vueContext.arrConsecutivos[++index]){
                id = this.arrConsecutivos[index++].id;
            }else if(type=="prev" && vueContext.arrConsecutivos[--index]){
                id = this.arrConsecutivos[index--].id;
            }
            window.location.href='teso-retencionesPagoEditar.php?id='+id;
        },
        showTab:function(tab){
            let tabs = this.$refs.rTabs.children;
            let tabsContent = this.$refs.rTabsContent.children;
            for (let i = 0; i < tabs.length; i++) {
                tabs[i].classList.remove("active");
                tabsContent[i].classList.remove("active")
            }
            tabs[tab-1].classList.add("active");
            tabsContent[tab-1].classList.add("active")
        }
    },
})
