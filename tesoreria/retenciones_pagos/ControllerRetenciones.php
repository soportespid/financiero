<?php
    date_default_timezone_set("America/Bogota");
    require_once 'ModelRetenciones.php';
    session_start();

    class ControllerRetenciones extends ModelRetenciones{
        public function __construct() {
            parent::__construct();
        }
        public function getData(){
            if(!empty($_SESSION)){
                $arrResponse = array(
                    "conceptos"=>$this->selectConceptos(),
                    "cuentas"=>$this->selectCuentas(),
                    "clasificadores"=>$this->selectClasificadores(),
                    "servicios"=>$this->selectServicios(),
                    "bienes"=>$this->selectBienes(),
                    "cuin"=>$this->selectCuin(),
                    "fuentes"=>$this->selectFuentes(),
                );
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
            die();
        }
        public function getAll(){
            if(!empty($_SESSION)){
                echo json_encode($this->selectData(),JSON_UNESCAPED_UNICODE);
            }
            die();
        }
        public function getInfoEdit(){
            if(!empty($_SESSION)){
                $id = strClean($_POST['codigo']);
                $request = $this->selectInfo($id);
                if(!empty($request)){
                    $arrResponse = array(
                        "status"=>true,
                        "data"=>$request,
                        "consecutivos"=>$this->selectConsecutivos(),
                        "conceptos"=>$this->selectConceptos(),
                        "cuentas"=>$this->selectCuentas(),
                        "clasificadores"=>$this->selectClasificadores(),
                        "servicios"=>$this->selectServicios(),
                        "bienes"=>$this->selectBienes(),
                        "cuin"=>$this->selectCuin(),
                        "fuentes"=>$this->selectFuentes()
                    );
                }else{
                    $arrResponse = array("status"=>false,"consecutivo"=>$this->selectConsecutivo());
                }
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
            die();
        }
        public function changeData(){
            if(!empty($_SESSION)){
                $id = intval($_POST['id']);
                $estado = $_POST['estado'];
                $request = $this->updateStatus($id,$estado);
                if($request == 1){
                    $arrResponse = array("status"=>true);
                }else{
                    $arrResponse = array("status"=>false,"msg"=>"Error");
                }
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
            die();
        }
        public function setData(){
            if(!empty($_SESSION)){
                if($_POST){
                    $objData = json_decode($_POST['data']);
                    if(empty($objData->nombre) || empty($objData->detalles)){
                        $arrResponse = array("status"=>false,"msg"=>"Error de datos");
                    }else{
                        $arrRequest = [];
                        $strNombre = replaceChar(strClean(strtoupper($objData->nombre)));
                        if($objData->consecutivo == 0){
                            $option = 1;
                            $codigo = $this->selectConsecutivo()+1;
                            $arrRequest = $this->insertCabecera(
                                $codigo,
                                $strNombre,
                                $objData->retencion,
                                $objData->tipo,
                                $objData->destino,
                                $objData->is_terceros,
                                $objData->is_iva,
                                $objData->is_nomina,
                                $objData->participacion
                            );
                            $requestId = is_array($arrRequest) ? $arrRequest['id'] : $arrRequest;
                        }else{
                            $option = 2;
                            $requestId = $this->updateCabecera(
                                $objData->consecutivo,
                                $strNombre,
                                $objData->retencion,
                                $objData->tipo,
                                $objData->destino,
                                $objData->is_terceros,
                                $objData->is_iva,
                                $objData->is_nomina,
                                $objData->participacion
                            );
                        }
                        if($requestId>0){
                            $request = $this->insertDetalle($requestId,$objData->detalles,$objData->fuentes);
                            if($request > 0){
                                if($option == 1){
                                    $arrResponse = array("status"=>true,"msg"=>"Datos guardados.","id"=>$arrRequest['codigo']);
                                }else{
                                    $arrResponse = array("status"=>true,"msg"=>"Datos actualizados.");
                                }
                            }else{
                                $arrResponse = array("status"=>false,"msg"=>"No se ha podido guardar el detalle.");
                            }
                        }else if($requestId=="existe"){
                            $arrResponse = array("status"=>false,"msg"=>"El nombre ya existe, pruebe con uno diferente.");
                        }else{
                            $arrResponse = array("status"=>false,"msg"=>"No se ha podido guardar la cabecera.");
                        }
                    }
                    echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
                }
            }
            die();
        }
    }
    if($_POST){
        $obj = new ControllerRetenciones();
        if($_POST['action']=="get"){
            $obj->getData();
        }else if($_POST['action']=="save"){
            $obj->setData();
        }else if($_POST['action']=="change"){
            $obj->changeData();
        }else if($_POST['action']=="all"){
            $obj->getAll();
        }else if($_POST['action']=="edit"){
            $obj->getInfoEdit();
        }
    }
?>
