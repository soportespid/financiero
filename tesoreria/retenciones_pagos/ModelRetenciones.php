<?php
    require_once '../../comun.inc';
    require_once '../../funciones.inc';
    require_once '../../Librerias/core/Helpers.php';

    Class ModelRetenciones{
        private $intId;
        private $linkbd;
        public function __construct(){
            $this->linkbd = conectar_v7();
            $this->linkbd->set_charset("utf8");
        }
        public function insertCabecera($intCodigo,$strNombre,$intRetencion,$strTipo,$strDestino,$isTerceros,$isIva,$isNomina,$strParticipacion){
            insertAuditoria("teso_auditoria","teso_funciones_id",2,"Crear",$intCodigo);
            $sql = "SELECT * FROM tesoretenciones WHERE nombre = '$strNombre'";
            $request = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
            if(empty($request)){
                $sql = "INSERT INTO tesoretenciones(codigo,nombre,tipo,estado,retencion,terceros,iva,destino,nomina,participacion)
                VALUES('$intCodigo','$strNombre','$strTipo','S','$intRetencion','$isTerceros','$isIva','$strDestino','$isNomina', '$strParticipacion')";
                mysqli_query($this->linkbd,$sql);
                $request = array("id"=>mysqli_insert_id($this->linkbd),"codigo"=>$this->selectConsecutivo());
            }else{
                $request = "existe";
            }
            return $request;
        }
        public function updateCabecera($intCodigo,$strNombre,$intRetencion,$strTipo,$strDestino,$isTerceros,$isIva,$isNomina,$strParticipacion){
            insertAuditoria("teso_auditoria","teso_funciones_id",2,"Editar",$intCodigo);
            $sql = "SELECT * FROM tesoretenciones WHERE nombre = '$strNombre' AND id != '$intCodigo'";
            $request = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
            if(empty($request)){
                $sql = "UPDATE tesoretenciones SET nombre='$strNombre',tipo='$strTipo',estado='S',retencion = '$intRetencion',
                terceros='$isTerceros',iva='$isIva',destino='$strDestino',nomina='$isNomina',participacion='$strParticipacion'
                WHERE id = '$intCodigo'";
                mysqli_query($this->linkbd,$sql);
                $request = $intCodigo;
            }else{
                $request = "existe";
            }
            return $request;
        }
        public function insertDetalle($intCodigo,$objDetalles,$objFuentes){
            mysqli_query($this->linkbd,"DELETE FROM tesoretenciones_det WHERE codigo = '$intCodigo'");
            mysqli_query($this->linkbd,"DELETE FROM tesoretenciones_det_presu WHERE codigo = '$intCodigo'");
            mysqli_query($this->linkbd,"DELETE FROM tesoretenciones_det_fuentes WHERE retencion_id = '$intCodigo'");
            $idRet = 0;
            foreach ($objFuentes as $obj) {
                $fuente = $obj->codigo;
                $sql = "INSERT INTO tesoretenciones_det_fuentes(retencion_id,fuente) VALUES('$intCodigo','$fuente')";
                mysqli_query($this->linkbd,$sql);
            }
            foreach ($objDetalles as $obj) {

                $causacion = $obj->causacion->codigo;
                $ingreso = $obj->ingreso->codigo;
                $sgr = $obj->sgr->codigo;
                $porcentaje = $obj->porcentaje;
                $destino = $obj->destino->tipo;
                $cuenta = $obj->cuenta->codigo;
                $cuentaClasificadora = $obj->cuenta_clasificadora->codigo;
                $codPresu = $this->selectConsecutivoPresu()+1;

                $causacion = $causacion == "" ? -1 : $causacion;
                $ingreso = $ingreso == "" ? -1 : $ingreso;
                $sgr = $sgr == "" ? -1 : $sgr;
                $sql = "INSERT INTO tesoretenciones_det(codigo,cod_presu,conceptocausa,conceptoingreso,modulo,tipoconce,porcentaje,estado,conceptosgr,destino)
                VALUES('$intCodigo','$codPresu','$causacion','$ingreso',4,'RE-RI',$porcentaje,'S','$sgr','$destino')";

                mysqli_query($this->linkbd,$sql);

                $idRet = mysqli_insert_id($this->linkbd);

                $sql = "INSERT INTO tesoretenciones_det_presu(id_retencion,codigo,cod_presu,cuentapres,cuenta_clasificadora)
                VALUES('$idRet','$intCodigo','$codPresu','$cuenta','$cuentaClasificadora')";
                mysqli_query($this->linkbd,$sql);
            }
            return $idRet;
        }
        public function selectConceptos(){
            $sql = "SELECT codigo,nombre,tipo FROM conceptoscontables WHERE modulo='4' AND (tipo = 'RE' OR tipo='SR' OR tipo ='RI' ) ORDER BY CAST(codigo AS UNSIGNED)";
            $request = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);

            return $request;
        }
        public function selectCuentas(){
            $maxVersion = ultimaVersionIngresosCCPET();
            //$sql = "SELECT codigo, nombre, tipo FROM cuentasingresosccpet WHERE municipio = '1' AND version = '$maxVersion'";
            $sql = "SELECT 
                    IF(t1.codigo_auxiliar IS NULL OR t1.codigo_auxiliar = '', t1.codigo, CONCAT(t1.codigo, '-', t1.codigo_auxiliar)) AS codigo, 
                    IF(t1.codigo_auxiliar IS NULL OR t1.codigo_auxiliar = '', t1.nombre, CONCAT(t1.nombre, '-', t1.nombre_auxiliar)) AS nombre, 
                    t1.tipo
                FROM cuentasingresosccpet t1
                INNER JOIN cuentasingresosccpetseleccionadas t2 ON t1.codigo = t2.codigo
                WHERE t1.version = $maxVersion";
            $request = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
            return $request;
        }
        public function selectClasificadores(){
            $sql = "SELECT clasificadores,cuenta FROM ccpetprogramarclasificadores";
            $request = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
            return $request;
        }
        public function selectServicios(){
            $sql = "SELECT grupo as codigo, titulo as nombre FROM ccpetservicios
                    WHERE version = (SELECT MAX(version) FROM ccpetbienestransportables) AND LENGTH(grupo) = 5
                    ORDER BY grupo ASC";
            $request = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
            return $request;
        }
        public function selectBienes(){
            $sql = "SELECT grupo as codigo, titulo as nombre FROM ccpetbienestransportables
                    WHERE version = (SELECT MAX(version) FROM ccpetbienestransportables) AND LENGTH(grupo) = 7
                    ORDER BY grupo ASC";
            $request = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
            return $request;
        }
        public function selectCuin(){
            $sql = "SELECT nombre,codigo_cuin as codigo FROM ccpet_cuin WHERE version=(SELECT MAX(version) FROM ccpet_cuin) ORDER BY id";
            $request = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
            return $request;
        }
        public function selectConsecutivo(){
            $sql = "SELECT MAX(CAST(codigo AS INT)) as consecutivo FROM tesoretenciones";
            $request = mysqli_query($this->linkbd,$sql)->fetch_assoc()['consecutivo'];
            return $request;
        }
        public function selectConsecutivos(){
            $sql = "SELECT codigo as id FROM tesoretenciones";
            $request = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
            return $request;
        }
        public function selectConsecutivoPresu(){
            $sql = "SELECT MAX(cod_presu) as consecutivo FROM tesoretenciones_det";
            $request = mysqli_query($this->linkbd,$sql)->fetch_assoc()['consecutivo'];
            return $request;
        }
        public function selectData(){
            $arrDestino = ['M'=>"Municipal","D"=>"Departamental","N"=>"Nacional",""=>"Otros"];
            $sql = "SELECT id,codigo,nombre,tipo,estado,destino FROM tesoretenciones ORDER BY CAST(codigo AS UNSIGNED) ASC";
            $request = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
            $total = count($request);
            for ($i=0; $i < $total; $i++) {
                $codigo = $request[$i]['id'];
                $sqlDet = "SELECT cuentapres,cuenta_clasificadora
                FROM tesoretenciones_det_presu WHERE codigo = '$codigo' ORDER BY id DESC";
                $request[$i]['is_status'] = $request[$i]['estado'] =="S"? 1 : 0;
                $request[$i]['tipo']= $request[$i]['tipo'] == "S" ? "Simple" :"Compuesto";
                $request[$i]['destino'] = $arrDestino[$request[$i]['destino']];
                $request[$i]['detalle'] = mysqli_fetch_all(mysqli_query($this->linkbd,$sqlDet),MYSQLI_ASSOC);
            }
            return $request;
        }
        public function updateStatus($id,$estado){
            insertAuditoria("teso_auditoria","teso_funciones_id",2,"Editar",$id);
            $sql = "UPDATE tesoretenciones SET estado = '$estado' WHERE codigo = $id";
            $request = mysqli_query($this->linkbd,$sql);
            return $request;
        }
        public function selectInfo($id){
            $this->intId = $id;
            $sql = "SELECT * FROM tesoretenciones WHERE codigo = '$this->intId'";
            $request = mysqli_query($this->linkbd,$sql)->fetch_assoc();
            if(!empty($request)){
                $this->intId = $request['id'];
                $sqlDet = "SELECT * FROM tesoretenciones_det  tr
                INNER JOIN tesoretenciones_det_presu tp ON tr.id = tp.id_retencion
                WHERE tr.codigo = '$this->intId'";
                $sqlFuente = "SELECT * FROM tesoretenciones_det_fuentes WHERE retencion_id = '$this->intId'";
                $request['detalle'] = mysqli_fetch_all(mysqli_query($this->linkbd,$sqlDet),MYSQLI_ASSOC);
                $request['fuentes'] = mysqli_fetch_all(mysqli_query($this->linkbd,$sqlFuente),MYSQLI_ASSOC);
            }
            return $request;
        }
        public function selectFuentes(){
            $sql="SELECT codigo_fuente as codigo, nombre
            FROM ccpet_fuentes_cuipo
            WHERE version = '1' ORDER BY codigo_fuente ASC";
            $request = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);;
            return $request;
        }
    }
?>
