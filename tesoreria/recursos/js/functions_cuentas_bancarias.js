const URL ='tesoreria/recursos/controllers/CuentasBancariasController.php';
var app = new Vue({
    el:"#myapp",
    data() {
        return {
            isLoading:false,
            isModal:false,
            isModalTercero:false,
            intPageVal: "",
            txtConsecutivo:0,
            txtNombre:"",
            arrCuentas:[],
            arrCuentasCopy:[],
            arrTerceros:[],
            arrTercerosCopy:[],
            txtSearchTercero:"",
            txtSearchCuentas:"",
            txtResultadosTerceros:0,
            txtResultadosCuentas:0,
            txtCuenta:"",
            arrSearch:[],
            arrSearchCopy:[],
            txtSearch:"",
            txtResultados:0,
            arrConsecutivos:[],
            selectTipo:"Ahorros",
            txtEstado:"",
            objCuenta:{codigo:"",nombre:""},
            objTercero:{codigo:"",nombre:""},
            arrDetalles:[]
        }
    },
    mounted() {
        const intPageVal = this.$refs.pageType.value;
        if(intPageVal == 2){
            this.getData();
            this.getEdit();
        }else if(intPageVal == 3){
            this.getSearch();
        }else{
            this.getData();
        }
    },
    methods: {
        getData:async function(){
            let formData = new FormData();
            formData.append("action","get");
            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();
            this.arrTerceros = objData.terceros;
            this.arrTercerosCopy = objData.terceros;
            this.arrCuentas = objData.cuentas;
            this.arrCuentasCopy = objData.cuentas;
            this.txtResultadosCuentas = this.arrCuentasCopy.length;
            this.txtResultadosTerceros = this.arrTercerosCopy.length;
        },
        getEdit:async function(){
            const codigo = new URLSearchParams(window.location.search).get('id');
            let formData = new FormData();
            formData.append("action","edit");
            formData.append("codigo",codigo);
            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();
            if(objData.status){                
                this.arrTerceros = objData.terceros;
                this.arrTercerosCopy = objData.terceros;
                this.arrCuentas = objData.cuentas;
                this.arrCuentasCopy = objData.cuentas;
                this.txtResultadosCuentas = this.arrCuentasCopy.length;
                this.txtResultadosTerceros = this.arrTercerosCopy.length;
                this.txtConsecutivo = codigo;
                const data = [...app.arrTerceros.filter(function(e){return e.cedulanit == codigo})][0];
                this.objTercero = {codigo:data.cedulanit,nombre:data.nombre};
                this.arrConsecutivos = objData.consecutivos;
                this.arrDetalles = objData.data;
            }else{
                window.location.href='teso-cuentas-bancarias-editar?id='+objData.consecutivo;
            }
        },
        search: function(type=""){
            let search = "";
            if(type == "modal")search = this.txtSearchCuentas.toLowerCase();
            if(type == "modal_tercero")search = this.txtSearchTercero.toLowerCase();
            if(type=="cod_tercero")search = this.objTercero.codigo;
            if(type == "cod_cuenta") search = this.objCuenta.codigo;
            if(type=="search")search = this.txtSearch;

            if(type=="modal"){
                this.arrCuentasCopy = [...this.arrCuentas.filter(e=>e.codigo.toLowerCase().includes(search) || e.nombre.toLowerCase().includes(search))];
                this.txtResultadosCuentas = this.arrCuentasCopy.length;
            }else if(type=="modal_tercero"){
                this.arrTercerosCopy = [...this.arrTerceros.filter(e=>e.codigo.toLowerCase().includes(search) || e.nombre.toLowerCase().includes(search))];
                this.txtResultadosTerceros = this.arrTercerosCopy.length;
            }else if(type == "cod_cuenta"){
                const data = [...this.arrCuentas.filter(e=>e.codigo == search)];
                this.objCuenta = data.length > 0 ? JSON.parse(JSON.stringify(data[0])) : {codigo:"",nombre:""};
            }else if(type=="cod_tercero"){
                const data = [...this.arrTerceros.filter(e=>e.codigo == search)];
                this.objTercero = data.length > 0 ? JSON.parse(JSON.stringify(data[0])) : {codigo:"",nombre:""};
            }else if(type=="search"){
                this.arrSearchCopy = [...this.arrSearch.filter(e=>e.tercero.toLowerCase().includes(search) || e.codigo.toLowerCase().includes(search)
                    || e.cuenta_banco.toLowerCase().includes(search) || e.nombre_tercero.toLowerCase().includes(search))];
                this.txtResultados = this.arrSearchCopy.length;
            }
        },
        add:function(){
            if(app.objCuenta.codigo =="" || app.txtCuenta==""){
                Swal.fire("Atención!","Debe seleccionar la cuenta contable y digitar la cuenta bancaria","warning");
                return false;
            }
            if(app.arrDetalles.length>0){
                let flag = false;
                for (let i = 0; i < app.arrDetalles.length; i++) {
                    const element = app.arrDetalles[i];
                    if(element.cuenta.codigo == app.objCuenta.codigo){
                        flag = true;
                        break;
                    }
                }
                if(!flag){
                    this.arrDetalles.push({
                        cuenta:JSON.parse(JSON.stringify(app.objCuenta)),
                        cuenta_bancaria:this.txtCuenta,
                        tipo:this.selectTipo,
                        flag:1
                    });
                }else{
                    Swal.fire("Atención!","Este detalle ya fue agregado","warning");
                    return false;
                }
            }else{
                this.arrDetalles.push({
                    cuenta:JSON.parse(JSON.stringify(app.objCuenta)),
                    cuenta_bancaria:this.txtCuenta,
                    tipo:this.selectTipo,
                    flag:1
                });
            }
            this.objCuenta = {codigo:"",nombre:""};
            this.txtCuenta="";
        },
        del:function(index){
            this.arrDetalles.splice(index,1);
        },
        selectItem:function({...item},type){
            if(type=="cuentas"){
                this.objCuenta = item;
                this.isModal = false;
            }else if(type=="tercero"){
                this.objTercero = item;
                this.isModalTercero = false;
            }
        },
        getSearch:async function(){
            const formData = new FormData();
            formData.append("action","search");
            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();
            this.arrSearch = objData;
            this.arrSearchCopy = objData;
            this.txtResultados = this.arrSearchCopy.length;
        },
        save:async function(){
            if(this.objTercero.codigo==""){
                Swal.fire("Atención!","Debe seleccionar el tercero.","warning");
                return false;
            }
            if(app.arrDetalles.length == 0){
                Swal.fire("Atención!","Debe agregar al menos una cuenta.","warning");
                return false;
            }
            Swal.fire({
                title:"¿Estás segur@ de guardar?",
                text:"",
                icon: 'warning',
                showCancelButton:true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText:"Sí, guardar",
                cancelButtonText:"No, cancelar"
            }).then(async function(result){
                if(result.isConfirmed){
                    const formData = new FormData();
                    formData.append("action","save");
                    formData.append("tercero",app.objTercero.codigo);
                    formData.append("detalle",JSON.stringify(app.arrDetalles));
                    formData.append("consecutivo",app.txtConsecutivo);
                    app.isLoading = true;
                    const response = await fetch(URL,{method:"POST",body:formData});
                    const objData = await response.json();
                    app.isLoading = false;
                    if(objData.status){
                        Swal.fire("Guardado",objData.msg,"success");
                        if(objData.id && app.txtConsecutivo == 0){
                            setTimeout(function(){
                                window.location.href='teso-cuentas-bancarias-editar?id='+objData.id;
                            },1500);
                        }else{
                            app.getData();
                        }
                    }else{
                        Swal.fire("Error",objData.msg,"error");
                    }
                }
            });
        },
        changeStatus:function(item){
            Swal.fire({
                title:"¿Estás segur@ de cambiar el estado?",
                text:"",
                icon: 'warning',
                showCancelButton:true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText:"Sí",
                cancelButtonText:"No, cancelar"
            }).then(async function(result){
                if(result.isConfirmed){
                    const formData = new FormData();
                    formData.append("action","change");
                    formData.append("tercero",item.tercero);
                    formData.append("cuenta",item.codigo);
                    formData.append("estado",item.estado =='S' ? 'N' : 'S');
                    const response = await fetch(URL,{method:"POST",body:formData});
                    const objData = await response.json();
                    if(objData.status){
                        let index = app.arrSearch.findIndex(e => e.tercero == item.tercero && e.codigo == item.codigo);
                        app.arrSearch[index].estado = item.estado =='S' ? 'N' : 'S';
                        app.arrSearch[index].is_status = app.arrSearch[index].estado =='S' ? 1 : 0;
                        app.search();
                        Swal.fire("Estado actualizado","","success");
                    }else{
                        Swal.fire("Error",objData.msg,"error");
                    }
                }else{
                    app.search();
                }
            });

        },
        nextItem:function(type){
            let id = this.txtConsecutivo;
            let index = this.arrConsecutivos.findIndex(function(e){return e.id == id});
            if(type=="next" && app.arrConsecutivos[++index]){
                id = this.arrConsecutivos[index++].id;
            }else if(type=="prev" && app.arrConsecutivos[--index]){
                id = this.arrConsecutivos[index--].id;
            }
            window.location.href='teso-cuentas-bancarias-editar?id='+id;
        },
    },
})
