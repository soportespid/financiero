<?php
    require_once '../../../Librerias/core/Helpers.php';
    require_once '../../../Librerias/core/Mysql.php';
    session_start();
    /*ini_set('display_errors', '1');
    ini_set('display_startup_errors', '1');
    error_reporting(E_ALL);*/
    class CuentasBancariasModel extends Mysql{
        private $intConsecutivo;
        private $strNombre;
        private $strTercero;
        private $intOrden;

        function __construct(){
            parent::__construct();
        }

        public function insertData(string $strTercero,array $arrData){
            $this->strTercero = $strTercero;
            foreach ($arrData as $data) {
                $sql="INSERT INTO tesobancosctas(cuenta,tercero,ncuentaban,tipo,estado) VALUES(?,?,?,?,?)";
                $this->insert($sql,[
                    $data['cuenta']['codigo'],
                    $this->strTercero,
                    $data['cuenta_bancaria'],
                    $data['tipo'],
                    "S"
                ]);
            }
            return 1;
        }
        public function updateData(string $strConsecutivo,array $arrData){
            $this->strTercero = $strConsecutivo;
            foreach ($arrData as $data) {
                if(isset($data['flag']) && $data['flag'] == 2){
                    $cuenta = $data['cuenta']['codigo'];
                    $sql="UPDATE tesobancosctas SET cuenta=?,ncuentaban=?,tipo=? WHERE tercero = '$this->strTercero' AND cuenta = '$cuenta'";
                    $this->insert($sql,[
                        $data['cuenta']['codigo'],
                        $data['cuenta_bancaria'],
                        $data['tipo'],
                    ]);
                }else if(isset($data['flag']) && $data['flag'] == 1){
                    $cuenta = $data['cuenta']['codigo'];
                    $sql="INSERT INTO tesobancosctas(cuenta,tercero,ncuentaban,tipo,estado) VALUES(?,?,?,?,?)";
                    $this->insert($sql,[
                        $data['cuenta']['codigo'],
                        $this->strTercero,
                        $data['cuenta_bancaria'],
                        $data['tipo'],
                        "S"
                    ]);
                }else{
                    $cuenta = $data['cuenta'];
                    $this->delete("DELETE FROM tesobancosctas WHERE tercero = '$this->strTercero' AND cuenta = '$cuenta'");
                }
            }
            return 1;
        }
        public function updateStatus($strTercero,$strCuenta,$estado){
            $this->strTercero = $strTercero;
            $sql = "UPDATE tesobancosctas SET estado = '$estado' WHERE tercero = '$this->strTercero' AND cuenta = '$strCuenta'";
            $request = $this->update($sql,[$estado]);
            return $request;
        }
        public function selectEdit($intConsecutivo){
            $this->strTercero = $intConsecutivo;
            $sql = "SELECT cuenta,ncuentaban,tipo FROM tesobancosctas WHERE tercero = $this->strTercero";
            $request = $this->select_all($sql);
            return $request;
        }
        public function selectCuentas(){
            $sql ="SELECT nic.cuenta as codigo,
            nic.nombre
            FROM cuentasnicsp nic
            WHERE nic.tipo = 'Auxiliar' AND LEFT(nic.cuenta,4) = '1110' AND nic.estado = 'S'
            AND NOT EXISTS (SELECT tb.cuenta FROM tesobancosctas tb WHERE tb.cuenta = nic.cuenta) ORDER BY nic.cuenta";
            $request = $this->select_all($sql);
            return $request;
        }
        public function selectSearch(){
            $sql ="SELECT nic.cuenta as codigo,
            nic.nombre,
            ban.tercero,
            ban.tipo,
            ban.estado,
            ban.ncuentaban as cuenta_banco,
            CASE WHEN t.razonsocial IS NULL OR t.razonsocial = ''
            THEN CONCAT(t.nombre1,' ',t.nombre2,' ',t.apellido1,' ',t.apellido2)
            ELSE t.razonsocial END AS nombre_tercero
            FROM cuentasnicsp nic
            INNER JOIN tesobancosctas AS ban ON ban.cuenta = nic.cuenta
            LEFT JOIN terceros AS t ON ban.tercero = t.cedulanit
            WHERE nic.tipo = 'Auxiliar' AND nic.estado = 'S' ORDER BY nic.cuenta";
            $request = $this->select_all($sql);
            $total = count($request);
            for ($i=0; $i < $total; $i++) {
                $request[$i]['is_status'] = $request[$i]['estado'] =="S"? 1 : 0;
            }
            return $request;
        }
    }
?>
