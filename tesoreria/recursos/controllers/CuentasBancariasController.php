<?php
    require_once '../../../Librerias/core/Helpers.php';
    require_once '../../../PHPExcel/Classes/PHPExcel.php';
    require_once '../models/CuentasBancariasModel.php';
    session_start();

    class CuentasBancariasController extends CuentasBancariasModel{
        public function getData(){
            if(!empty($_SESSION)){
                $arrData = [
                    "terceros"=>getTerceros(),
                    "cuentas"=>$this->selectCuentas()
                ];
                echo json_encode($arrData,JSON_UNESCAPED_UNICODE);
            }
            die();
        }
        public function save(){
            if(!empty($_SESSION)){
                $arrData = json_decode($_POST['detalle'],true);
                if(empty($_POST['tercero']) || empty($arrData)){
                    $arrResponse = array("status"=>false,"msg"=>"Error de datos");
                }else{
                    $strTercero =strClean($_POST['tercero']);
                    $strConsecutivo = strClean($_POST['consecutivo']);
                    $opcion="";
                    if($strConsecutivo == 0){
                        $opcion = 1;
                        $request = $this->insertData($strTercero,$arrData);
                    }else{
                        $opcion = 2;
                        $arrCuentas = $this->selectEdit($strConsecutivo);
                        $arrColumCuentas = array_column($arrCuentas,"cuenta");
                        $totalCuentas = count($arrCuentas);
                        $totalNuevasCuentas = count($arrData);
                        for ($i=0; $i < $totalCuentas; $i++) {
                            for ($j=0; $j < $totalNuevasCuentas ; $j++) {
                                if($arrData[$j]['cuenta']['codigo'] == $arrCuentas[$i]['cuenta']){
                                    $arrData[$j]['flag'] = 2;
                                    $arrCuentas[$i] = $arrData[$j];
                                    break;
                                }
                            }
                        }

                        foreach ($arrData as $data) {
                            if(!in_array($data['cuenta']['codigo'],$arrColumCuentas)){
                                array_push($arrCuentas,$data);
                            }
                        }
                        $request = $this->updateData($strConsecutivo,$arrCuentas);
                    }
                    if($request > 0){
                        if($opcion == 1){
                            $arrResponse = array("status"=>true,"msg"=>"Datos guardados correctamente.","id"=>$strTercero);
                        }else{
                            $arrResponse = array("status"=>true,"msg"=>"Datos actualizados correctamente.");
                        }
                    }else{
                        $arrResponse = array("status"=>false,"msg"=>"No se ha podido guardar, inténte de nuevo.");
                    }
                }
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
            die();
        }
        public function getEdit(){
            if(!empty($_SESSION)){
                $intId = strClean($_POST['codigo']);
                $request = $this->selectEdit($intId);
                if(!empty($request)){
                    $arrCuentas = $this->selectSearch();
                    $arrData = [];
                    foreach ($request as $e) {
                        array_push($arrData,[
                            "tipo"=>$e['tipo'],
                            "cuenta_bancaria"=>$e['ncuentaban'],
                            "cuenta"=>array_values(array_filter($arrCuentas,function($f)use($e){return $f['codigo']==$e['cuenta'];}))[0]
                        ]);
                    }
                    $arrResponse = array("status"=>true,"data"=>$arrData,"consecutivos"=>getConsecutivos("tesobancosctas","tercero"),"terceros"=>getTerceros(),
                    "cuentas"=>$this->selectCuentas());
                }else{
                    $consecutivos = getConsecutivos("tesobancosctas","tercero");
                    $arrResponse = array("status"=>false,"consecutivo"=>$consecutivos[count($consecutivos)-1]['id']);
                }
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
            die();
        }
        public function getSearch(){
            if(!empty($_SESSION)){
                $request = $this->selectSearch();
                echo json_encode($request,JSON_UNESCAPED_UNICODE);
            }
            die();
        }
        public function changeData(){
            if(!empty($_SESSION)){
                $id = strClean($_POST['tercero']);
                $cuenta = strClean($_POST['cuenta']);
                $estado = $_POST['estado'];
                $request = $this->updateStatus($id,$cuenta,$estado);
                if($request == 1){
                    $arrResponse = array("status"=>true);
                }else{
                    $arrResponse = array("status"=>false,"msg"=>"Error");
                }
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
            die();
        }
    }

    if($_POST){
        $obj = new CuentasBancariasController();
        if($_POST['action'] == "save"){
            $obj->save();
        }else if($_POST['action']=="edit"){
            $obj->getEdit();
        }else if($_POST['action']=="search"){
            $obj->getSearch();
        }else if($_POST['action']=="change"){
            $obj->changeData();
        }else if($_POST['action']=="get"){
            $obj->getData();
        }
    }

?>
