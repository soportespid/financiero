var app = new Vue({
    el: '#myapp',
	data:{

        error: '',
        loading: false,
        
        //Cabecera
        cuentaBancaria: '',
        codigoCuenta: '',
        nombreCuenta: '',
        cuenta: '',
        fuenteCuipo: '',
        codigoFuente: '',
        nombreFuente: '',

        //Modales
        showModalCuentas: false,
        showModalFuentes: false,
        cuentasDet: [],
        fuentesDet: [],
        searchBanco : {keywordBanco: ''},
        searchFuente : {keywordFuente: ''},

        //Detalles
        relacionDet: [],
    },

    mounted: function(){
        this.mostrarDetalles();
    },

    methods: 
    {
        //Desglosar array
        toFormData: function(obj){
            var form_data = new FormData();
            for(var key in obj){
                form_data.append(key, obj[key]);
            }
            return form_data;
        },

       desplegaModalCuentas: async function() {
            this.loading = true;

            await axios.post('tesoreria/relacionCuentaBancariaFuenteCCPET/teso-relacionCuentasBancariosFuentesCCPET.php?action=bancos')
            .then((response) => {
                app.cuentasDet = response.data.bancos;
            }).catch((error) => {
                this.error = true;
                console.log(error)
            }).finally(() => {
                this.loading = false;
                this.showModalCuentas = true;
            });
        },

        buscarBancos: async function() {

            var keywordBanco = app.toFormData(this.searchBanco);

            await axios.post('tesoreria/relacionCuentaBancariaFuenteCCPET/teso-relacionCuentasBancariosFuentesCCPET.php?action=filtraBanco', keywordBanco)
            .then((response) => {
                //console.log(response.data);
                app.cuentasDet = response.data.bancos;
            }).catch((error) => {
                this.error = true;
                console.log(error)
            }).finally(() => {
                this.loading = false;
                this.showModalCuentas = true;
            });
        },

        seleccionaBanco: async function(banco) {

            this.cuentaBancaria = banco[2] + ' - ' + banco[1];
            this.codigoCuenta = banco[2];
            this.nombreCuenta = banco[1];
            this.cuenta = banco[4];
            this.showModalCuentas = false;
        },

        desplegaModalFuentes: async function() {

            this.loading = true;

            await axios.post('tesoreria/relacionCuentaBancariaFuenteCCPET/teso-relacionCuentasBancariosFuentesCCPET.php?action=fuentes')
            .then((response) => {
                app.fuentesDet = response.data.fuentes;
            }).catch((error) => {
                this.error = true;
                console.log(error)
            }).finally(() => {
                this.loading = false;
                this.showModalFuentes = true;
            });
        },

        buscarFuentes: async function() {


            var keywordFuente = app.toFormData(this.searchFuente);
            this.loading = true;

            await axios.post('tesoreria/relacionCuentaBancariaFuenteCCPET/teso-relacionCuentasBancariosFuentesCCPET.php?action=filtraFuente', keywordFuente)
            .then((response) => {
                app.fuentesDet = response.data.fuentes;
            }).catch((error) => {
                this.error = true;
                console.log(error)
            }).finally(() => {
                this.loading = false;
                this.showModalFuentes = true;
            });
        },

        seleccionaFuente: async function(fuente) {

            this.fuenteCuipo = fuente[0] + ' ' + fuente[1];
            this.codigoFuente = fuente[0];
            this.nombreFuente = fuente[1];
            this.showModalFuentes = false;
        },

        guardar: async function() {

            if (this.cuentaBancaria != '' && this.codigoCuenta != '' && this.nombreCuenta != '' && this.cuenta != '' && this.fuenteCuipo != '' && this.codigoFuente != '' && this.nombreFuente != '') {

                var formData = new FormData();

                formData.append("banco", this.cuenta);
                formData.append("fuente", this.codigoFuente);

                await axios.post('tesoreria/relacionCuentaBancariaFuenteCCPET/teso-relacionCuentasBancariosFuentesCCPET.php?action=guardar', formData)
                .then((response) => {
                    
                    if (response.data.validacion) {

                        if (response.data.insertaBien) {
                            this.cuentaBancaria = '';
                            this.codigoCuenta = '';
                            this.nombreCuenta = '';
                            this.cuenta = '';
                            this.fuenteCuipo = '';
                            this.codigoFuente = '';
                            this.nombreFuente = '';
                        }
                        else {
                            Swal.fire(
                                'Error en guardado!',
                                'No se pudo guardar.',
                                'error'
                            );
                        }
                    }
                    else {
                        Swal.fire(
                            'Datos duplicados!',
                            'No se pudo guardar.',
                            'error'
                        );
                    }
                }).catch((error) => {
                    this.error = true;
                    console.log(error)
                }).finally(() => {
                    this.mostrarDetalles();
                });
            }
            else {
                Swal.fire(
                    'Falta información!',
                    'Para agregar falta información.',
                    'info'
                );
            }
           
        },

        eliminar: function(id) {


            Swal.fire({
                icon: 'question',
                title: 'Seguro que quieres eliminar?',
                showDenyButton: true,
                confirmButtonText: 'Eliminar',
                denyButtonText: 'Cancelar',
                }).then((result) => {
                if (result.isConfirmed) {

                    var formData = new FormData();

                    formData.append("id", id);
        
                    axios.post('tesoreria/relacionCuentaBancariaFuenteCCPET/teso-relacionCuentasBancariosFuentesCCPET.php?action=elimina', formData)
                    .then((response) => {
                        console.log(response.data);
                    }).catch((error) => {
                        this.error = true;
                        console.log(error)
                    }).finally(() => {
                        app.mostrarDetalles();
                    });
                } 
                else if (result.isDenied) {
                    Swal.fire('Eliminar cancelado', '', 'info')
                }
            })
        },

        mostrarDetalles: async function() {

            await axios.post('tesoreria/relacionCuentaBancariaFuenteCCPET/teso-relacionCuentasBancariosFuentesCCPET.php?action=informacion')
            .then((response) => {
                app.relacionDet = response.data.relacionDet;
            }).catch((error) => {
                this.error = true;
                console.log(error)
            }).finally(() => {
             
            });
        },
    }
});