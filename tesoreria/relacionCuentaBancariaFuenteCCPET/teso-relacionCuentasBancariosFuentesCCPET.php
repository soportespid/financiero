<?php
    require '../../comun.inc';
    require '../../funciones.inc';

    $linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");

    $out = array('error' => false);

    $action = "show";

    if(isset($_GET['action'])){
        $action = $_GET['action'];
    }

    if ($action == 'elimina') {

        $id = $_POST['id'];

        $sql = "DELETE FROM teso_banco_fuente WHERE id = $id";
        mysqli_query($linkbd, $sql);
    }

    if($action == 'bancos') {

        $bancos = array();

        //Busca bancos creados con sus terceros y sy
        $query = "SELECT terceros.razonsocial, cuentasnicsp.nombre, tesobancosctas.ncuentaban, tesobancosctas.tipo,tesobancosctas.cuenta FROM terceros, tesobancosctas, cuentasnicsp WHERE terceros.cedulanit=tesobancosctas.tercero AND cuentasnicsp.cuenta=tesobancosctas.cuenta ORDER BY cuentasnicsp.cuenta,terceros.cedulanit";
        $respuesta = mysqli_query($linkbd, $query);
        while ($row = mysqli_fetch_row($respuesta)) {
            
            array_push($bancos, $row);
        }

        $out['bancos'] = $bancos;
    }

    if ($action == 'filtraBanco') {

        $busqueda = $_POST['keywordBanco'];
        $bancos = array();

        //Busca bancos creados con sus terceros y sy
        $query = "SELECT terceros.razonsocial, cuentasnicsp.nombre, tesobancosctas.ncuentaban, tesobancosctas.tipo, tesobancosctas.cuenta FROM terceros, tesobancosctas, cuentasnicsp WHERE terceros.cedulanit=tesobancosctas.tercero AND cuentasnicsp.cuenta=tesobancosctas.cuenta AND tesobancosctas.ncuentaban LIKE '%$busqueda%' ORDER BY cuentasnicsp.cuenta,terceros.cedulanit";
        $respuesta = mysqli_query($linkbd, $query);
        while ($row = mysqli_fetch_row($respuesta)) {
            
            array_push($bancos, $row);
        }

        $out['bancos'] = $bancos;
    }

    if ($action == 'fuentes') {
        $sql="SELECT codigo_fuente, nombre FROM ccpet_fuentes_cuipo WHERE version=(SELECT MAX(version) FROM ccpet_fuentes_cuipo )";
        $res=mysqli_query($linkbd,$sql);
        $codigos = array();
        
        while($row=mysqli_fetch_row($res))
        {
            array_push($codigos, $row);
        }

        $out['fuentes'] = $codigos;
    }

    if ($action == 'filtraFuente') {

        $busqueda = $_POST['keywordFuente'];

        $sql="SELECT codigo_fuente, nombre FROM ccpet_fuentes_cuipo WHERE version=(SELECT MAX(version) FROM ccpet_fuentes_cuipo ) AND CONCAT(' ', codigo_fuente, nombre) LIKE '%$busqueda%'";
        $res=mysqli_query($linkbd,$sql);
        $codigos = array();
        
        while($row=mysqli_fetch_row($res))
        {
            array_push($codigos, $row);
        }

        $out['fuentes'] = $codigos;
    }

    if ($action == 'guardar') {

        $banco = $_POST['banco'];
        $fuente = $_POST['fuente'];
        
        $validaDatos = "SELECT * FROM teso_banco_fuente WHERE banco = '$banco' AND fuente = '$fuente'";
        $resp = mysqli_query($linkbd, $validaDatos);
        $valida = mysqli_num_rows($resp);

        if ($valida == 0) {

            $out['validacion'] = true;

            $id = selconsecutivo('teso_banco_fuente', 'id');

            $sql = "INSERT teso_banco_fuente VALUES ($id, '$banco', '$fuente')";
            if (mysqli_query($linkbd, $sql)) {
                $out['insertaBien'] = true;
            }
        }
        else {
    
        }
    }

    if ($action = 'informacion') {

        $relacionDet = array();

        $sql = "SELECT * FROM teso_banco_fuente";
        $res = mysqli_query($linkbd, $sql);
        while ($row = mysqli_fetch_row($res)) {
            
            $datos = array();

            $sqlBanco = "SELECT cuentasnicsp.nombre, tesobancosctas.ncuentaban, tesobancosctas.cuenta FROM terceros, tesobancosctas, cuentasnicsp WHERE terceros.cedulanit=tesobancosctas.tercero AND cuentasnicsp.cuenta=tesobancosctas.cuenta AND tesobancosctas.cuenta = '$row[1]'";
            $rowBanco = mysqli_fetch_row(mysqli_query($linkbd, $sqlBanco));

            $sqlFuente = "SELECT codigo_fuente, nombre FROM ccpet_fuentes_cuipo WHERE version=(SELECT MAX(version) FROM ccpet_fuentes_cuipo ) AND codigo_fuente = '$row[2]'";
            $rowFuente = mysqli_fetch_row(mysqli_query($linkbd, $sqlFuente));

            array_push($datos, $row[0]);
            array_push($datos, $rowBanco[1]);
            array_push($datos, $rowBanco[0]);
            array_push($datos, $rowFuente[0]);
            array_push($datos, $rowFuente[1]);
            array_push($relacionDet, $datos);
        }

        $out['relacionDet'] = $relacionDet;
    }

    header("Content-type: application/json");
    echo json_encode($out);
    die();
?>