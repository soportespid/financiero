<?php
    require_once '../../../comun.inc';
    require '../../../funciones.inc';
    require '../../../funcionesSP.inc.php';
    /*ini_set('display_errors', '1');
    ini_set('display_startup_errors', '1');
    error_reporting(E_ALL);*/
    session_start();
    //dep($_POST);exit;
    if($_POST){
        $obj = new Plantilla();
        if($_POST['action']=="search"){
            $currentPage = intval($_POST['page']);
            $obj->search($_POST['option'],$_POST['search'],$currentPage);
        }else if($_POST['action']=="get"){
            $obj->getData();
        }
    }

    class Plantilla{
        private $linkbd;
        private $arrData;
        public function __construct() {
            $this->linkbd = conectar_v7();
            $this->linkbd->set_charset("utf8");
        }
        public function search(string $option,string $search,int $currentPage){
            if(!empty($_SESSION)){
                if($option=="busqueda")$request = $this->selectData($search,$currentPage);
                if($option=="historial")$request = $this->selectHistorial($search,$currentPage);
                echo json_encode($request,JSON_UNESCAPED_UNICODE);
            }
            die();
        }
        public function getData(){
            if(!empty($_SESSION)){
                $request['busqueda'] = $this->selectData();
                $request['historial'] = $this->selectHistorial();
                echo json_encode($request,JSON_UNESCAPED_UNICODE);
            }
            die();
        }
        public function selectData($search="",$currentPage=1){
            $perPage = 200;
            $startRows = ($currentPage-1) * $perPage;
            $totalRows = 0;
            $s="";
            if($search !=""){
                $s="AND id_recaudo LIKE '$search%'";
            }
            $sql = "SELECT *,DATE_FORMAT(fecha,'%d/%m/%Y') as fecha FROM tesorecaudotransferencia
            WHERE estado !='' $s
            ORDER BY id_recaudo DESC LIMIT $startRows, $perPage";
            $arrData = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);

            $totalData = count($arrData);
            for ($i=0; $i < $totalData ; $i++) {
                $id = $arrData[$i]['id_recaudo'];
                $arrData[$i]['tercero'] = array(
                    "documento"=>$arrData[$i]['tercero'],
                    "nombre"=>$this->selectTercero($arrData[$i]['tercero'])
                );
                $arrData[$i]['valor'] = '$'.number_format($arrData[$i]['valortotal'],0,",",".");
            }
            $totalRows = mysqli_query($this->linkbd,"SELECT count(*) as total FROM tesorecaudotransferencia WHERE estado !='' $s")->fetch_assoc()['total'];
            $totalPages = $totalRows > 0 ? ceil($totalRows/$perPage) : 1;
            $arrResponse = array("status"=>true,"data"=>$arrData,"total"=>$totalRows,"total_pages"=>$totalPages);
            return $arrResponse;
        }
        public function selectHistorial($search="",$currentPage=1){
            $perPage = 200;
            $startRows = ($currentPage-1) * $perPage;
            $totalRows = 0;

            $sql ="SELECT
            t.id_recaudo,
            t.cuentabanco_ant,
            t.cuentabanco_nu,
            t.concepto,
            DATE_FORMAT(t.fecha,'%d/%m/%Y %l:%i:%s %p') as fecha,
            u.nom_usu
            FROM tesorecaudotransferencia_banco t
            INNER JOIN usuarios u
            ON t.usuario = u.cc_usu
            WHERE t.id_recaudo like '$search%'
            ORDER BY t.id DESC LIMIT $startRows, $perPage";
            $arrData = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);

            $totalRows = mysqli_query($this->linkbd,"SELECT count(*) as total FROM tesorecaudotransferencia_banco t
            INNER JOIN usuarios u
            ON t.usuario = u.cc_usu
            WHERE t.id_recaudo like '$search%'")->fetch_assoc()['total'];
            $totalPages = $totalRows > 0 ? ceil($totalRows/$perPage) : 1;
            $arrResponse = array("status"=>true,"data"=>$arrData,"total"=>$totalRows,"total_pages"=>$totalPages);
            return $arrResponse;
        }
        public function selectTercero($documento){
            $sql="SELECT CASE WHEN razonsocial IS NULL OR razonsocial = ''
            THEN CONCAT(nombre1,' ',nombre2,' ',apellido1,' ',apellido2)
            ELSE razonsocial END AS nombre FROM terceros WHERE cedulanit = '$documento'";
            $request = mysqli_query($this->linkbd,$sql)->fetch_assoc()['nombre'];
            return $request;
        }
    }
?>
