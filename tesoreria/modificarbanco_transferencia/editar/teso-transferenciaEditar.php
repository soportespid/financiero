<?php
    require_once '../../../comun.inc';
    require '../../../funciones.inc';
    require '../../../funcionesSP.inc.php';
    /*ini_set('display_errors', '1');
    ini_set('display_startup_errors', '1');
    error_reporting(E_ALL);*/
    session_start();
    if($_POST){
        $obj = new Plantilla();
        if($_POST['action']=="search"){
            $obj->search($_POST['search'],$_POST['option']);
        }else if($_POST['action']=="get"){
            $obj->getData($_POST['codigo']);
        }else if($_POST['action'] == "save"){
            $obj->save($_POST['data']);
        }
    }

    class Plantilla{
        private $linkbd;
        private $arrData;
        private $strVigencia;
        public function __construct() {
            $this->linkbd = conectar_v7();
            $this->linkbd->set_charset("utf8");
            $this->strVigencia = vigencia_usuarios($_SESSION['cedulausu']);
        }
        public function search(string $search,$option){
            if(!empty($_SESSION)){
                if($option=="modal_cuenta")$request = $this->selectCuentas($search);
                if($option=="codigo_cuenta")$request = $this->selectCuenta($search);
                echo json_encode($request,JSON_UNESCAPED_UNICODE);
            }
            die();
        }
        public function getData($codigo){
            if(!empty($_SESSION)){
                $data = $this->selectDatos($codigo);
                if(!empty($data)){
                    $request['consecutivo'] = $this->selectMax();
                    $request['datos'] = $data;
                    $request['cuentas'] = $this->selectCuentas();
                    $arrResponse = array("status"=>true,"data"=>$request);
                }else{
                    $arrResponse = array("status"=>false,"data"=>$this->selectMax());
                }
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
            die();
        }

        public function save($data){
            if(!empty($_SESSION)){
                $arrData = json_decode($data,true);
                if(empty($arrData['viejo']['ncuentaban']) || empty($arrData['nuevo']['ncuentaban']) || empty($arrData['razon'])){
                    $arrResponse = array("status"=>false,"msg"=>"Error de datos");
                }else{
                    $request = $this->updateData($arrData);
                    if(is_numeric($request) && $request > 0){
                        $arrResponse = array("status"=>true,"msg"=>"Datos guardados");
                    }else{
                        $arrResponse = array("status"=>false,"msg"=>"Ha ocurrido un error, no se ha podido guardar.");
                    }
                }
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
            die();
        }
        public function updateData(array $data){
            $id = $data['id'];
            $cuentaVieja = $data['viejo'];
            $cuentaNueva = $data['nuevo'];

            //Actualizo recibo caja
            $sql = "UPDATE tesorecaudotransferencia SET ncuentaban = '$cuentaNueva[ncuentaban]',banco = '$cuentaNueva[tercero]'
            WHERE id_recaudo = $id";
            $request = intval(mysqli_query($this->linkbd,$sql));


            //Actualizo comprobante detalle
            $sql = "UPDATE comprobante_det SET cuenta = '$cuentaNueva[cuenta]'
            WHERE tipo_comp = 14 AND numerotipo = $id AND (cuenta LIKE '1110%' OR cuenta LIKE '1105%')";
            $request = intval(mysqli_query($this->linkbd,$sql));


            //Guardo historial de cambios
            $sql = "INSERT INTO tesorecaudotransferencia_banco(id_recaudo,fecha,usuario,cuentabanco_ant,cuentabanco_nu,concepto)
            VALUES($id,NOW(),'$_SESSION[cedulausu]','$cuentaVieja[ncuentaban]','$cuentaNueva[ncuentaban]','$data[razon]')";
            $request = intval(mysqli_query($this->linkbd,$sql));
            return $request;
        }
        public function selectDatos($codigo){
            $sql = "SELECT *,DATE_FORMAT(fecha,'%d/%m/%Y') as fecha FROM tesorecaudotransferencia
            WHERE id_recaudo = '$codigo'";
            $request= mysqli_query($this->linkbd,$sql)->fetch_assoc();
            if(!empty($request)){
                $request['estado'] = $request['estado'] =="S" ? "Activo" : "Inactivo";
                $id = $request['id_recaudo'];
                $request['tercero'] = array(
                    "documento"=>$request['tercero'],
                    "nombre"=>$this->selectTercero($request['tercero'])
                );
                $request['detalle'] = $this->selectDatosDetalle($id);
                $request['valor'] = '$'.number_format($request['valortotal'],0,",",".");
            }
            return $request;
        }
        public function selectCuentas($search=""){
            $sql = "SELECT TB2.razonsocial,TB1.tercero,TB1.cuenta,TB1.ncuentaban,TB1.tipo
            FROM tesobancosctas TB1
            INNER JOIN terceros TB2
            ON TB1.tercero = TB2.cedulanit
            INNER JOIN cuentasnicsp TB3
            ON TB1.cuenta=TB3.cuenta
            WHERE TB1.estado='S' AND (TB2.razonsocial like '$search%' OR TB1.cuenta like '$search%' OR TB1.ncuentaban like '$search%')";
            $request['data'] = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
            $request['results'] = mysqli_query(
                $this->linkbd,
                "SELECT count(*) as total
                FROM tesobancosctas TB1
                INNER JOIN terceros TB2
                ON TB1.tercero = TB2.cedulanit
                INNER JOIN cuentasnicsp TB3
                ON TB1.cuenta=TB3.cuenta
                WHERE TB1.estado='S' AND (TB2.razonsocial like '$search%' OR TB1.cuenta like '$search%' OR TB1.ncuentaban like '$search%')")->fetch_assoc()['total'];
            return $request;
        }
        public function selectCuenta($search){
            $sql = "SELECT TB2.razonsocial,TB1.cuenta,TB1.ncuentaban,TB1.tipo
            FROM tesobancosctas TB1
            INNER JOIN terceros TB2
            ON TB1.tercero = TB2.cedulanit
            INNER JOIN cuentasnicsp TB3
            ON TB1.cuenta=TB3.cuenta
            WHERE TB1.estado='S' AND TB1.ncuentaban = '$search'";
            $request = mysqli_query($this->linkbd,$sql)->fetch_assoc();
            return $request;
        }
        public function selectDatosDetalle(int $id){
            $sql = "SELECT DISTINCT
                td.ingreso as codigo,
                td.valor,
                tp.nombre
                FROM tesorecaudotransferencia_det td
                INNER JOIN tesoingresos tp
                ON td.ingreso = tp.codigo
                WHERE td.id_recaudo = '$id'";
                $request = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);

            if(!empty($request)){
                $total = count($request);
                for ($i=0; $i < $total; $i++) {
                    $request[$i]['valor'] = "$".number_format($request[$i]['valor'],0,",",".");
                }
            }
            return $request;
        }
        public function selectMax(){
            $sql = "SELECT MAX(id_recaudo) as id FROM tesorecaudotransferencia";
            $request = mysqli_query($this->linkbd,$sql)->fetch_assoc()['id'];
            return $request;
        }
        public function selectTercero($documento){
            $sql="SELECT CASE WHEN razonsocial IS NULL OR razonsocial = ''
            THEN CONCAT(nombre1,' ',nombre2,' ',apellido1,' ',apellido2)
            ELSE razonsocial END AS nombre FROM terceros WHERE cedulanit = '$documento'";
            $request = mysqli_query($this->linkbd,$sql)->fetch_assoc()['nombre'];
            return $request;
        }
    }
?>
