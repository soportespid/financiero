<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
    header("Content-Type: text/html;charset=utf8");

    require 'comun.inc';
    require 'funciones.inc';

    session_start();
    date_default_timezone_set("America/Bogota");
?>

<html lang="es">
    <head>
        <meta charset="utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1"/>
		<meta name="viewport" content="user-scalable=no">
        <title>:: IDEAL 10 - Presupuesto</title>
        <link href="favicon.ico" rel="shortcut icon"/>

        <link href="css/css2.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
        <link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
        <link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
        <link href="css/tabs2.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
        <link href="css/style.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
        <script type="text/javascript" src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script type="text/javascript" src="css/calendario.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script type="text/javascript" src="JQuery/jquery-2.1.4.min.js"></script>

        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">

        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">

        <script type="text/javascript" src="bootstrap/fontawesome.5.11.2/js/all.js"></script>
        <link href="css/style.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
        <style>
            .c9 input[type="checkbox"]:not(:checked),
            .c9 input[type="checkbox"]:checked {
                position: absolute !important;
                left: -9999px !important;
            }
            .c9 input[type="checkbox"]:not(:checked) +  #t9,
            .c9 input[type="checkbox"]:checked +  #t9 {
                position: relative !important;
                padding-left: 1.95em !important;
                cursor: pointer !important;
            }

            /* checkbox aspect */
            .c9 input[type="checkbox"]:not(:checked) +  #t9:before,
            .c9 input[type="checkbox"]:checked +  #t9:before {
                content: '' !important;
                position: absolute !important;
                left: 0 !important; top: -2 !important;
                width: 1.55em !important; height: 1.55em !important;
                border: 2px solid #ccc !important;
                background: #fff !important;
                border-radius: 4px !important;
                box-shadow: inset 0 1px 3px rgba(0,0,0,.1) !important;
            }
            /* checked mark aspect */
            .c9 input[type="checkbox"]:not(:checked) +  #t9:after,
            .c9 input[type="checkbox"]:checked + #t9:after {
                content: url(imagenes/tilde.png) !important;
                position: absolute !important;
                top: .1em; left: .3em !important;
                font-size: 1.3em !important;
                line-height: 0.8 !important;
                color: #09ad7e !important;
                transition: all .2s !important;
            }
            /* checked mark aspect changes */
            .c9 input[type="checkbox"]:not(:checked) +  #t9:after {
                opacity: 0 !important;
                transform: scale(0) !important;
            }
            .c9 input[type="checkbox"]:checked +  #t9:after {
                opacity: 1 !important;
                transform: scale(1) !important;
            }
            /* disabled checkbox */
            .c9 input[type="checkbox"]:disabled:not(:checked) +  #t9:before,
            .c9 input[type="checkbox"]:disabled:checked +  #t9:before {
                box-shadow: none !important;
                border-color: #bbb !important;
                background-color: #ddd !important;
            }
            .c9 input[type="checkbox"]:disabled:checked +  #t9:after {
                color: #999 !important;
            }
            .c9 input[type="checkbox"]:disabled +  #t9 {
                color: #aaa !important;
            }
            /* accessibility */
            .c9 input[type="checkbox"]:checked:focus + #t9:before,
            .c9 input[type="checkbox"]:not(:checked):focus + #t9:before {
                border: 2px dotted blue !important;
            }

            /* hover style just for information */
            .c9 #t9:hover:before {
                border: 2px solid #4778d9 !important;
            }
            #t9{
                background-color: white !important;
            }

            .c10 input[type="checkbox"]:not(:checked),
            .c10 input[type="checkbox"]:checked {
                position: absolute !important;
                left: -9999px !important;
            }
            .c10 input[type="checkbox"]:not(:checked) +  #t10,
            .c10 input[type="checkbox"]:checked +  #t10 {
                position: relative !important;
                padding-left: 1.95em !important;
                cursor: pointer !important;
            }

            /* checkbox aspect */
            .c10 input[type="checkbox"]:not(:checked) +  #t10:before,
            .c10 input[type="checkbox"]:checked +  #t10:before {
                content: '' !important;
                position: absolute !important;
                left: 0 !important; top: -2 !important;
                width: 1.55em !important; height: 1.55em !important;
                border: 2px solid #ccc !important;
                background: #fff !important;
                border-radius: 4px !important;
                box-shadow: inset 0 1px 3px rgba(0,0,0,.1) !important;
            }
            /* checked mark aspect */
            .c10 input[type="checkbox"]:not(:checked) +  #t10:after,
            .c10 input[type="checkbox"]:checked + #t10:after {
                content: url(imagenes/tilde.png) !important;
                position: absolute !important;
                top: .1em; left: .3em !important;
                font-size: 1.3em !important;
                line-height: 0.8 !important;
                color: #09ad7e !important;
                transition: all .2s !important;
            }
            /* checked mark aspect changes */
            .c10 input[type="checkbox"]:not(:checked) +  #t10:after {
                opacity: 0 !important;
                transform: scale(0) !important;
            }
            .c10 input[type="checkbox"]:checked +  #t10:after {
                opacity: 1 !important;
                transform: scale(1) !important;
            }
            /* disabled checkbox */
            .c10 input[type="checkbox"]:disabled:not(:checked) +  #t10:before,
            .c10 input[type="checkbox"]:disabled:checked +  #t10:before {
                box-shadow: none !important;
                border-color: #bbb !important;
                background-color: #ddd !important;
            }
            .c10 input[type="checkbox"]:disabled:checked +  #t10:after {
                color: #999 !important;
            }
            .c10 input[type="checkbox"]:disabled +  #t10 {
                color: #aaa !important;
            }
            /* accessibility */
            .c10 input[type="checkbox"]:checked:focus + #t10:before,
            .c10 input[type="checkbox"]:not(:checked):focus + #t10:before {
                border: 2px dotted blue !important;
            }

            /* hover style just for information */
            .c10 #t10:hover:before {
                border: 2px solid #4778d9 !important;
            }
            #t10{
                background-color: white !important;
            }

            .background_active_color{
				background: #16a085;
			}
			.background_active{
				/* font: 115% sans-serif !important; */
    			/*font-weight: 700 !important;*/
				/*font-family: "Constantia", serif !important;*/
				font-family: calibri !important;
				font-weight: bold !important;
				font-size:20px !important;
			}
			.background_active_1{
				/* font: 115% sans-serif !important; */
    			/*font-weight: 700 !important;*/
				/*font-family: "Constantia", serif !important;*/
				font-family: helvética !important;
				font-size:20px !important;
			}
			.inicio--no-shadow{
				box-shadow: none;
			}
			.btn-delete{
				background: red;
				color: white;
				border-radius: 5px;
				border: none;
				font-size: 13px;
			}
			.btn-delete:hover, .btn-delete:focus{
				background: white;
				color: red;
			}
			.modal-mask {
			position: fixed;
			z-index: 9998;
			top: 0;
			left: 0;
			width: 100%;
			height: 100%;
			background-color: rgba(0, 0, 0, .5);
			display: table;
			transition: opacity .3s ease;
			}

			.modal-wrapper {
			display: table-cell;
			vertical-align: middle;
			}
			.modal-body{
				max-height: 500px;
				overflow-y: scroll;
			}
			.modal-intermetio{
				margin: 0 15px;
				font-family: helvética !important;
				font-size: 26px !important;
				padding: 10px 0;
			}
			.modal-intermedio-agregar{
				text-align:right;
				padding: 4px;
				margin-top: 6px;
				margin-right: 20px
			}
			.modal-body_1{
				padding-top: 15px;
				height: 40px;
			}
			.loader-table{
				/* background-color: #dff9fb;
				opacity: .5; */
				display: flex;
				align-items: center;
				justify-content: center;
				height: 75%;
			}
			.spinner{
				border: 4px solid rgba(0, 0, 0, 0.2);
				border-left-color: #39C;
				border-radius: 50%;
				width: 50px;
				height: 50px;
				animation: spin .9s linear infinite;
			}
			@keyframes spin {
				to { transform: rotate(360deg); }
			}
			.modal-container1
			{
				width: 50%;
				margin: 0px auto;
				padding: 20px 30px;
				text-align: left;
				background:linear-gradient(#99bbcc, #B6CEDA);
				border-radius: 10px;
				box-shadow: 0 2px 8px rgba(0, 0, 0, .33);
				transition: all .3s ease;
			}

            [v-cloak]{
                display : none;
            }

            label{
                font-size:13px;
            }

            input{
                height: calc(1em + 0.6rem + 0.5px) !important;
                font-size: 14px !important;
                margin-top: 4px !important;
            }

        </style>

    </head>
    <body>
        <div>
            <div id="myapp" style="height:inherit;" v-cloak>
                <div id="cargando" v-if="loading" class="loading">
					<span>Cargando...</span>
				</div>
                <div>
                    <table>
                        <tr><script>barra_imagenes("ccpet");</script><?php cuadro_titulos();?></tr>
                        <tr><?php menu_desplegable("ccpet");?></tr>
                    </table>
                    <div class="bg-white group-btn p-1">
                        <button type="button"  onClick="location.href='ccp-traslados-vue.php'" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
                            <span>Nuevo</span>
                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960">
                                <path d="M440-440H200v-80h240v-240h80v240h240v80H520v240h-80v-240Z"></path>
                            </svg>
                        </button>
                        <button type="button" v-on:Click="guardarAdd" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
                            <span>Guardar</span>
                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960">
                                <path d="M840-680v480q0 33-23.5 56.5T760-120H200q-33 0-56.5-23.5T120-200v-560q0-33 23.5-56.5T200-840h480l160 160Zm-80 34L646-760H200v560h560v-446ZM480-240q50 0 85-35t35-85q0-50-35-85t-85-35q-50 0-85 35t-35 85q0 50 35 85t85 35ZM240-560h360v-160H240v160Zm-40-86v446-560 114Z">
                                </path>
                            </svg>
                        </button>
                        <button type="button" v-on:click="location.href='ccp-buscarTraslados-vue.php'" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
                            <span>Buscar</span>
                            <svg class="fill-black group-hover:fill-white w-5 h-5" xmlns="http://www.w3.org/2000/svg"
                                viewBox="0 -960 960 960">
                                <path
                                    d="M784-120 532-372q-30 24-69 38t-83 14q-109 0-184.5-75.5T120-580q0-109 75.5-184.5T380-840q109 0 184.5 75.5T640-580q0 44-14 83t-38 69l252 252-56 56ZM380-400q75 0 127.5-52.5T560-580q0-75-52.5-127.5T380-760q-75 0-127.5 52.5T200-580q0 75 52.5 127.5T380-400Z">
                                </path>
                            </svg>
                        </button>
                        <button type="button" onclick="mypop=window.open('ccp-principal.php','','');mypop.focus();" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
                            <span>Nueva ventana</span>
                            <svg class="fill-black group-hover:fill-white w-5 h-5" xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960">
                                <path d="M320-120v-80h80v-80H160q-33 0-56.5-23.5T80-360v-400q0-33 23.5-56.5T160-840h640q33 0 56.5 23.5T880-760v400q0 33-23.5 56.5T800-280H560v80h80v80H320ZM160-360h640v-400H160v400Zm0 0v-400 400Z"></path>
                            </svg>
                        </button>
                        <button type="button" onclick="mypop=window.open('<?php echo $url2; ?>','','');mypop.focus();" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
                            <span class="group-hover:text-white">Duplicar pantalla</span>
                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960"><path d="M640-160v-360H160v360h480Zm80-200v-80h80v-360H320v200h-80v-200q0-33 23.5-56.5T320-880h480q33 0 56.5 23.5T880-800v360q0 33-23.5 56.5T800-360h-80ZM160-80q-33 0-56.5-23.5T80-160v-360q0-33 23.5-56.5T160-600h480q33 0 56.5 23.5T720-520v360q0 33-23.5 56.5T640-80H160Zm400-603ZM400-340Z"></path></svg>
                        </button>
                    </div>
                </div>
                <!--TABS-->
                <div ref="rTabs" class="nav-tabs p-1 bg-white">
                    <div class="nav-item active" @click="showTab(1)">Traslado presupuestal</div>
                    <div class="nav-item" @click="showTab(2)">Control de fuentes</div>
                </div>
                <!--CONTENIDO TABS-->
                <div ref="rTabsContent" class="nav-tabs-content">
                    <div class="nav-content active">
                        <div class="row" style="margin: -6px 0px 2px 0px">
                            <div class="col-12">
                                <div class="row" style="border-radius:2px; background-color: #F6F6F6; ">
                                    <div class="col-md-1" style="display: grid; align-content:center;">
                                        <label for="" style="margin-bottom: 0;">Fecha:</label>
                                    </div>
                                    <div class="col-md-2" style="padding: 1px">
                                        <div class="row">
                                            <div class="col-md-8" style="padding: 1px; margin-left: 15px;">
                                                <input type="text" id="fecha" name="fecha" class="form-control input" title="DD/MM/YYYY" placeholder="DD/MM/YYYY" onchange="">
                                            </div>
                                            <div class="col-md-2" style="padding: 2px; padding-top: 8px">
                                                <a href="#" onClick="displayCalendarFor('fecha');" tabindex="3" title="Calendario"><img src="imagenes/calendario04.png" align="absmiddle" style="width:20px;"></a>
                                            </div>
                                        </div>

                                    </div>

                                    <div class="col-md-1" style="display: grid; align-content:center;">
                                        <label for="" style="margin-bottom: 0; ">Acto adm:</label>
                                    </div>
                                    <div class="col-md-2" style="padding: 4px">
                                        <select v-model="acto_administrativo" class="form-control select" v-on:change="seleccionarActoAdm()" v-on:click="actosAdministrativos" style = "font-size: 8px !important; margin-top:1px; height: calc(1.5em + 0.8rem + 0.5px);">
                                            <option v-for="acto_administrativo in actos_administrativo" v-bind:value = "acto_administrativo[0]">
                                                {{ acto_administrativo[1] }} - {{ acto_administrativo[2] }}
                                            </option>
                                        </select>
                                    </div>

                                    <div class="col-md-1" style="display: grid; align-content:center;">
                                        <label for="" style="margin-bottom: 0;">Valor Acto Adm:</label>
                                    </div>
                                    <div class="col-md-1" style="padding: 1px">
                                        <input type="text" id="valor_acuerdo" name="valor_acuerdo" class="form-control" style = " text-align:center;" v-model="valor_acuerdo" readonly>
                                    </div>

                                    <div class="col-md-1" style="display: grid; align-content:center;">
                                        <label for="" style="margin-bottom: 0;">Finalizar:</label>
                                    </div>
                                    <div class="col-md-1 c9" style="padding: 1px; padding-top:10px;">
                                        <input type="checkbox"  id="checkbox" v-model="checked" v-on:change="puedeFinalizar">
                                        <label for="checkbox" id="t9" >
                                        </label>
                                    </div>

                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-12">
                                <h6 style="padding-left:30px; padding-top:4px; padding-bottom:1px; background-color: #559CFC; color:white">.: Cuenta CCPET y clasificadores complementarios.</h6>
                            </div>
                        </div>

                        <div class="row" style="margin: -6px 0px 2px 0px">
                            <div class="col-12">
                                <div class="row" style="border-radius:2px; background-color: #F6F6F6; ">

                                    <div class="col-md-1" style="display: grid; align-content:center;">
                                        <label for="" style="margin-bottom: 0;">Tipo traslado: </label>
                                    </div>
                                    <div class="col-md-1" style="padding: 1px">

                                        <select v-model="tipo_traslado" v-on:change="cambiaTipoTraslado" class="form-control select" style = "font-size: 10px; margin-top:4px">
                                            <option value="-1">Seleccione ...</option>
                                            <option v-for="tipo_traslado in tipos_traslado" v-bind:value="tipo_traslado.value">
                                                {{ tipo_traslado.text }}
                                            </option>
                                        </select>
                                    </div>

                                    <div class="col-md-1" style="display: grid; align-content:center;">
                                        <label for="" style="margin-bottom: 0;">Rubro:</label>
                                    </div>

                                    <div class="col-md-3" style="padding: 2px" v-if = "mostrarRubroContra">
                                        <input type="text" id="rubroPresupuestal" name="rubroPresupuestal" style="font-size: 11px; padding: 0px 0px 0px 4px;" class="form-control colordobleclik" v-on:dblclick="ventanaRubroCredito" v-on:change = "buscarRubroPresupuestal"  v-model="rubroPresupuestal" autocomplete="off" style = "height: calc(1.5em + 0.4rem + 1px);">
                                    </div>

                                    <div class="col-md-3" style="padding: 2px" v-else>
                                        <input type="text" id="rubroPresupuestal" name="rubroPresupuestal" style="font-size: 11px; padding: 0px 0px 0px 4px;" class="form-control colordobleclik" v-on:dblclick="ventanaRubroCredito" v-on:change = "buscarRubroPresupuestal"  v-model="rubroPresupuestal" autocomplete="off" style = "height: calc(1.5em + 0.4rem + 1px);">
                                    </div>
                                    <div class="col-md-6" style="display: grid; align-content:center; font-style: italic; color: #9D9E9D;">
                                        <label for="" style="margin-bottom: 0;">Programático-BPIM-fuente-sección presupuestal-vigencia del gasto-medio de pago</label>
                                    </div>
                                </div>
                                <div class="row" style="border-radius:2px; background-color: #F6F6F6; ">

                                    <div class="col-md-1" style="display: grid; align-content:center;">
                                        <label for="" style="margin-bottom: 0;">Tipo de Gasto:</label>
                                    </div>
                                    <div class="col-md-1" style="padding: 1px">
                                        <select v-model="tipoGasto" class="form-control select" v-on:change="cambiaTipoDeGasto" style = "font-size: 10px; margin-top:4px;">
                                            <option v-for="tipoDeGasto in tiposDeGasto" v-bind:value = "tipoDeGasto[0]">
                                                {{ tipoDeGasto[1] }}
                                            </option>
                                        </select>
                                    </div>

                                    <div class="col-md-2" style="display: grid; align-content:center;">
                                        <label for="" style="margin-bottom: 0;">Dependencia: </label>
                                    </div>
                                    <div class="col-md-1" style="padding: 1px">
                                        <select v-model="selectUnidadEjecutora" class="form-control select" style = "font-size: 10px; margin-top:4px">
                                            <option v-for="unidad in unidadesejecutoras" v-bind:value="unidad[0]">
                                                {{ unidad[0] }} - {{ unidad[1] }}
                                            </option>
                                        </select>
                                    </div>
                                    <div class="col-md-1" style="display: grid; align-content:center;">
                                        <label for="" style="margin-bottom: 0;">Medio pago: </label>
                                    </div>
                                    <div class="col-md-1" style="padding: 1px">

                                        <select v-model="selectMedioPago" class="form-control select" style = "font-size: 10px; margin-top:4px">
                                            <option v-for="option in optionsMediosPagos" v-bind:value="option.value">
                                                {{ option.text }}
                                            </option>
                                        </select>
                                    </div>

                                    <div class="col-md-1" style="display: grid; align-content:center;">
                                        <label for="" style="margin-bottom: 0;">Vig. del gasto: </label>
                                    </div>
                                    <div class="col-md-1" style="padding: 1px">
                                        <select v-model="selectVigenciaGasto" class="form-control select" style = "font-size: 10px; margin-top:4px">
                                            <option v-for="vigenciaDeGasto in vigenciasdelgasto" v-bind:value="vigenciaDeGasto[0]">
                                                {{ vigenciaDeGasto[1] }} - {{ vigenciaDeGasto[2] }}
                                            </option>
                                        </select>
                                    </div>

                                </div>
                            </div>
                        </div>

                        <div class="row" style="margin: -2px 0px 2px 0px">
                            <div class="col-12">
                                <div v-show = "mostrarFuncionamiento">

                                    <div class="row" style="border-radius:2px; background-color: #F6F6F6; ">

                                        <div class="col-md-1" style="display: grid; align-content:center;">
                                            <label for="" style="margin-bottom: 0;">Cuenta:</label>
                                        </div>

                                        <div class="col-md-1" style="padding: 2px">
                                            <input type="text" id="cuenta" name="cuenta" style="font-size: 11px; padding: 0px 0px 0px 4px;" class="form-control colordobleclik" v-on:dblclick="ventanaCuenta" v-on:change = "buscarCta"  v-model="cuenta" autocomplete="off" style = "height: calc(1.5em + 0.4rem + 1px);">
                                        </div>
                                        <div class="col-md-5" style="padding: 2px">
                                            <input type="text" id="nombreCuenta" name="nombreCuenta" class="form-control" v-model='nombreCuenta' style = "height: calc(1.5em + 0.4rem + 1px);" readonly>
                                        </div>

                                        <div class="col-md-1" style="display: grid; align-content:center;">
                                            <label for="" style="margin-bottom: 0;">Fuente:</label>
                                        </div>

                                        <div class="col-md-1" style="padding: 2px">
                                            <input type="text" id="fuente" name="fuente" class="form-control colordobleclik" v-on:dblclick="ventanaFuente" v-on:change = "buscarFuente" v-model="fuente" style = "height: calc(1.5em + 0.4rem + 1px);" autocomplete="off">
                                        </div>
                                        <div class="col-md-3" style="padding: 2px">
                                            <input type="text" id="nombreFuente" name="nombreFuente" class="form-control" v-model='nombreFuente' style = "height: calc(1.5em + 0.4rem + 1px);" readonly>
                                        </div>
                                    </div>
                                </div>

                                <div v-show = "mostrarInversion">
                                    <div class="row" style="border-radius:2px; background-color: #F6F6F6; ">
                                        <div class="col-md-1" v-show="selectOrden == 2" style="display: grid; align-content:center;">
                                            <label for="" style="margin-bottom: 0;">Sector:</label>
                                        </div>

                                        <div class="col-md-1" v-show="selectOrden == 2" style="padding: 2px">
                                            <input type="text" v-model="objSector.codigo" @change="search('cod_sector');" @dblclick="isModalSector=true" style="font-size: 11px; padding: 0px 0px 0px 4px;" class="form-control colordobleclik" style = "height: calc(1.5em + 0.4rem + 1px);">
                                        </div>
                                        <div class="col-md-3" v-show="selectOrden == 2" style="padding: 2px">
                                            <input type="text"  class="form-control" v-model="objSector.nombre" style = "height: calc(1.5em + 0.4rem + 1px);" readonly>
                                        </div>

                                        <div class="col-md-1" v-show="selectOrden == 2" style="display: grid; align-content:center;">
                                            <label for="" style="margin-bottom: 0;">Programa:</label>
                                        </div>

                                        <div class="col-md-1" v-show="selectOrden == 2" style="padding: 2px">
                                            <input v-model="objPrograma.codigo" @change="search('cod_programa');changePrograma()" @dblclick="isModalPrograma=true" type="text" id="codProyecto" name="codProyecto" style="font-size: 11px; padding: 0px 0px 0px 4px;" class="form-control colordobleclik" style = "height: calc(1.5em + 0.4rem + 1px);">
                                        </div>
                                        <div class="col-md-5" v-show="selectOrden == 2" style="padding: 2px">
                                            <input v-model="objPrograma.nombre" type="text" id="nombreProyecto" name="nombreProyecto" class="form-control" style = "height: calc(1.5em + 0.4rem + 1px);" readonly>
                                        </div>

                                        <div class="col-md-1" v-show="selectOrden == 2" style="display: grid; align-content:center;">
                                            <label for="" style="margin-bottom: 0;">Program&aacute;tico:</label>
                                        </div>

                                        <div class="col-md-1" v-show="selectOrden == 2" style="padding: 2px">
                                            <input type="text" id="programatico" name="programatico" style="font-size: 11px; padding: 0px 0px 0px 4px;" class="form-control colordobleclik" v-on:dblclick="ventanaProgramatico" v-on:change = "buscarProgramatico"  v-model="programatico" autocomplete="off" style = "height: calc(1.5em + 0.4rem + 1px);">
                                        </div>
                                        <div class="col-md-3" v-show="selectOrden == 2" style="padding: 2px">
                                            <input type="text" id="nombreProgramatico" name="nombreProgramatico" class="form-control" v-model='nombreProgramatico' style = "height: calc(1.5em + 0.4rem + 1px);" readonly>
                                        </div>
                                        <div class="col-md-1" style="display: grid; align-content:center;">
                                            <label for="" style="margin-bottom: 0;">Proyecto:</label>
                                        </div>

                                        <div class="col-md-1" style="padding: 2px">
                                            <input type="text" id="codProyecto" name="codProyecto" style="font-size: 11px; padding: 0px 0px 0px 4px;" class="form-control colordobleclik" v-on:dblclick="ventanaProyecto" v-on:change = "buscarProyecto"  v-model="codProyecto" autocomplete="off" style = "height: calc(1.5em + 0.4rem + 1px);">
                                        </div>
                                        <div class="col-md-5" style="padding: 2px">
                                            <input type="text" id="nombreProyecto" name="nombreProyecto" class="form-control" v-model='nombreProyecto' style = "height: calc(1.5em + 0.4rem + 1px);" readonly>
                                        </div>

                                        <div class="col-md-1" v-show="selectOrden != 2" style="display: grid; align-content:center;">
                                            <label for="" style="margin-bottom: 0;">Program&aacute;tico:</label>
                                        </div>

                                        <div class="col-md-1" v-show="selectOrden != 2" style="padding: 2px">
                                            <input type="text" id="programatico" name="programatico" style="font-size: 11px; padding: 0px 0px 0px 4px;" class="form-control colordobleclik" v-on:dblclick="ventanaProgramatico" v-on:change = "buscarProgramatico"  v-model="programatico" autocomplete="off" style = "height: calc(1.5em + 0.4rem + 1px);">
                                        </div>
                                        <div class="col-md-3" v-show="selectOrden != 2" style="padding: 2px">
                                            <input type="text" id="nombreProgramatico" name="nombreProgramatico" class="form-control" v-model='nombreProgramatico' style = "height: calc(1.5em + 0.4rem + 1px);" readonly>
                                        </div>

                                    </div>

                                    <div class="row" style="border-radius:2px; background-color: #F6F6F6; ">



                                        <div class="col-md-1" style="display: grid; align-content:center;">
                                            <label for="" style="margin-bottom: 0;">Fuente:</label>
                                        </div>

                                        <div class="col-md-1" style="padding: 2px">
                                            <input type="text" id="fuente" name="fuente" class="form-control colordobleclik" v-on:dblclick="ventanaFuente" v-on:change = "buscarFuente" v-model="fuente" style = "height: calc(1.5em + 0.4rem + 1px);" autocomplete="off">
                                        </div>
                                        <div class="col-md-3" style="padding: 2px">
                                            <input type="text" id="nombreFuente" name="nombreFuente" class="form-control" v-model='nombreFuente' style = "height: calc(1.5em + 0.4rem + 1px);" readonly>
                                        </div>

                                        <div class="col-md-1" style="display: grid; align-content:center;">
                                            <label for="" style="margin-bottom: 0;">Valor:</label>
                                        </div>

                                        <div class="col-md-1" style="padding: 1px">
                                            <input type="text" id="valor" name="valor" class="form-control" style = "height: calc(1.5em + 0.4rem + 1px); text-align:right; font-size: 14px;" v-model="valor">
                                        </div>

                                        <div v-show="mostrarSaldo" class="col-md-1" style="display: grid; align-content:center;">
                                            <label for="" style="margin-bottom: 0;">Saldo:</label>
                                        </div>

                                        <div v-show="mostrarSaldo" class="col-md-1" style="padding: 1px">
                                            <input type="text" id="saldo" name="saldo" class="form-control" style = "height: calc(1.5em + 0.4rem + 1px); text-align:right; font-size: 14px;" v-model="saldo" readonly>
                                        </div>

                                        <div class="col-md-2">
                                            <button type="button" class="btn btn-success btn-sm" v-on:click="agregarDetalle">Agregar Detalle</button>
                                        </div>

                                    </div>

                                </div>

                            </div>
                        </div>

                        <div class="row" style="margin: -2px 0px 2px 0px">
                            <div class="col-12">
                                <div class="row" style="border-radius:2px; background-color: #F6F6F6; ">


                                    <!-- <div class="col-md-1" style="padding: 1px">
                                        <input type="text" id="totalCdp" name="totalCdp" class="form-control" style = "height: calc(1.5em + 0.4rem + 1px);  text-align:right; font-size: 14px; color: #9D9E9D;" v-model="totalCdp" readonly>
                                    </div> -->
                                    <!-- <div class="col-md-5" > -->

                                        <div v-show = "mostrarFuncionamiento" class="col-md-1" style="display: grid; align-content:center;">
                                            <label for="" style="margin-bottom: 0;">Valor:</label>
                                        </div>

                                        <div v-show = "mostrarFuncionamiento" class="col-md-1" style="padding: 1px">
                                            <input type="text" id="valor" name="valor" class="form-control" style = "height: calc(1.5em + 0.4rem + 1px); text-align:right; font-size: 14px;" v-model="valor">
                                        </div>

                                        <div v-show="mostrarSaldo && mostrarFuncionamiento" class="col-md-1" style="display: grid; align-content:center;">
                                            <label for="" style="margin-bottom: 0;">Saldo:</label>
                                        </div>

                                        <div v-show="mostrarSaldo && mostrarFuncionamiento" class="col-md-1" style="padding: 1px">
                                            <input type="text" id="saldo" name="saldo" class="form-control" style = "height: calc(1.5em + 0.4rem + 1px); text-align:right; font-size: 14px;" v-model="saldo" readonly>
                                        </div>

                                        <div v-show = "mostrarFuncionamiento" class="col-md-2">
                                            <button type="button" class="btn btn-success btn-sm" v-on:click="agregarDetalle">Agregar Detalle</button>
                                        </div>
                                    <!-- </div> -->

                                </div>

                            </div>
                        </div>
                        <div class="overflow-auto fs-2" style="max-height:35vh">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <td width="5%" class='titulos' style="font: 80% sans-serif; padding-left:2px; border-radius: 5px 0 0 0;">Tipo traslado</td>
                                        <td width="5%" class='titulos' style="font: 80% sans-serif; ">Tipo de gasto</td>
                                        <td width="10%" class='titulos' style="font: 80% sans-serif; ">Dependencia</td>
                                        <td width="5%" class='titulos' style="font: 80% sans-serif; ">Medio pago</td>
                                        <td width="7%" class='titulos' style="font: 80% sans-serif; ">Vig. del gasto</td>
                                        <td width="10%" class='titulos' style="font: 80% sans-serif; ">Proyecto</td>
                                        <td width="10%" class='titulos' style="font: 80% sans-serif; ">Program&aacute;tico</td>
                                        <td width="10%" class='titulos' style="font: 80% sans-serif; ">Cuenta</td>
                                        <td width="10%" class='titulos' style="font: 80% sans-serif; ">Fuente</td>
                                        <td width="15%" class='titulos' style="font: 80% sans-serif; text-align: center;"> Credito</td>
                                        <td width="20%" class='titulos ' style="font: 80% sans-serif; text-align: center;"> Contracredito</td>
                                        <td width="8%" class='titulos' style="font: 80% sans-serif; padding-right:5px;">Quitar</td>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr v-for="detalle in detalles">
                                        <td style="font: 90% sans-serif; padding-left:10px">{{ detalle[0] }}</td>
                                        <td style="font: 90% sans-serif; padding-left:10px">{{ detalle[1] }}</td>
                                        <td style="font: 90% sans-serif; padding-left:10px">{{ detalle[2] }} - {{ detalle[3]}}</td>
                                        <td style="font: 90% sans-serif; padding-left:10px">{{ detalle[4] }}</td>
                                        <td style="font: 90% sans-serif; padding-left:10px">{{ detalle[5] }} - {{ detalle[6] }}</td>
                                        <td style="font: 90% sans-serif; padding-left:10px">{{ detalle[7] }} - {{ detalle[8] }}</td>
                                        <td style="font: 90% sans-serif; padding-left:10px">{{ detalle[9] }} - {{ detalle[10] }}</td>
                                        <td style="font: 90% sans-serif; padding-left:10px">{{ detalle[11] }} - {{ detalle[12] }}</td>
                                        <td style="font: 90% sans-serif; padding-left:10px">{{ detalle[13] }} - {{ detalle[14] }}</td>
                                        <td align="right" style="font: 90% sans-serif; padding-right:20px">{{ formatonumero(detalle[15]) }}</td>
                                        <td align="right" style="font: 90% sans-serif; padding-right:20px">{{ formatonumero(detalle[16]) }}</td>
                                        <td>
                                            <button type="button" class="btn btn-danger btn-sm" v-on:click="eliminarDetalle(detalle)">Eliminar</button>
                                        </td>
                                    </tr>
                                </tbody>
                                <tfoot>
                                <tr class='saludo1'>
                                    <td colspan = '9' style='font-size:14px; text-align: center; color:gray !important; ' class='saludo1'>Diferencia: {{ diferencia }}</td>
                                    <td align="right" style="font-size:12px sans-serif; padding-right:20px">{{ totalCredito }}</td>
                                    <td align="right" style="font-size:12px sans-serif; padding-right:20px">{{ totalContraCredito }}</td>
                                    <td></td>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                    <div class="nav-content">
                        <div class="bg-white">
                            <div class="overflow-auto" style="max-height:60vh">
                                <table  class="table fw-normal">
                                    <thead>
                                        <tr>
                                            <th>Fuente</th>
                                            <th class="text-right">Crédito</th>
                                            <th class="text-right">Contracrédito</th>
                                            <th class="text-right">Saldo</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr v-for="(data,index) in arrFuentes" :key="index" :class="data.saldo != 0 ? 'bg-danger text-white' : ''">
                                            <td>{{data.fuente+"-"+data.fuente_nombre}}</td>
                                            <td class="text-right">{{formatonumero(data.credito)}}</td>
                                            <td class="text-right">{{formatonumero(data.contra_credito)}}</td>
                                            <td class="text-right">{{formatonumero(data.saldo)}}</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div v-show="showModal_cuentas">
                    <transition name="modal">
                        <div class="modal-mask">
                            <div class="modal-wrapper">
                                <div class="modal-dialog modal-lg" style = "max-width: 1200px !important;" role="document">
                                    <div class="modal-content"  style = "width: 1200px !important;" scrollable>
                                        <div class="modal-header">
                                            <h5 class="modal-title">Cuentas CCPET</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true" @click="showModal_cuentas = false">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <div style="margin: 2px 0 0 0;">
                                                <div class="row" style="margin: 4px; border-radius:4px; background-color: #F6F6F6; ">
                                                    <div class="col-md-2" style="padding: 12px 0px 0px 30px; font: 140% sans-serif;">
                                                        <label for="">Cuenta:</label>
                                                    </div>

                                                    <div class="col-md-6 col-md-offset-6" style="padding: 4px">
                                                        <input type="text" class="form-control" placeholder="Buscar por nombre o c&oacute;digo de la cuenta" style="font: sans-serif; " v-on:keyup="searchMonitorCuenta" v-model="searchCuenta.keywordCuenta">
                                                    </div>
                                                </div>
                                                <table>
                                                    <thead>
                                                        <tr>
                                                            <td width="20%" class='titulos' style="font: 120% sans-serif; padding-left:10px; border-radius: 5px 0 0 0;">Cuenta</td>
                                                            <td class='titulos' style="font: 120% sans-serif; ">Nombre</td>
                                                            <td width="20%" class='titulos' style="font: 120% sans-serif; ">Tipo</td>

                                                        </tr>
                                                    </thead>
                                                </table>
                                            </div>
                                            <div style="margin: 2px 0 0 0; border-radius: 0 0 0 6px; height: 200px; overflow: scroll; overflow-x: hidden; background: white; ">
                                                <table class='inicio inicio--no-shadow'>
                                                    <tbody>
                                                        <?php
                                                            $co ='zebra1';
                                                            $co2='zebra2';
                                                        ?>
                                                        <tr v-for="cuenta in cuentasCcpet" v-on:click="seleccionarCuenta(cuenta)" class='<?php echo $co; ?>' style='text-rendering: optimizeLegibility; cursor: pointer !important; style=\"cursor: hand\"'>
                                                            <td width="20%" style="font: 120% sans-serif; padding-left:10px">{{ cuenta[1] }}</td>
                                                            <td  style="font: 120% sans-serif; padding-left:10px">{{ cuenta[2] }}</td>
                                                            <td width="20%" style="font: 120% sans-serif; padding-left:10px">{{ cuenta[6] }}</td>
                                                            <?php
                                                            $aux=$co;
                                                            $co=$co2;
                                                            $co2=$aux;
                                                            ?>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary" @click="showModal_cuentas = false">Cerrar</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </transition>
                </div>

                <div v-show="showModal_programaticoProyectos">
                    <transition name="modal">
                        <div class="modal-mask">
                            <div class="modal-wrapper">
                                <div class="modal-dialog modal-xl" style = "max-width: 1200px !important;" role="document">
                                    <div class="modal-content"  style = "width: 100% !important;" scrollable>
                                        <div class="modal-header">
                                            <h5 class="modal-title">Buscar rubros</h5>
                                            <button type="button" @click="showModal_programaticoProyectos=false;" class="btn btn-close"><div></div><div></div></button>
                                        </div>
                                        <div class="modal-body overflow-hidden">
                                            <div class="d-flex mb-3">
                                                <div class="form-control border-none">
                                                    <label class="form-label m-0" for="">Sector:</label>
                                                    <select @change="filter('sector')" v-model="selectSector">
                                                        <option value="0" disabled selected>Seleccione</option>
                                                        <option v-for="(data,index) in arrSectores" :key="index" :value="data.codigo">
                                                            {{data.codigo+"-"+data.nombre}}
                                                        </option>
                                                    </select>
                                                </div>
                                                <div class="form-control border-none">
                                                    <label class="form-label m-0" for="">Programa:</label>
                                                    <select @change="filter('programa')"  v-model="selectPrograma">
                                                        <option value="0" disabled selected>Seleccione</option>
                                                        <option v-for="(data,index) in arrProgramasFilter" :key="index" :value="data.codigo">
                                                            {{data.codigo+"-"+data.nombre}}
                                                        </option>
                                                    </select>
                                                </div>
                                                <div class="form-control border-none">
                                                    <label class="form-label m-0" for="">Producto:</label>
                                                    <select @change="filter('producto')"  v-model="selectProducto">
                                                        <option value="0" disabled selected>Seleccione</option>
                                                        <option v-for="(data,index) in arrProductosFilter" :key="index" :value="data.codigo">
                                                            {{data.codigo+"-"+data.nombre}}
                                                        </option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="overflow-auto" style="max-height:55vh">
                                                <table class='table table-hover'>
                                                    <thead>
                                                        <tr>
                                                            <th>Rubro</th>
                                                            <th>Proyecto</th>
                                                            <th>Fuente</th>
                                                            <th>Vigencia</th>
                                                            <th>Seccion presupuestal</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr v-for="cuenta in proyectosProgramaticosCopy" v-on:click="seleccionarProgramaticoProyecto(cuenta)" class='<?php echo $co; ?>' style='text-rendering: optimizeLegibility; cursor: pointer !important; style=\"cursor: hand\"'>
                                                            <td class="text-nowrap">{{ cuenta[0] }}</td>
                                                            <td >{{ cuenta[1] }}</td>
                                                            <td >{{ cuenta[2] }}</td>
                                                            <td >{{ cuenta[3] }}</td>
                                                            <td >{{ cuenta[4] }}</td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary" @click="showModal_programaticoProyectos = false">Cerrar</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </transition>
                </div>

                <div v-show="showModal_fuentes">
                    <transition name="modal">
                        <div class="modal-mask">
                            <div class="modal-wrapper">
                                <div class="modal-dialog modal-lg" style = "max-width: 1200px !important;" role="document">
                                    <div class="modal-content"  style = "width: 1200px !important;" scrollable>
                                        <div class="modal-header">
                                            <h5 class="modal-title">Fuentes CUIPO</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true" @click="showModal_fuentes = false">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <div style="margin: 2px 0 0 0;">
                                                <div class="row" style="margin: 4px; border-radius:4px; background-color: #F6F6F6; ">
                                                    <div class="col-md-3" style="padding: 12px 0px 0px 30px; font: 140% sans-serif;">
                                                        <label for="">C&oacute;digo o nombre:</label>
                                                    </div>

                                                    <div class="col-md-6 col-md-offset-6" style="padding: 4px">
                                                        <input type="text" class="form-control" placeholder="Buscar por nombre o c&oacute;digo de la fuente" style="font: sans-serif; " v-on:keyup="searchMonitorFuente" v-model="searchFuente.keywordFuente">
                                                    </div>
                                                </div>
                                                <table>
                                                    <thead>
                                                        <tr>
                                                            <td width="20%" class='titulos' style="font: 120% sans-serif; padding-left:10px; border-radius: 5px 0 0 0;">Fuente</td>
                                                            <td class='titulos' style="font: 120% sans-serif; ">Nombre</td>

                                                        </tr>
                                                    </thead>
                                                </table>
                                            </div>
                                            <div style="margin: 2px 0 0 0; border-radius: 0 0 0 6px; height: 200px; overflow: scroll; overflow-x: hidden; background: white; ">
                                                <table class='inicio inicio--no-shadow'>
                                                    <tbody>
                                                        <?php
                                                            $co ='zebra1';
                                                            $co2='zebra2';
                                                        ?>
                                                        <tr v-for="fuente in fuentesCuipo" v-on:click="seleccionarFuente(fuente)" class='<?php echo $co; ?>' style='text-rendering: optimizeLegibility; cursor: pointer !important; style=\"cursor: hand\"'>
                                                            <td width="20%" style="font: 120% sans-serif; padding-left:10px">{{ fuente[0] }}</td>
                                                            <td  style="font: 120% sans-serif; padding-left:10px">{{ fuente[1] }}</td>
                                                            <?php
                                                            $aux=$co;
                                                            $co=$co2;
                                                            $co2=$aux;
                                                            ?>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary" @click="showModal_fuentes = false">Cerrar</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </transition>
                </div>

                <div v-show="showModal_proyectos">
                    <transition name="modal">
                        <div class="modal-mask">
                            <div class="modal-wrapper">
                                <div class="modal-dialog modal-lg" style = "max-width: 1200px !important;" role="document">
                                    <div class="modal-content"  style = "width: 1200px !important;" scrollable>
                                        <div class="modal-header">
                                            <h5 class="modal-title">Proyectos de inversion </h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true" @click="showModal_proyectos = false">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <div style="margin: 2px 0 0 0;">
                                                <div class="row" style="margin: 4px; border-radius:4px; background-color: #F6F6F6; ">
                                                    <div class="col-md-2" style="padding: 12px 0px 0px 30px; font: 140% sans-serif;">
                                                        <label for="">Proyecto:</label>
                                                    </div>

                                                    <div class="col-md-6 col-md-offset-6" style="padding: 4px">
                                                        <input type="text" class="form-control" placeholder="Buscar por nombre o BPIM" style="font: sans-serif; " v-on:keyup="searchMonitorProyecto" v-model="searchProyecto.keywordProyecto">
                                                    </div>
                                                </div>
                                                <table>
                                                    <thead>
                                                        <tr>
                                                            <td width="20%" class='titulos' style="font: 120% sans-serif; padding-left:10px; border-radius: 5px 0 0 0;">BPIM</td>
                                                            <td class='titulos' style="font: 120% sans-serif; ">Nombre</td>
                                                        </tr>
                                                    </thead>
                                                </table>
                                            </div>
                                            <div style="margin: 2px 0 0 0; border-radius: 0 0 0 6px; height: 200px; overflow: scroll; overflow-x: hidden; background: white; ">
                                                <table class='inicio inicio--no-shadow'>
                                                    <tbody>
                                                        <?php
                                                            $co ='zebra1';
                                                            $co2='zebra2';
                                                        ?>
                                                        <tr v-for="proyecto in proyectos" v-on:click="seleccionarProyecto(proyecto)" class='<?php echo $co; ?>' style='text-rendering: optimizeLegibility; cursor: pointer !important; style=\"cursor: hand\"'>
                                                            <td width="20%" style="font: 120% sans-serif; padding-left:10px">{{ proyecto[2] }}</td>
                                                            <td  style="font: 120% sans-serif; padding-left:10px">{{ proyecto[4] }}</td>
                                                            <?php
                                                            $aux=$co;
                                                            $co=$co2;
                                                            $co2=$aux;
                                                            ?>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary" @click="showModal_proyectos = false">Cerrar</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </transition>
                </div>

                <div v-show="showModal_programatico">
                    <transition name="modal">
                        <div class="modal-mask">
                            <div class="modal-wrapper">
                                <div class="modal-dialog modal-lg" style = "max-width: 1200px !important;" role="document">
                                    <div class="modal-content"  style = "width: 1200px !important;" scrollable>
                                        <div class="modal-header">
                                            <h5 class="modal-title">Program&aacute;tico </h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true" @click="showModal_programatico = false">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <div style="margin: 2px 0 0 0;">
                                                <div class="row" style="margin: 4px; border-radius:4px; background-color: #F6F6F6; ">

                                                </div>
                                                <table>
                                                    <thead>
                                                        <tr>
                                                            <td width="20%" class='titulos' style="font: 120% sans-serif; padding-left:10px; border-radius: 5px 0 0 0;">Program&aacute;tico</td>
                                                            <td class='titulos' style="font: 120% sans-serif; ">Nombre</td>
                                                        </tr>
                                                    </thead>
                                                </table>
                                            </div>
                                            <div style="margin: 2px 0 0 0; border-radius: 0 0 0 6px; height: 200px; overflow: scroll; overflow-x: hidden; background: white; ">
                                                <table class='inicio inicio--no-shadow'>
                                                    <tbody>
                                                        <?php
                                                            $co ='zebra1';
                                                            $co2='zebra2';
                                                        ?>
                                                        <tr v-for="programatico in programaticos" v-on:click="seleccionarProgramatico(programatico)" class='<?php echo $co; ?>' style='text-rendering: optimizeLegibility; cursor: pointer !important; style=\"cursor: hand\"'>
                                                            <td width="20%" style="font: 120% sans-serif; padding-left:10px">{{ programatico[0] }}</td>
                                                            <td  style="font: 120% sans-serif; padding-left:10px">{{ programatico[1] }}</td>
                                                            <?php
                                                            $aux=$co;
                                                            $co=$co2;
                                                            $co2=$aux;
                                                            ?>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary" @click="showModal_programatico = false">Cerrar</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </transition>
                </div>
                <div v-show="isModalPrograma">
                    <transition name="modal">
                        <div class="modal-mask">
                            <div class="modal-wrapper">
                                <div class="modal-dialog modal-lg" style = "max-width: 1200px !important;" role="document">
                                    <div class="modal-content"  style = "width: 1200px !important;" scrollable>
                                        <div class="modal-header">
                                            <h5 class="modal-title">Buscar programas </h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true" @click="isModalPrograma = false">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <div style="margin: 2px 0 0 0;">
                                                <div class="row" style="margin: 4px; border-radius:4px; background-color: #F6F6F6; ">
                                                    <div class="col-md-2" style="padding: 12px 0px 0px 30px; font: 140% sans-serif;">
                                                        <label for="">Programa:</label>
                                                    </div>

                                                    <div class="col-md-6 col-md-offset-6" style="padding: 4px">
                                                        <input type="search" v-model="txtSearchProgramas" @keyup="search('modal_programa')" placeholder="Buscar">
                                                    </div>
                                                </div>
                                                <table>
                                                    <thead>
                                                        <tr>
                                                            <td width="20%" class='titulos' style="font: 120% sans-serif; padding-left:10px; border-radius: 5px 0 0 0;">Codigo</td>
                                                            <td class='titulos' style="font: 120% sans-serif; ">Nombre</td>
                                                        </tr>
                                                    </thead>
                                                </table>
                                            </div>
                                            <div class="overflow-auto max-vh-50 overflow-x-hidden " >
                                                <table class="table table-hover fw-normal p-2">
                                                    <thead>
                                                        <tr>
                                                            <th class="text-center">Item</th>
                                                            <th>Código</th>
                                                            <th>Nombre</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr v-for="(data,index) in arrProgramasCopy" :key="index" @dblclick="selectItem('modal_programa',data)">
                                                            <td>{{index+1}}</td>
                                                            <td>{{data.codigo}}</td>
                                                            <td>{{data.nombre}}</td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary" @click="isModalPrograma = false">Cerrar</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </transition>
                </div>
                <div v-show="isModalSector">
                    <transition name="modal">
                        <div class="modal-mask">
                            <div class="modal-wrapper">
                                <div class="modal-dialog modal-lg" style = "max-width: 1200px !important;" role="document">
                                    <div class="modal-content"  style = "width: 1200px !important;" scrollable>
                                        <div class="modal-header">
                                            <h5 class="modal-title">Buscar sectores </h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true" @click="isModalSector = false">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <div style="margin: 2px 0 0 0;">
                                                <div class="row" style="margin: 4px; border-radius:4px; background-color: #F6F6F6; ">
                                                    <div class="col-md-2" style="padding: 12px 0px 0px 30px; font: 140% sans-serif;">
                                                        <label for="">Sector:</label>
                                                    </div>

                                                    <div class="col-md-6 col-md-offset-6" style="padding: 4px">
                                                        <input type="search" v-model="txtSearchSectores" @keyup="search('modal_sector')" placeholder="Buscar">
                                                    </div>
                                                </div>
                                                <table>
                                                    <thead>
                                                        <tr>
                                                            <td width="20%" class='titulos' style="font: 120% sans-serif; padding-left:10px; border-radius: 5px 0 0 0;">Codigo</td>
                                                            <td class='titulos' style="font: 120% sans-serif; ">Nombre</td>
                                                        </tr>
                                                    </thead>
                                                </table>
                                            </div>
                                            <div class="overflow-auto max-vh-50 overflow-x-hidden " >
                                                <table class="table table-hover fw-normal p-2">
                                                    <thead>
                                                        <tr>
                                                            <th class="text-center">Item</th>
                                                            <th>Código</th>
                                                            <th>Nombre</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr v-for="(data,index) in arrSectoresCopy" :key="index" @dblclick="selectItem('modal_sector',data)">
                                                            <td>{{index+1}}</td>
                                                            <td>{{data.codigo}}</td>
                                                            <td>{{data.nombre}}</td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary" @click="isModalSector = false">Cerrar</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </transition>
                </div>
            </div>
        </div>

        <script src="Librerias/vue/vue.min.js"></script>
		<script src="Librerias/vue/axios.min.js"></script>
		<script src="presupuesto_ccpet/traslado/crear/ccp-traslados-vue.js?<?php echo date('d_m_Y_h_i_s');?>" type="module"></script>
        <script src="sweetalert2/dist/sweetalert2.min.js"></script>
        <link rel="stylesheet" href="sweetalert2/dist/sweetalert2.min.css">

    </body>
</html>
