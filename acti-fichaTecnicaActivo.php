<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require 'comun.inc';
	require 'funciones.inc';
	session_start();
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html lang=es>
	<head>
		<meta charset="utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1"/>
		<meta name="viewport" content="user-scalable=no">
		<title>:: IDEAL 10 - Activos Fijos</title>
		<link href="favicon.ico" rel="shortcut icon"/>
		<link href="css/css2z.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
        <link href="css/tabs.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" href="sweetalert2/dist/sweetalert2.min.css">
        <script src="sweetalert2/dist/sweetalert2.min.js"></script>
		<script src="vue/vue.min.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script src="css/calendario.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
        <script>
            
        </script>
		<style>
			input[type=file]::file-selector-button {
				margin-right: 20px;
				border: none;
				background: #084cdf;
				/* padding: 5px 5px; */
				border-radius: 10px;
				color: #fff;
				cursor: pointer;
				transition: background .2s ease-in-out;
			}

			input[type=file]::file-selector-button:hover {
				background: #0d45a5;
			}
            input.text, select.text, textarea.text {
                background: silver;
                border: 1px solid #393939;
                border-radius: 5px 5px 5px 5px;
                color: #393939;
                font-size: 12px;
                padding: 5px;
            }

			[v-cloak]{display : none;}

			.centrarSelect {

				height: 30px !important;
				text-align-last:center !important;
			}

			.aumentarTamaño {

				font-size:15px;
			}

            .tamano01 {
                text-align: center !important;
            }


		</style>
	</head>
	<body>
		<header>
			<table>
				<tr><script>barra_imagenes("acti");</script><?php cuadro_titulos();?></tr>
			</table>
		</header>

		<form name="form2" method="post" action="" enctype="multipart/form-data">
			<section id="myapp" v-cloak >
				<nav>
					<table>
						<tr><?php menu_desplegable("acti");?></tr>
						<tr>
							<td colspan="3" class="cinta">
								<img src="imagenes/add2.png" v-on:click="location.href=''" class="mgbt1" title="Nuevo">
								<img src="imagenes/guardad.png" title="Guardar" v-on:click="" class="mgbt1">
								<img src="imagenes/buscad.png" v-on:click="location.href=''" class="mgbt1" title="Buscar">
								<img src="imagenes/nv.png" onClick="mypop=window.open('acti-principal.php','',''); mypop.focus();" class="mgbt" title="Nueva Ventana">
							</td>
						</tr>
					</table>
				</nav>

				<article>
                    <div>
                        <table class="inicio">
							<tr>
								<td class="titulos" colspan="8">.: Ficha de activo fijo</td>
								<td class="cerrar" style='width:7%'><a onClick="location.href='acti-principal.php'">Cerrar</a></td>
							</tr>

							<tr>
								<td class="textonew01" style="width:3.5cm;">.: Placa:</td>
								<td>
									<input type="text" v-model="placa" @change="traeDatosFormulario" style="text-align:center;">
								</td>
                                
								<td class="textonew01" style="width:3.5cm;">.: Nombre:</td>
								<td colspan="3">
									<input type="text" v-model="nombre" style="width: 98%" readonly>
								</td>

								<td rowspan="10" colspan="2" style="text-align: right;"><img v-bind:src="img" alt="Foto" width="250" height="250"> </td>
							</tr> 
                            
                            <tr> 
								<td class="textonew01" style="width:3.5cm;">.: Fecha de compra:</td>
								<td>
									<input type="text" v-model="fechaCompra" style="text-align:center;" readonly>
								</td>

                                <td class="textonew01" style="width:3.5cm;">.: Fecha de activación:</td>
								<td>
									<input type="text" v-model="fechaActivacion" style="text-align:center;" readonly>
								</td>
                            </tr>

							<tr>
								<td class="textonew01" style="width:3.5cm;">.: Fecha registro:</td>
								<td>
									<input type="text" v-model="fechaRegistro" style="text-align:center;" readonly>
								</td>

								<td class="textonew01" style="width:3.5cm;">.: Valor activo:</td>
								<td>
									<input type="text" v-model="formatonumero(valorActivo)" style="text-align:center;" readonly>
								</td>
							</tr>

                            <tr>
								<td class="textonew01" style="width:3.5cm;">.: Valor depreciado:</td>
								<td>
									<input type="text" v-model="formatonumero(valorDepreciado)" style="text-align:center;" readonly>
								</td>

                                <td class="textonew01" style="width:3.5cm;">.: Valor Corrección:</td>
								<td>
									<input type="text" v-model="formatonumero(valorCorreccion)" style="text-align:center;" readonly>
								</td>
                            </tr>

                            <tr>
                                <td class="textonew01" style="width:3.5cm;">.: Referencia:</td>
								<td>
									<input type="text" v-model="referencia" readonly>
								</td>
                                
								<td class="textonew01" style="width:3.5cm;">.: Modelo:</td>
								<td>
									<input type="text" v-model="modelo" readonly>
								</td>
                            </tr>

							<tr>
								<td class="textonew01" style="width:3.5cm;">.: Serie:</td>
								<td>
									<input type="text" v-model="serial" readonly>
								</td>

								<td class="textonew01" style="width:3.5cm;">.: Estado:</td>
								<td>
									<input type="text" v-model="estadoActivo" style="width: 100%" readonly>
								</td>
							</tr>
                            
                            <tr>
                                <td class="textonew01" style="width:3.5cm;">.: Clase:</td>
								<td colspan="3">
									<input type="text" v-model="clase" style="width: 100%" readonly>
								</td>
                            </tr>

                            <tr>
                                <td class="textonew01" style="width:3.5cm;">.: Grupo:</td>
								<td colspan="3">
									<input type="text" v-model="grupo" style="width: 100%" readonly>
								</td>
                            </tr>

                            <tr>
                                <td class="textonew01" style="width:3.5cm;">.: Tipo:</td>
								<td colspan="3">
									<input type="text" v-model="tipo" style="width: 100%" readonly>
								</td>
                            </tr>

                            <tr>
                                <td class="textonew01" style="width:3.5cm;">.: Prototipo:</td>
								<td>
									<input type="text" v-model="prototipo" style="width: 100%" readonly>
								</td>

                                <td class="textonew01" style="width:3.5cm;">.: Area:</td>
								<td>
									<input type="text" v-model="area" style="width: 100%" readonly>
								</td>
                            </tr>

							<tr>
								<td class="textonew01" style="width:3.5cm;">.: Ubicación:</td>
								<td>
									<input type="text" v-model="ubicacion" style="width: 100%" readonly>
								</td>
							</tr>

                            <tr>
                                <td class="textonew01" style="width:3.5cm;">.: Centro de costos:</td>
								<td>
									<input type="text" v-model="cc" style="width: 100%" readonly>
								</td>

                                <td class="textonew01" style="width:3.5cm;">.: Disposición de activo:</td>
								<td>
									<input type="text" v-model="disposicion" style="width: 100%" readonly>
								</td>
                            </tr>

                            <tr>
                                <td class="textonew01" style="width:3.5cm;">.: Responsable:</td>
								<td colspan="3">
									<input type="text" v-model="responsable" style="width: 100%" readonly>
								</td>
                            </tr>

                            <tr>
                                <td class="textonew01">.: Foto:</td>
								<td>							
                                    <input type="file" id="foto" @change="obtenerImagen">
								</td>
                            </tr>
						</table>
                    </div>

					<div id="cargando" v-if="loading" class="loading">
						<span>Cargando...</span>
					</div>
				</article>
			</section>
		</form>

		<script src="node_modules/read-excel-file/bundle/read-excel-file.min.js"></script>
		<script src="node_modules/xlsx/dist/xlsx.full.min.js"></script>
		<script src="Librerias/vue/vue.min.js"></script>
		<script src="Librerias/vue/axios.min.js"></script>
		<script src="activos_fijos/fichaTecnica/acti-fichaTecnicaActivo.js?"></script>
        
	</body>
</html>