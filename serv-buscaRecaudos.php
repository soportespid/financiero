<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require 'comun.inc';
	require 'funciones.inc';
	session_start();
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html lang=es>
	<head>
    <meta charset="utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1"/>
		<meta name="viewport" content="user-scalable=no">
		<title>:: IDEAL 10 - Planeación</title>
		<link href="favicon.ico" rel="shortcut icon"/>
		<link href="css/css2z.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
        <link href="css/tabs.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" href="sweetalert2/dist/sweetalert2.min.css">
        <script type="text/javascript" src="css/programas.js"></script>
        <script src="sweetalert2/dist/sweetalert2.min.js"></script>
		<script src="vue/vue.min.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script src="css/calendario.js?<?php echo date('d_m_Y_h_i_s');?>"></script>

        <style>
            [v-cloak]{
                display : none;
            }
        </style>
	</head>
	<body>
		<header>
			<table>
				<tr><script>barra_imagenes("serv");</script><?php cuadro_titulos();?></tr>
			</table>
		</header>

		<form name="form2" method="post" action="">
			<section id="myapp" v-cloak>
				<nav>
					<table>
						<tr><?php menu_desplegable("serv");?></tr>
						<tr>
							<td colspan="3" class="cinta">
								<img src="imagenes/add.png" v-on:click="location.href=''" class="mgbt" title="Nuevo">
								<img src="imagenes/guardad.png" @click="" title="Guardar"  class="mgbt">
								<img src="imagenes/busca.png" v-on:click="location.href='plan-buscaCertificadoPAA'" class="mgbt" title="Buscar">
								<img src="imagenes/nv.png" onClick="mypop=window.open('plan-principal','',''); mypop.focus();" class="mgbt" title="Nueva Ventana">
							</td>
						</tr>
					</table>
				</nav>

				<article>
                    <div>
                        <table class="inicio">
                            <tr>
                                <td class="titulos" colspan="10">.: Datos de plan de adquisición</td>
                            </tr>
                            
                            <tr>
                                <td class="textonew01" style="width:10%;">.: Consecutivo:</td>
                                <td style="width: 11%;">
                                    <input type="text" style="width:100%;">
                                </td>

                                <td class="textonew01" style="width:10%;">.: Codigo de usuario:</td>
                                <td style="width: 11%;">
                                    <input type="text" style="width:100%;">
                                </td>

                                <td class="textonew01" style="width:10%;">.: Fecha inicio:</td>
                                <td style="width: 11%;">
                                    <input type="text" name="fechaIni"  value="<?php echo $_POST['fechaIni']?>" onKeyUp="return tabular(event,this)" id="fechaIni" title="DD/MM/YYYY" onDblClick="displayCalendarFor('fechaIni');" class="colordobleclik" autocomplete="off" onChange=""  readonly>
                                </td>

                                <td class="textonew01" style="width:10%;">.: Fecha final:</td>
                                <td style="width: 11%;">
                                <input type="text" name="fechaFin"  value="<?php echo $_POST['fechaFin']?>" onKeyUp="return tabular(event,this)" id="fechaFin" title="DD/MM/YYYY" onDblClick="displayCalendarFor('fechaFin');" class="colordobleclik" autocomplete="off" onChange=""  readonly>
                                </td>

                                <td style="padding-bottom:0px; text-align:center;">
                                    <em class="botonflechaverde">Buscar</em>
                                </td>
                            <tr>
                        </table>

                        <div class='subpantalla' style='height:50vh; width:100%; margin-top:0px;  overflow-x:hidden'>
                            <table class='inicio'>        
                                <thead>
                                    <tr>
                                        <th class="titulosnew00">Código de recaudo</th>
                                        <th class="titulosnew00">Código de usuario</th>							
                                        <th class="titulosnew00">Documento suscriptor</th>
                                        <th class="titulosnew00">Nombre suscriptor</th>
                                        <th class="titulosnew00">Número de factura</th>
                                        <th class="titulosnew00">Medio de pago</th>
                                        <th class="titulosnew00">Valor Pago</th>
                                        <th class="titulosnew00">Fecha de pago</th>
                                        <th class="titulosnew00">Realiza</th>
                                        <th class="titulosnew00">Estado</th> 
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <div id="cargando" v-if="loading" class="loading">
						<span>Cargando...</span>
					</div>
				</article>
			</section>
		</form>

		<script src="node_modules/read-excel-file/bundle/read-excel-file.min.js"></script>
		<script src="node_modules/xlsx/dist/xlsx.full.min.js"></script>
        <script src="Librerias/vue/vue.min.js"></script>
		<script src="Librerias/vue/axios.min.js"></script>
		<script type="module" src="planeacion/solicitudCertificadoPAA/buscar/plan-buscaCertificadoPAA.js"></script>
        
	</body>
</html>