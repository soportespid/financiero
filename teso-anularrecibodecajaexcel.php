<?php  
require_once 'PHPExcel/Classes/PHPExcel.php';
require "comun.inc";
require "funciones.inc";
session_start(); 
$objPHPExcel = new PHPExcel();

//----Propiedades----
$objPHPExcel->getProperties()
        ->setCreator("IDEAL 10")
        ->setLastModifiedBy("IDEAL 10")
        ->setTitle("Exportar Excel con PHP")
        ->setSubject("Documento de prueba")
        ->setDescription("Documento generado con PHPExcel")
        ->setKeywords("usuarios phpexcel")
        ->setCategory("reportes");

//----Cuerpo de Documento----
$objPHPExcel->setActiveSheetIndex(0)
	->mergeCells('A1:H1')
	->mergeCells('A2:H2')
	->setCellValue('A1', 'TESORERÍA - RECAUDO')
	->setCellValue('A2', 'ANULAR RECIBO DE CAJA');
	$objPHPExcel-> getActiveSheet ()
	-> getStyle ("A1")
	-> getFill ()
	-> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
	-> getStartColor ()
	-> setRGB ('C8C8C8');
	$objPHPExcel-> getActiveSheet ()
	-> getStyle ("A1:A2")
	-> getFont ()
	-> setBold ( true ) 
	-> setName ( 'Verdana' ) 
	-> setSize ( 10 ) 
	-> getColor ()
	-> setRGB ('000000');
	$objPHPExcel-> getActiveSheet ()
	-> getStyle ('A1:A2')
	-> getAlignment ()
	-> applyFromArray (array ( 'horizontal'  =>  PHPExcel_Style_Alignment :: HORIZONTAL_CENTER ,) );
	$objPHPExcel-> getActiveSheet ()
	-> getStyle ('A3:H3')
	-> getAlignment ()
	-> applyFromArray (array ( 'horizontal'  =>  PHPExcel_Style_Alignment :: VERTICAL_JUSTIFY ,) ); 
	$objPHPExcel-> getActiveSheet ()
	-> getStyle ("A2")
	-> getFill ()
	-> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
	-> getStartColor ()
	-> setRGB ('A6E5F3');
	$objPHPExcel-> getActiveSheet ()
	-> getStyle ("A3:H3")
	-> getFill ()
	-> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
	-> getStartColor ()
	-> setRGB ('22C6CB');
	$borders = array(
		'borders' => array(
			'allborders' => array(
				'style' => PHPExcel_Style_Border::BORDER_THIN,
			'color' => array('argb' => 'FF000000'),
			)
		),
	);
$objPHPExcel->getActiveSheet()->getStyle('A1:H1')->applyFromArray($borders);
$objPHPExcel->getActiveSheet()->getStyle('A2:H2')->applyFromArray($borders);
$objPHPExcel->getActiveSheet()->getStyle('A3:H3')->applyFromArray($borders);

$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A3', 'No Recibo')
            ->setCellValue('B3', 'Concepto')
            ->setCellValue('C3', 'Fecha')
			->setCellValue('D3', 'Contribuyente')
			->setCellValue('E3', 'Valor')
            ->setCellValue('F3', 'No Liquid')
            ->setCellValue('G3', 'Tipo')
			->setCellValue('H3', 'Estado');

$i=4;

for($x = 0; $x < count($_POST['nreciboE']); $x++ )
	{
		$objPHPExcel->setActiveSheetIndex(0)
		->setCellValueExplicit ("A$i", $_POST['nreciboE'][$x], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
		->setCellValueExplicit ("B$i", $_POST['conceptoE'][$x], PHPExcel_Cell_DataType :: TYPE_STRING)
		->setCellValueExplicit ("C$i", $_POST['fechaE'][$x], PHPExcel_Cell_DataType :: TYPE_STRING)
		->setCellValueExplicit ("D$i", $_POST['nomContribuyenteE'][$x], PHPExcel_Cell_DataType :: TYPE_STRING)
		->setCellValueExplicit ("E$i", $_POST['valorE'][$x], PHPExcel_Cell_DataType :: TYPE_STRING)
		->setCellValueExplicit ("F$i", $_POST['nliquiE'][$x], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
        ->setCellValueExplicit ("G$i", $_POST['tipoE'][$x], PHPExcel_Cell_DataType :: TYPE_STRING)
        ->setCellValueExplicit ("H$i", $_POST['estadoE'][$x], PHPExcel_Cell_DataType :: TYPE_STRING)
        ;
        $objPHPExcel->getActiveSheet()->getStyle("A$i:H$i")->applyFromArray($borders);
		$i++;
	}

//----Propiedades de la hoja
$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true); 
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true); 
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true); 
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true); 
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true); 
$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);  
$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);   
$objPHPExcel->getActiveSheet()->setTitle('Anular recibo de caja');
$objPHPExcel->setActiveSheetIndex(0);

//----Guardar documento----
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename="Teso-Recaudo-Anular-Recibo-de-caja.xls"');
header('Cache-Control: max-age=0');
 
$objWriter=PHPExcel_IOFactory::createWriter($objPHPExcel,'Excel5');
$objWriter->save('php://output');
exit;

?>