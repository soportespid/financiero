<?php
header("Cache-control: no-cache, no-store, must-revalidate");
header("Content-Type: text/html;charset=utf8");
require 'comun.inc';
require 'funciones.inc';
require 'conversor.php';
session_start();
$linkbd = conectar_v7();
$linkbd -> set_charset("utf8");
cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html lang=es>
	<head>
		<meta charset="utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1"/>
		<meta name="viewport" content="user-scalable=no">
		<title>:: IDEAL 10 - Presupuesto CCPET</title>
		<link href="favicon.ico" rel="shortcut icon"/>
		<link href="css/css2.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/tabs.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<script src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
        <script>

            function validar()
            {
                document.form2.submit();
            }

            function guardar()
            {
                if (document.form2.fecha.value!='')
                {
                    if(confirm("Esta Seguro de Guardar"))
                    {
                        document.form2.oculto.value=2;
                        document.form2.submit();
                    }
                }
                else{
                    alert('Faltan datos para completar el registro');
                    document.form2.fecha.focus();
                    document.form2.fecha.select();
                }
            }

            function adelante()
            {
                if(parseFloat(document.form2.ncomp.value)<parseFloat(document.form2.maximo.value))
                {
                    document.form2.oculto.value=1;
                    document.form2.ncomp.value=parseFloat(document.form2.ncomp.value)+1;
                    document.form2.idcomp.value=parseFloat(document.form2.idcomp.value)+1;
                    document.form2.action="ccp-recibocaja-reflejarppto.php";
                    document.form2.submit();
                }
            }

            function atrasc()
            {
                if(document.form2.ncomp.value>1)
                {
                    document.form2.oculto.value=1;
                    document.form2.ncomp.value=document.form2.ncomp.value-1;
                    document.form2.idcomp.value=document.form2.idcomp.value-1;
                    document.form2.action="ccp-recibocaja-reflejarppto.php";
                    document.form2.submit();
                }
            }

            function validar2()
            {
                document.form2.oculto.value=1;
                document.form2.ncomp.value=document.form2.idcomp.value;
                document.form2.action="ccp-recibocaja-reflejarppto.php";
                document.form2.submit();
            }

        </script>
    </head>
    <body>
        <IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
        <span id="todastablas2"></span>
        <table>
            <tr><script>barra_imagenes("ccpet");</script><?php cuadro_titulos();?></tr>	 
            <tr><?php menu_desplegable("ccpet");?></tr>
            <tr>
                <td colspan="3" class="cinta">
                    <a class="mgbt" href="#" ><img src="imagenes/add2.png" alt="Nuevo"  border="0" /></a> 
                    <a class="mgbt" href="#"><img src="imagenes/guardad.png"  alt="Guardar" /></a> 
                    <a class="mgbt" href="ccp-buscarecibocaja-reflejar.php"> <img src="imagenes/busca.png"  alt="Buscar" /></a>
                    <a onClick="mypop=window.open('plan-agenda.php','','');mypop.focus()" class="mgbt"><img src="imagenes/agenda1.png" title="Agenda" /></a>
                    <a class="mgbt" href="#" onClick="mypop=window.open('ccp-principal.php','','');mypop.focus();"><img src="imagenes/nv.png" alt="nueva ventana"></a> 
                    <a class="mgbt" href="#" onClick="guardar()"><img src="imagenes/reflejar1.png"  alt="Reflejar" style="width:24px;" /></a>
                    <a class="mgbt" href="ccp-reflejar.php"><img src="imagenes/iratras.png" alt="nueva ventana"></a>
                </td>
            </tr>		  
        </table>
        <tr>
            <td colspan="3" class="tablaprin" align="center"> 
	            <form name="form2" method="post" action=""> 
		            <?php
                    
                    function obtenerTipoPredio($catastral)
                    {
                        $tipo="";
                        $digitos=substr($catastral,5,2);
                        if($digitos=="00"){$tipo="rural";}
                        else {$tipo="urbano";}
                        return $tipo;
                    }
                    
                    if ($_GET['idrecibo']!=""){echo "<script>document.getElementById('codrec').value=$_GET[idrecibo];</script>";}
                    $sqlr = "select id_recibos,id_recaudo from  tesoreciboscaja ORDER BY id_recibos DESC";
                    $res = mysqli_query($linkbd, $sqlr);
                    $r = mysqli_fetch_row($res);
                    $_POST['maximo']=$r[0];
                    if(!$_POST['oculto']){
                        $check1="checked";
                        
                        if ($_POST['codrec']!="" || $_GET['idrecibo']!="")
                        {
                            if($_POST['codrec']!="")
                            {
                                $sqlr="select id_recibos,id_recaudo from  tesoreciboscaja where id_recibos='$_POST[codrec]'";
                            }
                            else
                            {
                                $sqlr="select id_recibos,id_recaudo from  tesoreciboscaja where id_recibos='$_GET[idrecibo]'";
                            }
                        }
                        else
                        {
                            $sqlr="select id_recibos,id_recaudo from  tesoreciboscaja ORDER BY id_recibos DESC";
                        }
                        $res=mysqli_query($linkbd, $sqlr);
                        $r=mysqli_fetch_row($res);
                        //$_POST[maximo]=$r[0];
                        $_POST['ncomp']=$r[0];
                        $_POST['idcomp']=$r[0];
                        $_POST['idrecaudo']=$r[1];
                    }
                    
                    if ($_POST['codrec']!="")
                    {
                        $sqlr = "select * from tesoreciboscaja where id_recibos='$_POST[codrec]'";
                    }
                    else
                    {
                        $sqlr = "select * from tesoreciboscaja where id_recibos=$_POST[idcomp]";
                    }
                    $res = mysqli_query($linkbd, $sqlr);
                    while($r = mysqli_fetch_row($res))
                    {
                        $_POST['tiporec']=$r[10];
                        $_POST['idrecaudo']=$r[4];
                        $_POST['ncomp']=$r[0];
                        $_POST['modorec']=$r[5];
                        $_POST['fecha']=$r[2];
                        preg_match("/([0-9]{4})\-([0-9]{2})\-([0-9]{2})/", $_POST['fecha'],$fecha);
                        $_POST['fecha']=$fecha[3]."/".$fecha[2]."/".$fecha[1];
                        /* $vigusu = vigencia_usuarios($_SESSION['cedulausu']); */
                        $vigencia = $fecha[1];
                        $vigusu = $fecha[1];
                        $_POST['vigencia']=$vigencia;
                    }

                    switch($_POST['tabgroup1'])
                    {
                        case 1:
                            $check1='checked';
                            break;
                        case 2:
                            $check2='checked';
                            break;
                        case 3:
                            $check3='checked';
                    }
                    ?>
                    <input name="codrec" id="codrec" type="hidden" value="<?php echo $_POST['codrec']?>" >
                    <input name="encontro" type="hidden" value="<?php echo $_POST['encontro']?>" >
                    <input name="codcatastral" type="hidden" value="<?php echo $_POST['codcatastral']?>" >
                    <input name="cobro_ambiental" type="hidden" value="<?php echo @ $_POST['cobro_ambiental']?>" >
                    <?php 
                    $_POST['concepto']="";
                    switch($_POST['tiporec'])
                    {
			            case 1:
                            
                            $sqlr="select *from tesoliquidapredial where tesoliquidapredial.idpredial=$_POST[idrecaudo] and  1=$_POST[tiporec]";
                            $_POST['encontro']="";
                            $res=mysqli_query($linkbd, $sqlr);
                            while ($row =mysqli_fetch_row($res)){
                                $_POST['codcatastral']=$row[1];		
                                $_POST['concepto']=$row[17].' Cod Catastral No '.$row[1];	
                                //		$_POST[modorec]=$row[24];
                                $_POST['valorecaudo']=$row[8];		
                                $_POST['totalc']=$row[8];	
                                $_POST['tercero']=$row[4];	
                                $_POST['ntercero']=buscatercero($row[4]);		
                                if ($_POST['ntercero']==''){
                                    $sqlr2="select *from tesopredios where cedulacatastral='".$row[1]."' ";
                                    $resc=mysqli_query($linkbd, $sqlr2);
                                    $rowc =mysqli_fetch_row($resc);
                                    $_POST['ntercero']=$rowc[6];
                                }	
                                $_POST['encontro']=1;
                            }
                            
                            $sqlr="select *from tesoreciboscaja where  id_recibos=$_POST[idcomp] ";
                            $res=mysqli_query($linkbd, $sqlr);
                            $row =mysqli_fetch_row($res); 
                            //$_POST[idcomp]=$row[0];
                            
                            $_POST['estadoc']=$row[9];
                            if ($row[9]=='N'){
                                $_POST['estado']="ANULADO";
                                $_POST['estadoc']='0';
                            }
                            else{
                                $_POST['estadoc']='1';
                                $_POST['estado']="ACTIVO";
                            }
                            $_POST['modorec']=$row[5];
                            $_POST['banco']=$row[7];
                            
                        break;
                        case 2:
                            $sqlr="select *from tesoindustria where tesoindustria.id_industria=$_POST[idrecaudo]  and 2=$_POST[tiporec]";
                            $_POST['encontro']="";
                            $res=mysqli_query($linkbd, $sqlr);
                            while ($row =mysqli_fetch_row($res)){
                                $_POST['concepto']="Liquidacion Industria y Comercio avisos y tableros - ".$row[3];	
                                $_POST['valorecaudo']=$row[6];		
                                $_POST['totalc']=$row[6];	
                                $_POST['tercero']=$row[5];	
                                $_POST['ntercero']=buscatercero($row[5]);	
                                //$_POST[modorec]=$row[13];
                                $_POST['encontro']=1;
                            }
                            $sqlr="select *from tesoreciboscaja where  id_recibos=$_POST[idcomp] ";
                            $res=mysqli_query($linkbd, $sqlr);
                            $row =mysqli_fetch_row($res); 
                            //$_POST[idcomp]=$row[0];
                            $_POST['estadoc']=$row[9];
                            if ($row[9]=='N'){
                                $_POST['estado']="ANULADO";
                                $_POST['estadoc']='0';
                            }
                            else{
                                $_POST['estadoc']='1';
                                $_POST['estado']="ACTIVO";
                            }
                            $_POST['modorec']=$row[5];
                            $_POST['banco']=$row[7];
                        break;
                        case 3:
                            $sqlr="select *from tesorecaudos where tesorecaudos.id_recaudo=$_POST[idrecaudo] and 3=$_POST[tiporec]";
                            $_POST['encontro']="";
                            $res=mysqli_query($linkbd, $sqlr);
                            while ($row =mysqli_fetch_row($res)){
                                $_POST['concepto']=$row[6];	
                                $_POST['valorecaudo']=$row[5];		
                                $_POST['totalc']=$row[5];	
                                $_POST['tercero']=$row[4];	
                                $_POST['ntercero']=buscatercero($row[4]);			
                                $_POST['encontro']=1;
                            }
                            $sqlr="select *from tesoreciboscaja where  id_recibos=$_POST[idcomp] ";
                            $res=mysqli_query($linkbd, $sqlr);
                            $row =mysqli_fetch_row($res);
                            $_POST['estadoc']=$row[9];
                            if ($row[9]=='N'){
                                $_POST['estado']="ANULADO";
                                $_POST['estadoc']='0';
                            }
                            else{
                                $_POST['estadoc']='1';
                                $_POST['estado']="ACTIVO";
                            }
                            $_POST['modorec']=$row[5];
                            $_POST['banco']=$row[7];
                        break;	
                    }
                        ?>
                        <table class="inicio" style="width:99.7%;">
                            <tr>
                                <td class="titulos" colspan="9">Reflejar Recibo de Caja Ppto</td>
                                <td class="cerrar" style="width:7%"><a href="teso-principal.php">Cerrar</a></td>
                            </tr>
                            <tr>
                                <td class="saludo1" style="width:2cm;">No Recibo:</td>
                                <td style="width:10%" > 
                                    <a href="#" onClick="atrasc()"><img src="imagenes/back.png" alt="anterior" align="absmiddle"></a>
                                    <input name="idcomp" type="text" size="5" value="<?php echo $_POST['idcomp']?>" onKeyUp="return tabular(event,this) "  onBlur="validar2()" style="width: 50%;">
                                    <input name="ncomp" type="hidden" value="<?php echo $_POST['ncomp']?>">
                                    <a href="#" onClick="adelante()"><img src="imagenes/next.png" alt="siguiente" align="absmiddle"></a> 
                                    <input type="hidden" value="a" name="atras" >
                                    <input type="hidden" value="s" name="siguiente" ><input type="hidden" value="<?php echo $_POST['maximo']?>" name="maximo">
                                </td>
                                <td class="saludo1" style="width:2.3cm;">Fecha:</td>
                                <td style="width:18%">
                                    <input name="fecha" type="text" value="<?php echo $_POST['fecha']?>" maxlength="10" size="10" readonly style="width: 45%;">  
                                    <?php 
                                        if($_POST['estado']=='ACTIVO'){
                                            echo "<input name='estado' type='text' value='ACTIVO' size='5' style='width:52%; background-color:#0CD02A; color:white; text-align:center;' readonly >";
                                        }
                                        else
                                        {
                                            echo "<input name='estado' type='text' value='ANULADO' size='5' style='width:40%; background-color:#FF0000; color:white; text-align:center;' readonly >";
                                        }
                                    ?>      
                                </td>
                                <td class="saludo1" style="width:2.5cm;">Vigencia:</td>
                                <td  style="width:12%">
                                    <input type="text" id="vigencia" name="vigencia" size="8" onKeyPress="javascript:return solonumeros(event)" onKeyUp="return tabular(event,this)"  value="<?php echo $_POST['vigencia']?>" onClick="document.getElementById('tipocta').focus();document.getElementById('tipocta').select();" readonly>   
                                    <input type="hidden" name="estado" value="<?php echo $_POST['estado'] ?>" size="5" readonly>  
                                    <input type="hidden" name="estadoc" value="<?php echo $_POST['estadoc'] ?>">
                                    <input type="hidden" name="cuotas" value="<?php echo $_POST['cuotas'] ?>" size="5" readonly>  
                                    <input type="hidden" name="tcuotas" value="<?php echo $_POST['tcuotas'] ?>">
                                </td>
        		                <td rowspan="6" colspan="<?php if($_POST['tiporec']=='1'){echo '1'; }else{echo '2';}?>" style="background:url(imagenes/fondo-2.png); background-repeat:no-repeat; background-position:right; background-size: 100% 100%;" ></td>
     		                </tr>
                            <tr>
                                <td class="saludo1" > Recaudo:</td>
                                <td> 
                                    <select name="tiporec" id="tiporec" onKeyUp="return tabular(event,this)" onChange="validar()" style="width:100%;">
                                        <?php
                                        if($_POST['tiporec']=='1'){
                                            echo'<option value="1">Predial</option>';
                                        }
                                        else
                                        if($_POST['tiporec']=='2'){
                                            echo'<option value="2">Industria y Comercio</option>';
                                        }
                                        else{
                                            echo'<option value="3">Otros Recuados</option>';
                                        }
                                        ?>
                                    </select>
        	  	                </td>
                                
                                <td class="saludo1">No Liquidaci&oacute;n:</td>
                                <td>
            		                <input type="text" id="idrecaudo" name="idrecaudo" value="<?php echo $_POST['idrecaudo']?>" style="width:100%" onKeyUp="return tabular(event,this)" onChange="validar()" readonly> 
	           	                </td>
                                <td class="saludo1">Recaudado en:</td>
                                <td> 
                                    <select name="modorec" id="modorec" onKeyUp="return tabular(event,this)" onChange="validar()" >
                                        <?php
                                            if($_POST['modorec']=='caja'){
                                                echo'<option value="caja">Caja</option>';
                                            }
                                            else{
                                                echo'<option value="banco">Banco</option>';
                                            }
                                        ?>
                                    </select>
                                </td>
	                        </tr>
                            <?php
                                if ($_POST['modorec']=='banco'){
                            ?>
                            <tr>
                                <td class='saludo1'>Cuenta :</td>
                                <td>
                                    <select id="banco" name="banco"  onChange="validar()" onKeyUp="return tabular(event,this)" style='width:100%;'>
                                        <?php
                                            $sqlr="select tesobancosctas.estado,tesobancosctas.cuenta,tesobancosctas.ncuentaban,tesobancosctas.tipo,terceros.razonsocial,tesobancosctas.tercero from tesobancosctas,terceros where tesobancosctas.tercero=terceros.cedulanit and tesobancosctas.estado='S' ";
                                            $res=mysqli_query($linkbd, $sqlr);
                                            while ($row =mysqli_fetch_row($res)){
                                                $i=$row[1];
                                                if($i==$_POST['banco']){
                                                    echo "<option value=$row[1] ";
                                                    echo "SELECTED";
                                                    $_POST['nbanco']=$row[4];
                                                    $_POST['ter']=$row[5];
                                                    $_POST['cb']=$row[2];
                                                    echo ">".$row[2]." - Cuenta ".$row[3]."</option>";	 	 
                                                }
                                            }	 	
                                        ?>
                                    </select>
                                    <input name="cb" type="hidden" value="<?php echo $_POST['cb']?>" >
                                    <input type="hidden" id="ter" name="ter" value="<?php echo $_POST['ter']?>" >
                                </td>
                                <td colspan="4"> 
                                    <input type="text" id="nbanco" name="nbanco" value="<?php echo $_POST['nbanco']?>"  style="width:100%" readonly>
                                </td>
                            </tr>
                            <?php
                            }
                            ?> 
                            <tr>
                                <td class="saludo1">Concepto:</td>
                                <td colspan="5">
                                    <input name="concepto" type="text" value="<?php echo $_POST['concepto'] ?>" onKeyUp="return tabular(event,this)" style='width:100%;' readonly>
                                </td>
                            </tr>
                            <tr>
                                <td class="saludo1">Documento: </td>
                                <td>
                                    <input name="tercero" type="text" value="<?php echo $_POST['tercero']?>" size="20" onKeyUp="return tabular(event,this)" readonly
                                    style="width:100%">
                                </td>
                                <td class="saludo1">Contribuyente:</td>
                                <td colspan="3">
                                    <input type="text" id="ntercero" name="ntercero" value="<?php echo $_POST['ntercero']?>" size="50" onKeyUp="return tabular(event,this) " readonly style="width:100%">
                                    <input type="hidden" id="cb" name="cb" value="<?php echo $_POST['cb']?>" >
                                    <input type="hidden" id="ct" name="ct" value="<?php echo $_POST['ct']?>" >
                                    <input type="hidden" value="1" name="oculto">
                                    <input type="hidden" value="<?php echo $_POST['trec']?>"  name="trec">
                                    <input type="hidden" value="0" name="agregadet">
                                </td>
                            </tr>
                            <tr>
                                <td class="saludo1">Valor:</td>
                                <td colspan="<?php if($_POST['tiporec']=='1'){echo '3'; }else{echo '1';}?>">
                                    <input type="text" id="valorecaudo" name="valorecaudo" value="<?php echo $_POST['valorecaudo']?>" size="30" onKeyUp="return tabular(event,this)" readonly style="width:100%">
                                </td>
                            </tr>
    	  	                <?php if ($_POST['modorec']!='banco'){echo"<tr style='height:20;'><tr>";}?>
		                </table>
    	                <div class="subpantallac7" style="height:40%">
                            <?php 
                            if($_POST['encontro']=='1')
                            {
		 	                    //echo "trr".$_POST[tiporec];
                                switch($_POST['tiporec'])
                                {
                                    case 1: //********PREDIAL
                                    {
                                        unset($_POST['dcoding']);
                                        unset($_POST['dncoding']);
                                        unset($_POST['dvalores']);
                                        $_POST['dcoding']= array();
                                        $_POST['dncoding']= array();
                                        $_POST['dvalores']= array();

                                        $_POST['trec']='PREDIAL';
                                        
                                        /* $sqlr="select *from tesoliquidapredial_det where tesoliquidapredial_det.idpredial=$_POST[idrecaudo] and estado ='S'  and 1=$_POST[tiporec]"; */

                                        $sqlr="select tb1.vigliquidada, tb1.totaliquidavig, tb2.codigocatastral from tesoliquidapredial_det AS tb1, tesoliquidapredial AS tb2 where tb1.idpredial = '".$_POST['idrecaudo']."' AND tb1.idpredial = tb2.idpredial and tb1.estado ='S' and 1=".$_POST['tiporec'];
                                        $res=mysqli_query($linkbd, $sqlr);
                                        //*******************CREANDO EL RECIBO DE CAJA DE PREDIAL ***********************
                                        while ($row =mysqli_fetch_row($res))
                                        {
                                            $tipopre=obtenerTipoPredio($row[2]);
                                            $vig=$row[0];
                                            if($vig==$vigusu){
                                                $codigoIngPredail = '';
                                                if($tipopre === 'rural'){
                                                    $codigoIngPredail = '03';
                                                }else{
                                                    $codigoIngPredail = '01';
                                                }

                                                $sqlr2="SELECT * FROM tesoingresos_predial WHERE codigo='".$codigoIngPredail."'";
                                                /* $res2=mysqli_query($linkbd, $sqlr2);
                                                $sqlr2="select * from tesoingresos where codigo='01'"; */
                                                $res2=mysqli_query($linkbd, $sqlr2);
                                                $row2=mysqli_fetch_row($res2); 
                                                $_POST['dcoding'][]=$row2[0];
                                                $_POST['dncoding'][]=$row2[1]." ".$vig;
                                                $_POST['dvalores'][]=$row[1];
                                            }
                                            else
                                            {
                                                $codigoIngPredail = '';
                                                if($tipopre === 'rural'){
                                                    $codigoIngPredail = '03';
                                                }else{
                                                    $codigoIngPredail = '01';
                                                }

                                                $sqlr2="SELECT * FROM tesoingresos_predial WHERE codigo='".$codigoIngPredail."'";
                                                $res2=mysqli_query($linkbd, $sqlr2);
                                                $row2 =mysqli_fetch_row($res2); 
                                                $_POST['dcoding'][]=$row2[0];
                                                $_POST['dncoding'][]=$row2[1]." ".$vig;
                                                $_POST['dvalores'][]=$row[1];
                                            }
                                        } 
                                        
					                }
                                    break;
                                    case 2: //***********INDUSTRIA Y COMERCIO
                                    {
                                        $_POST['dcoding']= array();
                                        $_POST['dncoding']= array();
                                        $_POST['dvalores']= array();
                                        $_POST['trec']='INDUSTRIA Y COMERCIO';

                                        $sqlr="select *from tesoindustria where tesoindustria.id_industria=$_POST[idrecaudo] and 2=$_POST[tiporec]";
                                        $res=mysqli_query($linkbd, $sqlr);
                                        while($row =mysqli_fetch_row($res))
                                        {
                                            $sqlr2="select * from tesoingresos where codigo='02'";
                                            $res2=mysqli_query($linkbd, $sqlr2);
                                            $row2 =mysqli_fetch_row($res2);
                                            $_POST['dcoding'][]=$row2[0];
                                            $_POST['dncoding'][]=$row2[1];
                                            $_POST['dvalores'][]=$row[6];
                                        }
					                }
                                    break;
                                    case 3: ///*****************otros recaudos *******************
                                    {
                                        $_POST['trec']='OTROS RECAUDOS';
                                        $sqlr="Select *from tesoreciboscaja_det where tesoreciboscaja_det.id_recibos=$_POST[idcomp]";
                                        $_POST['dcoding']= array();
                                        $_POST['dncoding']= array(); 
                                        $_POST['dvalores']= array(); 	
                                        $res=mysqli_query($linkbd, $sqlr);
                                        while($row =mysqli_fetch_row($res))
                                        {
                                            $_POST['dcoding'][]=$row[2];
                                            $sqlr2="select nombre from tesoingresos where  codigo='".$row[2]."'";
                                            $res2=mysqli_query($linkbd, $sqlr2);
                                            $row2 =mysqli_fetch_row($res2);
                                            $_POST['dncoding'][]=$row2[0];
                                            $_POST['dvalores'][]=$row[3];
                                        }
					                }break;
                                }
                            }
                            ?>
                            <table class="inicio">
                                <tr>
                                    <td colspan="4" class="titulos">Detalle Recibo de Caja</td>
                                </tr>                  
                                <tr>
                                    <td class="titulos2">Codigo</td><td class="titulos2">Ingreso</td><td class="titulos2">Valor</td>
                                </tr>
                                <?php 
                                if ($_POST['elimina']!='')
                                {
                                    $posi=$_POST['elimina'];
                                    unset($_POST['dcoding'][$posi]);
                                    unset($_POST['dncoding'][$posi]);
                                    unset($_POST['dvalores'][$posi]);
                                    $_POST['dcoding']= array_values($_POST['dcoding']);
                                    $_POST['dncoding']= array_values($_POST['dncoding']);
                                    $_POST['dvalores']= array_values($_POST['dvalores']);
                                }
                                if($_POST['agregadet']=='1')
                                {
                                    $_POST['dcoding'][]=$_POST['codingreso'];
                                    $_POST['dncoding'][]=$_POST['ningreso'];
                                    $_POST['dvalores'][]=$_POST['valor'];
                                    $_POST['agregadet']=0;
                                    echo"
                                    <script>
                                        document.form2.codingreso.value='';
                                        document.form2.valor.value='';
                                        document.form2.ningreso.value='';
                                        document.form2.codingreso.select();
                                        document.form2.codingreso.focus();
                                    </script>";
                                }
                                $_POST['totalc']=0;
                                $iter='saludo1a';
                                $iter2='saludo2';
                                $most = 'dvalores';
                                
                                for ($x=0;$x<count($_POST['dcoding']);$x++)
                                {
                                    echo "<tr class='$iter' >
                                        <td style='width:10%;'>
                                            <input type='hidden' name='dcoding[]' value='".$_POST['dcoding'][$x]."' size='4'>".$_POST['dcoding'][$x]."
                                        </td>
                                        <td>
                                            <input type='hidden' name='dncoding[]' value='".$_POST['dncoding'][$x]."' size='90'>".$_POST['dncoding'][$x]."
                                        </td>
                                        <td align='right' style='width:20%;'>
                                            <input type='hidden' name='dvalores[]' value='".$_POST['dvalores'][$x]."' size='15'>$ ".number_format($_POST[$most][$x],2,',','.')."
                                        </td>
                                    </tr>";
                                    $_POST['totalc']=$_POST['totalc']+$_POST['dvalores'][$x];
                                    $_POST['totalcf']=number_format($_POST['totalc'],2);
                                    $aux=$iter;
                                    $iter=$iter2;
                                    $iter2=$aux;
                                }
                                $resultado = convertir($_POST['totalc']);
                                $_POST['letras']=$resultado." PESOS M/CTE";
                                echo "<tr class='$iter'>
                                    <td></td>
                                    <td align='right'>Total</td>
                                    <td align='right'>
                                        <input name='totalcf' type='hidden' value='$_POST[totalcf]'>
                                        <input name='totalc' type='hidden' value='$_POST[totalc]'>$ ".number_format($_POST['totalc'],2,',','.')."
                                    </td>
                                </tr>
                                <tr class='titulos2'>
                                    <td>Son:</td><td colspan='5' >
                                        <input name='letras' type='hidden' value='$_POST[letras]' size='90'>".$_POST['letras']."
                                    </td>
                                </tr>";
                                ?> 
			                </table>
		                </div>
		                <?php
                        if($_POST['oculto']=='2')
                        {
                            //************VALIDAR SI YA FUE GUARDADO ************************
                            switch($_POST['tiporec']) 
                            {
                                case 1://***** PREDIAL *****************************************
                                {
                                    $sql="select codigocatastral from tesoliquidapredial WHERE idpredial=$_POST[idrecaudo] and estado !='N'";
                                    $resul=mysqli_query($linkbd, $sql);
                                    $rowcod=mysqli_fetch_row($resul);
                                    $tipopre=obtenerTipoPredio($rowcod[0]);
                                    
                                    //************ insercion de cabecera recaudos ************
                                    echo "<table class='inicio'><tr><td class='saludo1'><center>Se ha refñejado a presupuesto el Recibo de Caja con Exito <img src='imagenes/confirm.png'><script></script></center></td></tr></table>";
                                    
                                    echo"
                                    <script>
                                        document.form2.numero.value='';
                                        document.form2.valor.value=0;
                                    </script>";
                                    $query = "DELETE FROM pptorecibocajappto WHERE idrecibo=$_POST[idcomp]";
                                    mysqli_query($linkbd, $query);

                                    $sqlrs="select * from tesoliquidapredial_det where tesoliquidapredial_det.idpredial=$_POST[idrecaudo] and estado ='S'  and 1=$_POST[tiporec]";
                                    $res = mysqli_query($linkbd, $sqlrs);
                                    while ($row = mysqli_fetch_row($res)) 
                                    {
                                        $vig=$row[1];
                                        $vlrdesc=$row[10];

                                        $sqlrDesc = "SELECT descuentointpredial, descuentointbomberil, descuentointambiental, val_alumbrado FROM tesoliquidapredial_desc WHERE id_predial = '$_POST[idrecaudo]' AND vigencia = '$vig'";
                                        $respDesc = mysqli_query($linkbd, $sqlrDesc);
                                        $rowDesc = mysqli_fetch_row($respDesc);
                                        
                                        if($vig==$vigusu) //*************VIGENCIA ACTUAL *****************
                                        {
                                            $codigoIngPredail = '';
                                            if($tipopre === 'rural'){
                                                $codigoIngPredail = '03';
                                            }else{
                                                $codigoIngPredail = '01';
                                            }

                                            $sqlr2="SELECT * FROM tesoingresos_predial_det WHERE codigo='".$codigoIngPredail."' AND vigencia=(SELECT MAX(vigencia) FROM tesoingresos_predial_det WHERE codigo = '".$codigoIngPredail."') AND cuentapres != '' ORDER BY concepto ASC";
                                            $res2=mysqli_query($linkbd, $sqlr2);
                                            //******   ES LA CUENTA CAJA O BANCO
                                            while($rowi =mysqli_fetch_row($res2))
                                            {
                                                switch($rowi[2])
                                                {
                                                    case '20': case '21': //***  VALOR PREDIAL
                                                    {
                                                        $valPredial = round($row[4] - $vlrdesc - $rowDesc[0],0);
                                                                                                              
                                                        if($valPredial > 0)
                                                        {
                                                            $sqlr = "INSERT INTO pptorecibocajappto (cuenta, idrecibo, valor, vigencia, tipo, ingreso, fuente, seccion_presupuestal, medio_pago, vigencia_gasto) VALUES('$rowi[6]', $_POST[idcomp], $valPredial, '$_POST[vigencia]', 'P', '$codigoIngPredail', '$rowi[10]', '16', 'CSF', '1')";
                                                            mysqli_query($linkbd, $sqlr);
                                                        }
                                                    }
                                                    break;
                                                    case '24': case '25': //*** SOBRETASA AMBIENTAL */
                                                    {
                                                        $valSobreTasaAmb = $row[8] - $rowDesc[2];
                                                        
                                                        if($valSobreTasaAmb > 0)
                                                        {
                                                            $sqlr = "INSERT INTO pptorecibocajappto (cuenta, idrecibo, valor, vigencia, tipo, ingreso, fuente, seccion_presupuestal, medio_pago, vigencia_gasto) VALUES('$rowi[6]', $_POST[idcomp], $valSobreTasaAmb, '$_POST[vigencia]', 'P', '$codigoIngPredail', '$rowi[10]', '16', 'CSF', '1')";
                                                            mysqli_query($linkbd, $sqlr);
                                                        }
                                                    }
                                                    break;  
                                                    case '26': case '27': //** SOBRETASA BOMBERIL */
                                                    {
                                                        $valSobreTasaBom = $row[6] - $rowDesc[1];

                                                        if($valSobreTasaBom > 0)
                                                        {
                                                            $sqlr = "INSERT INTO pptorecibocajappto (cuenta, idrecibo, valor, vigencia, tipo, ingreso, fuente, seccion_presupuestal, medio_pago, vigencia_gasto) VALUES('$rowi[6]', $_POST[idcomp], $valSobreTasaBom, '$_POST[vigencia]', 'P', '$codigoIngPredail', '$rowi[10]', '16', 'CSF', '1')";
                                                            mysqli_query($linkbd, $sqlr);			
                                                        }
                                                    }
                                                    break;
                                                    case '28': //** INTERESES PREDIAL */
                                                    {
                                                        $valIntPredial = $row[5];
                                                        
                                                        if($valIntPredial > 0){
                                                            $sqlr = "INSERT INTO pptorecibocajappto (cuenta, idrecibo, valor, vigencia, tipo, ingreso, fuente, seccion_presupuestal, medio_pago, vigencia_gasto) VALUES('$rowi[6]', $_POST[idcomp], $valIntPredial, '$_POST[vigencia]', 'P', '$codigoIngPredail', '$rowi[10]', '16', 'CSF', '1')";
                                                            mysqli_query($linkbd, $sqlr);
                                                        }
                                                    }
                                                    break;
                                                    case '29': //** INTERESES SOBRETASA AMBIENTAL */
                                                    {
                                                        $valIntSobreTasaAmb = $row[9];

                                                        if($valIntSobreTasaAmb > 0)
                                                        {
                                                            $sqlr = "INSERT INTO pptorecibocajappto (cuenta, idrecibo, valor, vigencia, tipo, ingreso, fuente, seccion_presupuestal, medio_pago, vigencia_gasto) VALUES('$rowi[6]', $_POST[idcomp], $valIntSobreTasaAmb, '$_POST[vigencia]', 'P', '$codigoIngPredail', '$rowi[10]', '16', 'CSF', '1')";
                                                            mysqli_query($linkbd, $sqlr);
                                                        }
                                                    }
                                                    break;
                                                    case '30': //** INTERESES SOBRETASA BOMBERIL */
                                                    {
                                                        $valIntSobreTasabom = $row[7];
                                                        
                                                        if($valIntSobreTasabom > 0)
                                                        {
                                                            $sqlr = "INSERT INTO pptorecibocajappto (cuenta, idrecibo, valor, vigencia, tipo, ingreso, fuente, seccion_presupuestal, medio_pago, vigencia_gasto) VALUES('$rowi[6]', $_POST[idcomp], $valIntSobreTasabom, '$_POST[vigencia]', 'P', '$codigoIngPredail', '$rowi[10]', '16', 'CSF', '1')";
                                                            mysqli_query($linkbd, $sqlr);
                                                        }
                                                    }
                                                    break;
                                                    case '34': //** ALUMBRADO PUBLICO */
                                                    {
                                                        $valAlumb = $rowDesc[3];
                                                    
                                                        if($valAlumb > 0)
                                                        {			
                                                        	$sqlr = "INSERT INTO pptorecibocajappto (cuenta, idrecibo, valor, vigencia, tipo, ingreso, fuente, seccion_presupuestal, medio_pago, vigencia_gasto) VALUES('$rowi[6]', $_POST[idcomp], $valAlumb, '$_POST[vigencia]', 'P', '$codigoIngPredail', '$rowi[10]', '16', 'CSF', '1')";
                                                            mysqli_query($linkbd, $sqlr);
                                                        }
                                                    }

                                                    break; 
                                                    
                                                } 
                                            }
                                        
                                        }
                                        else  ///***********OTRAS VIGENCIAS ***********
                                        {

                                            $codigoIngPredail = '';
                                            if($tipopre === 'rural'){
                                                $codigoIngPredail = '04';
                                            }else{
                                                $codigoIngPredail = '02';
                                            }

                                            $sqlr2="SELECT * FROM tesoingresos_predial_det WHERE codigo='".$codigoIngPredail."' AND vigencia=(SELECT MAX(vigencia) FROM tesoingresos_predial_det WHERE codigo = '".$codigoIngPredail."') AND cuentapres != '' ORDER BY concepto ASC";
                                            $res2=mysqli_query($linkbd, $sqlr2);
                                            //******   ES LA CUENTA CAJA O BANCO
                                            while($rowi =mysqli_fetch_row($res2))
                                            {
                                                switch($rowi[2])
                                                {
                                                    case '22': case '23': //** PREDIAL
                                                    {
                                                        $valPredial = round($row[4] - $vlrdesc - $rowDesc[0],0);
                                                                                                              
                                                        if($valPredial > 0)
                                                        {
                                                            $sqlr = "INSERT INTO pptorecibocajappto (cuenta, idrecibo, valor, vigencia, tipo, ingreso, fuente, seccion_presupuestal, medio_pago, vigencia_gasto) VALUES('$rowi[6]', $_POST[idcomp], $valPredial, '$_POST[vigencia]', 'P', '$codigoIngPredail', '$rowi[10]', '16', 'CSF', '1')";
                                                            mysqli_query($linkbd, $sqlr);
                                                        }
                                                    }
                                                    break;  
                                                    case '24': case '25': //** SOBRETASA AMBIENTAL */
                                                    {
                                                        $valSobreTasaAmb = $row[8] - $rowDesc[2];
                                                        
                                                        if($valSobreTasaAmb > 0)
                                                        {
                                                            $sqlr = "INSERT INTO pptorecibocajappto (cuenta, idrecibo, valor, vigencia, tipo, ingreso, fuente, seccion_presupuestal, medio_pago, vigencia_gasto) VALUES('$rowi[6]', $_POST[idcomp], $valSobreTasaAmb, '$_POST[vigencia]', 'P', '$codigoIngPredail', '$rowi[10]', '16', 'CSF', '1')";
                                                            mysqli_query($linkbd, $sqlr);
                                                        }
                                                    }
                                                    break;  
                                                    case '26': case '27': //** SOBRETASA BOMBERIL */
                                                    {
                                                        $valSobreTasaBom = $row[6] - $rowDesc[1];

                                                        if($valSobreTasaBom > 0)
                                                        {
                                                            $sqlr = "INSERT INTO pptorecibocajappto (cuenta, idrecibo, valor, vigencia, tipo, ingreso, fuente, seccion_presupuestal, medio_pago, vigencia_gasto) VALUES('$rowi[6]', $_POST[idcomp], $valSobreTasaBom, '$_POST[vigencia]', 'P', '$codigoIngPredail', '$rowi[10]', '16', 'CSF', '1')";
                                                            mysqli_query($linkbd, $sqlr);
                                                        }
                                                    }
                                                    break;  
                                                    case '28': //** INTERESES PREDIAL */
                                                    {
                                                        $valIntPredial = $row[5];
                                                        
                                                        if($valIntPredial > 0){
                                                            $sqlr = "INSERT INTO pptorecibocajappto (cuenta, idrecibo, valor, vigencia, tipo, ingreso, fuente, seccion_presupuestal, medio_pago, vigencia_gasto) VALUES('$rowi[6]', $_POST[idcomp], $valIntPredial, '$_POST[vigencia]', 'P', '$codigoIngPredail', '$rowi[10]', '16', 'CSF', '1')";
                                                            mysqli_query($linkbd, $sqlr);
                                                        }
                                                    }
                                                    break;  
                                                    case '29': //** INTERESES SOBRETASA AMBIENTAL */
                                                    {
                                                        $valIntSobreTasaAmb = $row[9];

                                                        if($valIntSobreTasaAmb > 0)
                                                        {
                                                            $sqlr = "INSERT INTO pptorecibocajappto (cuenta, idrecibo, valor, vigencia, tipo, ingreso, fuente, seccion_presupuestal, medio_pago, vigencia_gasto) VALUES('$rowi[6]', $_POST[idcomp], $valIntSobreTasaAmb, '$_POST[vigencia]', 'P', '$codigoIngPredail', '$rowi[10]', '16', 'CSF', '1')";
                                                            mysqli_query($linkbd, $sqlr);
                                                        }
                                                    }
                                                    break;
                                                    case '30': //** INTERESES SOBRETASA BOMBERIL */ 
                                                    {
                                                        $valIntSobreTasabom = $row[7];
                                                        
                                                        if($valIntSobreTasabom > 0)
                                                        {
                                                            $sqlr = "INSERT INTO pptorecibocajappto (cuenta, idrecibo, valor, vigencia, tipo, ingreso, fuente, seccion_presupuestal, medio_pago, vigencia_gasto) VALUES('$rowi[6]', $_POST[idcomp], $valIntSobreTasabom, '$_POST[vigencia]', 'P', '$codigoIngPredail', '$rowi[10]', '16', 'CSF', '1')";
                                                            mysqli_query($linkbd, $sqlr);
                                                        }
                                                    }
                                                    break;  
                                                    case '34': //** ALUMBRADO PUBLICO */
                                                    {
                                                        $valAlumb = $rowDesc[3];
                                                    
                                                        if($valAlumb > 0)
                                                        {			
                                                        	$sqlr = "INSERT INTO pptorecibocajappto (cuenta, idrecibo, valor, vigencia, tipo, ingreso, fuente, seccion_presupuestal, medio_pago, vigencia_gasto) VALUES('$rowi[6]', $_POST[idcomp], $valAlumb, '$_POST[vigencia]', 'P', '$codigoIngPredail', '$rowi[10]', '16', 'CSF', '1')";
                                                            mysqli_query($linkbd, $sqlr);
                                                        }
                                                    }
                                                    break;
                                                }
                                            }
                                        }
                                    }
                            
                                }
                                break; 
                                case 3: //**************OTROS RECAUDOS
                                    $sqlr="delete from pptorecibocajappto where idrecibo=$_POST[idcomp]";
                                    mysqli_query($linkbd, $sqlr);
                                    
                                    for($x=0;$x<count($_POST['dcoding']);$x++)
                                    {
                                        //***** BUSQUEDA INGRESO ********
                                        $sqlri="SELECT * FROM tesoingresos_det WHERE codigo='".$_POST['dcoding'][$x]."' AND vigencia=(SELECT MAX(vigencia) FROM tesoingresos_det WHERE codigo='".$_POST['dcoding'][$x]."') AND cuentapres != ''";
                                        $resi=mysqli_query($linkbd, $sqlri); 
                                        while($rowi=mysqli_fetch_row($resi))
                                        {
                                            if($rowi[6]!="")
                                            {
                                                //**** busqueda cuenta presupuestal*****
                                                $porce=$rowi[5];
                                                $valor=round($_POST['dvalores'][$x]*($porce/100),2);
                                                
                                                if($valor > 0){
                                                    $sqlrFuente = "SELECT fuente FROM tesoingresos_fuentes WHERE codigo = '".$_POST['dcoding'][$x]."' AND estado = 'S'";
                                                    $respFuente = mysqli_query($linkbd, $sqlrFuente);
                                                    $rowFuente = mysqli_fetch_row($respFuente);
                                                    $sqlr = "INSERT INTO pptorecibocajappto (cuenta, idrecibo, valor, vigencia, tipo, ingreso, fuente, productoservicio, seccion_presupuestal, medio_pago, vigencia_gasto) VALUES('$rowi[6]', $_POST[idcomp], $valor, '$_POST[vigencia]', 'O', '".$_POST['dcoding'][$x]."', '$rowFuente[0]', '$rowi[7]', '16', 'CSF', '1')";
                                                    mysqli_query($linkbd, $sqlr);
                                                }
                                            }
                                        }
                                    }	
                                    echo "<table class='inicio'><tr><td class='saludo1'><center>Se ha Actualizado el Recibo de caja con Exito<img src='imagenes/confirm.png'><script></script></center></td></tr></table>";
                                break;
                            } //*****fin del switch
                        }//**fin del oculto 
                        ?>	
                    </form>
                </td>
            </tr>
        </table>
    </body>
</html>