<?php
header("Cache-control: no-cache, no-store, must-revalidate");
header("Content-Type: text/html;charset=utf8");
require 'comun.inc';
require 'funciones.inc';
session_start();
cargarcodigopag(@$_GET['codpag'], @$_SESSION['nivel']);
header('Cache-control: private');
date_default_timezone_set('America/Bogota')
?>
<!DOCTYPE >
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es">

<head>
    <meta http-equiv="Content-Type" content="text/html" charset="uft8" />
    <meta http-equiv="X-UA-Compatible" content="IE=9" />
    <title>:: IDEAL 10 - Servicios Públicos</title>
    <link href="css/css2.css" rel="stylesheet" type="text/css" />
    <link href="css/css3.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="css/programas.js"></script>
    <script type="text/javascript" src="css/calendario.js"></script>

    <?php titlepag(); ?>
</head>

<body>
    <IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
    <span id="todastablas2"></span>
    <table>
        <tr>
            <script>
                barra_imagenes("serv");
            </script><?php cuadro_titulos(); ?>
        </tr>
        <tr><?php menu_desplegable("serv"); ?></tr>
        <tr>
            <td colspan="3" class="cinta">
                <a class="mgbt"><img src="imagenes/add2.png" /></a>
                <a class="mgbt"><img src="imagenes/guardad.png" title="Guardar" /></a>
                <a class="mgbt"><img src="imagenes/buscad.png" /></a><a href="#" class="mgbt" onClick="mypop=window.open('serv-principal.php','','');mypop.focus();"><img src="imagenes/nv.png" title="Nueva Ventana"></a>
                <a href="serv-menuInformes"><img src="imagenes/iratras.png" class="mgbt" alt="Atrás"></a>
            </td>
        </tr>
    </table>
    <form name="form2" method="post" action="">
        <table class="inicio">
            <tr>
                <td class="titulos" colspan="2">.: Parametros Facturación </td>
                <td class="cerrar" style="width:7%;"><a href="serv-principal.php">Cerrar</a></td>
            </tr>

            <tr>
                <td>
                    <ol id="lista2">
                        <li onClick="location.href='serv-reporteIGAC-acueducto'" style="cursor:pointer;">Facturación acueducto -IGAC</li>
                        <li onClick="location.href='serv-reporteIGAC-alcantarillado'" style="cursor:pointer;">Facturación alcantarillado - IGAC</li>
                        <li onClick="location.href='serv-reporteIGAC-aseo'" style="cursor:pointer;">Facturación aseo - IGAC</li>
                    </ol>
                </td>
            </tr>
        </table>
    </form>
</body>

</html>