<?php
	require_once("tcpdf/tcpdf_include.php");
	require('comun.inc');
	require "funciones.inc";
    require 'funcionesSP.inc.php';
	date_default_timezone_set("America/Bogota");
	session_start();

    class Articulo{
        private $linkbd;
        public function __construct() {
            $this->linkbd = conectar_v7();
            $this->linkbd->set_charset("utf8");
        }
        public function selectMovimientos($codigo,$bodega,$fechaInicial,$fechaFinal){
            $dateRange ="";
            if($fechaInicial !="" && $fechaFinal !=""){
                $arrFechaInicio = explode("/",$fechaInicial);
                $arrFechaFinal = explode("/",$fechaFinal);

                $dateInicio = date_format(date_create($arrFechaInicio[2]."-".$arrFechaInicio[1]."-".$arrFechaInicio[0]),"Y-m-d");
                $dateFinal = date_format(date_create($arrFechaFinal[2]."-".$arrFechaFinal[1]."-".$arrFechaFinal[0]),"Y-m-d");
                $dateRange = " AND i.fecha >= '$dateInicio' AND i.fecha <= '$dateFinal'";
            }
            $arrData = [];
            $articulos = mysqli_fetch_all(mysqli_query($this->linkbd,
                "SELECT CONCAT(grupoinven,codigo) as codigo,
                nombre FROM almarticulos WHERE CONCAT(grupoinven,codigo) like '$codigo%'"),MYSQLI_ASSOC);
            if(count($articulos)>0){
                $total = count($articulos);
                for ($i=0; $i < $total ; $i++) {
                    $cantidad = 0;
                    $valorTotal = 0;
                    $idArt = $articulos[$i]['codigo'];
                    $movimientos = mysqli_fetch_all(mysqli_query(
                        $this->linkbd,
                        "SELECT DATE_FORMAT(i.fecha,'%d/%m/%Y') as fecha,
                        DATE(i.fecha) as fechaorden ,
                        d.codart,
                        d.codigo as documento,
                        d.valortotal as total,
                        d.valorunit as valor,
                        d.cantidad_entrada as entrada,
                        d.cantidad_salida as salida,
                        d.unidad,
                        CONCAT(d.tipomov,d.tiporeg) as movimiento,
                        d.tipomov,
                        c.nombre as centro,
                        t.nombre as mov_nom
                        FROM almginventario_det d
                        INNER JOIN almginventario i ON i.consec = d.codigo AND i.estado = 'S' AND CONCAT(d.tipomov,d.tiporeg) = CONCAT(i.tipomov,i.tiporeg)
                        INNER JOIN centrocosto c ON c.id_cc = d.cc
                        INNER JOIN almtipomov t ON CONCAT(d.tipomov,d.tiporeg) = CONCAT(t.tipom,t.codigo)
                        WHERE d.tipomov != 3 AND d.tipomov != 4 AND d.bodega='$bodega' AND d.codart = $idArt $dateRange
                        ORDER BY fechaorden ASC;"),MYSQLI_ASSOC
                    );
                    $totalMov = count($movimientos);
                    if($totalMov > 0){
                        $arrComprobantes = array(
                            "101"=>50,
                            "104"=>53,
                            "106"=>54,
                            "107"=>51,
                            "201"=>60,
                            "202"=>61,
                            "203"=>62,
                            "204"=>63,
                            "205"=>64,
                            "206"=>65
                        );
                        for ($j=0; $j < $totalMov ; $j++) {
                            $cantidad += $movimientos[$j]['entrada'];
                            if($movimientos[$j]['salida'] > 0)$cantidad-=$movimientos[$j]['salida'];
                            $valorTotal = $cantidad * $movimientos[$j]['valor'];
                            $tipoComprobante = $arrComprobantes[$movimientos[$j]['movimiento']];
                            $validCant = $movimientos[$j]['entrada'] > 0 ? $movimientos[$j]['entrada'] : $movimientos[$j]['salida'];
                            $movimientos[$j]['cantidad_saldo'] = $cantidad;
                            $movimientos[$j]['total_saldo'] = $valorTotal;
                            $movimientos[$j]['tipo'] = $tipoComprobante;
                            $movimientos[$j]['estado_contable'] = $this->validContabilidad($movimientos[$j]['codart'],$tipoComprobante,$validCant,$movimientos[$j]['documento']);
                            $movimientos[$j]['total_entrada'] = $movimientos[$j]['valor'] * $movimientos[$j]['entrada'];
                            $movimientos[$j]['total_salida'] = $movimientos[$j]['valor'] * $movimientos[$j]['salida'];
                        }
                        $data['movimientos'] = $movimientos;
                        $data['nombre'] = $articulos[$i]['codigo']." - ".$articulos[$i]['nombre'];
                        $data['valor_total'] = $valorTotal;
                        $data['cantidad_total'] = $cantidad;
                        $data['bodega'] = mysqli_query($this->linkbd,"SELECT nombre FROM almbodegas WHERE id_cc = '$bodega'")->fetch_assoc()['nombre'];
                        array_push($arrData,$data);
                    }
                }
            }
            return $arrData;
        }
        public function validContabilidad($codigo,$comprobante,$cantidad,$documento){
            $sql="SELECT cantarticulo FROM comprobante_det
            WHERE numacti = '$codigo' AND tipo_comp = '$comprobante' AND numerotipo = '$documento'";
            $request = mysqli_query($this->linkbd,$sql)->fetch_assoc()['cantarticulo'];
            $result = 0;
            if($request > 0){
                $result = $request == $cantidad ? 1 : 2;
            }else{
                $result = 3;
            }
            return $result;
        }
    }
	class MYPDF extends TCPDF
	{
		public function Header(){

			$linkbd = conectar_v7();
			$linkbd -> set_charset("utf8");
			$sqlr="select *from configbasica where estado='S' ";
			//echo $sqlr;
			$res=mysqli_query($linkbd, $sqlr);
			while($row=mysqli_fetch_row($res))
			{
				$nit=$row[0];
				$rs=$row[1];

			}
			//Parte Izquierda
			$this->Image('imagenes/escudo.jpg',  13, 13, 20, 20, 'JPG', '', 'T', true, 300, '', false, false, 0, false, false, false);// Logo
			$this->SetFont('helvetica','B',8);
			$this->SetY(10);
			$this->RoundedRect(10, 10, 277, 25, 1,'');
			$this->Cell(0.1);
			$this->Cell(26,25,'','R',0,'L');
			$this->SetY(8);
			$this->SetX(80);
			$this->SetFont('helvetica','B',9);
			$this->Cell(160,15,strtoupper("$rs"),0,0,'C');
			$this->SetFont('helvetica','B',7);
			$this->SetY(12);
			$this->SetX(80);
			$this->Cell(160,15,'NIT: '.$nit,0,0,'C');
			//*****************************************************************************************************************************
			$this->SetFont('helvetica','B',9);
			$this->SetY(23);
			$this->SetX(36);
            $this->Cell(251,12,"REPORTE KARDEX - ".DATA[0]['bodega'],'T',0,'C');


            $this->SetFont('helvetica','B',7);
			$this->SetY(10);
			$this->SetX(257);
			$this->Cell(30,7," FECHA: ". date("d/m/Y"),"L",0,'L');
			$this->SetY(17);
			$this->SetX(257);
			$this->Cell(35,6," ","L",0,'L');

			//**********************************************************
			$this->SetFont('times','B',10);
			$this->ln(12);
			//**********************************************************
		}
        public function ColoredTable($data) {
            // Colors, line width and bold font

            // Header
            $w = array(80, 70, 67.5,59.8);
            $wh = array(20,17.5,16.8,15);
            $totalData = count($data);
            for ($i=0; $i < $totalData; $i++) {
                $this->SetFillColor(222, 222, 222);
                $this->SetTextColor(000);
                //$this->SetDrawColor(128, 0, 0);
                $this->SetLineWidth(0.3);
                $this->SetFont('helvetica','B',7);
                $max = $this->getNumLines("Articulo - ".$data[$i]['nombre'], 30);
                $alturas = $max * 3;
                //Main header
                $this->MultiCell($w[0],$alturas,"Articulo - ".$data[$i]['nombre'],"LRTB",'L',true,0,'','',true,0,false,true,0,'M',true);
                $this->MultiCell($w[1],$alturas,"Entradas","LRTB",'C',true,0,'','',true,0,false,true,0,'M',true);
                $this->MultiCell($w[2],$alturas,"Salidas","LRTB",'C',true,0,'','',true,0,false,true,0,'M',true);
                $this->MultiCell($w[3],$alturas,"Saldo","LRTB",'C',true,0,'','',true,0,false,true,0,'M',true);
                $this->Ln();
                $this->setX(10);
                $this->Cell($wh[0], 7, "Fecha", 'LRTB', 0, 'C', 1);
                $this->Cell($wh[0], 7, "Documento", 'LRTB', 0, 'C', 1);
                $this->Cell($wh[0], 7, "Movimiento", 'LRTB', 0, 'C', 1);
                $this->Cell($wh[0], 7, "Centro costo", 'LRTB', 0, 'C', 1);
                $this->Cell($wh[1], 7, "Unidad", 'LRTB', 0, 'C', 1);
                $this->Cell($wh[1], 7, "Valor", 'LRTB', 0, 'C', 1);
                $this->Cell($wh[1], 7, "Cantidad", 'LRTB', 0, 'C', 1);
                $this->Cell($wh[1], 7, "Total", 'LRTB', 0, 'C', 1);
                $this->Cell($wh[2], 7, "Unidad", 'LRTB', 0, 'C', 1);
                $this->Cell($wh[2], 7, "Valor", 'LRTB', 0, 'C', 1);
                $this->Cell($wh[2], 7, "Cantidad", 'LRTB', 0, 'C', 1);
                $this->Cell($wh[2], 7, "Total", 'LRTB', 0, 'C', 1);
                $this->Cell($wh[3], 7, "Unidad", 'LRTB', 0, 'C', 1);
                $this->Cell($wh[3], 7, "Valor", 'LRTB', 0, 'C', 1);
                $this->Cell($wh[3], 7, "Cantidad", 'LRTB', 0, 'C', 1);
                $this->Cell($wh[3], 7, "Total", 'LRTB', 0, 'C', 1);
                $this->Ln();
                $arrMov = $data[$i]['movimientos'];
                $totalArt = count($arrMov);
                $this->SetFont('helvetica','',6);
                // Color and font restoration
                $this->SetFillColor(245,245,245);
                $this->SetTextColor(0);
                $this->SetFont('');
                // Data
                $fill = 1;
                $total = 0;
                for ($j=0; $j < $totalArt ; $j++) {

                    $centro = $this->getNumLines($arrMov[$j]['centro'], 30);
                    $unidad = $this->getNumLines($arrMov[$j]['unidad'], 30);
                    $max = max($centro,$unidad);
                    $alturas = $max * 3;
                    if($arrMov[$j]['estado_contable'] == 2){
                        $this->SetFillColor(75,0,130);
                        $this->SetTextColor(255);
                        $this->SetFont('helvetica','B',7);
                    }else if( $arrMov[$j]['estado_contable'] == 3){
                        $this->SetFillColor(255,0,0);
                        $this->SetTextColor(255,255,255);
                        $this->SetFont('helvetica','B',7);
                    }else{
                        $this->SetFillColor(245,245,245);
                        $this->SetTextColor(000);
                        $this->SetFont('helvetica','',7);
                    }
                    $this->Cell($wh[0], $alturas, $arrMov[$j]['fecha'], 'LRTB', 0, 'C', $fill);
                    $this->Cell($wh[0], $alturas, $arrMov[$j]['documento'], 'LRTB', 0, 'C', $fill);
                    $this->Cell($wh[0], $alturas, $arrMov[$j]['movimiento'], 'LRTB', 0, 'C', $fill);
                    $this->MultiCell($wh[0],$alturas,$arrMov[$j]['centro'],"LRTB",'C',$fill,0,'','',true,0,false,true,0,'M',true);
                    $this->MultiCell($wh[1],$alturas,$arrMov[$j]['unidad'],"LRTB",'C',$fill,0,'','',true,0,false,true,0,'M',true);
                    $this->Cell($wh[1], $alturas, "$".number_format($arrMov[$j]['valor']), 'LRTB', 0, 'C', $fill);
                    $this->Cell($wh[1], $alturas, $arrMov[$j]['entrada'], 'LRTB', 0, 'C', $fill);
                    $this->Cell($wh[1], $alturas, "$".number_format($arrMov[$j]['total_entrada']), 'LRTB', 0, 'C', $fill);
                    $this->MultiCell($wh[2],$alturas,$arrMov[$j]['unidad'],"LRTB",'C',$fill,0,'','',true,0,false,true,0,'M',true);
                    $this->Cell($wh[2], $alturas, "$".number_format($arrMov[$j]['valor']), 'LRTB', 0, 'C', $fill);
                    $this->Cell($wh[2], $alturas, $arrMov[$j]['salida'], 'LRTB', 0, 'C', $fill);
                    $this->Cell($wh[2], $alturas, "$".number_format($arrMov[$j]['total_salida']), 'LRTB', 0, 'C', $fill);
                    $this->MultiCell($wh[3],$alturas,$arrMov[$j]['unidad'],"LRTB",'C',$fill,0,'','',true,0,false,true,0,'M',true);
                    $this->Cell($wh[3], $alturas, "$".number_format($arrMov[$j]['valor']), 'LRTB', 0, 'C', $fill);
                    $this->Cell($wh[3], $alturas, $arrMov[$j]['cantidad_saldo'], 'LRTB', 0, 'C', $fill);
                    $this->Cell($wh[3], $alturas, "$".number_format($arrMov[$j]['total_saldo']), 'LRTB', 0, 'C', $fill);

                    $this->Ln();
                    //$fill=!$fill;
                }
                if($data[$i]['cantidad_total'] < 0){
                    $this->SetFillColor(255,255,0);
                }else{
                    $this->SetFillColor(245,245,245);
                    $this->SetTextColor(000);
                    $this->SetFont('helvetica','',7);
                }
                $this->Cell(257.5, $alturas, "En bodega: ".$data[$i]['cantidad_total'], 'LBT', 0, 'R', 1);
                $this->Cell(19.8, $alturas, "Total: "."$".number_format($data[$i]['valor_total']), 'BRT', 0, 'R', 1);
                $this->Ln();
                if($this->GetY()>177){
                    $this->AddPage();
                }

            }
            $this->Cell(array_sum($w), 0, '', '');
            $this->Ln();
            /*$this->SetFont('helvetica','B',8);
            $this->SetFillColor(245,245,245);
            $this->Cell(array_sum($w)-($w[8]+$w[9]), 6, '', '', 0, 'R');
            $this->Cell($w[count($w)-2], 6, 'TOTAL:', '', 0, 'R');
            $this->Cell($w[count($w)-1], 6, '$'.number_format($total), '', 0, 'R');*/
        }
		public function Footer()
		{
			$linkbd = conectar_v7();
			$linkbd -> set_charset("utf8");
			$sqlr="SELECT direccion,telefono,web,email FROM configbasica WHERE estado='S'";
			$resp=mysqli_query($linkbd, $sqlr);
			$user = $_SESSION['nickusu'];
			$fecha = date("Y-m-d H:i:s");
			$ip = $_SERVER['REMOTE_ADDR'];
			$useri = $_POST['user'];
			while($row=mysqli_fetch_row($resp))
			{
				$direcc=strtoupper($row[0]);
				$telefonos=$row[1];
				$dirweb=strtoupper($row[3]);
				$coemail=strtoupper($row[2]);
			}
			if($direcc!=''){$vardirec="Dirección: $direcc, ";}
			else {$vardirec="";}
			if($telefonos!=''){$vartelef="Telefonos: $telefonos";}
			else{$vartelef="";}
			if($dirweb!=''){$varemail="Email: $dirweb, ";}
			else {$varemail="";}
			if($coemail!=''){$varpagiw="Pagina Web: $coemail";}
			else{$varpagiw="";}
			$this->SetFont('helvetica', 'I', 8);
			$txt = <<<EOD
			$vardirec $vartelef
			$varemail $varpagiw
			EOD;
			$this->SetFont('helvetica', 'I', 6);
			$this->Cell(277,10,'','T',0,'T');
			$this->ln(2);
			$this->Write(0, $txt , '', 0, 'C', true, 0, false, false, 0);

            //echo $rowu[6];

            $sqlrcc="SELECT * FROM `usuarios` WHERE `cc_usu` = '$_SESSION[cedulausu]'";
            $respcc=mysqli_query($linkbd, $sqlrcc);
            $rowcc=mysqli_fetch_row($respcc);
            //echo $rowcc[1];

			$this->Cell(50, 10, 'Hecho por: '.$rowcc[1], 00, false, 'C', 0, '', 0, false, 'T', 'M');
			$this->Cell(70, 10, 'Impreso por: '.$user, 0, false, 'C', 0, '', 0, false, 'T', 'M');
			$this->Cell(30, 10, 'IP: '.$ip, 0, false, 'C',0, '', 0, false, 'T', 'M');
			$this->Cell(80, 10, 'Fecha: '.$fecha, 0, false, 'C', 0, '', 0, false, 'T', 'M');
			$this->Cell(33, 10, 'IDEAL.10 S.A.S', 0, false, 'C', 0, '', 0, false, 'T', 'M');
            $this->Cell(25, 10, 'Pagina '.$this->getAliasNumPage().'/'.$this->getAliasNbPages(), 0, false, 'C', 0, '', 0, false, 'T', 'M');
		}
	}
	if($_GET){
        $obj = new Articulo();
        $request = $obj->selectMovimientos($_GET['articulo'],$_GET['bodega'],$_GET['fecha_inicial'],$_GET['fecha_final']);
        //dep($request);exit;
        define("DATA",$request);
        $pdf = new MYPDF('L','mm','Letter', true, 'iso-8859-1', false);
        $pdf->SetDocInfoUnicode (true);
        // set document information
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('IDEALSAS');
        $pdf->SetTitle('Kardex Almacén');
        $pdf->SetSubject('Kardex Almacén');
        $pdf->SetKeywords('TCPDF, PDF, example, test, guide');
        $pdf->SetMargins(10, 38, 10);// set margins
        $pdf->SetHeaderMargin(38);// set margins
        $pdf->SetFooterMargin(17);// set margins
        $pdf->SetAutoPageBreak(TRUE, 20);// set auto page breaks
        // set some language-dependent strings (optional)
        if (@file_exists(dirname(__FILE__).'/lang/spa.php'))
        {
            require_once(dirname(__FILE__).'/lang/spa.php');
            $pdf->setLanguageArray($l);
        }
        $pdf->AddPage();

        $pdf->SetFillColor(255,255,0);
        $pdf->SetFont('helvetica','B',7);
        $pdf->Cell(40, 7, "Cantidad negativa", 'LRTB', 0, 'C', 1);
        $pdf->SetFillColor(75,0,130);
        $pdf->SetTextColor(255);
        $pdf->SetFont('helvetica','B',7);
        $pdf->Cell(50, 7, "Cantidad no coincide contabilidad", 'LRTB', 0, 'C', 1);
        $pdf->SetFillColor(255,0,0);
        $pdf->SetTextColor(255,255,255);
        $pdf->SetFont('helvetica','B',7);
        $pdf->Cell(40, 7, "No existe contabilidad", 'LRTB', 0, 'C', 1);
        $pdf->ln(10);
        $pdf->ColoredTable($request);
        $pdf->ln(5);

        $pdf->Output('reporte_kardex.pdf', 'I');
    }
?>


