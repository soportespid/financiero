<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");

	require "comun.inc";
	require "funciones.inc";
	require "conversor.php";
	require "validaciones.inc";

	$linkbd_V7 = conectar_v7();
	$linkbd_V7 -> set_charset("utf8");

	session_start();
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE > 
<html lang="es">
	<head>
		<meta charset="utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1"/>
		<meta name="viewport" content="user-scalable=no">
		<title>:: IDEAL 10 - Tesorer&iacute;a</title>
		<link href="favicon.ico" rel="shortcut icon"/>
        <link href="css/css2.css" rel="stylesheet" type="text/css" />
        <link href="css/css3.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="css/programas.js"></script>
    </head>
	<style>
	
	</style>
    <body>
        <IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
        <span id="todastablas2"></span>
        <table>
            <tr><script>barra_imagenes("teso");</script><?php cuadro_titulos();?></tr>	 
            <tr><?php menu_desplegable("teso");?></tr>
        	<tr>
          		<td colspan="3" class="cinta">
				<a class="mgbt"><img src="imagenes/add2.png" /></a> 
				<a class="mgbt"><img src="imagenes/guardad.png" style="width:24px;"/></a> 
				<a class="mgbt"><img src="imagenes/buscad.png"/></a> 
				<a href="#" onClick="mypop=window.open('teso-principal.php','','');mypop.focus();" class="mgbt"><img src="imagenes/nv.png" title="Nueva Ventana"></a></td>
        	</tr>
        </table>
 		<form name="form2" method="post" action="">
    		<table class="inicio">
      			<tr>
        			<td class="titulos" colspan="2">.: Informes Predios </td>
        			<td class="cerrar" style="width:7%;" ><a href="teso-principal.php">&nbsp;Cerrar</a></td>
      			</tr>
				<tr>
					<td class="saludo1" width=70%;>
						<ol id="lista2">
							<li onClick="location.href='teso-reportepredial.php'" style="cursor:pointer;">Estado predios por c&oacute;digo catastral</li>
							<li onClick="location.href='teso-reportepredialporvigencia.php'" style="cursor:pointer;">Estado predios acumulado</li>
                            <li onClick="location.href='teso-reportepredios.php'" style="cursor:pointer;">Reporte predios</li>
							<li onClick="location.href='teso-reportecobropredial.php'" style="cursor:pointer;">Reporte liquidaci&oacute;n predial</li>
							<li onClick="location.href='teso-estadocuenta.php'" style="cursor:pointer;">Estado de cuenta</li>
							<li onClick="location.href='teso-estadocuenta-masivo.php'" style="cursor:pointer;">Estado de cuenta masivo</li>
							<li onClick="location.href='teso-reportecarterapredial.php'" style="cursor:pointer;">Reporte cartera predial</li>
							<li onClick="location.href='teso-reporteacuerdosdepago.php'" style="cursor:pointer;">Reporte Acuerdos de pago predial</li>              
                        </ol>
					</td> 
					<td colspan="2" rowspan="1" style="background:url(imagenes/siglasideal.png); background-repeat:no-repeat; background-position:center; background-size: 100% 80%"></td>
					
				</tr>
				                
				</tr>							
    		</table>
		</form>
	</body>
</html>