<?php 
	require_once("tcpdf2/tcpdf_include.php");
	include ('jpgraph-4.3.5/src/jpgraph.php');
	require_once ('jpgraph-4.3.5/src/jpgraph_bar.php');//barras
	require('comun.inc');
	require ('funciones.inc');
	ini_set('max_execution_time',99999999);
	session_start();
	class MYPDF extends TCPDF {
		public function Header() {}
		public function Footer() {}
	}
	$orientacion = 'P';	//vertical
	$unidadpdf = 'mm';	//unidad de medida
	$tamanohoja = 'A4';	//tamaño de la hoja
	$unicode = true;	//Formato Unicode
	$format = 'UTF-8';	//codificación formato caracteres
	$tablacon = false;	//se va a agregar la pagina a una tabla de contenido
	$pdf = new MYPDF($orientacion,$unidadpdf,$tamanohoja , $unicode, $format, $tablacon);
	$pdf->SetDocInfoUnicode (true);
	// set document information
	$pdf->SetCreator(PDF_CREATOR);
	$pdf->SetAuthor('IDEAL10');
	$pdf->SetTitle('Factura');
	$pdf->SetSubject('Factura');
	$pdf->SetKeywords('');
	$pdf->SetMargins(0, 0, 0);
	$pdf->SetHeaderMargin(0);
	$pdf->SetFooterMargin(0);
	$pdf->SetAutoPageBreak(TRUE, 0);
	if (@file_exists(dirname(__FILE__).'/lang/spa.php')) 
	{
		require_once(dirname(__FILE__).'/lang/spa.php');
		$pdf->setLanguageArray($l);
	}
	ini_set('max_execution_time', 7200);
	$yy=0;
	$xx=0;
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
	$numcorte = $_POST['idCorte'];
	$facturas = json_decode($_POST['facturas'],true);
	$sqlcorte = "SELECT fecha_impresion,fecha_inicial,fecha_final,fecha_corte,version_tarifa,fecha_limite_pago FROM srvcortes WHERE numero_corte = '$numcorte'";
	$rescorte = mysqli_query($linkbd,$sqlcorte);
	$rowcorte = mysqli_fetch_row($rescorte);
	
	$fechaimpresion = date('d/m/Y',strtotime($rowcorte[0]));
	$fechainicial = date('d/m/Y',strtotime($rowcorte[1]));
	$fechafinal = date('d/m/Y',strtotime($rowcorte[2]));
	$fechasuspension = date('d/m/Y',strtotime($rowcorte[3]));
	$periodofac = $fechainicial.' al '.$fechafinal;
	$fecha_limite = date('d/m/Y',strtotime($rowcorte[5]));
	if (is_array($facturas) == false) {
		$facturas = [$facturas];
	}

	foreach($facturas as $numfac) {
		$pdf->AddPage();
		$c+=1;
		//CARGAR INFORMACIÓN
		{
			$sqlser = "SELECT DISTINCT nombre_suscriptor, cod_usuario, id_ruta, cliente, estado FROM srvfacturas WHERE num_factura = '$numfac'";
			$respser = mysqli_query($linkbd,$sqlser);
			$rowser = mysqli_fetch_row($respser);
			$nomcliente = $rowser[0];
			$codcliente = $rowser[1];
			$codruta = $rowser[2];
			$idcliente = $rowser[3];
			$estadocliente = $rowser[4];

			$sqlcliente = "SELECT  id_estrato, id_tercero, id_barrio, cod_catastral, estado FROM srvclientes WHERE id = '$idcliente'";
			$rescliente = mysqli_query($linkbd,$sqlcliente);
			$rowcliente = mysqli_fetch_row($rescliente);
			$idbarrio = $rowcliente[2];
			$codcatastral =  $rowcliente[3];
			$idestrato = $rowcliente[0];

			$sqlbarrio = "SELECT nombre FROM srvbarrios WHERE id = '$idbarrio'";
			$resbarrio = mysqli_query($linkbd,$sqlbarrio);
			$rowbarrio = mysqli_fetch_row($resbarrio);
			$nombarrio = $rowbarrio[0];

			$sqldireccion = "SELECT direccion FROM srvdireccion_cliente WHERE id = '$idcliente'";
			$resdireccion = mysqli_query($linkbd,$sqldireccion);
			$rowdireccion = mysqli_fetch_row($resdireccion);
			$direccionpredio = $rowdireccion[0];
			$direccionenvio = $rowdireccion[0];

			$sqlestrato = "SELECT descripcion, tipo, sub_contri FROM srvestratos WHERE id = '$idestrato'";
			$resestrato = mysqli_query($linkbd,$sqlestrato);
			$rowestrato = mysqli_fetch_row($resestrato);
			$estrato = $rowestrato[0];
			$tipoestrato = $rowestrato[1];
			$porcentaje_sub_des = $rowestrato[2] ;

			$sqlusosuelo = "SELECT nombre FROM srvtipo_uso WHERE id = '$tipoestrato'";
			$resusosuelo = mysqli_query($linkbd,$sqlusosuelo);
			$rowusosuelo = mysqli_fetch_row($resusosuelo);
			$usosuelo = $rowusosuelo[0];

			$sqlmedidorid1 = "SELECT id_medidor, id_estrato FROM srvasignacion_servicio WHERE id_clientes = '$idcliente' AND id_servicio = '1'";
			$resmedidorid1 = mysqli_query($linkbd,$sqlmedidorid1);
			$rowmedidorid1 = mysqli_fetch_row($resmedidorid1);
			$idmedidor1 = $rowmedidorid1[0];
			$idestrato = $rowmedidorid1[1];

			$sqlmedidor1 = "SELECT serial, diametro, marca, fecha_activacion FROM srvmedidores WHERE id = ";
			$resmedidor1 = mysqli_query($linkbd,$sqlmedidor1);
			$rowmedidor1 = mysqli_fetch_row($resmedidor1);

			$serialmedidor = $rowmedidor1[0];
			$diametromedidor = $rowmedidor1[1];
			$marcamedidor = $rowmedidor1[2];
			$fechainsmedidor = $rowmedidor1[3];
			$promedio = 0;
			$divprom = 0;

			//Otros Pagos
			$saldosOtrosPagos = 0;
			$zyx = 0;
			$valor_otros[$zyx] = 0;
			$concepto_otros[$zyx] = '';
			$valor_ap = 0;
			$saldo_ap = 0;

			$sqlAcuerdoPago = "SELECT SUM(acuerdo_pago) FROM srvfacturas WHERE num_factura = '$numfac'";
			$rowAcuerdoPago = mysqli_fetch_row(mysqli_query($linkbd, $sqlAcuerdoPago));
			
			$valor_ap = $rowAcuerdoPago[0];


			$sqlAcuerdoSaldo = "SELECT (valor_acuerdo-valor_liquidado) FROM srv_acuerdo_cab WHERE id_cliente = '$idcliente' AND estado_acuerdo = 'Activo'";
			$rowAcuerdoSaldo = mysqli_fetch_row(mysqli_query($linkbd, $sqlAcuerdoSaldo));

			$saldo_ap = $rowAcuerdoSaldo[0];

			$sqlotrostipo = "SELECT SUM(credito) FROM srvdetalles_facturacion WHERE numero_facturacion='$numfac' AND id_servicio='1' AND id_tipo_cobro = '8'";
			$resotrostipo = mysqli_query($linkbd,$sqlotrostipo);
			$rowotrostipo = mysqli_fetch_row($resotrostipo);

			if($rowotrostipo[0]!= 0){
				$concepto_otros[$zyx] = "SALDO AC: ";
				$valor_otros[$zyx] = $rowotrostipo[0];
				$saldosOtrosPagos += $rowotrostipo[0];
				$zyx++;
			}

			$sqlotrostipo = "SELECT SUM(credito) FROM srvdetalles_facturacion WHERE numero_facturacion='$numfac' AND id_servicio='1' AND id_tipo_cobro = '10'";
			$resotrostipo = mysqli_query($linkbd,$sqlotrostipo);
			$rowotrostipo = mysqli_fetch_row($resotrostipo);
			
			if($rowotrostipo[0]!= 0){
				$concepto_otros[$zyx] = "INTERES AC: ";
				$valor_otros[$zyx] = $rowotrostipo[0];
				$saldosOtrosPagos += $rowotrostipo[0];
				$zyx++;
			}

			$sqlotrostipo = "SELECT SUM(credito) FROM srvdetalles_facturacion WHERE numero_facturacion='$numfac' AND id_servicio='1' AND id_tipo_cobro = '9'";
			$resotrostipo = mysqli_query($linkbd,$sqlotrostipo);
			$rowotrostipo = mysqli_fetch_row($resotrostipo);
			
			if($rowotrostipo[0]!= 0){
				$concepto_otros[$zyx] = "OTROS COBROS AC: ";
				$valor_otros[$zyx] = $rowotrostipo[0];
				$saldosOtrosPagos += $rowotrostipo[0];
				$zyx++;
			}

			$sqlotrostipo = "SELECT SUM(credito) FROM srvdetalles_facturacion WHERE numero_facturacion='$numfac' AND id_servicio='3' AND id_tipo_cobro = '8'";
			$resotrostipo = mysqli_query($linkbd,$sqlotrostipo);
			$rowotrostipo = mysqli_fetch_row($resotrostipo);
			
			if($rowotrostipo[0]!= 0){
				$concepto_otros[$zyx] = "SALDO ASEO: ";
				$valor_otros[$zyx] = $rowotrostipo[0];
				$saldosOtrosPagos += $rowotrostipo[0];
				$zyx++;
			}

			$sqlotrostipo = "SELECT SUM(credito) FROM srvdetalles_facturacion WHERE numero_facturacion='$numfac' AND id_servicio='3' AND id_tipo_cobro = '10'";
			$resotrostipo = mysqli_query($linkbd,$sqlotrostipo);
			$rowotrostipo = mysqli_fetch_row($resotrostipo);
			
			if($rowotrostipo[0]!= 0){
				$concepto_otros[$zyx] = "INTERES ASEO: ";
				$valor_otros[$zyx] = $rowotrostipo[0];
				$saldosOtrosPagos += $rowotrostipo[0];
				$zyx++;
			}

			$sqlotrostipo = "SELECT SUM(credito) FROM srvdetalles_facturacion WHERE numero_facturacion='$numfac' AND id_servicio='1' AND id_tipo_cobro = '19'";
			$resotrostipo = mysqli_query($linkbd,$sqlotrostipo);
			$rowotrostipo = mysqli_fetch_row($resotrostipo);
			
			if($rowotrostipo[0]!= 0){
				$concepto_otros[$zyx] = "AJUSTE AL PESO: ";
				$valor_otros[$zyx] = $rowotrostipo[0];
				$saldosOtrosPagos += $rowotrostipo[0];
				$zyx++;
			}

			$FacturasVencidas = 0;
			$sqlFacturasVencidas = "SELECT estado_pago FROM srvcortes_detalle WHERE id_cliente = '$idcliente' AND id_corte < '$numcorte' ORDER BY id_corte DESC ";
			$resFacturasVencidas = mysqli_query($linkbd, $sqlFacturasVencidas);
			while ($rowFacturasVencidas = mysqli_fetch_row($resFacturasVencidas)){

				if ($rowFacturasVencidas[0] == 'V') {
					$FacturasVencidas += 1;
				}
				else {
					break;
				}
			}

			$sqllecturas2 = "SELECT lectura_medidor FROM srvlectura WHERE corte = $numcorte-1 AND id_cliente = '$idcliente' AND id_servicio = '1'";
			$reslecturas2 = mysqli_query($linkbd,$sqllecturas2);
			$rowlecturas2 = mysqli_fetch_row($reslecturas2);
	
			$lectura2 = $rowlecturas2[0];

			$sqllecturas1 = "SELECT consumo, lectura_medidor FROM srvlectura WHERE corte = '$numcorte' AND id_cliente = '$idcliente' AND id_servicio = '1'";
			$reslecturas1 = mysqli_query($linkbd,$sqllecturas1);
			$rowlecturas1 = mysqli_fetch_row($reslecturas1);
			$valconsumo1 = $rowlecturas1[0];
			$lectura1 = $rowlecturas1[1];
			$contari = 0;
			$sqltarifas1 = "SELECT rango_final, costo_unidad, subsidio, contribucion FROM srvtarifas WHERE id_servicio = '1' AND id_estrato = '$idestrato' AND version = $rowcorte[4] ORDER BY rango_final ASC";
			$restarifas1 = mysqli_query($linkbd,$sqltarifas1);
			while ($rowtarifas1 = mysqli_fetch_row($restarifas1)){
				$rangoc[$contari] = $rowtarifas1[0];
				$costouni[$contari] = $rowtarifas1[1];
				$porsub[$contari] = $rowtarifas1[2];
				$porcont[$contari] = $rowtarifas1[3];
				$contari++;
			}

			switch (true){
				case ($valconsumo1 <= $rangoc[0]): {
					$cantidad_cb = $valconsumo1;
					$cantidad_cc = 0;
					$cantidad_cs = 0;
					$tarifa_cb = $costouni[0];
					$tarifa_cc = 0;
					$tarifa_cs = 0;
					$subsidio_cb = $porsub[0];
					$subsidio_cc = 0;
					$subsidio_cs = 0;
					$contribu_cb = $porcont[0];
					$contribu_cc = 0;
					$contribu_cs = 0;
				}break;
				case ($valconsumo1 <= $rangoc[1]): {
					$cantidad_cb = $rangoc[0];
					$cantidad_cc = $valconsumo1 - $rangoc[0];
					$cantidad_cs = 0;
					$tarifa_cb = $costouni[0];
					$tarifa_cc = $costouni[1];
					$tarifa_cs = 0;
					$subsidio_cb = $porsub[0];
					$subsidio_cc = $porsub[1];
					$subsidio_cs = 0;
					$contribu_cb = $porcont[0];
					$contribu_cc = $porcont[1];
					$contribu_cs = 0;
				}break;
				default:{
					$cantidad_cb = $rangoc[0];
					$cantidad_cc = $rangoc[1] - $rangoc[0];
					$cantidad_cs = $valconsumo1 - $rangoc[1];
					$tarifa_cb = $costouni[0];
					$tarifa_cc = $costouni[1];
					$tarifa_cs = $costouni[2];
					$subsidio_cb = $porsub[0];
					$subsidio_cc = $porsub[1];
					$subsidio_cs = $porsub[2];
					$contribu_cb = $porcont[0];
					$contribu_cc = $porcont[1];
					$contribu_cs = $porcont[2];
				}
			}

			$sqlservicio1 = "SELECT cargo_f, subsidio_cf, contribucion_cf, consumo_b, subsidio_cb, contribucion_cb, consumo_c, subsidio_cc, contribucion_cc, consumo_s, subsidio_cs, contribucion_cs FROM srvfacturas WHERE num_factura = '$numfac' AND id_servicio = '1'";
			$resservicio1 = mysqli_query($linkbd,$sqlservicio1);
			$rowservicio1 = mysqli_fetch_row($resservicio1);

			//Cargo Fijo
			$acu_cf_tarifa = $rowservicio1[0];
			$acu_cf_subcont = abs($porcentaje_sub_des);
			$acu_cf_vlrsub = $rowservicio1[1];
			$acu_cf_vlrcontr = $rowservicio1[2];
			$acu_cf_precio = $acu_cf_tarifa - $acu_cf_vlrsub + $acu_cf_vlrcontr;
			$acu_cf_cant = 1;
			$acu_cf_valor = $acu_cf_precio  * $acu_cf_cant;

			//Consumo Básico
			$acu_cb_tarifa = $tarifa_cb;
			$acu_cb_subcont = $subsidio_cb + $contribu_cb;
			$acu_cb_vlrsub =  $tarifa_cb * ($subsidio_cb / 100);
			$acu_cb_vlrcontr = $tarifa_cb * ($contribu_cb / 100);
			$acu_cb_cant = $cantidad_cb;
			$acu_cb_precio = $acu_cb_tarifa - $acu_cb_vlrsub + $acu_cb_vlrcontr;
			$acu_cb_valor = $rowservicio1[3] - $rowservicio1[4] + $rowservicio1[5];

			//Consumo Complementario
			$acu_cc_tarifa = $tarifa_cc;
			$acu_cc_subcont = $subsidio_cc + $contribu_cc;
			$acu_cc_vlrsub = $tarifa_cc * ($subsidio_cc / 100);
			$acu_cc_vlrcontr = $tarifa_cc * ($contribu_cc / 100);
			$acu_cc_cant = $cantidad_cc;
			$acu_cc_precio = $acu_cc_tarifa - $acu_cc_vlrsub + $acu_cc_vlrcontr;
			$acu_cc_valor = $rowservicio1[6] - $rowservicio1[7] + $rowservicio1[8];

			//Consumo Suntuario
			$acu_cs_tarifa = $tarifa_cs;
			$acu_cs_subcont = $subsidio_cs + $contribu_cs;
			$acu_cs_vlrsub = $tarifa_cs * ($subsidio_cs / 100);
			$acu_cs_vlrcontr = $tarifa_cs * ($contribu_cs / 100);
			$acu_cs_cant = $cantidad_cs;
			$acu_cs_precio = $acu_cs_tarifa - $acu_cs_vlrsub + $acu_cs_vlrcontr;
			$acu_cs_valor = $rowservicio1[9] - $rowservicio1[10] + $rowservicio1[11];

			//Tasa de Uso de Agua
			$acu_tu_tarifa = '';
			$acu_tu_subcont = '$2.81/m3';
			$acu_tu_vlrsub = '';
			$acu_tu_vlrcontr = '';
			$acu_tu_precio = '';
			$acu_tu_cant = '';
			$acu_tu_valor = '';
			//Total Acueducto
			$acu_total = $acu_cf_valor + $acu_cb_valor + $acu_cc_valor + $acu_cs_valor;

			$sqlservicio3 = "SELECT cargo_f, subsidio_cf, contribucion_cf, consumo_b, subsidio_cb, contribucion_cb, consumo_c, subsidio_cc, contribucion_cc, consumo_s, subsidio_cs, contribucion_cs FROM srvfacturas WHERE num_factura = '$numfac' AND id_servicio = '3'";
			$resservicio3 = mysqli_query($linkbd,$sqlservicio3);
			$rowservicio3 = mysqli_fetch_row($resservicio3);

			//Cargo Fijo
			$ase_cf_tarifa = $rowservicio3[0];
			$ase_cf_subcont = abs($porcentaje_sub_des);
			$ase_cf_vlrsub = $rowservicio3[1];
			$ase_cf_vlrcontr = $rowservicio3[2];
			$ase_cf_precio = $ase_cf_tarifa - $ase_cf_vlrsub + $ase_cf_vlrcontr;
			$ase_cf_cant = 1;
			$ase_cf_valor = $ase_cf_precio * $ase_cf_cant;

			//CVNA
			$ase_cv_tarifa = $rowservicio3[3];
			$ase_cv_subcont = abs($porcentaje_sub_des);
			$ase_cv_vlrsub = $rowservicio3[4];
			$ase_cv_vlrcontr = $rowservicio3[5];
			$ase_cv_precio = $ase_cv_tarifa - $ase_cv_vlrsub + $ase_cv_vlrcontr;
			$ase_cv_cant = 1;
			$ase_cv_valor = $ase_cv_precio * $ase_cv_cant;

			//Total Aseo
			$ase_total = $ase_cf_valor +$ase_cv_valor;

			//Total Servicios
			$total_servicios = $acu_total + $alc_total + $ase_total;

			//total Factura
			$total_factura = $total_servicios + $saldosOtrosPagos + $valor_ap;
			$valorAproximado = ceil ($total_factura / 100) * 100;
			$ajustePeso = $valorAproximado - $total_factura;

			$concepto_otros[$zyx] = "AJUSTE AL PESO: ";
			$valor_otros[$zyx] = $ajustePeso;
			$saldosOtrosPagos += $ajustePeso;
			$zyx++;

			//consumos
			$consumo7 = $consumo6 = $consumo5 = $consumo4 = $consumo3 = $consumo2 = $consumo1 = 0;
			$sqlconsumo = "SELECT GROUP_CONCAT(consumo ORDER BY corte DESC SEPARATOR '<->') FROM srvlectura WHERE id_servicio = '1' AND id_cliente = '$idcliente' AND corte <= '$numcorte'";
			$resconsumo = mysqli_query($linkbd,$sqlconsumo);
			$rowconsumo = mysqli_fetch_row($resconsumo);

			$consumott = explode('<->', $rowconsumo[0]);
			if($consumott[0] != ''){$consumo1 = $consumott[0];}
			else {$consumo1 = 0;}
			if($consumott[1] != ''){$consumo2 = $consumott[1];}
			else {$consumo2 = 0;}
			if($consumott[2] != ''){$consumo3 = $consumott[2];}
			else {$consumo3 = 0;}
			if($consumott[3] != ''){$consumo4 = $consumott[3];}
			else {$consumo4 = 0;}
			if($consumott[4] != ''){$consumo5 = $consumott[4];}
			else {$consumo5 = 0;}
			if($consumott[5] != ''){$consumo6 = $consumott[5];}
			else {$consumo6 = 0;}
			if($consumott[6] != ''){$consumo7 = $consumott[6];}
			else {$consumo7 = 0;}

			if($consumo1 != 0){$promedio+=$consumo1; $divprom++;}
			if($consumo2!=0){$promedio+=$consumo2; $divprom++;}
			if($consumo3!=0){$promedio+=$consumo3; $divprom++;}
			if($consumo4!=0){$promedio+=$consumo4; $divprom++;}
			if($consumo5!=0){$promedio+=$consumo5; $divprom++;}
			if($consumo6!=0){$promedio+=$consumo6; $divprom++;}
			if($consumo7!=0){$promedio+=$consumo7; $divprom++;}

			$resultadoPromedio = number_format($promedio / $divprom,2,".",",");

			$observacion = '';
			$sqlobservacion = "SELECT codigo_observacion FROM srv_asigna_novedades WHERE id_cliente = '$idcliente' AND corte = '$numcorte' AND estado = 'S' ";
			$resobservacion = mysqli_query($linkbd,$sqlobservacion);
			$rowobservacion = mysqli_fetch_row($resobservacion);
			$sqlNovedadCab = "SELECT descripcion, afecta_facturacion FROM srv_tipo_observacion WHERE codigo_observacion = '$rowobservacion[0]'";
			$rowNovedadCab = mysqli_fetch_row(mysqli_query($linkbd, $sqlNovedadCab));
			$observacion = $rowNovedadCab[0];

			$frecuencu_rec = 2;
			$frecuencu_bar = 1;
			$val_cft = 5195.27;
			$val_cvna = 219802.16;
			$val_trn = 0.05760;
			$val_tra = 0;
			$val_tfa = 0;
			$val_cva = 211010.08;
			$val_tfn = 0;
		}
		//Codigo de barras
		{
			$cod00 = 415;//primer separador
			$cod01 = '0000000000000';//codigo GS1 Asignado
			$cod02 = str_pad($numfac,18,"0", STR_PAD_LEFT);//numero factura
			$cod03 = str_pad($total_factura,10,"0", STR_PAD_LEFT);//valor total factura
			$cod04 = $fechap[0].$fechap[1].$fechap[2];//fecha limite
			$codtotn = "($cod00)$cod01(8020)$cod02(3900)$cod03(96)$cod04";//codigo mostrar
			$codtot =chr(241)."$cod00".$cod01."8020".$cod02.chr(241)."3900".$cod03.chr(241)."96".$cod04;//codigo para banco
			
			//estilo del codigo de barras
			$style = array(
			'position' => 'C',
			'align' => 'C',
			'stretch' => false,
			'fitwidth' => true,
			'cellfitalign' => '',
			'border' => false,
			'hpadding' => 'auto',
			'vpadding' => 'auto',
			'fgcolor' => array(0,0,0),
			'bgcolor' => false,
			'text' => true,
			'font' => 'helvetica',
			'fontsize' => 8,
			'stretchtext' => 1);
		}
		//cuadro 1 
		{
			$pdf->Image('imagenes/aguasrosalia.jpg',158,9,40,20);
			$pdf->SetY(10+$yy);
			$pdf->SetFont('helvetica', 'B', 8);
			$pdf->Cell(208,0,'MUNICIPIO DE SANTA ROSALIA',0,1,'C',false,0,0,false,'T','C');
			$pdf->Cell(208,0,'AGUAS DEL ROSALIA',0,1,'C',false,0,0,false,'T','C');
			$pdf->Cell(208,0,'NIT 900267997-9',0,1,'C',false,0,0,false,'T','C');
			$pdf->SetFont('helvetica', '', 8);
			$pdf->Cell(208,0,'Calle 5 N. 7 - 03 Palacio Municipal',0,1,'C',false,0,0,false,'T','C');
			$pdf->SetFont('helvetica', '', 6.5);
			$pdf->Cell(208,0,'Email: apc.aguasderosalia.aaa@gmail.com Teléfono: 3105531782',0,1,'C',false,0,0,false,'T','C');
			$pdf->Cell(208,0,'FACTURA DE SERVICIOS PUBLICOS DE ACUEDUCTO. ALCANTARILLADO Y ASEO',0,1,'C',false,0,0,false,'T','C');
		}
		//cuadro 2 
		{
			$pdf->SetY(10+$yy);
			$pdf->Cell(55,0,'','RTL',1,'C',false,0,0,false,'T','C');
			$pdf->SetFont('helvetica', 'B', 9);
			$pdf->Cell(25,0,'FACTURA No.','L',0,'L',false,0,0,false,'T','C');
			$pdf->Cell(30,0,$numfac,'R',1,'L',false,0,0,false,'T','C');
			$pdf->Cell(25,0,'CÓDIGO','L',0,'L',false,0,0,false,'T','C');
			$pdf->Cell(30,0,$codcliente,'R',1,'L',false,0,0,false,'T','C');
			$pdf->SetFont('helvetica', '', 7);
			$pdf->Cell(25,0,'Periodo','L',0,'L',false,0,0,false,'T','C');
			$pdf->Cell(30,0,$periodofac,'R',1,'L',false,0,0,false,'T','C');
			$pdf->Cell(25,0,'Fecha Impresión:','L',0,'L',false,0,0,false,'T','C');
			$pdf->Cell(30,0,$fechaimpresion,'R',1,'L',false,0,0,false,'T','C');
			$pdf->Cell(25,0,'Suspensión desde:','L',0,'L',false,0,0,false,'T','C');
			$pdf->Cell(30,0,$fechasuspension,'R',1,'L',false,0,0,false,'T','C');
			$pdf->Cell(55,0,'','RBL',1,'C',false,0,0,false,'T','C');
		}
		//cuadro 3 
		{
			$pdf->Cell(55,0,'','LR',1,'L',false,0,0,false,'T','C');
			$pdf->SetFont('helvetica', 'B', 8);
			$pdf->Cell(55,0,'DATOS SUSCRIPTOR','LR',1,'L',false,0,0,false,'T','C');
			$pdf->SetFont('helvetica', '', 7);
			$pdf->Cell(12,0,'NOMBRE:','L',0,'L',false,0,0,false,'T','C');
			$pdf->SetFont('helvetica', 'B', 7);
			$pdf->Cell(43,0,substr($nomcliente,0,26),'R',1,'L',false,0,0,false,'T','C');
			$pdf->SetFont('helvetica', '', 7);
			$pdf->Cell(55,0,substr("BARRIO: $nombarrio",0,32),'LR',1,'L',false,0,0,false,'T','C');
			$pdf->Cell(26,0,'DIRECCIÓN PREDIO:','L',0,'L',false,0,0,false,'T','C');
			$pdf->Cell(29,0,substr($direccionpredio,0,24),'R',1,'L',false,0,0,false,'T','C');
			$pdf->Cell(26,0,'DIRECCIÓN ENVÍO:','L',0,'L',false,0,0,false,'T','C');
			$pdf->Cell(29,0,substr($direccionenvio,0,24),'R',1,'L',false,0,0,false,'T','C');
			$pdf->Cell(55,0,"CEDULA CATASTRAL: $codcatastral",'LR',1,'L',false,0,0,false,'T','C');
			$pdf->Cell(55,0,'','LR',1,'L',false,0,0,false,'T','C');

			$pdf->SetFont('helvetica', 'B', 8);
			$pdf->Cell(55,0,'CLASIFICACIÓN DEL PREDIO','LR',1,'L',false,0,0,false,'T','C');
			$pdf->SetFont('helvetica', '', 7);
			$pdf->Cell(11,0,'Uso:','L',0,'L',false,0,0,false,'T','C');
			$pdf->SetFont('helvetica', '', 6);
			$pdf->Cell(44,0,$usosuelo,'R',1,'L',false,0,0,false,'T','C');
			$pdf->SetFont('helvetica', '', 7);
			$pdf->Cell(11,0,'Estrato:','L',0,'L',false,0,0,false,'T','C');
			$pdf->Cell(44,0,$estrato,'R',1,'L',false,0,0,false,'T','C');
			$pdf->Cell(11,0,'Ruta:','L',0,'L',false,0,0,false,'T','C');
			$pdf->Cell(44,0,$codruta,'R',1,'L',false,0,0,false,'T','C');
			$pdf->Cell(11,0,'Estado:','L',0,'L',false,0,0,false,'T','C');
			$pdf->Cell(44,0,$estadocliente,'R',1,'L',false,0,0,false,'T','C');
			$pdf->Cell(55,0,'','LR',1,'L',false,0,0,false,'T','C');

			$pdf->SetFont('helvetica', 'B', 8);
			$pdf->Cell(55,0,'DATOS DEL MEDIDOR','LR',1,'L',false,0,0,false,'T','C');
			$pdf->SetFont('helvetica', '', 7);
			$pdf->Cell(11,0,'Serial:','L',0,'L',false,0,0,false,'T','C');
			$pdf->Cell(24,0,'',$serialmedidor,0,'L',false,0,0,false,'T','C');
			$pdf->Cell(10,0,'Diametro:','',0,'L',false,0,0,false,'T','C');
			$pdf->Cell(10,0,$diametromedidor,'R',1,'L',false,0,0,false,'T','C');
			$pdf->Cell(11,0,'Marca:','L',0,'L',false,0,0,false,'T','C');
			$pdf->Cell(44,0,$marcamedidor,'R',1,'L',false,0,0,false,'T','C');
			$pdf->Cell(11,0,'Fec/Instac:','L',0,'L',false,0,0,false,'T','C');
			$pdf->Cell(44,0,$fechainsmedidor,'R',1,'L',false,0,0,false,'T','C');
			$pdf->Cell(55,0,'','LR',1,'L',false,0,0,false,'T','C');

			$pdf->SetFont('helvetica', 'B', 8);
			$pdf->Cell(55,0,'CARTERA','LR',1,'L',false,0,0,false,'T','C');
			$pdf->SetFont('helvetica', '', 7);
			$pdf->Cell(25,0,'Facturas Vencidas:','L',0,'L',false,0,0,false,'T','C');
			$pdf->Cell(30,0,$FacturasVencidas,'R',1,'L',false,0,0,false,'T','C');
			$pdf->Cell(55,0,'','LR',1,'L',false,0,0,false,'T','C');
		}
		//cuadro 4
		{
			$pdf->SetFont('helvetica', 'B', 8);
			$pdf->Cell(140,0,'LIQUIDACIÓN DE LA FACTURA','RTL',1,'C',false,0,0,false,'T','C');
			//ACUEDUCTO
			{
				$pdf->Cell(140,0,'ACUEDUCTO','LR',1,'l',false,0,0,false,'T','C');
				$pdf->SetFont('helvetica', '', 7);
				$pdf->Cell(31,0,'Descripción','L',0,'L',false,0,0,false,'T','C');
				$pdf->Cell(15,0,'Tarifa','',0,'C',false,0,0,false,'T','C');
				$pdf->Cell(19,0,'%sub. o cont.','',0,'C',false,0,0,false,'T','C');
				$pdf->Cell(15,0,'Vlr. Sub.','',0,'C',false,0,0,false,'T','C');
				$pdf->Cell(15,0,'Vlr. Contr.','',0,'C',false,0,0,false,'T','C');
				$pdf->Cell(15,0,'Precio','',0,'C',false,0,0,false,'T','C');
				$pdf->Cell(15,0,'Cant.','',0,'C',false,0,0,false,'T','C');
				$pdf->Cell(15,0,'Valor','R',1,'C',false,0,0,false,'T','C');

				$pdf->Cell(31,0,'Cargo Fijo','L',0,'L',false,0,0,false,'T','C');
				$pdf->Cell(15,0,number_format($acu_cf_tarifa,2,".",","),'',0,'R',false,0,0,false,'T','C');
				$pdf->Cell(19,0,number_format($acu_cf_subcont,2,".",","),'',0,'R',false,0,0,false,'T','C');
				$pdf->Cell(15,0,number_format($acu_cf_vlrsub,2,".",","),'',0,'R',false,0,0,false,'T','C');
				$pdf->Cell(15,0,number_format($acu_cf_vlrcontr,2,".",","),'',0,'R',false,0,0,false,'T','C');
				$pdf->Cell(15,0,number_format($acu_cf_precio,2,".",","),'',0,'R',false,0,0,false,'T','C');
				$pdf->Cell(15,0,number_format($acu_cf_cant,0,".",","),'',0,'C',false,0,0,false,'T','C');
				$pdf->Cell(15,0,number_format($acu_cf_valor,2,".",","),'R',1,'R',false,0,0,false,'T','C');

				$pdf->Cell(31,0,'Consumo Básico','L',0,'L',false,0,0,false,'T','C');
				$pdf->Cell(15,0,number_format($acu_cb_tarifa,2,".",","),'',0,'R',false,0,0,false,'T','C');
				$pdf->Cell(19,0,number_format($acu_cb_subcont,2,".",","),'',0,'R',false,0,0,false,'T','C');
				$pdf->Cell(15,0,number_format($acu_cb_vlrsub,2,".",","),'',0,'R',false,0,0,false,'T','C');
				$pdf->Cell(15,0,number_format($acu_cb_vlrcontr,2,".",","),'',0,'R',false,0,0,false,'T','C');
				$pdf->Cell(15,0,number_format($acu_cb_precio,2,".",","),'',0,'R',false,0,0,false,'T','C');
				$pdf->Cell(15,0,number_format($acu_cb_cant,0,".",","),'',0,'C',false,0,0,false,'T','C');
				$pdf->Cell(15,0,number_format($acu_cb_valor,2,".",","),'R',1,'R',false,0,0,false,'T','C');

				$pdf->Cell(31,0,'Consumo Complementario','L',0,'L',false,0,0,false,'T','C');
				$pdf->Cell(15,0,number_format($acu_cc_tarifa,2,".",","),'',0,'R',false,0,0,false,'T','C');
				$pdf->Cell(19,0,number_format($acu_cc_subcont,2,".",","),'',0,'R',false,0,0,false,'T','C');
				$pdf->Cell(15,0,number_format($acu_cc_vlrsub,2,".",","),'',0,'R',false,0,0,false,'T','C');
				$pdf->Cell(15,0,number_format($acu_cc_vlrcontr,2,".",","),'',0,'R',false,0,0,false,'T','C');
				$pdf->Cell(15,0,number_format($acu_cc_precio,2,".",","),'',0,'R',false,0,0,false,'T','C');
				$pdf->Cell(15,0,number_format($acu_cc_cant,0,".",","),'',0,'C',false,0,0,false,'T','C');
				$pdf->Cell(15,0,number_format($acu_cc_valor,2,".",","),'R',1,'R',false,0,0,false,'T','C');
				
				$pdf->Cell(31,0,'Consumo Suntuario','L',0,'L',false,0,0,false,'T','C');
				$pdf->Cell(15,0,number_format($acu_cs_tarifa,2,".",","),'',0,'R',false,0,0,false,'T','C');
				$pdf->Cell(19,0,number_format($acu_cs_subcont,2,".",","),'',0,'R',false,0,0,false,'T','C');
				$pdf->Cell(15,0,number_format($acu_cs_vlrsub,2,".",","),'',0,'R',false,0,0,false,'T','C');
				$pdf->Cell(15,0,number_format($acu_cs_vlrcontr,2,".",","),'',0,'R',false,0,0,false,'T','C');
				$pdf->Cell(15,0,number_format($acu_cs_precio,2,".",","),'',0,'R',false,0,0,false,'T','C');
				$pdf->Cell(15,0,number_format($acu_cs_cant,0,".",","),'',0,'C',false,0,0,false,'T','C');
				$pdf->Cell(15,0,number_format($acu_cs_valor,2,".",","),'R',1,'R',false,0,0,false,'T','C');
				
				$pdf->Cell(31,0,'Tasa de Uso de Agua','L',0,'L',false,0,0,false,'T','C');
				$pdf->Cell(15,0,$acu_tu_tarifa,'',0,'R',false,0,0,false,'T','C');
				$pdf->Cell(19,0,$acu_tu_subcont,'',0,'R',false,0,0,false,'T','C');
				$pdf->Cell(15,0,$acu_tu_vlrsub,'',0,'R',false,0,0,false,'T','C');
				$pdf->Cell(15,0,$acu_tu_vlrcontr,'',0,'R',false,0,0,false,'T','C');
				$pdf->Cell(15,0,$acu_tu_precio,'',0,'R',false,0,0,false,'T','C');
				$pdf->Cell(15,0,$acu_tu_cant,'',0,'C',false,0,0,false,'T','C');
				$pdf->Cell(15,0,$acu_tu_valor,'R',1,'R',false,0,0,false,'T','C');
				
				$pdf->Cell(110,0,'Total Acueducto:','L',0,'R',false,0,0,false,'T','C');
				$pdf->Cell(30,0,number_format($acu_total,2,".",","),1,1,'R',false,0,0,false,'T','C');
			}
			//ALCANTARILLADO
			/*{
				$pdf->SetFont('helvetica', 'B', 8);
				$pdf->Cell(140,0,'ALCANTARILLADO','LR',1,'l',false,0,0,false,'T','C');
				$pdf->SetFont('helvetica', '', 7);
				$pdf->Cell(31,0,'Descripción','L',0,'L',false,0,0,false,'T','C');
				$pdf->Cell(15,0,'Tarifa','',0,'C',false,0,0,false,'T','C');
				$pdf->Cell(19,0,'%sub. o cont.','',0,'C',false,0,0,false,'T','C');
				$pdf->Cell(15,0,'Vlr. Sub.','',0,'C',false,0,0,false,'T','C');
				$pdf->Cell(15,0,'Vlr. Contr.','',0,'C',false,0,0,false,'T','C');
				$pdf->Cell(15,0,'Precio','',0,'C',false,0,0,false,'T','C');
				$pdf->Cell(15,0,'Cant.','',0,'C',false,0,0,false,'T','C');
				$pdf->Cell(15,0,'Valor','R',1,'C',false,0,0,false,'T','C');
				
				$pdf->Cell(31,0,'Cargo Fijo','L',0,'L',false,0,0,false,'T','C');
				$pdf->Cell(15,0,number_format($alc_cf_tarifa,2,".",","),'',0,'R',false,0,0,false,'T','C');
				$pdf->Cell(19,0,number_format($alc_cf_subcont,2,".",","),'',0,'R',false,0,0,false,'T','C');
				$pdf->Cell(15,0,number_format($alc_cf_vlrsub,2,".",","),'',0,'R',false,0,0,false,'T','C');
				$pdf->Cell(15,0,number_format($alc_cf_vlrcontr,2,".",","),'',0,'R',false,0,0,false,'T','C');
				$pdf->Cell(15,0,number_format($alc_cf_precio,2,".",","),'',0,'R',false,0,0,false,'T','C');
				$pdf->Cell(15,0,number_format($alc_cf_cant,0,".",","),'',0,'C',false,0,0,false,'T','C');
				$pdf->Cell(15,0,number_format($alc_cf_valor,2,".",","),'R',1,'R',false,0,0,false,'T','C');

				$pdf->Cell(31,0,'Consumo Básico','L',0,'L',false,0,0,false,'T','C');
				$pdf->Cell(15,0,number_format($alc_cb_tarifa,2,".",","),'',0,'R',false,0,0,false,'T','C');
				$pdf->Cell(19,0,number_format($alc_cb_subcont,2,".",","),'',0,'R',false,0,0,false,'T','C');
				$pdf->Cell(15,0,number_format($alc_cb_vlrsub,2,".",","),'',0,'R',false,0,0,false,'T','C');
				$pdf->Cell(15,0,number_format($alc_cb_vlrcontr,2,".",","),'',0,'R',false,0,0,false,'T','C');
				$pdf->Cell(15,0,number_format($alc_cb_precio,2,".",","),'',0,'R',false,0,0,false,'T','C');
				$pdf->Cell(15,0,number_format($alc_cb_cant,0,".",","),'',0,'C',false,0,0,false,'T','C');
				$pdf->Cell(15,0,number_format($alc_cb_valor,2,".",","),'R',1,'R',false,0,0,false,'T','C');

				$pdf->Cell(31,0,'Consumo Complementario','L',0,'L',false,0,0,false,'T','C');
				$pdf->Cell(15,0,number_format($alc_cc_tarifa,2,".",","),'',0,'R',false,0,0,false,'T','C');
				$pdf->Cell(19,0,number_format($alc_cc_subcont,2,".",","),'',0,'R',false,0,0,false,'T','C');
				$pdf->Cell(15,0,number_format($alc_cc_vlrsub,2,".",","),'',0,'R',false,0,0,false,'T','C');
				$pdf->Cell(15,0,number_format($alc_cc_vlrcontr,2,".",","),'',0,'R',false,0,0,false,'T','C');
				$pdf->Cell(15,0,number_format($alc_cc_precio,2,".",","),'',0,'R',false,0,0,false,'T','C');
				$pdf->Cell(15,0,number_format($alc_cc_cant,0,".",","),'',0,'C',false,0,0,false,'T','C');
				$pdf->Cell(15,0,number_format($alc_cc_valor,2,".",","),'R',1,'R',false,0,0,false,'T','C');
				
				$pdf->Cell(31,0,'Consumo Suntuario','L',0,'L',false,0,0,false,'T','C');
				$pdf->Cell(15,0,number_format($alc_cs_tarifa,2,".",","),'',0,'R',false,0,0,false,'T','C');
				$pdf->Cell(19,0,number_format($alc_cs_subcont,2,".",","),'',0,'R',false,0,0,false,'T','C');
				$pdf->Cell(15,0,number_format($alc_cs_vlrsub,2,".",","),'',0,'R',false,0,0,false,'T','C');
				$pdf->Cell(15,0,number_format($alc_cs_vlrcontr,2,".",","),'',0,'R',false,0,0,false,'T','C');
				$pdf->Cell(15,0,number_format($alc_cs_precio,2,".",","),'',0,'R',false,0,0,false,'T','C');
				$pdf->Cell(15,0,number_format($alc_cs_cant,0,".",","),'',0,'C',false,0,0,false,'T','C');
				$pdf->Cell(15,0,number_format($alc_cs_valor,2,".",","),'R',1,'R',false,0,0,false,'T','C');
				
				$pdf->Cell(31,0,'Tasa Retributiva','L',0,'L',false,0,0,false,'T','C');
				$pdf->Cell(15,0,number_format($alc_tr_tarifa,2,".",","),'',0,'R',false,0,0,false,'T','C');
				$pdf->Cell(19,0,number_format($alc_tr_subcont,2,".",","),'',0,'R',false,0,0,false,'T','C');
				$pdf->Cell(15,0,number_format($alc_tr_vlrsub,2,".",","),'',0,'R',false,0,0,false,'T','C');
				$pdf->Cell(15,0,number_format($alc_tr_vlrcontr,2,".",","),'',0,'R',false,0,0,false,'T','C');
				$pdf->Cell(15,0,number_format($alc_tr_precio,2,".",","),'',0,'R',false,0,0,false,'T','C');
				$pdf->Cell(15,0,number_format($alc_tr_cant,0,".",","),'',0,'C',false,0,0,false,'T','C');
				$pdf->Cell(15,0,number_format($alc_tr_valor,2,".",","),'R',1,'R',false,0,0,false,'T','C');
				
				$pdf->Cell(110,0,'Total Alcantarillado:','L',0,'R',false,0,0,false,'T','C');
				$pdf->Cell(30,0,number_format($alc_total,2,".",","),1,1,'R',false,0,0,false,'T','C');
			}*/
			//ASEO
			{
				$pdf->SetFont('helvetica', 'B', 8);
				$pdf->Cell(140,0,'ASEO','LR',1,'l',false,0,0,false,'T','C');
				$pdf->SetFont('helvetica', '', 7);
				$pdf->Cell(31,0,'Descripción','L',0,'L',false,0,0,false,'T','C');
				$pdf->Cell(15,0,'Tarifa','',0,'C',false,0,0,false,'T','C');
				$pdf->Cell(19,0,'%sub. o cont.','',0,'C',false,0,0,false,'T','C');
				$pdf->Cell(15,0,'Vlr. Sub.','',0,'C',false,0,0,false,'T','C');
				$pdf->Cell(15,0,'Vlr. Contr.','',0,'C',false,0,0,false,'T','C');
				$pdf->Cell(15,0,'Precio','',0,'C',false,0,0,false,'T','C');
				$pdf->Cell(15,0,'Cant.','',0,'C',false,0,0,false,'T','C');
				$pdf->Cell(15,0,'Valor','R',1,'C',false,0,0,false,'T','C');
				
				$pdf->Cell(31,0,'Cargo Fijo','L',0,'L',false,0,0,false,'T','C');
				$pdf->Cell(15,0,number_format($ase_cf_tarifa,2,".",","),'',0,'R',false,0,0,false,'T','C');
				$pdf->Cell(19,0,number_format($ase_cf_subcont,2,".",","),'',0,'R',false,0,0,false,'T','C');
				$pdf->Cell(15,0,number_format($ase_cf_vlrsub,2,".",","),'',0,'R',false,0,0,false,'T','C');
				$pdf->Cell(15,0,number_format($ase_cf_vlrcontr,2,".",","),'',0,'R',false,0,0,false,'T','C');
				$pdf->Cell(15,0,number_format($ase_cf_precio,2,".",","),'',0,'R',false,0,0,false,'T','C');
				$pdf->Cell(15,0,number_format($ase_cf_cant,0,".",","),'',0,'C',false,0,0,false,'T','C');
				$pdf->Cell(15,0,number_format($ase_cf_valor,2,".",","),'R',1,'R',false,0,0,false,'T','C');

				$pdf->Cell(31,0,'CVNA','L',0,'L',false,0,0,false,'T','C');
				$pdf->Cell(15,0,number_format($ase_cv_tarifa,2,".",","),'',0,'R',false,0,0,false,'T','C');
				$pdf->Cell(19,0,number_format($ase_cv_subcont,2,".",","),'',0,'R',false,0,0,false,'T','C');
				$pdf->Cell(15,0,number_format($ase_cv_vlrsub,2,".",","),'',0,'R',false,0,0,false,'T','C');
				$pdf->Cell(15,0,number_format($ase_cv_vlrcontr,2,".",","),'',0,'R',false,0,0,false,'T','C');
				$pdf->Cell(15,0,number_format($ase_cv_precio,2,".",","),'',0,'R',false,0,0,false,'T','C');
				$pdf->Cell(15,0,number_format($ase_cv_cant,0,".",","),'',0,'C',false,0,0,false,'T','C');
				$pdf->Cell(15,0,number_format($ase_cv_valor,2,".",","),'R',1,'R',false,0,0,false,'T','C');
				
				$pdf->Cell(110,0,'Total Aseo:','L',0,'R',false,0,0,false,'T','C');
				$pdf->Cell(30,0,number_format($ase_total,2,".",","),1,1,'R',false,0,0,false,'T','C');
			}
			$pdf->SetFont('helvetica', 'B', 8);
			$pdf->Cell(110,0,'TOTAL:','LB',0,'R',false,0,0,false,'T','C');
			$pdf->Cell(30,0,number_format($total_servicios,2,".",","),1,1,'R',false,0,0,false,'T','C');
		}
		//cuadro 5
		{
			$pdf->ln(14);
			$pdf->Cell(110,0,'',0,1,'R',false,0,0,false,'T','C');
			$pdf->Cell(110,0,'',0,1,'R',false,0,0,false,'T','C');
			$pdf->SetFont('helvetica', 'B', 8);
			$pdf->MultiCell(140,0,'Apreciado Usuario: Si usted no cancela la factura al siguiente dia de la fecha limite de pago se SUSPENDERA EL SERVICIO y se generara cobro por suspensión y reinstalción.',0,'C',false,1,'','',true,0,false,true,19,'T',false);
			$pdf->Cell(140,0,'',0,1,'R',false,0,0,false,'T','C');
			$pdf->Cell(110,0,'',0,1,'R',false,0,0,false,'T','C');
			$pdf->SetFont('helvetica', '', 6);
			$pdf->Cell(140,0,'Vigilada por la Superintendencia de Servicios Publicos Domiciliarios S.S.P.D.',0,1,'C',false,0,0,false,'T','C');
		}
		//cuadro 6
		{
			$pdf->ln(11);
			$pdf->SetFont('helvetica', '', 10);
			$pdf->Cell(25,0,'','LT',0,'L',false,0,0,false,'T','C');
			$pdf->Cell(181,0,'','RT',1,'L',false,0,0,false,'T','C');
			$pdf->Cell(25,0,'Código:','L',0,'L',false,0,0,false,'T','C');
			$pdf->SetFont('helvetica', 'B', 10);
			$pdf->Cell(181,0,$codcliente,'R',1,'L',false,0,0,false,'T','C');
			$pdf->SetFont('helvetica', '', 10);
			$pdf->Cell(25,0,'Suscriptor:','L',0,'L',false,0,0,false,'T','C');
			$pdf->SetFont('helvetica', 'B', 10);
			$pdf->Cell(181,0,substr($nomcliente,0,50),'R',1,'L',false,0,0,false,'T','C');
			$pdf->SetFont('helvetica', '', 10);
			$pdf->Cell(25,0,'Dirección:','L',0,'L',false,0,0,false,'T','C');
			$pdf->SetFont('helvetica', 'B', 10);
			$pdf->Cell(181,0,substr($direccionpredio,0,50),'R',1,'L',false,0,0,false,'T','C');
			$pdf->SetFont('helvetica', '', 10);
			$pdf->Cell(25,0,'Periodo:','L',0,'L',false,0,0,false,'T','C');
			$pdf->SetFont('helvetica', 'B', 10);
			$pdf->Cell(181,0,$periodofac,'R',1,'L',false,0,0,false,'T','C');
			$pdf->Cell(206,0,'','RL',1,'L',false,0,0,false,'T','C');
			$pdf->Cell(206,0,'','RL',1,'L',false,0,0,false,'T','C');
			$pdf->Cell(206,0,'','RL',1,'L',false,0,0,false,'T','C');
			$pdf->Cell(206,0,'','RL',1,'L',false,0,0,false,'T','C');
			$pdf->Cell(206,0,'','RL',1,'L',false,0,0,false,'T','C');
			$pdf->Cell(206,0,'','RL',1,'L',false,0,0,false,'T','C');
			$pdf->Cell(206,0,'Banco Agrario Cuenta Corriente 385200000409','RLB',1,'C',false,0,0,false,'T','C');
			$pdf->write1DBarcode($codtotn, 'C128',50,225,160, 18, 0.25, $style, 'N');
			$pdf->SetY(202+$yy);
			$pdf->SetX(160+$xx);
			$pdf->SetFont('helvetica', 'B', 10);
			$pdf->Cell(40,0,'Factura No','LTR',1,'C',false,0,0,false,'T','C');
			$pdf->SetX(160+$xx);
			$pdf->Cell(40,0,$numfac,'LBR',1,'C',false,0,0,false,'T','C');
			$pdf->SetX(160+$xx);
			$pdf->SetFont('helvetica', '', 7);
			$pdf->Cell(40,0,'Fecha de Emisión '.$fechaimpresion,0,1,'C',false,0,0,false,'T','C');
			$pdf->SetX(160+$xx);
			$pdf->SetFont('helvetica', 'B', 10);
			$pdf->Cell(40,0,'TOTAL A PAGAR','LTR',1,'C',false,0,0,false,'T','C');
			$pdf->SetX(160+$xx);
			$pdf->Cell(40,0,number_format(ceil ($total_factura / 100) * 100,2,".",","),'LBR',1,'C',false,0,0,false,'T','C');
		}
		//cuadro 7
		{
			$datay = array($consumo7,$consumo6,$consumo5,$consumo4,$consumo3,$consumo2,$consumo1);
			$graph = new Graph(350,220,'auto');
			$graph->SetScale("textlin");
			$graph->yaxis->SetTickPositions(array(0,1,5,10,15,20,25,30,35,40,45,50), array(2.5,7.5,12.5,17.5,22.5,27.5,32.5,37.5,42.5,47.5));
			$graph->SetBox(false);
			$graph->ygrid->SetFill(false);
			$graph->xaxis->SetTickLabels(array($mes1,$mes2,$mes3,$mes4,$mes5,$mes6,$mes7));
			$graph->yaxis->HideLine(false);
			$graph->yaxis->HideTicks(false,false);
			$b1plot = new BarPlot($datay);
			$graph->Add($b1plot);
			$b1plot->SetColor("white");
			$b1plot->SetFillGradient("#4B0082","white",GRAD_LEFT_REFLECTION);
			$b1plot->SetWidth(35);
			$grafica = 'graficobarras'.$numfac.'.jpg';
			unlink('informacion/temp/'.$grafica);
			$sArchivo = 'informacion/temp/'.$grafica;
			$graph->Stroke($sArchivo);
			$ruta_relativa = 'informacion/temp/';
			$html = '
			<table cellpadding="1" cellspacing="1" border="0" style="text-align:center;width:70%;">
			<tr><td style="text-align:center;"><img src="'.$ruta_relativa.$grafica.'" border="0" height="120" width="350" align="middle" /></td></tr>
			</table>';
			
			$pdf->SetY(30.3+$yy);
			$pdf->SetX(55+$xx);
			$pdf->SetFont('helvetica', 'B', 7);
			$pdf->Cell(151,0,'DATOS CONSUMOS','RT',1,'C',false,0,0,false,'T','C');
			
			$pdf->SetX(55+$xx);
			$pdf->Cell(151,0,'ACUEDUCTO','R',1,'L',false,0,0,false,'T','C');
			$pdf->SetFont('helvetica', '', 7);
			
			$pdf->SetX(55+$xx);
			$pdf->Cell(40,0,'Novedad de Lectura','',0,'C',false,0,0,false,'T','C');
			$pdf->Cell(37,0,'Lectura Anterior','',0,'C',false,0,0,false,'T','C');
			$pdf->Cell(37,0,'Lectura Actual','',0,'C',false,0,0,false,'T','C');
			$pdf->Cell(37,0,'Consumo (m3)','R',1,'C',false,0,0,false,'T','C');
			$pdf->SetFont('helvetica', 'B', 7);
			
			$pdf->SetX(55+$xx);
			$pdf->Cell(40,0,$observacion,'',0,'L',false,0,0,false,'T','C');
			$pdf->SetFont('helvetica', '', 7);
			$pdf->Cell(37,0,$lectura2,'',0,'C',false,0,0,false,'T','C');
			$pdf->Cell(37,0,$lectura1,'',0,'C',false,0,0,false,'T','C');
			$pdf->Cell(37,0,$consumo1,'R',1,'C',false,0,0,false,'T','C');
			$pdf->Line(206, 43, 206, 97, '');
			
			$pdf->SetY(50+$yy);
			$pdf->SetX(170+$xx);
			$pdf->SetFont('helvetica', '', 7);
			$pdf->Cell(35,0,'Promedio','',1,'C',false,0,0,false,'T','C');
			$pdf->SetX(170+$xx);
			$pdf->Cell(35,0,$resultadoPromedio,'',1,'C',false,0,0,false,'T','C');

			$pdf->SetY(80.3+$yy);
			$pdf->SetX(55+$xx);
			$pdf->SetFont('helvetica', 'B', 7);
			$pdf->Cell(151,0,'ASEO','',1,'L',false,0,0,false,'T','C');
			$pdf->SetX(55+$xx);
			$pdf->SetFont('helvetica', '', 7);
			$pdf->Cell(33,0,'Frecuencia Recolección:','',0,'L',false,0,0,false,'T','C');
			$pdf->Cell(10,0,$frecuencu_rec,'',0,'C',false,0,0,false,'T','C');
			$pdf->Cell(13,0,'CFT:','',0,'L',false,0,0,false,'T','C');
			$pdf->Cell(15,0,number_format($val_cft,2,".",","),'',0,'R',false,0,0,false,'T','C');
			$pdf->Cell(13,0,'TRN:','',0,'L',false,0,0,false,'T','C');
			$pdf->Cell(15,0,number_format($val_trn,5,".",","),'',0,'R',false,0,0,false,'T','C');
			$pdf->Cell(13,0,'TFA:','',0,'L',false,0,0,false,'T','C');
			$pdf->Cell(15,0,number_format($val_tfa,2,".",","),'',0,'R',false,0,0,false,'T','C');
			$pdf->Cell(24,0,'','',1,'L',false,0,0,false,'T','C');
			
			$pdf->SetX(55+$xx);
			$pdf->Cell(33,0,'Frecuencia Barrido:','',0,'L',false,0,0,false,'T','C');
			$pdf->Cell(10,0,$frecuencu_bar,'',0,'C',false,0,0,false,'T','C');
			$pdf->Cell(13,0,'CVNA:','',0,'L',false,0,0,false,'T','C');
			$pdf->Cell(15,0,number_format($val_cvna,2,".",","),'',0,'R',false,0,0,false,'T','C');
			$pdf->Cell(13,0,'TRA:','',0,'L',false,0,0,false,'T','C');
			$pdf->Cell(15,0,number_format($val_tra,2,".",","),'',0,'R',false,0,0,false,'T','C');
			$pdf->Cell(52,0,'','',1,'L',false,0,0,false,'T','C');
			
			$pdf->SetX(55+$xx);
			$pdf->Cell(43,0,'','',0,'L',false,0,0,false,'T','C');
			$pdf->Cell(13,0,'CVA:','',0,'L',false,0,0,false,'T','C');
			$pdf->Cell(15,0,number_format($val_cva,2,".",","),'',0,'R',false,0,0,false,'T','C');
			$pdf->Cell(13,0,'TFN:','',0,'L',false,0,0,false,'T','C');
			$pdf->Cell(15,0,number_format($val_tfn,2,".",","),'',0,'R',false,0,0,false,'T','C');
			$pdf->Cell(52,0,'','',1,'L',false,0,0,false,'T','C');
			
			$pdf->SetY(43.3+$yy);
			$pdf->SetX(55+$xx);
			$pdf->writeHTML($html, 1, 0, 1, 0, '');
			$pdf->SetX(55+$xx);
		}
		//cuadro 8
		{
			$pdf->SetY(96.9+$yy);
			$pdf->SetX(140+$xx);
			$pdf->SetFont('helvetica', 'B', 7);
			$pdf->Cell(66,0,'ACUERDO DE PAGO','RTL',1,'C',false,0,0,false,'T','C');
			$pdf->SetX(140+$xx);
			$pdf->SetFont('helvetica', '', 7);
			$pdf->Cell(20,0,'Valor: ','L',0,'L',false,0,0,false,'T','C');
			$pdf->Cell(46,0,number_format($valor_ap,2,".",","),'R',1,'R',false,0,0,false,'T','C');
			$pdf->SetX(140+$xx);
			$pdf->Cell(20,0,'Saldo: ',0,0,'L',false,0,0,false,'T','C');
			$pdf->Cell(46,0,number_format($saldo_ap,2,".",","),'R',1,'R',false,0,0,false,'T','C');
			$pdf->SetX(140+$xx);
			$pdf->Cell(66,0,'','RB',1,'C',false,0,0,false,'T','C');
			
			$pdf->SetX(140+$xx);
			$pdf->SetFont('helvetica', 'B', 7);
			$pdf->Cell(66,0,'OTROS COBROS','R',1,'C',false,0,0,false,'T','C');
			$pdf->SetX(140+$xx);
			$pdf->SetFont('helvetica', '', 7);
			$pdf->Cell(40,0,'CONCEPTO',0,0,'C',false,0,0,false,'T','C');
			$pdf->Cell(26,0,'VALOR','R',1,'C',false,0,0,false,'T','C');
			$pdf->SetX(140+$xx);
			$pdf->Cell(40,0,$concepto_otros[0],0,0,'L',false,0,0,false,'T','C');
			$pdf->Cell(26,0,number_format($valor_otros[0],2,".",","),'R',1,'R',false,0,0,false,'T','C');
			$pdf->SetX(140+$xx);
			$pdf->Cell(40,0,$concepto_otros[1],0,0,'L',false,0,0,false,'T','C');
			$pdf->Cell(26,0,number_format($valor_otros[1],2,".",","),'R',1,'R',false,0,0,false,'T','C');
			$pdf->SetX(140+$xx);
			$pdf->Cell(40,0,$concepto_otros[2],0,0,'L',false,0,0,false,'T','C');
			$pdf->Cell(26,0,number_format($valor_otros[2],2,".",","),'R',1,'R',false,0,0,false,'T','C');
			$pdf->SetX(140+$xx);
			$pdf->Cell(40,0,$concepto_otros[3],0,0,'L',false,0,0,false,'T','C');
			$pdf->Cell(26,0,number_format($valor_otros[3],2,".",","),'R',1,'R',false,0,0,false,'T','C');
			$pdf->SetX(140+$xx);
			$pdf->Cell(40,0,$concepto_otros[4],0,0,'L',false,0,0,false,'T','C');
			$pdf->Cell(26,0,number_format($valor_otros[4],2,".",","),'R',1,'R',false,0,0,false,'T','C');
			$pdf->SetX(140+$xx);
			$pdf->Cell(40,0,$concepto_otros[5],0,0,'L',false,0,0,false,'T','C');
			$pdf->Cell(26,0,number_format($valor_otros[5],2,".",","),'R',1,'R',false,0,0,false,'T','C');
			$pdf->SetX(140+$xx);
			$pdf->Cell(40,0,$concepto_otros[6],0,0,'L',false,0,0,false,'T','C');
			$pdf->Cell(26,0,number_format($valor_otros[6],2,".",","),'R',1,'R',false,0,0,false,'T','C');
			$pdf->SetX(140+$xx);
			$pdf->Cell(40,0,$concepto_otros[7],0,0,'L',false,0,0,false,'T','C');
			$pdf->Cell(26,0,number_format($valor_otros[7],2,".",","),'R',1,'R',false,0,0,false,'T','C');
			$pdf->SetX(140+$xx);
			$pdf->Cell(40,0,$concepto_otros[8],0,0,'L',false,0,0,false,'T','C');
			$pdf->Cell(26,0,number_format($valor_otros[8],2,".",","),'R',1,'R',false,0,0,false,'T','C');
			$pdf->SetX(140+$xx);
			$pdf->Cell(40,0,$concepto_otros[9],0,0,'L',false,0,0,false,'T','C');
			$pdf->Cell(26,0,number_format($valor_otros[9],2,".",","),'R',1,'R',false,0,0,false,'T','C');
			$pdf->SetX(140+$xx);
			$pdf->SetFont('helvetica', 'B', 7);
			$pdf->Cell(40,0,'Total Otros','B',0,'l',false,0,0,false,'T','C');
			$pdf->Cell(26,0,number_format($saldosOtrosPagos,2,".",","),'RB',1,'R',false,0,0,false,'T','R');
			$pdf->SetX(140+$xx);
			$pdf->SetFont('helvetica', 'B', 12);
			$pdf->Cell(66,0,'PAGUE HASTA','RL',1,'C',false,0,0,false,'T','C');
			$pdf->SetX(140+$xx);
			$pdf->Cell(66,0,$fecha_limite,'RBL',1,'C',false,0,0,false,'T','R');
			$pdf->SetX(140+$xx);
			$pdf->SetFont('helvetica', 'B', 12);
			$pdf->Cell(66,0,'TOTAL A PAGAR','RL',1,'C',false,0,0,false,'T','C');
			$pdf->SetX(140+$xx);
			$pdf->Cell(66,0,number_format(ceil ($total_factura / 100) * 100,2,".",","),'RBL',1,'C',false,0,0,false,'T','R');
			$pdf->SetY(171 +$yy);
			$pdf->SetX(150+$xx);
			$pdf->SetFont('helvetica', '', 6);
			$pdf->MultiCell(45,0,'La presente factura presta méritoejecutivo Art. 130 Ley 142/94',0,'C',false,0,'','',true,0,false,true,19,'T',false);
			$pdf->Image('imagenes/firma.png',150,174,40,18);
			$pdf->SetY(188 +$yy);
			$pdf->SetX(152+$xx);
			$pdf->SetFont('helvetica', 'B', 8);
			$pdf->MultiCell(40,0,'Lasides Merardo Tovar Ortiz Gerente',0,'C',false,0,'','',true,0,false,true,19,'T',false);


			$concepto_otros[0] = '';
			$valor_otros[0] = 0;
			$concepto_otros[1] = '';
			$valor_otros[1] = 0;
			$concepto_otros[2] = '';
			$valor_otros[2] = 0;
			$concepto_otros[3] = '';
			$valor_otros[3] = 0;
			$concepto_otros[4] = '';
			$valor_otros[4] = 0;
			$concepto_otros[5] = '';
			$valor_otros[5] = 0;
			$concepto_otros[6] = '';
			$valor_otros[6] = 0;
			$concepto_otros[7] = '';
			$valor_otros[7] = 0;
			$concepto_otros[8] = '';
			$valor_otros[8] = 0;
			$concepto_otros[9] = '';
			$valor_otros[9] = 0;
			$saldosOtrosPagos = 0;
		}
	}
	$pdf->Output('Listadofacturas.pdf', 'I');
?>