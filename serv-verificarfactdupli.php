<?php
	require"comun.inc";
	require"funciones.inc";
	require"serviciospublicos.inc";
	session_start();
	$linkbd=conectar_bd();	
	cargarcodigopag($_GET[codpag],$_SESSION["nivel"]);
	header("Cache-control: private"); // Arregla IE 6
	header("Content-Type: text/html;charset=ISO-8859-1");
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es">
	<head>
	 	<meta http-equiv="Content-type" content="text/html; charset=ISO-8859-1"/>
        <meta http-equiv="X-UA-Compatible" content="IE=9"/>
		<title>:: SPID - Servicios Publicos</title>
        <link href="css/css2.css" rel="stylesheet" type="text/css" />
        <link href="css/css3.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="css/programas.js"></script>
		<script type="text/javascript" src="css/calendario.js"></script>
		<script>
			function callprogress(vValor)
			{
 				document.getElementById("getprogress").innerHTML = vValor;
 				document.getElementById("getProgressBarFill").innerHTML = '<div class="ProgressBarFill" style="width: '+vValor+'%;"></div>';				
				document.getElementById("titulog1").style.display='block';
   				document.getElementById("progreso").style.display='block';
     			document.getElementById("getProgressBarFill").style.display='block';
				if (vValor==100){document.getElementById("titulog2").style.display='block';}
			} 
			function buscarsubsidios()
			{
				document.form2.oculto.value='1';
 				document.form2.submit();
			}
			function abrirexcel()
			{
				if(document.form2.servicios.value!="" && document.form2.estrato.value!="" && document.form2.estrato.value!="" && document.form2.vigencias.value!="")
				{
					//document.form2.action="serv-reportetercerosubsexcel01.php";
				}
				document.form2.target="_BLANK";
				document.form2.submit(); 
				document.form2.action="";
				document.form2.target="";
			}
		</script>
	</head>
	<body>
		<IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
		<span id="todastablas2"></span>
		<table>
			<tr><script>barra_imagenes("serv");</script><?php cuadro_titulos();?></tr>	 
			<tr><?php menu_desplegable("serv");?></tr>
			<tr>
  				<td colspan="3" class="cinta"><img src="imagenes/add.png" title="Nuevo" onClick="location.href='serv-reportetercerosubs.php'" class="mgbt"/><img src="imagenes/guardad.png" class="mgbt"/><img src="imagenes/buscad.png" class="mgbt"/></a><img src="imagenes/nv.png" title="Nueva Ventana"  onClick="mypop=window.open('serv-principal.php','','');mypop.focus();" class="mgbt"><img src="imagenes/csv.png"  title="Excel" onClick="abrirexcel()" class="mgbt"><a href="#" onClick="pdf()"><img src="imagenes/print.png" alt="imprimir"></a></td>
         	</tr>	
		</table>
 		<form name="form2" method="post" action="">
			<table  class="inicio" style="width:99.7%;" >
      			<tr>
        			<td class="titulos" colspan="10">:. Buscar Detalles de Facturacion Duplicados </td>
        			<td class="cerrar" style="width:7%" onClick="location.href='serv-principal.php'">Cerrar</td>
      			</tr>
   				<tr>
        			<td class="saludo1">Servicio:</td>
        			<td>
        				<select name="servicios">
    		  				<option value="">::: Seleccione Servicio ::: </option>
							<?php
								$sqlr="select * from servservicios ";
								$res=mysql_query($sqlr,$linkbd);
								while ($row =mysql_fetch_row($res)) 
				    			{
					 				if(0==strcmp($row[0],$_POST[servicios]))
									{
										echo "<option value='$row[0]' SELECTED>$row[0] - $row[1]</option>";
										$_POST[nomservi]=$row[1];
									}
									else {echo "<option value='$row[0]'>$row[0] - $row[1]</option>";}
								}	 	
							?>
   						</select>
        			</td>
         			<td class="saludo1">Estrato: </td>
                    <td>
                        <select name="estrato" id="select_estrato"> 
                            <option value="">::: Seleccione Estrato ::: </option>
                            <option value="11" <?php if($_POST[estrato]=="11"){$_POST[estralet]="UNO"; echo "SELECTED";}?>>11 - UNO </option>
                            <option value="12" <?php if($_POST[estrato]=="12"){$_POST[estralet]="DOS"; echo "SELECTED";}?>>12 - DOS </option>
                            <option value="13" <?php if($_POST[estrato]=="13"){$_POST[estralet]="TRES"; echo "SELECTED";}?>>13 - TRES </option>
                        </select>
         			</td>  
                    <td class="saludo1">Vigencia:</td>
        			<td>
        				<select name="vigencias" onChange="document.form2.submit();">
    		  				<option value="">::: Seleccione Vigencia ::: </option>
							<?php
								$sqlr="SELECT DISTINCT vigencia FROM servliquidaciones_gen ";
								$res=mysql_query($sqlr,$linkbd);
								while ($row =mysql_fetch_row($res)) 
				    			{
					 				if(0==strcmp($row[0],$_POST[vigencias]))
									{
										echo "<option value='$row[0]' SELECTED>$row[0]</option>";
									}
									else {echo "<option value='$row[0]'>$row[0]</option>";}
								}	 	
							?>
   						</select>
        			</td>
           		</tr>
               	<tr>
                	<td class="saludo1">N&deg; Liquidaci&oacute;</td>
        			<td>
        				<select name="nliquida">
    		  				<option value="">::: Seleccione Vigencia ::: </option>
							<?php
								if($_POST[vigencias]!=""){$critli="WHERE vigencia='$_POST[vigencias]'";}
								else{$critli="";}
								$sqlr="SELECT id_cab,mes,mesfin FROM servliquidaciones_gen $critli ORDER BY id_cab";
								$res=mysql_query($sqlr,$linkbd);
								while ($row =mysql_fetch_row($res)) 
				    			{
									if($row[1]==$row[2]){$_POST[mesletra]=mesletras($row[1]);}
									else {$_POST[mesletra]=mesletras($row[1])." a ".mesletras($row[2]);}
					 				if(0==strcmp($row[0],$_POST[nliquida]))
									{
										echo "<option value='$row[0]' SELECTED>$row[0] - $_POST[mesletra]</option>";
										
									}
									else {echo "<option value='$row[0]'>$row[0] - $_POST[mesletra]</option>";}
								}	 	
							?>
   						</select>
        			</td>
        			<td><input type="button" name="buscar" id="buscar" value="  Buscar  " onClick="buscarsubsidios();"></td>  
                    <td>
                    	<div id='titulog1' style="display:none; float:left"></div>
                        <div id='progreso' class='ProgressBar' style="display:none; float:left">
                            <div class='ProgressBarText'><span id='getprogress'></span>&nbsp;% </div>
                            <div id='getProgressBarFill'></div>
                        </div>
                    </td>  
        		</tr>                      
    		</table>     
            <input type="hidden" name="oculto" id="oculto" value="0"/>
    		<div class="subpantalla" style="height:64.5%; width:99.5%; overflow-x:hidden;">
      			<?php
	  				if($_POST[oculto]=='1')
					{
						
						$sqlrb="TRUNCATE servlistasubsidiost";	
						mysql_query($sqlrb,$linkbd); 
						$_POST[totarifa]=$_POST[tosubsid]=0;
						if($_POST[servicios]!=""){$crit1="AND T1.servicio='$_POST[servicios]'";}
						else{$crit1="";}
						if($_POST[estrato]!=""){$crit2="AND T1.estrato='$_POST[estrato]'";}
						else{$crit2="";}
						if($_POST[vigencias]!=""){$crit3="AND T2.vigencia='$_POST[vigencias]'";}
						else{$crit3="";}
						if($_POST[nliquida]!=""){$crit4="AND T2.liquidaciongen='$_POST[nliquida]'";}
						else{$crit4="";}
						$sqlr="SELECT T1.codusuario,T1.servicio,T1.estrato,T3.codcatastral,T3.nombretercero,T3.direccion,T1.tarifa, T1.subsidio,T1.id_liquidacion,T2.vigencia,T2.mes,T2.mesfin,T2.liquidaciongen 
						FROM servliquidaciones_det T1,servliquidaciones T2,servclientes T3  
						WHERE T1.id_liquidacion=T2.id_liquidacion AND T1.codusuario=T3.codigo $crit1 $crit2 $crit3 $crit4 
						GROUP BY T1.codusuario
						HAVING COUNT( T1.codusuario ) >1
						ORDER BY CAST(T1.codusuario AS UNSIGNED)";
						
						$resp = mysql_query($sqlr,$linkbd);
						$totalcli=mysql_affected_rows ($linkbd);
						$cliq=0;
						$ntr = mysql_num_rows($resp);
						echo"
						<table class='inicio'>
							<tr><td colspan='13' class='titulos'>Total: $ntr</td></tr>
							<tr>
								<td class='titulos2'>Cod. Cliente</td>
								<td class='titulos2'>Servicio</td>
								<td class='titulos2'>Estrato</td>
								<td class='titulos2'>Cod. Catastral</td>
								<td class='titulos2'>Cliente</td>
								<td class='titulos2'>Direcci&oacute;n</td>
								<td class='titulos2' style='width:7%'>Tarifa</td>
								<td class='titulos2' style='width:7%'>Subsidio</td>
								<td class='titulos2' style='width:7%'>Valor Real</td>
								<td class='titulos2'>Factura</td>
								<td class='titulos2'>Vigencia</td>
								<td class='titulos2'>Mes</td>
								<td class='titulos2' style='width:5%'>N&deg; Liqui.</td>
							<tr>";
						$iter='saludo1a';
						$iter2='saludo2';
						while ($row =mysql_fetch_row($resp)) 
 						{
							$cliq+=1;
							$porcentaje = $cliq * 100 / $totalcli; 
							echo"<script>progres='".round($porcentaje)."';callprogress(progres);</script>"; 
							flush(); 
							ob_flush();
							usleep(5);
							if($row[10]==$row[11]){$varmeses=mesletras($row[10]);}
							else {$varmeses=mesletras($row[10])." a ".mesletras($row[11]);}
							$_POST[totarifa]+=$row[6];
							$_POST[tosubsid]+=$row[7];
							echo"
							<tr class='$iter'>
								<td>$row[0]</td>
								<td>$row[1]</td>
								<td>$row[2]</td>
								<td>$row[3]</td>
								<td>".utf8_decode ($row[4])."</td>
								<td>".utf8_decode ($row[5])."</td>
								<td style='text-align:right;'>$ ".number_format($row[6],2,'.',',')."</td>
								<td style='text-align:right;'>$ ".number_format($row[7],2,'.',',')."</td>
								<td style='text-align:right;'>$ ".number_format(($row[6]-$row[7]),2,'.',',')."</td>
								<td>&nbsp;$row[8]&nbsp;</td>
								<td>&nbsp;$row[9]</td>
								<td>$varmeses</td>
								<td>$row[12]</td>
							</tr>";
							$aux=$iter;
	 						$iter=$iter2;
	 						$iter2=$aux;
							$sqlrit="INSERT INTO servlistasubsidiost (cliente,servicio,estrato,codcatastral,nomcliente,direccion,tarifa,subsidio, valorreal,factura,vigencia,mes,liquidacion) VALUES ('$row[0]','$row[1]','$row[2]','$row[3]','".utf8_decode ($row[4])."','".utf8_decode ($row[5])."','$row[6]','$row[7]','".($row[6]-$row[7])."','$row[8]','$row[9]','$varmeses','$row[12]')";	
							mysql_query($sqlrit,$linkbd);
						}
						echo"
							<tr class='titulos2'>
								<td style='text-align:right;' colspan='8'>Total Subsidiado::</td>
								<td style='text-align:right;'>$ ".number_format($_POST[tosubsid],0,',','.')."</td>
								<td style='text-align:right;'>$ ".number_format($_POST[tosubsid],0,',','.')."</td>
								<td></td>
							</tr>
						</table>";
					}
				?>
           </div>
           <input type="hidden" name="totarifa" id="totarifa" value="<?php echo $_POST[totarifa];?>"/>
           <input type="hidden" name="tosubsid" id="tosubsid" value="<?php echo $_POST[tosubsid];?>"/>
           <input type="hidden" name="nomservi" id="nomservi" value="<?php echo $_POST[nomservi];?>"/>
           <input type="hidden" name="nomestra" id="nomestra" value="<?php echo $_POST[nomestra];?>"/>
           <input type="hidden" name="mesletra" id="mesletra" value="<?php echo $_POST[mesletra];?>"/>
           <input type="hidden" name="estralet" id="estralet" value="<?php echo $_POST[estralet];?>"/>
		</form>
	</body>
</html>