<?php
	require "comun.inc";
	require "funciones.inc";
	require "conversor.php";
	require "validaciones.inc";
	session_start();
	$linkbd=conectar_bd();	
	cargarcodigopag($_GET[codpag],$_SESSION["nivel"]);
	header("Cache-control: private"); // Arregla IE 6
	header("Content-Type: text/html;charset=iso-8859-1");
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es">
	<head>
	 	<meta http-equiv="Content-type" content="text/html; charset=iso-8859-1"/>
        <meta http-equiv="X-UA-Compatible" content="IE=9"/>
		<title>:: SPID - Servicios P&uacute;blicos</title>
        <link href="css/css2.css" rel="stylesheet" type="text/css" />
        <link href="css/css3.css" rel="stylesheet" type="text/css" />
		<script type="text/javascript" src="css/programas.js"></script>
		<script>
			function fbuscar()
			{
				document.getElementById('oculto').value='3';
				document.form2.submit();
			}
			function fimprimir()
			{
				document.form2.action="serv-reportegeneralsubsipdf.php";
				document.form2.target="_BLANK";
				document.form2.submit(); 
				document.form2.action="";
				document.form2.target="";
			}
			function retornomenu(){location.href="serv-menureportes.php";}
		</script>
		<?php titlepag(); ?>
	</head>
	<body>
		<IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
		<span id="todastablas2"></span>
		<table>
			<tr><script>barra_imagenes("serv");</script><?php cuadro_titulos();?></tr>	 
			<tr><?php menu_desplegable("serv");?></tr>
			<tr>
  				<td colspan="3" class="cinta"><img src="imagenes/add2.png" title="Nuevo" onClick="location.href='serv-reportegeneralfacrec.php'" class="mgbt"/><img src="imagenes/guardad.png" class="mgbt1"/><img src="imagenes/buscad.png" class="mgbt1"/><img src="imagenes/nv.png" title="Nueva Ventana" onClick="mypop=window.open('serv-principal.php','','');mypop.focus();" class="mgbt"/><img src="imagenes/print.png" title="Imprimir" onClick="fimprimir()" class="mgbt"/><img src='imagenes/iratras.png' title="Men&uacute; Nomina" onClick="retornomenu()" class='mgbt'></td>
			</tr>		  
		</table>
        <div id="bgventanamodalm" class="bgventanamodalm">
            <div id="ventanamodalm" class="ventanamodalm">
                <IFRAME src="" name="ventanam" marginWidth=0 marginHeight=0 frameBorder=0 id="ventanam" frameSpacing=0 style=" width:700px; height:130px; top:200; overflow:hidden;"> 
                </IFRAME>
            </div>
        </div>
        <form name="form2" method="post" action="">
			<table class="inicio">
                <tr>
                    <td class="titulos" colspan="9">Reporte General Subsidios</td>
                    <td class="cerrar" style='width:7%'><a onClick="location.href='cont-principal.php'">&nbsp;Cerrar</a></td>
                </tr>
                <tr>
                	 <td class="saludo1" style="width:2cm;">vigencia:</td> 
                     <td style="width:8%">
                     	<select name="vigencia" id="vigencia" onChange="document.form2.submit();" style="width:100%">
                        	<option value="" >....</option>
                            <?php
                                $sqlr="SELECT DISTINCT vigencia FROM servfacturas ORDER BY vigencia DESC";
                                $resp = mysql_query($sqlr,$linkbd);
                                while ($row =mysql_fetch_row($resp)) 
                                {
                                    if("$row[0]"==$_POST[vigencia]){echo "<option value='$row[0]' SELECTED>$row[0]</option>";}
                                    else {echo "<option value='$row[0]'>$row[0]</option>";}	  
                                }
                            ?>
                        </select>
                     </td>  
                	<td class="saludo1" style="width:2.5cm;">Mes Inicial:</td> 
                     <td style="width:12%">
                     	<select name="mesini" id="mesini" onChange="document.form2.submit();" style="width:100%">
                        	<option value="" >....</option>
                            <?php
                                for ( $i = 1 ; $i <= 12 ; $i ++) 
								{
									 if("$i"==$_POST[mesini]){echo "<option value='$i' SELECTED>".mesletras($i)."</option>";}
									  else {echo "<option value='$i'>".mesletras($i)."</option>";}	
								}
                            ?>
                        </select>
                   	</td>
                    <td class="saludo1" style="width:2.5cm;" >Mes final:</td> 
                     <td style="width:12%">
                     	<select name="mesfin" id="mesfin" style="width:100%">
                        	<option value="" >....</option>
                            <?php
								$sy=$_POST[mesini];
								if ($sy==""){$sy=1;}
                               	for ( $i = $sy ; $i <= 12 ; $i ++) 
								{
									 if("$i"==$_POST[mesfin]){echo "<option value='$i' SELECTED>".mesletras($i)."</option>";}
									  else {echo "<option value='$i'>".mesletras($i)."</option>";}	
								}
                            ?>
                        </select>
                   	</td>
                    <td class="saludo1" style="width:3.5cm;" >Centro de Costos:</td> 
                     <td style="width:12%">
                     	<select name="cdc" id="cdc" style="width:100%">
                        	<option value="" >Todos</option>
                            <?php
                               	$sqlr="SELECT * FROM servservicios";
                                $resp = mysql_query($sqlr,$linkbd);
                                while ($row =mysql_fetch_row($resp)) 
                                {
                                    if("$row[0]"==$_POST[cdc]){echo "<option value='$row[0]' SELECTED>$row[1]</option>";}
                                    else {echo "<option value='$row[0]'>$row[1]</option>";}	  
                                }
                            ?>
                        </select>
                   	</td>
                    <td>&nbsp;<input type="button" name="buscar" id="bbuscar" value="  BUSCAR  " onClick="fbuscar();"/> </td>
                </tr>
			</table>
            <input type="hidden" name="oculto" id="oculto" value="1"/>
			<input type="hidden" name="tirespuesta" id="tirespuesta" value="<?php echo $_POST[tirespuesta]?>">
    		<div class="subpantalla" style="height:68.5%; width:99.6%; overflow-x:hidden;">
				<?php
					if ($_POST[oculto]=="3")
					{
						$crips01="";
						if($_POST[cdc]!=""){$crips01="AND T2.servicio='$_POST[cdc]'";}
						$sqlr="SELECT T1.mes,T2.servicio,T1.fecha,T1.mesfin,SUM(T2.subsidio) FROM servliquidaciones T1, servliquidaciones_det T2 WHERE  T1.id_liquidacion=T2.id_liquidacion AND T1.vigencia='$_POST[vigencia]' AND (CAST(T1.mes as UNSIGNED) BETWEEN $_POST[mesini] AND $_POST[mesfin]) $crips01 GROUP BY T1.mes,T2.servicio ORDER BY CAST(T1.mes as UNSIGNED)";
						$resp = mysql_query($sqlr,$linkbd);
						$row=mysql_num_rows($resp);
						echo"
						<table class='inicio' align='center' width='99%'>
							<tr><td colspan='8' class='titulos'>.: Resultados Busqueda:</td></tr>
							<tr><td colspan='8'>Terceros Encontrados: </td></tr>
							<tr>
								<td class='titulos2' width='8%'>VIGENCIA</td>
								<td class='titulos2' width='10%'>MES</td>
								<td class='titulos2' width='10%'>FECHA IMPRESION</td>
								<td class='titulos2' width='25%'>SERVICIO</td>
								<td class='titulos2' width='14%'>VALOR SUBSIDIO</td>
								<td class='titulos2'></td>
							</tr>";	
						$iter='saludo1a';
						$iter2='saludo2';
						$totalfac=0;
						$totalfac1=0;
						$totalfacg=0;
						$totaldeuda=0;
						$ti01=0;
						$ti02="";
						while ($row =mysql_fetch_row($resp)) 
						{
							
							$totalfac1+=round($row[4], 0, PHP_ROUND_HALF_UP);
							if($row[0]!=$row[3]){$mesfac=mesletras($row[0])." a ".mesletras($row[3]);}
							else{$mesfac=mesletras($row[0]);}                                   
							$sqlrsv="SELECT nombre FROM servservicios WHERE codigo='$row[1]'";
							$rowsv =mysql_fetch_row(mysql_query($sqlrsv,$linkbd));
							echo"
							<tr class='$iter' style='text-transform:uppercase; $estilo' >
								<td>$_POST[vigencia]</td>
								<td>$mesfac</td>
								<td>$row[2]</td>
								<td>$rowsv[0]</td>
								<td style='text-align:right;'>$ ".number_format($row[4],0,',','.')."</td>
								<td></td>
							</tr>";
							$aux=$iter;
							$iter=$iter2;
							$iter2=$aux;
						}
							echo"
							<tr class='titulos2'>
								<td style='text-align:right;' colspan='4'>Totales :</td>
								<td style='text-align:right;'>$ ".number_format($totalfac1,0,',','.')."</td>
								<td colspan='3'></td>
							</tr>
							</table>";
					}	
				?>
			</div>
		</form>
	</body>
</html>