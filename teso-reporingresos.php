<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");

	require "comun.inc";
	require "funciones.inc";
	require "conversor.php";
	require "validaciones.inc";

	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");

	session_start();
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE > 
<html lang="es">
	<head>
		<meta charset="utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1"/>
		<meta name="viewport" content="user-scalable=no">
		<title>:: IDEAL 10 - Tesorer&iacute;a</title>
		<link href="favicon.ico" rel="shortcut icon"/>
		<script type="text/javascript" src="jquery-1.11.0.min.js"></script>
		<script>
			function verep(idfac){
				document.form1.oculto.value=idfac;
				document.form1.submit();
			}
			function genrep(idfac){
				document.form2.oculto.value=idfac;
				document.form2.submit();
			}
			function buscacta(e){
				if (document.form2.cuenta.value!=""){
					document.form2.bc.value = '1';
					document.form2.submit();
				}
			}
			function validar(){
				document.form2.submit();
			}
			function buscater(e){
				if (document.form2.tercero.value!=""){
					document.form2.bt.value='1';
					document.form2.submit();
				}
			}
			function agregardetalle(){
				if(document.form2.numero.value!="" &&  document.form2.valor.value>0 &&  document.form2.banco.value!=""  ){ 
					document.form2.agregadet.value=1;
					//document.form2.chacuerdo.value=2;
					document.form2.submit();
				}else {
					alert("Falta informacion para poder Agregar");
				}
			}
			//************* genera reporte ************
			//***************************************
			function eliminar(idr){
				if (confirm("Esta Seguro de Eliminar el Recibo de Caja")){
					document.form2.oculto.value=2;
					document.form2.var1.value=idr;
					document.form2.submit();
				}
			}
			function guardar(){

				if (document.form2.fecha.value!='')
				{
					if (confirm("Esta Seguro de Guardar")){
						document.form2.oculto.value=2;
						document.form2.submit();
					}
				}else{
					alert('Faltan datos para completar el registro');
					document.form2.fecha.focus();
					document.form2.fecha.select();
				}
			}
			function pdf(){
				document.form2.action="teso-pdfconsignaciones.php";
				document.form2.target="_BLANK";
				document.form2.submit(); 
				document.form2.action="";
				document.form2.target="";
			}

			$(window).load(function () {
				$('#cargando').hide();
			});
			function excell() {
				document.form2.action = "teso-reporteingresosexcel.php";
				document.form2.target = "_BLANK";
				document.form2.submit();
				document.form2.action = "";
				document.form2.target = "";
			}
		</script>
		<script src="css/programas.js"></script>
		<script src="css/calendario.js"></script>
		<link href="css/css2.css" rel="stylesheet" type="text/css" />
		<link href="css/css3.css" rel="stylesheet" type="text/css" />

	</head>
	<body>
		<div id="cargando" style=" position:absolute;left: 46%; bottom: 45%">
			<img src="imagenes/loading.gif" style=" width: 80px; height: 80px"/>
		</div>
		<IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
		<span id="todastablas2"></span>
		<table>
			<tr><script>barra_imagenes("teso");</script><?php cuadro_titulos();?></tr>	 
			<tr><?php menu_desplegable("teso");?></tr>
			<tr>
				<td colspan="3" class="cinta">
					<a href="teso-recibocaja.php" class="mgbt"><img src="imagenes/add.png" alt="Nuevo" title="Nuevo"/></a>
					<a href="#" class="mgbt"><img src="imagenes/guardad.png" alt="Guardar" title="Guardar"/> 
					<a onClick="document.form2.submit();" href="#" class="mgbt"><img src="imagenes/busca.png" alt="Buscar" title="Buscar" /></a>
					<a href="#" onClick="mypop=window.open('teso-principal.php','','');mypop.focus();" class="mgbt"><img src="imagenes/nv.png" alt="nueva ventana" title="Nueva Ventana"></a>
					<a href="<?php echo "archivos/".$_SESSION['usuario']."-reporteingresos.csv"; ?>" download="<?php echo "archivos/".$_SESSION['usuario']."-reporteingresos.csv"; ?>" target="_blank" class="mgbt"><img src="imagenes/csv.png"  alt="csv" title="csv"></a>
					<img src="imagenes/excel.png" title="Excel" onClick='excell()' class="mgbt">
					<a href="teso-informestesoreria.php" class="mgbt"><img src="imagenes/iratras.png" title="Atr&aacute;s"></a>
				</td>
			</tr>
		</table>
		<form name="form2" method="post" action="teso-reporingresos.php">
			<table class="inicio" align="center" >
				<tr>
					<td class="titulos" colspan="6">:. Reporte de ingresos</td>
					<td width="70" class="cerrar" ><a href="teso-principal.php">Cerrar</a></td>
				</tr>
				<tr>
					<td class="saludo1">Fecha Inicial:</td>
					<td><input type="hidden" value="<?php echo $ $vigusu ?>" name="vigencias"><input id="fc_1198971545" title="DD/MM/YYYY" onchange="" name="fecha" type="text" value="<?php echo $_POST['fecha']?>" maxlength="10" size="10" onKeyPress="javascript:return solonumeros(event)" onKeyUp="return tabular(event,this)" onKeyDown="mascara(this,'/',patron,true)"><a href="#" onClick="displayCalendarFor('fc_1198971545');"><img src="imagenes/calendario04.png" style="width:20px" align="absmiddle" border="0"></a></td>
					<td class="saludo1">Fecha Final: </td>
					<td ><input id="fc_1198971546" title="DD/MM/YYYY" name="fecha2" type="text" value="<?php echo $_POST['fecha2']?>" maxlength="10" size="10" onKeyPress="javascript:return solonumeros(event)" onKeyUp="return tabular(event,this)" onchange="" onKeyDown="mascara(this,'/',patron,true)">   
					<a href="#" onClick="displayCalendarFor('fc_1198971546');"><img src="imagenes/calendario04.png" style="width:20px" align="absmiddle" border="0"></a> 
					<input type="button" name="generar" value="Generar" onClick="document.form2.submit()">
					<input name="oculto" type="hidden" value="1"><input name="var1" type="hidden" value=<?php echo $_POST['var1'];?>></td>
				</tr>                       
			</table>    
			<div class="subpantallap">
				<?php
					$oculto=$_POST['oculto'];
					if($_POST['oculto']==2){
						$sqlr = "select * from tesoreciboscaja where id_recibos=$_POST[var1]";
						$resp = mysqli_query($linkbd, $sqlr);
						$row = mysqli_fetch_row($resp);
						//********Comprobante contable en 000000000000
						$sqlr="update comprobante_cab set total_debito=0,total_credito=0,estado='0' where tipo_comp='5' and numerotipo=$row[0]";
						mysqli_query($linkbd, $sqlr);
						$sqlr="update comprobante_det set valdebito=0,valcredito=0 where id_comp='5 $row[0]'";
						mysqli_query($linkbd, $sqlr);
						//********PREDIAL O RECAUDO SE ACTIVA COLOCAR 'S'
						if($row[10]=='1'){
							$sqlr="update tesoliquidapredial set estado='S' where id_predial=$row[4]";
							mysqli_query($linkbd, $sqlr);
						}
						if($row[10]=='2'){
							$sqlr="update tesoindustria set estado='S' where id_industria=$row[4]";
							mysqli_query($linkbd, $sqlr);		 
						}
						if($row[10]=='3'){
							$sqlr="update tesorecaudos set estado='S' where id_recaudo=$row[4]";
							mysqli_query($linkbd, $sqlr);
						} 
						//******** RECIBO DE CAJA ANULAR 'N'	 
						$sqlr = "update tesoreciboscaja set estado='N' where id_recibos=$row[0]";
						mysqli_query($linkbd, $sqlr);
						$sqlr = "select * from pptorecibocajappto where idrecibo=$row[0]";
						$resp = mysqli_query($linkbd, $sqlr);
						while($r=mysqli_fetch_row($resp)){
							$sqlr="update pptocuentaspptoinicial set ingresos=ingresos-$r[3] where cuenta='$r[1]'";
							mysqli_query($linkbd, $sqlr);
						}	
						$sqlr="delete from pptorecibocajappto where idrecibo=$row[0]";
						$resp=mysqli_query($linkbd, $sqlr); 
					}

					$oculto=$_POST['oculto'];
					if($_POST['oculto']){
						$listaRecibos = array();
						$listaNumero = array();
						$listaNombre = array();
						$listaConcepto = array();
						$listaFecha = array();
						$listaUsuario = array();
						$listaValor = array();
						$listaLiquidacion = array();
						$listaTipo= array();
						$listaEstado = array();
						
						function consultar_ingresos($cod,$v=1){
							if ($v==1) {
								$sqlr = "SELECT T2.codigo,T2.nombre FROM tesoreciboscaja_det T1 INNER JOIN tesoingresos T2 ON T1.ingreso=T2.codigo WHERE T1.id_recibos='$cod'";
							}else{
								$sqlr = "SELECT T2.codigo,T2.nombre FROM tesosinreciboscaja_det T1 INNER JOIN tesoingresos T2 ON T1.ingreso=T2.codigo WHERE T1.id_recibos='$cod'";
							}
							$data = view($sqlr);
							foreach ($data as $key => $row) {
								$nomb_ingresos[] = $row['nombre'];
								$cod_ingresos[] = $row['codigo'];
							}
							$nomb_ingresos = array_unique($nomb_ingresos);
							$cod_ingresos = array_unique($cod_ingresos);
							$codigos = '';
							$nombres = '';
							foreach ($nomb_ingresos as $key => $val) {
								if($key==0){
									$nombres .= $val;
								}else{
									$nombres .= ' - '.$val;
								}
							}
							$datos['nombre'] = $nombres;
							foreach ($cod_ingresos as $key => $val) {
								if($key==0){
									$codigos .= $val;
								}else{
									$codigos .= ' - '.$val;
								}
							}
							$datos['codigo'] = $codigos;
							return $datos;
						}
						
						$crit1 = " ";
						$crit2 = " ";
						$fechaf = $_POST['fecha'];
						$fechaf2 = $_POST['fecha2'];
						preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST['fecha'],$fecha1);
						$fechaf = $fecha1[3]."-".$fecha1[2]."-".$fecha1[1];
						preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST['fecha2'],$fecha2);
						$fechaf2 = $fecha2[3]."-".$fecha2[2]."-".$fecha2[1];
						if ($_POST['numero']!=""){
							$crit1 = " AND id_recibos like '%".$_POST['numero']."%' ";
						}
						$sqlr = "SELECT * FROM tesoreciboscaja WHERE estado <> '' $crit1 $crit2 AND fecha BETWEEN '$fechaf' AND '$fechaf2' ORDER BY id_recibos ASC";
						$resp = mysqli_query($linkbd, $sqlr);
						$ntr = mysqli_num_rows($resp);
						$con=1;
						$namearch="archivos/".$_SESSION['usuario']."-reporteingresos.csv";
						$Descriptor1 = fopen($namearch,"w+"); 
						fputs($Descriptor1,"RECIBO;LIQUIDACION;INGRESOS;NOMB. INGRESO;CONCEPTO;USUARIO;FECHA;VALOR;TIPO;ESTADO\r\n");
						echo "
						<table class='inicio' align='center' >
							<tr>
								<td colspan='10' class='titulos'>.: Resultados Busqueda:</td>
							</tr>
							<tr>
								<td colspan='4'>Recibos de Caja Encontrados: $ntr</td>
							</tr>
							<tr>
								<td width='150' class='titulos2'>No Recibo</td>
								<td class='titulos2' style='width:5%;'>No Ingresos</td>
								<td class='titulos2' style='width:18%;'>Nomb. Ingreso</td>
								<td class='titulos2' style='width:25%;'>Concepto</td>
								<td class='titulos2'>Fecha</td>
								<td class='titulos2'>Usuario</td>
								<td class='titulos2'>Valor</td>
								<td class='titulos2'>No Liquid.</td>
								<td class='titulos2'>Tipo</td>
								<td class='titulos2'>ESTADO</td>
							</tr>";
							$iter = 'saludo1';
							$iter2 = 'saludo2';
							$tipos = array('Predial','Industria y Comercio','Otros Recaudos');
							while ($row =mysqli_fetch_row($resp)){
								$ingresos = consultar_ingresos($row[0],1);
								echo "
								<tr>
									<td class='$iter'>$row[0]</td>
									<td class='$iter'>$ingresos[codigo]</td>
									<td class='$iter'>$ingresos[nombre]</td>
									<td class='$iter'>$row[11]</td>
									<td class='$iter'>$row[2]</td>
									<td class='$iter'>$row[12]</td>
									<td class='$iter'>".number_format($row[8],2)."</td>
									<td class='$iter'>$row[4]</td>
									<td class='$iter'>".$tipos[$row[10]-1]."</td>
									<td class='$iter'>$row[9]</td>
								</tr>";
								$listaRecibos[] = $row[0];
								$listaNumero[] = $ingresos['codigo'];
								$listaNombre[] = $ingresos['nombre'];
								$listaConcepto[] = $row[11];
								$listaFecha[] = $row[2];
								$listaUsuario[] = $row[12];
								$listaValor[] = number_format($row[8],2);
								$listaLiquidacion[] = $row[4];
								$listaTipo[] = $tipos[$row[10]-1];
								$listaEstado[] = $row[9];

								$con+=1;
								$aux=$iter;
								$iter=$iter2;
								$iter2=$aux;
								fputs($Descriptor1,$row[0].";".$row[4].";".$ingresos['codigo'].";".$ingresos['nombre'].";".$row[11].";".$row[12].";".$row[2].";".number_format($row[8],2,",","").";".strtoupper($tipos[$row[10]-1]).";".strtoupper($row[9])."\r\n");	
							}
							$sqlr = "SELECT * FROM tesosinreciboscaja WHERE estado <> '' $crit1 $crit2 AND fecha BETWEEN '$fechaf' AND '$fechaf2' ORDER BY id_recibos DESC";
							$resp = mysqli_query($linkbd, $sqlr);
							$ntr = mysqli_num_rows($resp);
							while ($row =mysqli_fetch_row($resp)){
								$ingresos = consultar_ingresos($row[0],2);
								echo "
								<tr >
									<td class='$iter'>$row[0]</td>
									<td class='$iter'>$ingresos[codigo]</td>
									<td class='$iter'>$ingresos[nombre]</td>
									<td class='$iter'>$row[11]</td>
									<td class='$iter'>$row[2]</td>
									<td class='$iter'>$row[12] $ntercero</td>
									<td class='$iter'>".number_format($row[8],2)."</td>
									<td class='$iter'>$row[4]</td>
									<td class='$iter'>INGRESOS PROPIOS</td>
									<td class='$iter'>$row[9]</td>
								</tr>";
								$listaRecibos[] = $row[0];
								$listaNumero[] = $ingresos['codigo'];
								$listaNombre[] = $ingresos['nombre'];
								$listaConcepto[] = $row[11];
								$listaFecha[] = $row[2];
								$listaUsuario[] = $row[12]." ".$ntercero;
								$listaValor[] = number_format($row[8],2);
								$listaLiquidacion[] = $row[4];
								$listaTipo[] = ">INGRESOS PROPIOS";
								$listaEstado[] = $row[9];
								$con+=1;
								$aux=$iter;
								$iter=$iter2;
								$iter2=$aux;
								fputs($Descriptor1,$row[0].";".$row[4].";".$ingresos['codigo'].";".$ingresos['nombre'].";".$row[11].";".$row[12].";".$row[2].";".number_format($row[8],2,",","").";INGRESOS PROPIOS;".strtoupper($row[9])."\r\n");
							}
						echo"
						</table>
						<input type='hidden' name='lista_recibos' value='" . serialize($listaRecibos) . "'/>
						<input type='hidden' name='lista_numero' value='" . serialize($listaNumero) . "'/>
						<input type='hidden' name='lista_nombre' value='" . serialize($listaNombre) . "'/>
						<input type='hidden' name='lista_concepto' value='" . serialize($listaConcepto) . "'/>
						<input type='hidden' name='lista_fecha' value='" . serialize($listaFecha) . "'/>
						<input type='hidden' name='lista_usuario' value='" . serialize($listaUsuario) . "'/>
						<input type='hidden' name='lista_valor' value='" . serialize($listaValor) . "'/>
						<input type='hidden' name='lista_liquidacion' value='" . serialize($listaLiquidacion) . "'/>
						<input type='hidden' name='lista_tipo' value='" . serialize($listaTipo) . "'/>
						<input type='hidden' name='lista_estado' value='" . serialize($listaEstado) . "'/>";
					}
				?>
			</div>
		</form> 
				
	</body>
</html>