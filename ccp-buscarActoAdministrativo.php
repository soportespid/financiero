<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require 'comun.inc';
	require 'funciones.inc';
	session_start();
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
	titlepag();
?>
<!DOCTYPE >
<html lang=es>
	<head>
		<meta charset="utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1"/>
		<meta name="viewport" content="user-scalable=no">
		<title>:: IDEAL 10 - Servicios Públicos</title>
		<link href="css/css2z.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/tabs.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
		<script src="vue/vue.min.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script src="css/calendario.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<style>
			input[type=number]::-webkit-inner-spin-button,
			input[type=number]::-webkit-outer-spin-button {
				-webkit-appearance: none;
				margin: 0;
			}
			input[type=number]{
				text-align:right;
				box-sizing: border-box;
				font: 120% sans-serif;
				width: 100% !important;
			}
			[v-cloak]{display : none;}
		</style>
		<script src="sweetalert2/dist/sweetalert2.min.js"></script>
		<link rel="stylesheet" href="sweetalert2/dist/sweetalert2.min.css">
	</head>
	<body>
		<header>
			<table>
				<tr><script>barra_imagenes("ccpet");</script><?php cuadro_titulos();?></tr>
			</table>
		</header>
		<form name="form2" method="post" action="">
			<section id="myapp" v-cloak >
				<nav>
					<table>
						<tr><?php menu_desplegable("ccpet");?></tr>
					</table>
                    <div class="bg-white group-btn p-1"><button type="button" v-on:click="location.href='ccp-crearActoAdministrativo.php'"
                        class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
                        <span>Nuevo</span>
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960">
                            <path d="M440-440H200v-80h240v-240h80v240h240v80H520v240h-80v-240Z"></path>
                        </svg>
                    </button>
                    <button type="button" v-on:click="location.href='ccp-buscarActoAdministrativo.php'"
                        class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
                        <span>Buscar</span>
                        <svg class="fill-black group-hover:fill-white w-5 h-5" xmlns="http://www.w3.org/2000/svg"
                            viewBox="0 -960 960 960">
                            <path
                                d="M784-120 532-372q-30 24-69 38t-83 14q-109 0-184.5-75.5T120-580q0-109 75.5-184.5T380-840q109 0 184.5 75.5T640-580q0 44-14 83t-38 69l252 252-56 56ZM380-400q75 0 127.5-52.5T560-580q0-75-52.5-127.5T380-760q-75 0-127.5 52.5T200-580q0 75 52.5 127.5T380-400Z">
                            </path>
                        </svg>
                    </button>
                    <button type="button" onclick="mypop=window.open('ccp-principal.php','','');mypop.focus();"
                        class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
                        <span>Nueva ventana</span>
                        <svg class="fill-black group-hover:fill-white w-5 h-5" xmlns="http://www.w3.org/2000/svg"
                            viewBox="0 -960 960 960">
                            <path
                                d="M320-120v-80h80v-80H160q-33 0-56.5-23.5T80-360v-400q0-33 23.5-56.5T160-840h640q33 0 56.5 23.5T880-760v400q0 33-23.5 56.5T800-280H560v80h80v80H320ZM160-360h640v-400H160v400Zm0 0v-400 400Z">
                            </path>
                        </svg>
                    </button>

                </div>
				</nav>
				<article>
					<table class="inicio ancho">
						<tr>
							<td class="titulos" colspan="20" >Buscar Actos</td>
							<td class="cerrar" style="width:7%" onClick="location.href='serv-principal.php'">Cerrar</td>
						</tr>

                        <tr>
                            <td class="textoNewR">
                                <label class="labelR">Número:</label>
                            </td>

                            <td>
                                <input type="text" v-model="numero" autocomplete="off">
                            </td>

                            <td class="textoNewR">
                                <label class="labelR">Fecha Inicial:</label>
                            </td>

                            <td>
								<input type="text" name="fechaInicial" id="fc_1198971545" onchange="" maxlength="10" onKeyUp="return tabular(event,this)" id="fechaInicial" onKeyDown="mascara(this,'/',patron,true)" title="DD/MM/YYYY" style="height:30px;text-align:center;" onDblClick="displayCalendarFor('fc_1198971545');" class="colordobleclik" autocomplete="off">
                            </td>

                            <td class="textoNewR">
                                <label class="labelR">Fecha Final:</label>
                            </td>

                            <td>
								<input type="text" name="fechaFinal" id="fc_1198971546" onchange="" maxlength="10" onKeyUp="return tabular(event,this)" id="fechaFinal" onKeyDown="mascara(this,'/',patron,true)" title="DD/MM/YYYY" style="height:30px;text-align:center;" onDblClick="displayCalendarFor('fc_1198971546');" class="colordobleclik" autocomplete="off">
                            </td>

                            <td style="padding-bottom:0px">
                                <em class="botonflechaverde" v-on:Click="buscarActos">Buscar</em>
                            </td>
                        </tr>
					</table>

					<div class='subpantalla' style='height:66vh; width:99.2%; margin-top:0px; overflow:hidden'>
						<table class='tablamv'>
							<thead>
								<tr style="text-align:Center;">
									<th class="titulosnew00" style="width:6%;">Vigencia</th>
									<th class="titulosnew00" style="width:6%;">Número</th>
									<th class="titulosnew00">Acto Administrativo</th>
                                    <th class="titulosnew00" style="width:10%;">Valor inicial</th>
                                    <th class="titulosnew00" style="width:10%;">Adición</th>
									<th class="titulosnew00" style="width:10%;">Reducción</th>
									<th class="titulosnew00" style="width:10%;">Traslado</th>
                                    <th class="titulosnew00" style="width:8%;">Fecha acuerdo</th>
                                    <th class="titulosnew00" style="width:8%;">Tipo acto</th>
									<th class="titulosnew00" style="width:6%;">Estado</th>
                                    <th class="titulosnew00" style="width:5%;">Anular</th>
								</tr>
							</thead>

							<tr v-show="!existeInformacion">
                                <td colspan="7">
                                    <div style="text-align: center; color:turquoise; font-size:large" class="h4 text-primary text-center">
                                        Utilice los filtros para buscar información.
                                    </div>
                                </td>
                            </tr>

							<tbody>
								<tr v-for="(actos, index) in actosAdministrativos" v-bind:class="index % 2 ? 'saludo1a' : 'saludo2'" v-on:dblclick="seleccionaActo(actos)" style='text-rendering: optimizeLegibility; cursor: pointer !important;'>
									<td style="width:6%; font: 120% sans-serif; padding-left:5px; text-align:center;">{{ actos[4] }}</td>
									<td style="width:6%; font: 120% sans-serif; padding-left:5px; text-align:center;">{{ actos[1] }}</td>
									<td style="font: 120% sans-serif; padding-left:5px;">{{ actos[2] }}</td>
									<td style="width:10%; font: 120% sans-serif; padding-left:5px; text-align:right;">{{ formatonumero(actos[8]) }}</td>
									<td style="width:10%; font: 120% sans-serif; padding-left:5px; text-align:right;">{{ formatonumero(actos[5]) }}</td>
									<td style="width:10%; font: 120% sans-serif; padding-left:5px; text-align:right;">{{ formatonumero(actos[6]) }}</td>
									<td style="width:10%; font: 120% sans-serif; padding-left:5px; text-align:right;">{{ formatonumero(actos[7]) }}</td>
									<td style="width:8%; font: 120% sans-serif; padding-left:5px; text-align:center;">{{ actos[3] }}</td>
									<td style="width:8%; font: 120% sans-serif; padding-left:5px; text-align:center;">{{ buscarActo(actos[11]) }}</td>
									<td style="width:6%; font: 120% sans-serif; padding-left:5px; text-align:center;">{{ buscaEstado(actos[9]) }}</td>
									<td style="width:5%; text-align: right;">
										<a v-on:click="anular(actos[9], actos[0])" style='cursor:pointer;'>
											<img src="imagenes/newdelete.png" style="height:30px;" title="Eliminar">
										</a>
									</td>
								</tr>
							</tbody>
						</table>
					</div>

					<div id="cargando" v-if="loading" class="loading">
						<span>Cargando...</span>
					</div>

				</article>
			</section>
		</form>

		<script src="Librerias/vue/vue.min.js"></script>
		<script src="Librerias/vue/axios.min.js"></script>
		<script src="presupuesto_ccpet/actoAdministrativo/buscar/ccp-buscarActoAdminsitrativo.js?<?php echo date('d_m_Y_h_i_s');?>"></script>

	</body>
</html>
