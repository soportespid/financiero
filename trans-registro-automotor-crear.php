<?php

    header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require 'comun.inc';
	require 'funciones.inc';
	session_start();
    if(empty($_SESSION)){
        header("location: index.php");
    }
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html lang=es>
	<head>
        <meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1"/>
		<meta name="viewport" content="user-scalable=no">
		<title>:: IDEAL 10 SAS</title>
		<link href="favicon.ico" rel="shortcut icon"/>
		<link href="css/css2z.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
        <link href="css/style.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" href="sweetalert2/dist/sweetalert2.min.css">
        <script src="sweetalert2/dist/sweetalert2.min.js"></script>
		<script src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
        <style>
            [v-cloak]{
                display : none;
            }
        </style>
	</head>
	<body>
		<header>
			<table>
				<tr>
                    <script>barra_imagenes("trans");</script><?php cuadro_titulos();?>
                </tr>
			</table>
		</header>

        <section id="myapp" v-cloak>
            <!-- variables -->
            <input type="hidden" value = "1" ref="pageType">

            <!-- loading -->
            <div class="loading-container" v-show="isLoading" >
                <p class="text-loading"data-loading="IDEAL 10">IDEAL 10</p>
            </div>

            <nav>
                <table>
                    <tr><?php menu_desplegable("trans");?></tr>
                </table>
                <div class="bg-white group-btn p-1" id="newNavStyle">
                    <button type="button" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center" @click="window.location.reload()">
                        <span>Nuevo</span>
                        <svg viewBox="0 -960 960 960" ><path d="M440-440H200v-80h240v-240h80v240h240v80H520v240h-80v-240Z"/></svg>
                    </button>
                    <button type="button" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center" @click="save()">
                        <span>Guardar</span>
                        <svg viewBox="0 -960 960 960" ><path d="M840-680v480q0 33-23.5 56.5T760-120H200q-33 0-56.5-23.5T120-200v-560q0-33 23.5-56.5T200-840h480l160 160Zm-80 34L646-760H200v560h560v-446ZM480-240q50 0 85-35t35-85q0-50-35-85t-85-35q-50 0-85 35t-35 85q0 50 35 85t85 35ZM240-560h360v-160H240v160Zm-40-86v446-560 114Z"/></svg>
                    </button>
                    <button type="button" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center" @click="location.href='trans-registro-automotor-buscar'">
                        <span>Buscar</span>
                        <svg class="fill-black group-hover:fill-white w-5 h-5" viewBox="0 -960 960 960" ><path d="M784-120 532-372q-30 24-69 38t-83 14q-109 0-184.5-75.5T120-580q0-109 75.5-184.5T380-840q109 0 184.5 75.5T640-580q0 44-14 83t-38 69l252 252-56 56ZM380-400q75 0 127.5-52.5T560-580q0-75-52.5-127.5T380-760q-75 0-127.5 52.5T200-580q0 75 52.5 127.5T380-400Z"/></svg>
                    </button>
                    <button type="button" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center" @click="mypop=window.open('trans-principal','',''); mypop.focus();">
                        <span>Nueva ventana</span>
                        <svg class="fill-black group-hover:fill-white w-5 h-5" viewBox="0 -960 960 960" ><path d="M320-120v-80h80v-80H160q-33 0-56.5-23.5T80-360v-400q0-33 23.5-56.5T160-840h640q33 0 56.5 23.5T880-760v400q0 33-23.5 56.5T800-280H560v80h80v80H320ZM160-360h640v-400H160v400Zm0 0v-400 400Z"/></svg>
                    </button>

                </div>
            </nav>
            <article class="bg-white">
                <div>
                    <div>
                        <h2 class="titulos m-0">Crear - Registro automotor</h2>

                        <div class="d-flex">
                            <div class="form-control w-25">
                                <label class="form-label m-0" for="">Fecha tramite<span class="text-danger fw-bolder">*</span></label>
                                <input type="date" class="text-center" v-model="dateTramite">
                            </div>

                            <div class="form-control w-25">
                                <label class="form-label m-0" for="">Número placa<span class="text-danger fw-bolder">*</span></label>
                                <input type="text" class="text-center" v-model="placa" @change="checkPlaca()" maxlength="6">
                            </div>

                            <div class="form-control w-25">
                                <label class="form-label m-0" for="">Departamento:<span class="text-danger fw-bolder">*</span></label>
                                <select v-model="departamentoCod">
									<option value="">Seleccione</option>
                                    <option v-for="dpto in arrDepartamentos" :value="dpto.codigo">{{ dpto.nombre }}</option>
                                </select>
                            </div>

                            <div class="form-control w-25">
                                <label class="form-label m-0" for="">Municipio<span class="text-danger fw-bolder">*</span></label>
                                <select v-model="municipioCod">
									<option value="">Seleccione</option>
                                    <option v-for="mnpio in arrMunicipios" :value="mnpio.codigo" v-if="mnpio.codigo_departamento == departamentoCod">{{ mnpio.nombre }}</option>
                                </select>
                            </div>

                            <div class="form-control w-25">
                                <label class="form-label m-0" for="">Propietario<span class="text-danger fw-bolder">*</span></label>
                                <input type="number" class="colordobleclik" v-model="propietarioDocument" @dblclick="modalTerceros=true;" @change="filterArray('inputTercero')">
                            </div>

                            <div class="form-control justify-between">
                                <label class="form-label m-0" for=""></label>
                                <input type="text" v-model="propietarioName" readonly>
                            </div>
                        </div>

                        <h2 class="titulos m-0">Caracterización</h2>

                        <div class="d-flex">
                            <div class="d-flex w-50">
                                <div class="form-control">
                                    <label class="form-label m-0" for="">Tipo vehiculo<span class="text-danger fw-bolder">*</span></label>
                                    <select v-model="tipoId" @change="clearForm">
                                        <option value="0">Seleccione</option>
                                        <option v-for="tipo in arrTipos" :value="tipo.id">{{ tipo.nombre }}</option>
                                    </select>
                                </div>

                                <div class="form-control">
                                    <label class="form-label m-0" for="">Clase vehiculo<span class="text-danger fw-bolder">*</span></label>
                                    <input type="text" class="colordobleclik" v-model="claseName" @dblclick="desplegarModal('Clases')" readonly>
                                </div>

                                <div class="form-control">
                                    <label class="form-label m-0" for="">Marca<span class="text-danger fw-bolder">*</span></label>
                                    <input type="text" class="colordobleclik" v-model="marcaName" @dblclick="desplegarModal('Marcas')" readonly>
                                </div>
                            </div>

                            <div class="d-flex w-50">
                                <div class="form-control">
                                    <label class="form-label m-0" for="">Linea<span class="text-danger fw-bolder">*</span></label>
                                    <input type="text" class="colordobleclik" v-model="lineaName" @dblclick="desplegarModal('Lineas')" readonly>
                                </div>

                                <div class="form-control">
                                    <label class="form-label m-0" for="">Tipo combustible<span class="text-danger fw-bolder">*</span></label>
                                    <input type="text" class="colordobleclik" v-model="combustibleName" @dblclick="desplegarModal('Combustibles')" readonly>
                                </div>

                                <div class="form-control">
                                    <label class="form-label m-0" for="">Color<span class="text-danger fw-bolder">*</span></label>
                                    <input type="text" v-model="color">
                                </div>
                            </div>
                        </div>

                        <div class="d-flex">
                            <div class="d-flex w-50">
                                <div class="form-control">
                                    <label class="form-label m-0" for="">Modelo<span class="text-danger fw-bolder">*</span></label>
                                    <input type="text" class="text-center" v-model="modelo" maxlength="4">
                                </div>
                                <div class="form-control">
                                    <label class="form-label m-0" for="">Valor comercial (opcional)</label>
                                    <input type="number" class="text-right" v-model="intAvaluo">
                                </div>
                                <div class="form-control">
                                    <label class="form-label m-0" for="">Cilindrada CC<span class="text-danger fw-bolder">*</span></label>
                                    <input type="text" class="colordobleclik" v-model="cilindrajeName" @dblclick="desplegarModal('Cilindrajes')" readonly>
                                </div>

                                <div class="form-control">
                                    <label class="form-label m-0" for="">Capacidad de carga (Kg)<span class="text-danger fw-bolder">*</span></label>
                                    <input type="number" class="text-center" v-model="capacidad" min="0">
                                </div>
                            </div>

                            <div class="d-flex w-50">
                                <div class="form-control">
                                    <label class="form-label m-0" for="">Cantidad pasajeros<span class="text-danger fw-bolder">*</span></label>
                                    <input type="number" class="text-center" v-model="ocupantes" min="0">
                                </div>

                                <div class="form-control">
                                    <label class="form-label m-0" for="">Potencia/HP<span class="text-danger fw-bolder">*</span></label>
                                    <input type="number" class="text-center" v-model="potencia" min="0">
                                </div>

                                <div class="form-control">
                                    <label class="form-label m-0" for="">Tipo de público</label>
                                    <input type="text" class="colordobleclik" v-model="tipoPublicoName" @dblclick="desplegarModal('Tipo Publicos')" readonly>
                                </div>
                            </div>
                        </div>

                        <div class="d-flex">
                            <div class="d-flex w-50">
                                <div class="form-control">
                                    <label class="form-label m-0" for="">Carrocería<span class="text-danger fw-bolder">*</span></label>
                                    <input type="text" class="colordobleclik" v-model="carroceriaName" @dblclick="desplegarModal('Carrocerias')" readonly>
                                </div>

                                <div class="form-control">
                                <label class="form-label m-0" for="">Vehiculo antiguo<span class="text-danger fw-bolder">*</span></label>
                                    <select v-model="vehiculoAntiguo">
                                        <option value="N">No</option>
                                        <option value="S">Si</option>
                                    </select>
                                </div>

                                <div class="form-control">
                                    <label class="form-label m-0" for="">Blindaje<span class="text-danger fw-bolder">*</span></label>
                                    <select v-model="blindaje">
                                        <option value="N">No</option>
                                        <option value="S">Si</option>
                                    </select>
                                </div>
                            </div>

                            <div class="d-flex w-50">
                                <div class="form-control">
                                    <label class="form-label m-0" for="">Fecha resolución</label>
                                    <input type="date" class="text-center" v-model="fechaBlindaje">
                                </div>

                                <div class="form-control">
                                    <label class="form-label m-0" for="">Desmonte blindaje<span class="text-danger fw-bolder">*</span></label>
                                    <select v-model="desmontajeBlindaje">
                                        <option value="N">No</option>
                                        <option value="S">Si</option>
                                    </select>
                                </div>

                                <div class="form-control">
                                    <label class="form-label m-0" for="">Fecha resolución</label>
                                    <input type="date" class="text-center" v-model="fechaDesmontaje">
                                </div>
                            </div>
                        </div>

                        <h2 class="titulos m-0">Identificación interna del vehiculo</h2>

                        <div class="d-flex">
                            <div class="d-flex w-50">
                                <div class="form-control">
                                    <label class="form-label m-0" for="">Número de motor<span class="text-danger fw-bolder">*</span></label>
                                    <input type="text" v-model="numeroMotor">
                                </div>

                                <div class="form-control">
                                    <label class="form-label m-0" for="">Número de chasis</label>
                                    <input type="text" v-model="numeroChasis">
                                </div>
                            </div>

                            <div class="d-flex w-50">
                                <div class="form-control">
                                    <label class="form-label m-0" for="">Número de serie</label>
                                    <input type="text" v-model="numeroSerie">
                                </div>

                                <div class="form-control">
                                    <label class="form-label m-0" for="">Número de VIN</label>
                                    <input type="text" v-model="numeroVin">
                                </div>
                            </div>
                        </div>

                        <h2 class="titulos m-0">Tipo de uso</h2>

                        <div class="d-flex">
                            <div class="form-control">
                                <label class="form-label m-0" for="">Tipo servicio<span class="text-danger fw-bolder">*</span></label>
                                <select v-model="servicioId">
                                    <option value="">Seleccione</option>
                                    <option v-for="servicio in arrServicios" :value="servicio.id">{{ servicio.nombre }}</option>
                                </select>
                            </div>

                            <div class="form-control">
                                <label class="form-label m-0" for="">Empresa vinculadora</label>
                                <input type="text">
                            </div>
                        </div>
                    </div>
                </div>

                <!-- modales -->
                <div v-show="modalTerceros" class="modal">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                            <div class="modal-header m-0">
                                <h5 class="modal-title">Terceros</h5>
                                <button type="button" @click="modalTerceros=false;" class="btn btn-close"><div></div><div></div></button>
                            </div>
                            <div class="modal-body">
                                <div class="d-flex">
                                    <div class="form-control">
                                        <input type="search" v-model="txtSearchTercero" @keyup.enter="getTerceros(txtSearchTercero)" placeholder="Busca documento o nombre">
                                    </div>

                                    <div class="form-control justify-between w-15">
                                        <button type="button" class="btn btn-primary" @click="getTerceros(txtSearchTercero)">Buscar</button>
                                    </div>
                                </div>
                                <div class="overflow-auto max-vh-50 overflow-x-hidden" >
                                    <table class="table table-hover fw-normal">
                                        <thead>
                                            <tr class="text-center">
                                                <th>Item</th>
                                                <th>Documento</th>
                                                <th>Nombre</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr v-show="arrTerceros.length == 0">
                                                <td colspan="3">
                                                    <div style="text-align: center; font-size:large" class="h4 text-center">
                                                        Utilice los filtros para buscar información.
                                                    </div>
                                                </td>
                                            </tr>

                                            <tr v-for="(tercero,index) in arrTerceros" :key="index" @click="fillForm('tercero', tercero)">
                                                <td class="text-center w-15">{{ index+1 }}</td>
                                                <td class="text-center">{{ tercero.cedulanit }}</td>
                                                <td>{{ tercero.nombre }}</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div v-show="modalCaracterizacion" class="modal">
                    <div class="modal-dialog modal-sm">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title">{{ titleCaracterizacion }}</h5>
                                <button type="button" @click="modalCaracterizacion=false;" class="btn btn-close"><div></div><div></div></button>
                            </div>
                            <div class="modal-body">
                                <div class="d-flex flex-column">
                                    <div class="form-control m-0 mb-3">
                                        <input type="search" v-model="txtSearchCaracterizacion" @keyup="filterArray(accionCaracterizacion)" placeholder="Busca por nombre">
                                    </div>
                                </div>
                                <div class="overflow-auto max-vh-50 overflow-x-hidden" >
                                    <table class="table table-hover fw-normal">
                                        <thead>
                                            <tr class="text-center">
                                                <th>Item</th>
                                                <th>Nombre</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr v-show="arrCaracterizacion.length == 0">
                                                <td colspan="2">
                                                    <div style="text-align: center; font-size:large" class="h4 text-center">
                                                        Utilice los filtros para buscar información.
                                                    </div>
                                                </td>
                                            </tr>

                                            <tr v-for="(data,index) in arrCaracterizacion" :key="index" @click="fillForm(accionCaracterizacion, data)">
                                                <td class="text-center w-15">{{ data.id }}</td>
                                                <td>{{ data.nombre }}</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </article>
        </section>

        <script src="Librerias/vue/vue.min.js"></script>
		<script type="module" src="transporte/js/functions_registro_automotor.js?<?= date('d_m_Y_h_i_s');?>"></script>

	</body>
</html>
