<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require "comun.inc";
	require "funciones.inc";
	require "funcionesSP.inc.php";
	session_start();
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8"); 
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
?>

<!DOCTYPE >
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es">
	<head>
		<meta http-equiv="Content-type" content="text/html; charset=iso-8859-1"/>
		<meta http-equiv="X-UA-Compatible" content="IE=9"/>
		<title>:: IDEAL 10 - Servicios P&uacute;blicos</title>
		<link href="css/css2.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/cssSP.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/style.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
		<link href="css/sweetalert.css" rel="stylesheet" type="text/css"/>
		<script type="text/javascript" src="css/sweetalert.js"></script>
    	<script type="text/javascript" src="css/sweetalert.min.js"></script>
		<script type="text/javascript" src="css/programas.js"></script>
		<script type="text/javascript" src="css/calendario.js?<?php echo date('d_m_Y_h_i_s');?>"></script>

		<script>
			function despliegamodal2(_valor, _table)
			{
				document.getElementById('bgventanamodal2').style.visibility = _valor;
                if(_valor == 'hidden')
                {
                    document.getElementById('ventana2').src = '';
                }
				else 
				{
                    document.getElementById('ventana2').src = 'barrios-ventana01.php?table=' + _table;
				}

				if(_table == 'terceros')
				{
					document.getElementById('ventana2').src = 'terceros-ventana01.php?table=' + _table;
				}

				if(_table == 'estratos')
                {
					document.getElementById('ventana2').src = 'serv-ventanaEstratos.php';
				}
            }

            function despliegamodalm(_valor,_tip,mensa,pregunta)
			{
				document.getElementById("bgventanamodalm").style.visibility=_valor;
                if(_valor == "hidden") 
                {
                    document.getElementById('ventanam').src = "";
                }
				else
				{
					switch(_tip)
					{
                        case "1":	
                            document.getElementById('ventanam').src = "ventana-mensaje1.php?titulos=" + mensa;
                            break;
                        case "2":	
                            document.getElementById('ventanam').src = "ventana-mensaje3.php?titulos=" + mensa;
                            break;
                        case "3":	
                            document.getElementById('ventanam').src = "ventana-mensaje2.php?titulos=" + mensa;
                            break;
                        case "4":	
                            document.getElementById('ventanam').src = "ventana-consulta1.php?titulos=" + mensa + "&idresp=" + pregunta;
                            break;	
					}
				}
            }
            
			function funcionmensaje()
			{
				var idban = document.getElementById('codban').value;
				document.location.href = "serv-crearCliente.php";
            }
            
            //Inserta en el input cuando se selecciona un barrio, zona, lado y ruta.
			function respuestaModalBusqueda(pregunta, value, id)
			{
				switch(pregunta)
				{
                    case 'srvbarrios':	
                        document.getElementById('barrio').value = value;
                        document.getElementById('id_barrio').value = id;
                        document.form2.submit();
                        break;

					case 'srvveredas':	
                        document.getElementById('vereda').value = value;
                        document.getElementById('id_vereda').value = id;
                        document.form2.submit();
                        break;
                    
                    case 'srvzonas':	
                        document.getElementById('zona').value = value;
                        document.getElementById('id_zona').value = id;
                        break;
                        
                    case 'srvlados':	
                        document.getElementById('lado').value = value;
                        document.getElementById('id_lado').value = id;
                        break;
                    
                    case 'srvrutas':	
                        document.getElementById('ruta').value = value;
                        document.getElementById('id_ruta').value = id;
						break;

                }
                
                document.getElementById('bgventanamodal2').style.visibility = 'hidden';
            }

			function respuestaModalBusqueda2(id, razonsocial, nombre1, nombre2, apellido1, apellido2, cedulanit)
			{
				document.getElementById('terceros').value = cedulanit;

				if(razonsocial == '')
				{
					document.getElementById('id_tercero').value = id;
					document.getElementById('razonsocial').value = razonsocial;
					var nombreCompleto = nombre1 + ' ' + nombre2 + ' ' + apellido1 + ' ' + apellido2;
					document.getElementById('ntercero').value = nombreCompleto;
					document.form2.submit();
				}
				if (razonsocial != '') 
				{
					document.getElementById('id_tercero').value = id;
					document.getElementById('ntercero').value = razonsocial;
					document.form2.submit();
				} 
                
                document.getElementById('bgventanamodal2').style.visibility = 'hidden';
            }

            function respuestaconsulta(pregunta)
			{
				switch(pregunta)
				{
                    case "1":	
                        document.form2.oculto.value = '2';
                        document.form2.submit();
					break;
				}
			}
			function guardar()
			{
				var codigo = document.getElementById('codban').value;
                var nombreTercero = document.getElementById('ntercero').value;
				var cod_usuario = document.getElementById('cod_usuario').value;
                
				if (codigo.trim() != '' && nombreTercero.trim() != '' && cod_usuario != '') 
				{
                    despliegamodalm('visible', '4', 'Esta Seguro de Guardar', '1');
                }
				else 
				{
                    despliegamodalm('visible', '2', 'Falta información para crear el cliente');
                }
                
            }

			function validar(formulario)
            {
                document.form2.action="serv-crearCliente.php";
				document.form2.submit();
            }

			function buscaTerceros()
			{
				document.form2.buscaTercero.value = '1';
				document.form2.submit();
			}
        </script>
        
        <?php titlepag();?>
        
    </head>
    
	<body>
		<IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
		<span id="todastablas2"></span>
		<table>
			<tr>
                <script>barra_imagenes("serv");</script><?php cuadro_titulos();?>
            </tr>

			<tr>
                <?php menu_desplegable("serv");?>
            </tr>

			<tr>
				<td colspan="3" class="cinta">
					<a href="serv-crearCliente.php" class="mgbt"><img src="imagenes/add.png"/></a>
					<a onclick="guardar()" class="mgbt"><img src="imagenes/guarda.png" title="Guardar"/></a>
					<a href="serv-buscaCliente.php" class="mgbt"><img src="imagenes/busca.png" title="Buscar"/></a>
					<a onclick="mypop=window.open('plan-agenda.php','','');mypop.focus()" class="mgbt"><img src="imagenes/agenda1.png" title="Agenda" /></a>
					<a onclick="mypop=window.open('serv-principal.php','','');mypop.focus();" class="mgbt"><img src="imagenes/nv.png" title="Nueva ventana"></a>
					<a onclick="mypop=window.open('<?php echo $url2; ?>','','');mypop.focus();" class="mgbt"><img src="imagenes/duplicar_pantalla.png" title="Duplicar pesta&ntilde;a"></a>
				</td>
            </tr>
        </table>
        
		<div id="bgventanamodalm" class="bgventanamodalm">
			<div id="ventanamodalm" class="ventanamodalm">
				<IFRAME  name="ventanam" marginWidth=0 marginHeight=0 frameBorder=0 id="ventanam" frameSpacing=0 style=" width:700px; height:130px; top:200; overflow:hidden;"></IFRAME>
			</div>
        </div>
        
		<form name="form2" method="post" >
			<?php 
				if(@$_POST['oculto'] == '')
				{
					$_POST['codban'] = selconsecutivo('srvclientes','id');
				}

				if(@$_POST['buscaTercero'] == '1')
				{
					$datosTercero = buscaNombreTercero($_POST['terceros']);

					for ($i=0; $i < count($datosTercero); $i++) 
					{ 
						$_POST['ntercero'] = $datosTercero[0];
						$_POST['id_tercero'] = $datosTercero[1];
					}
					
				}
			?>
			<table class="inicio ancho">
				<tr>
					<td class="titulos" colspan="6">.: Ingresar Cliente</td>

					<td class="cerrar" style="width:7%" onClick="location.href='serv-principal.php'">Cerrar</td>
				</tr>

				<tr>
					<td class="tamano01" style="width:3cm;">ID:</td>

					<td style="width:15%; text-align:center;">
						<input type="text" name="codban" id="codban" onKeyUp="return tabular(event,this)" value="<?php echo @$_POST['codban'];?>" style="width:50%;height:30px;text-align:center" readonly/>
					</td>

					<td class="tamano01" style="width: 10%;">C&oacute;digo Usuario:</td> 

					<td>
						<input type="number" name='cod_usuario' id='cod_usuario' value="<?php echo @$_POST['cod_usuario'];?>" style="width:100%;height:30px;text-transform:uppercase;text-align:center;"/>
					</td>

					<td class="tamano01" style="width: 10%;">C&eacute;dula Catastral:</td> 

					<td style="width: 25%;">
						<input type="number" name='catastral' id='catastral' value="<?php echo @$_POST['catastral'];?>" style="width:100%;height:30px;text-transform:uppercase"/>
					</td>
				</tr>

				<tr>
					<td class="tamano01" style="width:5%;">Fecha:</td>

					<td style="width:15%;">
						<input type="text" name="fecha" value="<?php echo @ $_POST['fecha']?>" maxlength="10" onKeyPress="javascript:return solonumeros(event)" onKeyUp="return tabular(event,this)" id="fc_1198971545" onKeyDown="mascara(this,'/',patron,true)" title="DD/MM/YYYY" style="width:80%;height:30px;text-align:center">&nbsp;
							<img src="imagenes/calendario04.png" onClick="displayCalendarFor('fc_1198971545');" title="Calendario" class="icobut"/>
						</input>
					</td>

					<td class="tamano01">Usuario:</td>

					<td style="width:15%;">
						<input type="text" name="terceros" id="terceros"  onBlur="buscaTerceros()" value="<?php echo @$_POST['terceros']?>" style="width:88%;height:30px;" />

						<a title="Cuentas presupuestales" onClick="despliegamodal2('visible', 'terceros');" style="cursor:pointer;"><img src="imagenes/find02.png" style="width:20px;"/></a>

						<input type="hidden" name='id_tercero' id='id_tercero' value="<?php echo $_POST['id_tercero']?>">
						<input type="hidden" name='razonsocial' id='razonsocial' value="<?php echo $_POST['razonsocial']?>">
						<input type="hidden" name='nombre1' id='nombre1' value="<?php echo $_POST['nombre1']?>">
						<input type="hidden" name='nombre2' id='nombre2' value="<?php echo $_POST['nombre2']?>">
						<input type="hidden" name='apellido1' id='apellido1' value="<?php echo $_POST['apellido1']?>">
						<input type="hidden" name='apellido2' id='apellido2' value="<?php echo $_POST['apellido2']?>">
					</td>

					<td colspan="2">
						<input type="text" name="ntercero" id="ntercero" value="<?php echo @$_POST['ntercero']?>" style="width:100%;height:30px;" readonly>
					</td>					
				</tr> 

				<tr>
					<td class="tamano01">Direcci&oacute;n:</td>

					<td colspan="4">
						<input type="text" name="direccion" id="direccion" value="<?php echo @ $_POST['direccion']?>" style="width:100%;height:30px;text-transform:uppercase"/>	
					</td>
				</tr> 

				<tr>
					<td class="tamano01">Prefijo:</td>

					<td>
						<?php 
							$sqlr = " SELECT depto,mnpio FROM terceros WHERE id_tercero='$_POST[id_tercero]'";
							$resp = mysqli_query($linkbd, $sqlr);
							$row =mysqli_fetch_row($resp);
							$prefijo = $row[0].$row[1];
						?>
						<input type="text" name="codprefijo" id="codprefijo" value="<?php echo $prefijo;?>" style="width:100%;height:30px;" readonly/>	
					</td>

					<td class="tamano01">Estrato:</td>
					
					<td colspan="2">
						<input type="text" name='nombreEstrato' id='nombreEstrato' value="<?php echo $_POST['nombreEstrato']?>" style="width:100%;height: 30px;" onclick = "despliegamodal2('visible', 'estratos');" class="colordobleclik" readonly>

						<input type="hidden" name='id_estrato' id='id_estrato' value="<?php echo $_POST['id_estrato']?>">
					</td>
				</tr>

				<tr>
                <?php 
					if(($_POST['nombrebarrio'] == "" && $_POST['vereda'] == "") || $_POST['nombrebarrio'] != "" && $_POST['vereda'] == "")
					{
				?>
						<td class="tamano01" style="width:3cm;">Barrio:</td>

						<td>
							<input type="text" name='nombrebarrio' id='barrio'  value="<?php echo $_POST['nombrebarrio']?>" style="width:100%;height: 30px;" onclick = "despliegamodal2('visible', 'srvbarrios');" class="colordobleclik" readonly >
							<input type="hidden" name='id_barrio' id='id_barrio' value="<?php echo $_POST['id_barrio']?>">
						</td>
                <?php 
					} 

                	if (($_POST['nombrebarrio'] == "" && $_POST['vereda'] == "")|| $_POST['vereda'] != "" && $_POST['nombrebarrio'] == "" ) 
					{
				?>   
						<td class="tamano01" style="width:3cm;">Vereda:</td>
						
						<td>
							<input type="text" name='vereda' id='vereda'  value="<?php echo $_POST['vereda']?>" style="width:100%;height: 30px;" onclick = "despliegamodal2('visible', 'srvveredas');"  class="colordobleclik" readonly>
							<input type="hidden" name='id_vereda' id='id_vereda' value="<?php echo $_POST['id_vereda']?>">
						</td>

                <?php
			  		}
				 ?>
					<td class="tamano01" style="width:3cm;">Zona:</td>

					<td>
						<input type="text" name='zona' id='zona'  value="<?php echo $_POST['zona']?>" style="width: 100%; height: 30px;" onclick = "despliegamodal2('visible', 'srvzonas');"  class="colordobleclik" readonly>
                        <input type="hidden" name='id_zona' id='id_zona' value="<?php echo $_POST['id_zona']?>">
					</td>
				</tr>

				<tr>
                    
					<td class="tamano01" style="width:3cm;">Lado:</td>
					<td>
						<input type="text" name='lado' id='lado'  value="<?php echo $_POST['lado']?>" style="width:100%;height: 30px;" onclick = "despliegamodal2('visible', 'srvlados');"  class="colordobleclik" readonly>
						<input type="hidden" name='id_lado' id='id_lado' value="<?php echo $_POST['id_lado']?>">
					</td>
					
					<td class="tamano01" style="width:3cm;">Ruta:</td>
                    <td>
                        <input type="text" name='ruta' id='ruta'  value="<?php echo $_POST['ruta']?>" style="width:100%;height: 30px;" onclick = "despliegamodal2('visible', 'srvrutas');"  class="colordobleclik" readonly>
                        <input type="hidden" name='id_ruta' id='id_ruta' value="<?php echo $_POST['id_ruta']?>">
                    </td>

					<td class="tamano01" style="width: 3cm;">Código Ruta:</td>
					<td>
						<input type="text" name="cod_ruta" id="cod_ruta" value="<?php echo @$_POST['cod_ruta'] ?>" style="width:98%; height: 30px; text-align:center;">
					</td>
				</tr>
				
			</table>
			<input type="hidden" name="oculto" id="oculto" value="1"/>
			<input type="hidden" name="buscaTercero" id="buscaTercero" value=""/>
			<?php 
				if(@ $_POST['oculto'] == '2')
				{
					preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST['fecha'],$f);
					$fecha="$f[3]-$f[2]-$f[1]";

					if (!empty($_POST['id_zona'])) {
						$id_zona = $_POST['id_zona'];
					}
					else {
						$id_zona = 'NULL';
					}
                    
					if (!empty($_POST['id_lado'])) {
						$id_lado = $_POST['id_lado'];
					}
					else {
						$id_lado = 'NULL';
					}

					$number = $_POST['cod_usuario'];
					$length = 10;
				    $codigoUsuario = $number;

                    $query = "SELECT cod_usuario FROM srvclientes WHERE cod_usuario = '$codigoUsuario'";
					$resp = mysqli_query($linkbd,$query);
					$row = mysqli_num_rows($resp);

					if ($row == 0) {
						$sqlr = "INSERT INTO srvclientes (id_tercero, cod_usuario, cod_catastral, fecha_creacion, prefijo, id_barrio, id_vereda, id_zona, id_ruta, id_lado, id_estrato, estado, codigo_ruta) VALUES ('$_POST[id_tercero]', '$codigoUsuario', '$_POST[catastral]', '$fecha', '$_POST[codprefijo]', '$_POST[id_barrio]', '$_POST[id_vereda]', $id_zona, '$_POST[id_ruta]', $id_lado, '$_POST[id_estrato]', estado = 'S', '$_POST[cod_ruta]') ";	

						$_POST['codban'] = selconsecutivo('srvclientes','id');

						$sql_servicio = "INSERT INTO srvasignacion_servicio (id_clientes, id_servicio, id_estrato, fecha_activacion, tarifa_medidor, cargo_fijo, consumo, aforo, estado, estado_medidor) VALUES ('$_POST[id_tercero]', 1, '$_POST[id_estrato]', '$fecha', 'N', 'S', 'S', 'S', 'S', 1)";
						mysqli_query($linkbd, $sql_servicio);

						$sqlr1 = "INSERT INTO srvdireccion_cliente (id_cliente, direccion) VALUES ('".$_POST['codban']."','".$_POST['direccion']."')";
					

						if (!mysqli_query($linkbd, $sqlr)||!mysqli_query($linkbd, $sqlr1))
						{
						    echo "
						        <script> 
						            despliegamodalm('visible', '2', 'No se pudo ejecutar la peticion');
						        </script>";
						}
						else 
						{
						    echo "
						        <script>
									despliegamodalm('visible', '1', 'Se ha almacenado con Exito');
						        </script>";
						}
					}
					else {
						echo "<script>despliegamodalm('visible', '2', 'Codigo de usuario ya existente');</script>";
					}
						
				}
			?> 
		</form>
		<div id="bgventanamodal2">
			<div id="ventanamodal2">
				<IFRAME  src="" name="buster" marginWidth=0 marginHeight=0 frameBorder=0 id="ventana2" frameSpacing=0 style="left:500px; width:900px; height:500px; top:200;"></IFRAME>
			</div>
		</div>
	</body>
</html>
