<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=iso-8859-1");
	require"comun.inc";
	require"funciones.inc";
	session_start();
	$linkbd=conectar_v7();
	cargarcodigopag($_GET[codpag],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
	$scroll=$_GET['scrtop'];
	$totreg=$_GET['totreg'];
	$idcta=$_GET['idcta'];
	$altura=$_GET['altura'];
	$filtro="'".$_GET['filtro']."'";
	error_reporting(E_ALL);
	ini_set('display_errors', '1');
?>
<!DOCTYPE >
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es">
	<head>
		<meta http-equiv="Content-type" content="text/html; charset=iso-8859-1"/>
		<meta http-equiv="X-UA-Compatible" content="IE=9"/>
		<title>:: IDEAL 10 - Servicios P&uacute;blicos</title>
		<link href="css/css2.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<script type="text/javascript" src="css/calendario.js"></script>
		<script type="text/javascript" src="css/programas.js"></script>
		<style>
			.onoffswitch
			{
				position: relative; width: 71px;
				-webkit-user-select:none;
				-moz-user-select:none;
				-ms-user-select: none;
			}
			.onoffswitch-checkbox {display: none;}
			.onoffswitch-label
			{
				display: block;
				overflow: hidden;
				cursor: pointer;
				border: 2px solid #DDE6E2;
				border-radius: 20px;
			}
			.onoffswitch-inner
			{
				display: block;
				width: 200%;
				margin-left: -100%;
				transition: margin 0.3s ease-in 0s;
			}
			.onoffswitch-inner:before, .onoffswitch-inner:after
			{
				display: block;
				float: left;
				width: 50%;
				height: 23px;
				padding: 0;
				line-height: 23px;
				font-size: 14px;
				color: white;
				font-family: Trebuchet, Arial, sans-serif;
				font-weight: bold;
				box-sizing: border-box;
			}
			.onoffswitch-inner:before
			{
				content: "SI";
				padding-left: 10px;
				background-color: #51C3E0;
				color: #FFFFFF;
			}
			.onoffswitch-inner:after
			{
				content: "NO";
				padding-right: 10px;
				background-color: #EEEEEE; color: #999999;
				text-align: right;
			}
			.onoffswitch-switch 
			{
				display: block;
				width: 17px;
				margin: 3px;
				background: #FFFFFF;
				position: absolute;
				top: 0;
				bottom: 0;
				right: 44px;
				border: 2px solid #DDE6E2;
				border-radius: 20px;
				transition: all 0.3s ease-in 0s;
			}
			.onoffswitch-checkbox:checked + .onoffswitch-label .onoffswitch-inner {margin-left: 0;}
			.onoffswitch-checkbox:checked + .onoffswitch-label .onoffswitch-switch {right: 0px;}
		</style>
		<script>
			function despliegamodal2(_valor, _table)
			{
                document.getElementById('bgventanamodal2').style.visibility = _valor;
                
                if (_table == 'srvclientes')
                {
                    document.getElementById('ventana2').src = 'ventana-clienteservicio.php?table=' + _table;
                }
                else if(_table == 'srvsubsidios')
                {
                    document.getElementById('ventana2').src = 'subsidio-ventana.php';
                }
            }
			function despliegamodalm(_valor,_tip,mensa,pregunta)
			{
				document.getElementById("bgventanamodalm").style.visibility=_valor;
				if(_valor=="hidden"){document.getElementById('ventanam').src="";}
				else
				{
					switch(_tip)
					{
						case "1":	document.getElementById('ventanam').src="ventana-mensaje1.php?titulos="+mensa;break;
						case "2":	document.getElementById('ventanam').src="ventana-mensaje3.php?titulos="+mensa;break;
						case "3":	document.getElementById('ventanam').src="ventana-mensaje2.php?titulos="+mensa;break;
						case "4":	document.getElementById('ventanam').src="ventana-consulta1.php?titulos="+mensa+"&idresp="+pregunta;break;	
					}
				}
			}
			function respuestaModalBusqueda2(tabla, id, nombre, serial, referencia)
			{
				switch(tabla)
				{
					case 'srvclientes': 
						document.getElementById('cliente').value = id;
						document.getElementById('nCliente').value = nombre;
						document.form2.submit();
					break;	
                }
            }
			function funcionmensaje()
			{
				var idban=document.getElementById('codban').value;
				document.location.href="serv-editaAsignacionSubsidios.php?idban=" +actual+ "&scrtop=" +scrtop+ "&numpag=" +numpag+ "&limreg=" +limreg+ "&filtro=" +filtro;
			}
			function respuestaconsulta(pregunta)
			{
				switch(pregunta)
				{
					case "1":	document.form2.oculto.value='2';
								document.form2.submit();
								break;
				}
			}
			function guardar()
			{
				var v1 = document.getElementById('codban').value;

				if (v1 != "") 
                {
                    despliegamodalm('visible','4','Esta Seguro de Guardar','1');
                }
				else {
                    despliegamodalm('visible','2','Falta información para crear la Asignación');
                }
			}
			function actualizar()
			{
				document.form2.submit();
			}
			function cambiocheck()
			{
				if(document.getElementById('myonoffswitch').value=='S'){document.getElementById('myonoffswitch').value='N';}
				else{document.getElementById('myonoffswitch').value='S';}
				document.form2.submit();
			}
			
			function iratras(scrtop, numpag, limreg, filtro)
			{
				var idcta=document.getElementById('codban').value;
				location.href="serv-buscaAsignacionSubsidios.php?idcta="+idcta+"&scrtop="+scrtop+"&numpag="+numpag+"&limreg="+limreg+ "&filtro="+filtro;
			}
			function adelante(scrtop, numpag, limreg, filtro)
			{
				var maximo=document.getElementById('maximo').value;
				var actual=document.getElementById('codban').value;
				actual=parseFloat(actual)+1;
				if(actual<=parseFloat(maximo))
				{
					if(actual<10){actual="0"+actual;}
					location.href="serv-editaAsignacionSubsidios.php?idban=" +actual+ "&scrtop=" +scrtop+ "&numpag=" +numpag+ "&limreg=" +limreg+ "&filtro=" +filtro;
				}
			}
			function atrasc(scrtop, numpag, limreg, filtro, prev)
			{
				var minimo=document.getElementById('minimo').value;
				var actual=document.getElementById('codban').value;
				actual=parseFloat(actual)-1;
				if(actual>=parseFloat(minimo))
				{
					if(actual<10){actual="0"+actual;}
					location.href="serv-editaAsignacionSubsidios.php?idban=" +actual+ "&scrtop=" +scrtop+ "&numpag=" +numpag+ "&limreg=" +limreg+ "&filtro=" +filtro;
				}
			}
		</script>
		<?php titlepag();?>
	</head>
	<body>
		<IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
		<span id="todastablas2"></span>
		<?php
			$numpag=@ $_GET['numpag'];
			$limreg=@ $_GET['limreg'];
			$scrtop=26*$totreg;
		?>
		<table>
			<tr><script>barra_imagenes("serv");</script><?php cuadro_titulos();?></tr>	 
			<tr><?php menu_desplegable("serv");?></tr>
			<tr>
				<td colspan="3" class="cinta">
                    <img src="imagenes/add.png" title="Nuevo" onClick="location.href='serv-asignacionSubsidios.php'" class="mgbt"/>
                    <img src="imagenes/guarda.png" title="Guardar" onClick="guardar()" class="mgbt"/>
                    <img src="imagenes/busca.png" title="Buscar" onClick="location.href='serv-buscaAsignacionSubsidios.php'" class="mgbt"/>
                    <img src="imagenes/nv.png" title="Nueva Ventana" onClick="<?php echo paginasnuevas("serv");?>" class="mgbt">
                    <img src="imagenes/iratras.png" title="Atr&aacute;s" onClick="iratras(<?php echo "$scrtop, $numpag, $limreg, $filtro"; ?>)" class="mgbt"/>
                </td>
			</tr>
		</table>
		<div id="bgventanamodalm" class="bgventanamodalm">
			<div id="ventanamodalm" class="ventanamodalm">
				<IFRAME src="" name="ventanam" marginWidth=0 marginHeight=0 frameBorder=0 id="ventanam" frameSpacing=0 style=" width:700px; height:130px; top:200; overflow:hidden;"></IFRAME>
			</div>
		</div>
		<form name="form2" method="post" action="">
			<?php
				if(@ $_POST['oculto']=="")
				{
					$sqlr="SELECT MIN(id), MAX(id) FROM srvasignacion_subsidios";
					$res=mysqli_query($linkbd,$sqlr);
					$r=mysqli_fetch_row($res);
					$_POST['minimo']=$r[0];
					$_POST['maximo']=$r[1];
					
					$sqlr="SELECT particular FROM srvasignacion_subsidios WHERE id='".$_GET['idban']."'";
					$resp = mysqli_query($linkbd,$sqlr);
					$row = mysqli_fetch_row($resp); 

                    if($row[0] == 'S')
                    {
                        $sqlr = "SELECT * FROM srvasignacion_subsidios WHERE id='".$_GET['idban']."'";
                        $resp = mysqli_query($linkbd,$sqlr);
                        $row = mysqli_fetch_row($resp);

                        $_POST['tipo'] = 'P';
                        $_POST['codban']	  = $row[0];
                        $_POST['cliente']	  = $row[1];
                        $_POST['subsidio']    = $row[3];
                        $_POST['onoffswitch'] = $row[6];

                        $sqlCliente = "SELECT id_tercero FROM srvclientes WHERE id = '$row[1]' ";
                        $respCliente = mysqli_query($linkbd,$sqlCliente);
                        $rowCliente = mysqli_fetch_row($respCliente);
                                
                        $sqlTercero = "SELECT nombre1,nombre2,apellido1,apellido2,razonsocial FROM terceros WHERE id_tercero = '$rowCliente[0]' ";
                        $respTercero = mysqli_query($linkbd,$sqlTercero);
                        $rowTercero = mysqli_fetch_assoc($respTercero);
                                    
                        $nombreCompleto = $rowTercero['nombre1'].' '.$rowTercero['nombre2'].' '.$rowTercero['apellido1'].' '.$rowTercero['apellido2'];
                        if ($rowTercero['razonsocial']!='') 
                        {
                            $nombreCompleto = $rowTercero['razonsocial'];
                        }
                        
                        $_POST['nCliente'] = $nombreCompleto;

                        $sqlSubsidio = "SELECT nombre FROM srvsubsidios WHERE id = '$row[3]' ";
                        $respSubsidio = mysqli_query($linkbd,$sqlSubsidio);
                        $rowSubsidio = mysqli_fetch_row($respSubsidio);

                        $_POST['nSubsidio'] = $rowSubsidio[0];
                    }
                    
                    if($row[0] == 'N')
                    {
                        $sqlr = "SELECT * FROM srvasignacion_subsidios WHERE id='".$_GET['idban']."'";
                        $resp = mysqli_query($linkbd,$sqlr);
                        $row = mysqli_fetch_row($resp);
                        
                        $_POST['tipo'] = 'G';
                        $_POST['codban']	  = $row[0];
                        $_POST['idestrato']	  = $row[2];
                        $_POST['subsidio']    = $row[3];
                        $_POST['onoffswitch'] = $row[6];

                        $sqlSubsidio = "SELECT nombre FROM srvsubsidios WHERE id = '$row[3]' ";
                        $respSubsidio = mysqli_query($linkbd,$sqlSubsidio);
                        $rowSubsidio = mysqli_fetch_row($respSubsidio);

                        $_POST['nSubsidio'] = $rowSubsidio[0];
                    }
				}
			?>
			<table class="inicio" align="center">
                <tr>
                    <td class="titulos" colspan="2">.: Asignaci&oacute;n de Subsidio</td>
                    <td  class="cerrar" style="width: 1%;"><a href="serv-principal.php">Cerrar</a></td>
                </tr>

                <tr>
                    <td style="width:5%" class="saludo1">Tipo de subsidio:</td>
			        <td style="width:8%; height:30px;">
				        <select style="width:100%; height:30px;" name="tipo" id="tipo"  onChange="actualizar()">
					        <option value="P" <?php if($_POST['tipo']=='P') echo "SELECTED"?>>Particular</option>
					        <option value="G" <?php if($_POST['tipo']=='G') echo "SELECTED"?>>General</option>
				        </select>
			        </td>
                </tr>
            </table>

            <?php
	        if($_POST['tipo']=='P') //***** PARTICULAR
            {
			?>

			<table class="inicio ancho">
				<tr>
					<td class="titulos" colspan="8">.: Ingresar asignaci&oacute;n subsidio Particular</td>
					<td class="cerrar" style="width:7%" onClick="location.href='serv-principal.php'">Cerrar</td>
				</tr>
				<tr>
					<td class="tamano01" style="width:3cm;">C&oacute;digo:</td>
					<td style="width:15%;">
                        <input type="text" name="codban" id="codban" onKeyUp="return tabular(event,this)" value="<?php echo @ $_POST['codban'];?>" style="width:100%;height:30px;text-align:center;" readonly/>
                    </td>
                    <td class="saludo1">Cliente:</td>
					<td>
						<input type="text" name='cliente' id='cliente'  value="<?php echo @$_POST['cliente']?>" style="width: 100%; height: 30px;" onclick = "despliegamodal2('visible', 'srvclientes');"  class="colordobleclik" readonly>
                    </td>
                    <td colspan="4">
						<input type="text" name="nCliente" id="nCliente" value="<?php echo @$_POST['nCliente']?>" style="width:100%;height:30px;" readonly>
					</td>
				</tr>
				<tr>
                    <td class="saludo1">Subsidio:</td>
					<td>
						<input type="text" name='subsidio' id='subsidio'  value="<?php echo @$_POST['subsidio']?>" style="width: 100%; height: 30px;" onclick = "despliegamodal2('visible', 'srvsubsidios');"  class="colordobleclik" readonly>
                    </td>
                    <td colspan="2">
						<input type="text" name="nSubsidio" id="nSubsidio" value="<?php echo @$_POST['nSubsidio']?>" style="width:100%;height:30px;" readonly>
					</td>

                    <td class="tamano01">Estado:</td>
					<td>
						<div class="onoffswitch">
							<input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox" id="myonoffswitch" value="<?php echo @ $_POST['onoffswitch'];?>" <?php if(@ $_POST['onoffswitch']=='S'){echo "checked";}?> onChange="cambiocheck();"/>
							<label class="onoffswitch-label" for="myonoffswitch">
								<span class="onoffswitch-inner"></span>
								<span class="onoffswitch-switch"></span>
							</label>
						</div>
					</td>
				</tr>
			</table>
            <?php 
            } 
            if($_POST['tipo'] == 'G')
            {
            ?>
            <table class="inicio ancho">
				<tr>
					<td class="titulos" colspan="8">.: Ingresar asignaci&oacute;n subsidio General</td>
					<td class="cerrar" style="width:7%" onClick="location.href='serv-principal.php'">Cerrar</td>
				</tr>
				<tr>
					<td class="tamano01" style="width:3cm;">C&oacute;digo:</td>
					<td style="width:15%;">
                        <input type="text" name="codban" id="codban" onKeyUp="return tabular(event,this)" value="<?php echo @ $_POST['codban'];?>" style="width:100%;height:30px;text-align:center;" readonly/>
                    </td>
                    <td class="tamano01">Estrato:</td>
					<td>
						<select name="idestrato" id="idestrato" style="width:100%;height:30px;">
							<option value="-1">:: Seleccione Estrato ::</option>
							<?php
								$sqlr="SELECT id,descripcion FROM srvestratos WHERE estado = 'S' ORDER BY id";
								$resp = mysqli_query($linkbd,$sqlr);
								while ($row =mysqli_fetch_row($resp))
								{
									$sqlUsoSuelo = "SELECT nombre FROM srvusosdesuelo WHERE id = '$row[2]' ";
									$respUsoSuelo = mysqli_query($linkbd,$sqlUsoSuelo);
									$rowUsoSuelo = mysqli_fetch_row($respUsoSuelo);

									if($_POST['idestrato']==$row[0])
									{
										echo "<option value='$row[0]' SELECTED>$row[1] - $rowUsoSuelo[0]</option>";
									}
									else {echo "<option value='$row[0]'>$row[1] - - $rowUsoSuelo[0]</option>";}
								}
							?>
						</select>
					</td>
				</tr>
				<tr>
                    <td class="saludo1">Subsidio:</td>
					<td>
						<input type="text" name='subsidio' id='subsidio'  value="<?php echo @$_POST['subsidio']?>" style="width: 100%; height: 30px;" onclick = "despliegamodal2('visible', 'srvsubsidios');"  class="colordobleclik" readonly>
                    </td>
                    <td colspan="2">
						<input type="text" name="nSubsidio" id="nSubsidio" value="<?php echo @$_POST['nSubsidio']?>" style="width:100%;height:30px;" readonly>
					</td>
                    

                    <td class="tamano01">Estado:</td>
					<td>
						<div class="onoffswitch">
							<input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox" id="myonoffswitch" value="<?php echo @ $_POST['onoffswitch'];?>" <?php if(@ $_POST['onoffswitch']=='S'){echo "checked";}?> onChange="cambiocheck();"/>
							<label class="onoffswitch-label" for="myonoffswitch">
								<span class="onoffswitch-inner"></span>
								<span class="onoffswitch-switch"></span>
							</label>
						</div>
					</td>
				</tr>
			</table>
            <?php 
            }
            ?>
			<input type="hidden" name="maximo" id="maximo" value="<?php echo @ $_POST['maximo']?>"/>
			<input type="hidden" name="minimo" id="minimo" value="<?php echo @ $_POST['minimo']?>"/>
			<input type="hidden" name="oculto" id="oculto" value="1"/>
			<?php
				if(@ $_POST['oculto']=="2")
				{
					if (@ $_POST['onoffswitch']!='S')
					{
						$valest='N';
					}
					else 
					{
						$valest='S';
					}

                    if($_POST['tipo'] == 'P')
                    {
                        $sqlr="UPDATE srvasignacion_subsidios SET id_cliente = '".$_POST['cliente']."', id_subsidio = '".$_POST['subsidio']."', estado = '$valest', id_estrato = 0, particular = 'S', general = 'N' WHERE id='".$_POST['codban']."' ";
                        if ($sqlr!='') 
                        {
                            mysqli_query($linkbd,$sqlr);
                            echo "<script>despliegamodalm('visible','3','Se ha Editado con Exito');</script>";
                        }else {
                            echo"<script>despliegamodalm('visible','2','No se pudo ejecutar la petición');</script>";
                        }
                    }

                    if($_POST['tipo'] == 'G')
                    {
                        $sqlr="UPDATE srvasignacion_subsidios SET id_estrato = '".$_POST['idestrato']."', id_subsidio = '".$_POST['subsidio']."', estado = '$valest', id_cliente = 0, particular = 'N', general = 'S' WHERE id='".$_POST['codban']."' ";
                        if ($sqlr!='') 
                        {
                            mysqli_query($linkbd,$sqlr);
                            echo "<script>despliegamodalm('visible','3','Se ha Editado con Exito');</script>";
                        }else 
                        {
                            echo"<script>despliegamodalm('visible','2','No se pudo ejecutar la petición');</script>";
                        }
                    }
				}
			?>
		</form>
		<div id="bgventanamodal2">
			<div id="ventanamodal2">
				<IFRAME  src="" name="buster" marginWidth=0 marginHeight=0 frameBorder=0 id="ventana2" frameSpacing=0 style="left:500px; width:900px; height:500px; top:200;"></IFRAME>
			</div>
		</div>
	</body>
</html>