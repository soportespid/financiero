<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require "comun.inc";
	require "funciones.inc";
	session_start();
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html lang="es">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1">
		<meta name="viewport" content="user-scalable=no">
		<title>:: IDEAL 10 - Meci Calidad</title>
		<link href="favicon.ico" rel="shortcut icon"/>
		<link href="css/css2z.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet"/>
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet"/>
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet"/>
		<link href="sweetalert2/dist/sweetalert2.min.css" rel="stylesheet">
		<script src="sweetalert2/dist/sweetalert2.min.js"></script>
		<script type="text/javascript" src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script type="text/javascript" src="JQuery/jquery-2.1.4.min.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script>
			function verUltimaPos(idcta, filas, filtro){
				var scrtop = $('#divdet').scrollTop();
				var altura = $('#divdet').height();
				var numpag = $('#nummul').val();
				var limreg = $('#numres').val();
				if((numpag <= 0)||(numpag == "")){
					numpag = 0;
				}
				if((limreg == 0)||(limreg == "")){
					limreg = 10;
				}
				numpag++;
				location.href = "meci-gestiondocedita.php?idproceso=" + idcta + "&scrtop=" + scrtop + "&totreg=" + filas + "&altura=" + altura + "&numpag=" + numpag + "&limreg=" + limreg + "&filtro=" + filtro;
			}
			function eliminar_arch(cod1,narch,nombredel){
				Swal.fire({
					icon: 'question',
					title: '¿Esta Seguro de Eliminar la plantilla de la Clase de Contrato '+nombredel.toUpperCase()+'?',
					showDenyButton: true,
					confirmButtonText: 'Eliminar',
					confirmButtonColor: '#01CC42',
					denyButtonText: 'Cancelar',
					denyButtonColor: '#FF121A',
				}).then(
					(result) => {
						if (result.isConfirmed){
							document.getElementById('idclase').value = cod1;
							document.getElementById('archdel').value = narch;
							document.getElementById('nomdel').value = nombredel;
							document.getElementById('ocudelplan').value = "1";
							document.form2.submit();
						}
						else if (result.isDenied){
							document.getElementById('ocudelplan').value = "2";
							Swal.fire({
								icon: 'info',
								title: 'No se elimino la plantilla',
								confirmButtonText: 'Continuar',
								confirmButtonColor: '#FF121A',
								timer: 2500
							});
						}
					}
				)
			}
			function cambioswitch(id,valor){
				document.getElementById('idestado').value = id;
				if(valor==1){
					Swal.fire({
					icon: 'question',
					title: '¿Desea Activar este Tipo de Documento?',
					showDenyButton: true,
					confirmButtonText: 'Activar',
					confirmButtonColor: '#01CC42',
					denyButtonText: 'Cancelar',
					denyButtonColor: '#FF121A',
					}).then(
						(result) => {
							if (result.isConfirmed){
								document.form2.cambioestado.value = "1";
								document.form2.submit();
							}
							else if (result.isDenied){
								document.form2.nocambioestado.value = "1";
								Swal.fire({
									icon: 'info',
									title: 'No se Activo el Tipo de Documento',
									confirmButtonText: 'Continuar',
									confirmButtonColor: '#FF121A',
									timer: 2500
								}).then((result) => {
									if (result.dismiss === Swal.DismissReason.timer || result.isConfirmed) {
										document.form2.submit();
									}
								});  
							}
						}
					)
				}
				else{
					Swal.fire({
					icon: 'question',
					title: '¿Desea Desactivar este Tipo de Documento?',
					showDenyButton: true,
					confirmButtonText: 'Activar',
					confirmButtonColor: '#01CC42',
					denyButtonText: 'Cancelar',
					denyButtonColor: '#FF121A',
					}).then(
						(result) => {
							if (result.isConfirmed){
								document.form2.cambioestado.value = "0";
								document.form2.submit();
							}
							else if (result.isDenied){
								document.form2.nocambioestado.value = "0";
								Swal.fire({
									icon: 'info',
									title: 'No se Desactivo el Tipo de Documento',
									confirmButtonText: 'Continuar',
									confirmButtonColor: '#FF121A',
									timer: 2500
								}).then((result) => {
									if (result.dismiss === Swal.DismissReason.timer || result.isConfirmed) {
										document.form2.submit();
									}
								});  
							}
						}
					)
				}
			}
			function anular(id){
				Swal.fire({
					icon: 'question',
					title: '¿Esta Seguro de Eliminar la documentación ' + id + '?',
					showDenyButton: true,
					confirmButtonText: 'Eliminar',
					confirmButtonColor: '#01CC42',
					denyButtonText: 'Cancelar',
					denyButtonColor: '#FF121A',
				}).then(
					(result) => {
						if (result.isConfirmed){
							document.getElementById('eliminadocumento').value = id;
							document.form2.submit();
						}
						else if (result.isDenied){
							Swal.fire({
								icon: 'info',
								title: 'No se elimino la documentación',
								confirmButtonText: 'Continuar',
								confirmButtonColor: '#FF121A',
								timer: 2500
							});
						}
					}
				)
			}
		</script>
		<?php
			$scrtop = $_GET['scrtop'];
			if($scrtop == "") {
				$scrtop = 0;
			}
			echo"<script>
				window.onload=function(){
					$('#divdet').scrollTop(".$scrtop.")
				}
			</script>";
			$gidcta = "'".$_GET['idcta']."'";
			if(isset($_GET['filtro'])){
				$_POST['nombre'] = $_GET['filtro'];
			}
		?>
	</head>
	<body>
		<IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
		<span id="todastablas2"></span>
		<table>
			<tr><script>barra_imagenes("meci");</script><?php cuadro_titulos();?></tr>	 
			<tr><?php menu_desplegable("meci");?></tr>
			<tr>
				<td colspan="3" class="cinta">
					<img src="imagenes/add.png" onClick="location.href='meci-gestiondoc.php'" class="mgbt" title="Nuevo"/>
					<img src="imagenes/guardad.png" class="mgbt1"/>
					<img src="imagenes/busca.png" onClick="document.form2.submit()" class="mgbt" title="Buscar"/>
					<img src="imagenes/nv.png" onClick="mypop=window.open('meci-principal.php','','');mypop.focus();" class="mgbt" title="Nueva Ventana">
					<img src="imagenes/duplicar_pantalla.png" onClick="mypop=window.open('<?php echo $url2; ?>','','');mypop.focus();" class="tooltip bottom mgbt" title="Duplicar pestaña">
				</td>
			</tr>
		</table>	
		<?php
			if($_GET['numpag'] != ""){
				$oculto = $_POST['oculto'];
				if($oculto != 2){
					$_POST['numres'] = $_GET['limreg'];
					$_POST['numpos'] = $_GET['limreg']*($_GET['numpag']-1);
					$_POST['nummul'] = $_GET['numpag']-1;
				}
			}else{
				if($_POST['nummul'] == ""){
					$_POST['numres'] = 10;
					$_POST['numpos'] = 0;
					$_POST['nummul'] = 0;
				}
			}
		?>
		<form name="form2" method="post" action="meci-gestiondocbusca.php" enctype="multipart/form-data">
			<input type="hidden" name="cambioestado" id="cambioestado" value="<?php echo $_POST['cambioestado'];?>"/>
			<input type="hidden" name="nocambioestado" id="nocambioestado" value="<?php echo $_POST['nocambioestado'];?>"/>
			<?php
				if($_POST['oculto'] == ""){
					$_POST['cambioestado'] = "";
					$_POST['nocambioestado'] = "";
					$_POST['eliminadocumento'] = '';
				}
				//*****************************************************************
				if($_POST['cambioestado'] != ""){
					if($_POST['cambioestado'] == "1"){
						$sqlr = "UPDATE calgestiondoc SET estado='S' WHERE  codigospid = '".$_POST['idestado']."'";
						mysqli_fetch_row(mysqli_query($linkbd,$sqlr)); 
					}else{
						$sqlr = "UPDATE calgestiondoc SET estado='N' WHERE codigospid = '".$_POST['idestado']."'";
						mysqli_fetch_row(mysqli_query($linkbd,$sqlr)); 
					}
					echo"<script>document.form2.cambioestado.value = ''</script>";
				}
				//*****************************************************************
				if($_POST['nocambioestado'] != ""){
					if($_POST['nocambioestado'] == "1"){
						$_POST['lswitch1'][$_POST['idestado']] = 1;
					}else {
						$_POST['lswitch1'][$_POST['idestado']] = 0;
					}
					echo"<script>document.form2.nocambioestado.value = ''</script>";
				}
			?>
			<table  class="inicio ancho">
				<tr>
					<td class="titulos" colspan="4">:: Buscar Gesti&oacute;n Documental </td>
					<td class="cerrar" style="width:7%" onClick="location.href='meci-principal.php'">Cerrar</td>
				</tr>
				<tr>
					<td class="saludo1" style="width:5%">Proceso:</td>
					<td style="width:30%"><input type="text" name="nombre" id="nombre" value="<?php echo $_POST['nombre'];?>" style="width:100%"/></td>
					<td class="saludo1" style="width:13%">C&oacute;digo SPID:</td>
					<td><input type="text" name="documento" id="documento" value="<?php echo $_POST['documento'];?>"  maxlength="16"/></td>
				</tr>                       
			</table>
			<input type="hidden" name="oculto" id='oculto' value="1">
			<input type="hidden" name="iddel" id="iddel" value="<?php echo $_POST['iddel']?>">
			<input type="hidden" name="ocudel" id="ocudel" value="<?php echo $_POST['ocudel']?>">
			<input type="hidden" name="archdel" id="archdel" value="<?php echo $_POST['archdel']?>">
			<input type="hidden" name="eliminadocumento" id="eliminadocumento" value="<?php echo $_POST['eliminadocumento']?>">
			<input type="hidden" name="nomdel" id="nomdel" value="<?php echo $_POST['nomdel']?>">
			<input type="hidden" name="idclase" id="idclase" value="<?php echo $_POST['idclase']?>">
			<input type="hidden" name="ocudelplan" id="ocudelplan" value="<?php echo $_POST['ocudelplan']?>">
			<input type="hidden" name="contador" id="contador" value="<?php echo $_POST['contador']?>">
			<input type="hidden" name="idestado" id="idestado" value="<?php echo $_POST['idestado'];?>"/>
			<input type="hidden" name="numres" id="numres" value="<?php echo $_POST['numres'];?>"/>
			<input type="hidden" name="numpos" id="numpos" value="<?php echo $_POST['numpos'];?>"/>
			<input type="hidden" name="nummul" id="nummul" value="<?php echo $_POST['nummul'];?>"/>
			<div class="subpantallac5" style="height:68%; width:99.6%; overflow-x:hidden;">
				<?php
					//Eliminar documento
					if($_POST['eliminadocumento'] != ''){
						$sql = "DELETE FROM calgestiondoc WHERE codigospid = '".$_POST['eliminadocumento']."'";
						mysqli_query($linkbd,$sql);
						$sql = "DELETE FROM callistadoc WHERE codigospid = '".$_POST['eliminadocumento']."'";
						mysqli_query($linkbd,$sql);
						echo "
						<script>
							document.form2.eliminadocumento.value = '';
						</script>";
					}
					//Eliminar Archivos
					if($_POST['ocudelplan']=="1"){
						$sqlr = "UPDATE callistadoc SET nomarchivo = '' WHERE id = '".$_POST['idclase']."'";
						mysqli_query($linkbd,$sqlr);
						$filename = "informacion/calidad_documental/documentos/" . $_POST['archdel'];
						if (file_exists($filename)) {
							unlink($filename);
						}
						echo "
						<script>
							document.form2.ocudelplan.value = '2';
							document.form2.idclase.value = '';
							document.form2.archdel.value = '';
							document.form2.nomdel.value = '';
						</script>";
					}
					//Cargar Archivo
					if (is_uploaded_file($_FILES['upload']['tmp_name'][$_POST['contador']])){	
						$trozos = explode(".",$_FILES['upload']['name'][$_POST['contador']]);  
						$extension = end($trozos);  
						$nomar = $_POST['nomdel'].".".$extension;
						copy($_FILES['upload']['tmp_name'][$_POST['contador']], "informacion/calidad_documental/documentos/".$nomar);
						$sqlr = "UPDATE callistadoc SET nomarchivo = '$nomar' WHERE id = '".$_POST['idclase']."'";
						mysqli_query($linkbd,$sqlr);
					}
					$contad = 0;
					$crit1 = " ";
					$crit2 = " ";
					if ($_POST['nombre'] != ""){
						$sqlr2 = "SELECT id FROM calprocesos WHERE nombre LIKE '%".$_POST['nombre']."%'";
						$res2 = mysqli_query($linkbd,$sqlr2);
						$row2 = mysqli_fetch_row($res2);
						$proceso = $row2[0];
						$crit1 = " AND (cgd.proceso='$proceso') ";
					}
					if ($_POST['documento'] != ""){
						$crit2 = " AND cgd.codigospid LIKE '%$_POST[documento]%' ";
					}
					$sqlr = "SELECT cgd.*, cld.* FROM calgestiondoc cgd, callistadoc cld WHERE cgd.idarchivo = cld.id $crit1 $crit2";
					$resp = mysqli_query($linkbd,$sqlr);
					$_POST['numtop'] = mysqli_num_rows($resp);
					$nuncilumnas = ceil($_POST['numtop']/$_POST['numres']);
					$sqlr = "SELECT cgd.*, cld.* FROM calgestiondoc cgd, callistadoc cld WHERE cgd.idarchivo = cld.id  $crit1 $crit2 ORDER BY cgd.proceso, cgd.id LIMIT ".$_POST['numpos'].",".$_POST['numres'];
					$resp = mysqli_query($linkbd,$sqlr);
					$ntr = mysqli_num_rows($resp);
					$con = 1;
					$numcontrol = $_POST['nummul']+1;
					if($nuncilumnas == $numcontrol){
						$imagenforward = "<img src='imagenes/forward02.png' style='width:17px'>";
						$imagensforward = "<img src='imagenes/skip_forward02.png' style='width:16px'>";
					}else{
						$imagenforward = "<img src='imagenes/forward01.png' style='width:17px' title='Siguiente' onClick='numsiguiente()'>";
						$imagensforward = "<img src='imagenes/skip_forward01.png' style='width:16px' title='Fin' onClick='saltocol(\"$nuncilumnas\")'>";
					}
					if($_POST['numpos'] == 0){
						$imagenback = "<img src='imagenes/back02.png' style='width:17px'>";
						$imagensback = "<img src='imagenes/skip_back02.png' style='width:16px'>";
					}else{
						$imagenback = "<img src='imagenes/back01.png' style='width:17px' title='Anterior' onClick='numanterior();'>";
						$imagensback = "<img src='imagenes/skip_back01.png' style='width:16px' title='Inicio' onClick='saltocol(\"1\")'>";
					}
					echo "
						<table class='inicio' align='center' width='80%'>
							<tr>
								<td colspan='11' class='titulos'>.: Resultados Busqueda:</td>
								<td class='submenu'>
									<select name='renumres' id='renumres' onChange='cambionum();' style='width:100%'>
										<option value='10'"; if ($_POST['renumres']=='10'){echo 'selected';} echo ">10</option>
										<option value='20'"; if ($_POST['renumres']=='20'){echo 'selected';} echo ">20</option>
										<option value='30'"; if ($_POST['renumres']=='30'){echo 'selected';} echo ">30</option>
										<option value='50'"; if ($_POST['renumres']=='50'){echo 'selected';} echo ">50</option>
										<option value='100'"; if ($_POST['renumres']=='100'){echo 'selected';} echo ">100</option>
									</select>
								</td>
							</tr>
							<tr>
								<td colspan='6'>Encontrados:".$_POST['numtop']."</td>
							</tr>
							<tr>
								<td class='titulos2' style='width:10%'>C&oacute;digo SPID</td>
								<td class='titulos2'>Procesos</td>
								<td class='titulos2' style='width:10%'>Documentos</td>
								<td class='titulos2' style='width:10%'>Pol&iacute;ticas</td>
								<td class='titulos2' style='width:20%'>Titulo</td>
								<td class='titulos2' style='width:5%;text-align:center;' colspan='4'>Plantilla</td>
								<td class='titulos2' style='width:3%'>Versi&oacute;n</td>
								<td class='titulos2' style='width:6%'>Estado</td>
								<td class='titulos2' style='width:5%'>Eliminar</td>
							</tr>";	
					$iter = 'saludo1a';
					$iter2 = 'saludo2';
					$filas = 1;
					while ($row = mysqli_fetch_row($resp)){
						$sqlr2 = "SELECT nombre FROM calprocesos WHERE id = '$row[1]'";
						$res2 = mysqli_query($linkbd,$sqlr2);
						$row2 = mysqli_fetch_row($res2);
						$procesos = $row2[0];
						$sqlr2 = "SELECT nombre FROM caldocumentos WHERE id = '$row[2]'";
						$res2 = mysqli_query($linkbd,$sqlr2);
						$row2 = mysqli_fetch_row($res2);
						$documentos = $row2[0];
						$sqlr2 = "SELECT descripcion_valor FROM dominios WHERE nombre_dominio = 'TIPOS_DE_POLITICAS' AND valor_inicial = '$row[3]'";
						$res2 = mysqli_query($linkbd,$sqlr2);
						$row2 = mysqli_fetch_row($res2);
						$politicas = $row2[0];
						if($row[8] == 'S') {
							$coloracti = "#0F0";
							$_POST['lswitch1'][$row[4]] = 0;
						}else {
							$coloracti = "#C00";
							$_POST['lswitch1'][$row[4]] = 1;
						}
						if($gidcta != ""){
							if($gidcta == "'".$row[4]."'"){
								$estilo='background-color:#FF9';
							}else{
								$estilo="";
							}
						}else{
							$estilo="";
						}
						if ($row[15]!=""){
							$bdescargar = "<a href='informacion/calidad_documental/documentos/$row[15]' target='_blank' ><img src='imagenes/descargar.png'  title='(Descargar)' ></a>";
							$beliminar = "<a href='#' onClick='eliminar_arch(\"$row[9]\",\"$row[15]\",\"$row[10].\");'><img src='imagenes/cross.png'></a>";
							$bcargar = "<div class='upload' style='display:none'><input type='file' name='upload[]'/></div><img src='imagenes/del3.png' >";
							$clase = 'garbageY';
							$titulo = 'Planilla Asignada';
							$okelimina = '';
						}else{
							$bdescargar = "<img src='imagenes/vacio2.png' title='(Sin Plantilla)'>";
							$beliminar = "<img src='imagenes/del4.png'>";
							$bcargar = "<div class='upload'><input type='file' name='upload[]' onFocus='document.form2.contador.value=$contad; document.form2.idclase.value=\"$row[9]\";document.form2.nomdel.value=\"$row[10]\";' onChange='document.form2.submit();' /><img src='imagenes/attach.png'  title='(Cargar)' /> </div>";
							$clase = 'garbageX';
							$titulo = 'Eliminar';
							$okelimina = "anular('".$row[4]."')";
						}
						if($row[16] == "N"){
							$enrepara="<img src='imagenes/b_tblops.png' title='(En Mejora)'>";
						}else{
							$enrepara = "<img src='imagenes/confirm.png' title='(Activo)' title='(Activo)'>";
						}
						$contad++;
						if($politicas == ""){
							$nombredel = "$procesos\\n$documentos";
						}else{
							$nombredel = "$procesos\\n$politicas";
						}

						$idcta = "'".$row[4]."'";
						$numfil = "'".$filas."'";
						$filtro = "'".$_POST['nombre']."'";
						echo "
							<tr class='$iter' onDblClick=\"verUltimaPos($idcta, $numfil, $filtro)\" $estilo'>
								<td>$row[4]</td>
								<td >$procesos</td>
								<td >$documentos</td>
								<td >$politicas</td>
								<td >$row[6]</td>
								<td style='text-align:center;'>$bdescargar</td>
								<td style='text-align:center;'>$bcargar</td>
								<td style='text-align:center;'>$beliminar</td>
								<td style='text-align:center;'>$enrepara</td>
								<td style='text-align:center;'>$row[11]</td>
								<td class='centrartext'><input type='range' name='lswitch1[]' value='".$_POST['lswitch1'][$row[4]]."' min ='0' max='1' step ='1' style='background:$coloracti; width:60%' onChange='cambioswitch(\"$row[4]\",\"".$_POST['lswitch1'][$row[4]]."\")' /></td>
								<td style='font: 110% sans-serif; display: flex; justify-content: center;' onclick=\"$okelimina\"><div class='$clase' title='$titulo'></td>
							</tr>";
							$con+=1;
							$aux = $iter;
							$iter = $iter2;
							$iter2 = $aux;
							$filas++;
						}
					echo"
						</table>
						<table class='inicio'>
							<tr>
								<td style='text-align:center;'>
									<a href='#'>$imagensback</a>&nbsp;
									<a href='#'>$imagenback</a>&nbsp;&nbsp;";
					if($nuncilumnas <= 9){$numfin = $nuncilumnas;}
					else{$numfin=9;}
					for($xx = 1; $xx <= $numfin; $xx++)
					{
						if($numcontrol<=9){
							$numx=$xx;
						}else{
							$numx=$xx+($numcontrol-9);
						}
						if($numcontrol==$numx){echo"<a href='#' onClick='saltocol(\"$numx\")'; style='color:#24D915'> $numx </a>";}
						else {echo"<a href='#' onClick='saltocol(\"$numx\")'; style='color:#000000'> $numx </a>";}
					}
					echo "		&nbsp;&nbsp;<a href='#'>$imagenforward</a>
									&nbsp;<a href='#'>$imagensforward</a>
								</td>
							</tr>
						</table>";
				?>
			</div>
			<input type="hidden" name="numtop" id="numtop" value="<?php echo $_POST['numtop'];?>" />
		</form>
	</body>
</html>