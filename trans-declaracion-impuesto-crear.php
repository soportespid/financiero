<?php

    header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require 'comun.inc';
	require 'funciones.inc';
	session_start();
    if(empty($_SESSION)){
        header("location: index.php");
    }
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html lang=es>
	<head>
    <meta charset="utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1"/>
		<meta name="viewport" content="user-scalable=no">
		<title></title>
		<link href="favicon.ico" rel="shortcut icon"/>
		<link href="css/css2z.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
        <link href="css/style.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" href="sweetalert2/dist/sweetalert2.min.css">
        <script src="sweetalert2/dist/sweetalert2.min.js"></script>
		<script src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
	</head>
	<body>
		<header>
			<table>
				<tr><script>barra_imagenes("trans");</script><?php cuadro_titulos();?></tr>
			</table>
		</header>

        <main id="myapp" v-cloak>
            <input type="hidden" value = "1" ref="pageType">
            <div class="loading-container" v-show="isLoading" >
                <p class="text-loading"data-loading="IDEAL 10">IDEAL 10</p>
            </div>
            <nav>
                <table>
                    <tr><?php menu_desplegable("trans");?></tr>
                </table>
                <div class="bg-white group-btn p-1" id="newNavStyle">
                    <button type="button" @click="save()" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
                        <span>Guardar</span>
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960">
                            <path d="M840-680v480q0 33-23.5 56.5T760-120H200q-33 0-56.5-23.5T120-200v-560q0-33 23.5-56.5T200-840h480l160 160Zm-80 34L646-760H200v560h560v-446ZM480-240q50 0 85-35t35-85q0-50-35-85t-85-35q-50 0-85 35t-35 85q0 50 35 85t85 35ZM240-560h360v-160H240v160Zm-40-86v446-560 114Z">
                            </path>
                        </svg>
                    </button>
                    <button type="button" @click="window.location.href='trans-declaracion-impuesto-buscar'" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
                        <span>Buscar</span>
                        <svg class="fill-black group-hover:fill-white w-5 h-5" xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960"><path d="M784-120 532-372q-30 24-69 38t-83 14q-109 0-184.5-75.5T120-580q0-109 75.5-184.5T380-840q109 0 184.5 75.5T640-580q0 44-14 83t-38 69l252 252-56 56ZM380-400q75 0 127.5-52.5T560-580q0-75-52.5-127.5T380-760q-75 0-127.5 52.5T200-580q0 75 52.5 127.5T380-400Z"></path></svg>
                    </button>
                    <button type="button" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center" @click="mypop=window.open('trans-principal.php','',''); mypop.focus();">
                        <span>Nueva ventana</span>
                        <svg class="fill-black group-hover:fill-white w-5 h-5" xmlns="http://www.w3.org/2000/svg"  viewBox="0 -960 960 960" ><path d="M320-120v-80h80v-80H160q-33 0-56.5-23.5T80-360v-400q0-33 23.5-56.5T160-840h640q33 0 56.5 23.5T880-760v400q0 33-23.5 56.5T800-280H560v80h80v80H320ZM160-360h640v-400H160v400Zm0 0v-400 400Z"/></svg>
                    </button>
                    <button type="button" @click="mypop=window.open('trans-declaracion-impuesto-crear','','');mypop.focus();" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
                        <span class="group-hover:text-white">Duplicar pantalla</span>
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960"><path d="M640-160v-360H160v360h480Zm80-200v-80h80v-360H320v200h-80v-200q0-33 23.5-56.5T320-880h480q33 0 56.5 23.5T880-800v360q0 33-23.5 56.5T800-360h-80ZM160-80q-33 0-56.5-23.5T80-160v-360q0-33 23.5-56.5T160-600h480q33 0 56.5 23.5T720-520v360q0 33-23.5 56.5T640-80H160Zm400-603ZM400-340Z"></path></svg>
                    </button>
                    <button type="button" @click="window.location.href='trans-declaracion-impuesto-buscar'" class="btn btn-success btn-success-hover d-flex justify-between align-items-center">
                        <span>Atras</span>
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960"><path d="M760-200v-160q0-50-35-85t-85-35H273l144 144-57 56-240-240 240-240 57 56-144 144h367q83 0 141.5 58.5T840-360v160h-80Z"></path></svg>
                    </button>
                </div>
            </nav>
            <section class="bg-white">
                <div>
                    <h2 class="titulos m-0">Crear declaración de impuesto vehicular</h2>
                    <p class="m-0 ms-2">Todos los campos con (<span class="text-danger fw-bolder">*</span>) son obligatorios.</p>
                        <div class="d-flex">
                            <div class="d-flex w-50">
                                <div class="form-control">
                                    <label class="form-label m-0" for="">Fecha trámite<span class="text-danger fw-bolder">*</span></label>
                                    <input type="date" v-model="strFecha" @change="getImpuesto()">
                                </div>
                                <div class="form-control">
                                    <label class="form-label m-0" for="">Placa<span class="text-danger fw-bolder">*</span></label>
                                    <input type="text" class="colordobleclik" readonly v-model="objAuto.placa" @dblclick="getSearch(1,'placas');isModal=true">
                                </div>
                            </div>
                            <div class="d-flex w-50">
                                <div class="form-control">
                                    <label class="form-label m-0" for="">Fecha matrícula</label>
                                    <input type="date" v-model="objAuto.fecha" disabled>
                                </div>
                                <div class="form-control">
                                    <label class="form-label m-0" for="">Departamento</label>
                                    <input type="text" v-model="objAuto.departamento" disabled>
                                </div>
                                <div class="form-control">
                                    <label class="form-label m-0" for="">Municipio</label>
                                    <input type="text" v-model="objAuto.municipio" disabled>
                                </div>
                            </div>
                        </div>
                        <div class="d-flex">
                            <div class="d-flex w-50">
                                <div class="form-control w-50">
                                    <label class="form-label m-0" for="">Clase</label>
                                    <input type="text" v-model="objAuto.clase" disabled>
                                </div>
                                <div class="form-control w-50">
                                    <label class="form-label m-0" for="">Marca</label>
                                    <input type="text" v-model="objAuto.marca" disabled>
                                </div>
                                <div class="form-control">
                                    <label class="form-label m-0" for="">Línea</label>
                                    <input type="text" v-model="objAuto.linea" disabled>
                                </div>
                                <div class="form-control w-50">
                                    <label class="form-label m-0" for="">Modelo</label>
                                    <input type="text" v-model="objAuto.modelo" disabled>
                                </div>
                            </div>
                            <div class="d-flex w-50">
                                <div class="form-control">
                                    <label class="form-label m-0" for="">Tipo combustible</label>
                                    <input type="text" v-model="objAuto.combustible" disabled>
                                </div>
                                <div class="form-control">
                                    <label class="form-label m-0" for="">Cilindraje</label>
                                    <input type="text" v-model="objAuto.cilindraje" disabled>
                                </div>
                                <div class="form-control">
                                    <label class="form-label m-0" for="">Capacidad de carga</label>
                                    <input type="text" v-model="objAuto.carga" disabled>
                                </div>
                            </div>
                        </div>
                        <div class="d-flex">
                            <div class="d-flex w-50">
                                <div class="form-control">
                                    <label class="form-label m-0" for="">Cantidad pasajeros</label>
                                    <input type="text" v-model="objAuto.pasajeros" disabled>
                                </div>
                                <div class="form-control">
                                    <label class="form-label m-0" for="">Potencia/HP</label>
                                    <input type="text" v-model="objAuto.potencia" disabled>
                                </div>
                                <div class="form-control">
                                    <label class="form-label m-0" for="">Tipo de público</label>
                                    <input type="text" v-model="objAuto.publico" disabled>
                                </div>

                            </div>
                            <div class="d-flex w-50">
                                <div class="form-control">
                                    <label class="form-label m-0" for="">Carrocería</label>
                                    <input type="text" v-model="objAuto.carroceria" disabled>
                                </div>
                                <div class="form-control">
                                    <label class="form-label m-0" for="">Vehículo antiguo</label>
                                    <input type="text" v-model="objAuto.antiguo == 'S' ? 'Si' : 'No'" disabled>
                                </div>
                                <div class="form-control">
                                    <label class="form-label m-0" for="">Blindaje</label>
                                    <input type="text" v-model="objAuto.blindaje == 'S' ? 'Si' : 'No'" disabled>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div>
                    <h2 class="titulos m-0">Propietario</h2>
                    <div class="d-flex">
                        <div class="form-control">
                            <label class="form-label m-0" for="">Nombre</label>
                            <input type="text" v-model="objAuto.nombre_tercero" disabled>
                        </div>
                        <div class="form-control">
                            <label class="form-label m-0" for="">CC</label>
                            <input type="text" v-model="objAuto.propietario" disabled>
                        </div>
                        <div class="form-control">
                            <label class="form-label m-0" for="">Teléfono</label>
                            <input type="text" v-model="objAuto.celular" disabled>
                        </div>
                        <div class="form-control">
                            <label class="form-label m-0" for="">Departamento</label>
                            <input type="text" v-model="objAuto.departamento_tercero" disabled>
                        </div>
                        <div class="form-control">
                            <label class="form-label m-0" for="">Municipio</label>
                            <input type="text" v-model="objAuto.municipio_tercero" disabled>
                        </div>
                        <div class="form-control">
                            <label class="form-label m-0" for="">Dirección</label>
                            <input type="text" v-model="objAuto.direccion" disabled>
                        </div>
                    </div>
                </div>
                <div>
                    <h2 class="titulos m-0">Detalle</h2>
                    <div class="table-responsive overflow-auto" style="height:30vh">
                        <table class="table fw-normal">
                            <thead>
                                <tr class="text-center">
                                    <th >
                                        <div class="d-flex justify-center">
                                            <div class="d-flex align-items-center ">
                                                <label for="labelCheckAll" class="form-switch">
                                                    <input type="checkbox" id="labelCheckAll" v-model="isCheckAll" checked @change="changeAll()">
                                                    <span></span>
                                                </label>
                                            </div>
                                        </div>
                                    </th>
                                    <th>Vigencia</th>
                                    <th>Avalúo</th>
                                    <th>Tarifa</th>
                                    <th>Impuesto</th>
                                    <th>Descuento</th>
                                    <th>Sancion</th>
                                    <th>Dias mora</th>
                                    <th>Interés mora</th>
                                    <th>Total a pagar</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr v-for="(data,index) in arrImpuestos" :key="index">
                                    <td >
                                        <div class="d-flex justify-center">
                                            <div class="d-flex align-items-center ">
                                                <label :for="'labelCheckName'+index" class="form-switch">
                                                    <input type="checkbox" :id="'labelCheckName'+index" @change="changeStatus(index)" :checked="data.selected">
                                                    <span></span>
                                                </label>
                                            </div>
                                        </div>
                                    </td>
                                    <td class="text-center">{{data.vigencia}}</td>
                                    <td class="text-right">{{formatMoney(data.avaluo)}}</td>
                                    <td class="text-center">{{data.tarifa}}%</td>
                                    <td class="text-right">{{formatMoney(data.impuesto)}}</td>
                                    <td class="text-right">{{formatMoney(data.impuesto_descuento)}}</td>
                                    <td class="text-right">{{formatMoney(data.sancion)}}</td>
                                    <td class="text-center">{{data.dias_mora}}</td>
                                    <td class="text-right">{{formatMoney(data.interes_mora)}}</td>
                                    <td class="text-right fw-bold">{{formatMoney(data.total)}}</td>
                                </tr>
                                <tr>
                                    <td class="text-right fw-bold">Total</td>
                                    <td></td>
                                    <td class="text-right fw-bold">{{ formatMoney(objTotal.avaluo)}}</td>
                                    <td></td>
                                    <td class="text-right fw-bold">{{ formatMoney(objTotal.impuesto)}}</td>
                                    <td class="text-right fw-bold">{{ formatMoney(objTotal.impuesto_descuento)}}</td>
                                    <td class="text-right fw-bold">{{ formatMoney(objTotal.sancion)}}</td>
                                    <td></td>
                                    <td class="text-right fw-bold">{{ formatMoney(objTotal.interes_mora)}}</td>
                                    <td class="text-right fw-bold">{{ formatMoney(objTotal.total)}}</td>
                                </tr>
                            </tbody>
                        </table>
                </div>
            </section>
            <!-- MODALES-->
            <div v-show="isModal" class="modal">
                <div class="modal-dialog modal-xl">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title">Registro automotores</h5>
                            <button type="button" @click="isModal=false;" class="btn btn-close"><div></div><div></div></button>
                        </div>
                        <div class="modal-body">
                            <div class="d-flex">
                                <div class="form-control w-25">
                                    <label class="form-label" for="">Por página:</label>
                                    <select v-model="selectPorPagina">
                                        <option value="25">25</option>
                                        <option value="50">50</option>
                                        <option value="100">100</option>
                                        <option value="250">250</option>
                                        <option value="500">500</option>
                                        <option value="1000">1000</option>
                                        <option value="">Todo</option>
                                    </select>
                                </div>
                                <div class="form-control justify-between">
                                    <div></div>
                                    <div class="d-flex">
                                        <input type="search" placeholder="Buscar por placa" v-model="strBuscar">
                                        <button type="button" @click="getSearch(1,'placas')" class="btn btn-primary w-25">Buscar</button>
                                    </div>
                                </div>
                            </div>
                            <div class="table-responsive overflow-auto" style="height:50vh">
                                <table class="table table-hover fw-normal">
                                    <thead>
                                        <tr class="text-center">
                                            <th>Id</th>
                                            <th>Placa</th>
                                            <th>Fecha matrícula</th>
                                            <th>CC/NIT</th>
                                            <th>Nombre</th>
                                            <th>Departamento</th>
                                            <th>Municipio</th>
                                            <th>Clase</th>
                                            <th>Marca</th>
                                            <th>Línea</th>
                                            <th>Modelo</th>
                                            <th>Tipo combustible</th>
                                            <th>Cilindraje</th>
                                            <th>Capacidad de carga</th>
                                            <th>Pasajeros</th>
                                            <th>Potencia/Hp</th>
                                            <th>Tipo público</th>
                                            <th>Carrocería</th>
                                            <th>Antiguo</th>
                                            <th>Blindaje</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr v-for="(data,index) in arrData" @dblclick="selectItem(data,'placas')">
                                            <td>{{data.id}}</td>
                                            <td>{{data.placa}}</td>
                                            <td>{{data.fecha_format}}</td>
                                            <td>{{data.propietario}}</td>
                                            <td>{{data.nombre_tercero}}</td>
                                            <td>{{data.departamento}}</td>
                                            <td>{{data.municipio}}</td>
                                            <td>{{data.clase}}</td>
                                            <td>{{data.marca}}</td>
                                            <td>{{data.linea}}</td>
                                            <td>{{data.modelo}}</td>
                                            <td>{{data.combustible}}</td>
                                            <td>{{data.cilindraje}}</td>
                                            <td>{{data.carga}}</td>
                                            <td>{{data.pasajeros}}</td>
                                            <td>{{data.potencia}}</td>
                                            <td>{{data.publico}}</td>
                                            <td>{{data.carroceria}}</td>
                                            <td>{{data.antiguo == "S" ? "Si" : "No"}}</td>
                                            <td>{{data.blindaje == "S" ? "Si" : "No"}}</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div v-if="arrData.length > 0" class="list-pagination-container">
                                <p>Página {{ intPagina }} de {{ intTotalPaginas }}</p>
                                <ul class="list-pagination">
                                    <li v-show="intPagina > 1" @click="getSearch(intPagina = 1,'placas')"><< </li>
                                    <li v-show="intPagina > 1" @click="getSearch(--intPagina,'placas')"><</li>
                                    <li v-for="(pagina,index) in arrBotones" :class="intPagina == pagina ? 'active' : ''" @click="getSearch(pagina,'placas')" :key="index">{{pagina}}</li>
                                    <li v-show="intPagina < intTotalPaginas" @click="getSearch(++intPagina,'placas')">></li>
                                    <li v-show="intPagina < intTotalPaginas" @click="getSearch(intPagina = intTotalPaginas,'placas')" >>></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </main>
        <script src="Librerias/vue/vue.min.js"></script>
		<script type="module" src="transporte/js/functions_declaracion_impuesto.js?<?= date('d_m_Y_h_i_s');?>"></script>
	</body>
</html>
