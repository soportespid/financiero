<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");

	require 'comun.inc';
	require 'funciones.inc';

	session_start();
	date_default_timezone_set("America/Bogota");

?>
<!DOCTYPE >
<html lang="es">
	<head>
		<meta charset="utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1"/>
		<meta name="viewport" content="user-scalable=no">
		<title>:: Ideal - Informes</title>
		<link href="favicon.ico" rel="shortcut icon"/>

		<link href="css/css2.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/css2z.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<script type="text/javascript" src="css/programas.js"></script>
		<script type="text/javascript" src="css/calendario.js"></script>
		<script type="text/javascript" src="JQuery/jquery-2.1.4.min.js"></script>
		<link href="css/style.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>

		<style>
			.inicio--no-shadow{
				box-shadow: none;
			}

			[v-cloak]{
				display : none;
			}

			.seleccionarTodos {
				padding: 4px 0px 4px 10px;
				background: linear-gradient(#337CCF, #1450A3);
				border: 1px solid;
				border-top-color: #0A6EBD;
				border-right-color: #0A6EBD;
				border-bottom-color: #337CCF;
				border-left-color: #337CCF;
				box-shadow: 4px 4px 2px #368eb880;
				border-radius: 5px;
				display: flex;
				align-items: center;
				flex-direction: row;
				text-align: center;
				gap: 5px;
				color: #fff;
				cursor: pointer;
				font-size: 0.8rem;
			}

			.span-check{
				margin-right: 0.5em;
				text-align: center;
				width: 1.5rem;
				height: 1.5rem;
				background-color: rgb(12, 176, 252);
				border: 2px solid;
				border-color: rgb(46, 136, 238);
				border-radius: 5px;
				cursor: pointer;
			}

			.span-check-completed {
				margin-right: 0.5em;
				text-align: center;
				width: 1.5rem;
				height: 1.5rem;
				background-color: rgb(12, 176, 252);
				border: 2px solid;
				border-color: rgb(46, 136, 238);
				border-radius: 5px;
				cursor: pointer;
				background-image: url("data:image/svg+xml,%3csvg viewBox='0 0 16 16' fill='white' xmlns='http://www.w3.org/2000/svg'%3e%3cpath d='M12.207 4.793a1 1 0 010 1.414l-5 5a1 1 0 01-1.414 0l-2-2a1 1 0 011.414-1.414L6.5 9.086l4.293-4.293a1 1 0 011.414 0z'/%3e%3c/svg%3e");
				background-size:auto;
			}
		</style>
	</head>
	<body>
		<IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
		<span id="todastablas2"></span>
		<div id="myapp" v-cloak>
			<table>
				<tr><script>barra_imagenes("info");</script><?php cuadro_titulos();?></tr>
				<tr><?php menu_desplegable("info");?></tr>
				<tr>
					<td colspan="3" class="cinta">
						<a><img src="imagenes/add.png" title="Nuevo" onClick="location.href='ccp-parametrizaringresoscuipo.php'" class="mgbt"/></a>
						<a class="mgbt" ><img src="imagenes/guarda.png" v-on:click="guardarinfo()"/></a>
						<a><img src="imagenes/buscad.png" class="mgbt"/></a>
						<a href="#" onClick="mypop=window.open('info-principal.php','','');mypop.focus();" class="mgbt"><img src="imagenes/nv.png" title="Nueva Ventana"></a>
						<img src="imagenes/iratras.png" title="Atr&aacute;s" onClick="location.href='ccp-menucuipo.php'" class="mgbt"/>
					</td>
				</tr>
			</table>
			<div class="subpantalla" style="height:78%; width:99.6%; overflow:hidden; resize: vertical;">

				<div class="row">
					<div class="col-12">
						<table class="inicio">
							<tr>
								<td class="titulos" colspan="2">.: Parametrizar Ingresos CUIPO:</td>
								<td class="cerrar" style="width:7%" onClick="location.href='info-principal.php'">Cerrar</td>
							</tr>
						</table>
					</div>
				</div>
				<div class="d-flex w-100" style="max-height:60vh" >
					<div class="w-50">
                        <div class="d-flex">
                            <div class="form-control">
                                <label class="form-label">Buscar Rubro:</label>
                                <input type="search" placeholder="filtre por cuenta ccpet, fuente, nombre cuenta ccpet o nombre fuente" v-on:keyup="buscarRubro" v-model="buscar_rubro">
                            </div>
                            <div class="form-control">
                                <label class="form-label">Vigencia:</label>
                                <select v-model="vigencia"  v-on:Change="cargarParametros" >
                                        <option v-for="year in years" :value="year[0]">{{ year[0] }}</option>
                                </select>
                            </div>
                            <div class="form-control">
                                <label class="form-label">Seccion:</label>
                                <select v-model="SecPresupuestal"  v-on:Change="cargarParametros">
                                        <option v-for="SelSecPresup in SecPresup" :value="SelSecPresup[5]">{{ SelSecPresup[0] }} - {{ SelSecPresup[1] }}</option>
                                </select>
                            </div>
                        </div>
						<div>
							<table>
								<thead>
									<tr>
										<td class='titulos' width="30%"  style="font: 160% sans-serif; border-radius: 5px 0px 0px 0px;">Rubro</td>
										<td class='titulos'  style="font: 160% sans-serif;">Nombre</td>
										<td v-show="mostrarCheck" width = "20%" v-on:click = "selTodo">
											<div class = "seleccionarTodos" >
												Seleccionar todos <span v-bind:class="checkTodo ? 'span-check-completed' : 'span-check'"> </span>
											</div>
										</td>
									</tr>
								</thead>
							</table>
						</div>
						<div class="table-responsive" >
							<table class='inicio inicio--no-shadow'>
								<tbody v-if="show_resultados">

									<tr v-for="(result, index) in results" v-on:click="seleccionaCodigos(result[0])" :style="estaEnArray(result) == true ? myStyle : ''" v-bind:class="index % 2 ? 'contenidonew00' : 'contenidonew01'" style="cursor: pointer;">
										<td width="30%" style="font: 160% sans-serif; padding: 5px;">{{ result[0] }}</td>
										<td style="font: 160% sans-serif;">{{ result[1] }}</td>

									</tr>
								</tbody>
								<tbody v-else>
									<tr>
										<td width="20%"style="font: 120% sans-serif; padding-left:10px; text-align:center;" colspan="3">Sin resultados</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>

					<div class="w-50">
						<div class="d-flex" >
                            <div class="form-control">
                                <label class="form-label">Detalle Sectorial:</label>
                                <select v-model="detSectorial" >
                                    <option v-for="SeldetSecto in detSecto" :value="SeldetSecto[0]">{{ SeldetSecto[0] }} - {{ SeldetSecto[1] }}</option>
                                </select>
                            </div>
                        </div>
                        <div class="d-flex">
                            <div class="form-control">
                                <label class="form-label">Destinación Especifica:</label>
                                <select v-model="destEspecifica" v-on:change="valida_destEspecifica">
                                    <option value='SI'>SI</option>
                                    <option value='NO'>NO</option>
                                </select>
                            </div>
                            <div class="form-control" v-show="show_destEspecifica">
                                <label class="form-label">Tipo de Norma:</label>
                                <select  v-model="tiponorma">
                                    <option v-for="SeltNorma in tNorma" :value="SeltNorma[0]">{{ SeltNorma[0] }} - {{ SeltNorma[1] }}</option>
                                </select>
                            </div>
                        </div>
                        <div v-show="show_destEspecifica">
                            <div class="d-flex">
                                <div class="form-control">
                                    <label class="form-label">Fuente:</label>
                                    <select  v-model="numFuente">
                                        <option v-for="SelnFuente in nFuente" :value="SelnFuente[0]">{{ SelnFuente[0] }} - {{ SelnFuente[1] }}</option>
                                    </select>
                                </div>
                            </div>
                            <div class="d-flex">
                                <div class="form-control">
                                    <label class="form-label">Número de norma:</label>
                                    <input v-model="numeroNorma" type="text"  placeholder="Número de la norma">
                                </div>
                                <div class="form-control">
                                    <label class="form-label">Fecha de norma:</label>
                                    <input type="text" id="fecha" name="fecha"  class="bg-warning cursor-pointer" title="DD/MM/YYYY" placeholder="Fecha Norma" onDblClick="displayCalendarFor('fecha');" readonly autocomplete="off">
                                </div>
                            </div>
                            <div class="d-flex">
                                <div class="form-control">
                                    <label class="form-label">Porcentaje:</label>
                                    <input v-model="valorPorcentaje" type="text" placeholder="Porcentaje">
                                </div>
                                <div class="form-control">
                                    <label class="form-label">Valor:</label>
                                    <select v-model="tipoValor">
                                        <option value='0'>Porcentaje</option>
                                        <option value='1'>Valor</option>
                                    </select>
                                </div>
                            </div>
                        </div>
						<div class="table-responsive">
							<table>
								<thead>
									<tr>
									<th class="titulosnew00" style="width:20%;">Cuenta</th>
									<th class="titulosnew00" style="width:10%;">D.S.</th>
									<th class="titulosnew00" style="width:10%;">D.E.</th>
									<th class="titulosnew00" style="width:10%;">T. Norma</th>
									<th class="titulosnew00" style="width:10%;">N° Norma</th>
									<th class="titulosnew00" style="width:15%;">Fecha</th>
									<th class="titulosnew00" style="width:15%;">Fuentes</th>
									<th class="titulosnew01">Porcentaje</th>
									</tr>
								</thead>
                                <tbody v-if="show_resultados_seleccionados">
                                    <tr v-for="(result2, index) in results_seleccionadosOrd" v-bind:class="index % 2 ? 'contenidonew00' : 'contenidonew01'" style="cursor: pointer; font: 100% sans-serif;">
                                        <td width="20%"  style=" padding: 5px;">{{ result2[0] }}</td>
                                        <td style="width:10%;text-align:Center;">{{ result2[1] }}</td>
                                        <td style="width:10%;text-align:Center;">{{ result2[2] }}</td>
                                        <td style="width:10%;text-align:Center;">{{ result2[3] }}</td>
                                        <td style="width:10%;text-align:Center;">{{ result2[4] }}</td>
                                        <td style="width:15%;text-align:Center;">{{ result2[5] }}</td>
                                        <td style="width:15%;text-align:Center;">{{ result2[6] }}</td>
                                        <td style="text-align:Center;">{{ result2[7] }}</td>
                                    </tr>
                                </tbody>
                                <tbody v-else>
                                    <tr>
                                        <td width="20%"style="font: 120% sans-serif; padding-left:10px; text-align:center;" colspan="3">Sin Información Almacenada</td>
                                    </tr>
                                </tbody>
							</table>
						</div>
					</div>
				</div>

				<div id="cargando" v-if="loading" class="loading">
					<span>Cargando...</span>
				</div>

			</div>
		</div>

		<script type="module" src="./presupuesto_ccpet/clasificadores/ccp-parametrizaringresoscuipo.js"></script>
		<script src="Librerias/vue/axios.min.js"></script>
		<script src="sweetalert2/dist/sweetalert2.min.js"></script>
		<link rel="stylesheet" href="sweetalert2/dist/sweetalert2.min.css">
		<script src="https://unpkg.com/vue@3/dist/vue.global.js"></script>
	</body>
</html>
