<?php
    header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");

	require 'comun.inc';
	require 'funciones.inc';

	session_start();
    date_default_timezone_set("America/Bogota");

?>

<html lang="en">
    <head>
        <meta charset="utf-8"/>
        <meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1"/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>:: IDEAL 10 - Parametrización</title>
        <link href="favicon.ico" rel="shortcut icon"/>

        <link href="css/css2.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
        <link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
        <link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
        <script type="text/javascript" src="css/programas.js"></script>
        <script type="text/javascript" src="css/calendario.js"></script>
        <script type="text/javascript" src="JQuery/jquery-2.1.4.min.js"></script>

        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">

        <style>
            [v-cloak]{
                display : none;
            }

            label{
                font-size:13px;
            }

            input{
                height: calc(1em + 0.6rem + 0.5px) !important;
                font-size: 14px !important;
                /* margin-top: 4px !important; */
            }

            /* pretty radio */
            label > input[type="radio"] {
                display: none;
            }
            label > input[type="radio"] + *::before {
            content: "";
            display: inline-block;
            vertical-align: bottom;
            width: 1rem;
            height: 1rem;
            margin-right: 0.3rem;
            border-radius: 50%;
            border-style: solid;
            border-width: 0.1rem;
            border-color: gray;
            }
            label > input[type="radio"]:checked + * {
            color: teal;
            }
            label > input[type="radio"]:checked + *::before {
            background: radial-gradient(teal 0%, teal 40%, transparent 50%, transparent);
            border-color: teal;
            }

            /* basic layout */
            fieldset {
            margin: 20px;
            max-width: 400px;
            }
            label > input[type="radio"] + * {
            display: inline-block;
            padding: 0.5rem 1rem;
            }
        </style>

    </head>
    <body>
        <IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
        <span id="todastablas2"></span>

        <header>
            <table>
                <tr><script>barra_imagenes("para");</script><?php cuadro_titulos();?></tr>
            </table>
        </header>

        <section id="myapp" v-cloak>
            <nav>
                <table>
                    <tr><?php menu_desplegable("para");?></tr>
                </table>
                <div class="bg-white group-btn p-1">
                    <button type="button" onclick="window.location.href='ccp-ordenpresupuestal.php'" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
                        <span>Nuevo</span>
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960">
                            <path d="M440-440H200v-80h240v-240h80v240h240v80H520v240h-80v-240Z"></path>
                        </svg>
                    </button>
                    <button type="button" @click = "guardarOrdenPresu" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
                        <span>Guardar</span>
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960">
                            <path
                                d="M840-680v480q0 33-23.5 56.5T760-120H200q-33 0-56.5-23.5T120-200v-560q0-33 23.5-56.5T200-840h480l160 160Zm-80 34L646-760H200v560h560v-446ZM480-240q50 0 85-35t35-85q0-50-35-85t-85-35q-50 0-85 35t-35 85q0 50 35 85t85 35ZM240-560h360v-160H240v160Zm-40-86v446-560 114Z">
                            </path>
                        </svg>
                    </button>
                    <button type="button" onclick="window.open('para-principal');"
                        class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
                        <span>Nueva ventana</span>
                        <svg class="fill-black group-hover:fill-white w-5 h-5" xmlns="http://www.w3.org/2000/svg"
                            viewBox="0 -960 960 960">
                            <path
                                d="M320-120v-80h80v-80H160q-33 0-56.5-23.5T80-360v-400q0-33 23.5-56.5T160-840h640q33 0 56.5 23.5T880-760v400q0 33-23.5 56.5T800-280H560v80h80v80H320ZM160-360h640v-400H160v400Zm0 0v-400 400Z">
                            </path>
                        </svg>
                    </button>
                </div>
            </nav>

            <article>
                <table class="inicio ancho">

                    <tr>
                        <td class="titulos" colspan="9" >Orden presupuestal </td>
                        <td class="cerrar" style="width:7%" onClick="location.href='ccp-principal.php'">Cerrar</td>
                    </tr>

                    <tr>
                        <td style = "width: 10%; text-align: center">
                            <span style="color: #a6aec1;"></span>
                        </td>

                        <td class = "textoNewR" style = "width: 5%;">
                            <span style="color: #a6aec1;">2023502510001</span>
                        </td>

                        <td class = "textoNewR" style = "width: 14%;">
                            <span style="color: #a6aec1;">240204100</span>
                        </td>

                        <td class = "textoNewR" style = "width: 14%;">
                            <span style="color: #a6aec1;">1.2.4.3.04</span>
                        </td>

                        <td class = "textoNewR" style = "width: 8%;">
                            <span style="color: #a6aec1;">CSF</span>
                        </td>

                        <td class = "textoNewR" style = "width: 8%;">
                            <span style="color: #a6aec1;">1</span>
                        </td>
                        <td></td>
                        <td></td>

                    </tr>

                    <tr>
                        <td style = "width: 10%; text-align: center">
                            <label>
                                <input type="radio" name="radio-button" value="1"  v-model="ordenPresu"/>
                                <span></span>
                            </label>
                        </td>

                        <td class = "textoNewR" style = "width: 8%;">
                            <label class="labelR" >
                                BPIM
                            </label>
                        </td>

                        <td class = "textoNewR" style = "width: 14%;">
                            <label class="labelR">
                                Indicador producto (Program&aacute;tico)
                            </label>
                        </td>

                        <td class = "textoNewR" style = "width: 14%;">
                            <label class="labelR">
                                Fuente
                            </label>
                        </td>

                        <td class = "textoNewR" style = "width: 8%;">
                            <label class="labelR">
                                Medio de pago
                            </label>
                        </td>

                        <td class = "textoNewR" style = "width: 8%;">
                            <label class="labelR">
                                Vigencia del gasto
                            </label>
                        </td>
                        <td style = "width: 8%;"></td>
                        <td></td>

                        <td rowspan="3" style= "border-left: thick solid #cfd5e1;  height:12%; position:absolute; display:flex; justify-content:center; align-items: center;">
                            <p style="padding-left: 10px;">Adici&oacute;n Presupuestal, Reducci&oacute;n Presupuestal, Traslado presupuestal, Cdp's.</p>
                        </td>

                    </tr>
                    <tr>
                        <td colspan="8">
                        <hr/>
                        </td>
                    </tr>
                    <tr>
                        <td style = "width: 10%; text-align: center">
                            <span style="color: #a6aec1;"></span>
                        </td>

                        <td class = "textoNewR" style = "width: 5%;">
                            <span style="color: #a6aec1;">24</span>
                        </td>

                        <td class = "textoNewR" style = "width: 14%;">
                            <span style="color: #a6aec1;">2402</span>
                        </td>

                        <td class = "textoNewR" style = "width: 14%;">
                            <span style="color: #a6aec1;">240204100</span>
                        </td>

                        <td class = "textoNewR" style = "width: 8%;">
                            <span style="color: #a6aec1;">2023502510001</span>
                        </td>

                        <td class = "textoNewR" style = "width: 8%;">
                            <span style="color: #a6aec1;">1.2.4.3.04</span>
                        </td>
                        <td class = "textoNewR" style = "width: 8%;">
                            <span style="color: #a6aec1;">CSF</span>
                        </td>
                        <td class = "textoNewR" style = "width: 8%;">
                            <span style="color: #a6aec1;">1</span>
                        </td>
                        <td></td>

                    </tr>
                    <tr>
                        <td style = "text-align: center">
                            <label>
                                <input type="radio" name="radio-button" value="2"  v-model="ordenPresu"/>
                                <span></span>
                            </label>
                        </td>

                        <td class = "textoNewR">
                            <label class="labelR">
                                Sector
                            </label>
                        </td>

                        <td class = "textoNewR">
                            <label class="labelR">
                                Programa
                            </label>
                        </td>

                        <td class = "textoNewR">
                            <label class="labelR">
                                Indicador producto (Program&aacute;tico)
                            </label>
                        </td>

                        <td class = "textoNewR">
                            <label class="labelR">
                                BPIM
                            </label>
                        </td>

                        <td class = "textoNewR">
                            <label class="labelR">
                                Fuente
                            </label>
                        </td>

                        <td class = "textoNewR">
                            <label class="labelR">
                                Medio de pago
                            </label>
                        </td>

                        <td class = "textoNewR" style = "width: 8%;">
                            <label class="labelR">
                                Vigencia del gasto
                            </label>
                        </td>
                        <td></td>
                    </tr>

                </table>



                <div id="cargando" v-if="loading" class="loading">
                    <span>Cargando...</span>
                </div>

            </article>
        </section>

        <script type="module" src="./presupuesto_ccpet/parametros/ccp-ordenpresupuestal.js"></script>
        <script src="Librerias/vue/axios.min.js"></script>
        <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/vue/3.2.37/vue.global.min.js"></script> -->
        <!-- <script src="https://unpkg.com/vue@3"></script> -->
        <script src="sweetalert2/dist/sweetalert2.min.js"></script>
        <link rel="stylesheet" href="sweetalert2/dist/sweetalert2.min.css">
        <script src="https://unpkg.com/vue@3/dist/vue.global.js"></script>

    </body>
</html>
