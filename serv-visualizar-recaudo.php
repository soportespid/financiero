<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require "comun.inc";
	require "funciones.inc";
    require "conversor.php";
	session_start();
	$linkbd = conectar_v7();
    $linkbd -> set_charset("utf8");
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
	//error_reporting(E_ALL);
?>
<!DOCTYPE >
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es">
	<head>
		<meta http-equiv="Content-type" content="text/html; charset=iso-8859-1"/>
		<meta http-equiv="X-UA-Compatible" content="IE=9"/>
		<title>:: IDEAL 10 - Servicios P&uacute;blicos</title>
		<link href="css/css2.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
        <link href="css/cssSP.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<script type="text/javascript" src="css/programas.js"></script>
        <script type="text/javascript" src="css/calendario.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script src="sweetalert2/dist/sweetalert2.min.js"></script>
		<link rel="stylesheet" href="sweetalert2/dist/sweetalert2.min.css">

		<script>
            function pdf()
			{
				document.form2.action="serv-pdf-recaudo.php";
				document.form2.target="_BLANK";
				document.form2.submit(); 
				document.form2.action="";
				document.form2.target="";
			}
		</script>

		<?php titlepag();?>
	</head>
	<body>
		<IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
		<span id="todastablas2"></span>
		<table>
			<tr><script>barra_imagenes("serv");</script><?php cuadro_titulos();?></tr>

			<tr><?php menu_desplegable("serv");?></tr>

			<tr>
				<td colspan="3" class="cinta">
                    <a href="serv-crear-recaudo.php" class="mgbt"><img src="imagenes/add.png"/></a>
					<a onclick="guardar()" class="mgbt"><img src="imagenes/guarda.png" title="Guardar"/></a>
					<a href="serv-buscar-recaudo.php" class="mgbt"><img src="imagenes/busca.png" title="Buscar"/></a>
					<a onclick="mypop=window.open('plan-agenda.php','','');mypop.focus()" class="mgbt"><img src="imagenes/agenda1.png" title="Agenda" /></a>
					<a onclick="mypop=window.open('serv-principal.php','','');mypop.focus();" class="mgbt"><img src="imagenes/nv.png" title="Nueva ventana"></a>
					<a onclick="mypop=window.open('<?php echo $url2; ?>','','');mypop.focus();" class="mgbt"><img src="imagenes/duplicar_pantalla.png" title="Duplicar pesta&ntilde;a"></a>
                    <a onClick="pdf()" class="mgbt"><img src="imagenes/print.png" style="width:29px;height:25px;" title="Imprimir" /></a>
                    <a href="serv-buscar-recaudo.php" class="mgbt"><img src="imagenes/iratras.png" title="Atrás"></a>
                </td>
			</tr>
		</table>

		<div id="bgventanamodalm" class="bgventanamodalm">
			<div id="ventanamodalm" class="ventanamodalm">
				<IFRAME src="" name="ventanam" marginWidth=0 marginHeight=0 frameBorder=0 id="ventanam" frameSpacing=0 style=" width:700px; height:130px; top:200; overflow:hidden;"></IFRAME>
			</div>
		</div>

		<form name="form2" method="post" action="">
			<?php 
                $vigusu=vigencia_usuarios($_SESSION['cedulausu']);

				if(@$_POST['oculto']=="")
				{
                    $codigoRecaudo = $_GET['codRec'];
                    
                    $sqlRecaudo = "SELECT RF.codigo_recaudo, RF.id_cliente, RF.cod_usuario, RF.documento, RF.suscriptor, RF.numero_factura, RF.fecha_recaudo, RF.medio_pago, RF.numero_cuenta, RF.concepto, RF.valor_pago, RF.usuario, RF.estado FROM srv_recaudo_factura AS RF WHERE RF.codigo_recaudo = $codigoRecaudo";
                    $rowRecaudo = mysqli_fetch_assoc(mysqli_query($linkbd,$sqlRecaudo));

                    $_POST['numeroRecaudo'] = $rowRecaudo['codigo_recaudo'];
                    $_POST['numeroFactura'] = $rowRecaudo['numero_factura'];
                    $_POST['codUsuario'] = $rowRecaudo['cod_usuario'];
                    $_POST['documento'] = $rowRecaudo['documento'];
                    $_POST['nombreSuscriptor'] = $rowRecaudo['suscriptor'];
                    $_POST['fecha'] = date('d-m-Y',strtotime($rowRecaudo['fecha_recaudo']));
                    $_POST['valorFacturaVisible'] = "$".number_format($rowRecaudo['valor_pago'],2);
                    $_POST['valorFactura'] = $rowRecaudo['valor_pago'];
                    $_POST['concepto'] = $rowRecaudo['concepto'];
                    $_POST['modoRecaudo'] = $rowRecaudo['medio_pago'];

                    $sqlBancos = "SELECT ncuentaban FROM tesobancosctas WHERE cuenta = '$rowRecaudo[numero_cuenta]'";
                    $rowBancos = mysqli_fetch_row(mysqli_query($linkbd, $sqlBancos));

                    $_POST['cuentaRecaudo'] = $rowBancos[0];

                    $_POST['estado'] = $rowRecaudo['estado'];
                    $_POST['letras'] = convertirdecimal($_POST['valorFactura'],'.');

                    if($_POST['modoRecaudo'] == "banco")
                    {
                        $sqlBanco = "SELECT nombre FROM cuentasnicsp WHERE cuenta = '$rowRecaudo[numero_cuenta]' ";
                        $rowBanco = mysqli_fetch_assoc(mysqli_query($linkbd,$sqlBanco));

                        $_POST['cuentaBanco'] = $rowBanco['nombre'];
                    }

                    $sqlCorte = "SELECT id_corte FROM srvcortes_detalle WHERE numero_facturacion = $_POST[numeroFactura]";
                    $rowCorte = mysqli_fetch_row(mysqli_query($linkbd,$sqlCorte));

                    $sql = "SET lc_time_names = 'es_ES'";
                    mysqli_query($linkbd,$sql);

                    $sqlr = "SELECT numero_corte, UPPER(MONTHNAME(fecha_inicial)), UPPER(MONTHNAME(fecha_final)), YEAR(fecha_inicial), YEAR(fecha_final) FROM srvcortes WHERE numero_corte = $rowCorte[0]";
                    $resp = mysqli_query($linkbd,$sqlr);
                    $row = mysqli_fetch_row($resp);
                    
                    $corte = $row[0]." - " .$row[1]. " " .$row[3]. " - " .$row[2]. " " .$row[4];
                    
                    $_POST['descripcion'] = "Recaudo completo de la factura numero $_POST[numeroFactura] del corte liquidado $corte";
				}				
			?>
            <div>
                <table class="inicio ancho">
                    <tr>
                        <td class="titulos" colspan="6">.: Recaudo Facturación</td>
                        <td class="cerrar" style="width:7%" onClick="location.href='serv-principal.php'">Cerrar</td>
                    </tr>
                </table>

                <div class="subpantalla" style="height:30%; width:99.5%; float:left; overflow: hidden;">
					<table class="inicio grande">
                        <tr>
                            <td class="titulos" colspan="9">Datos Recaudo: </td>
                        </tr>

                        <tr>
                            <td class="tamano01" style="width:10%;">Número de Recaudo:</td>
                            <td style="width:10%;">
                                <input  type="text" name="numeroRecaudo" id="numeroRecaudo" value="<?php echo @ $_POST['numeroRecaudo'];?>" style="width:100%;text-align:center;" readonly/>
                            </td>

                            <td class="tamano01" style="width:10%;">N° de Factura:</td>

                            <td style="width:15%;">
                                <input type="text" name="numeroFactura" id="numeroFactura" value="<?php echo @ $_POST['numeroFactura'];?>" style="text-align:center; width:100%;" readonly/>
                            </td>

                            <td class="tamano01" style="width: 10%;">Código Usuario:</td> 
                            <td style="width: 15%">
                                <input type="text" name='codUsuario' id='codUsuario' value="<?php echo @$_POST['codUsuario'];?>" style="text-align:center; width: 100%;" readonly/>

                                <input type="hidden" name="id_cliente" id="id_cliente" value="<?php echo $_POST['id_cliente'] ?>">
                            </td>
                        </tr>

                        <tr>
                            <td class="tamano01" style="width: 10%;">Documento Suscriptor: </td>
							<td>
								<input type="text" name="documento" id="documento" value="<?php echo @ $_POST['documento'];?>" style="text-align: center; width: 100%;" readonly/>
							</td>

                            <td class="tamano01" style="width: 10%;">Suscriptor: </td>
							<td>
								<input type="text" name="nombreSuscriptor" id="nombreSuscriptor" value="<?php echo @ $_POST['nombreSuscriptor'];?>" style="text-align: center; width: 100%;" readonly/>
							</td>

                            <td class="tamano01" style="width:10%;">Fecha:</td>
							<td style="width:15%;">
								<input type="text" name="fecha" id="fecha" value="<?php echo @ $_POST['fecha']?>" onchange="" maxlength="10" onKeyUp="return tabular(event,this)" onKeyDown="mascara(this,'/',patron,true)" title="DD/MM/YYYY" placeholder="DD/MM/YYYY" style="height:30px;text-align:center;width:90%;" readonly/>
							</td>
                        </tr>

                        <tr>
                            <td class="tamano01">Valor Factura: </td>
							<td>
                                <input type="text" name="valorFacturaVisible" id="valorFacturaVisible" value="<?php echo $_POST['valorFacturaVisible'] ?>" style="text-align:center; width: 100%;" readonly>	
								<input type="hidden" name="valorFactura" id="valorFactura" value="<?php echo $_POST['valorFactura'] ?>">	
							</td>

                            <td class="tamano01">Concepto de pago:</td>
                            <td colspan="3">
                                <input  type="text" name="concepto" id="concepto" value="<?php echo @ $_POST['concepto'];?>" style="width:100%;height:30px;" readonly/>
                            </td>
                        </tr>

                        <tr>
                            <td class="tamano01">Recaudo en:</td>
                            <td>
                                <input  type="text" name="modoRecaudo" id="modoRecaudo" value="<?php echo @ $_POST['modoRecaudo'];?>" style="width:100%;height:30px;text-align:center;text-transform:uppercase;" readonly/>
                            </td>

                            <td class="tamano01">Nombre cuenta:</td>
                            <td>
                                <input  type="text" name="cuentaBanco" id="cuentaBanco" value="<?php echo @ $_POST['cuentaBanco'];?>" style="width:100%;height:30px;text-align:center;" readonly/>
                            </td>

                            <td class="tamano01">Número de cuenta:</td>
                            <td>
                                <input  type="text" name="cuentaRecaudo" id="cuentaRecaudo" value="<?php echo @ $_POST['cuentaRecaudo'];?>" style="width:100%;height:30px;text-align:center;" readonly/>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>

			<input type="hidden" name="oculto" id="oculto" value="1"/>
            <input type="hidden" name="bc" id="bc" value="1"/>
            <input type="hidden" name="estado" id="estado" value="<?php echo $_POST['estado'] ?>"/>
            <input type="hidden" name="letras" id="letras" value="<?php echo $_POST['letras'] ?>"/>
            <!-- <input type="hidden" name="cuentaBanco" id="cuentaBanco" value="<?php echo $_POST['cuentaBanco'] ?>"/> -->
            <input type="hidden" name="descripcion" id="descripcion" value="<?php echo $_POST['descripcion'] ?>"/>

			<?php 
				if(@$_POST['oculto']=="2")
				{

				}
			?>
		</form>
		<div id="bgventanamodal2">
			<div id="ventanamodal2">
				<IFRAME  src="" name="buster" marginWidth=0 marginHeight=0 frameBorder=0 id="ventana2" frameSpacing=0 style="left:500px; width:900px; height:500px; top:200;"></IFRAME>
			</div>
		</div>
	</body>
</html>