<?php

	header("Content-Type: text/html;charset=utf-8");
    require_once 'PHPExcel/Classes/PHPExcel.php';
    require "comun.inc";
    require "funciones.inc";
    require 'funcionesSP.inc.php';
    // ini_set('display_errors', '1');
    // ini_set('display_startup_errors', '1');
    // error_reporting(E_ALL);

    session_start();

    class Plantilla{
        private $linkbd;
        public function __construct() {
            $this->linkbd = conectar_v7();
            $this->linkbd->set_charset("utf8");
        }

        public function vigencias() {
            $vigencias = [];
            $sql_pdt = "SELECT PG.vigencia_inicial, PG.vigencia_final FROM plan_pdt AS PDT INNER JOIN para_periodos_gobierno AS PG ON PDT.periodo_gobierno_id = PG.id LIMIT 1";
            $pdt = mysqli_fetch_all(mysqli_query($this->linkbd,$sql_pdt),MYSQLI_ASSOC);
            $i = $pdt[0]["vigencia_inicial"];
            for ($i; $i <= $pdt[0]["vigencia_final"] ; $i++) { 
                $vigencias[] = $i;
            }
            return $vigencias;
        }

        public function searchData() {
            $sql = "SELECT PP.id,
            PDT.nombre_pdt,
            PP.eje_estrategico_id, 
            PP.programa_id, 
            CP.codigo AS programaCod,
            CP.nombre AS programaName,
            PP.nombre 
            FROM plan_plurianual AS PP
            INNER JOIN ccpetprogramas AS CP
            ON PP.programa_id = CP.id
            INNER JOIN plan_pdt AS PDT
            ON PP.pdt_id = PDT.id
            WHERE PP.estado = 'S'";
            $data = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
            foreach ($data as $key => $d) {
                $sectorCod = substr($d["programaCod"], 0, 2);
                $sql_sector = "SELECT nombre FROM ccpetsectores WHERE codigo = '$sectorCod'";
                $sector = mysqli_fetch_all(mysqli_query($this->linkbd,$sql_sector),MYSQLI_ASSOC);
                $data[$key]["sectorCod"] = $sectorCod;
                $data[$key]["sectorName"] = $sector[0]["nombre"];
            }
            return $data;
        }

        public function fuentes() {
            $sql = "SELECT 
            PPD.plurianual_id,
            PPD.id_fuente, 
            PFP.nombre, 
            PPD.valor_vig_01,
            PPD.valor_vig_02,
            PPD.valor_vig_03,
            PPD.valor_vig_04 
            FROM plan_plurianual_det AS PPD 
            INNER JOIN plan_fuentes_ppi AS PFP
            ON PPD.id_fuente = PFP.id
            ORDER BY PPD.plurianual_id, PPD.id_fuente";
            $data = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
            return $data;
        }
    }

    function getExcelColumns($start = 'A', $end = 'BM') {
        $columns = [];
        $current = $start;
    
        // Generar las columnas
        while ($current !== $end) {
            $columns[] = $current;
            $current++;
        }
        $columns[] = $end; // Añadir el último valor
    
        return $columns;
    }

    $obj = new Plantilla();
    $data = $obj->searchData();
    $vigencias = $obj->vigencias();
    $fuentes = $obj->fuentes();
    $vig01 = $vigencias[0];
    $vig02 = $vigencias[1];
    $vig03 = $vigencias[2];
    $vig04 = $vigencias[3];
    $objPHPExcel = new PHPExcel();

    $objPHPExcel->getSheet(0)->getStyle('A:BM')->applyFromArray(
        array(
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
            )
        )
    );
    $objPHPExcel->getSheet(0)->setTitle("ISGR");
    $objPHPExcel->getProperties()
    ->setCreator("IDEAL 10")
    ->setLastModifiedBy("IDEAL 10")
    ->setTitle("Exportar Excel con PHP")
    ->setSubject("Documento de prueba")
    ->setDescription("Documento generado con PHPExcel")
    ->setKeywords("usuarios phpexcel")
    ->setCategory("reportes");

    // ----Cuerpo de Documento----
    $objPHPExcel->getSheet(0)
    ->mergeCells('A1:BM1')
    ->mergeCells('A2:BM2')
    ->setCellValue('A1', 'PLANEACIÓN ESTRATEGICA')
    ->setCellValue('A2', 'PLAN PLURIANUAL CREADOS');
    $objPHPExcel->getSheet(0)
    -> getStyle ("A1")
    -> getFill ()
    -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
    -> getStartColor ()
    -> setRGB ('C8C8C8');
    $objPHPExcel->getSheet(0)
    -> getStyle ("A1:BM2")
    -> getFont ()
    -> setBold ( true )
    -> setName ( 'Verdana' )
    -> setSize ( 10 )
    -> getColor ()
    -> setRGB ('000000');
    $objPHPExcel->getSheet(0)
    -> getStyle ('A1:BM2')
    -> getAlignment ()
    -> applyFromArray (array ( 'horizontal'  =>  PHPExcel_Style_Alignment :: HORIZONTAL_CENTER ,) );
    $objPHPExcel->getSheet(0)
    -> getStyle ('A3:BM3')
    -> getAlignment ()
    -> applyFromArray (array ( 'horizontal'  =>  PHPExcel_Style_Alignment :: VERTICAL_JUSTIFY ,) );
    $objPHPExcel->getSheet(0)
    -> getStyle ("A2")
    -> getFill ()
    -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID);

    $borders = array(
        'borders' => array(
            'allborders' => array(
                'style' => PHPExcel_Style_Border::BORDER_THIN,
            'color' => array('argb' => 'FF000000'),
            )
        ),
    );
    $columns = getExcelColumns('A', 'BM');
    $objPHPExcel->getSheet(0)
    ->setCellValue('A3', "Nombre PDT")
    ->setCellValue('B3', "Código plurianual")
    ->setCellValue('C3', "Linea estrategica")
    ->setCellValue('D3', "Sector")
    ->setCellValue('E3', "Programa")
    ->setCellValue('F3', "Recursos propios $vig01")
    ->setCellValue('G3', "SGP Educación $vig01")
    ->setCellValue('H3', "SGP Salud $vig01")
    ->setCellValue('I3', "SGP Deporte $vig01")
    ->setCellValue('J3', "SGP Cultura $vig01")
    ->setCellValue('K3', "SGP Libre inversion $vig01")
    ->setCellValue('L3', "SGP Libre destinación $vig01")
    ->setCellValue('M3', "SGP Alimentación escolar $vig01")
    ->setCellValue('N3', "SGP Municipios rio Magdalena $vig01")
    ->setCellValue('O3', "SGP APSB $vig01")
    ->setCellValue('P3', "Credito $vig01")
    ->setCellValue('Q3', "Transferencias de capital - Cofinanciacion departamento $vig01")
    ->setCellValue('R3', "Transferencias de capital - Cofinanciacion nacion $vig01")
    ->setCellValue('S3', "SGR $vig01")
    ->setCellValue('T3', "Otros $vig01")
    ->setCellValue('U3', "Recursos propios $vig02")
    ->setCellValue('V3', "SGP Educación $vig02")
    ->setCellValue('W3', "SGP Salud $vig02")
    ->setCellValue('X3', "SGP Deporte $vig02")
    ->setCellValue('Y3', "SGP Cultura $vig02")
    ->setCellValue('Z3', "SGP Libre inversion $vig02")
    ->setCellValue('AA3', "SGP Libre destinación $vig02")
    ->setCellValue('AB3', "SGP Alimentación escolar $vig02")
    ->setCellValue('AC3', "SGP Municipios rio Magdalena $vig02")
    ->setCellValue('AD3', "SGP APSB $vig02")
    ->setCellValue('AE3', "Credito $vig02")
    ->setCellValue('AF3', "Transferencias de capital - Cofinanciacion departamento $vig02")
    ->setCellValue('AG3', "Transferencias de capital - Cofinanciacion nacion $vig02")
    ->setCellValue('AH3', "SGR $vig02")
    ->setCellValue('AI3', "Otros $vig02")
    ->setCellValue('AJ3', "Recursos propios $vig03")
    ->setCellValue('AK3', "SGP Educación $vig03")
    ->setCellValue('AL3', "SGP Salud $vig03")
    ->setCellValue('AM3', "SGP Deporte $vig03")
    ->setCellValue('AN3', "SGP Cultura $vig03")
    ->setCellValue('AO3', "SGP Libre inversion $vig03")
    ->setCellValue('AP3', "SGP Libre destinación $vig03")
    ->setCellValue('AQ3', "SGP Alimentación escolar $vig03")
    ->setCellValue('AR3', "SGP Municipios rio Magdalena $vig03")
    ->setCellValue('AS3', "SGP APSB $vig03")
    ->setCellValue('AT3', "Credito $vig03")
    ->setCellValue('AU3', "Transferencias de capital - Cofinanciacion departamento $vig03")
    ->setCellValue('AV3', "Transferencias de capital - Cofinanciacion nacion $vig03")
    ->setCellValue('AW3', "SGR $vig03")
    ->setCellValue('AX3', "Otros $vig03")
    ->setCellValue('AY3', "Recursos propios $vig04")
    ->setCellValue('AZ3', "SGP Educación $vig04")
    ->setCellValue('BA3', "SGP Salud $vig04")
    ->setCellValue('BB3', "SGP Deporte $vig04")
    ->setCellValue('BC3', "SGP Cultura $vig04")
    ->setCellValue('BD3', "SGP Libre inversion $vig04")
    ->setCellValue('BE3', "SGP Libre destinación $vig04")
    ->setCellValue('BF3', "SGP Alimentación escolar $vig04")
    ->setCellValue('BG3', "SGP Municipios rio Magdalena $vig04")
    ->setCellValue('BH3', "SGP APSB $vig04")
    ->setCellValue('BI3', "Credito $vig04")
    ->setCellValue('BJ3', "Transferencias de capital - Cofinanciacion departamento $vig04")
    ->setCellValue('BK3', "Transferencias de capital - Cofinanciacion nacion $vig04")
    ->setCellValue('BL3', "SGR $vig04")
    ->setCellValue('BM3', "Otros $vig04");
    
    $objPHPExcel->getSheet(0)
        -> getStyle ("A3:BM3")
        -> getFill ()
        -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
        -> getStartColor ()
        -> setRGB ('99ddff');
    $objPHPExcel->getSheet(0)->getStyle("A3:BM3")->getFont()->setBold(true);
    $objPHPExcel->getSheet(0)->getStyle('A1:BM3')->applyFromArray($borders);
    $objPHPExcel->getSheet(0)->getStyle('A2:BM3')->applyFromArray($borders);
    $objPHPExcel->getSheet(0)->getStyle('A3:BM3')->applyFromArray($borders);

    $columns = getExcelColumns('A', 'BM');
    foreach ($columns as $column) {
        $objPHPExcel->getSheet(0)->getColumnDimension("$column")->setAutoSize(true);
    }

    if(!empty($data)){
        $total = count($data);
        $row = 4;
        for ($i=0; $i < $total ; $i++) {
            $fuente_01 = $fuente_02 = $fuente_03  = $fuente_04  = $fuente_05 = $fuente_06  = $fuente_07  = $fuente_08  = $fuente_09  = $fuente_10  = $fuente_11  = $fuente_12  = $fuente_13  = $fuente_14  = $fuente_15 = [];
            foreach ($fuentes as $key => $fuente) {
                if ($fuente["plurianual_id"] == $data[$i]["id"]) {
                    switch ($fuente["id_fuente"]) {
                        case "1":
                            $fuente_01["vig_01"] = $fuente["valor_vig_01"];
                            $fuente_01["vig_02"] = $fuente["valor_vig_02"];
                            $fuente_01["vig_03"] = $fuente["valor_vig_03"];
                            $fuente_01["vig_04"] = $fuente["valor_vig_04"];
                            break;
                        case "2":
                            $fuente_02["vig_01"] = $fuente["valor_vig_01"];
                            $fuente_02["vig_02"] = $fuente["valor_vig_02"];
                            $fuente_02["vig_03"] = $fuente["valor_vig_03"];
                            $fuente_02["vig_04"] = $fuente["valor_vig_04"];
                            break;
                        case "3":
                            $fuente_03["vig_01"] = $fuente["valor_vig_01"];
                            $fuente_03["vig_02"] = $fuente["valor_vig_02"];
                            $fuente_03["vig_03"] = $fuente["valor_vig_03"];
                            $fuente_03["vig_04"] = $fuente["valor_vig_04"];
                            break;
                        case "4":
                            $fuente_04["vig_01"] = $fuente["valor_vig_01"];
                            $fuente_04["vig_02"] = $fuente["valor_vig_02"];
                            $fuente_04["vig_03"] = $fuente["valor_vig_03"];
                            $fuente_04["vig_04"] = $fuente["valor_vig_04"];
                            break;
                        case "5":
                            $fuente_05["vig_01"] = $fuente["valor_vig_01"];
                            $fuente_05["vig_02"] = $fuente["valor_vig_02"];
                            $fuente_05["vig_03"] = $fuente["valor_vig_03"];
                            $fuente_05["vig_04"] = $fuente["valor_vig_04"];
                            break;
                        case "6":
                            $fuente_06["vig_01"] = $fuente["valor_vig_01"];
                            $fuente_06["vig_02"] = $fuente["valor_vig_02"];
                            $fuente_06["vig_03"] = $fuente["valor_vig_03"];
                            $fuente_06["vig_04"] = $fuente["valor_vig_04"];
                            break;
                        case "7":
                            $fuente_07["vig_01"] = $fuente["valor_vig_01"];
                            $fuente_07["vig_02"] = $fuente["valor_vig_02"];
                            $fuente_07["vig_03"] = $fuente["valor_vig_03"];
                            $fuente_07["vig_04"] = $fuente["valor_vig_04"];
                            break;
                        case "8":
                            $fuente_08["vig_01"] = $fuente["valor_vig_01"];
                            $fuente_08["vig_02"] = $fuente["valor_vig_02"];
                            $fuente_08["vig_03"] = $fuente["valor_vig_03"];
                            $fuente_08["vig_04"] = $fuente["valor_vig_04"];
                            break;
                        case "9":
                            $fuente_09["vig_01"] = $fuente["valor_vig_01"];
                            $fuente_09["vig_02"] = $fuente["valor_vig_02"];
                            $fuente_09["vig_03"] = $fuente["valor_vig_03"];
                            $fuente_09["vig_04"] = $fuente["valor_vig_04"];
                            break;
                        case "10":
                            $fuente_10["vig_01"] = $fuente["valor_vig_01"];
                            $fuente_10["vig_02"] = $fuente["valor_vig_02"];
                            $fuente_10["vig_03"] = $fuente["valor_vig_03"];
                            $fuente_10["vig_04"] = $fuente["valor_vig_04"];
                            break;
                        case "11":
                            $fuente_11["vig_01"] = $fuente["valor_vig_01"];
                            $fuente_11["vig_02"] = $fuente["valor_vig_02"];
                            $fuente_11["vig_03"] = $fuente["valor_vig_03"];
                            $fuente_11["vig_04"] = $fuente["valor_vig_04"];
                            break;
                        case "12":
                            $fuente_12["vig_01"] = $fuente["valor_vig_01"];
                            $fuente_12["vig_02"] = $fuente["valor_vig_02"];
                            $fuente_12["vig_03"] = $fuente["valor_vig_03"];
                            $fuente_12["vig_04"] = $fuente["valor_vig_04"];
                            break;
                        case "13":
                            $fuente_13["vig_01"] = $fuente["valor_vig_01"];
                            $fuente_13["vig_02"] = $fuente["valor_vig_02"];
                            $fuente_13["vig_03"] = $fuente["valor_vig_03"];
                            $fuente_13["vig_04"] = $fuente["valor_vig_04"];
                            break;
                        case "14":
                            $fuente_14["vig_01"] = $fuente["valor_vig_01"];
                            $fuente_14["vig_02"] = $fuente["valor_vig_02"];
                            $fuente_14["vig_03"] = $fuente["valor_vig_03"];
                            $fuente_14["vig_04"] = $fuente["valor_vig_04"];
                            break;
                        case "15":
                            $fuente_15["vig_01"] = $fuente["valor_vig_01"];
                            $fuente_15["vig_02"] = $fuente["valor_vig_02"];
                            $fuente_15["vig_03"] = $fuente["valor_vig_03"];
                            $fuente_15["vig_04"] = $fuente["valor_vig_04"];
                            break;
                    }
                }
            }
            
            $objPHPExcel->getSheet(0)
            ->setCellValue("A$row", $data[$i]['nombre_pdt'])
            ->setCellValue("B$row", $data[$i]['id'])
            ->setCellValue("C$row", "LE".$data[$i]['eje_estrategico_id'])
            ->setCellValue("D$row", $data[$i]['sectorCod']." - ".$data[$i]['sectorName'])
            ->setCellValue("E$row", $data[$i]['programaCod']." - ".$data[$i]['programaName'])
            ->setCellValue("F$row", $fuente_01["vig_01"])
            ->setCellValue("G$row", $fuente_02["vig_01"])
            ->setCellValue("H$row", $fuente_03["vig_01"])
            ->setCellValue("I$row", $fuente_04["vig_01"])
            ->setCellValue("J$row", $fuente_05["vig_01"])
            ->setCellValue("K$row", $fuente_06["vig_01"])
            ->setCellValue("L$row", $fuente_07["vig_01"])
            ->setCellValue("M$row", $fuente_08["vig_01"])
            ->setCellValue("N$row", $fuente_09["vig_01"])
            ->setCellValue("O$row", $fuente_10["vig_01"])
            ->setCellValue("P$row", $fuente_11["vig_01"])
            ->setCellValue("Q$row", $fuente_12["vig_01"])
            ->setCellValue("R$row", $fuente_13["vig_01"])
            ->setCellValue("S$row", $fuente_14["vig_01"])
            ->setCellValue("T$row", $fuente_15["vig_01"])
            
            ->setCellValue("U$row", $fuente_01["vig_02"])
            ->setCellValue("V$row", $fuente_02["vig_02"])
            ->setCellValue("W$row", $fuente_03["vig_02"])
            ->setCellValue("X$row", $fuente_04["vig_02"])
            ->setCellValue("Y$row", $fuente_05["vig_02"])
            ->setCellValue("Z$row", $fuente_06["vig_02"])
            ->setCellValue("AA$row", $fuente_07["vig_02"])
            ->setCellValue("AB$row", $fuente_08["vig_02"])
            ->setCellValue("AC$row", $fuente_09["vig_02"])
            ->setCellValue("AD$row", $fuente_10["vig_02"])
            ->setCellValue("AE$row", $fuente_11["vig_02"])
            ->setCellValue("AF$row", $fuente_12["vig_02"])
            ->setCellValue("AG$row", $fuente_13["vig_02"])
            ->setCellValue("AH$row", $fuente_14["vig_02"])
            ->setCellValue("AI$row", $fuente_15["vig_02"])

            ->setCellValue("AJ$row", $fuente_01["vig_03"])
            ->setCellValue("AK$row", $fuente_02["vig_03"])
            ->setCellValue("AL$row", $fuente_03["vig_03"])
            ->setCellValue("AM$row", $fuente_04["vig_03"])
            ->setCellValue("AN$row", $fuente_05["vig_03"])
            ->setCellValue("AO$row", $fuente_06["vig_03"])
            ->setCellValue("AP$row", $fuente_07["vig_03"])
            ->setCellValue("AQ$row", $fuente_08["vig_03"])
            ->setCellValue("AR$row", $fuente_09["vig_03"])
            ->setCellValue("AS$row", $fuente_10["vig_03"])
            ->setCellValue("AT$row", $fuente_11["vig_03"])
            ->setCellValue("AU$row", $fuente_12["vig_03"])
            ->setCellValue("AV$row", $fuente_13["vig_03"])
            ->setCellValue("AW$row", $fuente_14["vig_03"])
            ->setCellValue("AX$row", $fuente_15["vig_03"])

            ->setCellValue("AY$row", $fuente_01["vig_04"])
            ->setCellValue("AZ$row", $fuente_02["vig_04"])
            ->setCellValue("BA$row", $fuente_03["vig_04"])
            ->setCellValue("BB$row", $fuente_04["vig_04"])
            ->setCellValue("BC$row", $fuente_05["vig_04"])
            ->setCellValue("BD$row", $fuente_06["vig_04"])
            ->setCellValue("BE$row", $fuente_07["vig_04"])
            ->setCellValue("BF$row", $fuente_08["vig_04"])
            ->setCellValue("BG$row", $fuente_09["vig_04"])
            ->setCellValue("BH$row", $fuente_10["vig_04"])
            ->setCellValue("BI$row", $fuente_11["vig_04"])
            ->setCellValue("BJ$row", $fuente_12["vig_04"])
            ->setCellValue("BK$row", $fuente_13["vig_04"])
            ->setCellValue("BL$row", $fuente_14["vig_04"])
            ->setCellValue("BM$row", $fuente_15["vig_04"])
            ;
            $row++;
        }
    }
  
    $objPHPExcel->setActiveSheetIndex(0);
    //----Guardar documento----
    header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment;filename="plurianual_creados.xlsx"');
    header('Cache-Control: max-age=0');
    $objWriter=PHPExcel_IOFactory::createWriter($objPHPExcel,'Excel2007');
    $objWriter->save('php://output');
    die();

?>
