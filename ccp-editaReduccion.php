<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=iso-8859-1");
	require 'comun.inc';
	require 'funciones.inc';
	require 'validaciones.inc';
	require 'conversor.php';
	session_start();
	$linkbd = conectar_v7();
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es">
	<head>
		<meta http-equiv="Content-type" content="text/html; charset=iso-8859-1"/>
		<meta http-equiv="X-UA-Compatible" content="IE=9"/>
		<title>IDEAL 10 - Planeaci&oacute;n Estrat&eacute;gica</title>
		<link href="css/css2.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/tabs.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
		<script src="vue/vue.js"></script>
		<script type="text/javascript" src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"> </script>
		<style>
			.modal-mask
			{
				position: fixed;
				z-index: 9998;
				top: 0;
				left: 0;
				width: 100%;
				height: 100%;
				background-color: rgba(0, 0, 0, .5);
				display: table;
				transition: opacity .3s ease;
			}
			.modal-wrapper 
			{
				display: table-cell;
				vertical-align: middle;
			}
			.modal-container
			{
				width: 60%;
				margin: 0px auto;
				padding: 20px 30px;
				text-align: left;
				background:linear-gradient(#99bbcc, #B6CEDA);
				border-radius: 2px;
				box-shadow: 0 2px 8px rgba(0, 0, 0, .33);
				transition: all .3s ease;
			}
			.modal-container1
			{
				width: 50%;
				margin: 0px auto;
				padding: 20px 30px;
				text-align: left;
				background:linear-gradient(#99bbcc, #B6CEDA);
				border-radius: 10px;
				box-shadow: 0 2px 8px rgba(0, 0, 0, .33);
				transition: all .3s ease;
			}
			.modal-container2
			{
				width: 80%;
				margin: 0px auto;
				padding: 20px 30px;
				text-align: left;
				background:linear-gradient(#99bbcc, #B6CEDA);
				border-radius: 2px;
				box-shadow: 0 2px 8px rgba(0, 0, 0, .33);
				transition: all .3s ease;
			}
			.modal-container3
			{
				width: 90%;
				margin: 0px auto;
				padding: 20px 30px;
				text-align: left;
				background:linear-gradient(#99bbcc, #B6CEDA);
				border-radius: 2px;
				box-shadow: 0 2px 8px rgba(0, 0, 0, .33);
				transition: all .3s ease;
			}
			footer
			{
				text-align: right;
			}

			/* Agrandar checkbox */
			input[type=checkbox]
			{
				/*Navegadores para soportar*/
				-ms-transform: scale(2); /* IE */
				-moz-transform: scale(2); /* FF */
				-webkit-transform: scale(2); /* Safari y Chrome */
				-o-transform: scale(2); /* Opera */
				padding: 10px;
			}
		</style>
		<?php titlepag();?>
	</head>
	<body>
		<div id="myapp">
			<table>
				<tr><script>barra_imagenes("ccpet");</script><?php cuadro_titulos();?></tr>
				<tr><?php menu_desplegable("ccpet");?></tr>
				<tr>
					<td colspan="3" class="cinta">
                        <img src="imagenes/add.png" onClick="location.href='ccp-reduccion.php'" class="mgbt" title="Nuevo" />
                        <img src="imagenes/guarda.png" title="Guardar" v-on:click="preguntaguardar('1')" class="mgbt"/>
                        <img src="imagenes/busca.png" onClick="location.href='ccp-buscaReduccion.php'" class="mgbt" title="Buscar"/>
                        <img src="imagenes/nv.png" onClick="mypop=window.open('ccp-principal.php','',''); mypop.focus();" class="mgbt" title="Nueva Ventana">
					</td>
				</tr>
			</table>
				<?php
					if(@ $_POST['oculto']=="")
					{
						$_POST['tabgroup1']=1;
					}
					switch($_POST['tabgroup1'])
					{
						case 1:
							$check1='checked';break;
						case 2:
							$check2='checked';break;
						case 3:
							$check3='checked';break;
						case 4:
							$check4='checked';break;
						case 5:
							$check5='checked';break;
					}
                    if(@ $_POST['oculto']=="")
					{
						$_POST['tabgroup3']=1;
					}
                    switch($_POST['tabgroup3'])
					{
						case 1:
							$checked1='checked';break;
						case 2:
							$checked2='checked';break;
						case 3:
							$checked3='checked';break;
						case 4:
							$checked4='checked';break;
					}
					switch($_POST['tabgroup4'])
					{
						case 1:
							$chek1='checked';break;
						case 2:
							$chek2='checked';break;
						case 3:
							$chek3='checked';break;
						case 4:
							$chek4='checked';break;
					}
				?>
                <div class="tabsmeci" style="height:77%; width:99.6%" >
                    <div class="tab" >
                        <input type="radio" id="tab-1" name="tabgroup1" value="1" <?php echo $check1;?> >
                        <label for="tab-1">Proyecto</label>
                        <div class="content" style="overflow:hidden;">
                            <table class="inicio ancho">
                                <tr>
                                    <td class="titulos" colspan="8" >Ingresar Adici&oacute;n a Proyecto Nuevo o Existente</td>

                                    <td class=" cerrar" style="width:7%" onClick="location.href='plan-principal.php'">Cerrar</td>
                                </tr>
                                <tr>
                                    <td class="tamano01" style="width:3cm">Proyectos Existentes:</td>
                                    
                                    <td colspan="">
                                        <input type="text" v-model="proyecto" v-on:dblclick='toggleModalProyecto' style="width:100%;height:30px;" v-bind:class="unidadejecutoradobleclick"  autocomplete="off" readonly/>
                                        <input type="hidden" v-model="cProyecto"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="tamano01">Actos Administrativos:</td>

                                    <td style="width:60%">
										<input type="text" v-model="nombreActoAdministrativo" style="width:100%;height:30px;" readonly/>
										<input type="hidden" v-model="actoAdministrativo"/>
                                    </td>

									<td class="tamano01">Valor Acto administrativo:</td>
									<td>
										<input type="text" v-model="valorActoAdmin" style="width:100%;height:30px;"  readonly>
									</td>
                                </tr>
                            </table>

                            <table class="inicio ancho">
                                <tr>
                                    <td class="tamano01" style="width:3cm">Unidad Ejecutora:</td>
                                    <td colspan="2">
                                        <input type="text" v-model="unidadejecutora" style="width:100%;height:30px;" autocomplete="off" readonly/>
                                        <input type="hidden" v-model="cunidadejecutora"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="tamano01" style="width:3cm">C&oacute;digo:</td>

                                    <td style="width:20%">
                                        <input type="text" v-model="codigo" style="width:100%;height:30px;" ref="codigo" readonly/>
                                    </td>

                                    <td class="tamano01" style="width:7%">Vigencia:</td>
                                    <td style="width:7%">
                                        <select v-model="vigencia" style="width:100%">
                                            <option v-for="year in years" :value="year[0]">{{ year[0] }}</option>
                                        </select>
                                    </td>

                                    <td class="tamano01" style="width:3cm">Nombre:</td>

                                    <td>
                                        <input type="text" v-model="nombre" style="width:100%;height:30px;" ref="nombre" readonly/>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="tamano01">Valor del proyecto:</td>

                                    <td>
                                        <input type="number" v-model="valorproyecto" style="width:100%;height:30px;" readonly/>
                                    </td>

                                    <td class="tamano01">Descripci&oacute;n:</td>

                                    <td colspan="3">
                                        <input type="text" v-model="descripcion" style="width:100%;height:30px;" ref="descripcion" readonly/>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="tamano01">Sector:</td>

                                    <td colspan="3">
                                        <input type="text" v-model="sector" style="width:100%;height:30px;" autocomplete="off" readonly/><input type="hidden" v-model="csector"/>
                                    </td>

                                    <td class="tamano01">Programa:</td>

                                    <td colspan="3">
                                        <input type="text" v-model="programa" style="width:100%;height:30px;" readonly/><input type="hidden" v-model="cprograma"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="tamano01">Subprograma:</td>

                                    <td colspan="3">
                                        <input type="text" v-model="subprograma" style="width:100%;height:30px;" readonly/><input type="hidden" v-model="csubprograma"/>
                                    </td>
                                </tr>
                            </table>
                            
                            <div class='subpantalla' style='height:80%; width:99.5%; margin-top:0px; overflow-x:hidden'>
                                <table class='inicio inicio--no-shadow'>
                                    <tbody>
                                        <tr class="titulos">
                                            <td>Producto</td>
                                            <td>Indicador</td>
                                            <td>Valor</td>
                                        </tr>
                                        <tr v-for="(vcproducto, index) in selectProductosExistentes" v-bind:class="index % 2 ? 'saludo1a' : 'saludo2'" style='text-rendering: optimizeLegibility; cursor: pointer !important; style=\"cursor: hand\"' >
                                            <td width="45%" style="font: 120% sans-serif; padding-left:10px">{{ vcproducto[1] }}</td>
                                            <td style="font: 120% sans-serif; padding-left:10px">{{ vcproducto[3] }}</td>
                                            <td width="8%">{{ vcproducto[8] }}</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="tab">
                        <input type="radio" id="tab-2" name="tabgroup1" value="2" <?php echo $check2;?> >
                        <label for="tab-2">Presupuesto Gastos</label>
                        <div class="content" style="overflow:hidden;">
                            <table class="inicio ancho">
                                <tr>
                                    <td class="titulos" colspan="6">Ingresar Reduccion</td>
                                    <td class="cerrar" style="width:7%" onClick="location.href='plan-principal.php'">Cerrar</td>
                                </tr>
                                <tr>
                                    <td class='tamano01' style="width:3cm;">Id. Producto:</td>

                                    <td colspan="4">
										<input type="text" v-model="nomidentproducto" style="width:100%;height:30px;text-transform: uppercase;font-weight: bold;" readonly/>
										<input type="hidden" v-model="identproducto">
                                    </td>
                                    
                                </tr>
                                <tr>
                                    <td class='tamano01'>Fuente:</td>

                                    <td>
										<input type="text" v-model="fuentef" ref ="fuentef" style="width:100%;height:30px;"  autocomplete="off" readonly/>
										<input type="hidden" v-model="cfuentef"/>
									</td>

                                    <td class='tamano01'>Meta:</td>

                                    <td>
										<input type="text" v-model="metaf" ref ="metaf" style="width:100%;height:30px;" autocomplete="off" readonly/>
										<input type="hidden" v-model="cmetaf"/>
									</td>

                                    <td class='tamano01'>Medio de Pago:</td>

                                    <td style="width:14%;">
                                        <select v-model="mediopago" style="width:100%" v-bind:class="parpadeomediopago" v-on:click="parpadeomediopago='';" >
                                            <option disabled value="">Seleccione un medio</option>
                                            <option value='CSF'>Con Situaci&oacute;n de Fondos</option>
                                            <option value='SSF'>Si Situaci&oacute;n de Fondos</option>
                                        </select>
                                    </td>
                                </tr>

                                <tr>
                                    <td class='tamano01' style="width:3cm;">Rubro:</td>

                                    <td style="width:35%">
										<input type="text" v-model="nrubro" v-on:dblclick='toggleModal4' class="colordobleclik" style="width:100%;height:30px;" readonly/>
										<input type="hidden" v-model="codrubro"/>
									</td>

                                    <td class='tamano01' style="width:3cm;">Clasificador:</td>

                                    <td colspan="3">
                                        <input type="text" v-model="nombreClasificador" style="width:100%;height:30px;" readonly>
										<input type="hidden" v-model="clasificador">
                                    </td>
                                </tr>
                                
                                <tr>
                                    <td class='tamano01' style="width:3cm;">Pol&iacute;tica P&uacute;blica:</td>

                                    <td style="width:35%;">
                                        <input type="text" v-model="nPoliticaPublica" ref="nPoliticaPublica" style="width:100%;height:30px;" readonly>
                                        <input type="hidden" v-model="codigoPoliticaPublica"/>
                                    </td>

                                    <td class="tamano01">Vigencia del Gasto:</td>

                                    <td style="width:60%" colspan="3">
										<input type="text" v-model="nombreVigenciaGasto" style="width:100%;height:30px;" readonly>
										<input type="hidden" v-model="vigenciaGasto">
                                    </td>
                                </tr>
                            </table>

                            <table class="inicio ancho" v-show="showopcion1">
                                <tr>
                                    <td class='tamano01' style="width:3cm;">Id Entidad:</td>
                                    <td style="width:20%"><input type="text" v-model="identidad" style="width:100%;height:30px;" autocomplete="off" readonly></td>
                                    <td class='tamano01' style="width:3cm;">Nit:</td>
                                    <td style="width:20%"><input type="text" v-model="nitentidad" style="width:100%;height:30px;" readonly></td>
                                    <td class='tamano01' style="width:3cm;">C&oacute;digo CUIN:</td>
                                    <td colspan="2"><input type="text" v-model="codigocuin" style="width:100%;height:30px;" readonly></td>
                                    <td style="width:7%"></td>
                                </tr>
                                <!-- Agregar para clasificador CUIN -->
                                <tr>
                                    <td class='tamano01' >Entidad:</td>
                                    <td colspan="3"><input type="text" v-model="nomentidad" style="width:100%;height:30px;" readonly/></td>
                                    <td class='tamano01' >Valor:</td>
                                    <td><input type="number" v-model="valorcuin" style="width:100%;height:30px;"/></td>
                                    <td style=" height: 30px;"><em class="botonflecha" v-on:click="agregarcuenta1()">Agregar</em></td>
                                </tr>
                            </table>
                            <!-- Agregar para sin clasificador -->
                            <table class="inicio ancho" v-show="showopcion2">
                                <tr>
                                    <td class='tamano01' style="width:3cm;">Valor:</td>
                                    <td width="26.2%"><input type="number" v-model="valorsinclasifi" style="width:100%;height:30px;"/></td>
                                    <td style=" height: 30px;"><em class="botonflecha" v-on:click="agregarcuenta2()">Agregar</em></td>
                                </tr>
                            </table>
                            <table class="inicio ancho" v-show="showopcion2_3">
                                <tr>
                                    <td class='tamano01' style="width:2.5cm;">Secci&oacute;n:</td>

                                    <td style="width:25%">
										<input type="text" v-model="seccion" style="width:100%;height:30px;" readonly/>
										<input type="hidden" v-model="cseccion"/>
									</td>

                                    <td class='tamano01' style="width:2.8cm;">Divisi&oacute;n:</td>

                                    <td>
										<input type="text" v-model="division" style="width:100%;height:30px;" readonly/>
										<input type="hidden" v-model="cdivision"/>
									</td>

                                    <td class='tamano01' style="width:2.5cm;">Grupo:</td>

                                    <td>
										<input type="text" v-model="grupo" style="width:100%;height:30px;" readonly/>
										<input type="hidden" v-model="cgrupo"/>
									</td>

                                    <td style="width:7%;"></td>
                                </tr>
                                <tr>
                                    <td class='tamano01'>Clase:</td>

                                    <td>
										<input type="text" v-model="clase" style="width:100%;height:30px;" readonly/>
										<input type="hidden" v-model="cclase"/>
									</td>

                                    <td  class='tamano01'>Subclase:</td>

                                    <td>
										<input type="text" v-model="subclase" style="width:100%;height:30px;" ref="subclase" v-on:keyup="validasubclase(subclase)"/>
										<input type="hidden" v-model="csubclase"/>
									</td>

                                    <td  class='tamano01' v-show="clasificador == 2 ? true : false">Producto:</td>

                                    <td v-show="clasificador == 2 ? true : false">
										<input type="text" v-model="subproducto" style="width:100%;height:30px;" ref="subproducto" v-on:keyup="validasubproducto(subproducto)" readonly/>
										<input type="hidden" v-model="csubproducto"/>
									</td>
                                </tr>
                                <!-- Agregar para clasificador bienes transportables y servicios -->
                                <tr>
                                    <td class='tamano01' >Valor:</td>
                                    <td><input type="number" v-model="valorrubro" style="width:100%;height:30px;" ref="valorrubro" v-bind:class="parpadeovalorrubro" v-on:keydown="parpadeovalorrubro='';"/></td>
                                    <td style=" height: 30px;"><em class="botonflecha" v-on:click="agregarcuenta()">Agregar</em></td>
                                </tr>
                            </table>
                            <div class="tabsmeci" style="width:99.8%" >
                                <div class="tab" >
                                    <input type="radio" id="tab-1b" name="tabgroup2" v-model="tabgroup2" v-bind:value="tb1" >
                                    <label for="tab-1b">Sin Clasificador</label>
                                    <div class="content" style="overflow-x:hidden;" v-bind:style="{ height: tapheight1 }">
                                        <table class='inicio inicio--no-shadow'>
                                            <tbody>
                                                <tr class="titulos">
                                                    <td>Fuente</td>
                                                    <td>Meta</td>
                                                    <td>Medio Pago</td>
                                                    <td>Rubro</td>
                                                    <td>Identificador Producto</td>
                                                    <td>Pol&iacute;tica P&uacute;blica</td>
                                                    <td>Vigencia de Gasto</td>
                                                    <td>valor</td>
                                                    <td></td>
                                                </tr>
                                                <tr v-for="(vsinclasificador, index) in selectcuetase" v-bind:class="index % 2 ? 'saludo1a' : 'saludo2'" style='text-rendering: optimizeLegibility; cursor: pointer !important; style=\"cursor: hand\"' >
                                                    <td style="font: 120% sans-serif; padding-left:10px">{{ vsinclasificador[3] }}</td>
                                                    <td style="font: 120% sans-serif; padding-left:10px; width:25%;">{{ vsinclasificador[7] }}</td>
                                                    <td style="font: 120% sans-serif; padding-left:10px; width:10%;">{{ vsinclasificador[4] }}</td>
                                                    <td style="font: 120% sans-serif; padding-left:10px">{{ vsinclasificador[1] }}</td>
                                                    <td style="font: 120% sans-serif; padding-left:10px; width:25%;">{{ vsinclasificador[5] }}</td>
                                                    <td style="font: 120% sans-serif; padding-left:10px; width:25%;">{{ vsinclasificador[8] }}</td>
                                                    <td style="font: 120% sans-serif; padding-left:10px; width:25%;">{{ vsinclasificador[9] }}</td>
                                                    <td style="font: 120% sans-serif; padding-left:10px; width:10%;">{{ vsinclasificador[2] }}</td>
                                                    <td style="width:5%;" v-on:click="eliminacuenta2(index,vsinclasificador[2],vsinclasificador[5])"><img src='imagenes/del.png'></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="tab">
                                    <input type="radio" id="tab-2b" name="tabgroup2" v-model="tabgroup2" v-bind:value="tb2" >
                                    <label for="tab-2b">Clasificador CUIN</label>
                                    <div class="content" style="overflow-x:hidden" v-bind:style="{ height: tapheight2 }">
                                        <table class='inicio inicio--no-shadow'>
                                            <tbody>
                                                <tr class="titulos">
                                                    <td>Fuente</td>
                                                    <td>Metas</td>
                                                    <td>Medio Pago</td>
                                                    <td>Rubro</td>
                                                    <td>Identidad</td>
                                                    <td>Nit</td>
                                                    <td>Entidad</td>
                                                    <td>C&oacute;digo CUIN</td>
                                                    <td>Identificador</td>
                                                    <td>Pol&iacute;tica P&uacute;blica</td>
                                                    <td>Vigencia de Gasto</td>
                                                    <td>valor</td>
                                                    <td></td>
                                                </tr>
                                                <tr v-for="(vcodigocuin, index) in selectcuetasc" v-bind:class="index % 2 ? 'saludo1a' : 'saludo2'" style='text-rendering: optimizeLegibility; cursor: pointer !important; style=\"cursor: hand\"' >
                                                    <td style="font: 120% sans-serif; padding-left:10px">{{ vcodigocuin[5] }}</td>
                                                    <td style="font: 120% sans-serif; padding-left:10px">{{ vcodigocuin[10] }}</td>
                                                    <td style="font: 120% sans-serif; padding-left:10px">{{ vcodigocuin[6] }}</td>
                                                    <td style="font: 120% sans-serif; padding-left:10px">{{ vcodigocuin[7] }}</td>
                                                    <td style="font: 120% sans-serif; padding-left:10px; width:15%;">{{ vcodigocuin[0] }}</td>
                                                    <td style="font: 120% sans-serif; padding-left:10px">{{ vcodigocuin[1] }}</td>
                                                    <td style="font: 120% sans-serif; padding-left:10px; width:35%;">{{ vcodigocuin[2] }}</td>
                                                    <td style="font: 120% sans-serif; padding-left:10px; width:20%;">{{ vcodigocuin[3] }}</td>
                                                    <td style="font: 120% sans-serif; padding-left:10px">{{ vcodigocuin[9] }}</td>
                                                    <td style="font: 120% sans-serif; padding-left:10px">{{ vcodigocuin[11] }}</td>
                                                    <td style="font: 120% sans-serif; padding-left:10px">{{ vcodigocuin[12] }}</td>
                                                    <td style="font: 120% sans-serif; padding-left:10px; width:20%;">{{ vcodigocuin[4] }}</td>
                                                    <td style="width:5%;" v-on:click="eliminacuenta1(index,vcodigocuin[4],vcodigocuin[8])"><img src='imagenes/del.png'></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="tab">
                                    <input type="radio" id="tab-3b" name="tabgroup2" v-model="tabgroup2" v-bind:value="tb3" >
                                    <label for="tab-3b">Clasificador B y S</label>
                                    <div class="content" style="overflow-x:hidden" v-bind:style="{ height: tabheight3 }">
                                        <table class='inicio inicio--no-shadow'>
                                            <tbody>
                                                <tr class="titulos">
                                                    <td>Fuente</td>
                                                    <td>Metas</td>
                                                    <td>Medio Pago</td>
                                                    <td>Rubro</td>
                                                    <td>Subclase</td>
                                                    <td>producto</td>
                                                    <td>Identificador</td>
                                                    <td>Pol&iacute;tica P&uacute;blica</td>
                                                    <td>Vigencia de Gasto</td>
                                                    <td>valor</td>
                                                    <td></td>
                                                </tr>
                                                <tr v-for="(vccuenta, index) in selectcuetasa" v-bind:class="index % 2 ? 'saludo1a' : 'saludo2'" style='text-rendering: optimizeLegibility; cursor: pointer !important; style=\"cursor: hand\"' >
                                                    <td style="font: 120% sans-serif; padding-left:10px;">{{ vccuenta[4] }}</td>
                                                    <td style="font: 120% sans-serif; padding-left:10px;width:20%;">{{ vccuenta[9] }}</td>
                                                    <td style="font: 120% sans-serif; padding-left:10px; width:10%;">{{ vccuenta[5] }}</td>
                                                    <td style="font: 120% sans-serif; padding-left:10px">{{ vccuenta[1] }}</td>
                                                    <td style="font: 120% sans-serif; padding-left:10px; width:20%;">{{ vccuenta[2] }}</td>
                                                    <td style="font: 120% sans-serif; padding-left:10px;width:20%;">{{ vccuenta[6] }}</td>
                                                    <td style="font: 120% sans-serif; padding-left:10px;width:20%;">{{ vccuenta[8] }}</td>
                                                    <td style="font: 120% sans-serif; padding-left:10px;width:20%;">{{ vccuenta[10] }}</td>
                                                    <td style="font: 120% sans-serif; padding-left:10px;width:20%;">{{ vccuenta[11] }}</td>
                                                    <td style="font: 120% sans-serif; padding-left:10px; ">{{ vccuenta[3] }}</td>
                                                    <td style="width:5%;" v-on:click="eliminacuentas(index,vccuenta[3],vccuenta[7])"><img src='imagenes/del.png'></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="tab">
                        <input type="radio" id="tab-3" name="tabgroup1" value="3" <?php echo $check3;?> >
                        <label for="tab-3">Presupuesto Ingresos</label>
                        <div class="content" style="overflow:hidden;">
                            <table class="inicio ancho">
                                <tr>
                                    <td class="titulos" colspan="6">Ingresar Reduccion</td>
                                    <td class="cerrar" style="width:7%" onClick="location.href='plan-principal.php'">Cerrar</td>
                                </tr>
                                
                                <tr>
                                    <td class='tamano01'>Fuente:</td>
                                    <td><input type="text" v-model="fuenteIngreso" ref ="fuenteIngreso" v-on:dblclick='toggleModalIngresoFuente' style="width:100%;height:30px;" v-bind:class="fuentedobleclick" autocomplete="off" readonly/><input type="hidden" v-model="codigoFuenteIngreso"/></td>

									<td class='tamano01'>Medio de Pago:</td>

                                    <td>
                                        <select v-model="medioPagoIngresos" style="width:100%" v-bind:class="parpadeomediopago" v-on:click="parpadeomediopago='';" >
                                            <option disabled value="">Seleccione un medio</option>
                                            <option value='CSF'>Con Situaci&oacute;n de Fondos</option>
                                            <option value='SSF'>Si Situaci&oacute;n de Fondos</option>
                                        </select>
                                    </td>
                                </tr>

                                <tr>
                                    <td class='tamano01' style="width:3cm;">Rubro:</td>
                                    <td style="width:35%"><input type="text" v-model="rubroIngreso" v-on:dblclick='toggleModalCuentasIngresos' class="colordobleclik" style="width:100%;height:30px;" readonly/><input type="hidden" v-model="codigoRubroIngreso"/></td>
                                    <td class='tamano01' style="width:3cm;">Clasificador:</td>
                                    <td colspan="3">
                                        <select v-show="(clasificadosIngresos.length > 0) ? true : false" v-model="clasificadorIngresos" v-on:change="desplegar();deshacer('12');" style="width:100%">
                                            <option disabled value="">Seleccione un medio de pago</option>
                                            <option v-for="cclasifica in clasificadosIngresos" :value="cclasifica[0]">{{ cclasifica[0] }} - {{ cclasifica[1] }}</option>
                                        </select>
                                        <select v-show="(clasificadosIngresos.length == 0) ? true : false" style="width:100%">
                                            <option value="0" selected>Sin Clasificador</option>
                                        </select>
                                    </td>
                                </tr>
                                <!-- Creación input politica publica -->
                                <tr>
                                    <td class='tamano01' style="width:3cm;">Pol&iacute;tica P&uacute;blica:</td>
                                    <td style="width:35%;">
                                        <input type="text" v-model="nPoliticaPublicaIngresos" ref="nPoliticaPublicaIngresos" v-on:dblclick='toggleModalPoliticaIngresos' style="width:100%;height:30px;" class="colordobleclik" readonly>
                                        <input type="hidden" v-model="codigoPoliticaPublicaIngresos"/>
                                    </td>

                                    <td class="tamano01" style="">Vigencia del Gasto:</td>
                                    <td style="width:60%" colspan="3">
                                        <select v-model="vigenciaGastoIngresos" style="width:100%">
                                            <option disabled value="">Seleccione una vigencia</option>
                                            <option v-for="vigenciadeGasto in vigenciasdeGastos" :value="vigenciadeGasto[1]">{{vigenciadeGasto[1]}} - {{vigenciadeGasto[2]}}</option>
                                        </select>
                                    </td>
                                </tr>
                            </table>
                            <table class="inicio ancho" v-show="showopcionIngresos1">
                                <tr>
                                    <td class='tamano01' style="width:3cm;">Id Entidad:</td>

                                    <td style="width:20%">
										<input type="text" v-model="identidadIngresos" v-on:dblclick='toggleModalIngresosCuin' style="width:100%;height:30px;" class="colordobleclik" autocomplete="off" readonly>
									</td>

                                    <td class='tamano01' style="width:3cm;">Nit:</td>

                                    <td style="width:20%">
										<input type="text" v-model="nitentidadIngresos" style="width:100%;height:30px;" readonly>
									</td>

                                    <td class='tamano01' style="width:3cm;">C&oacute;digo CUIN:</td>

                                    <td colspan="2">
										<input type="text" v-model="codigocuinIngresos" style="width:100%;height:30px;" readonly>
									</td>

                                    <td style="width:7%"></td>
                                </tr>
                                <!-- Agregar para clasificador CUIN -->
                                <tr>
                                    <td class='tamano01' >Entidad:</td>
                                    <td colspan="3"><input type="text" v-model="nomentidadIngrsos" style="width:100%;height:30px;" readonly/></td>
                                    <td class='tamano01' >Valor:</td>
                                    <td><input type="number" v-model="valorcuinIngresos" style="width:100%;height:30px;"/></td>
                                    <td style=" height: 30px;"><em class="botonflecha" v-on:click="agregarcuentaIngresos1()">Agregar</em></td>
                                </tr>
                            </table>
                            <!-- Agregar para sin clasificador -->
                            <table class="inicio ancho" v-show="showopcionIngresos2">
                                <tr>
                                    <td class='tamano01' style="width:3cm;">Valor:</td>
                                    <td width="26.2%"><input type="number" v-model="valorsinclasifiIngresos" style="width:100%;height:30px;"/></td>
                                    <td style=" height: 30px;"><em class="botonflecha" v-on:click="agregarcuentaIngresos2()">Agregar</em></td>
                                </tr>
                            </table>
                            <table class="inicio ancho" v-show="showopcionIngresos3">
                                <tr>
                                    <td class='tamano01' style="width:2.5cm;">Secci&oacute;n:</td>
                                    <td style="width:25%"><input type="text" v-model="seccionIngresos" v-on:dblclick='toggleModalSecciones' v-bind:class="secciondobleclick" style="width:100%;height:30px;" readonly/><input type="hidden" v-model="cseccionIngresos"/></td>
                                    <td class='tamano01' style="width:2.8cm;">Divisi&oacute;n:</td>
                                    <td ><input type="text" v-model="divisionIngresos" v-on:dblclick='toggleModalDivisiones' v-bind:class="divisiondobleclick" style="width:100%;height:30px;" readonly/><input type="hidden" v-model="cdivisionIngresos"/></td>
                                    <td class='tamano01' style="width:2.5cm;">Grupo:</td>
                                    <td ><input type="text" v-model="grupoIngresos" v-on:dblclick='toggleModalGrupos' v-bind:class="grupodobleclick" style="width:100%;height:30px;" readonly/><input type="hidden" v-model="cgrupoIngresos"/></td>
                                    <td style="width:7%;"></td>
                                </tr>
                                <tr>
                                    <td class='tamano01'>Clase:</td>
                                    <td><input type="text" v-model="claseIngresos" v-on:dblclick='toggleModalClase' v-bind:class="clasedobleclick" style="width:100%;height:30px;" readonly/><input type="hidden" v-model="cclaseIngresos"/></td>

                                    <td  class='tamano01'>Subclase:</td>
                                    <td><input type="text" v-model="subclaseIngresos" v-on:dblclick='toggleModalSubClases' v-bind:class="subclasedobleclick"  style="width:100%;height:30px;" ref="subclaseIngresos" v-on:keyup="validasubclase(subclase)"/><input type="hidden" v-model="csubclaseIngresos" readonly/></td>
                                    
                                    <td  class='tamano01' v-show="clasificadorIngresos == 2 ? true : false">Producto:</td>
                                    <td v-show="clasificadorIngresos == 2 ? true : false"><input type="text" v-model="subproductoIngresos" v-on:dblclick='toggleModalProducto' v-bind:class="subproductodobleclick" style="width:100%;height:30px;" ref="subproductoIngresos" v-on:keyup="validasubproducto(subproducto)" readonly/><input type="hidden" v-model="csubproductoIngresos"/></td>
                                </tr>
                                <!-- Agregar para clasificador bienes transportables y servicios -->
                                <tr>
                                    <td class='tamano01' >Valor:</td>
                                    <td><input type="number" v-model="valorrubroIngresos" style="width:100%;height:30px;" ref="valorrubroIngresos" v-bind:class="parpadeovalorrubro" v-on:keydown="parpadeovalorrubro='';"/></td>
                                    <td style=" height: 30px;"><em class="botonflecha" v-on:click="agregarcuentaIngresos()">Agregar</em></td>
                                </tr>
                            </table>
                            <div class="tabsmeci" style="width:99.8%" >
                                <div class="tab" >
                                    <input type="radio" id="tab-1h" name="tabgroupIngresos" v-model="tabgroupIngresos" v-bind:value="th1" >
                                    <label for="tab-1h">Sin Clasificador</label>
                                    <div class="content" style="overflow-x:hidden;" v-bind:style="{ height: tabheight1 }">
                                        <table class='inicio inicio--no-shadow'>
                                            <tbody>
                                                <tr class="titulos">
                                                    <td>Fuente</td>
													<td>Medio Pago</td>
                                                    <td>Rubro</td>
                                                    <td>Pol&iacute;tica P&uacute;blica</td>
                                                    <td>Vigencia de Gasto</td>
                                                    <td>valor</td>
                                                    <td></td>
                                                </tr>
                                                <tr v-for="(vsinclasificador, index) in selectcuetaseIngresos" v-bind:class="index % 2 ? 'saludo1a' : 'saludo2'" style='text-rendering: optimizeLegibility; cursor: pointer !important; style=\"cursor: hand\"' >
                                                    <td style="font: 120% sans-serif; padding-left:10px">{{ vsinclasificador[3] }}</td>
													<td style="font: 120% sans-serif; padding-left:10px">{{ vsinclasificador[6] }}</td>
                                                    <td style="font: 120% sans-serif; padding-left:10px;">{{ vsinclasificador[1] }}</td>
                                                    <td style="font: 120% sans-serif; padding-left:10px;">{{ vsinclasificador[4] }}</td>
                                                    <td style="font: 120% sans-serif; padding-left:10px">{{ vsinclasificador[5] }}</td>
                                                    <td style="font: 120% sans-serif; padding-left:10px;">{{ vsinclasificador[2] }}</td>
                                                    <td style="width:5%;" v-on:click="eliminacuentaIngresos2(index,vsinclasificador[2],vsinclasificador[5])"><img src='imagenes/del.png'></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="tab">
                                    <input type="radio" id="tab-2h" name="tabgroupIngresos" v-model="tabgroupIngresos" v-bind:value="th2" >
                                    <label for="tab-2h">Clasificador CUIN</label>
                                    <div class="content" style="overflow-x:hidden" v-bind:style="{ height: tabheight2 }">
                                        <table class='inicio inicio--no-shadow'>
                                            <tbody>
                                                <tr class="titulos">
                                                    <td>Fuente</td>	
													<td>Medio Pago</td>
                                                    <td>Rubro</td>
                                                    <td>Nit</td>
                                                    <td>Entidad</td>
                                                    <td>C&oacute;digo CUIN</td>
                                                    <td>Pol&iacute;tica P&uacute;blica</td>
                                                    <td>Vigencia de Gasto</td>
                                                    <td>valor</td>
                                                    <td></td>
                                                </tr>
                                                <tr v-for="(vcodigocuin, index) in selectcuetascIngresos" v-bind:class="index % 2 ? 'saludo1a' : 'saludo2'" style='text-rendering: optimizeLegibility; cursor: pointer !important; style=\"cursor: hand\"' >
                                                    <td style="font: 120% sans-serif; padding-left:10px">{{ vcodigocuin[5] }}</td>
													<td style="font: 120% sans-serif; padding-left:10px;">{{ vcodigocuin[9] }}</td>
                                                    <td style="font: 120% sans-serif; padding-left:10px">{{ vcodigocuin[6] }}</td>
                                                    <td style="font: 120% sans-serif; padding-left:10px">{{ vcodigocuin[1] }}</td>
                                                    <td style="font: 120% sans-serif; padding-left:10px">{{ vcodigocuin[2] }}</td>
                                                    <td style="font: 120% sans-serif; padding-left:10px; width:15%;">{{ vcodigocuin[3] }}</td>
                                                    <td style="font: 120% sans-serif; padding-left:10px">{{ vcodigocuin[7] }}</td>
                                                    <td style="font: 120% sans-serif; padding-left:10px; ">{{ vcodigocuin[8] }}</td>
                                                    <td style="font: 120% sans-serif; padding-left:10px;">{{ vcodigocuin[4] }}</td>
                                                    <td style="width:5%;" v-on:click="eliminacuentaIngresos1(index,vcodigocuin[4])"><img src='imagenes/del.png'></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="tab">
                                    <input type="radio" id="tab-3h" name="tabgroupIngresos" v-model="tabgroupIngresos" v-bind:value="th3" >
                                    <label for="tab-3h">Clasificador B y S</label>
                                    <div class="content" style="overflow-x:hidden" v-bind:style="{ height: tapheight3 }">
                                        <table class='inicio inicio--no-shadow'>
                                            <tbody>
                                                <tr class="titulos">
                                                    <td>Fuente</td>
													<td>Medio Pago</td>
                                                    <td>Rubro</td>
                                                    <td>Subclase</td>
                                                    <td>producto</td>
                                                    <td>Pol&iacute;tica P&uacute;blica</td>
                                                    <td>Vigencia de Gasto</td>
                                                    <td>valor</td>
                                                    <td></td>
                                                </tr>
                                                <tr v-for="(vccuenta, index) in selectcuetasaIngresos" v-bind:class="index % 2 ? 'saludo1a' : 'saludo2'" style='text-rendering: optimizeLegibility; cursor: pointer !important; style=\"cursor: hand\"' >
                                                    <td style="font: 120% sans-serif; padding-left:10px;">{{ vccuenta[4] }}</td>
													<td style="font: 120% sans-serif; padding-left:10px;">{{ vccuenta[8] }}</td>
                                                    <td style="font: 120% sans-serif; padding-left:10px;">{{ vccuenta[1] }}</td>
                                                    <td style="font: 120% sans-serif; padding-left:10px;">{{ vccuenta[2] }}</td>
                                                    <td style="font: 120% sans-serif; padding-left:10px;">{{ vccuenta[5] }}</td>
                                                    <td style="font: 120% sans-serif; padding-left:10px;">{{ vccuenta[6] }}</td>
                                                    <td style="font: 120% sans-serif; padding-left:10px;">{{ vccuenta[7] }}</td>
                                                    <td style="font: 120% sans-serif; padding-left:10px;">{{ vccuenta[3] }}</td>
                                                    <td style="width:5%;" v-on:click="eliminacuentasIngresos(index,vccuenta[3],vccuenta[7])"><img src='imagenes/del.png'></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>



					<div class="tab">
                        <input type="radio" id="tab-4" name="tabgroup1" value="4" <?php echo $check4;?> >
                        <label for="tab-4">Funcionamiento</label>
                        <div class="content" style="overflow:hidden;">
                            <table class="inicio ancho">
                                <tr>
                                    <td class="titulos" colspan="6">Ingresar Reduccion</td>
                                    <td class="cerrar" style="width:7%" onClick="location.href='plan-principal.php'">Cerrar</td>
                                </tr>
                                
                                <tr>
                                    <td class='tamano01'>Fuente:</td>

                                    <td>
										<input type="text" v-model="fuenteFuncionamiento" ref ="fuenteFuncionamiento" v-on:dblclick='toggleModalFuncionamientoFuente' style="width:100%;height:30px;" v-bind:class="fuentedobleclick" autocomplete="off" readonly/>
										<input type="hidden" v-model="codigoFuenteFuncionamiento"/>
									</td>

									<td class='tamano01'>Medio de Pago:</td>

                                    <td>
                                        <select v-model="medioPagoFuncionamiento" style="width:100%" v-bind:class="parpadeomediopago" v-on:click="parpadeomediopago='';" >
                                            <option disabled value="">Seleccione un medio</option>
                                            <option value='CSF'>Con Situaci&oacute;n de Fondos</option>
                                            <option value='SSF'>Si Situaci&oacute;n de Fondos</option>
                                        </select>
                                    </td>
                                </tr>

                                <tr>
                                    <td class='tamano01' style="width:3cm;">Rubro:</td>

                                    <td style="width:35%">
										<input type="text" v-model="rubroFuncionamiento" v-on:dblclick='toggleModalCuentasFuncionamiento' class="colordobleclik" style="width:100%;height:30px;" readonly/>
										<input type="hidden" v-model="codigoRubroFuncionamiento"/>
									</td>

                                    <td class='tamano01' style="width:3cm;">Clasificador:</td>

                                    <td colspan="3">
                                        <select v-show="(clasificadosFuncionamiento.length > 0) ? true : false" v-model="clasificadorFuncionamiento" v-on:change="desplegarFuncionamiento();deshacer('12');" style="width:100%">
                                            <option disabled value="">Seleccione un medio de pago</option>
                                            <option v-for="cclasifica in clasificadosFuncionamiento" :value="cclasifica[0]">{{ cclasifica[0] }} - {{ cclasifica[1] }}</option>
                                        </select>
                                        <select v-show="(clasificadosFuncionamiento.length == 0) ? true : false" style="width:100%">
                                            <option value="0" selected>Sin Clasificador</option>
                                        </select>
                                    </td>
                                </tr>
                                <!-- Creación input politica publica -->
                                <tr>
                                    <td class='tamano01' style="width:3cm;">Pol&iacute;tica P&uacute;blica:</td>
                                    <td style="width:35%;">
                                        <input type="text" v-model="nPoliticaPublicaFuncionamiento" ref="nPoliticaPublicaFuncionamiento" v-on:dblclick='toggleModalPoliticaFuncionamiento' style="width:100%;height:30px;" class="colordobleclik" readonly>
                                        <input type="hidden" v-model="codigoPoliticaPublicaFuncionamiento"/>
                                    </td>

                                    <td class="tamano01" style="">Vigencia del Gasto:</td>
                                    <td style="width:60%" colspan="3">
                                        <select v-model="vigenciaGastoFuncionamiento" style="width:100%">
                                            <option disabled value="">Seleccione una vigencia</option>
                                            <option v-for="vigenciadeGasto in vigenciasdeGastos" :value="vigenciadeGasto[1]">{{vigenciadeGasto[1]}} - {{vigenciadeGasto[2]}}</option>
                                        </select>
                                    </td>
                                </tr>
								<tr>
									<td class='tamano01' style="width:3cm;">Saldo maxio reducir</td>
                                    <td style="width:35%;">
                                        <input type="text" v-model="maximaReduccionFuncionamiento" style="width:100%;height:30px;" readonly>
                                    </td>
									<td></td>
									<td style=" height: 30px;"><em class="botonflecha" v-on:click="validarSaldo()">Cosultar Saldo</em></td>
								</tr>
                            </table>
                            <table class="inicio ancho" v-show="showopcionFuncionamiento1">
                                <tr>
                                    <td class='tamano01' style="width:3cm;">Id Entidad:</td>
                                    <td style="width:20%"><input type="text" v-model="identidadFuncionamiento" v-on:dblclick='toggleModalFuncionamientoCuin' style="width:100%;height:30px;" class="colordobleclik" autocomplete="off" readonly></td>
                                    <td class='tamano01' style="width:3cm;">Nit:</td>
                                    <td style="width:20%"><input type="text" v-model="nitentidadFuncionamiento" style="width:100%;height:30px;" readonly></td>
                                    <td class='tamano01' style="width:3cm;">C&oacute;digo CUIN:</td>
                                    <td colspan="2"><input type="text" v-model="codigocuinFuncionamiento" style="width:100%;height:30px;" readonly></td>
                                    <td style="width:7%"></td>
                                </tr>
                                <!-- Agregar para clasificador CUIN -->
                                <tr>
                                    <td class='tamano01' >Entidad:</td>
                                    <td colspan="3"><input type="text" v-model="nomentidadFuncionamiento" style="width:100%;height:30px;" readonly/></td>
                                    <td class='tamano01' >Valor:</td>
                                    <td><input type="number" v-model="valorcuinFuncionamiento" style="width:100%;height:30px;"/></td>
                                    <td style=" height: 30px;"><em class="botonflecha" v-on:click="agregarcuentaFuncionamiento1()">Agregar</em></td>
                                </tr>
                            </table>
                            <!-- Agregar para sin clasificador -->
                            <table class="inicio ancho" v-show="showopcionFuncionamiento2">
                                <tr>
                                    <td class='tamano01' style="width:3cm;">Valor:</td>
                                    <td width="26.2%"><input type="number" v-model="valorsinclasifiFuncionamiento" style="width:100%;height:30px;"/></td>
                                    <td style=" height: 30px;"><em class="botonflecha" v-on:click="agregarcuentaFuncionamiento2()">Agregar</em></td>
                                </tr>
                            </table>
                            <table class="inicio ancho" v-show="showopcionFuncionamiento3">
                                <tr>
                                    <td class='tamano01' style="width:2.5cm;">Secci&oacute;n:</td>

                                    <td style="width:25%">
										<input type="text" v-model="seccionFuncionamiento" v-on:dblclick='toggleModalSeccionesFuncionamiento' v-bind:class="secciondobleclick" style="width:100%;height:30px;" readonly/>
										<input type="hidden" v-model="cseccionFuncionamiento"/>
									</td>
									
                                    <td class='tamano01' style="width:2.8cm;">Divisi&oacute;n:</td>

                                    <td>
										<input type="text" v-model="divisionFuncionamiento" v-on:dblclick='toggleModalDivisionesFuncionamiento' v-bind:class="divisiondobleclick" style="width:100%;height:30px;" readonly/>
										<input type="hidden" v-model="cdivisionFuncionamiento"/>
									</td>

                                    <td class='tamano01' style="width:2.5cm;">Grupo:</td>

                                    <td>
										<input type="text" v-model="grupoFuncionamiento" v-on:dblclick='toggleModalGruposFuncionamiento' v-bind:class="grupodobleclick" style="width:100%;height:30px;" readonly/>
										<input type="hidden" v-model="cgrupoFuncionamiento"/>
									</td>
									
                                    <td style="width:7%;"></td>
                                </tr>
                                <tr>
                                    <td class='tamano01'>Clase:</td>
                                    <td><input type="text" v-model="claseFuncionamiento" v-on:dblclick='toggleModalClaseFuncionamiento' v-bind:class="clasedobleclick" style="width:100%;height:30px;" readonly/><input type="hidden" v-model="cclaseFuncionamiento"/></td>

                                    <td  class='tamano01'>Subclase:</td>
                                    <td><input type="text" v-model="subclaseFuncionamiento" v-on:dblclick='toggleModalSubClasesFuncionamiento' v-bind:class="subclasedobleclick"  style="width:100%;height:30px;"/><input type="hidden" v-model="csubclaseIngresos" readonly/></td>
                                    
                                    <td  class='tamano01' v-show="clasificadorFuncionamiento == 2 ? true : false">Producto:</td>
                                    <td v-show="clasificadorFuncionamiento == 2 ? true : false">
										<input type="text" v-model="subproductoFuncionamiento" v-on:dblclick='toggleModalProductoFuncionamiento' v-bind:class="subproductodobleclick" style="width:100%;height:30px;" readonly/>
										<input type="hidden" v-model="csubproductoFuncionamiento"/>
									</td>
                                </tr>
                                <!-- Agregar para clasificador bienes transportables y servicios -->
                                <tr>
                                    <td class='tamano01' >Valor:</td>
                                    <td><input type="number" v-model="valorrubroFuncionamiento" style="width:100%;height:30px;" ref="valorrubroFuncionamiento" v-bind:class="parpadeovalorrubro" v-on:keydown="parpadeovalorrubro='';"/></td>
                                    <td style=" height: 30px;"><em class="botonflecha" v-on:click="agregarcuentaFuncionamiento()">Agregar</em></td>
                                </tr>
                            </table>
                            <div class="tabsmeci" style="width:99.8%" >
                                <div class="tab" >
                                    <input type="radio" id="tab-1f" name="tabgroupFuncionamiento" v-model="tabgroupFuncionamiento" v-bind:value="tf1" >
                                    <label for="tab-1f">Sin Clasificador</label>
                                    <div class="content" style="overflow-x:hidden;" v-bind:style="{ height: tabheightFun1 }">
                                        <table class='inicio inicio--no-shadow'>
                                            <tbody>
                                                <tr class="titulos">
                                                    <td>Fuente</td>
													<td>Medio Pago</td>
                                                    <td>Rubro</td>
                                                    <td>Pol&iacute;tica P&uacute;blica</td>
                                                    <td>Vigencia de Gasto</td>
                                                    <td>valor</td>
                                                    <td></td>
                                                </tr>
                                                <tr v-for="(vsinclasificador, index) in selectcuetaseFuncionamiento" v-bind:class="index % 2 ? 'saludo1a' : 'saludo2'" style='text-rendering: optimizeLegibility; cursor: pointer !important; style=\"cursor: hand\"' >
                                                    <td style="font: 120% sans-serif; padding-left:10px">{{ vsinclasificador[3] }}</td>
													<td style="font: 120% sans-serif; padding-left:10px">{{ vsinclasificador[6] }}</td>
                                                    <td style="font: 120% sans-serif; padding-left:10px;">{{ vsinclasificador[1] }}</td>
                                                    <td style="font: 120% sans-serif; padding-left:10px;">{{ vsinclasificador[4] }}</td>
                                                    <td style="font: 120% sans-serif; padding-left:10px">{{ vsinclasificador[5] }}</td>
                                                    <td style="font: 120% sans-serif; padding-left:10px;">{{ vsinclasificador[2] }}</td>
                                                    <td style="width:5%;" v-on:click="eliminacuentaFuncionamiento2(index,vsinclasificador[2],vsinclasificador[5])"><img src='imagenes/del.png'></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="tab">
                                    <input type="radio" id="tab-2f" name="tabgroupFuncionamiento" v-model="tabgroupFuncionamiento" v-bind:value="tf2" >
                                    <label for="tab-2f">Clasificador CUIN</label>
                                    <div class="content" style="overflow-x:hidden" v-bind:style="{ height: tabheightFun2 }">
                                        <table class='inicio inicio--no-shadow'>
                                            <tbody>
                                                <tr class="titulos">
                                                    <td>Fuente</td>	
													<td>Medio Pago</td>
                                                    <td>Rubro</td>
                                                    <td>Nit</td>
                                                    <td>Entidad</td>
                                                    <td>C&oacute;digo CUIN</td>
                                                    <td>Pol&iacute;tica P&uacute;blica</td>
                                                    <td>Vigencia de Gasto</td>
                                                    <td>valor</td>
                                                    <td></td>
                                                </tr>
                                                <tr v-for="(vcodigocuin, index) in selectcuetascFuncionamiento" v-bind:class="index % 2 ? 'saludo1a' : 'saludo2'" style='text-rendering: optimizeLegibility; cursor: pointer !important; style=\"cursor: hand\"' >
                                                    <td style="font: 120% sans-serif; padding-left:10px">{{ vcodigocuin[5] }}</td>
													<td style="font: 120% sans-serif; padding-left:10px; ">{{ vcodigocuin[9] }}</td>
                                                    <td style="font: 120% sans-serif; padding-left:10px">{{ vcodigocuin[6] }}</td>
                                                    <td style="font: 120% sans-serif; padding-left:10px">{{ vcodigocuin[1] }}</td>
                                                    <td style="font: 120% sans-serif; padding-left:10px">{{ vcodigocuin[2] }}</td>
                                                    <td style="font: 120% sans-serif; padding-left:10px; width:15%;">{{ vcodigocuin[3] }}</td>
                                                    <td style="font: 120% sans-serif; padding-left:10px">{{ vcodigocuin[7] }}</td>
                                                    <td style="font: 120% sans-serif; padding-left:10px; ">{{ vcodigocuin[8] }}</td>
                                                    <td style="font: 120% sans-serif; padding-left:10px;">{{ vcodigocuin[4] }}</td>
                                                    <td style="width:5%;" v-on:click="eliminacuentaFuncionamiento1(index,vcodigocuin[4])"><img src='imagenes/del.png'></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="tab">
                                    <input type="radio" id="tab-3f" name="tabgroupFuncionamiento" v-model="tabgroupFuncionamiento" v-bind:value="tf3" >
                                    <label for="tab-3f">Clasificador B y S</label>
                                    <div class="content" style="overflow-x:hidden" v-bind:style="{ height: tabheightFun3 }">
                                        <table class='inicio inicio--no-shadow'>
                                            <tbody>
                                                <tr class="titulos">
                                                    <td>Fuente</td>
													<td>Medio Pago</td>
                                                    <td>Rubro</td>
                                                    <td>Subclase</td>
                                                    <td>producto</td>
                                                    <td>Pol&iacute;tica P&uacute;blica</td>
                                                    <td>Vigencia de Gasto</td>
                                                    <td>valor</td>
                                                    <td></td>
                                                </tr>
                                                <tr v-for="(vccuenta, index) in selectcuetasaFuncionamiento" v-bind:class="index % 2 ? 'saludo1a' : 'saludo2'" style='text-rendering: optimizeLegibility; cursor: pointer !important; style=\"cursor: hand\"' >
                                                    <td style="font: 120% sans-serif; padding-left:10px;">{{ vccuenta[4] }}</td>
													<td style="font: 120% sans-serif; padding-left:10px;">{{ vccuenta[8] }}</td>
                                                    <td style="font: 120% sans-serif; padding-left:10px;">{{ vccuenta[1] }}</td>
                                                    <td style="font: 120% sans-serif; padding-left:10px;">{{ vccuenta[2] }}</td>
                                                    <td style="font: 120% sans-serif; padding-left:10px;">{{ vccuenta[5] }}</td>
                                                    <td style="font: 120% sans-serif; padding-left:10px;">{{ vccuenta[6] }}</td>
                                                    <td style="font: 120% sans-serif; padding-left:10px;">{{ vccuenta[7] }}</td>
                                                    <td style="font: 120% sans-serif; padding-left:10px;">{{ vccuenta[3] }}</td>
                                                    <td style="width:5%;" v-on:click="eliminacuentasFuncionamiento(index,vccuenta[3],vccuenta[7])"><img src='imagenes/del.png'></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="tab">
                        <input type="radio" id="tab-5" name="tabgroup1" value="5" <?php echo $check5;?> >
                        <label for="tab-5">Reducciones</label>
                        <div class="content" style="overflow:hidden;">
                            <table class="inicio ancho">
                                <tr>
                                    <td class="titulos" colspan="6">Reducciones</td>
                                    <td class="cerrar" style="width:7%" onClick="location.href='plan-principal.php'">Cerrar</td>
                                </tr>
                                
                                <tr>
                                    <td style="width:300px;">
										<em class="botonflecha" v-on:click="MostrarPresupuesto()">Agregar Reducci&oacute;n</em>
									</td>

									<td style="width: 100px;" class="tamano01">Finalizar</td>
									<td>
										<input type="checkbox" id="checkbox" v-model="checked" v-on:click="finalizarReduccion">
										<!-- <label for="checkbox">{{ checked }}</label> -->
									</td>
                                </tr>
                            </table>
                            
                            <div class="tabsmeci" style="width:99.8%" >
                                <div class="tab" >
                                    <input type="radio" id="tab-1g" name="tabgroupMostrar" v-model="tabgroupMostrar" v-bind:value="1" >
                                    <label for="tab-1g">Presupuesto Gastos</label>
                                    <div class="content" style="overflow-x:hidden;" v-bind:style="{ height: tabheight10 }">
                                        <table class='inicio inicio--no-shadow'>
                                            <tbody>
                                                <tr class="titulos">
                                                    <td>Bpin</td>
                                                    <td>Rubro</td>
                                                    <td>Fuente</td>
                                                    <td>Vigencia del Gasto</td>
                                                    <td>Politica Publica</td>
                                                    <td>Meta</td>
                                                    <td>Medio de Pago</td>
                                                    <td>Cuin/Bienes/Servicios</td>
                                                    <td>Valor Gasto</td>
													<td></td>
                                                </tr>
                                                <tr v-for="(vccuenta, index) in presupuestoTotalGastos" v-bind:class="index % 2 ? 'saludo1a' : 'saludo2'" style='text-rendering: optimizeLegibility; cursor: pointer !important; style=\"cursor: hand\"' >
                                                    <td style="font: 120% sans-serif; padding-left:10px;">{{ vccuenta[0] }}</td>
                                                    <td style="font: 120% sans-serif; padding-left:10px;">{{ vccuenta[1] }}</td>
                                                    <td style="font: 120% sans-serif; padding-left:10px;">{{ vccuenta[2] }}</td>
                                                    <td style="font: 120% sans-serif; padding-left:10px;">{{ vccuenta[3] }}</td>
                                                    <td style="font: 120% sans-serif; padding-left:10px;">{{ vccuenta[4] }}</td>
                                                    <td style="font: 120% sans-serif; padding-left:10px;">{{ vccuenta[5] }}</td>
                                                    <td style="font: 120% sans-serif; padding-left:10px;">{{ vccuenta[6] }}</td>
                                                    <td style="font: 120% sans-serif; padding-left:10px;">{{ vccuenta[7] }}</td>
                                                    <td style="font: 120% sans-serif; padding-left:10px;">{{ vccuenta[8] }}</td>
													<td style="width:5%;text-align:center;" v-on:click="eliminarcuentagastos(index)"><img src='imagenes/del.png'></td>
                                                </tr>		
												<tr style="background-color: #76FDA6">
													<td colspan='8' style="font:150% sans-serif; padding-left:30px; font-weight: bold">Total Ingresos</td>
													<td style="font: 120% sans-serif; padding-left:10px;">{{ valorTotalGastos }}</td>
												</tr>							
                                            </tbody>
                                        </table>
                                    </div>	
                                </div>
                                <div class="tab" >
                                    <input type="radio" id="tab-2g" name="tabgroupMostrar" v-model="tabgroupMostrar" v-bind:value="2" >
                                    <label for="tab-2g">Presupuesto Ingresos</label>
                                    <div class="content" style="overflow-x:hidden;" v-bind:style="{ height: tabheight10 }">
                                        <table class='inicio inicio--no-shadow'>
                                            <tbody>
                                                <tr class="titulos">
                                                    
                                                    <td>Rubro</td>
                                                    <td>Fuente</td>
                                                    <td>Vigencia del Gasto</td>
													<td>Politica Publica</td>
                                                    <td>Cuin/Bienes/Servicios</td>
                                                    <td>Valor Ingresos</td>
													<td></td>
                                                </tr>
                                                <tr v-for="(vccuenta, index) in presupuestoTotalIngresos" v-bind:class="index % 2 ? 'saludo1a' : 'saludo2'" style='text-rendering: optimizeLegibility; cursor: pointer !important; style=\"cursor: hand\"' >
                                                    
                                                    <td style="font: 120% sans-serif; padding-left:10px;">{{ vccuenta[0] }}</td>
                                                    <td style="font: 120% sans-serif; padding-left:10px;">{{ vccuenta[1] }}</td>
                                                    <td style="font: 120% sans-serif; padding-left:10px;">{{ vccuenta[2] }}</td>
													<td style="font: 120% sans-serif; padding-left:10px;">{{ vccuenta[3] }}</td>
                                                    <td style="font: 120% sans-serif; padding-left:10px;">{{ vccuenta[4] }}</td>
                                                    <td style="font: 120% sans-serif; padding-left:10px;">{{ vccuenta[5] }}</td>
													<td style="width:5%;text-align:center;" v-on:click="eliminarcuentaIngresos(index)"><img src='imagenes/del.png'></td>
                                                </tr>									
												<tr style="background-color: #76FDA6">
													<td colspan='5' style="font:150% sans-serif; padding-left:30px; font-weight: bold">Total Ingresos</td>
													<td style="font: 120% sans-serif; padding-left:10px;">{{ valorTotalIngresos }}</td>
												</tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- termina -->

				<div v-show="showMensaje">
					<transition name="modal">
						<div class="modal-mask">
							<div class="modal-wrapper">
								<div class="modal-container1">
									<table id='ventanamensaje1' class='inicio' style="border-radius: 10px;">
										<tr >
											<td class="titulosmensajes1" v-bind:style="{color:colortitulosmensaje,}" style=" text-shadow: 7px 4px 5px grey;font-style: italic;border-radius: 50px;">{{titulomensaje}}</td>
										</tr>
										<tr>
											<td class='.cuerpomensajes1' style="text-align:center;"><h3 style="font-size: 20px;font-family:Gotham, 'Helvetica Neue', Helvetica, Arial, 'sans-serif';font-style: italic;">{{ contenidomensaje }}</h3></td>
										</tr>
										<tr>
											<td class='.cuerpomensajes1' style="padding: 14px;text-align:center">
												<em name="continuar" id="continuar" class="botonflecha" @click="toggleMensaje()">Continuar</em>
											</td>
										</tr>
									</table>
								</div>
							</div>
						</div>
					</transition>
				</div>
				<div v-show="showMensajeSN">
					<transition name="modal">
						<div class="modal-mask">
							<div class="modal-wrapper">
								<div class="modal-container1">
									<table id='ventanamensaje1' class='inicio' style="border-radius: 10px;">
										<tr >
											<td class="titulosmensajes1" v-bind:style="{color:colortitulosmensaje,}" style=" text-shadow: 7px 4px 5px grey;font-style: italic;border-radius: 50px;">{{titulomensaje}}</td>
										</tr>
										<tr>
											<td class='.cuerpomensajes1' style="text-align:center;"><h3 style="font-size: 20px;font-family:Gotham, 'Helvetica Neue', Helvetica, Arial, 'sans-serif';font-style: italic;">{{ contenidomensaje }}</h3></td>
										</tr>
										<tr>
											<td class='.cuerpomensajes1' style="padding: 14px;text-align:center">
												<em name="continuar" id="continuar" class="botonflechaverde" @click="toggleMensajeSN('1','S')">Aceptar</em> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
												<em name="continuar" id="continuar" class="botonflecharoja" @click="toggleMensajeSN('1','N')">Cancelar</em>
											</td>
										</tr>
									</table>
								</div>
							</div>
						</div>
					</transition>
				</div>
				<div v-show="showModal4">
					<transition name="modal">
						<div class="modal-mask">
							<div class="modal-wrapper">
								<div class="modal-container">
									<table class="inicio ancho">
										<tr>
											<td class="titulos" colspan="2" >SELECCIONAR CUENTAS PRESUPUESTALES</td>
											<td class="cerrar" style="width:7%" @click="showModal4 = false">Cerrar</td>
										</tr>
										<tr>
											<td class="tamano01" style="width:3cm">Descripci&oacute;n:</td>
											<td><input type="text" class="form-control" placeholder="Buscar por descripcion cuenta" v-on:keyup="searchMonitorCuentasPresupuestales" v-model="searchCuentaPresupuestal.keywordCuentaPresupuestal" style="width:100%" /></td>
										</tr>
									</table>
									<table>
										<thead>
											<tr>

												<td class='titulos' style="font: 160% sans-serif; ">C&oacute;digo</td>
												<td class='titulos' style="font:160% sans-serif; ">Nombre</td>
												<td class='titulos' style="font: 160% sans-serif; ">Tipo</td>
												<td class='titulos' style="font: 160% sans-serif; ">Fuente</td>
												<td class='titulos' style="font: 160% sans-serif; ">Indicador</td>
												<td class='titulos' style="font: 160% sans-serif; ">Subclase</td>
												<td class='titulos' style="font: 160% sans-serif; ">Subproducto</td>
                                                <td class='titulos' style="font: 160% sans-serif; ">Valor</td>

											</tr>
										</thead>
									</table>
									<div style='margin: 0px 5px 5px; border-radius: 0 0 5px 5px; height: 200px; overflow: scroll; overflow-x: hidden; background: white;'>
										<table class='inicio inicio--no-shadow'>
											<tbody>
												<tr v-for="(cuentapre,index) in rubrosExistentes" v-on:click="cargacuenta(cuentapre[0],cuentapre[1],cuentapre[4],cuentapre[3],cuentapre[5])"  v-bind:class="index % 2 ? 'saludo1a' : 'saludo2'" style='text-rendering: optimizeLegibility; cursor: pointer !important; style=\"cursor: hand\"'>
													<td style="font: 120% sans-serif; padding-left:10px; width: 10%;">{{ cuentapre[0] }}</td>
													<td style="font: 120% sans-serif; padding-left:10px">{{ cuentapre[1] }}</td>
													<td  style="width: 8%; font: 120% sans-serif; padding-left:10px">{{ cuentapre[2] }}</td>
													<td style="font: 120% sans-serif; padding-left:10px">{{ cuentapre[8] }}</td>
													<td style="font: 120% sans-serif; padding-left:10px">{{ cuentapre[9] }}</td>
													<td style="font: 120% sans-serif; padding-left:10px">{{ cuentapre[6] }}</td>
													<td style="font: 120% sans-serif; padding-left:10px">{{ cuentapre[7] }}</td>
                                                    <td style="font: 120% sans-serif; padding-left:10px">{{ cuentapre[3] }}</td>
												</tr>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</transition>
				</div>
				
                <!-- Modal de Proyectos existentes -->
                <div v-show="showModal15">
					<transition name="modal">
						<div class="modal-mask">
							<div class="modal-wrapper">
								<div class="modal-container3">
									<table class="inicio ancho">
										<tr>
											<td class="titulos" colspan="2" >SELECCIONAR PROYECTO EXISTENTE</td>
											<td class="cerrar" style="width:7%" @click="showModal15 = false">Cerrar</td>
										</tr>
										<tr>
											<td class="tamano01" style="width:3cm">NOMBRE:</td>
											<td><input type="text" class="form-control" placeholder="Buscar por nombre del proyecto" v-on:keyup="searchMonitorpp" v-model="searchpoliticaspublicas.keywordpp" style="width:100%" /></td>
										</tr>
									</table>
									<table>
										<thead>
											<tr>
												<td class='titulos' style="font: 160% sans-serif; border-radius: 5px 0px 0px 0px; width:10.5%;">Codigo</td>
												<td class='titulos' style="font: 160% sans-serif; width:38.9%;">Nombre</td>
                                                <td class='titulos' style="font: 160% sans-serif; width:38.9%;">Descripci&oacute;n</td>
                                                <td class='titulos' style="font: 160% sans-serif; ">Valor</td>
											</tr>
										</thead>
									</table>
									<div style='margin: 0px 5px 5px; border-radius: 0 0 5px 5px; height: 200px; overflow: scroll; overflow-x: hidden; background: white;'>
										<table class='inicio inicio--no-shadow'>
											<tbody>
												<tr v-for="(proyec,index) in proyectos" v-on:click="cargaProyectosExistentes(proyec[0],proyec[1],proyec[2],proyec[4],proyec[5])"  v-bind:class="index % 2 ? 'saludo1a' : 'saludo2'" style='text-rendering: optimizeLegibility; cursor: pointer !important; style=\"cursor: hand\"'>
													<td width="10%" style="font: 120% sans-serif;text-align:center;">{{ proyec[0] }}</td>
													<td width="40%" style="font: 120% sans-serif;">{{ proyec[1] }}</td>
                                                    <td width="40%" style="font: 120% sans-serif;">{{ proyec[2] }}</td>
                                                    <td width="10%" style="font: 120% sans-serif;">{{ proyec[3] }}</td>
												</tr>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</transition>
				</div>
                <!-- Segundo modal de fuentes  -->
                <div v-show="showModalIngresos1">
					<transition name="modal">
						<div class="modal-mask">
							<div class="modal-wrapper">
								<div class="modal-container3">
									<table class="inicio ancho">
										<tr>
											<td class="titulos" colspan="2" >SELECCIONAR FUENTE</td>
											<td class="cerrar" style="width:7%" @click="showModalIngresos1 = false">Cerrar</td>
										</tr>
										<tr>
											<td class="tamano01" style="width:3cm">NOMBRE:</td>
											<td><input type="text" class="form-control" placeholder="Buscar por nombre de la Fuente" v-on:keyup="searchMonitorfuentes" v-model="searchfuentes.keyword" style="width:100%" /></td>
										</tr>
									</table>
									<table>
										<thead>
											<tr>
												<td class='titulos' style="font: 160% sans-serif; border-radius: 5px 0px 0px 0px; width:4.9%;">Id fuente</td>
												<td class='titulos' style="font: 160% sans-serif; width:10%;">Entidad financiadora</td>
											</tr>
										</thead>
									</table>
									<div style='margin: 0px 5px 5px; border-radius: 0 0 5px 5px; height: 200px; overflow: scroll; overflow-x: hidden; background: white;'>
										<table class='inicio inicio--no-shadow'>
											<tbody>
												<tr v-for="(result,index) in results" v-on:click="cargaFuenteIngresos(result[1],result[2])"  v-bind:class="index % 2 ? 'saludo1a' : 'saludo2'" style='text-rendering: optimizeLegibility; cursor: pointer !important; style=\"cursor: hand\"'>
													<td width="30%" style="font: 160% sans-serif;">{{ result[1] }}</td>
													<td width="60%" style="font: 160% sans-serif;">{{ result[2] }}</td>
												</tr>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</transition>
				</div>
                <div v-show="showModalIngresos2">
					<transition name="modal">
						<div class="modal-mask">
							<div class="modal-wrapper">
								<div class="modal-container">
									<table class="inicio ancho">
										<tr>
											<td class="titulos" colspan="2" >SELECCIONAR CUENTAS PRESUPUESTALES</td>
											<td class="cerrar" style="width:7%" @click="showModalIngresos2 = false">Cerrar</td>
										</tr>
										<tr>
											<td class="tamano01" style="width:3cm">Descripci&oacute;n:</td>
											<td><input type="text" class="form-control" placeholder="Buscar por descripcion cuenta" v-on:keyup="searchMonitorCuentasPresupuestales" v-model="searchCuentaPresupuestal.keywordCuentaPresupuestal" style="width:100%" /></td>
										</tr>
									</table>
									<table>
										<thead>
											<tr>

												<td class='titulos' style="width:30%; font: 160% sans-serif; ">C&oacute;digo</td>
												<td class='titulos' style="font: 160% sans-serif; ">Nombre</td>
												<td class='titulos' style="width:10%; font: 160% sans-serif; ">Tipo</td>

											</tr>
										</thead>
									</table>
									<div style='margin: 0px 5px 5px; border-radius: 0 0 5px 5px; height: 200px; overflow: scroll; overflow-x: hidden; background: white;'>
										<table class='inicio inicio--no-shadow'>
											<tbody>
												<tr v-for="(cuentapre,index) in cuentasIngresos" v-on:click="cargaCuentaIngresos(cuentapre[0],cuentapre[1],cuentapre[2])"  v-bind:class="index % 2 ? 'saludo1a' : 'saludo2'" style='text-rendering: optimizeLegibility; cursor: pointer !important; style=\"cursor: hand\"'>
													<td style="font: 120% sans-serif; padding-left:10px; width: 30%;">{{ cuentapre[0] }}</td>
													<td style="font: 120% sans-serif; padding-left:10px">{{ cuentapre[1] }}</td>
													<td  style="width: 8%; font: 120% sans-serif; padding-left:10px">{{ cuentapre[2] }}</td>
												</tr>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</transition>
				</div>
                <!-- Cuin para ingresos -->
                <div v-show="showModalIngresos3">
					<transition name="modal">
						<div class="modal-mask">
							<div class="modal-wrapper">
								<div class="modal-container3">
									<table class="inicio ancho">
										<tr>
											<td class="titulos" colspan="2" >SELECCIONAR FUENTE</td>
											<td class="cerrar" style="width:7%" @click="showModalIngreso3 = false">Cerrar</td>
										</tr>
										<tr>
											<td class="tamano01" style="width:3cm">NOMBRE:</td>
											<td><input type="text" class="form-control" placeholder="Buscar por nombre de la Fuente" v-on:keyup="searchMonitorProducts" v-model="searchProduct.keywordProduct" style="width:100%" /></td>
										</tr>
									</table>
									<table>
										<thead>
											<tr>
												<td width="1%" class='titulos' style="font: 100% sans-serif; border-radius: 5px 0px 0px;">No</td>
												<td width="8%" class='titulos' style="font: 100% sans-serif;">Id entidad</td>
												<td width="8%" class='titulos' style="font: 100% sans-serif;">Nit</td>
												<td width="34%" class='titulos' style="font: 100% sans-serif;">Nombre</td>
												<td width="4%" class='titulos' style="font: 100% sans-serif;">Sector</td>
												<td width="5%" class='titulos' style="font: 100% sans-serif;">Subsector</td>
												<td width="4%" class='titulos' style="font: 100% sans-serif;">Tipo</td>
												<td width="5%" class='titulos' style="font: 100% sans-serif;">Supra regional</td>
												<td width="5%" class='titulos' style="font: 100% sans-serif;">Nivel territorial</td>
												<td width="5%" class='titulos' style="font: 100% sans-serif;">Depto</td>
												<td width="5%" class='titulos' style="font: 100% sans-serif;">Municipio</td>
												<td width="5%" class='titulos' style="font: 100% sans-serif;">Consecutivo</td>
												<td width="15%" class='titulos' style="font: 100% sans-serif; text-align:center; border-radius: 0px 5px 0px 0px;">C&oacute;digo CUIN</td>
											</tr>
										</thead>
									</table>
									<div style='margin: 0px 5px 5px; border-radius: 0 0 5px 5px; height: 200px; overflow: scroll; overflow-x: hidden; background: white;'>
										<table class='inicio inicio--no-shadow'>
											<tbody>
												<tr v-for="(clacuin,index) in codigoscuin" v-on:click="cargacodigocuinIngresos(clacuin[2],clacuin[3],clacuin[4],clacuin[13])" v-bind:class="index % 2 ? 'saludo1a' : 'saludo2'" style='text-rendering: optimizeLegibility; cursor: pointer !important; style=\"cursor: hand\"'>
													<td width="1%" style="font: 120% sans-serif;">{{ clacuin[1] }}</td>
													<td width="8%" style="font: 120% sans-serif;">{{ clacuin[2] }}</td>
													<td width="8%" style="font: 120% sans-serif;">{{ clacuin[3] }}</td>
													<td width="34%" style="font: 120% sans-serif;">{{ clacuin[4] }}</td>
													<td width="4%" style="font: 120% sans-serif;">{{ clacuin[5] }}</td>
													<td width="5%" style="font: 120% sans-serif;">{{ clacuin[6] }}</td>
													<td width="4%" style="font: 120% sans-serif;">{{ clacuin[7] }}</td>
													<td width="5%" style="font: 120% sans-serif;">{{ clacuin[8] }}</td>
													<td width="5%" style="font: 120% sans-serif;">{{ clacuin[9] }}</td>
													<td width="5%" style="font: 120% sans-serif;">{{ clacuin[10] }}</td>
													<td width="5%" style="font: 120% sans-serif;">{{ clacuin[11] }}</td>
													<td width="5%" style="font: 120% sans-serif;">{{ clacuin[12] }}</td>
													<td width="10%" style="font: 120% sans-serif;">{{ clacuin[13] }}</td>
												</tr>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</transition>
				</div>
				<div v-show="showModalIngresos4">
					<transition name="modal">
						<div class="modal-mask">
							<div class="modal-wrapper">
								<div class="modal-container3">
									<table class="inicio ancho">
										<tr>
											<td class="titulos" colspan="2" >SELECCIONAR POL&Iacute;TICA P&Uacute;BLICA</td>
											<td class="cerrar" style="width:7%" @click="showModalIngresos4 = false">Cerrar</td>
										</tr>
										<tr>
											<td class="tamano01" style="width:3cm">NOMBRE:</td>
											<td><input type="text" class="form-control" placeholder="Buscar por nombre de la Fuente" v-on:keyup="searchMonitorpp" v-model="searchpoliticaspublicas.keywordpp" style="width:100%" /></td>
										</tr>
									</table>
									<table>
										<thead>
											<tr>
												<td class='titulos' style="font: 160% sans-serif; border-radius: 5px 0px 0px 0px; width:4.9%;">Codigo</td>
												<td class='titulos' style="font: 160% sans-serif; width:10%;">Nombre</td>
											</tr>
										</thead>
									</table>
									<div style='margin: 0px 5px 5px; border-radius: 0 0 5px 5px; height: 200px; overflow: scroll; overflow-x: hidden; background: white;'>
										<table class='inicio inicio--no-shadow'>
											<tbody>
												<tr v-for="(resultpp,index) in resultspp" v-on:click="cargapoliticaIngresos(resultpp[1],resultpp[2])"  v-bind:class="index % 2 ? 'saludo1a' : 'saludo2'" style='text-rendering: optimizeLegibility; cursor: pointer !important; style=\"cursor: hand\"'>
													<td width="30%" style="font: 160% sans-serif;">{{ resultpp[1] }}</td>
													<td width="60%" style="font: 160% sans-serif;">{{ resultpp[2] }}</td>
												</tr>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</transition>
				</div>
				<div v-show="showModalIngresos5">
					<transition name="modal">
						<div class="modal-mask">
							<div class="modal-wrapper">
								<div class="modal-container">
									<table class="inicio ancho">
										<tr>
											<td class="titulos" colspan="2" >SELECCIONAR SECCION</td>
											<td class="cerrar" style="width:7%" @click="showModalIngresos5 = false">Cerrar</td>
										</tr>
										<tr>
											<td class="tamano01" style="width:3cm">NOMBRE:</td>
											<td><input type="text" class="form-control" placeholder="Buscar por nombre seccion" v-on:keyup="searchMonitorProducts" v-model="searchProduct.keywordProduct" style="width:100%" /></td>
										</tr>
									</table>
									<table>
										<thead>
											<tr>
												<td class='titulos' style="width:20%; font: 160% sans-serif; ">C&oacute;digo</td>
												<td class='titulos' style="font: 160% sans-serif; ">Nombre</td>
											</tr>
										</thead>
									</table>
									<div style='margin: 0px 5px 5px; border-radius: 0 0 5px 5px; height: 200px; overflow: scroll; overflow-x: hidden; background: white;'>
										<table class='inicio inicio--no-shadow'>
											<tbody>
												<tr v-for="(seccion,index) in secciones" v-on:click="cargaseccionIngresos(seccion[0],seccion[1])"  v-bind:class="index % 2 ? 'saludo1a' : 'saludo2'" style='text-rendering: optimizeLegibility; cursor: pointer !important; style=\"cursor: hand\"'>
													<td style="font: 120% sans-serif; padding-left:10px;width:20%;">{{ seccion[0] }}</td>
													<td style="font: 120% sans-serif; padding-left:10px">{{ seccion[1] }}</td>
												</tr>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</transition>
				</div>
				<div v-show="showModalIngresos6">
					<transition name="modal">
						<div class="modal-mask">
							<div class="modal-wrapper">
								<div class="modal-container">
									<table class="inicio ancho">
										<tr>
											<td class="titulos" colspan="2" >SELECCIONAR DIVISION</td>
											<td class="cerrar" style="width:7%" @click="showModalIngresos6 = false">Cerrar</td>
										</tr>
										<tr>
											<td class="tamano01" style="width:3cm">NOMBRE:</td>
											<td><input type="text" class="form-control" placeholder="Buscar por nombre de divisi�n" v-on:keyup="searchMonitorProducts" v-model="searchProduct.keywordProduct" style="width:100%" /></td>
										</tr>
									</table>
									<table>
										<thead>
											<tr>
												<td class='titulos' style="width:20%; font: 160% sans-serif; ">C&oacute;digo</td>
												<td class='titulos' style="font: 160% sans-serif; ">Nombre</td>
											</tr>
										</thead>
									</table>
									<div style='margin: 0px 5px 5px; border-radius: 0 0 5px 5px; height: 200px; overflow: scroll; overflow-x: hidden; background: white;'>
										<table class='inicio inicio--no-shadow'>
											<tbody>
												<tr v-for="(division,index) in divisiones" v-on:click="cargadivisionIngresos(division[0],division[1])"  v-bind:class="index % 2 ? 'saludo1a' : 'saludo2'" style='text-rendering: optimizeLegibility; cursor: pointer !important; style=\"cursor: hand\"'>
													<td style="font: 120% sans-serif; padding-left:10px; width:20%;">{{ division[0] }}</td>
													<td style="font: 120% sans-serif; padding-left:10px">{{ division[1] }}</td>
												</tr>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</transition>
				</div>
				<div v-show="showModalIngresos7">
					<transition name="modal">
						<div class="modal-mask">
							<div class="modal-wrapper">
								<div class="modal-container">
									<table class="inicio ancho">
										<tr>
											<td class="titulos" colspan="2" >SELECCIONAR GRUPO</td>
											<td class="cerrar" style="width:7%" @click="showModalIngresos7 = false">Cerrar</td>
										</tr>
										<tr>
											<td class="tamano01" style="width:3cm">NOMBRE:</td>
											<td><input type="text" class="form-control" placeholder="Buscar por nombre del grupo" v-on:keyup="searchMonitorProducts" v-model="searchProduct.keywordProduct" style="width:100%" /></td>
										</tr>
									</table>
									<table>
										<thead>
											<tr>
												<td class='titulos' style="width:20%; font: 160% sans-serif; ">C&oacute;digo</td>
												<td class='titulos' style="font: 160% sans-serif; ">Nombre</td>
											</tr>
										</thead>
									</table>
									<div style='margin: 0px 5px 5px; border-radius: 0 0 5px 5px; height: 200px; overflow: scroll; overflow-x: hidden; background: white;'>
										<table class='inicio inicio--no-shadow'>
											<tbody>
												<tr v-for="(grupo,index) in grupos" v-on:click="cargagrupoIngresos(grupo[0],grupo[1])"  v-bind:class="index % 2 ? 'saludo1a' : 'saludo2'" style='text-rendering: optimizeLegibility; cursor: pointer !important; style=\"cursor: hand\"'>
													<td style="font: 120% sans-serif; padding-left:10px;width:20%;">{{ grupo[0] }}</td>
													<td style="font: 120% sans-serif; padding-left:10px">{{ grupo[1] }}</td>
												</tr>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</transition>
				</div>
				<div v-show="showModalIngresos8">
					<transition name="modal">
						<div class="modal-mask">
							<div class="modal-wrapper">
								<div class="modal-container">
									<table class="inicio ancho">
										<tr>
											<td class="titulos" colspan="2" >SELECCIONAR CLASE</td>
											<td class="cerrar" style="width:7%" @click="showModalIngresos8 = false">Cerrar</td>
										</tr>
										<tr>
											<td class="tamano01" style="width:3cm">NOMBRE:</td>
											<td><input type="text" class="form-control" placeholder="Buscar por nombre de la clase" v-on:keyup="searchMonitorProducts" v-model="searchProduct.keywordProduct" style="width:100%" /></td>
										</tr>
									</table>
									<table>
										<thead>
											<tr>
												<td class='titulos' style="width:20%; font: 160% sans-serif; ">C&oacute;digo</td>
												<td class='titulos' style="font: 160% sans-serif; ">Nombre</td>
											</tr>
										</thead>
									</table>
									<div style='margin: 0px 5px 5px; border-radius: 0 0 5px 5px; height: 200px; overflow: scroll; overflow-x: hidden; background: white;'>
										<table class='inicio inicio--no-shadow'>
											<tbody>
												<tr v-for="(clase,index) in clases" v-on:click="cargaclaseIngresos(clase[0],clase[1])"  v-bind:class="index % 2 ? 'saludo1a' : 'saludo2'" style='text-rendering: optimizeLegibility; cursor: pointer !important; style=\"cursor: hand\"'>
													<td style="font: 120% sans-serif; padding-left:10px; width:20%;">{{ clase[0] }}</td>
													<td style="font: 120% sans-serif; padding-left:10px">{{ clase[1] }}</td>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</transition>
				</div>
				<div v-show="showModalIngresos9">
					<transition name="modal">
						<div class="modal-mask">
							<div class="modal-wrapper">
								<div class="modal-container">
									<table class="inicio ancho">
										<tr>
											<td class="titulos" colspan="2" >SELECCIONAR SUBCLASE</td>
											<td class="cerrar" style="width:7%" @click="showModalIngresos9 = false">Cerrar</td>
										</tr>
										<tr>
											<td class="tamano01" style="width:3cm">NOMBRE:</td>
											<td><input type="text" class="form-control" placeholder="Buscar por nombre de la subclase" v-on:keyup="buscarsubclases" v-model="searchsubclase.keywordsubclase" style="width:100%" /></td>
										</tr>
									</table>
									<table>
										<thead>
											<tr>
												<td class='titulos' style="font: 160% sans-serif; padding-left:10px; border-radius: 5px 0 0 0; width:20%;">C&oacute;digo</td>
												<td class='titulos' style="font: 160% sans-serif; ">Nombre</td>
												<td class='titulos' style="font: 160% sans-serif; width:15%;">CIIU Rev. 4 A.C. </td>
												<td class='titulos' style="font: 160% sans-serif; width:25%;">Sistema Armonizado 2012</td>
												<td class='titulos' style="font: 160% sans-serif; width:10%;">CPC 2 A.C.</td>
											</tr>
										</thead>
									</table>
									<div style='margin: 0px 5px 5px; border-radius: 0 0 5px 5px; height: 200px; overflow: scroll; overflow-x: hidden; background: white;'>
										<table class='inicio inicio--no-shadow'>
											<tbody>
												<tr v-for="(subclase,index) in subClases" v-bind:class="index % 2 ? 'saludo1a' : 'saludo2'" v-on:click="cargasubclaseIngresos(subclase[0],subclase[1])"  style='text-rendering: optimizeLegibility; cursor: pointer !important; style=\"cursor: hand\"'>
													<td width="20%" style="font: 120% sans-serif; padding-left:10px">{{ subclase[0] }}</td>
													<td width="30%" style="font: 120% sans-serif; padding-left:10px">{{ subclase[1] }}</td>
													<td width="15%" style="font: 120% sans-serif; padding-left:10px">{{ subclase[2] }}</td>
													<td width="25%" style="font: 120% sans-serif; padding-left:10px">{{ subclase[3] }}</td>
													<td width="10%" style="font: 120% sans-serif; padding-left:10px">{{ subclase[4] }}</td>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</transition>
				</div>
				<div v-show="showModalIngresos10">
					<transition name="modal">
						<div class="modal-mask">
							<div class="modal-wrapper">
								<div class="modal-container2">
									<table class="inicio ancho">
										<tr>
											<td class="titulos" colspan="2" >SELECCIONAR PRODUCTO</td>
											<td class="cerrar" style="width:7%" @click="showModalIngresos10 = false">Cerrar</td>
										</tr>
										<tr>
											<td class="tamano01" style="width:3cm">Producto:</td>
											<td><input type="text" class="form-control" placeholder="Buscar por nombre del producto" v-on:keyup="buscarsubproductos" v-model="searchsubproductos.keywordsubproductos" style="width:100%" /></td>
										</tr>
									</table>
									<table>
										<thead>
											<tr>
												<td width="10%" class='titulos' style="font: 160% sans-serif; border-radius: 5px 0 0 0;">C&oacute;digo</td>
												<td width="30%" class='titulos' style="font: 160% sans-serif;">Nombre programa</td>
												<td width="20%" class='titulos' style="font: 160% sans-serif;">Ciiu</td>
												<td width="20%" class='titulos' style="font: 160% sans-serif;">Sistema Armonizado</td>
												<td width="20%" class='titulos' style="font: 160% sans-serif; border-radius: 0 5px 0 0;">CPC</td>
											</tr>
										</thead>
									</table>
									<div style='margin: 0px 5px 5px; border-radius: 0 0 5px 5px; height: 200px; overflow: scroll; overflow-x: hidden; background: white;'>
										<table class='inicio inicio--no-shadow'>
											<tbody>
												<tr v-for="(subprod,index) in subproductos" v-on:click="cargasubproductoIngresos(subprod[0],subprod[1])" v-bind:class="index % 2 ? 'saludo1a' : 'saludo2'" style='text-rendering: optimizeLegibility; cursor: pointer !important; style=\"cursor: hand\"'>
													<td width="10%" style="font: 120% sans-serif; padding-left:10px">{{ subprod[0] }}</td>
													<td width="30%" style="font: 120% sans-serif; padding-left:10px">{{ subprod[1] }}</td>
													<td width="20%" style="font: 120% sans-serif; padding-left:10px">{{ subprod[2] }}</td>
													<td width="20%" style="font: 120% sans-serif; padding-left:10px">{{ subprod[3] }}</td>
													<td width="20%" style="font: 120% sans-serif; padding-left:10px">{{ subprod[4] }}</td>
												</tr>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</transition>
				</div>
				<div v-show="showModalFuncionamiento1">
					<transition name="modal">
						<div class="modal-mask">
							<div class="modal-wrapper">
								<div class="modal-container3">
									<table class="inicio ancho">
										<tr>
											<td class="titulos" colspan="2" >SELECCIONAR FUENTE</td>
											<td class="cerrar" style="width:7%" @click="showModalFuncionamiento1 = false">Cerrar</td>
										</tr>
										<tr>
											<td class="tamano01" style="width:3cm">NOMBRE:</td>
											<td><input type="text" class="form-control" placeholder="Buscar por nombre de la Fuente" v-on:keyup="searchMonitorfuentes" v-model="searchfuentes.keyword" style="width:100%" /></td>
										</tr>
									</table>
									<table>
										<thead>
											<tr>
												<td class='titulos' style="font: 160% sans-serif; border-radius: 5px 0px 0px 0px; width:4.9%;">Id fuente</td>
												<td class='titulos' style="font: 160% sans-serif; width:10%;">Entidad financiadora</td>
											</tr>
										</thead>
									</table>
									<div style='margin: 0px 5px 5px; border-radius: 0 0 5px 5px; height: 200px; overflow: scroll; overflow-x: hidden; background: white;'>
										<table class='inicio inicio--no-shadow'>
											<tbody>
												<tr v-for="(result,index) in results" v-on:click="cargaFuenteFuncionamiento(result[1],result[2])"  v-bind:class="index % 2 ? 'saludo1a' : 'saludo2'" style='text-rendering: optimizeLegibility; cursor: pointer !important; style=\"cursor: hand\"'>
													<td width="30%" style="font: 160% sans-serif;">{{ result[1] }}</td>
													<td width="60%" style="font: 160% sans-serif;">{{ result[2] }}</td>
												</tr>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</transition>
				</div>
				<div v-show="showModalFuncionamiento2">
					<transition name="modal">
						<div class="modal-mask">
							<div class="modal-wrapper">
								<div class="modal-container">
									<table class="inicio ancho">
										<tr>
											<td class="titulos" colspan="2" >SELECCIONAR CUENTAS PRESUPUESTALES</td>
											<td class="cerrar" style="width:7%" @click="showModalFuncionamiento2 = false">Cerrar</td>
										</tr>
										<tr>
											<td class="tamano01" style="width:3cm">Descripci&oacute;n:</td>
											<td><input type="text" class="form-control" placeholder="Buscar por descripcion cuenta" v-on:keyup="searchMonitorCuentasPresupuestales" v-model="searchCuentaPresupuestal.keywordCuentaPresupuestal" style="width:100%" /></td>
										</tr>
									</table>
									<table>
										<thead>
											<tr>

												<td class='titulos' style="width:30%; font: 160% sans-serif; ">C&oacute;digo</td>
												<td class='titulos' style="font: 160% sans-serif; ">Nombre</td>
												<td class='titulos' style="width:10%; font: 160% sans-serif; ">Tipo</td>

											</tr>
										</thead>
									</table>
									<div style='margin: 0px 5px 5px; border-radius: 0 0 5px 5px; height: 200px; overflow: scroll; overflow-x: hidden; background: white;'>
										<table class='inicio inicio--no-shadow'>
											<tbody>
												<tr v-for="(cuentapre,index) in cuentasFuncionamiento" v-on:click="cargaCuentaFuncionamiento(cuentapre[0],cuentapre[1],cuentapre[2])"  v-bind:class="index % 2 ? 'saludo1a' : 'saludo2'" style='text-rendering: optimizeLegibility; cursor: pointer !important; style=\"cursor: hand\"'>
													<td style="font: 120% sans-serif; padding-left:10px; width: 30%;">{{ cuentapre[0] }}</td>
													<td style="font: 120% sans-serif; padding-left:10px">{{ cuentapre[1] }}</td>
													<td  style="width: 8%; font: 120% sans-serif; padding-left:10px">{{ cuentapre[2] }}</td>
												</tr>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</transition>
				</div>
				<div v-show="showModalFuncionamiento3">
					<transition name="modal">
						<div class="modal-mask">
							<div class="modal-wrapper">
								<div class="modal-container3">
									<table class="inicio ancho">
										<tr>
											<td class="titulos" colspan="2" >SELECCIONAR POL&Iacute;TICA P&Uacute;BLICA</td>
											<td class="cerrar" style="width:7%" @click="showModalFuncionamiento3 = false">Cerrar</td>
										</tr>
										<tr>
											<td class="tamano01" style="width:3cm">NOMBRE:</td>
											<td><input type="text" class="form-control" placeholder="Buscar por nombre de la Fuente" v-on:keyup="searchMonitorpp" v-model="searchpoliticaspublicas.keywordpp" style="width:100%" /></td>
										</tr>
									</table>
									<table>
										<thead>
											<tr>
												<td class='titulos' style="font: 160% sans-serif; border-radius: 5px 0px 0px 0px; width:4.9%;">Codigo</td>
												<td class='titulos' style="font: 160% sans-serif; width:10%;">Nombre</td>
											</tr>
										</thead>
									</table>
									<div style='margin: 0px 5px 5px; border-radius: 0 0 5px 5px; height: 200px; overflow: scroll; overflow-x: hidden; background: white;'>
										<table class='inicio inicio--no-shadow'>
											<tbody>
												<tr v-for="(resultpp,index) in resultspp" v-on:click="cargapoliticaFuncionamiento(resultpp[1],resultpp[2])"  v-bind:class="index % 2 ? 'saludo1a' : 'saludo2'" style='text-rendering: optimizeLegibility; cursor: pointer !important; style=\"cursor: hand\"'>
													<td width="30%" style="font: 160% sans-serif;">{{ resultpp[1] }}</td>
													<td width="60%" style="font: 160% sans-serif;">{{ resultpp[2] }}</td>
												</tr>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</transition>
				</div>
				<div v-show="showModalFuncionamiento4">
					<transition name="modal">
						<div class="modal-mask">
							<div class="modal-wrapper">
								<div class="modal-container3">
									<table class="inicio ancho">
										<tr>
											<td class="titulos" colspan="2" >SELECCIONAR FUENTE</td>
											<td class="cerrar" style="width:7%" @click="showModalFuncionamiento4 = false">Cerrar</td>
										</tr>
										<tr>
											<td class="tamano01" style="width:3cm">NOMBRE:</td>
											<td><input type="text" class="form-control" placeholder="Buscar por nombre de la Fuente" v-on:keyup="searchMonitorProducts" v-model="searchProduct.keywordProduct" style="width:100%" /></td>
										</tr>
									</table>
									<table>
										<thead>
											<tr>
												<td width="1%" class='titulos' style="font: 100% sans-serif; border-radius: 5px 0px 0px;">No</td>
												<td width="8%" class='titulos' style="font: 100% sans-serif;">Id entidad</td>
												<td width="8%" class='titulos' style="font: 100% sans-serif;">Nit</td>
												<td width="34%" class='titulos' style="font: 100% sans-serif;">Nombre</td>
												<td width="4%" class='titulos' style="font: 100% sans-serif;">Sector</td>
												<td width="5%" class='titulos' style="font: 100% sans-serif;">Subsector</td>
												<td width="4%" class='titulos' style="font: 100% sans-serif;">Tipo</td>
												<td width="5%" class='titulos' style="font: 100% sans-serif;">Supra regional</td>
												<td width="5%" class='titulos' style="font: 100% sans-serif;">Nivel territorial</td>
												<td width="5%" class='titulos' style="font: 100% sans-serif;">Depto</td>
												<td width="5%" class='titulos' style="font: 100% sans-serif;">Municipio</td>
												<td width="5%" class='titulos' style="font: 100% sans-serif;">Consecutivo</td>
												<td width="15%" class='titulos' style="font: 100% sans-serif; text-align:center; border-radius: 0px 5px 0px 0px;">C&oacute;digo CUIN</td>
											</tr>
										</thead>
									</table>
									<div style='margin: 0px 5px 5px; border-radius: 0 0 5px 5px; height: 200px; overflow: scroll; overflow-x: hidden; background: white;'>
										<table class='inicio inicio--no-shadow'>
											<tbody>
												<tr v-for="(clacuin,index) in codigoscuin" v-on:click="cargacodigocuinFuncionamiento(clacuin[2],clacuin[3],clacuin[4],clacuin[13])" v-bind:class="index % 2 ? 'saludo1a' : 'saludo2'" style='text-rendering: optimizeLegibility; cursor: pointer !important; style=\"cursor: hand\"'>
													<td width="1%" style="font: 120% sans-serif;">{{ clacuin[1] }}</td>
													<td width="8%" style="font: 120% sans-serif;">{{ clacuin[2] }}</td>
													<td width="8%" style="font: 120% sans-serif;">{{ clacuin[3] }}</td>
													<td width="34%" style="font: 120% sans-serif;">{{ clacuin[4] }}</td>
													<td width="4%" style="font: 120% sans-serif;">{{ clacuin[5] }}</td>
													<td width="5%" style="font: 120% sans-serif;">{{ clacuin[6] }}</td>
													<td width="4%" style="font: 120% sans-serif;">{{ clacuin[7] }}</td>
													<td width="5%" style="font: 120% sans-serif;">{{ clacuin[8] }}</td>
													<td width="5%" style="font: 120% sans-serif;">{{ clacuin[9] }}</td>
													<td width="5%" style="font: 120% sans-serif;">{{ clacuin[10] }}</td>
													<td width="5%" style="font: 120% sans-serif;">{{ clacuin[11] }}</td>
													<td width="5%" style="font: 120% sans-serif;">{{ clacuin[12] }}</td>
													<td width="10%" style="font: 120% sans-serif;">{{ clacuin[13] }}</td>
												</tr>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</transition>
				</div>
				<div v-show="showModalFuncionamiento5">
					<transition name="modal">
						<div class="modal-mask">
							<div class="modal-wrapper">
								<div class="modal-container">
									<table class="inicio ancho">
										<tr>
											<td class="titulos" colspan="2" >SELECCIONAR SECCION</td>
											<td class="cerrar" style="width:7%" @click="showModalFuncionamiento5 = false">Cerrar</td>
										</tr>
										<tr>
											<td class="tamano01" style="width:3cm">NOMBRE:</td>
											<td><input type="text" class="form-control" placeholder="Buscar por nombre seccion" v-on:keyup="searchMonitorProducts" v-model="searchProduct.keywordProduct" style="width:100%" /></td>
										</tr>
									</table>
									<table>
										<thead>
											<tr>
												<td class='titulos' style="width:20%; font: 160% sans-serif; ">C&oacute;digo</td>
												<td class='titulos' style="font: 160% sans-serif; ">Nombre</td>
											</tr>
										</thead>
									</table>
									<div style='margin: 0px 5px 5px; border-radius: 0 0 5px 5px; height: 200px; overflow: scroll; overflow-x: hidden; background: white;'>
										<table class='inicio inicio--no-shadow'>
											<tbody>
												<tr v-for="(seccion,index) in secciones" v-on:click="cargaseccionFuncionamiento(seccion[0],seccion[1])"  v-bind:class="index % 2 ? 'saludo1a' : 'saludo2'" style='text-rendering: optimizeLegibility; cursor: pointer !important; style=\"cursor: hand\"'>
													<td style="font: 120% sans-serif; padding-left:10px;width:20%;">{{ seccion[0] }}</td>
													<td style="font: 120% sans-serif; padding-left:10px">{{ seccion[1] }}</td>
												</tr>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</transition>
				</div>
				<div v-show="showModalFuncionamiento6">
					<transition name="modal">
						<div class="modal-mask">
							<div class="modal-wrapper">
								<div class="modal-container">
									<table class="inicio ancho">
										<tr>
											<td class="titulos" colspan="2" >SELECCIONAR DIVISION</td>
											<td class="cerrar" style="width:7%" @click="showModalFuncionamiento6 = false">Cerrar</td>
										</tr>
										<tr>
											<td class="tamano01" style="width:3cm">NOMBRE:</td>
											<td><input type="text" class="form-control" placeholder="Buscar por nombre de divisi�n" v-on:keyup="searchMonitorProducts" v-model="searchProduct.keywordProduct" style="width:100%" /></td>
										</tr>
									</table>
									<table>
										<thead>
											<tr>
												<td class='titulos' style="width:20%; font: 160% sans-serif; ">C&oacute;digo</td>
												<td class='titulos' style="font: 160% sans-serif; ">Nombre</td>
											</tr>
										</thead>
									</table>
									<div style='margin: 0px 5px 5px; border-radius: 0 0 5px 5px; height: 200px; overflow: scroll; overflow-x: hidden; background: white;'>
										<table class='inicio inicio--no-shadow'>
											<tbody>
												<tr v-for="(division,index) in divisiones" v-on:click="cargadivisionFuncionamiento(division[0],division[1])"  v-bind:class="index % 2 ? 'saludo1a' : 'saludo2'" style='text-rendering: optimizeLegibility; cursor: pointer !important; style=\"cursor: hand\"'>
													<td style="font: 120% sans-serif; padding-left:10px; width:20%;">{{ division[0] }}</td>
													<td style="font: 120% sans-serif; padding-left:10px">{{ division[1] }}</td>
												</tr>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</transition>
				</div>
				<div v-show="showModalFuncionamiento7">
					<transition name="modal">
						<div class="modal-mask">
							<div class="modal-wrapper">
								<div class="modal-container">
									<table class="inicio ancho">
										<tr>
											<td class="titulos" colspan="2" >SELECCIONAR GRUPO</td>
											<td class="cerrar" style="width:7%" @click="showModalFuncionamiento7 = false">Cerrar</td>
										</tr>
										<tr>
											<td class="tamano01" style="width:3cm">NOMBRE:</td>
											<td><input type="text" class="form-control" placeholder="Buscar por nombre del grupo" v-on:keyup="searchMonitorProducts" v-model="searchProduct.keywordProduct" style="width:100%" /></td>
										</tr>
									</table>
									<table>
										<thead>
											<tr>
												<td class='titulos' style="width:20%; font: 160% sans-serif; ">C&oacute;digo</td>
												<td class='titulos' style="font: 160% sans-serif; ">Nombre</td>
											</tr>
										</thead>
									</table>
									<div style='margin: 0px 5px 5px; border-radius: 0 0 5px 5px; height: 200px; overflow: scroll; overflow-x: hidden; background: white;'>
										<table class='inicio inicio--no-shadow'>
											<tbody>
												<tr v-for="(grupo,index) in grupos" v-on:click="cargagrupoFuncionamiento(grupo[0],grupo[1])"  v-bind:class="index % 2 ? 'saludo1a' : 'saludo2'" style='text-rendering: optimizeLegibility; cursor: pointer !important; style=\"cursor: hand\"'>
													<td style="font: 120% sans-serif; padding-left:10px;width:20%;">{{ grupo[0] }}</td>
													<td style="font: 120% sans-serif; padding-left:10px">{{ grupo[1] }}</td>
												</tr>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</transition>
				</div>
				<div v-show="showModalFuncionamiento8">
					<transition name="modal">
						<div class="modal-mask">
							<div class="modal-wrapper">
								<div class="modal-container">
									<table class="inicio ancho">
										<tr>
											<td class="titulos" colspan="2" >SELECCIONAR CLASE</td>
											<td class="cerrar" style="width:7%" @click="showModalFuncionamiento8 = false">Cerrar</td>
										</tr>
										<tr>
											<td class="tamano01" style="width:3cm">NOMBRE:</td>
											<td><input type="text" class="form-control" placeholder="Buscar por nombre de la clase" v-on:keyup="searchMonitorProducts" v-model="searchProduct.keywordProduct" style="width:100%" /></td>
										</tr>
									</table>
									<table>
										<thead>
											<tr>
												<td class='titulos' style="width:20%; font: 160% sans-serif; ">C&oacute;digo</td>
												<td class='titulos' style="font: 160% sans-serif; ">Nombre</td>
											</tr>
										</thead>
									</table>
									<div style='margin: 0px 5px 5px; border-radius: 0 0 5px 5px; height: 200px; overflow: scroll; overflow-x: hidden; background: white;'>
										<table class='inicio inicio--no-shadow'>
											<tbody>
												<tr v-for="(clase,index) in clases" v-on:click="cargaclaseFuncionamiento(clase[0],clase[1])"  v-bind:class="index % 2 ? 'saludo1a' : 'saludo2'" style='text-rendering: optimizeLegibility; cursor: pointer !important; style=\"cursor: hand\"'>
													<td style="font: 120% sans-serif; padding-left:10px; width:20%;">{{ clase[0] }}</td>
													<td style="font: 120% sans-serif; padding-left:10px">{{ clase[1] }}</td>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</transition>
				</div>
				<div v-show="showModalFuncionamiento9">
					<transition name="modal">
						<div class="modal-mask">
							<div class="modal-wrapper">
								<div class="modal-container">
									<table class="inicio ancho">
										<tr>
											<td class="titulos" colspan="2" >SELECCIONAR SUBCLASE</td>
											<td class="cerrar" style="width:7%" @click="showModalFuncionamiento9 = false">Cerrar</td>
										</tr>
										<tr>
											<td class="tamano01" style="width:3cm">NOMBRE:</td>
											<td><input type="text" class="form-control" placeholder="Buscar por nombre de la subclase" v-on:keyup="buscarsubclases" v-model="searchsubclase.keywordsubclase" style="width:100%" /></td>
										</tr>
									</table>
									<table>
										<thead>
											<tr>
												<td class='titulos' style="font: 160% sans-serif; padding-left:10px; border-radius: 5px 0 0 0; width:20%;">C&oacute;digo</td>
												<td class='titulos' style="font: 160% sans-serif; ">Nombre</td>
												<td class='titulos' style="font: 160% sans-serif; width:15%;">CIIU Rev. 4 A.C. </td>
												<td class='titulos' style="font: 160% sans-serif; width:25%;">Sistema Armonizado 2012</td>
												<td class='titulos' style="font: 160% sans-serif; width:10%;">CPC 2 A.C.</td>
											</tr>
										</thead>
									</table>
									<div style='margin: 0px 5px 5px; border-radius: 0 0 5px 5px; height: 200px; overflow: scroll; overflow-x: hidden; background: white;'>
										<table class='inicio inicio--no-shadow'>
											<tbody>
												<tr v-for="(subclase,index) in subClases" v-bind:class="index % 2 ? 'saludo1a' : 'saludo2'" v-on:click="cargasubclaseFuncionamiento(subclase[0],subclase[1])"  style='text-rendering: optimizeLegibility; cursor: pointer !important; style=\"cursor: hand\"'>
													<td width="20%" style="font: 120% sans-serif; padding-left:10px">{{ subclase[0] }}</td>
													<td width="30%" style="font: 120% sans-serif; padding-left:10px">{{ subclase[1] }}</td>
													<td width="15%" style="font: 120% sans-serif; padding-left:10px">{{ subclase[2] }}</td>
													<td width="25%" style="font: 120% sans-serif; padding-left:10px">{{ subclase[3] }}</td>
													<td width="10%" style="font: 120% sans-serif; padding-left:10px">{{ subclase[4] }}</td>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</transition>
				</div>
				<div v-show="showModalFuncionamiento10">
					<transition name="modal">
						<div class="modal-mask">
							<div class="modal-wrapper">
								<div class="modal-container2">
									<table class="inicio ancho">
										<tr>
											<td class="titulos" colspan="2" >SELECCIONAR PRODUCTO</td>
											<td class="cerrar" style="width:7%" @click="showModalFuncionamiento10 = false">Cerrar</td>
										</tr>
										<tr>
											<td class="tamano01" style="width:3cm">Producto:</td>
											<td><input type="text" class="form-control" placeholder="Buscar por nombre del producto" v-on:keyup="buscarsubproductos" v-model="searchsubproductos.keywordsubproductos" style="width:100%" /></td>
										</tr>
									</table>
									<table>
										<thead>
											<tr>
												<td width="10%" class='titulos' style="font: 160% sans-serif; border-radius: 5px 0 0 0;">C&oacute;digo</td>
												<td width="30%" class='titulos' style="font: 160% sans-serif;">Nombre programa</td>
												<td width="20%" class='titulos' style="font: 160% sans-serif;">Ciiu</td>
												<td width="20%" class='titulos' style="font: 160% sans-serif;">Sistema Armonizado</td>
												<td width="20%" class='titulos' style="font: 160% sans-serif; border-radius: 0 5px 0 0;">CPC</td>
											</tr>
										</thead>
									</table>
									<div style='margin: 0px 5px 5px; border-radius: 0 0 5px 5px; height: 200px; overflow: scroll; overflow-x: hidden; background: white;'>
										<table class='inicio inicio--no-shadow'>
											<tbody>
												<tr v-for="(subprod,index) in subproductos" v-on:click="cargasubproductoFuncionamiento(subprod[0],subprod[1])" v-bind:class="index % 2 ? 'saludo1a' : 'saludo2'" style='text-rendering: optimizeLegibility; cursor: pointer !important; style=\"cursor: hand\"'>
													<td width="10%" style="font: 120% sans-serif; padding-left:10px">{{ subprod[0] }}</td>
													<td width="30%" style="font: 120% sans-serif; padding-left:10px">{{ subprod[1] }}</td>
													<td width="20%" style="font: 120% sans-serif; padding-left:10px">{{ subprod[2] }}</td>
													<td width="20%" style="font: 120% sans-serif; padding-left:10px">{{ subprod[3] }}</td>
													<td width="20%" style="font: 120% sans-serif; padding-left:10px">{{ subprod[4] }}</td>
												</tr>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</transition>
				</div>
				<input type="hidden" v-model="acuerdo" :value="acuerdo=<?php echo $_GET['is']; ?>"/>
			</div>
		</div>
		<script src="Librerias/vue/vue.min.js"></script>
		<script src="Librerias/vue/axios.min.js"></script>
		<script src="vue/presupuesto_ccp/ccp-editaReduccion.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
	</body>
</html>