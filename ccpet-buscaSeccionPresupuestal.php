<?php
    header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require "comun.inc";
	require "funciones.inc";
	session_start();
	$linkbd = conectar_v7();
    $linkbd -> set_charset("utf8");
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>:: IDEAL 10 - Parametrización</title>
    <link href="css/css2n.css" rel="stylesheet" type="text/css"/>
    <link href="css/css3n.css" rel="stylesheet" type="text/css"/>
    <link href="css/css4.css" rel="stylesheet" type="text/css"/>
    <link href="css/cssSP.css" rel="stylesheet" type="text/css"/>
    <script type="text/javascript" src="css/programas.js"></script>
    <script type="text/javascript" src="jquery-1.11.0.min.js"></script>
    <script type="text/javascript" src="css/calendario.js"></script>
    <link href="css/style.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
		<script>
			function verUltimaPos(idcta, filas, filtro)
			{
				var scrtop = $('#divdet').scrollTop();
				var altura = $('#divdet').height();
				var numpag = $('#nummul').val();
				var limreg = $('#numres').val();

				if((numpag <= 0) || (numpag == ""))
				{
					numpag = 0;
				}

				if((limreg == 0) || (limreg == ""))
				{
					limreg = 10;
				}

				numpag++;

				location.href="ccpet-editaSeccionPresupuestal.php?idban="+idcta+"&scrtop="+scrtop+"&totreg="+filas+"&altura="+altura+"&numpag="+numpag+"&limreg="+limreg+"&filtro="+filtro;
			}

			function despliegamodalm(_valor,_tip,mensa,pregunta)
			{
				document.getElementById("bgventanamodalm").style.visibility=_valor;
				if(_valor == "hidden")
				{
					document.getElementById('ventanam').src="";
				}
				else
				{
					switch(_tip)
					{
						case "1":	document.getElementById('ventanam').src="ventana-mensaje1.php?titulos="+mensa;break;
						case "2":	document.getElementById('ventanam').src="ventana-mensaje3.php?titulos="+mensa;break;
						case "3":	document.getElementById('ventanam').src="ventana-mensaje2.php?titulos="+mensa;break;
						case "4":	document.getElementById('ventanam').src="ventana-consulta2.php?titulos="+mensa+"&idresp="+pregunta;break;
					}
				}
			}

			function funcionmensaje(){}
        </script>

        <?php

        titlepag();
        $scrtop = @ $_GET['scrtop'];
        if($scrtop == "") $scrtop=0;
        echo"<script>
                window.onload=function()
                {
                    $('#divdet').scrollTop(".$scrtop.")
                }
            </script>";

        $gidcta = @ $_GET['idcta'];
        if(isset($_GET['filtro']))
        $_POST['nombre'] = $_GET['filtro'];
        ?>
</head>
<body>
    <IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
    <span id="todastablas2"></span>
    <table>
        <tr><script>barra_imagenes("para");</script><?php cuadro_titulos();?></tr>
        <tr><?php menu_desplegable("para");?></tr>
        <tr>
            <td colspan="3" class="cinta">
                <a href="ccpet-crearSeccionPresupuestal.php" class="mgbt"><img src="imagenes/add.png"/></a>

                <a class="mgbt"><img src="imagenes/guardad.png" title="Guardar"/></a>

                <a href="" class="mgbt"><img src="imagenes/busca.png" title="Buscar"/></a>

                <a onclick="mypop=window.open('plan-agenda.php','','');mypop.focus()" class="mgbt"><img src="imagenes/agenda1.png" title="Agenda" /></a>

                <a onclick="mypop=window.open('para-principal.php','','');mypop.focus();" class="mgbt"><img src="imagenes/nv.png" title="Nueva ventana"></a>

                <a onclick="mypop=window.open('<?php echo $url2; ?>','','');mypop.focus();" class="mgbt"><img src="imagenes/duplicar_pantalla.png" title="Duplicar pesta&ntilde;a"></a>
            </td>
        </tr>
    </table>

    <?php
        if(@ $_POST['oculto'] == "")
        {
            $_POST['cambioestado'] = "";
            $_POST['nocambioestado'] = "";
            $_POST['numres'] = 10;
            $_POST['numpos'] = 0;
            $_POST['nummul'] = 0;
        }

        if(@ $_GET['numpag'] != "")
        {
            if(@ $_POST['oculto'] != 2)
            {
                $_POST['numres'] = $_GET['limreg'];
                $_POST['numpos'] = $_GET['limreg'] * ($_GET['numpag']-1);
                $_POST['nummul'] = $_GET['numpag'] - 1;
            }
        }
        else
        {
            if(@ $_POST['nummul'] == "")
            {
                $_POST['numres'] = 10;
                $_POST['numpos'] = 0;
                $_POST['nummul'] = 0;
            }
        }
    ?>
    <div id="bgventanamodalm" class="bgventanamodalm">
        <div id="ventanamodalm" class="ventanamodalm">
            <IFRAME src="" name="ventanam" marginWidth=0 marginHeight=0 frameBorder=0 id="ventanam" frameSpacing=0 style=" width:700px; height:130px; top:200; overflow:hidden;"></IFRAME>
        </div>
    </div>

    <form name="form2" method="post" action="">

        <input type="hidden" name="numres" id="numres" value="<?php echo @ $_POST['numres'];?>"/>
        <input type="hidden" name="numpos" id="numpos" value="<?php echo @ $_POST['numpos'];?>"/>
        <input type="hidden" name="nummul" id="nummul" value="<?php echo @ $_POST['nummul'];?>"/>
        <input type="hidden" name="oculto" id="oculto" value="1">
        <input type="hidden" name="cambioestado" id="cambioestado" value="<?php echo @ $_POST['cambioestado'];?>">
        <input type="hidden" name="nocambioestado" id="nocambioestado" value="<?php echo @ $_POST['nocambioestado'];?>">
        <input type="hidden" name="idestado" id="idestado" value="<?php echo @ $_POST['idestado'];?>">

        <table class="inicio">
            <tr>
                <td class="titulos" colspan="4">:: Buscar Secci&oacute;n Presupuestal</td>

                <td class="cerrar" style="width:7%" onClick="location.href='ccpet-principal.php'">Cerrar</td>
            </tr>

            <tr>
                <td class="tamano01" style='width:4cm;'>:: C&oacute;digo o Nombre:</td>

                <td>
                    <input type="search" name="nombre" id="nombre" value="<?php echo @ $_POST['nombre'];?>" style='width:100%;height:30px;'/>
                </td>

                <td style="padding-bottom:0px"><em class="botonflecha" onClick="limbusquedas();">Buscar</em></td>
            </tr>
        </table>

        <div class="subpantallac5" style="height:69%; width:99.6%; margin-top:0px; overflow-x:hidden" id="divdet">

        <?php
            if (@ $_POST['nombre'] != "")
            {
                $crit1 = "WHERE concat_ws(' ',SP.id_seccion_presupuestal,SP.nombre) LIKE '%".$_POST['nombre']."%'";
            }
            else
            {
                $crit1 = '';
            }

            $sqlr = "SELECT SP.id_seccion_presupuestal, SP.nombre, UJ.nombre, SP.estado FROM pptoseccion_presupuestal AS SP INNER JOIN pptouniejecu AS UJ ON SP.id_unidad_ejecutora = UJ.id_cc $crit1 ORDER BY SP.id_seccion_presupuestal";

            $resp = mysqli_query($linkbd,$sqlr);

            $_POST['numtop'] = mysqli_num_rows($resp);

            $nuncilumnas = ceil($_POST['numtop'] / $_POST['numres']);

            $cond2 = "";

            if (@ $_POST['numres'] != "-1")
            {
                $cond2 = " LIMIT ".$_POST['numpos'].", ".$_POST['numres'];
            }

            $sqlr = "SELECT SP.id_seccion_presupuestal, SP.nombre, UJ.nombre, SP.estado FROM pptoseccion_presupuestal AS SP INNER JOIN pptouniejecu AS UJ ON SP.id_unidad_ejecutora = UJ.id_cc $crit1 ORDER BY SP.id_seccion_presupuestal $cond2";
            $resp = mysqli_query($linkbd,$sqlr);

            $con = 1;

            $numcontrol = $_POST['nummul'] + 1;

            if(($nuncilumnas == $numcontrol) || (@ $_POST['numres'] == "-1"))
            {
                $imagenforward = "<img src='imagenes/forward02.png' style='width:17px;cursor:default;'>";
                $imagensforward = "<img src='imagenes/skip_forward02.png' style='width:16px;cursor:default;' >";
            }
            else
            {
                $imagenforward = "<img src='imagenes/forward01.png' style='width:17px;cursor:pointer;' title='Siguiente' onClick='numsiguiente()'>";
                $imagensforward = "<img src='imagenes/skip_forward01.png' style='width:16px;cursor:pointer;' title='Fin' onClick='saltocol(\"$nuncilumnas\")'>";
            }
            if((@$_POST['numpos'] == 0) || (@ $_POST['numres'] == "-1"))
            {
                $imagenback = "<img src='imagenes/back02.png' style='width:17px;cursor:default;'>";
                $imagensback = "<img src='imagenes/skip_back02.png' style='width:16px;cursor:default;'>";
            }
            else
            {
                $imagenback = "<img src='imagenes/back01.png' style='width:17px;cursor:pointer;' title='Anterior' onClick='numanterior();'>";
                $imagensback = "<img src='imagenes/skip_back01.png' style='width:16px;cursor:pointer;' title='Inicio' onClick='saltocol(\"1\")'>";
            }

            $con = 1;
            echo "
                <table class='inicio'>
                    <tr>
                        <td colspan='3' class='titulos'>.: Resultados Busqueda:</td>
                        <td class='submenu'>
                            <select name='renumres' id='renumres' onChange='cambionum();' class='centrarSelect' style='width:100%'>
                                <option class='aumentarTamaño' value='10'"; if (@ $_POST['renumres']=='10'){echo 'selected';} echo ">10</option>
                                <option class='aumentarTamaño' value='20'"; if (@ $_POST['renumres']=='20'){echo 'selected';} echo ">20</option>
                                <option class='aumentarTamaño' value='30'"; if (@ $_POST['renumres']=='30'){echo 'selected';} echo ">30</option>
                                <option class='aumentarTamaño' value='50'"; if (@ $_POST['renumres']=='50'){echo 'selected';} echo ">50</option>
                                <option class='aumentarTamaño' value='100'"; if (@ $_POST['renumres']=='100'){echo 'selected';} echo ">100</option>
                                <option class='aumentarTamaño' value='-1'"; if (@ $_POST['renumres']=='-1'){echo 'selected';} echo ">Todos</option>
                            </select>
                        </td>
                    </tr>

                    <tr>
                        <td colspan='4'>Barrios Encontrados: ".$_POST['numtop']."</td>
                    </tr>
                    <tr>
                        <td class='titulos2' style='width:5%; text-align:center;'>C&oacute;digo</td>
                        <td class='titulos2' style='text-align:center;'>Secci&oacute;n Presupuestal</td>
                        <td class='titulos2' style='text-align:center;'>Unidad Ejecutora</td>
                        <td class='titulos2' colspan='2' style='width:7%; text-align:center;' >Estado</td>
                    </tr>";

                    $iter = 'saludo1a';
                    $iter2 = 'saludo2';
                    $filas = 1;
                    while ($row = mysqli_fetch_row($resp))
                    {
                        $con2 = $con + $_POST['numpos'];
                        if($row[3] == 'S')
                        {
                            $imgsem = "src='imagenes/sema_verdeON.jpg' title='Activo'";
                            $coloracti = "#0F0";
                        }
                        else
                        {
                            $imgsem = "src='imagenes/sema_rojoON.jpg' title='Inactivo'";
                            $coloracti = "#C00";
                        }

                        if($gidcta != "")
                        {
                            if($gidcta == $row[0])
                            {
                                $estilo ='background-color:yellow';
                            }
                            else
                            {
                                $estilo = "";
                            }
                        }
                        else
                        {
                            $estilo = "";
                        }

                        $idcta = "'$row[0]'";
                        $numfil = "'$filas'";
                        $filtro = "'".@ $_POST['nombre']."'";
                        echo"
                        <tr class='$iter' onDblClick=\"verUltimaPos($idcta, $numfil, $filtro)\" style='text-transform:uppercase; $estilo'>
                            <td style='text-align:center;'>".str_pad($row[0], 2, '0', STR_PAD_LEFT)."</td>
                            <td style='text-align:center;'>$row[1]</td>
                            <td style='text-align:center;'>$row[2]</td>
                            <td style='text-align:center;'><img $imgsem style='width:20px'/></td>
                        </tr>";

                        $con += 1;
                        $aux = $iter;
                        $iter = $iter2;
                        $iter2 = $aux;
                        $filas++;
                    }

                    if (@$_POST['numtop'] == 0)
                    {
                        echo "
                        <table class='inicio'>
                            <tr>
                                <td class='saludo1' style='text-align:center;width:100%'><img src='imagenes\alert.png' style='width:25px'>No hay coincidencias en la b&uacute;squeda $tibusqueda<img src='imagenes\alert.png' style='width:25px'></td>
                            </tr>
                        </table>";
                    }
                    echo"
                </table>

                <table class='inicio'>
                    <tr>
                        <td style='text-align:center;'>
                            <a>$imagensback</a>&nbsp;
                            <a>$imagenback</a>&nbsp;&nbsp;";

            if($nuncilumnas <= 9)
            {
                $numfin = $nuncilumnas;
            }
            else
            {
                $numfin = 9;
            }
            for($xx = 1; $xx <= $numfin; $xx++)
            {
                if($numcontrol <= 9)
                {
                    $numx = $xx;
                }
                else
                {
                    $numx = $xx + ($numcontrol - 9);
                }

                if($numcontrol == $numx)
                {
                    echo"<a onClick='saltocol(\"$numx\")'; style='color:#24D915;cursor:pointer;'> $numx </a>";
                }
                else
                {
                    echo"<a onClick='saltocol(\"$numx\")'; style='color:#000000;cursor:pointer;'> $numx </a>";
                }
            }
            echo"			&nbsp;&nbsp;<a>$imagenforward</a>
                            &nbsp;<a>$imagensforward</a>
                        </td>
                    </tr>
                </table>";
        ?>

        <input type="hidden" name="ocules" id="ocules" value="<?php echo @ $_POST['ocules'];?>"/>
        <input type="hidden" name="actdes" id="actdes" value="<?php echo @ $_POST['actdes'];?>"/>
        <input type="hidden" name="numtop" id="numtop" value="<?php echo @ $_POST['numtop'];?>"/>

    </form>

    <div id="bgventanamodal2">
        <div id="ventanamodal2">
            <IFRAME  src="" name="buster" marginWidth=0 marginHeight=0 frameBorder=0 id="ventana2" frameSpacing=0 style="left:500px; width:900px; height:500px; top:200;"></IFRAME>
        </div>
    </div>
</body>
</html>
