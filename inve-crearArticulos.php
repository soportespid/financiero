<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require 'comun.inc';
	require 'funciones.inc';
	session_start();
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html lang=es>
	<head>
    <meta charset="utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1"/>
		<meta name="viewport" content="user-scalable=no">
		<title>:: IDEAL 10 - Almacen</title>
		<link href="favicon.ico" rel="shortcut icon"/>
		<link href="css/css2z.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
        <link href="css/tabs.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" href="sweetalert2/dist/sweetalert2.min.css">
        <script type="text/javascript" src="css/programas.js"></script>
        <script src="sweetalert2/dist/sweetalert2.min.js"></script>
		<script src="vue/vue.min.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script src="css/calendario.js?<?php echo date('d_m_Y_h_i_s');?>"></script>

        <style>
            .checkbox-wrapper-31:hover .check {
                stroke-dashoffset: 0;
            }
        
            .checkbox-wrapper-31 {
                position: relative;
                display: inline-block;
                width: 40px;
                height: 40px;
            }
            .checkbox-wrapper-31 .background {
                fill: #ccc;
                transition: ease all 0.6s;
                -webkit-transition: ease all 0.6s;
            }
            .checkbox-wrapper-31 .stroke {
                fill: none;
                stroke: #fff;
                stroke-miterlimit: 10;
                stroke-width: 2px;
                stroke-dashoffset: 100;
                stroke-dasharray: 100;
                transition: ease all 0.6s;
                -webkit-transition: ease all 0.6s;
            }
            .checkbox-wrapper-31 .check {
                fill: none;
                stroke: #fff;
                stroke-linecap: round;
                stroke-linejoin: round;
                stroke-width: 2px;
                stroke-dashoffset: 22;
                stroke-dasharray: 22;
                transition: ease all 0.6s;
                -webkit-transition: ease all 0.6s;
            }
            .checkbox-wrapper-31 input[type=checkbox] {
                position: absolute;
                width: 100%;
                height: 100%;
                left: 0;
                top: 0;
                margin: 0;
                opacity: 0;
                -appearance: none;
            }
            .checkbox-wrapper-31 input[type=checkbox]:hover {
                cursor: pointer;
            }
            .checkbox-wrapper-31 input[type=checkbox]:checked + svg .background {
                fill: #6cbe45;
            }
            .checkbox-wrapper-31 input[type=checkbox]:checked + svg .stroke {
                stroke-dashoffset: 0;
            }
            .checkbox-wrapper-31 input[type=checkbox]:checked + svg .check {
                stroke-dashoffset: 0;
            }
        </style>
	</head>
	<body>
		<header>
			<table>
				<tr><script>barra_imagenes("inve");</script><?php cuadro_titulos();?></tr>
			</table>
		</header>

		<form name="form2" method="post" action="">
			<section id="myapp" v-cloak >
                <div id="cargando" v-show="isLoading" class="loading" style="z-index: 10000;">
                    <span>Cargando...</span>
                </div>
				<nav>
					<table>
						<tr><?php menu_desplegable("inve");?></tr>
					</table>
                    <div class="bg-white group-btn p-1" id="newNavStyle">
                        <button type="button" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center" @click="window.location.reload()">
                            <span>Nuevo</span>
                            <svg viewBox="0 -960 960 960" ><path d="M440-440H200v-80h240v-240h80v240h240v80H520v240h-80v-240Z"/></svg>
                        </button>
                        <button type="button" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center" @click="save()">
                            <span>Guardar</span>
                            <svg viewBox="0 -960 960 960" ><path d="M840-680v480q0 33-23.5 56.5T760-120H200q-33 0-56.5-23.5T120-200v-560q0-33 23.5-56.5T200-840h480l160 160Zm-80 34L646-760H200v560h560v-446ZM480-240q50 0 85-35t35-85q0-50-35-85t-85-35q-50 0-85 35t-35 85q0 50 35 85t85 35ZM240-560h360v-160H240v160Zm-40-86v446-560 114Z"/></svg>
                        </button>
                        <button type="button" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center" @click="location.href='inve-buscarArticulos.php'">
                            <span>Buscar</span>
                            <svg class="fill-black group-hover:fill-white w-5 h-5" viewBox="0 -960 960 960" ><path d="M784-120 532-372q-30 24-69 38t-83 14q-109 0-184.5-75.5T120-580q0-109 75.5-184.5T380-840q109 0 184.5 75.5T640-580q0 44-14 83t-38 69l252 252-56 56ZM380-400q75 0 127.5-52.5T560-580q0-75-52.5-127.5T380-760q-75 0-127.5 52.5T200-580q0 75 52.5 127.5T380-400Z"/></svg>
                        </button>
                        <button type="button" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center" @click="mypop=window.open('inve-principal.php','',''); mypop.focus();">
                            <span>Nueva ventana</span>
                            <svg class="fill-black group-hover:fill-white w-5 h-5" viewBox="0 -960 960 960" ><path d="M320-120v-80h80v-80H160q-33 0-56.5-23.5T80-360v-400q0-33 23.5-56.5T160-840h640q33 0 56.5 23.5T880-760v400q0 33-23.5 56.5T800-280H560v80h80v80H320ZM160-360h640v-400H160v400Zm0 0v-400 400Z"/></svg>
                        </button>
                    </div>
				</nav>
                <div v-show="isModalBienes">
                    <transition name="modal">
                        <div class="modal-mask">
                            <div class="modal-wrapper">
                                <div class="modal-container">
                                    <table class="inicio ancho">
                                        <tr>
                                            <td class="titulos" colspan="2" >
                                               Bienes transportables
                                            </td>
                                            <td class="cerrar" style="width:7%" @click="isModalBienes = false">Cerrar</td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <input type="text" v-model="strBuscarBienModal" v-on:keyup="search('clasificacion')" placeholder="Buscar por código o nombre" style="width: 100%;">
                                            </td>
                                            <td>
                                                <input type="button" class="btn btn-primary" @click="search('clasificacion')" value="Buscar" >
                                            </td>
                                        </tr>
                                    </table>
                                    <table class='tablamv'>
                                        <thead>
                                            <tr style="text-align:center;">
                                                <th class="titulosnew00" style="width: 20%;">Código</th>
                                                <th class="titulosnew00">Nombre</th>
                                                <th style="width: 1%;"></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr v-show="arrModalBienes == 0 ">
                                                <td colspan="7">
                                                    <div style="text-align: center; color:#222; font-size:large" class="h4 text-primary text-center">
                                                        Utilice el filtro inve buscar clasificadores.
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr @click="selectItem(bien);isModalBienes = false" v-for="(bien,index) in arrModalBienes" v-bind:key="bien.grupo" v-bind:class="index % 2 ? 'saludo1a' : 'saludo2'">
                                                <td style="width:20%; text-align:center;">{{bien.grupo}}</td>
                                                <td style="text-align:left;"> {{bien.titulo}}</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </transition>
                </div>
				<article>
                    <div>
                        <table class="inicio">
                            <tr>
                                <td class="titulos" colspan="9">.: Agregar articulos</td>
                            </tr>
                            <tr>
                                <td class="textonew01">
                                    <div style=" margin-bottom:10px;width:820px;display:flex;justify-items:start;align-items:center; gap:10px;">
                                        <div style="width:100%;display:flex;justify-items:start;align-items:center; ">
                                            <p style="margin:0; margin-right:5px; white-space: nowrap; width:150px;">.: Grupo inventario:</p>
                                            <select  style="width:400px;text-transform:uppercase" v-model="intSelectGrupo" @change="getCodigo">
                                                <option value="0" selected>Seleccione</option>
                                                <option v-for="data in arrGrupos" :key="data.codigo" :value="data.codigo">{{data.codigo+"-"+data.nombre}}</option>
                                            </select>
                                        </div>
                                        <div style="width:100%;display:flex;justify-items:start;align-items:center; ">
                                            <p style="margin:0; margin-right:5px; white-space: nowrap;width:150px;">.: Código:</p>
                                            <input type="text" v-model="strCodigo" readonly style="width:400px;text-transform:uppercase">
                                        </div>
                                    </div>
                                    <div style="margin-bottom:10px; width:820px;display:flex;justify-items:start;align-items:center;gap:10px;">
                                        <div style="width:100%;display:flex;justify-items:start;align-items:center; ">
                                            <p style="margin:0; margin-right:5px; white-space: nowrap;width:150px;">.: Nombre de artículo:</p>
                                            <input type="text" v-model="strNombre" style="width:400px;text-transform:uppercase">
                                        </div>
                                        <div style="width:100%;display:flex;justify-items:start;align-items:center; ">
                                            <p style="margin:0; margin-right:5px; white-space: nowrap;width:150px;">.: Unidad de medida:</p>
                                            <select  style="width:400px;text-transform:uppercase" v-model="strUnidad">
                                                <option value="0" selected>Seleccione</option>
                                                <option v-for="data in arrUnidades" :key="data.descripcion_valor" :value="data.descripcion_valor">{{data.descripcion_valor}}</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div style="margin-bottom:10px; width:100%;display:flex;justify-items:start;align-items:center; gap:10px;">
                                        <div style="width:100%;display:flex;justify-items:start;align-items:center;">
                                            <p style="margin:0; margin-right:5px; white-space: nowrap; width:150px">.: Código CPC:</p>
                                            <input class="colordobleclik" style="width:200px;margin-right:5px;" type="text" readonly v-model="strBuscarBienCodigo" @dblclick="isModalBienes=true" v-on:keyup.enter="search('cod_bien')" v-on:change="search('cod_bien')" >
                                            <input type="text" style="width:360px;" v-model="objBien.titulo" style="width:150px;text-transform:uppercase" readonly>
                                        </div>
                                    </div>
                                </td>
                            <tr>
                        </table>
                    </div>
				</article>
			</section>
		</form>

		<script src="node_modules/read-excel-file/bundle/read-excel-file.min.js"></script>
		<script src="node_modules/xlsx/dist/xlsx.full.min.js"></script>
        <script src="Librerias/vue/vue.min.js"></script>
		<script src="Librerias/vue/axios.min.js"></script>
		<script type="module" src="almacen/Articulos/crear/inve-crearArticulo.js?<?= date('d_m_Y_h_i_s');?>"></script>
        
	</body>
</html>