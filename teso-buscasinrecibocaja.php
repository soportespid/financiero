<?php //V 1000 12/12/16 ?> 
<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require "comun.inc";
	require "funciones.inc";
	session_start();
	$linkbd=conectar_v7();	
	$linkbd -> set_charset("utf8");
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es">
	<head>
	 	<meta http-equiv="Content-Type" content="text/html" charset="iso-8859-1"/>
        <meta http-equiv="X-UA-Compatible" content="IE=9"/>
		<title>:: IDEAL 10 - Tesorer&iacute;a</title>
        <link href="css/css2.css" rel="stylesheet" type="text/css" />
        <link href="css/css3.css" rel="stylesheet" type="text/css" />
		<script type="text/javascript" src="jquery-1.11.0.min.js"></script> 
        <script type="text/javascript" src="css/programas.js"></script>
		<script type="text/javascript" src="css/calendario.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script>
			function verUltimaPos(idcta, filas, filtro){
				var scrtop=$('#divdet').scrollTop();
				var altura=$('#divdet').height();
				var numpag=$('#nummul').val();
				var limreg=$('#numres').val();
				if((numpag<=0)||(numpag==""))
					numpag=0;
				if((limreg==0)||(limreg==""))
					limreg=10;
				numpag++;
				location.href="teso-sinrecibocajaver.php?idrecibo="+idcta+"&scrtop="+scrtop+"&totreg="+filas+"&altura="+altura+"&numpag="+numpag+"&limreg="+limreg+"&filtro="+filtro;
			}
			function buscarbotonfiltro()
            {
                if((document.form2.fechaini.value != "" && document.form2.fechafin.value == "") || (document.form2.fechaini.value == "" && document.form2.fechafin.value != "")){
                    alert("Falta digitar fecha");
                }else{
                    document.getElementById('numpos').value=0;
                    document.getElementById('nummul').value=0;
                    document.form2.submit();
                }
                
            }
			function crearexcel(){
				document.form2.action="teso-ingresosinternosexcel.php";
				document.form2.target="_BLANK";
				document.form2.submit();
				document.form2.action="";
				document.form2.target="";
			}
		</script>
		
		<?php titlepag();?>

        <?php

			if(isset($_GET['fini']) && isset($_GET['ffin'])){
				if(!empty($_GET['fini']) && !empty($_GET['ffin'])){
					$_POST['fecha']=$_GET['fini'];
					$_POST['fecha2']=$_GET['ffin'];
				}
			}
			preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST["fechaini"], $fech1);
			preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST["fechafin"], $fech2);
			$f1=$fech1[3]."-".$fech1[2]."-".$fech1[1];
			$f2=$fech2[3]."-".$fech2[2]."-".$fech2[1];

			$scrtop=$_GET['scrtop'];
			if($scrtop=="") $scrtop=0;
			echo"<script>
				window.onload=function(){
					$('#divdet').scrollTop(".$scrtop.")
				}
			</script>";
			$gidcta=$_GET['idcta'];
			if(isset($_GET['filtro']))
				$_POST['nombre']=$_GET['filtro'];
		?>
</head>
<body>
<IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
<span id="todastablas2"></span>
<table>
	<tr><script>barra_imagenes("teso");</script><?php cuadro_titulos();?></tr>	 
	<tr><?php menu_desplegable("teso");?></tr>
	<tr>
		<td colspan="3" class="cinta">
			<a href="teso-sinrecibocaja.php" class="mgbt"><img src="imagenes/add.png" title="Nuevo"/></a>
			<a class="mgbt"><img src="imagenes/guardad.png"/></a>
			<a class="mgbt"><img src="imagenes/buscad.png"/></a>
			<a onClick="mypop=window.open('plan-agenda.php','','');mypop.focus()" class="mgbt"><img src="imagenes/agenda1.png" title="Agenda" /></a>
			<a class="mgbt" onClick="mypop=window.open('teso-principal.php','','');mypop.focus();"><img src="imagenes/nv.png" title="Nueva ventana"></a>
			<a onClick="mypop=window.open('<?php echo $url2; ?>','','');mypop.focus();" class="tooltip bottom mgbt"><img src="imagenes/duplicar_pantalla.png" title="Duplicar pesta&ntilde;a"></a>
			<a onclick="crearexcel()" class="mgbt"><img src="imagenes/excel.png" title="Excel"></a>
		</td>
	</tr>	
</table>
<?php
if($_GET['numpag']!=""){
	$oculto=$_POST['oculto'];
	if($oculto!=2){
		$_POST['numres']=$_GET['limreg'];
		$_POST['numpos']=$_GET['limreg']*($_GET['numpag']-1);
		$_POST['nummul']=$_GET['numpag']-1;
	}
}
else{
	if($_POST['nummul']==""){
		$_POST['numres']=10;
		$_POST['numpos']=0;
		$_POST['nummul']=0;
	}
}
?>
<form name="form2" method="post" action="teso-buscasinrecibocaja.php">
	<table  class="inicio" align="center" >
    	<tr >
        	<td class="titulos" colspan="16">:. Buscar ingresos propios</td>
        	<td style="width:7%" class="cerrar" ><a href="teso-principal.php">Cerrar</a></td>
      	</tr>
      	<tr>
			<td class="tamano01" style="width:3.6cm;">Numero recibo:</td>
			<td colspan="4"><input type="search" name="numero" id="numero" value="<?php echo $_POST['numero'];?>" style="width:100%;"/></td>
			<td class="tamano01" style="width:3.6cm;">Concepto:</td>
			<td colspan="4"><input type="search" name="nombre" id="nombre" value="<?php echo $_POST['nombre'];?>" style="width:100%;"/></td>
			<td class="tamano01">Fecha inicial: </td>
			<td style="width:10%;"><input type="search" name="fechaini" id="fc_1198971545" title="DD/MM/YYYY" placeholder="DD/MM/YYYY" value="<?php echo $_POST['fechaini'];?>" onchange="" onKeyUp="return tabular(event,this)" onKeyDown="mascara(this,'/',patron,true)" maxlength="10" style="width:75%">&nbsp;<img src="imagenes/calendario04.png" onClick="displayCalendarFor('fc_1198971545');" class="icobut" title="Calendario"></td>
			<td class="tamano01" >Fecha final: </td>
			<td style="width:10%;"><input type="search" name="fechafin"  id="fc_1198971546" title="DD/MM/YYYY" placeholder="DD/MM/YYYY" value="<?php echo $_POST['fechafin'];?>" onchange="" onKeyUp="return tabular(event,this) " onKeyDown="mascara(this,'/',patron,true)" maxlength="10" style="width:75%"/>&nbsp;<img src="imagenes/calendario04.png" onClick="displayCalendarFor('fc_1198971546');"  class="icobut" title="Calendario"></td>  
			<td style="padding-bottom:1px"><em class="botonflecha" onClick="buscarbotonfiltro();">Buscar</em></td>
		</tr>                  
  	</table> 

	<input id="oculto" name="oculto" type="hidden" value="1">
    <input name="var1" type="hidden" id="var1" value=<?php echo $_POST['var1'];?> >
	<input type="hidden" name="numres" id="numres" value="<?php echo $_POST['numres'];?>"/>
   	<input type="hidden" name="numpos" id="numpos" value="<?php echo $_POST['numpos'];?>"/>
   	<input type="hidden" name="nummul" id="nummul" value="<?php echo $_POST['nummul'];?>"/>
	<input type="hidden" name="fecham1"  id="fecham1" value="<?php echo $_POST['fecham1']; ?>"/>
	<input type="hidden" name="fecham2" id="fecham2" value="<?php echo $_POST['fecham2']; ?>"/>

    <div class="subpantallap" style="height:68.5%; width:99.6%; overflow-x:hidden;" id="divdet">
   		<?php
		$oculto=$_POST['oculto'];
		if($_POST['oculto']==2){
	 		
	 		$sqlr="select * from tesosinreciboscaja where id_recibos=$_POST[var1]";
			$resp = mysqli_query($linkbd,$sqlr);
			$row = mysqli_fetch_row($resp);
	 		//********Comprobante contable en 000000000000
	  		$sqlr="update comprobante_cab set total_debito=0,total_credito=0,estado='0' where tipo_comp='25' and numerotipo=$row[0]";
	  		mysqli_query($linkbd,$sqlr);
	  		$sqlr="update comprobante_det set valdebito=0,valcredito=0 where id_comp='25 $row[0]'";
	  		mysqli_query($linkbd,$sqlr);
	   		$sqlr="update pptocomprobante_cab set estado='0' where tipo_comp='18' and numerotipo=$row[0]";
	  		mysqli_query($linkbd,$sqlr);
	 		//********PREDIAL O RECAUDO SE ACTIVA COLOCAR 'S'
	  		if($row[10]=='3'){
 	  			$sqlr="update tesosinrecaudos set estado='S' where id_recaudo=$row[4]";
	  			mysqli_query($linkbd,$sqlr);
	   		}	 
	 		//******** RECIBO DE CAJA ANULAR 'N'	 
	  		$sqlr="update tesosinreciboscaja set estado='N' where id_recibos=$row[0]";
	  		mysqli_query($linkbd,$sqlr);
	  		$sqlr="select * from pptosinrecibocajappto where idrecibo=$row[0]";
  	  		$resp=mysqli_query($linkbd,$sqlr);
	  		while($r=mysqli_fetch_row($resp)){
				$sqlr="update pptocuentaspptoinicial set ingresos=ingresos-$r[3] where cuenta='$r[1]'";
				mysqli_query($linkbd,$sqlr);
	   		}	
	   		$sqlr="delete from pptosinrecibocajappto where idrecibo=$row[0]";
  	  		$resp=mysqli_query($linkbd,$sqlr); 
		}
   		?>

      	<?php
			$_POST['fecham1']=$f1;
			$_POST['fecham2']=$f2;

			$oculto=$_POST['oculto'];

			$crit1="";
			$crit2="";

			if ($_POST['numero'] != "")
			{
				$crit1=" AND tesosinreciboscaja.id_recibos LIKE '".$_POST['numero']."' ";
			}

			if ($_POST['nombre']!="")
			{
				$crit2=" AND tesosinrecaudos.concepto LIKE '".$_POST['nombre']."'  ";
			}
			
			$sqlr="SELECT * FROM tesosinreciboscaja, tesosinrecaudos WHERE tesosinrecaudos.id_recaudo = tesosinreciboscaja.id_recaudo ".$crit1.$crit2." ORDER BY tesosinreciboscaja.id_recibos DESC";
			if(isset($_POST['fechaini']) && isset($_POST['fechafin'])){
				if(!empty($_POST['fechaini']) && !empty($_POST['fechafin'])){
					$sqlr="SELECT * FROM tesosinreciboscaja, tesosinrecaudos WHERE tesosinrecaudos.id_recaudo = tesosinreciboscaja.id_recaudo ".$crit1.$crit2." AND tesosinreciboscaja.fecha BETWEEN '$f1' AND '$f2' ORDER BY tesosinreciboscaja.id_recibos DESC";
				}
			}
			$resp = mysqli_query($linkbd,$sqlr);
			$ntr = mysqli_num_rows($resp);
			
			$_POST['numtop']=$ntr;

			$nuncilumnas=ceil($_POST['numtop']/$_POST['numres']);

			$cond2="";

			if ($_POST['numres']!="-1"){ $cond2="LIMIT $_POST[numpos], $_POST[numres]";}

			$sqlr="SELECT * FROM tesosinreciboscaja, tesosinrecaudos WHERE tesosinrecaudos.id_recaudo = tesosinreciboscaja.id_recaudo ".$crit1.$crit2." ORDER BY tesosinreciboscaja.id_recibos DESC $cond2";
			if(isset($_POST['fechaini']) && isset($_POST['fechafin'])){
				if(!empty($_POST['fechaini']) && !empty($_POST['fechafin'])){
					$sqlr="SELECT * FROM tesosinreciboscaja, tesosinrecaudos WHERE tesosinrecaudos.id_recaudo = tesosinreciboscaja.id_recaudo ".$crit1.$crit2." AND tesosinreciboscaja.fecha BETWEEN '$f1' AND '$f2' ORDER BY tesosinreciboscaja.id_recibos DESC $cond2";
				}
			}
	
			$resp = mysqli_query($linkbd,$sqlr);
			
			$con=1;

			$numcontrol=$_POST['nummul']+1;
			if(($nuncilumnas==$numcontrol)||($_POST['numres']=="-1")){
				$imagenforward="<img src='imagenes/forward02.png' style='width:17px'>";
				$imagensforward="<img src='imagenes/skip_forward02.png' style='width:16px' >";
			}
			else{
				$imagenforward="<img src='imagenes/forward01.png' style='width:17px' title='Siguiente' onClick='numsiguiente()'>";
				$imagensforward="<img src='imagenes/skip_forward01.png' style='width:16px' title='Fin' onClick='saltocol(\"$nuncilumnas\")'>";
			}
			if(($_POST['numpos']==0)||($_POST['numres']=="-1")){
				$imagenback="<img src='imagenes/back02.png' style='width:17px'>";
				$imagensback="<img src='imagenes/skip_back02.png' style='width:16px'>";
			}
			else{
				$imagenback="<img src='imagenes/back01.png' style='width:17px' title='Anterior' onClick='numanterior();'>";
				$imagensback="<img src='imagenes/skip_back01.png' style='width:16px' title='Inicio' onClick='saltocol(\"1\")'>";
			}

			$con=1;
		echo "<table class='inicio' align='center' >
			<tr>
				<td colspan='8' class='titulos'>.: Resultados Busqueda:</td>
				<td class='submenu'>
					<select name='renumres' id='renumres' onChange='cambionum();' style='width:100%'>
						<option value='10'"; if ($_POST['renumres']=='10'){echo 'selected';} echo ">10</option>
						<option value='20'"; if ($_POST['renumres']=='20'){echo 'selected';} echo ">20</option>
						<option value='30'"; if ($_POST['renumres']=='30'){echo 'selected';} echo ">30</option>
						<option value='50'"; if ($_POST['renumres']=='50'){echo 'selected';} echo ">50</option>
						<option value='100'"; if ($_POST['renumres']=='100'){echo 'selected';} echo ">100</option>
						<option value='-1'"; if ($_POST['renumres']=='-1'){echo 'selected';} echo ">Todos</option>
					</select>
				</td>
			</tr>
			<tr>
				<td colspan='9' id='RecEnc'>Recibos encontrados: $ntr2</td>
			</tr>
			<tr>
				<td width='150' class='titulos2'>No Recibo</td>
				<td class='titulos2'>Concepto</td>
				<td class='titulos2'>Fecha</td>
				<td class='titulos2'>Contribuyente</td>
				<td class='titulos2'>Valor</td>
				<td class='titulos2'>No Liquid.</td>
				<td class='titulos2'>Tipo</td>
				<td class='titulos2'>ESTADO</td>
				<td class='titulos2' width='5%'><center>Ver</td>
			</tr>";	
            $iter='saludo1a';
            $iter2='saludo2';
			$filas=1;
			$tipos=array('Predial','Industria y Comercio','Otros Recaudos');

			if($_POST['fechaini'] == '' && $_POST['fechafin'] == '' && $_POST['numero'] == '' && $_POST['nombre'] == '')
			{
				echo "
				<table class='inicio'>
					<tr>
						<td class='saludo1' style='text-align:center;width:100%;font-size:25px'>Utilice el filtro de busqueda</td>
					</tr>
				</table>";
				$nuncilumnas = 0;
			}
			elseif(mysqli_num_rows($resp) == 0 || mysqli_num_rows($resp) == '0')
			{
				echo "
				<table class='inicio'>
					<tr>
						<td class='saludo1' style='text-align:center;width:100%;font-size:25px'>No hay resultados de su busqueda.</td>
					</tr>
				</table>";
			}
			else
			{

				while ($row =mysqli_fetch_row($resp))
				{
					$ntr2 = $ntr;

					echo "<script>document.getElementById('RecEnc').innerHTML = 'Recibos encontrados: $ntr2'</script>";

					$sqlr1="select * from tesosinrecaudos where id_recaudo='".$row[4]."'";
					$resp1 = mysqli_query($linkbd, $sqlr1);
					$rowt = mysqli_fetch_row($resp1);
					//echo $sqlr1."<br>";
					//echo $rowt[4];
					
					$ntercero=buscatercero($rowt[4]);
					if($gidcta!=""){
						if($gidcta==$row[0]){
							$estilo='background-color:yellow';
						}
						else{
							$estilo="";
						}
					}
					else{
						$estilo="";
					}	
					$idcta="'".$row[0]."'";
					$numfil="'".$filas."'";
					$filtro="'".$_POST['nombre']."'";

					echo"
						<input type='hidden' name='nreciboE[]' value='".$row[0]."'>
						<input type='hidden' name='conceptoE[]' value='".$rowt[6]."'>
						<input type='hidden' name='fechaE[]' value='".$row[2]."'>
						<input type='hidden' name='nomContribuyenteE[]' value='".$row[15]," - ", $ntercero."'>
						<input type='hidden' name='valorE[]' value='".number_format($row[8],2)."'>
						<input type='hidden' name='nliquiE[]' value='".$row[4]."'>
						<input type='hidden' name='tipoE[]' value='".$tipos[$row[10]-1]."'>
					";

					if ($row[9]=='S')
					{
						echo"
						<input type='hidden' name='estadoE[]' value='ACTIVO'>";
					}
					if ($row[9]=='N')
					{
						echo"
						<input type='hidden' name='estadoE[]' value='ANULADO'>";
					}
					if ($row[9]=='P')
					{
						echo"
						<input type='hidden' name='estadoE[]' value='PAGO'>";
					}

					echo"<tr class='$iter' onMouseOver=\"anterior=this.style.backgroundColor;this.style.backgroundColor='#40b3ff';\"
				onMouseOut=\"this.style.backgroundColor=anterior\" onDblClick=\"verUltimaPos($idcta, $numfil, $filtro)\" style='text-transform:uppercase; $estilo' >
						<td>$row[0]</td>
						<td>$rowt[6]</td>
						<td>$row[2]</td>
						<td>$row[15] $ntercero</td>
						<td>".number_format($row[8],2)."</td>
						<td >$row[4]</td>
						<td>".$tipos[$row[10]-1]."</td>";
						if ($row[9]=='S'){echo "<td ><center><img src='imagenes/sema_verdeON.jpg' style='width:18px;'></center></td>";}
						if ($row[9]=='N'){echo "<td ><center><img src='imagenes/sema_rojoON.jpg' style='width:18px;'></center></td>";}
						echo"<td style='text-align:center;'>
							<a onClick=\"verUltimaPos($idcta, $numfil, $filtro)\" style='cursor:pointer;'>
								<img src='imagenes/lupa02.png' style='width:18px' title='Ver'>
							</a>
						</td>
					</tr>";
					$con+=1;
					$aux=$iter;
					$iter=$iter2;
					$iter2=$aux;
					$filas++;
				}
         	}
           	echo"</table>
			<table class='inicio'>
				<tr>
					<td style='text-align:center;'>
						<a href='#'>$imagensback</a>&nbsp;
						<a href='#'>$imagenback</a>&nbsp;&nbsp;";
						if($nuncilumnas<=9){$numfin=$nuncilumnas;}
						else{$numfin=9;}
						for($xx = 1; $xx <= $numfin; $xx++){
							if($numcontrol<=9){$numx=$xx;}
							else{$numx=$xx+($numcontrol-9);}
							if($numcontrol==$numx){echo"<a href='#' onClick='saltocol(\"$numx\")'; style='color:#24D915'> $numx </a>";}
							else {echo"<a href='#' onClick='saltocol(\"$numx\")'; style='color:#000000'> $numx </a>";}
						}
						echo "		&nbsp;&nbsp;<a href='#'>$imagenforward</a>
									&nbsp;<a href='#'>$imagensforward</a>
					</td>
				</tr>
			</table>";

?></div>
</form> 
</body>
</html>