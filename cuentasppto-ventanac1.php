<?php //V 1000 12/12/16 ?> 
<?php 
	require "comun.inc";
	require "funciones.inc";
	session_start();
	$linkbd=conectar_v7();	
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	header("Cache-control: private"); // Arregla IE 6
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es">
	<head>
	 	<meta http-equiv="Content-Type" content="text/html" charset="iso-8859-1"/>
        <meta http-equiv="X-UA-Compatible" content="IE=9"/>
		<title>:: IDEAL 10 - Presupuesto</title>
        <link href="css/css2.css" rel="stylesheet" type="text/css" />
        <link href="css/css3.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="css/calendario.js"></script>
		<script>
			var anterior;
			function ponprefijo(pref,opc){ 
				var cuenta="<?php echo $_GET['c']; ?>";
				if(cuenta=="1"){
				opener.document.form2.cuenta1.value =pref  ;
				opener.document.form2.cuenta1.focus();
				}else{
				opener.document.form2.cuenta2.value =pref  ;
				opener.document.form2.cuenta2.focus();	
				}
				
				window.close() ;
			} 
		</script> 
		<?php titlepag();?>
	</head>
	<body>
		<?php
		if(!$_POST['tipo'])
			$_POST['tipo']=$_GET['ti'];
		?>
	<form action="" method="post" enctype="multipart/form-data" name="form1">
		<table class="inicio">
			<tr >
				<td height="25" colspan="4" class="titulos" >Buscar CUENTAS PRESUPUESTALES</td>
				<td class="cerrar"><a onClick="parent.despliegamodal2('hidden');">&nbsp;Cerrar</a></td>
			</tr>
			<tr>
				<td colspan="4" class="titulos2" >:&middot; Por Descripcion </td>
			</tr>
			<tr >
				<td class="saludo1" >:&middot; Numero Cuenta:</td>
				<td  colspan="3"><input name="numero" type="text" size="30" >
					<input type="hidden" name="tipo"  id="tipo" value="<?php echo $_POST['tipo']?>" >
					<input type="hidden" name="oculto" id="oculto" value="1" >
					<input type="submit" name="Submit" value="Buscar" >
				</td>
			</tr>
			<tr >
				<td colspan="4" align="center">&nbsp;</td>
			</tr>
		</table>
		<div class="subpantalla" style="height:73.5%; width:99.6%; overflow-x:hidden;">
			<table class="inicio">
				<tr >
					<td height="25" colspan="5" class="titulos" >Resultados Busqueda </td>
				</tr>
				<tr >
					<td width="32" class="titulos2" >Item</td>
					<td width="76" class="titulos2" >Cuenta </td>
					<td width="140" class="titulos2" >Descripcion</td>	  
					<td width="140" class="titulos2" >Tipo</td>
					<td width="140" class="titulos2" >Estado</td>	  	  
				</tr>
				<?php
				$vigusu=vigencia_usuarios($_SESSION['cedulausu']); 
				$oculto=$_POST['oculto'];
				//echo $oculto;
				//if($oculto!="")
				{
					$link=conectar_v7();
					$cond="and  (pptocuentas.cuenta like'%".$_POST['numero']."%' or pptocuentas.nombre like '%".($_POST['numero'])."%')";
					if($_POST['tipo']=='1')
					{
						$sqlr="Select distinct pptocuentas.*, dominios.tipo from pptocuentas, dominios where dominios.descripcion_valor=pptocuentas.clasificacion AND dominios.nombre_dominio='CLASIFICACION_RUBROS' AND (dominios.tipo='I') and (pptocuentas.vigencia='".$vigusu."' or  pptocuentas.vigenciaf='".$vigusu."') $cond ORDER BY pptocuentas.cuenta ASC ";
					}
					if($_POST['tipo']=='2')
					{
						$sqlr="Select distinct pptocuentas.*, dominios.tipo from pptocuentas, dominios where dominios.descripcion_valor=pptocuentas.clasificacion AND dominios.nombre_dominio='CLASIFICACION_RUBROS' AND (dominios.tipo='G') and (pptocuentas.vigencia='".$vigusu."' or  pptocuentas.vigenciaf='".$vigusu."') $cond ORDER BY pptocuentas.cuenta ASC ";
					}
					$resp = mysqli_query($link,$sqlr);			
					$co='saludo1a';
					$co2='saludo2';	
					$i=1;
					while ($r = mysqli_fetch_row($resp)) 
					{
						echo "<tr class='$co' onMouseOver=\"anterior=this.style.backgroundColor;this.style.backgroundColor='#40b3ff';\" onMouseOut=\"this.style.backgroundColor=anterior\" "; 
						if ($r[2]=='Auxiliar' || $r[2]=='auxiliar'  || $r[2]=='Mayor'){echo" onClick=\"javascript:ponprefijo('$r[0]','$r[1]')\"";} 
						echo"><td>$i</td>
						<td >$r[0]</td>
						<td>".ucwords(strtolower($r[1]))."</td>
						<td>$r[2]</td>
						<td>".ucwords(strtolower($r[3]))."</td></tr>";
						$aux=$co;
						$co=$co2;
						$co2=$aux;
						$i=1+$i;
					}
					$_POST['oculto']="";
				}
				?>
			</table>
		</div>
	</form>
</body>
</html> 
