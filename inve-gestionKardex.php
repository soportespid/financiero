<?php

    header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require 'comun.inc';
	require 'funciones.inc';
	session_start();
    if(empty($_SESSION)){
        header("location: index.php");
    }
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html lang=es>
	<head>
    <meta charset="utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1"/>
		<meta name="viewport" content="user-scalable=no">
		<title>:: IDEAL 10 - Almacén</title>
		<link href="favicon.ico" rel="shortcut icon"/>
		<link href="css/css2z.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
        <link href="css/tabs.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" href="sweetalert2/dist/sweetalert2.min.css">
        <script type="text/javascript" src="css/programas.js"></script>
        <script src="sweetalert2/dist/sweetalert2.min.js"></script>
		<script src="vue/vue.min.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script src="css/calendario.js?<?php echo date('d_m_Y_h_i_s');?>"></script>

        <style>
            .yellow{
                background-color: #FFFF00;
            }
            .red{
                color:#fff;
                background-color: #ff0000;
            }
            .purple{
                color:#fff;
                background-color: #7D2181;
            }
            .label-purple{
                padding:4px;
                border-radius:5px;
                color:#fff;
                background-color: #7D2181;
                font-weight:bold;
            }
            .label-red{
                padding:4px;
                border-radius:5px;
                color:#fff;
                background-color: #ff0000;
                font-weight:bold;
            }
            .label-yellow{
                padding:4px;
                border-radius:5px;
                background-color: #FFFF00;
                font-weight:bold;
            }
            .checkbox-wrapper-31:hover .check {
                stroke-dashoffset: 0;
            }

            .checkbox-wrapper-31 {
                position: relative;
                display: inline-block;
                width: 40px;
                height: 40px;
            }
            .checkbox-wrapper-31 .background {
                fill: #ccc;
                transition: ease all 0.6s;
                -webkit-transition: ease all 0.6s;
            }
            .checkbox-wrapper-31 .stroke {
                fill: none;
                stroke: #fff;
                stroke-miterlimit: 10;
                stroke-width: 2px;
                stroke-dashoffset: 100;
                stroke-dasharray: 100;
                transition: ease all 0.6s;
                -webkit-transition: ease all 0.6s;
            }
            .checkbox-wrapper-31 .check {
                fill: none;
                stroke: #fff;
                stroke-linecap: round;
                stroke-linejoin: round;
                stroke-width: 2px;
                stroke-dashoffset: 22;
                stroke-dasharray: 22;
                transition: ease all 0.6s;
                -webkit-transition: ease all 0.6s;
            }
            .checkbox-wrapper-31 input[type=checkbox] {
                position: absolute;
                width: 100%;
                height: 100%;
                left: 0;
                top: 0;
                margin: 0;
                opacity: 0;
                -appearance: none;
            }
            .checkbox-wrapper-31 input[type=checkbox]:hover {
                cursor: pointer;
            }
            .checkbox-wrapper-31 input[type=checkbox]:checked + svg .background {
                fill: #6cbe45;
            }
            .checkbox-wrapper-31 input[type=checkbox]:checked + svg .stroke {
                stroke-dashoffset: 0;
            }
            .checkbox-wrapper-31 input[type=checkbox]:checked + svg .check {
                stroke-dashoffset: 0;
            }
        </style>
	</head>
	<body>
		<header>
			<table>
				<tr><script>barra_imagenes("inve");</script><?php cuadro_titulos();?></tr>
			</table>
		</header>

		<form name="form2" method="post" action="">
			<section id="myapp" v-cloak >
                <div id="cargando" v-show="isLoading" class="loading" style="z-index: 10000;">
                    <span>Cargando...</span>
                </div>
				<nav>
					<table>
						<tr><?php menu_desplegable("inve");?></tr>
                        <tr>
							<td colspan="3" class="cinta">
                                <img src="imagenes/add2.png"  class="mgbt" title="Nuevo">
								<img src="imagenes/guardad.png"   title="Guardar"  class="mgbt">
								<img src="imagenes/busca.png" @click="window.location.href='inve-gestionKardex.php'"   class="mgbt" title="Buscar">
								<img src="imagenes/nv.png" @click="mypop=window.open('inve-principal.php','',''); mypop.focus();" class="mgbt" title="Nueva Ventana">
                                <img src="imagenes/print.png" title="Imprimir" @click="printPDF()" class="mgbt">
                                <a @click="printExcel" class="mgbt"><img src="imagenes/excel.png" title="Excel"></a>
							</td>
						</tr>
					</table>
				</nav>
				<article>
                    <table class="inicio">
                        <tbody>
                            <tr>
                                <td class="titulos" colspan="9">.: Kardex almacén</td>
                            </tr>
                            <tr>
                                <td style="width:8%;">.: Fecha inicial:</td>
                                <td><input style="width:100%;" type="text" id="fechaInicial" name="fechaInicial" onKeyUp="return tabular(event,this)" id="fechaInicial" title="DD/MM/YYYY" onDblClick="displayCalendarFor('fechaInicial');" class="colordobleclik" autocomplete="off" onChange=""  readonly></td>
                                <td style="width:8%;">.: Fecha final:</td>
                                <td><input style="width:100%;" type="text" id="fechaFinal" name="fechaFinal"  onKeyUp="return tabular(event,this)" id="fechaFinal" title="DD/MM/YYYY" onDblClick="displayCalendarFor('fechaFinal');" class="colordobleclik" autocomplete="off" onChange=""  readonly></td>
                                <td style="width:5%;">.: Artículo:</td>
                                <td><input type="text" class="colordobleclik" v-model="strBuscarCodArticulo"  @change="search(1,'cod_articulo')" @keyup.enter="search(1,'cod_articulo')" @dblclick="isModalArticulos = true" style="width:100%"></td>
                                <td><input type="text" v-model="objArticulo.nombre" disabled style="width:100%"></td>
                            </tr>
                            <tr>
                                <td style="width:5%;">.: Bodega:</td>
                                <td>
                                    <select style="width:100%" v-model="selectBodega" >
                                        <option disabled selected>Seleccione</option>
                                        <option v-for="b in arrBodegas" :key="b.id_cc" :value="b.id_cc">{{b.id_cc}} - {{b.nombre}}</option>
                                    </select>
                                </td>
                                <td ><input type="button" style="width:100%" class="btn btn-primary" @click="searchMovs" value="Buscar"></td>
                                <td ></td>
                                <td style="width:8%;">.: Convenciones:</td>
                                <td colspan="3">
                                    <span class="label-yellow">Cantidad negativa</span>
                                    <span class="label-purple">Cantidad no coincide contabilidad</span>
                                    <span class="label-red">No existe contabilidad</span>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <div   class='subpantalla' style='height:50vh; width:100%; margin-top:0px;  overflow-x:hidden'>
                        <table v-for="(data,i) in arrMovimientos" :key="i" class='inicio' >
                            <thead>
                                <tr>
                                    <th colspan="4" align='left' style="width:40%"class='titulos' style='text-align:left;'>.: Artículo: {{ data.nombre}}</th>
                                    <th colspan="4" class='titulos'>.: Entradas</th>
                                    <th colspan="4" class='titulos'>.: Salidas</th>
                                    <th colspan="4" class='titulos'>.: Saldo</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td class="titulosnew00" align='center'>Fecha</td>
                                    <td class="titulosnew00" align='center'>Documento</td>
                                    <td class="titulosnew00" align='center'>Movimiento</td>
                                    <td class="titulosnew00" align='center'>Centro costo</td>
                                    <td class="titulosnew00" align='center'>Unidad</td>
                                    <td class="titulosnew00" align='center'>Valor</td>
                                    <td class="titulosnew00" align='center'>Cantidad</td>
                                    <td class="titulosnew00" align='center'>Total</td>
                                    <td class="titulosnew00" align='center'>Unidad</td>
                                    <td class="titulosnew00" align='center'>Valor</td>
                                    <td class="titulosnew00" align='center'>Cantidad</td>
                                    <td class="titulosnew00" align='center'>Total</td>
                                    <td class="titulosnew00" align='center'>Unidad</td>
                                    <td class="titulosnew00" align='center'>Cantidad</td>
                                    <td class="titulosnew00" align='center'>Valor</td>
                                    <td class="titulosnew00" align='center'>Total</td>
                                </tr>
                                <tr style="height:50px;" v-for="(mov,j) in data.movimientos" :key="j" :class="[j % 2 ? 'saludo1a' : 'saludo2',(mov.estado_contable == 2 ? 'purple' : ''),(mov.estado_contable == 3 ? 'red' : '')]">
                                    <td align='center'>{{mov.fecha}}</td>
                                    <td align='center'>{{mov.documento}}</td>
                                    <td align='center'>{{mov.movimiento}} - {{mov.mov_nom}}</td>
                                    <td align='center'>{{mov.centro}}</td>
                                    <td align='center'>{{mov.unidad}}</td>
                                    <td align='right'>{{formatNumero(mov.valor)}}</td>
                                    <td align='center'>{{mov.entrada}}</td>
                                    <td align='right'>{{formatNumero(mov.total_entrada)}}</td>
                                    <td align='center'>{{mov.unidad}}</td>
                                    <td align='right'>{{formatNumero(mov.valor)}}</td>
                                    <td align='center'>{{mov.salida}}</td>
                                    <td align='right'>{{formatNumero(mov.total_salida)}}</td>
                                    <td align='center'>{{mov.unidad}}</td>
                                    <td align='center'>{{mov.cantidad_saldo}}</td>
                                    <td align='right'>{{formatNumero(mov.valor)}}</td>
                                    <td align='right'>{{formatNumero(mov.total_saldo)}}</td>
                                </tr>
                                <tr :class="data.cantidad_total < 0 ? 'yellow' : ''">
                                    <td colspan="13" align="right">En bodega:  </td>
                                    <td align="center">{{ data.cantidad_total}}</td>
                                    <td align="right">Valor total: </td>
                                    <td align="right">{{ formatNumero(data.valor_total)}}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
				</article>
                <!--MODALES-->
                <div v-show="isModalArticulos">
                    <transition name="modal">
                        <div class="modal-mask">
                            <div class="modal-wrapper">
                                <div class="modal-container">
                                    <table class="inicio ancho">
                                        <tr>
                                            <td class="titulos" colspan="2" >.: Artículos</td>
                                            <td class="cerrar" style="width:7%" @click="isModalArticulos = false">Cerrar</td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <input type="search" v-model="strSearch" @keyup="search(1,'articulo')" placeholder="Buscar por código o nombre" style="width: 100%;">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="titulos" colspan="3" >.: Resultados de búsqueda</td>
                                        </tr>
                                        <tr>
                                            <td>Total: {{intResults}}</td>
                                        </tr>
                                    </table>
                                    <table class='tablamv'>
                                        <thead>
                                            <th style="width:20%"class="titulosnew00" >Código</th>
                                            <th align="start" class="titulosnew00">Nombre</th>
                                        </thead>
                                        <tbody>
                                            <tr v-for="(data,index) in arrArticulos" @click="selectItem(data,'articulo');isModalArticulos = false"  :key="index" v-bind:class="index % 2 ? 'saludo1a' : 'saludo2'">
                                                <td align="center" style="width:20%">{{ data.codigo_articulo}}</td>
                                                <td >{{ data.nombre}}</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <div  v-if="arrArticulos !=''" class="inicio">
                                        <p style="text-align:center">Página {{ intPage }} de {{ intTotalPages }}</p>
                                        <ul style="list-style:none; padding:0;display:flex;justify-content: center;align-items:center">
                                            <li v-show="intPage > 1" @click="search(intPage = 1)" style="cursor:pointer;padding:4px 10px;background:#fff; border:1px solid #39c"><< </li>
                                            <li v-show="intPage > 1" @click="search(--intPage)" style="cursor:pointer;padding:4px 10px;background:#fff; border:1px solid #39c" ><</li>
                                            <li v-show="intPage < intTotalPages" @click="search(++intPage)" style="cursor:pointer;padding:4px 10px;background:#fff; border:1px solid #39c" >></li>
                                            <li v-show="intPage < intTotalPages" @click="search(intPage = intTotalPages)" style="cursor:pointer;padding:4px 10px;background:#fff; border:1px solid #39c">>></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </transition>
                </div>
			</section>
		</form>

		<script src="node_modules/read-excel-file/bundle/read-excel-file.min.js"></script>
		<script src="node_modules/xlsx/dist/xlsx.full.min.js"></script>
        <script src="Librerias/vue/vue.min.js"></script>
		<script src="Librerias/vue/axios.min.js"></script>
		<script type="module" src="almacen/Kardex/buscar/inve-gestionKardex.js?<?= date('d_m_Y_h_i_s');?>"></script>

	</body>
</html>
