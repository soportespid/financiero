<?php
	require_once("tcpdf/tcpdf_include.php");
	require('comun.inc');
	require "funciones.inc";
	date_default_timezone_set("America/Bogota");
	session_start();
    class MYPDF extends TCPDF 
	{
        public function Header() 
		{
            $linkbd = conectar_v7();
			$linkbd -> set_charset("utf8");
			$sqlr="select *from configbasica where estado='S' ";
			$res=mysqli_query($linkbd, $sqlr);
			while($row=mysqli_fetch_row($res))
			{
				$nit=$row[0];
				$rs=$row[1];
			}
            $dept = "META"; // se debe hacer consulta en la base de datos el departamento del municipio
            $titulo = "SECRETARIA DE PLANEACIÓN, INFRAESTRUCTURA Y TIC";
            $codigo = $_POST['consecutivo'];
            preg_match("/([0-9]{4})\-([0-9]{2})\-([0-9]{2})/", $_POST['fecha'],$fecha);
            $fechaSolicitud="$fecha[3]/$fecha[2]/$fecha[1]";
			//Parte Izquierda
			$this->Image('imagenes/escudo.jpg', 12, 12, 25, 25, 'JPG', '', 'T', true, 300, '', false, false, 0, false, false, false);// Logo
			$this->SetFont('helvetica','B',8);
			$this->SetY(10);
			$this->RoundedRect(10, 10, 190, 29, 1,'' );
			$this->Cell(0.1);
			$this->Cell(30,29,'','R',0,'L'); 
			$this->SetY(10);
			$this->SetX(40);
			$this->SetFont('helvetica','B',9);
            $this->Cell(80,4,'REPUBLICA DE COLOMBIA',0,1,'C'); 
            $this->SetX(40);
			$this->Cell(80,4,'DEPARTAMENTO DEL '.$dept,0,1,'C'); 
            $this->SetX(40);
			$this->Cell(80,4,strtoupper($rs),0,1,'C'); 
			$this->SetX(40);
			$this->Cell(80,4,"Nit. ".$nit,0,0,'C');
			//*****************************************************************************************************************************
			$this->SetY(27);
			$this->SetX(40);
			$this->MultiCell(80,12,$titulo,'T','C'); 
            $this->SetY(10);
            $this->SetX(90);
			$this->Cell(30,29,'','R',0,'L'); 
			$this->SetFont('helvetica','B',10);
		
			//************************************
            $this->Image('imagenes/escudo.jpg', 12, 12, 25, 25, 'JPG', '', 'T', true, 300, '', false, false, 0, false, false, false);// Logo
            //************************************
			$this->SetFont('helvetica','B',10);
			$this->SetY(10);
            $this->SetX(120);
			$this->Cell(51,7.2,' CODIGO : '.$codigo,'R',1,'L');
            $this->SetX(120);
			$this->Cell(51,7.2,' VERSIÓN: '.$fechaSolicitud,'RT',1,'L');
            $this->SetX(120);
			$this->Cell(51,7.2,' Aprobación: '.$_POST['codPaa'],'RT',1,'L');
            $this->SetX(120);
			$this->Cell(51,7.2,' Página '.$this->getAliasNumPage().' de '.$this->getAliasNbPages(),'RT',0,'L');
            $this->Image('imagenes/bandera.jpg', 173, 12, 25, 25, 'JPG', '', 'T', true, 300, '', false, false, 0, false, false, false);
			//**********************************************************
			$this->SetFont('times','B',10);
			$this->ln(12);
        }
        public function Footer() 
		{
            $linkbd = conectar_v7();
			$linkbd -> set_charset("utf8");
			$sqlr="SELECT direccion,telefono,web,email FROM configbasica WHERE estado='S'";
			$resp=mysqli_query($linkbd, $sqlr);
			while($row=mysqli_fetch_row($resp))
			{
				$direcc=strtoupper($row[0]);
				$telefonos=$row[1];
				$dirweb=strtoupper($row[3]);
				$coemail=strtoupper($row[2]);
			}
			if($direcc!=''){$vardirec="Dirección: $direcc, ";}
			else {$vardirec="";}
			if($telefonos!=''){$vartelef="Telefonos: $telefonos";}
			else{$vartelef="";}
			if($dirweb!=''){$varemail="Email: $dirweb, ";}
			else {$varemail="";}
			
			$this->SetFont('helvetica', 'I', 8);
			$txt = <<<EOD
			$vardirec $vartelef
			$varemail 
			EOD;
            $this->Write(0, $txt , '', 0, 'C', true, 0, false, false, 0);
        }
    }
    $pdf = new MYPDF('P','mm','Letter', true, 'iso-8859-1', false);
    $pdf->SetDocInfoUnicode (true); 
    // set document information
    $pdf->SetCreator(PDF_CREATOR);
    $pdf->SetAuthor('IDEALSAS');
    $pdf->SetTitle('Certificado Plan anual de Adquisiciones');
    $pdf->SetSubject('Certificado Plan anual de Adquisiciones');
    $pdf->SetKeywords('TCPDF, PDF, example, test, guide');
    $pdf->SetMargins(10, 40, 10);// set margins
    $pdf->SetHeaderMargin(40);// set margins
    $pdf->SetFooterMargin(17);// set margins
    $pdf->SetAutoPageBreak(TRUE, 20);// set auto page breaks
    // set some language-dependent strings (optional)
    if (@file_exists(dirname(__FILE__).'/lang/spa.php')) 
    {
        require_once(dirname(__FILE__).'/lang/spa.php');
        $pdf->setLanguageArray($l);
    }	
    $pdf->AddPage();

    $linkbd = conectar_v7();
    $linkbd -> set_charset("utf8");
    $sqlr="select *from configbasica where estado='S' ";
    $res=mysqli_query($linkbd, $sqlr);
    while($row=mysqli_fetch_row($res))
    {
        $nit=$row[0];
        $rs=$row[1];
    }
    $razon = explode(" ",$rs);
    $sqlr="SELECT funcionario, nomcargo FROM firmaspdf_det WHERE idfirmas='6' AND estado ='S'";
    $res=mysqli_query($linkbd,$sqlr);
    $rowCargo=mysqli_fetch_row($res);
    preg_match("/([0-9]{4})\-([0-9]{2})\-([0-9]{2})/", $_POST['fecha'],$f);
    $fechaComoEntero = strtotime($_POST['fecha']);   
    //variables a llenar
    $mes = date("m", $fechaComoEntero);
    $año = date("Y", $fechaComoEntero);
    $fecha = "{$f[3]} de $mes de $año";
    $nombre = $rowCargo[0];
    $rol = $rowCargo[1];
    $vigencia = $f[1];

    $pdf->ln(3);
    $pdf->SetX(20);
    $pdf->SetFont('times','',12);
    $pdf->MultiCell(90,6,$rs. ', '.$fecha,0,'L',0,1);
    $pdf->ln(10);
    $pdf->SetX(20);
    $pdf->Cell(18,6,'Señor(a).',0,1,'L');
    $pdf->SetX(20);
    $pdf->SetFont('times','B',12);
    $pdf->Cell(70,6,$nombre,0,1,'L');
    $pdf->SetX(20);
    $pdf->SetFont('times','',12);
    $pdf->Cell(80,6,$rol,0,1,'L');
    $pdf->ln(10);
    $pdf->SetX(30);
    $pdf->SetFont('times','B',12);
    $pdf->Cell(17,6,"Asunto: ",0,0,'L');
    $pdf->SetFont('times','',12);
    $pdf->MultiCell(120,6,"Solicitud certificado Plan anual de Adquisiciones – Vigencia ". $vigencia,0,'L',0,1,'','',true,0,false,true,'','M', true);
    $pdf->ln(4);
    $pdf->SetX(20);
    $pdf->MultiCell(170,15,"Comedidamente me permito solicitar se sirva certificar que el objeto que se describen a continuación se encuentra registrado en el Plan Anual de Adquisiciones, con la siguiente descripción:",0,'L',0,1,'','',true,0,false,true,'','M', true);
    $pdf->ln(4);
    $pdf->SetX(30);
    $pdf->SetFont('times','U',12);
    $pdf->Cell(70,6,"1. DATOS DE LAS ADQUISICIONES",0,1,'L');
    $pdf->SetFont('times','',12);
    $pdf->ln(4);
    $pdf->SetX(20);
    
    //variables a llenar
    $sql = "SELECT duracionest, modalidad, fuente, contacto_respon FROM contraplancompras WHERE codplan = $_POST[codPaa]";
    $row = mysqli_fetch_row(mysqli_query($linkbd, $sql));

    $sqlModalidad = "SELECT id, modalidad FROM contramodalidad WHERE id = $row[1] AND estado = 'S' ORDER BY id ASC";
    $resModalidad = mysqli_query($linkbd, $sqlModalidad);
    $rowModalidad = mysqli_fetch_row($resModalidad);
    
    $nom = explode("-", $row[3]);
    $cargo = $nom[1];
    $responsable = $nom[0];
    $dura = explode("/", $row[0]);

    $objeto = "$_POST[descripcionPaa]";
    $duracion = "{$dura[0]} días y {$dura[1]} meses";
    $modalidad = $rowModalidad[1];
    $fuente = $row[2];

    $pdf->SetFont('times','B',12);
    $pdf->MultiCell(70,15,'OBJETO DEL CONTRATO',1,'L',0,0,'','',true,0,false,true,'','M', true);
    $pdf->SetFont('times','',12);
    $pdf->MultiCell(100,15,$objeto,1,'L',0,1,'','',true,0,false,true,'','M', true);
    $pdf->SetX(20);
    $pdf->SetFont('times','B',12);
    $pdf->MultiCell(70,15,'DURACIÓN DEL CONTRATO',1,'L',0,0,'','',true,0,false,true,'','M', true);
    $pdf->SetFont('times','',12);
    $pdf->MultiCell(100,15,$duracion,1,'L',0,1,'','',true,0,false,true,'','M', true);
    $pdf->SetX(20);
    $pdf->SetFont('times','B',12);
    $pdf->MultiCell(70,15,'MODALIDAD DE ADQUISICIÓN',1,'L',0,0,'','',true,0,false,true,'','M', true);
    $pdf->SetFont('times','',12);
    $pdf->MultiCell(100,15,$modalidad,1,'L',0,1,'','',true,0,false,true,'','M', true);
    $pdf->SetX(20);
    $pdf->SetFont('times','B',12);
    $pdf->MultiCell(70,15,'FUENTE DE FINANCIACIÓN',1,'L',0,0,'','',true,0,false,true,'','M', true);
    $pdf->SetFont('times','',12);
    $pdf->MultiCell(100,15,$fuente,1,'L',0,1,'','',true,0,false,true,'','M', true);
    $pdf->ln(6);
    $pdf->SetX(30);
    $pdf->SetFont('times','U',12);
    $pdf->Cell(155,6,"2.	DESCRIPCION DE LOS ELEMENTOS, OBRAS Y/O SERVICIOS A CONTRATAR",0,1,'L');
    $pdf->ln(2);
    $pdf->SetFillColor(245,245,245);
    $pdf->SetFont('helvetica','B',12);
    $pdf->SetX(20);
    $pdf->Cell(35,6,"ITEM",1,0,'C',1);
    $pdf->Cell(35,6,"CODIGO",1,0,'C',1);
    $pdf->Cell(100,6,"DESCRIPCIÓN",1,1,'C',1);

    $codigo = array("1", "2", "4", "7");
    $desc = array("he", "12", "ht", "yy");
    $pdf->SetFont('helvetica','',12);
    for($x=0;$x<count($_POST['productos']);$x++)
	{
        $item = 1;
        $lineaitem = $pdf ->getNumLines($desc[$x], 100);
        $lineacod = $pdf ->getNumLines($codigo[$x], 35);
        $valorMax = max($lineaitem,$lineacod);
        $altura = $valorMax * 3;
        if ($con%2==0){$pdf->SetFillColor(255,255,255);}
		else {$pdf->SetFillColor(245,245,245);}
        $pdf->SetX(20);
        $pdf->MultiCell(35,$altura,$item,1,'C',1,0);
        $pdf->MultiCell(35,$altura,$_POST['productos'][$x],1,'C',1,0);
        $pdf->MultiCell(100,$altura,$_POST['nombres'][$x],1,'C',1,1);;
        $con=$con+1;
        $item++;
    }
    $pdf->ln(15);
    $pdf->SetX(20);
    
    $pdf->Cell(30,6,"Atentamente,",0,1,'L');
    $pdf->ln(15);
    $nombre = $responsable;
    $cargo = $cargo;
    $pdf->SetX(65);
    $pdf->Cell(80,6,$nombre,0,1,'C');
    $pdf->SetX(65);
    $pdf->Cell(80,6,$cargo,0,1,'C');
    if($v>=155){ 
        $pdf->AddPage();
    }
    //fin
	$pdf->Output('pdfn.pdf', 'I');