const URL ='gestion_humana/parametros_nomina/editar/hum-parametrosNomina.php';

var app = new Vue({
    el:"#myapp",
    data() {
        return {
            isLoading:false,
            isProvisiona:false,
            isSalud:false,
            isPension:false,
            isArl:false,
            isFiscales:false,
            txtConsecutivo:0,
            txtNombre:"",
            txtMax:0,
            arrFuncionamiento:[],
            arrTempFuncionamiento:[],
            arrInversion:[],
            arrTempInversion:[],
            arrComercio:[],
            arrTempComercio:[],
            arrVariables:[],
            arrSelVariables:[],
            selectVariable:0,
            selectFuncionamiento:0,
            selectTempFuncionamiento:0,
            selectInversion:0,
            selectTempInversion:0,
            selectComercio:0,
            selectTempComercio:0,
        }
    },
    mounted() {
        this.getData();
    },
    methods: {
        getData: async function(){
            let codigo = new URLSearchParams(window.location.search).get('id');
            const formData = new FormData();
            formData.append("action","get");
            formData.append("codigo",codigo);
            this.isLoading = true;
            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();
            if(objData.status){
                const data = objData.data;
                this.arrFuncionamiento = objData.conceptos.funcionamiento;
                this.arrTempFuncionamiento = objData.conceptos.temp_funcionamiento;
                this.arrInversion = objData.conceptos.inversion;
                this.arrTempInversion = objData.conceptos.temp_inversion;
                this.arrComercio = objData.conceptos.comercio;
                this.arrTempComercio = objData.conceptos.temp_comercio;
                this.arrVariables = objData.variables.filter(function(e){return e.estado=="S"});
                this.arrAllVariables = objData.variables;
                this.txtConsecutivo = data.codigo;
                this.txtNombre = data.nombre;
                this.isProvisiona = data.provision == "S" ? true : false;
                this.isFiscales = data.pparafiscal == "S" ? true : false;
                this.isPension = data.ppension == "S" ? true : false;
                this.isSalud = data.psalud == "S" ? true : false;
                this.isArl = data.parl == "S" ? true : false;
                this.selectFuncionamiento=data.funcionamiento;
                this.selectTempFuncionamiento=data.funtemporal;
                this.selectInversion=data.inversion;
                this.selectTempInversion=data.invtemporal;
                this.selectComercio=data.gastoscomerc;
                this.selectTempComercio=data.gastoscomerctemporal;
                this.arrSelVariables = data.variables;
            }else{
                codigo = objData.consecutivo-1;
                window.location.href="hum-parametrosPagoNominaEditar.php?id="+codigo
            }
            this.isLoading = false;
        },
        addVariable:function(){
            const vueContext = this;
            if(vueContext.selectVariable == 0){
                Swal.fire("Error","Debe seleccionar una variable","error");
                return false;
            }
            if(vueContext.arrSelVariables.length > 0){
                let flag = false;
                for (let i = 0; i < vueContext.arrSelVariables.length; i++) {
                    const e = vueContext.arrSelVariables[i];
                    if(e.codigo == vueContext.selectVariable){
                        flag = true;
                        break;
                    }
                }
                if(flag){
                    Swal.fire("Error","La variable ya fue agregada, intente con otra.","error");
                    return false;
                }
            }
            this.arrSelVariables.push([...this.arrVariables.filter(function(e){return e.codigo == vueContext.selectVariable})][0]);
        },
        delVariable:function(index){
            this.arrSelVariables.splice(index,1);
        },
        save: function(){
            if(this.txtNombre ==""){
                Swal.fire("Error","Todos los campos con (*) son obligatorios","error");
                return false;
            }
            const vueContext = this;
            const data = {
                nombre:this.txtNombre,
                consecutivo:this.txtConsecutivo,
                is_provisiona: this.isProvisiona ==true ? "S" : "N",
                is_salud:this.isSalud ==true ? "S" : "N",
                is_pension:this.isPension ==true ? "S" : "N",
                is_arl:this.isArl ==true ? "S" : "N",
                is_fiscales:this.isFiscales ==true ? "S" : "N",
                variables:this.arrSelVariables,
                cod_funcionamiento:this.selectFuncionamiento,
                cod_temp_funcionamiento:this.selectTempFuncionamiento,
                cod_inversion:this.selectInversion,
                cod_temp_inversion:this.selectTempInversion,
                cod_comercio:this.selectComercio,
                cod_temp_comercio:this.selectTempComercio
            }
            Swal.fire({
                title:"¿Estás segur@ de guardar?",
                text:"",
                icon: 'warning',
                showCancelButton:true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText:"Sí, guardar",
                cancelButtonText:"No, cancelar"
            }).then(async function(result){
                if(result.isConfirmed){
                    const formData = new FormData();
                    formData.append("action","save");
                    formData.append("data",JSON.stringify(data));
                    vueContext.isLoading = true;
                    const response = await fetch(URL,{method:"POST",body:formData});
                    const objData = await response.json();
                    if(objData.status){
                        Swal.fire("Guardado",objData.msg,"success");
                    }else{
                        Swal.fire("Error",objData.msg,"error");
                    }
                    vueContext.isLoading = false;
                }
            });

        },
        editItem:function(type){
            let vueContext = this;
            let id = this.txtConsecutivo
            let index = this.arrAllVariables.findIndex(function(e){return e.codigo == id});
            if(type=="next" && vueContext.arrAllVariables[++index]){
                id = this.arrAllVariables[index++].codigo;
            }else if(type=="prev" && vueContext.arrAllVariables[--index]){
                id = this.arrAllVariables[index--].codigo;
            }
            window.location.href='hum-parametrosPagoNominaEditar.php?id='+id;
        },
        iratras:function(type){
			let id = this.txtConsecutivo;
			window.location.href="hum-parametrosPagoNominaBuscar.php?id="+id;
		},
        showTab:function(tab){
            let tabs = this.$refs.rTabs.children;
            let tabsContent = this.$refs.rTabsContent.children;
            for (let i = 0; i < tabs.length; i++) {
                tabs[i].classList.remove("active");
                tabsContent[i].classList.remove("active")
            }
            tabs[tab-1].classList.add("active");
            tabsContent[tab-1].classList.add("active")
        }
    },
    computed:{

    }
})
