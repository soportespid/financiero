<?php
    require_once '../../../comun.inc';
    require '../../../funciones.inc';
    require '../../../funcionesSP.inc.php';
    /*ini_set('display_errors', '1');
    ini_set('display_startup_errors', '1');
    error_reporting(E_ALL);*/
    session_start();
    //dep($_POST);exit;
    if($_POST){
        $obj = new Plantilla();
        if($_POST['action']=="get"){
            $obj->getData();
        }else if($_POST['action']=="save"){
            $obj->save();
        }
    }

    class Plantilla{
        private $linkbd;
        private $arrData;
        public function __construct() {
            $this->linkbd = conectar_v7();
            $this->linkbd->set_charset("utf8");
        }
        public function save(){
            if(!empty($_SESSION)){
                if(empty($_POST['data'])){
                    $arrResponse = array("status"=>false,"msg"=>"Error de datos");
                }else{
                    $arrData = json_decode($_POST['data'],true);
                    if($arrData['nombre'] == "" || $arrData['consecutivo'] ==""){
                        $arrResponse = array("status"=>false,"msg"=>"Todos los campos con (*) son obligatorios.");
                    }else{
                        $arrData['nombre'] = ucwords(replaceChar(strClean($arrData['nombre'])));
                        $request = $this->updateData($arrData);
                        if(is_numeric($request) && $request> 0 ){
                            $arrResponse = array("status"=>true,"msg"=>"Datos guardados.");
                        }else if($request =="existe"){
                            $arrResponse = array("status"=>false,"msg"=>"El nombre de la variable ya existe, pruebe con otro.");
                        }else{
                            $arrResponse = array("status"=>false,"msg"=>"Ha ocurrido un error, intente de nuevo.");
                        }
                    }
                }
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
            die();
        }
        public function getData(){
            if(!empty($_SESSION)){
                $id = strClean($_POST['codigo']);
                $request = $this->selectData($id);
                if(!empty($request)){
                    $arrData = array(
                        "conceptos"=>$this->selectConceptos(),
                        "consecutivo"=>$this->selectConsecutivo(),
                        "variables"=>$this->selectVariables(),
                        "status"=>true,
                        "data"=>$request
                    );
                }else{
                    $arrData = array("status"=>false,"consecutivo"=>$this->selectConsecutivo());
                }
                echo json_encode($arrData,JSON_UNESCAPED_UNICODE);
            }
            die();
        }
        public function updateData(array $arrData){
            $arrVariables = $arrData['variables'];
            $sql = "SELECT * FROM ccpethumvariables WHERE nombre = '{$arrData['nombre']}' AND codigo!='{$arrData['consecutivo']}'";
            $request = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);

            if(empty($request)){
                $sql = "UPDATE ccpethumvariables SET nombre='{$arrData['nombre']}',
                pparafiscal = '{$arrData['is_fiscales']}', provision='{$arrData['is_provisiona']}', psalud='{$arrData['is_salud']}',
                ppension='{$arrData['is_pension']}', parl='{$arrData['is_arl']}',
                funcionamiento='{$arrData['cod_funcionamiento']}', funtemporal='{$arrData['cod_temp_funcionamiento']}',
                inversion='{$arrData['cod_inversion']}', invtemporal='{$arrData['cod_temp_inversion']}',
                gastoscomerc='{$arrData['cod_comercio']}', gastoscomerctemporal='{$arrData['cod_temp_comercio']}'
                WHERE codigo='{$arrData['consecutivo']}'";
                $request = intval(mysqli_query($this->linkbd,$sql));
                if(count($arrVariables)>0){
                    mysqli_query($this->linkbd,"DELETE FROM ccpethumvariables_pro WHERE codigo1 = '{$arrData['consecutivo']}'");
                    $totalVariables = count($arrVariables);
                    for ($i=0; $i < $totalVariables ; $i++) {
                        $sql_var = "INSERT INTO ccpethumvariables_pro (codigo1, codigo2, estado)
                        VALUES('{$arrData['consecutivo']}','{$arrVariables[$i]['codigo']}','S')";
                        mysqli_query($this->linkbd,$sql_var);
                    }
                }
            }else{
                $request = "existe";
            }
            return $request;
        }
        public function selectData($id){
            $sql = "SELECT * FROM ccpethumvariables WHERE codigo = '$id'";
            $request = mysqli_query($this->linkbd,$sql)->fetch_assoc();
            if(!empty($request)){
                $sql_det = "SELECT
                pro.codigo2 as codigo,
                cab.nombre
                FROM ccpethumvariables_pro as pro
                INNER JOIN ccpethumvariables as cab
                ON cab.codigo = pro.codigo2
                WHERE codigo1 = '$id'";
                $request['variables'] = mysqli_fetch_all(mysqli_query($this->linkbd,$sql_det),MYSQLI_ASSOC);
            }
            return $request;
        }
        public function selectConceptos(){
            $sql = "SELECT * FROM conceptoscontables WHERE modulo='2'
            AND (tipo= 'FT' OR tipo='IN' OR tipo='IT' OR tipo = 'CP' OR tipo ='CT' OR tipo = 'F') ORDER BY codigo";
            $request = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
            $arrData = array(
                "funcionamiento"=>array_values(array_filter($request,function($e){return $e['tipo']=="F";})),
                "temp_funcionamiento"=>array_values(array_filter($request,function($e){return $e['tipo']=="FT";})),
                "inversion"=>array_values(array_filter($request,function($e){return $e['tipo']=="IN";})),
                "temp_inversion"=>array_values(array_filter($request,function($e){return $e['tipo']=="IT";})),
                "comercio"=>array_values(array_filter($request,function($e){return $e['tipo']=="CP";})),
                "temp_comercio"=>array_values(array_filter($request,function($e){return $e['tipo']=="CT";}))
            );
            return $arrData;
        }
        public function selectVariables(){
            $sql = "SELECT codigo, nombre,estado FROM ccpethumvariables";
            $request = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
            return $request;
        }
        public function selectConsecutivo(){
            $sql = "SELECT MAX(RIGHT(codigo,2)) as codigo FROM ccpethumvariables";
            $request = mysqli_query($this->linkbd,$sql)->fetch_assoc()['codigo']+1;
            return $request;
        }
    }
?>
