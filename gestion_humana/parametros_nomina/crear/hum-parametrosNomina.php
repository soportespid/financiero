<?php
    require_once '../../../comun.inc';
    require '../../../funciones.inc';
    require '../../../funcionesSP.inc.php';
    /*ini_set('display_errors', '1');
    ini_set('display_startup_errors', '1');
    error_reporting(E_ALL);*/
    session_start();
    //dep($_POST);exit;
    if($_POST){
        $obj = new Plantilla();
        if($_POST['action']=="get"){
            $obj->getData();
        }else if($_POST['action']=="save"){
            $obj->save();
        }
    }

    class Plantilla{
        private $linkbd;
        private $arrData;
        public function __construct() {
            $this->linkbd = conectar_v7();
            $this->linkbd->set_charset("utf8");
        }
        public function save(){
            if(!empty($_SESSION)){
                if(empty($_POST['data'])){
                    $arrResponse = array("status"=>false,"msg"=>"Error de datos");
                }else{
                    $arrData = json_decode($_POST['data'],true);
                    if($arrData['nombre'] == "" || $arrData['consecutivo'] ==""){
                        $arrResponse = array("status"=>false,"msg"=>"Todos los campos con (*) son obligatorios.");
                    }else{
                        $arrData['nombre'] = ucwords(replaceChar(strClean($arrData['nombre'])));
                        $request = $this->insertData($arrData);
                        if(is_numeric($request) && $request> 0 ){
                            $arrResponse = array("status"=>true,"msg"=>"Datos guardados.");
                        }else if($request =="existe"){
                            $arrResponse = array("status"=>false,"msg"=>"El nombre de la variable ya existe, pruebe con otro.");
                        }else{
                            $arrResponse = array("status"=>false,"msg"=>"Ha ocurrido un error, intente de nuevo.");
                        }
                    }
                }
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
            die();
        }
        public function getData(){
            if(!empty($_SESSION)){
                $arrData = array(
                    "conceptos"=>$this->selectConceptos(),
                    "consecutivo"=>$this->selectConsecutivo(),
                    "variables"=>$this->selectVariables()
                );
                echo json_encode($arrData,JSON_UNESCAPED_UNICODE);
            }
            die();
        }
        public function insertData(array $arrData){
            $arrVariables = $arrData['variables'];
            $sql = "SELECT * FROM ccpethumvariables WHERE nombre = '{$arrData['nombre']}'";
            $request = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);

            if(empty($request)){
                $sql = "INSERT INTO ccpethumvariables (codigo, nombre, pparafiscal, estado, provision, psalud, ppension, parl,
                funcionamiento, funtemporal, inversion, invtemporal, gastoscomerc, gastoscomerctemporal)
                VALUES(
                    '{$arrData['consecutivo']}',
                    '{$arrData['nombre']}',
                    '{$arrData['is_fiscales']}',
                    'S',
                    '{$arrData['is_provisiona']}',
                    '{$arrData['is_salud']}',
                    '{$arrData['is_pension']}',
                    '{$arrData['is_arl']}',
                    '{$arrData['cod_funcionamiento']}',
                    '{$arrData['cod_temp_funcionamiento']}',
                    '{$arrData['cod_inversion']}',
                    '{$arrData['cod_temp_inversion']}',
                    '{$arrData['cod_comercio']}',
                    '{$arrData['cod_temp_comercio']}'
                )";
                $request = intval(mysqli_query($this->linkbd,$sql));
                if(count($arrVariables)>0){
                    //mysqli_query($this->linkbd,"DELETE FROM ccpethumvariables_pro WHERE codigo1 = '{$arrData['consecutivo']}'");
                    $totalVariables = count($arrVariables);
                    for ($i=0; $i < $totalVariables ; $i++) {
                        $sql_var = "INSERT INTO ccpethumvariables_pro (codigo1, codigo2, estado)
                        VALUES('{$arrData['consecutivo']}','{$arrVariables[$i]['codigo']}','S')";
                        mysqli_query($this->linkbd,$sql_var);
                    }
                }
            }else{
                $request = "existe";
            }
            return $request;
        }
        public function selectConceptos(){
            $sql = "SELECT * FROM conceptoscontables WHERE modulo='2'
            AND (tipo= 'FT' OR tipo='IN' OR tipo='IT' OR tipo = 'CP' OR tipo ='CT' OR tipo = 'F') ORDER BY codigo";
            $request = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
            $arrData = array(
                "funcionamiento"=>array_values(array_filter($request,function($e){return $e['tipo']=="F";})),
                "temp_funcionamiento"=>array_values(array_filter($request,function($e){return $e['tipo']=="FT";})),
                "inversion"=>array_values(array_filter($request,function($e){return $e['tipo']=="IN";})),
                "temp_inversion"=>array_values(array_filter($request,function($e){return $e['tipo']=="IT";})),
                "comercio"=>array_values(array_filter($request,function($e){return $e['tipo']=="CP";})),
                "temp_comercio"=>array_values(array_filter($request,function($e){return $e['tipo']=="CT";}))
            );
            return $arrData;
        }
        public function selectVariables(){
            $sql = "SELECT codigo, nombre,estado FROM ccpethumvariables WHERE estado = 'S'";
            $request = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
            return $request;
        }
        public function selectConsecutivo(){
            $sql = "SELECT MAX(RIGHT(codigo,2)) as codigo FROM ccpethumvariables";
            $request = mysqli_query($this->linkbd,$sql)->fetch_assoc()['codigo']+1;
            return $request;
        }
    }
?>
