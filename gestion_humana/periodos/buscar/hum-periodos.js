const URL ='gestion_humana/periodos/buscar/hum-periodos.php';

var app = new Vue({
    el:"#myapp",
    data() {
        return {
            isLoading:false,
            txtResults:0,
            txtSearch:"",
            arrData:[],
            arrDataCopy:[]
        }
    },
    mounted() {
        this.getData();
    },
    methods: {
        getData: async function(){
            const formData = new FormData();
            formData.append("action","get");
            this.isLoading = true;
            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();
            this.arrData = objData;
            this.arrDataCopy = objData;
            this.txtResults = this.arrData.length;
            this.isLoading = false;
        },
        search:function(){
            let search = this.txtSearch.toLowerCase();
            this.arrDataCopy = [...this.arrData.filter(e=>e.nombre.toLowerCase().includes(search)
            || e.id_periodo.toLowerCase().includes(search))];
            this.txtResults = this.arrDataCopy.length;
        },
        editItem:function(id){
            window.location.href='hum-periodosEditar.php?id='+id;
        },
        changeStatus:function(item){
            const vueThis = this;
            Swal.fire({
                title:"¿Estás segur@ de cambiar el estado?",
                text:"",
                icon: 'warning',
                showCancelButton:true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText:"Sí",
                cancelButtonText:"No, cancelar"
            }).then(async function(result){
                if(result.isConfirmed){
                    const formData = new FormData();
                    formData.append("action","change");
                    formData.append("id",item.id_periodo);
                    formData.append("estado",item.estado =='S' ? 'N' : 'S');
                    const response = await fetch(URL,{method:"POST",body:formData});
                    const objData = await response.json();
                    if(objData.status){
                        let index = vueThis.arrData.findIndex(e => e.id_periodo == item.id_periodo);
                        vueThis.arrData[index].estado = item.estado =='S' ? 'N' : 'S';
                        vueThis.arrData[index].is_status = vueThis.arrData[index].estado =='S' ? 1 : 0;
                        vueThis.search();
                        Swal.fire("Estado actualizado","","success");
                    }else{
                        Swal.fire("Error",objData.msg,"error");
                    }
                }else{
                    vueThis.search();
                }
            });

        },
    },
    computed:{

    }
})
