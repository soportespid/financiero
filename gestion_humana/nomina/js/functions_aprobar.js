const URL ='gestion_humana/nomina/controllers/AprobarController.php';
var app = new Vue({
    el:"#myapp",
    data() {
        return {
            isLoading:false,
            isModalTercero:false,
            intPageVal: "",
            txtConsecutivo:0,
            arrLiquidaciones:[],
            arrSearch:[],
            arrSearchCopy:[],
            selectLiquidacion:"",
            txtSearch:"",
            txtResultados:0,
            arrConsecutivos:[],
            txtEstado:"",
            objTercero:{codigo:"",nombre:""},
            objLiquidacion:{cdp:"",rp:"",solicitud:"",nomina:"",descripcion:"",format_solicitud:"0",format_cdp:"0",format_rp:"0",vigencia:""},
            arrTerceros:[],
            arrTercerosCopy:[],
            arrAprobados:[],
            txtSearchTercero:"",
            txtResultadosTerceros:0,
            txtFecha:new Date().toISOString().split("T")[0],
            searchFechaInicial:new Date(new Date().getFullYear(), 0, 1).toISOString().split("T")[0],
            searchFechaFinal:new Date().toISOString().split("T")[0],
            searchCodigo:"",
            searchLiquidacion:"",
            selectMovimiento:"201",
            txtDescripcionRev:"",
            txtFechaRev:new Date().toISOString().split("T")[0],
            selectRevLiquidacion: "",
        }
    },
    mounted() {
        const intPageVal = this.$refs.pageType.value;
        if(intPageVal == 2){
            this.getEdit();
        }else if(intPageVal == 3){
            this.getSearch();
        }else{
            this.getData();
        }
    },
    methods: {
        getData:async function(){
            let formData = new FormData();
            formData.append("action","get");
            this.isLoading = true;
            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();
            this.isLoading = false;
            this.arrLiquidaciones = objData.liquidaciones;
            this.arrTerceros = objData.terceros;
            this.arrTercerosCopy = objData.terceros;
            this.arrAprobados = objData.aprobados;
            this.txtResultadosTerceros = objData.terceros.length;
        },
        setLiquidacion:async function(){
            if(app.selectLiquidacion != ""){
                this.objLiquidacion = this.arrLiquidaciones.filter(function(e){return e.nomina == app.selectLiquidacion})[0];
            }else{
                this.objLiquidacion ={cdp:"",rp:"",solicitud:"",nomina:"",descripcion:"",format_solicitud:"0",format_cdp:"0",format_rp:"0",vigencia:""};
            }
        },
        getEdit:async function(){
            const codigo = new URLSearchParams(window.location.search).get('id');
            let formData = new FormData();
            formData.append("action","edit");
            formData.append("codigo",codigo);
            this.isLoading = true;
            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();
            this.isLoading = false;
            if(objData.status){
                this.txtConsecutivo = objData.data.id;
                this.objLiquidacion ={
                    cdp:objData.data.cdp,
                    rp:objData.data.rp,
                    solicitud:objData.data.solicitud,
                    nomina:objData.data.nomina,
                    descripcion:objData.data.descripcion,
                    format_solicitud:objData.data.format_solicitud,
                    format_cdp:objData.data.format_cdp,
                    format_rp:objData.data.format_rp,
                    vigencia:objData.data.vigencia,
                    estado:objData.data.estado,
                    comprobante:objData.data.comprobante,
                    reversado:objData.data.reversado
                };
                this.txtFecha = objData.data.fecha;
                this.objTercero = {codigo:objData.data.codigo,nombre:objData.data.nombre};
                this.arrConsecutivos = objData.consecutivos;
                this.txtEstado = objData.data.estado;
            }else{
                window.location.href='hum-nomina-aprobar-visualizar?id='+objData.consecutivo;
            }
        },
        getSearch:async function(){
            const formData = new FormData();
            formData.append("action","search");
            formData.append("fecha_inicial",this.searchFechaInicial);
            formData.append("fecha_final",this.searchFechaFinal);
            formData.append("codigo",this.searchCodigo);
            formData.append("liquidacion",this.searchLiquidacion);
            this.isLoading = true;
            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();
            this.isLoading = false;
            this.arrSearch = objData;
            this.arrSearchCopy = objData;
            this.txtResultados = this.arrSearchCopy.length;
        },
        selectItem:function ({...data}){
            this.objTercero = data;
            this.isModalTercero = false;
        },
        save:async function(){
            if(this.selectMovimiento == "201"){
                if(this.selectLiquidacion=="" || this.objTercero.codigo==""){
                    Swal.fire("Atención!","Todos los campos con (*) son obligatorios.","warning");
                    return false;
                }
                if(this.objLiquidacion.total_cdp != this.objLiquidacion.total_solicitud || this.objLiquidacion.total_rp != this.objLiquidacion.total_solicitud){
                    Swal.fire("Atención!","El valor de la solicitud, CDP y RP deben ser iguales.","warning");
                    return false;
                }
            }else{
                if(this.txtDescripcionRev == "" || this.selectRevLiquidacion == ""){
                    Swal.fire("Atención!","Todos los campos con (*) son obligatorios.","warning");
                    return false;
                }
            }

            Swal.fire({
                title:"¿Estás segur@ de guardar?",
                text:"",
                icon: 'warning',
                showCancelButton:true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText:"Sí, guardar",
                cancelButtonText:"No, cancelar"
            }).then(async function(result){
                if(result.isConfirmed){
                    const formData = new FormData();
                    formData.append("action","save");
                    formData.append("fecha",app.txtFecha);
                    formData.append("tercero",JSON.stringify(app.objTercero));
                    formData.append("liquidacion",JSON.stringify(app.objLiquidacion));
                    formData.append("consecutivo",app.txtConsecutivo);
                    formData.append("movimiento",app.selectMovimiento);
                    formData.append("fecha_rev",app.txtFechaRev);
                    formData.append("descripcion_rev",app.txtDescripcionRev);
                    formData.append("aprobado",app.selectRevLiquidacion);
                    if(app.arrAprobados.length > 0){
                        formData.append("nomina_rev",app.arrAprobados.filter(function(e){return e.id == app.selectRevLiquidacion})[0].nomina);
                    }
                    app.isLoading = true;
                    const response = await fetch(URL,{method:"POST",body:formData});
                    const objData = await response.json();
                    app.isLoading = false;
                    if(objData.status){
                        Swal.fire("Guardado",objData.msg,"success");
                        if(objData.id && app.txtConsecutivo == 0){
                            setTimeout(function(){
                                window.location.href='hum-nomina-aprobar-visualizar?id='+objData.id;
                            },1500);
                        }
                    }else{
                        Swal.fire("Error",objData.msg,"error");
                    }
                }
            });
        },
        search:function(type=""){
            let search = "";
            if(type=="modal_tercero"){
                search = this.txtSearchTercero.toLowerCase();
                this.arrTercerosCopy = [...this.arrTerceros.filter(e=>e.codigo.toLowerCase().includes(search) || e.nombre.toLowerCase().includes(search))];
                this.txtResultadosTerceros = this.arrTercerosCopy.length;
            }else if(type=="cod_tercero"){
                search = this.objTercero.codigo;
                const data = [...this.arrTerceros.filter(e=>e.codigo == search)];
                this.objTercero = data.length > 0 ? JSON.parse(JSON.stringify(data[0])) : {codigo:"",nombre:""};
            }
        },
        changeStatus:function(item){
            Swal.fire({
                title:"¿Estás segur@ de anular?",
                text:"",
                icon: 'warning',
                showCancelButton:true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText:"Sí",
                cancelButtonText:"No, cancelar"
            }).then(async function(result){
                if(result.isConfirmed){
                    const formData = new FormData();
                    formData.append("action","change");
                    formData.append("id",item.id);
                    formData.append("nomina",item.nomina);
                    formData.append("fecha",item.fecha_n);
                    const response = await fetch(URL,{method:"POST",body:formData});
                    const objData = await response.json();
                    if(objData.status){
                        Swal.fire("Anulado correctamente","","success");
                        app.getSearch();
                    }else{
                        Swal.fire("Error",objData.msg,"error");
                    }
                }else{
                    app.getSearch();
                }
            });

        },
        nextItem:function(type){
            let id = this.txtConsecutivo;
            let index = this.arrConsecutivos.findIndex(function(e){return e.id == id});
            if(type=="next" && app.arrConsecutivos[++index]){
                id = this.arrConsecutivos[index++].id;
            }else if(type=="prev" && app.arrConsecutivos[--index]){
                id = this.arrConsecutivos[index--].id;
            }
           window.location.href='hum-nomina-aprobar-visualizar?id='+id;
        },
        formatMoney:function(valor){
            valor = new String(valor);
            // Separar la parte entera y decimal
            const [integerPart, decimalPart] = valor.split(",");

            // Formatear la parte entera
            const formattedInteger = integerPart.replace(/\B(?=(\d{3})+(?!\d))/g, ".");
            return decimalPart !== undefined
            ? `$${formattedInteger},${decimalPart}`
            : `$${formattedInteger}`;
        },
    },
})
