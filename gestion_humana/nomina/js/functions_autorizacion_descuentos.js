const URL ='gestion_humana/nomina/controllers/AutorizacionDescuentosController.php';
var app = new Vue({
    el:"#myapp",
    data() {
        return {
            isLoading:false,
            isModalContrato:false,
            intPageVal: "",
            txtConsecutivo:0,
            txtNombre:"",
            arrSearch:[],
            arrSearchCopy:[],
            txtSearch:"",
            txtResultados:0,
            txtSearchContrato:"",
            txtResultadosContrato:0,
            arrConsecutivos:[],
            arrContratos:[],
            arrContratosCopy:[],
            arrVariablesPago:[],
            arrVariablesRet:[],
            arrVariablesSsf:[],
            arrCuotas:[],
            txtEstado:"",
            txtValor:"",
            txtValorCuota:0,
            txtCuotas:1,
            objContrato:{codigo:"",nombre:"",detalle:[]},
            txtFecha:new Date().toISOString().split("T")[0],
            txtDescripcion: "",
            txtNombreRetencion:"",
            searchFechaInicial:new Date(new Date().getFullYear(), 0, 1).toISOString().split("T")[0],
            searchFechaFinal:new Date().toISOString().split("T")[0],
            searchDocumento:"",
            searchContrato:"",
            searchDescripcion:"",
            selectModoPago:"CSF",
            selectRetencion:"",
            selectSsf:"",
        }
    },
    mounted() {
        const intPageVal = this.$refs.pageType.value;
        if(intPageVal == 2){
            this.getEdit();
        }else if(intPageVal == 3){
            this.getSearch();
        }else{
            this.getData();
        }
    },
    computed:{
        valorCuota:function() {
            return this.formatMoney(this.txtValor)
        },
    },
    methods: {
        formatMoney:function(valor){
            valor = new String(valor);
            // Separar la parte entera y decimal
            const [integerPart, decimalPart] = valor.split(",");

            // Formatear la parte entera
            const formattedInteger = integerPart.replace(/\B(?=(\d{3})+(?!\d))/g, ".");
            return decimalPart !== undefined
            ? `$${formattedInteger},${decimalPart}`
            : `$${formattedInteger}`;
        },
        getData:async function(){
            const formData = new FormData();
            formData.append("action","get");
            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();
            this.arrVariablesPago = objData.variables_pago;
            this.arrVariablesRet = objData.variables_ret;
            this.arrVariablesSsf = objData.variables_ssf;
            this.arrContratos = objData.contratos;
            this.arrContratosCopy = objData.contratos;
            this.txtResultadosContrato = this.arrContratosCopy.length;
        },
        getCuotas:function(){
            this.arrCuotas = [];
            let mesActual = new Date();
            let intCuota = Math.round(this.txtValor/this.txtCuotas);
            let totalCuotas = 0;
            const cuotas = this.txtCuotas > 1 ? (this.txtCuotas-1) : 1;
            for (let i = 0; i < cuotas; i++) {
                this.arrCuotas.push(
                    {
                        cuota:i+1,
                        valor:intCuota,
                        valor_formateado:app.formatMoney(intCuota),
                        mes:new Date(mesActual.getFullYear(), mesActual.getMonth() + i).toISOString().split("T")[0].slice(0,7),
                        tipo_pago:app.arrVariablesPago[0].codigo,
                        estado:"S",
                    }
                );
            }
            if(this.txtCuotas > 1){
                this.arrCuotas.forEach(e=>{ totalCuotas+=e.valor});
                let lastCuota = this.txtValor-totalCuotas;
                this.arrCuotas.push({
                    cuota:this.arrCuotas.length+1,
                    valor:lastCuota,
                    valor_formateado:app.formatMoney(intCuota),
                    mes:new Date(mesActual.getFullYear(), mesActual.getMonth() + this.arrCuotas.length).toISOString().split("T")[0].slice(0,7),
                    tipo_pago:app.arrVariablesPago[0].codigo,
                    estado:"S",
                });
            }
        },
        updateCuotas:function(){
            let lastCuota = 0;
            let totalCuotas = 0;
            for (let i = 0; i < this.txtCuotas; i++) {
                let valor = this.setValueCuota(app.arrCuotas[i].valor_formateado);
                this.arrCuotas[i].valor = valor;
                this.arrCuotas[i].valor_formateado = this.formatMoney(valor);
                if(i < this.txtCuotas-1){
                    totalCuotas += parseFloat(this.arrCuotas[i].valor);
                }
            }
            lastCuota = this.txtValor-totalCuotas;
            if(lastCuota < 0){
                Swal.fire("Error","La última cuota no puede ser menor o igual a cero, debe corregir.","error");
            }
            this.arrCuotas[this.txtCuotas-1].valor = lastCuota;
            this.arrCuotas[this.txtCuotas-1].valor_formateado = this.formatMoney(lastCuota);
        },
        updateRet:function(index){
            const data = this.arrCuotas[index-1];
            const arrRet = data.metodo_pago == "CSF" ? this.arrVariablesRet.filter(e=>data.variable_retencion==e.codigo) : this.arrVariablesSsf.filter(e=>data.variable_retencion==e.codigo);
            if(arrRet.length > 0){
                this.arrCuotas[index-1].nombre_retencion = arrRet[0].nombre;
            }
        },
        search:function(type=""){
            let search = "";
            if(type == "modal_contrato")search = this.txtSearchContrato.toLowerCase();
            if(type=="cod_contrato")search = this.objContrato.codigo;

            if(type=="modal_contrato"){
                this.arrContratosCopy = [...this.arrContratos.filter(e=>e.codigo.toLowerCase().includes(search) || e.nombre.toLowerCase().includes(search))];
                this.txtResultadosContrato = this.arrContratosCopy.length;
            }else if(type=="cod_contrato"){
                const data = [...this.arrContratos.filter(e=>e.codigo == search)];
                this.objContrato = data.length > 0 ? JSON.parse(JSON.stringify(data[0])) : {codigo:"",nombre:""};
            }
        },
        getEdit:async function(){
            const codigo = new URLSearchParams(window.location.search).get('id');
            let formData = new FormData();
            formData.append("action","edit");
            formData.append("codigo",codigo);
            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();
            if(objData.status){
                this.arrCuotas = [];
                this.arrVariablesPago = objData.variables_pago;
                this.arrVariablesRet = objData.variables_ret;
                this.arrVariablesSsf = objData.variables_ssf;
                this.arrContratos = objData.contratos;
                this.arrContratosCopy = objData.contratos;
                this.txtResultadosContrato = this.arrContratosCopy.length;
                this.txtFecha = objData.data.fecha;
                this.txtCuotas = objData.data.ncuotas;
                this.txtEstado = objData.data.estado;
                this.txtValor = objData.data.deuda;
                this.txtConsecutivo = objData.data.id;
                this.txtDescripcion = objData.data.descripcion;
                this.selectModoPago = objData.data.modo_pago;
                this.arrConsecutivos = objData.consecutivos;
                this.objContrato = [...this.arrContratos.filter(function(e){return e.codigo == objData.data.idfuncionario })][0];
                if(this.selectModoPago == "CSF"){
                    this.selectRetencion = objData.data.id_retencion;
                }else{
                    this.selectSsf = objData.data.id_retencion;
                }
                const detalle = objData.data.detalle;
                for (let i = 0; i < this.txtCuotas; i++) {
                    this.arrCuotas.push(
                        {
                            cuota:detalle[i].cuota,
                            valor:detalle[i].valor,
                            valor_formateado:app.formatMoney(detalle[i].valor),
                            mes:detalle[i].mes,
                            tipo_pago:detalle[i].tipo_pago,
                            estado:detalle[i].estado,
                            id_descuento:detalle[i].id_descuento,
                            id_nomina:detalle[i].id_nomina,
                            id_egreso:detalle[i].id_egreso,
                            cuenta_ptto:detalle[i].cuenta_ptto,
                        }
                    );
                }
            }else{
                window.location.href='hum-nomina-descuentos-editar?id='+objData.consecutivo;
            }
        },
        getSearch:async function(){
            const formData = new FormData();
            formData.append("action","search");
            formData.append("fecha_inicial",this.searchFechaInicial);
            formData.append("fecha_final",this.searchFechaFinal);
            formData.append("documento",this.searchDocumento);
            formData.append("contrato",this.searchContrato);
            formData.append("descripcion",this.searchDescripcion);
            this.isLoading = true;
            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();
            this.isLoading = false;
            this.arrSearch = objData;
            this.arrSearchCopy = objData;
            this.txtResultados = this.arrSearchCopy.length;
        },
        save:async function(){
            if(this.objContrato.codigo=="" || this.txtValor <=0 || this.txtCuotas <= 0 || this.txtValor =="" || this.txtCuotas == ""){
                Swal.fire("Atención!","Los campos con (*) son obligatorios.","warning");
                return false;
            }
            if(app.arrCuotas.length == 0){
                Swal.fire("Error","Debe generar las cuotas del acuerdo","error");
                return false;
            }
            if(app.arrCuotas[app.arrCuotas.length-1] <=0){
                Swal.fire("Error","Las cuotas no pueden ser menores o iguales a cero","error");
                return false;
            }
            if(this.selectModoPago == "CSF" && this.selectRetencion =="" || this.selectModoPago == "SSF" && this.selectSsf ==""){
                Swal.fire("Atención!","Debe seleccionar la variable de retención.","warning");
                return false;
            }
            Swal.fire({
                title:"¿Estás segur@ de guardar?",
                text:"",
                icon: 'warning',
                showCancelButton:true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText:"Sí, guardar",
                cancelButtonText:"No, cancelar"
            }).then(async function(result){
                if(result.isConfirmed){
                    const formData = new FormData();
                    formData.append("action","save");
                    formData.append("codigo",app.objContrato.codigo);
                    formData.append("consecutivo",app.txtConsecutivo);
                    formData.append("valor",app.txtValor);
                    formData.append("cuotas",app.txtCuotas);
                    formData.append("fecha",app.txtFecha);
                    formData.append("descripcion",app.txtDescripcion);
                    formData.append("tercero",app.objContrato.tercero);
                    formData.append("data",JSON.stringify(app.arrCuotas));
                    formData.append("modo_pago",app.selectModoPago);
                    formData.append("variable_retencion",app.selectModoPago =="CSF" ? app.selectRetencion : app.selectSsf);
                    const response = await fetch(URL,{method:"POST",body:formData});
                    const objData = await response.json();
                    if(objData.status){
                        Swal.fire("Guardado",objData.msg,"success");
                        if(objData.id && app.txtConsecutivo == 0){
                            setTimeout(function(){
                                window.location.href='hum-nomina-descuentos-editar?id='+objData.id;
                            },1500);
                        }else{
                            app.getEdit();
                        }
                    }else{
                        Swal.fire("Error",objData.msg,"error");
                    }
                }
            });
        },
        changeStatus:function(item){
            Swal.fire({
                title:"¿Estás segur@ de cambiar el estado?",
                text:"",
                icon: 'warning',
                showCancelButton:true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText:"Sí",
                cancelButtonText:"No, cancelar"
            }).then(async function(result){
                if(result.isConfirmed){
                    const formData = new FormData();
                    formData.append("action","change");
                    formData.append("id",item.id);
                    formData.append("contrato",item.idfuncionario);
                    formData.append("retencion",item.id_retencion);
                    formData.append("estado",item.estado =='S' ? 'N' : 'S');
                    const response = await fetch(URL,{method:"POST",body:formData});
                    const objData = await response.json();
                    if(objData.status){
                        Swal.fire("Estado actualizado","","success");
                        app.getSearch();
                    }else{
                        Swal.fire("Error",objData.msg,"error");
                    }
                }else{
                    app.getSearch();
                }
            });

        },
        nextItem:function(type){
            let id = this.txtConsecutivo;
            let index = this.arrConsecutivos.findIndex(function(e){return e.id == id});
            if(type=="next" && app.arrConsecutivos[++index]){
                id = this.arrConsecutivos[index++].id;
            }else if(type=="prev" && app.arrConsecutivos[--index]){
                id = this.arrConsecutivos[index--].id;
            }
           window.location.href='hum-nomina-descuentos-editar?id='+id;
        },
        setValue:function(event){
            const input = event.target.value;
            const numericValue = input.replace(/[^0-9,]/g, "");
            const parts = numericValue.split(",");
            this.txtValor = numericValue;
            if (parts.length > 2) {
                this.txtValor = parts[0] + "," + parts[1];
            } else {
                this.txtValor = numericValue;
            }
        },
        setValueCuota:function(valor){
            const numericValue = valor.replace(/[^0-9,]/g, "");
            const parts = numericValue.split(",");
            if (parts.length > 2) {
                valor = parts[0] + "," + parts[1];
            } else {
                valor = numericValue;
            }
            return valor;
        },
        setDescription:function(){
            let nombreRetencion ="";
            let data = [];
            if(this.selectModoPago == "CSF"){
                data = [...app.arrVariablesRet.filter(function(e){return e.codigo == app.selectRetencion})];
            }else{
                data = [...app.arrVariablesSsf.filter(function(e){return e.codigo == app.selectSsf})];
            }
            if(data.length > 0){
                nombreRetencion = data[0].nombre;
            }
            this.txtDescripcion = "DESCUENTO AUTORIZADO POR CONCEPTO DE "+nombreRetencion;
        },
        formatNum: function(valor){
            return new Intl.NumberFormat("en-US", {style: "currency", currency: "USD"}).format(valor);
        },
        selectItem:function ({...data}){
            this.objContrato = data;
            this.isModalContrato = false;
        },
    },
})
