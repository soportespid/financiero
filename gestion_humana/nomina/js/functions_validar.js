const URL ='gestion_humana/nomina/controllers/ValidarController.php';
var app = new Vue({
    el:"#myapp",
    data() {
        return {
            isLoading:false,
            arrLiquidaciones:[],
            arrNeto:[],
            arrDet:[],
            txtEstado:"",
            selectLiquidacion:"",
        }
    },
    mounted() {
        this.getData();
    },
    methods: {
        getData:async function(){
            const formData = new FormData();
            formData.append("action","get");
            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();
            this.arrLiquidaciones = objData;
        },
        getEdit:async function(){
            let formData = new FormData();
            formData.append("action","edit");
            formData.append("codigo",app.selectLiquidacion);
            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();
            if(objData.status){
                const detalle = objData.data.detalle;
                this.arrNeto = detalle.neto;
                this.arrDet = detalle.liquidacion;
            }else{
                Swal.fire("Error",objData.msg,"error");
            }
        },
        save:async function(){
            if(this.selectNovedad==""){
                Swal.fire("Atención!","Debe seleccionar la novedad a liquidar","warning");
                return false;
            }
            Swal.fire({
                title:"¿Estás segur@ de guardar?",
                text:"",
                icon: 'warning',
                showCancelButton:true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText:"Sí, guardar",
                cancelButtonText:"No, cancelar"
            }).then(async function(result){
                if(result.isConfirmed){
                    const formData = new FormData();
                    formData.append("action","save");
                    formData.append("novedad",JSON.stringify(app.arrNovedades.filter(function(e){return e.id == app.selectNovedad})[0]));
                    formData.append("data",JSON.stringify(app.arrLiquidacion));
                    formData.append("consecutivo",app.txtConsecutivo);
                    formData.append("fecha",app.txtFecha);
                    app.isLoading = true;
                    const response = await fetch(URL,{method:"POST",body:formData});
                    const objData = await response.json();
                    app.isLoading = false;
                    if(objData.status){
                        Swal.fire("Guardado",objData.msg,"success");
                        setTimeout(function(){
                            window.location.href='hum-nomina-liquidar-editar?id='+objData.id;
                        },1500);
                    }else{
                        Swal.fire("Error",objData.msg,"error");
                    }
                }
            });
        },
        formatMoney:function(valor){
            valor = new String(valor);
            // Separar la parte entera y decimal
            const [integerPart, decimalPart] = valor.split(",");

            // Formatear la parte entera
            const formattedInteger = integerPart.replace(/\B(?=(\d{3})+(?!\d))/g, ".");
            return decimalPart !== undefined
            ? `$${formattedInteger},${decimalPart}`
            : `$${formattedInteger}`;
        },
    },
})
