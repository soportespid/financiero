const URL ='gestion_humana/nomina/controllers/VacacionesController.php';
var app = new Vue({
    el:"#myapp",
    data() {
        return {
            isLoading:false,
            isModalContrato:false,
            intPageVal: "",
            txtConsecutivo:0,
            arrSearch:[],
            arrSearchCopy:[],
            txtSearch:"",
            txtResultados:0,
            txtSearchContrato:"",
            txtResultadosContrato:0,
            arrConsecutivos:[],
            arrContratos:[],
            arrContratosCopy:[],
            arrMeses:[],
            arrVacaciones:[],
            arrFestivos:[""],
            txtEstado:"",
            objContrato:{codigo:"",nombre:"",salario:""},
            txtFecha:new Date().toISOString().split("T")[0],
            txtFechaInicial:new Date().toISOString().split("T")[0],
            txtFechaFinal:"",
            txtResolucion:"",
            txtVigencia:new Date().getFullYear(),
            txtValor:0,
            txtDias:1,
            searchFechaInicial:new Date(new Date().getFullYear(), 0, 1).toISOString().split("T")[0],
            searchFechaFinal:new Date().toISOString().split("T")[0],
            searchCodigo:"",
            searchTercero:"",
            selectMes:1,
            checkEps:true,
            checkArl:false,
            checkPara:false,
            txtTotal:0,
        }
    },
    mounted() {
        const intPageVal = this.$refs.pageType.value;
        if(intPageVal == 2){
            this.getEdit();
        }else if(intPageVal == 3){
            this.getSearch();
        }else{
            this.getData();
        }
    },
    computed:{
        valorDato:function() {
            return this.formatMoney(this.objContrato.salario)
        },
    },
    methods: {
        formatMoney:function(valor){
            valor = new String(valor);
            // Separar la parte entera y decimal
            const [integerPart, decimalPart] = valor.split(",");

            // Formatear la parte entera
            const formattedInteger = integerPart.replace(/\B(?=(\d{3})+(?!\d))/g, ".");
            return decimalPart !== undefined
            ? `$${formattedInteger},${decimalPart}`
            : `$${formattedInteger}`;
        },
        getData:async function(){
            const formData = new FormData();
            formData.append("action","get");
            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();
            this.arrContratos = objData.contratos;
            this.arrContratosCopy = objData.contratos;
            this.txtResultadosContrato = objData.contratos.length;
            this.arrMeses = objData.meses;
        },
        search:function(type=""){
            let search = "";
            if(type == "modal_contrato")search = this.txtSearchContrato.toLowerCase();
            if(type=="cod_contrato")search = this.objContrato.codigo;
            if(type=="modal_contrato"){
                this.arrContratosCopy = [...this.arrContratos.filter(e=>e.codigo.toLowerCase().includes(search) || e.nombre.toLowerCase().includes(search)
                || e.tercero.toLowerCase().includes(search))];
                this.txtResultadosContrato = this.arrContratosCopy.length;
            }else if(type=="cod_contrato"){
                const data = [...this.arrContratos.filter(e=>e.codigo == search)];
                this.objContrato = data.length > 0 ? JSON.parse(JSON.stringify(data[0])) : {codigo:"",nombre:"",salario:""};
                this.arrVacaciones = [];
            }
        },
        getEdit:async function(){
            const codigo = new URLSearchParams(window.location.search).get('id');
            let formData = new FormData();
            formData.append("action","edit");
            formData.append("codigo",codigo);
            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();
            if(objData.status){
                const data = objData.data;
                this.txtConsecutivo = codigo;
                this.arrConsecutivos = objData.consecutivos;
                this.arrContratos = objData.contratos;
                this.arrContratosCopy = objData.contratos;
                this.txtResultadosContrato = objData.contratos.length;
                this.arrMeses = objData.meses;
                this.objContrato = data.contrato;
                this.txtEstado = data.estado;
                this.txtResolucion = data.num_resolucion;
                this.selectMes = 1;
                this.checkEps = data.paga_ibc;
                this.checkArl = data.paga_arl;
                this.checkPara = data.paga_para;
                this.txtFechaInicial = data.fecha_ini;
                this.txtFechaFinal = data.fecha_fin;
                this.calcularDias();
            }else{
                window.location.href='hum-nomina-vacaciones-editar?id='+objData.consecutivo;
            }
        },
        getSearch:async function(){
            const formData = new FormData();
            formData.append("action","search");
            formData.append("fecha_inicial",this.searchFechaInicial);
            formData.append("fecha_final",this.searchFechaFinal);
            formData.append("codigo",this.searchCodigo);
            formData.append("tercero",this.searchTercero);
            this.isLoading = true;
            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();
            this.isLoading = false;
            this.arrSearch = objData;
            this.arrSearchCopy = objData;
            this.txtResultados = this.arrSearchCopy.length;
        },
        save:async function(){
            if(app.objContrato.codigo == "" || app.objContrato.salario == "" || app.objContrato.salario <=0 || this.txtFechaFinal == "" ){
                Swal.fire("Atención!","Los campos con (*) son obligatorios.","warning");
                return false;
            }
            if(app.arrVacaciones.length == 0){
                Swal.fire("Atención!","Debe agregar al menos un detalle.","warning");
                return false;
            }
            Swal.fire({
                title:"¿Estás segur@ de guardar?",
                text:"",
                icon: 'warning',
                showCancelButton:true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText:"Sí, guardar",
                cancelButtonText:"No, cancelar"
            }).then(async function(result){
                if(result.isConfirmed){
                    const formData = new FormData();
                    formData.append("action","save");
                    formData.append("consecutivo",app.txtConsecutivo);
                    formData.append("contrato",JSON.stringify(app.objContrato));
                    formData.append("fecha",app.txtFecha);
                    formData.append("fecha_inicial",app.txtFechaInicial);
                    formData.append("fecha_final",app.txtFechaFinal);
                    formData.append("resolucion",app.txtResolucion);
                    formData.append("check_eps",app.checkEps == true? "S" : "N");
                    formData.append("check_arl",app.checkArl == true ? "S" : "N");
                    formData.append("check_para",app.checkPara == true ? "S" : "N");
                    formData.append("data",JSON.stringify(app.arrVacaciones));
                    const response = await fetch(URL,{method:"POST",body:formData});
                    const objData = await response.json();
                    if(objData.status){
                        Swal.fire("Guardado",objData.msg,"success");
                        if(app.txtConsecutivo == 0){
                            setTimeout(function(){
                                window.location.href='hum-nomina-vacaciones-editar?id='+objData.id;
                            },1500);
                        }
                    }else{
                        Swal.fire("Error",objData.msg,"error");
                    }
                }
            });
        },
        changeStatus:function(item){
            Swal.fire({
                title:"¿Estás segur@ de cambiar el estado?",
                text:"",
                icon: 'warning',
                showCancelButton:true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText:"Sí",
                cancelButtonText:"No, cancelar"
            }).then(async function(result){
                if(result.isConfirmed){
                    const formData = new FormData();
                    formData.append("action","change");
                    formData.append("id",item.num_vaca);
                    formData.append("estado",item.estado =='S' ? 'N' : 'S');
                    const response = await fetch(URL,{method:"POST",body:formData});
                    const objData = await response.json();
                    if(objData.status){
                        Swal.fire("Estado actualizado","","success");
                        app.getSearch();
                    }else{
                        Swal.fire("Error",objData.msg,"error");
                    }
                }else{
                    app.getSearch();
                }
            });

        },
        nextItem:function(type){
            let id = this.txtConsecutivo;
            let index = this.arrConsecutivos.findIndex(function(e){return e.id == id});
            if(type=="next" && app.arrConsecutivos[++index]){
                id = this.arrConsecutivos[index++].id;
            }else if(type=="prev" && app.arrConsecutivos[--index]){
                id = this.arrConsecutivos[index--].id;
            }
           window.location.href='hum-nomina-vacaciones-editar?id='+id;
        },
        setValue:function(event){
            const input = event.target.value;
            const numericValue = input.replace(/[^0-9,]/g, "");
            const parts = numericValue.split(",");
            this.objContrato.salario = numericValue;
            if (parts.length > 2) {
                this.objContrato.salario = parts[0] + "," + parts[1];
            } else {
                this.objContrato.salario = numericValue;
            }
        },
        formatNum: function(valor){
            return new Intl.NumberFormat("en-US", {style: "currency", currency: "USD"}).format(valor);
        },
        calcularDias: async function(){
            if(app.objContrato.codigo == ""){
                this.txtFechaFinal = "";
                Swal.fire("Atención!","Debe seleccionar el contrato laboral.","warning");
                return false;
            }
            if(this.txtFechaFinal < this.txtFechaInicial){
                this.txtFechaFinal = "";
                Swal.fire("Atención!","La fecha final no puede ser menor a la inicial.","warning");
                return false;
            }
            let totalDias = 0;
            let totalValor = 0;
            let valorDia = Math.ceil(app.objContrato.salario/30);
            const arrData = [];
            const [vigenciaInicial,mesInicial,diaInicial] = this.txtFechaInicial.split("-");
            const [vigenciaFinal,mesFinal,diaFinal] = this.txtFechaFinal.split("-");
            const fechaInicial = new Date(vigenciaInicial,mesInicial-1,diaInicial);
            const fechaFinal = new Date(vigenciaFinal,mesFinal-1,diaFinal);
            const responseFestivosInicial = await fetch("https://api-colombia.com/api/v1/Holiday/year/"+fechaInicial.getFullYear());
            const responseFestivosFinal = await fetch("https://api-colombia.com/api/v1/Holiday/year/"+fechaFinal.getFullYear());
            let festivosInicial = await responseFestivosInicial.json();
            let festivosFinal = await responseFestivosFinal.json();
            festivosInicial = festivosInicial.map(function(e){return new Date(e.date).toISOString().split("T")[0]});
            festivosFinal = festivosFinal.map(function(e){return new Date(e.date).toISOString().split("T")[0]});
            while (fechaInicial <= fechaFinal) {
                let mesActual = fechaInicial.getMonth()+1;
                let vigencia = fechaInicial.getFullYear();
                let dia = fechaInicial.getDay();
                let objMes = arrData.find(function(e){return e.mes ===mesActual && e.vigencia == vigencia});
                let dateString = fechaInicial.toISOString().split("T")[0];
                if(dia != 0 && !festivosInicial.includes(dateString) && !festivosInicial.includes(dateString)){
                    if(!objMes){
                        objMes = {
                            "mes":mesActual,
                            "nombre":app.arrMeses.filter(function(e){return e.id == mesActual})[0].nombre,
                            "vigencia":vigencia,
                            "dias":0,
                            "valor_dia":0,
                            "valor_total":0
                        }
                        arrData.push(objMes);
                    }
                    objMes.dias+=1;
                    objMes.valor_dia = valorDia;
                    objMes.valor_total = valorDia*objMes.dias;
                    totalDias += 1;
                }
                fechaInicial.setDate(fechaInicial.getDate()+1);
            }
            arrData.forEach(e => {totalValor+=e.valor_total});
            this.arrVacaciones = arrData;
            this.txtDias = totalDias;
            this.txtTotal = totalValor;
        },
        selectItem:function ({...data}){
            this.objContrato = data;
            this.arrVacaciones = [];
            this.isModalContrato = false;
        },
        showTab:function(tab){
            let tabs = this.$refs.rTabs.children;
            let tabsContent = this.$refs.rTabsContent.children;
            for (let i = 0; i < tabs.length; i++) {
                tabs[i].classList.remove("active");
                tabsContent[i].classList.remove("active")
            }
            tabs[tab-1].classList.add("active");
            tabsContent[tab-1].classList.add("active")
        },
    },
})
