const URL ='gestion_humana/nomina/controllers/LiquidarController.php';
const URLEXPORT ='gestion_humana/nomina/controllers/LiquidarExportController.php';
var app = new Vue({
    el:"#myapp",
    data() {
        return {
            isLoading:false,
            intPageVal: "",
            txtConsecutivo:0,
            txtMes:"",
            txtVigencia:0,
            arrSearch:[],
            arrSearchCopy:[],
            txtSearch:"",
            txtFecha:new Date().toISOString().split("T")[0],
            searchFechaInicial:new Date(new Date().getFullYear(), 0, 1).toISOString().split("T")[0],
            searchFechaFinal:new Date().toISOString().split("T")[0],
            searchCodigo:"",
            txtResultados:0,
            arrConsecutivos:[],
            arrNovedades:[],
            arrLiquidacion:[],
            arrDescuentos:[],
            arrRetenciones:[],
            arrNeto:[],
            arrParafiscales:[],
            arrInfo:[],
            objNovedad:{det:[]},
            txtEstado:"",
            selectNovedad:"",
        }
    },
    mounted() {
        const intPageVal = this.$refs.pageType.value;
        if(intPageVal == 2){
            this.getEdit();
        }else if(intPageVal == 3){
            this.getSearch();
        }else{
            this.getData();
        }
    },
    methods: {
        getEdit:async function(){
            const codigo = new URLSearchParams(window.location.search).get('id');
            let formData = new FormData();
            formData.append("action","edit");
            formData.append("codigo",codigo);
            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();
            if(objData.status){
                const detalle = objData.data.detalle;
                this.txtConsecutivo = objData.data.id;
                this.txtMes = objData.data.mes;
                this.txtFecha = objData.data.fecha;
                this.txtVigencia = objData.data.vigencia;
                this.txtEstado = objData.data.estado;
                this.arrConsecutivos = objData.consecutivos;
                this.txtEstado = objData.data.estado;
                this.arrNeto = detalle.neto;
                this.arrLiquidacion = detalle.liquidacion;
                this.arrInfo = objData.data;
            }else{
                window.location.href='hum-nomina-liquidar-editar?id='+objData.consecutivo;
            }
        },
        getSearch:async function(){
            const formData = new FormData();
            formData.append("action","search");
            formData.append("fecha_inicial",this.searchFechaInicial);
            formData.append("fecha_final",this.searchFechaFinal);
            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();
            this.arrSearch = objData;
            this.arrSearchCopy = objData;
            this.txtResultados = this.arrSearchCopy.length;
        },
        getData:async function(){
            const formData = new FormData();
            formData.append("action","get");
            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();
            this.arrNovedades = objData;
        },
        getSelectedNovedad:async function(){
            if(this.selectNovedad !=""){
                const formData = new FormData();
                formData.append("action","nov");
                formData.append("codigo",this.selectNovedad);
                const response = await fetch(URL,{method:"POST",body:formData});
                const objData = await response.json();
                this.arrLiquidacion = objData.liquidacion;
                this.arrDescuentos = objData.descuentos;
                this.arrParafiscales = objData.parafiscales;
                this.arrRetenciones = objData.retenciones;
                this.arrNeto = objData.neto;
            }
        },
        exportData:function(){
            const form = document.createElement("form");
            form.method ="post";
            form.target="_blank";
            form.action=URLEXPORT;

            function addField(name,value){
                const input = document.createElement("input");
                input.type="hidden";
                input.name=name;
                input.value = value;
                form.appendChild(input);
            }
            addField("action","excel");
            addField("fecha_inicial",this.txtFechaInicial);
            addField("fecha_final",this.txtFechaFinal);
            addField("data",JSON.stringify(this.arrInfo));
            document.body.appendChild(form);
            form.submit();
            document.body.removeChild(form);
        },
        save:async function(){
            if(this.selectNovedad==""){
                Swal.fire("Atención!","Debe seleccionar la novedad a liquidar","warning");
                return false;
            }
            Swal.fire({
                title:"¿Estás segur@ de guardar?",
                text:"",
                icon: 'warning',
                showCancelButton:true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText:"Sí, guardar",
                cancelButtonText:"No, cancelar"
            }).then(async function(result){
                if(result.isConfirmed){
                    const formData = new FormData();
                    formData.append("action","save");
                    formData.append("novedad",JSON.stringify(app.arrNovedades.filter(function(e){return e.id == app.selectNovedad})[0]));
                    formData.append("data",JSON.stringify(app.arrLiquidacion));
                    formData.append("descuentos",JSON.stringify(app.arrDescuentos));
                    formData.append("retenciones",JSON.stringify(app.arrRetenciones));
                    formData.append("consecutivo",app.txtConsecutivo);
                    formData.append("fecha",app.txtFecha);
                    app.isLoading = true;
                    const response = await fetch(URL,{method:"POST",body:formData});
                    const objData = await response.json();
                    app.isLoading = false;
                    if(objData.status){
                        Swal.fire("Guardado",objData.msg,"success");
                        setTimeout(function(){
                            window.location.href='hum-nomina-liquidar-editar?id='+objData.id;
                        },1500);
                    }else{
                        Swal.fire("Error",objData.msg,"error");
                    }
                }
            });
        },
        changeStatus:function(item){
            Swal.fire({
                title:"¿Estás segur@ de anular esta liquidación?",
                text:"Tendrás que volverla a hacer...",
                icon: 'warning',
                showCancelButton:true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText:"Sí",
                cancelButtonText:"No, cancelar"
            }).then(async function(result){
                if(result.isConfirmed){
                    const formData = new FormData();
                    formData.append("action","change");
                    formData.append("id",item.id);
                    const response = await fetch(URL,{method:"POST",body:formData});
                    const objData = await response.json();
                    if(objData.status){
                        app.getSearch();
                        Swal.fire("Liquidación anulada","","success");
                    }else{
                        Swal.fire("Error",objData.msg,"error");
                    }
                }else{
                    app.getSearch();
                }
            });

        },
        nextItem:function(type){
            let id = this.txtConsecutivo;
            let index = this.arrConsecutivos.findIndex(function(e){return e.id == id});
            if(type=="next" && app.arrConsecutivos[++index]){
                id = this.arrConsecutivos[index++].id;
            }else if(type=="prev" && app.arrConsecutivos[--index]){
                id = this.arrConsecutivos[index--].id;
            }
           window.location.href='hum-nomina-liquidar-editar?id='+id;
        },
        formatMoney:function(valor){
            valor = new String(valor);
            // Separar la parte entera y decimal
            const [integerPart, decimalPart] = valor.split(",");

            // Formatear la parte entera
            const formattedInteger = integerPart.replace(/\B(?=(\d{3})+(?!\d))/g, ".");
            return decimalPart !== undefined
            ? `$${formattedInteger},${decimalPart}`
            : `$${formattedInteger}`;
        },
        showTab:function(tab){
            let tabs = this.$refs.rTabs.children;
            let tabsContent = this.$refs.rTabsContent.children;
            for (let i = 0; i < tabs.length; i++) {
                tabs[i].classList.remove("active");
                tabsContent[i].classList.remove("active")
            }
            tabs[tab-1].classList.add("active");
            tabsContent[tab-1].classList.add("active")
        },
    },
})
