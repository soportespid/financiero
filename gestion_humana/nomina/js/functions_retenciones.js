const URL ='gestion_humana/nomina/controllers/RetencionController.php';
var app = new Vue({
    el:"#myapp",
    data() {
        return {
            isLoading:false,
            intPageVal: "",
            txtConsecutivo:0,
            txtValor:0,
            arrSearch:[],
            arrSearchCopy:[],
            txtSearch:"",
            txtFecha:new Date().toISOString().split("T")[0],
            searchFechaInicial:new Date(new Date().getFullYear(), 0, 1).toISOString().split("T")[0],
            searchFechaFinal:new Date().toISOString().split("T")[0],
            searchCodigo:"",
            txtResultados:0,
            arrConsecutivos:[],
            arrNovedades:[],
            arrData:[],
            objNovedad:{det:[]},
            txtEstado:"",
            txtNom:0,
            selectNovedad:"",
        }
    },
    mounted() {
        const intPageVal = this.$refs.pageType.value;
        if(intPageVal == 2){
            this.getEdit();
        }else if(intPageVal == 3){
            this.getSearch();
        }else{
            this.getData();
        }
    },
    methods: {
        getEdit:async function(){
            const codigo = new URLSearchParams(window.location.search).get('id');
            let formData = new FormData();
            formData.append("action","edit");
            formData.append("codigo",codigo);
            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();
            if(objData.status){
                this.arrNovedades = objData.novedades;
                this.selectNovedad = objData.data.novedad_id;
                this.txtFecha = objData.data.fecha;
                this.txtConsecutivo = objData.data.id;
                this.arrConsecutivos = objData.consecutivos;
                this.txtEstado = objData.data.estado;
                this.txtNom = objData.data.id_nom;
                this.arrData = objData.data.detalle;
                this.arrData.forEach(e => {e.retencion_formato = this.formatMoney(e.retencion);});
            }else{
                window.location.href='hum-nomina-retenciones-editar?id='+objData.consecutivo;
            }
        },
        getSearch:async function(){
            const formData = new FormData();
            formData.append("action","search");
            formData.append("fecha_inicial",this.searchFechaInicial);
            formData.append("fecha_final",this.searchFechaFinal);
            formData.append("codigo",this.searchCodigo);
            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();
            this.arrSearch = objData;
            this.arrSearchCopy = objData;
            this.txtResultados = this.arrSearchCopy.length;
        },
        getData:async function(){
            const formData = new FormData();
            formData.append("action","get");
            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();
            this.arrNovedades = objData;
        },
        getSelectedNovedad:async function(){
            if(this.selectNovedad !=""){
                const formData = new FormData();
                formData.append("action","nov");
                formData.append("codigo",this.selectNovedad);
                const response = await fetch(URL,{method:"POST",body:formData});
                const objData = await response.json();
                this.arrData = objData;
                this.arrData.forEach(e => {
                    e.retencion_formato = this.formatMoney(e.retencion);
                    e.tipo_pago = e.variables[0].codigo;
                });
            }
        },
        save:async function(){
            if(this.selectNovedad == ""){
                Swal.fire("Atención!","Todos los campos con (*) son obligatorios","warning");
                return false;
            }
            Swal.fire({
                title:"¿Estás segur@ de guardar?",
                text:"",
                icon: 'warning',
                showCancelButton:true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText:"Sí, guardar",
                cancelButtonText:"No, cancelar"
            }).then(async function(result){
                if(result.isConfirmed){
                    let total = 0;
                    app.arrData.forEach(function(e){total+=parseInt(e.retencion)});
                    const formData = new FormData();
                    formData.append("action","save");
                    formData.append("data",JSON.stringify(app.arrData));
                    formData.append("fecha",app.txtFecha);
                    formData.append("novedad",JSON.stringify(app.arrNovedades.filter(function(e){return app.selectNovedad == e.id})[0]));
                    formData.append("consecutivo",app.txtConsecutivo);
                    formData.append("total",total);
                    const response = await fetch(URL,{method:"POST",body:formData});
                    const objData = await response.json();
                    if(objData.status){
                        Swal.fire("Guardado",objData.msg,"success");
                        if(objData.id && app.txtConsecutivo == 0){
                            setTimeout(function(){
                                window.location.href='hum-nomina-retenciones-editar?id='+objData.id;
                            },1500);
                        }else{
                            app.getEdit();
                        }
                    }else{
                        Swal.fire("Error",objData.msg,"error");
                    }
                }
            });
        },
        changeStatus:function(item){
            Swal.fire({
                title:"¿Estás segur@ de cambiar el estado?",
                text:"",
                icon: 'warning',
                showCancelButton:true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText:"Sí",
                cancelButtonText:"No, cancelar"
            }).then(async function(result){
                if(result.isConfirmed){
                    const formData = new FormData();
                    formData.append("action","change");
                    formData.append("id",item.id);
                    formData.append("estado",item.estado =='S' ? 'N' : 'S');
                    const response = await fetch(URL,{method:"POST",body:formData});
                    const objData = await response.json();
                    if(objData.status){
                        app.getSearch();
                        Swal.fire("Estado actualizado","","success");
                    }else{
                        Swal.fire("Error",objData.msg,"error");
                    }
                }else{
                    app.getSearch();
                }
            });

        },
        nextItem:function(type){
            let id = this.txtConsecutivo;
            let index = this.arrConsecutivos.findIndex(function(e){return e.id == id});
            if(type=="next" && app.arrConsecutivos[++index]){
                id = this.arrConsecutivos[index++].id;
            }else if(type=="prev" && app.arrConsecutivos[--index]){
                id = this.arrConsecutivos[index--].id;
            }
           window.location.href='hum-nomina-retenciones-editar?id='+id;
        },
        formatMoney:function(valor){
            valor = new String(valor);
            // Separar la parte entera y decimal
            const [integerPart, decimalPart] = valor.split(",");

            // Formatear la parte entera
            const formattedInteger = integerPart.replace(/\B(?=(\d{3})+(?!\d))/g, ".");
            return decimalPart !== undefined
            ? `$${formattedInteger},${decimalPart}`
            : `$${formattedInteger}`;
        },
        updateRetencion:function(){
            const data = [...this.arrData];
            for (let i = 0; i < data.length; i++) {
                const e = this.arrData[i];
                let retencion = parseInt(this.setValueElement(e.retencion_formato));
                e.retencion = retencion;
                e.retencion_formato = this.formatMoney(retencion);
                data[i] = e;
            }
            this.arrData = data;
        },
        setValueElement:function(valor){
            const numericValue = valor.replace(/[^0-9,]/g, "");
            const parts = numericValue.split(",");
            if (parts.length > 2) {
                valor = parts[0] + "," + parts[1];
            } else {
                valor = numericValue;
            }
            return valor;
        },
        showTab:function(tab){
            let tabs = this.$refs.rTabs.children;
            let tabsContent = this.$refs.rTabsContent.children;
            for (let i = 0; i < tabs.length; i++) {
                tabs[i].classList.remove("active");
                tabsContent[i].classList.remove("active")
            }
            tabs[tab-1].classList.add("active");
            tabsContent[tab-1].classList.add("active")
        },
    },
})
