const URL ='gestion_humana/nomina/controllers/NovedadesController.php';
var app = new Vue({
    el:"#myapp",
    data() {
        return {
            isLoading:false,
            isModalContrato:false,
            intPageVal: "",
            txtConsecutivo:0,
            arrSearch:[],
            arrSearchCopy:[],
            txtSearch:"",
            txtResultados:0,
            txtSearchContrato:"",
            txtResultadosContrato:0,
            arrConsecutivos:[],
            arrContratos:[],
            arrContratosCopy:[],
            arrMeses:[
                { id: 1, nombre: 'Enero' },
                { id: 2, nombre: 'Febrero' },
                { id: 3, nombre: 'Marzo' },
                { id: 4, nombre: 'Abril' },
                { id: 5, nombre: 'Mayo' },
                { id: 6, nombre: 'Junio' },
                { id: 7, nombre: 'Julio' },
                { id: 8, nombre: 'Agosto' },
                { id: 9, nombre: 'Septiembre' },
                { id: 10, nombre: 'Octubre' },
                { id: 11, nombre: 'Noviembre' },
                { id: 12, nombre: 'Diciembre' }
            ],
            txtEstado:"",
            objContrato:{codigo:"",nombre:""},
            txtAnio: new Date().getFullYear(),
            txtFecha:new Date().toISOString().split("T")[0],
            txtDescripcion: "",
            searchFechaInicial:new Date(new Date().getFullYear(), 0, 1).toISOString().split("T")[0],
            searchFechaFinal:new Date().toISOString().split("T")[0],
            arrVariablesPago:[{detalle:[]},{detalle:[]},{detalle:[]},],
            arrSelectVariablesPago:[],
            searchCodigo:"",
            selectMes:1,
            selectTipoPago:"",
            tipoPago:"01",
            tipoParam:"",
            txtHoraMensual:0,
            isCheckAll:false,
            arrEmpleadosExtras:[],
            txtTotalExtra:0,
            txtAuxTransporte:0,
            txtAuxAlimentacion:0,
            txtSalarioMinimo:0,
            txtPrenomina:0,
            indexOtros:0,
            arrVigencias:[],
            selectVigencia:0,
        }
    },
    mounted() {
        const intPageVal = this.$refs.pageType.value;
        if(intPageVal == 2){
            this.initData();
            this.getEdit();
        }else if(intPageVal == 3){
            this.getSearch();
        }else{
            this.initData();
        }
    },
    methods: {

        formatMoney:function(valor){
            valor = new String(valor);
            // Separar la parte entera y decimal
            const [integerPart, decimalPart] = valor.split(",");

            // Formatear la parte entera
            const formattedInteger = integerPart.replace(/\B(?=(\d{3})+(?!\d))/g, ".");
            return decimalPart !== undefined
            ? `$${formattedInteger},${decimalPart}`
            : `$${formattedInteger}`;
        },
        initData:function(){
            for (let i = 2019; i <= new Date().getFullYear(); i++) {
                this.arrVigencias.unshift(i);
            }
            this.selectMes = new Date().getMonth()+1;
            this.selectVigencia = this.arrVigencias[0];
        },
        getData:async function(){
            const formData = new FormData();
            formData.append("action","get");
            formData.append("anio",this.selectVigencia);
            formData.append("mes",this.selectMes);
            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();
            this.arrVariablesPago = objData.variables;
            this.selectTipoPago = "03";
            this.arrContratos = objData.contratos;
            this.arrContratosCopy = objData.contratos;
            this.txtResultadosContrato = objData.contratos.length;
            this.txtHoraMensual = objData.hora;
            this.txtAuxAlimentacion = objData.generales.alimentacion;
            this.txtAuxTransporte = objData.generales.transporte;
            this.txtSalarioMinimo = objData.generales.salario;

        },
        search:function(type=""){
            let search = "";
            if(type == "modal_contrato")search = this.txtSearchContrato.toLowerCase();
            if(type=="modal_contrato"){
                this.arrContratosCopy = [...this.arrContratos.filter(e=>e.codigo.toLowerCase().includes(search) || e.nombre.toLowerCase().includes(search)
                    || e.tercero.toLowerCase().includes(search))];
                this.txtResultadosContrato = this.arrContratosCopy.length;
            }
        },
        getEdit:async function(){
            const codigo = new URLSearchParams(window.location.search).get('id');
            let formData = new FormData();
            formData.append("action","edit");
            formData.append("codigo",codigo);
            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();
            if(objData.status){
                const data = objData.data;
                this.arrConsecutivos = objData.consecutivos;
                this.txtConsecutivo = codigo;
                this.arrVariablesPago = objData.variables;
                this.selectTipoPago = "03";
                this.arrContratos = objData.contratos;
                this.arrContratosCopy = objData.contratos;
                this.txtResultadosContrato = objData.contratos.length;
                this.arrMeses = objData.meses;
                this.txtHoraMensual = objData.hora;
                this.txtAuxAlimentacion = objData.generales.alimentacion;
                this.txtAuxTransporte = objData.generales.transporte;
                this.txtSalarioMinimo = objData.generales.salario;
                this.txtHoraMensual = objData.hora;
                this.txtDescripcion = data.descripcion;
                this.txtFecha = data.fecha;
                this.selectMes = data.mes;
                this.selectVigencia = data.vigencia;
                this.txtEstado = data.estado;
                this.txtPrenomina = data.prenomina;
            }else{
                window.location.href='hum-nomina-novedades-editar?id='+objData.consecutivo;
            }
        },
        getSearch:async function(){
            const formData = new FormData();
            formData.append("action","search");
            formData.append("fecha_inicial",this.searchFechaInicial);
            formData.append("fecha_final",this.searchFechaFinal);
            formData.append("codigo",this.searchCodigo);
            this.isLoading = true;
            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();
            this.isLoading = false;
            this.arrSearch = objData;
            this.arrSearchCopy = objData;
            this.txtResultados = this.arrSearchCopy.length;
        },
        changeStatusItem:function(codigo,type="contratos"){
            if(type=="contratos"){
                const index = this.arrContratos.findIndex(e=>e.codigo == codigo);
                this.arrContratos[index].is_checked = !this.arrContratos[index].is_checked;
                const flag = this.arrContratos.every((e)=>e.is_checked == true);
                this.isCheckAll = flag;
            }else if("sueldo"){
                const arrCabPago = this.arrVariablesPago[app.indexOtros];
                const arrDetPago =  arrCabPago.detalle;
                const index = arrDetPago.findIndex(e=>e.codigo == codigo);
                arrDetPago[index].is_checked_salud_empleado= app.tipoParam == "SF" ? !arrDetPago[index].is_checked_salud_empleado : arrDetPago[index].is_checked_salud_empleado;
                arrDetPago[index].is_checked_salud_empresa=app.tipoParam == "SE" ? !arrDetPago[index].is_checked_salud_empresa : arrDetPago[index].is_checked_salud_empresa;
                arrDetPago[index].is_checked_pension_empleado=app.tipoParam == "PF" ? !arrDetPago[index].is_checked_pension_empleado : arrDetPago[index].is_checked_pension_empleado;
                arrDetPago[index].is_checked_pension_empresa=app.tipoParam == "PE" ? !arrDetPago[index].is_checked_pension_empresa : arrDetPago[index].is_checked_pension_empresa;
                arrDetPago[index].is_checked_arl=app.tipoParam == "ARL" ? !arrDetPago[index].is_checked_arl : arrDetPago[index].is_checked_arl;
                arrDetPago[index].is_checked_ccf=app.tipoParam == "CCF" ? !arrDetPago[index].is_checked_ccf : arrDetPago[index].is_checked_ccf;
                arrDetPago[index].is_checked_icbf=app.tipoParam == "ICBF" ? !arrDetPago[index].is_checked_icbf : arrDetPago[index].is_checked_icbf;
                arrDetPago[index].is_checked_sena=app.tipoParam == "SENA" ? !arrDetPago[index].is_checked_sena : arrDetPago[index].is_checked_sena;
                arrDetPago[index].is_checked_inst_tecnico=app.tipoParam == "INS" ? !arrDetPago[index].is_checked_inst_tecnico : arrDetPago[index].is_checked_inst_tecnico;
                arrDetPago[index].is_checked_esap=app.tipoParam == "ESAP" ? !arrDetPago[index].is_checked_esap : arrDetPago[index].is_checked_esap;

                arrCabPago.is_checked_salud_empleado = arrDetPago.every((e)=>e.is_checked_salud_empleado == true);
                arrCabPago.is_checked_salud_empresa = arrDetPago.every((e)=>e.is_checked_salud_empresa == true);
                arrCabPago.is_checked_pension_empleado = arrDetPago.every((e)=>e.is_checked_pension_empleado == true);
                arrCabPago.is_checked_pension_empresa = arrDetPago.every((e)=>e.is_checked_pension_empresa == true);
                arrCabPago.is_checked_arl = arrDetPago.every((e)=>e.is_checked_arl == true);
                arrCabPago.is_checked_ccf = arrDetPago.every((e)=>e.is_checked_ccf == true);
                arrCabPago.is_checked_icbf = arrDetPago.every((e)=>e.is_checked_icbf == true);
                arrCabPago.is_checked_sena = arrDetPago.every((e)=>e.is_checked_sena == true);
                arrCabPago.is_checked_inst_tecnico = arrDetPago.every((e)=>e.is_checked_inst_tecnico == true);
                arrCabPago.is_checked_esap = arrDetPago.every((e)=>e.is_checked_esap == true);
                arrCabPago.detalle = arrDetPago;

                this.arrVariablesPago[app.indexOtros] = arrCabPago;
            }
        },
        changeAll:function(type="contratos"){
            if(type=="contratos"){
                this.arrContratos.forEach(e => {
                    e.is_checked = this.isCheckAll;
                });
            }else{
                const arrCabPago = this.arrVariablesPago[app.indexOtros];
                const arrDetPago =  arrCabPago.detalle;
                arrDetPago.forEach(e => {
                    if(app.tipoParam == "SF"){e.is_checked_salud_empleado = arrCabPago.is_checked_salud_empleado;}
                    else if(app.tipoParam == "SE"){e.is_checked_salud_empresa = arrCabPago.is_checked_salud_empresa;}
                    else if(app.tipoParam == "PF"){e.is_checked_pension_empleado = arrCabPago.is_checked_pension_empleado;}
                    else if(app.tipoParam == "PE"){e.is_checked_pension_empresa = arrCabPago.is_checked_pension_empresa;}
                    else if(app.tipoParam == "ARL"){e.is_checked_arl = arrCabPago.is_checked_arl;}
                    else if(app.tipoParam == "CCF"){e.is_checked_ccf = arrCabPago.is_checked_ccf;}
                    else if(app.tipoParam == "ICBF"){e.is_checked_icbf = arrCabPago.is_checked_icbf;}
                    else if(app.tipoParam == "SENA"){e.is_checked_sena = arrCabPago.is_checked_sena;}
                    else if(app.tipoParam == "INS"){e.is_checked_inst_tecnico = arrCabPago.is_checked_inst_tecnico;}
                    else if(app.tipoParam == "ESAP"){e.is_checked_esap = arrCabPago.is_checked_esap;}
                });
                arrCabPago.detalle = arrDetPago;
                this.arrVariablesPago[app.indexOtros].detalle = arrDetPago;
            }
        },
        save:async function(){
            if(this.txtAnio == "" || this.txtDescripcion == ""){
                Swal.fire("Atención!","Los campos con (*) son obligatorios.","warning");
                return false;
            }
            let flag = app.arrVariablesPago.every(e=>e.detalle.length == 0);
            if(flag){
                Swal.fire("Atención!","Debe configurar al menos una novedad.","warning");
                return false;
            }
            const data = app.arrVariablesPago.filter(function(e){return e.detalle.length > 0});
            Swal.fire({
                title:"¿Estás segur@ de guardar?",
                text:"",
                icon: 'warning',
                showCancelButton:true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText:"Sí, guardar",
                cancelButtonText:"No, cancelar"
            }).then(async function(result){
                if(result.isConfirmed){
                    const formData = new FormData();
                    formData.append("action","save");
                    formData.append("consecutivo",app.txtConsecutivo);
                    formData.append("anio",app.selectVigencia);
                    formData.append("mes",app.selectMes);
                    formData.append("fecha",app.txtFecha);
                    formData.append("descripcion",app.txtDescripcion);
                    formData.append("data",JSON.stringify(data));
                    app.isLoading = true;
                    const response = await fetch(URL,{method:"POST",body:formData});
                    const objData = await response.json();
                    app.isLoading = false;
                    if(objData.status){
                        Swal.fire("Guardado",objData.msg,"success");
                        if(objData.id && app.txtConsecutivo == 0){
                            setTimeout(function(){
                                window.location.href='hum-nomina-novedades-editar?id='+objData.id;
                            },1500);
                        }
                    }else{
                        Swal.fire("Error",objData.msg,"error");
                    }
                }
            });
        },
        changeStatus:function(item){
            Swal.fire({
                title:"¿Estás segur@ de cambiar el estado?",
                text:"",
                icon: 'warning',
                showCancelButton:true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText:"Sí",
                cancelButtonText:"No, cancelar"
            }).then(async function(result){
                if(result.isConfirmed){
                    const formData = new FormData();
                    formData.append("action","change");
                    formData.append("id",item.id);
                    formData.append("estado",item.estado =='S' ? 'N' : 'S');
                    const response = await fetch(URL,{method:"POST",body:formData});
                    const objData = await response.json();
                    if(objData.status){
                        Swal.fire("Estado actualizado","","success");
                        app.getSearch();
                    }else{
                        Swal.fire("Error",objData.msg,"error");
                    }
                }else{
                    app.getSearch();
                }
            });

        },
        nextItem:function(type){
            let id = this.txtConsecutivo;
            let index = this.arrConsecutivos.findIndex(function(e){return e.id == id});
            if(type=="next" && app.arrConsecutivos[++index]){
                id = this.arrConsecutivos[index++].id;
            }else if(type=="prev" && app.arrConsecutivos[--index]){
                id = this.arrConsecutivos[index--].id;
            }
           window.location.href='hum-nomina-novedades-editar?id='+id;
        },
        setValueElement:function(valor){
            const numericValue = valor.replace(/[^0-9,]/g, "");
            const parts = numericValue.split(",");
            if (parts.length > 2) {
                valor = parts[0] + "," + parts[1];
            } else {
                valor = numericValue;
            }
            return valor;
        },
        updateValores:function(){
            this.txtTotalExtra = 0;
            const arrDetPago =  this.arrVariablesPago[app.indexOtros].detalle;
            for (let i = 0; i < arrDetPago.length; i++) {
                const e = arrDetPago[i];
                let vacaciones = parseInt(e.dias_vacaciones);
                let incapacidad = parseInt(e.dias_incapacidad);
                let diasLaborados = parseInt(e.dias)-(vacaciones+incapacidad);

                let valor_ordinaria_diurna= app.setValueElement(e.valor_formateado_ordinaria_diurna);
                let valor_ordinaria_nocturna= app.setValueElement(e.valor_formateado_ordinaria_nocturna);
                let valor_dominical_diurna= app.setValueElement(e.valor_formateado_dominical_diurna);
                let valor_dominical_nocturna= app.setValueElement(e.valor_formateado_dominical_nocturna);
                let valor_recargo_nocturno= app.setValueElement(e.valor_formateado_recargo_nocturno);
                let valor_recargo_dominical_diurno= app.setValueElement(e.valor_formateado_recargo_dominical_diurno);
                let valor_recargo_dominical_nocturno = app.setValueElement(e.valor_formateado_recargo_dominical_nocturno);
                let salario = app.setValueElement(e.salario_formateado);
                let valorOtros = app.setValueElement(e.valor_otros_formateado);
                let valorHora = Math.round(salario/app.txtHoraMensual);
                let valorDias = Math.round((salario/e.dias_original)*diasLaborados);

                valor_ordinaria_diurna = parseInt((valorHora*1.25) * parseInt(e.ordinaria_diurna));
                valor_ordinaria_nocturna = parseInt((valorHora*1.75) * parseInt(e.ordinaria_nocturna));
                valor_dominical_diurna = parseInt((valorHora*2) * parseInt(e.dominical_diurna));
                valor_dominical_nocturna = parseInt((valorHora*2.5) * parseInt(e.dominical_nocturna));
                valor_recargo_nocturno = parseInt((valorHora*1.35) * parseInt(e.recargo_nocturno));
                valor_recargo_dominical_diurno = parseInt((valorHora*1.75) * parseInt(e.recargo_dominical_diurno));
                valor_recargo_dominical_nocturno = parseInt((valorHora*2.1) * parseInt(e.recargo_dominical_nocturno));

                let totalExtra = valor_ordinaria_diurna+valor_ordinaria_nocturna+valor_dominical_diurna+valor_dominical_nocturna;
                let totalRecargo = valor_recargo_nocturno+valor_recargo_dominical_diurno+valor_recargo_dominical_nocturno;
                let total = totalExtra+totalRecargo;
                let transporte = 0;
                let alimentacion = 0;
                if(salario <= app.txtSalarioMinimo*2){
                    transporte = Math.round((app.txtAuxTransporte/e.dias_original)*diasLaborados);
                    alimentacion = Math.round((app.txtAuxAlimentacion/e.dias_original)*diasLaborados);
                }

                e.transporte = transporte;
                e.alimentacion = alimentacion;
                e.valor_otros = valorOtros;
                e.salario = salario;
                e.valor_hora = valorHora;
                e.valor_dias = valorDias;
                e.total_extra = totalExtra;
                e.total_recargo = totalRecargo;
                e.total = total;
                e.dias_laborados = diasLaborados

                e.valor_otros_formateado = this.formatMoney(valorOtros);
                e.valor_formateado_ordinaria_diurna= this.formatMoney(valor_ordinaria_diurna)
                e.valor_formateado_ordinaria_nocturna= this.formatMoney(valor_ordinaria_nocturna)
                e.valor_formateado_dominical_diurna= this.formatMoney(valor_dominical_diurna)
                e.valor_formateado_dominical_nocturna= this.formatMoney(valor_dominical_nocturna)
                e.valor_formateado_recargo_nocturno= this.formatMoney(valor_recargo_nocturno)
                e.valor_formateado_recargo_dominical_diurno= this.formatMoney(valor_recargo_dominical_diurno)
                e.valor_formateado_recargo_dominical_nocturno= this.formatMoney(valor_recargo_dominical_nocturno)

                e.salario_formateado = this.formatMoney(salario);
                e.valor_hora_formateado = this.formatMoney(valorHora);
                e.total_extra_formateado= this.formatMoney(totalExtra)
                e.total_recargo_formateado= this.formatMoney(totalRecargo)
                e.total_formateado= this.formatMoney(total)
                arrDetPago[i] = e;
                app.txtTotalExtra+= total;
            }
            this.arrVariablesPago[app.indexOtros].detalle = arrDetPago;
        },
        changeTipoPago(){
            this.tipoPago = this.selectTipoPago;
            const indexPago = this.arrVariablesPago.findIndex(function(e){return e.codigo == app.tipoPago});
            this.indexOtros = indexPago;
        },
        generarEmpleados(){
            if(this.tipoPago != ""){
                const arrPago = this.arrVariablesPago[app.indexOtros];
                const arrDetPago =  this.arrVariablesPago[app.indexOtros].detalle;
                if(arrDetPago.length > 0){
                    const arrFilter = this.arrContratos.filter(function(e){return e.is_checked});
                    for (let i = 0; i < arrFilter.length; i++) {
                        const e = arrFilter[i];
                        let arrInArray = arrDetPago.filter(function(f){return f.codigo == e.codigo});
                        if(arrInArray.length == 0){
                            let transporte = 0;
                            let alimentacion = 0;
                            let vacaciones = parseInt(e.dias_vacaciones);
                            let incapacidad = parseInt(e.dias_incapacidad);
                            let diasLaborados = parseInt(e.dias)-(vacaciones+incapacidad);
                            if(e.salario <= app.txtSalarioMinimo*2){
                                transporte = Math.round((app.txtAuxTransporte/e.dias)*diasLaborados);
                                alimentacion = Math.round((app.txtAuxAlimentacion/e.dias)*diasLaborados);
                            }
                            arrDetPago.push({
                                transporte:transporte,
                                alimentacion:alimentacion,
                                dias:e.dias,
                                dias_original:e.dias,
                                dias_laborados:diasLaborados,
                                dias_vacaciones:e.dias_vacaciones,
                                dias_incapacidad:e.dias_incapacidad,
                                valor_dias:Math.round((e.salario/e.dias)*diasLaborados),
                                codigo:e.codigo,
                                nombre:e.nombre,
                                documento:e.tercero,
                                salario:e.salario,
                                salario_formateado:this.formatMoney(e.salario),
                                valor_hora_formateado:this.formatMoney(Math.round(e.salario/app.txtHoraMensual)),
                                valor_hora:Math.round(e.salario/app.txtHoraMensual),
                                valor_otros:0,
                                valor_otros_formateado:this.formatMoney(0),
                                ordinaria_diurna:0,
                                ordinaria_nocturna:0,
                                dominical_diurna:0,
                                dominical_nocturna:0,
                                recargo_nocturno:0,
                                recargo_dominical_diurno:0,
                                recargo_dominical_nocturno:0,
                                valor_ordinaria_diurna:0,
                                valor_ordinaria_nocturna:0,
                                valor_dominical_diurna:0,
                                valor_dominical_nocturna:0,
                                valor_recargo_nocturno:0,
                                valor_recargo_dominical_diurno:0,
                                valor_recargo_dominical_nocturno:0,
                                valor_formateado_ordinaria_diurna:this.formatMoney(0),
                                valor_formateado_ordinaria_nocturna:this.formatMoney(0),
                                valor_formateado_dominical_diurna:this.formatMoney(0),
                                valor_formateado_dominical_nocturna:this.formatMoney(0),
                                valor_formateado_recargo_nocturno:this.formatMoney(0),
                                valor_formateado_recargo_dominical_diurno:this.formatMoney(0),
                                valor_formateado_recargo_dominical_nocturno:this.formatMoney(0),
                                total_extra:0,
                                total_recargo:0,
                                total:0,
                                total_extra_formateado:0,
                                total_recargo_formateado:0,
                                total_formateado:0,
                                is_checked_salud_empleado:arrPago.is_checked_salud_empleado,
                                is_checked_salud_empresa:arrPago.is_checked_salud_empresa,
                                is_checked_pension_empleado:arrPago.is_checked_pension_empleado,
                                is_checked_pension_empresa:arrPago.is_checked_pension_empresa,
                                is_checked_arl:arrPago.is_checked_arl,
                                is_checked_ccf:arrPago.is_checked_ccf,
                                is_checked_icbf:arrPago.is_checked_icbf,
                                is_checked_sena:arrPago.is_checked_sena,
                                is_checked_inst_tecnico:arrPago.is_checked_inst_tecnico,
                                is_checked_esap:arrPago.is_checked_esap,
                            });
                        }
                    }
                }else{
                    const arrFilter = this.arrContratos.filter(function(e){return e.is_checked});
                    arrFilter.forEach(e => {
                        if(e.is_checked){
                            let transporte = 0;
                            let alimentacion = 0;
                            let vacaciones = parseInt(e.dias_vacaciones);
                            let incapacidad = parseInt(e.dias_incapacidad);
                            let diasLaborados = parseInt(e.dias)-(vacaciones+incapacidad);
                            if(e.salario <= app.txtSalarioMinimo*2){
                                transporte = Math.round((app.txtAuxTransporte/e.dias)*diasLaborados);
                                alimentacion = Math.round((app.txtAuxAlimentacion/e.dias)*diasLaborados);
                            }
                            arrDetPago.push({
                                transporte:transporte,
                                alimentacion:alimentacion,
                                dias:e.dias,
                                dias_original:e.dias,
                                dias_laborados:diasLaborados,
                                dias_vacaciones:e.dias_vacaciones,
                                dias_incapacidad:e.dias_incapacidad,
                                valor_dias:Math.round((e.salario/e.dias)*diasLaborados),
                                codigo:e.codigo,
                                nombre:e.nombre,
                                documento:e.tercero,
                                salario:e.salario,
                                salario_formateado:this.formatMoney(e.salario),
                                valor_hora_formateado:this.formatMoney(Math.round(e.salario/app.txtHoraMensual)),
                                valor_hora:Math.round(e.salario/app.txtHoraMensual),
                                valor_otros:0,
                                valor_otros_formateado:this.formatMoney(0),
                                ordinaria_diurna:0,
                                ordinaria_nocturna:0,
                                dominical_diurna:0,
                                dominical_nocturna:0,
                                recargo_nocturno:0,
                                recargo_dominical_diurno:0,
                                recargo_dominical_nocturno:0,
                                valor_ordinaria_diurna:0,
                                valor_ordinaria_nocturna:0,
                                valor_dominical_diurna:0,
                                valor_dominical_nocturna:0,
                                valor_recargo_nocturno:0,
                                valor_recargo_dominical_diurno:0,
                                valor_recargo_dominical_nocturno:0,
                                valor_formateado_ordinaria_diurna:this.formatMoney(0),
                                valor_formateado_ordinaria_nocturna:this.formatMoney(0),
                                valor_formateado_dominical_diurna:this.formatMoney(0),
                                valor_formateado_dominical_nocturna:this.formatMoney(0),
                                valor_formateado_recargo_nocturno:this.formatMoney(0),
                                valor_formateado_recargo_dominical_diurno:this.formatMoney(0),
                                valor_formateado_recargo_dominical_nocturno:this.formatMoney(0),
                                total_extra:0,
                                total_recargo:0,
                                total:0,
                                total_extra_formateado:0,
                                total_recargo_formateado:0,
                                total_formateado:0,
                                is_checked_salud_empleado:arrPago.is_checked_salud_empleado,
                                is_checked_salud_empresa:arrPago.is_checked_salud_empresa,
                                is_checked_pension_empleado:arrPago.is_checked_pension_empleado,
                                is_checked_pension_empresa:arrPago.is_checked_pension_empresa,
                                is_checked_arl:arrPago.is_checked_arl,
                                is_checked_ccf:arrPago.is_checked_ccf,
                                is_checked_icbf:arrPago.is_checked_icbf,
                                is_checked_sena:arrPago.is_checked_sena,
                                is_checked_inst_tecnico:arrPago.is_checked_inst_tecnico,
                                is_checked_esap:arrPago.is_checked_esap,
                            });
                        }
                    });

                }
                this.arrVariablesPago[app.indexOtros].detalle = arrDetPago;
            }
            this.isModalContrato = false;
        },
        formatNum: function(valor){
            return new Intl.NumberFormat("en-US", {style: "currency", currency: "USD"}).format(valor);
        },
        delItem:function(index=""){
            if(index!="" && index >= 0){
                const arrCabPago = this.arrVariablesPago[app.indexOtros];
                arrCabPago.detalle.splice(index,1);
                this.arrVariablesPago[app.indexOtros] = arrCabPago;
            }else{
                this.arrVariablesPago[app.indexOtros].detalle = [];
            }
        },
        selectItem:function ({...data}){
            this.objContrato = data;
            this.isModalContrato = false;
        },
        showTab:function(tab){
            let tabs = this.$refs.rTabs.children;
            let tabsContent = this.$refs.rTabsContent.children;
            for (let i = 0; i < tabs.length; i++) {
                tabs[i].classList.remove("active");
                tabsContent[i].classList.remove("active")
            }
            tabs[tab-1].classList.add("active");
            tabsContent[tab-1].classList.add("active")
        },
    },
})
