<?php
    require_once '../../../Librerias/core/Helpers.php';
    require_once '../../../PHPExcel/Classes/PHPExcel.php';
    require_once '../models/AutorizacionDescuentosModel.php';
    session_start();

    class AutorizacionDescuentosController extends AutorizacionDescuentosModel{
        public function getData(){
            if(!empty($_SESSION)){
                $arrResponse = [
                    "variables_pago"=>$this->selectVariablesPago(),
                    "variables_ret"=>$this->selectVariablesRetenciones(),
                    "variables_ssf"=>$this->selectVariablesSsf(),
                    "contratos"=>$this->selectContratos(),
                ];
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
        }
        public function save(){
            if(!empty($_SESSION)){
                $arrData = json_decode($_POST['data'],true);
                if(empty($_POST['codigo']) || empty($_POST['cuotas']) || empty($_POST['valor']) || empty($arrData)
                || empty($_POST['descripcion']) || empty($_POST['modo_pago']) || empty($_POST['variable_retencion'])){
                    $arrResponse = array("status"=>false,"msg"=>"Error de datos");
                }else{
                    $strFecha = strClean($_POST['fecha']);
                    $strTercero = strClean($_POST['tercero']);
                    $intContrato = intval($_POST['codigo']);
                    $intValorDeuda = floatval($_POST['valor']);
                    $intCuotas = intval($_POST['cuotas']);
                    $intConsecutivo = intval($_POST['consecutivo']);
                    $strDescripcion = replaceChar(strClean($_POST['descripcion']));
                    $strModoPago = strClean($_POST['modo_pago']);
                    $strVariableRetencion = strClean($_POST['variable_retencion']);

                    $opcion="";
                    if($intConsecutivo == 0){
                        $opcion = 1;

                        $request = $this->insertData(
                            $strDescripcion,
                            $strFecha,
                            $intContrato,
                            $intValorDeuda,
                            $intCuotas,
                            $strModoPago,
                            $strVariableRetencion,
                            $strTercero
                        );
                        $id = $request;
                    }else{
                        $opcion = 2;
                        $id = $intConsecutivo;
                        $request = $this->updateData(
                            $intConsecutivo,
                            $strDescripcion,
                            $strFecha,
                            $intContrato,
                            $intValorDeuda,
                            $intCuotas,
                            $strModoPago,
                            $strVariableRetencion,
                            $strTercero
                        );
                    }
                    if($request > 0){
                        $this->insertDet($id,$arrData);
                        if($opcion == 1){
                            insertAuditoria("hum_auditoria","hum_funciones_id",5,"Crear",$request,"Descuentos de nomina","hum_funciones");
                            $arrResponse = array("status"=>true,"msg"=>"Datos guardados correctamente.","id"=>$request);
                        }else{
                            insertAuditoria("hum_auditoria","hum_funciones_id",5,"Editar",$intConsecutivo,"Descuentos de nomina","hum_funciones");
                            $arrResponse = array("status"=>true,"msg"=>"Datos actualizados correctamente.");
                        }
                    }else if($request =="existe"){
                        $arrResponse = array("status"=>false,"msg"=>"El contrato ya tiene este descuento asignado, debe pagarlo por completo o desactivarlo para crear uno nuevo.");
                    }else{
                        $arrResponse = array("status"=>false,"msg"=>"No se ha podido guardar, inténte de nuevo.");
                    }
                }
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
            die();
        }
        public function getEdit(){
            if(!empty($_SESSION)){
                $intId = intval($_POST['codigo']);
                $request = $this->selectEdit($intId);
                if(!empty($request)){
                    $arrResponse = array(
                    "status"=>true,
                    "data"=>$request,
                    "variables_pago"=>$this->selectVariablesPago(),
                    "variables_ret"=>$this->selectVariablesRetenciones(),
                    "variables_ssf"=>$this->selectVariablesSsf(),
                    "contratos"=>$this->selectContratos(),
                    "consecutivos"=>getConsecutivos("humretenempleados","id"));
                }else{
                    $arrResponse = array("status"=>false,"consecutivo"=>searchConsec("humretenempleados ","id")-1);
                }
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
            die();
        }
        public function getSearch(){
            if(!empty($_SESSION)){
                $strFechaInicial = strClean($_POST['fecha_inicial']);
                $strFechaFinal = strClean($_POST['fecha_final']);
                $strDocumento = strClean($_POST['documento']);
                $intContrato = strClean($_POST['contrato']);
                $strDescripcion = strClean($_POST['descripcion']);
                $request = $this->selectSearch($strFechaInicial,$strFechaFinal,$strDocumento,$intContrato,$strDescripcion);
                //$this->selectTempCuotas();
                echo json_encode($request,JSON_UNESCAPED_UNICODE);
            }
            die();
        }
        public function changeData(){
            if(!empty($_SESSION)){
                $id = intval($_POST['id']);
                $idContrato = intval($_POST['contrato']);
                $idRetencion = strClean($_POST['retencion']);
                $estado = $_POST['estado'];
                $request = $this->updateStatus($id,$idContrato,$idRetencion,$estado);
                if($request == 1){
                    insertAuditoria("hum_auditoria","hum_funciones_id",5,"Editar estado",$id,"Descuentos de nomina","hum_funciones");
                    $arrResponse = array("status"=>true);
                }else{
                    $arrResponse = array("status"=>false,"msg"=>"Error");
                }
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
            die();
        }
    }

    if($_POST){
        $obj = new AutorizacionDescuentosController();
        if($_POST['action'] == "save"){
            $obj->save();
        }else if($_POST['action']=="edit"){
            $obj->getEdit();
        }else if($_POST['action']=="search"){
            $obj->getSearch();
        }else if($_POST['action']=="change"){
            $obj->changeData();
        }else if($_POST['action']=="get"){
            $obj->getData();
        }
    }

?>
