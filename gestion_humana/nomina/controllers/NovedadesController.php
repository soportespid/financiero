<?php
    require_once '../../../Librerias/core/Helpers.php';
    require_once '../../../PHPExcel/Classes/PHPExcel.php';
    require_once '../models/NovedadesModel.php';
    session_start();

    class NovedadesController extends NovedadesModel{
        public function getData(){
            if(!empty($_SESSION)){
                $intMes = intval($_POST['mes']);
                $intAnio = intval($_POST['anio']);
                $arrResponse = [
                    "meses"=>$this->selectMeses(),
                    "contratos"=>$this->selectContratos($intAnio,$intMes),
                    "hora"=>$this->selectHorasMensuales(),
                    "variables"=>$this->selectVariablesPago(),
                    "generales"=>$this->selectGenerales()
                ];
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
        }
        public function save(){
            if(!empty($_SESSION)){
                $arrData = json_decode($_POST['data'],true);
                if( empty($_POST['anio']) || empty($_POST['mes']) || empty($arrData) || empty($_POST['descripcion'])){
                    $arrResponse = array("status"=>false,"msg"=>"Error de datos");
                }else{
                    $strFecha = strClean($_POST['fecha']);
                    $intMes = intval($_POST['mes']);
                    $intAnio = intval($_POST['anio']);
                    $intConsecutivo = intval($_POST['consecutivo']);
                    $strDescripcion = replaceChar(strClean($_POST['descripcion']));
                    $opcion="";
                    if($intConsecutivo == 0){
                        $opcion = 1;
                        $request = $this->insertData($strFecha,$intMes,$intAnio,$strDescripcion);
                        $id = $request;
                    }else{
                        $opcion = 2;
                        $id = $intConsecutivo;
                        $request = $this->updateData($intConsecutivo,$strFecha,$intMes,$intAnio,$strDescripcion);
                    }
                    if($request > 0){
                        $this->insertDet($id,$strDescripcion,$arrData);
                        if($opcion == 1){
                            insertAuditoria("hum_auditoria","hum_funciones_id",2,"Crear",$request,"Preliquidaciones de nomina","hum_funciones");
                            $arrResponse = array("status"=>true,"msg"=>"Datos guardados correctamente.","id"=>$request);
                        }else{
                            insertAuditoria("hum_auditoria","hum_funciones_id",2,"Editar",$intConsecutivo,"Preliquidaciones de nomina","hum_funciones");
                            $arrResponse = array("status"=>true,"msg"=>"Datos actualizados correctamente.");
                        }
                    }else{
                        $arrResponse = array("status"=>false,"msg"=>"No se ha podido guardar, inténte de nuevo.");
                    }
                }
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
            die();
        }
        public function getEdit(){
            if(!empty($_SESSION)){
                $intId = intval($_POST['codigo']);
                $intMes = intval($_POST['mes']);
                $intAnio = intval($_POST['anio']);
                $request = $this->selectEdit($intId);
                if(!empty($request)){

                    $arrGenerales = $this->selectGenerales();
                    $horaMensual = $this->selectHorasMensuales();
                    $arrDet = $request['novedad_pago'];
                    $arrDetExtras = $request['novedad_extra'];
                    $arrDetSueldo = array_values(array_filter($arrDet,function($e){return $e['tipo']== "01";}));
                    $arrVariables = $this->selectVariablesPago();
                    $totalVariables = count($arrVariables);
                    $arrContratos = $this->selectContratos($intAnio,$intMes);
                    unset($request['novedad_pago'],$request['novedad_extra']);
                    for ($i=0; $i < $totalVariables ; $i++) {
                        $e = $arrVariables[$i];
                        $detalle = [];
                        if($e['codigo']=="01"){
                            $totalDetVar = count($arrDetSueldo);
                            for ($j=0; $j < $totalDetVar; $j++) {
                                $sueldo = $arrDetSueldo[$j];
                                $contrato = array_values(array_filter(
                                    $arrContratos,function($co)use($sueldo){return $co['codigo']==$sueldo['idfun'] || $co['tercero']==$sueldo['documento'];})
                                )[0];
                                array_push($detalle,[
                                    "transporte"=>0,
                                    "alimentacion"=>0,
                                    "dias"=>$sueldo['dias'],
                                    "dias_laborados"=>$sueldo['dias_laborados'],
                                    "dias_vacaciones"=>$sueldo['dias_vacaciones'],
                                    "dias_incapacidad"=>$sueldo['dias_incapacidad'],
                                    "dias_original"=>$contrato['dias'],
                                    "valor_dias"=>round(($sueldo['valord']/$contrato['dias'])*$sueldo['dias']),
                                    "codigo"=>$contrato['codigo'],
                                    "nombre"=>$contrato['nombre'],
                                    "documento"=>$contrato['tercero'],
                                    "salario"=>$sueldo['valorb'],
                                    "salario_formateado"=>formatNum($sueldo['valorb']),
                                    "valor_hora_formateado"=>formatNum(round($sueldo['valorb']/$horaMensual)),
                                    "valor_hora"=>round($sueldo['valorb']/$horaMensual),
                                    "valor_otros"=>0,
                                    "valor_otros_formateado"=>formatNum(0),
                                    "ordinaria_diurna"=>0,
                                    "ordinaria_nocturna"=>0,
                                    "dominical_diurna"=>0,
                                    "dominical_nocturna"=>0,
                                    "recargo_nocturno"=>0,
                                    "recargo_dominical_diurno"=>0,
                                    "recargo_dominical_nocturno"=>0,
                                    "valor_ordinaria_diurna"=>0,
                                    "valor_ordinaria_nocturna"=>0,
                                    "valor_dominical_diurna"=>0,
                                    "valor_dominical_nocturna"=>0,
                                    "valor_recargo_nocturno"=>0,
                                    "valor_recargo_dominical_diurno"=>0,
                                    "valor_recargo_dominical_nocturno"=>0,
                                    "valor_formateado_ordinaria_diurna"=>formatNum(0),
                                    "valor_formateado_ordinaria_nocturna"=>formatNum(0),
                                    "valor_formateado_dominical_diurna"=>formatNum(0),
                                    "valor_formateado_dominical_nocturna"=>formatNum(0),
                                    "valor_formateado_recargo_nocturno"=>formatNum(0),
                                    "valor_formateado_recargo_dominical_diurno"=>formatNum(0),
                                    "valor_formateado_recargo_dominical_nocturno"=>formatNum(0),
                                    "total_extra"=>0,
                                    "total_recargo"=>0,
                                    "total"=>0,
                                    "total_extra_formateado"=>0,
                                    "total_recargo_formateado"=>0,
                                    "total_formateado"=>0,
                                    "is_checked_salud_empleado"=>$sueldo['pse'] == "S" ? 1 : 0,
                                    "is_checked_salud_empresa"=>$sueldo['psr'] == "S" ? 1 : 0,
                                    "is_checked_pension_empleado"=>$sueldo['ppe'] == "S" ? 1 : 0,
                                    "is_checked_pension_empresa"=>$sueldo['ppr'] == "S" ? 1 : 0,
                                    "is_checked_arl"=>$sueldo['parl'] == "S" ? 1 : 0,
                                    "is_checked_ccf"=>$sueldo['pccf'] == "S" ? 1 : 0,
                                    "is_checked_icbf"=>$sueldo['picbf'] == "S" ? 1 : 0,
                                    "is_checked_sena"=>$sueldo['psena'] == "S" ? 1 : 0,
                                    "is_checked_inst_tecnico"=>$sueldo['pitec'] == "S" ? 1 : 0,
                                    "is_checked_esap"=>$sueldo['pesap'] == "S" ? 1 : 0,
                                ]);
                            }
                        }else if($e['codigo']=="02"){
                            $totalDetVar = count($arrDetExtras);
                            for ($j=0; $j < $totalDetVar; $j++) {
                                $extra = $arrDetExtras[$j];
                                $contrato = array_values(array_filter($arrContratos,function($co)use($extra){return $co['codigo']==$extra['contrato_id'];}))[0];
                                $valorHora = round($extra['salario']/$horaMensual);
                                $valorExtraDiurna = ($valorHora*1.25)*$extra['ord_diurna'];
                                $valorExtraNocturna = ($valorHora*1.75)*$extra['ord_nocturna'];
                                $valorExtraDomDiurna = ($valorHora*2)*$extra['dom_diurna'];
                                $valorExtraDomNocturna = ($valorHora*2.5)*$extra['dom_nocturna'];
                                $valorRecNocturno = ($valorHora*1.35)*$extra['rec_nocturna'];
                                $valorRecDomNocturno = ($valorHora*1.75)*$extra['rec_dom_diurna'];
                                $valorRecDomDiurno = ($valorHora*2.1)*$extra['rec_dom_nocturna'];
                                array_push($detalle,[
                                    "transporte"=>0,
                                    "alimentacion"=>0,
                                    "dias"=>$contrato['dias'],
                                    "dias_original"=>$contrato['dias'],
                                    "valor_dias"=>round(($extra['salario']/$contrato['dias'])*$contrato['dias']),
                                    "codigo"=>$contrato['codigo'],
                                    "nombre"=>$contrato['nombre'],
                                    "documento"=>$contrato['tercero'],
                                    "salario"=>$extra['salario'],
                                    "salario_formateado"=>formatNum($extra['salario']),
                                    "valor_hora_formateado"=>formatNum($valorHora),
                                    "valor_hora"=>$valorHora,
                                    "valor_otros"=>0,
                                    "valor_otros_formateado"=>formatNum(0),
                                    "ordinaria_diurna"=>$extra['ord_diurna'],
                                    "ordinaria_nocturna"=>$extra['ord_nocturna'],
                                    "dominical_diurna"=>$extra['dom_diurna'],
                                    "dominical_nocturna"=>$extra['dom_nocturna'],
                                    "recargo_nocturno"=>$extra['rec_nocturna'],
                                    "recargo_dominical_diurno"=>$extra['rec_dom_diurna'],
                                    "recargo_dominical_nocturno"=>$extra['rec_dom_nocturna'],
                                    "valor_ordinaria_diurna"=>$valorExtraDiurna,
                                    "valor_ordinaria_nocturna"=>$valorExtraNocturna,
                                    "valor_dominical_diurna"=>$valorExtraDomDiurna,
                                    "valor_dominical_nocturna"=>$valorExtraDomNocturna,
                                    "valor_recargo_nocturno"=>$valorRecNocturno,
                                    "valor_recargo_dominical_diurno"=>$valorRecDomDiurno,
                                    "valor_recargo_dominical_nocturno"=>$valorRecDomNocturno,
                                    "valor_formateado_ordinaria_diurna"=>formatNum($valorExtraDiurna),
                                    "valor_formateado_ordinaria_nocturna"=>formatNum($valorExtraNocturna),
                                    "valor_formateado_dominical_diurna"=>formatNum($valorExtraDomDiurna),
                                    "valor_formateado_dominical_nocturna"=>formatNum($valorExtraDomNocturna),
                                    "valor_formateado_recargo_nocturno"=>formatNum($valorRecNocturno),
                                    "valor_formateado_recargo_dominical_diurno"=>formatNum($valorRecDomDiurno),
                                    "valor_formateado_recargo_dominical_nocturno"=>formatNum($valorRecDomNocturno),
                                    "total_extra"=>$extra['total_extra'],
                                    "total_recargo"=>$extra['total_recargo'],
                                    "total"=>$extra['total'],
                                    "total_extra_formateado"=>formatNum($extra['total_extra']),
                                    "total_recargo_formateado"=>formatNum($extra['total_recargo']),
                                    "total_formateado"=>formatNum($extra['total']),
                                    "is_checked_salud_empleado"=>1,
                                    "is_checked_salud_empresa"=>1,
                                    "is_checked_pension_empleado"=>1,
                                    "is_checked_pension_empresa"=>1,
                                    "is_checked_arl"=>1,
                                    "is_checked_ccf"=>1,
                                    "is_checked_icbf"=>1,
                                    "is_checked_sena"=>1,
                                    "is_checked_inst_tecnico"=>1,
                                    "is_checked_esap"=>1,
                                ]);
                            }
                        }else{
                            $arrDetOtros = array_values(array_filter($arrDet,function($ot) use($e){return $ot['tipo']== $e['codigo'];}));
                            $totalDetVar = count($arrDetOtros);
                            for ($j=0; $j < $totalDetVar; $j++) {
                                $sueldo = $arrDetOtros[$j];
                                $contrato = array_values(array_filter(
                                    $arrContratos,function($co)use($sueldo){return $co['codigo']==$sueldo['idfun'] || $co['tercero']==$sueldo['documento'];})
                                    )[0];
                                $transporte = $e['codigo'] == "05" ? $sueldo['valord'] : 0;
                                $alimentacion = $e['codigo'] == "04" ? $sueldo['valord'] : 0;
                                array_push($detalle,[
                                    "transporte"=>$transporte,
                                    "alimentacion"=>$alimentacion,
                                    "dias"=>$sueldo['dias'],
                                    "dias_original"=>$contrato['dias'],
                                    "dias_laborados"=>$sueldo['dias_laborados'],
                                    "dias_vacaciones"=>$sueldo['dias_vacaciones'],
                                    "dias_incapacidad"=>$sueldo['dias_incapacidad'],
                                    "valor_dias"=>round(($sueldo['valorb']/$contrato['dias'])*$sueldo['dias']),
                                    "codigo"=>$contrato['codigo'],
                                    "nombre"=>$contrato['nombre'],
                                    "documento"=>$contrato['tercero'],
                                    "salario"=>$sueldo['valorb'],
                                    "salario_formateado"=>formatNum($sueldo['valorb']),
                                    "valor_hora_formateado"=>formatNum(round($sueldo['valorb']/$horaMensual)),
                                    "valor_hora"=>round($sueldo['valorb']/$horaMensual),
                                    "valor_otros"=>$sueldo['valord'],
                                    "valor_otros_formateado"=>formatNum($sueldo['valord']),
                                    "ordinaria_diurna"=>0,
                                    "ordinaria_nocturna"=>0,
                                    "dominical_diurna"=>0,
                                    "dominical_nocturna"=>0,
                                    "recargo_nocturno"=>0,
                                    "recargo_dominical_diurno"=>0,
                                    "recargo_dominical_nocturno"=>0,
                                    "valor_ordinaria_diurna"=>0,
                                    "valor_ordinaria_nocturna"=>0,
                                    "valor_dominical_diurna"=>0,
                                    "valor_dominical_nocturna"=>0,
                                    "valor_recargo_nocturno"=>0,
                                    "valor_recargo_dominical_diurno"=>0,
                                    "valor_recargo_dominical_nocturno"=>0,
                                    "valor_formateado_ordinaria_diurna"=>formatNum(0),
                                    "valor_formateado_ordinaria_nocturna"=>formatNum(0),
                                    "valor_formateado_dominical_diurna"=>formatNum(0),
                                    "valor_formateado_dominical_nocturna"=>formatNum(0),
                                    "valor_formateado_recargo_nocturno"=>formatNum(0),
                                    "valor_formateado_recargo_dominical_diurno"=>formatNum(0),
                                    "valor_formateado_recargo_dominical_nocturno"=>formatNum(0),
                                    "total_extra"=>0,
                                    "total_recargo"=>0,
                                    "total"=>0,
                                    "total_extra_formateado"=>0,
                                    "total_recargo_formateado"=>0,
                                    "total_formateado"=>0,
                                    "is_checked_salud_empleado"=>$sueldo['pse'] == "S" ? 1 : 0,
                                    "is_checked_salud_empresa"=>$sueldo['psr'] == "S" ? 1 : 0,
                                    "is_checked_pension_empleado"=>$sueldo['ppe'] == "S" ? 1 : 0,
                                    "is_checked_pension_empresa"=>$sueldo['ppr'] == "S" ? 1 : 0,
                                    "is_checked_arl"=>$sueldo['parl'] == "S" ? 1 : 0,
                                    "is_checked_ccf"=>$sueldo['pccf'] == "S" ? 1 : 0,
                                    "is_checked_icbf"=>$sueldo['picbf'] == "S" ? 1 : 0,
                                    "is_checked_sena"=>$sueldo['psena'] == "S" ? 1 : 0,
                                    "is_checked_inst_tecnico"=>$sueldo['pitec'] == "S" ? 1 : 0,
                                    "is_checked_esap"=>$sueldo['pesap'] == "S" ? 1 : 0,
                                ]);
                            }
                        }
                        $arrVariables[$i]['is_checked_salud_empleado'] = count(array_filter($detalle,function($f){return $f['is_checked_salud_empleado'] == 1; })) > 0 ? 1 : 0;
                        $arrVariables[$i]['is_checked_salud_empresa'] = count(array_filter($detalle,function($f){return $f['is_checked_salud_empresa'] == 1; })) > 0 ? 1 : 0;
                        $arrVariables[$i]['is_checked_pension_empleado'] = count(array_filter($detalle,function($f){return $f['is_checked_pension_empleado'] == 1; })) > 0 ? 1 : 0;
                        $arrVariables[$i]['is_checked_pension_empresa'] = count(array_filter($detalle,function($f){return $f['is_checked_pension_empresa'] == 1; })) > 0 ? 1 : 0;
                        $arrVariables[$i]['is_checked_arl'] = count(array_filter($detalle,function($f){return $f['is_checked_arl'] == 1; })) > 0 ? 1 : 0;
                        $arrVariables[$i]['is_checked_ccf'] = count(array_filter($detalle,function($f){return $f['is_checked_ccf'] == 1; })) > 0 ? 1 : 0;
                        $arrVariables[$i]['is_checked_icbf'] = count(array_filter($detalle,function($f){return $f['is_checked_icbf'] == 1; })) > 0 ? 1 : 0;
                        $arrVariables[$i]['is_checked_sena'] = count(array_filter($detalle,function($f){return $f['is_checked_sena'] == 1; })) > 0 ? 1 : 0;
                        $arrVariables[$i]['is_checked_inst_tecnico'] = count(array_filter($detalle,function($f){return $f['is_checked_inst_tecnico'] == 1; })) > 0 ? 1 : 0;
                        $arrVariables[$i]['is_checked_esap'] = count(array_filter($detalle,function($f){return $f['is_checked_esap'] == 1; })) > 0 ? 1 : 0;
                        $arrVariables[$i]['detalle'] = $detalle;
                    }
                    $arrResponse = array(
                        "status"=>true,
                        "data"=>$request,
                        "meses"=>$this->selectMeses(),
                        "contratos"=>$arrContratos,
                        "hora"=>$horaMensual,
                        "variables"=>$arrVariables,
                        "generales"=>$arrGenerales,
                        "consecutivos"=>getConsecutivos("hum_novedadespagos_cab","id")
                    );
                }else{
                    $arrResponse = array("status"=>false,"consecutivo"=>searchConsec("hum_novedadespagos_cab ","id")-1);
                }
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
            die();
        }
        public function getSearch(){
            if(!empty($_SESSION)){
                $strFechaInicial = strClean($_POST['fecha_inicial']);
                $strFechaFinal = strClean($_POST['fecha_final']);
                $strCodigo = strClean($_POST['codigo']);
                $request = $this->selectSearch($strFechaInicial,$strFechaFinal,$strCodigo);
                echo json_encode($request,JSON_UNESCAPED_UNICODE);
            }
            die();
        }
        public function changeData(){
            if(!empty($_SESSION)){
                $id = intval($_POST['id']);
                $estado = $_POST['estado'];
                $request = $this->updateStatus($id,$estado);
                if($request == 1){
                    insertAuditoria("hum_auditoria","hum_funciones_id",2,"Editar estado",$id,"Preliquidaciones de nomina","hum_funciones");

                    $arrResponse = array("status"=>true);
                }else{
                    $arrResponse = array("status"=>false,"msg"=>"Error");
                }
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
            die();
        }
    }

    if($_POST){
        $obj = new NovedadesController();
        if($_POST['action'] == "save"){
            $obj->save();
        }else if($_POST['action']=="edit"){
            $obj->getEdit();
        }else if($_POST['action']=="search"){
            $obj->getSearch();
        }else if($_POST['action']=="change"){
            $obj->changeData();
        }else if($_POST['action']=="get"){
            $obj->getData();
        }
    }

?>
