<?php
    require_once '../../../Librerias/core/Helpers.php';
    require_once '../../../PHPExcel/Classes/PHPExcel.php';
    require_once '../models/LiquidarModel.php';
    require_once '../models/LiquidarTrait.php';
    session_start();

    class RetencionController extends LiquidarModel{
        use LiquidarTrait;
        public function getData(){
            if(!empty($_SESSION)){
                $request = $this->selectNovedades();
                echo json_encode($request,JSON_UNESCAPED_UNICODE);
            }
        }
        public function save(){
            if(!empty($_SESSION)){
                $arrData = json_decode($_POST['data'],true);
                if(empty($arrData) || empty($_POST['fecha']) || empty($_POST['novedad'])){
                    $arrResponse = array("status"=>false,"msg"=>"Error de datos");
                }else{
                    $strFecha = strClean($_POST['fecha']);
                    $arrNovedad = json_decode($_POST['novedad'],true);
                    $intConsecutivo = intval($_POST['consecutivo']);
                    $intTotal = intval($_POST['total']);
                    $opcion="";
                    if($intConsecutivo == 0){
                        $opcion = 1;
                        $request = $this->insertRetencion($strFecha,$arrNovedad,$intTotal);
                        $id = $request;
                    }else{
                        $opcion = 2;
                        $id = $intConsecutivo;
                        $request = $this->updateRetencion($intConsecutivo,$strFecha,$intTotal);
                    }
                    if($request > 0){
                        $this->insertRetencionDet($id,$arrData);
                        if($opcion == 1){
                            insertAuditoria("hum_auditoria","hum_funciones_id",3,"Crear",$request,"Retenciones de nomina","hum_funciones");
                            $arrResponse = array("status"=>true,"msg"=>"Datos guardados correctamente.","id"=>$request);
                        }else{
                            insertAuditoria("hum_auditoria","hum_funciones_id",3,"Editar",$intConsecutivo,"Retenciones de nomina","hum_funciones");
                            $arrResponse = array("status"=>true,"msg"=>"Datos actualizados correctamente.");
                        }
                    }else{
                        $arrResponse = array("status"=>false,"msg"=>"No se ha podido guardar, inténte de nuevo.");
                    }
                }
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
            die();
        }
        public function getEdit(){
            if(!empty($_SESSION)){
                $intId = intval($_POST['codigo']);
                $request = $this->selectRetencionEdit($intId);
                if(!empty($request)){
                    $arrData = $this->getSelectNov($request['novedad_id']);
                    $dataNovedad = $arrData;
                    $total = count($dataNovedad);
                    for ($i=0; $i < $total ; $i++) {
                        $e=$dataNovedad[$i];
                        $arrRet = array_values(array_filter($request['detalle'],function($r) use($e){return $r['cod_funcionario'] == $e['id'];}));
                        $e['retencion'] = $arrRet[0]['retencion'];
                        $e['tipo_pago'] = $arrRet[0]['tipo_pago'];
                        $dataNovedad[$i] = $e;
                    }
                    $arrResponse = array(
                        "status"=>true,
                        "data"=>array(
                            "id"=>$request['id'],
                            "novedad_id"=>$request['novedad_id'],
                            "fecha"=>$request['fecha'],
                            "detalle"=>$dataNovedad,
                            "estado"=>$request['estado'],
                            "id_nom"=>$request['id_nom']
                        ),
                        "novedades"=>$this->selectNovedades(),
                        "consecutivos"=>getConsecutivos("hum_retencionesfun_cab","id")
                    );
                }else{
                    $arrResponse = array("status"=>false,"consecutivo"=>searchConsec("hum_retencionesfun_cab ","id")-1);
                }
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
            die();
        }
        public function getSearch(){
            if(!empty($_SESSION)){
                $strFechaInicial = strClean($_POST['fecha_inicial']);
                $strFechaFinal = strClean($_POST['fecha_final']);
                $strCodigo = strClean($_POST['codigo']);
                $request = $this->selectRetencionesSearch($strFechaInicial,$strFechaFinal,$strCodigo);
                echo json_encode($request,JSON_UNESCAPED_UNICODE);
            }
            die();
        }
        public function changeData(){
            if(!empty($_SESSION)){
                $id = intval($_POST['id']);
                $estado = $_POST['estado'];
                $request = $this->updateRetencionesStatus($id,$estado);
                if($request == 1){
                    insertAuditoria("hum_auditoria","hum_funciones_id",3,"Editar estado",$id,"Retenciones de nomina","hum_funciones");
                    $arrResponse = array("status"=>true);
                }else{
                    $arrResponse = array("status"=>false,"msg"=>"Error");
                }
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
            die();
        }
        public function getSelectNov($intCodigo =""){
            if(!empty($_SESSION)){
                $codigo = $intCodigo != "" ? $intCodigo : intval($_POST['codigo']);
                $intUvt = $this->selectGenerales()['uvt'];
                $arrNovedades = $this->selectNovedad($codigo);
                $arrNovedades = $this->getCalcNovedades($arrNovedades);
                $arrData = $this->getCalcRetencionesT($arrNovedades,$intUvt);
                if($intCodigo ==""){
                    echo json_encode($arrData,JSON_UNESCAPED_UNICODE);
                }else{
                    return $arrData;
                }
            }
            die();
        }
        public function getCalcNovedades(array $arrNovedades){
            $arrParafiscales = $arrNovedades[0]['parafiscales'];
            $arrFondoSolidario = $this->selectFondoSolidario();
            $netoAporteCcf = 0;
            $netoAporteArl = 0;
            $netoAporteIcbf = 0;
            $netoAporteSena = 0;
            $netoAporteEsap = 0;
            $netoAporteIns = 0;
            $netoValor = 0;
            $netoFsp = 0;
            $netoSaludEmpleado = 0;
            $netoSaludEmpresa = 0;
            $netoTotalSalud = 0;
            $netoPensionEmpleado = 0;
            $netoPensionEmpresa = 0;
            $netoPensionTotal = 0;
            $netoRetencion = 0;
            $netoOtrosDescuentos = 0;
            $netoTotalDescuentos = 0;
            $netoTotalPagado = 0;
            $total = count($arrNovedades);
            for ($i=0; $i < $total; $i++) {
                $totalValor = 0;
                $totalBaseIBC = 0;
                $totalBasePara = 0;
                $totalBaseArl = 0;
                $totalSaludEmpleado = 0;
                $totalSaludEmpresa = 0;
                $totalPensionEmpleado = 0;
                $totalPensionEmpresa = 0;
                $totalSalud = 0;
                $totalPension = 0;
                $totalFondoSolidario = 0;
                $totalArl = 0;
                $totalRetencion = 0;
                $totalOtroDesc = 0;
                $totalDescuentos = 0;
                $totalNeto = 0;
                $totalAporteCcf=0;
                $totalAporteSena = 0;
                $totalAporteIcbf = 0;
                $totalAporteInst = 0;
                $totalAporteEsap = 0;
                $arrNovDet = $arrNovedades[$i]['novedades'];
                $totalDet = count($arrNovDet);
                for ($j=0; $j < $totalDet; $j++) {
                    $det = $arrNovDet[$j];
                    $intDescuento = $det['descuento'];
                    $intValor = $det['total'];
                    $intBaseIBC = 0;
                    $intBasePara = 0;
                    $intBaseArl = 0;
                    $intSaludEmpleado = 0;
                    $intSaludEmpresa = 0;
                    $intPensionEmpleado = 0;
                    $intPensionEmpresa = 0;
                    $intTotalSalud = 0;
                    $intTotalPension = 0;
                    $intFondoSolidario = 0;
                    $intRetencion = 0;
                    $intAporteCcf=0;
                    $intAporteSena = 0;
                    $intAporteIcbf = 0;
                    $intAporteInst = 0;
                    $intAporteEsap = 0;
                    $intArl = 0;

                    $data = $this->calcNovedad($det,$arrFondoSolidario);
                    $intBaseArl =  $data['base_arl'];
                    $intBaseIBC = $data['base_ibc'];
                    $intBasePara = $data['base_para'];
                    $intArl = $data['valor_arl'];
                    $intSaludEmpleado = $data['valor_salud_empleado'];
                    $intSaludEmpresa = $data['valor_salud_empresa'];
                    $intTotalSalud = $data['valor_total_salud'];
                    $intPensionEmpleado = $data['valor_pension_empleado'];
                    $intPensionEmpresa = $data['valor_pension_empresa'];
                    $intTotalPension = $data['valor_total_pension'];
                    $intFondoSolidario = $data['valor_fondo'];
                    $intAporteCcf = $data['valor_aporte_ccf'];
                    $intAporteIcbf = $data['valor_aporte_icbf'];
                    $intAporteSena = $data['valor_aporte_sena'];
                    $intAporteInst = $data['valor_aporte_inst'];
                    $intAporteEsap = $data['valor_aporte_esap'];

                    $intTotalDescuentos = $intDescuento+$intFondoSolidario+$intSaludEmpleado+$intPensionEmpleado+$intRetencion;
                    if(isset($det['novedad']) && $det['novedad'] == 2){
                        $intValor = 0;
                        $intNeto = $intValor-$intTotalDescuentos;
                        $totalNeto+=0;
                    }else{
                        $intNeto = $intValor-$intTotalDescuentos;
                        $totalNeto+=$intNeto;
                    }
                    $totalBaseIBC+=$intBaseIBC;
                    $totalBasePara +=$intBasePara;
                    $totalBaseArl +=$intBaseArl;
                    $totalSaludEmpleado +=$intSaludEmpleado;
                    $totalPensionEmpleado +=$intPensionEmpleado;
                    $totalSaludEmpresa +=$intSaludEmpresa;
                    $totalSalud +=$intTotalSalud;
                    $totalPensionEmpresa +=$intPensionEmpresa;
                    $totalPension +=$intTotalPension;
                    $totalFondoSolidario +=$intFondoSolidario;
                    $totalArl+=$intArl;
                    $totalOtroDesc+=$intDescuento;
                    $totalRetencion+=$intRetencion;
                    $totalDescuentos+=$intTotalDescuentos;
                    $totalAporteCcf+=$intAporteCcf;
                    $totalAporteEsap+=$intAporteEsap;
                    $totalAporteIcbf+=$intAporteIcbf;
                    $totalAporteInst+=$intAporteInst;
                    $totalAporteSena+=$intAporteSena;

                    $totalValor+=$intValor;
                    $det['valor_ibc'] = $intBaseIBC;
                    $det['valor_ibc_para'] = $intBasePara;
                    $det['valor_ibc_arl'] = $intBaseArl;
                    $det['valor_salud_empleado'] = $intSaludEmpleado;
                    $det['valor_salud_empresa'] = $intSaludEmpresa;
                    $det['valor_total_salud'] = $intTotalSalud;
                    $det['valor_pension_empleado'] = $intPensionEmpleado;
                    $det['valor_pension_empresa'] = $intPensionEmpresa;
                    $det['valor_total_pension'] = $intTotalPension;
                    $det['valor_fondo'] = $intFondoSolidario;
                    $det['valor_descuento'] = $intDescuento;
                    $det['valor_retencion'] = $intRetencion;
                    $det['valor_total_descuentos'] = $intTotalDescuentos;
                    $det['valor_aporte_ccf'] = $intAporteCcf;
                    $det['valor_aporte_sena'] = $intAporteSena;
                    $det['valor_aporte_icbf'] = $intAporteIcbf;
                    $det['valor_aporte_inst'] = $intAporteInst;
                    $det['valor_aporte_esap'] = $intAporteEsap;
                    $det['valor_arl'] = $intArl;
                    $det['valor_neto'] = $intNeto;
                    $arrNovDet[$j] = $det;

                }

                $netoAporteCcf += $totalAporteCcf;
                $netoAporteArl += $totalArl;
                $netoAporteIcbf += $totalAporteIcbf;
                $netoAporteSena += $totalAporteSena;
                $netoAporteEsap += $totalAporteEsap;
                $netoAporteIns += $totalAporteInst;
                $netoValor += $totalValor;
                $netoSaludEmpleado += $totalSaludEmpleado;
                $netoSaludEmpresa += $totalSaludEmpresa;
                $netoTotalSalud += $totalSalud;
                $netoPensionEmpleado += $totalPensionEmpleado;
                $netoPensionEmpresa += $totalPensionEmpresa;
                $netoPensionTotal += $totalPension;
                $netoRetencion += $totalRetencion;
                $netoOtrosDescuentos += $totalOtroDesc;
                $netoTotalDescuentos += $totalDescuentos;
                $netoTotalPagado += $totalNeto;
                $netoFsp += $totalFondoSolidario;

                $arrNovedades[$i]['totales']['total_valor'] = $totalValor;
                $arrNovedades[$i]['totales']['total_base_ibc'] = $totalBaseIBC;
                $arrNovedades[$i]['totales']['total_base_para'] = $totalBasePara;
                $arrNovedades[$i]['totales']['total_base_arl'] = $totalBaseArl;
                $arrNovedades[$i]['totales']['total_arl'] = $totalArl;
                $arrNovedades[$i]['totales']['total_salud_empleado'] = $totalSaludEmpleado;
                $arrNovedades[$i]['totales']['total_salud_empresa'] = $totalSaludEmpresa;
                $arrNovedades[$i]['totales']['total_salud'] = $totalSalud;
                $arrNovedades[$i]['totales']['total_pension_empleado'] = $totalPensionEmpleado;
                $arrNovedades[$i]['totales']['total_pension_empresa'] = $totalPensionEmpresa;
                $arrNovedades[$i]['totales']['total_pension'] = $totalPension;
                $arrNovedades[$i]['totales']['total_fondo'] = $totalFondoSolidario;
                $arrNovedades[$i]['totales']['total_retencion'] = $totalRetencion;
                $arrNovedades[$i]['totales']['total_otros_desc'] = $totalOtroDesc;
                $arrNovedades[$i]['totales']['total_descuentos'] = $totalDescuentos;
                $arrNovedades[$i]['totales']['total_neto'] = $totalNeto;
                $arrNovedades[$i]['totales']['total_aporte_ccf'] = $totalAporteCcf;
                $arrNovedades[$i]['totales']['total_aporte_sena'] = $totalAporteSena;
                $arrNovedades[$i]['totales']['total_aporte_icbf'] = $totalAporteIcbf;
                $arrNovedades[$i]['totales']['total_aporte_inst'] = $totalAporteInst;
                $arrNovedades[$i]['totales']['total_aporte_esap'] = $totalAporteEsap;
                $arrNovedades[$i]['novedades'] = $arrNovDet;
            }
            $arrData=array(
                "neto"=>[
                    "neto_ccf"=>$netoAporteCcf,
                    "neto_arl"=>$netoAporteArl,
                    "neto_icbf"=>$netoAporteIcbf,
                    "neto_sena"=>$netoAporteSena,
                    "neto_esap"=>$netoAporteEsap,
                    "neto_inst"=>$netoAporteIns,
                    "neto_devengado"=>$netoValor,
                    "neto_salud_empleado"=>$netoSaludEmpleado,
                    "neto_salud_empresa"=>$netoSaludEmpresa,
                    "neto_total_salud"=>$netoTotalSalud,
                    "neto_pension_empleado"=>$netoPensionEmpleado,
                    "neto_pension_empresa"=>$netoPensionEmpresa,
                    "neto_total_pension"=>$netoPensionTotal,
                    "neto_fsp"=>$netoFsp,
                    "neto_retencion"=>$netoRetencion,
                    "neto_otros_descuentos"=>$netoOtrosDescuentos,
                    "neto_total_descuentos"=>$netoTotalDescuentos,
                    "neto_total_pagado"=>$netoTotalPagado
                ],
                "parafiscales"=>$arrParafiscales,
                "novedades"=>$arrNovedades
            );
            return $arrData;
        }
    }

    if($_POST){
        $obj = new RetencionController();
        if($_POST['action'] == "save"){
            $obj->save();
        }else if($_POST['action']=="edit"){
            $obj->getEdit();
        }else if($_POST['action']=="search"){
            $obj->getSearch();
        }else if($_POST['action']=="change"){
            $obj->changeData();
        }else if($_POST['action']=="get"){
            $obj->getData();
        }else if($_POST['action']=="nov"){
            $obj->getSelectNov();
        }
    }

?>
