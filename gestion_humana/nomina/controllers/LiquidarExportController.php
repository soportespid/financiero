<?php
    require_once '../../../Librerias/core/Helpers.php';
    require_once '../../../PHPExcel/Classes/PHPExcel.php';
    session_start();
    /*ini_set('display_errors', '1');
    ini_set('display_startup_errors', '1');
    error_reporting(E_ALL);*/
    class LiquidarExportController {
        public function exportExcel(){
            if(!empty($_SESSION)){
                $arrData = json_decode($_POST['data'],true);
                //dep($arrData);exit;
                if(!empty($arrData)){
                    $request = configBasica();
                    $strNit = $request['nit'];
                    $strRazon = $request['razonsocial'];
                    $intId = intval($arrData['id']);
                    $arrFecha = explode("-",$arrData['fecha']);
                    $strFecha = $arrFecha[2]."/".$arrFecha[1]."/".$arrFecha[0];
                    $strMes = strClean($arrData['mes']);
                    $intVigencia = intval($arrData['vigencia']);
                    $fileName = "reporte_liquidacion_nomina_".$intVigencia."_".$strMes.".xlsx";
                    $arrDet = $arrData['detalle']['liquidacion'];
                    $arrNeto = $arrData['detalle']['neto'];

                    $objPHPExcel = new PHPExcel();
                    $objPHPExcel->getActiveSheet()->getStyle('A:J')->applyFromArray(
                        array(
                            'alignment' => array(
                                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
                                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
                            )
                        )
                    );
                    $objPHPExcel->getProperties()
                    ->setCreator("IDEAL 10")
                    ->setLastModifiedBy("IDEAL 10")
                    ->setTitle("Exportar Excel con PHP")
                    ->setSubject("Documento de prueba")
                    ->setDescription("Documento generado con PHPExcel")
                    ->setKeywords("usuarios phpexcel")
                    ->setCategory("reportes");

                    //----Cuerpo de Documento----
                    $objPHPExcel->setActiveSheetIndex(0)
                    ->mergeCells('A1:Z1')
                    ->mergeCells('A2:Z2')
                    ->mergeCells('B3:Z3')
                    ->mergeCells('B4:Z4')
                    ->mergeCells('B5:Z5')
                    ->mergeCells('B6:Z6')
                    ->setCellValue('A1', 'GESTIÓN HUMANA - '.$strRazon)
                    ->setCellValue('A2', 'LIQUIDACIÓN DE NÓMINA')
                    ->setCellValue('A3', 'CÓDIGO')
                    ->setCellValue('A4', 'FECHA')
                    ->setCellValue('A5', 'VIGENCIA')
                    ->setCellValue('A6', 'MES')
                    ->setCellValue('B3', $intId)
                    ->setCellValue('B4', $strFecha)
                    ->setCellValue('B5', $intVigencia)
                    ->setCellValue('B6', $strMes);
                    $objPHPExcel-> getActiveSheet ()
                    -> getStyle ("A3")
                    -> getFill ()
                    -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
                    -> getStartColor ()
                    -> setRGB ('3399cc');
                    $objPHPExcel-> getActiveSheet ()
                    -> getStyle ("A4")
                    -> getFill ()
                    -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
                    -> getStartColor ()
                    -> setRGB ('3399cc');
                    $objPHPExcel-> getActiveSheet ()
                    -> getStyle ("A5")
                    -> getFill ()
                    -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
                    -> getStartColor ()
                    -> setRGB ('3399cc');
                    $objPHPExcel-> getActiveSheet ()
                    -> getStyle ("A6")
                    -> getFill ()
                    -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
                    -> getStartColor ()
                    -> setRGB ('3399cc');
                    $objPHPExcel-> getActiveSheet ()
                    -> getStyle ("A1")
                    -> getFill ()
                    -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
                    -> getStartColor ()
                    -> setRGB ('3399cc');
                    $objPHPExcel-> getActiveSheet ()
                    -> getStyle ("A1:A2")
                    -> getFont ()
                    -> setBold ( true )
                    -> setName ( 'Verdana' )
                    -> setSize ( 10 )
                    -> getColor ()
                    -> setRGB ('000000');
                    $objPHPExcel-> getActiveSheet ()
                    -> getStyle ('A1:A2')
                    -> getAlignment ()
                    -> applyFromArray (array ( 'horizontal'  =>  PHPExcel_Style_Alignment :: HORIZONTAL_CENTER ,) );
                    $objPHPExcel-> getActiveSheet ()
                    -> getStyle ('A4:j4')
                    -> getAlignment ()
                    -> applyFromArray (array ( 'horizontal'  =>  PHPExcel_Style_Alignment :: VERTICAL_JUSTIFY ,) );
                    $objPHPExcel-> getActiveSheet ()
                    -> getStyle ("A2")
                    -> getFill ()
                    -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID);

                    $borders = array(
                        'borders' => array(
                            'allborders' => array(
                                'style' => PHPExcel_Style_Border::BORDER_THIN,
                            'color' => array('argb' => 'FF000000'),
                            )
                        ),
                    );
                    $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A7', 'Tipo')
                    ->setCellValue('B7', "Empleado")
                    ->setCellValue('C7', "CC/NIT")
                    ->setCellValue('D7', "Básico")
                    ->setCellValue('E7', "Días liquidados")
                    ->setCellValue('F7', "Valor")
                    ->setCellValue('G7', "IBC")
                    ->setCellValue('H7', "Base parafiscales")
                    ->setCellValue('I7', "Base ARP")
                    ->setCellValue('J7', "ARP")
                    ->setCellValue('K7', "Salud empleado")
                    ->setCellValue('L7', "Salud empresa")
                    ->setCellValue('M7', "Total salud")
                    ->setCellValue('N7', "Pensión empleado")
                    ->setCellValue('O7', "Pensión empresa")
                    ->setCellValue('P7', "Total pensión")
                    ->setCellValue('Q7', "FSP")
                    ->setCellValue('R7', "Retencion en la fuente")
                    ->setCellValue('S7', "Otras deducciones")
                    ->setCellValue('T7', "Total deducciones")
                    ->setCellValue('U7', "Neto a pagar")
                    ->setCellValue('V7', "CCF")
                    ->setCellValue('W7', "SENA")
                    ->setCellValue('X7', "ICBF")
                    ->setCellValue('Y7', "Inst.Técnico")
                    ->setCellValue('Z7', "ESAP");
                    $objPHPExcel-> getActiveSheet ()
                        -> getStyle ("A7:Z7")
                        -> getFill ()
                        -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
                        -> getStartColor ()
                        -> setRGB ('99ddff');

                    $objPHPExcel->getActiveSheet()->getStyle("A7:Z7")->getFont()->setBold(true);
                    $objPHPExcel->getActiveSheet()->getStyle('A1:Z1')->applyFromArray($borders);
                    $objPHPExcel->getActiveSheet()->getStyle('A2:Z2')->applyFromArray($borders);
                    $objPHPExcel->getActiveSheet()->getStyle('A3:Z3')->applyFromArray($borders);
                    $objPHPExcel->getActiveSheet()->getStyle('A4:Z4')->applyFromArray($borders);
                    $objPHPExcel->getActiveSheet()->getStyle('A5:Z5')->applyFromArray($borders);
                    $objPHPExcel->getActiveSheet()->getStyle('A6:Z6')->applyFromArray($borders);
                    $objPHPExcel->getActiveSheet()->getStyle('A7:Z7')->applyFromArray($borders);

                    $objPHPExcel->getActiveSheet()->getStyle("A1")->getFont()->getColor()->setRGB("ffffff");
                    $objPHPExcel->getActiveSheet()->getStyle("A3")->getFont()->getColor()->setRGB("ffffff");
                    $objPHPExcel->getActiveSheet()->getStyle("A4")->getFont()->getColor()->setRGB("ffffff");
                    $objPHPExcel->getActiveSheet()->getStyle("A5")->getFont()->getColor()->setRGB("ffffff");
                    $objPHPExcel->getActiveSheet()->getStyle("A6")->getFont()->getColor()->setRGB("ffffff");

                    $objPHPExcel->getActiveSheet()->getStyle("A3")->getFont()->setBold(true);
                    $objPHPExcel->getActiveSheet()->getStyle("A4")->getFont()->setBold(true);
                    $objPHPExcel->getActiveSheet()->getStyle("A5")->getFont()->setBold(true);
                    $objPHPExcel->getActiveSheet()->getStyle("A6")->getFont()->setBold(true);

                    $row = 8;
                    foreach ($arrDet as $data) {
                        if($data['type']=="det"){
                            $objPHPExcel->setActiveSheetIndex(0)
                            ->setCellValueExplicit ("A$row", $data['nombre'], PHPExcel_Cell_DataType :: TYPE_STRING)
                            ->setCellValueExplicit ("B$row", $data['nombre_tercero'], PHPExcel_Cell_DataType :: TYPE_STRING)
                            ->setCellValueExplicit ("C$row", $data['documento'], PHPExcel_Cell_DataType :: TYPE_STRING)
                            ->setCellValueExplicit ("D$row", $data['salario'], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
                            ->setCellValueExplicit ("E$row", $data['dias'], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
                            ->setCellValueExplicit ("F$row", $data['total'], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
                            ->setCellValueExplicit ("G$row", $data['valor_ibc'], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
                            ->setCellValueExplicit ("H$row", $data['valor_ibc_para'], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
                            ->setCellValueExplicit ("I$row", $data['valor_ibc_arl'], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
                            ->setCellValueExplicit ("J$row", $data['valor_arl'], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
                            ->setCellValueExplicit ("K$row", $data['valor_salud_empleado'], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
                            ->setCellValueExplicit ("L$row", $data['valor_salud_empresa'], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
                            ->setCellValueExplicit ("M$row", $data['valor_total_salud'], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
                            ->setCellValueExplicit ("N$row", $data['valor_pension_empleado'], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
                            ->setCellValueExplicit ("O$row", $data['valor_pension_empresa'], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
                            ->setCellValueExplicit ("P$row", $data['valor_total_pension'], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
                            ->setCellValueExplicit ("Q$row", $data['valor_fondo'], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
                            ->setCellValueExplicit ("R$row", $data['valor_retencion'], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
                            ->setCellValueExplicit ("S$row", $data['valor_descuento'], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
                            ->setCellValueExplicit ("T$row", $data['valor_total_descuentos'], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
                            ->setCellValueExplicit ("U$row", $data['valor_neto'], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
                            ->setCellValueExplicit ("V$row", $data['valor_aporte_ccf'], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
                            ->setCellValueExplicit ("W$row", $data['valor_aporte_sena'], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
                            ->setCellValueExplicit ("X$row", $data['valor_aporte_icbf'], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
                            ->setCellValueExplicit ("Y$row", $data['valor_aporte_inst'], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
                            ->setCellValueExplicit ("Z$row", $data['valor_aporte_esap'], PHPExcel_Cell_DataType :: TYPE_NUMERIC);
                        }else{
                            $objPHPExcel->setActiveSheetIndex(0)
                            ->mergeCells("A$row:E$row")
                            ->setCellValue("A$row", "Total")
                            ->setCellValueExplicit ("F$row", $data['total']['total_valor'], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
                            ->setCellValueExplicit ("G$row", $data['total']['total_base_ibc'], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
                            ->setCellValueExplicit ("H$row", $data['total']['total_base_para'], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
                            ->setCellValueExplicit ("I$row", $data['total']['total_base_arl'], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
                            ->setCellValueExplicit ("J$row", $data['total']['total_arl'], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
                            ->setCellValueExplicit ("K$row", $data['total']['total_salud_empleado'], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
                            ->setCellValueExplicit ("L$row", $data['total']['total_salud_empresa'], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
                            ->setCellValueExplicit ("M$row", $data['total']['total_salud'], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
                            ->setCellValueExplicit ("N$row", $data['total']['total_pension_empleado'], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
                            ->setCellValueExplicit ("O$row", $data['total']['total_pension_empresa'], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
                            ->setCellValueExplicit ("P$row", $data['total']['total_pension'], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
                            ->setCellValueExplicit ("Q$row", $data['total']['total_fondo'], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
                            ->setCellValueExplicit ("R$row", $data['total']['total_retencion'], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
                            ->setCellValueExplicit ("S$row", $data['total']['total_otros_desc'], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
                            ->setCellValueExplicit ("T$row", $data['total']['total_descuentos'], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
                            ->setCellValueExplicit ("U$row", $data['total']['total_neto'], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
                            ->setCellValueExplicit ("V$row", $data['total']['total_aporte_ccf'], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
                            ->setCellValueExplicit ("W$row", $data['total']['total_aporte_sena'], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
                            ->setCellValueExplicit ("X$row", $data['total']['total_aporte_icbf'], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
                            ->setCellValueExplicit ("Y$row", $data['total']['total_aporte_inst'], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
                            ->setCellValueExplicit ("Z$row", $data['total']['total_aporte_esap'], PHPExcel_Cell_DataType :: TYPE_NUMERIC);

                            $objPHPExcel-> getActiveSheet ()
                            -> getStyle ("A$row:Z$row")
                            -> getFill ()
                            -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
                            -> getStartColor ()
                            -> setRGB ('99ddff');
                            $objPHPExcel->getActiveSheet()->getStyle("A$row")->getFont()->setBold(true);
                        }
                        $objPHPExcel->getActiveSheet()->getStyle("A$row:Z$row")->applyFromArray($borders);
                        $row++;
                    }

                    $objPHPExcel->setActiveSheetIndex(0)
                    ->mergeCells("A$row:E$row")
                    ->setCellValue("A$row", "Neto")
                    ->setCellValueExplicit ("F$row", $arrNeto['neto_devengado'], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
                    ->setCellValueExplicit ("G$row", 0, PHPExcel_Cell_DataType :: TYPE_NUMERIC)
                    ->setCellValueExplicit ("H$row", 0, PHPExcel_Cell_DataType :: TYPE_NUMERIC)
                    ->setCellValueExplicit ("I$row", 0, PHPExcel_Cell_DataType :: TYPE_NUMERIC)
                    ->setCellValueExplicit ("J$row", $arrNeto['neto_arl'], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
                    ->setCellValueExplicit ("K$row", $arrNeto['neto_salud_empleado'], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
                    ->setCellValueExplicit ("L$row", $arrNeto['neto_salud_empresa'], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
                    ->setCellValueExplicit ("M$row", $arrNeto['neto_total_salud'], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
                    ->setCellValueExplicit ("N$row", $arrNeto['neto_pension_empleado'], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
                    ->setCellValueExplicit ("O$row", $arrNeto['neto_pension_empresa'], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
                    ->setCellValueExplicit ("P$row", $arrNeto['neto_total_pension'], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
                    ->setCellValueExplicit ("Q$row", $arrNeto['neto_fsp'], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
                    ->setCellValueExplicit ("R$row", $arrNeto['neto_retencion'], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
                    ->setCellValueExplicit ("S$row", $arrNeto['neto_otros_descuentos'], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
                    ->setCellValueExplicit ("T$row", $arrNeto['neto_total_descuentos'], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
                    ->setCellValueExplicit ("U$row", $arrNeto['neto_total_pagado'], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
                    ->setCellValueExplicit ("V$row", $arrNeto['neto_ccf'], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
                    ->setCellValueExplicit ("W$row", $arrNeto['neto_sena'], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
                    ->setCellValueExplicit ("X$row", $arrNeto['neto_icbf'], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
                    ->setCellValueExplicit ("Y$row", $arrNeto['neto_inst'], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
                    ->setCellValueExplicit ("Z$row", $arrNeto['neto_esap'], PHPExcel_Cell_DataType :: TYPE_NUMERIC);

                    $objPHPExcel-> getActiveSheet ()
                    -> getStyle ("A$row:Z$row")
                    -> getFill ()
                    -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
                    -> getStartColor ()
                    -> setRGB ('99ddff');

                    $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
                    $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
                    $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
                    $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
                    $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
                    $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
                    $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
                    $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
                    $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
                    $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);
                    $objPHPExcel->getActiveSheet()->getColumnDimension('K')->setAutoSize(true);
                    $objPHPExcel->getActiveSheet()->getColumnDimension('L')->setAutoSize(true);
                    $objPHPExcel->getActiveSheet()->getColumnDimension('M')->setAutoSize(true);
                    $objPHPExcel->getActiveSheet()->getColumnDimension('N')->setAutoSize(true);
                    $objPHPExcel->getActiveSheet()->getColumnDimension('O')->setAutoSize(true);
                    $objPHPExcel->getActiveSheet()->getColumnDimension('P')->setAutoSize(true);
                    $objPHPExcel->getActiveSheet()->getColumnDimension('Q')->setAutoSize(true);
                    $objPHPExcel->getActiveSheet()->getColumnDimension('R')->setAutoSize(true);
                    $objPHPExcel->getActiveSheet()->getColumnDimension('S')->setAutoSize(true);
                    $objPHPExcel->getActiveSheet()->getColumnDimension('T')->setAutoSize(true);
                    $objPHPExcel->getActiveSheet()->getColumnDimension('U')->setAutoSize(true);
                    $objPHPExcel->getActiveSheet()->getColumnDimension('V')->setAutoSize(true);
                    $objPHPExcel->getActiveSheet()->getColumnDimension('W')->setAutoSize(true);
                    $objPHPExcel->getActiveSheet()->getColumnDimension('X')->setAutoSize(true);
                    $objPHPExcel->getActiveSheet()->getColumnDimension('Y')->setAutoSize(true);
                    $objPHPExcel->getActiveSheet()->getColumnDimension('Z')->setAutoSize(true);

                    $objPHPExcel->getActiveSheet()->getStyle("A$row:Z$row")->applyFromArray($borders);
                    $objPHPExcel->getActiveSheet()->getStyle("A$row")->getFont()->setBold(true);

                    //----Guardar documento----
                    header('Content-Type: application/vnd.ms-excel');
                    header('Content-Disposition: attachment;filename="'.$fileName.'"');
                    header('Cache-Control: max-age=0');
                    $objWriter=PHPExcel_IOFactory::createWriter($objPHPExcel,'Excel2007');
                    $objWriter->save('php://output');
                }
            }
            die();
        }
    }

    if($_POST){
        $obj = new LiquidarExportController();
        if($_POST['action'] == "excel"){
            $obj->exportExcel();
        }
    }

?>
