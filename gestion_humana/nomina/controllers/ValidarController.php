<?php
    require_once '../../../Librerias/core/Helpers.php';
    require_once '../../../PHPExcel/Classes/PHPExcel.php';
    require_once '../models/ValidarModel.php';
    require_once '../models/LiquidarTrait.php';
    session_start();

    class ValidarController extends ValidarModel{
        use LiquidarTrait;
        public function getData(){
            if(!empty($_SESSION)){
                $request = $this->selectLiquidaciones();
                echo json_encode($request,JSON_UNESCAPED_UNICODE);
            }
        }
        public function save(){
            if(!empty($_SESSION)){
                $arrData = json_decode($_POST['data'],true);
                $arrNovedad = json_decode($_POST['novedad'],true);
                if( empty($arrData) || empty($arrNovedad)){
                    $arrResponse = array("status"=>false,"msg"=>"Error de datos");
                }else{
                    $strFecha = strClean($_POST['fecha']);
                    $intMes = intval($arrNovedad['mes']);
                    $intVigencia = intval($arrNovedad['vigencia']);
                    $intIdNovedad = intval($arrNovedad['id']);
                    $request = $this->insertLiquidacion($strFecha,$intMes,$intVigencia,$intIdNovedad);
                    if($request > 0){
                        $this->insertLiquidacionDet($request,$arrData);
                        insertAuditoria("hum_auditoria","hum_funciones_id",4,"Crear",$request,"Liquidaciones de nomina","hum_funciones");
                        $arrResponse = array("status"=>true,"msg"=>"Datos guardados correctamente.","id"=>$request);
                    }else{
                        $arrResponse = array("status"=>false,"msg"=>"No se ha podido guardar, inténte de nuevo.");
                    }
                }
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
            die();
        }
        public function getEdit(){
            if(!empty($_SESSION)){
                $intId = intval($_POST['codigo']);
                $request = $this->selectLiquidacion($intId);
                if(!empty($request)){
                    $arrLiquidacion = [];
                    $arrDet = $request['detalle'];
                    $arrTerceros = array_values(array_unique(array_column($arrDet,"documento")));
                    $netoAporteCcf = 0;
                    $netoAporteArl = 0;
                    $netoAporteIcbf = 0;
                    $netoAporteSena = 0;
                    $netoAporteEsap = 0;
                    $netoAporteIns = 0;
                    $netoValor = 0;
                    $netoFsp = 0;
                    $netoSaludEmpleado = 0;
                    $netoSaludEmpresa = 0;
                    $netoTotalSalud = 0;
                    $netoPensionEmpleado = 0;
                    $netoPensionEmpresa = 0;
                    $netoPensionTotal = 0;
                    $netoRetencion = 0;
                    $netoOtrosDescuentos = 0;
                    $netoTotalDescuentos = 0;
                    $netoTotalPagado = 0;
                    foreach ($arrTerceros as $tercero) {
                        $detalle = array_values(array_filter($arrDet,function($e)use($tercero){return $tercero == $e['documento'];}));
                        $totalValor = 0;
                        $totalBaseIBC = 0;
                        $totalBasePara = 0;
                        $totalBaseArl = 0;
                        $totalSaludEmpleado = 0;
                        $totalSaludEmpresa = 0;
                        $totalPensionEmpleado = 0;
                        $totalPensionEmpresa = 0;
                        $totalSalud = 0;
                        $totalPension = 0;
                        $totalFondoSolidario = 0;
                        $totalArl = 0;
                        $totalRetencion = 0;
                        $totalOtroDesc = 0;
                        $totalDescuentos = 0;
                        $totalNeto = 0;
                        $totalAporteCcf=0;
                        $totalAporteSena = 0;
                        $totalAporteIcbf = 0;
                        $totalAporteInst = 0;
                        $totalAporteEsap = 0;
                        foreach ($detalle as $det) {
                            $totalBaseIBC+=$det['valor_ibc'];
                            $totalBasePara +=$det['valor_ibc_para'];
                            $totalBaseArl +=$det['valor_ibc_arl'];
                            $totalSaludEmpleado +=$det['valor_salud_empleado'];
                            $totalSaludEmpresa +=$det['valor_salud_empresa'];
                            $totalPensionEmpleado +=$det['valor_pension_empleado'];
                            $totalPensionEmpresa +=$det['valor_pension_empresa'];
                            $totalSalud +=$det['valor_total_salud'];
                            $totalPension +=$det['valor_total_pension'];
                            $totalFondoSolidario +=$det['valor_fondo'];
                            $totalArl+=$det['valor_arl'];
                            $totalOtroDesc+=$det['valor_descuento'];
                            $totalRetencion+=$det['valor_retencion'];
                            $totalDescuentos+=$det['valor_total_descuentos'];
                            $totalAporteCcf+=$det['valor_aporte_ccf'];
                            $totalAporteEsap+=$det['valor_aporte_esap'];
                            $totalAporteIcbf+=$det['valor_aporte_icbf'];
                            $totalAporteInst+=$det['valor_aporte_inst'];
                            $totalAporteSena+=$det['valor_aporte_sena'];
                            $totalNeto += $det['valor_neto'];
                            $totalValor+=$det['total'];
                            array_push($arrLiquidacion,array(
                                "type"=>"det",
                                "tipo_pago"=>$det['tipo_pago'],
                                "id"=>$det['id'],
                                "nombre"=>$det['descripcion'],
                                "nombre_tercero"=>$det['nombre'],
                                "documento"=>$det['documento'],
                                "salario"=>$det['salario'],
                                "dias"=>$det['dias'],
                                "total"=>$det['total'],
                                "valor_ibc"=>$det['valor_ibc'],
                                "valor_ibc_para"=>$det['valor_ibc_para'],
                                "valor_ibc_arl"=>$det['valor_ibc_arl'],
                                "valor_arl"=>$det['valor_arl'],
                                "valor_salud_empleado"=>$det['valor_salud_empleado'],
                                "valor_salud_empresa"=>$det['valor_salud_empresa'],
                                "valor_total_salud"=>$det['valor_total_salud'],
                                "valor_pension_empleado"=>$det['valor_pension_empleado'],
                                "valor_pension_empresa"=>$det['valor_pension_empresa'],
                                "valor_total_pension"=>$det['valor_total_pension'],
                                "valor_fondo"=>$det['valor_fondo'],
                                "valor_retencion"=>$det['valor_retencion'],
                                "valor_descuento"=>$det['valor_descuento'],
                                "valor_total_descuentos"=>$det['valor_total_descuentos'],
                                "valor_neto"=>$det['valor_neto'],
                                "valor_aporte_ccf"=>$det['valor_aporte_ccf'],
                                "valor_aporte_sena"=>$det['valor_aporte_sena'],
                                "valor_aporte_icbf"=>$det['valor_aporte_icbf'],
                                "valor_aporte_inst"=>$det['valor_aporte_inst'],
                                "valor_aporte_esap"=>$det['valor_aporte_esap'],
                            ));
                        }
                        $netoAporteCcf += $totalAporteCcf;
                        $netoAporteArl += $totalArl;
                        $netoAporteIcbf += $totalAporteIcbf;
                        $netoAporteSena += $totalAporteSena;
                        $netoAporteEsap += $totalAporteEsap;
                        $netoAporteIns += $totalAporteInst;
                        $netoValor += $totalValor;
                        $netoSaludEmpleado += $totalSaludEmpleado;
                        $netoSaludEmpresa += $totalSaludEmpresa;
                        $netoTotalSalud += $totalSalud;
                        $netoPensionEmpleado += $totalPensionEmpleado;
                        $netoPensionEmpresa += $totalPensionEmpresa;
                        $netoPensionTotal += $totalPension;
                        $netoRetencion += $totalRetencion;
                        $netoOtrosDescuentos += $totalOtroDesc;
                        $netoTotalDescuentos += $totalDescuentos;
                        $netoTotalPagado += $totalNeto;
                        $netoFsp += $totalFondoSolidario;
                        array_push($arrLiquidacion,array(
                            "type"=>"total",
                            "documento"=>$det['documento'],
                            "total"=> array(
                                "dias"=>$det['dias'],
                                'total_valor' =>$totalValor,
                                'total_base_ibc' => $totalBaseIBC,
                                'total_base_para' => $totalBasePara,
                                'total_base_arl' => $totalBaseArl,
                                'total_arl' => $totalArl,
                                'total_salud_empleado' => $totalSaludEmpleado,
                                'total_salud_empresa' => $totalSaludEmpresa,
                                'total_salud' => $totalSalud,
                                'total_pension_empleado' => $totalPensionEmpleado,
                                'total_pension_empresa' => $totalPensionEmpresa,
                                'total_pension' => $totalPension,
                                'total_fondo' => $totalFondoSolidario,
                                'total_retencion' => $totalRetencion,
                                'total_otros_desc' => $totalOtroDesc,
                                'total_descuentos' => $totalDescuentos,
                                'total_neto' => $totalNeto,
                                'total_aporte_ccf' => $totalAporteCcf,
                                'total_aporte_sena' => $totalAporteSena,
                                'total_aporte_icbf' => $totalAporteIcbf,
                                'total_aporte_inst' => $totalAporteInst,
                                'total_aporte_esap' => $totalAporteEsap,
                            )
                        ));
                    }
                    $arrLiquidacion = $this->validData($request['vigencia'],$request['codigo_mes'],$arrLiquidacion);
                    $arrResponse = array(
                        "status"=>true,
                        "data"=>array(
                            "id"=>$request['id_nom'],
                            "mes"=>$request['mes'],
                            "fecha"=>$request['fecha'],
                            "estado"=>$request['estado'],
                            "vigencia"=>$request['vigencia'],
                            "detalle"=>array(
                                "liquidacion"=>$arrLiquidacion,
                                "neto"=>[
                                    "neto_ccf"=>$netoAporteCcf,
                                    "neto_arl"=>$netoAporteArl,
                                    "neto_icbf"=>$netoAporteIcbf,
                                    "neto_sena"=>$netoAporteSena,
                                    "neto_esap"=>$netoAporteEsap,
                                    "neto_inst"=>$netoAporteIns,
                                    "neto_devengado"=>$netoValor,
                                    "neto_salud_empleado"=>$netoSaludEmpleado,
                                    "neto_salud_empresa"=>$netoSaludEmpresa,
                                    "neto_total_salud"=>$netoTotalSalud,
                                    "neto_pension_empleado"=>$netoPensionEmpleado,
                                    "neto_pension_empresa"=>$netoPensionEmpresa,
                                    "neto_total_pension"=>$netoPensionTotal,
                                    "neto_fsp"=>$netoFsp,
                                    "neto_retencion"=>$netoRetencion,
                                    "neto_otros_descuentos"=>$netoOtrosDescuentos,
                                    "neto_total_descuentos"=>$netoTotalDescuentos,
                                    "neto_total_pagado"=>$netoTotalPagado
                                ],
                            ),
                        ),
                        "consecutivos"=>getConsecutivos("humnomina ","id_nom")
                    );
                }else{
                    $arrResponse = array("status"=>false,"msg"=>"La liquidación no existe");
                }
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
            die();
        }
        public function validData($intVigencia,$intMes, $arrLiquidacion){
            $totalLiquidacion = count($arrLiquidacion);
            $arrFormato = $this->selectFormato($intVigencia,$intMes);
            $arrTerceros = array_values(array_unique(array_column($arrFormato,"documento")));
            foreach ($arrTerceros as $tercero) {
                $detLiquidacion = array_filter($arrLiquidacion,function($e) use($tercero){return $tercero == $e['documento'];});
                if(!empty($detLiquidacion)){
                    $detFormato = array_values(array_filter($arrFormato,function($e) use($tercero){return $tercero == $e['documento'];}));
                    $vacacionesFormato = array_values(array_filter($detFormato,function($e){return $e['vacaciones']==1;}));
                    $incapacidadFormato = array_values(array_filter($detFormato,function($e){return $e['incapacidad']==1;}));
                    $sueldoFormato = array_values(array_filter($detFormato,function($e){return $e['incapacidad']==0 && $e['vacaciones']==0 ;}))[0];
                    $liquidacion = array_values(array_filter($detLiquidacion,function($e){return $e['type']=="total";}))[0]['total'];
                    $liquidacionVacaciones = array_values(array_filter($detLiquidacion,function($e){return $e['nombre']=="VACACIONES - SUELDO PERSONAL DE NOMINA";}));
                    $liquidacionIncapacidad = array_values(array_filter($detLiquidacion,function($e){return $e['nombre']=="INCAPACIDADES - SUELDO PERSONAL DE NOMINA";}));

                    $flagVacaciones = 0;
                    $flagIncapacidad = 0;
                    $flagTotal = 0;
                    if($sueldoFormato['dias'] == $liquidacion['dias'] && $sueldoFormato['pension_empleado'] == $liquidacion['total_pension_empleado'] &&
                    $sueldoFormato['pension_empresa'] == $liquidacion['total_pension_empresa'] && $sueldoFormato['fondo_solidaridad'] == $liquidacion['total_fondo']
                    && $sueldoFormato['salud_empleado'] == $liquidacion['total_salud_empleado'] && $sueldoFormato['salud_empresa'] == $liquidacion['total_salud_empresa']
                    && $sueldoFormato['cajacf'] == $liquidacion['total_aporte_ccf'] && $sueldoFormato['sena'] == $liquidacion['total_aporte_sena']
                    && $sueldoFormato['icbf'] == $liquidacion['total_aporte_icbf'] && $sueldoFormato['instecnicos'] == $liquidacion['total_aporte_inst']
                    && $sueldoFormato['esap'] == $liquidacion['total_aporte_esap'] ){

                        $flagTotal = 1;
                    }

                    if(!empty($vacacionesFormato)){
                        $data = $vacacionesFormato[0];
                        $dataLiq = $liquidacionVacaciones[0];
                        if($data['dias'] == $dataLiq['dias'] && $data['pension_empleado'] == $dataLiq['valor_pension_empleado'] &&
                        $data['pension_empresa'] == $dataLiq['valor_pension_empresa'] && $data['fondo_solidaridad'] == $dataLiq['valor_fondo']
                        && $data['salud_empleado'] == $dataLiq['valor_salud_empleado'] && $data['salud_empresa'] == $dataLiq['valor_salud_empresa']
                        && $data['cajacf'] == $dataLiq['valor_aporte_ccf'] && $data['sena'] == $dataLiq['valor_aporte_sena']
                        && $data['icbf'] == $dataLiq['valor_aporte_icbf'] && $data['instecnicos'] == $dataLiq['valor_aporte_inst']
                        && $data['esap'] == $dataLiq['valor_aporte_esap'] ){
                            $flagVacaciones = 1;
                        }
                    }
                    if(!empty($incapacidadFormato)){
                        $data = $incapacidadFormato[0];
                        $dataLiq = $liquidacionIncapacidad[0];
                        if($data['dias'] == $dataLiq['dias'] && $data['pension_empleado'] == $dataLiq['valor_pension_empleado'] &&
                        $data['pension_empresa'] == $dataLiq['valor_pension_empresa'] && $data['fondo_solidaridad'] == $dataLiq['valor_fondo']
                        && $data['salud_empleado'] == $dataLiq['valor_salud_empleado'] && $data['salud_empresa'] == $dataLiq['valor_salud_empresa']
                        && $data['cajacf'] == $dataLiq['valor_aporte_ccf'] && $data['sena'] == $dataLiq['valor_aporte_sena']
                        && $data['icbf'] == $dataLiq['valor_aporte_icbf'] && $data['instecnicos'] == $dataLiq['valor_aporte_inst']
                        && $data['esap'] == $dataLiq['valor_aporte_esap'] ){
                            $flagIncapacidad = 1;
                        }
                    }
                }
                for ($i=0; $i < $totalLiquidacion ; $i++) {
                    $data = $arrLiquidacion[$i];
                    if($data['documento'] == $tercero){
                        if($data['nombre']=="VACACIONES - SUELDO PERSONAL DE NOMINA"){
                            $data['check'] = $flagVacaciones;
                        }else if($data['nombre']=="INCAPACIDADES - SUELDO PERSONAL DE NOMINA"){
                            $data['check'] = $flagIncapacidad;
                        }else if($data['nombre']!="INCAPACIDADES - SUELDO PERSONAL DE NOMINA" && $data['nombre']!="VACACIONES - SUELDO PERSONAL DE NOMINA"){
                            $data['check'] = $flagTotal;
                        }
                        $arrLiquidacion[$i] = $data;
                    }
                }
            }
            return $arrLiquidacion;
        }
    }
    if($_POST){
        $obj = new ValidarController();
        if($_POST['action'] == "save"){
            $obj->save();
        }else if($_POST['action']=="edit"){
            $obj->getEdit();
        }else if($_POST['action']=="get"){
            $obj->getData();
        }
    }

?>
