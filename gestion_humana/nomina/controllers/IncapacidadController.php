<?php
    require_once '../../../Librerias/core/Helpers.php';
    require_once '../../../PHPExcel/Classes/PHPExcel.php';
    require_once '../models/IncapacidadModel.php';
    session_start();

    class IncapacidadController extends IncapacidadModel{
        public function getData(){
            if(!empty($_SESSION)){
                $arrResponse = [
                    "meses"=>$this->selectMeses(),
                    "contratos"=>$this->selectContratos(),
                    "licencias"=>$this->selectLicencias(),
                    "porcentajes"=>$this->selectPorcentajes(),
                    "generales"=>$this->selectGenerales()
                ];
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
        }
        public function save(){
            if(!empty($_SESSION)){
                $arrData = json_decode($_POST['data'],true);
                $arrContrato = json_decode($_POST['contrato'],true);
                if( empty($_POST['fecha_final'])  || empty($arrData)|| empty($arrContrato)){
                    $arrResponse = array("status"=>false,"msg"=>"Error de datos");
                }else{
                    $strTipo = strClean($_POST['tipo']);
                    $strFecha = strClean($_POST['fecha']);
                    $strFechaInicial = strClean($_POST['fecha_inicial']);
                    $strFechaFinal = strClean($_POST['fecha_final']);
                    $strCheckArl = strClean($_POST['check_arl']);
                    $strCheckEps = strClean($_POST['check_eps']);
                    $strCheckPara = strClean($_POST['check_para']);
                    $intConsecutivo = intval($_POST['consecutivo']);
                    $strCodEps = strtoupper(strClean($_POST['cod_eps']));
                    $strCodEpsAnt = strtoupper(strClean($_POST['cod_eps_ant']));
                    $intVigencia = explode("-",$strFecha)[0];
                    $arrMeses = [];
                    $totalValor = 0;
                    $totalVacaciones = 0;
                    foreach ($arrData as $data) {
                        array_push($arrMeses,$data['nombre']);
                        $totalValor+=$data['valor_total'];
                        $totalVacaciones+=$data['dias'];
                    }
                    $strMeses = implode(" - ",array_unique($arrMeses));
                    $strMeses = ucwords(strtolower($strMeses));
                    $opcion="";
                    if($intConsecutivo == 0){
                        $opcion = 1;
                        $request = $this->insertData(
                            $strFecha,
                            $strFechaInicial,
                            $strFechaFinal,
                            $strCheckArl,
                            $strCheckEps,
                            $strCheckPara,
                            $strMeses,
                            $totalValor,
                            $totalVacaciones,
                            $arrContrato,
                            $strCodEps,
                            $strCodEpsAnt,
                            $intVigencia,
                            $strTipo
                        );
                        $id = $request;
                    }else{
                        $opcion = 2;
                        $id = $intConsecutivo;
                        $request = $this->updateData(
                            $intConsecutivo,
                            $strFecha,
                            $strFechaInicial,
                            $strFechaFinal,
                            $strCheckArl,
                            $strCheckEps,
                            $strCheckPara,
                            $strMeses,
                            $totalValor,
                            $totalVacaciones,
                            $arrContrato,
                            $strCodEps,
                            $strCodEpsAnt,
                            $intVigencia,
                            $strTipo
                        );
                    }
                    if($request > 0){
                        $this->insertDet($id,$arrData,$arrContrato,$strTipo);
                        if($opcion == 1){
                            insertAuditoria("hum_auditoria","hum_funciones_id",7,"Crear",$request,"Novedad de incapacidades","hum_funciones");
                            $arrResponse = array("status"=>true,"msg"=>"Datos guardados correctamente.","id"=>$request);
                        }else{
                            insertAuditoria("hum_auditoria","hum_funciones_id",7,"Editar",$intConsecutivo,"Novedad de incapacidades","hum_funciones");
                            $arrResponse = array("status"=>true,"msg"=>"Datos actualizados correctamente.");
                        }
                    }else if($request =="existe"){
                        $arrResponse = array("status"=>false,"msg"=>"El contrato ya tiene este descuento asignado, debe pagarlo por completo o desactivarlo para crear uno nuevo.");
                    }else{
                        $arrResponse = array("status"=>false,"msg"=>"No se ha podido guardar, inténte de nuevo.");
                    }
                }
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
            die();
        }
        public function getEdit(){
            if(!empty($_SESSION)){
                $intId = intval($_POST['codigo']);
                $request = $this->selectEdit($intId);
                if(!empty($request)){
                    $arrLicencias = $this->selectLicencias();
                    $arrContratos = $this->selectContratos();
                    $arrMeses = $this->selectMeses();
                    $contrato = array_values(array_filter($arrContratos,function($e)use($request){return $request['doc_funcionario'] == $e['tercero'];}))[0];
                    $contrato['salario'] = $request['salario'];
                    $request['contrato'] = $contrato;
                    $request['paga_ibc'] = $request['paga_ibc'] == "S" ? 1 : 0;
                    $request['paga_arl'] = $request['paga_arl'] == "S" ? 1 : 0;
                    $request['paga_para'] = $request['paga_para'] == "S" ? 1 : 0;
                    $arrResponse = array(
                    "data" => $request,
                    "status"=>true,
                    "meses"=>$arrMeses,
                    "contratos"=>$arrContratos,
                    "licencias"=>$arrLicencias,
                    "porcentajes"=>$this->selectPorcentajes(),
                    "generales"=>$this->selectGenerales(),
                    "consecutivos"=>getConsecutivos("hum_incapacidades","num_inca"));
                }else{
                    $arrResponse = array("status"=>false,"consecutivo"=>searchConsec("hum_incapacidades ","num_inca")-1);
                }
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
            die();
        }
        public function getSearch(){
            if(!empty($_SESSION)){
                $strFechaInicial = strClean($_POST['fecha_inicial']);
                $strFechaFinal = strClean($_POST['fecha_final']);
                $strCodigo = strClean($_POST['codigo']);
                $strTercero = strClean($_POST['tercero']);
                $request = $this->selectSearch($strFechaInicial,$strFechaFinal,$strCodigo,$strTercero);
                echo json_encode($request,JSON_UNESCAPED_UNICODE);
            }
            die();
        }
        public function changeData(){
            if(!empty($_SESSION)){
                $id = intval($_POST['id']);
                $estado = $_POST['estado'];
                $request = $this->updateStatus($id,$estado);
                if($request == 1){
                    insertAuditoria("hum_auditoria","hum_funciones_id",7,"Editar estado",$id,"Novedad de incapacidades","hum_funciones");
                    $arrResponse = array("status"=>true);
                }else{
                    $arrResponse = array("status"=>false,"msg"=>"Error");
                }
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
            die();
        }
    }

    if($_POST){
        $obj = new IncapacidadController();
        if($_POST['action'] == "save"){
            $obj->save();
        }else if($_POST['action']=="edit"){
            $obj->getEdit();
        }else if($_POST['action']=="search"){
            $obj->getSearch();
        }else if($_POST['action']=="change"){
            $obj->changeData();
        }else if($_POST['action']=="get"){
            $obj->getData();
        }
    }

?>
