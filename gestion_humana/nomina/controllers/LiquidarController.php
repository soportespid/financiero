<?php
    require_once '../../../Librerias/core/Helpers.php';
    require_once '../../../PHPExcel/Classes/PHPExcel.php';
    require_once '../models/LiquidarModel.php';
    require_once '../models/LiquidarTrait.php';
    session_start();

    class LiquidarController extends LiquidarModel{
        use LiquidarTrait;
        public function getData(){
            if(!empty($_SESSION)){
                $request = $this->selectNovedades();
                echo json_encode($request,JSON_UNESCAPED_UNICODE);
            }
        }
        public function save(){
            if(!empty($_SESSION)){
                $arrData = json_decode($_POST['data'],true);
                $arrNovedad = json_decode($_POST['novedad'],true);
                if( empty($arrData) || empty($arrNovedad)){
                    $arrResponse = array("status"=>false,"msg"=>"Error de datos");
                }else{
                    $strFecha = strClean($_POST['fecha']);
                    $intMes = intval($arrNovedad['mes']);
                    $intVigencia = intval($arrNovedad['vigencia']);
                    $strDescripcion = strClean($arrNovedad['descripcion']);
                    $intIdNovedad = intval($arrNovedad['id']);
                    $arrDescuentos = json_decode($_POST['descuentos'],true);
                    $arrRetenciones = json_decode($_POST['retenciones'],true);
                    $request = $this->insertLiquidacion($strFecha,$intMes,$intVigencia,$intIdNovedad,$strDescripcion);
                    if($request > 0){
                        $this->insertLiquidacionDet($request,$arrData,$arrDescuentos,$arrRetenciones);
                        insertAuditoria("hum_auditoria","hum_funciones_id",4,"Crear",$request,"Liquidaciones de nomina","hum_funciones");
                        $arrResponse = array("status"=>true,"msg"=>"Datos guardados correctamente.","id"=>$request);
                    }else{
                        $arrResponse = array("status"=>false,"msg"=>"No se ha podido guardar, inténte de nuevo.");
                    }
                }
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
            die();
        }
        public function getEdit(){
            if(!empty($_SESSION)){
                $intId = intval($_POST['codigo']);
                $request = $this->selectLiquidacion($intId);
                if(!empty($request)){
                    $arrLiquidacion = [];
                    $arrDet = $request['detalle'];
                    $arrTerceros = array_values(array_unique(array_column($arrDet,"documento")));
                    $netoAporteCcf = 0;
                    $netoAporteArl = 0;
                    $netoAporteIcbf = 0;
                    $netoAporteSena = 0;
                    $netoAporteEsap = 0;
                    $netoAporteIns = 0;
                    $netoValor = 0;
                    $netoFsp = 0;
                    $netoSaludEmpleado = 0;
                    $netoSaludEmpresa = 0;
                    $netoTotalSalud = 0;
                    $netoPensionEmpleado = 0;
                    $netoPensionEmpresa = 0;
                    $netoPensionTotal = 0;
                    $netoRetencion = 0;
                    $netoOtrosDescuentos = 0;
                    $netoTotalDescuentos = 0;
                    $netoTotalPagado = 0;
                    foreach ($arrTerceros as $tercero) {
                        $detalle = array_values(array_filter($arrDet,function($e)use($tercero){return $tercero == $e['documento'];}));
                        $totalValor = 0;
                        $totalBaseIBC = 0;
                        $totalBasePara = 0;
                        $totalBaseArl = 0;
                        $totalSaludEmpleado = 0;
                        $totalSaludEmpresa = 0;
                        $totalPensionEmpleado = 0;
                        $totalPensionEmpresa = 0;
                        $totalSalud = 0;
                        $totalPension = 0;
                        $totalFondoSolidario = 0;
                        $totalArl = 0;
                        $totalRetencion = 0;
                        $totalOtroDesc = 0;
                        $totalDescuentos = 0;
                        $totalNeto = 0;
                        $totalAporteCcf=0;
                        $totalAporteSena = 0;
                        $totalAporteIcbf = 0;
                        $totalAporteInst = 0;
                        $totalAporteEsap = 0;
                        foreach ($detalle as $det) {
                            $totalBaseIBC+=$det['valor_ibc'];
                            $totalBasePara +=$det['valor_ibc_para'];
                            $totalBaseArl +=$det['valor_ibc_arl'];
                            $totalSaludEmpleado +=$det['valor_salud_empleado'];
                            $totalSaludEmpresa +=$det['valor_salud_empresa'];
                            $totalPensionEmpleado +=$det['valor_pension_empleado'];
                            $totalPensionEmpresa +=$det['valor_pension_empresa'];
                            $totalSalud +=$det['valor_total_salud'];
                            $totalPension +=$det['valor_total_pension'];
                            $totalFondoSolidario +=$det['valor_fondo'];
                            $totalArl+=$det['valor_arl'];
                            $totalOtroDesc+=$det['valor_descuento'];
                            $totalRetencion+=$det['valor_retencion'];
                            $totalDescuentos+=$det['valor_total_descuentos'];
                            $totalAporteCcf+=$det['valor_aporte_ccf'];
                            $totalAporteEsap+=$det['valor_aporte_esap'];
                            $totalAporteIcbf+=$det['valor_aporte_icbf'];
                            $totalAporteInst+=$det['valor_aporte_inst'];
                            $totalAporteSena+=$det['valor_aporte_sena'];
                            $totalNeto += $det['valor_neto'];
                            $totalValor+=$det['total'];
                            array_push($arrLiquidacion,array(
                                "type"=>"det",
                                "tipo_pago"=>$det['tipo_pago'],
                                "id"=>$det['id'],
                                "nombre"=>$det['descripcion'],
                                "nombre_tercero"=>$det['nombre'],
                                "documento"=>$det['documento'],
                                "salario"=>$det['salario'],
                                "dias"=>$det['dias'],
                                "total"=>$det['total'],
                                "valor_ibc"=>$det['valor_ibc'],
                                "valor_ibc_para"=>$det['valor_ibc_para'],
                                "valor_ibc_arl"=>$det['valor_ibc_arl'],
                                "valor_arl"=>$det['valor_arl'],
                                "valor_salud_empleado"=>$det['valor_salud_empleado'],
                                "valor_salud_empresa"=>$det['valor_salud_empresa'],
                                "valor_total_salud"=>$det['valor_total_salud'],
                                "valor_pension_empleado"=>$det['valor_pension_empleado'],
                                "valor_pension_empresa"=>$det['valor_pension_empresa'],
                                "valor_total_pension"=>$det['valor_total_pension'],
                                "valor_fondo"=>$det['valor_fondo'],
                                "valor_retencion"=>$det['valor_retencion'],
                                "valor_descuento"=>$det['valor_descuento'],
                                "valor_total_descuentos"=>$det['valor_total_descuentos'],
                                "valor_neto"=>$det['valor_neto'],
                                "valor_aporte_ccf"=>$det['valor_aporte_ccf'],
                                "valor_aporte_sena"=>$det['valor_aporte_sena'],
                                "valor_aporte_icbf"=>$det['valor_aporte_icbf'],
                                "valor_aporte_inst"=>$det['valor_aporte_inst'],
                                "valor_aporte_esap"=>$det['valor_aporte_esap'],
                            ));
                        }
                        $netoAporteCcf += $totalAporteCcf;
                        $netoAporteArl += $totalArl;
                        $netoAporteIcbf += $totalAporteIcbf;
                        $netoAporteSena += $totalAporteSena;
                        $netoAporteEsap += $totalAporteEsap;
                        $netoAporteIns += $totalAporteInst;
                        $netoValor += $totalValor;
                        $netoSaludEmpleado += $totalSaludEmpleado;
                        $netoSaludEmpresa += $totalSaludEmpresa;
                        $netoTotalSalud += $totalSalud;
                        $netoPensionEmpleado += $totalPensionEmpleado;
                        $netoPensionEmpresa += $totalPensionEmpresa;
                        $netoPensionTotal += $totalPension;
                        $netoRetencion += $totalRetencion;
                        $netoOtrosDescuentos += $totalOtroDesc;
                        $netoTotalDescuentos += $totalDescuentos;
                        $netoTotalPagado += $totalNeto;
                        $netoFsp += $totalFondoSolidario;
                        array_push($arrLiquidacion,array(
                            "type"=>"total",
                            "total"=> array(
                                'total_valor' =>$totalValor,
                                'total_base_ibc' => $totalBaseIBC,
                                'total_base_para' => $totalBasePara,
                                'total_base_arl' => $totalBaseArl,
                                'total_arl' => $totalArl,
                                'total_salud_empleado' => $totalSaludEmpleado,
                                'total_salud_empresa' => $totalSaludEmpresa,
                                'total_salud' => $totalSalud,
                                'total_pension_empleado' => $totalPensionEmpleado,
                                'total_pension_empresa' => $totalPensionEmpresa,
                                'total_pension' => $totalPension,
                                'total_fondo' => $totalFondoSolidario,
                                'total_retencion' => $totalRetencion,
                                'total_otros_desc' => $totalOtroDesc,
                                'total_descuentos' => $totalDescuentos,
                                'total_neto' => $totalNeto,
                                'total_aporte_ccf' => $totalAporteCcf,
                                'total_aporte_sena' => $totalAporteSena,
                                'total_aporte_icbf' => $totalAporteIcbf,
                                'total_aporte_inst' => $totalAporteInst,
                                'total_aporte_esap' => $totalAporteEsap,
                            )
                        ));
                    }
                    $arrResponse = array(
                        "status"=>true,
                        "data"=>array(
                            "id"=>$request['id_nom'],
                            "mes"=>$request['mes'],
                            "fecha"=>$request['fecha'],
                            "estado"=>$request['estado'],
                            "vigencia"=>$request['vigencia'],
                            "detalle"=>array(
                                "liquidacion"=>$arrLiquidacion,
                                "neto"=>[
                                    "neto_ccf"=>$netoAporteCcf,
                                    "neto_arl"=>$netoAporteArl,
                                    "neto_icbf"=>$netoAporteIcbf,
                                    "neto_sena"=>$netoAporteSena,
                                    "neto_esap"=>$netoAporteEsap,
                                    "neto_inst"=>$netoAporteIns,
                                    "neto_devengado"=>$netoValor,
                                    "neto_salud_empleado"=>$netoSaludEmpleado,
                                    "neto_salud_empresa"=>$netoSaludEmpresa,
                                    "neto_total_salud"=>$netoTotalSalud,
                                    "neto_pension_empleado"=>$netoPensionEmpleado,
                                    "neto_pension_empresa"=>$netoPensionEmpresa,
                                    "neto_total_pension"=>$netoPensionTotal,
                                    "neto_fsp"=>$netoFsp,
                                    "neto_retencion"=>$netoRetencion,
                                    "neto_otros_descuentos"=>$netoOtrosDescuentos,
                                    "neto_total_descuentos"=>$netoTotalDescuentos,
                                    "neto_total_pagado"=>$netoTotalPagado
                                ],
                            ),
                        ),
                        "consecutivos"=>getConsecutivos("humnomina ","id_nom")
                    );
                }else{
                    $arrResponse = array("status"=>false,"consecutivo"=>searchConsec("humnomina ","id_nom")-1);
                }
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
            die();
        }
        public function getSearch(){
            if(!empty($_SESSION)){
                $strFechaInicial = strClean($_POST['fecha_inicial']);
                $strFechaFinal = strClean($_POST['fecha_final']);
                $strCodigo = strClean($_POST['codigo']);
                $request = $this->selectLiquidacionesSearch($strFechaInicial,$strFechaFinal,$strCodigo);
                echo json_encode($request,JSON_UNESCAPED_UNICODE);
            }
            die();
        }
        public function changeData(){
            if(!empty($_SESSION)){
                $id = intval($_POST['id']);
                $request = $this->updateLiquidacion($id);
                if($request == 1){
                    insertAuditoria("hum_auditoria","hum_funciones_id",4,"Anular",$id,"Liquidaciones de nomina","hum_funciones");
                    $arrResponse = array("status"=>true);
                }else if($request == "existe"){
                    $arrResponse = array("status"=>false,"msg"=>"La liquidación ya tiene solicitud de cdp, deberá anularlo.");
                }else{
                    $arrResponse = array("status"=>false,"msg"=>"Ha ocurrido un error, inténtelo de nuevo.");
                }
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
            die();
        }
        public function getSelectNov(){
            if(!empty($_SESSION)){
                $intCodigo = intval($_POST['codigo']);
                $arrNovedades = $this->selectNovedad($intCodigo);
                $arrNovedades = $this->getCalcNovedades($arrNovedades);
                $arrData = $this->getDataT($arrNovedades);
                echo json_encode($arrData,JSON_UNESCAPED_UNICODE);
            }
            die();
        }
        public function getCalcNovedades(array $arrNovedades){
            $arrParafiscales = $arrNovedades[0]['parafiscales'];
            $arrRetenciones = $arrNovedades[0]['retenciones'];
            $arrFondoSolidario = $this->selectFondoSolidario();
            $netoAporteCcf = 0;
            $netoAporteArl = 0;
            $netoAporteIcbf = 0;
            $netoAporteSena = 0;
            $netoAporteEsap = 0;
            $netoAporteIns = 0;
            $netoValor = 0;
            $netoFsp = 0;
            $netoSaludEmpleado = 0;
            $netoSaludEmpresa = 0;
            $netoTotalSalud = 0;
            $netoPensionEmpleado = 0;
            $netoPensionEmpresa = 0;
            $netoPensionTotal = 0;
            $netoRetencion = 0;
            $netoOtrosDescuentos = 0;
            $netoTotalDescuentos = 0;
            $netoTotalPagado = 0;
            $total = count($arrNovedades);
            for ($i=0; $i < $total; $i++) {
                $totalValor = 0;
                $totalBaseIBC = 0;
                $totalBasePara = 0;
                $totalBaseArl = 0;
                $totalSaludEmpleado = 0;
                $totalSaludEmpresa = 0;
                $totalPensionEmpleado = 0;
                $totalPensionEmpresa = 0;
                $totalSalud = 0;
                $totalPension = 0;
                $totalFondoSolidario = 0;
                $totalArl = 0;
                $totalRetencion = 0;
                $totalOtroDesc = 0;
                $totalDescuentos = 0;
                $totalNeto = 0;
                $totalAporteCcf=0;
                $totalAporteSena = 0;
                $totalAporteIcbf = 0;
                $totalAporteInst = 0;
                $totalAporteEsap = 0;
                $arrNovDet = $arrNovedades[$i]['novedades'];
                $totalDet = count($arrNovDet);
                for ($j=0; $j < $totalDet; $j++) {
                    $det = $arrNovDet[$j];
                    $intDescuento = $det['descuento'];
                    $intValor = $det['total'];
                    $intBaseIBC = 0;
                    $intBasePara = 0;
                    $intBaseArl = 0;
                    $intSaludEmpleado = 0;
                    $intSaludEmpresa = 0;
                    $intPensionEmpleado = 0;
                    $intPensionEmpresa = 0;
                    $intTotalSalud = 0;
                    $intTotalPension = 0;
                    $intFondoSolidario = 0;
                    $intRetencion = $det['retencion'];
                    $intAporteCcf=0;
                    $intAporteSena = 0;
                    $intAporteIcbf = 0;
                    $intAporteInst = 0;
                    $intAporteEsap = 0;
                    $intArl = 0;

                    $data = $this->calcNovedad($det,$arrFondoSolidario);
                    $intBaseArl =  $data['base_arl'];
                    $intBaseIBC = $data['base_ibc'];
                    $intBasePara = $data['base_para'];
                    $intArl = $data['valor_arl'];
                    $intSaludEmpleado = $data['valor_salud_empleado'];
                    $intSaludEmpresa = $data['valor_salud_empresa'];
                    $intTotalSalud = $data['valor_total_salud'];
                    $intPensionEmpleado = $data['valor_pension_empleado'];
                    $intPensionEmpresa = $data['valor_pension_empresa'];
                    $intTotalPension = $data['valor_total_pension'];
                    $intFondoSolidario = $data['valor_fondo'];
                    $intAporteCcf = $data['valor_aporte_ccf'];
                    $intAporteIcbf = $data['valor_aporte_icbf'];
                    $intAporteSena = $data['valor_aporte_sena'];
                    $intAporteInst = $data['valor_aporte_inst'];
                    $intAporteEsap = $data['valor_aporte_esap'];

                    $intTotalDescuentos = $intDescuento+$intFondoSolidario+$intSaludEmpleado+$intPensionEmpleado+$intRetencion;
                    if(isset($det['novedad']) && $det['novedad'] == 2){
                        $intValor = 0;
                        $intNeto = $intValor-$intTotalDescuentos;
                        $totalNeto+=0;
                    }else{
                        $intNeto = $intValor-$intTotalDescuentos;
                        $totalNeto+=$intNeto;
                    }
                    $totalBaseIBC+=$intBaseIBC;
                    $totalBasePara +=$intBasePara;
                    $totalBaseArl +=$intBaseArl;
                    $totalSaludEmpleado +=$intSaludEmpleado;
                    $totalSaludEmpresa +=$intSaludEmpresa;
                    $totalSalud +=$intTotalSalud;
                    $totalPensionEmpleado +=$intPensionEmpleado;
                    $totalPensionEmpresa +=$intPensionEmpresa;
                    $totalPension +=$intTotalPension;
                    $totalFondoSolidario +=$intFondoSolidario;
                    $totalArl+=$intArl;
                    $totalOtroDesc+=$intDescuento;
                    $totalRetencion+=$intRetencion;
                    $totalDescuentos+=$intTotalDescuentos;
                    $totalAporteCcf+=$intAporteCcf;
                    $totalAporteEsap+=$intAporteEsap;
                    $totalAporteIcbf+=$intAporteIcbf;
                    $totalAporteInst+=$intAporteInst;
                    $totalAporteSena+=$intAporteSena;

                    $totalValor+=$intValor;
                    $det['valor_ibc'] = $intBaseIBC;
                    $det['valor_ibc_para'] = $intBasePara;
                    $det['valor_ibc_arl'] = $intBaseArl;
                    $det['valor_salud_empleado'] = $intSaludEmpleado;
                    $det['valor_salud_empresa'] = $intSaludEmpresa;
                    $det['valor_total_salud'] = $intTotalSalud;
                    $det['valor_pension_empleado'] = $intPensionEmpleado;
                    $det['valor_pension_empresa'] = $intPensionEmpresa;
                    $det['valor_total_pension'] = $intTotalPension;
                    $det['valor_fondo'] = $intFondoSolidario;
                    $det['valor_descuento'] = $intDescuento;
                    $det['valor_retencion'] = $intRetencion;
                    $det['valor_total_descuentos'] = $intTotalDescuentos;
                    $det['valor_aporte_ccf'] = $intAporteCcf;
                    $det['valor_aporte_sena'] = $intAporteSena;
                    $det['valor_aporte_icbf'] = $intAporteIcbf;
                    $det['valor_aporte_inst'] = $intAporteInst;
                    $det['valor_aporte_esap'] = $intAporteEsap;
                    $det['valor_arl'] = $intArl;
                    $det['valor_neto'] = $intNeto;
                    $arrNovDet[$j] = $det;

                }

                $netoAporteCcf += $totalAporteCcf;
                $netoAporteArl += $totalArl;
                $netoAporteIcbf += $totalAporteIcbf;
                $netoAporteSena += $totalAporteSena;
                $netoAporteEsap += $totalAporteEsap;
                $netoAporteIns += $totalAporteInst;
                $netoValor += $totalValor;
                $netoSaludEmpleado += $totalSaludEmpleado;
                $netoSaludEmpresa += $totalSaludEmpresa;
                $netoTotalSalud += $totalSalud;
                $netoPensionEmpleado += $totalPensionEmpleado;
                $netoPensionEmpresa += $totalPensionEmpresa;
                $netoPensionTotal += $totalPension;
                $netoRetencion += $totalRetencion;
                $netoOtrosDescuentos += $totalOtroDesc;
                $netoTotalDescuentos += $totalDescuentos;
                $netoTotalPagado += $totalNeto;
                $netoFsp += $totalFondoSolidario;

                $arrNovedades[$i]['totales']['total_valor'] = $totalValor;
                $arrNovedades[$i]['totales']['total_base_ibc'] = $totalBaseIBC;
                $arrNovedades[$i]['totales']['total_base_para'] = $totalBasePara;
                $arrNovedades[$i]['totales']['total_base_arl'] = $totalBaseArl;
                $arrNovedades[$i]['totales']['total_arl'] = $totalArl;
                $arrNovedades[$i]['totales']['total_salud_empleado'] = $totalSaludEmpleado;
                $arrNovedades[$i]['totales']['total_salud_empresa'] = $totalSaludEmpresa;
                $arrNovedades[$i]['totales']['total_salud'] = $totalSalud;
                $arrNovedades[$i]['totales']['total_pension_empleado'] = $totalPensionEmpleado;
                $arrNovedades[$i]['totales']['total_pension_empresa'] = $totalPensionEmpresa;
                $arrNovedades[$i]['totales']['total_pension'] = $totalPension;
                $arrNovedades[$i]['totales']['total_fondo'] = $totalFondoSolidario;
                $arrNovedades[$i]['totales']['total_retencion'] = $totalRetencion;
                $arrNovedades[$i]['totales']['total_otros_desc'] = $totalOtroDesc;
                $arrNovedades[$i]['totales']['total_descuentos'] = $totalDescuentos;
                $arrNovedades[$i]['totales']['total_neto'] = $totalNeto;
                $arrNovedades[$i]['totales']['total_aporte_ccf'] = $totalAporteCcf;
                $arrNovedades[$i]['totales']['total_aporte_sena'] = $totalAporteSena;
                $arrNovedades[$i]['totales']['total_aporte_icbf'] = $totalAporteIcbf;
                $arrNovedades[$i]['totales']['total_aporte_inst'] = $totalAporteInst;
                $arrNovedades[$i]['totales']['total_aporte_esap'] = $totalAporteEsap;
                $arrNovedades[$i]['novedades'] = $arrNovDet;
            }
            $arrParafiscales['aporte_ccf']['valor'] = $netoAporteCcf;
            $arrParafiscales['aporte_sena']['valor'] = $netoAporteSena;
            $arrParafiscales['aporte_icbf']['valor'] = $netoAporteIcbf;
            $arrParafiscales['aporte_inst']['valor'] = $netoAporteIns;
            $arrParafiscales['aporte_esap']['valor'] = $netoAporteEsap;
            $arrParafiscales['salud_empresa']['valor'] = $netoTotalSalud;
            $arrParafiscales['pension_empresa']['valor'] = $netoPensionTotal;
            $arrParafiscales['aporte_arl']['nombre'] = "APORTES GENERALES AL SISTEMA DE RIESGOS LABORALES";
            $arrParafiscales['aporte_arl']['valor'] = $netoAporteArl;
            $arrData=array(
                "neto"=>[
                    "neto_ccf"=>$netoAporteCcf,
                    "neto_arl"=>$netoAporteArl,
                    "neto_icbf"=>$netoAporteIcbf,
                    "neto_sena"=>$netoAporteSena,
                    "neto_esap"=>$netoAporteEsap,
                    "neto_inst"=>$netoAporteIns,
                    "neto_devengado"=>$netoValor,
                    "neto_salud_empleado"=>$netoSaludEmpleado,
                    "neto_salud_empresa"=>$netoSaludEmpresa,
                    "neto_total_salud"=>$netoTotalSalud,
                    "neto_pension_empleado"=>$netoPensionEmpleado,
                    "neto_pension_empresa"=>$netoPensionEmpresa,
                    "neto_total_pension"=>$netoPensionTotal,
                    "neto_fsp"=>$netoFsp,
                    "neto_retencion"=>$netoRetencion,
                    "neto_otros_descuentos"=>$netoOtrosDescuentos,
                    "neto_total_descuentos"=>$netoTotalDescuentos,
                    "neto_total_pagado"=>$netoTotalPagado
                ],
                "parafiscales"=>$arrParafiscales,
                "novedades"=>$arrNovedades,
                "retenciones"=>$arrRetenciones
            );
            return $arrData;
        }
    }

    if($_POST){
        $obj = new LiquidarController();
        if($_POST['action'] == "save"){
            $obj->save();
        }else if($_POST['action']=="edit"){
            $obj->getEdit();
        }else if($_POST['action']=="search"){
            $obj->getSearch();
        }else if($_POST['action']=="change"){
            $obj->changeData();
        }else if($_POST['action']=="get"){
            $obj->getData();
        }else if($_POST['action']=="nov"){
            $obj->getSelectNov();
        }
    }

?>
