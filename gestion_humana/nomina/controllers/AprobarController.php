<?php
    require_once '../../../Librerias/core/Helpers.php';
    require_once '../../../PHPExcel/Classes/PHPExcel.php';
    require_once '../models/AprobarModel.php';
    session_start();

    class AprobarController extends AprobarModel{
        public function save(){
            if(!empty($_SESSION)){
                $arrTercero = json_decode($_POST['tercero'],true);
                $arrLiquidacion = json_decode($_POST['liquidacion'],true);
                $intMovimiento = intval($_POST['movimiento']);
                if($intMovimiento == 201){
                    if(empty($arrTercero) || empty($arrLiquidacion)){
                        $arrResponse = array("status"=>false,"msg"=>"Error de datos");
                    }else if($arrLiquidacion['total_solicitud'] != $arrLiquidacion['total_cdp'] || $arrLiquidacion['total_solicitud'] != $arrLiquidacion['total_rp'] ){
                        $arrResponse = array("status"=>false,"msg"=>"El valor de la solicitud, CDP y RP deben ser iguales.");
                    }else{
                        $strFecha = strClean($_POST['fecha']);
                        $intMes = intval($arrLiquidacion['mes']);
                        $intMes = $intMes < 10 ? "0".$intMes : $intMes;
                        $strMes = getMeses()[$intMes];
                        $request = $this->insertData($arrTercero,$arrLiquidacion,$strFecha,$intMovimiento);
                        if($request > 0){
                            $descripcion = "CAUSACION ".$arrLiquidacion['nomina']." MES $strMes";
                            $requestComp = $this->insertComprobanteCab($arrLiquidacion['nomina'],$descripcion,$strFecha,4);
                            if($requestComp > 0){
                                insertAuditoria("hum_auditoria","hum_funciones_id",9,"Crear",$request,"Aprobar nomina","hum_funciones");
                                $detalle = $this->selectNominaDetalle($arrLiquidacion['nomina']);
                                $detalle = $this->orderData($detalle,$strMes);
                                $this->insertComprobanteDet($arrLiquidacion['nomina'],$detalle);
                                $arrResponse = array("status"=>true,"msg"=>"Datos guardados correctamente.","id"=>$request);
                            }else{
                                $arrResponse = array("status"=>false,"msg"=>"Error al guardar comprobante.");
                            }
                        }else if($request =="existe"){
                            $arrResponse = array("status"=>false,"msg"=>"Esta liquidación ya fue aprobada, pruebe con otra.");
                        }else{
                            $arrResponse = array("status"=>false,"msg"=>"No se ha podido guardar, inténte de nuevo.");
                        }
                    }
                }else{
                    if(empty($_POST['descripcion_rev']) || empty($_POST['nomina_rev'])){
                        $arrResponse = array("status"=>false,"msg"=>"Error de datos");
                    }else{
                        $strDescripcion = strClean($_POST['descripcion_rev']);
                        $intNomina = intval($_POST['nomina_rev']);
                        $strFecha = strClean($_POST['fecha_rev']);
                        $intAprobado = intval($_POST['aprobado']);
                        $request = $this->insertReversar($strDescripcion,$intNomina,$strFecha,$intAprobado);
                        if($request > 0){
                            $arrResponse = array("status"=>true,"msg"=>"Datos guardados correctamente","id"=>$request);
                        }else if($request == "aprob"){
                            $arrResponse = array("status"=>false,"msg"=>"Error al actualizar la aprobación");
                        }else if($request == "aprov_rev"){
                            $arrResponse = array("status"=>false,"msg"=>"Error al guardar la aprobación reversada");
                        }else if($request == "comp_det"){
                            $arrResponse = array("status"=>false,"msg"=>"Error al guardar detalle de comprobante de ingreso reversado");
                        }else if($request == "comp"){
                            $arrResponse = array("status"=>false,"msg"=>"Error al guardar cabecera de comprobante de ingreso reversado");
                        }else if($request == "existe"){
                            $arrResponse = array("status"=>false,"msg"=>"Esta liquidación ya se ha reversado, intente con otro.");
                        }else if($request == "egreso"){
                            $arrResponse = array("status"=>false,"msg"=>"La liquidación ".$intNomina." ya tiene egreso, no puede reversar.");
                        }
                    }
                }
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
            die();
        }
        public function orderData(array $detalle,string $strMes){
            $total = count($detalle);
            $arrCuentas = [];
            if($total > 0){
                $arrVariablesPago = $this->selectVariablesPago();
                $arrSectores = $this->selectCuentasSectores();
                $arrParametros = $this->selectParametrosNomina();
                $arrVariablesPara = $this->selectVariablesPara();

                $arrSueldo = array_values(array_filter($arrVariablesPago,function($e) use($arrParametros){return $e['codigo'] == $arrParametros['sueldo'];}))[0];
                $arrTransporte = array_values(array_filter($arrVariablesPago,function($e) use($arrParametros){return $e['codigo'] == $arrParametros['aux_transporte'];}))[0];
                $arrAlimento = array_values(array_filter($arrVariablesPago,function($e) use($arrParametros){return $e['codigo'] == $arrParametros['sub_alimentacion'];}))[0];
                $arrPensionEmpresa = array_values(array_filter($arrVariablesPara,function($e) use($arrParametros){return $e['codigo'] == $arrParametros['pension_empleador'] && $e['sector']=="privado";}))[0];
                $arrPensionEmpleado = array_values(array_filter($arrVariablesPara,function($e) use($arrParametros){return $e['codigo'] == $arrParametros['pension_empleado'];}))[0];
                $arrSaludEmpresa = array_values(array_filter($arrVariablesPara,function($e) use($arrParametros){return $e['codigo'] == $arrParametros['salud_empleador'];}))[0];
                $arrSaludEmpleado = array_values(array_filter($arrVariablesPara,function($e) use($arrParametros){return $e['codigo'] == $arrParametros['salud_empleado'];}))[0];
                $arrCcf = array_values(array_filter($arrVariablesPara,function($e) use($arrParametros){return $e['codigo'] == $arrParametros['cajacompensacion'];}))[0];
                $arrArl = array_values(array_filter($arrVariablesPara,function($e) use($arrParametros){return $e['codigo'] == $arrParametros['arp'];}))[0];
                $arrIcbf = array_values(array_filter($arrVariablesPara,function($e) use($arrParametros){return $e['codigo'] == $arrParametros['icbf'];}))[0];
                $arrSena = array_values(array_filter($arrVariablesPara,function($e) use($arrParametros){return $e['codigo'] == $arrParametros['sena'];}))[0];
                $arrEsap = array_values(array_filter($arrVariablesPara,function($e) use($arrParametros){return $e['codigo'] == $arrParametros['esap'];}))[0];
                $arrInst = array_values(array_filter($arrVariablesPara,function($e) use($arrParametros){return $e['codigo'] == $arrParametros['iti'];}))[0];
                for ($i=0; $i < $total ; $i++) {
                    $det = $detalle[$i];
                    $det['cuenta_sector'] ="";
                    if($det['indicador_producto'] != ""){
                        $sector = substr($det['indicador_producto'],0,2);
                        $cuenta = array_values(array_filter($arrSectores,function($e) use($sector){return $e['id_sector'] == $sector;}))[0];
                        $det['cuenta_sector'] = $cuenta['id_cuenta'];
                    }
                    if($det['ptto_salario'] == "I"){
                        $det['ptto_salario'] = "IN";
                        $det['cod_pago'] = $det['inversion'];
                    }else if($det['ptto_salario'] == "G"){
                        $det['ptto_salario'] = "GP";
                        $det['cod_pago'] = $det['comercial'];
                    }else{
                        $det['cod_pago'] = $det['funcionamiento'];
                    }
                    if($det['ptto_para'] == "I"){
                        $det['ptto_para'] = "IN";
                        $det['cod_pago'] = $det['inversion'];
                    }else if($det['ptto_para'] == "G"){
                        $det['ptto_para'] = "GP";
                        $det['cod_pago'] = $det['comercial'];
                    }else{
                        $det['cod_pago'] = $det['funcionamiento'];
                    }
                    if($det['ptto_pago'] == "I"){
                        $det['ptto_pago'] = "IN";
                        $det['cod_pago'] = $det['inversion'];
                    }else if($det['ptto_pago'] == "G"){
                        $det['ptto_pago'] = "GP";
                        $det['cod_pago'] = $det['comercial'];
                    }else{
                        $det['cod_pago'] = $det['funcionamiento'];
                    }

                    /********************************Salarios y otros pagos*********************************** */
                    //Salarios
                    if($det['total'] > 0 && $det['tipo_pago'] == "01"){
                        $tipo = $det['ptto_para'] =="IN" ? $arrSueldo['inversion'] : $det['ptto_para'] =="F" ? $arrSueldo['funcionamiento'] : $arrSueldo['gastoscomerc'];
                        $data = [
                            "centro_costo"=>$det['centro_costo'],
                            "documento"=>$det['documento'],
                            "total"=>$det['total'],
                            "tipo"=>$det['ptto_salario'],
                            "cod_pago"=>$tipo,
                            "detalle"=>$det['nombre_variable']." mes de ".$strMes,
                            "sector"=>$det['cuenta_sector'],
                        ];
                        array_push($arrCuentas,$data);
                        if(isset($det['descuentos'])){$arrCuentas = $this->setDescuentos($arrCuentas,$det['descuentos'],$data,$strMes);}
                    }
                    //Transporte
                    if($det['total'] > 0 && $det['tipo_pago'] == "05"){
                        $tipo = $det['ptto_para'] =="IN" ? $arrTransporte['inversion'] : $det['ptto_para'] =="F" ? $arrTransporte['funcionamiento'] : $arrTransporte['gastoscomerc'];
                        $data = [
                            "centro_costo"=>$det['centro_costo'],
                            "documento"=>$det['documento'],
                            "total"=>$det['total'],
                            "tipo"=>$det['ptto_salario'],
                            "cod_pago"=>$tipo,
                            "detalle"=>$det['nombre_variable']." mes de ".$strMes,
                            "sector"=>$det['cuenta_sector'],
                        ];
                        array_push($arrCuentas, $data);
                        if(isset($det['descuentos'])){$arrCuentas = $this->setDescuentos($arrCuentas,$det['descuentos'],$data,$strMes);}
                    }
                    //Alimentación
                    if($det['total'] > 0 && $det['tipo_pago'] == "04"){
                        $tipo = $det['ptto_para'] =="IN" ? $arrAlimento['inversion'] : $det['ptto_para'] =="F" ? $arrAlimento['funcionamiento'] : $arrAlimento['gastoscomerc'];
                        $data = [
                            "centro_costo"=>$det['centro_costo'],
                            "documento"=>$det['documento'],
                            "total"=>$det['total'],
                            "tipo"=>$det['ptto_salario'],
                            "cod_pago"=>$tipo,
                            "detalle"=>$det['nombre_variable']." mes de ".$strMes,
                            "sector"=>$det['cuenta_sector'],
                        ];
                        array_push($arrCuentas, $data);
                        if(isset($det['descuentos'])){$arrCuentas = $this->setDescuentos($arrCuentas,$det['descuentos'],$data,$strMes);}
                    }
                    //Otros pagos
                    if($det['total'] > 0 && $det['tipo_pago'] != "01" && $det['tipo_pago'] != "04" && $det['tipo_pago'] != "05"){
                        $data = [
                            "centro_costo"=>$det['centro_costo'],
                            "documento"=>$det['documento'],
                            "total"=>$det['total'],
                            "tipo"=>$det['ptto_pago'],
                            "cod_pago"=>$det['cod_pago'],
                            "detalle"=>$det['nombre_variable']." mes de ".$strMes,
                            "sector"=>$det['cuenta_sector'],
                        ];
                        array_push($arrCuentas,$data);
                        if(isset($det['descuentos'])){$arrCuentas = $this->setDescuentos($arrCuentas,$det['descuentos'],$data,$strMes);}
                    }
                    /********************************Seguridad social*********************************** */
                    //Pension empresa
                    if($det['valor_pension_empresa'] > 0){
                        $tipo = $det['ptto_para'] =="IN" ? $arrPensionEmpresa['inversion'] : $det['ptto_para'] =="F" ? $arrPensionEmpresa['funcionamiento'] : $arrPensionEmpresa['gastoscomerc'];
                        array_push($arrCuentas,[
                                "centro_costo"=>$det['centro_costo'],
                                "documento"=>$det['documento'],
                                "total"=>$det['valor_pension_empresa'],
                                "cod_pago"=>$det['cod_pago'],
                                "tipo"=>$det['ptto_para'],
                                "detalle"=>"Aporte pensión empleador mes de ".$strMes,
                                "sector"=>$det['cuenta_sector'],
                                "cod_para"=>$tipo
                            ]
                        );
                    }
                    //Pension empleado
                    if($det['valor_pension_empleado'] > 0){
                        $tipo = $det['ptto_para'] =="IN" ? $arrPensionEmpleado['inversion'] : $det['ptto_para'] =="F" ? $arrPensionEmpleado['funcionamiento'] : $arrPensionEmpleado['gastoscomerc'];
                        array_push($arrCuentas,[
                                "centro_costo"=>$det['centro_costo'],
                                "documento"=>$det['documento'],
                                "total"=>$det['valor_pension_empleado'],
                                "cod_pago"=>$det['cod_pago'],
                                "tipo"=>$det['ptto_para'],
                                "detalle"=>"Aporte pensión empleado mes de ".$strMes,
                                "sector"=>$det['cuenta_sector'],
                                "cod_para"=>$tipo
                            ]
                        );
                    }
                    //Salud empresa
                    if($det['valor_salud_empresa'] > 0){
                        $tipo = $det['ptto_para'] =="IN" ? $arrSaludEmpresa['inversion'] : $det['ptto_para'] =="F" ? $arrSaludEmpresa['funcionamiento'] : $arrSaludEmpresa['gastoscomerc'];
                        array_push($arrCuentas,[
                                "centro_costo"=>$det['centro_costo'],
                                "documento"=>$det['documento'],
                                "total"=>$det['valor_salud_empresa'],
                                "cod_pago"=>$det['cod_pago'],
                                "tipo"=>$det['ptto_para'],
                                "detalle"=>"Aporte salud empleador mes de ".$strMes,
                                "sector"=>$det['cuenta_sector'],
                                "cod_para"=>$tipo
                            ]
                        );
                    }
                    //Salud empleado
                    if($det['valor_salud_empleado'] > 0){
                        $tipo = $det['ptto_para'] =="IN" ? $arrSaludEmpleado['inversion'] : $det['ptto_para'] =="F" ? $arrSaludEmpleado['funcionamiento'] : $arrSaludEmpleado['gastoscomerc'];
                        array_push($arrCuentas,[
                                "centro_costo"=>$det['centro_costo'],
                                "documento"=>$det['documento'],
                                "total"=>$det['valor_salud_empleado'],
                                "cod_pago"=>$det['cod_pago'],
                                "tipo"=>$det['ptto_para'],
                                "detalle"=>"Aporte salud empleado mes de ".$strMes,
                                "sector"=>$det['cuenta_sector'],
                                "cod_para"=>$tipo
                            ]
                        );
                    }
                    /********************************Parafiscales*********************************** */
                    //CCF
                    if($det['valor_aporte_ccf'] > 0){
                        $tipo = $det['ptto_para'] =="IN" ? $arrCcf['inversion'] : $det['ptto_para'] =="F" ? $arrCcf['funcionamiento'] : $arrCcf['gastoscomerc'];
                        array_push($arrCuentas,[
                                "centro_costo"=>$det['centro_costo'],
                                "documento"=>$det['documento'],
                                "total"=>$det['valor_aporte_ccf'],
                                "cod_pago"=>$det['cod_pago'],
                                "tipo"=>$det['ptto_para'],
                                "detalle"=>"Aporte caja compensación mes de ".$strMes,
                                "sector"=>$det['cuenta_sector'],
                                "cod_para"=>$tipo
                            ]
                        );
                    }
                    //ARL
                    if($det['valor_arl'] > 0){
                        $tipo = $det['ptto_para'] =="IN" ? $arrArl['inversion'] : $det['ptto_para'] =="F" ? $arrArl['funcionamiento'] : $arrArl['gastoscomerc'];
                        array_push($arrCuentas,[
                                "centro_costo"=>$det['centro_costo'],
                                "documento"=>$det['documento'],
                                "total"=>$det['valor_arl'],
                                "cod_pago"=>$det['cod_pago'],
                                "tipo"=>$det['ptto_para'],
                                "detalle"=>"Aporte ARL mes de ".$strMes,
                                "cod_para"=>$tipo
                            ]
                        );
                    }
                    //ICBF
                    if($det['valor_aporte_icbf'] > 0){
                        $tipo = $det['ptto_para'] =="IN" ? $arrIcbf['inversion'] : $det['ptto_para'] =="F" ? $arrIcbf['funcionamiento'] : $arrIcbf['gastoscomerc'];
                        array_push($arrCuentas,[
                                "centro_costo"=>$det['centro_costo'],
                                "documento"=>$det['documento'],
                                "total"=>$det['valor_aporte_icbf'],
                                "cod_pago"=>$det['cod_pago'],
                                "tipo"=>$det['ptto_para'],
                                "detalle"=>"Aporte ICBF mes de ".$strMes,
                                "sector"=>$det['cuenta_sector'],
                                "cod_para"=>$tipo
                            ]
                        );
                    }
                    //SENA
                    if($det['valor_aporte_sena'] > 0){
                        $tipo = $det['ptto_para'] =="IN" ? $arrSena['inversion'] : $det['ptto_para'] =="F" ? $arrSena['funcionamiento'] : $arrSena['gastoscomerc'];
                        array_push($arrCuentas,[
                                "centro_costo"=>$det['centro_costo'],
                                "documento"=>$det['documento'],
                                "total"=>$det['valor_aporte_sena'],
                                "cod_pago"=>$det['cod_pago'],
                                "tipo"=>$det['ptto_para'],
                                "detalle"=>"Aporte SENA mes de ".$strMes,
                                "sector"=>$det['cuenta_sector'],
                                "cod_para"=>$tipo
                            ]
                        );
                    }
                    //ESAP
                    if($det['valor_aporte_esap'] > 0){
                        $tipo = $det['ptto_para'] =="IN" ? $arrEsap['inversion'] : $det['ptto_para'] =="F" ? $arrEsap['funcionamiento'] : $arrEsap['gastoscomerc'];
                        array_push($arrCuentas,[
                                "centro_costo"=>$det['centro_costo'],
                                "documento"=>$det['documento'],
                                "total"=>$det['valor_aporte_esap'],
                                "cod_pago"=>$det['cod_pago'],
                                "tipo"=>$det['ptto_para'],
                                "detalle"=>"Aporte ESAP mes de ".$strMes,
                                "sector"=>$det['cuenta_sector'],
                                "cod_para"=>$tipo
                            ]
                        );
                    }
                    //Institutos
                    if($det['valor_aporte_esap'] > 0){
                        $tipo = $det['ptto_para'] =="IN" ? $arrInst['inversion'] : $det['ptto_para'] =="F" ? $arrInst['funcionamiento'] : $arrInst['gastoscomerc'];
                        array_push($arrCuentas,[
                                "centro_costo"=>$det['centro_costo'],
                                "documento"=>$det['documento'],
                                "total"=>$det['valor_aporte_esap'],
                                "cod_pago"=>$det['cod_pago'],
                                "tipo"=>$det['ptto_para'],
                                "detalle"=>"Aporte institutos técnicos mes de ".$strMes,
                                "sector"=>$det['cuenta_sector'],
                                "cod_para"=>$tipo
                            ]
                        );
                    }
                }
            }
            return $arrCuentas;
        }
        public function setDescuentos($arrData,$arrDescuentos,$det,$strMes){
            foreach ($arrDescuentos as $desc) {
                $documento = $det['documento'];
                $cuenta="";
                $tipoRetencion = $desc['tipo_retencion'];
                if($desc['tipo_descuento'] =="DE"){
                    $arrCuenta = $this->selectCuentaDescuento($desc['tipo_retencion']);
                    $cuenta = $arrCuenta['cuenta'];
                    $documento = $arrCuenta['beneficiario'];
                }else{
                    $tipoRetencion = $this->selectCuentaRetencion($tipoRetencion);
                }
                array_push($arrData,[
                        "centro_costo"=>$det['centro_costo'],
                        "documento"=>$documento,
                        "total"=>$desc['valor'],
                        "tipo"=>$det['tipo'],
                        "cod_pago"=>$det['cod_pago'],
                        "detalle"=>$desc['descripcion']." mes de ".$strMes,
                        "sector"=>"",
                        "tipo_descuento"=>$desc['tipo_descuento'],
                        "tipo_retencion"=>$tipoRetencion,
                        "cuenta"=>$cuenta
                    ]
                );
            }
            return $arrData;
        }
        public function getData(){
            if(!empty($_SESSION)){
                $arrData = array(
                    "liquidaciones" =>$this->selectLiquidaciones(),
                    "terceros"=>getTerceros(),
                    "aprobados"=>$this->selectAprobados()
                );
                echo json_encode($arrData,JSON_UNESCAPED_UNICODE);
            }
            die();
        }
        public function getEdit(){
            if(!empty($_SESSION)){
                $intId = intval($_POST['codigo']);
                $request = $this->selectEdit($intId);
                if(!empty($request)){
                    $arrResponse = array("status"=>true,"data"=>$request,"consecutivos"=>getConsecutivos("humnomina_aprobado","id_aprob","tipo_mov",201));
                }else{
                    $arrResponse = array("status"=>false,"consecutivo"=>searchConsec("humnomina_aprobado ","id_aprob")-1);
                }
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
            die();
        }
        public function getSearch(){
            if(!empty($_SESSION)){
                $strFechaInicial = strClean($_POST['fecha_inicial']);
                $strFechaFinal = strClean($_POST['fecha_final']);
                $strCodigo = strClean($_POST['codigo']);
                $strLiquidacion = strClean($_POST['liquidacion']);
                $request = $this->selectSearch($strFechaInicial,$strFechaFinal,$strCodigo,$strLiquidacion);
                echo json_encode($request,JSON_UNESCAPED_UNICODE);
            }
            die();
        }
        public function changeData(){
            if(!empty($_SESSION)){
                $strFechaFinal = strClean($_POST['fecha']);
                if(checkBlock($_SESSION['cedulausu'],$strFechaFinal )){
                    $id = intval($_POST['id']);
                    $nomina = intval($_POST['nomina']);
                    $request = $this->updateStatus($id,$nomina);
                    if($request == 1){
                        insertAuditoria("hum_auditoria","hum_funciones_id",9,"Anular",$id,"Aprobar nomina","hum_funciones");
                        $arrResponse = array("status"=>true);
                    }else if($request == "existe"){
                        $arrResponse = array("status"=>false,"msg"=>"La liquidación ".$nomina." ya tiene egreso, no puede anular.");
                    }else{
                        $arrResponse = array("status"=>false,"msg"=>"Error");
                    }
                }else{
                    $arrResponse = array("status"=>false,"msg"=>"No es posible anular, la fecha está bloqueada.");
                }
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
            die();
        }
    }

    if($_POST){
        $obj = new AprobarController();
        if($_POST['action'] == "save"){
            $obj->save();
        }else if($_POST['action']=="edit"){
            $obj->getEdit();
        }else if($_POST['action']=="search"){
            $obj->getSearch();
        }else if($_POST['action']=="change"){
            $obj->changeData();
        }else if($_POST['action']=="get"){
            $obj->getData();
        }
    }

?>
