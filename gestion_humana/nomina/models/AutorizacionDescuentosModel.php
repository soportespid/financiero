<?php
    require_once '../../../Librerias/core/Helpers.php';
    require_once '../../../Librerias/core/Mysql.php';
    session_start();
    /*ini_set('display_errors', '1');
    ini_set('display_startup_errors', '1');
    error_reporting(E_ALL);*/
    class AutorizacionDescuentosModel extends Mysql{
        private $intConsecutivo;
        private $strDescripcion;
        private $strFecha;
        private $intContrato;
        private $intValorDeuda;
        private $intValorCuota;
        private $intCuotas;
        private $strVariablePago;
        private $strVariableRetencion;
        private $strModoPago;
        private $strTercero;

        function __construct(){
            parent::__construct();
        }

        public function insertData(string $strDescripcion,string $strFecha, int $intContrato, float $intValorDeuda,int $intCuotas,
            string $strModoPago, string $strVariableRetencion,string $strTercero ){
            $this->strDescripcion = $strDescripcion;
            $this->strFecha = $strFecha;
            $this->intContrato = $intContrato;
            $this->intValorDeuda = $intValorDeuda;
            $this->intCuotas = $intCuotas;
            $this->strModoPago = $strModoPago;
            $this->strVariableRetencion = $strVariableRetencion;
            $this->strTercero = $strTercero;
            $sql = "SELECT * FROM humretenempleados WHERE id_retencion = '$this->strVariableRetencion' AND idfuncionario=$this->intContrato AND estado = 'S'";
            $request = $this->select_all($sql);
            $return ="";
            if(empty($request)){
                $sql="INSERT INTO humretenempleados(descripcion, id_retencion, fecha, empleado, deuda, ncuotas,
                estado, idfuncionario, modo_pago,habilitado,tipopago) VALUES(?,?,?,?,?,?,?,?,?,?,?)";
                $request = $this->insert($sql,[
                    $this->strDescripcion,
                    $this->strVariableRetencion,
                    $this->strFecha,
                    $this->strTercero,
                    $this->intValorDeuda,
                    $this->intCuotas,
                    "S",
                    $this->intContrato,
                    $this->strModoPago,
                    "H",
                    "01"
                ]);
                $return = $request;
            }else{
                $return ="existe";
            }

            return $return;
        }
        public function insertDet(int $intConsecutivo,array $data){
            $this->intConsecutivo = $intConsecutivo;
            $this->delete("DELETE FROM humretenempleados_det WHERE cab_id = $this->intConsecutivo");
            foreach ($data as $d) {
                $sql="INSERT INTO humretenempleados_det(cab_id, tipo_pago, cuota, valor,mes, estado) VALUES(?,?,?,?,?,?)";
                $request = $this->insert($sql,[
                    $this->intConsecutivo,
                    $d['tipo_pago'],
                    $d['cuota'],
                    $d['valor'],
                    $d['mes'],
                    $d['estado'],
                ]);
            }
            return $request;
        }
        public function updateData(int $intConsecutivo,string $strDescripcion,string $strFecha, int $intContrato, float $intValorDeuda,int $intCuotas,
        string $strModoPago, string $strVariableRetencion,string $strTercero){
            $this->intConsecutivo = $intConsecutivo;
            $this->strDescripcion = $strDescripcion;
            $this->strFecha = $strFecha;
            $this->intContrato = $intContrato;
            $this->intValorDeuda = $intValorDeuda;
            $this->intCuotas = $intCuotas;
            $this->strModoPago = $strModoPago;
            $this->strVariableRetencion = $strVariableRetencion;
            $this->strTercero = $strTercero;
            $sql="UPDATE humretenempleados SET descripcion=?, id_retencion=?, fecha=?, empleado=?, deuda=?, ncuotas=?,idfuncionario=?, modo_pago=?
            WHERE id = $this->intConsecutivo";
            $request = $this->update($sql,[
                $this->strDescripcion,
                $this->strVariableRetencion,
                $this->strFecha,
                $this->strTercero,
                $this->intValorDeuda,
                $this->intCuotas,
                $this->intContrato,
                $this->strModoPago
            ]);
            return $request;
        }
        public function updateStatus(int $intConsecutivo,int $intContrato,string $strVariableRetencion,string $estado){
            $this->intContrato = $intContrato;
            $this->intConsecutivo = $intConsecutivo;
            $this->strVariableRetencion = $strVariableRetencion;
            $sql = "UPDATE humretenempleados SET estado =? WHERE id_retencion = '$this->strVariableRetencion' AND idfuncionario=$this->intContrato AND estado = 'S'";
            $request = $this->update($sql, ["N"]);

            $sql = "UPDATE humretenempleados SET estado = ? WHERE id = $this->intConsecutivo";
            $request = $this->update($sql,[$estado]);

            return $request;
        }
        public function selectEdit($intConsecutivo){
            $this->intConsecutivo = $intConsecutivo;
            $sql = "SELECT id,descripcion, id_retencion, fecha, empleado, deuda, ncuotas,
            estado, idfuncionario, modo_pago
            FROM humretenempleados WHERE id = $this->intConsecutivo";
            $request = $this->select($sql);
            if(!empty($request)){
                $sqlDet = "SELECT tipo_pago, cuota, valor,mes, estado
                FROM humretenempleados_det
                WHERE cab_id = $this->intConsecutivo ORDER BY cuota";
                $requestDet = $this->select_all($sqlDet);

                //Cuotas Pagos
                $sqlPago = "SELECT T1.id_nom,T1.id_des,T1.ncta,T1.valor,T2.fecha,T2.mes
                FROM humnominaretenemp AS T1
                INNER JOIN humnomina AS T2 ON T1.id_nom = T2.id_nom
                WHERE T1.id = '$this->intConsecutivo' AND T1.estado = 'P' AND T1.tipo_des = 'DS'";
                $requestPago = $this->select_all($sqlPago);
                $totalDet = count($requestDet);
                for ($i=0; $i < $totalDet ; $i++) {
                    $cuota = $requestDet[$i];
                    $arrPago = array_values(array_filter($requestPago,function($e)use($cuota){return $cuota['mes'] == substr($e['fecha'],0,7);}))[0];
                    $cuota['id_descuento'] ="";
                    $cuota['id_nomina'] ="";
                    $cuota['id_egreso'] ="";
                    $cuota['cuenta_ptto'] ="";
                    if(!empty($arrPago)){
                        $cuota['id_descuento'] = $arrPago['id_des'];
                        $cuota['id_nomina'] = $arrPago['id_nom'];
                        $cuota['estado'] = "P";

                        $sqleg = "SELECT T1.id_egreso, T1.ntercero_det, T1.cuentap, DATE_FORMAT(T2.fecha,'%d/%m/%Y') as fecha_egreso
                        FROM tesoegresosnomina_det AS T1
                        INNER JOIN tesoegresosnomina AS T2  ON T1.id_egreso = T2.id_egreso
                        WHERE T1.id_orden = '{$cuota['id_nomina']}' AND T1.ndes = '$this->intConsecutivo' AND T1.tipo = 'DS'";
                        $arrEgreso = $this->select($sqleg);

                        $cuota['id_egreso'] = $arrEgreso['id_egreso'];
                        $cuota['cuenta_ptto'] = $arrEgreso['cuentap'];
                    }
                    $requestDet[$i] = $cuota;
                }
                $request['detalle'] = $requestDet;
            }
            return $request;
        }
        public function selectTempCuotas(){
            $this->delete("TRUNCATE TABLE humretenempleados_det");
            $sql = "SELECT * FROM humretenempleados";
            $request = $this->select_all($sql);
            $arrContratos = $this->select_all("SELECT id,tercero FROM hum_contratos");
            foreach ($request as $data) {
                $idContrato = array_values(array_filter($arrContratos,function($e) use($data){return $e['tercero'] == $data['empleado'];}))[0]['id'];
                $estadoCuota =$data['estado'];
                $estadoDescuento =$data['estado'];
                $totalCuotas = $data['ncuotas'];
                $totalDeuda = $data['deuda'];
                $valorCuota = $totalDeuda/$totalCuotas;
                $arrCuotas = [];
                $arrFecha = explode("-",$data['fecha']);
                $mes = $arrFecha[0]."-".$arrFecha[1];
                if($estadoDescuento !="P" && $data['habilitado'] == "D"){
                    $estadoDescuento = "N";
                }else if($estadoDescuento =="P"){
                    $estadoCuota ="P";
                }
                for ($i=0; $i < $totalCuotas; $i++) {
                    array_push($arrCuotas,[
                        "tipo_pago"=>$data['tipopago'],
                        "cuota"=>$i+1,
                        "valor"=>$valorCuota,
                        "mes"=>$mes,
                        "estado"=>$estadoCuota,
                    ]);
                }
                $sqlUpdate = "UPDATE humretenempleados SET estado=?,idfuncionario=? WHERE id = {$data['id']}";
                $this->update($sqlUpdate,[$estadoDescuento,$idContrato]);
                $this->insertDet($data['id'],$arrCuotas);
            }
        }
        public function selectSearch($strFechaInicial,$strFechaFinal,$strDocumento,$intContrato,$strDescripcion){
            $sql = "SELECT *,DATE_FORMAT(fecha,'%d/%m/%Y') as fecha
            FROM humretenempleados WHERE estado != '' AND fecha BETWEEN '$strFechaInicial' AND '$strFechaFinal'
            AND empleado LIKE '$strDocumento%' AND idfuncionario LIKE '$intContrato%' AND descripcion LIKE '$strDescripcion%'
            ORDER BY id DESC";
            $request = $this->select_all($sql);
            $total = count($request);
            for ($i=0; $i < $total; $i++) {
                $id = $request[$i]['id'];
                $sqlDet = "SELECT COUNT(*) as cuotas_pagas,COALESCE(SUM(valor)) AS total_pagado FROM humretenempleados_det WHERE estado = 'P' AND cab_id = $id";
                $sqlCuotas = "SELECT COUNT(1) as total FROM humnominaretenemp WHERE id = '$id' AND estado = 'P' AND tipo_des = 'DS'";
                $cuotasPagas = $this->select($sqlCuotas)['total'];
                $cuotasFaltantes = $request[$i]['ncuotas'] - $cuotasPagas;
                if($cuotasFaltantes == 0 && $request[$i]['estado'] == "S"){
                    $sqlUpdate = "UPDATE humretenempleados SET estado = ? WHERE id='$id'";
                    $this->update($sqlUpdate,['P']);
                    $request[$i]['estado'] = "P";
                }
                $arrDet = $this->select($sqlDet);
                $request[$i]['cuotas_pagas'] = 0;
                $request[$i]['total_pagado'] = 0;
                $request[$i]['is_status'] = $request[$i]['estado'] =="S"? 1 : 0;
                if(!empty($arrDet)){
                    $request[$i]['cuotas_pagas'] = $arrDet['cuotas_pagas'];
                    $request[$i]['total_pagado'] = $arrDet['total_pagado'];
                }
            }
            return $request;
        }
        public function selectVariablesPago(){
            $sql = "SELECT * FROM ccpethumvariables WHERE estado = 'S'";
            $request = $this->select_all($sql);
            return $request;
        }
        public function selectVariablesRetenciones(){
            $sql = "SELECT * FROM humvariablesretenciones WHERE estado = 'S'";
            $request = $this->select_all($sql);
            return $request;
        }
        public function selectVariablesSsf(){
            $sql = "SELECT id as codigo, nombre FROM tesomediodepagossf WHERE estado = 'S'";
            $request = $this->select_all($sql);
            return $request;
        }
        public function selectContratos(){
            $sql = "SELECT c.id as codigo,
            t.cedulanit as tercero,
            CASE
                WHEN t.razonsocial IS NULL OR t.razonsocial = ''
                THEN CONCAT(t.nombre1,' ',t.nombre2,' ',t.apellido1,' ',t.apellido2)
                ELSE t.razonsocial
            END AS nombre
            FROM hum_contratos c
            INNER JOIN terceros t ON t.cedulanit = c.tercero
            WHERE c.estado != 'TERMINADO' ORDER BY c.id DESC";
            $request = $this->select_all($sql);
            return $request;
        }
    }
?>
