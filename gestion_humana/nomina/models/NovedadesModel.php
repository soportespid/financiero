<?php
    require_once '../../../Librerias/core/Helpers.php';
    require_once '../../../Librerias/core/Mysql.php';
    session_start();
    /*ini_set('display_errors', '1');
    ini_set('display_startup_errors', '1');
    error_reporting(E_ALL);*/
    class NovedadesModel extends Mysql{
        private $intConsecutivo;
        private $strDescripcion;
        private $strFecha;
        private $intMes;
        private $intAnio;

        function __construct(){
            parent::__construct();
        }

        public function insertData(string $strFecha,int $intMes,int $intAnio, string $strDescripcion){
            $this->strDescripcion = $strDescripcion;
            $this->strFecha = $strFecha;
            $this->intMes = $intMes;
            $this->intAnio = $intAnio;

            $sql="INSERT INTO hum_novedadespagos_cab(fecha_nueva, mes, descripcion, vigencia, estado) VALUES(?,?,?,?,?)";
            $request = $this->insert($sql,[$this->strFecha,$this->intMes,$this->strDescripcion,$this->intAnio,"S"]);
            return $request;
        }
        public function insertDet(int $intConsecutivo,string $strDescripcion,array $data){
            $this->intConsecutivo = $intConsecutivo;
            $this->strDescripcion = $strDescripcion;

            $this->delete("DELETE FROM hum_novedadespagos WHERE codigo = $this->intConsecutivo");
            $this->delete("DELETE FROM hum_novedadextras WHERE cab_id = $this->intConsecutivo");

            $sql="INSERT INTO hum_novedadespagos(codigo, tipo, descripcion,idfun,documento,valorb,dias,valord,pse,
            psr,ppe,ppr,parl,pccf,picbf,psena,pitec,pesap,estado,dias_vacaciones,dias_incapacidad,dias_laborados)
            VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

            $sqlExtra="INSERT INTO hum_novedadextras(cab_id, contrato_id, ord_diurna,ord_nocturna,dom_diurna,dom_nocturna,rec_nocturna,
            rec_dom_diurna,rec_dom_nocturna,salario,total_extra,total_recargo,total) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?)";
            foreach ($data as $d) {
                $detalle = $d['detalle'];
                foreach ($detalle as $det) {
                    $det['salario'] = $det['salario'] !="" ? $det['salario'] : 0;
                    $det['is_checked_salud_empleado'] = $det['is_checked_salud_empleado'] != 1 ? "N" : "S";
                    $det['is_checked_salud_empresa'] = $det['is_checked_salud_empresa'] != 1 ? "N" : "S";
                    $det['is_checked_pension_empleado'] = $det['is_checked_pension_empleado'] != 1 ? "N" : "S";
                    $det['is_checked_pension_empresa'] = $det['is_checked_pension_empresa'] != 1 ? "N" : "S";
                    $det['is_checked_arl'] = $det['is_checked_arl'] != 1 ? "N" : "S";
                    $det['is_checked_ccf'] = $det['is_checked_ccf'] != 1 ? "N" : "S";
                    $det['is_checked_icbf'] = $det['is_checked_icbf'] != 1 ? "N" : "S";
                    $det['is_checked_sena'] = $det['is_checked_sena'] != 1 ? "N" : "S";
                    $det['is_checked_inst_tecnico'] = $det['is_checked_inst_tecnico'] != 1 ? "N" : "S";
                    $det['is_checked_esap'] = $det['is_checked_esap'] != 1 ? "N" : "S";
                    if($d['codigo'] == "01"){
                        //Sueldo
                        $request = $this->insert($sql,
                    [
                                $this->intConsecutivo,
                                "01",
                                $this->strDescripcion,
                                $det['codigo'],
                                $det['documento'],
                                $det['salario'],
                                $det['dias'],
                                $det['valor_dias'],
                                $det['is_checked_salud_empleado'],
                                $det['is_checked_salud_empresa'],
                                $det['is_checked_pension_empleado'],
                                $det['is_checked_pension_empresa'],
                                $det['is_checked_arl'],
                                $det['is_checked_ccf'],
                                $det['is_checked_icbf'],
                                $det['is_checked_sena'],
                                $det['is_checked_inst_tecnico'],
                                $det['is_checked_esap'],
                                "S",
                                $det['dias_vacaciones'],
                                $det['dias_incapacidad'],
                                $det['dias_laborados'],
                            ]
                        );
                    }else if($d['codigo'] == "02"){
                        //Extras
                        $request = $this->insert($sqlExtra,
                    [
                                $this->intConsecutivo,
                                $det['codigo'],
                                $det['ordinaria_diurna'],
                                $det['ordinaria_nocturna'],
                                $det['dominical_diurna'],
                                $det['dominical_nocturna'],
                                $det['recargo_nocturno'],
                                $det['recargo_dominical_diurno'],
                                $det['recargo_dominical_nocturno'],
                                $det['salario'],
                                $det['total_extra'],
                                $det['total_recargo'],
                                $det['total']
                            ]
                        );
                    }else{
                        //Otros pagos
                        $valor = $det['valor_otros'];
                        if($d['codigo'] == "05"){$valor = $det['transporte'];
                        }else if($d['codigo']=="04"){$valor = $det['alimentacion'];}
                        $request = $this->insert($sql,
                    [
                                $this->intConsecutivo,
                                $d['codigo'],
                                $this->strDescripcion,
                                $det['codigo'],
                                $det['documento'],
                                $det['salario'],
                                $det['dias'],
                                $valor,
                                $det['is_checked_salud_empleado'],
                                $det['is_checked_salud_empresa'],
                                $det['is_checked_pension_empleado'],
                                $det['is_checked_pension_empresa'],
                                $det['is_checked_arl'],
                                $det['is_checked_ccf'],
                                $det['is_checked_icbf'],
                                $det['is_checked_sena'],
                                $det['is_checked_inst_tecnico'],
                                $det['is_checked_esap'],
                                "S",
                                $det['dias_vacaciones'],
                                $det['dias_incapacidad'],
                                $det['dias_laborados'],
                            ]
                        );
                    }
                }
            }
            return $request;
        }
        public function updateData(int $intConsecutivo,string $strFecha,int $intMes,int $intAnio, string $strDescripcion){
            $this->intConsecutivo = $intConsecutivo;
            $this->strDescripcion = $strDescripcion;
            $this->strFecha = $strFecha;
            $this->intMes = $intMes;
            $this->intAnio = $intAnio;
            $sql="UPDATE hum_novedadespagos_cab SET fecha_nueva=?, mes=?, descripcion=?, vigencia=? WHERE id = $this->intConsecutivo";
            $request = $this->update($sql,[$this->strFecha,$this->intMes,$this->strDescripcion,$this->intAnio]);
            return $request;
        }
        public function updateStatus(int $intConsecutivo,string $estado){
            $this->intConsecutivo = $intConsecutivo;
            $sql = "UPDATE hum_novedadespagos_cab SET estado = ? WHERE id = $this->intConsecutivo";
            $request = $this->update($sql,[$estado]);
            return $request;
        }
        public function selectEdit($intConsecutivo){
            $this->intConsecutivo = $intConsecutivo;
            $sql = "SELECT id,fecha_nueva as fecha, mes, descripcion, vigencia, estado, prenomina
            FROM hum_novedadespagos_cab WHERE id = $this->intConsecutivo";
            $request = $this->select($sql);
            if(!empty($request)){
                $sql = "SELECT tipo,idfun,documento,valorb,dias,valord,pse,psr,ppe,ppr,parl,pccf,picbf,
                psena,pitec,pesap,dias_vacaciones,dias_incapacidad,dias_laborados
                FROM hum_novedadespagos WHERE codigo = $this->intConsecutivo";
                $sqlExtra = "SELECT * FROM hum_novedadextras WHERE cab_id = $this->intConsecutivo";
                $request['novedad_pago'] = $this->select_all($sql);
                $request['novedad_extra'] = $this->select_all($sqlExtra);
            }
            return $request;
        }
        public function selectSearch($strFechaInicial,$strFechaFinal,$strCodigo){
            $sql = "SELECT cab.id,
            cab.descripcion,
            cab.vigencia,
            cab.estado,
            cab.prenomina,
            m.nombre as mes,
            DATE_FORMAT(cab.fecha_nueva,'%d/%m/%Y') as fecha
            FROM hum_novedadespagos_cab cab
            LEFT JOIN meses m ON m.id = cab.mes
            WHERE cab.estado != '' AND cab.fecha_nueva BETWEEN '$strFechaInicial' AND '$strFechaFinal' AND cab.id LIKE '$strCodigo%' ORDER BY cab.id DESC";
            $request = $this->select_all($sql);
            $total = count($request);
            for ($i=0; $i < $total; $i++) {
                $request[$i]['is_status'] = $request[$i]['estado'] =="S"? 1 : 0;
            }
            return $request;
        }
        public function selectGenerales(){
            $sql = "SELECT salario,transporte,alimentacion FROM admfiscales ORDER BY vigencia DESC";
            $request = $this->select($sql);
            return $request;
        }
        public function selectHorasMensuales(){
            $sql = "SELECT h.valor
            FROM hum_horas_mesuales h
            INNER JOIN humparametrosliquida l ON l.horas_mensuales = h.id";
            $request = $this->select($sql);
            return !empty($request) ? $request['valor'] : 0;
        }
        public function selectMeses(): array{
            $sql = "SELECT * FROM meses WHERE estado = 'S'";
            $request = $this->select_all($sql);
            return $request;
        }
        public function selectVariablesPago(){
            $sql = "SELECT * FROM ccpethumvariables WHERE estado = 'S'";
            $request = $this->select_all($sql);
            if(!empty($request)){
                $total = count($request);
                for ($i=0; $i < $total ; $i++) {
                    $request[$i]['detalle'] = [];
                    $request[$i]['is_checked_salud_empleado'] = $request[$i]['psalud'] == "S" ? 1 : 0;
                    $request[$i]['is_checked_salud_empresa'] = $request[$i]['psalud'] == "S" ? 1 : 0;;
                    $request[$i]['is_checked_pension_empleado'] = $request[$i]['ppension'] == "S" ? 1 : 0;
                    $request[$i]['is_checked_pension_empresa'] = $request[$i]['ppension'] == "S" ? 1 : 0;
                    $request[$i]['is_checked_arl'] = $request[$i]['parl'] == "S" ? 1 : 0;
                    $request[$i]['is_checked_ccf'] = $request[$i]['pparafiscal'] == "S" ? 1 : 0;
                    $request[$i]['is_checked_icbf'] = $request[$i]['pparafiscal'] == "S" ? 1 : 0;
                    $request[$i]['is_checked_sena'] = $request[$i]['pparafiscal'] == "S" ? 1 : 0;
                    $request[$i]['is_checked_inst_tecnico'] = $request[$i]['pparafiscal'] == "S" ? 1 : 0;
                    $request[$i]['is_checked_esap'] = $request[$i]['pparafiscal'] == "S" ? 1 : 0;
                }
            }
            return $request;
        }
        public function selectContratos(int $intAnio,int $intMes){
            $this->intMes = $intMes;
            $this->intAnio = $intAnio;
            $sql = "SELECT
            cab.id as codigo,
            cab.tercero,
            cab.tipo,
            cab.periodo,
            DATE_FORMAT(cab.fecha_inicial,'%d/%m/%Y') as fecha_inicial,
            DATE_FORMAT(cab.fecha_final,'%d/%m/%Y') as fecha_final,
            cab.banco,
            cab.cuenta,
            cab.cargo_id,
            cab.estado,
            cab.automatico,
            c.nombrecargo as nombre_cargo,
            s.valor as salario,
            s.id_nivel as escala,
            s.nombre as nivel_salarial,
            CASE WHEN t.razonsocial IS NULL OR t.razonsocial = ''
            THEN CONCAT(t.nombre1,' ',t.nombre2,' ',t.apellido1,' ',t.apellido2)
            ELSE t.razonsocial END AS nombre,
            b.nombre as nombre_banco,
            p.nombre as nombre_periodo,
            t.direccion,
            t.tipodoc,
            p.dias
            FROM hum_contratos cab
            LEFT JOIN terceros t ON cab.tercero = t.cedulanit
            LEFT JOIN planaccargos c ON cab.cargo_id = c.codcargo
            LEFT JOIN humnivelsalarial s ON s.id_nivel=c.clasificacion
            LEFT JOIN humperiodos p ON cab.periodo = p.id_periodo
            LEFT JOIN hum_bancosfun b ON cab.banco = b.codigo
            WHERE cab.estado != 'TERMINADO'
            ORDER BY cab.id DESC";
            $request = $this->select_all($sql);
            if(!empty($request)){
                $total = count($request);
                for ($i=0; $i < $total ; $i++) {
                    $contrato = $request[$i];
                    if($contrato['estado'] =="RENOVADO"){
                        $idContrato = $contrato['codigo'];
                        $sqlRev = "SELECT
                        cab.id,
                        cab.tercero,
                        cab.tipo,
                        cab.periodo,
                        DATE_FORMAT(cab.fecha_inicial,'%d/%m/%Y') as fecha_inicial,
                        DATE_FORMAT(cab.fecha_final,'%d/%m/%Y') as fecha_final,
                        cab.banco,
                        cab.cuenta,
                        cab.cargo_id,
                        cab.novedad_id,
                        cab.jornada,
                        cab.contrato,
                        c.nombrecargo as nombre_cargo,
                        s.valor as salario,
                        s.id_nivel as escala,
                        s.nombre as nivel_salarial,
                        CASE WHEN t.razonsocial IS NULL OR t.razonsocial = ''
                        THEN CONCAT(t.nombre1,' ',t.nombre2,' ',t.apellido1,' ',t.apellido2)
                        ELSE t.razonsocial END AS nombre,
                        b.nombre as nombre_banco,
                        p.nombre as nombre_periodo,
                        t.direccion,
                        t.tipodoc,
                        p.dias
                        FROM hum_contratos_renovacion cab
                        LEFT JOIN terceros t ON cab.tercero = t.cedulanit
                        LEFT JOIN planaccargos c ON cab.cargo_id = c.codcargo
                        LEFT JOIN humnivelsalarial s ON s.id_nivel=c.clasificacion
                        LEFT JOIN humperiodos p ON cab.periodo = p.id_periodo
                        LEFT JOIN hum_bancosfun b ON cab.banco = b.codigo
                        LEFT JOIN hum_contratos_novedades nov ON nov.id = cab.novedad_id
                        WHERE nov.contrato_id = $idContrato
                        AND cab.fecha_inicial = (SELECT max(re.fecha_inicial) FROM hum_contratos_renovacion re WHERE re.novedad_id = cab.novedad_id)
                        ORDER BY nov.id DESC";
                        $arrRev = $this->select($sqlRev);
                        $request[$i]['fecha_inicial'] = $arrRev['fecha_inicial'];
                        $request[$i]['fecha_final'] = $arrRev['fecha_final'];
                        $request[$i]['salario'] = $arrRev['salario'];
                        $request[$i]['nombre_cargo'] = $arrRev['nombre_cargo'];
                        $request[$i]['nombre_periodo'] = $arrRev['nombre_periodo'];
                        $request[$i]['tipo'] = $arrRev['tipo'];

                    }
                    $sqlVacaciones = "SELECT COALESCE(SUM(det.dias_vaca),0) as vacaciones
                    FROM hum_vacaciones_det det
                    INNER JOIN hum_vacaciones cab ON cab.num_vaca = det.num_vaca
                    WHERE det.doc_funcionario = '{$contrato['tercero']}' AND det.mes = '$this->intMes'
                    AND det.vigencia = '$this->intAnio' AND cab.estado = 'S'";

                    $sqlIncapacidad = "SELECT COALESCE(SUM(det.dias_inca),0) as incapacidad
                    FROM hum_incapacidades_det det
                    LEFT JOIN hum_incapacidades cab ON cab.num_inca = det.num_inca
                    WHERE det.doc_funcionario = '{$contrato['tercero']}' AND det.mes = '$this->intMes'
                    AND det.vigencia = '$this->intAnio' AND cab.estado = 'S'";

                    /*
                    $mesTemp =$this->intMes < 10 ? "0".$this->intMes : $this->intMes;
                    $fechaDescuento = $this->intAnio."-".$mesTemp;

                    $sqlDescuentos = "SELECT
                    cab.descripcion,
                    det.valor,
                    det.cuota
                    FROM humretenempleados cab
                    INNER JOIN humretenempleados_det det ON cab.id = det.cab_id
                    WHERE cab.empleado = '{$contrato['tercero']}' AND SUBSTR(cab.fecha,1,7) = '$fechaDescuento'
                    AND SUBSTR(det.mes,1,7) = '$fechaDescuento' AND cab.estado = 'S' AND det.estado='S'";

                    $request[$i]['descuentos'] = $this->select_all($sqlDescuentos);*/
                    $request[$i]['dias_vacaciones'] = $this->select($sqlVacaciones)['vacaciones'];
                    $request[$i]['dias_incapacidad'] = $this->select($sqlIncapacidad)['incapacidad'];
                    $request[$i]['is_checked'] = 0;
                }
            }
            return $request;
        }
    }
?>
