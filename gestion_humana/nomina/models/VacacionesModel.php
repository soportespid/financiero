<?php
    require_once '../../../Librerias/core/Helpers.php';
    require_once '../../../Librerias/core/Mysql.php';
    session_start();
    /*ini_set('display_errors', '1');
    ini_set('display_startup_errors', '1');
    error_reporting(E_ALL);*/
    class VacacionesModel extends Mysql{
        private $intConsecutivo;
        private $strMeses;
        private $strFecha;
        private $strFechaInicial;
        private $strFechaFinal;
        private $strCheckArl;
        private $strCheckEps;
        private $strCheckPara;
        private $intTotal;
        private $intTotalVacaciones;
        private $strResolucion;
        private $intVigencia;
        function __construct(){
            parent::__construct();
        }

        public function insertData(string $strFecha,string $strFechaInicial,string $strFechaFinal, string $strCheckArl,
        string $strCheckEps,string $strCheckPara,$strMeses,int $intTotal,int $intTotalVacaciones,
        array $arrContrato,string $strResolucion,int $intVigencia){
            $this->strFecha = $strFecha;
            $this->strFechaInicial = $strFechaInicial;
            $this->strFechaFinal = $strFechaFinal;
            $this->strCheckArl = $strCheckArl;
            $this->strCheckEps = $strCheckEps;
            $this->strCheckPara = $strCheckPara;
            $this->strMeses = $strMeses;
            $this->intTotal = $intTotal;
            $this->intTotalVacaciones = $intTotalVacaciones;
            $this->strResolucion = $strResolucion;
            $this->intVigencia = $intVigencia;
            $sql="INSERT INTO hum_vacaciones(fecha,doc_funcionario,num_resolucion,salario,fecha_ini,fecha_fin,valor_total,paga_ibc,paga_arl,paga_para,
            meses,dias_total,estado,cod_fun,vigencia) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
            $request = $this->insert($sql,[
                $this->strFecha,
                $arrContrato['tercero'],
                $this->strResolucion,
                $arrContrato['salario'],
                $this->strFechaInicial,
                $this->strFechaFinal,
                $this->intTotal,
                $this->strCheckEps,
                $this->strCheckArl,
                $this->strCheckPara,
                $this->strMeses,
                $this->intTotalVacaciones,
                "S",
                $arrContrato['codigo'],
                $this->intVigencia
            ]);
            return $request;
        }
        public function insertDet(int $intConsecutivo,array $data,array $contrato ){
            $this->intConsecutivo = $intConsecutivo;
            $this->delete("DELETE FROM hum_vacaciones_det WHERE num_vaca = $this->intConsecutivo");
            $sql = "INSERT INTO hum_vacaciones_det(doc_funcionario,num_vaca,mes,dias_vaca,valor_dia,
            valor_total,estado,cod_fun,vigencia) VALUES(?,?,?,?,?,?,?,?,?)";
            foreach ($data as $e) {
                $this->insert($sql,[
                    $contrato['tercero'],
                    $this->intConsecutivo,
                    $e['mes'],
                    $e['dias'],
                    $e['valor_dia'],
                    $e['valor_total'],
                    "S",
                    $contrato['codigo'],
                    $e['vigencia']
                ]);
            }
        }
        public function updateData(int $intConsecutivo,string $strFecha,string $strFechaInicial,string $strFechaFinal, string $strCheckArl,
        string $strCheckEps,string $strCheckPara,$strMeses,int $intTotal,int $intTotalVacaciones,
        array $arrContrato,string $strResolucion,int $intVigencia){
            $this->strFecha = $strFecha;
            $this->strFechaInicial = $strFechaInicial;
            $this->strFechaFinal = $strFechaFinal;
            $this->strCheckArl = $strCheckArl;
            $this->strCheckEps = $strCheckEps;
            $this->strCheckPara = $strCheckPara;
            $this->strMeses = $strMeses;
            $this->intTotal = $intTotal;
            $this->intTotalVacaciones = $intTotalVacaciones;
            $this->strResolucion = $strResolucion;
            $this->intConsecutivo = $intConsecutivo;
            $this->intVigencia = $intVigencia;

            $sql="UPDATE hum_vacaciones SET num_resolucion=?,salario=?,fecha_ini=?,fecha_fin=?,valor_total=?,paga_ibc=?,paga_arl=?,paga_para=?,
            meses=?,dias_total=?,vigencia=? WHERE num_vaca = $this->intConsecutivo";

            $request = $this->update($sql,[
                $this->strResolucion,
                $arrContrato['salario'],
                $this->strFechaInicial,
                $this->strFechaFinal,
                $this->intTotal,
                $this->strCheckEps,
                $this->strCheckArl,
                $this->strCheckPara,
                $this->strMeses,
                $this->intTotalVacaciones,
                $this->intVigencia
            ]);
            return $request;
        }
        public function updateStatus(int $intConsecutivo,string $estado){
            $this->intConsecutivo = $intConsecutivo;
            $sql = "UPDATE hum_vacaciones SET estado = ? WHERE num_vaca = $this->intConsecutivo";
            $request = $this->update($sql,[$estado]);
            return $request;
        }
        public function selectEdit($intConsecutivo){
            $this->intConsecutivo = $intConsecutivo;
            $sql = "SELECT num_vaca,fecha,doc_funcionario,num_resolucion,salario, fecha_ini,fecha_fin, paga_ibc,paga_arl,paga_para,estado,cod_fun
            FROM hum_vacaciones WHERE num_vaca = $this->intConsecutivo";
            $request = $this->select($sql);
            if(!empty($request)){
                $sql = "SELECT id_det,mes,dias_vaca as dias,valor_dia,valor_total FROM hum_vacaciones_det WHERE num_vaca = $this->intConsecutivo AND estado != 'D'";
                $request['det'] = $this->select_all($sql);
            }
            return $request;
        }
        public function selectSearch($strFechaInicial,$strFechaFinal,$strCodigo,$strTercero){
            $sql = "SELECT cab.num_vaca,
            cab.valor_total,
            cab.estado,
            cab.meses,
            cab.doc_funcionario as tercero,
            cab.dias_total as dias,
            CASE WHEN t.razonsocial IS NULL OR t.razonsocial = ''
            THEN CONCAT(t.nombre1,' ',t.nombre2,' ',t.apellido1,' ',t.apellido2)
            ELSE t.razonsocial END AS nombre,
            DATE_FORMAT(cab.fecha_ini,'%d/%m/%Y') as fecha_inicial,
            DATE_FORMAT(cab.fecha_fin,'%d/%m/%Y') as fecha_final,
            DATE_FORMAT(cab.fecha,'%d/%m/%Y') as fecha
            FROM hum_vacaciones cab
            LEFT JOIN terceros t ON cab.doc_funcionario = t.cedulanit
            WHERE cab.fecha_ini BETWEEN '$strFechaInicial' AND '$strFechaFinal' AND cab.num_vaca LIKE '$strCodigo%' AND cab.doc_funcionario LIKE '$strTercero%'
            ORDER BY cab.num_vaca DESC";
            $request = $this->select_all($sql);
            $total = count($request);
            for ($i=0; $i < $total; $i++) {
                $request[$i]['is_status'] = $request[$i]['estado'] =="S"? 1 : 0;
            }
            return $request;
        }
        public function selectGenerales(){
            $sql = "SELECT salario,transporte,alimentacion FROM admfiscales ORDER BY vigencia DESC";
            $request = $this->select($sql);
            return $request;
        }
        public function selectMeses(): array{
            $sql = "SELECT * FROM meses WHERE estado = 'S'";
            $request = $this->select_all($sql);
            return $request;
        }
        public function selectContratos(){
            $sql = "SELECT
            cab.id as codigo,
            cab.tercero,
            cab.tipo,
            cab.periodo,
            DATE_FORMAT(cab.fecha_inicial,'%d/%m/%Y') as fecha_inicial,
            DATE_FORMAT(cab.fecha_final,'%d/%m/%Y') as fecha_final,
            cab.banco,
            cab.cuenta,
            cab.cargo_id,
            cab.estado,
            cab.automatico,
            c.nombrecargo as nombre_cargo,
            s.valor as salario,
            s.id_nivel as escala,
            s.nombre as nivel_salarial,
            CASE WHEN t.razonsocial IS NULL OR t.razonsocial = ''
            THEN CONCAT(t.nombre1,' ',t.nombre2,' ',t.apellido1,' ',t.apellido2)
            ELSE t.razonsocial END AS nombre,
            b.nombre as nombre_banco,
            p.nombre as nombre_periodo,
            t.direccion,
            t.tipodoc,
            p.dias
            FROM hum_contratos cab
            LEFT JOIN terceros t ON cab.tercero = t.cedulanit
            LEFT JOIN planaccargos c ON cab.cargo_id = c.codcargo
            LEFT JOIN humnivelsalarial s ON s.id_nivel=c.clasificacion
            LEFT JOIN humperiodos p ON cab.periodo = p.id_periodo
            LEFT JOIN hum_bancosfun b ON cab.banco = b.codigo
            WHERE cab.estado != 'TERMINADO'
            ORDER BY cab.id DESC";
            $request = $this->select_all($sql);
            if(!empty($request)){
                $total = count($request);
                for ($i=0; $i < $total ; $i++) {
                    $contrato = $request[$i];
                    if($contrato['estado'] =="RENOVADO"){
                        $idContrato = $contrato['codigo'];
                        $sqlRev = "SELECT
                        cab.id,
                        cab.tercero,
                        cab.tipo,
                        cab.periodo,
                        DATE_FORMAT(cab.fecha_inicial,'%d/%m/%Y') as fecha_inicial,
                        DATE_FORMAT(cab.fecha_final,'%d/%m/%Y') as fecha_final,
                        cab.banco,
                        cab.cuenta,
                        cab.cargo_id,
                        cab.novedad_id,
                        cab.jornada,
                        cab.contrato,
                        c.nombrecargo as nombre_cargo,
                        s.valor as salario,
                        s.id_nivel as escala,
                        s.nombre as nivel_salarial,
                        CASE WHEN t.razonsocial IS NULL OR t.razonsocial = ''
                        THEN CONCAT(t.nombre1,' ',t.nombre2,' ',t.apellido1,' ',t.apellido2)
                        ELSE t.razonsocial END AS nombre,
                        b.nombre as nombre_banco,
                        p.nombre as nombre_periodo,
                        t.direccion,
                        t.tipodoc,
                        p.dias
                        FROM hum_contratos_renovacion cab
                        LEFT JOIN terceros t ON cab.tercero = t.cedulanit
                        LEFT JOIN planaccargos c ON cab.cargo_id = c.codcargo
                        LEFT JOIN humnivelsalarial s ON s.id_nivel=c.clasificacion
                        LEFT JOIN humperiodos p ON cab.periodo = p.id_periodo
                        LEFT JOIN hum_bancosfun b ON cab.banco = b.codigo
                        LEFT JOIN hum_contratos_novedades nov ON nov.id = cab.novedad_id
                        WHERE nov.contrato_id = $idContrato
                        AND cab.fecha_inicial = (SELECT max(re.fecha_inicial) FROM hum_contratos_renovacion re WHERE re.novedad_id = cab.novedad_id)
                        ORDER BY nov.id DESC";
                        $arrRev = $this->select($sqlRev);
                        $request[$i]['fecha_inicial'] = $arrRev['fecha_inicial'];
                        $request[$i]['fecha_final'] = $arrRev['fecha_final'];
                        $request[$i]['salario'] = $arrRev['salario'];
                        $request[$i]['nombre_cargo'] = $arrRev['nombre_cargo'];
                        $request[$i]['nombre_periodo'] = $arrRev['nombre_periodo'];
                        $request[$i]['tipo'] = $arrRev['tipo'];

                    }
                    $request[$i]['is_checked'] = 0;
                }
            }
            return $request;
        }
    }
?>
