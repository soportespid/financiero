<?php

require __DIR__ . '/vendor/autoload.php';

use Laravel\Route;
use Laravel\DB;

    require_once '../../../Librerias/core/Helpers.php';
    require_once '../../../Librerias/core/Mysql.php';
    session_start();
    /*ini_set('display_errors', '1');
    ini_set('display_startup_errors', '1');
    error_reporting(E_ALL);*/
    class ValidarModel extends Mysql{
        private $intConsecutivo;
        private $intVigencia;
        private $intMes;

        function __construct(){
            parent::__construct();
        }
        public function selectLiquidaciones(){
            $sql = "SELECT cab.id_nom as id,
            me.nombre as mes,
            cab.vigencia,
            nov.descripcion
            FROM humnomina cab
            LEFT JOIN hum_novedadespagos_cab nov ON cab.id_nom = nov.prenomina
            LEFT JOIN meses me ON me.id = cab.mes
            WHERE cab.estado = 'S'
            ORDER BY cab.id_nom DESC";
            $request = $this->select_all($sql);
            $total = count($request);
            if($total > 0){
                for ($i=0; $i < $total ; $i++) {
                    $request[$i]['descripcion'] = $request[$i]['descripcion'] == null ? "" : $request[$i]['descripcion'];
                }
            }
            return $request;
        }

        public function selectFormato(int $intVigencia, int $intMes) {
            $this->intVigencia = $intVigencia;
            $this->intMes = $intMes;

            $tenant_db = DB::connectWithDomain(Route::currentDomain());

            $stmt = $tenant_db->prepare('SELECT `id` FROM `raw_planillas` WHERE `vigencia` = ? AND `mes` = ? LIMIT 1');
            $stmt->bind_param('ii', $intVigencia, $intMes);
            if (! $stmt->execute()) {
                $tenant_db->close();
                return [];
            }

            $result = $stmt->get_result();

            if ($result->num_rows == 0) {
                $tenant_db->close();
                return [];
            }

            $request = $result->fetch_assoc();

            $stmt = $tenant_db->prepare('SELECT * FROM `hum_operadores_planillas_det` WHERE `raw_planilla_id` = ?');
            $stmt->bind_param('i', $request['id']);
            if (! $stmt->execute()) {
                $tenant_db->close();
                return [];
            }

            $result = $stmt->get_result();

            if ($result->num_rows == 0) {
                $tenant_db->close();
                return [];
            }

            $tenant_db->close();
            return $result->fetch_all(MYSQLI_ASSOC);
        }

        public function selectLiquidacion(int $intConsecutivo){
            $this->intConsecutivo = $intConsecutivo;
            $sql="SELECT cab.id_nom,cab.fecha,m.nombre as mes,
            cab.vigencia,cab.estado,cab.mes as codigo_mes
            FROM humnomina cab
            LEFT JOIN meses m ON cab.mes = m.id
            WHERE cab.id_nom = $this->intConsecutivo";
            $request = $this->select($sql);
            if(!empty($request)){
                $sql = "SELECT det.id_nom,
                det.cedulanit as documento,
                det.salbas as salario,
                det.diaslab dias,
                det.devendias as total,
                det.ibc as valor_ibc,
                det.salud as valor_salud_empleado,
                det.saludemp as valor_salud_empresa,
                det.pension as valor_pension_empleado,
                det.pensionemp as valor_pension_empresa,
                det.fondosolid as valor_fondo,
                det.retefte as valor_retencion,
                det.otrasdeduc as valor_descuento,
                det.totaldeduc as valor_total_descuentos,
                det.netopagar as valor_neto,
                det.cajacf as valor_aporte_ccf,
                det.sena as valor_aporte_sena,
                det.icbf as valor_aporte_icbf,
                det.instecnicos as valor_aporte_inst,
                det.esap as valor_aporte_esap,
                det.basepara as valor_ibc_para,
                det.basearp as valor_ibc_arl,
                det.arp as valor_arl,
                det.totalsalud as valor_total_salud,
                det.totalpension as valor_total_pension,
                det.tipopago as tipo_pago,
                det.idfuncionario as id,
                det.detalle as descripcion,
                vap.nombre as nombre_variable,
                CASE WHEN t.razonsocial IS NULL OR t.razonsocial = ''
                THEN CONCAT(t.nombre1,' ',t.nombre2,' ',t.apellido1,' ',t.apellido2)
                ELSE t.razonsocial END AS nombre
                FROM humnomina_det det
                LEFT JOIN ccpethumvariables vap ON det.tipopago = vap.codigo
                LEFT JOIN terceros t ON det.cedulanit = t.cedulanit
                WHERE det.id_nom = $this->intConsecutivo";
                $request['detalle'] = $this->select_all($sql);
            }
            return $request;
        }
    }
?>
