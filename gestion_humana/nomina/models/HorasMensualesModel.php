<?php
    require_once '../../../Librerias/core/Helpers.php';
    require_once '../../../Librerias/core/Mysql.php';
    session_start();
    /*ini_set('display_errors', '1');
    ini_set('display_startup_errors', '1');
    error_reporting(E_ALL);*/
    class HorasMensualesModel extends Mysql{
        private $intConsecutivo;
        private $intValor;

        function __construct(){
            parent::__construct();
        }

        public function insertData(int $intValor){
            $this->intValor = $intValor;
            $sql = "SELECT * FROM hum_horas_mesuales WHERE valor = '$this->intValor'";
            $request = $this->select_all($sql);
            $return = "";
            if(empty($request)){
                $sql="INSERT INTO hum_horas_mesuales(valor,estado) VALUES(?,?)";
                $return = $this->insert($sql,[$this->intValor,"S"]);
            }else{
                $return = "existe";
            }
            return $return;
        }
        public function updateData(int $intConsecutivo,int $intValor){
            $this->intValor = $intValor;
            $this->intConsecutivo = $intConsecutivo;
            $sql = "SELECT * FROM hum_horas_mesuales WHERE valor = '$this->intValor' AND id != $this->intConsecutivo";
            $request = $this->select_all($sql);
            $return = "";
            if(empty($request)){
                $sql="UPDATE hum_horas_mesuales SET valor=? WHERE id = $this->intConsecutivo";
                $return = $this->update($sql,[$this->intValor]);
            }else{
                $return = "existe";
            }
            return $return;
        }
        public function updateStatus(int $intConsecutivo,string $estado){
            $this->intConsecutivo = $intConsecutivo;
            $sql = "UPDATE hum_horas_mesuales SET estado = '$estado' WHERE id = $this->intConsecutivo";
            $request = $this->update($sql,[$estado]);
            return $request;
        }
        public function selectEdit($intConsecutivo){
            $this->intConsecutivo = $intConsecutivo;
            $sql = "SELECT * FROM hum_horas_mesuales WHERE id = $this->intConsecutivo";
            $request = $this->select($sql);
            return $request;
        }
        public function selectSearch(){
            $sql = "SELECT * FROM hum_horas_mesuales ORDER BY id DESC";
            $request = $this->select_all($sql);
            $total = count($request);
            for ($i=0; $i < $total; $i++) {
                $request[$i]['is_status'] = $request[$i]['estado'] =="S"? 1 : 0;
            }
            return $request;
        }
    }
?>
