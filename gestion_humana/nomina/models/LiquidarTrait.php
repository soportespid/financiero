<?php
    trait LiquidarTrait{
        public function getDataT(array $arrNovedades){
            $arrNeto = $arrNovedades['neto'];
            $arrParafiscales = $arrNovedades['parafiscales'];
            $arrNewParafiscales[0] = $arrParafiscales['aporte_ccf'];
            $arrNewParafiscales[1] = $arrParafiscales['aporte_esap'];
            $arrNewParafiscales[2] = $arrParafiscales['aporte_icbf'];
            $arrNewParafiscales[3] = $arrParafiscales['aporte_inst'];
            $arrNewParafiscales[4] = $arrParafiscales['aporte_sena'];
            $arrNewParafiscales[5] = $arrParafiscales['aporte_arl'];
            $arrNewParafiscales[6] = $arrParafiscales['pension_empresa'];
            $arrNewParafiscales[7] = $arrParafiscales['salud_empresa'];
            $arrNovedades = $arrNovedades['novedades'];
            $arrLiquidacion = [];
            $arrDescuentos = [];
            $arrRetenciones = [];
            $total = count($arrNovedades);
            for ($i=0; $i < $total; $i++) {
                $arrNovDet = $arrNovedades[$i]['novedades'];
                $arrTempDescuentos = $arrNovedades[$i]['descuentos'];
                $arrTempRetenciones = $arrNovedades[$i]['retenciones'];
                $totalDescuento = 0;
                foreach ($arrTempDescuentos as $det) {
                    $totalDescuento+=$det['valor'];
                    array_push($arrDescuentos,array(
                        "nombre"=>$arrNovDet[0]['nombre_tercero'],
                        "documento"=>$arrNovDet[0]['documento'],
                        "cuota"=>$det['cuota'],
                        "descripcion"=>$det['descripcion'],
                        "valor"=>$det['valor'],
                        "tipo_pago"=>$det['tipo'],
                        "contrato_id"=>$arrNovDet[0]['contrato_id'],
                        "id_descuento"=>$det['id_descuento'],
                        "tipo_retencion"=>$det['id_retencion'],
                        "type"=>"valor"
                    ));
                }
                foreach ($arrTempRetenciones as $det) {
                    array_push($arrRetenciones,array(
                        "nombre"=>$arrNovDet[0]['nombre_tercero'],
                        "documento"=>$arrNovDet[0]['documento'],
                        "descripcion"=>$det['descripcion'],
                        "valor"=>$det['retencion'],
                        "tipo_pago"=>$det['tipo_pago'],
                        "contrato_id"=>$arrNovDet[0]['contrato_id'],
                        "tipo_retencion"=>$det['tipo_retencion'],
                    ));
                }
                if($totalDescuento > 0){
                    array_push($arrDescuentos,array(
                        "valor"=>$totalDescuento,
                        "type"=>"total"
                    ));
                }
                foreach ($arrNovDet as $det) {
                    array_push($arrLiquidacion,array(
                        "type"=>"det",
                        "tipo_pago"=>$det['tipo'],
                        "id"=>$det['contrato_id'],
                        "nombre"=>$det['nombre'],
                        "nombre_tercero"=>$det['nombre_tercero'],
                        "documento"=>$det['documento'],
                        "salario"=>$det['salario'],
                        "dias"=>$det['dias'],
                        "total"=>$det['total'],
                        "valor_ibc"=>$det['valor_ibc'],
                        "valor_ibc_para"=>$det['valor_ibc_para'],
                        "valor_ibc_arl"=>$det['valor_ibc_arl'],
                        "valor_arl"=>$det['valor_arl'],
                        "valor_salud_empleado"=>$det['valor_salud_empleado'],
                        "valor_salud_empresa"=>$det['valor_salud_empresa'],
                        "valor_total_salud"=>$det['valor_total_salud'],
                        "valor_pension_empleado"=>$det['valor_pension_empleado'],
                        "valor_pension_empresa"=>$det['valor_pension_empresa'],
                        "valor_total_pension"=>$det['valor_total_pension'],
                        "valor_fondo"=>$det['valor_fondo'],
                        "valor_retencion"=>$det['valor_retencion'],
                        "valor_descuento"=>$det['valor_descuento'],
                        "valor_total_descuentos"=>$det['valor_total_descuentos'],
                        "valor_neto"=>$det['valor_neto'],
                        "valor_aporte_ccf"=>$det['valor_aporte_ccf'],
                        "valor_aporte_sena"=>$det['valor_aporte_sena'],
                        "valor_aporte_icbf"=>$det['valor_aporte_icbf'],
                        "valor_aporte_inst"=>$det['valor_aporte_inst'],
                        "valor_aporte_esap"=>$det['valor_aporte_esap'],
                        "tipo_fondo"=>$det['tipo_fondo'] != "" ? $det['tipo_fondo'] :"",
                        "cesantias"=>$det['cesantias'],
                        "tercero_afp"=>$det['tercero_afp'],
                        "tercero_arl"=>$det['tercero_arl'],
                        "tercero_eps"=>$det['tercero_eps'],
                        "tercero_fondo"=>$det['tercero_fondo'],
                        "vinculacion"=>$det['vinculacion'],
                        "ptto_para"=>$det['ptto_para'],
                        "ptto_pago"=>$det['ptto_pago'],
                        "ptto_salario"=>$det['ptto_salario'],
                        "bpin"=>$det['bpin'],
                        "indicador"=>$det['indicador'],
                        "fuente"=>$det['fuente'],
                        "centro"=>$det['centro'],
                    ));
                }
                array_push($arrLiquidacion,array("type"=>"total","total"=>$arrNovedades[$i]['totales']));
            }
            return array(
                "descuentos"=>$arrDescuentos,
                "liquidacion"=>$arrLiquidacion,
                "parafiscales"=>$arrNewParafiscales,
                "retenciones"=>$arrRetenciones,
                "neto"=>$arrNeto
            );
        }
        public function getCalcRetencionesT(array $arrNovedades,int $uvt){
            $arrNovedades = $arrNovedades['novedades'];
            $arrRetenciones = [];
            $total = count($arrNovedades);
            for ($i=0; $i < $total; $i++) {
                $totalDevengado = 0;
                $totalFactor = 0;
                $arrNovDet = $arrNovedades[$i]['novedades'];
                $arrTotales = $arrNovedades[$i]['totales'];
                $arrVariables = [];
                foreach ($arrNovDet as $det) {
                    array_push($arrVariables,array("codigo"=>$det['tipo'],"nombre"=>$det['nombre']));
                    if($det['tipo'] != "06" || $det['tipo'] != "17"){$totalDevengado+=$det['total'];}
                    if($det['tipo'] =="05" || $det['tipo'] =="04" || $det['tipo'] =="01"){$totalFactor += $det['total'];}
                }
                $totalIngresosNoConstitutivos = intval($arrTotales['total_salud_empleado']+$arrTotales['total_pension_empleado']+$arrTotales['total_fondo']);
                $totalIngresoNeto = intval($totalDevengado-$totalIngresosNoConstitutivos);
                $totalNeto = intval($totalIngresoNeto-$arrTotales['total_otros_desc']);
                $rentaExenta = round(0.25*$totalNeto);
                $subtotal = $totalNeto-$rentaExenta;
                $totalRentaDeduccion = $rentaExenta+$arrTotales['total_descuentos']+$totalNeto;
                $rentaLimitada = round($totalIngresoNeto*0.4);
                $baseRetencion = $totalIngresoNeto-(min($totalRentaDeduccion,$rentaLimitada));
                $totalUvt = round($baseRetencion/$uvt);
                if($totalUvt > 95){
                    $totalUvtRetenido = $this->calcPorcentajeRet($totalUvt);
                    $totalRetenido = $totalUvtRetenido*$uvt;
                    sort($arrVariables);
                    array_push($arrRetenciones,
                        array(
                            "id"=>$arrNovDet[0]['contrato_id'],
                            "nombre"=>$arrNovDet[0]['nombre_tercero'],
                            "documento"=>$arrNovDet[0]['documento'],
                            "devengado"=>$totalDevengado,
                            "factor"=>$totalFactor,
                            "salud"=>$arrTotales['total_salud_empleado'],
                            "pension"=>$arrTotales['total_pension_empleado'],
                            "fsp"=>$arrTotales['total_fondo'],
                            "total_no_constitutivo"=>$totalIngresosNoConstitutivos,
                            "total_ingreso_neto"=>$totalIngresoNeto,
                            "deducciones"=>$arrTotales['total_otros_desc'],
                            "total_deducciones"=>$arrTotales['total_descuentos'],
                            "total_neto"=>$totalNeto,
                            "renta_exenta"=>$rentaExenta,
                            "subtotal"=>$subtotal,
                            "base_retencion"=>$baseRetencion,
                            "base_uvt"=>$totalUvt,
                            "total_uvt_retenido"=>$totalUvtRetenido,
                            "total_retenido"=>$totalRetenido,
                            "retencion"=>$totalRetenido,
                            "variables"=>$arrVariables
                        )
                    );
                }
            }
            return $arrRetenciones;
        }
        public function calcPorcentajeRet($uvt){
            $totalUvt = 0;
            if($uvt > 95 && $uvt<=150){
                $totalUvt =($uvt-95)*0.19;
            }else if($uvt > 150 && $uvt<=360){
                $totalUvt =($uvt-150)*0.28+10;
            }else if($uvt > 360 && $uvt<=640){
                $totalUvt =($uvt-360)*0.33+69;
            }else if($uvt > 640 && $uvt<=945){
                $totalUvt =($uvt-640)*0.35+162;
            }else if($uvt > 945 && $uvt<=2300){
                $totalUvt =($uvt-945)*0.37+268;
            }else if($uvt > 2300){
                $totalUvt =($uvt-2300)*0.39+770;
            }
            return intval($totalUvt);
        }
        public function calcNovedad($det,$arrFondoSolidario){
            $intBaseIBC = 0;
            $intBasePara = 0;
            $intBaseArl = 0;
            $intSaludEmpleado = 0;
            $intSaludEmpresa = 0;
            $intPensionEmpleado = 0;
            $intPensionEmpresa = 0;
            $intTotalSalud = 0;
            $intTotalPension = 0;
            $intFondoSolidario = 0;
            $intAporteCcf=0;
            $intAporteSena = 0;
            $intAporteIcbf = 0;
            $intAporteInst = 0;
            $intAporteEsap = 0;
            $intArl = 0;
            $totalPorcentajeSalud = $det['salud']+$det['salud_empresa'];
            $totalPorcentajePension = $det['pension']+$det['pension_empresa'];

            if($det['pse'] == "S"){
                $intBaseIBC = $det['total'];
                $intTotalSalud = $totalPorcentajeSalud*$intBaseIBC;
                $intSaludEmpleado = $intBaseIBC*$det['salud'];
            }
            if($det['psr']=="S"){
                $intBaseIBC = $det['total'];
                $intTotalSalud = $totalPorcentajeSalud*$intBaseIBC;
                $intSaludEmpresa = $intTotalSalud-$intSaludEmpleado;
            }
            if($det['ppe'] == "S"){
                $intTotalPension = $totalPorcentajePension*$intBaseIBC;
                $intPensionEmpleado = $intBaseIBC*$det['pension'];
                //Fondo Solidario
                $arrPorcentajeFondo = array_values(array_filter($arrFondoSolidario,function($e)use($intBaseIBC){
                    return $intBaseIBC >= $e['rangoinicial'] && $intBaseIBC <= $e['rangofinal'];
                }));
                if(!empty($arrPorcentajeFondo)){
                    $intFondoSolidario = $intBaseIBC*$arrPorcentajeFondo[0]['porcentaje'];
                }
            }
            if($det['ppr'] == "S"){
                $intTotalPension = $totalPorcentajePension*$intBaseIBC;
                $intPensionEmpleado = $intBaseIBC*$det['pension'];
                $intPensionEmpresa = $intTotalPension-$intPensionEmpleado;
            }
            //ARL
            if($det['parl'] =="S"){
                $intBaseArl = $det['total'];
                $intArl = $intBaseArl*$det['arl'];
            }
            //Parafiscales
            if($det['pccf'] =="S" || $det['picbf'] =="S" || $det['psena'] =="S" || $det['pitec'] =="S" || $det['pesap'] =="S"){
                $intBasePara = $det['total'];
                if($det['pccf'] =="S"){$intAporteCcf = $intBasePara*$det['aporte_ccf'];}
                if($det['picbf'] =="S"){$intAporteIcbf = $intBasePara*$det['aporte_icbf'];}
                if($det['psena'] =="S"){$intAporteSena = $intBasePara*$det['aporte_sena'];}
                if($det['pitec'] =="S"){$intAporteInst = $intBasePara*$det['aporte_inst'];}
                if($det['pesap'] =="S"){$intAporteEsap = $intBasePara*$det['aporte_esap'];}
            }
            if(isset($det['novedad'])){
                if($det['pse'] =="S"){
                    $intBaseIBC = ($det['salario']/$det['periodo'])*$det['dias'];
                    $intTotalSalud = $totalPorcentajeSalud*$intBaseIBC;
                    $intSaludEmpleado = $intBaseIBC*$det['salud'];
                    $intTotalSalud = $totalPorcentajeSalud*$intBaseIBC;
                    $intSaludEmpresa = $intTotalSalud-$intSaludEmpleado;
                    $intTotalPension = $totalPorcentajePension*$intBaseIBC;
                    $intPensionEmpleado = $intBaseIBC*$det['pension'];
                    $intTotalPension = $totalPorcentajePension*$intBaseIBC;
                    $intPensionEmpleado = $intBaseIBC*$det['pension'];
                    $intPensionEmpresa = $intTotalPension-$intPensionEmpleado;

                    //Fondo Solidario
                    $arrPorcentajeFondo = array_values(array_filter($arrFondoSolidario,function($e)use($intBaseIBC){
                        return $intBaseIBC >= $e['rangoinicial'] && $intBaseIBC <= $e['rangofinal'];
                    }));
                    if(!empty($arrPorcentajeFondo)){
                        $intFondoSolidario = $intBaseIBC*$arrPorcentajeFondo[0]['porcentaje'];
                    }
                }
                if($det['parl']=="S"){
                    $intBaseArl = ($det['salario']/$det['periodo'])*$det['dias'];
                    $intArl = $det['parl']*$intBaseArl;
                }
                if($det['pccf']=="S"){
                    $intBasePara = ($det['salario']/$det['periodo'])*$det['dias'];
                    //Parafiscales
                    $intAporteCcf = $intBasePara*$det['aporte_ccf'];
                    $intAporteIcbf = $intBasePara*$det['aporte_icbf'];
                    $intAporteSena = $intBasePara*$det['aporte_sena'];
                    $intAporteInst = $intBasePara*$det['aporte_inst'];
                    $intAporteEsap = $intBasePara*$det['aporte_esap'];
                }
            }

            return [
                "base_ibc"=>round($intBaseIBC),
                "base_para"=>round($intBasePara),
                "base_arl"=>round($intBaseArl),
                "valor_arl"=>round($intArl/100)*100,
                "valor_salud_empleado"=>round($intSaludEmpleado/100)*100,
                "valor_salud_empresa"=>round($intSaludEmpresa/100)*100,
                "valor_total_salud"=>round($intTotalSalud/100)*100,
                "valor_pension_empleado"=>round($intPensionEmpleado/100)*100,
                "valor_pension_empresa"=>round($intPensionEmpresa/100)*100,
                "valor_total_pension"=>round($intTotalPension/100)*100,
                "valor_fondo"=>round($intFondoSolidario/100)*100,
                "valor_aporte_ccf"=>round($intAporteCcf/100)*100,
                "valor_aporte_sena"=>round($intAporteSena/100)*100,
                "valor_aporte_icbf"=>round($intAporteIcbf/100)*100,
                "valor_aporte_inst"=>round($intAporteInst/100)*100,
                "valor_aporte_esap"=>round($intAporteEsap/100)*100
            ];
        }
    }

?>
