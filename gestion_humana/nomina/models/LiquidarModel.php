<?php
    require_once '../../../Librerias/core/Helpers.php';
    require_once '../../../Librerias/core/Mysql.php';
    session_start();
    /*ini_set('display_errors', '1');
    ini_set('display_startup_errors', '1');
    error_reporting(E_ALL);*/
    class LiquidarModel extends Mysql{
        private $intConsecutivo;
        private $intIdNovedad;
        private $strFecha;
        private $intMes;
        private $intVigencia;
        private $intTotal;
        private $strDescripcion;

        function __construct(){
            parent::__construct();
        }
        public function insertLiquidacion(string $strFecha,int $intMes,int $intVigencia,int $intIdNovedad,string $strDescripcion){
            $this->strFecha = $strFecha;
            $this->intMes = $intMes;
            $this->intVigencia = $intVigencia;
            $this->intIdNovedad = $intIdNovedad;
            $this->strDescripcion = $strDescripcion;
            $sql = "INSERT INTO humnomina (fecha,mes,vigencia,estado,novedad_id,descripcion) VALUES(?,?,?,?,?,?)";
            $sqlNom = "INSERT INTO hum_nom_cdp_rp (nomina, cdp, rp, codaprobado, vigencia, estado,solicitud) VALUES ( ?,?,?,?,?,?,?)";
            $request = $this->insert($sql,[$this->strFecha,$this->intMes,$this->intVigencia,"S",$this->intIdNovedad,$this->strDescripcion]);
            $this->insert($sqlNom,[$request,0,0,0,$this->intVigencia,"S",0]);
            $this->update("UPDATE hum_novedadespagos_cab SET prenomina=? WHERE id = $this->intIdNovedad",[$request]);
            $this->update("UPDATE hum_retencionesfun_cab SET id_nom=? WHERE novedad_id = $this->intIdNovedad",[$request]);
            return $request;
        }
        public function insertLiquidacionDet(int $intConsecutivo,array $arrData,array $arrDescuentos,array $arrRetenciones){
            $this->intConsecutivo = $intConsecutivo;
            foreach ($arrData as $data) {
                if($data['type']=="det" && $data['salario'] > 0){
                    $sql="INSERT INTO humnomina_det(id_nom, cedulanit, salbas, diaslab, devendias, ibc, salud,
                    saludemp, pension, pensionemp, fondosolid, retefte, otrasdeduc, totaldeduc, netopagar, estado, cajacf,
                    sena, icbf, instecnicos, esap, tipofondopension, basepara, basearp, arp, totalsalud,
                    totalpension, tipopago, idfuncionario,detalle,vinculacion,ptto_salario,ptto_pago,ptto_para,bpin,indicador_producto,fuente,cc,
                    cesantias,tercero_eps,tercero_arl,tercero_afp,tercero_fondo)
                    VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
                    $this->insert($sql,[
                        $this->intConsecutivo,
                        $data['documento'],
                        $data['salario'],
                        $data['dias'],
                        $data['total'],
                        $data['valor_ibc'],
                        $data['valor_salud_empleado'],
                        $data['valor_salud_empresa'],
                        $data['valor_pension_empleado'],
                        $data['valor_pension_empresa'],
                        $data['valor_fondo'],
                        $data['valor_retencion'],
                        $data['valor_descuento'],
                        $data['valor_total_descuentos'],
                        $data['valor_neto'],
                        "S",
                        $data['valor_aporte_ccf'],
                        $data['valor_aporte_sena'],
                        $data['valor_aporte_icbf'],
                        $data['valor_aporte_inst'],
                        $data['valor_aporte_esap'],
                        $data['tipo_fondo'],
                        $data['valor_ibc_para'],
                        $data['valor_ibc_arl'],
                        $data['valor_arl'],
                        $data['valor_total_salud'],
                        $data['valor_total_pension'],
                        $data['tipo_pago'],
                        $data['id'],
                        $data['nombre'],
                        $data['vinculacion'],
                        $data['ptto_salario'],
                        $data['ptto_pago'],
                        $data['ptto_para'],
                        $data['bpin'] !="" ? $data['bpin'] : "",
                        $data['indicador'] != "" ? $data['indicador'] : "",
                        $data['fuente'] != "" ? $data['fuente'] : "",
                        $data['centro'] != "" ? $data['centro'] : "",
                        $data['cesantias'],
                        $data['tercero_eps'],
                        $data['tercero_arl'],
                        $data['tercero_afp'],
                        $data['tercero_fondo']
                    ]);
                }
            }
            foreach ($arrDescuentos as $data) {
                if($data['type']=="valor"){
                    $sql = "INSERT INTO humnomina_det_desc (id_nom, tercero, descripcion, valor, tipo_pago,contrato_id,tipo_retencion,tipo_descuento) VALUES(?,?,?,?,?,?,?,?)";
                    $dato = [
                        $this->intConsecutivo,
                        $data['documento'],
                        $data['descripcion'],
                        $data['valor'],
                        $data['tipo_pago'],
                        $data['contrato_id'],
                        $data['tipo_retencion'],
                        "DE"
                    ];
                    $this->insert($sql,$dato);
                }
            }
            foreach ($arrRetenciones as $data) {
                $sql = "INSERT INTO humnomina_det_desc (id_nom, tercero, descripcion, valor, tipo_pago,contrato_id,tipo_retencion,tipo_descuento) VALUES(?,?,?,?,?,?,?,?)";
                $dato = [
                    $this->intConsecutivo,
                    $data['documento'],
                    $data['descripcion'],
                    $data['valor'],
                    $data['tipo_pago'],
                    $data['contrato_id'],
                    $data['tipo_retencion'],
                    "RE"
                ];
                $this->insert($sql,$dato);
            }
        }
        public function insertRetencion(string $strFecha,array $arrNovedad,int $intTotal){
            $this->intTotal = $intTotal;
            $this->strFecha = $strFecha;
            $this->intIdNovedad = $arrNovedad['id'];
            $sql = "INSERT INTO hum_retencionesfun_cab (novedad_id,total,fecha) VALUES(?,?,?)";
            $request = $this->insert($sql,[$this->intIdNovedad,$this->intTotal,$this->strFecha]);
            return $request;
        }
        public function updateRetencion(int $intConsecutivo,string $strFecha,int $intTotal){
            $this->intConsecutivo = $intConsecutivo;
            $this->intTotal = $intTotal;
            $this->strFecha = $strFecha;
            $sql = "UPDATE hum_retencionesfun_cab SET total=?,fecha=? WHERE id = $this->intConsecutivo";
            $request = $this->update($sql,[$this->intTotal,$this->strFecha]);
            return $request;
        }
        public function updateLiquidacion(int $intConsecutivo){
            $this->intConsecutivo = $intConsecutivo;
            $this->intIdNovedad = $this->select("SELECT novedad_id FROM humnomina WHERE id_nom = $this->intConsecutivo")['novedad_id'];
            $sql = "SELECT * FROM hum_nom_cdp_rp WHERE nomina = $this->intConsecutivo AND solicitud != 0";
            $request = $this->select($sql);
            if(empty($request)){
                $sql = "UPDATE humnomina SET estado = ? WHERE id_nom = $this->intConsecutivo";
                $request = $this->update($sql,['N']);
                $this->delete("DELETE FROM humnomina_det_desc WHERE id_nom = $this->intConsecutivo");
                $this->update("UPDATE hum_nom_cdp_rp SET estado = ? WHERE nomina = $this->intConsecutivo",['N']);
                $this->update("UPDATE hum_novedadespagos_cab SET prenomina=? WHERE prenomina  = $this->intConsecutivo AND id = $this->intIdNovedad",[0]);
                $this->update("UPDATE hum_retencionesfun_cab SET id_nom=? WHERE id_nom = $this->intConsecutivo AND novedad_id = $this->intIdNovedad",[0]);
                $return = $request;
            }else{
                $return ="existe";
            }
            return $return;
        }
        public function insertRetencionDet(int $intConsecutivo, array $arrData){
            $this->intConsecutivo = $intConsecutivo;
            $this->delete("DELETE FROM hum_retencionesfun WHERE cc = $this->intConsecutivo");
            foreach ($arrData as $data) {
                $sql="INSERT INTO hum_retencionesfun (docfuncionario,nomfuncionario,valorretencion,tipopago,cod_funcionario,cc)
                VALUES(?,?,?,?,?,?)";
                $this->insert($sql,[
                    $data['documento'],
                    $data['nombre'],
                    $data['retencion'],
                    $data['tipo_pago'],
                    $data['id'],
                    $this->intConsecutivo
                ]);
            }
        }
        public function selectRetencionEdit(int $intConsecutivo){
            $this->intConsecutivo = $intConsecutivo;
            $sql = "SELECT id,novedad_id,date_format(fecha,'%Y-%m-%d') as fecha,estado,id_nom
            FROM hum_retencionesfun_cab WHERE id=$this->intConsecutivo";
            $request = $this->select($sql);
            if(!empty($request)){
                $sql = "SELECT cod_funcionario,valorretencion as retencion,tipopago as tipo_pago
                FROM hum_retencionesfun WHERE cc =$this->intConsecutivo";
                $request['detalle'] = $this->select_all($sql);
            }
            return $request;
        }
        public function updateRetencionesStatus(int $intConsecutivo,string $estado){
            $this->intConsecutivo = $intConsecutivo;
            $sql = "UPDATE hum_retencionesfun_cab SET estado = ? WHERE id = $this->intConsecutivo";
            $request = $this->update($sql,[$estado]);
            return $request;
        }
        public function selectRetencionesSearch($strFechaInicial,$strFechaFinal,$strCodigo){
            $sql = "SELECT id,
            novedad_id,
            estado,
            total,
            id_nom,
            DATE_FORMAT(fecha,'%d/%m/%Y') as fecha
            FROM hum_retencionesfun_cab
            WHERE fecha BETWEEN '$strFechaInicial' AND '$strFechaFinal' AND id LIKE '$strCodigo%'
            ORDER BY id DESC";
            $request = $this->select_all($sql);
            $total = count($request);
            for ($i=0; $i < $total; $i++) {
                $request[$i]['is_status'] = $request[$i]['estado'] =="S"? 1 : 0;
            }
            return $request;
        }
        public function selectLiquidacionesSearch($strFechaInicial,$strFechaFinal,$strCodigo){
            $sql = "SELECT cab.id_nom as id,
            cab.vigencia,
            cab.estado,
            me.nombre as mes,
            cab.descripcion,
            DATE_FORMAT(cab.fecha,'%d/%m/%Y') as fecha
            FROM humnomina cab
            LEFT JOIN meses me ON me.id = cab.mes
            WHERE cab.fecha BETWEEN '$strFechaInicial' AND '$strFechaFinal' AND cab.id_nom LIKE '$strCodigo%'
            ORDER BY cab.id_nom DESC";
            $request = $this->select_all($sql);
            return $request;
        }
        public function selectLiquidacion(int $intConsecutivo){
            $this->intConsecutivo = $intConsecutivo;
            $sql="SELECT cab.id_nom,cab.fecha,m.nombre as mes,
            cab.vigencia,cab.estado
            FROM humnomina cab
            LEFT JOIN meses m ON cab.mes = m.id
            WHERE cab.id_nom = $this->intConsecutivo";
            $request = $this->select($sql);
            if(!empty($request)){
                $sql = "SELECT det.id_nom,
                det.cedulanit as documento,
                det.salbas as salario,
                det.diaslab dias,
                det.devendias as total,
                det.ibc as valor_ibc,
                det.salud as valor_salud_empleado,
                det.saludemp as valor_salud_empresa,
                det.pension as valor_pension_empleado,
                det.pensionemp as valor_pension_empresa,
                det.fondosolid as valor_fondo,
                det.retefte as valor_retencion,
                det.otrasdeduc as valor_descuento,
                det.totaldeduc as valor_total_descuentos,
                det.netopagar as valor_neto,
                det.cajacf as valor_aporte_ccf,
                det.sena as valor_aporte_sena,
                det.icbf as valor_aporte_icbf,
                det.instecnicos as valor_aporte_inst,
                det.esap as valor_aporte_esap,
                det.basepara as valor_ibc_para,
                det.basearp as valor_ibc_arl,
                det.arp as valor_arl,
                det.totalsalud as valor_total_salud,
                det.totalpension as valor_total_pension,
                det.tipopago as tipo_pago,
                det.idfuncionario as id,
                det.detalle as descripcion,
                vap.nombre as nombre_variable,
                CASE WHEN t.razonsocial IS NULL OR t.razonsocial = ''
                THEN CONCAT(t.nombre1,' ',t.nombre2,' ',t.apellido1,' ',t.apellido2)
                ELSE t.razonsocial END AS nombre
                FROM humnomina_det det
                LEFT JOIN ccpethumvariables vap ON det.tipopago = vap.codigo
                LEFT JOIN terceros t ON det.cedulanit = t.cedulanit
                WHERE det.id_nom = $this->intConsecutivo";
                $request['detalle'] = $this->select_all($sql);
            }
            return $request;
        }
        public function selectNovedades(){
            $sql = "SELECT
            cab.id,
            cab.vigencia,
            cab.mes,
            cab.descripcion,
            mon.nombre
            FROM hum_novedadespagos_cab cab
            INNER JOIN meses mon ON mon.id = cab.mes
            WHERE cab.estado ='S' AND cab.prenomina =0 ORDER BY cab.id DESC";
            $request = $this->select_all($sql);
            return $request;
        }
        public function selectNovedad(int $intConsecutivo){
            $arrParafiscales = $this->selectParafiscales();
            $this->intConsecutivo = $intConsecutivo;

            //Novedad
            $sql = "SELECT det.tipo,det.idfun as contrato_id,det.documento,det.valorb as salario,
            det.dias,det.valord as total,det.pse,det.psr,det.ppe,det.ppr,det.parl,det.dias_laborados,
            det.pccf,det.picbf,det.psena,det.pitec,det.pesap,var.nombre,
            CASE WHEN t.razonsocial IS NULL OR t.razonsocial = ''
            THEN CONCAT(t.nombre1,' ',t.nombre2,' ',t.apellido1,' ',t.apellido2)
            ELSE t.razonsocial END AS nombre_tercero,
            cab.mes,
            cab.vigencia
            FROM hum_novedadespagos det
            LEFT JOIN hum_novedadespagos_cab cab ON det.codigo = cab.id
            LEFT JOIN terceros t ON det.documento = t.cedulanit
            LEFT JOIN ccpethumvariables var ON var.codigo = det.tipo
            WHERE det.codigo = $this->intConsecutivo";

            //Retenciones
            $sqlRetencion="SELECT det.valorretencion as retencion,
            det.docfuncionario as documento,
            det.tipopago as tipo_pago,
            cab.tipo_retencion,
            concat('RETENCIÓN POR ',re.nombre) as descripcion
            FROM hum_retencionesfun det
            INNER JOIN hum_retencionesfun_cab cab ON cab.id = det.cc
            LEFT JOIN tesoretenciones re ON re.id = cab.tipo_retencion
            WHERE cab.estado = 'S' AND cab.novedad_id = $this->intConsecutivo";

            $request = $this->select_all($sql);
            $requestRet = $this->select_all($sqlRetencion);
            $arrTerceros = array_values(array_unique(array_column($request,'documento')));
            $arrContratos = $this->selectContratos();
            $total = count($arrTerceros);
            $arrData = [];
            for ($i=0; $i < $total ; $i++) {
                $tercero = $arrTerceros[$i];
                $arrRetenciones = array_values(array_filter($requestRet,function($e)use($tercero){
                    return $tercero == $e['documento'];
                }));
                $arrNovedades = array_values(array_filter($request,function($e)use($tercero){
                    return $tercero == $e['documento'];
                }));
                $contrato = array_values(array_filter($arrContratos,function($e)use($tercero){
                    return $tercero == $e['tercero'];
                }))[0];
                $seguridad = $contrato['seguridad'];
                $presupuesto = $contrato['presupuesto'];
                $floatPension = floatval($seguridad['pension']);
                $floatSalud = floatval($seguridad['salud']);
                $floatArl = floatval($seguridad['arl']);
                $arrComun = $arrNovedades[0];
                $idFuncionario = $arrComun['contrato_id'];
                $mes = $arrComun['mes'];
                $vigencia = $arrComun['vigencia'];
                //Horas extras
                $sqlExtra = "SELECT * FROM hum_novedadextras WHERE cab_id = '$this->intConsecutivo' AND contrato_id = '$idFuncionario'";
                $arrExtra = $this->select($sqlExtra);
                if(!empty($arrExtra)){
                    $totalHoras = $arrExtra['ord_diurna']+$arrExtra['ord_nocturna']+$arrExtra['rec_dom_diurna']+
                    $arrExtra['rec_dom_nocturna']+$arrExtra['rec_nocturna']+$arrExtra['dom_diurna']+$arrExtra['dom_nocturna'];
                    $totalDias = ceil($totalHoras/24);
                    $arrExtra['documento'] = $tercero;
                    $arrExtra['nombre_tercero'] = $arrNovedades[0]['nombre_tercero'];
                    $arrExtra['nombre'] = "HORAS EXTRAS, DOMINICALES, FESTIVOS Y RECARGOS";
                    $arrExtra['tipo'] ="02";
                    $arrExtra['dias'] = $totalDias;
                    $arrExtra['contrato_id'] = $idFuncionario;
                    array_push($arrNovedades,$arrExtra);
                }
                //Vacaciones
                $sqlVacaciones = "SELECT COALESCE(SUM(det.dias_vaca),0) as dias,
                COALESCE(SUM(det.valor_total),0) as total,cab.paga_ibc,cab.paga_arl,cab.paga_para,cab.salario
                FROM hum_vacaciones_det det
                LEFT JOIN hum_vacaciones cab ON cab.num_vaca = det.num_vaca
                WHERE det.doc_funcionario = '$tercero' AND det.mes = '$mes'
                AND det.vigencia = '$vigencia' AND cab.estado = 'S'";

                //Incapacidades
                $sqlIncapacidad = "SELECT COALESCE(SUM(det.dias_inca),0) as dias,
                COALESCE(SUM(det.valor_total),0) as total,
                cab.paga_ibc,
                cab.paga_arl,
                cab.paga_para,
                cab.salario
                FROM hum_incapacidades_det det
                LEFT JOIN hum_incapacidades cab ON cab.num_inca = det.num_inca
                WHERE det.doc_funcionario = '$tercero' AND det.mes = '$mes'
                AND det.vigencia = '$vigencia' AND cab.estado = 'S' AND det.tipo_inca != 'LNR'";

                //Descuentos
                $mesTemp =$mes < 10 ? "0".$mes : $mes;
                $fechaDescuento = $vigencia."-".$mesTemp;
                $sqlDescuentos = "SELECT
                cab.descripcion,
                cab.id as id_descuento,
                cab.id_retencion,
                cab.idfuncionario,
                det.valor,
                det.cuota,
                det.tipo_pago as tipo
                FROM humretenempleados cab
                INNER JOIN humretenempleados_det det ON cab.id = det.cab_id
                WHERE cab.empleado = '$tercero' AND SUBSTR(cab.fecha,1,7) = '$fechaDescuento'
                AND SUBSTR(det.mes,1,7) = '$fechaDescuento' AND cab.estado = 'S' AND det.estado='S'";

                $arrVacaciones = $this->select($sqlVacaciones);
                $arrIncapacidad = $this->select($sqlIncapacidad);
                $arrDescuentos = $this->select_all($sqlDescuentos);
                $totalDiasNovedad = 0;
                if(!empty($arrVacaciones) && $arrVacaciones['total'] > 0){
                    $totalDiasNovedad += $arrVacaciones['dias'];
                    array_push($arrNovedades,[
                        "documento"=>$tercero,
                        "salario"=>$arrVacaciones['salario'],
                        "nombre_tercero"=>$arrComun['nombre_tercero'],
                        "nombre"=>"VACACIONES - SUELDO PERSONAL DE NOMINA",
                        "dias"=>$arrVacaciones['dias'],
                        "total"=>round($arrVacaciones['total']),
                        "pse"=>$arrVacaciones['paga_ibc'],
                        "psr"=>$arrVacaciones['paga_ibc'],
                        "ppe"=>$arrVacaciones['paga_ibc'],
                        "ppr"=>$arrVacaciones['paga_ibc'],
                        "parl"=>$arrVacaciones['paga_arl'],
                        "pccf"=>$arrVacaciones['paga_para'],
                        "psena"=>$arrVacaciones['paga_para'],
                        "picbf"=>$arrVacaciones['paga_para'],
                        "pitec"=>$arrVacaciones['paga_para'],
                        "pesap"=>$arrVacaciones['paga_para'],
                        "periodo"=>$contrato['dias'],
                        "novedad"=>2,
                        "tipo"=>"01",
                        "contrato_id"=>$idFuncionario
                    ]);
                }
                if(!empty($arrIncapacidad) && $arrIncapacidad['total'] > 0){
                    $totalDiasNovedad += $arrIncapacidad['dias'];
                    array_push($arrNovedades,[
                        "documento"=>$tercero,
                        "salario"=>$arrIncapacidad['salario'],
                        "nombre_tercero"=>$arrComun['nombre_tercero'],
                        "nombre"=>"INCAPACIDADES - SUELDO PERSONAL DE NOMINA",
                        "dias"=>$arrIncapacidad['dias'],
                        "total"=>round($arrIncapacidad['total']),
                        "pse"=>$arrIncapacidad['paga_ibc'],
                        "psr"=>$arrIncapacidad['paga_ibc'],
                        "ppe"=>$arrIncapacidad['paga_ibc'],
                        "ppr"=>$arrIncapacidad['paga_ibc'],
                        "parl"=>$arrIncapacidad['paga_arl'],
                        "pccf"=>$arrIncapacidad['paga_para'],
                        "psena"=>$arrIncapacidad['paga_para'],
                        "picbf"=>$arrIncapacidad['paga_para'],
                        "pitec"=>$arrIncapacidad['paga_para'],
                        "pesap"=>$arrIncapacidad['paga_para'],
                        "periodo"=>$contrato['dias'],
                        "novedad"=>1,
                        "tipo"=>"01",
                        "contrato_id"=>$idFuncionario
                    ]);
                }
                $totalNovedades = count($arrNovedades);
                for ($j=0; $j < $totalNovedades; $j++) {
                    $totalDescuentos = 0;
                    $novedad = $arrNovedades[$j];
                    $novedad['descuento'] = 0;
                    $novedad['retencion'] = 0;
                    $arrNovedadDescuento = array_values(array_filter($arrDescuentos,function($e) use($novedad){
                        return $novedad['tipo'] == $e['tipo'];
                    }));
                    if(!empty($arrRetenciones) && $novedad['tipo'] == $arrRetenciones[0]['tipo_pago']){
                        $novedad['retencion'] = $arrRetenciones[0]['retencion'];
                    }
                    if(!empty($arrNovedadDescuento)){
                        foreach ($arrNovedadDescuento as $desc) {$totalDescuentos += $desc['valor'];}
                        $novedad['descuento'] = $totalDescuentos;
                    }
                    if($novedad['tipo'] == "01" && !isset($novedad['novedad'])){
                        $novedad['dias'] = $novedad['dias']-$totalDiasNovedad;
                    }else if(($novedad['tipo'] == "04" || $novedad['tipo'] == "05")){
                        $novedad['dias'] = $novedad['dias']-$totalDiasNovedad;
                    }
                    $arrNovedades[$j] = $novedad;
                    $arrNovedades[$j]['pension'] = $floatPension/100;
                    $arrNovedades[$j]['salud'] = $floatSalud/100;
                    $arrNovedades[$j]['arl'] = $floatArl/100;
                    $arrNovedades[$j]['salud_empresa'] = $arrParafiscales['salud_empresa']['porcentaje']/100;
                    $arrNovedades[$j]['pension_empresa'] = $arrParafiscales['pension_empresa']['porcentaje']/100;
                    $arrNovedades[$j]['aporte_ccf'] = $arrParafiscales['aporte_ccf']['porcentaje']/100;
                    $arrNovedades[$j]['aporte_sena'] = $arrParafiscales['aporte_sena']['porcentaje']/100;
                    $arrNovedades[$j]['aporte_icbf'] = $arrParafiscales['aporte_icbf']['porcentaje']/100;
                    $arrNovedades[$j]['aporte_inst'] = $arrParafiscales['aporte_inst']['porcentaje']/100;
                    $arrNovedades[$j]['aporte_esap'] = $arrParafiscales['aporte_esap']['porcentaje']/100;
                    $arrNovedades[$j]['tipo_fondo'] = $seguridad['tipo_fondo'];
                    $arrNovedades[$j]['cesantias'] = $seguridad['cesantias'];
                    $arrNovedades[$j]['tercero_afp'] = $seguridad['tercero_afp'];
                    $arrNovedades[$j]['tercero_arl'] = $seguridad['tercero_arl'];
                    $arrNovedades[$j]['tercero_eps'] = $seguridad['tercero_eps'];
                    $arrNovedades[$j]['tercero_fondo'] = $seguridad['tercero_fondo'];
                    $arrNovedades[$j]['ptto_para'] = $presupuesto['ptto_para'];
                    $arrNovedades[$j]['ptto_salario'] = $presupuesto['ptto_salario'];
                    $arrNovedades[$j]['ptto_pago'] = $presupuesto['ptto_pago'];
                    $arrNovedades[$j]['vinculacion'] = $presupuesto['vinculacion'];
                    $arrNovedades[$j]['bpin'] = $presupuesto['bpin'];
                    $arrNovedades[$j]['indicador'] = $presupuesto['indicador'];
                    $arrNovedades[$j]['fuente'] = $presupuesto['fuente'];
                    $arrNovedades[$j]['centro'] = $presupuesto['centro'];
                }
                sort($arrNovedades);
                array_push($arrData,array("novedades"=>$arrNovedades,"descuentos"=>$arrDescuentos,"parafiscales"=>$arrParafiscales,"retenciones"=>$arrRetenciones));
            }
            return $arrData;
        }
        public function selectParafiscales(){
            $sql = "SELECT codigo,nombre,tipo,estado,porcentaje FROM humparafiscalesccpet";
            $request = $this->select_all($sql);
            return [
                "salud_empresa"=>array_values(array_filter($request,function($e){return $e['codigo']==26;}))[0],
                "pension_empresa"=>array_values(array_filter($request,function($e){return $e['codigo']==25;}))[0],
                "aporte_ccf"=>array_values(array_filter($request,function($e){return $e['codigo']==28;}))[0],
                "aporte_sena"=>array_values(array_filter($request,function($e){return $e['codigo']==31;}))[0],
                "aporte_icbf"=>array_values(array_filter($request,function($e){return $e['codigo']==30;}))[0],
                "aporte_inst"=>array_values(array_filter($request,function($e){return $e['codigo']==33;}))[0],
                "aporte_esap"=>array_values(array_filter($request,function($e){return $e['codigo']==32;}))[0],
            ];
        }
        public function selectFondoSolidario(){
            $salario = $this->selectGenerales()['salario'];
            $sql = "SELECT * FROM humfondosoli WHERE estado = 'S'";
            $request = $this->select_all($sql);
            return array_map(function($e)use($salario){
                $e['rangoinicial'] = $e['rangoinicial']*$salario;
                $e['rangofinal'] = $e['rangofinal']*$salario;
                $e['porcentaje'] = $e['porcentaje']/100;
                return $e;
            },$request);
        }
        public function selectVariablesPago(){
            $sql = "SELECT * FROM ccpethumvariables WHERE estado = 'S'";
            $request = $this->select_all($sql);
            if(!empty($request)){
                $total = count($request);
                for ($i=0; $i < $total ; $i++) {
                    $request[$i]['detalle'] = [];
                    $request[$i]['is_checked_salud_empleado'] = 1;
                    $request[$i]['is_checked_salud_empresa'] = 1;
                    $request[$i]['is_checked_pension_empleado'] = 1;
                    $request[$i]['is_checked_pension_empresa'] = 1;
                    $request[$i]['is_checked_arl'] = 1;
                    $request[$i]['is_checked_ccf'] = 1;
                    $request[$i]['is_checked_icbf'] = 1;
                    $request[$i]['is_checked_sena'] = 1;
                    $request[$i]['is_checked_inst_tecnico'] = 1;
                    $request[$i]['is_checked_esap'] = 1;
                    $request[$i]['nombre'] = ucfirst(strtolower($request[$i]['nombre']));
                }
            }
            return $request;
        }
        public function selectParametrosNomina(){
            $sql = "SELECT * FROM humparametrosliquida";
            $request = $this->select($sql);
            return $request;
        }
        public function selectGenerales(){
            $sql = "SELECT salario,transporte,alimentacion,uvt FROM admfiscales ORDER BY vigencia DESC";
            $request = $this->select($sql);
            return $request;
        }
        public function selectMeses(){
            $sql = "SELECT * FROM meses WHERE estado = 'S'";
            $request = $this->select_all($sql);
            return $request;
        }
        public function selectContratos(){
            $sql = "SELECT
            cab.id as codigo,
            cab.tercero,
            cab.tipo,
            cab.periodo,
            DATE_FORMAT(cab.fecha_inicial,'%d/%m/%Y') as fecha_inicial,
            DATE_FORMAT(cab.fecha_final,'%d/%m/%Y') as fecha_final,
            cab.banco,
            cab.cuenta,
            cab.cargo_id,
            cab.estado,
            cab.automatico,
            c.nombrecargo as nombre_cargo,
            s.valor as salario,
            s.id_nivel as escala,
            s.nombre as nivel_salarial,
            CASE WHEN t.razonsocial IS NULL OR t.razonsocial = ''
            THEN CONCAT(t.nombre1,' ',t.nombre2,' ',t.apellido1,' ',t.apellido2)
            ELSE t.razonsocial END AS nombre,
            b.nombre as nombre_banco,
            p.nombre as nombre_periodo,
            t.direccion,
            t.tipodoc,
            p.dias
            FROM hum_contratos cab
            LEFT JOIN terceros t ON cab.tercero = t.cedulanit
            LEFT JOIN planaccargos c ON cab.cargo_id = c.codcargo
            LEFT JOIN humnivelsalarial s ON s.id_nivel=c.clasificacion
            LEFT JOIN humperiodos p ON cab.periodo = p.id_periodo
            LEFT JOIN hum_bancosfun b ON cab.banco = b.codigo
            WHERE cab.estado != 'TERMINADO'
            ORDER BY cab.id DESC";
            $request = $this->select_all($sql);
            if(!empty($request)){
                $total = count($request);
                for ($i=0; $i < $total ; $i++) {
                    $contrato = $request[$i];
                    if($contrato['estado'] =="RENOVADO"){
                        $idContrato = $contrato['codigo'];
                        $sqlRev = "SELECT
                        cab.id,
                        cab.tercero,
                        cab.tipo,
                        cab.periodo,
                        DATE_FORMAT(cab.fecha_inicial,'%d/%m/%Y') as fecha_inicial,
                        DATE_FORMAT(cab.fecha_final,'%d/%m/%Y') as fecha_final,
                        cab.banco,
                        cab.cuenta,
                        cab.cargo_id,
                        cab.novedad_id,
                        cab.jornada,
                        cab.contrato,
                        c.nombrecargo as nombre_cargo,
                        s.valor as salario,
                        s.id_nivel as escala,
                        s.nombre as nivel_salarial,
                        CASE WHEN t.razonsocial IS NULL OR t.razonsocial = ''
                        THEN CONCAT(t.nombre1,' ',t.nombre2,' ',t.apellido1,' ',t.apellido2)
                        ELSE t.razonsocial END AS nombre,
                        b.nombre as nombre_banco,
                        p.nombre as nombre_periodo,
                        t.direccion,
                        t.tipodoc,
                        p.dias
                        FROM hum_contratos_renovacion cab
                        LEFT JOIN terceros t ON cab.tercero = t.cedulanit
                        LEFT JOIN planaccargos c ON cab.cargo_id = c.codcargo
                        LEFT JOIN humnivelsalarial s ON s.id_nivel=c.clasificacion
                        LEFT JOIN humperiodos p ON cab.periodo = p.id_periodo
                        LEFT JOIN hum_bancosfun b ON cab.banco = b.codigo
                        LEFT JOIN hum_contratos_novedades nov ON nov.id = cab.novedad_id
                        WHERE nov.contrato_id = $idContrato ORDER BY nov.id DESC LIMIT 1";
                        $arrRev = $this->select($sqlRev);
                        $request[$i]['fecha_inicial'] = $arrRev['fecha_inicial'];
                        $request[$i]['fecha_final'] = $arrRev['fecha_final'];
                        $request[$i]['salario'] = $arrRev['salario'];
                        $request[$i]['nombre_cargo'] = $arrRev['nombre_cargo'];
                        $request[$i]['nombre_periodo'] = $arrRev['nombre_periodo'];
                        $request[$i]['tipo'] = $arrRev['tipo'];

                    }
                    $sqlSeg = "SELECT seg.pension,seg.salud,arl.tarifa as arl,te.tipoemprse as tipo_fondo,
                    seg.cesantias,seg.tercero_eps,seg.tercero_arl,seg.tercero_afp,seg.tercero_fondo
                    FROM hum_contratos_seguridad seg
                    INNER JOIN hum_nivelesarl arl ON arl.id = seg.riesgo_id
                    LEFT JOIN hum_terceros_emprse te ON seg.tercero_afp = te.numdocumento
                    WHERE seg.contrato_id = {$contrato['codigo']} ORDER BY seg.id DESC LIMIT 1";

                    $sqlPtto = "SELECT ptto.id,
                    ptto.contrato_id,
                    ptto.vinculacion,
                    ptto.ptto_salario,
                    ptto.ptto_pago,
                    ptto.ptto_para,
                    ptto.fuente,
                    ptto.indicador,
                    bpin.codigo AS bpin,
                    ptto.cc_id as centro
                    FROM hum_contratos_ptto ptto
                    LEFT JOIN ccpproyectospresupuesto AS bpin ON ptto.id_bpin = bpin.id
                    WHERE contrato_id = {$contrato['codigo']}
                    ORDER BY id DESC LIMIT 1";

                    $request[$i]['presupuesto'] =  $this->select($sqlPtto);
                    $request[$i]['seguridad'] = $this->select($sqlSeg);
                    $request[$i]['is_checked'] = 0;
                }
            }
            return $request;
        }
    }
?>
