<?php
    require_once '../../../Librerias/core/Helpers.php';
    require_once '../../../Librerias/core/Mysql.php';
    session_start();
    /*ini_set('display_errors', '1');
    ini_set('display_startup_errors', '1');
    error_reporting(E_ALL);*/
    class AprobarModel extends Mysql{
        private $intConsecutivo;
        private $intVigencia;
        private $strFecha;
        private $strFechaInicial;
        private $strFechaFinal;
        private $strCodigo;
        private $strLiquidacion;
        private $strDescripcion;
        private $intMovimiento;
        private $intTipoComp;
        private $intIdComp;
        private $intValor;

        function __construct(){
            parent::__construct();
            $this->intVigencia = date_create(date("Y-m-d"))->format('Y');
        }
        public function selectAprobados(){
            $sql = "SELECT id_nom as nomina,id_aprob as id FROM humnomina_aprobado  WHERE estado = 'S' AND tipo_mov = '201' AND vigencia = $this->intVigencia";
            $request = $this->select_all($sql);
            $total = count($request);
            $arrData = [];
            for ($i=0; $i < $total ; $i++) {
                $data = $request[$i];
                $sql = "SELECT * FROM tesoegresosnomina WHERE id_orden = {$data['nomina']}";
                $req = $this->select_all($sql);
                if(empty($req)){array_push($arrData,$data);}
            }
            return $arrData;
        }
        public function selectLiquidaciones(){
            $sql = "SELECT
            cdprp.nomina,
            cdprp.cdp,
            cdprp.rp,
            cdprp.solicitud,
            sol.total as total_solicitud,
            cdp.valor as total_cdp,
            rp.valor as total_rp,
            hum.descripcion,
            hum.mes,
            cdprp.vigencia
            FROM hum_nom_cdp_rp cdprp
            INNER JOIN humnomina hum ON cdprp.nomina = hum.id_nom
            INNER JOIN ccpetcdp cdp ON cdprp.cdp = cdp.consvigencia AND cdprp.vigencia = cdp.vigencia AND cdp.tipo_mov = '201'
            INNER JOIN ccpetrp rp ON cdprp.rp = rp.consvigencia AND cdprp.vigencia = rp.vigencia AND rp.tipo_mov = '201'
            INNER JOIN ccpet_solicitudnomina_cdp_cab sol ON cdprp.solicitud = sol.id AND cdprp.vigencia = sol.vigencia
            WHERE cdprp.estado = 'S' AND sol.estado = 'S' AND hum.estado = 'S' AND cdprp.vigencia = $this->intVigencia";
            $request = $this->select_all($sql);
            $total = count($request);
            $arrData = [];
            if($total > 0){
                for ($i=0; $i < $total ; $i++) {
                    $sql = "SELECT * FROM humnomina_aprobado WHERE id_nom = {$request[$i]['nomina']} AND estado = 'S'";
                    $req = $this->select($sql);
                    $data = $request[$i];
                    if(empty($req)){
                        $data['format_solicitud'] = formatNum($data['total_solicitud']);
                        $data['format_cdp'] = formatNum($data['total_cdp']);
                        $data['format_rp'] = formatNum($data['total_rp']);
                        array_push($arrData,$data);
                    }
                }
            }
            return $arrData;
        }
        public function selectNominaDetalle(int $strLiquidacion){
            $this->strLiquidacion = $strLiquidacion;
            $sql = "SELECT det.id_nom,
                    det.cedulanit as documento,
                    det.salbas as salario,
                    det.diaslab dias,
                    det.devendias as total,
                    det.ibc as valor_ibc,
                    det.salud as valor_salud_empleado,
                    det.saludemp as valor_salud_empresa,
                    det.pension as valor_pension_empleado,
                    det.pensionemp as valor_pension_empresa,
                    det.fondosolid as valor_fondo,
                    det.retefte as valor_retencion,
                    det.otrasdeduc as valor_descuento,
                    det.totaldeduc as valor_total_descuentos,
                    det.netopagar as valor_neto,
                    det.cajacf as valor_aporte_ccf,
                    det.sena as valor_aporte_sena,
                    det.icbf as valor_aporte_icbf,
                    det.instecnicos as valor_aporte_inst,
                    det.esap as valor_aporte_esap,
                    det.basepara as valor_ibc_para,
                    det.basearp as valor_ibc_arl,
                    det.arp as valor_arl,
                    det.totalsalud as valor_total_salud,
                    det.totalpension as valor_total_pension,
                    det.tipopago as tipo_pago,
                    det.idfuncionario as id,
                    det.detalle as descripcion,
                    vap.nombre as nombre_variable,
                    det.vinculacion,
                    det.ptto_salario,
                    det.ptto_pago,
                    det.ptto_para,
                    vap.funcionamiento,
                    vap.inversion,
                    vap.gastoscomerc as comercial,
                    det.cc as centro_costo,
                    det.indicador_producto,
                    CASE WHEN t.razonsocial IS NULL OR t.razonsocial = ''
                    THEN CONCAT(t.nombre1,' ',t.nombre2,' ',t.apellido1,' ',t.apellido2)
                    ELSE t.razonsocial END AS nombre
                    FROM humnomina_det det
                    LEFT JOIN ccpethumvariables vap ON det.tipopago = vap.codigo
                    LEFT JOIN terceros t ON det.cedulanit = t.cedulanit
                    WHERE det.id_nom = $this->strLiquidacion";
            $request = $this->select_all($sql);
            $total = count($request);
            if($total){
                $arrDescuentos = $this->selectNominaDescuentos($this->strLiquidacion);
                for ($i=0; $i < $total ; $i++) {
                    $det = $request[$i];
                    $descuentos = array_values(array_filter($arrDescuentos,function($e) use($det){
                        return $det['id'] == $e['contrato_id'] && $det['tipo_pago'] == $e['tipo_pago'];
                    }));
                    if(!empty($descuentos)){
                        $request[$i]['descuentos'] = $descuentos;
                    }
                }
            }
            return $request;
        }
        public function selectNominaDescuentos(int $strLiquidacion){
            $this->strLiquidacion = $strLiquidacion;
            $sql = "SELECT * FROM humnomina_det_desc WHERE id_nom = $this->strLiquidacion";
            $request = $this->select_all($sql);
            return $request;
        }
        public function selectCuentasSectores(){
            $sql = "SELECT * FROM ccpet_cuentasectores";
            $request = $this->select_all($sql);
            return $request;
        }
        public function selectVariablesPara(){
            $sql = "SELECT codigo,funcionamiento,inversion,gastoscomerc as comercial,sector
            FROM humparafiscalesccpet_det WHERE estado = 'S'";
            $request = $this->select_all($sql);
            return $request;
        }
        public function insertData(array $arrTercero,array $arrLiquidacion,string $strFecha, int $intMovimiento){
            $this->strFecha = $strFecha;
            $this->intMovimiento = $intMovimiento;
            $sql = "SELECT * FROM humnomina_aprobado
            WHERE estado = 'S' AND tipo_mov = '201' AND id_nom ={$arrLiquidacion['nomina']}";
            $request = $this->select_all($sql);
            $return = "";
            if(empty($request)){
                $sql = "INSERT INTO humnomina_aprobado (id_nom,fecha,id_rp,estado,tipo_mov,id_cdp,tercero,vigencia,id_solicitud,descripcion)
                VALUES (?,?,?,?,?,?,?,?,?,?)";
                $arrData = [
                    $arrLiquidacion['nomina'],
                    $this->strFecha,
                    $arrLiquidacion['rp'],
                    "S",
                    $this->intMovimiento,
                    $arrLiquidacion['cdp'],
                    $arrTercero['codigo'],
                    $arrLiquidacion['vigencia'],
                    $arrLiquidacion['solicitud'],
                    $arrLiquidacion['descripcion']
                ];
                $this->update("UPDATE humnomina SET estado =? WHERE id_nom = '{$arrLiquidacion['nomina']}'",['P']);
                $request = $this->insert($sql,$arrData);
                $return = $request;
            }else{
                $return = "existe";
            }
            return $return;
        }
        public function insertComprobanteCab(int $strLiquidacion,string $strDescripcion,string $strFecha,int $intTipoComp,$intValor = 0){
            $this->strLiquidacion = $strLiquidacion;
            $this->strDescripcion = $strDescripcion;
            $this->strFecha = $strFecha;
            $this->intTipoComp = $intTipoComp;
            $this->intValor = $intValor;
			$sql="INSERT INTO comprobante_cab (numerotipo, tipo_comp, fecha, concepto, total, total_debito, total_credito, diferencia, estado)
            VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";
            $data = array($this->strLiquidacion, $this->intTipoComp, $this->strFecha, $this->strDescripcion, 0, 0, 0, 0, 1);
            $request = $this->insert($sql,$data);
            return $request;
        }
        public function insertComprobanteDet(int $strLiquidacion,array $arrData){
            $arrVariablesPara = $this->selectVariablesPara();
            $this->strLiquidacion = $strLiquidacion;
            foreach ($arrData as $det) {
                if(isset($det['cod_para'])){

                    $arrDetCredito = array_values(array_filter($arrVariablesPara,function($e)use($det){return $e['codigo'] == $det['cod_para'];}))[0];
                    $codCredito = $det['tipo'] =="IN" ? $arrDetCredito['inversion'] : $det['tipo'] =="F" ? $arrDetCredito['funcionamiento'] : $arrDetCredito['comercial'];
                    $arrConcepto[0] = getConceptoDet($det['cod_pago'],2,$det['tipo'],$det['centro_costo'],"",$det['sector'])[0];
                    $arrConcepto[1] = array_values(array_filter(
                        getConceptoDet($codCredito,2,$det['tipo'],$det['centro_costo'],"",$det['sector'])
                    ,function($e){return $e['credito'] == "S";}))[0];
                }else if(isset($det['tipo_descuento'])){
                    if($det['tipo_descuento']=="DE"){
                        $debito = array_values(array_filter(
                            getConceptoDet($det['cod_pago'],2,$det['tipo'],$det['centro_costo'],"",$det['sector'])
                        ,function($e){
                            return $e['credito'] == "S";
                        }))[0];
                        $debito['debito'] = "S";
                        $debito['credito'] ="N";
                        $arrConcepto[0] = $debito;
                        $arrConcepto[1]['credito'] = "S";
                        $arrConcepto[1]['cuenta'] = $det['cuenta'];
                    }else{
                        $arrConcepto[0] = getConceptoDet($det['cod_pago'],2,$det['tipo'],$det['centro_costo'],"",$det['sector'])[0];
                        $arrConcepto[1] = array_values(array_filter(
                            getConceptoDet($det['tipo_retencion'],4,"RI",$det['centro_costo'],"",$det['sector'])
                        ,function($e){
                            return $e['credito'] == "S";
                        }))[0];
                    }
                }else{
                    $arrConcepto = getConceptoDet($det['cod_pago'],2,$det['tipo'],$det['centro_costo'],"",$det['sector']);
                }
                foreach($arrConcepto as $concepto){
                    $sql="INSERT INTO comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,valdebito,valcredito,estado,vigencia,tipo_comp,numerotipo)
                    VALUES (?,?,?,?,?,?,?,?,?,?,?)";
                    $data = [
                        "4 ".$this->strLiquidacion,
                        $concepto['cuenta'],
                        $det['documento'],
                        $det['centro_costo'],
                        $det['detalle'],
                        $concepto['debito'] == "S" ? $det['total'] : 0,
                        $concepto['credito'] == "S" ? $det['total'] : 0,
                        1,
                        $this->intVigencia,
                        4,
                        $this->strLiquidacion,
                    ];
                    $this->insert($sql,$data);
                }
            }
        }
        public function insertReversar(string $strDescripcion,int $strLiquidacion,string $strFecha, int $intAprobado){
            $this->strFecha = $strFecha;
            $this->strDescripcion = $strDescripcion;
            $this->strLiquidacion = $strLiquidacion;
            $sql = "SELECT * FROM tesoegresosnomina WHERE id_orden = $this->strLiquidacion";
            $request = $this->select($sql);
            $return ="";
            if(empty($request)){
                $sql  = "SELECT * FROM humnomina_aprobado WHERE tipo_mov = '201' AND estado = 'R' AND id_aprob = $intAprobado";
                $request = $this->select($sql);
                if(empty($request)){
                    $sql  = "SELECT * FROM humnomina_aprobado WHERE tipo_mov = '201' AND estado = 'S' AND id_nom=$this->strLiquidacion";
                    $request = $this->select($sql);
                    $arrAprob = $request;
                    $this->intConsecutivo = $arrAprob['id_aprob'];
                    $sql = "SELECT det.id_comp,
                    det.cuenta,
                    det.tercero,
                    det.centrocosto,
                    det.detalle,
                    det.valdebito,
                    det.valcredito,
                    det.vigencia,
                    det.tipo_comp,
                    det.numerotipo
                    FROM comprobante_cab cab
                    INNER JOIN comprobante_det det ON cab.numerotipo = det.numerotipo AND cab.tipo_comp = det.tipo_comp
                    WHERE cab.estado = 1 AND det.estado = 1 AND cab.numerotipo = $this->strLiquidacion AND cab.tipo_comp = 4";
                    $arrCompDet = $this->select_all($sql);
                    $this->intIdComp = $this->insertComprobanteCab($this->strLiquidacion,$this->strDescripcion,$this->strFecha,2004);
                    if($this->intIdComp > 0){
                        $sql ="UPDATE humnomina_aprobado SET estado = ? WHERE id_aprob = $this->intConsecutivo";
                        $request = $this->update($sql,['R']);
                        if($request > 0){
                            $this->update("UPDATE humnomina SET estado =? WHERE id_nom = $this->strLiquidacion",['S']);
                            $sql = "INSERT INTO humnomina_aprobado (id_aprob,id_nom,fecha,id_rp,estado,tipo_mov,id_cdp,tercero,vigencia,id_solicitud,descripcion)
                            VALUES (?,?,?,?,?,?,?,?,?,?,?)";
                            $arrData = [
                                $this->intConsecutivo,
                                $this->strLiquidacion,
                                $this->strFecha,
                                $arrAprob['id_rp'],
                                "R",
                                "401",
                                $arrAprob['id_cdp'],
                                $arrAprob['tercero'],
                                $arrAprob['vigencia'],
                                $arrAprob['id_solicitud'],
                                $this->strDescripcion
                            ];
                            $request = $this->insert($sql,$arrData);
                            if($request > 0){
                                foreach ($arrCompDet as $comp) {
                                    $sql="INSERT INTO comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,valdebito,valcredito,estado,vigencia,tipo_comp,numerotipo)
                                    VALUES (?,?,?,?,?,?,?,?,?,?,?)";
                                    $data = [
                                        "200".$comp['id_comp'],
                                        $comp['cuenta'],
                                        $comp['tercero'],
                                        $comp['centrocosto'],
                                        $comp['detalle'],
                                        $comp['valcredito'],
                                        $comp['valdebito'],
                                        1,
                                        $this->intVigencia,
                                        "2004",
                                        $this->strLiquidacion,
                                    ];
                                    $req = $this->insert($sql,$data);
                                    if($req == 0){$return = "comp_det"; break;}
                                }
                                $return = $this->intConsecutivo;
                            }else{
                                $return = "aprob_rev";
                            }
                        }else{
                            $return = "aprob";
                        }
                    }else{
                        $return = "comp";
                    }
                }else{
                    $return = "existe";
                }
            }else{
                $return = "egreso";
            }
            return $return;
        }
        public function updateStatus(int $intConsecutivo, int $strLiquidacion){
            $this->intConsecutivo = $intConsecutivo;
            $this->strLiquidacion = $strLiquidacion;
            //$sql = "SELECT * FROM comprobante_cab WHERE tipo_comp = '4' AND numerotipo = $intCodigo";
            $sql = "SELECT * FROM tesoegresosnomina WHERE id_orden = $this->strLiquidacion";
            $request = $this->select($sql);
            $return = "";
            if(empty($request)){
                $sql = "UPDATE humnomina_aprobado SET estado = ? WHERE id_aprob = $this->intConsecutivo";
                $this->update("UPDATE comprobante_cab SET estado = ? WHERE tipo_comp = '4' AND numerotipo = $this->strLiquidacion",[0]);
                $this->update("UPDATE comprobante_det SET estado = ? WHERE tipo_comp = '4' AND numerotipo = $this->strLiquidacion",[0]);
                $this->update("UPDATE humnomina SET estado =? WHERE id_nom = $this->strLiquidacion",['S']);
                $request = $this->update($sql,['N']);
                $return = $request;
            }else{
                $return = "existe";
            }
            return $return;
        }
        public function selectEdit(int $intConsecutivo){
            $this->intConsecutivo = $intConsecutivo;
            $sql = "SELECT
            cab.id_aprob as id,
            cab.id_nom as nomina,
            cab.id_cdp as cdp,
            cab.id_rp as rp,
            cab.id_solicitud as solicitud,
            coalesce(sol.total,0) as total_solicitud,
            coalesce(cdp.valor,0) as total_cdp,
            coalesce(rp.valor,0) as total_rp,
            hum.descripcion,
            cab.fecha,
            cab.vigencia,
            cab.estado,
            cab.tipo_mov,
            t.cedulanit as codigo,CASE WHEN t.razonsocial IS NULL OR t.razonsocial = ''
            THEN CONCAT(t.nombre1,' ',t.nombre2,' ',t.apellido1,' ',t.apellido2)
            ELSE t.razonsocial END AS nombre
            FROM humnomina_aprobado cab
            LEFT JOIN humnomina hum ON cab.id_nom  = hum.id_nom
            LEFT JOIN ccpetcdp cdp ON cab.id_cdp = cdp.consvigencia AND cab.vigencia = cdp.vigencia AND cdp.tipo_mov = '201'
            LEFT JOIN ccpetrp rp ON cab.id_rp = rp.consvigencia AND cab.vigencia = rp.vigencia AND cdp.tipo_mov = '201'
            LEFT JOIN ccpet_solicitudnomina_cdp_cab sol ON cab.id_solicitud = sol.consecutivo AND cab.vigencia = sol.vigencia
            LEFT JOIN terceros t ON cab.tercero = t.cedulanit
            WHERE cab.id_aprob = $this->intConsecutivo AND cab.tipo_mov = '201'";
            $request = $this->select($sql);
            if(!empty($request)){
                $sqlComp = "SELECT * FROM tesoegresosnomina WHERE id_orden = {$request['nomina']}";
                $comp = $this->select($sqlComp);
                $request['comprobante'] = empty($comp) ? 0 : 1;
                $request['format_solicitud'] = formatNum($request['total_solicitud']);
                $request['format_cdp'] = formatNum($request['total_cdp']);
                $request['format_rp'] = formatNum($request['total_rp']);
                $request['reversado'] = $this->select("SELECT * FROM humnomina_aprobado WHERE id_aprob = $this->intConsecutivo AND tipo_mov = '401'");
            }
            return $request;
        }
        public function selectSearch(string $strFechaInicial,string $strFechaFinal,string $strCodigo,string $strLiquidacion){
            $this->strCodigo = $strCodigo;
            $this->strFechaInicial = $strFechaInicial;
            $this->strFechaFinal = $strFechaFinal;
            $this->strLiquidacion = $strLiquidacion;
            $sql = "SELECT
            cab.id_aprob as id,
            cab.id_nom as nomina,
            cab.id_cdp as cdp,
            cab.id_rp as rp,
            cab.id_solicitud as solicitud,
            coalesce(sol.total,0) as total_solicitud,
            coalesce(cdp.valor,0) as total_cdp,
            coalesce(rp.valor,0) as total_rp,
            cab.descripcion,
            cab.vigencia,
            cab.estado,
            DATE_FORMAT(cab.fecha,'%d/%m/%Y') as fecha,
            cab.fecha as fecha_n,
            t.cedulanit as codigo,CASE WHEN t.razonsocial IS NULL OR t.razonsocial = ''
            THEN CONCAT(t.nombre1,' ',t.nombre2,' ',t.apellido1,' ',t.apellido2)
            ELSE t.razonsocial END AS nombre,
            cab.tipo_mov
            FROM humnomina_aprobado cab
            LEFT JOIN ccpetcdp cdp ON cab.id_cdp = cdp.consvigencia AND cab.vigencia = cdp.vigencia AND cdp.tipo_mov = '201'
            LEFT JOIN ccpetrp rp ON cab.id_rp = rp.consvigencia AND cab.vigencia = rp.vigencia AND cdp.tipo_mov = '201'
            LEFT JOIN ccpet_solicitudnomina_cdp_cab sol ON cab.id_solicitud = sol.consecutivo AND cab.vigencia = sol.vigencia
            LEFT JOIN terceros t ON cab.tercero = t.cedulanit
            WHERE cab.fecha BETWEEN '$this->strFechaInicial' AND '$this->strFechaFinal' AND cab.id_nom LIKE '$this->strLiquidacion%'
            AND cab.id_aprob LIKE '$this->strCodigo%' AND cab.tipo_mov = '201' ORDER BY cab.id_aprob DESC ";
            $request = $this->select_all($sql);
            $total = count($request);
            if($total > 0){
                for ($i=0; $i < $total ; $i++) {
                    $sqlComp = "SELECT * FROM tesoegresosnomina WHERE id_orden = {$request[$i]['nomina']}";
                    $comp = $this->select($sqlComp);
                    $request[$i]['comprobante'] = empty($comp) ? 0 : 1;
                    $request[$i]['format_solicitud'] = formatNum($request[$i]['total_solicitud']);
                    $request[$i]['format_cdp'] = formatNum($request[$i]['total_cdp']);
                    $request[$i]['format_rp'] = formatNum($request[$i]['total_rp']);
                }
            }
            return $request;
        }
        public function selectParametrosNomina(){
            $sql = "SELECT * FROM humparametrosliquida";
            $request = $this->select($sql);
            return $request;
        }
        public function selectVariablesPago(){
            $sql = "SELECT * FROM ccpethumvariables WHERE estado = 'S'";
            $request = $this->select_all($sql);
            return $request;
        }
        public function selectCuentaDescuento($codigo){
            $sql = "SELECT DISTINCT T1.nombre,T1.beneficiario,T2.cuenta
                FROM humvariablesretenciones AS T1
                INNER JOIN humvariablesretenciones_det AS T2 ON T1.codigo = T2.codigo
                WHERE T1.codigo = '$codigo' AND T2.credito = 'S' ORDER BY T2.fechainicial LIMIT 1";
            return $this->select($sql);
        }
        public function selectCuentaRetencion($codigo){
            $sql = "SELECT T2.conceptoingreso as codigo FROM tesoretenciones T1, tesoretenciones_det T2 WHERE T1.id = T2.codigo AND T1.id = '$codigo'";
            $request = $this->select($sql)['codigo'];
            return $request;
        }
    }
?>
