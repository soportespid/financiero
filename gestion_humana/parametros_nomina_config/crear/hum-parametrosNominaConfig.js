const URL ='gestion_humana/parametros_nomina_config/crear/hum-parametrosNominaConfig.php';

var app = new Vue({
    el:"#myapp",
    data() {
        return {
            isLoading:false,
            isModal:false,
            arrPago:[],
            arrParafiscales:[],
            arrTerceros:[],
            arrTercerosCopy:[],
            selectSueldo:0,
            selectAlimentacion:0,
            selectTransporte:0,
            selectCaja:0,
            selectIcbf:0,
            selectSena:0,
            selectInstituto:0,
            selectEsap:0,
            selectArl:0,
            selectSaludEmpleador:0,
            selectSaludEmpleado:0,
            selectPensionEmpleado:0,
            selectPensionEmpleador:0,
            selectCesantias:0,
            selectInteresesCesantias:0,
            selectHoras:0,
            txtResultados:0,
            txtBuscar:"",
            objTercero:{nombre:"",documento:""},
        }
    },
    mounted() {
        this.getData();
    },
    methods: {
        getData: async function(){
            const formData = new FormData();
            formData.append("action","get");
            this.isLoading = true;
            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();
            this.arrPago = objData.variables_pago;
            this.arrParafiscales = objData.variables_parafiscales;
            this.arrTerceros = objData.terceros;
            this.arrTercerosCopy = objData.terceros;
            this.txtResultados = this.arrTerceros.length;
            this.arrHoras = objData.horas;
            if(objData.data.tercero != null){
                this.objTercero = objData.data.tercero;
            }
            //Parámetros pago
            this.selectSueldo=objData.data.sueldo == -1 ? 0 : objData.data.sueldo;
            this.selectAlimentacion=objData.data.sub_alimentacion == -1 ? 0 : objData.data.sub_alimentacion;
            this.selectTransporte=objData.data.aux_transporte == -1 ? 0 : objData.data.aux_transporte;
            this.selectHoras=objData.data.horas_mensuales == -1 ? 0 : objData.data.horas_mensuales;
            //Parámetros para fiscales
            this.selectCaja= objData.data.cajacompensacion == -1 ? 0 : objData.data.cajacompensacion;
            this.selectIcbf= objData.data.icbf == -1 ? 0 : objData.data.icbf;
            this.selectSena= objData.data.sena == -1 ? 0 : objData.data.sena;
            this.selectInstituto= objData.data.iti == -1 ? 0 : objData.data.iti;
            this.selectEsap= objData.data.esap == -1 ? 0 : objData.data.esap;
            this.selectArl= objData.data.arp == -1 ? 0 : objData.data.arp;
            this.selectSaludEmpleador= objData.data.salud_empleador == -1 ? 0 : objData.data.salud_empleador;
            this.selectSaludEmpleado= objData.data.salud_empleado == -1 ? 0 : objData.data.salud_empleado;
            this.selectPensionEmpleado= objData.data.pension_empleado == -1 ? 0 : objData.data.pension_empleado;
            this.selectPensionEmpleador= objData.data.pension_empleador == -1 ? 0 : objData.data.pension_empleador;
            this.selectCesantias= objData.data.provi_cesantias == -1 ? 0 : objData.data.provi_cesantias;
            this.selectInteresesCesantias= objData.data.int_cesantiaspara == -1 ? 0 : objData.data.int_cesantiaspara;
            this.isLoading = false;
        },
        search:function(type=""){

            if(type=="documento"){
                let obj = [...this.arrTerceros.filter(e=>e.documento == this.objTercero.documento)][0];
                if(typeof obj != "undefined"){
                    this.objTercero = obj;
                }
            }else{
                let search = this.txtBuscar.toLowerCase();
                this.arrTercerosCopy = [...this.arrTerceros.filter(e=>e.nombre.toLowerCase().includes(search)
                    || e.documento.toLowerCase().includes(search))];
                this.txtResultados = this.arrTercerosCopy.length;
            }
        },
        selectItem:function({...item}){
            this.objTercero = item;
            this.isModal = false;
        },
        save: function(){
            const vueContext = this;
            const data = {
                caja:this.selectCaja,
                icbf:this.selectIcbf,
                sena:this.selectSena,
                instituto:this.selectInstituto,
                esap:this.selectEsap,
                arl:this.selectArl,
                salud_empleador:this.selectSaludEmpleador,
                salud_empleado:this.selectSaludEmpleado,
                pension_empleador:this.selectPensionEmpleador,
                pension_empleado:this.selectPensionEmpleado,
                cesantias:this.selectCesantias,
                interes_cesantias:this.selectInteresesCesantias,
                sueldo:this.selectSueldo,
                alimentacion:this.selectAlimentacion,
                transporte:this.selectTransporte,
                aprueba:this.objTercero.documento,
                horas_mensuales:this.selectHoras,
            }
            Swal.fire({
                title:"¿Estás segur@ de guardar?",
                text:"",
                icon: 'warning',
                showCancelButton:true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText:"Sí, guardar",
                cancelButtonText:"No, cancelar"
            }).then(async function(result){
                if(result.isConfirmed){
                    const formData = new FormData();
                    formData.append("action","save");
                    formData.append("data",JSON.stringify(data));
                    vueContext.isLoading = true;
                    const response = await fetch(URL,{method:"POST",body:formData});
                    const objData = await response.json();
                    if(objData.status){
                        Swal.fire("Guardado",objData.msg,"success");
                    }else{
                        Swal.fire("Error",objData.msg,"error");
                    }
                    vueContext.isLoading = false;
                }
            });
        },
    },
    computed:{

    }
})
