<?php
    require_once '../../../comun.inc';
    require '../../../funciones.inc';
    require '../../../funcionesSP.inc.php';
    /*ini_set('display_errors', '1');
    ini_set('display_startup_errors', '1');
    error_reporting(E_ALL);*/
    session_start();
    //dep($_POST);exit;
    if($_POST){
        $obj = new Plantilla();
        if($_POST['action']=="get"){
            $obj->getData();
        }else if($_POST['action']=="save"){
            $obj->save();
        }
    }

    class Plantilla{
        private $linkbd;
        private $arrData;
        public function __construct() {
            $this->linkbd = conectar_v7();
            $this->linkbd->set_charset("utf8");
        }
        public function save(){
            if(!empty($_SESSION)){
                if(empty($_POST['data'])){
                    $arrResponse = array("status"=>false,"msg"=>"Error de datos");
                }else{
                    $arrData = json_decode($_POST['data'],true);
                    if(!is_array($arrData)){
                        $arrResponse = array("status"=>false,"msg"=>"Todos los campos con (*) son obligatorios.");
                    }else{
                        $request = $this->insertData($arrData);
                        if(is_numeric($request) && $request> 0 ){
                            $arrResponse = array("status"=>true,"msg"=>"Datos guardados.");
                        }else{
                            $arrResponse = array("status"=>false,"msg"=>"Ha ocurrido un error, intente de nuevo.");
                        }
                    }
                }
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
            die();
        }
        public function getData(){
            if(!empty($_SESSION)){
                $arrData = array(
                    "variables_pago"=>$this->selectVariables(),
                    "variables_parafiscales"=>$this->selectParafiscales(),
                    "terceros" =>$this->selectTerceros(),
                    "horas"=>$this->selectHorasMensuales(),
                    "data"=>$this->selectData()
                );
                echo json_encode($arrData,JSON_UNESCAPED_UNICODE);
            }
            die();
        }
        public function insertData(array $data){
            $sqlr = "DELETE FROM humparametrosliquida";
			mysqli_query($this->linkbd,$sqlr);
            $sql = "INSERT INTO humparametrosliquida(sueldo,sub_alimentacion,aux_transporte,cajacompensacion,icbf,sena,iti,esap,arp,
            salud_empleador,salud_empleado,pension_empleado,pension_empleador,provi_cesantias,int_cesantiaspara,aprueba,horas_mensuales)
            VALUES(
                '{$data['sueldo']}',
                '{$data['alimentacion']}',
                '{$data['transporte']}',
                '{$data['caja']}',
                '{$data['icbf']}',
                '{$data['sena']}',
                '{$data['instituto']}',
                '{$data['esap']}',
                '{$data['arl']}',
                '{$data['salud_empleador']}',
                '{$data['salud_empleado']}',
                '{$data['pension_empleado']}',
                '{$data['pension_empleador']}',
                '{$data['cesantias']}',
                '{$data['interes_cesantias']}',
                '{$data['aprueba']}',
                '{$data['horas_mensuales']}'
            )";
            $request = intval(mysqli_query($this->linkbd,$sql));
            return $request;
        }
        public function selectData(){
            $sql = "SELECT * FROM humparametrosliquida";
            $request = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC)[0];
            $sql_tercero="SELECT CONCAT(razonsocial,' ',nombre1,' ',nombre2,' ',apellido1,' ',apellido2) as nombre, cedulanit as documento
            FROM terceros WHERE cedulanit = '{$request['aprueba']}'";
            $request['tercero'] = mysqli_query($this->linkbd,$sql_tercero)->fetch_assoc();
            return $request;
        }
        public function selectHorasMensuales(){
            $sql = "SELECT id as codigo, valor FROM hum_horas_mesuales WHERE estado = 'S'";
            $request = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
            return $request;
        }

        public function selectVariables(){
            $sql = "SELECT codigo, nombre,estado FROM ccpethumvariables WHERE estado = 'S'";
            $request = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
            return $request;
        }
        public function selectParafiscales(){
            $sql = "SELECT codigo, nombre,estado FROM humparafiscalesccpet WHERE estado = 'S'";
            $request = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
            return $request;
        }
        public function selectTerceros(){
            $sql = "SELECT CONCAT(razonsocial,' ',nombre1,' ',nombre2,' ',apellido1,' ',apellido2) as nombre, cedulanit as documento
            FROM terceros WHERE estado = 'S'";
            $request = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
            return $request;
        }
    }
?>
