const URL ='gestion_humana/contratos/controllers/NivelSalarialController.php';
var app = new Vue({
    el:"#myapp",
    data() {
        return {
            isLoading:false,
            intPageVal: "",
            txtConsecutivo:0,
            txtNombre:"",
            arrSearch:[],
            arrSearchCopy:[],
            txtSearch:"",
            txtResultados:0,
            arrConsecutivos:[],
            selectOrden:1,
            txtEstado:"",
            txtValor:0,
        }
    },
    mounted() {
        const intPageVal = this.$refs.pageType.value;
        if(intPageVal == 2){
            this.getEdit();
        }else if(intPageVal == 3){
            this.getSearch();
        }else{
            //this.getData();
        }
    },
    methods: {
        getData:async function(){
            const formData = new FormData();
            formData.append("action","get");
            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();
            console.log(objData);
        },
        getEdit:async function(){
            const codigo = new URLSearchParams(window.location.search).get('id');
            let formData = new FormData();
            formData.append("action","edit");
            formData.append("codigo",codigo);
            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();
            if(objData.status){
                this.txtConsecutivo = objData.data.id_nivel;
                this.txtNombre = objData.data.nombre;
                this.txtValor = objData.data.valor;
                this.arrConsecutivos = objData.consecutivos;
                this.txtEstado = objData.data.estado;
            }else{
                window.location.href='hum-nivelSalarialEditar?id='+objData.consecutivo;
            }
        },
        getSearch:async function(){
            const formData = new FormData();
            formData.append("action","search");
            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();
            this.arrSearch = objData;
            this.arrSearchCopy = objData;
            this.txtResultados = this.arrSearchCopy.length;
        },
        save:async function(){
            if(this.txtValor == "" || this.txtValor <= 0|| this.txtNombre =="" || this.txtCodigo ==""){
                Swal.fire("Error","Todos los campos con (*) son obligatorios","error");
                return false;
            }
            Swal.fire({
                title:"¿Estás segur@ de guardar?",
                text:"",
                icon: 'warning',
                showCancelButton:true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText:"Sí, guardar",
                cancelButtonText:"No, cancelar"
            }).then(async function(result){
                if(result.isConfirmed){
                    const formData = new FormData();
                    formData.append("action","save");
                    formData.append("valor",app.txtValor);
                    formData.append("nombre",app.txtNombre);
                    formData.append("consecutivo",app.txtConsecutivo);
                    const response = await fetch(URL,{method:"POST",body:formData});
                    const objData = await response.json();
                    if(objData.status){
                        Swal.fire("Guardado",objData.msg,"success");
                        if(objData.id && app.txtConsecutivo == 0){
                            setTimeout(function(){
                                window.location.href='hum-nivelSalarialEditar?id='+objData.id;
                            },1500);
                        }else{
                            app.getEdit();
                        }
                    }else{
                        Swal.fire("Error",objData.msg,"error");
                    }
                }
            });
        },
        search:function(){
            let search = this.txtSearch.toLowerCase();
            this.arrSearchCopy = [...this.arrSearch.filter(e=>e.id_nivel.toLowerCase().includes(search) || e.nombre.toLowerCase().includes(search))];
            this.txtResultados = this.arrSearchCopy.length;
        },
        changeStatus:function(item){
            Swal.fire({
                title:"¿Estás segur@ de cambiar el estado?",
                text:"",
                icon: 'warning',
                showCancelButton:true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText:"Sí",
                cancelButtonText:"No, cancelar"
            }).then(async function(result){
                if(result.isConfirmed){
                    const formData = new FormData();
                    formData.append("action","change");
                    formData.append("id",item.id_nivel);
                    formData.append("estado",item.estado =='S' ? 'N' : 'S');
                    const response = await fetch(URL,{method:"POST",body:formData});
                    const objData = await response.json();
                    if(objData.status){
                        let index = app.arrSearch.findIndex(e => e.id == item.id);
                        app.arrSearch[index].estado = item.estado =='S' ? 'N' : 'S';
                        app.arrSearch[index].is_status = app.arrSearch[index].estado =='S' ? 1 : 0;
                        app.search();
                        Swal.fire("Estado actualizado","","success");
                    }else{
                        Swal.fire("Error",objData.msg,"error");
                    }
                }else{
                    app.search();
                }
            });

        },
        nextItem:function(type){
            let id = this.txtConsecutivo;
            let index = this.arrConsecutivos.findIndex(function(e){return e.id == id});
            if(type=="next" && app.arrConsecutivos[++index]){
                id = this.arrConsecutivos[index++].id;
            }else if(type=="prev" && app.arrConsecutivos[--index]){
                id = this.arrConsecutivos[index--].id;
            }
           window.location.href='hum-nivelSalarialEditar?id='+id;
        },
        formatNum: function(valor){
            return new Intl.NumberFormat("en-US", {style: "currency", currency: "USD"}).format(valor);
        },
    },
})
