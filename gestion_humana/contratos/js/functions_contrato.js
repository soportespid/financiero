const URL ='gestion_humana/contratos/controllers/ContratoController.php';
const URLEXPORT ='gestion_humana/contratos/controllers/ContratoExportController.php';
var app = new Vue({
    el:"#myapp",
    data() {
        return {
            isLoading:false,
            isModalRubro:false,
            isModalTercero:false,
            isModalCargo:false,
            isModalFuente:false,
            txtSearchTercero:"",
            txtSearchCargo:"",
            txtSearchProyecto:"",
            txtSearchFuente:"",
            txtResultadosTerceros:0,
            txtResultadosProyectos:0,
            txtResultadosCargos:0,
            txtResultadosFuentes:0,
            txtParaSaludFuncionario:4,
            txtParaSaludEmpleador:8.5,
            txtParaPensionFuncionario:4,
            txtParaPensionEmpleador:12,
            txtParaCcf:4,
            txtParaIcbf:3,
            txtParaSena:0.5,
            txtParaInterCesantias:1,
            txtParaEsap:0.5,
            txtObligacionEmpleador:"",
            txtObligacionEmpleado:"",
            txtClausula:"",
            txtFechaIngreso:new Date().toISOString().split("T")[0],
            txtFechaEps:new Date().toISOString().split("T")[0],
            txtFechaArl:new Date().toISOString().split("T")[0],
            txtFechaAfp:new Date().toISOString().split("T")[0],
            txtFechaCesantias:new Date().toISOString().split("T")[0],
            txtFechaTerminacion:"",
            txtParagrafo :"",
            txtJornadaLvInicio:"08:00",
            txtJornadaLvFinal:"17:30",
            txtJornadaLsInicio:"08:00",
            txtJornadaLsFinal:"13:00",
            txtJornadaLdInicio:"",
            txtJornadaLdFinal:"",
            txtCuentaBancaria:"",
            txtNombreLabor:"",
            selectBancos:"00",
            selectUnidad:"-1",
            selectSeccion:"-1",
            selectCentro:"-1",
            selectJornada:"1",
            selectContrato:1,
            selectPeriodo:"",
            selectRiesgo:"",
            selectVinculacion:"T",
            selectPttoSalario:"F",
            selectPttoPagos:"F",
            selectPagoCesantias:"A",
            selectPttoPara:"F",
            selectSector:0,
            selectPrograma:0,
            selectProducto:0,
            selectSectorData:0,
            selectProgramaData:0,
            selectProductoData:0,
            arrFuentes:[],
            arrFuentesCopy:[],
            arrBpimCopy:[],
            arrBpim:[],
            arrProgramas:[],
            arrSectores:[],
            arrProductos:[],
            arrUnidades:[],
            arrSecciones:[],
            arrSeccionesCopy:[],
            arrCentrosCopy:[],
            arrCentros:[],
            arrBancos:[],
            arrCargos:[],
            arrCargosCopy:[],
            arrTerceros:[],
            arrTercerosCopy:[],
            arrObligaciones:[],
            arrClausulas:[],
            arrPeriodos:[],
            arrRiesgos:[],
            terceroType:1,
            objTercero:{codigo:"",nombre:""},
            objEps:{codigo:"",nombre:""},
            objArl:{codigo:"",nombre:""},
            objAfp:{codigo:"",nombre:""},
            objFondo:{codigo:"",nombre:""},
            objCargo:{codigo:"",nombre:"",valor:0,requisitos:""},
            objFuente:{codigo:"",nombre:""},
            objProyecto:{id:"",sector:"",programa:"",producto:"",indicador:"",bpin:"",nombre:""},
            pdfContrato:"",
            intPageVal: "",
            txtConsecutivo:0,
            txtRutaAnexo:"",
            arrSearch:[],
            arrSearchCopy:[],
            txtSearch:"",
            txtResultados:0,
            isAutomatico:0,
            txtEstado:"",
        }
    },
    mounted() {
        const intPageVal = this.$refs.pageType.value;
        if(intPageVal == 1){
            this.getData();
        }else if(intPageVal == 2){
            this.getEdit();
        }else{
            this.getSearch();
        }
    },
    methods: {
        getData: async function(){
            const formData = new FormData();
            formData.append("action","get");
            this.isLoading = true;
            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();
            await this.getProyectos();
            this.isLoading = false;
            this.arrBancos = objData.bancos;
            this.arrTerceros = objData.terceros;
            this.arrTercerosCopy = objData.terceros;
            this.arrCargos = objData.cargos;
            this.arrCargosCopy = objData.cargos;
            this.arrCentros = objData.centros;
            this.arrSecciones = objData.secciones;
            this.arrUnidades = objData.unidades;
            this.arrPeriodos = objData.periodos;
            this.arrRiesgos = objData.riesgos;
            this.arrRequisitos = objData.requisitos;
            this.selectRiesgo = this.arrRiesgos[0].codigo;
            this.selectPeriodo = this.arrPeriodos[0].codigo;
            this.txtResultadosCargos = this.arrCargosCopy.length;
            this.txtResultadosTerceros = this.arrTercerosCopy.length;
            this.txtResultadosFuentes = this.arrFuentesCopy.length;
            this.arrObligaciones.push(
                {"type":1,"value":"Pagar en la forma pactada el monto equivalente a la remuneración."},
                {"type":1,"value":"Realizar la afiliación y correspondiente aporte a parafiscales."},
                {"type":1,"value":"Dotar al TRABAJADOR de los elementos de trabajo necesarios para el correcto desempeño de la gestión contratada."},
                {"type":1,"value":"Las obligaciones especiales enunciadas en los artículos 56 y 57 del Código Sustantivo del Trabajo."},
                {"type":2,"value":"Cumplir a cabalidad con el objeto del contrato, en la forma convenida."},
                {"type":2,"value":"Las obligaciones especiales enunciadas en los artículos 56 y 58 del Código Sustantivo del Trabajo."},
            )
            this.arrClausulas.push(
                {
                    "clausula":"Terminación unilateral del contrato. El presente contrato se podrá terminar unilateralmente y sin indemnización alguna, por cualquiera de las partes, siempre y cuando se configure algunas de las situaciones previstas en el artículo 62 del Código Sustantivo del Trabajo o haya incumplimiento grave de alguna cláusula prevista en el contrato de trabajo. Se considera incumplimiento grave el desconocimiento de las obligaciones o prohibiciones previstas en el contrato",
                    "paragrafo":"",
                },
                {
                    "clausula":"Este contrato ha sido redactado estrictamente de acuerdo con la ley y la jurisprudencia y será interpretado de buena fe y en consonancia con el Código Sustantivo del Trabajo  cuyo objeto, definido en su artículo 1º, es lograr la justicia en las relaciones entre empleadores y trabajadores dentro de un espíritu de coordinación económica y equilibrio social.",
                    "paragrafo":"",
                },
                {
                    "clausula":"El presente contrato reemplaza en su integridad y deja sin efecto alguno cualquiera otro contrato verbal o escrito celebrado por las partes con anterioridad. Las modificaciones que se acuerden al presente contrato se anotarán a continuación de su texto.",
                    "paragrafo":""
                }
            );
        },
        getProyectos: async function(){
            const formData = new FormData();
            formData.append("action","get");
            this.isLoading = true;
            const response = await fetch('presupuesto_ccpet/procesos_gastos/controllers/RubroController.php',{method:"POST",body:formData});
            const objData = await response.json();
            this.isLoading = false;
            this.arrBpim = objData.bpim;
            this.arrSectores = objData.sectores;
            this.arrSectoresData = objData.sectores;
            this.arrFuentes = objData.fuentes;
            this.arrFuentesCopy = objData.fuentes;
            this.arrBpimCopy = objData.bpim;
        },
        getEdit:async function(){
            const codigo = new URLSearchParams(window.location.search).get('id');
            let formData = new FormData();
            formData.append("action","edit");
            formData.append("codigo",codigo);
            this.isLoading = true;
            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();
            this.isLoading = false;
            if(objData.status){
                await this.getProyectos();
                const data = objData.data;
                this.arrTerceros = objData.terceros;
                this.arrTercerosCopy = objData.terceros;
                this.arrCargos = objData.cargos;
                this.arrCargosCopy = objData.cargos;
                this.arrCentros = objData.centros;
                this.arrSecciones = objData.secciones;
                this.arrUnidades = objData.unidades;
                this.arrPeriodos = objData.periodos;
                this.arrRiesgos = objData.riesgos;
                this.arrRequisitos = objData.requisitos;
                this.txtResultadosCargos = this.arrCargosCopy.length;
                this.txtResultadosTerceros = this.arrTercerosCopy.length;
                this.arrConsecutivos = Array.from(new Set(objData.consecutivos.map(e=>{return JSON.stringify(e)}))).map(e=>{return JSON.parse(e)});
                this.txtConsecutivo = data.id;
                this.txtCuentaBancaria = data.cuenta;
                this.selectContrato = data.tipo;
                this.selectPeriodo = data.periodo;
                this.txtFechaIngreso = data.fecha_inicial;
                this.txtFechaTerminacion = data.fecha_final;
                this.txtRutaAnexo = data.anexo;
                this.arrObligaciones = data.contrato.obligaciones;
                this.arrClausulas = data.contrato.clausulas;
                this.selectPeriodo = data.periodo;
                this.selectBancos = data.banco;
                this.selectJornada = data.jornada.jornada;
                this.objCargo = {codigo:data.cargo_id,nombre:data.nombre_cargo,valor:data.salario,escala:data.escala,nivel_salarial:data.nivel_salarial,requisitos:data.requisitos};
                this.objTercero = {codigo:data.tercero,nombre:data.nombre_tercero,direccion:data.direccion,tipodoc:data.tipodoc,cuentas:data.cuentas};
                this.txtJornadaLvInicio = data.jornada.horario.inicio;
                this.txtJornadaLvFinal = data.jornada.horario.final;
                this.txtJornadaLsInicio = data.jornada.horario.inicio_sabado;
                this.txtJornadaLsFinal = data.jornada.horario.final_sabado;
                this.txtJornadaLdInicio = data.jornada.horario.inicio_domingo;
                this.txtJornadaLdFinal = data.jornada.horario.final_domingo;
                this.isAutomatico = parseInt(data.automatico);
                this.selectVinculacion = data.ptto.vinculacion;
                this.selectPttoSalario = data.ptto.ptto_salario;
                this.selectPttoPagos = data.ptto.ptto_pago;
                this.selectPttoPara = data.ptto.ptto_para;
                this.selectUnidad = data.ptto.unidad_id;
                let proyecto = [...this.arrBpim.filter(e=>e.id == data.ptto.id_bpin && data.ptto.indicador)][0];
                let fuente = [...this.arrFuentes.filter(e=>e.codigo == data.ptto.fuente)][0];
                if(fuente != undefined){
                    this.objFuente = fuente;
                }
                if(proyecto != undefined){
                    this.objProyecto.sector = proyecto.sector;
                    this.objProyecto.programa = proyecto.programa;
                    this.objProyecto.producto = proyecto.producto;
                    this.objProyecto.indicador = proyecto.indicador;
                    this.objProyecto.bpin = proyecto.codigo;
                    this.objProyecto.id = proyecto.id;
                    this.objProyecto.nombre = proyecto.nombre;
                }

                this.changeUnidad();
                this.selectSeccion = data.ptto.seccion_id;
                this.changeUnidad('seccion');
                this.selectCentro = data.ptto.cc_id;
                this.selectPagoCesantias = data.seguridad.cesantias;
                this.selectRiesgo = data.seguridad.riesgo_id;
                this.txtParaPensionFuncionario = parseFloat(data.seguridad.pension);
                this.txtParaSaludFuncionario = parseFloat(data.seguridad.salud);
                this.objEps = JSON.parse(JSON.stringify([...this.arrTerceros.filter(e=>e.codigo == data.seguridad.tercero_eps)][0]));
                this.objArl = JSON.parse(JSON.stringify([...this.arrTerceros.filter(e=>e.codigo == data.seguridad.tercero_arl)][0]));
                this.objAfp = JSON.parse(JSON.stringify([...this.arrTerceros.filter(e=>e.codigo == data.seguridad.tercero_afp)][0]));
                this.objFondo = JSON.parse(JSON.stringify([...this.arrTerceros.filter(e=>e.codigo == data.seguridad.tercero_fondo)][0]));
                this.txtFechaEps = data.seguridad.fecha_eps;
                this.txtFechaArl = data.seguridad.fecha_arl;
                this.txtFechaAfp = data.seguridad.fecha_afp;
                this.txtFechaFondo = data.seguridad.fecha_fondo;
                this.txtEstado = data.estado;
            }else{
                window.location.href='hum-contrato-editar?id='+objData.consecutivo;
            }
        },
        getSearch:async function(){
            const formData = new FormData();
            formData.append("action","search");
            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();
            this.arrSearch = objData;
            this.arrSearchCopy = objData;
            this.txtResultados = this.arrSearchCopy.length;
        },
        save:async function(){
            let arrDescuentosNomina = [];
            if(this.objTercero.codigo == ""){
                Swal.fire("Atención","Debe asignar el tercero.","warning");
                return false;
            }
            if(this.selectVinculacion =="" || this.selectPttoSalario =="" || this.selectPttoPagos == "" || this.selectUnidad == "-1"
                || this.selectSeccion == "-1" || this.selectCentro == "-1" || this.selectPttoPara == ""){
                Swal.fire("Atención","Todos los campos de afectación presupuestal son obligatorios.","warning");
                return false;
            }
            if(this.objAfp.codigo =="" || this.objArl.codigo=="" || this.objEps.codigo=="" || this.objFondo.codigo=="" || this.selectPagoCesantias==""
                || this.selectRiesgo =="" || this.txtParaSaludFuncionario == "" || this.txtParaSaludFuncionario <= 0
                || this.txtParaPensionFuncionario == "" || this.txtParaPensionFuncionario <= 0
            ){
                Swal.fire("Atención","Todos los campos de seguridad social son obligatorios.","warning");
                return false;
            }
            if(this.selectPttoSalario == "I"){
                if(this.objProyecto.id ==""){
                    Swal.fire("Atención","Debe seleccionar el proyecto.","warning");
                    return false;
                }
                if(this.objFuente.codigo ==""){
                    Swal.fire("Atención","Debe seleccionar la fuente .","warning");
                    return false;
                }
            }
            if(this.txtConsecutivo == 0){
                if(this.selectContrato == 2){
                    if(this.txtFechaTerminacion == ""){
                        Swal.fire("Atención","El contrato a término fijo debe tener fecha de terminación.","warning");
                        return false;
                    }
                    const fechaIngreso = new Date(this.txtFechaIngreso);
                    const fechaTerminacion = new Date(this.txtFechaTerminacion);
                    const result = fechaTerminacion.getFullYear() - fechaIngreso.getFullYear();
                    if(result > 3){
                        Swal.fire("Atención","El contrato a término fijo no puede ser por más de 3 años.","warning");
                        return false;
                    }else if(fechaTerminacion < fechaIngreso){
                        Swal.fire("Atención","La fecha de terminación no puede ser menor a la de ingreso.","warning");
                        return false;
                    }
                }
                if(this.txtCuentaBancaria == ""){
                    Swal.fire("Atención","Debe seleccionar la cuenta bancaria.","warning");
                    return false;
                }
                if(this.objCargo.codigo == ""){
                    Swal.fire("Atención","Debe asignar el cargo.","warning");
                    return false;
                }

                if(app.arrObligaciones.length == 0){
                    Swal.fire("Atención","Debe asignar las obligaciones del empleado y empleador.","warning");
                    return false;
                }
            }
            const obj = {
                fecha_ingreso:this.txtFechaIngreso,
                fecha_terminacion:this.txtFechaTerminacion,
                labor:this.txtNombreLabor,
                contrato:this.selectContrato,
                periodo:app.selectPeriodo,
                cuenta_bancaria:{
                    banco: this.selectBancos,
                    numero:this.txtCuentaBancaria
                },
                cargo:this.objCargo,
                tercero:this.objTercero,
                jornada:this.selectJornada,
                horario:{
                    inicio:app.txtJornadaLvInicio,
                    final:app.txtJornadaLvFinal,
                    inicio_sabado:app.txtJornadaLsInicio,
                    final_sabado:app.txtJornadaLsFinal,
                    inicio_domingo:app.txtJornadaLdInicio,
                    final_domingo:app.txtJornadaLdFinal
                },
                obligaciones:this.arrObligaciones.filter(function(e){return e.value != ""}),
                clausulas:this.arrClausulas.filter(function(e){return e.clausula != ""}),
            }

            Swal.fire({
                title:"¿Estás segur@ de guardar?",
                text:"",
                icon: 'warning',
                showCancelButton:true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText:"Sí, guardar",
                cancelButtonText:"No, cancelar"
            }).then(async function(result){
                if(result.isConfirmed){
                    const formData = new FormData();
                    formData.append("action","save");
                    formData.append("data",JSON.stringify(obj));
                    formData.append("pdf",app.pdfContrato);
                    formData.append("consecutivo",app.txtConsecutivo);
                    formData.append("automatico",app.isAutomatico ? 1 : 0);
                    formData.append("descuentos",JSON.stringify(arrDescuentosNomina));
                    formData.append("vinculacion",app.selectVinculacion);
                    formData.append("ptto_salario",app.selectPttoSalario);
                    formData.append("ptto_pagos",app.selectPttoPagos);
                    formData.append("ptto_para",app.selectPttoPara);
                    formData.append("fuente",app.objFuente.codigo);
                    formData.append("id_bpin",app.objProyecto.id);
                    formData.append("indicador",app.objProyecto.indicador);
                    formData.append("ptto_para",app.selectPttoPara);
                    formData.append("unidad",app.selectUnidad);
                    formData.append("seccion",app.selectSeccion);
                    formData.append("centro",app.selectCentro);
                    formData.append("tercero_eps",app.objEps.codigo);
                    formData.append("tercero_arl",app.objArl.codigo);
                    formData.append("tercero_afp",app.objAfp.codigo);
                    formData.append("tercero_fondo",app.objFondo.codigo);
                    formData.append("fecha_eps",app.txtFechaEps);
                    formData.append("fecha_arl",app.txtFechaArl);
                    formData.append("fecha_afp",app.txtFechaAfp);
                    formData.append("fecha_fondo",app.txtFechaCesantias);
                    formData.append("cesantias",app.selectPagoCesantias);
                    formData.append("riesgo",app.selectRiesgo);
                    formData.append("salud",app.txtParaSaludFuncionario);
                    formData.append("pension",app.txtParaPensionFuncionario);
                    app.isLoading = true;
                    const response = await fetch(URL,{method:"POST",body:formData});
                    const objData = await response.json();
                    app.isLoading = false;
                    if(objData.status){
                        Swal.fire("Guardado",objData.msg,"success");
                        if(objData.id && app.txtConsecutivo == 0){
                            setTimeout(function(){
                                window.location.href='hum-contrato-editar?id='+objData.id;
                            },1500);
                        }else{
                            app.getEdit();
                        }
                    }else{
                        Swal.fire("Error",objData.msg,"error");
                    }
                }
            });
        },
        selectTipoPtto:function(){
            if(this.selectPttoSalario == "F"){
                this.selectPttoPara = "F";
                this.selectPttoPagos = "F";
            }
            if(this.selectPttoSalario == "G" || this.selectPttoPara == "G" || this.selectPttoPagos == "G"){
                this.selectPttoPara = "G";
                this.selectPttoPagos = "G";
                this.selectPttoSalario = "G";
            }
        },
        selectItem:function ({...data},type=""){
            if(type =="tercero"){
                if(this.terceroType==1){
                    this.objTercero = data;
                }else if(this.terceroType==2){
                    this.objEps = data;
                }else if(this.terceroType==3){
                    this.objArl = data;
                }else if(this.terceroType==4){
                    this.objAfp = data;
                }else if(this.terceroType==5){
                    this.objFondo = data;
                }
                this.isModalTercero = false;
            }else if(type =="cargo"){
                this.objCargo = data;
                this.isModalCargo = false;
            }else if(type=="proyecto"){
                if(data.fuente!=""){
                    this.objFuente.codigo = data.fuente;
                    this.objFuente.nombre = data.nombre_fuente;
                }
                this.objProyecto.sector = data.sector;
                this.objProyecto.programa = data.programa;
                this.objProyecto.producto = data.producto;
                this.objProyecto.indicador = data.indicador;
                this.objProyecto.bpin = data.codigo;
                this.objProyecto.id = data.id;
                this.objProyecto.nombre = data.nombre;
                this.isModalRubro = false;
            }else if(type=="fuente"){
                this.objFuente = data;
                this.isModalFuente = false;
            }
        },
        addObligacion:function(type=1){
            if((type==2 && this.txtObligacionEmpleado == "") || (type==1 && this.txtObligacionEmpleador == "")){
                Swal.fire("Atención","El campo no puede estar vacio.","warning");
                return false;
            }
            let obligacion = type == 1 ? this.txtObligacionEmpleador : this.txtObligacionEmpleado
            this.arrObligaciones.push({"type":type,"value":obligacion});
            this.txtObligacionEmpleador = type == 1 ? "" : this.txtObligacionEmpleador;
            this.txtObligacionEmpleado = type == 2 ? "" : this.txtObligacionEmpleado;
        },
        delItem:function(index,type=1){
            if(type==1){
                this.arrObligaciones.splice(index,1);
            }else if(type==2){
                this.arrClausulas.splice(index,1);
            }
        },
        addClausula:function(){
            if(this.txtClausula == ""){
                Swal.fire("Atención","El campo no puede estar vacio.","warning");
                return false;
            }
            this.arrClausulas.push({
                "clausula":this.txtClausula,
                "paragrafo":this.txtParagrafo
            });
            this.txtClausula = "";
            this.txtParagrafo = "";
        },
        changeUnidad:function(type="unidad"){
            if(type=="seccion"){
                this.selectCentro = -1;
                this.arrCentrosCopy = [...this.arrCentros.filter(e=>e.seccion == app.selectSeccion)];
            }else{
                this.arrSeccionesCopy = [...this.arrSecciones.filter(e=>e.unidad == app.selectUnidad)];
                this.arrCentrosCopy = [];
                this.selectSeccion = -1;
            }
        },
        search:function(type=""){
            let search = "";
            if(type=="modal_tercero"){
                search = this.txtSearchTercero.toLowerCase();
                this.arrTercerosCopy = [...this.arrTerceros.filter(e=>e.codigo.toLowerCase().includes(search) || e.nombre.toLowerCase().includes(search))];
                this.txtResultadosTerceros = this.arrTercerosCopy.length;
            }else if(type=="modal_cargo"){
                search = this.txtSearchCargo.toLowerCase();
                this.arrCargosCopy = [...this.arrCargos.filter(e=>e.codigo.toLowerCase().includes(search))];
                this.txtResultadosCargos = this.arrCargosCopy.length;
            }else if(type=="modal_proyectos"){
                search = this.txtSearchProyecto.toLowerCase();
                this.arrBpimCopy = [...this.arrBpim.filter(e=>e.codigo.toLowerCase().includes(search) || e.nombre.toLowerCase().includes(search) || e.fuente.toLowerCase().includes(search))];
            }else if(type=="modal_fuentes"){
                search = this.txtSearchFuente.toLowerCase();
                this.arrFuentesCopy = [...this.arrFuentes.filter(e=>e.codigo.toLowerCase().includes(search) || e.nombre.toLowerCase().includes(search))];
                this.txtResultadosFuentes = this.arrFuentesCopy.length;
            }else if(type=="cod_tercero"){
                search = this.objTercero.codigo;
                const data = [...this.arrTerceros.filter(e=>e.codigo == search)];
                this.objTercero = data.length > 0 ? JSON.parse(JSON.stringify(data[0])) : {codigo:"",nombre:""};
            }else if(type=="cod_fuente"){
                search = this.objFuente.codigo;
                const data = [...this.arrFuentes.filter(e=>e.codigo == search)];
                this.objFuente = data.length > 0 ? JSON.parse(JSON.stringify(data[0])) : {codigo:"",nombre:""};
            }else if(type=="search"){
                search = this.txtSearch;
                this.arrSearchCopy = [...this.arrSearch.filter(e=>e.id.toLowerCase().includes(search) || e.nombre_tercero.toLowerCase().includes(search)
                || e.tipo.toLowerCase().includes(search) || e.nombre_periodo.toLowerCase().includes(search)
                || e.tercero.toLowerCase().includes(search))];
                this.txtResultados = this.arrSearchCopy.length;
            }
        },
        filter:function(type=""){
            if(type=="sector" && this.selectSector != 0){
                this.arrProductos = [];
                this.arrBpimCopy = [...this.arrBpim.filter((e)=>{return e.sector==app.selectSector})];
                this.arrProgramas = Array.from(new Set(
                    app.arrBpimCopy.map(e=>{return JSON.stringify({codigo:e.programa,nombre:e.nombre_programa})})
                )).map(item => JSON.parse(item));
            }else if(type=="programa" && this.selectPrograma != 0){
                this.arrBpimCopy = [...this.arrBpim.filter((e)=>{return e.sector==app.selectSector && e.programa==app.selectPrograma})];
                this.arrProductos = Array.from(new Set(
                    app.arrBpimCopy.map(e=>{return JSON.stringify({codigo:e.producto,nombre:e.nombre_producto})})
                )).map(item => JSON.parse(item));
            }else if(type=="producto" && this.selectProducto != 0){
                this.arrBpimCopy = [...this.arrBpim.filter((e)=>{
                    return e.sector==app.selectSector && e.programa==app.selectPrograma && e.producto==app.selectProducto}
                )];
            }else{
                this.arrBpimCopy = [...this.arrBpim];
            }
        },
        changeCuenta:function(){
            this.txtCuentaBancaria = JSON.parse(JSON.stringify([...this.objTercero.cuentas.filter(e=>e.codigo == app.selectBancos)][0].cuenta));
        },
        exportData:function(){
            if(this.selectContrato == 2){
                if(this.txtFechaTerminacion == ""){
                    Swal.fire("Atención","El contrato a término fijo debe tener fecha de terminación.","warning");
                    return false;
                }
                const fechaIngreso = new Date(this.txtFechaIngreso);
                const fechaTerminacion = new Date(this.txtFechaTerminacion);
                const result = fechaTerminacion.getFullYear() - fechaIngreso.getFullYear();
                if(result > 3){
                    Swal.fire("Atención","El contrato a término fijo no puede ser por más de 3 años.","warning");
                    return false;
                }else if(fechaTerminacion < fechaIngreso){
                    Swal.fire("Atención","La fecha de terminación no puede ser menor a la de ingreso.","warning");
                    return false;
                }
            }
            if(this.txtCuentaBancaria == ""){
                Swal.fire("Atención","Debe escribir la cuenta bancaria.","warning");
                return false;
            }
            if(this.objCargo.codigo == ""){
                Swal.fire("Atención","Debe asignar el cargo.","warning");
                return false;
            }
            if(this.objTercero.codigo == ""){
                Swal.fire("Atención","Debe asignar el tercero.","warning");
                return false;
            }
            if(app.arrObligaciones.length == 0){
                Swal.fire("Atención","Debe asignar las obligaciones del empleado y empleador.","warning");
                return false;
            }
            let obj = {
                fecha_ingreso:this.txtFechaIngreso,
                fecha_terminacion:this.txtFechaTerminacion,
                labor:this.txtNombreLabor,
                contrato:this.selectContrato,
                periodo:this.arrPeriodos.filter(e=>{return e.codigo == app.selectPeriodo})[0],
                cuenta_bancaria:{
                    banco: this.arrBancos.filter(e=>{return e.codigo == app.selectBancos})[0],
                    numero:app.txtCuentaBancaria
                },
                cargo:this.objCargo,
                tercero:this.objTercero,
                jornada:this.selectJornada,
                horario:{
                    inicio:this.formatAMPM(app.txtJornadaLvInicio),
                    final:this.formatAMPM(app.txtJornadaLvFinal),
                    inicio_sabado:this.formatAMPM(app.txtJornadaLsInicio),
                    final_sabado:this.formatAMPM(app.txtJornadaLsFinal),
                    inicio_domingo:this.formatAMPM(app.txtJornadaLdInicio),
                    final_domingo:this.formatAMPM(app.txtJornadaLdFinal)
                },
                obligaciones:this.arrObligaciones.filter(function(e){return e.value != ""}),
                clausulas:this.arrClausulas.filter(function(e){return e.clausula != ""}),
            }
            const form = document.createElement("form");
            form.method ="post";
            form.target="_blank";
            form.action=URLEXPORT;

            function addField(name,value){
                const input = document.createElement("input");
                input.type="hidden";
                input.name=name;
                input.value = value;
                form.appendChild(input);
            }
            addField("action","pdf");
            addField("data",JSON.stringify(obj));
            document.body.appendChild(form);
            form.submit();
            document.body.removeChild(form);
        },
        uploadFile:function(element){
            this.pdfContrato = element.event.target.files[0];
        },
        formatNum: function(valor){
            return new Intl.NumberFormat("en-US", {style: "currency", currency: "USD"}).format(valor);
        },
        showTab:function(tab){
            let tabs = this.$refs.rTabs.children;
            let tabsContent = this.$refs.rTabsContent.children;
            for (let i = 0; i < tabs.length; i++) {
                tabs[i].classList.remove("active");
                tabsContent[i].classList.remove("active")
            }
            tabs[tab-1].classList.add("active");
            tabsContent[tab-1].classList.add("active")
        },
        formatAMPM:function(date) {
            const arrDate = date.split(":");
            let hours = parseInt(arrDate[0]);
            let minutes = parseInt(arrDate[1]);
            let ampm = hours >= 12 ? 'pm' : 'am';
            hours = hours % 12;
            hours = hours ? hours : 12; // the hour '0' should be '12'
            minutes = minutes < 10 ? '0'+minutes : minutes;
            let strTime = hours + ':' + minutes + ' ' + ampm;
            return strTime;
        },
        nextItem:function(type){
            let id = this.txtConsecutivo;
            let index = this.arrConsecutivos.findIndex(function(e){return e.id == id});
            if(type=="next" && app.arrConsecutivos[++index]){
                id = this.arrConsecutivos[index++].id;
            }else if(type=="prev" && app.arrConsecutivos[--index]){
                id = this.arrConsecutivos[index--].id;
            }
           window.location.href='hum-contrato-editar?id='+id;
        },
    },
})
