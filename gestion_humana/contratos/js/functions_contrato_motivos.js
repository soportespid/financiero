const URL ='gestion_humana/contratos/controllers/ContratoMotivosController.php';
var app = new Vue({
    el:"#myapp",
    data() {
        return {
            isLoading:false,
            intPageVal: "",
            txtConsecutivo:0,
            txtMotivo:"",
            arrSearch:[],
            arrSearchCopy:[],
            txtSearch:"",
            txtResultados:0,
            arrConsecutivos:[]
        }
    },
    mounted() {
        const intPageVal = this.$refs.pageType.value;
        if(intPageVal == 2){
            this.getEdit();
        }else if(intPageVal == 3){
            this.getSearch();
        }
    },
    methods: {
        getEdit:async function(){
            const codigo = new URLSearchParams(window.location.search).get('id');
            let formData = new FormData();
            formData.append("action","edit");
            formData.append("codigo",codigo);
            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();
            if(objData.status){
                this.txtConsecutivo = objData.data.id;
                this.txtMotivo = objData.data.motivo;
                this.arrConsecutivos = objData.consecutivos
            }else{
                window.location.href='hum-contrato-motivos-editar?id='+objData.consecutivo;
            }
        },
        getSearch:async function(){
            const formData = new FormData();
            formData.append("action","search");
            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();
            this.arrSearch = objData;
            this.arrSearchCopy = objData;
            this.txtResultados = this.arrSearchCopy.length;
        },
        save:async function(){
            if(this.txtMotivo==""){
                Swal.fire("Atención!","El campo no puede estar vacio.","warning");
                return false;
            }

            Swal.fire({
                title:"¿Estás segur@ de guardar?",
                text:"",
                icon: 'warning',
                showCancelButton:true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText:"Sí, guardar",
                cancelButtonText:"No, cancelar"
            }).then(async function(result){
                if(result.isConfirmed){
                    const formData = new FormData();
                    formData.append("action","save");
                    formData.append("motivo",app.txtMotivo);
                    formData.append("consecutivo",app.txtConsecutivo);
                    const response = await fetch(URL,{method:"POST",body:formData});
                    const objData = await response.json();
                    if(objData.status){
                        Swal.fire("Guardado",objData.msg,"success");
                        if(objData.id && app.txtConsecutivo == 0){
                            setTimeout(function(){
                                window.location.href='hum-contrato-motivos-editar?id='+objData.id;
                            },1500);
                        }else{
                            app.getEdit();
                        }
                    }else{
                        Swal.fire("Error",objData.msg,"error");
                    }
                }
            });
        },
        search:function(){
            let search = this.txtSearch.toLowerCase();
            this.arrSearchCopy = [...this.arrSearch.filter(e=>e.id.toLowerCase().includes(search) || e.motivo.toLowerCase().includes(search))];
            this.txtResultados = this.arrSearchCopy.length;
        },
        nextItem:function(type){
            let id = this.txtConsecutivo;
            let index = this.arrConsecutivos.findIndex(function(e){return e.id == id});
            if(type=="next" && app.arrConsecutivos[++index]){
                id = this.arrConsecutivos[index++].id;
            }else if(type=="prev" && app.arrConsecutivos[--index]){
                id = this.arrConsecutivos[index--].id;
            }
           window.location.href='hum-contrato-motivos-editar?id='+id;
        },
    },
})
