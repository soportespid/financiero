const URL ='gestion_humana/contratos/controllers/ContratoNovedadController.php';
const URLEXPORT ='gestion_humana/contratos/controllers/ContratoExportController.php';
var app = new Vue({
    el:"#myapp",
    data() {
        return {
            isLoading:false,
            isModalContrato:false,
            isModalTercero:false,
            isModalCargo:false,
            txtSearchTercero:"",
            txtSearchCargo:"",
            txtResultadosTerceros:0,
            txtResultadosCargos:0,
            txtObligacionEmpleador:"",
            txtObligacionEmpleado:"",
            txtClausula:"",
            txtFechaIngreso:new Date(new Date().setDate(1)).toISOString().split("T")[0],
            txtFechaTerminacion:"",
            txtParagrafo :"",
            txtJornadaLvInicio:"08:00",
            txtJornadaLvFinal:"17:30",
            txtJornadaLsInicio:"08:00",
            txtJornadaLsFinal:"13:00",
            txtJornadaLdInicio:"",
            txtJornadaLdFinal:"",
            txtCuentaBancaria:"",
            txtNombreLabor:"",
            selectBancos:"00",
            selectJornada:"1",
            selectContrato:1,
            selectPeriodo:"",
            arrBancos:[],
            arrCargos:[],
            arrCargosCopy:[],
            arrTerceros:[],
            arrTercerosCopy:[],
            arrObligaciones:[],
            arrClausulas:[],
            arrPeriodos:[],
            terceroType:1,
            objTercero:{codigo:"",nombre:""},
            objCargo:{codigo:"",nombre:"",valor:0,requisitos:[]},
            pdfContrato:"",
            txtRutaAnexo:"",
            intPageVal: "",
            txtConsecutivo:0,
            txtSearch:"",
            txtSearchContrato:"",
            txtResultadosContratos:0,
            txtResultados:0,
            arrConsecutivos:[],
            arrSearch:[],
            arrSearchCopy:[],
            arrMotivos:[],
            arrContratos:[],
            arrContratosCopy:[],
            selectRetiro:0,
            selectNovedad:0,
            objContrato:{id:""},
            pdfRetiro:"",
        }
    },
    mounted() {
        const intPageVal = this.$refs.pageType.value;
        if(intPageVal == 1){
            this.getData();
        }else if(intPageVal == 2){
            this.getEdit();
        }else if(intPageVal == 3){
            this.getSearch();
        }
    },
    methods: {
        getData:async function(){
            let formData = new FormData();
            formData.append("action","get");
            this.isLoading = true;
            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();
            this.isLoading = false;
            this.arrMotivos = objData.motivos;
            this.arrContratos = objData.contratos;
            this.arrContratosCopy = objData.contratos;
            this.arrTerceros = objData.terceros;
            this.arrTercerosCopy = objData.terceros;
            this.arrCargos = objData.cargos;
            this.arrCargosCopy = objData.cargos;
            this.arrPeriodos = objData.periodos;
            this.selectPeriodo = this.arrPeriodos[0].codigo;
            this.txtResultadosCargos = this.arrCargosCopy.length;
            this.txtResultadosTerceros = this.arrTercerosCopy.length;
            this.txtResultadosContratos = objData.contratos.length;

        },
        getEdit:async function(){
            const codigo = new URLSearchParams(window.location.search).get('id');
            let formData = new FormData();
            formData.append("action","edit");
            formData.append("codigo",codigo);
            this.isLoading = true;
            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();
            this.isLoading = false;
            if(objData.status){
                this.arrMotivos = objData.motivos;
                this.arrTerceros = objData.terceros;
                this.arrCargos = objData.cargos;
                this.arrCargosCopy = objData.cargos;
                this.arrPeriodos = objData.periodos;
                this.txtConsecutivo = objData.data.id;
                this.selectNovedad = objData.data.tipo;
                this.selectRetiro = objData.data.motivo_id;
                this.arrConsecutivos = objData.consecutivos;
                this.objContrato.id = objData.data.contrato_id;
                if(this.selectNovedad == 1){
                    const data = objData.data.detalle;
                    this.objContrato = data;
                    this.arrConsecutivos = Array.from(new Set(objData.consecutivos.map(e=>{return JSON.stringify(e)}))).map(e=>{return JSON.parse(e)});
                    this.txtCuentaBancaria = data.cuenta;
                    this.selectContrato = data.tipo;
                    this.selectPeriodo = data.periodo;
                    this.txtFechaIngreso = data.fecha_inicial;
                    this.txtFechaTerminacion = data.fecha_final;
                    this.txtRutaAnexo = data.anexo;
                    this.arrObligaciones = data.contrato.obligaciones;
                    this.arrClausulas = data.contrato.clausulas;
                    this.selectPeriodo = data.periodo;
                    this.selectBancos = data.banco;
                    this.selectJornada = data.jornada.jornada;
                    this.objCargo = {codigo:data.cargo_id,nombre:data.nombre_cargo,valor:data.salario,escala:data.escala,nivel_salarial:data.nivel_salarial,requisitos:data.requisitos};
                    this.objTercero = {codigo:data.tercero,nombre:data.nombre_tercero,direccion:data.direccion,tipodoc:data.tipodoc,cuentas:data.cuentas};
                    this.txtJornadaLvInicio = data.jornada.horario.inicio;
                    this.txtJornadaLvFinal = data.jornada.horario.final;
                    this.txtJornadaLsInicio = data.jornada.horario.inicio_sabado;
                    this.txtJornadaLsFinal = data.jornada.horario.final_sabado;
                    this.txtJornadaLdInicio = data.jornada.horario.inicio_domingo;
                    this.txtJornadaLdFinal = data.jornada.horario.final_domingo;
                }
            }else{
                window.location.href='hum-contrato-novedad-editar?id='+objData.consecutivo;
            }
        },
        getSearch:async function(){
            const formData = new FormData();
            formData.append("action","search");
            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();
            this.arrSearch = objData;
            this.arrSearchCopy = objData;
            this.txtResultados = this.arrSearchCopy.length;
        },
        getTipoContratos:async function(){
            if(this.selectNovedad == 1){
                this.arrContratosCopy = [...this.arrContratos.filter(function(e){return e.estado =="TERMINADO";})];
            }else if(this.selectNovedad == 2){
                this.arrContratosCopy = [...this.arrContratos.filter(function(e){return e.estado !="TERMINADO";})];
            }
            this.objContrato = {id:""}
            this.txtResultadosContratos = this.arrContratosCopy.length
        },
        save:async function(){
            let obj = {}
            if(this.txtConsecutivo == 0){
                if(this.selectNovedad == 0){
                    Swal.fire("Atención!","Debe seleccionar el tipo de novedad.","warning");
                    return false;
                }
                if(this.objContrato.id == ""){
                    Swal.fire("Atención!","Debe seleccionar el contrato.","warning");
                    return false;
                }
                if(this.selectNovedad == 2){
                    if(this.selectRetiro==0){
                        Swal.fire("Atención!","Debe seleccionar el motivo de retiro.","warning");
                        return false;
                    }
                }else if(this.selectNovedad == 1){
                    if(this.selectContrato == 2){
                        if(this.txtFechaTerminacion == ""){
                            Swal.fire("Atención","El contrato a término fijo debe tener fecha de terminación.","warning");
                            return false;
                        }
                        const fechaIngreso = new Date(this.txtFechaIngreso);
                        const fechaTerminacion = new Date(this.txtFechaTerminacion);
                        const result = fechaTerminacion.getFullYear() - fechaIngreso.getFullYear();
                        if(result > 3){
                            Swal.fire("Atención","El contrato a término fijo no puede ser por más de 3 años.","warning");
                            return false;
                        }else if(fechaTerminacion < fechaIngreso){
                            Swal.fire("Atención","La fecha de terminación no puede ser menor a la de ingreso.","warning");
                            return false;
                        }
                    }
                    if(this.txtCuentaBancaria == ""){
                        Swal.fire("Atención","Debe escribir la cuenta bancaria.","warning");
                        return false;
                    }
                    if(this.objCargo.codigo == ""){
                        Swal.fire("Atención","Debe asignar el cargo.","warning");
                        return false;
                    }
                    if(this.objTercero.codigo == ""){
                        Swal.fire("Atención","Debe asignar el tercero.","warning");
                        return false;
                    }
                    if(app.arrObligaciones.length == 0){
                        Swal.fire("Atención","Debe asignar las obligaciones del empleado y empleador.","warning");
                        return false;
                    }
                    obj = {
                        fecha_ingreso:this.txtFechaIngreso,
                        fecha_terminacion:this.txtFechaTerminacion,
                        labor:this.txtNombreLabor,
                        contrato:this.selectContrato,
                        periodo:app.selectPeriodo,
                        cuenta_bancaria:{
                            banco: app.selectBancos,
                            numero:app.txtCuentaBancaria
                        },
                        cargo:this.objCargo,
                        tercero:this.objTercero,
                        jornada:this.selectJornada,
                        horario:{
                            inicio:app.txtJornadaLvInicio,
                            final:app.txtJornadaLvFinal,
                            inicio_sabado:app.txtJornadaLsInicio,
                            final_sabado:app.txtJornadaLsFinal,
                            inicio_domingo:app.txtJornadaLdInicio,
                            final_domingo:app.txtJornadaLdFinal
                        },
                        obligaciones:this.arrObligaciones.filter(function(e){return e.value != ""}),
                        clausulas:this.arrClausulas.filter(function(e){return e.clausula != ""}),
                    }
                }
            }else{
                if(app.pdfContrato ==""){
                    Swal.fire("Atención","Debe seleccionar el archivo antes de guardar.","warning");
                    return false;
                }
            }

            Swal.fire({
                title:"¿Estás segur@ de guardar?",
                text:"",
                icon: 'warning',
                showCancelButton:true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText:"Sí, guardar",
                cancelButtonText:"No, cancelar"
            }).then(async function(result){
                if(result.isConfirmed){
                    const formData = new FormData();
                    formData.append("action","save");
                    formData.append("motivo",app.selectRetiro);
                    formData.append("novedad",app.selectNovedad);
                    formData.append("consecutivo",app.txtConsecutivo);
                    formData.append("soporte",app.pdfRetiro);
                    formData.append("pdf",app.pdfContrato);
                    formData.append("data",JSON.stringify(obj));
                    formData.append("contrato",JSON.stringify(app.objContrato));
                    app.isLoading = true;
                    const response = await fetch(URL,{method:"POST",body:formData});
                    const objData = await response.json();
                    app.isLoading = false;
                    if(objData.status){
                        Swal.fire("Guardado",objData.msg,"success");
                        if(objData.id && app.txtConsecutivo == 0){
                            setTimeout(function(){
                                window.location.href='hum-contrato-novedad-editar?id='+objData.id;
                            },1500);
                        }else{
                            app.getEdit();
                        }
                    }else{
                        Swal.fire("Error",objData.msg,"error");
                    }
                }
            });
        },
        selectItem:function ({...data},type=""){
            if(type =="tercero"){
                this.objTercero = data;
                this.isModalTercero = false;
            }else if(type =="cargo"){
                this.objCargo = data;
                this.isModalCargo = false;
            }else if(type =="contrato"){
                this.objContrato = data;
                this.txtCuentaBancaria = data.cuenta;
                this.selectContrato = data.tipo;
                this.selectPeriodo = data.periodo;
                this.txtFechaIngreso = data.fecha_inicial;
                this.txtFechaTerminacion = data.fecha_final;
                this.txtRutaAnexo = data.anexo;
                this.arrObligaciones = data.contrato.obligaciones;
                this.arrClausulas = data.contrato.clausulas;
                this.selectPeriodo = data.periodo;
                this.selectBancos = data.banco;
                this.selectJornada = data.jornada.jornada;
                this.objCargo = {codigo:data.cargo_id,nombre:data.nombre_cargo,valor:data.salario,escala:data.escala,nivel_salarial:data.nivel_salarial,requisitos:data.requisitos};
                this.objTercero = {codigo:data.tercero,nombre:data.nombre_tercero,direccion:data.direccion,tipodoc:data.tipodoc,cuentas:data.cuentas};
                this.txtJornadaLvInicio = data.jornada.horario.inicio;
                this.txtJornadaLvFinal = data.jornada.horario.final;
                this.txtJornadaLsInicio = data.jornada.horario.inicio_sabado;
                this.txtJornadaLsFinal = data.jornada.horario.final_sabado;
                this.txtJornadaLdInicio = data.jornada.horario.inicio_domingo;
                this.txtJornadaLdFinal = data.jornada.horario.final_domingo;
                this.isModalContrato = false;
            }
        },
        search:function(type=""){
            let search = "";
            if(type == "modal_tercero")search = this.txtSearchTercero.toLowerCase();
            if(type == "modal_cargo")search = this.txtSearchCargo.toLowerCase();
            if(type == "modal_contrato")search = this.txtSearchContrato.toLowerCase();
            if(type=="cod_tercero")search = this.objTercero.codigo;
            if(type=="cod_contrato")search = this.objContrato.id;
            if(type=="search")search = this.txtSearch;

            if(type=="modal_tercero"){
                this.arrTercerosCopy = [...this.arrTerceros.filter(e=>e.codigo.toLowerCase().includes(search) || e.nombre.toLowerCase().includes(search))];
                this.txtResultadosTerceros = this.arrTercerosCopy.length;
            }else if(type=="modal_cargo"){
                this.arrCargosCopy = [...this.arrCargos.filter(e=>e.codigo.toLowerCase().includes(search) || e.nombre.toLowerCase().includes(search))];
                this.txtResultadosCargos = this.arrCargosCopy.length;
            }else if(type=="modal_contrato"){
                this.arrContratosCopy = [...this.arrContratos.filter(
                    e=>(e.estado == "TERMINADO"  && app.selectNovedad == 1  && (e.id.toLowerCase().includes(search) || e.nombre_tercero.toLowerCase().includes(search)))
                    || (e.estado != "TERMINADO"  && app.selectNovedad == 2  && (e.id.toLowerCase().includes(search) || e.nombre_tercero.toLowerCase().includes(search)))
                )];
                this.txtResultadosContratos = this.arrContratosCopy.length;
            }else if(type=="cod_tercero"){
                const data = [...this.arrTerceros.filter(e=>e.codigo == search)];
                this.objTercero = data.length > 0 ? JSON.parse(JSON.stringify(data[0])) : {codigo:"",nombre:""};
            }else if(type=="cod_contrato"){
                let data = [...this.arrContratos.filter(e=>(e.id == search && app.selectNovedad == 1 && e.estado == "TERMINADO")
                    || (e.id == search && app.selectNovedad == 2 && e.estado != "TERMINADO"))];
                this.objContrato = {id:""};
                if(data.length > 0){
                    data = JSON.parse(JSON.stringify(data[0]));
                    this.objContrato = data;
                    this.txtCuentaBancaria = data.cuenta;
                    this.selectContrato = data.tipo;
                    this.selectPeriodo = data.periodo;
                    this.txtFechaIngreso = data.fecha_inicial;
                    this.txtFechaTerminacion = data.fecha_final;
                    this.txtRutaAnexo = data.anexo;
                    this.arrObligaciones = data.contrato.obligaciones;
                    this.arrClausulas = data.contrato.clausulas;
                    this.selectPeriodo = data.periodo;
                    this.selectBancos = data.banco;
                    this.selectJornada = data.jornada.jornada;
                    this.objCargo = {codigo:data.cargo_id,nombre:data.nombre_cargo,valor:data.salario,escala:data.escala,nivel_salarial:data.nivel_salarial};
                    this.objTercero = {codigo:data.tercero,nombre:data.nombre_tercero,direccion:data.direccion,tipodoc:data.tipodoc,cuentas:data.cuentas};
                    this.txtJornadaLvInicio = data.jornada.horario.inicio;
                    this.txtJornadaLvFinal = data.jornada.horario.final;
                    this.txtJornadaLsInicio = data.jornada.horario.inicio_sabado;
                    this.txtJornadaLsFinal = data.jornada.horario.final_sabado;
                    this.txtJornadaLdInicio = data.jornada.horario.inicio_domingo;
                    this.txtJornadaLdFinal = data.jornada.horario.final_domingo;
                }
            }else if(type=="search"){
                this.arrSearchCopy = [...this.arrSearch.filter(e=>e.id.toLowerCase().includes(search) || e.contrato_id.toLowerCase().includes(search)
                ||  e.novedad.toLowerCase().includes(search))];
                this.txtResultados = this.arrSearchCopy.length;
            }
        },
        addObligacion:function(type=1){
            if((type==2 && this.txtObligacionEmpleado == "") || (type==1 && this.txtObligacionEmpleador == "")){
                Swal.fire("Atención","El campo no puede estar vacio.","warning");
                return false;
            }
            let obligacion = type == 1 ? this.txtObligacionEmpleador : this.txtObligacionEmpleado
            this.arrObligaciones.push({"type":type,"value":obligacion});
            this.txtObligacionEmpleador = type == 1 ? "" : this.txtObligacionEmpleador;
            this.txtObligacionEmpleado = type == 2 ? "" : this.txtObligacionEmpleado;
        },
        delItem:function(index,type=1){
            if(type==1){
                this.arrObligaciones.splice(index,1);
            }else if(type==2){
                this.arrClausulas.splice(index,1);
            }
        },
        addClausula:function(){
            if(this.txtClausula == ""){
                Swal.fire("Atención","El campo no puede estar vacio.","warning");
                return false;
            }
            this.arrClausulas.push({
                "clausula":this.txtClausula,
                "paragrafo":this.txtParagrafo
            });
            this.txtClausula = "";
            this.txtParagrafo = "";
        },
        exportData:function(){
            if(this.selectContrato == 2){
                if(this.txtFechaTerminacion == ""){
                    Swal.fire("Atención","El contrato a término fijo debe tener fecha de terminación.","warning");
                    return false;
                }
                const fechaIngreso = new Date(this.txtFechaIngreso);
                const fechaTerminacion = new Date(this.txtFechaTerminacion);
                const result = fechaTerminacion.getFullYear() - fechaIngreso.getFullYear();
                if(result > 3){
                    Swal.fire("Atención","El contrato a término fijo no puede ser por más de 3 años.","warning");
                    return false;
                }else if(fechaTerminacion < fechaIngreso){
                    Swal.fire("Atención","La fecha de terminación no puede ser menor a la de ingreso.","warning");
                    return false;
                }
            }
            if(this.txtCuentaBancaria == ""){
                Swal.fire("Atención","Debe escribir la cuenta bancaria.","warning");
                return false;
            }
            if(this.objCargo.codigo == ""){
                Swal.fire("Atención","Debe asignar el cargo.","warning");
                return false;
            }
            if(this.objTercero.codigo == ""){
                Swal.fire("Atención","Debe asignar el tercero.","warning");
                return false;
            }
            if(app.arrObligaciones.length == 0){
                Swal.fire("Atención","Debe asignar las obligaciones del empleado y empleador.","warning");
                return false;
            }
            let obj = {
                fecha_ingreso:this.txtFechaIngreso,
                fecha_terminacion:this.txtFechaTerminacion,
                labor:this.txtNombreLabor,
                contrato:this.selectContrato,
                periodo:this.arrPeriodos.filter(e=>{return e.codigo == app.selectPeriodo})[0],
                cuenta_bancaria:{
                    banco: app.objTercero.cuentas.filter(e=>{return e.codigo == app.selectBancos})[0],
                    numero:app.txtCuentaBancaria
                },
                cargo:this.objCargo,
                tercero:this.objTercero,
                jornada:this.selectJornada,
                horario:{
                    inicio:app.txtJornadaLvInicio,
                    final:app.txtJornadaLvFinal,
                    inicio_sabado:app.txtJornadaLsInicio,
                    final_sabado:app.txtJornadaLsFinal,
                    inicio_domingo:app.txtJornadaLdInicio,
                    final_domingo:app.txtJornadaLdFinal
                },
                obligaciones:this.arrObligaciones.filter(function(e){return e.value != ""}),
                clausulas:this.arrClausulas.filter(function(e){return e.clausula != ""}),
            }
            const form = document.createElement("form");
            form.method ="post";
            form.target="_blank";
            form.action=URLEXPORT;

            function addField(name,value){
                const input = document.createElement("input");
                input.type="hidden";
                input.name=name;
                input.value = value;
                form.appendChild(input);
            }
            addField("action","pdf");
            addField("data",JSON.stringify(obj));
            document.body.appendChild(form);
            form.submit();
            document.body.removeChild(form);
        },
        changeCuenta:function(){
            this.txtCuentaBancaria = JSON.parse(JSON.stringify([...this.objTercero.cuentas.filter(e=>e.codigo == app.selectBancos)][0].cuenta));
        },
        uploadFile:function(element){
            this.pdfContrato = element.event.target.files[0];
        },
        formatNum: function(valor){
            return new Intl.NumberFormat("en-US", {style: "currency", currency: "USD"}).format(valor);
        },
        showTab:function(tab){
            let tabs = this.$refs.rTabs.children;
            let tabsContent = this.$refs.rTabsContent.children;
            for (let i = 0; i < tabs.length; i++) {
                tabs[i].classList.remove("active");
                tabsContent[i].classList.remove("active")
            }
            tabs[tab-1].classList.add("active");
            tabsContent[tab-1].classList.add("active")
        },
        formatAMPM:function(date) {
            const arrDate = date.split(":");
            let hours = parseInt(arrDate[0]);
            let minutes = parseInt(arrDate[1]);
            let ampm = hours >= 12 ? 'pm' : 'am';
            hours = hours % 12;
            hours = hours ? hours : 12; // the hour '0' should be '12'
            minutes = minutes < 10 ? '0'+minutes : minutes;
            let strTime = hours + ':' + minutes + ' ' + ampm;
            return strTime;
        },
        nextItem:function(type){
            let id = this.txtConsecutivo;
            let index = this.arrConsecutivos.findIndex(function(e){return e.id == id});
            if(type=="next" && app.arrConsecutivos[++index]){
                id = this.arrConsecutivos[index++].id;
            }else if(type=="prev" && app.arrConsecutivos[--index]){
                id = this.arrConsecutivos[index--].id;
            }
           window.location.href='hum-contrato-novedad-editar?id='+id;
        },
    },
})
