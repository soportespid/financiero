<?php
    require_once '../../../Librerias/core/Helpers.php';
    require_once '../../../PHPExcel/Classes/PHPExcel.php';
    require_once '../models/ContratoModel.php';
    session_start();

    class ContratoController extends ContratoModel{
        public function initialData(){
            if(!empty($_SESSION)){
                $arrResponse = array(
                    "cargos"=>$this->selectCargos(),
                    "terceros"=>getTerceros(),
                    "unidades"=>$this->selectUnidades(),
                    "secciones"=>$this->selecSecciones(),
                    "periodos"=>$this->selectPeriodos(),
                    "centros"=>$this->selectCentros(),
                    "riesgos"=>$this->selectRiesgos(),
                );
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
            die();
        }
        public function save(){
            if(!empty($_SESSION)){
                $arrData = json_decode($_POST['data'],true);
                if(!empty($arrData)){
                    $intConsecutivo = intval($_POST['consecutivo']);
                    $arrPeriodo = $arrData['periodo'];
                    $arrCuenta = $arrData['cuenta_bancaria'];
                    $arrCargo = $arrData['cargo'];
                    $arrTercero = $arrData['tercero'];
                    $strJornada = json_encode(array("jornada"=>$arrData['jornada'],"horario"=>$arrData['horario']),JSON_UNESCAPED_UNICODE);
                    $strContrato = json_encode(array("obligaciones"=>$arrData['obligaciones'],"clausulas"=>$arrData['clausulas']),JSON_UNESCAPED_UNICODE);
                    if($intConsecutivo == 0 && ($arrTercero['codigo'] == "" || $arrCargo['codigo']=="" || $arrCuenta['numero'] == "" || empty($_POST['vinculacion'])
                    || empty($_POST['ptto_salario']) || empty($_POST['ptto_para']) || empty($_POST['ptto_pagos']) || empty($_POST['unidad']) || empty($_POST['seccion'])
                    || empty($_POST['centro']) || empty($_POST['tercero_eps']) || empty($_POST['tercero_arl']) || empty($_POST['tercero_afp'])
                    || empty($_POST['tercero_fondo']) || empty($_POST['fecha_eps']) || empty($_POST['fecha_arl']) || empty($_POST['fecha_afp'])
                    || empty($_POST['fecha_fondo']) || empty($_POST['cesantias']) || empty($_POST['riesgo']) || empty($_POST['salud']) || empty($_POST['pension']))){
                        $arrResponse = array("status"=>false,"msg"=>"Error de datos");
                    }else if($intConsecutivo != 0 && (empty($_POST['vinculacion']) || empty($_POST['unidad']) || empty($_POST['seccion'])
                    || empty($_POST['centro']) || empty($_POST['cesantias']) || empty($_POST['riesgo']) || empty($_POST['salud']) || empty($_POST['pension'])
                    || empty($_POST['ptto_para']))){
                        $arrResponse = array("status"=>false,"msg"=>"Error de datos");

                    }else{
                        $strTercero = strClean($arrTercero['codigo']);
                        $intTipoContrato = intval($arrData['contrato']);
                        $intPeriodoId = intval($arrPeriodo['codigo']);
                        $strFechaInicio = $arrData['fecha_ingreso'];
                        $strFechaFinal = $arrData['fecha_terminacion'];
                        $strBancoId = $arrCuenta['banco'];
                        $strCuentaBanco = $arrCuenta['numero'];
                        $intCargoId = $arrCargo['codigo'];
                        $intAutomatico = intval($_POST['automatico']);
                        $strVinculacion = strClean($_POST['vinculacion']);
                        $strPttoSalarios = strClean($_POST['ptto_salario']);
                        $strPttoPagos = strClean($_POST['ptto_pagos']);
                        $strPttoPara = strClean($_POST['ptto_para']);
                        $intUnidad = intval($_POST['unidad']);
                        $intSeccion = intval($_POST['seccion']);
                        $intIdbpin = intval($_POST['id_bpin']);
                        $strIndicador = strClean($_POST['indicador']);
                        $strFuente = strClean($_POST['fuente']);
                        $strCentro = strClean($_POST['centro']);
                        $strTerceroEps = strClean($_POST['tercero_eps']);
                        $strTerceroArl = strClean($_POST['tercero_arl']);
                        $strTerceroAfp = strClean($_POST['tercero_afp']);
                        $strTerceroFondo = strClean($_POST['tercero_fondo']);
                        $strFechaEps = strClean($_POST['fecha_eps']);
                        $strFechaArl = strClean($_POST['fecha_arl']);
                        $strFechaAfp = strClean($_POST['fecha_afp']);
                        $strFechaFondo = strClean($_POST['fecha_fondo']);
                        $intCesantias = strClean($_POST['cesantias']);
                        $intRiesgo = intval($_POST['riesgo']);
                        $floatSalud = floatval($_POST['salud']);
                        $floatPension = floatval($_POST['pension']);
                        $id = 0;
                        if($intConsecutivo == 0){
                            $request = $this->insertContrato(
                                $strTercero,
                                $intTipoContrato,
                                $intPeriodoId,
                                $strFechaInicio,
                                $strFechaFinal,
                                $strBancoId,
                                $strCuentaBanco,
                                $intCargoId,
                                $strJornada,
                                $strContrato,
                                $intAutomatico,
                                $arrCargo['requisitos'],
                            );
                            $id = $request;
                        }else{
                            if($_FILES['pdf']){
                                $ruta = 'contrato_'.$intConsecutivo.'_'.bin2hex(random_bytes(6)).'.pdf';
                                uploadFile($_FILES['pdf'],$ruta);
                                $request = $this->updateRuta($intConsecutivo,$ruta);
                            }
                            $request = $this->updateContrato($intConsecutivo,$arrTercero['codigo'],$intAutomatico,$arrCargo['requisitos'],$intCargoId);
                            $id = $intConsecutivo;
                        }

                        if($request > 0){
                            $request_ptto = $this->insertPtto($id,$strVinculacion,$strPttoSalarios,$strPttoPagos,$intUnidad,$intSeccion,$strCentro,$strPttoPara,$strFuente,$intIdbpin,$strIndicador);
                            if($request_ptto > 0){
                                $request_seguridad = $this->insertSeguridad($id,$strTerceroEps,$strTerceroArl,$strTerceroAfp,$strTerceroFondo,
                                    $strFechaEps,$strFechaArl,$strFechaAfp, $strFechaFondo,$intCesantias,$intRiesgo,$floatSalud,$floatPension);
                                if($request_seguridad > 0){

                                    if($intConsecutivo == 0){
                                        if($_FILES['pdf']){
                                            $ruta = 'contrato_'.$request.'_'.bin2hex(random_bytes(6)).'.pdf';
                                            uploadFile($_FILES['pdf'],$ruta);
                                            $this->insertRuta($request,$ruta);
                                        }
                                        $arrResponse = array("status"=>true,"msg"=>"Contrato creado correctamente","id"=>$request);
                                        insertAuditoria("hum_auditoria","hum_funciones_id",9,"Crear",$request,"Contratos laborales","hum_funciones");
                                    }else{
                                        insertAuditoria("hum_auditoria","hum_funciones_id",9,"Editar",$intConsecutivo,"Contratos laborales","hum_funciones");
                                        $arrResponse = array("status"=>true,"msg"=>"Datos actualizados correctamente.");
                                    }
                                }else{
                                    $arrResponse = array("status"=>false,"msg"=>"Error al guardar seguridad social.");
                                }
                            }else{
                                $arrResponse = array("status"=>false,"msg"=>"Error al guardar afectación presupuestal.");
                            }
                        }else if($request =="existe" && $intConsecutivo == 0){
                            $arrResponse = array("status"=>false,"msg"=>"El tercero ya tiene un contrato asignado. En caso de modificaciones debe dirigirse a novedades de contrato");
                        }else{
                            $arrResponse = array("status"=>false,"msg"=>"Error al guardar, inténtelo de nuevo.");
                        }
                    }
                    echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
                }
            }
            die();
        }
        public function getEdit(){
            if(!empty($_SESSION)){
                $intId = intval($_POST['codigo']);
                $request = $this->selectEdit($intId);
                if(!empty($request)){
                    $arrCargoRequisitos = $request['requisitos'];
                    $arrRequisitos = $this->selectRequisitos();
                    if(!empty($arrCargoRequisitos)){
                        for ($i=0; $i < count($arrRequisitos) ; $i++) {
                            $areas = $arrRequisitos[$i];
                            $tipos = $areas['tipos'];
                            for ($j=0; $j < count($tipos) ; $j++) {
                                $tipo = $tipos[$j];
                                $requisitos = $tipo['requisitos'];
                                for ($k=0; $k < count($requisitos)  ; $k++) {
                                    $requisito = $requisitos[$k];
                                    $cargoReq = array_values(array_filter($arrCargoRequisitos,function($e)use($requisito,$areas,$tipo){
                                        return $requisito['id'] == $e['requisito_id'] && $tipo['id'] == $e['tipo_id']
                                        && $areas['id'] == $e['funciones_id'];
                                    }));
                                    $requisito['is_checked'] = !empty($cargoReq) ? 1 : 0;
                                    $requisito['checked'] = intval($cargoReq[0]['estado']);
                                    $requisitos[$k] = $requisito;
                                }
                                $tipos[$j]['requisitos'] = $requisitos;
                                $tipos[$j]['is_checked'] = !empty(array_filter($requisitos,function($e){return $e['is_checked'] == 1;})) ? 1 : 0;
                            }
                            $arrRequisitos[$i]['tipos'] = $tipos;
                            $arrRequisitos[$i]['is_checked'] = !empty(array_filter($tipos,function($e){return $e['is_checked'] == 1;})) ? 1 : 0;
                        }
                        $request['requisitos'] = $arrRequisitos;
                    }
                    $arrResponse = array(
                        "status"=>true,
                        "data"=>$request,
                        "consecutivos"=>getConsecutivos("hum_contratos","id"),
                        "cargos"=>$this->selectCargos(),
                        "terceros"=>getTerceros(),
                        "unidades"=>$this->selectUnidades(),
                        "secciones"=>$this->selecSecciones(),
                        "periodos"=>$this->selectPeriodos(),
                        "centros"=>$this->selectCentros(),
                        "riesgos"=>$this->selectRiesgos(),
                    );
                }else{
                    $arrResponse = array("status"=>false,"consecutivo"=>searchConsec("hum_contratos ","id")-1);
                }
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
            die();
        }
        public function getSearch(){
            if(!empty($_SESSION)){
                //$this->selectTempFuncionarios();
                $request = $this->selectSearch();
                if(!empty($request)){
                    $arrTipoContrato = [
                        1=>"INDEFINIDO",
                        2=>"FIJO",
                        3=>"OBRA O LABOR"
                    ];
                    $total = count($request);
                    for ($i=0; $i < $total; $i++) {
                        if($request[$i]['tipo'] == 2 ){
                            $request[$i]['automatico'] = $request[$i]['automatico'] == 1 ? "Si" : "No";
                        }else{
                            $request[$i]['automatico'] = "N/A";
                        }
                        $request[$i]['fecha_final'] = $request[$i]['tipo'] == 1 ? "N/A" : $request[$i]['fecha_final'];
                        $request[$i]['tipo'] = $arrTipoContrato[$request[$i]['tipo']];
                        $request[$i]['estado'] = strtolower(($request[$i]['estado']));
                    }
                }
                echo json_encode($request,JSON_UNESCAPED_UNICODE);
            }
            die();
        }
    }

    if($_POST){
        $obj = new ContratoController();
        if($_POST['action'] == "get"){
            $obj->initialData();
        }else if($_POST['action'] == "save"){
            $obj->save();
        }else if($_POST['action']=="edit"){
            $obj->getEdit();
        }else if($_POST['action']=="search"){
            $obj->getSearch();
        }
    }

?>
