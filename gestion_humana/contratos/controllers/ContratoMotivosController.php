<?php
    require_once '../../../Librerias/core/Helpers.php';
    require_once '../../../PHPExcel/Classes/PHPExcel.php';
    require_once '../models/ContratoMotivosModel.php';
    session_start();

    class ContratoMotivosController extends ContratoMotivosModel{
        public function save(){
            if(!empty($_SESSION)){
                if(empty($_POST['motivo'])){
                    $arrResponse = array("status"=>false,"msg"=>"Error de datos");
                }else{
                    $strMotivo = ucfirst(replaceChar(strClean($_POST['motivo'])));
                    $intConsecutivo = intval($_POST['consecutivo']);
                    $opcion="";
                    if($intConsecutivo == 0){
                        $opcion = 1;
                        $request = $this->insertData($strMotivo);
                    }else{
                        $opcion = 2;
                        $request = $this->updateData($intConsecutivo,$strMotivo);
                    }
                    if($request > 0){
                        if($opcion == 1){
                            insertAuditoria("hum_auditoria","hum_funciones_id",10,"Crear",$request,"Motivos de retiro de contrato","hum_funciones");
                            $arrResponse = array("status"=>true,"msg"=>"Datos guardados correctamente.","id"=>$request);
                        }else{
                            insertAuditoria("hum_auditoria","hum_funciones_id",10,"Editar",$intConsecutivo,"Motivos de retiro de contrato","hum_funciones");
                            $arrResponse = array("status"=>true,"msg"=>"Datos actualizados correctamente.");
                        }
                    }else if($request =="existe"){
                        $arrResponse = array("status"=>false,"msg"=>"Este motivo ya existe, pruebe con otro.");
                    }else{
                        $arrResponse = array("status"=>false,"msg"=>"No se ha podido guardar, inténte de nuevo.");
                    }
                }
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
            die();
        }
        public function getEdit(){
            if(!empty($_SESSION)){
                $intId = intval($_POST['codigo']);
                $request = $this->selectEdit($intId);
                if(!empty($request)){
                    $arrResponse = array("status"=>true,"data"=>$request,"consecutivos"=>getConsecutivos("hum_contratos_motivos","id"));
                }else{
                    $arrResponse = array("status"=>false,"consecutivo"=>searchConsec("hum_contratos_motivos ","id")-1);
                }
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
            die();
        }
        public function getSearch(){
            if(!empty($_SESSION)){
                $request = $this->selectSearch();
                echo json_encode($request,JSON_UNESCAPED_UNICODE);
            }
            die();
        }
    }

    if($_POST){
        $obj = new ContratoMotivosController();
        if($_POST['action'] == "save"){
            $obj->save();
        }else if($_POST['action']=="edit"){
            $obj->getEdit();
        }else if($_POST['action']=="search"){
            $obj->getSearch();
        }
    }

?>
