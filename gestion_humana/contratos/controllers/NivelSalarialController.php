<?php
    require_once '../../../Librerias/core/Helpers.php';
    require_once '../../../PHPExcel/Classes/PHPExcel.php';
    require_once '../models/NivelSalarialModel.php';
    session_start();

    class NivelSalarialController extends NivelSalarialModel{
        public function save(){
            if(!empty($_SESSION)){
                if(empty($_POST['nombre'])){
                    $arrResponse = array("status"=>false,"msg"=>"Error de datos");
                }else{
                    $strNombre = ucfirst(replaceChar(strClean($_POST['nombre'])));
                    $dblValor = doubleval($_POST['valor']);
                    $intConsecutivo = intval($_POST['consecutivo']);
                    $opcion="";
                    if($intConsecutivo == 0){
                        $opcion = 1;
                        $request = $this->insertData($strNombre,$dblValor);
                    }else{
                        $opcion = 2;
                        $request = $this->updateData($intConsecutivo,$strNombre,$dblValor);
                    }
                    if($request > 0){
                        if($opcion == 1){
                            insertAuditoria("hum_auditoria","hum_funciones_id",12,"Crear",$request,"Nivel salarial","hum_funciones");
                            $arrResponse = array("status"=>true,"msg"=>"Datos guardados correctamente.","id"=>$request);
                        }else{
                            insertAuditoria("hum_auditoria","hum_funciones_id",12,"Editar",$intConsecutivo,"Nivel salarial","hum_funciones");
                            $arrResponse = array("status"=>true,"msg"=>"Datos actualizados correctamente.");
                        }
                    }else if($request =="existe"){
                        $arrResponse = array("status"=>false,"msg"=>"Este nombre ya existe, pruebe con otro.");
                    }else{
                        $arrResponse = array("status"=>false,"msg"=>"No se ha podido guardar, inténte de nuevo.");
                    }
                }
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
            die();
        }
        public function getEdit(){
            if(!empty($_SESSION)){
                $intId = intval($_POST['codigo']);
                $request = $this->selectEdit($intId);
                if(!empty($request)){
                    $arrResponse = array("status"=>true,"data"=>$request,"consecutivos"=>getConsecutivos("humnivelsalarial","id_nivel"));
                }else{
                    $arrResponse = array("status"=>false,"consecutivo"=>searchConsec("humnivelsalarial ","id_nivel")-1);
                }
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
            die();
        }
        public function getSearch(){
            if(!empty($_SESSION)){
                $request = $this->selectSearch();
                echo json_encode($request,JSON_UNESCAPED_UNICODE);
            }
            die();
        }
        public function changeData(){
            if(!empty($_SESSION)){
                $id = intval($_POST['id']);
                $estado = $_POST['estado'];
                $request = $this->updateStatus($id,$estado);
                if($request == 1){
                    insertAuditoria("hum_auditoria","hum_funciones_id",12,"Editar estado",$id,"Nivel salarial","hum_funciones");
                    $arrResponse = array("status"=>true);
                }else{
                    $arrResponse = array("status"=>false,"msg"=>"Error");
                }
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
            die();
        }
    }

    if($_POST){
        $obj = new NivelSalarialController();
        if($_POST['action'] == "save"){
            $obj->save();
        }else if($_POST['action']=="edit"){
            $obj->getEdit();
        }else if($_POST['action']=="search"){
            $obj->getSearch();
        }else if($_POST['action']=="change"){
            $obj->changeData();
        }
    }

?>
