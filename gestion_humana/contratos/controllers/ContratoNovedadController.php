<?php
    require_once '../../../Librerias/core/Helpers.php';
    require_once '../../../PHPExcel/Classes/PHPExcel.php';
    require_once '../models/ContratoNovedadModel.php';
    session_start();

    class ContratoNovedadController extends ContratoNovedadModel{
        public function getData(){
            if(!empty($_SESSION)){
                $arrContratos = $this->selectContratos();
                if(!empty($arrContratos)){
                    $arrRequisitos = $this->selectRequisitos();
                    $total = count($arrContratos);
                    for ($p=0; $p < $total; $p++) {
                        $arrCargoRequisitos = $arrContratos[$p]['requisitos'];
                        if(!empty($arrCargoRequisitos)){
                            for ($i=0; $i < count($arrRequisitos) ; $i++) {
                                $areas = $arrRequisitos[$i];
                                $tipos = $areas['tipos'];
                                for ($j=0; $j < count($tipos) ; $j++) {
                                    $tipo = $tipos[$j];
                                    $requisitos = $tipo['requisitos'];
                                    for ($k=0; $k < count($requisitos)  ; $k++) {
                                        $requisito = $requisitos[$k];
                                        $cargoReq = array_values(array_filter($arrCargoRequisitos,function($e)use($requisito,$areas,$tipo){
                                            return $requisito['id'] == $e['requisito_id'] && $tipo['id'] == $e['tipo_id']
                                            && $areas['id'] == $e['funciones_id'];
                                        }));
                                        $requisito['is_checked'] = !empty($cargoReq) ? 1 : 0;
                                        $requisito['checked'] = intval($cargoReq[0]['estado']);
                                        $requisitos[$k] = $requisito;
                                    }
                                    $tipos[$j]['requisitos'] = $requisitos;
                                    $tipos[$j]['is_checked'] = !empty(array_filter($requisitos,function($e){return $e['is_checked'] == 1;})) ? 1 : 0;
                                }
                                $arrRequisitos[$i]['tipos'] = $tipos;
                                $arrRequisitos[$i]['is_checked'] = !empty(array_filter($tipos,function($e){return $e['is_checked'] == 1;})) ? 1 : 0;
                            }
                            $arrContratos[$p]['requisitos'] = $arrRequisitos;
                        }
                    }
                }
                $arrData = array(
                    "motivos"=>$this->selectMotivos(),
                    "contratos"=>$arrContratos,
                    "cargos"=>$this->selectCargos(),
                    "terceros"=>getTerceros(),
                    "bancos"=>$this->selectBancos(),
                    "periodos"=>$this->selectPeriodos(),
                );
                echo json_encode($arrData);
            }
        }
        public function save(){
            if(!empty($_SESSION)){
                if(empty($_POST['novedad']) || empty($_POST['contrato'])){
                    $arrResponse = array("status"=>false,"msg"=>"Error de datos");
                }else{
                    $intConsecutivo = intval($_POST['consecutivo']);
                    $intNovedad = intval($_POST['novedad']);
                    $intIdMotivo = intval($_POST['motivo']);
                    $arrContrato = json_decode($_POST['contrato'],true);
                    $arrData = json_decode($_POST['data'],true);
                    $opcion = "";
                    if($intConsecutivo == 0){
                        $opcion = 1;
                        $request = $this->insertData($intNovedad,$intIdMotivo,$arrContrato,$arrData);
                    }else{
                        if($_FILES['pdf']){
                            $ruta = 'renovado_'.$intConsecutivo.'_'.bin2hex(random_bytes(6)).'.pdf';
                            uploadFile($_FILES['pdf'],$ruta);
                            $request = $this->updateRuta($intConsecutivo,$ruta);
                        }
                    }
                    if($request > 0){
                        if($opcion == 1){
                            if($_FILES['pdf']){
                                $ruta = 'renovado_'.$request.'_'.bin2hex(random_bytes(6)).'.pdf';
                                uploadFile($_FILES['pdf'],$ruta);
                                $this->insertRuta($request,$ruta);
                            }
                            insertAuditoria("hum_auditoria","hum_funciones_id",11,"Crear",$request,"Novedad de contrato","hum_funciones");
                            $arrResponse = array("status"=>true,"msg"=>"Datos guardados correctamente.","id"=>$request);
                        }else{
                            insertAuditoria("hum_auditoria","hum_funciones_id",11,"Editar",$intConsecutivo,"Novedad de contrato","hum_funciones");
                            $arrResponse = array("status"=>true,"msg"=>"Contrato firmado cargado correctamente.");
                        }
                    }else if($request =="existe"){
                        if($intNovedad == 1){
                            $arrResponse = array("status"=>false,"msg"=>"Este contrato ya fue renovado, debe retirarlo para renovarlo de nuevo.");
                        }else if($intNovedad == 2){
                            $arrResponse = array("status"=>false,"msg"=>"Este contrato ya fue retirado, debe renovarlo para retirarlo de nuevo.");
                        }
                    }else{
                        $arrResponse = array("status"=>false,"msg"=>"No se ha podido guardar, inténte de nuevo.");
                    }
                }
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
            die();
        }
        public function getEdit(){
            if(!empty($_SESSION)){
                $intId = intval($_POST['codigo']);
                $request = $this->selectEdit($intId);
                if(!empty($request)){
                    $arrCargoRequisitos = $request['detalle']['requisitos'];
                    $arrRequisitos = $this->selectRequisitos();
                    if(!empty($arrCargoRequisitos)){
                        for ($i=0; $i < count($arrRequisitos) ; $i++) {
                            $areas = $arrRequisitos[$i];
                            $tipos = $areas['tipos'];
                            for ($j=0; $j < count($tipos) ; $j++) {
                                $tipo = $tipos[$j];
                                $requisitos = $tipo['requisitos'];
                                for ($k=0; $k < count($requisitos)  ; $k++) {
                                    $requisito = $requisitos[$k];
                                    $cargoReq = array_values(array_filter($arrCargoRequisitos,function($e)use($requisito,$areas,$tipo){
                                        return $requisito['id'] == $e['requisito_id'] && $tipo['id'] == $e['tipo_id']
                                        && $areas['id'] == $e['funciones_id'];
                                    }));
                                    $requisito['is_checked'] = !empty($cargoReq) ? 1 : 0;
                                    $requisito['checked'] = intval($cargoReq[0]['estado']);
                                    $requisitos[$k] = $requisito;
                                }
                                $tipos[$j]['requisitos'] = $requisitos;
                                $tipos[$j]['is_checked'] = !empty(array_filter($requisitos,function($e){return $e['is_checked'] == 1;})) ? 1 : 0;
                            }
                            $arrRequisitos[$i]['tipos'] = $tipos;
                            $arrRequisitos[$i]['is_checked'] = !empty(array_filter($tipos,function($e){return $e['is_checked'] == 1;})) ? 1 : 0;
                        }
                        $request['detalle']['requisitos'] = $arrRequisitos;
                    }
                    $arrResponse = array(
                        "status"=>true,
                        "data"=>$request,
                        "motivos"=>$this->selectMotivos(),
                        "cargos"=>$this->selectCargos(),
                        "terceros"=>getTerceros(),
                        "bancos"=>$this->selectBancos(),
                        "periodos"=>$this->selectPeriodos(),
                        "consecutivos"=>getConsecutivos("hum_contratos_novedades","id")
                    );
                }else{
                    $arrResponse = array("status"=>false,"consecutivo"=>searchConsec("hum_contratos_novedades ","id")-1);
                }
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
            die();
        }
        public function getSearch(){
            if(!empty($_SESSION)){
                $request = $this->selectSearch();
                echo json_encode($request,JSON_UNESCAPED_UNICODE);
            }
            die();
        }
    }

    if($_POST){
        $obj = new ContratoNovedadController();
        if($_POST['action'] == "get"){
            $obj->getData();
        }else if($_POST['action'] == "save"){
            $obj->save();
        }else if($_POST['action']=="edit"){
            $obj->getEdit();
        }else if($_POST['action']=="search"){
            $obj->getSearch();
        }
    }

?>
