<?php
    require_once '../../../Librerias/core/Helpers.php';
    require_once '../../../PHPExcel/Classes/PHPExcel.php';
    require_once '../models/CaracterizacionFuncionesModel.php';
    session_start();

    class CaracterizacionFuncionesController extends CaracterizacionFuncionesModel{
        public function save(){
            if(!empty($_SESSION)){
                if(empty($_POST['nombre'])){
                    $arrResponse = array("status"=>false,"msg"=>"Error de datos");
                }else{
                    $strNombre = ucfirst(replaceChar(strClean($_POST['nombre'])));
                    $intOrden = intval($_POST['orden']);
                    $intConsecutivo = intval($_POST['consecutivo']);
                    $opcion="";
                    if($intConsecutivo == 0){
                        $opcion = 1;
                        $request = $this->insertData($strNombre,$intOrden);
                    }else{
                        $opcion = 2;
                        $request = $this->updateData($intConsecutivo,$strNombre,$intOrden);
                    }
                    if($request > 0){
                        if($opcion == 1){
                            insertAuditoria("mipg_auditoria","mipg_funciones_id",1,"Crear",$request);
                            $arrResponse = array("status"=>true,"msg"=>"Datos guardados correctamente.","id"=>$request);
                        }else{
                            insertAuditoria("mipg_auditoria","mipg_funciones_id",1,"Editar",$intConsecutivo);
                            $arrResponse = array("status"=>true,"msg"=>"Datos actualizados correctamente.");
                        }
                    }else if($request =="existe"){
                        $arrResponse = array("status"=>false,"msg"=>"Este nombre ya existe, pruebe con otro.");
                    }else{
                        $arrResponse = array("status"=>false,"msg"=>"No se ha podido guardar, inténte de nuevo.");
                    }
                }
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
            die();
        }
        public function getEdit(){
            if(!empty($_SESSION)){
                $intId = intval($_POST['codigo']);
                $request = $this->selectEdit($intId);
                if(!empty($request)){
                    $arrResponse = array("status"=>true,"data"=>$request,"consecutivos"=>getConsecutivos("hum_caracterizacion_funciones","id"));
                }else{
                    $arrResponse = array("status"=>false,"consecutivo"=>searchConsec("hum_caracterizacion_funciones ","id")-1);
                }
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
            die();
        }
        public function getSearch(){
            if(!empty($_SESSION)){
                $request = $this->selectSearch();
                echo json_encode($request,JSON_UNESCAPED_UNICODE);
            }
            die();
        }
        public function changeData(){
            if(!empty($_SESSION)){
                $id = intval($_POST['id']);
                $estado = $_POST['estado'];
                $request = $this->updateStatus($id,$estado);
                if($request == 1){
                    insertAuditoria("mipg_auditoria","mipg_funciones_id",1,"Editar estado",$id);
                    $arrResponse = array("status"=>true);
                }else{
                    $arrResponse = array("status"=>false,"msg"=>"Error");
                }
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
            die();
        }
    }

    if($_POST){
        $obj = new CaracterizacionFuncionesController();
        if($_POST['action'] == "save"){
            $obj->save();
        }else if($_POST['action']=="edit"){
            $obj->getEdit();
        }else if($_POST['action']=="search"){
            $obj->getSearch();
        }else if($_POST['action']=="change"){
            $obj->changeData();
        }
    }

?>
