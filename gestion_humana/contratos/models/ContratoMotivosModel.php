<?php
    require_once '../../../Librerias/core/Helpers.php';
    require_once '../../../Librerias/core/Mysql.php';
    session_start();
    /*ini_set('display_errors', '1');
    ini_set('display_startup_errors', '1');
    error_reporting(E_ALL);*/
    class ContratoMotivosModel extends Mysql{
        private $intConsecutivo;
        private $strMotivo;

        function __construct(){
            parent::__construct();
        }

        public function insertData($strMotivo){
            $this->strMotivo = $strMotivo;
            $sql = "SELECT * FROM hum_contratos_motivos WHERE motivo = '$this->strMotivo'";
            $request = $this->select_all($sql);
            $return = "";
            if(empty($request)){
                $sql="INSERT INTO hum_contratos_motivos(motivo) VALUES(?)";
                $return = $this->insert($sql,[$this->strMotivo]);
            }else{
                $return = "existe";
            }
            return $return;
        }
        public function updateData($intConsecutivo,$strMotivo){
            $this->strMotivo = $strMotivo;
            $this->intConsecutivo = $intConsecutivo;
            $sql = "SELECT * FROM hum_contratos_motivos WHERE motivo = '$this->strMotivo' AND id != $this->intConsecutivo";
            $request = $this->select_all($sql);
            $return = "";
            if(empty($request)){
                $sql="UPDATE hum_contratos_motivos SET motivo=? WHERE id = $this->intConsecutivo";
                $return = $this->update($sql,[$this->strMotivo]);
            }else{
                $return = "existe";
            }
            return $return;
        }
        public function selectEdit($intConsecutivo){
            $this->intConsecutivo = $intConsecutivo;
            $sql = "SELECT * FROM hum_contratos_motivos WHERE id = $this->intConsecutivo";
            $request = $this->select($sql);
            return $request;
        }
        public function selectSearch(){
            $sql = "SELECT * FROM hum_contratos_motivos ORDER BY id DESC";
            $request = $this->select_all($sql);
            return $request;
        }
    }
?>
