<?php
    require_once '../../../Librerias/core/Helpers.php';
    require_once '../../../Librerias/core/Mysql.php';
    session_start();
    /*ini_set('display_errors', '1');
    ini_set('display_startup_errors', '1');
    error_reporting(E_ALL);*/
    class NivelSalarialModel extends Mysql{
        private $intConsecutivo;
        private $strNombre;
        private $dblValor;

        function __construct(){
            parent::__construct();
        }

        public function insertData(string $strNombre,int $dblValor){
            $this->strNombre = $strNombre;
            $this->dblValor = $dblValor;
            $sql = "SELECT * FROM humnivelsalarial WHERE nombre = '$this->strNombre'";
            $request = $this->select_all($sql);
            $return = "";
            if(empty($request)){
                $sql="INSERT INTO humnivelsalarial(nombre,valor,estado) VALUES(?,?,?)";
                $return = $this->insert($sql,[$this->strNombre,$this->dblValor,'S']);
            }else{
                $return = "existe";
            }
            return $return;
        }
        public function updateData(int $intConsecutivo,string $strNombre,int $dblValor){
            $this->strNombre = $strNombre;
            $this->intConsecutivo = $intConsecutivo;
            $this->dblValor = $dblValor;
            $sql = "SELECT * FROM humnivelsalarial WHERE nombre = '$this->strNombre' AND id_nivel != $this->intConsecutivo";
            $request = $this->select_all($sql);
            $return = "";
            if(empty($request)){
                $sql="UPDATE humnivelsalarial SET nombre=?,valor=? WHERE id_nivel = $this->intConsecutivo";
                $return = $this->update($sql,[$this->strNombre,$this->dblValor]);
            }else{
                $return = "existe";
            }
            return $return;
        }
        public function updateStatus(int $intConsecutivo,string $estado){
            $this->intConsecutivo = $intConsecutivo;
            $sql = "UPDATE humnivelsalarial SET estado = '$estado' WHERE id_nivel = $this->intConsecutivo";
            $request = $this->update($sql,[$estado]);
            return $request;
        }
        public function selectEdit($intConsecutivo){
            $this->intConsecutivo = $intConsecutivo;
            $sql = "SELECT * FROM humnivelsalarial WHERE id_nivel = $this->intConsecutivo";
            $request = $this->select($sql);
            return $request;
        }
        public function selectSearch(){
            $sql = "SELECT * FROM humnivelsalarial ORDER BY id_nivel DESC";
            $request = $this->select_all($sql);
            $total = count($request);
            for ($i=0; $i < $total; $i++) {
                $request[$i]['is_status'] = $request[$i]['estado'] =="S"? 1 : 0;
            }
            return $request;
        }
    }
?>
