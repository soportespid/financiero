<?php
    require_once '../../../Librerias/core/Helpers.php';
    require_once '../../../Librerias/core/Mysql.php';
    session_start();
    /*ini_set('display_errors', '1');
    ini_set('display_startup_errors', '1');
    error_reporting(E_ALL);*/
    class CaracterizacionRequisitosModel extends Mysql{
        private $intConsecutivo;
        private $strNombre;
        private $intOrden;
        private $intFuncion;

        function __construct(){
            parent::__construct();
        }

        public function insertData(string $strNombre, int $intFuncion){
            $this->strNombre = $strNombre;
            $this->intFuncion = $intFuncion;
            $sql = "SELECT * FROM hum_caracterizacion_requisito WHERE nombre = '$this->strNombre' AND tipo_id = $this->intFuncion";
            $request = $this->select_all($sql);
            $return = "";
            if(empty($request)){
                $sql="INSERT INTO hum_caracterizacion_requisito(nombre,tipo_id) VALUES(?,?)";
                $return = $this->insert($sql,[$this->strNombre,$this->intFuncion]);
            }else{
                $return = "existe";
            }
            return $return;
        }
        public function updateData(int $intConsecutivo,string $strNombre, int $intFuncion){
            $this->intFuncion = $intFuncion;
            $this->strNombre = $strNombre;
            $this->intConsecutivo = $intConsecutivo;
            $sql = "SELECT * FROM hum_caracterizacion_requisito WHERE nombre = '$this->strNombre' AND tipo_id = $this->intFuncion AND id != $this->intConsecutivo";
            $request = $this->select_all($sql);
            $return = "";
            if(empty($request)){
                $sql="UPDATE hum_caracterizacion_requisito SET nombre=?,tipo_id=? WHERE id = $this->intConsecutivo";
                $return = $this->update($sql,[$this->strNombre,$this->intFuncion]);
            }else{
                $return = "existe";
            }
            return $return;
        }
        public function updateStatus(int $intConsecutivo,string $estado){
            $this->intConsecutivo = $intConsecutivo;
            $sql = "UPDATE hum_caracterizacion_requisito SET estado = '$estado' WHERE id = $this->intConsecutivo";
            $request = $this->update($sql,[$estado]);
            return $request;
        }
        public function selectFunciones(){
            $sql="SELECT * FROM hum_caracterizacion_tipo WHERE estado = 'S' ORDER BY orden";
            $request = $this->select_all($sql);
            return $request;
        }
        public function selectEdit($intConsecutivo){
            $this->intConsecutivo = $intConsecutivo;
            $sql = "SELECT * FROM hum_caracterizacion_requisito WHERE id = $this->intConsecutivo";
            $request = $this->select($sql);
            return $request;
        }
        public function selectSearch(){
            $sql = "SELECT r.id,
            f.nombre as nombre_funcion,
            t.nombre as nombre_tipo,
            r.nombre,
            r.estado
            FROM hum_caracterizacion_requisito r
            INNER JOIN hum_caracterizacion_tipo t ON t.id = r.tipo_id
            INNER JOIN hum_caracterizacion_funciones f ON t.funcion_id = f.id
            ORDER BY r.id DESC";
            $request = $this->select_all($sql);
            $total = count($request);
            for ($i=0; $i < $total; $i++) {
                $request[$i]['is_status'] = $request[$i]['estado'] =="S"? 1 : 0;
            }
            return $request;
        }
    }
?>
