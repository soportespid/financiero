<?php
    require_once '../../../Librerias/core/Helpers.php';
    require_once '../../../Librerias/core/Mysql.php';
    session_start();
    /*ini_set('display_errors', '1');
    ini_set('display_startup_errors', '1');
    error_reporting(E_ALL);*/
    class ContratoModel extends Mysql{
        private $intConsecutivo;
        private $strTercero;
        private $intPeriodoId;
        private $strFechaInicio;
        private $strFechaFinal;
        private $strBancoId;
        private $strCuentaBanco;
        private $intCargoId;
        private $strJornada;
        private $strContrato;
        private $intTipoContrato;
        private $strRuta;
        private $intAutomatico;
        private $intDescuento;
        private $strVinculacion;
        private $strPttoPagos;
        private $strPttoSalarios;
        private $strPttoPara;
        private $intUnidad;
        private $intSeccion;
        private $strCentro;
        private $strTerceroEps;
        private $strTerceroArl;
        private $strTerceroAfp;
        private $strTerceroFondo;
        private $strFechaEps;
        private $strFechaArl;
        private $strFechaAfp;
        private $strFechaFondo;
        private $intCesantias;
        private $intRiesgo;
        private $floatSalud;
        private $floatPension;
        private $strFuente;
        private $strIndicador;
        private $intIdbpin;

        function __construct(){
            parent::__construct();
        }

        public function selectCargos(){
            $sql = "SELECT
            c.codcargo as codigo,
            c.nombrecargo as nombre,
            c.clasificacion,
            s.valor,
            s.id_nivel as escala,
            s.nombre as nivel_salarial
            FROM planaccargos c
            INNER JOIN humnivelsalarial s ON s.id_nivel=c.clasificacion
            WHERE c.estado = 'S' AND s.estado = 'S' ORDER BY c.codcargo DESC";
            $request = $this->select_all($sql);
            $dataRequisitos = $this->selectRequisitos();
            if(!empty($request)){
                $totalCargos = count($request);
                for ($i=0; $i < $totalCargos ; $i++) {
                    $id = $request[$i]['codigo'];
                    $sql = "SELECT hf.id as funciones_id, ht.id as tipo_id, hr.id as requisito_id
                    FROM planaccargos_requisitos pr
                    INNER JOIN hum_caracterizacion_requisito hr ON hr.id = pr.requisito_id
                    INNER JOIN hum_caracterizacion_tipo ht ON ht.id = hr.tipo_id
                    INNER JOIN hum_caracterizacion_funciones hf ON hf.id = ht.funcion_id
                    WHERE hr.estado = 'S' AND ht.estado = 'S' AND hf.estado = 'S' AND pr.cargo_id = $id";
                    $arrCargoRequisitos = $this->select_all($sql);
                    if(!empty($arrCargoRequisitos)){
                        for ($j=0; $j < count($dataRequisitos) ; $j++) {
                            $areas = $dataRequisitos[$j];
                            $tipos = $areas['tipos'];
                            for ($k=0; $k < count($tipos) ; $k++) {
                                $tipo = $tipos[$k];
                                $requisitos = $tipo['requisitos'];
                                for ($l=0; $l < count($requisitos)  ; $l++) {
                                    $requisito = $requisitos[$l];
                                    $cargoReq = array_filter($arrCargoRequisitos,function($e)use($requisito,$areas,$tipo){
                                        return $requisito['id'] == $e['requisito_id'] && $tipo['id'] == $e['tipo_id']
                                        && $areas['id'] == $e['funciones_id'];
                                    });
                                    $requisito['is_checked'] = !empty($cargoReq) ? 1 : 0;
                                    $requisito['checked'] =1;
                                    $requisitos[$l] = $requisito;
                                }
                                $tipos[$k]['requisitos'] = $requisitos;
                                $tipos[$k]['is_checked'] = !empty(array_filter($requisitos,function($e){return $e['is_checked'] == 1;})) ? 1 : 0;
                            }
                            $dataRequisitos[$j]['tipos'] = $tipos;
                            $dataRequisitos[$j]['is_checked'] = !empty(array_filter($tipos,function($e){return $e['is_checked'] == 1;})) ? 1 : 0;
                        }
                    }
                    $request[$i]['requisitos'] = $dataRequisitos;
                }
            }
            return $request;
        }
        public function selectRequisitos(){
            $sql = "SELECT * FROM hum_caracterizacion_funciones ORDER BY orden";
            $request = $this->select_all($sql);
            $arrData = [];
            if(!empty($request)){
                $total = count($request);
                for ($i=0; $i < $total ; $i++) {
                    $e = $request[$i];
                    $idFuncion = $e['id'];
                    $sqlTipos = "SELECT * FROM hum_caracterizacion_tipo WHERE funcion_id = $idFuncion ORDER BY orden ";
                    $arrTipos = $this->select_all($sqlTipos);
                    if(!empty($arrTipos)){
                        $totalTipos = count($arrTipos);
                        for ($j=0; $j < $totalTipos ; $j++) {
                            $idTipo = $arrTipos[$j]['id'];
                            $sqlRequisitos = "SELECT * FROM hum_caracterizacion_requisito WHERE tipo_id = $idTipo";
                            $arrRequisitos = $this->select_all($sqlRequisitos);
                            $totalRequisitos = count($arrRequisitos);
                            for ($k=0; $k < $totalRequisitos ; $k++) {
                                $arrRequisitos[$k]['is_checked'] = 0;
                            }
                            $arrTipos[$j]['requisitos'] = $arrRequisitos;
                            $arrTipos[$j]['is_checked'] = 1;
                        }
                        $arrTipos = array_values(array_filter($arrTipos,function($e){return !empty($e['requisitos']);}));
                        $request[$i]['tipos'] = $arrTipos;
                        $request[$i]['is_checked'] = 0;
                        array_push($arrData,$request[$i]);
                    }
                }
            }
            return $arrData;
        }
        public function selectBancos(){
            $sql = "SELECT codigo, nombre FROM hum_bancosfun WHERE estado='S' ORDER BY CONVERT(codigo, SIGNED INTEGER)";
            $request = $this->select_all($sql);
            return $request;
        }
        public function selectPeriodos(){
            $sql = "SELECT id_periodo as codigo,nombre,dias FROM humperiodos ORDER BY id_periodo";
            $request = $this->select_all($sql);
            return $request;
        }
        public function selectUnidades(){
            $sql = "SELECT id_cc as codigo, nombre FROM pptouniejecu WHERE estado = 'S' ORDER BY id_cc ASC";
            $request = $this->select_all($sql);
            return $request;
        }
        public function selecSecciones(){
            $sql = "SELECT id_seccion_presupuestal as codigo, nombre,id_unidad_ejecutora as unidad FROM pptoseccion_presupuestal";
            $request = $this->select_all($sql);
            return $request;
        }
        public function selectRiesgos(){
            $sql = "SELECT id as codigo, tarifa, detalle as nombre FROM hum_nivelesarl WHERE estado = 'S' ORDER BY id";
            $request = $this->select_all($sql);
            return $request;
        }
        public function selectCentros(){
            $sql = "SELECT T1.id_cc as codigo, T1.nombre,T3.id_seccion_presupuestal as seccion
            FROM centrocosto AS T1
            INNER JOIN centrocostos_seccionpresupuestal AS T2 ON T1.id_cc = T2.id_cc
            INNER JOIN pptoseccion_presupuestal AS T3 ON T3.id_seccion_presupuestal = T2.id_sp
            WHERE T1.estado = 'S'  ORDER BY CONVERT(T1.id_cc, SIGNED INTEGER)";
            $request = $this->select_all($sql);
            return $request;
        }
        public function selectEdit(int $intConsecutivo){
            $this->intConsecutivo = $intConsecutivo;
            $sql = "SELECT
            cab.id,
            cab.tercero,
            cab.tipo,
            cab.periodo,
            DATE_FORMAT(cab.fecha_inicial,'%Y-%m-%d') as fecha_inicial,
            DATE_FORMAT(cab.fecha_final,'%Y-%m-%d') as fecha_final,
            cab.banco,
            cab.cuenta,
            cab.cargo_id,
            cab.jornada,
            cab.contrato,
            cab.estado,
            cab.automatico,
            cab.descuento,
            c.nombrecargo as nombre_cargo,
            s.valor as salario,
            s.id_nivel as escala,
            s.nombre as nivel_salarial,
            CASE WHEN t.razonsocial IS NULL OR t.razonsocial = ''
            THEN CONCAT(t.nombre1,' ',t.nombre2,' ',t.apellido1,' ',t.apellido2)
            ELSE t.razonsocial END AS nombre_tercero,
            p.nombre as nombre_periodo,
            t.direccion,
            t.tipodoc
            FROM hum_contratos cab
            LEFT JOIN terceros t ON cab.tercero = t.cedulanit
            LEFT JOIN planaccargos c ON cab.cargo_id = c.codcargo
            LEFT JOIN humnivelsalarial s ON s.id_nivel=c.clasificacion
            LEFT JOIN humperiodos p ON cab.periodo = p.id_periodo
            WHERE cab.id = $this->intConsecutivo";
            $request = $this->select($sql);
            if(!empty($request)){
                $idCargo = $request['cargo_id'];
                $sqlRuta = "SELECT ruta FROM hum_contratos_anexos WHERE documento_id = $this->intConsecutivo AND tipo = 'CONTRATO'";
                $sqlPtto = "SELECT * FROM hum_contratos_ptto WHERE contrato_id = $this->intConsecutivo ORDER BY id DESC LIMIT 1";

                $sqlSeguridad = "SELECT *,DATE_FORMAT(fecha_eps,'%Y-%m-%d') as fecha_eps,
                DATE_FORMAT(fecha_afp,'%Y-%m-%d') as fecha_afp,
                DATE_FORMAT(fecha_arl,'%Y-%m-%d') as fecha_arl,
                DATE_FORMAT(fecha_fondo,'%Y-%m-%d') as fecha_fondo
                FROM hum_contratos_seguridad WHERE contrato_id = $this->intConsecutivo ORDER BY id DESC LIMIT 1";

                $requestRuta = $this->select($sqlRuta);
                $sqlRequisitos = "SELECT hf.id as funciones_id, ht.id as tipo_id, hr.id as requisito_id,pr.estado
                    FROM hum_contratos_requisitos pr
                    INNER JOIN hum_caracterizacion_requisito hr ON hr.id = pr.requisito_id
                    INNER JOIN hum_caracterizacion_tipo ht ON ht.id = hr.tipo_id
                    INNER JOIN hum_caracterizacion_funciones hf ON hf.id = ht.funcion_id
                    WHERE hr.estado = 'S' AND ht.estado = 'S' AND hf.estado = 'S' AND
                    pr.documento_id = $this->intConsecutivo AND pr.cargo_id = $idCargo AND pr.tipo ='CONTRATO'";

                $sqlCuentas = "SELECT ban.codigo, ban.nombre,te.numero_cuenta as cuenta
                FROM teso_cuentas_terceros te
                LEFT JOIN hum_bancosfun ban ON ban.codigo =te.codigo_banco
                LEFT JOIN terceros t ON te.id_tercero = t.id_tercero
                WHERE t.cedulanit = {$request['tercero']}";

                $request['cuentas']= $this->select_all($sqlCuentas);
                $request['anexo'] = !empty($requestRuta) ? $requestRuta['ruta'] : "";
                $request['jornada'] = json_decode($request['jornada'],JSON_UNESCAPED_UNICODE);
                $request['contrato'] = json_decode($request['contrato'],JSON_UNESCAPED_UNICODE);
                $request['ptto'] = $this->select($sqlPtto);
                $request['seguridad']=$this->select($sqlSeguridad);
                $request['requisitos'] = $this->select_all($sqlRequisitos);
            }
            return $request;
        }
        public function selectSearch(){

            $sql = "SELECT
            cab.id,
            cab.tercero,
            cab.tipo,
            cab.periodo,
            DATE_FORMAT(cab.fecha_inicial,'%d/%m/%Y') as fecha_inicial,
            DATE_FORMAT(cab.fecha_final,'%d/%m/%Y') as fecha_final,
            cab.banco,
            cab.cuenta,
            cab.cargo_id,
            cab.estado,
            cab.automatico,
            c.nombrecargo as nombre_cargo,
            s.valor as salario,
            s.id_nivel as escala,
            s.nombre as nivel_salarial,
            CASE WHEN t.razonsocial IS NULL OR t.razonsocial = ''
            THEN CONCAT(t.nombre1,' ',t.nombre2,' ',t.apellido1,' ',t.apellido2)
            ELSE t.razonsocial END AS nombre_tercero,
            b.nombre as nombre_banco,
            p.nombre as nombre_periodo,
            t.direccion,
            t.tipodoc
            FROM hum_contratos cab
            LEFT JOIN terceros t ON cab.tercero = t.cedulanit
            LEFT JOIN planaccargos c ON cab.cargo_id = c.codcargo
            LEFT JOIN humnivelsalarial s ON s.id_nivel=c.clasificacion
            LEFT JOIN humperiodos p ON cab.periodo = p.id_periodo
            LEFT JOIN hum_bancosfun b ON cab.banco = b.codigo
            ORDER BY cab.id DESC";
            $request = $this->select_all($sql);
            if(!empty($request)){
                $total = count($request);
                for ($i=0; $i < $total ; $i++) {
                    $contrato = $request[$i];
                    if($contrato['estado'] =="RENOVADO"){
                        $idContrato = $contrato['id'];
                        $sqlRev = "SELECT
                        cab.id,
                        cab.tercero,
                        cab.tipo,
                        cab.periodo,
                        DATE_FORMAT(cab.fecha_inicial,'%d/%m/%Y') as fecha_inicial,
                        DATE_FORMAT(cab.fecha_final,'%d/%m/%Y') as fecha_final,
                        cab.banco,
                        cab.cuenta,
                        cab.cargo_id,
                        cab.novedad_id,
                        cab.jornada,
                        cab.contrato,
                        c.nombrecargo as nombre_cargo,
                        s.valor as salario,
                        s.id_nivel as escala,
                        s.nombre as nivel_salarial,
                        CASE WHEN t.razonsocial IS NULL OR t.razonsocial = ''
                        THEN CONCAT(t.nombre1,' ',t.nombre2,' ',t.apellido1,' ',t.apellido2)
                        ELSE t.razonsocial END AS nombre_tercero,
                        b.nombre as nombre_banco,
                        p.nombre as nombre_periodo,
                        t.direccion,
                        t.tipodoc
                        FROM hum_contratos_renovacion cab
                        LEFT JOIN terceros t ON cab.tercero = t.cedulanit
                        LEFT JOIN planaccargos c ON cab.cargo_id = c.codcargo
                        LEFT JOIN humnivelsalarial s ON s.id_nivel=c.clasificacion
                        LEFT JOIN humperiodos p ON cab.periodo = p.id_periodo
                        LEFT JOIN hum_bancosfun b ON cab.banco = b.codigo
                        LEFT JOIN hum_contratos_novedades nov ON nov.id = cab.novedad_id
                        WHERE nov.contrato_id = $idContrato
                        AND cab.fecha_inicial = (SELECT max(re.fecha_inicial) FROM hum_contratos_renovacion re WHERE re.novedad_id = cab.novedad_id)
                        ORDER BY nov.id DESC";
                        $arrRev = $this->select($sqlRev);
                        $request[$i]['fecha_inicial'] = $arrRev['fecha_inicial'];
                        $request[$i]['fecha_final'] = $arrRev['fecha_final'];
                        $request[$i]['salario'] = $arrRev['salario'];
                        $request[$i]['nombre_cargo'] = $arrRev['nombre_cargo'];
                        $request[$i]['nombre_periodo'] = $arrRev['nombre_periodo'];
                        $request[$i]['tipo'] = $arrRev['tipo'];

                    }
                }
            }
            return $request;
        }
        public function selectTempFuncionarios(){

            $arrCargos = $this->selectCargos();
            $arrPeriodos = $this->selectPeriodos();

            $sql="SELECT T1.codfun,T1.fechain as fecha
            FROM hum_funcionarios T1
            WHERE (T1.item = 'IDCARGO' OR T1.item = 'DOCTERCERO' OR T1.item = 'NUMCUENTA' OR T1.item = 'NOMCUENTA' OR T1.item = 'NUMEPS' OR T1.item = 'NUMARL'
                    OR T1.item = 'NUMAFP' OR T1.item = 'NUMFDC' OR T1.item = 'NUMCC' OR T1.item = 'PERLIQ' OR T1.item = 'TPCESANTIAS'
                    OR T1.item = 'NIVELARL' OR T1.item = 'UNIEJECUTORA' OR T1.item = 'TPRESUPUESTO' OR T1.item = 'PORSF' OR T1.item = 'PORPF'
                    OR T1.item = 'TPRESUPUESTOPARA' OR T1.item = 'SECCIONPRESUP' OR T1.item = 'TPRESUPUESTOOTROS' OR T1.item = 'TVINCULACION') AND T1.estado = 'S'
            GROUP BY T1.codfun
            ORDER BY CONVERT(T1.codfun, SIGNED INTEGER)";
            $request = $this->select_all($sql);

            $this->delete("TRUNCATE TABLE hum_contratos");
            $this->delete("TRUNCATE TABLE hum_contratos_novedades");
            $this->delete("TRUNCATE TABLE hum_contratos_renovacion");
            $this->delete("TRUNCATE TABLE hum_contratos_requisitos");
            $this->delete("TRUNCATE TABLE hum_contratos_ptto");
            $this->delete("TRUNCATE TABLE hum_contratos_seguridad");
            foreach ($request as $data) {
                $fecha = $data['fecha'];
                $idFuncionario = $data['codfun'];
                $tipoContrato = 1;
                $idCargo = $this->selectTempCampoFuncionario("IDCARGO",$idFuncionario);
                $arrCargo = array_values(array_filter($arrCargos,function($e)use($idCargo){return $idCargo == $e['codigo'];}));
                $cargo = 0;
                $cargoRequisitos = [];
                if(!empty($arrCargo)){
                    $cargo = $arrCargo[0]['codigo'];
                    $cargoRequisitos = $arrCargo[0]['requisitos'];
                }
                $tercero = $this->selectTempCampoFuncionario("DOCTERCERO",$idFuncionario);
                $cuenta = $this->selectTempCampoFuncionario("NUMCUENTA",$idFuncionario);
                $banco = $this->selectTempCampoFuncionario("NOMCUENTA",$idFuncionario);
                $eps = strval($this->selectTempCampoFuncionario("NUMEPS",$idFuncionario));
                $arl = strval($this->selectTempCampoFuncionario("NUMARL",$idFuncionario));
                $afp = strval($this->selectTempCampoFuncionario("NUMAFP",$idFuncionario));
                $cesantias = strval($this->selectTempCampoFuncionario("NUMFDC",$idFuncionario));
                $centro = strval($this->selectTempCampoFuncionario("NUMCC",$idFuncionario));
                $idPeriodo = $this->selectTempCampoFuncionario("PERLIQ",$idFuncionario);
                $arrPeriodo = array_values(array_filter($arrPeriodos,function($e)use($idPeriodo){return $idPeriodo == $e['dias'];}));
                if(!empty($arrPeriodo)){
                    $periodo = $arrPeriodo[0]['codigo'];
                }
                $tipoCesantias = $this->selectTempCampoFuncionario("TPCESANTIAS",$idFuncionario);
                $nivelArl = $this->selectTempCampoFuncionario("NIVELARL",$idFuncionario);
                $unidadEjecutora = intval($this->selectTempCampoFuncionario("UNIEJECUTORA",$idFuncionario));
                $pttoSalario = strval($this->selectTempCampoFuncionario("TPRESUPUESTO",$idFuncionario));
                $saludFuncionario = floatval($this->selectTempCampoFuncionario("PORSF",$idFuncionario));
                $pensionFuncionario = floatval($this->selectTempCampoFuncionario("PORPF",$idFuncionario));
                $seccionPresupuestal = intval($this->selectTempCampoFuncionario("SECCIONPRESUP",$idFuncionario));
                $pttoOtros = strval($this->selectTempCampoFuncionario("TPRESUPUESTOOTROS",$idFuncionario));
                $vinculacion = strval($this->selectTempCampoFuncionario("TVINCULACION",$idFuncionario));
                $jornada = '{"jornada":"1","horario":{"inicio":"08:00","final":"17:30","inicio_sabado":"08:00","final_sabado":"13:00","inicio_domingo":"","final_domingo":""}}';
                $contrato = '{"obligaciones":[{"type":1,"value":"Pagar en la forma pactada el monto equivalente a la remuneración."},{"type":1,"value":"Realizar la afiliación y correspondiente aporte a parafiscales."},{"type":1,"value":"Dotar al TRABAJADOR de los elementos de trabajo necesarios para el correcto desempeño de la gestión contratada."},{"type":1,"value":"Las obligaciones especiales enunciadas en los artículos 56 y 57 del Código Sustantivo del Trabajo."},{"type":2,"value":"Cumplir a cabalidad con el objeto del contrato, en la forma convenida."},{"type":2,"value":"Las obligaciones especiales enunciadas en los artículos 56 y 58 del Código Sustantivo del Trabajo."}],"clausulas":[{"clausula":"Terminación unilateral del contrato. El presente contrato se podrá terminar unilateralmente y sin indemnización alguna, por cualquiera de las partes, siempre y cuando se configure algunas de las situaciones previstas en el artículo 62 del Código Sustantivo del Trabajo o haya incumplimiento grave de alguna cláusula prevista en el contrato de trabajo. Se considera incumplimiento grave el desconocimiento de las obligaciones o prohibiciones previstas en el contrato","paragrafo":""},{"clausula":"Este contrato ha sido redactado estrictamente de acuerdo con la ley y la jurisprudencia y será interpretado de buena fe y en consonancia con el Código Sustantivo del Trabajo  cuyo objeto, definido en su artículo 1º, es lograr la justicia en las relaciones entre empleadores y trabajadores dentro de un espíritu de coordinación económica y equilibrio social.","paragrafo":""},{"clausula":"El presente contrato reemplaza en su integridad y deja sin efecto alguno cualquiera otro contrato verbal o escrito celebrado por las partes con anterioridad. Las modificaciones que se acuerden al presente contrato se anotarán a continuación de su texto.","paragrafo":""}]}';
                $requestContrato = $this->insertContrato($tercero,$tipoContrato,$periodo,$fecha,"0000-00-00 00:00:00",$banco,$cuenta,$cargo,$jornada,$contrato,0,$cargoRequisitos);

                if($requestContrato=="existe"){
					$idContrato = $this->select("SELECT id FROM hum_contratos WHERE tercero = '$tercero'")['id'];
                    $sqlNovedad = "INSERT INTO hum_contratos_novedades(contrato_id, tipo,fecha) VALUES(?,?,?)";
                    $requestNovedad = $this->insert($sqlNovedad,[$idContrato,1,$fecha]);
                    $this->update("UPDATE hum_contratos SET estado =? WHERE id = $idContrato",["RENOVADO"]);
                    $sql = "INSERT INTO hum_contratos_renovacion(id,tercero,tipo,periodo,fecha_inicial,fecha_final,banco,cuenta,cargo_id,jornada,contrato,novedad_id)
                    VALUES(?,?,?,?,?,?,?,?,?,?,?,?)";
                    $this->insert($sql,[
                        $idFuncionario,
                        $tercero,
                        $tipoContrato,
                        $periodo,
                        $fecha,
                        "0000-00-00 00:00:00",
                        $cuenta,
                        $banco,
                        $cargo,
                        $jornada,
                        $contrato,
                        $requestNovedad
                    ]);
                    if(!empty($requisitos)){
                        foreach ($requisitos as $e) {
                            $tipos = $e['tipos'];
                            foreach ($tipos as $f) {
                                $req = $f['requisitos'];
                                foreach ($req as $g) {
                                    if($g['is_checked'] && $f['is_checked'] && $e['is_checked']){
                                        $sqlReq = "INSERT INTO hum_contratos_requisitos(cargo_id,requisito_id,documento_id,tipo,estado) VALUES(?,?,?,?,?)";
                                        $this->insert($sqlReq,[$cargo,$g['id'],$requestNovedad,"RENOVADO",1]);
                                    }
                                }
                            }
                        }
                    }
                }else{
                    $this->insertPtto($requestContrato,$vinculacion,$pttoSalario,$pttoOtros,$unidadEjecutora,$seccionPresupuestal,$centro);
                    $this->insertSeguridad($requestContrato,$eps,$arl,$afp,$cesantias,$fecha,$fecha,$fecha,$fecha,$tipoCesantias,$nivelArl,$saludFuncionario,$pensionFuncionario);
                }
            }
        }
        public function selectTempCampoFuncionario($campo,$id){
            $sql = "SELECT COALESCE(h.descripcion, '') AS descripcion
                    FROM hum_funcionarios AS h
                    WHERE h.item = '$campo'
                    AND h.estado = 'S'
                    AND h.codfun = $id
                    AND h.fechain = (
                        SELECT MAX(f.fechain)
                        FROM hum_funcionarios f
                        WHERE f.codfun = $id
                            AND f.item = '$campo'
                            AND f.estado = 'S'
                    )";
            $request = $this->select($sql);
            return $request['descripcion'];
        }
        public function insertContrato(string $strTercero, int $intTipoContrato, int $intPeriodoId, string $strFechaInicio, string $strFechaFinal,
            string $strBancoId, string $strCuentaBanco, int $intCargoId, string $strJornada, string $strContrato, int $intAutomatico,array $requisitos){

            $this->strTercero = $strTercero;
            $this->intTipoContrato = $intTipoContrato;
            $this->intPeriodoId = $intPeriodoId;
            $this->strFechaInicio = $strFechaInicio;
            $this->strFechaFinal = $strFechaFinal;
            $this->strBancoId = $strBancoId;
            $this->strCuentaBanco = $strCuentaBanco;
            $this->intCargoId = $intCargoId;
            $this->strJornada = $strJornada;
            $this->strContrato = $strContrato;
            $this->intAutomatico = $intAutomatico;
            $sql = "SELECT * FROM hum_contratos WHERE tercero = '$this->strTercero'";
            $request = $this->select($sql);
            $return ="";
            if(empty($request)){
                $sql = "INSERT INTO hum_contratos(tercero,tipo,periodo,fecha_inicial,fecha_final,banco,cuenta,cargo_id,jornada,contrato,automatico)
                VALUES(?,?,?,?,?,?,?,?,?,?,?)";
                $request = $this->insert($sql,[
                    $this->strTercero,
                    $this->intTipoContrato,
                    $this->intPeriodoId,
                    $this->strFechaInicio,
                    $this->strFechaFinal,
                    $this->strBancoId,
                    $this->strCuentaBanco,
                    $this->intCargoId,
                    $this->strJornada,
                    $this->strContrato,
                    $this->intAutomatico,
                ]);
                $return = $request;
                if(!empty($requisitos)){
                    foreach ($requisitos as $e) {
                        $tipos = $e['tipos'];
                        foreach ($tipos as $f) {
                            $req = $f['requisitos'];
                            foreach ($req as $g) {
                                if($g['is_checked'] && $f['is_checked'] && $e['is_checked']){
                                    $sqlReq = "INSERT INTO hum_contratos_requisitos(cargo_id,requisito_id,documento_id,tipo,estado) VALUES(?,?,?,?,?)";
                                    $this->insert($sqlReq,[$this->intCargoId,$g['id'],$return,"CONTRATO",$g['checked']]);
                                }
                            }
                        }
                    }
                }
            }else{
                $return = "existe";
            }
            return $return;
        }
        public function insertPtto(int $intConsecutivo,string $strVinculacion,string $strPttoSalarios,string $strPttoPagos,int $intUnidad,int $intSeccion,
        string $strCentro,string $strPttoPara,string $strFuente,int $intIdbpin,string $strIndicador){
            $this->intConsecutivo = $intConsecutivo;
            $this->strVinculacion = $strVinculacion;
            $this->strPttoSalarios = $strPttoSalarios;
            $this->strPttoPagos = $strPttoPagos;
            $this->strPttoPara = $strPttoPara;
            $this->intUnidad = $intUnidad;
            $this->intSeccion = $intSeccion;
            $this->strCentro = $strCentro;
            $this->strFuente = $strFuente;
            $this->intIdbpin = $intIdbpin;
            $this->strIndicador = $strIndicador;
            $sql = "INSERT INTO hum_contratos_ptto(contrato_id,vinculacion,ptto_salario,ptto_pago,unidad_id,seccion_id,cc_id,ptto_para,fuente,id_bpin,indicador) VALUES(?,?,?,?,?,?,?,?,?,?,?)";
            $request = $this->insert($sql,[
                $this->intConsecutivo,
                $this->strVinculacion,
                $this->strPttoSalarios,
                $this->strPttoPagos,
                $this->intUnidad,
                $this->intSeccion,
                $this->strCentro,
                $this->strPttoPara,
                $this->strFuente,
                $this->intIdbpin,
                $this->strIndicador
            ]);
            return $request;
        }
        public function insertSeguridad(int $intConsecutivo,string $strTerceroEps,string $strTerceroArl,string $strTerceroAfp,string $strTerceroFondo,string $strFechaEps,string $strFechaArl,string $strFechaAfp,
        string $strFechaFondo,string $intCesantias,int $intRiesgo,float $floatSalud,float $floatPension){
            $this->intConsecutivo = $intConsecutivo;
            $this->strTerceroEps = $strTerceroEps;
            $this->strTerceroArl = $strTerceroArl;
            $this->strTerceroAfp = $strTerceroAfp;
            $this->strTerceroFondo = $strTerceroFondo;
            $this->strFechaEps = $strFechaEps;
            $this->strFechaArl = $strFechaArl;
            $this->strFechaAfp = $strFechaAfp;
            $this->strFechaFondo = $strFechaFondo;
            $this->intCesantias = $intCesantias;
            $this->intRiesgo = $intRiesgo;
            $this->floatSalud = $floatSalud;
            $this->floatPension = $floatPension;
            $sql = "INSERT INTO hum_contratos_seguridad(contrato_id,tercero_eps,tercero_arl,tercero_afp,tercero_fondo,
            fecha_eps,fecha_arl,fecha_afp,fecha_fondo,cesantias,riesgo_id,salud,pension) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?)";
            $request = $this->insert($sql,[
                $this->intConsecutivo,
                $this->strTerceroEps,
                $this->strTerceroArl,
                $this->strTerceroAfp,
                $this->strTerceroFondo,
                $this->strFechaEps,
                $this->strFechaArl,
                $this->strFechaAfp,
                $this->strFechaFondo,
                $this->intCesantias,
                $this->intRiesgo,
                $this->floatSalud,
                $this->floatPension,
            ]);
            return $request;
        }
        public function updateContrato(int $intConsecutivo,string $strTercero, int $intAutomatico, array $requisitos,int $intCargoId){
            $this->intConsecutivo = $intConsecutivo;
            $this->strTercero = $strTercero;
            $this->intAutomatico = $intAutomatico;
            $this->intCargoId = $intCargoId;
            $sql = "SELECT * FROM hum_contratos WHERE id = $this->intConsecutivo";
            $request = $this->select($sql);
            if(!empty($request)){
                $sql = "UPDATE hum_contratos SET tercero=?,automatico=? WHERE id = $this->intConsecutivo";
                $request = $this->update($sql,[$this->strTercero,$this->intAutomatico]);
                if(!empty($requisitos)){
                    $this->delete("DELETE FROM hum_contratos_requisitos WHERE cargo_id = $this->intCargoId
                    AND documento_id = $this->intConsecutivo AND tipo = 'CONTRATO'");
                    foreach ($requisitos as $e) {
                        $tipos = $e['tipos'];
                        foreach ($tipos as $f) {
                            $req = $f['requisitos'];
                            foreach ($req as $g) {
                                if($g['is_checked'] && $f['is_checked'] && $e['is_checked']){
                                    $sqlReq = "INSERT INTO hum_contratos_requisitos(cargo_id,requisito_id,documento_id,tipo,estado) VALUES(?,?,?,?,?)";
                                    $this->insert($sqlReq,[$this->intCargoId,$g['id'],$this->intConsecutivo,"CONTRATO",$g['checked']]);
                                }
                            }
                        }
                    }
                }
            }
            return $request;
        }
        public function insertRuta($id,$ruta){
            $sql = "INSERT INTO hum_contratos_anexos(tipo,documento_id,ruta) VALUES(?,?,?)";
            $this->intConsecutivo = $id;
            $this->strRuta = $ruta;
            $request = $this->insert($sql,["CONTRATO",$this->intConsecutivo,$this->strRuta]);
            return $request;
        }
        public function updateRuta($id,$ruta){
            $this->intConsecutivo = $id;
            $this->strRuta = $ruta;
            $sql = "SELECT * FROM hum_contratos_anexos WHERE documento_id = $id";
            $request = $this->select($sql);
            if(!empty($request)){
                $sql = "UPDATE  hum_contratos_anexos SET ruta = ? WHERE documento_id =$this->intConsecutivo";
                $request = $this->update($sql,[$this->strRuta]);
            }else{
                $request = $this->insertRuta($id,$ruta);
            }
            return $request;
        }
    }
?>
