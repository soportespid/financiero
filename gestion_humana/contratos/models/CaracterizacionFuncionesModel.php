<?php
    require_once '../../../Librerias/core/Helpers.php';
    require_once '../../../Librerias/core/Mysql.php';
    session_start();
    /*ini_set('display_errors', '1');
    ini_set('display_startup_errors', '1');
    error_reporting(E_ALL);*/
    class CaracterizacionFuncionesModel extends Mysql{
        private $intConsecutivo;
        private $strNombre;
        private $intOrden;

        function __construct(){
            parent::__construct();
        }

        public function insertData(string $strNombre,int $intOrden){
            $this->strNombre = $strNombre;
            $this->intOrden = $intOrden;
            $sql = "SELECT * FROM hum_caracterizacion_funciones WHERE nombre = '$this->strNombre'";
            $request = $this->select_all($sql);
            $return = "";
            if(empty($request)){
                $sql="INSERT INTO hum_caracterizacion_funciones(nombre,orden) VALUES(?,?)";
                $return = $this->insert($sql,[$this->strNombre,$this->intOrden]);
            }else{
                $return = "existe";
            }
            return $return;
        }
        public function updateData(int $intConsecutivo,string $strNombre,int $intOrden){
            $this->strNombre = $strNombre;
            $this->intConsecutivo = $intConsecutivo;
            $this->intOrden = $intOrden;
            $sql = "SELECT * FROM hum_caracterizacion_funciones WHERE nombre = '$this->strNombre' AND id != $this->intConsecutivo";
            $request = $this->select_all($sql);
            $return = "";
            if(empty($request)){
                $sql="UPDATE hum_caracterizacion_funciones SET nombre=?,orden=? WHERE id = $this->intConsecutivo";
                $return = $this->update($sql,[$this->strNombre,$this->intOrden]);
            }else{
                $return = "existe";
            }
            return $return;
        }
        public function updateStatus(int $intConsecutivo,string $estado){
            $this->intConsecutivo = $intConsecutivo;
            $sql = "UPDATE hum_caracterizacion_funciones SET estado = '$estado' WHERE id = $this->intConsecutivo";
            $request = $this->update($sql,[$estado]);
            return $request;
        }
        public function selectEdit($intConsecutivo){
            $this->intConsecutivo = $intConsecutivo;
            $sql = "SELECT * FROM hum_caracterizacion_funciones WHERE id = $this->intConsecutivo";
            $request = $this->select($sql);
            return $request;
        }
        public function selectSearch(){
            $sql = "SELECT * FROM hum_caracterizacion_funciones ORDER BY id DESC";
            $request = $this->select_all($sql);
            $total = count($request);
            for ($i=0; $i < $total; $i++) {
                $request[$i]['is_status'] = $request[$i]['estado'] =="S"? 1 : 0;
            }
            return $request;
        }
    }
?>
