<?php
    require_once '../../../Librerias/core/Helpers.php';
    require_once '../../../Librerias/core/Mysql.php';
    session_start();
    /*ini_set('display_errors', '1');
    ini_set('display_startup_errors', '1');
    error_reporting(E_ALL);*/
    class ContratoNovedadModel extends Mysql{
        private $intConsecutivo;
        private $intIdNovedad;
        private $intIdContrato;
        private $strMotivo;
        private $intIdMotivo;
        private $strRuta;

        function __construct(){
            parent::__construct();
        }

        public function selectMotivos(){
            $sql = "SELECT * FROM hum_contratos_motivos";
            $request = $this->select_all($sql);
            return $request;
        }
        public function selectContratos(){
            $sql = "SELECT
            cab.id,
            cab.tercero,
            cab.tipo,
            cab.periodo,
            DATE_FORMAT(cab.fecha_inicial,'%Y-%m-%d') as fecha_inicial,
            DATE_FORMAT(cab.fecha_final,'%Y-%m-%d') as fecha_final,
            cab.banco,
            cab.cuenta,
            cab.cargo_id,
            cab.jornada,
            cab.contrato,
            cab.estado,
            c.nombrecargo as nombre_cargo,
            s.valor as salario,
            s.id_nivel as escala,
            s.nombre as nivel_salarial,
            CASE WHEN t.razonsocial IS NULL OR t.razonsocial = ''
            THEN CONCAT(t.nombre1,' ',t.nombre2,' ',t.apellido1,' ',t.apellido2)
            ELSE t.razonsocial END AS nombre_tercero,
            p.nombre as nombre_periodo,
            t.direccion,
            t.tipodoc
            FROM hum_contratos cab
            LEFT JOIN terceros t ON cab.tercero = t.cedulanit
            LEFT JOIN planaccargos c ON cab.cargo_id = c.codcargo
            LEFT JOIN humnivelsalarial s ON s.id_nivel=c.clasificacion
            LEFT JOIN humperiodos p ON cab.periodo = p.id_periodo
            ORDER BY cab.id DESC";
            $request = $this->select_all($sql);
            if(!empty($request)){
                $total = count($request);
                for ($i=0; $i < $total ; $i++) {
                    $idCargo = $request[$i]['cargo_id'];
                    $idContrato = $request[$i]['id'];
                    $sqlRequisitos = "SELECT hf.id as funciones_id, ht.id as tipo_id, hr.id as requisito_id,pr.estado
                    FROM hum_contratos_requisitos pr
                    INNER JOIN hum_caracterizacion_requisito hr ON hr.id = pr.requisito_id
                    INNER JOIN hum_caracterizacion_tipo ht ON ht.id = hr.tipo_id
                    INNER JOIN hum_caracterizacion_funciones hf ON hf.id = ht.funcion_id
                    WHERE hr.estado = 'S' AND ht.estado = 'S' AND hf.estado = 'S' AND
                    pr.documento_id = $idContrato AND pr.cargo_id = $idCargo AND pr.tipo ='CONTRATO'";

                    $sqlCuentas = "SELECT ban.codigo, ban.nombre,te.numero_cuenta as cuenta
                    FROM teso_cuentas_terceros te
                    LEFT JOIN hum_bancosfun ban ON ban.codigo =te.codigo_banco
                    LEFT JOIN terceros t ON te.id_tercero = t.id_tercero
                    WHERE t.cedulanit = {$request[$i]['tercero']}";

                    $request[$i]['cuentas']= $this->select_all($sqlCuentas);
                    $request[$i]['jornada'] = json_decode($request[$i]['jornada'],JSON_UNESCAPED_UNICODE);
                    $request[$i]['contrato'] = json_decode($request[$i]['contrato'],JSON_UNESCAPED_UNICODE);
                    $request[$i]['requisitos'] = $this->select_all($sqlRequisitos);
                }
            }
            return $request;
        }
        public function selectCargos(){
            $sql = "SELECT
            c.codcargo as codigo,
            c.nombrecargo as nombre,
            c.clasificacion,
            s.valor,
            s.id_nivel as escala,
            s.nombre as nivel_salarial
            FROM planaccargos c
            INNER JOIN humnivelsalarial s ON s.id_nivel=c.clasificacion
            WHERE c.estado = 'S' AND s.estado = 'S' ORDER BY c.codcargo DESC";
            $request = $this->select_all($sql);
            $dataRequisitos = $this->selectRequisitos();
            if(!empty($request)){
                $totalCargos = count($request);
                for ($i=0; $i < $totalCargos ; $i++) {
                    $id = $request[$i]['codigo'];
                    $sql = "SELECT hf.id as funciones_id, ht.id as tipo_id, hr.id as requisito_id
                    FROM planaccargos_requisitos pr
                    INNER JOIN hum_caracterizacion_requisito hr ON hr.id = pr.requisito_id
                    INNER JOIN hum_caracterizacion_tipo ht ON ht.id = hr.tipo_id
                    INNER JOIN hum_caracterizacion_funciones hf ON hf.id = ht.funcion_id
                    WHERE hr.estado = 'S' AND ht.estado = 'S' AND hf.estado = 'S' AND pr.cargo_id = $id";
                    $arrCargoRequisitos = $this->select_all($sql);
                    if(!empty($arrCargoRequisitos)){
                        for ($j=0; $j < count($dataRequisitos) ; $j++) {
                            $areas = $dataRequisitos[$j];
                            $tipos = $areas['tipos'];
                            for ($k=0; $k < count($tipos) ; $k++) {
                                $tipo = $tipos[$k];
                                $requisitos = $tipo['requisitos'];
                                for ($l=0; $l < count($requisitos)  ; $l++) {
                                    $requisito = $requisitos[$l];
                                    $cargoReq = array_filter($arrCargoRequisitos,function($e)use($requisito,$areas,$tipo){
                                        return $requisito['id'] == $e['requisito_id'] && $tipo['id'] == $e['tipo_id']
                                        && $areas['id'] == $e['funciones_id'];
                                    });
                                    $requisito['is_checked'] = !empty($cargoReq) ? 1 : 0;
                                    $requisito['checked'] = 1;
                                    $requisitos[$l] = $requisito;
                                }
                                $tipos[$k]['requisitos'] = $requisitos;
                                $tipos[$k]['is_checked'] = !empty(array_filter($requisitos,function($e){return $e['is_checked'] == 1;})) ? 1 : 0;
                            }
                            $dataRequisitos[$j]['tipos'] = $tipos;
                            $dataRequisitos[$j]['is_checked'] = !empty(array_filter($tipos,function($e){return $e['is_checked'] == 1;})) ? 1 : 0;
                        }
                    }
                    $request[$i]['requisitos'] = $dataRequisitos;
                }
            }
            return $request;
        }
        public function selectRequisitos(){
            $sql = "SELECT * FROM hum_caracterizacion_funciones ORDER BY orden";
            $request = $this->select_all($sql);
            $arrData = [];
            if(!empty($request)){
                $total = count($request);
                for ($i=0; $i < $total ; $i++) {
                    $e = $request[$i];
                    $idFuncion = $e['id'];
                    $sqlTipos = "SELECT * FROM hum_caracterizacion_tipo WHERE funcion_id = $idFuncion ORDER BY orden ";
                    $arrTipos = $this->select_all($sqlTipos);
                    if(!empty($arrTipos)){
                        $totalTipos = count($arrTipos);
                        for ($j=0; $j < $totalTipos ; $j++) {
                            $idTipo = $arrTipos[$j]['id'];
                            $sqlRequisitos = "SELECT * FROM hum_caracterizacion_requisito WHERE tipo_id = $idTipo";
                            $arrRequisitos = $this->select_all($sqlRequisitos);
                            $totalRequisitos = count($arrRequisitos);
                            for ($k=0; $k < $totalRequisitos ; $k++) {
                                $arrRequisitos[$k]['is_checked'] = 0;
                            }
                            $arrTipos[$j]['requisitos'] = $arrRequisitos;
                            $arrTipos[$j]['is_checked'] = 0;
                        }
                        $arrTipos = array_values(array_filter($arrTipos,function($e){return !empty($e['requisitos']);}));
                        $request[$i]['tipos'] = $arrTipos;
                        $request[$i]['is_checked'] = 0;
                        array_push($arrData,$request[$i]);
                    }
                }
            }
            return $arrData;
        }
        public function selectBancos(){
            $sql = "SELECT codigo, nombre FROM hum_bancosfun WHERE estado='S' ORDER BY CONVERT(codigo, SIGNED INTEGER)";
            $request = $this->select_all($sql);
            return $request;
        }
        public function selectPeriodos(){
            $sql = "SELECT id_periodo as codigo,nombre,dias FROM humperiodos ORDER BY id_periodo";
            $request = $this->select_all($sql);
            return $request;
        }
        public function insertData(int $intIdNovedad,int $intIdMotivo,array $arrContrato, array $arrData){
            $this->intIdNovedad = $intIdNovedad;
            $this->intIdContrato = $arrContrato['id'];
            $this->intIdMotivo = $intIdMotivo;
            $requisitos = $arrData['cargo']['requisitos'];
            $strJornada = json_encode(array("jornada"=>$arrData['jornada'],"horario"=>$arrData['horario']),JSON_UNESCAPED_UNICODE);
            $strContrato = json_encode(array("obligaciones"=>$arrData['obligaciones'],"clausulas"=>$arrData['clausulas']),JSON_UNESCAPED_UNICODE);
            $strEstado = "";
            $return ="";
            if($this->intIdNovedad == 1){
                $sql = "SELECT * FROM hum_contratos WHERE id = $this->intIdContrato AND estado = 'RENOVADO'";
                $strEstado = "RENOVADO";
            }else if($this->intIdNovedad == 2){
                $sql = "SELECT * FROM hum_contratos WHERE id = $this->intIdContrato AND estado = 'TERMINADO'";
                $strEstado = "TERMINADO";
            }
            $request = $this->select($sql);
            if(empty($request)){
                $sql = "INSERT INTO hum_contratos_novedades(contrato_id, tipo,motivo_id) VALUES(?,?,?)";
                $request = $this->insert($sql,[$this->intIdContrato,$this->intIdNovedad,$intIdMotivo]);
                $this->update("UPDATE hum_contratos SET estado =? WHERE id = $this->intIdContrato",[$strEstado]);
                if($this->intIdNovedad == 1){
                    $sql = "INSERT INTO hum_contratos_renovacion(tercero,tipo,periodo,fecha_inicial,fecha_final,banco,cuenta,cargo_id,jornada,contrato,novedad_id)
                    VALUES(?,?,?,?,?,?,?,?,?,?,?)";
                    $this->insert($sql,[
                        $arrData['tercero']['codigo'],
                        $arrData['contrato'],
                        $arrData['periodo'],
                        $arrData['fecha_ingreso'],
                        $arrData['fecha_terminacion'],
                        $arrData['cuenta_bancaria']['banco'],
                        $arrData['cuenta_bancaria']['numero'],
                        $arrData['cargo']['codigo'],
                        $strJornada,
                        $strContrato,
                        $request
                    ]);
                    if(!empty($requisitos)){
                        foreach ($requisitos as $e) {
                            $tipos = $e['tipos'];
                            foreach ($tipos as $f) {
                                $req = $f['requisitos'];
                                foreach ($req as $g) {
                                    if($g['is_checked'] && $f['is_checked'] && $e['is_checked']){
                                        $sqlReq = "INSERT INTO hum_contratos_requisitos(cargo_id,requisito_id,documento_id,tipo,estado) VALUES(?,?,?,?,?)";
                                        $this->insert($sqlReq,[$arrData['cargo']['codigo'],$g['id'],$request,"RENOVADO",$g['checked']]);
                                    }
                                }
                            }
                        }
                    }
                }
                $return = $request;
            }else{
                $return = "existe";
            }
            return $return;
        }
        public function insertRuta($id,$ruta){
            $sql = "INSERT INTO hum_contratos_anexos(tipo,documento_id,ruta) VALUES(?,?,?)";
            $this->intConsecutivo = $id;
            $this->strRuta = $ruta;
            $request = $this->insert($sql,["RENOVADO",$this->intConsecutivo,$this->strRuta]);
            return $request;
        }
        public function updateRuta($id,$ruta){
            $this->intConsecutivo = $id;
            $this->strRuta = $ruta;
            $sql = "SELECT * FROM hum_contratos_anexos WHERE documento_id = $id";
            $request = $this->select($sql);
            if(!empty($request)){
                $sql = "UPDATE  hum_contratos_anexos SET ruta = ? WHERE documento_id =$this->intConsecutivo";
                $request = $this->update($sql,[$this->strRuta]);
            }else{
                $request = $this->insertRuta($id,$ruta);
            }
            return $request;
        }
        public function selectEdit(int $id){
            $this->intConsecutivo = $id;
            $sql = "SELECT * FROM hum_contratos_novedades WHERE id = $this->intConsecutivo";
            $request = $this->select($sql);
            if(!empty($request)){
                if($request['tipo']==1){
                    $sqlRev = "SELECT
                    cab.id,
                    cab.tercero,
                    cab.tipo,
                    cab.periodo,
                    DATE_FORMAT(cab.fecha_inicial,'%Y-%m-%d') as fecha_inicial,
                    DATE_FORMAT(cab.fecha_final,'%Y-%m-%d') as fecha_final,
                    cab.banco,
                    cab.cuenta,
                    cab.cargo_id,
                    cab.novedad_id,
                    cab.jornada,
                    cab.contrato,
                    c.nombrecargo as nombre_cargo,
                    s.valor as salario,
                    s.id_nivel as escala,
                    s.nombre as nivel_salarial,
                    CASE WHEN t.razonsocial IS NULL OR t.razonsocial = ''
                    THEN CONCAT(t.nombre1,' ',t.nombre2,' ',t.apellido1,' ',t.apellido2)
                    ELSE t.razonsocial END AS nombre_tercero,
                    p.nombre as nombre_periodo,
                    t.direccion,
                    t.tipodoc
                    FROM hum_contratos_renovacion cab
                    LEFT JOIN terceros t ON cab.tercero = t.cedulanit
                    LEFT JOIN planaccargos c ON cab.cargo_id = c.codcargo
                    LEFT JOIN humnivelsalarial s ON s.id_nivel=c.clasificacion
                    LEFT JOIN humperiodos p ON cab.periodo = p.id_periodo
                    WHERE cab.novedad_id = $this->intConsecutivo";
                    $requestRev = $this->select($sqlRev);
                    if(!empty($requestRev)){
                        $idCargo = $requestRev['cargo_id'];
                        $sqlRuta = "SELECT ruta FROM hum_contratos_anexos WHERE documento_id = $this->intConsecutivo AND tipo = 'RENOVADO'";

                        $sqlRequisitos = "SELECT hf.id as funciones_id, ht.id as tipo_id, hr.id as requisito_id,pr.estado
                        FROM hum_contratos_requisitos pr
                        INNER JOIN hum_caracterizacion_requisito hr ON hr.id = pr.requisito_id
                        INNER JOIN hum_caracterizacion_tipo ht ON ht.id = hr.tipo_id
                        INNER JOIN hum_caracterizacion_funciones hf ON hf.id = ht.funcion_id
                        WHERE hr.estado = 'S' AND ht.estado = 'S' AND hf.estado = 'S' AND
                        pr.documento_id = $this->intConsecutivo AND pr.cargo_id = $idCargo AND pr.tipo ='RENOVADO'";
                        $requestRuta = $this->select($sqlRuta);

                        $sqlCuentas = "SELECT ban.codigo, ban.nombre,te.numero_cuenta as cuenta
                        FROM teso_cuentas_terceros te
                        LEFT JOIN hum_bancosfun ban ON ban.codigo =te.codigo_banco
                        LEFT JOIN terceros t ON te.id_tercero = t.id_tercero
                        WHERE t.cedulanit = {$requestRev['tercero']}";

                        $requestRev['cuentas']= $this->select_all($sqlCuentas);
                        $requestRev['anexo'] = !empty($requestRuta) ? $requestRuta['ruta'] : "";
                        $requestRev['jornada'] = json_decode($requestRev['jornada'],JSON_UNESCAPED_UNICODE);
                        $requestRev['contrato'] = json_decode($requestRev['contrato'],JSON_UNESCAPED_UNICODE);
                        $requestRev['id'] = $request['contrato_id'];
                        $requestRev['requisitos'] = $this->select_all($sqlRequisitos);
                    }
                    $request['detalle'] =$requestRev;
                }
            }
            return $request;
        }
        public function selectSearch(){
            $arrNovedad = [
                1=>"RENOVAR",
                2=>"RETIRAR"
            ];
            $sql = "SELECT *, DATE_FORMAT(fecha,'%d/%m/%Y') as fecha FROM hum_contratos_novedades ORDER BY id DESC";
            $request = $this->select_all($sql);
            if(!empty($request)){
                $total = count($request);
                for ($i=0; $i < $total; $i++) {
                    $request[$i]['novedad'] = $arrNovedad[$request[$i]['tipo']];
                }
            }
            return $request;
        }
    }
?>
