const URL ='gestion_humana/tablas_parafiscales/crear/hum-tablasParafiscales.php';

var app = new Vue({
    el:"#myapp",
    data() {
        return {
            isLoading:false,
            isProvisiona:false,
            isSalud:false,
            isPension:false,
            isArl:false,
            isFiscales:false,
            txtConsecutivo:0,
            txtNombre:"",
            txtPorcentaje:"",
            arrFuncionamiento:[],
            arrTempFuncionamiento:[],
            arrInversion:[],
            arrTempInversion:[],
            arrComercio:[],
            arrTempComercio:[],
            arrVariables:[],
            arrSelVariables:[],
            selectVariable:"N/A",
            selectTipo:0,
            selectFuncionamiento:0,
            selectTempFuncionamiento:0,
            selectInversion:0,
            selectTempInversion:0,
            selectComercio:0,
            selectTempComercio:0,
            txtPorcentaje:"",
        }
    },
    mounted() {
        this.getData();
    },
    methods: {
        getData: async function(){
            const formData = new FormData();
            formData.append("action","get");
            this.isLoading = true;
            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();
            this.arrFuncionamiento = objData.conceptos.funcionamiento;
            this.arrTempFuncionamiento = objData.conceptos.temp_funcionamiento;
            this.arrInversion = objData.conceptos.inversion;
            this.arrTempInversion = objData.conceptos.temp_inversion;
            this.arrComercio = objData.conceptos.comercio;
            this.arrTempComercio = objData.conceptos.temp_comercio;
            this.arrVariables = objData.variables.filter(function(e){return e.estado=="S"});
            this.txtConsecutivo = objData.consecutivo;
            this.isLoading = false;
        },
        addVariable:function(){
            const vueContext = this;
            if(vueContext.selectFuncionamiento == 0 || vueContext.selectTempFuncionamiento == 0
                || vueContext.selectInversion == 0 || vueContext.selectTempInversion == 0
             ){
                Swal.fire("Error","Debe asignar variables de pago","error");
                return false;
            }
            if(vueContext.arrSelVariables.length > 0){
                let flag = false;
                for (let i = 0; i < vueContext.arrSelVariables.length; i++) {
                    const e = vueContext.arrSelVariables[i];
                    if(e.funcionamiento.codigo == vueContext.selectFuncionamiento && e.temp_funcionamiento.codigo ==vueContext.selectTempFuncionamiento
                        && e.inversion.codigo == vueContext.selectInversion && e.temp_inversion.codigo == vueContext.selectTempInversion
                        && e.sector == vueContext.selectVariable
                     ){
                        let flagComercio = e.comercio ? true : false;
                        let flagTempComercio = e.temp_comercio ? true : false;
                        if(flagComercio){
                            if(e.comercio.codigo == vueContext.selectComercio){
                                flag = true;
                                break;
                            }
                        }
                        if(flagTempComercio){
                            if(e.temp_comercio.codigo == vueContext.selectTempComercio){
                                flag = true;
                                break;
                            }
                        }
                        flag = true;
                        break;
                    }
                }
                if(flag){
                    Swal.fire("Error","Esta parametrización ya fue agregada, intente con otra.","error");
                    return false;
                }
            }
            this.arrSelVariables.push(
                {
                    funcionamiento:this.arrFuncionamiento.filter(function(e){return e.codigo == vueContext.selectFuncionamiento})[0],
                    temp_funcionamiento:this.arrTempFuncionamiento.filter(function(e){return e.codigo == vueContext.selectTempFuncionamiento})[0],
                    inversion:this.arrInversion.filter(function(e){return e.codigo == vueContext.selectInversion})[0],
                    temp_inversion:this.arrTempInversion.filter(function(e){return e.codigo == vueContext.selectTempInversion})[0],
                    comercio:this.arrComercio.filter(function(e){return e.codigo == vueContext.selectComercio})[0],
                    temp_comercio:this.arrTempComercio.filter(function(e){return e.codigo == vueContext.selectTempComercio})[0],
                    sector:vueContext.selectVariable
                }
            );
        },
        delVariable:function(index){
            this.arrSelVariables.splice(index,1);
        },
        save: function(){
            const vueContext = this;
            if(this.txtNombre =="" || this.selectTipo == 0 ||  this.txtPorcentaje == ""){
                Swal.fire("Error","Todos los campos con (*) son obligatorios","error");
                return false;
            }
            if(vueContext.arrSelVariables.length == 0){
                Swal.fire("Error","Debe agregar al menos una variable parametrizada","error");
                return false;
            }
            const data = {
                nombre:this.txtNombre,
                consecutivo:this.txtConsecutivo,
                porcentaje:this.txtPorcentaje,
                tipo:this.selectTipo,
                variables:this.arrSelVariables
            }
            Swal.fire({
                title:"¿Estás segur@ de guardar?",
                text:"",
                icon: 'warning',
                showCancelButton:true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText:"Sí, guardar",
                cancelButtonText:"No, cancelar"
            }).then(async function(result){
                if(result.isConfirmed){
                    const formData = new FormData();
                    formData.append("action","save");
                    formData.append("data",JSON.stringify(data));
                    vueContext.isLoading = true;
                    const response = await fetch(URL,{method:"POST",body:formData});
                    const objData = await response.json();
                    if(objData.status){
                        Swal.fire("Guardado",objData.msg,"success");
                        setTimeout(function(){
                            window.location.reload();
                        },2000);
                    }else{
                        Swal.fire("Error",objData.msg,"error");
                    }
                    vueContext.isLoading = false;
                }
            });

        },
    },
    computed:{

    }
})
