<?php
    require_once '../../../comun.inc';
    require '../../../funciones.inc';
    require '../../../funcionesSP.inc.php';
    /*ini_set('display_errors', '1');
    ini_set('display_startup_errors', '1');
    error_reporting(E_ALL);*/
    session_start();
    //dep($_POST);exit;
    if($_POST){
        $obj = new Plantilla();
        if($_POST['action']=="get"){
            $obj->getData();
        }else if($_POST['action']=="change"){
            $obj->changeData();
        }
    }

    class Plantilla{
        private $linkbd;
        private $arrData;
        public function __construct() {
            $this->linkbd = conectar_v7();
            $this->linkbd->set_charset("utf8");
        }

        public function getData(){
            if(!empty($_SESSION)){
                echo json_encode($this->selectData(),JSON_UNESCAPED_UNICODE);
            }
            die();
        }
        public function changeData(){
            if(!empty($_SESSION)){
                $id = intval($_POST['id']);
                $estado = $_POST['estado'];
                $sql = "UPDATE humparafiscalesccpet SET estado = '$estado' WHERE codigo = $id";
                $request = mysqli_query($this->linkbd,$sql);
                if($request == 1){
                    $arrResponse = array("status"=>true);
                }else{
                    $arrResponse = array("status"=>false,"msg"=>"Error");
                }
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
            die();
        }
        public function selectData(){
            $sql = "SELECT codigo,nombre,tipo,estado,porcentaje FROM humparafiscalesccpet ORDER BY codigo DESC";
            $request = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
            $total = count($request);
            for ($i=0; $i < $total; $i++) {
                $request[$i]['is_status'] = $request[$i]['estado'] =="S"? 1 : 0;
                $request[$i]['tipo'] = $request[$i]['tipo'] =="A" ? "Aporte" : "Descuento";
                $request[$i]['nombre'] = ucwords(strtolower($request[$i]['nombre']));
            }
            return $request;
        }

    }
?>
