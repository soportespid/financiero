<?php
    require_once '../../../comun.inc';
    require '../../../funciones.inc';
    require '../../../funcionesSP.inc.php';
    /*ini_set('display_errors', '1');
    ini_set('display_startup_errors', '1');
    error_reporting(E_ALL);*/
    session_start();
    //dep($_POST);exit;
    if($_POST){
        $obj = new Plantilla();
        if($_POST['action']=="get"){
            $obj->getData();
        }else if($_POST['action']=="save"){
            $obj->save();
        }
    }

    class Plantilla{
        private $linkbd;
        private $arrData;
        private $intId;
        public function __construct() {
            $this->linkbd = conectar_v7();
            $this->linkbd->set_charset("utf8");
        }
        public function save(){
            if(!empty($_SESSION)){
                if(empty($_POST['data'])){
                    $arrResponse = array("status"=>false,"msg"=>"Error de datos");
                }else{
                    $arrData = json_decode($_POST['data'],true);
                    //dep($arrData );exit;
                    if($arrData['nombre'] == "" || $arrData['consecutivo'] =="" || $arrData['porcentaje'] == ""
                    || $arrData['tipo'] == "0"){
                        $arrResponse = array("status"=>false,"msg"=>"Todos los campos con (*) son obligatorios.");
                    }else if(empty($arrData['variables'])){
                        $arrResponse = array("status"=>false,"msg"=>"Debe agregar al menos una variable parametrizada.");
                    }else{
                        $arrData['nombre'] = ucwords(replaceChar(strClean($arrData['nombre'])));
                        $arrData['tipo'] = strtoupper(replaceChar(strClean($arrData['tipo'])));
                        $arrData['porcentaje'] = doubleval($arrData['porcentaje']);
                        $arrData['consecutivo'] = strtoupper(replaceChar(strClean($arrData['consecutivo'])));

                        $request = $this->updateData($arrData);
                        if(is_numeric($request) && $request> 0 ){
                            $arrResponse = array("status"=>true,"msg"=>"Datos guardados.");
                        }else if($request =="existe"){
                            $arrResponse = array("status"=>false,"msg"=>"El nombre del parafiscal ya existe, pruebe con otro.");
                        }else{
                            $arrResponse = array("status"=>false,"msg"=>"Ha ocurrido un error, intente de nuevo.");
                        }
                    }
                }
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
            die();
        }
        public function getData(){
            if(!empty($_SESSION)){
                $id = intval(strClean($_POST['codigo']));
                $request = $this->selectData($id);
                if(!empty($request['parafiscal'])){
                    $arrData = array(
                        "conceptos"=>$this->selectConceptos(),
                        "consecutivo"=>$this->selectConsecutivo(),
                        "variables"=>$this->selectVariables(),
                        "status"=>true,
                        "data"=>$request
                    );
                }else{
                    $arrData = array("status"=>false,"consecutivo"=>$this->selectConsecutivo()-1);
                }
                echo json_encode($arrData,JSON_UNESCAPED_UNICODE);
            }
            die();
        }
        public function updateData(array $arrData){
            $arrVariables = $arrData['variables'];
            $sql = "SELECT * FROM humparafiscalesccpet WHERE nombre = '{$arrData['nombre']}' AND codigo != '{$arrData['consecutivo']}'";
            $request = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
            if(empty($request)){
                $sql = "UPDATE humparafiscalesccpet SET nombre='{$arrData['nombre']}', tipo='{$arrData['tipo']}',porcentaje={$arrData['porcentaje']}
                WHERE codigo = '{$arrData['consecutivo']}'";

                $request = intval(mysqli_query($this->linkbd,$sql));
                mysqli_query($this->linkbd,"DELETE FROM humparafiscalesccpet_det WHERE codigo = '{$arrData['consecutivo']}'");
                $totalVariables = count($arrVariables);
                for ($i=0; $i < $totalVariables ; $i++) {
                    $sql_max = "SELECT MAX(id_det) as id FROM humparafiscalesccpet_det";
                    $id_det = mysqli_query($this->linkbd,$sql_max)->fetch_assoc()['id']+1;

                    $funcionamiento = $arrVariables[$i]['funcionamiento']['codigo'];
                    $temp_funcionamiento = $arrVariables[$i]['temp_funcionamiento']['codigo'];
                    $inversion = $arrVariables[$i]['inversion']['codigo'];
                    $temp_inversion = $arrVariables[$i]['temp_inversion']['codigo'];
                    $comercio = $arrVariables[$i]['comercio'] ? $arrVariables[$i]['comercio']['codigo'] : "";
                    $temp_comercio = $arrVariables[$i]['temp_comercio'] ? $arrVariables[$i]['temp_comercio']['codigo'] : "";
                    $sector = $arrVariables[$i]['sector'];
                    $sql_var = "INSERT INTO humparafiscalesccpet_det (id_det,codigo, funcionamiento, funcionamientotemp,
                    inversion,inversiontemp,gastoscomerc,gastoscomerctemp,sector,estado)
                    VALUES(
                    $id_det,
                    '{$arrData['consecutivo']}',
                    '$funcionamiento',
                    '$temp_funcionamiento',
                    '$inversion',
                    '$temp_inversion',
                    '$comercio',
                    '$temp_comercio',
                    '$sector',
                    'S')";
                    mysqli_query($this->linkbd,$sql_var);
                }
            }else{
                $request = "existe";
            }
            return $request;
        }
        public function selectConceptos(){
            $sql = "SELECT * FROM conceptoscontables WHERE modulo='2'
            AND (tipo= 'FT' OR tipo='IN' OR tipo='IT' OR tipo = 'CP' OR tipo ='CT' OR tipo = 'F') ORDER BY codigo";
            $request = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
            $arrData = array(
                "funcionamiento"=>array_values(array_filter($request,function($e){return $e['tipo']=="F";})),
                "temp_funcionamiento"=>array_values(array_filter($request,function($e){return $e['tipo']=="FT";})),
                "inversion"=>array_values(array_filter($request,function($e){return $e['tipo']=="IN";})),
                "temp_inversion"=>array_values(array_filter($request,function($e){return $e['tipo']=="IT";})),
                "comercio"=>array_values(array_filter($request,function($e){return $e['tipo']=="CP";})),
                "temp_comercio"=>array_values(array_filter($request,function($e){return $e['tipo']=="CT";}))
            );
            return $arrData;
        }
        public function selectVariables(){
            $sql = "SELECT codigo, nombre,estado FROM ccpethumvariables WHERE estado = 'S'";
            $request = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
            return $request;
        }
        public function selectConsecutivo(){
            $sql = "SELECT MAX(RIGHT(codigo,2)) as codigo FROM humparafiscalesccpet";
            $request = mysqli_query($this->linkbd,$sql)->fetch_assoc()['codigo']+1;
            return $request;
        }
        public function selectData($id){
            $this->intId = $id;
            $sql = "SELECT * FROM humparafiscalesccpet ORDER BY id ASC";
            $request = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
            $parafiscal = array_values(array_filter($request,function($e){return $e['codigo'] == $this->intId;}))[0];
            if(!empty($parafiscal)){
                $sql_det = "SELECT * FROM humparafiscalesccpet_det WHERE codigo = '$id' ORDER BY codigo ASC";
                $parafiscal['variables'] = mysqli_fetch_all(mysqli_query($this->linkbd,$sql_det),MYSQLI_ASSOC);
            }
            return array("consecutivos"=>$request,"parafiscal"=>$parafiscal);
        }
    }
?>
