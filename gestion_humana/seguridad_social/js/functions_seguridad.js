const URL ='gestion_humana/contratos/controllers/ContratoController.php';
const URLEXPORT ='gestion_humana/contratos/controllers/ContratoExportController.php';
var app = new Vue({
    el:"#myapp",
    data() {
        return {
            isLoading:false,
            isModalTercero:false,
            isModalCargo:false,
            txtSearchTercero:"",
            txtSearchCargo:"",
            txtResultadosTerceros:0,
            txtResultadosCargos:0,
            txtParaSaludFuncionario:4,
            txtParaSaludEmpleador:8.5,
            txtParaPensionFuncionario:4,
            txtParaPensionEmpleador:12,
            txtParaCcf:4,
            txtParaIcbf:3,
            txtParaSena:0.5,
            txtParaInterCesantias:1,
            txtParaEsap:0.5,
            txtObligacionEmpleador:"",
            txtObligacionEmpleado:"",
            txtClausula:"",
            txtFechaIngreso:"",
            txtFechaTerminacion:"",
            txtParagrafo :"",
            txtJornadaLvInicio:"",
            txtJornadaLvFinal:"",
            txtJornadaLsInicio:"",
            txtJornadaLsFinal:"",
            txtJornadaLdInicio:"",
            txtJornadaLdFinal:"",
            txtCuentaBancaria:"",
            selectBancos:"00",
            selectUnidad:"-1",
            selectSeccion:"-1",
            selectCentro:"-1",
            selectJornada:"1",
            selectContrato:1,
            selectPeriodo:"",
            arrUnidades:[],
            arrSecciones:[],
            arrSeccionesCopy:[],
            arrCentrosCopy:[],
            arrCentros:[],
            arrBancos:[],
            arrCargos:[],
            arrCargosCopy:[],
            arrTerceros:[],
            arrTercerosCopy:[],
            arrObligaciones:[],
            arrClausulas:[],
            arrPeriodos:[],
            terceroType:1,
            objTercero:{codigo:"",nombre:""},
            objEps:{codigo:"",nombre:""},
            objArl:{codigo:"",nombre:""},
            objAfp:{codigo:"",nombre:""},
            objFondo:{codigo:"",nombre:""},
            objCargo:{codigo:"",nombre:"",valor:0}
        }
    },
    mounted() {
        this.getData();
    },
    methods: {
        getData: async function(){
            const formData = new FormData();
            formData.append("action","get");
            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();
            this.arrBancos = objData.bancos;
            this.arrTerceros = objData.terceros;
            this.arrTercerosCopy = objData.terceros;
            this.arrCargos = objData.cargos;
            this.arrCargosCopy = objData.cargos;
            this.arrCentros = objData.centros;
            this.arrSecciones = objData.secciones;
            this.arrUnidades = objData.unidades;
            this.arrPeriodos = objData.periodos;
            this.selectPeriodo = this.arrPeriodos[0].dias;
            this.txtResultadosCargos = this.arrCargosCopy.length;
            this.txtResultadosTerceros = this.arrTercerosCopy.length;
            console.log(objData);
        },
        selectItem:function ({...data},type=""){
            if(type =="tercero"){
                if(this.terceroType==1){
                    this.objTercero = data;
                }else if(this.terceroType==2){
                    this.objEps = data;
                }else if(this.terceroType==3){
                    this.objArl = data;
                }else if(this.terceroType==4){
                    this.objAfp = data;
                }else if(this.terceroType==5){
                    this.objFondo = data;
                }
                this.isModalTercero = false;
            }else if(type =="cargo"){
                this.objCargo = data;
                this.isModalCargo = false;
            }
        },
        addObligacion:function(type=1){
            if((type==2 && this.txtObligacionEmpleado == "") || (type==1 && this.txtObligacionEmpleador == "")){
                Swal.fire("Atención","El campo no puede estar vacio.","warning");
                return false;
            }
            let obligacion = type == 1 ? this.txtObligacionEmpleador : this.txtObligacionEmpleado
            this.arrObligaciones.push({"type":type,"value":obligacion});
            this.txtObligacionEmpleador = type == 1 ? "" : this.txtObligacionEmpleador;
            this.txtObligacionEmpleado = type == 2 ? "" : this.txtObligacionEmpleado;
        },
        delItem:function(index,type=1){
            if(type==1){
                this.arrObligaciones.splice(index,1);
            }else if(type==2){
                this.arrClausulas.splice(index,1);
            }
        },
        addClausula:function(){
            if(this.txtClausula == ""){
                Swal.fire("Atención","El campo no puede estar vacio.","warning");
                return false;
            }
            this.arrClausulas.push({
                "clausula":this.txtClausula,
                "paragrafo":this.txtParagrafo
            });
            this.txtClausula = "";
            this.txtParagrafo = "";
        },
        changeUnidad:function(type="unidad"){
            if(type=="seccion"){
                this.selectCentro = -1;
                this.arrCentrosCopy = [...this.arrCentros.filter(e=>e.seccion == app.selectSeccion)];
            }else{
                this.arrSeccionesCopy = [...this.arrSecciones.filter(e=>e.unidad == app.selectUnidad)];
                this.arrCentrosCopy = [];
                this.selectSeccion = -1;
            }
        },
        search:function(type=""){
            let search = "";
            if(type == "modal_tercero")search = this.txtSearchTercero.toLowerCase();
            if(type == "modal_cargo")search = this.txtSearchCargo.toLowerCase();
            if(type=="cod_tercero")search = this.objTercero.codigo;

            if(type=="modal_tercero"){
                this.arrTercerosCopy = [...this.arrTerceros.filter(e=>e.codigo.toLowerCase().includes(search) || e.nombre.toLowerCase().includes(search))];
                this.txtResultadosTerceros = this.arrTercerosCopy.length;
            }else if(type=="modal_cargo"){
                this.arrCargosCopy = [...this.arrCargos.filter(e=>e.codigo.toLowerCase().includes(search) || e.nombre.toLowerCase().includes(search))];
                this.txtResultadosCargos = this.arrCargosCopy.length;
            }else if(type=="cod_tercero"){
                const data = [...this.arrTerceros.filter(e=>e.codigo == search)];
                this.objTercero = data.length > 0 ? JSON.parse(JSON.stringify(data[0])) : {codigo:"",nombre:""};
            }
        },
        exportData:function(){
            let obj = {
                fecha_ingreso:this.txtFechaIngreso,
                fecha_terminacion:this.txtFechaTerminacion,
                contrato:this.selectContrato,
                periodo:this.arrPeriodos.filter(e=>{return e.dias == app.selectPeriodo})[0],
                cuenta_bancaria:{
                    banco: this.arrBancos.filter(e=>{return e.codigo == app.selectBancos})[0],
                    numero:app.txtCuentaBancaria
                },
                cargo:this.objCargo,
                tercero:this.objTercero,
                jornada:this.selectJornada,
                horario:{
                    inicio:this.formatAMPM(app.txtJornadaLvInicio),
                    final:this.formatAMPM(app.txtJornadaLvFinal),
                    inicio_sabado:this.formatAMPM(app.txtJornadaLsInicio),
                    final_sabado:this.formatAMPM(app.txtJornadaLsFinal),
                    inicio_domingo:this.formatAMPM(app.txtJornadaLdInicio),
                    final_domingo:this.formatAMPM(app.txtJornadaLdFinal)
                },
                obligaciones:this.arrObligaciones,
                clausulas:this.arrClausulas,
            }
            const form = document.createElement("form");
            form.method ="post";
            form.target="_blank";
            form.action=URLEXPORT;

            function addField(name,value){
                const input = document.createElement("input");
                input.type="hidden";
                input.name=name;
                input.value = value;
                form.appendChild(input);
            }
            addField("action","pdf");
            addField("data",JSON.stringify(obj));
            document.body.appendChild(form);
            form.submit();
            document.body.removeChild(form);
        },
        formatNum: function(valor){
            return new Intl.NumberFormat("en-US", {style: "currency", currency: "USD"}).format(valor);
        },
        showTab:function(tab){
            let tabs = this.$refs.rTabs.children;
            let tabsContent = this.$refs.rTabsContent.children;
            for (let i = 0; i < tabs.length; i++) {
                tabs[i].classList.remove("active");
                tabsContent[i].classList.remove("active")
            }
            tabs[tab-1].classList.add("active");
            tabsContent[tab-1].classList.add("active")
        },
        formatAMPM:function(date) {
            const arrDate = date.split(":");
            let hours = parseInt(arrDate[0]);
            let minutes = parseInt(arrDate[1]);
            let ampm = hours >= 12 ? 'pm' : 'am';
            hours = hours % 12;
            hours = hours ? hours : 12; // the hour '0' should be '12'
            minutes = minutes < 10 ? '0'+minutes : minutes;
            let strTime = hours + ':' + minutes + ' ' + ampm;
            return strTime;
          }
    },
})
