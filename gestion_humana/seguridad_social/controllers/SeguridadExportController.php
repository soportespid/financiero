<?php
    require_once '../../../Librerias/core/Helpers.php';
    require_once '../../../Librerias/core/Mysql.php';
    require_once '../../../PHPExcel/Classes/PHPExcel.php';
    require_once '../../../tcpdf/tcpdf_include.php';
    require_once '../../../tcpdf/tcpdf.php';
    session_start();
    /*ini_set('display_errors', '1');
    ini_set('display_startup_errors', '1');
    error_reporting(E_ALL);*/
    class ContratoExportController extends Mysql{
        public function exportPdf(){
            if(!empty($_SESSION)){
                $arrData = json_decode($_POST['data'],true);
                if(!empty($arrData)){
                    $arrFecha = explode("-",$arrData['fecha_ingreso']);
                    $arrMeses = getMeses();
                    $arrTercero = $arrData['tercero'];
                    $arrCargo = $arrData['cargo'];
                    $arrHorario = $arrData['horario'];
                    $arrPeriodo = $arrData['periodo'];
                    $arrClausulas = $arrData['clausulas'];
                    $request = configBasica();
                    $strTipoDoc = $this->select("SELECT nombre FROM docindentidad WHERE estado = 1 AND id_tipodocid = '{$arrTercero['tipodoc']}'")['nombre'];
                    $strNit = $request['nit'];
                    $strRazon = strtoupper($request['razonsocial']);
                    $strRepresentante = strtoupper($request['representante']);
                    $strNombreTercero = strtoupper($arrTercero['nombre']);
                    $intJornada = $arrData['jornada'];
                    //$strDireccion = $request['direccion'];
                    $arrTipoContrato = array(
                        1=>"Contrato laboral termino indefinido",
                        2=>"Contrato laboral termino fijo",
                        3=>"Contrato laboral ocasional o transitorio",
                        4=>"Contrato obra o labor",
                        5=>"Contrato por prestación de servicios"
                    );
                    $strTipoContrato = $arrTipoContrato[$arrData['contrato']];

                    $encabezado = 'Entre las partes, por un lado '.strtoupper($strRepresentante).', representante legal de '.$strRazon.'
                    con NIT '.$strNit.', quien en adelante y para los efectos del presente contrato se denomina como EL EMPLEADOR, y por el otro, '.$strNombreTercero.',
                    domiciliado en la dirección '.$arrTercero['direccion'].', identificado con '.strtolower($strTipoDoc)." ".$arrTercero['codigo'].'
                    quien en adelante y para los efectos del presente contrato se denomina como EL TRABAJADOR,
                    hemos acordado suscribir este contrato de trabajo el dia '.$arrFecha[2].'
                    de '.$arrMeses[$arrFecha[1]].' del '.$arrFecha[0].', el cual se regirá por las siguientes cláusulas:<br>';

                    $clausula1 = '<strong>ARTICULO 1</strong>. Naturaleza y Objeto. Se trata de un '.strtolower($strTipoContrato).',
                    en vigencia del cual el EMPLEADOR contrata al TRABAJADOR para que este de forma personal,
                    dirija su capacidad de trabajo en aras de la prestación de servicios y desempeño de las actividades propias del cargo de '.$arrCargo['nombre'].',
                    y como contraprestación el EMPLEADOR pagará una remuneración<br><br>';


                    $arrObligacionesEmpleador = array_filter($arrData['obligaciones'],function($e){return $e['type']==1;});
                    $arrObligacionesEmpleado = array_filter($arrData['obligaciones'],function($e){return $e['type']==2;});

                    $clausula2 = '<strong>ARTICULO 2.</strong> Obligaciones de las partes. <br><br>1. Del empleador <ol style="list-style-type:lower-alpha">';
                    foreach ($arrObligacionesEmpleador as $data) {
                        $clausula2.= '<li>'.$data['value'].'</li>';
                    }
                    $clausula2 .='</ol>2. Del empleado <ol style="list-style-type:lower-alpha">';
                    foreach ($arrObligacionesEmpleado as $data) {
                        $clausula2.= '<li>'.$data['value'].'</li>';
                    }
                    $clausula2.='</ol><br>';
                    $jornada1 = $intJornada == 1 ? '<li>Lunes a viernes de '.$arrHorario['inicio'].' a '.$arrHorario['final'].'</li>' : "";
                    $jornada2 = $intJornada == 2 ? '<li>Lunes a sabado de '.$arrHorario['inicio_sabado'].' a '.$arrHorario['final_sabado'].'</li>' : "";
                    $jornada3 = $intJornada == 3 ? '<li>Lunes a domingo de '.$arrHorario['inicio_domingo'].' a '.$arrHorario['final_domingo'].'</li>' : "";
                    $clausula3 ='<strong>ARTICULO 3.</strong> Jornada laboral. Se obliga a laborar la jornada de la siguiente manera:';
                    $clausula3.= '<ol>'.$jornada1.$jornada2.$jornada3.'</ol><br>';
                    $clausula4 = '<strong>ARTICULO 4.</strong> Remuneración. El EMPLEADOR pagará al TRABAJADOR por la prestación de sus servicios un salario
                    de '.getValorLetras($arrCargo['valor'],"M/CTE")." (".formatNum($arrCargo['valor']).') pagados de manera '.strtolower($arrPeriodo['nombre']).'<br><br>';
                    $clasulas="";
                    $cont = 5;
                    foreach ($arrClausulas as $data) {
                        $paragrafo = $data['paragrafo'] != "" ? "<br><br><strong>Parágrafo.</strong> ".$data['paragrafo']."<br><br>":"";
                        $clausula =$paragrafo != "" ? '<strong>ARTICULO '.$cont.'.</strong> '.$data['clausula'].$paragrafo : '<strong>ARTÍCULO '.$cont.'.</strong> '.$data['clausula']."<br><br>";
                        $clasulas.= $clausula;
                        $cont++;
                    }
                    $cuerpo = $clausula1.$clausula2.$clausula3.$clausula4.$clasulas;
                    define("CONTRATO",$strTipoContrato);
                    define(FECHA,$arrData['fecha_ingreso']);
                    $file = "contrato_".bin2hex(random_bytes(6)).'.pdf';
                    $pdf = new MYPDF('P','mm','Letter', true, 'iso-8859-1', false);
                    $pdf->SetDocInfoUnicode (true);
                    // set document information
                    $pdf->SetCreator(PDF_CREATOR);
                    $pdf->SetAuthor('IDEALSAS');
                    $pdf->SetTitle('CONTRATO');
                    $pdf->SetSubject('CONTRATO');
                    $pdf->SetKeywords('TCPDF, PDF, example, test, guide');
                    $pdf->SetMargins(10, 38, 10);// set margins
                    $pdf->SetHeaderMargin(38);// set margins
                    $pdf->SetFooterMargin(17);// set margins
                    $pdf->SetAutoPageBreak(TRUE, 20);// set auto page breaks
                    // set some language-dependent strings (optional)
                    if (@file_exists(dirname(__FILE__).'/lang/spa.php'))
                    {
                        require_once(dirname(__FILE__).'/lang/spa.php');
                        $pdf->setLanguageArray($l);
                    }
                    $pdf->AddPage();
                    $pdf->SetFont('Helvetica','',10);
                    $titulo = '<br><h4 style="text-align:center;margin-bottom:20px;">CONTRATO DE TRABAJO ENTRE '.$strRazon.' Y '.$strNombreTercero.'</h4><br><br>';
                    $pdf->writeHTML($titulo);
                    $pdf->writeHTML($encabezado);
                    $pdf->writeHTML($cuerpo);
                    $pdf->ln();
                    //Campo para recibido y sello
                    $getY = $pdf->getY();
                    if($getY > 233){
                        $pdf->AddPage();
                    }
                    $pdf->SetFont('helvetica','b',10);
                    $pdf->SetTextColor(0,0,0);
                    $pdf->SetFillColor(255,255,255);
                    $pdf->setX(10);
                    $pdf->cell(95,4,'EL EMPLEADOR, ','',0,'L',1);
                    $pdf->setX(120);
                    $pdf->cell(95,4,'El TRABAJADOR, ','',0,'L',1);
                    $pdf->ln();
                    $pdf->setX(10);
                    $pdf->cell(80,20,'','B',0,'L',1);
                    $pdf->setX(120);
                    $pdf->cell(80,20,'','B',0,'L',1);
                    $pdf->ln();
                    $pdf->setX(10);
                    $pdf->cell(95,4,$strRepresentante,'T',0,'L',1);
                    $pdf->setX(120);
                    $pdf->cell(80,4,$strNombreTercero,'T',0,'L',1);
                    $pdf->ln();
                    $pdf->setX(10);
                    $pdf->SetFont('helvetica','',10);
                    $pdf->cell(95,4,"Representante legal",'',0,'L',1);
                    $pdf->setX(120);
                    $pdf->SetFont('helvetica','',10);
                    $pdf->cell(80,4,$strTipoDoc." ".$arrTercero['codigo'],'',0,'L',1);
                    $pdf->ln();
                    $pdf->setX(10);
                    $pdf->SetFont('helvetica','B',10);
                    $pdf->cell(95,4,$strRazon,'',0,'L',1);
                    $pdf->ln();
                    $pdf->setX(10);
                    $pdf->SetFont('helvetica','',10);
                    $pdf->cell(95,4,"NIT ".$strNit,'',0,'L',1);

                    $pdf->Output($file, 'I');
                }
            }
            die();
        }

    }
    class MYPDF extends TCPDF {

		public function Header()
		{
			$request = configBasica();
            $strNit = $request['nit'];
            $strRazon = $request['razonsocial'];

			//Parte Izquierda
			$this->Image('../../../imagenes/escudo.jpg',  13, 13, 20, 20, 'JPG', '', 'T', true, 300, '', false, false, 0, false, false, false);// Logo
			$this->SetFont('helvetica','B',8);
			$this->SetY(10);
			$this->RoundedRect(10, 10, 190, 25, 1,'');
			$this->Cell(0.1);
			$this->Cell(26,25,'','R',0,'L');
			$this->SetY(8);
			$this->SetX(40);
			$this->SetFont('helvetica','B',9);
			$this->Cell(140,15,strtoupper("$strRazon"),0,0,'C');
			$this->SetFont('helvetica','B',7);
			$this->SetY(12);
			$this->SetX(40);
			$this->Cell(140,15,'NIT: '.$strNit,0,0,'C');
			//*****************************************************************************************************************************
			$this->SetFont('helvetica','B',9);
			$this->SetY(23);
			$this->SetX(36);
            $this->Cell(164,12,strtoupper(CONTRATO),'T',0,'C');


            $this->SetFont('helvetica','B',7);
			$this->SetY(10);
			$this->SetX(167);
			$this->Cell(30,7," FECHA: ". date_format(date_create(FECHA),"d/m/Y"),"L",0,'L');
			$this->SetY(17);
			$this->SetX(167);
            $this->Cell(35,6,"","L",0,'L');

			//**********************************************************
			$this->SetFont('times','B',10);
			$this->ln(12);
			//**********************************************************
		}
		public function Footer(){

			$request = configBasica();
            $strDireccion = $request['direccion'] != "" ? "Dirección: ".strtoupper($request['direccion']) : "";
            $strWeb = $request['web'] != "" ? "Pagina web: ".strtoupper($request['web']) :"";
            $strEmail = $request['email'] !="" ? "Email: ".strtoupper($request['email']) :"";
            $strTelefono = $request['telefono'] != "" ? "Telefonos: ".$request['telefono'] : "";
            $strUsuario = searchUser($_SESSION['cedulausu'])['nom_usu'];
			$strNick = $_SESSION['nickusu'];
			$strFecha = date("d/m/Y H:i:s");
			$strIp = $_SERVER['REMOTE_ADDR'];

			$this->SetFont('helvetica', 'I', 8);
			$txt = <<<EOD
			$strDireccion $strTelefono
			$strEmail $strWeb
			EOD;
			$this->SetFont('helvetica', 'I', 6);
			$this->Cell(277,10,'','T',0,'T');
			$this->ln(2);
			$this->Write(0, $txt , '', 0, 'C', true, 0, false, false, 0);

			$this->Cell(50, 10, 'Hecho por: '.$strUsuario, 00, false, 'C', 0, '', 0, false, 'T', 'M');
			$this->Cell(30, 10, 'Impreso por: '.$strNick, 0, false, 'C', 0, '', 0, false, 'T', 'M');
			$this->Cell(30, 10, 'IP: '.$strIp, 0, false, 'C',0, '', 0, false, 'T', 'M');
			$this->Cell(30, 10, 'Fecha: '.$strFecha, 0, false, 'C', 0, '', 0, false, 'T', 'M');
			$this->Cell(30, 10, 'IDEAL.10 S.A.S', 0, false, 'C', 0, '', 0, false, 'T', 'M');
            $this->Cell(20, 10, 'Pagina '.$this->getAliasNumPage().'/'.$this->getAliasNbPages(), 0, false, 'C', 0, '', 0, false, 'T', 'M');
		}
	}

    if($_POST){
        $obj = new ContratoExportController();
        if($_POST['action'] == "pdf"){
            $obj->exportPdf();
        }
    }

?>
