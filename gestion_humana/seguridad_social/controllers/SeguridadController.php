<?php
    require_once '../../../Librerias/core/Helpers.php';
    require_once '../../../PHPExcel/Classes/PHPExcel.php';
    require_once '../models/ContratoModel.php';
    session_start();

    class ContratoController extends ContratoModel{
        public function initialData(){
            if(!empty($_SESSION)){
                $arrResponse = array(
                    "cargos"=>$this->selectCargos(),
                    "terceros"=>getTerceros(),
                    "unidades"=>$this->selectUnidades(),
                    "secciones"=>$this->selecSecciones(),
                    "bancos"=>$this->selectBancos(),
                    "periodos"=>$this->selectPeriodos(),
                    "centros"=>$this->selectCentros(),
                    "riesgos"=>$this->selectRiesgos()
                );
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
            die();
        }
        public function save(){
            if(!empty($_SESSION)){

            }
            die();
        }
        public function getEdit(){
            if(!empty($_SESSION)){

            }
            die();
        }
        public function getSearch(){

            die();
        }
    }

    if($_POST){
        $obj = new ContratoController();
        if($_POST['action'] == "get"){
            $obj->initialData();
        }
    }

?>
