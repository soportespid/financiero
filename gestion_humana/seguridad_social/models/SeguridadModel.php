<?php
    require_once '../../../Librerias/core/Helpers.php';
    require_once '../../../Librerias/core/Mysql.php';
    session_start();
    /*ini_set('display_errors', '1');
    ini_set('display_startup_errors', '1');
    error_reporting(E_ALL);*/
    class ContratoModel extends Mysql{
        private $intConsecutivo;
        private $intIdComp;
        private $strTipo;
        private $strCuentaBanco;
        private $strCuentaCaja;
        private $strTercero;
        private $strConcepto;
        private $floatTotal;
        private $strFecha;
        private $strFechaFinal;
        function __construct(){
            parent::__construct();
        }

        public function selectCargos(){
            $sql = "SELECT
            c.codcargo as codigo,
            c.nombrecargo as nombre,
            c.clasificacion,
            s.valor,
            s.id_nivel as escala,
            s.nombre as nivel_salarial
            FROM planaccargos c
            INNER JOIN humnivelsalarial s ON s.id_nivel=c.clasificacion
            WHERE c.estado = 'S' AND s.estado = 'S' ORDER BY c.codcargo DESC";
            $request = $this->select_all($sql);
            return $request;
        }
        public function selectBancos(){
            $sql = "SELECT codigo, nombre FROM hum_bancosfun WHERE estado='S' ORDER BY CONVERT(codigo, SIGNED INTEGER)";
            $request = $this->select_all($sql);
            return $request;
        }
        public function selectPeriodos(){
            $sql = "SELECT id_periodo as codigo,nombre,dias FROM humperiodos ORDER BY id_periodo";
            $request = $this->select_all($sql);
            return $request;
        }
        public function selectUnidades(){
            $sql = "SELECT id_cc as codigo, nombre FROM pptouniejecu WHERE estado = 'S' ORDER BY id_cc ASC";
            $request = $this->select_all($sql);
            return $request;
        }
        public function selecSecciones(){
            $sql = "SELECT id_seccion_presupuestal as codigo, nombre,id_unidad_ejecutora as unidad FROM pptoseccion_presupuestal";
            $request = $this->select_all($sql);
            return $request;
        }
        public function selectRiesgos(){
            $sql = "SELECT id as codigo, tarifa, detalle as nombre FROM hum_nivelesarl WHERE estado = 'S' ORDER BY id";
            $request = $this->select_all($sql);
            return $request;
        }
        public function selectCentros(){
            $sql = "SELECT T1.id_cc as codigo, T1.nombre,T3.id_seccion_presupuestal as seccion
            FROM centrocosto AS T1
            INNER JOIN centrocostos_seccionpresupuestal AS T2 ON T1.id_cc = T2.id_cc
            INNER JOIN pptoseccion_presupuestal AS T3 ON T3.id_seccion_presupuestal = T2.id_sp
            WHERE T1.estado = 'S'  ORDER BY CONVERT(T1.id_cc, SIGNED INTEGER)";
            $request = $this->select_all($sql);
            return $request;
        }
    }
?>
