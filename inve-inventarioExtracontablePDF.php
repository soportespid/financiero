<?php

use PhpOffice\PhpSpreadsheet\Writer\Pdf;

    require_once("tcpdf/tcpdf_include.php");
	require('comun.inc');
	require "funciones.inc";
    require 'funcionesSP.inc.php';
	date_default_timezone_set("America/Bogota");
	session_start();
	class MYPDF extends TCPDF {
		public function Header() {
			$linkbd = conectar_v7();
			$linkbd -> set_charset("utf8");
			$sqlr="SELECT * FROM configbasica WHERE estado='S'";
			$res=mysqli_query($linkbd, $sqlr);
			while($row=mysqli_fetch_row($res)) {
				$nit = $row[0];
				$rs  = $row[1];
			}
			//Parte Izquierda
			$this->Image('imagenes/escudo.jpg',  13, 13, 20, 20, 'JPG', '', 'T', true, 300, '', false, false, 0, false, false, false);// Logo
			$this->SetFont('helvetica','B',8);
			$this->SetY(10);
			$this->RoundedRect(10, 10, 190, 25, 1,'');
			$this->Cell(0.1);
			$this->Cell(26,25,'','R',0,'L');
			$this->SetY(8);
			$this->SetX(40);
			$this->SetFont('helvetica','B',9);
			$this->Cell(140,15,strtoupper("$rs"),0,0,'C');
			$this->SetFont('helvetica','B',7);
			$this->SetY(12);
			$this->SetX(40);
			$this->Cell(140,15,'NIT: '.$nit,0,0,'C');
			//*****************************************************************************************************************************
			$this->SetFont('helvetica','B',9);
			$this->SetY(23);
			$this->SetX(36);
            $this->Cell(164,12,"CERTIFICADO DE INVENTARIO EXTRACONTABLE",'T',0,'C');


            $this->SetFont('helvetica','B',7);
			$this->SetY(10);
			$this->SetX(167);
			$this->Cell(30,13," FECHA: ". date("d/m/Y"),"L",0,'L');
			$this->SetY(17);
			$this->SetX(167);

			//**********************************************************
			$this->SetFont('times','B',10);
			$this->ln(12);
			//**********************************************************
		}
		public function Footer()
		{
			$linkbd = conectar_v7();
			$linkbd -> set_charset("utf8");
			$sqlr="SELECT direccion,telefono,web,email FROM configbasica WHERE estado='S'";
			$resp=mysqli_query($linkbd, $sqlr);
			$user = $_SESSION['nickusu'];
            $cedula = $_SESSION['cedulausu'];
			$fecha = date("Y-m-d H:i:s");
			$ip = $_SERVER['REMOTE_ADDR'];
			while($row=mysqli_fetch_row($resp))
			{
				$direcc=strtoupper($row[0]);
				$telefonos=$row[1];
				$dirweb=strtoupper($row[3]);
				$coemail=strtoupper($row[2]);
			}
			if($direcc!=''){$vardirec="Dirección: $direcc, ";}
			else {$vardirec="";}
			if($telefonos!=''){$vartelef="Telefonos: $telefonos";}
			else{$vartelef="";}
			if($dirweb!=''){$varemail="Email: $dirweb, ";}
			else {$varemail="";}
			if($coemail!=''){$varpagiw="Pagina Web: $coemail";}
			else{$varpagiw="";}
			$this->SetFont('helvetica', 'I', 8);
			$txt = <<<EOD
			$vardirec $vartelef
			$varemail $varpagiw
			EOD;
			$this->SetFont('helvetica', 'I', 6);
			$this->Cell(277,10,'','T',0,'T');
			$this->ln(2);
			$this->Write(0, $txt , '', 0, 'C', true, 0, false, false, 0);


            $sqlrcc="SELECT * FROM `usuarios` WHERE `cc_usu` = '$cedula'";
            $respcc=mysqli_query($linkbd, $sqlrcc);
            $rowcc=mysqli_fetch_row($respcc);
            //echo $rowcc[1];

			$this->Cell(50, 10, 'Hecho por: '.$rowcc[1], 00, false, 'C', 0, '', 0, false, 'T', 'M');
			$this->Cell(30, 10, 'Impreso por: '.$user, 0, false, 'C', 0, '', 0, false, 'T', 'M');
			$this->Cell(30, 10, 'IP: '.$ip, 0, false, 'C',0, '', 0, false, 'T', 'M');
			$this->Cell(30, 10, 'Fecha: '.$fecha, 0, false, 'C', 0, '', 0, false, 'T', 'M');
			$this->Cell(30, 10, 'IDEAL.10 S.A.S', 0, false, 'C', 0, '', 0, false, 'T', 'M');
            $this->Cell(20, 10, 'Pagina '.$this->getAliasNumPage().'/'.$this->getAliasNbPages(), 0, false, 'C', 0, '', 0, false, 'T', 'M');
		}
	}

	// if($_POST['dataCab']){
        $data = json_decode($_POST['data'],true);

        $pdf = new MYPDF('P','mm','Letter', true, 'iso-8859-1', false);
        $pdf->SetDocInfoUnicode (true);
        // set document information
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('IDEALSAS');
        $pdf->SetTitle('Almacen');
        $pdf->SetSubject('Almacen');
        $pdf->SetKeywords('TCPDF, PDF, example, test, guide');
        $pdf->SetMargins(10, 38, 10);// set margins
        $pdf->SetHeaderMargin(38);// set margins
        $pdf->SetFooterMargin(17);// set margins
        $pdf->SetAutoPageBreak(TRUE, 20);// set auto page breaks
        // set some language-dependent strings (optional)
        if (@file_exists(dirname(__FILE__).'/lang/spa.php'))
        {
            require_once(dirname(__FILE__).'/lang/spa.php');
            $pdf->setLanguageArray($l);
        }
        $pdf->SetFillColor(255,255,255);
        $pdf->AddPage();
        /*if($arrCabecera['tipomovimiento']=="401"){
            $pdf->SetTextColor(255,0,0);
            $pdf->SetFont('Helvetica','B',30);
            $pdf->Cell(190, 10, 'REVERSADO', 00, false, 'C', 0, '', 0, false, 'T', 'M');
            $pdf->ln();
        }*/
        $pdf->SetTextColor(0,0,0);
        $pdf->SetFont('Helvetica','',9);
        $pdf->SetFont('Helvetica','B',9);
        $pdf->SetFillColor(153,221,255);
        $pdf->MultiCell(190,4,"Descripción","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
        $pdf->ln();
        $pdf->SetFont('Helvetica','',20);
        $pdf->SetFillColor(255,255,255);
        $pdf->MultiCell(190,8,"CERTIFICADO ENTREGA DE INVENTARIO DE BIENES DE CONSUMO CON CONTROL, DEVOLUTIVOS DE LAS DIFERENTES OFICINAS DE LA ADMINISTRACIÓN DE LA ENTIDAD","LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
        
        $pdf->ln();
        $pdf->ln();
        
        $pdf->MultiCell(190,4,"Activos retirados","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
        $pdf->ln();
        $pdf->SetFillColor(153,221,255);
        $pdf->SetFont('helvetica','B',6);
        $pdf->MultiCell(7,10,"Item","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
        $pdf->MultiCell(18,10,"Cod articulo","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
        $pdf->MultiCell(30,10,"Nombre","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
        $pdf->MultiCell(15,10,"Fecha compra","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
        $pdf->MultiCell(25,10,"Dependencia","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
        $pdf->MultiCell(30,10,"Responsable","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
        $pdf->MultiCell(15,10,"Cantidad","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
        $pdf->MultiCell(25,10,"Valor unitario","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
        $pdf->MultiCell(25,10,"Valor total","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
        $pdf->ln();
        $pdf->SetFillColor(245,245,245);
        $pdf->SetFont('helvetica','',6);
        $fill = true;
		$index = 1;
        foreach ($data as $articulo) {

			$str_nombre = $pdf->getNumLines($articulo[3], 30);
            $str_responsable = $pdf->getNumLines($articulo[5], 30);
            $max = max($str_nombre, $str_responsable);
            $value = $max * 6;
			
			$fecha_compra = date("d/m/Y", strtotime($articulo[1]));

            $pdf->MultiCell(7,$value,$index,"LRBT",'C',$fill,0,'','',true,0,false,true,0,'M',true);
			$pdf->MultiCell(18,$value,$articulo[2],"LRBT",'C',$fill,0,'','',true,0,false,true,0,'M',true);
			$pdf->MultiCell(30,$value,$articulo[3],"LRBT",'C',$fill,0,'','',true,0,false,true,0,'M',true);
			$pdf->MultiCell(15,$value,$fecha_compra,"LRBT",'C',$fill,0,'','',true,0,false,true,0,'M',true);
			$pdf->MultiCell(25,$value,$articulo[10],"LRBT",'C',$fill,0,'','',true,0,false,true,0,'M',true);
			$pdf->MultiCell(30,$value,$articulo[11],"LRBT",'C',$fill,0,'','',true,0,false,true,0,'M',true);
			$pdf->MultiCell(15,$value,$articulo[7],"LRBT",'C',$fill,0,'','',true,0,false,true,0,'M',true);
			$pdf->MultiCell(25,$value,"$".number_format($articulo[8], 2),"LRBT",'C',$fill,0,'','',true,0,false,true,0,'M',true);
			$pdf->MultiCell(25,$value,"$".number_format($articulo[9], 2),"LRBT",'C',$fill,0,'','',true,0,false,true,0,'M',true);
			
			$index++;
			if ($pdf->GetY()>210) $pdf-> addPage();
            $pdf->ln();
            $fill = !$fill;
        }

        $pdf->ln();
        $pdf->ln();

        $getY = $pdf->getY();
        $pdf->setX(10);
        $pdf->SetFont('helvetica','B',9);
        $pdf->SetFillColor(153,221,255);
        $pdf->cell(90,4,'Entrega','LRTB',0,'L',1);

		$pdf->setX(110);
        $pdf->SetFont('helvetica','B',9);
        $pdf->SetFillColor(153,221,255);
        $pdf->cell(90,4,'Recibio','LRTB',0,'L',1);

		$pdf->ln();
        $pdf->setX(10);
        $pdf->SetFont('helvetica','',7);
        $pdf->SetFillColor(255,255,255);
        $pdf->cell(90,20,'','LRTB',0,'L',1);

		$pdf->setX(110);
        $pdf->SetFont('helvetica','',7);
        $pdf->SetFillColor(255,255,255);
        $pdf->cell(90,20,'','LRTB',0,'L',1);


        $pdf->ln();
        $pdf->setX(10);
        $pdf->cell(90,4,'Nombre: '  ,'LRT',0,'L',1);

		$pdf->setX(110);
        $pdf->cell(90,4,'Nombre: '  ,'LRT',0,'L',1);

        $pdf->ln();
        $pdf->setX(10);
        $pdf->cell(90,4,'C.C.','LRB',0,'L',1);

		$pdf->setX(110);
        $pdf->cell(90,4,'C.C.','LRB',0,'L',1);

        $pdf->Output('acta_de_retiro_activos_'.CONSECUTIVO.'.pdf', 'I');
    // }
?>
