<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=iso-8859-1");
	require "comun.inc";
	require "funciones.inc";
	session_start();
	$linkbd = conectar_v7();
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
	$scroll=$_GET['scrtop'];
	$totreg=$_GET['totreg'];
	$idcta=$_GET['idcta'];
	$altura=$_GET['altura'];
	$filtro="'".$_GET['filtro']."'";
?>
<!DOCTYPE >
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es">
	<head>
		<meta http-equiv="Content-type" content="text/html; charset=iso-8859-1"/>
		<meta http-equiv="X-UA-Compatible" content="IE=9"/>
		<title>:: IDEAL 10 - Servicios P&uacute;blicos</title>
		<link href="css/css2.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/cssSP.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<script type="text/javascript" src="css/calendario.js"></script>
		<script type="text/javascript" src="css/programas.js"></script>
		<style></style>
		<script>
			function despliegamodal2(_valor, _table)
			{
                document.getElementById('bgventanamodal2').style.visibility = _valor;
                
                if (_table == 'srvclientes')
                {
                    document.getElementById('ventana2').src = 'ventana-clienteservicio.php?table=' + _table;
                }
                else if(_table == 'srvotroscobros')
                {
                    document.getElementById('ventana2').src = 'otros-cobros-ventana.php';
                }
            }

			function despliegamodalm(_valor,_tip,mensa,pregunta)
			{
				document.getElementById("bgventanamodalm").style.visibility=_valor;
				if(_valor=="hidden"){document.getElementById('ventanam').src="";}
				else
				{
					switch(_tip)
					{
						case "1":	document.getElementById('ventanam').src="ventana-mensaje1.php?titulos="+mensa;break;
						case "2":	document.getElementById('ventanam').src="ventana-mensaje3.php?titulos="+mensa;break;
						case "3":	document.getElementById('ventanam').src="ventana-mensaje2.php?titulos="+mensa;break;
						case "4":	document.getElementById('ventanam').src="ventana-consulta1.php?titulos="+mensa+"&idresp="+pregunta;break;	
					}
				}
			}

			function respuestaModalBusqueda2(tabla, id, nombre, serial, referencia)
			{
				switch(tabla)
				{
					case 'srvclientes': 
						document.getElementById('cliente').value = id;
						document.getElementById('nCliente').value = nombre;
						document.form2.submit();
					break;	
                }
            }

			function funcionmensaje()
			{
				var idban=document.getElementById('codban').value;
				document.location.href = "serv-asignacionOtrosCobros.php";
			}

			function respuestaconsulta(pregunta)
			{
				switch(pregunta)
				{
					case "1":	
						document.form2.oculto.value='2';
						document.form2.submit();
					break;
				}
			}

			function guardar()
			{
                var otroCobro = document.getElementById('otroCobro').value;
				var estado = document.getElementById('myonoffswitch').value;

				if (otroCobro != "" && estado != '') 
                {
                    despliegamodalm('visible','4','Esta Seguro de Guardar','1');
                }
				else {
                    despliegamodalm('visible','2','Falta información para crear la Asignación');
                }
			}

			function actualizar()
			{
				document.form2.submit();
			}

			function cambiocheck()
			{
				if(document.getElementById('myonoffswitch').value=='S')
				{
					document.getElementById('myonoffswitch').value='N';
				}
				else
				{
					document.getElementById('myonoffswitch').value='S';
				}

				document.form2.submit();
			}

			function iratras(scrtop, numpag, limreg, filtro)
			{
				var idcta=document.getElementById('codban').value;

				location.href="serv-buscaAsignacionOtrosCobros.php?idcta="+idcta+"&scrtop="+scrtop+"&numpag="+numpag+"&limreg="+limreg+ "&filtro="+filtro;
			}

			function adelante(scrtop, numpag, limreg, filtro)
			{
				var maximo=document.getElementById('maximo').value;
				var actual=document.getElementById('codban').value;
				actual=parseFloat(actual)+1;

				if(actual<=parseFloat(maximo))
				{
					if(actual<10){actual="0"+actual;}
					location.href="serv-editaAsignacionOtrosCobros.php?idban=" +actual+ "&scrtop=" +scrtop+ "&numpag=" +numpag+ "&limreg=" +limreg+ "&filtro=" +filtro;
				}
			}

			function atrasc(scrtop, numpag, limreg, filtro, prev)
			{
				var minimo=document.getElementById('minimo').value;
				var actual=document.getElementById('codban').value;
				actual=parseFloat(actual)-1;

				if(actual>=parseFloat(minimo))
				{
					if(actual<10){actual="0"+actual;}
					location.href="serv-editaAsignacionOtrosCobros.php?idban=" +actual+ "&scrtop=" +scrtop+ "&numpag=" +numpag+ "&limreg=" +limreg+ "&filtro=" +filtro;
				}
			}
		</script>

		<?php titlepag();?>

	</head>

	<body>
		<IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
		<span id="todastablas2"></span>
		<?php
			$numpag = @$_GET['numpag'];
			$limreg = @$_GET['limreg'];
			$scrtop = 26 * $totreg;
		?>
		<table>
			<tr><script>barra_imagenes("serv");</script><?php cuadro_titulos();?></tr>	 

			<tr><?php menu_desplegable("serv");?></tr>

			<tr>
				<td colspan="3" class="cinta">
					<a href="serv-asignacionOtrosCobros.php" class="mgbt"><img src="imagenes/add.png"/></a>

					<a onclick="guardar()" class="mgbt"><img src="imagenes/guarda.png" title="Guardar"/></a>

					<a href="serv-buscaAsignacionOtrosCobros.php" class="mgbt"><img src="imagenes/busca.png" title="Buscar"/></a>

					<a onclick="mypop=window.open('plan-agenda.php','','');mypop.focus()" class="mgbt"><img src="imagenes/agenda1.png" title="Agenda" /></a>

					<a onclick="mypop=window.open('serv-principal.php','','');mypop.focus();" class="mgbt"><img src="imagenes/nv.png" title="Nueva ventana"></a>

					<a onclick="mypop=window.open('<?php echo $url2; ?>','','');mypop.focus();" class="mgbt"><img src="imagenes/duplicar_pantalla.png" title="Duplicar pesta&ntilde;a"></a>

					<a onClick="iratras(<?php echo "$scrtop, $numpag, $limreg, $filtro"; ?>)" class="mgbt"><img src="imagenes/iratras.png" title="Atr&aacute;s"></a>
                </td>
			</tr>
		</table>

		<div id="bgventanamodalm" class="bgventanamodalm">
			<div id="ventanamodalm" class="ventanamodalm">
				<IFRAME src="" name="ventanam" marginWidth=0 marginHeight=0 frameBorder=0 id="ventanam" frameSpacing=0 style=" width:700px; height:130px; top:200; overflow:hidden;"></IFRAME>
			</div>
		</div>

		<form name="form2" method="post" action="">
			<?php
				if(@$_POST['oculto'] == "")
				{
					$sqlr = "SELECT MIN(id), MAX(id) FROM srvasignacion_otroscobros";
					$res = mysqli_query($linkbd,$sqlr);
					$r = mysqli_fetch_row($res);

					$_POST['minimo']=$r[0];
					$_POST['maximo']=$r[1];
					
					$sqlr="SELECT * FROM srvasignacion_otroscobros WHERE id='".$_GET['idban']."'";
					$resp = mysqli_query($linkbd,$sqlr);
					$row = mysqli_fetch_row($resp); 

					$_POST['codban']	  = $row[0];
					$_POST['cliente']	  = $row[1];
					$_POST['otroCobro']   = $row[2];
					$_POST['fecha']       = date('d/m/Y',strtotime($row[3]));

					if($row[4] == 'S')
					{
						$_POST['financiado'] = "FINANCIADO";
					}
					else
					{
						$_POST['financiado'] = "NO FINANCIADO";
					}

					$_POST['valor']		  = $row[5];
					$_POST['cuotas'] 	  = $row[6];
					$_POST['onoffswitch'] = $row[7];

                    $sqlOtroCobro = "SELECT nombre FROM srvotroscobros WHERE id = '$row[2]' ";
                    $respOtroCobro = mysqli_query($linkbd,$sqlOtroCobro);
                    $rowOtroCobro = mysqli_fetch_row($respOtroCobro);

                    $sqlCliente = "SELECT id_tercero FROM srvclientes WHERE id = '$row[1]' ";
                    $respCliente = mysqli_query($linkbd,$sqlCliente);
                    $rowCliente = mysqli_fetch_row($respCliente);

                    $sqlTercero = "SELECT nombre1,nombre2,apellido1,apellido2,razonsocial FROM terceros WHERE id_tercero = '$rowCliente[0]' ";
                    $respTercero = mysqli_query($linkbd,$sqlTercero);
                    $rowTercero = mysqli_fetch_assoc($respTercero);

                    $nombreCompleto = $rowTercero['nombre1'].' '.$rowTercero['nombre2'].' '.$rowTercero['apellido1'].' '.$rowTercero['apellido2'];
                    if ($rowTercero['razonsocial']!='') 
                    {
                        $nombreCompleto = $rowTercero['razonsocial'];
                    }

                    $_POST['nCliente']   = $nombreCompleto;
                    $_POST['nOtroCobro'] = $rowOtroCobro[0];

				}
			?>
			<table class="inicio ancho">
				<tr>
					<td class="titulos" colspan="8">.: Editar Asignaci&oacute;n Otro Cobro</td>

					<td class="cerrar" style="width:7%" onClick="location.href='serv-principal.php'">Cerrar</td>
				</tr>

				<tr>
					<td class="tamano01" style="width:3cm;">C&oacute;digo:</td>

					<td style="width:15%;">
						<a onClick="atrasc(<?php echo "$scrtop, $numpag, $limreg, $filtro"; ?>)" class="icobut" title="Anterior"><img src="imagenes/back.png"/></a>

                        <input type="text" name="codban" id="codban" onKeyUp="return tabular(event,this)" value="<?php echo @ $_POST['codban'];?>" style="width:75%;height:30px;text-align:center;" readonly/>

						<a onClick="adelante(<?php echo "$scrtop, $numpag, $limreg, $filtro" ?>);" class="icobut" title="Sigiente"><img src="imagenes/next.png"/></a>
                    </td>

                    <td class="saludo1">Cliente:</td>

					<td>
						<input type="text" name='cliente' id='cliente'  value="<?php echo @$_POST['cliente']?>" style="width: 100%; height: 30px; text-align:center;" readonly>
                    </td>

                    <td>
						<input type="text" name="nCliente" id="nCliente" value="<?php echo @$_POST['nCliente']?>" style="width:100%;height:30px;" readonly>
					</td>
				</tr>

				<tr>
                    <td class="saludo1">Otro Cobro:</td>

					<td>
						<input type="text" name='otroCobro' id='otroCobro'  value="<?php echo @$_POST['otroCobro']?>" style="width: 90%; height: 30px; text-align:center;" onclick = "despliegamodal2('visible', 'srvotroscobros');"  class="colordobleclik" readonly>
                    </td>

                    <td colspan="2">
						<input type="text" name="nOtroCobro" id="nOtroCobro" value="<?php echo @$_POST['nOtroCobro']?>" style="width:100%;height:30px; text-align:center;" readonly>
					</td>
                    
					<td class="tamano01" style="width:3cm;">Fecha:</td>

					<td style="width:15%;">
                        <input type="text" name="fecha" value="<?php echo @ $_POST['fecha']?>" maxlength="10" onKeyPress="javascript:return solonumeros(event)" onKeyUp="return tabular(event,this)" id="fc_1198971545" onKeyDown="mascara(this,'/',patron,true)" title="DD/MM/YYYY" style="width:80%;">&nbsp;
                        <img src="imagenes/calendario04.png" onClick="displayCalendarFor('fc_1198971545');" title="Calendario" class="icobut"/>
                    </td>

                    
				</tr>
				
				<tr>
					<td class="saludo1">Valor a pagar:</td>

					<td>
						<input type="text" name="valor" id="valor" value="<?php echo @$_POST['valor']?>" style="height:30px; width: 197px; text-align:center;" readonly>
					</td>

					<td class="saludo1">Cobro Financiado:</td>

					<td>
						<input type="text" name="financiado" id="financiado" value="<?php echo @$_POST['financiado']?>" style="height:30px; width: 197px; text-align:center;" readonly>
					</td>

					<?php
						if($_POST['financiado'] == 'FINANCIADO')
						{
					?>
							<td class="saludo1" style="width: 68px; height: 30px">Numero de Cuotas</td>

							<td>
								<input type="number" name="cuotas" id="cuotas" value="<?php echo @$_POST['cuotas']?>" maxlength="2" style="text-align:center; height: 30px;" readonly>
							</td>
					<?php
						}
					?>
				</tr>

				<tr>
					<td class="tamano01">Estado:</td>

					<td>
						<div class="onoffswitch">
							<input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox" id="myonoffswitch" value="<?php echo @ $_POST['onoffswitch'];?>" <?php if(@$_POST['onoffswitch']=='S'){echo "checked";}?> onChange="cambiocheck();"/>
							<label class="onoffswitch-label" for="myonoffswitch">
								<span class="onoffswitch-inner"></span>
								<span class="onoffswitch-switch"></span>
							</label>
						</div>
					</td>
				</tr>
			</table>

			<input type="hidden" name="maximo" id="maximo" value="<?php echo @ $_POST['maximo']?>"/>
			<input type="hidden" name="minimo" id="minimo" value="<?php echo @ $_POST['minimo']?>"/>
			<input type="hidden" name="oculto" id="oculto" value="1"/>
			<?php
				if(@$_POST['oculto']=="2")
				{
					if (@$_POST['onoffswitch']!='S')
					{
						$valest='N';
					}
					else 
					{
						$valest='S';
					}

                    preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST['fecha'],$f);
						$fecha="$f[3]-$f[2]-$f[1]";
					
					$sqlr ="UPDATE srvasignacion_otroscobros SET estado = '$valest', fecha = '$fecha' WHERE id = '$_POST[codban]' ";
					
					if(!mysqli_query($linkbd,$sqlr)) 
					{
						
						echo "<script>despliegamodalm('visible','3','Se ha Editado con Exito');</script>";
					}
					else 
					{
						echo"<script>despliegamodalm('visible','2','No se pudo ejecutar la petición');</script>";
					}
				}
			?>
		</form>

		<div id="bgventanamodal2">
			<div id="ventanamodal2">
				<IFRAME  src="" name="buster" marginWidth=0 marginHeight=0 frameBorder=0 id="ventana2" frameSpacing=0 style="left:500px; width:900px; height:500px; top:200;"></IFRAME>
			</div>
		</div>

	</body>
</html>