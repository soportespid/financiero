<?php  
	require_once 'PHPExcel/Classes/PHPExcel.php';
	require "comun.inc";
	require "funciones.inc";
    session_start();
?>
<?php

    $objPHPExcel = new PHPExcel();
    
	//----Propiedades----
	$objPHPExcel->getProperties()
        ->setCreator("SPID")
        ->setLastModifiedBy("SPID")
        ->setTitle("Reporte Notas bancarias")
        ->setSubject("Tesoreria")
        ->setDescription("Tesoreria")
        ->setKeywords("Tesoreria")
        ->setCategory("Tesoreria");

	//----Cuerpo de Documento----
	$objPHPExcel->setActiveSheetIndex(0)
	->mergeCells('A1:E1')
	->mergeCells('A2:E2')
	->setCellValue('A1', 'TESORERÍA - NOTAS BANCARIAS')
	->setCellValue('A2', 'NOTAS BANCARIAS');
	$objPHPExcel-> getActiveSheet ()
	-> getStyle ("A1")
	-> getFill ()
	-> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
	-> getStartColor ()
	-> setRGB ('C8C8C8');
	$objPHPExcel-> getActiveSheet ()
	-> getStyle ("A1:A2")
	-> getFont ()
	-> setBold ( true ) 
	-> setName ( 'Verdana' ) 
	-> setSize ( 10 ) 
	-> getColor ()
	-> setRGB ('000000');
	$objPHPExcel-> getActiveSheet ()
	-> getStyle ('A1:A2')
	-> getAlignment ()
	-> applyFromArray (array ( 'horizontal'  =>  PHPExcel_Style_Alignment :: HORIZONTAL_CENTER ,) );
	$objPHPExcel-> getActiveSheet ()
	-> getStyle ('A3:E3')
	-> getAlignment ()
	-> applyFromArray (array ( 'horizontal'  =>  PHPExcel_Style_Alignment :: VERTICAL_JUSTIFY ,) ); 
	$objPHPExcel-> getActiveSheet ()
	-> getStyle ("A2")
	-> getFill ()
	-> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
	-> getStartColor ()
	-> setRGB ('A6E5F3');
	$objPHPExcel-> getActiveSheet ()
	-> getStyle ("A3:E3")
	-> getFill ()
	-> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
	-> getStartColor ()
	-> setRGB ('22C6CB');
	$borders = array(
		'borders' => array(
			'allborders' => array(
				'style' => PHPExcel_Style_Border::BORDER_THIN,
			'color' => array('argb' => 'FF000000'),
			)
		),
	);
	$objPHPExcel->getActiveSheet()->getStyle('A1:E1')->applyFromArray($borders);
	$objPHPExcel->getActiveSheet()->getStyle('A2:E2')->applyFromArray($borders);
	$objPHPExcel->getActiveSheet()->getStyle('A3:E3')->applyFromArray($borders);
	$objWorksheet = $objPHPExcel->getActiveSheet();
    
	$objPHPExcel->setActiveSheetIndex(0)
	->setCellValue('A3', 'Nota bancaria')
    ->setCellValue('B3', 'Fecha')
    ->setCellValue('C3', 'Concepto')
	->setCellValue('D3', 'Valor')
	->setCellValue('E3', 'Estado');
	
    $i=4;
    for($xx=0; $xx<count($_POST['notaBancaria']); $xx++)
    {
		$estadoContrato='';

        if($_POST['estadoNota'][$xx]=='N' || $_POST['estadoNota'][$xx]=='R' || $_POST['estadoNota'][$xx]=='')
            $estadoContrato = 'Anulado';
        else
            $estadoContrato = 'Activo';
        
        $objPHPExcel->setActiveSheetIndex(0)
		    ->setCellValueExplicit ("A$i", $_POST['notaBancaria'][$xx], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
            ->setCellValueExplicit ("B$i", $_POST['fechaNota'][$xx], PHPExcel_Cell_DataType :: TYPE_STRING)
            ->setCellValueExplicit ("C$i", $_POST['conceptoNota'][$xx], PHPExcel_Cell_DataType :: TYPE_STRING)
		    ->setCellValueExplicit ("D$i", $_POST['valorNota'][$xx], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
		    ->setCellValueExplicit ("E$i", $estadoContrato, PHPExcel_Cell_DataType :: TYPE_STRING);
        
            $objPHPExcel->getActiveSheet()->getStyle("A$i:E$i")->applyFromArray($borders);
		$i++;
    }
		
	//----Propiedades de la hoja 1
	$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth('60');
	$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->setTitle('Notas bancarias');
	$objPHPExcel->setActiveSheetIndex(0);

//----Guardar documento----
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename="Teso - Notas Bancarias.xls"');
header('Cache-Control: max-age=0');
 
$objWriter=PHPExcel_IOFactory::createWriter($objPHPExcel,'Excel5');
$objWriter->save('php://output');
exit;

?>