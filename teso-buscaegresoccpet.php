<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");

	require "comun.inc";
	require "funciones.inc";
	require "conversor.php";
	require "validaciones.inc";

	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");

	session_start();
	date_default_timezone_set("America/Bogota");

?>
<!DOCTYPE >
<html lang="es">
	<head>
		<meta charset="utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1"/>
		<meta name="viewport" content="user-scalable=no">
		<title>:: IDEAL 10 - Tesorer&iacute;a</title>
		<link href="favicon.ico" rel="shortcut icon"/>
        <link href="css/css2.css" rel="stylesheet" type="text/css" />
        <link href="css/css3.css" rel="stylesheet" type="text/css" />
        <link href="css/tabs.css" rel="stylesheet" type="text/css"/>
		<script type="text/javascript" src="jquery-1.11.0.min.js"></script>
        <script type="text/javascript" src="css/programas.js"></script>
		<script type="text/javascript" src="css/calendario.js"></script>
		<script>
			function verUltimaPos(idcta, filas, filtro){
				var scrtop=$('#divdet').scrollTop();
				var altura=$('#divdet').height();
				var numpag=$('#nummul').val();
				var limreg=$('#numres').val();
				if((numpag<=0)||(numpag==""))
					numpag=0;
				if((limreg==0)||(limreg==""))
					limreg=10;
				numpag++;
				var fi=document.getElementById("fc_1198971545").value;
				var ff=document.getElementById("fc_1198971546").value;
				location.href="teso-egresoverccpet.php?idop="+idcta+"&scrtop="+scrtop+"&totreg="+filas+"&altura="+altura+"&numpag="+numpag+"&limreg="+limreg+"&filtro="+filtro+"&fini="+fi+"&ffin="+ff;
			}


			function crearexcel(){
				document.form2.action="teso-buscaegresoexcel.php";
				document.form2.target="_BLANK";
				document.form2.submit();
				//refrescar();
			}

			function eliminar(idr){
				if (confirm("Esta Seguro de Eliminar la liquidacion No "+idr)){
				document.form2.oculto.value=2;
				document.form2.var1.value=idr;
				document.form2.submit();
				}
			}
		</script>
        <?php
				if(isset($_GET['fini']) && isset($_GET['ffin'])){
					if(!empty($_GET['fini']) && !empty($_GET['ffin'])){
						$_POST['fecha']=$_GET['fini'];
						$_POST['fecha2']=$_GET['ffin'];
					}
				}
				$fech1=explode("/",$_POST['fecha']);
				$fech2=explode("/",$_POST['fecha2']);
				$f1=$fech1[2]."-".$fech1[1]."-".$fech1[0];
				$f2=$fech2[2]."-".$fech2[1]."-".$fech2[0];

		$scrtop=$_GET['scrtop'];
		if($scrtop=="") $scrtop=0;
		echo"<script>
			window.onload=function(){
				$('#divdet').scrollTop(".$scrtop.")
			}
		</script>";
		$gidcta=$_GET['idcta'];
		if(isset($_GET['filtro']))
			$_POST['nombre']=$_GET['filtro'];
		?>
    </head>
    <body>
        <IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
        <span id="todastablas2"></span>
        <table>
            <tr><script>barra_imagenes("teso");</script><?php cuadro_titulos();?></tr>
            <tr><?php menu_desplegable("teso");?></tr>

        </table>
        <div class="bg-white group-btn p-1">
            <button type="button" onclick="window.location.href='teso-egreso-ccpet.php'" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
                <span>Nuevo</span>
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960"><path d="M440-440H200v-80h240v-240h80v240h240v80H520v240h-80v-240Z"></path></svg>
            </button>
            <button type="button" onclick="mypop=window.open('plan-agenda.php','','');mypop.focus()" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
                <span class="group-hover:text-white">Agenda</span>
                <svg class="fill-black group-hover:fill-white w-5 h-5" xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960"><path d="M560-564v-68q33-14 67.5-21t72.5-7q26 0 51 4t49 10v64q-24-9-48.5-13.5T700-600q-38 0-73 9.5T560-564Zm0 220v-68q33-14 67.5-21t72.5-7q26 0 51 4t49 10v64q-24-9-48.5-13.5T700-380q-38 0-73 9t-67 27Zm0-110v-68q33-14 67.5-21t72.5-7q26 0 51 4t49 10v64q-24-9-48.5-13.5T700-490q-38 0-73 9.5T560-454ZM260-320q47 0 91.5 10.5T440-278v-394q-41-24-87-36t-93-12q-36 0-71.5 7T120-692v396q35-12 69.5-18t70.5-6Zm260 42q44-21 88.5-31.5T700-320q36 0 70.5 6t69.5 18v-396q-33-14-68.5-21t-71.5-7q-47 0-93 12t-87 36v394Zm-40 118q-48-38-104-59t-116-21q-42 0-82.5 11T100-198q-21 11-40.5-1T40-234v-482q0-11 5.5-21T62-752q46-24 96-36t102-12q58 0 113.5 15T480-740q51-30 106.5-45T700-800q52 0 102 12t96 36q11 5 16.5 15t5.5 21v482q0 23-19.5 35t-40.5 1q-37-20-77.5-31T700-240q-60 0-116 21t-104 59ZM280-494Z"></path></svg>
            </button>
            <button type="button" onclick="window.open('teso-principal');" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
                <span>Nueva ventana</span>
                <svg class="fill-black group-hover:fill-white w-5 h-5" xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960"><path d="M320-120v-80h80v-80H160q-33 0-56.5-23.5T80-360v-400q0-33 23.5-56.5T160-840h640q33 0 56.5 23.5T880-760v400q0 33-23.5 56.5T800-280H560v80h80v80H320ZM160-360h640v-400H160v400Zm0 0v-400 400Z"></path></svg>
            </button>
            <button type="button" onclick="mypop=window.open('/financiero/teso-buscaegresoccpet.php','','');mypop.focus();" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
                <span class="group-hover:text-white">Duplicar pantalla</span>
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960"><path d="M640-160v-360H160v360h480Zm80-200v-80h80v-360H320v200h-80v-200q0-33 23.5-56.5T320-880h480q33 0 56.5 23.5T880-800v360q0 33-23.5 56.5T800-360h-80ZM160-80q-33 0-56.5-23.5T80-160v-360q0-33 23.5-56.5T160-600h480q33 0 56.5 23.5T720-520v360q0 33-23.5 56.5T640-80H160Zm400-603ZM400-340Z"></path></svg>
            </button>
            <button type="button" onclick="crearexcel()" class="btn btn-success btn-success-hover d-flex justify-between align-items-center">
                <span>Exportar Excel</span>
                <svg class="fill-black group-hover:fill-white w-4 h-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 384 512"><!--!Font Awesome Free 6.5.1 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license/free Copyright 2024 Fonticons, Inc. --><path d="M48 448V64c0-8.8 7.2-16 16-16H224v80c0 17.7 14.3 32 32 32h80V448c0 8.8-7.2 16-16 16H64c-8.8 0-16-7.2-16-16zM64 0C28.7 0 0 28.7 0 64V448c0 35.3 28.7 64 64 64H320c35.3 0 64-28.7 64-64V154.5c0-17-6.7-33.3-18.7-45.3L274.7 18.7C262.7 6.7 246.5 0 229.5 0H64zm90.9 233.3c-8.1-10.5-23.2-12.3-33.7-4.2s-12.3 23.2-4.2 33.7L161.6 320l-44.5 57.3c-8.1 10.5-6.3 25.5 4.2 33.7s25.5 6.3 33.7-4.2L192 359.1l37.1 47.6c8.1 10.5 23.2 12.3 33.7 4.2s12.3-23.2 4.2-33.7L222.4 320l44.5-57.3c8.1-10.5 6.3-25.5-4.2-33.7s-25.5-6.3-33.7 4.2L192 280.9l-37.1-47.6z"></path></svg>
            </button>
        </div>
        <?php
		if($_GET['numpag']!=""){
			$oculto=$_POST['oculto'];
			if($oculto!=2){
				$_POST['numres']=$_GET['limreg'];
				$_POST['numpos']=$_GET['limreg']*($_GET['numpag']-1);
				$_POST['nummul']=$_GET['numpag']-1;
			}
		}
		else{
			if($_POST['nummul']==""){
				$_POST['numres']=10;
				$_POST['numpos']=0;
				$_POST['nummul']=0;
			}
		}
		?>
 		<form name="form2" method="post" action="teso-buscaegresoccpet.php">
 		<?php
				if ($_POST['oculto']==""){$_POST['iddeshff']=0;$_POST['tabgroup1']=1;}
				 switch($_POST['tabgroup1'])
                {
                    case 1:	$check1='checked';break;
                    case 2:	$check2='checked';break;
                    case 3:	$check3='checked';break;
                }
			?>
         	<input type="hidden" name="numres" id="numres" value="<?php echo $_POST['numres'];?>"/>
       		<input type="hidden" name="numpos" id="numpos" value="<?php echo $_POST['numpos'];?>"/>
         	<input type="hidden" name="nummul" id="nummul" value="<?php echo $_POST['nummul'];?>"/>
         	<input name="excel"  id="excel"  type="hidden" value="<?php echo $_POST['excel'] ?>" >
			<table  class="inicio" align="center" >
      			<tr>
        			<td class="titulos" colspan="8">:. Buscar Liquidaciones CxP</td>
        			<td class="cerrar" style='width:7%'><a href="teso-principal.php">&nbsp;Cerrar</a></td>
              	</tr>
              	<tr>
              		<td class="saludo1" style="width:20%">Fecha Inicial:</td>
       				<td style="width:12%;"><input name="fecha"  type="text" value="<?php echo $_POST['fecha']?>" maxlength="10" onchange="" onKeyPress="javascript:return solonumeros(event)" onKeyUp="return tabular(event,this)"  id="fc_1198971545" onKeyDown="mascara(this,'/',patron,true)" style="width:80%;" title="DD/MM/YYYY"/>&nbsp;<a href="#" onClick="displayCalendarFor('fc_1198971545');" title="Calendario"><img src="imagenes/calendario04.png" style="width:20px;"/></a></td>
       				<td class="saludo1" style="width:3.1cm;">Fecha Final:</td>
       				<td style="width:12%;"><input name="fecha2" type="text" value="<?php echo $_POST['fecha2']?>" maxlength="10" onchange="" onKeyPress="javascript:return solonumeros(event)" onKeyUp="return tabular(event,this)"  id="fc_1198971546" onKeyDown="mascara(this,'/',patron,true)" style="width:80%;" title="DD/MM/YYYY"/>&nbsp;<a href="#" onClick="displayCalendarFor('fc_1198971546');" title="Calendario"><img src="imagenes/calendario04.png" style="width:20px;"/></a></td>
                 	<td style="width:4.5cm" class="saludo1">N&uacute;mero o Detalle Orden: </td>
    				<td >
                		<input type="search" name="nombre" id="nombre" value="<?php echo $_POST['nombre'];?>" style="width:100%" >
               		</td>
       			</tr>
				<tr>
					<td class="saludo1">:. N&deg; Documento, Nombre o Razon Social:</td>
                	<td colspan="4">
                    	<input name="nombreTercero" type="text" style="width:100%" value="<?php echo $_POST['nombreTercero']; ?>">
                    	<input name="oculto" id="oculto" type="hidden" value="1">
                	</td>
					<td>
						<input type="button" name="bboton" onClick="limbusquedas();" value="&nbsp;&nbsp;Buscar&nbsp;&nbsp;" />
					</td>

				</tr>
    		</table>
            <input name="oculto" id="oculto" type="hidden" value="1">
            <input name="var1" type="hidden" value=<?php echo $_POST['var1'];?>>
    		<script>
    			document.form2.nombre.focus();
	    		document.form2.nombre.select();
    		</script>
    		<input type="hidden" name="fecham1"  id="fecham1" value="<?php echo $_POST['fecham1']; ?>"/>
    		<input type="hidden" name="fecham2" id="fecham2" value="<?php echo $_POST['fecham2']; ?>"/>
     		<div class="tabsmeci" style="height:64.5%; width:99.6%;">
             	<div class="tab">
                    <input type="radio" id="tab-1" name="tabgroup1" value="1" <?php echo $check1;?> >
                    <label for="tab-1">CxP General</label>
           	<div class="content" style="overflow-x:hidden;" id="divdet">
      			<?php
	  			$vigusu=vigencia_usuarios($_SESSION['cedulausu']);

				$_POST['fecham1']=$f1;
				$_POST['fecham2']=$f2;
				//$_POST[fecha]=$f1;
				//$_POST[fecha2]=$f2;

				if($_POST['oculto']==2){
				 	$sqlr="select * from tesoordenpago where id_orden=$_POST[var1] and tipo_mov='201' ";
				 	$resp = mysqli_query($linkbd, $sqlr);
				 	$row = mysqli_fetch_row($resp);
				 	$rpe=$row[4];
				 	$vpa=$row[10];
				 	$nop=$row[0];
				 	$vigop=$row[3];
				 	//********Comprobante contable en 000000000000
				  	$sqlr="update comprobante_cab set total_debito=0,total_credito=0,estado='0' where tipo_comp='11' and numerotipo=$row[0]";
				  	mysqli_query($linkbd, $sqlr);
				  	$sqlr="update comprobante_det set valdebito=0,valcredito=0 where id_comp='11 $row[0]'";
				  	mysqli_query($linkbd, $sqlr);
				 	//***anular ppto
				  	$sqlr="update pptocomprobante_cab set estado='0' where tipo_comp='8' and numerotipo=$row[0]";
				  	mysqli_query($linkbd, $sqlr);
				  	$sqlr="update pptocomprobante_det set estado='0' where tipo_comp='8' and numerotipo=$row[0]";
				  	mysqli_query($linkbd, $sqlr);
					$sqlr="update pptocomprobante_det set estado='0' where tipo_comp='7' and numerotipo=$row[4] and doc_receptor=$row[0]";
				  	mysqli_query($linkbd, $sqlr);
				 	//********PREDIAL O RECAUDO SE ACTIVA COLOCAR 'S'
				  	$sqlr="select * from tesoordenpago_det where id_orden=$nop";
				  	$resp=mysqli_query($linkbd, $sqlr);
				  	while($r=mysqli_fetch_row($resp)){
						$sqlr="update pptorp_detalle set saldo=saldo+$r[4] where cuenta='$r[2]' and vigencia='".$vigop."' and consvigencia=$rpe and tipo_mov='201' ";
						mysqli_query($linkbd, $sqlr);
					//	echo "<br>".$sqlr;
				   	}
				   	$sqlr="update tesoordenpago  set estado='N' where id_orden=$_POST[var1] and tipo_mov='201' ";
				  	mysqli_query($linkbd, $sqlr);
				  	$sqlr="update pptorp set saldo=saldo+$vpa where vigencia='".$vigop."' and consvigencia=$rpe and tipo_mov='201' ";
				  	mysqli_query($linkbd, $sqlr);
				}

				//******
				$crit1="";
				$crit2=[];
				$criTercero="";
				if(isset($_POST['nombreTercero'])){
					if ($_POST['nombreTercero']!="")
					{
						$criTercero="and concat_ws(' ', nombre1, nombre2, apellido1, apellido2, razonsocial, cedulanit) LIKE '%$_POST[nombreTercero]%'";
						//sacar el consecutivo
						$sqlrTercero="select cedulanit from terceros where terceros.estado='S' ".$criTercero." order by terceros.apellido1,terceros.apellido2,terceros.nombre1,terceros.nombre2,terceros.razonsocial";
						$respTercero = mysqli_query($linkbd, $sqlrTercero);
						while($rowTercero =mysqli_fetch_row($respTercero))
						{
							$crit2[] = " AND tercero LIKE '%$rowTercero[0]%'";
						}

					}
				}
				if(isset($_POST['nombre'])){
					if ($_POST['nombre']!="")
						$crit1="and concat_ws(' ', id_orden, conceptorden) LIKE '%$_POST[nombre]%'";
				}
				//sacar el consecutivo
				if(count($crit2)>1)
				{
					//$resp = mysqli_query($linkbd, $sqlr);
					$numcontrol=doubleVal($_POST['nummul'])+1;
					if(($nuncilumnas==$numcontrol)||($_POST['numres']=="-1")){
						$imagenforward="<img src='imagenes/forward02.png' style='width:17px'>";
						$imagensforward="<img src='imagenes/skip_forward02.png' style='width:16px' >";
					}
					else{
						$imagenforward="<img src='imagenes/forward01.png' style='width:17px' title='Siguiente' onClick='numsiguiente()'>";
						$imagensforward="<img src='imagenes/skip_forward01.png' style='width:16px' title='Fin' onClick='saltocol(\"$nuncilumnas\")'>";
					}
					if(($_POST['numpos']==0)||($_POST['numres']=="-1")){
						$imagenback="<img src='imagenes/back02.png' style='width:17px'>";
						$imagensback="<img src='imagenes/skip_back02.png' style='width:16px'>";
					}
					else{
						$imagenback="<img src='imagenes/back01.png' style='width:17px' title='Anterior' onClick='numanterior();'>";
						$imagensback="<img src='imagenes/skip_back01.png' style='width:16px' title='Inicio' onClick='saltocol(\"1\")'>";
					}

					$con=1;
					echo "<table class='inicio' align='center'>
						<tr>
							<td colspan='8' class='titulos'>.: Resultados Busqueda:</td>
							<td class='submenu'>
								<select name='renumres' id='renumres' onChange='cambionum();' style='width:100%'>
									<option value='10'"; if ($_POST['renumres']=='10'){echo 'selected';} echo ">10</option>
									<option value='20'"; if ($_POST['renumres']=='20'){echo 'selected';} echo ">20</option>
									<option value='30'"; if ($_POST['renumres']=='30'){echo 'selected';} echo ">30</option>
									<option value='50'"; if ($_POST['renumres']=='50'){echo 'selected';} echo ">50</option>
									<option value='100'"; if ($_POST['renumres']=='100'){echo 'selected';} echo ">100</option>
									<option value='-1'"; if ($_POST['renumres']=='-1'){echo 'selected';} echo ">Todos</option>
								</select>
							</td>
						</tr>
						<tr>
							<td class='titulos2' style='width:3%'>Item</td>
							<td class='titulos2' style='width:5%'>Vigencia</td>
							<td class='titulos2' style='width:3%'>N RP</td>
							<td class='titulos2'>Nombre</td>
							<td class='titulos2' >Detalle</td>
							<td class='titulos2' >Valor</td>
							<td class='titulos2' style='width:6.5%'>Fecha</td>
							<td class='titulos2' style='width:5%'>Estado</td>
							<td class='titulos2' style='width:5%'>Ver</td>
						</tr>";
						$iter='saludo1a';
						$iter2='saludo2';
						$filas=1;

					for($xx=0;$xx<count($crit2);$xx++)
					{
						if($_POST['nombre']!="" || $_POST['fecha']!="" || $_POST['fecha2']!="" || $_POST['nombreTercero']!="")
						{
							$sqlr="select * from tesoordenpago where tesoordenpago.id_orden>-1 $crit1 $crit2[$xx] and tipo_mov='201' order by tesoordenpago.id_orden DESC";
							if(isset($_POST['fecha']) && isset($_POST['fecha2'])){
								if(!empty($_POST['fecha']) && !empty($_POST['fecha2'])){
									$sqlr="select * from tesoordenpago where tesoordenpago.id_orden>-1 $crit1 $crit2[$xx] and fecha between '$f1' AND '$f2' and tipo_mov='201' order by tesoordenpago.id_orden DESC";
								}
							}
							$resp = mysqli_query($linkbd, $sqlr);
							$ntr = mysqli_num_rows($resp);
							$_POST['numtop']=$ntr;
							$nuncilumnas=ceil($_POST['numtop']/$_POST['numres']);
							$cond2="";
							if ($_POST['numres']!="-1"){
								$cond2="LIMIT $_POST[numpos], $_POST[numres]";
							}
							$sqlr="select * from tesoordenpago where tesoordenpago.id_orden>-1 $crit1 $crit2[$xx] and tipo_mov='201' order by tesoordenpago.id_orden desc $cond2";
							if(isset($_POST['fecha']) && isset($_POST['fecha2'])){
								if(!empty($_POST['fecha']) && !empty($_POST['fecha2'])){
									$sqlr="select * from tesoordenpago where tesoordenpago.id_orden>-1 $crit1 $crit2[$xx] and fecha between '$f1' AND '$f2' and tipo_mov='201' order by tesoordenpago.id_orden DESC";
								}
							}

						}
						while ($row =mysqli_fetch_row($resp)){
							$detalle=$row[2];
							$ntr=buscatercero($row[6]);
							switch ($row[13]){
								case "S":
									$imagen="src='imagenes/confirm.png' title='Activo'";
									$camcelda="<td style='text-align:center;'><a href='#' onClick=eliminar($row[0])><img src='imagenes/anular.png' title='Anular'></a></td>";
									break;
								case "P":
									$imagen="src='imagenes/dinero3.png' title='Pago'";
									$camcelda="<td style='text-align:center;'><img src='imagenes/candado.png' title='bloqueado' style='width:18px'/></td>";
									break;
								case "N":
									$imagen="src='imagenes/cross.png' title='Anulado'";
									$camcelda="<td style='text-align:center;'><img src='imagenes/candado.png' title='bloqueado' style='width:18px'/></td>";
								case "R":
									$imagen="src='imagenes/reversado.png' title='Reversado'";
									$camcelda="<td style='text-align:center;'><img src='imagenes/candado.png' title='bloqueado' style='width:18px'/></td>";
							}
							if($gidcta!=""){
								if($gidcta==$row[0]){
									$estilo='background-color:#FF9';
								}
								else{
									$estilo="";
								}
							}
							else{
								$estilo="";
							}
							$idcta="'$row[0]'";
							$numfil="'$filas'";
							$filtro="'$_POST[nombre]'";
							echo"<tr class='$iter' onMouseOver=\"anterior=this.style.backgroundColor;this.style.backgroundColor='#40b3ff';\"
				onMouseOut=\"this.style.backgroundColor=anterior\" onDblClick=\"verUltimaPos($idcta, $numfil, $filtro)\" style='text-transform:uppercase; $estilo' >
								<td>$row[0]</td>
								<td>$row[3]</td>
								<td>$row[4]</td>
								<td >$row[6] - $ntr</td>
								<td>$row[7]</td>
								<td style='text-align:right;'>$ ".number_format($row[10],$_SESSION["ndecimales"],$_SESSION["spdecimal"],$_SESSION["spmillares"])."&nbsp;&nbsp;</td>
								<td>".date('d-m-Y',strtotime($row[2]))."</td>
								<td style='text-align:center;'><img $imagen style='width:18px'></td>";
								echo"<td style='text-align:center;'>
									<a onClick=\"verUltimaPos($idcta, $numfil, $filtro)\" style='cursor:pointer;'>
										<img src='imagenes/lupa02.png' style='width:18px' title='Ver'>
									</a>
								</td>
							</tr>";
							$con+=1;
							$aux=$iter;
							$iter=$iter2;
							$iter2=$aux;
							$filas++;
						}

					}
					echo"</table>
							<table class='inicio'>
								<tr>
									<td style='text-align:center;'>
										<a href='#'>$imagensback</a>&nbsp;
										<a href='#'>$imagenback</a>&nbsp;&nbsp;";
						if($nuncilumnas<=9){$numfin=$nuncilumnas;}
						else{$numfin=9;}
						for($xx = 1; $xx <= $numfin; $xx++)
						{
							if($numcontrol<=9){$numx=$xx;}
							else{$numx=$xx+($numcontrol-9);}
							if($numcontrol==$numx){echo"<a href='#' onClick='saltocol(\"$numx\")'; style='color:#24D915'> $numx </a>";}
							else {echo"<a href='#' onClick='saltocol(\"$numx\")'; style='color:#000000'> $numx </a>";}
						}
						echo "		&nbsp;&nbsp;<a href='#'>$imagenforward</a>
										&nbsp;<a href='#'>$imagensforward</a>
									</td>
								</tr>
							</table>";
				}
				else
				{
					if($_POST['nombre']!="" || $_POST['fecha']!="" || $_POST['fecha2']!="" || $crit2[0]!="" )
					{
						$sqlr="select * from tesoordenpago where tesoordenpago.id_orden>-1 $crit1 $crit2[0] and tipo_mov='201' order by tesoordenpago.id_orden DESC";
						if(isset($_POST['fecha']) && isset($_POST['fecha2'])){
							if(!empty($_POST['fecha']) && !empty($_POST['fecha2'])){
								$sqlr="select * from tesoordenpago where tesoordenpago.id_orden>-1 $crit1 $crit2[0] and fecha between '$f1' AND '$f2' and tipo_mov='201' order by tesoordenpago.id_orden DESC";
							}
						}

						$resp = mysqli_query($linkbd, $sqlr);
						$ntr = mysqli_num_rows($resp);
						$_POST['numtop']=$ntr;
						$nuncilumnas=ceil($_POST['numtop']/$_POST['numres']);
						$cond2="";
						if ($_POST['numres']!="-1"){
							$cond2="LIMIT $_POST[numpos], $_POST[numres]";
						}
						$sqlr="select * from tesoordenpago where tesoordenpago.id_orden>-1 $crit1 $crit2[0] and tipo_mov='201' order by tesoordenpago.id_orden desc $cond2";
						if(isset($_POST['fecha']) && isset($_POST['fecha2'])){
							if(!empty($_POST['fecha']) && !empty($_POST['fecha2'])){
								$sqlr="select * from tesoordenpago where tesoordenpago.id_orden>-1 $crit1 $crit2[0] and fecha between '$f1' AND '$f2' and tipo_mov='201' order by tesoordenpago.id_orden DESC";
							}
						}
						//echo $sqlr;
					}

					if(isset($sqlr)){
						$resp = mysqli_query($linkbd, $sqlr);
					}
					$numcontrol=doubleVal($_POST['nummul'])+1;
					if(($nuncilumnas==$numcontrol)||($_POST['numres']=="-1")){
						$imagenforward="<img src='imagenes/forward02.png' style='width:17px'>";
						$imagensforward="<img src='imagenes/skip_forward02.png' style='width:16px' >";
					}
					else{
						$imagenforward="<img src='imagenes/forward01.png' style='width:17px' title='Siguiente' onClick='numsiguiente()'>";
						$imagensforward="<img src='imagenes/skip_forward01.png' style='width:16px' title='Fin' onClick='saltocol(\"$nuncilumnas\")'>";
					}
					if(($_POST['numpos']==0)||($_POST['numres']=="-1")){
						$imagenback="<img src='imagenes/back02.png' style='width:17px'>";
						$imagensback="<img src='imagenes/skip_back02.png' style='width:16px'>";
					}
					else{
						$imagenback="<img src='imagenes/back01.png' style='width:17px' title='Anterior' onClick='numanterior();'>";
						$imagensback="<img src='imagenes/skip_back01.png' style='width:16px' title='Inicio' onClick='saltocol(\"1\")'>";
					}

					$con=1;
					echo "<table class='inicio' align='center'>
						<tr>
							<td colspan='8' class='titulos'>.: Resultados Busqueda:</td>
							<td class='submenu'>
								<select name='renumres' id='renumres' onChange='cambionum();' style='width:100%'>
									<option value='10'"; if ($_POST['renumres']=='10'){echo 'selected';} echo ">10</option>
									<option value='20'"; if ($_POST['renumres']=='20'){echo 'selected';} echo ">20</option>
									<option value='30'"; if ($_POST['renumres']=='30'){echo 'selected';} echo ">30</option>
									<option value='50'"; if ($_POST['renumres']=='50'){echo 'selected';} echo ">50</option>
									<option value='100'"; if ($_POST['renumres']=='100'){echo 'selected';} echo ">100</option>
									<option value='-1'"; if ($_POST['renumres']=='-1'){echo 'selected';} echo ">Todos</option>
								</select>
							</td>
						</tr>
						<tr>
							<td class='titulos2' style='width:3%'>Item</td>
							<td class='titulos2' style='width:5%'>Vigencia</td>
							<td class='titulos2' style='width:3%'>N RP</td>
							<td class='titulos2'>Nombre</td>
							<td class='titulos2' >Detalle</td>
							<td class='titulos2' >Valor</td>
							<td class='titulos2' style='width:6.5%'>Fecha</td>
							<td class='titulos2' style='width:5%'>Estado</td>
							<td class='titulos2' style='width:5%'>Ver</td>
						</tr>";
						$iter='saludo1a';
						$iter2='saludo2';
						$filas=1;

						if(isset($resp)){
							while ($row =mysqli_fetch_row($resp)){
								$detalle=$row[2];
								$ntr=buscatercero($row[6]);
								switch ($row[13]){
									case "S":
										$imagen="src='imagenes/confirm.png' title='Activo'";
										$camcelda="<td style='text-align:center;'><a href='#' onClick=eliminar($row[0])><img src='imagenes/anular.png' title='Anular'></a></td>";
										break;
									case "P":
										$imagen="src='imagenes/dinero3.png' title='Pago'";
										$camcelda="<td style='text-align:center;'><img src='imagenes/candado.png' title='bloqueado' style='width:18px'/></td>";
										break;
									case "N":
										$imagen="src='imagenes/cross.png' title='Anulado'";
										$camcelda="<td style='text-align:center;'><img src='imagenes/candado.png' title='bloqueado' style='width:18px'/></td>";
									case "R":
										$imagen="src='imagenes/reversado.png' title='Reversado'";
										$camcelda="<td style='text-align:center;'><img src='imagenes/candado.png' title='bloqueado' style='width:18px'/></td>";
								}
								if($gidcta!=""){
									if($gidcta==$row[0]){
										$estilo='background-color:#FF9';
									}
									else{
										$estilo="";
									}
								}
								else{
									$estilo="";
								}
								$idcta="'$row[0]'";
								$numfil="'$filas'";
								$filtro="'$_POST[nombre]'";
								echo"<tr class='$iter' onMouseOver=\"anterior=this.style.backgroundColor;this.style.backgroundColor='#40b3ff';\"
					onMouseOut=\"this.style.backgroundColor=anterior\" onDblClick=\"verUltimaPos($idcta, $numfil, $filtro)\" style='text-transform:uppercase; $estilo' >
									<td>$row[0]</td>
									<td>$row[3]</td>
									<td>$row[4]</td>
									<td >$row[6] - $ntr</td>
									<td>$row[7]</td>
									<td style='text-align:right;'>$ ".number_format($row[10],$_SESSION["ndecimales"],$_SESSION["spdecimal"],$_SESSION["spmillares"])."&nbsp;&nbsp;</td>
									<td>".date('d-m-Y',strtotime($row[2]))."</td>
									<td style='text-align:center;'><img $imagen style='width:18px'></td>";
									echo"<td style='text-align:center;'>
										<a onClick=\"verUltimaPos($idcta, $numfil, $filtro)\" style='cursor:pointer;'>
											<img src='imagenes/lupa02.png' style='width:18px' title='Ver'>
										</a>
									</td>
								</tr>";
								$con+=1;
								$aux=$iter;
								$iter=$iter2;
								$iter2=$aux;
								$filas++;
							}
						}
							echo"</table>
							<table class='inicio'>
								<tr>
									<td style='text-align:center;'>
										<a href='#'>$imagensback</a>&nbsp;
										<a href='#'>$imagenback</a>&nbsp;&nbsp;";
						if(doubleVal($nuncilumnas)<=9){$numfin=$nuncilumnas;}
						else{$numfin=9;}
						for($xx = 1; $xx <= $numfin; $xx++)
						{
							if(doubleVal($numcontrol)<=9){$numx=$xx;}
							else{$numx=doubleVal($xx)+(doubleVal($numcontrol)-9);}
							if($numcontrol==$numx){echo"<a href='#' onClick='saltocol(\"$numx\")'; style='color:#24D915'> $numx </a>";}
							else {echo"<a href='#' onClick='saltocol(\"$numx\")'; style='color:#000000'> $numx </a>";}
						}
						echo "		&nbsp;&nbsp;<a href='#'>$imagenforward</a>
									&nbsp;<a href='#'>$imagensforward</a>
								</td>
							</tr>
						</table>";
				}
?></div>
            	</div>
          <div class="tab">
                    <input type="radio" id="tab-2" name="tabgroup1" value="2" <?php echo $check2;?> >
                    <label for="tab-2">CxP Reversado</label>
           	<div class="content" style="overflow-x:hidden;" id="divdet">
      			<?php
	  			$vigusu=vigencia_usuarios($_SESSION['cedulausu']);
				$_POST['vigencia']=$vigusu;
				$oculto=$_POST['oculto'];
				$fech1=explode("/",$_POST['fecha']);
				$fech2=explode("/",$_POST['fecha2']);
				$f1=$fech1[2]."-".$fech1[1]."-".$fech1[0];
				$f2=$fech2[2]."-".$fech2[1]."-".$fech2[0];
				if($_POST['oculto']==2){

				 	$sqlr="select * from tesoordenpago where id_orden=$_POST[var1] and vigencia=$vigusu and tipo_mov='401' ";
				 	if(isset($_POST['fecha']) && isset($_POST['fecha2'])){
					if(!empty($_POST['fecha']) && !empty($_POST['fecha2'])){
						$sqlr="select * from tesoordenpago where id_orden=$_POST[var1] and fecha between '$f1' AND '$f2' and tipo_mov='401' order by tesoordenpago.id_orden DESC";
					}
				}

				 	$resp = mysqli_query($linkbd, $sqlr);
				 	$row = mysqli_fetch_row($resp);
				 	$rpe=$row[4];
				 	$vpa=$row[10];
				 	$nop=$row[0];
				 	$vigop=$row[3];
				 	//********Comprobante contable en 000000000000
				  	$sqlr="update comprobante_cab set total_debito=0,total_credito=0,estado='0' where tipo_comp='11' and numerotipo=$row[0]";
				  	mysqli_query($linkbd, $sqlr);
				  	$sqlr="update comprobante_det set valdebito=0,valcredito=0 where id_comp='11 $row[0]'";
				  	mysqli_query($linkbd, $sqlr);
				 	//***anular ppto
				  	$sqlr="update pptocomprobante_cab set estado='0' where tipo_comp='8' and numerotipo=$row[0]";
				  	mysqli_query($linkbd, $sqlr);
				  	$sqlr="update pptocomprobante_det set estado='0' where tipo_comp='8' and numerotipo=$row[0]";
				  	mysqli_query($linkbd, $sqlr);
					$sqlr="update pptocomprobante_det set estado='0' where tipo_comp='7' and numerotipo=$row[4] and doc_receptor=$row[0]";
				  	mysqli_query($linkbd, $sqlr);
				 	//********PREDIAL O RECAUDO SE ACTIVA COLOCAR 'S'
				  	$sqlr="select * from tesoordenpago_det where id_orden=$nop and tipo_mov='201' ";
				  	$resp=mysqli_query($linkbd, $sqlr);
				  	while($r=mysqli_fetch_row($resp)){
						$sqlr="update pptorp_detalle set saldo=saldo+$r[4] where cuenta='$r[2]' and vigencia='".$vigop."' and consvigencia=$rpe and tipo_mov='201' ";
						mysqli_query($linkbd, $sqlr);
					//	echo "<br>".$sqlr;
				   	}
				   	$sqlr="update tesoordenpago  set estado='N' where id_orden=$_POST[var1] and tipo_mov='201' ";
				  	mysqli_query($linkbd, $sqlr);
				  	$sqlr="update pptorp set saldo=saldo+$vpa where vigencia='".$vigop."' and consvigencia=$rpe and tipo_mov='201' ";
				  	mysqli_query($linkbd, $sqlr);
				}

				//******
				$crit1="";
				if ($_POST['nombre']!="")
					$crit1="and concat_ws(' ', id_orden, conceptorden) LIKE '%$_POST[nombre]%'";
				//sacar el consecutivo
				//$sqlr="select * from tesoordenpago_cab_r where tesoordenpago_cab_r.id_orden>-1 $crit1 order by tesoordenpago_cab_r.id_orden DESC";
				$sqlr="select * from tesoordenpago where tesoordenpago.id_orden>-1 $crit1 and tesoordenpago.tipo_mov='401' order by tesoordenpago.id_orden DESC";

				if(isset($_POST['fecha']) && isset($_POST['fecha2'])){
					if(!empty($_POST['fecha']) && !empty($_POST['fecha2'])){

						$sqlr="select * from tesoordenpago where tesoordenpago.id_orden>-1 $crit1 and tesoordenpago.tipo_mov='401' and fecha between '$f1' AND '$f2' order by tesoordenpago.id_orden DESC";
					}
				}


				$resp = mysqli_query($linkbd, $sqlr);
				$ntr = mysqli_num_rows($resp);
				$_POST['numtop']=$ntr;
				$nuncilumnas=ceil($_POST['numtop']/$_POST['numres']);
				$cond2="";
				if ($_POST['numres']!="-1"){
					$cond2="LIMIT $_POST[numpos], $_POST[numres]";
				}

				$sqlr="select tesoordenpago.id_orden,tesoordenpago.fecha,tesoordenpago.vigencia,tesoordenpago.id_rp,tesoordenpago.conceptorden,sum(tesoordenpago_det.valor),tesoordenpago.user,(select top.valorrp from tesoordenpago as top where top.id_orden=tesoordenpago.id_orden and top.tipo_mov='201' and top.tipo_mov>-1),(select top1.estado from tesoordenpago as top1 where top1.id_orden=tesoordenpago.id_orden and top1.tipo_mov='201' and top1.tipo_mov>-1) from tesoordenpago,tesoordenpago_det where  tesoordenpago.id_orden>-1 and tesoordenpago_det.id_orden=tesoordenpago.id_orden and tesoordenpago.tipo_mov='401' and tesoordenpago_det.tipo_mov='401'  $crit1 group by tesoordenpago.id_orden order by tesoordenpago.id_orden desc $cond2";
				if(isset($_POST['fecha']) && isset($_POST['fecha2'])){
					if(!empty($_POST['fecha']) && !empty($_POST['fecha2'])){
						$sqlr="select tesoordenpago.id_orden,tesoordenpago.fecha,tesoordenpago.vigencia,tesoordenpago.id_rp,tesoordenpago.conceptorden,sum(tesoordenpago_det.valor),tesoordenpago.user,(select top.valorrp from tesoordenpago as top where top.id_orden=tesoordenpago.id_orden and top.tipo_mov='201' and top.tipo_mov>-1 and top.fecha between '$f1' AND '$f2'),(select top1.estado from tesoordenpago as top1 where top1.id_orden=tesoordenpago.id_orden and top1.tipo_mov='201' and top1.tipo_mov>-1 and top1.fecha between '$f1' AND '$f2') from tesoordenpago,tesoordenpago_det where  tesoordenpago.id_orden>-1 and tesoordenpago_det.id_orden=tesoordenpago.id_orden and tesoordenpago.tipo_mov='401' and tesoordenpago_det.tipo_mov='401' and tesoordenpago.fecha between '$f1' AND '$f2'  $crit1 group by tesoordenpago.id_orden order by tesoordenpago.id_orden desc $cond2";
					}
				}

				$resp = mysqli_query($linkbd, $sqlr);
				$con=1;
				$numcontrol=$_POST['nummul']+1;
				if(($nuncilumnas==$numcontrol)||($_POST['numres']=="-1")){
					$imagenforward="<img src='imagenes/forward02.png' style='width:17px'>";
					$imagensforward="<img src='imagenes/skip_forward02.png' style='width:16px' >";
				}
				else{
					$imagenforward="<img src='imagenes/forward01.png' style='width:17px' title='Siguiente' onClick='numsiguiente()'>";
					$imagensforward="<img src='imagenes/skip_forward01.png' style='width:16px' title='Fin' onClick='saltocol(\"$nuncilumnas\")'>";
				}
				if(($_POST['numpos']==0)||($_POST['numres']=="-1")){
					$imagenback="<img src='imagenes/back02.png' style='width:17px'>";
					$imagensback="<img src='imagenes/skip_back02.png' style='width:16px'>";
				}
				else{
					$imagenback="<img src='imagenes/back01.png' style='width:17px' title='Anterior' onClick='numanterior();'>";
					$imagensback="<img src='imagenes/skip_back01.png' style='width:16px' title='Inicio' onClick='saltocol(\"1\")'>";
				}

				$con=1;
				echo "<table class='inicio' align='center'>
					<tr>
						<td colspan='9' class='titulos'>.: Resultados Busqueda:</td>
						<td class='submenu'>
							<select name='renumres' id='renumres' onChange='cambionum();' style='width:100%'>
								<option value='10'"; if ($_POST['renumres']=='10'){echo 'selected';} echo ">10</option>
								<option value='20'"; if ($_POST['renumres']=='20'){echo 'selected';} echo ">20</option>
								<option value='30'"; if ($_POST['renumres']=='30'){echo 'selected';} echo ">30</option>
								<option value='50'"; if ($_POST['renumres']=='50'){echo 'selected';} echo ">50</option>
								<option value='100'"; if ($_POST['renumres']=='100'){echo 'selected';} echo ">100</option>
								<option value='-1'"; if ($_POST['renumres']=='-1'){echo 'selected';} echo ">Todos</option>
							</select>
						</td>
					</tr>
					<tr>
						<td colspan='11'>Liquidaciones Encontrados: $ntr</td>
					</tr>
					<tr>
						<td class='titulos2' style='width:3%'>Item</td>
						<td class='titulos2' style='width:5%'>Vigencia</td>
						<td class='titulos2' style='width:3%'>N RP</td>
						<td class='titulos2' >Detalle</td>
						<td class='titulos2' >Valor CxP</td>
						<td class='titulos2' >Valor Reintegrado</td>
						<td class='titulos2' >Reintegrado por:</td>
						<td class='titulos2' style='width:6.5%'>Fecha Reintegro</td>
						<td class='titulos2' style='width:5%'>Estado</td>
						<td class='titulos2' style='width:5%'>Ver</td>
					</tr>";
                    $iter='saludo1a';
                    $iter2='saludo2';
					$filas=1;
 					while ($row =mysqli_fetch_row($resp)){
 						$detalle=$row[2];
 						$ntr=buscatercero($row[11]);
 						switch ($row[8]){
							case "S":
								$imagen="src='imagenes/confirm.png' title='Activo'";
								$camcelda="<td style='text-align:center;'><a href='#' onClick=eliminar($row[0])><img src='imagenes/anular.png' title='Anular'></a></td>";
								break;
							case "P":
								$imagen="src='imagenes/dinero3.png' title='Pago'";
								$camcelda="<td style='text-align:center;'><img src='imagenes/candado.png' title='bloqueado' style='width:18px'/></td>";
								break;
							case "N":
								$imagen="src='imagenes/cross.png' title='Anulado'";
								$camcelda="<td style='text-align:center;'><img src='imagenes/candado.png' title='bloqueado' style='width:18px'/></td>";
							case "R":
								$imagen="src='imagenes/reversado.png' title='Reversado'";
								$camcelda="<td style='text-align:center;'><img src='imagenes/candado.png' title='bloqueado' style='width:18px'/></td>";
						}
						if($gidcta!=""){
							if($gidcta==$row[0]){
								$estilo='background-color:#FF9';
							}
							else{
								$estilo="";
							}
						}
						else{
							$estilo="";
						}
						$idcta="'$row[0]'";
						$numfil="'$filas'";
						$filtro="'$_POST[nombre]'";

						if($row[8]!='R'){
							$estilo='background-color:yellow !important';
						}
						echo"<tr class='$iter' onMouseOver=\"anterior=this.style.backgroundColor;this.style.backgroundColor='#40b3ff';\"
			onMouseOut=\"this.style.backgroundColor=anterior\" onDblClick=\"verUltimaPos($idcta, $numfil, $filtro)\" style='text-transform:uppercase; $estilo' >
						 	<td>$row[0]</td>
						 	<td>$row[2]</td>
						 	<td>$row[3]</td>
						 	<td>$row[4]</td>
						 	<td style='text-align:right;'>$ ".number_format($row[7],$_SESSION["ndecimales"],$_SESSION["spdecimal"],$_SESSION["spmillares"])."&nbsp;&nbsp;</td>
						 	<td style='text-align:right;'>$ ".number_format($row[5],$_SESSION["ndecimales"],$_SESSION["spdecimal"],$_SESSION["spmillares"])."&nbsp;&nbsp;</td>
						 	<td>".$row[6]."</td>
						 	<td>".date('d-m-Y',strtotime($row[1]))."</td>
						 	<td style='text-align:center;'><img $imagen style='width:18px'></td>";
							echo"<td style='text-align:center;'>
								<a onClick=\"verUltimaPos($idcta, $numfil, $filtro)\" style='cursor:pointer;'>
									<img src='imagenes/lupa02.png' style='width:18px' title='Ver'>
								</a>
							</td>
						</tr>";
                    	$con+=1;
                        $aux=$iter;
                        $iter=$iter2;
                        $iter2=$aux;
						$filas++;
 					}
                        echo"</table>
						<table class='inicio'>
							<tr>
								<td style='text-align:center;'>
									<a href='#'>$imagensback</a>&nbsp;
									<a href='#'>$imagenback</a>&nbsp;&nbsp;";
					if($nuncilumnas<=9){$numfin=$nuncilumnas;}
					else{$numfin=9;}
					for($xx = 1; $xx <= $numfin; $xx++)
					{
						if($numcontrol<=9){$numx=$xx;}
						else{$numx=$xx+($numcontrol-9);}
						if($numcontrol==$numx){echo"<a href='#' onClick='saltocol(\"$numx\")'; style='color:#24D915'> $numx </a>";}
						else {echo"<a href='#' onClick='saltocol(\"$numx\")'; style='color:#000000'> $numx </a>";}
					}
					echo "		&nbsp;&nbsp;<a href='#'>$imagenforward</a>
									&nbsp;<a href='#'>$imagensforward</a>
								</td>
							</tr>
						</table>";
?></div>
            	</div>
            </div>
</form>
</body>
</html>
