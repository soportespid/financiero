<?php

    header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require 'comun.inc';
	require 'funciones.inc';
	session_start();
    if(empty($_SESSION)){
        header("location: index.php");
    }
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html lang=es>
	<head>
    <meta charset="utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1"/>
		<meta name="viewport" content="user-scalable=no">
		<title>:: IDEAL 10 - Presupuesto</title>
		<link href="favicon.ico" rel="shortcut icon"/>
		<link href="css/css2z.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
        <link href="css/tabs.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
        <link href="css/style.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" href="sweetalert2/dist/sweetalert2.min.css">
        <script type="text/javascript" src="css/programas.js"></script>
        <script src="sweetalert2/dist/sweetalert2.min.js"></script>
		<script src="vue/vue.min.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script src="css/calendario.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
	</head>
	<body>
		<header>
			<table>
				<tr><script>barra_imagenes("ccpet");</script><?php cuadro_titulos();?></tr>
			</table>
		</header>

		<form name="form2" method="post" action="">
			<section id="myapp" v-cloak >
                <div class="loading-container" v-show="isLoading" >
                    <p class="text-loading"data-loading="IDEAL 10">IDEAL 10</p>
                </div>
				<nav>
					<table>
						<tr><?php menu_desplegable("ccpet");?></tr>
                        <tr>
							<td colspan="3" class="cinta">
                                <img src="imagenes/add2.png"  class="mgbt" title="Nuevo">
								<img src="imagenes/guardad.png"  title="Guardar"  class="mgbt">
								<img src="imagenes/buscad.png"  class="mgbt" title="Buscar">
								<img src="imagenes/nv.png" @click="mypop=window.open('ccp-principal.php','',''); mypop.focus();" class="mgbt" title="Nueva Ventana">
                                <img src="imagenes/excel.png" @click="printExcel" alt="excel" title="Excel" class="mgbt">
                                <a href="ccp-ejecucionpresupuestal.php"  class="mgbt"><img src="imagenes/iratras.png" title="Atr&aacute;s"></a>
                            </td>
						</tr>
					</table>
				</nav>
				<article>
                     <div  class="bg-white">
                        <div>
                            <h2 class="titulos m-0">Reporte auxiliar por cuenta ingresos</h2>
                            <p class="m-0 ms-2">Todos los campos con (<span class="text-danger fw-bolder">*</span>) son obligatorios</p>
                            <div class="d-flex">
                                <div class="form-control w-25">
                                    <label class="form-label m-0" for="">Fecha inicial:</label>
                                    <input type="date" v-model="txtFechaInicial">
                                </div>
                                <div class="form-control w-25">
                                    <label class="form-label m-0" for="">Fecha final:</label>
                                    <input type="date" v-model="txtFechaFinal">
                                </div>
                                <div class="form-control">
                                    <label class="form-label m-0" for="">Cuenta <span class="text-danger fw-bolder">*</span>:</label>
                                    <div class="d-flex">
                                        <input type="text" class="colordobleclik w-50" v-model="objCuenta.codigo" @change="search('cod_cuenta')" @dblclick="isModal=true">
                                        <input type="text" v-model="objCuenta.nombre" disabled>
                                    </div>
                                </div>
                                <div class="form-control">
                                    <label class="form-label m-0" for="">Fuente:</label>
                                    <div class="d-flex">
                                        <input type="text" class="colordobleclik w-50" v-model="objFuente.codigo" @change="search('cod_fuente')" @dblclick="isModalFuente=true">
                                        <input type="text" v-model="objFuente.nombre" disabled>
                                    </div>
                                </div>
                            </div>
                            <div class="d-flex">
                                <div class="form-control">
                                    <label class="form-label m-0" for="">Medio de pago:</label>
                                    <select  v-model="selectPago">
                                        <option value="0">Todos</option>
                                        <option value="CSF">CSF</option>
                                        <option value="SSF">SSF</option>
                                    </select>

                                </div>
                                <div class="form-control">
                                    <label class="form-label m-0" for="">Vigencia de gasto:</label>
                                    <select  v-model="selectVigencias">
                                        <option value="0">Todos</option>
                                        <option v-for="(data,index) in arrVigencias" :key="index" :value="data.codigo">{{data.codigo+" - "+data.nombre}}</option>
                                    </select>
                                </div>
                                <div class="form-control">
                                    <label class="form-label m-0" for="">Sección presupuestal:</label>
                                    <select  v-model="selectSecciones">
                                        <option value="0">Todos</option>
                                        <option v-for="(data,index) in arrSecciones" :key="index" :value="data.id_seccion_presupuestal">{{data.id_seccion_presupuestal+" - "+data.nombre}}</option>
                                    </select>
                                </div>
                                <div class="w-50 form-control flex-column justify-end">
                                    <label class="form-label m-0" for=""></label>
                                    <button type="button" @click="genData" class="btn btn-primary">Generar</button>
                                </div>
                            </div>
                        </div>
                        <div>
                            <h2 class="titulos m-0">Detalles informe</h2>
                            <div class="table-responsive" style="height:50vh">
                                <table  class="table table-hover fw-normal">
                                    <thead>
                                        <tr>
                                            <th>Tipo comprobante</th>
                                            <th>Consecutivo</th>
                                            <th>Tercero</th>
                                            <th>Objeto</th>
                                            <th>Fecha</th>
                                            <th>Valor</th>
                                            <th class="text-center">Opciones</th>
                                        </tr>
                                    </thead>
                                    <tbody ref="tableData"></tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <!--MODALES-->
                    <div v-show="isModal" class="modal">
                        <div class="modal-dialog modal-lg" >
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title">Buscar cuentas</h5>
                                    <button type="button" @click="isModal=false;" class="btn btn-close"><div></div><div></div></button>
                                </div>
                                <div class="modal-body">
                                    <div class="d-flex flex-column">
                                        <div class="form-control m-0 mb-3">
                                            <input type="search" v-model="txtSearch" @keyup="search('modal')" placeholder="Buscar por código o nombre">
                                        </div>
                                        <div class="form-control m-0 p-2">
                                            <label class="form-label" for="labelInputName">Resultados: <span class="fw-bold">{{txtResultados}}</span></label>
                                        </div>
                                    </div>
                                    <div class="overflow-auto max-vh-50 overflow-x-hidden " >
                                        <table class="table table-hover fw-normal p-2">
                                            <thead>
                                                <tr>
                                                    <th class="text-center">Item</th>
                                                    <th>Código</th>
                                                    <th>Nombre</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr v-for="(data,index) in arrCuentasCopy" :key="index" @dblclick="selectItem('modal',data)">
                                                    <td>{{index+1}}</td>
                                                    <td>{{data.codigo}}</td>
                                                    <td>{{data.nombre}}</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div v-show="isModalFuente" class="modal">
                        <div class="modal-dialog modal-lg" >
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title">Buscar fuentes</h5>
                                    <button type="button" @click="isModalFuente=false;" class="btn btn-close"><div></div><div></div></button>
                                </div>
                                <div class="modal-body">
                                    <div class="d-flex flex-column">
                                        <div class="form-control m-0 mb-3">
                                            <input type="search" v-model="txtSearchFuente" @keyup="search('modal_fuente')" placeholder="Buscar por código o nombre">
                                        </div>
                                        <div class="form-control m-0 p-2">
                                            <label class="form-label" for="labelInputName">Resultados: <span class="fw-bold">{{txtResultadosFuentes}}</span></label>
                                        </div>
                                    </div>
                                    <div class="overflow-auto max-vh-50 overflow-x-hidden " >
                                        <table class="table table-hover fw-normal p-2">
                                            <thead>
                                                <tr>
                                                    <th class="text-center">Item</th>
                                                    <th>Código</th>
                                                    <th>Nombre</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr v-for="(data,index) in arrFuentesCopy" :key="index" @dblclick="selectItem('modal_fuente',data)">
                                                    <td>{{index+1}}</td>
                                                    <td>{{data.codigo}}</td>
                                                    <td>{{data.nombre}}</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
				</article>
			</section>
		</form>

		<script src="node_modules/read-excel-file/bundle/read-excel-file.min.js"></script>
		<script src="node_modules/xlsx/dist/xlsx.full.min.js"></script>
        <script src="Librerias/vue/vue.min.js"></script>
		<script src="Librerias/vue/axios.min.js"></script>
		<script type="module" src="presupuesto_ccpet/auxiliar_cuenta_ingresos/ccp-auxiliarIngresos.js?<?= date('d_m_Y_h_i_s');?>"></script>

	</body>
</html>
