<?php
	require('comun.inc');
	require('fpdf.php');
	require "funciones.inc";
	session_start();
	require_once("tcpdf/tcpdf_include.php");
	//require_once('barras/tcpdf_include.php');
	date_default_timezone_set("America/Bogota");
	class MYPDF extends TCPDF {
		function Header(){
			$linkbd = conectar_v7();
			$linkbd -> set_charset("utf8");	

			$sqlr = "SELECT * FROM configbasica WHERE estado = 'S'";
			$res = mysqli_query($linkbd, $sqlr);
			while($row = mysqli_fetch_row($res)){
				$nit = $row[0];
				$rs = $row[1];
				$nalca = $row[6];
			}
			if ($_POST['estado']=='0'){
				$this->Image('imagenes/anulado.jpg',60,75,100,100);
			}
			//Parte Izquierda
			$this->Image('imagenes/eng.jpg',12,12,46,27);
			$this->SetFont('Times','B',10);
			$this->SetY(10);
			$this->RoundedRect(10, 10, 199, 31, 2.5,'' );
			$this->Cell(0.1);
			$this->Cell(50,31,'','R',0,'L'); 
			
			$this->SetFont('Times','B',14);
			$this->SetY(13);
			$this->Cell(50.1);
			$this->Cell(149,10,$rs,0,0,'C');

			$this->SetFont('Times','B',12);
			$this->SetY(20);
			$this->Cell(50.1);
			$this->Cell(149,5,$nit,0,0,'C');
			
			$this->SetY(1);
			$this->Cell(50.1);
			$this->Cell(149,20,''.$_POST['ntipocomp'],0,0,'L'); 


			$sql = "SELECT nombre FROM tipo_comprobante WHERE codigo='".$_POST['tipocomprobante']."' AND estado='S' ";
			$res = mysqli_query($linkbd, $sql);
			$fila = mysqli_fetch_row($res);

			$this->SetFont('Times','B',10);
			
			$this->SetY(27);
			$this->Cell(50.2);
			$this->Cell(111,14,''.$fila[0],'T',0,'C'); 
			//$this->multiCell(110.7,4,''.$_POST['concepto'],'T','L');
			
			$this->SetY(27);
			$this->Cell(161.1);
			$this->Cell(37.8,14,'','TL',0,'L');
			
			$this->SetY(27.5);
			$this->Cell(162);
			$this->Cell(35,5,'NUMERO : '.$_POST['ncomp'],0,0,'L');
			
			$this->SetY(35.5);
			$this->Cell(162);
			$this->Cell(35,5,'FECHA: '.date('d-m-Y',strtotime($_POST['fecha'])),0,0,'L');

			$this->SetFont('Times','B',10);
			$this->SetY(43);
			$this->Cell(1);
			$this->multiCell(198,4,'CONCEPTO: '.$_POST['concepto'],0,'L');

			//****************************************************************************
			$this->line(10.1,51,209,51);
			$this->RoundedRect(10,52, 199, 5, 1.2,'' );
			$this->SetFont('Times','B',10);
			$this->SetY(52);
			$this->Cell(0.1);
			$this->Cell(19,5,'CODIGO',0,1,'C'); 
			$this->SetY(52);
			$this->Cell(19.1);
			$this->Cell(54,5,'CUENTA',0,1,'C');
			$this->SetY(52);
			$this->Cell(73.1);
			$this->Cell(23,5,'TERCERO',0,1,'C');
			$this->SetY(52);
			$this->Cell(101);
			$this->Cell(36,5,'DETALLE',0,1,'C');
			$this->SetY(52);
			$this->Cell(96.5);
			$this->Cell(5,5,'C.C.',0,1,'C');
			$this->SetY(52);
			$this->Cell(137);
		
			$this->Cell(31,5,'DEBITO',0,1,'C');
			$this->SetY(52);
			$this->Cell(168);
			$this->Cell(31,5,'CREDITO',0,1,'C');
			$this->line(10.1,58,209,58);
			$this->line(10.1,59,209,59);
			$this->ln(2);
		}
		//Pie de página
		function Footer(){
			$this->SetY(-15);
			$this->SetFont('Times','I',10);
			$this->Cell(0, 10, 'Impreso por: Software SPID - Ideal 10 SAS. Pagina '.$this->getAliasNumPage().' de '.$this->getAliasNbPages(), 0, false, 'R', 0, '', 0, false, 'T', 'M');
		}
	}

	$pdf = new MYPDF('P','mm','Letter', true, 'iso-8859-1', false);// create new PDF document // Orientacion del pdf
	$pdf->SetDocInfoUnicode (true); 
	// set document information
	$pdf->SetCreator(PDF_CREATOR);
	$pdf->SetAuthor('Ideal10sas');
	$pdf->SetTitle('Certificados');
	$pdf->SetSubject('Certificado de Disponibilidad');
	$pdf->SetKeywords('TCPDF, PDF, example, test, guide');
	$pdf->SetMargins(10, 60, 10);// set margins // Posicion de los valores de cada columna
	$pdf->SetHeaderMargin(90);// set margins
	$pdf->SetFooterMargin(20);// set margins
	$pdf->SetAutoPageBreak(TRUE, 20);// set auto page breaks
	$pdf->AddPage();
	$pdf->SetFont('Times','',10);
	$pdf->SetAutoPageBreak(true,20);
	$pdf->SetY(60);
	$con = 0;
	$control = 52;
	$ct = 1;	

	while ($con<count($_POST['dcuentas'])){
		if ($con%2==0){
			$pdf->SetFillColor(245,245,245);
		}else{
			$pdf->SetFillColor(255,255,255);
		}
	
		$pdf->Cell(0.1);
		$pdf->Cell(19,4,''.$_POST['dcuentas'][$con],'LR',0,'L',1);//descrip
		$pdf->Cell(53,4,''.substr(ucfirst(strtolower($_POST['dncuentas'][$con])),0,33),'LR',0,'L',1);//descrip
		$pdf->Cell(23,4,''.substr(ucfirst(strtolower($_POST['dterceros'][$con])),0,25),'LR',0,'R',1);//descrip
		$pdf->Cell(5,4,''.$_POST['dccs'][$con],'LR',0,'R',1);//descrip
		$pdf->Cell(37,4,''.substr(ucfirst(strtolower($_POST['ddetalles'][$con])),0,25),'LR',0,'L',1);//descrip

		$pdf->Cell(31,4,''.number_format($_POST['ddebitos'][$con],2,".",","),'LR',0,'R',1);
		$pdf->Cell(31,4,''.number_format($_POST['dcreditos'][$con],2,".","."),'LR',0,'R',1);
		$pdf->ln(4);	
		$con=$con+1;	
		$ct=$ct+1;

		if ($ct%$control==0){
			$pdf->line(10.1,258.3,209,258.3);
		}
	}
	$niy = $pdf->Gety();

	$pdf->line(10.1,$niy,209,$niy);

	$pdf->ln(-8);
	$pdf->SetFont('Times','B',10);
	
	$pdf->ln(10);

	//$pdf->SetLineWidth(0.2);	
	$pdf->cell(105.1);
	$v=$pdf->gety();
	$pdf->cell(32,5,'Total',0,0,'R');
	$x=$pdf->getx();
	$pdf->RoundedRect($x, $v, 62, 10, 1.2,'' );

	$pdf->cell(31,5,''.$_POST['cuentadeb'],'R',0,'R');

	$pdf->cell(31,5,''.$_POST['cuentacred'],0,0,'R');

	$pdf->ln(5);
	$pdf->cell(105.1,5,'',0,0,'R');
	$pdf->Cell(32,5,'Diferencia: '.$_POST['diferencia'],0,0,'R');
	$pdf->cell(62,5,'','T',0,'C');

	$pdf->ln(10);
	$v=$pdf->gety();
	$x=$pdf->getx();
	$pdf->RoundedRect($x, $v, 66, 15, 1.2,'' );
	$pdf->RoundedRect($x+66.5, $v, 65.5, 15, 1.2,'' );
	$pdf->RoundedRect($x+132.5, $v, 66.5, 15, 1.2,'' );

	$pdf->Cell(66,5,'ELABORO: ',0,0,'L');

	$pdf->Cell(66,5,'REVISO: ',0,0,'L');

	$pdf->Cell(67,5,'APROBO: ',0,1,'L');

	$pdf->SetLineWidth(0.4);	
	$pdf->ln(2);
	$pdf->cell(6);
	$pdf->Cell(54,5,$_SESSION['usuario'],'B',0,'C');
	$pdf->cell(12);
	$pdf->Cell(54,5,'','B',0,'C');
	$pdf->cell(12);
	$pdf->Cell(54,5,'','B',1,'L');
		
	$pdf->Output();
?> 
