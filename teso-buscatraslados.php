<?php
header("Cache-control: no-cache, no-store, must-revalidate");
header("Content-Type: text/html;charset=utf8");
require "comun.inc";
require "funciones.inc";
sesion();
$linkbd = conectar_v7();
$linkbd->set_charset("utf8");
cargarcodigopag($_GET['codpag'], $_SESSION["nivel"]);
date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE>
<html lang=es>

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1" />
    <meta name="viewport" content="user-scalable=no">
    <title>:: IDEAL 10 - Tesorer&iacute;a</title>
    <link href="favicon.ico" rel="shortcut icon" />
    <link href="css/css2.css?<?php echo date('d_m_Y_h_i_s'); ?>" rel="stylesheet" type="text/css">
    <link href="css/css3.css?<?php echo date('d_m_Y_h_i_s'); ?>" rel="stylesheet" type="text/css">
    <link href="css/css4.css?<?php echo date('d_m_Y_h_i_s'); ?>" rel="stylesheet" type="text/css">
    <link href="css/style.css?<?php echo date('d_m_Y_h_i_s'); ?>" rel="stylesheet" type="text/css">
    <script type="text/javascript" src="css/calendario.js?<?php echo date('d_m_Y_h_i_s'); ?>"></script>
    <script type="text/javascript" src="css/programas.js?<?php echo date('d_m_Y_h_i_s'); ?>"></script>
    <script type="text/javascript" src="jquery-1.11.0.min.js"></script>
    <script src="sweetalert2/dist/sweetalert2.min.js"></script>
    <link rel="stylesheet" href="sweetalert2/dist/sweetalert2.min.css">
    <script>
        function crearexcel() {
            document.form2.action = "teso-trasladosbancosexcel.php";
            document.form2.target = "_BLANK";
            document.form2.submit();
            document.form2.action = "";
            document.form2.target = "";
        }
        function validar() {
            document.form2.submit();
        }
        function anular(idr) {
            Swal.fire({
                icon: 'question',
                title: '¿Seguro que quiere anular el traslado?',
                showDenyButton: true,
                confirmButtonText: 'Guardar',
                confirmButtonColor: '#01CC42',
                denyButtonText: 'Cancelar',
                denyButtonColor: '#FF121A',
            }).then(
                (result) => {
                    if (result.isConfirmed) {
                        document.form2.oculto.value = 2;
                        document.form2.var1.value = idr;
                        document.form2.submit();
                    }
                    else if (result.isDenied) {
                        Swal.fire({
                            icon: 'info',
                            title: 'No se anulo el traslado',
                            confirmButtonText: 'Continuar',
                            confirmButtonColor: '#FF121A',
                            timer: 2500
                        });
                    }
                }
            )
        }
        function pdf() {
            document.form2.action = "teso-pdftraslados.php";
            document.form2.target = "_BLANK";
            document.form2.submit();
            document.form2.action = "";
            document.form2.target = "";
        }
        function verUltimaPos(idr, dc) {
            var scrtop = $('#divdet').scrollTop();
            document.location.href = "teso-trasladosvisualizar.php?idr=" + idr + "&dc=" + dc + "&scrtop=" + scrtop;
        }
    </script>
    <?php
    titlepag();
    $scrtop = $_GET['scrtop'];
    if ($scrtop == "") {
        $scrtop = 0;
    } else {
        echo "<script> window.onload=function(){ $('#divdet').scrollTop(" . $scrtop . ")}</script>";
    }
    ?>
</head>

<body>
    <IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
    <span id="todastablas2"></span>
    <table>
        <tr>
            <script>barra_imagenes("teso");</script><?php cuadro_titulos(); ?>
        </tr>
        <tr><?php menu_desplegable("teso"); ?></tr>
    </table>
    <div class="bg-white group-btn p-1"><button type="button" onclick="location.href='teso-traslados.php'"
            class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
            <span>Nuevo</span>
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960">
                <path d="M440-440H200v-80h240v-240h80v240h240v80H520v240h-80v-240Z"></path>
            </svg>
        </button><button type="button" onclick="document.form2.submit();"
            class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
            <span>Buscar</span>
            <svg class="fill-black group-hover:fill-white w-5 h-5" xmlns="http://www.w3.org/2000/svg"
                viewBox="0 -960 960 960">
                <path
                    d="M784-120 532-372q-30 24-69 38t-83 14q-109 0-184.5-75.5T120-580q0-109 75.5-184.5T380-840q109 0 184.5 75.5T640-580q0 44-14 83t-38 69l252 252-56 56ZM380-400q75 0 127.5-52.5T560-580q0-75-52.5-127.5T380-760q-75 0-127.5 52.5T200-580q0 75 52.5 127.5T380-400Z">
                </path>
            </svg>
        </button><button type="button" onclick="mypop=window.open('plan-agenda.php','','');mypop.focus()"
            class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
            <span class="group-hover:text-white">Agenda</span>
            <svg class="fill-black group-hover:fill-white w-5 h-5" xmlns="http://www.w3.org/2000/svg"
                viewBox="0 -960 960 960">
                <path
                    d="M560-564v-68q33-14 67.5-21t72.5-7q26 0 51 4t49 10v64q-24-9-48.5-13.5T700-600q-38 0-73 9.5T560-564Zm0 220v-68q33-14 67.5-21t72.5-7q26 0 51 4t49 10v64q-24-9-48.5-13.5T700-380q-38 0-73 9t-67 27Zm0-110v-68q33-14 67.5-21t72.5-7q26 0 51 4t49 10v64q-24-9-48.5-13.5T700-490q-38 0-73 9.5T560-454ZM260-320q47 0 91.5 10.5T440-278v-394q-41-24-87-36t-93-12q-36 0-71.5 7T120-692v396q35-12 69.5-18t70.5-6Zm260 42q44-21 88.5-31.5T700-320q36 0 70.5 6t69.5 18v-396q-33-14-68.5-21t-71.5-7q-47 0-93 12t-87 36v394Zm-40 118q-48-38-104-59t-116-21q-42 0-82.5 11T100-198q-21 11-40.5-1T40-234v-482q0-11 5.5-21T62-752q46-24 96-36t102-12q58 0 113.5 15T480-740q51-30 106.5-45T700-800q52 0 102 12t96 36q11 5 16.5 15t5.5 21v482q0 23-19.5 35t-40.5 1q-37-20-77.5-31T700-240q-60 0-116 21t-104 59ZM280-494Z">
                </path>
            </svg>
        </button><button type="button" onclick="mypop=window.open('teso-principal.php','','');mypop.focus();"
            class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
            <span>Nueva ventana</span>
            <svg class="fill-black group-hover:fill-white w-5 h-5" xmlns="http://www.w3.org/2000/svg"
                viewBox="0 -960 960 960">
                <path
                    d="M320-120v-80h80v-80H160q-33 0-56.5-23.5T80-360v-400q0-33 23.5-56.5T160-840h640q33 0 56.5 23.5T880-760v400q0 33-23.5 56.5T800-280H560v80h80v80H320ZM160-360h640v-400H160v400Zm0 0v-400 400Z">
                </path>
            </svg>
        </button><button type="button"
            onclick="mypop=window.open('/financiero/teso-buscatraslados.php','','');mypop.focus();"
            class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
            <span class="group-hover:text-white">Duplicar pantalla</span>
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960">
                <path
                    d="M640-160v-360H160v360h480Zm80-200v-80h80v-360H320v200h-80v-200q0-33 23.5-56.5T320-880h480q33 0 56.5 23.5T880-800v360q0 33-23.5 56.5T800-360h-80ZM160-80q-33 0-56.5-23.5T80-160v-360q0-33 23.5-56.5T160-600h480q33 0 56.5 23.5T720-520v360q0 33-23.5 56.5T640-80H160Zm400-603ZM400-340Z">
                </path>
            </svg>
        </button><button type="button" onclick="crearexcel()"
            class="btn btn-success btn-success-hover d-flex justify-between align-items-center">
            <span>Exportar Excel</span>
            <svg class="fill-black group-hover:fill-white w-4 h-4" xmlns="http://www.w3.org/2000/svg"
                viewBox="0 0 384 512"><!--!Font Awesome Free 6.5.1 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license/free Copyright 2024 Fonticons, Inc. -->
                <path
                    d="M48 448V64c0-8.8 7.2-16 16-16H224v80c0 17.7 14.3 32 32 32h80V448c0 8.8-7.2 16-16 16H64c-8.8 0-16-7.2-16-16zM64 0C28.7 0 0 28.7 0 64V448c0 35.3 28.7 64 64 64H320c35.3 0 64-28.7 64-64V154.5c0-17-6.7-33.3-18.7-45.3L274.7 18.7C262.7 6.7 246.5 0 229.5 0H64zm90.9 233.3c-8.1-10.5-23.2-12.3-33.7-4.2s-12.3 23.2-4.2 33.7L161.6 320l-44.5 57.3c-8.1 10.5-6.3 25.5 4.2 33.7s25.5 6.3 33.7-4.2L192 359.1l37.1 47.6c8.1 10.5 23.2 12.3 33.7 4.2s12.3-23.2 4.2-33.7L222.4 320l44.5-57.3c8.1-10.5 6.3-25.5-4.2-33.7s-25.5-6.3-33.7 4.2L192 280.9l-37.1-47.6z">
                </path>
            </svg>
        </button></div>
    <form name="form2" method="post" action="">
        <div class="loading" id="divcarga"><span>Cargando...</span></div>
        <table class="inicio ancho">
            <tr>
                <td class="titulos" colspan="7">:. Buscar Traslados de Bancos </td>
                <td class="cerrar" style="width:7%" onClick="location.href='teso-principal.php'">Cerrar</td>
            </tr>
            <tr>
                <td class="tamano01" style="width:4cm;">Numero Comprobante:</td>
                <td style="width:12%"><input name="numero" type="text" value="" style="width:98%;height:30px;"></td>
                <td class="tamano01" style="width:2.5cm;">Fecha Inicial: </td>
                <td style="width:12%"><input name="fechaini" type="text" id="fc_1198971545" title="DD/MM/YYYY"
                        value="<?php echo $_POST['fechaini']; ?>" onKeyUp="return tabular(event,this) "
                        onKeyDown="mascara(this,'/',patron,true)" maxlength="10" style="width:100%;;"
                        class="colordobleclik" onDblClick="displayCalendarFor('fc_1198971545');" autocomplete="off"
                        onChange=""></td>
                <td class="tamano01" style="width:2.5cm;">Fecha Final: </td>
                <td style="width:12%"><input name="fechafin" type="text" id="fc_1198971546" title="DD/MM/YYYY"
                        value="<?php echo $_POST['fechafin']; ?>" onKeyUp="return tabular(event,this) "
                        onKeyDown="mascara(this,'/',patron,true)" maxlength="10" style="width:100%;"
                        class="colordobleclik" onDblClick="displayCalendarFor('fc_1198971546');" autocomplete="off"
                        onChange=""></td>
                <td><em class="botonflechaverde" onClick="document.form2.submit();">Buscar</em></td>
                <td></td>
                <input name="oculto" type="hidden" value="1"><input name="var1" type="hidden" value=<?php echo $_POST['var1']; ?>>
            </tr>
        </table>
        <?php
        if ($_POST['oculto'] == 2) {
            $sqlr = "SELECT * FROM tesotraslados_cab WHERE id_consignacion = '" . $_POST['var1'] . "'";
            $resp = mysqli_query($linkbd, $sqlr);
            $row = mysqli_fetch_row($resp);
            //********Comprobante contable en 000000000000
            $sqlr = "update comprobante_cab set total_debito=0, total_credito=0,estado='0' where tipo_comp=10 and numerotipo=$row[0]";
            mysqli_query($linkbd, $sqlr);
            $sqlr = "update tesotraslados_cab set estado='N' where id_consignacion=$row[0]";
            mysqli_query($linkbd, $sqlr);
        }
        ?>
        <?php
        if ($_POST['oculto'] != '') {
            if (($_POST['fechaini'] != "" and $_POST['fechafin'] != "") || ($_POST['numero'] != "")) {
                $crit1 = " ";
                $crit2 = " ";
                if ($_POST['numero'] != "") {
                    $crit1 = " and id_consignacion like '" . $_POST['numero'] . "' ";
                } else {
                    preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST['fechaini'], $fecha);
                    $fechai = "$fecha[3]-$fecha[2]-$fecha[1]";
                    preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST['fechafin'], $fecha);
                    $fechaf = $fecha[3] . "-" . $fecha[2] . "-" . $fecha[1];
                    $crit2 = " AND fecha BETWEEN '$fechai' AND '$fechaf'";
                }
                $sqlr = "SELECT * FROM tesotraslados_cab WHERE estado <> '' $crit1 $crit2 ORDER BY id_consignacion DESC";
                $resp = mysqli_query($linkbd, $sqlr);
                $ntr = mysqli_num_rows($resp);
                $con = 1;
                echo "
							<table class='tablamv'>
								<thead>
									<tr style='text-align:left;'><th colspan='5' class='titulos'>.: Resultados Busqueda: $ntr</th></tr>
									<tr>
										<th class='titulosnew00' style='width:8%'><center>No Traslado</th>
										<th class='titulosnew00' style='width:10%'><center>Fecha</th>
										<th class='titulosnew00' style='width:50%'>Concepto Traslado</th>
										<th class='titulosnew00' style='width:8%'><center>Destino</th>
										<th class='titulosnew00' style='width:16%'>Valor</th>
										<th class='titulosnew00' >Estado</th>
									</tr>
								</thead>
								<tbody style='max-height: 52vh;' id='divdet'>";
                $iter = 'saludo1a';
                $iter2 = 'saludo2';
                while ($row = mysqli_fetch_row($resp)) {
                    $sqlr = "SELECT SUM(VALOR), tipo_traslado, destino_ext FROM tesotraslados WHERE id_trasladocab=$row[0]";
                    $resp2 = mysqli_query($linkbd, $sqlr);
                    $row2 = mysqli_fetch_row($resp2);
                    $valor = $row2[0];
                    if ($_GET['idr'] != "") {
                        if ($_GET['idr'] == $row[0]) {
                            $estilo = 'background-color:yellow';
                        } else {
                            $estilo = "";
                        }
                    } else {
                        $estilo = "";
                    }
                    if ($row2[1] == 'INT') {
                        $destino = $row[6];
                    } else {
                        $destino = $row2[2];
                    }
                    echo "
								<input type='hidden' name='nTrasladE[]' value='" . $row[0] . "'>
								<input type='hidden' name='fechaE[]' value='" . $row[2] . "'>
								<input type='hidden' name='conceptoTrasladoE[]' value='" . $row[5] . "'>
								<input type='hidden' name='destino[]' value='$destino'>
								<input type='hidden' name='valorE[]' value='" . number_format($row2[0], 2, ',', '.') . "'>
								";
                    if ($row[4] == 'S') {
                        echo "<input type='hidden' name='estadoE[]' value='ACTIVO'>";
                        $clase = 'garbageX';
                        $titulo = 'Activa';
                    }
                    if ($row[4] == 'N') {
                        echo "<input type='hidden' name='estadoE[]' value='INACTIVO'>";
                        $clase = 'garbageY';
                        $titulo = 'Anulada';
                    }
                    if ($row[4] == 'R') {
                        echo "<input type='hidden' name='estadoE[]' value='INACTIVO'>";
                        $clase = 'garbageY';
                        $titulo = 'Reversada';
                    }

                    $idr = "'$row[0]'";
                    $dc = "'$row[0]'";
                    echo "
								<tr class='$iter' onDblClick=\"verUltimaPos($idr, $dc)\" style='text-transform:uppercase; $estilo' >
									<td style='text-align:center; width:8%'>$row[0]</td>
									<td style='text-align:center; width:10%'>$row[2]</td>
									<td style='width:50%'>$row[5]</td>
									<td style='text-align:center; width:8%'>$destino</td>
									<td style='text-align:right;width:16%'>" . number_format($valor, 2, ",", ".") . "</td>
									<td style='font: 110% sans-serif; display: flex; justify-content: center;' onclick=\"anular('" . $row[0] . "')\"><div class='$clase' title='$titulo'></td>
								</tr>";
                    $con += 1;
                    $aux = $iter;
                    $iter = $iter2;
                    $iter2 = $aux;
                }
                echo "
								</tbody>
							</table>";
            } else {
                echo "
							<script>
								Swal.fire(
									'Error!',
									'Se debe ingresar fecha inicial y fecha final',
									'error'
								);
							</script>";
            }
        }
        echo "<script>document.getElementById('divcarga').style.display='none';</script>";
        ?>
    </form>
</body>

</html>
