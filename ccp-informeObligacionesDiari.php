<?php

    header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require 'comun.inc';
	require 'funciones.inc';
	session_start();
    if(empty($_SESSION)){
        header("location: index.php");
    }
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html lang=es>
	<head>
    <meta charset="utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1"/>
		<meta name="viewport" content="user-scalable=no">
		<title>:: IDEAL 10 - Informes</title>
		<link href="favicon.ico" rel="shortcut icon"/>
		<link href="css/css2z.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
        <link href="css/tabs.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
        <link href="css/style.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" href="sweetalert2/dist/sweetalert2.min.css">
        <script type="text/javascript" src="css/programas.js"></script>
        <script src="sweetalert2/dist/sweetalert2.min.js"></script>
		<script src="vue/vue.min.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script src="css/calendario.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
	</head>
	<body>
		<header>
			<table>
				<tr><script>barra_imagenes("info");</script><?php cuadro_titulos();?></tr>
			</table>
		</header>

		<form name="form2" method="post" action="">
			<section id="myapp" v-cloak >
                <div id="cargando" v-show="isLoading" class="loading" style="z-index: 10000;">
                    <span>Cargando...</span>
                </div>
				<nav>
					<table>
						<tr><?php menu_desplegable("info");?></tr>
                        <tr>
							<td colspan="3" class="cinta">
                                <img src="imagenes/add2.png"  class="mgbt" title="Nuevo">
								<img src="imagenes/guardad.png"   title="Guardar"  class="mgbt">
								<img src="imagenes/buscad.png"   class="mgbt" title="Buscar">
								<img src="imagenes/nv.png" @click="mypop=window.open('info-principal.php','',''); mypop.focus();" class="mgbt" title="Nueva Ventana">
                                <a href="ccp-informesdiari.php"  class="mgbt"><img src="imagenes/iratras.png" title="Atr&aacute;s"></a>
                                <a @click="printCsv()" class="mgbt"><img src="imagenes/csv.png" title="CSV"></a>
							</td>
						</tr>
					</table>
				</nav>
				<article>
                    <div class="inicio">
                        <div>
                            <h2 class="titulos m-0">.: Relación de obligaciones</h2>
                            <div class="d-flex w-50">
                                <div class="form-control">
                                    <label class="form-label" for="">.: Fecha inicial <span class="text-danger fw-bolder">*</span>:</label>
                                    <input type="text" name="fechaInicial" onKeyUp="return tabular(event,this)" id="fechaInicial" title="DD/MM/YYYY" onDblClick="displayCalendarFor('fechaInicial');" class="colordobleclik" autocomplete="off" onChange=""  readonly>
                                </div>
                                <div class="form-control">
                                    <label class="form-label" for="">.: Fecha final<span class="text-danger fw-bolder">*</span>:</label>
                                    <div class="d-flex">
                                        <input type="text" id="fechaFinal" name="fechaFinal" onKeyUp="return tabular(event,this)" title="DD/MM/YYYY" onDblClick="displayCalendarFor('fechaFinal');" class="colordobleclik" autocomplete="off" onChange=""  readonly>
                                        <button type="button" @click="getData" class="btn btn-primary">Generar</button>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div class="overflow-auto max-vh-50 overflow-x-hidden p-2" >
                            <table class="table fw-normal">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>nombreRubro</th>
                                        <th>Vigencia</th>
                                        <th>Macro campo valor</th>
                                        <th>Código presupuestal</th>
                                        <th>Fecha</th>
                                        <th>Valor</th>
                                        <th>NIT</th>
                                        <th>Compromiso</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr v-for="(data,index) in arrData" :key="index">
                                        <td>{{data.id}}</td>
                                        <td>{{data.nombre}}</td>
                                        <td>{{data.vigencia}}</td>
                                        <td>2</td>
                                        <td>{{data.rubro}}</td>
                                        <td>{{data.fecha}}</td>
                                        <td>{{formatNumero(data.valor)}}</td>
                                        <td>{{data.cedulanit}}</td>
                                        <td>{{data.id_rp}}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
				</article>
			</section>
		</form>

		<script src="node_modules/read-excel-file/bundle/read-excel-file.min.js"></script>
		<script src="node_modules/xlsx/dist/xlsx.full.min.js"></script>
        <script src="Librerias/vue/vue.min.js"></script>
		<script src="Librerias/vue/axios.min.js"></script>
		<script type="module" src="presupuesto_ccpet/plantilla_appui/relacion_obligaciones/ccp-informeObligaciones.js?<?= date('d_m_Y_h_i_s');?>"></script>

	</body>
</html>
