<?php

	header("Content-Type: text/html;charset=utf-8");
    require_once 'PHPExcel/Classes/PHPExcel.php';
    require "comun.inc";
    require "funciones.inc";
    require 'funcionesSP.inc.php';
    /*ini_set('display_errors', '1');
    ini_set('display_startup_errors', '1');
    error_reporting(E_ALL);*/

    session_start();

    class Plantilla{
        private $linkbd;
        private $arrData;
        public function __construct() {
            $this->linkbd = conectar_v7();
            $this->linkbd->set_charset("utf8");
        }
        public function selectFormatos(){
            $sql = "SELECT * FROM contexogenaformatos ORDER BY id";
            $arrData = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
            return $arrData;
        }
        public function selectData(){
            $sql = "SELECT codigo,nombre,formato
            FROM contexogenaconceptos
            ORDER BY id";
            $arrData = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
            return $arrData;
        }
        public function selectColumns(){
            $sql = "SELECT * FROM contexogenacolumnas ORDER BY id";
            $arrData = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
            return $arrData;
        }
    }

    $obj = new Plantilla();
    $request = $obj->selectData();
    $requestCol = $obj->selectColumns();
    $requestForm = $obj->selectFormatos();
    $objPHPExcel = new PHPExcel();

    $objPHPExcel->createSheet(1);
    $objPHPExcel->createSheet(2);
    $objPHPExcel->getSheet(0)->getStyle('A:C')->applyFromArray(
        array(
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
            )
        )
    );
    $objPHPExcel->getSheet(0)->setTitle("Formatos");
    $objPHPExcel->getSheet(1)->setTitle("Conceptos");
    $objPHPExcel->getSheet(2)->setTitle("Columnas");
    $objPHPExcel->getProperties()
    ->setCreator("IDEAL 10")
    ->setLastModifiedBy("IDEAL 10")
    ->setTitle("Exportar Excel con PHP")
    ->setSubject("Documento de prueba")
    ->setDescription("Documento generado con PHPExcel")
    ->setKeywords("usuarios phpexcel")
    ->setCategory("reportes");

    //----Cuerpo de Documento----
    $objPHPExcel->getSheet(0)
    ->mergeCells('A1:B1')
    ->mergeCells('A2:B2')
    ->setCellValue('A1', 'CONTABILIDAD')
    ->setCellValue('A2', 'PLANTILLA FORMATOS PARA EXÓGENA');
    $objPHPExcel->getSheet(0)
    -> getStyle ("A1")
    -> getFill ()
    -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
    -> getStartColor ()
    -> setRGB ('C8C8C8');
    $objPHPExcel->getSheet(0)
    -> getStyle ("A1:A2")
    -> getFont ()
    -> setBold ( true )
    -> setName ( 'Verdana' )
    -> setSize ( 10 )
    -> getColor ()
    -> setRGB ('000000');
    $objPHPExcel->getSheet(0)
    -> getStyle ('A1:A2')
    -> getAlignment ()
    -> applyFromArray (array ( 'horizontal'  =>  PHPExcel_Style_Alignment :: HORIZONTAL_CENTER ,) );
    $objPHPExcel->getSheet(0)
    -> getStyle ('A3:B3')
    -> getAlignment ()
    -> applyFromArray (array ( 'horizontal'  =>  PHPExcel_Style_Alignment :: VERTICAL_JUSTIFY ,) );
    $objPHPExcel->getSheet(0)
    -> getStyle ("A2")
    -> getFill ()
    -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID);

    $borders = array(
        'borders' => array(
            'allborders' => array(
                'style' => PHPExcel_Style_Border::BORDER_THIN,
            'color' => array('argb' => 'FF000000'),
            )
        ),
    );
    $objPHPExcel->getSheet(0)
    ->setCellValue('A3', 'Formato')
    ->setCellValue('B3', "Nombre");
    $objPHPExcel->getSheet(0)
        -> getStyle ("A3:B3")
        -> getFill ()
        -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
        -> getStartColor ()
        -> setRGB ('99ddff');
    $objPHPExcel->getSheet(0)->getStyle("A3:B3")->getFont()->setBold(true);
    $objPHPExcel->getSheet(0)->getStyle('A1:B1')->applyFromArray($borders);
    $objPHPExcel->getSheet(0)->getStyle('A2:B2')->applyFromArray($borders);
    $objPHPExcel->getSheet(0)->getStyle('A3:B3')->applyFromArray($borders);


    $objPHPExcel->getSheet(0)->getColumnDimension('A')->setAutoSize(true);
    $objPHPExcel->getSheet(0)->getColumnDimension('B')->setAutoSize(true);

    //Hoja conceptos

    $objPHPExcel->getSheet(1)->getStyle('A:C')->applyFromArray(
        array(
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
            )
        )
    );

    $objPHPExcel->getSheet(1)
    ->mergeCells('A1:C1')
    ->mergeCells('A2:C2')
    ->setCellValue('A1', 'CONTABILIDAD')
    ->setCellValue('A2', 'PLANTILLA PARA CARGA DE CONCEPTOS EXÓGENA');
    $objPHPExcel->getSheet(1)
    -> getStyle ("A1")
    -> getFill ()
    -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
    -> getStartColor ()
    -> setRGB ('C8C8C8');
    $objPHPExcel->getSheet(1)
    -> getStyle ("A1:A2")
    -> getFont ()
    -> setBold ( true )
    -> setName ( 'Verdana' )
    -> setSize ( 10 )
    -> getColor ()
    -> setRGB ('000000');
    $objPHPExcel->getSheet(1)
    -> getStyle ('A1:A2')
    -> getAlignment ()
    -> applyFromArray (array ( 'horizontal'  =>  PHPExcel_Style_Alignment :: HORIZONTAL_CENTER ,) );
    $objPHPExcel->getSheet(1)
    -> getStyle ('A3:C3')
    -> getAlignment ()
    -> applyFromArray (array ( 'horizontal'  =>  PHPExcel_Style_Alignment :: VERTICAL_JUSTIFY ,) );
    $objPHPExcel->getSheet(1)
    -> getStyle ("A2")
    -> getFill ()
    -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID);

    $borders = array(
        'borders' => array(
            'allborders' => array(
                'style' => PHPExcel_Style_Border::BORDER_THIN,
            'color' => array('argb' => 'FF000000'),
            )
        ),
    );
    $objPHPExcel->getSheet(1)
    ->setCellValue('A3', 'formato')
    ->setCellValue('B3', "codigo")
    ->setCellValue('C3', "Concepto");
    $objPHPExcel->getSheet(1)
        -> getStyle ("A3:C3")
        -> getFill ()
        -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
        -> getStartColor ()
        -> setRGB ('99ddff');
    $objPHPExcel->getSheet(1)->getStyle("A3:C3")->getFont()->setBold(true);
    $objPHPExcel->getSheet(1)->getStyle('A1:C1')->applyFromArray($borders);
    $objPHPExcel->getSheet(1)->getStyle('A2:C2')->applyFromArray($borders);
    $objPHPExcel->getSheet(1)->getStyle('A3:C3')->applyFromArray($borders);

    $objWorksheet = $objPHPExcel->getSheet(1);

    $objPHPExcel->getSheet(1)->getColumnDimension('A')->setAutoSize(true);
    $objPHPExcel->getSheet(1)->getColumnDimension('B')->setAutoSize(true);
    $objPHPExcel->getSheet(1)->getColumnDimension('C')->setAutoSize(true);

    //Hoja de columnas
    $objPHPExcel->getSheet(2)
    ->mergeCells('A1:C1')
    ->mergeCells('A2:C2')
    ->setCellValue('A1', 'CONTABILIDAD')
    ->setCellValue('A2', 'PLANTILLA PARA CARGA DE COLUMNAS EXÓGENA');
    $objPHPExcel-> getSheet(2)
    -> getStyle ("A1")
    -> getFill ()
    -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
    -> getStartColor ()
    -> setRGB ('C8C8C8');
    $objPHPExcel-> getSheet(2)
    -> getStyle ("A1:A3")
    -> getFont ()
    -> setBold ( true )
    -> setName ( 'Verdana' )
    -> setSize ( 10 )
    -> getColor ()
    -> setRGB ('000000');
    $objPHPExcel-> getSheet(2)
    -> getStyle ('A1:A3')
    -> getAlignment ()
    -> applyFromArray (array ( 'horizontal'  =>  PHPExcel_Style_Alignment :: HORIZONTAL_CENTER ,) );
    $objPHPExcel-> getSheet(2)
    -> getStyle ('A3:C3')
    -> getAlignment ()
    -> applyFromArray (array ( 'horizontal'  =>  PHPExcel_Style_Alignment :: VERTICAL_JUSTIFY ,) );
    $objPHPExcel-> getSheet(2)
    -> getStyle ("A2")
    -> getFill ()
    -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID);

    $borders = array(
        'borders' => array(
            'allborders' => array(
                'style' => PHPExcel_Style_Border::BORDER_THIN,
            'color' => array('argb' => 'FF000000'),
            )
        ),
    );
    $objPHPExcel->getSheet(2)
    ->setCellValue('A3', 'Tipo')
    ->setCellValue('B3', "Nombre");
    $objPHPExcel-> getSheet(2)
        -> getStyle ("A3:B3")
        -> getFill ()
        -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
        -> getStartColor ()
        -> setRGB ('99ddff');
    $objPHPExcel->getSheet(2)->getStyle("A3:B3")->getFont()->setBold(true);
    $objPHPExcel->getSheet(2)->getStyle('A1:B1')->applyFromArray($borders);
    $objPHPExcel->getSheet(2)->getStyle('A2:B2')->applyFromArray($borders);
    $objPHPExcel->getSheet(2)->getStyle('A3:B3')->applyFromArray($borders);


    $objPHPExcel->getSheet(2)->getColumnDimension('A')->setAutoSize(true);
    $objPHPExcel->getSheet(2)->getColumnDimension('B')->setAutoSize(true);

    //Formatos
    if(!empty($requestForm)){
        $total = count($requestForm);
        $row = 4;
        for ($i=0; $i < $total ; $i++) {
            $objPHPExcel->getSheet(0)
            ->setCellValue("A$row", $requestForm[$i]['formato'])
            ->setCellValue("B$row", $requestForm[$i]['nombre']);
            $row++;
        }
    }
    //Conceptos
    if(!empty($request)){
        $total = count($request);
        $row = 4;
        for ($i=0; $i < $total ; $i++) {
            $objPHPExcel->getSheet(1)
            ->setCellValue("A$row", $request[$i]['formato'])
            ->setCellValue("B$row", $request[$i]['codigo'])
            ->setCellValue("C$row", $request[$i]['nombre']);
            $row++;
        }
    }
    //Columnas
    if(!empty($requestCol)){
        $total = count($requestCol);
        $row = 4;
        for ($i=0; $i < $total ; $i++) {
            $objPHPExcel->getSheet(2)
            ->setCellValue("A$row", $requestCol[$i]['tipo'])
            ->setCellValue("B$row", $requestCol[$i]['nombre']);
            $row++;
        }
    }

    $objPHPExcel->setActiveSheetIndex(0);
    //----Guardar documento----
    header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment;filename="parámetros-exogena.xlsx"');
    header('Cache-Control: max-age=0');
    $objWriter=PHPExcel_IOFactory::createWriter($objPHPExcel,'Excel2007');
    $objWriter->save('php://output');
    die();

?>
