<?php 
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require "comun.inc";
	require "funciones.inc";
	sesion();
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html lang="es">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=9">
		<title>:: Spid - Gestion Humana</title>
		<link href="css/css2.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css">
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css">
		<script type="text/javascript" src="css/calendario.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script type="text/javascript" src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script>
			var anterior;
			function ponprefijo(dato1, dato2, dato3, dato4, dato5, dato6, dato7, dato8){ 
				parent.document.form2.trasladoSSF.value = dato1;
				parent.document.form2.numero.value = dato2;
				parent.document.form2.concepto.value = dato3;
				parent.document.form2.cc.value = dato5;
				parent.document.form2.cc2.value = dato4;
				parent.document.form2.banco.value = dato7;
				parent.document.form2.banco2.value = dato6;
				parent.document.form2.valor.value = dato8;
				parent.despliegamodal2("hidden");
				parent.document.form2.submit();
			}
		</script>
		<?php titlepag();?>
	</head>
	<body>
		<form action="" method="post" enctype="multipart/form-data" name="form2">
			<?php if($_POST['oculto'] == ""){$_POST['numpos'] = 0;$_POST['numres'] = 10;$_POST['nummul'] = 0;}?>
			<table  class="inicio" >
				<tr>
					<td class="titulos" colspan="4">:: Buscar Cargo Administrativo</td>
					<td class="cerrar" style="width:7%" onClick="parent.despliegamodal2('hidden');">Cerrar</td>
				</tr>
				<tr>
					<td class="saludo1">:: Nombre:</td>
					<td><input name="nombre" type="text" value="" size="40"></td>
					<td class="saludo1">:: Codigo:</td>
					<td>
						<input name="documento" type="text" id="documento" value="">
						<input type="button" name="bboton" onClick="limbusquedas();" value="&nbsp;&nbsp;Buscar&nbsp;&nbsp;" />
					</td>
				</tr>
			</table> 
			<input type="hidden" name="oculto" id="oculto" value="1">
			<input type="hidden" name="numres" id="numres" value="<?php echo $_POST['numres'];?>"/>
			<input type="hidden" name="numpos" id="numpos" value="<?php echo $_POST['numpos'];?>"/>
			<input type="hidden" name="nummul" id="nummul" value="<?php echo $_POST['nummul'];?>"/>
			<?php
				$crit1 = " ";
				$crit2 = " ";
				if ($_POST['documento'] != ""){$crit1 = "AND codcargo LIKE '%".$_POST['numero']."%'";}
				if ($_POST['nombre'] != ""){$crit2 = "AND nombrecargo LIKE '%".$_POST['nombre']."%'";}
				
				$sqlr = "SELECT id_trasladocab, ntransaccion, cco, ccd, ncuentaban1, ncuentaban2, valor FROM tesotraslados WHERE tipo_pago = 'SSF' AND estado = 'S' AND id_trasladocab NOT IN (SELECT idSSF FROM tesotraslados WHERE idSSF IS NOT NULL AND estado = 'S');";
				$resp = mysqli_query($linkbd,$sqlr);
				$_POST['numtop'] = mysqli_num_rows($resp);
				$con = 1;
				
				echo "
				<table class='inicio' align='center' width='99%'>
				<tr>
					<td colspan='3' class='titulos'>.: Resultados Busqueda:</td>
				</tr>
				<tr><td colspan='3'>Centro Costo Encontrados: ".$_POST['numtop']."</td></tr>
				<tr>
					<td class='titulos2' width='10%'>No</td>
					<td class='titulos2' width='10%'>Código</td>
					<td class='titulos2' width='80%'>Concepto</td>
					
				</tr>";	
				$iter = 'saludo1a';
				$iter2 = 'saludo2';
				while ($row = mysqli_fetch_assoc($resp)){
					$vid = $row['id_trasladocab'];
					$numtran = $row['ntransaccion'];
					$ccorigen = $row['cco'];
					$ccdestino = $row['ccd'];
					$numcuenta1n = $row['ncuentaban1'];
					$numcuenta2n = $row['ncuentaban2'];
					$vvalor = $row['valor'];
					
					$sql1 = "SELECT concepto FROM tesotraslados_cab WHERE id_consignacion = '$vid'";
					$res1 = mysqli_query($linkbd,$sql1);
					$row1 = mysqli_fetch_assoc($res1);
					$vnconcepto = $row1['concepto'];

					$sql2 = "SELECT cuenta FROM tesobancosctas WHERE estado = 'S' AND ncuentaban = '$numcuenta1n'";
					$res2 = mysqli_query($linkbd, $sql2);
					$row2 = mysqli_fetch_assoc($res2);
					$numcuenta1 = $row2['cuenta'];

					$sql3 = "SELECT cuenta FROM tesobancosctas WHERE estado = 'S' AND ncuentaban = '$numcuenta2n'";
					$res3 = mysqli_query($linkbd, $sql3);
					$row3 = mysqli_fetch_assoc($res3);
					$numcuenta2 = $row3['cuenta'];

					echo" 
					<tr class='$iter' onClick=\"javascript:ponprefijo('$vid', '$numtran', '$vnconcepto', '$ccorigen', '$ccdestino','$numcuenta1', '$numcuenta2', '$vvalor')\" >
						<td>$con</td>
						<td>$vid</td>
						<td>$vnconcepto</td>
					</tr>";
					$aux = $iter;
					$iter = $iter2;
					$iter2 = $aux;
					$con++;
				}
				echo"</table>";
			?>
			<input type="hidden" name="numtop" id="numtop" value="<?php echo $_POST['numtop'];?>"/>
		</form>
	</body>
</html>
