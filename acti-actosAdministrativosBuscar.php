<?php
	require "comun.inc";
	require "funciones.inc";
	session_start();
	$linkbd = conectar_v7();
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	header("Cache-control: private");
	header("Content-Type: text/html;charset=iso-8859-1");
	date_default_timezone_set("America/Bogota"); 
?>
<!DOCTYPE >
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es">
	<head>
	 	<meta http-equiv="Content-type" content="text/html; charset=iso-8859-1"/>
        <meta http-equiv="X-UA-Compatible" content="IE=9"/>
		<title>:: IDEAL 10 - Control de activos</title>
        <link href="css/css2.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<script type="text/javascript" src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
        <script type='text/javascript' src='JQuery/jquery-2.1.4.min.js'></script>
		<script type="text/javascript" src="css/calendario.js"></script>
        <script>
			var ctrlPressed = false;
			var tecla01 = 16, tecla02 = 80, tecla03 = 81;
			$(document).keydown(
				function(e){
					
					if (e.keyCode == tecla01){ctrlPressed = true;}
					if (e.keyCode == tecla03){tecla3Pressed = true;}
					if (ctrlPressed && (e.keyCode == tecla02) && tecla3Pressed)
					{
						
						if(document.form2.iddeshff.value=="0"){document.form2.iddeshff.value="1";}
						else {document.form2.iddeshff.value="0";}
						document.form2.submit();
					}
					})
					$(document).keyup(function(e){if (e.keyCode ==tecla01){ctrlPressed = false;}})
					$(document).keyup(function(e){if (e.keyCode ==tecla03){tecla3Pressed = false;}
				})

			function cambioswitch(id,valor)
			{
				document.getElementById('idestado').value=id;

				if(valor==1)
                {
                    despliegamodalm('visible','4','Desea Activar la Incapacida No '+id,'1');
                }
				else
                {
                    despliegamodalm('visible','4','Desea Anular la Incapacidad No '+id,'2');
                }
			}
			function despliegamodalm(_valor,_tip,mensa,pregunta)
			{
				document.getElementById("bgventanamodalm").style.visibility=_valor;
				if(_valor=="hidden"){document.getElementById('ventanam').src="";}
				else
				{
					switch(_tip)
					{
						case "1":
							document.getElementById('ventanam').src="ventana-mensaje1.php?titulos="+mensa;break;
						case "2":
							document.getElementById('ventanam').src="ventana-mensaje3.php?titulos="+mensa;break;
						case "3":
							document.getElementById('ventanam').src="ventana-mensaje2.php?titulos="+mensa;break;
						case "4":
							document.getElementById('ventanam').src="ventana-consulta2.php?titulos="+mensa+"&idresp="+pregunta;break;	
					}
				}
			}
			function funcionmensaje(){}
			function respuestaconsulta(estado,pregunta)
			{
				if(estado=="S")
				{
					switch(pregunta)
					{
						case "1":	document.form2.cambioestado.value="1";break;
						case "2":	document.form2.cambioestado.value="0";break;
						case "3":	document.form2.vardeshacer.value="S";break;
					}
				}
				else
				{
					switch(pregunta)
					{
						case "1":	document.form2.nocambioestado.value="1";break;
						case "2":	document.form2.nocambioestado.value="0";break;
						case "3":	document.form2.iddeshacer.value="";break;
					}
				}
				document.form2.submit();
			}
		</script>
		<?php titlepag(); ?>
	</head>
	<body>
		<IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
		<span id="todastablas2"></span>
		<table>
    		<tr><script>barra_imagenes("acti");</script><?php cuadro_titulos();?></tr>	 

    		<tr><?php menu_desplegable("acti");?></tr>

			<tr>
  				<td colspan="3" class="cinta">
                    <img src="imagenes/add.png" title="Nuevo" onClick="location.href='acti-actosAdministrativos.php'" class="mgbt"/>
                    <img src="imagenes/guardad.png" class="mgbt1"/>
                    <img src="imagenes/busca.png" title="Buscar" onClick="document.form2.submit();" class="mgbt"/>
                    <img src="imagenes/nv.png" title="Nueva Ventana" class="mgbt" onClick="mypop=window.open('acti-principal.php','','');mypop.focus();">
                    <img src="imagenes/printd.png" class="mgbt1"/>
                </td>
        	</tr>
     	</table>

        <div id="bgventanamodalm" class="bgventanamodalm">
            <div id="ventanamodalm" class="ventanamodalm">
                <IFRAME src="" name="ventanam" marginWidth=0 marginHeight=0 frameBorder=0 id="ventanam" frameSpacing=0 style=" width:700px; height:130px; top:200; overflow:hidden;"> 
                </IFRAME>
            </div>
        </div>

 		<form name="form2" method="post" action="">
       		<?php 
				if($_POST['oculto'] == "")
				{
					$_POST['numpos'] = 0;
					$_POST['numres'] = 10;
					$_POST['nummul'] = 0;
					$_POST['iddeshff'] = 0;
					$_POST['idestado'] = $_POST['iddeshacer'] = 0;
					$_POST['vardeshacer'] = "N";
				}
			?>

        	<input type="hidden" name="iddeshff" id="iddeshff" value="<?php echo $_POST['iddeshff'];?>"/>

        	<table class="inicio">
      			<tr>
        			<td class="titulos" colspan="9">.: Buscar Actos Administrativos de Acuerdo de Ajuste</td>

                    <td class="cerrar" style="width:7%" onClick="location.href='acti-principal.php'">Cerrar</td>
      			</tr>

               <tr>
			   		<td class="saludo1" style="width:2.2cm;">Fecha Inicial:</td>

       				<td style="width:9%;">
                       <input name="fecha"  type="text" value="<?php echo $_POST['fecha']?>" maxlength="10"  onKeyPress="javascript:return solonumeros(event)" onKeyUp="return tabular(event,this)"  id="fc_1198971545" onKeyDown="mascara(this,'/',patron,true)" style="width:75%;" title="DD/MM/YYYY"/>&nbsp;<a href="#" onClick="displayCalendarFor('fc_1198971545');" title="Calendario"><img src="imagenes/calendario04.png" style="width:20px;"/></a>
                    </td>

       				<td class="saludo1" style="width:2.2cm;">Fecha Final:</td>

       				<td style="width:9%;">
                       <input name="fecha2" type="text" value="<?php echo $_POST['fecha2']?>" maxlength="10"  onKeyPress="javascript:return solonumeros(event)" onKeyUp="return tabular(event,this)"  id="fc_1198971546" onKeyDown="mascara(this,'/',patron,true)" style="width:75%;" title="DD/MM/YYYY"/>&nbsp;<a href="#" onClick="displayCalendarFor('fc_1198971546');" title="Calendario"><img src="imagenes/calendario04.png" style="width:20px;"/></a>
                    </td>

					<td class='saludo1' style='width:7%'>Tipo Entrada</td>

					<td style='width:7%'>
						<select name='tipoentra' id='tipoentra' onChange='validar()'>
							<option value='-1'>:: Seleccione ::</option>
								<?php
                                    $sqlr="SELECT * FROM almtipomov WHERE requiereacto='2' ORDER BY tipom, codigo";
                                    $resp = mysqli_query($linkbd,$sqlr);
                                    while($row = mysqli_fetch_row($resp)) 
                                    {
                                        if("$row[1]$row[0]"==$_POST['tipoentra'])
                                        {
                                            $_POST['tipoentra']="$row[1]$row[0]";
                                            $_POST['ntipoentra']=$row[2];
                                            echo "<option value='$row[1]$row[0]' SELECTED>$row[1]$row[0] - $row[2]</option>";
                                        }
                                        else 
                                        {
                                            echo "<option value='$row[1]$row[0]'>$row[1]$row[0] - $row[2]</option>"; 
                                        }
                                    }
								?>           
						</select>
					</td>

                    <td class="saludo1" style="width:3cm;">Tercero:</td>

                    <td style="width:30%;">
                        <input type="search" style="width:100%; height:30px;" name="numero" id="numero" value="<?php echo $_POST['numero'];?>"/>
                    </td>
                    
                    <td style="width:10%;">
                        <label class="boton02" onClick="document.form2.submit();">&nbsp;&nbsp;Buscar&nbsp;&nbsp;</label>
                    </td>

                   <td colspan="2"></td>
                </tr>
        	</table>

            <input type="hidden" name="oculto" id="oculto" value="1"/>  
            <input type="hidden" name="cambioestado" id="cambioestado" value="<?php echo $_POST['cambioestado'];?>"/>
    		<input type="hidden" name="nocambioestado" id="nocambioestado" value="<?php echo $_POST['nocambioestado'];?>"/>
    		<input type="hidden" name="idestado" id="idestado" value="<?php echo $_POST['idestado'];?>"/> 
            <input type="hidden" name="iddeshacer" id="iddeshacer" value="<?php echo $_POST['iddeshacer'];?>"/> 
            <input type="hidden" name="vardeshacer" id="vardeshacer" value="<?php echo $_POST['vardeshacer'];?>"/>
            <input type="hidden" name="desdel" id="desdel" value="<?php echo $_POST['desdel'];?>"/>
            <input type="hidden" name="numres" id="numres" value="<?php echo $_POST['numres'];?>"/>
    		<input type="hidden" name="numpos" id="numpos" value="<?php echo $_POST['numpos'];?>"/>
       		<input type="hidden" name="nummul" id="nummul" value="<?php echo $_POST['nummul'];?>"/>
            
            <div class="subpantalla" style="height:68.5%; width:99.6%; overflow-x:hidden;">
            	<?php
					$fech1 = split("/",$_POST['fecha']);
					$fech2 = split("/",$_POST['fecha2']);
					$f1 = $fech1[2]."-".$fech1[1]."-".$fech1[0];
					$f2 = $fech2[2]."-".$fech2[1]."-".$fech2[0];

					if($_POST['numero']!="")
                    {
                        $crit1="WHERE nomtercero like '%$_POST[numero]%' ";
                    }
					else
                    {
                        $crit1=" ";
                    }

					if($_POST['fecha']!="" && $_POST['fecha2']!="" && $_POST['numero']=="")
                    {
                        $crit2=" WHERE fecha BETWEEN '$f1' AND '$f2' ";
                    }
					else
                    {
                        $crit2=" ";
                    }

					if($_POST['tipoentra']!="-1" && $_POST['numero']=="" && $_POST['fecha']=="" && $_POST['fecha2']=="")
                    {
                        $crit3=" WHERE tipo_mov='$_POST[tipoentra]' ";
                    }
					elseif($_POST['tipoentra']!="-1" && $_POST['fecha']!="" && $_POST['fecha2']!="")
                    {
                        $crit3=" AND tipo_mov='$_POST[tipoentra]' ";
                    }
					else
                    {
                        $crit3=" ";
                    }

					$sqlr="SELECT id FROM actictoajusteent $crit1 $crit2 $crit3";
					$resp = mysqli_query($linkbd,$sqlr);
					$_POST['numtop'] = mysqli_num_rows($resp);
					$nuncilumnas = ceil($_POST['numtop']/$_POST['numres']);

					$sqlr = "SELECT id,fecha,nomtercero,motivo,valortotal,estado FROM actictoajusteent $crit1 $crit2 $crit3 GROUP BY id ASC LIMIT $_POST[numpos], $_POST[numres]";
                    //echo $sqlr;
					$resp = mysqli_query($linkbd,$sqlr);
					$ntr = mysqli_num_rows($resp);

					$con=1;
					$numcontrol=$_POST['nummul']+1;

					if($nuncilumnas==$numcontrol)
					{
						$imagenforward="<img src='imagenes/forward02.png' style='width:17px'>";
						$imagensforward="<img src='imagenes/skip_forward02.png' style='width:16px' >";
					}
					else 
					{
						$imagenforward="<img src='imagenes/forward01.png' style='width:17px' title='Siguiente' onClick='numsiguiente()'>";
						$imagensforward="<img src='imagenes/skip_forward01.png' style='width:16px' title='Fin' onClick='saltocol(\"$nuncilumnas\")'>";
					}
					if($_POST['numpos']==0)
					{
						$imagenback="<img src='imagenes/back02.png' style='width:17px'>";
						$imagensback="<img src='imagenes/skip_back02.png' style='width:16px'>";
					}
					else
					{
						$imagenback="<img src='imagenes/back01.png' style='width:17px' title='Anterior' onClick='numanterior();'>";
						$imagensback="<img src='imagenes/skip_back01.png' style='width:16px' title='Inicio' onClick='saltocol(\"1\")'>";
					}
					echo "
					<table class='inicio' align='center' >
						<tr>
							<td colspan='6' class='titulos'>.: Resultados Busqueda:</td>
							<td class='submenu'>
								<select name='renumres' id='renumres' onChange='cambionum();' style='width:100%'>
									<option value='10'"; if ($_POST['renumres']=='10'){echo 'selected';} echo ">10</option>
									<option value='20'"; if ($_POST['renumres']=='20'){echo 'selected';} echo ">20</option>
									<option value='30'"; if ($_POST['renumres']=='30'){echo 'selected';} echo ">30</option>
									<option value='50'"; if ($_POST['renumres']=='50'){echo 'selected';} echo ">50</option>
									<option value='100'"; if ($_POST['renumres']=='100'){echo 'selected';} echo ">100</option>
								</select>
							</td>
						</tr>

						<tr><td colspan='7'>Actos de Ajuste Encontrados: $_POST[numtop]</td></tr>

						<tr>
							<th class='titulos2' style='width:5%'>No.</th>
							<th class='titulos2' style='width:10%'>FECHA</th>
							<th class='titulos2' style='width:23%'>TERCERO</td>
							<td class='titulos2'>MOTIVO</td>
							<td class='titulos2' style='width:10%'>VALOR</td>
							<td class='titulos2' style='width:6%;'>ESTADO</td>
							<td class='titulos2' style='width:6%;'>ANULAR</td>
						</tr>";	

					$iter='saludo1a';
					$iter2='saludo2';

					while ($row = mysqli_fetch_row($resp)) 
					{
						switch($row[5])
						{
							case 'S':	
                                $imgsem="src='imagenes/sema_verdeON.jpg' title='Asignada'";
                                $imganu="src='imagenes/borrar02.png' title='No se puede Anular'";
                            break;

							case 'N':	
                                $imgsem="src='imagenes/sema_rojoON.jpg' title='Anulada'";
                                $imganu="src='imagenes/borrar02.png' title='Ya se encuentra Anulada'";
                            break;

							case 'A':	
                                $imgsem="src='imagenes/sema_amarilloON.jpg' title='Activa'";
                                $imganu="src='imagenes/borrar01.png' title='Disponible para Anular'";
                            break;
						}

						echo "
						<tr class='$iter' style='text-transform:uppercase' onDblClick=\"location.href='acti-actoAdministrativoEditar.php?codi=$row[0]'\">
							<td style='text-align:right;'>$row[0]&nbsp;</td>
							<td>$row[1]</td>
							<td>".iconv($_SESSION["VERCARPHPINI"], $_SESSION["VERCARPHPFIN"]."//TRANSLIT",$row[2])."</td>
							<td>".iconv($_SESSION["VERCARPHPINI"], $_SESSION["VERCARPHPFIN"]."//TRANSLIT",$row[3])."</td>
							<td style='text-align:right;'>$".number_format($row[4],0,',','.')."&nbsp;</td>
							<td style='text-align:center;'><img $imgsem style='width:20px'/></td>
							<td style='text-align:center;'><img $imganu style='width:20px' class='icoop'/></td>
						</tr>";

						$aux=$iter;
						$iter=$iter2;
						$iter2=$aux;
					}

					echo"
					</table>

					<table class='inicio'>
						<tr>
							<td style='text-align:center;'>
								<a href='#'>$imagensback</a>&nbsp;
									<a href='#'>$imagenback</a>&nbsp;&nbsp;";
					if($nuncilumnas<=9){$numfin=$nuncilumnas;}
					else{$numfin=9;}
					for($xx = 1; $xx <= $numfin; $xx++)
					{
						if($numcontrol<=9){$numx=$xx;}
						else{$numx=$xx+($numcontrol-9);}
						if($numcontrol==$numx){echo"<a href='#' onClick='saltocol(\"$numx\")'; style='color:#24D915'> $numx </a>";}
						else {echo"<a href='#' onClick='saltocol(\"$numx\")'; style='color:#000000'> $numx </a>";}
					}
					echo"			&nbsp;&nbsp;<a href='#'>$imagenforward</a>
								&nbsp;<a href='#'>$imagensforward</a>
							</td>
						</tr>
					</table>"
				?>
            </div>
           
			<input type="hidden" name="numtop" id="numtop" value="<?php echo $_POST['numtop'];?>" />

		</form>

        <div id="bgventanamodal2">
			<div id="ventanamodal2">
				<IFRAME  src="" name="buster" marginWidth=0 marginHeight=0 frameBorder=0 id="ventana2" frameSpacing=0 style="left:500px; width:900px; height:500px; top:200;"></IFRAME>
        	</div>
		</div>

    </body>
</html>