<?php

    header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require 'comun.inc';
	require 'funciones.inc';
	session_start();
    if(empty($_SESSION)){
        header("location: index.php");
    }
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html lang=es>
	<head>
    <meta charset="utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1"/>
		<meta name="viewport" content="user-scalable=no">
		<title>:: IDEAL 10 - Planeación estrategica</title>
		<link href="favicon.ico" rel="shortcut icon"/>
		<link href="css/css2z.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
        <link href="css/tabs.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
        <link href="css/style.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" href="sweetalert2/dist/sweetalert2.min.css">
        <script type="text/javascript" src="css/programas.js"></script>
        <script src="sweetalert2/dist/sweetalert2.min.js"></script>
		<script src="vue/vue.min.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script src="css/calendario.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
	</head>
	<body>
		<header>
			<table>
				<tr><script>barra_imagenes("plan");</script><?php cuadro_titulos();?></tr>
			</table>
		</header>

        <section id="myapp" v-cloak>
            <input type="hidden" value = "2" ref="pageType">
            <div class="loading-container" v-show="isLoading" >
                <p class="text-loading"data-loading="IDEAL 10">IDEAL 10</p>
            </div>
            <nav>
                <table>
                    <tr><?php menu_desplegable("plan");?></tr>
                </table>
                <div class="bg-white group-btn p-1" id="newNavStyle">
                    <button type="button" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center" @click="location.href='plan-planAnualCompras'">
                        <span>Nuevo</span>
                        <svg viewBox="0 -960 960 960" ><path d="M440-440H200v-80h240v-240h80v240h240v80H520v240h-80v-240Z"/></svg>
                    </button>
                    <button type="button" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center" @click="update()">
                        <span>Guardar</span>
                        <svg viewBox="0 -960 960 960" ><path d="M840-680v480q0 33-23.5 56.5T760-120H200q-33 0-56.5-23.5T120-200v-560q0-33 23.5-56.5T200-840h480l160 160Zm-80 34L646-760H200v560h560v-446ZM480-240q50 0 85-35t35-85q0-50-35-85t-85-35q-50 0-85 35t-35 85q0 50 35 85t85 35ZM240-560h360v-160H240v160Zm-40-86v446-560 114Z"/></svg>
                    </button>
                    <button type="button" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center" @click="location.href='plan-plancomprasbuscar'">
                        <span>Buscar</span>
                        <svg class="fill-black group-hover:fill-white w-5 h-5" viewBox="0 -960 960 960" ><path d="M784-120 532-372q-30 24-69 38t-83 14q-109 0-184.5-75.5T120-580q0-109 75.5-184.5T380-840q109 0 184.5 75.5T640-580q0 44-14 83t-38 69l252 252-56 56ZM380-400q75 0 127.5-52.5T560-580q0-75-52.5-127.5T380-760q-75 0-127.5 52.5T200-580q0 75 52.5 127.5T380-400Z"/></svg>
                    </button>
                    <button type="button" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center" @click="mypop=window.open('plan-principal.php','',''); mypop.focus();">
                        <span>Nueva ventana</span>
                        <svg class="fill-black group-hover:fill-white w-5 h-5" viewBox="0 -960 960 960" ><path d="M320-120v-80h80v-80H160q-33 0-56.5-23.5T80-360v-400q0-33 23.5-56.5T160-840h640q33 0 56.5 23.5T880-760v400q0 33-23.5 56.5T800-280H560v80h80v80H320ZM160-360h640v-400H160v400Zm0 0v-400 400Z"/></svg>
                    </button>
                    <button type="button" class="btn btn-success d-flex justify-between align-items-center" @click="window.location.href='plan-menuPlanAnualAdquisiciones.php'">
                        <span>Atrás</span>
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960"><path d="M760-200v-160q0-50-35-85t-85-35H273l144 144-57 56-240-240 240-240 57 56-144 144h367q83 0 141.5 58.5T840-360v160h-80Z"/></svg>
                    </button>
                </div>
            </nav>
            <article>
                <div class="bg-white">
                    <div>
                        <h2 class="titulos m-0">Editar plan anual de adquisición</h2>
                        
                        <div>
                            <div class="d-flex">
                                <div class="form-control w-50">
                                    <label class="form-label m-0" for="">Consecutivo:</label>
                                    <div class="d-flex">
                                        <button type="button" class="btn btn-primary" @click="nextItem('prev')"><</button>
                                        <input type="text"  style="text-align:center;" v-model="consecutivo" readonly>
                                        <button type="button" class="btn btn-primary" @click="nextItem('next')">></button>
                                    </div>
                                </div>
                                <div class="form-control w-50">
                                    <label class="form-label m-0" for="">Fecha creación<span class="text-danger fw-bolder">*</span></label>
                                    <input type="date" v-model="fechaCreacion" class="text-center">
                                </div>

                                <div class="form-control w-50">
                                    <label class="form-label m-0" for="">Fecha inicio<span class="text-danger fw-bolder">*</span></label>
                                    <input type="date" v-model="fechaInicio" class="text-center">
                                </div>

                                <div class="form-control w-50">
                                    <label class="form-label m-0" for="">Duración (Meses)<span class="text-danger fw-bolder">*</span></label>
                                    <input type="number" v-model="duracion" class="text-center">
                                </div>

                                <div class="form-control">
                                    <label class="form-label m-0" for="">Modalidad<span class="text-danger fw-bolder">*</span></label>
                                    <select v-model="codModalidad">
                                        <option value="">Seleccione</option>
                                        <option v-for="modalidad in modalidades" :value="modalidad.codigo">{{modalidad.codigo}}-{{modalidad.nombre}}</option>
                                    </select>
                                </div>

                                <div class="form-control">
                                    <label class="form-label m-0" for="">Fuente<span class="text-danger fw-bolder">*</span></label>
                                    <select v-model="codFuente">
                                        <option value="">Seleccione</option>
                                        <option v-for="fuente in fuentes" :value="fuente.codigo">{{fuente.codigo}}-{{fuente.nombre}}</option>
                                    </select>
                                </div>
                            </div>

                            <div class="d-flex">
                                <div class="form-control">
                                    <label class="form-label m-0" for="">Descripción<span class="text-danger fw-bolder">*</span></label>
                                    <input type="text" v-model="descripcion">
                                </div>
                            </div>

                            <div class="d-flex">
                                <div class="form-control">
                                    <label class="form-label m-0" for="">Valor estimado<span class="text-danger fw-bolder">*</span></label>
                                    <div class="form-number number-primary">
                                        <span></span>
                                        <input type="text" v-model="valorEstimadoFormat" @input="formatInputNumber('valorEstimadoFormat', 'valorEstimado')" class="text-right">
                                    </div>
                                </div>

                                <div class="form-control">
                                    <label class="form-label m-0" for="">Valor vigencia actual<span class="text-danger fw-bolder">*</span></label>
                                    <div class="form-number number-primary">
                                        <span></span>
                                        <input type="text" v-model="valorVigenciaFormat" @input="formatInputNumber('valorVigenciaFormat', 'valorVigencia')" class="text-right">
                                    </div>
                                </div>

                                <div class="form-control">
                                    <label class="form-label m-0" for="">Requiere vigencias futuras<span class="text-danger fw-bolder">*</span></label>
                                    <select v-model="vigenciasFuturas">
                                        <option value="">Seleccione</option>
                                        <option value="Si">Si</option>
                                        <option value="No">No</option>
                                    </select>
                                </div>

                                <div class="form-control">
                                    <label class="form-label m-0" for="">Estado vigencias futuras<span class="text-danger fw-bolder">*</span></label>
                                    <select v-model="statusVigenciasFuturas">
                                        <option value="">Seleccione</option>
                                        <option value="NA">NA</option>
                                        <option value="No solicitadas">No solicitadas</option>
                                        <option value="Solicitadas">Solicitadas</option>
                                        <option value="Aprobadas">Aprobadas</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="d-flex">
                            <div class="form-control">
                                <label class="form-label m-0" for="">Nombre responsable<span class="text-danger fw-bolder">*</span></label>
                                <input type="text" v-model="responsableName">
                            </div>

                            <div class="form-control">
                                <label class="form-label m-0" for="">Cargo<span class="text-danger fw-bolder">*</span></label>
                                <input type="text" v-model="responsableCargo">
                            </div>
                        </div>

                        <div class="d-flex">
                            <div class="form-control w-25">
                                <label class="form-label m-0" for="">Producto<span class="text-danger fw-bolder">*</span></label>
                                <input type="text" v-model="codProducto" @dblclick="modalProductos=true;" @change="getProducto()" class="colordobleclik" placeholder="Código UNSPSC">
                            </div>

                            <div class="form-control justify-between">
                                <label class="form-label m-0" for=""></label>
                                <input type="text" v-model="nameProducto" readonly>
                            </div>

                            <div class="form-control justify-between w-25">
                                <label for=""></label>
                                <button type="button" @click="addProducto()" class="btn btn-primary">Agregar</button>
                            </div>
                        </div>
                    </div>         
                    
                    <div class="table-responsive" style="height: 30%;">
                        <table class="table table-hover fw-normal">
                            <thead>
                                <tr>
                                    <th class="text-center" style="width: 20%;">Código UNSPSC</th>
                                    <th>Nombre</th>
                                    <th class="text-center">Opción</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr v-for="(producto,index) in productosSelect" :key="index">
                                    <td class="text-center">{{producto.codigo}}</td>
                                    <td>{{producto.nombre}}</td>
                                    <td class="text-center">
                                        <button type="button" class="btn btn-danger btn-sm m-1" @click="delProducto(index)">
                                            Eliminar
                                        </button>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>

                <!-- modales -->
                <div v-show="modalProductos" class="modal">
                    <div class="modal-dialog modal-xl">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title">Productos</h5>
                                <button type="button" @click="modalProductos=false;" class="btn btn-close"><div></div><div></div></button>
                            </div>
                            <div class="modal-body">
                                <div class="overflow-auto max-vh-50 overflow-x-hidden">
                                    <div class="d-flex">
                                        <div class="form-control">
                                            <label class="form-label m-0" for="">Nivel uno<span class="text-danger fw-bolder">*</span></label>
                                            <select v-model="nivelUno" @change="clearNiveles()">
                                                <option value="">Seleccione</option>
                                                <option v-for="uno in productosNivelUno" :value="uno.codigo">{{uno.codigo}}-{{uno.nombre}}</option>
                                            </select>
                                        </div>

                                        <div class="form-control">
                                            <label class="form-label m-0" for="">Nivel dos<span class="text-danger fw-bolder">*</span></label>
                                            <select v-model="nivelDos" @change="clearNiveles()">
                                                <option value="">Seleccione</option>
                                                <option v-for="dos in productosNivelDos" :value="dos.codigo" v-if="dos.padre == nivelUno">{{dos.codigo}}-{{dos.nombre}}</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="d-flex">
                                        <div class="form-control">
                                            <label class="form-label m-0" for="">Nivel tres<span class="text-danger fw-bolder">*</span></label>
                                            <select v-model="nivelTres" @change="clearNiveles()">
                                                <option value="">Seleccione</option>
                                                <option v-for="tres in productosNivelTres" :value="tres.codigo" v-if="tres.padre == nivelDos">{{tres.codigo}}-{{tres.nombre}}</option>
                                            </select>
                                        </div>

                                        <div class="form-control">
                                            <label class="form-label m-0" for="">Nivel cuatro<span class="text-danger fw-bolder">*</span></label>
                                            <select v-model="nivelCuatro" @change="getNivelCinco()">
                                                <option value="">Seleccione</option>
                                                <option v-for="cuatro in productosNivelCuatro" :value="cuatro.codigo" v-if="cuatro.padre == nivelTres">{{cuatro.codigo}}-{{cuatro.nombre}}</option>
                                            </select>
                                        </div>
                                    </div>
                                    <br>
                                    <div class="d-flex flex-column">
                                        <div class="form-control m-0 mb-3">
                                            <input type="search" v-model="txtSearch" @keyup="searchData('modalProductos')" placeholder="Busca código o nombre">
                                        </div>
                                    </div>
                                    
                                    <table class="table table-hover fw-normal">
                                        <thead>
                                            <tr>
                                                <th class="text-center" style="width: 20%;">Código</th>
                                                <th>Nombre</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr v-for="(producto,index) in productos" :key="index" @click="selectItem('selectProducto', producto)">
                                                <td class="text-center">{{producto.codigo}}</td>
                                                <td>{{producto.nombre}}</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </article>
        </section>

        <script src="Librerias/vue/vue.min.js"></script>
		<script src="Librerias/vue/axios.min.js"></script>
		<script type="module" src="planeacion/paa/js/paa_functions.js"></script>

	</body>
</html>