<?php
	require_once("tcpdf/tcpdf_include.php");
	require('comun.inc');
	require('funciones.inc');
    require 'funcionesSP.inc.php';
	session_start();
	date_default_timezone_set("America/Bogota");
	$val = 0;


    class Plantilla{
        private $linkbd;
        private $intId;
        public function __construct(){
            $this->linkbd = conectar_v7();
            $this->linkbd->set_charset("utf8");
        }

        public function show($id){
            if($id > 0){
                $this->intId = $id;
                $sql = "SELECT *, DATE_FORMAT(fecha,'%d/%m/%Y') as fecha FROM srv_prescripciones WHERE id = $this->intId";
                $request = mysqli_query($this->linkbd,$sql)->fetch_assoc();
                $intCodigo = $request['cod_usuario'];
                $intFactura = $request['num_factura'];
                $arrCliente = mysqli_query($this->linkbd,"SELECT id, id_tercero,id_barrio, cod_catastral, id_estrato FROM srvclientes WHERE cod_usuario =$intCodigo")->fetch_assoc();
                $request['barrio'] = buscaNombreBarrio($arrCliente['id_barrio']);
                $request['direccion'] = encuentraDireccionConIdCliente($arrCliente['id']);
                $request['estrato'] = buscarNombreEstrato($arrCliente['id_estrato']);
                $request['nombre'] = buscaNombreTerceroConId($arrCliente['id_tercero']);
                $request['documento'] = buscaDocumentoTerceroConId($arrCliente['id_tercero']);
                $request['catastro'] = $arrCliente['cod_catastral'];
                $servicios = mysqli_fetch_all(mysqli_query($this->linkbd,
                "SELECT f.num_factura,f.id_servicio,s.nombre FROM srvfacturas f INNER JOIN srvservicios s ON f.id_servicio = s.id WHERE f.num_factura = $intFactura;"),MYSQLI_ASSOC);
                $totalServicios = count($servicios);
                for ($i=0; $i < $totalServicios; $i++) {
                    $idServicio = $servicios[$i]['id_servicio'];
                    $sql="SELECT SUM(cargo_f+consumo_b+consumo_c+consumo_s+
                    contribucion_cf+contribucion_cb+contribucion_cc+contribucion_cs
                    +deuda_anterior+abonos+acuerdo_pago+venta_medidor+interes_mora+alumbrado-subsidio_cf-subsidio_cb-subsidio_cc-subsidio_cs) as total
                    FROM srvfacturas WHERE num_factura = '$intFactura' AND id_servicio = $idServicio";
                    $servicios[$i]['total'] = floor(mysqli_query($this->linkbd,$sql)->fetch_assoc()['total']);
                }
                $request['servicios'] = $servicios;
            }
            return $request;
        }
    }
	class MYPDF extends TCPDF{
		public function Header(){

			$linkbd=conectar_v7();
			$linkbd -> set_charset("utf8");
			$sqlr="SELECT nit, razonsocial FROM configbasica WHERE estado='S'";
			$resp=mysqli_query($linkbd,$sqlr);
			while($row=mysqli_fetch_row($resp)){$nit=$row[0];$rs=strtoupper($row[1]);}
			$this->Image('imagenes/escudo.jpg',  13, 13, 20, 20, 'JPG', '', 'T', true, 300, '', false, false, 0, false, false, false);// Logo
			$this->SetFont('helvetica','B',8);
			$this->SetY(10);
			$this->RoundedRect(10, 10, 277, 25, 1,'1111'); //Borde del encabezado
			$this->Cell(26,25,'','R',0,'L');  //Linea que separa el encabazado verticalmente
			$this->SetY(8);
			$this->SetX(80);
			$this->SetFont('helvetica','B',9);
			$this->Cell(160,15,"$rs",0,0,'C');
			$this->SetY(12);
			$this->SetX(80);
			$this->SetFont('helvetica','B',7);
			$this->Cell(160,15,'NIT: '.$nit,0,0,'C');
			$this->SetY(23);
			$this->SetX(36);
			$this->SetFont('helvetica','B',9);
			$this->Cell(251,12,"COMPROBANTE DE PRESCRIPCIÓN",'T',0,'C');
			$this->SetFont('helvetica','I',10);
			$this->SetY(27);
			$this->SetX(62);
			$mov='';
			if(isset($_POST['movimiento']))
			{
				if($_POST['movimiento']=='401' || $_POST['movimiento']=='402'){$mov="DOCUMENTO DE REVERSION";}
			}

			$this->SetFont('helvetica','B',6);
			$this->SetY(10);
			$this->SetX(257);
			$this->Cell(30,5," CONSECUTIVO: ".DATA['consecutivo'],"L",0,'L');
			$this->SetY(13.5);
			$this->SetX(257);
			$this->Cell(35,6," FECHA: ".DATA['fecha'],"L",0,'L');
            $this->SetY(18);
			$this->SetX(257);
			$this->Cell(35,5," VIGENCIA: ".vigencia_usuarios($_SESSION['cedulausu']),"L",0,'L');
			$this->SetFont('helvetica','B',8);
			$this->SetY(25);

			$this->cell(248,8,'NETO A PAGAR: ',0,0,'R');
			$this->RoundedRect(257.5, 26 ,27, 6, 1,'');
			$this->cell(26,8,'$'.number_format(DATA['valor_pagar'],2),0,0,'R');
		}
        public function ColoredTable($data) {
            // Colors, line width and bold font
            $this->SetFillColor(222, 222, 222);
            $this->SetTextColor(000);
            //$this->SetDrawColor(128, 0, 0);
            $this->SetLineWidth(0.3);
            $this->SetFont('helvetica','B',7);
            // Header
            $w = array(207.5, 70);
            $header =array(
                "Nombre",
                "Valor"
            );
            $num_headers = count($header);
            for ($i=0; $i < $num_headers; $i++) {
                $this->Cell($w[$i], 7, $header[$i], 0, 0, 'C', 1);
            }
            $this->SetFont('helvetica','',6);
            $this->Ln();
            // Color and font restoration
            $this->SetFillColor(245,245,245);
            $this->SetTextColor(0);
            $this->SetFont('helvetica','',7.5);
            // Data
            $fill = 0;
            $total = 0;
            foreach($data as $row) {

                $this->Cell($w[0], 7, $row['nombre'], 0, 0, 'C', $fill);
                $this->Cell($w[1], 7, '$'.number_format($row['total'],2), 0, 0, 'C', $fill);
                $this->Ln();
                $fill=!$fill;
                $total+=$row['total'];
            }
            $total = ceil($total/100)*100;
            $this->Cell(array_sum($w), 0, '', '');
            $this->Ln();
            $this->SetFillColor(222, 222, 222);
            $this->SetTextColor(000);
            $this->SetLineWidth(0.3);
            $this->SetFont('helvetica','B',7.5);
            $this->Cell(207.5, 6, 'TOTAL:', '', 0, 'R',1);
            $this->Cell(70, 6, '$'.number_format($total), '', 0, 'C',1);
        }
		public function Footer(){
			$linkbd=conectar_v7();
			$linkbd -> set_charset("utf8");
			$sqlr="SELECT direccion,telefono,web,email FROM configbasica WHERE estado='S'";
			$resp=mysqli_query($linkbd,$sqlr);
			while($row=mysqli_fetch_row($resp))
			{
				$direcc=iconv($_SESSION["VERCARPDFINI"], $_SESSION["VERCARPDFFIN"]."//TRANSLIT",strtoupper($row[0]));
				$telefonos=$row[1];
				$dirweb=iconv($_SESSION["VERCARPDFINI"], $_SESSION["VERCARPDFFIN"]."//TRANSLIT",strtoupper($row[3]));
				$coemail=iconv($_SESSION["VERCARPDFINI"], $_SESSION["VERCARPDFFIN"]."//TRANSLIT",strtoupper($row[2]));
			}
			if($direcc!=''){$vardirec="Dirección: $direcc, ";}
			else {$vardirec="";}
			if($telefonos!=''){$vartelef="Telefonos: $telefonos";}
			else{$vartelef="";}
			if($dirweb!=''){$varemail="Email: $dirweb, ";}
			else {$varemail="";}
			if($coemail!=''){$varpagiw="Pagina Web: $coemail";}
			else{$varpagiw="";}
			$resp=mysqli_query($linkbd, $sqlr);
			$user = $_SESSION['nickusu'];
			$fecha = date("Y-m-d H:i:s");
			$ip = $_SERVER['REMOTE_ADDR'];
			$useri = $_POST['user'];
			//$this->SetY(-16);
			$this->SetFont('helvetica', 'I', 6);
			$txt = <<<EOD
			$vardirec $vartelef
			$varemail $varpagiw
			EOD;
			$this->Cell(277,8,'','T',0,'T');
			$this->ln(2);
			$this->Write(0, $txt , '', 0, 'C', true, 0, false, false, 0);
			$this->Cell(50, 8, 'Impreso por: '.$user, 0, false, 'L', 0, '', 0, false, 'T', 'M');
			$this->Cell(30, 8, 'IP: '.$ip, 0, false, 'C', 0, '', 0, false, 'T', 'M');
			$this->Cell(117, 8, 'Fecha: '.$fecha, 0, false, 'C', 0, '', 0, false, 'T', 'M');
			$this->Cell(30, 8, 'IDEAL.10 S.A.S', 0, false, 'C', 0, '', 0, false, 'T', 'M');
			$this->Cell(58, 8, 'Pagina '.$this->getAliasNumPage().'/'.$this->getAliasNbPages(), 0, false, 'R', 0, '', 0, false, 'T', 'M');



		}
	}
    if($_GET){

        $obj = new Plantilla();
        $request = $obj->show($_GET['id']);
        define('DATA',$request);

        $pdf = new MYPDF('L','mm','Letter', true, 'iso-8859-1', false);// create new PDF document
        $pdf->SetDocInfoUnicode (true);
        // set document information
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('G&CSAS');
        $pdf->SetTitle('Comprobante prescripción');
        $pdf->SetSubject('Certificado de Disponibilidad');
        $pdf->SetKeywords('TCPDF, PDF, example, test, guide');
        $pdf->SetMargins(10, 38, 10);// set margins
        $pdf->SetHeaderMargin(38);// set margins
        $pdf->SetFooterMargin(17);// set margins
        $pdf->SetAutoPageBreak(TRUE, 20);// set auto page breaks
        // set some language-dependent strings (optional)
        if (@file_exists(dirname(__FILE__).'/lang/spa.php'))
        {
            require_once(dirname(__FILE__).'/lang/spa.php');
            $pdf->setLanguageArray($l);
        }
        // ---------------------------------------------------------
        $pdf->AddPage();


        $pdf->SetFillColor(245,245,245);
        $pdf->cell(0.2);
        $pdf->SetFont('helvetica','B',7);
        $pdf->cell(23,4,'Beneficiario: ','LT',0,'L',1);
        $pdf->SetFont('helvetica','',7);
        $ntercero = $request['nombre'];
        $pdf->cell(90,4,''.substr(ucwords($ntercero),0,100),'T',0,'L',1);

        $pdf->SetFont('helvetica','B',7);
        $pdf->cell(23,4,'C.C o NIT: ','T',0,'L',1);
        $pdf->SetFont('helvetica','',7);
        $pdf->cell(141,4,''.$request['documento'],'TR',1,'L',1);

        $pdf->SetFont('helvetica','B',7);
        $pdf->cell(0.2);

        $concepto = 'Prescripcion de la factura número '.$request['num_factura'].' del codigo de usuario '.$request['cod_usuario'].'';
        $lineass = $pdf->getNumLines($concepto, 254);
        $alturadt=(4*$lineass);
        $pdf->MultiCell(23,$alturadt,'Detalle: ', 'LB', 'J', 0, 0, '', '', true, 0, false, true, $alturadt, 'T');
        $pdf->SetFont('helvetica','',8);
        $pdf->MultiCell(254,$alturadt,$concepto,'RB','L',false,1,'','',true,0,false,true,$alturadt,'T',false);

        //	$pdf->Cell(199,$altura,"CONCEPTO:  ".iconv($_SESSION["VERCARPDFINI"], $_SESSION["VERCARPDFFIN"]."//TRANSLIT",strtoupper($_POST['concepto'])),'LR','L',true,1,'','',true,0,false,true,$altura,'M',false);


        $pdf->SetFillColor(245,245,245);
        $pdf->SetFont('helvetica','B',7);
        $pdf->cell(0.2);
        $pdf->SetFillColor(255,255,255);
        $pdf->SetFont('helvetica','B',7);
        $pdf->cell(0.01);
        $pdf->cell(23,4,'Valor factura:','L',0,'L',1);
        $pdf->SetFont('helvetica','',7);
        $pdf->cell(90,4,'$'.number_format($request['valor_deuda'],2),0,0,'L',1);
        $pdf->SetFont('helvetica','B',7);
        $pdf->cell(23,4,'',0,0,'L',1);
        $pdf->SetFont('helvetica','',7);
        $pdf->cell(141,4,'','R',1,'L',1);
        $pdf->SetFillColor(245,245,245);
        $pdf->SetFont('helvetica','B',7);
        $pdf->cell(0.2);
        $pdf->cell(23,4,'Descuento:','L',0,'L',1);
        $pdf->SetFont('helvetica','',7);
        $pdf->cell(254,4,'$'.number_format($request['descuento'],2),'R',1,'L',1);
        $pdf->SetFillColor(255,255,255);
        $pdf->SetFont('helvetica','B',7);
        $pdf->cell(0.2);
        $pdf->cell(23,4,'Valor a pagar: ','LB',0,'L',1);
        $pdf->SetFont('helvetica','',7);
        $pdf->cell(254,4,'$'.number_format($request['valor_pagar'],2),'BR',0,'L',1);
        $pdf->ln(6);
        $pdf->ColoredTable(DATA['servicios']);
        $pdf->Output('comprobante_prescripcion');
    }
?>
