<?php 
	require "comun.inc";
	require "funciones.inc";
	session_start();
	$linkbd = conectar_v7();	
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	header("Cache-control: private");
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es">
	<head>
	 	<meta http-equiv="Content-Type" content="text/html" charset="iso-8859-1"/>
        <meta http-equiv="X-UA-Compatible" content="IE=9"/>
		<title>:: Estratos - Servicios P&uacute;blicos</title>
        <link href="css/css2.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="css/calendario.js"></script>
        
		<script>
			function ponprefijo(pref,opc,clase)
			{
				parent.document.form2.nombreEstrato.value = opc + ' - ' + clase;
				parent.document.form2.id_estrato.value = pref;
				parent.document.form2.nombreEstrato.focus();
				parent.despliegamodal2("hidden");
			} 
		</script> 

		<?php titlepag();?>

	</head>

	<body>
		<?php
			
		?>
	<form action="" method="post" enctype="multipart/form-data" name="form1">
		<table class="inicio">
			<tr>
				<td height="25" colspan="4" class="titulos">Buscar Estratos Existentes</td>

				<td class="cerrar">
                    <a onClick="parent.despliegamodal2('hidden');">&nbsp;Cerrar</a>
                </td>
			</tr>

			<tr>
				<td colspan="4" class="titulos2">Por Descripcion</td>
			</tr>

			<tr>
				<td class="saludo1">Codigo o Nombre Estrato:</td>

				<td colspan="3">
                    <input name="numero" type="text" size="30">

                    <input type="submit" name="Submit" value="Buscar">
					<input type="hidden" name="oculto" id="oculto" value="1">
				</td>
			</tr>
		</table>

		<div class="subpantalla" style="height:85%; width:99.6%; overflow-x:hidden;">
			<table class="inicio">
				<tr>
					<td height="25" colspan="4" class="titulos">:. Resultados Busqueda</td>
				</tr>

				<tr>
					<td class="titulos2" width="10%" style="text-align: center;">Codigo Estrato</td>
					<td class="titulos2">Nombre Estrato</td>
					<td class="titulos2">Uso</td>	  
					<td class="titulos2">Clase</td>
				</tr>
				<?php
				$vigusu=vigencia_usuarios($_SESSION['cedulausu']); 
				$oculto=$_POST['oculto'];
				//echo $oculto;
				//if($oculto!="")
				{
					$linkbd = conectar_v7();

					$cond = " AND  (id LIKE'%".$_POST['numero']."%' OR descripcion LIKE '%".($_POST['numero'])."%')";
					
					$sqlr = "SELECT id, upper(descripcion), tipo, uso FROM srvestratos WHERE estado = 'S' $cond";
					$resp = mysqli_query($linkbd, $sqlr);			

					$co = 'saludo1a';
					$co2 = 'saludo2';	

					while($row = mysqli_fetch_row($resp)) 
					{
                        $sqlTipoUso = "SELECT nombre FROM srvtipo_uso WHERE id = $row[2]";
                        $resTipoUso = mysqli_query($linkbd,$sqlTipoUso);
                        $rowTipoUso = mysqli_fetch_row($resTipoUso);

                        $sqlClaseUso = "SELECT nombre FROM srvusosdesuelo WHERE id = $row[3]";
                        $resClaseUso = mysqli_query($linkbd,$sqlClaseUso);
                        $rowClaseUso = mysqli_fetch_row($resClaseUso);

						echo" 
						<tr class='$co' style='text-transform:uppercase; text-align:center;serv-ventanaEstratos.php' onClick=\"javascript:ponprefijo('$row[0]','$row[1]', '$rowClaseUso[0]')\">
                            <td style='text-align:center'>$row[0]</td>
                            <td>$row[1]</td>
                            <td>$row[2] - $rowTipoUso[0]</td>
                            <td>$row[3] - $rowClaseUso[0]</td>
						</tr> ";
						
						$aux=$co;
						$co=$co2;
						$co2=$aux;
					}
					$_POST['oculto']="";
				}
				?>
			</table>
		</div>
	</form>
</body>
</html> 
