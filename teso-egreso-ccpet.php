<?php
header("Cache-control: no-cache, no-store, must-revalidate");
header("Content-Type: text/html;charset=utf8");

require "comun.inc";
require "funciones.inc";
require "conversor.php";
require "validaciones.inc";

$linkbd_V7 = conectar_v7();
$linkbd_V7->set_charset("utf8");

session_start();
date_default_timezone_set("America/Bogota");

?>
<!DOCTYPE>
<html lang="es">

<head>
	<meta charset="utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1" />
	<meta name="viewport" content="user-scalable=no">
	<title>:: IDEAL 10 - Tesorer&iacute;a</title>
	<link href="favicon.ico" rel="shortcut icon" />

	<link href="css/css2.css?<?php echo date('d_m_Y_h_i_s'); ?>" rel="stylesheet" type="text/css" />
	<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s'); ?>" rel="stylesheet" type="text/css" />
	<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s'); ?>" rel="stylesheet" type="text/css" />
	<link href="css/style.css?<?php echo date('d_m_Y_h_i_s'); ?>" rel="stylesheet" type="text/css" />

	<link href="css/tabs2.css?<?php echo date('d_m_Y_h_i_s'); ?>" rel="stylesheet" type="text/css" />
	<script type="text/javascript" src="css/programas.js?<?php echo date('d_m_Y_h_i_s'); ?>"></script>
	<script type="text/javascript" src="css/calendario.js?<?php echo date('d_m_Y_h_i_s'); ?>"></script>
	<script type="text/javascript" src="JQuery/jquery-2.1.4.min.js?<?php echo date('d_m_Y_h_i_s'); ?>"></script>

	<script>
		function refrescar() {
			document.form2.rp.value = '';
			document.form2.cdp.value = '';
			document.form2.detallecdp.value = '';
			document.form2.cc.value = '';
			document.form2.tercero.value = '';
			document.form2.ntercero.value = '';
			document.form2.detallegreso.value = '';
			document.form2.valorrp.value = '';
			document.form2.saldorp.value = '';
			document.form2.pvigant.value = 0;
			document.form2.valor.value = 0;
			document.form2.base.value = '';
			document.form2.iva.value = '';
			document.form2.totalc.value = '';
			document.form2.totalcf.value = '';
			document.form2.valorcheque.value = '';
			document.form2.cambio.value = '1';
			document.form2.valoregreso.value = 0;
			document.form2.valorretencion.value = 0;
			document.form2.valorcheque.value = 0;
			document.form2.submit();
		}
		function buscacta(e) {
			if (document.form2.cuenta.value != "") {
				document.form2.bc.value = '1';
				document.form2.submit();
			}
		}
		function validar() {
			document.form2.submit();

		}
		function validar2() {
			document.form2.cxpo.value = 1;
			document.form2.tipomovimiento.value = '401';
			document.form2.submit();

		}
		function buscarp(e) {
			if (document.form2.rp.value != "") {

				document.form2.brp.value = '1';
				document.form2.submit();
			}
		}

		function agregardetalle() {
			if (document.form2.numero.value != "" && document.form2.valor.value > 0 && document.form2.banco.value != "") {
				document.form2.agregadet.value = 1;
				document.form2.submit();
			}
			else { alert("Falta informacion para poder Agregar"); }
		}

		function agregardetalled() {
			if (document.form2.retencion.value != "" && document.form2.vporcentaje.value != "") {
				document.form2.agregadetdes.value = 1;
				document.form2.calculaRetencion.value = 1;
				document.form2.submit();
			}
			else { alert("Falta informacion para poder Agregar"); }
		}

		function eliminar(variable) {
			if (confirm("Esta Seguro de Eliminar")) {
				document.form2.elimina.value = variable;
				vvend = document.getElementById('elimina');
				vvend.value = variable;
				document.form2.submit();
			}
		}

		function eliminard(variable) {
			if (confirm("Esta Seguro de Eliminar")) {
				document.form2.eliminad.value = variable;
				document.form2.calculaRetencion.value = 1;
				vvend = document.getElementById('eliminad');
				vvend.value = variable;
				document.form2.submit();
			}
		}

		function guardar() {
			let tipoMov = document.form2.tipomovimiento.value;
			if (tipoMov == 201) {
				var tieneCuenta = true;
				let cuentasDeb = document.form2.elements['cuentaDebito[]'];
				if (!cuentasDeb.length) {
					cuentasDeb = [cuentasDeb];
				}
				const numcuentas = cuentasDeb.length; 
				for (var x = 0; x < numcuentas; x++) {
					if (cuentasDeb[x].value == 'Sin Cuenta' || cuentasDeb[x].value == '') {
						tieneCuenta = false;
						break;
					}
				}
				if (tieneCuenta) {
					var verificar_si_digito_iva = document.getElementById('iva').value;
					if (verificar_si_digito_iva != '') {

						var fechabloqueo = document.form2.fechabloq.value;
						var fechadocumento = document.form2.fecha.value;
						var nuevaFecha = fechadocumento.split("/");
						var fechaCompara = nuevaFecha[2] + "-" + nuevaFecha[1] + "-" + nuevaFecha[0];
						if ((Date.parse(fechabloqueo)) > (Date.parse(fechaCompara))) {
							despliegamodalm('visible', '2', 'Fecha de documento menor que fecha de bloqueo');
						}
						else {
							/*if(vigencia==nuevaFecha[2])
							{ */
							if (document.form2.tipomovimiento.value == '201') {
								var saldo = parseInt(document.form2.saldorp.value);
								var valcxp = parseInt(document.form2.valor.value);
								if (saldo < valcxp) {
									despliegamodalm('visible', '2', 'La orden de pago excede el saldo del RP');
								}
								else {
									if (saldo <= 0) {
										despliegamodalm('visible', '2', 'No hay saldo en dicho registro');
									}
									else {
										if (document.form2.fecha.value != '' && document.form2.destino.value != '' && document.form2.medioDePago.value != '-1') {
											despliegamodalm('visible', '4', 'Esta Seguro de Guardar', '1');
										}
										else { despliegamodalm('visible', '2', 'Falta informacion para Crear la Cuenta'); }
									}
								}
							}
							else {
								if (document.form2.cxp.value != '') {
									despliegamodalm('visible', '4', 'Esta Seguro de Guardar', '1');
								}
								else {
									despliegamodalm('visible', '2', 'Falta informacion para Crear la Cuenta');
								}
							}
							/* }
							else
							{
								despliegamodalm('visible','2','La fecha del documento debe coincidir con su vigencia');
							} */
						}
					} else {
						despliegamodalm('visible', '2', 'Falta digitar el iva, si no tiene iva, digitar 0 (cero).');
					}
				}
				else {
					despliegamodalm('visible', '2', 'Falta atributo contable en la pestaña de contabilidad.');
				}
				
			} else {

				let fechaComp = document.form2.fechaComp.value;
				let fechaRev = document.form2.fecha.value;
				let fechaFrac = fechaComp.split('-');
				let fechaComprobante = fechaFrac[0] + '' + fechaFrac[1] + '' + fechaFrac[2];

				let fechaFracRev = fechaRev.split('/');
				let fechaReversion = fechaFracRev[2] + '' + fechaFracRev[1] + '' + fechaFracRev[0]

				if (parseInt(fechaComprobante) > parseInt(fechaReversion)) {

					despliegamodalm('visible', '2', 'La fecha de reversión no puede ser menor a la fecha de creación del documento.');

				} else {

					let descripcion = document.form2.descripcion.value;
					if (descripcion.length > 12) {

						despliegamodalm('visible', '4', 'Esta Seguro de Guardar', '1');

					} else {
						despliegamodalm('visible', '2', 'Descripción insufieciente.');
					}
				}
			}

		}

		function despliegamodalm(_valor, _tip, mensa, pregunta) {
			document.getElementById("bgventanamodalm").style.visibility = _valor;
			if (_valor == "hidden") {
				document.getElementById('ventanam').src = "";
			}
			else {
				switch (_tip) {
					case "1": document.getElementById('ventanam').src = "ventana-mensaje1.php?titulos=" + mensa; break;
					case "2": document.getElementById('ventanam').src = "ventana-mensaje3.php?titulos=" + mensa; break;
					case "3": document.getElementById('ventanam').src = "ventana-mensaje2.php?titulos=" + mensa; break;
					case "4": document.getElementById('ventanam').src = "ventana-consulta1.php?titulos=" + mensa + "&idresp=" + pregunta; break;
				}
			}
		}

		function funcionmensaje() {
			if (document.form2.tipomovimiento.value == '201') {
				var _cons = document.getElementById('idcomp').value;
				document.location.href = "teso-egresoverccpet.php?idop=" + _cons + "&scrtop=0&numpag=1&limreg=10&filtro=#";
			}
			else {
				var _cons = document.getElementById('cxp').value;
				document.location.href = "teso-egresoverccpet.php?idop=" + _cons + "&scrtop=0&numpag=1&limreg=10&filtro=#";
			}
		}

		function respuestaconsulta(pregunta) {
			switch (pregunta) {
				case "1":
					document.form2.oculto.value = '2';
					document.form2.submit(); break;
			}
		}

		function calcularpago() {
			pvig = parseFloat(document.form2.sumarbase.value);
			pvig2 = parseFloat(document.form2.pvigant.value);
			valorp = document.form2.valor.value;
			var iva = document.form2.iva.value;
			if (iva == '') {
				iva = 0;
			}
			document.form2.base.value = parseFloat(valorp) + parseFloat(pvig) + parseFloat(pvig2) - parseFloat(iva);
			//document.form2.base.value=parseFloat(valorp)+parseFloat(pvig)+parseFloat(pvig2)-parseFloat(document.form2.iva.value);
			descuentos = document.form2.totaldes.value;
			valorc = valorp - descuentos;
			document.form2.valorcheque.value = valorc;
			document.form2.valoregreso.value = valorp;
			document.form2.valorretencion.value = descuentos;
			document.form2.submit();
		}
		function pdf() {
			document.form2.action = "pdfcxp.php";
			document.form2.target = "_BLANK";
			document.form2.submit();
			document.form2.action = "";
			document.form2.target = "";
		}
		function sumapagosant() {
			cali = document.getElementsByName('pagos[]');
			valrubro = document.getElementsByName('pvalores[]');
			sumar = 0;
			for (var i = 0; i < cali.length; i++) {
				if (cali.item(i).checked == true) {
					sumar = parseInt(sumar) + parseInt(valrubro.item(i).value);
				}
			}
			pvig = parseInt(document.form2.pvigant.value);
			document.form2.sumarbase.value = sumar + pvig;
			document.getElementById("sumarbase2").value = sumar + pvig;
			resumar();
		}

		function resumar() {
			cali = document.getElementsByName('rubros[]');
			valrubro = document.getElementsByName('dvalores[]');
			sumar = 0;
			for (var i = 0; i < cali.length; i++) {
				if (cali.item(i).checked == true) { sumar = parseInt(sumar) + parseInt(valrubro.item(i).value); }
			}
			/* if(document.form2.regimen.value==1)
			{
				$d=0;
				iva1=sumar-(sumar/1.19);
				document.form2.iva.value=parseInt(iva1);
				iva2=document.form2.sumarbase.value-(document.form2.sumarbase.value/1.19);
				//alert(iva2);
				document.form2.iva.value=parseInt(document.form2.iva.value)+parseInt(iva2);
			}
			  else
			{
					document.form2.iva.value=0;
			  } */
			//alert(sumar);

			//alert(document.form2.iva.value);
			document.form2.base.value = sumar - document.form2.iva.value + parseInt(document.form2.sumarbase.value);
			document.form2.totalc.value = sumar;
			document.form2.valor.value = sumar;
			document.form2.valoregreso.value = sumar;
			document.form2.valorcheque.value = document.form2.valor.value - document.form2.valorretencion.value;
			document.form2.submit();
		}

		function checkinicios() {
			cali = document.getElementsByName('inicios[]');
			for (var i = 0; i < cali.length; i++) {
				if (document.getElementById("inicia").checked == true) {
					cali.item(i).checked = true;
					document.getElementById("inicia").value = 1;
				}
				else {
					cali.item(i).checked = false;
					document.getElementById("inicia").value = 0;
				}
			}
			document.form2.submit();
		}
		function checktodos() {
			cali = document.getElementsByName('rubros[]');
			valrubro = document.getElementsByName('dvalores[]');
			for (var i = 0; i < cali.length; i++) {
				if (document.getElementById("todos").checked == true) {
					cali.item(i).checked = true;
					document.getElementById("todos").value = 1;
				}
				else {
					cali.item(i).checked = false;
					document.getElementById("todos").value = 0;
				}
			}
			resumar();
		}

		function buscater(e) {
			if (document.form2.tercero.value != "") {
				document.form2.bt.value = '1';
				document.form2.submit();
			}
		}

		function despliegamodal2(_valor, _nomve, _vaux, pos = '', sector = '', cuentaDebito = '', cuentaCredito = '') {
			_vaux = document.getElementById('vigencia').value;
			document.getElementById("bgventanamodal2").style.visibility = _valor;
			if (_valor == "hidden") {
				document.getElementById('ventana2').src = "";
			}
			else {
				switch (_nomve) {
					case "1": document.getElementById('ventana2').src = "registroccpet-ventana01.php?vigencia=" + _vaux; break;
					case "2": document.getElementById('ventana2').src = "tercerosgral-ventana01.php?objeto=tercero&nobjeto=ntercero&tnfoco=detallegreso"; break;
					case "3": document.getElementById('ventana2').src = "reversar-cxp.php?vigencia=" + _vaux; break;
					case "4": document.getElementById('ventana2').src = "ventana-servicio.php?vigencia=" + _vaux; break;
					case "5": document.getElementById('ventana2').src = "parametrizarCuentaccpet-ventana01.php?cuenta=" + _vaux + "&posicion=" + pos + "&sector=" + sector; break;
					case "6":
						{
							let ivaDescontable = document.getElementById('ivaDescontable').value;
							if (ivaDescontable == 2) {
								document.getElementById('ventana2').src = "ivadescontable-ventana.php";
							} else {
								parent.despliegamodal2("hidden");
								alert('No aplica el iva descontable.');
							}
						} break;
					case "7": document.getElementById('ventana2').src = `cuantasDestinodecompra_add.php?pos=${pos}&id=${sector}&cuentadebito=${cuentaDebito}&cuentacredito=${cuentaCredito}`; break;
				}
			}
		}

		function sumatotal(pos) {
			var posicion = pos;
			document.getElementById("ccambio").value = '1';
			document.form2.brp.value = '1';
			var vectorpos = document.getElementsByName("poscheck[]");
			var valor = document.getElementsByName("valauto[]");
			var valitem = valor.item(posicion).value;
			var items = document.getElementsByName("agrega[]");
			var itembool = items.item(posicion).checked;
			if (itembool == true) {
				vectorpos.item(pos).value = '1';
				document.getElementById("sumarauto").value = parseFloat(document.getElementById("sumarauto").value) + parseFloat(valitem);
				document.getElementById("saldoauto").value = parseFloat(document.getElementById("saldoauto").value) - parseFloat(valitem);
			}
			else {
				vectorpos.item(pos).value = '0';
				document.getElementById("sumarauto").value = parseFloat(document.getElementById("sumarauto").value) - parseFloat(valitem);
				document.getElementById("saldoauto").value = parseFloat(document.getElementById("saldoauto").value) + parseFloat(valitem);
			}
			document.getElementById("bservi").value = '';
			document.form2.submit();

		}

		function direccionaCuentaGastos(row) {
			//alert (row);
			window.open("presu-editarcuentaspasiva.php?idcta=" + row);
		}

		function excel() {
			document.form2.action = "teso-egresoregistrosexcel.php";
			document.form2.target = "_BLANK";
			document.form2.submit();
			document.form2.action = "";
			document.form2.target = "";
		}

		function cambiaFecha() {
			var fecha = document.getElementById('fc_1198971545').value;
			fecArray = fecha.split('/');
			document.getElementById('vigencia').value = fecArray[2];
		}
	</script>
</head>

<body>
	<IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
	<span id="todastablas2"></span>
	<table>
		<tr>
			<script>barra_imagenes("teso");</script><?php cuadro_titulos(); ?>
		</tr>
		<tr><?php menu_desplegable("teso"); ?></tr>
	</table>
	<div class="bg-white group-btn p-1"><button type="button" onclick="window.location.href='teso-egreso-ccpet.php'"
			class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
			<span>Nuevo</span>
			<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960">
				<path d="M440-440H200v-80h240v-240h80v240h240v80H520v240h-80v-240Z"></path>
			</svg>
		</button><button type="button" onclick="guardar()"
			class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
			<span>Guardar</span>
			<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960">
				<path
					d="M840-680v480q0 33-23.5 56.5T760-120H200q-33 0-56.5-23.5T120-200v-560q0-33 23.5-56.5T200-840h480l160 160Zm-80 34L646-760H200v560h560v-446ZM480-240q50 0 85-35t35-85q0-50-35-85t-85-35q-50 0-85 35t-35 85q0 50 35 85t85 35ZM240-560h360v-160H240v160Zm-40-86v446-560 114Z">
				</path>
			</svg>
		</button><button type="button" onclick="window.location.href='teso-buscaegresoccpet.php'"
			class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
			<span>Buscar</span>
			<svg class="fill-black group-hover:fill-white w-5 h-5" xmlns="http://www.w3.org/2000/svg"
				viewBox="0 -960 960 960">
				<path
					d="M784-120 532-372q-30 24-69 38t-83 14q-109 0-184.5-75.5T120-580q0-109 75.5-184.5T380-840q109 0 184.5 75.5T640-580q0 44-14 83t-38 69l252 252-56 56ZM380-400q75 0 127.5-52.5T560-580q0-75-52.5-127.5T380-760q-75 0-127.5 52.5T200-580q0 75 52.5 127.5T380-400Z">
				</path>
			</svg>
		</button><button type="button" onclick="mypop=window.open('plan-agenda.php','','');mypop.focus()"
			class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
			<span class="group-hover:text-white">Agenda</span>
			<svg class="fill-black group-hover:fill-white w-5 h-5" xmlns="http://www.w3.org/2000/svg"
				viewBox="0 -960 960 960">
				<path
					d="M560-564v-68q33-14 67.5-21t72.5-7q26 0 51 4t49 10v64q-24-9-48.5-13.5T700-600q-38 0-73 9.5T560-564Zm0 220v-68q33-14 67.5-21t72.5-7q26 0 51 4t49 10v64q-24-9-48.5-13.5T700-380q-38 0-73 9t-67 27Zm0-110v-68q33-14 67.5-21t72.5-7q26 0 51 4t49 10v64q-24-9-48.5-13.5T700-490q-38 0-73 9.5T560-454ZM260-320q47 0 91.5 10.5T440-278v-394q-41-24-87-36t-93-12q-36 0-71.5 7T120-692v396q35-12 69.5-18t70.5-6Zm260 42q44-21 88.5-31.5T700-320q36 0 70.5 6t69.5 18v-396q-33-14-68.5-21t-71.5-7q-47 0-93 12t-87 36v394Zm-40 118q-48-38-104-59t-116-21q-42 0-82.5 11T100-198q-21 11-40.5-1T40-234v-482q0-11 5.5-21T62-752q46-24 96-36t102-12q58 0 113.5 15T480-740q51-30 106.5-45T700-800q52 0 102 12t96 36q11 5 16.5 15t5.5 21v482q0 23-19.5 35t-40.5 1q-37-20-77.5-31T700-240q-60 0-116 21t-104 59ZM280-494Z">
				</path>
			</svg>
		</button><button type="button" onclick="window.open('teso-principal');"
			class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
			<span>Nueva ventana</span>
			<svg class="fill-black group-hover:fill-white w-5 h-5" xmlns="http://www.w3.org/2000/svg"
				viewBox="0 -960 960 960">
				<path
					d="M320-120v-80h80v-80H160q-33 0-56.5-23.5T80-360v-400q0-33 23.5-56.5T160-840h640q33 0 56.5 23.5T880-760v400q0 33-23.5 56.5T800-280H560v80h80v80H320ZM160-360h640v-400H160v400Zm0 0v-400 400Z">
				</path>
			</svg>
		</button><button type="button"
			onclick="mypop=window.open('/financiero/teso-egreso-ccpet.php','','');mypop.focus();"
			class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
			<span class="group-hover:text-white">Duplicar pantalla</span>
			<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960">
				<path
					d="M640-160v-360H160v360h480Zm80-200v-80h80v-360H320v200h-80v-200q0-33 23.5-56.5T320-880h480q33 0 56.5 23.5T880-800v360q0 33-23.5 56.5T800-360h-80ZM160-80q-33 0-56.5-23.5T80-160v-360q0-33 23.5-56.5T160-600h480q33 0 56.5 23.5T720-520v360q0 33-23.5 56.5T640-80H160Zm400-603ZM400-340Z">
				</path>
			</svg>
		</button><button type="button" onclick="excel()"
			class="btn btn-success btn-success-hover d-flex justify-between align-items-center">
			<span>Exportar Excel</span>
			<svg class="fill-black group-hover:fill-white w-4 h-4" xmlns="http://www.w3.org/2000/svg"
				viewBox="0 0 384 512"><!--!Font Awesome Free 6.5.1 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license/free Copyright 2024 Fonticons, Inc. -->
				<path
					d="M48 448V64c0-8.8 7.2-16 16-16H224v80c0 17.7 14.3 32 32 32h80V448c0 8.8-7.2 16-16 16H64c-8.8 0-16-7.2-16-16zM64 0C28.7 0 0 28.7 0 64V448c0 35.3 28.7 64 64 64H320c35.3 0 64-28.7 64-64V154.5c0-17-6.7-33.3-18.7-45.3L274.7 18.7C262.7 6.7 246.5 0 229.5 0H64zm90.9 233.3c-8.1-10.5-23.2-12.3-33.7-4.2s-12.3 23.2-4.2 33.7L161.6 320l-44.5 57.3c-8.1 10.5-6.3 25.5 4.2 33.7s25.5 6.3 33.7-4.2L192 359.1l37.1 47.6c8.1 10.5 23.2 12.3 33.7 4.2s12.3-23.2 4.2-33.7L222.4 320l44.5-57.3c8.1-10.5 6.3-25.5-4.2-33.7s-25.5-6.3-33.7 4.2L192 280.9l-37.1-47.6z">
				</path>
			</svg>
		</button></div>
	<?php
	$vigusu = vigencia_usuarios($_SESSION['cedulausu']);
	//*********** cuenta origen va al credito y la destino al debito
	$maxVersion = ultimaVersionGastosCCPET();
	if (!$_POST['oculto']) {
		$_POST['sinConta'] = array();
		$_POST['cuentaDebito'] = array();
		$_POST['cuentaCredito'] = array();
		$_POST['escogerCuenta'] = array();
		$_POST['contabiliza'] = array();

		$_POST['valauto'] = array();
		$_POST['salauto'] = array();
		$_POST['agrega'] = array();
		$_POST['selectipo'] = "rp";
		$sqlr = "select *from cuentapagar where estado='S' ";
		$res = mysqli_query($linkbd_V7, $sqlr);
		while ($row = mysqli_fetch_row($res)) {
			$_POST['cuentapagar'] = $row[1];
		}
		$_POST['pvigant'] = 0;
		$check1 = "checked";
		$fec = date("d/m/Y");
		$_POST['fecha'] = $fec;
		$dateFragment = explode("/", $fec);
		$_POST['vigencia'] = $dateFragment[2];
		$sqlr = "select max(numerotipo) from comprobante_cab where tipo_comp='11' ";
		$res = mysqli_query($linkbd_V7, $sqlr);
		$consec = 0;
		while ($r = mysqli_fetch_row($res)) {
			$consec = $r[0];
		}
		$consec += 1;
		$_POST['idcomp'] = $consec;
		$sqlr = "select max(id_orden) from tesoordenpago";
		$res = mysqli_query($linkbd_V7, $sqlr);
		$consec = 0;
		while ($r = mysqli_fetch_row($res)) {
			$consec = $r[0];
		}
		$consec += 1;
		$_POST['idcomp'] = $consec;
		if ($_POST['cxpo'] != 1)
			$_POST['tipomovimiento'] = '201';
		$_POST['iva'] = '';
	}
	/* echo $_POST['tabgroup1']."hola"; */
	if ($_POST['tabgroup1']) {
		switch ($_POST['tabgroup1']) {
			case 1:
				$check1 = 'checked';
				break;
			case 2:
				$check2 = 'checked';
				break;
			case 3:
				$check3 = 'checked';
				break;
			case 4:
				$check4 = 'checked';
				break;
			case 5:
				$check5 = 'checked';
				break;
			case 6:
				$check6 = 'checked';
				break;
		}
	} else {
		$check1 = "checked";
	}

	if ($_POST['anticipo'] == 'S') {
		$chkant = ' checked ';
		//$_POST[reado]='readonly';
	}

	?>
	<div id="bgventanamodalm" class="bgventanamodalm">
		<div id="ventanamodalm" class="ventanamodalm">
			<IFRAME src="" name="ventanam" marginWidth=0 marginHeight=0 frameBorder=0 id="ventanam" frameSpacing=0
				style=" width:700px; height:130px; top:200; ">
			</IFRAME>
		</div>
	</div>
	<form name="form2" method="post" action="">
		<?php
		$sesion = $_SESSION['cedulausu'];
		$sqlr = "Select dominios.valor_final from usuarios,dominios where usuarios.cc_usu=$sesion and dominios.NOMBRE_DOMINIO='PERMISO_MODIFICA_DOC' and dominios.valor_inicial=usuarios.cc_usu ";
		$resp = mysqli_query($linkbd_V7, $sqlr);
		$fechaBloqueo = mysqli_fetch_row($resp);
		echo "<input type='hidden' name='fechabloq' id='fechabloq' value='" . $fechaBloqueo[0] . "' />";
		?>
		<table class="inicio">
			<tr>
				<td class="titulos" style="width:100%;">.: Tipo de Movimiento
					<input type="hidden" value="1" name="oculto" id="oculto">
					<select name="tipomovimiento" id="tipomovimiento" onKeyUp="return tabular(event,this)"
						onChange="validar()" style="width:20%;">
						<?php
						$user = $_SESSION['cedulausu'];
						$sql = "SELECT * from permisos_movimientos WHERE usuario='$user' AND estado='T' ";
						$res = mysqli_query($linkbd_V7, $sql);
						$num = mysqli_num_rows($res);
						if ($num == 1) {
							$sqlr = "select * from tipo_movdocumentos where estado='S' and modulo=4 AND (id='2' OR id='4')";
							$resp = mysqli_query($linkbd_V7, $sqlr);
							while ($row = mysqli_fetch_row($resp)) {
								if ($_POST['tipomovimiento'] == $row[0] . $row[1]) {
									echo "<option value='$row[0]$row[1]' SELECTED >$row[0]$row[1]-$row[2]</option>";
								} else {
									echo "<option value='$row[0]$row[1]'>$row[0]$row[1]-$row[2]</option>";
								}
							}
						} else {
							$sql = "SELECT codmov,tipomov from permisos_movimientos WHERE usuario='$user' AND estado='S' AND modulo='4' AND transaccion='TPA' ";
							$res = mysqli_query($linkbd_V7, $sql);
							while ($row = mysqli_fetch_row($res)) {
								if ($_POST['tipomovimiento'] == $row[0]) {
									echo "<option value='$row[0]' SELECTED>$row[0] - $row[1]</option>";
								} else {
									echo "<option value='$row[0]'>$row[0] - $row[1]</option>";
								}

							}
						}

						?>
					</select>
				</td>
				<td style="width:80%;">
				</td>
			</tr>
		</table>
		<input type="hidden" id="vigencia" name="vigencia" value="<?php echo $_POST['vigencia'] ?>"
			onKeyUp="return tabular(event,this)" readonly />
		<?php
		$_POST['destino'] = '00';
		if ($_POST['cambio'] == '1') {
			unset($_POST['dcuentas']);
			unset($_POST['dncuentas']);
			unset($_POST['dvigenciaGasto']);
			unset($_POST['ddivipola']);
			unset($_POST['dchip']);
			unset($_POST['darea']);
			unset($_POST['ddetallesectorial']);
			unset($_POST['dtipo_gasto']);
			unset($_POST['dseccion_presupuestal']);
			unset($_POST['dnombreseccion_presupuestal']);
			unset($_POST['drecursos']);
			unset($_POST['dnrecursos']);
			unset($_POST['dvalores']);
			unset($_POST['dvaloresoc']);
			unset($_POST['dvaloresoc']);
			unset($_POST['pcxp']);
			unset($_POST['pfecha']);
			unset($_POST['prp']);
			unset($_POST['pter']);
			unset($_POST['pvalores']);
			unset($_POST['pagos']);
			unset($_POST['ddescuentos']);
			unset($_POST['dndescuentos']);
			unset($_POST['dporcentajes']);
			unset($_POST['ddesvalores']);
			unset($_POST['bpim']);
			unset($_POST['medio_pago']);
			$_POST['cambio'] = '';
		}
		if ($_POST['bservi'] == '1') {
			$sql = "SELECT SUM(valor) FROM inv_servicio_det WHERE codcompro='$_POST[servicio]' AND estado='S' AND liberado='1' ";
			$res = mysqli_query($linkbd_V7, $sql);
			$fila = mysqli_fetch_row($res);
			$_POST['sumarauto'] = $fila[0];
			$_POST['saldoauto'] = 0;
			//************
			$sql = "SELECT contrasoladquisiciones.coddestcompra FROM contracontrato,inv_servicio,contrasoladquisiciones WHERE inv_servicio.codcompro='$_POST[servicio]' AND contracontrato.id_contrato=inv_servicio.idproceso AND inv_servicio.vigencia='$_POST[vigencia]' AND ! ( inv_servicio.estado =  'R'
OR inv_servicio.estado =  'P' ) AND contracontrato.codsolicitud = contrasoladquisiciones.codsolicitud AND contrasoladquisiciones.estado='S' ";
			$res = mysqli_query($linkbd_V7, $sql);
			$row = mysqli_fetch_row($res);
			$_POST['destino'] = $row[0];
		}

		if ($_POST['brp'] == '1') {
			$saldoRP = generaSaldoRPccpet($_POST['rp'], $_POST['vigencia']);
			if ($saldoRP != 0) {
				$sqlrdc = "SELECT id FROM ccpetdc WHERE vigencia = '" . $_POST['vigencia'] . "' AND consvigencia = '" . $_POST['rp'] . "' AND estado = 'S'";
				$respdc = mysqli_query($linkbd_V7, $sqlrdc);
				$cantdc = mysqli_num_rows($respdc);
				if ($cantdc > 0) {
					$rowdc = mysqli_fetch_row($respdc);
					$_POST['idDestinoCompra'] = $rowdc[0];
					$nresul = buscaregistroccpet($_POST['rp'], $_POST['vigencia']);

					$sqlr = "select idcdp,vigenciacdp from ccpetrp where consvigencia=$_POST[rp] and vigencia=$_POST[vigencia] and tipo_mov='201' ";

					$resp = mysqli_query($linkbd_V7, $sqlr);
					$row = mysqli_fetch_row($resp);
					if ($nresul != '|1') {

						$_POST['cdp'] = $row[0];
						$vigencia = $_SESSION['vigencia'];
						//*** busca detalle cdp
						$sqlr = "select ccpetrp.vigencia,ccpetrp.consvigencia,ccpetcdp.objeto,ccpetrp.estado,ccpetcdp.consvigencia, ccpetrp.valor,ccpetrp.saldo,ccpetrp.tercero, ccpetcdp.objeto from ccpetrp,ccpetcdp where ccpetcdp.consvigencia=$_POST[cdp] and ccpetrp.idcdp=ccpetcdp.consvigencia and ccpetrp.consvigencia=" . $_POST['rp'] . " and ccpetrp.vigencia=$_POST[vigencia] and ccpetrp.idcdp=ccpetcdp.consvigencia and ccpetcdp.vigencia='$row[1]' and ccpetrp.tipo_mov='201' and ccpetcdp.tipo_mov='201' order  by ccpetrp.vigencia,ccpetrp.consvigencia,ccpetcdp.objeto,ccpetrp.estado";
						$resp = mysqli_query($linkbd_V7, $sqlr);
						$row = mysqli_fetch_row($resp);
						$_POST['detallecdp'] = $row[2];
						$_POST['tercero'] = $row[7];
						$_POST['ntercero'] = buscatercero($_POST['tercero']);
						$_POST['regimen'] = buscaregimen($_POST['tercero']);
						$_POST['valorrp'] = $row[5];
						$_POST['saldorp'] = generaSaldoRPccpet($_POST['rp'], $_POST['vigencia']);

						if (isset($_POST['sumarauto']) && !empty($_POST['sumarauto'])) {
							$_POST['valor'] = $_POST['sumarauto'];
						} else {
							$_POST['valor'] = generaSaldoRPccpet($_POST['rp'], $_POST['vigencia']);
						}

						/* if($_POST['regimen']==1)
												{
													$_POST['iva'] = round($_POST['valor']-$_POST['valor']/(1.19),1);
												}
												else
												{
													$_POST['iva'] = 0;
												}

												$_POST['base'] = $_POST['valor']-$_POST['iva'];	*/
						$_POST['base'] = doubleVal($_POST['valor']) - doubleVal($_POST['iva']);
						$_POST['detallegreso'] = str_replace('"', "", $row[8]);
						$_POST['valoregreso'] = $_POST['valor'];
						$_POST['valorretencion'] = $_POST['totaldes'];
						$_POST['valorcheque'] = doubleVal($_POST['valoregreso']) - doubleVal($_POST['valorretencion']);
						$_POST['valorcheque'] = number_format($_POST['valorcheque'], 2);
					} else {
						$_POST['cdp'] = "";
						$_POST['detallecdp'] = "";
						$_POST['tercero'] = "";
						$_POST['ntercero'] = "";
					}
				} else {
					$_POST['cdp'] = "";
					$_POST['detallecdp'] = "";
					$_POST['tercero'] = "";
					$_POST['ntercero'] = "";

					echo "<script>despliegamodalm('visible','2','Este registro presupuestal no tiene radicado de destino de compra, consulte con el contador de la entidad.')</script>";


				}
			} else {
				$_POST['cdp'] = "";
				$_POST['detallecdp'] = "";
				$_POST['tercero'] = "";
				$_POST['ntercero'] = "";

				echo "<script>despliegamodalm('visible','2','Este registro presupuestal no tiene saldo.')</script>";
			}

		}
		if ($_POST['bt'] == '1')//***** busca tercero
		{
			$nresul = buscatercero($_POST['tercero']);
			$_POST['regimen'] = buscaregimen($_POST['tercero']);
			if ($nresul != '') {
				$_POST['ntercero'] = $nresul;
				/* if($_POST['regimen'] == 1)
								{
									$_POST['iva']=round($_POST['valor']-$_POST['valor']/(1.19),1);
								}
								 else
								{
									$_POST['iva']=0;
								}
								 $_POST['base'] = $_POST['valor']-$_POST['iva']; */
				$_POST['base'] = doubleVal($_POST['valor']) - doubleVal($_POST['iva']);
			} else {
				$_POST['ntercero'] = "";
			}
		}
		if (isset($_POST['anticipo']) && $_POST['anticipo'] == "S") {
			//$_POST[reado]='readonly';
			$_POST['iva'] = 0;
		}

		if ($_POST['tipomovimiento'] == '201') {
			?>
			<div class="tabs" style="height:68%; width:99.7%">
				<div class="tab">
					<input type="radio" id="tab-1" name="tabgroup1" value="1" <?php echo $check1; ?>>
					<label id="clabel" for="tab-1">Orden de pago</label>
					<div class="content" style="overflow-x:hidden;">
						<table class="inicio" align="center">
							<tr>
								<td class="titulos" colspan="8">Liquidacion Orden de Pago CCPET</td>
								<td class="cerrar" style="width:7%" onClick="location.href='teso-principal.php'">Cerrar</td>
							</tr>
							<tr>
								<td class="saludo1" style="width:10%;">Numero Orden:</td>
								<td style="width:10%;">
									<input type="text" name="idcomp" id="idcomp" value="<?php echo $_POST['idcomp'] ?>"
										style="width:40%;" readonly />
									<input type="hidden" name='idDestinoCompra' id='idDestinoCompra'
										value="<?php echo $_POST['idDestinoCompra'] ?>">
									<span class="saludo3">Anticipo: <input class="defaultcheckbox" type="checkbox"
											name="anticipo" value="S" onChange="validar()" <?php echo $chkant; ?> /></span>
								</td>
								<td class="saludo1" style="width:5%;">Fecha:</td>
								<td style="width:8%;">
									<input type="text" name="fecha" onchange="cambiaFecha()"
										value="<?php echo $_POST['fecha'] ?>" onKeyUp="return tabular(event,this)"
										id="fc_1198971545" title="DD/MM/YYYY"
										onDblClick="displayCalendarFor('fc_1198971545');" class="colordobleclik"
										autocomplete="off" onChange="" readonly>
								</td>
								<td class="saludo1" style="width:10%">Medio de pago: </td>
								<td style="width:8%">
									<select name="medioDePago" id="medioDePago" onKeyUp="return tabular(event,this)"
										style="width:100%">
										<option value="-1" <?php if (($_POST['medioDePago'] == '-1'))
											echo "SELECTED"; ?>>
											Seleccione...</option>
										<option value="1" <?php if (($_POST['medioDePago'] == '1'))
											echo "SELECTED"; ?>>Con SF
										</option>
										<option value="2" <?php if ($_POST['medioDePago'] == '2')
											echo "SELECTED"; ?>>Sin SF
										</option>
									</select>

								</td>

								<td class="saludo1" style="width:10%;">Tipo de Entrada:</td>
								<td>
									<select name="selectipo" onChange="refrescar()" onKeyUp="return tabular(event,this)"
										style="width:100%;">
										<option value="rp" <?php if ($_POST['selectipo'] == "rp") {
											echo "SELECTED";
										} ?>>
											Registro Presupuestal</option>
										<option value="servicio" <?php if ($_POST['selectipo'] == "servicio") {
											echo "SELECTED";
										} ?>>Entrada de Servicio</option>
									</select>
								</td>

								<!-- <td rowspan="5" colspan="2" style="background:url(imagenes/factura03.png); background-repeat:no-repeat; background-position:right; background-size: 90% 95%"></td> -->
							</tr>
							<tr>

								<input type="hidden" name="cambio" id="cambio" value="<?php echo $_POST['cambio']; ?>" />
								<input type="hidden" name="bservi" id="bservi" value="<?php echo $_POST['bservi']; ?>" />
								<input type="hidden" name="ccambio" id="ccambio" value="<?php echo $_POST['ccambio']; ?>" />
								<input type="hidden" name="destino" id="destino" value="<?php echo $_POST['destino']; ?>" />
								<!--
								<td class="saludo1">Destino Compra:</td>
								  <td>
									<select name="destino"  onChange="validar()" onKeyUp="return tabular(event,this)" style="width:100%;">

										<?php
										/*
																		  $sqlr="select *from almdestinocompra where estado='S' order by codigo	";
																		  $res=mysql_query($sqlr,$linkbd);
																		  while ($row =mysql_fetch_row($res))
																		  {
																			   if("$row[0]"==$_POST[destino]){echo "<option value='$row[0]' SELECTED>$row[0] - $row[1]</option>";}
																			  else {echo "<option value='$row[0]'>$row[0] - $row[1]</option>";}
																		  }
																	  */
										?>
									   </select>
								</td>
								 !-->
								<td class="saludo1">
									<?php
									if ($_POST['selectipo'] == 'rp') {
										echo "Registro: ";
									} else {
										echo "Servicio: ";
									}
									?>
								</td>
								<td>
									<?php
									if ($_POST['selectipo'] == 'rp') {
										?>
										<input type="text" name="rp" id="rp" value="<?php echo $_POST['rp'] ?>"
											onKeyUp="return tabular(event,this)" onBlur="buscarp(event)" style="width:80%;" />
										<?php
									} else {
										?>
										<input type="text" name="servicio" id="servicio" value="<?php echo $_POST['servicio'] ?>"
											onKeyUp="return tabular(event,this)" onBlur="buscarp(event)" style="width:80%;" /
											readonly>
										<input type="hidden" name="rp" id="rp" value="<?php echo $_POST['rp'] ?>">
										<?php
									}
									?>
									<input type="hidden" value="0" name="brp">
									<a href="#"
										onClick="despliegamodal2('visible','<?php if ($_POST['selectipo'] == 'rp') {
											echo "1";
										} else {
											echo "4";
										} ?>','<?php echo $_POST['vigencia'] ?>');"
										title="Buscar Registro">
										<img src="imagenes/find02.png" style="width:20px;">
									</a>
								</td>

								<td class="saludo1">CDP:</td>
								<td><input type="text" id="cdp" name="cdp" value="<?php echo $_POST['cdp'] ?>"
										style="width:100%;" readonly></td>
								<td class="saludo1">Detalle RP:</td>
								<td colspan="3"><input type="text" id="detallecdp" name="detallecdp"
										value="<?php echo $_POST['detallecdp'] ?>" style="width:100%;" readonly></td>
							</tr>
							<tr>
								<td class="saludo1">Tercero:</td>
								<td>
									<input type="text" id="tercero" name="tercero" onKeyUp="return tabular(event,this)" onBlur="buscater(event)" value="<?php echo $_POST['tercero'] ?>" style="width:80%;">
									<input type="hidden" value="0" name="bt">&nbsp;<a href="#" onClick="despliegamodal2('visible','2')"><img src="imagenes/find02.png" style="width:20px;"></a>
								</td>
								<td colspan="6">
									<input type="text" id="ntercero" name="ntercero" value="<?php echo $_POST['ntercero'] ?>"
										style="width:100%;" readonly>
								</td>
							</tr>
							<tr>
								<td class="saludo1">Detalle Orden:</td>
								<td colspan="7"><input type="text" id="detallegreso" name="detallegreso"
										value="<?php echo $_POST['detallegreso'] ?>" style="width:100%;"></td>
							</tr>
							<tr>
								<td class="saludo1">Valor RP:</td>
								<td><input type="text" id="valorrp" name="valorrp" value="<?php echo $_POST['valorrp'] ?>"
										onKeyUp="return tabular(event,this)" style="width:100%;" readonly></td>
								<td class="saludo1">Saldo:</td>
								<td><input type="text" id="saldorp" name="saldorp" value="<?php echo $_POST['saldorp'] ?>"
										onKeyUp="return tabular(event,this)" readonly>
								</td>
								<td class="saludo1">Pagos Vig Anterior:</td>
								<td><input type="text" id="pvigant" name="pvigant" value="<?php echo $_POST['pvigant'] ?>"
										onKeyUp="return tabular(event,this)" onBlur='sumapagosant()' style="width:100%;">
								</td>
								<td class="saludo1">Valor a Pagar:</td>
								<td><input class='inputnum' type="text" id="valor" name="valor"
										value="<?php echo $_POST['valor'] ?>" style="width:100%;" readonly> </td>
							</tr>
							<tr>

								<td class="saludo1">Base:</td>
								<td><input type="text" id="base" name="base" value="<?php echo $_POST['base'] ?>"
										onKeyUp="return tabular(event,this)" onChange='calcularpago()' readonly> </td>
								<td class="saludo1">Iva:</td>
								<td><input type="text" id="iva" name="iva" value="<?php echo $_POST['iva'] ?>"
										onKeyUp="return tabular(event,this)" onChange='calcularpago()'
										onBlur="calcularpago()" style="width:100%;"><input type="hidden" id="regimen"
										name="regimen" value="<?php echo $_POST['regimen'] ?>">
									<?php
									if ($_POST['iva'] > 0) {
										?>

									<td class="saludo1">Iva Descontable:</td>
									<td style="width:14%;">
										<select name="ivaDescontable" id="ivaDescontable" onKeyUp="return tabular(event,this)"
											style="width:100%">
											<option value="1" <?php if (($_POST['ivaDescontable'] == '1'))
												echo "SELECTED"; ?>>No
												aplica</option>
											<option value="2" <?php if ($_POST['ivaDescontable'] == '2')
												echo "SELECTED"; ?>>Si
												aplica</option>
										</select>

									</td>

									<td>
										<input type="number" id="cuentaIvaDescontable" name="cuentaIvaDescontable"
											value="<?php echo $_POST['cuentaIvaDescontable'] ?>"
											onKeyUp="return tabular(event,this)" style="width:100%;" class="colordobleclik"
											onDblClick="despliegamodal2('visible','6')" readonly>
										<input type="hidden" name="reado" id="reado" value="<?php echo $_POST['reado']; ?>">
									</td>
									<?php
									}
									?>
							</tr>
						</table>
						<div class="subpantallac6" style="width:99.6%; height:52%; overflow-x:hidden;">
							<?php
							$sqldestino = "SELECT * FROM pptorp_almacen WHERE id_rp='$_POST[rp]' AND vigencia='$_POST[vigencia]'";
							$resdestino = mysqli_query($linkbd_V7, $sqldestino);
							$rowdestino = mysqli_fetch_row($resdestino);
							$_POST['destcompra'] = $rowdestino[1];
							if ($_POST['brp'] == '1') {
								//$_POST[brp]=0;
								//*** busca contenido del rp
								$_POST['dcuentas'] = array();
								$_POST['dregalias'] = array();
								$_POST['dncuentas'] = array();
								$_POST['dvigenciaGasto'] = array();
								$_POST['dtipo_gasto'] = array();
								$_POST['ddivipola'] = array();
								$_POST['dchip'] = array();
								$_POST['darea'] = array();
								$_POST['ddetallesectorial'] = array();
								$_POST['dseccion_presupuestal'] = array();
								$_POST['dnombreseccion_presupuestal'] = array();
								$_POST['dvalores'] = array();
								$_POST['dvaloresoc'] = array();
								$_POST['drecursos'] = array();
								$_POST['productoServicio'] = array();
								$_POST['nombreProductoServicio'] = array();
								$_POST['indicador_producto'] = array();
								$_POST['bpim'] = array();
								$_POST['medio_pago'] = array();
								$_POST['rubros'] = array();
								$_POST['dnrecursos'] = array();
								$_POST['sector'] = array();
								$_POST['nombreSector'] = array();
								$_POST['inicios'] = array();
								$sqlr = "select *from ccpetrp_detalle where ccpetrp_detalle.consvigencia=(select ccpetrp.consvigencia from ccpetrp where consvigencia=" . $_POST['rp'] . " and ccpetrp.vigencia=$_POST[vigencia] and ccpetrp.tipo_mov='201') and ccpetrp_detalle.vigencia=$_POST[vigencia] and ccpetrp_detalle.tipo_mov='201' ";
								$res = mysqli_query($linkbd_V7, $sqlr);//echo $sqlr;
								while ($r = mysqli_fetch_row($res)) {
									$consec = $r[0];
									$_POST['dcuentas'][] = $r[3];
									if (isset($_POST['sumarauto']) && !empty($_POST['sumarauto'])) {
										$totsaldo = generaSaldoRPxcuentaccpet($r[2], $r[3], $_POST['vigencia'], $r[5], $r[4], $r[11], $r[16], $r[14], $r[17], $r[18],$r[22]);
										$saldo = $_POST['sumarauto'];
										if ($saldo > 0) {
											if ($totsaldo >= $_POST['sumarauto']) {
												$_POST['dvalores'][] = $_POST['sumarauto'];
												$saldo = 0;
											} else {
												$_POST['dvalores'][] = $totsaldo;
												$saldo = $saldo - $totsaldo;
											}
										} else {
											$_POST['dvalores'][] = 0;
										}

									} else {
										$_POST['dvalores'][] = generaSaldoRPxcuentaccpet($r[2], $r[3], $_POST['vigencia'], $r[5], $r[4], $r[11], $r[16], $r[14], $r[17], $r[18],$r[22]);
									}

									$_POST['dvaloresoc'][] = $r[6];
									$_POST['dncuentas'][] = buscacuentaccpetgastos($r[3], $maxVersion);
									$_POST['dvigenciaGasto'][] = $r[14];
									$_POST['dseccion_presupuestal'][] = $r[17];
									$_POST['dnombreseccion_presupuestal'][] = buscarNombreSeccion($r[17]);
									$_POST['dtipo_gasto'][] = $r[18];
									$_POST['ddivipola'][] = $r[19];
									$_POST['dchip'][] = $r[20];
									$_POST['darea'][] = $r[21];
									$_POST['ddetallesectorial'][] = $r[22];
									$_POST['rubros'][] = $r[3];
									$_POST['inicios'][] = $r[3];
									$nfuente = buscafuenteccpet($r[5]);
									$cdfuente = substr($nfuente, 0, strpos($nfuente, "_"));
									$_POST['drecursos'][] = $r[5];
									$_POST['productoServicio'][] = $r[4];
									$tamProductoServicio = strlen($r[4]);
									if ($r[4] != '') {
										if ($tamProductoServicio == 5) {
											$_POST['nombreProductoServicio'][] = buscaservicioccpetgastos($r[4]);
										} else {
											$_POST['nombreProductoServicio'][] = buscaproductoccpetgastos($r[4]);
										}
									}

									$_POST['indicador_producto'][] = $r[11];
									$_POST['medio_pago'][] = $r[12];
									$_POST['bpim'][] = $r[16];
									$indicadorProducto = substr($r[11], 0, 2);
									$_POST['dnrecursos'][] = $nfuente;
									$_POST['sector'][] = $indicadorProducto;
									$_POST['nombreSector'][] = buscaNombreSector($indicadorProducto);
								}
								//$_POST[brp]=0;
								//$_POST[ccambio]=0;
							}
							?>
							<table class="inicio">
								<tr>
									<td colspan="12" class="titulos">Detalle Orden de Pago CCPET</td>
								</tr>
								<?php

								if ($_POST['todos'] == 1) {
									$checkt = 'checked';
								} else {
									$checkt = '';
								}

								if ($_POST['inicia'] == 1) {
									$checkint = 'checked';
								} else {
									$checkint = '';
								}

								?>

								<tr>
									<td class='titulos2' style='text-align:center;'>
									<input type="hidden" id="inicia" name="inicia"
										onClick="checkinicios()" value="<?php echo $_POST['inicia'] ?>" <?php echo $checkint ?> />
										Vig. Gasto
									</td>
									<td class='titulos2' style='text-align:center;'>Sec. presupuestal</td>
									<td class='titulos2' style='text-align:center;'>Medio pago</td>
									<td class="titulos2">Cuenta</td>
									<td class="titulos2">Nombre Cuenta</td>
									<td class="titulos2">Fuente</td>
									<td class='titulos2' style='text-align:center;'>Producto/Servicio</td>
									<td class='titulos2' style='width:10%;text-align:center;'>Programatico MGA</td>
									<td class='titulos2' style='text-align:center;'>BPIM</td>
									<td class="titulos2" style='text-align:center;'>Valor</td>
									<td class="titulos2">
										
											<input type="hidden" id="todos" name="todos"
											onClick="checktodos()" value="<?php echo $_POST['todos'] ?>" <?php echo $checkt ?> /><input type='hidden' name='elimina' id='elimina'></td>
									<input type="hidden" name="destcompra" id="destcompra"
										value="<?php echo $_POST['destcompra'] ?>">
								</tr>
								<?php
								$_POST['totalc'] = 0;
								$_POST['token'] = 0;
								$iter = 'saludo1a';
								$iter2 = 'saludo2';
								if (isset($_POST['dcuentas'])) {
									for ($x = 0; $x < count($_POST['dcuentas']); $x++) {
										$chs = '';
										$chk = '';
										// echo "a".$_POST[dcuentas][$x];
										$ck = '';
										$ch = '';
										if (is_array($_POST['inicios'])) {
											$ck = esta_en_array($_POST['inicios'], $_POST['dcuentas'][$x]);
										}
										if (is_array($_POST['rubros'])) {
											$ch = esta_en_array($_POST['rubros'], $_POST['dcuentas'][$x]);
										}
										if ($ch == '1') {
											$chk = "checked";
											$_POST['totalc'] = doubleVal($_POST['totalc']) + doubleVal($_POST['dvalores'][$x]);
										}

										if ($ck == '1') {
											$chs = "checked";
											$_POST['token'] = doubleVal($_POST['token']) + doubleVal($_POST['dvalores'][$x]);
										}

										$sql1 = "SELECT regalias FROM pptocuentas WHERE cuenta='" . $_POST['dcuentas'][$x] . "' AND (vigencia='$_POST[vigencia]'or vigenciaf='$_POST[vigencia]')";
										$rst1 = mysqli_query($linkbd_V7, $sql1);
										$ar = mysqli_fetch_row($rst1);

										echo "
										<input type='hidden' name='dcuentas[]' value='" . $_POST['dcuentas'][$x] . "'/>
										<input type='hidden' name='dregalias[]' value='" . $ar[0] . "'/>
										<input type='hidden' name='dncuentas[]' value='" . $_POST['dncuentas'][$x] . "'/>
										<input type='hidden' name='dvigenciaGasto[]' value='" . $_POST['dvigenciaGasto'][$x] . "'/>
										<input type='hidden' name='dseccion_presupuestal[]' value='" . $_POST['dseccion_presupuestal'][$x] . "'/>
										<input type='hidden' name='dnombreseccion_presupuestal[]' value='" . $_POST['dnombreseccion_presupuestal'][$x] . "'/>
										<input type='hidden' name='dtipo_gasto[]' value='" . $_POST['dtipo_gasto'][$x] . "'/>
										<input type='hidden' name='ddivipola[]' value='" . $_POST['ddivipola'][$x] . "'/>
										<input type='hidden' name='dchip[]' value='" . $_POST['dchip'][$x] . "'/>
										<input type='hidden' name='darea[]' value='" . $_POST['darea'][$x] . "'/>
										<input type='hidden' name='ddetallesectorial[]' value='" . $_POST['ddetallesectorial'][$x] . "'/>
										<input type='hidden' name='drecursos[]' value='" . $_POST['drecursos'][$x] . "'/>
										<input type='hidden' name='dnrecursos[]' value='" . $_POST['dnrecursos'][$x] . "'/>
										<input type='hidden' name='productoServicio[]' value='" . $_POST['productoServicio'][$x] . "'/>
										<input type='hidden' name='nombreProductoServicio[]' value='" . $_POST['nombreProductoServicio'][$x] . "'/>
										<input type='hidden' name='indicador_producto[]' value='" . $_POST['indicador_producto'][$x] . "'/>
										<input type='hidden' name='bpim[]' value='" . $_POST['bpim'][$x] . "'/>
										<input type='hidden' name='medio_pago[]' value='" . $_POST['medio_pago'][$x] . "'/>
										<input type='hidden' name='sector[]' value='" . $_POST['sector'][$x] . "'/>
										<input type='hidden' name='nombreSector[]' value='" . $_POST['nombreSector'][$x] . "'/>
										<input type='hidden' name='dvalores[]' value='" . $_POST['dvalores'][$x] . "'/>
										<input type='hidden' name='dvaloresoc[]' value='" . $_POST['dvaloresoc'][$x] . "'/>
										<tr  class='$iter' onMouseOver=\"anterior=this.style.backgroundColor;this.style.backgroundColor='#40b3ff';\"
					onMouseOut=\"this.style.backgroundColor=anterior\" style='text-transform:uppercase'>
											<td>
												<input type='hidden' name='inicios[]' disabled value='" . $_POST['dcuentas'][$x] . "' onClick='submit()' $chs>
												" . $_POST['dvigenciaGasto'][$x] . "
											</td>
											<td>" . $_POST['dseccion_presupuestal'][$x] . " - " . $_POST['dnombreseccion_presupuestal'][$x] . "</td>
											<td>" . $_POST['medio_pago'][$x] . "</td>
											<td style='width:10%;'>" . $_POST['dcuentas'][$x] . "</td>
											<td style='width:30%;'>" . $_POST['dncuentas'][$x] . "</td>
											<td style='width:15%;'>" . $_POST['drecursos'][$x] . " - " . $_POST['dnrecursos'][$x] . "</td>
											<td style='width:15%;'>" . $_POST['productoServicio'][$x] . " - " . $_POST['nombreProductoServicio'][$x] . "</td>
											<td style='width:6%;'>" . $_POST['indicador_producto'][$x] . "</td>
											<td style='width:8%;'>" . $_POST['bpim'][$x] . "</td>
											<td style='text-align:right;width:15%; background-color:#80C4E9; cursor:pointer' ";
										if ($ch == '1') {
											echo "onDblClick='llamarventanaegre(this,$x);'";
										}
										echo " >$ " . number_format($_POST['dvalores'][$x], 2, '.', ',') . "</td>
											<td style='width:3%;'><input type='hidden' name='rubros[]' value='" . $_POST['dcuentas'][$x] . "' onClick='resumar()' $chk></td>
										</tr>";
										$aux = $iter;
										$iter = $iter2;
										$iter2 = $aux;

									}
								}
								$resultado = convertir($_POST['totalc']);
								$_POST['letras'] = $resultado . " PESOS M/CTE";
								$_POST['totalcf'] = number_format($_POST['totalc'], 2, ".", ",");
								echo "
								<input type='hidden' name='totalcf' value='$_POST[totalcf]'/>
								<input type='hidden' name='totalc' value='$_POST[totalc]'/>
								<input type='hidden' name='letras' value='$_POST[letras]'/>
								<tr class='$iter' style='text-align:right;'>
									<td colspan = '9' style='font-size:18px; text-align: center; color:gray !important; ' class='saludo1'>TOTAL:</td>
									<td class='saludo1'>$ $_POST[totalcf]</td>
								</tr>
								<tr class='titulos2'>
									<td>Son:</td>
									<td colspan='12'>$_POST[letras]</td>
								</tr>";
								?>
								<script>
									document.form2.valor.value = <?php echo round($_POST['totalc'], 2); ?>;
									document.form2.valoregreso.value = <?php echo round($_POST['totalc'], 2); ?>;
									document.form2.valorcheque.value = document.form2.valor.value - document.form2.valorretencion.value;
								</script>
							</table>
						</div>
						<?php
						if (!$_POST['oculto']) {
							echo " <script> document.form2.fecha.focus();document.form2.fecha.select();</script>";
						}
						//***** busca tercero
						if ($_POST['brp'] == '1') {
							$nresul = buscaregistroccpet($_POST['rp'], $_POST['vigencia']);
							if ($nresul != '') {
								$_POST['cdp'] = $nresul;
								echo "<script></script>";
							} else {
								$_POST['cdp'] = "";
								//echo"<script>alert('Registro Presupuestal Incorrecto');document.form2.rp.select();</script>";
								$_POST['brp'] = '';
							}
						}

						if ($_POST['bt'] == '1') {
							$nresul = buscatercero($_POST['tercero']);
							$_POST['regimen'] = buscaregimen($_POST['tercero']);
							if ($nresul != '') {
								$_POST['ntercero'] = $nresul;
								/* if($_POST['regimen']==1)
													  {
														  $_POST['iva']=round($_POST['valor']-$_POST['valor']/(1.19),1);
													  }
													   else
													  {
														  $_POST['iva']=0;
													  }
													   $_POST['base'] = $_POST['valor']-$_POST['iva']; */
								$_POST['base'] = doubleVal($_POST['valor']) - doubleVal($_POST['iva']);
								echo "
								<script>
									document.getElementById('detallegreso').focus();
									document.getElementById('detallegreso').select();
								</script>";
							} else {
								$_POST['ntercero'] = "";
								echo "<script>alert('Tercero Incorrecto o no Existe');document.getElementById('tercero').value='';document.form2.tercero.focus();</script>";
							}
						}
						?>
					</div>
				</div>
				<div class="tab">
					<input type="radio" id="tab-5" name="tabgroup1" value="5" <?php echo $check5; ?>>
					<label id="clabel" for="tab-5">Pagos Autorizados</label>
					<div class="content">
						<table class="inicio">
							<tr>
								<td class="titulos" colspan="8">Pagos Autorizados - Puede Seleccionar Parcialmente Los Pagos
									Autorizados</td>
							</tr>

							<td style="width: 70% !important" valign="top">
								<table class="inicio" align="center" style="width: 98%; margin: auto !important">

									<tr>
										<td class="titulos2" style="width: 10%"></td>
										<td class="titulos2" style="width: 15%">No. Pago</td>
										<td class="titulos2" style="width: 15%">No. Servicio</td>
										<td class="titulos2" style="width: 30%">Valor Autorizado</td>
										<td class="titulos2" style="width: 30%">Saldo</td>
										<td class="titulos2" style="width: 10%"></td>
									</tr>
									<?php
									$co = 'saludo1';
									$co2 = 'saludo2';
									if ($_POST['selectipo'] == 'servicio') {
										$sqlr = "SELECT inv_servicio_det.codcompro,inv_servicio_det.valor,inv_servicio_det.numpago,inv_servicio_det.estado FROM inv_servicio,inv_servicio_det WHERE inv_servicio.codcompro=inv_servicio_det.codcompro AND !(inv_servicio.estado='R' OR inv_servicio.estado='P') AND inv_servicio.vigencia='$_POST[vigencia]' AND  inv_servicio.codcompro='$_POST[servicio]' AND inv_servicio_det.liberado='1' ORDER BY inv_servicio_det.numpago ASC";

										$sumaservicio = 0;
										$res = mysqli_query($linkbd_V7, $sqlr);
										$i = 1;
										$pos = 0;
										while ($row = mysqli_fetch_row($res)) {
											$habilita = "";
											$check = "";
											if (isset($_POST['poscheck'][$pos])) {
												if ($_POST['poscheck'][$pos] == '1' || $_POST['poscheck'][$pos] == '') {
													$check = "CHECKED";
												} else {
													$check = "";
												}
											} else {
												$check = "CHECKED";
											}
											if ($row[3] == 'P') {
												$habilita = "disabled";
												$_POST['bloqueos'][$pos] = '1';
												$check = "";
											} else {
												$_POST['bloqueos'][$pos] = '0';
											}
											echo "<tr class='$co'>";
											echo "<td>$i</td>";
											echo "<td>$row[0] <input type='hidden' name='posicion[]' value='" . $row[2] . "' />   </td>";
											echo "<td><input name='valauto[]' type='hidden' value='$row[1]' /> $ " . number_format($row[1], 2, ',', '.') . "</td>";
											echo "<td><input name='salauto[]' type='hidden' value='0' /> $ " . number_format(0, 2, ',', '.') . "<input type='hidden' name='bloqueos[]' value='" . $_POST['bloqueos'][$pos] . "' /> </td>";
											echo "<td><input type='checkbox' name='agrega[]' onChange='sumatotal($pos)' $check $habilita/> <input type='hidden' name='poscheck[]' value='" . $_POST['poscheck'][$pos] . "' /></td>";
											echo "</tr>";
											$aux = $co;
											$co = $co2;
											$co2 = $aux;
											$i += 1;
											$pos += 1;
										}
									}
									if (isset($_POST['sumarauto2'])) {
										echo "
								<tr class='$co'>
									<td class='saludo1' align='right' colspan='2'>Total Pago:</td>
									<td align='right'>
										<input id='sumarauto' name='sumarauto' type='hidden' value='" . $_POST['sumarauto'] . "' >
										<input id='sumarauto2' name='sumarauto2' type='text' value=' $ " . number_format($_POST['sumarauto'], 2, ',', '.') . "' readonly style='text-align:right; width: 100%'>
									</td>
									<td align='right'>
										<input id='saldoauto' name='saldoauto' type='hidden' value='" . $_POST['saldoauto'] . "'>
										<input id='saldoauto2' name='saldoauto2' type='text' value=' $ " . number_format($_POST['saldoauto'], 2, ',', '.') . "' readonly style='text-align:right;width: 100%'>
									</td>
								</tr>";
									}
									?>
								</table>
							</td>
							<td style="border: 1px dashed gray; border-radius: 8px">
								<div style="align-content: center;">
									<img src="imagenes/contrato.png" title="Contrato" style="width: 120px; height: 120px" />
								</div>
							</td>

						</table>
					</div>
				</div>
				<div class="tab">
					<input type="radio" id="tab-4" name="tabgroup1" value="4" <?php echo $check4; ?>>
					<label id="clabel" for="tab-4">Pagos Anteriores</label>
					<div class="content">
						<table class="inicio" align="center">
							<tr>
								<td class="titulos" colspan="8">Pagos Anteriores - Seleccione para Agregar Valor a la Base
									de Retencion</td>
							</tr>
							<tr>
								<td class="titulos2">No Orden</td>
								<td class="titulos2">Fecha</td>
								<td class="titulos2">RP</td>
								<td class="titulos2">Tercero</td>
								<td class="titulos2">Valor</td>
								<td class="titulos2">-</td>
							</tr>
							<?php

							if ($_POST['brp'] == '1') {
								$sqlr = "select id_orden, fecha, id_rp,tercero,base from tesoordenpago where estado<>'N' and estado<>'R' and vigencia='$_POST[vigencia]' and id_rp='$_POST[rp]' and tipo_mov='201' ";

								$co = 'saludo1';
								$co2 = 'saludo2';
								$sumabase = 0;
								$res = mysqli_query($linkbd_V7, $sqlr);
								$contpcxp = 0;
								while ($row = mysqli_fetch_row($res)) {
									$valoranterior = 0;
									$chkpag = '';
									$sqlr = "select sum(valor) from tesoordenpago_bases where id_ordenbase=$row[0]";
									$res2 = mysqli_query($linkbd_V7, $sqlr);
									$row2 = mysqli_fetch_row($res2);
									$valoranterior = $row2[0];
									$ch = '';
									if (is_array($_POST['pagos'])) {
										$ch = esta_en_array($_POST['pagos'], $row[0]);
									}

									if ($ch == '1') {
										$chkpag = "checked";
										$sumabase += doubleVal($row[4]) - doubleVal($valoranterior);
									}

									$pago_ant = 0;
									$sqlr = "select sum(valor) from tesoordenpago_det where id_orden='$row[0]' and tipo_mov='201' ";
									$resd = mysqli_query($linkbd_V7, $sqlr);
									while ($rowd = mysqli_fetch_row($resd)) {
										$pago_ant = $rowd[0];
									}
									$nvalor = doubleVal($pago_ant) - doubleVal($valoranterior);
									echo "
									<tr class='$co'>
										<td><input type='hidden' name='pcxp[]' value=" . $row[0] . " >$row[0]</td>
										<td><input type='hidden' name='pfecha[]' value=" . $row[1] . " >$row[1]</td>
										<td><input type='hidden' name='prp[]' value=" . $row[2] . " >$row[2]</td>
										<td><input type='hidden' name='pter[]' value=" . $row[3] . " >$row[3] - " . buscatercero($row[3]) . "</td>
										<td align='right'><input type='text' name='pvalores[]' value=" . $nvalor . " onKeyPress='javascript:return solonumeros(event)' onBlur='sumapagosant()' style='text-align:right'></td>
										<td>
											<center>
												<input type='checkbox' name='pagos[]' name='pagos[]' value='" . $row[0] . "' $chkpag onClick='sumapagosant()' >
											</center>
										</td>
									</tr>";
									$aux = $co;
									$co = $co2;
									$co2 = $aux;
									$contpcxp += 1;
								}
							} else {
								$co = 'saludo1';
								$co2 = 'saludo2';
								$sumabase = 0;
								if (isset($_POST['pvalores'])) {
									for ($x = 0; $x < count($_POST['pvalores']); $x++) {
										$valoranterior = 0;
										$chkpag = '';
										$ch = '';
										if (is_array($_POST['pagos'])) {
											$ch = esta_en_array($_POST['pagos'], $_POST['pcxp'][$x]);
										}
										if ($ch == '1') {
											$chkpag = "checked";
											$sumabase += doubleVal($_POST['pvalores'][$x]) - doubleVal($valoranterior);
										}

										$nvalor = doubleVal($_POST['pvalores'][$x]) - doubleVal($valoranterior);
										echo "
										<tr class='$co'>
											<td><input type='hidden' name='pcxp[]' value=" . $_POST['pcxp'][$x] . " >" . $_POST['pcxp'][$x] . "</td>
											<td><input type='hidden' name='pfecha[]' value=" . $_POST['pfecha'][$x] . " >" . $_POST['pfecha'][$x] . "</td>
											<td><input type='hidden' name='prp[]' value=" . $_POST['prp'][$x] . " >" . $_POST['prp'][$x] . "</td>
											<td><input type='hidden' name='pter[]' value=" . $_POST['pter'][$x] . " >" . $_POST['pter'][$x] . " - " . buscatercero($_POST['pter'][$x]) . "</td>
											<td align='right'><input type='text' name='pvalores[]' value=" . $_POST['pvalores'][$x] . " onKeyPress='javascript:return solonumeros(event)' onBlur='sumapagosant()' style='text-align:right'></td>
											<td><center><input type='checkbox' name='pagos[]' name='pagos[]' value='" . $_POST['pcxp'][$x] . "' $chkpag onClick='sumapagosant()' ></center></td>
										</tr>";
										$aux = $co;
										$co = $co2;
										$co2 = $aux;
									}
								}
							}
							if (isset($sumabase)) {
								echo "
								<tr class='$co'>
									<td class='saludo1' align='right' colspan='4'>Suma Base:</td>
									<td align='right'>
										<input id='sumarbase' name='sumarbase' type='hidden' value='$sumabase' readonly style='text-align:right'>
										<input id='sumarbase2' name='sumarbase2' type='text' value='" . number_format($sumabase, 2) . "' readonly style='text-align:right'>
									</td>
								</tr>";
							}
							?>
						</table>
					</div>
				</div>
				<div class="tab">
					<input type="radio" id="tab-2" name="tabgroup1" value="2" <?php echo $check2; ?>>
					<label id="clabel" for="tab-2">Retenciones</label>
					<div class="content" style="overflow-x:hidden;">
						<?php
						if (!$_POST['calculaRetencion']) {
							$_POST['calculaRetencion'] = 0;
						}
						if ($_POST['manual'] == "0") {
							$coloracti = "#C00";
						} else {
							$coloracti = "#0F0";
						}
						?>
						<table class="inicio" align="center">
							<tr>
								<td class="titulos" colspan="10">Retenciones</td>
								<td class="cerrar" style="width:7%;"><a href="teso-principal.php">&nbsp;Cerrar</a></td>
							</tr>
							<tr>
								<td class="saludo1" style="width:12%;">Retencion y Descuento:</td>
								<td style="width:15%;">
									<select name="retencion" onChange="validar()" onKeyUp="return tabular(event,this)">
										<option value="">Seleccione ...</option>
										<?php
										$sqlr = "select *from tesoretenciones where estado='S'";
										$res = mysqli_query($linkbd_V7, $sqlr);
										while ($row = mysqli_fetch_row($res)) {
											if ("$row[0]" == $_POST['retencion']) {
												echo "<option value='$row[0]' SELECTED>$row[1] - $row[2]</option>";
												$_POST['porcentaje'] = $row[5];
												$_POST['nretencion'] = $row[1] . " - " . $row[2];
												if ($_POST['porcentaje'] > 0) {
													if ($_POST['manual'] != "0") {
														if ($row[7] != '1') {
															$_POST['vporcentaje'] = round((doubleVal($_POST['base']) * doubleVal($_POST['porcentaje'])) / 100);
														} else {
															$_POST['vporcentaje'] = round((doubleVal($_POST['iva']) * doubleVal($_POST['porcentaje'])) / 100);
														}
														$ro = 'readonly';
													}
												} else {
													$ro = '';
													if ($_POST['vporcentaje'] == 0) {
														$ro = '';
													} else {
														$ro = 'readonly';
													}
												}
											} else {
												echo "<option value='$row[0]'>$row[1] - $row[2]</option>";
											}
										}
										?>
									</select>
									<input type="hidden" value="<?php echo $_POST['nretencion'] ?>" name="nretencion">
								</td>
								<td style="width:8%;" class="saludo1">Automatico:</td>
								<td style="width:8%;">
									<input type="hidden" id="contador" name="contador"
										value="<?php echo $_POST['contador']; ?>">

									<input type="hidden" id="oculto12" name="oculto12"
										value="<?php echo $_POST['oculto12']; ?>">
									<input type="hidden" name="estado" value="<?php echo $_POST['estado']; ?>">
									<?php
									$valuees = "ACTIVO";
									$stylest = "width:100%; background-color:#0CD02A ;color:#fff; text-align:center;";
									echo "<input type='hidden' name='estado' id='estado' value='$valuees' style='$stylest' readonly />";
									?>
									<input type='range' name='manual' id="manual" value='<?php echo $_POST['manual'] ?>'
										min='0' max='1' step='1'
										style='background: <?php echo $coloracti; ?>; width:100%;position: relative;float: left;'
										onChange='validar()' />
								</td>
								</td>
								<td style="width:6%;"><input id="porcentaje" name="porcentaje" type="text" size="5"
										value="<?php echo $_POST['porcentaje'] ?>" readonly>%</td>

								<td class="saludo1" style="width:4%;">Valor:</td>
								<td>
									<input class='inputnum' id="vporcentaje" name="vporcentaje" type="text" size="10"
										value="<?php echo $_POST['vporcentaje'] ?>" <?php echo $ro; ?>>
									<input type="hidden" name="calculaRetencion" id="calculaRetencion"
										value="<?php echo $_POST['calculaRetencion'] ?>">
								</td>
								<td style="width:10%;">
									<input type='hidden' class='inputnum' id="totaldes" name="totaldes" size="10"
										value="<?php echo $_POST['totaldes'] ?>" readonly>
									<input type="button" name="agregard" id="agregard" value="   Agregar   "
										onClick="agregardetalled()"><input type="hidden" value="0" name="agregadetdes">
								</td>
							</tr>
							<?php
							if ($_POST['calculaRetencion'] == 0) {
								$_POST['ddescuentos'] = [];
								$_POST['dndescuentos'] = [];
								$_POST['dporcentajes'] = [];
								$_POST['ddesvalores'] = [];
								$_POST['dmanual'] = [];

								$valorRetencion = 0;
								$sqlrRetencion = "SELECT idretencion, porcentaje FROM ccpetdc_retenciones WHERE iddestino = '$_POST[idDestinoCompra]'";
								$resRetencion = mysqli_query($linkbd_V7, $sqlrRetencion);
								while ($rowRetencion = mysqli_fetch_row($resRetencion)) {
									$sqlr = "SELECT iva FROM tesoretenciones WHERE id = '$rowRetencion[0]' AND estado = 'S'";
									$res = mysqli_query($linkbd_V7, $sqlr);
									$row = mysqli_fetch_row($res);
									if ($row[0] == 1) {
										$valorRetencion = round((doubleVal($_POST['iva']) * doubleVal($rowRetencion[1])) / 100);
									} else {
										$valorRetencion = round((doubleVal($_POST['base']) * doubleVal($rowRetencion[1])) / 100);
									}


									$_POST['ddescuentos'][] = $rowRetencion[0];
									$_POST['dndescuentos'][] = buscaRetencionConCodigo($rowRetencion[0]);
									$_POST['dporcentajes'][] = $rowRetencion[1];
									$_POST['ddesvalores'][] = $valorRetencion;
									$_POST['dmanual'][] = $_POST['manual'];
								}
							}
							$_POST['valoregreso'] = $_POST['valor'];
							$_POST['valorretencion'] = $_POST['totaldes'];
							$_POST['valorcheque'] = doubleVal($_POST['valoregreso']) - doubleVal($_POST['valorretencion']);
							if ($_POST['eliminad'] != '') {
								$posi = $_POST['eliminad'];
								unset($_POST['ddescuentos'][$posi]);
								unset($_POST['dndescuentos'][$posi]);
								unset($_POST['dporcentajes'][$posi]);
								unset($_POST['ddesvalores'][$posi]);
								unset($_POST['dmanual'][$posi]);
								$_POST['ddescuentos'] = array_values($_POST['ddescuentos']);
								$_POST['dndescuentos'] = array_values($_POST['dndescuentos']);
								$_POST['dporcentajes'] = array_values($_POST['dporcentajes']);
								$_POST['ddesvalores'] = array_values($_POST['ddesvalores']);
								$_POST['dmanual'] = array_values($_POST['dmanual']);
							}
							if ($_POST['agregadetdes'] == '1') {
								$_POST['ddescuentos'][] = $_POST['retencion'];
								$_POST['dndescuentos'][] = $_POST['nretencion'];
								$_POST['dporcentajes'][] = $_POST['porcentaje'];
								$_POST['ddesvalores'][] = $_POST['vporcentaje'];
								$_POST['dmanual'][] = $_POST['manual'];
								$_POST['agregadetdes'] = '0';
								echo "
		 						<script>
		 							document.form2.porcentaje.value=0;
									document.form2.vporcentaje.value=0;
									document.form2.retencion.value='';
								</script>";
							}
							?>
						</table>
						<table class="inicio" style="overflow:scroll">
							<tr>
								<td class="titulos">Descuento</td>
								<td class="titulos">%</td>
								<td class="titulos">Valor</td>
								<td class="titulos2"><img src="imagenes/del.png"><input type='hidden' name='eliminad'
										id='eliminad'></td>
							</tr>
							<?php
							$totaldes = 0;
							if (isset($_POST['ddescuentos'])) {
								for ($x = 0; $x < count($_POST['ddescuentos']); $x++) {
									echo "
									<input type='hidden' name='dndescuentos[]' value='" . $_POST['dndescuentos'][$x] . "'>
									<input type='hidden' name='ddescuentos[]' value='" . $_POST['ddescuentos'][$x] . "' >
									<input type='hidden' name='dporcentajes[]' value='" . $_POST['dporcentajes'][$x] . "'>
									<input type='hidden' name='ddesvalores[]' value='" . ($_POST['ddesvalores'][$x]) . "'>
									<input type='hidden' name='dmanual[]' value='" . ($_POST['dmanual'][$x]) . "'>
									<tr>
										<td class='saludo2'>" . $_POST['dndescuentos'][$x] . "</td>
										<td class='saludo2'>" . $_POST['dporcentajes'][$x] . "</td>
										<td class='saludo2'>" . number_format($_POST['ddesvalores'][$x], 2) . "</td>
										<td class='saludo2'><a href='#' onclick='eliminard($x)'><img src='imagenes/del.png'></a></td>
									</tr>";

									$totaldes = $totaldes + ($_POST['ddesvalores'][$x]);
								}
							}
							if (isset($totaldes)) {
								$_POST['valorretencion'] = $totaldes;
								echo "
								<script>
									document.form2.totaldes.value='$totaldes';
									document.form2.valorretencion.value='$totaldes';
								</script>";
								echo "<tr>
									<td colspan = '2' style='font-size:18px; text-align: center; color:gray !important; ' class='saludo1'>TOTAL:</td>
									<td class='saludo1'>
										". number_format($totaldes, 2) . "
									</td>
									<td></td>
								</tr>";
							}
							$_POST['valorcheque'] = doubleVal($_POST['valoregreso']) - doubleVal($_POST['valorretencion']);
							?>
						</table>
					</div>
				</div>
				<div class="tab">
					<input type="radio" id="tab-3" name="tabgroup1" value="3" <?php echo $check3; ?>>
					<label id="clabel" for="tab-3">Cuenta por Pagar</label>
					<div class="content" style="overflow-x:hidden;">
						<table class="inicio" align="center">
							<tr>
								<td colspan="6" class="titulos">Cheque</td>
								<td class="cerrar"><a href="teso-principal.php">&nbsp;Cerrar</a></td>
							</tr>
							<tr colspan="3">
								<td class="saludo1" style="width:10%;">Cuenta Contable:</td>
								<td>
									<input name="cuentapagar" type="text" value="<?php echo $_POST['cuentapagar'] ?>"
										readonly>
									<input name="cb" type="hidden" value="<?php echo $_POST['cb'] ?>"><input type="hidden"
										id="ter" name="ter" value="<?php echo $_POST['ter'] ?>">
								</td>
								<td class="saludo1" style="width:10%;">Valor Retenciones:</td>
								<td><input type="text" id="valorretencion" name="valorretencion"
										value="<?php echo $_POST['valorretencion'] ?>" onKeyUp="return tabular(event,this)"
										readonly></td>
							</tr>
							<tr>
								<td class="saludo1">Valor Orden de Pago:</td>
								<td>
									<input type="text" id="valoregreso" name="valoregreso"
										value="<?php echo $_POST['valoregreso'] ?>" onKeyUp="return tabular(event,this)"
										readonly>
								</td>

								<td class="saludo1">Valor Cta Pagar:</td>
								<td><input class='inputnum' type="text" id="valorcheque" name="valorcheque"
										value="<?php echo $_POST['valorcheque'] ?>" size="20"
										onKeyUp="return tabular(event,this)" readonly></td>
							</tr>
						</table>
					</div>
				</div>
				<div class="tab">
					<input type="radio" id="tab-6" name="tabgroup1" value="6" <?php echo $check6; ?>>
					<label id="clabel" for="tab-6">Contabilidad</label>
					<div class="content" style="overflow-x:hidden;">
						<table class="inicio">
							<tr>
								<td colspan="13" class="titulos">Parametrizar Progranacion Contable</td>
							</tr>
							<tr>
								<td class='titulos2' style='text-align:center;'>Vig. Gasto</td>
								<td class='titulos2' style='text-align:center;'>Sec. presupuestal</td>
								<td class='titulos2' style='text-align:center;'>Cuenta</td>
								<td class='titulos2' style='text-align:center;'>Nombre Cuenta</td>
								<td class='titulos2' style='text-align:center;'>Fuente</td>
								<td class='titulos2' style='text-align:center;'>Sector</td>
								<td class='titulos2' style='text-align:center;'>Producto/Servicio</td>
								<td class='titulos2' style='text-align:center;'>Programativo MGA</td>
								<td class='titulos2' style='text-align:center;'>Medio de Pago</td>
								<td class='titulos2' style='text-align:center;'>Valor</td>
								<td class='titulos2' style='text-align:center;'>Cuenta debito</td>
								<td class='titulos2' style='text-align:center;'>Cuenta Credito</td>
								<td class='titulos2' style='text-align:center;'>Contabiliza</td>
							</tr>
							<?php
							$iter = 'saludo1a';
							$iter2 = 'saludo2';

							if (isset($_POST['dcuentas'])) {
								for ($x = 0; $x < count($_POST['dcuentas']); $x++) {
									$sqlrCuentaCont = "SELECT cuenta_debito, cuenta_credito, contabiliza, id FROM ccpetdc_detalle WHERE vigencia = '$_POST[vigencia]' AND consvigencia = '$_POST[idDestinoCompra]' AND cuenta = '" . $_POST['dcuentas'][$x] . "' AND productoservicio = '" . $_POST['productoServicio'][$x] . "' AND fuente = '" . $_POST['drecursos'][$x] . "' AND indicador_producto = '" . $_POST['indicador_producto'][$x] . "' AND cod_vigenciag = '" . $_POST['dvigenciaGasto'][$x] . "' AND medio_pago = '" . $_POST['medio_pago'][$x] . "' AND seccion_presupuestal = '" . $_POST['dseccion_presupuestal'][$x] . "' AND bpim = '" . $_POST['bpim'][$x] . "'";
									$resCuentaCont = mysqli_query($linkbd_V7, $sqlrCuentaCont);
									$rowCuentaCont = mysqli_fetch_row($resCuentaCont);

									if ($rowCuentaCont[2] == 'S') {
										$_POST["idswsino$x"] = 'checked';
									}

									$sqladd = "SELECT id FROM ccpetdc_cuentasadd WHERE id_detalle = '$rowCuentaCont[3]'";
									$resadd = mysqli_query($linkbd_V7, $sqladd);
									$numAdd = mysqli_num_rows($resadd);
									if($numAdd > 0){
										$_POST['cuentaDebito'][$x] = $_POST['cuentaDebito'][$x];
										$_POST['cuentaCredito'][$x] = $_POST['cuentaCredito'][$x];
										$activaAdd =  "onClick=\"despliegamodal2('visible', '7', '', '$x', '$rowCuentaCont[3]', '$rowCuentaCont[0]', '$rowCuentaCont[1]')\"";
									} else {
										$activaAdd = '';
										$_POST['cuentaDebito'][$x] = $rowCuentaCont[0];
										$_POST['cuentaCredito'][$x] = $rowCuentaCont[1];
									}

									echo "
									<tr  class='$iter' style='cursor: hand'>
										<td style='width:10%;'>" . $_POST['dvigenciaGasto'][$x] . "</td>
										<td style='width:10%;'>" . $_POST['dseccion_presupuestal'][$x] . " - " . $_POST['dnombreseccion_presupuestal'][$x] . "</td>
										<td style='width:10%;'>" . $_POST['dcuentas'][$x] . "</td>
										<td style='width:20%;'>" . $_POST['dncuentas'][$x] . "</td>
										<td style='width:20%;'>" . $_POST['drecursos'][$x] . " - " . $_POST['dnrecursos'][$x] . "</td>
										<td style='width:15%;'>" . $_POST['sector'][$x] . " - " . $_POST['nombreSector'][$x] . "</td>
										<td style='width:15%;'>" . $_POST['productoServicio'][$x] . " - " . $_POST['nombreProductoServicio'][$x] . "</td>
										<td style='width:8%;'>" . $_POST['indicador_producto'][$x] . "</td>
										<td style='width:4%;'>" . $_POST['medio_pago'][$x] . "</td>
										<td style='width:10%;'>" . number_format($_POST['dvalores'][$x], 2, ',', '.') . "</td>
										<td style='width:10%;'><input name='cuentaDebito[]' class='colordobleclik' value='".$_POST['cuentaDebito'][$x]."' $activaAdd readonly></td>
										<td style='width:10%;'>
											<input name='cuentaCredito[]' class='colordobleclik' value='".$_POST['cuentaCredito'][$x]."' $activaAdd readonly>
											<input name='contabiliza[]' type='hidden' value='".$rowCuentaCont[2]."' readonly>
										</td>
										<td style='text-align:center;'>
											<div class='swsino'>
												<input type='checkbox' disabled name='idswsino$x' class='swsino-checkbox' id='idswsino$x' value='" . $_POST["idswsino$x"] . "' " . $_POST["idswsino$x"] . " onChange=\"cambiocheck('$x');\">
												<label class='swsino-label' for='idswsino$x'>
													<span class='swsino-inner'></span>
													<span class='swsino-switch'></span>
												</label>
											</div>
										</td>";

									echo "
									</tr>";
									$aux = $iter;
									$iter = $iter2;
									$iter2 = $aux;
								}
							}
							?>
						</table>
					</div>
				</div>
			</div>
			<?php
		} else {
			?>
			<table class="inicio">
				<tr>
					<td class="titulos" colspan="6">.: Documento a Reversar</td>
				</tr>
				<tr>
					<td class="saludo1" style="width:10%;">N&uacute;mero Orden:</td>
					<td style="width:13%;">
						<input type="hidden" name="cxpo" id="cxpo" value="<?php echo $_POST['cxpo'] ?>" readonly>
						<input type="text" name="cxp" id="cxp" value="<?php echo $_POST['cxp']; ?>" style="width:80%;"
							onBlur="validar2()" onKeyUp="return tabular(event,this)" readonly>
						<a href="#" onClick="despliegamodal2('visible','3','<?php echo $_POST['vigencia'] ?>');"
							title="Buscar Orden de pago"><img src="imagenes/find02.png" style="width:20px;"></a>
					</td>
					<td class="saludo1" style="width:10%;">Fecha:</td>
					<td style="width:10%;">
						<input type="text" name="fecha" value="<?php echo $_POST['fecha'] ?>"
							onKeyUp="return tabular(event,this)" id="fc_1198971545" title="DD/MM/YYYY"
							onDblClick="displayCalendarFor('fc_1198971545');" class="colordobleclik" autocomplete="off"
							onChange="" readonly>
					</td>
					<td class="saludo1" style="width:10%;">Descripcion: </td>
					<td style="width:60%;" colspan="3">
						<input type="text" name="descripcion" id="descripcion" value="<?php echo $_POST['descripcion'] ?>"
							style="width:100%;">
					</td>
				</tr>
				<tr>
					<td class="saludo1">Tipo de entrada</td>
					<td>
						<?php
						if (isset($_POST['cxp'])) {
							if (!empty($_POST['cxp'])) {
								$sql = "SELECT 1 FROM inv_servicio_cxp WHERE cxp='$_POST[cxp]' ";
								$res = mysqli_query($linkbd_V7, $sql);
								$num = mysqli_num_rows($res);
								if ($num == 0) {
									$_POST['selectipo'] = "rp";
									echo "<script>document.getElementById('selectipo').value='rp'; </script>";
								} else {
									$_POST['selectipo'] = "servicio";
									echo "<script>document.getElementById('selectipo').value='servicio'; </script>";
								}
							}
						}
						?>
						<select name="selectipo" onChange="refrescar()" onKeyUp="return tabular(event,this)"
							style="width:94%;">
							<option value="rp" <?php if ($_POST['selectipo'] == "rp") {
								echo "SELECTED";
							} ?>>Registro
								Presupuestal</option>
							<option value="servicio" <?php if ($_POST['selectipo'] == "servicio") {
								echo "SELECTED";
							} ?>>Entrada
								de Servicio</option>
						</select>
					</td>
					<td class="saludo1">No. RP: </td>
					<td><input type="text" name="rp" id="rp" value="<?php echo $_POST['rp']; ?>" readonly></td>
					<td class="saludo1">Valor Orden: </td>
					<td><input type="text" name="valorcxp" id="valorcxp" value="<?php echo $_POST['valorcxp']; ?>" readonly>
					</td>
				</tr>
			</table>
			<div class="subpantallac6" style="width:99.6%; height:52%; overflow-x:hidden;">
				<?php

				if ($_POST['cxp'] != '') {
					if ($_POST['tipomovimiento'] == '201') {
						$_POST['dcuentas'] = array();
						$_POST['dvigenciaGasto'] = array();
						$_POST['dseccion_presupuestal'] = array();
						$_POST['dnombreseccion_presupuestal'] = array();
						$_POST['dtipo_gasto'] = array();
						$_POST['ddivipola'] = array();
						$_POST['dchip'] = array();
						$_POST['darea'] = array();
						$_POST['ddetallesectorial'] = array();
						$_POST['dncuentas'] = array();
						$_POST['dvalores'] = array();
						$_POST['dvaloresoc'] = array();
						$_POST['drecursos'] = array();
						$_POST['rubros'] = array();
						$_POST['dnrecursos'] = array();
						$_POST['productoServicio'] = array();
						$_POST['nombreProductoServicio'] = array();
						$_POST['indicador_producto'] = array();
						$_POST['bpim'] = array();
						$_POST['sector'] = array();
						$_POST['nombreSector'] = array();
						$_POST['medio_pago'] = array();
						$sqlr = "select *from ccpetrp_detalle where ccpetrp_detalle.consvigencia=$_POST[rp] and ccpetrp_detalle.vigencia=$_POST[vigencia] where tipo_mov='201' ";

						$res = mysqli_query($linkbd_V7, $sqlr);
						while ($r = mysqli_fetch_row($res)) {
							$consec = $r[0];
							$_POST['dcuentas'][] = $r[3];
							$_POST['dvalores'][] = generaSaldoRPxcuentaccpet($r[2], $r[3], $_POST['vigencia'], $r[5], $r[4], $r[11], $r[16], $r[14], $r[17], $r[18]);
							$_POST['dvaloresoc'][] = generaSaldoRPxcuentaccpet($r[2], $r[3], $_POST['vigencia'], $r[5], $r[4], $r[11], $r[16], $r[14], $r[17], $r[18]);
							$_POST['dncuentas'][] = buscacuentaccpetgastos($r[3], $maxVersion);
							$_POST['rubros'][] = $r[3];
							$nfuente = buscafuenteccpet($r[5]);
							$cdfuente = substr($nfuente, 0, strpos($nfuente, "_"));
							$_POST['drecursos'][] = $cdfuente;
							$_POST['dnrecursos'][] = $nfuente;
							$_POST['productoServicio'][] = $r[4];
							$tamProductoServicio = strlen($r[9]);
							if ($r[9] != '') {
								if ($tamProductoServicio == 5) {
									$_POST['nombreProductoServicio'][] = buscaservicioccpetgastos($r[9]);
								} else {
									$_POST['nombreProductoServicio'][] = buscaproductoccpetgastos($r[9]);
								}
							}
							$_POST['indicador_producto'][] = $r[11];
							$_POST['medio_pago'][] = $r[12];
							$_POST['dvigenciaGasto'][] = $r[14];
							$_POST['dseccion_presupuestal'][] = $r[17];
							$_POST['dnombreseccion_presupuestal'][] = buscarNombreSeccion($r[17]);
							$_POST['dtipo_gasto'][] = $r[18];
							$_POST['ddivipola'][] = $r[19];
							$_POST['dchip'][] = $r[20];
							$_POST['darea'][] = $r[21];
							$_POST['ddetallesectorial'][] = $r[22];
							$_POST['bpim'][] = $r[16];
							$indicadorProducto = substr($r[11], 0, 2);
							$_POST['sector'][] = $indicadorProducto;
							$_POST['nombreSector'][] = buscaNombreSector($indicadorProducto);

						}
					} else if ($_POST['tipomovimiento'] == '401') {
						//$_POST[brp]=0;
						//*** busca contenido del rp
						$_POST['dcuentas'] = array();
						$_POST['dncuentas'] = array();
						$_POST['dcc'] = array();
						$_POST['dvalores'] = array();
						$_POST['dfuentes'] = array();
						$_POST['dnfuentes'] = array();
						$_POST['dproductosServicios'] = array();
						$_POST['dnproductosServicios'] = array();
						$_POST['dindicadorProducto'] = array();
						$_POST['dmedioPago'] = array();
						$_POST['dvigenciaGasto'] = array();
						$_POST['dbpim'] = array();
						$_POST['dseccionPresupuestal'] = array();



						$sqlr = "select * from tesoordenpago_det where id_orden=$_POST[cxp] and tipo_mov='201' ";
						$res = mysqli_query($linkbd_V7, $sqlr);
						while ($r = mysqli_fetch_row($res)) {
							$consec = $r[1];
							/* $_POST[''] */
							$_POST['dcuentas'][] = $r[2];
							$_POST['dncuentas'][] = buscacuentaccpetgastos($r[2], $maxVersion);
							$_POST['dcc'][] = $r[3];
							$_POST['dvalores'][] = $r[4];
							$_POST['dfuentes'][] = $r[8];
							$_POST['dnfuentes'][] = buscafuenteccpet($r[8]);
							$_POST['dproductosServicios'][] = $r[9];
							$_POST['dindicadorProducto'][] = $r[10];
							$_POST['dmedioPago'][] = $r[11];
							$_POST['dvigenciaGasto'][] = $r[12];
							$_POST['dbpim'][] = $r[14];
							$_POST['dseccionPresupuestal'] = $r[15];

							$tamProductoServicio = strlen($r[9]);
							if ($r[9] != '') {
								$numIniProductoServicio = substr($r[9], 0, 1);
								if (intval($numIniProductoServicio) > 4) {
									$_POST['dnproductosServicios'][] = buscaservicioccpetgastos($r[9]);
								} else {
									$_POST['dnproductosServicios'][] = buscaproductoccpetgastos($r[9]);
								}
							} else {
								$_POST['dnproductosServicios'][] = '';
							}
						}
					}
				}
				$sqlrT = "SELECT cc, tercero, fecha FROM tesoordenpago WHERE id_orden='$_POST[cxp]' AND tipo_mov='201'";
				$resT = mysqli_query($linkbd_V7, $sqlrT);
				$rowT = mysqli_fetch_row($resT);
				$_POST['ccR'] = $rowT[0];
				$_POST['terceroR'] = $rowT[1];
				$_POST['fechaComp'] = $rowT[2];
				?>
				<table class="inicio">
					<tr>
						<td colspan="10" class="titulos">Detalle Orden de Pago - Reversi&oacute;n.</td>
					</tr>
					<?php
					if ($_POST['todos'] == 1) {
						$checkt = 'checked';
					} else {
						$checkt = '';
					}
					if ($_POST['inicios'] == 1) {
						$checkint = 'checked';
					} else {
						$checkint = '';
					}
					?>
					<tr>
						<td class='titulos2' style='text-align:center;'>Vig. Gasto</td>
						<td class='titulos2' style='text-align:center;'>Sec. presupuestal</td>
						<td class='titulos2' style='text-align:center;'>Medio pago</td>
						<td class='titulos2' style='text-align:center;'>Cuenta CCPET</td>
						<td class='titulos2' style='text-align:center;'>Fuente</td>
						<td class='titulos2' style='text-align:center;'>CPC</td>
						<td class='titulos2' style='text-align:center;'>Programativo MGA</td>
						<td class='titulos2' style='text-align:center;'>BPIM</td>
						<td class='titulos2' style='text-align:center;'>Valor</td>

					</tr>
					<?php
					$_POST['totalc'] = 0;
					$iter = 'saludo1a';
					$iter2 = 'saludo2';
					if (isset($_POST['dcuentas'])) {
						for ($x = 0; $x < count($_POST['dcuentas']); $x++) {

							echo "
							<input type='hidden' name='dcuentas[]' value='" . $_POST['dcuentas'][$x] . "'/>
							<input type='hidden' name='dncuentas[]' value='" . $_POST['dncuentas'][$x] . "'/>
							<input type='hidden' name='dcc' value='" . $_POST['dcc'] . "'>

							<input type='hidden' name='dvalores[]' value='" . $_POST['dvalores'][$x] . "'/>
							<input type='hidden' name='dfuentes[]' value='" . $_POST['dfuentes'][$x] . "'/>
							<input type='hidden' name='dnfuentes[]' value='" . $_POST['dnfuentes'][$x] . "'/>
							<input type='hidden' name='dproductosServicios[]' value='" . $_POST['dproductosServicios'][$x] . "'/>
							<input type='hidden' name='dnproductosServicios[]' value='" . $_POST['dnproductosServicios'][$x] . "'/>
							<input type='hidden' name='dindicadorProducto[]' value='" . $_POST['dindicadorProducto'][$x] . "'/>
							<input type='hidden' name='dmedioPago[]' value='" . $_POST['dmedioPago'][$x] . "'/>
							<input type='hidden' name='dvigenciaGasto[]' value='" . $_POST['dvigenciaGasto'][$x] . "'/>
							<input type='hidden' name='dbpim[]' value='" . $_POST['dbpim'][$x] . "'/>
							<input type='hidden' name='dseccionPresupuestal[]' value='" . $_POST['dseccionPresupuestal'][$x] . "'/>

							<tr  class='$iter' onMouseOver=\"anterior=this.style.backgroundColor;this.style.backgroundColor='#40b3ff';\"
		onMouseOut=\"this.style.backgroundColor=anterior\" style='text-transform:uppercase'>
								<td style='width:6%;'>" . $_POST['dvigenciaGasto'][$x] . "</td>
								<td style='width:6%;'>" . $_POST['dseccionPresupuestal'][$x] . "</td>
								<td style='width:8%;'>" . $_POST['dmedioPago'][$x] . "</td>
								<td>" . $_POST['dcuentas'][$x] . " - " . $_POST['dncuentas'][$x] . "</td>
								<td style='width:15%;'>" . $_POST['dfuentes'][$x] . " - " . $_POST['dnfuentes'][$x] . "</td>
								<td style='width:15%;'>" . $_POST['dproductosServicios'][$x] . " - " . $_POST['dnproductosServicios'][$x] . "</td>
								<td style='width:10%;'>" . $_POST['dindicadorProducto'][$x] . "</td>
								<td style='width:10%;'>" . $_POST['dbpim'][$x] . "</td>
								<td style='width:15%; text-align:right;'>" . number_format($_POST['dvalores'][$x], 2, ".", ",") . "</td>

							</tr>";
							$_POST['totalc'] += doubleVal($_POST['dvalores'][$x]);
							$aux = $iter;
							$iter = $iter2;
							$iter2 = $aux;
						}
					}
					if (isset($_POST['totalc'])) {
						$resultado = convertir($_POST['totalc']);
						$_POST['letras'] = $resultado . " PESOS M/CTE";
						$_POST['totalcf'] = number_format($_POST['totalc'], 2, ".", ",");
						echo "
						<input type='hidden' name='totalcf' value='$_POST[totalcf]'/>
						<input type='hidden' name='totalc' value='$_POST[totalc]'/>
						<input type='hidden' name='letras' value='$_POST[letras]'/>
						<input type='hidden' name='terceroR' value='$_POST[terceroR]'/>
						<input type='hidden' name='fechaComp' value='$_POST[fechaComp]'/>
						<tr class='$iter' style='text-align:right;'>
							<td colspan='8'>Total:</td>
							<td>$ $_POST[totalcf]</td>
						</tr>
						<tr class='titulos2'>
							<td>Son:</td>
							<td colspan='8'>$_POST[letras]</td>
						</tr>";
					}
					?>
					<script>
						document.form2.valor.value = <?php echo $_POST['totalc']; ?>;
						document.form2.valoregreso.value = <?php echo $_POST['totalc']; ?>;
						document.form2.valorcheque.value = document.form2.valor.value - document.form2.valorretencion.value;
					</script>
				</table>
			</div>
			<?php
		}

		//echo count($_POST[dcuentas]);
		if ($_POST['oculto'] == '2') {
			preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST['fecha'], $fecha);
			$fechaf = $fecha[3] . "-" . $fecha[2] . "-" . $fecha[1];
			$vigenciaReversion = $fecha[3];

			$bloq = bloqueos($_SESSION['cedulausu'], $fechaf);

			$query = "SELECT conta_pago FROM tesoparametros";
			$resultado = mysqli_query($linkbd_V7, $query);
			$arreglo = mysqli_fetch_row($resultado);
			$opcion = $arreglo[0];
			$vigencia = $_POST['vigencia'];

			if ($bloq >= 1) {
				if ($_POST['tipomovimiento'] == '201') {
					$sqlr = "select count(*) from tesoordenpago where id_orden=$_POST[idcomp] and tipo_mov='201' ";
					$res = mysqli_query($linkbd_V7, $sqlr);
					while ($r = mysqli_fetch_row($res)) {
						$numerorecaudos = $r[0];
					}
					if ($numerorecaudos == 0) {
						//***********ACTUALIZAR PAGOS ENTRADA SERVICIO

						//************CREACION DEL COMPROBANTE CONTABLE ************************
						//***busca el consecutivo del comprobante contable
						$consec = 0;
						$sqlr6 = "select max(id_orden) from  tesoordenpago ";
						$res1 = mysqli_query($linkbd_V7, $sqlr6);
						while ($r = mysqli_fetch_row($res1)) {
							$consec = $r[0];
						}

						$consec += 1;
						if ($_POST['selectipo'] == 'servicio') {
							$aumenta = 0;
							$pagos = "";
							for ($i = 0; $i < count($_POST['posicion']); $i++) {
								if (isset($_POST['poscheck'][$i])) {
									if (($_POST['poscheck'][$i] == '' || $_POST['poscheck'][$i] == '1') && $_POST['bloqueos'][$i] != '1') {
										$sql = "UPDATE inv_servicio_det SET estado='P' WHERE codcompro='$_POST[servicio]' AND numpago='" . $_POST['posicion'][$i] . "' ";
										mysqli_query($linkbd_V7, $sql);
										$aumenta++;
										$pagos .= ($_POST['posicion'][$i] . ",");
									}

								}
							}

							$pagos = substr($pagos, 0, -1);
							$sql = "UPDATE inv_servicio SET numpagosok=numpagosok+$aumenta WHERE codcompro='$_POST[servicio]' ";
							mysqli_query($linkbd_V7, $sql);

							$sql = "INSERT INTO inv_servicio_cxp(codcompro,cxp,pagos,total) VALUES ('$_POST[servicio]','$consec','$pagos','$_POST[sumarauto]')";
							mysqli_query($linkbd_V7, $sql);

							$sql = "SELECT numpagosauto,numpagosok FROM inv_servicio WHERE codcompro='$_POST[servicio]'";
							$res = mysqli_query($linkbd_V7, $sql);
							$fila = mysqli_fetch_row($res);
							if ($fila[0] == $fila[1]) {
								$sql = "UPDATE inv_servicio SET estado='P'  WHERE codcompro='$_POST[servicio]'";
								mysqli_query($linkbd_V7, $sql);
							}
						}
						//*********************en esta parte se realizan las contabilizaciones de las retenciones *****//
						$valorDescuentosDebito = 0;
						for ($j = 0; $j < count($_POST['dcuentas']); $j++) {
							if ($opcion == "1") {
								$cantDescuentos = count($_POST['dndescuentos']);
								for ($x = 0; $x < $cantDescuentos; $x++) {
									$dd = $_POST['ddescuentos'][$x];

									$sqlr = "select * from tesoretenciones, tesoretenciones_det where tesoretenciones_det.codigo=tesoretenciones.id and tesoretenciones.id='" . $dd . "' ORDER BY porcentaje DESC";

									$resdes = mysqli_query($linkbd_V7, $sqlr);
									$valordes = 0;
									$valorProcentaje = 0;
									$idRetencion = '';
									while ($rowdes = mysqli_fetch_assoc($resdes)) {
										$valordes = 0;
										if ($idRetencion == $rowdes['codigo']) {
											$valorProcentaje = $val2 + $rowdes['porcentaje'];
										} else {
											$valorProcentaje = $rowdes['porcentaje'];
											$idRetencion = $rowdes['codigo'];
										}

										$val2 = 0;
										$val2 = $rowdes['porcentaje'];

										if (doubleVal($rowdes['iva']) > 0 && doubleVal($rowdes['terceros']) == 1) {
											$val1 = $_POST['dvalores'][$j];
											$val3 = $_POST['ddesvalores'][$x];
											$valordes = round((doubleVal($val1) / doubleVal($_POST['valor'])) * (doubleVal($val2) / 100) * doubleVal($val3), 0);
											//$valordes = round(($val2/100)*$val3,0);
										} else {
											$val1 = $_POST['dvalores'][$j];
											$val3 = $_POST['ddesvalores'][$x];
											$valordes = round((doubleVal($val1) / doubleVal($_POST['valor'])) * (doubleVal($val2) / 100) * doubleVal($val3), 0);
											//$valordes = round(($val2/100)*$val3,0);
										}

										//$arreglocuenta[$_POST['dcuentas'][$j]."".$_POST['drecursos'][$j]."".$_POST['productoservicio'][$j]."".$_POST['indicador_producto'][$j]] += $valordes;

										//$valorDescuentosDebito += $valordes;
										$nomDescuento = $_POST['dndescuentos'][$x];
										//$cc = $_POST['cc'];
										$sec_presu = intval($_POST['dseccion_presupuestal'][$j]);
										$sqlrCc = "SELECT id_cc FROM centrocostos_seccionpresupuestal WHERE (id_sp = $sec_presu OR id_sp = '" . $_POST['dseccion_presupuestal'][$j] . "')";
										$resCc = mysqli_query($linkbd_V7, $sqlrCc);
										$rowCc = mysqli_fetch_row($resCc);
										$cc = $rowCc[0];
										//echo $cc."<br>";
										$codigoRetencion = 0;
										$rest = 0;
										$codigoCausa = 0;

										if ($_POST['medioDePago'] != '1') {
											//concepto contable //********************************************* */
											$rest = substr($rowdes['tipoconce'], 0, 2);
											$sq = "select fechainicial from conceptoscontables_det where codigo='" . $rowdes['conceptocausa'] . "' and modulo='" . $rowdes['modulo'] . "' and tipo='$rest' and fechainicial<'$fechaf' and cuenta!='' order by fechainicial asc";
											$re = mysqli_query($linkbd_V7, $sq);
											while ($ro = mysqli_fetch_assoc($re)) {
												$_POST['fechacausa'] = $ro["fechainicial"];
											}

											$sqlr = "select * from conceptoscontables_det where codigo='" . $rowdes['conceptocausa'] . "' and modulo='" . $rowdes['modulo'] . "' and cc='" . $cc . "' and tipo='$rest' and fechainicial='" . $_POST['fechacausa'] . "'";

											$rst = mysqli_query($linkbd_V7, $sqlr);
											$row1 = mysqli_fetch_assoc($rst);
											if ($row1['cuenta'] != '' && $valordes > 0) {
												//echo "Hola 5  ";
												$sqlr = "insert into comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle, cheque,valdebito, valcredito, estado, vigencia) values ('11 $consec','" . $row1['cuenta'] . "','" . $_POST['tercero'] . "' ,'" . $cc . "' , 'Descuento " . $nomDescuento . "',''," . $valordes . ",0,'1' ,'" . $vigencia . "')";
												mysqli_query($linkbd_V7, $sqlr);
											}

											$rest = substr($rowdes['tipoconce'], -2);
											$sq = "select fechainicial from conceptoscontables_det where codigo='" . $rowdes['conceptoingreso'] . "' and modulo='" . $rowdes['modulo'] . "' and tipo='$rest' and fechainicial<'$fechaf' and cuenta!='' order by fechainicial asc";
											$re = mysqli_query($linkbd_V7, $sq);
											while ($ro = mysqli_fetch_assoc($re)) {
												$_POST['fechacausa'] = $ro["fechainicial"];
											}

											$sqlr = "select * from conceptoscontables_det where codigo='" . $rowdes['conceptoingreso'] . "' and modulo='" . $rowdes['modulo'] . "' and cc='" . $cc . "' and tipo='$rest' and fechainicial='" . $_POST['fechacausa'] . "'";
											//echo $sqlr." hoal <br>";
											$rst = mysqli_query($linkbd_V7, $sqlr);
											$row1 = mysqli_fetch_assoc($rst);
											if ($row1['cuenta'] != '' && doubleVal($valordes) > 0) {
												//echo "Hola 5  ";
												$sqlr = "insert into comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle, cheque,valdebito, valcredito, estado, vigencia) values ('11 $consec','" . $row1['cuenta'] . "','" . $_POST['tercero'] . "' ,'" . $cc . "' , 'Descuento " . $nomDescuento . "','',0," . $valordes . ",'1' ,'" . $vigencia . "')";
												mysqli_query($linkbd_V7, $sqlr);

												if ($valorProcentaje <= 100) {
													$sqlr = "insert into comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque,valdebito, valcredito,estado,vigencia) values ('11 $consec','" . $_POST['cuentaCredito'][$j] . "','$_POST[tercero]','" . $cc . "','Causacion " . $_POST['dncuentas'][$j] . "','','" . $valordes . "','0','1', '$_POST[vigencia]')";
													mysqli_query($linkbd_V7, $sqlr);
												}
											}

											$rest = "SR";
											$sq = "select fechainicial from conceptoscontables_det where codigo='" . $rowdes['conceptosgr'] . "' and modulo='" . $rowdes['modulo'] . "' and tipo='$rest' and fechainicial<'$fechaf' and cuenta!='' order by fechainicial asc";
											$re = mysqli_query($linkbd_V7, $sq);
											while ($ro = mysqli_fetch_assoc($re)) {
												$_POST['fechacausa'] = $ro["fechainicial"];
											}

											$sqlr = "select * from conceptoscontables_det where codigo='" . $rowdes['conceptosgr'] . "' and modulo='" . $rowdes['modulo'] . "' and cc='" . $cc . "' and tipo='$rest' and fechainicial='" . $_POST['fechacausa'] . "'";
											//echo $sqlr." hoal <br>";
											$rst = mysqli_query($linkbd_V7, $sqlr);
											$row1 = mysqli_fetch_assoc($rst);
											if ($row1['cuenta'] != '' && doubleVal($valordes) > 0) {
												//echo "Hola 5  ";
												$sqlr = "insert into comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle, cheque,valdebito, valcredito, estado, vigencia) values ('11 $consec','" . $row1['cuenta'] . "','" . $_POST['tercero'] . "' ,'" . $cc . "' , 'Descuento " . $nomDescuento . "','',0," . $valordes . ",'1' ,'" . $vigencia . "')";
												mysqli_query($linkbd_V7, $sqlr);
											}

											continue;

										} else {

											$codigoIngreso = $rowdes['conceptoingreso'];
											if ($codigoIngreso != "-1") {
												$codigoRetencion = $rowdes['conceptoingreso'];
												$rest = substr($rowdes['tipoconce'], -2);
												$val2 = $rowdes['porcentaje'];
											}

										}

										$sq = "select fechainicial from conceptoscontables_det where codigo='$codigoRetencion' and modulo='" . $rowdes['modulo'] . "' and tipo='$rest' and fechainicial<'$fechaf' and cuenta!='' order by fechainicial asc";
										$re = mysqli_query($linkbd_V7, $sq);
										while ($ro = mysqli_fetch_assoc($re)) {
											$_POST['fechacausa'] = $ro["fechainicial"];
										}

										$sqlr = "select * from conceptoscontables_det where codigo='$codigoRetencion' and modulo='" . $rowdes['modulo'] . "' and cc='" . $cc . "' and tipo='$rest' and fechainicial='" . $_POST['fechacausa'] . "'";
										//echo $sqlr."<br>";
										$rst = mysqli_query($linkbd_V7, $sqlr);
										$row1 = mysqli_fetch_assoc($rst);

										if ($row1['cuenta'] != '' && doubleVal($valordes) > 0) {
											$sqlr = "insert into comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle, cheque,valdebito, valcredito, estado, vigencia) values ('11 $consec','" . $row1['cuenta'] . "','" . $_POST['tercero'] . "' ,'" . $cc . "' , 'Descuento " . $nomDescuento . "','',0," . $valordes . ",'1' ,'" . $vigencia . "')";
											//	echo $sqlr."<br>";
											mysqli_query($linkbd_V7, $sqlr);
											if ($valorProcentaje <= 100) {
												$sqlr = "insert into comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque,valdebito, valcredito,estado,vigencia) values ('11 $consec','" . $_POST['cuentaCredito'][$j] . "','$_POST[tercero]','" . $cc . "','Causacion " . $_POST['dncuentas'][$j] . "','','" . $valordes . "','0','1', '$_POST[vigencia]')";
												mysqli_query($linkbd_V7, $sqlr);
											}
										}

										$realizaCausacion = 0;
										if (doubleVal($rowdes['terceros']) != 1 || $rowdes['tipo'] == 'C') {
											$realizaCausacion = 1;
											if ($rowdes['destino'] != 'M' && $rowdes['tipo'] == 'C') {
												$realizaCausacion = 0;
											}
										}

										if ($rowdes['conceptocausa'] != '-1' && $_POST['medioDePago'] == '1' && doubleVal($realizaCausacion) == 1) {
											//concepto contable //********************************************* */
											$rest = substr($rowdes['tipoconce'], 0, 2);
											$sq = "select fechainicial from conceptoscontables_det where codigo='" . $rowdes['conceptocausa'] . "' and modulo='" . $rowdes['modulo'] . "' and tipo='$rest' and fechainicial<'$fechaf' and cuenta!='' order by fechainicial asc";
											$re = mysqli_query($linkbd_V7, $sq);
											while ($ro = mysqli_fetch_assoc($re)) {
												$_POST['fechacausa'] = $ro["fechainicial"];
											}
											$sqlr = "select * from conceptoscontables_det where codigo='" . $rowdes['conceptocausa'] . "' and modulo='" . $rowdes['modulo'] . "' and cc='" . $cc . "' and tipo='$rest' and fechainicial='" . $_POST['fechacausa'] . "'";
											//echo $sqlr." hoal <br>";
											$rst = mysqli_query($linkbd_V7, $sqlr);
											$row1 = mysqli_fetch_assoc($rst);
											if ($row1['cuenta'] != '' && doubleVal($valordes) > 0) {
												//echo "Hola 5  ";
												$sqlr = "insert into comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle, cheque,valdebito, valcredito, estado, vigencia) values ('11 $consec','" . $row1['cuenta'] . "','" . $_POST['tercero'] . "' ,'" . $cc . "' , 'Descuento " . $nomDescuento . "',''," . $valordes . ",0,'1' ,'" . $vigencia . "')";
												mysqli_query($linkbd_V7, $sqlr);
												if ($valorProcentaje <= 100) {
													$sqlr = "insert into comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle, cheque,valdebito, valcredito, estado, vigencia) values ('11 $consec','" . $row1['cuenta'] . "','" . $_POST['tercero'] . "' ,'" . $cc . "' , 'Descuento " . $nomDescuento . "','',0," . $valordes . ",'1' ,'" . $vigencia . "')";
													mysqli_query($linkbd_V7, $sqlr);
												}
											}
										}
									}
								}
							}
						}


						if ($_POST['causacion'] == '2') {
							$_POST['detallegreso'] = "ESTE DOCUMENTO NO REQUIERE CAUSACION CONTABLE - " . $_POST['detallegreso'];
						}

						//***cabecera comprobante CXP LIQUIDADA
						$sqlr = "insert into comprobante_cab (numerotipo,tipo_comp,fecha,concepto,total,total_debito,total_credito,diferencia, estado) values ($consec,11,'$fechaf','$_POST[detallegreso]',0,$_POST[totalc],$_POST[totalc],0,'1')";
						mysqli_query($linkbd_V7, $sqlr);

						$idcomp = mysqli_insert_id($linkbd_V7);
						$_POST['idcomp'] = $idcomp;

						echo "<input type='hidden' name='ncomp' value='$idcomp'>";


						$cantCuentas = count($_POST['dcuentas']);
						$valorDesc = 0;
						$valorDeb = 0;
						if ($_POST['ivaDescontable'] == '') {
							$_POST['ivaDescontable'] = 0;
						}
						$cuentaIvaDesc = '';
						if (doubleVal($_POST['ivaDescontable']) > 0) {
							$sqlr = "INSERT INTO tesoordenpagoivadescontable (id_orden, valor) VALUES ($consec, $_POST[cuentaIvaDescontable])";
							mysqli_query($linkbd_V7, $sqlr);

							$cuentaIvaDesc = $_POST['cuentaIvaDescontable'];

						}



						for ($x = 0; $x < $cantCuentas; $x++) {
							$valorDesc = 0;
							if ($_POST['contabiliza'][$x] == 'S') {
								$sec_presu = intval($_POST['dseccion_presupuestal'][$x]);
								$sqlrCc = "SELECT id_cc FROM centrocostos_seccionpresupuestal WHERE (id_sp = $sec_presu OR id_sp = '" . $_POST['dseccion_presupuestal'][$x] . "')";
								$resCc = mysqli_query($linkbd_V7, $sqlrCc);
								$rowCc = mysqli_fetch_row($resCc);
								$cc = $rowCc[0];
								if (doubleVal($_POST['dvalores'][$x]) > 0 && $_POST['cuentaCredito'][$x] != 'N/A') {


									if ($cuentaIvaDesc != '') {
										$valorDesc = round((doubleVal($_POST['dvalores'][$x]) / doubleVal($_POST['valor'])) * doubleVal($_POST['iva']), 2);
									}

									$valorCred = doubleVal($_POST['dvalores'][$x]) - doubleVal($valorDesc);

									$sqlr = "insert into comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque, valdebito,valcredito,estado,vigencia) values ('11 $consec','" . $_POST['cuentaCredito'][$x] . "','" . $_POST['tercero'] . "','" . $cc . "','Causacion " . $_POST['dncuentas'][$x] . "','',0,'" . $valorCred . "','1','" . $_POST['vigencia'] . "')";
									mysqli_query($linkbd_V7, $sqlr);

									$valorDeb = doubleVal($_POST['dvalores'][$x]) - doubleVal($valorDesc);
									$sqlr = "insert into comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque, valdebito,valcredito,estado,vigencia) values ('11 $consec','" . $_POST['cuentaDebito'][$x] . "','" . $_POST['tercero'] . "','" . $cc . "','Causacion " . $_POST['dncuentas'][$x] . "',''," . $valorDeb . ",0,'1','" . $_POST['vigencia'] . "')";
									mysqli_query($linkbd_V7, $sqlr);

									if (doubleVal($valorDesc) > 0) {

										$sqlr = "insert into comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque, valdebito,valcredito,estado,vigencia) values ('11 $consec','" . $_POST['cuentaCredito'][$x] . "','" . $_POST['tercero'] . "','" . $cc . "','Iva descontable " . $_POST['dncuentas'][$x] . "','',0,'" . $valorDesc . "','1','" . $_POST['vigencia'] . "')";
										mysqli_query($linkbd_V7, $sqlr);

										$sqlr = "insert into comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque, valdebito,valcredito,estado,vigencia) values ('11 $consec','" . $cuentaIvaDesc . "','" . $_POST['tercero'] . "','" . $cc . "','Iva descontable " . $_POST['dncuentas'][$x] . "',''," . $valorDesc . ",0,'1','" . $_POST['vigencia'] . "')";
										mysqli_query($linkbd_V7, $sqlr);
									}
								}
							}
						}

						//for ($y=0;$y<count($_POST[rubros]);$y++)//**************PPTO AFECTAR LAS CUENTAS PPTALES CAMPO CxP
						//{
						for ($x = 0; $x < count($_POST['dcuentas']); $x++) {
							//if($_POST[dcuentas][$x]==$_POST[rubros][$y])
							//{
							$sqlr2 = "update ccpetrp_detalle set saldo=saldo-" . $_POST['dvalores'][$x] . " where cuenta='" . $_POST['dcuentas'][$x] . "' and consvigencia=" . $_POST['rp'] . " and vigencia=" . $_POST['vigencia'] . " and tipo_mov='201'";
							$res2 = mysqli_query($linkbd_V7, $sqlr2);
							//}
						}
						//}


						//ACTUALIZACION  DEL REGISTRO

						$sqlr = "update ccpetrp set saldo=saldo-" . $_POST['valoregreso'] . " where consvigencia=" . $_POST['rp'] . " and vigencia=" . $_POST['vigencia'] . " and tipo_mov='201' ";
						$res = mysqli_query($linkbd_V7, $sqlr);

						$sqlr = "select saldo from ccpetrp where consvigencia=" . $_POST['rp'] . " and vigencia=" . $_POST['vigencia'] . " and tipo_mov='201'";
						// echo $sqlr."<br>";
						$res = mysqli_query($linkbd_V7, $sqlr);
						$row = mysqli_fetch_row($res);
						// echo $row[0];
						if ($row[0] == 0) {
							$sqlr = "update ccpetrp set estado='S' where consvigencia=" . $_POST['rp'] . " and vigencia=" . $_POST['vigencia'] . " and tipo_mov='201' ";
							$res = mysqli_query($linkbd_V7, $sqlr);
						}
						//*******INCIO DE TABLAS DE TESORERIA **************
						//**** ENCABEZADO ORDEN DE PAGO

						$sqlr = "insert into tesoordenpago (id_comp,fecha,vigencia,id_rp,cc,tercero,conceptorden,valorrp,saldo,valorpagar, cuentapagar,valorretenciones,estado,base,iva,anticipo,user,tipo_mov,medio_pago) values($idcomp,'$fechaf'," . $_POST['vigencia'] . ",$_POST[rp],'','$_POST[tercero]','$_POST[detallegreso]',$_POST[valorrp],$_POST[saldorp],$_POST[totalc],'$_POST[cuentapagar]',$_POST[totaldes],'S','$_POST[base]','$_POST[iva]','$_POST[anticipo]','" . $_SESSION['nickusu'] . "','201','$_POST[medioDePago]')";
						mysqli_query($linkbd_V7, $sqlr);
						$idorden = mysqli_insert_id($linkbd_V7);
						//****** DETALLE ORDEN DE PAGO
						//for ($y=0;$y<count($_POST[rubros]);$y++)
						//{
						for ($x = 0; $x < count($_POST['dcuentas']); $x++) {
							//if($_POST[dcuentas][$x]==$_POST[rubros][$y])
							//{

							$sqlrCuetas = "INSERT INTO tesoordenpago_cuenta (id_orden, cuenta_deb, cuenta_cred, rubro, fuente, productoservicio, indicador_producto, tipo_mov, usuario, estado, bpim, seccion_presupuestal, tipo_gasto, codigo_vigenciag, medio_pago) VALUES ($idorden, '" . $_POST['cuentaDebito'][$x] . "', '" . $_POST['cuentaCredito'][$x] . "', '" . $_POST['dcuentas'][$x] . "', '" . $_POST['drecursos'][$x] . "', '" . $_POST['productoServicio'][$x] . "', '" . $_POST['indicador_producto'][$x] . "', '$_POST[tipomovimiento]', '" . $_SESSION['nickusu'] . "', 'S', '" . $_POST['bpim'][$x] . "', '" . $_POST['dseccion_presupuestal'][$x] . "', '" . $_POST['dtipo_gasto'][$x] . "', '" . $_POST['dvigenciaGasto'][$x] . "', '" . $_POST['medio_pago'][$x] . "')";
							mysqli_query($linkbd_V7, $sqlrCuetas);

							$sec_presu = intval($_POST['dseccion_presupuestal'][$x]);
							$sqlrCc = "SELECT id_cc FROM centrocostos_seccionpresupuestal WHERE (id_sp = $sec_presu OR id_sp = '" . $_POST['dseccion_presupuestal'][$x] . "')";
							$resCc = mysqli_query($linkbd_V7, $sqlrCc);
							$rowCc = mysqli_fetch_row($resCc);
							$cc = $rowCc[0];

							$sqlr = "insert into tesoordenpago_det (id_orden,cuentap,cc,valor,estado,tipo_mov,vigencia,fuente,productoservicio, indicador_producto, medio_pago, codigo_vigenciag, bpim,seccion_presupuestal, tipo_gasto, divipola, chip, area, detalle_sectorial) values ($idorden,'" . $_POST['dcuentas'][$x] . "','" . $cc . "'," . $_POST['dvalores'][$x] . ",'S','201'," . $_POST['vigencia'] . ",'" . $_POST['drecursos'][$x] . "','" . $_POST['productoServicio'][$x] . "', '" . $_POST['indicador_producto'][$x] . "', '" . $_POST['medio_pago'][$x] . "', '" . $_POST['dvigenciaGasto'][$x] . "', '" . $_POST['bpim'][$x] . "', '" . $_POST['dseccion_presupuestal'][$x] . "', '" . $_POST['dtipo_gasto'][$x] . "', '" . $_POST['ddivipola'][$x] . "', '" . $_POST['dchip'][$x] . "', '" . $_POST['darea'][$x] . "', '" . $_POST['ddetallesectorial'][$x] . "')";
							if (!mysqli_query($linkbd_V7, $sqlr)) {
								echo "
													<table class='inicio'>
														<tr>
														<td class='saludo1'>
															<center>
																<font color=blue>
																	<img src='imagenes\alert.png'>Manejador de Errores de la Clase BD<br>
																	<font size=1>
																	</font>
																</font>
																<br>
																<p align=center>No se pudo ejecutar la petición: <br>
																	<font color=red>
																		<b>$sqlr</b>
																	</font>
																</p>";
								echo "Ocurrió el siguiente problema:<br>";
								echo "<pre>";
								echo "</pre></center></td></tr></table>";
							} else {
								echo "<script>despliegamodalm('visible','1','Se ha almacenado la Orden de Pago con Exito');</script>";

								echo "
												<script>
													document.form2.numero.value='';
													document.form2.valor.value=0;
													document.form2.oculto.value=1;
												</script>";
							}
							//}
						}
						//}
						//****CUENTA POR PAGAR - DESTINO COMPRA
						/* $sqlr="insert into tesoordenpago_almacen (id_orden,destino,vigencia) values ($idorden,'".$_POST['destino']."',$vigusu)";
											 mysqli_query($linkbd_V7, $sqlr);
											 //****** ORDEN PAGO BASES RETENCION ****
											 for($x=0;$x<count($_POST['pcxp']);$x++)
											 {
												 $ch = esta_en_array($_POST['pagos'],$_POST['pcxp'][$x]);
												 if($ch=='1')
												 {
													 $sqlr="insert into tesoordenpago_bases (id_orden,id_ordenbase,valor,vigencia) values ($idorden,".$_POST['pcxp'][$x].",".$_POST['pvalores'][$x].",'".$vigusu."')";
													 mysqli_query($linkbd_V7, $sqlr);
												 }
											 } */

						$causacion = "";
						$estado = "";
						if ($opcion == "1") {
							$causacion = "orden";
							$estado = "S";
						} else if ($opcion == "2") {
							$causacion = "egreso";
							$estado = "N";
						}

						//****** RETENCIONES ORDEN DE PAGO
						for ($x = 0; $x < count($_POST['ddescuentos']); $x++) {
							$sqlr = "insert into tesoordenpago_retenciones_manual_automatico (id_orden,id_retencion,automatico) values ($idorden,'" . $_POST['ddescuentos'][$x] . "','" . $_POST['dmanual'][$x] . "')";
							mysqli_query($linkbd_V7, $sqlr);

							$sqlr = "insert into tesoordenpago_retenciones (id_retencion,id_orden,porcentaje,valor,estado,causacion,pasa_ppto) values ('" . $_POST['ddescuentos'][$x] . "',$idorden,'" . $_POST['dporcentajes'][$x] . "'," . $_POST['ddesvalores'][$x] . ",'S','$causacion','$estado')";


							if (!mysqli_query($linkbd_V7, $sqlr)) {
								echo "<table class='inicio'><tr><td class='saludo1'><center><font color=blue><img src='imagenes\alert.png'>Manejador de Errores de la Clase BD<br><font size=1></font></font><br><p align=center>No se pudo ejecutar la petición: <br><font color=red><b>$sqlr</b></font></p>";
								echo "Ocurrió el siguiente problema:<br>";
								echo "<pre>";
								echo "</pre></center></td></tr></table>";
							} else {

								echo "
										<script>
											document.form2.numero.value='';
											document.form2.valor.value=0;
											document.form2.oculto.value=1;
											document.form2.fecha.focus();
											document.form2.fecha.select();
										</script>";
							}


						}
					}
				} else {
					$sqlrCab = "INSERT INTO comprobante_cab(numerotipo, tipo_comp, fecha, concepto, total_debito, total_credito, diferencia, estado) VALUES('$_POST[cxp]', 2011, '$fechaf', '$_POST[descripcion]', '$_POST[valorcxp]', '$_POST[valorcxp]', 0, 1)";
					mysqli_query($linkbd_V7, $sqlrCab);

					cargartrazas('conta', 'comprobante_cab', 'insert', $_POST['cxp'], '2011', $_POST['cxp'], '', $_POST['descripcion'], $sqlrCab);

					$sqlrDetSel = "SELECT * FROM comprobante_det WHERE numerotipo='$_POST[cxp]' AND tipo_comp='11'";
					$resDetSel = mysqli_query($linkbd_V7, $sqlrDetSel);

					cargartrazas('conta', 'comprobante_det', 'select', $_POST['cxp'], '11', $_POST['cxp'], '', '', $sqlrDetSel);

					while ($rowDetSel = mysqli_fetch_row($resDetSel)) {

						$debito = 0;
						$credito = 0;

						if (doubleVal($rowDetSel[7]) > 0) {
							$debito = 0;
							$credito = $rowDetSel[7];
						} else {
							$debito = $rowDetSel[8];
							$credito = 0;
						}

						$id_comp = "2011 " . $rowDetSel[12];

						$sqlrDet = "INSERT INTO comprobante_det(id_comp,cuenta,tercero,centrocosto,detalle,cheque, valdebito,valcredito,estado,vigencia,tipo_comp,numerotipo) VALUES('$id_comp', '$rowDetSel[2]', '$rowDetSel[3]', '$rowDetSel[4]', '$rowDetSel[5]', '$rowDetSel[6]', $debito, $credito, '$rowDetSel[9]', '$rowDetSel[10]', '2011', '$rowDetSel[12]')";

						mysqli_query($linkbd_V7, $sqlrDet);

						cargartrazas('conta', 'comprobante_det', 'insert', "$id_comp", '2011', $rowDetSel[12], '', '', $sqlrDet);

					}

					$sqlr = "update tesoordenpago set estado='R' where id_orden='$_POST[cxp]' and tipo_mov='201' ";
					mysqli_query($linkbd_V7, $sqlr);

					cargartrazas('teso', 'tesoordenpago', 'update', "$_POST[cxp]", '', $_POST['cxp'], '', '', $sqlr);

					$sqlr = "update tesoordenpago_det set estado='R' where id_orden='$_POST[cxp]' and tipo_mov='201' ";
					mysqli_query($linkbd_V7, $sqlr);

					cargartrazas('teso', 'tesoordenpago_det', 'update', "$_POST[cxp]", '', $_POST['cxp'], '', '', $sqlr);

					$sqlrRet = "DELETE FROM tesoordenpago_retenciones WHERE id_irden='$_POST[cxp]'";
					mysqli_query($linkbd_V7, $sqlrRet);

					cargartrazas('teso', 'tesoordenpago_retenciones', 'delete', "$_POST[cxp]", '', $_POST['cxp'], '', '', $sqlrRet);


					$sqlr = "insert into tesoordenpago(id_orden,fecha,vigencia,id_rp,cc,tercero,conceptorden,user,tipo_mov,estado) values ('$_POST[cxp]','$fechaf','$vigenciaReversion','$_POST[rp]','','$_POST[terceroR]','$_POST[descripcion]','" . $_SESSION['nickusu'] . "','401','R')";
					mysqli_query($linkbd_V7, $sqlr);

					cargartrazas('teso', 'tesoordenpago', 'insert', "$_POST[cxp]", '', $_POST['cxp'], '', $_POST['descripcion'], $sqlr);

					/* for($x=0;$x<count($_POST['dcuentas']);$x++)
									  { */
					/* $sqlr="insert into tesoordenpago_det(id_orden,cuentap,valor,vigencia,fuente,tipo_mov,estado,productoservicio, indicador_producto, bpim) values('$_POST[cxp]','".$_POST['dcuentas'][$x]."',0,'$_POST[vigencia]','".$_POST['drecursos'][$x]."','401','R','".$_POST['productoServicio'][$x]."', '".$_POST['indicador_producto'][$x]."', '$_POST[bpim]')";
										 mysqli_query($linkbd_V7, $sqlr); */

					/* $sqlrCuetas = "INSERT INTO tesoordenpago_cuenta (id_orden, cuenta_deb, cuenta_cred, rubro, fuente, productoservicio, indicador_producto, tipo_mov, usuario, estado) VALUES ('$_POST[cxp]', '".$_POST['cuentaDebito'][$x]."', '".$_POST['cuentaCredito'][$x]."', '".$_POST['dcuentas'][$x]."', '".$_POST['drecursos'][$x]."', '".$_POST['productoServicio'][$x]."', '".$_POST['indicador_producto'][$x]."', '$_POST[tipomovimiento]', '".$_SESSION['nickusu']."', 'S')";
										 mysqli_query($linkbd_V7, $sqlrCuetas); */
					/* } */

					echo "<table class='inicio'><tr><td class='saludo1'><center>Se ha reversado la Orden de pago con Exito <img src='imagenes\confirm.png'></center></td></tr></table>";
					echo "<script>funcionmensaje();</script>";
				}
			} else {
				echo "<div class='inicio'><img src='imagenes\alert.png'> No Tiene los Permisos para Modificar este Documento</div>";
			}
			//****fin if bloqueo
		}//************ FIN DE IF OCULTO************
		?>
		<div id="bgventanamodal2">
			<div id="ventanamodal2">
				<IFRAME src="" name="buster" marginWidth=0 marginHeight=0 frameBorder=0 id="ventana2" frameSpacing=0
					style="left:500px; width:900px; height:500px; top:200;">
				</IFRAME>
			</div>
		</div>
	</form>
</body>

</html>
