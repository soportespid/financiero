<?php
	require_once("tcpdf/tcpdf_include.php");
	require('comun.inc');
	require('funciones.inc');
	session_start();
	date_default_timezone_set("America/Bogota");
	$val = 0;
	class MYPDF extends TCPDF
	{
		public function Header()
		{
            if ($_POST['estado']=='REVERSADO'){
                $this->Image('imagenes/reversado02.png',200,10,50,15);
            }
			$linkbd=conectar_v7();
			$linkbd -> set_charset("utf8");
			$sqlr="SELECT nit, razonsocial FROM configbasica WHERE estado='S'";
			$resp=mysqli_query($linkbd,$sqlr);
			while($row=mysqli_fetch_row($resp)){$nit=$row[0];$rs=strtoupper($row[1]);}
			$this->Image('imagenes/escudo.jpg',  13, 13, 20, 20, 'JPG', '', 'T', true, 300, '', false, false, 0, false, false, false);// Logo
			$this->SetFont('helvetica','B',8);
			$this->SetY(10);
			$this->RoundedRect(10, 10, 277, 25, 1,''); //Borde del encabezado
			$this->Cell(26,25,'','R',0,'L');  //Linea que separa el encabazado verticalmente
			$this->SetY(8);
			$this->SetX(80);
			$this->SetFont('helvetica','B',9);
			$this->Cell(160,15,"$rs",0,0,'C');
			$this->SetY(12);
			$this->SetX(80);
			$this->SetFont('helvetica','B',7);
			$this->Cell(160,15,'NIT: '.$nit,0,0,'C');
			$this->SetY(23);
			$this->SetX(36);
			$this->SetFont('helvetica','B',9);
			$this->Cell(251,12,"COMPROBANTE EGRESO",'T',0,'C');
			$this->SetFont('helvetica','I',10);
			$this->SetY(27);
			$this->SetX(62);
			$mov='';
			if(isset($_POST['movimiento']))
			{
				if($_POST['movimiento']=='401' || $_POST['movimiento']=='402'){$mov="DOCUMENTO DE REVERSION";}
			}

			$this->SetFont('helvetica','B',6);
			$this->SetY(10);
			$this->SetX(257);
			$this->Cell(30,5," NUMERO: ".$_POST['egreso'],"L",0,'L');
			$this->SetY(13.5);
			$this->SetX(257);
			$this->Cell(35,6," FECHA: ".$_POST['fecha'],"L",0,'L');
			$this->SetY(18);
			$this->SetX(257);
			$this->Cell(35,5," VIGENCIA: ".$_POST['vigencia'],"L",0,'L');

			$this->SetFont('helvetica','B',8);
			$this->SetY(25);

			$this->cell(248,8,'NETO A PAGAR: ',0,0,'R');
			$this->RoundedRect(257.5, 26 ,27, 6, 1,'');
			$this->cell(26,8,'$'.number_format($_POST['valorpagar'],2),0,0,'R');
			}
		public function Footer()
		{
			$linkbd=conectar_v7();
			$linkbd -> set_charset("utf8");
			$sqlr="SELECT direccion,telefono,web,email FROM configbasica WHERE estado='S'";
			$resp=mysqli_query($linkbd,$sqlr);
			while($row=mysqli_fetch_row($resp))
			{
				$direcc=iconv($_SESSION["VERCARPDFINI"], $_SESSION["VERCARPDFFIN"]."//TRANSLIT",strtoupper($row[0]));
				$telefonos=$row[1];
				$dirweb=iconv($_SESSION["VERCARPDFINI"], $_SESSION["VERCARPDFFIN"]."//TRANSLIT",strtoupper($row[3]));
				$coemail=iconv($_SESSION["VERCARPDFINI"], $_SESSION["VERCARPDFFIN"]."//TRANSLIT",strtoupper($row[2]));
			}
			if($direcc!=''){$vardirec="Dirección: $direcc, ";}
			else {$vardirec="";}
			if($telefonos!=''){$vartelef="Telefonos: $telefonos";}
			else{$vartelef="";}
			if($dirweb!=''){$varemail="Email: $dirweb, ";}
			else {$varemail="";}
			if($coemail!=''){$varpagiw="Pagina Web: $coemail";}
			else{$varpagiw="";}
			$resp=mysqli_query($linkbd, $sqlr);
			$user = $_SESSION['nickusu'];
			$fecha = date("Y-m-d H:i:s");
			$ip = $_SERVER['REMOTE_ADDR'];
			$useri = $_POST['user'];
			//$this->SetY(-16);
			$this->SetFont('helvetica', 'I', 6);
			$txt = <<<EOD
			$vardirec $vartelef
			$varemail $varpagiw
			EOD;
			$this->Cell(277,8,'','T',0,'T');
			$this->ln(2);
			$this->Write(0, $txt , '', 0, 'C', true, 0, false, false, 0);
			$this->Cell(25, 8, 'Hecho por: '.$useri, 0, false, 'C', 0, '', 0, false, 'T', 'M');
			$this->Cell(100, 8, 'Impreso por: '.$user, 0, false, 'C', 0, '', 0, false, 'T', 'M');
			$this->Cell(30, 8, 'IP: '.$ip, 0, false, 'C', 0, '', 0, false, 'T', 'M');
			$this->Cell(102, 8, 'Fecha: '.$fecha, 0, false, 'C', 0, '', 0, false, 'T', 'M');
			$this->Cell(5, 8, 'IDEAL.10 S.A.S    Pagina '.$this->getAliasNumPage().'/'.$this->getAliasNbPages(), 0, false, 'C', 0, '', 0, false, 'T', 'M');



		}
	}
	$pdf = new MYPDF('L','mm','Letter', true, 'iso-8859-1', false);// create new PDF document
	$pdf->SetDocInfoUnicode (true);
	// set document information
	$pdf->SetCreator(PDF_CREATOR);
	$pdf->SetAuthor('G&CSAS');
	$pdf->SetTitle('Certificados');
	$pdf->SetSubject('Certificado de Disponibilidad');
	$pdf->SetKeywords('TCPDF, PDF, example, test, guide');
	$pdf->SetMargins(10, 38, 10);// set margins
	$pdf->SetHeaderMargin(38);// set margins
	$pdf->SetFooterMargin(17);// set margins
	$pdf->SetAutoPageBreak(TRUE, 20);// set auto page breaks
	// set some language-dependent strings (optional)
	if (@file_exists(dirname(__FILE__).'/lang/spa.php'))
	{
		require_once(dirname(__FILE__).'/lang/spa.php');
		$pdf->setLanguageArray($l);
	}
	// ---------------------------------------------------------
	$pdf->AddPage();


	$pdf->SetFillColor(245,245,245);
	$pdf->cell(0.2);
	$pdf->SetFont('helvetica','B',7);
	$pdf->cell(23,4,'Beneficiario: ','LT',0,'L',1);
	$pdf->SetFont('helvetica','',7);
	$ntercero = $_POST['ntercero'];
	$pdf->cell(90,4,''.substr(ucwords($ntercero),0,100),'T',0,'L',1);

	$pdf->SetFont('helvetica','B',7);
	$pdf->cell(23,4,'C.C o NIT: ','T',0,'L',1);
	$pdf->SetFont('helvetica','',7);
	$pdf->cell(40,4,''.$_POST['tercero'],'T',0,'L',1);
	$pdf->SetFont('helvetica','B',7);
	$pdf->cell(23,4,'Cta Beneficiario:','T',0,'L',1);
	$pdf->SetFont('helvetica','',7);
	if(mb_strlen ($_POST['tercerocta']) < 50){
		$pdf->SetFont('helvetica','',7);
	}else{
		$pdf->SetFont('helvetica','',5);
	}
	$pdf->cell(78,4,$_POST['tercerocta'],'TR',1,'L',1);
	$pdf->SetFont('helvetica','B',7);



	$pdf->cell(0.2);

	$concepto = ucfirst(strtolower($_POST['concepto']));
	$lineass = $pdf->getNumLines($concepto, 254);
	$alturadt=(4*$lineass);
	$pdf->MultiCell(23,$alturadt,'Concepto: ', 'LB', 'J', 0, 0, '', '', true, 0, false, true, $alturadt, 'T');
	$pdf->SetFont('helvetica','',8);
	$pdf->MultiCell(254,$alturadt,"$concepto",'RB','L',false,1,'','',true,0,false,true,$alturadt,'T',false);

	//	$pdf->Cell(199,$altura,"CONCEPTO:  ".iconv($_SESSION["VERCARPDFINI"], $_SESSION["VERCARPDFFIN"]."//TRANSLIT",strtoupper($_POST['concepto'])),'LR','L',true,1,'','',true,0,false,true,$altura,'M',false);


	$pdf->SetFillColor(245,245,245);
	$pdf->SetFont('helvetica','B',7);
	$pdf->cell(0.2);
	$pdf->cell(23,4,'No CxP::','L',0,'L',1);
	$pdf->SetFont('helvetica','',7);
	$pdf->cell(90,4,''.$_POST['orden'],0,0,'L',1);
	$pdf->SetFont('helvetica','B',7);
	$pdf->cell(23,4,'Forma Pago:',0,0,'L',1);
	$pdf->SetFont('helvetica','',7);
	$pdf->cell(141,4,''.strtoupper($_POST['tipop']),'R',1,'L',1);
	$pdf->SetFillColor(255,255,255);
	$pdf->SetFont('helvetica','B',7);
	$pdf->cell(0.2);
	$pdf->cell(23,4,'Banco: ','L',0,'L',1);
	$pdf->SetFont('helvetica','',7);
	$pdf->cell(90,4,''.substr(strtoupper($_POST['nbanco']),0,80),0,0,'L',1);
	$pdf->SetFont('helvetica','B',7);
	$pdf->cell(23,4,'N Cta.:',0,0,'L',1);
	$pdf->SetFont('helvetica','',7);
	$pdf->cell(141,4,''.$_POST['tcta'].' '.$_POST['cb'],'R',1,'L',1);
	$pdf->SetFillColor(245,245,245);
	$pdf->SetFont('helvetica','B',7);
	$pdf->cell(0.2);
	$pdf->cell(23,4,'Cheque/Tranf.:','L',0,'L',1);
	$pdf->SetFont('helvetica','',7);
	$pdf->cell(254,4,''.$_POST['ntransfe'].$_POST['ncheque'],'R',1,'L',1);
	$pdf->SetFillColor(255,255,255);
	$pdf->SetFont('helvetica','B',7);
	$pdf->cell(0.2);
	$pdf->cell(23,4,'Valor Pago:','L',0,'L',1);
	$pdf->SetFont('helvetica','',7);
	$pdf->cell(90,4,'$'.number_format($_POST['valororden'],2),0,0,'L',1);
	$pdf->SetFont('helvetica','B',7);
	$pdf->cell(23,4,'Retenciones: ',0,0,'L',1);
	$pdf->SetFont('helvetica','',7);
	$pdf->cell(48,4,'$'.number_format($_POST['retenciones'],2),0,0,'L',1);
	$pdf->SetFont('helvetica','B',7);
	$pdf->cell(16,4,'Iva: ',0,0,'L',1);
	$pdf->SetFont('helvetica','',7);
	$pdf->cell(77,4,'$'.number_format($_POST['iva'],2),'R',1,'L',1);
	$pdf->SetFillColor(245,245,245);
	$pdf->SetFont('helvetica','B',7);
	$pdf->cell(0.2);
	$pdf->cell(23,4,'Neto a Pagar:','L',0,'L',1);
	$pdf->SetFont('helvetica','',7);
	$pdf->cell(254,4,'$'.number_format($_POST['valorpagar'],2),'R',1,'L',1);
	$pdf->SetFillColor(255,255,255);
	$pdf->SetFont('helvetica','B',7);
	$pdf->cell(0.2);
	$pdf->cell(23,4,'Son: ','LB',0,'L',1);
	$pdf->SetFont('helvetica','',7);
	$pdf->cell(254,4,''.strtoupper($_POST['letras']),'BR',0,'L',1);


	$pdf->ln(6);


	$y=$pdf->GetY();
	$pdf->SetY($y);
	/* $pdf->SetFillColor(222,222,222);
	$pdf->SetFont('helvetica','B',7);
	$pdf->Cell(0.1);
	$pdf->Cell(277,4,'RETENCIONES',0,0,'C',1);
	$pdf->ln(5); */
	$y=$pdf->GetY();
	$pdf->SetFillColor(222,222,222);
	$pdf->SetFont('helvetica','B',7);
	$pdf->SetY($y);
	$pdf->Cell(0.1);
	$pdf->Cell(45,4,'Codigo ',0,0,'C',1);
	$pdf->SetY($y);
	$pdf->Cell(46);
	$pdf->Cell(140,4,'Retencion',0,0,'C',1);
	$pdf->SetY($y);
	$pdf->Cell(187);
	$pdf->Cell(45,4,'Porcentaje',0,0,'C',1);
	$pdf->SetY($y);
	$pdf->Cell(233);
	$pdf->Cell(44,4,'Valor',0,0,'C',1);
	$pdf->SetFont('helvetica','',7);
	$cont=0;
	$pdf->ln(5);
	for($x=0;$x<count($_POST['ddescuentos']);$x++)
	{
		if ($con%2==0){$pdf->SetFillColor(255,255,255);}
		else {$pdf->SetFillColor(245,245,245);}
		$pdf->Cell(46,4,''.$_POST['ddescuentos'][$x],'',0,'C',1);
		$pdf->Cell(141,4,''.$_POST['dndescuentos'][$x],'',0,'L',1);
		$pdf->Cell(46,4,''.$_POST['dporcentajes'][$x].'%','',0,'R',1);
		$pdf->Cell(44,4,'$'.number_format($_POST['ddesvalores'][$x],2),'',1,'R',1);
		$con=$con+1;
	}
	if(count($_POST['dcuenta'])>0)
	{
		$pdf->ln(2);
		$pdf->SetFillColor(222,222,222);
		$pdf->SetFont('helvetica','B',7);
		$pdf->Cell(0.1);
		$pdf->Cell(277,4,'AFECTACIÓN PRESUPUESTAL',0,0,'C',1);
		$pdf->ln(5);

		/* $pdf->Cell(60,4,'Cuenta ',0,0,'C',true,'',0,false,'T','C');
		$pdf->Cell(1);
		$pdf->Cell(160,4,'Descripción',0,0,'C',true,'',1,false,'T','C');
		$pdf->Cell(1);
		$pdf->Cell(55,4,'Valor ',0,1,'C',true,'',1,false,'T','C');
		$pdf->ln(1);*/
		$pdf->SetFont('helvetica','',7);
		$con=0;
		$ltrb = '';
		for($i=0;$i<count($_POST['dcuenta']);$i++)
		{
			$gdcuenta[$_POST['dcuenta'][$i]][0]=$_POST['dcuenta'][$i];
			$gdcuenta[$_POST['dcuenta'][$i]][1]=$_POST['ncuenta'][$i];
			$gdcuenta[$_POST['dcuenta'][$i]][2]=$gdcuenta[$_POST['dcuenta'][$i]][2]+str_replace(',','',$_POST['rvalor'][$i]);
		}
		foreach($gdcuenta as $val)
		{
			if ($con%2==0){$pdf->SetFillColor(255,255,255);}
			else{$pdf->SetFillColor(245,245,245);}
			$niy=$pdf->Gety();
			if($niy >= 269.5) $ltrb = 'B';
			if($niy >= 275.5) $ltrb = 'T';
			$pdf->Cell(60,4,''.$val[0],''.$ltrb,0,'C',true,'',1,false,'T','C');
			$pdf->Cell(1);
			$pdf->Cell(160,4,''.$val[1],''.$ltrb,0,'L',true,'',1,false,'T','C');
			$pdf->Cell(1);
			$pdf->Cell(55,4,'$'.number_format($val[2],2,".",","),''.$ltrb,1,'R',true,'',1,false,'T','C');
			$ltrb = '';
			$con=$con+1;
		}
		/* $pdf->SetFillColor(245,245,245);
		$pdf->ln(1);
		$pdf->Cell(199);
		$pdf->SetFont('helvetica','B',7);
		$pdf->Cell(22,4,'Total: ','',0,'C',true,'',1,false,'T','C');
		$pdf->SetFont('helvetica','',7);
		$pdf->Cell(1);
		$pdf->Cell(55,4,'$'.$_POST['varto'],'',1,'R',true,'',0,false,'T','C'); */
	}
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
	$sqlr="select id_cargo,id_comprobante from pptofirmas where id_comprobante='11' and vigencia='".$_POST['vigencia']."'";
	$res=mysqli_query($linkbd,$sqlr);
	while($row=mysqli_fetch_assoc($res))
	{
		if($row["id_cargo"]=='0')
		{
			$_POST['ppto'][]=buscatercero($_POST['tercero']);
			$_POST['nomcargo'][]='BENEFICIARIO';
		}
		else
		{
			$sqlr1="select cedulanit,(select nombrecargo from planaccargos where codcargo='".$row["id_cargo"]."') from planestructura_terceros where codcargo='".$row["id_cargo"]."' and estado='S'";
			$res1=mysqli_query($linkbd,$sqlr1);
			$row1=mysqli_fetch_row($res1);
			$_POST['ppto'][]=buscar_empleado($row1[0]);
			$_POST['nomcargo'][]=$row1[1];
		}
	}

	$pdf->ln(14);
	$tamFirmas = count($_POST['ppto']);
	for($x=0;$x<$tamFirmas;$x++)
	{
		$pdf->setFont('times','B',7);
		if (($x%2)==0) {
			$v = $pdf->gety();
			if($v>=180)
			{
				$pdf->AddPage();
				$pdf->ln(14);
				$v=$pdf->gety();
			}
			if(isset($_POST['ppto'][$x+1]))
			{
				$pdf->Line(40,$v,130,$v);
				$pdf->Line(170,$v,260,$v);
				$v2=$pdf->gety();
				$pdf->Cell(150,3,''.$_POST['ppto'][$x],0,1,'C',false,0,0,false,'T','C');
				$pdf->Cell(150,3,''.$_POST['nomcargo'][$x],0,1,'C',false,0,0,false,'T','C');
				$pdf->SetY($v2);
				$pdf->Cell(410,3,''.$_POST['ppto'][$x+1],0,1,'C',false,0,0,false,'T','C');
				$pdf->Cell(410,3,''.$_POST['nomcargo'][$x+1],0,1,'C',false,0,0,false,'T','C');
				if($tamFirmas > 2)
				{
					$pdf->ln(10);
				}

			}
			else
			{
				$v = $v + 5;
				$pdf->ln(5);

				$pdf->Line(105,$v,195,$v);
				$pdf->Cell(280,3,''.$_POST['ppto'][$x],0,1,'C',false,0,0,false,'T','C');
				$pdf->Cell(280,3,''.$_POST['nomcargo'][$x],0,0,'C',false,0,0,false,'T','C');
			}
			$v3 = $pdf->gety();
		}
		/* $pdf->SetY($v3);
		$pdf->SetFont('helvetica','',7); */
	}

	$pdf->Output();
?>
