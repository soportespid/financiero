<?php

	header("Content-Type: text/html;charset=utf-8");
    require_once 'PHPExcel/Classes/PHPExcel.php';
    require "comun.inc";
    require "funciones.inc";
    require 'funcionesSP.inc.php';
    /*ini_set('display_errors', '1');
    ini_set('display_startup_errors', '1');
    error_reporting(E_ALL);*/

    session_start();

    class Plantilla{
        private $linkbd;
        private $arrData;
        public function __construct() {
            $this->linkbd = conectar_v7();
            $this->linkbd->set_charset("utf8");
        }
        public function search(string $search){
            if(!empty($_SESSION)){
                $request = $this->selectData($search);
                return $request;
            }
        }
        public function selectData($search=""){
            $s="";
            if($search !=""){
                $s="WHERE id LIKE '$search%' OR cedulanit LIKE '$search%'";
            }
            $sql = "SELECT id,cedulanit, estado,  DATE_FORMAT(fecha,'%d/%m/%Y') as fecha
            FROM tesorepresentantelegal $s
            ORDER BY id DESC";
            $arrData = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
            $total = count($arrData);
            for ($i=0; $i < $total; $i++) {
                $cedula = $arrData[$i]['cedulanit'];
                $sql = "SELECT CONCAT(razonsocial,' ',nombre1,' ',nombre2,' ',apellido1,' ',apellido2) as nombre
                FROM terceros WHERE cedulanit = $cedula";
                $nombre = mysqli_query($this->linkbd,$sql)->fetch_assoc()['nombre'];
                $arrData[$i]['nombre'] = $nombre !="" ? $nombre : "";
            }
            return $arrData;
        }
    }

    if($_GET){
        $obj = new Plantilla();
        $request = $obj->search($_GET['search']);
        //dep($request);exit;
        $objPHPExcel = new PHPExcel();
        $objPHPExcel->getActiveSheet()->getStyle('A:J')->applyFromArray(
            array(
                'alignment' => array(
                    'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
                    'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
                )
            )
        );
        $objPHPExcel->getProperties()
        ->setCreator("IDEAL 10")
        ->setLastModifiedBy("IDEAL 10")
        ->setTitle("Exportar Excel con PHP")
        ->setSubject("Documento de prueba")
        ->setDescription("Documento generado con PHPExcel")
        ->setKeywords("usuarios phpexcel")
        ->setCategory("reportes");

        //----Cuerpo de Documento----
        $objPHPExcel->setActiveSheetIndex(0)
        ->mergeCells('A1:E1')
        ->mergeCells('A2:E2')
        ->setCellValue('A1', 'TESORERÍA')
        ->setCellValue('A2', 'REPORTE INSCRITOS');
        $objPHPExcel-> getActiveSheet ()
        -> getStyle ("A1")
        -> getFill ()
        -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
        -> getStartColor ()
        -> setRGB ('C8C8C8');
        $objPHPExcel-> getActiveSheet ()
        -> getStyle ("A1:A2")
        -> getFont ()
        -> setBold ( true )
        -> setName ( 'Verdana' )
        -> setSize ( 10 )
        -> getColor ()
        -> setRGB ('000000');
        $objPHPExcel-> getActiveSheet ()
        -> getStyle ('A1:A2')
        -> getAlignment ()
        -> applyFromArray (array ( 'horizontal'  =>  PHPExcel_Style_Alignment :: HORIZONTAL_CENTER ,) );
        $objPHPExcel-> getActiveSheet ()
        -> getStyle ('A3:E3')
        -> getAlignment ()
        -> applyFromArray (array ( 'horizontal'  =>  PHPExcel_Style_Alignment :: VERTICAL_JUSTIFY ,) );
        $objPHPExcel-> getActiveSheet ()
        -> getStyle ("A2")
        -> getFill ()
        -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID);

        $borders = array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                'color' => array('argb' => 'FF000000'),
                )
            ),
        );
        $objPHPExcel->setActiveSheetIndex(0)
        ->setCellValue('A3', 'ID')
        ->setCellValue('B3', "Documento")
        ->setCellValue('C3', "Nombre")
        ->setCellValue('D3', "Fecha registro")
        ->setCellValue('E3', "Estado");
        $objPHPExcel-> getActiveSheet ()
            -> getStyle ("A3:E3")
            -> getFill ()
            -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
            -> getStartColor ()
            -> setRGB ('99ddff');
        $objPHPExcel->getActiveSheet()->getStyle("A3:E3")->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getStyle('A1:E1')->applyFromArray($borders);
        $objPHPExcel->getActiveSheet()->getStyle('A2:E2')->applyFromArray($borders);
        $objPHPExcel->getActiveSheet()->getStyle('A3:E3')->applyFromArray($borders);

        $objWorksheet = $objPHPExcel->getActiveSheet();
        $totalData = count($request);
        $row = 4;
        for ($i=0; $i < $totalData ; $i++) {
            $objPHPExcel->getActiveSheet()->getStyle("A$row:E$row")->applyFromArray($borders);
            $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValueExplicit ("A$row", $request[$i]['id'], PHPExcel_Cell_DataType :: TYPE_STRING)
                ->setCellValueExplicit ("B$row", $request[$i]['cedulanit'], PHPExcel_Cell_DataType :: TYPE_STRING)
                ->setCellValueExplicit ("C$row", $request[$i]['nombre'], PHPExcel_Cell_DataType :: TYPE_STRING)
                ->setCellValueExplicit ("D$row", $request[$i]['fecha'], PHPExcel_Cell_DataType :: TYPE_STRING)
                ->setCellValueExplicit ("E$row", $request[$i]['estado'] =="S" || $request[$i]['estado'] =="A" ?"Matriculado":"Matricula cancelada" , PHPExcel_Cell_DataType :: TYPE_STRING);
            if($request[$i]['estado'] == "S" || $request[$i]['estado'] == "A"){
                $objPHPExcel-> getActiveSheet ()
                -> getStyle ("E$row")
                -> getFill ()
                -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
                -> getStartColor ()
                -> setRGB ('198754');
                $objPHPExcel->getActiveSheet()->getStyle("E$row")->getFont()->getColor()->setRGB("ffffff");
                $objPHPExcel->getActiveSheet()->getStyle("E$row")->getFont()->setBold(true);
            }else{
                $objPHPExcel-> getActiveSheet ()
                -> getStyle ("E$row")
                -> getFill ()
                -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
                -> getStartColor ()
                -> setRGB ('ff0000');
                $objPHPExcel->getActiveSheet()->getStyle("E$row")->getFont()->getColor()->setRGB("ffffff");
                $objPHPExcel->getActiveSheet()->getStyle("E$row")->getFont()->setBold(true);
            }
            $row++;
        }
        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
        //----Guardar documento----
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="reporte-matriculados.xlsx"');
        header('Cache-Control: max-age=0');
        $objWriter=PHPExcel_IOFactory::createWriter($objPHPExcel,'Excel2007');
        $objWriter->save('php://output');
        die();
    }
?>
