<?php

    header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require 'comun.inc';
	require 'funciones.inc';
	session_start();
    if(empty($_SESSION)){
        header("location: index.php");
    }
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html lang=es>
	<head>
    <meta charset="utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1"/>
		<meta name="viewport" content="user-scalable=no">
		<title>:: IDEAL 10 - Informes</title>
		<link href="favicon.ico" rel="shortcut icon"/>
		<link href="css/css2z.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
        <link href="css/tabs.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
        <link href="css/style.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" href="sweetalert2/dist/sweetalert2.min.css">
        <script type="text/javascript" src="css/programas.js"></script>
        <script src="sweetalert2/dist/sweetalert2.min.js"></script>
		<script src="vue/vue.min.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script src="css/calendario.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
	</head>
	<body>
		<header>
			<table>
				<tr><script>barra_imagenes("info");</script><?php cuadro_titulos();?></tr>
			</table>
		</header>

		<form name="form2" method="post" action="">
			<section id="myapp" v-cloak >
                <div class="loading-container" v-show="isLoading" >
                    <p class="text-loading"data-loading="IDEAL 10">IDEAL 10</p>
                </div>
				<nav>
					<table>
						<tr><?php menu_desplegable("info");?></tr>
                        <tr>
							<td colspan="3" class="cinta">
                                <img src="imagenes/add2.png"  class="mgbt" title="Nuevo">
								<img src="imagenes/guardad.png"  title="Guardar"  class="mgbt">
								<img src="imagenes/buscad.png"  class="mgbt" title="Buscar">
								<img src="imagenes/nv.png" @click="mypop=window.open('info-principal.php','',''); mypop.focus();" class="mgbt" title="Nueva Ventana">
                                <img src="imagenes/contaduria.jpg" title="contraloria general de la nacion" @click="exportData" class="mgbt">
                                <img src="imagenes/excel.png" class="mgbt" @click="exportExcel" title="excel"></a>
                                <a href="cont-gestioninformecgr.php"  class="mgbt"><img src="imagenes/iratras.png" title="Atr&aacute;s"></a>
                            </td>
						</tr>
					</table>
				</nav>
				<article>
                     <div  class="bg-white">
                        <div>
                            <h2 class="titulos m-0">Operaciones recíprocas</h2>
                            <div class="d-flex w-50">
                                <div class="form-control">
                                    <label class="form-label m-0" for="">Vigencia:</label>
                                    <select v-model="selectVigencia">
                                        <option v-for="(data,index) in arrVigencias" :key="index" :value="data">{{ data}}</option>
                                    </select>
                                </div>
                                <div class="form-control">
                                    <label class="form-label m-0" for="">Periodo:</label>
                                    <select v-model="selectPeriodo">
                                        <option v-for="(data,index) in arrPeriodos" :key="index" :value="data.id">{{ data.nombre}}</option>
                                    </select>
                                </div>
                                <div class="form-control justify-between w-25">
                                    <label for=""></label>
                                    <button type="button" class="btn btn-primary" @click="getBalance">Generar</button>
                                </div>
                            </div>
                        </div>
                        <div>
                            <h2 class="titulos m-0">Balance de prueba</h2>
                            <div class="table-responsive">
                                <table  class="table fw-normal">
                                    <thead>
                                        <tr>
                                            <th>Código</th>
                                            <th>Cuenta</th>
                                            <th>Código entidad</th>
                                            <th class="text-right">Saldo final</th>
                                            <th class="text-right">Corriente</th>
                                            <th class="text-right">No corriente</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr v-for="(data,index) in arrData" :key="index" :class="data.comprobante.length < 6 ? 'bg-primary text-white fw-bold' : ''">
                                            <td>{{data.comprobante}}</td>
                                            <td>{{data.nombre}}</td>
                                            <td>{{data.comprobante.length == 6 ? data.tercero : ""}}</td>
                                            <td class="text-right">{{formatNumero(data.saldo)}}</td>
                                            <td class="text-right">{{data.tipo == 0 ? formatNumero(data.saldo_chip) : 0}}</td>
                                            <td class="text-right">{{data.tipo == 1 ? formatNumero(data.saldo_chip) : 0}}</td>
                                        </tr>
                                        <tr class="bg-primary text-white fw-bold">
                                            <td colspan="3" class="fw-bold text-right">Total:</td>
                                            <td class="text-right">{{formatNumero(total)}}</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
				</article>
			</section>
		</form>

		<script src="node_modules/read-excel-file/bundle/read-excel-file.min.js"></script>
		<script src="node_modules/xlsx/dist/xlsx.full.min.js"></script>
        <script src="Librerias/vue/vue.min.js"></script>
		<script src="Librerias/vue/axios.min.js"></script>
		<script type="module" src="contabilidad_vue/operacion_reciproca/cont-reciproca.js?<?= date('d_m_Y_h_i_s');?>"></script>

	</body>
</html>
