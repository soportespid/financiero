<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require "comun.inc";
	require "funciones.inc";
	require "funcionesSP.inc.php";
	session_start();
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8"); 
	cargarcodigopag($_GET['codpag'],$_SESSION['nivel']);
    date_default_timezone_set("America/Bogota");
    
    $scroll = $_GET['scrtop'];
    $totreg = $_GET['totreg'];
    $idcta = $_GET['idcta'];
    $altura = $_GET['altura'];
	$filtro = " '".$_GET['filtro']."' ";
	$numpag=@ $_GET['numpag'];
	$limreg=@ $_GET['limreg'];
	$scrtop=26*$totreg;
	titlepag();
			
?>

<!DOCTYPE >

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es">
	<head>
		<meta http-equiv="Content-type" content="text/html; charset=iso-8859-1"/>
		<meta http-equiv="X-UA-Compatible" content="IE=9"/>
		<title>:: IDEAL 10 - Servicios P&uacute;blicos</title>
		<link href="css/css2.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/cssSP.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/tabs.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/style.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
		<script type="text/javascript" src="css/calendario.js"></script>
        <script type="text/javascript" src="css/programas.js"></script>
        
		<style></style>
        
		<script>

            function validar(formulario)
			{
				document.getElementById('oculto').value = '7';
				document.form2.submit();
			}

            function despliegamodal2(_valor, _table)
            {
                document.getElementById('bgventanamodal2').style.visibility = _valor;

                if(_valor == 'hidden')
                {
                    document.getElementById('ventana2').src = '';
                }
				else 
				{
                    document.getElementById('ventana2').src = 'barrios-ventana01.php?table=' + _table;
				}

				if(_table == 'terceros')
				{
					document.getElementById('ventana2').src = 'terceros-ventana01.php?table=' + _table;
				}

				if(_table == 'estratos')
                {
					document.getElementById('ventana2').src = 'serv-ventanaEstratos.php';
				}
            }

            function respuestaModalBusqueda(pregunta, value, id)
			{
				switch(pregunta)
				{
                    case 'srvbarrios':	
                        document.getElementById('barrio').value = value;
                        document.getElementById('id_barrio').value = id;
                        break;

                    case 'srvveredas':	
                        document.getElementById('vereda').value = value;
                        document.getElementById('id_vereda').value = id;
                        break;
                    
                    case 'srvzonas':	
                        document.getElementById('zona').value = value;
                        document.getElementById('id_zona').value = id;
                        break;
                        
                    case 'srvlados':	
                        document.getElementById('lado').value = value;
                        document.getElementById('id_lado').value = id;
                        break;
                    
                    case 'srvrutas':	
                        document.getElementById('ruta').value = value;
                        document.getElementById('id_ruta').value = id;
						break;
                }

                document.getElementById('bgventanamodal2').style.visibility = 'hidden';
            }

            function respuestaModalBusqueda2(id, razonsocial, nombre1, nombre2, apellido1, apellido2, cedulanit)
			{
				document.getElementById('terceros').value = cedulanit;

				if(razonsocial == '')
				{
					document.getElementById('id_tercero').value = id;
					document.getElementById('razonsocial').value = razonsocial;
					var nombreCompleto = nombre1 + ' ' + nombre2 + ' ' + apellido1 + ' ' + apellido2;
					document.getElementById('ntercero').value = nombreCompleto;
					document.form2.submit();
				}
				if (razonsocial != '') 
				{
					document.getElementById('id_tercero').value = id;
					document.getElementById('ntercero').value = razonsocial;
					document.form2.submit();
				} 
                
                document.getElementById('bgventanamodal2').style.visibility = 'hidden';
            }

			function despliegamodalm(_valor,_tip,mensa,pregunta)
			{
				document.getElementById("bgventanamodalm").style.visibility=_valor;
                if(_valor == "hidden")
                {
                    document.getElementById('ventanam').src="";
                }
				else
				{
					switch(_tip)
					{
						case "1":	document.getElementById('ventanam').src="ventana-mensaje1.php?titulos="+mensa;break;
						case "2":	document.getElementById('ventanam').src="ventana-mensaje3.php?titulos="+mensa;break;
						case "3":	document.getElementById('ventanam').src="ventana-mensaje2.php?titulos="+mensa;break;
						case "4":	document.getElementById('ventanam').src="ventana-consulta1.php?titulos="+mensa+"&idresp="+pregunta;break;	
					}
				}
            }
			
            function funcionmensaje()
			{
				var x = window.location.search;
				location.href = "serv-editaCliente.php"+x;
            }
            
			function respuestaconsulta(pregunta)
			{
				switch(pregunta)
				{
					case "1":	
						document.form2.oculto.value='2';
						document.form2.submit();
					break;
				}
				document.form2.submit();
            }

			function guardar()
			{
				var validacion01 = document.getElementById('codban').value;
				var validacion02 = document.getElementById('ntercero').value;
                if (validacion01.trim() != '' && validacion02.trim()) 
                {
                    despliegamodalm('visible','4','Esta Seguro de Modificar','1');
                }
                else 
                {
                    despliegamodalm('visible','2','Falta informacion para modificar el cliente');
                }
            }
            
			function cambiocheck()
			{
                if(document.getElementById('myonoffswitch').value == 'S')
                {
                    document.getElementById('myonoffswitch').value = 'N';
                }
                else
                {
                    document.getElementById('myonoffswitch').value = 'S';
                }

				document.form2.submit();
            }
            
			function iratras(scrtop, numpag, limreg, filtro)
			{
				var idcta = document.getElementById('codban').value;
				location.href = "serv-buscaCliente.php?idcta="+idcta+"&scrtop="+scrtop+"&numpag="+numpag+"&limreg="+limreg+ "&filtro="+filtro;
            }
            
			function adelante(scrtop, numpag, limreg, filtro)
			{
				var maximo = document.getElementById('maximo').value;
				var actual = document.getElementById('codban').value;
				actual = parseFloat(actual) + 1;
				if(actual <= parseFloat(maximo))
				{
                    if(actual < 10)
                    {
                        actual = "0" + actual;
                    }

					location.href="serv-editaCliente.php?idban=" +actual+ "&scrtop=" +scrtop+ "&numpag=" +numpag+ "&limreg=" +limreg+ "&filtro=" +filtro;
				}
            }
            
			function atrasc(scrtop, numpag, limreg, filtro, prev)
			{
				var minimo = document.getElementById('minimo').value;
				var actual = document.getElementById('codban').value;
				actual = parseFloat(actual) - 1;
				if(actual >= parseFloat(minimo))
				{
					if(actual < 10){actual = "0" + actual;}
					location.href = "serv-editaCliente.php?idban=" +actual+ "&scrtop=" +scrtop+ "&numpag=" +numpag+ "&limreg=" +limreg+ "&filtro=" +filtro;
				}
			}

			function buscaTerceros()
			{
				document.form2.buscaTercero.value = '1';
				document.form2.submit();
			}
        </script>
                
	</head>
	<body>
		<IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
		<span id="todastablas2"></span>
		<?php
			
		?>
		<table>
			<tr>
                <script>barra_imagenes("serv");</script><?php cuadro_titulos();?>
            </tr>

			<tr><?php menu_desplegable("serv");?></tr>
			<tr>
				<td colspan="3" class="cinta">
					<a href="serv-crearCliente.php" class="mgbt"><img src="imagenes/add.png"/></a>

					<a onclick="guardar();" class="mgbt"><img src="imagenes/guarda.png" title="Guardar"/></a>

					<a href="serv-buscaCliente.php" class="mgbt"><img src="imagenes/busca.png" title="Buscar"/></a>

					<a onclick="mypop=window.open('plan-agenda.php','','');mypop.focus()" class="mgbt"><img src="imagenes/agenda1.png" title="Agenda" /></a>

					<a onclick="mypop=window.open('serv-principal.php','','');mypop.focus();" class="mgbt"><img src="imagenes/nv.png" title="Nueva ventana"></a>

					<a onclick="mypop=window.open('<?php echo $url2; ?>','','');mypop.focus();" class="mgbt"><img src="imagenes/duplicar_pantalla.png" title="Duplicar pesta&ntilde;a"></a>

					<a onClick="iratras(<?php echo "$scrtop, $numpag, $limreg, $filtro"; ?>)" class="mgbt"><img src="imagenes/iratras.png" title="Atr&aacute;s"></a>
                </td>
			</tr>
        </table>
        
		<div id="bgventanamodalm" class="bgventanamodalm">
			<div id="ventanamodalm" class="ventanamodalm">
				<IFRAME src="" name="ventanam" marginWidth=0 marginHeight=0 frameBorder=0 id="ventanam" frameSpacing=0 style=" width:700px; height:130px; top:200; overflow:hidden;"></IFRAME>
			</div>
		</div>

		<form name="form2" method="post" action="" >
			<?php
				if(@ $_POST['oculto'] == '')
				{
					$vigusu = vigencia_usuarios($_SESSION['cedulausu']);
                    $_POST['codban'] = selconsecutivo('srvclientes','id');
					$_POST['tabgroup1'] = 1;

                    $sqlr = "SELECT MIN(id), MAX(id) FROM srvclientes";
                    
                    $res = mysqli_query($linkbd,$sqlr);
                    
                    $r = mysqli_fetch_row($res);
                    
					$_POST['minimo'] = $r[0];
                    $_POST['maximo'] = $r[1];

                    $sqlr = "SELECT * FROM srvdireccion_cliente WHERE id_cliente='".$_GET['idban']."'";

                    $resp = mysqli_query($linkbd, $sqlr);

                    $row = mysqli_fetch_row($resp);

                    $_POST ['direccion']  = $row[2];
					$_POST['direccion_actual'] = $row[2];


                    $sqlr = "SELECT * FROM srvclientes WHERE id='".$_GET['idban']."'";

                    $resp = mysqli_query($linkbd, $sqlr);

                    $row = mysqli_fetch_assoc($resp);

                    $_POST['catastral']   = $row['cod_catastral'];
                    $_POST['id_barrio']   = $row['id_barrio'];
                    $_POST['id_vereda']   = $row['id_vereda'];
                    $_POST['id_zona']     = $row['id_zona'];
                    $_POST['id_lado']     = $row['id_lado'];
                    $_POST['id_ruta']     = $row['id_ruta'];
                    $_POST['poblacion']   = $row['id_poblacion'];
                    $_POST['fecha']       = date('d/m/Y',strtotime($row['fecha_creacion']));
                    $_POST['codban']      = $row['id'];
                    $_POST['cod_usuario'] = $row['cod_usuario'];
                    $_POST['onoffswitch'] = $row['estado'];
					$_POST['id_tercero']  = $row['id_tercero'];
					$_POST['id_estrato']  = $row['id_estrato'];
					$_POST['cod_ruta']	  = $row['codigo_ruta'];

					$_POST['tercero_actual'] = $row['id_tercero'];
					$_POST['estrato_actual'] = $row['id_estrato'];
					$_POST['zona_actual'] 	 = $row['id_zona'];
					$_POST['lado_actual']    = $row['id_lado'];
					$_POST['ruta_actual']    = $row['id_ruta'];
					$_POST['cod_usuario_actual'] = $row['cod_usuario'];
					$_POST['cod_ruta_actual'] = $row['codigo_ruta'];

					$sqlEstrato = "SELECT descripcion FROM srvestratos WHERE id = '$row[id_estrato]' ";
					$resEstrato = mysqli_query($linkbd,$sqlEstrato);
					$rowEstrato = mysqli_fetch_row($resEstrato);

					$_POST['nombreEstrato'] = $rowEstrato[0];
                    
                    $sqlr = "SELECT * FROM terceros WHERE id_tercero = '".$_POST['id_tercero']."' ";

                    $resp = mysqli_query($linkbd, $sqlr);

                    $row = mysqli_fetch_assoc($resp);

                    $nombreCompleto = $row['nombre1']. ' ' .$row['nombre2']. ' ' .$row['apellido1']. ' ' .$row['apellido2'];

					if($row['razonsocial'] != '')
					{
						$nombreCompleto = $row['razonsocial'];
					}

					$_POST['ntercero'] = $nombreCompleto;
					$_POST['terceros'] = $row['cedulanit'];

                    $sqlr = "SELECT nombre FROM srvbarrios WHERE id = '".$_POST['id_barrio']."' ";
                    $resp = mysqli_query($linkbd, $sqlr);
                    $row = mysqli_fetch_assoc($resp);

                    $_POST ['barrio'] = $row['nombre'];

                    
                    $sqlr = "SELECT nombre FROM srvveredas WHERE id = '".$_POST['id_vereda']."' ";
                    $resp = mysqli_query($linkbd, $sqlr);
                    $row = mysqli_fetch_assoc($resp);

                    $_POST ['vereda'] = $row['nombre'];


                    $sqlr = "SELECT nombre FROM srvzonas WHERE id='".$_POST['id_zona']."' ";
                    $resp = mysqli_query($linkbd, $sqlr);
                    $row = mysqli_fetch_assoc($resp);

                    $_POST ['zona'] = $row['nombre'];

                
                    $sqlr = "SELECT nombre FROM srvlados WHERE id='".$_POST['id_lado']."' ";
                    $resp = mysqli_query($linkbd, $sqlr);
                    $row = mysqli_fetch_assoc($resp);

                    $_POST ['lado'] = $row['nombre'];


                    $sqlr = "SELECT nombre FROM srvrutas WHERE id='".$_POST['id_ruta']."' ";
                    $resp = mysqli_query($linkbd, $sqlr);
                    $row = mysqli_fetch_assoc($resp);

                    $_POST ['ruta'] = $row['nombre'];

				}

				if(@$_POST['buscaTercero'] == '1')
				{
					$datosTercero = buscaNombreTercero($_POST['terceros']);

					for ($i=0; $i < count($datosTercero); $i++) 
					{ 
						$_POST['ntercero'] = $datosTercero[0];
						$_POST['id_tercero'] = $datosTercero[1];
					}
					
				}

				switch($_POST['tabgroup1'])
				{
					case 1:	
						$check1='checked';
					break;

					case 2:	
						$check2='checked';
					break;
				}
			?>
			<div>
				<table class="inicio">
					<tr>
						<td class="titulos" colspan="6">.: Editar Cliente</td>
						<td class="cerrar" style="width:7%" onClick="location.href='serv-principal.php'">Cerrar</td>
					</tr>
					<tr>                             
						<td class="tamano01" style="width:3cm;">ID: </td>
						<td style="width:15%; text-align: center;">
							<a onClick="atrasc(<?php echo "$scrtop, $numpag, $limreg, $filtro"; ?>)" class="icobut"><img src="imagenes/back.png" title="Anterior"/></a>
							
							<input type="text" name="codban" id="codban"  value="<?php echo $_POST['codban']?>" style="width:60%;height:30px; text-align: center;" readonly/>

							<a onClick="adelante(<?php echo "$scrtop, $numpag, $limreg, $filtro" ?>);" class="icobut"><img src="imagenes/next.png"  title="Sigiente"/></a>
						</td>

						<td class="tamano01" style="width: 10%;">C&oacute;digo Usuario:</td> 

						<td>
							<input type="text" name='cod_usuario' id='cod_usuario' value="<?php echo @$_POST['cod_usuario'];?>" style="width:100%;height:30px;text-transform:uppercase;text-align:center;"/>
							<input type="hidden" name="cod_usuario_actual" id="cod_usuario_actual" value="<?php echo $_POST['cod_usuario_actual'] ?>">
						</td>

						<td class="tamano01" style="width: 10%;">Codigo Catastral:</td>

						<td style="width: 25%;">
							<input type="text" name="catastral" id="catastral" value="<?php echo @ $_POST['catastral'];?>" style="width:100%;height:30px;text-transform:uppercase; text-align: center;" readonly/>
						</td>

					</tr>

					<tr>
						<td class="tamano01" style="width:5%;">Fecha:</td>

						<td style="width:15%;">
							<input type="text" name="fecha" value="<?php echo @ $_POST['fecha']?>" maxlength="10" onKeyPress="javascript:return solonumeros(event)" id="fc_1198971545" onKeyDown="mascara(this,'/',patron,true)" title="DD/MM/YYYY" style="width:80%;height:30px;text-align:center"readonly>&nbsp;<img src="imagenes/calendario04.png" title="Calendario" class="icobut"/>
						</td>

						<td class="tamano01">Usuario:</td>

						<td style="width:15%;">
							<input type="text" name="terceros" id="terceros"  onBlur="buscaTerceros()" value="<?php echo @$_POST['terceros']?>" style="width:88%;height:30px;text-align:center" />

							<a title="Cuentas presupuestales" onClick="despliegamodal2('visible', 'terceros');" style="cursor:pointer;"><img src="imagenes/find02.png" style="width:20px;"/></a>

							<input type="hidden" name='id_tercero' id='id_tercero' value="<?php echo $_POST['id_tercero']?>">
							<input type="hidden" name='razonsocial' id='razonsocial' value="<?php echo $_POST['razonsocial']?>">
							<input type="hidden" name='nombre1' id='nombre1' value="<?php echo $_POST['nombre1']?>">
							<input type="hidden" name='nombre2' id='nombre2' value="<?php echo $_POST['nombre2']?>">
							<input type="hidden" name='apellido1' id='apellido1' value="<?php echo $_POST['apellido1']?>">
							<input type="hidden" name='apellido2' id='apellido2' value="<?php echo $_POST['apellido2']?>">
							<input type="hidden" name='tercero_actual' id='tercero_actual' value="<?php echo $_POST['tercero_actual']?>">
						</td>

						<td colspan="2">
							<input type="text" name="ntercero" id="ntercero" value="<?php echo @$_POST['ntercero']?>" style="width:100%;height:30px;" readonly>
						</td>
					</tr>

					<?php
						if ($_POST['id_tercero'] != $_POST['tercero_actual']) 
						{
					?>
							<tr>
								<td class="tamano01">Novedad:</td>
								<td colspan="4">
									<input type="text" name="novedad_cambio" id="novedad_cambio" value="<?php echo @$_POST['novedad_cambio']?>" style="width:100%;height:30px;" />	
								</td>
							</tr>
					<?php
						}
					?>
					<tr>
						<td class="tamano01">Direcci&oacute;n:</td>
						
						<td colspan="4">
							<input type="text" name="direccion" id="direccion" value="<?php echo @$_POST['direccion']?>" style="width:100%;height:30px;"/>	
							<input type="hidden" name="direccion_actual" id="direccion_actual" value="<?php echo $_POST['direccion_actual'] ?>">
						</td>
					</tr>
					<tr>
						<td class="tamano01">Prefijo:</td>
						<td>
							<?php 
								$sqlr 		= "SELECT depto,mnpio FROM terceros WHERE id_tercero='$_POST[id_tercero]'";
								$resp 		= mysqli_query($linkbd, $sqlr);
								$row 		= mysqli_fetch_row($resp);
								$prefijo 	= $row[0].$row[1];
							?>
							<input type="text" name="codprefijo" id="codprefijo" value="<?php echo $prefijo;?>" style="width:100%;height:30px;" readonly/>	
						</td>

						<td class="tamano01">Estrato:</td>
						
						<td>
							<input type="text" name='nombreEstrato' id='nombreEstrato' value="<?php echo $_POST['nombreEstrato']?>" style="width:100%;height: 30px;" onclick = "despliegamodal2('visible', 'estratos');" class="colordobleclik" readonly>

							<input type="hidden" name='id_estrato' id='id_estrato' value="<?php echo $_POST['id_estrato']?>">
							<input type="hidden" name="estrato_actual" id="estrato_actual" value="<?php echo $_POST['estrato_actual'] ?>">
						</td>
						
						
						
					</tr>

					<tr>
						<td class="tamano01" style="width:3cm;">Barrio:</td>

						<td>
							<input type="text" name="barrio" id="barrio" onKeyUp="return tabular(event,this)" value="<?php echo $_POST['barrio']?>" style="width:100%;height: 30px;" onclick = "despliegamodal2('visible', 'srvbarrios');"  class="colordobleclik" readonly>

							<input type="hidden" name="id_barrio" id="id_barrio" value="<?php echo @ $_POST['id_barrio']?>">
							
						</td>
						
						<td class="tamano01" style="width:3cm;">Vereda:</td>

						<td>
							<input type="text" name="vereda" id="vereda" onKeyUp="return tabular(event,this)" value="<?php echo $_POST['vereda']?>" style="width:100%;height: 30px;" onclick = "despliegamodal2('visible', 'srvveredas');"  class="colordobleclik" readonly>

							<input type="hidden" name="id_vereda" id="id_vereda" value="<?php echo @ $_POST['id_vereda']?>">
							
						</td>

						<td class="tamano01" style="width:3cm;">Zona:</td>
						
						<td>
							<input type="text" name="zona" id="zona" onKeyUp="return tabular(event,this)" value="<?php echo $_POST['zona']?>" style="width:100%; height: 30px;" onclick = "despliegamodal2('visible', 'srvzonas');"  class="colordobleclik" readonly>

							<input type="hidden" name="id_zona" id="id_zona" value="<?php echo $_POST['id_zona']?>">
							<input type="hidden" name="zona_actual" id="zona_actual" value="<?php echo $_POST['zona_actual'] ?>">
						</td>
					</tr>

					<tr>
						

						<td class="tamano01" style="width:3cm;">Lado:</td>

						<td>
							<input type="text" name="lado" id="lado" onKeyUp="return tabular(event,this)" value="<?php echo $_POST['lado']?>" style="width:100%;height: 30px;" onclick = "despliegamodal2('visible', 'srvlados');"  class="colordobleclik" readonly>

							<input type="hidden" name="id_lado" id="id_lado" value="<?php echo $_POST['id_lado']?>">
							<input type="hidden" name="lado_actual" id="lado_actual" value="<?php echo $_POST['lado_actual'] ?>">
						</td>

						<td class="tamano01" style="width:3cm;">Ruta:</td>

						<td>
							<input type="text" name="ruta" id="ruta" onKeyUp="return tabular(event,this)" value="<?php echo $_POST['ruta']?>" style="width:100%;height: 30px;" onclick = "despliegamodal2('visible', 'srvrutas');"  class="colordobleclik" readonly>

							<input type="hidden" name="id_ruta" id="id_ruta" value="<?php echo $_POST['id_ruta']?>">
							<input type="hidden" name="ruta_actual" id="ruta_actual" value="<?php echo $_POST['ruta_actual'] ?>">
						</td>
						
						<td class="tamano01" style="width: 3cm;">Código Ruta:</td>
						<td>
							<input type="text" name="cod_ruta" id="cod_ruta" value="<?php echo @$_POST['cod_ruta'] ?>" style="width: 98%; height: 30px; text-align:center;">
						</td>
					</tr>
					
					<tr>
						<td class="tamano01" style="width:3cm;">Estado:</td>
						<td>
							<div class="onoffswitch">
								<input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox" id="myonoffswitch" value="<?php echo @ $_POST['onoffswitch'];?>" <?php if(@ $_POST['onoffswitch']=='S'){echo "checked";}?> onChange="cambiocheck();"/>
								<label class="onoffswitch-label" for="myonoffswitch">
									<span class="onoffswitch-inner"></span>
									<span class="onoffswitch-switch"></span>
								</label>
							</div>
						</td>
					</tr>
				</table>
				
				<div class="tabscontra" style="height:54%; width:99.6%">
					<div class="tab">
						<input type="radio" id="tab-1" name="tabgroup1" value="1" <?php echo $check1;?>/>

						<label for="tab-1">Historial General</label>

						<div class="content" style="overflow:hidden">
							<table class="inicio">
								<tr>
									<td class="titulos" style="text-align:center;" colspan="10">.: Historial Cambios Generales</td>
								</tr>

								<tr style="text-align: center;" class="titulos2">
									<td>Fecha Cambio</td>
									<td>Cod Usuario</td>
									<td>Usuario</td>
									<td>Cedula</td>
									<td>Estrato</td>
									<td>Zona</td>
									<td>Lado</td>
									<td>Ruta</td>
									<td>Direccion</td>
									<td>Perfil</td>
								</tr>

								<?php
									$iter = 'saludo1a';
									$iter2 = 'saludo2';

									$sqlHistorial = "SELECT HC.id_tercero, HC.cod_usuario, HC.direccion, E.descripcion, Z.nombre, L.nombre, R.nombre, HC.fecha_cambio, HC.usuario, T.nombre1, T.nombre2, T.apellido1, T.apellido2, T.razonsocial, T.cedulanit FROM srv_historial_clientes AS HC INNER JOIN terceros AS T ON HC.id_tercero = T.id_tercero INNER JOIN srvestratos AS E ON HC.id_estrato = E.id INNER JOIN srvzonas AS Z ON HC.id_zona = Z.id INNER JOIN srvlados AS L ON HC.id_lado = L.id INNER JOIN srvrutas AS R ON HC.id_ruta = R.id WHERE id_cliente = '$_POST[codban]'"; 
									$resp = mysqli_query($linkbd, $sqlHistorial);
									while ($row = mysqli_fetch_row($resp))
									{
										if ($row[13] != '')
										{
											$nombreUsuario = $row[13];
										}
										else
										{
											$nombreUsuario = $row[9]. ' '. $row[10]. ' '. $row[11]. ' '. $row[12];
										}
								?>
										<tr class='<?php echo $iter?>' style='text-transform:uppercase; text-align:center;'>
											<td><?php echo $row[7] ?></td>
											<td><?php echo $row[1] ?></td>
											<td><?php echo $nombreUsuario ?></td>
											<td><?php echo $row[14] ?></td>
											<td><?php echo $row[3] ?></td>
											<td><?php echo $row[4] ?></td>
											<td><?php echo $row[5] ?></td>
											<td><?php echo $row[6] ?></td>
											<td><?php echo $row[2] ?></td>
											<td><?php echo $row[8] ?></td>
										</tr>
								<?php		
										$aux = $iter;
										$iter = $iter2;
										$iter2 = $aux; 	
									}
								?>
							</table>
						</div>
					</div>

					<div class="tab">
						<input type="radio" id="tab-2" name="tabgroup1" value="2" <?php echo $check2;?>/>

						<label for="tab-2">Historial Usuarios</label>

						<div class="content" style="overflow:hidden">
							<table class="inicio">
								<tr>
									<td class="titulos" style="text-align:center;" colspan="6">.: Historial de Usuarios</td>
								</tr>

								<tr style="text-align: center;" class="titulos2">
									<td>Fecha Cambio</td>
									<td>Usuario Anterior</td>
									<td>Usuario Nuevo</td>
									<td>Novedad</td>
									<td>Perfil</td>
								</tr>

								<?php
									//buscar en base de terceros antiguos
									$iter = 'saludo1a';
									$iter2 = 'saludo2';

									$sqlCambioTercero = "SELECT id_tercero_anterior, id_tercero_nuevo, fecha_cambio, novedad, usuario FROM srvcambio_tercero WHERE id_cliente = '$_POST[codban]'";
									$res = mysqli_query($linkbd, $sqlCambioTercero);
									while ($row = mysqli_fetch_row($res))
									{
										$sql1 = "SELECT nombre1, nombre2, apellido1, apellido2, razonsocial FROM terceros WHERE id_tercero = $row[0]";
										$res1 = mysqli_query($linkbd,$sql1);
										$row1 = mysqli_fetch_row($res1);

										if ($row1[4] != '')
										{
											$nombreUsuarioAnterior = $row1[4];
										}
										else
										{
											$nombreUsuarioAnterior = $row1[0]. ' ' .$row1[1]. ' ' .$row1[2]. ' ' .$row1[3];
										}

										$sql2 = "SELECT nombre1, nombre2, apellido1, apellido2, razonsocial FROM terceros WHERE id_tercero = $row[1]";
										$res2 = mysqli_query($linkbd,$sql2);
										$row2 = mysqli_fetch_row($res2);

										if ($row2[4] != '')
										{
											$nombreUsuarioNuevo = $row2[4];
										}
										else
										{
											$nombreUsuarioNuevo = $row2[0]. ' ' .$row2[1]. ' ' .$row2[2]. ' ' .$row2[3];
										}
								?>
										<tr class='<?php echo $iter?>' style='text-transform:uppercase; text-align:center;'>
											<td><?php echo $row[2] ?></td>
											<td><?php echo $nombreUsuarioAnterior ?></td>
											<td><?php echo $nombreUsuarioNuevo ?></td>
											<td><?php echo $row[3] ?></td>
											<td><?php echo $row[4] ?></td>
										</tr>
								<?php
										$aux = $iter;
										$iter = $iter2;
										$iter2 = $aux; 	
									}
								?>
							</table>
						</div>
					</div>
				</div>

			<input type="hidden" name="maximo" id="maximo" value="<?php echo @ $_POST['maximo']?>"/>
			<input type="hidden" name="minimo" id="minimo" value="<?php echo @ $_POST['minimo']?>"/>
			<input type="hidden" name="oculto" id="oculto" value="1"/>
			<input type="hidden" name="buscaTercero" id="buscaTercero" value=""/>

			    <?php
                    if(@ $_POST['oculto'] == "2")
                    {	
						$fecha = date('Y-m-d');
						$usuario = $_SESSION['usuario'];
                 
                        if (@ $_POST['onoffswitch'] != 'S')
                        {
                            $valest = 'N';
                        }
                        else 
                        {
                            $valest = 'S';
                        }

						if (!empty($_POST['id_zona'])) {
							$id_zona = $_POST['id_zona'];
						}
						else {
							$id_zona = 'NULL';
						}
						
						if (!empty($_POST['id_lado'])) {
							$id_lado = $_POST['id_lado'];
						}
						else {
							$id_lado = 'NULL';
						}
	
						$number = $_POST['cod_usuario'];
						$length = 10;
						$codigoUsuario = substr(str_repeat(0, $length).$number, - $length);

						if (($_POST['direccion'] != $_POST['direccion_actual']) || ($_POST['id_estrato'] != $_POST['estrato_actual']) || ($_POST['id_zona'] != $_POST['zona_actual']) || ($_POST['id_lado'] != $_POST['lado_actual']) || ($_POST['id_ruta']) != $_POST['ruta_actual'] || ($_POST['cod_ruta'] != $_POST['cod_ruta_actual'])) 
						{
							$query = "INSERT srv_historial_clientes (id_cliente, id_tercero, cod_usuario, direccion, id_estrato, id_zona, id_lado, id_ruta, codigo_ruta, fecha_cambio, usuario) VALUES ('$_POST[codban]', '$_POST[tercero_actual]', '$_POST[cod_usuario_actual]', '$_POST[direccion_actual]', '$_POST[estrato_actual]', '$_POST[zona_actual]', '$_POST[lado_actual]', '$_POST[ruta_actual]', '$_POST[cod_ruta_actual]', '$fecha', '$usuario')";
							mysqli_query($linkbd, $query);
						}

						if (($_POST['id_tercero'] != $_POST['tercero_actual']))
						{
							$sqlr1 = "INSERT INTO srvcambio_tercero (id_cliente, id_tercero_anterior, id_tercero_nuevo, fecha_cambio, novedad, usuario, estado) VALUES ('$_POST[codban]', '$_POST[tercero_actual]', '$_POST[id_tercero]', '$fecha', '$_POST[novedad_cambio]', '$usuario', 'N')";
							mysqli_query($linkbd, $sqlr1);
						}
						
						$sqlr = "UPDATE srvclientes SET prefijo='".$_POST['codprefijo']."', id_barrio='".$_POST['id_barrio']."', id_poblacion='".$_POST['poblacion']."', id_vereda='".$_POST['id_vereda']."',id_zona=$id_zona,id_lado=$id_lado,id_ruta='".$_POST['id_ruta']."', id_estrato='".$_POST['id_estrato']."', id_tercero='".$_POST['id_tercero']."', estado='$valest', cod_usuario = '$_POST[cod_usuario]', codigo_ruta = '$_POST[cod_ruta]' WHERE id='".$_POST['codban']."' ";

						$sqlr2 = "UPDATE srvdireccion_cliente SET direccion = '$_POST[direccion]' WHERE id_cliente='".$_POST['codban']."'";
						mysqli_query($linkbd, $sqlr2);

						if (!mysqli_query($linkbd,$sqlr))
						{
							echo"
								<script>
									despliegamodalm('visible','2','No se pudo ejecutar la peticion');
								</script>";
						}
						else 
						{
							echo "
								<script>
									despliegamodalm('visible','1','Se ha Editado con Exito');
								</script>";
						}
                    }
            	?>    
                </table>
                    
                    <input type="hidden" name="ocules" id="ocules" value="<?php echo @ $_POST['ocules'];?>">
                    <input type="hidden" name="actdes" id="actdes" value="<?php echo @ $_POST['actdes'];?>">
                    <input type="hidden" name="numtop" id="numtop" value="<?php echo @ $_POST['numtop'];?>" />
                    
                    <div id="bgventanamodal2">
                    <div id="ventanamodal2">
                        <IFRAME  src="" name="buster" marginWidth=0 marginHeight=0 frameBorder=0 id="ventana2" frameSpacing=0 style="left:500px; width:900px; height:500px; top:200;"></IFRAME>
                    </div>
                    
                </div>

                
        </form>                          
    </body>   
</html>
