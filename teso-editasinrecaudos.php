<?php
	require "comun.inc";
	require "funciones.inc";
	require "conversor.php";
	session_start();
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	titlepag();
 	$linkbd=conectar_v7();
	$scroll=$_GET['scrtop'];
	$totreg=$_GET['totreg'];
	$idcta=$_GET['idcta'];
	$altura=$_GET['altura'];
	$filtro="'".$_GET['filtro']."'";
?>

<html lang="es">
	<head>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
    	<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<title>:: IDEAL 10 - Tesorer&iacute;a</title>
		<link href="favicon.ico" rel="shortcut icon"/>
		<script src="css/programas.js"></script>
		<link href="css/css2.css" rel="stylesheet" type="text/css" />
		<link href="css/css2z.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/tabs.css" rel="stylesheet" type="text/css" />

		<script>
			/* function agregardetalle() {

				if(document.form2.codingreso.value!="" &&  document.form2.valor.value>0 ) { 
				
					document.form2.agregadet.value=1;
					document.form2.submit();
				}
				else {
					
					alert("Falta informacion para poder Agregar");
				}
			}

			function eliminar(variable) {
				
				if (confirm("Esta Seguro de Eliminar")) {

					document.form2.elimina.value=variable;
					vvend=document.getElementById('elimina');
					vvend.value=variable;
					document.form2.submit();
				}
			} */

			function pdf() {

				document.form2.action="teso-pdfrecaudos.php";
				document.form2.target="_BLANK";
				document.form2.submit(); 
				document.form2.action="";
				document.form2.target="";
			}

			function adelante(scrtop, numpag, limreg, filtro) {
				
				if(parseFloat(document.form2.ncomp.value)<parseFloat(document.form2.maximo.value)) {

					document.form2.oculto.value=1;
					document.form2.ncomp.value=parseFloat(document.form2.ncomp.value)+1;
					document.form2.idcomp.value=parseFloat(document.form2.idcomp.value)+1;
					var idcta=document.getElementById('ncomp').value;
					document.form2.action="teso-editasinrecaudos.php?idcta="+idcta+"&scrtop="+scrtop+"&numpag="+numpag+"&limreg="+limreg+"&filtro="+filtro;
					document.form2.submit();
				}
			}

			function atrasc(scrtop, numpag, limreg, filtro) {

				if(document.form2.ncomp.value>1) {

					document.form2.oculto.value=1;
					document.form2.ncomp.value=document.form2.ncomp.value-1;
					document.form2.idcomp.value=document.form2.idcomp.value-1;
					var idcta=document.getElementById('ncomp').value;
					document.form2.action="teso-editasinrecaudos.php?idcta="+idcta+"&scrtop="+scrtop+"&numpag="+numpag+"&limreg="+limreg+"&filtro="+filtro;
					document.form2.submit();
				}
			}

			function iratras(scrtop, numpag, limreg, filtro) {

				var idcta=document.getElementById('ncomp').value;
				location.href="teso-buscasinrecaudos.php?idcta="+idcta+"&scrtop="+scrtop+"&numpag="+numpag+"&limreg="+limreg+"&filtro="+filtro;
			}

			function dirigetipoingreso(_id){
				window.open("teso-editaingresos.php?idr=" + _id);
			}

		</script>
	</head>

	<body>
		<IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
		<span id="todastablas2"></span>

        <?php
			$numpag=$_GET['numpag'];
			$limreg=$_GET['limreg'];
			$scrtop=26*$totreg;
		?>

		<table>
			<tr><script>barra_imagenes("teso");</script><?php cuadro_titulos();?></tr>	 
			<tr><?php menu_desplegable("teso");?></tr>

			<tr>
				<td colspan="3" class="cinta">
					<a href="teso-sinrecaudos.php" class="mgbt"><img src="imagenes/add.png" title="Nuevo"/></a> 
					<a class="mgbt1"><img src="imagenes/guardad.png"/></a>
					<a href="teso-buscasinrecaudos.php" class="mgbt"><img src="imagenes/busca.png" title="Buscar" /></a> 
					<a onClick="mypop=window.open('plan-agenda.php','','');mypop.focus()" class="mgbt"><img src="imagenes/agenda1.png" title="Agenda"/></a>
					<a onClick="mypop=window.open('teso-principal.php','','');mypop.focus();" class="mgbt"><img src="imagenes/nv.png" title="Nueva ventana"></a>
					<a onClick="mypop=window.open('<?php echo $url2; ?>','','');mypop.focus();" class="tooltip bottom mgbt"><img src="imagenes/duplicar_pantalla.png" title="Duplicar pesta&ntilde;a"></a>
					<a onClick="pdf()" class="mgbt"> <img src="imagenes/print.png"  title="Buscar" /></a> 
					<a onClick="iratras(<?php echo $scrtop; ?>, <?php echo $numpag; ?>, <?php echo $limreg; ?>, <?php echo $filtro; ?>)" class="mgbt"><img src="imagenes/iratras.png" title="Atr&aacute;s"></a>
				</td>
			</tr>		  
		</table>

		<?php
			$vigencia=date('Y');
 			$vigusu=vigencia_usuarios($_SESSION['cedulausu']); 

			if(!$_POST['oculto']) {

				$check1="checked";
				$fec=date("d/m/Y");	
				$_POST['vigencia']=$vigencia;
				$sqlr="select *from cuentacaja where estado='S' and vigencia=".$vigusu;
				$res=mysqli_query($linkbd,$sqlr);
				while ($row =mysqli_fetch_row($res)) {

					$_POST['cuentacaja']=$row[1];
				}
	
				$sqlr="select * from tesosinrecaudos ORDER BY id_recaudo DESC";
				$res=mysqli_query($linkbd, $sqlr);
				$r=mysqli_fetch_row($res);
				$_POST['maximo']=$r[0];
				$_POST['ncomp']=$_GET['idrecaudo'];
				$check1="checked"; 
				$check1="checked"; 
				$check1="checked"; 
				$fec=date("d/m/Y");
				$_POST['vigencia']=$vigusu; 		
				$sqlr="select * from tesosinrecaudos where id_recaudo=".$_POST['ncomp'];
				$res=mysqli_query($linkbd,$sqlr);
				$consec=0;
				while($r=mysqli_fetch_row($res)) {

					$_POST['fecha']=date('d/m/Y',strtotime($r[2]));
					$_POST['compcont']=$r[1];
					$consec=$r[0];	  
					$consec=$r[0];	  
					$consec=$r[0];	  
					$_POST['rp']=$r[4];
				}

					$_POST['idcomp']=$consec;	
					$fechaf=$fecha[3]."/".$fecha[2]."/".$fecha[1];
					$_POST['fecha']=$fechaf;	
			}

			$sqlr="select *from tesosinrecaudos where tesosinrecaudos.id_recaudo=$_POST[idcomp] ";
  	  		$_POST['encontro']="";
  			$res=mysqli_query($linkbd,$sqlr);
			while ($row =mysqli_fetch_row($res)) {

				$_POST['concepto']=$row[6];	
				$_POST['valorecaudo']=$row[5];	
				$_POST['totalc']=$row[5];	
				$_POST['tercero']=$row[4];	
				$_POST['ntercero']=buscatercero($row[4]);	
				$_POST['fecha']=date('d/m/Y',strtotime($row[2]));
				$_POST['valor']=0;		 	
				$_POST['encontro']=1;
				$_POST['numerocomp']=$row[1];
				if($row[7]=='S')
					$_POST['estadoc']='ACTIVO'; 	 				  
				if($row[7]=='P')
					$_POST['estadoc']='PAGO'; 	 				  
				if($row[7]=='N')
					$_POST['estadoc']='ANULADO'; 
			}
		?>

		<form name="form2" method="post" action=""> 
			<table class="inicio" align="center" >
     			<tr>
					<td style="width:95%;" class="titulos" colspan="2">Reconocimiento Ingresos internos</td>
					<td style="width:5%;" class="cerrar" ><a href="teso-principal.php">Cerrar</a></td>
		      	</tr>

				<tr>
					<td>
					<table>
						<tr>
							<td style="width:12%;" class="saludo1" >Numero Liquidaci&oacute;n:</td>
							<td  style="width:10%;" >
								<a href="#" onClick="atrasc(<?php echo $scrtop; ?>, <?php echo $numpag; ?>, <?php echo $limreg; ?>, <?php echo $filtro; ?>)"><img src="imagenes/back.png" alt="anterior" align="absmiddle"></a> 
								<input type="hidden" id="numerocomp" name="numerocomp" value="<?php echo $_POST['numerocomp']?>" >
								<input id="idcomp" name="idcomp" type="text" style="width:60%; text-align:center" value="<?php echo $_POST['idcomp']?>" onKeyUp="return tabular(event,this) "  onBlur="validar2()"  readonly> 
								<input id="ncomp" name="ncomp" type="hidden" value="<?php echo $_POST['ncomp']?>"><input name="compcont" type="hidden" value="<?php echo $_POST['compcont']?>">
								<a href="#" onClick="adelante(<?php echo $scrtop; ?>, <?php echo $numpag; ?>, <?php echo $limreg; ?>, <?php echo $filtro; ?>)"><img src="imagenes/next.png" alt="siguiente" align="absmiddle"></a> 
								<input type="hidden" value="a" name="atras" >
								<input type="hidden" value="s" name="siguiente" >
								<input type="hidden" value="<?php echo $_POST['maximo']?>" name="maximo">
							</td>
							<td style="width:10%;" class="saludo1">Fecha:</td>
							<td style="width:10%;">
								<input name="fecha" type="text"  onKeyDown="mascara(this,'/',patron,true)"  onKeyUp="return tabular(event,this)" value="<?php echo $_POST['fecha']?>" style="width:100%" maxlength="10" readonly>        
							</td>
							<td style="width:5%;" class="saludo1">Vigencia:</td>
							<td style="width:10%;">
								<input type="text" id="vigencia" name="vigencia"  onKeyPress="javascript:return solonumeros(event)" 
								onKeyUp="return tabular(event,this)"  value="<?php echo $_POST['vigencia']?>" onClick="document.getElementById('tipocta').focus();document.getElementById('tipocta').select();" readonly> 
							</td>
							<td style="width:10%;" class="saludo1">Estado:</td>
							<td style="width:10%;">
								<?php 
								if($_POST['estadoc']=="ACTIVO"){
									$valuees="ACTIVO";
									$stylest="width:100%; background-color:#0CD02A; color:white; text-align:center;";
								}else if($_POST['estadoc']=="ANULADO"){
									$valuees="ANULADO";
									$stylest="width:100%; background-color:#FF0000; color:white; text-align:center;";
								}else if($_POST['estadoc']=="PAGO"){
									$valuees="PAGO";
									$stylest="width:100%; background-color:#0404B4; color:white; text-align:center;";
								}

								echo "<input type='text' name='estado' id='estado' value='$valuees' style='$stylest' readonly />";
							?>
							</td>
							<td></td>   
						</tr>
						<tr>
							<td  class="saludo1">Concepto Liquidaci&oacute;n:</td>
							<td colspan="8" >
								<textarea name="concepto" id="concepto" style="width:100%; form-sizing: content; max-height: 100px; min-height: 20px; resize: vertical; background-color:#E6F7FF;" class="estilos-scroll" disabled><?php echo $_POST['concepto']?></textarea>
							</td>
						</tr>  
						<tr>
							<td class="saludo1">CC/NIT: </td>
							<td>
								<input  style="width:80%;" name="tercero" type="text" onKeyUp="return tabular(event,this)" value="<?php echo $_POST['tercero']?>"  readonly>
								
							</td>
							<td class="saludo1">Contribuyente:
							</td>
							<td colspan="6">
								<input  style="width:100%;" type="text" id="ntercero" name="ntercero" value="<?php echo $_POST['ntercero']?>"  onKeyUp="return tabular(event,this) "  readonly>
								<input type="hidden" id="cb" name="cb" value="<?php echo $_POST['cb']?>" >
								<input type="hidden" id="ct" name="ct" value="<?php echo $_POST['ct']?>" >
								<input type="hidden" value="1" name="oculto">
							</td>
						
						</tr>
						<!-- <tr>
							<td class="saludo1">Cod Ingreso:</td>
							<td colspan="3">
								<input  style="width:26%;" name="codingreso" type="text" id="codingreso" onKeyUp="return tabular(event,this)" value="<?php echo $_POST['codingreso']?>"  readonly > <a href="#" onClick="mypop=window.open('ingresos-ventana.php?ti=I&modulo=4','','menubar=0,scrollbars=yes, toolbar=no, location=no, width=700px,height=500px');mypop.focus();"><img src="imagenes/buscarep.png" align="absmiddle" border="0"> 
								<input style="width:66%;" name="ningreso" type="text" id="ningreso" value="<?php echo $_POST['ningreso']?>"  readonly>
							</a>
							</td>
							<td class="saludo1" >Valor:</td>
							<td>
								<input name="valor" type="text" id="valor" onKeyUp="return tabular(event,this)" value="<?php echo $_POST['valor']?>" readonly ></td><td colspan="2">
							</td>
						</tr> -->
							
					</table>
				</tr>
      		</table>

     		<div class="subpantalla estilos-scroll" style="resize: vertical;">
       			<?php 
					$sqlr="select *from tesosinrecaudos_det where tesosinrecaudos_det.id_recaudo=$_POST[idcomp]";
					$_POST['dcoding']= array(); 		 
					$_POST['dncoding']= array();
					$_POST['dfuente']= array();
					$_POST['dvalores']= array(); 		 
  					$res=mysqli_query($linkbd,$sqlr);
					while ($row =mysqli_fetch_row($res)) {
						$_POST['dcoding'][]=$row[2];
						$_POST['dncoding'][]=buscaingreso($row[2]);
						$_POST['dfuente'][]=$row[6];
						$_POST['dvalores'][]=$row[3];	
					}
 				?>

	   			<table class="inicio">
					<tr>
						<td colspan="5" class="titulos">Detalles reconocimientos ingresos internos</td>
					</tr>      

					<tr>
						<td style="width:5%;" class="titulos2">Codigo</td>
						<td style="width:72%;" class="titulos2">Ingreso</td>
						<td style="width:10%;" class="titulos2">Fuente</td>
						<td style="width:10%;" class="titulos2">Valor</td>
						<td style="width:3%;" class="titulos2">
							
						</td>
					</tr>

					<?php 		
						if ($_POST['elimina']!='') {

							$posi=$_POST['elimina'];		  
							unset($_POST['dcoding'][$posi]);	
							unset($_POST['dncoding'][$posi]);	
							unset($_POST['dfuente'][$posi]);			 
							unset($_POST['dvalores'][$posi]);			  		 
							$_POST['dcoding']= array_values($_POST['dcoding']); 		 
							$_POST['dncoding']= array_values($_POST['dncoding']); 
							$_POST['dfuente']= array_values($_POST['dfuente']); 		 		 
							$_POST['dvalores']= array_values($_POST['dvalores']); 		 		 		 		 		 
						}	 
						if ($_POST['agregadet']=='1') {

							$_POST['dcoding'][]=$_POST['codingreso'];
							$_POST['dncoding'][]=$_POST['ningreso'];		 		
							$_POST['dvalores'][]=$_POST['valor'];
							$_POST['agregadet']=0;
							?>

							<script>
								//document.form2.cuenta.focus();	
								document.form2.codingreso.value="";
								document.form2.valor.value="";	
								document.form2.ningreso.value="";				
								document.form2.codingreso.select();
								document.form2.codingreso.focus();	
							</script>
							<?php
						}

		  				$_POST['totalc']=0;
						$co="saludo1a";
						$co2="saludo2";

		 				for ($x=0;$x<count($_POST['dcoding']);$x++) {

							echo"

								<input style='width:100%;'  name='dcoding[]' value='".$_POST['dcoding'][$x]."' type='hidden'  readonly>
								<input style='width:100%;' name='dncoding[]' value='".$_POST['dncoding'][$x]."' type='hidden'  readonly>
								<input name='dfuente[]' value='".$_POST['dfuente'][$x]."' type='hidden' style='width:100%;' readonly>
								<input name='dvalores[]' value='".$_POST['dvalores'][$x]."' type='hidden' style='width:100%;' readonly>

								<tr class='$co' onMouseOver=\"anterior=this.style.backgroundColor;this.style.backgroundColor='#40b3ff';\" onMouseOut=\"this.style.backgroundColor=anterior\" ondblclick=\"dirigetipoingreso('".$_POST['dcoding'][$x]."')\">

									<td>
										".$_POST['dcoding'][$x]."
									</td>

									<td>
										".$_POST['dncoding'][$x]."
									</td>
									
									<td>
										".$_POST['dfuente'][$x]."
									</td>

									<td style='text-align:right; padding-right:10px'>$ ".number_format($_POST['dvalores'][$x],2)."</td>

									<td>
									</td>
								</tr>";

							$aux=$co;
							$co=$co2;
							$co2=$aux;
							$_POST['totalc']=$_POST['totalc']+$_POST['dvalores'][$x];
							$_POST['totalcf']=number_format($_POST['totalc'],2);
		 				}
						
						$resultado = convertir($_POST['totalc']);
						$_POST['letras']=$resultado." Pesos";

		 				echo "
							<input style='width:100%;' name='totalcf' type='hidden' value='$_POST[totalcf]' readonly>
							<input style='width:100%;' name='totalc' type='hidden' value='$_POST[totalc]'>
							<input name='letras' type='hidden' value='$_POST[letras]' style='width:100%;'>
							<tr  class='$co'>
								<td colspan='3' style='font-weight:bold; font-size:14px'>Total:</td>
								<td style='text-align:right; padding-right:10px'>
									$ ".$_POST['totalcf']."
								</td>
								<td></td>
							</tr>
							<tr  class='$co2'>
								<td style='font-weight:bold; font-size:14px'>Son:</td>
								<td colspan='5'>
									".$_POST['letras']."
								</td>
							</tr>";
					?> 
	   			</table>
			</div>
		</form>
 		</td></tr></table>
	</body>
</html> 		