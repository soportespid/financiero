<?php
    header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");

	require 'comun.inc';
	require 'funciones.inc';

	session_start();
    date_default_timezone_set("America/Bogota");

?>


<html lang="es">
    <head>
        <meta charset="utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1"/>
		<meta name="viewport" content="user-scalable=no">
        <title>:: IDEAL 10 - Presupuesto</title>
        <link href="favicon.ico" rel="shortcut icon"/>
        <link href="css/css2.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
        <link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
        <link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
        <script type="text/javascript" src="css/programas.js"></script>
		<script type="text/javascript" src="css/calendario.js"></script>
		<script type="text/javascript" src="JQuery/jquery-2.1.4.min.js"></script>
        <link href="css/style.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />

        <script>
            function cambiaFecha(){
                var fecha = document.getElementById('fecha_cdp').value;
                fecArray = fecha.split('-');
                document.getElementById('vigencia').value = fecArray[0];
            }
        </script>

        <style>


            [v-cloak]{
                display : none;
            }

        </style>

    </head>
    <body>
        <IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
        <span id="todastablas2"></span>
        <div>
            <div id="myapp">
                <div id="cargando" v-if="loading" class="loading">
					<span>Cargando...</span>
				</div>
                <div>
                    <table>
                        <tr><script>barra_imagenes("ccpet");</script><?php cuadro_titulos();?></tr>
                        <tr><?php menu_desplegable("ccpet");?></tr>
                    </table>
                    <div class="bg-white group-btn p-1" id="newNavStyle">
                        <button type="button" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center" @click="window.location.href='ccp-solicitudcdp.php'">
                            <span>Nuevo</span>
                            <svg  xmlns="http://www.w3.org/2000/svg"  viewBox="0 -960 960 960" ><path d="M440-440H200v-80h240v-240h80v240h240v80H520v240h-80v-240Z"/></svg>
                        </button>
                        <button type="button" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center" v-on:Click="guardarCdp">
                            <span>Guardar</span>
                            <svg xmlns="http://www.w3.org/2000/svg"  viewBox="0 -960 960 960" ><path d="M840-680v480q0 33-23.5 56.5T760-120H200q-33 0-56.5-23.5T120-200v-560q0-33 23.5-56.5T200-840h480l160 160Zm-80 34L646-760H200v560h560v-446ZM480-240q50 0 85-35t35-85q0-50-35-85t-85-35q-50 0-85 35t-35 85q0 50 35 85t85 35ZM240-560h360v-160H240v160Zm-40-86v446-560 114Z"/></svg>
                        </button>
                        <button type="button" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center" @click="window.location.href='ccp-buscarcdp.php'">
                            <span>Buscar</span>
                            <svg class="fill-black group-hover:fill-white w-5 h-5" xmlns="http://www.w3.org/2000/svg"  viewBox="0 -960 960 960" ><path d="M784-120 532-372q-30 24-69 38t-83 14q-109 0-184.5-75.5T120-580q0-109 75.5-184.5T380-840q109 0 184.5 75.5T640-580q0 44-14 83t-38 69l252 252-56 56ZM380-400q75 0 127.5-52.5T560-580q0-75-52.5-127.5T380-760q-75 0-127.5 52.5T200-580q0 75 52.5 127.5T380-400Z"/></svg>
                        </button>
                        <button type="button" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center" @click="mypop=window.open('ccp-principal.php','',''); mypop.focus();">
                            <span>Nueva ventana</span>
                            <svg class="fill-black group-hover:fill-white w-5 h-5" xmlns="http://www.w3.org/2000/svg"  viewBox="0 -960 960 960" ><path d="M320-120v-80h80v-80H160q-33 0-56.5-23.5T80-360v-400q0-33 23.5-56.5T160-840h640q33 0 56.5 23.5T880-760v400q0 33-23.5 56.5T800-280H560v80h80v80H320ZM160-360h640v-400H160v400Zm0 0v-400 400Z"/></svg>
                        </button>
                        <button type="button" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center" @click="mypop=window.open('ccp-solicitudcdp.php','','');mypop.focus();">
                            <span class="group-hover:text-white">Duplicar pantalla</span>
                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960"><path d="M640-160v-360H160v360h480Zm80-200v-80h80v-360H320v200h-80v-200q0-33 23.5-56.5T320-880h480q33 0 56.5 23.5T880-800v360q0 33-23.5 56.5T800-360h-80ZM160-80q-33 0-56.5-23.5T80-160v-360q0-33 23.5-56.5T160-600h480q33 0 56.5 23.5T720-520v360q0 33-23.5 56.5T640-80H160Zm400-603ZM400-340Z"></path></svg>
                        </button>
                        <button type="button" class="btn btn-success d-flex justify-between align-items-center" @click="window.location.href='ccp-gestioncdp'">
                            <span>Atrás</span>
                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960"><path d="M760-200v-160q0-50-35-85t-85-35H273l144 144-57 56-240-240 240-240 57 56-144 144h367q83 0 141.5 58.5T840-360v160h-80Z"/></svg>
                        </button>
                    </div>
                </div>
                <h2 class="titulos m-0">Certificado de Disponibilidad Presupuestal</h2>
                <div class="d-flex w-50">
                    <div class="form-control flex-row align-items-center">
                        <label class="form-label m-0 w-25" for="">Tipo de movimiento:</label>
                        <select v-model="tipoMovimiento"  v-on:change="seleccionarFormulario()">
                            <option v-for="tipoDeMovimiento in tiposDeMovimiento" v-bind:value = "tipoDeMovimiento[0]">
                                {{ tipoDeMovimiento[0] }} - {{ tipoDeMovimiento[1] }}
                            </option>
                        </select>
                    </div>
                </div>
                <div v-show="mostrarCrearCdp">
                    <div class="d-flex">
                        <div class="form-control w-15">
                            <label class="form-label m-0" for="">Solicitud: <span class="text-danger fw-bolder">*</span></label>
                            <input type="text" class="bg-warning cursor-pointer text-center" @dblclick="cargarSolicitudes()"   v-model="solicitud" readonly>
                            <input type="hidden" id="consecutivo"  name="consecutivo" class="text-center"  v-model="consecutivo" readonly disabled>
                        </div>
                        <div class="form-control w-25">
                            <label class="form-label m-0" for="">Fecha solicitud<span class="text-danger fw-bolder">*</span></label>
                            <input type="date" class="text-center" v-model="fechaSolicitud" readonly>
                        </div>

                        <div class="form-control w-25">
                            <label class="form-label m-0" for="">Fecha de vencimiento</label>
                            <input type="date" class="text-center" v-model="fechaVencimiento" readonly>
                        </div>

                        <div class="form-control">
                            <label class="form-label m-0" for="">Area solicitante</label>
                            <input type="text" v-model="area_Solicitante" readonly>
                        </div>
                    </div>
                    <div class="d-flex">
                        <div class="form-control w-15">
                            <label class="form-label m-0" for="">Fecha Cdp<span class="text-danger fw-bolder">*</span></label>
                            <input type="date" class="text-center" id="fecha_cdp" v-model="fecha_cdp" onchange="cambiaFecha();">
                            <!-- <input type="text" id="fecha" name="fecha" class="bg-warning cursor-pointer" onClick="displayCalendarFor('fecha');" title="DD/MM/YYYY" placeholder="DD/MM/YYYY" onchange="cambiaFecha();"> -->
                        </div>
                        <div class="form-control">
                            <label class="form-label m-0" for="">Solicita</label>
                            <input type="text" id="solicita" name="solicita" v-model = "solicita">
                            <input type="hidden" id="vigencia" name="vigencia" readonly disabled>
                        </div>

                    </div>
                    <div class="d-flex">
                        <div class="form-control">
                            <label class="form-label m-0" for="">Objeto:</label>
                            <textarea class="resize-vertical" id="objeto" name="objeto" rows="3" v-model="objeto" style="max-height:100px"></textarea>
                        </div>
                    </div>
                    <h2 class="titulos m-0">Cuenta CCPET y clasificadores complementarios</h2>
                    <!-- <div class="d-flex">
                        <div class="d-flex w-50">
                            <div class="form-control flex-row align-items-center">
                                <label class="form-label m-0 w-15" for="">Rubro:</label>
                                <input placeholder="Programático-BPIM-fuente-Dependencia-vigencia del gasto-medio de pago" type="text" id="rubroPresupuestal" name="rubroPresupuestal"  v-on:change = "buscarRubroPresupuestal"  v-model="rubroPresupuestal" autocomplete="off">
                            </div>
                            <div class="form-control flex-row align-items-center">
                                <label class="form-label m-0" for="">Tipo de Gasto:</label>
                                <select v-model="tipoGasto" v-on:change="cambiaTipoDeGasto">
                                    <option v-for="tipoDeGasto in tiposDeGasto" v-bind:value = "tipoDeGasto[0]">{{ tipoDeGasto[1] }}</option>
                                </select>
                            </div>
                        </div>
                        <div class="d-flex w-50">
                            <div class="form-control flex-row align-items-center">
                                <label class="form-label m-0 w-100" for="">Dependencia:</label>
                                <select v-model="selectUnidadEjecutora" v-on:change="parametrosCompletos">
                                    <option v-for="unidad in unidadesejecutoras" v-bind:value="unidad[0]">
                                        {{ unidad[0] }} - {{ unidad[1] }}
                                    </option>
                                </select>
                            </div>
                            <div class="form-control flex-row align-items-center">
                                <label class="form-label m-0 w-50" for="">Medio pago:</label>
                                <select v-model="selectMedioPago" v-on:change="parametrosCompletos">
                                    <option v-for="option in optionsMediosPagos" v-bind:value="option.value">
                                        {{ option.text }}
                                    </option>
                                </select>
                            </div>
                            <div class="form-control flex-row align-items-center">
                                <label class="form-label m-0 w-50" for="">Vig. del gasto:</label>
                                <select v-model="selectVigenciaGasto" v-on:change="parametrosCompletos">
                                    <option v-for="vigenciaDeGasto in vigenciasdelgasto" v-bind:value="vigenciaDeGasto[0]">
                                        {{ vigenciaDeGasto[1] }} - {{ vigenciaDeGasto[2] }}
                                    </option>
                                </select>
                            </div>
                        </div>
                    </div> -->
                    <!-- <div class="d-flex" v-if = "mostrarFuncionamiento">
                        <div class="d-flex w-50">
                            <div class="form-control flex-row align-items-center">
                                <label class="form-label m-0 w-15" for="">Cuenta:</label>
                                <div class="d-flex w-100">
                                    <input type="text" id="cuenta" name="cuenta" class="bg-warning w-25 cursor-pointer" v-on:dblclick="ventanaCuenta" v-on:change = "buscarCta"  v-model="cuenta" autocomplete="off">
                                    <input type="text" id="nombreCuenta" name="nombreCuenta"  v-model='nombreCuenta' readonly disabled>
                                </div>
                            </div>
                        </div>
                        <div class="d-flex w-50">
                            <div class="form-control flex-row align-items-center">
                                <label class="form-label m-0 w-25" for="">Fuente:</label>
                                <div class="d-flex w-100">
                                    <input type="text" id="fuente" name="fuente" class="bg-warning w-25 cursor-pointer" v-on:dblclick="ventanaFuente" v-on:change = "buscarFuente" v-model="fuente"  autocomplete="off">
                                    <input type="text" id="nombreFuente" name="nombreFuente"  v-model='nombreFuente' readonly disabled>
                                </div>
                            </div>
                            <div class="form-control flex-row align-items-center w-25">
                                <label class="form-label m-0" for="">Valor:</label>
                                <input type="text" id="valor" name="valor" v-model="valor">
                            </div>
                            <div class="form-control flex-row align-items-center w-25">
                                <label class="form-label m-0" for="">Saldo:</label>
                                <input type="hidden" id="saldo" name="saldo" v-model="saldo">
                                <input type="text" :value="formatonumero(saldo)" disabled>
                            </div>
                        </div>
                    </div>
                    <div v-if = "mostrarInversion" class="d-flex">
                        <div class="w-100">
                            <div class="d-flex">
                                <div class="form-control flex-row align-items-center">
                                    <label class="form-label m-0 w-15" for="">Proyecto:</label>
                                    <div class="d-flex w-100">
                                        <input type="text" id="codProyecto" name="codProyecto"  class="bg-warning w-25 cursor-pointer" v-on:dblclick="ventanaProyecto" v-on:change = "buscarProyecto"  v-model="codProyecto" autocomplete="off" >
                                        <input type="text" id="nombreProyecto" name="nombreProyecto"  v-model='nombreProyecto' disabled>
                                    </div>
                                </div>
                            </div>
                            <div class="d-flex">
                                <div class="form-control flex-row align-items-center">
                                    <label class="form-label m-0 w-25" for="">Fuente:</label>
                                    <div class="d-flex w-100">
                                        <input type="text" id="fuente" name="fuente" class="bg-warning w-25 cursor-pointer" v-on:dblclick="ventanaFuente" v-on:change = "buscarFuente" v-model="fuente"  autocomplete="off">
                                        <input type="text" id="nombreFuente" name="nombreFuente"  v-model='nombreFuente' readonly disabled>
                                    </div>
                                </div>
                                <div class="form-control flex-row align-items-center w-25">
                                    <label class="form-label m-0" for="">Valor:</label>
                                    <input type="text" id="valor" name="valor" v-model="valor">
                                </div>
                                <div class="form-control flex-row align-items-center w-25">
                                    <label class="form-label m-0" for="">Saldo:</label>
                                    <input type="hidden" id="saldo" name="saldo" v-model="saldo">
                                    <input type="text" :value="formatonumero(saldo)" disabled>
                                </div>
                            </div>
                        </div>
                        <div class="w-100">
                            <div class="d-flex">
                                <div class="form-control flex-row align-items-center">
                                    <label class="form-label m-0 w-15" for="">Programatico:</label>
                                    <div class="d-flex w-100">
                                        <input type="text" id="programatico" name="programatico"  class="bg-warning w-25 cursor-pointer" v-on:dblclick="ventanaProgramatico" v-on:change = "buscarProgramatico"  v-model="programatico" autocomplete="off" >
                                        <input type="text" id="nombreProgramatico" name="nombreProgramatico" v-model='nombreProgramatico' disabled>
                                    </div>
                                </div>
                            </div>
                            <div class="d-flex">
                                <div class="form-control flex-row align-items-center">
                                    <label class="form-label m-0 w-15" for="">Cuenta:</label>
                                    <div class="d-flex w-100">
                                        <input type="text" id="cuenta" name="cuenta" class="bg-warning w-25 cursor-pointer" v-on:dblclick="ventanaCuenta" v-on:change = "buscarCta"  v-model="cuenta" autocomplete="off">
                                        <input type="text" id="nombreCuenta" name="nombreCuenta"  v-model='nombreCuenta' readonly disabled>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                    <span class="ms-2 text-primary fw-bold">Los siguientes clasificadores no afectan el saldo:</span>
                    <div class="d-flex">
                        <div class="d-flex w-50">
                            <div class="form-control flex-row align-items-center" v-if = "cuentaConCpc != ''  && mostrarFuncionamiento || cuentaConCpc != ''  && mostrarInversion">
                                <label class="form-label m-0" for="">CPC:</label>
                                <div class="d-flex w-100">
                                    <input type="text" id="codigoCpc" name="codigoCpc" class="bg-warning w-25 cursor-pointer" v-on:dblclick="ventanaCodigoCpc" v-on:change = "buscarCodigoCpc" v-model="codigoCpc"  autocomplete="off">
                                    <input type="text" id="nombreCodigoCpc" name="nombreCodigoCpc"  v-model='nombreCodigoCpc'  disabled>
                                </div>
                            </div>
                            <div class="form-control flex-row align-items-center">
                                <label class="form-label m-0 w-15" for="">DIVIPOLA:</label>
                                <div class="d-flex w-100">
                                    <input type="text" id="divipola" name="divipola"  class="bg-warning w-25 cursor-pointer" v-on:dblclick="ventanaDivipola" v-on:change = "buscarDivipola"  v-model="divipola" autocomplete="off">
                                    <input type="text" id="nombreDivipola" name="nombreDivipola"  v-model='nombreDivipola'  disabled>
                                </div>
                            </div>
                        </div>
                        <div class="d-flex w-50">
                            <div class="form-control flex-row align-items-center">
                                <label class="form-label m-0 w-15" for="">CHIP:</label>
                                <div class="d-flex w-100">
                                    <input type="text" id="chip" name="chip"  class="bg-warning w-25 cursor-pointer" v-on:dblclick="ventanaChip" v-on:change = "buscarChip" v-model="chip"  autocomplete="off">
                                    <input type="text" id="nombreChip" name="nombreChip"  v-model='nombreChip' disabled>
                                    <button type="button" class="btn btn-primary btn-sm" v-on:click="agregarDetalle">Agregar</button>
                                </div>
                            </div>
                        </div>
                     </div> -->
                     <div class="table-responsive" style="max-height:25vh">
                        <table  class="table fw-normal">
                            <thead>
                                <tr class="text-center">
                                    <th>Sector</th>
                                    <th>Programa</th>
                                    <th>SubPrograma</th>
                                    <th>Producto</th>
                                    <th>Indicador Producto</th>
                                    <th>Proyecto</th>
                                    <th>Cuenta CCPET</th>
                                    <th>Fuente</th>
                                    <th>CPC</th>
                                    <th>Medio pago</th>
                                    <th>Vig. del gasto</th>
                                    <th>Valor</th>
                                    <!-- <th>Quitar</th> -->
                                </tr>
                            </thead>
                            <tbody>
                                <tr v-for="detalle in detalles"  class='<?php echo $co; ?>' style='text-rendering: optimizeLegibility; height: 10px; cursor: pointer !important; style=\"cursor: hand\";'>
                                    <td>{{ buscaSector(detalle[8])  }}</td>
                                    <td>{{ buscaPrograma(detalle[8]) }}</td>
                                    <td>{{ buscaSubPrograma(detalle[8]) }}</td>
                                    <td>{{ buscaProducto(detalle[8]) }}</td>
                                    <td>{{ detalle[8] }} - {{ detalle[9] }}</td>
                                    <td>{{ detalle[6] }} - {{ detalle[7] }}</td>
                                    <td>{{ detalle[10] }} - {{ detalle[11] }}</td>
                                    <td>{{ detalle[12] }}</td>
                                    <td>{{ detalle[14] }}</td>
                                    <td>{{ detalle[3] }}</td>
                                    <td>{{ detalle[4] }} - {{ detalle[5] }}</td>
                                    <td>{{ formatonumero(detalle[18]) }}</td>
                                    <!-- <td>
                                        <button type="button" class="btn btn-danger" v-on:click="eliminarDetalle(detalle)">Eliminar</button>
                                    </td> -->
                                </tr>
                            </tbody>
                            <tfoot>
                                <tr class='saludo1'>
                                    <td colspan = '11'  class='text-right'>TOTAL:</td>
                                    <td align="right">  {{ formatonumero(totalCdp) }}</td>
                                    <td></td>
                                </tr>
                            </tfoot>
                        </table>
                     </div>
                </div>
                <!-- MODALES -->
                
                <div v-show="mostrarCrearCdp ">
                    <div v-show="showModal_cuentas" class="modal">
                        <div class="modal-dialog modal-lg" >
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title">Cuentas CCPET</h5>
                                    <button type="button" @click="showModal_cuentas = false" class="btn btn-close"><div></div><div></div></button>
                                </div>
                                <div class="modal-body">
                                    <div class="d-flex flex-column">
                                        <div class="form-control m-0 mb-3">
                                            <input type="search" placeholder="Buscar" v-on:keyup="searchMonitorCuenta" v-model="searchCuenta.keywordCuenta">
                                        </div>
                                    </div>
                                    <div class="overflow-auto max-vh-50 overflow-x-hidden " >
                                        <table class="table table-hover fw-normal">
                                            <thead>
                                                <tr>
                                                    <th>Codigo</th>
                                                    <th>Nombre</th>
                                                    <th>Tipo</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr v-for="cuenta in cuentasCcpet" v-on:click="seleccionarCuenta(cuenta)">
                                                    <td>{{cuenta[1]}}</td>
                                                    <td>{{cuenta[2]}}</td>
                                                    <td>{{cuenta[6]}}</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div v-show="showModal_proyectos" class="modal">
                        <div class="modal-dialog modal-lg" >
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title">Proyectos de inversion</h5>
                                    <button type="button" @click="showModal_proyectos = false" class="btn btn-close"><div></div><div></div></button>
                                </div>
                                <div class="modal-body">
                                    <div class="d-flex flex-column">
                                        <div class="form-control m-0 mb-3">
                                            <input type="search" placeholder="Buscar por nombre o BPIM" v-on:keyup="searchMonitorProyecto" v-model="searchProyecto.keywordProyecto">
                                        </div>
                                    </div>
                                    <div class="overflow-auto max-vh-50 overflow-x-hidden " >
                                        <table class="table table-hover fw-normal">
                                            <thead>
                                                <tr>
                                                    <th>Codigo</th>
                                                    <th>Nombre</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr v-for="proyecto in proyectos" v-on:click="seleccionarProyecto(proyecto)">
                                                    <td>{{proyecto[2]}}</td>
                                                    <td>{{proyecto[4]}}</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div v-show="showModal_programatico" class="modal">
                        <div class="modal-dialog modal-lg" >
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title">Programatico</h5>
                                    <button type="button" @click="showModal_programatico = false" class="btn btn-close"><div></div><div></div></button>
                                </div>
                                <div class="modal-body">
                                    <div class="overflow-auto max-vh-50 overflow-x-hidden " >
                                        <table class="table table-hover fw-normal">
                                            <thead>
                                                <tr>
                                                    <th>Codigo</th>
                                                    <th>Nombre</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr v-for="programatico in programaticos" v-on:click="seleccionarProgramatico(programatico)">
                                                    <td>{{programatico[0]}}</td>
                                                    <td>{{programatico[1]}}</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div v-show="showModal_solicitud" class="modal">
                        <div class="modal-dialog modal-lg" >
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title">Solicitud de CDP</h5>
                                    <button type="button" @click="showModal_solicitud = false" class="btn btn-close"><div></div><div></div></button>
                                </div>
                                <div class="modal-body">
                                    <div class="d-flex flex-column">
                                        <div class="form-control m-0 mb-3">
                                            <input type="search" v-model="txtSearch" @keyup="searchData()" placeholder="Busca por Objeto o numero de solicitud">
                                        </div>
                                    </div>
                                    <div class="overflow-auto max-vh-50 overflow-x-hidden " >
                                        <table class="table table-hover fw-normal">
                                            <thead>
                                                <tr>
                                                    <th>Solicitud</th>
                                                    <th>Fecha de solicitud</th>
                                                    <th>Fecha de vencimiento</th>
                                                    <th>Area solicitante</th>
                                                    <th>Objeto</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr v-for="solicitud in solicitudesCopy" v-on:click="seleccionarSolicitud(solicitud)">
                                                    <td>{{solicitud[0]}}</td>
                                                    <td>{{solicitud[3]}}</td>
                                                    <td>{{solicitud[4]}}</td>
                                                    <td>{{ areaSolicitante(solicitud[5]) }}</td>
                                                    <td>{{solicitud[14]}}</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div v-show="showModal_fuentes" class="modal">
                        <div class="modal-dialog modal-lg" >
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title">Fuentes CUIPO</h5>
                                    <button type="button" @click="showModal_fuentes = false" class="btn btn-close"><div></div><div></div></button>
                                </div>
                                <div class="modal-body">
                                <div class="d-flex flex-column">
                                        <div class="form-control m-0 mb-3">
                                            <input type="search" placeholder="Buscar por nombre o codigo" v-on:keyup="searchMonitorFuente" v-model="searchFuente.keywordFuente">
                                        </div>
                                    </div>
                                    <div class="overflow-auto max-vh-50 overflow-x-hidden " >
                                        <table class="table table-hover fw-normal">
                                            <thead>
                                                <tr>
                                                    <th>Codigo</th>
                                                    <th>Nombre</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr v-for="fuente in fuentesCuipo" v-on:click="seleccionarFuente(fuente)">
                                                    <td>{{fuente[0]}}</td>
                                                    <td>{{fuente[1]}}</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div v-show="showModal_divipola" class="modal">
                        <div class="modal-dialog modal-lg" >
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title">DIVIPOLA</h5>
                                    <button type="button" @click="showModal_divipola = false" class="btn btn-close"><div></div><div></div></button>
                                </div>
                                <div class="modal-body">
                                <div class="d-flex flex-column">
                                        <div class="form-control m-0 mb-3">
                                            <input type="search" placeholder="Buscar por nombre o codigo" v-on:keyup="searchMonitorDivipola" v-model="searchDivipola.keywordDivipola">
                                        </div>
                                    </div>
                                    <div class="overflow-auto max-vh-50 overflow-x-hidden " >
                                        <table class="table table-hover fw-normal">
                                            <thead>
                                                <tr>
                                                    <th>Codigo</th>
                                                    <th>Nombre</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr v-for="divipola in divipolas" v-on:click="seleccionarDivipola(divipola)">
                                                    <td>{{divipola[0]}}</td>
                                                    <td>{{divipola[1]}}</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div v-show="showModal_chips" class="modal">
                        <div class="modal-dialog modal-lg" >
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title">CHIPS</h5>
                                    <button type="button" @click="showModal_chips = false" class="btn btn-close"><div></div><div></div></button>
                                </div>
                                <div class="modal-body">
                                <div class="d-flex flex-column">
                                        <div class="form-control m-0 mb-3">
                                            <input type="search" placeholder="Buscar por nombre o codigo"  v-on:keyup="searchMonitorChip" v-model="searchChip.keywordChip">
                                        </div>
                                    </div>
                                    <div class="overflow-auto max-vh-50 overflow-x-hidden " >
                                        <table class="table table-hover fw-normal">
                                            <thead>
                                                <tr>
                                                    <th>Codigo</th>
                                                    <th>Nombre</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr v-for="chip in chips" v-on:click="seleccionarChip(chip)">
                                                    <td>{{chip[0]}}</td>
                                                    <td>{{chip[1]}}</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div v-show="showModal_bienes_transportables">
                        <transition name="modal">
                            <div class="modal-mask">
                                <div class="modal-wrapper">
                                    <div class="modal-dialog modal-lg" style = "max-width: 1200px !important;" role="document">
                                        <div class="modal-content"  style = "width: 100% !important;" scrollable>
                                            <span id="start_page"> </span>
                                            <div class="modal-header">
                                                <div class="row">
                                                    <div class="col-12">
                                                        <h5 class="modal-title">Clasificador Bienes transportables Sec. 0-4</h5>
                                                    </div>
                                                </div>
                                                <button type="button" @click="showModal_bienes_transportables = false" class="btn btn-close"><div></div><div></div></button>
                                            </div>
                                            <div class="modal-body" style = "height: 400px; overflow: scroll; overflow-x: hidden;">
                                                <div v-show="mostrarDivision">
                                                    <div class="row" style="margin: 2px 0 0 0; border-radius: 5px !important; border-radius:4px; background-color: #E1E2E2; ">
                                                        <div class="col-md-3" style="padding: 12px 0px 0px 30px; font: 140% sans-serif;">
                                                            <label for="">Buscar Producto:</label>
                                                        </div>

                                                        <div class="col-md-6" style="padding: 4px">
                                                            <input type="text" class="form-control" style="height: auto; border-radius:2px;" placeholder="Ej: 1800001" v-on:keyup.enter="buscarGeneral"  v-model="searchGeneral.keywordGeneral">
                                                        </div>
                                                        <div class="col-md-2 col-sm-4 col-md-offset-1" style="padding: 4px">
                                                            <button type="submit" class="btn btn-dark" value="Buscar" style="height: auto; border-radius:5px;" v-on:click="buscarGeneral">Buscar</button>
                                                        </div>
                                                    </div>
                                                    <div style="margin: 2px 0 0 0; border-radius: 0 0 0 6px; height: 200px; overflow: scroll; overflow-x: hidden; background: white; ">
                                                        <table class='inicio inicio--no-shadow'>
                                                            <tbody>
                                                                <?php
                                                                    $co ='zebra1';
                                                                    $co2='zebra2';
                                                                ?>
                                                                <tr v-for="division in divisiones" v-on:click="buscarGrupo(division)" v-bind:class="division[0] === division_p ? 'background_active_color' : ''" class='<?php echo $co; ?>' style='text-rendering: optimizeLegibility; cursor: pointer !important; style=\"cursor: hand\"'>
                                                                    <td width="20%" style="font: 120% sans-serif; padding-left:10px">{{ division[0] }}</td>
                                                                    <td width="80%" style="font: 120% sans-serif; padding-left:10px">{{ division[1] }}</td>

                                                                    <?php
                                                                    $aux=$co;
                                                                    $co=$co2;
                                                                    $co2=$aux;
                                                                    ?>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                                <div v-show="mostrarGrupo">
                                                    <div style="margin: 2px 0 0 0;">
                                                        <div class="row" style="margin: 4px; border-radius:4px; background-color: #E1E2E2; ">
                                                            <div class="col-md-2" style="padding: 12px 0px 0px 30px; font: 140% sans-serif;">
                                                                <label for="">Grupos:</label>
                                                            </div>

                                                            <div class="col-md-6 col-md-offset-6" style="padding: 4px">
                                                                <input type="text" class="form-control" placeholder="Buscar por nombre o c&oacute;digo de grupo" v-on:keyup="searchMonitorGrupos" v-model="searchGrupo.keywordGrupo">
                                                            </div>
                                                        </div>
                                                        <table>
                                                            <thead>
                                                                <tr>
                                                                    <td width="20%" class='titulos' style="font: 120% sans-serif; padding-left:10px; border-radius: 5px 0 0 0;">C&oacute;digo</td>
                                                                    <td width="80%" class='titulos' style="font: 120% sans-serif; ">Nombre</td>
                                                                </tr>
                                                            </thead>
                                                        </table>
                                                    </div>
                                                    <div style="margin: 2px 0 0 0; border-radius: 0 0 0 6px; height: 200px; overflow: scroll; overflow-x: hidden; background: white; ">
                                                        <table class='inicio inicio--no-shadow'>
                                                            <tbody>
                                                                <?php
                                                                    $co ='zebra1';
                                                                    $co2='zebra2';
                                                                ?>
                                                                <tr v-for="grupo in grupos" v-on:click="buscarClase(grupo)"  v-bind:class="grupo[0] === grupo_p ? 'background_active_color' : ''" class='<?php echo $co; ?>' style='text-rendering: optimizeLegibility; cursor: pointer !important; style=\"cursor: hand\"'>
                                                                    <td width="20%" style="font: 120% sans-serif; padding-left:10px">{{ grupo[0] }}</td>
                                                                    <td width="80%" style="font: 120% sans-serif; padding-left:10px">{{ grupo[1] }}</td>

                                                                    <?php
                                                                    $aux=$co;
                                                                    $co=$co2;
                                                                    $co2=$aux;
                                                                    ?>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                                <div v-show="mostrarClase">
                                                    <div style="margin: 2px 0 0 0;">
                                                        <div class="row" style="margin: 4px; border-radius:4px; background-color: #E1E2E2; ">
                                                            <div class="col-md-2" style="padding: 12px 0px 0px 30px; font: 140% sans-serif;">
                                                                <label for="">Clases:</label>
                                                            </div>

                                                            <div class="col-md-6 col-md-offset-6" style="padding: 4px">
                                                                <input type="text" class="form-control" placeholder="Buscar por nombre o c&oacute;digo de clase" v-on:keyup="searchMonitorClases" v-model="searchClase.keywordClase">
                                                            </div>
                                                        </div>
                                                        <table>
                                                            <thead>
                                                                <tr>
                                                                    <td width="20%" class='titulos' style="font: 120% sans-serif; padding-left:10px; border-radius: 5px 0 0 0;">C&oacute;digo</td>
                                                                    <td width="80%" class='titulos' style="font: 120% sans-serif; ">Nombre</td>
                                                                </tr>
                                                            </thead>
                                                        </table>
                                                    </div>
                                                    <div style="margin: 2px 0 0 0; border-radius: 0 0 0 6px; height: 200px; overflow: scroll; overflow-x: hidden; background: white; ">
                                                        <table class='inicio inicio--no-shadow'>
                                                            <tbody>
                                                                <?php
                                                                    $co ='zebra1';
                                                                    $co2='zebra2';
                                                                ?>
                                                                <tr v-for="clase in clases" v-on:click="buscarSubclase(clase)"  v-bind:class="clase[0] === clase_p ? 'background_active_color' : ''" class='<?php echo $co; ?>' style='text-rendering: optimizeLegibility; cursor: pointer !important; style=\"cursor: hand\"'>
                                                                    <td width="20%" style="font: 120% sans-serif; padding-left:10px">{{ clase[0] }}</td>
                                                                    <td width="80%" style="font: 120% sans-serif; padding-left:10px">{{ clase[1] }}</td>

                                                                    <?php
                                                                    $aux=$co;
                                                                    $co=$co2;
                                                                    $co2=$aux;
                                                                    ?>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                                <div v-show="mostrarSubClase">
                                                    <div style="margin: 2px 0 0 0;">
                                                        <div class="row" style="margin: 4px; border-radius:4px; background-color: #E1E2E2; ">
                                                            <div class="col-md-2" style="padding: 12px 0px 0px 30px; font: 140% sans-serif;">
                                                                <label for="">Subclase:</label>
                                                            </div>

                                                            <div class="col-md-6 col-md-offset-6" style="padding: 4px">
                                                                <input type="text" class="form-control" placeholder="Buscar por nombre o c&oacute;digo de subclase" v-on:keyup="searchMonitorSubClases" v-model="searchSubClase.keywordSubClase">
                                                            </div>
                                                        </div>
                                                        <table>
                                                            <thead>
                                                                <tr>
                                                                    <td width="20%" class='titulos' style="font: 120% sans-serif; padding-left:10px; border-radius: 5px 0 0 0;">C&oacute;digo</td>
                                                                    <td width="30%" class='titulos' style="font: 120% sans-serif; ">Nombre</td>
                                                                    <td width="15%" class='titulos' style="font: 120% sans-serif; ">CIIU Rev. 4 A.C. </td>
                                                                    <td width="25%" class='titulos' style="font: 120% sans-serif; ">Sistema Armonizado 2012</td>
                                                                    <td width="10%" class='titulos' style="font: 120% sans-serif; ">CPC 2 A.C.</td>
                                                                </tr>
                                                            </thead>
                                                        </table>
                                                    </div>
                                                    <div style="margin: 2px 0 0 0; border-radius: 0 0 0 6px; height: 200px; overflow: scroll; overflow-x: hidden; background: white; ">
                                                        <table class='inicio inicio--no-shadow'>
                                                            <tbody>
                                                                <?php
                                                                    $co ='zebra1';
                                                                    $co2='zebra2';
                                                                ?>
                                                                <tr v-for="subclase in subClases" v-on:click="seleccionarSublaseProducto(subclase)"  v-bind:class="subclase[0] === subClase_p ? 'background_active_color' : ''" class='<?php echo $co; ?>' style='text-rendering: optimizeLegibility; cursor: pointer !important; style=\"cursor: hand\"'>
                                                                    <td width="20%" style="font: 120% sans-serif; padding-left:10px">{{ subclase[0] }}</td>
                                                                    <td width="30%" style="font: 120% sans-serif; padding-left:10px">{{ subclase[1] }}</td>
                                                                    <td width="15%" style="font: 120% sans-serif; padding-left:10px">{{ subclase[2] }}</td>
                                                                    <td width="25%" style="font: 120% sans-serif; padding-left:10px">{{ subclase[3] }}</td>
                                                                    <td width="10%" style="font: 120% sans-serif; padding-left:10px">{{ subclase[4] }}</td>

                                                                    <?php
                                                                    $aux=$co;
                                                                    $co=$co2;
                                                                    $co2=$aux;
                                                                    ?>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                                <div v-show="mostrarSubClaseProducto">
                                                    <div style="margin: 2px 0 0 0;">
                                                        <div class="row" style="margin: 4px; border-radius:4px; background-color: #E1E2E2; ">
                                                            <div class="col-md-2" style="padding: 12px 0px 0px 30px; font: 140% sans-serif;">
                                                                <label for="">Producto:</label>
                                                            </div>
                                                        </div>
                                                        <table>
                                                            <thead>
                                                                <tr>
                                                                    <td width="20%" class='titulos' style="font: 120% sans-serif; padding-left:10px; border-radius: 5px 0 0 0;">Subclase</td>
                                                                    <td width="60%" class='titulos' style="font: 120% sans-serif; ">Titulo</td>
                                                                    <td width="20%" class='titulos' style="font: 120% sans-serif; ">Ud </td>
                                                                </tr>
                                                            </thead>
                                                        </table>
                                                    </div>
                                                    <div style="margin: 2px 0 0 0; border-radius: 0 0 0 6px; height: 100px; overflow: scroll; overflow-x: hidden; background: white; ">
                                                        <table class='inicio inicio--no-shadow'>
                                                            <tbody>
                                                                <tr v-for="subclase in subClases_captura" v-on:click="seleccionarBienes(subclase)"
                                                                v-on:dblclick="seleccionarCpc"
                                                                v-bind:class="subclase[0] === subClase_p ? 'background_active_color' : ''" style='text-rendering: optimizeLegibility; cursor: pointer !important; cursor: hand;'>
                                                                    <td width="20%" style="font: 120% sans-serif; padding-left:10px">{{ subclase[0] }}</td>
                                                                    <td width="60%" style="font: 120% sans-serif; padding-left:10px">{{ subclase[1] }}</td>
                                                                    <td width="20%" style="font: 120% sans-serif; padding-left:10px">{{ subclase[2] }}</td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                                <span id="end_page"> </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </transition>
                    </div>
                    <div v-show="showModal_servicios">
                        <transition name="modal">
                            <div class="modal-mask">
                                <div class="modal-wrapper">
                                    <div class="modal-dialog modal-lg" style = "max-width: 1200px !important;" role="document">
                                        <div class="modal-content"  style = "width: 100% !important;" scrollable>
                                            <div class="modal-header">
                                                <div class="row">
                                                    <div class="col-12">
                                                        <h5 class="modal-title">Clasificador Servicios Sec. 5-9</h5>
                                                    </div>
                                                </div>
                                                <button type="button" @click="showModal_servicios = false" class="btn btn-close"><div></div><div></div></button>
                                            </div>
                                            <div class="modal-body" style = "height: 400px; overflow: scroll; overflow-x: hidden;">

                                                <div>
                                                    <div style="margin: 2px 0 0 0;">
                                                        <div class="row" style="margin: 4px; border-radius:4px; background-color: #E1E2E2; ">
                                                            <div class="col-md-2" style="padding: 12px 0px 0px 30px; font: 140% sans-serif;">
                                                                <label for="">Subclase:</label>
                                                            </div>

                                                            <div class="col-md-6 col-md-offset-6" style="padding: 4px">
                                                                <input type="text" class="form-control" placeholder="Buscar por nombre o c&oacute;digo de subclase" v-on:keyup="searchMonitorSubClasesServicios" v-model="searchSubClase.keywordSubClase">
                                                            </div>
                                                        </div>
                                                        <table>
                                                            <thead>
                                                                <tr>
                                                                    <td width="20%" class='titulos' style="font: 120% sans-serif; padding-left:10px; border-radius: 5px 0 0 0;">C&oacute;digo</td>
                                                                    <td width="30%" class='titulos' style="font: 120% sans-serif; ">Nombre</td>
                                                                    <td width="15%" class='titulos' style="font: 120% sans-serif; ">CIIU Rev. 4 A.C. </td>
                                                                    <td width="25%" class='titulos' style="font: 120% sans-serif; ">Sistema Armonizado 2012</td>
                                                                    <td width="10%" class='titulos' style="font: 120% sans-serif; ">CPC 2 A.C.</td>
                                                                </tr>
                                                            </thead>
                                                        </table>
                                                    </div>
                                                    <div style="margin: 2px 0 0 0; border-radius: 0 0 0 6px; height: 200px; overflow: scroll; overflow-x: hidden; background: white; ">
                                                        <table class='inicio inicio--no-shadow'>
                                                            <tbody>
                                                                <?php
                                                                    $co ='zebra1';
                                                                    $co2='zebra2';
                                                                ?>
                                                                <tr v-for="subclase in subClasesServicios" v-on:click="seleccionarServicios(subclase)"
                                                                v-on:dblclick="seleccionarServicioCpc"
                                                                v-bind:class="subclase[0] === subClaseServicios_p ? 'background_active_color' : ''" class='<?php echo $co; ?>' style='text-rendering: optimizeLegibility; cursor: pointer !important; style=\"cursor: hand\"'>
                                                                    <td width="20%" style="font: 120% sans-serif; padding-left:10px">{{ subclase[0] }}</td>
                                                                    <td width="30%" style="font: 120% sans-serif; padding-left:10px">{{ subclase[1] }}</td>
                                                                    <td width="15%" style="font: 120% sans-serif; padding-left:10px">{{ subclase[2] }}</td>
                                                                    <td width="25%" style="font: 120% sans-serif; padding-left:10px">{{ subclase[3] }}</td>
                                                                    <td width="10%" style="font: 120% sans-serif; padding-left:10px">{{ subclase[4] }}</td>

                                                                    <?php
                                                                    $aux=$co;
                                                                    $co=$co2;
                                                                    $co2=$aux;
                                                                    ?>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                                <span id="end_page_servicios"> </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </transition>
                    </div>
                </div>
                <div v-show = "mostrarReversionCdp">
                    <article>
                        <table class="inicio ancho">
                            <tr>
                                <td class="titulos" colspan="7" >Reversar documento CDP</td>
                                <td class="cerrar" style="width:7%" onClick="location.href='ccp-principal.php'">Cerrar</td>
                            </tr>

                            <tr>
                                <td class = "textoNewR"  style = "width: 8%;">
                                    <label class="labelR">
                                        Fecha Rev:
                                    </label>
                                </td>
                                <td  style = "width: 10%;">
                                    <input type="text" style = "width: 80%;" name="fechaRev" value="<?php echo $_POST['fechaRev']?>" onKeyUp="return tabular(event,this)" id="fc_1198971545" title="DD/MM/YYYY" onDblClick="displayCalendarFor('fc_1198971545');" class="colordobleclik" autocomplete="off" onChange="" readonly>
                                </td>

                                <td class="textoNewR" style = "width: 8%;">
                                    <label class="labelR">N° CDP:</label>
                                </td>
                                <td  style = "width: 10%;">
                                    <input type="text"  style = "width: 80%;" class="colordobleclik" v-model = "numCdpRev" @dblclick="ventanaCdpRev" @change="validarCdpRev">
                                </td>

                                <td class = "textoNewR"  style = "width: 8%;">
                                    <label class="labelR">
                                        Descripci&oacute;n:
                                    </label>
                                </td>
                                <td>
                                    <input type="text" style = "width: 100%;" v-model = "descripcionRev">
                                </td>
                            </tr>

                            <tr>
                                <td class="textoNewR">
                                    <label class="labelR">
                                        Fecha CDP:
                                    </label>
                                </td>
                                <td>
                                    <input type="text" v-model = "fechaCdp" style = "width: 80%" readonly>
                                </td>
                                <td class="textoNewR">
                                    <label class="labelR">
                                        Solicita:
                                    </label>
                                </td>
                                <td colspan="3">
                                    <input type="text" v-model = "solicitaCdp" style = "width: 100%;" readonly>
                                </td>
                            </tr>

                            <tr>
                                <td class="textoNewR">
                                    <label class="labelR">
                                        Objeto:
                                    </label>
                                </td>
                                <td colspan = "5">
                                    <input type="text" v-model = "objetoCdp" style = "width: 100%;" readonly>
                                </td>
                            </tr>
                        </table>

                        <div class='subpantalla' style='height:56vh; margin-top:0px; overflow:hidden'>
                            <table class='tablamvR' id="tableId">
                                <thead>
                                    <tr style="text-align:Center;">
                                        <th class="titulosnew00" style="width:7%;">Tipo de gasto</th>
                                        <th class="titulosnew00" style="width:8%;">Dependencia</th>
                                        <th class="titulosnew00" style="width:7%;">Medio pago</th>
                                        <th class="titulosnew00" style="width:7%;">Vig. del gasto</th>
                                        <th class="titulosnew00" style="width:7%;">Proyecto</th>
                                        <th class="titulosnew00" style="width:7%;">Programático</th>
                                        <th class="titulosnew00">Cuenta</th>
                                        <th class="titulosnew00" style="width:15%;">CPC</th>
                                        <th class="titulosnew00" style="width:8%;">Divipola</th>
                                        <th class="titulosnew00" style="width:8%;">CHIP</th>
                                        <th class="titulosnew00" style="width:8%;">Saldo</th>
                                        <th style="width:1%;"></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr v-for="(row,index) in detallesRev" :key="index" v-bind:class="[index % 2 ? 'contenidonew00' : 'contenidonew01']">
                                        <td style="width:7%;">{{ row[18] }}</td>
                                        <td style="width:8%;">{{ row[17] }}</td>
                                        <td style="width:7%;">{{ row[12] }}</td>
                                        <td style="width:7%;">{{ row[14] }}</td>
                                        <td style="width:7%;">{{ row[16] }}</td>
                                        <td style="width:7%;">{{ row[11] }}</td>
                                        <td>{{ row[3] }}</td>
                                        <td style="width:15%;">{{ row[4] }}</td>
                                        <td style="width:8%;">{{ row[19] }}</td>
                                        <td style="width:8%;">{{ row[20] }}</td>
                                        <td style="width:8%; text-align: right;">{{ formatonumero(valorRev[index]) }}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </article>

                    <div v-show="showModal_cdps_rev" class="modal">
                        <div class="modal-dialog modal-lg" >
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title">Cdps con saldo</h5>
                                    <button type="button" @click="showModal_cdps_rev = false" class="btn btn-close"><div></div><div></div></button>
                                </div>
                                <div class="modal-body">
                                    <div class="d-flex flex-column">
                                        <!-- <div class="form-control m-0 mb-3">
                                            <input type="search" placeholder="Buscar" v-on:keyup="searchMonitorCuenta" v-model="searchCuenta.keywordCuenta">
                                        </div> -->
                                    </div>
                                    <div class="overflow-auto max-vh-50 overflow-x-hidden " >
                                        <table class="table table-hover fw-normal">
                                            <thead>
                                                <tr>
                                                    <th>Codigo</th>
                                                    <th>Nombre</th>
                                                    <th>Tipo</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr v-for="cdp in cdps_rev" v-on:click="seleccionarCdpRev(cdp)" >
                                                    <td>{{cdp[0]}}</td>
                                                    <td>{{cdp[2]}}</td>
                                                    <td>{{ formatonumero(cdp[3]) }}</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- <div v-show="showModal_cdps_rev">
                        <transition name="modal">
                            <div class="modal-mask">
                                <div class="modal-wrapper">
                                    <div class="modal-dialog modal-lg" style = "max-width: 1200px !important;" role="document">
                                        <div class="modal-content"  style = "width: 1200px !important;" scrollable>
                                            <div class="modal-header">
                                                <h5 class="modal-title">Cdps con saldo</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true" @click="showModal_cdps_rev = false">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                <div style="margin: 2px 0 0 0;">
                                                    <div class="row" style="margin: 4px; border-radius:4px; background-color: #F6F6F6; ">
                                                        <!-- <div class="col-md-2" style="padding: 12px 0px 0px 30px; font: 140% sans-serif;">
                                                            <label for="">Cdp:</label>
                                                        </div>

                                                        <div class="col-md-6 col-md-offset-6" style="padding: 4px">
                                                            <input type="text" class="form-control" placeholder="Buscar por objeto o numero de CDP" style="font: sans-serif; " v-on:keyup="searchMonitorCdpRev" v-model="searchCdpRev.keywordCdpRev">
                                                        </div> 
                                                    </div>
                                                    <table>
                                                        <thead>
                                                            <tr>
                                                                <td width="10%" class='titulos' style="font: 120% sans-serif; padding-left:10px; border-radius: 5px 0 0 0;">CDP</td>
                                                                <td class='titulos' style="font: 120% sans-serif; ">Objeto</td>
                                                                <td width="15%" class='titulos' style="font: 120% sans-serif; ">Saldo</td>
                                                            </tr>
                                                        </thead>
                                                    </table>
                                                </div>
                                                <div style="margin: 2px 0 0 0; border-radius: 0 0 0 6px; height: 200px; overflow: scroll; overflow-x: hidden; background: white; ">
                                                    <table class='inicio inicio--no-shadow'>
                                                        <tbody>
                                                            <?php
                                                                $co ='zebra1';
                                                                $co2='zebra2';
                                                            ?>
                                                            <tr v-for="cdp in cdps_rev" v-on:click="seleccionarCdpRev(cdp)" class='<?php echo $co; ?>' style='text-rendering: optimizeLegibility; cursor: pointer !important; style=\"cursor: hand\"'>
                                                                <td width="10%" style="font: 120% sans-serif; padding-left:10px">{{ cdp[0] }}</td>
                                                                <td  style="font: 120% sans-serif; padding-left:10px">{{ cdp[2] }}</td>

                                                                <td width="15%" style="font: 120% sans-serif; padding-left:10px">{{ formatonumero(cdp[3]) }}</td>
                                                                <?php
                                                                $aux=$co;
                                                                $co=$co2;
                                                                $co2=$aux;
                                                                ?>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary" @click="showModal_cdps_rev = false">Cerrar</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </transition>
                    </div> -->

                </div>

            </div>
        </div>
        <!-- <script>
            import VueSweetalert2 from 'node_modules/vue-sweetalert2';
        </script> -->
        <!-- <script src="node_modules/sweetalert2.all.min.js"></script> -->
        <!-- <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script> -->
        <!-- <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script> -->

        <!-- <script src="https://unpkg.com/vue-swal"></script> -->

        <script src="Librerias/vue/vue.min.js"></script>
		<script src="Librerias/vue/axios.min.js"></script>
		<script src="presupuesto_ccpet/solicitudcdp/ccp-solicitudcdp.js?<?php echo date('d_m_Y_h_i_s');?>" type="module"></script>
        <script src="sweetalert2/dist/sweetalert2.min.js"></script>
        <link rel="stylesheet" href="sweetalert2/dist/sweetalert2.min.css">

    </body>
</html>
