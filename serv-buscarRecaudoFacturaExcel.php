<?php 
	header("Content-Type: text/html;charset=utf-8");
    require_once 'PHPExcel/Classes/PHPExcel.php';
    require "comun.inc";
	
    session_start();
	$intCodigoUsuario = $_GET['codigo'];
	$intNumeroFactura = $_GET['factura'];
	$intConsecutivo = $_GET['consecutivo'];
	$dateInicio = $_GET['dateInicio'];
	$dateFinal = $_GET['dateFinal'];
	$dateRange ="";
	$total=0;

	$linkbd = conectar_v7();
    $objPHPExcel = new PHPExcel();
	$objPHPExcel->getActiveSheet()->getStyle('A:J')->applyFromArray(
		array(
			'alignment' => array(
				'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
				'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
			)
		)
	);
	$objPHPExcel->getProperties()
	->setCreator("IDEAL 10")
	->setLastModifiedBy("IDEAL 10")
	->setTitle("Exportar Excel con PHP")
	->setSubject("Documento de prueba")
	->setDescription("Documento generado con PHPExcel")
	->setKeywords("usuarios phpexcel")
	->setCategory("reportes");

	//----Cuerpo de Documento----
	$objPHPExcel->setActiveSheetIndex(0)
	->mergeCells('A1:j1')
	->mergeCells('A2:j2')
	->setCellValue('A1', 'SERVICIOS PÚBLICOS')
	->setCellValue('A2', 'REPORTE DE RECAUDOS');
	$objPHPExcel-> getActiveSheet ()
	-> getStyle ("A1")
	-> getFill ()
	-> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
	-> getStartColor ()
	-> setRGB ('C8C8C8');
	$objPHPExcel-> getActiveSheet ()
	-> getStyle ("A1:A2")
	-> getFont ()
	-> setBold ( true ) 
	-> setName ( 'Verdana' ) 
	-> setSize ( 10 ) 
	-> getColor ()
	-> setRGB ('000000');
	$objPHPExcel-> getActiveSheet ()
	-> getStyle ('A1:A2')
	-> getAlignment ()
	-> applyFromArray (array ( 'horizontal'  =>  PHPExcel_Style_Alignment :: HORIZONTAL_CENTER ,) );
	$objPHPExcel-> getActiveSheet ()
	-> getStyle ('A3:j3')
	-> getAlignment ()
	-> applyFromArray (array ( 'horizontal'  =>  PHPExcel_Style_Alignment :: VERTICAL_JUSTIFY ,) ); 
	$objPHPExcel-> getActiveSheet ()
	-> getStyle ("A2")
	-> getFill ()
	-> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
	-> getStartColor ()
	-> setRGB ('A6E5F3');
	$objPHPExcel-> getActiveSheet ()
	-> getStyle ("A3:j3")
	-> getFill ()
	-> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
	-> getStartColor ()
	-> setRGB ('22C6CB');
	$borders = array(
		'borders' => array(
			'allborders' => array(
				'style' => PHPExcel_Style_Border::BORDER_THIN,
			'color' => array('argb' => 'FF000000'),
			)
		),
	);
	$objPHPExcel->getActiveSheet()->getStyle('A1:j1')->applyFromArray($borders);
	$objPHPExcel->getActiveSheet()->getStyle('A2:j2')->applyFromArray($borders);
	$objPHPExcel->getActiveSheet()->getStyle('A3:j3')->applyFromArray($borders);
	$objWorksheet = $objPHPExcel->getActiveSheet();

	$objPHPExcel->setActiveSheetIndex(0)
	->setCellValue('A3', 'Consecutivo de recaudo')
	->setCellValue('B3', 'Codigos de usuario')
	->setCellValue('C3', 'Nombre')
	->setCellValue('D3', 'Número de factura')
	->setCellValue('E3', 'Fecha de recaudo')
	->setCellValue('F3', 'Descuento')
	->setCellValue('G3', 'Medio de pago')
	->setCellValue('H3', 'Realiza')
	->setCellValue('I3', 'Estado')
	->setCellValue('J3', 'Valor de pago');

	
	if($dateInicio !="" && $dateFinal !=""){
		$arrFechaInicio = explode("/",$dateInicio);
		$arrFechaFinal = explode("/",$dateFinal);
		$dateInicio = date_format(date_create($arrFechaInicio[2]."-".$arrFechaInicio[1]."-".$arrFechaInicio[0]),"Y-m-d");
		$dateFinal = date_format(date_create($arrFechaFinal[2]."-".$arrFechaFinal[1]."-".$arrFechaFinal[0]),"Y-m-d");
		$dateRange = " AND fecha_recaudo >= '$dateInicio' AND fecha_recaudo <= '$dateFinal'";
	}
	$sql = "SELECT 
			codigo_recaudo as consecutivo,
			DATE_FORMAT(fecha_recaudo,'%d/%m/%Y') as fecha_recaudo,
			cod_usuario, 
			suscriptor as nombre,
			numero_factura,
			descuento_intereses as descuento,
			medio_pago,
			valor_pago,
			usuario as realiza,
			estado 
			FROM srv_recaudo_factura
			WHERE estado='ACTIVO' AND cod_usuario LIKE '$intCodigoUsuario%' AND numero_factura LIKE '$intNumeroFactura%'
			AND codigo_recaudo LIKE '$intConsecutivo%' $dateRange ORDER BY id ASC
	";
	$request = mysqli_fetch_all(mysqli_query($linkbd,$sql),MYSQLI_ASSOC);
	$rowExcel = 4;
	
	foreach ($request as $data) {
		$objPHPExcel->setActiveSheetIndex(0)
		->setCellValueExplicit ("A$rowExcel", $data['consecutivo'], PHPExcel_Cell_DataType :: TYPE_STRING)
		->setCellValueExplicit ("B$rowExcel", $data['cod_usuario'], PHPExcel_Cell_DataType :: TYPE_STRING)
		->setCellValueExplicit ("C$rowExcel", strtoupper($data['nombre']), PHPExcel_Cell_DataType :: TYPE_STRING)
		->setCellValueExplicit ("D$rowExcel", $data['numero_factura'], PHPExcel_Cell_DataType :: TYPE_STRING)
		->setCellValueExplicit ("E$rowExcel", $data['fecha_recaudo'], PHPExcel_Cell_DataType :: TYPE_STRING)
		->setCellValueExplicit ("F$rowExcel", $data['descuento'], PHPExcel_Cell_DataType :: TYPE_STRING)
		->setCellValueExplicit ("G$rowExcel", $data['medio_pago'], PHPExcel_Cell_DataType :: TYPE_STRING)
		->setCellValueExplicit ("H$rowExcel", strtoupper($data['realiza']), PHPExcel_Cell_DataType :: TYPE_STRING)
		->setCellValueExplicit ("I$rowExcel", $data['estado'], PHPExcel_Cell_DataType :: TYPE_STRING)
		->setCellValueExplicit ("J$rowExcel", $data['valor_pago'], PHPExcel_Cell_DataType :: TYPE_NUMERIC);
		$objPHPExcel->getActiveSheet()->getStyle("A$rowExcel:J$rowExcel")->applyFromArray($borders);
		$rowExcel++;
		$total+=$data['valor_pago'];
	}
	
	$objPHPExcel->setActiveSheetIndex(0)
	->mergeCells("A$rowExcel:I$rowExcel")
	->setCellValue("A$rowExcel", 'TOTAL')
	->setCellValue("J$rowExcel",$total);
	$objPHPExcel->getActiveSheet()->getStyle("A$rowExcel")->getFont()->setBold(true);
	$objPHPExcel->getActiveSheet()->getStyle("A$rowExcel:H$rowExcel")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
	$objPHPExcel->getActiveSheet()->getStyle("H4:H$rowExcel")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
	$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true); 
	$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
	$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
	$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(40);
	$objPHPExcel->getActiveSheet()->getStyle('B4:B'.$objPHPExcel->getActiveSheet()->getHighestRow())->getAlignment()->setWrapText(true); 
	$objPHPExcel->getActiveSheet()->getStyle('C4:C'.$objPHPExcel->getActiveSheet()->getHighestRow())->getAlignment()->setWrapText(true); 
	$objPHPExcel->getActiveSheet()->getStyle('D4:D'.$objPHPExcel->getActiveSheet()->getHighestRow())->getAlignment()->setWrapText(true); 
	$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true); 
	$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true); 
	$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->setTitle('Reporte recaudo');
	$objPHPExcel->setActiveSheetIndex(0);
	
    //----Guardar documento----
    header('Content-Type: application/vnd.ms-excel');
	header('Content-Disposition: attachment;filename="reporte-recaudo.xlsx"');
	header('Cache-Control: max-age=0');
    $objWriter=PHPExcel_IOFactory::createWriter($objPHPExcel,'Excel2007');
    $objWriter->save('php://output');
    die();
?>