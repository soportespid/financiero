<?php
    require_once '../../../Librerias/core/Helpers.php';
    require_once '../../../Librerias/core/Mysql.php';
    session_start();
    /*ini_set('display_errors', '1');
    ini_set('display_startup_errors', '1');
    error_reporting(E_ALL);*/
    class ConsolidadoCdpModel extends Mysql{
        private $strFechaInicial;
        private $strFechaFinal;
        private $strTipo;
        private $strArea;
        private $strDependencia;
        private $strVigenciaInicial;
        private $strVigenciaFinal;
        private $intVersion;

        function __construct(){
            parent::__construct();
            $this->intVersion = getVersionGastosCcpet();
        }

        public function selectTipos(){
            $sql = "SELECT nombre,inicio_cuenta as codigo FROM ccpettipo_gasto WHERE estado = 'S'";
            $request = $this->select_all($sql);
            return $request;
        }
        public function selectDependencias(){
            $sql = "SELECT nombre,id_seccion_presupuestal as codigo FROM pptoseccion_presupuestal WHERE estado = 'S'";
            $request = $this->select_all($sql);
            return $request;
        }
        public function selectAreas(){
            $sql = "SELECT nombrearea as nombre,codarea as codigo FROM planacareas WHERE estado = 'S'";
            $request = $this->select_all($sql);
            return $request;
        }
        public function selectData(string $strFechaInicial,string $strFechaFinal,string $strArea,string $strDependencia,string $strTipo){
            $arrFechaInicial = explode("-",$strFechaInicial);
            $arrFechaFinal = explode("-",$strFechaFinal);
            $this->strFechaInicial = $strFechaInicial;
            $this->strFechaFinal = $strFechaFinal;
            $this->strVigenciaInicial = $arrFechaInicial[0];
            $this->strVigenciaFinal = $arrFechaFinal[0];
            $this->strArea = $strArea;
            $this->strDependencia = $strDependencia;
            $this->strTipo = $strTipo;

            $sql = "SELECT DISTINCT det.consvigencia as cdp,
            det.vigencia,
            det.cuenta,
            det.fuente,
            det.productoservicio,
            coalesce(sum(det.valor),0) as valor_cdp,
            det.tipo_mov,
            det.indicador_producto,
            det.bpim as bpin,
            det.seccion_presupuestal as dependencia,
            det.area,
            det.tipo_gasto as tipo,
            det.codigo_vigenciag as vigencia_gasto,
            det.medio_pago,
            cta.nombre as nombre_cuenta,
            coalesce(pro.nombre,'') as nombre_proyecto,
            DATE_FORMAT(cab.fecha,'%d/%m/%Y') as fecha,
            cab.solicita,
            cab.objeto
            FROM ccpetcdp cab
            INNER JOIN ccpetcdp_detalle det ON cab.consvigencia = det.consvigencia AND cab.vigencia = det.vigencia
            LEFT JOIN cuentasccpet cta ON det.cuenta = cta.codigo AND cta.version = $this->intVersion
            LEFT JOIN ccpproyectospresupuesto pro ON det.bpim = pro.codigo
            WHERE det.area LIKE '$this->strArea%'
            AND det.seccion_presupuestal LIKE '$this->strDependencia%'
            AND det.cuenta LIKE '$this->strTipo%'
            AND cab.fecha BETWEEN '$this->strFechaInicial' AND '$this->strFechaFinal'
            AND det.tipo_mov = '201' AND cab.tipo_mov = '201'
            GROUP BY det.cuenta,det.fuente,det.indicador_producto,det.area,det.bpim,det.seccion_presupuestal,
            det.codigo_vigenciag,det.medio_pago,cab.consvigencia,det.productoservicio
            ORDER BY det.consvigencia DESC";
            $request = $this->select_all($sql);
            return $request;
        }
        public function selectSaldoCdp($idCdp,$vigencia, $cuenta, $fuente, $bpim, $indicador_producto, $seccion_presupuestal,$area,$medioPago,$vigenciaGasto,$valorCdp){
            $sqlReversado = "SELECT COALESCE(SUM(det.valor),0) as valor
            FROM ccpetcdp cab
            INNER JOIN ccpetcdp_detalle det ON det.consvigencia = cab.consvigencia AND det.vigencia = cab.vigencia
            WHERE cab.vigencia = '$vigencia' AND det.consvigencia = $idCdp AND cab.tipo_mov LIKE '4%' AND det.tipo_mov LIKE '4%'
            AND det.cuenta = '$cuenta' AND det.fuente = '$fuente' AND det.bpim = '$bpim'
            AND det.indicador_producto = '$indicador_producto'
            AND det.seccion_presupuestal = '$seccion_presupuestal' AND det.area = '$area'
            AND det.codigo_vigenciag = '$vigenciaGasto' AND det.medio_pago = '$medioPago'";
            $requestRev= $this->select($sqlReversado)['valor'];
            $saldo = round($valorCdp - $requestRev, 2);
            return $saldo;
        }
        public function selectSaldoRp($idCdp,$vigencia, $cuenta, $fuente, $bpim, $indicador_producto, $seccion_presupuestal,$area,$medioPago,$vigenciaGasto){
            $sql = "SELECT COALESCE(SUM(det.valor),0) as valor
            FROM ccpetrp cab
            INNER JOIN ccpetrp_detalle det ON det.consvigencia = cab.consvigencia AND det.vigencia = cab.vigencia
            WHERE cab.vigencia = '$vigencia' AND cab.idcdp = $idCdp
            AND cab.tipo_mov = '201' AND det.cuenta = '$cuenta' AND det.fuente = '$fuente'
            AND det.bpim = '$bpim' AND det.indicador_producto = '$indicador_producto'
            AND det.seccion_presupuestal = '$seccion_presupuestal' AND det.area = '$area'
            AND det.codigo_vigenciag = '$vigenciaGasto' AND det.medio_pago = '$medioPago'";
            $request = $this->select($sql)['valor'];
            return $request;
        }
        public function selectSaldoObligacion($strFechaInicial,$strFechaFinal,$vigencia, $cuenta, $fuente, $bpim, $indicador_producto, $seccion_presupuestal,$area,$medioPago,$vigenciaGasto, $idCdp){
            $request = 0;
            $sqlrRps = "SELECT consvigencia as idrp FROM ccpetrp WHERE vigencia = '$vigencia' AND idcdp = $idCdp";
            $arrRps = $this->select_all($sqlrRps);
            foreach ($arrRps as $key => $value) {
                $sql = "SELECT COALESCE(SUM(det.valor),0) as valor
                FROM tesoordenpago cab
                INNER JOIN tesoordenpago_det det ON det.id_orden = cab.id_orden AND det.vigencia = cab.vigencia
                WHERE cab.vigencia = '$vigencia' AND cab.id_rp = '{$value['idrp']}' AND cab.fecha BETWEEN '$strFechaInicial' AND '$strFechaFinal'
                AND cab.estado != 'R' AND det.cuentap = '$cuenta' AND det.fuente = '$fuente'
                AND det.bpim = '$bpim' AND det.indicador_producto = '$indicador_producto'
                AND det.seccion_presupuestal = '$seccion_presupuestal' AND det.area = '$area'
                AND det.codigo_vigenciag = '$vigenciaGasto' AND det.medio_pago = '$medioPago'
                ";
                $request += $this->select($sql)['valor'];
            }
            
            return $request;
        }
        public function selectSaldoPagos($strFechaInicial,$strFechaFinal,$vigencia, $cuenta, $fuente, $bpim, $indicador_producto, $seccion_presupuestal,$area,$medioPago,$vigenciaGasto){
            $sql = "SELECT COALESCE(SUM(det.valor),0) as valor
            FROM tesoegresos cab
            INNER JOIN tesoordenpago_det det ON det.id_orden = cab.id_orden AND det.vigencia = cab.vigencia
            WHERE cab.vigencia = '$vigencia' AND cab.fecha BETWEEN '$strFechaInicial' AND '$strFechaFinal'
            AND cab.estado != 'R' AND cab.estado != 'N' AND det.cuentap = '$cuenta' AND det.fuente = '$fuente'
            AND det.bpim = '$bpim' AND det.indicador_producto = '$indicador_producto'
            AND det.seccion_presupuestal = '$seccion_presupuestal' AND det.area = '$area'
            AND det.codigo_vigenciag = '$vigenciaGasto' AND det.medio_pago = '$medioPago'
            ";
            $request = $this->select($sql)['valor'];
            return $request;
        }
    }
?>
