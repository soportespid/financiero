<?php
    require_once '../../../Librerias/core/Helpers.php';
    require_once '../../../Librerias/core/Mysql.php';
    session_start();
    /*ini_set('display_errors', '1');
    ini_set('display_startup_errors', '1');
    error_reporting(E_ALL);*/
    class ConsolidadoRpModel extends Mysql{
        private $strFechaInicial;
        private $strFechaFinal;
        private $strTipo;
        private $strArea;
        private $strDependencia;
        private $strVigenciaInicial;
        private $strVigenciaFinal;
        private $intVersion;

        function __construct(){
            parent::__construct();
            $this->intVersion = getVersionGastosCcpet();
        }

        public function selectTipos(){
            $sql = "SELECT nombre,inicio_cuenta as codigo FROM ccpettipo_gasto WHERE estado = 'S'";
            $request = $this->select_all($sql);
            return $request;
        }
        public function selectDependencias(){
            $sql = "SELECT nombre,id_seccion_presupuestal as codigo FROM pptoseccion_presupuestal WHERE estado = 'S'";
            $request = $this->select_all($sql);
            return $request;
        }
        public function selectAreas(){
            $sql = "SELECT nombrearea as nombre,codarea as codigo FROM planacareas WHERE estado = 'S'";
            $request = $this->select_all($sql);
            return $request;
        }
        public function selectData(string $strFechaInicial,string $strFechaFinal,string $strArea,string $strDependencia,string $strTipo){
            $arrFechaInicial = explode("-",$strFechaInicial);
            $arrFechaFinal = explode("-",$strFechaFinal);
            $this->strFechaInicial = $strFechaInicial;
            $this->strFechaFinal = $strFechaFinal;
            $this->strVigenciaInicial = $arrFechaInicial[0];
            $this->strVigenciaFinal = $arrFechaFinal[0];
            $this->strArea = $strArea;
            $this->strDependencia = $strDependencia;
            $this->strTipo = $strTipo;

            $sql = "SELECT DISTINCT cab.consvigencia as rp,
            cab.idcdp as cdp,
            cab.detalle as objeto,
            det.vigencia,
            det.cuenta,
            det.productoservicio,
            det.fuente,
            coalesce(sum(det.valor),0) as valor_rp,
            det.tipo_mov,
            det.indicador_producto,
            det.bpim as bpin,
            det.seccion_presupuestal as dependencia,
            det.area,
            det.tipo_gasto as tipo,
            det.codigo_vigenciag as vigencia_gasto,
            det.medio_pago,
            cta.nombre as nombre_cuenta,
            DATE_FORMAT(cab.fecha,'%d/%m/%Y') as fecha,
            CASE WHEN t.razonsocial IS NULL OR t.razonsocial = ''
            THEN CONCAT(t.nombre1,' ',t.nombre2,' ',t.apellido1,' ',t.apellido2)
            ELSE t.razonsocial END AS nombre_tercero,
            cab.tercero as documento
            FROM ccpetrp cab
            INNER JOIN ccpetrp_detalle det ON cab.consvigencia = det.consvigencia AND cab.vigencia = det.vigencia
            LEFT JOIN cuentasccpet cta ON det.cuenta = cta.codigo AND cta.version = $this->intVersion
            LEFT JOIN terceros t ON cab.tercero = t.cedulanit
            WHERE COALESCE(det.area, '') LIKE '$this->strArea%'
            AND det.seccion_presupuestal LIKE '$this->strDependencia%'
            AND det.cuenta LIKE '$this->strTipo%'
            AND cab.fecha BETWEEN '$this->strFechaInicial' AND '$this->strFechaFinal'
            AND det.tipo_mov = '201' AND cab.tipo_mov = '201'
            GROUP BY det.cuenta,det.fuente,det.indicador_producto,det.area,det.bpim,det.seccion_presupuestal,
            det.codigo_vigenciag,det.medio_pago,det.productoservicio,cab.consvigencia,cab.vigencia
            ORDER BY cab.consvigencia DESC";
            //dep($sql);exit;
            $request = $this->select_all($sql);
            $total = count($request);
            if($total > 0){
                $arrProyectos = $this->select_all("SELECT * FROM ccpproyectospresupuesto");
                for ($i=0; $i < $total ; $i++) {
                    $det = $request[$i];
                    $data = array_values(array_filter($arrProyectos,function($e)use($det){return $e['codigo'] == $det['bpin'];}))[0];
                    $det['nombre_proyecto'] = $data['nombre'];
                    $request[$i] = $det;
                }
            }
            return $request;
        }
        public function selectSaldoRp($id,$vigencia, $cuenta, $fuente, $bpim, $indicador_producto, $seccion_presupuestal,$area,$medioPago,$vigenciaGasto,$valor){
            $sqlReversado = "SELECT COALESCE(SUM(det.valor),0) as valor
            FROM ccpetrp cab
            INNER JOIN ccpetrp_detalle det ON det.consvigencia = cab.consvigencia AND det.vigencia = cab.vigencia
            WHERE cab.vigencia = '$vigencia' AND cab.idcdp = $id AND det.tipo_mov LIKE '4%' AND cab.tipo_mov LIKE '4%'
            AND det.cuenta = '$cuenta' AND det.fuente = '$fuente' AND det.bpim = '$bpim'
            AND det.indicador_producto = '$indicador_producto'
            AND det.seccion_presupuestal = '$seccion_presupuestal' AND COALESCE(det.area, '') LIKE '$area'
            AND det.codigo_vigenciag = '$vigenciaGasto' AND det.medio_pago = '$medioPago'";
            $requestRev= $this->select($sqlReversado)['valor'];

            $saldoRp = round($valor - $requestRev, 2);
            return $saldoRp;
        }
        public function selectSaldoObligacion($strFechaInicial,$strFechaFinal,$vigencia, $cuenta, $fuente, $bpim, $indicador_producto, $seccion_presupuestal,$area,$medioPago,$vigenciaGasto, $rp){
            $sql = "SELECT COALESCE(SUM(det.valor),0) as valor
            FROM tesoordenpago cab
            INNER JOIN tesoordenpago_det det ON det.id_orden = cab.id_orden AND det.vigencia = cab.vigencia
            WHERE cab.id_rp = '$rp' AND cab.vigencia = '$vigencia' AND cab.fecha BETWEEN '$strFechaInicial' AND '$strFechaFinal'
            AND cab.estado != 'R' AND det.cuentap = '$cuenta' AND det.fuente = '$fuente'
            AND det.bpim = '$bpim' AND det.indicador_producto = '$indicador_producto'
            AND det.seccion_presupuestal = '$seccion_presupuestal' AND COALESCE(det.area, '') LIKE '$area'
            AND det.codigo_vigenciag = '$vigenciaGasto' AND det.medio_pago = '$medioPago'
            ";
            $request = $this->select($sql)['valor'];
            return $request;
        }
        public function selectSaldoPagos($strFechaInicial,$strFechaFinal,$vigencia, $cuenta, $fuente, $bpim, $indicador_producto, $seccion_presupuestal,$area,$medioPago,$vigenciaGasto, $rp){
            $sql = "SELECT COALESCE(SUM(det.valor),0) as valor
            FROM tesoegresos cab
            INNER JOIN tesoordenpago_det det ON det.id_orden = cab.id_orden AND det.vigencia = cab.vigencia
            INNER JOIN tesoordenpago ope ON ope.id_orden = cab.id_orden AND ope.id_rp = '$rp'
            WHERE cab.vigencia = '$vigencia' AND cab.fecha BETWEEN '$strFechaInicial' AND '$strFechaFinal'
            AND cab.estado != 'R' AND cab.estado != 'N' AND det.cuentap = '$cuenta' AND det.fuente = '$fuente'
            AND det.bpim = '$bpim' AND det.indicador_producto = '$indicador_producto'
            AND det.seccion_presupuestal = '$seccion_presupuestal' AND COALESCE(det.area, '') LIKE '$area'
            AND det.codigo_vigenciag = '$vigenciaGasto' AND det.medio_pago = '$medioPago'
            ";
            $request = $this->select($sql)['valor'];
            return $request;
        }
    }
?>
