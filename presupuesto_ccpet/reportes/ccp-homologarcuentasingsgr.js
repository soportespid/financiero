
const URL = 'presupuesto_ccpet/reportes/ccp-homologarcuentasingsgr.php';
import { ordenarArbol, buscaNombreRubros, filtroEnArrayDeObjetos } from '../../funciones.js'

const app = Vue.createApp({
    data() {
      return {
        results: [],
        results_seleccionados: [],
        results_seleccionadosOrd: [],
        show_table_search: false,
        show_resultados: true,
        show_resultados_seleccionados: false,
        get_code: [],
        get_code_1: [],
        myStyle: {
            backgroundColor: '#12DB67',
            color: 'white'
        },
        myStyleMayor: {
            'font-weight': 'bold',
        },

        loading: false,
        rubrosInicial: [],
        accounts: [],
        accountsAux: [],
        accountsMayor: [],
        fuentes: [],
        arbol: [],
        arbolInv: [],
        rubrosInv: [],
        rubrosAdInv: [],
        rubrosTrasladosCredInv: [],
        nombreRubrosInv: [],
        buscar_rubro: '',
        resultsBuscar: [],
        arbolGeneral:[],
        vigencia: 0,
        years:[],
        checkTodo: false,

        showModal_cuentas: false,

        cuentaBuscada: '',
        cuentasSGR : [],
        cuentaBuscada: [],
        searchCuenta : {keywordCuenta: ''},
        cuentaSelSGR: [],

        puedeGuardar: false,

        tiposRecursosSGR : [],
        showModal_tiposRecursos: false,
        searchTipoRecurso : {keywordTipoRecurso: ''},

        tercerosSGR : [],
        showModal_terceros: false,
        searchTercero : {keywordTercero: ''},

        politicasPublicasSGR : [],
        showModal_politicas_publicas: false,
        searchPoliticaPublica : {keywordPoliticaPublica: ''},

        rubroVentanaEmergente: '',

      }
    },

    async mounted() {
        this.loading = false;
        await this.cargayears();
        this.cargarParametros();
    },

    computed: {
        /* mostrarCheck(){
            return this.buscar_rubro.length > 2 ? true : false
        }, */
    },

    methods: {

        cargayears: async function(){
			await axios.post('vue/presupuesto_ccp/ccp-bancoproyectos.php?buscar=years')
			.then(
				(response)=>
				{
					this.years=response.data.anio;
					var idanio=response.data.anio.length -1;
					if (idanio>=0){this.vigencia = response.data.anio[idanio][0];}
					else{this.vigencia ='';}
				}
            );
            
		},

        async cargarParametros(){

            this.loading = true;

            let formData = new FormData();
            formData.append("vigencia", this.vigencia);
            
            await axios.post(URL, formData)
            .then((response) => {
                this.rubrosInicial = response.data.rubrosInicial;
                this.accounts = response.data.accounts;
                this.fuentes = response.data.fuentes;
                if(response.data.cuentaSelSGR)
                    this.cuentaSelSGR = response.data.cuentaSelSGR;

            }).catch((error) => {
                this.error = true;
                console.log(error)
            }).finally(() => {
                this.loading = false;
                this.auxiliarMayor();
                this.orderAccounts();
            });
        },

        async orderAccounts(){

            const arrPuro = this.rubrosInicial.map((e) => {
                return e[0]
            });

            const dataArrSet = new Set(arrPuro);

            const dataArr = Array.from(dataArrSet);
           
            const nombresRubros = this.accountsAux.concat(this.fuentes);
            this.results = buscaNombreRubros(dataArr, nombresRubros);

            this.resultsBuscar = [...this.results];

        },

        auxiliarMayor(){
            this.accountsAux = this.accounts.filter(element => element[2] == 'C');
            this.accountsMayor = this.accounts.filter(element => element[2] == 'A');
        },


        estaEnArray(codigos){
            const parsedobj = JSON.stringify(this.results_seleccionados);
            return parsedobj.includes(codigos[0])
        },

        removeItemFromArr(item){
            var i = this.results_seleccionados.indexOf( item );
                
            if ( i !== -1 ) {
                this.results_seleccionados.splice( i, 1 );
            }

        },

        seleccionaCodigosCheck(codigos){

            const parsedobj = JSON.stringify(this.results_seleccionados);

            if(parsedobj.includes(codigos[0])){
                this.removeItemFromArr(codigos);
            }else{
                this.results_seleccionados.push(codigos);
            }
        },

        async ordenarArbolCheck(){
            this.results_seleccionadosOrd = await ordenarArbol(this.arbolGeneral, this.results_seleccionados);
            
            this.show_resultados_seleccionados = this.results_seleccionados.length > 0 ? true : false;
        },

        async buscarRubro(){

            this.results = [];
            this.checkTodo = false;
            var text = this.buscar_rubro;
            const data = this.resultsBuscar;
            this.results = await filtroEnArrayDeObjetos({'data': data, 'text': text});

        },

        async guardarRubroSGR(){
            const objectValues = Object.values(this.cuentaSelSGR);
            const objectkeys = Object.keys(this.cuentaSelSGR);
            const tamObjectValues = objectValues.length;
            if(tamObjectValues > 0){
                
                Swal.fire({
                    title: 'Esta seguro de guardar?',
                    text: "Guardar clasificador presupuestal de gastos, confirmar campos!",
                    icon: 'question',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Si, guardar!'
                }).then((result) => {
                    if (result.isConfirmed) {

                        this.loading = true;

                        var formData = new FormData();

                        for(let i=0; i <= tamObjectValues-1; i++){

                            formData.append("cuentasSGR["+i+"]", objectValues[i]);
                            formData.append("rubros["+i+"]", objectkeys[i]);
                            
                        }
                        
                        axios.post(URL+'?action=guardarRubroSGR', formData)
                        .then((response) => {
                            console.log(response.data);
                            if(response.data.insertaBien){
                                Swal.fire({
                                    position: 'top-end',
                                    icon: 'success',
                                    title: 'Los rubros SGR se guardaron con Exito',
                                    showConfirmButton: false,
                                    timer: 1500
                                }).then((response) => {
                                        this.redireccionar();
                                    });
                            }else{
                                
                                Swal.fire(
                                    'Error!',
                                    'No se pudo guardar.',
                                    'error'
                                );
                            }

                        }).catch((error) => {
                            this.error = true;
                            console.log(error)
                        }).finally(() => {
                            this.loading = false;
                        });
                    }
                })

            }else{
                Swal.fire(
                    'Falta seleccionar rubros.',
                    'Seleccione los rubros con los que desea crear el clasificador dando clic en el listado.',
                    'warning'
                )
            }
            
        },

        async agregarTipoRecursoSGR(cuentaBuscada, nombreCuentaBuscada){
            this.rubroVentanaEmergente = cuentaBuscada.split('-')[0] + ' - ' + nombreCuentaBuscada;
            this.cuentaBuscada = cuentaBuscada;
            await axios.post(URL+'?action=cargarTiposRecursos')
                .then((response) => {
                    this.tiposRecursosSGR = response.data.tiposRecursosSGR;
                }).finally(() => {
                    this.showModal_tiposRecursos = true;
                });
        },

        seleccionarTipoRecurso(tipoRecursoSelec){
            console.log(this.cuentaSelSGR);
            if(this.cuentaSelSGR){
                this.cuentaSelSGR[this.cuentaBuscada + '-2'] = tipoRecursoSelec[1];
            }
            /* this.cuentaSelSGR[this.cuentaBuscada + '-2'] = tipoRecursoSelec[1]; */
            this.showModal_tiposRecursos = false;
            this.puedeGuardar = true;
            
        },

        searchMonitorTipoRecurso(){
            let keywordTipoRecurso = this.toFormData(this.searchTipoRecurso);
            
            axios.post(URL+'?action=filtrarTiposRecursos', keywordTipoRecurso)
            .then((response) => {
                this.tiposRecursosSGR = response.data.tiposRecursosSGR;
            });
            
        },

        async agregarTerceroSGR(cuentaBuscada, nombreCuentaBuscada){
            this.rubroVentanaEmergente = cuentaBuscada.split('-')[0] + ' - ' + nombreCuentaBuscada;
            this.cuentaBuscada = cuentaBuscada;
            await axios.post(URL+'?action=cargarTerceros')
                .then((response) => {
                    this.tercerosSGR = response.data.tercerosSGR;
                }).finally(() => {
                    this.showModal_terceros = true;
                });
        },

        seleccionarTercero(terceroSelec){
            if(this.cuentaSelSGR){
                this.cuentaSelSGR[this.cuentaBuscada + '-3'] = terceroSelec[1];
            }
            /* this.cuentaSelSGR[this.cuentaBuscada + '-3'] = terceroSelec[1]; */
            this.showModal_terceros = false;
            this.puedeGuardar = true;
            
        },

        searchMonitorTercero(){
            let keywordTercero = this.toFormData(this.searchTercero);
            
            axios.post(URL+'?action=filtrarTerceros', keywordTercero)
            .then((response) => {
                this.tercerosSGR = response.data.tercerosSGR;
            });
            
        },

        async agregarPoliticaPublicaSGR(politicaPublicaBuscada, nombreCuentaBuscada){
            this.rubroVentanaEmergente = politicaPublicaBuscada.split('-')[0] + ' - ' + nombreCuentaBuscada;
            this.cuentaBuscada = politicaPublicaBuscada;
            await axios.post(URL+'?action=cargarPoliticasPublicas')
                .then((response) => {
                    this.politicasPublicasSGR = response.data.politicasPublicasSGR;
                }).finally(() => {
                    this.showModal_politicas_publicas = true;
                });
        },

        seleccionarPoliticaPublica(politicaPublicaSelec){
            if(this.cuentaSelSGR){
                this.cuentaSelSGR[this.cuentaBuscada + '-4'] = politicaPublicaSelec[1];
            }
            /* this.cuentaSelSGR[this.cuentaBuscada + '-3'] = terceroSelec[1]; */
            this.showModal_politicas_publicas = false;
            this.puedeGuardar = true;
            
        },

        searchMonitorPoliticaPublica(){
            let keywordPoliticaPublica = this.toFormData(this.searchPoliticaPublica);
            
            axios.post(URL+'?action=filtrarPoliticasPublicas', keywordPoliticaPublica)
            .then((response) => {
                this.politicasPublicasSGR = response.data.politicasPublicasSGR;
            });
            
        },

        redireccionar(){
            
            location.href ="ccp-homologarcuentasingsgr.php";
        },

        toFormData(obj){
            var form_data = new FormData();
            for(var key in obj){
                form_data.append(key, obj[key]);
            }
            return form_data;
        },
    }
})

app.mount('#myapp')