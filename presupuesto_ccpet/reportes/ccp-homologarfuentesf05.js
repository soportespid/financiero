
const URL = 'presupuesto_ccpet/reportes/ccp-homologarfuentesf05.php';
import { ordenarArbol, buscaNombreRubros, filtroEnArrayDeObjetos } from '../../funciones.js'

const app = Vue.createApp({
    data() {
      return {
        results: [],
        results_seleccionados: [],
        results_seleccionadosOrd: [],
        show_table_search: false,
        show_resultados: true,
        show_resultados_seleccionados: false,
        get_code: [],
        get_code_1: [],
        myStyle: {
            backgroundColor: '#12DB67',
            color: 'white'
        },
        myStyleMayor: {
            'font-weight': 'bold',
        },

        loading: false,
        rubrosInicial: [],
        accounts: [],
        accountsAux: [],
        accountsMayor: [],
        fuentes: [],
        arbol: [],
        arbolInv: [],
        rubrosInv: [],
        rubrosAdInv: [],
        rubrosTrasladosCredInv: [],
        nombreRubrosInv: [],
        buscar_fuente: '',
        resultsBuscar: [],
        arbolGeneral:[],
        vigencia: 0,
        years:[],
        checkTodo: false,

        showModal_cuentas: false,

        cuentaBuscada: '',
        

        puedeGuardar: false,


        rubroVentanaEmergente: '',

        fuentesf05: [],
        fuentesUsadas: [],
        selFuenteHomologar: [],
        showModal_fuentesf05: false,
        searchFuentef05 : {keywordFuentef05: ''},

      }
    },

    async mounted() {
        this.loading = false;
        await this.cargayears();
        this.cargarParametros();
    },

    computed: {
        /* mostrarCheck(){
            return this.buscar_rubro.length > 2 ? true : false
        }, */
    },

    methods: {

        cargayears: async function(){
			await axios.post('vue/presupuesto_ccp/ccp-bancoproyectos.php?buscar=years')
			.then(
				(response)=>
				{
					this.years=response.data.anio;
					var idanio=response.data.anio.length -1;
					if (idanio>=0){this.vigencia = response.data.anio[idanio][0];}
					else{this.vigencia ='';}
				}
            );
            
		},

        async cargarParametros(){

            this.loading = true;

            let formData = new FormData();
            formData.append("vigencia", this.vigencia);
            
            await axios.post(URL, formData)
            .then((response) => {
                this.fuentesUsadas = response.data.fuentesUsadas;
                this.fuentes = response.data.fuentes;
                if(response.data.selFuenteHomologar)
                    this.selFuenteHomologar = response.data.selFuenteHomologar;

            }).catch((error) => {
                this.error = true;
                console.log(error)
            }).finally(() => {
                this.loading = false;
                this.orderAccounts();
            });
        },

        async orderAccounts(){

            /* const arrPuro = this.fuentesUsadas.map((e) => {
                return e[0]
            }); */

            const dataArrSet = new Set(this.fuentesUsadas);

            const dataArr = Array.from(dataArrSet);
           
            /* const nombresRubros = this.accountsAux.concat(this.fuentes); */
            this.results = buscaNombreRubros(dataArr, this.fuentes);

            this.resultsBuscar = [...this.results];
            
        },


        estaEnArray(codigos){
            const parsedobj = JSON.stringify(this.results_seleccionados);
            return parsedobj.includes(codigos[0])
        },

        removeItemFromArr(item){
            var i = this.results_seleccionados.indexOf( item );
                
            if ( i !== -1 ) {
                this.results_seleccionados.splice( i, 1 );
            }

        },

        seleccionaCodigosCheck(codigos){

            const parsedobj = JSON.stringify(this.results_seleccionados);

            if(parsedobj.includes(codigos[0])){
                this.removeItemFromArr(codigos);
            }else{
                this.results_seleccionados.push(codigos);
            }
        },

        async ordenarArbolCheck(){
            this.results_seleccionadosOrd = await ordenarArbol(this.arbolGeneral, this.results_seleccionados);
            
            this.show_resultados_seleccionados = this.results_seleccionados.length > 0 ? true : false;
        },

        async buscarFuente(){

            this.results = [];
            this.checkTodo = false;
            var text = this.buscar_fuente;
            const data = this.resultsBuscar;
            this.results = await filtroEnArrayDeObjetos({'data': data, 'text': text});

        },

        async guardarFuentesf04(){
            const objectValues = Object.values(this.selFuenteHomologar);
            const objectkeys = Object.keys(this.selFuenteHomologar);
            const tamObjectValues = objectValues.length;
            if(tamObjectValues > 0){
                
                Swal.fire({
                    title: 'Esta seguro de guardar?',
                    text: "Guardar clasificador presupuestal de gastos, confirmar campos!",
                    icon: 'question',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Si, guardar!'
                }).then((result) => {
                    if (result.isConfirmed) {

                        this.loading = true;

                        var formData = new FormData();

                        for(let i=0; i <= tamObjectValues-1; i++){

                            formData.append("fuentesf05["+i+"]", objectValues[i]);
                            formData.append("fuentes["+i+"]", objectkeys[i]);
                            
                        }
                        
                        axios.post(URL+'?action=guardarFuentesf05', formData)
                        .then((response) => {
                            console.log(response.data);
                            if(response.data.insertaBien){
                                Swal.fire({
                                    position: 'top-end',
                                    icon: 'success',
                                    title: 'Las fuentes se guardaron con Exito',
                                    showConfirmButton: false,
                                    timer: 1500
                                }).then((response) => {
                                        this.redireccionar();
                                    });
                            }else{
                                
                                Swal.fire(
                                    'Error!',
                                    'No se pudo guardar.',
                                    'error'
                                );
                            }

                        }).catch((error) => {
                            this.error = true;
                            console.log(error)
                        }).finally(() => {
                            this.loading = false;
                        });
                    }
                })

            }else{
                Swal.fire(
                    'Falta seleccionar rubros.',
                    'Seleccione las fuentes a homologar.',
                    'warning'
                )
            }
            
        },

        async agregarFuentef05(cuentaBuscada, nombreCuentaBuscada){
            this.rubroVentanaEmergente = cuentaBuscada.split('-')[0] + ' - ' + nombreCuentaBuscada;
            this.cuentaBuscada = cuentaBuscada;
            await axios.post(URL+'?action=cargarFuentesf05')
                .then((response) => {
                    this.fuentesf05 = response.data.fuentesf05;
                }).finally(() => {
                    this.showModal_fuentesf05 = true;
                });
        },

        seleccionarFuentef05(fuentef05){
            if(this.selFuenteHomologar){
                this.selFuenteHomologar[this.cuentaBuscada] = fuentef05[1];
            }
            /* this.cuentaSelSGR[this.cuentaBuscada + '-2'] = tipoRecursoSelec[1]; */
            this.showModal_fuentesf05 = false;
            this.puedeGuardar = true;
            
        },
        

        searchMonitorFuentef05(){
            let keywordFuentef05 = this.toFormData(this.searchFuentef05);
            
            axios.post(URL+'?action=filtrarFuentesf05', keywordFuentef05)
            .then((response) => {
                this.fuentesf05 = response.data.fuentesf05;
            });
            
        },


        redireccionar(){
            
            location.href ="ccp-homologarfuentesf05.php";
        },

        toFormData(obj){
            var form_data = new FormData();
            for(var key in obj){
                form_data.append(key, obj[key]);
            }
            return form_data;
        },
    }
})

app.mount('#myapp')