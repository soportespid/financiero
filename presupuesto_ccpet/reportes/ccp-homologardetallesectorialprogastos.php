<?php

    require_once '../../comun.inc';
    require '../../funciones.inc';

    session_start();

	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");

    $maxVersion = ultimaVersionGastosCCPET();

    $out = array('error' => false);

    $action = "show";

    if(isset($_GET['action'])){
        $action = $_GET['action'];
    }



    if($action == 'show'){

        $fuentes = [];
        $rubrosInv = [];
        $rubrosAdInv = [];
        $rubrosTrasladosCredInv = [];
        $nombreRubrosInv = [];
        $cuentaSelSGR = [];


        $vigencia = $_POST['vigencia'];

        $sqlrFuentes = "SELECT codigo_fuente, nombre FROM ccpet_fuentes_cuipo";
        $respFuentes = mysqli_query($linkbd, $sqlrFuentes);
        while($rowFuentes = mysqli_fetch_row($respFuentes)){
            array_push($fuentes, $rowFuentes);
        }

        $sqlrInv = "SELECT TB2.indicador_producto, TB1.codigo, TB2.id_fuente FROM ccpproyectospresupuesto AS TB1, ccpproyectospresupuesto_presupuesto AS TB2 WHERE TB1.id = TB2.codproyecto AND TB1.vigencia = '$vigencia' AND (TB2.indicador_producto like '19%') ORDER BY TB2.indicador_producto ASC";
        $respInv = mysqli_query($linkbd, $sqlrInv);
        while($rowInv = mysqli_fetch_row($respInv)){
            $rubros = [];
            $nombreRubro = '';
            $nombreRubro = $rowInv[0].'-'.$rowInv[1].'-'.$rowInv[2];
            array_push($rubros, $nombreRubro);
            array_push($rubrosInv, $rubros);
        }

        $sqlrAdInv = "SELECT programatico, bpim, fuente FROM ccpet_adiciones WHERE vigencia = '$vigencia' AND bpim != '' AND (programatico like '19%')";
        $respAdInv = mysqli_query($linkbd, $sqlrAdInv);
        while($rowAdInv = mysqli_fetch_row($respAdInv)){
            $rubros = [];
            $nombreRubro = '';

            $nombreRubro = $rowAdInv[0].'-'.$rowAdInv[1].'-'.$rowAdInv[2];
            array_push($rubros, $nombreRubro);
            array_push($rubrosAdInv, $rubros);
        }

        $sqlrTrasladosCredInv = "SELECT programatico, bpim, fuente FROM ccpet_traslados WHERE vigencia = '$vigencia' AND bpim != '' AND tipo = 'C' AND (programatico LIKE '19%')";
        $respTrasladosCredInv = mysqli_query($linkbd, $sqlrTrasladosCredInv);
        while($rowTrasladosInv = mysqli_fetch_row($respTrasladosCredInv)){
            $rubros = [];
            $nombreRubro = '';

            $nombreRubro = $rowTrasladosInv[0].'-'.$rowTrasladosInv[1].'-'.$rowTrasladosInv[2];
            array_push($rubros, $nombreRubro);
            array_push($rubrosTrasladosCredInv, $rubros);
        }

        $sqlrProyInv = "SELECT codigo, nombre FROM ccpproyectospresupuesto WHERE vigencia = '$vigencia'";
        $respProyInv = mysqli_query($linkbd, $sqlrProyInv);
        while($rowProyInv = mysqli_fetch_row($respProyInv)){
            $nomProy = [];
            array_push($nomProy, $rowProyInv[0]);
            array_push($nomProy, $rowProyInv[1]);
            array_push($nomProy, 'A');
            array_push($nombreRubrosInv, $nomProy);
        }

        $sqlrSectores = "SELECT codigo, nombre FROM ccpetsectores WHERE version = (SELECT MAX(version) FROM ccpetsectores)";
        $respSectores = mysqli_query($linkbd, $sqlrSectores);
        while($rowSectores = mysqli_fetch_row($respSectores)){
            $nomSec = [];
            array_push($nomSec, $rowSectores[0]);
            array_push($nomSec, $rowSectores[1]);
            array_push($nomSec, 'A');
            array_push($nombreRubrosInv, $nomSec);
        }

        $sqlrProgramas = "SELECT codigo, nombre FROM ccpetprogramas WHERE version = (SELECT MAX(version) FROM ccpetprogramas)";
        $respProgramas = mysqli_query($linkbd, $sqlrProgramas);
        while($rowProgramas = mysqli_fetch_row($respProgramas)){
            $nomProg = [];
            array_push($nomProg, $rowProgramas[0]);
            array_push($nomProg, $rowProgramas[1]);
            array_push($nomProg, 'A');
            array_push($nombreRubrosInv, $nomProg);
        }

        $sqlrProductos = "SELECT cod_producto, producto FROM ccpetproductos WHERE version = (SELECT MAX(version) FROM ccpetproductos) GROUP BY cod_producto";
        $respProductos = mysqli_query($linkbd, $sqlrProductos);
        while($rowProductos = mysqli_fetch_row($respProductos)){
            $nomProg = [];
            array_push($nomProg, $rowProductos[0]);
            array_push($nomProg, $rowProductos[1]);
            array_push($nomProg, 'A');
            array_push($nombreRubrosInv, $nomProg);
        }

        $sqlrProgramatico = "SELECT codigo_indicador, indicador_producto FROM ccpetproductos WHERE version = (SELECT MAX(version) FROM ccpetproductos)";
        $respProgramatico = mysqli_query($linkbd, $sqlrProgramatico);
        while($rowProgramatico = mysqli_fetch_row($respProgramatico)){
            $nomInd = [];
            array_push($nomInd, $rowProgramatico[0]);
            array_push($nomInd, $rowProgramatico[1]);
            array_push($nomInd, 'A');
            array_push($nombreRubrosInv, $nomInd);
        }

        $sqlrSGR = "SELECT rubro, cuentasgr FROM ccpetdetallesectorialprogastos";
        $respSGR = mysqli_query($linkbd, $sqlrSGR);
        while($rowSGR = mysqli_fetch_row($respSGR)){
            $cuentaSelSGR[$rowSGR[0]] = $rowSGR[1];
        }

        $out['fuentes'] = $fuentes;
        $out['rubrosInv'] = $rubrosInv;
        $out['rubrosAdInv'] = $rubrosAdInv;
        $out['rubrosTrasladosCredInv'] = $rubrosTrasladosCredInv;
        $out['nombreRubrosInv'] = $nombreRubrosInv;
        $out['cuentaSelSGR'] = $cuentaSelSGR;

    }

    /* if($action == "buscaRubrosClasificador"){
        $clasificadorSelDet = [];

        if(isset($_POST['id_clasificador']) && $_POST['id_clasificador'] != ''){
            $id_clasificador = $_POST['id_clasificador'];
            $sqlr = "SELECT rubros, nombre, tipo FROM ccpet_clasificadorgastos_det WHERE id_clasificador = $id_clasificador AND tipo = 'C' ORDER BY rubros ASC";
            $res = mysqli_query($linkbd, $sqlr);

            while ($row = mysqli_fetch_row($res)) {
                array_push($clasificadorSelDet, $row);
            }
        }
        $out['clasificadorSelDet'] = $clasificadorSelDet;
    } */

    if($action == 'cargarCuentas'){

        $cuentasSGR = [];
        $sqlr = "SELECT codigo, concepto FROM cuipo_sec_prog_gastos ORDER BY codigo ASC";
        $res = mysqli_query($linkbd, $sqlr);
        while($row = mysqli_fetch_row($res)){
            array_push($cuentasSGR, $row);
        }

        $out['cuentasSGR'] = $cuentasSGR;

    }


    if($action == 'filtrarCuentas'){

        $keywordCuenta=$_POST['keywordCuenta'];

        $cuentasSGR = array();

        $sqlr = "SELECT * FROM cuipo_sec_prog_gastos WHERE concat_ws(' ', codigo, concepto ) LIKE '%$keywordCuenta%' ORDER BY codigo ASC";//echo $sqlr;
        $res = mysqli_query($linkbd, $sqlr);
        while($row = mysqli_fetch_row($res)){
            array_push($cuentasSGR, $row);
        }

        $out['cuentasSGR'] = $cuentasSGR;
    }


    if($action == 'guardarRubroSGR'){

        $sqlrVaciar = "TRUNCATE TABLE ccpetdetallesectorialprogastos";
        mysqli_query($linkbd, $sqlrVaciar);

        $user = $_SESSION['nickusu'];
        $fecha_mod = date("Y-m-d");

        $tam = count($_POST["cuentasSGR"]);

        for($x = 0; $x < $tam; $x++){

            $sector = '';
            $fuente = '';
            $programatico = '';
            $bpim = '';

            $partsRubro = explode("-", $_POST["rubros"][$x]);

            $sector = substr($_POST["rubros"][$x], 0, 2);
            $fuente = $partsRubro[2];
            $programatico = substr($_POST["rubros"][$x], 0, 7);
            $bpim = $partsRubro[1];



            $sqlrInsert = "INSERT INTO ccpetdetallesectorialprogastos(rubro, cuentasgr, sector, fuente, programatico, bpim, user, fecha_mod) VALUES ('".trim($_POST["rubros"][$x])."', '".trim($_POST["cuentasSGR"][$x])."', '".trim($sector)."', '".trim($fuente)."', '".trim($programatico)."', '".trim($bpim)."', '".$user."', '".$fecha_mod."')";
            mysqli_query($linkbd, $sqlrInsert);

        }
        $out['insertaBien'] = true;
    }

    /* if($action == 'editarDet'){

        $id_clasificador = $_POST['id_clasificador'];

        $sqlrD = "DELETE FROM ccpet_clasificadorgastos_det WHERE id_clasificador = $id_clasificador";
        mysqli_query($linkbd, $sqlrD);

        for($x = 0; $x < count($_POST["detallesAd"]); $x++){
            $rubro = '';
            $nombre = '';
            $tipo = '';

            $rubro = $_POST["detallesAd"][$x][0];
            $nombre = $_POST["detallesAd"][$x][1];
            $tipo = $_POST["detallesAd"][$x][2];

            $sql = "INSERT INTO ccpet_clasificadorgastos_det(rubros, nombre, tipo, id_clasificador) VALUES ('$rubro', '$nombre','$tipo', '".$id_clasificador."')";
            $res = mysqli_query($linkbd, $sql);
        }
        $out['insertaBien'] = true;
    } */

    header("Content-type: application/json");
    echo json_encode($out);
    die();
