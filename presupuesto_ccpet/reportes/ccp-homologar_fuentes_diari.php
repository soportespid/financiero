<?php

    require_once '../../comun.inc';
    require '../../funciones.inc';
    require '../../funcionesSP.inc.php';

    session_start();

	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");

    $maxVersion = ultimaVersionIngresosCCPET();

    $out = array('error' => false);

    $action = "show";

    if(isset($_GET['action'])){
        $action = $_GET['action'];
    }



    if($action == 'show'){

        $rubrosInicial = [];
        $fuentesUsadas = [];
        $fuentes = [];

        $vigencia = $_POST['vigencia'];

        $sqlrFuentes = "SELECT codigo_fuente, nombre FROM ccpet_fuentes_cuipo";
        $respFuentes = mysqli_query($linkbd, $sqlrFuentes);
        while($rowFuentes = mysqli_fetch_row($respFuentes)){
            array_push($fuentes, $rowFuentes);
        }

        $sqlrRps = "SELECT fuente FROM ccpetrp_detalle WHERE vigencia = '$vigencia' AND fuente != '' GROUP BY fuente";
        $respRps = mysqli_query($linkbd, $sqlrRps);
        while($rowRps = mysqli_fetch_row($respRps)){
            array_push($fuentesUsadas, $rowRps[0]);
        }

        $sqlrFuenteHomologar = "SELECT fuente_cuipo, fuente_diari FROM ccpet_homologarfuentesdiari";
        $respFuenteHomologar = mysqli_query($linkbd, $sqlrFuenteHomologar);
        while($rowFuenteHomologar = mysqli_fetch_row($respFuenteHomologar)){
            $selFuenteHomologar[$rowFuenteHomologar[0]] = $rowFuenteHomologar[1];
        }

        $out['fuentesUsadas'] = $fuentesUsadas;
        $out['fuentes'] = $fuentes;
        $out['selFuenteHomologar'] = $selFuenteHomologar;

    }

    if($action == 'cargarFuentesDiari'){

        $fuentesDiari = [];
        $sqlr = "SELECT * FROM ccpet_fuentes_diari ORDER BY id ASC";
        $res = mysqli_query($linkbd, $sqlr);
        while($row = mysqli_fetch_row($res)){
            array_push($fuentesDiari, $row);
        }

        $out['fuentesdiari'] = $fuentesDiari;

    }



    if($action == 'filtrarFuentesDiari'){

        $keywordFuenteDiari=$_POST['keywordFuenteDiari'];

        $fuentesdiari = array();

        $sqlr = "SELECT * FROM ccpet_fuentes_diari WHERE concat_ws(' ', fuente) LIKE '%$keywordFuenteDiari%' ORDER BY id ASC";//echo $sqlr;
        $res = mysqli_query($linkbd, $sqlr);
        while($row = mysqli_fetch_row($res)){
            array_push($fuentesdiari, $row);
        }

        $out['fuentesdiari'] = $fuentesdiari;
    }


    if($action == 'guardarFuentesDiari'){
        $user = $_SESSION['nickusu'];

        $sqlrVaciar = "TRUNCATE TABLE ccpet_homologarfuentesdiari";
        mysqli_query($linkbd, $sqlrVaciar);

        $tam = count($_POST["fuentesdiari"]);

        for($x = 0; $x < $tam; $x++){

            $sqlrInsert = "INSERT INTO ccpet_homologarfuentesdiari(fuente_cuipo, fuente_diari) VALUES ('".trim($_POST["fuentes"][$x])."', '".trim($_POST["fuentesdiari"][$x])."')";
            mysqli_query($linkbd, $sqlrInsert);

        }
        $out['insertaBien'] = true;
    }


    header("Content-type: application/json");
    echo json_encode($out);
    die();
