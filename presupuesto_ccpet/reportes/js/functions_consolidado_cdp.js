const URL ='presupuesto_ccpet/reportes/controllers/ConsolidadoCdpController.php';
const URLEXPORT ='presupuesto_ccpet/reportes/controllers/ConsolidadoCdpExportController.php';
var app = new Vue({
    el:"#myapp",
    data() {
        return {
            isLoading:false,
            txtFechaInicial:new Date(new Date().getFullYear(), 0, 1).toISOString().split("T")[0],
            txtFechaFinal:new Date().toISOString().split("T")[0],
            selectDependencia:"",
            selectTipo:"",
            selectArea:"",
            arrDependencias:[],
            arrAreas:[],
            arrTipos:[],
            arrInfo:[],
            arrTotal:[],
        }
    },
    mounted(){
        this.getData();
    },
    methods: {
        getData:async function(){
            const formData = new FormData();
            formData.append("action","get");
            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();
            this.arrDependencias = objData.dependencias;
            this.arrAreas = objData.areas;
            this.arrTipos = objData.tipos;
        },
        genData:async function(){
            const formData = new FormData();
            formData.append("action","gen");
            formData.append("fecha_inicial",this.txtFechaInicial);
            formData.append("fecha_final",this.txtFechaFinal);
            formData.append("area",this.selectArea);
            formData.append("dependencia",this.selectDependencia);
            formData.append("tipo",this.selectTipo);
            this.isLoading = true;
            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();
            this.isLoading = false;
            this.arrInfo = objData.datos;
            this.arrTotal = objData.totales;
        },
        exportData:function(){
            const form = document.createElement("form");
            form.method ="post";
            form.target="_blank";
            form.action=URLEXPORT;

            function addField(name,value){
                const input = document.createElement("input");
                input.type="hidden";
                input.name=name;
                input.value = value;
                form.appendChild(input);
            }
            addField("action","excel");
            addField("fecha_inicial",this.txtFechaInicial);
            addField("fecha_final",this.txtFechaFinal);
            addField("area",JSON.stringify(app.arrAreas.filter(function(e){return e.codigo == app.selectArea})[0]));
            addField("dependencia",JSON.stringify(app.arrDependencias.filter(function(e){return e.codigo == app.selectDependencia})[0]));
            addField("tipo",JSON.stringify(app.arrTipos.filter(function(e){return e.codigo == app.selectTipo})[0]));
            addField("data",JSON.stringify(this.arrInfo));
            addField("total",JSON.stringify(this.arrTotal));
            document.body.appendChild(form);
            form.submit();
            document.body.removeChild(form);
        },
    },
})
