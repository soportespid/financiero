<?php
    require_once '../../../Librerias/core/Helpers.php';
    require_once '../../../PHPExcel/Classes/PHPExcel.php';
    require_once '../models/ConsolidadoCdpModel.php';
    session_start();

    class ConsolidadoCdpController extends ConsolidadoCdpModel{
        public function getData(){
            if(!empty($_SESSION)){
                $arrData = [
                    "dependencias"=>$this->selectDependencias(),
                    "tipos"=>$this->selectTipos(),
                    "areas"=>$this->selectAreas()
                ];
                echo json_encode($arrData,JSON_UNESCAPED_UNICODE);
            }
            die();
        }
        public function genData(){
            if(!empty($_SESSION)){
                $strFechaInicial = strClean($_POST['fecha_inicial']);
                $strFechaFinal = strClean($_POST['fecha_final']);
                $strArea = strClean($_POST['area']);
                $strDependencia = strClean($_POST['dependencia']);
                $strTipo = strClean($_POST['tipo']);
                $request = $this->selectData($strFechaInicial,$strFechaFinal,$strArea,$strDependencia,$strTipo);
                $arrDatos = $request;
                $total = count($arrDatos);
                $totalCdp = 0;
                $totalRp = 0;
                $totalObligacion = 0;
                $totalPago = 0;
                for ($i=0; $i < $total ; $i++) {
                    $det = $arrDatos[$i];
                    $arrDatos[$i]['valor_cdp'] = $this->selectSaldoCdp(
                        $det['cdp'],
                        $det['vigencia'],
                        $det['cuenta'],
                        $det['fuente'],
                        $det['bpin'],
                        $det['indicador_producto'],
                        $det['dependencia'],
                        $det['area'],
                        $det['medio_pago'],
                        $det['vigencia_gasto'],
                        $det['valor_cdp']
                    );
                    $arrDatos[$i]['valor_rp'] = $this->selectSaldoRp(
                        $det['cdp'],
                        $det['vigencia'],
                        $det['cuenta'],
                        $det['fuente'],
                        $det['bpin'],
                        $det['indicador_producto'],
                        $det['dependencia'],
                        $det['area'],
                        $det['medio_pago'],
                        $det['vigencia_gasto']
                    );
                    $arrDatos[$i]['valor_obligacion'] = $this->selectSaldoObligacion(
                        $strFechaInicial,
                        $strFechaFinal,
                        $det['vigencia'],
                        $det['cuenta'],
                        $det['fuente'],
                        $det['bpin'],
                        $det['indicador_producto'],
                        $det['dependencia'],
                        $det['area'],
                        $det['medio_pago'],
                        $det['vigencia_gasto'],
                        $det['cdp']
                    );
                    $arrDatos[$i]['valor_egreso'] = $this->selectSaldoPagos(
                        $strFechaInicial,
                        $strFechaFinal,
                        $det['vigencia'],
                        $det['cuenta'],
                        $det['fuente'],
                        $det['bpin'],
                        $det['indicador_producto'],
                        $det['dependencia'],
                        $det['area'],
                        $det['medio_pago'],
                        $det['vigencia_gasto']
                    );
                    $arrDatos[$i]['format_cdp'] = formatNum($arrDatos[$i]['valor_cdp']);
                    $arrDatos[$i]['format_rp'] = formatNum($arrDatos[$i]['valor_rp']);
                    $arrDatos[$i]['format_obligacion'] = formatNum($arrDatos[$i]['valor_obligacion']);
                    $arrDatos[$i]['format_egreso'] = formatNum($arrDatos[$i]['valor_egreso']);
                    $arrDatos[$i]['descripcion'] = $det['nombre_cuenta'];
                    if($det['bpin'] != ""){
                        $arrDatos[$i]['rubro'] = $det['indicador_producto']."-".$det['bpin']."-".$det['fuente']."-".$det['dependencia']."-".$det['vigencia_gasto']."-".$det['medio_pago']."-".$det['area'];
                        $arrDatos[$i]['descripcion'] = $det['nombre_proyecto'];
                    }else if($det['bpin'] == "" && $det['productoservicio'] != ""){
                        $arrDatos[$i]['rubro'] = $det['cuenta']."-".$det['productoservicio']."-".$det['fuente'];
                    }else{
                        $arrDatos[$i]['rubro'] = $det['cuenta']."-".$det['fuente'];
                    }
                    $totalCdp += $arrDatos[$i]['valor_cdp'];
                    $totalRp += $arrDatos[$i]['valor_rp'];
                    $totalObligacion += $arrDatos[$i]['valor_obligacion'];
                    $totalPago += $arrDatos[$i]['valor_egreso'];
                }
                $arrData = [
                    "datos"=>$arrDatos,
                    "totales"=>[
                        "total_cdp"=>$totalCdp,
                        "total_rp"=>$totalRp,
                        "total_obligacion"=>$totalObligacion,
                        "total_egreso"=>$totalPago,
                        "format_cdp"=>formatNum($totalCdp),
                        "format_rp"=>formatNum($totalRp),
                        "format_obligacion"=>formatNum($totalObligacion),
                        "format_egreso"=>formatNum($totalPago)
                    ]
                ];
                echo json_encode($arrData,JSON_UNESCAPED_UNICODE);
            }
            die();
        }
    }

    if($_POST){
        $obj = new ConsolidadoCdpController();
        if($_POST['action'] == "get"){
            $obj->getData();
        }else if($_POST['action'] == "gen"){
            $obj->genData();
        }
    }

?>
