<?php
    require_once '../../../Librerias/core/Helpers.php';
    require_once '../../../PHPExcel/Classes/PHPExcel.php';
    session_start();
    /*ini_set('display_errors', '1');
    ini_set('display_startup_errors', '1');
    error_reporting(E_ALL);*/
    class ConsolidadoRpExportController {
        public function exportExcel(){
            if(!empty($_SESSION)){
                $arrData = json_decode($_POST['data'],true);
                if(!empty($arrData)){
                    $arrTotal = json_decode($_POST['total'],true);
                    $request = configBasica();
                    $strNit = $request['nit'];
                    $strRazon = $request['razonsocial'];
                    $arrArea = $_POST['area'] != "undefined" ? json_decode($_POST['area'],true) : [];
                    $arrDependencia = $_POST['dependencia'] != "undefined" ? json_decode($_POST['dependencia'],true) : [];
                    $arrTipo = $_POST['tipo'] != "undefined" ? json_decode($_POST['tipo'],true) : [];
                    $arrFechaInicial = explode("-",$_POST['fecha_inicial']);
                    $arrFechaFinal = explode("-",$_POST['fecha_final']);
                    $strFechaInicial = $arrFechaInicial[2]."/".$arrFechaInicial[1]."/".$arrFechaInicial[0];
                    $strFechaFinal = $arrFechaFinal[2]."/".$arrFechaFinal[1]."/".$arrFechaFinal[0];
                    $fileName = "reporte_consolidado_rp_".$arrFechaInicial[2].$arrFechaInicial[1].$arrFechaInicial[0].$arrFechaFinal[2].$arrFechaFinal[1].$arrFechaFinal[0].".xlsx";
                    $objPHPExcel = new PHPExcel();
                    $objPHPExcel->getActiveSheet()->getStyle('A:M')->applyFromArray(
                        array(
                            'alignment' => array(
                                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
                            )
                        )
                    );
                    $objPHPExcel->getProperties()
                    ->setCreator("IDEAL 10")
                    ->setLastModifiedBy("IDEAL 10")
                    ->setTitle("Exportar Excel con PHP")
                    ->setSubject("Documento de prueba")
                    ->setDescription("Documento generado con PHPExcel")
                    ->setKeywords("usuarios phpexcel")
                    ->setCategory("reportes");

                    //----Cuerpo de Documento----
                    $objPHPExcel->setActiveSheetIndex(0)
                    ->mergeCells('A1:M1')
                    ->mergeCells('A2:M2')
                    ->mergeCells('B3:M3')
                    ->mergeCells('B4:M4')
                    ->mergeCells('B5:M5')
                    ->mergeCells('B6:M6')
                    ->setCellValue('A1', 'PRESUPUESTO - '.$strRazon)
                    ->setCellValue('A2', 'CONSOLIDADO DE REGISTROS PRESUPUESTALES')
                    ->setCellValue('A3', 'FECHA')
                    ->setCellValue('A4', 'DEPENDENCIA')
                    ->setCellValue('A5', 'TIPO')
                    ->setCellValue('A6', 'AREA')
                    ->setCellValue('B3', "Desde ".$strFechaInicial." hasta ".$strFechaFinal)
                    ->setCellValue('B4', empty($arrDependencia) ? "Todas" : $arrDependencia['codigo']."-".$arrDependencia['nombre'])
                    ->setCellValue('B5', empty($arrTipo) ? "Todas" : $arrTipo['codigo']."-".$arrTipo['nombre'])
                    ->setCellValue('B6', empty($arrArea) ? "Todas" : $arrArea['codigo']."-".$arrArea['nombre']);
                    $objPHPExcel-> getActiveSheet ()
                    -> getStyle ("A3")
                    -> getFill ()
                    -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
                    -> getStartColor ()
                    -> setRGB ('3399cc');
                    $objPHPExcel-> getActiveSheet ()
                    -> getStyle ("A4")
                    -> getFill ()
                    -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
                    -> getStartColor ()
                    -> setRGB ('3399cc');
                    $objPHPExcel-> getActiveSheet ()
                    -> getStyle ("A5")
                    -> getFill ()
                    -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
                    -> getStartColor ()
                    -> setRGB ('3399cc');
                    $objPHPExcel-> getActiveSheet ()
                    -> getStyle ("A6")
                    -> getFill ()
                    -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
                    -> getStartColor ()
                    -> setRGB ('3399cc');
                    $objPHPExcel-> getActiveSheet ()
                    -> getStyle ("A1")
                    -> getFill ()
                    -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
                    -> getStartColor ()
                    -> setRGB ('3399cc');
                    $objPHPExcel-> getActiveSheet ()
                    -> getStyle ("A1:A2")
                    -> getFont ()
                    -> setBold ( true )
                    -> setName ( 'Verdana' )
                    -> setSize ( 10 )
                    -> getColor ()
                    -> setRGB ('000000');
                    $objPHPExcel-> getActiveSheet ()
                    -> getStyle ('A1:A2')
                    -> getAlignment ()
                    -> applyFromArray (array ( 'horizontal'  =>  PHPExcel_Style_Alignment :: HORIZONTAL_CENTER ,) );
                    $objPHPExcel-> getActiveSheet ()
                    -> getStyle ('A4:M4')
                    -> getAlignment ()
                    -> applyFromArray (array ( 'horizontal'  =>  PHPExcel_Style_Alignment :: VERTICAL_JUSTIFY ,) );
                    $objPHPExcel-> getActiveSheet ()
                    -> getStyle ("A2")
                    -> getFill ()
                    -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID);

                    $borders = array(
                        'borders' => array(
                            'allborders' => array(
                                'style' => PHPExcel_Style_Border::BORDER_THIN,
                            'color' => array('argb' => 'FF000000'),
                            )
                        ),
                    );
                    $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A7', 'RP')
                    ->setCellValue('B7', "Rubro")
                    ->setCellValue('C7', "Descripción")
                    ->setCellValue('D7', "Fecha compromiso")
                    ->setCellValue('E7', "CDP")
                    ->setCellValue('F7', "Documento")
                    ->setCellValue('G7', "Nombre")
                    ->setCellValue('H7', "Objeto")
                    ->setCellValue('I7', "Compromiso")
                    ->setCellValue('J7', "Obligaciones")
                    ->setCellValue('K7', "Saldo compromiso")
                    ->setCellValue('L7', "Pagos")
                    ->setCellValue('M7', "Por pagar");
                    $objPHPExcel-> getActiveSheet ()
                        -> getStyle ("A7:M7")
                        -> getFill ()
                        -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
                        -> getStartColor ()
                        -> setRGB ('99ddff');

                    $objPHPExcel->getActiveSheet()->getStyle("A7:M7")->getFont()->setBold(true);
                    $objPHPExcel->getActiveSheet()->getStyle('A1:M1')->applyFromArray($borders);
                    $objPHPExcel->getActiveSheet()->getStyle('A2:M2')->applyFromArray($borders);
                    $objPHPExcel->getActiveSheet()->getStyle('A3:M3')->applyFromArray($borders);
                    $objPHPExcel->getActiveSheet()->getStyle('A4:M4')->applyFromArray($borders);
                    $objPHPExcel->getActiveSheet()->getStyle('A5:M5')->applyFromArray($borders);
                    $objPHPExcel->getActiveSheet()->getStyle('A6:M6')->applyFromArray($borders);
                    $objPHPExcel->getActiveSheet()->getStyle('A7:M7')->applyFromArray($borders);

                    $objPHPExcel->getActiveSheet()->getStyle("A1")->getFont()->getColor()->setRGB("ffffff");
                    $objPHPExcel->getActiveSheet()->getStyle("A3")->getFont()->getColor()->setRGB("ffffff");
                    $objPHPExcel->getActiveSheet()->getStyle("A4")->getFont()->getColor()->setRGB("ffffff");
                    $objPHPExcel->getActiveSheet()->getStyle("A5")->getFont()->getColor()->setRGB("ffffff");
                    $objPHPExcel->getActiveSheet()->getStyle("A6")->getFont()->getColor()->setRGB("ffffff");

                    $objPHPExcel->getActiveSheet()->getStyle("A3")->getFont()->setBold(true);
                    $objPHPExcel->getActiveSheet()->getStyle("A4")->getFont()->setBold(true);
                    $objPHPExcel->getActiveSheet()->getStyle("A5")->getFont()->setBold(true);
                    $objPHPExcel->getActiveSheet()->getStyle("A6")->getFont()->setBold(true);

                    $row = 8;
                    foreach ($arrData as $data) {
                        $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValueExplicit ("A$row", $data['rp'], PHPExcel_Cell_DataType :: TYPE_STRING)
                        ->setCellValueExplicit ("B$row", $data['rubro'], PHPExcel_Cell_DataType :: TYPE_STRING)
                        ->setCellValueExplicit ("C$row", $data['descripcion'], PHPExcel_Cell_DataType :: TYPE_STRING)
                        ->setCellValueExplicit ("D$row", $data['fecha'], PHPExcel_Cell_DataType :: TYPE_STRING)
                        ->setCellValueExplicit ("E$row", $data['cdp'], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
                        ->setCellValueExplicit ("F$row", $data['documento'], PHPExcel_Cell_DataType :: TYPE_STRING)
                        ->setCellValueExplicit ("G$row", $data['nombre_tercero'], PHPExcel_Cell_DataType :: TYPE_STRING)
                        ->setCellValueExplicit ("H$row", $data['objeto'], PHPExcel_Cell_DataType :: TYPE_STRING)
                        ->setCellValueExplicit ("I$row", $data['valor_rp'], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
                        ->setCellValueExplicit ("J$row", $data['valor_obligacion'], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
                        ->setCellValueExplicit ("K$row", $data['valor_rp']-$data['valor_obligacion'], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
                        ->setCellValueExplicit ("L$row", $data['valor_egreso'], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
                        ->setCellValueExplicit ("M$row", $data['valor_obligacion']-$data['valor_egreso'], PHPExcel_Cell_DataType :: TYPE_NUMERIC);
                        $objPHPExcel->getActiveSheet()->getStyle("A$row:M$row")->applyFromArray($borders);
                        $row++;
                    }

                    $objPHPExcel->setActiveSheetIndex(0)
                    ->mergeCells("A$row:H$row")
                    ->setCellValue("A$row","Total:")
                    ->setCellValue("I$row",$arrTotal['total_rp'])
                    ->setCellValue("J$row",$arrTotal['total_obligacion'])
                    ->setCellValue("K$row",$arrTotal['total_saldo'])
                    ->setCellValue("L$row",$arrTotal['total_egreso'])
                    ->setCellValue("M$row",$arrTotal['total_pagar']);

                    $objPHPExcel-> getActiveSheet ()
                        -> getStyle ("A$row:M$row")
                        -> getFill ()
                        -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
                        -> getStartColor ()
                        -> setRGB ('99ddff');
                    $objPHPExcel->getActiveSheet()->getStyle("A$row:M$row")->applyFromArray($borders);
                    $objPHPExcel->getActiveSheet()->getStyle("A$row:M$row")->getFont()->setBold(true);

                    $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
                    $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
                    $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(50);
                    $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
                    $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
                    $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
                    $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(50);
                    $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(50);
                    $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
                    $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);
                    $objPHPExcel->getActiveSheet()->getColumnDimension('K')->setAutoSize(true);
                    $objPHPExcel->getActiveSheet()->getColumnDimension('L')->setAutoSize(true);
                    $objPHPExcel->getActiveSheet()->getColumnDimension('M')->setAutoSize(true);

                    //----Guardar documento----
                    header('Content-Type: application/vnd.ms-excel');
                    header('Content-Disposition: attachment;filename="'.$fileName.'"');
                    header('Cache-Control: max-age=0');
                    $objWriter=PHPExcel_IOFactory::createWriter($objPHPExcel,'Excel2007');
                    $objWriter->save('php://output');
                }
            }
            die();
        }
    }

    if($_POST){
        $obj = new ConsolidadoRpExportController();
        if($_POST['action'] == "excel"){
            $obj->exportExcel();
        }
    }

?>
