<?php

    require_once '../../comun.inc';
    require '../../funciones.inc';
 
    session_start();

	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");

    $maxVersion = ultimaVersionIngresosCCPET();

    $out = array('error' => false);

    $action = "show";

    if(isset($_GET['action'])){
        $action = $_GET['action'];
    }

    
    
    if($action == 'show'){

        $rubrosInicial = [];
        $accounts = [];
        $fuentes = [];

        $vigencia = $_POST['vigencia'];

        $sqlrIni = "SELECT DISTINCT cuenta, fuente FROM ccpetinicialing WHERE vigencia = '".$vigencia."'";
        $resIni = mysqli_query($linkbd, $sqlrIni);
        while($rowIni = mysqli_fetch_row($resIni)){
            $rubro = '';
            $rubroNom = [];
            $rubro = $rowIni[0]."-".$rowIni[1];
            /* $nombre = buscacuentaccpetgastos($rowIni[0], $maxVersion)." - ".buscaNombreFuenteCCPET($rowIni[1]); */
            array_push($rubroNom, $rubro);
            array_push($rubrosInicial, $rubroNom);
        }

        $sqlrAd = "SELECT DISTINCT cuenta, fuente FROM ccpet_adiciones WHERE vigencia = '".$vigencia."' AND cuenta != '' AND tipo_cuenta = 'I'";
        $respAd = mysqli_query($linkbd, $sqlrAd);
        while($rowAd = mysqli_fetch_row($respAd)){
            $rubro = '';
            $rubroNom = [];
            $rubro = $rowAd[0]."-".$rowAd[1];
            /* $nombre = buscacuentaccpetgastos($rowAd[0], $maxVersion)." - ".buscaNombreFuenteCCPET($rowAd[1]); */
            array_push($rubroNom, $rubro);
            array_push($rubrosInicial, $rubroNom);
        }

        $sqlrEjecucion = "SELECT tsad.rubro, tsad.fuente FROM tesoreservas AS tsa, tesoreservas_det AS tsad WHERE tsa.estado = 'S' AND tsa.id = tsad.id_reserva AND tsa.vigencia = '".$vigencia."' UNION SELECT tsad.rubro, tsad.fuente FROM tesosuperavit AS tsa, tesosuperavit_det AS tsad WHERE tsa.estado = 'S' AND tsa.id = tsad.id_tesosuperavit AND tsa.vigencia = '".$vigencia."' UNION SELECT cuenta, fuente FROM pptorecibocajappto WHERE vigencia = '".$vigencia."' UNION SELECT cuenta, fuente FROM pptosinrecibocajappto WHERE vigencia = '".$vigencia."' UNION SELECT cuenta, fuente FROM pptonotasbanppto WHERE vigencia = '".$vigencia."' UNION SELECT cuenta, fuente FROM pptoingtranppto WHERE vigencia = '".$vigencia."' UNION SELECT cuenta, fuente FROM pptoretencionpago WHERE vigencia = '".$vigencia."' UNION SELECT cuenta, fuente FROM srv_recaudo_factura_ppto WHERE vigencia = '".$vigencia."'";

        $respEjecucion = mysqli_query($linkbd, $sqlrEjecucion);
        while($rowEjecucion = mysqli_fetch_row($respEjecucion)){
            $rubro = '';
            $rubroNom = [];
            $rubro = $rowEjecucion[0]."-".$rowEjecucion[1];
            array_push($rubroNom, $rubro);
            array_push($rubrosInicial, $rubroNom);
        }


        $sqlrAccounts = "SELECT codigo, nombre, tipo, nivel FROM cuentasingresosccpet WHERE version = $maxVersion";
        $respAccounts = mysqli_query($linkbd, $sqlrAccounts);
        while($rowAccounts = mysqli_fetch_row($respAccounts)){
            array_push($accounts, $rowAccounts);
        }

        $sqlrFuentes = "SELECT codigo_fuente, nombre FROM ccpet_fuentes_cuipo";
        $respFuentes = mysqli_query($linkbd, $sqlrFuentes);
        while($rowFuentes = mysqli_fetch_row($respFuentes)){
            array_push($fuentes, $rowFuentes);
        }

        $sqlrSGR = "SELECT rubro, cuentasgr FROM rubros_cuentasingsgr";
        $respSGR = mysqli_query($linkbd, $sqlrSGR);
        while($rowSGR = mysqli_fetch_row($respSGR)){
            $cuentaSelSGR[$rowSGR[0]] = $rowSGR[1];
        }

        $out['rubrosInicial'] = $rubrosInicial;
        $out['accounts'] = $accounts;
        $out['fuentes'] = $fuentes;
        $out['cuentaSelSGR'] = $cuentaSelSGR;
        
    }

    if($action == "buscaRubrosClasificador"){
        $clasificadorSelDet = [];

        if(isset($_POST['id_clasificador']) && $_POST['id_clasificador'] != ''){
            $id_clasificador = $_POST['id_clasificador'];
            $sqlr = "SELECT rubros, nombre, tipo FROM ccpet_clasificadorgastos_det WHERE id_clasificador = $id_clasificador AND tipo = 'C' ORDER BY rubros ASC";
            $res = mysqli_query($linkbd, $sqlr);
        
            while ($row = mysqli_fetch_row($res)) {
                array_push($clasificadorSelDet, $row);
            }
        }
        $out['clasificadorSelDet'] = $clasificadorSelDet;
    }

    if($action == 'cargarCuentas'){

        $cuentasSGR = [];
        $sqlr = "SELECT * FROM cuentassgr ORDER BY id ASC";
        $res = mysqli_query($linkbd, $sqlr);
        while($row = mysqli_fetch_row($res)){
            array_push($cuentasSGR, $row);
        }

        $out['cuentasSGR'] = $cuentasSGR;

    }


    if($action == 'filtrarCuentas'){

        $keywordCuenta=$_POST['keywordCuenta'];

        $cuentasSGR = array();

        $sqlr = "SELECT * FROM cuentassgr WHERE concat_ws(' ', codigo, nombre) LIKE '%$keywordCuenta%' ORDER BY id ASC";//echo $sqlr;
        $res = mysqli_query($linkbd, $sqlr);
        while($row = mysqli_fetch_row($res)){
            array_push($cuentasSGR, $row);
        }

        $out['cuentasSGR'] = $cuentasSGR;
    }

    if($action == 'cargarTiposRecursos'){

        $tiposRecursosSGR = [];
        $sqlr = "SELECT * FROM sgr_tipos_recursos ORDER BY id ASC";
        $res = mysqli_query($linkbd, $sqlr);
        while($row = mysqli_fetch_row($res)){
            array_push($tiposRecursosSGR, $row);
        }

        $out['tiposRecursosSGR'] = $tiposRecursosSGR;

    }

    if($action == 'filtrarTiposRecursos'){

        $keywordTipoRecurso=$_POST['keywordTipoRecurso'];

        $tiposRecursosSGR = array();

        $sqlr = "SELECT * FROM sgr_tipos_recursos WHERE concat_ws(' ', codigo, nombre) LIKE '%$keywordTipoRecurso%' ORDER BY id ASC";//echo $sqlr;
        $res = mysqli_query($linkbd, $sqlr);
        while($row = mysqli_fetch_row($res)){
            array_push($tiposRecursosSGR, $row);
        }

        $out['tiposRecursosSGR'] = $tiposRecursosSGR;
    }

    if($action == 'cargarTerceros'){

        $tercerosSGR = [];
        $sqlr = "SELECT * FROM sgr_terceros ORDER BY id ASC";
        $res = mysqli_query($linkbd, $sqlr);
        while($row = mysqli_fetch_row($res)){
            array_push($tercerosSGR, $row);
        }

        $out['tercerosSGR'] = $tercerosSGR;

    }

    if($action == 'filtrarTerceros'){

        $keywordTercero=$_POST['keywordTercero'];

        $tercerosSGR = array();

        $sqlr = "SELECT * FROM sgr_terceros WHERE concat_ws(' ', codigo, nombre) LIKE '%$keywordTercero%' ORDER BY id ASC";//echo $sqlr;
        $res = mysqli_query($linkbd, $sqlr);
        while($row = mysqli_fetch_row($res)){
            array_push($tercerosSGR, $row);
        }

        $out['tercerosSGR'] = $tercerosSGR;
    }

    if($action == 'cargarPoliticasPublicas'){

        $politicasPublicasSGR = [];
        $sqlr = "SELECT * FROM ccpet_politicapublica ORDER BY codigo ASC";
        $res = mysqli_query($linkbd, $sqlr);
        while($row = mysqli_fetch_row($res)){
            array_push($politicasPublicasSGR, $row);
        }

        $out['politicasPublicasSGR'] = $politicasPublicasSGR;

    }

    if($action == 'filtrarPoliticasPublicas'){

        $keywordPoliticaPublica=$_POST['keywordPoliticaPublica'];

        $politicasPublicasSGR = array();

        $sqlr = "SELECT * FROM ccpet_politicapublica WHERE concat_ws(' ', codigo, nombre) LIKE '%$keywordPoliticaPublica%' ORDER BY codigo ASC";//echo $sqlr;
        $res = mysqli_query($linkbd, $sqlr);
        while($row = mysqli_fetch_row($res)){
            array_push($politicasPublicasSGR, $row);
        }

        $out['politicasPublicasSGR'] = $politicasPublicasSGR;
    }

    if($action == 'guardarCab'){

        $user = $_SESSION['nickusu'];
        $nombre = $_POST['nombre_clasificador'];

        date_default_timezone_set('America/Bogota');
        $fecha_creacion = date("Y-m-d");
        $estado = 1; 

        $sql = "INSERT INTO ccpet_clasificadorgastos_cab(nombre, estado,user, fecha_creacion) VALUES ('$nombre', '$estado', '$user', '$fecha_creacion')";
        $res = mysqli_query($linkbd, $sql);
        $id_cab = mysqli_insert_id($linkbd);
    
        $out['id_cab'] =  $id_cab;
    }


    header("Content-type: application/json");
    echo json_encode($out);
    die();