
const URL = 'presupuesto_ccpet/reportes/ccp-fut_resgistros.php';
import { ordenarArbol, buscaNombreRubros, filtroEnArrayDeObjetos } from '../../funciones.js'

const app = Vue.createApp({
    data() {
      return {
        results: [],
        results_seleccionados: [],
        results_seleccionadosOrd: [],
        show_table_search: false,
        show_resultados: true,
        show_resultados_seleccionados: false,
        get_code: [],
        get_code_1: [],
        myStyle: {
            backgroundColor: '#12DB67',
            color: 'white'
        },
        myStyleMayor: {
            'font-weight': 'bold',
        },

        loading: false,
        rubrosInicial: [],
        accounts: [],
        accountsAux: [],
        accountsMayor: [],
        fuentes: [],
        arbol: [],
        arbolInv: [],
        rubrosInv: [],
        rubrosAdInv: [],
        rubrosTrasladosCredInv: [],
        nombreRubrosInv: [],
        buscar_rubro: '',
        resultsBuscar: [],
        arbolGeneral:[],
        vigencia: 0,
        years:[],
        checkTodo: false,

        showModal_cuentas: false,

        cuentaBuscada: '',
        cuentasSGR : [],
        cuentaBuscada: [],
        searchCuenta : {keywordCuenta: ''},
        registro: [],

        puedeGuardar: false,

        fuentesSGR : [],
        showModal_fuentes: false,
        searchFuente : {keywordFuente: ''},

        tiposRecursosSGR : [],
        showModal_tiposRecursos: false,
        searchTipoRecurso : {keywordTipoRecurso: ''},

        tercerosSGR : [],
        showModal_terceros: false,
        searchTercero : {keywordTercero: ''},

        rubroVentanaEmergente: '',

      }
    },

    async mounted() {
        this.loading = false;
        await this.cargayears();
        this.cargarParametros();
    },

    computed: {
        /* mostrarCheck(){
            return this.buscar_rubro.length > 2 ? true : false
        }, */
    },

    methods: {

        cargayears: async function(){
			await axios.post('vue/presupuesto_ccp/ccp-bancoproyectos.php?buscar=years')
			.then(
				(response)=>
				{
					this.years=response.data.anio;
					var idanio=response.data.anio.length -1;
					if (idanio>=0){this.vigencia = response.data.anio[idanio][0];}
					else{this.vigencia ='';}
				}
            );
            
		},

        async cargarParametros(){

            this.loading = true;

            let formData = new FormData();
            formData.append("vigencia", this.vigencia);
            
            await axios.post(URL, formData)
            .then((response) => {
                this.accounts = response.data.accounts;
                this.fuentes = response.data.fuentes;
                this.rubrosInv = response.data.rubrosInv;
                this.rubrosAdInv = response.data.rubrosAdInv;
                this.rubrosTrasladosCredInv = response.data.rubrosTrasladosCredInv;
                this.nombreRubrosInv = response.data.nombreRubrosInv;
                if(response.data.registro)
                    this.registro = response.data.registro;

            }).catch((error) => {
                this.error = true;
                console.log(error)
            }).finally(() => {
                this.loading = false;
                this.orderAccounts();
            });
        },

        async orderAccounts(){

            const rubrosInv = this.rubrosInv.concat(this.rubrosAdInv, this.rubrosTrasladosCredInv);
            const arrPuroInv = rubrosInv.map((e) => {
                return e[0]
            });

            const dataArrInvSet = new Set(arrPuroInv);
            const dataArrInv = Array.from(dataArrInvSet);

            const nombresRubrosInv = this.nombreRubrosInv.concat(this.fuentes);

            this.rubrosJoinInv = buscaNombreRubros(dataArrInv, nombresRubrosInv);

            this.arbolGeneral = this.nombreRubrosInv;

            this.results = this.rubrosJoinInv;
            this.resultsBuscar = [...this.results];

        },


        estaEnArray(codigos){
            const parsedobj = JSON.stringify(this.results_seleccionados);
            return parsedobj.includes(codigos[0])
        },

        removeItemFromArr(item){
            var i = this.results_seleccionados.indexOf( item );
                
            if ( i !== -1 ) {
                this.results_seleccionados.splice( i, 1 );
            }

        },

        seleccionaCodigosCheck(codigos){

            const parsedobj = JSON.stringify(this.results_seleccionados);

            if(parsedobj.includes(codigos[0])){
                this.removeItemFromArr(codigos);
            }else{
                this.results_seleccionados.push(codigos);
            }
        },

        async ordenarArbolCheck(){
            this.results_seleccionadosOrd = await ordenarArbol(this.arbolGeneral, this.results_seleccionados);
            
            this.show_resultados_seleccionados = this.results_seleccionados.length > 0 ? true : false;
        },

        async buscarRubro(){

            this.results = [];
            this.checkTodo = false;
            var text = this.buscar_rubro;
            const data = this.resultsBuscar;
            this.results = await filtroEnArrayDeObjetos({'data': data, 'text': text});

        },

        async guardarRubroSGR(){
            const objectValues = Object.values(this.registro);
            const objectkeys = Object.keys(this.registro);
            const tamObjectValues = objectValues.length;
            if(tamObjectValues > 0){
                
                Swal.fire({
                    title: 'Esta seguro de guardar?',
                    text: "Guardar clasificador presupuestal de gastos, confirmar campos!",
                    icon: 'question',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Si, guardar!'
                }).then((result) => {
                    if (result.isConfirmed) {

                        this.loading = true;

                        var formData = new FormData();

                        for(let i=0; i <= tamObjectValues-1; i++){

                            formData.append("cuentasSGR["+i+"]", objectValues[i]);
                            formData.append("rubros["+i+"]", objectkeys[i]);
                            
                        }
                        
                        axios.post(URL+'?action=guardarRubroSGR', formData)
                        .then((response) => {
                            console.log(response.data);
                            if(response.data.insertaBien){
                                Swal.fire({
                                    position: 'top-end',
                                    icon: 'success',
                                    title: 'Los rubros SGR se guardaron con Exito',
                                    showConfirmButton: false,
                                    timer: 1500
                                }).then((response) => {
                                        this.redireccionar();
                                    });
                            }else{
                                
                                Swal.fire(
                                    'Error!',
                                    'No se pudo guardar.',
                                    'error'
                                );
                            }

                        }).catch((error) => {
                            this.error = true;
                            console.log(error)
                        }).finally(() => {
                            this.loading = false;
                        });
                    }
                })

            }else{
                Swal.fire(
                    'Falta seleccionar rubros.',
                    'Seleccione los rubros con los que desea crear el clasificador dando clic en el listado.',
                    'warning'
                )
            }
            
        },


        async agregarRubroSGR(cuentaBuscada, nombreCuentaBuscada){

            this.rubroVentanaEmergente = cuentaBuscada + ' - ' + nombreCuentaBuscada;
            this.cuentaBuscada = cuentaBuscada;
            await axios.post(URL+'?action=cargarCuentas')
                .then((response) => {
                    this.cuentasSGR = response.data.cuentasSGR;
                }).finally(() => {
                    this.showModal_cuentas = true;
                });
        },

        

        seleccionarCuenta(cuentaSelec){
            if(cuentaSelec[3] == 'C'){

                this.registro[this.cuentaBuscada] = cuentaSelec [1];
                this.showModal_cuentas = false;
                this.puedeGuardar = true;
                
            }else{
                
                
                this.showModal_cuentas = false;
                Swal.fire(
                'Tipo de cuenta incorrecto.',
                'Escoger una cuenta de captura (C)',
                'warning'
                ).then((result) => {
                    
                    if(result.isConfirmed || result.isDismissed){
                        this.showModal_cuentas = true;
                    }
               
                });
               
                
            }
        },

        searchMonitorCuenta(){
            let keywordCuenta = this.toFormData(this.searchCuenta);
            
            axios.post(URL+'?action=filtrarCuentas', keywordCuenta)
            .then((response) => {
                this.cuentasSGR = response.data.cuentasSGR;
                
            });
            
        },

        async agregarFuenteSGR(cuentaBuscada, nombreCuentaBuscada){
            this.rubroVentanaEmergente = cuentaBuscada + ' - ' + nombreCuentaBuscada;
            this.cuentaBuscada = cuentaBuscada;
            await axios.post(URL+'?action=cargarFuentes')
                .then((response) => {
                    this.fuentesSGR = response.data.fuentesSGR;
                }).finally(() => {
                    this.showModal_fuentes = true;
                });
        },

        seleccionarFuente(fuenteSelec){
            if(this.registro){
                this.registro[this.cuentaBuscada + '-1'] = fuenteSelec[1];
            }
            /* this.registro[this.cuentaBuscada + '-1'] = fuenteSelec[1]; */
            this.showModal_fuentes = false;
            this.puedeGuardar = true;
            
        },

        searchMonitorFuente(){
            let keywordFuente = this.toFormData(this.searchFuente);
            
            axios.post(URL+'?action=filtrarFuentes', keywordFuente)
            .then((response) => {
                this.fuentesSGR = response.data.fuentesSGR;
            });
            
        },

        async agregarTipoRecursoSGR(cuentaBuscada, nombreCuentaBuscada){
            this.rubroVentanaEmergente = cuentaBuscada + ' - ' + nombreCuentaBuscada;
            this.cuentaBuscada = cuentaBuscada;
            await axios.post(URL+'?action=cargarTiposRecursos')
                .then((response) => {
                    this.tiposRecursosSGR = response.data.tiposRecursosSGR;
                }).finally(() => {
                    this.showModal_tiposRecursos = true;
                });
        },

        seleccionarTipoRecurso(tipoRecursoSelec){
         
            if(this.registro){
                this.registro[this.cuentaBuscada + '-2'] = tipoRecursoSelec[1];
            }
            /* this.registro[this.cuentaBuscada + '-2'] = tipoRecursoSelec[1]; */
            this.showModal_tiposRecursos = false;
            this.puedeGuardar = true;
            
        },

        searchMonitorTipoRecurso(){
            let keywordTipoRecurso = this.toFormData(this.searchTipoRecurso);
            
            axios.post(URL+'?action=filtrarTiposRecursos', keywordTipoRecurso)
            .then((response) => {
                this.tiposRecursosSGR = response.data.tiposRecursosSGR;
            });
            
        },

        async agregarTerceroSGR(cuentaBuscada, nombreCuentaBuscada){
            this.rubroVentanaEmergente = cuentaBuscada + ' - ' + nombreCuentaBuscada;
            this.cuentaBuscada = cuentaBuscada;
            await axios.post(URL+'?action=cargarTerceros')
                .then((response) => {
                    this.tercerosSGR = response.data.tercerosSGR;
                }).finally(() => {
                    this.showModal_terceros = true;
                });
        },

        seleccionarTercero(terceroSelec){
         
            if(this.registro){
                this.registro[this.cuentaBuscada + '-3'] = terceroSelec[1];
            }
            /* this.registro[this.cuentaBuscada + '-3'] = terceroSelec[1]; */
            this.showModal_terceros = false;
            this.puedeGuardar = true;
            
        },

        searchMonitorTercero(){
            let keywordTercero = this.toFormData(this.searchTercero);
            
            axios.post(URL+'?action=filtrarTerceros', keywordTercero)
            .then((response) => {
                this.tercerosSGR = response.data.tercerosSGR;
            });
            
        },

        redireccionar(){
            
            location.href ="ccp-homologarcuentassgr.php";
        },

        toFormData(obj){
            var form_data = new FormData();
            for(var key in obj){
                form_data.append(key, obj[key]);
            }
            return form_data;
        },
    }
})

app.mount('#myapp')