<?php

    require_once '../../comun.inc';
    require '../../funciones.inc';
 
    session_start();

	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");

    $maxVersion = ultimaVersionIngresosCCPET();

    $out = array('error' => false);

    $action = "show";

    if(isset($_GET['action'])){
        $action = $_GET['action'];
    }

    
    
    if($action == 'show'){

        $rubrosInicial = [];
        $rubrosAd = [];
        $accounts = [];
        $fuentes = [];

        $vigencia = $_POST['vigencia'];

        $sqlrIni = "SELECT DISTINCT cuenta, fuente FROM ccpetinicialing WHERE vigencia = '".$vigencia."'";
        $resIni = mysqli_query($linkbd, $sqlrIni);
        while($rowIni = mysqli_fetch_row($resIni)){
            $rubro = '';
            $rubroNom = [];
            $rubro = $rowIni[0]."-".$rowIni[1]."-INICIAL";
            /* $nombre = buscacuentaccpetgastos($rowIni[0], $maxVersion)." - ".buscaNombreFuenteCCPET($rowIni[1]); */
            array_push($rubroNom, $rubro);
            array_push($rubrosInicial, $rubroNom);
        }

        $sqlrAd = "SELECT DISTINCT cuenta, fuente FROM ccpet_adiciones WHERE vigencia = '".$vigencia."' AND cuenta != '' AND tipo_cuenta = 'I'";
        $respAd = mysqli_query($linkbd, $sqlrAd);
        while($rowAd = mysqli_fetch_row($respAd)){
            $rubro = '';
            $rubroNom = [];
            $rubro = $rowAd[0]."-".$rowAd[1]."-ADICION";
            /* $nombre = buscacuentaccpetgastos($rowAd[0], $maxVersion)." - ".buscaNombreFuenteCCPET($rowAd[1]); */
            array_push($rubroNom, $rubro);
            array_push($rubrosAd, $rubroNom);
        }

        $sqlrAccounts = "SELECT codigo, nombre, tipo, nivel FROM cuentasingresosccpet WHERE version = $maxVersion";
        $respAccounts = mysqli_query($linkbd, $sqlrAccounts);
        while($rowAccounts = mysqli_fetch_row($respAccounts)){
            array_push($accounts, $rowAccounts);
        }

        $sqlrFuentes = "SELECT codigo_fuente, nombre FROM ccpet_fuentes_cuipo";
        $respFuentes = mysqli_query($linkbd, $sqlrFuentes);
        while($rowFuentes = mysqli_fetch_row($respFuentes)){
            array_push($fuentes, $rowFuentes);
        }

        $sqlrSGR = "SELECT rubro, cuentasgr FROM rubros_cuentasingejecusgr";
        $respSGR = mysqli_query($linkbd, $sqlrSGR);
        while($rowSGR = mysqli_fetch_row($respSGR)){
            $cuentaSelSGR[$rowSGR[0]] = $rowSGR[1];
        }

        $out['rubrosInicial'] = $rubrosInicial;
        $out['rubrosAd'] = $rubrosAd;
        $out['accounts'] = $accounts;
        $out['fuentes'] = $fuentes;
        $out['cuentaSelSGR'] = $cuentaSelSGR;
        
    }

    if($action == "buscaRubrosClasificador"){
        $clasificadorSelDet = [];

        if(isset($_POST['id_clasificador']) && $_POST['id_clasificador'] != ''){
            $id_clasificador = $_POST['id_clasificador'];
            $sqlr = "SELECT rubros, nombre, tipo FROM ccpet_clasificadorgastos_det WHERE id_clasificador = $id_clasificador AND tipo = 'C' ORDER BY rubros ASC";
            $res = mysqli_query($linkbd, $sqlr);
        
            while ($row = mysqli_fetch_row($res)) {
                array_push($clasificadorSelDet, $row);
            }
        }
        $out['clasificadorSelDet'] = $clasificadorSelDet;
    }

    if($action == 'cargarCuentas'){

        $cuentasSGR = [];
        $sqlr = "SELECT * FROM cuentassgr ORDER BY id ASC";
        $res = mysqli_query($linkbd, $sqlr);
        while($row = mysqli_fetch_row($res)){
            array_push($cuentasSGR, $row);
        }

        $out['cuentasSGR'] = $cuentasSGR;

    }


    if($action == 'filtrarCuentas'){

        $keywordCuenta=$_POST['keywordCuenta'];

        $cuentasSGR = array();

        $sqlr = "SELECT * FROM cuentassgr WHERE concat_ws(' ', codigo, nombre) LIKE '%$keywordCuenta%' ORDER BY id ASC";//echo $sqlr;
        $res = mysqli_query($linkbd, $sqlr);
        while($row = mysqli_fetch_row($res)){
            array_push($cuentasSGR, $row);
        }

        $out['cuentasSGR'] = $cuentasSGR;
    }

    if($action == 'cargarBpim'){

        $bpimSGR = [];
        $sqlr = "SELECT codigo, nombre FROM ccpproyectospresupuesto ORDER BY id ASC";
        $res = mysqli_query($linkbd, $sqlr);
        while($row = mysqli_fetch_row($res)){
            array_push($bpimSGR, $row);
        }

        $out['bpimSGR'] = $bpimSGR;

    }

    if($action == 'filtrarBpim'){

        $keywordBpim=$_POST['keywordBpim'];

        $bpimSGR = array();

        $sqlr = "SELECT codigo, nombre FROM ccpproyectospresupuesto WHERE concat_ws(' ', codigo, nombre) LIKE '%$keywordBpim%' ORDER BY id ASC";//echo $sqlr;
        $res = mysqli_query($linkbd, $sqlr);
        while($row = mysqli_fetch_row($res)){
            array_push($bpimSGR, $row);
        }

        $out['bpimSGR'] = $bpimSGR;
    }

    if($action == 'cargarTiposRecursos'){

        $tiposRecursosSGR = [];
        $sqlr = "SELECT * FROM sgr_tipos_recursos ORDER BY id ASC";
        $res = mysqli_query($linkbd, $sqlr);
        while($row = mysqli_fetch_row($res)){
            array_push($tiposRecursosSGR, $row);
        }

        $out['tiposRecursosSGR'] = $tiposRecursosSGR;

    }

    if($action == 'filtrarTiposRecursos'){

        $keywordTipoRecurso=$_POST['keywordTipoRecurso'];

        $tiposRecursosSGR = array();

        $sqlr = "SELECT * FROM sgr_tipos_recursos WHERE concat_ws(' ', codigo, nombre) LIKE '%$keywordTipoRecurso%' ORDER BY id ASC";//echo $sqlr;
        $res = mysqli_query($linkbd, $sqlr);
        while($row = mysqli_fetch_row($res)){
            array_push($tiposRecursosSGR, $row);
        }

        $out['tiposRecursosSGR'] = $tiposRecursosSGR;
    }

    if($action == 'cargarTerceros'){

        $tercerosSGR = [];
        $sqlr = "SELECT * FROM sgr_terceros ORDER BY id ASC";
        $res = mysqli_query($linkbd, $sqlr);
        while($row = mysqli_fetch_row($res)){
            array_push($tercerosSGR, $row);
        }

        $out['tercerosSGR'] = $tercerosSGR;

    }

    if($action == 'filtrarTerceros'){

        $keywordTercero=$_POST['keywordTercero'];

        $tercerosSGR = array();

        $sqlr = "SELECT * FROM sgr_terceros WHERE concat_ws(' ', codigo, nombre) LIKE '%$keywordTercero%' ORDER BY id ASC";//echo $sqlr;
        $res = mysqli_query($linkbd, $sqlr);
        while($row = mysqli_fetch_row($res)){
            array_push($tercerosSGR, $row);
        }

        $out['tercerosSGR'] = $tercerosSGR;
    }

    if($action == 'cargarPoliticasPublicas'){

        $politicasPublicasSGR = [];
        $sqlr = "SELECT * FROM ccpet_politicapublica ORDER BY codigo ASC";
        $res = mysqli_query($linkbd, $sqlr);
        while($row = mysqli_fetch_row($res)){
            array_push($politicasPublicasSGR, $row);
        }

        $out['politicasPublicasSGR'] = $politicasPublicasSGR;

    }

    if($action == 'filtrarPoliticasPublicas'){

        $keywordPoliticaPublica=$_POST['keywordPoliticaPublica'];

        $politicasPublicasSGR = array();

        $sqlr = "SELECT * FROM ccpet_politicapublica WHERE concat_ws(' ', codigo, nombre) LIKE '%$keywordPoliticaPublica%' ORDER BY codigo ASC";//echo $sqlr;
        $res = mysqli_query($linkbd, $sqlr);
        while($row = mysqli_fetch_row($res)){
            array_push($politicasPublicasSGR, $row);
        }

        $out['politicasPublicasSGR'] = $politicasPublicasSGR;
    }

    if($action == 'guardarCab'){

        $user = $_SESSION['nickusu'];
        $nombre = $_POST['nombre_clasificador'];

        date_default_timezone_set('America/Bogota');
        $fecha_creacion = date("Y-m-d");
        $estado = 1; 

        $sql = "INSERT INTO ccpet_clasificadorgastos_cab(nombre, estado,user, fecha_creacion) VALUES ('$nombre', '$estado', '$user', '$fecha_creacion')";
        $res = mysqli_query($linkbd, $sql);
        $id_cab = mysqli_insert_id($linkbd);
    
        $out['id_cab'] =  $id_cab;
    }
   
    if($action == 'guardarRubroSGR'){

        $sqlrVaciar = "TRUNCATE TABLE rubros_cuentasingejecusgr";
        mysqli_query($linkbd, $sqlrVaciar);

        $user = $_SESSION['nickusu'];
        $fecha_mod = date("Y-m-d");

        $tam = count($_POST["cuentasSGR"]);

        for($x = 0; $x < $tam; $x++){
            
            $sector = '';
            $fuente = '';
            $programatico = '';
            $bpim = '';
            $opcion = '';
            $tipo = '';

            $partsRubro = explode("-", $_POST["rubros"][$x]);

            $fuente = $partsRubro[1];
            $programatico = substr($_POST["rubros"][$x], 0, 4);
            $bpim = $partsRubro[1];

            if($partsRubro[3] != ''){
                $opcion = $partsRubro[3];
            }

            if($partsRubro[2] != ''){
                $tipo = $partsRubro[2];
            }

            $sqlrInsert = "INSERT INTO rubros_cuentasingejecusgr(rubro, cuentasgr, fuente, user, fecha_mod, opcion, tipo) VALUES ('".trim($_POST["rubros"][$x])."', '".trim($_POST["cuentasSGR"][$x])."', '".trim($fuente)."', '".$user."', '".$fecha_mod."', '".$opcion."', '".$tipo."')";
            mysqli_query($linkbd, $sqlrInsert);

        }
        $out['insertaBien'] = true;
    }

    header("Content-type: application/json");
    echo json_encode($out);
    die();