const URL = "presupuesto_ccpet/diferenciaFuentes/controllers/fuente_controller.php";
const URLEXCEL = "cpp-diferenciaFuentesExcel.php";
const URLPDF = "ccp-diferenciaFuentesPdf.php";

var app = new Vue({
    el:"#myapp",
    data() {
        return {
            isLoading: false,
            fuentes: [],
            vigencias: [],
            vigenciaSelected: '',
            totalIngesos: 0,
            totalGastos: 0,
            totalDiferencias: 0
        }
    },
    mounted() {
        this.get();
    },

    methods: {
        /* metodos para traer información */
        async get() {
            const formData = new FormData();
            formData.append("action","get");
            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();
            this.vigencias = objData.vigencias;
            this.vigenciaSelected = this.vigencias[0].vigencia;
        },

        async getFuentes() {
            if (this.vigenciaSelected == '') {
                Swal.fire("Atención!","Debe seleccionar una vigencia","warning");
                return;
            }

            const formData = new FormData();
            formData.append("action","fuentes");
            formData.append("vigencia",this.vigenciaSelected);
            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();
            this.fuentes = objData.fuentes;
            this.saldosTotales();
        },
        /* metodos para procesar información */
    
        /* Metodos para guardar o actualizar información */
        
        /* Formatos para mostrar información */
        viewFormatNumber: function(valor){
            return new Intl.NumberFormat("en-US", {style: "currency", currency: "USD"}).format(valor);  
        },
        /* enviar informacion excel o pdf */
        donwload(type) {
            const form = document.createElement("form");
            form.method = "post";
            form.target = "_blank";
            if (type == "pdf") {
                form.action = URLPDF;
            } else {
                form.action = URLPDF;
            }
            
            function addField(name,value){
                const input = document.createElement("input");
                input.type="hidden";
                input.name=name;
                input.value = value;
                form.appendChild(input);
            }

            addField("vigencia", this.vigenciaSelected);
            addField("fuentes", JSON.stringify(this.fuentes));
            
            document.body.appendChild(form);
            form.submit();
            document.body.removeChild(form);

            
        },
        
        //saldos totales
        saldosTotales() {
            console.log(this.fuentes);
            this.totalIngesos = 0;
            this.totalGastos = 0;
            this.totalDiferencias = 0;
            for (let index = 0; index < this.fuentes.length; index++) {
                this.totalIngesos += this.fuentes[index].ingresos;
                this.totalGastos += this.fuentes[index].gastos;
                this.totalDiferencias += this.fuentes[index].diferencia;
            
            }
        }
        
    },
    computed:{

    }
})
