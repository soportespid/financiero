<?php
    require_once '../../../Librerias/core/Helpers.php';
    require_once '../models/fuente_models.php';
    session_start();
    header('Content-Type: application/json');

    class fuenteController extends fuenteModel {
        /* buscar */
        public function get() {
            if (!empty($_SESSION)) {
                $arrResponse = array("vigencias" => $this->getVigencias());
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);    
            }
            die();
        }

        public function getFuentes() {
            $vigencia = $_POST["vigencia"];
            $arrResponse = array("fuentes" => $this->dataFuentes($vigencia));
            echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
        }
    }

    if($_POST){
        $obj = new fuenteController();
        $accion = $_POST["action"];
        
        if ($accion == "get") {
            $obj->get();
        } else if ($accion == "fuentes") {
            $obj->getFuentes();
        }   
    }
?>