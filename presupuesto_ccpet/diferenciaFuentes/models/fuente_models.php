<?php
    require_once '../../../Librerias/core/Helpers.php';
    require_once '../../../Librerias/core/Mysql.php';
    session_start();
    
    class fuenteModel extends Mysql {
        /* Read data */
        public function getVigencias() {
            $sql = "SELECT anio AS vigencia FROM admbloqueoanio WHERE bloqueado = 'N' ORDER BY anio DESC";
            $vigencias = $this->select_all($sql);
            return $vigencias;
        }

        public function dataFuentes($vigencia) {
            $fuentes = [];
            $ingresos = $this->getValuesIngresos($vigencia);
            foreach ($ingresos as $ingreso) {
                $fuentesParts = explode("_", $ingreso["fuente"]);
                $data = array(
                    "codFuente" => $fuentesParts[0],
                    "nombreFuente" => $this->getNombreFuente($fuentesParts[0]),
                    "ingresos" => round($ingreso["ingreso"], 2),
                    "gastos" => 0,
                    "diferencia" => round($ingreso["ingreso"], 2) - 0
                );

                array_push($fuentes, $data);
            }

            $gastos = $this->getValuesGastos($vigencia);
            
            foreach ($gastos as &$gasto) {
                $found = false;
                $fuentesParts = explode("_", $gasto["fuente"]);
                foreach ($fuentes as &$fuente) {
                    

                    if ($fuente["codFuente"] == $fuentesParts[0]) {
                        $fuente["gastos"] = $fuente["gastos"] + round($gasto["gasto"], 2);
                        $fuente["diferencia"] = $fuente["ingresos"] - $fuente["gastos"];
                        $found = true;
                        break;
                    }
                }

                if (!$found) {
                    $data = array(
                        "codFuente" => $fuentesParts[0],
                        "nombreFuente" => $this->getNombreFuente($fuentesParts[0]),
                        "ingresos" => 0,
                        "gastos" => round($gasto["gasto"], 2),
                        "diferencia" => 0 - round($gasto["gasto"], 2)
                    );
                    array_push($fuentes, $data);
                }
            }

            $gastoInv = $this->getValuesGastosInv($vigencia);
            
            foreach ($gastoInv as &$gasto) {
                $found = false;
                $fuentesParts = explode("_", $gasto["fuente"]);
                foreach ($fuentes as &$fuente) {
                    if ($fuente["codFuente"] == $fuentesParts[0]) {
                        $fuente["gastos"] = $fuente["gastos"] + round($gasto["gasto"], 2);
                        $fuente["diferencia"] = round(($fuente["ingresos"] - $fuente["gastos"]), 2);
                        $found = true;
                        break;
                    }
                }

                if (!$found) {
                    $data = array(
                        "codFuente" => $fuentesParts[0],
                        "nombreFuente" => $this->getNombreFuente($fuentesParts[0]),
                        "ingresos" => 0,
                        "gastos" => round($gasto["gasto"], 2),
                        "diferencia" => 0 - round($gasto["gasto"], 2)
                    );
                    array_push($fuentes, $data);
                }
            }

            function array_sort_by(&$arrIni, $col, $order)
            {
                $arrAux = array();
                foreach ($arrIni as $key=> $row)
                {
                    $arrAux[$key] = is_object($row) ? $arrAux[$key] = $row->$col : $row[$col];
                    $arrAux[$key] = strtolower($arrAux[$key]);
                }
                array_multisort($arrAux, $order, $arrIni);
            }

            array_sort_by($fuentes, 'codFuente', SORT_ASC);

            return $fuentes;
        }

        public function getValuesIngresos($vigencia) {
            $sql = "SELECT fuente, SUM(valor) AS ingreso FROM ccpetinicialing WHERE vigencia = '$vigencia' GROUP BY fuente ORDER BY fuente ASC";
            $ingresos = $this->select_all($sql);
            return $ingresos;
        }

        public function getValuesGastos($vigencia) {
            $bases = $this->getBases();
            $gastos = [];

            foreach ($bases as $base) {
                $conectar = new Mysql($base["base"], $base["usuario"]);
                $sql = "SELECT fuente, SUM(valor) AS gasto FROM ccpetinicialgastosfun WHERE vigencia = '$vigencia' GROUP BY fuente ORDER BY fuente ASC";
                $gastosBase = $conectar->select_all($sql);

                foreach ($gastosBase as $gasto) {
                    $gastos = array_merge($gastos, array(array("fuente" => $gasto["fuente"], "gasto" => $gasto["gasto"])));
                }

            }
            return $gastos;
        }

        public function getValuesGastosInv($vigencia) {

            $bases = $this->getBases();

            $gastos = [];

            foreach ($bases as $base) {
                $conectar = new Mysql($base["base"], $base["usuario"]);

                $sql = "SELECT det.id_fuente AS fuente,
                SUM(det.valorcsf + det.valorssf) AS gasto
                FROM ccpproyectospresupuesto AS cab
                INNER JOIN ccpproyectospresupuesto_presupuesto AS det
                ON cab.id = det.codproyecto
                WHERE cab.vigencia = '$vigencia' AND cab.estado = 'S'
                GROUP BY det.id_fuente
                ORDER BY det.id_fuente ASC";
                $gastosBase = $conectar->select_all($sql);

                foreach ($gastosBase as $gasto) {
                    $gastos = array_merge($gastos, array(array("fuente" => $gasto["fuente"], "gasto" => $gasto["gasto"])));
                }
            }

            return $gastos; 
        }

        public function getBases() {
            $con = new Mysql();
            $sql = "SELECT codigo,base, usuario FROM redglobal";
            $request = $con->select_all($sql);
            return $request;
        }

        public function getNombreFuente($fuente) {
            $sql = "SELECT nombre FROM ccpet_fuentes_cuipo WHERE codigo_fuente = '$fuente'";
            $data = $this->select($sql);
            return $data["nombre"];
        }
        /* Create data */
        
        /* Update data */

        /* Delate data */
       
    }
?>