<?php
    require_once '../../../Librerias/core/Helpers.php';
    require_once '../models/SolicitudModel.php';
    session_start();

    class SolicitudController extends SolicitudModel{
        public function getData(){
            if(!empty($_SESSION)){
                $arrData = array(
                    "liquidaciones"=>$this->getLiquidaciones()
                );
                echo json_encode($arrData,JSON_UNESCAPED_UNICODE);
            }
        }
        public function getLiquidaciones(){
            $arrVariablesPago = $this->selectVariablesPago();
            $arrVariablesPara = $this->selectVariablesPara();
            $arrParametros = $this->selectParametrosNomina();
            $arrSueldo = array_values(array_filter($arrVariablesPago,function($e) use($arrParametros){return $e['codigo'] == $arrParametros['sueldo'];}))[0];
            $arrTransporte = array_values(array_filter($arrVariablesPago,function($e) use($arrParametros){return $e['codigo'] == $arrParametros['aux_transporte'];}))[0];
            $arrAlimento = array_values(array_filter($arrVariablesPago,function($e) use($arrParametros){return $e['codigo'] == $arrParametros['sub_alimentacion'];}))[0];
            $arrPension = array_values(array_filter($arrVariablesPara,function($e) use($arrParametros){return $e['codigo'] == $arrParametros['pension_empleador'] && $e['sector']=="privado";}))[0];
            $arrSalud = array_values(array_filter($arrVariablesPara,function($e) use($arrParametros){return $e['codigo'] == $arrParametros['salud_empleador'];}))[0];
            $arrCcf = array_values(array_filter($arrVariablesPara,function($e) use($arrParametros){return $e['codigo'] == $arrParametros['cajacompensacion'];}))[0];
            $arrArl = array_values(array_filter($arrVariablesPara,function($e) use($arrParametros){return $e['codigo'] == $arrParametros['arp'];}))[0];
            $arrIcbf = array_values(array_filter($arrVariablesPara,function($e) use($arrParametros){return $e['codigo'] == $arrParametros['icbf'];}))[0];
            $arrSena = array_values(array_filter($arrVariablesPara,function($e) use($arrParametros){return $e['codigo'] == $arrParametros['sena'];}))[0];
            $arrEsap = array_values(array_filter($arrVariablesPara,function($e) use($arrParametros){return $e['codigo'] == $arrParametros['esap'];}))[0];
            $arrInst = array_values(array_filter($arrVariablesPara,function($e) use($arrParametros){return $e['codigo'] == $arrParametros['iti'];}))[0];

            $arrLiquidaciones = $this->selectLiquidaciones();
            $arrCuentasCcpet = $this->selectCcpetNomina();
            $arrData = [];
            foreach ($arrLiquidaciones as $cab) {
                $detalle = $cab['detalle'];
                $arrCuentas = [];
                foreach ($arrCuentasCcpet as $cuenta) {
                    $total = 0;
                    foreach ($detalle as $det) {
                        if($det['ptto_salario'] == "I"){
                            $det['ptto_salario'] = "IN";
                            $det['cod_pago'] = $det['inversion'];
                        }else if($det['ptto_salario'] == "G"){
                            $det['ptto_salario'] = "GP";
                            $det['cod_pago'] = $det['comercial'];
                        }else{
                            $det['cod_pago'] = $det['funcionamiento'];
                        }
                        if($det['ptto_para'] == "I"){
                            $det['ptto_para'] = "IN";
                            $det['cod_pago'] = $det['inversion'];
                        }else if($det['ptto_para'] == "G"){
                            $det['ptto_para'] = "GP";
                            $det['cod_pago'] = $det['comercial'];
                        }else{
                            $det['cod_pago'] = $det['funcionamiento'];
                        }
                        if($det['ptto_pago'] == "I"){
                            $det['ptto_pago'] = "IN";
                            $det['cod_pago'] = $det['inversion'];
                        }else if($det['ptto_pago'] == "G"){
                            $det['ptto_pago'] = "GP";
                            $det['cod_pago'] = $det['comercial'];
                        }else{
                            $det['cod_pago'] = $det['funcionamiento'];
                        }
                        $pensionTipo = $det['ptto_para'] =="IN" ? $arrPension['inversion'] : $det['ptto_para'] =="F" ? $arrPension['funcionamiento'] : $arrPension['comercial'];
                        $saludTipo = $det['ptto_para'] =="IN" ? $arrSalud['inversion'] : $det['ptto_para'] =="F" ? $arrSalud['funcionamiento'] : $arrSalud['comercial'];
                        $ccfTipo = $det['ptto_para'] =="IN" ? $arrCcf['inversion'] : $det['ptto_para'] =="F" ? $arrCcf['funcionamiento'] : $arrCcf['comercial'];
                        $arlTipo = $det['ptto_para'] =="IN" ? $arrArl['inversion'] : $det['ptto_para'] =="F" ? $arrArl['funcionamiento'] : $arrArl['comercial'];
                        $icbfTipo = $det['ptto_para'] =="IN" ? $arrIcbf['inversion'] : $det['ptto_para'] =="F" ? $arrIcbf['funcionamiento'] : $arrIcbf['comercial'];
                        $senaTipo = $det['ptto_para'] =="IN" ? $arrSena['inversion'] : $det['ptto_para'] =="F" ? $arrSena['funcionamiento'] : $arrSena['comercial'];
                        $esapTipo = $det['ptto_para'] =="IN" ? $arrEsap['inversion'] : $det['ptto_para'] =="F" ? $arrEsap['funcionamiento'] : $arrEsap['comercial'];
                        $insTipo = $det['ptto_para'] =="IN" ? $arrInst['inversion'] : $det['ptto_para'] =="F" ? $arrInst['funcionamiento'] : $arrInst['comercial'];
                        //Salario
                        if($det['tipo_pago'] == "01"){
                            $tipo = $det['ptto_para'] =="IN" ? $arrSueldo['inversion'] : $det['ptto_para'] =="F" ? $arrSueldo['funcionamiento'] : $arrSueldo['gastoscomerc'];
                            if($tipo ==  $cuenta['tipo_pago'] && $det['ptto_salario'] == $cuenta['tipo']){
                                $total += $det['total'];
                            }
                        }
                        //Transporte
                        if($det['tipo_pago'] == "05"){
                            $tipo = $det['ptto_para'] =="IN" ? $arrTransporte['inversion'] : $det['ptto_para'] =="F" ? $arrTransporte['funcionamiento'] : $arrTransporte['gastoscomerc'];
                            if($tipo ==  $cuenta['tipo_pago'] && $det['ptto_salario'] == $cuenta['tipo']){
                                $total += $det['total'];
                            }
                        }
                        //Alimentación
                        if($det['tipo_pago'] == "04"){
                            $tipo = $det['ptto_para'] =="IN" ? $arrAlimento['inversion'] : $det['ptto_para'] =="F" ? $arrAlimento['funcionamiento'] : $arrAlimento['gastoscomerc'];
                            if($tipo ==  $cuenta['tipo_pago'] && $det['ptto_salario'] == $cuenta['tipo']){
                                $total += $det['total'];
                            }
                        }
                        //Otros pagos
                        if($det['tipo_pago'] != "01" && $det['tipo_pago'] != "04" && $det['tipo_pago'] != "05" && $det['cod_pago'] == $cuenta['tipo_pago']  && $det['ptto_pago'] == $cuenta['tipo']){
                            $total += $det['total'];
                        }
                        //Pension
                        if($pensionTipo == $cuenta['tipo_pago']  && $det['ptto_para'] == $cuenta['tipo']){
                            $total += $det['valor_pension_empresa'];
                        }
                        //Salud
                        if($saludTipo == $cuenta['tipo_pago']  && $det['ptto_para'] == $cuenta['tipo']){
                            $total += $det['valor_salud_empresa'];
                        }
                        //CCF
                        if($ccfTipo == $cuenta['tipo_pago']  && $det['ptto_para'] == $cuenta['tipo']){
                            $total += $det['valor_aporte_ccf'];
                        }
                        //ARL
                        if($arlTipo == $cuenta['tipo_pago']  && $det['ptto_para'] == $cuenta['tipo']){
                            $total += $det['valor_arl'];
                        }
                        //ICBF
                        if($icbfTipo == $cuenta['tipo_pago']  && $det['ptto_para'] == $cuenta['tipo']){
                            $total += $det['valor_aporte_icbf'];
                        }
                        //SENA
                        if($senaTipo == $cuenta['tipo_pago']  && $det['ptto_para'] == $cuenta['tipo']){
                            $total += $det['valor_aporte_sena'];
                        }
                        //ESAP
                        if($esapTipo == $cuenta['tipo_pago']  && $det['ptto_para'] == $cuenta['tipo']){
                            $total += $det['valor_aporte_esap'];
                        }
                        //Institutos
                        if($insTipo == $cuenta['tipo_pago']  && $det['ptto_para'] == $cuenta['tipo']){
                            $total += $det['valor_aporte_inst'];
                        }
                    }
                    array_push($arrCuentas,array(
                        "codigo"=>$cuenta['cuenta'],
                        "nombre"=>$cuenta['nombre'],
                        "tipo_pago"=>$cuenta['tipo_pago'],
                        "valor"=>$total,
                    ));
                }
                $arrCuentas = array_values(array_filter($arrCuentas,function($e){return $e['valor'] > 0;}));
                usort($arrCuentas, 'tipo_pago');
                $cab['detalle'] = $arrCuentas;
                array_push($arrData,$cab);
            }
            return $arrData;
        }
        public function save(){
            if(!empty($_SESSION)){
                $arrData = json_decode($_POST['data'],true);
                if(empty($_POST['fecha'] || empty($_POST['liquidacion']))){
                    $arrResponse = array("status"=>false,"msg"=>"Error de datos");
                }else{
                    $intTotal = intval($_POST['total']);
                    $intLiquidacion = intval($_POST['liquidacion']);
                    $intConsecutivo = intval($_POST['consecutivo']);
                    $strDescripcion = ucfirst(strClean(replaceChar($arrData['descripcion'])));
                    $strFecha = $_POST['fecha'];
                    $opcion="";
                    if($intConsecutivo == 0){
                        $opcion = 1;
                        $request = $this->insertData($strFecha,$strDescripcion,$intLiquidacion,$intTotal);
                        $intConsecutivo = $request;
                    }else{
                        $opcion = 2;
                        $request = $this->updateData($intConsecutivo,$strFecha);
                    }
                    if($request > 0){
                        if($opcion == 1){
                            insertAuditoria("ccpet_auditoria","ccpet_funciones_id",3,"Crear",$request,"Solicitud cdp nomina","ccpet_funciones");
                            $this->insertDet($request,$arrData['detalle']);
                            $arrResponse = array("status"=>true,"msg"=>"Datos guardados correctamente.","id"=>$request);
                        }else{
                            insertAuditoria("ccpet_auditoria","ccpet_funciones_id",3,"Editar",$intConsecutivo,"Solicitud cdp nomina","ccpet_funciones");
                            $arrResponse = array("status"=>true,"msg"=>"Datos actualizados correctamente.");
                        }
                    }else if($request =="existe"){
                        $arrResponse = array("status"=>false,"msg"=>"La liquidación ".$intLiquidacion." ya tiene solicitud de cdp.");
                    }else{
                        $arrResponse = array("status"=>false,"msg"=>"No se ha podido guardar, inténte de nuevo.");
                    }
                }
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
            die();
        }
        public function getEdit(){
            if(!empty($_SESSION)){
                $intId = intval($_POST['codigo']);
                $request = $this->selectEdit($intId);
                if(!empty($request)){
                    $arrResponse = array("status"=>true,"data"=>$request,"consecutivos"=>$this->selectConsecutivos());
                }else{
                    $arrResponse = array("status"=>false,"consecutivo"=>$this->selectConsecutivo()-1);
                }
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
            die();
        }
        public function getSearch(){
            if(!empty($_SESSION)){
                $strFechaInicial = strClean($_POST['fecha_inicial']);
                $strFechaFinal = strClean($_POST['fecha_final']);
                $strCodigo = strClean($_POST['codigo']);
                $request = $this->selectSearch($strFechaInicial,$strFechaFinal,$strCodigo);
                echo json_encode($request,JSON_UNESCAPED_UNICODE);
            }
            die();
        }
        public function changeData(){
            if(!empty($_SESSION)){
                $id = intval($_POST['id']);
                $request = $this->updateStatus($id);
                if($request == 1){
                    insertAuditoria("ccpet_auditoria","ccpet_funciones_id",3,"Anular",$request,"Solicitud cdp nomina","ccpet_funciones");
                    $arrResponse = array("status"=>true);
                }else if($request == "existe"){
                    $arrResponse = array("status"=>false,"msg"=>"La solicitud ya tiene cdp, deberá anularlo.");
                }else{
                    $arrResponse = array("status"=>false,"msg"=>"Ha ocurrido un error, inténtelo de nuevo.");
                }
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
            die();
        }
    }

    if($_POST){
        $obj = new SolicitudController();
        if($_POST['action'] == "save"){
            $obj->save();
        }else if($_POST['action']=="edit"){
            $obj->getEdit();
        }else if($_POST['action']=="search"){
            $obj->getSearch();
        }else if($_POST['action']=="change"){
            $obj->changeData();
        }else if($_POST['action']=="get"){
            $obj->getData();
        }
    }

?>
