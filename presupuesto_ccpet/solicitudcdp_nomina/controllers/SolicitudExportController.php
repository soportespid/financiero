<?php
    require_once '../../../Librerias/core/Helpers.php';
    require_once '../../../tcpdf/tcpdf.php';
    require_once '../../../Librerias/core/Mysql.php';
    session_start();
    /*ini_set('display_errors', '1');
    ini_set('display_startup_errors', '1');
    error_reporting(E_ALL);*/
    class SolicitudExportController {
        public function exportPdf(){
            if(!empty($_SESSION)){
                $arrData = json_decode($_POST['data'],true);
                if(!empty($arrData)){
                    $con = new Mysql();
                    $sql = "SELECT funcionario as nombre,nomcargo as cargo FROM firmaspdf_det WHERE idfirmas='1' AND estado ='S'";
                    $arrFirmas = $con->select_all($sql);
                    define("FIRMAS",$arrFirmas);
                    define("ESTADO",$arrData['estado']);
                    $arrFecha = explode("-",$arrData['fecha']);
                    $strFecha = $arrFecha[2]."/".$arrFecha[1]."/".$arrFecha[0];
                    $arrDet = $arrData['detalle'];

                    $pdf = new MYPDF('P','mm','Letter', true, 'iso-8859-1', false);
                    $pdf->SetDocInfoUnicode (true);
                    // set document information
                    $pdf->SetCreator(PDF_CREATOR);
                    $pdf->SetAuthor('IDEALSAS');
                    $pdf->SetTitle('FORMATO SOLICITUD CERTIFICADO DISPONIBILIDAD PRESUPUESTAL NÓMINA');
                    $pdf->SetSubject('FORMATO SOLICITUD CERTIFICADO DISPONIBILIDAD PRESUPUESTAL NÓMINA');
                    $pdf->SetKeywords('TCPDF, PDF, example, test, guide');
                    $pdf->SetMargins(10, 38, 10);// set margins
                    $pdf->SetHeaderMargin(38);// set margins
                    $pdf->SetFooterMargin(17);// set margins
                    $pdf->SetAutoPageBreak(TRUE, 20);// set auto page breaks
                    // set some language-dependent strings (optional)
                    if (@file_exists(dirname(__FILE__).'/lang/spa.php'))
                    {
                        require_once(dirname(__FILE__).'/lang/spa.php');
                        $pdf->setLanguageArray($l);
                    }
                    $pdf->SetFillColor(255,255,255);
                    $pdf->AddPage();

                    $pdf->SetFont('helvetica','B',9);
                    $pdf->SetFillColor(153,221,255);
                    $pdf->MultiCell(190,10,"LA ".$arrFirmas[0]['cargo']. " \n SOLICITA","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->ln();
                    $pdf->SetFillColor(255,255,255);
                    $pdf->SetTextColor(0,0,0);
                    $pdf->SetFont('Helvetica','',8);
                    $pdf->MultiCell(20,5,"CONSECUTIVO:","LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->MultiCell(15,5,$arrData["consecutivo"],"LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->MultiCell(25,5,"LIQUIDACIÓN:","LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->MultiCell(25,5,$arrData['id'],"LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->MultiCell(25,5,"FECHA SOLICITUD:","LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->MultiCell(25,5,$strFecha,"LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->MultiCell(20,5,"VALOR:","LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->MultiCell(35,5,formatNum($arrData["total"], 2),"LRBT",'R',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->ln();
                    $pdf->MultiCell(20,10,"OBJETO:","LRBT",'LRBT',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->MultiCell(170,10,$arrData['descripcion'],"LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->ln();
                    $pdf->SetFont('helvetica','B',9);
                    $pdf->SetFillColor(153,221,255);
                    $pdf->MultiCell(190,5,"DETALLE","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->ln();
                    $pdf->MultiCell(35,5,"Código","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->MultiCell(120,5,"Rubro","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->MultiCell(35,5,"Valor","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->ln();
                    $pdf->SetFillColor(255,255,255);
                    $pdf->SetFont('Helvetica','',8);
                    foreach ($arrDet as $det) {
                        $altura = $pdf->getNumLines($det['nombre'])*5;
                        $pdf->MultiCell(35,$altura,$det['codigo'],"LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
                        $pdf->MultiCell(120,$altura,$det['nombre'],"LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
                        $pdf->MultiCell(35,$altura,formatNum($det['valor']),"LRBT",'R',true,0,'','',true,0,false,true,0,'M',true);
                        $pdf->ln();
                        if($pdf->GetY() > 190){$pdf->AddPage();}
                    }
                    $pdf->SetFillColor(153,221,255);
                    $pdf->SetFont('helvetica','B',8);
                    $pdf->MultiCell(155,$altura,"Total","LRBT",'R',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->MultiCell(35,$altura,formatNum($arrData['total']),"LRBT",'R',false,0,'','',true,0,false,true,0,'M',true);
                    $pdf->Output("solicitud_cdp_nomina_".$arrData['consecutivo']."_.pdf", 'I');
                }
            }
            die();
        }

    }
    class MYPDF extends TCPDF {

		public function Header(){
			$request = configBasica();
            $strNit = $request['nit'];
            $strRazon = $request['razonsocial'];

			//Parte Izquierda
			$this->Image('../../../imagenes/escudo.jpg',  13, 13, 20, 20, 'JPG', '', 'T', true, 300, '', false, false, 0, false, false, false);// Logo
			$this->SetFont('helvetica','B',8);
			$this->SetY(10);
			$this->RoundedRect(10, 10, 190, 25, 1,'');
			$this->Cell(0.1);
			$this->Cell(26,25,'','R',0,'L');
			$this->SetY(8);
			$this->SetX(40);
			$this->SetFont('helvetica','B',9);
			$this->Cell(140,15,strtoupper("$strRazon"),0,0,'C');
			$this->SetFont('helvetica','B',7);
			$this->SetY(12);
			$this->SetX(40);
			$this->Cell(140,15,'NIT: '.$strNit,0,0,'C');
			//*****************************************************************************************************************************
			$this->SetFont('helvetica','B',9);
			$this->SetY(23);
			$this->SetX(36);
            $this->Cell(164,12,"FORMATO SOLICITUD CERTIFICADO DISPONIBILIDAD PRESUPUESTAL NÓMINA",'T',0,'C');

			//**********************************************************
			$this->SetFont('times','B',10);
			$this->ln(12);
			//**********************************************************
            if(ESTADO=="N"){
                $img_file = "../../../assets/img/anulado.png";
                $this->SetAlpha(0.35);
                $this->Image($img_file, 0, 20, 250, 280, '', '', '', false, 300, '', false, false, 300);
                $this->SetAlpha(1);
            }

		}
		public function Footer(){
			$request = configBasica();
            $strDireccion = $request['direccion'] != "" ? "Dirección: ".strtoupper($request['direccion']) : "";
            $strWeb = $request['web'] != "" ? "Pagina web: ".strtoupper($request['web']) :"";
            $strEmail = $request['email'] !="" ? "Email: ".strtoupper($request['email']) :"";
            $strTelefono = $request['telefono'] != "" ? "Telefonos: ".$request['telefono'] : "";
            $strUsuario = searchUser($_SESSION['cedulausu'])['nom_usu'];
			$strNick = $_SESSION['nickusu'];
			$strFecha = date("d/m/Y H:i:s");
			$strIp = $_SERVER['REMOTE_ADDR'];
            //firmas
            $this->setY(240);
            $this->setX(30);
            $this->SetFont('helvetica','B',6);
            $this->SetFillColor(153,221,255);
            $this->cell(70,4,'DATOS SOLICITANTE','LRTB',0,'C',1);
            $this->setX(110);
            $this->cell(70,4,'DATOS QUIEN RECIBE','LRTB',0,'C',1);
            $this->ln();
            $this->setX(30);
            $this->SetFont('helvetica','B',6);
            $this->SetFillColor(255,255,255);
            $this->MultiCell(70,4,"NOMBRE: ".FIRMAS[0]['nombre'],"LRTB",'L',true,0,'','',true,0,false,true,0,'M',true);
            $this->setX(110);
            $this->cell(70,4,'NOMBRE: '.FIRMAS[1]['nombre'],'LRTB',0,'L',1);
            $this->ln();
            $this->setX(30);
            $this->SetFont('helvetica','B',6);
            $this->SetFillColor(255,255,255);
            $this->MultiCell(70,8,"CARGO: ".FIRMAS[0]['cargo'],"LRTB",'L',true,0,'','',true,0,false,true,0,'M',true);
            $this->setX(110);
            $this->cell(70,4,'CARGO: '.FIRMAS[1]['cargo'],'LRTB',0,'L',1);
            $this->ln();
            $this->setX(110);
            $this->cell(70,4,'FECHA RECEPCIÓN: ','LRTB',0,'L',1);
            $this->ln();
            $this->setX(30);
            $this->SetFont('helvetica','',6);
            $this->SetFillColor(255,255,255);
            $this->cell(70,15,'Firma','LRTB',0,'C',1);
            $this->setX(110);
            $this->cell(70,15,'Firma','LRTB',0,'C',1);

            $this->setY(280);
			$this->SetFont('helvetica', 'I', 8);
			$txt = <<<EOD
			$strDireccion $strTelefono
			$strEmail $strWeb
			EOD;
			$this->SetFont('helvetica', 'I', 6);
			$this->Cell(190,10,'','T',0,'T');
			$this->ln(2);
			$this->Write(0, $txt , '', 0, 'C', true, 0, false, false, 0);

			$this->Cell(50, 10, 'Hecho por: '.$strUsuario, 00, false, 'C', 0, '', 0, false, 'T', 'M');
			$this->Cell(30, 10, 'Impreso por: '.$strNick, 0, false, 'C', 0, '', 0, false, 'T', 'M');
			$this->Cell(30, 10, 'IP: '.$strIp, 0, false, 'C',0, '', 0, false, 'T', 'M');
			$this->Cell(30, 10, 'Fecha: '.$strFecha, 0, false, 'C', 0, '', 0, false, 'T', 'M');
			$this->Cell(30, 10, 'IDEAL.10 S.A.S', 0, false, 'C', 0, '', 0, false, 'T', 'M');
            $this->Cell(20, 10, 'Pagina '.$this->getAliasNumPage().'/'.$this->getAliasNbPages(), 0, false, 'C', 0, '', 0, false, 'T', 'M');
		}
	}

    if($_POST){
        $obj = new SolicitudExportController();
        if($_POST['action'] == "pdf"){
            $obj->exportPdf();
        }
    }

?>
