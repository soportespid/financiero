const URL ='presupuesto_ccpet/solicitudcdp_nomina/controllers/SolicitudController.php';
const URLEXPORT ='presupuesto_ccpet/solicitudcdp_nomina/controllers/SolicitudExportController.php';
var app = new Vue({
    el:"#myapp",
    data() {
        return {
            isLoading:false,
            txtFecha:new Date().toISOString().split("T")[0],
            searchFechaInicial:new Date(new Date().getFullYear(), 0, 1).toISOString().split("T")[0],
            searchFechaFinal:new Date().toISOString().split("T")[0],
            searchCodigo:"",
            searchLiquidacion:"",
            txtConsecutivo:0,
            selectLiquidacion:"",
            arrLiquidaciones:[],
            arrSearch:[],
            objLiquidacion:{"id":"","descripcion":"","detalle":[]},
            arrSearchCopy:[],
            txtSearch:"",
            txtResultados:0,
            arrConsecutivos:[],
            arrExport:[],
            txtEstado:"",
            txtCdp:0,
            txtTotal:0,
        }
    },
    mounted() {
        const intPageVal = this.$refs.pageType.value;
        if(intPageVal == 2){
            this.getEdit();
        }else if(intPageVal == 3){
            this.getSearch();
        }else{
            this.getData();
        }
    },
    methods: {
        getData: async function(){
            const formData = new FormData();
            formData.append("action","get");
            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();
            this.arrLiquidaciones = objData.liquidaciones;
        },
        changeLiquidacion:async function(){
            this.txtTotal = 0;
            this.objLiquidacion = this.arrLiquidaciones.find(e=>e.id == this.selectLiquidacion);
            this.objLiquidacion.detalle.forEach(e=>{
                this.txtTotal += parseInt(e.valor);
            });

        },
        formatMoney:function(valor){
            valor = new String(valor);
            // Separar la parte entera y decimal
            const [integerPart, decimalPart] = valor.split(",");

            // Formatear la parte entera
            const formattedInteger = integerPart.replace(/\B(?=(\d{3})+(?!\d))/g, ".");
            return decimalPart !== undefined
            ? `$${formattedInteger},${decimalPart}`
            : `$${formattedInteger}`;
        },
        getEdit:async function(){
            const codigo = new URLSearchParams(window.location.search).get('id');
            let formData = new FormData();
            formData.append("action","edit");
            formData.append("codigo",codigo);
            await this.getData();
            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();
            if(objData.status){
                this.txtConsecutivo = objData.data.id;
                this.txtTotal = objData.data.total;
                this.txtFecha = objData.data.fecha;
                this.txtCdp = objData.data.cdp;
                this.objLiquidacion={
                    "consecutivo":objData.data.consecutivo,
                    "id":objData.data.nomina,
                    "total":objData.data.total,
                    "fecha":objData.data.fecha,
                    "estado":objData.data.estado,
                    "vigencia":objData.data.vigencia,
                    "descripcion":objData.data.descripcion,
                    "detalle":objData.data.detalle
                };
                this.arrExport = this.objLiquidacion;
                this.selectLiquidacion = objData.data.nomina;
                this.arrConsecutivos = objData.consecutivos;
                this.txtEstado = objData.data.estado;
            }else{
                window.location.href='ccpet-solicitud-nomina-cdp-editar?id='+objData.consecutivo;
            }
        },
        exportData:function(){
            const form = document.createElement("form");
            form.method ="post";
            form.target="_blank";
            form.action=URLEXPORT;
            function addField(name,value){
                const input = document.createElement("input");
                input.type="hidden";
                input.name=name;
                input.value = value;
                form.appendChild(input);
            }
            addField("action","pdf");
            addField("data",JSON.stringify(this.arrExport));
            document.body.appendChild(form);
            form.submit();
            document.body.removeChild(form);
        },
        getSearch:async function(){
            const formData = new FormData();
            formData.append("action","search");
            formData.append("fecha_inicial",this.searchFechaInicial);
            formData.append("fecha_final",this.searchFechaFinal);
            formData.append("codigo",this.searchCodigo);
            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();
            this.arrSearch = objData;
            this.arrSearchCopy = objData;
            this.txtResultados = this.arrSearchCopy.length;
        },
        save:async function(){
            if(this.selectLiquidacion=="" || this.objLiquidacion.descripcion == ""){
                Swal.fire("Atención!","Todos los campos con (*).","warning");
                return false;
            }
            Swal.fire({
                title:"¿Estás segur@ de guardar?",
                text:"",
                icon: 'warning',
                showCancelButton:true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText:"Sí, guardar",
                cancelButtonText:"No, cancelar"
            }).then(async function(result){
                if(result.isConfirmed){
                    const formData = new FormData();
                    formData.append("action","save");
                    formData.append("fecha",app.txtFecha);
                    formData.append("liquidacion",app.selectLiquidacion);
                    formData.append("total",app.txtTotal);
                    formData.append("data",JSON.stringify(app.objLiquidacion));
                    formData.append("consecutivo",app.txtConsecutivo);
                    const response = await fetch(URL,{method:"POST",body:formData});
                    const objData = await response.json();
                    if(objData.status){
                        Swal.fire("Guardado",objData.msg,"success");
                        if(objData.id && app.txtConsecutivo == 0){
                            setTimeout(function(){
                                window.location.href='ccpet-solicitud-nomina-cdp-editar?id='+objData.id;
                            },1500);
                        }else{
                            app.getEdit();
                        }
                    }else{
                        Swal.fire("Error",objData.msg,"error");
                    }
                }
            });
        },
        search:function(){
            let search = this.txtSearch.toLowerCase();
            this.arrSearchCopy = [...this.arrSearch.filter(e=>e.id.toLowerCase().includes(search) || e.motivo.toLowerCase().includes(search))];
            this.txtResultados = this.arrSearchCopy.length;
        },
        changeStatus:function(item){
            Swal.fire({
                title:"¿Estás segur@ de anular esta liquidación?",
                text:"Tendrás que volverla a hacer...",
                icon: 'warning',
                showCancelButton:true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText:"Sí",
                cancelButtonText:"No, cancelar"
            }).then(async function(result){
                if(result.isConfirmed){
                    const formData = new FormData();
                    formData.append("action","change");
                    formData.append("id",item.consecutivo);
                    const response = await fetch(URL,{method:"POST",body:formData});
                    const objData = await response.json();
                    if(objData.status){
                        app.getSearch();
                        Swal.fire("Solicitud anulada","","success");
                    }else{
                        Swal.fire("Error",objData.msg,"error");
                    }
                }else{
                    app.getSearch();
                }
            });

        },
        nextItem:function(type){
            let id = this.txtConsecutivo;
            let index = this.arrConsecutivos.findIndex(function(e){return e.id == id});
            if(type=="next" && app.arrConsecutivos[++index]){
                id = this.arrConsecutivos[index++].id;
            }else if(type=="prev" && app.arrConsecutivos[--index]){
                id = this.arrConsecutivos[index--].id;
            }
           window.location.href='ccpet-solicitud-nomina-cdp-editar?id='+id;
        },
    },
})
