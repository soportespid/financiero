<?php
    require_once '../../../Librerias/core/Helpers.php';
    require_once '../../../Librerias/core/Mysql.php';
    session_start();
    /*ini_set('display_errors', '1');
    ini_set('display_startup_errors', '1');
    error_reporting(E_ALL);*/
    class SolicitudModel extends Mysql{
        private $intConsecutivo;
        private $intVigencia;
        private $strFecha;
        private $intLiquidacion;
        private $intTotal;
        private $strDescripcion;
        private $intVersion;

        function __construct(){
            parent::__construct();
            $this->intVigencia = date_create(date("Y-m-d"))->format('Y');
            $this->intVersion = getVersionGastosCcpet();
        }
        public function insertData(string $strFecha,string $strDescripcion,int $intLiquidacion,int $intTotal){
            $this->strFecha = $strFecha;
            $this->intLiquidacion = $intLiquidacion;
            $this->intTotal = $intTotal;
            $this->strDescripcion = $strDescripcion;
            $this->intConsecutivo = $this->selectConsecutivo();
            $sql = "SELECT * FROM hum_nom_cdp_rp WHERE nomina = $this->intLiquidacion AND solicitud!=0 AND vigencia = $this->intVigencia AND estado = 'S'";
            $request = $this->select_all($sql);
            $return = $request;
            if(empty($request)){
                $sql = "INSERT INTO ccpet_solicitudnomina_cdp_cab(vigencia,consecutivo,descripcion,total,id_nom,fecha) VALUES(?,?,?,?,?,?)";
                $request = $this->insert($sql,[$this->intVigencia,$this->intConsecutivo,$this->strDescripcion,$this->intTotal,$this->intLiquidacion,$this->strFecha]);
                $return = $request;
                $sqlNom = "UPDATE hum_nom_cdp_rp SET solicitud=? WHERE nomina = $this->intLiquidacion AND vigencia = $this->intVigencia AND estado = 'S'";
                $this->update($sqlNom,[$this->selectConsecutivo()-1]);
            }else{
                $return = "existe";
            }
            return $return;
        }
        public function insertDet(int $intConsecutivo,array $data){
            $this->intConsecutivo = $intConsecutivo;
            foreach ($data as $det) {
                $sql = "INSERT INTO ccpet_solicitudnomina_cdp_det(solicitud_id,codigo,valor) VALUES(?,?,?)";
                $this->insert($sql,[$this->intConsecutivo,$det['codigo'],$det['valor']]);
            }
        }
        public function updateStatus(int $intConsecutivo){
            $this->intConsecutivo = $intConsecutivo;
            $sql = "SELECT * FROM hum_nom_cdp_rp WHERE solicitud = $this->intConsecutivo AND cdp != 0 AND vigencia = $this->intVigencia AND estado = 'S'";
            $request = $this->select($sql);
            if(empty($request)){
                $sql = "UPDATE ccpet_solicitudnomina_cdp_cab SET estado = ? WHERE id = $this->intConsecutivo AND vigencia = $this->intVigencia";
                $request = $this->update($sql,['N']);
                $this->update("UPDATE hum_nom_cdp_rp SET solicitud = ? WHERE solicitud = $this->intConsecutivo AND vigencia = $this->intVigencia",[0]);
                $return = $request;
            }else{
                $return ="existe";
            }
            return $return;
        }
        public function selectSearch($strFechaInicial,$strFechaFinal,$strCodigo){
            $sql = "SELECT
            cab.id,
            cab.consecutivo,
            cab.vigencia,
            cab.descripcion,
            cab.total,
            cab.estado,
            cab.id_nom as nomina,
            cdp.cdp,
            DATE_FORMAT(cab.fecha,'%d/%m/%Y') as fecha
            FROM ccpet_solicitudnomina_cdp_cab cab
            LEFT JOIN hum_nom_cdp_rp cdp ON cdp.nomina = cab.id_nom
            WHERE cab.fecha BETWEEN '$strFechaInicial' AND '$strFechaFinal' AND cab.consecutivo LIKE '$strCodigo%'
            ORDER BY cab.consecutivo DESC";
            $request = $this->select_all($sql);
            return $request;
        }
        public function selectConsecutivo(){
            $sql = "SELECT MAX(consecutivo) as consecutivo FROM ccpet_solicitudnomina_cdp_cab WHERE vigencia = $this->intVigencia";
            $request = $this->select($sql);
            return $request['consecutivo']+1;
        }
        public function selectConsecutivos(){
            $sql = "SELECT consecutivo as id FROM ccpet_solicitudnomina_cdp_cab WHERE vigencia = $this->intVigencia";
            $request = $this->select_all($sql);
            return $request;
        }
        public function selectLiquidaciones(){
            $sql = "SELECT cab.id_nom as id,
            cab.vigencia,
            cab.estado,
            me.nombre as mes,
            nov.descripcion,
            DATE_FORMAT(cab.fecha,'%d/%m/%Y') as fecha
            FROM humnomina cab
            LEFT JOIN hum_novedadespagos_cab nov ON cab.id_nom = nov.prenomina AND cab.novedad_id = nov.id
            LEFT JOIN meses me ON me.id = cab.mes
            WHERE cab.estado = 'S' AND cab.vigencia = $this->intVigencia ORDER BY cab.id_nom DESC";
            $request = $this->select_all($sql);
            if(!empty($request)){
                $total = count($request);
                for ($i=0; $i < $total ; $i++) {
                    $sql = "SELECT det.id_nom,
                    det.cedulanit as documento,
                    det.salbas as salario,
                    det.diaslab dias,
                    det.devendias as total,
                    det.ibc as valor_ibc,
                    det.salud as valor_salud_empleado,
                    det.saludemp as valor_salud_empresa,
                    det.pension as valor_pension_empleado,
                    det.pensionemp as valor_pension_empresa,
                    det.fondosolid as valor_fondo,
                    det.retefte as valor_retencion,
                    det.otrasdeduc as valor_descuento,
                    det.totaldeduc as valor_total_descuentos,
                    det.netopagar as valor_neto,
                    det.cajacf as valor_aporte_ccf,
                    det.sena as valor_aporte_sena,
                    det.icbf as valor_aporte_icbf,
                    det.instecnicos as valor_aporte_inst,
                    det.esap as valor_aporte_esap,
                    det.basepara as valor_ibc_para,
                    det.basearp as valor_ibc_arl,
                    det.arp as valor_arl,
                    det.totalsalud as valor_total_salud,
                    det.totalpension as valor_total_pension,
                    det.tipopago as tipo_pago,
                    det.idfuncionario as id,
                    det.detalle as descripcion,
                    vap.nombre as nombre_variable,
                    det.vinculacion,
                    det.ptto_salario,
                    det.ptto_pago,
                    det.ptto_para,
                    vap.funcionamiento,
                    vap.inversion,
                    vap.gastoscomerc as comercial,
                    CASE WHEN t.razonsocial IS NULL OR t.razonsocial = ''
                    THEN CONCAT(t.nombre1,' ',t.nombre2,' ',t.apellido1,' ',t.apellido2)
                    ELSE t.razonsocial END AS nombre
                    FROM humnomina_det det
                    LEFT JOIN ccpethumvariables vap ON det.tipopago = vap.codigo
                    LEFT JOIN terceros t ON det.cedulanit = t.cedulanit
                    WHERE det.id_nom = {$request[$i]['id']}";
                    $request[$i]['detalle'] = $this->select_all($sql);
                }
            }
            return $request;
        }
        public function selectVariablesPara(){
            $sql = "SELECT codigo,funcionamiento,inversion,gastoscomerc as comercial
            FROM humparafiscalesccpet_det WHERE estado = 'S'";
            $request = $this->select_all($sql);
            return $request;
        }
        public function selectCcpetNomina(){
            $sql = "SELECT co.cuenta_p as cuenta,co.concepto_nom as tipo_pago, co.tipo, cc.nombre
            FROM ccpetcuentas_concepto_contable co
            LEFT JOIN cuentasccpet cc ON cc.codigo = co.cuenta_p AND cc.version = '$this->intVersion'
            WHERE co.estado = 'S' AND co.concepto_nom != '' AND co.concepto_nom != 'null'";
            $request = $this->select_all($sql);
            return $request;
        }
        public function selectEdit(int $intConsecutivo){
            $this->intConsecutivo = $intConsecutivo;
            $sql = "SELECT
            cab.id,
            cab.consecutivo,
            cab.vigencia,
            cab.descripcion,
            cab.total,
            cab.estado,
            cab.id_nom as nomina,
            cdp.cdp,
            cab.fecha
            FROM ccpet_solicitudnomina_cdp_cab cab
            LEFT JOIN hum_nom_cdp_rp cdp ON cdp.nomina = cab.id_nom
            WHERE cab.id = $this->intConsecutivo";
            $request = $this->select($sql);
            if(!empty($request)){
                $sql = "SELECT  det.codigo,det.valor,cc.nombre
                FROM ccpet_solicitudnomina_cdp_det det
                LEFT JOIN cuentasccpet cc ON cc.codigo = det.codigo AND cc.version = '$this->intVersion'
                WHERE det.solicitud_id = $this->intConsecutivo";
                $request['detalle'] = $this->select_all($sql);
            }
            return $request;
        }
        public function selectParametrosNomina(){
            $sql = "SELECT * FROM humparametrosliquida";
            $request = $this->select($sql);
            return $request;
        }
        public function selectVariablesPago(){
            $sql = "SELECT * FROM ccpethumvariables WHERE estado = 'S'";
            $request = $this->select_all($sql);
            return $request;
        }
    }
?>
