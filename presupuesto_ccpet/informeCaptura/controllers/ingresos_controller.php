<?php
    require_once '../../../Librerias/core/Helpers.php';
    require_once '../models/ingresos_models.php';
    session_start();
    header('Content-Type: application/json');

    class ingresosController extends ingresosModel {
        /* buscar */
        public function get() {
            if (!empty($_SESSION)) {
                $arrResponse = array("vigencias" => $this->getVigencias());
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);    
            }
            die();
        }

        public function getData() {
            $vigencia = $_POST["vigencia"];
            $arrResponse = array("data" => $this->getPresuIngresos($vigencia), "total" => $this->valorTotalPresuInicialIngresos($vigencia));
            echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
        }
    }

    if($_POST){
        $obj = new ingresosController();
        $accion = $_POST["action"];
        
        if ($accion == "get") {
            $obj->get();
        } else if ($accion == "getData") {
            $obj->getData();
        }   
    }
?>