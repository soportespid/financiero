const URL = "presupuesto_ccpet/informeCaptura/controllers/ingresos_controller.php";
const URLEXCEL = "cpp-informeCapturaIngresosExcel.php";
const URLPDF = "ccp-informeCapturaIngresosPdf.php";

var app = new Vue({
    el:"#myapp",
    data() {
        return {
            isLoading: false,
            data: [],
            vigencias: [],
            vigenciaSelected: '',
            total: 0,

        }
    },
    mounted() {
        this.get();
    },
    methods: {
        /* metodos para traer información */
        async get() {
            const formData = new FormData();
            formData.append("action","get");
            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();
            this.vigencias = objData.vigencias;
            this.vigenciaSelected = this.vigencias[0].vigencia;
        },

        async getData() {
            if (this.vigenciaSelected == '') {
                Swal.fire("Atención!","Debe seleccionar una vigencia","warning");
                return;
            }

            const formData = new FormData();
            formData.append("action","getData");
            formData.append("vigencia",this.vigenciaSelected);
            this.isLoading = true;
            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();
            this.isLoading = false;
            this.data = objData.data;
            this.total = objData.total;
            
        },
        /* metodos para procesar información */
    
        /* Metodos para guardar o actualizar información */
        
        /* Formatos para mostrar información */
        viewFormatNumber: function(valor){
            return new Intl.NumberFormat("en-US", {style: "currency", currency: "USD"}).format(valor);  
        },
        /* enviar informacion excel o pdf */
        donwload(type) {
            const form = document.createElement("form");
            form.method = "post";
            form.target = "_blank";
            if (type == "pdf") {
                form.action = URLPDF;
            } else {
                form.action = URLEXCEL;
            }
            
            function addField(name,value){
                const input = document.createElement("input");
                input.type="hidden";
                input.name=name;
                input.value = value;
                form.appendChild(input);
            }
            addField("total", this.total);
            addField("data", JSON.stringify(this.data));

            document.body.appendChild(form);
            form.submit();
            document.body.removeChild(form);
        }
    },
    computed:{

    }
})
