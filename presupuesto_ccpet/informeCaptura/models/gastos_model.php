<?php
    require_once '../../../Librerias/core/Helpers.php';
    require_once '../../../Librerias/core/Mysql.php';
    session_start();
    
    class gastosModel extends Mysql {
        /* Read data */
        public function getVigencias() {
            $sql = "SELECT anio AS vigencia FROM admbloqueoanio WHERE bloqueado = 'N' ORDER BY anio DESC";
            $vigencias = $this->select_all($sql);
            return $vigencias;
        }

        private function versionGastos() {
            $sql = "SELECT MAX(version) AS valor FROM cuentasccpet";
            $version = $this->select($sql);
            return $version["valor"];
        }

        public function cuentasGastos($version) {
            $sql = "SELECT codigo, nombre, tipo FROM cuentasccpet WHERE version = '$version' ORDER BY codigo ASC";
            $cuentas = $this->select_all($sql);
            return $cuentas;
        }

        public function seccionesPresupuestales() {
            $sql = "SELECT id_seccion_presupuestal, nombre FROM pptoseccion_presupuestal WHERE estado = 'S' ORDER BY id_seccion_presupuestal ASC";
            $secciones = $this->select_all($sql);
            return $secciones;
        }

        public function getValueCuentaMayor($cuenta, $seccion, $vigencia) {
            $sql = "SELECT SUM(valor) AS valor FROM ccpetinicialgastosfun WHERE cuenta LIKE '$cuenta%' AND seccion_presupuestal = '$seccion' AND vigencia = '$vigencia'";
            $value = $this->select($sql);
            return $value["valor"];
        }

        public function getValueCuentaCaptura($cuenta, $seccion, $vigencia) {
            $sql = "SELECT SUM(valor) AS valor, fuente, medio_pago FROM ccpetinicialgastosfun WHERE cuenta LIKE '$cuenta%' AND seccion_presupuestal = '$seccion' AND vigencia = '$vigencia' GROUP BY fuente, medio_pago";
            $value = $this->select_all($sql);
            return $value;
        }

        public function valorTotalPresuInicialGastos($vigencia) {
            $sql = "SELECT SUM(valor) AS valor FROM ccpetinicialgastosfun WHERE vigencia = '$vigencia'";
            $value = $this->select($sql);
            return $value["valor"];
        }

        public function getPresuGastos($vigencia) {
            $dataCuentas = array();
            $version = $this->versionGastos();
            $cuentas = $this->cuentasGastos($version);
            $secciones = $this->seccionesPresupuestales();

            foreach ($secciones as $seccion) {
                $data = array(
                    "rubro" => $seccion["id_seccion_presupuestal"],
                    "nombre" => strtoupper($seccion["nombre"]),
                    "gastos" => 0,
                    "tipo" => "T"
                );

                array_push($dataCuentas, $data);

                foreach ($cuentas as $cuenta) {
                    if ($cuenta["tipo"] == "A") {
                        $valor = $this->getValueCuentaMayor($cuenta["codigo"], $seccion["id_seccion_presupuestal"], $vigencia);
                        if ($valor > 0) {
                            $data = array(
                                "rubro" => $cuenta["codigo"],
                                "nombre" => $cuenta["nombre"],
                                "gastos" => $this->getValueCuentaMayor($cuenta["codigo"], $seccion["id_seccion_presupuestal"], $vigencia),
                                "tipo" => "A"
                            );
    
                            array_push($dataCuentas, $data);
                        }
                        
                    } else {
                        $valores = $this->getValueCuentaCaptura($cuenta["codigo"], $seccion["id_seccion_presupuestal"], $vigencia);

                        foreach ($valores as $valores) {
                            if ($valores["valor"] > 0) {
                                $data = array(
                                    "rubro" => "$cuenta[codigo]-$valores[fuente]-$valores[medio_pago]",
                                    "nombre" => $cuenta["nombre"],
                                    "gastos" => $valores["valor"],
                                    "tipo" => "C"
                                );
        
                                array_push($dataCuentas, $data);
                            }   
                        }
                    }
                }
            }

            return $dataCuentas;
        }

        /* Create data */
        
        /* Update data */

        /* Delate data */
       
    }
?>