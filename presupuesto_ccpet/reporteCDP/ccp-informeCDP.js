var app = new Vue({
    el: '#myapp',
	data:{

        error: '',
        loading: false,
        detalles: [],
        selectFiltro:0,
    },

    mounted: function(){


    },

    methods:
    {
        //Desglosar array
        toFormData: function(obj){
            var form_data = new FormData();
            for(var key in obj){
                form_data.append(key, obj[key]);
            }
            return form_data;
        },

        formatonumero: function(valor){
            return new Intl.NumberFormat("en-US", {style: "currency", currency: "USD"}).format(valor);
        },

        traeParametros: function(name){
            //Captura parametros
            name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
            var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
            results = regex.exec(location.search);
            return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
        },

        traeDatosFormulario: async function() {

            const fechaIni = document.getElementById('fechaIni').value;
            const fechaFin = document.getElementById('fechaFin').value;

            if (fechaIni == '' || fechaFin == '') {
                Swal.fire("Error", "Las fechas no pueden estar vacias", "warning");
                return false;

            }
            this.loading = true;
            const formData = new FormData();
            formData.append("action","datosFormulario");
            formData.append("fechaIni",fechaIni);
            formData.append("fechaFin",fechaFin);
            formData.append("filtro",app.selectFiltro);
            const response = await fetch('presupuesto_ccpet/reporteCDP/ccp-informeCDP.php',{method:"POST",body:formData});
            const objData = await response.json();
            app.detalles = objData.cdps;
            this.loading = false;
        },

        excel: function() {

            if (this.detalles != '') {

                document.form2.action="ccp-excelInformeCDP.php";
				document.form2.target="_BLANK";
				document.form2.submit();
            }
            else {
                Swal.fire("Faltan datos", "Genera el informe primero", "warning");
            }
        },
    }
});
