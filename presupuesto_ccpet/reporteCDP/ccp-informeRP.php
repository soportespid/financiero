<?php
	require_once '../../comun.inc';
    require '../../funciones.inc';
    require '../../conversor.php';
    require '../../funcionesSP.inc.php';

    session_start();

	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");

    $out = array('error' => false);

    $action = "show";

    function getSaldo($idCdp,$vigencia, $cuenta, $fuente, $bpim, $indicador_producto, $medio_pago, $codigo_vigenciag, $productoservicio, $seccion_presupuestal){
        $linkbd = conectar_v7();
        $linkbd -> set_charset("utf8");
        $sql = "SELECT COALESCE(SUM(det.valor),0) as valor
        FROM ccpetrp cab
        INNER JOIN ccpetrp_detalle det ON det.consvigencia = cab.consvigencia AND det.vigencia = cab.vigencia
        WHERE cab.vigencia = '$vigencia' AND cab.idcdp = $idCdp
        AND cab.tipo_mov = '201' AND det.cuenta = '$cuenta' AND det.fuente = '$fuente'
        AND det.bpim = '$bpim' AND det.indicador_producto = '$indicador_producto' AND det.medio_pago = '$medio_pago'
        AND det.codigo_vigenciag = '$codigo_vigenciag' AND det.productoservicio = '$productoservicio'
        AND det.seccion_presupuestal = '$seccion_presupuestal'";
        $request = mysqli_query($linkbd,$sql)->fetch_assoc()['valor'];

        $sqlReversado = "SELECT COALESCE(SUM(det.valor),0) as valor
        FROM ccpetrp cab
        INNER JOIN ccpetrp_detalle det ON det.consvigencia = cab.consvigencia AND det.vigencia = cab.vigencia
        WHERE cab.vigencia = '$vigencia' AND cab.idcdp = $idCdp AND det.tipo_mov LIKE '4%'
        AND det.cuenta = '$cuenta' AND det.fuente = '$fuente' AND det.bpim = '$bpim'
        AND det.indicador_producto = '$indicador_producto' AND det.medio_pago = '$medio_pago'
        AND det.codigo_vigenciag = '$codigo_vigenciag' AND det.productoservicio = '$productoservicio'
        AND det.seccion_presupuestal = '$seccion_presupuestal'";
        $requestRev= mysqli_query($linkbd, $sqlReversado)->fetch_assoc()['valor'];

        $saldoRp = round($request - $requestRev, 2);
        return $saldoRp;
    }
    function selectTercero($cedula){
        $linkbd = conectar_v7();
        $linkbd -> set_charset("utf8");
        $sql="SELECT CASE WHEN razonsocial IS NULL OR razonsocial = ''
        THEN CONCAT(nombre1,' ',nombre2,' ',apellido1,' ',apellido2)
        ELSE razonsocial END AS nombre FROM terceros WHERE cedulanit = '$cedula'";
        $request = mysqli_query($linkbd,$sql)->fetch_assoc()['nombre'];
        return $request;
    }
    if(isset($_POST['action'])){
        $action = $_POST['action'];
    }

    if ($action == "datosFormulario") {

        $maxVersion = ultimaVersionGastosCCPET();
        $maxVersionIngresos = ultimaVersionIngresosCCPET();

        preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST["fechaIni"], $fecha);
        $fechaIni = $fecha[3]."-".$fecha[2]."-".$fecha[1];

        preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST["fechaFin"], $fecha);
        $fechaFin = $fecha[3]."-".$fecha[2]."-".$fecha[1];
        $intFiltro = intval($_POST['filtro']);

        $rps = array();


        $filtroCondicion = "";
        if($intFiltro == 1){
            $filtroCondicion = "AND SUBSTRING(cuenta, 1, 3) = '2.3'";
        }else if($intFiltro == 2){
            $filtroCondicion = "AND SUBSTRING(cuenta, 1, 3) = '2.2'";
        }else if($intFiltro == 3){
            $filtroCondicion = "AND SUBSTRING(cuenta, 1, 3) = '2.1'";
        }
        $sqlRpCab = "SELECT vigencia, consvigencia, DATE_FORMAT(fecha,'%d/%m/%Y') as fecha, contrato, idcdp, tercero, detalle
        FROM ccpetrp WHERE fecha BETWEEN '$fechaIni' AND '$fechaFin' AND tipo_mov = '201' ORDER BY fecha ASC";

        $resRpCab = mysqli_query($linkbd, $sqlRpCab);
        while ($rowRpCab = mysqli_fetch_row($resRpCab)) {

            $cont = 0;

            $sqlRpDet = "SELECT cuenta, productoservicio, fuente, bpim, indicador_producto, medio_pago, codigo_vigenciag, valor, seccion_presupuestal
            FROM ccpetrp_detalle WHERE vigencia = '$rowRpCab[0]' AND consvigencia = '$rowRpCab[1]' AND tipo_mov = '201' $filtroCondicion";

            $resRpDet = mysqli_query($linkbd, $sqlRpDet);
            $cantidadRegistros = mysqli_num_rows($resRpDet);
            while ($rowRpDet = mysqli_fetch_row($resRpDet)) {

                $detalles = [];

                $sqlrIng = "SELECT nombre, tipo FROM cuentasccpet WHERE version = $maxVersion AND codigo = '$rowRpDet[0]' ORDER BY id ASC";
                $resIng = mysqli_query($linkbd, $sqlrIng);
                $rowIng = mysqli_fetch_row($resIng);

                $cuenta = $rowRpDet[0] . " - " . $rowIng[0];

                $sqlFuente = "SELECT nombre FROM ccpet_fuentes_cuipo WHERE codigo_fuente = '$rowRpDet[2]'";
                $rowFuente = mysqli_fetch_row(mysqli_query($linkbd, $sqlFuente));

                $fuente = $rowRpDet[2] . " - " . $rowFuente[0];

                $sqlrProyecto = "SELECT nombre FROM ccpproyectospresupuesto WHERE  codigo = '$rowRpDet[3]'";
                $resProyecto = mysqli_query($linkbd, $sqlrProyecto);
                $rowProyecto = mysqli_fetch_row($resProyecto);

                $proyecto = $rowRpDet[3] . " - " . $rowProyecto[0];

                $rubro = '';

                if($rowRpDet[3] != ''){
                    $rubro = $rowRpDet[4]."-".$rowRpDet[3]."-".$rowRpDet[2]."-".$rowRpDet[8]."-".$rowRpDet[6]."-".$rowRpDet[5];
                }else{
                    $rubro = $rowRpDet[0];
                }


                $sqlVigenciaGasto = "SELECT nombre FROM ccpet_vigenciadelgasto WHERE codigo = $rowRpDet[6]";
                $rowVigenciaGasto = mysqli_fetch_row(mysqli_query($linkbd, $sqlVigenciaGasto));

                $vigenciaGasto = $rowVigenciaGasto[0];

                $sqlRpReversado = "SELECT SUM(valor) FROM ccpetrp_detalle WHERE vigencia = '$rowRpCab[0]' AND consvigencia = '$rowRpCab[1]' AND tipo_mov LIKE '4%' AND cuenta = '$rowRpDet[0]' AND fuente = '$rowRpDet[2]' AND bpim = '$rowRpDet[3]' AND indicador_producto = '$rowRpDet[4]' AND medio_pago = '$rowRpDet[5]' AND codigo_vigenciag = '$rowRpDet[6]' AND productoservicio = '$rowRpDet[1]' AND seccion_presupuestal = '$rowRpDet[8]'";
                $resRpReversado = mysqli_query($linkbd, $sqlRpReversado);
                $rowRpReversado = mysqli_fetch_row($resRpReversado);

                $idRp= $rowRpCab[1];
                $valorRp = round($rowRpDet[7]-$rowRpReversado[0], 2);
                $saldo = getSaldo($idRp,$rowRpCab[0], $rowRpDet[0], $rowRpDet[2], $rowRpDet[3], $rowRpDet[4], $rowRpDet[5], $rowRpDet[6], $rowRpDet[1], $rowRpDet[8]);

                $tercero = selectTercero($rowRpCab[5]);
                /* if ($cont == 0){ */

                    array_push($detalles, $rowRpCab[0]);
                    array_push($detalles, $rowRpCab[1]);
                    array_push($detalles, $rowRpCab[2]);
                    array_push($detalles, $rowRpCab[3]);
                    array_push($detalles, $rowRpCab[4]);
                    array_push($detalles, $rowRpCab[5]);
                    array_push($detalles, $tercero);
                    array_push($detalles, $rowRpCab[6]);
                    array_push($detalles, $rubro);
                    array_push($detalles, $cuenta);
                    array_push($detalles, $rowRpDet[1]);
                    array_push($detalles, $fuente);
                    array_push($detalles, $proyecto);
                    array_push($detalles, $rowRpDet[4]);
                    array_push($detalles, $rowRpDet[5]);
                    array_push($detalles, $vigenciaGasto);
                    array_push($detalles, $valorRp);
                    array_push($detalles, $saldo);

                /* }
                else {
                    array_push($detalles, ' ');
                    array_push($detalles, ' ');
                    array_push($detalles, ' ');
                    array_push($detalles, ' ');
                    array_push($detalles, ' ');
                    array_push($detalles, ' ');
                    array_push($detalles, ' ');
                    array_push($detalles, $cuenta);
                    array_push($detalles, $rowRpDet[1]);
                    array_push($detalles, $fuente);
                    array_push($detalles, $proyecto);
                    array_push($detalles, $rowRpDet[4]);
                    array_push($detalles, $rowRpDet[5]);
                    array_push($detalles, $vigenciaGasto);
                    array_push($detalles, $valorRp);
                    array_push($detalles, $saldo);
                    array_push($detalles, $rubro);
                } */

                array_push($rps, $detalles);

                $cont++;
            }
        }

        $out['cdps'] = $rps;
    }

    header("Content-type: application/json");
    echo json_encode($out);
    die();
