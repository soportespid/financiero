<?php
	require_once '../../comun.inc';
    require '../../funciones.inc';
    require '../../conversor.php';
    require '../../funcionesSP.inc.php';

    session_start();

	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");

    $out = array('error' => false);

    $action = "show";

    function getSaldo($idCdp,$vigencia, $cuenta, $fuente, $bpim, $indicador_producto, $medio_pago, $codigo_vigenciag, $productoservicio, $seccion_presupuestal){
        $linkbd = conectar_v7();
        $linkbd -> set_charset("utf8");
        $sql = "SELECT COALESCE(SUM(det.valor),0) as valor
        FROM ccpetrp cab
        INNER JOIN ccpetrp_detalle det ON det.consvigencia = cab.consvigencia AND det.vigencia = cab.vigencia
        WHERE cab.vigencia = '$vigencia' AND cab.idcdp = $idCdp AND cab.tipo_mov = '201' AND det.cuenta = '$cuenta' AND det.fuente = '$fuente' AND det.bpim = '$bpim' AND det.indicador_producto = '$indicador_producto' AND det.medio_pago = '$medio_pago' AND det.codigo_vigenciag = '$codigo_vigenciag' AND det.productoservicio = '$productoservicio' AND det.seccion_presupuestal = '$seccion_presupuestal'";
        $request = mysqli_query($linkbd,$sql)->fetch_assoc()['valor'];

        $sqlReversado = "SELECT COALESCE(SUM(det.valor),0) as valor
        FROM ccpetrp cab
        INNER JOIN ccpetrp_detalle det ON det.consvigencia = cab.consvigencia AND det.vigencia = cab.vigencia
        WHERE cab.vigencia = '$vigencia' AND cab.idcdp = $idCdp AND det.tipo_mov LIKE '4%' AND det.cuenta = '$cuenta' AND det.fuente = '$fuente' AND det.bpim = '$bpim' AND det.indicador_producto = '$indicador_producto' AND det.medio_pago = '$medio_pago' AND det.codigo_vigenciag = '$codigo_vigenciag' AND det.productoservicio = '$productoservicio' AND det.seccion_presupuestal = '$seccion_presupuestal'";
        $requestRev= mysqli_query($linkbd, $sqlReversado)->fetch_assoc()['valor'];

        $saldoRp = round($request - $requestRev, 2);

        return $saldoRp;
    }
    if(isset($_POST['action'])){
        $action = $_POST['action'];
    }

    if ($action == "datosFormulario") {

        $maxVersion = ultimaVersionGastosCCPET();
        $maxVersionIngresos = ultimaVersionIngresosCCPET();

        preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST["fechaIni"], $fecha);
        $fechaIni = $fecha[3]."-".$fecha[2]."-".$fecha[1];

        preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST["fechaFin"], $fecha);
        $fechaFin = $fecha[3]."-".$fecha[2]."-".$fecha[1];
        $intFiltro = intval($_POST['filtro']);

        $cdps = array();


        $filtroCondicion = "";
        if($intFiltro == 1){
            $filtroCondicion = "AND SUBSTRING(cuenta, 1, 3) = '2.3'";
        }else if($intFiltro == 2){
            $filtroCondicion = "AND SUBSTRING(cuenta, 1, 3) = '2.2'";
        }else if($intFiltro == 3){
            $filtroCondicion = "AND SUBSTRING(cuenta, 1, 3) = '2.1'";
        }
        $sqlCdpCab = "SELECT vigencia, consvigencia, DATE_FORMAT(fecha,'%d/%m/%Y') as fecha, objeto
        FROM ccpetcdp WHERE fecha BETWEEN '$fechaIni' AND '$fechaFin' AND tipo_mov = '201' ORDER BY consvigencia ASC";

        $resCdpCab = mysqli_query($linkbd, $sqlCdpCab);
        while ($rowCdpCab = mysqli_fetch_row($resCdpCab)) {

            $cont = 0;

            $sqlCdpDet = "SELECT cuenta, productoservicio, fuente, bpim, indicador_producto, medio_pago, codigo_vigenciag, valor, seccion_presupuestal
            FROM ccpetcdp_detalle WHERE vigencia = '$rowCdpCab[0]' AND consvigencia = '$rowCdpCab[1]' AND tipo_mov = '201' $filtroCondicion";

            $resCdpDet = mysqli_query($linkbd, $sqlCdpDet);
            $cantidadRegistros = mysqli_num_rows($resCdpDet);
            while ($rowCdpDet = mysqli_fetch_row($resCdpDet)) {

                $detalles = array();

                $sqlrIng = "SELECT nombre, tipo FROM cuentasccpet WHERE version = $maxVersion AND codigo = '$rowCdpDet[0]' ORDER BY id ASC";
                $resIng = mysqli_query($linkbd, $sqlrIng);
                $rowIng = mysqli_fetch_row($resIng);

                $cuenta = $rowCdpDet[0] . " - " . $rowIng[0];

                $sqlFuente = "SELECT nombre FROM ccpet_fuentes_cuipo WHERE codigo_fuente = '$rowCdpDet[2]'";
                $rowFuente = mysqli_fetch_row(mysqli_query($linkbd, $sqlFuente));

                $fuente = $rowCdpDet[2] . " - " . $rowFuente[0];

                $sqlrProyecto = "SELECT nombre FROM ccpproyectospresupuesto WHERE  codigo = '$rowCdpDet[3]'";
                $resProyecto = mysqli_query($linkbd, $sqlrProyecto);
                $rowProyecto = mysqli_fetch_row($resProyecto);

                $proyecto = $rowCdpDet[3] . " - " . $rowProyecto[0];

                $rubro = '';

                if($rowCdpDet[3] != ''){
                    $rubro = $rowCdpDet[4]."-".$rowCdpDet[3]."-".$rowCdpDet[2]."-".$rowCdpDet[8]."-".$rowCdpDet[6]."-".$rowCdpDet[5];
                }else{
                    $rubro = $rowCdpDet[0];
                }


                $sqlVigenciaGasto = "SELECT nombre FROM ccpet_vigenciadelgasto WHERE codigo = $rowCdpDet[6]";
                $rowVigenciaGasto = mysqli_fetch_row(mysqli_query($linkbd, $sqlVigenciaGasto));

                $vigenciaGasto = $rowVigenciaGasto[0];

                $sqlCdpReversado = "SELECT SUM(valor) FROM ccpetcdp_detalle WHERE vigencia = '$rowCdpCab[0]' AND consvigencia = '$rowCdpCab[1]' AND tipo_mov LIKE '4%' AND cuenta = '$rowCdpDet[0]' AND fuente = '$rowCdpDet[2]' AND bpim = '$rowCdpDet[3]' AND indicador_producto = '$rowCdpDet[4]' AND medio_pago = '$rowCdpDet[5]' AND codigo_vigenciag = '$rowCdpDet[6]' AND productoservicio = '$rowCdpDet[1]' AND seccion_presupuestal = '$rowCdpDet[8]'";
                $resCdpReversado = mysqli_query($linkbd, $sqlCdpReversado);
                $rowCdpReversado = mysqli_fetch_row($resCdpReversado);

                $idCdp= $rowCdpCab[1];
                $valorCdp = round($rowCdpDet[7]-$rowCdpReversado[0], 2);
                $valorRp = getSaldo($idCdp,$rowCdpCab[0], $rowCdpDet[0], $rowCdpDet[2], $rowCdpDet[3], $rowCdpDet[4], $rowCdpDet[5], $rowCdpDet[6], $rowCdpDet[1], $rowCdpDet[8]);
                $saldo = round($valorCdp-$valorRp, 2);
                if ($cont == 0){

                    array_push($detalles, $rowCdpCab[0]);
                    array_push($detalles, $rowCdpCab[1]);
                    array_push($detalles, $rowCdpCab[2]);
                    array_push($detalles, $rowCdpCab[3]);
                    array_push($detalles, $cuenta);
                    array_push($detalles, $rowCdpDet[1]);
                    array_push($detalles, $fuente);
                    array_push($detalles, $proyecto);
                    array_push($detalles, $rowCdpDet[4]);
                    array_push($detalles, $rowCdpDet[5]);
                    array_push($detalles, $vigenciaGasto);
                    array_push($detalles, $valorCdp);
                    array_push($detalles, $saldo);
                    array_push($detalles, $rubro);
                }
                else {
                    array_push($detalles, ' ');
                    array_push($detalles, ' ');
                    array_push($detalles, ' ');
                    array_push($detalles, ' ');
                    array_push($detalles, $cuenta);
                    array_push($detalles, $rowCdpDet[1]);
                    array_push($detalles, $fuente);
                    array_push($detalles, $proyecto);
                    array_push($detalles, $rowCdpDet[4]);
                    array_push($detalles, $rowCdpDet[5]);
                    array_push($detalles, $vigenciaGasto);
                    array_push($detalles, $valorCdp);
                    array_push($detalles, $saldo);
                    array_push($detalles, $rubro);
                }

                array_push($cdps, $detalles);

                $cont++;
            }
        }

        $out['cdps'] = $cdps;
    }

    header("Content-type: application/json");
    echo json_encode($out);
    die();
