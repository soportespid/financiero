<?php
    require_once '../../../comun.inc';
    require '../../../funciones.inc';
    require '../../../funcionesSP.inc.php';
    /*ini_set('display_errors', '1');
    ini_set('display_startup_errors', '1');
    error_reporting(E_ALL);*/
    session_start();
    //dep($_POST);exit;
    if($_POST){
        $obj = new Plantilla();
        if($_POST['action']=="get"){
            $obj->getData();
        }
    }

    class Plantilla{
        private $linkbd;
        private $arrData;
        public function __construct() {
            $this->linkbd = conectar_v7();
            $this->linkbd->set_charset("utf8");
        }
        public function getData(){
            if(!empty($_SESSION)){
                $fechaInicial = explode("/",$_POST['fecha_inicial']);
                $fechaInicial = $fechaInicial[2]."-".$fechaInicial[1]."-".$fechaInicial[0];
                $fechaFinal = explode("/",$_POST['fecha_final']);
                $fechaFinal = $fechaFinal[2]."-".$fechaFinal[1]."-".$fechaFinal[0];
                echo json_encode($this->selectData($fechaInicial,$fechaFinal),JSON_UNESCAPED_UNICODE);
            }
            die();
        }
        public function selectData($fechaInicial,$fechaFinal){
            $arrNewData = [];
            $arrData = [];
            $arrData[] = $this->selectPttoReciboCaja($fechaInicial,$fechaFinal);
            $arrData[]  = $this->selectPttoSinReciboCaja($fechaInicial,$fechaFinal);
            $arrData[]  = $this->selectNotasBancarias($fechaInicial,$fechaFinal);
            $arrData[]  = $this->selectRecaudosTransferencia($fechaInicial,$fechaFinal);
            $arrData[]  = $this->selectRetencionesPago($fechaInicial,$fechaFinal);
            $arrData[]  = $this->selectRetencionesOtrosPago($fechaInicial,$fechaFinal);
            $arrData[]  = $this->selectRecaudoFacturas($fechaInicial,$fechaFinal);
            $arrData[]  = $this->selectSuperavit($fechaInicial,$fechaFinal);
            $arrData[]  = $this->selectCumaribo($fechaInicial,$fechaFinal);
            $arrData[] = $this->selectRetencionesOrdenPago($fechaInicial,$fechaFinal);
            $total = count($arrData);
            $totalValor = 0;
            for ($i=0; $i < $total ; $i++) {
                $totalData = count($arrData[$i]);
                for ($j=0; $j < $totalData ; $j++) {
                    $totalValor+= $arrData[$i][$j]['valor'];
                    array_push($arrNewData,$arrData[$i][$j]);
                }
            }
            return array("data"=>$arrNewData,"total"=>$totalValor);
        }
        public function selectPttoReciboCaja($fechaInicial,$fechaFinal){
            $sql = "SELECT
                prc.idrecibo as id,
                prc.vigencia_gasto as vigencia,
                prc.cuenta,
                trc.id_comp,
                rec.tercero as cedulanit,
                rec.concepto,
                cta.nombre as nombre_banco,
                trc.id_recaudo as numero_reconocimiento,
                bcta.ncuentaban as cuenta_banco,
                DATE_FORMAT(trc.fecha,'%Y-%m-%d') as fecha_csv,
                DATE_FORMAT(trc.fecha,'%d/%m/%Y') as fecha,
                trc.cuentabanco as cuenta_contable,
                CONCAT(t.razonsocial,' ',t.nombre1,' ',t.nombre2,' ',t.apellido1,' ',t.apellido2) as nombre_tercero,
                SUM(prc.valor) as valor
            FROM pptorecibocajappto prc
            INNER JOIN tesoreciboscaja trc ON trc.id_recibos = prc.idrecibo
            LEFT JOIN tesorecaudos rec ON rec.id_recaudo = trc.id_recaudo
            LEFT JOIN terceros t ON rec.tercero = t.cedulanit
            INNER JOIN cuentasnicsp cta ON trc.cuentabanco = cta.cuenta
            INNER JOIN tesobancosctas bcta ON bcta.cuenta = trc.cuentabanco
            WHERE (trc.estado != 'N' OR trc.estado != 'R') AND prc.tipo != 'R' AND trc.fecha BETWEEN '$fechaInicial' AND '$fechaFinal'
            GROUP by prc.fuente, prc.seccion_presupuestal, prc.medio_pago, prc.vigencia_gasto, prc.productoservicio";
            $request = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
            return $request;
        }
        public function selectPttoSinReciboCaja($fechaInicial,$fechaFinal){
            $sql = "SELECT
                prc.idrecibo as id,
                prc.vigencia_gasto as vigencia,
                prc.cuenta,
                rec.tercero as cedulanit,
                rec.concepto,
                cta.nombre as nombre_banco,
                trc.id_recaudo as numero_reconocimiento,
                bcta.ncuentaban as cuenta_banco,
                DATE_FORMAT(trc.fecha,'%Y-%m-%d') as fecha_csv,
                DATE_FORMAT(trc.fecha,'%d/%m/%Y') as fecha,
                trc.cuentabanco as cuenta_contable,
                CONCAT(t.razonsocial,' ',t.nombre1,' ',t.nombre2,' ',t.apellido1,' ',t.apellido2) as nombre_tercero,
                SUM(prc.valor) as valor
            FROM pptosinrecibocajappto prc
            INNER JOIN tesosinreciboscaja trc ON trc.id_recibos = prc.idrecibo
            LEFT JOIN tesorecaudos rec ON rec.id_recaudo = trc.id_recaudo
            LEFT JOIN terceros t ON rec.tercero = t.cedulanit
            INNER JOIN cuentasnicsp cta ON trc.cuentabanco = cta.cuenta
            INNER JOIN tesobancosctas bcta ON bcta.cuenta = trc.cuentabanco
            WHERE (trc.estado != 'N' OR trc.estado != 'R') AND trc.fecha BETWEEN '$fechaInicial' AND '$fechaFinal'
            GROUP by prc.fuente, prc.seccion_presupuestal, prc.medio_pago, prc.vigencia_gasto, prc.productoservicio";
            $request = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
            return $request;
        }
        public function selectNotasBancarias($fechaInicial,$fechaFinal){
            $sql = "SELECT
                prc.idrecibo as id,
                prc.vigencia_gasto as vigencia,
                prc.cuenta,
                SUM(prc.valor) as valor,
                nota.idrecibo as numero_reconocimiento,
                nota.concepto,
                DATE_FORMAT(nota.fecha,'%Y-%m-%d') as fecha_csv,
                DATE_FORMAT(nota.fecha,'%d/%m/%Y') as fecha,
                notadet.tercero as cedulanit,
                notadet.ncuentaban as cuenta_banco,
                ctas.cuenta as cuenta_contable,
                nic.nombre as nombre_banco
            FROM pptonotasbanppto prc
            INNER JOIN tesonotasbancarias_cab nota ON prc.idrecibo = nota.id_comp
            INNER JOIN tesonotasbancarias_det notadet ON nota.id_comp = notadet.id_notabancab
            INNER JOIN tesobancosctas ctas ON notadet.ncuentaban = ctas.ncuentaban
            INNER JOIN cuentasnicsp nic ON nic.cuenta = ctas.cuenta
            WHERE (nota.estado != 'N' OR nota.estado != 'R') AND nota.fecha BETWEEN '$fechaInicial' AND '$fechaFinal'
            GROUP BY prc.cuenta_clasificadora,prc.fuente, prc.seccion_presupuestal, prc.medio_pago, prc.vigencia_gasto";
            $request = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);

            $total = count($request);
            for ($i=0; $i < $total ; $i++) {
                $sql_tercero = "SELECT CONCAT(razonsocial,' ',nombre1,' ',nombre2,' ',apellido1,' ',apellido2) as nombre
                FROM terceros WHERE cedulanit = {$request[$i]['cedulanit']}";
                $request[$i]['nombre_tercero'] = mysqli_query($this->linkbd,$sql_tercero)->fetch_assoc()['nombre'];
            }
            return $request;
        }
        public function selectRecaudosTransferencia($fechaInicial,$fechaFinal){
            $sql="SELECT
                pitp.cuenta,
                pitp.idrecibo as id,
                pitp.vigencia_gasto,
                SUM(pitp.valor) as valor,
                titp.concepto,
                titp.tercero as cedulanit,
                titp.ncuentaban as cuenta_banco,
                titp.idcomp as numero_reconocimiento,
                DATE_FORMAT(titp.fecha,'%Y-%m-%d') as fecha_csv,
                DATE_FORMAT(titp.fecha,'%d/%m/%Y') as fecha,
                CONCAT(t.razonsocial,' ',t.nombre1,' ',t.nombre2,' ',t.apellido1,' ',t.apellido2) as nombre_tercero,
                cta.cuenta as cuenta_contable,
                cta.nombre as nombre_banco
            FROM pptoingtranppto pitp
            INNER JOIN tesorecaudotransferencia titp ON pitp.idrecibo = titp.id_recaudo
            LEFT JOIN terceros t ON titp.tercero = t.cedulanit
            INNER JOIN tesobancosctas bcta ON bcta.ncuentaban = titp.ncuentaban
            INNER JOIN cuentasnicsp cta ON bcta.cuenta = cta.cuenta
            WHERE (titp.estado != 'N' OR titp.estado != 'R') AND titp.fecha BETWEEN '$fechaInicial' AND '$fechaFinal'
            GROUP BY pitp.fuente, pitp.seccion_presupuestal, pitp.medio_pago, pitp.vigencia_gasto, pitp.productoservicio
            ";
            $request = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
            return $request;
        }
        public function selectRetencionesPago($fechaInicial,$fechaFinal){
            $sql="SELECT
                prc.cuenta,
                prc.idrecibo as id,
                prc.vigencia_gasto,
                SUM(prc.valor) as valor,
                trc.concepto,
                trc.tercero as cedulanit,
                trc.cuentabanco as cuenta_banco,
                trc.id_comp as numero_reconocimiento,
                DATE_FORMAT(trc.fecha,'%Y-%m-%d') as fecha_csv,
                DATE_FORMAT(trc.fecha,'%d/%m/%Y') as fecha,
                CONCAT(t.razonsocial,' ',t.nombre1,' ',t.nombre2,' ',t.apellido1,' ',t.apellido2) as nombre_tercero,
                trc.banco as cuenta_contable,
                cta.nombre as nombre_banco
            FROM pptoretencionpago prc
            INNER JOIN tesoegresos trc ON prc.idrecibo = trc.id_egreso
            LEFT JOIN terceros t ON trc.tercero = t.cedulanit
            INNER JOIN cuentasnicsp cta ON trc.banco = cta.cuenta
            WHERE (trc.estado != 'N' OR trc.estado != 'R') AND trc.fecha BETWEEN '$fechaInicial' AND '$fechaFinal'
            AND trc.tipo_mov='201' AND prc.tipo='egreso' AND NOT EXISTS (SELECT 1 FROM tesoegresos tra WHERE tra.id_egreso=trc.id_egreso  AND tra.tipo_mov='401')
            GROUP BY prc.fuente, prc.seccion_presupuestal, prc.medio_pago, prc.vigencia_gasto, prc.productoservicio
            ";
            $request = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
            return $request;
        }
        public function selectRetencionesOrdenPago($fechaInicial,$fechaFinal){
            $sql="SELECT
                prc.cuenta,
                prc.idrecibo as id,
                prc.vigencia_gasto,
                SUM(prc.valor) as valor,
                trc.conceptorden as concepto,
                trc.tercero as cedulanit,
                trc.id_comp as numero_reconocimiento,
                DATE_FORMAT(trc.fecha,'%Y-%m-%d') as fecha_csv,
                DATE_FORMAT(trc.fecha,'%d/%m/%Y') as fecha,
                CONCAT(t.razonsocial,' ',t.nombre1,' ',t.nombre2,' ',t.apellido1,' ',t.apellido2) as nombre_tercero,
            FROM pptoretencionpago prc
            INNER JOIN tesoordenpago trc ON prc.idrecibo = trc.id_egreso
            LEFT JOIN terceros t ON trc.tercero = t.cedulanit
            WHERE trc.estado != 'N' AND trc.fecha BETWEEN '$fechaInicial' AND '$fechaFinal'
            AND trc.tipo_mov='201' AND prc.tipo='orden' AND NOT EXISTS (SELECT 1 FROM tesoordenpago tca WHERE tca.id_orden=trc.id_orden  AND tca.tipo_mov='401')
            GROUP BY prc.fuente, prc.seccion_presupuestal, prc.medio_pago, prc.vigencia_gasto, prc.productoservicio
            ";
            $request = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
            if(!empty($request)){
                $total = count($request);
                for ($i=0; $i < $total ; $i++) {
                    $request[$i]['cuenta_banco'] = "";
                    $request[$i]['cuenta_contable'] = "";
                    $request[$i]['nombre_banco'] = "";
                }
            }
            return $request;
        }
        public function selectRetencionesOtrosPago($fechaInicial,$fechaFinal){
            $sql="SELECT
                prc.cuenta,
                prc.idrecibo as id,
                prc.vigencia_gasto,
                SUM(prc.valor) as valor,
                trc.concepto,
                trc.tercero as cedulanit,
                bcta.ncuentaban as cuenta_banco,
                trc.id_comp as numero_reconocimiento,
                DATE_FORMAT(trc.fecha,'%Y-%m-%d') as fecha_csv,
                DATE_FORMAT(trc.fecha,'%d/%m/%Y') as fecha,
                CONCAT(t.razonsocial,' ',t.nombre1,' ',t.nombre2,' ',t.apellido1,' ',t.apellido2) as nombre_tercero,
                trc.banco as cuenta_contable,
                cta.nombre as nombre_banco
            FROM pptoretencionotrosegresos prc
            INNER JOIN tesoegresos trc ON prc.idrecibo = trc.id_egreso
            LEFT JOIN terceros t ON trc.tercero = t.cedulanit
            INNER JOIN cuentasnicsp cta ON trc.banco = cta.cuenta
            INNER JOIN tesobancosctas bcta ON bcta.cuenta = trc.banco
            WHERE trc.estado != 'N'  AND trc.fecha BETWEEN '$fechaInicial' AND '$fechaFinal'
            GROUP BY prc.fuente, prc.seccion_presupuestal, prc.medio_pago, prc.vigencia_gasto, prc.productoservicio
            ";
            $request = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
            return $request;
        }
        public function selectRecaudoFacturas($fechaInicial,$fechaFinal){
            $sql="SELECT
                prc.cuenta,
                prc.codigo_recaudo as id,
                prc.vigencia_gasto,
                SUM(prc.valor) as valor,
                trc.concepto,
                trc.documento as cedulanit,
                bcta.ncuentaban as cuenta_banco,
                trc.codigo_recaudo as numero_reconocimiento,
                DATE_FORMAT(trc.fecha_recaudo,'%Y-%m-%d') as fecha_csv,
                DATE_FORMAT(trc.fecha_recaudo,'%d/%m/%Y') as fecha,
                trc.numero_cuenta as cuenta_contable,
                cta.nombre as nombre_banco,
                trc.suscriptor as nombre_tercero
            FROM srv_recaudo_factura_ppto prc
            INNER JOIN srv_recaudo_factura trc ON prc.codigo_recaudo = trc.codigo_recaudo
            INNER JOIN cuentasnicsp cta ON trc.numero_cuenta = cta.cuenta
            INNER JOIN tesobancosctas bcta ON bcta.cuenta = trc.numero_cuenta
            WHERE trc.estado != 'REVERSADO' AND trc.fecha_recaudo BETWEEN '$fechaInicial' AND '$fechaFinal'
            GROUP BY prc.fuente, prc.seccion_presupuestal, prc.medio_pago, prc.vigencia_gasto, prc.producto_servicio
            ";
            $request = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
            return $request;
        }
        public function selectSuperavit($fechaInicial,$fechaFinal){
            $sql="SELECT
                prc.id,
                DATE_FORMAT(prc.fecha,'%Y-%m-%d') as fecha_csv,
                DATE_FORMAT(prc.fecha,'%d/%m/%Y') as fecha,
                trc.rubro as cuenta,
                trc.id_tesosuperavit as numero_reconocimiento,
                prc.concepto,
                SUM(trc.valor) as valor
            FROM tesosuperavit prc
            INNER JOIN tesosuperavit_det trc ON prc.id = trc.id_tesosuperavit
            WHERE prc.estado = 'S' AND prc.fecha BETWEEN '$fechaInicial' AND '$fechaFinal'
            GROUP BY trc.fuente, trc.seccion_presupuestal, trc.medio_pago, trc.vigencia_gasto";
            $request = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
            if(!empty($request)){
                $sqlConfig="SELECT * FROM configbasica WHERE estado='S' ";
                $config = mysqli_query($this->linkbd,$sqlConfig)->fetch_assoc();
                $total = count($request);
                for ($i=0; $i < $total ; $i++) {
                    $request[$i]['cedulanit'] = $config['nit'];
                    $request[$i]['nombre_tercero'] = $config['razonsocial'];
                    $request[$i]['cuenta_banco'] = "";
                    $request[$i]['cuenta_contable'] = "";
                    $request[$i]['nombre_banco'] = "";
                }
            }
            return $request;
        }
        public function selectCumaribo($fechaInicial,$fechaFinal){
            $sql="SELECT
                prc.idrecibo as id,
                prc.vigencia_gasto as vigencia,
                prc.cuenta,
                cta.nombre as nombre_banco,
                trc.id_recaudo as numero_reconocimiento,
                bcta.ncuentaban as cuenta_banco,
                DATE_FORMAT(trc.fecha,'%Y-%m-%d') as fecha_csv,
                DATE_FORMAT(trc.fecha,'%d/%m/%Y') as fecha,
                trc.cuentabanco as cuenta_contable,
                SUM(prc.valor) as valor
            FROM pptosinrecibocajaspppto prc
            INNER JOIN tesosinreciboscajasp trc ON trc.id_recibos = prc.idrecibo
            INNER JOIN cuentasnicsp cta ON trc.cuentabanco = cta.cuenta
            INNER JOIN tesobancosctas bcta ON bcta.cuenta = trc.cuentabanco
            WHERE (trc.estado != 'N' OR trc.estado != 'R') AND trc.fecha BETWEEN '$fechaInicial' AND '$fechaFinal'
            GROUP by prc.fuente, prc.seccion_presupuestal, prc.medio_pago, prc.vigencia_gasto, prc.productoservicio";
            $request = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
            if(!empty($request)){
                $total = count($request);
                for ($i=0; $i < $total ; $i++) {
                    $request[$i]['cedulanit'] = "";
                    $request[$i]['nombre_tercero'] = "";
                    $request[$i]['cuenta_banco'] = "";
                    $request[$i]['cuenta_contable'] = "";
                    $request[$i]['nombre_banco'] = "";
                    $request[$i]['concepto'] = "";
                }
            }
            return $request;
        }
    }
?>
