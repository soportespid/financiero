const URL ='presupuesto_ccpet/plantilla_appui/relacion_ingresos/ccp-informeIngresos.php';

var app = new Vue({
    el:"#myapp",
    data() {
        return {
            isLoading:false,
            arrData:[]
        }
    },
    mounted() {

    },
    methods: {
        getData: async function(){
            let fechaInicial = document.querySelector("#fechaInicial").value;
            let fechaFinal = document.querySelector("#fechaFinal").value;
            if(fechaFinal == "" || fechaInicial ==""){
                Swal.fire("Error","Las fechas no pueden estar vacias","error");
                return false;
            }
            const formData = new FormData();
            formData.append("action","get");
            formData.append("fecha_inicial",document.querySelector("#fechaInicial").value);
            formData.append("fecha_final",document.querySelector("#fechaFinal").value);
            this.isLoading = true;
            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();
            this.arrData = objData.data;
            console.log(this.formatNumero(objData.total));
            this.isLoading = false;
        },
        printCsv:function(){
            let totalRows = this.arrData.length
            let fechaInicial = document.querySelector("#fechaInicial").value;
            let fechaFinal = document.querySelector("#fechaFinal").value;
            if(fechaFinal == "" || fechaInicial==""){
                Swal.fire("Error","Las fechas no pueden estar vacias","error");
                return false;
            }
            if(totalRows == 0){
                Swal.fire("Error","Debe generar datos antes de exportar a csv.","error");
                return false;
            }
            const delimiter = "|";

            const headers = [
                "id",
                "codigoPresupuestal",
                "vigencia",
                "macroCampo",
                "fechaRecaudo",
                "numeroRecibo",
                "nombreBanco",
                "cuentaBancariaOrig",
                "cuentaContable",
                "numeroReconocimiento",
                "nit",
                "nombre",
                "conceptoRecaudo",
                "valor",
                "cuentaBancariaDest"
            ];
            let csvContent = headers.join(delimiter)+"\n";

            for (let i = 0; i < totalRows; i++) {
                const e = this.arrData[i];
                const row = [
                    i+1,
                    e.cuenta,
                    e.vigencia,
                    2,
                    e.fecha_csv,
                    e.id,
                    e.nombre_banco,
                    "",
                    e.cuenta_contable,
                    e.numero_reconocimiento,
                    e.cedulanit,
                    e.nombre_tercero,
                    e.concepto,
                    e.valor.replace(".",","),
                    e.cuenta_banco,
                ];
                csvContent+=row.join(delimiter)+"\n";

            }
            const link = document.createElement('a');
            const fechaActual = new Date().toISOString().slice(0, 10);
            const arrFecha = fechaActual.split("-");
            link.href = 'data:text/csv;charset=utf-8,' + encodeURIComponent(csvContent);
            link.target = '_blank';
            link.download = `RELACION_DE_INGRESOS_${arrFecha[0]+""+arrFecha[1]+""+arrFecha[2]}.csv`;
            link.click();
        },
        formatNumero: function(valor){
            return new Intl.NumberFormat("en-US", {style: "currency", currency: "USD"}).format(valor);
        },
    },
    computed:{

    }
})
