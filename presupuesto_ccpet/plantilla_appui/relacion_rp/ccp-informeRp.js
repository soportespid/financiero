const URL ='presupuesto_ccpet/plantilla_appui/relacion_rp/ccp-informeRp.php';

var app = new Vue({
    el:"#myapp",
    data() {
        return {
            isLoading:false,
            arrData:[]
        }
    },
    mounted() {

    },
    methods: {
        getData: async function(){
            let fechaInicial = document.querySelector("#fechaInicial").value;
            let fechaFinal = document.querySelector("#fechaFinal").value;
            if(fechaFinal == "" || fechaInicial ==""){
                Swal.fire("Error","Las fechas no pueden estar vacias","error");
                return false;
            }
            const formData = new FormData();
            formData.append("action","get");
            formData.append("fecha_inicial",document.querySelector("#fechaInicial").value);
            formData.append("fecha_final",document.querySelector("#fechaFinal").value);
            this.isLoading = true;
            const response = await fetch(URL,{method:"POST",body:formData});
            this.arrData = await response.json();
            this.isLoading = false;
        },
        printCsv:function(){
            let totalRows = this.arrData.length
            let fechaInicial = document.querySelector("#fechaInicial").value;
            let fechaFinal = document.querySelector("#fechaFinal").value;
            if(fechaFinal == "" || fechaInicial==""){
                Swal.fire("Error","Las fechas no pueden estar vacias","error");
                return false;
            }
            if(totalRows == 0){
                Swal.fire("Error","Debe generar datos antes de exportar a csv.","error");
                return false;
            }
            const delimiter = "|";
            const headers = [
                "idCompromiso",
                "detalleCompromiso",
                "vigencia",
                "macroCampo",
                "codigoPresupuestal",
                "fechaCompromiso",
                "idCdp",
                "valorCompromiso",
                "nitTercero",
                "nombreTercero",
                "tipoCompromiso",
                "tipoGasto",
                "nombreRubro"
            ];
            let csvContent = headers.join(delimiter)+"\n";

            for (let i = 0; i < totalRows; i++) {
                const e = this.arrData[i];
                const row = [
                    e.id,
                    e.detalle,
                    e.vigencia,
                    2,
                    e.rubro,
                    e.fecha_csv,
                    e.idcdp,
                    e.valor,
                    e.cedulanit,
                    e.nombre_tercero,
                    2,
                    2,
                    e.nombre,
                ];
                csvContent+=row.join(delimiter)+"\n";

            }
            const link = document.createElement('a');
            const fechaActual = new Date().toISOString().slice(0, 10);
            const arrFecha = fechaActual.split("-");
            link.href = 'data:text/csv;charset=utf-8,' + encodeURIComponent(csvContent);
            link.target = '_blank';
            link.download = `RELACION_DE_COMPROMISOS_${arrFecha[0]+""+arrFecha[1]+""+arrFecha[2]}.csv`;
            link.click();
        },
        printExcel: function(){
            let fechaInicial = document.querySelector("#fechaInicial").value;
            let fechaFinal = document.querySelector("#fechaFinal").value;
            if(fechaFinal == "" || fechaInicial==""){
                Swal.fire("Error","Las fechas no pueden estar vacias","error");
                return false;
            }
            window.open("ccp-informeRpDiariExcel.php?fecha_inicial="+fechaInicial+"&fecha_final="+fechaFinal,"__blank");
        },
        formatNumero: function(valor){
            return new Intl.NumberFormat("en-US", {style: "currency", currency: "USD"}).format(valor);
        },
    },
    computed:{

    }
})
