<?php
    require_once '../../../comun.inc';
    require '../../../funciones.inc';
    require '../../../funcionesSP.inc.php';
    /*ini_set('display_errors', '1');
    ini_set('display_startup_errors', '1');
    error_reporting(E_ALL);*/
    session_start();
    //dep($_POST);exit;
    if($_POST){
        $obj = new Plantilla();
        if($_POST['action']=="get"){
            $obj->getData();
        }
    }

    class Plantilla{
        private $linkbd;
        private $arrData;
        public function __construct() {
            $this->linkbd = conectar_v7();
            $this->linkbd->set_charset("utf8");
        }
        public function getData(){
            if(!empty($_SESSION)){
                $fechaInicial = explode("/",$_POST['fecha_inicial']);
                $fechaInicial = $fechaInicial[2]."-".$fechaInicial[1]."-".$fechaInicial[0];
                $fechaFinal = explode("/",$_POST['fecha_final']);
                $fechaFinal = $fechaFinal[2]."-".$fechaFinal[1]."-".$fechaFinal[0];
                echo json_encode($this->selectData($fechaInicial,$fechaFinal),JSON_UNESCAPED_UNICODE);
            }
            die();
        }
        public function selectData($fechaInicial,$fechaFinal){
            $sql = "SELECT cab.consvigencia as id,cab.detalle,
            DATE_FORMAT(cab.fecha,'%Y-%m-%d') as fecha_csv,
            DATE_FORMAT(cab.fecha,'%d/%m/%Y') as fecha,
            cab.idcdp,
            det.codigo_vigenciag as vigencia,
            det.valor,
            t.cedulanit,
            det.tipo_gasto,
            det.tipo_mov,
            CONCAT(t.razonsocial,' ',t.nombre1,' ',t.nombre2,' ',t.apellido1,' ',t.apellido2) as nombre_tercero,
            CASE
                    WHEN SUBSTRING(det.cuenta, 1, 3) = '2.3' THEN CONCAT(det.indicador_producto,'-',det.bpim)
                    ELSE det.cuenta
            END AS rubro,
            CASE
                WHEN SUBSTRING(det.cuenta, 1, 3) IN ('2.1', '2.2') THEN (
                    SELECT nombre
                    FROM cuentasccpet
                    WHERE cuentasccpet.codigo = det.cuenta
                    LIMIT 1
                )
                WHEN SUBSTRING(det.cuenta, 1, 3) = '2.3' THEN (
                    SELECT nombre
                    FROM ccpproyectospresupuesto
                    WHERE ccpproyectospresupuesto.codigo = det.bpim
                    LIMIT 1
                )
                ELSE NULL
            END AS nombre
            FROM ccpetrp_detalle det
            INNER JOIN ccpetrp cab
            ON cab.consvigencia = det.consvigencia
            INNER JOIN terceros t
            ON cab.tercero = t.cedulanit
            WHERE (SUBSTRING(det.cuenta, 1, 3) = '2.1' OR SUBSTRING(det.cuenta, 1, 3) = '2.2' OR SUBSTRING(det.cuenta, 1, 3) = '2.3')
            AND ((cab.tipo_mov = '201' AND det.tipo_mov = '201') OR (cab.tipo_mov LIKE '4%' AND det.tipo_mov LIKE '4%'))
            AND cab.fecha BETWEEN '$fechaInicial' AND '$fechaFinal'
            ORDER BY DATE(cab.fecha) DESC;";
            $request = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
            $dataMov2 = array_values(array_filter($request,function($e){return $e['tipo_mov'] == "201";}));
            $dataMov4 = array_values(array_filter($request,function($e){return $e['tipo_mov'] != "201";}));
            $arrData = $this->filtrarDatos($dataMov2,$dataMov4);
            return $arrData;
        }
        public function filtrarDatos($dataMov2,$dataMov4){
            $totalMov2 = count($dataMov2);
            $totalMov4 = count($dataMov4);
            $arrData = [];
            for ($i=0; $i < $totalMov2; $i++) {
                $mov2 = $dataMov2[$i];
                $mov2['detalle'] = replaceChar($mov2['detalle']);
                $mov2['nombre'] = replaceChar($mov2['nombre']);
                $totalData = count($arrData);
                if($totalData > 0){
                    $flag = false;
                    for ($j=0; $j < $totalData ; $j++) {
                        $data = $arrData[$j];
                        if($mov2['id'] == $data['id'] && $mov2['rubro'] == $data['rubro']){
                            $data['valor']+=$mov2['valor'];
                            $flag = true;
                            break;
                        }
                    }
                    if(!$flag){
                        array_push($arrData,$mov2);
                    }
                }else{
                    array_push($arrData,$mov2);
                }
            }
            $totalData = count($arrData);
            for ($i=0; $i < $totalData; $i++) {
                for ($j=0; $j < $totalMov4 ; $j++) {
                    if($arrData[$i]['id'] == $dataMov4[$j]['id']){
                        $valor = $arrData[$i]['valor'] -$dataMov4[$j]['valor'];
                        $arrData[$i]['valor'] = abs($valor);
                        break;
                    }
                }
            }
            return $arrData;
        }
    }
?>
