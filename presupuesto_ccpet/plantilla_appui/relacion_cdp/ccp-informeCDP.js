const URL ='presupuesto_ccpet/plantilla_appui/relacion_cdp/ccp-informeCDP.php';

var app = new Vue({
    el:"#myapp",
    data() {
        return {
            isLoading:false,
            arrData:[]
        }
    },
    mounted() {

    },
    methods: {
        getData: async function(){
            let fechaInicial = document.querySelector("#fechaInicial").value;
            let fechaFinal = document.querySelector("#fechaFinal").value;
            if(fechaFinal == "" || fechaInicial ==""){
                Swal.fire("Error","Las fechas no pueden estar vacias","error");
                return false;
            }
            const formData = new FormData();
            formData.append("action","get");
            formData.append("fecha_inicial",document.querySelector("#fechaInicial").value);
            formData.append("fecha_final",document.querySelector("#fechaFinal").value);
            this.isLoading = true;
            const response = await fetch(URL,{method:"POST",body:formData});
            this.arrData = await response.json();
            this.isLoading = false;
        },
        printCsv:function(){
            let totalRows = this.arrData.length;
            let fechaInicial = document.querySelector("#fechaInicial").value;
            let fechaFinal = document.querySelector("#fechaFinal").value;
            if(fechaFinal == "" || fechaInicial==""){
                Swal.fire("Error","Las fechas no pueden estar vacias","error");
                return false;
            }
            if(totalRows == 0){
                Swal.fire("Error","Debe generar datos antes de exportar a csv.","error");
                return false;
            }
            const delimiter = "|";
            const headers = [
                "idCdp",
                "detalleCdpObjeto",
                "vigencia",
                "macroCampo",
                "codigoPresupuestal",
                "fuente",
                "nombreRubro",
                "fechaCdp",
                "valorCdp"
            ];
            let csvContent = headers.join(delimiter)+"\n";
            for (let i = 0; i < totalRows; i++) {
                const e = this.arrData[i];
                const row = [
                    e.id,
                    e.detalle,
                    e.vigencia,
                    2,
                    e.rubro,
                    e.fuente,
                    e.nombre,
                    e.fecha_csv,
                    e.valor,
                ];
                csvContent+=row.join(delimiter)+"\n";

            }
            const link = document.createElement('a');
            const fechaActual = new Date().toISOString().slice(0, 10);
            const arrFecha = fechaActual.split("-");
            link.href = 'data:text/csv;charset=utf-8,' + encodeURIComponent(csvContent);
            link.target = '_blank';
            link.download = `RELACION_DE_CDP_${arrFecha[0]+""+arrFecha[1]+""+arrFecha[2]}.csv`;
            link.click();
        },
        printExcel: function(){
            let fechaInicial = document.querySelector("#fechaInicial").value;
            let fechaFinal = document.querySelector("#fechaFinal").value;
            if(fechaFinal == "" || fechaInicial==""){
                Swal.fire("Error","Las fechas no pueden estar vacias","error");
                return false;
            }
            window.open("ccp-informeCDPDiariExcel.php?fecha_inicial="+fechaInicial+"&fecha_final="+fechaFinal,"__blank");
        },
        formatNumero: function(valor){
            return new Intl.NumberFormat("en-US", {style: "currency", currency: "USD"}).format(valor);
        },
    },
    computed:{

    }
})
