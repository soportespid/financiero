<?php
    require_once '../../../comun.inc';
    require '../../../funciones.inc';
    require '../../../funcionesSP.inc.php';
    /*ini_set('display_errors', '1');
    ini_set('display_startup_errors', '1');
    error_reporting(E_ALL);*/
    session_start();
    //dep($_POST);exit;
    if($_POST){
        $obj = new Plantilla();
        if($_POST['action']=="get"){
            $obj->getData();
        }
    }

    class Plantilla{
        private $linkbd;
        private $arrData;
        public function __construct() {
            $this->linkbd = conectar_v7();
            $this->linkbd->set_charset("utf8");
        }
        public function getData(){
            if(!empty($_SESSION)){
                $fechaInicial = explode("/",$_POST['fecha_inicial']);
                $fechaInicial = $fechaInicial[2]."-".$fechaInicial[1]."-".$fechaInicial[0];
                $fechaFinal = explode("/",$_POST['fecha_final']);
                $fechaFinal = $fechaFinal[2]."-".$fechaFinal[1]."-".$fechaFinal[0];
                echo json_encode($this->selectData($fechaInicial,$fechaFinal),JSON_UNESCAPED_UNICODE);
            }
            die();
        }
        public function selectData($fechaInicial,$fechaFinal){
            $sql = "SELECT cab.id_orden as obligacion,
            cab.id_egreso as id,
            cab.concepto as detalle,
            DATE_FORMAT(cab.fecha,'%Y-%m-%d') as fecha_csv,
            DATE_FORMAT(cab.fecha,'%d/%m/%Y') as fecha,
            cab.retenciones,
            det.codigo_vigenciag as vigencia,
            sum(det.valor) as valor,
            cab.tercero as cedulanit,
            det.tipo_gasto,
            det.tipo_mov,
            CASE
                    WHEN SUBSTRING(det.cuentap, 1, 3) = '2.3' THEN CONCAT(det.indicador_producto,'-',det.bpim)
                    ELSE det.cuentap
            END AS rubro,
            CASE
                WHEN SUBSTRING(det.cuentap, 1, 3) IN ('2.1', '2.2') THEN (
                    SELECT nombre
                    FROM cuentasccpet
                    WHERE cuentasccpet.codigo = det.cuentap
                    LIMIT 1
                )
                WHEN SUBSTRING(det.cuentap, 1, 3) = '2.3' THEN (
                    SELECT nombre
                    FROM ccpproyectospresupuesto
                    WHERE ccpproyectospresupuesto.codigo = det.bpim
                    LIMIT 1
                )
                ELSE NULL
            END AS nombre
            FROM tesoordenpago_det det
            INNER JOIN tesoegresos cab
            ON cab.id_orden = det.id_orden
            WHERE (SUBSTRING(det.cuentap, 1, 3) = '2.1' OR SUBSTRING(det.cuentap, 1, 3) = '2.2' OR SUBSTRING(det.cuentap, 1, 3) = '2.3')
            AND cab.estado != 'R' AND cab.fecha BETWEEN '$fechaInicial' AND '$fechaFinal'
            GROUP BY det.id_orden,det.cuentap, det.codigo_vigenciag
            ORDER BY det.id_orden DESC;";
            $request = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
            for ($i=0; $i < count($request) ; $i++) {
                $request[$i]['nombre'] = replaceChar($request[$i]['nombre']);
                $request[$i]['detalle'] = replaceChar($request[$i]['detalle']);
            }
            return $request;
        }
    }
?>
