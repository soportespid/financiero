import { createApp } from '../../../Librerias/vue3/dist/vue.esm-browser.js'

const DIR_ORIGIN = window.location.origin + '/';
const DIR_PATHNAME = window.location.pathname.split("/", 2)[1] + '/';
const DIR_FULL = DIR_ORIGIN + '/' + DIR_PATHNAME;

const app = createApp({
    data() {
      return {
        detalles: [],
        numActo: '',
        loading: false,
      }
    },

    mounted() {
		  this.loading = false;
	  },

    computed: {
      existeInformacion(){
        const det = JSON.parse(JSON.stringify(this.detalles));
        return det.length > 0 ? true : false
      },

    },

    methods: {

      async buscarReduccion() {
        //console.log(JSON.parse(JSON.stringify(this.detallesAd)));
        let fechaInicial = document.getElementById('fc_1198971545').value;
        let fechaFinal = document.getElementById('fc_1198971546').value;

        if(this.numActo != '' || (fechaInicial && fechaFinal)){

          if(fechaInicial && fechaFinal){
            let arrFechaInicial = fechaInicial.split("/");
            fechaInicial = `${arrFechaInicial[2]}-${arrFechaInicial[1]}-${arrFechaInicial[0]}`;
            let arrFechaFinal = fechaFinal.split("/");
            fechaFinal = `${arrFechaFinal[2]}-${arrFechaFinal[1]}-${arrFechaFinal[0]}`;
          }else{
            fechaInicial = '';
            fechaFinal = '';
          }

          let formData = new FormData();
          formData.append("fechaInicial", fechaInicial);
          formData.append("fechaFinal", fechaFinal);
          formData.append("numActo", this.numActo);


          //const URL = `${DIR_FULL}presupuesto_ccpet/reduccion/buscar/ccp-buscarReduccion-vue.php?action=buscarReducciones`;

          await axios.post('presupuesto_ccpet/reduccion/buscar/ccp-buscarReduccion-vue.php?action=buscarReducciones',formData)
            .then((response) => {
              
              this.detalles = response.data.reducciones;
              
            });
            
        }else{

          Swal.fire(
              'Falta informaci&oacute;n para generar reporte.',
              'Verifique que el n&uacute;mero de acto administrativo o la fecha inicial y final tengan informaci&oacute;n para realizar la busqueda.',
              'warning'
          );

        }

      },

      toFormData(obj){
        var form_data = new FormData();
        for(var key in obj){
            form_data.append(key, obj[key]);
        }
        return form_data;
      },

      comprobarEstado(estado){
        switch(estado){
          case 'S':
            return 'Activo'
          break;
          case 'N':
            return 'Anulado'
          break;
          case 'F':
            return 'Finalizado'
          break;
        }
      },

      formatonumero: function(valor){
        return new Intl.NumberFormat("en-US", {style: "currency", currency: "USD"}).format(valor);
      },

      seleccionar: function(id) {
        var x = "ccp-visualizaReduccion.php?id="+id[0];
        window.open(x, '_blank');
        window.focus();
      }

    }
})
  
app.mount('#myapp')