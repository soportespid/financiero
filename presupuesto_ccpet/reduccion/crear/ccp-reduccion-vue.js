import { divideRubro } from "../../../funciones.js";

var app = new Vue({
    el: '#myapp',
	data:{

        vigencia: '',
        loading: false,
        acto_administrativo: '',
        actos_administrativo: [],

        valor_acuerdo: '$0.00',
        checked: '',
        tipoGasto: '',
        tiposDeGasto: [],
        selectUnidadEjecutora : '',
        unidadesejecutoras: [],
        optionsMediosPagos: [
			{ text: 'CSF', value: 'CSF' },
			{ text: 'SSF', value: 'SSF' }
		],
        selectMedioPago: 'CSF',

        selectVigenciaGasto: '',
        vigenciasdelgasto: [],
        mostrarFuncionamiento : true,

        cuenta:'',
        nombreCuenta: '',
        showModal_cuentas: false,
        searchCuenta : {keywordCuenta: ''},
        cuentasCcpet: [],

        fuente: '',
        nombreFuente: '',
        showModal_fuentes: false,
        fuentesCuipo: [],
        searchFuente : {keywordFuente: ''},

        mostrarInversion : false,
        codProyecto: '',
        nombreProyecto: '',
        showModal_proyectos: false,
        proyectos: [],
        searchProyecto : {keywordProyecto: ''},

        programatico: '',
        nombreProgramatico: '',
        showModal_programatico: false,
        programaticos: [],

        tipo_cuenta: '-1',
        tipos_cuenta: [
			{ text: 'Ingresos', value: 'I' },
			{ text: 'Gastos', value: 'G' }
		],

        totalGastos: '$0.00',
        totalIngresos: '$0.00',
        diferencia: '$0.00',

        mostrarTipoGasto: false,


        valor: 0,

        detalles: [],

        mostrarSaldo : true,
        saldo: 0,

        rubroPresupuestal: '',

        isModalPrograma:false,
        isModalSector:false,
        selectOrden:1,
        arrProgramas:[],
        arrSectores:[],
        arrProgramasCopy:[],
        arrSectoresCopy:[],
        arrFuentes: [],
        objSector:{codigo:"",nombre:""},
        objPrograma:{codigo:"",nombre:""},
        txtSearchProgramas:"",
        txtSearchSectores:"",
        txtResultadosProgramas:0,
        txtResultadosSectores:0,
    },

    mounted: async function(){

        await this.cargarFecha();
        this.seccionPresupuestal();
        this.vigenciasDelgasto();
        this.actosAdministrativos();
        this.getData();
    },

    methods: {
        showTab:function(tab){
            let tabs = this.$refs.rTabs.children;
            let tabsContent = this.$refs.rTabsContent.children;
            for (let i = 0; i < tabs.length; i++) {
                tabs[i].classList.remove("active");
                tabsContent[i].classList.remove("active")
            }
            tabs[tab-1].classList.add("active");
            tabsContent[tab-1].classList.add("active")
        },
        reCalcularDiferencia:function(){
            const vueContext = this;
            this.arrFuentes = [];
            for (let i = 0; i < vueContext.detalles.length; i++) {
                const detalle = vueContext.detalles[i];
                let credito = isNaN (detalle[15]) || detalle[15] == "" || detalle[15] == undefined ? 0 : detalle[15];
                let contraCredito = isNaN (detalle[16])|| detalle[16]=="" || detalle[15] == undefined ? 0 : detalle[16];
                credito = Math.round(parseFloat(credito));
                contraCredito = Math.round(parseFloat(contraCredito));
                if(vueContext.arrFuentes.length > 0){
                    let flag = false;
                    for (let j = 0; j < vueContext.arrFuentes.length; j++) {
                        const fuente = vueContext.arrFuentes[j];
                        if(fuente.fuente == detalle[13]){
                            fuente.credito+=parseFloat(credito);
                            fuente.contra_credito+=parseFloat(contraCredito);
                            fuente.saldo = parseFloat(fuente.credito) - parseFloat(fuente.contra_credito)
                            flag = true;
                            break;
                        }
                    }
                    if(!flag){
                        vueContext.arrFuentes.push(
                            {
                                fuente:detalle[13],
                                fuente_nombre:detalle[14],
                                credito:parseFloat(credito),
                                contra_credito:parseFloat(contraCredito),
                                saldo:parseFloat(credito) - parseFloat(contraCredito)
                            }
                        );
                    }
                }else{
                    vueContext.arrFuentes.push(
                        {
                            fuente:detalle[13],
                            fuente_nombre:detalle[14],
                            credito:parseFloat(credito),
                            contra_credito:parseFloat(contraCredito),
                            saldo:parseFloat(credito) - parseFloat(contraCredito)
                        }
                    );
                }
            }
        },
        getData: async function(){
            const formData = new FormData();
            formData.append("action","get");
            const response = await fetch('presupuesto_ccpet/adicion/crear/ccp-adicion.php',{method:"POST",body:formData});
            const objData = await response.json();

            this.arrProgramas = objData.data.programas;
            this.arrSectores = objData.data.sectores;
            this.arrProgramasCopy = objData.data.programas;
            this.arrSectoresCopy = objData.data.sectores;
            this.txtResultadosProgramas = this.arrProgramas.length;
            this.txtResultadosSectores = this.arrSectores.length;
            this.selectOrden = objData.data.orden;
        },
        search:function(type=""){
            let search = "";
            if(type == "modal_programa")search = this.txtSearchProgramas.toLowerCase();
            if(type == "modal_sector")search = this.txtSearchSectores.toLowerCase();
            if(type == "cod_sector") search = this.objSector.codigo;
            if(type == "cod_programa") search = this.objPrograma.codigo;

            if(type == "modal_sector"){
                this.arrSectoresCopy = [...this.arrSectores.filter(e=>e.codigo.toLowerCase().includes(search)
                    || e.nombre.toLowerCase().includes(search))];
                this.txtResultadosSectores = this.arrSectoresCopy.length;
            }else if(type == "modal_programa"){
                this.arrProgramasCopy = [...this.arrProgramas.filter(e=>e.codigo.toLowerCase().includes(search)
                    || e.nombre.toLowerCase().includes(search))];
                this.txtResultadosProgramas = this.arrProgramasCopy.length;
            }else if(type == "cod_sector"){
                this.objSector = {};
                this.objSector = {...this.arrSectores.filter(e=>e.codigo==search)[0]};
                this.changeSector();
            }else if(type == "cod_programa"){
                this.objPrograma = {};
                this.objPrograma = {...this.arrProgramas.filter(e=>e.codigo==search)[0]};
                this.changePrograma();
            }
        },
        changeSector:function(){
            if(this.objSector.codigo){
                this.objPrograma = {};
                this.codProyecto = "";
                this.programatico = "";
            }
        },
        changePrograma:function(){
            if(this.objPrograma.codigo){
                let sector = this.objPrograma.codigo.substring(0,2);
                this.objSector = {...this.arrSectores.filter(e=>e.codigo==sector)[0]};
                this.codProyecto = "";
                this.programatico = "";
            }
        },
        changeProgramatico:function(){
            if(this.programatico != ""){
                let sector = this.programatico.substring(0,2);
                let programa = this.programatico.substring(0,4);
                this.objSector = {...this.arrSectores.filter(e=>e.codigo==sector)[0]};
                this.objPrograma = {...this.arrProgramas.filter(e=>e.codigo==programa)[0]};
            }
        },
        selectItem:function(type="",{...item}){
            if(type == "modal_sector"){
                this.objSector = item;
                this.isModalSector = false;
                this.changeSector();
            }else if(type == "modal_programa"){
                this.objPrograma = item;
                this.isModalPrograma = false;
                this.changePrograma();
            }
        },
        cargarFecha: function(){

            const fechaAct = new Date().toJSON().slice(0,10).replace(/-/g,'/');
            const fechaArr = fechaAct.split('/');
            const fechaV = fechaArr[2]+'/'+fechaArr[1]+'/'+fechaArr[0];
            document.getElementById('fecha').value = fechaV;
            this.vigencia = fechaArr[0];
        },

        cambiaTipoCuenta: async function(bandera = true){
            if(this.tipo_cuenta == 'G'){
                this.mostrarTipoGasto = true;
                if(bandera){
                    await this.cargarTiposDeGasto();
                }
            }else{
                this.tipoGasto = '';
                this.tiposDeGasto = [];
                this.mostrarTipoGasto = false;
            }
            this.cambiaParametro();
        },

        actosAdministrativos: function(){
            const fechat = document.getElementById('fecha').value;
            const fechatAr = fechat.split('/');
            this.vigencia = fechatAr[2];
            axios.post('presupuesto_ccpet/reduccion/crear/ccp-reduccion-vue.php?action=actosAdm&vig=' + this.vigencia)
            .then(
                (response)=>{
                    this.actos_administrativo = response.data.actosAdm;
                }
            );
        },

        seleccionarActoAdm: async function(){
            this.loading =  true;
            this.actos_administrativo.forEach(function logArrayElements(element, index, array) {

                if(element[0] == app.acto_administrativo){
                    app.valor_acuerdo = new Intl.NumberFormat("en-US", {style: "currency", currency: "USD",minimumFractionDigits: 0, maximumFractionDigits: 2,}).format(element[6]);
                    let fechaActParts = element[3].split('-');
                    document.getElementById('fecha').value = fechaActParts[2]+'/'+fechaActParts[1]+'/'+fechaActParts[0];
                    //app.valor_acuerdo = element[5];
                }
            });

            await axios.post('presupuesto_ccpet/reduccion/crear/ccp-reduccion-vue.php?action=cargarDet&idAcuerdo=' + this.acto_administrativo)
            .then((response) => {
                app.detalles = response.data.detalles;
                app.totalGastos = new Intl.NumberFormat("en-US", {style: "currency", currency: "USD",minimumFractionDigits: 0, maximumFractionDigits: 2,}).format(response.data.totalIng);
                app.totalIngresos = new Intl.NumberFormat("en-US", {style: "currency", currency: "USD",minimumFractionDigits: 0, maximumFractionDigits: 2,}).format(response.data.totalGastos);

                /* if(response.data.tiposDeGasto.length > 0){
                    this.tipoGasto = response.data.tiposDeGasto[0][0];
                } */
            }).finally(() => {
                this.loading =  false;
            });

            this.diferenciaRed();
            this.reCalcularDiferencia();
        },

        cargarTiposDeGasto: async function(){

            await axios.post('vue/presupuesto_ccp/ccp-cdpbasico.php?action=cargarTiposDeGasto')
            .then((response) => {

                app.tiposDeGasto = response.data.tiposDeGasto;

                if(response.data.tiposDeGasto.length > 0){
                    this.tipoGasto = response.data.tiposDeGasto[0][0];
                }
            });
            /* this.cambiaParametro(); */
        },

        seccionPresupuestal: async function(){
            await axios.post('vue/presupuesto_ccp/ccp-inicialgastosfun.php?action=secciones')
            .then(
                (response)=>{
                    this.unidadesejecutoras = response.data.secciones;
                    this.selectUnidadEjecutora = this.unidadesejecutoras[0][0];
                }
            );
        },

        vigenciasDelgasto: async function(){
            await axios.post('vue/presupuesto_ccp/ccp-inicialgastosfun.php?action=vigenciasDelGasto')
            .then(
                (response) => {
                    this.vigenciasdelgasto = response.data.vigenciasDelGasto;
                    this.selectVigenciaGasto = this.vigenciasdelgasto[0][0];
                }
            );
        },

        agregarDetalle: function(){
            this.loading =  true;
            let difSaldo = 0;
            difSaldo = this.saldo - this.valor;

            if(!this.checked){

                if(this.tipo_cuenta != ''){

                    if(this.selectUnidadEjecutora != ''){

                        if(this.selectMedioPago != ''){

                            if(this.selectVigenciaGasto){

                                if(this.fuente != ''){

                                    if(this.valor > 0){

                                        if(difSaldo >= 0){

                                            if(this.tipoGasto == '3'){

                                                if(this.codProyecto != ''){

                                                    if(this.programatico != ''){

                                                        var detallesAgr = [];

                                                        detallesAgr.push(this.tipo_cuenta);

                                                        this.tiposDeGasto.forEach(function logArrayElements(element, index, array) {
                                                            if(element[0] == app.tipoGasto){
                                                                detallesAgr.push(element[1]);
                                                            }
                                                        });

                                                        detallesAgr.push(this.selectUnidadEjecutora);
                                                        this.unidadesejecutoras.forEach(function logArrayElements(element, index, array) {
                                                            if(element[0] == app.selectUnidadEjecutora){
                                                                detallesAgr.push(element[1].toLowerCase());
                                                            }
                                                        });
                                                        detallesAgr.push(this.selectMedioPago);
                                                        detallesAgr.push(this.selectVigenciaGasto);
                                                        this.vigenciasdelgasto.forEach(function logArrayElements(element, index, array) {
                                                            if(element[0] == app.selectVigenciaGasto){
                                                                detallesAgr.push(element[2].toLowerCase());
                                                            }
                                                        });

                                                        detallesAgr.push(this.codProyecto);
                                                        detallesAgr.push(this.nombreProyecto.toLowerCase().slice(0,30));
                                                        detallesAgr.push(this.programatico);
                                                        detallesAgr.push(this.nombreProgramatico.toLowerCase().slice(0,30));

                                                        /* }else{
                                                            detallesAgr.push(this.cuenta);
                                                            detallesAgr.push(this.nombreCuenta.toLowerCase().slice(0,15));
                                                            detallesAgr.push('');
                                                            detallesAgr.push('');
                                                        } */
                                                        detallesAgr.push('');
                                                        detallesAgr.push('');
                                                        detallesAgr.push(this.fuente);
                                                        detallesAgr.push(this.nombreFuente.toLowerCase().slice(0,30));
                                                        //detallesAgr.push(new Intl.NumberFormat().format(this.valor));

                                                        detallesAgr.push('');
                                                        detallesAgr.push(this.valor);

                                                        let existeDetalle = 0;
                                                        this.detalles.forEach(function(elemento){
                                                            let result = app.comparaArrays(elemento, detallesAgr);

                                                            if(result){
                                                                existeDetalle = 1;
                                                                return false;
                                                            }
                                                        });

                                                        if(existeDetalle == 0){

                                                            this.detalles.push(detallesAgr);
                                                            this.cuenta = '';
                                                            this.codProyecto = '';
                                                            this.nombreProyecto = '';
                                                            this.programatico = '';
                                                            this.nombreProgramatico = '';
                                                            this.nombreCuenta = '';
                                                            this.fuente = '';
                                                            this.nombreFuente = '';


                                                            /* if(this.tipo_cuenta == 'I'){
                                                                this.totalIngresos = parseFloat(this.totalIngresos) + parseFloat(this.valor);
                                                            }else{
                                                                this.totalGastos = parseFloat(this.totalGastos) + parseFloat(this.valor);
                                                            } */
                                                            if(this.tipo_cuenta == 'I'){
                                                                this.totalIngresos = this.sumarIngresos(parseFloat(this.valor));

                                                                this.totalIngresos= new Intl.NumberFormat("en-US", {style: "currency", currency: "USD",minimumFractionDigits: 0, maximumFractionDigits: 2,}).format(this.totalIngresos);
                                                            }else{
                                                                this.totalGastos = this.sumarGastos(parseFloat(this.valor));

                                                                this.totalGastos= new Intl.NumberFormat("en-US", {style: "currency", currency: "USD",minimumFractionDigits: 0, maximumFractionDigits: 2,}).format(this.totalGastos);
                                                            }

                                                            this.valor = 0;
                                                            this.saldo = 0;
                                                            this.cargarTiposDeGasto();
                                                            this.diferenciaRed();
                                                        }else{

                                                            Swal.fire(
                                                                'Detalle repetido.',
                                                                'Debe tener el rubro o alguno de sus clasificadores diferentes a los detalles que se han agregado.',
                                                                'warning'
                                                            );

                                                        }
                                                    }else{
                                                        Swal.fire(
                                                            'Falta informaci&oacute;n.',
                                                            'Falta escoger el indicador program&aacute;tico MGA.',
                                                            'warning'
                                                        );
                                                    }

                                                }else{
                                                    Swal.fire(
                                                        'Falta informaci&oacute;n.',
                                                        'Falta escoger el proyecto de inversi&oacute;n.',
                                                        'warning'
                                                    );
                                                }

                                            }else{

                                                var detallesAgr = [];

                                                detallesAgr.push(this.tipo_cuenta);

                                                if(this.tipo_cuenta == 'I'){
                                                    detallesAgr.push('Ingresos');
                                                }else{
                                                    this.tiposDeGasto.forEach(function logArrayElements(element, index, array) {
                                                        if(element[0] == app.tipoGasto){
                                                            detallesAgr.push(element[1]);
                                                        }
                                                    });
                                                }

                                                detallesAgr.push(this.selectUnidadEjecutora);
                                                this.unidadesejecutoras.forEach(function logArrayElements(element, index, array) {
                                                    if(element[0] == app.selectUnidadEjecutora){
                                                        detallesAgr.push(element[1].toLowerCase());
                                                    }
                                                });
                                                detallesAgr.push(this.selectMedioPago);
                                                detallesAgr.push(this.selectVigenciaGasto);
                                                this.vigenciasdelgasto.forEach(function logArrayElements(element, index, array) {
                                                    if(element[0] == app.selectVigenciaGasto){
                                                        detallesAgr.push(element[2].toLowerCase());
                                                    }
                                                });

                                                detallesAgr.push('');
                                                detallesAgr.push('');
                                                detallesAgr.push('');
                                                detallesAgr.push('');
                                                detallesAgr.push(this.cuenta);
                                                detallesAgr.push(this.nombreCuenta.toLowerCase().slice(0,30));

                                                /* }else{
                                                    detallesAgr.push(this.cuenta);
                                                    detallesAgr.push(this.nombreCuenta.toLowerCase().slice(0,15));
                                                    detallesAgr.push('');
                                                    detallesAgr.push('');
                                                } */

                                                detallesAgr.push(this.fuente);
                                                detallesAgr.push(this.nombreFuente.toLowerCase().slice(0,30));

                                                //detallesAgr.push(new Intl.NumberFormat().format(this.valor));
                                                if(this.tipo_cuenta == 'I'){
                                                    detallesAgr.push(this.valor);
                                                    detallesAgr.push('');
                                                }else{
                                                    detallesAgr.push('');
                                                    detallesAgr.push(this.valor);
                                                }


                                                let existeDetalle = 0;
                                                this.detalles.forEach(function(elemento){
                                                    let result = app.comparaArrays(elemento, detallesAgr);

                                                    if(result){
                                                        existeDetalle = 1;
                                                        return false;
                                                    }
                                                });

                                                if(existeDetalle == 0){

                                                    this.detalles.push(detallesAgr);
                                                    this.cuenta = '';
                                                    this.codProyecto = '';
                                                    this.nombreProyecto = '';
                                                    this.programatico = '';
                                                    this.nombreProgramatico = '';
                                                    this.nombreCuenta = '';
                                                    this.fuente = '';
                                                    this.nombreFuente = '';
                                                    if(this.tipo_cuenta == 'I'){
                                                        this.totalIngresos = this.sumarIngresos(parseFloat(this.valor));
                                                        this.totalIngresos= new Intl.NumberFormat("en-US", {style: "currency", currency: "USD",minimumFractionDigits: 0, maximumFractionDigits: 2,}).format(this.totalIngresos);
                                                    }else{
                                                        this.totalGastos = this.sumarGastos(parseFloat(this.valor));
                                                        this.totalGastos= new Intl.NumberFormat("en-US", {style: "currency", currency: "USD",minimumFractionDigits: 0, maximumFractionDigits: 2,}).format(this.totalGastos);
                                                    }


                                                    this.valor = 0;
                                                    this.cargarTiposDeGasto();
                                                    this.diferenciaRed();

                                                }else{

                                                    Swal.fire(
                                                        'Detalle repetido.',
                                                        'Debe tener el rubro o alguno de sus clasificadores diferentes a los detalles que se han agregado.',
                                                        'warning'
                                                    );

                                                }

                                            }
                                        }else{
                                            this.loading =  false;
                                            Swal.fire(
                                                'No hay saldo suficiente.',
                                                'El valor a reducir debe ser menor o igual al saldo.',
                                                'warning'
                                            );
                                        }
                                    }else{
                                        Swal.fire(
                                            'Falta informaci&oacute;n.',
                                            'Falta digitar el valor.',
                                            'warning'
                                        );
                                    }
                                }else{
                                    Swal.fire(
                                        'Falta informaci&oacute;n.',
                                        'Falta escoger la fuente.',
                                        'warning'
                                    );
                                }
                            }else{
                                Swal.fire(
                                    'Falta informaci&oacute;n.',
                                    'Falta escoger la vigencia del gasto.',
                                    'warning'
                                );
                            }
                        }else{

                            Swal.fire(
                                'Falta informaci&oacute;n.',
                                'Falta escoger el medio de pago.',
                                'warning'
                            );
                        }
                    }else{
                        Swal.fire(
                            'Falta informaci&oacute;n.',
                            'Falta escoger la secci&oacute;n presupuestal.',
                            'warning'
                        );
                    }
                }else{

                    Swal.fire(
                        'Falta informaci&oacute;n.',
                        'Falta escoger el tipo de cuenta.',
                        'warning'
                    );
                }
            }else{
                Swal.fire(
                    'Adici&oacute;n finalizada.',
                    'La reduci&oacute;n no se puede modificar porque esta finalizada.',
                    'warning'
                );
            }
            this.reCalcularDiferencia();
            this.loading =  false;

        },

        comparaArrays: function(array1, array2){
            array1 = array1.slice(0,-3);
            array2 = array2.slice(0,-3);

            return Array.isArray(array1) &&
                    Array.isArray(array2) &&
                    array1.length === array2.length &&
                    array1.every((val, index) => val === array2[index]);

        },

        quitarValor: function(item){
            if(item[0] == 'I'){
                var totIng = this.quitarPesosYComas(this.totalIngresos);

                var totalIngresosNuevo = 0;
                totalIngresosNuevo = parseFloat(totIng) - parseFloat(item[15]);
                this.totalIngresos = new Intl.NumberFormat("en-US", {style: "currency", currency: "USD",minimumFractionDigits: 0, maximumFractionDigits: 2,}).format(totalIngresosNuevo);

            }else{
                var totGastos = this.quitarPesosYComas(this.totalGastos);

                var totalGastosNuevo = 0;

                totalGastosNuevo = parseFloat(totGastos) - parseFloat(item[16]);

                this.totalGastos = new Intl.NumberFormat("en-US", {style: "currency", currency: "USD",minimumFractionDigits: 0, maximumFractionDigits: 2,}).format(totalGastosNuevo);
            }
        },

        eliminarDetalle: async function(item){
            if(!this.checked){
                await this.quitarValor(item);
                var i = this.detalles.indexOf( item );

                if ( i !== -1 ) {
                    this.detalles.splice( i, 1 );
                }

                this.diferenciaRed();
                this.reCalcularDiferencia();
                /* setTimeout(function(){
                    this.diferencia();
                },2000); */
            }else{
                Swal.fire(
                    'Adici&oacute;n finalizada.',
                    'La reducci&oacute;n no se puede modificar porque esta finalizada.',
                    'warning'
                );
            }

        },

        quitarPesosYComas: function(valor=''){

            var valorACambiar = '';
            valorACambiar = valor.replace(/[$]+/g, '');
            valorACambiar = valorACambiar.replace(/[,]+/g, '');

            /* if(valorACambiar == '[object Undefined]'){
                valorACambiar = 0;
            } */

            return valorACambiar;
        },

        diferenciaRed: function(){

            var totIng = this.quitarPesosYComas(this.totalIngresos);
            var totGastos = this.quitarPesosYComas(this.totalGastos);
            //totIng = totIng.replace(/[$]+/g, '');
            this.diferencia = parseFloat(totIng) - parseFloat(totGastos);
            this.diferencia= new Intl.NumberFormat("en-US", {style: "currency", currency: "USD",minimumFractionDigits: 0, maximumFractionDigits: 2,}).format(this.diferencia);

        },

        sumarIngresos: function(valorSum){

            var totIng = this.quitarPesosYComas(this.totalIngresos);

            var totSumIng = parseFloat(totIng) + parseFloat(valorSum);

            return totSumIng;
        },

        sumarGastos: function(valorSum){

            var totGastos = this.quitarPesosYComas(this.totalGastos);

            var totSumGastos = parseFloat(totGastos) + parseFloat(valorSum);

            return totSumGastos;
        },

        guardar: function(){

            var totDif = this.quitarPesosYComas(this.diferencia);
            if(parseFloat(totDif) == 0){

                if(document.getElementById('fecha').value != '' && this.acto_administrativo != '' && this.detalles.length > 0){
                    Swal.fire({
                        title: 'Esta seguro de guardar?',
                        text: "Guardar la reducci&oacute;n en la base de datos, confirmar campos!",
                        icon: 'question',
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Si, guardar!'
                    }).then((result) => {
                        if (result.isConfirmed) {
                            this.loading =  true;
                            var formData = new FormData();

                            for(let i=0; i <= this.detalles.length-1; i++){
                                const val = Object.values(this.detalles[i]).length;

                                for(let x = 0; x <= val; x++){
                                    formData.append("detalles["+i+"][]", Object.values(this.detalles[i])[x]);
                                }

                            }

                            formData.append("acto_administrativo", this.acto_administrativo);
                            formData.append("vigencia", this.vigencia);
                            formData.append("fecha", document.getElementById('fecha').value);

                            formData.append("finalizar", this.checked);

                            axios.post('presupuesto_ccpet/reduccion/crear/ccp-reduccion-vue.php?action=guardar', formData)
                            .then((response) => {
                                if(response.data.insertaBien){
                                    Swal.fire({
                                        position: 'top-end',
                                        icon: 'success',
                                        title: 'La reduccion se guard&oacute; con Exito',
                                        showConfirmButton: false,
                                        timer: 1500
                                    }).then((response) => {
                                            app.redireccionar();
                                        });
                                }else{

                                    Swal.fire(
                                        'Error!',
                                        'No se pudo guardar.',
                                        'error'
                                    );
                                }

                            }).finally(() => {
                                this.loading =  false;
                            });

                        }
                    });
                }else{
                    Swal.fire(
                        'Falta informaci&oacute;n para guardar la reducci&oacute;n.',
                        'Verifique que todos los campos esten diligenciados.',
                        'warning'
                    );
                }
            }else{
                Swal.fire(
                    'La suma de ingresos y gastos deben ser iguales.',
                    'Verifique que la diferencia entre sumas de ingresos y gastos sea igual cero.',
                    'warning'
                );
            }
        },

        redireccionar: function(){

            location.href ="ccp-reduccion-vue.php";
        },

        puedeFinalizar: function(){

            var valorAcuerdo = this.quitarPesosYComas(this.valor_acuerdo);
            var gastos = this.quitarPesosYComas(this.totalGastos);
            var ingresos = this.quitarPesosYComas(this.totalIngresos);

            if(parseFloat(valorAcuerdo) > 0){
                if(parseFloat(valorAcuerdo) == parseFloat(ingresos) && parseFloat(valorAcuerdo) == parseFloat(gastos)){
                    this.checked = 'checked';
                }else{
                    Swal.fire(
                        'No se puede finalizar acto administrativo.',
                        'El valor del acto administrativo y el total de ingresos y gastos debe ser igual.',
                        'warning'
                    );
                    this.checked = '';
                }
            }else{
                Swal.fire(
                    'No se puede finalizar acto administrativo.',
                    'El valor del acto administrativo debe ser mayor a 0.',
                    'warning'
                );
                this.checked = '';
            }

        },

        cambiaParametro: function(){
            this.cuenta = '';
            this.nombreCuenta = '';
            this.fuente = '';
            this.nombreFuente = '';
            this.valor = '';
            this.programatico = '';
            this.nombreProgramatico = '';
            this.codProyecto = '';
            this.nombreProyecto = '';
            this.saldo = 0;
            if(this.tiposDeGasto.length > 0){
                if(this.tiposDeGasto[this.tipoGasto-1][2] == 2.3){
                    this.mostrarInversion = true;
                    this.mostrarFuncionamiento = false;
                }else{
                    this.mostrarFuncionamiento = true;
                    this.mostrarInversion = false;
                }
            }else{
                this.mostrarFuncionamiento = true;
                this.mostrarInversion = false;
            }
        },

        ventanaCuenta: function(){

            if(this.tipo_cuenta != '-1'){
                this.loading =  true;
                if(this.tipo_cuenta == 'I'){

                    axios.post('presupuesto_ccpet/reduccion/crear/ccp-reduccion-vue.php?action=cargarCuentasIng')
                        .then((response) => {
                            app.cuentasCcpet = response.data.cuentasCcpet;
                        }).finally(() => {
                            this.loading =  false;
                            app.showModal_cuentas = true;
                        });
                }else{

                    axios.post('presupuesto_ccpet/reduccion/crear/ccp-reduccion-vue.php?action=cargarCuentas&inicioCuenta=' + this.tiposDeGasto[this.tipoGasto-1][2])
                        .then((response) => {
                            app.cuentasCcpet = response.data.cuentasCcpet;
                        }).finally(() => {
                            this.loading =  false;
                            app.showModal_cuentas = true;
                        });
                }
            }else{
                Swal.fire(
                    'Falta escoger el tipo de cuenta.',
                    'Escoger el tipo de cuenta, ingresos o egresos.',
                    'warning'
                    );
            }


        },

        buscarCta: async function(){
            if(this.cuenta != ''){
                if(this.tipo_cuenta != '-1'){
                    if(this.tipo_cuenta == 'I'){
                        await axios.post('presupuesto_ccpet/reduccion/crear/ccp-reduccion-vue.php?action=buscarCuentaIngreso&cuenta=' + this.cuenta)
                        .then((response) => {
                            if(response.data.resultBusquedaCuenta.length > 0){
                                if(response.data.resultBusquedaCuenta[0][1] == 'C'){
                                    app.nombreCuenta = response.data.resultBusquedaCuenta[0][0];

                                }else{
                                    Swal.fire(
                                    'Tipo de cuenta incorrecto.',
                                    'Escoger una cuenta de captura (C)',
                                    'warning'
                                    ).then((result) => {
                                        app.nombreCuenta = '';
                                        app.cuenta = '';

                                    });
                                }

                            }else{
                                Swal.fire(
                                    'Cuenta incorrecta.',
                                    'Esta cuenta no existe en el catalogo CCPET.',
                                    'warning'
                                    ).then((result) => {
                                        app.nombreCuenta = '';
                                        app.cuenta = '';
                                    });
                            }

                        });
                    }else{
                        await axios.post('vue/presupuesto_ccp/ccp-cdpbasico.php?action=buscarCuenta&cuenta=' + this.cuenta)
                        .then((response) => {
                            if(response.data.resultBusquedaCuenta.length > 0){
                                if(response.data.resultBusquedaCuenta[0][1] == 'C'){
                                    app.nombreCuenta = response.data.resultBusquedaCuenta[0][0];

                                }else{
                                    Swal.fire(
                                    'Tipo de cuenta incorrecto.',
                                    'Escoger una cuenta de captura (C)',
                                    'warning'
                                    ).then((result) => {
                                        app.nombreCuenta = '';
                                        app.cuenta = '';

                                    });
                                }

                            }else{
                                Swal.fire(
                                    'Cuenta incorrecta.',
                                    'Esta cuenta no existe en el catalogo CCPET.',
                                    'warning'
                                    ).then((result) => {
                                        app.nombreCuenta = '';
                                        app.cuenta = '';
                                    });
                            }

                        });
                    }
                }else{
                    Swal.fire(
                        'Falta escoger el tipo de cuenta.',
                        'Escoger el tipo de cuenta, ingresos o egresos.',
                        'warning'
                        ).then((result) => {
                            app.nombreCuenta = '';
                            app.cuenta = '';
                        });
                }
            }else{
                this.nombreCuenta = '';
            }
            this.parametrosCompletos();
        },

        seleccionarCuenta: function(cuentaSelec){
            if(cuentaSelec[6] == 'C'){

                this.cuenta = cuentaSelec [1];
                this.nombreCuenta = cuentaSelec [2];
                this.showModal_cuentas = false;

            }else{

                this.showModal_cuentas = false;
                Swal.fire(
                'Tipo de cuenta incorrecto.',
                'Escoger una cuenta de captura (C)',
                'warning'
                ).then((result) => {

                    if(result.isConfirmed || result.isDismissed){
                        this.showModal_cuentas = true;
                    }

                });

            }
            this.parametrosCompletos();
        },

        searchMonitorCuenta: function(){
            var keywordCuenta = app.toFormData(app.searchCuenta);
            if(this.tipo_cuenta == 'I'){
                axios.post('presupuesto_ccpet/reduccion/crear/ccp-reduccion-vue.php?action=filtrarCuentasIngreso', keywordCuenta)
                .then((response) => {
                    app.cuentasCcpet = response.data.cuentasCcpet;

                });
            }else{
                axios.post('presupuesto_ccpet/reduccion/crear/ccp-reduccion-vue.php?action=filtrarCuentas&inicioCuenta=' + this.tiposDeGasto[this.tipoGasto-1][2], keywordCuenta)
                .then((response) => {
                    app.cuentasCcpet = response.data.cuentasCcpet;

                });
            }
        },

        ventanaFuente: function(){
            if(this.tipoGasto != '3'){
                if(this.cuenta != ''){
                    app.showModal_fuentes = true;
                    axios.post('presupuesto_ccpet/reduccion/crear/ccp-reduccion-vue.php?action=cargarFuentes')
                        .then((response) => {
                            app.fuentesCuipo = response.data.fuentes;

                        });
                }else{
                    Swal.fire(
                        'Falta escoger la cuenta presupuestal CCPET.',
                        'Escoger la cuenta antes de buscar la fuente, para acotar la busqueda.',
                        'warning'
                    )
                }

            }else{
                if(this.codProyecto != '' && this.programatico != ''){
                    app.showModal_fuentes = true;

                    axios.post('presupuesto_ccpet/reduccion/crear/ccp-reduccion-vue.php?action=cargarFuentes')
                        .then((response) => {
                            app.fuentesCuipo = response.data.fuentes;

                        });
                }else{
                    Swal.fire(
                        'Falta escoger el proyecto o indicador programatico.',
                        'Escoger el proyecto o indicador programatico antes de buscar la fuente, para acotar la busqueda.',
                        'warning'
                    )
                }
            }



        },

        buscarFuente: async function(){

            if(this.fuente != ''){
                await axios.post('vue/presupuesto_ccp/ccp-cdpbasico.php?action=buscarFuente&fuente=' + this.fuente)
                .then((response) => {
                    if(response.data.resultBusquedaFuente.length > 0){

                        app.nombreFuente = response.data.resultBusquedaFuente[0][0];

                    }else{
                        Swal.fire(
                            'Fuente incorrecta.',
                            'Esta Fuente no existe en las fuentes CUIPO del sistema.',
                            'warning'
                            ).then((result) => {
                                app.nombreFuente = '';
                                app.fuente = '';
                            });
                    }

                });
            }else{
                this.nombreFuente = '';
                this.fuente = '';
            }

            this.parametrosCompletos();
        },

        searchMonitorFuente: function(){
            var keywordFuente = app.toFormData(app.searchFuente);
            axios.post('vue/presupuesto_ccp/ccp-cdpbasico.php?action=filtrarFuentes', keywordFuente)
            .then((response) => {
                app.fuentesCuipo = response.data.fuentes;

            });
        },

        seleccionarFuente: function(fuenteSelec){
            this.fuente = fuenteSelec [0];
            this.nombreFuente = fuenteSelec [1];
            this.showModal_fuentes = false;

            this.parametrosCompletos();
        },

        ventanaProyecto: function(){
            app.showModal_proyectos = true;
            axios.post('vue/presupuesto_ccp/ccp-cdpbasico.php?action=cargarProyecto&vigencia='+this.vigencia)
                .then((response) => {

                    app.proyectos = response.data.proyectosCcpet;
                });
        },

        seleccionarProyecto: function(proyectoSelec){

            this.codProyecto = proyectoSelec [2];
            this.nombreProyecto = proyectoSelec [4];
            this.showModal_proyectos = false;

            this.parametrosCompletos();
        },

        buscarProyecto: async function(){
            if(this.codProyecto != ''){
                await axios.post('vue/presupuesto_ccp/ccp-cdpbasico.php?action=buscarProyecto&proyecto=' + this.codProyecto)
                .then((response) => {
                    if(response.data.resultBusquedaProyecto.length > 0){
                            app.nombreProyecto = response.data.resultBusquedaProyecto[0][0];
                    }else{
                        Swal.fire(
                            'Proyecto incorrecto.',
                            'Este proyecto no existe.',
                            'warning'
                            ).then((result) => {
                                app.nombreProyecto = '';
                            });
                    }

                });

            }else{
                this.nombreProyecto = '';
            }

            this.parametrosCompletos();
        },

        searchMonitorProyecto: function(){
            var keywordProyecto = app.toFormData(app.searchProyecto);
            axios.post('vue/presupuesto_ccp/ccp-cdpbasico.php?action=filtrarProyectos&vigencia=' + this.vigencia, keywordProyecto)
            .then((response) => {
                app.proyectos = response.data.proyectosCcpet;
            });
        },

        ventanaProgramatico: function(){
            if(this.codProyecto != ''){
                app.showModal_programatico = true;
                axios.post('presupuesto_ccpet/reduccion/crear/ccp-reduccion-vue.php?action=cargarProgramaticos&proyecto=' + this.codProyecto)
                .then((response) => {
                    app.programaticos = response.data.programaticos;
                });
            }else{
                Swal.fire(
                    'Falta escoger el proyecto.',
                    'Antes de seleccionar el program&aacute;tico se debe seleccionar un proyecto.',
                    'warning'
                    ).then((result) => {
                        app.programaticos = [];
                    });
            }


        },

        buscarProgramatico: async function(bandera = false){
            if(this.programatico != ''){
                if(this.codProyecto != '' || bandera){
                    await axios.post('vue/presupuesto_ccp/ccp-cdpbasico.php?action=buscarProgramatico&programatico=' + this.programatico)
                    .then((response) => {
                        if(response.data.resultBusquedaProgramatico.length > 0){
                                app.nombreProgramatico = response.data.resultBusquedaProgramatico[0][0];
                                this.changeProgramatico();
                        }else{
                            Swal.fire(
                                'Proyecto incorrecto.',
                                'Este proyecto no existe.',
                                'warning'
                                ).then((result) => {
                                    app.nombreProgramatico = '';
                                    app.programatico = '';
                                });
                        }

                    });
                }else{
                    Swal.fire(
                        'Falta escoger un proyecto.',
                        '',
                        'info'
                        );
                }


            }else{
                this.nombreProgramatico = '';
            }

            this.parametrosCompletos();
        },

        parametrosCompletos: function(){
            if(this.tipo_cuenta == 'G'){
                if(this.tipoGasto == 3){

                    if(this.codProyecto != '' &&this.programatico != ''  && this.fuente != '' && this.vigencia != '' && this.tipoGasto != '' && this.selectUnidadEjecutora != '' && this.selectMedioPago != '' && this.selectVigenciaGasto != ''){
                        this.calcularSaldoInversion();
                    }else{
                        this.saldo = 0;
                    }
                }else{

                    if(this.cuenta != '' && this.fuente != '' && this.vigencia != '' && this.tipoGasto != '' && this.selectUnidadEjecutora != '' && this.selectMedioPago != '' && this.selectVigenciaGasto != ''){
                        this.calcularSaldo();
                    }else{
                        this.saldo = 0;
                    }
                }
            }else{
                if(this.cuenta != '' && this.fuente != '' && this.vigencia != '' && this.selectUnidadEjecutora != '' && this.selectMedioPago != '' && this.selectVigenciaGasto != ''){
                    this.calcularSaldoIngreso();
                }else{
                    this.saldo = 0;
                }
            }


        },


        //Calcular el saldo de la cuenta
        calcularSaldo: async function(){

            var formData = new FormData();
            //guardar cabecera
            //[rubro, fuente, vigencia, tipo_gasto, seccion_presupuestal, medio_pago, vigencia_gasto]

            formData.append("rubro",this.cuenta);
            formData.append("fuente", this.fuente);
            formData.append("vigencia", this.vigencia);
            formData.append("tipo_gasto", this.tipoGasto);
            formData.append("seccion_presupuestal", this.selectUnidadEjecutora);
            formData.append("medio_pago", this.selectMedioPago);
            formData.append("vigencia_gasto", this.selectVigenciaGasto);
            await axios.post('presupuesto_ccpet/traslado/crear/ccp-traslados-vue.php?action=saldoPorCuenta', formData)
                 .then((response) => {
                    app.saldo = response.data.saldoPorCuenta;
                 });
            this.calcularSaldoConDetalles();
        },

        calcularSaldoConDetalles: function(){

            this.detalles.forEach(function(elemento){

                if(app.cuenta == elemento[10] && app.fuente == elemento[12] && app.selectUnidadEjecutora == elemento[1] && app.selectMedioPago == elemento[3] && app.selectVigenciaGasto == elemento[4]){
                    app.saldo -= elemento[18];
                }
            });

        },

        //Calcular el saldo de la cuenta ingresos
        calcularSaldoIngreso: async function(){

            var formData = new FormData();
            //guardar cabecera
            //[rubro, fuente, vigencia, tipo_gasto, seccion_presupuestal, medio_pago, vigencia_gasto]

            formData.append("rubro",this.cuenta);
            formData.append("fuente", this.fuente);
            formData.append("vigencia", this.vigencia);
            formData.append("seccion_presupuestal", this.selectUnidadEjecutora);
            formData.append("medio_pago", this.selectMedioPago);
            formData.append("vigencia_gasto", this.selectVigenciaGasto);
            await axios.post('presupuesto_ccpet/reduccion/crear/ccp-reduccion-vue.php?action=saldoPorCuentaIng', formData)
                 .then((response) => {
                    app.saldo = response.data.saldoPorCuentaIng;
                 });
        },


        calcularSaldoInversion: async function(){

            var formData = new FormData();
            //guardar cabecera
            //[rubro, fuente, vigencia, tipo_gasto, seccion_presupuestal, medio_pago, vigencia_gasto]

            formData.append("codProyecto",this.codProyecto);
            formData.append("programatico",this.programatico);
            formData.append("fuente", this.fuente);
            formData.append("vigencia", this.vigencia);
            formData.append("tipo_gasto", this.tipoGasto);
            formData.append("seccion_presupuestal", this.selectUnidadEjecutora);
            formData.append("medio_pago", this.selectMedioPago);
            formData.append("vigencia_gasto", this.selectVigenciaGasto);
            await axios.post('presupuesto_ccpet/traslado/crear/ccp-traslados-vue.php?action=saldoPorCuenta', formData)
                 .then((response) => {
                    app.saldo = response.data.saldoPorCuenta;
                 });
            this.calcularSaldoConDetallesInv();
        },

        calcularSaldoConDetallesInv: function(){

            this.detalles.forEach(function(elemento){

                if(app.codProyecto == elemento[6] && app.programatico == elemento[8] && app.fuente == elemento[12] && app.selectUnidadEjecutora == elemento[1] && app.selectMedioPago == elemento[3] && app.selectVigenciaGasto == elemento[4]){
                    app.saldo -= elemento[18];
                }
            });

        },

        seleccionarProgramatico: function(programaticoSelec){

            this.programatico = programaticoSelec [0];
            this.nombreProgramatico = programaticoSelec [1];
            this.showModal_programatico = false;

            this.parametrosCompletos();
            this.changeProgramatico();
        },

        async buscarRubroPresupuestal (){
            let partesRubroPresupuestal = divideRubro(this.rubroPresupuestal);

            const tipoGastos = {
                'funcionamiento': 1,
                'deuda': 2,
                'inversion': 3,
                'comercializacion': 4,
                'ingresos': 5
            }

            let claves = Object.keys(partesRubroPresupuestal);

            if( claves.length == 0 ){
                await this.cambiaTipoCuenta(false);
                return;
            }

            for(let i = 0; i < claves.length; i++){
                let clave = claves[i];

                if(clave == 'tipoObjeto'){
                    if (tipoGastos[partesRubroPresupuestal[clave]] == 5){
                        this.tipo_cuenta = 'I';
                        await this.cambiaTipoCuenta(false);
                    }else{
                        await this.cargarTiposDeGasto();
                        this.tipo_cuenta = 'G';
                        this.tipoGasto = tipoGastos[partesRubroPresupuestal[clave]];

                        await this.cambiaTipoCuenta(false);
                    }


                    await this.cambiaParametro();
                }

                if(clave == 'cuenta'){
                    this.cuenta = partesRubroPresupuestal[clave];
                    this.buscarCta();
                }

                if(clave == 'fuente'){
                    this.fuente = partesRubroPresupuestal[clave];
                    this.buscarFuente();
                }

                if(clave == 'bpim'){
                    this.codProyecto = partesRubroPresupuestal[clave];
                    await this.buscarProyecto();
                }

                if(clave == 'programatico'){
                    this.programatico = partesRubroPresupuestal[clave];
                    this.buscarProgramatico(true);
                    this.changeProgramatico();
                }



                if(clave == 'seccion_presupuestal'){
                    this.selectUnidadEjecutora = partesRubroPresupuestal[clave];
                }

                if(clave == 'vigencia_gasto'){
                    this.selectVigenciaGasto = partesRubroPresupuestal[clave];
                }

                if(clave == 'medio_pago'){
                    this.selectMedioPago = partesRubroPresupuestal[clave];
                }

            }

        },

        toFormData: function(obj){
            var form_data = new FormData();
            for(var key in obj){
                form_data.append(key, obj[key]);
            }
            return form_data;
        },

    }
});
