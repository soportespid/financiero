<?php
    $URL_BBDD = true;
	require_once '../../../comun.inc';
    require '../../../funciones.inc';
    //require '../../../vue/presupuesto_ccp/funcionesccp.inc.php';
    require_once '../../../vue/presupuesto_ccp/funcionesccp.inc.php';
    session_start();

	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");


    $out = array('error' => false);

    $maxVersion = ultimaVersionGastosCCPET();
    $maxVersionIngresos = ultimaVersionIngresosCCPET();

    function selectOrden(){
        $linkbd = conectar_v7();
	    $linkbd -> set_charset("utf8");
        $sql="SELECT orden FROM ccpet_parametros";
        $request = mysqli_query($linkbd,$sql)->fetch_assoc()['orden'];
        return $request;
    }
    function selectProgramas(){
        $linkbd = conectar_v7();
	    $linkbd -> set_charset("utf8");
        $sql = "SELECT codigo, nombre FROM ccpetprogramas";
        $request = mysqli_fetch_all(mysqli_query($linkbd,$sql),MYSQLI_ASSOC);
        return $request;
    }
    function selectSectores(){
        $linkbd = conectar_v7();
	    $linkbd -> set_charset("utf8");
        $sql = "SELECT codigo, nombre FROM ccpetsectores";
        $request = mysqli_fetch_all(mysqli_query($linkbd,$sql),MYSQLI_ASSOC);
        return $request;
    }

    $action = "show";

    if(isset($_GET['action'])){
        $action = $_GET['action'];
    }
    if(isset($_POST['action']) && $_POST['action'] == "get"){
        $arrData = array(
            "sectores"=>selectSectores(),
            "programas"=>selectProgramas(),
            "orden"=>selectOrden()
        );
        $out['data'] = $arrData;
    }
    if($action == 'cargarDet'){

        $detalles = array();
        $totalIng = 0;
        $totalGastos = 0;
        $sqlr = "SELECT tipo_cuenta, tipo_gasto, seccion_presupuestal, medio_pago, codigo_vigenciag, bpim, programatico, cuenta, fuente, valor  FROM ccpet_reducciones WHERE  id_acuerdo = '".$_GET['idAcuerdo']."'";
        $res = mysqli_query($linkbd, $sqlr);
        while($row = mysqli_fetch_row($res)){
            $detallesAgregar = array();
            array_push($detallesAgregar, $row[0]);
            array_push($detallesAgregar, $row[1]);
            array_push($detallesAgregar, $row[2]);

            $sqlrSec = "SELECT nombre FROM pptoseccion_presupuestal WHERE  id_seccion_presupuestal = '$row[2]'";
            $resSec = mysqli_query($linkbd, $sqlrSec);
            $rowSec = mysqli_fetch_row($resSec);

            array_push($detallesAgregar, $rowSec[0]);
            array_push($detallesAgregar, $row[3]);
            array_push($detallesAgregar, $row[4]);

            $sqlrVig = "SELECT nombre FROM ccpet_vigenciadelgasto WHERE  codigo = '$row[4]'";
            $resVig = mysqli_query($linkbd, $sqlrVig);
            $rowVig = mysqli_fetch_row($resVig);

            array_push($detallesAgregar, $rowVig[0]);

            array_push($detallesAgregar, $row[5]);

            $sqlrProyecto = "SELECT nombre FROM ccpproyectospresupuesto WHERE  codigo = '$row[5]'";
            $resProyecto = mysqli_query($linkbd, $sqlrProyecto);
            $rowProyecto = mysqli_fetch_row($resProyecto);

            array_push($detallesAgregar, $rowProyecto[0]);

            array_push($detallesAgregar, $row[6]);

            $sqlrProgramatico = "SELECT indicador_producto FROM ccpetproductos WHERE codigo_indicador = '$row[6]'";
            $resProgramatico = mysqli_query($linkbd, $sqlrProgramatico);
            $rowProgramatico = mysqli_fetch_row($resProgramatico);

            array_push($detallesAgregar, $rowProgramatico[0]);

            array_push($detallesAgregar, $row[7]);

            if($row[0] == 'I'){

                $sqlrIng = "SELECT nombre, tipo FROM cuentasingresosccpet WHERE version = $maxVersionIngresos AND codigo = '$row[7]' ORDER BY id ASC";
                $resIng = mysqli_query($linkbd, $sqlrIng);//echo $sqlr;
                $rowIng = mysqli_fetch_row($resIng);

                array_push($detallesAgregar, $rowIng[0]);

            }else{

                $sqlrGastos = "SELECT nombre, tipo FROM cuentasccpet WHERE version = $maxVersion AND codigo = '$row[7]' ORDER BY id ASC";
                $resGastos = mysqli_query($linkbd, $sqlrGastos);
                $rowGastos = mysqli_fetch_row($resGastos);
                array_push($detallesAgregar, $rowGastos[0]);

            }

            array_push($detallesAgregar, $row[8]);

            $sqlrMaxFuente = "SELECT MAX(version) FROM ccpet_fuentes_cuipo";
            $resMaxFuente = mysqli_query($linkbd, $sqlrMaxFuente);
            $rowMaxFuente = mysqli_fetch_row($resMaxFuente);
            //echo $_GET['programatico'];

            $sqlFuente = "SELECT ccp_f.nombre FROM ccpet_fuentes_cuipo AS ccp_f WHERE ccp_f.version = '$rowMaxFuente[0]' AND ccp_f.codigo_fuente='$row[8]' AND  LENGTH(ccp_f.codigo_fuente) > 6";
            $resFuente = mysqli_query($linkbd, $sqlFuente);
            $rowFuente = mysqli_fetch_row($resFuente);

            array_push($detallesAgregar, $rowFuente[0]);

            if($row[0] == 'I'){
                array_push($detallesAgregar, $row[9]);
                array_push($detallesAgregar, 0);
                $totalIng+=$row[9];

            }else{
                array_push($detallesAgregar, 0);
                array_push($detallesAgregar, $row[9]);
                $totalGastos+=$row[9];
            }

            array_push($detalles, $detallesAgregar);

        }

        $out['detalles'] = $detalles;
        $out['totalIng'] = $totalIng;
        $out['totalGastos'] = $totalGastos;
    }

    if($action == 'cargarCuentas'){

        $cuentasCcpet = array();
        $inicioCuenta = $_GET['inicioCuenta'];
        $sqlr = "SELECT * FROM cuentasccpet WHERE version = $maxVersion AND codigo LIKE '$inicioCuenta%' ORDER BY id ASC";
        $res = mysqli_query($linkbd, $sqlr);
        while($row = mysqli_fetch_row($res)){
            array_push($cuentasCcpet, $row);
        }

        $out['cuentasCcpet'] = $cuentasCcpet;

    }

    if($action == 'filtrarCuentas'){

        $keywordCuenta=$_POST['keywordCuenta'];

        $cuentasCcpet = array();
        $inicioCuenta = $_GET['inicioCuenta'];

        $sqlr = "SELECT * FROM cuentasccpet WHERE version = $maxVersion AND codigo LIKE '$inicioCuenta%' AND concat_ws(' ', codigo, nombre) LIKE '%$keywordCuenta%' ORDER BY id ASC";//echo $sqlr;
        $res = mysqli_query($linkbd, $sqlr);
        while($row = mysqli_fetch_row($res)){
            array_push($cuentasCcpet, $row);
        }

        $out['cuentasCcpet'] = $cuentasCcpet;
    }

    if($action == 'cargarCuentasIng'){

        $cuentasCcpet = array();
        $sqlr = "SELECT * FROM cuentasingresosccpet WHERE version = $maxVersionIngresos ORDER BY id ASC";
        $res = mysqli_query($linkbd, $sqlr);
        while($row = mysqli_fetch_row($res)){
            array_push($cuentasCcpet, $row);
        }

        $out['cuentasCcpet'] = $cuentasCcpet;

    }

    if($action == 'actosAdm'){
        $actosAdm = array();
        $sqlr = "SELECT * FROM ccpetacuerdos WHERE estado='S' AND vigencia='".$_GET['vig']."' AND tipo<>'I' AND valorreduccion > 0";
        $res = mysqli_query($linkbd, $sqlr);
        while($row = mysqli_fetch_row($res)){
            array_push($actosAdm, $row);
        }

        $out['actosAdm'] = $actosAdm;
    }



    if($action == 'filtrarCuentasIngreso'){

        $keywordCuenta=$_POST['keywordCuenta'];

        $cuentasCcpet = array();

        $sqlr = "SELECT * FROM cuentasingresosccpet WHERE version = $maxVersionIngresos AND codigo LIKE '$inicioCuenta%' AND concat_ws(' ', codigo, nombre) LIKE '%$keywordCuenta%' ORDER BY id ASC";//echo $sqlr;
        $res = mysqli_query($linkbd, $sqlr);
        while($row = mysqli_fetch_row($res)){
            array_push($cuentasCcpet, $row);
        }

        $out['cuentasCcpet'] = $cuentasCcpet;
    }

    if($action == 'filtrarCuentas'){

        $keywordCuenta=$_POST['keywordCuenta'];

        $cuentasCcpet = array();
        $inicioCuenta = $_GET['inicioCuenta'];

        $sqlr = "SELECT * FROM cuentasccpet WHERE version = $maxVersion AND codigo LIKE '$inicioCuenta%' AND concat_ws(' ', codigo, nombre) LIKE '%$keywordCuenta%' ORDER BY id ASC";//echo $sqlr;
        $res = mysqli_query($linkbd, $sqlr);
        while($row = mysqli_fetch_row($res)){
            array_push($cuentasCcpet, $row);
        }

        $out['cuentasCcpet'] = $cuentasCcpet;
    }

    if($action == 'buscarCuentaIngreso'){
        $cuenta = $_GET['cuenta'];
        $resultBusquedaCuenta = array();

        $sqlr = "SELECT nombre, tipo FROM cuentasingresosccpet WHERE version = $maxVersionIngresos AND codigo = '$cuenta' ORDER BY id ASC";
        $res = mysqli_query($linkbd, $sqlr);//echo $sqlr;
        while($row = mysqli_fetch_row($res)){
            array_push($resultBusquedaCuenta, $row);
        }

        $out['resultBusquedaCuenta'] = $resultBusquedaCuenta;

    }

    if($action == 'buscarCuenta'){
        $cuenta = $_GET['cuenta'];
        $resultBusquedaCuenta = array();

        $sqlr = "SELECT nombre, tipo FROM cuentasccpet WHERE version = $maxVersion AND codigo = '$cuenta' ORDER BY id ASC";
        $res = mysqli_query($linkbd, $sqlr);//echo $sqlr;
        while($row = mysqli_fetch_row($res)){
            array_push($resultBusquedaCuenta, $row);
        }

        $out['resultBusquedaCuenta'] = $resultBusquedaCuenta;

    }



    if($action == 'cargarFuentes'){

        $fuentes = array();
        $sqlrMaxFuente = "SELECT MAX(version) FROM ccpet_fuentes_cuipo";
        $resMaxFuente = mysqli_query($linkbd, $sqlrMaxFuente);
        $rowMaxFuente = mysqli_fetch_row($resMaxFuente);
        //echo $_GET['programatico'];

        $sql = "SELECT ccp_f.codigo_fuente, ccp_f.nombre FROM ccpet_fuentes_cuipo AS ccp_f WHERE ccp_f.version = '$rowMaxFuente[0]' AND  LENGTH(ccp_f.codigo_fuente) > 6";
        $res = mysqli_query($linkbd, $sql);

        while($row = mysqli_fetch_row($res))
        {
            array_push($fuentes, $row);
        }

        $out['fuentes'] = $fuentes;

    }



    if($action == 'cargarProgramaticos'){

        $cuentasCcpet = array();

        $programaticos = array();
        $proyecto = $_GET['proyecto'];
        $sqlr = "SELECT id FROM ccpproyectospresupuesto WHERE codigo = '$proyecto'";
        $res = mysqli_query($linkbd, $sqlr);

        while($row = mysqli_fetch_row($res)){
            $sqlrP = "SELECT indicador FROM ccpproyectospresupuesto_productos WHERE codproyecto = '$row[0]' GROUP BY indicador";
            $resP = mysqli_query($linkbd, $sqlrP);
            $programatico = array();
            while($rowP = mysqli_fetch_row($resP)){
                $programatico = array();
                $sqlrNom = "SELECT indicador_producto FROM ccpetproductos WHERE codigo_indicador = '$rowP[0]'";
                $resNom = mysqli_query($linkbd, $sqlrNom);
                $rowNom = mysqli_fetch_row($resNom);
                array_push($programatico, $rowP[0]);
                array_push($programatico, $rowNom[0]);

                array_push($programaticos, $programatico);

            }

        }


        $out['programaticos'] = $programaticos;
    }

    if($action == 'guardar'){

        preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST["fecha"], $fecha);
		$fechaf = $fecha[3]."-".$fecha[2]."-".$fecha[1];

        $sqlr = "DELETE FROM ccpet_adiciones WHERE id_acuerdo = '".$_POST['acto_administrativo']."'";
        mysqli_query($linkbd, $sqlr);

        for($x = 0; $x < count($_POST["detalles"]); $x++){

            $vigencia = '';
            $id_acuerdo = '';
            $tipo_cuenta = '';
            $tipo_gasto = '';
            $seccion_presupuestal = '';
            $medio_pago = '';
            $codigo_vigenciag = '';
            $bpim = '';
            $programatico = '';
            $cuenta = '';
            $fuente = '';
            $valor = '';
            $estado = '';


            $vigencia = $_POST["vigencia"];
            $id_acuerdo = $_POST["acto_administrativo"];
            $tipo_cuenta = $_POST["detalles"][$x][0];
            $tipo_gasto = $_POST["detalles"][$x][1];
            $seccion_presupuestal = $_POST["detalles"][$x][2];
            $medio_pago = $_POST["detalles"][$x][4];
            $codigo_vigenciag = $_POST["detalles"][$x][5];
            $bpim = $_POST["detalles"][$x][7];
            $programatico = $_POST["detalles"][$x][9];
            $cuenta = $_POST["detalles"][$x][11];
            $fuente = $_POST["detalles"][$x][13];
            if($tipo_cuenta == 'I'){
                $valor = $_POST["detalles"][$x][15];
            }else{
                $valor = $_POST["detalles"][$x][16];
            }

            $estado = 'S';

            $sqlrD = "INSERT INTO ccpet_reducciones(vigencia, id_acuerdo, fecha, tipo_cuenta, tipo_gasto, seccion_presupuestal, medio_pago, codigo_vigenciag, bpim, programatico, cuenta, fuente, valor, estado) VALUES ('".$vigencia."', '".$id_acuerdo."', '".$fechaf."', '".$tipo_cuenta."', '".$tipo_gasto."', '".$seccion_presupuestal."', '".$medio_pago."', '$codigo_vigenciag', '".$bpim."', '".$programatico."', '".$cuenta."', '".$fuente."', '".$valor."', '".$estado."')";
            mysqli_query($linkbd, $sqlrD);




        }
        //echo $_POST["finalizar"]."hola";
        if($_POST["finalizar"]){
            $sqlr = "UPDATE ccpetacuerdos SET estado = 'F' WHERE id_acuerdo = '$_POST[acto_administrativo]'";
            mysqli_query($linkbd, $sqlr);
        }

        $out['insertaBien'] = true;

    }

    if($action == 'buscarCuentaIngreso'){
        $cuenta = $_GET['cuenta'];
        $resultBusquedaCuenta = array();

        $sqlr = "SELECT nombre, tipo FROM cuentasingresosccpet WHERE version = $maxVersionIngresos AND codigo = '$cuenta' ORDER BY id ASC";
        $res = mysqli_query($linkbd, $sqlr);//echo $sqlr;
        while($row = mysqli_fetch_row($res)){
            array_push($resultBusquedaCuenta, $row);
        }

        $out['resultBusquedaCuenta'] = $resultBusquedaCuenta;

    }

    //[rubro, fuente, vigencia, tipo_gasto, seccion_presupuestal, medio_pago, vigencia_gasto]
    if($action == 'saldoPorCuentaIng'){

        $parametros = [
            'rubro' => $_POST['rubro'],
            'fuente' => $_POST['fuente'],
            'vigencia' => $_POST['vigencia'],
            'seccion_presupuestal' => $_POST['seccion_presupuestal'],
            'medio_pago' => $_POST['medio_pago'],
            'vigencia_gasto' => $_POST['vigencia_gasto'],

            'codProyecto' => $_POST['codProyecto'],
            'programatico' => $_POST['programatico'],
        ];

        $saldoPorCuentaIng = 0;

        $saldoPorCuentaIng = saldoPorRubroIngreso($parametros);

        $out['saldoPorCuentaIng'] = round($saldoPorCuentaIng, 2);
    }

    header("Content-type: application/json");
    echo json_encode($out);
    die();
