<?php
	require_once '../../comun.inc';
    require '../../funciones.inc';
    require '../../validaciones.inc';
    require '../../vue/presupuesto_ccp/funcionesccp.inc.php';
    session_start();

	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");


    $out = array('error' => false);

    $maxVersion = ultimaVersionGastosCCPET();

    $action = "show";

    if(isset($_GET['action'])){
        $action = $_GET['action'];
    }

    if($action == 'show'){

        $tiposDeMovimiento = array();

        $user = $_SESSION['cedulausu'];
        $sql = "SELECT * FROM permisos_movimientos WHERE usuario='$user' AND estado='T' ";
        $res = mysqli_query($linkbd, $sql);
        $num = mysqli_num_rows($res);
        if($num == 1){
            $sqlr = "SELECT id, codigo, descripcion FROM tipo_movdocumentos WHERE estado='S' AND modulo=3 AND (id='2' OR id='4')";
            $resp = mysqli_query($linkbd, $sqlr);
            while ($row = mysqli_fetch_row($resp))
            {
                $unionMovimiento = '';
                $codigoMovimiento = array();

                $unionMovimiento = $row[0]."".$row[1];

                array_push($codigoMovimiento, $unionMovimiento);
                array_push($codigoMovimiento, $row[2]);

                array_push($tiposDeMovimiento, $codigoMovimiento);
            }
        }else{
            $sql = "SELECT codmov,tipomov FROM permisos_movimientos WHERE usuario='$user' AND estado='S' AND modulo='3' AND transaccion='PGA' ";
            $res = mysqli_query($linkbd, $sql);
            while($row = mysqli_fetch_row($res)){
                array_push($tiposDeMovimiento, $row);
            }
        }

        $out['tiposDeMovimiento'] = $tiposDeMovimiento;
    }

    if($action == 'solicitudesCdp'){

        $solicitudesCdp = [];
        $sqlr = "SELECT * FROM plan_solicitud_cdp WHERE estado = 'S'";
        $res = mysqli_query($linkbd, $sqlr);
        while($row = mysqli_fetch_row($res)){
            array_push($solicitudesCdp, $row);
        }

        $sql = "SELECT MAX(fecha) AS fecha FROM ccpetcdp WHERE estado = 'S'";
        $res = mysqli_query($linkbd, $sql);
        $row = mysqli_fetch_row($res);
        $out['fechaMax'] = $row[0];
        

        $out['solicitudesCdp'] = $solicitudesCdp;

        $areas = [];
        $sqlr = "SELECT codarea AS codigo, nombrearea AS nombre FROM planacareas WHERE estado = 'S' ORDER BY codarea";
        $res = mysqli_query($linkbd, $sqlr);
        while($row = mysqli_fetch_row($res)){
            array_push($areas, $row);
        }

        $out['areas'] = $areas;
    }

    if($action == 'seleccionarSolicitud'){

        $detalles = [];

        $tiposDegasto = [
            '1' => 'Funcionamiento',
            '2' => 'Servicios de la deuda',
            '3' => 'Inversion',
        ];

        $idSolicitud = $_GET['solicitudId'];


        $sqlrDep = "SELECT id_seccion_presupuestal, nombre FROM pptoseccion_presupuestal WHERE estado = 'S'";
        $resDep = mysqli_query($linkbd, $sqlrDep);
        while($rowDep = mysqli_fetch_row($resDep)){
            $dependencias[$rowDep[0]] = $rowDep[1];
        }

        $sqlrVig = "SELECT codigo, nombre FROM ccpet_vigenciadelgasto";
        $resVig = mysqli_query($linkbd, $sqlrVig);
        while($rowVig = mysqli_fetch_row($resVig)){
            $vigencias[$rowVig[0]] = $rowVig[1];
        }

        
        
        $sqlr = "SELECT * FROM plan_solicitud_cdp_det WHERE id_solicitud_cdp = '$idSolicitud'";
        $res = mysqli_query($linkbd, $sqlr);
        while($row = mysqli_fetch_row($res)){

            $det = [];

            $tipoGasto = $tiposDegasto[$row[2]];
            $dependencia = $row[3];
            $nombreDependencia = $dependencias[$dependencia];
            $medio_pago = $row[4];
            $vigencia_gasto = $row[5];
            $nombreVigencia = $vigencias[$vigencia_gasto];

            $idBpim = $row[10];

            $sqlrBpim = "SELECT codigo, nombre FROM ccpproyectospresupuesto WHERE id = '$idBpim'";
            $resBpim = mysqli_query($linkbd, $sqlrBpim);
            $rowBpim = mysqli_fetch_row($resBpim);

            $proyecto = $rowBpim[0];
            $nombreProyecto = $rowBpim[1];

            $indicador = $row[9];

            $sqlrInd = "SELECT indicador_producto FROM ccpetproductos WHERE codigo_indicador = '$indicador'";
            $resInd = mysqli_query($linkbd, $sqlrInd);
            $rowInd = mysqli_fetch_row($resInd);

            $indicadorProducto = $rowInd[0];

            $cuentaCcpet = $row[6];
            $sqlrCcpet = "SELECT nombre FROM cuentasccpet WHERE codigo = '$cuentaCcpet' AND version = $maxVersion";
            $resCcpet = mysqli_query($linkbd, $sqlrCcpet);
            $rowCcpet = mysqli_fetch_row($resCcpet);

            $nombreCuenta = $rowCcpet[0];

            $fuente = $row[7];

            $sqlrFuente = "SELECT nombre FROM ccpet_fuentes_cuipo WHERE codigo_fuente = '$fuente'";
            $resFuente = mysqli_query($linkbd, $sqlrFuente);
            $rowFuente = mysqli_fetch_row($resFuente);

            $nombreFuente = $rowFuente[0];

            $cpc = $row[8];

            $digitoInicial = substr($cpc, 0, 1);
            $nombreCpc = '';
            if($digitoInicial >= 5){
                $sqlrCpc = "SELECT titulo FROM ccpetservicios WHERE GRUPO = '$cpc'";
                $resCpc = mysqli_query($linkbd, $sqlrCpc);
                $rowCpc = mysqli_fetch_row($resCpc);
                $nombreCpc = $rowCpc[0];
            }else{
                $sqlrCpc = "SELECT titulo FROM ccpetbienestransportables WHERE GRUPO = '$cpc'";
                $resCpc = mysqli_query($linkbd, $sqlrCpc);
                $rowCpc = mysqli_fetch_row($resCpc);
                $nombreCpc = $rowCpc[0];
            }

            $divipola = '';
            $chip = '';

            $valor = $row[13];

            $detSectorial = $row[11];

            array_push($det, $tipoGasto);
            array_push($det, $dependencia);
            array_push($det, $nombreDependencia);
            array_push($det, $medio_pago);
            array_push($det, $vigencia_gasto);
            array_push($det, $nombreVigencia);
            array_push($det, $proyecto);
            array_push($det, $nombreProyecto);
            array_push($det, $indicador);
            array_push($det, $indicadorProducto);
            array_push($det, $cuentaCcpet);
            array_push($det, $nombreCuenta);
            array_push($det, $fuente);
            array_push($det, $nombreFuente);
            array_push($det, $cpc);
            array_push($det, $nombreCpc);
            array_push($det, $divipola);
            array_push($det, $chip);
            array_push($det, $valor);
            array_push($det, $detSectorial);


            array_push($detalles, $det);
        }

        $out['detalles'] = $detalles;

    }

    if($action == 'sectores'){
        $sectores = [];

        $sqlr = "SELECT codigo, nombre FROM ccpetsectores ORDER BY codigo ASC";
        $res = mysqli_query($linkbd, $sqlr);
        while($row = mysqli_fetch_row($res)){
            array_push($sectores, $row);
        }

        $out['sectores'] = $sectores;
    }

    if($action == 'programas'){
        $programas = [];

        $sqlr = "SELECT codigo, nombre, codigo_subprograma, nombre_subprograma FROM ccpetprogramas ORDER BY codigo ASC";
        $res = mysqli_query($linkbd, $sqlr);
        while($row = mysqli_fetch_row($res)){
            array_push($programas, $row);
        }

        $out['programas'] = $programas;
    }

    if($action == 'productos'){
        $productos = [];

        $sqlr = "SELECT cod_producto, producto FROM ccpetproductos ORDER BY cod_producto ASC";
        $res = mysqli_query($linkbd, $sqlr);
        while($row = mysqli_fetch_row($res)){
            array_push($productos, $row);
        }

        $out['productos'] = $productos;
    }

    if($action == 'seleccionarConsecutivo'){

        $consecutivo = 0;
        $sqlr = "SELECT MAX(consvigencia) FROM ccpetcdp WHERE vigencia=$_GET[vigencia] AND tipo_mov='201'";
        $res = mysqli_query($linkbd, $sqlr);
        while($r = mysqli_fetch_row($res)){
            $maximo = $r[0];
        }
        if(!$maximo){
            $consecutivo = 1;
        }
        else{
            $consecutivo = $maximo+1;
        }

        $out['consecutivo'] = $consecutivo;
    }

    if($action == 'cargarVigencia'){

        $vigencia = vigencia_usuarios($_SESSION['cedulausu']);
        $out['vigencia'] = $vigencia;

    }

    if($action == 'cargarTiposDeGasto'){

        $tiposDeGasto = array();

        $sqlr = "SELECT * FROM ccpettipo_gasto WHERE estado = 'S'";
        $res = mysqli_query($linkbd, $sqlr);
        while($row = mysqli_fetch_row($res)){
            array_push($tiposDeGasto, $row);
        }

        $out['tiposDeGasto'] = $tiposDeGasto;
    }

    if($action == 'cargarCuentas'){

        $cuentasCcpet = array();
        $inicioCuenta = $_GET['inicioCuenta'];
        $sqlr = "SELECT * FROM cuentasccpet WHERE version = $maxVersion AND codigo LIKE '$inicioCuenta%' ORDER BY id ASC";
        $res = mysqli_query($linkbd, $sqlr);
        while($row = mysqli_fetch_row($res)){
            array_push($cuentasCcpet, $row);
        }

        $out['cuentasCcpet'] = $cuentasCcpet;
    }

    if($action == 'cargarProgramaticos'){

        $cuentasCcpet = array();

        $programaticos = array();
        $proyecto = $_GET['proyecto'];
        $sqlr = "SELECT id FROM ccpproyectospresupuesto WHERE codigo = '$proyecto'";
        $res = mysqli_query($linkbd, $sqlr);

        while($row = mysqli_fetch_row($res)){
            $sqlrP = "SELECT indicador FROM ccpproyectospresupuesto_productos WHERE codproyecto = '$row[0]' GROUP BY indicador";
            $resP = mysqli_query($linkbd, $sqlrP);
            $programatico = array();
            while($rowP = mysqli_fetch_row($resP)){
                $programatico = array();
                $sqlrNom = "SELECT indicador_producto FROM ccpetproductos WHERE codigo_indicador = '$rowP[0]'";
                $resNom = mysqli_query($linkbd, $sqlrNom);
                $rowNom = mysqli_fetch_row($resNom);
                array_push($programatico, $rowP[0]);
                array_push($programatico, $rowNom[0]);

                array_push($programaticos, $programatico);

            }

        }


        $out['programaticos'] = $programaticos;
    }

    if($action == 'cargarProyecto'){

        $proyectosCcpet = array();
        $vigencia = $_GET['vigencia'];
        $sqlr = "SELECT * FROM ccpproyectospresupuesto WHERE vigencia = '$vigencia'";
        $res = mysqli_query($linkbd, $sqlr);
        while($row = mysqli_fetch_row($res)){
            array_push($proyectosCcpet, $row);
        }

        $out['proyectosCcpet'] = $proyectosCcpet;
    }

    if($action == 'validarCdpRev'){

        $cdpValido = false;
        $fecha = $_GET['fecha'];
        $vigencia = $_GET['vigencia'];
        $consvigencia = $_GET['consvigencia'];
        $sqlr = "SELECT consvigencia, vigencia, objeto FROM ccpetcdp WHERE vigencia = '$vigencia' AND tipo_mov = '201' AND consvigencia = '$consvigencia' ";
        $res = mysqli_query($linkbd, $sqlr);
        while($row = mysqli_fetch_row($res)){
            $valorCDP = 0;
            $valorCDP = generaSaldoCDPccpetBasico($row[0],$row[1],$fecha);
			if($valorCDP>0){
                $cdpValido = true;
            }

        }

        $out['cdpValido'] = $cdpValido;
    }

    if($action == 'buscarCuenta'){
        $cuenta = $_GET['cuenta'];
        $resultBusquedaCuenta = array();

        $sqlr = "SELECT nombre, tipo FROM cuentasccpet WHERE version = $maxVersion AND codigo = '$cuenta' ORDER BY id ASC";
        $res = mysqli_query($linkbd, $sqlr);//echo $sqlr;
        while($row = mysqli_fetch_row($res)){
            array_push($resultBusquedaCuenta, $row);
        }

        $out['resultBusquedaCuenta'] = $resultBusquedaCuenta;

    }

    if($action == 'buscarProyecto'){
        $proyecto = $_GET['proyecto'];
        $resultBusquedaProyecto = array();

        $sqlr = "SELECT nombre FROM ccpproyectospresupuesto WHERE codigo = '$proyecto'";
        $res = mysqli_query($linkbd, $sqlr);//echo $sqlr;
        while($row = mysqli_fetch_row($res)){
            array_push($resultBusquedaProyecto, $row);
        }

        $out['resultBusquedaProyecto'] = $resultBusquedaProyecto;

    }

    if($action == 'buscarProgramatico'){
        $programatico = $_GET['programatico'];
        $resultBusquedaProgramatico = array();

        $sqlr = "SELECT indicador_producto FROM ccpetproductos WHERE codigo_indicador = '$programatico'";
        $res = mysqli_query($linkbd, $sqlr);//echo $sqlr;
        while($row = mysqli_fetch_row($res)){
            array_push($resultBusquedaProgramatico, $row);
        }

        $out['resultBusquedaProgramatico'] = $resultBusquedaProgramatico;

    }

    if($action == 'buscarFuente'){
        $fuente = $_GET['fuente'];
        $resultBusquedaFuente = array();

        $sqlr = "SELECT nombre FROM ccpet_fuentes_cuipo WHERE version = (SELECT MAX(version) FROM ccpet_fuentes_cuipo ) AND codigo_fuente = '$fuente' ";
        $res = mysqli_query($linkbd, $sqlr);//echo $sqlr;
        while($row = mysqli_fetch_row($res)){
            array_push($resultBusquedaFuente, $row);
        }

        $out['resultBusquedaFuente'] = $resultBusquedaFuente;
    }

    if($action == 'buscarDivipola'){
        $divipola = $_GET['divipola'];
        $resultBusquedaDivipola = array();

        $sqlr = "SELECT nombre FROM ccpet_divipolas WHERE version = (SELECT MAX(version) FROM ccpet_divipolas ) AND codigo = '$divipola'";
        $res = mysqli_query($linkbd, $sqlr);//echo $sqlr;
        while($row = mysqli_fetch_row($res)){
            array_push($resultBusquedaDivipola, $row);
        }

        $out['resultBusquedaDivipola'] = $resultBusquedaDivipola;
    }

    if($action == 'buscarChip'){
        $chip = $_GET['chip'];
        $resultBusquedaChip = array();

        $sqlr = "SELECT nombre FROM ccpet_chip WHERE version = (SELECT MAX(version) FROM ccpet_chip ) AND codigo = '$chip'";
        $res = mysqli_query($linkbd, $sqlr);//echo $sqlr;
        while($row = mysqli_fetch_row($res)){
            array_push($resultBusquedaChip, $row);
        }

        $out['resultBusquedaChip'] = $resultBusquedaChip;
    }

    if($action == 'buscarCpc'){
        $codigoCpc = $_GET['codigoCpc'];

        $resultBusquedaCpc = array();

        $rest = substr($codigoCpc, 0, 1);

        if($rest <= 4){
            $sqlr = "SELECT titulo FROM ccpetbienestransportables WHERE version = (SELECT MAX(version) FROM ccpetbienestransportables ) AND grupo = '$codigoCpc'";

        }else{
            $sqlr = "SELECT titulo FROM ccpetservicios WHERE version = (SELECT MAX(version) FROM ccpetservicios ) AND grupo = '$codigoCpc'";
        }

        $res = mysqli_query($linkbd, $sqlr);
        while($row = mysqli_fetch_row($res)){
            array_push($resultBusquedaCpc, $row);
        }


        $out['resultBusquedaCpc'] = $resultBusquedaCpc;
    }



    if($action == 'filtrarCuentas'){

        $keywordCuenta=$_POST['keywordCuenta'];

        $cuentasCcpet = array();
        $inicioCuenta = $_GET['inicioCuenta'];

        $sqlr = "SELECT * FROM cuentasccpet WHERE version = $maxVersion AND codigo LIKE '$inicioCuenta%' AND concat_ws(' ', codigo, nombre) LIKE '%$keywordCuenta%' ORDER BY id ASC";//echo $sqlr;
        $res = mysqli_query($linkbd, $sqlr);
        while($row = mysqli_fetch_row($res)){
            array_push($cuentasCcpet, $row);
        }

        $out['cuentasCcpet'] = $cuentasCcpet;
    }

    if($action == 'filtrarProyectos'){

        $keywordProyecto = $_POST['keywordProyecto'];

        $proyectosCcpet = array();
        $vigencia = $_GET['vigencia'];
        $sqlr = "SELECT * FROM ccpproyectospresupuesto WHERE vigencia = '$vigencia' AND concat_ws(' ', codigo, nombre) LIKE '%$keywordProyecto%'";
        $res = mysqli_query($linkbd, $sqlr);
        while($row = mysqli_fetch_row($res)){
            array_push($proyectosCcpet, $row);
        }

        $out['proyectosCcpet'] = $proyectosCcpet;
    }

    if($action == 'cargarFuentes'){

        $fuentes = array();
        $sqlrMaxFuente = "SELECT MAX(version) FROM ccpet_fuentes_cuipo";
        $resMaxFuente = mysqli_query($linkbd, $sqlrMaxFuente);
        $rowMaxFuente = mysqli_fetch_row($resMaxFuente);

        $cuenta = $_GET['cuenta'];
        $tipoGasto = $_GET['tipoGasto'];
        $proyecto = $_GET['proyecto'];
        $programatico = $_GET['programatico'];

        //echo $_GET['programatico'];

        if($tipoGasto != '3'){
            $sql = "SELECT ccp_f.codigo_fuente, ccp_f.nombre FROM ccpet_fuentes_cuipo AS ccp_f, ccpetinicialgastosfun AS ccp_g_i WHERE ccp_f.version = '$rowMaxFuente[0]' AND ccp_f.codigo_fuente =  ccp_g_i.fuente AND ccp_g_i.cuenta = '$cuenta' AND ccp_g_i.valor > 0 GROUP BY ccp_f.codigo_fuente";

        }else{
            $sql = "SELECT ccp_f.codigo_fuente, ccp_f.nombre FROM ccpet_fuentes_cuipo AS ccp_f, ccpproyectospresupuesto AS ccp_g_i, ccpproyectospresupuesto_presupuesto AS cpp_g_i_d WHERE ccp_f.version = '$rowMaxFuente[0]' AND ccp_f.codigo_fuente =  cpp_g_i_d.id_fuente AND ccp_g_i.codigo = '$proyecto' AND (cpp_g_i_d.valorcsf > 0 OR cpp_g_i_d.valorssf > 0) AND cpp_g_i_d.indicador_producto = '$programatico' AND ccp_g_i.id = cpp_g_i_d.codproyecto GROUP BY ccp_f.codigo_fuente";
        }

        //echo $sql;
        $res = mysqli_query($linkbd, $sql);

        while($row = mysqli_fetch_row($res))
        {
            array_push($fuentes, $row);
        }

        $sqlrAd = "SELECT ccp_f.codigo_fuente, ccp_f.nombre FROM ccpet_fuentes_cuipo AS ccp_f, ccpet_adiciones AS ccp_ad WHERE ccp_f.version = '$rowMaxFuente[0]' AND ccp_f.codigo_fuente = ccp_ad.fuente AND ccp_ad.cuenta LIKE '%$cuenta%' AND ccp_ad.bpim LIKE '%$proyecto%' AND  ccp_ad.programatico LIKE '%$programatico%' GROUP BY ccp_f.codigo_fuente ";

        $resAd = mysqli_query($linkbd, $sqlrAd);

        while($rowAd = mysqli_fetch_row($resAd)){
            array_push($fuentes, $rowAd);
        }

        $sqlrCred = "SELECT ccp_f.codigo_fuente, ccp_f.nombre FROM ccpet_fuentes_cuipo AS ccp_f, ccpet_traslados AS ccp_cred WHERE ccp_f.version = '$rowMaxFuente[0]' AND ccp_cred.tipo='C' AND ccp_f.codigo_fuente = ccp_cred.fuente AND ccp_cred.cuenta LIKE '%$cuenta%' AND ccp_cred.bpim LIKE '%$proyecto' AND ccp_cred.programatico LIKE '%$programatico%' GROUP BY ccp_f.codigo_fuente";

        $resCred = mysqli_query($linkbd, $sqlrCred);

        while($rowCred = mysqli_fetch_row($resCred)){
            array_push($fuentes, $rowCred);
        }

        $out['fuentes'] = $fuentes;

    }

    if($action == 'cargarDivipolas'){

        $divipolas = array();
        $sql = "SELECT codigo, nombre FROM ccpet_divipolas WHERE version = (SELECT MAX(version) FROM ccpet_divipolas )";
        $res = mysqli_query($linkbd, $sql);

        while($row = mysqli_fetch_row($res))
        {
            array_push($divipolas, $row);
        }

        $out['divipolas'] = $divipolas;

    }

    if($action == 'cargarChips'){

        $chips = array();
        $sql = "SELECT codigo, nombre FROM ccpet_chip WHERE version = (SELECT MAX(version) FROM ccpet_chip )";
        $res = mysqli_query($linkbd, $sql);

        while($row = mysqli_fetch_row($res))
        {
            array_push($chips, $row);
        }

        $out['chips'] = $chips;

    }

    if($action == 'filtrarFuentes'){

        $keywordFuente = $_POST['keywordFuente'];

        $sql = "SELECT codigo_fuente, nombre FROM ccpet_fuentes_cuipo WHERE concat_ws(' ', codigo_fuente, nombre) LIKE '%$keywordFuente%' AND version = (SELECT MAX(version) FROM ccpet_fuentes_cuipo )";
        $res = mysqli_query($linkbd,$sql);

        $fuentes = array();

        while($row = mysqli_fetch_row($res))
        {
            array_push($fuentes, $row);
        }

        $out['fuentes'] = $fuentes;

    }

    if($action == 'filtrarDivipolas'){

        $keywordDivipola = $_POST['keywordDivipola'];

        $sql = "SELECT codigo, nombre FROM ccpet_divipolas WHERE concat_ws(' ', codigo, nombre) LIKE '%$keywordDivipola%' AND version = (SELECT MAX(version) FROM ccpet_divipolas )";
        $res = mysqli_query($linkbd,$sql);
        $divipolas = array();

        while($row = mysqli_fetch_row($res))
        {
            array_push($divipolas, $row);
        }

        $out['divipolas'] = $divipolas;

    }

    if($action == 'filtrarChips'){

        $keywordChip = $_POST['keywordChip'];

        $sql = "SELECT codigo, nombre FROM ccpet_chip WHERE concat_ws(' ', codigo, nombre) LIKE '%$keywordChip%' AND version = (SELECT MAX(version) FROM ccpet_chip )";
        $res = mysqli_query($linkbd,$sql);
        $chips = array();

        while($row = mysqli_fetch_row($res))
        {
            array_push($chips, $row);
        }

        $out['chips'] = $chips;

    }


    //[rubro, fuente, vigencia, tipo_gasto, seccion_presupuestal, medio_pago, vigencia_gasto]
    if($action == 'saldoPorCuenta'){

        $parametros = [
            'rubro' => $_POST['rubro'],
            'fuente' => $_POST['fuente'],
            'vigencia' => $_POST['vigencia'],
            'tipo_gasto' => $_POST['tipo_gasto'],
            'seccion_presupuestal' => $_POST['seccion_presupuestal'],
            'medio_pago' => $_POST['medio_pago'],
            'vigencia_gasto' => $_POST['vigencia_gasto'],

            'codProyecto' => $_POST['codProyecto'],
            'programatico' => $_POST['programatico'],
        ];

        $saldoPorCuenta = 0;

        $saldoPorCuenta = saldoPorRubro($parametros);

        $out['saldoPorCuenta'] = round($saldoPorCuenta, 2);
    }



    if($action == 'guardarCdp'){

        $user = $_SESSION['nickusu'];
        $estado = '';
        $area_solicitante = $_POST["areaSolicitante"];
        if($_POST["tipoDeMovimiento"] == '201'){
            $estado = 'S';

            $consecutivo = 0;
            $sqlr = "SELECT MAX(consvigencia) FROM ccpetcdp WHERE vigencia='".$_POST["vigencia"]."' AND tipo_mov='201'";
            $res = mysqli_query($linkbd, $sqlr);
            while($r = mysqli_fetch_row($res)){
                $maximo = $r[0];
            }
            if(!$maximo){
                $consecutivo = 1;
            }
            else{
                $consecutivo = $maximo+1;
            }

            $_POST["consecutivo"] = $consecutivo;

            $sqlrUpdateSolicitud = "UPDATE plan_solicitud_cdp SET estado = 'A' WHERE id = '".$_POST["solicitud"]."'";
            mysqli_query($linkbd, $sqlrUpdateSolicitud);

            $sqlrInsert = "INSERT INTO ccpet_solicitud_cdp (id_solicitud_cdp, vigencia, consecutivo) VALUES ('".$_POST["solicitud"]."', '".$_POST["vigencia"]."', '".$_POST["consecutivo"]."')";
            mysqli_query($linkbd, $sqlrInsert);

        }else{
            $estado = 'R';
            $sqlr = "UPDATE ccpetcdp SET estado = 'R' WHERE consvigencia = '".$_POST["consecutivo"]."' AND vigencia = '".$_POST["vigencia"]."'";
            mysqli_query($linkbd, $sqlr);
        }

        preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST["fecha"], $fecha);
		$fechaf = $fecha[3]."-".$fecha[2]."-".$fecha[1];

        //verificar si ya existe un cdp con el mismo consecutivo y estado

        $sqlrB = "SELECT * FROM ccpetcdp WHERE consvigencia = '".$_POST["consecutivo"]."' AND vigencia = '".$_POST["vigencia"]."' AND tipo_mov LIKE '4%'";
        $res = mysqli_query($linkbd, $sqlrB);
        if(mysqli_num_rows($res) == 0){
            $sqlr = "INSERT INTO ccpetcdp (vigencia, consvigencia, fecha, valor, estado, solicita, objeto, saldo, tipo_mov, user) VALUES ('".$_POST["vigencia"]."', '".$_POST["consecutivo"]."', '$_POST[fecha]', '".$_POST["totalCdp"]."', '$estado', '".$_POST["solicita"]."', '".$_POST["objeto"]."', '".$_POST["totalCdp"]."', '".$_POST["tipoDeMovimiento"]."', '$user')";
            mysqli_query($linkbd, $sqlr);
        }

        if(count($_POST["detallesCdp"]) > 0){

            for($x = 0; $x < count($_POST["detallesCdp"]); $x++){

                $vigencia = '';
                $consecutivo = '';
                $rubro = '';
                $fuente = '';
                $valor = '';
                $saldo = '';
                $indicador_producto = '';
                $medio_pago = '';
                $codigo_vigenciag = '';
                $bpim = '';
                $seccion_presupuestal = '';
                $tipo_gasto = '';
                $divipola = '';
                $chip = '';
                $productoservicio = '';
                $detalle_sectorial = '';


                $vigencia = $_POST["vigencia"];
                $consecutivo = $_POST["consecutivo"];

                if($estado == 'S'){
                    $rubro = $_POST["detallesCdp"][$x][10];
                    $fuente = $_POST["detallesCdp"][$x][12];
                    $valor = $_POST["detallesCdp"][$x][18];
                    $saldo = $_POST["detallesCdp"][$x][18];
                    $indicador_producto = $_POST["detallesCdp"][$x][8];
                    $medio_pago = $_POST["detallesCdp"][$x][3];
                    $codigo_vigenciag = $_POST["detallesCdp"][$x][4];
                    $bpim = $_POST["detallesCdp"][$x][6];
                    $seccion_presupuestal = $_POST["detallesCdp"][$x][1];
                    $tipo_gasto = $_POST["detallesCdp"][$x][0];
                    $divipola = $_POST["detallesCdp"][$x][16];
                    $chip = $_POST["detallesCdp"][$x][17];
                    $productoservicio = $_POST["detallesCdp"][$x][14];
                    $detalle_sectorial = $_POST["detallesCdp"][$x][19];
                }else{
                    $rubro = $_POST["detallesCdp"][$x][3];
                    $fuente = $_POST["detallesCdp"][$x][5];
                    $valor = $_POST["valorRev"][$x];
                    $saldo = $_POST["valorRev"][$x];
                    $indicador_producto = $_POST["detallesCdp"][$x][11];
                    $medio_pago = $_POST["detallesCdp"][$x][12];
                    $codigo_vigenciag = $_POST["detallesCdp"][$x][14];
                    $bpim = $_POST["detallesCdp"][$x][16];
                    $seccion_presupuestal = $_POST["detallesCdp"][$x][17];
                    $tipo_gasto = $_POST["detallesCdp"][$x][18];
                    $divipola = $_POST["detallesCdp"][$x][19];
                    $chip = $_POST["detallesCdp"][$x][20];
                    $productoservicio = $_POST["detallesCdp"][$x][4];
                    $detalle_sectorial = ''; // falta detalle sectoria reversion
                }

                if($bpim == 'null'){
                    $bpim = '';
                }

                $chip = $chip == '' ? 1 : $chip;




                /* $rubro = $_POST["detallesCdp"][$x][10];
                $bpim = '';

                if($_POST["detallesCdp"][$x][0] == 'Inversion'){
                    $rubro = '2.99';
                    $bpim = $_POST["detallesCdp"][$x][6];
                }else{
                    $rubro = $_POST["detallesCdp"][$x][6];
                } */

                $sqlrD = "INSERT INTO ccpetcdp_detalle(vigencia, consvigencia, cuenta, productoservicio, fuente, valor, estado, saldo, tipo_mov, indicador_producto, medio_pago, codigo_vigenciag, bpim, seccion_presupuestal, tipo_gasto, divipola, chip, area, detalle_sectorial) VALUES ('".$vigencia."', '".$consecutivo."', '".trim($rubro)."', '".trim($productoservicio)."', '".trim($fuente)."', '".$valor."', '$estado', '".$saldo."', '".$_POST["tipoDeMovimiento"]."', '".trim($indicador_producto)."', '".$medio_pago."', '".$codigo_vigenciag."', '".trim($bpim)."', '".$seccion_presupuestal."', '".$tipo_gasto."', '".$divipola."', '".$chip."', '".$area_solicitante."', '".$detalle_sectorial."')";
                mysqli_query($linkbd, $sqlrD);
            }
            $out['insertaBien'] = true;
        }else{
            $out['insertaBien'] = false;
        }

        $out['consecutivo'] = $_POST["consecutivo"];

    }

    if($action == 'tieneCpc'){

        $cuenta = $_GET['cuenta'];

        $tieneCpc = '';

        $sqlr = "SELECT clasificadores FROM ccpetprogramarclasificadoresgastos WHERE cuenta = '$cuenta' AND clasificadores != '' ";
        $res = mysqli_query($linkbd,$sqlr);
        $row = mysqli_fetch_row($res);

        $tieneCpc =  $row[0];

        $out['tieneCpc'] = $tieneCpc;

    }

    header("Content-type: application/json");
    echo json_encode($out);
    die();
