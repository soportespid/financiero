import { divideRubro } from "../../funciones.js";

var app = new Vue({
    el: '#myapp',
	data:{

        tiposDeMovimiento: [],
        tipoMovimiento: '',
        consecutivo: '',
        mostrarCrearCdp : false,
        mostrarReversionCdp : false,
        mostrarInversion : false,
        mostrarFuncionamiento : true,
        solicita: '',
        objeto: '',
        tiposDeGasto: [],
        tipoGasto: '',
        unidadesejecutoras: [],
        selectUnidadEjecutora : '',
        optionsMediosPagos: [
			{ text: 'CSF', value: 'CSF' },
			{ text: 'SSF', value: 'SSF' }
		],
        selectMedioPago: 'CSF',
        vigenciasdelgasto: [],
        selectVigenciaGasto: '',
        cuenta:'',
        nombreCuenta: '',
        showModal_cuentas: false,
        fuente: '',
        nombreFuente: '',
        valor: 0,
        saldo: 0,

        detalles: [],
        detallesRev: [],

        cuentasCcpet:[],
        searchCuenta : {keywordCuenta: ''},
        searchProyecto : {keywordProyecto: ''},

        showModal_fuentes: false,
        showModal_divipola: false,
        searchFuente : {keywordFuente: ''},
        searchDivipola : {keywordDivipola: ''},
        searchChip : {keywordChip: ''},



        showModal_chips: false,
        chips : [],

        fuentesCuipo: [],

        totalCdp : 0,

        codProyecto: '',
        nombreProyecto: '',
        showModal_proyectos: false,
        proyectos: [],

        programatico: '',
        nombreProgramatico: '',
        showModal_programatico: false,
        programaticos: [],

        divipola: '',
        nombreDivipola: '',

        chip: '',
        nombreChip: '',

        codigoCpc: '',
        nombreCodigoCpc: '',
        cuentaConCpc: '',

        showModal_bienes_transportables: false,
        show_table_search: false,
        secciones: [],

        searchBienes: {keyword: ''},
        searchDivision: {keywordDivision: ''},
        searchGrupo: {keywordGrupo: ''},
        searchClase: {keywordClase: ''},
        searchSubClase: {keywordSubClase: ''},

        divisiones: [],
        grupos: [],
        clases: [],
        subClases: [],
        subClasesServicios: [],
		subClases_captura: [],



        division_p: '',
        division_p_nombre: '',
        grupo_p: '',
        grupo_p_nombre: '',
        clase_p: '',
        clase_p_nombre: '',
        subClase_p: '',
        subClase_p_nombre: '',
        seccion_p: '',
        seccion_p_nombre: '',

        mostrarDivision: false,
        mostrarGrupo: false,
        mostrarClase: false,
        mostrarSubClase: false,
        mostrarSubClaseProducto: false,

        valorTotal: 0,
        valorBienesTranspotables: 0,

        cuentasSelectSubClase: [],
        searchGeneral: {keywordGeneral: ''},
        searchGeneral2: {keywordGeneral: ''},

        cuentaActual: '',

        showModal_servicios: false,
        seccionesServicios: [],

        gruposServicios: [],
        clasesServicios: [],
        subClasesServicios: [],
        divisionesServicios: [],

        divisionServicios_p: '',
        grupoServicios_p: '',
        claseServicios_p: '',
        subClaseServicios_p: '',
        subClaseServicios_p_nombre: '',
        seccionServicios_p: '',
        seccionServicios_p_nombre: '',

        mostrarGrupoServicios: false,
        mostrarClaseServicios: false,
        mostrarSubClaseServicios: false,
        mostrarDivisionServicios: false,

        searchDivisionServicios: {keywordDivisionServicios: ''},
        searchGrupoServicios: {keywordGrupoServicios: ''},
        searchClaseServicios: {keywordClaseServicios: ''},
        searchSubClaseServicios: {keywordSubClaseServicios: ''},

        divipolas: [],

        loading: false,

        //Variables de la reversion del cdo
        numCdpRev: '',
        descripcionRev: '',
        fechaCdp: '',
        solicitaCdp: '',
        objetoCdp: '',

        showModal_cdps_rev: false,
        cdps_rev: [],
        cdpCabRev: [],
        valorRev: [],
        searchCdpRev : {keywordCdpRev: ''},

        rubroPresupuestal: '',
        ultimoDigito: '',

        solicitud: '',
        showModal_solicitud: false,
        solicitudes: [],
        solicitudesCopy: [],
        searchSolicitud : {keywordSolicitud: ''},
        areas: [],

        fechaSolicitud : '',
        fechaVencimiento : '',
        area_Solicitante : '',
        areaSolicitante_cod : '',

        sectores: [],
        programas: [],
        productos: [],

        txtSearch: '',
        fechaMax: '',
        fecha_cdp: '',

    },

    mounted: async function(){

        //await this.cargarFecha();
        this.cargarTiposDeMovientoUsuario();
        this.cargarTiposDeGasto();
        this.cargarSectores();
        this.cargarProgramas();
        this.cargarProductos();
    },

    methods: {

        cargarProgramas: function(){
            axios.post('presupuesto_ccpet/solicitudcdp/ccp-solicitudcdp.php?action=programas')
            .then((response) => {
                this.programas = response.data.programas;
            })
        },

        cargarSectores: function(){
            axios.post('presupuesto_ccpet/solicitudcdp/ccp-solicitudcdp.php?action=sectores')
            .then((response) => {
                this.sectores = response.data.sectores;
            })   
        },

        cargarProductos: function(){
            axios.post('presupuesto_ccpet/solicitudcdp/ccp-solicitudcdp.php?action=productos')
            .then((response) => {
                this.productos = response.data.productos;
            })
        },

        buscaSector: function(sector){
            if(sector == '') return '';
            let sectorBuscar = sector.substr(0,2);
            let resultado = sectorBuscar +' - '+ this.sectores.filter((e) => e[0] == sectorBuscar)[0][1].toLowerCase();
            return resultado;
        },

        buscaPrograma: function(programatico){
            if(programatico == '') return '';
            let buscaProgramaBuscar = programatico.substr(0,4);
            let resultado = buscaProgramaBuscar +' - '+ this.programas.filter((e) => e[0] == buscaProgramaBuscar)[0][1].toLowerCase();
            return resultado;
        },

        buscaProducto: function(producto){
            if(producto == '') return '';
            let buscaProductoBuscar = producto.substr(0,7);
            let resultado = buscaProductoBuscar +' - '+ this.productos.filter((e) => e[0] == buscaProductoBuscar)[0][1].toLowerCase();
            return resultado;
        },

        buscaSubPrograma: function(programatico){
            if(programatico == '') return '';
            let buscaSubProgramaBuscar = programatico.substr(0,4);
            let resultado = buscaSubProgramaBuscar+''+ this.programas.filter((e) => e[0] == buscaSubProgramaBuscar)[0][2] +' - '+ this.programas.filter((e) => e[0] == buscaSubProgramaBuscar)[0][3].toLowerCase();
            return resultado;
        },

        cargarSolicitudes: function(){
            this.showModal_solicitud = true;
            axios.post('presupuesto_ccpet/solicitudcdp/ccp-solicitudcdp.php?action=solicitudesCdp')
            .then((response) => {
                this.solicitudes = response.data.solicitudesCdp;
                this.solicitudesCopy = response.data.solicitudesCdp;
                this.areas = response.data.areas;
                this.fechaMax = response.data.fechaMax;
            });
        },

        areaSolicitante: function(area){
            return this.areas.filter((e) => e[0] == area)[0][1];
        },

        seleccionarSolicitud: async function(solicitud){
            this.detalles = [];
            this.solicitud = solicitud[0];
            this.fechaSolicitud = solicitud[3];
            this.fechaVencimiento = solicitud[4];
            this.areaSolicitante_cod = solicitud[5];
            this.area_Solicitante = this.areaSolicitante(solicitud[5]);
            this.solicita = this.area_Solicitante;
            this.objeto = solicitud[14];

            await axios.post('presupuesto_ccpet/solicitudcdp/ccp-solicitudcdp.php?action=seleccionarSolicitud&solicitudId='+solicitud[0])
            .then((response) => {
                this.detalles = response.data.detalles;
            }).finally(() => {
                this.showModal_solicitud = false;
            });

            this.sumarTotales();
        },

        sumarTotales: function(){
            this.totalCdp = 0;
            this.detalles.forEach((e) => {
                this.totalCdp += parseFloat(e[18]);
            });
        },

        cargarTiposDeMovientoUsuario: async function(){
            await axios.post('presupuesto_ccpet/solicitudcdp/ccp-solicitudcdp.php')
            .then((response) => {
                //console.log(response.data);
                app.tiposDeMovimiento = response.data.tiposDeMovimiento;

                if(response.data.tiposDeMovimiento.length > 0){
                    this.tipoMovimiento = response.data.tiposDeMovimiento[0][0];
                }

            });

            this.seleccionarFormulario();
        },

        cargarFecha: function(){

            const fechaAct = new Date().toJSON().slice(0,10).replace(/-/g,'/');
            const fechaArr = fechaAct.split('/');
            const fechaV = fechaArr[2]+'/'+fechaArr[1]+'/'+fechaArr[0];
            document.getElementById('fecha').value = fechaV;
            document.getElementById('vigencia').value = fechaArr[0];
            //this.vigencia = fechaArr[0];
        },

        cargarTiposDeGasto: async function(){

            await axios.post('presupuesto_ccpet/solicitudcdp/ccp-solicitudcdp.php?action=cargarTiposDeGasto')
            .then((response) => {

                app.tiposDeGasto = response.data.tiposDeGasto;

                if(response.data.tiposDeGasto.length > 0){
                    this.tipoGasto = response.data.tiposDeGasto[0][0];
                }
            });
            this.cambiaTipoDeGasto();
        },

        seleccionarFormulario: function(){

            if(this.tipoMovimiento == '201'){
                this.mostrarCrearCdp = true;
                this.mostrarReversionCdp = false;
                this.seleccioncarConsecutivo();
                this.seccionPresupuestal();
                this.vigenciasDelgasto();
            }else if(this.tipoMovimiento == '401' || this.tipoMovimiento == '402'){
                this.mostrarReversionCdp = true;
                this.mostrarCrearCdp = false;
            }else{

            }
        },

        seccionPresupuestal: async function(){
            await axios.post('vue/presupuesto_ccp/ccp-inicialgastosfun.php?action=secciones')
                        .then(
                            (response)=>{
                                //console.log(response.data.secciones);
                                this.unidadesejecutoras = response.data.secciones;
                                this.selectUnidadEjecutora = this.unidadesejecutoras[0][0];
                            }
                        );
        },

        vigenciasDelgasto: async function(){
            await axios.post('vue/presupuesto_ccp/ccp-inicialgastosfun.php?action=vigenciasDelGasto')
                        .then(
                            (response) => {
                                this.vigenciasdelgasto = response.data.vigenciasDelGasto;
                                this.selectVigenciaGasto = this.vigenciasdelgasto[0][0];
                            }
                        );
        },

        cambiarParametros: async function(){
            //aqui vamos
            var formData = new FormData();
            formData.append("seccionPresupuestal", this.nombre);
            formData.append("tipo", this.tipogf);
            formData.append("codigo", this.numero);
            formData.append("correlacion", this.correlacion);

            await axios.post('vue/presupuesto_ccp/ccp-funciones.php?action=saldoCuentaCcpet')
                .then(
                    (response) => {
                        this.saldo = response.data.saldoPorCuenta;
                    }
                );



        },

        buscarCta: async function(){
            if(this.cuenta != ''){
                if(this.tiposDeGasto[this.tipoGasto-1][2] == this.cuenta.substring(0, 3)){

                    this.loading = true;
                    await axios.post('presupuesto_ccpet/solicitudcdp/ccp-solicitudcdp.php?action=buscarCuenta&cuenta=' + this.cuenta)
                    .then((response) => {
                        //console.log(response.data.resultBusquedaCuenta);
                        if(response.data.resultBusquedaCuenta.length > 0){
                            if(response.data.resultBusquedaCuenta[0][1] == 'C'){
                                app.nombreCuenta = response.data.resultBusquedaCuenta[0][0];
                                app.codigoCpc = '';
                                app.nombreCodigoCpc = '';
                                app.tieneCpc();

                            }else{
                                Swal.fire(
                                'Tipo de cuenta incorrecto.',
                                'Escoger una cuenta de captura (C)',
                                'warning'
                                ).then((result) => {
                                    app.nombreCuenta = '';
                                    app.cuenta = '';
                                    app.codigoCpc = '';
                                    app.nombreCodigoCpc = '';
                                    app.tieneCpc();

                                });
                            }

                        }else{
                            Swal.fire(
                                'Cuenta incorrecta.',
                                'Esta cuenta no existe en el catalogo CCPET.',
                                'warning'
                                ).then((result) => {
                                    app.nombreCuenta = '';
                                    app.cuenta = '';
                                    app.codigoCpc = '';
                                    app.nombreCodigoCpc = '';
                                    app.tieneCpc();
                                });
                        }

                    }).finally(() => {
                        this.loading =  false;
                    });

                }else{
                    Swal.fire(
                        'Cuenta incorrecta.',
                        'Esta cuenta no coincide con el tipo de gasto seleccionado.',
                        'warning'
                        ).then((result) => {
                            app.nombreCuenta = '';
                            app.cuenta = '';
                            app.codigoCpc = '';
                            app.nombreCodigoCpc = '';
                            app.tieneCpc();
                        });
                }

            }else{
                this.nombreCuenta = '';
            }
            this.parametrosCompletos();
        },

        buscarProyecto: async function(){
            if(this.codProyecto != ''){
                this.loading = true;
                await axios.post('presupuesto_ccpet/solicitudcdp/ccp-solicitudcdp.php?action=buscarProyecto&proyecto=' + this.codProyecto)
                .then((response) => {
                    if(response.data.resultBusquedaProyecto.length > 0){
                            app.nombreProyecto = response.data.resultBusquedaProyecto[0][0];
                    }else{
                        Swal.fire(
                            'Proyecto incorrecto.',
                            'Este proyecto no existe.',
                            'warning'
                            ).then((result) => {
                                app.nombreProyecto = '';
                            });
                    }

                }).finally(() => {
                    this.loading =  false;
                });

            }else{
                this.nombreProyecto = '';
            }
            this.parametrosCompletos();
        },

        buscarProgramatico: async function(bandera = false){
            if(this.programatico != ''){
                if(this.codProyecto != '' || bandera){
                    this.loading = true;
                    await axios.post('presupuesto_ccpet/solicitudcdp/ccp-solicitudcdp.php?action=buscarProgramatico&programatico=' + this.programatico)
                    .then((response) => {
                        if(response.data.resultBusquedaProgramatico.length > 0){
                                app.nombreProgramatico = response.data.resultBusquedaProgramatico[0][0];
                        }else{
                            Swal.fire(
                                'Proyecto incorrecto.',
                                'Este proyecto no existe.',
                                'warning'
                                ).then((result) => {
                                    app.nombreProgramatico = '';
                                });
                        }

                    }).finally(() => {
                        this.loading =  false;
                    });
                }else{
                    Swal.fire(
                        'Falta escoger un proyecto.',
                        '',
                        'info'
                        );
                }


            }else{
                this.nombreProgramatico = '';
            }
            this.parametrosCompletos();
        },

        seleccionarFuente: function(fuenteSelec){
            this.fuente = fuenteSelec [0];
            this.nombreFuente = fuenteSelec [1];
            this.showModal_fuentes = false;
            this.parametrosCompletos();
        },

        seleccionarDivipola: function(divipolaSelec){
            this.divipola = divipolaSelec [0];
            this.nombreDivipola = divipolaSelec [1];
            this.showModal_divipola = false;
        },

        seleccionarChip: function(chipSelec){
            this.chip = chipSelec [0];
            this.nombreChip = chipSelec [1];
            this.showModal_chips = false;
        },

        buscarFuente: async function(){

            if(this.fuente != ''){
                this.loading = true;
                await axios.post('presupuesto_ccpet/solicitudcdp/ccp-solicitudcdp.php?action=buscarFuente&fuente=' + this.fuente)
                .then((response) => {
                    //console.log(response.data.resultBusquedaFuente);
                    if(response.data.resultBusquedaFuente.length > 0){

                        app.nombreFuente = response.data.resultBusquedaFuente[0][0];

                    }else{
                        Swal.fire(
                            'Fuente incorrecta.',
                            'Esta Fuente no existe en las fuentes CUIPO del sistema.',
                            'warning'
                            ).then((result) => {
                                app.nombreFuente = '';
                                app.fuente = '';
                            });
                    }

                }).finally(() => {
                    this.loading =  false;
                });
            }else{
                this.nombreFuente = '';
                this.fuente = '';
            }
            this.parametrosCompletos();
        },

        buscarCodigoCpc: async function(){

            if(this.codigoCpc != ''){
                if(this.codigoCpc.length > 4){

                    let iniciCodigoCpc = this.codigoCpc.substr(0,1);
                    let finCuenta = this.cuenta.substr(-1, 1);
                    if (iniciCodigoCpc == finCuenta) {
                        //if(this.codigoCpc.substr(0,1))
                        this.loading = true;
                        await axios.post('presupuesto_ccpet/solicitudcdp/ccp-solicitudcdp.php?action=buscarCpc&codigoCpc=' + this.codigoCpc)
                        .then((response) => {

                            if(response.data.resultBusquedaCpc.length > 0){

                                app.nombreCodigoCpc = response.data.resultBusquedaCpc[0][0];


                            }else{
                                Swal.fire(
                                    'C&oacute;digo CPC incorrecto.',
                                    'Este C&oacute;digo no existe en el catalogo CPC del sistema.',
                                    'warning'
                                    ).then((result) => {
                                        app.nombreCodigoCpc = '';
                                        app.codigoCpc = '';
                                    });
                            }

                        }).finally(() => {
                            this.loading =  false;
                        });
                    }else{
                        Swal.fire(
                            'C&oacute;digo CPC equivocado.',
                            'Este C&oacute;digo no pertenece a esta esta cuenta ccpet.',
                            'warning'
                            ).then((result) => {
                                app.nombreCodigoCpc = '';
                                app.codigoCpc = '';
                            }
                        );
                    }
                }else{
                    Swal.fire(
                        'C&oacute;digo CPC equivocado.',
                        'Este C&oacute;digo debe tener como m&iacute;nimo 5 digitos.',
                        'warning'
                        ).then((result) => {
                            app.nombreCodigoCpc = '';
                            app.codigoCpc = '';
                        }
                    );
                }

            }else{
                this.nombreCodigoCpc = '';
            }
        },

        buscarChip: async function(){

            if(this.chip != ''){
                this.loading = true;
                await axios.post('presupuesto_ccpet/solicitudcdp/ccp-solicitudcdp.php?action=buscarChip&chip=' + this.chip)
                .then((response) => {
                    //console.log(response.data.resultBusquedaFuente);
                    if(response.data.resultBusquedaChip.length > 0){

                        app.nombreChip = response.data.resultBusquedaChip[0][0];

                    }else{
                        Swal.fire(
                            'Chip incorrecto.',
                            'Este tercero chip no existe en el sistema.',
                            'warning'
                            ).then((result) => {
                                app.nombreChip = '';
                                app.chip = '';
                            });
                    }

                }).finally(() => {
                    this.loading =  false;
                });
            }else{
                this.nombreChip = '';
                this.chip = '';
            }
        },

        buscarDivipola: async function(){

            if(this.divipola != ''){
                this.loading = true;
                await axios.post('presupuesto_ccpet/solicitudcdp/ccp-solicitudcdp.php?action=buscarDivipola&divipola=' + this.divipola)
                .then((response) => {
                    //console.log(response.data.resultBusquedaFuente);
                    if(response.data.resultBusquedaDivipola.length > 0){

                        app.nombreDivipola = response.data.resultBusquedaDivipola[0][0];

                    }else{
                        Swal.fire(
                            'Divipola incorrecto.',
                            'Este codigo divipola no existe en el sistema.',
                            'warning'
                            ).then((result) => {
                                app.nombreDivipola = '';
                                app.divipola = '';
                            });
                    }

                }).finally(() => {
                    this.loading =  false;
                });
            }else{
                this.nombreDivipola = '';
                this.divipola = '';
            }
        },

        ventanaFuente: function(){

            if(this.tipoGasto != '3'){
                if(this.cuenta != ''){
                    this.loading = true;
                    app.showModal_fuentes = true;
                    axios.post('presupuesto_ccpet/solicitudcdp/ccp-solicitudcdp.php?action=cargarFuentes&cuenta=' + this.cuenta + '&tipoGasto=' + this.tipoGasto)
                        .then((response) => {
                            //console.log(response.data);
                            app.fuentesCuipo = response.data.fuentes;

                        }).finally(() => {
                            this.loading =  false;
                        });
                }else{
                    Swal.fire(
                        'Falta escoger la cuenta presupuestal CCPET.',
                        'Escoger la cuenta antes de buscar la fuente, para acotar la busqueda.',
                        'warning'
                    )
                }

            }else{
                if(this.codProyecto != '' && this.programatico != ''){

                    this.loading = true;
                    axios.post('presupuesto_ccpet/solicitudcdp/ccp-solicitudcdp.php?action=cargarFuentes&tipoGasto=' + this.tipoGasto + '&proyecto=' + this.codProyecto + '&programatico=' + this.programatico)
                        .then((response) => {
                            //console.log(response.data);
                            app.showModal_fuentes = true;
                            app.fuentesCuipo = response.data.fuentes;

                        }).finally(() => {
                            this.loading =  false;
                        });
                }else{
                    Swal.fire(
                        'Falta escoger el proyecto o indicador programatico.',
                        'Escoger el proyecto o indicador programatico antes de buscar la fuente, para acotar la busqueda.',
                        'warning'
                    )
                }
            }


        },

        ventanaProyecto: function(){

            this.loading = true;
            var vig = document.getElementById('vigencia').value;
            axios.post('presupuesto_ccpet/solicitudcdp/ccp-solicitudcdp.php?action=cargarProyecto&vigencia='+vig)
                .then((response) => {
                    app.showModal_proyectos = true;
                    app.proyectos = response.data.proyectosCcpet;
                }).finally(() => {
                    this.loading =  false;
                });
        },

        ventanaCuenta: function(){


            this.loading = true;
            axios.post('presupuesto_ccpet/solicitudcdp/ccp-solicitudcdp.php?action=cargarCuentas&inicioCuenta=' + this.tiposDeGasto[this.tipoGasto-1][2])
                .then((response) => {
                    app.showModal_cuentas = true;
                    app.cuentasCcpet = response.data.cuentasCcpet;
                    app.tieneCpc();
                }).finally(() => {
                    this.loading =  false;
                });

        },

        ventanaCodigoCpc: function(){

            this.buscaVentanaModal(this.cuentaConCpc, this.cuenta);

        },

        ventanaDivipola: function(){

            this.loading = true;
            axios.post('presupuesto_ccpet/solicitudcdp/ccp-solicitudcdp.php?action=cargarDivipolas')
                .then((response) => {
                    app.showModal_divipola = true;
                    app.divipolas = response.data.divipolas;
                }).finally(() => {
                    this.loading =  false;
                });

        },

        ventanaChip: function(){

            this.loading = true;
            axios.post('presupuesto_ccpet/solicitudcdp/ccp-solicitudcdp.php?action=cargarChips')
                .then((response) => {
                    app.showModal_chips = true;
                    app.chips = response.data.chips;
                }).finally(() => {
                    this.loading =  false;
                });

        },

        ventanaProgramatico: function(){
            if(this.codProyecto != ''){

                this.loading = true;
                axios.post('presupuesto_ccpet/solicitudcdp/ccp-solicitudcdp.php?action=cargarProgramaticos&proyecto=' + this.codProyecto)
                .then((response) => {
                    app.showModal_programatico = true;
                    app.programaticos = response.data.programaticos;
                }).finally(() => {
                    this.loading =  false;
                });
            }else{
                Swal.fire(
                    'Falta escoger el proyecto.',
                    'Antes de seleccionar el program&aacute;tico se debe seleccionar un proyecto.',
                    'warning'
                    ).then((result) => {
                        app.nombreFuente = '';
                    });
            }


        },

        seleccionarCuenta: function(cuentaSelec){
            if(cuentaSelec[6] == 'C'){

                this.cuenta = cuentaSelec [1];
                this.nombreCuenta = cuentaSelec [2];
                this.showModal_cuentas = false;
                this.tieneCpc();

            }else{

                this.showModal_cuentas = false;
                Swal.fire(
                'Tipo de cuenta incorrecto.',
                'Escoger una cuenta de captura (C)',
                'warning'
                ).then((result) => {

                    if(result.isConfirmed || result.isDismissed){
                        this.showModal_cuentas = true;
                    }

                });


            }
            this.parametrosCompletos();
        },

        seleccionarProyecto: function(proyectoSelec){

            this.codProyecto = proyectoSelec [2];
            this.nombreProyecto = proyectoSelec [4];
            this.showModal_proyectos = false;

            this.parametrosCompletos();
        },

        seleccionarCdpRev: function(cdpRev){

            this.numCdpRev = cdpRev [0];
            this.showModal_cdps_rev = false;
            this.buscaInformacionCdpRev();
        },

        ventanaCdpRev: async function(){


            const fechat = document.getElementById('fc_1198971545').value;
            if(fechat != ''){
                const fechatAr = fechat.split('/');
                const fechaf = fechatAr[2]+'-'+12+'-'+31;
                var vig = fechatAr[2];
                this.loading = true;
                await  axios.post('vue/presupuesto_ccp/ccp-rppbasico.php?action=cargarCdps&vigencia=' + vig + '&fecha=' + fechaf)
                    .then((response) => {
                        console.log(response.data);
                        app.showModal_cdps_rev = true;
                        app.cdps_rev = response.data.cdps;
                    }).finally(() => {
                        this.loading =  false;
                    });
            }else{
                Swal.fire(
                    'Falta la fecha de reversi&oacute;n.',
                    'Falta Escoger la fecha de reversi&oacute;n.',
                    'warning'
                );
            }
        },

        validarCdpRev: async function(){

            const fechat = document.getElementById('fc_1198971545').value;
            if(fechat != ''){
                if(this.numCdpRev != ''){
                    const fechatAr = fechat.split('/');
                    const fechaf = fechatAr[2]+'-'+12+'-'+31;
                    var vig = fechatAr[2];
                    this.loading = true;
                    await  axios.post('presupuesto_ccpet/solicitudcdp/ccp-solicitudcdp.php?action=validarCdpRev&vigencia=' + vig + '&consvigencia=' + this.numCdpRev + '&fecha=' + fechaf )
                    .then((response) => {
                        if(response.data.cdpValido){
                            this.buscaInformacionCdpRev();
                        }else{
                            this.limpiarFormRev();
                        }
                    }).finally(() => {
                        this.loading =  false;
                    });
                }
            }else{
                Swal.fire(
                    'Falta la fecha de reversi&oacute;n.',
                    'Falta Escoger la fecha de reversi&oacute;n.',
                    'warning'
                );
                this.numCdpRev = '';
            }
        },

        limpiarFormRev: function(){
            Swal.fire(
                'Sin saldo.',
                'No tiene saldo en este cdp.',
                'warning'
            );
            this.fechaCdp = '';
            this.numCdpRev = '';
            this.solicitaCdp = '';
            this.objetoCdp = '';
            this.detallesRev = [];
            this.valorRev = [];
        },

        buscaInformacionCdpRev: function(){

            const fechat = document.getElementById('fc_1198971545').value;
            const fechatAr = fechat.split('/');
            const fechaf = fechatAr[2]+'-'+12+'-'+31;
            var vig = fechatAr[2];
            this.loading = true;
            axios.post('vue/presupuesto_ccp/ccp-rppbasico.php?action=buscaInformacionCdp&vigencia=' + vig + '&consvigencia=' +  this.numCdpRev + '&fecha=' + fechaf)
            .then((response) => {
                app.cdpCabRev = response.data.cdpCab;
                app.detallesRev = response.data.cdpDet;
                app.valorRev = response.data.valorReg;
                app.llenarInformacionCdpRev();
                //app.consecutivo = response.data.consecutivo;
            }).finally(() => {
                this.loading =  false;
            });
        },

        llenarInformacionCdpRev: function(){
            for(var x = 0; x < this.cdpCabRev.length; x++){
                this.fechaCdp = this.cdpCabRev[x][1];
                this.solicitaCdp = this.cdpCabRev[x][2];
                this.objetoCdp = this.cdpCabRev[x][3];
            }
            //console.log(this.detallesCdp);
        },

        /* searchMonitorCdpRev: function(){
            var keywordCdp = app.toFormData(app.searchCdpRev);

            const fechat = document.getElementById('fc_1198971545').value;
            const fechatAr = fechat.split('/');
            const fechaf = fechatAr[2]+'-'+fechatAr[1]+'-'+fechatAr[0];
            var vig = document.getElementById('vigencia').value;
            axios.post('vue/presupuesto_ccp/ccp-rppbasico.php?action=filtrarCdps&vigencia=' + vig + '&fecha=' + fechaf, keywordCdp)
            .then((response) => {
               app.cdps_rev = response.data.cdps;

            });
        }, */

        seleccionarProgramatico: function(programaticoSelec){

            this.programatico = programaticoSelec [0];
            this.nombreProgramatico = programaticoSelec [1];
            this.showModal_programatico = false;

            this.parametrosCompletos();
        },

        agregarDetalle: function(){

            if(this.tipoGasto != ''){

                if(this.selectUnidadEjecutora != ''){

                    if(this.selectMedioPago != ''){

                        if(this.selectVigenciaGasto){

                            if(this.cuenta != '' ){

                                if(this.fuente != ''){

                                    if(this.valor > 0){

                                        if(this.valor <= this.saldo){


                                            if(this.tipoGasto == '3'){

                                                if(this.codProyecto != ''){

                                                    if(this.programatico != ''){

                                                        if(this.divipola != ''){

                                                            if(this.chip != ''){

                                                                if(this.cuentaConCpc != ''){

                                                                    if(this.codigoCpc != ''){

                                                                        var detallesAgr = [];
                                                                        this.tiposDeGasto.forEach(function logArrayElements(element, index, array) {
                                                                            //console.log("a[" + index + "] = " + element);
                                                                            if(element[0] == app.tipoGasto){
                                                                                detallesAgr.push(element[1]);
                                                                            }
                                                                        });
                                                                        /* detallesAgr.push(this.tipoGasto); */
                                                                        detallesAgr.push(this.selectUnidadEjecutora);
                                                                        this.unidadesejecutoras.forEach(function logArrayElements(element, index, array) {
                                                                            //console.log("a[" + index + "] = " + element);
                                                                            if(element[0] == app.selectUnidadEjecutora){
                                                                                detallesAgr.push(element[1].toLowerCase());
                                                                            }
                                                                        });
                                                                        detallesAgr.push(this.selectMedioPago);
                                                                        detallesAgr.push(this.selectVigenciaGasto);
                                                                        this.vigenciasdelgasto.forEach(function logArrayElements(element, index, array) {
                                                                            if(element[0] == app.selectVigenciaGasto){
                                                                                detallesAgr.push(element[2].toLowerCase());
                                                                            }
                                                                        });

                                                                        detallesAgr.push(this.codProyecto);
                                                                        detallesAgr.push(this.nombreProyecto.toLowerCase().slice(0,30));
                                                                        detallesAgr.push(this.programatico);
                                                                        detallesAgr.push(this.nombreProgramatico.toLowerCase().slice(0,30));
                                                                        detallesAgr.push(this.cuenta);
                                                                        detallesAgr.push(this.nombreCuenta.toLowerCase().slice(0,30));

                                                                        /* }else{
                                                                            detallesAgr.push(this.cuenta);
                                                                            detallesAgr.push(this.nombreCuenta.toLowerCase().slice(0,15));
                                                                            detallesAgr.push('');
                                                                            detallesAgr.push('');
                                                                        } */

                                                                        detallesAgr.push(this.fuente);
                                                                        detallesAgr.push(this.nombreFuente.toLowerCase().slice(0,30));

                                                                        detallesAgr.push(this.codigoCpc);
                                                                        detallesAgr.push(this.nombreCodigoCpc.toLowerCase().slice(0,15));

                                                                        detallesAgr.push(this.divipola);
                                                                        detallesAgr.push(this.chip);
                                                                        //detallesAgr.push(new Intl.NumberFormat().format(this.valor));

                                                                        detallesAgr.push(this.valor);

                                                                        let existeDetalle = 0;
                                                                        this.detalles.forEach(function(elemento){
                                                                            let result = app.comparaArrays(elemento, detallesAgr);

                                                                            if(result){
                                                                                existeDetalle = 1;
                                                                                return false;
                                                                            }
                                                                        });

                                                                        if(existeDetalle == 0){

                                                                            this.detalles.push(detallesAgr);
                                                                            this.cuenta = '';
                                                                            this.codProyecto = '';
                                                                            this.nombreProyecto = '';
                                                                            this.programatico = '';
                                                                            this.nombreProgramatico = '';
                                                                            this.nombreCuenta = '';
                                                                            this.fuente = '';
                                                                            this.nombreFuente = '';
                                                                            this.divipola = '';
                                                                            this.nombreDivipola = '';
                                                                            this.codigoCpc = '';
                                                                            this.nombreCodigoCpc = '';
                                                                            this.chip = '';
                                                                            this.nombreChip = '';

                                                                            this.totalCdp = parseFloat(this.totalCdp) + parseFloat(this.valor);
                                                                            this.valor = 0;
                                                                            this.saldo = 0;
                                                                            this.cargarTiposDeGasto();
                                                                            this.tieneCpc();


                                                                        }else{

                                                                            Swal.fire(
                                                                                'Detalle repetido.',
                                                                                'Debe tener el rubro o alguno de sus clasificadores diferentes a los detalles que se han agregado.',
                                                                                'warning'
                                                                            );

                                                                        }

                                                                    }else{

                                                                        Swal.fire(
                                                                            'Falta informaci&oacute;n.',
                                                                            'Falta escoger el c&oacute;digo CPC.',
                                                                            'warning'
                                                                        );

                                                                    }

                                                                }else{

                                                                    var detallesAgr = [];
                                                                    this.tiposDeGasto.forEach(function logArrayElements(element, index, array) {
                                                                        //console.log("a[" + index + "] = " + element);
                                                                        if(element[0] == app.tipoGasto){
                                                                            detallesAgr.push(element[1]);
                                                                        }
                                                                    });
                                                                    /* detallesAgr.push(this.tipoGasto); */
                                                                    detallesAgr.push(this.selectUnidadEjecutora);
                                                                    this.unidadesejecutoras.forEach(function logArrayElements(element, index, array) {
                                                                        //console.log("a[" + index + "] = " + element);
                                                                        if(element[0] == app.selectUnidadEjecutora){
                                                                            detallesAgr.push(element[1].toLowerCase());
                                                                        }
                                                                    });
                                                                    detallesAgr.push(this.selectMedioPago);
                                                                    detallesAgr.push(this.selectVigenciaGasto);
                                                                    this.vigenciasdelgasto.forEach(function logArrayElements(element, index, array) {
                                                                        if(element[0] == app.selectVigenciaGasto){
                                                                            detallesAgr.push(element[2].toLowerCase());
                                                                        }
                                                                    });

                                                                    detallesAgr.push(this.codProyecto);
                                                                    detallesAgr.push(this.nombreProyecto.toLowerCase().slice(0,30));
                                                                    detallesAgr.push(this.programatico);
                                                                    detallesAgr.push(this.nombreProgramatico.toLowerCase().slice(0,30));
                                                                    detallesAgr.push(this.cuenta);
                                                                    detallesAgr.push(this.nombreCuenta.toLowerCase().slice(0,30));

                                                                    /* }else{
                                                                        detallesAgr.push(this.cuenta);
                                                                        detallesAgr.push(this.nombreCuenta.toLowerCase().slice(0,15));
                                                                        detallesAgr.push('');
                                                                        detallesAgr.push('');
                                                                    } */

                                                                    detallesAgr.push(this.fuente);
                                                                    detallesAgr.push(this.nombreFuente.toLowerCase().slice(0,30));

                                                                    detallesAgr.push(this.codigoCpc);
                                                                    detallesAgr.push(this.nombreCodigoCpc.toLowerCase().slice(0,15));

                                                                    detallesAgr.push(this.divipola);
                                                                    detallesAgr.push(this.chip);
                                                                    //detallesAgr.push(new Intl.NumberFormat().format(this.valor));

                                                                    detallesAgr.push(this.valor);

                                                                    let existeDetalle = 0;
                                                                    this.detalles.forEach(function(elemento){
                                                                        let result = app.comparaArrays(elemento, detallesAgr);

                                                                        if(result){
                                                                            existeDetalle = 1;
                                                                            return false;
                                                                        }
                                                                    });

                                                                    if(existeDetalle == 0){

                                                                        this.detalles.push(detallesAgr);
                                                                        this.cuenta = '';
                                                                        this.codProyecto = '';
                                                                        this.nombreProyecto = '';
                                                                        this.programatico = '';
                                                                        this.nombreProgramatico = '';
                                                                        this.nombreCuenta = '';
                                                                        this.fuente = '';
                                                                        this.nombreFuente = '';
                                                                        this.divipola = '';
                                                                        this.nombreDivipola = '';
                                                                        this.codigoCpc = '';
                                                                        this.nombreCodigoCpc = '';
                                                                        this.chip = '';
                                                                        this.nombreChip = '';

                                                                        this.totalCdp = parseFloat(this.totalCdp) + parseFloat(this.valor);
                                                                        this.valor = 0;
                                                                        this.saldo = 0;
                                                                        this.cargarTiposDeGasto();
                                                                        this.tieneCpc();


                                                                    }else{

                                                                        Swal.fire(
                                                                            'Detalle repetido.',
                                                                            'Debe tener el rubro o alguno de sus clasificadores diferentes a los detalles que se han agregado.',
                                                                            'warning'
                                                                        );

                                                                    }

                                                                }


                                                            }else{
                                                                Swal.fire(
                                                                    'Falta informaci&oacute;n.',
                                                                    'Falta escoger el c&oacute;digo CHIP.',
                                                                    'warning'
                                                                );
                                                            }


                                                        }else{
                                                            Swal.fire(
                                                                'Falta informaci&oacute;n.',
                                                                'Falta escoger el c&oacute;digo DIVIPOLA.',
                                                                'warning'
                                                            );
                                                        }



                                                    }else{
                                                        Swal.fire(
                                                            'Falta informaci&oacute;n.',
                                                            'Falta escoger el indicador program&aacute;tico MGA.',
                                                            'warning'
                                                        );
                                                    }

                                                }else{
                                                    Swal.fire(
                                                        'Falta informaci&oacute;n.',
                                                        'Falta escoger el proyecto de inversi&oacute;n.',
                                                        'warning'
                                                    );
                                                }

                                            }else{

                                                if(this.divipola != ''){

                                                    if(this.chip != ''){

                                                        if(this.cuentaConCpc != ''){

                                                            if(this.codigoCpc != ''){

                                                                var detallesAgr = [];
                                                                this.tiposDeGasto.forEach(function logArrayElements(element, index, array) {
                                                                    //console.log("a[" + index + "] = " + element);
                                                                    if(element[0] == app.tipoGasto){
                                                                        detallesAgr.push(element[1]);
                                                                    }
                                                                });
                                                                /* detallesAgr.push(this.tipoGasto); */
                                                                detallesAgr.push(this.selectUnidadEjecutora);
                                                                this.unidadesejecutoras.forEach(function logArrayElements(element, index, array) {
                                                                    //console.log("a[" + index + "] = " + element);
                                                                    if(element[0] == app.selectUnidadEjecutora){
                                                                        detallesAgr.push(element[1].toLowerCase());
                                                                    }
                                                                });
                                                                detallesAgr.push(this.selectMedioPago);
                                                                detallesAgr.push(this.selectVigenciaGasto);
                                                                this.vigenciasdelgasto.forEach(function logArrayElements(element, index, array) {
                                                                    if(element[0] == app.selectVigenciaGasto){
                                                                        detallesAgr.push(element[2].toLowerCase());
                                                                    }
                                                                });

                                                                detallesAgr.push('');
                                                                detallesAgr.push('');
                                                                detallesAgr.push('');
                                                                detallesAgr.push('');
                                                                detallesAgr.push(this.cuenta);
                                                                detallesAgr.push(this.nombreCuenta.toLowerCase().slice(0,30));

                                                                /* }else{
                                                                    detallesAgr.push(this.cuenta);
                                                                    detallesAgr.push(this.nombreCuenta.toLowerCase().slice(0,15));
                                                                    detallesAgr.push('');
                                                                    detallesAgr.push('');
                                                                } */

                                                                detallesAgr.push(this.fuente);
                                                                detallesAgr.push(this.nombreFuente.toLowerCase().slice(0,30));

                                                                detallesAgr.push(this.codigoCpc);
                                                                detallesAgr.push(this.nombreCodigoCpc.toLowerCase().slice(0,15));

                                                                detallesAgr.push(this.divipola);
                                                                detallesAgr.push(this.chip);
                                                                //detallesAgr.push(new Intl.NumberFormat().format(this.valor));

                                                                detallesAgr.push(this.valor);

                                                                let existeDetalle = 0;
                                                                this.detalles.forEach(function(elemento){
                                                                    let result = app.comparaArrays(elemento, detallesAgr);

                                                                    if(result){
                                                                        existeDetalle = 1;
                                                                        return false;
                                                                    }
                                                                });

                                                                if(existeDetalle == 0){

                                                                    this.detalles.push(detallesAgr);
                                                                    this.cuenta = '';
                                                                    this.codProyecto = '';
                                                                    this.nombreProyecto = '';
                                                                    this.programatico = '';
                                                                    this.nombreProgramatico = '';
                                                                    this.nombreCuenta = '';
                                                                    this.fuente = '';
                                                                    this.nombreFuente = '';
                                                                    this.divipola = '';
                                                                    this.nombreDivipola = '';
                                                                    this.codigoCpc = '';
                                                                    this.nombreCodigoCpc = '';
                                                                    this.chip = '';
                                                                    this.nombreChip = '';

                                                                    this.totalCdp = parseFloat(this.totalCdp) + parseFloat(this.valor);
                                                                    this.valor = 0;
                                                                    this.saldo = 0;
                                                                    this.cargarTiposDeGasto();
                                                                    this.tieneCpc();


                                                                }else{

                                                                    Swal.fire(
                                                                        'Detalle repetido.',
                                                                        'Debe tener el rubro o alguno de sus clasificadores diferentes a los detalles que se han agregado.',
                                                                        'warning'
                                                                    );

                                                                }

                                                            }else{

                                                                Swal.fire(
                                                                    'Falta informaci&oacute;n.',
                                                                    'Falta escoger el c&oacute;digo CPC.',
                                                                    'warning'
                                                                );

                                                            }

                                                        }else{

                                                            var detallesAgr = [];
                                                            this.tiposDeGasto.forEach(function logArrayElements(element, index, array) {
                                                                //console.log("a[" + index + "] = " + element);
                                                                if(element[0] == app.tipoGasto){
                                                                    detallesAgr.push(element[1]);
                                                                }
                                                            });
                                                            /* detallesAgr.push(this.tipoGasto); */
                                                            detallesAgr.push(this.selectUnidadEjecutora);
                                                            this.unidadesejecutoras.forEach(function logArrayElements(element, index, array) {
                                                                //console.log("a[" + index + "] = " + element);
                                                                if(element[0] == app.selectUnidadEjecutora){
                                                                    detallesAgr.push(element[1].toLowerCase());
                                                                }
                                                            });
                                                            detallesAgr.push(this.selectMedioPago);
                                                            detallesAgr.push(this.selectVigenciaGasto);
                                                            this.vigenciasdelgasto.forEach(function logArrayElements(element, index, array) {
                                                                if(element[0] == app.selectVigenciaGasto){
                                                                    detallesAgr.push(element[2].toLowerCase());
                                                                }
                                                            });

                                                            detallesAgr.push('');
                                                            detallesAgr.push('');
                                                            detallesAgr.push('');
                                                            detallesAgr.push('');
                                                            detallesAgr.push(this.cuenta);
                                                            detallesAgr.push(this.nombreCuenta.toLowerCase().slice(0,30));

                                                            /* }else{
                                                                detallesAgr.push(this.cuenta);
                                                                detallesAgr.push(this.nombreCuenta.toLowerCase().slice(0,15));
                                                                detallesAgr.push('');
                                                                detallesAgr.push('');
                                                            } */

                                                            detallesAgr.push(this.fuente);
                                                            detallesAgr.push(this.nombreFuente.toLowerCase().slice(0,30));

                                                            detallesAgr.push(this.codigoCpc);
                                                            detallesAgr.push(this.nombreCodigoCpc.toLowerCase().slice(0,15));

                                                            detallesAgr.push(this.divipola);
                                                            detallesAgr.push(this.chip);
                                                            //detallesAgr.push(new Intl.NumberFormat().format(this.valor));

                                                            detallesAgr.push(this.valor);

                                                            let existeDetalle = 0;
                                                            this.detalles.forEach(function(elemento){
                                                                let result = app.comparaArrays(elemento, detallesAgr);

                                                                if(result){
                                                                    existeDetalle = 1;
                                                                    return false;
                                                                }
                                                            });

                                                            if(existeDetalle == 0){

                                                                this.detalles.push(detallesAgr);
                                                                this.cuenta = '';
                                                                this.codProyecto = '';
                                                                this.nombreProyecto = '';
                                                                this.programatico = '';
                                                                this.nombreProgramatico = '';
                                                                this.nombreCuenta = '';
                                                                this.fuente = '';
                                                                this.nombreFuente = '';
                                                                this.divipola = '';
                                                                this.nombreDivipola = '';
                                                                this.codigoCpc = '';
                                                                this.nombreCodigoCpc = '';
                                                                this.chip = '';
                                                                this.nombreChip = '';

                                                                this.totalCdp = parseFloat(this.totalCdp) + parseFloat(this.valor);
                                                                this.valor = 0;
                                                                this.saldo = 0;
                                                                this.cargarTiposDeGasto();
                                                                this.tieneCpc();


                                                            }else{

                                                                Swal.fire(
                                                                    'Detalle repetido.',
                                                                    'Debe tener el rubro o alguno de sus clasificadores diferentes a los detalles que se han agregado.',
                                                                    'warning'
                                                                );

                                                            }
                                                        }


                                                    }else{
                                                        Swal.fire(
                                                            'Falta informaci&oacute;n.',
                                                            'Falta escoger el c&oacute;digo CHIP.',
                                                            'warning'
                                                        );
                                                    }


                                                }else{
                                                    Swal.fire(
                                                        'Falta informaci&oacute;n.',
                                                        'Falta escoger el c&oacute;digo DIVIPOLA.',
                                                        'warning'
                                                    );
                                                }

                                            }


                                        }else{

                                            Swal.fire(
                                                'No tiene este saldo disponible.',
                                                'El valor no puede ser mayor al saldo disponible.',
                                                'warning'
                                            );
                                        }


                                    }else{
                                        Swal.fire(
                                            'Falta informaci&oacute;n.',
                                            'Falta digitar el valor.',
                                            'warning'
                                        );
                                    }
                                }else{
                                    Swal.fire(
                                        'Falta informaci&oacute;n.',
                                        'Falta escoger la fuente.',
                                        'warning'
                                    );
                                }
                            }else{
                                Swal.fire(
                                    'Falta informaci&oacute;n.',
                                    'Falta escoger la cuenta presupuestal CCPET.',
                                    'warning'
                                );
                            }
                        }else{
                            Swal.fire(
                                'Falta informaci&oacute;n.',
                                'Falta escoger la vigencia del gasto.',
                                'warning'
                            );
                        }
                    }else{

                        Swal.fire(
                            'Falta informaci&oacute;n.',
                            'Falta escoger el medio de pago.',
                            'warning'
                        );
                    }
                }else{
                    Swal.fire(
                        'Falta informaci&oacute;n.',
                        'Falta escoger la secci&oacute;n presupuestal.',
                        'warning'
                    );
                }
            }else{

                Swal.fire(
                    'Falta informaci&oacute;n.',
                    'Falta escoger el tipo de gasto.',
                    'warning'
                );
            }

            this.seleccioncarConsecutivo();

        },

        comparaArrays: function(array1, array2){
            /* const array1 = [];
            const array2 = [];
            array1.push(a1);
            array2.push(a2); */
            array1 = array1.slice(0,-3);
            array2 = array2.slice(0,-3);

            return Array.isArray(array1) &&
                    Array.isArray(array2) &&
                    array1.length === array2.length &&
                    array1.every((val, index) => val === array2[index]);

        },

        eliminarDetalle: function(item){
            //console.log(item);
            this.totalCdp = parseFloat(this.totalCdp) - parseFloat(item[18]);
            var i = this.detalles.indexOf( item );

            if ( i !== -1 ) {
                this.detalles.splice( i, 1 );
            }

            this.parametrosCompletos();
        },

        searchMonitorCuenta: function(){
            var keywordCuenta = app.toFormData(app.searchCuenta);
            axios.post('presupuesto_ccpet/solicitudcdp/ccp-solicitudcdp.php?action=filtrarCuentas&inicioCuenta=' + this.tiposDeGasto[this.tipoGasto-1][2], keywordCuenta)
            .then((response) => {
                //console.log(response.data);
                app.cuentasCcpet = response.data.cuentasCcpet;

            });
        },

        searchMonitorCdp: function(){
            
        },

        searchMonitorChip: function(){
            var keywordChip = app.toFormData(app.searchChip);
            axios.post('presupuesto_ccpet/solicitudcdp/ccp-solicitudcdp.php?action=filtrarChips', keywordChip)
            .then((response) => {
                //console.log(response.data);
                app.chips = response.data.chips;

            });
        },

        searchMonitorProyecto: function(){
            var keywordProyecto = app.toFormData(app.searchProyecto);
            var vig = document.getElementById('vigencia').value;
            axios.post('presupuesto_ccpet/solicitudcdp/ccp-solicitudcdp.php?action=filtrarProyectos&vigencia=' + vig, keywordProyecto)
            .then((response) => {
                //console.log(response.data);
                app.proyectos = response.data.proyectosCcpet;
            });
        },

        searchMonitorFuente: function(){
            var keywordFuente = app.toFormData(app.searchFuente);
            axios.post('presupuesto_ccpet/solicitudcdp/ccp-solicitudcdp.php?action=filtrarFuentes', keywordFuente)
            .then((response) => {
                app.fuentesCuipo = response.data.fuentes;

            });
        },

        searchMonitorDivipola: function(){
            var keywordDivipola = app.toFormData(app.searchDivipola);
            axios.post('presupuesto_ccpet/solicitudcdp/ccp-solicitudcdp.php?action=filtrarDivipolas', keywordDivipola)
            .then((response) => {
                app.divipolas = response.data.divipolas;

            });
        },

        seleccioncarConsecutivo: function(){
            var vig = document.getElementById('vigencia').value;
            axios.post('presupuesto_ccpet/solicitudcdp/ccp-solicitudcdp.php?action=seleccionarConsecutivo&vigencia=' + vig)
            .then((response) => {

                app.consecutivo = response.data.consecutivo;
            });
        },

        cambiaTipoDeGasto: function(){
            this.cuenta = '';
            this.nombreCuenta = '';
            this.fuente = '';
            this.nombreFuente = '';
            this.saldo = '';
            this.valor = '';
            this.programatico = '';
            this.nombreProgramatico = '';
            this.codProyecto = '';
            this.nombreProyecto = '';

            if(this.tiposDeGasto[this.tipoGasto-1][2] == 2.3){
                this.mostrarInversion = true;
                this.mostrarFuncionamiento = false;
            }else{
                this.mostrarFuncionamiento = true;
                this.mostrarInversion = false;
            }

            this.parametrosCompletos();
        },

        toFormData: function(obj){
            var form_data = new FormData();
            for(var key in obj){
                form_data.append(key, obj[key]);
            }
            return form_data;
        },

        //Revisar si existen todos los parametros
        parametrosCompletos: function(){


            if(this.tipoGasto == 3){
                var vig = document.getElementById('vigencia').value;
                if(this.codProyecto != '' &&this.programatico != ''  && this.fuente != '' && vig != '' && this.tipoGasto != '' && this.selectUnidadEjecutora != '' && this.selectMedioPago != '' && this.selectVigenciaGasto != ''){
                    this.calcularSaldoInversion();
                    this.tieneCpc();
                }else{
                    this.saldo = 0;
                }
            }else{
                var vig = document.getElementById('vigencia').value;
                if(this.cuenta != '' && this.fuente != '' && vig != '' && this.tipoGasto != '' && this.selectUnidadEjecutora != '' && this.selectMedioPago != '' && this.selectVigenciaGasto != ''){
                    this.calcularSaldo();
                    this.tieneCpc();
                }else{
                    this.saldo = 0;
                }
            }

        },

        tieneCpc(){
            if(this.cuenta != ''){

                axios.post('presupuesto_ccpet/solicitudcdp/ccp-solicitudcdp.php?action=tieneCpc&cuenta=' + this.cuenta)
                    .then((response) => {

                    if(response.data.tieneCpc == null){
                        app.cuentaConCpc = '';
                    }else{
                        app.cuentaConCpc = response.data.tieneCpc;
                    }

                });
            }else{

                this.cuentaConCpc = '';
            }
        },

        //Calcular el saldo de la cuenta
        calcularSaldo: async function(){

            var formData = new FormData();
            //guardar cabecera
            //[rubro, fuente, vigencia, tipo_gasto, seccion_presupuestal, medio_pago, vigencia_gasto]
            var vig = document.getElementById('vigencia').value;
            formData.append("rubro",this.cuenta);
            formData.append("fuente", this.fuente);
            formData.append("vigencia", vig);
            formData.append("tipo_gasto", this.tipoGasto);
            formData.append("seccion_presupuestal", this.selectUnidadEjecutora);
            formData.append("medio_pago", this.selectMedioPago);
            formData.append("vigencia_gasto", this.selectVigenciaGasto);
            this.loading = true;
            await axios.post('presupuesto_ccpet/solicitudcdp/ccp-solicitudcdp.php?action=saldoPorCuenta', formData)
                 .then((response) => {
                    console.log(response.data.saldoPorCuenta);
                    app.saldo = response.data.saldoPorCuenta;
                 }).finally(() => {
                    this.loading =  false;
                });
            this.calcularSaldoConDetalles();
        },

        calcularSaldoConDetalles: function(){

            this.detalles.forEach(function(elemento){

                if(app.cuenta == elemento[10] && app.fuente == elemento[12] && app.selectUnidadEjecutora == elemento[1] && app.selectMedioPago == elemento[3] && app.selectVigenciaGasto == elemento[4]){
                    app.saldo -= elemento[18];
                }
            });

        },

        calcularSaldoInversion: async function(){

            var formData = new FormData();
            //guardar cabecera
            //[rubro, fuente, vigencia, tipo_gasto, seccion_presupuestal, medio_pago, vigencia_gasto]
            var vig = document.getElementById('vigencia').value;
            formData.append("codProyecto",this.codProyecto);
            formData.append("programatico",this.programatico);
            formData.append("fuente", this.fuente);
            formData.append("vigencia", vig);
            formData.append("tipo_gasto", this.tipoGasto);
            formData.append("seccion_presupuestal", this.selectUnidadEjecutora);
            formData.append("medio_pago", this.selectMedioPago);
            formData.append("vigencia_gasto", this.selectVigenciaGasto);
            await axios.post('presupuesto_ccpet/solicitudcdp/ccp-solicitudcdp.php?action=saldoPorCuenta', formData)
                 .then((response) => {
                    console.log(response.data.saldoPorCuenta);
                    app.saldo = response.data.saldoPorCuenta;
                 });
            this.calcularSaldoConDetallesInv();
        },

        calcularSaldoConDetallesInv: function(){

            this.detalles.forEach(function(elemento){

                if(app.codProyecto == elemento[6] && app.programatico == elemento[8] && app.fuente == elemento[12] && app.selectUnidadEjecutora == elemento[1] && app.selectMedioPago == elemento[3] && app.selectVigenciaGasto == elemento[4]){
                    app.saldo -= elemento[18];
                }
            });

        },

        buscaVentanaModal: async function(clasificador, cuentaCpc){

            switch (clasificador){

                case "2":
                    this.showModal_bienes_transportables = true;
                    app.show_table_search = false;
                    
                    var ultimoCaracter = cuentaCpc.substr(-1,1);
                    this.ultimoDigito = ultimoCaracter;
                    this.fetchMembersBienes(ultimoCaracter);
                    var cuentaArr = [];
                    cuentaArr[0] = ultimoCaracter;
                    cuentaArr[1] = "nombre";
                    //console.log(cuentaArr);
                    this.division(cuentaArr);
                    app.show_table_search = true;
                break;
                case "3":
                    this.showModal_servicios = true;
                    app.show_table_search = false;
                    
                    var ultimoCaracter = cuentaCpc.substr(-1,1);
                    this.ultimoDigito = ultimoCaracter;
                    this.fetchMembersServicios(ultimoCaracter);
                    var cuentaArr = [];
                    cuentaArr[0] = ultimoCaracter;
                    cuentaArr[1] = "nombre";
                    this.divisionServicios(cuentaArr);
                    app.show_table_search = true;
                break;
            }

        },

        fetchMembersBienes: function(ultimoDigito){
            axios.post('vue/ccp-bienestransportables.php?action=searchGeneral&ultimoDigito='+ultimoDigito)
                .then(function(response){
                    app.bienes_transportables = response.data.bienes_transportables;
                    //app.secciones = response.data.secciones;
                });
        },

        division: function(seccion, scrollArriba = false)
        {
            this.searchDivision= {keywordDivision: ''};
            this.searchGrupo= {keywordGrupo: ''};
            this.searchClase= {keywordClase: ''};
            this.searchSubClase= {keywordSubClase: ''};
            app.divisiones = [];
            app.grupos = [];
            app.clases = [];
            app.subClases = [];
            this.division_p = '';
            this.division_p_nombre = '';
            this.grupo_p = '';
            this.grupo_p_nombre = '';
            this.clase_p = '';
            this.clase_p_nombre = '';
            //this.subClase_p = '';
            this.subClase_p_nombre = '';
            app.mostrarGrupo = false;
            app.mostrarClase = false;
            app.mostrarSubClase = false;
            app.mostrarSubClaseProducto = false;
            this.seccion_p = seccion[0];
            this.seccion_p_nombre = seccion[1];
            axios.post('vue/ccp-bienestransportables.php?seccion='+this.seccion_p)
                .then(function(response){
                    app.divisiones = response.data.divisiones;
                    if(response.data.divisiones == ''){
                        app.mostrarDivision = false;
                    }
                    else{
                        app.mostrarDivision = true;
                    }
                });
            if(!scrollArriba)
            {
                setTimeout(function(){ document.getElementById("end_page").scrollIntoView({block: "end", behavior: "smooth"}); }, 1)
            }
            //console.log(this.divisiones);
        },

        buscarGrupo: function(division, scrollArriba = false)
        {
            this.searchGrupo= {keywordGrupo: ''};
            this.searchClase= {keywordClase: ''};
            this.searchSubClase = {keywordSubClase: ''};
            app.grupos = [];
            app.clases = [];
            app.subClases = [];
            app.mostrarSubClaseProducto = false;
            app.subClases_captura = [];
            app.mostrarClase = false;
            app.mostrarSubClase = false;
            this.grupoServicios_p = '';
            this.claseServicios_p = '';
            this.subClaseServicios_p = '';
            if(this.division_p == '' || this.division_p == division[0] || !app.mostrarGrupo)
            {
                app.mostrarGrupo = !app.mostrarGrupo;
            }

            if(app.mostrarGrupo)
            {
                //app.mostrarSiguienteNivel = !app.mostrarSiguienteNivel;
                this.division_p = division[0];
                axios.post('vue/ccp-bienestransportables.php?division='+this.division_p)
                    .then(function(response){
                        app.grupos = response.data.grupos;
                        if(response.data.grupos == ''){
                            app.mostrarGrupo = false;
                        }
                        else{
                            app.mostrarGrupo = true;
                        }
                    });
            }
            if(!scrollArriba)
            {
                setTimeout(function(){ document.getElementById("end_page").scrollIntoView({behavior: "smooth"}); }, 1)

            }
        },

        buscarClase: function(grupo, scrollArriba = false)
        {
            this.searchClase= {keywordClase: ''};
            this.searchSubClase= {keywordSubClase: ''};
            app.mostrarSubClaseProducto = false;
            app.subClases_captura = [];
            app.clases = [];
            app.subClases = [];
            app.mostrarSubClase = false;
            app.searchProduct = {keywordProduct: ''};
            this.clase_p = '';
            this.clase_p_nombre = '';
            //this.subClase_p = '';
            this.subClase_p_nombre = '';
            //app.productos = [];


            if(this.grupo_p == '' || this.grupo_p == grupo[0] || !app.mostrarClase)
            {
                app.mostrarClase = !app.mostrarClase;
            }

            if(app.mostrarClase)
            {
                //app.mostrarSiguienteNivel = !app.mostrarSiguienteNivel;
                this.grupo_p = grupo[0];
                this.grupo_p_nombre = grupo[1];
                axios.post('vue/ccp-bienestransportables.php?grupo='+this.grupo_p)
                    .then(function(response){
                        app.clases = response.data.clases;
                        if(response.data.clases == ''){
                            app.mostrarClase = false;
                        }
                        else{
                            app.mostrarClase = true;
                        }
                    });
            }
            if(!scrollArriba)
            {
                setTimeout(function(){ document.getElementById("end_page").scrollIntoView({behavior: "smooth"}); }, 1)

            }
        },

        buscarSubclase: function(clase, scrollArriba = false)
        {
            this.searchSubClase = {keywordSubClase: ''};

            app.subClases = [];

            app.mostrarSubClase = false;
            app.mostrarSubClaseProducto = false;
            app.subClases_captura = [];
            app.searchProduct = {keywordProduct: ''};

            this.subClaseServicios_p = '';
            //app.productos = [];
            //this.subClase_p = '';
            this.subClase_p_nombre = '';
            if(this.clase_p == '' || this.clase_p == clase[0] || !app.mostrarSubClase)
            {
                app.mostrarSubClase = !app.mostrarSubClase;
            }

            if(app.mostrarSubClase)
            {
                //app.mostrarSiguienteNivel = !app.mostrarSiguienteNivel;
                this.clase_p = clase[0];
                this.clase_p_nombre = clase[1];
                axios.post('vue/ccp-bienestransportables.php?clase='+this.clase_p)
                    .then(function(response){
                        app.subClases = response.data.subClases;
                        if(response.data.subClases == ''){
                            app.mostrarSubClase = false;
                        }
                        else{
                            app.mostrarSubClase = true;
                        }
                    });
            }
            if(!scrollArriba)
            {
                setTimeout(function(){ document.getElementById("end_page").scrollIntoView({behavior: "smooth"}); }, 1)

            }
        },

        seleccionarSublaseProducto: async function(subClase, scrollArriba = false)
        {
            //this.showModal = !this.showModal;
            this.subClase_p = subClase[0];
            this.subClase_p_nombre = subClase[1];

            await axios.post('vue/ccp-bienestransportables.php?subClase='+this.subClase_p)
            .then(function(response){
                app.subClases_captura = response.data.subClases_captura;
                if(response.data.subClases_captura == ''){
                    app.mostrarSubClaseProducto = false;
                }
                else{
                    app.mostrarSubClaseProducto = true;
                }
            });
            if(!scrollArriba)
            {
                setTimeout(function(){ document.getElementById("end_page").scrollIntoView({behavior: "smooth"}); }, 1)

            }
        },

        seleccionarBienes: function(subClase){
            /*this.cuentasSelectSubClase = [];
            this.cuin_p = '';*/

            this.cuentasSelectSubClase = subClase;
            //this.cuin_p = cuin[2];

            this.subClase_p = subClase[0];
            this.subClase_p_nombre = subClase[1];
        },

        buscarGeneral: function()
		{
			if(this.cuentaActual.slice(-1)==app.searchGeneral.keywordGeneral.substr(0,1))
			{
				var parsedobj = JSON.parse(JSON.stringify(app.searchGeneral))
				if(parsedobj.keywordGeneral == '')
				{
					app.mostrarSeccion = true;
					app.show_table_search = false;
					this.fetchMembers();
				}
				else
				{
					app.grupos = [];
					app.clases = [];
					app.subClases = [];
					app.result_search = [];
					app.mostrarGrupo = false;
					app.mostrarClase = false;
					app.mostrarSubClase = false;
					app.mostrarSeccion = false;
					var keywordGeneral = app.toFormData(app.searchGeneral);
					axios.post('vue/ccp-bienestransportables.php?action=searchGeneral', keywordGeneral)
						.then(function(response){
							app.result_search = response.data.subClasesGeneral;
							//console.log(response.data.subClasesGeneral);
							if(response.data.subClasesGeneral == ''){
								app.noMember = true;
								app.show_table_search = true;
							}
							else{
								app.noMember = false;
								app.show_levels(app.result_search[0]);
								app.show_table_search = true;
							}
						});
					// Enviar el scroll al final cuando ya esta definido
					/*setTimeout(() => {
						document.getElementById("end_page").scrollIntoView({behavior: 'smooth'});
					}, 50);*/
					setTimeout(function(){ document.getElementById("end_page").scrollIntoView({block: "end", behavior: "smooth"}); }, 1);
					app.searchGeneral.keywordGeneral = '';
				}
			}
			else
			{
                this.showModal_bienes_transportables = false;
                Swal.fire(
                'Error en el c&oacute;digo.',
                'Digite un c&oacute;digo que empiece con el mismo n&uacute;mero que termina la cuenta CCPET.',
                'error'
                ).then((result) => {

                    if(result.isConfirmed || result.isDismissed){
                        this.showModal_bienes_transportables = true;
                    }

                });

			}
		},

        buscarGeneral2: function()
		{
			if(this.cuentaActual.slice(-1)==app.searchGeneral2.keywordGeneral.substr(0,1))
			{
				var parsedobj = JSON.parse(JSON.stringify(app.searchGeneral2))
				if(parsedobj.keywordGeneral == '')
				{
					app.mostrarSeccion = true;
					app.show_table_search = false;
					this.fetchMembers();
				}
				else
				{
					app.grupos = [];
					app.clases = [];
					app.subClases = [];
					app.result_search = [];
					app.mostrarGrupo = false;
					app.mostrarClase = false;
					app.mostrarSubClase = false;
					app.mostrarSeccion = false;
					this.cuentasSelectSubClaseServicios = [];
					var keywordGeneral = app.toFormData(app.searchGeneral2);
					axios.post('vue/ccp-servicios.php?action=searchGeneral', keywordGeneral)
						.then(function(response)
						{
							app.result_search = response.data.subClasesGeneral;
							if(response.data.subClasesGeneral == '')
							{
								app.noMember = true;
								app.show_table_search = true;
							}
							else
							{
								app.noMember = false;
								app.show_levels2(app.result_search[0]);
								app.show_table_search = true;
							}
						});
					setTimeout(function(){ document.getElementById("end_page").scrollIntoView({block: "end", behavior: "smooth"}); }, 1);
					app.searchGeneral2.keywordGeneral = '';
				}
			}
			else
			{
				this.toggleMensaje();
				this.colortitulosmensaje='crimson';
				this.titulomensaje='Mensaje de Error';
				this.contenidomensaje='Error en el c\xf3digo, no se puede cambiar el sector';
			}
		},

        show_levels: function(codigo){

            app.mostrarSeccion = true;
			codigo_final = codigo[0];

            //this.subClase_p = codigo_final;
            //console.log(this.subClase_p);
            codigo_subClase = codigo_final.slice(0,-2);
            codigo_Clase = codigo_subClase.slice(0,-1);
            codigo_grupo = codigo_Clase.slice(0,-1);
            codigo_division = codigo_grupo.slice(0,-1);
            codigo_seccion = codigo_division.slice(0,-1);
            this.searchHastaSubclase(codigo_final, codigo_subClase, codigo_Clase, codigo_grupo, codigo_division, codigo_seccion, codigo[5]);
            // else{
            //     console.log(" Este item es de otro nivel! el nivel es " + codigo[4])
            // }
            /*setTimeout(() => {
                const start_page = document.getElementById("start_page").scrollIntoView({behavior: 'smooth'});
            }, 50);*/

        },

        searchHastaSubclase: async function(codigo_final, codigo_subClase, codigo_Clase, codigo_grupo, codigo_division, codigo_seccion, ud)
        {
            app.secciones = [];
            app.divisiones = [];
            app.grupos = [];
            app.clases = [];
            app.subClases = [];

            this.seccion_general = [];
            this.division_general = [];
            this.grupo_general = [];
            this.clase_general = [];
            this.subClase_general = [];
            this.producto_general = [];

            this.seccion_p = codigo_seccion;

            this.fetchMembersBienes();

            // aqui empieza a buscar la seccion
            //var seccion_nombre_buscar =  await this.buscarNombre(this.seccion_p);

            this.seccion_general.push(this.seccion_p);
            //console.log(codigo_seccion);
            //this.seccion_general.push(seccion_nombre_buscar);
            //console.log(this.seccion_general);
            this.division(this.seccion_general, false);
            // aqui termina

            //aqui empiza a buscar el nombre de la division
            //var seccion_nombre_buscar =  await this.buscarNombre(codigo_division);

            this.division_general.push(codigo_division);
            //this.division_general.push(seccion_nombre_buscar);
            //console.log(this.division_general);
            this.buscarGrupo(this.division_general, false);
            //qui termina
            //aqui empiza a buscar el nombre del grupo
            //var seccion_nombre_buscar =  await this.buscarNombre(codigo_grupo);

            this.grupo_general.push(codigo_grupo);
            //this.grupo_general.push(seccion_nombre_buscar);

            this.buscarClase(this.grupo_general, false);
            //aqui termina
            //aqui empiza a buscar el nombre de la clase
            //var seccion_nombre_buscar =  await this.buscarNombre(codigo_Clase);

            this.clase_general.push(codigo_Clase);
            //this.clase_general.push(seccion_nombre_buscar);

            this.buscarSubclase(this.clase_general, false);
            //console.log(codigo_final);


            this.producto_general.push(codigo_subClase);
            //this.producto_general.push(seccion_nombre_buscar);

            this.seleccionarSublaseProducto(this.producto_general);

            var seccion_nombre_buscar =  await this.buscarNombre(codigo_final);
            this.cuentasSelectSubClase.push(codigo_final);
            this.cuentasSelectSubClase.push(seccion_nombre_buscar);
            this.cuentasSelectSubClase.push(ud);

            this.seleccionarBienes(this.cuentasSelectSubClase);

            //aqui termina
            //this.subClase_p = codigo_subClase;

           this.searchGeneral = {keywordGeneral: ''};
        },

        searchMonitorGrupos: function() {
            var keywordGrupo = app.toFormData(app.searchGrupo);
            app.clases = [];
            app.subClases = [];
            app.mostrarClase = false;
            app.mostrarSubClase = false;
            this.searchClase= {keywordClase: ''};
            this.searchSubClase= {keywordSubClase: ''};
            axios.post('vue/ccp-bienestransportables.php?action=searchGrupo&divisionSearch='+this.division_p, keywordGrupo)
                .then(function(response){
                    app.grupos = response.data.grupos;
                    if(response.data.grupos == ''){
                        app.noMember = true;
                        // app.show_table_search = false
                    }
                    else{
                        app.noMember = false;
                        //app.show_table_search = true
                    }

                });


        },

        searchMonitorClases: function() {
            var keywordClase = app.toFormData(app.searchClase);
            app.subClases = [];
            app.mostrarSubClase = false;
            this.searchSubClase= {keywordSubClase: ''};
            axios.post('vue/ccp-bienestransportables.php?action=searchClase1&ultimoDigito='+this.ultimoDigito, keywordClase)
                .then(function(response){
                    app.clases = response.data.clases;
                    if(response.data.clases == ''){
                        app.noMember = true;
                        // app.show_table_search = false
                    }
                    else{
                        app.noMember = false;
                        //app.show_table_search = true
                    }

                });


        },

        searchMonitorSubClases: function() {
            var keywordSubClase = app.toFormData(app.searchSubClase);
            axios.post('vue/ccp-bienestransportables.php?action=searchSubClase1&ultimoDigito='+this.ultimoDigito, keywordSubClase)
                .then(function(response){
                    console.log(response.data);
                    app.subClases = response.data.subClases;
                    if(response.data.subClases == ''){
                        app.noMember = true;
                        // app.show_table_search = false
                    }
                    else{
                        app.noMember = false;
                        //app.show_table_search = true
                    }

                });


        },

        searchMonitorSubClasesServicios: function() {
            var keywordSubClase = app.toFormData(app.searchSubClase);
            axios.post('vue/ccp-servicios.php?action=searchSubClase1&ultimoDigito='+this.ultimoDigito, keywordSubClase)
                .then(function(response){
                    console.log(response.data);
                    app.subClasesServicios = response.data.subClases;
                    if(response.data.subClases == ''){
                        app.noMember = true;
                        // app.show_table_search = false
                    }
                    else{
                        app.noMember = false;
                        //app.show_table_search = true
                    }

                });


        },

        seleccionarCpc: function(){
            if(this.subClase_p != ''){
                this.codigoCpc = this.subClase_p;
                this.nombreCodigoCpc = this.subClase_p_nombre;
                this.subClase = '';
                this.subClase_p_nombre = '';
                this.showModal_bienes_transportables = false;
            }else{
                Swal.fire(
                    'Falta informaci&oacute;n.',
                    'Falta escoger el producto o servicio.',
                    'warning'
                );
            }

        },

        seleccionarServicioCpc: function(){
            if(this.subClaseServicios_p != ''){
                this.codigoCpc = this.subClaseServicios_p;
                this.nombreCodigoCpc = this.subClaseServicios_p_nombre;
                this.subClase = '';
                this.subClaseServicios_p_nombre = '';
                this.showModal_servicios = false;
            }else{
                Swal.fire(
                    'Falta informaci&oacute;n.',
                    'Falta escoger el producto o servicio.',
                    'warning'
                );
            }

        },

        fetchMembersServicios: function(ultimoDigito){
            axios.post('vue/ccp-servicios.php?action=searchGeneral&ultimoDigito='+ultimoDigito)
                .then(function(response){
                    app.subClasesServicios = response.data.subClasesGeneral;
                    //app.seccionesServicios = response.data.secciones;
                });
        },

        divisionServicios: function(seccion, scrollArriba = false)
        {
            this.searchDivisionServicios = {keywordDivisionServicios: ''};
            this.searchGrupoServicios = {keywordGrupoServicios: ''};
            this.searchClaseServicios = {keywordClaseServicios: ''};
            this.searchSubClaseServicios = {keywordSubClaseServicios: ''};
            app.gruposServicios = [];
            app.clasesServicios = [];
            app.subClasesServicios = [];
            this.divisionServicios_p = '';
            this.grupoServicios_p = '';
            this.claseServicios_p = '';
            this.subClaseServicios_p = '';
            app.mostrarGrupoServicios = false;
            app.mostrarClaseServicios = false;
            app.mostrarSubClaseServicios = false;

            this.seccionServicios_p = seccion[0];
            this.seccionServicios_p_nombre = seccion[1];

            axios.post('vue/ccp-servicios.php?seccion='+this.seccionServicios_p)
                .then(function(response){
                    app.divisionesServicios = response.data.divisiones;
                    if(response.data.divisiones == ''){
                        app.mostrarDivisionServicios = false;
                    }
                    else{
                        app.mostrarDivisionServicios = true;
                    }
                });
            if(!scrollArriba)
            {
                setTimeout(function(){ document.getElementById("end_page_servicios").scrollIntoView({block: "end", behavior: "smooth"}); }, 1)
            }
        },

        buscarGrupoServicios: function(division, scrollArriba = false)
        {
            this.searchGrupoServicios= {keywordGrupoServicios: ''};
            this.searchClaseServicios= {keywordClaseServicios: ''};
            this.searchSubClaseServicios = {keywordSubClaseServicios: ''};
            app.gruposServicios = [];
            app.clasesServicios = [];
            app.subClasesServicios = [];
            app.mostrarClaseServicios = false;
            app.mostrarSubClaseServicios = false;
            this.grupoServicios_p = '';
            this.claseServicios_p = '';
            this.subClaseServicios_p = '';
            if(this.divisionServicios_p == '' || this.divisionServicios_p == division[0] || !app.mostrarGrupoServicios)
            {
                app.mostrarGrupoServicios = !app.mostrarGrupoServicios;
            }

            if(app.mostrarGrupoServicios)
            {
                //app.mostrarSiguienteNivel = !app.mostrarSiguienteNivel;
                this.divisionServicios_p = division[0];
                axios.post('vue/ccp-servicios.php?division='+this.divisionServicios_p)
                    .then(function(response){
                        app.gruposServicios = response.data.grupos;
                        if(response.data.grupos == ''){
                            app.mostrarGrupoServicios = false;
                        }
                        else{
                            app.mostrarGrupoServicios = true;
                        }
                    });
            }
            if(!scrollArriba)
            {
                setTimeout(function(){ document.getElementById("end_page_servicios").scrollIntoView({behavior: "smooth"}); }, 1)

            }
        },

        buscarClaseServicios: function(grupo, scrollArriba = false)
        {
            this.searchClaseServicios= {keywordClaseServicios: ''};
            this.searchSubClaseServicios = {keywordSubClaseServicios: ''};
            app.clasesServicios = [];
            app.subClasesServicios = [];
            app.mostrarSubClaseServicios = false;
            this.claseServicios_p = '';
            this.subClaseServicios_p = '';
            //app.productos = [];
            if(this.grupoServicios_p == '' || this.grupoServicios_p == grupo[0] || !app.mostrarClaseServicios)
            {
                app.mostrarClaseServicios = !app.mostrarClaseServicios;
            }

            if(app.mostrarClaseServicios)
            {
                //app.mostrarSiguienteNivel = !app.mostrarSiguienteNivel;
                this.grupoServicios_p = grupo[0];
                axios.post('vue/ccp-servicios.php?grupo='+this.grupoServicios_p)
                    .then(function(response){
                        app.clasesServicios = response.data.clases;
                        if(response.data.clases == ''){
                            app.mostrarClaseServicios = false;
                        }
                        else{
                            app.mostrarClaseServicios = true;
                        }
                    });
            }
            if(!scrollArriba)
            {
                setTimeout(function(){ document.getElementById("end_page_servicios").scrollIntoView({behavior: "smooth"}); }, 1)

            }
        },

        buscarSubclaseServicios: function(clase, scrollArriba = false)
        {

            this.searchSubClaseServicios = {keywordSubClaseServicios: ''};
            //app.productos = [];
            app.subClasesServicios = [];
            this.subClaseServicios_p = '';
            if(this.claseServicios_p == '' || this.claseServicios_p == clase[0] || !app.mostrarSubClaseServicios)
            {
                app.mostrarSubClaseServicios = !app.mostrarSubClaseServicios;
            }

            if(app.mostrarSubClaseServicios)
            {
                //app.mostrarSiguienteNivel = !app.mostrarSiguienteNivel;
                this.claseServicios_p = clase[0];
                axios.post('vue/ccp-servicios.php?clase='+this.claseServicios_p)
                    .then(function(response){
                        app.subClasesServicios = response.data.subClases;
                        if(response.data.subClases == ''){
                            app.mostrarSubClaseServicios = false;
                        }
                        else{
                            app.mostrarSubClaseServicios = true;
                        }
                    });
            }
            if(!scrollArriba)
            {
                setTimeout(function(){ document.getElementById("end_page_servicios").scrollIntoView({behavior: "smooth"}); }, 1)

            }
        },

        seleccionarServicios: function(subClase)
		{
			this.cuentasSelectSubClaseServicios = subClase;
			this.subClaseServicios_p = subClase[0];
			this.subClaseServicios_p_nombre = subClase[1];
		},

        searchHastaSubclase2: async function(codigo_final, codigo_subClase, codigo_Clase, codigo_grupo, codigo_division, codigo_seccion)
		{
			app.secciones = [];
			app.divisiones = [];
			app.grupos = [];
			app.clases = [];
			app.subClases = [];
			this.seccion_general = [];
			this.division_general = [];
			this.grupo_general = [];
			this.clase_general = [];
			this.subClase_general = [];
			this.seccion_p = codigo_seccion;
			// aqui empieza a buscar la seccion
			this.seccion_general.push(this.seccion_p);
			this.divisionServicios(this.seccion_general, false);
			// aqui termina
			//aqui empiza a buscar el nombre de la division
			this.division_general.push(codigo_division);
			//console.log(this.division_general);
			this. buscarGrupoServicios(this.division_general, false);
			//qui termina
			//aqui empiza a buscar el nombre del grupo
			this.grupo_general.push(codigo_grupo);
			this.buscarClaseServicios(this.grupo_general, false);
			//aqui termina
			//aqui empiza a buscar el nombre de la clase
			this.clase_general.push(codigo_Clase);
			this.buscarSubclaseServicios(this.clase_general, false);
			this.cuentasSelectSubClaseServicios.push(codigo_final);
			this.seleccionarServicios(this.cuentasSelectSubClaseServicios);
			//aqui termina
			this.searchGeneral = {keywordGeneral: ''};
		},

        //guardar cdp
        guardarCdp: function(){

            if(this.tipoMovimiento == '201'){

                if (this.fecha_cdp < this.fechaMax) {
                    Swal.fire("Atención!","La fecha de CDP debe ser igual o mayor al ultimo CDP creada","warning");
                    return false;
                }

                if(this.fecha_cdp != '' && this.objeto != '' && this.consecutivo != '' && this.detalles.length > 0){

                    Swal.fire({
                            title: 'Esta seguro de guardar?',
                            text: "Guardar cdp en la base de datos, confirmar campos!",
                            icon: 'question',
                            showCancelButton: true,
                            confirmButtonColor: '#3085d6',
                            cancelButtonColor: '#d33',
                            confirmButtonText: 'Si, guardar!'
                        }).then((result) => {
                        if (result.isConfirmed) {
                            var formData = new FormData();

                            for(let i=0; i <= this.detalles.length-1; i++){
                                const val = Object.values(this.detalles[i]).length;

                                for(let x = 0; x <= val; x++){
                                    formData.append("detallesCdp["+i+"][]", Object.values(this.detalles[i])[x]);
                                }

                            }
                            var vig = document.getElementById('vigencia').value;
                            formData.append("consecutivo", this.consecutivo);
                            formData.append("vigencia", vig);
                            formData.append("fecha", this.fecha_cdp);
                            formData.append("solicita", this.solicita);
                            formData.append("objeto", this.objeto);
                            formData.append("totalCdp", this.totalCdp);
                            formData.append("tipoDeMovimiento", this.tipoMovimiento);
                            formData.append("solicitud", this.solicitud);
                            formData.append("areaSolicitante", this.areaSolicitante_cod);

                            axios.post('presupuesto_ccpet/solicitudcdp/ccp-solicitudcdp.php?action=guardarCdp', formData)
                            .then((response) => {
                                console.log(response.data);
                                if(response.data.insertaBien){
                                    
                                    this.consecutivo = response.data.consecutivo;
                                    
                                    Swal.fire({
                                        position: 'top-end',
                                        icon: 'success',
                                        title: 'El cdp se guard&oacute; con Exito',
                                        showConfirmButton: false,
                                        timer: 1500
                                    }).then((response) => {
                                            app.redireccionar();
                                        });
                                }else{

                                    Swal.fire(
                                        'Error!',
                                        'No se pudo guardar.',
                                        'error'
                                    );
                                }

                            });

                        }
                    });

                }else{
                    Swal.fire(
                        'Falta informaci&oacute;n para guardar el cdp.',
                        'Verifique que todos los campos esten diligenciados.',
                        'warning'
                    );
                }
            }else{
                if(document.getElementById('fc_1198971545').value != '' && this.numCdpRev != '' && this.descripcionRev != '' && this.detallesRev.length > 0){

                    Swal.fire({
                            title: 'Esta seguro de reversar?',
                            text: "Reversar cdp en la base de datos, confirmar campos!",
                            icon: 'question',
                            showCancelButton: true,
                            confirmButtonColor: '#3085d6',
                            cancelButtonColor: '#d33',
                            confirmButtonText: 'Si, reversar!'
                        }).then((result) => {
                        if (result.isConfirmed) {
                            var formData = new FormData();

                            for(let i=0; i <= this.detallesRev.length-1; i++){
                                const val = Object.values(this.detallesRev[i]).length;

                                for(let x = 0; x <= val; x++){
                                    formData.append("detallesCdp["+i+"][]", Object.values(this.detallesRev[i])[x]);

                                }
                                formData.append("valorRev["+i+"]", this.valorRev[i]);

                            }

                            /* var vig = document.getElementById('vigencia').value; */
                            const fechat = document.getElementById('fc_1198971545').value;
                            const fechatAr = fechat.split('/');
                            var vig = fechatAr[2];

                            formData.append("consecutivo", this.numCdpRev);
                            formData.append("vigencia", vig);
                            formData.append("fecha", document.getElementById('fc_1198971545').value);
                            formData.append("solicita", this.solicitaCdp);
                            formData.append("objeto", this.descripcionRev);
                            formData.append("totalCdp", 0);
                            formData.append("tipoDeMovimiento", this.tipoMovimiento);

                            axios.post('presupuesto_ccpet/solicitudcdp/ccp-solicitudcdp.php?action=guardarCdp', formData)
                            .then((response) => {

                                if(response.data.insertaBien){
                                    Swal.fire({
                                        position: 'top-end',
                                        icon: 'success',
                                        title: 'El cdp se revers&oacute;; con Exito',
                                        showConfirmButton: false,
                                        timer: 1500
                                    }).then((response) => {
                                            app.redireccionar();
                                        });
                                }else{

                                    Swal.fire(
                                        'Error!',
                                        'No se pudo guardar.',
                                        'error'
                                    );
                                }

                            });

                        }
                    });

                }else{
                    Swal.fire(
                        'Falta informaci&oacute;n para reversar el cdp.',
                        'Verifique que todos los campos esten diligenciados.',
                        'warning'
                    );
                }
            }

        },

        async buscarRubroPresupuestal (){
            let partesRubroPresupuestal = divideRubro(this.rubroPresupuestal);

            const tipoGastos = {
                'funcionamiento': 1,
                'deuda': 2,
                'inversion': 3,
                'comercializacion': 4,
            }

            let claves = Object.keys(partesRubroPresupuestal);

            for(let i = 0; i < claves.length; i++){
                let clave = claves[i];

                if(clave == 'tipoObjeto'){
                    this.tipoGasto = tipoGastos[partesRubroPresupuestal[clave]];
                    await this.cambiaTipoDeGasto();
                }

                if(clave == 'cuenta'){
                    this.cuenta = partesRubroPresupuestal[clave];
                    this.buscarCta();
                }

                if(clave == 'fuente'){
                    this.fuente = partesRubroPresupuestal[clave];
                    this.buscarFuente();
                }

                if(clave == 'bpim'){
                    this.codProyecto = partesRubroPresupuestal[clave];
                    await this.buscarProyecto();
                }

                if(clave == 'programatico'){
                    this.programatico = partesRubroPresupuestal[clave];
                    this.buscarProgramatico(true);
                }



                if(clave == 'seccion_presupuestal'){
                    this.selectUnidadEjecutora = partesRubroPresupuestal[clave];
                }

                if(clave == 'vigencia_gasto'){
                    this.selectVigenciaGasto = partesRubroPresupuestal[clave];
                }

                if(clave == 'medio_pago'){
                    this.selectMedioPago = partesRubroPresupuestal[clave];
                }

            }

            this.rubroPresupuestal = '';

        },

        redireccionar: function(){
            let cdp = 0;
            let vig = 0;
            if(this.tipoMovimiento == '201'){
                cdp = this.consecutivo;
                vig = document.getElementById('vigencia').value;
            }else{
                const fechat = document.getElementById('fc_1198971545').value;
                const fechatAr = fechat.split('/');
                vig = fechatAr[2];
                cdp = this.numCdpRev;
            }


            //const  vig = this.vigencia;
            location.href ="ccp-cdpVisualizar.php?is="+cdp+"&vig="+vig;
        },

        formatonumero: function(valor){
			return new Intl.NumberFormat("en-US", {style: "currency", currency: "USD"}).format(valor);
		},

        searchData: function() {
            let search = this.txtSearch.toLowerCase();
            this.solicitudesCopy = [...this.solicitudes.filter(e=>e[0].toLowerCase().includes(search) || e[14].toLowerCase().includes(search) || e[3].toLowerCase().includes(search))];
            
        }

    }
});
