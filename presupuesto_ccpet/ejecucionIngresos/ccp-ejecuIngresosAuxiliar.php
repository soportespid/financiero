<?php
	require_once '../../comun.inc';
    require '../../funciones.inc';
    require '../../conversor.php';
    session_start();

	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");

    $out = array('error' => false);

    $action = "show";

    if(isset($_GET['action'])){
        $action = $_GET['action'];
    }

    if ($action == "datosFormulario") {

        $vigGasto = $_POST['vigGasto'];
        $secPresupuestal = $_POST['secPresupuestal'];
        $cuenta = $_POST['cuenta'];
        $fuente = $_POST['fuente'];
        $medioPago = $_POST['medioPago'];
        $cpc = $_POST['cpc'];

        preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST['fechaIni'],$fecha);
        $fechaIni = "$fecha[3]-$fecha[2]-$fecha[1]";
        $vigenciaIni = $fecha[3];

        preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST['fechaFin'],$fecha);
        $fechaFin = "$fecha[3]-$fecha[2]-$fecha[1]";
        $vigenciaFin = $fecha[3];

        $auxiliar = array();

        $inicial = 0;

        $sqlPresupuestoInicial = "SELECT valor, vigencia FROM ccpetinicialing WHERE cuenta LIKE '$cuenta' AND seccion_presupuestal LIKE '$secPresupuestal' AND vigencia_gasto LIKE '$vigGasto' AND medio_pago LIKE '$medioPago' AND fuente LIKE '$fuente' AND (vigencia = '$vigenciaIni' OR vigencia = '$vigenciaFin')";
        $resPresupuestoInicial = mysqli_query($linkbd, $sqlPresupuestoInicial);
        while ($rowPresupuestoInicial = mysqli_fetch_row($resPresupuestoInicial)) {

            $datos = array();

            array_push($datos, "T");
            array_push($datos, "Presupuesto inicial");
            array_push($datos, "");
            array_push($datos, $rowPresupuestoInicial[1]);
            array_push($datos, $rowPresupuestoInicial[0]);
            $inicial += $rowPresupuestoInicial[0];
            array_push($auxiliar, $datos);
        }

        $totalAdicion = 0;

        $sqlAdiciones = "SELECT id_acuerdo, fecha, valor FROM ccpet_adiciones WHERE cuenta LIKE '$cuenta' AND tipo_cuenta = 'I' AND seccion_presupuestal LIKE '$secPresupuestal' AND medio_pago LIKE '$medioPago' AND codigo_vigenciag LIKE '$vigGasto' AND fuente LIKE '$fuente' AND estado = 'S' AND fecha BETWEEN '$fechaIni' AND '$fechaFin'";
        $resAdiciones = mysqli_query($linkbd, $sqlAdiciones);
        while ($rowAdiciones = mysqli_fetch_row($resAdiciones)) {

            $datos = array();

            array_push($datos, "D");
            array_push($datos, "Adición");
            array_push($datos, $rowAdiciones[0]);
            array_push($datos, $rowAdiciones[1]);
            array_push($datos, $rowAdiciones[2]);
            $totalAdicion += $rowAdiciones[2];
            array_push($auxiliar, $datos);
        }

        $datos = array();

        array_push($datos, "T");
        array_push($datos, " Total Adición");
        array_push($datos, "");
        array_push($datos, "");
        array_push($datos, $totalAdicion);
        array_push($auxiliar, $datos);

        $totalReduccion = 0;

        $sqlReducciones = "SELECT id_adicion, fecha, valor FROM ccpet_reducciones WHERE cuenta LIKE '$cuenta' AND tipo_cuenta = 'I' AND seccion_presupuestal LIKE '$secPresupuestal' AND medio_pago LIKE '$medioPago' AND codigo_vigenciag LIKE '$vigGasto' AND fuente LIKE '$fuente' AND  estado = 'S' AND fecha BETWEEN '$fechaIni' AND '$fechaFin'";
        $resReducciones = mysqli_query($linkbd, $sqlReducciones);
        while ($rowReducciones = mysqli_fetch_row($resReducciones)) {

            $datos = array();

            array_push($datos, "D");
            array_push($datos, "Reducción");
            array_push($datos, $rowReducciones[0]);
            array_push($datos, $rowReducciones[1]);
            array_push($datos, $rowReducciones[2]);
            $totalReduccion += $rowReducciones[2];
            array_push($auxiliar, $datos);
        }


        $datos = array();

        array_push($datos, "T");
        array_push($datos, " Total Reducción");
        array_push($datos, "");
        array_push($datos, "");
        array_push($datos, $totalReduccion);
        array_push($auxiliar, $datos);


        $datos = array();

        $definitivo = 0;
        $definitivo = $inicial + $totalAdicion - $totalReduccion;

        array_push($datos, "T");
        array_push($datos, "Definitivo");
        array_push($datos, "");
        array_push($datos, "");
        array_push($datos, $definitivo);
        array_push($auxiliar, $datos);

        $totalReservas = 0;
        $totalRecaudosConsulta = 0;

        $sqlReservas = "SELECT tsa.id, tsa.fecha, tsa.valor FROM tesoreservas AS tsa, tesoreservas_det AS tsad WHERE tsa.estado = 'S' AND tsa.id = tsad.id_reserva AND tsa.fecha BETWEEN '$fechaIni' AND '$fechaFin' AND tsad.rubro LIKE '$cuenta' AND fuente LIKE '$fuente'";
        $resReservas = mysqli_query($linkbd, $sqlReservas);
        while ($rowReservas = mysqli_fetch_row($resReservas)) {

            $datos = array();

            array_push($datos, "D");
            array_push($datos, "Reservas");
            array_push($datos, $rowReservas[0]);
            array_push($datos, $rowReservas[1]);
            array_push($datos, $rowReservas[2]);
            $totalRecaudosConsulta += $rowReservas[2];
            array_push($auxiliar, $datos);
        }

        /* $datos = array();

        array_push($datos, "T");
        array_push($datos, " Total Reservas");
        array_push($datos, "");
        array_push($datos, "");
        array_push($datos, $totalReservas);
        array_push($auxiliar, $datos); */



        $sqlReciboCajaPpto = "SELECT prc.idrecibo, trc.fecha, prc.valor FROM pptorecibocajappto prc,tesoreciboscaja trc WHERE prc.cuenta LIKE '$cuenta' AND (prc.vigencia = $vigenciaIni OR prc.vigencia = $vigenciaFin) AND trc.id_recibos=prc.idrecibo AND NOT(trc.estado='N' OR trc.estado='R') AND NOT(prc.tipo='R') AND trc.fecha between '$fechaIni' AND '$fechaFin' AND prc.seccion_presupuestal LIKE '$secPresupuestal' AND prc.medio_pago LIKE '$medioPago' AND prc.vigencia_gasto LIKE '$vigGasto' AND prc.fuente LIKE '$fuente'";
        $resReciboCajaPpto = mysqli_query($linkbd, $sqlReciboCajaPpto);
        while ($rowReciboCajaPpto = mysqli_fetch_row($resReciboCajaPpto)) {

            $datos = array();

            array_push($datos, "D");
            array_push($datos, "Recibo de caja");
            array_push($datos, $rowReciboCajaPpto[0]);
            array_push($datos, $rowReciboCajaPpto[1]);
            array_push($datos, $rowReciboCajaPpto[2]);
            $totalRecaudosConsulta += $rowReciboCajaPpto[2];
            array_push($auxiliar, $datos);
        }

        $sqlSuperavit = "SELECT tsa.id, tsa.fecha, tsad.valor FROM tesosuperavit AS tsa, tesosuperavit_det AS tsad WHERE tsa.estado = 'S' AND tsa.id = tsad.id_tesosuperavit AND tsa.fecha BETWEEN '$fechaIni' AND '$fechaFin' AND tsad.rubro LIKE '$cuenta' AND vigencia_gasto LIKE '$vigGasto' AND tsad.fuente LIKE '$fuente' AND tsad.seccion_presupuestal LIKE '$secPresupuestal' AND tsad.medio_pago LIKE '$medioPago'";
        $resSuperavit = mysqli_query($linkbd, $sqlSuperavit);
        while ($rowSuperavit = mysqli_fetch_row($resSuperavit)) {

            $datos = array();

            array_push($datos, "D");
            array_push($datos, "Superavit");
            array_push($datos, $rowSuperavit[0]);
            array_push($datos, $rowSuperavit[1]);
            array_push($datos, $rowSuperavit[2]);
            $totalRecaudosConsulta += $rowSuperavit[2];
            array_push($auxiliar, $datos);
        }



        //pptosinrecibocajappto
        $sqlSinReciboCajaPpto = "SELECT psrc.idrecibo, tsrc.fecha, psrc.valor FROM pptosinrecibocajappto psrc,tesosinreciboscaja tsrc WHERE psrc.cuenta LIKE '$cuenta' AND (psrc.vigencia=$vigenciaIni OR psrc.vigencia = '$vigenciaFin') AND tsrc.id_recibos=psrc.idrecibo AND NOT(tsrc.estado='N' OR tsrc.estado='R') AND tsrc.fecha between '$fechaIni' AND '$fechaFin' AND psrc.medio_pago LIKE '$medioPago' AND psrc.seccion_presupuestal LIKE '$secPresupuestal' AND psrc.vigencia_gasto LIKE '$vigGasto' AND psrc.fuente LIKE '$fuente'";
        $resSinReciboCajaPpto = mysqli_query($linkbd, $sqlSinReciboCajaPpto);
        while ($rowSinReciboCajaPpto = mysqli_fetch_row($resSinReciboCajaPpto)) {

            $datos = array();

            array_push($datos, "D");
            array_push($datos, "Sin recibo de caja");
            array_push($datos, $rowSinReciboCajaPpto[0]);
            array_push($datos, $rowSinReciboCajaPpto[1]);
            array_push($datos, $rowSinReciboCajaPpto[2]);
            $totalRecaudosConsulta += $rowSinReciboCajaPpto[2];
            array_push($auxiliar, $datos);
        }

        $sqlPptoNotasBanPpto = "SELECT pnb.idrecibo, tnp.fecha, pnb.valor FROM pptonotasbanppto pnb,tesonotasbancarias_cab tnp WHERE pnb.cuenta LIKE '$cuenta' AND (pnb.vigencia = '$vigenciaIni' OR pnb.vigencia = '$vigenciaFin') AND tnp.id_comp=pnb.idrecibo AND NOT(tnp.estado='R' OR tnp.estado='N')  AND (tnp.vigencia = '$vigenciaIni' OR tnp.vigencia = '$vigenciaFin') AND tnp.fecha between '$fechaIni' AND '$fechaFin' AND pnb.seccion_presupuestal LIKE '$secPresupuestal' AND pnb.medio_pago LIKE '$medioPago' AND pnb.vigencia_gasto LIKE '$vigGasto' AND pnb.fuente LIKE '$fuente'";
        $resPptoNotasBanPpto = mysqli_query($linkbd, $sqlPptoNotasBanPpto);
        while ($rowPptoNotasBanPpto = mysqli_fetch_row($resPptoNotasBanPpto)) {

            $datos = array();

            array_push($datos, "D");
            array_push($datos, "Notas bancarias");
            array_push($datos, $rowPptoNotasBanPpto[0]);
            array_push($datos, $rowPptoNotasBanPpto[1]);
            array_push($datos, $rowPptoNotasBanPpto[2]);
            $totalRecaudosConsulta += $rowPptoNotasBanPpto[2];
            array_push($auxiliar, $datos);
        }

        //pptoingtranppto recaudos transferencias
        $sqlRecaudoTransferencia = "SELECT pitp.idrecibo, titp.fecha, pitp.valor FROM pptoingtranppto pitp,tesorecaudotransferencia titp WHERE pitp.cuenta LIKE '$cuenta' AND (pitp.vigencia = '$vigenciaIni' OR pitp.vigencia = '$vigenciaFin') AND pitp.idrecibo=titp.id_recaudo AND NOT(titp.estado='N' OR titp.estado='R') AND titp.fecha between '$fechaIni' AND '$fechaFin' AND pitp.seccion_presupuestal LIKE '$secPresupuestal' AND pitp.medio_pago LIKE '$medioPago' AND pitp.vigencia_gasto LIKE '$vigGasto' AND pitp.fuente LIKE '$fuente'";
        $resRecaudoTransferencia = mysqli_query($linkbd, $sqlRecaudoTransferencia);
        while ($rowRecaudoTransferencia = mysqli_fetch_row($resRecaudoTransferencia)) {

            $datos = array();

            array_push($datos, "D");
            array_push($datos, "Recaudos Transferencias");
            array_push($datos, $rowRecaudoTransferencia[0]);
            array_push($datos, $rowRecaudoTransferencia[1]);
            array_push($datos, $rowRecaudoTransferencia[2]);
            $totalRecaudosConsulta += $rowRecaudoTransferencia[2];
            array_push($auxiliar, $datos);
        }

        $queryspCumaribo = "SELECT TB2.idrecibo, TB1.fecha, TB2.valor from tesosinreciboscajasp TB1,pptosinrecibocajaspppto TB2 WHERE TB1.id_recibos=TB2.idrecibo AND TB1.estado='S' AND (TB2.vigencia = '$vigenciaIni' OR TB2.vigencia = '$vigenciaFin') AND TB1.vigencia=TB2.vigencia AND TB2.cuenta LIKE '$cuenta' AND TB1.fecha between '$fechaIni' AND '$fechaFin' AND TB2.seccion_presupuestal LIKE '$secPresupuestal' AND TB2.medio_pago LIKE '$medioPago' AND TB2.vigencia_gasto LIKE '$vigGasto' AND TB2.fuente LIKE '$fuente'";
        $resspCumaribo = mysqli_query($linkbd, $queryspCumaribo);
        while ($rowspCumaribo = mysqli_fetch_row($resspCumaribo)) {

            $datos = array();

            array_push($datos, "D");
            array_push($datos, "Recaudos servicios publicos Cumaribo");
            array_push($datos, $rowspCumaribo[0]);
            array_push($datos, $rowspCumaribo[1]);
            array_push($datos, $rowspCumaribo[2]);
            $totalRecaudosConsulta += $rowspCumaribo[2];
            array_push($auxiliar, $datos);
        }

        //pptoretencionpago egresos
        $sqlRetencionEgresos = "SELECT prc.idrecibo, trc.fecha, prc.valor FROM pptoretencionpago prc,tesoegresos trc WHERE prc.cuenta LIKE '$cuenta' AND (prc.vigencia = '$vigenciaIni' OR prc.vigencia = '$vigenciaFin') AND trc.id_egreso=prc.idrecibo AND NOT(trc.estado='N') AND trc.fecha between '$fechaIni' AND '$fechaFin' AND trc.tipo_mov='201' AND prc.seccion_presupuestal LIKE '$secPresupuestal' AND prc.medio_pago LIKE '$medioPago' AND prc.vigencia_gasto LIKE '$vigGasto' AND prc.tipo='egreso' AND NOT EXISTS (SELECT 1 FROM tesoegresos tra WHERE tra.id_egreso=trc.id_egreso  AND tra.tipo_mov='401') AND prc.fuente LIKE '$fuente'";
        $resRetencionEgresos = mysqli_query($linkbd, $sqlRetencionEgresos);
        while ($rowRetencionEgresos = mysqli_fetch_row($resRetencionEgresos)) {

            $datos = array();

            array_push($datos, "D");
            array_push($datos, "Egresos");
            array_push($datos, $rowRetencionEgresos[0]);
            array_push($datos, $rowRetencionEgresos[1]);
            array_push($datos, $rowRetencionEgresos[2]);
            $totalRecaudosConsulta += $rowRetencionEgresos[2];
            array_push($auxiliar, $datos);
        }

        //pptoretencionotrosegresos otros egresos
        $sqlRetencionEgresos = "SELECT prc.idrecibo, trc.fecha, prc.valor FROM pptoretencionotrosegresos prc,tesopagotercerosvigant trc WHERE prc.cuenta LIKE '$cuenta' AND (prc.vigencia = '$vigenciaIni' OR prc.vigencia = '$vigenciaFin') AND trc.id_pago=prc.idrecibo AND NOT(trc.estado='N') AND trc.fecha between '$fechaIni' AND '$fechaFin' AND prc.seccion_presupuestal LIKE '$secPresupuestal' AND prc.medio_pago LIKE '$medioPago' AND prc.vigencia_gasto LIKE '$vigGasto' AND prc.tipo='egreso' AND prc.fuente LIKE '$fuente'";
        $resRetencionEgresos = mysqli_query($linkbd, $sqlRetencionEgresos);
        while ($rowRetencionEgresos = mysqli_fetch_row($resRetencionEgresos)) {

            $datos = array();

            array_push($datos, "D");
            array_push($datos, "Otros Egresos");
            array_push($datos, $rowRetencionEgresos[0]);
            array_push($datos, $rowRetencionEgresos[1]);
            array_push($datos, $rowRetencionEgresos[2]);
            $totalRecaudosConsulta += $rowRetencionEgresos[2];
            array_push($auxiliar, $datos);
        }


        //pptoretencionpago Orden pago
        $sqlRetencionOrdenPago = "SELECT prc.idrecibo, trc.fecha, prc.valor FROM pptoretencionpago prc,tesoordenpago trc WHERE prc.cuenta LIKE '$cuenta' AND (prc.vigencia='$vigenciaIni' OR prc.vigencia = '$vigenciaFin') AND trc.id_orden=prc.idrecibo AND NOT(trc.estado='N') AND trc.fecha between '$fechaIni' AND '$fechaFin' AND trc.tipo_mov='201' AND prc.seccion_presupuestal LIKE '$secPresupuestal' AND prc.medio_pago LIKE '$medioPago' AND prc.vigencia_gasto LIKE '$vigGasto' AND prc.fuente LIKE '$fuente' AND prc.tipo='orden' AND NOT EXISTS (SELECT 1 FROM tesoordenpago tca WHERE tca.id_orden=trc.id_orden  AND tca.tipo_mov='401')";
        $resRetencionOrdenPago = mysqli_query($linkbd, $sqlRetencionOrdenPago);
        while ($rowRetencionOrdenPago = mysqli_fetch_row($resRetencionOrdenPago)) {

            $datos = array();

            array_push($datos, "D");
            array_push($datos, "Orden Pago");
            array_push($datos, $rowRetencionOrdenPago[0]);
            array_push($datos, $rowRetencionOrdenPago[1]);
            array_push($datos, $rowRetencionOrdenPago[2]);
            $totalRecaudosConsulta += $rowRetencionOrdenPago[2];
            array_push($auxiliar, $datos);
        }

        //srv_recaudo_factura_ppto
        $sqlSP = "SELECT prc.codigo_recaudo, trc.fecha_recaudo, prc.valor FROM srv_recaudo_factura_ppto prc,srv_recaudo_factura trc WHERE prc.cuenta LIKE '$cuenta' AND (prc.vigencia = '$vigenciaIni' OR prc.vigencia = '$vigenciaFin') AND trc.codigo_recaudo=prc.codigo_recaudo AND NOT(trc.estado='REVERSADO') AND trc.fecha_recaudo between '$fechaIni' AND '$fechaFin' AND prc.seccion_presupuestal LIKE '$secPresupuestal' AND prc.medio_pago LIKE '$medioPago' AND prc.vigencia_gasto LIKE '$vigGasto' AND prc.fuente LIKE '$fuente' ";
        $resSP = mysqli_query($linkbd, $sqlSP);
        while ($rowSP = mysqli_fetch_row($resSP)) {

            $datos = array();

            array_push($datos, "D");
            array_push($datos, "Recaudo servicios públicos");
            array_push($datos, $rowSP[0]);
            array_push($datos, $rowSP[1]);
            array_push($datos, $rowSP[2]);
            $totalRecaudosConsulta += $rowSP[2];
            array_push($auxiliar, $datos);
        }

        //sp antiguo cubarral
        $sqlSPAntiguo = "SELECT servreciboscaja_det.id_recibos, servreciboscaja.fecha, servreciboscaja_det.valor FROM servreciboscaja_det, servreciboscaja WHERE servreciboscaja.estado =  'S' AND servreciboscaja.id_recibos = servreciboscaja_det.id_recibos AND servreciboscaja_det.cuentapres LIKE '$cuenta' AND servreciboscaja.fecha BETWEEN '$fechaIni' AND '$fechaFin' AND servreciboscaja_det.seccion_presupuestal LIKE '$secPresupuestal' AND servreciboscaja_det.medio_pago LIKE '$medioPago' AND servreciboscaja_det.vigencia_gasto LIKE '$vigGasto' AND servreciboscaja_det.fuente LIKE '$fuente'";
        $resSPAntiguo = mysqli_query($linkbd, $sqlSPAntiguo);
        while ($rowSPAntiguo = mysqli_fetch_row($resSPAntiguo)) {

            $datos = array();

            array_push($datos, "D");
            array_push($datos, "Recaudo servicios públicos");
            array_push($datos, $rowSPAntiguo[0]);
            array_push($datos, $rowSPAntiguo[1]);
            array_push($datos, $rowSPAntiguo[2]);
            $totalRecaudosConsulta += $rowSPAntiguo[2];
            array_push($auxiliar, $datos);
        }

        $datos = array();

        array_push($datos, "T");
        array_push($datos, " Total Recaudos consulta");
        array_push($datos, "");
        array_push($datos, "");
        array_push($datos, $totalRecaudosConsulta);
        array_push($auxiliar, $datos);

        $datos = array();

        $totalRecaudos = $totalSuperavit + $totalReservas + $totalRecaudosConsulta;

        array_push($datos, "T");
        array_push($datos, " Total Recaudos");
        array_push($datos, "");
        array_push($datos, "");
        array_push($datos, $totalRecaudos);
        array_push($auxiliar, $datos);

        $datos = array();

        $saldoPorRecaudar = $definitivo - $totalRecaudos;

        array_push($datos, "T");
        array_push($datos, " Saldo por recaudar");
        array_push($datos, "");
        array_push($datos, "");
        array_push($datos, $saldoPorRecaudar);
        array_push($auxiliar, $datos);

        $out['auxiliar'] = $auxiliar;
    }


    header("Content-type: application/json");
    echo json_encode($out);
    die();
