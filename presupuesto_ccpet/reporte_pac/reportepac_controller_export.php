<?php
    require_once '../../Librerias/core/Helpers.php';
    require_once '../../PHPExcel/Classes/PHPExcel.php';
    require_once '../../tcpdf/tcpdf_include.php';
    require_once '../../tcpdf/tcpdf.php';
    session_start();
    ini_set('display_errors', '1');
    ini_set('display_startup_errors', '1');
    error_reporting(E_ALL & ~E_DEPRECATED & ~E_STRICT & ~E_NOTICE);
    class ExportController {
        public function exportExcel(){
            if(!empty($_SESSION)){
                $sumatorias = json_decode($_POST['sumatorias'], true);
                $mesesUnicos = json_decode($_POST['mesesUnicos'], true);

                if (! (empty($sumatorias) || empty($mesesUnicos))) {
                    $arrFechaInicial = explode("-",$_POST['fecha_inicial']);
                    $arrFechaFinal = explode("-",$_POST['fecha_final']);
                    $strFechaInicial = $arrFechaInicial[2]."/".$arrFechaInicial[1]."/".$arrFechaInicial[0];
                    $strFechaFinal = $arrFechaFinal[2]."/".$arrFechaFinal[1]."/".$arrFechaFinal[0];

                    $objPHPExcel = new PHPExcel();
                    $objPHPExcel->getActiveSheet()->getStyle('A:P')->applyFromArray(
                        array(
                            'alignment' => array(
                                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
                                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
                            )
                        )
                    );
                    $objPHPExcel->getProperties()
                    ->setCreator("IDEAL 10")
                    ->setLastModifiedBy("IDEAL 10")
                    ->setTitle("Exportar Excel con PHP")
                    ->setSubject("Documento de prueba")
                    ->setDescription("Documento generado con PHPExcel")
                    ->setKeywords("usuarios phpexcel")
                    ->setCategory("reportes");
                    //----Cuerpo de Documento----
                    $objPHPExcel->setActiveSheetIndex(0)
                    ->mergeCells('A1:P1')
                    ->mergeCells('A2:P2')
                    ->setCellValue('A1', 'PRESUPUESTO')
                    ->setCellValue('A2', 'REPORTE PAC');

                    $objPHPExcel-> getActiveSheet ()
                    -> getStyle ("A1")
                    -> getFill ()
                    -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
                    -> getStartColor ()
                    -> setRGB ('C8C8C8');
                    $objPHPExcel-> getActiveSheet ()
                    -> getStyle ("A1:A2")
                    -> getFont ()
                    -> setBold ( true )
                    -> setName ( 'Verdana' )
                    -> setSize ( 10 )
                    -> getColor ()
                    -> setRGB ('000000');
                    $objPHPExcel-> getActiveSheet ()
                    -> getStyle ('A1:A2')
                    -> getAlignment ()
                    -> applyFromArray (array ( 'horizontal'  =>  PHPExcel_Style_Alignment :: HORIZONTAL_CENTER ,) );
                    $objPHPExcel-> getActiveSheet ()
                    -> getStyle ('A3:P3')
                    -> getAlignment ()
                    -> applyFromArray (array ( 'horizontal'  =>  PHPExcel_Style_Alignment :: VERTICAL_JUSTIFY ,) );
                    $objPHPExcel-> getActiveSheet ()
                    -> getStyle ("A2")
                    -> getFill ()
                    -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID);

                    $borders = array(
                        'borders' => array(
                            'allborders' => array(
                                'style' => PHPExcel_Style_Border::BORDER_THIN,
                            'color' => array('argb' => 'FF000000'),
                            )
                        ),
                    );
                    $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('B3', $strFechaInicial)
                    ->setCellValue('B4', $strFechaFinal);

                    $objPHPExcel->setActiveSheetIndex(0)
                    ->mergeCells('B3:P3')
                    ->mergeCells('B4:P4')
                    ->setCellValue('A3', "Fecha inicial")
                    ->setCellValue('A4', "Fecha final");

                    $objPHPExcel->setActiveSheetIndex(0)
                        ->mergeCells('A5:P5')
                        ->setCellValue('A5', "RECAUDOS");

                    // Centrar el texto horizontal y verticalmente
                    $objPHPExcel->getActiveSheet()
                        ->getStyle('A5')
                        ->getAlignment()
                        ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)
                        ->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                        $objPHPExcel->setActiveSheetIndex(0)
                        ->mergeCells('A5:P5')
                        ->setCellValue('A5', "RECAUDOS");

                    // Centrar el texto horizontal y verticalmente
                    $objPHPExcel->getActiveSheet()
                        ->getStyle('A5')
                        ->getAlignment()
                        ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)
                        ->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);

                    $worksheet = $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A6', 'Nombre fuente')
                    ->setCellValue('B6', "Fuente");

                    $i = 0;
                    foreach ($mesesUnicos as $mes) {
                        $worksheet->setCellValue(chr(67 + $i) . '6', $mes);
                        $i++;
                    }

                    $worksheet->setCellValue(chr(67 + $i) . '6', 'Total');

                    /*
                    ->setCellValue('C6', "Nombre fuente")
                    ->setCellValue('D6', "Ingresos")
                    ->setCellValue('E6', "Gastos")
                    ->setCellValue('F6', "Ingresos")
                    ->setCellValue('G6', "Gastos")
                    ->setCellValue('H6', "Ingresos")
                    ->setCellValue('I6', "Gastos")
                    ->setCellValue('J6', "Crédito")
                    ->setCellValue('K6', "Contracrédito")
                    ->setCellValue('L6', "Ingresos")
                    ->setCellValue('M6', "Gastos")
                    ->setCellValue('N6', "Recaudo")
                    ->setCellValue('O6', "Pago")
                    ->setCellValue('P6', "Diferencia (Recaudo - Pago)");*/
                    $objPHPExcel-> getActiveSheet ()
                        -> getStyle ("A3")
                        -> getFill ()
                        -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
                        -> getStartColor ()
                        -> setRGB ('3399cc');
                    $objPHPExcel-> getActiveSheet ()
                        -> getStyle ("A4")
                        -> getFill ()
                        -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
                        -> getStartColor ()
                        -> setRGB ('3399cc');
                    $objPHPExcel-> getActiveSheet ()
                        -> getStyle ("A5:P5")
                        -> getFill ()
                        -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
                        -> getStartColor ()
                        -> setRGB ('3399cc');
                    $objPHPExcel-> getActiveSheet ()
                    -> getStyle ("A6:P6")
                    -> getFill ()
                    -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
                    -> getStartColor ()
                    -> setRGB ('99ddff');
                    $objPHPExcel->getActiveSheet()->getStyle('A1:P1')->applyFromArray($borders);
                    $objPHPExcel->getActiveSheet()->getStyle('A2:P2')->applyFromArray($borders);
                    $objPHPExcel->getActiveSheet()->getStyle('A3:P3')->applyFromArray($borders);
                    $objPHPExcel->getActiveSheet()->getStyle('A4:P4')->applyFromArray($borders);
                    $objPHPExcel->getActiveSheet()->getStyle('A5:P5')->applyFromArray($borders);
                    $objPHPExcel->getActiveSheet()->getStyle('A6:P6')->applyFromArray($borders);

                    $objPHPExcel->getActiveSheet()->getStyle('A3')->getFont()->getColor()->setRGB("ffffff");
                    $objPHPExcel->getActiveSheet()->getStyle('A4')->getFont()->getColor()->setRGB("ffffff");
                    $objPHPExcel->getActiveSheet()->getStyle('A5:P5')->getFont()->getColor()->setRGB("ffffff");

                    $objPHPExcel->getActiveSheet()->getStyle("A3")->getFont()->setBold(true);
                    $objPHPExcel->getActiveSheet()->getStyle("A4")->getFont()->setBold(true);


                    $objPHPExcel->getActiveSheet()->getStyle("A5")->getFont()->setBold(true);
                    $objPHPExcel->getActiveSheet()->getStyle("B5")->getFont()->setBold(true);
                    $objPHPExcel->getActiveSheet()->getStyle("C5")->getFont()->setBold(true);
                    $objPHPExcel->getActiveSheet()->getStyle("D5")->getFont()->setBold(true);
                    $objPHPExcel->getActiveSheet()->getStyle("E5")->getFont()->setBold(true);
                    $objPHPExcel->getActiveSheet()->getStyle("F5")->getFont()->setBold(true);
                    $objPHPExcel->getActiveSheet()->getStyle("G5")->getFont()->setBold(true);
                    $objPHPExcel->getActiveSheet()->getStyle("H5")->getFont()->setBold(true);
                    $objPHPExcel->getActiveSheet()->getStyle("I5")->getFont()->setBold(true);
                    $objPHPExcel->getActiveSheet()->getStyle("J5")->getFont()->setBold(true);
                    $objPHPExcel->getActiveSheet()->getStyle("K5")->getFont()->setBold(true);
                    $objPHPExcel->getActiveSheet()->getStyle("L5")->getFont()->setBold(true);
                    $objPHPExcel->getActiveSheet()->getStyle("M5")->getFont()->setBold(true);
                    $objPHPExcel->getActiveSheet()->getStyle("N5")->getFont()->setBold(true);
                    $objPHPExcel->getActiveSheet()->getStyle("O5")->getFont()->setBold(true);
                    $objPHPExcel->getActiveSheet()->getStyle("P5")->getFont()->setBold(true);

                    $objPHPExcel->getActiveSheet()->getStyle("A6")->getFont()->setBold(true);
                    $objPHPExcel->getActiveSheet()->getStyle("B6")->getFont()->setBold(true);
                    $objPHPExcel->getActiveSheet()->getStyle("C6")->getFont()->setBold(true);
                    $objPHPExcel->getActiveSheet()->getStyle("D6")->getFont()->setBold(true);
                    $objPHPExcel->getActiveSheet()->getStyle("E6")->getFont()->setBold(true);
                    $objPHPExcel->getActiveSheet()->getStyle("F6")->getFont()->setBold(true);
                    $objPHPExcel->getActiveSheet()->getStyle("G6")->getFont()->setBold(true);
                    $objPHPExcel->getActiveSheet()->getStyle("H6")->getFont()->setBold(true);
                    $objPHPExcel->getActiveSheet()->getStyle("I6")->getFont()->setBold(true);
                    $objPHPExcel->getActiveSheet()->getStyle("J6")->getFont()->setBold(true);
                    $objPHPExcel->getActiveSheet()->getStyle("K6")->getFont()->setBold(true);
                    $objPHPExcel->getActiveSheet()->getStyle("L6")->getFont()->setBold(true);
                    $objPHPExcel->getActiveSheet()->getStyle("M6")->getFont()->setBold(true);
                    $objPHPExcel->getActiveSheet()->getStyle("N6")->getFont()->setBold(true);
                    $objPHPExcel->getActiveSheet()->getStyle("O6")->getFont()->setBold(true);
                    $objPHPExcel->getActiveSheet()->getStyle("P6")->getFont()->setBold(true);
                    $row = 7;

                    foreach ($sumatorias as $fuente) {
                        $worksheet = $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValueExplicit ("A$row", $fuente['nombre'], PHPExcel_Cell_DataType::TYPE_STRING)
                        ->setCellValueExplicit ("B$row", $fuente['codigo'], PHPExcel_Cell_DataType::TYPE_STRING);

                        $i = 0;
                        foreach ($mesesUnicos as $mes) {
                            $worksheet->setCellValueExplicit(chr(67 + $i) . $row, $fuente['recaudos'][$mes], PHPExcel_Cell_DataType::TYPE_NUMERIC);
                            $i++;
                        }

                        $worksheet->setCellValueExplicit(chr(67 + $i) . $row, $fuente['sumaRecaudos'], PHPExcel_Cell_DataType::TYPE_NUMERIC);

                        //->setCellValueExplicit ("C$row", $data['vigencia'], PHPExcel_Cell_DataType :: TYPE_STRING)
                        //->setCellValueExplicit ("D$row", $data['recaudo'], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
                        //->setCellValueExplicit ("E$row", $data['pago'], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
                        //->setCellValueExplicit ("F$row", $data['diferencia'], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
                        //->setCellValueExplicit ("G$row", $data['diferencia_estado'], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
                        //->setCellValueExplicit ("H$row", $data['mes'], PHPExcel_Cell_DataType :: TYPE_NUMERIC);
                        /*$objPHPExcel->getActiveSheet()->getStyle("A$row:P$row")->applyFromArray($borders);
                        if($data['inicial_estado']== 0){
                            $objPHPExcel-> getActiveSheet ()
                            -> getStyle ("D$row:E$row")
                            -> getFill ()
                            -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
                            -> getStartColor ()
                            -> setRGB ('FF0000');
                            $objPHPExcel->getActiveSheet()->getStyle("D$row:E$row")->getFont()->getColor()->setRGB("ffffff");
                        }
                        if($data['adiciones_estado']== 0){
                            $objPHPExcel-> getActiveSheet ()
                            -> getStyle ("F$row:G$row")
                            -> getFill ()
                            -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
                            -> getStartColor ()
                            -> setRGB ('FF0000');
                            $objPHPExcel->getActiveSheet()->getStyle("F$row:G$row")->getFont()->getColor()->setRGB("ffffff");
                        }
                        if($data['reducciones_estado']== 0){
                            $objPHPExcel-> getActiveSheet ()
                            -> getStyle ("H$row:I$row")
                            -> getFill ()
                            -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
                            -> getStartColor ()
                            -> setRGB ('FF0000');
                            $objPHPExcel->getActiveSheet()->getStyle("H$row:I$row")->getFont()->getColor()->setRGB("ffffff");
                        }
                        if($data['traslados_estado']== 0){
                            $objPHPExcel-> getActiveSheet ()
                            -> getStyle ("J$row:K$row")
                            -> getFill ()
                            -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
                            -> getStartColor ()
                            -> setRGB ('FF0000');
                            $objPHPExcel->getActiveSheet()->getStyle("J$row:k$row")->getFont()->getColor()->setRGB("ffffff");
                        }
                        if($data['definitivo_estado']== 0){
                            $objPHPExcel-> getActiveSheet ()
                            -> getStyle ("L$row:M$row")
                            -> getFill ()
                            -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
                            -> getStartColor ()
                            -> setRGB ('FF0000');
                            $objPHPExcel->getActiveSheet()->getStyle("L$row:M$row")->getFont()->getColor()->setRGB("ffffff");
                        }
                        if($data['diferencia_estado']== 0){
                            $objPHPExcel-> getActiveSheet ()
                            -> getStyle ("P$row")
                            -> getFill ()
                            -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
                            -> getStartColor ()
                            -> setRGB ('FF0000');
                            $objPHPExcel->getActiveSheet()->getStyle("P$row")->getFont()->getColor()->setRGB("ffffff");
                        }*/
                        $row++;
                    }

                    $objPHPExcel->setActiveSheetIndex(0)
                        ->mergeCells("A$row:P$row")
                        ->setCellValue("A$row", "PAGOS");

                    // Centrar el texto horizontal y verticalmente
                    $objPHPExcel->getActiveSheet()
                        ->getStyle("A$row")
                        ->getAlignment()
                        ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)
                        ->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                        $objPHPExcel->setActiveSheetIndex(0)
                        ->mergeCells("A$row:P$row")
                        ->setCellValue("A$row", "PAGOS");

                    // Centrar el texto horizontal y verticalmente
                    $objPHPExcel->getActiveSheet()
                        ->getStyle("A$row")
                        ->getAlignment()
                        ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)
                        ->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                        $objPHPExcel-> getActiveSheet ()
                        -> getStyle ("A$row:P$row")
                        -> getFill ()
                        -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
                        -> getStartColor ()
                        -> setRGB ('3399cc');

                    $objPHPExcel->getActiveSheet()->getStyle("A$row:P$row")->applyFromArray($borders);
                    $objPHPExcel->getActiveSheet()->getStyle("A$row:P$row")->getFont()->getColor()->setRGB("ffffff");
                    $objPHPExcel->getActiveSheet()->getStyle("A$row")->getFont()->setBold(true);
                    $objPHPExcel->getActiveSheet()->getStyle("B$row")->getFont()->setBold(true);
                    $objPHPExcel->getActiveSheet()->getStyle("C$row")->getFont()->setBold(true);
                    $objPHPExcel->getActiveSheet()->getStyle("D$row")->getFont()->setBold(true);
                    $objPHPExcel->getActiveSheet()->getStyle("E$row")->getFont()->setBold(true);
                    $objPHPExcel->getActiveSheet()->getStyle("F$row")->getFont()->setBold(true);
                    $objPHPExcel->getActiveSheet()->getStyle("G$row")->getFont()->setBold(true);
                    $objPHPExcel->getActiveSheet()->getStyle("H$row")->getFont()->setBold(true);
                    $objPHPExcel->getActiveSheet()->getStyle("I$row")->getFont()->setBold(true);
                    $objPHPExcel->getActiveSheet()->getStyle("J$row")->getFont()->setBold(true);
                    $objPHPExcel->getActiveSheet()->getStyle("K$row")->getFont()->setBold(true);
                    $objPHPExcel->getActiveSheet()->getStyle("L$row")->getFont()->setBold(true);
                    $objPHPExcel->getActiveSheet()->getStyle("M$row")->getFont()->setBold(true);
                    $objPHPExcel->getActiveSheet()->getStyle("N$row")->getFont()->setBold(true);
                    $objPHPExcel->getActiveSheet()->getStyle("O$row")->getFont()->setBold(true);
                    $objPHPExcel->getActiveSheet()->getStyle("P$row")->getFont()->setBold(true);

                    $row++;

                    $objPHPExcel->getActiveSheet()->getStyle("A$row:P$row")->applyFromArray($borders);
                    $objPHPExcel->getActiveSheet()->getStyle("A$row")->getFont()->setBold(true);
                    $objPHPExcel->getActiveSheet()->getStyle("B$row")->getFont()->setBold(true);
                    $objPHPExcel->getActiveSheet()->getStyle("C$row")->getFont()->setBold(true);
                    $objPHPExcel->getActiveSheet()->getStyle("D$row")->getFont()->setBold(true);
                    $objPHPExcel->getActiveSheet()->getStyle("E$row")->getFont()->setBold(true);
                    $objPHPExcel->getActiveSheet()->getStyle("F$row")->getFont()->setBold(true);
                    $objPHPExcel->getActiveSheet()->getStyle("G$row")->getFont()->setBold(true);
                    $objPHPExcel->getActiveSheet()->getStyle("H$row")->getFont()->setBold(true);
                    $objPHPExcel->getActiveSheet()->getStyle("I$row")->getFont()->setBold(true);
                    $objPHPExcel->getActiveSheet()->getStyle("J$row")->getFont()->setBold(true);
                    $objPHPExcel->getActiveSheet()->getStyle("K$row")->getFont()->setBold(true);
                    $objPHPExcel->getActiveSheet()->getStyle("L$row")->getFont()->setBold(true);
                    $objPHPExcel->getActiveSheet()->getStyle("M$row")->getFont()->setBold(true);
                    $objPHPExcel->getActiveSheet()->getStyle("N$row")->getFont()->setBold(true);
                    $objPHPExcel->getActiveSheet()->getStyle("O$row")->getFont()->setBold(true);
                    $objPHPExcel->getActiveSheet()->getStyle("P$row")->getFont()->setBold(true);

                    $worksheet = $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue("A$row", 'Nombre fuente')
                    ->setCellValue("B$row", "Fuente");

                    $objPHPExcel-> getActiveSheet ()
                    -> getStyle ("A$row:P$row")
                    -> getFill ()
                    -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
                    -> getStartColor ()
                    -> setRGB ('99ddff');

                    $i = 0;
                    foreach ($mesesUnicos as $mes) {
                        $worksheet->setCellValue(chr(67 + $i) . $row, $mes);
                        $i++;
                    }

                    $worksheet->setCellValue(chr(67 + $i) . $row, 'Total');

                    $row++;

                    foreach ($sumatorias as $fuente) {
                        $worksheet = $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValueExplicit ("A$row", $fuente['nombre'], PHPExcel_Cell_DataType::TYPE_STRING)
                        ->setCellValueExplicit ("B$row", $fuente['codigo'], PHPExcel_Cell_DataType::TYPE_STRING);

                        $i = 0;
                        foreach ($mesesUnicos as $mes) {
                            $worksheet->setCellValueExplicit(chr(67 + $i) . $row, $fuente['pagos'][$mes], PHPExcel_Cell_DataType::TYPE_NUMERIC);
                            $i++;
                        }

                        $worksheet->setCellValueExplicit(chr(67 + $i) . $row, $fuente['sumaPagos'], PHPExcel_Cell_DataType::TYPE_NUMERIC);

                        $row++;
                    }

                    $objPHPExcel->setActiveSheetIndex(0)
                        ->mergeCells("A$row:P$row")
                        ->setCellValue("A$row", "DIFERENCIAS");

                    // Centrar el texto horizontal y verticalmente
                    $objPHPExcel->getActiveSheet()
                        ->getStyle("A$row")
                        ->getAlignment()
                        ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)
                        ->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                        $objPHPExcel->setActiveSheetIndex(0)
                        ->mergeCells("A$row:P$row")
                        ->setCellValue("A$row", "DIFERENCIAS");

                    // Centrar el texto horizontal y verticalmente
                    $objPHPExcel->getActiveSheet()
                        ->getStyle("A$row")
                        ->getAlignment()
                        ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)
                        ->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                        $objPHPExcel-> getActiveSheet ()
                        -> getStyle ("A$row:P$row")
                        -> getFill ()
                        -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
                        -> getStartColor ()
                        -> setRGB ('3399cc');

                    $objPHPExcel->getActiveSheet()->getStyle("A$row:P$row")->applyFromArray($borders);
                    $objPHPExcel->getActiveSheet()->getStyle("A$row:P$row")->getFont()->getColor()->setRGB("ffffff");
                    $objPHPExcel->getActiveSheet()->getStyle("A$row")->getFont()->setBold(true);
                    $objPHPExcel->getActiveSheet()->getStyle("B$row")->getFont()->setBold(true);
                    $objPHPExcel->getActiveSheet()->getStyle("C$row")->getFont()->setBold(true);
                    $objPHPExcel->getActiveSheet()->getStyle("D$row")->getFont()->setBold(true);
                    $objPHPExcel->getActiveSheet()->getStyle("E$row")->getFont()->setBold(true);
                    $objPHPExcel->getActiveSheet()->getStyle("F$row")->getFont()->setBold(true);
                    $objPHPExcel->getActiveSheet()->getStyle("G$row")->getFont()->setBold(true);
                    $objPHPExcel->getActiveSheet()->getStyle("H$row")->getFont()->setBold(true);
                    $objPHPExcel->getActiveSheet()->getStyle("I$row")->getFont()->setBold(true);
                    $objPHPExcel->getActiveSheet()->getStyle("J$row")->getFont()->setBold(true);
                    $objPHPExcel->getActiveSheet()->getStyle("K$row")->getFont()->setBold(true);
                    $objPHPExcel->getActiveSheet()->getStyle("L$row")->getFont()->setBold(true);
                    $objPHPExcel->getActiveSheet()->getStyle("M$row")->getFont()->setBold(true);
                    $objPHPExcel->getActiveSheet()->getStyle("N$row")->getFont()->setBold(true);
                    $objPHPExcel->getActiveSheet()->getStyle("O$row")->getFont()->setBold(true);
                    $objPHPExcel->getActiveSheet()->getStyle("P$row")->getFont()->setBold(true);

                    $row++;

                    $objPHPExcel->getActiveSheet()->getStyle("A$row:P$row")->applyFromArray($borders);
                    $objPHPExcel->getActiveSheet()->getStyle("A$row")->getFont()->setBold(true);
                    $objPHPExcel->getActiveSheet()->getStyle("B$row")->getFont()->setBold(true);
                    $objPHPExcel->getActiveSheet()->getStyle("C$row")->getFont()->setBold(true);
                    $objPHPExcel->getActiveSheet()->getStyle("D$row")->getFont()->setBold(true);
                    $objPHPExcel->getActiveSheet()->getStyle("E$row")->getFont()->setBold(true);
                    $objPHPExcel->getActiveSheet()->getStyle("F$row")->getFont()->setBold(true);
                    $objPHPExcel->getActiveSheet()->getStyle("G$row")->getFont()->setBold(true);
                    $objPHPExcel->getActiveSheet()->getStyle("H$row")->getFont()->setBold(true);
                    $objPHPExcel->getActiveSheet()->getStyle("I$row")->getFont()->setBold(true);
                    $objPHPExcel->getActiveSheet()->getStyle("J$row")->getFont()->setBold(true);
                    $objPHPExcel->getActiveSheet()->getStyle("K$row")->getFont()->setBold(true);
                    $objPHPExcel->getActiveSheet()->getStyle("L$row")->getFont()->setBold(true);
                    $objPHPExcel->getActiveSheet()->getStyle("M$row")->getFont()->setBold(true);
                    $objPHPExcel->getActiveSheet()->getStyle("N$row")->getFont()->setBold(true);
                    $objPHPExcel->getActiveSheet()->getStyle("O$row")->getFont()->setBold(true);
                    $objPHPExcel->getActiveSheet()->getStyle("P$row")->getFont()->setBold(true);

                    $worksheet = $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue("A$row", 'Nombre fuente')
                    ->setCellValue("B$row", "Fuente");

                    $objPHPExcel-> getActiveSheet ()
                    -> getStyle ("A$row:P$row")
                    -> getFill ()
                    -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
                    -> getStartColor ()
                    -> setRGB ('99ddff');

                    $i = 0;
                    foreach ($mesesUnicos as $mes) {
                        $worksheet->setCellValue(chr(67 + $i) . $row, $mes);
                        $i++;
                    }

                    $worksheet->setCellValue(chr(67 + $i) . $row, 'Total');

                    $row++;

                    foreach ($sumatorias as $fuente) {
                        $worksheet = $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValueExplicit ("A$row", $fuente['nombre'], PHPExcel_Cell_DataType::TYPE_STRING)
                        ->setCellValueExplicit ("B$row", $fuente['codigo'], PHPExcel_Cell_DataType::TYPE_STRING);

                        $i = 0;
                        foreach ($mesesUnicos as $mes) {
                            $worksheet->setCellValueExplicit(chr(67 + $i) . $row, $fuente['diferencias'][$mes], PHPExcel_Cell_DataType::TYPE_NUMERIC);
                            $i++;
                        }

                        $worksheet->setCellValueExplicit(chr(67 + $i) . $row, $fuente['sumaDiferencias'], PHPExcel_Cell_DataType::TYPE_NUMERIC);

                        $row++;
                    }

                    $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
                    $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
                    $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
                    $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
                    $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
                    $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
                    $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
                    $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
                    $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
                    $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);
                    $objPHPExcel->getActiveSheet()->getColumnDimension('K')->setAutoSize(true);
                    $objPHPExcel->getActiveSheet()->getColumnDimension('L')->setAutoSize(true);
                    $objPHPExcel->getActiveSheet()->getColumnDimension('M')->setAutoSize(true);
                    $objPHPExcel->getActiveSheet()->getColumnDimension('N')->setAutoSize(true);
                    $objPHPExcel->getActiveSheet()->getColumnDimension('O')->setAutoSize(true);
                    $objPHPExcel->getActiveSheet()->getColumnDimension('P')->setAutoSize(true);
                    header('Content-Type: application/vnd.ms-excel');
                    header('Content-Disposition: attachment;filename="reporte_fuente_de_recursos.xlsx"');
                    header('Cache-Control: max-age=0');
                    $objWriter=PHPExcel_IOFactory::createWriter($objPHPExcel,'Excel2007');
                    $objWriter->save('php://output');
                }
            }
            die();
        }
    }
    if($_POST){
        $obj = new ExportController();
        if($_POST['action'] == "excel"){
            $obj->exportExcel();
        }else if($_POST['action'] == "pdf"){
            $obj->exportPdf();
        }
    }

?>
