const URL ='presupuesto_ccpet/reporte_pac/ccp-reportepac.php';
const URLEXPORT = 'presupuesto_ccpet/reporte_pac/reportepac_controller_export.php';

var app = new Vue({
    el:"#myapp",
    data() {
        return {
            isLoading:false,
            isModal:false,
            txtSearch:"",
            isCheckAll:1,
            txtResultados:0,
            arrFuentes:[],
            arrFuentesCopy:[],
            txtFechaInicial:new Date(new Date().getFullYear(),0,1).toISOString().split("T")[0],
            txtFechaFinal:new Date().toISOString().split("T")[0],
            selectGasto:"",
            arrData: [],
            arrDataIngresos:[],
            mesesUnicos:[],
            sumatorias:[],
        }
    },
    mounted() {
        this.getData();
    },
    methods: {

        getData: async function(){
            const formData = new FormData();
            formData.append("action","get");
            formData.append("sumatorias",this.sumatorias);
            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();
            this.arrFuentes = objData.fuentes;
            this.arrFuentesCopy = objData.fuentes;
            this.txtResultados = this.arrFuentesCopy.length;

        },
        search:function(type=""){
            let search = "";
            if(type == "modal")search = this.txtSearch.toLowerCase();
            if(type=="modal"){
                this.arrFuentesCopy = [...this.arrFuentes.filter(e=>e.codigo.toLowerCase().includes(search) || e.nombre.toLowerCase().includes(search))];
                this.txtResultados = this.arrFuentesCopy.length;
            }
        },
        generate: async function(){
            const arrFuentes = this.arrFuentes.filter(e=>e.is_checked);
            const formData = new FormData();
            formData.append("action","gen");
            formData.append("fecha_inicial",this.txtFechaInicial);
            formData.append("fecha_final",this.txtFechaFinal);
            formData.append("gasto",this.selectGasto);
            formData.append("fuentes",JSON.stringify(arrFuentes));
            this.isLoading = true;
            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();
            this.isLoading = false;
            this.arrData = objData;
            this.arrDataIngresos = await this.agruparPorMesFuentes(objData);
            this.sumatorias = this.sumarTotales(this.arrDataIngresos);
            console.log(this.arrDataIngresos);
        },
        sumarTotales:function(datos){
            return datos.map(fuente => {
                const sumaRecaudos = Object.values(fuente.recaudos).reduce((acc, val) => acc + val, 0);
                const sumaPagos = Object.values(fuente.pagos).reduce((acc, val) => acc + val, 0);
                const sumaDiferencias = Object.values(fuente.diferencias).reduce((acc, val) => acc + val, 0);

                return {
                  ...fuente,
                  sumaRecaudos,
                  sumaPagos,
                  sumaDiferencias
                };
            });
        },
                // Se suman los valores
        agruparPorMesFuentes: async function(datos){
            console.log(datos);
            this.mesesUnicos = [...new Set(datos.map(item => item.mes))];
            this.mesesUnicos.sort();
            const resultado = {};

            datos.forEach(({ codigo, nombre, mes, recaudo, pago }) => {
                if (!resultado[codigo]) {
                  resultado[codigo] = { nombre, codigo, recaudos: {}, pagos: {}, diferencias: {} };
                }
                resultado[codigo].recaudos[mes] = recaudo;
                resultado[codigo].pagos[mes] = pago;
                resultado[codigo].diferencias[mes] = recaudo - pago; // Diferencia
              });

            return Object.values(resultado);
            /* return datos.reduce((resultado, item) => {
                const { mes, codigo, recaudo, pago } = item;

                // Si el mes no existe en el resultado, se crea
                if (!resultado[mes]) {
                    resultado[mes] = {};
                }

                // Si la fuente no existe dentro del mes, se crea
                if (!resultado[mes][codigo]) {
                    resultado[mes][codigo] = { recaudo: 0, pago: 0 };
                }

                // Se suman los valores de recaudo y pago
                resultado[mes][codigo].recaudo += recaudo;
                resultado[mes][codigo].pago += pago;

                return resultado;
            }, {}); */
        },

        changeStatus:function(codigo){
            const index = this.arrFuentes.findIndex(e=>e.codigo == codigo);
            this.arrFuentes[index].is_checked = !this.arrFuentes[index].is_checked;
            const flag = this.arrFuentes.every((e)=>e.is_checked == true);
            this.isCheckAll = flag;
        },
        changeAll:function(){
            this.arrFuentes.forEach(e => {
                e.is_checked = this.isCheckAll;
            });
        },
        exportData:function(type){
            if(app.arrData.length == 0){
                Swal.fire("Atención!","Debe generar el reporte para generar el excel.","warning");
                return false;
            }
            const form = document.createElement("form");
            form.method ="post";
            form.target="_blank";
            form.action= URLEXPORT;

            function addField(name,value){
                const input = document.createElement("input");
                input.type="hidden";
                input.name=name;
                input.value = value;
                form.appendChild(input);
            }

            addField("fecha_inicial",this.txtFechaInicial);
            addField("fecha_final",this.txtFechaFinal);
            addField("action",type == 1 ? "pdf" : "excel");
            addField("sumatorias",JSON.stringify(this.sumatorias));
            addField("mesesUnicos",JSON.stringify(this.mesesUnicos));
            document.body.appendChild(form);
            form.submit();
            document.body.removeChild(form);
        },
        formatNumero: function(valor){
            return new Intl.NumberFormat("en-US", {style: "currency", currency: "USD"}).format(valor);
        },
    },
})
