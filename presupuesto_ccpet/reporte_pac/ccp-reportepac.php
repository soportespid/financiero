<?php
    require_once '../../Librerias/core/Helpers.php';
    require_once '../../PHPExcel/Classes/PHPExcel.php';
    require_once 'ccp-reportepac_model.php';
    session_start();

    class ReporteController extends ReporteModel{
        public function initialData(){
            if(!empty($_SESSION)){
                $arrResponse = array("fuentes"=>$this->selectFuentes_1());
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
            die();
        }
        public function genData(){
            if(!empty($_SESSION)){
                $strFechaInicial = strClean($_POST['fecha_inicial']);
                $strFechaFinal = strClean($_POST['fecha_final']);
                $intVigenciaGasto = strClean($_POST['gasto']);
                $arrFuentes = json_decode($_POST['fuentes'],true);
                $arrData = [];
                if(!empty($arrFuentes)){
                    /* $arrData = $this->selectInicial($strFechaInicial,$strFechaFinal,$intVigenciaGasto,$arrFuentes);
                    $arrData = $this->selectAdiciones($strFechaInicial,$strFechaFinal,$intVigenciaGasto,$arrFuentes,$arrData);
                    $arrData = $this->selectReducciones($strFechaInicial,$strFechaFinal,$intVigenciaGasto,$arrFuentes,$arrData);
                    $arrData = $this->selectTraslados($strFechaInicial,$strFechaFinal,$intVigenciaGasto,$arrFuentes,$arrData); */
                    $arrData = $this->selectRecaudos($strFechaInicial,$strFechaFinal,$intVigenciaGasto,$arrFuentes,$arrData);
                    $arrData = $this->selectPagos($strFechaInicial,$strFechaFinal,$intVigenciaGasto,$arrFuentes,$arrData);
                    $arrData = $this->orderData($arrData);

                    
                }
                echo json_encode($arrData,JSON_UNESCAPED_UNICODE);
            }
            die();
        }
        public function orderData($arrData){
            $arrGastos = array(
                1=>"1 - Vigencia actual",
                2=>"2 - Reservas",
                3=>"3 - Cuentas por pagar",
                4=>"4 - Vigencias futuras - vigencia actual",
                5=>"5 - Vigencias futuras - reservas",
                6=>"6 - Vigencias futuras - cuentas por pagar",
            );
            $arrTempFuentes = array_values(array_unique(array_column($arrData,"llave")));
            $arrTempData = [];
            foreach ($arrTempFuentes as $data) {
                array_push($arrTempData,
                    array_values(array_filter($arrData,function($e)use($data){return $data==$e['llave'];}))
                );
            }
            $arrData = $arrTempData;
            $arrTempData = [];
            $totalData = count($arrData);
            for ($i=0; $i < $totalData; $i++) {
                $data = $arrData[$i];
                $recaudos = 0;
                $pagos201 = 0;
                $pagos401 = 0;
                foreach ($data as $det) {
                    if($det['tipo'] == "ingreso" && $det['dato'] == "recaudo")$recaudos+=$det['total'];
                    if(isset($det['movimiento']) && $det['movimiento']  == "201" && $det['dato'] == "pago")$pagos201+=$det['total'];
                    if(isset($det['movimiento']) && $det['movimiento']  == "401" && $det['dato'] == "pago")$pagos401+=$det['total'];
                }
                $pagos = $pagos201-$pagos401;
                $diferencia = $recaudos-$pagos;
                array_push($arrTempData,array(
                    "nombre"=>$data[0]['nombre'],
                    "codigo"=>$data[0]['codigo'],
                    "vigencia"=>isset($arrGastos[$data[0]['vigencia']]) ? $arrGastos[$data[0]['vigencia']] : "",
                    "recaudo"=>$recaudos,
                    "pago"=>$pagos,
                    "diferencia"=>$diferencia,
                    "diferencia_estado"=>$diferencia >= 0 ? 1 : 0,
                    "mes"=>$data[0]['mes'],
                ));
            }
            $arrData = array_values(array_filter($arrTempData,function($e){return $e['vigencia']!="";}));
            return $arrData;
        }
    }

    if($_POST){
        $obj = new ReporteController();
        if($_POST['action'] == "get"){
            $obj->initialData();
        }else if($_POST['action'] == "gen"){
            $obj->genData();
        }
    }

?>
