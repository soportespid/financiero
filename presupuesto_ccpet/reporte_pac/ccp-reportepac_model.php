<?php
    require_once '../../Librerias/core/Helpers.php';
    require_once '../../Librerias/core/Mysql.php';
    session_start();
    /*ini_set('display_errors', '1');
    ini_set('display_startup_errors', '1');
    error_reporting(E_ALL);*/
    class ReporteModel extends Mysql{
        private $strFechaInicial;
        private $strFechaFinal;
        private $strGasto;
        function __construct(){
            parent::__construct();
        }

        public function selectInicial($strFechaInicial,$strFechaFinal,$strGasto,$arrFuentes){
            $this->strFechaInicial = $strFechaInicial;
            $this->strFechaFinal = $strFechaFinal;
            $this->strGasto = $strGasto;
            $arrData = [];
            $total = count($arrFuentes);
            for ($i=0; $i < $total ; $i++) {
                $partsFuente = explode("_", $arrFuentes[$i]['codigo']);
                $fuente = $partsFuente[0];

                $sqlGastos = "SELECT COALESCE(SUM(pp.valorcsf)+SUM(pp.valorssf),0)  as total,
                pp.vigencia_gasto as vigencia
                FROM ccpproyectospresupuesto p
                INNER JOIN ccpproyectospresupuesto_presupuesto pp ON p.id = pp.codproyecto
                WHERE p.estado = 'S' AND p.vigencia BETWEEN YEAR('$this->strFechaInicial') AND YEAR('$this->strFechaFinal')
                AND pp.vigencia_gasto like '$this->strGasto%' AND pp.id_fuente LIKE '$fuente%' GROUP BY pp.vigencia_gasto";

                $sqlGastos2 = "SELECT COALESCE(SUM(valor),0) as total,vigencia_gasto as vigencia,fuente
                FROM ccpetinicialgastosfun
                WHERE vigencia BETWEEN YEAR('$this->strFechaInicial') AND YEAR('$this->strFechaFinal')
                AND vigencia_gasto like '$strGasto%' AND fuente LIKE '$fuente%' GROUP BY vigencia_gasto";

                $sqlIngresos = "SELECT COALESCE(SUM(valor),0) as total,vigencia_gasto as vigencia,fuente
                FROM ccpetinicialing
                WHERE vigencia BETWEEN YEAR('$this->strFechaInicial') AND YEAR('$this->strFechaFinal')
                AND vigencia_gasto like '$strGasto%' AND fuente LIKE '$fuente%' GROUP BY vigencia_gasto";

                $arrGastos = $this->select_all($sqlGastos);
                $arrGastos2 = $this->select_all($sqlGastos2);
                $arrIngresos = $this->select_all($sqlIngresos);

                $totalGastos = count($arrGastos);
                $totalGastos2 = count($arrGastos2);
                $totalIngresos = count($arrIngresos);

                if($totalGastos > 0){
                    foreach ($arrGastos as $data) {
                        array_push($arrData,array(
                            "nombre"=>$arrFuentes[$i]['nombre'],
                            "codigo"=>$fuente,
                            "vigencia"=>$data['vigencia'],
                            "total"=>$data['total'],
                            "tipo"=>"egreso",
                            "dato"=>"inicial",
                            "llave"=>$fuente."-".$data['vigencia']
                        ));
                    }
                }
                if($totalGastos2 > 0){
                    foreach ($arrGastos2 as $data) {
                        array_push($arrData,array(
                            "nombre"=>$arrFuentes[$i]['nombre'],
                            "codigo"=>$fuente,
                            "vigencia"=>$data['vigencia'],
                            "total"=>$data['total'],
                            "tipo"=>"egreso",
                            "dato"=>"inicial",
                            "llave"=>$fuente."-".$data['vigencia']
                        ));
                    }
                }
                if($totalIngresos > 0){
                    foreach ($arrIngresos as $data) {
                        array_push($arrData,array(
                            "nombre"=>$arrFuentes[$i]['nombre'],
                            "codigo"=>$fuente,
                            "vigencia"=>$data['vigencia'],
                            "total"=>$data['total'],
                            "tipo"=>"ingreso",
                            "dato"=>"inicial",
                            "llave"=>$fuente."-".$data['vigencia']
                        ));
                    }
                }
            }
            return $arrData;
        }
        public function selectAdiciones($strFechaInicial,$strFechaFinal,$strGasto,$arrFuentes,$arrData){
            $this->strFechaInicial = $strFechaInicial;
            $this->strFechaFinal = $strFechaFinal;
            $this->strGasto = $strGasto;
            $total = count($arrFuentes);
            for ($i=0; $i < $total ; $i++) {
                $partsFuente = explode("_", $arrFuentes[$i]['codigo']);
                $fuente = $partsFuente[0];

                $sqlGastos = "SELECT COALESCE(SUM(valor),0) as total,COALESCE(codigo_vigenciag,'') as vigencia
                FROM ccpet_adiciones
                WHERE estado='S'  AND fecha BETWEEN '$this->strFechaInicial' AND '$this->strFechaFinal'
                AND codigo_vigenciag like '$this->strGasto%' AND fuente LIKE '$fuente%' AND tipo_cuenta='I' GROUP BY codigo_vigenciag";

                $sqlIngresos = "SELECT COALESCE(SUM(valor),0) as total,COALESCE(codigo_vigenciag,'') as vigencia
                FROM ccpet_adiciones
                WHERE estado='S'  AND fecha BETWEEN '$this->strFechaInicial' AND '$this->strFechaFinal'
                AND codigo_vigenciag like '$this->strGasto%' AND fuente LIKE '$fuente%' AND tipo_cuenta='G' GROUP BY codigo_vigenciag";

                $arrIngresos = $this->select_all($sqlIngresos);
                $arrGastos = $this->select_all($sqlGastos);

                $totalGastos = count($arrGastos);
                $totalIngresos = count($arrIngresos);

                if($totalGastos > 0){
                    foreach ($arrGastos as $data) {
                        array_push($arrData,array(
                            "nombre"=>$arrFuentes[$i]['nombre'],
                            "codigo"=>$fuente,
                            "vigencia"=>$data['vigencia'],
                            "total"=>$data['total'],
                            "tipo"=>"egreso",
                            "dato"=>"adicion",
                            "llave"=>$fuente."-".$data['vigencia']
                        ));
                    }
                }
                if($totalIngresos > 0){
                    foreach ($arrIngresos as $data) {
                        array_push($arrData,array(
                            "nombre"=>$arrFuentes[$i]['nombre'],
                            "codigo"=>$fuente,
                            "vigencia"=>$data['vigencia'],
                            "total"=>$data['total'],
                            "tipo"=>"ingreso",
                            "dato"=>"adicion",
                            "llave"=>$fuente."-".$data['vigencia']
                        ));
                    }
                }
            }
            return $arrData;
        }
        public function selectReducciones($strFechaInicial,$strFechaFinal,$strGasto,$arrFuentes,$arrData){
            $this->strFechaInicial = $strFechaInicial;
            $this->strFechaFinal = $strFechaFinal;
            $this->strGasto = $strGasto;
            $total = count($arrFuentes);
            for ($i=0; $i < $total ; $i++) {
                $partsFuente = explode("_", $arrFuentes[$i]['codigo']);
                $fuente = $partsFuente[0];

                $sqlGastos = "SELECT COALESCE(SUM(valor),0) as total,COALESCE(codigo_vigenciag,'') as vigencia
                FROM ccpet_reducciones
                WHERE estado='S'  AND fecha BETWEEN '$this->strFechaInicial' AND '$this->strFechaFinal'
                AND codigo_vigenciag like '$this->strGasto%' AND fuente LIKE '$fuente%' AND tipo_cuenta='I' GROUP BY codigo_vigenciag";

                $sqlIngresos = "SELECT COALESCE(SUM(valor),0) as total,COALESCE(codigo_vigenciag,'') as vigencia
                FROM ccpet_reducciones
                WHERE estado='S'  AND fecha BETWEEN '$this->strFechaInicial' AND '$this->strFechaFinal'
                AND codigo_vigenciag like '$this->strGasto%' AND fuente LIKE '$fuente%' AND tipo_cuenta='G' GROUP BY codigo_vigenciag";

                $arrIngresos = $this->select_all($sqlIngresos);
                $arrGastos = $this->select_all($sqlGastos);

                $totalGastos = count($arrGastos);
                $totalIngresos = count($arrIngresos);

                if($totalGastos > 0){
                    foreach ($arrGastos as $data) {
                        array_push($arrData,array(
                            "nombre"=>$arrFuentes[$i]['nombre'],
                            "codigo"=>$fuente,
                            "vigencia"=>$data['vigencia'],
                            "total"=>$data['total'],
                            "tipo"=>"egreso",
                            "dato"=>"reduccion",
                            "llave"=>$fuente."-".$data['vigencia']
                        ));
                    }
                }
                if($totalIngresos > 0){
                    foreach ($arrIngresos as $data) {
                        array_push($arrData,array(
                            "nombre"=>$arrFuentes[$i]['nombre'],
                            "codigo"=>$fuente,
                            "vigencia"=>$data['vigencia'],
                            "total"=>$data['total'],
                            "tipo"=>"ingreso",
                            "dato"=>"reduccion",
                            "llave"=>$fuente."-".$data['vigencia']
                        ));
                    }
                }
            }
            return $arrData;
        }
        public function selectTraslados($strFechaInicial,$strFechaFinal,$strGasto,$arrFuentes,$arrData){
            $this->strFechaInicial = $strFechaInicial;
            $this->strFechaFinal = $strFechaFinal;
            $this->strGasto = $strGasto;
            $total = count($arrFuentes);
            for ($i=0; $i < $total ; $i++) {
                $partsFuente = explode("_", $arrFuentes[$i]['codigo']);
                $fuente = $partsFuente[0];

                $sqlCredito = "SELECT COALESCE(SUM(valor),0) as total,COALESCE(codigo_vigenciag,'') as vigencia
                FROM ccpet_traslados
                WHERE estado='S'  AND tipo = 'C' AND fecha BETWEEN '$this->strFechaInicial' AND '$this->strFechaFinal'
                AND codigo_vigenciag like '$this->strGasto%' AND fuente LIKE '$fuente%'";

                $sqlContracredito = "SELECT COALESCE(SUM(valor),0) as total,COALESCE(codigo_vigenciag,'') as vigencia
                FROM ccpet_traslados
                WHERE estado='S'  AND tipo = 'R' AND fecha BETWEEN '$this->strFechaInicial' AND '$this->strFechaFinal'
                AND codigo_vigenciag like '$this->strGasto%' AND fuente LIKE '$fuente%'";

                $arrCredito = $this->select_all($sqlCredito);
                $arrContracredito = $this->select_all($sqlContracredito);

                $totalCredito = count($arrCredito);
                $totalContracredito = count($arrContracredito);

                if($totalCredito > 0){
                    foreach ($arrCredito as $data) {
                        array_push($arrData,array(
                            "nombre"=>$arrFuentes[$i]['nombre'],
                            "codigo"=>$fuente,
                            "vigencia"=>$data['vigencia'],
                            "total"=>$data['total'],
                            "tipo"=>"credito",
                            "dato"=>"traslado",
                            "llave"=>$fuente."-".$data['vigencia']
                        ));
                    }
                }
                if($totalContracredito > 0){
                    foreach ($arrContracredito as $data) {
                        array_push($arrData,array(
                            "nombre"=>$arrFuentes[$i]['nombre'],
                            "codigo"=>$fuente,
                            "vigencia"=>$data['vigencia'],
                            "total"=>$data['total'],
                            "tipo"=>"contracredito",
                            "dato"=>"traslado",
                            "llave"=>$fuente."-".$data['vigencia']
                        ));
                    }
                }
            }
            return $arrData;
        }
        public function selectRecaudos($strFechaInicial,$strFechaFinal,$strGasto,$arrFuentes,$arrData){
            $this->strFechaInicial = $strFechaInicial;
            $this->strFechaFinal = $strFechaFinal;
            $this->strGasto = $strGasto;
            $total = count($arrFuentes);
            for ($i=0; $i < $total ; $i++) {
                $partsFuente = explode("_", $arrFuentes[$i]['codigo']);
                $fuente = $partsFuente[0];

                $sqlRecibo = "SELECT
                                COALESCE(SUM(det.valor), 0) AS total,
                                DATE_FORMAT(cab.fecha, '%Y-%m') AS mes,
                                COALESCE(det.vigencia_gasto, '') AS vigencia
                            FROM 
                                tesoreciboscaja cab
                            INNER JOIN 
                                pptorecibocajappto det ON cab.id_recibos = det.idrecibo
                            WHERE
                                NOT(cab.estado = 'N' OR cab.estado = 'R') 
                                AND cab.fecha BETWEEN '$this->strFechaInicial' AND '$this->strFechaFinal'
                                AND det.vigencia_gasto LIKE '$this->strGasto%'
                                AND det.fuente LIKE '$fuente%'
                            GROUP BY
                                DATE_FORMAT(cab.fecha, '%Y-%m'), det.vigencia_gasto
                            ORDER BY
                                DATE_FORMAT(cab.fecha, '%Y-%m')";

                $sqlSinRecibo = "SELECT 
                                    COALESCE(SUM(det.valor),0) as total,
                                    DATE_FORMAT(cab.fecha, '%Y-%m') as mes,
                                    COALESCE(det.vigencia_gasto,'') as vigencia
                                FROM 
                                    tesosinreciboscaja cab
                                INNER JOIN 
                                    pptosinrecibocajappto det ON cab.id_recibos = det.idrecibo
                                WHERE 
                                    NOT(cab.estado='N' OR cab.estado='R')  
                                    AND cab.fecha BETWEEN '$this->strFechaInicial' AND '$this->strFechaFinal'
                                    AND det.vigencia_gasto like '$this->strGasto%' 
                                    AND det.fuente LIKE '$fuente%' 
                                GROUP BY 
                                    DATE_FORMAT(cab.fecha, '%Y-%m'),det.vigencia_gasto
                                ORDER BY 
                                    DATE_FORMAT(cab.fecha, '%Y-%m')";
                                    
                $sqlNotasBancarias = "SELECT 
                                        COALESCE(SUM(det.valor),0) as total,
                                        DATE_FORMAT(cab.fecha, '%Y-%m') as mes,
                                        COALESCE(det.vigencia_gasto,'') as vigencia
                                    FROM 
                                        tesonotasbancarias_cab cab
                                    INNER JOIN 
                                        pptonotasbanppto det ON cab.id_comp=det.idrecibo
                                    WHERE 
                                        NOT(cab.estado='N' OR cab.estado='R')  
                                        AND cab.fecha BETWEEN '$this->strFechaInicial' AND '$this->strFechaFinal'
                                        AND det.vigencia_gasto like '$this->strGasto%' 
                                        AND det.fuente LIKE '$fuente%' 
                                    GROUP BY 
                                        DATE_FORMAT(cab.fecha, '%Y-%m'),det.vigencia_gasto
                                    ORDER BY 
                                        DATE_FORMAT(cab.fecha, '%Y-%m')";

                $sqlRecaudoTransferencia = "SELECT
                                            COALESCE(SUM(det.valor), 0) AS total,
                                            DATE_FORMAT(cab.fecha, '%Y-%m') AS mes,
                                            COALESCE(det.vigencia_gasto, '') AS vigencia
                                        FROM 
                                            tesorecaudotransferencia cab
                                        INNER JOIN 
                                            pptoingtranppto det ON det.idrecibo = cab.id_recaudo
                                        WHERE
                                            NOT(cab.estado = 'N' OR cab.estado = 'R') 
                                            AND cab.fecha BETWEEN '$this->strFechaInicial' AND '$this->strFechaFinal'
                                            AND det.vigencia_gasto LIKE '$this->strGasto%'
                                            AND det.fuente LIKE '$fuente%'
                                        GROUP BY
                                            DATE_FORMAT(cab.fecha, '%Y-%m'), det.vigencia_gasto
                                        ORDER BY
                                            DATE_FORMAT(cab.fecha, '%Y-%m')";
                
                $sqlRetencionEgresos = "SELECT 
                                            COALESCE(SUM(det.valor),0) as total,
                                            DATE_FORMAT(cab.fecha, '%Y-%m') as mes,
                                            COALESCE(det.vigencia_gasto,'') as vigencia
                                        FROM 
                                            tesoegresos cab
                                        INNER JOIN 
                                            pptoretencionpago det ON cab.id_egreso=det.idrecibo
                                        WHERE 
                                            cab.estado!='N' 
                                            AND cab.fecha BETWEEN '$this->strFechaInicial' AND '$this->strFechaFinal'
                                            AND det.vigencia_gasto like '$this->strGasto%' AND cab.tipo_mov='201'
                                            AND det.tipo='egreso' 
                                            AND NOT EXISTS (SELECT 1 FROM tesoegresos tra WHERE tra.id_egreso=cab.id_egreso  AND tra.tipo_mov='401')
                                            AND det.fuente LIKE '$fuente%' 
                                            GROUP BY 
                                                DATE_FORMAT(cab.fecha, '%Y-%m'),det.vigencia_gasto
                                            ORDER BY
                                                DATE_FORMAT(cab.fecha, '%Y-%m')";

                $sqlSuperavit = "SELECT 
                                    COALESCE(SUM(det.valor),0) as total,
                                    DATE_FORMAT(cab.fecha, '%Y-%m') as mes,
                                    COALESCE(det.vigencia_gasto,'') as vigencia
                                FROM 
                                    tesosuperavit cab
                                INNER JOIN 
                                    tesosuperavit_det det ON cab.id=det.id_tesosuperavit
                                WHERE 
                                    cab.estado ='S' 
                                    AND cab.fecha BETWEEN '$this->strFechaInicial' AND '$this->strFechaFinal'
                                    AND det.vigencia_gasto like '$this->strGasto%' 
                                    AND det.fuente LIKE '$fuente%' 
                                GROUP BY 
                                    DATE_FORMAT(cab.fecha, '%Y-%m'),det.vigencia_gasto
                                ORDER BY 
                                    DATE_FORMAT(cab.fecha, '%Y-%m')";

                $sqlRetencionOtrosEgresos = "SELECT 
                                                COALESCE(SUM(det.valor),0) as total,
                                                DATE_FORMAT(cab.fecha, '%Y-%m') as mes,
                                                COALESCE(det.vigencia_gasto,'') as vigencia
                                            FROM 
                                                tesopagotercerosvigant cab
                                            INNER JOIN 
                                                pptoretencionotrosegresos det ON cab.id_pago=det.idrecibo
                                            WHERE 
                                                cab.estado !='N' 
                                                AND det.tipo='egreso' 
                                                AND cab.fecha BETWEEN '$this->strFechaInicial' AND '$this->strFechaFinal'
                                                AND det.vigencia_gasto like '$this->strGasto%' 
                                                AND det.fuente LIKE '$fuente%' 
                                            GROUP BY 
                                                DATE_FORMAT(cab.fecha, '%Y-%m'),det.vigencia_gasto
                                            ORDER BY 
                                                DATE_FORMAT(cab.fecha, '%Y-%m')";

                $sqlRetencionPago = "SELECT 
                                        COALESCE(SUM(det.valor),0) as total,
                                        DATE_FORMAT(cab.fecha, '%Y-%m') as mes,
                                        COALESCE(det.vigencia_gasto,'') as vigencia
                                    FROM 
                                        tesoordenpago cab
                                    INNER JOIN 
                                        pptoretencionpago det ON cab.id_orden=det.idrecibo
                                    WHERE 
                                        cab.estado !='N' 
                                        AND det.tipo='orden' 
                                        AND cab.fecha BETWEEN '$this->strFechaInicial' AND '$this->strFechaFinal'
                                        AND det.vigencia_gasto like '$this->strGasto%' 
                                        AND det.fuente LIKE '$fuente%' 
                                        AND cab.tipo_mov='201'
                                        AND NOT EXISTS (SELECT 1 FROM tesoordenpago tca WHERE tca.id_orden=cab.id_orden  AND tca.tipo_mov='401') 
                                    GROUP BY 
                                        DATE_FORMAT(cab.fecha, '%Y-%m'),det.vigencia_gasto
                                    ORDER BY
                                        DATE_FORMAT(cab.fecha, '%Y-%m')";

                $sqlRecaudoFactura = "SELECT 
                                        COALESCE(SUM(det.valor),0) as total,
                                        DATE_FORMAT(cab.fecha_recaudo, '%Y-%m') as mes,
                                        COALESCE(det.vigencia_gasto,'') as vigencia
                                    FROM 
                                        srv_recaudo_factura cab
                                    INNER JOIN 
                                        srv_recaudo_factura_ppto det ON cab.codigo_recaudo=det.codigo_recaudo
                                    WHERE 
                                        cab.estado !='REVERSADO' 
                                        AND cab.fecha_recaudo BETWEEN '$this->strFechaInicial' AND '$this->strFechaFinal'
                                        AND det.vigencia_gasto like '$this->strGasto%' 
                                        AND det.fuente LIKE '$fuente%' 
                                    GROUP BY 
                                        DATE_FORMAT(cab.fecha_recaudo, '%Y-%m'), det.vigencia_gasto
                                    ORDER BY 
                                        DATE_FORMAT(cab.fecha_recaudo, '%Y-%m')";

                $sqlRecaudoFacturaAcuerdo = "SELECT 
                                                COALESCE(SUM(det.valor),0) as total,
                                                DATE_FORMAT(cab.fecha_acuerdo, '%Y-%m') as mes,
                                                COALESCE(det.vigencia_gasto,'') as vigencia
                                            FROM 
                                                srv_acuerdo_cab cab
                                            INNER JOIN 
                                                srv_acuerdo_ppto det ON cab.codigo_acuerdo=det.codigo_acuerdo
                                            WHERE 
                                                cab.estado_acuerdo !='Reversado' 
                                                AND cab.fecha_acuerdo BETWEEN '$this->strFechaInicial' AND '$this->strFechaFinal'
                                                AND det.vigencia_gasto like '$this->strGasto%' 
                                                AND det.fuente LIKE '$fuente%' 
                                            GROUP BY 
                                                DATE_FORMAT(cab.fecha_acuerdo, '%Y-%m'),det.vigencia_gasto
                                            ORDER BY 
                                                DATE_FORMAT(cab.fecha_acuerdo, '%Y-%m')";

                $sqlReservas = "SELECT 
                                    COALESCE(SUM(det.valor),0) as total,
                                    DATE_FORMAT(cab.fecha, '%Y-%m') as mes,
                                    COALESCE(det.vigencia_gasto,'') as vigencia
                                FROM 
                                    tesoreservas cab
                                INNER JOIN 
                                    tesoreservas_det det ON cab.id=det.id_reserva
                                WHERE 
                                    cab.estado ='S' 
                                    AND cab.fecha BETWEEN '$this->strFechaInicial' AND '$this->strFechaFinal'
                                    AND det.vigencia_gasto like '$this->strGasto%' 
                                    AND det.fuente LIKE '$fuente%' 
                                GROUP BY 
                                    DATE_FORMAT(cab.fecha, '%Y-%m'),det.vigencia_gasto
                                ORDER BY 
                                    DATE_FORMAT(cab.fecha, '%Y-%m')";

                
                $arrRecibo = $this->select_all($sqlRecibo);
                $arrSinRecibo = $this->select_all($sqlSinRecibo);
                $arrNotasBancarias = $this->select_all($sqlNotasBancarias);
                $arrRecaudoTransferencia = $this->select_all($sqlRecaudoTransferencia);
                $arrRetencionEgresos = $this->select_all($sqlRetencionEgresos);
                $arrSuperavit = $this->select_all($sqlSuperavit);
                $arrRetencionOtrosEgresos = $this->select_all($sqlRetencionOtrosEgresos);
                $arrRetencionPago = $this->select_all($sqlRetencionPago);
                $arrRecaudoFactura = $this->select_all($sqlRecaudoFactura);
                $arrRecaudoFacturaAcuerdo = $this->select_all($sqlRecaudoFacturaAcuerdo);
                $arrReservas = $this->select_all($sqlReservas);

                $totalRecibo = count($arrRecibo);
                $totalSinRecibo = count($arrSinRecibo);
                $totalNotas = count($arrNotasBancarias);
                $totalTransferencia = count($arrRecaudoTransferencia);
                $totalRetencion = count($arrRetencionEgresos);
                $totalSuperavit = count($arrSuperavit);
                $totalRetencionOtros = count($arrRetencionOtrosEgresos);
                $totalRetencionPago = count($arrRetencionPago);
                $totalFactura = count($arrRecaudoFactura);
                $totalFacturaAcuerdo = count($arrRecaudoFacturaAcuerdo);
                $totalReservas = count($arrReservas);

                
                if($totalRecibo > 0){
                    foreach ($arrRecibo as $data) {
                        array_push($arrData,array(
                            "nombre"=>$arrFuentes[$i]['nombre'],
                            "codigo"=>$fuente,
                            "vigencia"=>$data['vigencia'],
                            "total"=>$data['total'],
                            "mes"=> $data['mes'],
                            "tipo"=>"ingreso",
                            "dato"=>"recaudo",
                            "llave"=>$fuente."-".$data['vigencia']."-".$data['mes']
                        ));
                    }
                }
                
                if($totalSinRecibo > 0){
                    foreach ($arrSinRecibo as $data) {
                        array_push($arrData,array(
                            "nombre"=>$arrFuentes[$i]['nombre'],
                            "codigo"=>$fuente,
                            "vigencia"=>$data['vigencia'],
                            "total"=>$data['total'],
                            "mes"=> $data['mes'],
                            "tipo"=>"ingreso",
                            "dato"=>"recaudo",
                            "llave"=>$fuente."-".$data['vigencia']."-".$data['mes']
                        ));
                    }
                }
                if($totalNotas > 0){
                    foreach ($arrNotasBancarias as $data) {
                        array_push($arrData,array(
                            "nombre"=>$arrFuentes[$i]['nombre'],
                            "codigo"=>$fuente,
                            "vigencia"=>$data['vigencia'],
                            "total"=>$data['total'],
                            "mes"=> $data['mes'],
                            "tipo"=>"ingreso",
                            "dato"=>"recaudo",
                            "llave"=>$fuente."-".$data['vigencia']."-".$data['mes']
                        ));
                    }
                }
                if($totalTransferencia > 0){
                    foreach ($arrRecaudoTransferencia as $data) {
                        array_push($arrData,array(
                            "nombre"=>$arrFuentes[$i]['nombre'],
                            "codigo"=>$fuente,
                            "vigencia"=>$data['vigencia'],
                            "total"=>$data['total'],
                            "mes"=> $data['mes'],
                            "tipo"=>"ingreso",
                            "dato"=>"recaudo",
                            "llave"=>$fuente."-".$data['vigencia']."-".$data['mes']
                        ));
                    }
                }
                if($totalRetencion > 0){
                    foreach ($arrRetencionEgresos as $data) {
                        array_push($arrData,array(
                            "nombre"=>$arrFuentes[$i]['nombre'],
                            "codigo"=>$fuente,
                            "vigencia"=>$data['vigencia'],
                            "total"=>$data['total'],
                            "mes"=> $data['mes'],
                            "tipo"=>"ingreso",
                            "dato"=>"recaudo",
                            "llave"=>$fuente."-".$data['vigencia']."-".$data['mes']
                        ));
                    }
                }
                if($totalSuperavit > 0){
                    foreach ($arrSuperavit as $data) {
                        array_push($arrData,array(
                            "nombre"=>$arrFuentes[$i]['nombre'],
                            "codigo"=>$fuente,
                            "vigencia"=>$data['vigencia'],
                            "total"=>$data['total'],
                            "mes"=> $data['mes'],
                            "tipo"=>"ingreso",
                            "dato"=>"recaudo",
                            "llave"=>$fuente."-".$data['vigencia']."-".$data['mes']
                        ));
                    }
                }
                if($totalRetencionOtros > 0){
                    foreach ($arrRetencionOtrosEgresos as $data) {
                        array_push($arrData,array(
                            "nombre"=>$arrFuentes[$i]['nombre'],
                            "codigo"=>$fuente,
                            "vigencia"=>$data['vigencia'],
                            "total"=>$data['total'],
                            "mes"=> $data['mes'],
                            "tipo"=>"ingreso",
                            "dato"=>"recaudo",
                            "llave"=>$fuente."-".$data['vigencia']."-".$data['mes']
                        ));
                    }
                }
                if($totalRetencionPago > 0){
                    foreach ($arrRetencionPago as $data) {
                        array_push($arrData,array(
                            "nombre"=>$arrFuentes[$i]['nombre'],
                            "codigo"=>$fuente,
                            "vigencia"=>$data['vigencia'],
                            "total"=>$data['total'],
                            "mes"=> $data['mes'],
                            "tipo"=>"ingreso",
                            "dato"=>"recaudo",
                            "llave"=>$fuente."-".$data['vigencia']."-".$data['mes']
                        ));
                    }
                }
                if($totalFactura > 0){
                    foreach ($arrRecaudoFactura as $data) {
                        array_push($arrData,array(
                            "nombre"=>$arrFuentes[$i]['nombre'],
                            "codigo"=>$fuente,
                            "vigencia"=>$data['vigencia'],
                            "total"=>$data['total'],
                            "mes"=> $data['mes'],
                            "tipo"=>"ingreso",
                            "dato"=>"recaudo",
                            "llave"=>$fuente."-".$data['vigencia']."-".$data['mes']
                        ));
                    }
                }
                if($totalFacturaAcuerdo > 0){
                    foreach ($arrRecaudoFacturaAcuerdo as $data) {
                        array_push($arrData,array(
                            "nombre"=>$arrFuentes[$i]['nombre'],
                            "codigo"=>$fuente,
                            "vigencia"=>$data['vigencia'],
                            "total"=>$data['total'],
                            "mes"=> $data['mes'],
                            "tipo"=>"ingreso",
                            "dato"=>"recaudo",
                            "llave"=>$fuente."-".$data['vigencia']."-".$data['mes']
                        ));
                    }
                }
                if($totalReservas > 0){
                    foreach ($arrReservas as $data) {
                        array_push($arrData,array(
                            "nombre"=>$arrFuentes[$i]['nombre'],
                            "codigo"=>$fuente,
                            "vigencia"=>$data['vigencia'],
                            "total"=>$data['total'],
                            "mes"=> $data['mes'],
                            "tipo"=>"ingreso",
                            "dato"=>"recaudo",
                            "llave"=>$fuente."-".$data['vigencia']."-".$data['mes']
                        ));
                    }
                }
            }
            return $arrData;
        }
        public function selectPagos($strFechaInicial,$strFechaFinal,$strGasto,$arrFuentes,$arrData){
            $this->strFechaInicial = $strFechaInicial;
            $this->strFechaFinal = $strFechaFinal;
            $this->strGasto = $strGasto;
            $total = count($arrFuentes);
            for ($i=0; $i < $total ; $i++) {
                $partsFuente = explode("_", $arrFuentes[$i]['codigo']);
                $fuente = $partsFuente[0];

                $sqlEgresos = "SELECT 
                                    COALESCE(SUM(det.valor),0) as total,
                                    DATE_FORMAT(cab.fecha, '%Y-%m') as mes,
                                    COALESCE(det.codigo_vigenciag,'') as vigencia
                                FROM 
                                    tesoegresos cab
                                INNER JOIN  
                                    tesoordenpago_det det ON cab.id_orden =det.id_orden
                                WHERE 
                                    det.tipo_mov='201' 
                                    AND cab.fecha BETWEEN '$this->strFechaInicial' AND '$this->strFechaFinal' 
                                    AND cab.tipo_mov = '201'
                                    AND det.codigo_vigenciag like '$this->strGasto%' 
                                    AND det.fuente LIKE '$fuente%' 
                                GROUP BY 
                                    DATE_FORMAT(cab.fecha, '%Y-%m'),det.codigo_vigenciag
                                ORDER BY 
                                    DATE_FORMAT(cab.fecha, '%Y-%m')";

                $sqlEgresos2 = "SELECT 
                                    COALESCE(SUM(det.valor),0) as total,
                                    DATE_FORMAT(cab.fecha, '%Y-%m') as mes,
                                    COALESCE(det.codigo_vigenciag,'') as vigencia
                                FROM 
                                    tesoegresos cab
                                INNER JOIN  
                                    tesoordenpago_det det ON cab.id_orden =det.id_orden
                                WHERE 
                                    det.tipo_mov='401' 
                                    AND cab.fecha BETWEEN '$this->strFechaInicial' AND '$this->strFechaFinal' 
                                    AND cab.tipo_mov = '401'
                                    AND det.codigo_vigenciag like '$this->strGasto%' 
                                    AND det.fuente LIKE '$fuente%' 
                                GROUP BY 
                                    DATE_FORMAT(cab.fecha, '%Y-%m'),det.codigo_vigenciag
                                ORDER BY 
                                    DATE_FORMAT(cab.fecha, '%Y-%m')";

                $sqlEgresoNomina = "SELECT 
                                        COALESCE(SUM(det.valordevengado),0) as total,
                                        DATE_FORMAT(cab.fecha, '%Y-%m') as mes,
                                        COALESCE(det.vigencia_gasto,'') as vigencia
                                    FROM 
                                        tesoegresosnomina cab
                                    INNER JOIN  
                                        tesoegresosnomina_det det ON cab.id_egreso =det.id_egreso
                                    WHERE 
                                        det.tipo_mov='201' 
                                        AND cab.tipo_mov = '201'
                                        AND NOT(det.tipo='SE' OR det.tipo='PE' OR det.tipo='DS' OR det.tipo='RE' OR det.tipo='FS')
                                        AND det.vigencia_gasto like '$this->strGasto%' 
                                        AND cab.fecha BETWEEN '$this->strFechaInicial' AND '$this->strFechaFinal' 
                                        AND det.fuente LIKE '$fuente%' 
                                    GROUP BY 
                                        DATE_FORMAT(cab.fecha, '%Y-%m'),det.vigencia_gasto
                                    ORDER BY 
                                        DATE_FORMAT(cab.fecha, '%Y-%m')";

                $sqlEgresoNomina2 = "SELECT 
                                        COALESCE(SUM(det.valordevengado),0) as total,
                                        DATE_FORMAT(cab.fecha, '%Y-%m') as mes,
                                        COALESCE(det.vigencia_gasto,'') as vigencia
                                    FROM 
                                        tesoegresosnomina cab
                                    INNER JOIN  
                                        tesoegresosnomina_det det ON cab.id_egreso =det.id_egreso
                                    WHERE 
                                        det.tipo_mov='401' 
                                        AND cab.tipo_mov = '401'
                                        AND NOT(det.tipo='SE' OR det.tipo='PE' OR det.tipo='DS' OR det.tipo='RE' OR det.tipo='FS')
                                        AND det.vigencia_gasto like '$this->strGasto%' 
                                        AND cab.fecha BETWEEN '$this->strFechaInicial' AND '$this->strFechaFinal' 
                                        AND det.fuente LIKE '$fuente%' 
                                    GROUP BY 
                                        DATE_FORMAT(cab.fecha, '%Y-%m'),det.vigencia_gasto
                                    ORDER BY 
                                        DATE_FORMAT(cab.fecha, '%Y-%m')";

                $arrEgresos = $this->select_all($sqlEgresos);
                $arrEgresosNomina = $this->select_all($sqlEgresoNomina);
                $arrEgresos2 = $this->select_all($sqlEgresos2);
                $arrEgresosNomina2 = $this->select_all($sqlEgresoNomina2);

                $totalEgresos = count($arrEgresos);
                $totalEgresosNomina = count($arrEgresosNomina);

                if($totalEgresos > 0){
                    foreach ($arrEgresos as $data) {
                        array_push($arrData,array(
                            "nombre"=>$arrFuentes[$i]['nombre'],
                            "codigo"=>$fuente,
                            "vigencia"=>$data['vigencia'],
                            "total"=>$data['total'],
                            "mes"=>$data['mes'],
                            "tipo"=>"egreso",
                            "movimiento"=>"201",
                            "dato"=>"pago",
                            "llave"=>$fuente."-".$data['vigencia']."-".$data['mes']
                        ));
                    }
                    foreach ($arrEgresos2 as $data) {
                        array_push($arrData,array(
                            "nombre"=>$arrFuentes[$i]['nombre'],
                            "codigo"=>$fuente,
                            "vigencia"=>$data['vigencia'],
                            "total"=>$data['total'],
                            "mes"=>$data['mes'],
                            "tipo"=>"egreso",
                            "movimiento"=>"401",
                            "dato"=>"pago",
                            "llave"=>$fuente."-".$data['vigencia']."-".$data['mes']
                        ));
                    }
                }
                if($totalEgresosNomina > 0){
                    foreach ($arrEgresosNomina as $data) {
                        array_push($arrData,array(
                            "nombre"=>$arrFuentes[$i]['nombre'],
                            "codigo"=>$fuente,
                            "vigencia"=>$data['vigencia'],
                            "total"=>$data['total'],
                            "mes"=>$data['mes'],
                            "tipo"=>"egreso",
                            "movimiento"=>"201",
                            "dato"=>"pago",
                            "llave"=>$fuente."-".$data['vigencia']."-".$data['mes']
                        ));
                    }
                    foreach ($arrEgresosNomina2 as $data) {
                        array_push($arrData,array(
                            "nombre"=>$arrFuentes[$i]['nombre'],
                            "codigo"=>$fuente,
                            "vigencia"=>$data['vigencia'],
                            "total"=>$data['total'],
                            "mes"=>$data['mes'],
                            "tipo"=>"egreso",
                            "movimiento"=>"401",
                            "dato"=>"pago",
                            "llave"=>$fuente."-".$data['vigencia']."-".$data['mes']
                        ));
                    }
                }
            }
            return $arrData;
        }
        public function selectFuentes(){
            $sql="SELECT codigo_fuente as codigo, nombre
            FROM ccpet_fuentes_cuipo
            WHERE version = '1' AND codigo_fuente NOT LIKE '%_%' ORDER BY codigo_fuente ASC";
            $request = $this->select_all($sql);
            if(!empty($request)){
                $total = count($request);
                for ($i=0; $i < $total; $i++) {
                    $request[$i]['is_checked'] = 1;
                }
            }
            return $request;
        }

        public function selectFuentes_1(){
            $sql="SELECT codigo_fuente as codigo, nombre
            FROM ccpet_fuentes_cuipo
            WHERE version = '1' ORDER BY codigo_fuente ASC";
            $request = $this->select_all($sql);
            if(!empty($request)){
                $total = count($request);
                for ($i=0; $i < $total; $i++) {
                    $request[$i]['is_checked'] = 1;
                }
            }
            return $request;
        }
    }
?>
