const URL ='presupuesto_ccpet/catalogo_superavit/buscar/ccpet-catalogoBuscar.php';

var app = new Vue({
    el:"#myapp",
    data() {
        return {
            isLoading:false,
            arrData:[],
            txtResults:0,
            intTotalPages:1,
            intPage:1,
            txtSearch:""
        }
    },
    mounted() {
        this.getData();
    },
    methods: {

        getData: async function(){

            const formData = new FormData();
            formData.append("action","get");
            this.isLoading=true;
            const response = await fetch(URL,{method:"POST", body:formData});
            const objData = await response.json();
            this.arrData = objData.data;
            this.txtResults = objData.total;
            this.intTotalPages = objData.total_pages;
            this.isLoading=false;
        },
        search: async function(page=1){
            if(page <= 0){
                page = 1;
                this.intPage = page;
            }else if(page > this.intTotalPages){
                page = this.intTotalPages;
                this.intPage = page;
            }
            const formData = new FormData();
            formData.append("action","search");
            formData.append("search",this.txtSearch);
            formData.append("page",page);
            this.isLoading=true;

            const response = await fetch(URL,{method:"POST", body:formData});
            const objData = await response.json();

            this.arrData = objData.data;
            this.txtResults = objData.total;
            this.intTotalPages = objData.total_pages;

            this.isLoading=false;
        },
        editItem:function(id){
            window.location.href= 'ccpet-catalogoSuperavitEditar.php?id='+id;
        }
    },
    computed:{

    }
})
