const URL ='presupuesto_ccpet/catalogo_superavit/editar/ccpet-catalogoEditar.php';

var app = new Vue({
    el:"#myapp",
    data() {
        return {
            isLoading:false,
            txtNombre:"",
            txtConsecutivo:0,
        }
    },
    mounted() {
        this.getData();
    },
    methods: {

        getData: async function(){
            let codigo = new URLSearchParams(window.location.search).get('id');
            const formData = new FormData();
            formData.append("action","get");
            formData.append("codigo",codigo);
            const response = await fetch(URL,{method:"POST", body:formData});
            const objData = await response.json();
            console.log(objData);
            if(objData.status){
                this.txtConsecutivo = objData.data.codigo;
                this.txtNombre = objData.data.nombre;
            }else{
                window.location.href="ccpet-catalogoCompIngresoBuscar.php";
            }

        },
        save:async function(){
            if(this.txtNombre == ""){
                Swal.fire("Error","Todos los campos con (*) Son obligatorios","error");
                return false;
            }
            let formData = new FormData();
            formData.append("action","save");
            formData.append("nombre",this.txtNombre);
            formData.append("codigo",this.txtConsecutivo);
            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();
            if(objData.status){
                Swal.fire("Guardado",objData.msg,"success");
                setTimeout(function(){
                    window.location.reload();
                },3000);
            }else{
                Swal.fire("Error",objData.msg,"error");
            }
        }
    },
    computed:{

    }
})
