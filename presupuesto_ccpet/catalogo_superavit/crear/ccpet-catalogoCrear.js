const URL ='presupuesto_ccpet/catalogo_superavit/crear/ccpet-catalogoCrear.php';

var app = new Vue({
    el:"#myapp",
    data() {
        return {
            isLoading:false,
            txtNombre:"",
            txtConsecutivo:0,
        }
    },
    mounted() {
        this.getData();
    },
    methods: {

        getData: async function(){
            const formData = new FormData();
            formData.append("action","get");
            const response = await fetch(URL,{method:"POST", body:formData});
            const objData = await response.json();
            this.txtConsecutivo = objData.consecutivo;
            console.log(objData);
        },
        save:async function(){
            if(this.txtNombre == ""){
                Swal.fire("Error","Todos los campos con (*) Son obligatorios","error");
                return false;
            }
            let formData = new FormData();
            formData.append("action","save");
            formData.append("nombre",this.txtNombre);
            formData.append("codigo",this.txtConsecutivo);
            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();
            if(objData.status){
                Swal.fire("Guardado",objData.msg,"success");
                setTimeout(function(){
                    window.location.reload();
                },3000);
            }else{
                Swal.fire("Error",objData.msg,"error");
            }
        },
        formatNumero: function(valor){
            return new Intl.NumberFormat("en-US", {style: "currency", currency: "USD"}).format(valor);
        },
        validInteger:function(valor){
            valor = new String(valor);
            if(valor.includes(".") || valor.includes(",")){
                Swal.fire("Error","La cantidad debe ser un número entero.","error");
            }
        },
    },
    computed:{

    }
})
