<?php
    require_once '../../../comun.inc';
    require '../../../funciones.inc';
    require '../../../funcionesSP.inc.php';
    /*ini_set('display_errors', '1');
    ini_set('display_startup_errors', '1');
    error_reporting(E_ALL);*/
    session_start();
    //dep($_POST);exit;
    if($_POST){
        $obj = new Plantilla();
        if($_POST['action']=="get"){
            $obj->getData();
        }else if($_POST['action'] == "save"){
            $obj->save($_POST['codigo'],$_POST['nombre']);
        }
    }

    class Plantilla{
        private $linkbd;
        private $strNombre;
        private $strCodigo;
        public function __construct() {
            $this->linkbd = conectar_v7();
            $this->linkbd->set_charset("utf8");
        }
        public function getData(){
            if(!empty($_SESSION)){
                $request['consecutivo'] = $this->selectConsecutivo();
                echo json_encode($request,JSON_UNESCAPED_UNICODE);
            }
            die();
        }

        public function save($codigo,$nombre){
            if(!empty($_SESSION)){
                //dep($arrData);exit;
                if(empty($codigo) || empty($nombre)){
                    $arrResponse = array("status"=>false,"msg"=>"Error de datos");
                }else{
                    $nombre = ucwords($nombre);
                    $request = $this->insertData($codigo,$nombre);
                    if(is_numeric($request) && $request > 0){
                        $arrResponse = array("status"=>true,"msg"=>"Datos guardados");
                    }else if($request=="existe"){
                        $arrResponse = array("status"=>false,"msg"=>"El nombre ya existe, intente con otro.");
                    }else{
                        $arrResponse = array("status"=>false,"msg"=>"Ha ocurrido un error, no se ha podido guardar.");
                    }
                }
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
            die();
        }
        public function insertData(string $codigo,string $nombre){
            $this->strCodigo = $codigo;
            $this->strNombre = $nombre;
            $sql = "SELECT * FROM ccpet_catalogo_superavit  WHERE nombre = '$this->strNombre'";
            $request = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
            if(empty($request)){
                $sql = "INSERT INTO ccpet_catalogo_superavit (codigo,nombre) VALUES('$this->strCodigo','$this->strNombre')";
                $request = intval(mysqli_query($this->linkbd,$sql));
            }else{
                $request = "existe";
            }
            return $request;
        }
        public function selectConsecutivo(){
            $sql = "SELECT  MAX(id) as id from ccpet_catalogo_superavit";
            $request = mysqli_query($this->linkbd,$sql)->fetch_assoc()['id']+1;
            $consecutivo = $request;
            if($request < 10){
                $consecutivo = "0".$request;
            }
            return $consecutivo;
        }
    }
?>
