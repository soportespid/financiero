<?php

	require_once '../../comun.inc';
    require '../../funciones.inc';
    session_start();

	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");

    $out = array('error' => false);

    $maxVersion = ultimaVersionGastosCCPET();

    $action = "show";

    if(isset($_GET['action'])){
        $action = $_GET['action'];
    }
    
    if($action == 'buscarSaldos'){

        $medioDePago = $_POST['medioDePago'];
        $vigenciaDelGasto = $_POST['vigenciaDelGasto'];

        $crit1 = '';
        $crit2 = '';
        $comas = [",", ";"];

        if($medioDePago != '-1'){
            $crit1 = "AND TB2.medio_pago = '$medioDePago'";
        }

        if($vigenciaDelGasto != '-1'){
            $crit2 = "AND TB2.codigo_vigenciag = '$vigenciaDelGasto'";
        }

        $reservas = [];

        $critFecha = '';
        $critNumContrato = '';

        if($_POST['fechaInicial'] != '' && $_POST['fechaFinal'] != ''){
            $critFecha = " AND fecha BETWEEN '".$_POST['fechaInicial']."' AND '".$_POST['fechaFinal']."'";
        }

        $sqlr = "SELECT TB1.consvigencia, TB2.cuenta, TB2.indicador_producto, TB2.bpim, TB2.fuente, TB1.fecha, SUM(TB2.valor), TB1.idcdp, TB1.contrato, TB1.detalle, TB1.tercero, TB1.vigencia FROM ccpetrp AS TB1, ccpetrp_detalle AS TB2 WHERE TB1.vigencia = TB2.vigencia AND TB1.consvigencia = TB2.consvigencia $critFecha AND TB1.tipo_mov = TB2.tipo_mov AND TB1.tipo_mov = '201' $crit1 $crit2 GROUP BY TB1.consvigencia, TB2.cuenta, TB2.indicador_producto, TB2.bpim, TB2.fuente";
        $res = mysqli_query($linkbd, $sqlr);
        while($row = mysqli_fetch_assoc($res)){
            $valorRp = 0;
            $saldoPorEjecu = 0;
            $rubro = '';
            $nombreRubro = '';
            $tercero = '';
            $nombreFuente = "";

            $sqlrRev = "SELECT SUM(valor) FROM ccpetrp_detalle WHERE cuenta = '$row[cuenta]' AND indicador_producto = '$row[indicador_producto]' AND bpim = '$row[bpim]' AND fuente = '$row[fuente]' AND consvigencia = '$row[consvigencia]' AND vigencia = '$row[vigencia]' AND SUBSTRING(tipo_mov, 1, 1) LIKE '4%'";
            $resRev = mysqli_query($linkbd, $sqlrRev);
            $rowRev = mysqli_fetch_assoc($resRev);

            $sqlrCxp = "SELECT SUM(T2.valor) FROM tesoordenpago AS T1, tesoordenpago_det AS T2 WHERE (T1.estado = 'S' OR T1.estado = 'P') AND T1.id_orden = T2.id_orden AND T1.id_rp = '".$row['consvigencia']."' AND T1.vigencia = '".$row['vigencia']."' AND T2.cuentap = '".$row['cuenta']."' AND T2.indicador_producto = '".$row['indicador_producto']."' AND T2.bpim = '".$row['bpim']."' AND T2.fuente = '".$row['fuente']."'";
            $resCxp = mysqli_query($linkbd, $sqlrCxp);
            $rowCxp = mysqli_fetch_row($resCxp);

            $sqlr_cxp_nom = "SELECT SUM(TB2.valor) FROM hum_nom_cdp_rp AS TB1, humnom_presupuestal AS TB2 WHERE TB1.rp = '".$row['consvigencia']."' AND TB1.vigencia = '".$row['vigencia']."' AND TB1.nomina = TB2.id_nom AND TB2.estado = 'P' AND TB2.cuenta = '".$row['cuenta']."' AND TB2.fuente = '".$row['fuente']."' AND TB2.indicador = '".$row['indicador_producto']."' AND TB2.bpin = '".$row['bpim']."'";

            $res_cxp_nom = mysqli_query($linkbd, $sqlr_cxp_nom);
            $row_cxp_nom = mysqli_fetch_row($res_cxp_nom);

            $valorRp = $row['SUM(TB2.valor)'] - $rowRev['SUM(valor)'];

            $saldoRp = $row['SUM(TB2.valor)'] - $rowRev['SUM(valor)'] - $rowCxp[0] - $row_cxp_nom[0];

            if($saldoRp > 0){

                if(substr($row['cuenta'],0,3) != '2.3'){
                    $rubro = $row['cuenta'].' - '.$row['fuente'];
                    $nombreRubro = buscacuentaccpetgastos($row['cuenta'], $maxVersion);
                }else{
                    $rubro = $row['indicador_producto'].' - '.$row['bpim'].' - '.$row['fuente'];
                    $nombreRubro = buescarNombreProyecto($row['bpim'], $row['vigencia']);
                }

                
                $nombreFuente = buscafuenteccpet($row['fuente']);
                

                $det = array();
                
                array_push($det, $rubro);
                array_push($det, $row['consvigencia']);
                array_push($det, $row['fecha']);
                array_push($det, round($valorRp, 2));
                array_push($det, str_replace($comas, "", buscatercero($row['tercero'])));
                array_push($det, $row['contrato']);
                array_push($det, round($saldoRp, 2));
                array_push($det, preg_replace("/[\r\n|\n|\r]+/", " ", str_replace($comas, "", $nombreFuente))); 

                array_push($reservas, $det);
                
            }
        }
        $out['reservas'] = $reservas;
    }

    if($action == 'vigenciasDelGasto'){
        $sql = "SELECT * FROM ccpet_vigenciadelgasto WHERE version = (SELECT MAX(version) FROM ccpet_vigenciadelgasto)";
        $res = mysqli_query($linkbd, $sql);
        $vigenciasDelGasto = array();

        while ($row = mysqli_fetch_row($res)) {
            array_push($vigenciasDelGasto, $row);
        }
        $out['vigenciasDelGasto'] = $vigenciasDelGasto;
    }

   
    header("Content-type: application/json");
    echo json_encode($out);
    die();