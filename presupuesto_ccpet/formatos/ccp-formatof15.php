<?php

	require_once '../../comun.inc';
    require '../../funciones.inc';
    session_start();

	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");

    $out = array('error' => false);

    $maxVersion = ultimaVersionGastosCCPET();

    $action = "show";

    if(isset($_GET['action'])){
        $action = $_GET['action'];
    }
    
    if($action == 'buscarSaldos'){

        $medioDePago = $_POST['medioDePago'];
        $vigenciaDelGasto = $_POST['vigenciaDelGasto'];

        $crit1 = '';
        $crit2 = '';
        $crit1Vig = '';
        $crit1VigTeso = '';
        $comas = [",", ";"];

        if($medioDePago != '-1'){
            $crit1 = "AND TB2.medio_pago = '$medioDePago'";
        }

        if($vigenciaDelGasto != '-1'){
            $crit2 = "AND (TB2.codigo_vigenciag = '2' OR TB2.codigo_vigenciag = '5')";
            $crit1VigTeso = "AND (T2.codigo_vigenciag = '2' OR T2.codigo_vigenciag = '5')";
            $crit1Vig = "AND (codigo_vigenciag = '2' OR codigo_vigenciag = '5')";
        }

        $cxp = [];

        $critFechaNom = '';
        $critNumContrato = '';

        if($_POST['fechaInicial'] != '' && $_POST['fechaFinal'] != ''){
            $critFecha = " AND TB1.fecha BETWEEN '".$_POST['fechaInicial']."' AND '".$_POST['fechaFinal']."'";
            $critFechaNom = " AND TB3.fecha BETWEEN '".$_POST['fechaInicial']."' AND '".$_POST['fechaFinal']."'";
            $critFechaNomEgreso = " AND T1.fecha BETWEEN '".$_POST['fechaInicial']."' AND '".$_POST['fechaFinal']."'";
        }


        $sqlr = "SELECT TB1.id_orden, TB2.cuentap, TB2.indicador_producto, TB2.bpim, TB2.fuente, TB1.fecha, SUM(TB2.valor), TB1.valorretenciones, TB1.id_rp, TB1.conceptorden, TB1.tercero, TB1.vigencia, TB1.valorpagar FROM tesoordenpago AS TB1, tesoordenpago_det AS TB2 WHERE TB1.id_orden = TB2.id_orden $critFecha AND (TB1.estado != 'R' AND TB1.estado != 'N') AND NOT EXISTS (SELECT 1 FROM tesoegresos WHERE tesoegresos.id_orden = TB1.id_orden and tesoegresos.estado='S' AND tesoegresos.fecha BETWEEN '".$_POST['fechaInicial']."' AND '".$_POST['fechaFinal']."') $crit1 $crit2 GROUP BY TB1.id_orden, TB2.cuentap, TB2.indicador_producto, TB2.bpim, TB2.fuente";
        $res = mysqli_query($linkbd, $sqlr);
        while($row = mysqli_fetch_assoc($res)){
            $valorRp = 0;
            $saldoPorEjecu = 0;
            $rubro = '';
            $nombreRubro = '';
            $tercero = '';
            $nombreFuente = "";

            $sqlrRp = "SELECT contrato FROM ccpetrp WHERE consvigencia = '$row[id_rp]' AND vigencia = '$row[vigencia]' AND tipo_mov = '201' ";
            $resRp = mysqli_query($linkbd, $sqlrRp);
            $rowRp = mysqli_fetch_row($resRp);


            /* $sqlr_cxp_nom = "SELECT SUM(TB2.valor) FROM hum_nom_cdp_rp AS TB1, humnom_presupuestal AS TB2 WHERE TB1.rp = '".$row['consvigencia']."' AND TB1.vigencia = '".$row['vigencia']."' AND TB1.nomina = TB2.id_nom AND TB2.estado = 'P' AND TB2.cuenta = '".$row['cuenta']."' AND TB2.fuente = '".$row['fuente']."' AND TB2.indicador = '".$row['indicador_producto']."' AND TB2.bpin = '".$row['bpim']."'"; */

            /* $res_cxp_nom = mysqli_query($linkbd, $sqlr_cxp_nom);
            $row_cxp_nom = mysqli_fetch_row($res_cxp_nom); */

            $valorCxp = $row['SUM(TB2.valor)'];

            $porcentajeRet = $row['SUM(TB2.valor)']/$row['valorpagar'];

            $valorRet = $porcentajeRet * $row['valorretenciones'];

            $valorNeto = $row['SUM(TB2.valor)'] - $valorRet;


            if($valorCxp == 0){
                continue;
            }
            
            if(substr($row['cuentap'],0,3) != '2.3'){
                $rubro = $row['cuentap'].' - '.$row['fuente'];
                $nombreRubro = buscacuentaccpetgastos($row['cuentap'], $maxVersion);
            }else{
                $rubro = $row['indicador_producto'].' - '.$row['bpim'].' - '.$row['fuente'];
                $nombreRubro = buescarNombreProyecto($row['bpim'], $row['vigencia']);
            }

            
            $nombreFuente = buscafuenteccpet($row['fuente']);
            

            $det = array();
            
            array_push($det, $rubro);
            array_push($det, $row['id_orden']);
            array_push($det, $row['tercero'] . ' - ' . str_replace($comas, "", buscatercero($row['tercero'])));
            array_push($det, $rowRp[0]);
            array_push($det, $row['fecha']);
            array_push($det, round($valorCxp, 2));
            array_push($det, $row['fuente'] . ' - ' . str_replace($comas, "", $nombreFuente));
            array_push($det, round($valorNeto, 2));
            array_push($det, round($valorRet, 0));

            array_push($cxp, $det);
        }

        /* $sqlr_cxp_nom = "SELECT SUM(TB2.valor) FROM hum_nom_cdp_rp AS TB1, humnom_presupuestal AS TB2 WHERE TB1.rp = '".$row['consvigencia']."' AND TB1.vigencia = '".$row['vigencia']."' AND TB1.nomina = TB2.id_nom AND TB2.estado = 'P' AND TB2.cuenta = '".$row['cuenta']."' AND TB2.fuente = '".$row['fuente']."' AND TB2.indicador = '".$row['indicador_producto']."' AND TB2.bpin = '".$row['bpim']."'"; */

       /*  $sqlr = "SELECT TB1.nomina, TB2.cuenta, TB2.indicador, TB2.bpin, TB2.fuente, TB3.fecha, SUM(TB2.valor), TB1.rp, TB1.vigencia FROM hum_nom_cdp_rp AS TB1, humnom_presupuestal AS TB2, humnomina AS TB3 WHERE TB1.nomina = TB2.id_nom AND TB2.id_nom = TB3.id_nom  $critFechaNom $crit1 $crit2 GROUP BY TB1.nomina, TB2.cuenta, TB2.indicador, TB2.bpin, TB2.fuente"; */

        $sqlr="SELECT T1.id_nom, T1.cuenta, T1.indicador, T1.bpin, T1.fuente, T1.valor, T2.fecha FROM humnom_presupuestal AS T1 INNER JOIN humnomina AS T2 ON T1.id_nom = T2.id_nom WHERE T1.estado = 'P' AND T2.fecha BETWEEN '".$_POST['fechaInicial']."' AND '".$_POST['fechaFinal']."' ORDER BY T1.id_nom";

        $res = mysqli_query($linkbd, $sqlr);
        while($row = mysqli_fetch_assoc($res)){
            $valorRp = 0;
            $saldoPorEjecu = 0;
            $rubro = '';
            $nombreRubro = '';
            $tercero = '';
            $nombreFuente = "";

            $restric = " AND NOT(T2.estado = 'N' AND T2.estado = 'R') AND NOT(T1.tipo = 'SE' OR T1.tipo = 'PE' OR T1.tipo = 'DS' OR T1.tipo = 'RE' OR T1.tipo = 'FS') AND T2.fecha BETWEEN '".$_POST['fechaInicial']."' AND '".$_POST['fechaFinal']."'";
            $sqlr1 = "SELECT SUM(T1.valordevengado) FROM tesoegresosnomina_det AS T1 INNER JOIN tesoegresosnomina AS T2 ON T1.id_egreso = T2.id_egreso WHERE T1.id_orden = '".$row['id_nom']."' AND T1.cuentap = '".$row['cuenta']."' AND T1.fuente = '".$row['fuente']."' AND T1.indicador_producto = '".$row['indicador']."' AND T1.bpin = '".$row['bpin']."' $restric";
            $resp1 = mysqli_query($linkbd, $sqlr1);
            $row1 = mysqli_fetch_row($resp1);
            $dif = $row['valor'] - $row1[0];

            $sqlrRpNom = "SELECT rp, vigencia FROM hum_nom_cdp_rp WHERE nomina = '".$row['id_nom']."' ";
            $resRpNom = mysqli_query($linkbd, $sqlrRpNom);
            $rowRpNom = mysqli_fetch_row($resRpNom);

            $sqlrRp = "SELECT contrato, tercero FROM ccpetrp WHERE consvigencia = '$rowRpNom[0]' AND vigencia = '$rowRpNom[1]' AND tipo_mov = '201' ";
            $resRp = mysqli_query($linkbd, $sqlrRp);
            $rowRp = mysqli_fetch_row($resRp);

           /*  $sqlrRp = "SELECT contrato, tercero FROM ccpetrp WHERE consvigencia = '$row[rp]' AND vigencia = '$row[vigencia]' AND tipo_mov = '201' ";
            $resRp = mysqli_query($linkbd, $sqlrRp);
            $rowRp = mysqli_fetch_row($resRp);

            $sqlrEgresoNom = "SELECT SUM(T2.valordevengado) FROM tesoegresosnomina AS T1, tesoegresosnomina_det AS T2 WHERE T1.id_egreso = T2.id_egreso AND T2.id_orden = '$row[nomina]' AND T1.estado != 'N' AND T2.cuentap = '$row[cuenta]' AND T2.fuente = '$row[fuente]' AND T2.indicador_producto = '$row[indicador]' AND T2.bpin = '$row[bpin]' AND NOT(T2.tipo='SE' OR T2.tipo='PE' OR T2.tipo='DS' OR T2.tipo='RE' OR T2.tipo='FS') $critFechaNomEgreso GROUP BY T2.id_orden, T2.cuentap, T2.indicador_producto, T2.bpin, T2.fuente";
            $resEgresoNom = mysqli_query($linkbd, $sqlrEgresoNom);
            $rowEgresoNom = mysqli_fetch_row($resEgresoNom);

            $valorCxp = $row['SUM(TB2.valor)'] - $rowEgresoNom[0];

            $valorNeto = $row['SUM(TB2.valor)']; */


            if($dif == 0){
                continue;
            }
            
            if(substr($row['cuenta'],0,3) != '2.3'){
                $rubro = $row['cuenta'].' - '.$row['fuente'];
                $nombreRubro = buscacuentaccpetgastos($row['cuenta'], $maxVersion);
            }else{
                $rubro = $row['indicador'].' - '.$row['bpin'].' - '.$row['fuente'];
                $nombreRubro = buescarNombreProyecto($row['bpin'], $row['vigencia']);
            }

            
            $nombreFuente = buscafuenteccpet($row['fuente']);
            

            $det = array();
            
            array_push($det, $rubro);
            array_push($det, $row['id_nom']);
            array_push($det, $rowRp[1] . ' - ' . str_replace($comas, "", buscatercero($rowRp[1])));
            array_push($det, $rowRp[0]);
            array_push($det, $row['fecha']);
            array_push($det, round($dif, 2));
            array_push($det, $row['fuente'] . ' - ' . str_replace($comas, "", $nombreFuente));
            array_push($det, round($dif, 2));
            array_push($det, round(0, 2));

            array_push($cxp, $det);
        }


        $out['cxp'] = $cxp;
    }

    if($action == 'vigenciasDelGasto'){
        $sql = "SELECT * FROM ccpet_vigenciadelgasto WHERE version = (SELECT MAX(version) FROM ccpet_vigenciadelgasto)";
        $res = mysqli_query($linkbd, $sql);
        $vigenciasDelGasto = array();

        while ($row = mysqli_fetch_row($res)) {
            array_push($vigenciasDelGasto, $row);
        }
        $out['vigenciasDelGasto'] = $vigenciasDelGasto;
    }

   
    header("Content-type: application/json");
    echo json_encode($out);
    die();