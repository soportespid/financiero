<?php

	require_once '../../comun.inc';
    require '../../funciones.inc';
    session_start();

	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");

    $out = array('error' => false);

    $maxVersion = ultimaVersionGastosCCPET();

    $action = "show";

    if(isset($_GET['action'])){
        $action = $_GET['action'];
    }
    
    if($action == 'buscarSaldos'){


        $crit1 = '';
        $crit2 = '';


        $contratos = array();

        $critFecha = '';

        

        $fechafin = $_POST['fechaFinal'];

        $dias = explode("-", $fechafin);

        $fechaini = $dias[0]."-".$dias[1]."-01";

        if($_POST['fechaInicial'] != '' && $_POST['fechaFinal'] != ''){
            $critFecha = " AND cab.fecha BETWEEN '".$_POST['fechaInicial']."' AND '".$_POST['fechaFinal']."'";
        }

        $sqlr = "SELECT * FROM tesobancosctas WHERE estado = 'S'";
        $res = mysqli_query($linkbd, $sqlr);
        while($row = mysqli_fetch_assoc($res)){
            $valorRp = 0;
            $saldoPorEjecu = 0;
            $rubro = '';
            $nombreRubro = '';
            $tercero = '';
            $comas = [];
            $nombreFuente = "";

            $sqlrIng = "SELECT  det.cuenta, (SUM(det.valdebito)) as saldof 
							FROM comprobante_cab AS cab 
							INNER JOIN comprobante_det AS det ON det.tipo_comp = cab.tipo_comp AND det.numerotipo = cab.numerotipo
							WHERE det.cuenta = '$row[cuenta]' $critFecha AND cab.estado = '1' AND cab.tipo_comp < 2000 
							GROUP BY det.cuenta 
							ORDER BY cab.tipo_comp, cab.numerotipo, det.id_det, cab.fecha";

            $resIng = mysqli_query($linkbd, $sqlrIng);
            $saldoperant = 0;	
            $rowIng = mysqli_fetch_row($resIng);
            $saldoIngresos = round($rowIng[1], 2);

            $sqlrbook = "SELECT  det.cuenta, (SUM(det.valdebito)-SUM(det.valcredito)) as saldof 
							FROM comprobante_cab AS cab 
							INNER JOIN comprobante_det AS det ON det.tipo_comp = cab.tipo_comp AND det.numerotipo = cab.numerotipo
							WHERE det.cuenta = '$row[cuenta]' AND cab.fecha BETWEEN '' AND '".$_POST['fechaFinal']."' AND cab.estado = '1' 
							GROUP BY det.cuenta 
							ORDER BY cab.tipo_comp, cab.numerotipo, det.id_det, cab.fecha";
            $resbook = mysqli_query($linkbd, $sqlrbook);
            $saldoLibro = 0;	
            $rowbook = mysqli_fetch_row($resbook);
            $saldoLibro = round($rowbook[1], 2);
            
            $sqlr1 = "SELECT * FROM conciliacion_cab WHERE cuenta = '$row[cuenta]' AND periodo1 = '$fechaini' AND periodo2 = '$fechafin'";
            $resp1 = mysqli_query($linkbd, $sqlr1);
            $row1 = mysqli_fetch_row($resp1);
            $saldoExtracto = round($row1[1],2);

            $sqlrDest = "SELECT nombre FROM cuentasnicsp WHERE cuenta = '$row[cuenta]'";
            $respDest = mysqli_query($linkbd, $sqlrDest);
            $rowDest = mysqli_fetch_row($respDest);
            
            $comas = [",", ";"];

            $nombreTercero = buscatercero($row['tercero']);

            $det = array();
            array_push($det, $nombreTercero);
            array_push($det, $row['cuenta']);
            array_push($det, $row['ncuentaban']);
            array_push($det, str_replace($comas, "", $rowDest[0]));
            array_push($det, $saldoIngresos);
            array_push($det, $saldoLibro);
            array_push($det, $saldoExtracto);
            array_push($det, $saldoLibro);

            array_push($contratos, $det);
            
            /* array_push($contratos, $row); */
        }
        $out['contratos'] = $contratos;
    }

    if($action == 'vigenciasDelGasto'){
        $sql = "SELECT * FROM ccpet_vigenciadelgasto WHERE version = (SELECT MAX(version) FROM ccpet_vigenciadelgasto)";
        $res = mysqli_query($linkbd, $sql);
        $vigenciasDelGasto = array();

        while ($row = mysqli_fetch_row($res)) {
            array_push($vigenciasDelGasto, $row);
        }
        $out['vigenciasDelGasto'] = $vigenciasDelGasto;
    }

   
    header("Content-type: application/json");
    echo json_encode($out);
    die();