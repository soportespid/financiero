<?php

	require_once '../../comun.inc';
    require '../../funciones.inc';
    session_start();

	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");

    $out = array('error' => false);

    $maxVersion = ultimaVersionGastosCCPET();

    $action = "show";

    if(isset($_GET['action'])){
        $action = $_GET['action'];
    }
    
    if($action == 'buscarSaldos'){

        $actosAdministrativos = [];

        $critFecha = '';

        $tipoActo = [
            1 => 'ACUERDO',
            2 => 'RESOLUCION',
            3 => 'DECRETO',
        ];

        $item = 1;

        $comas = [",", ";"];

        if($_POST['fechaInicial'] != '' && $_POST['fechaFinal'] != ''){
            $critFecha = " AND fecha BETWEEN '".$_POST['fechaInicial']."' AND '".$_POST['fechaFinal']."'";
        }

        $sqlr = "SELECT tipo_acto_adm, consecutivo, fecha, valoradicion, valorreduccion FROM ccpetacuerdos WHERE estado != 'N' AND (valoradicion > 0 OR valorreduccion > 0) $critFecha ORDER BY id_acuerdo";
        $res = mysqli_query($linkbd, $sqlr);


        /* $sqlr = "SELECT TB1.consvigencia, TB2.cuenta, TB2.indicador_producto, TB2.bpim, TB2.fuente, TB1.fecha, SUM(TB2.valor), TB1.idcdp, TB1.contrato, TB1.detalle, TB1.tercero, TB1.vigencia FROM ccpetrp AS TB1, ccpetrp_detalle AS TB2 WHERE TB1.vigencia = TB2.vigencia AND TB1.consvigencia = TB2.consvigencia $critFecha AND TB1.tipo_mov = TB2.tipo_mov AND TB1.tipo_mov = '201' GROUP BY TB1.consvigencia, TB2.cuenta, TB2.indicador_producto, TB2.bpim, TB2.fuente";
        $res = mysqli_query($linkbd, $sqlr); */
        while($row = mysqli_fetch_assoc($res)){

            $acto_adm = $tipoActo[$row['tipo_acto_adm']];
            $fechaPartes = explode("-", $row['fecha']);

            $fecha = $fechaPartes[0].'/'.$fechaPartes[1].'/'.$fechaPartes[2];

            $det = [];
            array_push($det, $item);
            array_push($det, $acto_adm);
            array_push($det, $row['consecutivo']);
            array_push($det, $fecha);
            array_push($det, round($row['valoradicion'], 2));
            array_push($det, round($row['valorreduccion'], 2));
            array_push($det, '');

            array_push($actosAdministrativos, $det);

            $item++;
            
        }
        $out['actosAdministrativos'] = $actosAdministrativos;
    }


   
    header("Content-type: application/json");
    echo json_encode($out);
    die();