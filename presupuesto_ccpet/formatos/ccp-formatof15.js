/* 
const DIR_ORIGIN = window.location.origin + '/';
const DIR_PATHNAME = window.location.pathname.split("/", 2)[1] + '/';
const DIR_FULL = DIR_ORIGIN + '/' + DIR_PATHNAME; */

/* const URL = 'presupuesto_ccpet/AuxiliarGastos/ccp-auxiliarporcomprobantedata.php';
const URL = `presupuesto_ccpet/contrato/ccp-buscarContratos.php?action=buscarContratos`; */

const app = Vue.createApp({

    data() {
      return {
        detalles: [],
        numContrato: '',
        loading: false,
        optionsMediosPagos: [
          { text: 'Todos', value: '-1' },
          { text: 'CSF', value: 'CSF' },
          { text: 'SSF', value: 'SSF' }
        ],
        selectMedioPago: '-1',
        vigenciasdelgasto: [],
        selectVigenciaGasto: '-1',
      }
    },

    mounted() {
        this.loading = false;
        this.vigenciasDelgasto();
    },

    computed: {
      existeInformacion(){
        const det = JSON.parse(JSON.stringify(this.detalles));
        return det.length > 0 ? true : false
      },

    },

    methods: {

        downloadExl() {

            let wb = XLSX.utils.table_to_book(document.getElementById('tableId')),
                wopts = {
                    bookType: 'xls',
                    bookSST: false,
                    type: 'binary'
                },
                wbout = XLSX.write(wb, wopts);

                saveAs(new Blob([this.s2ab(wbout)], {
                type: "application/octet-stream;charset=utf-8"
                            }), "CONSTITUCION CUENTAS POR PAGAR.xls");
        },

        s2ab(s) {
            if (typeof ArrayBuffer !== 'undefind') {
                var buf = new ArrayBuffer(s.length);
                var view = new Uint8Array(buf);
                for (var i = 0; i != s.length; ++i) view[i] = s.charCodeAt(i) & 0xFF;
                return buf;
            } else {
                var buf = new Array(s.length);
                for (var i = 0; i != s.length; ++i) buf[i] = s.charCodeAt(i) & 0xFF;
                return buf;
            }
        },

        async quitarComas(e){
          const t = await e.replace(',', ' ').replace(';', ' ');
          return t;
        },

        downloadCsv(){     

          let fechaFinal = document.getElementById('fc_1198971546').value;

          let arrFechaFinal = fechaFinal.split("/");
          const nombreArchivo = `formato_${arrFechaFinal[2]}${arrFechaFinal[1]}_F15_CDM.csv`;

          const dataDet = this.detalles;
          let csvContent = "data:text/csv;charset=utf-8," + dataDet.map(e => e.join(",")).join("\n");
          var encodedUri = encodeURI(csvContent);
          var link = document.createElement("a");
          link.setAttribute("href", encodedUri);
          link.setAttribute("download", nombreArchivo);
          document.body.appendChild(link); 
          link.click();

        },


      async buscarSaldos() {

        let fechaInicial = document.getElementById('fc_1198971545').value;
        let fechaFinal = document.getElementById('fc_1198971546').value;

        if(fechaInicial && fechaFinal){

          if(fechaInicial && fechaFinal){
            let arrFechaInicial = fechaInicial.split("/");
            fechaInicial = `${arrFechaInicial[2]}-${arrFechaInicial[1]}-${arrFechaInicial[0]}`;
            let arrFechaFinal = fechaFinal.split("/");
            fechaFinal = `${arrFechaFinal[2]}-${arrFechaFinal[1]}-${arrFechaFinal[0]}`;
          }else{
            fechaInicial = '';
            fechaFinal = '';
          }

          let formData = new FormData();
          formData.append("medioDePago", this.selectMedioPago);
          formData.append("vigenciaDelGasto", this.selectVigenciaGasto);
          formData.append("fechaInicial", fechaInicial);
          formData.append("fechaFinal", fechaFinal);


          const URL = `presupuesto_ccpet/formatos/ccp-formatof15.php?action=buscarSaldos`;

          this.loading =  true;

          await axios.post(URL, formData)
            .then((response) => {
              console.log(response.data);
              this.detalles = response.data.cxp;
              
            }).finally(() => {
                this.loading =  false;
            });
            
        }else{

          Swal.fire(
              'Falta informaci&oacute;n para generar reporte.',
              'Verifique la fecha inicial y final tengan informaci&oacute;n para realizar la busqueda.',
              'warning'
          );

        }

      },

      toFormData(obj){
        var form_data = new FormData();
        for(var key in obj){
            form_data.append(key, obj[key]);
        }
        return form_data;
      },

      formatonumero: function(valor){
        return new Intl.NumberFormat("en-US", {style: "currency", currency: "USD"}).format(valor);
      },

      reiniciarVariable(){
        this.detalles = [];
      },

      vigenciasDelgasto: async function(){
        
        const URL = `presupuesto_ccpet/formatos/ccp-formatof15.php?action=vigenciasDelGasto`;

        await axios.post(URL)
            .then(
                (response) => {
                    this.vigenciasdelgasto = response.data.vigenciasDelGasto;
                    /* this.selectVigenciaGasto = this.vigenciasdelgasto[0][0]; */
                }
            );
    },

    }
})
  
app.mount('#myapp')