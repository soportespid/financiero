<?php

	require_once '../../comun.inc';
    require '../../funciones.inc';
    session_start();

	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");

    $out = array('error' => false);

    $maxVersion = ultimaVersionGastosCCPET();

    $action = "show";

    if(isset($_GET['action'])){
        $action = $_GET['action'];
    }

    if($action == 'buscarSaldos'){

        $medioDePago = $_POST['medioDePago'];
        $vigenciaDelGasto = $_POST['vigenciaDelGasto'];

        $crit1 = '';
        $crit2 = '';


        if($medioDePago != '-1'){
            $crit1 = "AND TB2.medio_pago = '$medioDePago'";
        }

        if($vigenciaDelGasto != '-1'){
            $crit2 = "AND TB2.codigo_vigenciag = '$vigenciaDelGasto'";
        }

        $contratos = array();

        $critFecha = '';
        $critNumContrato = '';

        if($_POST['fechaInicial'] != '' && $_POST['fechaFinal'] != ''){
            $critFecha = " AND fecha BETWEEN '".$_POST['fechaInicial']."' AND '".$_POST['fechaFinal']."'";
        }

        $sqlr = "SELECT TB1.consvigencia, TB2.cuenta, TB2.indicador_producto, TB2.bpim, TB2.fuente, TB1.fecha, SUM(TB2.valor), TB1.idcdp, TB1.contrato, TB1.detalle, TB1.tercero, TB1.vigencia FROM ccpetrp AS TB1, ccpetrp_detalle AS TB2 WHERE TB1.vigencia = TB2.vigencia AND TB1.consvigencia = TB2.consvigencia $critFecha AND TB1.tipo_mov = TB2.tipo_mov AND TB1.tipo_mov = '201' $crit1 $crit2 GROUP BY TB1.consvigencia, TB2.cuenta, TB2.indicador_producto, TB2.bpim, TB2.fuente";
        $res = mysqli_query($linkbd, $sqlr);
        while($row = mysqli_fetch_assoc($res)){
            $valorRp = 0;
            $saldoPorEjecu = 0;
            $rubro = '';
            $nombreRubro = '';
            $tercero = '';
            $comas = [];
            $nombreFuente = "";

            $sqlrRev = "SELECT SUM(valor) FROM ccpetrp_detalle WHERE cuenta = '$row[cuenta]' AND indicador_producto = '$row[indicador_producto]' AND bpim = '$row[bpim]' AND fuente = '$row[fuente]' AND consvigencia = '$row[consvigencia]' AND vigencia = '$row[vigencia]' AND SUBSTRING(tipo_mov, 1, 1) LIKE '4%'";
            $resRev = mysqli_query($linkbd, $sqlrRev);
            $rowRev = mysqli_fetch_assoc($resRev);

            $valorRp = $row['SUM(TB2.valor)'] - $rowRev['SUM(valor)'];

            $comas = [",", ";"];

            //buscar saldos de cdp

            $sqrlSaldoCdp = "SELECT SUM(TB2.valor) FROM ccpetcdp AS TB1, ccpetcdp_detalle AS TB2 WHERE TB1.consvigencia = TB2.consvigencia AND TB1.vigencia = TB2.vigencia $critFecha AND TB2.cuenta = '$row[cuenta]' AND TB2.indicador_producto = '$row[indicador_producto]' AND TB2.bpim = '$row[bpim]' AND TB2.fuente = '$row[fuente]' AND TB2.consvigencia = '$row[idcdp]' AND TB2.vigencia = '$row[vigencia]' AND SUBSTRING(TB2.tipo_mov, 1, 1) LIKE '2%' AND SUBSTRING(TB1.tipo_mov, 1, 1) LIKE '2%' GROUP BY TB1.consvigencia, TB2.cuenta, TB2.indicador_producto, TB2.bpim, TB2.fuente";
            $reslSaldoCdp = mysqli_query($linkbd, $sqrlSaldoCdp);
            $rowlSaldoCdp = mysqli_fetch_assoc($reslSaldoCdp);
            $saldocdp = $rowlSaldoCdp['SUM(TB2.valor)'];

            $sqrlSaldoCdpRev = "SELECT SUM(TB2.valor) FROM ccpetcdp AS TB1, ccpetcdp_detalle AS TB2 WHERE TB1.consvigencia = TB2.consvigencia AND TB1.vigencia = TB2.vigencia $critFecha AND TB2.cuenta = '$row[cuenta]' AND TB2.indicador_producto = '$row[indicador_producto]' AND TB2.bpim = '$row[bpim]' AND TB2.fuente = '$row[fuente]' AND TB2.consvigencia = '$row[idcdp]' AND TB2.vigencia = '$row[vigencia]' AND SUBSTRING(TB2.tipo_mov, 1, 1) LIKE '4%' AND SUBSTRING(TB1.tipo_mov, 1, 1) LIKE '4%' GROUP BY TB1.consvigencia, TB2.cuenta, TB2.indicador_producto, TB2.bpim, TB2.fuente";
            $reslSaldoCdpRev = mysqli_query($linkbd, $sqrlSaldoCdpRev);
            $rowlSaldoCdpRev = mysqli_fetch_assoc($reslSaldoCdpRev);
            $saldocdpRev = $rowlSaldoCdpRev['SUM(TB2.valor)'];

            $saldocdpTotal = $saldocdp - $saldocdpRev;

            //saldos de pagos

            $sqlrPago = "SELECT SUM(TB2.valor) FROM tesoordenpago AS TB1, tesoordenpago_det AS TB2 WHERE  TB1.id_orden = TB2.id_orden AND TB1.id_rp = '$row[consvigencia]' $critFecha AND TB1.vigencia = '$row[vigencia]' AND TB1.estado = 'P' AND TB2.cuentap = '$row[cuenta]' AND TB2.indicador_producto = '$row[indicador_producto]' AND TB2.bpim = '$row[bpim]' AND TB2.fuente = '$row[fuente]'";
            $resPago = mysqli_query($linkbd, $sqlrPago);
            $rowPago = mysqli_fetch_assoc($resPago);
            $saldopago = $rowPago['SUM(TB2.valor)'];

            $sqlrPagoNom = "SELECT SUM(TB2.valordevengado) FROM tesoegresosnomina AS TB1, tesoegresosnomina_det AS TB2 WHERE  TB1.id_egreso = TB2.id_egreso $critFecha AND TB1.estado = 'S' AND TB2.cuentap = '$row[cuenta]' AND TB2.indicador_producto = '$row[indicador_producto]' AND TB2.bpin = '$row[bpim]' AND TB2.fuente = '$row[fuente]'";
            $resPagoNom = mysqli_query($linkbd, $sqlrPagoNom);
            $rowPagoNom = mysqli_fetch_assoc($resPagoNom);
            $saldopagoNom = $rowPagoNom['SUM(TB2.valor)'];


            $saldopagoTotal = $saldopago - $saldopagoNom;


            if(substr($row['cuenta'],0,3) != '2.3'){
                $rubro = $row['cuenta'].' - '.$row['fuente'];
                $nombreRubro = buscacuentaccpetgastos($row['cuenta'], $maxVersion);
            }else{
                $rubro = $row['indicador_producto'].' - '.$row['bpim'].' - '.$row['fuente'];
                $nombreRubro = buescarNombreProyecto($row['bpim'], $row['vigencia']);
            }

            /* $nombreFuente = buscafuenteccpet($row['fuente']); */

            $sqlrFuentesF05 = "SELECT fuentef05 FROM ccpet_homologarfuentesf05 WHERE fuenteCuipo = '$row[fuente]'";
            $resFuentesF05 = mysqli_query($linkbd, $sqlrFuentesF05);
            $rowFuentesF05 = mysqli_fetch_assoc($resFuentesF05);

            if($rowFuentesF05['fuentef05'] != ''){
                $nombreFuente = $rowFuentesF05['fuentef05'];
            }else{
                $nombreFuente = buscafuenteccpet($row['fuente']);
            }

            //buscar fecha del cdp

            $sqlrFecha = "SELECT fecha FROM ccpetcdp WHERE consvigencia = '$row[idcdp]' AND vigencia = '$row[vigencia]' ";
            $resFecha = mysqli_query($linkbd, $sqlrFecha);
            $rowFecha = mysqli_fetch_assoc($resFecha);
            $fechaCdp = $rowFecha['fecha'];

            $det = [];
            array_push($det, $row['consvigencia']);
            array_push($det, $rubro);
            array_push($det, str_replace($comas, "", $nombreRubro));
            array_push($det, str_replace($comas, "", $nombreFuente));
            array_push($det, $row['fecha']);
            array_push($det, $valorRp);
            array_push($det, $row['idcdp']);
            array_push($det, $row['contrato']);
            array_push($det, preg_replace("/[\r\n|\n|\r]+/", " ", str_replace($comas, "", $row['detalle'])));
            array_push($det, str_replace($comas, "", buscatercero($row['tercero'])));
            array_push($det, $row['tercero']);
            array_push($det, $fechaCdp);
            array_push($det, $saldocdpTotal);
            array_push($det, $saldopagoTotal);

            array_push($contratos, $det);

            /* array_push($contratos, $row); */
        }

        $sqlrBaseReg = "SELECT base, usuario FROM redglobal WHERE unirreporte = 'S' AND cuipo = 'N'";
        $respBaseReg = mysqli_query($linkbd, $sqlrBaseReg);
        $rowBaseReg = mysqli_fetch_row($respBaseReg);

        $linkmulti = conectar_Multi($rowBaseReg[0], $rowBaseReg[1]);
	    $linkmulti -> set_charset("utf8");

        $sqlr = "SELECT TB1.consvigencia, TB2.cuenta, TB2.indicador_producto, TB2.bpim, TB2.fuente, TB1.fecha, SUM(TB2.valor), TB1.idcdp, TB1.contrato, TB1.detalle, TB1.tercero, TB1.vigencia FROM ccpetrp AS TB1, ccpetrp_detalle AS TB2 WHERE TB1.vigencia = TB2.vigencia AND TB1.consvigencia = TB2.consvigencia $critFecha AND TB1.tipo_mov = TB2.tipo_mov AND TB1.tipo_mov = '201' $crit1 $crit2 GROUP BY TB1.consvigencia, TB2.cuenta, TB2.indicador_producto, TB2.bpim, TB2.fuente";
        $res = mysqli_query($linkmulti, $sqlr);
        while($row = mysqli_fetch_assoc($res)){
            $valorRp = 0;
            $saldoPorEjecu = 0;
            $rubro = '';
            $nombreRubro = '';
            $tercero = '';
            $comas = [];
            $nombreFuente = "";

            $sqlrRev = "SELECT SUM(valor) FROM ccpetrp_detalle WHERE cuenta = '$row[cuenta]' AND indicador_producto = '$row[indicador_producto]' AND bpim = '$row[bpim]' AND fuente = '$row[fuente]' AND consvigencia = '$row[consvigencia]' AND vigencia = '$row[vigencia]' AND SUBSTRING(tipo_mov, 1, 1) LIKE '4%'";
            $resRev = mysqli_query($linkmulti, $sqlrRev);
            $rowRev = mysqli_fetch_assoc($resRev);

            $valorRp = $row['SUM(TB2.valor)'] - $rowRev['SUM(valor)'];

            $comas = [",", ";"];

            //buscar saldos de cdp

            $sqrlSaldoCdp = "SELECT SUM(TB2.valor) FROM ccpetcdp AS TB1, ccpetcdp_detalle AS TB2 WHERE TB1.consvigencia = TB2.consvigencia AND TB1.vigencia = TB2.vigencia $critFecha AND TB2.cuenta = '$row[cuenta]' AND TB2.indicador_producto = '$row[indicador_producto]' AND TB2.bpim = '$row[bpim]' AND TB2.fuente = '$row[fuente]' AND TB2.consvigencia = '$row[idcdp]' AND TB2.vigencia = '$row[vigencia]' AND SUBSTRING(TB2.tipo_mov, 1, 1) LIKE '2%' AND SUBSTRING(TB1.tipo_mov, 1, 1) LIKE '2%' GROUP BY TB1.consvigencia, TB2.cuenta, TB2.indicador_producto, TB2.bpim, TB2.fuente";
            $reslSaldoCdp = mysqli_query($linkmulti, $sqrlSaldoCdp);
            $rowlSaldoCdp = mysqli_fetch_assoc($reslSaldoCdp);
            $saldocdp = $rowlSaldoCdp['SUM(TB2.valor)'];

            $sqrlSaldoCdpRev = "SELECT SUM(TB2.valor) FROM ccpetcdp AS TB1, ccpetcdp_detalle AS TB2 WHERE TB1.consvigencia = TB2.consvigencia AND TB1.vigencia = TB2.vigencia $critFecha AND TB2.cuenta = '$row[cuenta]' AND TB2.indicador_producto = '$row[indicador_producto]' AND TB2.bpim = '$row[bpim]' AND TB2.fuente = '$row[fuente]' AND TB2.consvigencia = '$row[idcdp]' AND TB2.vigencia = '$row[vigencia]' AND SUBSTRING(TB2.tipo_mov, 1, 1) LIKE '4%' AND SUBSTRING(TB1.tipo_mov, 1, 1) LIKE '4%' GROUP BY TB1.consvigencia, TB2.cuenta, TB2.indicador_producto, TB2.bpim, TB2.fuente";
            $reslSaldoCdpRev = mysqli_query($linkmulti, $sqrlSaldoCdpRev);
            $rowlSaldoCdpRev = mysqli_fetch_assoc($reslSaldoCdpRev);
            $saldocdpRev = $rowlSaldoCdpRev['SUM(TB2.valor)'];

            $saldocdpTotal = $saldocdp - $saldocdpRev;

            //saldos de pagos

            $sqlrPago = "SELECT SUM(TB2.valor) FROM tesoordenpago AS TB1, tesoordenpago_det AS TB2 WHERE  TB1.id_orden = TB2.id_orden AND TB1.id_rp = '$row[consvigencia]' $critFecha AND TB1.vigencia = '$row[vigencia]' AND TB1.estado = 'S' AND TB2.cuentap = '$row[cuenta]' AND TB2.indicador_producto = '$row[indicador_producto]' AND TB2.bpim = '$row[bpim]' AND TB2.fuente = '$row[fuente]'";
            $resPago = mysqli_query($linkmulti, $sqlrPago);
            $rowPago = mysqli_fetch_assoc($resPago);
            $saldopago = $rowPago['SUM(TB2.valor)'];

            $sqlrPagoNom = "SELECT SUM(TB2.valordevengado) FROM tesoegresosnomina AS TB1, tesoegresosnomina_det AS TB2 WHERE  TB1.id_egreso = TB2.id_egreso $critFecha AND TB1.estado = 'P' AND TB2.cuentap = '$row[cuenta]' AND TB2.indicador_producto = '$row[indicador_producto]' AND TB2.bpin = '$row[bpim]' AND TB2.fuente = '$row[fuente]'";
            $resPagoNom = mysqli_query($linkmulti, $sqlrPagoNom);
            $rowPagoNom = mysqli_fetch_assoc($resPagoNom);
            $saldopagoNom = $rowPagoNom['SUM(TB2.valor)'];


            $saldopagoTotal = $saldopago - $saldopagoNom;


            if(substr($row['cuenta'],0,3) != '2.3'){
                $rubro = $row['cuenta'].' - '.$row['fuente'];
                $nombreRubro = buscacuentaccpetgastos($row['cuenta'], $maxVersion);
            }else{
                $rubro = $row['indicador_producto'].' - '.$row['bpim'].' - '.$row['fuente'];
                $nombreRubro = buescarNombreProyecto($row['bpim'], $row['vigencia'], $rowBaseReg[0], $rowBaseReg[1]);
            }

            /* $nombreFuente = buscafuenteccpet($row['fuente']); */

            $sqlrFuentesF05 = "SELECT fuentef05 FROM ccpet_homologarfuentesf05 WHERE fuenteCuipo = '$row[fuente]'";
            $resFuentesF05 = mysqli_query($linkmulti, $sqlrFuentesF05);
            $rowFuentesF05 = mysqli_fetch_assoc($resFuentesF05);

            if($rowFuentesF05['fuentef05'] != ''){
                $nombreFuente = $rowFuentesF05['fuentef05'];
            }else{
                $nombreFuente = buscafuenteccpet($row['fuente'], $rowBaseReg[0], $rowBaseReg[1]);
            }

            //buscar fecha del cdp

            $sqlrFecha = "SELECT fecha FROM ccpetcdp WHERE consvigencia = '$row[consvigencia]' AND vigencia = '$row[vigencia]' ";
            $resFecha = mysqli_query($linkmulti, $sqlrFecha);
            $rowFecha = mysqli_fetch_assoc($resFecha);
            $fechaCdp = $rowFecha['fecha'];

            $det = array();
            array_push($det, $row['consvigencia']);
            array_push($det, $rubro);
            array_push($det, str_replace($comas, "", $nombreRubro));
            array_push($det, str_replace($comas, "", $nombreFuente));
            array_push($det, $row['fecha']);
            array_push($det, $valorRp);
            array_push($det, $row['idcdp']);
            array_push($det, $row['contrato']);
            array_push($det, preg_replace("/[\r\n|\n|\r]+/", " ", str_replace($comas, "", $row['detalle'])));
            array_push($det, str_replace($comas, "", buscatercero($row['tercero'])));
            array_push($det, $row['tercero']);
            array_push($det, $fechaCdp);
            array_push($det, $saldocdpTotal);
            array_push($det, $saldopagoTotal);

            array_push($contratos, $det);

            /* array_push($contratos, $row); */
        }


        $out['contratos'] = $contratos;
    }

    if($action == 'vigenciasDelGasto'){
        $sql = "SELECT * FROM ccpet_vigenciadelgasto WHERE version = (SELECT MAX(version) FROM ccpet_vigenciadelgasto)";
        $res = mysqli_query($linkbd, $sql);
        $vigenciasDelGasto = array();

        while ($row = mysqli_fetch_row($res)) {
            array_push($vigenciasDelGasto, $row);
        }
        $out['vigenciasDelGasto'] = $vigenciasDelGasto;
    }


    header("Content-type: application/json");
    echo json_encode($out);
    die();
