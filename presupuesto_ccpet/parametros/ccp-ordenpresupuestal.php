<?php

    require_once '../../comun.inc';
    require '../../funciones.inc';
 
    session_start();

	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");

    $out = array('error' => false);

    $action = "show";

    if(isset($_GET['action'])){
        $action = $_GET['action'];
    }
    
    if($action == 'show'){

        $ordenPresu = 0;

        $sqlr = "SELECT orden FROM ccpet_parametros";
        $res = mysqli_query($linkbd, $sqlr);
        $row = mysqli_fetch_row($res);
        $ordenPresu = $row[0];
        
        $out['ordenPresu'] = $ordenPresu;

    }

    if($action == 'guardarOrdenPresu'){
       
        $user = $_SESSION['nickusu'];
        $ordenPresu = $_POST['orden'];
        $consecutivo = 0;
        $sqlr = "DELETE FROM ccpet_parametros";
        mysqli_query($linkbd, $sqlr);

        $sqlrI = "INSERT INTO ccpet_parametros (orden, user) VALUES ('".$ordenPresu."', '".$user."')";
        mysqli_query($linkbd, $sqlrI);
        
        $out['insertaBien'] = true;
        

    }

    header("Content-type: application/json");
    echo json_encode($out);
    die();