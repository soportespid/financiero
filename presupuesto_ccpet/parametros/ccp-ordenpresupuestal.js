
const URL = 'presupuesto_ccpet/parametros/ccp-ordenpresupuestal.php';

const app = Vue.createApp({
    data() {
      return {
        ordenPresu: 1,
        loading: false,
      }
    },

    mounted() {
		  this.loading = false;
          this.cargarParametros();
	  },

    computed: {
      
    },

    methods: {
        
        getParametros(name){
            //Captura parametros
            name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
            var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
            results = regex.exec(location.search);
            return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
        },

        async cargarParametros() {

            this.loading = true;

            /* this.tipo_comp = this.getParametros('tipo_comprobante');
            let formData = new FormData();
            formData.append("tipo_comp", this.tipo_comp); */
            
            await axios.post(URL)
            .then((response) => {
                if(response.data.ordenPresu != null)
                    this.ordenPresu = response.data.ordenPresu;
                
            }).catch((error) => {
                this.error = true;
                console.log(error)
            }).finally(() => {
                this.loading = false;
            });     
        },

        //metodos par aeste archivo


        guardarOrdenPresu(){

            Swal.fire({
                title: 'Esta seguro de guardar?',
                text: "Guardar orden presupuestal en la base de datos, confirmar campos!",
                icon: 'question',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Si, guardar!'
            }).then((result) => {
                if (result.isConfirmed) {
                    var formData = new FormData();

                    formData.append("orden", this.ordenPresu);
                
                    axios.post(URL + '?action=guardarOrdenPresu', formData)
                    .then((response) => {
                        
                        if(response.data.insertaBien){
                            Swal.fire({
                                position: 'top-end',
                                icon: 'success',
                                title: 'El orden presupuestal se guard&oacute; con Exito',
                                showConfirmButton: false,
                                timer: 1500
                            }).then((response) => {
                                    this.redireccionar();
                                });
                        }else{
                        
                            Swal.fire(
                                'Error!',
                                'No se pudo guardar.',
                                'error'
                            );
                        }
                        
                    });
                    
                }
            });
            
        },

        redireccionar(){
            
            location.href ="ccp-ordenpresupuestal.php";
        },

        toFormData(obj){
            var form_data = new FormData();
            for(var key in obj){
                form_data.append(key, obj[key]);
            }
            return form_data;
        },

        formatonumero: function(valor){
            return new Intl.NumberFormat("en-US", {style: "currency", currency: "USD"}).format(valor);
        },
    }
})

app.mount('#myapp')