<?php
    $URL_BBDD = true;
	require_once '../../../comun.inc';
    require '../../../funciones.inc';
    require '../../../conversor.php';
    session_start();

	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");

    $out = array('error' => false);

    $action = "show";

    if(isset($_GET['action'])){
        $action = $_GET['action'];
    }

    if ($action == "datosFormulario") {

        $maxVersion = ultimaVersionGastosCCPET();
        $maxVersionIngresos = ultimaVersionIngresosCCPET();

        $acuerdoId = $_GET['acuerdoId'];

        $sqlAcuerdo = "SELECT numero_acuerdo, fecha, valortraslado, YEAR(fecha), consecutivo, estado FROM ccpetacuerdos WHERE id_acuerdo = $acuerdoId";
        $rowAcuerdo = mysqli_fetch_row(mysqli_query($linkbd, $sqlAcuerdo));


        $out['fecha'] = date("d/m/Y", strtotime($rowAcuerdo[1]));
        $out['actoAdm'] = $rowAcuerdo[0];
        $out['actoAdmId'] = $rowAcuerdo[4];
        $out['valorAdicion'] = $rowAcuerdo[2];
        $out['vigencia'] = $rowAcuerdo[3];
        $estado = $rowAcuerdo[5] == 'S' ? 'ACTIVO' : 'FINALIZADO';

        $out['estado'] = $estado;
        $out['letras'] = convertir($rowAcuerdo[2]);

        $detalles = [];
        $totalGastos = 0;
        $totalIngresos = 0;

        $sqlAdiciones = "SELECT tipo, tipo_gasto, seccion_presupuestal, medio_pago, codigo_vigenciag, bpim, programatico, cuenta, fuente, valor FROM ccpet_traslados WHERE id_acuerdo = $acuerdoId AND vigencia = '$rowAcuerdo[3]'";
        $resAdiciones = mysqli_query($linkbd, $sqlAdiciones);
        while ($rowAdiciones = mysqli_fetch_row($resAdiciones)) {

            $datos = [];

            $tipoGasto = "";

            $tiposDeGasto = ['2.1' => 'Funcionamiento', '2.2' => 'Servicio a la deuda', '2.4' => 'Gastos de comercializacion'];

            if ($rowAdiciones[5] !== '') {
                $tipoGasto = "Inversión";
            }
            else {
                $inicioCuenta = substr($rowAdiciones[7], 0, 3);
                $tipoGasto = $tiposDeGasto[$inicioCuenta];
            }

            $sqlSecPresupuestal = "SELECT nombre FROM pptoseccion_presupuestal WHERE id_seccion_presupuestal = '$rowAdiciones[2]'";
            $rowSecPresupuestal = mysqli_fetch_row(mysqli_query($linkbd, $sqlSecPresupuestal));

            $secPresupuestal = $rowAdiciones[2] . " - " . $rowSecPresupuestal[0];

            $sqlVigGasto = "SELECT nombre FROM ccpet_vigenciadelgasto WHERE  codigo = '$rowAdiciones[4]'";
            $rowVigGasto = mysqli_fetch_row(mysqli_query($linkbd, $sqlVigGasto));

            $vigGasto = $rowAdiciones[4] . " - " . $rowVigGasto[0];

            $sqlrProyecto = "SELECT nombre FROM ccpproyectospresupuesto WHERE  codigo = '$rowAdiciones[5]'";
            $resProyecto = mysqli_query($linkbd, $sqlrProyecto);
            $rowProyecto = mysqli_fetch_row($resProyecto);

            $proyecto = $rowAdiciones[5] . " - " . $rowProyecto[0];

            $sqlrProgramatico = "SELECT indicador_producto FROM ccpetproductos WHERE codigo_indicador = '$rowAdiciones[6]'";
            $resProgramatico = mysqli_query($linkbd, $sqlrProgramatico);
            $rowProgramatico = mysqli_fetch_row($resProgramatico);

            $programatico = $rowAdiciones[6] . " - " . $rowProgramatico[0];

            if($rowAdiciones[7] != ''){

                $sqlrIng = "SELECT nombre, tipo FROM cuentasccpet WHERE version = $maxVersion AND codigo = '$rowAdiciones[7]' ORDER BY id ASC";
                $resIng = mysqli_query($linkbd, $sqlrIng);
                $rowIng = mysqli_fetch_row($resIng);
            }

            $cuenta = $rowAdiciones[7] . " - " . $rowIng[0];

            $sqlFuente = "SELECT nombre FROM ccpet_fuentes_cuipo WHERE codigo_fuente = '$rowAdiciones[8]'";
            $rowFuente = mysqli_fetch_row(mysqli_query($linkbd, $sqlFuente));

            $fuente = $rowAdiciones[8] . " - " . $rowFuente[0];

            $valorIngreso = 0;
            $valorGasto = 0;

            if($rowAdiciones[0] == 'C') {
                $valorIngreso = $rowAdiciones[9];
                $valorGasto = 0;
            }
            else {
                $valorIngreso = 0;
                $valorGasto = $rowAdiciones[9];
            }

            $totalGastos += $valorGasto;
            $totalIngresos += $valorIngreso;

            array_push($datos, $rowAdiciones[0]); //tipo cuenta
            array_push($datos, $tipoGasto); //tipo gasto
            array_push($datos, $secPresupuestal); //seccion presupuestal
            array_push($datos, $rowAdiciones[3]); //medio de pago
            array_push($datos, $vigGasto); //Vigencia del gasto
            array_push($datos, $proyecto); //proyecto - bpim
            array_push($datos, $programatico); //programatico
            array_push($datos, $cuenta); //cuenta
            array_push($datos, $fuente); //fuente
            array_push($datos, $valorIngreso); //dinero de ingresos
            array_push($datos, $valorGasto); //dinero de gastos

            array_push($datos, $rowAdiciones[7]); //codigo cuenta
            array_push($datos, $rowIng[0]); //nombre cuenta
            array_push($datos, $rowAdiciones[5]); //codigo proyecto
            array_push($datos, $rowProyecto[0]); //nombre proyecto
            array_push($datos, $rowAdiciones[6]); //codigo programatico
            array_push($datos, $rowAdiciones[8]); //codigo fuente
            array_push($datos, $rowAdiciones[2]); //codigo sec presupuestal
            array_push($datos, $rowAdiciones[4]); //codigo vig gasto

            array_push($detalles, $datos);
        }

        $out['detalles'] = $detalles;
        $out['totalGasto'] = $totalGastos;
        $out['totalIngreso'] = $totalIngresos;
    }

    header("Content-type: application/json");
    echo json_encode($out);
    die();
