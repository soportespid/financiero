var app = new Vue({
    el: '#myapp',
	data:{

        error: '',
        loading: false,
        
        fecha: '',
        vigencia: '',
        actoAdm: '',
        actoAdmId: '',
        valorAdicion: 0,
        estado: '',
        letras: '',
        detalles: [],
        totalGasto: 0,
        totalIngreso: 0,
    },

    mounted: function(){

        this.traeDatosFormulario();
    },

    methods: 
    {
        //Desglosar array
        toFormData: function(obj){
            var form_data = new FormData();
            for(var key in obj){
                form_data.append(key, obj[key]);
            }
            return form_data;
        },

        formatonumero: function(valor){
            return new Intl.NumberFormat("en-US", {style: "currency", currency: "USD"}).format(valor);
        },

        traeParametros: function(name){
            //Captura parametros
            name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
            var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
            results = regex.exec(location.search);
            return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
        },

        traeDatosFormulario: async function() {

            const acuerdoId = this.traeParametros('id');
            
            await axios.post('presupuesto_ccpet/traslado/visualizar/ccp-visualizaTraslado.php?action=datosFormulario&acuerdoId='+acuerdoId)
            .then((response) => {
                
                this.fecha = response.data.fecha;
                this.vigencia = response.data.vigencia;
                this.actoAdm = response.data.actoAdm;
                this.actoAdmId = response.data.actoAdmId;
                this.estado = response.data.estado;
                this.valorAdicion = response.data.valorAdicion;
                this.letras = response.data.letras;
                this.detalles = response.data.detalles;
                this.totalGasto = response.data.totalGasto;
                this.totalIngreso = response.data.totalIngreso;
            }).catch((error) => {
                this.error = true;
                console.log(error)
            }).finally(() => {
                
               
            });
        },

        excel: function() {

            if (this.detalles != '') {

                document.form2.action="ccp-excelTraslado.php";
				document.form2.target="_BLANK";
				document.form2.submit(); 
            }
            else {
                Swal.fire("Faltan datos", "warning");
            }
        },
    }
});