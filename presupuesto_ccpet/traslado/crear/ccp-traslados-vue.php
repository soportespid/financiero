<?php
    $URL_BBDD = true;
	require_once '../../../comun.inc';
    require '../../../funciones.inc';
    //require '../../../vue/presupuesto_ccp/funcionesccp.inc.php';
    require_once '../../../vue/presupuesto_ccp/funcionesccp.inc.php';
    session_start();

	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");


    $out = array('error' => false);

    $maxVersion = ultimaVersionGastosCCPET();
    $maxVersionIngresos = ultimaVersionIngresosCCPET();

    function selectOrden(){
        $linkbd = conectar_v7();
	    $linkbd -> set_charset("utf8");
        $sql="SELECT orden FROM ccpet_parametros";
        $request = mysqli_query($linkbd,$sql)->fetch_assoc()['orden'];
        return $request;
    }
    function selectProgramas(){
        $linkbd = conectar_v7();
	    $linkbd -> set_charset("utf8");
        $sql = "SELECT codigo, nombre FROM ccpetprogramas";
        $request = mysqli_fetch_all(mysqli_query($linkbd,$sql),MYSQLI_ASSOC);
        return $request;
    }
    function selectSectores(){
        $linkbd = conectar_v7();
	    $linkbd -> set_charset("utf8");
        $sql = "SELECT codigo, nombre FROM ccpetsectores";
        $request = mysqli_fetch_all(mysqli_query($linkbd,$sql),MYSQLI_ASSOC);
        return $request;
    }


    $action = "show";

    if(isset($_GET['action']) || isset($_POST['action'])){
        $action = $_GET['action'] || $_POST['action'];
    }
    if($action == "get"){
        $arrData = array(
            "sectores"=>selectSectores(),
            "programas"=>selectProgramas(),
            "orden"=>selectOrden()
        );
        $out['data'] = $arrData;
    }
    if($action == 'cargarDet'){

        $detallesTraslados = array();
        $totalCredito = 0;
        $totalContraCredito = 0;
        $sqlr = "SELECT tipo, tipo_gasto, seccion_presupuestal, medio_pago, codigo_vigenciag, bpim, programatico, cuenta, fuente, valor  FROM ccpet_traslados WHERE  id_acuerdo = '".$_GET['idAcuerdo']."'";
        $res = mysqli_query($linkbd, $sqlr);
        while($row = mysqli_fetch_row($res)){
            $detallesAgregar = array();
            array_push($detallesAgregar, $row[0]);
            /* $sqlrTipoGasto = "SELECT nombre FROM ccpettipo_gasto WHERE  id = '$row[1]'";
            $resTipoGasto = mysqli_query($linkbd, $sqlrTipoGasto);
            $rowTipoGasto = mysqli_fetch_row($resTipoGasto);

            array_push($detallesAgregar, $rowTipoGasto[0]); */

            array_push($detallesAgregar, $row[1]);
            array_push($detallesAgregar, $row[2]);

            $sqlrSec = "SELECT nombre FROM pptoseccion_presupuestal WHERE  id_seccion_presupuestal = '$row[2]'";
            $resSec = mysqli_query($linkbd, $sqlrSec);
            $rowSec = mysqli_fetch_row($resSec);

            array_push($detallesAgregar, $rowSec[0]);
            array_push($detallesAgregar, $row[3]);
            array_push($detallesAgregar, $row[4]);

            $sqlrVig = "SELECT nombre FROM ccpet_vigenciadelgasto WHERE  codigo = '$row[4]'";
            $resVig = mysqli_query($linkbd, $sqlrVig);
            $rowVig = mysqli_fetch_row($resVig);

            array_push($detallesAgregar, $rowVig[0]);

            array_push($detallesAgregar, $row[5]);

            $sqlrProyecto = "SELECT nombre FROM ccpproyectospresupuesto WHERE  codigo = '$row[5]'";
            $resProyecto = mysqli_query($linkbd, $sqlrProyecto);
            $rowProyecto = mysqli_fetch_row($resProyecto);

            array_push($detallesAgregar, $rowProyecto[0]);

            array_push($detallesAgregar, $row[6]);

            $sqlrProgramatico = "SELECT indicador_producto FROM ccpetproductos WHERE codigo_indicador = '$row[6]'";
            $resProgramatico = mysqli_query($linkbd, $sqlrProgramatico);
            $rowProgramatico = mysqli_fetch_row($resProgramatico);

            array_push($detallesAgregar, $rowProgramatico[0]);

            array_push($detallesAgregar, $row[7]);


            $sqlrGastos = "SELECT nombre, tipo FROM cuentasccpet WHERE version = $maxVersion AND codigo = '$row[7]' ORDER BY id ASC";
            $resGastos = mysqli_query($linkbd, $sqlrGastos);
            $rowGastos = mysqli_fetch_row($resGastos);
            array_push($detallesAgregar, $rowGastos[0]);

            array_push($detallesAgregar, $row[8]);

            $sqlrMaxFuente = "SELECT MAX(version) FROM ccpet_fuentes_cuipo";
            $resMaxFuente = mysqli_query($linkbd, $sqlrMaxFuente);
            $rowMaxFuente = mysqli_fetch_row($resMaxFuente);
            //echo $_GET['programatico'];

            $sqlFuente = "SELECT ccp_f.nombre FROM ccpet_fuentes_cuipo AS ccp_f WHERE ccp_f.version = '$rowMaxFuente[0]' AND ccp_f.codigo_fuente='$row[8]' AND  LENGTH(ccp_f.codigo_fuente) > 6";
            $resFuente = mysqli_query($linkbd, $sqlFuente);
            $rowFuente = mysqli_fetch_row($resFuente);

            array_push($detallesAgregar, $rowFuente[0]);

            if($row[0] == 'C'){
                array_push($detallesAgregar, $row[9]);
                array_push($detallesAgregar, 0);
                $totalCredito+=$row[9];

            }else{
                array_push($detallesAgregar, 0);
                array_push($detallesAgregar, $row[9]);
                $totalContraCredito+=$row[9];
            }

            array_push($detallesTraslados, $detallesAgregar);

        }

        $out['detallesTraslados'] = $detallesTraslados;
        $out['totalCredito'] = $totalCredito;
        $out['totalContraCredito'] = $totalContraCredito;
    }
    if($action == 'saldoPorCuenta'){

        $parametros = [
            'rubro' => $_POST['rubro'],
            'fuente' => $_POST['fuente'],
            'vigencia' => $_POST['vigencia'],
            'tipo_gasto' => $_POST['tipo_gasto'],
            'seccion_presupuestal' => $_POST['seccion_presupuestal'],
            'medio_pago' => $_POST['medio_pago'],
            'vigencia_gasto' => $_POST['vigencia_gasto'],

            'codProyecto' => $_POST['codProyecto'],
            'programatico' => $_POST['programatico'],
        ];

        $saldoPorCuenta = 0;

        $saldoPorCuenta = saldoPorRubro($parametros);

        $out['saldoPorCuenta'] = round($saldoPorCuenta, 2);
    }
    if($action == 'cargarCuentas'){

        $cuentasCcpet = array();
        $inicioCuenta = $_GET['inicioCuenta'];
        $sqlr = "SELECT * FROM cuentasccpet WHERE version = $maxVersion AND codigo LIKE '$inicioCuenta%' ORDER BY id ASC";
        $res = mysqli_query($linkbd, $sqlr);
        while($row = mysqli_fetch_row($res)){
            array_push($cuentasCcpet, $row);
        }

        $out['cuentasCcpet'] = $cuentasCcpet;

    }
    if($action == 'filtrarCuentas'){

        $keywordCuenta=$_POST['keywordCuenta'];

        $cuentasCcpet = array();
        $inicioCuenta = $_GET['inicioCuenta'];

        $sqlr = "SELECT * FROM cuentasccpet WHERE version = $maxVersion AND codigo LIKE '$inicioCuenta%' AND concat_ws(' ', codigo, nombre) LIKE '%$keywordCuenta%' ORDER BY id ASC";//echo $sqlr;
        $res = mysqli_query($linkbd, $sqlr);
        while($row = mysqli_fetch_row($res)){
            array_push($cuentasCcpet, $row);
        }

        $out['cuentasCcpet'] = $cuentasCcpet;
    }
    if($action == 'cargarCuentasIng'){

        $cuentasCcpet = array();
        $sqlr = "SELECT * FROM cuentasingresosccpet WHERE version = $maxVersionIngresos ORDER BY id ASC";
        $res = mysqli_query($linkbd, $sqlr);
        while($row = mysqli_fetch_row($res)){
            array_push($cuentasCcpet, $row);
        }

        $out['cuentasCcpet'] = $cuentasCcpet;

    }
    if($action == 'actosAdm'){
        $actosAdm = array();
        $sqlr = "SELECT * FROM ccpetacuerdos WHERE estado='S' AND vigencia='".$_GET['vig']."' AND tipo<>'I' AND valortraslado>0";
        $res = mysqli_query($linkbd, $sqlr);
        while($row = mysqli_fetch_row($res)){
            array_push($actosAdm, $row);
        }

        $out['actosAdm'] = $actosAdm;
    }
    if($action == 'filtrarCuentasIngreso'){

        $keywordCuenta=$_POST['keywordCuenta'];

        $cuentasCcpet = array();

        $sqlr = "SELECT * FROM cuentasingresosccpet WHERE version = $maxVersionIngresos AND codigo LIKE '$inicioCuenta%' AND concat_ws(' ', codigo, nombre) LIKE '%$keywordCuenta%' ORDER BY id ASC";//echo $sqlr;
        $res = mysqli_query($linkbd, $sqlr);
        while($row = mysqli_fetch_row($res)){
            array_push($cuentasCcpet, $row);
        }

        $out['cuentasCcpet'] = $cuentasCcpet;
    }
    if($action == 'filtrarCuentas'){

        $keywordCuenta=$_POST['keywordCuenta'];

        $cuentasCcpet = array();
        $inicioCuenta = $_GET['inicioCuenta'];

        $sqlr = "SELECT * FROM cuentasccpet WHERE version = $maxVersion AND codigo LIKE '$inicioCuenta%' AND concat_ws(' ', codigo, nombre) LIKE '%$keywordCuenta%' ORDER BY id ASC";//echo $sqlr;
        $res = mysqli_query($linkbd, $sqlr);
        while($row = mysqli_fetch_row($res)){
            array_push($cuentasCcpet, $row);
        }

        $out['cuentasCcpet'] = $cuentasCcpet;
    }
    if($action == 'buscarCuentaIngreso'){
        $cuenta = $_GET['cuenta'];
        $resultBusquedaCuenta = array();

        $sqlr = "SELECT nombre, tipo FROM cuentasingresosccpet WHERE version = $maxVersionIngresos AND codigo = '$cuenta' ORDER BY id ASC";
        $res = mysqli_query($linkbd, $sqlr);//echo $sqlr;
        while($row = mysqli_fetch_row($res)){
            array_push($resultBusquedaCuenta, $row);
        }

        $out['resultBusquedaCuenta'] = $resultBusquedaCuenta;

    }
    if($action == 'buscarCuenta'){
        $cuenta = $_GET['cuenta'];
        $resultBusquedaCuenta = array();

        $sqlr = "SELECT nombre, tipo FROM cuentasccpet WHERE version = $maxVersion AND codigo = '$cuenta' ORDER BY id ASC";
        $res = mysqli_query($linkbd, $sqlr);//echo $sqlr;
        while($row = mysqli_fetch_row($res)){
            array_push($resultBusquedaCuenta, $row);
        }

        $out['resultBusquedaCuenta'] = $resultBusquedaCuenta;

    }
    if($action == 'cargarFuentes'){

        $fuentes = array();
        $sqlrMaxFuente = "SELECT MAX(version) FROM ccpet_fuentes_cuipo";
        $resMaxFuente = mysqli_query($linkbd, $sqlrMaxFuente);
        $rowMaxFuente = mysqli_fetch_row($resMaxFuente);
        //echo $_GET['programatico'];

        $sql = "SELECT ccp_f.codigo_fuente, ccp_f.nombre FROM ccpet_fuentes_cuipo AS ccp_f WHERE ccp_f.version = '$rowMaxFuente[0]' AND  LENGTH(ccp_f.codigo_fuente) > 6";
        $res = mysqli_query($linkbd, $sql);

        while($row = mysqli_fetch_row($res))
        {
            array_push($fuentes, $row);
        }

        $out['fuentes'] = $fuentes;

    }
    if($action == 'cargarProgramaticos'){

        $cuentasCcpet = array();

        $programaticos = array();
        $proyecto = $_GET['proyecto'];
        $sqlr = "SELECT id FROM ccpproyectospresupuesto WHERE codigo = '$proyecto'";
        $res = mysqli_query($linkbd, $sqlr);

        while($row = mysqli_fetch_row($res)){
            $sqlrP = "SELECT indicador FROM ccpproyectospresupuesto_productos WHERE codproyecto = '$row[0]' GROUP BY indicador";
            $resP = mysqli_query($linkbd, $sqlrP);
            $programatico = array();
            while($rowP = mysqli_fetch_row($resP)){
                $programatico = array();
                $sqlrNom = "SELECT indicador_producto FROM ccpetproductos WHERE codigo_indicador = '$rowP[0]'";
                $resNom = mysqli_query($linkbd, $sqlrNom);
                $rowNom = mysqli_fetch_row($resNom);
                array_push($programatico, $rowP[0]);
                array_push($programatico, $rowNom[0]);

                array_push($programaticos, $programatico);

            }

        }
        $out['programaticos'] = $programaticos;
    }

    if($action == 'buscarProgramatico'){

        $programatico = $_GET['programatico'];

        $proyecto = $_GET['bpim'];
        $vigencia = $_GET['vigencia'];
        $resultBusquedaProgramatico = array();

        $sqlr = "SELECT id FROM ccpproyectospresupuesto WHERE codigo = '$proyecto' AND vigencia = '$vigencia'";
        $res = mysqli_query($linkbd, $sqlr);

        while($row = mysqli_fetch_row($res)){
            $sqlrP = "SELECT indicador FROM ccpproyectospresupuesto_productos WHERE codproyecto = '$row[0]' AND indicador = '$programatico' GROUP BY indicador";
            $resP = mysqli_query($linkbd, $sqlrP);
            $programatico = array();
            while($rowP = mysqli_fetch_row($resP)){
                $programatico = array();
                $sqlrNom = "SELECT indicador_producto FROM ccpetproductos WHERE codigo_indicador = '$rowP[0]'";
                $resNom = mysqli_query($linkbd, $sqlrNom);
                $rowNom = mysqli_fetch_row($resNom);
                array_push($programatico, $rowNom[0]);

                array_push($resultBusquedaProgramatico, $programatico);

            }

        }
        $out['resultBusquedaProgramatico'] = $resultBusquedaProgramatico;

    }


    if($action == 'cargarProyectosCredito'){
        $sqlVigencias = "SELECT codigo,nombre FROM ccpet_vigenciadelgasto WHERE version = (SELECT MAX(version) FROM ccpet_vigenciadelgasto)";
        $sqlFuentes = "SELECT codigo_fuente as codigo, nombre FROM ccpet_fuentes_cuipo WHERE version = '1' ORDER BY codigo_fuente";
        $arrVigencias = mysqli_fetch_all(mysqli_query($linkbd,$sqlVigencias),MYSQLI_ASSOC);
        $arrFuentes = mysqli_fetch_all(mysqli_query($linkbd,$sqlFuentes),MYSQLI_ASSOC);

        $proyectosProgramaticos = [];

        $vigencia = $_GET['vigencia'];

        $sql = "SELECT DISTINCT
        cp.id,
        cp.codigo,
        cp.nombre,
        pro.indicador,
        pro.sector,
        pro.programa,
        pro.producto,
        sec.nombre as nombre_sector,
        cpro.nombre as nombre_programa,
        p.producto as nombre_producto,
        cpp.id_fuente as fuente,
        secp.id_seccion_presupuestal as seccion,
        secp.nombre as nombre_seccion,
        cpp.vigencia_gasto as vigencia
        FROM ccpproyectospresupuesto cp
        LEFT JOIN ccpproyectospresupuesto_presupuesto cpp ON cpp.codproyecto = cp.id
        INNER JOIN ccpproyectospresupuesto_productos pro ON pro.codproyecto = cp.id
        INNER JOIN ccpetsectores sec ON pro.sector = sec.codigo
        INNER JOIN ccpetprogramas cpro ON cpro.codigo = pro.programa
        INNER JOIN ccpetproductos p ON p.cod_producto = pro.producto AND pro.indicador = p.codigo_indicador
        INNER JOIN pptoseccion_presupuestal secp ON secp.id_seccion_presupuestal = cp.idunidadej
        WHERE cp.estado = 'S' AND cp.vigencia = '$vigencia' ORDER BY cpp.id DESC";
        $request = mysqli_fetch_all(mysqli_query($linkbd,$sql),MYSQLI_ASSOC);
        $arrProCreados = array_values(array_filter($request,function($e){return $e['fuente']!="";}));
        foreach ($arrProCreados as $d) {
            $arrFuente = array_values(array_filter($arrFuentes,function($e)use($d){return $d['fuente'] == $e['codigo'];}))[0];
            $arrVigencia = array_values(array_filter($arrVigencias,function($e)use($d){return $d['vigencia'] == $e['codigo'];}))[0];
            array_push($proyectosProgramaticos,[
                $d['indicador']."-".$d['codigo']."-".$d['fuente']."-".$d['seccion']."-".$d['vigencia'],//0
                $d['nombre_producto']."-".$d['nombre'],//1
                $d['fuente']."-".$arrFuente['nombre'],//2
                $d['vigencia']."-".$arrVigencia['nombre'],//3
                $d['seccion']."-".$d['nombre_seccion'],//4
                $d['programa'],//5
                $d['producto'],//6
                $d['sector'],//7
                $d['nombre_programa'],//8
                $d['nombre_producto'],//9
                $d['id']//10
            ]);
        }
        $out['proyectosProgramaticos'] = $proyectosProgramaticos;
    }
    if($action == 'guardarAd'){

        preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST["fecha"], $fecha);
		$fechaf = $fecha[3]."-".$fecha[2]."-".$fecha[1];

        $sqlr = "DELETE FROM ccpet_traslados WHERE id_acuerdo = '".$_POST['acto_administrativo']."'";
        mysqli_query($linkbd, $sqlr);

        for($x = 0; $x < count($_POST["detallesAd"]); $x++){

            $vigencia = '';
            $id_acuerdo = '';
            $tipo_cuenta = '';
            $tipo_gasto = '';
            $seccion_presupuestal = '';
            $medio_pago = '';
            $codigo_vigenciag = '';
            $bpim = '';
            $programatico = '';
            $cuenta = '';
            $fuente = '';
            $valor = '';
            $estado = '';


            $vigencia = $_POST["vigencia"];
            $id_acuerdo = $_POST["acto_administrativo"];
            $tipo_cuenta = $_POST["detallesAd"][$x][0];
            $tipo_gasto = $_POST["detallesAd"][$x][1];
            $seccion_presupuestal = $_POST["detallesAd"][$x][2];
            $medio_pago = $_POST["detallesAd"][$x][4];
            $codigo_vigenciag = $_POST["detallesAd"][$x][5];
            $bpim = $_POST["detallesAd"][$x][7];
            $programatico = $_POST["detallesAd"][$x][9];
            $cuenta = $_POST["detallesAd"][$x][11];
            $fuente = $_POST["detallesAd"][$x][13];
            if($tipo_cuenta == 'C'){
                $valor = $_POST["detallesAd"][$x][15];
            }else{
                $valor = $_POST["detallesAd"][$x][16];
            }

            $estado = 'S';

            $sqlrD = "INSERT INTO ccpet_traslados(vigencia, id_acuerdo, fecha, tipo, tipo_gasto, seccion_presupuestal, medio_pago, codigo_vigenciag, bpim, programatico, cuenta, fuente, valor, estado) VALUES ('".$vigencia."', '".$id_acuerdo."', '".$fechaf."', '".$tipo_cuenta."', '".$tipo_gasto."', '".$seccion_presupuestal."', '".$medio_pago."', '$codigo_vigenciag', '".trim($bpim)."', '".trim($programatico)."', '".trim($cuenta)."', '".trim($fuente)."', '".$valor."', '".$estado."')";
            mysqli_query($linkbd, $sqlrD);




        }

        if($_POST["finalizar"] != false){
            $sqlr = "UPDATE ccpetacuerdos SET estado = 'F' WHERE id_acuerdo = '$_POST[acto_administrativo]'";
            mysqli_query($linkbd, $sqlr);
        }

        $out['insertaBien'] = true;

    }

    header("Content-type: application/json");
    echo json_encode($out);
    die();
