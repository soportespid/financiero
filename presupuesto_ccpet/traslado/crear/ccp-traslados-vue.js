import { divideRubro, filtroEnArrayDeObjetos } from "../../../funciones.js";
/* import { ordenarArbol, buscaNombreRubros, filtroEnArrayDeObjetos } from './../../funciones.js' */

var app = new Vue({
    el: '#myapp',
	data:{
        arrFuentes: [],
        vigencia: '',
        loading: false,
        acto_administrativo: '',
        actos_administrativo: [],

        mostrarSaldo : false,

        valor_acuerdo: '$0.00',
        checked: '',
        tipoGasto: '',
        tiposDeGasto: [],
        selectUnidadEjecutora : '',
        unidadesejecutoras: [],
        optionsMediosPagos: [
			{ text: 'CSF', value: 'CSF' },
			{ text: 'SSF', value: 'SSF' }
		],
        selectMedioPago: 'CSF',

        selectVigenciaGasto: '',
        vigenciasdelgasto: [],
        mostrarFuncionamiento : true,

        cuenta:'',
        nombreCuenta: '',
        showModal_cuentas: false,
        searchCuenta : {keywordCuenta: ''},
        cuentasCcpet: [],

        fuente: '',
        nombreFuente: '',
        showModal_fuentes: false,
        fuentesCuipo: [],
        searchFuente : {keywordFuente: ''},

        mostrarInversion : false,
        codProyecto: '',
        nombreProyecto: '',
        showModal_proyectos: false,
        proyectos: [],
        searchProyecto : {keywordProyecto: ''},

        programatico: '',
        nombreProgramatico: '',
        showModal_programatico: false,
        programaticos: [],

        mostrarRubroContra: false,
        showModal_programaticoProyectos: false,
        buscarProgramaticoProyecto : '',
        proyectosProgramaticos: [],

        tipo_traslado: '-1',
        tipos_traslado: [
            { text: 'Contracredito', value: 'R' },
			{ text: 'Credito', value: 'C' }
		],

        totalCredito: '$0.00',
        totalContraCredito: '$0.00',
        diferencia: '$0.00',


        valor: 0,
        saldo : 0,
        detalles: [],

        detallesEliminados: [],

        rubroPresupuestal: '',

        isModalPrograma:false,
        isModalSector:false,
        selectOrden:1,
        arrProgramas:[],
        arrSectores:[],
        arrProgramasCopy:[],
        arrSectoresCopy:[],
        objSector:{codigo:"",nombre:""},
        objPrograma:{codigo:"",nombre:""},
        txtSearchProgramas:"",
        txtSearchSectores:"",
        txtResultadosProgramas:0,
        txtResultadosSectores:0,
        selectOrden:1,
        selectSector:0,
        selectPrograma:0,
        selectProducto:0,
        proyectosProgramaticosCopy:[],
        arrProgramasFilter:[],
        arrSectoresFilter:[],
        arrProductosFilter:[],
    },

    mounted: async function(){

        await this.cargarFecha();
        this.seccionPresupuestal();
        this.vigenciasDelgasto();
        this.actosAdministrativos();
        this.cargarTiposDeGasto();
        this.getData();
    },

    methods: {
        filter:function(type=""){
            if(type=="sector" && this.selectSector != 0){
                this.arrProductosFilter = [];
                this.proyectosProgramaticosCopy = [...this.proyectosProgramaticos.filter((e)=>{return e[7]==app.selectSector})];
                this.arrProgramasFilter = Array.from(new Set(
                    app.proyectosProgramaticosCopy.map(e=>{return JSON.stringify({codigo:e[5],nombre:e[8]})})
                )).map(item => JSON.parse(item));
            }else if(type=="programa" && this.selectPrograma != 0){
                this.proyectosProgramaticosCopy = [...this.proyectosProgramaticos.filter((e)=>{return e[7]==app.selectSector && e[5]==app.selectPrograma})];
                this.arrProductosFilter = Array.from(new Set(
                    app.proyectosProgramaticosCopy.map(e=>{return JSON.stringify({codigo:e[6],nombre:e[9]})})
                )).map(item => JSON.parse(item));
            }else if(type=="producto" && this.selectProducto != 0){
                this.proyectosProgramaticosCopy = [...this.proyectosProgramaticos.filter((e)=>{
                    return e[7]==app.selectSector && e[5]==app.selectPrograma && e[6]==app.selectProducto}
                )];
            }else{
                this.proyectosProgramaticosCopy = [...this.proyectosProgramaticos];
            }
        },
        getData: async function(){
            const formData = new FormData();
            formData.append("action","get");
            const response = await fetch('presupuesto_ccpet/traslado/crear/ccp-traslados-vue.php',{method:"POST",body:formData});
            const objData = await response.json();
            this.arrProgramas = objData.data.programas;
            this.arrSectores = objData.data.sectores;
            this.arrProgramasCopy = objData.data.programas;
            this.arrSectoresCopy = objData.data.sectores;
            this.txtResultadosProgramas = this.arrProgramas.length;
            this.txtResultadosSectores = this.arrSectores.length;
            this.selectOrden = objData.data.orden;
        },
        search:function(type=""){
            let search = "";
            if(type == "modal_programa")search = this.txtSearchProgramas.toLowerCase();
            if(type == "modal_sector")search = this.txtSearchSectores.toLowerCase();
            if(type == "cod_sector") search = this.objSector.codigo;
            if(type == "cod_programa") search = this.objPrograma.codigo;

            if(type == "modal_sector"){
                this.arrSectoresCopy = [...this.arrSectores.filter(e=>e.codigo.toLowerCase().includes(search)
                    || e.nombre.toLowerCase().includes(search))];
                this.txtResultadosSectores = this.arrSectoresCopy.length;
            }else if(type == "modal_programa"){
                this.arrProgramasCopy = [...this.arrProgramas.filter(e=>e.codigo.toLowerCase().includes(search)
                    || e.nombre.toLowerCase().includes(search))];
                this.txtResultadosProgramas = this.arrProgramasCopy.length;
            }else if(type == "cod_sector"){
                this.objSector = {};
                this.objSector = {...this.arrSectores.filter(e=>e.codigo==search)[0]};
                this.changeSector();
            }else if(type == "cod_programa"){
                this.objPrograma = {};
                this.objPrograma = {...this.arrProgramas.filter(e=>e.codigo==search)[0]};
                this.changePrograma();
            }
        },
        changeSector:function(){
            if(this.objSector.codigo){
                this.objPrograma = {};
                this.codProyecto = "";
                this.programatico = "";
            }
        },
        changePrograma:function(){
            if(this.objPrograma.codigo){
                let sector = this.objPrograma.codigo.substring(0,2);
                this.objSector = {...this.arrSectores.filter(e=>e.codigo==sector)[0]};
                this.codProyecto = "";
                this.programatico = "";
            }
        },
        changeProgramatico:function(){
            if(this.programatico != ""){
                let sector = this.programatico.substring(0,2);
                let programa = this.programatico.substring(0,4);
                this.objSector = {...this.arrSectores.filter(e=>e.codigo==sector)[0]};
                this.objPrograma = {...this.arrProgramas.filter(e=>e.codigo==programa)[0]};
            }
        },
        selectItem:function(type="",{...item}){
            if(type == "modal_sector"){
                this.objSector = item;
                this.isModalSector = false;
                this.changeSector();
            }else if(type == "modal_programa"){
                this.objPrograma = item;
                this.isModalPrograma = false;
                this.changePrograma();
            }
        },
        cargarFecha: function(){

            const fechaAct = new Date().toJSON().slice(0,10).replace(/-/g,'/');
            const fechaArr = fechaAct.split('/');
            const fechaV = fechaArr[2]+'/'+fechaArr[1]+'/'+fechaArr[0];
            document.getElementById('fecha').value = fechaV;
            this.vigencia = fechaArr[0];
        },

        actosAdministrativos: function(){
            const fechat = document.getElementById('fecha').value;
            const fechatAr = fechat.split('/');
            this.vigencia = fechatAr[2];
            axios.post('presupuesto_ccpet/traslado/crear/ccp-traslados-vue.php?action=actosAdm&vig=' + this.vigencia)
            .then(
                (response)=>{
                    this.actos_administrativo = response.data.actosAdm;
                }
            );
        },

        seleccionarActoAdm: async function(){
            this.loading =  true;
            this.actos_administrativo.forEach(function logArrayElements(element, index, array) {
                //console.log("a[" + index + "] = " + element[5] +" --> " + app.acto_administrativo);
                if(element[0] == app.acto_administrativo){
                    app.valor_acuerdo = new Intl.NumberFormat("en-US", {style: "currency", currency: "USD",minimumFractionDigits: 0, maximumFractionDigits: 2,}).format(element[7]);
                    let fechaActParts = element[3].split('-');
                    document.getElementById('fecha').value = fechaActParts[2]+'/'+fechaActParts[1]+'/'+fechaActParts[0];
                    //app.valor_acuerdo = element[5];
                }
            });

            await axios.post('presupuesto_ccpet/traslado/crear/ccp-traslados-vue.php?action=cargarDet&idAcuerdo=' + this.acto_administrativo)
            .then((response) => {
                app.detalles = response.data.detallesTraslados;
                app.totalCredito = new Intl.NumberFormat("en-US", {style: "currency", currency: "USD",minimumFractionDigits: 0, maximumFractionDigits: 2,}).format(response.data.totalCredito);
                app.totalContraCredito = new Intl.NumberFormat("en-US", {style: "currency", currency: "USD",minimumFractionDigits: 0, maximumFractionDigits: 2,}).format(response.data.totalContraCredito);

                /* if(response.data.tiposDeGasto.length > 0){
                    this.tipoGasto = response.data.tiposDeGasto[0][0];
                } */
            }).finally(() => {
                this.loading =  false;
            });

            this.diferenciaAd();
            this.reCalcularDiferencia();
        },

        cargarTiposDeGasto: async function(){

            await axios.post('vue/presupuesto_ccp/ccp-cdpbasico.php?action=cargarTiposDeGasto')
            .then((response) => {
                //console.log(response.data.tiposDeGasto);
                app.tiposDeGasto = response.data.tiposDeGasto;

                if(response.data.tiposDeGasto.length > 0){
                    this.tipoGasto = response.data.tiposDeGasto[0][0];
                }
            });
            this.cambiaTipoDeGasto();
        },

        seccionPresupuestal: async function(){
            await axios.post('vue/presupuesto_ccp/ccp-inicialgastosfun.php?action=secciones')
            .then(
                (response)=>{
                    //console.log(response.data.secciones);
                    this.unidadesejecutoras = response.data.secciones;
                    this.selectUnidadEjecutora = this.unidadesejecutoras[0][0];
                }
            );
        },

        vigenciasDelgasto: async function(){
            await axios.post('vue/presupuesto_ccp/ccp-inicialgastosfun.php?action=vigenciasDelGasto')
            .then(
                (response) => {
                    this.vigenciasdelgasto = response.data.vigenciasDelGasto;
                    this.selectVigenciaGasto = this.vigenciasdelgasto[0][0];
                }
            );
        },

        agregarDetalle: function(){

            this.loading = true;
            let difSaldo = 0;
            if(this.tipo_traslado == 'R'){
                difSaldo = this.saldo - this.valor;
            }

            if(!this.checked){

                if(this.tipo_traslado != ''){

                    if(this.selectUnidadEjecutora != ''){

                        if(this.selectMedioPago != ''){

                            if(this.selectVigenciaGasto){

                                if(this.fuente != ''){

                                    if(this.valor > 0){

                                        if(difSaldo >= 0){

                                            if(this.tipoGasto == '3'){

                                                if(this.codProyecto != ''){

                                                    if(this.programatico != ''){

                                                        var detallesAgr = [];

                                                        detallesAgr.push(this.tipo_traslado);

                                                        this.tiposDeGasto.forEach(function logArrayElements(element, index, array) {
                                                            //console.log("a[" + index + "] = " + element);
                                                            if(element[0] == app.tipoGasto){
                                                                detallesAgr.push(element[1]);
                                                            }
                                                        });
                                                        /* detallesAgr.push(this.tipoGasto); */
                                                        detallesAgr.push(this.selectUnidadEjecutora);
                                                        this.unidadesejecutoras.forEach(function logArrayElements(element, index, array) {
                                                            //console.log("a[" + index + "] = " + element);
                                                            if(element[0] == app.selectUnidadEjecutora){
                                                                detallesAgr.push(element[1].toLowerCase());
                                                            }
                                                        });
                                                        detallesAgr.push(this.selectMedioPago);
                                                        detallesAgr.push(this.selectVigenciaGasto);
                                                        this.vigenciasdelgasto.forEach(function logArrayElements(element, index, array) {
                                                            if(element[0] == app.selectVigenciaGasto){
                                                                detallesAgr.push(element[2].toLowerCase());
                                                            }
                                                        });

                                                        detallesAgr.push(this.codProyecto);
                                                        detallesAgr.push(this.nombreProyecto.toLowerCase().slice(0,30));
                                                        detallesAgr.push(this.programatico);
                                                        detallesAgr.push(this.nombreProgramatico.toLowerCase().slice(0,30));

                                                        /* }else{
                                                            detallesAgr.push(this.cuenta);
                                                            detallesAgr.push(this.nombreCuenta.toLowerCase().slice(0,15));
                                                            detallesAgr.push('');
                                                            detallesAgr.push('');
                                                        } */
                                                        detallesAgr.push('');
                                                        detallesAgr.push('');
                                                        detallesAgr.push(this.fuente);
                                                        detallesAgr.push(this.nombreFuente.toLowerCase().slice(0,30));
                                                        //detallesAgr.push(new Intl.NumberFormat().format(this.valor));

                                                        if(this.tipo_traslado == 'C'){
                                                            detallesAgr.push(this.valor);
                                                            detallesAgr.push(0);
                                                        }else{
                                                            detallesAgr.push(0);
                                                            detallesAgr.push(this.valor);
                                                        }


                                                        let existeDetalle = 0;
                                                        this.detalles.forEach(function(elemento){
                                                            let result = app.comparaArrays(elemento, detallesAgr);

                                                            if(result){
                                                                existeDetalle = 1;
                                                                return false;
                                                            }
                                                        });

                                                        if(existeDetalle == 0){

                                                            this.detalles.push(detallesAgr);
                                                            this.cuenta = '';
                                                            this.codProyecto = '';
                                                            this.nombreProyecto = '';
                                                            this.programatico = '';
                                                            this.nombreProgramatico = '';
                                                            this.nombreCuenta = '';
                                                            this.fuente = '';
                                                            this.nombreFuente = '';


                                                            /* if(this.tipo_cuenta == 'I'){
                                                                this.totalIngresos = parseFloat(this.totalIngresos) + parseFloat(this.valor);
                                                            }else{
                                                                this.totalGastos = parseFloat(this.totalGastos) + parseFloat(this.valor);
                                                            } */
                                                            if(this.tipo_traslado == 'C'){
                                                                this.totalCredito = this.sumarIngresos(parseFloat(this.valor));

                                                                this.totalCredito= new Intl.NumberFormat("en-US", {style: "currency", currency: "USD",minimumFractionDigits: 0, maximumFractionDigits: 2,}).format(this.totalCredito);
                                                            }else{
                                                                this.totalContraCredito = this.sumarGastos(parseFloat(this.valor));

                                                                this.totalContraCredito= new Intl.NumberFormat("en-US", {style: "currency", currency: "USD",minimumFractionDigits: 0, maximumFractionDigits: 2,}).format(this.totalContraCredito);
                                                            }

                                                            this.valor = 0;
                                                            this.saldo = 0;
                                                            this.cargarTiposDeGasto();
                                                            this.diferenciaAd();
                                                        }else{
                                                            this.loading =  false;
                                                            Swal.fire(
                                                                'Detalle repetido.',
                                                                'Debe tener el rubro o alguno de sus clasificadores diferentes a los detalles que se han agregado.',
                                                                'warning'
                                                            );

                                                        }
                                                    }else{
                                                        this.loading =  false;
                                                        Swal.fire(
                                                            'Falta informaci&oacute;n.',
                                                            'Falta escoger el indicador program&aacute;tico MGA.',
                                                            'warning'
                                                        );
                                                    }

                                                }else{
                                                    this.loading =  false;
                                                    Swal.fire(
                                                        'Falta informaci&oacute;n.',
                                                        'Falta escoger el proyecto de inversi&oacute;n.',
                                                        'warning'
                                                    );
                                                }

                                            }else{

                                                var detallesAgr = [];

                                                detallesAgr.push(this.tipo_traslado);

                                                this.tiposDeGasto.forEach(function logArrayElements(element, index, array) {
                                                    //console.log("a[" + index + "] = " + element);
                                                    if(element[0] == app.tipoGasto){
                                                        detallesAgr.push(element[1]);
                                                    }
                                                });

                                                /* detallesAgr.push(this.tipoGasto); */
                                                detallesAgr.push(this.selectUnidadEjecutora);
                                                this.unidadesejecutoras.forEach(function logArrayElements(element, index, array) {
                                                    //console.log("a[" + index + "] = " + element);
                                                    if(element[0] == app.selectUnidadEjecutora){
                                                        detallesAgr.push(element[1].toLowerCase());
                                                    }
                                                });
                                                detallesAgr.push(this.selectMedioPago);
                                                detallesAgr.push(this.selectVigenciaGasto);
                                                this.vigenciasdelgasto.forEach(function logArrayElements(element, index, array) {
                                                    if(element[0] == app.selectVigenciaGasto){
                                                        detallesAgr.push(element[2].toLowerCase());
                                                    }
                                                });

                                                detallesAgr.push('');
                                                detallesAgr.push('');
                                                detallesAgr.push('');
                                                detallesAgr.push('');
                                                detallesAgr.push(this.cuenta);
                                                detallesAgr.push(this.nombreCuenta.toLowerCase().slice(0,30));

                                                /* }else{
                                                    detallesAgr.push(this.cuenta);
                                                    detallesAgr.push(this.nombreCuenta.toLowerCase().slice(0,15));
                                                    detallesAgr.push('');
                                                    detallesAgr.push('');
                                                } */

                                                detallesAgr.push(this.fuente);
                                                detallesAgr.push(this.nombreFuente.toLowerCase().slice(0,30));

                                                //detallesAgr.push(new Intl.NumberFormat().format(this.valor));
                                                if(this.tipo_traslado == 'C'){
                                                    detallesAgr.push(this.valor);
                                                    detallesAgr.push('');
                                                }else{
                                                    detallesAgr.push('');
                                                    detallesAgr.push(this.valor);
                                                }


                                                let existeDetalle = 0;
                                                this.detalles.forEach(function(elemento){
                                                    let result = app.comparaArrays(elemento, detallesAgr);

                                                    if(result){
                                                        existeDetalle = 1;
                                                        return false;
                                                    }
                                                });

                                                if(existeDetalle == 0){

                                                    this.detalles.push(detallesAgr);
                                                    this.cuenta = '';
                                                    this.codProyecto = '';
                                                    this.nombreProyecto = '';
                                                    this.programatico = '';
                                                    this.nombreProgramatico = '';
                                                    this.nombreCuenta = '';
                                                    this.fuente = '';
                                                    this.nombreFuente = '';
                                                    if(this.tipo_traslado == 'C'){
                                                        this.totalCredito = this.sumarIngresos(parseFloat(this.valor));
                                                        this.totalCredito= new Intl.NumberFormat("en-US", {style: "currency", currency: "USD",minimumFractionDigits: 0, maximumFractionDigits: 2,}).format(this.totalCredito);
                                                    }else{
                                                        this.totalContraCredito = this.sumarGastos(parseFloat(this.valor));
                                                        this.totalContraCredito= new Intl.NumberFormat("en-US", {style: "currency", currency: "USD",minimumFractionDigits: 0, maximumFractionDigits: 2,}).format(this.totalContraCredito);
                                                    }


                                                    this.valor = 0;
                                                    this.saldo = 0;
                                                    this.cargarTiposDeGasto();
                                                    this.diferenciaAd();

                                                }else{
                                                    this.loading =  false;
                                                    Swal.fire(
                                                        'Detalle repetido.',
                                                        'Debe tener el rubro o alguno de sus clasificadores diferentes a los detalles que se han agregado.',
                                                        'warning'
                                                    );

                                                }

                                            }
                                        }else{
                                            this.loading =  false;
                                            Swal.fire(
                                                'No hay saldo suficiente.',
                                                'El valor del contracredito debe ser menor o igual al saldo.',
                                                'warning'
                                            );
                                        }


                                    }else{
                                        this.loading =  false;
                                        Swal.fire(
                                            'Falta informaci&oacute;n.',
                                            'Falta digitar el valor.',
                                            'warning'
                                        );
                                    }
                                }else{
                                    this.loading =  false;
                                    Swal.fire(
                                        'Falta informaci&oacute;n.',
                                        'Falta escoger la fuente.',
                                        'warning'
                                    );
                                }
                            }else{
                                this.loading =  false;
                                Swal.fire(
                                    'Falta informaci&oacute;n.',
                                    'Falta escoger la vigencia del gasto.',
                                    'warning'
                                );
                            }
                        }else{
                            this.loading =  false;
                            Swal.fire(
                                'Falta informaci&oacute;n.',
                                'Falta escoger el medio de pago.',
                                'warning'
                            );
                        }
                    }else{
                        this.loading =  false;
                        Swal.fire(
                            'Falta informaci&oacute;n.',
                            'Falta escoger la secci&oacute;n presupuestal.',
                            'warning'
                        );
                    }
                }else{
                    this.loading =  false;
                    Swal.fire(
                        'Falta informaci&oacute;n.',
                        'Falta escoger el tipo de cuenta.',
                        'warning'
                    );
                }
            }else{
                this.loading =  false;
                Swal.fire(
                    "Traslado finalizada.",
                    'El traslado no se puede modificar porque esta finalizada.',
                    'warning'
                );
            }
            this.reCalcularDiferencia();
            this.loading =  false;
        },

        comparaArrays: function(array1, array2){
            /* const array1 = [];
            const array2 = [];
            array1.push(a1);
            array2.push(a2); */
            array1 = array1.slice(0,-3);
            array2 = array2.slice(0,-3);
            /* console.log(array1, array2) */
            return Array.isArray(array1) &&
                    Array.isArray(array2) &&
                    array1.length === array2.length &&
                    array1.every((val, index) => val === array2[index]);

        },

        quitarValor: function(item){
            if(item[0] == 'C'){
                var totCred = this.quitarPesosYComas(this.totalCredito);

                var totalCreditoNuevo = 0;
                totalCreditoNuevo = parseFloat(totCred) - parseFloat(item[15]);
                this.totalCredito = new Intl.NumberFormat("en-US", {style: "currency", currency: "USD",minimumFractionDigits: 0, maximumFractionDigits: 2,}).format(totalCreditoNuevo);

            }else{
                var totContraCredito = this.quitarPesosYComas(this.totalContraCredito);

                var totalContraCreditoNuevo = 0;

                totalContraCreditoNuevo = parseFloat(totContraCredito) - parseFloat(item[16]);

                this.totalContraCredito = new Intl.NumberFormat("en-US", {style: "currency", currency: "USD",minimumFractionDigits: 0, maximumFractionDigits: 2,}).format(totalContraCreditoNuevo);
            }
        },

        eliminarDetalle: async function(item){

            if(!this.checked){
                await this.quitarValor(item);
                var i = this.detalles.indexOf( item );

                if ( i !== -1 ) {

                    let existeItenEliminado = false;
                    this.detallesEliminados.forEach(function(elemento){
                        if(elemento[2] === item[2] && elemento[4] === item[4] && elemento[5] === item[5] && elemento[7] === item[7] && elemento[9] === item[9] && elemento[13] === item[13]){
                            existeItenEliminado = true
                        }
                        /* let result = app.comparaArrays(elemento, item);
                        if(result){
                            existeItenEliminado = true

                        } */
                    });


                    if(!existeItenEliminado){
                        this.detallesEliminados.push(this.detalles[i]);
                    }


                    /* let j = this.detallesEliminados.indexOf( item );
                    if(j == -1){
                        this.detallesEliminados.push(this.detalles[i]);
                    } */

                    this.detalles.splice( i, 1 );
                    this.parametrosCompletos();
                }

                this.diferenciaAd();
                this.reCalcularDiferencia();
                /* setTimeout(function(){
                    this.diferenciaAd();
                },2000); */
            }else{
                Swal.fire(
                    'Traslado finalizado.',
                    'El traslado no se puede modificar porque esta finalizada.',
                    'warning'
                );
            }

        },

        quitarPesosYComas: function(valor=''){

            var valorACambiar = '';
            valorACambiar = valor.replace(/[$]+/g, '');
            valorACambiar = valorACambiar.replace(/[,]+/g, '');

            /* if(valorACambiar == '[object Undefined]'){
                valorACambiar = 0;
            } */

            return valorACambiar;
        },

        diferenciaAd: function(){

            var totCredito = this.quitarPesosYComas(this.totalCredito);
            var totContraCredito = this.quitarPesosYComas(this.totalContraCredito);
            //totIng = totIng.replace(/[$]+/g, '');
            this.diferencia = parseFloat(totCredito) - parseFloat(totContraCredito);
            this.diferencia= new Intl.NumberFormat("en-US", {style: "currency", currency: "USD",minimumFractionDigits: 0, maximumFractionDigits: 2,}).format(this.diferencia);

        },

        sumarIngresos: function(valorSum){

            var totCred = this.quitarPesosYComas(this.totalCredito);

            var totSumCred = parseFloat(totCred) + parseFloat(valorSum);

            return totSumCred;
        },

        sumarGastos: function(valorSum){

            var totContraCredito = this.quitarPesosYComas(this.totalContraCredito);

            var totSumContraCredito = parseFloat(totContraCredito) + parseFloat(valorSum);

            return totSumContraCredito;
        },

        guardarAdd: function(){

            this.loading = true;
            var totDif = this.quitarPesosYComas(this.diferencia);
            let isSaldo = this.arrFuentes.every(function(e){return e.saldo == 0});
            if(!isSaldo){
                this.loading =  false;
                Swal.fire(
                    'El saldo de cada fuente debe ser igual a cero.',
                    'Debe verificar el crédito y contracredito de cada fuente.',
                    'warning'
                );
                return false;
            }
            if(parseFloat(totDif) == 0){

                if(document.getElementById('fecha').value != '' && this.acto_administrativo != '' && this.detalles.length > 0){
                    Swal.fire({
                        title: 'Esta seguro de guardar?',
                        text: "Guardar el traslado en la base de datos, confirmar campos!",
                        icon: 'question',
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Si, guardar!'
                    }).then((result) => {
                        if (result.isConfirmed) {
                            var formData = new FormData();

                            for(let i=0; i <= this.detalles.length-1; i++){
                                const val = Object.values(this.detalles[i]).length;

                                for(let x = 0; x <= val; x++){
                                    formData.append("detallesAd["+i+"][]", Object.values(this.detalles[i])[x]);
                                }

                            }

                            formData.append("acto_administrativo", this.acto_administrativo);
                            formData.append("vigencia", this.vigencia);
                            formData.append("fecha", document.getElementById('fecha').value);
                            formData.append("finalizar", this.checked);

                            axios.post('presupuesto_ccpet/traslado/crear/ccp-traslados-vue.php?action=guardarAd', formData)
                            .then((response) => {
                                console.log(response.data);
                                if(response.data.insertaBien){
                                    Swal.fire({
                                        position: 'top-end',
                                        icon: 'success',
                                        title: 'El traslado se guard&oacute; con Exito',
                                        showConfirmButton: false,
                                        timer: 1500
                                    }).then((response) => {
                                            app.redireccionar();
                                        });
                                }else{

                                    Swal.fire(
                                        'Error!',
                                        'No se pudo guardar.',
                                        'error'
                                    );
                                }

                            });

                        }
                    }).finally(() => {
                        this.loading =  false;
                    });

                }else{
                    this.loading =  false;
                    Swal.fire(
                        'Falta informaci&oacute;n para guardar el traslado.',
                        'Verifique que todos los campos esten diligenciados.',
                        'warning'
                    );
                }
            }else{
                this.loading =  false;
                Swal.fire(
                    'La suma de creditos y contracreditos deben ser iguales.',
                    'Verifique que la diferencia entre sumas de creditos y contracreditos sea igual cero.',
                    'warning'
                );
            }
        },

        redireccionar: function(){
            /* const  cdp = this.consecutivo;
            const  vig = this.vigencia;
            location.href ="ccp-cdpVisualizar.php?is="+cdp+"&vig="+vig; */

            location.href ="ccp-traslados-vue.php";
        },

        puedeFinalizar: function(){

            var valorAcuerdo = this.quitarPesosYComas(this.valor_acuerdo);
            var credito = this.quitarPesosYComas(this.totalCredito);
            var contraCredito = this.quitarPesosYComas(this.totalContraCredito);

            if(parseFloat(valorAcuerdo) > 0){
                if(parseFloat(valorAcuerdo) == parseFloat(contraCredito) && parseFloat(valorAcuerdo) == parseFloat(credito)){
                    //this.checked = 'checked';
                }else{
                    Swal.fire(
                        'No se puede finalizar acto administrativo.',
                        'El valor del acto administrativo y el total de creditos y contracreditos debe ser igual.',
                        'warning'
                    );
                    this.checked = '';
                }
            }else{
                Swal.fire(
                    'No se puede finalizar acto administrativo.',
                    'El valor del acto administrativo debe ser mayor a 0.',
                    'warning'
                );
                this.checked = '';
            }

        },

        cambiaTipoDeGasto: function(){
            this.cuenta = '';
            this.nombreCuenta = '';
            this.fuente = '';
            this.nombreFuente = '';
            this.valor = '';
            this.programatico = '';
            this.nombreProgramatico = '';
            this.codProyecto = '';
            this.nombreProyecto = '';

            if(this.tiposDeGasto[this.tipoGasto-1][2] == 2.3){
                this.mostrarInversion = true;
                this.mostrarFuncionamiento = false;
            }else{
                this.mostrarFuncionamiento = true;
                this.mostrarInversion = false;
            }

        },

        cambiaTipoTraslado: function(){

            if(this.acto_administrativo != ''){
                this.cuenta = '';
                this.nombreCuenta = '';
                this.fuente = '';
                this.nombreFuente = '';
                this.valor = '';
                this.programatico = '';
                this.nombreProgramatico = '';
                this.codProyecto = '';
                this.nombreProyecto = '';
                this.rubroPresupuestal = '';

                if(this.tipo_traslado == 'R'){
                    this.mostrarSaldo = true;
                    this.mostrarRubroContra = false;
                }else{
                    this.mostrarSaldo = false;
                    this.mostrarRubroContra = true;
                }
            }else{
                this.tipo_traslado = '-1';
                Swal.fire(
                    'Falta escoger el acto adm.',
                    'Escoger el acto administrativo.',
                    'warning'
                    );
            }


        },

        ventanaCuenta: function(){

            if(this.tipo_traslado != '-1'){

                app.showModal_cuentas = true;
                axios.post('presupuesto_ccpet/traslado/crear/ccp-traslados-vue.php?action=cargarCuentas&inicioCuenta=' + this.tiposDeGasto[this.tipoGasto-1][2])
                    .then((response) => {
                        app.cuentasCcpet = response.data.cuentasCcpet;
                    });

            }else{
                Swal.fire(
                    'Falta escoger el tipo de traslado.',
                    'Escoger el tipo de cuenta de egresos.',
                    'warning'
                    );
            }


        },

        ventanaRubroCredito: function(){
                app.showModal_programaticoProyectos = true;
                axios.post('presupuesto_ccpet/traslado/crear/ccp-traslados-vue.php?action=cargarProyectosCredito&vigencia=' + this.vigencia)
                    .then((response) => {
                        app.proyectosProgramaticos = response.data.proyectosProgramaticos;
                        app.proyectosProgramaticosCopy= response.data.proyectosProgramaticos;
                        this.resultsBuscar = [...this.proyectosProgramaticos];
                    });
        },

        searchMonitorProgramaticoProyecto: async function(){
            //var keywordCuenta = app.toFormData(app.searchCuenta);

            this.proyectosProgramaticos = [];
            this.checkTodo = false;
            var text = this.buscarProgramaticoProyecto;
            const data = this.resultsBuscar;
            this.proyectosProgramaticos = await filtroEnArrayDeObjetos({'data': data, 'text': text});

            /* axios.post('presupuesto_ccpet/traslado/crear/ccp-traslados-vue.php?action=filtrarCuentas&inicioCuenta=' + this.tiposDeGasto[this.tipoGasto-1][2], keywordCuenta)
            .then((response) => {
                //console.log(response.data);
                app.cuentasCcpet = response.data.cuentasCcpet;

            }); */

        },

        buscarCta: async function(){
            if(this.cuenta != ''){
                if(this.tipo_traslado != '-1'){

                    await axios.post('vue/presupuesto_ccp/ccp-cdpbasico.php?action=buscarCuenta&cuenta=' + this.cuenta)
                    .then((response) => {
                        //console.log(response.data.resultBusquedaCuenta);
                        if(response.data.resultBusquedaCuenta.length > 0){
                            if(response.data.resultBusquedaCuenta[0][1] == 'C'){
                                app.nombreCuenta = response.data.resultBusquedaCuenta[0][0];

                            }else{
                                Swal.fire(
                                'Tipo de cuenta incorrecto.',
                                'Escoger una cuenta de captura (C)',
                                'warning'
                                ).then((result) => {
                                    app.nombreCuenta = '';
                                    app.cuenta = '';

                                });
                            }

                        }else{
                            Swal.fire(
                                'Cuenta incorrecta.',
                                'Esta cuenta no existe en el catalogo CCPET.',
                                'warning'
                                ).then((result) => {
                                    app.nombreCuenta = '';
                                    app.cuenta = '';
                                });
                        }

                    });

                }else{
                    Swal.fire(
                        'Falta escoger el tipo de cuenta.',
                        'Escoger el tipo de cuenta egresos.',
                        'warning'
                        ).then((result) => {
                            app.nombreCuenta = '';
                            app.cuenta = '';
                        });
                }
            }else{
                this.nombreCuenta = '';
            }

            this.parametrosCompletos();
        },

        seleccionarCuenta: function(cuentaSelec){
            if(cuentaSelec[6] == 'C'){

                this.cuenta = cuentaSelec [1];
                this.nombreCuenta = cuentaSelec [2];
                this.showModal_cuentas = false;

            }else{


                this.showModal_cuentas = false;
                Swal.fire(
                'Tipo de cuenta incorrecto.',
                'Escoger una cuenta de captura (C)',
                'warning'
                ).then((result) => {

                    if(result.isConfirmed || result.isDismissed){
                        this.showModal_cuentas = true;
                    }

                });


            }
            this.parametrosCompletos();
        },

        seleccionarProgramaticoProyecto: function(cuentaSelec){
            this.rubroPresupuestal = cuentaSelec [0];
            this.showModal_programaticoProyectos = false;
            this.buscarRubroPresupuestal();
        },

        searchMonitorCuenta: function(){
            var keywordCuenta = app.toFormData(app.searchCuenta);

            axios.post('presupuesto_ccpet/traslado/crear/ccp-traslados-vue.php?action=filtrarCuentas&inicioCuenta=' + this.tiposDeGasto[this.tipoGasto-1][2], keywordCuenta)
            .then((response) => {
                //console.log(response.data);
                app.cuentasCcpet = response.data.cuentasCcpet;

            });

        },

        ventanaFuente: function(){

            if(this.tipoGasto != '3'){
                if(this.cuenta != ''){
                    app.showModal_fuentes = true;
                    axios.post('presupuesto_ccpet/traslado/crear/ccp-traslados-vue.php?action=cargarFuentes')
                        .then((response) => {
                            //console.log(response.data);
                            app.fuentesCuipo = response.data.fuentes;

                        });
                }else{
                    Swal.fire(
                        'Falta escoger la cuenta presupuestal CCPET.',
                        'Escoger la cuenta antes de buscar la fuente, para acotar la busqueda.',
                        'warning'
                    )
                }

            }else{
                if(this.codProyecto != '' && this.programatico != ''){
                    app.showModal_fuentes = true;

                    axios.post('presupuesto_ccpet/traslado/crear/ccp-traslados-vue.php?action=cargarFuentes')
                        .then((response) => {
                            //console.log(response.data);
                            app.fuentesCuipo = response.data.fuentes;

                        });
                }else{
                    Swal.fire(
                        'Falta escoger el proyecto o indicador programatico.',
                        'Escoger el proyecto o indicador programatico antes de buscar la fuente, para acotar la busqueda.',
                        'warning'
                    )
                }
            }


        },

        buscarFuente: async function(){

            if(this.fuente != ''){
                await axios.post('vue/presupuesto_ccp/ccp-cdpbasico.php?action=buscarFuente&fuente=' + this.fuente)
                .then((response) => {
                    //console.log(response.data.resultBusquedaFuente);
                    if(response.data.resultBusquedaFuente.length > 0){

                        app.nombreFuente = response.data.resultBusquedaFuente[0][0];

                    }else{
                        Swal.fire(
                            'Fuente incorrecta.',
                            'Esta Fuente no existe en las fuentes CUIPO del sistema.',
                            'warning'
                            ).then((result) => {
                                app.nombreFuente = '';
                                app.fuente = '';
                            });
                    }

                });
            }else{
                this.nombreFuente = '';
                this.fuente = '';
            }

            this.parametrosCompletos();
        },

        searchMonitorFuente: function(){
            var keywordFuente = app.toFormData(app.searchFuente);
            axios.post('vue/presupuesto_ccp/ccp-cdpbasico.php?action=filtrarFuentes', keywordFuente)
            .then((response) => {
                app.fuentesCuipo = response.data.fuentes;

            });
        },

        seleccionarFuente: function(fuenteSelec){
            this.fuente = fuenteSelec [0];
            this.nombreFuente = fuenteSelec [1];
            this.showModal_fuentes = false;
            this.parametrosCompletos();
        },

        ventanaProyecto: function(){
            app.showModal_proyectos = true;
            axios.post('vue/presupuesto_ccp/ccp-cdpbasico.php?action=cargarProyecto&vigencia='+this.vigencia)
                .then((response) => {

                    app.proyectos = response.data.proyectosCcpet;
                });
        },

        seleccionarProyecto: function(proyectoSelec){

            this.codProyecto = proyectoSelec [2];
            this.nombreProyecto = proyectoSelec [4];
            this.showModal_proyectos = false;
            this.parametrosCompletos();
        },

        buscarProyecto: async function(){
            if(this.codProyecto != ''){
                await axios.post('vue/presupuesto_ccp/ccp-cdpbasico.php?action=buscarProyecto&proyecto=' + this.codProyecto)
                .then((response) => {
                    if(response.data.resultBusquedaProyecto.length > 0){
                            app.nombreProyecto = response.data.resultBusquedaProyecto[0][0];
                            this.buscarProgramatico();
                    }else{
                        Swal.fire(
                            'Proyecto incorrecto.',
                            'Este proyecto no existe; radicar proyecto en el módulo de planeación estratégica',
                            'warning'
                            ).then((result) => {
                                this.codProyecto = '';
                                app.nombreProyecto = '';
                                this.buscarProgramatico();

                            });
                    }

                });

            }else{
                this.nombreProyecto = '';
            }

            this.parametrosCompletos();
        },

        searchMonitorProyecto: function(){
            var keywordProyecto = app.toFormData(app.searchProyecto);
            axios.post('vue/presupuesto_ccp/ccp-cdpbasico.php?action=filtrarProyectos&vigencia=' + this.vigencia, keywordProyecto)
            .then((response) => {
                //console.log(response.data);
                app.proyectos = response.data.proyectosCcpet;
            });
        },

        ventanaProgramatico: function(){
            if(this.codProyecto != ''){
                app.showModal_programatico = true;
                axios.post('presupuesto_ccpet/traslado/crear/ccp-traslados-vue.php?action=cargarProgramaticos&proyecto=' + this.codProyecto)
                .then((response) => {
                    app.programaticos = response.data.programaticos;
                });
            }else{
                Swal.fire(
                    'Falta escoger el proyecto.',
                    'Antes de seleccionar el program&aacute;tico se debe seleccionar un proyecto.',
                    'warning'
                    ).then((result) => {
                        app.programaticos = [];
                    });
            }

        },

        buscarProgramatico: async function(bandera = false){
            if(this.programatico != '' && this.codProyecto != ''){
                if(this.codProyecto != '' || bandera){
                    await axios.post('presupuesto_ccpet/traslado/crear/ccp-traslados-vue.php?action=buscarProgramatico&programatico=' + this.programatico + '&vigencia=' + this.vigencia + '&bpim=' + this.codProyecto)
                    .then((response) => {
                        if(response.data.resultBusquedaProgramatico.length > 0){
                            app.nombreProgramatico = response.data.resultBusquedaProgramatico[0][0];
                            this.changeProgramatico();
                        }else{
                            Swal.fire(
                                'Program&aacute;tico incorrecto.',
                                'Este program&aacute;tico no existe; agregar program&aacute;tico al proyecto en el módulo de planeación estratégica',
                                'warning'
                                ).then((result) => {
                                    app.nombreProgramatico = '';
                                    app.programatico = '';
                                });
                        }

                    });
                }else{
                    Swal.fire(
                        'Falta escoger un proyecto.',
                        '',
                        'info'
                        );
                }


            }else{
                this.nombreProgramatico = '';
                this.programatico = '';
            }
            this.parametrosCompletos();
        },

        seleccionarProgramatico: function(programaticoSelec){

            this.programatico = programaticoSelec [0];
            this.nombreProgramatico = programaticoSelec [1];
            this.showModal_programatico = false;
            this.changeProgramatico();

        },

        parametrosCompletos: function(){
            if(this.tipoGasto == 3){

                if(this.codProyecto != '' &&this.programatico != ''  && this.fuente != '' && this.vigencia != '' && this.tipoGasto != '' && this.selectUnidadEjecutora != '' && this.selectMedioPago != '' && this.selectVigenciaGasto != ''){
                    this.calcularSaldoInversion();
                }else{
                    this.saldo = 0;
                }
            }else{

                if(this.cuenta != '' && this.fuente != '' && this.vigencia != '' && this.tipoGasto != '' && this.selectUnidadEjecutora != '' && this.selectMedioPago != '' && this.selectVigenciaGasto != ''){
                    this.calcularSaldo();
                }else{
                    this.saldo = 0;
                }
            }

        },

        //Calcular el saldo de la cuenta
        calcularSaldo: async function(){

            var formData = new FormData();
            //guardar cabecera
            //[rubro, fuente, vigencia, tipo_gasto, seccion_presupuestal, medio_pago, vigencia_gasto]

            formData.append("rubro",this.cuenta);
            formData.append("fuente", this.fuente);
            formData.append("vigencia", this.vigencia);
            formData.append("tipo_gasto", this.tipoGasto);
            formData.append("seccion_presupuestal", this.selectUnidadEjecutora);
            formData.append("medio_pago", this.selectMedioPago);
            formData.append("vigencia_gasto", this.selectVigenciaGasto);
            await axios.post('presupuesto_ccpet/traslado/crear/ccp-traslados-vue.php?action=saldoPorCuenta', formData)
                 .then((response) => {
                    app.saldo = response.data.saldoPorCuenta;
                 });
            this.calcularSaldoConDetalles();
        },

        calcularSaldoConDetalles: function(){

            this.detalles.forEach(function(elemento){

                if(app.cuenta == elemento[10] && app.fuente == elemento[12] && app.selectUnidadEjecutora == elemento[1] && app.selectMedioPago == elemento[3] && app.selectVigenciaGasto == elemento[4]){
                    app.saldo -= elemento[18];
                }
            });

        },

        calcularSaldoInversion: async function(){

            var formData = new FormData();
            //guardar cabecera
            //[rubro, fuente, vigencia, tipo_gasto, seccion_presupuestal, medio_pago, vigencia_gasto]

            formData.append("codProyecto",this.codProyecto);
            formData.append("programatico",this.programatico);
            formData.append("fuente", this.fuente);
            formData.append("vigencia", this.vigencia);
            formData.append("tipo_gasto", this.tipoGasto);
            formData.append("seccion_presupuestal", this.selectUnidadEjecutora);
            formData.append("medio_pago", this.selectMedioPago);
            formData.append("vigencia_gasto", this.selectVigenciaGasto);
            await axios.post('presupuesto_ccpet/traslado/crear/ccp-traslados-vue.php?action=saldoPorCuenta', formData)
                 .then((response) => {
                    app.saldo = response.data.saldoPorCuenta;
                 });
            this.calcularSaldoConDetallesInv();
        },

        calcularSaldoConDetallesInv: function(){

            this.detallesEliminados.forEach(function(elemento){
                //console.log(elemento);
                if(app.codProyecto == elemento[7] && app.programatico == elemento[9] && app.fuente == elemento[13] && app.selectUnidadEjecutora == elemento[2] && app.selectMedioPago == elemento[4] && app.selectVigenciaGasto == elemento[5]){
                    app.saldo += Number(elemento[16]);
                }
            });

        },

        async buscarRubroPresupuestal (){
            let partesRubroPresupuestal = divideRubro(this.rubroPresupuestal);

            const tipoGastos = {
                'funcionamiento': 1,
                'deuda': 2,
                'inversion': 3,
                'comercializacion': 4,
            }

            let claves = Object.keys(partesRubroPresupuestal);

            for(let i = 0; i < claves.length; i++){
                let clave = claves[i];

                if(clave == 'tipoObjeto'){
                    this.tipoGasto = tipoGastos[partesRubroPresupuestal[clave]];
                    await this.cambiaTipoDeGasto();
                }

                if(clave == 'cuenta'){
                    this.cuenta = partesRubroPresupuestal[clave];
                    this.buscarCta();
                }

                if(clave == 'fuente'){
                    this.fuente = partesRubroPresupuestal[clave];
                    this.buscarFuente();
                }

                if(clave == 'bpim'){
                    this.codProyecto = partesRubroPresupuestal[clave];
                    await this.buscarProyecto();
                }

                if(clave == 'programatico'){
                    this.programatico = partesRubroPresupuestal[clave];
                    /* this.buscarProgramatico(true);
                    this.changeProgramatico(); */
                }



                if(clave == 'seccion_presupuestal'){
                    this.selectUnidadEjecutora = partesRubroPresupuestal[clave];
                }

                if(clave == 'vigencia_gasto'){
                    this.selectVigenciaGasto = partesRubroPresupuestal[clave];
                }

                if(clave == 'medio_pago'){
                    this.selectMedioPago = partesRubroPresupuestal[clave];
                }

            }

        },

        toFormData: function(obj){
            var form_data = new FormData();
            for(var key in obj){
                form_data.append(key, obj[key]);
            }
            return form_data;
        },

        formatonumero: function(valor){
            return new Intl.NumberFormat("en-US", {style: "currency", currency: "USD"}).format(valor);
        },
        showTab:function(tab){
            let tabs = this.$refs.rTabs.children;
            let tabsContent = this.$refs.rTabsContent.children;
            for (let i = 0; i < tabs.length; i++) {
                tabs[i].classList.remove("active");
                tabsContent[i].classList.remove("active")
            }
            tabs[tab-1].classList.add("active");
            tabsContent[tab-1].classList.add("active")
        },
        reCalcularDiferencia:function(){
            const vueContext = this;
            this.arrFuentes = [];
            console.log(this.arrFuentes);
            for (let i = 0; i < vueContext.detalles.length; i++) {
                const detalle = vueContext.detalles[i];
                let credito = isNaN (detalle[15]) || detalle[15] == "" || detalle[15] == undefined ? 0 : detalle[15];
                let contraCredito = isNaN (detalle[16])|| detalle[16]=="" || detalle[15] == undefined ? 0 : detalle[16];
                credito = Math.round(parseFloat(credito));
                contraCredito = Math.round(parseFloat(contraCredito));
                if(vueContext.arrFuentes.length > 0){
                    let flag = false;
                    for (let j = 0; j < vueContext.arrFuentes.length; j++) {
                        const fuente = vueContext.arrFuentes[j];
                        if(fuente.fuente == detalle[13]){
                            fuente.credito+=credito;
                            fuente.contra_credito+=contraCredito;
                            fuente.saldo = parseFloat(fuente.credito) - parseFloat(fuente.contra_credito)
                            flag = true;
                            break;
                        }
                    }
                    if(!flag){
                        vueContext.arrFuentes.push(
                            {
                                fuente:detalle[13],
                                fuente_nombre:detalle[14],
                                credito:parseFloat(credito),
                                contra_credito:parseFloat(contraCredito),
                                saldo:parseFloat(credito) - parseFloat(contraCredito)
                            }
                        );
                    }
                }else{
                    vueContext.arrFuentes.push(
                        {
                            fuente:detalle[13],
                            fuente_nombre:detalle[14],
                            credito:parseFloat(credito),
                            contra_credito:parseFloat(contraCredito),
                            saldo:parseFloat(credito) - parseFloat(contraCredito)
                        }
                    );
                }
            }
        }

    }
});
