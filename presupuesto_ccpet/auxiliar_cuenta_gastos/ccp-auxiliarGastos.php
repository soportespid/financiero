<?php

    date_default_timezone_set("America/Bogota");
    require_once '../../comun.inc';
    require '../../funciones.inc';
    require '../../funcionesSP.inc.php';

    session_start();
    if($_POST){
        $obj = new Plantilla();
        if($_POST['action']=="get"){
            $obj->getData();
        }else if($_POST['action']=="gen"){
            $obj->getInfo();
        }
    }

    class Plantilla{
        private $linkbd;
        public function __construct() {
            $this->linkbd = conectar_v7();
            $this->linkbd->set_charset("utf8");
        }
        public function getData(){
            if(!empty($_SESSION)){
                $arrData = array(
                    "secciones"=>$this->selectSecciones(),
                    "vigencias"=>$this->selectVigencias(),
                    "fuentes"=>$this->selectFuentes(),
                    "cuentas"=>$this->selectCuentas(),
                    "bpim"=>$this->selectBpim(),
                    "programatico"=>$this->selectProgramatico(),
                    "orden"=>$this->selectOrden(),
                    "programas"=>$this->selectProgramas(),
                    "sectores"=>$this->selectSectores(),
                );
                echo json_encode($arrData,JSON_UNESCAPED_UNICODE);
            }
            die();
        }
        /******************Funciones para generar informe********************* */
        public function getInfo(){
            if(!empty($_SESSION)){
                $arrData = [];
                $strCuenta = $_POST['cuenta'];
                $strFuente = $_POST['fuente'];
                $strFechaInicial = $_POST['fecha_inicial'];
                $strFechaFinal = $_POST['fecha_final'];
                $intVigencia = $_POST['vigencia'];
                $strSeccion = $_POST['seccion'];
                $strPago = $_POST['pago'];
                $strBpim = $_POST['bpim'];
                $strProgramatico = $_POST['programatico'];
                $arrInicial = $this->selectInicial($strCuenta,$strSeccion,$intVigencia,$strPago,$strFuente,$strBpim,$strProgramatico,$strFechaInicial,$strFechaFinal);
                $arrAdicion = $this->selectAdicion($strCuenta,$strSeccion,$intVigencia,$strPago,$strFuente,$strBpim,$strProgramatico,$strFechaInicial,$strFechaFinal);
                $arrReduccion = $this->selectReduccion($strCuenta,$strSeccion,$intVigencia,$strPago,$strFuente,$strBpim,$strProgramatico,$strFechaInicial,$strFechaFinal);
                $arrCredito = $this->selectTraslados($strCuenta,$strSeccion,$intVigencia,$strPago,$strFuente,$strBpim,$strProgramatico,$strFechaInicial,$strFechaFinal,"C");
                $arrContraCredito = $this->selectTraslados($strCuenta,$strSeccion,$intVigencia,$strPago,$strFuente,$strBpim,$strProgramatico,$strFechaInicial,$strFechaFinal,"R");
                $arrCdp = $this->selectCdp($strCuenta,$strSeccion,$intVigencia,$strPago,$strFuente,$strBpim,$strProgramatico,$strFechaInicial,$strFechaFinal);
                $arrRp = $this->selectRp($strCuenta,$strSeccion,$intVigencia,$strPago,$strFuente,$strBpim,$strProgramatico,$strFechaInicial,$strFechaFinal);
                $arrCxp = $this->selectCxp($strCuenta,$strSeccion,$intVigencia,$strPago,$strFuente,$strBpim,$strProgramatico,$strFechaInicial,$strFechaFinal);
                $arrNomina = $this->selectNomina($strCuenta,$strSeccion,$intVigencia,$strPago,$strFuente,$strBpim,$strProgramatico,$strFechaInicial,$strFechaFinal);
                $arrEgresos = $this->selectEgresos($strCuenta,$strSeccion,$intVigencia,$strPago,$strFuente,$strBpim,$strProgramatico,$strFechaInicial,$strFechaFinal);
                $arrEgresosNomina = $this->selectEgresosNomina($strCuenta,$strSeccion,$intVigencia,$strPago,$strFuente,$strBpim,$strProgramatico,$strFechaInicial,$strFechaFinal);
                $definitivo = $arrInicial['total'] + $arrAdicion['total'] - $arrReduccion['total'] + $arrCredito['total'] - $arrContraCredito['total'];
                $totalObligaciones = $arrCxp['total'] + $arrNomina['total'];
                $totalEgresos = $arrEgresos['total'] + $arrEgresosNomina['total'];
                $totalSaldo = $definitivo - $arrCdp['total'];
                //Inicial
                array_push($arrData,$arrInicial['data']);
                //Adicion
                array_push($arrData,$arrAdicion['data']);
                //Reducción
                array_push($arrData,$arrReduccion['data']);
                //Crédito
                array_push($arrData,$arrCredito['data']);
                //Contra crédito
                array_push($arrData,$arrContraCredito['data']);
                //Definitivo
                array_push($arrData,[array("tipo"=>"T","comprobante"=>"Definitivo","valor"=>$definitivo)]);
                //Cdp
                array_push($arrData,$arrCdp['data']);
                //Rp
                array_push($arrData,$arrRp['data']);
                //CXP
                array_push($arrData,$arrCxp['data']);
                //Nomina
                array_push($arrData,$arrNomina['data']);
                //Total obligaciones
                array_push($arrData,[array("tipo"=>"T","comprobante"=>"Total obligaciones","valor"=>$totalObligaciones)]);
                //Egresos
                array_push($arrData,$arrEgresos['data']);
                array_push($arrData,$arrEgresosNomina['data']);
                //Total egresos
                array_push($arrData,[array("tipo"=>"T","comprobante"=>"Total egresos","valor"=>$totalEgresos)]);
                //Total saldo
                array_push($arrData,[array("tipo"=>"T","comprobante"=>"Total saldo","valor"=>$totalSaldo)]);
                $arrData = $this->fixArrayInfo($arrData);
                $arrResponse = array("status"=>true,"html"=>$this->getHtml($arrData));
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
            die();
        }
        public function getHtml($data){
            $html ="";
            foreach ($data as $row) {
                $strFechaFormat ="";
                if(isset($row['fecha'])){
                    $strFecha = date_create($row['fecha']);
                    $strFechaFormat = date_format($strFecha,"d/m/Y");
                }
                if($row['tipo'] == "T"){
                    $html.='
                    <tr class="bg-secondary fw-bold">
                        <td>'.$row['comprobante'].'</td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td class="text-right">$'.number_format($row['valor']).'</td>
                    </tr>
                    ';
                }else{
                    $event = "";
                    if(isset($row['url'])){
                        $link = "window.open('".$row['url']."','_blank')";
                        $event = 'ondblclick="'.$link.'"';
                    }
                    $html.='
                    <tr '.$event.'>
                        <td>'.$row['comprobante'].'</td>
                        <td>'.$row['consecutivo'].'</td>
                        <td>'.$row['tercero'].'</td>
                        <td>'.$row['objeto'].'</td>
                        <td>'.$strFechaFormat.'</td>
                        <td>'.$row['estado'].'</td>
                        <td class="text-right">$'.number_format($row['valor']).'</td>
                    </tr>
                    ';
                }
            }
            return $html;
        }
        public function fixArrayInfo(array $arrInfo){
            $arrData = [];
            foreach ($arrInfo as $cab) {
                foreach ($cab as $det) {
                    array_push($arrData,$det);
                }
            }
            return $arrData;
        }
        public function selectEgresosNomina($cuenta,$secPresupuestal,$vigGasto,$medioPago,$fuente,$bpim,$programatico,$strFechaInicial,$strFechaFinal){
            $vigenciaIni = explode("-",$strFechaInicial)[0];
            $vigenciaFin = explode("-",$strFechaFinal)[0];
            $arrData = [];
            $vigGasto = $vigGasto != 0 ? " AND TD.vigencia_gasto LIKE '$vigGasto'" : "";
            $secPresupuestal = $secPresupuestal != 0 ? " AND TD.seccion_presupuestal LIKE '$secPresupuestal'" : "";
            $medioPago = !is_numeric($medioPago) ? " AND TD.medio_pago LIKE '$medioPago' " : "";

            $sql = "SELECT TE.id_egreso, TE.fecha, TD.valor,TE.concepto,TE.tercero
            FROM tesoegresosnomina_det AS TD, tesoegresosnomina AS TE
            WHERE (TE.vigencia = '$vigenciaIni' OR TE.vigencia = '$vigenciaFin') AND TE.fecha BETWEEN '$strFechaInicial' AND '$strFechaFinal'
            AND TD.id_egreso = TE.id_egreso AND TE.estado = 'S' AND NOT(TD.tipo='SE' OR TD.tipo='PE' OR TD.tipo='DS' OR TD.tipo='RE' OR TD.tipo='FS')
            AND TD.cuentap LIKE '%$cuenta%' $secPresupuestal $vigGasto $medioPago AND TD.fuente = '$fuente' AND TD.bpin LIKE '%$bpim%'
            AND TD.indicador_producto LIKE '%$programatico%'";
            $request = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
            $total = 0;

            if(!empty($request)){
                foreach ($request as $e) {
                    $sql="SELECT cedulanit as documento,
                    CASE WHEN razonsocial IS NULL OR razonsocial = ''
                    THEN CONCAT(nombre1,' ',nombre2,' ',apellido1,' ',apellido2)
                    ELSE razonsocial END AS nombre FROM terceros  WHERE cedulanit = '$e[tercero]'";
                    $arrTercero = mysqli_query($this->linkbd,$sql)->fetch_assoc();
                    array_push($arrData,array(
                        "url"=>'teso-pagonominaver.php?idegre='.$e['id_egreso'],
                        "tipo"=>"D",
                        "comprobante"=>"Egresos nomina",
                        "consecutivo"=>$e['id_egreso'],
                        "fecha"=>$e['fecha'],
                        "estado"=>"Activo",
                        "valor"=>$e['valor'],
                        "tercero"=>$arrTercero['documento']."-".$arrTercero['nombre'],
                        "objeto"=>$e['concepto']
                    ));
                    $total+=$e['valor'];
                }
            }
            return array("data"=>$arrData,"total"=>$total);
        }
        public function selectEgresos($cuenta,$secPresupuestal,$vigGasto,$medioPago,$fuente,$bpim,$programatico,$strFechaInicial,$strFechaFinal){
            $vigenciaIni = explode("-",$strFechaInicial)[0];
            $vigenciaFin = explode("-",$strFechaFinal)[0];
            $arrData = [];
            $vigGasto = $vigGasto != 0 ? " AND TD.codigo_vigenciag LIKE '$vigGasto'" : "";
            $secPresupuestal = $secPresupuestal != 0 ? " AND TD.seccion_presupuestal LIKE '$secPresupuestal'" : "";
            $medioPago = !is_numeric($medioPago) ? " AND TD.medio_pago LIKE '$medioPago' " : "";


            $sql = "SELECT TE.id_egreso, TE.fecha, TD.valor,TE.tercero,TE.concepto
            FROM tesoordenpago_det AS TD, tesoegresos AS TE
            WHERE (TD.vigencia = '$vigenciaIni' OR TD.vigencia = '$vigenciaFin') AND TE.fecha BETWEEN '$strFechaInicial' AND '$strFechaFinal'
            AND TE.estado != 'N' AND TE.estado != 'R' AND TD.id_orden = TE.id_orden AND TD.cuentap LIKE '%$cuenta%' $secPresupuestal
            $vigGasto $medioPago AND TD.bpim LIKE '%$bpim%' AND TD.indicador_producto LIKE '%$programatico%'
            AND TD.fuente = '$fuente'";
            $request = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
            $total = 0;

            if(!empty($request)){
                foreach ($request as $e) {
                    $sql="SELECT cedulanit as documento,
                    CASE WHEN razonsocial IS NULL OR razonsocial = ''
                    THEN CONCAT(nombre1,' ',nombre2,' ',apellido1,' ',apellido2)
                    ELSE razonsocial END AS nombre FROM terceros  WHERE cedulanit = '$e[tercero]'";
                    $arrTercero = mysqli_query($this->linkbd,$sql)->fetch_assoc();
                    array_push($arrData,array(
                        "url"=>'teso-girarchequesver-ccpet.php?idegre='.$e['id_egreso'],
                        "tipo"=>"D",
                        "comprobante"=>"Egresos",
                        "consecutivo"=>$e['id_egreso'],
                        "fecha"=>$e['fecha'],
                        "estado"=>"Activo",
                        "valor"=>$e['valor'],
                        "tercero"=>$arrTercero['documento']."-".$arrTercero['nombre'],
                        "objeto"=>$e['concepto']
                    ));
                    $total+=$e['valor'];
                }
            }
            return array("data"=>$arrData,"total"=>$total);
        }
        public function selectNomina($cuenta,$secPresupuestal,$vigGasto,$medioPago,$fuente,$bpim,$programatico,$strFechaInicial,$strFechaFinal){
            $arrData = [];
            $vigGasto = $vigGasto != 0 ? " AND HD.vigencia_gasto LIKE '$vigGasto'" : "";
            $secPresupuestal = $secPresupuestal != 0 ? " AND HD.seccion_presupuestal LIKE '$secPresupuestal'" : "";
            $medioPago = !is_numeric($medioPago) ? " AND HD.medio_pago LIKE '$medioPago' " : "";

            $sql = "SELECT HN.id_aprob, HN.fecha, HD.valor,HN.tercero
            FROM humnom_presupuestal AS HD, humnomina_aprobado AS HN
            WHERE HN.id_nom = HD.id_nom AND HN.fecha BETWEEN '$strFechaInicial' AND '$strFechaFinal'
            AND HN.estado!='N' AND HD.estado='P' AND HD.cuenta LIKE '%$cuenta%' $secPresupuestal $vigGasto $medioPago
            AND HD.fuente = '$fuente' AND HD.bpin LIKE '%$bpim%' AND HD.indicador LIKE '%$programatico%'";
            $request = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
            $total = 0;

            if(!empty($request)){
                foreach ($request as $e) {
                    $sql="SELECT cedulanit as documento,
                    CASE WHEN razonsocial IS NULL OR razonsocial = ''
                    THEN CONCAT(nombre1,' ',nombre2,' ',apellido1,' ',apellido2)
                    ELSE razonsocial END AS nombre FROM terceros  WHERE cedulanit = '$e[tercero]'";
                    $arrTercero = mysqli_query($this->linkbd,$sql)->fetch_assoc();
                    array_push($arrData,array(
                        "url"=>'hum-aprobarnominaver.php?idr='.$e['id_aprob'],
                        "tipo"=>"D",
                        "comprobante"=>"Obligacion nomina",
                        "consecutivo"=>$e['id_aprob'],
                        "fecha"=>$e['fecha'],
                        "estado"=>"Activo",
                        "valor"=>$e['valor'],
                        "tercero"=>$arrTercero['documento']."-".$arrTercero['nombre'],
                        "objeto"=>""
                    ));
                    $total+=$e['valor'];
                }
            }
            return array("data"=>$arrData,"total"=>$total);
        }
        public function selectCxp($cuenta,$secPresupuestal,$vigGasto,$medioPago,$fuente,$bpim,$programatico,$strFechaInicial,$strFechaFinal){
            $vigenciaIni = explode("-",$strFechaInicial)[0];
            $vigenciaFin = explode("-",$strFechaFinal)[0];
            $arrData = [];
            $vigGasto = $vigGasto != 0 ? " AND DET.codigo_vigenciag LIKE '$vigGasto'" : "";
            $secPresupuestal = $secPresupuestal != 0 ? " AND DET.seccion_presupuestal LIKE '$secPresupuestal'" : "";
            $medioPago = !is_numeric($medioPago) ? " AND DET.medio_pago LIKE '$medioPago' " : "";

            $sql = "SELECT CAB.id_orden, CAB.fecha, DET.valor, CAB.estado,
            CAB.tercero,CAB.conceptorden
            FROM tesoordenpago_det AS DET, tesoordenpago AS CAB
            WHERE CAB.id_orden = DET.id_orden AND CAB.fecha BETWEEN '$strFechaInicial' AND '$strFechaFinal'
            AND (DET.vigencia = '$vigenciaIni' OR DET.vigencia = '$vigenciaFin') AND DET.cuentap LIKE '%$cuenta%'
            $secPresupuestal $medioPago $vigGasto AND DET.fuente = '$fuente' AND DET.bpim LIKE '%$bpim%'
            AND DET.indicador_producto LIKE '%$programatico%' AND CAB.estado != 'R'";
            $request = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);

            $total = 0;
            if(!empty($request)){
                foreach ($request as $e) {
                    $sql="SELECT cedulanit as documento,
                    CASE WHEN razonsocial IS NULL OR razonsocial = ''
                    THEN CONCAT(nombre1,' ',nombre2,' ',apellido1,' ',apellido2)
                    ELSE razonsocial END AS nombre FROM terceros  WHERE cedulanit = '$e[tercero]'";
                    $arrTercero = mysqli_query($this->linkbd,$sql)->fetch_assoc();
                    array_push($arrData,array(
                        "url"=>'teso-egresoverccpet.php?idop='.$e['id_orden'],
                        "tipo"=>"D",
                        "comprobante"=>"Obligacion",
                        "consecutivo"=>$e['id_orden'],
                        "fecha"=>$e['fecha'],
                        "estado"=>"Activo",
                        "valor"=>$e['valor'],
                        "tercero"=>$arrTercero['documento']."-".$arrTercero['nombre'],
                        "objeto"=>$e['conceptorden']
                    ));
                    $total+=$e['valor'];
                }
            }
            return array("data"=>$arrData,"total"=>$total);
        }
        public function selectRp($cuenta,$secPresupuestal,$vigGasto,$medioPago,$fuente,$bpim,$programatico,$strFechaInicial,$strFechaFinal){
            $vigenciaIni = explode("-",$strFechaInicial)[0];
            $vigenciaFin = explode("-",$strFechaFinal)[0];
            $arrData = [];
            $vigGasto = $vigGasto != 0 ? " AND TB2.codigo_vigenciag LIKE '$vigGasto'" : "";
            $secPresupuestal = $secPresupuestal != 0 ? " AND TB2.seccion_presupuestal LIKE '$secPresupuestal'" : "";
            $medioPago = !is_numeric($medioPago) ? " AND TB2.medio_pago LIKE '$medioPago' " : "";

            $sql = "SELECT TB2.consvigencia, TB1.fecha, TB2.valor, TB1.vigencia, TB1.estado,
            TB1.detalle,TB1.tercero
            FROM ccpetrp AS TB1, ccpetrp_detalle AS TB2
            WHERE TB1.vigencia = TB2.vigencia AND TB1.consvigencia = TB2.consvigencia AND (TB1.fecha BETWEEN '$strFechaInicial' AND '$strFechaFinal')
            AND (TB1.vigencia = '$vigenciaIni' OR TB1.vigencia = '$vigenciaFin')  $secPresupuestal
            $medioPago $vigGasto AND TB2.bpim LIKE '%$bpim%' AND TB2.indicador_producto LIKE '%$programatico%'
            AND TB2.cuenta LIKE '%$cuenta%' AND TB1.tipo_mov = '201'
            AND TB2.tipo_mov LIKE '201' AND TB2.fuente = '$fuente'";
            $request = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);

            $total = 0;
            if(!empty($request)){
                foreach ($request as $e) {
                    $sqlRev = "SELECT COALESCE(SUM(TB2.valor),0) as valor
                    FROM ccpetrp_detalle AS TB2, ccpetrp AS TB1
                    WHERE TB1.consvigencia = TB2.consvigencia AND TB1.vigencia = TB2.vigencia AND TB1.fecha BETWEEN '$strFechaInicial' AND '$strFechaFinal'
                    AND TB1.consvigencia = '$e[consvigencia]'  AND TB1.vigencia = '$e[vigencia]' AND (TB2.vigencia = '$vigenciaIni' OR TB2.vigencia = '$vigenciaFin')
                    AND TB2.cuenta LIKE '%$cuenta%' AND TB1.tipo_mov LIKE '4%' AND TB2.tipo_mov LIKE '4%' AND TB2.bpim LIKE '%$bpim%'
                    AND TB2.indicador_producto LIKE '%$programatico%' $secPresupuestal AND TB2.fuente = '$fuente'
                    $medioPago $vigGasto";
                    $estado = $e['estado'] != 'N' ? 'Activo' : 'Anulado';
                    $requestRev = mysqli_query($this->linkbd,$sqlRev)->fetch_assoc();
                    $valor = $e['valor'];
                    if(!empty($requestRev)){
                        $valor = $e['valor']-$requestRev['valor'];
                    }
                    $strFecha = explode("-",$e['fecha'])[0];
                    $sql="SELECT cedulanit as documento,
                    CASE WHEN razonsocial IS NULL OR razonsocial = ''
                    THEN CONCAT(nombre1,' ',nombre2,' ',apellido1,' ',apellido2)
                    ELSE razonsocial END AS nombre FROM terceros  WHERE cedulanit = '$e[tercero]'";
                    $arrTercero = mysqli_query($this->linkbd,$sql)->fetch_assoc();
                    array_push($arrData,array(
                        "url"=>'ccp-rpVisualizar.php?is='.$e['consvigencia'].'&vig='.$strFecha,
                        "tipo"=>"D",
                        "comprobante"=>"Compromisos",
                        "consecutivo"=>$e['consvigencia'],
                        "fecha"=>$e['fecha'],
                        "estado"=>$estado,
                        "valor"=>$valor,
                        "tercero"=>$arrTercero['documento']."-".$arrTercero['nombre'],
                        "objeto"=>$e['detalle']
                    ));
                    $total+=$valor;
                }
            }
            array_push($arrData,array(
                "tipo"=>"T",
                "comprobante"=>"Total compromisos",
                "valor"=>$total,
            ));
            return array("data"=>$arrData,"total"=>$total);
        }
        public function selectCdp($cuenta,$secPresupuestal,$vigGasto,$medioPago,$fuente,$bpim,$programatico,$strFechaInicial,$strFechaFinal){
            $arrData = [];
            $vigGasto = $vigGasto != 0 ? " AND TB2.codigo_vigenciag LIKE '$vigGasto'" : "";
            $secPresupuestal = $secPresupuestal != 0 ? " AND TB2.seccion_presupuestal LIKE '$secPresupuestal'" : "";
            $medioPago = $medioPago != 0 ? " AND TB2.medio_pago LIKE '$medioPago' " : "";

            $sql = "SELECT TB2.consvigencia, TB1.fecha,TB1.solicita,TB1.objeto, TB2.valor, TB2.vigencia, TB1.estado
            FROM ccpetcdp AS TB1, ccpetcdp_detalle AS TB2
            WHERE TB1.vigencia = TB2.vigencia AND TB1.consvigencia = TB2.consvigencia $secPresupuestal $medioPago $vigGasto
            AND TB2.fuente = '$fuente' AND TB2.cuenta LIKE '%$cuenta%' AND TB2.tipo_mov LIKE '201' AND TB1.tipo_mov LIKE '201'
            AND TB2.bpim LIKE '%$bpim%' AND TB2.indicador_producto LIKE '%$programatico%'
            AND TB1.fecha BETWEEN '$strFechaInicial' AND '$strFechaFinal'";

            $request = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
            $total = 0;
            if(!empty($request)){
                foreach ($request as $e) {
                    $sqlCdpRev = "SELECT COALESCE(SUM(TB2.valor),0) as valor
                    FROM ccpetcdp AS TB1, ccpetcdp_detalle AS TB2
                    WHERE TB1.vigencia = TB2.vigencia AND TB1.consvigencia = TB2.consvigencia AND TB1.consvigencia = '$e[consvigencia]'
                    $secPresupuestal $medioPago $vigGasto AND TB2.cuenta LIKE '%$cuenta%' AND TB2.tipo_mov LIKE '4%'
                    AND TB1.tipo_mov LIKE '4%' AND TB2.bpim LIKE '%$bpim%' AND TB2.indicador_producto LIKE '%$programatico%'
                    AND TB1.fecha BETWEEN '$strFechaInicial' AND '$strFechaFinal' AND TB2.fuente = '$fuente'";
                    $estado = $e['estado'] != 'N' ? 'Activo' : 'Anulado';
                    $requestCdp = mysqli_query($this->linkbd,$sqlCdpRev)->fetch_assoc();
                    $valor = $e['valor'];
                    if(!empty($requestCdp)){
                        $valor = $e['valor']-$requestCdp['valor'];
                    }
                    $strFecha = explode("-",$e['fecha'])[0];
                    $sql="SELECT cedulanit as documento,
                    CASE WHEN razonsocial IS NULL OR razonsocial = ''
                    THEN CONCAT(nombre1,' ',nombre2,' ',apellido1,' ',apellido2)
                    ELSE razonsocial END AS nombre FROM terceros  WHERE CONCAT(nombre1,' ',nombre2,' ',apellido1,' ',apellido2) = '$e[solicita]'
                    OR razonsocial = '$e[solicita]'";
                    $arrTercero = mysqli_query($this->linkbd,$sql)->fetch_assoc();
                    array_push($arrData,array(
                        "url"=>'ccp-cdpVisualizar.php?is='.$e['consvigencia'].'&vig='.$strFecha,
                        "tipo"=>"D",
                        "comprobante"=>"Disponibilidad",
                        "consecutivo"=>$e['consvigencia'],
                        "fecha"=>$e['fecha'],
                        "estado"=>$estado,
                        "valor"=>$valor,
                        "tercero"=>!empty($arrTercero) ? $arrTercero['documento']."-".$arrTercero['nombre'] : $e['solicita'],
                        "objeto"=>$e['objeto']
                    ));
                    $total+=$valor;
                }
            }
            array_push($arrData,array(
                "tipo"=>"T",
                "comprobante"=>"Total disponibilidad",
                "valor"=>$total,
            ));
            return array("data"=>$arrData,"total"=>$total);
        }
        public function selectTraslados($cuenta,$secPresupuestal,$vigGasto,$medioPago,$fuente,$bpim,$programatico,$strFechaInicial,$strFechaFinal,$tipo){
            $arrData = [];
            $vigGasto = $vigGasto != 0 ? " AND codigo_vigenciag LIKE '$vigGasto'" : "";
            $secPresupuestal = $secPresupuestal != 0 ? " AND seccion_presupuestal LIKE '$secPresupuestal'" : "";
            $medioPago = $medioPago != 0 ? " AND medio_pago LIKE '$medioPago'" : "";

            $sql = "SELECT id_acuerdo, fecha, valor, estado FROM ccpet_traslados
            WHERE cuenta LIKE '%$cuenta%' AND tipo = '$tipo' $secPresupuestal $medioPago $vigGasto AND fuente = '$fuente'
            AND estado = 'S' AND bpim LIKE '%$bpim%' AND programatico LIKE '%$programatico%' AND fecha
            BETWEEN '$strFechaInicial' AND '$strFechaFinal'";
            $request = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
            $total = 0;
            if(!empty($request)){
                foreach ($request as $e) {
                    $sqlAcuerdo = "SELECT consecutivo, tipo_acto_adm FROM ccpetacuerdos WHERE id_acuerdo = $e[id_acuerdo]";
                    $requestAcuerdo = mysqli_query($this->linkbd,$sqlAcuerdo)->fetch_assoc();
                    $tipoActo = '';
                    $estado = $e['estado'] == 'S' ? 'Activo' : 'Anulado';
                    if($requestAcuerdo['tipo_acto_adm'] == 1) {
                        $tipoActo = 'Acuerdo';
                    }elseif ($requestAcuerdo['tipo_acto_adm'] == 2) {
                        $tipoActo = 'Resolucion';
                    }else {
                        $tipoActo = 'Decreto';
                    }
                    array_push($arrData,array(
                        "url"=>"ccp-visualizaTraslado.php?id=".$e['id_acuerdo'],
                        "tipo"=>"D",
                        "comprobante"=>$tipo == "C" ? "Credito" :"Contra credito",
                        "consecutivo"=>$e['id_acuerdo']."-".$tipoActo.":".$requestAcuerdo['consecutivo'],
                        "fecha"=>$e['fecha'],
                        "estado"=>$estado,
                        "valor"=>$e['valor'],
                        "tercero"=>"",
                        "objeto"=>""
                    ));
                    $total+=$e['valor'];
                }
            }
            array_push($arrData,array(
                "tipo"=>"T",
                "comprobante"=>$tipo == "C" ? "Total creditos" :"Total contra creditos",
                "valor"=>$total,
            ));
            return array("data"=>$arrData,"total"=>$total);
        }
        public function selectReduccion($cuenta,$secPresupuestal,$vigGasto,$medioPago,$fuente,$bpim,$programatico,$strFechaInicial,$strFechaFinal){
            $arrData = [];
            $vigGasto = $vigGasto != 0 ? " AND codigo_vigenciag LIKE '$vigGasto'" : "";
            $secPresupuestal = $secPresupuestal != 0 ? " AND seccion_presupuestal LIKE '$secPresupuestal'" : "";
            $medioPago = $medioPago != 0 ? " AND medio_pago LIKE '$medioPago'" : "";

            $sql = "SELECT id_adicion, fecha, valor, estado FROM ccpet_reducciones
            WHERE cuenta LIKE '%$cuenta%' AND tipo_cuenta = 'G' $secPresupuestal $medioPago $vigGasto AND fuente = '$fuente'
            AND estado = 'S' AND bpim LIKE '%$bpim%' AND programatico LIKE '%$programatico%' AND fecha
            BETWEEN '$strFechaInicial' AND '$strFechaFinal'";
            $request = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
            $total = 0;
            if(!empty($request)){
                foreach ($request as $e) {
                    $sqlAcuerdo = "SELECT consecutivo, tipo_acto_adm FROM ccpetacuerdos WHERE id_acuerdo = $e[id_acuerdo]";
                    $requestAcuerdo = mysqli_query($this->linkbd,$sqlAcuerdo)->fetch_assoc();
                    $tipoActo = '';
                    $estado = $e['estado'] == 'S' ? 'Activo' : 'Anulado';
                    if($requestAcuerdo['tipo_acto_adm'] == 1) {
                        $tipoActo = 'Acuerdo';
                    }elseif ($requestAcuerdo['tipo_acto_adm'] == 2) {
                        $tipoActo = 'Resolucion';
                    }else {
                        $tipoActo = 'Decreto';
                    }
                    array_push($arrData,array(
                        "tipo"=>"D",
                        "comprobante"=>"Reducción",
                        "consecutivo"=>$e['id_adicion']."-".$tipoActo.":".$requestAcuerdo['consecutivo'],
                        "fecha"=>$e['fecha'],
                        "estado"=>$estado,
                        "valor"=>$e['valor'],
                        "tercero"=>"",
                        "objeto"=>""
                    ));
                    $total+=$e['valor'];
                }
            }
            array_push($arrData,array(
                "tipo"=>"T",
                "comprobante"=>"Total reducción",
                "valor"=>$total,
            ));
            return array("data"=>$arrData,"total"=>$total);
        }
        public function selectAdicion($cuenta,$secPresupuestal,$vigGasto,$medioPago,$fuente,$bpim,$programatico,$strFechaInicial,$strFechaFinal){
            $arrData = [];
            $vigGasto = $vigGasto != 0 ? " AND codigo_vigenciag LIKE '$vigGasto'" : "";
            $secPresupuestal = $secPresupuestal != 0 ? " AND seccion_presupuestal LIKE '$secPresupuestal'" : "";
            $medioPago = $medioPago != 0 ? " AND medio_pago LIKE '$medioPago'" : "";

            $sql = "SELECT id_acuerdo, fecha, valor, estado FROM ccpet_adiciones
            WHERE cuenta LIKE '%$cuenta%' AND tipo_cuenta = 'G' $secPresupuestal $medioPago $vigGasto AND fuente = '$fuente'
            AND estado = 'S' AND bpim LIKE '%$bpim%' AND programatico LIKE '%$programatico%' AND fecha
            BETWEEN '$strFechaInicial' AND '$strFechaFinal'";
            $request = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
            $total = 0;
            if(!empty($request)){
                foreach ($request as $e) {
                    $sqlAcuerdo = "SELECT consecutivo, tipo_acto_adm FROM ccpetacuerdos WHERE id_acuerdo = $e[id_acuerdo]";
                    $requestAcuerdo = mysqli_query($this->linkbd,$sqlAcuerdo)->fetch_assoc();
                    $tipoActo = '';
                    $estado = $e['estado'] == 'S' ? 'Activo' : 'Anulado';
                    if($requestAcuerdo['tipo_acto_adm'] == 1) {
                        $tipoActo = 'Acuerdo';
                    }elseif ($requestAcuerdo['tipo_acto_adm'] == 2) {
                        $tipoActo = 'Resolucion';
                    }else {
                        $tipoActo = 'Decreto';
                    }
                    array_push($arrData,array(
                        "url"=>'ccp-visualizaAdicion.php?id='.$e['id_acuerdo'],
                        "tipo"=>"D",
                        "comprobante"=>"Adición",
                        "consecutivo"=>$e['id_acuerdo']."-".$tipoActo.":".$requestAcuerdo['consecutivo'],
                        "fecha"=>$e['fecha'],
                        "estado"=>$estado,
                        "valor"=>$e['valor'],
                        "tercero"=>"",
                        "objeto"=>""
                    ));
                    $total+=$e['valor'];
                }
            }
            array_push($arrData,array(
                "tipo"=>"T",
                "comprobante"=>"Total adición",
                "valor"=>$total,
            ));
            return array("data"=>$arrData,"total"=>$total);
        }
        public function selectInicial($cuenta,$secPresupuestal,$vigGasto,$medioPago,$fuente,$bpim,$programatico,$strFechaInicial,$strFechaFinal){
            $arrData = [];
            $vigenciaIni = explode("-",$strFechaInicial)[0];
            $vigenciaFin = explode("-",$strFechaFinal)[0];
            $total = 0;
            $fecha = "";
            if($cuenta !=""){
                $vigGasto = $vigGasto != 0 ? " AND vigencia_gasto LIKE '$vigGasto'" : "";
                $secPresupuestal = $secPresupuestal != 0 ? " AND seccion_presupuestal LIKE '$secPresupuestal'" : "";
                $medioPago = $medioPago != 0 ? " AND medio_pago LIKE '$medioPago'" : "";

                $sql = "SELECT COALESCE(valor,0) as valor, vigencia
                FROM ccpetinicialgastosfun
                WHERE cuenta LIKE '$cuenta' $secPresupuestal $vigGasto $medioPago AND fuente = '$fuente'
                AND (vigencia = '$vigenciaIni' OR vigencia = '$vigenciaFin')";
            }else{
                $sql="SELECT COALESCE(TB2.valorcsf + TB2.valorssf, 0) as valor, TB1.vigencia
                FROM ccpproyectospresupuesto AS TB1, ccpproyectospresupuesto_presupuesto AS TB2
                WHERE TB1.id = TB2.codproyecto AND TB1.codigo = '$bpim' AND TB1.vigencia = '$vigenciaIni' AND TB1.vigencia = '$vigenciaFin'
                AND TB2.id_fuente = '$fuente' AND TB2.medio_pago = '$medioPago' AND TB2.indicador_producto = '$programatico' AND TB2.vigencia_gasto = '$vigGasto'";
            }
            $request = mysqli_query($this->linkbd,$sql)->fetch_assoc();
            if(!empty($request)){
                $total = $request['valor'];
                $fecha = $request['vigencia'];
            }

            array_push($arrData,array(
                "tipo"=>"D",
                "comprobante"=>"Presupuesto inicial",
                "consecutivo"=>"",
                "fecha"=>$fecha,
                "estado"=>"",
                "valor"=>$total,
                "tercero"=>"",
                "objeto"=>""
            ));
            return array("data"=>$arrData,"total"=>$total);
        }
        /******************Funciones para datos iniciales********************* */
        public function selectProgramas(){
            $sql = "SELECT codigo, nombre FROM ccpetprogramas";
            $request = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
            return $request;
        }
        public function selectSectores(){
            $sql = "SELECT codigo, nombre FROM ccpetsectores";
            $request = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
            return $request;
        }
        public function selectProgramatico(){
            $sql = "SELECT DISTINCT p.codigo_indicador as codigo,p.indicador_producto as nombre,
            p.cod_producto,p.producto
            FROM ccpproyectospresupuesto_productos pp
            INNER JOIN ccpetproductos p ON pp.indicador = p.codigo_indicador
            WHERE pp.estado = 'S'";
            $request = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
            return $request;
        }
        public function selectBpim(){
            $sql = "SELECT DISTINCT codigo,nombre FROM ccpproyectospresupuesto WHERE estado = 'S'";
            $request = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
            return $request;
        }
        public function selectSecciones(){
            $sql = "SELECT * FROM pptoseccion_presupuestal WHERE estado = 'S'";
            $request = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
            return $request;
        }
        public function selectVigencias(){
            $sql = "SELECT * FROM ccpet_vigenciadelgasto WHERE version = (SELECT MAX(version) FROM ccpet_vigenciadelgasto)";
            $request = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
            return $request;
        }
        public function selectFuentes(){
            $sql="SELECT codigo_fuente as codigo, nombre
            FROM ccpet_fuentes_cuipo
            WHERE version = '1' ORDER BY codigo_fuente";
            $request = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
            return $request;
        }
        public function selectCuentas(){
            $sql = "SELECT id,codigo, nombre, tipo, nivel
            FROM cuentasccpet WHERE tipo ='C' ORDER BY id ASC";
            $request = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
            return $request;
        }
        public function selectOrden(){
            $sql="SELECT orden FROM ccpet_parametros";
            $request = mysqli_query($this->linkbd,$sql)->fetch_assoc();
            if(!empty($request)){
                $request = $request['orden'];
            }else{
                $request = 1;
            }
            return $request;
        }
    }
?>
