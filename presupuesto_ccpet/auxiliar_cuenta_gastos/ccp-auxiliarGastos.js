const URL ='presupuesto_ccpet/auxiliar_cuenta_gastos/ccp-auxiliarGastos.php';

var app = new Vue({
    el:"#myapp",
    data() {
        return {
            isLoading:false,
            isModal:false,
            isModalFuente:false,
            isModalBpim:false,
            isModalProgramatico:false,
            isModalPrograma:false,
            isModalSector:false,
            txtFechaInicial:new Date(new Date().getFullYear(),0,1).toISOString().split("T")[0],
            txtFechaFinal:new Date().toISOString().split("T")[0],
            txtSearch:"",
            txtSearchFuente:"",
            txtSearchBpim:"",
            txtSearchProgramatico:"",
            txtSearchProgramas:"",
            txtSearchSectores:"",
            txtResultados:0,
            txtResultadosFuentes:0,
            txtResultadosBpim:0,
            txtResultadosProgramatico:0,
            txtResultadosProgramas:0,
            txtResultadosSectores:0,
            txtBpim:"",
            txtProgramatico:"",
            selectSecciones:0,
            selectVigencias:0,
            selectPago:0,
            selectOrden:1,
            arrSecciones:[],
            arrVigencias:[],
            arrCuentas:[],
            arrFuentes:[],
            arrBpim:[],
            arrProgramas:[],
            arrSectores:[],
            arrProgramatico:[],
            arrCuentasCopy:[],
            arrFuentesCopy:[],
            arrBpimCopy:[],
            arrProgramaticoCopy:[],
            arrProgramasCopy:[],
            arrSectoresCopy:[],
            objCuenta:{codigo:"",nombre:""},
            objFuente:{codigo:"",nombre:""},
            objBpim:{codigo:"",nombre:""},
            objProgramatico:{codigo:"",nombre:""},
            objSector:{codigo:"",nombre:""},
            objPrograma:{codigo:"",nombre:""},
        }
    },
    mounted() {
        this.getData();
    },
    methods: {
        getData: async function(){
            const formData = new FormData();
            formData.append("action","get");
            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();
            this.arrSecciones = objData.secciones;
            this.arrVigencias = objData.vigencias;
            this.arrCuentas = objData.cuentas;
            this.arrFuentes = objData.fuentes;
            this.arrBpim = objData.bpim;
            this.arrProgramatico = objData.programatico;
            this.arrProgramas = objData.programas;
            this.arrSectores = objData.sectores;
            this.arrCuentasCopy = objData.cuentas;
            this.arrFuentesCopy = objData.fuentes;
            this.arrBpimCopy = objData.bpim;
            this.arrProgramaticoCopy = objData.programatico;
            this.arrProgramasCopy = objData.programas;
            this.arrSectoresCopy = objData.sectores;
            this.txtResultados = this.arrCuentas.length;
            this.txtResultadosFuentes = this.arrFuentes.length;
            this.txtResultadosBpim = this.arrBpim.length;
            this.txtResultadosProgramatico = this.arrProgramatico.length;
            this.txtResultadosProgramas = this.arrProgramas.length;
            this.txtResultadosSectores = this.arrSectores.length;
            this.selectOrden = objData.orden;
            this.genDataByGet();
        },
        search:function(type=""){
            let search = "";
            if(type == "modal")search = this.txtSearch.toLowerCase();
            if(type == "modal_fuente")search = this.txtSearchFuente.toLowerCase();
            if(type == "modal_bpim")search = this.txtSearchBpim.toLowerCase();
            if(type == "modal_programatico")search = this.txtSearchProgramatico.toLowerCase();
            if(type == "modal_programa")search = this.txtSearchProgramas.toLowerCase();
            if(type == "modal_sector")search = this.txtSearchSectores.toLowerCase();
            if(type == "cod_cuenta") search = this.objCuenta.codigo;
            if(type == "cod_fuente") search = this.objFuente.codigo;
            if(type == "cod_bpim") search = this.objBpim.codigo;
            if(type == "cod_programatico") search = this.objProgramatico.codigo;
            if(type == "cod_sector") search = this.objSector.codigo;
            if(type == "cod_programa") search = this.objPrograma.codigo;

            if(type=="modal"){
                this.arrCuentasCopy = [...this.arrCuentas.filter(e=>e.codigo.toLowerCase().includes(search)
                || e.nombre.toLowerCase().includes(search))];
                this.txtResultados = this.arrCuentasCopy.length;
            }else if(type == "modal_fuente"){
                this.arrFuentesCopy = [...this.arrFuentes.filter(e=>e.codigo.toLowerCase().includes(search)
                    || e.nombre.toLowerCase().includes(search))];
                this.txtResultadosFuentes = this.arrFuentesCopy.length;
            }else if(type == "modal_bpim"){
                this.arrBpimCopy = [...this.arrBpim.filter(e=>e.codigo.toLowerCase().includes(search)
                    || e.nombre.toLowerCase().includes(search))];
                this.txtResultadosBpim = this.arrBpimCopy.length;
            }else if(type == "modal_sector"){
                this.arrSectoresCopy = [...this.arrSectores.filter(e=>e.codigo.toLowerCase().includes(search)
                    || e.nombre.toLowerCase().includes(search))];
                this.txtResultadosSectores = this.arrSectoresCopy.length;
            }else if(type == "modal_programa"){
                this.arrProgramasCopy = [...this.arrProgramas.filter(e=>e.codigo.toLowerCase().includes(search)
                    || e.nombre.toLowerCase().includes(search))];
                this.txtResultadosProgramas = this.arrProgramasCopy.length;
            }else if(type == "modal_programatico"){
                this.arrProgramaticoCopy = [...this.arrProgramatico.filter(e=>e.codigo.toLowerCase().includes(search)
                    || e.nombre.toLowerCase().includes(search) || e.cod_producto.toLowerCase().includes(search) || e.producto.toLowerCase().includes(search))];
                this.txtResultadosProgramatico = this.arrProgramaticoCopy.length;
            }else if(type == "cod_cuenta"){
                this.objCuenta = {};
                this.objCuenta = {...this.arrCuentas.filter(e=>e.codigo==search)[0]};
            }else if(type == "cod_fuente"){
                this.objFuente = {};
                this.objFuente = {...this.arrFuentes.filter(e=>e.codigo==search)[0]};
            }else if(type == "cod_bpim"){
                this.objBpim = {};
                this.objBpim = {...this.arrBpim.filter(e=>e.codigo==search)[0]};
            }else if(type == "cod_programatico"){
                this.objProgramatico = {};
                this.objProgramatico = {...this.arrProgramatico.filter(e=>e.codigo==search)[0]};
                this.changeProgramatico();
            }else if(type == "cod_sector"){
                this.objSector = {};
                this.objSector = {...this.arrSectores.filter(e=>e.codigo==search)[0]};
                this.changeSector();
            }else if(type == "cod_programa"){
                this.objPrograma = {};
                this.objPrograma = {...this.arrProgramas.filter(e=>e.codigo==search)[0]};
                this.changePrograma();
            }
        },
        genDataByGet: async function(){
            let fecha_inicial = new URLSearchParams(window.location.search).get('fechaIni');
            let fecha_final = new URLSearchParams(window.location.search).get('fechaFin');;
            let cuenta = new URLSearchParams(window.location.search).get('cuenta');
            let fuente = new URLSearchParams(window.location.search).get('fuente');
            let vigencia = new URLSearchParams(window.location.search).get('vig');
            let seccion = new URLSearchParams(window.location.search).get('sec');
            let pago = new URLSearchParams(window.location.search).get('medio');
            if(cuenta != null){
                let arrFechaInicial = fecha_inicial.split("/");
                let arrFechaFinal = fecha_final.split("/");
                fecha_inicial = arrFechaInicial[2]+"-"+arrFechaInicial[1]+"-"+arrFechaInicial[0];
                fecha_final = arrFechaFinal[2]+"-"+arrFechaFinal[1]+"-"+arrFechaFinal[0];
                this.objCuenta = {...this.arrCuentas.filter(e=>e.codigo==cuenta)[0]};
                this.objFuente = {...this.arrFuentes.filter(e=>e.codigo==fuente)[0]};
                if(this.objCuenta.codigo == undefined){
                    let arrCuenta = cuenta.split("-");
                    this.objBpim = {...this.arrBpim.filter(e=>e.codigo==arrCuenta[1])[0]};
                    this.objProgramatico = {...this.arrProgramatico.filter(e=>e.codigo==arrCuenta[0])[0]};
                }
                let programatico = "";
                if(this.objSector.codigo && this.objSector.codigo != ""){
                    programatico = this.objSector.codigo;
                }
                if(this.objPrograma.codigo && this.objPrograma.codigo != ""){
                    programatico = this.objPrograma.codigo;
                }
                if(this.objProgramatico.codigo){
                    programatico = this.objProgramatico.codigo
                    let sector = this.objProgramatico.codigo.substring(0,2);
                    let programa = this.objProgramatico.codigo.substring(0,4);
                    this.objSector = {...this.arrSectores.filter(e=>e.codigo==sector)[0]};
                    this.objPrograma = {...this.arrProgramas.filter(e=>e.codigo==programa)[0]};
                }
                this.txtFechaInicial = fecha_inicial;
                this.txtFechaFinal = fecha_final;
                this.selectVigencias = vigencia;
                this.selectSecciones = seccion;
                this.selectPago = pago;
                const formData = new FormData();
                formData.append("action","gen");
                formData.append("fecha_inicial",this.txtFechaInicial);
                formData.append("fecha_final",this.txtFechaFinal);
                formData.append("cuenta",this.objCuenta.codigo ? this.objCuenta.codigo : "");
                formData.append("fuente",this.objFuente.codigo ? this.objFuente.codigo : "");
                formData.append("vigencia",this.selectVigencias);
                formData.append("seccion",this.selectSecciones);
                formData.append("pago",this.selectPago);
                formData.append("bpim",this.objBpim.codigo ? this.objBpim.codigo : "");
                formData.append("programatico",programatico);
                this.isLoading = true;
                const response = await fetch(URL,{method:"POST",body:formData});
                const objData = await response.json();
                if(objData.status){
                    this.$refs.tableData.innerHTML = objData.html;
                }
                this.isLoading = false;
            }
        },
        genData: async function(){
            let programatico = "";
            if(this.objSector.codigo && this.objSector.codigo != ""){
                programatico = this.objSector.codigo;
            }
            if(this.objPrograma.codigo && this.objPrograma.codigo != ""){
                programatico = this.objPrograma.codigo;
            }
            if(this.objProgramatico.codigo && this.objPrograma.codigo != ""){
                programatico = this.objProgramatico.codigo
            }
            const formData = new FormData();
            formData.append("action","gen");
            formData.append("fecha_inicial",this.txtFechaInicial);
            formData.append("fecha_final",this.txtFechaFinal);
            formData.append("cuenta",this.objCuenta.codigo ? this.objCuenta.codigo : "");
            formData.append("fuente",this.objFuente.codigo ? this.objFuente.codigo : "");
            formData.append("vigencia",this.selectVigencias);
            formData.append("seccion",this.selectSecciones);
            formData.append("pago",this.selectPago);
            formData.append("bpim",this.objBpim.codigo ? this.objBpim.codigo : "");
            formData.append("programatico",programatico);
            this.isLoading = true;
            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();
            if(objData.status){
                this.$refs.tableData.innerHTML = objData.html;
            }else{
                Swal.fire("Error",objData.msg,"error");
            }
            this.isLoading = false;
        },
        changeSector:function(){
            if(this.objSector.codigo){
                this.objPrograma = {};
                this.objBpim = {};
                this.objProgramatico = {};
                this.genData();
            }
        },
        changePrograma:function(){
            if(this.objPrograma.codigo){
                let sector = this.objPrograma.codigo.substring(0,2);
                this.objSector = {...this.arrSectores.filter(e=>e.codigo==sector)[0]};
                this.objBpim = {};
                this.objProgramatico = {};
                this.genData();
            }
        },
        changeProgramatico:function(){
            if(this.objProgramatico.codigo){
                let sector = this.objProgramatico.codigo.substring(0,2);
                let programa = this.objProgramatico.codigo.substring(0,4);
                this.objSector = {...this.arrSectores.filter(e=>e.codigo==sector)[0]};
                this.objPrograma = {...this.arrProgramas.filter(e=>e.codigo==programa)[0]};
                this.objBpim = {};
                this.genData();
            }
        },
        selectItem:function(type="",{...item}){
            if(type == "modal"){
                this.objCuenta = item;
                this.isModal = false;
            }else if(type == "modal_fuente"){
                this.objFuente = item;
                this.isModalFuente = false;
            }else if(type == "modal_bpim"){
                this.objBpim = item;
                this.isModalBpim = false;
            }else if(type == "modal_programatico"){
                this.objProgramatico = item;
                this.isModalProgramatico = false;
                this.changeProgramatico();
            }else if(type == "modal_sector"){
                this.objSector = item;
                this.isModalSector = false;
                this.changeSector();
            }else if(type == "modal_programa"){
                this.objPrograma = item;
                this.isModalPrograma = false;
                this.changePrograma();
            }
        },
        printExcel:function(){
            if(this.objCuenta.codigo ==""){
                Swal.fire("Error","Todos los campos con (*) son obligatorios","error");
                return false;
            }
            let fuente = this.objFuente.codigo ? this.objFuente.codigo : "";
            let bpim = this.objBpim.codigo ? this.objBpim.codigo : "";
            let cuenta = this.objCuenta.codigo ? this.objCuenta.codigo : "";
            let programatico = "";
            let nombreProgramatico ="";
            let nombreBpim = this.objBpim.codigo ? this.objBpim.nombre : "";
            if(this.objSector.codigo && this.objSector.codigo != ""){
                programatico = this.objSector.codigo;
                nombreProgramatico = this.objSector.nombre;
            }
            if(this.objPrograma.codigo && this.objPrograma.codigo != ""){
                programatico = this.objPrograma.codigo;
                nombreProgramatico = this.objPrograma.nombre;
            }
            if(this.objProgramatico.codigo && this.objPrograma.codigo != ""){
                programatico = this.objProgramatico.codigo;
                nombreProgramatico = this.objProgramatico.nombre;
            }
            window.open("ccp-auxiliarGastosCuentaExcel.php?fecha_inicial="+this.txtFechaInicial
            +"&fecha_final="+this.txtFechaFinal+"&cuenta="+cuenta+"&fuente="+fuente
            +"&vigencia="+this.selectVigencias+"&seccion="+this.selectSecciones+"&pago="+this.selectPago
            +"&bpim="+bpim+"&programatico="+programatico+"&nombre_cuenta="+this.objCuenta.nombre+"&nombre_fuente="+this.objFuente.nombre
            +"&nombre_bpim="+nombreBpim+"&nombre_programatico="+nombreProgramatico
            ,"_blank");
        },
        formatNumero: function(valor){
            return new Intl.NumberFormat("en-US", {style: "currency", currency: "USD"}).format(valor);
        },
    },
    computed:{

    }
})
