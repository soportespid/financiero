<?php
    //$URL_BBDD = true;
	require_once '../../comun.inc';
    require '../../funciones.inc';
    //require_once '../../../vue/presupuesto_ccp/funcionesccp.inc.php';
    session_start();

	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");


    $out = array('error' => false);

    $action = "show";

    if(isset($_GET['action'])){
        $action = $_GET['action'];
    }
    
    if($action == 'buscarAuxiliar'){

        /* $out['errorS'] = var_dump('envuarl'); */
        //echo "shola";
        $detalles_Ini = array();
        $detalles_IniInv = array();
        $detalles_ad = array();
        $detalles_red = array();
        $detalles_cred = array();
        $detalles_contraCred = array();
        $detalles_cdp = array();
        $detalles_rp = array();
        $detalles_cxp = array();
        $detalles_cxpNom = array();
        $detalles_egreso = array();
        $detalles_egresoNom = array();

        $seccion_presupuestal = $_POST['selectUnidadEjecutora'];
        $medio_pago = $_POST['selectMedioPago'];
        $vigencia_gasto = $_POST['selectVigenciaGasto'];
        $rubro = $_POST['rubro'];
        $bpim = $_POST['codProyecto'];
        $programatico = $_POST['programatico'];
        $cpc = $_POST['codigoCpc'];
        $fuente = $_POST['fuente'];


        $critFecha = '';
        $critNumAcuerdo = '';

        if($_POST['fechaInicial'] != '' && $_POST['fechaFinal'] != ''){

            $critFecha = " AND fecha BETWEEN '".$_POST['fechaInicial']."' AND '".$_POST['fechaFinal']."'";
            $critFechaEjecu = " AND TB1.fecha BETWEEN '".$_POST['fechaInicial']."' AND '".$_POST['fechaFinal']."'";

        }

        //Adicion funcionamiento o deuda

        $sqlrIni = "SELECT SUM(TB1.valor),vigencia,fuente FROM ccpetinicialgastosfun AS TB1 WHERE TB1.seccion_presupuestal = '$seccion_presupuestal' AND TB1.medio_pago = '$medio_pago' AND TB1.vigencia_gasto = '$vigencia_gasto' AND TB1.cuenta = '$rubro' AND TB1.fuente like '%$fuente%' GROUP BY TB1.seccion_presupuestal, TB1.medio_pago, TB1.vigencia_gasto, TB1.fuente";
        $resIni = mysqli_query($linkbd, $sqlrIni);
        while($rowIni = mysqli_fetch_row($resIni)){

            $detallesIni = array();

            array_push($detallesIni, $seccion_presupuestal);
            array_push($detallesIni, $medio_pago);
            array_push($detallesIni, $vigencia_gasto);
            array_push($detallesIni, '');
            array_push($detallesIni, '');
            array_push($detallesIni, $rubro);
            array_push($detallesIni, $rowIni[2]);
            array_push($detallesIni, '');
            array_push($detallesIni, 1);
            array_push($detallesIni, $rowIni[1].'-01-01');
            array_push($detallesIni, 'Presupuesto inicial.');
            array_push($detallesIni, '201');
            array_push($detallesIni, $rowIni[0]);
            array_push($detallesIni, 0);
            array_push($detallesIni, $rowIni[0]);

            array_push($detalles_Ini, $detallesIni);
        }

        //Inicial proyectos
        $sqlrIniInv = "SELECT SUM(TB2.valorcsf) + SUM(TB2.valorssf), TB2.indicador_producto, TB1.vigencia, TB2.id_fuente FROM ccpproyectospresupuesto AS TB1, ccpproyectospresupuesto_presupuesto AS TB2 WHERE TB1.codigo = '$bpim' AND TB2.indicador_producto LIKE '%$programatico%' AND TB1.idunidadej = '$seccion_presupuestal' AND TB2.medio_pago = '$medio_pago' AND TB2.vigencia_gasto = '$vigencia_gasto' AND TB2.id_fuente like '%$fuente%' GROUP BY TB1.idunidadej, TB2.medio_pago, TB2.vigencia_gasto, TB2.id_fuente, TB2.indicador_producto";

        $resIniInv = mysqli_query($linkbd, $sqlrIniInv);
        while($rowIniInv = mysqli_fetch_row($resIniInv)){

            $detallesIniInv = array();

            array_push($detallesIniInv, $seccion_presupuestal);
            array_push($detallesIniInv, $medio_pago);
            array_push($detallesIniInv, $vigencia_gasto);
            array_push($detallesIniInv, $bpim);
            array_push($detallesIniInv, $rowIniInv[1]);
            array_push($detallesIniInv, '');
            array_push($detallesIniInv, $rowIniInv[3]);
            array_push($detallesIniInv, '');
            array_push($detallesIniInv, 1);
            array_push($detallesIniInv, $rowIniInv[2].'-01-01');
            array_push($detallesIniInv, 'Presupuesto inicial inversion.');
            array_push($detallesIniInv, '201');
            array_push($detallesIniInv, $rowIniInv[0]);
            array_push($detallesIniInv, 0);
            array_push($detallesIniInv, $rowIniInv[0]);

            array_push($detalles_IniInv, $detallesIniInv);
        }

        //Adiciones
        $sqlrAd = "SELECT SUM(valor), cuenta, bpim, programatico, fuente, fecha,id_acuerdo FROM ccpet_adiciones WHERE seccion_presupuestal = '$seccion_presupuestal' AND medio_pago = '$medio_pago' AND codigo_vigenciag = '$vigencia_gasto' AND cuenta LIKE '%$rubro%' AND bpim LIKE '%$bpim%' AND programatico LIKE '%$programatico%' AND fuente LIKE '%$fuente%' $critFecha GROUP BY seccion_presupuestal, medio_pago, codigo_vigenciag, cuenta, bpim, programatico, fuente";

        $resAd = mysqli_query($linkbd, $sqlrAd);
        while($rowAd = mysqli_fetch_row($resAd)){

            $detallesAd = array();

            $sqlrAcuerdo = "SELECT consecutivo, numero_acuerdo FROM ccpetacuerdos WHERE id_acuerdo = $rowAd[6]";
            $resAcuerdo = mysqli_query($linkbd, $sqlrAcuerdo);
            $rowAcuerdo = mysqli_fetch_row($resAcuerdo);

            array_push($detallesAd, $seccion_presupuestal);
            array_push($detallesAd, $medio_pago);
            array_push($detallesAd, $vigencia_gasto);
            array_push($detallesAd, $rowAd[2]);//bpim
            array_push($detallesAd, $rowAd[3]);//programatico
            array_push($detallesAd, $rowAd[1]);//cuenta
            array_push($detallesAd, $rowAd[4]);//fuente
            array_push($detallesAd, '');//cpc
            array_push($detallesAd, $rowAcuerdo[0]);//num. comprobante
            array_push($detallesAd, $rowAd[5]);//fecha
            array_push($detallesAd, $rowAcuerdo[1]);//concepto
            array_push($detallesAd, '201');
            array_push($detallesAd, $rowAd[0]);//valor entrada
            array_push($detallesAd, 0);//valor salida
            array_push($detallesAd, $rowAd[0]);//saldo

            array_push($detalles_ad, $detallesAd);
        }

        //Reducciones
        $sqlrRed = "SELECT SUM(valor), cuenta, bpim, programatico, fuente, fecha,id_acuerdo FROM ccpet_reducciones WHERE seccion_presupuestal = '$seccion_presupuestal' AND medio_pago = '$medio_pago' AND codigo_vigenciag = '$vigencia_gasto' AND cuenta LIKE '%$rubro%' AND bpim LIKE '%$bpim%' AND programatico LIKE '%$programatico%' AND fuente LIKE '%$fuente%' $critFecha GROUP BY seccion_presupuestal, medio_pago, codigo_vigenciag, cuenta, bpim, programatico, fuente";

        $resRed = mysqli_query($linkbd, $sqlrRed);
        while($rowRed = mysqli_fetch_row($resRed)){

            $detallesRed = array();
            $entrada = 0;
            $salida = 0;
            $saldo = 0;

            $sqlrAcuerdo = "SELECT consecutivo, numero_acuerdo FROM ccpetacuerdos WHERE id_acuerdo = $rowRed[6]";
            $resAcuerdo = mysqli_query($linkbd, $sqlrAcuerdo);
            $rowAcuerdo = mysqli_fetch_row($resAcuerdo);

            $entrada =  0;
            $salida =  $rowRed[0];
            $saldo = $entrada - $salida;

            array_push($detallesRed, $seccion_presupuestal);
            array_push($detallesRed, $medio_pago);
            array_push($detallesRed, $vigencia_gasto);
            array_push($detallesRed, $rowRed[2]);//bpim
            array_push($detallesRed, $rowRed[3]);//programatico
            array_push($detallesRed, $rowRed[1]);//cuenta
            array_push($detallesRed, $rowRed[4]);//fuente
            array_push($detallesRed, '');//cpc
            array_push($detallesRed, $rowAcuerdo[0]);//num. comprobante
            array_push($detallesRed, $rowRed[5]);//fecha
            array_push($detallesRed, $rowAcuerdo[1]);//concepto
            array_push($detallesRed, '201');
            array_push($detallesRed, $entrada);//valor entrada
            array_push($detallesRed, $salida);//valor salida
            array_push($detallesRed, $saldo);//saldo

            array_push($detalles_red, $detallesRed);
        }

        //Traslado presupuestal - Credito 
        $sqlrCred = "SELECT SUM(valor), cuenta, bpim, programatico, fuente, fecha,id_acuerdo FROM ccpet_traslados WHERE tipo = 'C' AND seccion_presupuestal = '$seccion_presupuestal' AND medio_pago = '$medio_pago' AND codigo_vigenciag = '$vigencia_gasto' AND cuenta LIKE '%$rubro%' AND bpim LIKE '%$bpim%' AND programatico LIKE '%$programatico%' AND fuente LIKE '%$fuente%' $critFecha GROUP BY seccion_presupuestal, medio_pago, codigo_vigenciag, cuenta, bpim, programatico, fuente";

        $resCred = mysqli_query($linkbd, $sqlrCred);
        while($rowCred = mysqli_fetch_row($resCred)){

            $detallesCred = array();
            $entrada = 0;
            $salida = 0;
            $saldo = 0;

            $sqlrAcuerdo = "SELECT consecutivo, numero_acuerdo FROM ccpetacuerdos WHERE id_acuerdo = $rowCred[6]";
            $resAcuerdo = mysqli_query($linkbd, $sqlrAcuerdo);
            $rowAcuerdo = mysqli_fetch_row($resAcuerdo);

            $entrada =  $rowCred[0];
            $salida =  0;
            $saldo = $entrada - $salida;

            array_push($detallesCred, $seccion_presupuestal);
            array_push($detallesCred, $medio_pago);
            array_push($detallesCred, $vigencia_gasto);
            array_push($detallesCred, $rowCred[2]);//bpim
            array_push($detallesCred, $rowCred[3]);//programatico
            array_push($detallesCred, $rowCred[1]);//cuenta
            array_push($detallesCred, $rowCred[4]);//fuente
            array_push($detallesCred, '');//cpc
            array_push($detallesCred, $rowAcuerdo[0]);//num. comprobante
            array_push($detallesCred, $rowCred[5]);//fecha
            array_push($detallesCred, $rowAcuerdo[1]);//concepto
            array_push($detallesCred, '201');
            array_push($detallesCred, $entrada);//valor entrada
            array_push($detallesCred, $salida);//valor salida
            array_push($detallesCred, $saldo);//saldo

            array_push($detalles_cred, $detallesCred);
        }

        //Traslado presupuestal - ContraCredito
        $sqlrContraCred = "SELECT SUM(valor), cuenta, bpim, programatico, fuente, fecha,id_acuerdo FROM ccpet_traslados WHERE tipo = 'R' AND seccion_presupuestal = '$seccion_presupuestal' AND medio_pago = '$medio_pago' AND codigo_vigenciag = '$vigencia_gasto' AND cuenta LIKE '%$rubro%' AND bpim LIKE '%$bpim%' AND programatico LIKE '%$programatico%' AND fuente LIKE '%$fuente%' $critFecha GROUP BY seccion_presupuestal, medio_pago, codigo_vigenciag, cuenta, bpim, programatico, fuente";

        $resContraCred = mysqli_query($linkbd, $sqlrContraCred);
        while($rowContraCred = mysqli_fetch_row($resContraCred)){

            $detallesContraCred = array();
            $entrada = 0;
            $salida = 0;
            $saldo = 0;

            $sqlrAcuerdo = "SELECT consecutivo, numero_acuerdo FROM ccpetacuerdos WHERE id_acuerdo = $rowContraCred[6]";
            $resAcuerdo = mysqli_query($linkbd, $sqlrAcuerdo);
            $rowAcuerdo = mysqli_fetch_row($resAcuerdo);

            $entrada =  0;
            $salida =  $rowContraCred[0];
            $saldo = $entrada - $salida;

            array_push($detallesContraCred, $seccion_presupuestal);
            array_push($detallesContraCred, $medio_pago);
            array_push($detallesContraCred, $vigencia_gasto);
            array_push($detallesContraCred, $rowContraCred[2]);//bpim
            array_push($detallesContraCred, $rowContraCred[3]);//programatico
            array_push($detallesContraCred, $rowContraCred[1]);//cuenta
            array_push($detallesContraCred, $rowContraCred[4]);//fuente
            array_push($detallesContraCred, '');//cpc
            array_push($detallesContraCred, $rowAcuerdo[0]);//num. comprobante
            array_push($detallesContraCred, $rowContraCred[5]);//fecha
            array_push($detallesContraCred, $rowAcuerdo[1]);//concepto
            array_push($detallesContraCred, '201');
            array_push($detallesContraCred, $entrada);//valor entrada
            array_push($detallesContraCred, $salida);//valor salida
            array_push($detallesContraCred, $saldo);//saldo

            array_push($detalles_contraCred, $detallesContraCred);
        }

        //Cdp - Certificado de Disponibilidad Presupuestal
        $sqlrCdp = "SELECT SUM(TB2.valor), TB2.cuenta, TB2.bpim, TB2.indicador_producto, TB2.fuente, TB1.fecha, TB1.objeto, TB2.productoservicio, TB1.consvigencia, TB1.vigencia FROM ccpetcdp AS TB1, ccpetcdp_detalle AS TB2 WHERE TB1.vigencia = TB2.vigencia AND TB1.consvigencia = TB2.consvigencia AND TB2.seccion_presupuestal = '$seccion_presupuestal' AND TB2.medio_pago = '$medio_pago' AND TB2.codigo_vigenciag = '$vigencia_gasto' AND TB2.cuenta LIKE '%$rubro%' AND TB2.bpim LIKE '%$bpim%' AND TB2.indicador_producto LIKE '%$programatico%' AND TB2.fuente LIKE '%$fuente%' AND TB1.tipo_mov='201' $critFechaEjecu GROUP BY TB2.seccion_presupuestal, TB2.medio_pago, TB2.codigo_vigenciag, TB2.cuenta, TB2.bpim, TB2.indicador_producto, TB2.fuente, TB2.productoservicio, TB1.consvigencia, TB1.vigencia";

        $resCdp = mysqli_query($linkbd, $sqlrCdp);
        while($rowCdp = mysqli_fetch_row($resCdp)){

            $detallesCdp = array();

            $entrada = 0;
            $salida = 0;
            $saldo = 0;
            $tipo_mov = '';

            $entrada =  $rowCdp[0];
            $tipo_mov = '201';

            $sqlrCdpRev = "SELECT SUM(TB2.valor) FROM ccpetcdp AS TB1, ccpetcdp_detalle AS TB2 WHERE TB1.vigencia = '$rowCdp[9]' AND TB1.consvigencia = '$rowCdp[8]' AND TB1.vigencia = TB2.vigencia AND TB1.consvigencia = TB2.consvigencia AND TB2.seccion_presupuestal = '$seccion_presupuestal' AND TB2.medio_pago = '$medio_pago' AND TB2.codigo_vigenciag = '$vigencia_gasto' AND TB2.cuenta = '$rowCdp[1]' AND TB2.bpim = '$rowCdp[2]' AND TB2.indicador_producto = '$rowCdp[3]' AND TB2.fuente = '$rowCdp[4]' AND TB1.tipo_mov LIKE '4%' GROUP BY TB1.vigencia, TB1.consvigencia";

            $resCdpRev = mysqli_query($linkbd, $sqlrCdpRev);
            $rowCdpRev = mysqli_fetch_row($resCdpRev);

            $salida = $rowCdpRev[0];
            if($rowCdpRev[0] > 0){
                $tipo_mov .= '-401';
            }else{
                $salida = 0;
            }

            $saldo = $entrada - $salida;

            array_push($detallesCdp, $seccion_presupuestal);
            array_push($detallesCdp, $medio_pago);
            array_push($detallesCdp, $vigencia_gasto);
            array_push($detallesCdp, $rowCdp[2]);//bpim
            array_push($detallesCdp, $rowCdp[3]);//programatico
            array_push($detallesCdp, $rowCdp[1]);//cuenta
            array_push($detallesCdp, $rowCdp[4]);//fuente
            array_push($detallesCdp, $rowCdp[7]);//cpc
            array_push($detallesCdp, $rowCdp[8]);//num. comprobante
            array_push($detallesCdp, $rowCdp[5]);//fecha
            array_push($detallesCdp, $rowCdp[6]);//concepto
            array_push($detallesCdp, $tipo_mov);//tipo de movimiento
            array_push($detallesCdp, $entrada);//valor entrada
            array_push($detallesCdp, $salida);//valor salida
            array_push($detallesCdp, $saldo);//saldo

            array_push($detalles_cdp, $detallesCdp);

        }

        // rp - registro presupuestal
        $sqlrRp = "SELECT SUM(TB2.valor), TB2.cuenta, TB2.bpim, TB2.indicador_producto, TB2.fuente, TB1.fecha, TB1.detalle, TB2.productoservicio, TB1.consvigencia, TB1.vigencia FROM ccpetrp AS TB1, ccpetrp_detalle AS TB2 WHERE TB1.vigencia = TB2.vigencia AND TB1.consvigencia = TB2.consvigencia AND TB2.seccion_presupuestal = '$seccion_presupuestal' AND TB2.medio_pago = '$medio_pago' AND TB2.codigo_vigenciag = '$vigencia_gasto' AND TB2.cuenta LIKE '%$rubro%' AND TB2.bpim LIKE '%$bpim%' AND TB2.indicador_producto LIKE '%$programatico%' AND TB2.fuente LIKE '%$fuente%' AND TB1.tipo_mov='201' $critFechaEjecu GROUP BY TB2.seccion_presupuestal, TB2.medio_pago, TB2.codigo_vigenciag, TB2.cuenta, TB2.bpim, TB2.indicador_producto, TB2.fuente, TB2.productoservicio, TB1.consvigencia, TB1.vigencia";

        $resRp = mysqli_query($linkbd, $sqlrRp);
        while($rowRp = mysqli_fetch_row($resRp)){

            $detallesRp = array();

            $entrada = 0;
            $salida = 0;
            $saldo = 0;
            $tipo_mov = '';

            $entrada =  $rowRp[0];
            $tipo_mov = '201';

            $sqlrRpRev = "SELECT SUM(TB2.valor) FROM ccpetrp AS TB1, ccpetrp_detalle AS TB2 WHERE TB1.vigencia = '$rowRp[9]' AND TB1.consvigencia = '$rowRp[8]' AND TB1.vigencia = TB2.vigencia AND TB1.consvigencia = TB2.consvigencia AND TB2.seccion_presupuestal = '$seccion_presupuestal' AND TB2.medio_pago = '$medio_pago' AND TB2.codigo_vigenciag = '$vigencia_gasto' AND TB2.cuenta = '$rowRp[1]' AND TB2.bpim = '$rowRp[2]' AND TB2.indicador_producto = '$rowRp[3]' AND TB2.fuente = '$rowRp[4]' AND TB1.tipo_mov LIKE '4%' GROUP BY TB1.vigencia, TB1.consvigencia";

            $resRpRev = mysqli_query($linkbd, $sqlrRpRev);
            $rowRpRev = mysqli_fetch_row($resRpRev);

            $salida = $rowRpRev[0];
            if($rowRpRev[0] > 0){
                $tipo_mov .= '-401';
            }else{
                $salida = 0;
            }

            $saldo = $entrada - $salida;

            array_push($detallesRp, $seccion_presupuestal);
            array_push($detallesRp, $medio_pago);
            array_push($detallesRp, $vigencia_gasto);
            array_push($detallesRp, $rowRp[2]);//bpim
            array_push($detallesRp, $rowRp[3]);//programatico
            array_push($detallesRp, $rowRp[1]);//cuenta
            array_push($detallesRp, $rowRp[4]);//fuente
            array_push($detallesRp, $rowRp[7]);//cpc
            array_push($detallesRp, $rowRp[8]);//num. comprobante
            array_push($detallesRp, $rowRp[5]);//fecha
            array_push($detallesRp, $rowRp[6]);//concepto
            array_push($detallesRp, $tipo_mov);//tipo de movimiento
            array_push($detallesRp, $entrada);//valor entrada
            array_push($detallesRp, $salida);//valor salida
            array_push($detallesRp, $saldo);//saldo

            array_push($detalles_rp, $detallesRp);

        }

        //cxp - obligacion

        $sqlrCxp = "SELECT SUM(TB2.valor), TB2.cuentap, TB2.bpim, TB2.indicador_producto, TB2.fuente, TB1.fecha, TB1.conceptorden, TB2.productoservicio, TB1.id_orden FROM tesoordenpago AS TB1, tesoordenpago_det AS TB2 WHERE TB1.id_orden = TB2.id_orden AND TB2.seccion_presupuestal = '$seccion_presupuestal' AND TB2.medio_pago = '$medio_pago' AND TB2.codigo_vigenciag = '$vigencia_gasto' AND TB2.cuentap LIKE '%$rubro%' AND TB2.bpim LIKE '%$bpim%' AND TB2.indicador_producto LIKE '%$programatico%' AND TB2.fuente LIKE '%$fuente%' AND TB1.tipo_mov='201' $critFechaEjecu GROUP BY TB2.seccion_presupuestal, TB2.medio_pago, TB2.codigo_vigenciag, TB2.cuentap, TB2.bpim, TB2.indicador_producto, TB2.fuente, TB2.productoservicio, TB1.id_orden";

        $resCxp = mysqli_query($linkbd, $sqlrCxp);
        while($rowCxp = mysqli_fetch_row($resCxp)){

            $detallesCxp = array();

            $entrada = 0;
            $salida = 0;
            $saldo = 0;
            $tipo_mov = '';

            $entrada =  $rowCxp[0];
            $tipo_mov = '201';

            $sqlrCxpRev = "SELECT SUM(TB2.valor) FROM tesoordenpago AS TB1, tesoordenpago_det AS TB2 WHERE  TB1.id_orden = '$rowCxp[8]' AND TB1.vigencia = TB2.vigencia AND TB1.id_orden = TB2.id_orden AND TB2.seccion_presupuestal = '$seccion_presupuestal' AND TB2.medio_pago = '$medio_pago' AND TB2.codigo_vigenciag = '$vigencia_gasto' AND TB2.cuentap = '$rowCxp[1]' AND TB2.bpim = '$rowCxp[2]' AND TB2.indicador_producto = '$rowCxp[3]' AND TB2.fuente = '$rowCxp[4]' AND TB1.tipo_mov LIKE '4%' GROUP BY TB1.id_orden";

            $resCxpRev = mysqli_query($linkbd, $sqlrCxpRev);
            $rowCxpRev = mysqli_fetch_row($resCxpRev);

            $salida = $rowCxpRev[0];
            if($rowCxpRev[0] > 0){
                $tipo_mov .= '-401';
            }else{
                $salida = 0;
            }

            $saldo = $entrada - $salida;

            array_push($detallesCxp, $seccion_presupuestal);
            array_push($detallesCxp, $medio_pago);
            array_push($detallesCxp, $vigencia_gasto);
            array_push($detallesCxp, $rowCxp[2]);//bpim
            array_push($detallesCxp, $rowCxp[3]);//programatico
            array_push($detallesCxp, $rowCxp[1]);//cuenta
            array_push($detallesCxp, $rowCxp[4]);//fuente
            array_push($detallesCxp, $rowCxp[7]);//cpc
            array_push($detallesCxp, $rowCxp[8]);//num. comprobante
            array_push($detallesCxp, $rowCxp[5]);//fecha
            array_push($detallesCxp, $rowCxp[6]);//concepto
            array_push($detallesCxp, $tipo_mov);//tipo de movimiento
            array_push($detallesCxp, $entrada);//valor entrada
            array_push($detallesCxp, $salida);//valor salida
            array_push($detallesCxp, $saldo);//saldo

            array_push($detalles_cxp, $detallesCxp);

        }

        //Egreso

        $sqlrEgreso = "SELECT SUM(TB3.valor), TB3.cuentap, TB3.bpim, TB3.indicador_producto, TB3.fuente, TB1.fecha, TB1.concepto, TB3.productoservicio, TB1.id_egreso FROM tesoegresos AS TB1, tesoordenpago AS TB2, tesoordenpago_det AS TB3 WHERE TB1.id_orden = TB2.id_orden AND TB2.id_orden = TB3.id_orden AND TB3.seccion_presupuestal = '$seccion_presupuestal' AND TB3.medio_pago = '$medio_pago' AND TB3.codigo_vigenciag = '$vigencia_gasto' AND TB3.cuentap LIKE '%$rubro%' AND TB3.bpim LIKE '%$bpim%' AND TB3.indicador_producto LIKE '%$programatico%' AND TB3.fuente LIKE '%$fuente%' AND TB2.tipo_mov='201' $critFechaEjecu GROUP BY TB3.seccion_presupuestal, TB3.medio_pago, TB3.codigo_vigenciag, TB3.cuentap, TB3.bpim, TB3.indicador_producto, TB3.fuente, TB3.productoservicio, TB1.id_egreso";
        //echo $sqlrEgreso;
        $resEgreso = mysqli_query($linkbd, $sqlrEgreso);
        while($rowEgreso = mysqli_fetch_row($resEgreso)){

            $detallesEgreso = array();

            $entrada = 0;
            $salida = 0;
            $saldo = 0;
            $tipo_mov = '';

            $entrada =  $rowEgreso[0];
            $tipo_mov = '201';

            $sqlrEgresoRev = "SELECT SUM(TB3.valor) FROM tesoegresos AS TB1, tesoordenpago AS TB2, tesoordenpago_det AS TB3 WHERE  TB1.id_egreso = '$rowEgreso[8]' AND TB1.id_orden = TB2.id_orden AND TB2.id_orden = TB3.id_orden AND TB3.seccion_presupuestal = '$seccion_presupuestal' AND TB3.medio_pago = '$medio_pago' AND TB3.codigo_vigenciag = '$vigencia_gasto' AND TB3.cuentap = '$rowEgreso[1]' AND TB3.bpim = '$rowEgreso[2]' AND TB3.indicador_producto = '$rowEgreso[3]' AND TB3.fuente = '$rowEgreso[4]' AND TB1.tipo_mov LIKE '4%' GROUP BY TB1.id_egreso";

            $resEgresoRev = mysqli_query($linkbd, $sqlrEgresoRev);
            $rowEgresoRev = mysqli_fetch_row($resEgresoRev);

            $salida = $rowEgresoRev[0];
            if($rowEgresoRev[0] > 0){
                $tipo_mov .= '-401';
            }else{
                $salida = 0;
            }

            $saldo = $entrada - $salida;

            array_push($detallesEgreso, $seccion_presupuestal);
            array_push($detallesEgreso, $medio_pago);
            array_push($detallesEgreso, $vigencia_gasto);
            array_push($detallesEgreso, $rowEgreso[2]);//bpim
            array_push($detallesEgreso, $rowEgreso[3]);//programatico
            array_push($detallesEgreso, $rowEgreso[1]);//cuenta
            array_push($detallesEgreso, $rowEgreso[4]);//fuente
            array_push($detallesEgreso, $rowEgreso[7]);//cpc
            array_push($detallesEgreso, $rowEgreso[8]);//num. comprobante
            array_push($detallesEgreso, $rowEgreso[5]);//fecha
            array_push($detallesEgreso, $rowEgreso[6]);//concepto
            array_push($detallesEgreso, $tipo_mov);//tipo de movimiento
            array_push($detallesEgreso, $entrada);//valor entrada
            array_push($detallesEgreso, $salida);//valor salida
            array_push($detallesEgreso, $saldo);//saldo

            array_push($detalles_egreso, $detallesEgreso);

        }


        /* if($_POST['numActo'] != ''){
            $critNumAcuerdo = " AND consecutivo = ".$_POST['numActo']."";
        } */

        /* $sqlr = "SELECT * FROM ccpetacuerdos WHERE valortraslado > 0 $critFecha $critNumAcuerdo";
        $res = mysqli_query($linkbd, $sqlr);
        while($row = mysqli_fetch_row($res)){
            array_push($traslados, $row);
        } */
        $out['detallesIni'] = $detalles_Ini;
        $out['detallesIniInv'] = $detalles_IniInv;
        $out['detallesAd'] = $detalles_ad;
        $out['detallesRed'] = $detalles_red;
        $out['detallesCred'] = $detalles_cred;
        $out['detallesContraCred'] = $detalles_contraCred;
        $out['detalles_cdp'] = $detalles_cdp;
        $out['detalles_rp'] = $detalles_rp;
        $out['detalles_cxp'] = $detalles_cxp;
        $out['detalles_egreso'] = $detalles_egreso;
    }

   
    header("Content-type: application/json");
    echo json_encode($out);
    die();