import { createApp } from '../../Librerias/vue3/dist/vue.esm-browser.js'

const DIR_ORIGIN = window.location.origin + '/';
const DIR_PATHNAME = window.location.pathname.split("/", 2)[1] + '/';
const DIR_FULL = DIR_ORIGIN + '/' + DIR_PATHNAME;

const app = createApp({

    data() {
      return {

        loading: false,

        detallesIni: [],
        detallesIniInv: [],
        detallesAd: [],
        detallesRed: [],
        detallesCred: [],
        detallesContraCred: [],
        detallesCdp: [],
        detallesRp: [],
        detallesCxp: [],
        detallesCxpNom: [],
        detallesEgreso: [],
        detallesEgresoNom: [],

        tiposDeGasto: [],
        tipoGasto: '',
        
        unidadesejecutoras: [],
        selectUnidadEjecutora : '',
        optionsMediosPagos: [
			{ text: 'CSF', value: 'CSF' },
			{ text: 'SSF', value: 'SSF' }
		],
        selectMedioPago: 'CSF',
        vigenciasdelgasto: [],
        selectVigenciaGasto: '',
        fuente: '',
        nombreFuente: '',
        codProyecto: '',
        nombreProyecto : '',
        programatico: '',
        nombreProgramatico: '',

        rubro: '',
        nombreRubro: '',
        showModal_rubros: false,
        cuentasCcpet:[],
        searchCuenta: {keywordCuenta: ''},

        showModal_fuentes: false,
        fuentesCuipo: '',
        searchFuente : {keywordFuente: ''},

        codigoCpc: '',
        nombreCodigoCpc: '',
        cuentaConCpc: '',
        showModal_bienes_transportables: false,
        show_table_search: false,
        secciones: [],

        searchBienes: {keyword: ''},
        searchDivision: {keywordDivision: ''},
        searchGrupo: {keywordGrupo: ''},
        searchClase: {keywordClase: ''},
        searchSubClase: {keywordSubClase: ''},

        divisiones: [],
        grupos: [],
        clases: [],
        subClases: [],
        subClasesServicios: [],
		subClases_captura: [],

        division_p: '',
        division_p_nombre: '',
        grupo_p: '',
        grupo_p_nombre: '',
        clase_p: '',
        clase_p_nombre: '',
        subClase_p: '',
        subClase_p_nombre: '',
        seccion_p: '',
        seccion_p_nombre: '',

        mostrarDivision: false,
        mostrarGrupo: false,
        mostrarClase: false,
        mostrarSubClase: false,
        mostrarSubClaseProducto: false,

        cuentasSelectSubClase: [],
        searchGeneral: {keywordGeneral: ''},
        searchGeneral2: {keywordGeneral: ''},

        cuentaActual: '',

        showModal_servicios: false,
        seccionesServicios: [],

        gruposServicios: [],
        clasesServicios: [],
        subClasesServicios: [],
        divisionesServicios: [],

        divisionServicios_p: '',
        grupoServicios_p: '',
        claseServicios_p: '',
        subClaseServicios_p: '',
        subClaseServicios_p_nombre: '',
        seccionServicios_p: '',
        seccionServicios_p_nombre: '',

        mostrarGrupoServicios: false,
        mostrarClaseServicios: false,
        mostrarSubClaseServicios: false,
        mostrarDivisionServicios: false,

        searchDivisionServicios: {keywordDivisionServicios: ''},
        searchGrupoServicios: {keywordGrupoServicios: ''},
        searchClaseServicios: {keywordClaseServicios: ''},
        searchSubClaseServicios: {keywordSubClaseServicios: ''},

        showModal_proyectos: false,
        proyectos: [],
        searchProyecto : {keywordProyecto: ''},

        showModal_programatico: false,
        programaticos: '',

        sumaEntradaIni: 0,
        sumaSalidaIni: 0,
        sumaSaldoIni: 0,

        sumaEntradaIniInv: 0,
        sumaSalidaIniInv: 0,
        sumaSaldoIniInv: 0,

        sumaEntradaAd: 0,
        sumaSalidaAd: 0,
        sumaSaldoAd: 0,

        sumaEntradaRed: 0,
        sumaSalidaRed: 0,
        sumaSaldoRed: 0,

        sumaEntradaCred: 0,
        sumaSalidaCred: 0,
        sumaSaldoCred: 0,

        sumaEntradaContraCred: 0,
        sumaSalidaContraCred: 0,
        sumaSaldoContraCred: 0,

        sumaSaldoProgramacion: 0,
        sumaEntradaProgramacion: 0,
        sumaSalidaProgramacion: 0,

        sumaEntradaCdp: 0,
        sumaSalidaCdp: 0,
        sumaSaldoCdp: 0,

        sumaEntradaRp: 0,
        sumaSalidaRp: 0,
        sumaSaldoRp: 0,

        sumaEntradaCxp: 0,
        sumaSalidaCxp: 0,
        sumaSaldoCxp: 0,

        sumaEntradaEgreso: 0,
        sumaSalidaEgreso: 0,
        sumaSaldoEgreso: 0,

      }
    },

    mounted() {
        this.loading = false;
        this.cargarTiposDeGasto();
        this.seccionPresupuestal();
        this.vigenciasDelgasto();
    },

    computed: {
        existeInformacion(){
            const detIni = JSON.parse(JSON.stringify(this.detallesIni));
            const detIniInv = JSON.parse(JSON.stringify(this.detallesIniInv));
            const detAd = JSON.parse(JSON.stringify(this.detallesAd));
            const detRed = JSON.parse(JSON.stringify(this.detallesRed));
            const detCred = JSON.parse(JSON.stringify(this.detallesCred));
            return  (
                        (detIni.length > 0) ? true : 
                            (detIniInv.length > 0) ? true : 
                                (detAd.length > 0) ? true : 
                                    (detRed.length > 0) ? true : 
                                        (detCred.length > 0) ? true : false
                    )
          },
    },

    methods: {

        downloadExl() {

            let wb = XLSX.utils.table_to_book(document.getElementById('tableId')),
                wopts = {
                    bookType: 'xls',
                    bookSST: false,
                    type: 'binary'
                },
                wbout = XLSX.write(wb, wopts);

                saveAs(new Blob([this.s2ab(wbout)], {
                type: "application/octet-stream;charset=utf-8"
                            }), "Auxiliar gastos.xls");
        },

        s2ab(s) {
            if (typeof ArrayBuffer !== 'undefind') {
                var buf = new ArrayBuffer(s.length);
                var view = new Uint8Array(buf);
                for (var i = 0; i != s.length; ++i) view[i] = s.charCodeAt(i) & 0xFF;
                return buf;
            } else {
                var buf = new Array(s.length);
                for (var i = 0; i != s.length; ++i) buf[i] = s.charCodeAt(i) & 0xFF;
                return buf;
            }
        },

        toFormData(obj){
            var form_data = new FormData();
            for(var key in obj){
                form_data.append(key, obj[key]);
            }
            return form_data;
        },

        formatonumero: function(valor){
            return new Intl.NumberFormat("en-US", {style: "currency", currency: "USD"}).format(valor);
        },

        async cargarTiposDeGasto(){

            const URL = `${DIR_FULL}vue/presupuesto_ccp/ccp-cdpbasico.php?action=cargarTiposDeGasto`;

            await axios.post(URL)
            .then((response) => {
                this.tiposDeGasto = response.data.tiposDeGasto;

                if(response.data.tiposDeGasto.length > 0){
                    
                    this.tipoGasto = response.data.tiposDeGasto[0][0];
                }
            });
            this.cambiaTipoDeGasto();
        },

        reiniciarVariable(){
            this.detallesIni = [];
        },

        async buscarDocumentos(){

            let fechaInicial = document.getElementById('fc_1198971545').value;
            let fechaFinal = document.getElementById('fc_1198971546').value;

            if(this.rubro != '' || this.codProyecto != '' || (fechaInicial && fechaFinal)){

                if(fechaInicial && fechaFinal){
                    let arrFechaInicial = fechaInicial.split("/");
                    fechaInicial = `${arrFechaInicial[2]}-${arrFechaInicial[1]}-${arrFechaInicial[0]}`;
                    let arrFechaFinal = fechaFinal.split("/");
                    fechaFinal = `${arrFechaFinal[2]}-${arrFechaFinal[1]}-${arrFechaFinal[0]}`;
                }else{
                    fechaInicial = '';
                    fechaFinal = '';
                }

                let formData = new FormData();
                formData.append("fechaInicial", fechaInicial);
                formData.append("fechaFinal", fechaFinal);
                formData.append("rubro", this.rubro);
                formData.append("tipoGasto", this.tipoGasto);
                formData.append("selectUnidadEjecutora", this.selectUnidadEjecutora);
                formData.append("selectMedioPago", this.selectMedioPago);
                formData.append("selectVigenciaGasto", this.selectVigenciaGasto);
                formData.append("codProyecto", this.codProyecto);
                formData.append("programatico", this.programatico);
                formData.append("codigoCpc", this.codigoCpc);
                formData.append("fuente", this.fuente);

                const URL = `${DIR_FULL}presupuesto_ccpet/AuxiliarGastos/ccp-auxiliarGastos.php?action=buscarAuxiliar`;
                
                await axios.post(URL, formData)
                .then((response) => {
                    console.log(response.data);
                    this.detallesIni = response.data.detallesIni;
                    this.detallesIniInv = response.data.detallesIniInv;
                    this.detallesAd = response.data.detallesAd;
                    this.detallesRed = response.data.detallesRed;
                    this.detallesCred = response.data.detallesCred;
                    this.detallesContraCred = response.data.detallesContraCred;
                    this.detallesCdp = response.data.detalles_cdp;
                    this.detallesRp = response.data.detalles_rp;
                    this.detallesCxp = response.data.detalles_cxp;
                    this.detallesEgreso = response.data.detalles_egreso;

                    this.sumarInicial();
                    this.sumarInicialInv();
                    this.sumarAd();
                    this.sumarRed();
                    this.sumarCred();
                    this.sumarContraCred();
                    this.sumarTotalProgramacion();
                    this.sumarCdp();
                    this.sumarRp();
                    this.sumarCxp();
                    this.sumarEgreso();
                });

            }else{
                Swal.fire(
                    'Faltan parametros para la buscqueda.',
                    'Revisar que el campo de la cuenta, el proyecto o las fechas no este vacio.',
                    'warning'
                    ).then((result) => {
                        
                        if(result.isConfirmed || result.isDismissed){
                            
                        }
                   
                    });
            }
        },

        async sumarInicial(){
            this.sumaEntradaIni = 0;
            this.sumaSalidaIni = 0;
            this.sumaSaldoIni = 0;

            await this.detallesIni.forEach(element => {
                this.sumaEntradaIni += parseInt(element[12]);
                this.sumaSalidaIni += parseInt(element[13]);
                this.sumaSaldoIni += parseInt(element[14]);
            });

        },

        async sumarInicialInv(){
            this.sumaEntradaIniInv = 0;
            this.sumaSalidaIniInv = 0;
            this.sumaSaldoIniInv = 0;

            await this.detallesIniInv.forEach(element => {
                this.sumaEntradaIniInv += parseInt(element[12]);
                this.sumaSalidaIniInv += parseInt(element[13]);
                this.sumaSaldoIniInv += parseInt(element[14]);
            });

        },

        async sumarAd(){
            this.sumaEntradaAd = 0;
            this.sumaSalidaAd = 0;
            this.sumaSaldoAd = 0;

            await this.detallesAd.forEach(element => {
                this.sumaEntradaAd += parseInt(element[12]);
                this.sumaSalidaAd += parseInt(element[13]);
                this.sumaSaldoAd += parseInt(element[14]);
            });

        },

        async sumarRed(){
            this.sumaEntradaRed = 0;
            this.sumaSalidaRed = 0;
            this.sumaSaldoRed = 0;

            await this.detallesRed.forEach(element => {
                this.sumaEntradaRed += parseInt(element[12]);
                this.sumaSalidaRed += parseInt(element[13]);
                this.sumaSaldoRed += parseInt(element[14]);
            });

        },

        async sumarCred(){
            this.sumaEntradaCred = 0;
            this.sumaSalidaCred = 0;
            this.sumaSaldoCred = 0;

            await this.detallesCred.forEach(element => {
                this.sumaEntradaCred += parseInt(element[12]);
                this.sumaSalidaCred += parseInt(element[13]);
                this.sumaSaldoCred += parseInt(element[14]);
            });

        },

        async sumarContraCred(){
            this.sumaEntradaContraCred = 0;
            this.sumaSalidaContraCred = 0;
            this.sumaSaldoContraCred = 0;

            await this.detallesContraCred.forEach(element => {
                this.sumaEntradaContraCred += parseInt(element[12]);
                this.sumaSalidaContraCred += parseInt(element[13]);
                this.sumaSaldoContraCred += parseInt(element[14]);
            });

        },

        async sumarTotalProgramacion(){
            this.sumaSaldoProgramacion = 0;
            this.sumaEntradaProgramacion = 0;
            this.sumaSalidaProgramacion = 0;

            this.sumaEntradaProgramacion = parseInt(this.sumaEntradaIni) + parseInt(this.sumaEntradaIniInv) + parseInt(this.sumaEntradaAd) + parseInt(this.sumaEntradaRed) + parseInt(this.sumaEntradaCred) + parseInt(this.sumaEntradaContraCred);

            this.sumaSalidaProgramacion = parseInt(this.sumaSalidaIni) + parseInt(this.sumaSalidaIniInv) + parseInt(this.sumaSalidaAd) + parseInt(this.sumaSalidaRed) + parseInt(this.sumaSalidaCred) + parseInt(this.sumaSalidaContraCred);

            this.sumaSaldoProgramacion = parseInt(this.sumaSaldoIni) + parseInt(this.sumaSaldoIniInv) + parseInt(this.sumaSaldoAd) + parseInt(this.sumaSaldoRed) + parseInt(this.sumaSaldoCred) + parseInt(this.sumaSaldoContraCred);
        },

        async sumarCdp(){

            this.sumaEntradaCdp = 0;
            this.sumaSalidaCdp = 0;
            this.sumaSaldoCdp = 0;

            await this.detallesCdp.forEach(element => {
                this.sumaEntradaCdp += parseInt(element[12]);
                this.sumaSalidaCdp += parseInt(element[13]);
                this.sumaSaldoCdp += parseInt(element[14]);
            });

        },

        async sumarRp(){

            this.sumaEntradaRp = 0;
            this.sumaSalidaRp = 0;
            this.sumaSaldoRp = 0;

            await this.detallesRp.forEach(element => {
                this.sumaEntradaRp += parseInt(element[12]);
                this.sumaSalidaRp += parseInt(element[13]);
                this.sumaSaldoRp += parseInt(element[14]);
            });

        },

        async sumarCxp(){

            this.sumaEntradaCxp = 0;
            this.sumaSalidaCxp = 0;
            this.sumaSaldoCxp = 0;

            await this.detallesCxp.forEach(element => {
                this.sumaEntradaCxp += parseInt(element[12]);
                this.sumaSalidaCxp += parseInt(element[13]);
                this.sumaSaldoCxp += parseInt(element[14]);
            });

        },

        async sumarEgreso(){

            this.sumaEntradaEgreso = 0;
            this.sumaSalidaEgreso = 0;
            this.sumaSaldoEgreso = 0;

            await this.detallesEgreso.forEach(element => {
                this.sumaEntradaEgreso += parseInt(element[12]);
                this.sumaSalidaEgreso += parseInt(element[13]);
                this.sumaSaldoEgreso += parseInt(element[14]);
            });

        },

        cambiaTipoDeGasto(){

        },

        parametrosCompletos: function(){
            
            
           /*  if(this.tipoGasto == 3){
                var vig = document.getElementById('vigencia').value;
                if(this.codProyecto != '' &&this.programatico != ''  && this.fuente != '' && vig != '' && this.tipoGasto != '' && this.selectUnidadEjecutora != '' && this.selectMedioPago != '' && this.selectVigenciaGasto != ''){
                    this.calcularSaldoInversion();
                    this.tieneCpc();
                }else{
                    this.saldo = 0;
                }
            }else{
                var vig = document.getElementById('vigencia').value;
                if(this.cuenta != '' && this.fuente != '' && vig != '' && this.tipoGasto != '' && this.selectUnidadEjecutora != '' && this.selectMedioPago != '' && this.selectVigenciaGasto != ''){
                    this.calcularSaldo();
                    this.tieneCpc();
                }else{
                    this.saldo = 0;
                }
            } */
            
        },

        async seccionPresupuestal(){
            const URL = `${DIR_FULL}vue/presupuesto_ccp/ccp-inicialgastosfun.php?action=secciones`;
            await axios.post(URL)
                        .then(
                            (response)=>{
                                this.unidadesejecutoras = response.data.secciones;
                                this.selectUnidadEjecutora = this.unidadesejecutoras[0][0];
                            }
                        );
        },

        async vigenciasDelgasto(){
            const URL = `${DIR_FULL}vue/presupuesto_ccp/ccp-inicialgastosfun.php?action=vigenciasDelGasto`;
            await axios.post(URL)
                        .then(
                            (response) => {
                                this.vigenciasdelgasto = response.data.vigenciasDelGasto;
                                this.selectVigenciaGasto = this.vigenciasdelgasto[0][0];
                            }
                        );
        },

        async ventanaRubro(){

            this.loading = true;

            const URL = `${DIR_FULL}vue/presupuesto_ccp/ccp-cdpbasico.php?action=cargarCuentas&inicioCuenta=${this.tiposDeGasto[this.tipoGasto-1][2]}`;

            await axios.post(URL)
                .then((response) => {
                    this.showModal_rubros = true;
                    this.cuentasCcpet = response.data.cuentasCcpet;
                    this.tieneCpc();
                }).finally(() => {
                    this.loading =  false;
                });
        },

        searchMonitorCuenta(){
            var keywordCuenta = this.toFormData(this.searchCuenta);

            const URL = `${DIR_FULL}vue/presupuesto_ccp/ccp-cdpbasico.php?action=filtrarCuentas&inicioCuenta=${this.tiposDeGasto[this.tipoGasto-1][2]}`;

            axios.post(URL, keywordCuenta)
            .then((response) => {
                this.cuentasCcpet = response.data.cuentasCcpet;
            });
        },

        seleccionarCuenta(cuentaSelec){
            if(cuentaSelec[6] == 'C'){

                this.rubro = cuentaSelec [1];
                this.nombreRubro = cuentaSelec [2];
                this.showModal_rubros = false;
                this.tieneCpc();
                
            }else{
                
                
                this.showModal_rubros = false;
                Swal.fire(
                'Tipo de cuenta incorrecto.',
                'Escoger una cuenta de captura (C)',
                'warning'
                ).then((result) => {
                    
                    if(result.isConfirmed || result.isDismissed){
                        this.showModal_rubros = true;
                    }
               
                });
               
                
            }
            this.parametrosCompletos();
        },

        async buscarCta(){
            if(this.rubro != ''){

                const URL = `${DIR_FULL}vue/presupuesto_ccp/ccp-cdpbasico.php?action=buscarCuenta&cuenta=${this.rubro}`;

                await axios.post(URL)
                .then((response) => {
                    if(response.data.resultBusquedaCuenta.length > 0){
                        if(response.data.resultBusquedaCuenta[0][1] == 'C'){
                            this.nombreRubro = response.data.resultBusquedaCuenta[0][0];
                            this.codigoCpc = '';
                            this.nombreCodigoCpc = '';
                            this.tieneCpc();
                            
                        }else{
                            Swal.fire(
                            'Tipo de cuenta incorrecto.',
                            'Escoger una cuenta de captura (C)',
                            'warning'
                            ).then((result) => {
                                this.nombreRubro = '';
                                this.rubro = '';
                                this.codigoCpc = '';
                                this.nombreCodigoCpc = '';
                                this.tieneCpc();
                                
                            });
                        }
                        
                    }else{
                        Swal.fire(
                            'Cuenta incorrecta.',
                            'Esta cuenta no existe en el catalogo CCPET.',
                            'warning'
                            ).then((result) => {
                                this.nombreRubro = '';
                                this.rubro = '';
                                this.codigoCpc = '';
                                this.nombreCodigoCpc = '';
                                this.tieneCpc();
                            });
                    }
                    
                });
                
            }else{
                this.nombreRubro = '';
            }
            this.parametrosCompletos();
        },

        ventanaFuente(){

            this.showModal_fuentes = true;

            const URL = `${DIR_FULL}vue/presupuesto_ccp/ccp-cdpbasico.php?action=cargarFuentes&cuenta=${this.rubro}&tipoGasto=${this.tipoGasto}&proyecto=${this.codProyecto}&programatico=${this.programatico}`;
            
            axios.post(URL)
                .then((response) => {
                    console.log(response.data);
                    this.fuentesCuipo = response.data.fuentes;
                    
                });
        },

        async buscarFuente(){

            if(this.fuente != ''){

                const URL = `${DIR_FULL}vue/presupuesto_ccp/ccp-cdpbasico.php?action=buscarFuente&fuente=${this.fuente}`;

                await axios.post(URL)
                .then((response) => {

                    if(response.data.resultBusquedaFuente.length > 0){
                        
                        this.nombreFuente = response.data.resultBusquedaFuente[0][0];
                        
                    }else{
                        Swal.fire(
                            'Fuente incorrecta.',
                            'Esta Fuente no existe en las fuentes CUIPO del sistema.',
                            'warning'
                            ).then((result) => {
                                this.nombreFuente = '';
                                this.fuente = '';
                            });
                    }
                    
                });
            }else{
                this.nombreFuente = '';
                this.fuente = '';
            }
            this.parametrosCompletos();
        },

        seleccionarFuente(fuenteSelec){
            this.fuente = fuenteSelec [0];
            this.nombreFuente = fuenteSelec [1];
            this.showModal_fuentes = false;
            this.parametrosCompletos();
        },

        searchMonitorFuente(){
            var keywordFuente = this.toFormData(this.searchFuente);

            const URL = `${DIR_FULL}vue/presupuesto_ccp/ccp-cdpbasico.php?action=filtrarFuentes`;

            axios.post(URL, keywordFuente)
            .then((response) => {
                this.fuentesCuipo = response.data.fuentes;
                
            });
        },


        ventanaCodigoCpc(){
            if(this.rubro){
                if(this.cuentaConCpc){
                    this.buscaVentanaModal(this.cuentaConCpc, this.rubro);
                }
                else{
                    Swal.fire(
                        'Esta cuenta no requiere CPC.',
                        'La cuenta CCPET seleccionada no requiere catalogo CPC.',
                        'warning'
                        ).then((result) => {
                        });
                }
            }else{
                Swal.fire(
                    'Falta escoger la cuenta.',
                    'Falta escoger la cuenta para buscar los CPC relacionados con la cuenta CCPET.',
                    'warning'
                    ).then((result) => {
                    });
            }
            
        
        },

        async buscaVentanaModal(clasificador, cuentaCpc){
            
            switch (clasificador){
                
                case "2":
                    this.showModal_bienes_transportables = true;
                    this.show_table_search = false;
                    this.fetchMembersBienes();
                    var ultimoCaracter = cuentaCpc.substr(-1,1);
                    var cuentaArr = [];
                    cuentaArr[0] = ultimoCaracter;
                    cuentaArr[1] = "nombre";
                    this.division(cuentaArr);
                    this.show_table_search = true;
                break;
                case "3":
                    this.showModal_servicios = true;
                    this.show_table_search = false;
                    this.fetchMembersServicios();
                    var ultimoCaracter = cuentaCpc.substr(-1,1);
                    var cuentaArr = [];
                    cuentaArr[0] = ultimoCaracter;
                    cuentaArr[1] = "nombre";
                    this.divisionServicios(cuentaArr);
                    this.show_table_search = true;
                break;
            }
            
        },

        tieneCpc(){
            if(this.rubro != ''){

                const URL = `${DIR_FULL}vue/presupuesto_ccp/ccp-cdpbasico.php?action=tieneCpc&cuenta=${this.rubro}`;

                axios.post(URL)
                    .then((response) => {
                    
                    if(response.data.tieneCpc == null){
                        this.cuentaConCpc = '';
                    }else{
                        this.cuentaConCpc = response.data.tieneCpc;
                    }
                    
                });
            }else{
                
                this.cuentaConCpc = '';
            }
        },

        async fetchMembersBienes(){
            const URL = `vue/ccp-bienestransportables.php`;
            await axios.post(URL)
                .then((response) => {
                    this.secciones = response.data.secciones;
                });
        },

        async division(seccion, scrollArriba = false){
            this.searchDivision= {keywordDivision: ''};
            this.searchGrupo= {keywordGrupo: ''};
            this.searchClase= {keywordClase: ''};
            this.searchSubClase= {keywordSubClase: ''};
            this.divisiones = [];
            this.grupos = [];
            this.clases = [];
            this.subClases = [];
            this.division_p = '';
            this.division_p_nombre = '';
            this.grupo_p = '';
            this.grupo_p_nombre = '';
            this.clase_p = '';
            this.clase_p_nombre = '';
            //this.subClase_p = '';
            this.subClase_p_nombre = '';
            this.mostrarGrupo = false;
            this.mostrarClase = false;
            this.mostrarSubClase = false;
            this.mostrarSubClaseProducto = false;
            this.seccion_p = seccion[0];
            this.seccion_p_nombre = seccion[1];

            const URL = `vue/ccp-bienestransportables.php?seccion=${this.seccion_p}`;

            await axios.post(URL)
                .then((response) => {

                    this.divisiones = response.data.divisiones;
                    
                    if(response.data.divisiones == ''){
                        this.mostrarDivision = false;
                    }
                    else{
                        this.mostrarDivision = true;
                    }
                });
            if(!scrollArriba)
            {
                setTimeout(function(){ document.getElementById("end_page").scrollIntoView({block: "end", behavior: "smooth"}); }, 1)
            }
        },

        buscarGrupo(division, scrollArriba = false){
            this.searchGrupo= {keywordGrupo: ''};
            this.searchClase= {keywordClase: ''};
            this.searchSubClase = {keywordSubClase: ''};
            this.grupos = [];
            this.clases = [];
            this.subClases = [];
            this.mostrarSubClaseProducto = false;
            this.subClases_captura = [];
            this.mostrarClase = false;
            this.mostrarSubClase = false;
            this.grupoServicios_p = '';
            this.claseServicios_p = '';
            this.subClaseServicios_p = '';
            if(this.division_p == '' || this.division_p == division[0] || !this.mostrarGrupo)
            {
                this.mostrarGrupo = !this.mostrarGrupo;
            }
            
            if(this.mostrarGrupo){
                //this.mostrarSiguienteNivel = !this.mostrarSiguienteNivel;
                this.division_p = division[0];

                const URL = `vue/ccp-bienestransportables.php?division=${this.division_p}`;

                axios.post(URL)
                    .then((response) => {
                        this.grupos = response.data.grupos;
                        if(response.data.grupos == ''){
                            this.mostrarGrupo = false;
                        }
                        else{
                            this.mostrarGrupo = true;
                        }
                    });
            }
            if(!scrollArriba)
            {
                setTimeout(function(){ document.getElementById("end_page").scrollIntoView({behavior: "smooth"}); }, 1)

            }
        },

        buscarClase(grupo, scrollArriba = false){
            this.searchClase= {keywordClase: ''};
            this.searchSubClase= {keywordSubClase: ''};
            this.mostrarSubClaseProducto = false;
            this.subClases_captura = [];
            this.clases = [];
            this.subClases = [];
            this.mostrarSubClase = false;
            this.searchProduct = {keywordProduct: ''};
            this.clase_p = '';
            this.clase_p_nombre = '';
            //this.subClase_p = '';
            this.subClase_p_nombre = '';
            //this.productos = [];


            if(this.grupo_p == '' || this.grupo_p == grupo[0] || !this.mostrarClase)
            {
                this.mostrarClase = !this.mostrarClase;
            }
            
            if(this.mostrarClase){
                //this.mostrarSiguienteNivel = !this.mostrarSiguienteNivel;
                this.grupo_p = grupo[0];
                this.grupo_p_nombre = grupo[1];

                const URL = `${DIR_FULL}vue/ccp-bienestransportables.php?grupo=${this.grupo_p}`;

                axios.post(URL)
                    .then((response) =>{
                        this.clases = response.data.clases;
                        if(response.data.clases == ''){
                            this.mostrarClase = false;
                        }
                        else{
                            this.mostrarClase = true;
                        }
                    });
            }
            if(!scrollArriba)
            {
                setTimeout(function(){ document.getElementById("end_page").scrollIntoView({behavior: "smooth"}); }, 1)

            }
        },

        buscarSubclase(clase, scrollArriba = false){
            this.searchSubClase = {keywordSubClase: ''};
            
            this.subClases = [];
            
            this.mostrarSubClase = false;
            this.mostrarSubClaseProducto = false;
            this.subClases_captura = [];
            this.searchProduct = {keywordProduct: ''};
            
            this.subClaseServicios_p = '';
            //this.productos = [];
            //this.subClase_p = '';
            this.subClase_p_nombre = '';
            if(this.clase_p == '' || this.clase_p == clase[0] || !this.mostrarSubClase)
            {
                this.mostrarSubClase = !this.mostrarSubClase;
            }
            
            if(this.mostrarSubClase)
            {
                //this.mostrarSiguienteNivel = !this.mostrarSiguienteNivel;
                this.clase_p = clase[0];
                this.clase_p_nombre = clase[1];

                const URL = `${DIR_FULL}vue/ccp-bienestransportables.php?clase=${this.clase_p}`;

                axios.post(URL)
                    .then((response) => {
                        this.subClases = response.data.subClases;
                        if(response.data.subClases == ''){
                            this.mostrarSubClase = false;
                        }
                        else{
                            this.mostrarSubClase = true;
                        }
                    });
            }
            if(!scrollArriba)
            {
                setTimeout(function(){ document.getElementById("end_page").scrollIntoView({behavior: "smooth"}); }, 1)

            }
        },

        async seleccionarSublaseProducto(subClase, scrollArriba = false){
            //this.showModal = !this.showModal;
            this.subClase_p = subClase[0];
            this.subClase_p_nombre = subClase[1];

            const URL = `${DIR_FULL}vue/ccp-bienestransportables.php?subClase=${this.subClase_p}`;

            await axios.post(URL)
            .then((response) => {
                this.subClases_captura = response.data.subClases_captura;
                if(response.data.subClases_captura == ''){
                    this.mostrarSubClaseProducto = false;
                }
                else{
                    this.mostrarSubClaseProducto = true;
                }
            });
            if(!scrollArriba)
            {
                setTimeout(function(){ document.getElementById("end_page").scrollIntoView({behavior: "smooth"}); }, 1)

            }
        },

        seleccionarBienes(subClase){
            /*this.cuentasSelectSubClase = [];
            this.cuin_p = '';*/

            this.cuentasSelectSubClase = subClase;
            //this.cuin_p = cuin[2];

            this.subClase_p = subClase[0];
            this.subClase_p_nombre = subClase[1];
        },

        buscarGeneral(){
			if(this.cuentaActual.slice(-1)==this.searchGeneral.keywordGeneral.substr(0,1))
			{
				var parsedobj = JSON.parse(JSON.stringify(this.searchGeneral))
				if(parsedobj.keywordGeneral == '')
				{
					this.mostrarSeccion = true;
					this.show_table_search = false;
					this.fetchMembers();
				}
				else
				{
					this.grupos = [];
					this.clases = [];
					this.subClases = [];
					this.result_search = [];
					this.mostrarGrupo = false;
					this.mostrarClase = false;
					this.mostrarSubClase = false;
					this.mostrarSeccion = false;
					var keywordGeneral = this.toFormData(this.searchGeneral);

                    const URL = `${DIR_FULL}vue/ccp-bienestransportables.php?action=searchGeneral`;
					axios.post(URL, keywordGeneral)
						.then((response) => {
							this.result_search = response.data.subClasesGeneral;
							if(response.data.subClasesGeneral == ''){
								this.noMember = true;
								this.show_table_search = true;
							}
							else{
								this.noMember = false;
								this.show_levels(this.result_search[0]);
								this.show_table_search = true;
							}
						});
					// Enviar el scroll al final cuando ya esta definido
					/*setTimeout(() => {
						document.getElementById("end_page").scrollIntoView({behavior: 'smooth'});   
					}, 50);*/
					setTimeout(function(){ document.getElementById("end_page").scrollIntoView({block: "end", behavior: "smooth"}); }, 1);
					this.searchGeneral.keywordGeneral = '';
				}
			}
			else
			{
                this.showModal_bienes_transportables = false;
                Swal.fire(
                'Error en el c&oacute;digo.',
                'Digite un c&oacute;digo que empiece con el mismo n&uacute;mero que termina la cuenta CCPET.',
                'error'
                ).then((result) => {
                    
                    if(result.isConfirmed || result.isDismissed){
                        this.showModal_bienes_transportables = true;
                    }
               
                });
				
			}
		},

        buscarGeneral2(){
			if(this.cuentaActual.slice(-1)==this.searchGeneral2.keywordGeneral.substr(0,1))
			{
				var parsedobj = JSON.parse(JSON.stringify(this.searchGeneral2))
				if(parsedobj.keywordGeneral == '')
				{
					this.mostrarSeccion = true;
					this.show_table_search = false;
					this.fetchMembers();
				}
				else
				{
					this.grupos = [];
					this.clases = [];
					this.subClases = [];
					this.result_search = [];
					this.mostrarGrupo = false;
					this.mostrarClase = false;
					this.mostrarSubClase = false;
					this.mostrarSeccion = false;
					this.cuentasSelectSubClaseServicios = [];
					var keywordGeneral = this.toFormData(this.searchGeneral2);

                    const URL = `${DIR_FULL}vue/ccp-servicios.php?action=searchGeneral`;
					axios.post(URL, keywordGeneral)
						.then((response) => {
							this.result_search = response.data.subClasesGeneral;
							if(response.data.subClasesGeneral == '')
							{
								this.noMember = true;
								this.show_table_search = true;
							}
							else
							{
								this.noMember = false;
								this.show_levels2(this.result_search[0]);
								this.show_table_search = true;
							}
						});
					setTimeout(function(){ document.getElementById("end_page").scrollIntoView({block: "end", behavior: "smooth"}); }, 1);
					this.searchGeneral2.keywordGeneral = '';
				}
			}
			else
			{
				this.toggleMensaje();
				this.colortitulosmensaje='crimson';
				this.titulomensaje='Mensaje de Error';
				this.contenidomensaje='Error en el c\xf3digo, no se puede cambiar el sector';
			}
		},

        show_levels(codigo){
            
            this.mostrarSeccion = true;
			codigo_final = codigo[0];
            
            //this.subClase_p = codigo_final;
            codigo_subClase = codigo_final.slice(0,-2);  
            codigo_Clase = codigo_subClase.slice(0,-1); 
            codigo_grupo = codigo_Clase.slice(0,-1);
            codigo_division = codigo_grupo.slice(0,-1);
            codigo_seccion = codigo_division.slice(0,-1);
            this.searchHastaSubclase(codigo_final, codigo_subClase, codigo_Clase, codigo_grupo, codigo_division, codigo_seccion, codigo[5]);            
            // else{
            // }
            /*setTimeout(() => {
                const start_page = document.getElementById("start_page").scrollIntoView({behavior: 'smooth'});  
            }, 50);*/

        },

        async searchHastaSubclase(codigo_final, codigo_subClase, codigo_Clase, codigo_grupo, codigo_division, codigo_seccion, ud){
            this.secciones = [];
            this.divisiones = [];
            this.grupos = [];
            this.clases = [];
            this.subClases = [];

            this.seccion_general = [];
            this.division_general = [];
            this.grupo_general = [];
            this.clase_general = [];
            this.subClase_general = [];
            this.producto_general = [];

            this.seccion_p = codigo_seccion;

            this.fetchMembersBienes();

            // aqui empieza a buscar la seccion 
            //var seccion_nombre_buscar =  await this.buscarNombre(this.seccion_p);

            this.seccion_general.push(this.seccion_p);
            //this.seccion_general.push(seccion_nombre_buscar);
            this.division(this.seccion_general, false);
            // aqui termina

            //aqui empiza a buscar el nombre de la division
            //var seccion_nombre_buscar =  await this.buscarNombre(codigo_division);

            this.division_general.push(codigo_division);
            //this.division_general.push(seccion_nombre_buscar);
            this.buscarGrupo(this.division_general, false);
            //qui termina
            //aqui empiza a buscar el nombre del grupo
            //var seccion_nombre_buscar =  await this.buscarNombre(codigo_grupo);

            this.grupo_general.push(codigo_grupo);
            //this.grupo_general.push(seccion_nombre_buscar);

            this.buscarClase(this.grupo_general, false);
            //aqui termina
            //aqui empiza a buscar el nombre de la clase
            //var seccion_nombre_buscar =  await this.buscarNombre(codigo_Clase);

            this.clase_general.push(codigo_Clase);
            //this.clase_general.push(seccion_nombre_buscar);

            this.buscarSubclase(this.clase_general, false);
            

            this.producto_general.push(codigo_subClase);
            //this.producto_general.push(seccion_nombre_buscar);

            this.seleccionarSublaseProducto(this.producto_general);

            var seccion_nombre_buscar =  await this.buscarNombre(codigo_final);
            this.cuentasSelectSubClase.push(codigo_final);
            this.cuentasSelectSubClase.push(seccion_nombre_buscar);
            this.cuentasSelectSubClase.push(ud);

            this.seleccionarBienes(this.cuentasSelectSubClase);
            
            //aqui termina
            //this.subClase_p = codigo_subClase;

           this.searchGeneral = {keywordGeneral: ''};
        },

        searchMonitorGrupos() {
            var keywordGrupo = this.toFormData(this.searchGrupo);
            this.clases = [];
            this.subClases = [];
            this.mostrarClase = false;
            this.mostrarSubClase = false;
            this.searchClase= {keywordClase: ''};
            this.searchSubClase= {keywordSubClase: ''};

            const URL = `${DIR_FULL}vue/ccp-bienestransportables.php?action=searchGrupo&divisionSearch=${this.division_p}`;

            axios.post(URL, keywordGrupo)
                .then((response) => {
                    this.grupos = response.data.grupos;
                    if(response.data.grupos == ''){
                        this.noMember = true;
                        // this.show_table_search = false
                    }
                    else{
                        this.noMember = false;
                        //this.show_table_search = true
                    }
                    
                });
                
        },

        searchMonitorClases() {
            var keywordClase = this.toFormData(this.searchClase);
            this.subClases = [];
            this.mostrarSubClase = false;
            this.searchSubClase= {keywordSubClase: ''};

            const URL = `${DIR_FULL}vue/ccp-bienestransportables.php?action=searchClase&grupoSearch=${this.grupo_p}`;

            axios.post(URL, keywordClase)
                .then((response) => {
                    this.clases = response.data.clases;
                    if(response.data.clases == ''){
                        this.noMember = true;
                        // this.show_table_search = false
                    }
                    else{
                        this.noMember = false;
                        //this.show_table_search = true
                    }
                    
                });
        },

        searchMonitorSubClases() {
            var keywordSubClase = this.toFormData(this.searchSubClase);

            const URL = `vue/ccp-bienestransportables.php?action=searchSubClase&subClaseSearch=${this.clase_p}`;
            axios.post(URL, keywordSubClase)
                .then((response) => {
                    this.subClases = response.data.subClases;
                    if(response.data.subClases == ''){
                        this.noMember = true;
                        // this.show_table_search = false
                    }
                    else{
                        this.noMember = false;
                        //this.show_table_search = true
                    }
                }); 
        },

        seleccionarCpc(){
            if(this.subClase_p != ''){
                this.codigoCpc = this.subClase_p;
                this.nombreCodigoCpc = this.subClase_p_nombre;
                this.subClase = '';
                this.subClase_p_nombre = '';
                this.showModal_bienes_transportables = false;
            }else{
                Swal.fire(
                    'Falta informaci&oacute;n.',
                    'Falta escoger el producto o servicio.',
                    'warning'
                );
            }
            
        },

        seleccionarServicioCpc(){
            if(this.subClaseServicios_p != ''){
                this.codigoCpc = this.subClaseServicios_p;
                this.nombreCodigoCpc = this.subClaseServicios_p_nombre;
                this.subClase = '';
                this.subClaseServicios_p_nombre = '';
                this.showModal_servicios = false;
            }else{
                Swal.fire(
                    'Falta informaci&oacute;n.',
                    'Falta escoger el producto o servicio.',
                    'warning'
                );
            }
            
        },

        fetchMembersServicios(){

            const URL = `vue/ccp-servicios.php`;
            
            axios.post(URL)
                .then((response) => {
                    this.seccionesServicios = response.data.secciones;
                });
        },

        divisionServicios(seccion, scrollArriba = false)
        {
            this.searchDivisionServicios = {keywordDivisionServicios: ''};
            this.searchGrupoServicios = {keywordGrupoServicios: ''};
            this.searchClaseServicios = {keywordClaseServicios: ''};
            this.searchSubClaseServicios = {keywordSubClaseServicios: ''};
            this.gruposServicios = [];
            this.clasesServicios = [];
            this.subClasesServicios = [];
            this.divisionServicios_p = '';
            this.grupoServicios_p = '';
            this.claseServicios_p = '';
            this.subClaseServicios_p = '';
            this.mostrarGrupoServicios = false;
            this.mostrarClaseServicios = false;
            this.mostrarSubClaseServicios = false;

            this.seccionServicios_p = seccion[0];
            this.seccionServicios_p_nombre = seccion[1];

            const URL = `${DIR_FULL}vue/ccp-servicios.php?seccion=${this.seccionServicios_p}`;
            axios.post(URL)
                .then((response) => {
                    this.divisionesServicios = response.data.divisiones;
                    if(response.data.divisiones == ''){
                        this.mostrarDivisionServicios = false;
                    }
                    else{
                        this.mostrarDivisionServicios = true;
                    }
                });
            if(!scrollArriba)
            {
                setTimeout(function(){ document.getElementById("end_page_servicios").scrollIntoView({block: "end", behavior: "smooth"}); }, 1)
            }
        },

        buscarGrupoServicios(division, scrollArriba = false){
            this.searchGrupoServicios= {keywordGrupoServicios: ''};
            this.searchClaseServicios= {keywordClaseServicios: ''};
            this.searchSubClaseServicios = {keywordSubClaseServicios: ''};
            this.gruposServicios = [];
            this.clasesServicios = [];
            this.subClasesServicios = [];
            this.mostrarClaseServicios = false;
            this.mostrarSubClaseServicios = false;
            this.grupoServicios_p = '';
            this.claseServicios_p = '';
            this.subClaseServicios_p = '';
            if(this.divisionServicios_p == '' || this.divisionServicios_p == division[0] || !this.mostrarGrupoServicios)
            {
                this.mostrarGrupoServicios = !this.mostrarGrupoServicios;
            }
            
            if(this.mostrarGrupoServicios)
            {
                //this.mostrarSiguienteNivel = !this.mostrarSiguienteNivel;
                this.divisionServicios_p = division[0];

                const URL = `${DIR_FULL}vue/ccp-servicios.php?division=${this.divisionServicios_p}`;
                axios.post(URL)
                    .then((response) => {
                        this.gruposServicios = response.data.grupos;
                        if(response.data.grupos == ''){
                            this.mostrarGrupoServicios = false;
                        }
                        else{
                            this.mostrarGrupoServicios = true;
                        }
                    });
            }
            if(!scrollArriba)
            {
                setTimeout(function(){ document.getElementById("end_page_servicios").scrollIntoView({behavior: "smooth"}); }, 1)

            }
        },

        buscarClaseServicios(grupo, scrollArriba = false){
            this.searchClaseServicios= {keywordClaseServicios: ''};
            this.searchSubClaseServicios = {keywordSubClaseServicios: ''};
            this.clasesServicios = [];
            this.subClasesServicios = [];
            this.mostrarSubClaseServicios = false;
            this.claseServicios_p = '';
            this.subClaseServicios_p = '';
            //this.productos = [];
            if(this.grupoServicios_p == '' || this.grupoServicios_p == grupo[0] || !this.mostrarClaseServicios)
            {
                this.mostrarClaseServicios = !this.mostrarClaseServicios;
            }
            
            if(this.mostrarClaseServicios)
            {
                //this.mostrarSiguienteNivel = !this.mostrarSiguienteNivel;
                this.grupoServicios_p = grupo[0];

                const URL = `${DIR_FULL}vue/ccp-servicios.php?grupo=${this.grupoServicios_p}`;
                axios.post(URL)
                    .then((response) => {
                        this.clasesServicios = response.data.clases;
                        if(response.data.clases == ''){
                            this.mostrarClaseServicios = false;
                        }
                        else{
                            this.mostrarClaseServicios = true;
                        }
                    });
            }
            if(!scrollArriba)
            {
                setTimeout(function(){ document.getElementById("end_page_servicios").scrollIntoView({behavior: "smooth"}); }, 1)

            }
        },

        buscarSubclaseServicios(clase, scrollArriba = false){
            this.searchSubClaseServicios = {keywordSubClaseServicios: ''};
            //this.productos = [];
            this.subClasesServicios = [];
            this.subClaseServicios_p = '';
            if(this.claseServicios_p == '' || this.claseServicios_p == clase[0] || !this.mostrarSubClaseServicios)
            {
                this.mostrarSubClaseServicios = !this.mostrarSubClaseServicios;
            }
            
            if(this.mostrarSubClaseServicios)
            {
                //this.mostrarSiguienteNivel = !this.mostrarSiguienteNivel;
                this.claseServicios_p = clase[0];

                const URL = `${DIR_FULL}vue/ccp-servicios.php?clase=${this.claseServicios_p}`;
                axios.post(URL)
                    .then((response) => {
                        this.subClasesServicios = response.data.subClases;
                        if(response.data.subClases == ''){
                            this.mostrarSubClaseServicios = false;
                        }
                        else{
                            this.mostrarSubClaseServicios = true;
                        }
                    });
            }
            if(!scrollArriba)
            {
                setTimeout(function(){ document.getElementById("end_page_servicios").scrollIntoView({behavior: "smooth"}); }, 1)

            }
        },

        seleccionarServicios(subClase){
			this.cuentasSelectSubClaseServicios = subClase;
			this.subClaseServicios_p = subClase[0];
			this.subClaseServicios_p_nombre = subClase[1];
		},

        async searchHastaSubclase2(codigo_final, codigo_subClase, codigo_Clase, codigo_grupo, codigo_division, codigo_seccion){
			this.secciones = [];
			this.divisiones = [];
			this.grupos = [];
			this.clases = [];
			this.subClases = [];
			this.seccion_general = [];
			this.division_general = [];
			this.grupo_general = [];
			this.clase_general = [];
			this.subClase_general = [];
			this.seccion_p = codigo_seccion;
			// aqui empieza a buscar la seccion 
			this.seccion_general.push(this.seccion_p);
			this.divisionServicios(this.seccion_general, false);
			// aqui termina
			//aqui empiza a buscar el nombre de la division
			this.division_general.push(codigo_division);
			this. buscarGrupoServicios(this.division_general, false);
			//qui termina
			//aqui empiza a buscar el nombre del grupo
			this.grupo_general.push(codigo_grupo);
			this.buscarClaseServicios(this.grupo_general, false);
			//aqui termina
			//aqui empiza a buscar el nombre de la clase
			this.clase_general.push(codigo_Clase);
			this.buscarSubclaseServicios(this.clase_general, false);
			this.cuentasSelectSubClaseServicios.push(codigo_final);
			this.seleccionarServicios(this.cuentasSelectSubClaseServicios);
			//aqui termina
			this.searchGeneral = {keywordGeneral: ''};
		},

        async buscarCodigoCpc(){

            if(this.codigoCpc != ''){
                let iniciCodigoCpc = this.codigoCpc.substr(0,1);
                let finCuenta = this.rubro.substr(-1, 1);
                if (iniciCodigoCpc == finCuenta) {
                    //if(this.codigoCpc.substr(0,1))

                    const URL = `${DIR_FULL}vue/presupuesto_ccp/ccp-cdpbasico.php?action=buscarCpc&codigoCpc=${this.codigoCpc}`;

                    await axios.post(URL)
                    .then((response) => {
                        
                        if(response.data.resultBusquedaCpc.length > 0){
                            
                            this.nombreCodigoCpc = response.data.resultBusquedaCpc[0][0];
                            
                            
                        }else{
                            Swal.fire(
                                'C&oacute;digo CPC incorrecto.',
                                'Este C&oacute;digo no existe en el catalogo CPC del sistema.',
                                'warning'
                                ).then((result) => {
                                    this.nombreCodigoCpc = '';
                                    this.codigoCpc = '';
                                });
                        }
                        
                    });
                }else{
                    Swal.fire(
                        'C&oacute;digo CPC equivocado.',
                        'Este C&oacute;digo no pertenece a esta esta cuenta ccpet.',
                        'warning'
                        ).then((result) => {
                            this.nombreCodigoCpc = '';
                            this.codigoCpc = '';
                        }
                    );
                }
                
            }else{
                this.nombreCodigoCpc = '';
            }
        },

        async ventanaProyecto(){

            let vig = document.getElementById('vigencia').value;

            if(vig != ''){

                this.showModal_proyectos = true;

                const URL = `${DIR_FULL}vue/presupuesto_ccp/ccp-cdpbasico.php?action=cargarProyecto&vigencia=${vig}`;

                await axios.post(URL)
                    .then((response) => {
                        this.proyectos = response.data.proyectosCcpet;
                    });
            }else{
                Swal.fire(
                    'Falta escoger las fechas.',
                    'Los proyectos a mostrar pertenecen la la vigencia de la fecha escogida.',
                    'warning'
                    ).then((result) => {
                        
                    }
                );
            }
            
        },

        async buscarProyecto(){
            if(this.codProyecto != ''){

                const URL = `${DIR_FULL}vue/presupuesto_ccp/ccp-cdpbasico.php?action=buscarProyecto&proyecto=${this.codProyecto}`;
                await axios.post(URL)
                .then((response) => {
                    if(response.data.resultBusquedaProyecto.length > 0){
                            this.nombreProyecto = response.data.resultBusquedaProyecto[0][0];
                    }else{
                        Swal.fire(
                            'Proyecto incorrecto.',
                            'Este proyecto no existe.',
                            'warning'
                            ).then((result) => {
                                this.nombreProyecto = '';
                                this.codProyecto = '';
                            });
                    }
                    
                });
                
            }else{
                this.nombreProyecto = '';
            }
            this.parametrosCompletos();
        },

        searchMonitorProyecto(){
            var keywordProyecto = this.toFormData(this.searchProyecto);
            var vig = document.getElementById('vigencia').value;

            const URL = `${DIR_FULL}vue/presupuesto_ccp/ccp-cdpbasico.php?action=filtrarProyectos&vigencia=${vig}`;

            axios.post(URL, keywordProyecto)
            .then((response) => {
                this.proyectos = response.data.proyectosCcpet;
            });
        },

        seleccionarProyecto(bpim, nombreProy){

            this.codProyecto = bpim;
            this.nombreProyecto = nombreProy;
            this.showModal_proyectos = false;
            
            this.parametrosCompletos();
        },

        ventanaProgramatico(){

            if(this.codProyecto != ''){

                this.showModal_programatico = true;

                const URL = `${DIR_FULL}vue/presupuesto_ccp/ccp-cdpbasico.php?action=cargarProgramaticos&proyecto=${this.codProyecto}`;

                axios.post(URL)
                    .then((response) => {
                        this.programaticos = response.data.programaticos;
                    });
            }else{

                Swal.fire(
                    'Falta escoger el proyecto.',
                    'Antes de seleccionar el program&aacute;tico se debe seleccionar un proyecto.',
                    'warning'
                    ).then((result) => {
                        
                    });
            }
        },

        async buscarProgramatico(){
            if(this.programatico != ''){
                if(this.codProyecto != ''){

                    const URL = `${DIR_FULL}vue/presupuesto_ccp/ccp-cdpbasico.php?action=buscarProgramatico&programatico=${this.programatico}`;
                    await axios.post(URL)
                    .then((response) => {
                        if(response.data.resultBusquedaProgramatico.length > 0){
                                this.nombreProgramatico = response.data.resultBusquedaProgramatico[0][0];
                        }else{
                            Swal.fire(
                                'Proyecto incorrecto.',
                                'Este proyecto no existe.',
                                'warning'
                                ).then((result) => {
                                    this.nombreProgramatico = '';
                                });
                        }
                        
                    });
                }else{
                    Swal.fire(
                        'Falta escoger un proyecto.',
                        '',
                        'info'
                        );
                }
                
                
            }else{
                this.nombreProgramatico = '';
            }
            this.parametrosCompletos();
        },

        seleccionarProgramatico(programaticoSelec){
            
            this.programatico = programaticoSelec [0];
            this.nombreProgramatico = programaticoSelec [1];
            this.showModal_programatico = false;
            
            this.parametrosCompletos();
        },


    }
})
  
app.mount('#myapp')