<?php

    require_once '../../comun.inc';
    require '../../funciones.inc';

    session_start();

	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");

    $out = array('error' => false);

    $action = "show";

    if(isset($_GET['action'])){
        $action = $_GET['action'];
    }

    if($action == 'show'){

        $detalles = array();

        $critFecha = '';

        if($_POST['fechaIni'] != '' && $_POST['fechaFin'] != ''){
            $critFecha = " AND TB1.fecha BETWEEN '".$_POST['fechaIni']."' AND '".$_POST['fechaFin']."'";
        }

        $tipo_comprobante = $_POST['tipo_comp'];
        $seccion_presupuestal = $_POST['seccion_presupuestal'];
        $medio_pago = $_POST['medio_pago'];
        $vigencia_gasto = $_POST['vigencia_gasto'];
        $vigencia_fiscal = $_POST['vigencia_fiscal'];
        $fuente_f = $_POST['fuente_f'];
        $bpim_inv = $_POST['bpim_inv'];
        $cuenta_f = $_POST['cuenta_f'];
        $programatico_inv = $_POST['programatico_inv'];

        if($seccion_presupuestal == 'todos'){
            $seccion_presupuestal = '';
        }
        if($medio_pago == 'todos'){
            $medio_pago = '';
        }
        if($vigencia_gasto == 'todos'){
            $vigencia_gasto = '';
        }

        if($tipo_comprobante == 'disponibilidad'){

            $slqrCdp = "SELECT TB1.consvigencia, TB1.fecha, TB1.objeto, TB1.tipo_mov, TB2.cuenta, TB2.productoservicio, SUM(TB2.valor), LEFT(TB2.productoservicio, 1) FROM ccpetcdp AS TB1, ccpetcdp_detalle AS TB2 WHERE TB1.consvigencia = TB2.consvigencia AND TB1.vigencia = TB2.vigencia AND TB1.tipo_mov = TB2.tipo_mov AND TB1.vigencia = '$vigencia_fiscal' AND TB2.seccion_presupuestal LIKE '%$seccion_presupuestal%' AND TB2.medio_pago LIKE '%$medio_pago%' AND TB2.codigo_vigenciag LIKE '%$vigencia_gasto%' AND TB2.fuente = '$fuente_f' AND TB2.bpim = '$bpim_inv' AND TB2.indicador_producto = '$programatico_inv' AND TB2.cuenta LIKE '$cuenta_f%' $critFecha GROUP BY TB1.consvigencia, TB1.vigencia, TB1.tipo_mov";/* echo $slqrCdp."<br>"; */
            $resCdp = mysqli_query($linkbd, $slqrCdp);
            while($rowCdp = mysqli_fetch_row($resCdp)){

                $detalles_cdp = [];
                $valor = 0;
                $valorRev = 0;

                $sql = "SELECT nombre FROM cuentasccpet WHERE codigo = '$rowCdp[4]'";
                $row = mysqli_fetch_row(mysqli_query($linkbd, $sql));

                $nombreCuenta = $rowCdp[4] . " - " . $row[0];

                if ($rowCdp[7] >= 5) {
                    //servicios

                    $sql = "SELECT titulo FROM ccpetservicios WHERE grupo = '$rowCdp[5]'";
                    $row = mysqli_fetch_row(mysqli_query($linkbd, $sql));
                }
                else {
                    ///bienes transportables.
                    $sql = "SELECT titulo FROM ccpetbienestransportables WHERE grupo = '$rowCdp[5]'";
                    $row = mysqli_fetch_row(mysqli_query($linkbd, $sql));
                }

                $cpc = $rowCdp[5] . " - " . $row[0];

                array_push($detalles_cdp, $rowCdp[0]);
                array_push($detalles_cdp, $rowCdp[1]);
                array_push($detalles_cdp, $rowCdp[2]);
                array_push($detalles_cdp, $rowCdp[3]);
                array_push($detalles_cdp, $nombreCuenta);
                array_push($detalles_cdp, $cpc);

                if($rowCdp[3] == '201'){
                    $valor = $rowCdp[6];
                    $valorRev = 0;
                }else{
                    $valor = 0;
                    $valorRev = $rowCdp[6];
                }
                $cdps = [];
                array_push($cdps, $rowCdp[0]);
                array_push($detalles_cdp, $valor);
                array_push($detalles_cdp, $valorRev);
                array_push($detalles_cdp, $cdps);

                $rps = [];
                $cxps = [];
                $cxpsN = [];
                $egresos = [];
                $egresosN = [];
                $sqlrRp = "SELECT consvigencia FROM ccpetrp WHERE vigencia = '$vigencia_fiscal' AND idcdp = '$rowCdp[0]'";
                $resRp = mysqli_query($linkbd, $sqlrRp);
                while($rowRp = mysqli_fetch_row($resRp)){
                    array_push($rps, $rowRp[0]);

                    $sqlrCxp = "SELECT id_orden FROM tesoordenpago WHERE id_rp = $rowRp[0] AND vigencia = '$vigencia_fiscal'";
                    $resCxp = mysqli_query($linkbd, $sqlrCxp);
                    while($rowCxp = mysqli_fetch_row($resCxp)){
                        array_push($cxps, $rowCxp[0]);

                        $sqlrEgreso = "SELECT id_egreso FROM tesoegresos WHERE id_orden = '$rowCxp[0]'";
                        $resEgreso = mysqli_query($linkbd, $sqlrEgreso);
                        while($rowEgreso = mysqli_fetch_row($resEgreso)){
                            array_push($egresos, $rowEgreso[0]);
                        }
                    }

                    $sqlrCxpN = "SELECT nomina FROM hum_nom_cdp_rp WHERE rp = $rowRp[0] AND vigencia = '$vigencia_fiscal'";
                    $resCxpN = mysqli_query($linkbd, $sqlrCxpN);
                    while($rowCxpN = mysqli_fetch_row($resCxpN)){
                        array_push($cxpsN, $rowCxpN[0]);

                        $sqlrEgresoN = "SELECT id_egreso FROM tesoegresosnomina WHERE id_orden = '$rowCxpN[0]' ";
                        $resEgresoN = mysqli_query($linkbd, $sqlrEgresoN);
                        while($rowEgresoN = mysqli_fetch_row($resEgresoN)){
                            array_push($egresosN, $rowEgresoN[0]);
                        }
                    }
                }
                array_push($detalles_cdp, $rps);
                array_push($detalles_cdp, $cxps);
                array_push($detalles_cdp, $cxpsN);
                array_push($detalles_cdp, $egresos);
                array_push($detalles_cdp, $egresosN);

                array_push($detalles, $detalles_cdp);
            }
        }

        if($tipo_comprobante == 'compromiso'){

            $slqrRp = "SELECT TB1.consvigencia, TB1.fecha, TB1.detalle, TB1.tipo_mov, TB2.cuenta, TB2.productoservicio, SUM(TB2.valor), TB1.idcdp FROM ccpetrp AS TB1, ccpetrp_detalle AS TB2 WHERE TB1.consvigencia = TB2.consvigencia AND TB1.vigencia = TB2.vigencia AND TB1.tipo_mov = TB2.tipo_mov AND TB1.vigencia = '$vigencia_fiscal' AND TB2.seccion_presupuestal LIKE '%$seccion_presupuestal%' AND TB2.medio_pago LIKE '%$medio_pago%' AND TB2.codigo_vigenciag LIKE '%$vigencia_gasto%' AND TB2.fuente = '$fuente_f' AND TB2.bpim = '$bpim_inv' AND TB2.indicador_producto = '$programatico_inv' AND TB2.cuenta LIKE '$cuenta_f%' $critFecha GROUP BY TB1.consvigencia, TB1.vigencia, TB1.tipo_mov";/* echo $slqrRp."\ln"; */
            $resRp = mysqli_query($linkbd, $slqrRp);
            while($rowRp = mysqli_fetch_row($resRp)){
                $detalles_rp = [];
                $valor = 0;
                $valorRev = 0;

                $sql = "SELECT nombre FROM cuentasccpet WHERE codigo = '$rowRp[4]'";
                $row = mysqli_fetch_row(mysqli_query($linkbd, $sql));

                $nombreCuenta = $rowRp[4] . " - " . $row[0];


                array_push($detalles_rp, $rowRp[0]);
                array_push($detalles_rp, $rowRp[1]);
                array_push($detalles_rp, $rowRp[2]);
                array_push($detalles_rp, $rowRp[3]);
                array_push($detalles_rp, $nombreCuenta);
                array_push($detalles_rp, $rowRp[5]);

                if($rowRp[3] == '201'){
                    $valor = $rowRp[6];
                    $valorRev = 0;
                }else{
                    $valor = 0;
                    $valorRev = $rowRp[6];
                }
                $cdps = [];
                array_push($cdps, $rowRp[7]);
                array_push($detalles_rp, $valor);
                array_push($detalles_rp, $valorRev);
                array_push($detalles_rp, $cdps);

                $rps = [];
                $cxps = [];
                $cxpsN = [];
                $egresos = [];
                $egresosN = [];

                array_push($rps, $rowRp[0]);

                $sqlrCxp = "SELECT id_orden FROM tesoordenpago WHERE id_rp = $rowRp[0] AND vigencia = '$vigencia_fiscal'";
                $resCxp = mysqli_query($linkbd, $sqlrCxp);
                while($rowCxp = mysqli_fetch_row($resCxp)){
                    array_push($cxps, $rowCxp[0]);

                    $sqlrEgreso = "SELECT id_egreso FROM tesoegresos WHERE id_orden = '$rowCxp[0]'";
                    $resEgreso = mysqli_query($linkbd, $sqlrEgreso);
                    while($rowEgreso = mysqli_fetch_row($resEgreso)){
                        array_push($egresos, $rowEgreso[0]);
                    }
                }

                $sqlrCxpN = "SELECT nomina FROM hum_nom_cdp_rp WHERE rp = $rowRp[0] AND vigencia = '$vigencia_fiscal'";
                $resCxpN = mysqli_query($linkbd, $sqlrCxpN);
                while($rowCxpN = mysqli_fetch_row($resCxpN)){
                    array_push($cxpsN, $rowCxpN[0]);

                    $sqlrEgresoN = "SELECT id_egreso FROM tesoegresosnomina WHERE id_orden = '$rowCxpN[0]' ";
                    $resEgresoN = mysqli_query($linkbd, $sqlrEgresoN);
                    while($rowEgresoN = mysqli_fetch_row($resEgresoN)){
                        array_push($egresosN, $rowEgresoN[0]);
                    }
                }

                array_push($detalles_rp, $rps);
                array_push($detalles_rp, $cxps);
                array_push($detalles_rp, $cxpsN);
                array_push($detalles_rp, $egresos);
                array_push($detalles_rp, $egresosN);

                array_push($detalles, $detalles_rp);
            }
        }

        if($tipo_comprobante == 'obligacion'){

            $sqlrCxp = "SELECT TB1.id_orden, TB1.fecha, TB1.conceptorden, TB1.tipo_mov, TB2.cuentap, TB2.productoservicio, SUM(TB2.valor), TB1.id_rp, LEFT(TB2.productoservicio, 1) FROM tesoordenpago AS TB1, tesoordenpago_det AS TB2 WHERE TB1.id_orden = TB2.id_orden AND TB1.tipo_mov = TB2.tipo_mov AND TB2.seccion_presupuestal LIKE '%$seccion_presupuestal%' AND TB2.medio_pago LIKE '%$medio_pago%' AND TB2.codigo_vigenciag LIKE '%$vigencia_gasto%' AND TB2.fuente = '$fuente_f' AND TB2.bpim = '$bpim_inv' AND TB2.indicador_producto = '$programatico_inv' AND TB2.cuentap LIKE '$cuenta_f%' $critFecha GROUP BY TB1.id_orden, TB1.tipo_mov";/* echo $slqrRp."\ln"; */
            $resCxp = mysqli_query($linkbd, $sqlrCxp);
            while($rowCxp = mysqli_fetch_row($resCxp)){
                $detalles_cxp = [];
                $valor = 0;
                $valorRev = 0;

                $sql = "SELECT nombre FROM cuentasccpet WHERE codigo = '$rowCxp[4]'";
                $row = mysqli_fetch_row(mysqli_query($linkbd, $sql));

                $nombreCuenta = $rowCxp[4] . " - " . $row[0];

                if ($rowCxp[8] >= 5) {
                    //servicios

                    $sql = "SELECT titulo FROM ccpetservicios WHERE grupo = '$rowCxp[5]'";
                    $row = mysqli_fetch_row(mysqli_query($linkbd, $sql));
                }
                else {
                    ///bienes transportables.
                    $sql = "SELECT titulo FROM ccpetbienestransportables WHERE grupo = '$rowCxp[5]'";
                    $row = mysqli_fetch_row(mysqli_query($linkbd, $sql));
                }

                $cpc = $rowCxp[5] . " - " . $row[0];

                array_push($detalles_cxp, $rowCxp[0]);
                array_push($detalles_cxp, $rowCxp[1]);
                array_push($detalles_cxp, $rowCxp[2]);
                array_push($detalles_cxp, $rowCxp[3]);
                array_push($detalles_cxp, $nombreCuenta);
                array_push($detalles_cxp, $cpc);

                if($rowCxp[3] == '201'){
                    $valor = $rowCxp[6];
                    $valorRev = 0;
                }else{
                    $valor = 0;
                    $valorRev = $rowCxp[6];
                }
                array_push($detalles_cxp, $valor);
                array_push($detalles_cxp, $valorRev);

                $cdps = [];
                $sqlrCdp = "SELECT idcdp FROM ccpetrp WHERE consvigencia = $rowCxp[7] AND vigencia = '$vigencia_fiscal'";
                $resCdp = mysqli_query($linkbd, $sqlrCdp);
                while($rowCdp = mysqli_fetch_row($resCdp)){
                    array_push($cdps, $rowCdp[0]);
                }

                array_push($detalles_cxp, $cdps);

                $rps = [];
                $cxps = [];
                $cxpsN = [];
                $egresos = [];
                $egresosN = [];

                array_push($rps, $rowCxp[7]);

                array_push($cxps, $rowCxp[0]);

                $sqlrEgreso = "SELECT id_egreso FROM tesoegresos WHERE id_orden = '$rowCxp[0]'";
                $resEgreso = mysqli_query($linkbd, $sqlrEgreso);
                while($rowEgreso = mysqli_fetch_row($resEgreso)){
                    array_push($egresos, $rowEgreso[0]);
                }

                array_push($detalles_cxp, $rps);
                array_push($detalles_cxp, $cxps);
                array_push($detalles_cxp, $cxpsN);
                array_push($detalles_cxp, $egresos);
                array_push($detalles_cxp, $egresosN);

                array_push($detalles, $detalles_cxp);
            }

            $sqlrCxpN = "SELECT TB1.id_nom, TB1.fecha, TB2.cuenta, SUM(TB2.valor), TB3.rp FROM humnomina AS TB1, humnom_presupuestal AS TB2, hum_nom_cdp_rp AS TB3 WHERE TB1.id_nom = TB2.id_nom AND TB1.id_nom = TB3.nomina AND TB2.seccion_presupuestal LIKE '%$seccion_presupuestal%' AND TB2.medio_pago LIKE '%$medio_pago%' AND TB2.vigencia_gasto LIKE '%$vigencia_gasto%' AND TB2.fuente = '$fuente_f' AND TB2.bpin = '$bpim_inv' AND TB2.indicador = '$programatico_inv' AND TB2.cuenta LIKE '$cuenta_f%' AND TB1.estado != 'N' $critFecha GROUP BY TB1.id_nom ";
            $resCxpN = mysqli_query($linkbd, $sqlrCxpN);
            while($rowCxpN = mysqli_fetch_row($resCxpN)){

                $detalles_cxp_n = [];
                $valor = 0;
                $valorRev = 0;
                array_push($detalles_cxp_n, $rowCxpN[0]);
                array_push($detalles_cxp_n, $rowCxpN[1]);

                $cdps = [];
                $sqlrCdp = "SELECT idcdp, detalle FROM ccpetrp WHERE consvigencia = $rowCxpN[4] AND vigencia = '$vigencia_fiscal'";
                $resCdp = mysqli_query($linkbd, $sqlrCdp);
                $rowCdp = mysqli_fetch_row($resCdp);
                array_push($detalles_cxp_n, $rowCdp[1]);

                $sql = "SELECT nombre FROM cuentasccpet WHERE codigo = '$rowCxpN[2]'";
                $row = mysqli_fetch_row(mysqli_query($linkbd, $sql));

                $nombreCuenta = $rowCxpN[2] . " - " . $row[0];

                array_push($detalles_cxp_n, '201');
                array_push($detalles_cxp_n, $nombreCuenta);
                array_push($detalles_cxp_n, '');
                array_push($detalles_cxp_n, $rowCxpN[3]);
                array_push($detalles_cxp_n, '');

                array_push($cdps, $rowCdp[0]);

                array_push($detalles_cxp_n, $cdps);

                $rps = [];
                $cxps = [];
                $cxpsN = [];
                $egresos = [];
                $egresosN = [];

                array_push($rps, $rowCxpN[4]);

                array_push($cxpsN, $rowCxpN[0]);

                $sqlrEgresoN = "SELECT id_egreso FROM tesoegresosnomina WHERE id_orden = '$rowCxpN[0]' ";
                $resEgresoN = mysqli_query($linkbd, $sqlrEgresoN);
                while($rowEgresoN = mysqli_fetch_row($resEgresoN)){
                    array_push($egresosN, $rowEgresoN[0]);
                }

                array_push($detalles_cxp_n, $rps);
                array_push($detalles_cxp_n, $cxps);
                array_push($detalles_cxp_n, $cxpsN);
                array_push($detalles_cxp_n, $egresos);
                array_push($detalles_cxp_n, $egresosN);

                array_push($detalles, $detalles_cxp_n);
            }
        }

        if($tipo_comprobante == 'egreso'){

            $sqlrEgreso = "SELECT TB1.id_egreso, TB1.fecha, TB1.concepto, TB1.tipo_mov, TB2.cuentap, TB2.productoservicio, SUM(TB2.valor), TB3.id_rp, TB3.vigencia, TB1.id_orden, LEFT(TB2.productoservicio, 1) FROM tesoegresos AS TB1, tesoordenpago_det AS TB2, tesoordenpago AS TB3 WHERE TB1.id_orden = TB3.id_orden AND TB3.id_orden = TB2.id_orden AND TB3.tipo_mov = TB2.tipo_mov AND TB2.seccion_presupuestal LIKE '%$seccion_presupuestal%' AND TB2.medio_pago LIKE '%$medio_pago%' AND TB2.codigo_vigenciag LIKE '%$vigencia_gasto%' AND TB2.fuente = '$fuente_f' AND TB2.bpim = '$bpim_inv' AND TB2.indicador_producto = '$programatico_inv' AND TB2.cuentap LIKE '$cuenta_f%' $critFecha GROUP BY TB1.id_egreso, TB1.tipo_mov";
            $resEgreso = mysqli_query($linkbd, $sqlrEgreso);
            while($rowEgreso = mysqli_fetch_row($resEgreso)){
                $detalles_egreso = [];
                $valor = 0;
                $valorRev = 0;

                $sql = "SELECT nombre FROM cuentasccpet WHERE codigo = '$rowEgreso[4]'";
                $row = mysqli_fetch_row(mysqli_query($linkbd, $sql));

                $nombreCuenta = $rowEgreso[4] . " - " . $row[0];

                if ($rowEgreso[10] >= 5) {
                    //servicios

                    $sql = "SELECT titulo FROM ccpetservicios WHERE grupo = '$rowEgreso[5]'";
                    $row = mysqli_fetch_row(mysqli_query($linkbd, $sql));
                }
                else {
                    ///bienes transportables.
                    $sql = "SELECT titulo FROM ccpetbienestransportables WHERE grupo = '$rowEgreso[5]'";
                    $row = mysqli_fetch_row(mysqli_query($linkbd, $sql));
                }

                $cpc = $rowEgreso[5] . " - " . $row[0];

                array_push($detalles_egreso, $rowEgreso[0]);
                array_push($detalles_egreso, $rowEgreso[1]);
                array_push($detalles_egreso, $rowEgreso[2]);
                array_push($detalles_egreso, $rowEgreso[3]);
                array_push($detalles_egreso, $nombreCuenta);
                array_push($detalles_egreso, $cpc);

                if($rowEgreso[3] == '201'){
                    $valor = $rowEgreso[6];
                    $valorRev = 0;
                }else{
                    $valor = 0;
                    $valorRev = $rowEgreso[6];
                }
                array_push($detalles_egreso, $valor);
                array_push($detalles_egreso, $valorRev);

                $cdps = [];
                $sqlrCdp = "SELECT idcdp FROM ccpetrp WHERE consvigencia = $rowEgreso[7] AND vigencia = '$vigencia_fiscal'";
                $resCdp = mysqli_query($linkbd, $sqlrCdp);
                while($rowCdp = mysqli_fetch_row($resCdp)){
                    array_push($cdps, $rowCdp[0]);
                }

                array_push($detalles_egreso, $cdps);

                $rps = [];
                $cxps = [];
                $cxpsN = [];
                $egresos = [];
                $egresosN = [];

                array_push($rps, $rowEgreso[7]);

                array_push($cxps, $rowEgreso[9]);

                array_push($egresos, $rowEgreso[0]);


                array_push($detalles_egreso, $rps);
                array_push($detalles_egreso, $cxps);
                array_push($detalles_egreso, $cxpsN);
                array_push($detalles_egreso, $egresos);
                array_push($detalles_egreso, $egresosN);

                array_push($detalles, $detalles_egreso);
            }


            $sqlrEgresoN = "SELECT TB1.id_egreso, TB1.fecha, 'Egreso nomina', TB1.tipo_mov, TB2.cuentap, TB2.productoservicio, SUM(TB2.valordevengado), TB1.id_orden, LEFT(TB2.productoservicio, 1) FROM tesoegresosnomina AS TB1, tesoegresosnomina_det AS TB2 WHERE TB1.id_egreso = TB2.id_egreso AND TB1.tipo_mov = TB2.tipo_mov AND TB2.seccion_presupuestal LIKE '%$seccion_presupuestal%' AND TB2.medio_pago LIKE '%$medio_pago%' AND TB2.vigencia_gasto LIKE '%$vigencia_gasto%' AND TB2.fuente = '$fuente_f' AND TB2.bpin = '$bpim_inv' AND TB2.indicador_producto = '$programatico_inv' AND TB2.cuentap LIKE '$cuenta_f%' $critFecha GROUP BY TB1.id_egreso, TB1.tipo_mov";
            $resEgresoN = mysqli_query($linkbd, $sqlrEgresoN);
            while($rowEgresoN = mysqli_fetch_row($resEgresoN)){

                $detalles_egreso_n = [];
                $valor = 0;
                $valorRev = 0;

                $sql = "SELECT nombre FROM cuentasccpet WHERE codigo = '$rowEgresoN[4]'";
                $row = mysqli_fetch_row(mysqli_query($linkbd, $sql));

                $nombreCuenta = $rowEgresoN[4] . " - " . $row[0];

                if ($rowEgresoN[8] >= 5) {
                    //servicios

                    $sql = "SELECT titulo FROM ccpetservicios WHERE grupo = '$rowEgresoN[5]'";
                    $row = mysqli_fetch_row(mysqli_query($linkbd, $sql));
                }
                else {
                    ///bienes transportables.
                    $sql = "SELECT titulo FROM ccpetbienestransportables WHERE grupo = '$rowEgresoN[5]'";
                    $row = mysqli_fetch_row(mysqli_query($linkbd, $sql));
                }

                $cpc = $rowEgresoN[5] . " - " . $row[0];

                array_push($detalles_egreso_n, $rowEgresoN[0]);
                array_push($detalles_egreso_n, $rowEgresoN[1]);
                array_push($detalles_egreso_n, $rowEgresoN[2]);
                array_push($detalles_egreso_n, $rowEgresoN[3]);
                array_push($detalles_egreso_n, $nombreCuenta);
                array_push($detalles_egreso_n, $cpc);

                if($rowEgresoN[3] == '201'){
                    $valor = $rowEgresoN[6];
                    $valorRev = 0;
                }else{
                    $valor = 0;
                    $valorRev = $rowEgresoN[6];
                }
                array_push($detalles_egreso_n, $valor);
                array_push($detalles_egreso_n, $valorRev);

                $cdps = [];
                $rps = [];

                $sqlrCdp = "SELECT cdp, rp FROM hum_nom_cdp_rp WHERE nomina = $rowEgresoN[7] AND vigencia = '$vigencia_fiscal'";
                $resCdp = mysqli_query($linkbd, $sqlrCdp);
                while($rowCdp = mysqli_fetch_row($resCdp)){
                    array_push($cdps, $rowCdp[0]);
                    array_push($rps, $rowCdp[1]);
                }

                array_push($detalles_egreso_n, $cdps);


                $cxps = [];
                $cxpsN = [];
                $egresos = [];
                $egresosN = [];

                array_push($cxpsN, $rowEgresoN[7]);

                array_push($egresosN, $rowEgresoN[0]);


                array_push($detalles_egreso_n, $rps);
                array_push($detalles_egreso_n, $cxps);
                array_push($detalles_egreso_n, $cxpsN);
                array_push($detalles_egreso_n, $egresos);
                array_push($detalles_egreso_n, $egresosN);

                array_push($detalles, $detalles_egreso_n);
            }
        }

        $out['detalles'] = $detalles;
    }


    header("Content-type: application/json");
    echo json_encode($out);
    die();
