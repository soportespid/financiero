
/* const DIR_ORIGIN = window.location.origin + '/';
const DIR_PATHNAME = window.location.pathname.split("/", 2)[1] + '/';
const DIR_FULL = DIR_ORIGIN + '' + DIR_PATHNAME; */

const URL = 'presupuesto_ccpet/AuxiliarGastos/ccp-auxiliarporcomprobantedata.php';

const app = Vue.createApp({
    data() {
      return {
        detalles: [],
        numActo: '',
        loading: false,
        tipo_comp: '',
        fechaIni: '',
        fechaFin: '',
        seccion_presupuestal: '',
        medio_pago: '',
        vigencia_gasto: '',
        vigencia_fiscal: '',
        fuente_f: '',
        cuenta_f: '',
        bpim_inv: '',
        programatico_inv: '',
        nombreCuenta: '',
      }
    },

    mounted() {
		  this.loading = false;
          this.valoresIniciales();
	  },

    computed: {
      
    },

    methods: {
        
        getParametros(name){
            //Captura parametros
            name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
            var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
            results = regex.exec(location.search);
            return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
        },

        async valoresIniciales() {

            this.loading = true;

            this.tipo_comp = this.getParametros('tipo_comprobante');
            this.fechaIni = this.getParametros('fechaIni');
            this.fechaFin = this.getParametros('fechaFin');
            this.seccion_presupuestal = this.getParametros('seccion_presupuestal');
            this.medio_pago = this.getParametros('medio_pago');
            this.vigencia_gasto = this.getParametros('vigencia_gasto');
            this.vigencia_fiscal = this.getParametros('vigencia_fiscal');
            this.fuente_f = this.getParametros('fuente');
            this.bpim_inv = this.getParametros('bpim_inv');
            this.cuenta_f = this.getParametros('cuenta_f');
            this.programatico_inv = this.getParametros('programatico_inv');

            if(this.seccion_presupuestal == '-1'){
                this.seccion_presupuestal = 'todos';
            }
            if(this.medio_pago == '-1'){
                this.medio_pago = 'todos';
            }
            if(this.vigencia_gasto == '-1'){
                this.vigencia_gasto = 'todos';
            }

            let fechaInicial = '';
            let fechaFinal = '';

            if(this.fechaIni && this.fechaFin){
                let arrFechaInicial = this.fechaIni.split("/");
                fechaInicial = `${arrFechaInicial[2]}-${arrFechaInicial[1]}-${arrFechaInicial[0]}`;
                this.vigencia_fiscal = arrFechaInicial[2];
                let arrFechaFinal = this.fechaFin.split("/");
                fechaFinal = `${arrFechaFinal[2]}-${arrFechaFinal[1]}-${arrFechaFinal[0]}`;
            }else{
                fechaInicial = '';
                fechaFinal = '';
            }

            let formData = new FormData();
            formData.append("tipo_comp", this.tipo_comp);
            formData.append("fechaIni", fechaInicial);
            formData.append("fechaFin", fechaFinal);
            formData.append("seccion_presupuestal", this.seccion_presupuestal);
            formData.append("medio_pago", this.medio_pago);
            formData.append("vigencia_gasto", this.vigencia_gasto);
            formData.append("vigencia_fiscal", this.vigencia_fiscal);
            formData.append("fuente_f", this.fuente_f);
            formData.append("bpim_inv", this.bpim_inv);
            formData.append("cuenta_f", this.cuenta_f);
            formData.append("programatico_inv", this.programatico_inv);
            
            await axios.post(URL, formData)
            .then((response) => {
                console.log(response.data);
                this.detalles = response.data.detalles;
                
            }).catch((error) => {
                this.error = true;
                console.log(error)
            }).finally(() => {
                this.loading = false;
            });     
        },

        redirectCdp(consvigencia){
            const datos = "is="+consvigencia+"&vig="+this.vigencia_fiscal;
            mypop=window.open("ccp-cdpVisualizar.php?" + datos,'','');
            mypop.focus();
        },

        redirectRp(consvigencia){
            const datos = "is="+consvigencia+"&vig="+this.vigencia_fiscal;
            mypop=window.open("ccp-rpVisualizar.php?" + datos,'','');
            mypop.focus();
        },

        redirectCxp(id_orden){
            const datos = "idop="+id_orden;
            mypop=window.open("teso-egresoverccpet.php?" + datos,'','');
            mypop.focus();
        },

        redirectCxpN(id_orden){
            const datos = "idnomi="+id_orden;
            mypop=window.open("hum-liquidarnominamirar.php?" + datos,'','');
            mypop.focus();
        },

        redirectEgreso(egreso){
            const datos = "idegre="+egreso;
            mypop=window.open("teso-girarchequesver-ccpet.php?" + datos,'','');
            mypop.focus();
        },

        redirectEgresoN(egreso){
            const datos = "idegre="+egreso;
            mypop=window.open("teso-pagonominaver.php?" + datos,'','');
            mypop.focus();
        },

        toFormData(obj){
            var form_data = new FormData();
            for(var key in obj){
                form_data.append(key, obj[key]);
            }
            return form_data;
        },

        formatonumero: function(valor){
            return new Intl.NumberFormat("en-US", {style: "currency", currency: "USD"}).format(valor);
        },

        excel: function() {

            if (this.detalles != '') {

                document.form2.action="ccp-excelAuxiliarComprobante.php";
				document.form2.target="_BLANK";
				document.form2.submit(); 
            }
            else {
                Swal.fire("Faltan datos", "warning");
            }
        },
    }
})

app.mount('#myapp')