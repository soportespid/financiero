<?php
	require_once '../../comun.inc';
    require '../../funciones.inc';
    require '../../conversor.php';
    require '../../funcionesSP.inc.php';
    session_start();
    /*ini_set('display_errors', '1');
    ini_set('display_startup_errors', '1');
    error_reporting(E_ALL);*/

	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");

    $out = array('error' => false);

    $action = "show";

    if(isset($_GET['action'])){
        $action = $_GET['action'];
    }
    function validarTotales($detalles){
        $arrCPC = array_values(array_filter($detalles,function($e){return $e[7]!="";}));
        $arrDet = [];
        for ($i=0; $i < count($arrCPC); $i++) {
            $cpc = $arrCPC[$i];
            $totalDet = count($arrDet);
            if($totalDet > 0){
                $flag = false;
                for ($j=0; $j < $totalDet ; $j++) {
                    $det = $arrDet[$j];
                    if($det['cuenta'] == $cpc[3] && $det['id'] == $cpc[2]){
                        $arrDet[$j]['total']+=$cpc[14];
                        $flag = true;
                        break;
                    }
                }
                if(!$flag){
                    array_push($arrDet,array("id"=>$cpc[2],"cuenta"=>$cpc[3],"total"=>$cpc[14]));
                }
            }else{
                array_push($arrDet,array("id"=>$cpc[2],"cuenta"=>$cpc[3],"total"=>$cpc[14]));
            }
        }
        $total = count($detalles);
        $totalArrDet = count($arrDet);
        for ($i=0; $i < $total; $i++) {
            $cpc = substr($detalles[$i][7],0,1);
            for ($j=0; $j < $totalArrDet ; $j++) {
                if($detalles[$i][3] == $arrDet[$j]['cuenta'] && $detalles[$i][2] == $arrDet[$j]['id']
                && !is_numeric($detalles[$i][7])){
					$valor = $detalles[$i][11] == 0 ? 1 : $detalles[$i][11];
                    $detalles[$i][14] = $arrDet[$j]['total'];
                    $detalles[$i][15] = $detalles[$i][11] - $detalles[$i][14];
                    $detalles[$i][16] = round(($detalles[$i][14]/$valor) * 100,2);
                    break;
                }
            }
        }
        return $detalles;
    }
    function selectNombreClasificador(string $codigo, string $cuenta){
        $linkbd = conectar_v7();
        $linkbd -> set_charset("utf8");
        $sql = "SELECT clasificadores FROM ccpetprogramarclasificadores WHERE cuenta = '$cuenta'";
        $request = mysqli_query($linkbd,$sql)->fetch_assoc();
        $strNombre ="";
        $clasificador = 0;
        if(!empty($request)){
            $clasificador =$request['clasificadores'];
            if($clasificador == 4){
                $sql = "SELECT  coalesce(nombre,'') as nombre FROM ccpet_catalogocomp_ingresos WHERE codigo='$codigo'";
                $request = mysqli_query($linkbd,$sql)->fetch_assoc();
                $strNombre = $request['nombre'];
            }else if($clasificador == 5 ){
                $sql = "SELECT  coalesce(nombre,'') as nombre FROM ccpet_catalogo_superavit WHERE codigo='$codigo'";
                $request = mysqli_query($linkbd,$sql)->fetch_assoc();
                $strNombre = $request['nombre'];
            }else if($clasificador == 2){
                $sql = "SELECT coalesce(titulo,'') as nombre FROM ccpetbienestransportables
                WHERE version = (SELECT MAX(version) FROM ccpetbienestransportables) AND LENGTH(grupo) = 7 AND grupo = '$codigo'";
                $request = mysqli_query($linkbd,$sql)->fetch_assoc();
                $strNombre = $request['nombre'];
            }else if($clasificador == 3){
                $sql = "SELECT coalesce(titulo,'') as nombre FROM ccpetservicios
                WHERE version = (SELECT MAX(version) FROM ccpetbienestransportables) AND LENGTH(grupo) = 5 AND grupo = '$codigo'";
                $request = mysqli_query($linkbd,$sql)->fetch_assoc();
                $strNombre = $request['nombre'];
            }
        }
        return $strNombre;
    }
    function validaArray($arrayNuevo, $arrayBase)
    {
        for ($i=0; $i < count($arrayBase); $i++) {

            if (
                $arrayNuevo[0] == $arrayBase[$i][3] &&
                $arrayNuevo[1] == $arrayBase[$i][2] &&
                $arrayNuevo[2] == $arrayBase[$i][1] &&
                $arrayNuevo[3] == $arrayBase[$i][6] &&
                $arrayNuevo[4] == $arrayBase[$i][5] &&
                $arrayNuevo[6] == $arrayBase[$i][19]
            ) {
                return $i;
            }
        }

        return -1;
    }

    function validaArraySuperavit($arrayNuevo, $arrayBase) {

        for ($i=0; $i < count($arrayBase); $i++) {

            if (
                $arrayNuevo[0] == $arrayBase[$i][3] &&
                $arrayNuevo[1] == $arrayBase[$i][5] &&
                $arrayNuevo[2] == $arrayBase[$i][1] &&
                $arrayNuevo[3] == $arrayBase[$i][7]
            ) {
                return $i;
            }
        }

        return -1;
    }

    function validaArrayConsultaRecaudos($arrayNuevo, $arrayBase) {

        for ($i=0; $i < count($arrayBase); $i++) {
            if (
                $arrayNuevo[0] == $arrayBase[$i][3] &&
                $arrayNuevo[1] == $arrayBase[$i][5] &&
                $arrayNuevo[2] == $arrayBase[$i][2] &&
                $arrayNuevo[3] == $arrayBase[$i][6] &&
                $arrayNuevo[4] == $arrayBase[$i][1] &&
                $arrayNuevo[6] == $arrayBase[$i][7] &&
                $arrayNuevo[7] == $arrayBase[$i][19]
            ) {
                return $i;
            }
        }

        return -1;
    }

    function llenaBase($arrayBase, $index) {

        for ($i=0; $i < count($arrayBase); $i++) {

            $arrayBase[$i][$index] = "0";
        }

        return $arrayBase;
    }

    function encontrarFuente($codigoFuente) {
        $linkbd = conectar_v7();
        $linkbd -> set_charset("utf8");

        $sql = "SELECT nombre FROM ccpet_fuentes_cuipo WHERE codigo_fuente = '$codigoFuente'";
        $row = mysqli_fetch_row(mysqli_query($linkbd, $sql));

        return $row[0];
    }

    if ($action == "datosIniciales") {

        $maxVersion = ultimaVersionIngresosCCPET();

        $cuentaMaxLargo = '';

        $seccionPresupuestal = array();

        $sqlr = "SELECT id_seccion_presupuestal, nombre FROM pptoseccion_presupuestal WHERE estado='S' ";
        $res = mysqli_query($linkbd, $sqlr);
        while ($row = mysqli_fetch_row($res)) {

            array_push($seccionPresupuestal, $row);
        }

        $vigenciaGasto = array();

        $sqlr = "SELECT codigo, nombre FROM ccpet_vigenciadelgasto ";
        $res = mysqli_query($linkbd, $sqlr);
        while ($row = mysqli_fetch_row($res)) {

            array_push($vigenciaGasto, $row);
        }

        $cuentas = array();

        $sqlCuentasIngresos = "SELECT codigo, nombre, tipo FROM cuentasingresosccpet WHERE municipio=1 AND version='$maxVersion' ORDER BY id ASC";
        $resCuentasIngresos = mysqli_query($linkbd, $sqlCuentasIngresos);
        while ($rowCuentasIngresos = mysqli_fetch_row($resCuentasIngresos)) {

            array_push($cuentas, $rowCuentasIngresos);
        }

        $sqlrMaxLargo = "SELECT codigo FROM cuentasingresosccpet WHERE LENGTH(codigo) = (SELECT MAX(LENGTH(codigo)) FROM cuentasingresosccpet WHERE version = '".$maxVersion."') AND version = '".$maxVersion."' LIMIT 1";
        $respMaxLargo = mysqli_query($linkbd, $sqlrMaxLargo);
        $rowMaxLargo = mysqli_fetch_row($respMaxLargo);
        $cuentaMaxLargo = $rowMaxLargo[0];

        $out['cuentas'] = $cuentas;
        $out['secPresupuestal'] = $seccionPresupuestal;
        $out['vigGasto'] = $vigenciaGasto;
        $out['cuentaMaxLargo'] = $cuentaMaxLargo;
    }

    if ($action == 'filtrarCuenta') {

        $busqueda = $_POST['keywordCuenta'];

        $cuentas = array();

        $sqlCuentasIngresos = "SELECT codigo, nombre, tipo FROM cuentasingresosccpet WHERE municipio=1 AND version='$maxVersion' AND concat_ws('', codigo, nombre) LIKE '$busqueda%' ORDER BY id ASC";
        $resCuentasIngresos = mysqli_query($linkbd, $sqlCuentasIngresos);
        while ($rowCuentasIngresos = mysqli_fetch_row($resCuentasIngresos)) {

            array_push($cuentas, $rowCuentasIngresos);
        }

        $out['cuentas'] = $cuentas;
    }

    if ($action == "datosFormulario") {

        $detalles = [];

        $maxVersion = ultimaVersionIngresosCCPET();

        preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_GET["fechaIni"], $fecha);
        $fechaIni = $fecha[3]."-".$fecha[2]."-".$fecha[1];

        $vigenciaIni = $fecha[3];

        preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_GET["fechaFin"], $fecha);
        $fechaFin = $fecha[3]."-".$fecha[2]."-".$fecha[1];

        $vigenciaFin = $fecha[3];

        $secPresu = "";
        $medioPago = "";
        $vigGasto = "";

        if ($_GET['secPresu'] == '') {
            $secPresu = "%";
        }
        else {
            $secPresu = $_GET['secPresu'];
        }

        if ($_GET['medioPago'] == '') {
            $medioPago = "%";
        }
        else {
            $medioPago = $_GET['medioPago'];
        }

        if ($_GET['vigGasto'] == '') {
            $vigGasto = "%";
        }
        else {
            $vigGasto = $_GET['vigGasto'];
        }

        $tamCuentas = count($_POST["cuentas"]);

        for($x = 0; $x < $tamCuentas; $x++){

            $xy = 0;
            $tamFuentes = count($_POST["fuentes"]);
            do{

                if ($_POST["cuentas"][$x][2] == 'C') {

                    $datosBase = array();
                    $sqlPresupuestoInicial = "SELECT fuente, seccion_presupuestal, vigencia_gasto, medio_pago, SUM(valor) FROM ccpetinicialing WHERE cuenta LIKE '".$_POST["cuentas"][$x][0]."' AND seccion_presupuestal LIKE '$secPresu' AND vigencia_gasto LIKE '$vigGasto' AND medio_pago LIKE '$medioPago' AND (vigencia = '$vigenciaIni' OR vigencia = '$vigenciaFin') AND valor <> 0 AND fuente LIKE '".$_POST["fuentes"][$xy]."%' GROUP BY fuente, seccion_presupuestal, vigencia_gasto, medio_pago";
                    $resPresupuestoInicial = mysqli_query($linkbd, $sqlPresupuestoInicial);
                    while ($rowPresupuestoInicial = mysqli_fetch_row($resPresupuestoInicial)) {

                        $datos2 = array();

                        if ($rowPresupuestoInicial) {

                            array_push($datos2, 'C');
                            array_push($datos2, $rowPresupuestoInicial[2]); //vigencia gasto
                            array_push($datos2, $rowPresupuestoInicial[1]); //seccion presupuestal
                            array_push($datos2, $_POST["cuentas"][$x][0]); //cuenta
                            array_push($datos2, $_POST["cuentas"][$x][1]); //Nombre cuenta
                            array_push($datos2, $rowPresupuestoInicial[0]); //fuente
                            array_push($datos2, $rowPresupuestoInicial[3]); //medio pago
                            array_push($datos2, ""); //CPC
                            array_push($datos2, $rowPresupuestoInicial[4]); //Valor inicial
                            $datos2[19] ="";
                            $datos2[20]="";
                            array_push($datosBase, $datos2);
                        }
                    }

                    //ADICIÓN

                    $adicion = array();

                    $sqlAdiciones = "SELECT fuente, seccion_presupuestal, codigo_vigenciag, medio_pago, SUM(valor),clasificador
                    FROM ccpet_adiciones
                    WHERE cuenta LIKE '".$_POST["cuentas"][$x][0]."' AND tipo_cuenta = 'I' AND seccion_presupuestal LIKE '$secPresu'
                    AND medio_pago LIKE '$medioPago' AND codigo_vigenciag LIKE '$vigGasto' AND estado = 'S' AND fecha BETWEEN '$fechaIni' AND '$fechaFin'
                    AND fuente LIKE '".$_POST["fuentes"][$xy]."%' GROUP BY fuente, seccion_presupuestal, codigo_vigenciag, medio_pago, clasificador";
                    $resAdiciones = mysqli_query($linkbd, $sqlAdiciones);
                    while ($rowAdiciones = mysqli_fetch_row($resAdiciones)) {

                        $datos2 = array();

                        if ($rowAdiciones) {

                            array_push($datos2, $_POST["cuentas"][$x][0]); //cuenta
                            array_push($datos2, $rowAdiciones[1]); //seccion presupuestal
                            array_push($datos2, $rowAdiciones[2]); //vigencia gasto
                            array_push($datos2, $rowAdiciones[3]); //medio pago
                            array_push($datos2, $rowAdiciones[0]); //fuente
                            array_push($datos2, $rowAdiciones[4]); //valor
                            array_push($datos2, $rowAdiciones[5]); //Clasificador superavit
                            array_push($adicion, $datos2);
                        }
                    }

                    $datosBase = llenaBase($datosBase, 9);

                    for ($i=0; $i < count($adicion); $i++) {

                        $result = validaArray($adicion[$i], $datosBase);

                        if ($result == -1) {

                            $prueba = array();

                            array_push($prueba, 'C');
                            array_push($prueba, $adicion[$i][2]); //vigencia gasto
                            array_push($prueba, $adicion[$i][1]); //seccion presupuestal
                            array_push($prueba, $_POST["cuentas"][$x][0]); //cuenta
                            array_push($prueba, $_POST["cuentas"][$x][1]); //Nombre cuenta
                            array_push($prueba, $adicion[$i][4]); //fuente
                            array_push($prueba, $adicion[$i][3]); //medio pago
                            array_push($prueba, ""); //CPC
                            array_push($prueba, "0"); //Valor inicial
                            array_push($prueba, $adicion[$i][5]); //adicion
                            $prueba[19]=$adicion[$i][6];
                            $prueba[20]=selectNombreClasificador($adicion[$i][6],$_POST["cuentas"][$x][0]);
                            array_push($datosBase, $prueba);
                        }
                        else {
                            $datosBase[$result][9] = $adicion[$i][5];
                        }
                    }

                    //Reducción
                    $reduccion = array();

                    $sqlReduccion = "SELECT fuente, seccion_presupuestal, codigo_vigenciag, medio_pago, SUM(valor) FROM ccpet_reducciones WHERE cuenta LIKE '".$_POST["cuentas"][$x][0]."' AND tipo_cuenta = 'I' AND seccion_presupuestal LIKE '$secPresu' AND medio_pago LIKE '$medioPago' AND codigo_vigenciag LIKE '$vigGasto' AND estado = 'S' AND fecha BETWEEN '$fechaIni' AND '$fechaFin' AND fuente LIKE '".$_POST["fuentes"][$xy]."%' GROUP BY fuente, seccion_presupuestal, codigo_vigenciag, medio_pago";
                    $resReduccion = mysqli_query($linkbd, $sqlReduccion);
                    while ($rowReduccion = mysqli_fetch_row($resReduccion)) {

                        $datos2 = array();

                        if ($rowReduccion) {
                            array_push($datos2, $_POST["cuentas"][$x][0]); //cuenta
                            array_push($datos2, $rowReduccion[1]); //seccion presupuestal
                            array_push($datos2, $rowReduccion[2]); //vigencia gasto
                            array_push($datos2, $rowReduccion[3]); //medio pago
                            array_push($datos2, $rowReduccion[0]); //fuente
                            array_push($datos2, $rowReduccion[4]); //valor

                            array_push($reduccion, $datos2);
                        }
                    }

                    $datosBase = llenaBase($datosBase, 10);

                    for ($i=0; $i < count($reduccion); $i++) {

                        $result = validaArray($reduccion[$i], $datosBase);

                        if ($result == -1) {

                            $prueba = array();

                            array_push($prueba, 'C');
                            array_push($prueba, $reduccion[$i][2]); //vigencia gasto
                            array_push($prueba, $reduccion[$i][1]); //seccion presupuestal
                            array_push($prueba, $_POST["cuentas"][$x][0]); //cuenta
                            array_push($prueba, $_POST["cuentas"][$x][1]); //Nombre cuenta
                            array_push($prueba, $reduccion[$i][4]); //fuente
                            array_push($prueba, $reduccion[$i][3]); //medio pago
                            array_push($prueba, ""); //CPC
                            array_push($prueba, "0"); //Valor inicial
                            array_push($prueba, "0"); //adicion
                            array_push($prueba, $reduccion[$i][5]); //reduccion
                            array_push($datosBase, $prueba);
                        }
                        else {
                            $datosBase[$result][10] = $reduccion[$i][5];
                        }
                    }

                    //Definitivo
                    for ($i=0; $i < count($datosBase); $i++) {

                        $datosBase[$i][11] = $datosBase[$i][8] + $datosBase[$i][9] - $datosBase[$i][10];
                    }

                    //Reservas
                    $reservas = array();

                    $queryReservas = "SELECT tsad.fuente, tsad.vigencia_gasto, tsad.clasificacion, SUM(tsad.valor), tsad.clasificador FROM tesoreservas AS tsa, tesoreservas_det AS tsad WHERE tsa.estado = 'S' AND tsa.id = tsad.id_reserva AND tsa.fecha BETWEEN '$fechaIni' AND '$fechaFin' AND tsad.rubro LIKE '".$_POST["cuentas"][$x][0]."' AND tsad.fuente LIKE '".$_POST["fuentes"][$xy]."%' AND tsad.clasificacion LIKE '$secPresu' AND tsad.clasificador LIKE '$medioPago' AND tsad.codigo_vigenciag LIKE '$vigGasto' GROUP BY tsad.fuente, tsad.vigencia_gasto";
                    $resReservas = mysqli_query($linkbd, $queryReservas);
                    while ($rowReservas = mysqli_fetch_row($resReservas)) {

                        $datos2 = array();

                        if ($rowReservas) {

                            /* array_push($datos2, $_POST["cuentas"][$x][0]); //cuenta
                            array_push($datos2, $rowReservas[0]); //fuente
                            array_push($datos2, $rowReservas[1]); //vigencia gasto
                            if ($rowReservas[2] == '0') {
                                array_push($datos2, ""); //cpc
                            }
                            else {
                                array_push($datos2, $rowReservas[2]); //cpc
                            }
                            array_push($datos2, $rowReservas[3]); //valor
                            array_push($reservas, $datos2); */

                            array_push($datos2, $_POST["cuentas"][$x][0]); //cuenta
                            array_push($datos2, $rowReservas[0]); //fuente
                            array_push($datos2, $rowReservas[2]); //sec presu
                            array_push($datos2, $rowReservas[4]); //medio pago
                            array_push($datos2, $rowReservas[1]); //vig gasto
                            array_push($datos2, $rowReservas[3]); //valor
                            array_push($datos2, ''); //cpc

                            array_push($reservas, $datos2);
                        }
                    }

                    $datosBase = llenaBase($datosBase, 12);

                    for ($i=0; $i < count($reservas); $i++) {

                        $result = validaArraySuperavit($reservas[$i], $datosBase);

                        if ($result == -1) {

                            $prueba = array();

                            array_push($prueba, 'C');
                            array_push($prueba, $reservas[$i][4]); //vigencia gasto
                            array_push($prueba, $reservas[$i][2]); //seccion presupuestal
                            array_push($prueba, $_POST["cuentas"][$x][0]); //cuenta
                            array_push($prueba, $_POST["cuentas"][$x][1]); //Nombre cuenta
                            array_push($prueba, $reservas[$i][1]); //fuente
                            array_push($prueba, $reservas[$i][3]); //medio pago
                            array_push($prueba, $reservas[$i][6]); //CPC
                            array_push($prueba, "0"); //Valor inicial
                            array_push($prueba, "0"); //adicion
                            array_push($prueba, "0"); //reduccion
                            array_push($prueba, "0"); //definitivo
                            array_push($prueba, "0"); //reservas
                            array_push($prueba, $reservas[$i][5]); //recaudo consulta

                            array_push($datosBase, $prueba);
                        }
                        else {
                            $datosBase[$result][13] = $reservas[$i][4];
                        }
                    }

                    //Recaudos de consulta

                    $consulta = array();
                    $datosBase = llenaBase($datosBase, 13);

                    //pptorecibocajappto
                    $sqlReciboCajaPpto = "SELECT prc.fuente, prc.seccion_presupuestal, prc.medio_pago, prc.vigencia_gasto, SUM(prc.valor), prc.productoservicio FROM pptorecibocajappto prc,tesoreciboscaja trc WHERE prc.cuenta LIKE '".$_POST["cuentas"][$x][0]."' AND (prc.vigencia = $vigenciaIni OR prc.vigencia = $vigenciaFin) AND trc.id_recibos=prc.idrecibo AND NOT(trc.estado='N' OR trc.estado='R') AND NOT(prc.tipo='R') AND trc.fecha between '$fechaIni' AND '$fechaFin' AND prc.seccion_presupuestal LIKE '$secPresu' AND prc.medio_pago LIKE '$medioPago' AND prc.vigencia_gasto LIKE '$vigGasto' AND prc.fuente LIKE '".$_POST["fuentes"][$xy]."%' GROUP BY prc.fuente, prc.seccion_presupuestal, prc.medio_pago, prc.vigencia_gasto, prc.productoservicio";
                    $resReciboCajaPpto = mysqli_query($linkbd, $sqlReciboCajaPpto);
                    while ($rowReciboCajaPpto = mysqli_fetch_row($resReciboCajaPpto)) {

                        $datos2 = array();

                        if ($rowReciboCajaPpto) {

                            array_push($datos2, $_POST["cuentas"][$x][0]); //cuenta
                            array_push($datos2, $rowReciboCajaPpto[0]); //fuente
                            array_push($datos2, $rowReciboCajaPpto[1]); //sec presu
                            array_push($datos2, $rowReciboCajaPpto[2]); //medio pago
                            array_push($datos2, $rowReciboCajaPpto[3]); //vig gasto
                            array_push($datos2, $rowReciboCajaPpto[4]); //valor
                            array_push($datos2, $rowReciboCajaPpto[5]); //cpc

                            array_push($consulta, $datos2);
                        }
                    }

                    for ($i=0; $i < count($consulta); $i++) {

                        $result = validaArrayConsultaRecaudos($consulta[$i], $datosBase);

                        if ($result == -1) {
                            $prueba = array();

                            array_push($prueba, 'C');
                            array_push($prueba, $consulta[$i][4]); //vigencia gasto
                            array_push($prueba, $consulta[$i][2]); //seccion presupuestal
                            array_push($prueba, $_POST["cuentas"][$x][0]); //cuenta
                            array_push($prueba, $_POST["cuentas"][$x][1]); //Nombre cuenta
                            array_push($prueba, $consulta[$i][1]); //fuente
                            array_push($prueba, $consulta[$i][3]); //medio pago
                            array_push($prueba, $consulta[$i][6]); //CPC
                            array_push($prueba, "0"); //Valor inicial
                            array_push($prueba, "0"); //adicion
                            array_push($prueba, "0"); //reduccion
                            array_push($prueba, "0"); //definitivo
                            array_push($prueba, "0"); //reservas
                            array_push($prueba, $consulta[$i][5]); //recaudo consulta

                            array_push($datosBase, $prueba);
                        }
                        else {
                            $datosBase[$result][13] = $consulta[$i][5]; //valor
                        }
                    }


                    $consulta = array();

                    //pptosinrecibocajappto
                    $sqlSinReciboCajaPpto = "SELECT psrc.fuente, psrc.seccion_presupuestal, psrc.medio_pago, psrc.vigencia_gasto, SUM(psrc.valor), psrc.productoservicio FROM pptosinrecibocajappto psrc,tesosinreciboscaja tsrc WHERE psrc.cuenta LIKE '".$_POST["cuentas"][$x][0]."' AND (psrc.vigencia=$vigenciaIni OR psrc.vigencia = '$vigenciaFin') AND tsrc.id_recibos=psrc.idrecibo AND NOT(tsrc.estado='N' OR tsrc.estado='R') AND tsrc.fecha between '$fechaIni' AND '$fechaFin' AND psrc.medio_pago LIKE '$medioPago' AND psrc.seccion_presupuestal LIKE '$secPresu' AND psrc.vigencia_gasto LIKE '$vigGasto' AND psrc.fuente LIKE '".$_POST["fuentes"][$xy]."%' GROUP BY psrc.fuente, psrc.seccion_presupuestal, psrc.medio_pago, psrc.vigencia_gasto, psrc.productoservicio";
                    $resSinReciboCajaPpto = mysqli_query($linkbd, $sqlSinReciboCajaPpto);
                    while ($rowSinReciboCajaPpto = mysqli_fetch_row($resSinReciboCajaPpto)) {

                        $datos2 = array();

                        if ($rowSinReciboCajaPpto) {

                            array_push($datos2, $_POST["cuentas"][$x][0]); //cuenta
                            array_push($datos2, $rowSinReciboCajaPpto[0]); //fuente
                            array_push($datos2, $rowSinReciboCajaPpto[1]); //sec presu
                            array_push($datos2, $rowSinReciboCajaPpto[2]); //medio pago
                            array_push($datos2, $rowSinReciboCajaPpto[3]); //vig gasto
                            array_push($datos2, $rowSinReciboCajaPpto[4]); //valor
                            array_push($datos2, $rowSinReciboCajaPpto[5]); //cpc

                            array_push($consulta, $datos2);
                        }
                    }

                    for ($i=0; $i < count($consulta); $i++) {

                        $result = validaArrayConsultaRecaudos($consulta[$i], $datosBase);

                        if ($result == -1) {
                            $prueba = array();

                            array_push($prueba, 'C');
                            array_push($prueba, $consulta[$i][4]); //vigencia gasto
                            array_push($prueba, $consulta[$i][2]); //seccion presupuestal
                            array_push($prueba, $_POST["cuentas"][$x][0]); //cuenta
                            array_push($prueba, $_POST["cuentas"][$x][1]); //Nombre cuenta
                            array_push($prueba, $consulta[$i][1]); //fuente
                            array_push($prueba, $consulta[$i][3]); //medio pago
                            array_push($prueba, $consulta[$i][6]); //CPC
                            array_push($prueba, "0"); //Valor inicial
                            array_push($prueba, "0"); //adicion
                            array_push($prueba, "0"); //reduccion
                            array_push($prueba, "0"); //definitivo
                            array_push($prueba, "0"); //reservas
                            array_push($prueba, $consulta[$i][5]); //recaudo consulta

                            array_push($datosBase, $prueba);
                        }
                        else {
                            $datosBase[$result][13] += $consulta[$i][5]; //valor
                        }
                    }

                    $consulta = array();

                    //pptonotasbanppto
                    $sqlPptoNotasBanPpto = "SELECT pnb.fuente,
                    pnb.seccion_presupuestal, pnb.medio_pago,
                    pnb.vigencia_gasto, SUM(pnb.valor),cuenta_clasificadora
                    FROM pptonotasbanppto pnb,tesonotasbancarias_cab tnp WHERE pnb.cuenta LIKE '".$_POST["cuentas"][$x][0]."' AND (pnb.vigencia = '$vigenciaIni' OR pnb.vigencia = '$vigenciaFin') AND tnp.id_comp=pnb.idrecibo AND NOT(tnp.estado='R' OR tnp.estado='N')  AND (tnp.vigencia = '$vigenciaIni' OR tnp.vigencia = '$vigenciaFin') AND tnp.fecha between '$fechaIni' AND '$fechaFin' AND pnb.seccion_presupuestal LIKE '$secPresu' AND pnb.medio_pago LIKE '$medioPago' AND pnb.vigencia_gasto LIKE '$vigGasto' AND pnb.fuente LIKE '".$_POST["fuentes"][$xy]."%' GROUP BY pnb.cuenta_clasificadora,pnb.fuente, pnb.seccion_presupuestal, pnb.medio_pago, pnb.vigencia_gasto";
                    $resPptoNotasBanPpto = mysqli_query($linkbd, $sqlPptoNotasBanPpto);
                    while ($rowPptoNotasBanPpto = mysqli_fetch_row($resPptoNotasBanPpto)) {

                        $datos2 = array();

                        if ($rowPptoNotasBanPpto) {

                            array_push($datos2, $_POST["cuentas"][$x][0]); //cuenta
                            array_push($datos2, $rowPptoNotasBanPpto[0]); //fuente
                            array_push($datos2, $rowPptoNotasBanPpto[1]); //sec presu
                            array_push($datos2, $rowPptoNotasBanPpto[2]); //medio pago
                            array_push($datos2, $rowPptoNotasBanPpto[3]); //vig gasto
                            array_push($datos2, $rowPptoNotasBanPpto[4]); //valor
                            array_push($datos2, ""); //cpc
                            array_push($datos2, $rowPptoNotasBanPpto[5]); //Cuenta clasificadora
                            array_push($datos2, "notas");
                            array_push($consulta, $datos2);
                        }
                    }
                    for ($i=0; $i < count($consulta); $i++) {

                        $result = validaArrayConsultaRecaudos($consulta[$i], $datosBase);
                        if ($result == -1) {
                            $prueba = array();
                            array_push($prueba, 'C');
                            array_push($prueba, $consulta[$i][4]); //vigencia gasto
                            array_push($prueba, $consulta[$i][2]); //seccion presupuestal
                            array_push($prueba, $_POST["cuentas"][$x][0]); //cuenta
                            array_push($prueba, $_POST["cuentas"][$x][1]); //Nombre cuenta
                            array_push($prueba, $consulta[$i][1]); //fuente
                            array_push($prueba, $consulta[$i][3]); //medio pago
                            array_push($prueba, $consulta[$i][6]); //CPC
                            array_push($prueba, "0"); //Valor inicial
                            array_push($prueba, "0"); //adicion
                            array_push($prueba, "0"); //reduccion
                            array_push($prueba, "0"); //definitivo
                            array_push($prueba, "0"); //reservas
                            array_push($prueba, $consulta[$i][5]); //recaudo consulta
                            $prueba[19]=$consulta[$i][7];
                            $prueba[20]=selectNombreClasificador($consulta[$i][7],$_POST["cuentas"][$x][0]);
                            $prueba[21]=$consulta[$i][8];
                            array_push($datosBase, $prueba);
                        }
                        else {
                            $datosBase[$result][13] += $consulta[$i][5]; //valor
                        }
                    }

                    $consulta = array();

                    //pptoingtranppto recaudos transferencias
                    $sqlRecaudoTransferencia = "SELECT pitp.fuente, pitp.seccion_presupuestal, pitp.medio_pago, pitp.vigencia_gasto, SUM(pitp.valor), pitp.productoservicio FROM pptoingtranppto pitp,tesorecaudotransferencia titp WHERE pitp.cuenta LIKE '".$_POST["cuentas"][$x][0]."' AND (pitp.vigencia = '$vigenciaIni' OR pitp.vigencia = '$vigenciaFin') AND pitp.idrecibo=titp.id_recaudo AND NOT(titp.estado='N' OR titp.estado='R') AND titp.fecha between '$fechaIni' AND '$fechaFin' AND pitp.seccion_presupuestal LIKE '$secPresu' AND pitp.medio_pago LIKE '$medioPago' AND pitp.vigencia_gasto LIKE '$vigGasto' AND pitp.fuente LIKE '".$_POST["fuentes"][$xy]."%' GROUP BY pitp.fuente, pitp.seccion_presupuestal, pitp.medio_pago, pitp.vigencia_gasto, pitp.productoservicio";
                    $resRecaudoTransferencia = mysqli_query($linkbd,$sqlRecaudoTransferencia);
                    while ($rowRecaudoTransferencia = mysqli_fetch_row($resRecaudoTransferencia)){

                        $datos2 = array();

                        if ($rowRecaudoTransferencia){

                            array_push($datos2, $_POST["cuentas"][$x][0]); //cuenta
                            array_push($datos2, $rowRecaudoTransferencia[0]); //fuente
                            array_push($datos2, $rowRecaudoTransferencia[1]); //sec presu
                            array_push($datos2, $rowRecaudoTransferencia[2]); //medio pago
                            array_push($datos2, $rowRecaudoTransferencia[3]); //vig gasto
                            array_push($datos2, $rowRecaudoTransferencia[4]); //valor
                            array_push($datos2, $rowRecaudoTransferencia[5]); //cpc

                            array_push($consulta, $datos2);
                        }
                    }

                    for ($i=0; $i < count($consulta); $i++){

                        $result = validaArrayConsultaRecaudos($consulta[$i], $datosBase);

                        if ($result == -1) {
                            $prueba = array();

                            array_push($prueba, 'C');
                            array_push($prueba, $consulta[$i][4]); //vigencia gasto
                            array_push($prueba, $consulta[$i][2]); //seccion presupuestal
                            array_push($prueba, $_POST["cuentas"][$x][0]); //cuenta
                            array_push($prueba, $_POST["cuentas"][$x][1]); //Nombre cuenta
                            array_push($prueba, $consulta[$i][1]); //fuente
                            array_push($prueba, $consulta[$i][3]); //medio pago
                            array_push($prueba, $consulta[$i][6]); //CPC
                            array_push($prueba, "0"); //Valor inicial
                            array_push($prueba, "0"); //adicion
                            array_push($prueba, "0"); //reduccion
                            array_push($prueba, "0"); //definitivo
                            array_push($prueba, "0"); //reservas
                            array_push($prueba, $consulta[$i][5]); //recaudo consulta

                            array_push($datosBase, $prueba);
                        }
                        else {
                            $datosBase[$result][13] += $consulta[$i][5]; //valor
                        }
                    }

                    $consulta = array();

                    //pptoretencionpago egresos
                    $sqlRetencionEgresos = "SELECT prc.fuente, prc.seccion_presupuestal, prc.medio_pago, prc.vigencia_gasto, SUM(prc.valor), prc.productoservicio FROM pptoretencionpago prc,tesoegresos trc WHERE prc.cuenta LIKE '".$_POST["cuentas"][$x][0]."' AND (prc.vigencia = '$vigenciaIni' OR prc.vigencia = '$vigenciaFin') AND trc.id_egreso=prc.idrecibo AND NOT(trc.estado='N') AND trc.fecha between '$fechaIni' AND '$fechaFin' AND trc.tipo_mov='201' AND prc.seccion_presupuestal LIKE '$secPresu' AND prc.medio_pago LIKE '$medioPago' AND prc.vigencia_gasto LIKE '$vigGasto' AND prc.tipo='egreso' AND NOT EXISTS (SELECT 1 FROM tesoegresos tra WHERE tra.id_egreso=trc.id_egreso  AND tra.tipo_mov='401') AND prc.fuente LIKE '".$_POST["fuentes"][$xy]."%' GROUP BY prc.fuente, prc.seccion_presupuestal, prc.medio_pago, prc.vigencia_gasto, prc.productoservicio";
                    $resRetencionEgresos = mysqli_query($linkbd, $sqlRetencionEgresos);
                    while ($rowRetencionEgresos = mysqli_fetch_row($resRetencionEgresos)) {

                        $datos2 = array();

                        if ($rowRetencionEgresos) {

                            array_push($datos2, $_POST["cuentas"][$x][0]); //cuenta
                            array_push($datos2, $rowRetencionEgresos[0]); //fuente
                            array_push($datos2, $rowRetencionEgresos[1]); //sec presu
                            array_push($datos2, $rowRetencionEgresos[2]); //medio pago
                            array_push($datos2, $rowRetencionEgresos[3]); //vig gasto
                            array_push($datos2, $rowRetencionEgresos[4]); //valor
                            array_push($datos2, $rowRetencionEgresos[5]); //cpc

                            array_push($consulta, $datos2);
                        }
                    }

                    for ($i=0; $i < count($consulta); $i++) {

                        $result = validaArrayConsultaRecaudos($consulta[$i], $datosBase);

                        if ($result == -1) {
                            $prueba = array();

                            array_push($prueba, 'C');
                            array_push($prueba, $consulta[$i][4]); //vigencia gasto
                            array_push($prueba, $consulta[$i][2]); //seccion presupuestal
                            array_push($prueba, $_POST["cuentas"][$x][0]); //cuenta
                            array_push($prueba, $_POST["cuentas"][$x][1]); //Nombre cuenta
                            array_push($prueba, $consulta[$i][1]); //fuente
                            array_push($prueba, $consulta[$i][3]); //medio pago
                            array_push($prueba, $consulta[$i][6]); //CPC
                            array_push($prueba, "0"); //Valor inicial
                            array_push($prueba, "0"); //adicion
                            array_push($prueba, "0"); //reduccion
                            array_push($prueba, "0"); //definitivo
                            array_push($prueba, "0"); //reservas
                            array_push($prueba, $consulta[$i][5]); //recaudo consulta

                            array_push($datosBase, $prueba);
                        }
                        else {
                            $datosBase[$result][13] += $consulta[$i][5]; //valor
                        }
                    }

                    $consulta = array();

                    //pptoretencionotrosegresos otros egresos
                    $sqlRetencionEgresos = "SELECT prc.fuente, prc.seccion_presupuestal, prc.medio_pago, prc.vigencia_gasto, SUM(prc.valor), prc.productoservicio FROM pptoretencionotrosegresos prc,tesopagotercerosvigant trc WHERE prc.cuenta LIKE '".$_POST["cuentas"][$x][0]."' AND (prc.vigencia = '$vigenciaIni' OR prc.vigencia = '$vigenciaFin') AND trc.id_pago=prc.idrecibo AND NOT(trc.estado='N') AND trc.fecha between '$fechaIni' AND '$fechaFin' AND trc.estado='S' AND prc.seccion_presupuestal LIKE '$secPresu' AND prc.medio_pago LIKE '$medioPago' AND prc.vigencia_gasto LIKE '$vigGasto' AND prc.tipo='egreso' AND prc.fuente LIKE '".$_POST["fuentes"][$xy]."%' GROUP BY prc.fuente, prc.seccion_presupuestal, prc.medio_pago, prc.vigencia_gasto, prc.productoservicio";
                    $resRetencionEgresos = mysqli_query($linkbd, $sqlRetencionEgresos);
                    while ($rowRetencionEgresos = mysqli_fetch_row($resRetencionEgresos)) {

                        $datos2 = array();

                        if ($rowRetencionEgresos) {

                            array_push($datos2, $_POST["cuentas"][$x][0]); //cuenta
                            array_push($datos2, $rowRetencionEgresos[0]); //fuente
                            array_push($datos2, $rowRetencionEgresos[1]); //sec presu
                            array_push($datos2, $rowRetencionEgresos[2]); //medio pago
                            array_push($datos2, $rowRetencionEgresos[3]); //vig gasto
                            array_push($datos2, $rowRetencionEgresos[4]); //valor
                            array_push($datos2, $rowRetencionEgresos[5]); //cpc

                            array_push($consulta, $datos2);
                        }
                    }

                    for ($i=0; $i < count($consulta); $i++) {

                        $result = validaArrayConsultaRecaudos($consulta[$i], $datosBase);

                        if ($result == -1) {
                            $prueba = array();

                            array_push($prueba, 'C');
                            array_push($prueba, $consulta[$i][4]); //vigencia gasto
                            array_push($prueba, $consulta[$i][2]); //seccion presupuestal
                            array_push($prueba, $_POST["cuentas"][$x][0]); //cuenta
                            array_push($prueba, $_POST["cuentas"][$x][1]); //Nombre cuenta
                            array_push($prueba, $consulta[$i][1]); //fuente
                            array_push($prueba, $consulta[$i][3]); //medio pago
                            array_push($prueba, $consulta[$i][6]); //CPC
                            array_push($prueba, "0"); //Valor inicial
                            array_push($prueba, "0"); //adicion
                            array_push($prueba, "0"); //reduccion
                            array_push($prueba, "0"); //definitivo
                            array_push($prueba, "0"); //reservas
                            array_push($prueba, $consulta[$i][5]); //recaudo consulta

                            array_push($datosBase, $prueba);
                        }
                        else {
                            $datosBase[$result][13] += $consulta[$i][5]; //valor
                        }
                    }

                    $consulta = array();

                    //pptoretencionpago
                    $sqlRetencionOrdenPago = "SELECT prc.fuente, prc.seccion_presupuestal, prc.medio_pago, prc.vigencia_gasto, SUM(prc.valor), prc.productoservicio FROM pptoretencionpago prc,tesoordenpago trc WHERE prc.cuenta LIKE '".$_POST["cuentas"][$x][0]."' AND (prc.vigencia='$vigenciaIni' OR prc.vigencia = '$vigenciaFin') AND trc.id_orden=prc.idrecibo AND NOT(trc.estado='N') AND trc.fecha between '$fechaIni' AND '$fechaFin' AND trc.tipo_mov='201' AND prc.seccion_presupuestal LIKE '$secPresu' AND prc.medio_pago LIKE '$medioPago' AND prc.vigencia_gasto LIKE '$vigGasto' AND prc.tipo='orden' AND NOT EXISTS (SELECT 1 FROM tesoordenpago tca WHERE tca.id_orden=trc.id_orden  AND tca.tipo_mov='401') AND prc.fuente LIKE '".$_POST["fuentes"][$xy]."%' GROUP BY prc.fuente, prc.seccion_presupuestal, prc.medio_pago, prc.vigencia_gasto, prc.productoservicio";
                    $resRetencionOrdenPago = mysqli_query($linkbd, $sqlRetencionOrdenPago);
                    while ($rowRetencionOrdenPago = mysqli_fetch_row($resRetencionOrdenPago)) {

                        $datos2 = array();

                        if ($rowRetencionOrdenPago) {

                            array_push($datos2, $_POST["cuentas"][$x][0]); //cuenta
                            array_push($datos2, $rowRetencionOrdenPago[0]); //fuente
                            array_push($datos2, $rowRetencionOrdenPago[1]); //sec presu
                            array_push($datos2, $rowRetencionOrdenPago[2]); //medio pago
                            array_push($datos2, $rowRetencionOrdenPago[3]); //vig gasto
                            array_push($datos2, $rowRetencionOrdenPago[4]); //valor
                            array_push($datos2, $rowRetencionOrdenPago[5]); //cpc

                            array_push($consulta, $datos2);
                        }
                    }

                    for ($i=0; $i < count($consulta); $i++){

                        $result = validaArrayConsultaRecaudos($consulta[$i], $datosBase);

                        if ($result == -1) {
                            $prueba = array();

                            array_push($prueba, 'C');
                            array_push($prueba, $consulta[$i][4]); //vigencia gasto
                            array_push($prueba, $consulta[$i][2]); //seccion presupuestal
                            array_push($prueba, $_POST["cuentas"][$x][0]); //cuenta
                            array_push($prueba, $_POST["cuentas"][$x][1]); //Nombre cuenta
                            array_push($prueba, $consulta[$i][1]); //fuente
                            array_push($prueba, $consulta[$i][3]); //medio pago
                            array_push($prueba, $consulta[$i][6]); //CPC
                            array_push($prueba, "0"); //Valor inicial
                            array_push($prueba, "0"); //adicion
                            array_push($prueba, "0"); //reduccion
                            array_push($prueba, "0"); //definitivo
                            array_push($prueba, "0"); //reservas
                            array_push($prueba, $consulta[$i][5]); //recaudo consulta

                            array_push($datosBase, $prueba);
                        }
                        else {
                            $datosBase[$result][13] += $consulta[$i][5]; //valor
                        }
                    }

                    $consulta = array();
                    //srv_recaudo_factura_ppto
                    $sqlSP = "SELECT prc.fuente, prc.seccion_presupuestal, prc.medio_pago, prc.vigencia_gasto, SUM(prc.valor), prc.producto_servicio FROM srv_recaudo_factura_ppto prc,srv_recaudo_factura trc WHERE prc.cuenta LIKE '".$_POST["cuentas"][$x][0]."' AND (prc.vigencia = '$vigenciaIni' OR prc.vigencia = '$vigenciaFin') AND trc.codigo_recaudo=prc.codigo_recaudo AND NOT(trc.estado='REVERSADO') AND trc.fecha_recaudo between '$fechaIni' AND '$fechaFin' AND prc.seccion_presupuestal LIKE '$secPresu' AND prc.medio_pago LIKE '$medioPago' AND prc.vigencia_gasto LIKE '$vigGasto' AND prc.fuente LIKE '".$_POST["fuentes"][$xy]."%' GROUP BY prc.fuente, prc.seccion_presupuestal, prc.medio_pago, prc.vigencia_gasto, prc.producto_servicio";
                    $resSP = mysqli_query($linkbd, $sqlSP);
                    while ($rowSP = mysqli_fetch_row($resSP)) {

                        $datos2 = array();

                        if ($rowSP) {

                            array_push($datos2, $_POST["cuentas"][$x][0]); //cuenta
                            array_push($datos2, $rowSP[0]); //fuente
                            array_push($datos2, $rowSP[1]); //sec presu
                            array_push($datos2, $rowSP[2]); //medio pago
                            array_push($datos2, $rowSP[3]); //vig gasto
                            array_push($datos2, $rowSP[4]); //valor
                            array_push($datos2, $rowSP[5]); //cpc

                            array_push($consulta, $datos2);
                        }
                    }

                    for ($i=0; $i < count($consulta); $i++) {

                        $result = validaArrayConsultaRecaudos($consulta[$i], $datosBase);

                        if ($result == -1) {
                            $prueba = array();

                            array_push($prueba, 'C');
                            array_push($prueba, $consulta[$i][4]); //vigencia gasto
                            array_push($prueba, $consulta[$i][2]); //seccion presupuestal
                            array_push($prueba, $_POST["cuentas"][$x][0]); //cuenta
                            array_push($prueba, $_POST["cuentas"][$x][1]); //Nombre cuenta
                            array_push($prueba, $consulta[$i][1]); //fuente
                            array_push($prueba, $consulta[$i][3]); //medio pago
                            array_push($prueba, $consulta[$i][6]); //CPC
                            array_push($prueba, "0"); //Valor inicial
                            array_push($prueba, "0"); //adicion
                            array_push($prueba, "0"); //reduccion
                            array_push($prueba, "0"); //definitivo
                            array_push($prueba, "0"); //reservas
                            array_push($prueba, $consulta[$i][5]); //recaudo consulta

                            array_push($datosBase, $prueba);
                        }
                        else {
                            $datosBase[$result][13] += $consulta[$i][5]; //valor
                        }
                    }
                    $consulta = array();
                    //Recaudos factura por acuerdo servicios publicos
                    $sqlSP = "SELECT prc.fuente, prc.seccion_presupuestal, prc.medio_pago, prc.vigencia_gasto, SUM(prc.valor), prc.producto_servicio FROM srv_acuerdo_ppto prc,srv_acuerdo_cab trc WHERE prc.cuenta LIKE '".$_POST["cuentas"][$x][0]."' AND (prc.vigencia = '$vigenciaIni' OR prc.vigencia = '$vigenciaFin') AND trc.codigo_acuerdo=prc.codigo_acuerdo AND NOT(trc.estado_acuerdo='Reversado') AND trc.fecha_acuerdo between '$fechaIni' AND '$fechaFin' AND prc.seccion_presupuestal LIKE '$secPresu' AND prc.medio_pago LIKE '$medioPago' AND prc.vigencia_gasto LIKE '$vigGasto' AND prc.fuente LIKE '".$_POST["fuentes"][$xy]."%' GROUP BY prc.fuente, prc.seccion_presupuestal, prc.medio_pago, prc.vigencia_gasto, prc.producto_servicio";
                    $resSP = mysqli_query($linkbd, $sqlSP);
                    while ($rowSP = mysqli_fetch_row($resSP)) {

                        $datos2 = array();

                        if ($rowSP) {

                            array_push($datos2, $_POST["cuentas"][$x][0]); //cuenta
                            array_push($datos2, $rowSP[0]); //fuente
                            array_push($datos2, $rowSP[1]); //sec presu
                            array_push($datos2, $rowSP[2]); //medio pago
                            array_push($datos2, $rowSP[3]); //vig gasto
                            array_push($datos2, $rowSP[4]); //valor
                            array_push($datos2, $rowSP[5]); //cpc

                            array_push($consulta, $datos2);
                        }
                    }

                    for ($i=0; $i < count($consulta); $i++) {

                        $result = validaArrayConsultaRecaudos($consulta[$i], $datosBase);

                        if ($result == -1) {
                            $prueba = array();

                            array_push($prueba, 'C');
                            array_push($prueba, $consulta[$i][4]); //vigencia gasto
                            array_push($prueba, $consulta[$i][2]); //seccion presupuestal
                            array_push($prueba, $_POST["cuentas"][$x][0]); //cuenta
                            array_push($prueba, $_POST["cuentas"][$x][1]); //Nombre cuenta
                            array_push($prueba, $consulta[$i][1]); //fuente
                            array_push($prueba, $consulta[$i][3]); //medio pago
                            array_push($prueba, $consulta[$i][6]); //CPC
                            array_push($prueba, "0"); //Valor inicial
                            array_push($prueba, "0"); //adicion
                            array_push($prueba, "0"); //reduccion
                            array_push($prueba, "0"); //definitivo
                            array_push($prueba, "0"); //reservas
                            array_push($prueba, $consulta[$i][5]); //recaudo consulta

                            array_push($datosBase, $prueba);
                        }
                        else {
                            $datosBase[$result][13] += $consulta[$i][5]; //valor
                        }
                    }

                    $consulta = array();

                    $sqlSPAntiguo = "SELECT servreciboscaja_det.fuente, servreciboscaja_det.seccion_presupuestal, servreciboscaja_det.medio_pago, servreciboscaja_det.vigencia_gasto, SUM(servreciboscaja_det.valor) FROM servreciboscaja_det, servreciboscaja WHERE servreciboscaja.estado = 'S' AND servreciboscaja.id_recibos = servreciboscaja_det.id_recibos AND servreciboscaja_det.cuentapres LIKE  '".$_POST["cuentas"][$x][0]."' AND servreciboscaja.fecha BETWEEN '$fechaIni' AND '$fechaFin' AND servreciboscaja_det.seccion_presupuestal LIKE '$secPresu' AND servreciboscaja_det.medio_pago LIKE '$medioPago' AND servreciboscaja_det.vigencia_gasto LIKE '$vigGasto' AND servreciboscaja_det.fuente LIKE '".$_POST["fuentes"][$xy]."%' GROUP BY servreciboscaja_det.fuente, servreciboscaja_det.seccion_presupuestal, servreciboscaja_det.medio_pago, servreciboscaja_det.vigencia_gasto";
                    $resSPAntiguo = mysqli_query($linkbd, $sqlSPAntiguo);
                    while ($rowSPAntiguo = mysqli_fetch_row($resSPAntiguo)) {

                        $datos2 = array();

                        if ($rowSPAntiguo) {

                            array_push($datos2, $_POST["cuentas"][$x][0]); //cuenta
                            array_push($datos2, $rowSPAntiguo[0]); //fuente
                            array_push($datos2, $rowSPAntiguo[1]); //sec presu
                            array_push($datos2, $rowSPAntiguo[2]); //medio pago
                            array_push($datos2, $rowSPAntiguo[3]); //vig gasto
                            array_push($datos2, $rowSPAntiguo[4]); //valor
                            array_push($datos2, ""); //cpc

                            array_push($consulta, $datos2);
                        }
                    }

                    for ($i=0; $i < count($consulta); $i++) {

                        $result = validaArrayConsultaRecaudos($consulta[$i], $datosBase);

                        if ($result == -1) {
                            $prueba = array();

                            array_push($prueba, 'C');
                            array_push($prueba, $consulta[$i][4]); //vigencia gasto
                            array_push($prueba, $consulta[$i][2]); //seccion presupuestal
                            array_push($prueba, $_POST["cuentas"][$x][0]); //cuenta
                            array_push($prueba, $_POST["cuentas"][$x][1]); //Nombre cuenta
                            array_push($prueba, $consulta[$i][1]); //fuente
                            array_push($prueba, $consulta[$i][3]); //medio pago
                            array_push($prueba, $consulta[$i][6]); //CPC
                            array_push($prueba, "0"); //Valor inicial
                            array_push($prueba, "0"); //adicion
                            array_push($prueba, "0"); //reduccion
                            array_push($prueba, "0"); //definitivo
                            array_push($prueba, "0"); //reservas
                            array_push($prueba, $consulta[$i][5]); //recaudo consulta

                            array_push($datosBase, $prueba);
                        }
                        else {
                            $datosBase[$result][13] += $consulta[$i][5]; //valor
                        }
                    }

                    //Superavit
                    $superavit = array();

                    $sqlSuperavit = "SELECT tsad.fuente, tsad.seccion_presupuestal, tsad.medio_pago, tsad.vigencia_gasto, SUM(tsad.valor),clasificador_superavit
                    FROM tesosuperavit AS tsa, tesosuperavit_det AS tsad WHERE tsa.estado = 'S' AND tsa.id = tsad.id_tesosuperavit AND tsa.fecha
                    BETWEEN '$fechaIni' AND '$fechaFin' AND tsad.rubro LIKE '".$_POST["cuentas"][$x][0]."' AND tsad.seccion_presupuestal LIKE '$secPresu'
                    AND tsad.medio_pago LIKE '$medioPago' AND vigencia_gasto LIKE '$vigGasto' AND tsad.fuente LIKE '".$_POST["fuentes"][$xy]."%'
                    GROUP BY tsad.clasificador_superavit,tsad.fuente, tsad.seccion_presupuestal, tsad.medio_pago, tsad.vigencia_gasto";
                    $resSuperavit = mysqli_query($linkbd, $sqlSuperavit);

                    while ($rowSuperavit = mysqli_fetch_row($resSuperavit)) {

                        $datos2 = array();

                        if ($rowSuperavit) {

                            array_push($datos2, $_POST["cuentas"][$x][0]); //cuenta
                            array_push($datos2, $rowSuperavit[0]); //fuente
                            array_push($datos2, $rowSuperavit[1]); //sec presu
                            array_push($datos2, $rowSuperavit[2]); //medio pago
                            array_push($datos2, $rowSuperavit[3]); //vig gasto
                            array_push($datos2, $rowSuperavit[4]); //valor
                            array_push($datos2, ""); //cpc
                            array_push($datos2, $rowSuperavit[5]); //Clasificador superavit
                            array_push($datos2, "superavit"); //Clasificador superavit
                            array_push($superavit, $datos2);
                        }
                    }

                    for ($i=0; $i < count($superavit); $i++) {
                        $result = validaArrayConsultaRecaudos($superavit[$i], $datosBase);

                        if ($result == -1) {

                            $prueba = array();

                            array_push($prueba, 'C');
                            array_push($prueba, $superavit[$i][4]); //vigencia gasto
                            array_push($prueba, $superavit[$i][2]); //seccion presupuestal
                            array_push($prueba, $_POST["cuentas"][$x][0]); //cuenta
                            array_push($prueba, $_POST["cuentas"][$x][1]); //Nombre cuenta
                            array_push($prueba, $superavit[$i][1]); //fuente
                            array_push($prueba, $superavit[$i][3]); //medio pago
                            array_push($prueba, $superavit[$i][6]); //CPC
                            array_push($prueba, "0"); //Valor inicial
                            array_push($prueba, "0"); //adicion
                            array_push($prueba, "0"); //reduccion
                            array_push($prueba, "0"); //definitivo
                            array_push($prueba, "0"); //reservas
                            array_push($prueba, $superavit[$i][5]); //recaudo consulta
                            $prueba[19]=$superavit[$i][7];
                            $prueba[20]=selectNombreClasificador($superavit[$i][7],$_POST["cuentas"][$x][0]);
                            $prueba[21]=$superavit[$i][8];
                            array_push($datosBase, $prueba);
                        }
                        else {
                            $datosBase[$result][13] += $superavit[$i][5];
                        }
                    }

                    $consulta = array();

                    $queryspCumaribo = "SELECT TB2.fuente, TB2.seccion_presupuestal, TB2.medio_pago, TB2.vigencia_gasto, SUM(TB2.valor) from tesosinreciboscajasp TB1,pptosinrecibocajaspppto TB2 WHERE TB1.id_recibos=TB2.idrecibo AND TB1.estado='S' AND (TB2.vigencia = '$vigenciaIni' OR TB2.vigencia = '$vigenciaFin') AND TB1.vigencia=TB2.vigencia AND TB2.cuenta LIKE '".$_POST["cuentas"][$x][0]."' AND TB1.fecha between '$fechaIni' AND '$fechaFin' AND TB2.seccion_presupuestal LIKE '$secPresu' AND TB2.medio_pago LIKE '$medioPago' AND TB2.vigencia_gasto LIKE '$vigGasto' AND TB2.fuente LIKE '".$_POST["fuentes"][$xy]."%' GROUP BY TB2.fuente, TB2.seccion_presupuestal, TB2.medio_pago, TB2.vigencia_gasto";
                    $resspCumaribo = mysqli_query($linkbd, $queryspCumaribo);
                    while ($rowspCumaribo = mysqli_fetch_row($resspCumaribo)) {

                        $datos2 = array();

                        if ($rowspCumaribo) {

                            array_push($datos2, $_POST["cuentas"][$x][0]); //cuenta
                            array_push($datos2, $rowspCumaribo[0]); //fuente
                            array_push($datos2, $rowspCumaribo[1]); //sec presu
                            array_push($datos2, $rowspCumaribo[2]); //medio pago
                            array_push($datos2, $rowspCumaribo[3]); //vig gasto
                            array_push($datos2, $rowspCumaribo[4]); //valor
                            array_push($datos2, ""); //cpc
                            array_push($consulta, $datos2);
                        }
                    }

                    for ($i=0; $i < count($consulta); $i++) {

                        $result = validaArrayConsultaRecaudos($consulta[$i], $datosBase);

                        if ($result == -1) {

                            $prueba = array();

                            array_push($prueba, 'C');
                            array_push($prueba, $consulta[$i][2]); //vigencia gasto
                            array_push($prueba, ""); //seccion presupuestal
                            array_push($prueba, $_POST["cuentas"][$x][0]); //cuenta
                            array_push($prueba, $_POST["cuentas"][$x][1]); //Nombre cuenta
                            array_push($prueba, $consulta[$i][1]); //fuente
                            array_push($prueba, ""); //medio pago
                            array_push($prueba, $consulta[$i][3]); //CPC
                            array_push($prueba, "0"); //Valor inicial
                            array_push($prueba, "0"); //adicion
                            array_push($prueba, "0"); //reduccion
                            array_push($prueba, "0"); //definitivo
                            array_push($prueba, "0"); //reservas
                            array_push($prueba, $consulta[$i][5]); //recaudo consulta
                            array_push($datosBase, $prueba);
                        }
                        else {
                            $datosBase[$result][13] += $consulta[$i][5];
                        }
                    }

                    $datosBase = llenaBase($datosBase, 14);
                    $datosBase = llenaBase($datosBase, 15);
                    $datosBase = llenaBase($datosBase, 16);
                    $datosBase = llenaBase($datosBase, 17);
                    $datosBase = llenaBase($datosBase, 18);

                    for ($i=0; $i < count($datosBase); $i++){
                        if($datosBase[$i][0] == 'C' && $datosBase[$i][12] == '0' && $datosBase[$i][13] == '0'){

                            $seccionPresupuestal = $datosBase[$i][2];
                            $vigenciaGasto = $datosBase[$i][1];
                            $medioDePago = $datosBase[$i][6];
                            $fuente = $datosBase[$i][5];
                            $recaudoAcumulado = 0;

                            $queryReservasAcu = "SELECT COALESCE(SUM(tsad.valor),0) FROM tesoreservas AS tsa, tesoreservas_det AS tsad WHERE tsa.estado = 'S' AND tsa.id = tsad.id_reserva AND tsad.vigencia_gasto LIKE '$vigenciaGasto' AND tsa.fecha BETWEEN '$fechaIni' AND '$fechaFin' AND tsad.rubro LIKE '".$_POST["cuentas"][$x][0]."' AND tsad.fuente LIKE '$fuente' AND tsad.vigencia_gasto LIKE '$vigenciaGasto' AND tsad.clasificador LIKE '$medioDePago' AND tsad.clasificacion LIKE '$seccionPresupuestal'";
                            $rowReservasAcu = mysqli_fetch_row(mysqli_query($linkbd, $queryReservasAcu));

                            $recaudoAcumulado += $rowReservasAcu[0];

                            //pptorecibocajappto
                            $sqlReciboCajaPptoAcu = "SELECT COALESCE(SUM(prc.valor),0) FROM pptorecibocajappto prc,tesoreciboscaja trc WHERE prc.cuenta LIKE '".$_POST["cuentas"][$x][0]."' AND (prc.vigencia = $vigenciaIni OR prc.vigencia = $vigenciaFin) AND trc.id_recibos=prc.idrecibo AND NOT(trc.estado='N' OR trc.estado='R') AND NOT(prc.tipo='R') AND trc.fecha between '$fechaIni' AND '$fechaFin' AND prc.seccion_presupuestal LIKE '$seccionPresupuestal' AND prc.medio_pago LIKE '$medioDePago' AND prc.vigencia_gasto LIKE '$vigenciaGasto' AND prc.fuente LIKE '$fuente'";
                            $rowReciboCajaPptoAcu = mysqli_fetch_row(mysqli_query($linkbd, $sqlReciboCajaPptoAcu));

                            $recaudoAcumulado += $rowReciboCajaPptoAcu[0];

                            //pptosinrecibocajappto
                            $sqlSinReciboCajaPptoAcu = "SELECT COALESCE(SUM(psrc.valor),0) FROM pptosinrecibocajappto psrc,tesosinreciboscaja tsrc WHERE psrc.cuenta LIKE '".$_POST["cuentas"][$x][0]."' AND (psrc.vigencia=$vigenciaIni OR psrc.vigencia = '$vigenciaFin') AND tsrc.id_recibos=psrc.idrecibo AND NOT(tsrc.estado='N' OR tsrc.estado='R') AND tsrc.fecha between '$fechaIni' AND '$fechaFin' AND psrc.seccion_presupuestal LIKE '$seccionPresupuestal' AND psrc.medio_pago LIKE '$medioDePago' AND psrc.vigencia_gasto LIKE '$vigenciaGasto' AND psrc.fuente LIKE '$fuente'";
                            $rowSinReciboCajaPptoAcu = mysqli_fetch_row(mysqli_query($linkbd, $sqlSinReciboCajaPptoAcu));

                            $recaudoAcumulado += $rowSinReciboCajaPptoAcu[0];

                            //pptonotasbanppto
                            $sqlPptoNotasBanPptoAcu = "SELECT COALESCE(SUM(pnb.valor),0) FROM pptonotasbanppto pnb,tesonotasbancarias_cab tnp WHERE pnb.cuenta LIKE '".$_POST["cuentas"][$x][0]."' AND (pnb.vigencia = '$vigenciaIni' OR pnb.vigencia = '$vigenciaFin') AND tnp.id_comp=pnb.idrecibo AND NOT(tnp.estado='R' OR tnp.estado='N')  AND (tnp.vigencia = '$vigenciaIni' OR tnp.vigencia = '$vigenciaFin') AND tnp.fecha between '$fechaIni' AND '$fechaFin' AND pnb.seccion_presupuestal LIKE '$seccionPresupuestal' AND pnb.medio_pago LIKE '$medioDePago' AND pnb.vigencia_gasto LIKE '$vigenciaGasto' AND pnb.fuente LIKE '$fuente'";
                            $rowPptoNotasBanPptoAcu = mysqli_fetch_row(mysqli_query($linkbd, $sqlPptoNotasBanPptoAcu));

                            $recaudoAcumulado += $rowPptoNotasBanPptoAcu[0];

                            //pptoingtranppto recaudos transferencias
                            $sqlRecaudoTransferenciaAcu = "SELECT COALESCE(SUM(pitp.valor),0) FROM pptoingtranppto pitp,tesorecaudotransferencia titp WHERE pitp.cuenta LIKE '".$_POST["cuentas"][$x][0]."' AND (pitp.vigencia = '$vigenciaIni' OR pitp.vigencia = '$vigenciaFin') AND pitp.idrecibo=titp.id_recaudo AND NOT(titp.estado='N' OR titp.estado='R') AND titp.fecha between '$fechaIni' AND '$fechaFin' AND pitp.seccion_presupuestal LIKE '$seccionPresupuestal' AND pitp.medio_pago LIKE '$medioDePago' AND pitp.vigencia_gasto LIKE '$vigenciaGasto' AND pitp.fuente LIKE '$fuente'";
                            $rowRecaudoTransferenciaAcu = mysqli_fetch_row(mysqli_query($linkbd, $sqlRecaudoTransferenciaAcu));

                            $recaudoAcumulado += $rowRecaudoTransferenciaAcu[0];

                            //pptoretencionpago egresos
                            $sqlRetencionEgresosAcu = "SELECT COALESCE(SUM(prc.valor),0) FROM pptoretencionpago prc,tesoegresos trc WHERE prc.cuenta LIKE '".$_POST["cuentas"][$x][0]."' AND (prc.vigencia = '$vigenciaIni' OR prc.vigencia = '$vigenciaFin') AND trc.id_egreso=prc.idrecibo AND NOT(trc.estado='N') AND trc.fecha between '$fechaIni' AND '$fechaFin' AND trc.tipo_mov='201' AND prc.tipo='egreso' AND NOT EXISTS (SELECT 1 FROM tesoegresos tra WHERE tra.id_egreso=trc.id_egreso  AND tra.tipo_mov='401') AND prc.seccion_presupuestal LIKE '$seccionPresupuestal' AND prc.medio_pago LIKE '$medioDePago' AND prc.vigencia_gasto LIKE '$vigenciaGasto' AND prc.fuente LIKE '$fuente'";
                            $rowRetencionEgresosAcu = mysqli_fetch_row(mysqli_query($linkbd, $sqlRetencionEgresosAcu));

                            $recaudoAcumulado += $rowRetencionEgresosAcu[0];

                            //pptoretencionotrosegresos otros egresos
                            $sqlRetencionOtrosEgresosAcu = "SELECT COALESCE(SUM(prc.valor),0) FROM pptoretencionotrosegresos prc,tesopagotercerosvigant trc WHERE prc.cuenta LIKE '".$_POST["cuentas"][$x][0]."' AND (prc.vigencia = '$vigenciaIni' OR prc.vigencia = '$vigenciaFin') AND trc.id_pago=prc.idrecibo AND NOT(trc.estado='N') AND trc.fecha between '$fechaIni' AND '$fechaFin' AND trc.estado='S' AND prc.tipo='egreso' AND prc.seccion_presupuestal LIKE '$seccionPresupuestal' AND prc.medio_pago LIKE '$medioDePago' AND prc.vigencia_gasto LIKE '$vigenciaGasto' AND prc.fuente LIKE '$fuente'";
                            $rowRetencionOtrosEgresosAcu = mysqli_fetch_row(mysqli_query($linkbd, $sqlRetencionOtrosEgresosAcu));

                            $recaudoAcumulado += $rowRetencionOtrosEgresosAcu[0];

                            //pptoretencionpago
                            $sqlRetencionOrdenPagoAcu = "SELECT COALESCE(SUM(prc.valor),0) FROM pptoretencionpago prc,tesoordenpago trc WHERE prc.cuenta LIKE '".$_POST["cuentas"][$x][0]."' AND (prc.vigencia='$vigenciaIni' OR prc.vigencia = '$vigenciaFin') AND trc.id_orden=prc.idrecibo AND NOT(trc.estado='N') AND trc.fecha between '$fechaIni' AND '$fechaFin' AND trc.tipo_mov='201' AND prc.tipo='orden' AND NOT EXISTS (SELECT 1 FROM tesoordenpago tca WHERE tca.id_orden=trc.id_orden  AND tca.tipo_mov='401') AND prc.seccion_presupuestal LIKE '$seccionPresupuestal' AND prc.medio_pago LIKE '$medioDePago' AND prc.vigencia_gasto LIKE '$vigenciaGasto' AND prc.fuente LIKE '$fuente'";
                            $rowRetencionOrdenPagoAcu = mysqli_fetch_row(mysqli_query($linkbd, $sqlRetencionOrdenPagoAcu));

                            $recaudoAcumulado += $rowRetencionOrdenPagoAcu[0];

                            //srv_recaudo_factura_ppto
                            $sqlSPAcu = "SELECT COALESCE(SUM(prc.valor),0) FROM srv_recaudo_factura_ppto prc,srv_recaudo_factura trc WHERE prc.cuenta LIKE '".$_POST["cuentas"][$x][0]."' AND (prc.vigencia = '$vigenciaIni' OR prc.vigencia = '$vigenciaFin') AND trc.codigo_recaudo=prc.codigo_recaudo AND NOT(trc.estado='REVERSADO') AND trc.fecha_recaudo between '$fechaIni' AND '$fechaFin' AND prc.seccion_presupuestal LIKE '$seccionPresupuestal' AND prc.medio_pago LIKE '$medioDePago' AND prc.vigencia_gasto LIKE '$vigenciaGasto' AND prc.fuente LIKE '$fuente'";
                            $rowSPAcu = mysqli_fetch_row(mysqli_query($linkbd, $sqlSPAcu));
                            $recaudoAcumulado += $rowSPAcu[0];

                            //Recaudo acuerdo de pago servicios públicos
                            $sqlSPAcuerdo = "SELECT COALESCE(SUM(prc.valor),0)
                            FROM srv_acuerdo_ppto prc,srv_acuerdo_cab trc
                            WHERE prc.cuenta LIKE '".$_POST["cuentas"][$x][0]."' AND (prc.vigencia = '$vigenciaIni' OR prc.vigencia = '$vigenciaFin')
                            AND trc.codigo_acuerdo=prc.codigo_acuerdo AND NOT(trc.estado_acuerdo='Reversado') AND trc.fecha_acuerdo between '$fechaIni'
                            AND '$fechaFin' AND prc.seccion_presupuestal LIKE '$seccionPresupuestal' AND prc.medio_pago LIKE '$medioDePago'
                            AND prc.vigencia_gasto LIKE '$vigenciaGasto' AND prc.fuente LIKE '$fuente'";
                            $rowSPAcuerdo = mysqli_fetch_row(mysqli_query($linkbd, $sqlSPAcu));
                            $recaudoAcumulado += $rowSPAcuerdo[0];


                            $sqlSPAntiguoAcu = "SELECT COALESCE(SUM(servreciboscaja_det.valor),0) FROM servreciboscaja_det, servreciboscaja WHERE servreciboscaja.estado = 'S' AND servreciboscaja.id_recibos = servreciboscaja_det.id_recibos AND servreciboscaja_det.cuentapres LIKE  '".$_POST["cuentas"][$x][0]."' AND servreciboscaja.fecha BETWEEN '$fechaIni' AND '$fechaFin' AND servreciboscaja_det.seccion_presupuestal LIKE '$seccionPresupuestal' AND servreciboscaja_det.medio_pago LIKE '$medioDePago' AND servreciboscaja_det.vigencia_gasto LIKE '$vigenciaGasto' AND servreciboscaja_det.fuente LIKE '$fuente'";
                            $rowSPAntiguoAcu = mysqli_fetch_row(mysqli_query($linkbd, $sqlSPAntiguoAcu));

                            $recaudoAcumulado += $rowSPAntiguoAcu[0];

                            $queryspCumariboAcu = "SELECT COALESCE(SUM(TB2.valor),0) from tesosinreciboscajasp TB1,pptosinrecibocajaspppto TB2 WHERE TB1.id_recibos=TB2.idrecibo AND TB1.estado='S' AND (TB2.vigencia = '$vigenciaIni' OR TB2.vigencia = '$vigenciaFin') AND TB1.vigencia=TB2.vigencia AND TB2.cuenta LIKE '".$_POST["cuentas"][$x][0]."' AND TB1.fecha between '$fechaIni' AND '$fechaFin' AND TB2.seccion_presupuestal LIKE '$seccionPresupuestal' AND TB2.medio_pago LIKE '$medioDePago' AND TB2.vigencia_gasto LIKE '$vigenciaGasto' AND TB2.fuente LIKE '$fuente'";
                            $rowspCumariboAcu = mysqli_fetch_row(mysqli_query($linkbd, $queryspCumariboAcu));

                            $recaudoAcumulado += $rowspCumariboAcu[0];

                            $datosBase[$i][13] = $recaudoAcumulado;
                        }
                    }

                    ////Salida
                    for ($i=0; $i < count($datosBase); $i++){
                        $datosBase[$i][14] = $datosBase[$i][12] + $datosBase[$i][13];
                        $datosBase[$i][15] = $datosBase[$i][11] - $datosBase[$i][14];


                        $enEjecucion = 0;
                        if($datosBase[$i][11] == 0 && $datosBase[$i][7] == ''){
                            $enEjecucion = round((($datosBase[$i][14]) * 100),2);
                        }else if($datosBase[$i][7] != ''){
                            $enEjecucion = round((0),2);
                        }else{
                            $enEjecucion = round((($datosBase[$i][14]/$datosBase[$i][11]) * 100),2);
                        }


                        $valida = is_nan($enEjecucion);
                        /* echo $datosBase[$i][3]." ---> ".$enEjecucion." --->  ".$valida." \n \n"; */
                        if ($valida) {
                            $enEjecucion = 0;
                        }

                        $valida = is_infinite($enEjecucion);

                        if ($valida) {
                            $enEjecucion = 0;
                        }

                        $datosBase[$i][16] = $enEjecucion;

                        $datosBase[$i][17] = encontrarFuente($datosBase[$i][5]);

                        if ($datosBase[$i][7] != ''){

                            $cpc = $datosBase[$i][7];
                            $inicial =  intval(substr($datosBase[$i][7],0,1));

                            if($inicial >= 0 && $inicial <= 4) {

                                $queryCpc = "SELECT titulo FROM ccpetbienestransportables WHERE grupo LIKE '$cpc'";
                                $rowCpc = mysqli_fetch_assoc(mysqli_query($linkbd, $queryCpc));
                                $datosBase[$i][18] = $rowCpc['titulo'];
                            }
                            else if ($inicial >= 5 && $inicial <= 9) {
                                $queryCpc = "SELECT titulo FROM ccpetservicios WHERE grupo LIKE '$cpc'";
                                $rowCpc = mysqli_fetch_assoc(mysqli_query($linkbd, $queryCpc));
                                $datosBase[$i][18] = $rowCpc['titulo'];
                            }


                        }
                        else {
                            $datosBase[$i][18] = "";
                        }

                        if ($datosBase[$i][11] > 0 || $datosBase[$i][14] > 0) {
                            array_push($detalles, $datosBase[$i]);
                        }
                    }
                }
                $xy++;
            }
            while($xy < $tamFuentes);
        }
        mysqli_close($linkbd);
        $out['detalles'] = validarTotales($detalles);
    }

    header("Content-type: application/json");
    echo json_encode($out);
    die();
