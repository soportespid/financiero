const URL = 'presupuesto_ccpet/clasificadores/ccp-generarclasificadoringresos.php';

import { buscaNombreRubros, ordenarArbol } from './../../funciones.js'

let app = new Vue({
    el: '#myapp',
    components: { Multiselect: window.VueMultiselect.default },
	data:{

        error: '',
        loading: false,

        seccionPresupuestal: [],
        vigenciaGasto: [],

        secPresu: '',
        vigGasto: '',
        medioPago: '',
        searchCuenta: {keywordCuenta: ''},
        detalles: [],

        fuentes: [],
        valueFuentes: [],
        optionsFuentes: [],
        fuentesUsadas: [],

        rubrosInicial: [],
        accountsAux: [],
        accountsMayor: [],
        resultsBuscar: [],
        results: [],
        cuentasUsadas: [],

        myStyle: {
            backgroundColor: '#80DBF5',
            'font-weight': 800,
            color: '#424343',
        },

        clasificadores: [],
        selectClasificador: '-1',

        results_det:[],
        cuentaMaxLargo: '',

        arbolIngresos: [],
        totalPresupuestado: 29850910663,
        totalRecaudado: 15476685048,
        porcentajeEjecutado: 51.84,

    },

    mounted: function(){


        const vigencia = new Date().getFullYear();
        const mes = new Date().getMonth() + 1;
        const day = new Date().getDate();

        const mesActual = mes.toString().padStart(2, '0');
        const diaActual = day.toString().padStart(2, '0');

        document.getElementById('fechaIni').value = '01'+'/'+'01'+'/'+vigencia;
        document.getElementById('fechaFin').value = diaActual+'/'+mesActual+'/'+vigencia;

        this.datosIniciales();

        this.fuentesOpciones();

        this.clasificadoresPresupuestales();
    },

    methods:
    {
        //Desglosar array
        toFormData: function(obj){
            let form_data = new FormData();
            for(let key in obj){
                form_data.append(key, obj[key]);
            }
            return form_data;
        },

        formatonumero: function(valor){
            return new Intl.NumberFormat("en-US", {style: "currency", currency: "USD"}).format(valor);
        },

        traeParametros: function(name){
            //Captura parametros
            name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
            let regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
            results = regex.exec(location.search);
            return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
        },

        async fuentesOpciones(){
            let fechaIn = document.getElementById('fechaIni').value;
            let partesFecha = fechaIn.split('/');
            let formDataDet = new FormData();
            formDataDet.append("vigencia", partesFecha[2])
            await axios.post('presupuesto_ccpet/clasificadores/ccp-generarclasificadoringresos.php', formDataDet)
                .then(
                    (response) => {
                        this.rubrosInicial = response.data.rubrosInicial;
                        this.accounts = response.data.accounts;
                        this.fuentes = response.data.fuentes;

                    }
                );
            this.separarRubrosFuentes();
            this.auxiliarMayor();

        },

        clasificadoresPresupuestales: async function(){
            await axios.post('presupuesto_ccpet/clasificadores/ccp-buscaclasificadoresingresos.php')
                .then(
                    (response) => {
                        this.clasificadores = response.data.clasificadores;
                        /* this.selectVigenciaGasto = this.vigenciasdelgasto[0][0]; */
                    }
                );
        },

        async orderAccounts(){

            /* const arrPuro = this.cuentasUsadas.map((e) => {
                return e[0]
            }); */

            const dataArrSet = new Set(this.cuentasUsadas);

            const dataArr = Array.from(dataArrSet);

            const resData = buscaNombreRubros(dataArr, this.accountsAux);

            this.results = ordenarArbol(this.accountsMayor, resData);

            this.resultsBuscar = [...this.results];

        },

        auxiliarMayor(){
            this.accountsAux = this.accounts.filter(element => element[2] == 'C');
            this.accountsMayor = this.accounts.filter(element => element[2] == 'A');
        },

        async separarRubrosFuentes(){
            await this.rubrosInicial.map((e) => {
                let partesRubros = e[0].split('-');
                this.fuentesUsadas.push(partesRubros[1]);
                this.cuentasUsadas.push(partesRubros[0]);
            });

            this.organizarFuentes();
            this.orderAccounts();
        },

        async organizarFuentes(){
            const dataArrSet = new Set(this.fuentesUsadas);
            const dataArr = Array.from(dataArrSet);
            const fuentesOrg = await dataArr.map((e) => {
                return {fuente : e, nombre : this.buscarFuente(e)}
            });
            this.optionsFuentes = fuentesOrg;
        },

        buscarFuente(fuente){
            const nombFuente = this.fuentes.find(e => e[0] == fuente)
            const nombF = typeof nombFuente == 'object' ? Object.values(nombFuente) : ''

            return typeof nombFuente == 'string' ? '' : nombF[1];
        },

        cambiaCriteriosBusqueda: function(){
            this.fuentesOpciones();
            this.detalles = [];
            this.arbolIngresos = [];
        },

        fuenteConNombre({ fuente, nombre }){
            return `${fuente} - ${nombre}`
        },

        datosIniciales: async function() {

            await axios.post('presupuesto_ccpet/grafIng/ccp-ejecuPresuIngresosGraf.php?action=datosIniciales')
            .then((response) => {

                this.seccionPresupuestal = response.data.secPresupuestal;
                this.vigenciaGasto = response.data.vigGasto;
                this.cuentaMaxLargo = response.data.cuentaMaxLargo;
            }).catch((error) => {
                this.error = true;
                console.log(error)
            }).finally(() => {
                this.loading = false;

            });
        },

        async cuentasDelClasificador(){

            this.results_det = [];
            let formDataDet = new FormData();
            formDataDet.append("id_clasificador", this.selectClasificador)
            await axios.post('presupuesto_ccpet/clasificadores/ccp-buscaclasificadoresingresos.php?action=search_clasificador_det_aux', formDataDet)
                .then(
                    (response) => {
                        this.results_det = response.data.clasificadores_det;
                        /* this.selectVigenciaGasto = this.vigenciasdelgasto[0][0]; */
                    }
                );
            this.cambiaCriteriosBusqueda();
        },

        traeDatosFormulario: async function() {

            await this.fuentesOpciones();

            let fechaIni = document.getElementById('fechaIni').value;
            let fechaFin = document.getElementById('fechaFin').value;
            this.arbolIngresos = [];
            if (fechaIni != "" && fechaFin != "") {

                this.loading = true;

                let formData1 = new FormData();

                for(let i=0; i <= this.results.length-1; i++){
                    const val = this.results[i].length-1;
                    for(let x = 0; x <= val; x++){
                        formData1.append("cuentas["+i+"][]", this.results[i][x]);
                    }
                }

                this.valueFuentes.forEach((e, i) => {
                    formData1.append("fuentes["+i+"]", e.fuente);
                });

                await axios.post('presupuesto_ccpet/grafIng/ccp-ejecuPresuIngresosGraf.php?action=datosFormulario&fechaIni='+fechaIni+'&fechaFin='+fechaFin+'&secPresu='+this.secPresu+'&medioPago='+this.medioPago+'&vigGasto='+this.vigGasto, formData1)
                .then((response) => {

                    console.log(response.data);
                    this.detalles = response.data.detalles;
                }).catch((error) => {
                    this.error = true;
                    console.log(error)
                }).finally(() => {

                    this.loading = false;

                });
                await this.buscarCuentasClasificador();
                this.cuentasMayoresAux();
                //console.log(this.arbolIngresos);
            }
            else {
                Swal.fire("Error", "Debes seleccionar fechas", "warning");
            }
        },

        async buscarCuentasClasificador(){
            if(this.results_det.length > 0){
                this.detalles = await this.filtrarPorClasificador(this.detalles);
            }
        },

        cuentasMayoresAux(){
            const partsPorNiveles = this.cuentaMaxLargo.split('.');

            const ordenPorNiveles = partsPorNiveles.reverse().map((e,i) => {
                return { nivel: partsPorNiveles.length - i, descontar: e.length + 1}
            });

            //sumar valores totales en tres variables, totalPresupuesto, totalRecaudado, porcentaje ejecutado
            /* this.detalles.forEach(element => {
                this.totalPresupuestado += parseInt(element[11]);
                this.totalRecaudado += parseInt(element[13]);
            }) */

            this.totalPresupuestado = 29850910663;
            this.totalRecaudado = 15476685048;


            this.porcentajeEjecutado = (this.totalRecaudado * 100) / this.totalPresupuestado;

            console.log(this.totalPresupuestado);
            console.log(this.totalRecaudado);

            this.detalles.forEach(element => {

                let resultado = this.accounts.find( cuenta => cuenta[0] === element[3] );
                let nivelCuenta = 0;

                if(resultado === undefined){
                    nivelCuenta = 1;
                    resultado = ["","","C",""];
                }else{
                    nivelCuenta = resultado[3];
                }

                let niveles = ordenPorNiveles.filter(niv => niv.nivel <= nivelCuenta);

                let restante = 0;

                niveles.forEach(elementNiv => {
                    let resultArbol = [];
                    let largo =  resultado[0].length-restante;

                    let cuentaABuscar = resultado[0].substring(0, largo);

                    let resultadoBusqueda = this.accounts.find(cuenta => cuenta[0] === cuentaABuscar );

                    if(resultadoBusqueda === undefined)resultadoBusqueda = ["","","C",""];
                    resultArbol = this.arbolIngresos.find(cuentaArbol => cuentaArbol[0] == resultadoBusqueda[0]);
                    if(resultArbol == undefined){

                        let ramas = [];
                        ramas.push(resultadoBusqueda[0]);
                        ramas.push(resultadoBusqueda[1]);
                        ramas.push(resultadoBusqueda[2]);
                        if(resultadoBusqueda[2] == 'C'){
                            ramas.push(element[1]);
                            ramas.push(element[2]);
                            ramas.push(element[5]);
                            ramas.push(element[17]);
                            ramas.push(element[6]);
                            ramas.push(element[7]);
                            ramas.push(element[18]);
                            ramas.push(element[8]);
                            ramas.push(element[9]);
                            ramas.push(element[10]);
                            ramas.push(element[11]);
                            ramas.push(element[12]);
                            ramas.push(element[13]);
                            ramas.push(element[14]);
                            ramas.push(element[15]);
                            ramas.push(element[16]);
                            ramas.push(element[19]);
                            ramas.push(element[20]);

                        }else{

                            let largoCuenta = resultadoBusqueda[0].length;
                            let arregloSum = this.detalles.filter(elementSum =>
                                (elementSum[3].substring(0, largoCuenta) == resultadoBusqueda[0])
                            );
                            let totalIni = 0;
                            let totalAd = 0;
                            let totalRed = 0;
                            let totalDef = 0;
                            let totalRecAnt = 0;
                            let totalRecConsulta = 0;
                            let totalRec = 0;
                            let totalPorRec = 0;
                            let enEjecucion = 0;
                            arregloSum.forEach(elementSuma => {
                                if(elementSuma[7] === ''){
                                    totalIni = parseFloat(totalIni) + parseFloat(elementSuma[8]);
                                    totalAd = parseFloat(totalAd) + parseFloat(elementSuma[9]);
                                    totalRed = parseFloat(totalRed) + parseFloat(elementSuma[10]);
                                    totalDef = parseFloat(totalDef) + parseFloat(elementSuma[11]);
                                    totalRecAnt = parseFloat(totalRecAnt) + parseFloat(elementSuma[12]);
                                    totalRecConsulta = parseFloat(totalRecConsulta) + parseFloat(elementSuma[13]);
                                    totalRec = parseFloat(totalRec) + parseFloat(elementSuma[14]);
                                    totalPorRec = parseFloat(totalPorRec) + parseFloat(elementSuma[15]);
                                }
                            });
                            if(totalDef == 0 && element[7] == ''){
                                enEjecucion = Math.round(((totalRec) * 100),2);
                            }else if(element[7] != ''){
                                enEjecucion = Math.round((0),2);
                            }else{
                                enEjecucion = Math.round(((totalRec/totalDef) * 100),2);
                            }
                            /*ramas.push('');
                            ramas.push('');*/
                            ramas.push('');
                            ramas.push('');
                            ramas.push('');
                            ramas.push('');
                            ramas.push('');
                            ramas.push('');
                            ramas.push('');
                            ramas.push(totalIni);
                            ramas.push(totalAd);
                            ramas.push(totalRed);
                            ramas.push(totalDef);
                            ramas.push(totalRecAnt);
                            ramas.push(totalRecConsulta);
                            ramas.push(totalRec);
                            ramas.push(totalPorRec);
                            ramas.push(enEjecucion);
                        }

                        this.arbolIngresos.push(ramas);
                    }else{
                        let resultArbolFuente = this.arbolIngresos.find(cuentaArbol => {
                            cuentaArbol[0] == element[3] && cuentaArbol[3] == element[1] &&
                             cuentaArbol[4] == element[2] && cuentaArbol[5] == element[5] &&
                              cuentaArbol[7] == element[6] && cuentaArbol[8] == element[7] &&
                              cuentaArbol[19] == element[19];
                            }
                        );
                        if(resultArbolFuente == undefined){

                            if(resultadoBusqueda[2] == 'C'){
                                let ramas = [];
                                ramas.push(resultadoBusqueda[0]);
                                ramas.push(resultadoBusqueda[1]);
                                ramas.push(resultadoBusqueda[2]);
                                ramas.push(element[1]);
                                ramas.push(element[2]);
                                ramas.push(element[5]);
                                ramas.push(element[17]);
                                ramas.push(element[6]);
                                ramas.push(element[7]);
                                ramas.push(element[18]);
                                ramas.push(element[8]);
                                ramas.push(element[9]);
                                ramas.push(element[10]);
                                ramas.push(element[11]);
                                ramas.push(element[12]);
                                ramas.push(element[13]);
                                ramas.push(element[14]);
                                ramas.push(element[15]);
                                ramas.push(element[16]);
                                ramas.push(element[19]);
                                ramas.push(element[20]);

                                this.arbolIngresos.push(ramas);
                            }
                        }
                    }
                    restante += elementNiv.descontar;
                })

            })

            this.arbolIngresos.sort()
        },

        async filtrarPorClasificador(filtrarRubros){

            const filtro = await filtrarRubros.map((element) => {
                /* let largeAccount = element[0].length; */
                let a = (this.results_det.find((e) => (e[0] == element[3] && e[1] == element[5])
                ));
                return a != undefined ? element : ''
            });
            const resp = filtro.filter((element) => element != '');
            return resp;
        },


        esBienesServicios(rubro){
            return rubro.substring(0, 9) == '1.1.02.05';
        },

        excel: function() {

            if (this.arbolIngresos != '') {

                document.form2.action="ccp-excelPresuIngresos.php";
				document.form2.target="_BLANK";
				document.form2.submit();
            }
            else {
                Swal.fire("Faltan datos", "warning");
            }
        },

        auxiliarDetalle: function(aux) {
            if (aux[2] == 'C') {

                let fechaIni = document.getElementById('fechaIni').value;
                let fechaFin = document.getElementById('fechaFin').value;

                let x = "ccp-auxiliarIngresosCuenta.php?vig="+aux[3]+"&sec="+aux[4]+"&cuenta="+aux[0]+"&fuente="+aux[5]+"&medio="+aux[7]+"&cpc="+aux[8]+"&fechaIni="+fechaIni+"&fechaFin="+fechaFin;
                window.open(x, '_blank');
                window.focus();
            }
        }
    }
});

Vue.component('line-chart2', {
    template: '<canvas ref="canvas" width="400" height="200"></canvas>',
    mounted() {
        // Crear el gráfico usando directamente Chart.js
        const ctx = this.$refs.canvas.getContext('2d');
        new Chart(ctx, {
            type: 'line',
            data: {
                labels: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre"],
                datasets: [
                {
                    label: "Presupuestado",
                    data: [21482, 22330, 22330, 22330, 22330, 22330, 22330, 29850, 29850, 29850],
                    borderColor: "rgba(54, 162, 235, 1)",
                    backgroundColor: "rgba(54, 162, 235, 0.2)",
                    fill: false
                },
                {
                    label: "Recaudado",
                    data: [1437, 2847, 7495, 8890, 10227, 13943, 15366, 15471, 15476, 15476],
                    borderColor: "rgba(255, 99, 132, 1)",
                    backgroundColor: "rgba(255, 99, 132, 0.2)",
                    fill: false
                }
                ]
            },
            options: {
                plugins: {
                    datalabels: {
                        anchor: 'end',
                        align: 'top',
                        formatter: function(value) {
                            return '$ ' + value.toLocaleString(); // Muestra el valor con separador de miles
                        },
                        color: '#000',
                        font: {
                            weight: 'bold',
                            size: 8
                        }
                    }
                },
                scales: {
                yAxes: [{
                    ticks: {
                        callback: function(value) {
                            return '$ ' + value.toLocaleString(); // Formato con separadores de miles
                        },
                        beginAtZero: true,
                        fontSize: 10
                    },
                    scaleLabel: {
                        display: true,
                        labelString: 'Valor en millones ($)',
                        fontSize: 14
                    }
                }]
                }
            },
            plugins: [ChartDataLabels]
        });
    }
});


Vue.component('line-chart3', {
    template: '<canvas ref="canvas" width="400" height="200"></canvas>',
    mounted() {
        // Crear el gráfico usando directamente Chart.js
        const ctx = this.$refs.canvas.getContext('2d');
        new Chart(ctx, {
            type: 'bar',
            data: {
                labels: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre"],
                datasets: [
                {
                    label: "Presupuestado",
                    data: [21482, 22330, 22330, 22330, 22330, 22330, 22330, 29850, 29850, 29850],
                    backgroundColor: "rgba(54, 162, 235, 0.6)"
                },
                {
                    label: "Recaudado",
                    data: [1437, 2847, 7495, 8890, 10227, 13943, 15366, 15471, 15476, 15476],
                    backgroundColor: "rgba(255, 99, 132, 0.6)"
                }
                ]
            },
            options: {
                plugins: {
                    datalabels: {
                        anchor: 'end',
                        align: 'top',
                        formatter: function(value) {
                            return '$ ' + value.toLocaleString(); // Muestra el valor con separador de miles
                        },
                        color: '#000',
                        font: {
                            weight: 'bold',
                            size: 8
                        }
                    }
                },
                scales: {
                    yAxes: [{
                        ticks: {
                            callback: function(value) {
                                return '$ ' + value.toLocaleString(); // Formato con separadores de miles
                            },
                            beginAtZero: true,
                            fontSize: 10
                        },
                        scaleLabel: {
                            display: true,
                            labelString: 'Valor en millones ($)',
                            fontSize: 14
                        }
                    }]
                }
            }
        });
    }
});


Vue.component('line-chart4', {
    template: '<canvas ref="canvas" width="400" height="200"></canvas>',
    mounted() {
        // Crear el gráfico usando directamente Chart.js
        const ctx = this.$refs.canvas.getContext('2d');

        // Calcular el total presupuestado y recaudado
        const totalPresupuestado = 22330 * 7 + 29850 * 3;
        const totalRecaudado = 1437 + 2847 + 7495 + 8890 + 10227 + 13943 + 15366 + 15471 + 15476 + 15476;
        new Chart(ctx, {
            type: 'pie',
            data: {
                labels: ["Presupuestado Total", "Recaudado Total"],
                datasets: [{
                    data: [totalPresupuestado, totalRecaudado],
                    backgroundColor: ["rgba(54, 162, 235, 0.6)", "rgba(255, 99, 132, 0.6)"]
                }]
            },
            options: {
                responsive: true,
                maintainAspectRatio: false,
                plugins: {
                    title: {
                        display: true,
                        text: "Presupuesto vs. Recaudación (en millones)",
                        font: {
                            size: 16
                        }
                    },
                    datalabels: {
                        anchor: 'end',
                        align: 'top',
                        formatter: function(value) {
                            return '$ ' + value.toLocaleString(); // Muestra el valor con separador de miles
                        },
                        color: '#000',
                        font: {
                            weight: 'bold',
                            size: 8
                        }
                    }
                },
            }
        });
    }
});
