<?php
    /*ini_set('display_errors', '1');
    ini_set('display_startup_errors', '1');
    error_reporting(E_ALL);*/
    date_default_timezone_set("America/Bogota");
    require_once '../../comun.inc';
    require '../../funciones.inc';
    require '../../funcionesSP.inc.php';

    session_start();
    if($_POST){
        $obj = new Plantilla();
        if($_POST['action']=="get"){
            $obj->getData();
        }else if($_POST['action']=="gen"){
            $obj->getInfo();
        }
    }

    class Plantilla{
        private $linkbd;
        public function __construct() {
            $this->linkbd = conectar_v7();
            $this->linkbd->set_charset("utf8");
        }
        public function getData(){
            if(!empty($_SESSION)){
                $arrData = array(
                    "secciones"=>$this->selectSecciones(),
                    "vigencias"=>$this->selectVigencias(),
                    "fuentes"=>$this->selectFuentes(),
                    "cuentas"=>$this->selectCuentas(),
                    "orden"=>$this->selectOrden()
                );
                echo json_encode($arrData,JSON_UNESCAPED_UNICODE);
            }
            die();
        }
        /******************Funciones para generar informe********************* */
        public function getInfo(){
            if(!empty($_SESSION)){
                if(empty($_POST['cuenta'])){
                    $arrResponse = array("status"=>false,"msg"=>"Error de datos");
                }else{
                    $arrData = [];
                    $strCuenta = $_POST['cuenta'];
                    $strFuente = $_POST['fuente'];
                    $strFechaInicial = $_POST['fecha_inicial'];
                    $strFechaFinal = $_POST['fecha_final'];
                    $intVigencia = $_POST['vigencia'];
                    $strSeccion = $_POST['seccion'];
                    $strPago = $_POST['pago'];
                    $intCpc = $_POST['cpc'];
                    $arrInicial = $this->selectInicial($strCuenta,$strSeccion,$intVigencia,$strPago,$strFuente,$strFechaInicial,$strFechaFinal);
                    $arrAdicion = $this->selectAdicion($strCuenta,$strSeccion,$intVigencia,$strPago,$strFuente,$strFechaInicial,$strFechaFinal);
                    $arrReduccion = $this->selectReduccion($strCuenta,$strSeccion,$intVigencia,$strPago,$strFuente,$strFechaInicial,$strFechaFinal);
                    $arrReservas = $this->selectReservas($strCuenta,$strSeccion,$intVigencia,$strPago,$strFuente,$strFechaInicial,$strFechaFinal);
                    $arrRecibos = $this->selectRecibos($strCuenta,$strSeccion,$intVigencia,$strPago,$strFuente,$strFechaInicial,$strFechaFinal);
                    $arrSuperavit = $this->selectSuperavit($strCuenta,$strSeccion,$intVigencia,$strPago,$strFuente,$strFechaInicial,$strFechaFinal);
                    $arrSinRecibo = $this->selectSinRecibo($strCuenta,$strSeccion,$intVigencia,$strPago,$strFuente,$strFechaInicial,$strFechaFinal);
                    $arrNotasBancarias = $this->selectNotasBancarias($strCuenta,$strSeccion,$intVigencia,$strPago,$strFuente,$strFechaInicial,$strFechaFinal);
                    $arrTransferencias = $this->selectTransferencias($strCuenta,$strSeccion,$intVigencia,$strPago,$strFuente,$strFechaInicial,$strFechaFinal);
                    $arrCumariboSp = $this->selectCumariboSp($strCuenta,$strSeccion,$intVigencia,$strPago,$strFuente,$strFechaInicial,$strFechaFinal);
                    $arrEgresos = $this->selectRetencionEgresos($strCuenta,$strSeccion,$intVigencia,$strPago,$strFuente,$strFechaInicial,$strFechaFinal);
                    $arrOtrosEgresos = $this->selectRetencionOtrosEgresos($strCuenta,$strSeccion,$intVigencia,$strPago,$strFuente,$strFechaInicial,$strFechaFinal);
                    $arrOrdenPago = $this->selectRetencionOrdenPago($strCuenta,$strSeccion,$intVigencia,$strPago,$strFuente,$strFechaInicial,$strFechaFinal);
                    $arrRecaudoFactura = $this->selectRecaudoFactura($strCuenta,$strSeccion,$intVigencia,$strPago,$strFuente,$strFechaInicial,$strFechaFinal,$intCpc);
                    $arrCumariboSpAntiguo = $this->selectCumariboSpAntiguo($strCuenta,$strSeccion,$intVigencia,$strPago,$strFuente,$strFechaInicial,$strFechaFinal,$intCpc);
                    $arrRecaudoFacturaAcuerdo = $this->selectRecaudoFacturaAcuerdo($strCuenta,$strSeccion,$intVigencia,$strPago,$strFuente,$strFechaInicial,$strFechaFinal,$intCpc);

                    $total = $arrReservas['total']+$arrRecibos['total']+$arrSuperavit['total']+$arrSinRecibo['total']+$arrNotasBancarias['total']+$arrTransferencias['total']
                    +$arrCumariboSp['total']+$arrEgresos['total']+$arrOtrosEgresos['total']+$arrOrdenPago['total']+$arrRecaudoFactura['total']+$arrCumariboSpAntiguo['total']
                    +$arrRecaudoFacturaAcuerdo['total'];

                    $definitivo = $arrInicial['total'] + $arrAdicion['total'] - $arrReduccion['total'];
                    $saldo = $definitivo-$total;
                    //Inicial
                    array_push($arrData,$arrInicial['data']);
                    //Adicion
                    array_push($arrData,$arrAdicion['data']);
                    //Reducción
                    array_push($arrData,$arrReduccion['data']);
                    //Definitivo
                    array_push($arrData,[array("tipo"=>"T","comprobante"=>"Definitivo","valor"=>$definitivo)]);
                    //Reservas
                    array_push($arrData,$arrReservas['data']);
                    //Recibos
                    array_push($arrData,$arrRecibos['data']);
                    //Superavit
                    array_push($arrData,$arrSuperavit['data']);
                    //Sin recibos
                    array_push($arrData,$arrSinRecibo['data']);
                    //Notas bancarias
                    array_push($arrData,$arrNotasBancarias['data']);
                    //Transferencias
                    array_push($arrData,$arrTransferencias['data']);
                    //Cumaribo
                    array_push($arrData,$arrCumariboSp['data']);
                    //Egresos
                    array_push($arrData,$arrEgresos['data']);
                    //Otros egresos
                    array_push($arrData,$arrOtrosEgresos['data']);
                    //Orden pago
                    array_push($arrData,$arrOrdenPago['data']);
                    //Facturas
                    array_push($arrData,$arrRecaudoFactura['data']);
                    //Cumaribo antiguo
                    array_push($arrData,$arrCumariboSpAntiguo['data']);
                    //Facturas acuerdo de pago
                    array_push($arrData,$arrRecaudoFacturaAcuerdo['data']);
                    //Total consulta
                    array_push($arrData,[array("tipo"=>"T","comprobante"=>"Total consulta","valor"=>$total)]);
                    //Total recaudos
                    array_push($arrData,[array("tipo"=>"T","comprobante"=>"Total recaudos","valor"=>$total)]);
                    //Saldo por recaudar
                    array_push($arrData,[array("tipo"=>"T","comprobante"=>"Saldo por recaudar","valor"=>$saldo)]);
                    $arrData = $this->fixArrayInfo($arrData);
                    $arrResponse = array("status"=>true,"html"=>$this->getHtml($arrData));
                }
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
            die();
        }
        public function getHtml($data){
            $html ="";
            foreach ($data as $row) {
                $strFecha = date_create($row['fecha']);
                $strFechaFormat = date_format($strFecha,"d/m/Y");
                if($row['tipo'] == "T"){
                    $html.='
                    <tr class="bg-secondary fw-bold">
                        <td>'.$row['comprobante'].'</td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td class="text-right">$'.number_format($row['valor']).'</td>
                        <td></td>
                    </tr>
                    ';
                }else{
                    $event = "";
                    $reflejar ="";
                    if(isset($row['url'])){
                        $link = "window.open('".$row['url']."','_blank')";
                        $event = 'ondblclick="'.$link.'"';
                    }
                    if(isset($row['reflejar'])){
                        $reflejar = '<a href="'.$row['reflejar'].'" target="_blank" class="btn btn-sm btn-success text-white">Reflejar</a>';
                    }
                    $html.='
                    <tr '.$event.'>
                        <td>'.$row['comprobante'].'</td>
                        <td>'.$row['consecutivo'].'</td>
                        <td>'.$row['tercero'].'</td>
                        <td>'.$row['objeto'].'</td>
                        <td>'.$strFechaFormat.'</td>
                        <td class="text-right">$'.number_format($row['valor']).'</td>
                        <td class="text-center">'.$reflejar.'</td>
                    </tr>
                    ';
                }
            }
            return $html;
        }
        public function fixArrayInfo(array $arrInfo){
            $arrData = [];
            foreach ($arrInfo as $cab) {
                foreach ($cab as $det) {
                    array_push($arrData,$det);
                }
            }
            return $arrData;
        }
        public function selectRecaudoFacturaAcuerdo($cuenta,$secPresupuestal,$vigGasto,$medioPago,$fuente,$strFechaInicial,$strFechaFinal,$cpc){
            $arrData = [];
            $vigenciaIni = explode("-",$strFechaInicial)[0];
            $vigenciaFin = explode("-",$strFechaFinal)[0];

            $vigGasto = " AND prc.vigencia_gasto = '$vigGasto'";
            $secPresupuestal = " AND prc.seccion_presupuestal = '$secPresupuestal'";
            $medioPago = " AND prc.medio_pago = '$medioPago'";
            $fuente = " AND prc.fuente = '$fuente'";
            $cpc = " AND prc.producto_servicio = '$cpc'";

            $sql = "SELECT prc.codigo_acuerdo, trc.fecha_acuerdo, prc.valor,trc.documento, trc.nombre_suscriptor
            FROM srv_acuerdo_ppto prc
            INNER JOIN srv_acuerdo_cab trc ON trc.codigo_acuerdo=prc.codigo_acuerdo
            WHERE prc.cuenta = '$cuenta' AND (prc.vigencia = '$vigenciaIni' OR prc.vigencia = '$vigenciaFin')
            AND NOT(trc.estado_acuerdo='Reversado') AND trc.fecha_acuerdo between '$strFechaInicial' AND '$strFechaFinal' $secPresupuestal
            $medioPago $vigGasto $fuente $cpc";
            $request = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
            $total = 0;
            if(!empty($request)){
                foreach ($request as $e) {
                    $estado = "";
                    $url = "serv-visualizarAcuerdoPago?id=".$e['codigo_acuerdo'];
                    array_push($arrData,array(
                        "url"=>$url,
                        "tipo"=>"D",
                        "comprobante"=>"Recaudo acuerdo servicios públicos",
                        "consecutivo"=>$e['codigo_acuerdo'],
                        "fecha"=>$e['fecha_acuerdo'],
                        "estado"=>$estado,
                        "valor"=>$e['valor'],
                        "tercero"=>$e['documento']."-".$e['nombre_suscriptor'],
                        "objeto"=>""
                    ));
                    $total+=$e['valor'];
                }
            }
            return array("data"=>$arrData,"total"=>$total);
        }
        public function selectCumariboSpAntiguo($cuenta,$secPresupuestal,$vigGasto,$medioPago,$fuente,$strFechaInicial,$strFechaFinal,$cpc){
            $arrData = [];

            $vigGasto = " AND servreciboscaja_det.vigencia_gasto = '$vigGasto'";
            $secPresupuestal = " AND servreciboscaja_det.seccion_presupuestal = '$secPresupuestal'";
            $medioPago = " AND servreciboscaja_det.medio_pago = '$medioPago'";
            $fuente = " AND servreciboscaja_det.fuente = '$fuente'";

            $sql = "SELECT servreciboscaja_det.id_recibos, servreciboscaja.fecha, servreciboscaja_det.valor
            FROM servreciboscaja_det, servreciboscaja
            WHERE servreciboscaja.estado =  'S' AND servreciboscaja.id_recibos = servreciboscaja_det.id_recibos AND servreciboscaja_det.cuentapres = '$cuenta'
            AND servreciboscaja.fecha BETWEEN '$strFechaInicial' AND '$strFechaFinal' $secPresupuestal $medioPago $vigGasto $fuente";

            $request = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
            $total = 0;
            if(!empty($request)){
                foreach ($request as $e) {
                    $estado = "";
                    $url = "serv-visualizar-recaudo.php?codRec=".$e['id_recibos'];
                    array_push($arrData,array(
                        "url"=>$url,
                        "tipo"=>"D",
                        "comprobante"=>"Recaudo servicios públicos",
                        "consecutivo"=>$e['id_recibos'],
                        "fecha"=>$e['fecha'],
                        "estado"=>$estado,
                        "valor"=>$e['valor'],
                        "tercero"=>"",
                        "objeto"=>""
                    ));
                    $total+=$e['valor'];
                }
            }
            return array("data"=>$arrData,"total"=>$total);
        }
        public function selectRecaudoFactura($cuenta,$secPresupuestal,$vigGasto,$medioPago,$fuente,$strFechaInicial,$strFechaFinal,$cpc){
            $arrData = [];
            $vigenciaIni = explode("-",$strFechaInicial)[0];
            $vigenciaFin = explode("-",$strFechaFinal)[0];

            $vigGasto = " AND prc.vigencia_gasto = '$vigGasto'";
            $secPresupuestal = " AND prc.seccion_presupuestal = '$secPresupuestal'";
            $medioPago = " AND prc.medio_pago = '$medioPago'";
            $fuente = " AND prc.fuente = '$fuente'";
            $cpc = " AND prc.producto_servicio = '$cpc'";

            $sql = "SELECT prc.codigo_recaudo, trc.fecha_recaudo, prc.valor,trc.documento as tercero,trc.concepto
            FROM srv_recaudo_factura_ppto prc
            INNER JOIN srv_recaudo_factura trc ON trc.codigo_recaudo=prc.codigo_recaudo
            WHERE prc.cuenta = '$cuenta' AND (prc.vigencia = '$vigenciaIni' OR prc.vigencia = '$vigenciaFin')
            AND NOT(trc.estado='REVERSADO') AND trc.fecha_recaudo between '$strFechaInicial' AND '$strFechaFinal' $secPresupuestal
            $medioPago $vigGasto $fuente $cpc";
            $request = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
            $total = 0;
            if(!empty($request)){
                foreach ($request as $e) {
                    $sql="SELECT cedulanit as documento,
                    CASE WHEN razonsocial IS NULL OR razonsocial = ''
                    THEN CONCAT(nombre1,' ',nombre2,' ',apellido1,' ',apellido2)
                    ELSE razonsocial END AS nombre FROM terceros  WHERE cedulanit = '$e[tercero]'";
                    $arrTercero = mysqli_query($this->linkbd,$sql)->fetch_assoc();
                    $estado = "";
                    $url = "serv-visualizar-recaudo.php?codRec=".$e['codigo_recaudo'];
                    array_push($arrData,array(
                        "url"=>$url,
                        "tipo"=>"D",
                        "comprobante"=>"Recaudo servicios públicos",
                        "consecutivo"=>$e['codigo_recaudo'],
                        "fecha"=>$e['fecha_recaudo'],
                        "estado"=>$estado,
                        "valor"=>$e['valor'],
                        "tercero"=>$arrTercero['documento']."-".$arrTercero['nombre'],
                        "objeto"=>$e['conceptorden']
                    ));
                    $total+=$e['valor'];
                }
            }
            return array("data"=>$arrData,"total"=>$total);
        }
        public function selectRetencionOrdenPago($cuenta,$secPresupuestal,$vigGasto,$medioPago,$fuente,$strFechaInicial,$strFechaFinal){
            $arrData = [];
            $vigenciaIni = explode("-",$strFechaInicial)[0];
            $vigenciaFin = explode("-",$strFechaFinal)[0];

            $vigGasto = " AND prc.vigencia_gasto = '$vigGasto'";
            $secPresupuestal = " AND prc.seccion_presupuestal = '$secPresupuestal'";
            $medioPago = " AND prc.medio_pago = '$medioPago'";
            $fuente = " AND prc.fuente = '$fuente'";

            $sql = "SELECT prc.idrecibo, trc.fecha, prc.valor,trc.tercero,trc.conceptorden
            FROM pptoretencionpago prc,tesoordenpago trc
            WHERE prc.cuenta = '$cuenta' AND (prc.vigencia='$vigenciaIni' OR prc.vigencia = '$vigenciaFin') AND trc.id_orden=prc.idrecibo
            AND NOT(trc.estado='N') AND trc.fecha between '$strFechaInicial' AND '$strFechaFinal' AND trc.tipo_mov='201' $secPresupuestal
            $medioPago $vigGasto $fuente AND prc.tipo='orden'
            AND NOT EXISTS (SELECT 1 FROM tesoordenpago tca WHERE tca.id_orden=trc.id_orden  AND tca.tipo_mov='401')";
            $request = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
            $total = 0;
            if(!empty($request)){
                foreach ($request as $e) {
                    $sql="SELECT cedulanit as documento,
                    CASE WHEN razonsocial IS NULL OR razonsocial = ''
                    THEN CONCAT(nombre1,' ',nombre2,' ',apellido1,' ',apellido2)
                    ELSE razonsocial END AS nombre FROM terceros  WHERE cedulanit = '$e[tercero]'";
                    $arrTercero = mysqli_query($this->linkbd,$sql)->fetch_assoc();
                    $estado = "";
                    $url = "teso-egresoverccpet.php?idop=".$e['idrecibo'];
                    array_push($arrData,array(
                        "url"=>$url,
                        "tipo"=>"D",
                        "comprobante"=>"Orden Pago",
                        "consecutivo"=>$e['idrecibo'],
                        "fecha"=>$e['fecha'],
                        "estado"=>$estado,
                        "valor"=>$e['valor'],
                        "tercero"=>$arrTercero['documento']."-".$arrTercero['nombre'],
                        "objeto"=>$e['conceptorden']
                    ));
                    $total+=$e['valor'];
                }
            }
            return array("data"=>$arrData,"total"=>$total);
        }
        public function selectRetencionOtrosEgresos($cuenta,$secPresupuestal,$vigGasto,$medioPago,$fuente,$strFechaInicial,$strFechaFinal){
            $arrData = [];
            $vigenciaIni = explode("-",$strFechaInicial)[0];
            $vigenciaFin = explode("-",$strFechaFinal)[0];

            $vigGasto = " AND prc.vigencia_gasto = '$vigGasto'";
            $secPresupuestal = " AND prc.seccion_presupuestal = '$secPresupuestal'";
            $medioPago = " AND prc.medio_pago = '$medioPago'";
            $fuente = " AND prc.fuente = '$fuente'";

            $sql = "SELECT prc.idrecibo, trc.fecha, prc.valor,trc.concepto,trc.tercero
            FROM pptoretencionotrosegresos prc,tesopagotercerosvigant trc
            WHERE prc.cuenta = '$cuenta' AND (prc.vigencia = '$vigenciaIni' OR prc.vigencia = '$vigenciaFin') AND trc.id_pago=prc.idrecibo
            AND NOT(trc.estado='N') AND trc.fecha between '$strFechaInicial' AND '$strFechaFinal' $secPresupuestal
            $medioPago $vigGasto AND prc.tipo='egreso' $fuente";
            $request = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
            $total = 0;
            if(!empty($request)){
                foreach ($request as $e) {
                    $sql="SELECT cedulanit as documento,
                    CASE WHEN razonsocial IS NULL OR razonsocial = ''
                    THEN CONCAT(nombre1,' ',nombre2,' ',apellido1,' ',apellido2)
                    ELSE razonsocial END AS nombre FROM terceros  WHERE cedulanit = '$e[tercero]'";
                    $arrTercero = mysqli_query($this->linkbd,$sql)->fetch_assoc();
                    $estado = "";
                    $url = "teso-editapagotercerosvigant.php?idpago=".$e['idrecibo'];
                    array_push($arrData,array(
                        "url"=>$url,
                        "tipo"=>"D",
                        "comprobante"=>"Otros Egresos",
                        "consecutivo"=>$e['idrecibo'],
                        "fecha"=>$e['fecha'],
                        "estado"=>$estado,
                        "valor"=>$e['valor'],
                        "tercero"=>$arrTercero['documento']."-".$arrTercero['nombre'],
                        "objeto"=>$e['concepto']
                    ));
                    $total+=$e['valor'];
                }
            }
            return array("data"=>$arrData,"total"=>$total);
        }
        public function selectRetencionEgresos($cuenta,$secPresupuestal,$vigGasto,$medioPago,$fuente,$strFechaInicial,$strFechaFinal){
            $arrData = [];
            $vigenciaIni = explode("-",$strFechaInicial)[0];
            $vigenciaFin = explode("-",$strFechaFinal)[0];

            $vigGasto = " AND prc.vigencia_gasto = '$vigGasto'";
            $secPresupuestal = " AND prc.seccion_presupuestal = '$secPresupuestal'";
            $medioPago = " AND prc.medio_pago = '$medioPago'";
            $fuente = " AND prc.fuente = '$fuente'";

            $sql = "SELECT prc.idrecibo, trc.fecha, prc.valor,trc.concepto,trc.tercero
            FROM pptoretencionpago prc,tesoegresos trc
            WHERE prc.cuenta = '$cuenta' AND (prc.vigencia = '$vigenciaIni' OR prc.vigencia = '$vigenciaFin') AND trc.id_egreso=prc.idrecibo
            AND NOT(trc.estado='N') AND trc.fecha between '$strFechaInicial' AND '$strFechaFinal' AND trc.tipo_mov='201' $secPresupuestal $medioPago
            $vigGasto AND prc.tipo='egreso' AND NOT EXISTS (SELECT 1 FROM tesoegresos tra WHERE tra.id_egreso=trc.id_egreso  AND tra.tipo_mov='401')
            $fuente";
            $request = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
            $total = 0;
            if(!empty($request)){
                foreach ($request as $e) {
                    $sql="SELECT cedulanit as documento,
                    CASE WHEN razonsocial IS NULL OR razonsocial = ''
                    THEN CONCAT(nombre1,' ',nombre2,' ',apellido1,' ',apellido2)
                    ELSE razonsocial END AS nombre FROM terceros  WHERE cedulanit = '$e[tercero]'";
                    $arrTercero = mysqli_query($this->linkbd,$sql)->fetch_assoc();
                    $estado = "";
                    $url = "teso-girarchequesver-ccpet.php?idegre=".$e['idrecibo'];
                    array_push($arrData,array(
                        "url"=>$url,
                        "tipo"=>"D",
                        "comprobante"=>"Egresos",
                        "consecutivo"=>$e['idrecibo'],
                        "fecha"=>$e['fecha'],
                        "estado"=>$estado,
                        "valor"=>$e['valor'],
                        "tercero"=>$arrTercero['documento']."-".$arrTercero['nombre'],
                        "objeto"=>$e['concepto']
                    ));
                    $total+=$e['valor'];
                }
            }
            return array("data"=>$arrData,"total"=>$total);
        }
        public function selectCumariboSp($cuenta,$secPresupuestal,$vigGasto,$medioPago,$fuente,$strFechaInicial,$strFechaFinal){
            $arrData = [];
            $vigenciaIni = explode("-",$strFechaInicial)[0];
            $vigenciaFin = explode("-",$strFechaFinal)[0];

            $vigGasto = " AND TB2.vigencia_gasto = '$vigGasto'";
            $secPresupuestal = " AND TB2.seccion_presupuestal = '$secPresupuestal'";
            $medioPago = " AND TB2.medio_pago = '$medioPago'";
            $fuente = " AND TB2.fuente = '$fuente'";

            $sql = "SELECT TB2.idrecibo, TB1.fecha, TB2.valor,b.tercero
            from tesosinreciboscajasp TB1,pptosinrecibocajaspppto TB2
            LEFT JOIN tesobancosctas b ON TB1.cuentabanco = b.cuenta
            WHERE TB1.id_recibos=TB2.idrecibo AND TB1.estado='S' AND (TB2.vigencia = '$vigenciaIni' OR TB2.vigencia = '$vigenciaFin')
            AND TB1.vigencia=TB2.vigencia AND TB2.cuenta = '$cuenta' AND TB1.fecha between '$strFechaInicial' AND '$strFechaFinal'
            $secPresupuestal $medioPago $vigGasto $fuente";
            $request = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
            $total = 0;
            if(!empty($request)){
                foreach ($request as $e) {
                    $sql="SELECT cedulanit as documento,
                    CASE WHEN razonsocial IS NULL OR razonsocial = ''
                    THEN CONCAT(nombre1,' ',nombre2,' ',apellido1,' ',apellido2)
                    ELSE razonsocial END AS nombre FROM terceros  WHERE cedulanit = '$e[tercero]'";
                    $arrTercero = mysqli_query($this->linkbd,$sql)->fetch_assoc();
                    $estado = "";
                    array_push($arrData,array(
                        "tipo"=>"D",
                        "comprobante"=>"Recaudos servicios publicos Cumaribo",
                        "consecutivo"=>$e['idrecibo'],
                        "fecha"=>$e['fecha'],
                        "estado"=>$estado,
                        "valor"=>$e['valor'],
                        "tercero"=>$arrTercero['documento']."-".$arrTercero['nombre'],
                        "objeto"=>""
                    ));
                    $total+=$e['valor'];
                }
            }
            return array("data"=>$arrData,"total"=>$total);
        }
        public function selectTransferencias($cuenta,$secPresupuestal,$vigGasto,$medioPago,$fuente,$strFechaInicial,$strFechaFinal){
            $arrData = [];
            $vigenciaIni = explode("-",$strFechaInicial)[0];
            $vigenciaFin = explode("-",$strFechaFinal)[0];

            $vigGasto = " AND pitp.vigencia_gasto = '$vigGasto'";
            $secPresupuestal = " AND pitp.seccion_presupuestal = '$secPresupuestal'";
            $medioPago = " AND pitp.medio_pago = '$medioPago'";
            $fuente = " AND pitp.fuente = '$fuente'";

            $sql = "SELECT pitp.idrecibo, titp.fecha, pitp.valor,titp.concepto,titp.tercero
            FROM pptoingtranppto pitp,tesorecaudotransferencia titp
            WHERE pitp.cuenta = '$cuenta' AND (pitp.vigencia = '$vigenciaIni' OR pitp.vigencia = '$vigenciaFin') AND pitp.idrecibo=titp.id_recaudo
            AND NOT(titp.estado='N' OR titp.estado='R') AND titp.fecha between '$strFechaInicial' AND '$strFechaFinal' $secPresupuestal
            $medioPago $vigGasto $fuente";
            $request = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
            $total = 0;
            if(!empty($request)){
                foreach ($request as $e) {
                    $sql="SELECT cedulanit as documento,
                    CASE WHEN razonsocial IS NULL OR razonsocial = ''
                    THEN CONCAT(nombre1,' ',nombre2,' ',apellido1,' ',apellido2)
                    ELSE razonsocial END AS nombre FROM terceros  WHERE cedulanit = '$e[tercero]'";
                    $arrTercero = mysqli_query($this->linkbd,$sql)->fetch_assoc();
                    $estado = "";
                    $url = "teso-editarecaudotransferencia.php?idrecaudo=".$e['idrecibo'];
                    $reflejar = "ccp-recaudotransferencia-reflejar.php?idrecaudo=".$e['idrecibo'];
                    array_push($arrData,array(
                        "reflejar"=>$reflejar,
                        "url"=>$url,
                        "tipo"=>"D",
                        "comprobante"=>"Recaudos Transferencias",
                        "consecutivo"=>$e['idrecibo'],
                        "fecha"=>$e['fecha'],
                        "estado"=>$estado,
                        "valor"=>$e['valor'],
                        "tercero"=>$arrTercero['documento']."-".$arrTercero['nombre'],
                        "objeto"=>$e['concepto']
                    ));
                    $total+=$e['valor'];
                }
            }
            return array("data"=>$arrData,"total"=>$total);
        }
        public function selectNotasBancarias($cuenta,$secPresupuestal,$vigGasto,$medioPago,$fuente,$strFechaInicial,$strFechaFinal){
            $arrData = [];
            $vigenciaIni = explode("-",$strFechaInicial)[0];
            $vigenciaFin = explode("-",$strFechaFinal)[0];

            $vigGasto = " AND pnb.vigencia_gasto = '$vigGasto'";
            $secPresupuestal = " AND pnb.seccion_presupuestal = '$secPresupuestal'";
            $medioPago = " AND pnb.medio_pago = '$medioPago'";
            $fuente = " AND pnb.fuente = '$fuente'";

            $sql = "SELECT pnb.idrecibo, tnp.fecha, pnb.valor,tnp.concepto
            FROM pptonotasbanppto pnb,tesonotasbancarias_cab tnp
            WHERE pnb.cuenta = '$cuenta' AND (pnb.vigencia = '$vigenciaIni' OR pnb.vigencia = '$vigenciaFin')
            AND tnp.id_comp=pnb.idrecibo AND NOT(tnp.estado='R' OR tnp.estado='N')  AND (tnp.vigencia = '$vigenciaIni' OR tnp.vigencia = '$vigenciaFin')
            AND tnp.fecha BETWEEN '$strFechaInicial' AND '$strFechaFinal' $secPresupuestal
            $medioPago $vigGasto $fuente";
            $request = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
            $total = 0;
            if(!empty($request)){
                foreach ($request as $e) {
                    $estado = "";
                    $url = "teso-editanotasbancarias_new.php?id=".$e['idrecibo'];
                    array_push($arrData,array(
                        "url"=>$url,
                        "tipo"=>"D",
                        "comprobante"=>"Notas bancarias",
                        "consecutivo"=>$e['idrecibo'],
                        "fecha"=>$e['fecha'],
                        "estado"=>$estado,
                        "valor"=>$e['valor'],
                        "tercero"=>"",
                        "objeto"=>$e['concepto']
                    ));
                    $total+=$e['valor'];
                }
            }
            return array("data"=>$arrData,"total"=>$total);
        }
        public function selectSinRecibo($cuenta,$secPresupuestal,$vigGasto,$medioPago,$fuente,$strFechaInicial,$strFechaFinal){
            $arrData = [];
            $vigenciaIni = explode("-",$strFechaInicial)[0];
            $vigenciaFin = explode("-",$strFechaFinal)[0];

            $vigGasto = " AND psrc.vigencia_gasto = '$vigGasto'";
            $secPresupuestal = " AND psrc.seccion_presupuestal = '$secPresupuestal'";
            $medioPago = " AND psrc.medio_pago = '$medioPago'";
            $fuente = " AND psrc.fuente = '$fuente'";

            $sql="SELECT psrc.idrecibo, tsrc.fecha, psrc.valor,b.tercero, tsrc.id_recaudo
            FROM pptosinrecibocajappto psrc,tesosinreciboscaja tsrc
            LEFT JOIN tesobancosctas b ON tsrc.cuentabanco = b.cuenta
            WHERE psrc.cuenta = '$cuenta' AND (psrc.vigencia=$vigenciaIni OR psrc.vigencia = '$vigenciaFin') AND tsrc.id_recibos=psrc.idrecibo
            AND NOT(tsrc.estado='N' OR tsrc.estado='R') AND tsrc.fecha BETWEEN '$strFechaInicial' AND '$strFechaFinal' $medioPago
            $secPresupuestal $vigGasto $fuente";
            $request = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
            $total = 0;
            if(!empty($request)){
                foreach ($request as $e) {
                    $sql="SELECT cedulanit as documento,
                    CASE WHEN razonsocial IS NULL OR razonsocial = ''
                    THEN CONCAT(nombre1,' ',nombre2,' ',apellido1,' ',apellido2)
                    ELSE razonsocial END AS nombre FROM terceros  WHERE cedulanit = '$e[tercero]'";
                    $arrTercero = mysqli_query($this->linkbd,$sql)->fetch_assoc();
                    $sqlObj = "SELECT concepto FROM tesosinrecaudos WHERE id_recaudo = '$e[id_recaudo]'";
                    $requestObj = mysqli_query($this->linkbd,$sqlObj)->fetch_assoc();
                    //dep($requestObj);
                    $estado = "";
                    $url = "teso-ingresoInternoEditar?id=".$e['idrecibo'];
                    $reflejar = "ccp-sinrecibocaja-reflejar.php?idrecibo=".$e['idrecibo'];
                    array_push($arrData,array(
                        "reflejar"=>$reflejar,
                        "url"=>$url,
                        "tipo"=>"D",
                        "comprobante"=>"Ingresos internos",
                        "consecutivo"=>$e['idrecibo'],
                        "fecha"=>$e['fecha'],
                        "estado"=>$estado,
                        "valor"=>$e['valor'],
                        "tercero"=>$arrTercero['documento']."-".$arrTercero['nombre'],
                        "objeto"=>$requestObj['concepto']
                    ));
                    $total+=$e['valor'];
                }
            }
            return array("data"=>$arrData,"total"=>$total);
        }
        public function selectSuperavit($cuenta,$secPresupuestal,$vigGasto,$medioPago,$fuente,$strFechaInicial,$strFechaFinal){
            $arrData = [];
            $vigGasto = " AND vigencia_gasto = '$vigGasto'";
            $secPresupuestal = " AND tsad.seccion_presupuestal = '$secPresupuestal'";
            $medioPago = " AND ptsad.medio_pago = '$medioPago'";
            $fuente = " AND tsad.fuente = '$fuente'";

            $sql = "SELECT tsa.id, tsa.fecha, tsad.valor,tsa.concepto
            FROM tesosuperavit AS tsa, tesosuperavit_det AS tsad
            WHERE tsa.estado = 'S' AND tsa.id = tsad.id_tesosuperavit AND tsa.fecha
            BETWEEN '$strFechaInicial' AND '$strFechaFinal' AND tsad.rubro = '$cuenta' $vigGasto
            $fuente $secPresupuestal $medioPago";
            $request = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
            $total = 0;
            if(!empty($request)){
                foreach ($request as $e) {
                    $estado = "";
                    $url = "teso-verSuperavit-ccpet.php?is=".$e['id'];
                    array_push($arrData,array(
                        "url"=>$url,
                        "tipo"=>"D",
                        "comprobante"=>"Superavit",
                        "consecutivo"=>$e['id'],
                        "fecha"=>$e['fecha'],
                        "estado"=>$estado,
                        "valor"=>$e['valor'],
                        "tercero"=>"",
                        "objeto"=>$e['concepto']
                    ));
                    $total+=$e['valor'];
                }
            }
            return array("data"=>$arrData,"total"=>$total);
        }
        public function selectRecibos($cuenta,$secPresupuestal,$vigGasto,$medioPago,$fuente,$strFechaInicial,$strFechaFinal){
            $arrData = [];
            $vigenciaIni = explode("-",$strFechaInicial)[0];
            $vigenciaFin = explode("-",$strFechaFinal)[0];

            $vigGasto = " AND prc.vigencia_gasto = '$vigGasto'";
            $secPresupuestal = " AND prc.seccion_presupuestal = '$secPresupuestal'";
            $medioPago = " AND prc.medio_pago = '$medioPago'";
            $fuente = " AND prc.fuente = '$fuente'";

            $sql = "SELECT prc.idrecibo, trc.fecha, prc.valor, b.tercero,trc.descripcion
            FROM pptorecibocajappto prc,tesoreciboscaja trc
            LEFT JOIN tesobancosctas b ON trc.cuentabanco = b.cuenta
            WHERE prc.cuenta = '$cuenta' AND (prc.vigencia = $vigenciaIni OR prc.vigencia = $vigenciaFin)
            AND trc.id_recibos=prc.idrecibo AND NOT(trc.estado='N' OR trc.estado='R') AND NOT(prc.tipo='R')
            AND trc.fecha between '$strFechaInicial' AND '$strFechaFinal' $secPresupuestal $medioPago $vigGasto $fuente";
            $request = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
            $total = 0;
            if(!empty($request)){
                foreach ($request as $e) {
                    $sql="SELECT cedulanit as documento,
                    CASE WHEN razonsocial IS NULL OR razonsocial = ''
                    THEN CONCAT(nombre1,' ',nombre2,' ',apellido1,' ',apellido2)
                    ELSE razonsocial END AS nombre FROM terceros  WHERE cedulanit = '$e[tercero]'";
                    $arrTercero = mysqli_query($this->linkbd,$sql)->fetch_assoc();
                    $estado = "";
                    $url = "teso-recibocajaver.php?idrecibo=".$e['idrecibo'];
                    $reflejar = "ccp-recibocaja-reflejarppto?idrecibo=".$e['idrecibo'];
                    array_push($arrData,array(
                        "reflejar"=>$reflejar,
                        "url"=>$url,
                        "tipo"=>"D",
                        "comprobante"=>"Recibo de caja",
                        "consecutivo"=>$e['idrecibo'],
                        "fecha"=>$e['fecha'],
                        "estado"=>$estado,
                        "valor"=>$e['valor'],
                        "tercero"=>$arrTercero['documento']."-".$arrTercero['nombre'],
                        "objeto"=>$e['descripcion']
                    ));
                    $total+=$e['valor'];
                }
            }
            return array("data"=>$arrData,"total"=>$total);
        }
        public function selectReservas($cuenta,$secPresupuestal,$vigGasto,$medioPago,$fuente,$strFechaInicial,$strFechaFinal){
            $arrData = [];
            $fuente = " AND tsad.fuente = '$fuente'";
            $vigGasto = " AND tsad.codigo_vigenciag = '$vigGasto'";
            $secPresupuestal = " AND tsad.clasificacion = '$secPresupuestal'";
            $medioPago = " AND tsad.clasificador = '$medioPago'";

            $sql = "SELECT tsa.id, tsa.fecha, tsa.valor
            FROM tesoreservas AS tsa, tesoreservas_det AS tsad
            WHERE tsa.estado = 'S' AND tsa.id = tsad.id_reserva AND tsa.fecha
            BETWEEN '$strFechaInicial' AND '$strFechaFinal' AND tsad.rubro = '$cuenta' $fuente $secPresupuestal $medioPago $vigGasto
            GROUP BY tsad.fuente, tsad.vigencia_gasto";
            $request = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
            $total = 0;
            if(!empty($request)){
                foreach ($request as $e) {
                    $estado = $e['estado'] == 'S' ? 'Activo' : 'Anulado';
                    $url = "teso-verReservas-ccpet.php?is=".$e['id'];
                    array_push($arrData,array(
                        "url"=>$url,
                        "tipo"=>"D",
                        "comprobante"=>"Reservas",
                        "consecutivo"=>$e['id'],
                        "fecha"=>$e['fecha'],
                        "estado"=>$estado,
                        "valor"=>$e['valor'],
                        "tercero"=>"",
                        "objeto"=>""
                    ));
                    $total+=$e['valor'];
                }
            }
            return array("data"=>$arrData,"total"=>$total);
        }
        public function selectReduccion($cuenta,$secPresupuestal,$vigGasto,$medioPago,$fuente,$strFechaInicial,$strFechaFinal){
            $arrData = [];
            $vigGasto = " AND codigo_vigenciag = '$vigGasto'";
            $secPresupuestal = " AND seccion_presupuestal = '$secPresupuestal'";
            $medioPago = " AND medio_pago = '$medioPago'";

            $sql = "SELECT id_adicion, fecha, valor, estado FROM ccpet_reducciones
            WHERE cuenta = '$cuenta' AND tipo_cuenta = 'I' $secPresupuestal $medioPago $vigGasto AND fuente = '$fuente'
            AND estado = 'S' AND fecha BETWEEN '$strFechaInicial' AND '$strFechaFinal'";
            $request = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
            $total = 0;
            if(!empty($request)){
                foreach ($request as $e) {
                    $estado = $e['estado'] == 'S' ? 'Activo' : 'Anulado';
                    $url = "ccp-visualizaAdicion.php?id=".$e['id_adicion'];
                    array_push($arrData,array(
                        "url"=>$url,
                        "tipo"=>"D",
                        "comprobante"=>"Reducción",
                        "consecutivo"=>$e['id_adicion'],
                        "fecha"=>$e['fecha'],
                        "estado"=>$estado,
                        "valor"=>$e['valor'],
                        "tercero"=>"",
                        "objeto"=>""
                    ));
                    $total+=$e['valor'];
                }
            }
            array_push($arrData,array(
                "tipo"=>"T",
                "comprobante"=>"Total reducción",
                "valor"=>$total,
            ));
            return array("data"=>$arrData,"total"=>$total);
        }
        public function selectAdicion($cuenta,$secPresupuestal,$vigGasto,$medioPago,$fuente,$strFechaInicial,$strFechaFinal){
            $arrData = [];
            $vigGasto = " AND codigo_vigenciag = '$vigGasto'";
            $secPresupuestal = " AND seccion_presupuestal = '$secPresupuestal'";
            $medioPago = " AND medio_pago = '$medioPago'";

            $sql = "SELECT id_acuerdo, fecha, valor, estado FROM ccpet_adiciones
            WHERE cuenta = '$cuenta' AND tipo_cuenta = 'I' $secPresupuestal $medioPago $vigGasto AND fuente = '$fuente'
            AND estado = 'S' AND fecha BETWEEN '$strFechaInicial' AND '$strFechaFinal'";
            $request = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
            $total = 0;
            if(!empty($request)){
                foreach ($request as $e) {
                    $estado = $e['estado'] == 'S' ? 'Activo' : 'Anulado';
                    $url = "ccp-visualizaAdicion.php?id=".$e['id_acuerdo'];
                    array_push($arrData,array(
                        "url"=>$url,
                        "tipo"=>"D",
                        "comprobante"=>"Adición",
                        "consecutivo"=>$e['id_acuerdo'],
                        "fecha"=>$e['fecha'],
                        "estado"=>$estado,
                        "valor"=>$e['valor'],
                        "tercero"=>"",
                        "objeto"=>""
                    ));
                    $total+=$e['valor'];
                }
            }
            array_push($arrData,array(
                "tipo"=>"T",
                "comprobante"=>"Total adición",
                "valor"=>$total,
            ));
            return array("data"=>$arrData,"total"=>$total);
        }
        public function selectInicial($cuenta,$secPresupuestal,$vigGasto,$medioPago,$fuente,$vigenciaIni,$vigenciaFin){
            $arrData = [];
            $vigenciaIni = explode("-",$vigenciaIni)[0];
            $vigenciaFin = explode("-",$vigenciaFin)[0];

            $vigGasto = " AND vigencia_gasto = '$vigGasto'";
            $secPresupuestal = " AND seccion_presupuestal = '$secPresupuestal'";
            $medioPago = " AND medio_pago = '$medioPago'";

            $sql = "SELECT COALESCE(valor,0) as valor, vigencia
            FROM ccpetinicialing
            WHERE cuenta = '$cuenta' $secPresupuestal $vigGasto $medioPago AND fuente = '$fuente'
            AND (vigencia = '$vigenciaIni' OR vigencia = '$vigenciaFin')";
            $request = mysqli_query($this->linkbd,$sql)->fetch_assoc();
            $total = $request['valor'];
            array_push($arrData,array(
                "tipo"=>"D",
                "comprobante"=>"Presupuesto inicial",
                "consecutivo"=>"",
                "fecha"=>$request['vigencia'],
                "estado"=>"",
                "valor"=>$request['valor'],
                "tercero"=>"",
                "objeto"=>""
            ));
            return array("data"=>$arrData,"total"=>$total);
        }
        /******************Funciones para datos iniciales********************* */
        public function selectSecciones(){
            $sql = "SELECT * FROM pptoseccion_presupuestal WHERE estado = 'S'";
            $request = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
            return $request;
        }
        public function selectVigencias(){
            $sql = "SELECT * FROM ccpet_vigenciadelgasto WHERE version = (SELECT MAX(version) FROM ccpet_vigenciadelgasto)";
            $request = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
            return $request;
        }
        public function selectFuentes(){
            $sql="SELECT codigo_fuente as codigo, nombre
            FROM ccpet_fuentes_cuipo
            WHERE version = '1' ORDER BY codigo_fuente";
            $request = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
            return $request;
        }
        public function selectCuentas(){
            $sql = "SELECT id,codigo, nombre, tipo, nivel
            FROM cuentasingresosccpet WHERE tipo ='C' ORDER BY id ASC";
            $request = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
            return $request;
        }
        public function selectOrden(){
            $sql="SELECT orden FROM ccpet_parametros";
            $request = mysqli_query($this->linkbd,$sql)->fetch_assoc()['orden'];
            return $request;
        }
    }
?>
