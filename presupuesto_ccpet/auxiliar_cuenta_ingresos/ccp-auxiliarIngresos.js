const URL ='presupuesto_ccpet/auxiliar_cuenta_ingresos/ccp-auxiliarIngresos.php';

var app = new Vue({
    el:"#myapp",
    data() {
        return {
            isLoading:false,
            isModal:false,
            isModalFuente:false,
            txtFechaInicial:new Date(new Date().getFullYear(),0,1).toISOString().split("T")[0],
            txtFechaFinal:new Date().toISOString().split("T")[0],
            txtSearch:"",
            txtSearchFuente:"",
            txtSearchBpim:"",
            txtSearchProgramatico:"",
            txtResultados:0,
            txtResultadosFuentes:0,
            txtResultadosBpim:0,
            txtResultadosProgramatico:0,
            txtBpim:"",
            txtCpc:"",
            txtProgramatico:"",
            selectSecciones:0,
            selectVigencias:0,
            selectPago:0,
            arrSecciones:[],
            arrVigencias:[],
            arrCuentas:[],
            arrFuentes:[],
            arrCuentasCopy:[],
            arrFuentesCopy:[],
            objCuenta:{codigo:"",nombre:""},
            objFuente:{codigo:"",nombre:""},
        }
    },
    mounted() {
        this.getData();
    },
    methods: {
        getData: async function(){
            const formData = new FormData();
            formData.append("action","get");
            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();
            this.arrSecciones = objData.secciones;
            this.arrVigencias = objData.vigencias;
            this.arrCuentas = objData.cuentas;
            this.arrFuentes = objData.fuentes;
            this.arrCuentasCopy = objData.cuentas;
            this.arrFuentesCopy = objData.fuentes;
            this.txtResultados = this.arrCuentas.length;
            this.txtResultadosFuentes = this.arrFuentes.length;
            this.genDataByGet();
        },
        search:function(type=""){
            let search = "";
            if(type == "modal")search = this.txtSearch.toLowerCase();
            if(type == "modal_fuente")search = this.txtSearchFuente.toLowerCase();
            if(type == "cod_cuenta") search = this.objCuenta.codigo;
            if(type == "cod_fuente") search = this.objFuente.codigo;

            if(type=="modal"){
                this.arrCuentasCopy = [...this.arrCuentas.filter(e=>e.codigo.toLowerCase().includes(search)
                || e.nombre.toLowerCase().includes(search))];
                this.txtResultados = this.arrCuentasCopy.length;
            }else if(type == "modal_fuente"){
                this.arrFuentesCopy = [...this.arrFuentes.filter(e=>e.codigo.toLowerCase().includes(search)
                    || e.nombre.toLowerCase().includes(search))];
                this.txtResultadosFuentes = this.arrFuentesCopy.length;
            }else if(type == "cod_cuenta"){
                this.objCuenta = {};
                this.objCuenta = {...this.arrCuentas.filter(e=>e.codigo==search)[0]};
            }else if(type == "cod_fuente"){
                this.objFuente = {};
                this.objFuente = {...this.arrFuentes.filter(e=>e.codigo==search)[0]};
            }
        },
        genDataByGet: async function(){
            let fecha_inicial = new URLSearchParams(window.location.search).get('fechaIni');
            let fecha_final = new URLSearchParams(window.location.search).get('fechaFin');;
            let cuenta = new URLSearchParams(window.location.search).get('cuenta');
            let fuente = new URLSearchParams(window.location.search).get('fuente');
            let vigencia = new URLSearchParams(window.location.search).get('vig');
            let seccion = new URLSearchParams(window.location.search).get('sec');
            let pago = new URLSearchParams(window.location.search).get('medio');
            let cpc = new URLSearchParams(window.location.search).get('cpc');
            if(cuenta != null){
                let arrFechaInicial = fecha_inicial.split("/");
                let arrFechaFinal = fecha_final.split("/");
                fecha_inicial = arrFechaInicial[2]+"-"+arrFechaInicial[1]+"-"+arrFechaInicial[0];
                fecha_final = arrFechaFinal[2]+"-"+arrFechaFinal[1]+"-"+arrFechaFinal[0];
                this.objCuenta = {...this.arrCuentas.filter(e=>e.codigo==cuenta)[0]};
                this.objFuente = fuente != null ? {...this.arrFuentes.filter(e=>e.codigo==fuente)[0]} :"";
                this.txtFechaInicial = fecha_inicial;
                this.txtFechaFinal = fecha_final;
                this.selectVigencias = vigencia;
                this.selectSecciones = seccion;
                this.selectPago = pago;
                this.txtCpc = cpc;
                const formData = new FormData();
                formData.append("action","gen");
                formData.append("fecha_inicial",this.txtFechaInicial);
                formData.append("fecha_final",this.txtFechaFinal);
                formData.append("cuenta",this.objCuenta.codigo);
                formData.append("fuente",this.objFuente.codigo ? this.objFuente.codigo : "");
                formData.append("vigencia",this.selectVigencias);
                formData.append("seccion",this.selectSecciones);
                formData.append("pago",this.selectPago);
                formData.append("cpc",this.txtCpc);
                this.isLoading = true;
                const response = await fetch(URL,{method:"POST",body:formData});
                const objData = await response.json();
                if(objData.status){
                    this.$refs.tableData.innerHTML = objData.html;
                }
                this.isLoading = false;
            }
        },
        genData: async function(){
            if(this.objCuenta.codigo ==""){
                Swal.fire("Error","Todos los campos con (*) son obligatorios","error");
                return false;
            }
            const formData = new FormData();
            formData.append("action","gen");
            formData.append("fecha_inicial",this.txtFechaInicial);
            formData.append("fecha_final",this.txtFechaFinal);
            formData.append("cuenta",this.objCuenta.codigo);
            formData.append("fuente",this.objFuente.codigo ? this.objFuente.codigo : "");
            formData.append("vigencia",this.selectVigencias);
            formData.append("seccion",this.selectSecciones);
            formData.append("pago",this.selectPago);
            formData.append("cpc",this.txtCpc);
            this.isLoading = true;
            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();
            if(objData.status){
                this.$refs.tableData.innerHTML = objData.html;
            }else{
                Swal.fire("Error",objData.msg,"error");
            }
            this.isLoading = false;
        },
        selectItem:function(type="",{...item}){
            if(type == "modal"){
                this.objCuenta = item;
                this.isModal = false;
            }else if(type == "modal_fuente"){
                this.objFuente = item;
                this.isModalFuente = false;
            }
        },
        printExcel:function(){
            if(this.objCuenta.codigo ==""){
                Swal.fire("Error","Todos los campos con (*) son obligatorios","error");
                return false;
            }
            let fuente = this.objFuente.codigo ? this.objFuente.codigo : "";
            let nombreFuente = this.objFuente.codigo ? this.objFuente.nombre : "";
            window.open("ccp-auxiliarIngresosCuentaExcel.php?fecha_inicial="+this.txtFechaInicial
            +"&fecha_final="+this.txtFechaFinal+"&cuenta="+this.objCuenta.codigo+"&fuente="+fuente
            +"&vigencia="+this.selectVigencias+"&seccion="+this.selectSecciones+"&pago="+this.selectPago
            +"&nombre_fuente="+nombreFuente+"&nombre_cuenta="+this.objCuenta.nombre+"&cpc="+this.txtCpc
            ,"_blank");
        },
        formatNumero: function(valor){
            return new Intl.NumberFormat("en-US", {style: "currency", currency: "USD"}).format(valor);
        },
    },
    computed:{

    }
})
