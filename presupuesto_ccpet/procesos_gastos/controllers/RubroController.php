<?php
    require_once '../../../Librerias/core/Helpers.php';
    require_once '../../../PHPExcel/Classes/PHPExcel.php';
    require_once '../models/RubroModel.php';
    session_start();

    class RubroController extends RubroModel{
        public function initialData(){
            if(!empty($_SESSION)){
                $arrResponse = array();
                $arrProyectos = $this->selectBpim();
                $arrFuentes = $this->selectFuentes();
                $arrVigencias = $this->selectVigencias();
                $totalProyectos = count($arrProyectos);
                if($totalProyectos > 0){
                    for ($i=0; $i < $totalProyectos; $i++) {
                        $element = $arrProyectos[$i];
                        $arrProyectos[$i]['is_saved'] = 0;
                        if($arrProyectos[$i]['fuente'] != ""){
                            $arrFuente = array_values(array_filter($arrFuentes,function($e)use($element){return $element['fuente'] == $e['codigo'];}))[0];
                            $arrVigencia = array_values(array_filter($arrVigencias,function($e)use($element){return $element['vigencia'] == $e['codigo'];}))[0];
                            $arrProyectos[$i]['nombre_fuente'] = $arrFuente['nombre'];
                            $arrProyectos[$i]['nombre_vigencia'] = $arrVigencia['nombre'];
                            $arrProyectos[$i]['is_saved'] = 1;
                            $arrProyectos[$i]['rubro'].=$arrProyectos[$i]['indicador']."-".$arrProyectos[$i]['codigo']."-";
                            $arrProyectos[$i]['rubro'].=$arrProyectos[$i]['fuente']."-".$arrProyectos[$i]['seccion']."-".$arrProyectos[$i]['vigencia']."-".$arrProyectos[$i]['pago'];
                        }
                    }
                    $arrProCreados = array_values(array_filter($arrProyectos,function($e){return $e['fuente']!="";}));
                    $arrResponse = array(
                        "fuentes"=>$arrFuentes,
                        "bpim"=>$arrProyectos,
                        "proyectos"=>$arrProCreados,
                        "sectores"=>$this->selectSectores(),
                        "vigencias"=>$arrVigencias
                    );
                    
                }
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
            die();
        }
        public function save(){
            if(!empty($_SESSION)){
                $arrData = json_decode($_POST['data'],true);
                if(is_array($arrData) && !empty($arrData)){
                    $request = $this->insertData($arrData);
                    if($request > 0){
                        $arrResponse = array("status"=>true,"msg"=>"Datos guardados.");
                    }
                }else{
                    $arrResponse = array("status"=>false,"msg"=>"Error de datos");
                }
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
            die();
        }
    }

    if($_POST){
        $obj = new RubroController();
        if($_POST['action'] == "get"){
            $obj->initialData();
        }else if($_POST['action']=="save"){
            $obj->save();
        }
    }

?>
