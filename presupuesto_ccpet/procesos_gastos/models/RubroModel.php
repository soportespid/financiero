<?php
    require_once '../../../Librerias/core/Helpers.php';
    require_once '../../../Librerias/core/Mysql.php';
    session_start();
    /*ini_set('display_errors', '1');
    ini_set('display_startup_errors', '1');
    error_reporting(E_ALL);*/
    class RubroModel extends Mysql{
        private $intVigencia;
        function __construct(){
            parent::__construct();
            $this->intVigencia = date_create()->format('Y');
        }

        public function selectBpim(){
            $sql = "SELECT DISTINCT
            cp.id,
            cp.codigo,
            cp.vigencia as vigencia_proyecto,
            cp.nombre,
            pro.indicador,
            pro.sector,
            pro.programa,
            pro.producto,
            sec.nombre as nombre_sector,
            cpro.nombre as nombre_programa,
            p.producto as nombre_producto,
            cpp.id_fuente as fuente,
            secp.id_seccion_presupuestal as seccion,
            secp.nombre as nombre_seccion,
            cpp.vigencia_gasto as vigencia,
            cpp.medio_pago as pago
            FROM ccpproyectospresupuesto cp
            LEFT JOIN ccpproyectospresupuesto_presupuesto cpp ON cpp.codproyecto = cp.id
            INNER JOIN ccpproyectospresupuesto_productos pro ON pro.codproyecto = cp.id
            INNER JOIN ccpetsectores sec ON pro.sector = sec.codigo
            INNER JOIN ccpetprogramas cpro ON cpro.codigo = pro.programa
            INNER JOIN ccpetproductos p ON p.cod_producto = pro.producto AND pro.indicador = p.codigo_indicador
            INNER JOIN pptoseccion_presupuestal secp ON secp.id_seccion_presupuestal = cp.idunidadej
            WHERE cp.estado = 'S' AND cp.vigencia =$this->intVigencia ORDER BY cpp.id DESC";
            $request = $this->select_all($sql);
            return $request;
        }
        public function selectSectores(){
            $sql = "SELECT codigo, nombre FROM ccpetsectores";
            $request = $this->select_all($sql);
            return $request;
        }
        public function selectVigencias(){
            $sql = "SELECT codigo,nombre FROM ccpet_vigenciadelgasto WHERE version = (SELECT MAX(version) FROM ccpet_vigenciadelgasto)";
            $request = $this->select_all($sql);
            return $request;
        }
        public function selectFuentes(){
            $sql="SELECT codigo_fuente as codigo, nombre
            FROM ccpet_fuentes_cuipo
            WHERE version = '1' ORDER BY codigo_fuente";
            $request = $this->select_all($sql);
            return $request;
        }

        public function insertData(array $data){
            $return = 0;
            foreach ($data as $d) {
                if(!$d['is_saved']){
                    $id = searchConsec("ccpproyectospresupuesto_presupuesto", "id");
                    $sql = "INSERT INTO ccpproyectospresupuesto_presupuesto(id,codproyecto,id_fuente,medio_pago,valorcsf,valorssf,estado,indicador_producto,
                    metas,politicas_publicas,vigencia_gasto) VALUES(?,?,?,?,?,?,?,?,?,?,?)";
                    $arrValues = [$id,$d['id'],$d['fuente'],$d['pago'],0,0,"S",$d['indicador'],0,1000,$d['vigencia']];
                    $this->insert($sql,$arrValues);
                    $return = $id;
                    insertAuditoria("ccpet_auditoria","ccpet_funciones_id",1,"Crear",$id);
                }
            }
            return $return;
        }
    }
?>
