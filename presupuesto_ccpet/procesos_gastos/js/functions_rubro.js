const URL ='presupuesto_ccpet/procesos_gastos/controllers/RubroController.php';

var app = new Vue({
    el:"#myapp",
    data() {
        return {
            isLoading:false,
            isModalRubro:false,
            isModalFuente:false,
            txtSearchFuente:"",
            txtSearchBpim:"",
            txtResultadosFuentes:0,
            txtResultadosBpim:0,
            arrFuentes:[],
            arrBpim:[],
            arrProgramas:[],
            arrSectores:[],
            arrProductos:[],
            arrProgramasData:[],
            arrSectoresData:[],
            arrProductosData:[],
            arrFuentesCopy:[],
            arrBpimCopy:[],
            arrVigencias:[],
            arrData:[],
            arrDataCopy:[],
            selectPago:"CSF",
            selectSecciones:0,
            selectVigencias:0,
            selectSector:0,
            selectPrograma:0,
            selectProducto:0,
            selectSectorData:0,
            selectProgramaData:0,
            selectProductoData:0,
            objFuente:{codigo:"",nombre:""},
            objRubro:{
                codigo:"",
                nombre:"",
                rubro:"",
                indicador:"",
                sector:"",
                nombre_sector:"",
                programa:"",
                nombre_programa:"",
                producto:"",
                nombre_producto:""
            },
        }
    },
    mounted() {
        this.getData();
    },
    methods: {
        getData: async function(){
            const formData = new FormData();
            formData.append("action","get");
            this.isLoading = true;
            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();
            this.isLoading = false;
            this.arrVigencias = objData.vigencias;
            this.arrFuentes = objData.fuentes;
            this.arrBpim = objData.bpim;
            this.arrSectores = objData.sectores;
            this.arrSectoresData = objData.sectores;
            this.arrFuentesCopy = objData.fuentes;
            this.arrBpimCopy = objData.bpim;
            this.arrData = objData.proyectos;
            this.arrDataCopy = objData.proyectos;
            this.txtResultadosFuentes = this.arrFuentes.length;
            this.txtResultadosBpim = this.arrBpim.length;
            this.selectVigencias = this.arrVigencias[0].codigo;
        },
        search:function(type=""){
            let search = "";
            if(type == "modal_fuente")search = this.txtSearchFuente.toLowerCase();
            if(type == "cod_fuente") search = this.objFuente.codigo;

            if(type == "modal_fuente"){
                this.arrFuentesCopy = [...this.arrFuentes.filter(e=>e.codigo.toLowerCase().includes(search)
                    || e.nombre.toLowerCase().includes(search))];
                this.txtResultadosFuentes = this.arrFuentesCopy.length;
            }else if(type == "cod_fuente"){
                this.objFuente = {};
                this.objFuente = {...this.arrFuentes.filter(e=>e.codigo==search)[0]};
            }
        },
        selectItem:function(type="",{...item}){
            if(type == "modal_fuente"){
                this.objFuente = item;
                this.isModalFuente = false;
            }else if(type == "modal_rubro"){
                this.objRubro = {
                    id:item.id,
                    codigo:item.codigo,
                    nombre:item.nombre,
                    rubro:item.indicador+"-"+item.codigo,
                    indicador:item.indicador,
                    sector:item.sector,
                    nombre_sector:item.nombre_sector,
                    programa:item.programa,
                    nombre_programa:item.nombre_programa,
                    producto:item.producto,
                    nombre_producto:item.nombre_producto,
                    seccion:item.seccion,
                    nombre_seccion:item.nombre_seccion
                };
                this.isModalRubro = false;
            }
        },
        add:function(){
            if(this.objRubro.rubro ==""){
                Swal.fire("Atención!","Debe seleccionar un proyecto.","warning");
                return false;
            }
            if(this.objFuente.codigo ==""){
                Swal.fire("Atención!","Debe seleccionar la fuente.","warning");
                return false;
            }
            let arrVigencia = [...this.arrVigencias.filter(e=>{return e.codigo == app.selectVigencias})][0];
            this.objRubro.is_saved = 0;
            this.objRubro.fuente = this.objFuente.codigo;
            this.objRubro.nombre_fuente = this.objFuente.nombre;
            this.objRubro.vigencia = arrVigencia['codigo'];
            this.objRubro.nombre_vigencia = arrVigencia['nombre'];
            this.objRubro.pago = this.selectPago;
            if(app.arrData.length > 0){
                let flag = true;
                for (let i = 0; i < app.arrData.length; i++) {
                    const e = app.arrData[i];
                    if(e.codigo == app.objRubro.codigo && e.indicador == app.objRubro.indicador
                        && e.sector == app.objRubro.sector && e.programa == app.objRubro.programa
                        && e.programa == app.objRubro.programa && e.producto == app.objRubro.producto
                        && e.fuente == app.objRubro.fuente && e.vigencia == app.objRubro.vigencia
                        && e.seccion == app.objRubro.seccion && e.pago == app.objRubro.pago
                    ){
                        flag = false;
                        break;
                    }
                }
                if(flag){
                    this.objRubro.rubro = this.objRubro.rubro+"-"+this.objRubro.fuente+"-"+this.objRubro.seccion+"-"+this.objRubro.vigencia+"-"+this.objRubro.pago;
                    this.arrData.push(this.objRubro);
                }else{
                    Swal.fire("Atención!","Este rubro ya fue creado.","warning");
                    return false;
                }

            }else{
                this.objRubro.rubro = this.objRubro.rubro+"-"+this.objRubro.fuente+"-"+this.objRubro.seccion+"-"+this.objRubro.vigencia+"-"+this.objRubro.pago;
                this.arrData.push(this.objRubro);
            }
            this.objFuente={codigo:"",nombre:""}
            this.objRubro={
                codigo:"",
                nombre:"",
                rubro:"",
                indicador:"",
                sector:"",
                nombre_sector:"",
                programa:"",
                nombre_programa:"",
                producto:"",
                nombre_producto:""
            }
            this.arrData.sort(function(a, b) {
                return a.is_saved - b.is_saved;
            });
            this.arrDataCopy = this.arrData;
            this.selectSectorData=0;
            this.selectProgramaData=0;
            this.selectProductoData=0;

            this.filterData();
        },
        del:function(index){
            this.arrData.splice(index,1);
            this.arrDataCopy = this.arrData;
            this.selectSectorData=0;
            this.selectProgramaData=0;
            this.selectProductoData=0;
            this.filterData();
        },
        save: async function(){
            if(app.arrData.length == 0){
                Swal.fire("Atención!","Debe crear al menos un rubro.","warning");
                return false;
            }
            Swal.fire({
                title:"¿Estás segur@ de guardar?",
                text:"",
                icon: 'warning',
                showCancelButton:true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText:"Sí, guardar",
                cancelButtonText:"No, cancelar"
            }).then(async function(result){
                if(result.isConfirmed){
                    const formData = new FormData();
                    formData.append("data",JSON.stringify(app.arrData));
                    formData.append("action","save");
                    const response = await fetch(URL,{method:"POST",body:formData});
                    const objData = await response.json();
                    if(objData.status){
                        Swal.fire("Guardado",objData.msg,"success");
                        app.getData();
                    }else{
                        Swal.fire("Guardado",objData.msg,"error");
                    }
                }
            });
        },
        filter:function(type=""){

            if(type=="sector" && this.selectSector != 0){
                this.arrProductos = [];
                this.arrBpimCopy = [...this.arrBpim.filter((e)=>{return e.sector==app.selectSector})];
                this.arrProgramas = Array.from(new Set(
                    app.arrBpimCopy.map(e=>{return JSON.stringify({codigo:e.programa,nombre:e.nombre_programa})})
                )).map(item => JSON.parse(item));
            }else if(type=="programa" && this.selectPrograma != 0){
                this.arrBpimCopy = [...this.arrBpim.filter((e)=>{return e.sector==app.selectSector && e.programa==app.selectPrograma})];
                this.arrProductos = Array.from(new Set(
                    app.arrBpimCopy.map(e=>{return JSON.stringify({codigo:e.producto,nombre:e.nombre_producto})})
                )).map(item => JSON.parse(item));
            }else if(type=="producto" && this.selectProducto != 0){
                this.arrBpimCopy = [...this.arrBpim.filter((e)=>{
                    return e.sector==app.selectSector && e.programa==app.selectPrograma && e.producto==app.selectProducto}
                )];
            }else{
                this.arrBpimCopy = [...this.arrBpim];
            }
        },
        filterData:function(type=""){

            if(type=="sector" && this.selectSectorData != 0){
                this.arrProductosData = [];
                this.arrDataCopy = [...this.arrData.filter((e)=>{return e.sector==app.selectSectorData})];
                this.arrProgramasData = Array.from(new Set(
                    app.arrDataCopy.map(e=>{return JSON.stringify({codigo:e.programa,nombre:e.nombre_programa})})
                )).map(item => JSON.parse(item));
            }else if(type=="programa" && this.selectProgramaData != 0){
                this.arrDataCopy = [...this.arrData.filter((e)=>{return e.sector==app.selectSectorData && e.programa==app.selectProgramaData})];
                this.arrProductosData = Array.from(new Set(
                    app.arrDataCopy.map(e=>{return JSON.stringify({codigo:e.producto,nombre:e.nombre_producto})})
                )).map(item => JSON.parse(item));
            }else if(type=="producto" && this.selectProductoData != 0){
                this.arrDataCopy = [...this.arrData.filter((e)=>{
                    return e.sector==app.selectSectorData && e.programa==app.selectProgramaData && e.producto==app.selectProductoData}
                )];
            }else{
                this.arrDataCopy = [...this.arrData];
            }
        },
        formatNumero: function(valor){
            return new Intl.NumberFormat("en-US", {style: "currency", currency: "USD"}).format(valor);
        },
    },
    computed:{

    }
})
