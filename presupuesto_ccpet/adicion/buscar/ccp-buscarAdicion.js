import { createApp } from '../../../Librerias/vue3/dist/vue.esm-browser.js'

const DIR_ORIGIN = window.location.origin + '/';
const DIR_PATHNAME = window.location.pathname.split("/", 2)[1] + '/';
const DIR_FULL = DIR_ORIGIN + '/' + DIR_PATHNAME;

const app = createApp({
    data() {
      return {
        detallesAd: [],
        detallesAdicion: [],
        numActo: '',
        loading: false,
        letrero: true,
      }
    },

    mounted() {
		  this.loading = false;

      
      let today = new Date();

      // `getDate()` devuelve el día del mes (del 1 al 31)
      let day = today.getDate();
      
      // `getMonth()` devuelve el mes (de 0 a 11)
      let month = today.getMonth() + 1;

      
      // `getFullYear()` devuelve el año completo
      let year = today.getFullYear();

      document.getElementById('fc_1198971545').value = '01'+'/'+'01'+'/'+year;
      
      // muestra la fecha de hoy en formato `MM/DD/YYYY`

      let dayFormat = day.toString().padStart(2, '0');
      let monthFormat = month.toString().padStart(2, '0');

      document.getElementById('fc_1198971546').value = `${dayFormat}/${monthFormat}/${year}`;
	  },

    computed: {
      // existeInformacion(){

      //   if (this.detallesAd.length > 0) {
      //     return true;
      //   }
      //   else {
      //     return false;
      //   }
      // },

    },

    methods: {

      async buscarAdiciones() {
        
        //console.log(JSON.parse(JSON.stringify(this.detallesAd)));
        let fechaInicial = document.getElementById('fc_1198971545').value;
        let fechaFinal = document.getElementById('fc_1198971546').value;

        if(this.numActo != '' || (fechaInicial && fechaFinal)){

          if(fechaInicial && fechaFinal){
            let arrFechaInicial = fechaInicial.split("/");
            fechaInicial = `${arrFechaInicial[2]}-${arrFechaInicial[1]}-${arrFechaInicial[0]}`;
            let arrFechaFinal = fechaFinal.split("/");
            fechaFinal = `${arrFechaFinal[2]}-${arrFechaFinal[1]}-${arrFechaFinal[0]}`;
          }else{
            fechaInicial = '';
            fechaFinal = '';
          }

          let formData = new FormData();
          formData.append("fechaInicial", fechaInicial);
          formData.append("fechaFinal", fechaFinal);
          formData.append("numActo", this.numActo);

          //console.log(window.location);
          this.letrero = false;
          const URL = `${DIR_FULL}presupuesto_ccpet/adicion/buscar/ccp-buscarAdicion.php?action=buscarAdiciones`;

          await axios.post('presupuesto_ccpet/adicion/buscar/ccp-buscarAdicion.php?action=buscarAdiciones',formData)
            .then((response) => {
              this.detallesAd = response.data.adiciones;
              
            });
            
        }else{

          Swal.fire(
              'Falta informaci&oacute;n para generar reporte.',
              'Verifique que el n&uacute;mero de acto administrativo o la fecha inicial y final tengan informaci&oacute;n para realizar la busqueda.',
              'warning'
          );

        }

      },

      toFormData(obj){
        var form_data = new FormData();
        for(var key in obj){
            form_data.append(key, obj[key]);
        }
        return form_data;
      },

      comprobarEstado(estado){
        switch(estado){
          case 'S':
            return 'Activo'
          break;
          case 'N':
            return 'Anulado'
          break;
          case 'F':
            return 'Finalizado'
          break;
        }
      },

      formatonumero: function(valor){
        return new Intl.NumberFormat("en-US", {style: "currency", currency: "USD"}).format(valor);
      },

      seleccionar: function(id) {
       /*  var x = "ccp-visualizaAdicion.php?id="+id[0]; */
        location.href = "ccp-visualizaAdicion.php?id="+id[0];
       /*  window.open(x, '_blank');
        window.focus(); */
      }

    }
})
  
app.mount('#myapp')