import { divideRubro } from "../../../funciones.js";

var app = new Vue({
    el: '#myapp',
	data:{

        vigencia: '',
        acto_administrativo: '',
        actos_administrativo: [],

        valor_acuerdo: '$0.00',
        checked: '',
        checkedFuentes: '',
        tipoGasto: '',
        tiposDeGasto: [],
        selectUnidadEjecutora : '',
        uniEjecutoras: [],
        selectUniEjecutora : '',
        unidadesejecutoras: [],
        optionsMediosPagos: [
			{ text: 'CSF', value: 'CSF' },
			{ text: 'SSF', value: 'SSF' }
		],
        selectMedioPago: 'CSF',

        selectVigenciaGasto: '',
        vigenciasdelgasto: [],
        mostrarFuncionamiento : true,

        cuenta:'',
        nombreCuenta: '',
        showModal_cuentas: false,
        searchCuenta : {keywordCuenta: ''},
        cuentasCcpet: [],

        fuente: '',
        nombreFuente: '',
        showModal_fuentes: false,
        fuentesCuipo: [],
        searchFuente : {keywordFuente: ''},

        mostrarInversion : false,
        codProyecto: '',
        nombreProyecto: '',
        showModal_proyectos: false,
        proyectos: [],
        searchProyecto : {keywordProyecto: ''},

        programatico: '',
        nombreProgramatico: '',
        showModal_programatico: false,
        programaticos: [],

        tipo_cuenta: '-1',
        tipos_cuenta: [
			{ text: 'Ingresos', value: 'I' },
			{ text: 'Gastos', value: 'G' }
		],

        totalGastos: '$0.00',
        totalIngresos: '$0.00',
        diferencia: '$0.00',

        mostrarTipoGasto: false,


        valor: 0,

        detalles: [],

        basesApp: [],

        isActiveAd: true,

        rubroPresupuestal: '',
        isModalPrograma:false,
        isModalSector:false,
        isModalClasificador:false,
        selectOrden:1,
        arrProgramas:[],
        arrSectores:[],
        arrProgramasCopy:[],
        arrSectoresCopy:[],
        arrFuentes: [],
        arrClasificadoresProgramados:[],
        arrClasificadores:[],
        arrClasificadoresCopy:[],
        objSector:{codigo:"",nombre:""},
        objPrograma:{codigo:"",nombre:""},
        objClasificador:{codigo:"",nombre:""},
        txtSearchProgramas:"",
        txtSearchSectores:"",
        txtSearchClasificadores:"",
        txtResultadosProgramas:0,
        txtResultadosSectores:0,
        txtResultadosClasificadores:0,
        isClasificador:false,

    },

    mounted: async function(){

        await this.cargarFecha();
        await this.uniEjecutorasSearch();
        this.seccionPresupuestal();
        this.vigenciasDelgasto();
        this.actosAdministrativos();
        this.getData();
    },

    methods: {
        showTab:function(tab){
            let tabs = this.$refs.rTabs.children;
            let tabsContent = this.$refs.rTabsContent.children;
            for (let i = 0; i < tabs.length; i++) {
                tabs[i].classList.remove("active");
                tabsContent[i].classList.remove("active")
            }
            tabs[tab-1].classList.add("active");
            tabsContent[tab-1].classList.add("active")
        },
        reCalcularDiferencia:function(){
            const vueContext = this;
            this.arrFuentes = [];
            for (let i = 0; i < vueContext.detalles.length; i++) {
                const detalle = vueContext.detalles[i];
                let credito = isNaN (detalle[15]) || detalle[15] == "" || detalle[15] == undefined ? 0 : detalle[15];
                let contraCredito = isNaN (detalle[16])|| detalle[16]=="" || detalle[15] == undefined ? 0 : detalle[16];
                credito = Math.round(parseFloat(credito));
                contraCredito = Math.round(parseFloat(contraCredito));
                if(vueContext.arrFuentes.length > 0){
                    let flag = false;
                    for (let j = 0; j < vueContext.arrFuentes.length; j++) {
                        const fuente = vueContext.arrFuentes[j];
                        if(fuente.fuente == detalle[13]){
                            fuente.credito+=parseFloat(credito);
                            fuente.contra_credito+=parseFloat(contraCredito);
                            fuente.saldo = parseFloat(fuente.credito) - parseFloat(fuente.contra_credito)
                            flag = true;
                            break;
                        }
                    }
                    if(!flag){
                        vueContext.arrFuentes.push(
                            {
                                fuente:detalle[13],
                                fuente_nombre:detalle[14],
                                credito:parseFloat(credito),
                                contra_credito:parseFloat(contraCredito),
                                saldo:parseFloat(credito) - parseFloat(contraCredito)
                            }
                        );
                    }
                }else{
                    vueContext.arrFuentes.push(
                        {
                            fuente:detalle[13],
                            fuente_nombre:detalle[14],
                            credito:parseFloat(credito),
                            contra_credito:parseFloat(contraCredito),
                            saldo:parseFloat(credito) - parseFloat(contraCredito)
                        }
                    );
                }
            }
        },
        getData: async function(){
            const formData = new FormData();
            formData.append("action","get");
            const response = await fetch('presupuesto_ccpet/adicion/crear/ccp-adicion.php',{method:"POST",body:formData});
            const objData = await response.json();

            this.arrProgramas = objData.data.programas;
            this.arrSectores = objData.data.sectores;
            this.arrProgramasCopy = objData.data.programas;
            this.arrSectoresCopy = objData.data.sectores;
            this.arrClasificadores = objData.data.clasificadores;
            this.arrClasificadoresCopy = objData.data.clasificadores;
            this.arrClasificadoresProgramados = objData.data.clasificadores_programados;
            this.txtResultadosProgramas = this.arrProgramas.length;
            this.txtResultadosSectores = this.arrSectores.length;
            this.selectOrden = objData.data.orden;
        },
        search:function(type=""){
            let search = "";
            if(type == "modal_programa")search = this.txtSearchProgramas.toLowerCase();
            if(type == "modal_sector")search = this.txtSearchSectores.toLowerCase();
            if(type == "modal_clasificador")search = this.txtSearchClasificadores.toLowerCase();
            if(type == "cod_sector") search = this.objSector.codigo;
            if(type == "cod_programa") search = this.objPrograma.codigo;
            if(type == "cod_clasificador") search = this.objClasificador.codigo;

            if(type == "modal_sector"){
                this.arrSectoresCopy = [...this.arrSectores.filter(e=>e.codigo.toLowerCase().includes(search)
                    || e.nombre.toLowerCase().includes(search))];
                this.txtResultadosSectores = this.arrSectoresCopy.length;
            }else if(type == "modal_programa"){
                this.arrProgramasCopy = [...this.arrProgramas.filter(e=>e.codigo.toLowerCase().includes(search)
                    || e.nombre.toLowerCase().includes(search))];
                this.txtResultadosProgramas = this.arrProgramasCopy.length;
            }else if(type == "modal_clasificador"){
                this.arrClasificadoresCopy = [...this.arrClasificadores.filter(e=>e.codigo.toLowerCase().includes(search)
                    || e.nombre.toLowerCase().includes(search))];
            }else if(type == "cod_sector"){
                this.objSector = {};
                this.objSector = {...this.arrSectores.filter(e=>e.codigo==search)[0]};
                this.changeSector();
            }else if(type == "cod_programa"){
                this.objPrograma = {};
                this.objPrograma = {...this.arrProgramas.filter(e=>e.codigo==search)[0]};
                this.changePrograma();
            }else if(type == "cod_clasificador"){
                this.objClasificador = {};
                this.objClasificador = {...this.arrClasificadores.filter(e=>e.codigo==search)[0]};
            }
        },
        changeSector:function(){
            if(this.objSector.codigo){
                this.objPrograma = {};
                this.codProyecto = "";
                this.programatico = "";
            }
        },
        changePrograma:function(){
            if(this.objPrograma.codigo){
                let sector = this.objPrograma.codigo.substring(0,2);
                this.objSector = {...this.arrSectores.filter(e=>e.codigo==sector)[0]};
                this.codProyecto = "";
                this.programatico = "";
            }
        },
        changeProgramatico:function(){
            if(this.programatico != ""){
                let sector = this.programatico.substring(0,2);
                let programa = this.programatico.substring(0,4);
                this.objSector = {...this.arrSectores.filter(e=>e.codigo==sector)[0]};
                this.objPrograma = {...this.arrProgramas.filter(e=>e.codigo==programa)[0]};
            }
        },
        selectItem:function(type="",{...item}){
            if(type == "modal_sector"){
                this.objSector = item;
                this.isModalSector = false;
                this.changeSector();
            }else if(type == "modal_programa"){
                this.objPrograma = item;
                this.isModalPrograma = false;
                this.changePrograma();
            }else if(type == "modal_clasificador"){
                this.objClasificador = item;
                this.isModalClasificador = false;
            }
        },
        cargarFecha: function(){

            const fechaAct = new Date().toJSON().slice(0,10).replace(/-/g,'/');
            const fechaArr = fechaAct.split('/');
            const fechaV = fechaArr[2]+'/'+fechaArr[1]+'/'+fechaArr[0];
            document.getElementById('fecha').value = fechaV;
            this.vigencia = fechaArr[0];
        },

        cambiaTipoCuenta: function(bandera = true){
            this.cuenta = '';
            this.nombreCuenta = '';
            this.fuente = '';
            this.nombreFuente = '';
            this.valor = '';
            this.programatico = '';
            this.nombreProgramatico = '';
            this.codProyecto = '';
            this.nombreProyecto = '';

            if(this.tipo_cuenta == 'G'){
                this.mostrarTipoGasto = true;
                if(bandera){
                    this.cargarTiposDeGasto();
                }
            }else{
                this.tipoGasto = '';
                this.tiposDeGasto = [];
                this.mostrarTipoGasto = false;
            }
            this.cambiaParametro();
        },

        cambiaParametro: function(){
            this.cuenta = '';
            this.nombreCuenta = '';
            this.fuente = '';
            this.nombreFuente = '';
            this.valor = '';
            this.programatico = '';
            this.nombreProgramatico = '';
            this.codProyecto = '';
            this.nombreProyecto = '';
            this.saldo = 0;
            if(this.tiposDeGasto.length > 0){
                if(this.tiposDeGasto[this.tipoGasto-1][2] == 2.3){
                    this.mostrarInversion = true;
                    this.mostrarFuncionamiento = false;
                }else{
                    this.mostrarFuncionamiento = true;
                    this.mostrarInversion = false;
                }
            }else{
                this.mostrarFuncionamiento = true;
                this.mostrarInversion = false;
            }
        },

        actosAdministrativos: function(){
            const fechat = document.getElementById('fecha').value;
            const fechatAr = fechat.split('/');
            this.vigencia = fechatAr[2];
            axios.post('presupuesto_ccpet/adicion/crear/ccp-adicion.php?action=actosAdm&vig=' + this.vigencia)
            .then(
                (response)=>{
                    this.actos_administrativo = response.data.actosAdm;
                }
            );
        },

        seleccionarActoAdm: async function(){

            this.actos_administrativo.forEach(function logArrayElements(element, index, array) {
                if(element[0] == app.acto_administrativo){
                    app.valor_acuerdo = new Intl.NumberFormat("en-US", {style: "currency", currency: "USD",minimumFractionDigits: 0, maximumFractionDigits: 2,}).format(element[5]);
                    let fechaActParts = element[3].split('-');
                    document.getElementById('fecha').value = fechaActParts[2]+'/'+fechaActParts[1]+'/'+fechaActParts[0];
                    //app.valor_acuerdo = element[5];
                }
            });

            let formData = new FormData();

            this.basesApp.map((b, i) => {
                formData.append("basesApp["+i+"][]", Object.values(b));
            })

            await axios.post('presupuesto_ccpet/adicion/crear/ccp-adicion.php?action=cargarDet&idAcuerdo=' + this.acto_administrativo, formData)
            .then((response) => {
                app.detalles = response.data.detallesAd;
                app.totalGastos = new Intl.NumberFormat("en-US", {style: "currency", currency: "USD",minimumFractionDigits: 0, maximumFractionDigits: 2,}).format(response.data.totalGastos);
                app.totalIngresos = new Intl.NumberFormat("en-US", {style: "currency", currency: "USD",minimumFractionDigits: 0, maximumFractionDigits: 2,}).format(response.data.totalIng);

                /* if(response.data.tiposDeGasto.length > 0){
                    this.tipoGasto = response.data.tiposDeGasto[0][0];
                } */
            });

            this.diferenciaAd();
            this.reCalcularDiferencia();
        },

        cargarTiposDeGasto: async function(){

            await axios.post('vue/presupuesto_ccp/ccp-cdpbasico.php?action=cargarTiposDeGasto')
            .then((response) => {

                app.tiposDeGasto = response.data.tiposDeGasto;

                if(response.data.tiposDeGasto.length > 0){
                    this.tipoGasto = response.data.tiposDeGasto[0][0];
                }
            });
            this.cambiaTipoDeGasto();
        },

        seccionPresupuestal: async function(){

            var formData = new FormData();

            formData.append("base", this.uniEjecutoras[this.selectUniEjecutora-1][2]);
            formData.append("usuario", this.uniEjecutoras[this.selectUniEjecutora-1][6]);

            await axios.post('vue/presupuesto_ccp/ccp-inicialgastosfun.php?action=secciones', formData)
            .then(
                (response)=>{
                    this.unidadesejecutoras = response.data.secciones;
                    this.selectUnidadEjecutora = this.unidadesejecutoras[0][0];
                }
            );
        },

        uniEjecutorasSearch: async function(){

            await axios.post('presupuesto_ccpet/adicion/crear/ccp-adicion.php?action=unidades')
            .then(
                (response)=>{
                    this.uniEjecutoras = response.data.uniEjecutoras;
                    this.selectUniEjecutora = this.uniEjecutoras[0][0];
                }
            );
            this.bases();
        },

        bases(){
            const basesUnidad = this.uniEjecutoras.map(uniEjecutora => {
                return  {
                            base: uniEjecutora[2],
                            usuario: uniEjecutora[6],
                            unidad: uniEjecutora[1],
                            nombre: uniEjecutora[3].toLowerCase(),
                            tipo: uniEjecutora[4],
                            id: uniEjecutora[0],
                        };
            });
            this.basesApp = basesUnidad;
        },

        vigenciasDelgasto: async function(){
            await axios.post('vue/presupuesto_ccp/ccp-inicialgastosfun.php?action=vigenciasDelGasto')
            .then(
                (response) => {
                    this.vigenciasdelgasto = response.data.vigenciasDelGasto;
                    this.selectVigenciaGasto = this.vigenciasdelgasto[0][0];
                }
            );
        },

        agregarDetalle: function(){
            if(!this.checked){

                if(this.tipo_cuenta != ''){

                    if(this.selectUnidadEjecutora != ''){

                        if(this.selectMedioPago != ''){

                            if(this.selectVigenciaGasto){

                                if(this.fuente != ''){

                                    if(this.valor > 0){

                                        if(this.tipoGasto == '3'){

                                            if(this.codProyecto != ''){

                                                if(this.programatico != ''){

                                                    var detallesAgr = [];

                                                    detallesAgr.push(this.tipo_cuenta);

                                                    this.tiposDeGasto.forEach(function logArrayElements(element, index, array) {
                                                        if(element[0] == app.tipoGasto){
                                                            detallesAgr.push(element[1]);
                                                        }
                                                    });
                                                    /* detallesAgr.push(this.tipoGasto); */
                                                    detallesAgr.push(this.selectUnidadEjecutora);
                                                    this.unidadesejecutoras.forEach(function logArrayElements(element, index, array) {
                                                        if(element[0] == app.selectUnidadEjecutora){
                                                            detallesAgr.push(element[1].toLowerCase());
                                                        }
                                                    });
                                                    detallesAgr.push(this.selectMedioPago);
                                                    detallesAgr.push(this.selectVigenciaGasto);
                                                    this.vigenciasdelgasto.forEach(function logArrayElements(element, index, array) {
                                                        if(element[0] == app.selectVigenciaGasto){
                                                            detallesAgr.push(element[2].toLowerCase());
                                                        }
                                                    });

                                                    detallesAgr.push(this.codProyecto);
                                                    detallesAgr.push(this.nombreProyecto.toLowerCase().slice(0,30));
                                                    detallesAgr.push(this.programatico);
                                                    detallesAgr.push(this.nombreProgramatico.toLowerCase().slice(0,30));

                                                    /* }else{
                                                        detallesAgr.push(this.cuenta);
                                                        detallesAgr.push(this.nombreCuenta.toLowerCase().slice(0,15));
                                                        detallesAgr.push('');
                                                        detallesAgr.push('');
                                                    } */
                                                    detallesAgr.push('');
                                                    detallesAgr.push('');
                                                    detallesAgr.push(this.fuente);
                                                    detallesAgr.push(this.nombreFuente.toLowerCase().slice(0,30));
                                                    //detallesAgr.push(new Intl.NumberFormat().format(this.valor));

                                                    detallesAgr.push('');
                                                    detallesAgr.push(this.valor);

                                                    detallesAgr.push(this.selectUniEjecutora);

                                                    this.uniEjecutoras.forEach(function logArrayElements(element, index, array) {
                                                        if(element[0] == app.selectUniEjecutora){
                                                            detallesAgr.push(element[1]);
                                                            detallesAgr.push(element[3].toLowerCase());
                                                            detallesAgr.push(element[2]);
                                                            detallesAgr.push(element[4]);
                                                            detallesAgr.push(element[6]);
                                                        }
                                                    });

                                                    let existeDetalle = 0;
                                                    this.detalles.forEach(function(elemento){
                                                        let result = app.comparaArrays(elemento, detallesAgr);

                                                        if(result){
                                                            existeDetalle = 1;
                                                            return false;
                                                        }
                                                    });


                                                    if(existeDetalle == 0){
                                                        if(this.isClasificador && this.objClasificador.codigo ==""){
                                                            Swal.fire(
                                                                'Cuenta con clasificador.',
                                                                'La cuenta debe tener un clasificador.',
                                                                'warning'
                                                            );
                                                            return false;
                                                        }
                                                        //Clasificador
                                                        if(this.isClasificador){
                                                            detallesAgr.push(this.objClasificador.codigo);
                                                            detallesAgr.push(this.objClasificador.nombre);
                                                            this.objClasificador = {"codigo":"","nombre":""};
                                                            this.isClasificador = false;
                                                        }else{
                                                            detallesAgr.push("");
                                                            detallesAgr.push("");
                                                        }
                                                        this.detalles.push(detallesAgr);
                                                        this.cuenta = '';
                                                        this.codProyecto = '';
                                                        this.nombreProyecto = '';
                                                        this.programatico = '';
                                                        this.nombreProgramatico = '';
                                                        this.nombreCuenta = '';
                                                        this.fuente = '';
                                                        this.nombreFuente = '';


                                                        /* if(this.tipo_cuenta == 'I'){
                                                            this.totalIngresos = parseFloat(this.totalIngresos) + parseFloat(this.valor);
                                                        }else{
                                                            this.totalGastos = parseFloat(this.totalGastos) + parseFloat(this.valor);
                                                        } */
                                                        if(this.tipo_cuenta == 'I'){
                                                            this.totalIngresos = this.sumarIngresos(parseFloat(this.valor));

                                                            this.totalIngresos= new Intl.NumberFormat("en-US", {style: "currency", currency: "USD",minimumFractionDigits: 0, maximumFractionDigits: 2,}).format(this.totalIngresos);
                                                        }else{
                                                            this.totalGastos = this.sumarGastos(parseFloat(this.valor));

                                                            this.totalGastos= new Intl.NumberFormat("en-US", {style: "currency", currency: "USD",minimumFractionDigits: 0, maximumFractionDigits: 2,}).format(this.totalGastos);
                                                        }

                                                        this.valor = 0;
                                                        this.cargarTiposDeGasto();
                                                        this.diferenciaAd();
                                                    }else{

                                                        Swal.fire(
                                                            'Detalle repetido.',
                                                            'Debe tener el rubro o alguno de sus clasificadores diferentes a los detalles que se han agregado.',
                                                            'warning'
                                                        );

                                                    }
                                                }else{
                                                    Swal.fire(
                                                        'Falta informaci&oacute;n.',
                                                        'Falta escoger el indicador program&aacute;tico MGA.',
                                                        'warning'
                                                    );
                                                }

                                            }else{
                                                Swal.fire(
                                                    'Falta informaci&oacute;n.',
                                                    'Falta escoger el proyecto de inversi&oacute;n.',
                                                    'warning'
                                                );
                                            }

                                        }else{

                                            var detallesAgr = [];

                                            detallesAgr.push(this.tipo_cuenta);

                                            if(this.tipo_cuenta == 'I'){
                                                detallesAgr.push('Ingresos');
                                            }else{
                                                this.tiposDeGasto.forEach(function logArrayElements(element, index, array) {
                                                    if(element[0] == app.tipoGasto){
                                                        detallesAgr.push(element[1]);
                                                    }
                                                });
                                            }

                                            /* detallesAgr.push(this.tipoGasto); */
                                            detallesAgr.push(this.selectUnidadEjecutora);
                                            this.unidadesejecutoras.forEach(function logArrayElements(element, index, array) {
                                                if(element[0] == app.selectUnidadEjecutora){
                                                    detallesAgr.push(element[1].toLowerCase());
                                                }
                                            });
                                            detallesAgr.push(this.selectMedioPago);
                                            detallesAgr.push(this.selectVigenciaGasto);
                                            this.vigenciasdelgasto.forEach(function logArrayElements(element, index, array) {
                                                if(element[0] == app.selectVigenciaGasto){
                                                    detallesAgr.push(element[2].toLowerCase());
                                                }
                                            });

                                            detallesAgr.push('');
                                            detallesAgr.push('');
                                            detallesAgr.push('');
                                            detallesAgr.push('');
                                            detallesAgr.push(this.cuenta);
                                            detallesAgr.push(this.nombreCuenta.toLowerCase().slice(0,30));

                                            /* }else{
                                                detallesAgr.push(this.cuenta);
                                                detallesAgr.push(this.nombreCuenta.toLowerCase().slice(0,15));
                                                detallesAgr.push('');
                                                detallesAgr.push('');
                                            } */

                                            detallesAgr.push(this.fuente);
                                            detallesAgr.push(this.nombreFuente.toLowerCase().slice(0,30));

                                            //detallesAgr.push(new Intl.NumberFormat().format(this.valor));
                                            if(this.tipo_cuenta == 'I'){
                                                detallesAgr.push(this.valor);
                                                detallesAgr.push('');
                                            }else{
                                                detallesAgr.push('');
                                                detallesAgr.push(this.valor);
                                            }

                                            detallesAgr.push(this.selectUniEjecutora);
                                            this.uniEjecutoras.forEach(function logArrayElements(element, index, array) {
                                                if(element[0] == app.selectUniEjecutora){
                                                    detallesAgr.push(element[1]);
                                                    detallesAgr.push(element[3].toLowerCase());
                                                    detallesAgr.push(element[2]);
                                                    detallesAgr.push(element[4]);
                                                    detallesAgr.push(element[6]);
                                                }
                                            });


                                            let existeDetalle = 0;
                                            this.detalles.forEach(function(elemento){
                                                let result = app.comparaArrays(elemento, detallesAgr);

                                                if(result){
                                                    existeDetalle = 1;
                                                    return false;
                                                }
                                            });

                                            if(existeDetalle == 0){
                                                if(this.isClasificador && this.objClasificador.codigo ==""){
                                                    Swal.fire(
                                                        'Cuenta con clasificador.',
                                                        'La cuenta debe tener un clasificador.',
                                                        'warning'
                                                    );
                                                    return false;
                                                }
                                                //Clasificador
                                                if(this.isClasificador){
                                                    detallesAgr.push(this.objClasificador.codigo);
                                                    detallesAgr.push(this.objClasificador.nombre);
                                                    this.objClasificador = {"codigo":"","nombre":""};
                                                    this.isClasificador = false;
                                                }else{
                                                    detallesAgr.push("");
                                                    detallesAgr.push("");
                                                }
                                                this.detalles.push(detallesAgr);
                                                this.cuenta = '';
                                                this.codProyecto = '';
                                                this.nombreProyecto = '';
                                                this.programatico = '';
                                                this.nombreProgramatico = '';
                                                this.nombreCuenta = '';
                                                this.fuente = '';
                                                this.nombreFuente = '';
                                                if(this.tipo_cuenta == 'I'){
                                                    this.totalIngresos = this.sumarIngresos(parseFloat(this.valor));
                                                    this.totalIngresos= new Intl.NumberFormat("en-US", {style: "currency", currency: "USD",minimumFractionDigits: 0, maximumFractionDigits: 2,}).format(this.totalIngresos);
                                                }else{
                                                    this.totalGastos = this.sumarGastos(parseFloat(this.valor));
                                                    this.totalGastos= new Intl.NumberFormat("en-US", {style: "currency", currency: "USD",minimumFractionDigits: 0, maximumFractionDigits: 2,}).format(this.totalGastos);
                                                }


                                                this.valor = 0;
                                                this.cargarTiposDeGasto();
                                                this.diferenciaAd();

                                            }else{

                                                Swal.fire(
                                                    'Detalle repetido.',
                                                    'Debe tener el rubro o alguno de sus clasificadores diferentes a los detalles que se han agregado.',
                                                    'warning'
                                                );

                                            }

                                        }



                                    }else{
                                        Swal.fire(
                                            'Falta informaci&oacute;n.',
                                            'Falta digitar el valor.',
                                            'warning'
                                        );
                                    }
                                }else{
                                    Swal.fire(
                                        'Falta informaci&oacute;n.',
                                        'Falta escoger la fuente.',
                                        'warning'
                                    );
                                }
                            }else{
                                Swal.fire(
                                    'Falta informaci&oacute;n.',
                                    'Falta escoger la vigencia del gasto.',
                                    'warning'
                                );
                            }
                        }else{

                            Swal.fire(
                                'Falta informaci&oacute;n.',
                                'Falta escoger el medio de pago.',
                                'warning'
                            );
                        }
                    }else{
                        Swal.fire(
                            'Falta informaci&oacute;n.',
                            'Falta escoger la secci&oacute;n presupuestal.',
                            'warning'
                        );
                    }
                }else{

                    Swal.fire(
                        'Falta informaci&oacute;n.',
                        'Falta escoger el tipo de cuenta.',
                        'warning'
                    );
                }
            }else{
                Swal.fire(
                    'Adici&oacute;n finalizada.',
                    'La adicion no se puede modificar porque esta finalizada.',
                    'warning'
                );
            }
            console.log(this.detalles);
            this.reCalcularDiferencia();
        },

        comparaArrays: function(array1, array2){
            /* const array1 = [];
            const array2 = [];
            array1.push(a1);
            array2.push(a2); */
            array1 = array1.slice(0,-3);
            array2 = array2.slice(0,-3);

            return Array.isArray(array1) &&
                    Array.isArray(array2) &&
                    array1.length === array2.length &&
                    array1.every((val, index) => val === array2[index]);

        },

        quitarValor: function(item){
            if(item[0] == 'I'){
                var totIng = this.quitarPesosYComas(this.totalIngresos);

                var totalIngresosNuevo = 0;
                totalIngresosNuevo = parseFloat(totIng) - parseFloat(item[15]);
                this.totalIngresos = new Intl.NumberFormat("en-US", {style: "currency", currency: "USD",minimumFractionDigits: 0, maximumFractionDigits: 2,}).format(totalIngresosNuevo);

            }else{
                var totGastos = this.quitarPesosYComas(this.totalGastos);

                var totalGastosNuevo = 0;

                totalGastosNuevo = parseFloat(totGastos) - parseFloat(item[16]);

                this.totalGastos = new Intl.NumberFormat("en-US", {style: "currency", currency: "USD",minimumFractionDigits: 0, maximumFractionDigits: 2,}).format(totalGastosNuevo);
            }
        },

        eliminarDetalle: async function(item){
            if(!this.checked){
                await this.quitarValor(item);
                var i = this.detalles.indexOf( item );

                if ( i !== -1 ) {
                    this.detalles.splice( i, 1 );
                }

                this.diferenciaAd();
                this.reCalcularDiferencia();
                /* setTimeout(function(){
                    this.diferenciaAd();
                },2000); */
            }else{
                Swal.fire(
                    'Adici&oacute;n finalizada.',
                    'La adicion no se puede modificar porque esta finalizada.',
                    'warning'
                );
            }

        },

        quitarPesosYComas: function(valor=''){

            var valorACambiar = '';
            valorACambiar = valor.replace(/[$]+/g, '');
            valorACambiar = valorACambiar.replace(/[,]+/g, '');

            /* if(valorACambiar == '[object Undefined]'){
                valorACambiar = 0;
            } */

            return valorACambiar;
        },

        diferenciaAd: function(){

            var totIng = this.quitarPesosYComas(this.totalIngresos);
            var totGastos = this.quitarPesosYComas(this.totalGastos);
            //totIng = totIng.replace(/[$]+/g, '');
            this.diferencia = parseFloat(totIng) - parseFloat(totGastos);
            this.diferencia= new Intl.NumberFormat("en-US", {style: "currency", currency: "USD",minimumFractionDigits: 0, maximumFractionDigits: 2,}).format(this.diferencia);

        },

        sumarIngresos: function(valorSum){

            var totIng = this.quitarPesosYComas(this.totalIngresos);

            var totSumIng = parseFloat(totIng) + parseFloat(valorSum);

            return totSumIng;
        },

        sumarGastos: function(valorSum){

            var totGastos = this.quitarPesosYComas(this.totalGastos);

            var totSumGastos = parseFloat(totGastos) + parseFloat(valorSum);

            return totSumGastos;
        },

        guardarAdd: function(){

            var totDif = this.quitarPesosYComas(this.diferencia);
            let isSaldo = this.arrFuentes.every(function(e){return e.saldo == 0});
            if(!isSaldo){
                this.loading =  false;
                Swal.fire(
                    'El saldo de cada fuente debe ser igual a cero.',
                    'Debe verificar el crédito y contracredito de cada fuente.',
                    'warning'
                );
                return false;
            }
            if(parseFloat(totDif) == 0){


                if(document.getElementById('fecha').value == '' && this.acto_administrativo == '' && this.detalles.length == 0){
                    Swal.fire('Falta información para guardar la adición .','Verifique que todos los campos esten diligenciados.','warning');
                    return false;
                }
                Swal.fire({
                    title: 'Esta seguro de guardar?',
                    text: "Guardar la adición en la base de datos, confirmar campos!",
                    icon: 'question',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Si, guardar!'
                }).then((result) => {
                    if (result.isConfirmed) {
                        let formData = new FormData();

                        for(let i=0; i <= this.detalles.length-1; i++){
                            const val = Object.values(this.detalles[i]).length;

                            for(let x = 0; x <= val; x++){
                                formData.append("detallesAd["+i+"][]", Object.values(this.detalles[i])[x]);
                            }

                        }

                        this.basesApp.map((b, i) => {
                            formData.append("basesApp["+i+"][]", Object.values(b));
                        })

                        formData.append("acto_administrativo", this.acto_administrativo);
                        formData.append("vigencia", this.vigencia);
                        formData.append("fecha", document.getElementById('fecha').value);
                        formData.append("finalizar", this.checked);

                        axios.post('presupuesto_ccpet/adicion/crear/ccp-adicion.php?action=guardarAd', formData)
                        .then((response) => {
                            if(response.data.insertaBien){
                                Swal.fire({
                                    position: 'top-end',
                                    icon: 'success',
                                    title: 'La adición se guardó con éxito',
                                    showConfirmButton: false,
                                    timer: 1500
                                }).then((response) => {
                                        app.redireccionar();
                                    });
                            }else{

                                Swal.fire(
                                    'Error!',
                                    'No se pudo guardar.',
                                    'error'
                                );
                            }

                        });

                    }
                });
            }else{
                Swal.fire(
                    'La suma de ingresos y gastos deben ser iguales.',
                    'Verifique que la diferencia entre sumas de ingresos y gastos sea igual cero.',
                    'warning'
                );
            }
        },

        redireccionar: function(){
            /* const  cdp = this.consecutivo;
            const  vig = this.vigencia;
            location.href ="ccp-cdpVisualizar.php?is="+cdp+"&vig="+vig; */

            location.href ="ccp-adicion.php";
        },

        puedeFinalizar: function(){

            var valorAcuerdo = this.quitarPesosYComas(this.valor_acuerdo);
            var gastos = this.quitarPesosYComas(this.totalGastos);
            var ingresos = this.quitarPesosYComas(this.totalIngresos);

            if(parseFloat(valorAcuerdo) > 0){
                if(parseFloat(valorAcuerdo) == parseFloat(ingresos) && parseFloat(valorAcuerdo) == parseFloat(gastos)){
                    this.checked = 'checked';
                }else{
                    Swal.fire(
                        'No se puede finalizar acto administrativo.',
                        'El valor del acto administrativo y el total de ingresos y gastos debe ser igual.',
                        'warning'
                    );
                    this.checked = '';
                }
            }else{
                Swal.fire(
                    'No se puede finalizar acto administrativo.',
                    'El valor del acto administrativo debe ser mayor a 0.',
                    'warning'
                );
                this.checked = '';
            }

        },

        cambiaTipoDeGasto: function(){
            this.cuenta = '';
            this.nombreCuenta = '';
            this.fuente = '';
            this.nombreFuente = '';
            this.valor = '';
            this.programatico = '';
            this.nombreProgramatico = '';
            this.codProyecto = '';
            this.nombreProyecto = '';

            if(this.tiposDeGasto.length > 0){
                if(this.tiposDeGasto[this.tipoGasto-1][2] == 2.3){
                    this.mostrarInversion = true;
                    this.mostrarFuncionamiento = false;
                }else{
                    this.mostrarFuncionamiento = true;
                    this.mostrarInversion = false;
                }
            }else{
                this.mostrarFuncionamiento = true;
                this.mostrarInversion = false;
            }

            /* if(this.tiposDeGasto[this.tipoGasto-1][2] == 2.3){
                this.mostrarInversion = true;
                this.mostrarFuncionamiento = false;
            }else{
                this.mostrarFuncionamiento = true;
                this.mostrarInversion = false;
            } */

        },

        ventanaCuenta: async function(){

            if(this.tipo_cuenta != '-1'){
                if(this.tipo_cuenta == 'I'){
                    app.showModal_cuentas = true;
                    await axios.post('presupuesto_ccpet/adicion/crear/ccp-adicion.php?action=cargarCuentasIng')
                        .then((response) => {
                            app.cuentasCcpet = response.data.cuentasCcpet;
                        });
                }else{
                    app.showModal_cuentas = true;
                    await axios.post('presupuesto_ccpet/adicion/crear/ccp-adicion.php?action=cargarCuentas&inicioCuenta=' + this.tiposDeGasto[this.tipoGasto-1][2])
                        .then((response) => {
                            app.cuentasCcpet = response.data.cuentasCcpet;
                        });
                }
            }else{
                Swal.fire(
                    'Falta escoger el tipo de cuenta.',
                    'Escoger el tipo de cuenta, ingresos o egresos.',
                    'warning'
                    );
            }


        },

        buscarCta: async function(){
            if(this.cuenta != ''){
                if(this.tipo_cuenta != '-1'){
                    if(this.tipo_cuenta == 'I'){
                        await axios.post('presupuesto_ccpet/adicion/crear/ccp-adicion.php?action=buscarCuentaIngreso&cuenta=' + this.cuenta)
                        .then((response) => {
                            if(response.data.resultBusquedaCuenta.length > 0){
                                if(response.data.resultBusquedaCuenta[0][1] == 'C'){
                                    app.nombreCuenta = response.data.resultBusquedaCuenta[0][0];
                                    const clasificadoresProgramados = app.arrClasificadoresProgramados.filter(function(e){return e.cuenta == app.cuenta});
                                    if(clasificadoresProgramados.length > 0){
                                        app.isClasificador = true;
                                    }else{
                                        app.isClasificador = false;
                                    }
                                }else{
                                    Swal.fire(
                                    'Tipo de cuenta incorrecto.',
                                    'Escoger una cuenta de captura (C)',
                                    'warning'
                                    ).then((result) => {
                                        app.nombreCuenta = '';
                                        app.cuenta = '';

                                    });
                                }

                            }else{
                                Swal.fire(
                                    'Cuenta incorrecta.',
                                    'Esta cuenta no existe en el catalogo CCPET.',
                                    'warning'
                                    ).then((result) => {
                                        app.nombreCuenta = '';
                                        app.cuenta = '';
                                    });
                            }

                        });
                    }else{
                        await axios.post('vue/presupuesto_ccp/ccp-cdpbasico.php?action=buscarCuenta&cuenta=' + this.cuenta)
                        .then((response) => {
                            if(response.data.resultBusquedaCuenta.length > 0){
                                if(response.data.resultBusquedaCuenta[0][1] == 'C'){
                                    app.nombreCuenta = response.data.resultBusquedaCuenta[0][0];

                                }else{
                                    Swal.fire(
                                    'Tipo de cuenta incorrecto.',
                                    'Escoger una cuenta de captura (C)',
                                    'warning'
                                    ).then((result) => {
                                        app.nombreCuenta = '';
                                        app.cuenta = '';

                                    });
                                }

                            }else{
                                Swal.fire(
                                    'Cuenta incorrecta.',
                                    'Esta cuenta no existe en el catalogo CCPET.',
                                    'warning'
                                    ).then((result) => {
                                        app.nombreCuenta = '';
                                        app.cuenta = '';
                                    });
                            }

                        });
                    }
                }else{
                    Swal.fire(
                        'Falta escoger el tipo de cuenta.',
                        'Escoger el tipo de cuenta, ingresos o egresos.',
                        'warning'
                        ).then((result) => {
                            app.nombreCuenta = '';
                            app.cuenta = '';
                        });
                }
            }else{
                this.nombreCuenta = '';
            }
        },

        seleccionarCuenta: function(cuentaSelec){
            if(cuentaSelec[6] == 'C'){

                this.cuenta = cuentaSelec [1];
                this.nombreCuenta = cuentaSelec [2];
                this.showModal_cuentas = false;
                const clasificadoresProgramados = app.arrClasificadoresProgramados.filter(function(e){return e.cuenta == app.cuenta});
                if(clasificadoresProgramados.length > 0){
                    app.isClasificador = true;
                }else{
                    app.isClasificador = false;
                }

            }else{


                this.showModal_cuentas = false;
                Swal.fire(
                'Tipo de cuenta incorrecto.',
                'Escoger una cuenta de captura (C)',
                'warning'
                ).then((result) => {

                    if(result.isConfirmed || result.isDismissed){
                        this.showModal_cuentas = true;
                    }

                });


            }
        },

        searchMonitorCuenta: function(){
            var keywordCuenta = app.toFormData(app.searchCuenta);
            if(this.tipo_cuenta == 'I'){
                axios.post('presupuesto_ccpet/adicion/crear/ccp-adicion.php?action=filtrarCuentasIngreso', keywordCuenta)
                .then((response) => {
                    app.cuentasCcpet = response.data.cuentasCcpet;

                });
            }else{
                axios.post('presupuesto_ccpet/adicion/crear/ccp-adicion.php?action=filtrarCuentas&inicioCuenta=' + this.tiposDeGasto[this.tipoGasto-1][2], keywordCuenta)
                .then((response) => {
                    app.cuentasCcpet = response.data.cuentasCcpet;

                });
            }
        },

        ventanaFuente: function(){

            if(this.tipoGasto != '3'){
                if(this.cuenta != ''){
                    app.showModal_fuentes = true;
                    axios.post('presupuesto_ccpet/adicion/crear/ccp-adicion.php?action=cargarFuentes')
                        .then((response) => {
                            app.fuentesCuipo = response.data.fuentes;

                        });
                }else{
                    Swal.fire(
                        'Falta escoger la cuenta presupuestal CCPET.',
                        'Escoger la cuenta antes de buscar la fuente, para acotar la busqueda.',
                        'warning'
                    )
                }

            }else{
                if(this.codProyecto != '' && this.programatico != ''){
                    app.showModal_fuentes = true;

                    axios.post('presupuesto_ccpet/adicion/crear/ccp-adicion.php?action=cargarFuentes')
                        .then((response) => {
                            app.fuentesCuipo = response.data.fuentes;

                        });
                }else{
                    Swal.fire(
                        'Falta escoger el proyecto o indicador programatico.',
                        'Escoger el proyecto o indicador programatico antes de buscar la fuente, para acotar la busqueda.',
                        'warning'
                    )
                }
            }


        },

        buscarFuente: async function(){

            if(this.fuente != ''){
                await axios.post('vue/presupuesto_ccp/ccp-cdpbasico.php?action=buscarFuente&fuente=' + this.fuente)
                .then((response) => {
                    if(response.data.resultBusquedaFuente.length > 0){

                        app.nombreFuente = response.data.resultBusquedaFuente[0][0];

                    }else{
                        Swal.fire(
                            'Fuente incorrecta.',
                            'Esta Fuente no existe en las fuentes CUIPO del sistema.',
                            'warning'
                            ).then((result) => {
                                app.nombreFuente = '';
                                app.fuente = '';
                            });
                    }

                });
            }else{
                this.nombreFuente = '';
                this.fuente = '';
            }
        },

        searchMonitorFuente: function(){
            var keywordFuente = app.toFormData(app.searchFuente);
            axios.post('vue/presupuesto_ccp/ccp-cdpbasico.php?action=filtrarFuentes', keywordFuente)
            .then((response) => {
                app.fuentesCuipo = response.data.fuentes;

            });
        },

        seleccionarFuente: function(fuenteSelec){
            this.fuente = fuenteSelec [0];
            this.nombreFuente = fuenteSelec [1];
            this.showModal_fuentes = false;
        },

        ventanaProyecto: function(){
            app.showModal_proyectos = true;
            axios.post('vue/presupuesto_ccp/ccp-cdpbasico.php?action=cargarProyecto&vigencia='+this.vigencia)
                .then((response) => {

                    app.proyectos = response.data.proyectosCcpet;
                });
        },

        seleccionarProyecto: function(proyectoSelec){

            this.codProyecto = proyectoSelec [2];
            this.nombreProyecto = proyectoSelec [4];
            this.showModal_proyectos = false;

        },

        buscarProyecto: async function(){
            if(this.codProyecto != ''){
                await axios.post('vue/presupuesto_ccp/ccp-cdpbasico.php?action=buscarProyecto&proyecto=' + this.codProyecto)
                .then((response) => {
                    if(response.data.resultBusquedaProyecto.length > 0){
                            app.nombreProyecto = response.data.resultBusquedaProyecto[0][0];
                    }else{
                        Swal.fire(
                            'Proyecto incorrecto.',
                            'Este proyecto no existe.',
                            'warning'
                            ).then((result) => {
                                app.nombreProyecto = '';
                            });
                    }

                });

            }else{
                this.nombreProyecto = '';
            }
        },

        searchMonitorProyecto: function(){
            var keywordProyecto = app.toFormData(app.searchProyecto);
            axios.post('vue/presupuesto_ccp/ccp-cdpbasico.php?action=filtrarProyectos&vigencia=' + this.vigencia, keywordProyecto)
            .then((response) => {
                app.proyectos = response.data.proyectosCcpet;
            });
        },

        ventanaProgramatico: function(){
            if(this.codProyecto != ''){
                app.showModal_programatico = true;
                axios.post('presupuesto_ccpet/adicion/crear/ccp-adicion.php?action=cargarProgramaticos&proyecto=' + this.codProyecto)
                .then((response) => {
                    app.programaticos = response.data.programaticos;
                });
            }else{
                Swal.fire(
                    'Falta escoger el proyecto.',
                    'Antes de seleccionar el program&aacute;tico se debe seleccionar un proyecto.',
                    'warning'
                    ).then((result) => {
                        app.programaticos = [];
                    });
            }


        },

        buscarProgramatico: async function(bandera = false){
            if(this.programatico != ''){
                if(this.codProyecto != '' || bandera){
                    await axios.post('vue/presupuesto_ccp/ccp-cdpbasico.php?action=buscarProgramatico&programatico=' + this.programatico)
                    .then((response) => {
                        if(response.data.resultBusquedaProgramatico.length > 0){
                                app.nombreProgramatico = response.data.resultBusquedaProgramatico[0][0];
                                this.changeProgramatico();
                        }else{
                            Swal.fire(
                                'Proyecto incorrecto.',
                                'Este proyecto no existe.',
                                'warning'
                                ).then((result) => {
                                    app.nombreProgramatico = '';
                                    app.programatico = '';
                                });
                        }

                    });
                }else{
                    Swal.fire(
                        'Falta escoger un proyecto.',
                        '',
                        'info'
                        );
                }


            }else{
                this.nombreProgramatico = '';
            }
        },

        seleccionarProgramatico: function(programaticoSelec){

            this.programatico = programaticoSelec [0];
            this.nombreProgramatico = programaticoSelec [1];
            this.showModal_programatico = false;
            this.changeProgramatico();
        },

        aprobarFuentes(){

        },

        async buscarRubroPresupuestal (){
            let partesRubroPresupuestal = divideRubro(this.rubroPresupuestal);

            const tipoGastos = {
                'funcionamiento': 1,
                'deuda': 2,
                'inversion': 3,
                'comercializacion': 4,
                'ingresos': 5
            }

            /* let claves = Object.keys(partesRubroPresupuestal); */

            let claves = Object.keys(partesRubroPresupuestal);

            if( claves.length == 0 ){
                await this.cambiaTipoCuenta(false);
                return;
            }

            for(let i = 0; i < claves.length; i++){
                let clave = claves[i];

                if(clave == 'tipoObjeto'){
                    if (tipoGastos[partesRubroPresupuestal[clave]] == 5){
                        this.tipo_cuenta = 'I';
                        await this.cambiaTipoCuenta(false);
                    }else{
                        await this.cargarTiposDeGasto();
                        this.tipo_cuenta = 'G';
                        this.tipoGasto = tipoGastos[partesRubroPresupuestal[clave]];

                        await this.cambiaTipoCuenta(false);
                    }


                    await this.cambiaTipoDeGasto();
                    await this.cambiaParametro();
                }

                if(clave == 'cuenta'){
                    this.cuenta = partesRubroPresupuestal[clave];
                    this.buscarCta();
                }

                if(clave == 'fuente'){
                    this.fuente = partesRubroPresupuestal[clave];
                    this.buscarFuente();
                }

                if(clave == 'bpim'){
                    this.codProyecto = partesRubroPresupuestal[clave];
                    await this.buscarProyecto();
                }

                if(clave == 'programatico'){
                    this.programatico = partesRubroPresupuestal[clave];
                    this.buscarProgramatico(true);
                    this.changeProgramatico();
                }



                if(clave == 'seccion_presupuestal'){
                    this.selectUnidadEjecutora = partesRubroPresupuestal[clave];
                }

                if(clave == 'vigencia_gasto'){
                    this.selectVigenciaGasto = partesRubroPresupuestal[clave];
                }

                if(clave == 'medio_pago'){
                    this.selectMedioPago = partesRubroPresupuestal[clave];
                }

            }

        },

        toFormData: function(obj){
            var form_data = new FormData();
            for(var key in obj){
                form_data.append(key, obj[key]);
            }
            return form_data;
        },

        formatonumero: function(valor){
            valor = valor == null ? 0 : valor
            return new Intl.NumberFormat("en-US", {style: "currency", currency: "USD"}).format(valor);
        },

        mostrarAdicion: function(){
            this.isActiveAd = true;
        },



    }
});
