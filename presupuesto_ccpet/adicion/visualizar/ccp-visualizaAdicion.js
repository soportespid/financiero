var app = new Vue({
    el: '#myapp',
	data:{

        error: '',
        loading: false,
        
        fecha: '',
        vigencia: '',
        actoAdm: '',
        actoAdmId: '',
        valorAdicion: 0,
        estado: '',
        letras: '',
        detalles: [],
        totalGasto: 0,
        totalIngreso: 0,
        uniEjecutoras: [],
        basesApp: [],
    },

    mounted: async function(){

        await this.uniEjecutorasSearch();
        this.traeDatosFormulario();
        
    },

    methods: 
    {
        //Desglosar array
        toFormData: function(obj){
            var form_data = new FormData();
            for(var key in obj){
                form_data.append(key, obj[key]);
            }
            return form_data;
        },

        formatonumero: function(valor){
            return new Intl.NumberFormat("en-US", {style: "currency", currency: "USD"}).format(valor);
        },

        traeParametros: function(name){
            //Captura parametros
            name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
            var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
            results = regex.exec(location.search);
            return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
        },

        uniEjecutorasSearch: async function(){

            await axios.post('presupuesto_ccpet/adicion/crear/ccp-adicion.php?action=unidades')
            .then(
                (response)=>{
                    this.uniEjecutoras = response.data.uniEjecutoras;
                }
            );
            this.bases();
        },

        bases(){
            const basesUnidad = this.uniEjecutoras.map(uniEjecutora => {
                return  {
                            base: uniEjecutora[2], 
                            usuario: uniEjecutora[6],
                            unidad: uniEjecutora[1],
                            nombre: uniEjecutora[3].toLowerCase(),
                            tipo: uniEjecutora[4],
                            id: uniEjecutora[0],
                        };
            });
            this.basesApp = basesUnidad;
        },

        traeDatosFormulario: async function() {

            const acuerdoId = this.traeParametros('id');

            let formData = new FormData();
            console.log(this.basesApp);
            this.basesApp.map((b, i) => {
                formData.append("basesApp["+i+"][]", Object.values(b));
            })
            
            await axios.post('presupuesto_ccpet/adicion/visualizar/ccp-visualizaAdicion.php?action=datosFormulario&acuerdoId='+acuerdoId, formData)
            .then((response) => {
                this.fecha = response.data.fecha;
                this.vigencia = response.data.vigencia;
                this.actoAdm = response.data.actoAdm;
                this.actoAdmId = response.data.actoAdmId;
                this.estado = response.data.estado;
                this.valorAdicion = response.data.valorAdicion;
                this.letras = response.data.letras;
                this.detalles = response.data.detalles;
                this.totalGasto = response.data.totalGasto;
                this.totalIngreso = response.data.totalIngreso;
            }).catch((error) => {
                this.error = true;
                console.log(error)
            }).finally(() => {
                
               
            });
        },

        excel: function() {

            if (this.detalles != '') {

                document.form2.action="ccp-excelAdicion.php";
				document.form2.target="_BLANK";
				document.form2.submit(); 
            }
            else {
                Swal.fire("Faltan datos", "warning");
            }
        },
    }
});