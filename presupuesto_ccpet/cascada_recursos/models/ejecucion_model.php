<?php
    require_once '../../../Librerias/core/Helpers.php';
    require_once '../../../Librerias/core/Mysql.php';
    session_start();
    /*ini_set('display_errors', '1');
    ini_set('display_startup_errors', '1');
    error_reporting(E_ALL);*/
    class EjecucionModel extends Mysql{
        private $intVigencia;
        private $intMes;
        function __construct(){
            parent::__construct();
        }

        public function selectInicial($intVigencia,$strPago,$strGasto){
            $this->intVigencia = $intVigencia;
            $sqlPago = "COALESCE(SUM(pp.valorcsf)+SUM(pp.valorssf),0)";
            if($strPago =="CSF"){
                $sqlPago = "COALESCE(SUM(pp.valorcsf),0)";
            }else if($strPago =="SSF"){
                $sqlPago = "COALESCE(SUM(pp.valorssf),0)";
            }
            $sql = "SELECT $sqlPago  as total
            FROM ccpproyectospresupuesto p
            INNER JOIN ccpproyectospresupuesto_presupuesto pp ON p.id = pp.codproyecto
            WHERE p.estado = 'S' AND p.vigencia = '$this->intVigencia' AND pp.vigencia_gasto like '$strGasto%'";
            $request = $this->select($sql)['total'];
            return $request;
        }
        public function selectInicialGastos($intVigencia,$strPago,$strGasto,$tipo ="funcionamiento"){
            $this->intVigencia = $intVigencia;
            if($tipo=="funcionamiento"){
                $sqlTipo = "cuenta like '2.1%'";
            }else if($tipo=="operacion"){
                $sqlTipo = "cuenta like '2.4%'";
            }else if($tipo=="deuda"){
                $sqlTipo = "cuenta like '2.2%'";
            }
            $sql = "SELECT COALESCE(SUM(valor),0) as total FROM ccpetinicialgastosfun
            WHERE vigencia = '$this->intVigencia' AND $sqlTipo AND medio_pago like '$strPago%'
            AND vigencia_gasto like '$strGasto%' ";
            $request = $this->select($sql)['total'];
            return $request;
        }
        public function selectAdiciones($intVigencia,$intMes,$strPago,$strGasto,$tipo ="inversion"){
            $this->intVigencia = $intVigencia;
            $this->intMes = $intMes;
            if($tipo=="inversion"){
                $sqlTipo = "bpim != ''";
            }else if($tipo=="funcionamiento"){
                $sqlTipo = "cuenta like '2.1%'";
            }else if($tipo=="operacion"){
                $sqlTipo = "cuenta like '2.4%'";
            }else if($tipo=="deuda"){
                $sqlTipo = "cuenta like '2.2%'";
            }
            $sql = "SELECT COALESCE(SUM(valor),0) as total
            FROM ccpet_adiciones
            WHERE estado='S' AND $sqlTipo AND MONTH(fecha) BETWEEN 1 AND $this->intMes
            AND medio_pago like '$strPago%' AND codigo_vigenciag like '$strGasto%' AND vigencia = '$this->intVigencia'";
            $request = $this->select($sql)['total'];
            return $request;
        }
        public function selectReducciones($intVigencia,$intMes,$strPago,$strGasto,$tipo ="inversion"){
            $this->intVigencia = $intVigencia;
            $this->intMes = $intMes;
            if($tipo=="inversion"){
                $sqlTipo = "bpim != ''";
            }else if($tipo=="funcionamiento"){
                $sqlTipo = "cuenta like '2.1%'";
            }else if($tipo=="operacion"){
                $sqlTipo = "cuenta like '2.4%'";
            }else if($tipo=="deuda"){
                $sqlTipo = "cuenta like '2.2%'";
            }
            $sql = "SELECT COALESCE(SUM(valor),0) as total FROM ccpet_reducciones WHERE estado='S' AND $sqlTipo
            AND medio_pago like '$strPago%' AND codigo_vigenciag like '$strGasto%' AND MONTH(fecha) BETWEEN 1 AND $this->intMes
            AND vigencia = '$this->intVigencia'";
            $request = $this->select($sql)['total'];
            return $request;
        }
        public function selectTraslados($intVigencia,$intMes,$strPago,$strGasto,$tipo ="inversion"){
            $this->intVigencia = $intVigencia;
            $this->intMes = $intMes;
            if($tipo=="inversion"){
                $sqlTipo = "bpim != ''";
            }else if($tipo=="funcionamiento"){
                $sqlTipo = "cuenta like '2.1%'";
            }else if($tipo=="operacion"){
                $sqlTipo = "cuenta like '2.4%'";
            }else if($tipo=="deuda"){
                $sqlTipo = "cuenta like '2.2%'";
            }
            $sql = "SELECT COALESCE(SUM(valor),0) as total
            FROM ccpet_traslados
            WHERE estado='S' AND tipo = 'C' AND $sqlTipo
            AND medio_pago like '$strPago%' AND codigo_vigenciag like '$strGasto%'
            AND MONTH(fecha) BETWEEN 1 AND $this->intMes AND vigencia = '$this->intVigencia'";

            $sql2 = "SELECT COALESCE(SUM(valor),0) as total
            FROM ccpet_traslados
            WHERE estado='S' AND tipo = 'R' AND $sqlTipo
            AND medio_pago like '$strPago%' AND codigo_vigenciag like '$strGasto%'
            AND MONTH(fecha) BETWEEN 1 AND $this->intMes AND vigencia = '$this->intVigencia'";
            $request = $this->select($sql)['total'] - $this->select($sql2)['total'];
            return $request;
        }
        public function selectCdp($intVigencia,$intMes,$strPago,$strGasto,$tipo ="inversion",$flag=true){
            $this->intVigencia = $intVigencia;
            $this->intMes = $intMes;
            $sql ="";
            $tipoBusqueda = "AND MONTH(cab.fecha) BETWEEN 1 AND $this->intMes";
            if(!$flag){
                $tipoBusqueda = "AND MONTH(cab.fecha) = $this->intMes";
            }
            if($tipo=="operacion"){
                $sqlTipo = "det.cuenta like '2.4%'";
            }else if($tipo=="deuda"){
                $sqlTipo = "det.cuenta like '2.2%'";
            }else if($tipo=="inversion"){
                $sqlTipo = "det.cuenta like '2.3%'";
            }
            $sql = "SELECT
            det.seccion_presupuestal,
            det.bpim,
            det.fuente,
            det.medio_pago,
            det.indicador_producto,
            COALESCE(SUM(det.valor),0) as valor,
            det.codigo_vigenciag,
            cab.consvigencia,
            cab.vigencia
            FROM ccpetcdp cab
            INNER JOIN ccpetcdp_detalle det ON cab.consvigencia = det.consvigencia AND cab.vigencia = det.vigencia
            WHERE  det.vigencia = '$this->intVigencia' AND cab.vigencia = '$this->intVigencia' AND $sqlTipo
            AND cab.tipo_mov = '201' AND det.tipo_mov = '201' AND det.codigo_vigenciag like '$strGasto%'  $tipoBusqueda
            GROUP BY det.seccion_presupuestal, det.bpim, det.fuente, det.medio_pago,
            det.indicador_producto, det.codigo_vigenciag, cab.consvigencia, cab.vigencia;";

            if($tipo=="funcionamiento"){
                $sql = "SELECT DISTINCT
                det.cuenta, det.fuente,
                det.seccion_presupuestal, det.codigo_vigenciag,
                det.medio_pago, SUM(det.valor) as valor, cab.consvigencia, cab.vigencia
                FROM ccpetcdp cab
                INNER JOIN  ccpetcdp_detalle det ON cab.vigencia =det.vigencia AND cab.consvigencia = det.consvigencia
                WHERE cab.vigencia = $this->intVigencia AND det.vigencia = $this->intVigencia
                AND (det.cuenta like '2.1%' OR det.cuenta like '2.2%' OR det.cuenta like det.cuenta like '2.4%')
                AND cab.tipo_mov = '201' AND det.tipo_mov = '201' AND det.codigo_vigenciag like '$strGasto%'
                $tipoBusqueda GROUP BY det.cuenta, det.fuente, det.seccion_presupuestal,
                det.codigo_vigenciag, det.medio_pago";
            }
            $totalCdp = 0;
            $totalCdpRev = 0;
            if($sql != ""){
                $arrCdp = $this->select_all($sql);
                if(!empty($arrCdp)){
                    foreach ($arrCdp as $cdp) {
                        $totalCdp+= $cdp['valor'];
                        $sql = "SELECT COALESCE(SUM(det.valor),0) as valor
                        FROM ccpetcdp cab
                        INNER JOIN  ccpetcdp_detalle det ON cab.vigencia =det.vigencia AND cab.consvigencia = det.consvigencia
                        WHERE det.tipo_mov like '4%' AND cab.vigencia = $this->intVigencia AND det.vigencia = $this->intVigencia
                        AND cab.tipo_mov like '4%' AND det.medio_pago like '$strPago%' AND det.codigo_vigenciag like '$strGasto%'
                        $tipoBusqueda AND $sqlTipo AND det.seccion_presupuestal = '$cdp[seccion_presupuestal]'
                        AND det.fuente = '$cdp[fuente]' AND det.medio_pago = '$cdp[medio_pago]' AND det.bpim = '$cdp[bpim]'
                        AND det.codigo_vigenciag = '$cdp[codigo_vigenciag]' AND det.indicador_producto = '$cdp[indicador_producto]'
                        AND det.consvigencia = '$cdp[consvigencia]' AND det.vigencia = '$cdp[vigencia]'
                        GROUP BY det.seccion_presupuestal, det.bpim, det.fuente, det.medio_pago, det.indicador_producto,
                        det.codigo_vigenciag, cab.consvigencia, cab.vigencia,det.cuenta";

                        if($tipo=="funcionamiento"){
                            $sql = "SELECT SUM(det.valor) as valor
                            FROM ccpetcdp cab
                            INNER JOIN  ccpetcdp_detalle det ON cab.vigencia =det.vigencia AND cab.consvigencia = det.consvigencia
                            WHERE cab.vigencia = '$this->intVigencia'
                            AND det.vigencia = '$this->intVigencia' AND det.cuenta = '$cdp[cuenta]'
                            AND cab.tipo_mov LIKE '4%' AND det.tipo_mov LIKE '4%' AND det.fuente = '$cdp[fuente]'
                            AND det.seccion_presupuestal = '$cdp[seccion_presupuestal]' AND det.codigo_vigenciag = '$cdp[codigo_vigenciag]'
                            AND det.medio_pago = '$cdp[medio_pago]' $tipoBusqueda
                            GROUP BY det.cuenta, det.fuente, det.seccion_presupuestal, det.codigo_vigenciag, det.medio_pago";
                        }
                        $totalCdpRev += $this->select($sql)['valor'];
                    }
                }
            }
            $request = $totalCdp - $totalCdpRev;
            return $request;
        }
        public function selectRp($intVigencia,$intMes,$strPago,$strGasto,$tipo ="inversion",$flag=true){
            $this->intVigencia = $intVigencia;
            $this->intMes = $intMes;
            $tipoBusqueda = "AND MONTH(cab.fecha) BETWEEN 1 AND $this->intMes";
            if(!$flag){
                $tipoBusqueda = "AND MONTH(cab.fecha) = $this->intMes";
            }
            if($tipo=="inversion"){
                $sqlTipo = "det.bpim != ''";
            }else if($tipo=="funcionamiento"){
                $sqlTipo = "det.cuenta like '2.1%'";
            }else if($tipo=="operacion"){
                $sqlTipo = "det.cuenta like '2.4%'";
            }else if($tipo=="deuda"){
                $sqlTipo = "det.cuenta like '2.2%'";
            }
            $sql = "SELECT COALESCE(SUM(det.valor),0) as total
            FROM ccpetrp cab
            INNER JOIN  ccpetrp_detalle det ON cab.vigencia =det.vigencia AND cab.consvigencia = det.consvigencia
            WHERE det.tipo_mov='201' AND cab.vigencia = $this->intVigencia AND cab.tipo_mov = '201'
            AND det.medio_pago like '$strPago%' AND det.codigo_vigenciag like '$strGasto%'
            $tipoBusqueda AND $sqlTipo";

            $sql2 = "SELECT COALESCE(SUM(det.valor),0) as total
            FROM ccpetrp cab
            INNER JOIN  ccpetrp_detalle det ON cab.vigencia =det.vigencia AND cab.consvigencia = det.consvigencia
            WHERE det.tipo_mov like '4%' AND cab.vigencia = $this->intVigencia AND cab.tipo_mov like '4%'
            AND det.medio_pago like '$strPago%' AND det.codigo_vigenciag like '$strGasto%'
            $tipoBusqueda AND $sqlTipo";
            $request = $this->select($sql)['total'] - $this->select($sql2)['total'];
            return $request;
        }
        public function selectOrdenPago($intVigencia,$intMes,$strPago,$strGasto,$tipo ="inversion",$flag=true){
            $this->intVigencia = $intVigencia;
            $this->intMes = $intMes;
            $tipoBusqueda = "AND MONTH(cab.fecha) BETWEEN 1 AND $this->intMes";
            if(!$flag){
                $tipoBusqueda = "AND MONTH(cab.fecha) = $this->intMes";
            }
            if($tipo=="inversion"){
                $sqlTipo = "det.bpim != ''";
            }else if($tipo=="funcionamiento"){
                $sqlTipo = "det.cuentap like '2.1%'";
            }else if($tipo=="operacion"){
                $sqlTipo = "det.cuentap like '2.4%'";
            }else if($tipo=="deuda"){
                $sqlTipo = "det.cuentap like '2.2%'";
            }
            $sql = "SELECT COALESCE(SUM(det.valor),0) as total
            FROM tesoordenpago cab
            INNER JOIN  tesoordenpago_det det ON cab.id_orden =det.id_orden
            WHERE det.tipo_mov='201' AND cab.vigencia = $this->intVigencia
            AND cab.tipo_mov = '201' AND det.vigencia = $this->intVigencia AND cab.estado != 'R'
            AND cab.estado != 'N' AND det.medio_pago like '$strPago%' AND det.codigo_vigenciag like '$strGasto%'

            $tipoBusqueda AND $sqlTipo";


            $sql2 = "SELECT COALESCE(SUM(det.valor),0) as total
            FROM tesoordenpago cab
            INNER JOIN  tesoordenpago_det det ON cab.id_orden =det.id_orden
            WHERE det.tipo_mov like '4%' AND cab.vigencia = $this->intVigencia AND cab.tipo_mov like '4%'
            AND det.medio_pago like '$strPago%' AND det.codigo_vigenciag like '$strGasto%'
            $tipoBusqueda AND $sqlTipo";
            $request = $this->select($sql)['total'] - $this->select($sql2)['total'];
            return $request;
        }
        public function selectNomina($intVigencia,$intMes,$strPago,$strGasto,$tipo ="inversion",$flag=true){
            $this->intVigencia = $intVigencia;
            $this->intMes = $intMes;
            $tipoBusqueda = "AND MONTH(cab.fecha) BETWEEN 1 AND $this->intMes";
            if(!$flag){
                $tipoBusqueda = "AND MONTH(cab.fecha) = $this->intMes";
            }
            if($tipo=="inversion"){
                $sqlTipo = "det.bpin != ''";
            }else if($tipo=="funcionamiento"){
                $sqlTipo = "det.cuenta like '2.1%'";
            }else if($tipo=="operacion"){
                $sqlTipo = "det.cuenta like '2.4%'";
            }else if($tipo=="deuda"){
                $sqlTipo = "det.cuenta like '2.2%'";
            }
            $sql = "SELECT COALESCE(SUM(det.valor),0) as total
            FROM humnomina_aprobado cab
            INNER JOIN  humnom_presupuestal det ON cab.id_nom =det.id_nom
            WHERE det.estado='P' AND cab.vigencia = $this->intVigencia AND cab.estado != 'N'
            AND det.medio_pago like '$strPago%' AND det.vigencia_gasto like '$strGasto%'
            $tipoBusqueda AND $sqlTipo";
            $request = $this->select($sql)['total'];
            return $request;
        }
        public function selectEgresos($intVigencia,$intMes,$strPago,$strGasto,$tipo ="inversion",$flag=true){
            $this->intVigencia = $intVigencia;
            $this->intMes = $intMes;
            $tipoBusqueda = "AND MONTH(cab.fecha) BETWEEN 1 AND $this->intMes";
            if(!$flag){
                $tipoBusqueda = "AND MONTH(cab.fecha) = $this->intMes";
            }
            if($tipo=="inversion"){
                $sqlTipo = "det.bpim != ''";
            }else if($tipo=="funcionamiento"){
                $sqlTipo = "det.cuentap like '2.1%'";
            }else if($tipo=="operacion"){
                $sqlTipo = "det.cuentap like '2.4%'";
            }else if($tipo=="deuda"){
                $sqlTipo = "det.cuentap like '2.2%'";
            }
            $sql = "SELECT COALESCE(SUM(det.valor),0) as total
            FROM tesoegresos cab
            INNER JOIN  tesoordenpago_det det ON cab.id_orden =det.id_orden
            WHERE cab.vigencia = $this->intVigencia AND det.vigencia = $this->intVigencia AND cab.estado NOT IN ('R','N')
            AND det.medio_pago like '$strPago%' AND det.codigo_vigenciag like '$strGasto%'
            $tipoBusqueda AND $sqlTipo";
            $request = $this->select($sql)['total'];
            return $request;
        }
        public function selectEgresosNomina($intVigencia,$intMes,$strPago,$strGasto,$tipo ="inversion",$flag=true){
            $this->intVigencia = $intVigencia;
            $this->intMes = $intMes;
            $tipoBusqueda = "AND MONTH(cab.fecha) BETWEEN 1 AND $this->intMes";
            if(!$flag){
                $tipoBusqueda = "AND MONTH(cab.fecha) = $this->intMes";
            }
            if($tipo=="inversion"){
                $sqlTipo = "det.bpin != ''";
            }else if($tipo=="funcionamiento"){
                $sqlTipo = "det.cuentap like '2.1%'";
            }else if($tipo=="operacion"){
                $sqlTipo = "det.cuentap like '2.4%'";
            }else if($tipo=="deuda"){
                $sqlTipo = "det.cuentap like '2.2%'";
            }

            $sql = "SELECT COALESCE(SUM(det.valordevengado),0) as total
            FROM tesoegresosnomina cab
            INNER JOIN  tesoegresosnomina_det det ON cab.id_egreso =det.id_egreso
            WHERE cab.vigencia = $this->intVigencia AND cab.estado = 'S'
            AND NOT(det.tipo='SE' OR det.tipo='PE' OR det.tipo='DS' OR det.tipo='RE' OR det.tipo='FS')
            AND det.medio_pago like '$strPago%' AND det.vigencia_gasto like '$strGasto%'
            $tipoBusqueda AND $sqlTipo";
            $request = $this->select($sql)['total'] ;
            return $request;
        }

    }
?>
