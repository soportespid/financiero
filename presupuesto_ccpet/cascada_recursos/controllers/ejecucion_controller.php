<?php
    require_once '../../../Librerias/core/Helpers.php';
    require_once '../models/ejecucion_model.php';
    session_start();

    class EjecucionController extends EjecucionModel{

        public function initialData(){
            if(!empty($_SESSION)){
                $intVigencia = intval($_POST['vigencia']);
                $intMes = intval($_POST['mes']);
                $strGasto = strClean($_POST['gasto']);
                $strPago = strClean($_POST['medio']);
                //Inicial
                $inversionInicial =
                $this->selectInicial($intVigencia,$strPago,$strGasto)+
                $this->selectAdiciones($intVigencia,$intMes,$strPago,$strGasto)-
                $this->selectReducciones($intVigencia,$intMes,$strPago,$strGasto)+
                $this->selectTraslados($intVigencia,$intMes,$strPago,$strGasto);


                $funcionamientoInicial =
                $this->selectInicialGastos($intVigencia,$strPago,$strGasto,"funcionamiento")+
                $this->selectAdiciones($intVigencia,$intMes,$strPago,$strGasto,"funcionamiento")-
                $this->selectReducciones($intVigencia,$intMes,$strPago,$strGasto,"funcionamiento") +
                $this->selectTraslados($intVigencia,$intMes,$strPago,$strGasto,"funcionamiento");

                $operacionInicial =
                $this->selectInicialGastos($intVigencia,$strPago,$strGasto,"operacion")+
                $this->selectAdiciones($intVigencia,$intMes,$strPago,$strGasto,"operacion")-
                $this->selectReducciones($intVigencia,$intMes,$strPago,$strGasto,"operacion") +
                $this->selectTraslados($intVigencia,$intMes,$strPago,$strGasto,"operacion");

                $deudaInicial =
                $this->selectInicialGastos($intVigencia,$strPago,$strGasto,"deuda")+
                $this->selectAdiciones($intVigencia,$intMes,$strPago,$strGasto,"deuda")-
                $this->selectReducciones($intVigencia,$intMes,$strPago,$strGasto,"deuda") +
                $this->selectTraslados($intVigencia,$intMes,$strPago,$strGasto,"deuda");

                $totalInicial = $inversionInicial+$funcionamientoInicial+$operacionInicial+$deudaInicial;

                //CDPS
                $inversionCdp = $this->selectCdp($intVigencia,$intMes,$strPago,$strGasto,"inversion",false);
                $funcionamientoCdp = $this->selectCdp($intVigencia,$intMes,$strPago,$strGasto,"funcionamiento",false);
                $operacionCdp = $this->selectCdp($intVigencia,$intMes,$strPago,$strGasto,"operacion",false);
                $deudaCdp = $this->selectCdp($intVigencia,$intMes,$strPago,$strGasto,"deuda",false);
                $totalCdp = $inversionCdp+$funcionamientoCdp+$operacionCdp+$deudaCdp;

                //RP
                $inversionRp = $this->selectRp($intVigencia,$intMes-1,$strPago,$strGasto,);
                $funcionamientoRp = $this->selectRp($intVigencia,$intMes-1,$strPago,$strGasto,"funcionamiento");
                $operacionRp = $this->selectRp($intVigencia,$intMes-1,$strPago,$strGasto,"operacion");
                $deudaRp = $this->selectRp($intVigencia,$intMes-1,$strPago,$strGasto,"deuda");
                $totalRp = $inversionRp+$funcionamientoRp+$operacionRp+$deudaRp;

                //RP actual
                $inversionRpActual = $this->selectRp($intVigencia,$intMes,$strPago,$strGasto,"inversion",false);
                $funcionamientoRpActual = $this->selectRp($intVigencia,$intMes,$strPago,$strGasto,"funcionamiento",false);
                $operacionRpActual = $this->selectRp($intVigencia,$intMes,$strPago,$strGasto,"operacion",false);
                $deudaRpActual = $this->selectRp($intVigencia,$intMes,$strPago,$strGasto,"deuda",false);
                $totalRpActual = $inversionRpActual+$funcionamientoRpActual+$operacionRpActual+$deudaRpActual;

                //Obligaciones
                $inversionObligacion = $this->selectOrdenPago($intVigencia,$intMes,$strPago,$strGasto,"inversion",false)+
                $this->selectNomina($intVigencia,$intMes,$strPago,$strGasto,"inversion",false);
                $funcionamientoObligacion = $this->selectOrdenPago($intVigencia,$intMes,$strPago,$strGasto,"funcionamiento",false)+
                $this->selectNomina($intVigencia,$intMes,$strPago,$strGasto,"funcionamiento",false);
                $operacionObligacion = $this->selectOrdenPago($intVigencia,$intMes,$strPago,$strGasto,"operacion",false)+
                $this->selectNomina($intVigencia,$intMes,$strPago,$strGasto,"operacion",false);
                $deudaObligacion = $this->selectOrdenPago($intVigencia,$intMes,$strPago,$strGasto,"deuda",false)+
                $this->selectNomina($intVigencia,$intMes,$strPago,$strGasto,"deuda",false);
                $totalObligacion = $inversionObligacion+$funcionamientoObligacion+$operacionObligacion+$deudaObligacion;


                //Pagos
                $inversionPagos = $this->selectEgresos($intVigencia,$intMes,$strPago,$strGasto,"inversion",false)+
                $this->selectEgresosNomina($intVigencia,$intMes,$strPago,$strGasto,"inversion",false);
                $funcionamientoPagos = $this->selectEgresos($intVigencia,$intMes,$strPago,$strGasto,"funcionamiento",false)+
                $this->selectEgresosNomina($intVigencia,$intMes,$strPago,$strGasto,"funcionamiento",false);
                $operacionPagos = $this->selectEgresos($intVigencia,$intMes,$strPago,$strGasto,"operacion",false)+
                $this->selectEgresosNomina($intVigencia,$intMes,$strPago,$strGasto,"operacion",false);
                $deudaPagos = $this->selectEgresos($intVigencia,$intMes,$strPago,$strGasto,"deuda",false)+
                $this->selectEgresosNomina($intVigencia,$intMes,$strPago,$strGasto,"deuda",false);
                $totalPagos = $inversionPagos+$funcionamientoPagos+$operacionPagos+$deudaPagos;

                $arrResponse = array(
                    "inicial"=>array(
                        "inversion"=>formatNum($inversionInicial),
                        "funcionamiento"=>formatNum($funcionamientoInicial),
                        "operacion"=>formatNum($operacionInicial),
                        "deuda"=>formatNum($deudaInicial),
                        "total"=>formatNum($totalInicial)
                    ),
                    "rps"=>array(
                        "inversion"=>formatNum($inversionRp),
                        "funcionamiento"=>formatNum($funcionamientoRp),
                        "operacion"=>formatNum($operacionRp),
                        "deuda"=>formatNum($deudaRp),
                        "total"=>formatNum($totalRp)
                    ),
                    "actual"=>array(
                        "cdps"=>array(
                            "inversion"=>formatNum($inversionCdp),
                            "funcionamiento"=>formatNum($funcionamientoCdp),
                            "operacion"=>formatNum($operacionCdp),
                            "deuda"=>formatNum($deudaCdp),
                            "total"=>formatNum($totalCdp)
                        ),
                        "rps"=>array(
                            "inversion"=>formatNum($inversionRpActual),
                            "funcionamiento"=>formatNum($funcionamientoRpActual),
                            "operacion"=>formatNum($operacionRpActual),
                            "deuda"=>formatNum($deudaRpActual),
                            "total"=>formatNum($totalRpActual)
                        ),
                        "obligaciones"=>array(
                            "inversion"=>formatNum($inversionObligacion),
                            "funcionamiento"=>formatNum($funcionamientoObligacion),
                            "operacion"=>formatNum($operacionObligacion),
                            "deuda"=>formatNum($deudaObligacion),
                            "total"=>formatNum($totalObligacion)
                        ),
                        "pagos"=>array(
                            "inversion"=>formatNum($inversionPagos),
                            "funcionamiento"=>formatNum($funcionamientoPagos),
                            "operacion"=>formatNum($operacionPagos),
                            "deuda"=>formatNum($deudaPagos),
                            "total"=>formatNum($totalPagos)
                        )

                    ),
                    "saldos"=>array(
                        "inversion"=>formatNum($inversionInicial-$inversionRp),
                        "funcionamiento"=>formatNum($funcionamientoInicial-$funcionamientoRp),
                        "operacion"=>formatNum($operacionInicial-$operacionRp),
                        "deuda"=>formatNum($deudaInicial-$deudaRp),
                        "total"=>formatNum($totalInicial-$totalRp)
                    )
                );
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
            die();
        }
    }
    if($_POST){
        $obj = new EjecucionController();
        if($_POST['action'] == "get"){
            $obj->initialData();
        }
    }

?>
