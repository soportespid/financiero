const URL ='presupuesto_ccpet/cascada_recursos/controllers/ejecucion_controller.php';

var app = new Vue({
    el:"#myapp",
    data() {
        return {
            isLoading:false,
            arrFechas:[],
            selectVigencia:new Date().getFullYear(),
            selectMes: new Date().getMonth()+1,
            selectPago:"",
            selectGasto:"",
            arrMeses:{
                1:{"valor":1,"nombre":"Enero"},
                2:{"valor":2,"nombre":"Febrero"},
                3:{"valor":3,"nombre":"Marzo"},
                4:{"valor":4,"nombre":"Abril"},
                5:{"valor":5,"nombre":"Mayo"},
                6:{"valor":6,"nombre":"Junio"},
                7:{"valor":7,"nombre":"Julio"},
                8:{"valor":8,"nombre":"Agosto"},
                9:{"valor":9,"nombre":"Septiembre"},
                10:{"valor":10,"nombre":"Octubre"},
                11:{"valor":11,"nombre":"Noviembre"},
                12:{"valor":12,"nombre":"Diciembre"},
            },
            objInicial:{},
            objRps:{},
            objSaldos:{},
            objActual:{}
        }
    },
    mounted() {
        this.setVigencias();
        this.getData();
    },
    methods: {
        setVigencias: function (){
            for (let i = this.selectVigencia; i >= 2019 ; i--) {
                this.arrFechas.push(i);
            }
        },
        getData: async function(){
            const formData = new FormData();
            formData.append("action","get");
            formData.append("vigencia",this.selectVigencia);
            formData.append("mes",this.selectMes);
            formData.append("medio",this.selectPago);
            formData.append("gasto",this.selectGasto);
            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();

            this.objActual = objData.actual;
            this.objRps = objData.rps;
            this.objSaldos = objData.saldos;
            this.objInicial = objData.inicial;
        },
    },
})
