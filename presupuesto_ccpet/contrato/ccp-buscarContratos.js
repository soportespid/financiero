/*
const DIR_ORIGIN = window.location.origin + '/';
const DIR_PATHNAME = window.location.pathname.split("/", 2)[1] + '/';
const DIR_FULL = DIR_ORIGIN + '/' + DIR_PATHNAME; */

/* const URL = 'presupuesto_ccpet/AuxiliarGastos/ccp-auxiliarporcomprobantedata.php';
const URL = `presupuesto_ccpet/contrato/ccp-buscarContratos.php?action=buscarContratos`; */

const app = Vue.createApp({

    data() {
      return {
        detalles: [],
        numContrato: '',
        loading: false,
        checked: '',
      }
    },

    mounted() {
        this.loading = false;
        document.getElementById("myapp").style.display="block";
    },

    computed: {
      existeInformacion(){
        const det = JSON.parse(JSON.stringify(this.detalles));
        return det.length > 0 ? true : false
      },

    },

    methods: {

        downloadExl() {
            let fechaInicial = document.getElementById('fc_1198971545').value;
            let fechaFinal = document.getElementById('fc_1198971546').value;

            if(this.numContrato != '' || (fechaInicial && fechaFinal)){
                if(fechaInicial && fechaFinal){
                    let arrFechaInicial = fechaInicial.split("/");
                    fechaInicial = `${arrFechaInicial[2]}-${arrFechaInicial[1]}-${arrFechaInicial[0]}`;
                    let arrFechaFinal = fechaFinal.split("/");
                    fechaFinal = `${arrFechaFinal[2]}-${arrFechaFinal[1]}-${arrFechaFinal[0]}`;
                }else{
                    fechaInicial = '';
                    fechaFinal = '';
                }
            }
            window.open("ccp-buscarContratosExcel.php?fechaInicial="+fechaInicial+"&fechaFinal="+fechaFinal+"&numContrato="+this.numContrato+"&check="+this.checked);
        },

        s2ab(s) {
            if (typeof ArrayBuffer !== 'undefind') {
                var buf = new ArrayBuffer(s.length);
                var view = new Uint8Array(buf);
                for (var i = 0; i != s.length; ++i) view[i] = s.charCodeAt(i) & 0xFF;
                return buf;
            } else {
                var buf = new Array(s.length);
                for (var i = 0; i != s.length; ++i) buf[i] = s.charCodeAt(i) & 0xFF;
                return buf;
            }
        },

      async buscarContratos() {

        let fechaInicial = document.getElementById('fc_1198971545').value;
        let fechaFinal = document.getElementById('fc_1198971546').value;

        if(this.numContrato != '' || (fechaInicial && fechaFinal)){

          if(fechaInicial && fechaFinal){
            let arrFechaInicial = fechaInicial.split("/");
            fechaInicial = `${arrFechaInicial[2]}-${arrFechaInicial[1]}-${arrFechaInicial[0]}`;
            let arrFechaFinal = fechaFinal.split("/");
            fechaFinal = `${arrFechaFinal[2]}-${arrFechaFinal[1]}-${arrFechaFinal[0]}`;
          }else{
            fechaInicial = '';
            fechaFinal = '';
          }

          let formData = new FormData();
          formData.append("fechaInicial", fechaInicial);
          formData.append("fechaFinal", fechaFinal);
          formData.append("numContrato", this.numContrato);
          formData.append("check", this.checked);

          console.log(this.checked);

          const URL = `presupuesto_ccpet/contrato/ccp-buscarContratos.php?action=buscarContratos`;

          this.loading =  true;

          await axios.post(URL, formData)
            .then((response) => {
              console.log(response.data);
              this.detalles = response.data.contratos;

            }).finally(() => {
                this.loading =  false;
            });

        }else{

          Swal.fire(
              'Falta informaci&oacute;n para generar reporte.',
              'Verifique que el n&uacute;mero de contrato o la fecha inicial y final tengan informaci&oacute;n para realizar la busqueda.',
              'warning'
          );

        }

      },

      toFormData(obj){
        var form_data = new FormData();
        for(var key in obj){
            form_data.append(key, obj[key]);
        }
        return form_data;
      },

      formatonumero: function(valor){
        return new Intl.NumberFormat("en-US", {style: "currency", currency: "USD"}).format(valor);
      },

      reiniciarVariable(){
        this.detalles = [];
      },

    }
})

app.mount('#myapp')
