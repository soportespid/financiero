<?php

	require_once '../../comun.inc';
    require '../../funciones.inc';
    session_start();

	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");

    $out = array('error' => false);

    $maxVersion = ultimaVersionGastosCCPET();

    $action = "show";

    if(isset($_GET['action'])){
        $action = $_GET['action'];
    }

    if($action == 'buscarContratos'){

        $contratos = array();

        $critFecha = '';
        $critNumContrato = '';

        if($_POST['fechaInicial'] != '' && $_POST['fechaFinal'] != ''){
            $critFecha = " AND fecha BETWEEN '".$_POST['fechaInicial']."' AND '".$_POST['fechaFinal']."'";
        }

        if($_POST['numContrato'] != ''){
            $critNumContrato = " AND contrato LIKE '%".$_POST['numContrato']."%' ";
        }

        $sqlr = "SELECT vigencia, consvigencia, fecha, detalle, tercero, contrato, idcdp FROM ccpetrp WHERE estado!='N' AND tipo_mov='201' $critFecha $critNumContrato ORDER BY consvigencia DESC";
        $res = mysqli_query($linkbd, $sqlr);
        while($row = mysqli_fetch_assoc($res)){
            $valorRp = 0;
            $valorCxp = 0;
            $saldoPorEjecu = 0;

            $sqlr_rev_tot = "SELECT SUM(valor) FROM ccpetrp_detalle WHERE vigencia = '".$row['vigencia']."' AND consvigencia= '".$row['consvigencia']."' AND tipo_mov LIKE '4%'";
            $res_rev_tot = mysqli_query($linkbd, $sqlr_rev_tot);
            $row_rev_tot = mysqli_fetch_assoc($res_rev_tot);

            $sqlr_det = "SELECT cuenta, productoservicio, fuente, valor, tipo_mov, medio_pago, codigo_vigenciag, bpim, indicador_producto, seccion_presupuestal FROM ccpetrp_detalle WHERE vigencia = '".$row['vigencia']."' AND consvigencia= '".$row['consvigencia']."' AND tipo_mov LIKE '201'";
            $res_det = mysqli_query($linkbd, $sqlr_det);
            while($row_det = mysqli_fetch_assoc($res_det)){


                $valorRpInd = 0;
                if($_POST['check'] == ''){

                    $sqlr_det_rev = "SELECT valor FROM ccpetrp_detalle WHERE vigencia = '".$row['vigencia']."' AND consvigencia= '".$row['consvigencia']."' AND cuenta = '".$row_det['cuenta']."' AND productoservicio = '".$row_det['productoservicio']."' AND fuente = '".$row_det['fuente']."' AND tipo_mov LIKE '4%' AND medio_pago = '".$row_det['medio_pago']."' AND codigo_vigenciag = '".$row_det['codigo_vigenciag']."' AND bpim = '".$row_det['bpim']."' AND indicador_producto = '".$row_det['indicador_producto']."' AND seccion_presupuestal = '".$row_det['seccion_presupuestal']."'";
                    $res_det_rev = mysqli_query($linkbd, $sqlr_det_rev);
                    $row_det_rev = mysqli_fetch_row($res_det_rev);

                    $valorRpInd = round($row_det['valor'] - $row_det_rev[0], 2);

                    $rubro = '';

                    if($row_det['bpim'] != ''){
                        $rubro = $row_det['indicador_producto']."-".$row_det['bpim']."-".$row_det['fuente']."-".$row_det['seccion_presupuestal']."-".$row_det['codigo_vigenciag']."-".$row_det['medio_pago'];
                    }else{
                        $rubro = $row_det['cuenta'];
                    }

                    $saldoPorEjecu = 0;
                    $det = array();

                    array_push($det, $row['contrato']);
                    array_push($det, $row['consvigencia']);
                    array_push($det, $row['vigencia']);
                    array_push($det, $row['idcdp']);
                    array_push($det, "'".$row['detalle']."'");
                    array_push($det, $row['fecha']);
                    array_push($det, $row['tercero']);
                    array_push($det, buscatercero($row['tercero']));
                    array_push($det, $row_det['cuenta']);
                    array_push($det, buscacuentaccpetgastos($row_det['cuenta'], $maxVersion));
                    array_push($det, $row_det['fuente']);
                    array_push($det, buscafuenteccpet($row_det['fuente']));
                    array_push($det, $row_det['productoservicio']);
                    array_push($det, $row_det['medio_pago']);
                    array_push($det, $row_det['codigo_vigenciag']);
                    array_push($det, $row_det['bpim']);
                    array_push($det, $row_det['indicador_producto']);
                    array_push($det, $row_det['seccion_presupuestal']);
                    array_push($det, $valorRpInd);
                    array_push($det, $rubro);

                }else{
                    $valorRpInd = $row_det['valor'] - $row_rev_tot['valor'];
                    $valorRp += $valorRpInd;
                    $row_rev_tot['valor'] = 0;
                }

                $valorCxp_det = 0;

                $sqlr_cxp = "SELECT SUM(valor) FROM tesoordenpago AS TB1, tesoordenpago_det AS TB2 WHERE TB1.id_orden = TB2.id_orden AND TB1.vigencia = '".$row['vigencia']."' AND TB1.id_rp = '".$row['consvigencia']."' AND TB1.estado != 'R' AND TB2.cuentap = '".$row_det['cuenta']."' AND TB2.tipo_mov = '201' AND TB2.fuente = '".$row_det['fuente']."' AND TB2.productoservicio = '".$row_det['productoservicio']."' AND TB2.indicador_producto = '".$row_det['indicador_producto']."' AND TB2.medio_pago = '".$row_det['medio_pago']."' AND TB2.codigo_vigenciag = '".$row_det['codigo_vigenciag']."' AND TB2.bpim = '".$row_det['bpim']."' AND TB2.seccion_presupuestal = '".$row_det['seccion_presupuestal']."' GROUP BY TB1.id_rp";

                $res_cxp = mysqli_query($linkbd, $sqlr_cxp);
                $row_cxp = mysqli_fetch_row($res_cxp);
                if($_POST['check'] == ''){

                    $valorCxp_det += $row_cxp[0];

                    //array_push($det, $row_cxp[0]);
                    $saldoPorEjecu = $valorRpInd - $row_cxp[0];
                    //array_push($det, $saldoPorEjecu);

                    /* array_push($contratos, $det); */
                }else{

                    $valorCxp += $row_cxp[0];
                    /* $saldoPorEjecutar = 0;
                    $saldoPorEjecutar = $row_det['valor'] - $row_cxp[0];
                    $saldoPorEjecu += $saldoPorEjecutar; */

                }

                $sqlr_cxp_nom = "SELECT SUM(TB2.valor) FROM hum_nom_cdp_rp AS TB1, humnom_presupuestal AS TB2 WHERE TB1.rp = '".$row['consvigencia']."' AND TB1.vigencia = '".$row['vigencia']."' AND TB1.nomina = TB2.id_nom AND TB2.estado = 'P' AND TB2.cuenta = '".$row_det['cuenta']."' AND TB2.fuente = '".$row_det['fuente']."' AND TB2.producto = '".$row_det['productoservicio']."' AND TB2.indicador = '".$row_det['indicador_producto']."' AND TB2.medio_pago = '".$row_det['medio_pago']."' AND TB2.vigencia_gasto = '".$row_det['codigo_vigenciag']."' AND TB2.bpin = '".$row_det['bpim']."' AND TB2.seccion_presupuestal = '".$row_det['seccion_presupuestal']."' GROUP BY TB1.rp";

                $res_cxp_nom = mysqli_query($linkbd, $sqlr_cxp_nom);
                $row_cxp_nom = mysqli_fetch_row($res_cxp_nom);
                if($_POST['check'] == ''){
                    $valorCxp_det += $row_cxp_nom[0];
                    array_push($det, $valorCxp_det);
                    $saldoPorEjecu -= $row_cxp_nom[0];
                    array_push($det, $saldoPorEjecu);

                    array_push($contratos, $det);
                }else{

                    $valorCxp += $row_cxp_nom[0];
                    /* $saldoPorEjecutar = 0;
                    $saldoPorEjecutar = $row_det['valor'] - $row_cxp_nom[0];
                    $saldoPorEjecu += $saldoPorEjecutar; */

                }


            }
            if($_POST['check'] != ''){
                $det = array();
                array_push($det, $row['contrato']);
                array_push($det, $row['consvigencia']);
                array_push($det, $row['vigencia']);
                array_push($det, $row['idcdp']);
                array_push($det, $row['detalle']);
                array_push($det, $row['fecha']);
                array_push($det, $row['tercero']);
                array_push($det, buscatercero($row['tercero']));
                array_push($det, $valorRp);
                array_push($det, $valorCxp);
                $saldoPorEjecu = 0;
                $saldoPorEjecu = $valorRp - $valorCxp;
                array_push($det, $saldoPorEjecu);

                array_push($contratos, $det);
            }

            /* array_push($contratos, $row); */
        }
        $out['contratos'] = $contratos;
    }


    header("Content-type: application/json");
    echo json_encode($out);
    die();
