<?php
    require_once '../../../Librerias/core/Helpers.php';
    require_once '../../../Librerias/core/Mysql.php';
    session_start();
    /*ini_set('display_errors', '1');
    ini_set('display_startup_errors', '1');
    error_reporting(E_ALL);*/
    class ReporteModel extends Mysql{
        private $strFechaInicial;
        private $strFechaFinal;
        private $strGasto;
        function __construct(){
            parent::__construct();
        }

        public function selectInicial($strFechaInicial,$strFechaFinal,$strGasto,$arrFuentes){
            $this->strFechaInicial = $strFechaInicial;
            $this->strFechaFinal = $strFechaFinal;
            $this->strGasto = $strGasto;
            $arrData = [];
            $total = count($arrFuentes);
            $arrBases = getBases();
            foreach($arrBases as $base){
                $con = connectBase($base['base'], $base['usuario']);
                for ($i=0; $i < $total ; $i++) {
                    $partsFuente = explode("_", $arrFuentes[$i]['codigo']);
                    $fuente = $partsFuente[0];
                    $sqlGastos = "SELECT COALESCE(SUM(pp.valorcsf)+SUM(pp.valorssf),0)  as total,
                    pp.vigencia_gasto as vigencia
                    FROM ccpproyectospresupuesto p
                    INNER JOIN ccpproyectospresupuesto_presupuesto pp ON p.id = pp.codproyecto
                    WHERE p.estado = 'S' AND p.vigencia BETWEEN YEAR('$this->strFechaInicial') AND YEAR('$this->strFechaFinal')
                    AND pp.vigencia_gasto like '$this->strGasto%' AND pp.id_fuente LIKE '$fuente%'";

                    $sqlGastos2 = "SELECT COALESCE(SUM(valor),0) as total,vigencia_gasto as vigencia,fuente
                    FROM ccpetinicialgastosfun
                    WHERE vigencia BETWEEN YEAR('$this->strFechaInicial') AND YEAR('$this->strFechaFinal')
                    AND vigencia_gasto like '$strGasto%' AND fuente LIKE '$fuente%'";

                    $sqlIngresos = "SELECT COALESCE(SUM(valor),0) as total,vigencia_gasto as vigencia,fuente
                    FROM ccpetinicialing
                    WHERE vigencia BETWEEN YEAR('$this->strFechaInicial') AND YEAR('$this->strFechaFinal')
                    AND vigencia_gasto like '$strGasto%' AND fuente LIKE '$fuente%'";

                    $arrGastos = $con->select_all($sqlGastos);
                    $arrGastos2 = $con->select_all($sqlGastos2);
                    $arrIngresos = $con->select_all($sqlIngresos);

                    $totalGastos = count($arrGastos);
                    $totalGastos2 = count($arrGastos2);
                    $totalIngresos = count($arrIngresos);

                    if($totalGastos > 0){
                        foreach ($arrGastos as $data) {
                            array_push($arrData,array(
                                "nombre"=>$arrFuentes[$i]['nombre'],
                                "codigo"=>$fuente,
                                "vigencia"=>$data['vigencia'],
                                "total"=>$data['total'],
                                "tipo"=>"egreso",
                                "dato"=>"inicial",
                                "llave"=>$fuente."-".$data['vigencia']
                            ));
                        }
                    }
                    if($totalGastos2 > 0){
                        foreach ($arrGastos2 as $data) {
                            array_push($arrData,array(
                                "nombre"=>$arrFuentes[$i]['nombre'],
                                "codigo"=>$fuente,
                                "vigencia"=>$data['vigencia'],
                                "total"=>$data['total'],
                                "tipo"=>"egreso",
                                "dato"=>"inicial",
                                "llave"=>$fuente."-".$data['vigencia']
                            ));
                        }
                    }
                    if($totalIngresos > 0){
                        foreach ($arrIngresos as $data) {
                            array_push($arrData,array(
                                "nombre"=>$arrFuentes[$i]['nombre'],
                                "codigo"=>$fuente,
                                "vigencia"=>$data['vigencia'],
                                "total"=>$data['total'],
                                "tipo"=>"ingreso",
                                "dato"=>"inicial",
                                "llave"=>$fuente."-".$data['vigencia']
                            ));
                        }
                    }
                }
            }
            return $arrData;
        }
        public function selectAdiciones($strFechaInicial,$strFechaFinal,$strGasto,$arrFuentes,$arrData){
            $this->strFechaInicial = $strFechaInicial;
            $this->strFechaFinal = $strFechaFinal;
            $this->strGasto = $strGasto;
            $total = count($arrFuentes);
            $arrBases = getBases();
            foreach($arrBases as $base){
                $con = connectBase($base['base'], $base['usuario']);
                for ($i=0; $i < $total ; $i++) {
                    $partsFuente = explode("_", $arrFuentes[$i]['codigo']);
                    $fuente = $partsFuente[0];

                    $sqlGastos = "SELECT COALESCE(SUM(valor),0) as total,COALESCE(codigo_vigenciag,'') as vigencia
                    FROM ccpet_adiciones
                    WHERE estado='S'  AND fecha BETWEEN '$this->strFechaInicial' AND '$this->strFechaFinal'
                    AND codigo_vigenciag like '$this->strGasto%' AND fuente LIKE '$fuente%' AND tipo_cuenta='I' ";

                    $sqlIngresos = "SELECT COALESCE(SUM(valor),0) as total,COALESCE(codigo_vigenciag,'') as vigencia
                    FROM ccpet_adiciones
                    WHERE estado='S'  AND fecha BETWEEN '$this->strFechaInicial' AND '$this->strFechaFinal'
                    AND codigo_vigenciag like '$this->strGasto%' AND fuente LIKE '$fuente%' AND tipo_cuenta='G' ";

                    $arrIngresos = $con->select_all($sqlIngresos);
                    $arrGastos = $con->select_all($sqlGastos);

                    $totalGastos = count($arrGastos);
                    $totalIngresos = count($arrIngresos);

                    if($totalGastos > 0){
                        foreach ($arrGastos as $data) {
                            array_push($arrData,array(
                                "nombre"=>$arrFuentes[$i]['nombre'],
                                "codigo"=>$fuente,
                                "vigencia"=>$data['vigencia'],
                                "total"=>$data['total'],
                                "tipo"=>"egreso",
                                "dato"=>"adicion",
                                "llave"=>$fuente."-".$data['vigencia']
                            ));
                        }
                    }
                    if($totalIngresos > 0){
                        foreach ($arrIngresos as $data) {
                            array_push($arrData,array(
                                "nombre"=>$arrFuentes[$i]['nombre'],
                                "codigo"=>$fuente,
                                "vigencia"=>$data['vigencia'],
                                "total"=>$data['total'],
                                "tipo"=>"ingreso",
                                "dato"=>"adicion",
                                "llave"=>$fuente."-".$data['vigencia']
                            ));
                        }
                    }
                }
            }
            return $arrData;
        }
        public function selectReducciones($strFechaInicial,$strFechaFinal,$strGasto,$arrFuentes,$arrData){
            $this->strFechaInicial = $strFechaInicial;
            $this->strFechaFinal = $strFechaFinal;
            $this->strGasto = $strGasto;
            $total = count($arrFuentes);
            $arrBases = getBases();
            foreach($arrBases as $base){
                $con = connectBase($base['base'], $base['usuario']);
                for ($i=0; $i < $total ; $i++) {
                    $partsFuente = explode("_", $arrFuentes[$i]['codigo']);
                    $fuente = $partsFuente[0];

                    $sqlGastos = "SELECT COALESCE(SUM(valor),0) as total,COALESCE(codigo_vigenciag,'') as vigencia
                    FROM ccpet_reducciones
                    WHERE estado='S'  AND fecha BETWEEN '$this->strFechaInicial' AND '$this->strFechaFinal'
                    AND codigo_vigenciag like '$this->strGasto%' AND fuente LIKE '$fuente%' AND tipo_cuenta='I' ";

                    $sqlIngresos = "SELECT COALESCE(SUM(valor),0) as total,COALESCE(codigo_vigenciag,'') as vigencia
                    FROM ccpet_reducciones
                    WHERE estado='S'  AND fecha BETWEEN '$this->strFechaInicial' AND '$this->strFechaFinal'
                    AND codigo_vigenciag like '$this->strGasto%' AND fuente LIKE '$fuente%' AND tipo_cuenta='G' ";

                    $arrIngresos = $con->select_all($sqlIngresos);
                    $arrGastos = $con->select_all($sqlGastos);

                    $totalGastos = count($arrGastos);
                    $totalIngresos = count($arrIngresos);

                    if($totalGastos > 0){
                        foreach ($arrGastos as $data) {
                            array_push($arrData,array(
                                "nombre"=>$arrFuentes[$i]['nombre'],
                                "codigo"=>$fuente,
                                "vigencia"=>$data['vigencia'],
                                "total"=>$data['total'],
                                "tipo"=>"egreso",
                                "dato"=>"reduccion",
                                "llave"=>$fuente."-".$data['vigencia']
                            ));
                        }
                    }
                    if($totalIngresos > 0){
                        foreach ($arrIngresos as $data) {
                            array_push($arrData,array(
                                "nombre"=>$arrFuentes[$i]['nombre'],
                                "codigo"=>$fuente,
                                "vigencia"=>$data['vigencia'],
                                "total"=>$data['total'],
                                "tipo"=>"ingreso",
                                "dato"=>"reduccion",
                                "llave"=>$fuente."-".$data['vigencia']
                            ));
                        }
                    }
                }
            }
            return $arrData;
        }
        public function selectTraslados($strFechaInicial,$strFechaFinal,$strGasto,$arrFuentes,$arrData){
            $this->strFechaInicial = $strFechaInicial;
            $this->strFechaFinal = $strFechaFinal;
            $this->strGasto = $strGasto;
            $total = count($arrFuentes);
            $arrBases = getBases();
            foreach($arrBases as $base){
                $con = connectBase($base['base'], $base['usuario']);
                for ($i=0; $i < $total ; $i++) {
                    $partsFuente = explode("_", $arrFuentes[$i]['codigo']);
                    $fuente = $partsFuente[0];

                    $sqlCredito = "SELECT COALESCE(SUM(valor),0) as total,COALESCE(codigo_vigenciag,'') as vigencia
                    FROM ccpet_traslados
                    WHERE estado='S'  AND tipo = 'C' AND fecha BETWEEN '$this->strFechaInicial' AND '$this->strFechaFinal'
                    AND codigo_vigenciag like '$this->strGasto%' AND fuente LIKE '$fuente%'";

                    $sqlContracredito = "SELECT COALESCE(SUM(valor),0) as total,COALESCE(codigo_vigenciag,'') as vigencia
                    FROM ccpet_traslados
                    WHERE estado='S'  AND tipo = 'R' AND fecha BETWEEN '$this->strFechaInicial' AND '$this->strFechaFinal'
                    AND codigo_vigenciag like '$this->strGasto%' AND fuente LIKE '$fuente%'";

                    $arrCredito = $con->select_all($sqlCredito);
                    $arrContracredito = $con->select_all($sqlContracredito);

                    $totalCredito = count($arrCredito);
                    $totalContracredito = count($arrContracredito);

                    if($totalCredito > 0){
                        foreach ($arrCredito as $data) {
                            array_push($arrData,array(
                                "nombre"=>$arrFuentes[$i]['nombre'],
                                "codigo"=>$fuente,
                                "vigencia"=>$data['vigencia'],
                                "total"=>$data['total'],
                                "tipo"=>"credito",
                                "dato"=>"traslado",
                                "llave"=>$fuente."-".$data['vigencia']
                            ));
                        }
                    }
                    if($totalContracredito > 0){
                        foreach ($arrContracredito as $data) {
                            array_push($arrData,array(
                                "nombre"=>$arrFuentes[$i]['nombre'],
                                "codigo"=>$fuente,
                                "vigencia"=>$data['vigencia'],
                                "total"=>$data['total'],
                                "tipo"=>"contracredito",
                                "dato"=>"traslado",
                                "llave"=>$fuente."-".$data['vigencia']
                            ));
                        }
                    }
                }
            }
            return $arrData;
        }
        public function selectRecaudos($strFechaInicial,$strFechaFinal,$strGasto,$arrFuentes,$arrData){
            $this->strFechaInicial = $strFechaInicial;
            $this->strFechaFinal = $strFechaFinal;
            $this->strGasto = $strGasto;
            $total = count($arrFuentes);
            $arrBases = getBases();
            foreach($arrBases as $base){
                $con = connectBase($base['base'], $base['usuario']);
                for ($i=0; $i < $total ; $i++) {
                    $partsFuente = explode("_", $arrFuentes[$i]['codigo']);
                    $fuente = $partsFuente[0];

                    $sqlRecibo = "SELECT COALESCE(SUM(det.valor),0) as total,COALESCE(det.vigencia_gasto,'') as vigencia
                    FROM tesoreciboscaja cab
                    INNER JOIN pptorecibocajappto det ON cab.id_recibos = det.idrecibo
                    WHERE NOT(cab.estado='N' OR cab.estado='R') AND cab.fecha BETWEEN '$this->strFechaInicial' AND '$this->strFechaFinal'
                    AND det.vigencia_gasto like '$this->strGasto%' AND det.fuente LIKE '$fuente%' ";

                    $sqlSinRecibo = "SELECT COALESCE(SUM(det.valor),0) as total,COALESCE(det.vigencia_gasto,'') as vigencia
                    FROM tesosinreciboscaja cab
                    INNER JOIN pptosinrecibocajappto det ON cab.id_recibos = det.idrecibo
                    WHERE NOT(cab.estado='N' OR cab.estado='R')  AND cab.fecha BETWEEN '$this->strFechaInicial' AND '$this->strFechaFinal'
                    AND det.vigencia_gasto like '$this->strGasto%' AND det.fuente LIKE '$fuente%' ";

                    $sqlNotasBancarias = "SELECT COALESCE(SUM(det.valor),0) as total,COALESCE(det.vigencia_gasto,'') as vigencia
                    FROM tesonotasbancarias_cab cab
                    INNER JOIN pptonotasbanppto det ON cab.id_comp=det.idrecibo
                    WHERE NOT(cab.estado='N' OR cab.estado='R')  AND cab.fecha BETWEEN '$this->strFechaInicial' AND '$this->strFechaFinal'
                    AND det.vigencia_gasto like '$this->strGasto%' AND det.fuente LIKE '$fuente%' ";

                    $sqlRecaudoTransferencia = "SELECT COALESCE(SUM(det.valor),0) as total,COALESCE(det.vigencia_gasto,'') as vigencia
                    FROM tesorecaudotransferencia cab
                    INNER JOIN pptoingtranppto det ON det.idrecibo=cab.id_recaudo
                    WHERE NOT(cab.estado='N' OR cab.estado='R')  AND cab.fecha BETWEEN '$this->strFechaInicial' AND '$this->strFechaFinal'
                    AND det.vigencia_gasto like '$this->strGasto%' AND det.fuente LIKE '$fuente%' ";

                    $sqlRetencionEgresos = "SELECT COALESCE(SUM(det.valor),0) as total,COALESCE(det.vigencia_gasto,'') as vigencia
                    FROM tesoegresos cab
                    INNER JOIN pptoretencionpago det ON cab.id_egreso=det.idrecibo
                    WHERE cab.estado!='N' AND cab.fecha BETWEEN '$this->strFechaInicial' AND '$this->strFechaFinal'
                    AND det.vigencia_gasto like '$this->strGasto%' AND cab.tipo_mov='201'
                    AND det.tipo='egreso' AND NOT EXISTS (SELECT 1 FROM tesoegresos tra WHERE tra.id_egreso=cab.id_egreso  AND tra.tipo_mov='401')
                    AND det.fuente LIKE '$fuente%' ";

                    $sqlSuperavit = "SELECT COALESCE(SUM(det.valor),0) as total,COALESCE(det.vigencia_gasto,'') as vigencia
                    FROM tesosuperavit cab
                    INNER JOIN tesosuperavit_det det ON cab.id=det.id_tesosuperavit
                    WHERE cab.estado ='S' AND cab.fecha BETWEEN '$this->strFechaInicial' AND '$this->strFechaFinal'
                    AND det.vigencia_gasto like '$this->strGasto%' AND det.fuente LIKE '$fuente%' ";

                    $sqlRetencionOtrosEgresos = "SELECT COALESCE(SUM(det.valor),0) as total,COALESCE(det.vigencia_gasto,'') as vigencia
                    FROM tesopagotercerosvigant cab
                    INNER JOIN pptoretencionotrosegresos det ON cab.id_pago=det.idrecibo
                    WHERE cab.estado !='N' AND det.tipo='egreso' AND cab.fecha BETWEEN '$this->strFechaInicial' AND '$this->strFechaFinal'
                    AND det.vigencia_gasto like '$this->strGasto%' AND det.fuente LIKE '$fuente%' ";

                    $sqlRetencionPago = "SELECT COALESCE(SUM(det.valor),0) as total,COALESCE(det.vigencia_gasto,'') as vigencia
                    FROM tesoordenpago cab
                    INNER JOIN pptoretencionpago det ON cab.id_orden=det.idrecibo
                    WHERE cab.estado !='N' AND det.tipo='orden' AND cab.fecha BETWEEN '$this->strFechaInicial' AND '$this->strFechaFinal'
                    AND det.vigencia_gasto like '$this->strGasto%' AND det.fuente LIKE '$fuente%' AND cab.tipo_mov='201'
                    AND NOT EXISTS (SELECT 1 FROM tesoordenpago tca WHERE tca.id_orden=cab.id_orden  AND tca.tipo_mov='401') ";

                    $sqlRecaudoFactura = "SELECT COALESCE(SUM(det.valor),0) as total,COALESCE(det.vigencia_gasto,'') as vigencia
                    FROM srv_recaudo_factura cab
                    INNER JOIN srv_recaudo_factura_ppto det ON cab.codigo_recaudo=det.codigo_recaudo
                    WHERE cab.estado !='REVERSADO' AND cab.fecha_recaudo BETWEEN '$this->strFechaInicial' AND '$this->strFechaFinal'
                    AND det.vigencia_gasto like '$this->strGasto%' AND det.fuente LIKE '$fuente%' ";

                    $sqlRecaudoFacturaAcuerdo = "SELECT COALESCE(SUM(det.valor),0) as total,COALESCE(det.vigencia_gasto,'') as vigencia
                    FROM srv_acuerdo_cab cab
                    INNER JOIN srv_acuerdo_ppto det ON cab.codigo_acuerdo=det.codigo_acuerdo
                    WHERE cab.estado_acuerdo !='Reversado' AND cab.fecha_acuerdo BETWEEN '$this->strFechaInicial' AND '$this->strFechaFinal'
                    AND det.vigencia_gasto like '$this->strGasto%' AND det.fuente LIKE '$fuente%' ";

                    $sqlReservas = "SELECT COALESCE(SUM(det.valor),0) as total,COALESCE(det.vigencia_gasto,'') as vigencia
                    FROM tesoreservas cab
                    INNER JOIN tesoreservas_det det ON cab.id=det.id_reserva
                    WHERE cab.estado ='S' AND cab.fecha BETWEEN '$this->strFechaInicial' AND '$this->strFechaFinal'
                    AND det.vigencia_gasto like '$this->strGasto%' AND det.fuente LIKE '$fuente%' ";


                    $arrRecibo = $con->select_all($sqlRecibo);
                    $arrSinRecibo = $con->select_all($sqlSinRecibo);
                    $arrNotasBancarias = $con->select_all($sqlNotasBancarias);
                    $arrRecaudoTransferencia = $con->select_all($sqlRecaudoTransferencia);
                    $arrRetencionEgresos = $con->select_all($sqlRetencionEgresos);
                    $arrSuperavit = $con->select_all($sqlSuperavit);
                    $arrRetencionOtrosEgresos = $con->select_all($sqlRetencionOtrosEgresos);
                    $arrRetencionPago = $con->select_all($sqlRetencionPago);
                    $arrRecaudoFactura = $con->select_all($sqlRecaudoFactura);
                    $arrRecaudoFacturaAcuerdo = $con->select_all($sqlRecaudoFacturaAcuerdo);
                    $arrReservas = $con->select_all($sqlReservas);

                    $totalRecibo = count($arrRecibo);
                    $totalSinRecibo = count($arrSinRecibo);
                    $totalNotas = count($arrNotasBancarias);
                    $totalTransferencia = count($arrRecaudoTransferencia);
                    $totalRetencion = count($arrRetencionEgresos);
                    $totalSuperavit = count($arrSuperavit);
                    $totalRetencionOtros = count($arrRetencionOtrosEgresos);
                    $totalRetencionPago = count($arrRetencionPago);
                    $totalFactura = count($arrRecaudoFactura);
                    $totalFacturaAcuerdo = count($arrRecaudoFacturaAcuerdo);
                    $totalReservas = count($arrReservas);

                    if($totalRecibo > 0){
                        foreach ($arrRecibo as $data) {
                            array_push($arrData,array(
                                "nombre"=>$arrFuentes[$i]['nombre'],
                                "codigo"=>$fuente,
                                "vigencia"=>$data['vigencia'],
                                "total"=>$data['total'],
                                "tipo"=>"ingreso",
                                "dato"=>"recaudo",
                                "llave"=>$fuente."-".$data['vigencia']
                            ));
                        }
                    }
                    if($totalSinRecibo > 0){
                        foreach ($arrSinRecibo as $data) {
                            array_push($arrData,array(
                                "nombre"=>$arrFuentes[$i]['nombre'],
                                "codigo"=>$fuente,
                                "vigencia"=>$data['vigencia'],
                                "total"=>$data['total'],
                                "tipo"=>"ingreso",
                                "dato"=>"recaudo",
                                "llave"=>$fuente."-".$data['vigencia']
                            ));
                        }
                    }
                    if($totalNotas > 0){
                        foreach ($arrNotasBancarias as $data) {
                            array_push($arrData,array(
                                "nombre"=>$arrFuentes[$i]['nombre'],
                                "codigo"=>$fuente,
                                "vigencia"=>$data['vigencia'],
                                "total"=>$data['total'],
                                "tipo"=>"ingreso",
                                "dato"=>"recaudo",
                                "llave"=>$fuente."-".$data['vigencia']
                            ));
                        }
                    }
                    if($totalTransferencia > 0){
                        foreach ($arrRecaudoTransferencia as $data) {
                            array_push($arrData,array(
                                "nombre"=>$arrFuentes[$i]['nombre'],
                                "codigo"=>$fuente,
                                "vigencia"=>$data['vigencia'],
                                "total"=>$data['total'],
                                "tipo"=>"ingreso",
                                "dato"=>"recaudo",
                                "llave"=>$fuente."-".$data['vigencia']
                            ));
                        }
                    }
                    if($totalRetencion > 0){
                        foreach ($arrRetencionEgresos as $data) {
                            array_push($arrData,array(
                                "nombre"=>$arrFuentes[$i]['nombre'],
                                "codigo"=>$fuente,
                                "vigencia"=>$data['vigencia'],
                                "total"=>$data['total'],
                                "tipo"=>"ingreso",
                                "dato"=>"recaudo",
                                "llave"=>$fuente."-".$data['vigencia']
                            ));
                        }
                    }
                    if($totalSuperavit > 0){
                        foreach ($arrSuperavit as $data) {
                            array_push($arrData,array(
                                "nombre"=>$arrFuentes[$i]['nombre'],
                                "codigo"=>$fuente,
                                "vigencia"=>$data['vigencia'],
                                "total"=>$data['total'],
                                "tipo"=>"ingreso",
                                "dato"=>"recaudo",
                                "llave"=>$fuente."-".$data['vigencia']
                            ));
                        }
                    }
                    if($totalRetencionOtros > 0){
                        foreach ($arrRetencionOtrosEgresos as $data) {
                            array_push($arrData,array(
                                "nombre"=>$arrFuentes[$i]['nombre'],
                                "codigo"=>$fuente,
                                "vigencia"=>$data['vigencia'],
                                "total"=>$data['total'],
                                "tipo"=>"ingreso",
                                "dato"=>"recaudo",
                                "llave"=>$fuente."-".$data['vigencia']
                            ));
                        }
                    }
                    if($totalRetencionPago > 0){
                        foreach ($arrRetencionPago as $data) {
                            array_push($arrData,array(
                                "nombre"=>$arrFuentes[$i]['nombre'],
                                "codigo"=>$fuente,
                                "vigencia"=>$data['vigencia'],
                                "total"=>$data['total'],
                                "tipo"=>"ingreso",
                                "dato"=>"recaudo",
                                "llave"=>$fuente."-".$data['vigencia']
                            ));
                        }
                    }
                    if($totalFactura > 0){
                        foreach ($arrRecaudoFactura as $data) {
                            array_push($arrData,array(
                                "nombre"=>$arrFuentes[$i]['nombre'],
                                "codigo"=>$fuente,
                                "vigencia"=>$data['vigencia'],
                                "total"=>$data['total'],
                                "tipo"=>"ingreso",
                                "dato"=>"recaudo",
                                "llave"=>$fuente."-".$data['vigencia']
                            ));
                        }
                    }
                    if($totalFacturaAcuerdo > 0){
                        foreach ($arrRecaudoFacturaAcuerdo as $data) {
                            array_push($arrData,array(
                                "nombre"=>$arrFuentes[$i]['nombre'],
                                "codigo"=>$fuente,
                                "vigencia"=>$data['vigencia'],
                                "total"=>$data['total'],
                                "tipo"=>"ingreso",
                                "dato"=>"recaudo",
                                "llave"=>$fuente."-".$data['vigencia']
                            ));
                        }
                    }
                    if($totalReservas > 0){
                        foreach ($arrReservas as $data) {
                            array_push($arrData,array(
                                "nombre"=>$arrFuentes[$i]['nombre'],
                                "codigo"=>$fuente,
                                "vigencia"=>$data['vigencia'],
                                "total"=>$data['total'],
                                "tipo"=>"ingreso",
                                "dato"=>"recaudo",
                                "llave"=>$fuente."-".$data['vigencia']
                            ));
                        }
                    }
                }
            }
            return $arrData;
        }
        public function selectPagos($strFechaInicial,$strFechaFinal,$strGasto,$arrFuentes,$arrData){
            $this->strFechaInicial = $strFechaInicial;
            $this->strFechaFinal = $strFechaFinal;
            $this->strGasto = $strGasto;
            $total = count($arrFuentes);
            $arrBases = getBases();
            foreach($arrBases as $base){
                $con = connectBase($base['base'], $base['usuario']);
                for ($i=0; $i < $total ; $i++) {
                    $partsFuente = explode("_", $arrFuentes[$i]['codigo']);
                    $fuente = $partsFuente[0];

                    $sqlEgresos = "SELECT COALESCE(SUM(det.valor),0) as total,COALESCE(det.codigo_vigenciag,'') as vigencia
                    FROM tesoegresos cab
                    INNER JOIN  tesoordenpago_det det ON cab.id_orden =det.id_orden
                    WHERE det.tipo_mov='201' AND cab.fecha BETWEEN '$this->strFechaInicial' AND '$this->strFechaFinal' AND cab.tipo_mov = '201'
                    AND det.codigo_vigenciag like '$this->strGasto%' AND det.fuente LIKE '$fuente%' ";

                    $sqlEgresos2 = "SELECT COALESCE(SUM(det.valor),0) as total,COALESCE(det.codigo_vigenciag,'') as vigencia
                    FROM tesoegresos cab
                    INNER JOIN  tesoordenpago_det det ON cab.id_orden =det.id_orden
                    WHERE det.tipo_mov='401' AND cab.fecha BETWEEN '$this->strFechaInicial' AND '$this->strFechaFinal' AND cab.tipo_mov = '401'
                    AND det.codigo_vigenciag like '$this->strGasto%' AND det.fuente LIKE '$fuente%' ";

                    $sqlEgresoNomina = "SELECT COALESCE(SUM(det.valordevengado),0) as total,COALESCE(det.vigencia_gasto,'') as vigencia
                    FROM tesoegresosnomina cab
                    INNER JOIN  tesoegresosnomina_det det ON cab.id_egreso =det.id_egreso
                    WHERE det.tipo_mov='201' AND cab.tipo_mov = '201'
                    AND NOT(det.tipo='SE' OR det.tipo='PE' OR det.tipo='DS' OR det.tipo='RE' OR det.tipo='FS')
                    AND det.vigencia_gasto like '$this->strGasto%' AND
                    cab.fecha BETWEEN '$this->strFechaInicial' AND '$this->strFechaFinal' AND det.fuente LIKE '$fuente%' ";

                    $sqlEgresoNomina2 = "SELECT COALESCE(SUM(det.valordevengado),0) as total,COALESCE(det.vigencia_gasto,'') as vigencia
                    FROM tesoegresosnomina cab
                    INNER JOIN  tesoegresosnomina_det det ON cab.id_egreso =det.id_egreso
                    WHERE det.tipo_mov='401' AND cab.tipo_mov = '401'
                    AND NOT(det.tipo='SE' OR det.tipo='PE' OR det.tipo='DS' OR det.tipo='RE' OR det.tipo='FS')
                    AND det.vigencia_gasto like '$this->strGasto%' AND
                    cab.fecha BETWEEN '$this->strFechaInicial' AND '$this->strFechaFinal' AND det.fuente LIKE '$fuente%' ";

                    $arrEgresos = $con->select_all($sqlEgresos);
                    $arrEgresosNomina = $con->select_all($sqlEgresoNomina);
                    $arrEgresos2 = $con->select_all($sqlEgresos2);
                    $arrEgresosNomina2 = $con->select_all($sqlEgresoNomina2);

                    $totalEgresos = count($arrEgresos);
                    $totalEgresosNomina = count($arrEgresosNomina);

                    if($totalEgresos > 0){
                        foreach ($arrEgresos as $data) {
                            array_push($arrData,array(
                                "nombre"=>$arrFuentes[$i]['nombre'],
                                "codigo"=>$fuente,
                                "vigencia"=>$data['vigencia'],
                                "total"=>$data['total'],
                                "tipo"=>"egreso",
                                "movimiento"=>"201",
                                "dato"=>"pago",
                                "llave"=>$fuente."-".$data['vigencia']
                            ));
                        }
                        foreach ($arrEgresos2 as $data) {
                            array_push($arrData,array(
                                "nombre"=>$arrFuentes[$i]['nombre'],
                                "codigo"=>$fuente,
                                "vigencia"=>$data['vigencia'],
                                "total"=>$data['total'],
                                "tipo"=>"egreso",
                                "movimiento"=>"401",
                                "dato"=>"pago",
                                "llave"=>$fuente."-".$data['vigencia']
                            ));
                        }
                    }
                    if($totalEgresosNomina > 0){
                        foreach ($arrEgresosNomina as $data) {
                            array_push($arrData,array(
                                "nombre"=>$arrFuentes[$i]['nombre'],
                                "codigo"=>$fuente,
                                "vigencia"=>$data['vigencia'],
                                "total"=>$data['total'],
                                "tipo"=>"egreso",
                                "movimiento"=>"201",
                                "dato"=>"pago",
                                "llave"=>$fuente."-".$data['vigencia']
                            ));
                        }
                        foreach ($arrEgresosNomina2 as $data) {
                            array_push($arrData,array(
                                "nombre"=>$arrFuentes[$i]['nombre'],
                                "codigo"=>$fuente,
                                "vigencia"=>$data['vigencia'],
                                "total"=>$data['total'],
                                "tipo"=>"egreso",
                                "movimiento"=>"401",
                                "dato"=>"pago",
                                "llave"=>$fuente."-".$data['vigencia']
                            ));
                        }
                    }
                }
            }
            return $arrData;
        }
        public function selectFuentes(){
            $sql="SELECT codigo_fuente as codigo, nombre
            FROM ccpet_fuentes_cuipo
            WHERE version = '1' AND codigo_fuente NOT LIKE '%_%' ORDER BY codigo_fuente ASC";
            $request = $this->select_all($sql);
            if(!empty($request)){
                $total = count($request);
                for ($i=0; $i < $total; $i++) {
                    $request[$i]['is_checked'] = 1;
                }
            }
            return $request;
        }

        public function selectFuentes_1(){
            $sql="SELECT codigo_fuente as codigo, nombre
            FROM ccpet_fuentes_cuipo
            WHERE version = '1' ORDER BY codigo_fuente ASC";
            $request = $this->select_all($sql);
            if(!empty($request)){
                $total = count($request);
                for ($i=0; $i < $total; $i++) {
                    $request[$i]['is_checked'] = 1;
                }
            }
            return $request;
        }
    }
?>
