const URL ='presupuesto_ccpet/reporte_fuentes/controllers/reporte_controller.php';
const URLEXPORT = 'presupuesto_ccpet/reporte_fuentes/controllers/reporte_controller_export.php';

var app = new Vue({
    el:"#myapp",
    data() {
        return {
            isLoading:false,
            isModal:false,
            txtSearch:"",
            isCheckAll:1,
            txtResultados:0,
            arrFuentes:[],
            arrFuentesCopy:[],
            txtFechaInicial:new Date(new Date().getFullYear(),0,1).toISOString().split("T")[0],
            txtFechaFinal:new Date().toISOString().split("T")[0],
            selectGasto:"",
            arrData: []
        }
    },
    mounted() {
        this.getData();
    },
    methods: {

        getData: async function(){
            const formData = new FormData();
            formData.append("action","get");
            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();
            this.arrFuentes = objData.fuentes;
            this.arrFuentesCopy = objData.fuentes;
            this.txtResultados = this.arrFuentesCopy.length;

        },
        search:function(type=""){
            let search = "";
            if(type == "modal")search = this.txtSearch.toLowerCase();
            if(type=="modal"){
                this.arrFuentesCopy = [...this.arrFuentes.filter(e=>e.codigo.toLowerCase().includes(search) || e.nombre.toLowerCase().includes(search))];
                this.txtResultados = this.arrFuentesCopy.length;
            }
        },
        generate: async function(){
            const arrFuentes = this.arrFuentes.filter(e=>e.is_checked);
            const formData = new FormData();
            formData.append("action","gen");
            formData.append("fecha_inicial",this.txtFechaInicial);
            formData.append("fecha_final",this.txtFechaFinal);
            formData.append("gasto",this.selectGasto);
            formData.append("fuentes",JSON.stringify(arrFuentes));
            this.isLoading = true;
            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();
            this.isLoading = false;
            this.arrData = objData;
        },
        changeStatus:function(codigo){
            const index = this.arrFuentes.findIndex(e=>e.codigo == codigo);
            this.arrFuentes[index].is_checked = !this.arrFuentes[index].is_checked;
            const flag = this.arrFuentes.every((e)=>e.is_checked == true);
            this.isCheckAll = flag;
        },
        changeAll:function(){
            this.arrFuentes.forEach(e => {
                e.is_checked = this.isCheckAll;
            });
        },
        exportData:function(type){
            if(app.arrData.length == 0){
                Swal.fire("Atención!","Debe generar el reporte para generar el excel.","warning");
                return false;
            }
            const form = document.createElement("form");
            form.method ="post";
            form.target="_blank";
            form.action= URLEXPORT;

            function addField(name,value){
                const input = document.createElement("input");
                input.type="hidden";
                input.name=name;
                input.value = value;
                form.appendChild(input);
            }

            addField("fecha_inicial",this.txtFechaInicial);
            addField("fecha_final",this.txtFechaFinal);
            addField("action",type == 1 ? "pdf" : "excel");
            addField("data",JSON.stringify(this.arrData));
            document.body.appendChild(form);
            form.submit();
            document.body.removeChild(form);
        },
        formatNumero: function(valor){
            return new Intl.NumberFormat("en-US", {style: "currency", currency: "USD"}).format(valor);
        },
    },
})
