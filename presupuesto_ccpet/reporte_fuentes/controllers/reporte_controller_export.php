<?php
    require_once '../../../Librerias/core/Helpers.php';
    require_once '../../../PHPExcel/Classes/PHPExcel.php';
    require_once '../../../tcpdf/tcpdf_include.php';
    require_once '../../../tcpdf/tcpdf.php';
    session_start();
    /*ini_set('display_errors', '1');
    ini_set('display_startup_errors', '1');
    error_reporting(E_ALL);*/
    class ExportController {
        public function exportExcel(){
            if(!empty($_SESSION)){
                $arrData = json_decode($_POST['data'],true);
                if(!empty($arrData)){

                    $arrFechaInicial = explode("-",$_POST['fecha_inicial']);
                    $arrFechaFinal = explode("-",$_POST['fecha_final']);
                    $strFechaInicial = $arrFechaInicial[2]."/".$arrFechaInicial[1]."/".$arrFechaInicial[0];
                    $strFechaFinal = $arrFechaFinal[2]."/".$arrFechaFinal[1]."/".$arrFechaFinal[0];

                    $objPHPExcel = new PHPExcel();
                    $objPHPExcel->getActiveSheet()->getStyle('A:P')->applyFromArray(
                        array(
                            'alignment' => array(
                                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
                                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
                            )
                        )
                    );
                    $objPHPExcel->getProperties()
                    ->setCreator("IDEAL 10")
                    ->setLastModifiedBy("IDEAL 10")
                    ->setTitle("Exportar Excel con PHP")
                    ->setSubject("Documento de prueba")
                    ->setDescription("Documento generado con PHPExcel")
                    ->setKeywords("usuarios phpexcel")
                    ->setCategory("reportes");
                    //----Cuerpo de Documento----
                    $objPHPExcel->setActiveSheetIndex(0)
                    ->mergeCells('A1:P1')
                    ->mergeCells('A2:P2')
                    ->setCellValue('A1', 'PRESUPUESTO')
                    ->setCellValue('A2', 'REPORTE FUENTE DE RECURSOS');

                    $objPHPExcel-> getActiveSheet ()
                    -> getStyle ("A1")
                    -> getFill ()
                    -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
                    -> getStartColor ()
                    -> setRGB ('C8C8C8');
                    $objPHPExcel-> getActiveSheet ()
                    -> getStyle ("A1:A2")
                    -> getFont ()
                    -> setBold ( true )
                    -> setName ( 'Verdana' )
                    -> setSize ( 10 )
                    -> getColor ()
                    -> setRGB ('000000');
                    $objPHPExcel-> getActiveSheet ()
                    -> getStyle ('A1:A2')
                    -> getAlignment ()
                    -> applyFromArray (array ( 'horizontal'  =>  PHPExcel_Style_Alignment :: HORIZONTAL_CENTER ,) );
                    $objPHPExcel-> getActiveSheet ()
                    -> getStyle ('A3:P3')
                    -> getAlignment ()
                    -> applyFromArray (array ( 'horizontal'  =>  PHPExcel_Style_Alignment :: VERTICAL_JUSTIFY ,) );
                    $objPHPExcel-> getActiveSheet ()
                    -> getStyle ("A2")
                    -> getFill ()
                    -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID);

                    $borders = array(
                        'borders' => array(
                            'allborders' => array(
                                'style' => PHPExcel_Style_Border::BORDER_THIN,
                            'color' => array('argb' => 'FF000000'),
                            )
                        ),
                    );
                    $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('B3', $strFechaInicial)
                    ->setCellValue('B4', $strFechaFinal);

                    $objPHPExcel->setActiveSheetIndex(0)
                    ->mergeCells('B3:P3')
                    ->mergeCells('B4:P4')
                    ->setCellValue('A3', "Fecha inicial")
                    ->setCellValue('A4', "Fecha final");

                    $objPHPExcel->setActiveSheetIndex(0)
                    ->mergeCells('A5:C5')
                    ->mergeCells('D5:E5')
                    ->mergeCells('F5:G5')
                    ->mergeCells('H5:I5')
                    ->mergeCells('J5:K5')
                    ->mergeCells('L5:M5')
                    ->mergeCells('N5:P5')
                    ->setCellValue('D5', "Inicial")
                    ->setCellValue('F5', "Adición")
                    ->setCellValue('H5', "Reducción")
                    ->setCellValue('J5', "Traslados")
                    ->setCellValue('L5', "Definitivo");

                    $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A6', 'Vigencia del gasto')
                    ->setCellValue('B6', "Fuente")
                    ->setCellValue('C6', "Nombre fuente")
                    ->setCellValue('D6', "Ingresos")
                    ->setCellValue('E6', "Gastos")
                    ->setCellValue('F6', "Ingresos")
                    ->setCellValue('G6', "Gastos")
                    ->setCellValue('H6', "Ingresos")
                    ->setCellValue('I6', "Gastos")
                    ->setCellValue('J6', "Crédito")
                    ->setCellValue('K6', "Contracrédito")
                    ->setCellValue('L6', "Ingresos")
                    ->setCellValue('M6', "Gastos")
                    ->setCellValue('N6', "Recaudo")
                    ->setCellValue('O6', "Pago")
                    ->setCellValue('P6', "Diferencia (Recaudo - Pago)");
                    $objPHPExcel-> getActiveSheet ()
                        -> getStyle ("A3")
                        -> getFill ()
                        -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
                        -> getStartColor ()
                        -> setRGB ('3399cc');
                    $objPHPExcel-> getActiveSheet ()
                        -> getStyle ("A4")
                        -> getFill ()
                        -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
                        -> getStartColor ()
                        -> setRGB ('3399cc');
                    $objPHPExcel-> getActiveSheet ()
                        -> getStyle ("A5:P5")
                        -> getFill ()
                        -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
                        -> getStartColor ()
                        -> setRGB ('3399cc');
                    $objPHPExcel-> getActiveSheet ()
                    -> getStyle ("A6:P6")
                    -> getFill ()
                    -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
                    -> getStartColor ()
                    -> setRGB ('99ddff');
                    $objPHPExcel->getActiveSheet()->getStyle('A1:P1')->applyFromArray($borders);
                    $objPHPExcel->getActiveSheet()->getStyle('A2:P2')->applyFromArray($borders);
                    $objPHPExcel->getActiveSheet()->getStyle('A3:P3')->applyFromArray($borders);
                    $objPHPExcel->getActiveSheet()->getStyle('A4:P4')->applyFromArray($borders);
                    $objPHPExcel->getActiveSheet()->getStyle('A5:P5')->applyFromArray($borders);
                    $objPHPExcel->getActiveSheet()->getStyle('A6:P6')->applyFromArray($borders);

                    $objPHPExcel->getActiveSheet()->getStyle('A3')->getFont()->getColor()->setRGB("ffffff");
                    $objPHPExcel->getActiveSheet()->getStyle('A4')->getFont()->getColor()->setRGB("ffffff");
                    $objPHPExcel->getActiveSheet()->getStyle('A5:P5')->getFont()->getColor()->setRGB("ffffff");

                    $objPHPExcel->getActiveSheet()->getStyle("A3")->getFont()->setBold(true);
                    $objPHPExcel->getActiveSheet()->getStyle("A4")->getFont()->setBold(true);


                    $objPHPExcel->getActiveSheet()->getStyle("A5")->getFont()->setBold(true);
                    $objPHPExcel->getActiveSheet()->getStyle("B5")->getFont()->setBold(true);
                    $objPHPExcel->getActiveSheet()->getStyle("C5")->getFont()->setBold(true);
                    $objPHPExcel->getActiveSheet()->getStyle("D5")->getFont()->setBold(true);
                    $objPHPExcel->getActiveSheet()->getStyle("E5")->getFont()->setBold(true);
                    $objPHPExcel->getActiveSheet()->getStyle("F5")->getFont()->setBold(true);
                    $objPHPExcel->getActiveSheet()->getStyle("G5")->getFont()->setBold(true);
                    $objPHPExcel->getActiveSheet()->getStyle("H5")->getFont()->setBold(true);
                    $objPHPExcel->getActiveSheet()->getStyle("I5")->getFont()->setBold(true);
                    $objPHPExcel->getActiveSheet()->getStyle("J5")->getFont()->setBold(true);
                    $objPHPExcel->getActiveSheet()->getStyle("K5")->getFont()->setBold(true);
                    $objPHPExcel->getActiveSheet()->getStyle("L5")->getFont()->setBold(true);
                    $objPHPExcel->getActiveSheet()->getStyle("M5")->getFont()->setBold(true);
                    $objPHPExcel->getActiveSheet()->getStyle("N5")->getFont()->setBold(true);
                    $objPHPExcel->getActiveSheet()->getStyle("O5")->getFont()->setBold(true);
                    $objPHPExcel->getActiveSheet()->getStyle("P5")->getFont()->setBold(true);

                    $objPHPExcel->getActiveSheet()->getStyle("A6")->getFont()->setBold(true);
                    $objPHPExcel->getActiveSheet()->getStyle("B6")->getFont()->setBold(true);
                    $objPHPExcel->getActiveSheet()->getStyle("C6")->getFont()->setBold(true);
                    $objPHPExcel->getActiveSheet()->getStyle("D6")->getFont()->setBold(true);
                    $objPHPExcel->getActiveSheet()->getStyle("E6")->getFont()->setBold(true);
                    $objPHPExcel->getActiveSheet()->getStyle("F6")->getFont()->setBold(true);
                    $objPHPExcel->getActiveSheet()->getStyle("G6")->getFont()->setBold(true);
                    $objPHPExcel->getActiveSheet()->getStyle("H6")->getFont()->setBold(true);
                    $objPHPExcel->getActiveSheet()->getStyle("I6")->getFont()->setBold(true);
                    $objPHPExcel->getActiveSheet()->getStyle("J6")->getFont()->setBold(true);
                    $objPHPExcel->getActiveSheet()->getStyle("K6")->getFont()->setBold(true);
                    $objPHPExcel->getActiveSheet()->getStyle("L6")->getFont()->setBold(true);
                    $objPHPExcel->getActiveSheet()->getStyle("M6")->getFont()->setBold(true);
                    $objPHPExcel->getActiveSheet()->getStyle("N6")->getFont()->setBold(true);
                    $objPHPExcel->getActiveSheet()->getStyle("O6")->getFont()->setBold(true);
                    $objPHPExcel->getActiveSheet()->getStyle("P6")->getFont()->setBold(true);
                    $row = 7;
                    foreach ($arrData as $data) {
                        $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValueExplicit ("A$row", $data['vigencia'], PHPExcel_Cell_DataType :: TYPE_STRING)
                        ->setCellValueExplicit ("B$row", $data['codigo'], PHPExcel_Cell_DataType :: TYPE_STRING)
                        ->setCellValueExplicit ("C$row", $data['nombre'], PHPExcel_Cell_DataType :: TYPE_STRING)
                        ->setCellValueExplicit ("D$row", $data['inicial_ingresos'], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
                        ->setCellValueExplicit ("E$row", $data['inicial_gastos'], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
                        ->setCellValueExplicit ("F$row", $data['adiciones_ingresos'], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
                        ->setCellValueExplicit ("G$row", $data['adiciones_gastos'], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
                        ->setCellValueExplicit ("H$row", $data['reducciones_ingresos'], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
                        ->setCellValueExplicit ("I$row", $data['reducciones_gastos'], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
                        ->setCellValueExplicit ("J$row", $data['traslados_credito'], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
                        ->setCellValueExplicit ("K$row", $data['traslados_contracredito'], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
                        ->setCellValueExplicit ("L$row", $data['definitivo_ingresos'], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
                        ->setCellValueExplicit ("M$row", $data['definitivo_gastos'], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
                        ->setCellValueExplicit ("N$row", $data['recaudo'], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
                        ->setCellValueExplicit ("O$row", $data['pago'], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
                        ->setCellValueExplicit ("P$row", $data['diferencia'], PHPExcel_Cell_DataType :: TYPE_NUMERIC);
                        $objPHPExcel->getActiveSheet()->getStyle("A$row:P$row")->applyFromArray($borders);
                        if($data['inicial_estado']== 0){
                            $objPHPExcel-> getActiveSheet ()
                            -> getStyle ("D$row:E$row")
                            -> getFill ()
                            -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
                            -> getStartColor ()
                            -> setRGB ('FF0000');
                            $objPHPExcel->getActiveSheet()->getStyle("D$row:E$row")->getFont()->getColor()->setRGB("ffffff");
                        }
                        if($data['adiciones_estado']== 0){
                            $objPHPExcel-> getActiveSheet ()
                            -> getStyle ("F$row:G$row")
                            -> getFill ()
                            -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
                            -> getStartColor ()
                            -> setRGB ('FF0000');
                            $objPHPExcel->getActiveSheet()->getStyle("F$row:G$row")->getFont()->getColor()->setRGB("ffffff");
                        }
                        if($data['reducciones_estado']== 0){
                            $objPHPExcel-> getActiveSheet ()
                            -> getStyle ("H$row:I$row")
                            -> getFill ()
                            -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
                            -> getStartColor ()
                            -> setRGB ('FF0000');
                            $objPHPExcel->getActiveSheet()->getStyle("H$row:I$row")->getFont()->getColor()->setRGB("ffffff");
                        }
                        if($data['traslados_estado']== 0){
                            $objPHPExcel-> getActiveSheet ()
                            -> getStyle ("J$row:K$row")
                            -> getFill ()
                            -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
                            -> getStartColor ()
                            -> setRGB ('FF0000');
                            $objPHPExcel->getActiveSheet()->getStyle("J$row:k$row")->getFont()->getColor()->setRGB("ffffff");
                        }
                        if($data['definitivo_estado']== 0){
                            $objPHPExcel-> getActiveSheet ()
                            -> getStyle ("L$row:M$row")
                            -> getFill ()
                            -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
                            -> getStartColor ()
                            -> setRGB ('FF0000');
                            $objPHPExcel->getActiveSheet()->getStyle("L$row:M$row")->getFont()->getColor()->setRGB("ffffff");
                        }
                        if($data['diferencia_estado']== 0){
                            $objPHPExcel-> getActiveSheet ()
                            -> getStyle ("P$row")
                            -> getFill ()
                            -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
                            -> getStartColor ()
                            -> setRGB ('FF0000');
                            $objPHPExcel->getActiveSheet()->getStyle("P$row")->getFont()->getColor()->setRGB("ffffff");
                        }
                        $row++;
                    }
                    $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
                    $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
                    $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
                    $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
                    $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
                    $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
                    $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
                    $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
                    $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
                    $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);
                    $objPHPExcel->getActiveSheet()->getColumnDimension('K')->setAutoSize(true);
                    $objPHPExcel->getActiveSheet()->getColumnDimension('L')->setAutoSize(true);
                    $objPHPExcel->getActiveSheet()->getColumnDimension('M')->setAutoSize(true);
                    $objPHPExcel->getActiveSheet()->getColumnDimension('N')->setAutoSize(true);
                    $objPHPExcel->getActiveSheet()->getColumnDimension('O')->setAutoSize(true);
                    $objPHPExcel->getActiveSheet()->getColumnDimension('P')->setAutoSize(true);
                    header('Content-Type: application/vnd.ms-excel');
                    header('Content-Disposition: attachment;filename="reporte_fuente_de_recursos.xlsx"');
                    header('Cache-Control: max-age=0');
                    $objWriter=PHPExcel_IOFactory::createWriter($objPHPExcel,'Excel2007');
                    $objWriter->save('php://output');
                }
            }
            die();
        }
        public function exportPdf(){
            if(!empty($_SESSION)){
                $arrData = json_decode($_POST['data'],true);
                if(!empty($arrData)){
                    $arrFechaInicial = explode("-",$_POST['fecha_inicial']);
                    $arrFechaFinal = explode("-",$_POST['fecha_final']);
                    $strFechaInicial = $arrFechaInicial[2]."/".$arrFechaInicial[1]."/".$arrFechaInicial[0];
                    $strFechaFinal = $arrFechaFinal[2]."/".$arrFechaFinal[1]."/".$arrFechaFinal[0];

                    $pdf = new MYPDF('L','mm','Letter', true, 'iso-8859-1', false);
                    $pdf->SetDocInfoUnicode (true);
                    $intAltoHead = 5;
                    // set document information
                    $pdf->SetCreator(PDF_CREATOR);
                    $pdf->SetAuthor('IDEALSAS');
                    $pdf->SetTitle('REPORTE POR FUENTE DE RECURSOS');
                    $pdf->SetSubject('REPORTE POR FUENTE DE RECURSOS');
                    $pdf->SetKeywords('TCPDF, PDF, example, test, guide');
                    $pdf->SetMargins(10, 38, 10);// set margins
                    $pdf->SetHeaderMargin(38);// set margins
                    $pdf->SetFooterMargin(17);// set margins
                    $pdf->SetAutoPageBreak(TRUE, 20);// set auto page breaks
                    $pdf->AddPage();
                    $pdf->SetFillColor(51, 153, 204);
                    $pdf->SetTextColor(255,255,255);
                    $pdf->SetFont('Helvetica','b',9);
                    $pdf->MultiCell(53,4,"Fecha","LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->SetFillColor(255,255,255);
                    $pdf->SetTextColor(0,0,0);
                    $pdf->SetFont('Helvetica','',9);
                    $pdf->MultiCell(224,4,"Desde ".$strFechaInicial." Hasta ".$strFechaFinal,"RBT",'L',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->ln();
                    $pdf->ln();
                    $pdf->SetFillColor(51, 153, 204);
                    $pdf->SetTextColor(255,255,255);
                    $pdf->SetFont('Helvetica','b',7);
                    $pdf->MultiCell(53,4,"","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->MultiCell(35,4,"Inicial","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->MultiCell(35,4,"Adición","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->MultiCell(35,4,"Reducción","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->MultiCell(35,4,"Traslados","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->MultiCell(35,4,"Definitivo","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->MultiCell(49,4,"","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->ln();
                    $pdf->SetFillColor(153,221,255);
                    $pdf->SetFont('helvetica','B',6);
                    $pdf->SetTextColor(0,0,0);
                    $pdf->MultiCell(22,$intAltoHead,"Vigencia del gasto","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->MultiCell(11,$intAltoHead,"Fuente","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->MultiCell(20,$intAltoHead,"Nombre fuente","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->MultiCell(17.5,$intAltoHead,"Ingresos","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->MultiCell(17.5,$intAltoHead,"Egresos","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->MultiCell(17.5,$intAltoHead,"Ingresos","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->MultiCell(17.5,$intAltoHead,"Egresos","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->MultiCell(17.5,$intAltoHead,"Ingresos","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->MultiCell(17.5,$intAltoHead,"Egresos","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->MultiCell(17.5,$intAltoHead,"Crédito","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->MultiCell(17.5,$intAltoHead,"Contracrédito","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->MultiCell(17.5,$intAltoHead,"Ingresos","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->MultiCell(17.5,$intAltoHead,"Egresos","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->MultiCell(16,$intAltoHead,"Recaudo","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->MultiCell(16,$intAltoHead,"Pago","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->MultiCell(17,$intAltoHead,"Diferencia (Recaudo-pago)","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->ln();
                    $fill = true;
                    foreach ($arrData as $data) {
                        $pdf->SetFillColor(245,245,245);
                        $pdf->SetFont('helvetica','',5.5);
                        $pdf->SetTextColor(0,0,0);
                        $strVigencia = $pdf->getNumLines($data['vigencia'],22);
                        $strFuente = $pdf->getNumLines($data['nombre'],20);
                        $max = max($strVigencia,$strFuente);
                        $height = $intAltoHead*$max;
                        $pdf->MultiCell(22,$height,$data['vigencia'],"",'L',$fill,0,'','',true,0,false,true,0,'M',true);
                        $pdf->MultiCell(11,$height,$data['codigo'],"",'L',$fill,0,'','',true,0,false,true,0,'M',true);
                        $pdf->MultiCell(20,$height,$data['nombre'],"",'L',$fill,0,'','',true,0,false,true,0,'M',true);
                        if(!$data['inicial_estado']){
                            $pdf->SetFillColor(255,0,0);
                            $pdf->SetTextColor(255,255,255);
                        }else{
                            $pdf->SetFillColor(245,245,245);
                            $pdf->SetTextColor(0,0,0);
                        }
                        $pdf->MultiCell(17.5,$height,formatNum($data['inicial_ingresos'],0,",",".",true),"",'R',$fill,0,'','',true,0,false,true,0,'M',true);
                        $pdf->MultiCell(17.5,$height,formatNum($data['inicial_gastos'],0,",",".",true),"",'R',$fill,0,'','',true,0,false,true,0,'M',true);
                        if(!$data['adiciones_estado']){
                            $pdf->SetFillColor(255,0,0);
                            $pdf->SetTextColor(255,255,255);
                        }else{
                            $pdf->SetFillColor(245,245,245);
                            $pdf->SetTextColor(0,0,0);
                        }
                        $pdf->MultiCell(17.5,$height,formatNum($data['adiciones_ingresos'],0,",",".",true),"",'R',$fill,0,'','',true,0,false,true,0,'M',true);
                        $pdf->MultiCell(17.5,$height,formatNum($data['adiciones_gastos'],0,",",".",true),"",'R',$fill,0,'','',true,0,false,true,0,'M',true);
                        if(!$data['reducciones_estado']){
                            $pdf->SetFillColor(255,0,0);
                            $pdf->SetTextColor(255,255,255);
                        }else{
                            $pdf->SetFillColor(245,245,245);
                            $pdf->SetTextColor(0,0,0);
                        }
                        $pdf->MultiCell(17.5,$height,formatNum($data['reducciones_ingresos'],0,",",".",true),"",'R',$fill,0,'','',true,0,false,true,0,'M',true);
                        $pdf->MultiCell(17.5,$height,formatNum($data['reducciones_gastos'],0,",",".",true),"",'R',$fill,0,'','',true,0,false,true,0,'M',true);
                        if(!$data['traslados_estado']){
                            $pdf->SetFillColor(255,0,0);
                            $pdf->SetTextColor(255,255,255);
                        }else{
                            $pdf->SetFillColor(245,245,245);
                            $pdf->SetTextColor(0,0,0);
                        }
                        $pdf->MultiCell(17.5,$height,formatNum($data['traslados_credito'],0,",",".",true),"",'R',$fill,0,'','',true,0,false,true,0,'M',true);
                        $pdf->MultiCell(17.5,$height,formatNum($data['traslados_contracredito'],0,",",".",true),"",'R',$fill,0,'','',true,0,false,true,0,'M',true);
                        if(!$data['definitivo_estado']){
                            $pdf->SetFillColor(255,0,0);
                            $pdf->SetTextColor(255,255,255);
                        }else{
                            $pdf->SetFillColor(245,245,245);
                            $pdf->SetTextColor(0,0,0);
                        }
                        $pdf->MultiCell(17.5,$height,formatNum($data['definitivo_ingresos'],0,",",".",true),"",'R',$fill,0,'','',true,0,false,true,0,'M',true);
                        $pdf->MultiCell(17.5,$height,formatNum($data['definitivo_gastos'],0,",",".",true),"",'R',$fill,0,'','',true,0,false,true,0,'M',true);

                        $pdf->SetFillColor(245,245,245);
                        $pdf->SetTextColor(0,0,0);

                        $pdf->MultiCell(16,$height,formatNum($data['recaudo'],0,",",".",true),"",'R',$fill,0,'','',true,0,false,true,0,'M',true);
                        $pdf->MultiCell(16,$height,formatNum($data['pago'],0,",",".",true),"",'R',$fill,0,'','',true,0,false,true,0,'M',true);
                        if(!$data['diferencia_estado']){
                            $pdf->SetFillColor(255,0,0);
                            $pdf->SetTextColor(255,255,255);
                        }else{
                            $pdf->SetFillColor(245,245,245);
                            $pdf->SetTextColor(0,0,0);
                        }
                        $pdf->MultiCell(17,$height,formatNum($data['diferencia'],0,",",".",true),"",'R',$fill,0,'','',true,0,false,true,0,'M',true);
                        $fill = !$fill;
                        $pdf->ln();
                        if($pdf->GetY()>165){
                            $pdf->AddPage();
                        }
                    }
                    $pdf->ln();
                    $pdf->Output('reporte_fuente_de_recursos.pdf', 'I');
                }
            }
            die();
        }

    }
    class MYPDF extends TCPDF{
        public function Header(){
			$request = configBasica();
            $strNit = $request['nit'];
            $strRazon = $request['razonsocial'];
			//Parte Izquierda
			$this->Image('../../../imagenes/escudo.jpg',  13, 13, 20, 20, 'JPG', '', 'T', true, 300, '', false, false, 0, false, false, false);// Logo
			$this->SetFont('helvetica','B',8);
			$this->SetY(10);
			$this->RoundedRect(10, 10, 277, 25, 1,'');
			$this->Cell(0.1);
			$this->Cell(26,25,'','R',0,'L');
			$this->SetY(8);
			$this->SetX(40);
			$this->SetFont('helvetica','B',9);
			$this->Cell(242,15,strtoupper("$strRazon"),0,0,'C');
			$this->SetFont('helvetica','B',7);
			$this->SetY(12);
			$this->SetX(40);
			$this->Cell(242,15,'NIT: '.$strNit,0,0,'C');
			//*****************************************************************************************************************************
			$this->SetFont('helvetica','B',9);
			$this->SetY(23);
			$this->SetX(36);
            $this->Cell(251,12,"REPORTE POR FUENTE DE RECURSOS",'T',0,'C');


            $this->SetFont('helvetica','B',7);
			$this->SetY(10);
			$this->SetX(257);
			$this->Cell(30,7," FECHA: ". date("d/m/Y"),"L",0,'L');
			$this->SetY(17);
			$this->SetX(257);
            $this->Cell(35,6,"","L",0,'L');

			//**********************************************************
			$this->SetFont('times','B',10);
			$this->ln(12);
			//**********************************************************
		}
		public function Footer(){
            $request = configBasica();
            $strDireccion = $request['direccion'] != "" ? "Dirección: ".strtoupper($request['direccion']) : "";
            $strWeb = $request['web'] != "" ? "Pagina web: ".strtoupper($request['web']) :"";
            $strEmail = $request['email'] !="" ? "Email: ".strtoupper($request['email']) :"";
            $strTelefono = $request['telefono'] != "" ? "Telefonos: ".$request['telefono'] : "";
            $strUsuario = searchUser($_SESSION['cedulausu'])['nom_usu'];
			$strNick = $_SESSION['nickusu'];
			$strFecha = date("d/m/Y H:i:s");
			$strIp = $_SERVER['REMOTE_ADDR'];

			$this->SetFont('helvetica', 'I', 8);
			$txt = <<<EOD
			$strDireccion $strTelefono
			$strEmail $strWeb
			EOD;
			$this->SetFont('helvetica', 'I', 6);
			$this->Cell(277,10,'','T',0,'T');
			$this->ln(2);
			$this->Write(0, $txt , '', 0, 'C', true, 0, false, false, 0);

			$this->Cell(46, 10, 'Hecho por: '.$strUsuario, 00, false, 'C', 0, '', 0, false, 'T', 'M');
			$this->Cell(46, 10, 'Impreso por: '.$strNick, 0, false, 'C', 0, '', 0, false, 'T', 'M');
			$this->Cell(46, 10, 'IP: '.$strIp, 0, false, 'C',0, '', 0, false, 'T', 'M');
			$this->Cell(46, 10, 'Fecha: '.$strFecha, 0, false, 'C', 0, '', 0, false, 'T', 'M');
			$this->Cell(46, 10, 'IDEAL.10 S.A.S', 0, false, 'C', 0, '', 0, false, 'T', 'M');
            $this->Cell(46, 10, 'Pagina '.$this->getAliasNumPage().'/'.$this->getAliasNbPages(), 0, false, 'C', 0, '', 0, false, 'T', 'M');
		}
    }


    if($_POST){
        $obj = new ExportController();
        if($_POST['action'] == "excel"){
            $obj->exportExcel();
        }else if($_POST['action'] == "pdf"){
            $obj->exportPdf();
        }
    }

?>
