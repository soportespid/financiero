<?php
    require_once '../../../Librerias/core/Helpers.php';
    require_once '../../../PHPExcel/Classes/PHPExcel.php';
    require_once '../models/reporte_model.php';
    session_start();

    class ReporteController extends ReporteModel{
        public function initialData(){
            if(!empty($_SESSION)){
                $arrResponse = array("fuentes"=>$this->selectFuentes_1());
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
            die();
        }
        public function genData(){
            if(!empty($_SESSION)){
                $strFechaInicial = strClean($_POST['fecha_inicial']);
                $strFechaFinal = strClean($_POST['fecha_final']);
                $intVigenciaGasto = strClean($_POST['gasto']);
                $arrFuentes = json_decode($_POST['fuentes'],true);
                $arrData = [];
                if(!empty($arrFuentes)){
                    $arrData = $this->selectInicial($strFechaInicial,$strFechaFinal,$intVigenciaGasto,$arrFuentes);
                    $arrData = $this->selectAdiciones($strFechaInicial,$strFechaFinal,$intVigenciaGasto,$arrFuentes,$arrData);
                    $arrData = $this->selectReducciones($strFechaInicial,$strFechaFinal,$intVigenciaGasto,$arrFuentes,$arrData);
                    $arrData = $this->selectTraslados($strFechaInicial,$strFechaFinal,$intVigenciaGasto,$arrFuentes,$arrData);
                    $arrData = $this->selectRecaudos($strFechaInicial,$strFechaFinal,$intVigenciaGasto,$arrFuentes,$arrData);
                    $arrData = $this->selectPagos($strFechaInicial,$strFechaFinal,$intVigenciaGasto,$arrFuentes,$arrData);
                    $arrData = $this->orderData($arrData,intval($intVigenciaGasto));
                }
                echo json_encode($arrData,JSON_UNESCAPED_UNICODE);
            }
            die();
        }
        public function orderData($arrData,$intVigenciaGasto){
            $arrGastos = array(
                1=>"1 - Vigencia actual",
                2=>"2 - Reservas",
                3=>"3 - Cuentas por pagar",
                4=>"4 - Vigencias futuras - vigencia actual",
                5=>"5 - Vigencias futuras - reservas",
                6=>"6 - Vigencias futuras - cuentas por pagar",
            );
            $arrTempFuentes = array_values(array_unique(array_column($arrData,"llave")));
            $arrTempData = [];
            foreach ($arrTempFuentes as $data) {
                array_push($arrTempData,
                    array_values(array_filter($arrData,function($e)use($data){return $data==$e['llave'];}))
                );
            }
            $arrData = $arrTempData;
            $arrTempData = [];
            $totalData = count($arrData);
            for ($i=0; $i < $totalData; $i++) {
                $data = $arrData[$i];
                $inicialIngresos = 0;
                $inicialEgresos = 0;
                $adicionesIngresos = 0;
                $adicionesEgresos = 0;
                $reduccionesIngresos = 0;
                $reduccionesEgresos = 0;
                $trasladosCredito = 0;
                $trasladosContracredito = 0;
                $recaudos = 0;
                $pagos201 = 0;
                $pagos401 = 0;
                foreach ($data as $det) {
                    if($det['tipo'] == "ingreso" && $det['dato'] == "inicial")$inicialIngresos+=$det['total'];
                    if($det['tipo'] == "egreso" && $det['dato'] == "inicial")$inicialEgresos+=$det['total'];
                    if($det['tipo'] == "ingreso" && $det['dato'] == "adicion")$adicionesIngresos+=$det['total'];
                    if($det['tipo'] == "egreso" && $det['dato'] == "adicion")$adicionesEgresos+=$det['total'];
                    if($det['tipo'] == "ingreso" && $det['dato'] == "reduccion")$reduccionesIngresos+=$det['total'];
                    if($det['tipo'] == "egreso" && $det['dato'] == "reduccion")$reduccionesEgresos+=$det['total'];
                    if($det['tipo'] == "credito" && $det['dato'] == "traslado")$trasladosCredito+=$det['total'];
                    if($det['tipo'] == "contracredito" && $det['dato'] == "traslado")$trasladosContracredito+=$det['total'];
                    if($det['tipo'] == "ingreso" && $det['dato'] == "recaudo")$recaudos+=$det['total'];
                    if(isset($det['movimiento']) && $det['movimiento']  == "201" && $det['dato'] == "pago")$pagos201+=$det['total'];
                    if(isset($det['movimiento']) && $det['movimiento']  == "401" && $det['dato'] == "pago")$pagos401+=$det['total'];
                }
                $pagos = $pagos201-$pagos401;
                $diferencia = $recaudos -$pagos;
                $definitivoIngresos = $inicialIngresos+$adicionesIngresos-$reduccionesIngresos;
                $definitivoEgresos = ($inicialEgresos+$adicionesEgresos+$trasladosCredito)-($trasladosContracredito+$reduccionesEgresos);
                $strNombreVigencia ="";
                if($intVigenciaGasto > 0 && isset($arrGastos[$data[0]['vigencia']])){
                    $strNombreVigencia = $arrGastos[$data[0]['vigencia']];
                }else if($intVigenciaGasto == 0){
                    $strNombreVigencia = "Todos";
                }
                array_push($arrTempData,array(
                    "nombre"=>$data[0]['nombre'],
                    "codigo"=>$data[0]['codigo'],
                    "codigo_vigencia"=>$data[0]['vigencia'],
                    "vigencia"=>$strNombreVigencia,
                    "inicial_ingresos"=>$inicialIngresos,
                    "inicial_gastos"=>$inicialEgresos,
                    "inicial_estado"=>$inicialIngresos == $inicialEgresos ? 1 : 0,
                    "adiciones_ingresos"=>$adicionesIngresos,
                    "adiciones_gastos"=>$adicionesEgresos,
                    "adiciones_estado"=>$adicionesIngresos == $adicionesEgresos ? 1 : 0,
                    "reducciones_ingresos"=>$reduccionesIngresos,
                    "reducciones_gastos"=>$reduccionesEgresos,
                    "reducciones_estado"=>$reduccionesIngresos == $reduccionesEgresos ? 1 :0,
                    "traslados_credito"=>$trasladosCredito,
                    "traslados_contracredito"=>$trasladosContracredito,
                    "traslados_estado"=>$trasladosCredito == $trasladosContracredito ? 1 : 0,
                    "definitivo_ingresos"=>$definitivoIngresos,
                    "definitivo_gastos"=>$definitivoEgresos,
                    "definitivo_estado"=>$definitivoIngresos == $definitivoEgresos ? 1 : 0,
                    "recaudo"=>$recaudos,
                    "pago"=>$pagos,
                    "diferencia"=>$diferencia,
                    "diferencia_estado"=>$diferencia >= 0 ? 1 : 0
                ));
            }
            $arrData = array_values(array_filter($arrTempData,function($e){return $e['codigo_vigencia']!="";}));
            return $arrData;
        }
    }

    if($_POST){
        $obj = new ReporteController();
        if($_POST['action'] == "get"){
            $obj->initialData();
        }else if($_POST['action'] == "gen"){
            $obj->genData();
        }
    }

?>
