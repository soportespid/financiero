
const URL = 'presupuesto_ccpet/clasificadores/ccp-seleccionarcuentasingresos.php';
import { ordenarArbol } from './../../funciones.js'

const app = Vue.createApp({
    data() {
      return {
        loading: false,
        rubrosIngresos: [],
        rubrosIngresosAuxiliar: [],
        rubrosSeleccionados: [],
        myStyleSeleccionados: {
            backgroundColor: '#a3e4d7',
        },
        myStyleAgregados: {
            cursor: 'pointer',
            'font-weight': 'normal',
        },
        rubrosAgregados: [],
        mostarRubrosSeleccionados: false,
        rubrosSeleccionadosModal: [],
        rubrosGuardados: [],

        mostarAgregarAuxiliar: false,
        rubroSeleccionado: '',
        auxiliaresDelRubro: [],

        showModal: false,
        checkedClasificadores : [],

        checkedClasificadores_1 : [],
        noMember: false,
        show_resultados: false,
        show_table_search: false,
        listClasi: [],
        showModal_respuesta: false,
        cuentasSeleccionadas: [],
        nomClasificador: [],

      }
    },

    created() {
        this.cargarCuentas();
        
    },

    async mounted() {
        this.listClasificadores();
        await this.buscarClasificador();
    },

    computed: {
        mostrarGuardarRubros(){
            return this.rubrosSeleccionados.length != this.rubrosGuardados.length ? true : false
        },

        mostrarBotonVisuaizarRubros(){
            return this.rubrosSeleccionados.length == this.rubrosGuardados.length ? true : false
        }
    },

    methods: {

        async listClasificadores(){
            let listaC = [];
            await axios.post('vue/ccp-programarClasificadores.php?action=list_clasi')
            .then(function(response){
                listaC = response.data.list_clasi;
                /* app.listClasi = response.data.list_clasi; */
            });
            this.listClasi = listaC;
        },
        
        getParametros(name){
            //Captura parametros
            name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
            var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
            results = regex.exec(location.search);
            return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
        },

        async cargarCuentas() {

            this.loading = true;

            /* this.tipo_comp = this.getParametros('tipo_comprobante');
            let formData = new FormData();
            formData.append("tipo_comp", this.tipo_comp); */
            
            await axios.post(URL)
            .then((response) => {
                if(response.data.cuentasIngresosSeleccionadas != null){
                    this.rubrosGuardados = response.data.cuentasIngresosSeleccionadas;
                    //this.rubrosSeleccionados = response.data.cuentasIngresosSeleccionadas;
                }
                if(response.data.accounts){
                    this.rubrosIngresos = response.data.accounts;
                }else{
                    //agregar error de busqueda
                    Swal.fire({
                        position: 'top-end',
                        icon: 'error',
                        title: 'No se encontraron resultados',
                        showConfirmButton: false,
                        timer: 1500
                    })
                }
                
                
            }).catch((error) => {
                this.error = true;
                console.log(error)
            }).finally(() => {
                this.loading = false;
            });
            this.filtrarRubrosAgregados();
        },

        filtrarRubrosAgregados(){
          this.rubrosAgregados = this.rubrosIngresos.filter((rubro) => rubro[2] === 'A');

          const copyOfRubrosGuardados = JSON.parse(JSON.stringify(this.rubrosGuardados))
          
          this.rubrosSeleccionados = copyOfRubrosGuardados;
        },

        guardarRubros(){
            if(this.rubrosSeleccionados.length != ''){

                Swal.fire({
                    title: 'Esta seguro de guardar?',
                    text: "Guardar rubros en la base de datos, confirmar campos!",
                    icon: 'question',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Si, guardar!'
                }).then((result) => {
                    if (result.isConfirmed) {
                        var formData = new FormData();

                        for(let i=0; i <= this.rubrosSeleccionados.length-1; i++){
                            const val = Object.values(this.rubrosSeleccionados[i]).length;
                            
                            for(let x = 0; x <= val; x++){
                                formData.append("detallesAd["+i+"][]", Object.values(this.rubrosSeleccionados[i])[x]);
                            }
                            
                        }

                        /* this.rubrosSeleccionados.map((b, i) => {
                            
                            formData.append("detallesAd["+i+"][]", Object.values(b));
                        }) */
                    
                        axios.post(URL + '?action=guardarRubros', formData)
                        .then((response) => {
                            
                            if(response.data.insertaBien){
                                Swal.fire({
                                    position: 'top-end',
                                    icon: 'success',
                                    title: 'Los rubros se han guardado; con Exito',
                                    showConfirmButton: false,
                                    timer: 1500
                                }).then((response) => {
                                        this.redireccionar();
                                    });
                            }else{
                            
                                Swal.fire(
                                    'Error!',
                                    'No se pudo guardar.',
                                    'error'
                                );
                            }
                            
                        });
                        
                    }
                });
            }else{
                Swal.fire(
                    'Falta informaci&oacute;n para guardar clasificaci&oacute;n.',
                    'Verifique que todos los campos esten diligenciados.',
                    'warning'
                );
            }
        },

        selRubro(rubro){

            if(rubro != undefined && rubro[2] == 'C'){
                //const parsedobj = JSON.stringify(this.rubrosSeleccionados);
                
                if(this.rubrosSeleccionados.some(selectedRubro => selectedRubro[0] === rubro[0])){
                    
                    this.removeItemFromArr(rubro);
                }else{
                    this.rubrosSeleccionados.push(rubro);
                }
                
            }else{
                Swal.fire(
                    'Tipo de cuenta incorrecto.',
                    'Escoger una cuenta de captura (C)',
                    'warning'
                );
            }
        },

        removeItemFromArr(item){
            
            var i = this.rubrosSeleccionados.findIndex( (element) => {
                return element[0] === item[0]
            });
            if ( i !== -1 ) {
                this.rubrosSeleccionados.splice( i, 1 );
            }
        }, 

        estaEnArray(rubro) {
            return this.rubrosSeleccionados.some(selectedRubro => selectedRubro[0] === rubro);
        },

        visualizarCuentas(){
            this.rubrosSeleccionadosModal = ordenarArbol(this.rubrosAgregados, this.rubrosSeleccionados);
            this.mostarRubrosSeleccionados = true;
        },

        redireccionar(){
            
            location.href ="ccp-seleccionarcuentasingresos.php";
        },

        excel() {
            document.form2.action="ccp-excelrubrosseleccionados.php";
            document.form2.target="_BLANK";
            document.form2.submit();
        },

        toFormData(obj){
            var form_data = new FormData();
            for(var key in obj){
                form_data.append(key, obj[key]);
            }
            return form_data;
        },

        formatonumero: function(valor){
            return new Intl.NumberFormat("en-US", {style: "currency", currency: "USD"}).format(valor);
        },

        agregarAxuliar(rubro){
            this.mostarAgregarAuxiliar = true;
            this.rubroSeleccionado = rubro[0]+' - '+rubro[1];

            this.auxiliaresDelRubro = this.rubrosIngresos.filter((auxiliar) => {
                                                                        if(auxiliar[4] != null){
                                                                            return auxiliar[0].substring(0, rubro[0].length) == rubro[0];
                                                                        }
                                                                    });
            /* let consecutivo = 0;
            this.auxiliaresDelRubro.map((auxiliar) => {
                let consecutivoAux = auxiliar[0].split('-')[1].split('-')[0];
                if(consecutivoAux > consecutivo){
                    consecutivo = consecutivoAux;
                }
            });
            //convertir a string y sumarle 1
            consecutivo = parseInt(consecutivo) + 1;
            //agregar 0 a la izquierda si es menor a 10
            if(consecutivo < 10){
                consecutivo = '0'+consecutivo;
            }
            let auxiliar = rubro[0]+'-'+consecutivo;
            this.auxiliaresDelRubro.push([auxiliar, rubro[1], 'C', rubro[3], consecutivo, '']); */
        },

        agregarOtroAuxiliar(){
            let consecutivo = 0;
            this.auxiliaresDelRubro.map((auxiliar) => {
                let consecutivoAux = auxiliar[0].split('-')[1].split('-')[0];
                if(consecutivoAux > consecutivo){
                    consecutivo = consecutivoAux;
                }
            });
            //convertir a string y sumarle 1
            consecutivo = parseInt(consecutivo) + 1;
            //agregar 0 a la izquierda si es menor a 10
            if(consecutivo < 10){
                consecutivo = '0'+consecutivo;
            }
            let auxiliar = this.rubroSeleccionado.split(' - ')[0]+'-'+consecutivo;
            let nombreRubro = this.rubroSeleccionado.split(' - ')[1];
            let nivel = this.rubroSeleccionado.split(' - ')[3];
            this.auxiliaresDelRubro.push([auxiliar, nombreRubro, 'C', nivel, consecutivo, '']);
        },

        quitarAuxiliar(rubro){
            this.auxiliaresDelRubro = this.auxiliaresDelRubro.filter((auxiliar) => auxiliar[0] != rubro[0]);            
        },

        agregarNombreAuxiliar(rubro, event){
            let nombre = event.target.value;
            this.auxiliaresDelRubro.map((auxiliar) => {
                if(auxiliar[0] == rubro[0]){
                    auxiliar[5] = nombre;
                }
            });
        },

        guardarAuxiliares(){
            /* if(this.auxiliaresDelRubro.length != 0){ */
                this.mostarAgregarAuxiliar = false;
                Swal.fire({
                    title: 'Esta seguro de guardar?',
                    text: "Guardar auxiliares en la base de datos, confirmar campos!",
                    icon: 'question',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Si, guardar!'
                }).then((result) => {
                    if (result.isConfirmed) {
                        var formData = new FormData();

                        for(let i=0; i <= this.auxiliaresDelRubro.length-1; i++){
                            const val = Object.values(this.auxiliaresDelRubro[i]).length;
                            
                            for(let x = 0; x <= val; x++){
                                formData.append("detallesAd["+i+"][]", Object.values(this.auxiliaresDelRubro[i])[x]);
                            }
                            
                        }

                        formData.append("rubroSel", this.rubroSeleccionado.split(' - ')[0]);
                        
                        axios.post(URL + '?action=guardarAuxiliares', formData)
                        .then((response) => {
                            console.log(response.data);
                            if(response.data.insertaBien){
                                Swal.fire({
                                    position: 'top-end',
                                    icon: 'success',
                                    title: 'Los auxiliares se han guardado; con Exito',
                                    showConfirmButton: false,
                                    timer: 1500
                                }).then((response) => {
                                        this.cargarCuentas();
                                    });
                            }else{
                            
                                Swal.fire(
                                    'Error!',
                                    'No se pudo guardar.',
                                    'error'
                                );
                            }
                            
                        });
                        
                    }
                });
            /* }else{
                Swal.fire(
                    'Falta informaci&oacute;n para guardar clasificaci&oacute;n.',
                    'Verifique que todos los campos esten diligenciados.',
                    'warning'
                );
            } */
        },

        async programarCuentas(rubro){
            this.showModal = !this.showModal;
            this.checkedClasificadores = [];
            let clasi = [];
            this.cuentasSeleccionadas = [];
            this.cuentasSeleccionadas.push(rubro);
            await axios.post('vue/ccp-programarClasificadores.php?action=buscarCuenta&cuenta=' + rubro)
            .then(function(response){

                app.checkedClasificadores_1 = response.data.clasificadores;
                /* app.cuentasSeleccionadas = response.data.cuentas; */
                
                if(response.data.cuentas == ''){
                    app.noMember = true;
                    app.show_resultados = false;
                    app.show_table_search = true;

                }
                else{
                    app.noMember = false;
                    app.show_resultados = true;
                    app.show_table_search = true;
                }
                if(response.data.clasificadores != ''){
                    var parsedobj1 = JSON.parse(JSON.stringify(app.checkedClasificadores_1));
                    app.checkedClasificadores_1 = [];
                    app.checkedClasificadores_1 = parsedobj1[0];
                    // if(app.checkedClasificadores_1 != undefined){
                    var partes = app.checkedClasificadores_1[0].split(',');
                    // }
                    
                    for(let i = 0; i < partes.length; i++){
                        
                        clasi.push(partes[i]);
                    }
                }
            });
            this.checkedClasificadores = clasi;
        },

        guardarClasificadores: async function(){
            this.codigosSeleccionado = [];
            let modal = false;
            let modal_respuesta = false;

            var clasi = this.checkedClasificadores.join();
            for(var i=0; i< this.cuentasSeleccionadas.length; i++)
            {
                this.codigosSeleccionado.push(this.cuentasSeleccionadas[i]);

            }

            var formData = new FormData();
            formData.append("clasificadores", clasi);
            formData.append("cuentas", this.codigosSeleccionado);
            
            await axios.post('vue/ccp-programarClasificadores.php?action=guardarClasificadores', formData)
                .then(function(response){
                    console.log(response.data);
                if(response.data.insertaBien){
                    modal = false
                    modal_respuesta = true
                    
                }
            });
            this.showModal = modal;
            this.showModal_respuesta = modal_respuesta;
        },

        buscarClasificador:  function(){
            
             axios.post('vue/ccp-programarClasificadores.php?action=buscarClasificador')
            .then((response) => {
                this.nomClasificador = response.data.nomClasi;
            });
            
        },

    }
})

app.mount('#myapp')