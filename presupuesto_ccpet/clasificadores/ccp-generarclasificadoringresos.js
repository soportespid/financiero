
const URL = 'presupuesto_ccpet/clasificadores/ccp-generarclasificadoringresos.php';
import { ordenarArbol, buscaNombreRubros, filtroEnArrayDeObjetos } from './../../funciones.js'

const app = Vue.createApp({
    data() {
      return {
        results: [],
        results_seleccionados: [],
        results_seleccionadosOrd: [],
        show_table_search: false,
        show_resultados: true,
        show_resultados_seleccionados: false,
        get_code: [],
        get_code_1: [],
        myStyle: {
            backgroundColor: '#12DB67',
            color: 'white'
        },
        myStyleMayor: {
            'font-weight': 'bold',
        },
        name_clasificador: '',

        loading: false,
        rubrosInicial: [],
        rubrosJoin: [],
        accounts: [],
        accountsAux: [],
        accountsMayor: [],
        fuentes: [],
        arbol: [],
        arbolInv: [],
        buscar_rubro: '',
        resultsBuscar: [],
        vigencia: 0,
        years:[],
        checkTodo: false,
      }
    },

    async mounted() {
        this.loading = false;
        await this.cargayears();
        this.cargarParametros();
    },

    computed: {
        mostrarCheck(){
            return this.buscar_rubro.length > 2 ? true : false
        },
    },

    methods: {

        cargayears: async function(){
			await axios.post('vue/presupuesto_ccp/ccp-bancoproyectos.php?buscar=years')
			.then(
				(response)=>
				{
					this.years=response.data.anio;
					var idanio=response.data.anio.length -1;
					if (idanio>=0){this.vigencia = response.data.anio[idanio][0];}
					else{this.vigencia ='';}
				}
            );
            
		},

        async cargarParametros(){

            this.loading = true;

            let formData = new FormData();
            formData.append("vigencia", this.vigencia);
            
            await axios.post(URL+'?action=show1', formData)
            .then((response) => {
                /* if(response.data.rubrosInicial != null) */
                this.rubrosInicial = response.data.rubrosInicial;
                this.accounts = response.data.accounts;
                this.fuentes = response.data.fuentes;

            }).catch((error) => {
                this.error = true;
                console.log(error)
            }).finally(() => {
                this.loading = false;
                this.auxiliarMayor();
                this.orderAccounts();
            });
        },

        async orderAccounts(){

            const arrPuro = this.rubrosInicial.map((e) => {
                return e[0]
            });

            const dataArrSet = new Set(arrPuro);

            const dataArr = Array.from(dataArrSet);
           
            const nombresRubros = this.accountsAux.concat(this.fuentes);
            this.results = buscaNombreRubros(dataArr, nombresRubros);

            this.resultsBuscar = [...this.results];

        },

        auxiliarMayor(){
            this.accountsAux = this.accounts.filter(element => element[2] == 'C');
            this.accountsMayor = this.accounts.filter(element => element[2] == 'A');
        },

        estaEnArray(codigos){
            const parsedobj = JSON.stringify(this.results_seleccionados);
            return parsedobj.includes(codigos[0])
        },

        removeItemFromArr(item){
            var i = this.results_seleccionados.indexOf( item );
                
            if ( i !== -1 ) {
                this.results_seleccionados.splice( i, 1 );
            }

        }, 

        async seleccionaCodigos(codigos){
           
            const parsedobj = JSON.stringify(this.results_seleccionados);

            if(parsedobj.includes(codigos[0])){
                this.removeItemFromArr(codigos);
            }else{
                this.results_seleccionados.push(codigos);
            }

            this.results_seleccionadosOrd = await ordenarArbol(this.accountsMayor, this.results_seleccionados);
            
            this.show_resultados_seleccionados = this.results_seleccionados.length > 0 ? true : false;
        },

        seleccionaCodigosCheck(codigos){

            const parsedobj = JSON.stringify(this.results_seleccionados);

            if(parsedobj.includes(codigos[0])){
                this.removeItemFromArr(codigos);
            }else{
                this.results_seleccionados.push(codigos);
            }
        },

        async ordenarArbolCheck(){
            
            this.results_seleccionadosOrd = await ordenarArbol(this.accountsMayor, this.results_seleccionados);
            
            this.show_resultados_seleccionados = this.results_seleccionados.length > 0 ? true : false;
        },

        async buscarRubro(){

            this.results = [];
            var text = this.buscar_rubro;
            const data = this.resultsBuscar;
            this.results = await filtroEnArrayDeObjetos({'data': data, 'text': text});

        },

        async addClasificador(){

            if(this.name_clasificador != ''){
                if(this.results_seleccionadosOrd.length > 0){
                    
                    Swal.fire({
                        title: 'Esta seguro de guardar?',
                        text: "Guardar clasificador presupuestal de ingresos, confirmar campos!",
                        icon: 'question',
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Si, guardar!'
                    }).then((result) => {
                        if (result.isConfirmed) {

                            this.loading = true;

                            var formData = new FormData();
                            formData.append("nombre_clasificador", this.name_clasificador);
                            
                            axios.post(URL+'?action=guardarCab', formData)
                            .then((response) => {
                                
                                if(response.data.id_cab != ''){
                                    var formData1 = new FormData();

                                    for(let i=0; i <= this.results_seleccionadosOrd.length-1; i++){
                                        const val = Object.values(this.results_seleccionadosOrd[i]).length;
                                        
                                        for(let x = 0; x <= val; x++){
                                            formData1.append("detallesAd["+i+"][]", Object.values(this.results_seleccionadosOrd[i])[x]);
                                        }
                                        
                                    }

                                    formData1.append("id_cab", response.data.id_cab);

                                    axios.post(URL+'?action=guardarDet', formData1)
                                    .then((response) => {
                                        if(response.data.insertaBien){
                                            Swal.fire({
                                                position: 'top-end',
                                                icon: 'success',
                                                title: 'El clasificador se guard&oacute; con Exito',
                                                showConfirmButton: false,
                                                timer: 1500
                                            }).then((response) => {
                                                    this.redireccionar();
                                                });
                                        }else{
                                            
                                            Swal.fire(
                                                'Error!',
                                                'No se pudo guardar.',
                                                'error'
                                            );
                                        }
                                    })

                                }

                            }).catch((error) => {
                                this.error = true;
                                console.log(error)
                            }).finally(() => {
                                this.loading = false;
                            });
                        }
                    })

                }else{
                    Swal.fire(
                        'Falta seleccionar rubros.',
                        'Seleccione los rubros con los que desea crear el clasificador dando clic en el listado.',
                        'warning'
                    )
                }
            }else{
                Swal.fire(
                    'Falta digitar el nombre del clasificador.',
                    'Digite el nombre del clasificador en la casilla correspondiente.',
                    'warning'
                )
            }            
           
        },
        //metodos par aeste archivo

        redireccionar(){
            
            location.href ="ccp-generarclasificadoringresos.php";
        },

        toFormData(obj){
            var form_data = new FormData();
            for(var key in obj){
                form_data.append(key, obj[key]);
            }
            return form_data;
        },

        selTodo(){
            let timerInterval
            Swal.fire({
                title: 'Buscando y organizando mayores de los rubros seleccionados!',
                html: '<b></b>',
                timer: 6000,
                timerProgressBar: true,
                didOpen: () => {
                    Swal.showLoading()
                    const b = Swal.getHtmlContainer().querySelector('b')
                    timerInterval = setInterval(() => {
                    b.textContent = Swal.getTimerLeft()
                    }, 100)
                },
                willClose: () => {
                    clearInterval(timerInterval)
                }
            }).then((result) => {
                this.checkTodo = !this.checkTodo;
                this.results.map(element => {
                    this.seleccionaCodigosCheck(element);
                })
                this.ordenarArbolCheck();
                /* Read more about handling dismissals below */
                if (result.dismiss === Swal.DismissReason.timer) {
                    /* console.log('I was closed by the timer') */
                }
            })

        },

    }
})

app.mount('#myapp')