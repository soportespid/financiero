<?php

    require_once '../../comun.inc';
    require '../../funciones.inc';
 
    session_start();

	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");

    $maxVersion = ultimaVersionIngresosCCPET();

    $out = array('error' => false);

    $action = "show";

    if(isset($_GET['action'])){
        $action = $_GET['action'];
    }
    
    if($action == 'show'){

        $cuentasIngresosSeleccionadas = [];
        $accounts = [];
        $codigosAux = [];

        $vigencia = $_POST['vigencia'];
    
        $sqlrAccounts = "SELECT 
            IF(codigo_auxiliar IS NULL OR codigo_auxiliar = '', codigo, CONCAT(codigo, '-', codigo_auxiliar)) AS codigo, 
            IF(codigo_auxiliar IS NULL OR codigo_auxiliar = '', nombre, CONCAT(nombre, '-', nombre_auxiliar)) AS nombre, 
            tipo, 
            nivel, 
            codigo_auxiliar, 
            nombre_auxiliar  
        FROM cuentasingresosccpet
        WHERE  version = $maxVersion order by codigo";
        $respAccounts = mysqli_query($linkbd, $sqlrAccounts);
        while($rowAccounts = mysqli_fetch_row($respAccounts)){
            array_push($accounts, $rowAccounts);
        }

        $sqlrSel = "SELECT codigo, nombre, tipo, nivel FROM cuentasingresosccpetseleccionadas";
        $respSel = mysqli_query($linkbd, $sqlrSel);
        while($rowSel = mysqli_fetch_row($respSel)){
            array_push($cuentasIngresosSeleccionadas, $rowSel);
        }

        $out['cuentasIngresosSeleccionadas'] = $cuentasIngresosSeleccionadas;
        $out['accounts'] = $accounts;
        $out['codigosAux'] = $codigosAux;

    }

    if($action == 'guardarRubros'){

        $user = $_SESSION['nickusu'];

        $sql = "DELETE FROM cuentasingresosccpetseleccionadas";
        $res = mysqli_query($linkbd, $sql);

        for($x = 0; $x < count($_POST["detallesAd"]); $x++){
            $rubro = '';
            $nombre = '';
            $tipo = '';

            $rubro = $_POST["detallesAd"][$x][0];
            $nombre = $_POST["detallesAd"][$x][1];
            $tipo = $_POST["detallesAd"][$x][2];
            $nivel = $_POST["detallesAd"][$x][3];

            $sql = "INSERT INTO cuentasingresosccpetseleccionadas(codigo, nombre, tipo, nivel, user) VALUES ('$rubro', '$nombre','$tipo', '$nivel', '$user')";
            $res = mysqli_query($linkbd, $sql);
        }

        $out['insertaBien'] = true;

    }

    if($action == 'guardarAuxiliares'){
        $user = $_SESSION['nickusu'];

        $rubroSel = $_POST['rubroSel'];

        $sql = "DELETE FROM cuentasingresosccpet WHERE codigo_auxiliar IS NOT NULL AND version = $maxVersion AND codigo = '$rubroSel' AND codigo_auxiliar != ''";
        $res = mysqli_query($linkbd, $sql);

        for($x = 0; $x < count($_POST["detallesAd"]); $x++){
            $rubro = '';
            $nombre = '';
            $tipo = '';
            $nivel = '';
            $codigo_auxiliar = ''; 
            $nombre_auxiliar = '';

            $rubro = explode('-', $_POST["detallesAd"][$x][0])[0];
            $nombre = $_POST["detallesAd"][$x][1];
            $tipo = $_POST["detallesAd"][$x][2];
            $nivel = $_POST["detallesAd"][$x][3];
            $codigo_auxiliar = $_POST["detallesAd"][$x][4];
            $nombre_auxiliar = $_POST["detallesAd"][$x][5];

            

            $sqlrSelect = "SELECT padre, municipio, departamento, nivel, nombre FROM cuentasingresosccpet WHERE codigo = '$rubro' AND version = $maxVersion limit 1";
            $respSelect = mysqli_query($linkbd, $sqlrSelect);
            $rowSelect = mysqli_fetch_row($respSelect);

            $sql = "INSERT INTO cuentasingresosccpet(codigo, nombre, padre, nivel, version, tipo, municipio, departamento, codigo_auxiliar, nombre_auxiliar) VALUES ('$rubro', '$rowSelect[4]', '$rowSelect[0]', '$rowSelect[3]', $maxVersion, '$tipo', '$rowSelect[1]', '$rowSelect[2]', '$codigo_auxiliar', '$nombre_auxiliar')";
            $res = mysqli_query($linkbd, $sql);
        }

        $out['insertaBien'] = true;
    }

    header("Content-type: application/json");
    echo json_encode($out);
    die();