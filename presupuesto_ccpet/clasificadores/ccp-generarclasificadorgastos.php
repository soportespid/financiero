<?php

    require_once '../../comun.inc';
    require '../../funciones.inc';
 
    session_start();

	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");

    $maxVersion = ultimaVersionGastosCCPET();

    $out = array('error' => false);

    $action = "show";

    if(isset($_GET['action'])){
        $action = $_GET['action'];
    }

    
    
    if($action == 'show'){

        $rubrosInicial = [];
        $rubrosAd = [];
        $rubrosTrasladosCred = [];
        $accounts = [];
        $fuentes = [];
        $rubrosInv = [];
        $rubrosAdInv = [];
        $rubrosTrasladosCredInv = [];
        $nombreRubrosInv = [];


        $vigencia = $_POST['vigencia'];

        $sqlrIni = "SELECT DISTINCT cuenta, fuente FROM ccpetinicialgastosfun WHERE vigencia = '".$vigencia."'";
        $resIni = mysqli_query($linkbd, $sqlrIni);
        while($rowIni = mysqli_fetch_row($resIni)){
            $rubro = '';
            $rubroNom = [];
            $rubro = $rowIni[0]."-".$rowIni[1];
            /* $nombre = buscacuentaccpetgastos($rowIni[0], $maxVersion)." - ".buscaNombreFuenteCCPET($rowIni[1]); */
            array_push($rubroNom, $rubro);
            array_push($rubrosInicial, $rubroNom);
        }

        $sqlrAd = "SELECT DISTINCT cuenta, fuente FROM ccpet_adiciones WHERE vigencia = '".$vigencia."' AND cuenta != '' AND tipo_cuenta = 'G'";
        $respAd = mysqli_query($linkbd, $sqlrAd);
        while($rowAd = mysqli_fetch_row($respAd)){
            $rubro = '';
            $rubroNom = [];
            $rubro = $rowAd[0]."-".$rowAd[1];
            /* $nombre = buscacuentaccpetgastos($rowAd[0], $maxVersion)." - ".buscaNombreFuenteCCPET($rowAd[1]); */
            array_push($rubroNom, $rubro);
            array_push($rubrosAd, $rubroNom);
        }

        $sqlrTrasladosCred = "SELECT DISTINCT cuenta, fuente FROM ccpet_traslados WHERE vigencia = '".$vigencia."' AND cuenta != '' AND tipo = 'C'";
        $respTrasladosCred = mysqli_query($linkbd, $sqlrTrasladosCred);
        while($rowTrasladosCred = mysqli_fetch_row($respTrasladosCred)){
            $rubro = '';
            $rubroNom = [];
            $rubro = $rowTrasladosCred[0]."-".$rowTrasladosCred[1];
            /* $nombre = buscacuentaccpetgastos($rowTrasladosCred[0], $maxVersion)." - ".buscaNombreFuenteCCPET($rowTrasladosCred[1]); */
            array_push($rubroNom, $rubro);
            array_push($rubrosTrasladosCred, $rubroNom);
        }

        $sqlrAccounts = "SELECT codigo, nombre, tipo FROM cuentasccpet WHERE version = $maxVersion AND SUBSTRING(codigo, 1, 3) != '2.3'";
        $respAccounts = mysqli_query($linkbd, $sqlrAccounts);
        while($rowAccounts = mysqli_fetch_row($respAccounts)){
            array_push($accounts, $rowAccounts);
        }

        $sqlrFuentes = "SELECT codigo_fuente, nombre FROM ccpet_fuentes_cuipo";
        $respFuentes = mysqli_query($linkbd, $sqlrFuentes);
        while($rowFuentes = mysqli_fetch_row($respFuentes)){
            array_push($fuentes, $rowFuentes);
        }

        $sqlrInv = "SELECT TB2.indicador_producto, TB1.codigo, TB2.id_fuente FROM ccpproyectospresupuesto AS TB1, ccpproyectospresupuesto_presupuesto AS TB2 WHERE TB1.id = TB2.codproyecto AND TB1.vigencia = '$vigencia'";
        $respInv = mysqli_query($linkbd, $sqlrInv);
        while($rowInv = mysqli_fetch_row($respInv)){
            $rubros = [];
            $nombreRubro = '';
            $nombreRubro = $rowInv[0].'-'.$rowInv[1].'-'.$rowInv[2];
            array_push($rubros, $nombreRubro);
            array_push($rubrosInv, $rubros);
        }

        $sqlrAdInv = "SELECT programatico, bpim, fuente FROM ccpet_adiciones WHERE vigencia = '$vigencia' AND bpim != ''";
        $respAdInv = mysqli_query($linkbd, $sqlrAdInv);
        while($rowAdInv = mysqli_fetch_row($respAdInv)){
            $rubros = [];
            $nombreRubro = '';

            $nombreRubro = $rowAdInv[0].'-'.$rowAdInv[1].'-'.$rowAdInv[2];
            array_push($rubros, $nombreRubro);
            array_push($rubrosAdInv, $rubros);
        }

        $sqlrTrasladosCredInv = "SELECT programatico, bpim, fuente FROM ccpet_traslados WHERE vigencia = '$vigencia' AND bpim != '' AND tipo = 'C'";
        $respTrasladosCredInv = mysqli_query($linkbd, $sqlrTrasladosCredInv);
        while($rowTrasladosInv = mysqli_fetch_row($respTrasladosCredInv)){
            $rubros = [];
            $nombreRubro = '';

            $nombreRubro = $rowTrasladosInv[0].'-'.$rowTrasladosInv[1].'-'.$rowTrasladosInv[2];
            array_push($rubros, $nombreRubro);
            array_push($rubrosTrasladosCredInv, $rubros);
        }

        $sqlrProyInv = "SELECT codigo, nombre FROM ccpproyectospresupuesto WHERE vigencia = '$vigencia'";
        $respProyInv = mysqli_query($linkbd, $sqlrProyInv);
        while($rowProyInv = mysqli_fetch_row($respProyInv)){
            $nomProy = [];
            array_push($nomProy, $rowProyInv[0]);
            array_push($nomProy, $rowProyInv[1]);
            array_push($nomProy, 'A');
            array_push($nombreRubrosInv, $nomProy);
        }

        $sqlrSectores = "SELECT codigo, nombre FROM ccpetsectores WHERE version = (SELECT MAX(version) FROM ccpetsectores)";
        $respSectores = mysqli_query($linkbd, $sqlrSectores);
        while($rowSectores = mysqli_fetch_row($respSectores)){
            $nomSec = [];
            array_push($nomSec, $rowSectores[0]);
            array_push($nomSec, $rowSectores[1]);
            array_push($nomSec, 'A');
            array_push($nombreRubrosInv, $nomSec);
        }

        $sqlrProgramas = "SELECT codigo, nombre FROM ccpetprogramas WHERE version = (SELECT MAX(version) FROM ccpetprogramas)";
        $respProgramas = mysqli_query($linkbd, $sqlrProgramas);
        while($rowProgramas = mysqli_fetch_row($respProgramas)){
            $nomProg = [];
            array_push($nomProg, $rowProgramas[0]);
            array_push($nomProg, $rowProgramas[1]);
            array_push($nomProg, 'A');
            array_push($nombreRubrosInv, $nomProg);
        }

        $sqlrProductos = "SELECT cod_producto, producto FROM ccpetproductos WHERE version = (SELECT MAX(version) FROM ccpetproductos) GROUP BY cod_producto";
        $respProductos = mysqli_query($linkbd, $sqlrProductos);
        while($rowProductos = mysqli_fetch_row($respProductos)){
            $nomProg = [];
            array_push($nomProg, $rowProductos[0]);
            array_push($nomProg, $rowProductos[1]);
            array_push($nomProg, 'A');
            array_push($nombreRubrosInv, $nomProg);
        }

        $sqlrProgramatico = "SELECT codigo_indicador, indicador_producto FROM ccpetproductos WHERE version = (SELECT MAX(version) FROM ccpetproductos)";
        $respProgramatico = mysqli_query($linkbd, $sqlrProgramatico);
        while($rowProgramatico = mysqli_fetch_row($respProgramatico)){
            $nomInd = [];
            array_push($nomInd, $rowProgramatico[0]);
            array_push($nomInd, $rowProgramatico[1]);
            array_push($nomInd, 'A');
            array_push($nombreRubrosInv, $nomInd);
        }


        $out['rubrosInicial'] = $rubrosInicial;
        $out['rubrosAd'] = $rubrosAd;
        $out['rubrosTrasladosCred'] = $rubrosTrasladosCred;
        $out['accounts'] = $accounts;
        $out['fuentes'] = $fuentes;
        $out['rubrosInv'] = $rubrosInv;
        $out['rubrosAdInv'] = $rubrosAdInv;
        $out['rubrosTrasladosCredInv'] = $rubrosTrasladosCredInv;
        $out['nombreRubrosInv'] = $nombreRubrosInv;
        
    }

    if($action == "buscaRubrosClasificador"){
        $clasificadorSelDet = [];

        if(isset($_POST['id_clasificador']) && $_POST['id_clasificador'] != ''){
            $id_clasificador = $_POST['id_clasificador'];
            $sqlr = "SELECT rubros, nombre, tipo FROM ccpet_clasificadorgastos_det WHERE id_clasificador = $id_clasificador AND tipo = 'C' ORDER BY rubros ASC";
            $res = mysqli_query($linkbd, $sqlr);
        
            while ($row = mysqli_fetch_row($res)) {
                array_push($clasificadorSelDet, $row);
            }
        }
        $out['clasificadorSelDet'] = $clasificadorSelDet;
    }

    if($action == 'guardarCab'){

        $user = $_SESSION['nickusu'];
        $nombre = $_POST['nombre_clasificador'];

        date_default_timezone_set('America/Bogota');
        $fecha_creacion = date("Y-m-d");
        $estado = 1; 

        $sql = "INSERT INTO ccpet_clasificadorgastos_cab(nombre, estado,user, fecha_creacion) VALUES ('$nombre', '$estado', '$user', '$fecha_creacion')";
        $res = mysqli_query($linkbd, $sql);
        $id_cab = mysqli_insert_id($linkbd);
    
        $out['id_cab'] =  $id_cab;
    }

    if($action == 'editarCab'){
        $id_clasificador = $_POST['id_clasificador'];
        $nombre = $_POST['nombre_clasificador'];

        $sqlr = "UPDATE ccpet_clasificadorgastos_cab SET nombre = '$nombre' WHERE id = $id_clasificador";
        mysqli_query($linkbd, $sqlr);
    }

    if($action == 'guardarDet'){

        for($x = 0; $x < count($_POST["detallesAd"]); $x++){
            $rubro = '';
            $nombre = '';
            $tipo = '';

            $rubro = $_POST["detallesAd"][$x][0];
            $nombre = $_POST["detallesAd"][$x][1];
            $tipo = $_POST["detallesAd"][$x][2];

            $sql = "INSERT INTO ccpet_clasificadorgastos_det(rubros, nombre, tipo, id_clasificador) VALUES ('$rubro', '$nombre','$tipo', '".$_POST["id_cab"]."')";
            $res = mysqli_query($linkbd, $sql);
        }
        $out['insertaBien'] = true;
    }

    if($action == 'editarDet'){

        $id_clasificador = $_POST['id_clasificador'];
    
        $sqlrD = "DELETE FROM ccpet_clasificadorgastos_det WHERE id_clasificador = $id_clasificador";
        mysqli_query($linkbd, $sqlrD);
        
        for($x = 0; $x < count($_POST["detallesAd"]); $x++){
            $rubro = '';
            $nombre = '';
            $tipo = '';

            $rubro = $_POST["detallesAd"][$x][0];
            $nombre = $_POST["detallesAd"][$x][1];
            $tipo = $_POST["detallesAd"][$x][2];

            $sql = "INSERT INTO ccpet_clasificadorgastos_det(rubros, nombre, tipo, id_clasificador) VALUES ('$rubro', '$nombre','$tipo', '".$id_clasificador."')";
            $res = mysqli_query($linkbd, $sql);
        }
        $out['insertaBien'] = true;
    }

    header("Content-type: application/json");
    echo json_encode($out);
    die();