
const URL = 'presupuesto_ccpet/clasificadores/ccp-buscaclasificadoresingresos.php';

const app = Vue.createApp({
    data() {
      return {
        noMember: false,
        search: {keyword: ''},
        results: [],
        results_det: [],
        show_table_search: false,
        show_resultados: false,
        show_results_det: false,
        myStyle: {
            backgroundColor: 'gray',
            color: 'white'
        },

        myStyleSelect: {
            backgroundColor: '#12DB67',
            color: 'white'
        },
        loading: false,
        id_results : '',
      }
    },

    mounted() {
        this.loading = false;
        this.getData();
    },

    computed: {
      
    },

    methods: {

        getData: function(){
            this.loading = true;
            axios.post(URL)
                .then((response) => {
                    this.results = response.data.clasificadores;
                    if(this.results == ''){
                        this.show_resultados = false;
                    }else{
                        this.show_resultados = true;
                    }
                }).catch((error) => {
                    this.error = true;
                    console.log(error)
                }).finally(() => {
                    this.loading = false;
                });
        },

        showClasificador(clasificador_cab){
            this.id_results = clasificador_cab[0];
            this.results_det = [];
            var formDataDet = new FormData();
            formDataDet.append("id_clasificador", clasificador_cab[0])
            axios.post(URL+'?action=search_clasificador_det', formDataDet)
                .then((response) => {
                    this.results_det = response.data.clasificadores_det;
                    if(response.data.clasificadores_det != ''){
                        this.show_results_det = true;
                    }else{
                        this.show_results_det = false;
                    }
                })
                .catch((error) => {
                    console.log("Error en la peticion!");
                    console.log(error);
                });
                setTimeout(() => {
                    document.getElementById("end_page").scrollIntoView({behavior: 'smooth'});   
                }, 50);
        },

        deleteClasificador(id_clasificador){
            Swal.fire({
                title: '¿Esta seguro de eliminar clasificador?',
                text: "Eliminar clasificador presupuestal de ingresos, confirmar campos!",
                icon: 'question',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Si, eliminar!'
            }).then((result) => {
                if (result.isConfirmed) {
                    var formData = new FormData();
                    formData.append("id_clasificador", id_clasificador);
                    axios.post(URL+'?action=delete', formData)
                    .then((response) => {
                        console.log(response)
                        this.show_results_det = false;
                    });
                    this.getData();
                }
            });
        },

        searchMonitor() {
            this.results_det = [];
            this.id_results = '';
            this.show_results_det = false;
            let keyword = this.toFormData(this.search);
            axios.post(URL+'?action=search', keyword)
                .then((response) => {
                    this.results = response.data.clasificadores;
                    if(response.data.clasificadores == ''){
                        this.show_resultados = false;
                    }
                    else{
                        this.show_resultados = true;
                    }                    
                });
        },

        estaEnArray(codigos){
            return this.id_results == codigos[0] ? true : false
        },

        

        toFormData(obj){
            var form_data = new FormData();
            for(var key in obj){
                form_data.append(key, obj[key]);
            }
            return form_data;
        },

        estadoClasificador(estado){
            return estado == 1 ? 'Activo' : 'Anulado'
        },

        editarClasificador(id_clasificador, name_clasificador){
            location.href="ccp-editargenerarclasificadoringresos.php?id_clasificador="+id_clasificador+"&name_clasificador="+name_clasificador;
        },
    }
})

app.mount('#myapp')