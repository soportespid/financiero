<?php

	require_once '../../comun.inc';
	require '../../funciones.inc';
 
	session_start();

	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");

	$maxVersion = ultimaVersionIngresosCCPET();

	$out = array('error' => false);

	$action = "show";

	if(isset($_GET['action'])){
		$action = $_GET['action'];
	}
	
	if($action == 'show'){

		$rubrosInicial = [];
		$accounts = [];
		$fuentes = [];

		$vigencia = $_POST['vigencia'];
		$base = $_POST['nomBase'];
		$usuario = $_POST['nomUsuario'];

		$linkmulti = conectar_Multi($base,$usuario);
		$linkmulti -> set_charset("utf8");

		$sqlrIni = "SELECT DISTINCT cuenta, fuente FROM ccpetinicialing WHERE vigencia = '".$vigencia."'";
		$resIni = mysqli_query($linkmulti, $sqlrIni);
		while($rowIni = mysqli_fetch_row($resIni)){
			$rubro = '';
			$rubroNom = [];
			$rubro = $rowIni[0]."-".$rowIni[1];
			array_push($rubroNom, $rubro);
			array_push($rubrosInicial, $rubroNom);
		}

		$sqlrAd = "SELECT DISTINCT cuenta, fuente FROM ccpet_adiciones WHERE vigencia = '".$vigencia."' AND cuenta != '' AND tipo_cuenta = 'I'";
		$respAd = mysqli_query($linkmulti, $sqlrAd);
		while($rowAd = mysqli_fetch_row($respAd)){
			$rubro = '';
			$rubroNom = [];
			$rubro = $rowAd[0]."-".$rowAd[1];
			array_push($rubroNom, $rubro);
			array_push($rubrosInicial, $rubroNom);
		}

		$sqlrEjecucion = "SELECT tsad.rubro, tsad.fuente FROM tesoreservas AS tsa, tesoreservas_det AS tsad WHERE tsa.estado = 'S' AND tsa.id = tsad.id_reserva AND tsa.vigencia = '".$vigencia."' UNION SELECT tsad.rubro, tsad.fuente FROM tesosuperavit AS tsa, tesosuperavit_det AS tsad WHERE tsa.estado = 'S' AND tsa.id = tsad.id_tesosuperavit AND tsa.vigencia = '".$vigencia."' UNION SELECT cuenta, fuente FROM pptorecibocajappto WHERE vigencia = '".$vigencia."' UNION SELECT cuenta, fuente FROM pptosinrecibocajappto WHERE vigencia = '".$vigencia."' UNION SELECT cuenta, fuente FROM pptonotasbanppto WHERE vigencia = '".$vigencia."' UNION SELECT cuenta, fuente FROM pptoingtranppto WHERE vigencia = '".$vigencia."' UNION SELECT cuenta, fuente FROM pptoretencionpago WHERE vigencia = '".$vigencia."' UNION SELECT cuenta, fuente FROM srv_recaudo_factura_ppto WHERE vigencia = '".$vigencia."'";

		$respEjecucion = mysqli_query($linkmulti, $sqlrEjecucion);
		while($rowEjecucion = mysqli_fetch_row($respEjecucion)){
			$rubro = '';
			$rubroNom = [];
			$rubro = $rowEjecucion[0]."-".$rowEjecucion[1];
			array_push($rubroNom, $rubro);
			array_push($rubrosInicial, $rubroNom);
		}


		$sqlrAccounts = "SELECT codigo, nombre, tipo, nivel FROM cuentasingresosccpet WHERE version = $maxVersion";
		$respAccounts = mysqli_query($linkmulti, $sqlrAccounts);
		while($rowAccounts = mysqli_fetch_row($respAccounts)){
			array_push($accounts, $rowAccounts);
		}

		$sqlrFuentes = "SELECT codigo_fuente, nombre FROM ccpet_fuentes_cuipo";
		$respFuentes = mysqli_query($linkmulti, $sqlrFuentes);
		while($rowFuentes = mysqli_fetch_row($respFuentes)){
			array_push($fuentes, $rowFuentes);
		}

		$out['rubrosInicial'] = $rubrosInicial;
		$out['accounts'] = $accounts;
		$out['fuentes'] = $fuentes;

	}

	if($action == "buscaRubrosClasificador"){
		$clasificadorSelDet = [];

		if(isset($_POST['id_clasificador']) && $_POST['id_clasificador'] != ''){
			$id_clasificador = $_POST['id_clasificador'];
			$sqlr = "SELECT rubros, nombre, tipo FROM ccpet_clasificadoringresos_det WHERE id_clasificador = $id_clasificador AND tipo = 'C' ORDER BY rubros ASC";
			$res = mysqli_query($linkbd, $sqlr);
		
			while ($row = mysqli_fetch_row($res)) {
				array_push($clasificadorSelDet, $row);
			}
		}
		$out['clasificadorSelDet'] = $clasificadorSelDet;
	}

	if($action == 'guardarCab'){

		$user = $_SESSION['nickusu'];
		$nombre = $_POST['nombre_clasificador'];

		date_default_timezone_set('America/Bogota');
		$fecha_creacion = date("Y-m-d");
		$estado = 1; 

		$sql = "INSERT INTO ccpet_clasificadoringresos_cab(nombre, estado,user, fecha_creacion) VALUES ('$nombre', '$estado', '$user', '$fecha_creacion')";
		$res = mysqli_query($linkbd, $sql);
		$id_cab = mysqli_insert_id($linkbd);
	
		$out['id_cab'] =  $id_cab;
	}

	if($action == 'editarCab'){
		$id_clasificador = $_POST['id_clasificador'];
		$nombre = $_POST['nombre_clasificador'];

		$sqlr = "UPDATE ccpet_clasificadoringresos_cab SET nombre = '$nombre' WHERE id = $id_clasificador";
		mysqli_query($linkbd, $sqlr);
	}

	if($action == 'guardarDet'){

		for($x = 0; $x < count($_POST["detallesAd"]); $x++){
			$rubro = '';
			$nombre = '';
			$tipo = '';

			$rubro = $_POST["detallesAd"][$x][0];
			$nombre = $_POST["detallesAd"][$x][1];
			$tipo = $_POST["detallesAd"][$x][2];

			$sql = "INSERT INTO ccpet_clasificadoringresos_det(rubros, nombre, tipo, id_clasificador) VALUES ('$rubro', '$nombre','$tipo', '".$_POST["id_cab"]."')";
			$res = mysqli_query($linkbd, $sql);
		}

		$out['insertaBien'] = true;

	}

	if($action == 'editarDet'){

		$id_clasificador = $_POST['id_clasificador'];
	
		$sqlrD = "DELETE FROM ccpet_clasificadoringresos_det WHERE id_clasificador = $id_clasificador";
		mysqli_query($linkbd, $sqlrD);
		
		for($x = 0; $x < count($_POST["detallesAd"]); $x++){
			$rubro = '';
			$nombre = '';
			$tipo = '';

			$rubro = $_POST["detallesAd"][$x][0];
			$nombre = $_POST["detallesAd"][$x][1];
			$tipo = $_POST["detallesAd"][$x][2];

			$sql = "INSERT INTO ccpet_clasificadoringresos_det(rubros, nombre, tipo, id_clasificador) VALUES ('$rubro', '$nombre','$tipo', '".$id_clasificador."')";
			$res = mysqli_query($linkbd, $sql);
		}

		$out['insertaBien'] = true;
	}

	if($action == 'validaCuenta'){
		
		$numcuenta = $_POST['numCuenta'];
		$base = $_POST['nomBase'];
		$usuario = $_POST['nomUsuario'];

		$linkmulti = conectar_Multi($base,$usuario);
		$linkmulti -> set_charset("utf8");

		$sql1 = "SELECT detalle_sectorial FROM ccpecuentastiponormas_cab WHERE estado = 'S' AND cuenta = '$numcuenta'";
		$res1 = mysqli_query($linkmulti, $sql1);
		$banNuevo1 = mysqli_num_rows($res1);
		$row1 = mysqli_fetch_row($res1);
		if($banNuevo1 > 0){
			$detSectorial = $row1[0];
		}else{
			$detSectorial = 0;
		}

		$detCuenta = array();
		$sql2 = "SELECT aplicade, tiponorma, numeronorma, fechanorma, fuentes, porcentaje FROM ccpecuentastiponormas WHERE estado = 'S' AND cuenta = '$numcuenta'";
		$res2 = mysqli_query($linkmulti, $sql2);
		$banNuevo2 = mysqli_num_rows($res2);
		while($row2 = mysqli_fetch_row($res2)){
			$datos = array();
			array_push($datos, $numcuenta);//cuenta
			array_push($datos, $detSectorial);//detalle sectorial
			array_push($datos, $row2[0]);//aplica destinación especifica
			array_push($datos, $row2[1]);//tipo norma
			array_push($datos, $row2[2]);//numero norma
			array_push($datos, $row2[3]);//fecha norma
			array_push($datos, $row2[4]);//fuente
			array_push($datos, $row2[5]);//porcentaje
			array_push($detCuenta, $datos);
		}
		if($banNuevo2 == 0){
			$datos = array();
			array_push($datos, $numcuenta);//cuenta
			array_push($datos, $detSectorial);//detalle sectorial
			array_push($datos, '');//aplica destinación especifica
			array_push($datos, '');//tipo norma
			array_push($datos, '');//numero norma
			array_push($datos, '');//fecha norma
			array_push($datos, '');//fuente
			array_push($datos, '');//porcentaje
			array_push($detCuenta, $datos);
		}
		$out['detCuenta'] = $detCuenta;
		$out['detSectorial'] = $detSectorial;
	}

	if($action == 'guardarT'){

		$base = $_POST['nomBase'];
		$usuario = $_POST['nomUsuario'];
		$linkmulti = conectar_Multi($base,$usuario);
		$linkmulti -> set_charset("utf8");

		$vDetalleSec = $_POST['DetalleSec'];
		$vDestinacion = $_POST['Destinacion'];
		$vTipoNorma = $_POST['TipoNorma'];
		$vfuente = $_POST['fuente'];
		$vNumNorma = $_POST['NumNorma'];
		$vporcentaje = $_POST['porcentaje'];
		$vtipovalor = $_POST['tipovalor'];
		$vinfoCuenta = explode("-", $_POST['infoCuenta']);
		preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST['fechnorma'],$fecha);
		$vfechnorma = "$fecha[3]-$fecha[2]-$fecha[1]";

		if($vDetalleSec != ''){

			$sql1 = "DELETE FROM `ccpecuentastiponormas_cab` WHERE cuenta = '$vinfoCuenta[0]' AND fuenteid = '$vinfoCuenta[1]'";
			$res1 = mysqli_query($linkmulti, $sql1);

			$sql2 = "INSERT INTO ccpecuentastiponormas_cab(cuenta, detalle_sectorial, estado,fuenteid) VALUES ('$vinfoCuenta[0]','$vDetalleSec','S','$vinfoCuenta[1]')";
			$res2 = mysqli_query($linkmulti, $sql2);
			$row2 = mysqli_fetch_row($res2);
		}
		if($vDestinacion == 'SI'){

			$sql1 = "DELETE FROM `ccpecuentastiponormas` WHERE cuenta = '$vinfoCuenta[0]' AND fuenteid = '$vinfoCuenta[1]' AND fuentes = '$vfuente'";
			$res1 = mysqli_query($linkmulti, $sql1);
			
			if($vDetalleSec != ''){

				$sql2 = "INSERT INTO ccpecuentastiponormas(cuenta, aplicade, tiponorma, numeronorma, fechanorma, estado, detalle_sectorial, fuentes, porcentaje, fuenteid, tipovalor) VALUES ('$vinfoCuenta[0]','$vDestinacion','$vTipoNorma','$vNumNorma','$vfechnorma','S','$vDetalleSec','$vfuente','$vporcentaje', '$vinfoCuenta[1]', '$vtipovalor')";
				$res2 = mysqli_query($linkmulti, $sql2);
				$row2 = mysqli_fetch_row($res2);

			}else{

				$sql2 = "INSERT INTO ccpecuentastiponormas(cuenta, aplicade, tiponorma, numeronorma, fechanorma, estado, detalle_sectorial, fuentes, porcentaje, fuenteid, tipovalor) VALUES ('$vinfoCuenta[0]','$vDestinacion','$vTipoNorma','$vNumNorma','$vfechnorma','S','0','$vfuente','$vporcentaje', '$vinfoCuenta[1]', '$vtipovalor')";
				$res2 = mysqli_query($linkmulti, $sql2);
				$row2 = mysqli_fetch_row($res2);
			}
		}
		$out['atguardar'] = '1';
	}

	header("Content-type: application/json");
	echo json_encode($out);
	die();