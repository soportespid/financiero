<?php

    require_once '../../comun.inc';
    require '../../funciones.inc';
 
    session_start();

	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");

    $maxVersion = ultimaVersionIngresosCCPET();

    $out = array('error' => false);

    $action = "show";

    if(isset($_GET['action'])){
        $action = $_GET['action'];
    }
    
    if($action == 'show'){

        $rubrosInicial = [];
        $accounts = [];
        $fuentes = [];

        $vigencia = $_POST['vigencia'];

        $sqlrIni = "SELECT DISTINCT cuenta, fuente FROM ccpetinicialing WHERE vigencia = '".$vigencia."'";
        $resIni = mysqli_query($linkbd, $sqlrIni);
        while($rowIni = mysqli_fetch_row($resIni)){
            $rubro = '';
            $rubroNom = [];
            $rubro = $rowIni[0]."*".$rowIni[1];
            /* $nombre = buscacuentaccpetgastos($rowIni[0], $maxVersion)." - ".buscaNombreFuenteCCPET($rowIni[1]); */
            array_push($rubroNom, $rubro);
            array_push($rubrosInicial, $rubroNom);
        }

        $sqlrAd = "SELECT DISTINCT cuenta, fuente FROM ccpet_adiciones WHERE vigencia = '".$vigencia."' AND cuenta != '' AND tipo_cuenta = 'I'";
        $respAd = mysqli_query($linkbd, $sqlrAd);
        while($rowAd = mysqli_fetch_row($respAd)){
            $rubro = '';
            $rubroNom = [];
            $rubro = $rowAd[0]."*".$rowAd[1];
            /* $nombre = buscacuentaccpetgastos($rowAd[0], $maxVersion)." - ".buscaNombreFuenteCCPET($rowAd[1]); */
            array_push($rubroNom, $rubro);
            array_push($rubrosInicial, $rubroNom);
        }

        $sqlrEjecucion = "SELECT tsad.rubro, tsad.fuente FROM tesoreservas AS tsa, tesoreservas_det AS tsad WHERE tsa.estado = 'S' AND tsa.id = tsad.id_reserva AND tsa.vigencia = '".$vigencia."' UNION SELECT tsad.rubro, tsad.fuente FROM tesosuperavit AS tsa, tesosuperavit_det AS tsad WHERE tsa.estado = 'S' AND tsa.id = tsad.id_tesosuperavit AND tsa.vigencia = '".$vigencia."' UNION SELECT cuenta, fuente FROM pptorecibocajappto WHERE vigencia = '".$vigencia."' UNION SELECT cuenta, fuente FROM pptosinrecibocajappto WHERE vigencia = '".$vigencia."' UNION SELECT cuenta, fuente FROM pptonotasbanppto WHERE vigencia = '".$vigencia."' UNION SELECT cuenta, fuente FROM pptoingtranppto WHERE vigencia = '".$vigencia."' UNION SELECT cuenta, fuente FROM pptoretencionpago WHERE vigencia = '".$vigencia."' UNION SELECT cuenta, fuente FROM srv_recaudo_factura_ppto WHERE vigencia = '".$vigencia."'";

        $respEjecucion = mysqli_query($linkbd, $sqlrEjecucion);
        while($rowEjecucion = mysqli_fetch_row($respEjecucion)){
            $rubro = '';
            $rubroNom = [];
            $rubro = $rowEjecucion[0]."*".$rowEjecucion[1];
            array_push($rubroNom, $rubro);
            array_push($rubrosInicial, $rubroNom);
        }


        $sqlrAccounts = "SELECT 
                            IF(codigo_auxiliar IS NULL OR codigo_auxiliar = '', codigo, CONCAT(codigo, '-', codigo_auxiliar)) AS codigo, 
                            IF(codigo_auxiliar IS NULL OR codigo_auxiliar = '', nombre, CONCAT(nombre, '-', nombre_auxiliar)) AS nombre,
                            tipo, nivel FROM cuentasingresosccpet WHERE version = $maxVersion";
        $respAccounts = mysqli_query($linkbd, $sqlrAccounts);
        while($rowAccounts = mysqli_fetch_row($respAccounts)){
            array_push($accounts, $rowAccounts);
        }

        $sqlrFuentes = "SELECT codigo_fuente, nombre FROM ccpet_fuentes_cuipo";
        $respFuentes = mysqli_query($linkbd, $sqlrFuentes);
        while($rowFuentes = mysqli_fetch_row($respFuentes)){
            array_push($fuentes, $rowFuentes);
        }

        $out['rubrosInicial'] = $rubrosInicial;
        $out['accounts'] = $accounts;
        $out['fuentes'] = $fuentes;

    }

    if($action == 'show1'){

        $rubrosInicial = [];
        $accounts = [];
        $fuentes = [];

        $vigencia = $_POST['vigencia'];

        $sqlrIni = "SELECT DISTINCT cuenta, fuente FROM ccpetinicialing WHERE vigencia = '".$vigencia."'";
        $resIni = mysqli_query($linkbd, $sqlrIni);
        while($rowIni = mysqli_fetch_row($resIni)){
            $rubro = '';
            $rubroNom = [];
            $rubro = $rowIni[0]."-".$rowIni[1];
            /* $nombre = buscacuentaccpetgastos($rowIni[0], $maxVersion)." - ".buscaNombreFuenteCCPET($rowIni[1]); */
            array_push($rubroNom, $rubro);
            array_push($rubrosInicial, $rubroNom);
        }

        $sqlrAd = "SELECT DISTINCT cuenta, fuente FROM ccpet_adiciones WHERE vigencia = '".$vigencia."' AND cuenta != '' AND tipo_cuenta = 'I'";
        $respAd = mysqli_query($linkbd, $sqlrAd);
        while($rowAd = mysqli_fetch_row($respAd)){
            $rubro = '';
            $rubroNom = [];
            $rubro = $rowAd[0]."-".$rowAd[1];
            /* $nombre = buscacuentaccpetgastos($rowAd[0], $maxVersion)." - ".buscaNombreFuenteCCPET($rowAd[1]); */
            array_push($rubroNom, $rubro);
            array_push($rubrosInicial, $rubroNom);
        }

        $sqlrEjecucion = "SELECT tsad.rubro, tsad.fuente FROM tesoreservas AS tsa, tesoreservas_det AS tsad WHERE tsa.estado = 'S' AND tsa.id = tsad.id_reserva AND tsa.vigencia = '".$vigencia."' UNION SELECT tsad.rubro, tsad.fuente FROM tesosuperavit AS tsa, tesosuperavit_det AS tsad WHERE tsa.estado = 'S' AND tsa.id = tsad.id_tesosuperavit AND tsa.vigencia = '".$vigencia."' UNION SELECT cuenta, fuente FROM pptorecibocajappto WHERE vigencia = '".$vigencia."' UNION SELECT cuenta, fuente FROM pptosinrecibocajappto WHERE vigencia = '".$vigencia."' UNION SELECT cuenta, fuente FROM pptonotasbanppto WHERE vigencia = '".$vigencia."' UNION SELECT cuenta, fuente FROM pptoingtranppto WHERE vigencia = '".$vigencia."' UNION SELECT cuenta, fuente FROM pptoretencionpago WHERE vigencia = '".$vigencia."' UNION SELECT cuenta, fuente FROM srv_recaudo_factura_ppto WHERE vigencia = '".$vigencia."'";

        $respEjecucion = mysqli_query($linkbd, $sqlrEjecucion);
        while($rowEjecucion = mysqli_fetch_row($respEjecucion)){
            $rubro = '';
            $rubroNom = [];
            $rubro = $rowEjecucion[0]."-".$rowEjecucion[1];
            array_push($rubroNom, $rubro);
            array_push($rubrosInicial, $rubroNom);
        }


        $sqlrAccounts = "SELECT 
                            IF(codigo_auxiliar IS NULL OR codigo_auxiliar = '', codigo, CONCAT(codigo, '-', codigo_auxiliar)) AS codigo, 
                            IF(codigo_auxiliar IS NULL OR codigo_auxiliar = '', nombre, CONCAT(nombre, '-', nombre_auxiliar)) AS nombre,
                            tipo, nivel FROM cuentasingresosccpet WHERE version = $maxVersion";
        $respAccounts = mysqli_query($linkbd, $sqlrAccounts);
        while($rowAccounts = mysqli_fetch_row($respAccounts)){
            array_push($accounts, $rowAccounts);
        }

        $sqlrFuentes = "SELECT codigo_fuente, nombre FROM ccpet_fuentes_cuipo";
        $respFuentes = mysqli_query($linkbd, $sqlrFuentes);
        while($rowFuentes = mysqli_fetch_row($respFuentes)){
            array_push($fuentes, $rowFuentes);
        }

        $out['rubrosInicial'] = $rubrosInicial;
        $out['accounts'] = $accounts;
        $out['fuentes'] = $fuentes;

    }

    if($action == "buscaRubrosClasificador"){
        $clasificadorSelDet = [];

        if(isset($_POST['id_clasificador']) && $_POST['id_clasificador'] != ''){
            $id_clasificador = $_POST['id_clasificador'];
            $sqlr = "SELECT rubros, nombre, tipo FROM ccpet_clasificadoringresos_det WHERE id_clasificador = $id_clasificador AND tipo = 'C' ORDER BY rubros ASC";
            $res = mysqli_query($linkbd, $sqlr);
        
            while ($row = mysqli_fetch_row($res)) {
                array_push($clasificadorSelDet, $row);
            }
        }
        $out['clasificadorSelDet'] = $clasificadorSelDet;
    }

    if($action == 'guardarCab'){

        $user = $_SESSION['nickusu'];
        $nombre = $_POST['nombre_clasificador'];

        date_default_timezone_set('America/Bogota');
        $fecha_creacion = date("Y-m-d");
        $estado = 1; 

        $sql = "INSERT INTO ccpet_clasificadoringresos_cab(nombre, estado,user, fecha_creacion) VALUES ('$nombre', '$estado', '$user', '$fecha_creacion')";
        $res = mysqli_query($linkbd, $sql);
        $id_cab = mysqli_insert_id($linkbd);
    
        $out['id_cab'] =  $id_cab;
    }

    if($action == 'editarCab'){
        $id_clasificador = $_POST['id_clasificador'];
        $nombre = $_POST['nombre_clasificador'];

        $sqlr = "UPDATE ccpet_clasificadoringresos_cab SET nombre = '$nombre' WHERE id = $id_clasificador";
        mysqli_query($linkbd, $sqlr);
    }

    if($action == 'guardarDet'){

        for($x = 0; $x < count($_POST["detallesAd"]); $x++){
            $rubro = '';
            $nombre = '';
            $tipo = '';

            $rubro = $_POST["detallesAd"][$x][0];
            $nombre = $_POST["detallesAd"][$x][1];
            $tipo = $_POST["detallesAd"][$x][2];

            $sql = "INSERT INTO ccpet_clasificadoringresos_det(rubros, nombre, tipo, id_clasificador) VALUES ('$rubro', '$nombre','$tipo', '".$_POST["id_cab"]."')";
            $res = mysqli_query($linkbd, $sql);
        }

        $out['insertaBien'] = true;

    }

    if($action == 'editarDet'){

        $id_clasificador = $_POST['id_clasificador'];
    
        $sqlrD = "DELETE FROM ccpet_clasificadoringresos_det WHERE id_clasificador = $id_clasificador";
        mysqli_query($linkbd, $sqlrD);
        
        for($x = 0; $x < count($_POST["detallesAd"]); $x++){
            $rubro = '';
            $nombre = '';
            $tipo = '';

            $rubro = $_POST["detallesAd"][$x][0];
            $nombre = $_POST["detallesAd"][$x][1];
            $tipo = $_POST["detallesAd"][$x][2];

            $sql = "INSERT INTO ccpet_clasificadoringresos_det(rubros, nombre, tipo, id_clasificador) VALUES ('$rubro', '$nombre','$tipo', '".$id_clasificador."')";
            $res = mysqli_query($linkbd, $sql);
        }

        $out['insertaBien'] = true;
    }

    header("Content-type: application/json");
    echo json_encode($out);
    die();