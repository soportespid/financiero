
const URL = 'presupuesto_ccpet/clasificadores/ccp-parametrizaringresoscuipo.php';
import { ordenarArbol, buscaNombreRubros, filtroEnArrayDeObjetos } from './../../funciones.js'

const app = Vue.createApp({
	data() {
		return {
		results: [],
			results_seleccionados: '',
			results_seleccionadosOrd: [],
			show_table_search: false,
			show_resultados: true,
			show_resultados_seleccionados: false,
			show_destEspecifica: false,
			get_code: [],
			get_code_1: [],
			myStyle: {
				backgroundColor: '#12DB67',
				color: 'white'
			},
			myStyleMayor: {
				'font-weight': 'bold',
			},
			name_clasificador: '',

			loading: false,
			rubrosInicial: [],
			rubrosJoin: [],
			accounts: [],
			accountsAux: [],
			accountsMayor: [],
			fuentes: [],
			arbol: [],
			arbolInv: [],
			buscar_rubro: '',
			resultsBuscar: [],
			vigencia: 0,
			years: [],
			SecPresupuestal: '',
			SecPresup: [],
			detSectorial: '0',
			detSecto: [],
			destEspecifica: 'NO',
			tiponorma: '0',
			tNorma: [],
			numFuente:'',
			nFuente: [],
			numeroNorma: '',
			valorPorcentaje: '',
			tipoValor:'0',
			checkTodo: false,
		}
	},

	async mounted() {
		this.loading = false;
		await this.cargayears();
		await this.cargarSecPresup();
		this.cargarParametros();
		await this.cargardetSectorial();
		await this.cargartipoNorma();
		await this.cargarfuentes();
	},

	computed: {
		mostrarCheck(){
			return this.buscar_rubro.length > 2 ? true : false
		},
	},

	methods: {
		cargayears: async function(){
			await axios.post('vue/presupuesto_ccp/ccp-reportecuipo.php?buscar=years')
			.then(
				(response)=>{
					this.years = response.data.anio;
					let idanio = response.data.anio.length -1;
					if (idanio >= 0){
						this.vigencia = response.data.anio[idanio][0];
					}else{
						this.vigencia = '';
					}
				}
			);
		},

		cargarSecPresup: async function(){
			await axios.post('vue/presupuesto_ccp/ccp-reportecuipo.php?buscar=secpresu')
			.then(
				(response)=>{
					this.SecPresup =  response.data.Secpre;
					let idSecPresu = response.data.Secpre.length;
					for(var x = 0; x < idSecPresu; x++){
						if(response.data.Secpre[x][4] == 'IN'){
							this.SecPresupuestal = response.data.Secpre[x][5];
						}
					}
				}

			);
		},

		cargardetSectorial: async function(){
			await axios.post('vue/presupuesto_ccp/ccp-reportecuipo.php?buscar=detSecto')
			.then(
				(response)=>{
					this.detSecto =  response.data.detSector;
				}
			);
		},

		cargartipoNorma: async function(){
			await axios.post('vue/presupuesto_ccp/ccp-reportecuipo.php?buscar=tnorma')
			.then(
				(response)=>{
					this.tNorma =  response.data.dettNorma;
				}
			);
		},

		cargarfuentes: async function(){
			await axios.post('vue/presupuesto_ccp/ccp-reportecuipo.php?buscar=nfuentes')
			.then(
				(response)=>{
					this.nFuente =  response.data.detfuente;
				}
			);
		},

		guardarinfo: function(){
			Swal.fire({
				icon: 'question',
				title: '¿Seguro que quieres guardar la información?',
				showDenyButton: true,
				confirmButtonText: 'Guardar',
				confirmButtonColor: '#01CC42',
				denyButtonText: 'Cancelar',
				denyButtonColor: '#FF121A',
			}).then(
				(result) => {
					if (result.isConfirmed){
						this.validadGuardar();	
					}
					else if (result.isDenied){
						Swal.fire({
							icon: 'info',
							title: 'No se guardo la información',
							confirmButtonText: 'Continuar',
							confirmButtonColor: '#FF121A',
							timer: 2500
						});
					}
				}
			)
		},

		validadGuardar(){
			let iniciaguardado = '0';
			if(this.detSectorial == '0' && this.destEspecifica == 'NO'){
				iniciaguardado = '1';
			}
			if(this.detSectorial != '0' && this.destEspecifica == 'NO'){
				iniciaguardado = '1';
			}
			if(this.destEspecifica == 'SI'){
				if(this.tiponorma != '0'){
					if(this.numFuente != ''){
						if(this.numeroNorma != ''){
							if(document.getElementById('fecha').value != ''){
								if(this.valorPorcentaje != ''){
									iniciaguardado = '1';
								}else{
									Swal.fire({
										icon: 'error',
										title: 'Error!',
										text: 'Falta ingresar valor o porcentaje',
										confirmButtonText: 'Continuar',
										confirmButtonColor: '#FF121A',
										timer: 2500
									});
									iniciaguardado = '0';
								}
							}else{
								Swal.fire({
									icon: 'error',
									title: 'Error!',
									text: 'Falta ingresar fecha de la norma',
									confirmButtonText: 'Continuar',
									confirmButtonColor: '#FF121A',
									timer: 2500
								});
								iniciaguardado = '0';
							}
						}else{
							Swal.fire({
								icon: 'error',
								title: 'Error!',
								text: 'Falta ingresar numero de norma',
								confirmButtonText: 'Continuar',
								confirmButtonColor: '#FF121A',
								timer: 2500
							});
							iniciaguardado = '0';
						}
					}else{
						Swal.fire({
							icon: 'error',
							title: 'Error!',
							text: 'Falta ingresar fuente',
							confirmButtonText: 'Continuar',
							confirmButtonColor: '#FF121A',
							timer: 2500
						});
						iniciaguardado = '0';
					}
				}else{
					Swal.fire({
						icon: 'error',
						title: 'Error!',
						text: 'Falta ingresar tipo de norma',
						confirmButtonText: 'Continuar',
						confirmButtonColor: '#FF121A',
						timer: 2500
					});
					iniciaguardado = '0';
				}
			}
			if(iniciaguardado == '1'){
				this.procesoguardar();
			}
		},

		async procesoguardar(){
			let formData = new FormData();
			let URL2 = URL + '?action=guardarT';
			let numFecha = document.getElementById('fecha').value;
			let partes = this.SecPresupuestal.split('<->');

			formData.append("DetalleSec", this.detSectorial);
			formData.append("Destinacion", this.destEspecifica);
			formData.append("TipoNorma", this.tiponorma);
			formData.append("fuente", this.numFuente);
			formData.append("NumNorma", this.numeroNorma);
			formData.append("fechnorma", numFecha);
			formData.append("porcentaje", this.valorPorcentaje);
			formData.append("tipovalor", this.tipoValor);
			formData.append("infoCuenta", this.results_seleccionados);
			formData.append("nomBase", partes[1]);
			formData.append("nomUsuario", partes[2]);
			
			await axios.post(URL2, formData)
			.then((response) => {
				if(response.data.atguardar == '1'){
					Swal.fire({
						icon: 'success',
						title: 'Se ha Agregado con exito',
						showConfirmButton: true,
						confirmButtonText: 'Continuar',
						confirmButtonColor: '#01CC42',
						timer: 3500
					})
					this.cargarcuentas(this.results_seleccionados);
				}
			})

		},

		valida_destEspecifica: function(){
			if(this.destEspecifica ==  'SI'){
				this.show_destEspecifica = true
			}else{
				this.show_destEspecifica = false
			}
		},

		async cargarParametros(){

			this.loading = true;
			let partes = this.SecPresupuestal.split('<->');
			let formData = new FormData();
			formData.append("vigencia", this.vigencia);
			formData.append("codSecPresu", partes[0]);
			formData.append("nomBase", partes[1]);
			formData.append("nomUsuario", partes[2]);
			
			await axios.post(URL, formData)
			.then((response) => {
				/* if(response.data.rubrosInicial != null) */
				this.rubrosInicial = response.data.rubrosInicial;
				this.accounts = response.data.accounts;
				this.fuentes = response.data.fuentes;

			}).catch((error) => {
				this.error = true;
				console.log(error)
			}).finally(() => {
				this.loading = false;
				this.auxiliarMayor();
				this.orderAccounts();
			});
		},

		async orderAccounts(){

			const arrPuro = this.rubrosInicial.map((e) => {
				return e[0]
			});

			const dataArrSet = new Set(arrPuro);

			const dataArr = Array.from(dataArrSet);

			const nombresRubros = this.accountsAux.concat(this.fuentes);
			this.results = buscaNombreRubros(dataArr, nombresRubros);

			this.resultsBuscar = [...this.results];

		},

		async cargarcuentas(cuenta){
			let partes = this.SecPresupuestal.split('<->');
			let pncuentas = cuenta.split('-');
			let formData = new FormData();
			let URL2 = URL + '?action=validaCuenta';
			formData.append("numCuenta", pncuentas[0]);
			formData.append("codSecPresu", partes[0]);
			formData.append("nomBase", partes[1]);
			formData.append("nomUsuario", partes[2]);
			await axios.post(URL2, formData)
			.then((response) => {
				if(response.data.detCuenta != 0){
					this.results_seleccionadosOrd = response.data.detCuenta;
				}
			})
		},



		auxiliarMayor(){
			this.accountsAux = this.accounts.filter(element => element[2] == 'C');
			this.accountsMayor = this.accounts.filter(element => element[2] == 'A');
		},

		estaEnArray(codigos){
			const parsedobj = JSON.stringify(this.results_seleccionados);
			return parsedobj.includes(codigos[0])
		},

		removeItemFromArr(item){

			let i = this.results_seleccionados.indexOf( item );
			if ( i !== -1 ) {
				this.results_seleccionados.splice( i, 1 );
			}
			let item2 = item.split("-"); 
			console.log(item2[0]),
			console.log(this.results_seleccionadosOrd);
			let ii = this.results_seleccionadosOrd.findIndex(subarray => subarray.includes(item2[0]));console.log(ii)
			if ( ii !== -1 ) {
				this.results_seleccionadosOrd.splice( ii, 1 );
			}
		},

		async seleccionaCodigos(codigos){ 

			const parsedobj = JSON.stringify(this.results_seleccionados); 
			
			if(parsedobj.includes(codigos)){
				this.removeItemFromArr(codigos);
			}else{
				this.results_seleccionados = codigos;
				this.cargarcuentas(codigos);
			}

			this.show_resultados_seleccionados = this.results_seleccionados.length > 0 ? true : false;
		},

		seleccionaCodigosCheck(codigos){

			const parsedobj = JSON.stringify(this.results_seleccionados);

			if(parsedobj.includes(codigos[0])){
				this.removeItemFromArr(codigos);
			}else{
				this.results_seleccionados = codigos;
			}
		},

		async buscarRubro(){

			this.results = [];
			var text = this.buscar_rubro;
			const data = this.resultsBuscar;
			this.results = await filtroEnArrayDeObjetos({'data': data, 'text': text});

		},

		toFormData(obj){
			var form_data = new FormData();
			for(var key in obj){
				form_data.append(key, obj[key]);
			}
			return form_data;
		},

		selTodo(){
			let timerInterval
			Swal.fire({
				title: 'Buscando y organizando mayores de los rubros seleccionados!',
				html: '<b></b>',
				timer: 6000,
				timerProgressBar: true,
				didOpen: () => {
					Swal.showLoading()
					const b = Swal.getHtmlContainer().querySelector('b')
					timerInterval = setInterval(() => {
					b.textContent = Swal.getTimerLeft()
					}, 100)
				},
				willClose: () => {
					clearInterval(timerInterval)
				}
			}).then((result) => {
				this.checkTodo = !this.checkTodo;
				this.results.map(element => {
					this.seleccionaCodigosCheck(element);
				})
				//this.ordenarArbolCheck();
				/* Read more about handling dismissals below */
				if (result.dismiss === Swal.DismissReason.timer) {
					/* console.log('I was closed by the timer') */
				}
			})

		},

	}
})

app.mount('#myapp')