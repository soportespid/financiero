<?php

    require_once '../../comun.inc';
    require '../../funciones.inc';
 
    session_start();

	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");

    $maxVersion = ultimaVersionGastosCCPET();

    $out = array('error' => false);

    $action = "show";

    if(isset($_GET['action'])){
        $action = $_GET['action'];
    }
    
    if($action == 'show'){

        $sql = "SELECT * FROM ccpet_clasificadorgastos_cab ORDER BY id ASC";
        $res = mysqli_query($linkbd, $sql);
        $clasificadores = array();

        while ($row = mysqli_fetch_row($res)) {
            array_push($clasificadores, $row);
        }

        $out['clasificadores'] = $clasificadores;

    }

    if ($action == 'search') {
        $keyword = $_POST['keyword'];
        $sql = "SELECT * FROM ccpet_clasificadorgastos_cab WHERE concat_ws(' ', nombre) LIKE '%$keyword%'";
        $res = mysqli_query($linkbd, $sql);
        $clasificadores = array();
    
        while ($row = mysqli_fetch_row($res)) {
            array_push($clasificadores, $row);
        }
    
        $out['clasificadores'] = $clasificadores;
    }

    if ($action == 'search_clasificador_det') {
        $id_clasificador = $_POST['id_clasificador'];
        /* $sql = "SELECT PR.codigo, PR.nombre, PR.tipo FROM cuentasingresosccpet AS PR, cuentasingresosccpet_det AS DET WHERE PR.id = DET.id_cuentasingreso AND DET.id_clasificador = $id_clasificador AND PR.version = '$maxVersion' ORDER BY PR.codigo ASC"; */
        $sqlr = "SELECT rubros, nombre, tipo FROM ccpet_clasificadorgastos_det WHERE id_clasificador = $id_clasificador ORDER BY rubros ASC";
        $res = mysqli_query($linkbd, $sqlr);
        $clasificadores_det = array();
    
        while ($row = mysqli_fetch_row($res)) {
            array_push($clasificadores_det, $row);
        }
    
        $out['clasificadores_det'] = $clasificadores_det;
    }

    if ($action == 'search_clasificador_det_aux') {
        $id_clasificador = $_POST['id_clasificador'];
        /* $sql = "SELECT PR.codigo, PR.nombre, PR.tipo FROM cuentasingresosccpet AS PR, cuentasingresosccpet_det AS DET WHERE PR.id = DET.id_cuentasingreso AND DET.id_clasificador = $id_clasificador AND PR.version = '$maxVersion' ORDER BY PR.codigo ASC"; */
        $sqlr = "SELECT rubros FROM ccpet_clasificadorgastos_det WHERE id_clasificador = $id_clasificador AND tipo = 'C' ORDER BY rubros ASC";
        $res = mysqli_query($linkbd, $sqlr);
        $clasificadores_det = array();
    
        while ($row = mysqli_fetch_row($res)) {
            $rubros = [];
            $rubrosParts = explode("-", $row[0]);
            array_push($clasificadores_det, $rubrosParts);
        }
    
        $out['clasificadores_det'] = $clasificadores_det;
    }

    if($action == 'delete'){
        $id_clasificador=$_POST['id_clasificador'];   
        $query_delete_det = "DELETE FROM ccpet_clasificadorgastos_det WHERE id_clasificador = $id_clasificador";
        $query_delete_cab = "DELETE FROM ccpet_clasificadorgastos_cab WHERE id = $id_clasificador";
        mysqli_query($linkbd,$query_delete_det);
        mysqli_query($linkbd,$query_delete_cab);
    }

    header("Content-type: application/json");
    echo json_encode($out);
    die();