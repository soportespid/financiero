<?php
	require_once '../../comun.inc';
    require '../../funciones.inc';
    require '../../conversor.php';
    session_start();

	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");

    $maxVersion = ultimaVersionGastosCCPET();

    $out = array('error' => false);

    $action = "show";

    if(isset($_GET['action'])){
        $action = $_GET['action'];
    }

    function validaArray($arrayNuevo, $arrayBase)
    {
        for ($i=0; $i < count($arrayBase); $i++) { 
            
            if (
                $arrayNuevo[0] == $arrayBase[$i][3] &&
                $arrayNuevo[1] == $arrayBase[$i][2] &&
                $arrayNuevo[2] == $arrayBase[$i][1] &&
                $arrayNuevo[3] == $arrayBase[$i][6] &&
                $arrayNuevo[4] == $arrayBase[$i][5]
            ) {
                return $i;
            }
        }

        return -1;
    }

    function validaArrayCpc($arrayNuevo, $arrayBase)
    {
        for ($i=0; $i < count($arrayBase); $i++) {
            
            if (
                $arrayNuevo[0] == $arrayBase[$i][3] &&
                $arrayNuevo[1] == $arrayBase[$i][2] &&
                $arrayNuevo[2] == $arrayBase[$i][1] &&
                $arrayNuevo[3] == $arrayBase[$i][6] &&
                $arrayNuevo[4] == $arrayBase[$i][5] &&
                $arrayNuevo[6] == $arrayBase[$i][13]
            ){
                return $i;
            }
        }

        return -1;
    }

    function llenaBase($arrayBase, $index) {

        for ($i=0; $i < count($arrayBase); $i++){
            
            $arrayBase[$i][$index] = "0";
        }

        return $arrayBase;
    }

    function llenaBaseVacio($arrayBase, $index) {

        for ($i=0; $i < count($arrayBase); $i++) { 
            
            $arrayBase[$i][$index] = "";
        }

        return $arrayBase;
    }

    function array_sort_by(&$arrIni, $col, $order)
    {
        $arrAux = array();
        foreach ($arrIni as $key=> $row)
        {
            $arrAux[$key] = is_object($row) ? $arrAux[$key] = $row->$col : $row[$col];
            $arrAux[$key] = strtolower($arrAux[$key]);
        }
        array_multisort($arrAux, $order, $arrIni);
    }

    if ($action == "datosIniciales") {

        $seccionPresupuestal = array();

        $rubrosInicial = [];
        $cuentasFun = [];

        $vigencia = $_POST['vigencia'];

        $sqlr = "SELECT id_seccion_presupuestal, nombre FROM pptoseccion_presupuestal WHERE estado='S' ";
        $res = mysqli_query($linkbd, $sqlr);
        while ($row = mysqli_fetch_row($res)) {

            array_push($seccionPresupuestal, $row);
        }

        $vigenciaGasto = array();

        $sqlr = "SELECT codigo, nombre FROM ccpet_vigenciadelgasto ";
        $res = mysqli_query($linkbd, $sqlr);
        while ($row = mysqli_fetch_row($res)) {

            array_push($vigenciaGasto, $row);
        }


        $sqlrIni = "SELECT DISTINCT cuenta FROM ccpetinicialgastosfun WHERE vigencia = '".$vigencia."'";
        $resIni = mysqli_query($linkbd, $sqlrIni);
        while($rowIni = mysqli_fetch_row($resIni)){
            $rubro = '';
            $rubroNom = [];
            $rubro = $rowIni[0];
            array_push($rubroNom, $rubro);
            array_push($rubrosInicial, $rubroNom);
        }

        $sqlrAd = "SELECT DISTINCT cuenta FROM ccpet_adiciones WHERE vigencia = '".$vigencia."' AND cuenta != '' AND tipo_cuenta = 'G'";
        $respAd = mysqli_query($linkbd, $sqlrAd);
        while($rowAd = mysqli_fetch_row($respAd)){
            $rubro = '';
            $rubroNom = [];
            $rubro = $rowAd[0];
            array_push($rubroNom, $rubro);
            array_push($rubrosInicial, $rubroNom);
        }

        $sqlrTrasladosCred = "SELECT DISTINCT cuenta FROM ccpet_traslados WHERE vigencia = '".$vigencia."' AND cuenta != '' AND tipo = 'C'";
        $respTrasladosCred = mysqli_query($linkbd, $sqlrTrasladosCred);
        while($rowTrasladosCred = mysqli_fetch_row($respTrasladosCred)){
            $rubro = '';
            $rubroNom = [];
            $rubro = $rowTrasladosCred[0];
            array_push($rubroNom, $rubro);
            array_push($rubrosInicial, $rubroNom);
        }

        $sqlrAccounts = "SELECT codigo, nombre, tipo FROM cuentasccpet WHERE version = $maxVersion AND SUBSTRING(codigo, 1, 3) != '2.3'";
        $respAccounts = mysqli_query($linkbd, $sqlrAccounts);
        while($rowAccounts = mysqli_fetch_row($respAccounts)){
            array_push($cuentasFun, $rowAccounts);
        }

        $out['secPresupuestal'] = $seccionPresupuestal;
        $out['vigGasto'] = $vigenciaGasto;
        $out['rubrosInicial'] = $rubrosInicial;
        $out['cuentasFun'] = $cuentasFun;
    }

    if ($action == "datosFormulario") {

        /*
            Tipo
            A - Mayor
            C - Mayor de Captura
            D - captura CPC

            Orden array gastos

            0. Tipo de cuenta
            1. Vig. gasto
            2. Sec Presupuestal
            3. Codigo cuenta
            4. Nombre Cuenta
            5. Fuente
            6. Medio de pago
            7. Presupuesto inicial
            8. Adiciones
            9. Reducciones
            10. Credito
            11. ContraCredito
            12. Definitivo
            13. CPC
            14. Disponibilidad
            15. Compromisos
            16. Obligacion
            17. Egresos
            18. Saldo
            19. nombre fuente
            20. nombre CPC
        */

        $detalles = array();
        $maxVersion = ultimaVersionIngresosCCPET();

        preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_GET["fechaIni"], $fecha);
        $fechaIni = $fecha[3]."-".$fecha[2]."-".$fecha[1];

        $vigenciaIni = $fecha[3];

        preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_GET["fechaFin"], $fecha);
        $fechaFin = $fecha[3]."-".$fecha[2]."-".$fecha[1];

        $vigenciaFin = $fecha[3];

        $secPresu = "";
        $medioPago = "";
        $vigGasto = "";

        if ($_GET['secPresu'] == '') {
            $secPresu = "%";
        }
        else {
            $secPresu = $_GET['secPresu'];
        }

        if ($_GET['medioPago'] == '') {
            $medioPago = "%";
        }
        else {
            $medioPago = $_GET['medioPago'];
        }

        if ($_GET['vigGasto'] == '') {
            $vigGasto = "%";
        }
        else {
            $vigGasto = $_GET['vigGasto'];
        }

        $tamCuentas = count($_POST["cuentas"]);
        
        for($x = 0; $x < $tamCuentas; $x++){
            
            $xy = 0;
            $tamFuentes = count($_POST["fuentes"]);
            do{
                if ($_POST["cuentas"][$x][2] == 'A'){

                    $datos = array();
                    $presuInicial = 0;
                    $adicion = 0;
                    $reduccion = 0;
                    $credito = 0;
                    $contraCredito = 0;
                    $definitivo = 0;
                    $disponibilidad = 0;
                    $compromisos = 0;
                    $obligacion = 0;
                    $egresos = 0;
                    $saldo = 0;

                    $sqlInicial = "SELECT COALESCE(SUM(valor),0) FROM ccpetinicialgastosfun WHERE cuenta LIKE '".$_POST['cuentas'][$x][0]."%' AND seccion_presupuestal LIKE '$secPresu' AND vigencia_gasto LIKE '$vigGasto' AND medio_pago LIKE '$medioPago' AND (vigencia LIKE '$vigenciaIni' OR vigencia LIKE '$vigenciaFin') AND fuente LIKE '".$_POST["fuentes"][$xy]."%'";
                    $rowInicial = mysqli_fetch_row(mysqli_query($linkbd, $sqlInicial));/* echo $sqlInicial."\n \n"; */

                    $sqlAdicion = "SELECT COALESCE(SUM(valor),0) FROM ccpet_adiciones WHERE cuenta LIKE '".$_POST['cuentas'][$x][0]."%' AND tipo_cuenta = 'G' AND seccion_presupuestal LIKE '$secPresu' AND codigo_vigenciag LIKE '$vigGasto' AND medio_pago LIKE '$medioPago' AND fecha BETWEEN '$fechaIni' AND '$fechaFin' AND fuente LIKE '".$_POST["fuentes"][$xy]."%'";
                    $rowAdicion = mysqli_fetch_row(mysqli_query($linkbd, $sqlAdicion));

                    $sqlReduccion = "SELECT COALESCE(SUM(valor),0) FROM ccpet_reducciones WHERE cuenta LIKE '".$_POST['cuentas'][$x][0]."%' AND tipo_cuenta = 'G' AND seccion_presupuestal LIKE '$secPresu' AND medio_pago LIKE '$medioPago' AND codigo_vigenciag LIKE '$vigGasto' AND estado = 'S' AND fecha BETWEEN '$fechaIni' AND '$fechaFin' AND fuente LIKE '".$_POST["fuentes"][$xy]."%'";
                    $rowReduccion = mysqli_fetch_row(mysqli_query($linkbd, $sqlReduccion));

                    $sqlCredito = "SELECT COALESCE(SUM(valor),0) FROM ccpet_traslados WHERE cuenta LIKE '".$_POST['cuentas'][$x][0]."%' AND tipo = 'C' AND seccion_presupuestal LIKE '$secPresu' AND medio_pago LIKE '$medioPago' AND codigo_vigenciag LIKE '$vigGasto' AND estado = 'S' AND fecha BETWEEN '$fechaIni' AND '$fechaFin' AND fuente LIKE '".$_POST["fuentes"][$xy]."%'";
                    $rowCredito = mysqli_fetch_row(mysqli_query($linkbd, $sqlCredito));

                    $sqlContraCredito = "SELECT COALESCE(SUM(valor),0) FROM ccpet_traslados WHERE cuenta LIKE '".$_POST['cuentas'][$x][0]."%' AND tipo = 'R' AND seccion_presupuestal LIKE '$secPresu' AND medio_pago LIKE '$medioPago' AND codigo_vigenciag LIKE '$vigGasto' AND estado = 'S' AND fecha BETWEEN '$fechaIni' AND '$fechaFin' AND fuente LIKE '".$_POST["fuentes"][$xy]."%'";
                    $rowContraCredito = mysqli_fetch_row(mysqli_query($linkbd, $sqlContraCredito));

                    //cdp
                    $sqlCdp = "SELECT COALESCE(SUM(TB2.valor),0) FROM ccpetcdp AS TB1, ccpetcdp_detalle AS TB2 WHERE TB1.vigencia = TB2.vigencia AND TB1.consvigencia = TB2.consvigencia AND TB2.seccion_presupuestal LIKE '$secPresu' AND TB2.medio_pago LIKE '$medioPago' AND TB2.codigo_vigenciag LIKE '$vigGasto' AND TB2.cuenta LIKE '".$_POST['cuentas'][$x][0]."%' AND TB2.cuenta NOT LIKE '2.3%' AND TB1.tipo_mov = '201' AND TB2.tipo_mov = '201' AND TB1.fecha BETWEEN '$fechaIni' AND '$fechaFin' AND TB2.fuente LIKE '".$_POST["fuentes"][$xy]."%'";
                    
                    $rowCdp = mysqli_fetch_row(mysqli_query($linkbd, $sqlCdp));

                    $sqlCdpRev = "SELECT COALESCE(SUM(TB2.valor),0) FROM ccpetcdp AS TB1, ccpetcdp_detalle AS TB2 WHERE TB1.vigencia = TB2.vigencia AND TB1.consvigencia = TB2.consvigencia AND TB2.seccion_presupuestal LIKE '$secPresu' AND TB2.medio_pago LIKE '$medioPago' AND TB2.codigo_vigenciag LIKE '$vigGasto' AND TB2.cuenta LIKE '".$_POST['cuentas'][$x][0]."%' AND TB2.cuenta NOT LIKE '2.3%' AND TB1.tipo_mov LIKE '4%' AND TB2.tipo_mov LIKE '4%' AND TB1.fecha BETWEEN '$fechaIni' AND '$fechaFin' AND TB2.fuente LIKE '".$_POST["fuentes"][$xy]."%'";
                    $rowCdpRev = mysqli_fetch_row(mysqli_query($linkbd, $sqlCdpRev));

                    //rp
                    $sqlRp = "SELECT COALESCE(SUM(TB2.valor),0) FROM ccpetrp AS TB1, ccpetrp_detalle AS TB2 WHERE TB1.vigencia = TB2.vigencia AND TB1.consvigencia = TB2.consvigencia AND TB2.seccion_presupuestal LIKE '$secPresu' AND TB2.medio_pago LIKE '$medioPago' AND TB2.codigo_vigenciag LIKE '$vigGasto' AND TB2.cuenta LIKE '".$_POST['cuentas'][$x][0]."%' AND TB2.cuenta NOT LIKE '2.3%' AND TB1.tipo_mov = '201' AND TB2.tipo_mov = '201' AND TB1.fecha BETWEEN '$fechaIni' AND '$fechaFin' AND TB2.fuente LIKE '".$_POST["fuentes"][$xy]."%'";
                    $rowRp = mysqli_fetch_row(mysqli_query($linkbd, $sqlRp));

                    $sqlRpRev = "SELECT COALESCE(SUM(TB2.valor),0) FROM ccpetrp AS TB1, ccpetrp_detalle AS TB2 WHERE TB1.vigencia = TB2.vigencia AND TB1.consvigencia = TB2.consvigencia AND TB2.seccion_presupuestal LIKE '$secPresu' AND TB2.medio_pago LIKE '$medioPago' AND TB2.codigo_vigenciag LIKE '$vigGasto' AND TB2.cuenta LIKE '".$_POST['cuentas'][$x][0]."%' AND TB2.cuenta NOT LIKE '2.3%' AND TB1.tipo_mov LIKE '4%' AND TB2.tipo_mov LIKE '4%' AND TB1.fecha BETWEEN '$fechaIni' AND '$fechaFin' AND TB2.fuente LIKE '".$_POST["fuentes"][$xy]."%'";
                    $rowRpRev = mysqli_fetch_row(mysqli_query($linkbd, $sqlRpRev));

                    //cxp
                    $sqlCxp = "SELECT COALESCE(SUM(DET.valor),0) FROM tesoordenpago_det AS DET, tesoordenpago AS CAB WHERE CAB.id_orden = DET.id_orden AND CAB.fecha BETWEEN '$fechaIni' AND '$fechaFin' AND (DET.vigencia = '$vigenciaIni' OR DET.vigencia = '$vigenciaFin') AND DET.cuentap LIKE '".$_POST['cuentas'][$x][0]."%' AND DET.cuentap NOT LIKE '2.3%' AND DET.seccion_presupuestal LIKE '$secPresu' AND DET.medio_pago LIKE '$medioPago' AND DET.codigo_vigenciag LIKE '$vigGasto' AND CAB.estado != 'R' AND DET.fuente LIKE '".$_POST["fuentes"][$xy]."%'";
                    $rowCxp = mysqli_fetch_row(mysqli_query($linkbd, $sqlCxp));

                    //cxp nomina
                    $sqlCxpNomina = "SELECT COALESCE(SUM(HD.valor),0) FROM humnom_presupuestal AS HD, humnomina_aprobado AS HN WHERE HN.id_nom = HD.id_nom AND HN.fecha BETWEEN '$fechaIni' AND '$fechaFin' AND HN.estado!='N' AND HD.estado='P' AND HD.cuenta LIKE '".$_POST['cuentas'][$x][0]."%' AND HD.cuenta NOT LIKE '2.3%' AND HD.seccion_presupuestal LIKE '$secPresu' AND HD.vigencia_gasto LIKE '$vigGasto' AND HD.medio_pago LIKE '$medioPago' AND HD.fuente LIKE '".$_POST["fuentes"][$xy]."%'";
                    $rowCxpNomina = mysqli_fetch_row(mysqli_query($linkbd, $sqlCxpNomina));

                    //egreso
                    $sqlEgreso = "SELECT COALESCE(SUM(TD.valor),0) FROM tesoordenpago_det AS TD, tesoegresos AS TE WHERE (TD.vigencia = '$vigenciaIni' OR TD.vigencia = '$vigenciaFin') AND TE.fecha BETWEEN '$fechaIni' AND '$fechaFin' AND TE.estado != 'N' AND TE.estado != 'R' AND TD.id_orden = TE.id_orden AND TD.cuentap LIKE '".$_POST['cuentas'][$x][0]."%' AND TD.cuentap NOT LIKE '2.3%' AND TD.seccion_presupuestal LIKE '$secPresu' AND TD.codigo_vigenciag LIKE '$vigGasto' AND TD.medio_pago LIKE '$medioPago' AND TD.fuente LIKE '".$_POST["fuentes"][$xy]."%'";
                    $rowEgreso = mysqli_fetch_row(mysqli_query($linkbd, $sqlEgreso));

                    $sqlEgresoNomina = "SELECT COALESCE(SUM(TD.valordevengado),0) FROM tesoegresosnomina_det AS TD, tesoegresosnomina AS TE WHERE (TE.vigencia = '$vigenciaIni' OR TE.vigencia = '$vigenciaFin') AND TE.fecha BETWEEN '$fechaIni' AND '$fechaFin' AND TD.id_egreso = TE.id_egreso AND TE.estado = 'S' AND NOT(TD.tipo='SE' OR TD.tipo='PE' OR TD.tipo='DS' OR TD.tipo='RE' OR TD.tipo='FS') AND TD.seccion_presupuestal LIKE '$secPresu' AND TD.vigencia_gasto LIKE '$vigGasto' AND TD.medio_pago LIKE '$medioPago' AND TD.cuentap LIKE '".$_POST['cuentas'][$x][0]."%' AND TD.cuentap NOT LIKE '2.3%' AND TD.fuente LIKE '".$_POST["fuentes"][$xy]."%'";
                    $rowEgresoNomina = mysqli_fetch_row(mysqli_query($linkbd, $sqlEgresoNomina));

                    $presuInicial = $rowInicial[0];
                    $adicion = $rowAdicion[0];
                    $reduccion = $rowReduccion[0];
                    $credito = $rowCredito[0];
                    $contraCredito = $rowContraCredito[0];
                    $definitivo = $presuInicial + $adicion - $reduccion + $credito - $contraCredito;
                    $disponibilidad = $rowCdp[0] - $rowCdpRev[0];
                    $compromisos = $rowRp[0] - $rowRpRev[0];
                    $obligacion = $rowCxp[0] + $rowCxpNomina[0];
                    $egresos = $rowEgreso[0] + $rowEgresoNomina[0];
                    $saldo = $definitivo - $disponibilidad;


                    if ($definitivo > 0) {

                        array_push($datos, 'A');
                        array_push($datos, ""); //Vig. gasto
                        array_push($datos, ""); //Sec. presupuestal
                        array_push($datos, $_POST['cuentas'][$x][0]); //Codigo cuenta
                        array_push($datos, $_POST['cuentas'][$x][1]); //Nombre cuenta
                        array_push($datos, ""); //Fuente
                        array_push($datos, ""); //Medio de pago
                        array_push($datos, $presuInicial); //Presupuesto Inicial
                        array_push($datos, $adicion); //Adiciones
                        array_push($datos, $reduccion); //Reduccciones
                        array_push($datos, $credito); //credito
                        array_push($datos, $contraCredito); //contracredito
                        array_push($datos, $definitivo); //definitivo 
                        array_push($datos, ""); //CPC
                        array_push($datos, $disponibilidad); 
                        array_push($datos, $compromisos); 
                        array_push($datos, $obligacion); 
                        array_push($datos, $egresos); 
                        array_push($datos, $saldo); 

                        array_push($detalles, $datos);
                    }
                }
                else if ($_POST['cuentas'][$x][2] == 'C') {

                    $datosBase = array();

                    $sqlPresupuestoInicial = "SELECT fuente, seccion_presupuestal, vigencia_gasto, medio_pago, SUM(valor) FROM ccpetinicialgastosfun WHERE cuenta LIKE '".$_POST['cuentas'][$x][0]."' AND seccion_presupuestal LIKE '$secPresu' AND vigencia_gasto LIKE '$vigGasto' AND medio_pago LIKE '$medioPago' AND (vigencia = '$vigenciaIni' OR vigencia = '$vigenciaFin') AND fuente LIKE '".$_POST["fuentes"][$xy]."%' GROUP BY fuente, seccion_presupuestal, vigencia_gasto, medio_pago";
                    $resPresupuestoInicial = mysqli_query($linkbd, $sqlPresupuestoInicial);
                    while ($rowPresupuestoInicial = mysqli_fetch_row($resPresupuestoInicial)) {
                        
                        $datos2 = array();
                        
                        if ($rowPresupuestoInicial) {
                            
                            array_push($datos2, 'C');
                            array_push($datos2, $rowPresupuestoInicial[2]); //vigencia gasto
                            array_push($datos2, $rowPresupuestoInicial[1]); //seccion presupuestal
                            array_push($datos2, $_POST['cuentas'][$x][0]); //cuenta
                            array_push($datos2, $_POST['cuentas'][$x][1]); //Nombre cuenta
                            array_push($datos2, $rowPresupuestoInicial[0]); //fuente
                            array_push($datos2, $rowPresupuestoInicial[3]); //medio pago
                            array_push($datos2, $rowPresupuestoInicial[4]); //Valor inicial
                            
                            array_push($datosBase, $datos2);
                        }
                    }
                    //ADICIÓN

                    $adicion = array();

                    $sqlAdiciones = "SELECT fuente, seccion_presupuestal, codigo_vigenciag, medio_pago, SUM(valor) FROM ccpet_adiciones WHERE cuenta LIKE '".$_POST['cuentas'][$x][0]."' AND tipo_cuenta = 'G' AND seccion_presupuestal LIKE '$secPresu' AND medio_pago LIKE '$medioPago' AND codigo_vigenciag LIKE '$vigGasto' AND estado = 'S' AND fecha BETWEEN '$fechaIni' AND '$fechaFin' AND fuente LIKE '".$_POST["fuentes"][$xy]."%' GROUP BY fuente, seccion_presupuestal, codigo_vigenciag, medio_pago";
                    $resAdiciones = mysqli_query($linkbd, $sqlAdiciones);
                    while ($rowAdiciones = mysqli_fetch_row($resAdiciones)) {

                        $datos2 = array();

                        if ($rowAdiciones) {

                            array_push($datos2, $_POST['cuentas'][$x][0]); //cuenta
                            array_push($datos2, $rowAdiciones[1]); //seccion presupuestal
                            array_push($datos2, $rowAdiciones[2]); //vigencia gasto
                            array_push($datos2, $rowAdiciones[3]); //medio pago
                            array_push($datos2, $rowAdiciones[0]); //fuente
                            array_push($datos2, $rowAdiciones[4]); //valor

                            array_push($adicion, $datos2); 
                        }
                    }    

                    $datosBase = llenaBase($datosBase, 8);

                    for ($i=0; $i < count($adicion); $i++){
                        
                        $result = validaArray($adicion[$i], $datosBase);

                        if ($result == -1){
                            
                            $prueba = array();

                            array_push($prueba, 'C');
                            array_push($prueba, $adicion[$i][2]); //vigencia gasto
                            array_push($prueba, $adicion[$i][1]); //seccion presupuestal
                            array_push($prueba, $_POST['cuentas'][$x][0]); //cuenta
                            array_push($prueba, $_POST['cuentas'][$x][1]); //Nombre cuenta
                            array_push($prueba, $adicion[$i][4]); //fuente
                            array_push($prueba, $adicion[$i][3]); //medio pago
                            array_push($prueba, "0"); //Valor inicial
                            array_push($prueba, $adicion[$i][5]); //adicion
                            array_push($datosBase, $prueba);
                        }
                        else{
                            $datosBase[$result][8] = $adicion[$i][5];
                        }
                    }

                    //Reducción
                    $reduccion = array();

                    $sqlReduccion = "SELECT fuente, seccion_presupuestal, codigo_vigenciag, medio_pago, SUM(valor) FROM ccpet_reducciones WHERE cuenta LIKE '".$_POST['cuentas'][$x][0]."' AND tipo_cuenta = 'G' AND seccion_presupuestal LIKE '$secPresu' AND medio_pago LIKE '$medioPago' AND codigo_vigenciag LIKE '$vigGasto' AND estado = 'S' AND fecha BETWEEN '$fechaIni' AND '$fechaFin' AND fuente LIKE '".$_POST["fuentes"][$xy]."%' GROUP BY fuente, seccion_presupuestal, codigo_vigenciag, medio_pago";
                    $resReduccion = mysqli_query($linkbd, $sqlReduccion);
                    while ($rowReduccion = mysqli_fetch_row($resReduccion)){
                        
                        $datos2 = array();

                        if ($rowReduccion){
                            array_push($datos2, $_POST['cuentas'][$x][0]); //cuenta
                            array_push($datos2, $rowReduccion[1]); //seccion presupuestal
                            array_push($datos2, $rowReduccion[2]); //vigencia gasto
                            array_push($datos2, $rowReduccion[3]); //medio pago
                            array_push($datos2, $rowReduccion[0]); //fuente
                            array_push($datos2, $rowReduccion[4]); //valor

                            array_push($reduccion, $datos2); 
                        }
                    }

                    $datosBase = llenaBase($datosBase, 9);

                    for ($i=0; $i < count($reduccion); $i++){
                        
                        $result = validaArray($reduccion[$i], $datosBase);
                        
                        if ($result == -1){
                            
                            $prueba = array();

                            array_push($prueba, 'C');
                            array_push($prueba, $reduccion[$i][2]); //vigencia gasto
                            array_push($prueba, $reduccion[$i][1]); //seccion presupuestal
                            array_push($prueba, $_POST['cuentas'][$x][0]); //cuenta
                            array_push($prueba, $_POST['cuentas'][$x][1]); //Nombre cuenta
                            array_push($prueba, $reduccion[$i][4]); //fuente
                            array_push($prueba, $reduccion[$i][3]); //medio pago
                            array_push($prueba, "0"); //Valor inicial
                            array_push($prueba, "0"); //adicion
                            array_push($prueba, $reduccion[$i][5]); //reduccion
                            array_push($datosBase, $prueba);
                        }
                        else{
                            $datosBase[$result][9] = $reduccion[$i][5];
                        }
                    }

                    //Credito
                    $credito = array();

                    $sqlCredito = "SELECT fuente, seccion_presupuestal, codigo_vigenciag, medio_pago, SUM(valor) FROM ccpet_traslados WHERE cuenta LIKE '".$_POST['cuentas'][$x][0]."' AND tipo = 'C' AND seccion_presupuestal LIKE '$secPresu' AND medio_pago LIKE '$medioPago' AND codigo_vigenciag LIKE '$vigGasto' AND estado = 'S' AND fecha BETWEEN '$fechaIni' AND '$fechaFin' AND fuente LIKE '".$_POST["fuentes"][$xy]."%' GROUP BY fuente, seccion_presupuestal, codigo_vigenciag, medio_pago";
                    $resCredito = mysqli_query($linkbd, $sqlCredito);
                    while ($rowCredito = mysqli_fetch_row($resCredito)){
                        
                        $datos2 = array();

                        if ($rowCredito){
                            array_push($datos2, $_POST['cuentas'][$x][0]); //cuenta
                            array_push($datos2, $rowCredito[1]); //seccion presupuestal
                            array_push($datos2, $rowCredito[2]); //vigencia gasto
                            array_push($datos2, $rowCredito[3]); //medio pago
                            array_push($datos2, $rowCredito[0]); //fuente
                            array_push($datos2, $rowCredito[4]); //valor

                            array_push($credito, $datos2); 
                        }
                    }

                    $datosBase = llenaBase($datosBase, 10);

                    for ($i=0; $i < count($credito); $i++){
                        
                        $result = validaArray($credito[$i], $datosBase);
                        
                        if ($result == -1){
                            
                            $prueba = array();

                            array_push($prueba, 'C');
                            array_push($prueba, $credito[$i][2]); //vigencia gasto
                            array_push($prueba, $credito[$i][1]); //seccion presupuestal
                            array_push($prueba, $_POST['cuentas'][$x][0]); //cuenta
                            array_push($prueba, $_POST['cuentas'][$x][1]); //Nombre cuenta
                            array_push($prueba, $credito[$i][4]); //fuente
                            array_push($prueba, $credito[$i][3]); //medio pago
                            array_push($prueba, "0"); //Valor inicial
                            array_push($prueba, "0"); //adicion
                            array_push($prueba, "0"); //reduccion
                            array_push($prueba, $credito[$i][5]); //credito
                            array_push($datosBase, $prueba);
                        }
                        else{
                            $datosBase[$result][10] = $credito[$i][5];
                        }
                    }

                    //contracredito

                    $contraCredito = array();

                    $sqlContraCredito = "SELECT fuente, seccion_presupuestal, codigo_vigenciag, medio_pago, SUM(valor) FROM ccpet_traslados WHERE cuenta LIKE '".$_POST['cuentas'][$x][0]."' AND tipo = 'R' AND seccion_presupuestal LIKE '$secPresu' AND medio_pago LIKE '$medioPago' AND codigo_vigenciag LIKE '$vigGasto' AND estado = 'S' AND fecha BETWEEN '$fechaIni' AND '$fechaFin' AND fuente LIKE '".$_POST["fuentes"][$xy]."%' GROUP BY fuente, seccion_presupuestal, codigo_vigenciag, medio_pago";
                    $resContraCredito = mysqli_query($linkbd, $sqlContraCredito);
                    while ($rowContraCredito = mysqli_fetch_row($resContraCredito)){
                        
                        $datos2 = array();

                        if ($rowContraCredito){
                            array_push($datos2, $_POST['cuentas'][$x][0]); //cuenta
                            array_push($datos2, $rowContraCredito[1]); //seccion presupuestal
                            array_push($datos2, $rowContraCredito[2]); //vigencia gasto
                            array_push($datos2, $rowContraCredito[3]); //medio pago
                            array_push($datos2, $rowContraCredito[0]); //fuente
                            array_push($datos2, $rowContraCredito[4]); //valor

                            array_push($contraCredito, $datos2); 
                        }
                    }

                    $datosBase = llenaBase($datosBase, 11);

                    for ($i=0; $i < count($contraCredito); $i++){
                        
                        $result = validaArray($contraCredito[$i], $datosBase);
                        
                        if ($result == -1){
                            
                            $prueba = array();

                            array_push($prueba, 'C');
                            array_push($prueba, $contraCredito[$i][2]); //vigencia gasto
                            array_push($prueba, $contraCredito[$i][1]); //seccion presupuestal
                            array_push($prueba, $_POST['cuentas'][$x][0]); //cuenta
                            array_push($prueba, $_POST['cuentas'][$x][1]); //Nombre cuenta
                            array_push($prueba, $contraCredito[$i][4]); //fuente
                            array_push($prueba, $contraCredito[$i][3]); //medio pago
                            array_push($prueba, "0"); //Valor inicial
                            array_push($prueba, "0"); //adicion
                            array_push($prueba, "0"); //reduccion
                            array_push($prueba, "0"); //credito
                            array_push($prueba, $contraCredito[$i][5]); //contracredito
                            array_push($datosBase, $prueba);
                        }
                        else{
                            $datosBase[$result][11] = $contraCredito[$i][5];
                        }
                    }

                    //Definitivo
                    for ($i=0; $i < count($datosBase); $i++){
                        
                        $datosBase[$i][12] = $datosBase[$i][7] + $datosBase[$i][8] - $datosBase[$i][9] + $datosBase[$i][10] - $datosBase[$i][11];
                    }
                    //cdp

                    $cdp = array();
                
                    $sqlCdp = "SELECT TB2.fuente, TB2.seccion_presupuestal, TB2.codigo_vigenciag, TB2.medio_pago, SUM(TB2.valor), TB2.productoservicio, TB2.vigencia FROM ccpetcdp AS TB1, ccpetcdp_detalle AS TB2 WHERE TB1.vigencia = TB2.vigencia AND TB1.consvigencia = TB2.consvigencia AND TB2.seccion_presupuestal LIKE '$secPresu' AND TB2.medio_pago LIKE '$medioPago' AND TB2.codigo_vigenciag LIKE '$vigGasto' AND TB2.cuenta LIKE '".$_POST['cuentas'][$x][0]."' AND TB1.tipo_mov = '201' AND TB2.tipo_mov = '201' AND TB1.fecha BETWEEN '$fechaIni' AND '$fechaFin' AND TB2.fuente LIKE '".$_POST["fuentes"][$xy]."%' GROUP BY TB2.seccion_presupuestal, TB2.medio_pago, TB2.codigo_vigenciag, TB2.fuente,TB2.productoservicio";
                    
                    $resCdp = mysqli_query($linkbd, $sqlCdp);
                    while ($rowCdp = mysqli_fetch_row($resCdp)) {
                        
                        $datos2 = array();

                        if ($rowCdp){
                            
                            $sqlCdpRev = "SELECT COALESCE(SUM(DET.valor),0) FROM ccpetcdp_detalle AS DET, ccpetcdp CAB WHERE CAB.consvigencia = DET.consvigencia AND CAB.vigencia = DET.vigencia AND CAB.fecha BETWEEN '$fechaIni' AND '$fechaFin' AND CAB.vigencia = '$rowCdp[6]' AND (DET.vigencia = '$vigenciaIni' OR DET.vigencia = '$vigenciaFin') AND DET.cuenta = '".$_POST['cuentas'][$x][0]."' AND CAB.tipo_mov LIKE '4%' AND DET.tipo_mov LIKE '4%' AND DET.fuente = '$rowCdp[0]' AND DET.seccion_presupuestal = '$rowCdp[1]' AND DET.codigo_vigenciag = '$rowCdp[2]' AND DET.medio_pago = '$rowCdp[3]' AND DET.fuente = '$rowCdp[0]' AND DET.productoservicio = '$rowCdp[5]'";
                            $rowCdpRev = mysqli_fetch_row(mysqli_query($linkbd, $sqlCdpRev));

                            $valor = 0;
                            $valor = $rowCdp[4] - $rowCdpRev[0];

                            array_push($datos2, $_POST['cuentas'][$x][0]); //cuenta
                            array_push($datos2, $rowCdp[1]); //seccion presupuestal
                            array_push($datos2, $rowCdp[2]); //vigencia gasto
                            array_push($datos2, $rowCdp[3]); //medio pago
                            array_push($datos2, $rowCdp[0]); //fuente
                            array_push($datos2, $valor); //valor
                            array_push($datos2, $rowCdp[5]); //cpc

                            array_push($cdp, $datos2); 
                        }
                    }
                    
                    $datosBase = llenaBaseVacio($datosBase, 13);
                    $datosBase = llenaBase($datosBase, 14);

                    for ($i=0; $i < count($cdp); $i++) {
                        
                        $result = validaArrayCpc($cdp[$i], $datosBase);
                        
                        if ($result == -1) {
                            
                            $prueba = array();

                            array_push($prueba, 'D');
                            array_push($prueba, $cdp[$i][2]); //vigencia gasto
                            array_push($prueba, $cdp[$i][1]); //seccion presupuestal
                            array_push($prueba, $_POST['cuentas'][$x][0]); //cuenta
                            array_push($prueba, ''); //Nombre cuenta
                            array_push($prueba, $cdp[$i][4]); //fuente
                            array_push($prueba, $cdp[$i][3]); //medio pago
                            array_push($prueba, "0"); //Valor inicial
                            array_push($prueba, "0"); //adicion
                            array_push($prueba, "0"); //reduccion
                            array_push($prueba, "0"); //credito
                            array_push($prueba, "0"); //cointracredito
                            array_push($prueba, "0"); //definitivo
                            array_push($prueba, $cdp[$i][6]); //cpc
                            array_push($prueba, $cdp[$i][5]); //cdp
                            array_push($datosBase, $prueba);
                        }
                        else {
                            $datosBase[$result][13] = $cdp[$i][6]; //cpc
                            $datosBase[$result][14] = $cdp[$i][5]; //valor
                        }
                    }

                    //rp 
                    $rp = array();

                    $sqlRp = "SELECT TB2.fuente, TB2.seccion_presupuestal, TB2.codigo_vigenciag, TB2.medio_pago, SUM(TB2.valor), TB2.productoservicio, TB1.consvigencia, TB1.vigencia FROM ccpetrp AS TB1, ccpetrp_detalle AS TB2 WHERE TB1.vigencia = TB2.vigencia AND TB1.consvigencia = TB2.consvigencia AND (TB1.fecha BETWEEN '$fechaIni' AND '$fechaFin') AND (TB1.vigencia = '$vigenciaIni' OR TB1.vigencia = '$vigenciaFin')  AND TB2.seccion_presupuestal LIKE '$secPresu' AND TB2.medio_pago LIKE '$medioPago' AND TB2.codigo_vigenciag LIKE '$vigGasto' AND TB2.cuenta LIKE '".$_POST['cuentas'][$x][0]."' AND TB1.tipo_mov = '201' AND TB2.tipo_mov = '201' AND TB2.fuente LIKE '".$_POST["fuentes"][$xy]."%' GROUP BY TB2.seccion_presupuestal, TB2.medio_pago, TB2.codigo_vigenciag, TB2.fuente,TB2.productoservicio";             
                    $resRp = mysqli_query($linkbd, $sqlRp);
                    while ($rowRp = mysqli_fetch_row($resRp)) {
                        
                        $datos2 = array();

                        if ($rowRp) {

                            $sqlRpRev = "SELECT SUM(DET.valor) FROM ccpetrp_detalle AS DET, ccpetrp AS CAB WHERE CAB.consvigencia = DET.consvigencia AND CAB.vigencia = DET.vigencia AND CAB.fecha BETWEEN '$fechaIni' AND '$fechaFin' AND CAB.vigencia = '$rowRp[7]' AND (DET.vigencia = '$vigenciaIni' OR DET.vigencia = '$vigenciaFin') AND DET.cuenta = '".$_POST['cuentas'][$x][0]."' AND CAB.tipo_mov LIKE '4%' AND DET.tipo_mov LIKE '4%' AND DET.seccion_presupuestal = '$rowRp[1]' AND DET.productoservicio = '$rowRp[5]' AND DET.fuente = '$rowRp[0]' AND DET.medio_pago = '$rowRp[3]' AND DET.codigo_vigenciag = '$rowRp[2]'";
                            
                            $rowRpRev = mysqli_fetch_row(mysqli_query($linkbd, $sqlRpRev));

                            $valor = 0;
                            $valor = $rowRp[4] - $rowRpRev[0];

                            array_push($datos2, $_POST['cuentas'][$x][0]); //cuenta
                            array_push($datos2, $rowRp[1]); //seccion presupuestal
                            array_push($datos2, $rowRp[2]); //vigencia gasto
                            array_push($datos2, $rowRp[3]); //medio pago
                            array_push($datos2, $rowRp[0]); //fuente
                            array_push($datos2, $valor); //valor
                            array_push($datos2, $rowRp[5]); //cpc

                            array_push($rp, $datos2); 
                        }
                    }

                    $datosBase = llenaBase($datosBase, 15);

                    for ($i=0; $i < count($rp); $i++) { 
                        
                        $result = validaArrayCpc($rp[$i], $datosBase);
                        
                        if ($result == -1) {
                            
                            $prueba = array();

                            array_push($prueba, 'D');
                            array_push($prueba, $rp[$i][2]); //vigencia gasto
                            array_push($prueba, $rp[$i][1]); //seccion presupuestal
                            array_push($prueba, $_POST['cuentas'][$x][0]); //cuenta
                            array_push($prueba, ''); //Nombre cuenta
                            array_push($prueba, $rp[$i][4]); //fuente
                            array_push($prueba, $rp[$i][3]); //medio pago
                            array_push($prueba, "0"); //Valor inicial
                            array_push($prueba, "0"); //adicion
                            array_push($prueba, "0"); //reduccion
                            array_push($prueba, "0"); //credito
                            array_push($prueba, "0"); //cointracredito
                            array_push($prueba, "0"); //definitivo
                            array_push($prueba, $rp[$i][6]); //cpc
                            array_push($prueba, "0"); //cdp
                            array_push($prueba, $rp[$i][5]); //rp
                            array_push($datosBase, $prueba);
                        }
                        else {
                            $datosBase[$result][15] = $rp[$i][5]; //valor
                        }
                    }

                    //CXP
                    $cxp = array();

                    $sqlCxp = "SELECT DET.fuente, DET.seccion_presupuestal, DET.codigo_vigenciag, DET.medio_pago, SUM(DET.valor), DET.productoservicio FROM tesoordenpago_det AS DET, tesoordenpago AS CAB WHERE CAB.id_orden = DET.id_orden AND CAB.fecha BETWEEN '$fechaIni' AND '$fechaFin' AND (DET.vigencia = '$vigenciaIni' OR DET.vigencia = '$vigenciaFin') AND DET.cuentap LIKE '".$_POST['cuentas'][$x][0]."' AND DET.seccion_presupuestal LIKE '$secPresu' AND DET.medio_pago LIKE '$medioPago' AND DET.codigo_vigenciag LIKE '$vigGasto' AND CAB.estado != 'R' AND DET.fuente LIKE '".$_POST["fuentes"][$xy]."%' GROUP BY DET.fuente, DET.seccion_presupuestal, DET.codigo_vigenciag, DET.medio_pago, DET.productoservicio";
                    $resCxp = mysqli_query($linkbd, $sqlCxp);
                    while ($rowCxp = mysqli_fetch_row($resCxp)) {
                        
                        $datos2 = array();

                        if ($rowCxp) {

                            array_push($datos2, $_POST['cuentas'][$x][0]); //cuenta
                            array_push($datos2, $rowCxp[1]); //seccion presupuestal
                            array_push($datos2, $rowCxp[2]); //vigencia gasto
                            array_push($datos2, $rowCxp[3]); //medio pago
                            array_push($datos2, $rowCxp[0]); //fuente
                            array_push($datos2, $rowCxp[4]); //valor
                            array_push($datos2, $rowCxp[5]); //cpc

                            array_push($cxp, $datos2); 

                        }
                    }

                    $datosBase = llenaBase($datosBase, 16);

                    for ($i=0; $i < count($cxp); $i++) {
                        
                        $result = validaArrayCpc($cxp[$i], $datosBase);
                        
                        if ($result == -1) {
                            
                            $prueba = array();

                            array_push($prueba, 'D');
                            array_push($prueba, $cxp[$i][2]); //vigencia gasto
                            array_push($prueba, $cxp[$i][1]); //seccion presupuestal
                            array_push($prueba, $_POST['cuentas'][$x][0]); //cuenta
                            array_push($prueba, ''); //Nombre cuenta
                            array_push($prueba, $cxp[$i][4]); //fuente
                            array_push($prueba, $cxp[$i][3]); //medio pago
                            array_push($prueba, "0"); //Valor inicial
                            array_push($prueba, "0"); //adicion
                            array_push($prueba, "0"); //reduccion
                            array_push($prueba, "0"); //credito
                            array_push($prueba, "0"); //cointracredito
                            array_push($prueba, "0"); //definitivo
                            array_push($prueba, $cxp[$i][6]); //cpc
                            array_push($prueba, "0"); //cdp
                            array_push($prueba, "0"); //rp
                            array_push($prueba, $cxp[$i][5]); //obligacion
                            array_push($datosBase, $prueba);
                        }
                        else {
                            $datosBase[$result][16] = $cxp[$i][5]; //valor
                        }
                    }

                    // cxp nomina
                    $cxpNom = array();

                    $sqlCxpNomina = "SELECT HD.fuente, HD.seccion_presupuestal, HD.vigencia_gasto, HD.medio_pago, SUM(HD.valor), HD.producto FROM humnom_presupuestal AS HD, humnomina_aprobado AS HN WHERE HN.id_nom = HD.id_nom AND HN.fecha BETWEEN '$fechaIni' AND '$fechaFin' AND HN.estado!='N' AND HD.estado='P' AND HD.cuenta LIKE '".$_POST['cuentas'][$x][0]."%' AND HD.seccion_presupuestal LIKE '$secPresu' AND HD.vigencia_gasto LIKE '$vigGasto' AND HD.medio_pago LIKE '$medioPago' AND HD.fuente LIKE '".$_POST["fuentes"][$xy]."%' GROUP BY HD.fuente, HD.seccion_presupuestal, HD.vigencia_gasto, HD.medio_pago, HD.producto ";
                    $resCxpNomina = mysqli_query($linkbd, $sqlCxpNomina);
                    while ($rowCxpNomina = mysqli_fetch_row($resCxpNomina)) {
                        
                        $datos2 = array();

                        if ($rowCxpNomina) {

                            array_push($datos2, $_POST['cuentas'][$x][0]); //cuenta
                            array_push($datos2, $rowCxpNomina[1]); //seccion presupuestal
                            array_push($datos2, $rowCxpNomina[2]); //vigencia gasto
                            array_push($datos2, $rowCxpNomina[3]); //medio pago
                            array_push($datos2, $rowCxpNomina[0]); //fuente
                            array_push($datos2, $rowCxpNomina[4]); //valor
                            array_push($datos2, $rowCxpNomina[5]); //cpc

                            array_push($cxpNom, $datos2); 

                        }
                    }

                    for ($i=0; $i < count($cxpNom); $i++) {
                        
                        $result = validaArrayCpc($cxpNom[$i], $datosBase);
                        
                        if ($result == -1) {
                            
                            $prueba = array();

                            array_push($prueba, 'D');
                            array_push($prueba, $cxpNom[$i][2]); //vigencia gasto
                            array_push($prueba, $cxpNom[$i][1]); //seccion presupuestal
                            array_push($prueba, $_POST['cuentas'][$x][0]); //cuenta
                            array_push($prueba, ''); //Nombre cuenta
                            array_push($prueba, $cxpNom[$i][4]); //fuente
                            array_push($prueba, $cxpNom[$i][3]); //medio pago
                            array_push($prueba, "0"); //Valor inicial
                            array_push($prueba, "0"); //adicion
                            array_push($prueba, "0"); //reduccion
                            array_push($prueba, "0"); //credito
                            array_push($prueba, "0"); //cointracredito
                            array_push($prueba, "0"); //definitivo
                            array_push($prueba, $cxpNom[$i][6]); //cpc
                            array_push($prueba, "0"); //cdp
                            array_push($prueba, "0"); //rp
                            array_push($prueba, $cxpNom[$i][5]); //obligacion
                            array_push($datosBase, $prueba);
                        }
                        else {
                            $datosBase[$result][16] += $cxpNom[$i][5]; //valor
                        }
                    }

                    //egreso

                    $egreso = array();

                    $sqlEgreso = "SELECT TD.fuente, TD.seccion_presupuestal, TD.codigo_vigenciag, TD.medio_pago, SUM(TD.valor), TD.productoservicio FROM tesoordenpago_det AS TD, tesoegresos AS TE WHERE (TD.vigencia = '$vigenciaIni' OR TD.vigencia = '$vigenciaFin') AND TE.fecha BETWEEN '$fechaIni' AND '$fechaFin' AND TE.estado != 'N' AND TE.estado != 'R' AND TD.id_orden = TE.id_orden AND TD.cuentap LIKE '".$_POST['cuentas'][$x][0]."' AND TD.seccion_presupuestal LIKE '$secPresu' AND TD.codigo_vigenciag LIKE '$vigGasto' AND TD.medio_pago LIKE '$medioPago' AND TD.fuente LIKE '".$_POST["fuentes"][$xy]."%' GROUP BY TD.fuente, TD.seccion_presupuestal, TD.codigo_vigenciag, TD.medio_pago, TD.productoservicio";
                    $resEgreso = mysqli_query($linkbd, $sqlEgreso);
                    while ($rowEgreso = mysqli_fetch_row($resEgreso)) {
                        
                        $datos2 = array();

                        if ($rowEgreso) {

                            array_push($datos2, $_POST['cuentas'][$x][0]); //cuenta
                            array_push($datos2, $rowEgreso[1]); //seccion presupuestal
                            array_push($datos2, $rowEgreso[2]); //vigencia gasto
                            array_push($datos2, $rowEgreso[3]); //medio pago
                            array_push($datos2, $rowEgreso[0]); //fuente
                            array_push($datos2, $rowEgreso[4]); //valor
                            array_push($datos2, $rowEgreso[5]); //cpc

                            array_push($egreso, $datos2); 

                        }
                    }
                    
                    $datosBase = llenaBase($datosBase, 17);

                    for ($i=0; $i < count($egreso); $i++) {
                        
                        $result = validaArrayCpc($egreso[$i], $datosBase);
                        
                        if ($result == -1) {
                            
                            $prueba = array();

                            array_push($prueba, 'D');
                            array_push($prueba, $egreso[$i][2]); //vigencia gasto
                            array_push($prueba, $egreso[$i][1]); //seccion presupuestal
                            array_push($prueba, $_POST['cuentas'][$x][0]); //cuenta
                            array_push($prueba, ''); //Nombre cuenta
                            array_push($prueba, $egreso[$i][4]); //fuente
                            array_push($prueba, $egreso[$i][3]); //medio pago
                            array_push($prueba, "0"); //Valor inicial
                            array_push($prueba, "0"); //adicion
                            array_push($prueba, "0"); //reduccion
                            array_push($prueba, "0"); //credito
                            array_push($prueba, "0"); //cointracredito
                            array_push($prueba, "0"); //definitivo
                            array_push($prueba, $egreso[$i][6]); //cpc
                            array_push($prueba, "0"); //cdp
                            array_push($prueba, "0"); //rp
                            array_push($prueba, "0"); //obligacion
                            array_push($prueba, $egreso[$i][5]); //egresos
                            array_push($datosBase, $prueba);
                        }
                        else {
                            $datosBase[$result][17] = $egreso[$i][5]; //valor
                        }
                    }

                    $egresoNom = array();

                    $sqlEgresoNomina = "SELECT TD.fuente, TD.seccion_presupuestal, TD.vigencia_gasto, TD.medio_pago, SUM(TD.valordevengado), TD.productoservicio FROM tesoegresosnomina_det AS TD, tesoegresosnomina AS TE WHERE (TE.vigencia = '$vigenciaIni' OR TE.vigencia = '$vigenciaFin') AND TE.fecha BETWEEN '$fechaIni' AND '$fechaFin' AND TD.id_egreso = TE.id_egreso AND TE.estado = 'S' AND NOT(TD.tipo='SE' OR TD.tipo='PE' OR TD.tipo='DS' OR TD.tipo='RE' OR TD.tipo='FS') AND TD.cuentap LIKE '".$_POST['cuentas'][$x][0]."' AND TD.seccion_presupuestal LIKE '$secPresu' AND TD.vigencia_gasto LIKE '$vigGasto' AND TD.medio_pago LIKE '$medioPago' AND TD.fuente LIKE '".$_POST["fuentes"][$xy]."%' GROUP BY TD.fuente, TD.seccion_presupuestal, TD.vigencia_gasto, TD.medio_pago, TD.productoservicio";
                    $resEgresoNomina = mysqli_query($linkbd, $sqlEgresoNomina);
                    while ($rowEgresoNomina = mysqli_fetch_row($resEgresoNomina)) {
                        
                        $datos2 = array();

                        if ($rowEgresoNomina) {

                            array_push($datos2, $_POST['cuentas'][$x][0]); //cuenta
                            array_push($datos2, $rowEgresoNomina[1]); //seccion presupuestal
                            array_push($datos2, $rowEgresoNomina[2]); //vigencia gasto
                            array_push($datos2, $rowEgresoNomina[3]); //medio pago
                            array_push($datos2, $rowEgresoNomina[0]); //fuente
                            array_push($datos2, $rowEgresoNomina[4]); //valor
                            array_push($datos2, $rowEgresoNomina[5]); //cpc

                            array_push($egresoNom, $datos2); 

                        }
                    }

                    for ($i=0; $i < count($egresoNom); $i++) {
                        
                        $result = validaArrayCpc($egresoNom[$i], $datosBase);
                        
                        if ($result == -1) {
                            
                            $prueba = array();

                            array_push($prueba, 'D');
                            array_push($prueba, $egresoNom[$i][2]); //vigencia gasto
                            array_push($prueba, $egresoNom[$i][1]); //seccion presupuestal
                            array_push($prueba, $_POST['cuentas'][$x][0]); //cuenta
                            array_push($prueba, ''); //Nombre cuenta
                            array_push($prueba, $egresoNom[$i][4]); //fuente
                            array_push($prueba, $egresoNom[$i][3]); //medio pago
                            array_push($prueba, "0"); //Valor inicial
                            array_push($prueba, "0"); //adicion
                            array_push($prueba, "0"); //reduccion
                            array_push($prueba, "0"); //credito
                            array_push($prueba, "0"); //cointracredito
                            array_push($prueba, "0"); //definitivo
                            array_push($prueba, $egresoNom[$i][6]); //cpc
                            array_push($prueba, "0"); //cdp
                            array_push($prueba, "0"); //rp
                            array_push($prueba, "0"); //obligacion
                            array_push($prueba, $egresoNom[$i][5]); //egresos
                            array_push($datosBase, $prueba);
                        }
                        else {
                            $datosBase[$result][17] += $egresoNom[$i][5]; //valor
                        }
                    }

                    for ($i=0; $i < count($datosBase); $i++) {

                        if ($datosBase[$i][0] == 'C' && $datosBase[$i][14] == '0'){

                            $seccionPresupuestal = $datosBase[$i][2];
                            $vigenciaGasto = $datosBase[$i][1];
                            $medioDePago = $datosBase[$i][6];
                            $fuente = $datosBase[$i][5];

                            $sqlCdp = "SELECT COALESCE(SUM(TB2.valor),0) FROM ccpetcdp AS TB1, ccpetcdp_detalle AS TB2 WHERE TB1.vigencia = TB2.vigencia AND TB1.consvigencia = TB2.consvigencia AND TB2.seccion_presupuestal LIKE '$seccionPresupuestal' AND TB2.medio_pago LIKE '$medioDePago' AND TB2.codigo_vigenciag LIKE '$vigenciaGasto' AND TB2.cuenta LIKE '".$_POST['cuentas'][$x][0]."' AND TB2.fuente LIKE '$fuente' AND TB1.tipo_mov = '201' AND TB2.tipo_mov = '201' AND TB1.fecha BETWEEN '$fechaIni' AND '$fechaFin'";
                            $rowCdp = mysqli_fetch_row(mysqli_query($linkbd, $sqlCdp));

                            $sqlCdpRev = "SELECT COALESCE(SUM(TB2.valor),0) FROM ccpetcdp AS TB1, ccpetcdp_detalle AS TB2 WHERE TB1.vigencia = TB2.vigencia AND TB1.consvigencia = TB2.consvigencia AND TB2.seccion_presupuestal LIKE '$seccionPresupuestal' AND TB2.medio_pago LIKE '$medioDePago' AND TB2.codigo_vigenciag LIKE '$vigenciaGasto' AND TB2.cuenta LIKE '".$_POST['cuentas'][$x][0]."' AND TB2.fuente LIKE '$fuente' AND TB1.tipo_mov LIKE '4%' AND TB2.tipo_mov LIKE '4%' AND TB1.fecha BETWEEN '$fechaIni' AND '$fechaFin'";
                            $rowCdpRev = mysqli_fetch_row(mysqli_query($linkbd, $sqlCdpRev));

                            $datosBase[$i][14] = $rowCdp[0] - $rowCdpRev[0];

                            //rp
                            $sqlRp = "SELECT COALESCE(SUM(TB2.valor),0) FROM ccpetrp AS TB1, ccpetrp_detalle AS TB2 WHERE TB1.vigencia = TB2.vigencia AND TB1.consvigencia = TB2.consvigencia AND TB2.seccion_presupuestal LIKE '$seccionPresupuestal' AND TB2.medio_pago LIKE '$medioDePago' AND TB2.codigo_vigenciag LIKE '$vigenciaGasto' AND TB2.cuenta LIKE '".$_POST['cuentas'][$x][0]."' AND TB2.fuente LIKE '$fuente' AND TB1.tipo_mov = '201' AND TB2.tipo_mov = '201' AND TB1.fecha BETWEEN '$fechaIni' AND '$fechaFin'";
                            $rowRp = mysqli_fetch_row(mysqli_query($linkbd, $sqlRp));

                            $sqlRpRev = "SELECT COALESCE(SUM(TB2.valor),0) FROM ccpetrp AS TB1, ccpetrp_detalle AS TB2 WHERE TB1.vigencia = TB2.vigencia AND TB1.consvigencia = TB2.consvigencia AND TB2.seccion_presupuestal LIKE '$seccionPresupuestal' AND TB2.medio_pago LIKE '$medioDePago' AND TB2.codigo_vigenciag LIKE '$vigenciaGasto' AND TB2.cuenta LIKE '".$_POST['cuentas'][$x][0]."' AND TB2.fuente LIKE '$fuente' AND TB1.tipo_mov LIKE '4%' AND TB2.tipo_mov LIKE '4%' AND TB1.fecha BETWEEN '$fechaIni' AND '$fechaFin'";
                            $rowRpRev = mysqli_fetch_row(mysqli_query($linkbd, $sqlRpRev));

                            $datosBase[$i][15] = $rowRp[0] - $rowRpRev[0];

                            //cxp
                            $sqlCxp = "SELECT COALESCE(SUM(DET.valor),0) FROM tesoordenpago_det AS DET, tesoordenpago AS CAB WHERE CAB.id_orden = DET.id_orden AND CAB.fecha BETWEEN '$fechaIni' AND '$fechaFin' AND (DET.vigencia = '$vigenciaIni' OR DET.vigencia = '$vigenciaFin') AND DET.cuentap LIKE '".$_POST['cuentas'][$x][0]."' AND DET.seccion_presupuestal LIKE '$seccionPresupuestal' AND DET.medio_pago LIKE '$medioDePago' AND DET.codigo_vigenciag LIKE '$vigenciaGasto' AND DET.fuente LIKE '$fuente' AND CAB.estado != 'R'";
                            $rowCxp = mysqli_fetch_row(mysqli_query($linkbd, $sqlCxp));

                            //cxp nomina
                            $sqlCxpNomina = "SELECT COALESCE(SUM(HD.valor),0) FROM humnom_presupuestal AS HD, humnomina_aprobado AS HN WHERE HN.id_nom = HD.id_nom AND HN.fecha BETWEEN '$fechaIni' AND '$fechaFin' AND HN.estado!='N' AND HD.estado='P' AND HD.cuenta LIKE '".$_POST['cuentas'][$x][0]."' AND HD.seccion_presupuestal LIKE '$seccionPresupuestal' AND HD.vigencia_gasto LIKE '$vigenciaGasto' AND HD.medio_pago LIKE '$medioDePago' AND HD.fuente LIKE '$fuente'";
                            $rowCxpNomina = mysqli_fetch_row(mysqli_query($linkbd, $sqlCxpNomina));

                            $datosBase[$i][16] = $rowCxp[0] + $rowCxpNomina[0];

                            //egreso
                            $sqlEgreso = "SELECT COALESCE(SUM(TD.valor),0) FROM tesoordenpago_det AS TD, tesoegresos AS TE WHERE (TD.vigencia = '$vigenciaIni' OR TD.vigencia = '$vigenciaFin') AND TE.fecha BETWEEN '$fechaIni' AND '$fechaFin' AND TE.estado != 'N' AND TE.estado != 'R' AND TD.id_orden = TE.id_orden AND TD.cuentap LIKE '".$_POST['cuentas'][$x][0]."' AND TD.seccion_presupuestal LIKE '$seccionPresupuestal' AND TD.codigo_vigenciag LIKE '$vigenciaGasto' AND TD.medio_pago LIKE '$medioDePago' AND TD.fuente LIKE '$fuente'";
                            $rowEgreso = mysqli_fetch_row(mysqli_query($linkbd, $sqlEgreso));

                            //egreso nomina
                            $sqlEgresoNomina = "SELECT COALESCE(SUM(TD.valordevengado),0) FROM tesoegresosnomina_det AS TD, tesoegresosnomina AS TE WHERE (TE.vigencia = '$vigenciaIni' OR TE.vigencia = '$vigenciaFin') AND TE.fecha BETWEEN '$fechaIni' AND '$fechaFin' AND TD.id_egreso = TE.id_egreso AND TE.estado = 'S' AND NOT(TD.tipo='SE' OR TD.tipo='PE' OR TD.tipo='DS' OR TD.tipo='RE' OR TD.tipo='FS') AND TD.seccion_presupuestal LIKE '$seccionPresupuestal' AND TD.vigencia_gasto LIKE '$vigenciaGasto' AND TD.medio_pago LIKE '$medioDePago' AND TD.fuente LIKE '$fuente' AND TD.cuentap LIKE '".$_POST['cuentas'][$x][0]."%' AND TD.cuentap NOT LIKE '2.3%'";
                            $rowEgresoNomina = mysqli_fetch_row(mysqli_query($linkbd, $sqlEgresoNomina));

                            $datosBase[$i][17] = $rowEgreso[0] + $rowEgresoNomina[0];
                        }  
                    }       

                    //array_sort_by($datosBase, 3, $order = SORT_ASC);
                    array_sort_by($datosBase, 5, $order = SORT_ASC);

                    //Salida
                    for ($i=0; $i < count($datosBase); $i++) {

                        //Suma de saldos
                        if ($datosBase[$i][0] != 'D') {
                            $datosBase[$i][18] = $datosBase[$i][12] - $datosBase[$i][14];
                        }             
                        else {
                            $datosBase[$i][18] = 0;
                        }     

                        //nombre de fuente 5
                        if ($datosBase[$i][0] != 'A') {

                            $codigoFuente = $datosBase[$i][5];

                            $sqlFuente = "SELECT nombre FROM ccpet_fuentes_cuipo WHERE codigo_fuente LIKE '$codigoFuente'";
                            $rowFuente = mysqli_fetch_assoc(mysqli_query($linkbd, $sqlFuente));

                            $datosBase[$i][19] = $rowFuente['nombre'];
                        }
                        else {
                            $datosBase[$i][19] = "";
                        }

                        if ($datosBase[$i][0] == 'D'){

                            $cpc = $datosBase[$i][13];
                            $inicial =  intval(substr($datosBase[$i][13],0,1));
                            
                            if($inicial >= 0 && $inicial <= 4) {

                                $queryCpc = "SELECT titulo FROM ccpetbienestransportables WHERE grupo LIKE '$cpc'";
                                $rowCpc = mysqli_fetch_assoc(mysqli_query($linkbd, $queryCpc));
                                $datosBase[$i][20] = $rowCpc['titulo'];
                            }
                            else if ($inicial >= 5 && $inicial <= 9) {
                                $queryCpc = "SELECT titulo FROM ccpetservicios WHERE grupo LIKE '$cpc'";
                                $rowCpc = mysqli_fetch_assoc(mysqli_query($linkbd, $queryCpc));
                                $datosBase[$i][20] = $rowCpc['titulo'];
                            }

                            
                        }
                        else {
                            $datosBase[$i][20] = "";
                        }
                        
                        if ($datosBase[$i][12] >= 0 || $datosBase[$i][14] >= 0) {
                            array_push($detalles, $datosBase[$i]);  
                        }
                    }
                }

                $xy++;
            }
            while($xy < $tamFuentes);
        }
        
        $out['detalles'] = $detalles;
    }

    header("Content-type: application/json");
    echo json_encode($out);
    die();