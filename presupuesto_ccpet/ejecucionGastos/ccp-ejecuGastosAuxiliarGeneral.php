<?php
	require_once '../../comun.inc';
    require '../../funciones.inc';
    require '../../conversor.php';
    session_start();

	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");

    $out = array('error' => false);

    $action = "show";

    if(isset($_GET['action'])){
        $action = $_GET['action'];
    }

    if ($action == "datosFormulario") {

        $vigGasto = $_POST['vigGasto'];
        $secPresupuestal = $_POST['secPresupuestal'];
        $cuenta = $_POST['cuenta'];
        $fuente = $_POST['fuente'];
        $medioPago = $_POST['medioPago'];
        $bpim = $_POST['bpim'];
        $programatico = $_POST['programatico'];

        preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST['fechaIni'],$fecha);
        $fechaIni = "$fecha[3]-$fecha[2]-$fecha[1]";
        $vigenciaIni = $fecha[3];

        preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST['fechaFin'],$fecha);
        $fechaFin = "$fecha[3]-$fecha[2]-$fecha[1]";
        $vigenciaFin = $fecha[3];

        $auxiliar = array();

            if($cuenta != ''){

            

                $inicial = 0;

                $sqlPresupuestoInicial = "SELECT COALESCE(valor,0), vigencia FROM ccpetinicialgastosfun WHERE cuenta LIKE '$cuenta' AND seccion_presupuestal LIKE '$secPresupuestal' AND vigencia_gasto LIKE '$vigGasto' AND medio_pago LIKE '$medioPago' AND fuente LIKE '$fuente' AND (vigencia = '$vigenciaIni' OR vigencia = '$vigenciaFin')";
                $rowPresupuestoInicial = mysqli_fetch_row(mysqli_query($linkbd, $sqlPresupuestoInicial));
                
                $datos = array();
        
                array_push($datos, "D");
                array_push($datos, "Presupuesto inicial");
                array_push($datos, "");
                array_push($datos, $rowPresupuestoInicial[1]);
                array_push($datos, '');
                array_push($datos, $rowPresupuestoInicial[0]);
                $inicial = $rowPresupuestoInicial[0];
                array_push($auxiliar, $datos);
            }else{

                $sqlrPresupuestoInicialInv = "SELECT COALESCE(TB2.valorcsf + TB2.valorssf, 0), TB1.vigencia FROM ccpproyectospresupuesto AS TB1, ccpproyectospresupuesto_presupuesto AS TB2 WHERE TB1.id = TB2.codproyecto AND TB1.codigo = '$bpim' AND TB1.vigencia = '$vigenciaIni' AND TB1.vigencia = '$vigenciaFin' AND TB2.id_fuente = '$fuente' AND TB2.medio_pago = '$medioPago' AND TB2.indicador_producto = '$programatico' AND TB2.vigencia_gasto = '$vigGasto'";
                $rowPresupuestoInicialInv = mysqli_fetch_row(mysqli_query($linkbd, $sqlrPresupuestoInicialInv));

                $datos = array();
        
                array_push($datos, "D");
                array_push($datos, "Presupuesto inicial");
                array_push($datos, "");
                array_push($datos, $rowPresupuestoInicialInv[1]);
                array_push($datos, '');
                array_push($datos, $rowPresupuestoInicialInv[0]);
                $inicial = $rowPresupuestoInicialInv[0];
                array_push($auxiliar, $datos);
            }
            
            $totalAdicion = 0;
    
            $sqlAdiciones = "SELECT id_acuerdo, fecha, valor, estado FROM ccpet_adiciones WHERE cuenta LIKE '$cuenta' AND tipo_cuenta = 'G' AND seccion_presupuestal LIKE '$secPresupuestal' AND medio_pago LIKE '$medioPago' AND codigo_vigenciag LIKE '$vigGasto' AND fuente LIKE '$fuente' AND estado = 'S' AND bpim LIKE '%$bpim%' AND programatico LIKE '%$programatico%' AND fecha BETWEEN '$fechaIni' AND '$fechaFin'";
            // echo $sqlAdiciones;
            $resAdiciones = mysqli_query($linkbd, $sqlAdiciones);
            while ($rowAdiciones = mysqli_fetch_row($resAdiciones)) {
                
                $sqlrAcuerdo = "SELECT consecutivo, tipo_acto_adm FROM ccpetacuerdos WHERE id_acuerdo = $rowAdiciones[0]";
                $resrAcuerdo = mysqli_query($linkbd, $sqlrAcuerdo);
                $rowrAcuerdo = mysqli_fetch_row($resrAcuerdo);

                $tipoActo = '';
                if ($rowrAcuerdo[1] == 1) {
                    $tipoActo = 'Acuerdo';
                }elseif ($rowrAcuerdo[1] == 2) {
                    $tipoActo = 'Resolucion';
                }else {
                    $tipoActo = 'Decreto';
                }

                $datos = array();

                $estado = $rowAdiciones[3] == 'S' ? 'Activo' : 'Anulado';
    
                array_push($datos, "D");
                array_push($datos, "Adición");
                array_push($datos, $rowAdiciones[0] . '- ' . $tipoActo . ': ' . $rowrAcuerdo[0]);
                array_push($datos, $rowAdiciones[1]);
                array_push($datos, $estado);
                array_push($datos, $rowAdiciones[2]);
                $totalAdicion += $rowAdiciones[2];
                array_push($auxiliar, $datos);
            }
    
            $datos = array();
    
            array_push($datos, "T");
            array_push($datos, " Total Adición");
            array_push($datos, "");
            array_push($datos, "");
            array_push($datos, "");
            array_push($datos, $totalAdicion);
            array_push($auxiliar, $datos);
    
            $totalReduccion = 0;
    
            $sqlReducciones = "SELECT id_adicion, fecha, valor, estado FROM ccpet_reducciones WHERE cuenta LIKE '$cuenta' AND tipo_cuenta = 'G' AND seccion_presupuestal LIKE '$secPresupuestal' AND medio_pago LIKE '$medioPago' AND codigo_vigenciag LIKE '$vigGasto' AND fuente LIKE '$fuente' AND  estado = 'S' AND bpim LIKE '%$bpim%' AND programatico LIKE '%$programatico%' AND fecha BETWEEN '$fechaIni' AND '$fechaFin'";
            $resReducciones = mysqli_query($linkbd, $sqlReducciones);
            while ($rowReducciones = mysqli_fetch_row($resReducciones)) {

                $sqlrAcuerdo = "SELECT consecutivo, tipo_acto_adm FROM ccpetacuerdos WHERE id_acuerdo = $rowReducciones[0]";
                $resrAcuerdo = mysqli_query($linkbd, $sqlrAcuerdo);
                $rowrAcuerdo = mysqli_fetch_row($resrAcuerdo);

                $tipoActo = '';
                if ($rowrAcuerdo[1] == 1) {
                    $tipoActo = 'Acuerdo';
                }elseif ($rowrAcuerdo[1] == 2) {
                    $tipoActo = 'Resolucion';
                }else {
                    $tipoActo = 'Decreto';
                }
                
                $datos = array();

                $estado = $rowReducciones[3] == 'S' ? 'Activo' : 'Anulado';
    
                array_push($datos, "D");
                array_push($datos, "Reducción");
                array_push($datos, $rowReducciones[0] . '-  ' . $tipoActo . ': ' . $rowrAcuerdo[0]);
                array_push($datos, $rowReducciones[1]);
                array_push($datos, $estado);
                array_push($datos, $rowReducciones[2]);
                $totalReduccion += $rowReducciones[2];
                array_push($auxiliar, $datos);
            }
    
    
            $datos = array();
    
            array_push($datos, "T");
            array_push($datos, " Total Reducción");
            array_push($datos, "");
            array_push($datos, "");
            array_push($datos, "");
            array_push($datos, $totalReduccion);
            array_push($auxiliar, $datos);
    
            $totalCredito = 0;
    
            $sqlCredito = "SELECT id_acuerdo, fecha, valor, estado FROM ccpet_traslados WHERE cuenta LIKE '$cuenta' AND tipo = 'C' AND seccion_presupuestal LIKE '$secPresupuestal' AND medio_pago LIKE '$medioPago' AND codigo_vigenciag LIKE '$vigGasto' AND fuente LIKE '$fuente' AND estado = 'S' AND bpim LIKE '%$bpim%' AND programatico LIKE '%$programatico%' AND fecha BETWEEN '$fechaIni' AND '$fechaFin'";
            $resCredito = mysqli_query($linkbd, $sqlCredito);
            while ($rowCredito = mysqli_fetch_row($resCredito)) {

                $sqlrAcuerdo = "SELECT consecutivo, tipo_acto_adm FROM ccpetacuerdos WHERE id_acuerdo = $rowCredito[0]";
                $resrAcuerdo = mysqli_query($linkbd, $sqlrAcuerdo);
                $rowrAcuerdo = mysqli_fetch_row($resrAcuerdo);

                $tipoActo = '';
                if ($rowrAcuerdo[1] == 1) {
                    $tipoActo = 'Acuerdo';
                }elseif ($rowrAcuerdo[1] == 2) {
                    $tipoActo = 'Resolucion';
                }else {
                    $tipoActo = 'Decreto';
                }
                
                $datos = array();

                $estado = $rowCredito[3] == 'S' ? 'Activo' : 'Anulado';
    
                array_push($datos, "D");
                array_push($datos, "Credito");
                array_push($datos, $rowCredito[0] . '- ' . $tipoActo . ': ' . $rowrAcuerdo[0]);
                array_push($datos, $rowCredito[1]);
                array_push($datos, $estado);
                array_push($datos, $rowCredito[2]);
                $totalCredito += $rowCredito[2];
                array_push($auxiliar, $datos);
            }
    
            $datos = array();
    
            array_push($datos, "T");
            array_push($datos, " Total Creditos");
            array_push($datos, "");
            array_push($datos, "");
            array_push($datos, "");
            array_push($datos, $totalCredito);
            array_push($auxiliar, $datos);
    
    
            $totalContraCredito = 0;
    
            $sqlContraCredito = "SELECT id_acuerdo, fecha, valor, estado FROM ccpet_traslados WHERE cuenta LIKE '$cuenta' AND tipo = 'R' AND seccion_presupuestal LIKE '$secPresupuestal' AND medio_pago LIKE '$medioPago' AND codigo_vigenciag LIKE '$vigGasto' AND fuente LIKE '$fuente' AND estado = 'S' AND bpim LIKE '%$bpim%' AND programatico LIKE '%$programatico%' AND fecha BETWEEN '$fechaIni' AND '$fechaFin'";
            $resContraCredito = mysqli_query($linkbd, $sqlContraCredito);
            while ($rowContraCredito = mysqli_fetch_row($resContraCredito)) {

                $sqlrAcuerdo = "SELECT consecutivo, tipo_acto_adm FROM ccpetacuerdos WHERE id_acuerdo = $rowContraCredito[0]";
                $resrAcuerdo = mysqli_query($linkbd, $sqlrAcuerdo);
                $rowrAcuerdo = mysqli_fetch_row($resrAcuerdo);

                $tipoActo = '';
                if ($rowrAcuerdo[1] == 1) {
                    $tipoActo = 'Acuerdo';
                }elseif ($rowrAcuerdo[1] == 2) {
                    $tipoActo = 'Resolucion';
                }else {
                    $tipoActo = 'Decreto';
                }
                
                $datos = array();

                $estado = $rowContraCredito[3] == 'S' ? 'Activo' : 'Anulado';
    
                array_push($datos, "D");
                array_push($datos, "ContraCredito");
                array_push($datos, $rowContraCredito[0] . '- ' . $tipoActo . ': ' . $rowrAcuerdo[0]);
                array_push($datos, $rowContraCredito[1]);
                array_push($datos, $estado);
                array_push($datos, $rowContraCredito[2]);
                $totalContraCredito += $rowContraCredito[2];
                array_push($auxiliar, $datos);
            }
    
            $datos = array();
    
            array_push($datos, "T");
            array_push($datos, " Total ContraCredito");
            array_push($datos, "");
            array_push($datos, "");
            array_push($datos, "");
            array_push($datos, $totalContraCredito);
            array_push($auxiliar, $datos);
    
    
            $datos = array();
    
            $definitivo = 0;
            $definitivo = $inicial + $totalAdicion - $totalReduccion + $totalCredito - $totalContraCredito;
    
            array_push($datos, "T");
            array_push($datos, "Definitivo");
            array_push($datos, "");
            array_push($datos, "");
            array_push($datos, "");
            array_push($datos, $definitivo);
            array_push($auxiliar, $datos);
        

        $totalCdp = 0;

        $sqlCdp = "SELECT TB2.consvigencia, TB1.fecha, TB2.valor, TB2.vigencia, TB1.estado FROM ccpetcdp AS TB1, ccpetcdp_detalle AS TB2 WHERE TB1.vigencia = TB2.vigencia AND TB1.consvigencia = TB2.consvigencia AND TB2.seccion_presupuestal LIKE '$secPresupuestal' AND TB2.medio_pago LIKE '$medioPago' AND TB2.codigo_vigenciag LIKE '$vigGasto' AND TB2.cuenta LIKE '%$cuenta%' AND TB2.tipo_mov LIKE '201' AND TB1.tipo_mov LIKE '201' AND TB2.bpim LIKE '%$bpim%' AND TB2.indicador_producto LIKE '%$programatico%' AND TB1.fecha BETWEEN '$fechaIni' AND '$fechaFin' AND TB2.fuente LIKE '$fuente'";
        //echo $sqlCdp;
        $resCdp = mysqli_query($linkbd, $sqlCdp);
        while ($rowCdp = mysqli_fetch_row($resCdp)){

            if ($rowCdp) {

                $sqlCdpRev = "SELECT COALESCE(SUM(TB2.valor),0) FROM ccpetcdp AS TB1, ccpetcdp_detalle AS TB2 WHERE TB1.vigencia = TB2.vigencia AND TB1.consvigencia = TB2.consvigencia AND TB1.consvigencia = '$rowCdp[0]' AND TB2.seccion_presupuestal LIKE '$secPresupuestal' AND TB2.medio_pago LIKE '$medioPago' AND TB2.codigo_vigenciag LIKE '$vigGasto' AND TB2.cuenta LIKE '%$cuenta%' AND TB2.tipo_mov LIKE '4%' AND TB1.tipo_mov LIKE '4%' AND TB2.bpim LIKE '%$bpim%' AND TB2.indicador_producto LIKE '%$programatico%' AND TB1.fecha BETWEEN '$fechaIni' AND '$fechaFin' AND TB2.fuente LIKE '$fuente'";
                $resCdpRev = mysqli_query($linkbd, $sqlCdpRev);
                $rowCdpRev = mysqli_fetch_row($resCdpRev);

                $valor = 0;
                $valor = $rowCdp[2]-$rowCdpRev[0];

                $datos = array();

                $estado = $rowCdp[4] != 'N' ? 'Activo' : 'ANULADO';

                array_push($datos, "D");
                array_push($datos, "Disponibilidad");
                array_push($datos, $rowCdp[0]);
                array_push($datos, $rowCdp[1]);
                array_push($datos, $estado);
                array_push($datos, $valor);
                array_push($datos, $rowCdp[3]);
                $totalCdp += $valor;
                array_push($auxiliar, $datos);
            }
        }

        $datos = array();

        array_push($datos, "T");
        array_push($datos, " Total Disponibilidad");
        array_push($datos, "");
        array_push($datos, "");
        array_push($datos, "");
        array_push($datos, $totalCdp);
        array_push($auxiliar, $datos);

        $totalRp = 0;

        $sqlRp = "SELECT TB2.consvigencia, TB1.fecha, TB2.valor, TB1.vigencia, TB1.estado FROM ccpetrp AS TB1, ccpetrp_detalle AS TB2 WHERE TB1.vigencia = TB2.vigencia AND TB1.consvigencia = TB2.consvigencia AND (TB1.fecha BETWEEN '$fechaIni' AND '$fechaFin') AND (TB1.vigencia = '$vigenciaIni' OR TB1.vigencia = '$vigenciaFin')  AND TB2.seccion_presupuestal LIKE '$secPresupuestal' AND TB2.medio_pago LIKE '$medioPago' AND TB2.codigo_vigenciag LIKE '$vigGasto' AND TB2.bpim LIKE '%$bpim%' AND TB2.indicador_producto LIKE '%$programatico%' AND TB2.cuenta LIKE '%$cuenta%' AND TB1.tipo_mov = '201' AND TB2.tipo_mov LIKE '201' AND TB2.fuente LIKE '$fuente'";
        $resRp = mysqli_query($linkbd, $sqlRp);
        while ($rowRp = mysqli_fetch_row($resRp)) {

            if ($rowRp) {

                $sqlRpRev = "SELECT COALESCE(SUM(DET.valor),0) FROM ccpetrp_detalle AS DET, ccpetrp AS CAB WHERE CAB.consvigencia = DET.consvigencia AND CAB.vigencia = DET.vigencia AND CAB.fecha BETWEEN '$fechaIni' AND '$fechaFin' AND CAB.consvigencia = '$rowRp[0]'  AND CAB.vigencia = '$rowRp[3]' AND (DET.vigencia = '$vigenciaIni' OR DET.vigencia = '$vigenciaFin') AND DET.cuenta = '%$cuenta%' AND CAB.tipo_mov LIKE '4%' AND DET.tipo_mov LIKE '4%' AND DET.bpim LIKE '%$bpim%' AND DET.indicador_producto LIKE '%$programatico%' AND DET.seccion_presupuestal = '$secPresupuestal' AND DET.fuente = '$fuente' AND DET.medio_pago = '$medioPago' AND DET.codigo_vigenciag = '$vigGasto'";
                $rowRpRev = mysqli_fetch_row(mysqli_query($linkbd, $sqlRpRev));

                $valor = 0;
                $valor = $rowRp[2] - $rowRpRev[0];

                $datos = array();

                $estado = $rowRp[4] != 'N' ? 'Activo' : 'ANULADO';

                array_push($datos, "D");
                array_push($datos, "Compromisos");
                array_push($datos, $rowRp[0]);
                array_push($datos, $rowRp[1]);
                array_push($datos, $estado);
                array_push($datos, $valor);
                array_push($datos, $rowRp[3]);
                $totalRp += $valor;
                array_push($auxiliar, $datos);
            }
        }

        $datos = array();

        array_push($datos, "T");
        array_push($datos, " Total Compromisos");
        array_push($datos, "");
        array_push($datos, "");
        array_push($datos, "");
        array_push($datos, $totalRp);
        array_push($auxiliar, $datos);

        $totalCxp = 0;

        $sqlCxp = "SELECT CAB.id_orden, CAB.fecha, DET.valor, CAB.estado FROM tesoordenpago_det AS DET, tesoordenpago AS CAB WHERE CAB.id_orden = DET.id_orden AND CAB.fecha BETWEEN '$fechaIni' AND '$fechaFin' AND (DET.vigencia = '$vigenciaIni' OR DET.vigencia = '$vigenciaFin') AND DET.cuentap LIKE '%$cuenta%' AND DET.seccion_presupuestal LIKE '$secPresupuestal' AND DET.medio_pago LIKE '$medioPago' AND DET.codigo_vigenciag LIKE '$vigGasto' AND DET.fuente LIKE '$fuente' AND DET.bpim LIKE '%$bpim%' AND DET.indicador_producto LIKE '%$programatico%' AND CAB.estado != 'R'";
        $resCxp = mysqli_query($linkbd, $sqlCxp);
        while ($rowCxp = mysqli_fetch_row($resCxp)) {

            if ($rowCxp) {

                $datos = array();

                array_push($datos, "D");
                array_push($datos, "Obligacion");
                array_push($datos, $rowCxp[0]);
                array_push($datos, $rowCxp[1]);
                array_push($datos, 'Activo');
                array_push($datos, $rowCxp[2]);
                $totalCxp += $rowCxp[2];
                array_push($auxiliar, $datos);
            }
        }

        $sqlCxpNomina = "SELECT HN.id_aprob, HN.fecha, HD.valor FROM humnom_presupuestal AS HD, humnomina_aprobado AS HN WHERE HN.id_nom = HD.id_nom AND HN.fecha BETWEEN '$fechaIni' AND '$fechaFin' AND HN.estado!='N' AND HD.estado='P' AND HD.cuenta LIKE '%$cuenta%' AND HD.seccion_presupuestal LIKE '$secPresupuestal' AND HD.vigencia_gasto LIKE '$vigGasto' AND HD.medio_pago LIKE '$medioPago' AND HD.fuente LIKE '$fuente' AND HD.bpin LIKE '%$bpim%' AND HD.indicador LIKE '%$programatico%'";
        $resCxpNomina = mysqli_query($linkbd, $sqlCxpNomina);
        while ($rowCxpNomina = mysqli_fetch_row($resCxpNomina)) {
            
            if ($rowCxpNomina) {

                $datos = array();

                array_push($datos, "D");
                array_push($datos, "Obligacion Nomina");
                array_push($datos, $rowCxpNomina[0]);
                array_push($datos, $rowCxpNomina[1]);
                array_push($datos, 'Activo');
                array_push($datos, $rowCxpNomina[2]);
                $totalCxp += $rowCxpNomina[2];
                array_push($auxiliar, $datos);
            }
        }

        $datos = array();

        array_push($datos, "T");
        array_push($datos, " Total Obligaciones");
        array_push($datos, "");
        array_push($datos, "");
        array_push($datos, "");
        array_push($datos, $totalCxp);
        array_push($auxiliar, $datos);


        $totalEgresos = 0;

        $sqlEgreso = "SELECT TE.id_egreso, TE.fecha, TD.valor FROM tesoordenpago_det AS TD, tesoegresos AS TE WHERE (TD.vigencia = '$vigenciaIni' OR TD.vigencia = '$vigenciaFin') AND TE.fecha BETWEEN '$fechaIni' AND '$fechaFin' AND TE.estado != 'N' AND TE.estado != 'R' AND TD.id_orden = TE.id_orden AND TD.cuentap LIKE '%$cuenta%' AND TD.seccion_presupuestal LIKE '$secPresupuestal' AND TD.codigo_vigenciag LIKE '$vigGasto' AND TD.medio_pago LIKE '$medioPago' AND TD.bpim LIKE '%$bpim%' AND TD.indicador_producto LIKE '%$programatico%' AND TD.fuente LIKE '$fuente'";
        $resEgreso = mysqli_query($linkbd, $sqlEgreso);
        while ($rowEgreso = mysqli_fetch_row($resEgreso)) {
            
            if ($rowEgreso) {

                $datos = array();

                array_push($datos, "D");
                array_push($datos, "Egreso");
                array_push($datos, $rowEgreso[0]);
                array_push($datos, $rowEgreso[1]);
                array_push($datos, 'Activo');
                array_push($datos, $rowEgreso[2]);
                $totalEgresos += $rowEgreso[2];
                array_push($auxiliar, $datos);
            }
        }

        $sqlEgresoNomina = "SELECT TE.id_egreso, TE.fecha, TD.valor FROM tesoegresosnomina_det AS TD, tesoegresosnomina AS TE WHERE (TE.vigencia = '$vigenciaIni' OR TE.vigencia = '$vigenciaFin') AND TE.fecha BETWEEN '$fechaIni' AND '$fechaFin' AND TD.id_egreso = TE.id_egreso AND TE.estado = 'S' AND NOT(TD.tipo='SE' OR TD.tipo='PE' OR TD.tipo='DS' OR TD.tipo='RE' OR TD.tipo='FS') AND TD.cuentap LIKE '%$cuenta%' AND TD.seccion_presupuestal LIKE '$secPresupuestal' AND TD.vigencia_gasto LIKE '$vigGasto' AND TD.medio_pago LIKE '$medioPago' AND TD.fuente LIKE '$fuente' AND TD.bpin LIKE '%$bpim%' AND TD.indicador_producto LIKE '%$programatico%'";
        $resEgresoNomina = mysqli_query($linkbd, $sqlEgresoNomina);
        while ($rowEgresoNomina = mysqli_fetch_row($resEgresoNomina)) {
            
            if ($rowEgresoNomina) {
 
                $datos = array();

                array_push($datos, "D");
                array_push($datos, "Egreso Nomina");
                array_push($datos, $rowEgresoNomina[0]);
                array_push($datos, $rowEgresoNomina[1]);
                array_push($datos, 'Activo');
                array_push($datos, $rowEgresoNomina[2]);
                $totalEgresos += $rowEgresoNomina[2];
                array_push($auxiliar, $datos);
            }
        }

        $datos = array();

        array_push($datos, "T");
        array_push($datos, " Total Egresos");
        array_push($datos, "");
        array_push($datos, "");
        array_push($datos, "");
        array_push($datos, $totalEgresos);
        array_push($auxiliar, $datos);

        $totalSaldo = $definitivo - $totalCdp;
        
        

        $datos = array();

        array_push($datos, "T");
        array_push($datos, " Total Saldo");
        array_push($datos, "");
        array_push($datos, "");
        array_push($datos, "");
        array_push($datos, $totalSaldo);
        array_push($auxiliar, $datos);

        $out['auxiliar'] = $auxiliar;
    }


    header("Content-type: application/json");
    echo json_encode($out);
    die();