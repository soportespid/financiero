var app = new Vue({
    el: '#myapp',
	data:{

        error: '',
        loading: false,
        
        cuenta: '',
        vigGasto: '',
        secPresupuestal: '',
        fuente: '',
        medioPago: '',
        cpc: '',
        fechaIni: '',
        fechaFin: '',
        detalles: [],
    },

    mounted: function(){

        this.traeDatosFormulario();
    },

    methods: 
    {
        //Desglosar array
        toFormData: function(obj){
            var form_data = new FormData();
            for(var key in obj){
                form_data.append(key, obj[key]);
            }
            return form_data;
        },

        formatonumero: function(valor){
            return new Intl.NumberFormat("en-US", {style: "currency", currency: "USD"}).format(valor);
        },

        traeParametros: function(name){
            //Captura parametros
            name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
            var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
            results = regex.exec(location.search);
            return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
        },

        traeDatosFormulario: async function() {

            this.vigGasto = this.traeParametros('vig');
            this.secPresupuestal = this.traeParametros('sec');
            this.cuenta = this.traeParametros('cuenta');
            this.fuente = this.traeParametros('fuente');
            this.medioPago = this.traeParametros('medio');
            this.cpc = this.traeParametros('cpc');
            this.fechaIni = this.traeParametros('fechaIni');
            this.fechaFin = this.traeParametros('fechaFin');

            var formData = new FormData();

            formData.append("vigGasto", this.vigGasto);
            formData.append("secPresupuestal", this.secPresupuestal);
            formData.append("cuenta", this.cuenta);
            formData.append("fuente", this.fuente);
            formData.append("medioPago", this.medioPago);
            formData.append("cpc", this.cpc);
            formData.append("fechaIni", this.fechaIni);
            formData.append("fechaFin", this.fechaFin);
            
            await axios.post('presupuesto_ccpet/ejecucionGastos/ccp-ejecuGastosAuxiliar.php?action=datosFormulario', formData)
            .then((response) => {
                
                console.log(response.data);
                this.detalles = response.data.auxiliar;
            }).catch((error) => {
                this.error = true;
                console.log(error)
            }).finally(() => {
                
               
            });
        },

        excel: function() {

            if (this.detalles != '') {

                document.form2.action="ccp-excelAuxiliarGastos.php";
				document.form2.target="_BLANK";
				document.form2.submit(); 
            }
            else {
                Swal.fire("Faltan datos", "warning");
            }
        },


        seleccionaDetalle(auxiliar) {

            if (auxiliar[0] == 'D') {

                switch (auxiliar[1]) {
                    case 'Adición':
                        var x = "ccp-visualizaAdicion.php?id="+auxiliar[2];
                        window.open(x, '_blank');
                        window.focus();
                    break;

                    case 'Reducción':
                        var x = "ccp-visualizaReduccion.php?id="+auxiliar[2];
                        window.open(x, '_blank');
                        window.focus();
                    break;

                    case 'Credito':
                        var x = "ccp-visualizaTraslado.php?id="+auxiliar[2];
                        window.open(x, '_blank');
                        window.focus();
                    break

                    case 'ContraCredito':
                        var x = "ccp-visualizaTraslado.php?id="+auxiliar[2];
                        window.open(x, '_blank');
                        window.focus();
                    break

                    case 'Disponibilidad':
                        var x = "ccp-cdpVisualizar.php?is="+auxiliar[2]+"&vig="+auxiliar[6];
                        window.open(x, '_blank');
                        window.focus();
                    break;

                    case 'Compromisos':
                        var x = "ccp-rpVisualizar.php?is="+auxiliar[2]+"&vig="+auxiliar[6];
                        window.open(x, '_blank');
                        window.focus();
                    break;

                    case 'Obligacion':
                        var x = "teso-egresoverccpet.php?idop="+auxiliar[2];
                        window.open(x, '_blank');
                        window.focus();
                    break;

                    case 'Obligacion Nomina':
                        var x = "hum-aprobarnominaver.php?idr="+auxiliar[2];
                        window.open(x, '_blank');
                        window.focus();
                    break;
            
                    case 'Egreso':
                        var x = "teso-girarchequesver-ccpet.php?idegre="+auxiliar[2];
                        window.open(x, '_blank');
                        window.focus();
                    break;
                    
                    case 'Egreso Nomina':
                        var x = "teso-pagonominaver.php?idegre="+auxiliar[2];
                        window.open(x, '_blank');
                        window.focus();
                    break;

                    default:
                        break;
                }
            }
        },
    }
});