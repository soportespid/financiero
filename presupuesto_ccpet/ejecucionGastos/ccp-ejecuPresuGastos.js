import { buscaNombreRubros, ordenarArbol } from './../../funciones.js'

var app = new Vue({
    el: '#myapp',
    components: { Multiselect: window.VueMultiselect.default },
	data:{

        error: '',
        loading: false,
        
        seccionPresupuestal: [],
        vigenciaGasto: [],

        secPresu: '',
        vigGasto: '',
        medioPago: '',

        detalles: [],

        myStyle: {
            backgroundColor: '#80DBF5',
            'font-weight': 800,
            color: '#424343',
        },

        fuentes: [],
        valueFuentes: [],
        optionsFuentes: [],
        fuentesUsadas: [],

        rubrosInicial: [],
        cuentasFun: [],

        results: [],
        resultsBuscar: [],

        accountsMayor: [],
        accountsAux: [],

        checked: '',
    },

    mounted: function(){

        const vigencia = new Date().getFullYear();
        const mes = new Date().getMonth() + 1; 
        const day = new Date().getDate();

        const mesActual = mes.toString().padStart(2, '0');
        const diaActual = day.toString().padStart(2, '0');

        document.getElementById('fechaIni').value = '01'+'/'+'01'+'/'+vigencia;
        document.getElementById('fechaFin').value = diaActual+'/'+mesActual+'/'+vigencia;

        this.datosIniciales();

        this.fuentesOpciones();
    },

    methods: 
    {
        //Desglosar array
        toFormData: function(obj){
            var form_data = new FormData();
            for(var key in obj){
                form_data.append(key, obj[key]);
            }
            return form_data;
        },

        formatonumero: function(valor){
            return new Intl.NumberFormat("en-US", {style: "currency", currency: "USD"}).format(valor);
        },

        traeParametros: function(name){
            //Captura parametros
            name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
            var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
            results = regex.exec(location.search);
            return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
        },

        async fuentesOpciones(){
            let fechaIn = document.getElementById('fechaIni').value;
            let partesFecha = fechaIn.split('/');
            let formDataDet = new FormData();
            formDataDet.append("vigencia", partesFecha[2])
            await axios.post('vue/presupuesto_ccp/ccp-ejecucionpresupuestal-vue.php?action=fuentes', formDataDet)
                .then(
                    (response) => {
                        this.fuentes = response.data.fuentes;
                        this.fuentesUsadas = response.data.fuentesUsadas;
                        /* this.vigenciasdelgasto = response.data.vigenciasDelGasto; */
                        /* this.selectVigenciaGasto = this.vigenciasdelgasto[0][0]; */
                    }
                );
            const dataArrSet = new Set(this.fuentesUsadas);
            const dataArr = Array.from(dataArrSet);
            const fuentesOrg = await dataArr.map((e) => {
                return {fuente : e, nombre : this.buscarFuente(e)}
            });
            this.optionsFuentes = fuentesOrg;
        },

        buscarFuente(fuente){
            const nombFuente = this.fuentes.find(e => e[0] == fuente)
            const nombF = typeof nombFuente == 'object' ? Object.values(nombFuente) : ''

            return typeof nombFuente == 'string' ? '' : nombF[1];
        },

        datosIniciales: async function() {

            let fechaIn = document.getElementById('fechaIni').value;
            let partesFecha = fechaIn.split('/');
            let formDataDet = new FormData();
            formDataDet.append("vigencia", partesFecha[2])
            
            await axios.post('presupuesto_ccpet/ejecucionGastos/ccp-ejecuPresuGastos.php?action=datosIniciales', formDataDet)
            .then((response) => {
                
                this.seccionPresupuestal = response.data.secPresupuestal;
                this.vigenciaGasto = response.data.vigGasto;
                this.rubrosInicial = response.data.rubrosInicial;
                this.cuentasFun = response.data.cuentasFun;
            }).catch((error) => {
                this.error = true;
                console.log(error)
            }).finally(() => {
                this.loading = false;
                this.auxiliarMayor();
                this.orderAccounts();
            });
        },

        async orderAccounts(){

            const arrPuro = this.rubrosInicial.map((e) => {
                return e[0]
            });

            const dataArrSet = new Set(arrPuro);

            const dataArr = Array.from(dataArrSet);

            const resData = buscaNombreRubros(dataArr, this.accountsAux);

            this.results = ordenarArbol(this.accountsMayor, resData);

            this.resultsBuscar = [...this.results];

        },

        auxiliarMayor(){
            this.accountsAux = this.cuentasFun.filter(element => element[2] == 'C');
            this.accountsMayor = this.cuentasFun.filter(element => element[2] == 'A');
        },

        filtrarRubrosConCpc(){
            this.cambiaCriteriosBusqueda();
            this.results = [];
            if(this.checked){
                const dataRubrosConCpc = this.resultsBuscar.filter(element => element[0].substring(0, 8) == '2.1.2.02');
                this.results = dataRubrosConCpc;
            }else{
                this.results = this.resultsBuscar;
            }
        },

        traeDatosFormulario: async function() {

            await this.fuentesOpciones();
            await this.datosIniciales();
            
            let fechaIni = document.getElementById('fechaIni').value;
            let fechaFin = document.getElementById('fechaFin').value;

            if (fechaIni != "" && fechaFin != "") {
                
                this.loading = true;

                let formData1 = new FormData();

                for(let i=0; i <= this.results.length-1; i++){
                    const val = this.results[i].length-1;
                    
                    for(let x = 0; x <= val; x++){
                        formData1.append("cuentas["+i+"][]", this.results[i][x]);
                    }
                }

                this.valueFuentes.forEach((e, i) => {
                    formData1.append("fuentes["+i+"]", e.fuente);
                });

                await axios.post('presupuesto_ccpet/ejecucionGastos/ccp-ejecuPresuGastos.php?action=datosFormulario&fechaIni='+fechaIni+'&fechaFin='+fechaFin+'&secPresu='+this.secPresu+'&medioPago='+this.medioPago+'&vigGasto='+this.vigGasto, formData1)
                .then((response) => {
                    this.detalles = response.data.detalles;
                }).catch((error) => {
                    this.error = true;
                    console.log(error)
                }).finally(() => {
                    this.loading = false;
                    
                });
            }
            else {
                Swal.fire("Error", "Debes seleccionar fechas", "warning");
            }
        },

        esBienesServicios(rubro){
            return rubro.substring(0, 8) == '2.1.2.02';
        },

        fuenteConNombre({ fuente, nombre }){
            return `${fuente} - ${nombre}`
        },

        cambiaCriteriosBusqueda: function(){
            this.detalles = [];
        },

        auxiliarDetalle: function(aux) {
            if (aux[0] == 'C' || aux[0] == 'D') {

                var fechaIni = document.getElementById('fechaIni').value;
                var fechaFin = document.getElementById('fechaFin').value;

                var x = "ccp-ejecuGastosAuxiliar.php?vig="+aux[1]+"&sec="+aux[2]+"&cuenta="+aux[3]+"&fuente="+aux[5]+"&medio="+aux[6]+"&cpc="+aux[13]+"&fechaIni="+fechaIni+"&fechaFin="+fechaFin;
                window.open(x, '_blank');
                window.focus();
            }
        },

        excel: function() {

            if (this.detalles != '') {

                document.form2.action="ccp-excelPresuGastos.php";
				document.form2.target="_BLANK";
				document.form2.submit(); 
            }
            else {
                Swal.fire("Faltan datos", "warning");
            }
        },
    }
});