<?php
    require_once '../../../Librerias/core/Helpers.php';
    require_once '../../../Librerias/core/Mysql.php';
    require_once '../../../funciones.inc';
    session_start();
    
    class cdpModel extends Mysql {
        private $cedula;
        private $vigencia;
        private $versionGastos;
        private $versionFuentes;

        function __construct(){
            parent::__construct();
            $this->cedula = $_SESSION["cedulausu"];
            $this->vigencia = getVigenciaUsuario($this->cedula);
            $this->versionGastos = getVersionGastosCcpet();
            $this->versionFuentes = getVersionFuentesCcpet();
        }

        /* Read data */
        public function get_cdps() {
            $data = [];
            $sql = "SELECT consvigencia, fecha, objeto, valor 
            FROM ccpetcdp 
            ORDER BY consvigencia DESC";
            $cdps = $this->select_all($sql);
    
            foreach ($cdps as &$cdp) {
                if ($this->valida_exist_cdp($cdp["consvigencia"]) == 0) {
                    $dataArray = array(
                        "consvigencia" => $cdp["consvigencia"],
                        "fecha" => $cdp["fecha"],
                        "objeto" => $cdp["objeto"],
                        "valor" => $cdp["valor"],
                        "bpim" => $this->get_bpim_and_area($cdp["consvigencia"])["bpim"],
                        "area" => $this->get_bpim_and_area($cdp["consvigencia"])["area"],
                        "nombre_area" => $this->get_nombre_area($this->get_bpim_and_area($cdp["consvigencia"])["area"])
                    );

                    array_push($data, $dataArray);
                }
            }

            return $data;
        }

        public function get_areas() {
            $sql = "SELECT codarea AS codigo, nombrearea AS nombre FROM planacareas WHERE estado = 'S' ORDER BY codarea";
            $data = $this->select_all($sql);
            return $data;
        }

        /* valida si el cdp ya existe en el registro de contrato */
        private function valida_exist_cdp($cdp) {
            $sql = "SELECT COUNT(*) AS total_row FROM ccp_registro_contrato WHERE cdp = '$cdp' AND vigencia = '$this->vigencia' AND estado <> 'N'";
            $data = $this->select($sql);
            return $data["total_row"];
        }

        private function get_bpim_and_area($cdp) {
            $sql = "SELECT bpim, area FROM ccpetcdp_detalle WHERE consvigencia = '$cdp' AND vigencia = '$this->vigencia' LIMIT 1";
            $data = $this->select($sql);
            return $data;
        }

        private function get_nombre_area($codArea) {
            $sql = "SELECT nombrearea FROM planacareas WHERE codarea = '$codArea'";
            $data = $this->select($sql);
            return $data["nombrearea"];
        }

        public function get_data_solicitud($codCdp) {
            $idSolicitud = $this->get_id_solicitud($codCdp);
            $sql = "SELECT id_paa, tipo_contrato_o_acto AS tipo_documento, tipo_contrato, numero_acto, fecha_acto FROM plan_solicitud_cdp WHERE id = '$idSolicitud'";
            $data = $this->select($sql);
            if ($data["id_paa"] != 0) {
                $paa = $this->get_paa($data["id_paa"]);
                $data["codigo_paa"] = $paa["codigo"];
                $data["nombre_modalidad"] = $paa["nombre_modalidad"];
                $data["duracion"] = $paa["duracionest"];
            } else {
                $data["codigo_paa"] = "0";
                $data["nombre_modalidad"] = "NO APLICA";
                $data["duracion"] = 0;
            }
            return $data;
        }

        private function get_id_solicitud($codCdp) {
            $sql = "SELECT id_solicitud_cdp FROM ccpet_solicitud_cdp WHERE consecutivo = '$codCdp' AND vigencia = '$this->vigencia'";
            $data = $this->select($sql);
            return $data["id_solicitud_cdp"];
        }

        private function get_paa($idPaa) {
            $sql = "SELECT paa.codplan AS codigo, modalidad.nombre AS nombre_modalidad, paa.duracionest FROM contraplancompras AS paa INNER JOIN plan_modalidad_seleccion AS modalidad ON paa.modalidad = modalidad.codigo WHERE paa.id = $idPaa";
            $data = $this->select($sql);
            return $data;
        }

        private function get_search_consecutivo() {
            $sql = "SELECT MAX(codigo) AS consecutivo FROM ccp_registro_contrato WHERE vigencia = '$this->vigencia'";
            $data = $this->select($sql);
            return $data["consecutivo"];
        }

        private function get_terceroId($documento){
            $sql = "SELECT id_tercero FROM terceros WHERE cedulanit = '$documento'";
            $data = $this->select($sql);
            return $data["id_tercero"];
        }

        public function get_bancos($documento) {
            $terceroId = $this->get_terceroId($documento);
            $sql = "SELECT tipo_cuenta, codigo_banco, numero_cuenta FROM teso_cuentas_terceros WHERE id_tercero = $terceroId";
            $data = $this->select_all($sql);
            return $data;
        }

        private function get_acto_adm($cdp) {
            $idSolicitud = $this->get_id_solicitud($cdp);
            $sql_solicitud = "SELECT numero_acto FROM plan_solicitud_cdp WHERE id = $idSolicitud";
            $acto_adm = $this->select($sql_solicitud)["numero_acto"];
            return $acto_adm;
        }

        public function get_data_search() {
            $sql = "SELECT vigencia, codigo, fecha, cdp, contrato, tercero, objeto, valor, estado, id FROM ccp_registro_contrato WHERE vigencia = '$this->vigencia' ORDER BY codigo DESC";
            $data = $this->select_all($sql);

            foreach ($data as &$d) {
                $d["acto_adm"] = $this->get_acto_adm($d["cdp"]);
                $d["beneficiario"] = getNombreTercero($d["tercero"]);
            }

            return $data;
        }

        public function get_data_edit($id) {
            $sql = "SELECT codigo AS consecutivo, contrato, fecha, cdp, tercero, valor, plazo, plazo_dias, garantias, objeto, forma_pago, cuenta_banco, estado FROM ccp_registro_contrato WHERE id = $id";
            $data = $this->select($sql);

            $idSolicitud = $this->get_id_solicitud($data["cdp"]);

            $sql_solicitud = "SELECT tipo_contrato_o_acto AS tipo_documento, tipo_contrato, numero_acto, fecha_acto, id_paa FROM plan_solicitud_cdp WHERE id = '$idSolicitud'";
            $data_solicitud = $this->select($sql_solicitud);

            $data["tipo_documento"] = $data_solicitud["tipo_documento"];
            $data["tipo_contrato"] = $data_solicitud["tipo_contrato"];
            $data["numero_acto"] = $data_solicitud["numero_acto"];
            $data["fecha_acto"] = $data_solicitud["fecha_acto"];
            $paa = $this->get_paa($data_solicitud["id_paa"]);
            $data["codigo_paa"] = $paa["codigo"] == 0 ? "NO APLICA" : $paa["codigo"];
            $data["nombre_modalidad"] = $paa["codigo"] == 0 ? "NO APLICA" : $paa["nombre_modalidad"];
            $data["nombre_tercero"] = getNombreTercero($data["tercero"]);

            return $data;
        }
        
        public function get_data_edit_det($codigo) {
            $sql = "SELECT descripcion, unidad, cantidad, valor_unitario AS valor, valor_total AS subtotal FROM ccp_registro_contrato_det WHERE codigo = $codigo";
            $data = $this->select_all($sql);
            return $data;
        }

        public function get_consecutivos() {
            $sql = "SELECT id FROM ccp_registro_contrato";
            $data = $this->select_all($sql);
            return $data;
        }

        /* Create data */
        public function insert_cab($data, $items) {
            $consecutivo = $this->get_search_consecutivo();
            $sql = "INSERT INTO ccp_registro_contrato (codigo, contrato, fecha, vigencia, cdp, tercero, valor, plazo, plazo_dias, garantias, objeto, estado, forma_pago, cuenta_banco) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
            $cab = [
                $consecutivo + 1,
                $data["contrato"],
                $data["fecha"],
                $this->vigencia,
                $data["cdp"],
                $data["tercero"],
                $data["valor"],
                $data["plazo"],
                $data["plazo_dias"],
                $data["garantias"],
                $data["objeto"],
                "S",
                $data["formaPago"],
                $data["cuentaBanco"]
            ];

            $id = $this->insert($sql, $cab);
            $this->create_auditoria("Crear", $consecutivo + 1);
            if ($id > 0) {
                $this->insert_items($items, $consecutivo + 1);
            }

            return $id;
        }

        private function insert_items($items, $consecutivo) {
            $sql = "INSERT INTO ccp_registro_contrato_det (codigo, descripcion, unidad, cantidad, valor_unitario, valor_total) VALUES (?, ?, ?, ?, ?, ?)";
            
            foreach ($items as $item) {
                $data = [
                    $consecutivo, 
                    $item["descripcion"],
                    $item["unidad"],
                    $item["cantidad"],
                    $item["valor"],
                    $item["subtotal"]
                ];    
    
                $this->insert($sql, $data);
            }            
        }

        private function create_auditoria(string $accion, int $consecutivo) {
            $sql_funcion = "SELECT id FROM ccpet_funciones WHERE nombre = 'Registro Contrato'";
            $funcion = $this->select($sql_funcion);

            insertAuditoria("ccpet_auditoria","ccpet_funciones_id",$funcion["id"],$accion,$consecutivo);
        }

        /* Update data */
        public function anula_registro($id) {
            $sql_consecutivo = "SELECT codigo FROM ccp_registro_contrato WHERE id = $id";
            $data = $this->select($sql_consecutivo);
            $consecutivo = $data["codigo"];
            
            $sql = "UPDATE ccp_registro_contrato SET estado = ? WHERE id = $id";
            $result = $this->update($sql, ["N"]);
            $this->create_auditoria("Anular", $consecutivo);
            return $result;
        }

        /* Delate data */
       
    }
?>