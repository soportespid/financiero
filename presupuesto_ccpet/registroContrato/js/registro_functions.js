const URL = "presupuesto_ccpet/registroContrato/controllers/registro_controller.php";
// const URLEXCEL = "";
const URLPDF = "presupuesto_ccpet/registroContrato/controllers/registroExport_controller.php";

var app = new Vue({
    el:"#myapp",
    data() {
        return {
            /* general */
            isLoading: false,
            txtSearch: '',
            id: 0,
            tabs: '1',

            fecha: '',
            codCdp: '',
            objeto: '',
            valor: '',
            valorFormat: '',
            valorEstimado: '',
            tipoDocumento: '',
            codPaa: '',
            nombreModalidad: '',
            numero_acto: '',
            fecha_acto: '',
            tipoContrato: '',
            numeroContrato: '',
            duracion: 0,
            duracion_dias: 0,
            garantias: '',
            terceroDoc: '',
            terceroName: '',
            formaPago: '',

            /* modal cdps */
            modalCdp: false,
            cdps: [], cdpsCopy: [], 
            areas: [], selectArea: 0,
            fechaIni: '', fechaFin: '',

            /* modal terceros */
            terceros: [], tercerosCopy: [],
            modalTercero: false,

            /* items */
            descripcion: '',
            unidadMedida: '',
            cantidad: '',
            valorUnitario: '',
            valorUnitarioFormat: '',
            subTotal: '',
            totalItems: '',
            arrayItems: [],

            /* bancos */
            numCuentaBanco: '',
            bancos: [], bancosCopy: [],
            modalBanco: false,
            txtSearchBanco: '',

            /* pagina buscar */
            arrSearch: [], arrSearchCopy: [],
            txtConsecutivo: '',
            txtCdp: '',
            txtContrato: '',
            txtTercero: '',
            txtObjeto: '',

            /* pagina visualizar */
            consecutivo: '',
            arrConsecutivos: [],
            estado: '',
        }
    },
    mounted() {
        this.intPageVal = this.$refs.pageType.value;

        if (this.intPageVal == 1) {
            this.get();
        } else if (this.intPageVal == 2) {
            this.getSearch();
        } else if (this.intPageVal == 3) {
            this.getVisualizar();
        }
    },
    methods: {
        /* metodos para traer información */
        async get() {
            const formData = new FormData();
            formData.append("action","get");
            this.isLoading = true;
            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();
            this.cdps = this.cdpsCopy = objData.cdps;
            this.areas = objData.areas;
            this.terceros = this.tercerosCopy = objData.terceros;
            this.isLoading = false;
        },

        async getSearch() {
            const formData = new FormData();
            formData.append("action","getSearch");
            this.isLoading = true;
            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();
            this.arrSearch = this.arrSearchCopy = objData.arrSearch;
            this.isLoading = false;
        },

        async getVisualizar() {
            this.id = new URLSearchParams(window.location.search).get('id');
            const formData = new FormData();
            formData.append("action","visualizar");
            formData.append("id",this.id);
            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();
            this.consecutivo = objData.arrCab.consecutivo;
            this.fecha = objData.arrCab.fecha;
            this.codCdp = objData.arrCab.cdp;
            this.tipoDocumento = objData.arrCab.tipo_documento;
            this.tipoContrato = objData.arrCab.tipo_contrato;
            this.numeroContrato = objData.arrCab.contrato;
            this.numero_acto = objData.arrCab.numero_acto;
            this.fecha_acto = objData.arrCab.fecha_acto;
            this.objeto = objData.arrCab.objeto;
            this.terceroDoc = objData.arrCab.tercero;
            this.terceroName = objData.arrCab.nombre_tercero;
            this.numCuentaBanco = objData.arrCab.cuenta_banco;
            this.codPaa = objData.arrCab.codigo_paa;
            this.nombreModalidad = objData.arrCab.nombre_modalidad;
            this.valorFormat = objData.arrCab.valor;
            this.duracion = objData.arrCab.plazo;
            this.duracion_dias = objData.arrCab.plazo_dias;
            this.garantias = objData.arrCab.garantias;
            this.formaPago = objData.arrCab.forma_pago;
            this.estado = objData.arrCab.estado;
            this.arrayItems = objData.arrDet;
            this.totalItems = this.arrayItems.reduce((acc, item) => acc + parseFloat(item.subtotal), 0);
            this.arrConsecutivos = objData.arrIds;
        },

        async getDataSolicitud(codCdp) {
            const formData = new FormData();
            formData.append("action","dataSolicitud");
            formData.append("codCdp",codCdp);
            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();
            this.tipoDocumento = objData.dataSolicitud.tipo_documento;
            this.tipoContrato = objData.dataSolicitud.tipo_contrato;
            this.codPaa = objData.dataSolicitud.codigo_paa;
            this.nombreModalidad = objData.dataSolicitud.nombre_modalidad
            this.numero_acto = objData.dataSolicitud.numero_acto;
            this.fecha_acto = objData.dataSolicitud.fecha_acto;
            this.duracion = objData.dataSolicitud.duracion;
        },

        async getBancos() {
            if (this.terceroDoc == "") {
                Swal.fire("Atención", "Por favor, selecciona un tercero para visualizar los bancos disponibles.","warning");
                return false;
            }
            const formData = new FormData();
            formData.append("action","bancos");
            formData.append("documento",this.terceroDoc);
            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();
            this.bancos = this.bancosCopy = objData.bancos;
        },

        /* metodos para procesar información */
        deployModal(type="") {
            if (type=="bancos") {
                this.getBancos();
                this.modalBanco = true;
            }
        },

        filter(type="") {
            if(type=="area"){
                if (this.selectArea != 0) {
                    this.cdps = [...this.cdpsCopy.filter((e)=>{return e.area==app.selectArea})];
                } else {
                    this.cdps = this.cdpsCopy;    
                }
                
            } else if (type=="modalCdpTxt") {
                let search = this.txtSearch.toLowerCase();
                this.cdps = [...this.cdpsCopy.filter(e=>e.consvigencia.toLowerCase().includes(search) || e.bpim.toLowerCase().includes(search) || e.objeto.toLowerCase().includes(search))];
            } else if (type=="modalTerceroTxt") {
                let search = this.txtSearch.toLowerCase();
                this.terceros = [...this.tercerosCopy.filter(e=>e.cedulanit.toLowerCase().includes(search) || e.nombre.toLowerCase().includes(search))];
            } else if (type=="modalBancoTxt") {
                let search = this.txtSearch.toLowerCase();
                this.bancos = [...this.bancosCopy.filter(e=>e.numero_cuenta.toLowerCase().includes(search) || e.tipo_cuenta.toLowerCase().includes(search))];
            }
        },

        filterByDate() {
            if (this.fechaIni == "" || this.fechaFin == "") {
                this.cdps = this.cdpsCopy;
                return false;
            }
        
            const start = new Date(this.fechaIni);
            const end = new Date(this.fechaFin);
    
            this.cdps = this.cdpsCopy.filter(cdp => {
                const cdpDate = new Date(cdp.fecha);
                return cdpDate >= start && cdpDate <= end;
            });
        },

        filtersSearch(type) {
            if (type=="consecutivo") {
                let search = this.txtConsecutivo.toLowerCase();
                this.arrSearch = [...this.arrSearchCopy.filter(e=>e.codigo.toLowerCase().includes(search))];
            } else if (type=="cdp") {
                let search = this.txtCdp.toLowerCase();
                this.arrSearch = [...this.arrSearchCopy.filter(e=>e.cdp.toLowerCase().includes(search))];
            } else if (type=="contrato") {
                let search = this.txtContrato.toLowerCase();
                this.arrSearch = [...this.arrSearchCopy.filter(e=>e.contrato.toLowerCase().includes(search) || e.acto_adm.toLowerCase().includes(search))];
            } else if (type=="tercero") {
                let search = this.txtTercero.toLowerCase();
                this.arrSearch = [...this.arrSearchCopy.filter(e=>e.tercero.toLowerCase().includes(search) || e.beneficiario.toLowerCase().includes(search))];
            } else if (type=="objeto") {
                let search = this.txtObjeto.toLowerCase();
                this.arrSearch = [...this.arrSearchCopy.filter(e=>e.objeto.toLowerCase().includes(search))];
            }
        },

        selectItem(type="", item) {
            if (type=="cdp") {
                this.codCdp = item.consvigencia;
                this.objeto = item.objeto;
                this.valor = item.valor;
                this.valorFormat = this.formatNumber(item.valor);
                this.valorEstimado = item.valor;
                this.getDataSolicitud(item.consvigencia);
                this.modalCdp = false;
                this.cdps = this.cdpsCopy;
            } else if (type=="tercero") {
                this.terceroDoc = item.cedulanit;
                this.terceroName = item.nombre;
                this.terceros = this.tercerosCopy;
                this.modalTercero = false;
            } else if (type=="banco") {
                this.numCuentaBanco = item.numero_cuenta;
                this.modalBanco = false;
                this.bancos = this.bancosCopy;
            }
        },

        calcularSubTotal() {
            if (this.cantidad == "" || this.valorUnitario == "") {
                return false;
            }

            let total = parseFloat(this.cantidad) * parseFloat(this.valorUnitario);
            total = total.toFixed(2);
            this.subTotal = total;

        },

        addItem() {
            if(this.codPaa == 0) {
                Swal.fire("Atención", "Este CDP no requiere de agregar ITEMS porque seleccionaste como PAA 'No Aplica'","warning");
                return false;
            }

            if(this.descripcion == "" || this.unidadMedida =="" || this.cantidad =="" || this.valorUnitario=="" ){
				Swal.fire("Atención!","Todos los campos con (*) son obligatorios","warning");
				return false;
			}

            if (this.subTotal <= 0) {
                Swal.fire("Atención", "El valor subtotal debe ser mayor a 0 para poder agregar el ITEM","warning");
                return false;
            }

            this.arrayItems.push({
                "descripcion":this.descripcion,
                "unidad":this.unidadMedida.toUpperCase(),
                "valor":parseInt(this.valorUnitario),
                "cantidad":this.cantidad,
                "subtotal":this.subTotal
            });

            this.totalItems = this.arrayItems.reduce((acc, item) => acc + parseFloat(item.subtotal), 0);
        },

        delItem(index) {
            this.arrayItems.splice(index, 1);
            this.totalItems = this.arrayItems.reduce((acc, item) => acc + parseFloat(item.subtotal), 0);
        },

        nextItem:function(type){
            let id = this.id;
            let index = this.arrConsecutivos.findIndex(function(e){return e.id == id});
            if(type=="next" && this.arrConsecutivos[++index]){
                id = this.arrConsecutivos[index++].id;
            }else if(type=="prev" && this.arrConsecutivos[--index]){
                id = this.arrConsecutivos[index--].id;
            }
            window.location.href="ccp-registroContratoEditar"+'?id='+id;
        },

        /* Metodos para guardar o actualizar información */
        async save() {
            if (this.tipoDocumento == 1) {
                if (this.numeroContrato == "") {
                    Swal.fire("Atención", "El número de contrato no puede quedar vació","warning");
                    return false;
                }
            }

            //generales
            if (this.fecha == "" || this.codCdp == "" || this.objeto == "" || this.terceroDoc == "") {
                Swal.fire("Atención", "Todos los campos con (*) son obligatorios","warning");
                return false;
            }

            if (this.codPaa != 0) {
                if (this.duracion == "" && this.garantias == "") {
                    Swal.fire("Atención", "La duración y las garantias son obligatorias llenarlas","warning");
                    return false;
                }

                if (this.arrayItems.length == 0) {
                    Swal.fire("Atención", "Debes agregar al menos un ITEM para poder guardar la información","warning");
                    return false;
                }

                if (parseFloat(this.totalItems) != parseFloat(this.valor)) {
                    Swal.fire("Atención", "El valor total de los items debe ser igual al valor del CDP","warning");
                    return false;
                }
            }

            const formData = new FormData();
            formData.append("action","save");
            formData.append("fecha",this.fecha);
            formData.append("codCdp",this.codCdp);
            formData.append("numContrato",this.numeroContrato);
            formData.append("tercero",this.terceroDoc);
            formData.append("objeto",this.objeto);
            formData.append("plazo",this.duracion);
            formData.append("plazo_dias",this.duracion_dias);
            formData.append("garantias",this.garantias);
            formData.append("valor",this.valor);
            formData.append("formaPago",this.formaPago);
            formData.append("cuentaBanco",this.numCuentaBanco);
            formData.append("items", JSON.stringify(this.arrayItems));
            
            Swal.fire({
                title:"¿Estás segur@ de guardar?",
                text:"",
                icon: 'warning',
                showCancelButton:true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText:"Sí, guardar",
                cancelButtonText:"No, cancelar"
            }).then(async function(result) {
                if(result.isConfirmed) {
                    const response = await fetch(URL,{method:"POST",body:formData});
                    const objData = await response.json();

                    if (objData.status) {
                        Swal.fire("Guardado",objData.msg,"success");
                        if(objData.id){
                            setTimeout(function(){
                                window.location.href="ccp-registroContratoEditar"+'?id='+objData.id;
                            },1500);
                        }
                    }
                    else {
                        Swal.fire("Error!",objData.msg,"warning");
                    }
                }
            });
        },

        async anular(id, index) {
            const formData = new FormData();
            formData.append("action","anula");
            formData.append("id",id);
            Swal.fire({
                title:"¿Estás segur@ de anular?",
                text:"",
                icon: 'warning',
                showCancelButton:true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText:"Sí, anular",
                cancelButtonText:"No, cancelar"
            }).then(async function(result) {
                if(result.isConfirmed) {
                    const response = await fetch(URL,{method:"POST",body:formData});
                    const objData = await response.json();

                    if (objData.status) {
                        app.arrSearch[index].estado = 'N';
                        app.arrSearchCopy[index].estado = 'N';
                        Swal.fire("Anulado",objData.msg,"success");
                    }
                    else {
                        Swal.fire("Error!",objData.msg,"warning");
                    }
                }
            });
        },

        /* Formatos para mostrar información */
        formatInputNumber(conFormato, sinFormato) {
            this[conFormato] = this.formatNumber(this[conFormato]);
            this[sinFormato] = this[conFormato].replace(/[^0-9.]/g, "");
        },

        formatNumber(number) {
            if (!number) return ""; // Si no hay entrada, retorna vacío
    
            // Elimina caracteres no válidos (excepto números y el punto decimal)
            number = number.replace(/[^0-9.]/g, "");
    
            // Divide la parte entera y decimal
            const [integerPart, decimalPart] = number.split(".");
    
            // Formatear la parte entera con separador de miles
            const formattedInteger = integerPart.replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    
            // Si hay parte decimal, la une; si no, devuelve solo la parte entera
            return decimalPart !== undefined
                ? `${formattedInteger}.${decimalPart.slice(0, 2)}` // Limita a 2 decimales si es necesario
                : formattedInteger;
        },

        viewFormatNumber: function(valor){
            return new Intl.NumberFormat("en-US", {style: "currency", currency: "USD"}).format(valor);  
        },

        formatFecha: function(fecha) {
            // Separar la fecha en componentes
            const parts = fecha.split('-');
            if (parts.length !== 3) {
                throw new Error('Formato de fecha inválido. Use Y-m-d.');
            }
            const year = parts[0];
            const month = parts[1];
            const day = parts[2];
        
            // Retornar la fecha en el formato deseado
            return `${day}/${month}/${year}`;
        },

        /* enviar informacion excel o pdf */
        pdf: function() {
            const form = document.createElement("form");
            form.method = "post";
            form.target = "_blank";
            form.action = URLPDF;       
            
            function addField(name,value){
                const input = document.createElement("input");
                input.type="hidden";
                input.name=name;
                input.value = value;
                form.appendChild(input);
            }

            addField("action","pdf");
            addField("consecutivo",this.consecutivo);
            addField("fecha",this.fecha);
            addField("codCdp",this.codCdp);
            addField("tipoDocumento",this.tipoDocumento);
            addField("valor",this.valorFormat);
            addField("codPaa",this.codPaa);
            addField("nombreModalidad",this.nombreModalidad);
            addField("tipoContrato",this.tipoContrato);
            addField("numeroContrato",this.numeroContrato);
            addField("numero_acto",this.numero_acto);
            addField("fecha_acto",this.fecha_acto);
            addField("duracion_meses",this.duracion);
            addField("duracion_dias",this.duracion_dias);
            addField("garantias",this.garantias);
            addField("terceroDoc",this.terceroDoc);
            addField("terceroName",this.terceroName);
            addField("numCuentaBanco",this.numCuentaBanco);
            addField("objeto",this.objeto);
            addField("formaPago",this.formaPago);
            addField("estado",this.estado);
            addField("items", JSON.stringify(this.arrayItems));
            
            document.body.appendChild(form);
            form.submit();
            document.body.removeChild(form);
        },
    },
    computed:{

    }
})
