<?php
    require_once '../../../Librerias/core/Helpers.php';
    require_once '../../../tcpdf/tcpdf.php';
    require_once '../../../Librerias/core/Mysql.php';
    session_start();
    // ini_set('display_errors', '1');
    // ini_set('display_startup_errors', '1');
    // error_reporting(E_ALL);
    class RegistroExportController {
        public function exportPdf(){
            if(!empty($_SESSION)){
                $con = new Mysql();        
                $consecutivo = $_POST["consecutivo"];
                $fecha = DateTime::createFromFormat('Y-m-d', $_POST["fecha"])->format('d/m/Y');
                $vigencia = DateTime::createFromFormat('Y-m-d', $_POST["fecha"])->format('Y');
                $cdp = $_POST["codCdp"];
                $valor = number_format($_POST["valor"], 2);
                $codPaa = $_POST["codPaa"];
                $modalidad = $_POST["nombreModalidad"];
                $tipoDocumento = $_POST["tipoDocumento"];
                $duracion_meses = $_POST["duracion_meses"];
                $duracion_dias = $_POST["duracion_dias"];
                $garantias = $_POST["garantias"] == "S" ? "SI" : "NO";
                $doc_tercero = $_POST["terceroDoc"];
                $name_tercero = $_POST["terceroName"];
                $cuenta_banco = $_POST["numCuentaBanco"];
                $objeto = $_POST["objeto"];
                $forma_pago = $_POST["formaPago"];
                $estado = $_POST["estado"];
                define("ESTADO", $estado);
                $items = json_decode($_POST["items"], true);

                $sql_solicitud_id = "SELECT id_solicitud_cdp AS id FROM ccpet_solicitud_cdp WHERE consecutivo = '$cdp' AND vigencia = '$vigencia'";
                $solicitudId = $con->select($sql_solicitud_id)["id"];

                $sql_solicitud = "SELECT tipo_presupuesto FROM plan_solicitud_cdp WHERE id = $solicitudId";
                $tipoPresupuesto = $con->select($sql_solicitud)["tipo_presupuesto"];

                $sql_cuentas_fun = "SELECT cuenta, cpc, fuente, valor FROM plan_solicitud_cdp_det WHERE id_solicitud_cdp = $solicitudId AND indicador = ''";
                $cuentasFun = $con->select_all($sql_cuentas_fun);

                $sql_cuentas_inv = "SELECT s.indicador AS codIndicador, b.codigo AS bpin, s.fuente AS codFuente, s.cuenta, s.valor FROM plan_solicitud_cdp_det AS s LEFT JOIN ccpproyectospresupuesto AS b ON s.id_bpin = b.id WHERE s.id_solicitud_cdp = $solicitudId AND s.indicador != ''";
                $cuentasInv = $con->select_all($sql_cuentas_inv);
                // if(!empty($data)){
                    
                    $pdf = new MYPDF('P','mm','Letter', true, 'iso-8859-1', false);
                    $pdf->SetDocInfoUnicode (true);
                    // set document information
                    $pdf->SetCreator(PDF_CREATOR);
                    $pdf->SetAuthor('IDEALSAS');
                    $pdf->SetTitle('REGISTRO DE CONTRATO');
                    $pdf->SetSubject('REGISTRO DE CONTRATO');
                    $pdf->SetKeywords('TCPDF, PDF, example, test, guide');
                    $pdf->SetMargins(10, 38, 10);// set margins
                    $pdf->SetHeaderMargin(38);// set margins
                    $pdf->SetFooterMargin(17);// set margins
                    $pdf->SetAutoPageBreak(TRUE, 20);// set auto page breaks
                    // set some language-dependent strings (optional)
                    if (@file_exists(dirname(__FILE__).'/lang/spa.php'))
                    {
                        require_once(dirname(__FILE__).'/lang/spa.php');
                        $pdf->setLanguageArray($l);
                    }
                    $pdf->SetFillColor(255,255,255);
                    $pdf->AddPage();
                    $pdf->SetFont('helvetica','B',9);
                    $pdf->SetFillColor(153,221,255);
                    $pdf->MultiCell(190,5,"LA SECRETARIA","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->ln();
                    $pdf->SetFillColor(255,255,255);
                    $pdf->SetTextColor(0,0,0);
                    $pdf->SetFont('Helvetica','',8);
                    $pdf->MultiCell(20,5,"CONSECUTIVO:","LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->MultiCell(15,5,"$consecutivo","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->MultiCell(25,5,"CONSECUTIVO CDP:","LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->MultiCell(25,5,"$cdp","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->MultiCell(25,5,"FECHA SOLICITUD:","LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->MultiCell(25,5,"$fecha","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->MultiCell(20,5,"VALOR:","LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->MultiCell(35,5,"$$valor","LRBT",'R',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->ln();
                    $pdf->SetFont('helvetica','B',9);
                    $pdf->SetFillColor(153,221,255);
                    $pdf->MultiCell(190,5,"INFORMACIÓN GENERAL","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->ln();
                    $pdf->SetFont('Helvetica','',8);
                    $pdf->SetFillColor(255,255,255);
                    $pdf->MultiCell(35,5,"PLAN DE COMPRAS:","LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->MultiCell(25,5,"$codPaa","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->MultiCell(50,5,"MODALIDAD DE CONTRATACIÓN:","LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->MultiCell(80,5,"$modalidad","LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->ln();
                    $pdf->MultiCell(50,5,"TIPO DE PRESUPUESTO ASIGNADO:","LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->MultiCell(25,5,"FUNCIONAMIENTO","LBT",'C',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->MultiCell(21,5,$tipoPresupuesto == 1 ? "X" : "","RBT",'C',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->MultiCell(25,5,"INVERSION","LBT",'C',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->MultiCell(22,5,$tipoPresupuesto == 2 ? "X" : "","RBT",'C',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->MultiCell(25,5,"FUN/INV","LBT",'C',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->MultiCell(22,5,$tipoPresupuesto == 3 ? "X" : "","RBT",'C',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->ln();
                    $pdf->MultiCell(50,5,"TIPO DE DOCUMENTO:","LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->MultiCell(50,5,"POR TIPO DE CONTRATO","LBT",'C',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->MultiCell(20,5,$tipoDocumento == 1 ? "X" : "","RBT",'C',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->MultiCell(50,5,"POR ACTO ADMINISTRATIVO","LBT",'C',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->MultiCell(20,5,$tipoDocumento == 2 ? "X" : "","RBT",'C',true,0,'','',true,0,false,true,0,'M',true);
                    if ($tipoDocumento == 1) {
                        $codTipoContrato = $_POST["tipoContrato"];
                        $numContrato = $_POST["numeroContrato"];

                        if ($codTipoContrato == 1) {
                            $tipoContrato = "Obra";
                        } else if ($codTipoContrato == 2) {
                            $tipoContrato = "Consultora servicio";
                        } else if ($codTipoContrato == 3) {
                            $tipoContrato = "Suministro y/o compraventa";
                        } else if ($codTipoContrato == 4) {
                            $tipoContrato = "Prestacion de servicios";
                        } else {
                            $tipoContrato = "Otro";
                        }

                        $pdf->ln();
                        $pdf->MultiCell(50,5,"TIPO DE CONTRATO:","LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
                        $pdf->MultiCell(70,5,"$tipoContrato","LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
                        $pdf->MultiCell(35,5,"NÚMERO CONTRATO:","LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
                        $pdf->MultiCell(35,5,"$numContrato","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
                    } else {
                        $numeroActo = $_POST["numero_acto"];
                        $fechaActo = $fecha = DateTime::createFromFormat('Y-m-d', $_POST["fecha_acto"])->format('d/m/Y');;
                        $pdf->ln();
                        $pdf->MultiCell(50,5,"NÚMERO DE ACTO:","LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
                        $pdf->MultiCell(45,5,"$numeroActo","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
                        $pdf->MultiCell(50,5,"FECHA DE ACTO:","LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
                        $pdf->MultiCell(45,5,"$fechaActo","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
                    }
                    $pdf->ln();
                    $pdf->MultiCell(40,5,"DURACIÓN (MESES):","LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->MultiCell(24,5,"$duracion_meses","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->MultiCell(40,5,"DURACIÓN (DÍAS):","LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->MultiCell(23,5,"$duracion_dias","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->MultiCell(40,5,"GARANTIAS:","LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->MultiCell(23,5,"$garantias","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->ln();
                    $pdf->SetFont('helvetica','B',8);
                    $pdf->SetFillColor(153,221,255);
               
                    $pdf->MultiCell(190,5,"DATOS BENEFICIARIO","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->SetFont('helvetica','',8);
                    $pdf->SetFillColor(245,245,245);
                    $pdf->ln();
                    $pdf->MultiCell(30,5,"DOCUMENTO:","LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->MultiCell(30,5,"$doc_tercero","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->MultiCell(30,5,"BENEFICIARIO:","LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->MultiCell(100,5,"$name_tercero","LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->ln();
                    $pdf->MultiCell(30,5,"CUENTA BANCARIA:","LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->MultiCell(160,5,"$cuenta_banco","LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->ln();
                    /* $objeto = $_POST["objeto"]; */
                    $ancho_celda = 160; // Ancho definido para el texto
                    $tamanio_fuente = 10; // Tamaño de fuente

                    // Calcular la altura necesaria para el texto
                    $altura_texto = $pdf->getStringHeight($ancho_celda, $objeto, false, true, $tamanio_fuente);

                    // Definir una altura mínima para evitar que se vea muy pequeño
                    $altura_minima = 10; 
                    $altura = max($altura_texto, $altura_minima);

                    $pdf->MultiCell(30, $altura, "OBJETO:", "LRBT", 'LRBT', true, 0, '', '', true, 0, false, true, 0, 'M', true);
                    $pdf->MultiCell($ancho_celda, $altura, $objeto, "LRBT", 'L', true, 0, '', '', true, 0, false, true, 0, 'M', true);

                    /* $pdf->MultiCell(30,12,"OBJETO:","LRBT",'LRBT',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->MultiCell(160,12,"$objeto","LRBT",'L',true,0,'','',true,0,false,true,0,'M',true); */
                    $pdf->ln();
                    $pdf->MultiCell(30,14,"FORMA DE PAGO:","LRBT",'LRBT',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->MultiCell(160,14,"$forma_pago","LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->ln();
                    $pdf->SetFont('helvetica','B',8);
                    $pdf->SetFillColor(153,221,255);
                    $pdf->MultiCell(190,5,"ALCANCE OBJETO CONTRACTUAL","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->ln();
                
                    $pdf->MultiCell(78,8,"Descripción","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->MultiCell(18,8,"Unidad de medida","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->MultiCell(38,8,"Valor unitario","LRBT",'R',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->MultiCell(18,8,"Cantidad","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->MultiCell(38,8,"Subtotal","LRBT",'R',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->ln();
                    $pdf->SetFont('helvetica','',8);
                    $pdf->SetFillColor(255,255,255);
                    foreach ($items as $item) {
                        $height = $pdf->getNumLines($item['descripcion'])*5;
                        $pdf->MultiCell(78,$height,$item['descripcion'],"LRBT",'',true,0,'','',true,0,false,true,0,'M',true);
                        $pdf->MultiCell(18,$height,$item['unidad'],"LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
                        $pdf->MultiCell(38,$height,formatNum($item['valor']),"LRBT",'R',true,0,'','',true,0,false,true,0,'M',true);
                        $pdf->MultiCell(18,$height,$item['cantidad'],"LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
                        $pdf->MultiCell(38,$height,formatNum($item['subtotal']),"LRBT",'R',true,0,'','',true,0,false,true,0,'M',true);
                        $pdf->ln();

                        $getY = $pdf->getY();
                        if ($getY > 200) {
                            $pdf->AddPage();
                        }
                    }

                    $pdf->SetFont('helvetica','B',9);
                    $pdf->SetFillColor(153,221,255);

                    if (!empty($cuentasFun)) {
                        $sql_version_cuentas = "SELECT MAX(version) AS version FROM cuentasccpet";
                        $row_version_cuentas = $con->select($sql_version_cuentas);
                        $versionCuentas = $row_version_cuentas["version"];
                
                        $pdf->SetFont('helvetica','B',8);
                        $pdf->SetFillColor(153,221,255);
                        $pdf->MultiCell(190,5,"DISCRIMINACIÓN PRESUPUESTAL","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
                        $pdf->ln();
                        
                        $pdf->MultiCell(28,8,"Tipo presupuesto","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
                        $pdf->MultiCell(28,8,"Tipo gasto","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
                        $pdf->MultiCell(28,8,"División gasto","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
                        $pdf->MultiCell(28,8,"Subdivision gasto","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
                        $pdf->MultiCell(32,8,"CCPET","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
                        $pdf->MultiCell(18,8,"CPC","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
                        $pdf->MultiCell(28,8,"Fuente","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
                        $pdf->ln();
                        $pdf->SetFillColor(245,245,245);
                        $pdf->SetFont('helvetica','',8);
                        $fill = true;
                
                        function search_name_cuenta($version, $cuenta){
                            $con = new Mysql();
                            $sql = "SELECT nombre FROM cuentasccpet WHERE codigo = '$cuenta' AND version = $version";
                            $row = $con->select($sql);
                            return $row["nombre"];
                        }
                
                        foreach ($cuentasFun as $fun) {
                            $codTipoPresu = substr($fun["cuenta"], 0, 3);
                            $nomTipoPresu = search_name_cuenta($versionCuentas, $codTipoPresu);
                            $codTipoGasto = substr($fun["cuenta"], 0, 5);
                            $nomTipoGasto = search_name_cuenta($versionCuentas, $codTipoGasto);
                            $codDivisionGasto = substr($fun["cuenta"], 0, 8);
                            $nameDivisionGasto = search_name_cuenta($versionCuentas, $codDivisionGasto);
                            $codSubdivisionGasto = substr($fun["cuenta"], 0, 11);
                            $nameSubdivisionGasto = search_name_cuenta($versionCuentas, $codSubdivisionGasto);
                            $nameCuenta = search_name_cuenta($versionCuentas, $fun["cuenta"]);
                            $sql_fuentes = "SELECT nombre FROM ccpet_fuentes_cuipo WHERE codigo_fuente = '$fun[fuente]'";
                            $row_fuentes = $con->select($sql_fuentes);
                            $nomFuente = $row_fuentes["nombre"];
                
                            $height = $pdf->getNumLines($fun['nameCuenta'])*5;

                            $pdf->SetFont('helvetica','',8);
                            $pdf->SetFillColor(255,255,255);
                            $pdf->MultiCell(28,15,"$codTipoPresu - $nomTipoPresu","LRBT",'J',true,0,'','',true,0,false,true,0,'M',true);
                            $pdf->MultiCell(28,15,"$codTipoGasto - $nomTipoGasto","LRBT",'J',true,0,'','',true,0,false,true,0,'M',true);
                            $pdf->MultiCell(28,15,"$codDivisionGasto - $nameDivisionGasto","LRBT",'J',true,0,'','',true,0,false,true,0,'M',true);
                            $pdf->MultiCell(28,15,"$codSubdivisionGasto - $nameSubdivisionGasto","LRBT",'J',true,0,'','',true,0,false,true,0,'M',true);
                            $pdf->MultiCell(32,15,"$fun[cuenta] - $nameCuenta","LRBT",'J',true,0,'','',true,0,false,true,0,'M',true);
                            $pdf->MultiCell(18,15,"$fun[cpc]","LRBT",'J',true,0,'','',true,0,false,true,0,'M',true);
                            $pdf->MultiCell(28,15,"$fun[fuente] - $nomFuente","LRBT",'J',true,0,'','',true,0,false,true,0,'M',true);
                            $pdf->ln();
                            $fill = !$fill;
                
                            $getY = $pdf->getY();
                            if ($getY > 200) {
                                $pdf->AddPage();
                            }
                        }
                    }
                    
                    if (!empty($cuentasInv)) {
                        $pdf->SetFont('helvetica','B',8);
                        $pdf->SetFillColor(153,221,255);
                        $pdf->MultiCell(190,5,"DISCRIMINACIÓN PRESUPUESTAL","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
                        $pdf->ln();
                        
                        $pdf->MultiCell(22,8,"Sector","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
                        $pdf->MultiCell(24,8,"Programa","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
                        $pdf->MultiCell(24,8,"SubPrograma","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
                        $pdf->MultiCell(24,8,"Producto","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
                        $pdf->MultiCell(24,8,"Indicador","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
                        $pdf->MultiCell(28,8,"BPIN","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
                        $pdf->MultiCell(24,8,"Fuente","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
                        $pdf->MultiCell(20,8,"CCPET","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
                        $pdf->ln();
                        $pdf->SetFillColor(245,245,245);
                        $pdf->SetFont('helvetica','',8);
                        $fill = true;
                
                        foreach ($cuentasInv as $inv) {
                            $codSector = $nombreSector = "";
                            $codIndicador = $inv["codIndicador"];
                            $bpin = $inv["bpin"];
                            $codSector = substr($codIndicador, 0, 2);
                            $sql_sector = "SELECT nombre FROM ccpetsectores WHERE codigo = '$codSector'";
                            $row_sector = $con->select($sql_sector);
                            $nomSector = $row_sector["nombre"];
                            $codPrograma = substr($codIndicador, 0, 4);
                            $sql_programa = "SELECT nombre, codigo_subprograma, nombre_subprograma FROM ccpetprogramas WHERE codigo LIKE '$codPrograma'";
                            $row_programa = $con->select($sql_programa);
                            $nomPrograma = $row_programa["nombre"];
                            $codSubPrograma = $codPrograma.$row_programa["codigo_subprograma"];
                            $nomSubPrograma = $row_programa["nombre_subprograma"];
                            $codProducto = substr($codIndicador, 0, 7);
                            $sql_producto = "SELECT DISTINCT producto AS nombre FROM ccpetproductos WHERE cod_producto LIKE '$codProducto'";
                            $row_producto = $con->select($sql_producto);
                            $nomProducto = $row_producto["nombre"];
                            $sql_indicador = "SELECT indicador_producto AS nombre FROM ccpetproductos WHERE codigo_indicador LIKE '$codIndicador'";
                            $row_indicador = $con->select($sql_indicador);
                            $nomIndicador = $row_indicador["nombre"];
                            $sql_bpin = "SELECT nombre FROM ccpproyectospresupuesto WHERE codigo = '$bpin' AND vigencia = '$vigencia'";
                            $row_bpin = $con->select($sql_bpin);
                            $nomBpin = $row_bpin["nombre"];
                            
                            $sql_fuentes = "SELECT nombre FROM ccpet_fuentes_cuipo WHERE codigo_fuente = '$inv[codFuente]'";
                            $row_fuentes = $con->select($sql_fuentes);
                            $nomFuente = $row_fuentes["nombre"];
                            $pdf->SetFont('helvetica','',8);
                            $pdf->SetFillColor(255,255,255);
                            $pdf->MultiCell(22,15,"$codSector - $nomSector","LRBT",'J',true,0,'','',true,0,false,true,0,'M',true);
                            $pdf->MultiCell(24,15,"$codPrograma - $nomPrograma","LRBT",'J',true,0,'','',true,0,false,true,0,'M',true);
                            $pdf->MultiCell(24,15,"$codSubPrograma - $nomSubPrograma","LRBT",'J',true,0,'','',true,0,false,true,0,'M',true);
                            $pdf->MultiCell(24,15,"$codProducto - $nomProducto","LRBT",'J',true,0,'','',true,0,false,true,0,'M',true);
                            $pdf->MultiCell(24,15,"$inv[codIndicador] - $nomIndicador","LRBT",'J',true,0,'','',true,0,false,true,0,'M',true);
                            $pdf->MultiCell(28,15,"$inv[bpin] - $nomBpin","LRBT",'J',true,0,'','',true,0,false,true,0,'M',true);
                            $pdf->MultiCell(24,15,"$inv[codFuente] - $nomFuente","LRBT",'J',true,0,'','',true,0,false,true,0,'M',true);
                            $pdf->SetFont('helvetica','',6);
                            $pdf->MultiCell(20,15,$inv["cuenta"],"LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);           
                            
                            $pdf->ln();
                            $fill = !$fill;
                
                            $getY = $pdf->getY();
                            if ($getY > 200) {
                                $pdf->AddPage();
                            }
                        }
                    }
                    $pdf->ln();
                    $pdf->SetFont('helvetica','B',8);
                    $pdf->SetFillColor(153,221,255);
                    $pdf->MultiCell(190,6,"CUMPLIMIENTO DE LOS REQUISITOS ESTABLEDICOS","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->ln();
                    $pdf->SetFillColor(245,245,245);
                    $pdf->MultiCell(190,6,"SE CERTIFICA QUE CUMPLE CON LOS REQUISITOS PRECONTRACTUALES Y CONTRACTUALES EXIGIDOS PARA ESTE REGISTRO","LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);      

                    $strFile = 'registro_contrato_'.$consecutivo;
                    $pdf->Output($strFile.'.pdf', 'I');
                // }
            }
            die();
        }

    }
    class MYPDF extends TCPDF {
        protected $last_page_flag = false;

        public function Close() {
            $this->last_page_flag = true;
            parent::Close();
        }

		public function Header(){
			$request = configBasica();
            $strNit = $request['nit'];
            $strRazon = $request['razonsocial'];

			//Parte Izquierda
			$this->Image('../../../imagenes/escudo.jpg',  13, 13, 20, 20, 'JPG', '', 'T', true, 300, '', false, false, 0, false, false, false);// Logo
			$this->SetFont('helvetica','B',8);
			$this->SetY(10);
			$this->RoundedRect(10, 10, 190, 25, 1,'');
			$this->Cell(0.1);
			$this->Cell(26,25,'','R',0,'L');
			$this->SetY(8);
			$this->SetX(40);
			$this->SetFont('helvetica','B',9);
			$this->Cell(140,15,strtoupper("$strRazon"),0,0,'C');
			$this->SetFont('helvetica','B',7);
			$this->SetY(12);
			$this->SetX(40);
			$this->Cell(140,15,'NIT: '.$strNit,0,0,'C');
			//*****************************************************************************************************************************
			$this->SetFont('helvetica','B',9);
			$this->SetY(23);
			$this->SetX(36);
            $this->Cell(164,12,"FORMATO REGISTRO DE CONTRATO",'T',0,'C');

			//**********************************************************
			$this->SetFont('times','B',10);
			$this->ln(12);
           
			//**********************************************************
            if(ESTADO=="N"){
                $img_file = "../../../assets/img/anulado.png";
                $this->SetAlpha(0.35);
                $this->Image($img_file, 0, 20, 250, 280, '', '', '', false, 300, '', false, false, 300);
                $this->SetAlpha(1);
            }
		}
		public function Footer(){
            $con = new Mysql();        
			$request = configBasica();
            $_POST["consecutivo"];
            $strDireccion = $request['direccion'] != "" ? "Dirección: ".strtoupper($request['direccion']) : "";
            $strWeb = $request['web'] != "" ? "Pagina web: ".strtoupper($request['web']) :"";
            $strEmail = $request['email'] !="" ? "Email: ".strtoupper($request['email']) :"";
            $strTelefono = $request['telefono'] != "" ? "Telefonos: ".$request['telefono'] : "";
            $sql_funcion = "SELECT id FROM ccpet_funciones WHERE nombre = 'Registro Contrato'";
            $row_funcion = $con->select($sql_funcion);
            $sql_auditoria = "SELECT usuario FROM ccpet_auditoria WHERE ccpet_funciones_id = $row_funcion[id] AND consecutivo = '$_POST[consecutivo]'";
            $row_auditoria = $con->select($sql_auditoria);
            if ($row_auditoria["usuario"] != "") {
                $strUsuario = $row_auditoria["usuario"];
            } else {
                $strUsuario = searchUser($_SESSION['cedulausu'])['nom_usu'];
            }
			$strNick = $_SESSION['nickusu'];
			$strFecha = date("d/m/Y H:i:s");
			$strIp = $_SERVER['REMOTE_ADDR'];
            
            if ($this->last_page_flag) {
                $this->setY(240);
                $this->setX(30);
                $this->SetFont('helvetica','B',6);
                $this->SetFillColor(153,221,255);
                $this->setX(80);
                $this->cell(70,4,'DATOS QUIEN CERTIFICA','LRTB',0,'C',1);
                $this->ln();
                $this->setX(30);
                $this->SetFont('helvetica','B',6);
                $this->SetFillColor(255,255,255);
                $this->setX(80);
                $this->cell(70,4,' NOMBRE:','LRTB',0,'L',1);
                $this->ln();
                $this->setX(30);
                $this->SetFont('helvetica','B',6);
                $this->SetFillColor(255,255,255);
                $this->setX(80);
                $this->cell(70,4,' CARGO: ','LRTB',0,'L',1);
                $this->ln();
                $this->setX(80);
                $this->cell(70,4,' FECHA RECEPCIÓN: ','LRTB',0,'L',1);
                $this->ln();
                $this->setX(30);
                $this->SetFont('helvetica','',6);
                $this->SetFillColor(255,255,255);
                $this->setX(80);
                $this->cell(70,15,'Firma','LRTB',0,'C',1);
            }

            $this->setY(280);
			$this->SetFont('helvetica', 'I', 8);
			$txt = <<<EOD
			$strDireccion $strTelefono
			$strEmail $strWeb
			EOD;
			$this->SetFont('helvetica', 'I', 6);
			$this->Cell(190,10,'','T',0,'T');
			$this->ln(2);
			$this->Write(0, $txt , '', 0, 'C', true, 0, false, false, 0);

			$this->Cell(50, 10, 'Hecho por: '.$strUsuario, 00, false, 'C', 0, '', 0, false, 'T', 'M');
			$this->Cell(30, 10, 'Impreso por: '.$strNick, 0, false, 'C', 0, '', 0, false, 'T', 'M');
			$this->Cell(30, 10, 'IP: '.$strIp, 0, false, 'C',0, '', 0, false, 'T', 'M');
			$this->Cell(30, 10, 'Fecha: '.$strFecha, 0, false, 'C', 0, '', 0, false, 'T', 'M');
			$this->Cell(30, 10, 'IDEAL.10 S.A.S', 0, false, 'C', 0, '', 0, false, 'T', 'M');
            $this->Cell(20, 10, 'Pagina '.$this->getAliasNumPage().'/'.$this->getAliasNbPages(), 0, false, 'C', 0, '', 0, false, 'T', 'M');
		}
	}

    if($_POST){
        // var_dump($_POST);
        $obj = new RegistroExportController();
        if($_POST['action'] == "pdf"){
            $obj->exportPdf();
        }
    }

?>
