<?php
    require_once '../../../Librerias/core/Helpers.php';
    require_once '../models/registro_models.php';
    session_start();
    header('Content-Type: application/json');
    class registroController extends cdpModel {
        /* crear */
        public function get() {
            
            if (!empty($_SESSION)) {
                $arrResponse = array(
                    "cdps" => $this->get_cdps(),
                    "areas" => $this->get_areas(),
                    "terceros" => getTerceros()
                );
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);    
            }
            die();
        }

        public function dataSolicitud() {
            if (!empty($_SESSION)) {
                $cdp = $_POST["codCdp"];
                $arrResponse = array(
                    "dataSolicitud" => $this->get_data_solicitud($cdp)
                );
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);    
            }
            die();
        }

        public function bancosTercero() {
            if (!empty($_SESSION)) {
                $documento = $_POST["documento"];
                $arrResponse = array(
                    "bancos" => $this->get_bancos($documento)
                );
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);    
            }
            die();
        }

        public function save() {
            if (!empty($_SESSION)) {
                $cab = [
                    "fecha" => $_POST["fecha"],
                    "cdp" => $_POST["codCdp"],
                    "contrato" => $_POST["numContrato"],
                    "tercero" => $_POST["tercero"],
                    "objeto" => $_POST["objeto"],
                    "plazo" => $_POST["plazo"],
                    "plazo_dias" => $_POST["plazo_dias"],
                    "garantias" => $_POST["garantias"],
                    "valor" => $_POST["valor"],
                    "formaPago" => $_POST["formaPago"],
                    "cuentaBanco" => $_POST["cuentaBanco"]
                ];

                $items = json_decode($_POST["items"], true);

                $id = $this->insert_cab($cab, $items);

                if ($id > 0) {
                    $arrResponse = array(
                        "status" => true,
                        "msg" => "Guardado exitoso!",
                        "id" => $id
                    );
                } else {
                    $arrResponse = array(
                        "status" => false,
                        "msg" => "Error en guardado!"
                    );
                }
                
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);    
            }
            die();
        }

        /* buscar */
        public function search() {
            if (!empty($_SESSION)) {
                $arrResponse = array(
                    "arrSearch" => $this->get_data_search()
                );
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);    
            }
            die();
        }

        public function anula() {
            if (!empty($_SESSION)) {
                $id = $_POST["id"];
                $result = $this->anula_registro($id);
                if ($result) {
                    $arrResponse = array(
                        "status" => true,
                        "msg" => "Anulado exitoso!"
                    );
                } else {
                    $arrResponse = array(
                        "status" => false,
                        "msg" => "Error en anulado!"
                    );
                }

                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);    
            }
            die();
        }

        /* editar */
        public function visualizar() {
            if (!empty($_SESSION)) {
                $id = $_POST["id"];
                $dataCab = $this->get_data_edit($id);
                $dataDet = $this->get_data_edit_det($dataCab["consecutivo"]);

                $arrResponse = array(
                    "arrCab" => $dataCab,
                    "arrDet" => $dataDet,
                    "arrIds" => $this->get_consecutivos()
                );
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);    
            }
            die();
        }
    }

    if($_POST){
        $obj = new registroController();
        $accion = $_POST["action"];
        if ($accion == "get") {
            $obj->get();
        } else if ($accion=="dataSolicitud") {
            $obj->dataSolicitud();
        } else if ($accion=="save") {
            $obj->save();
        } else if ($accion == "bancos") {
            $obj->bancosTercero();
        } else if ($accion == "getSearch") {
            $obj->search();
        } else if ($accion == "anula") {
            $obj->anula();
        } else if ($accion == "visualizar") {
            $obj->visualizar();
        }
    }
?>