<?php

	require_once '../../comun.inc';
    require '../../funciones.inc';
    session_start();

	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");

    $out = array('error' => false);

    $maxVersion = ultimaVersionGastosCCPET();

    $action = "show";

    if(isset($_GET['action'])){
        $action = $_GET['action'];
    }
    
    if($action == 'buscarSaldos'){

        $contratos = array();

        $critFecha = '';
        $critNumContrato = '';

        if($_POST['fechaInicial'] != '' && $_POST['fechaFinal'] != ''){
            $critFecha = " AND fecha BETWEEN '".$_POST['fechaInicial']."' AND '".$_POST['fechaFinal']."'";
        }

        $sqlr = "SELECT TB1.consvigencia, TB1.detalle, TB1.idcdp, TB1.fecha, TB1.tercero, TB1.contrato, TB2.cuenta, TB2.indicador_producto, TB2.bpim, TB2.fuente, TB2.seccion_presupuestal, TB2.codigo_vigenciag, TB2.medio_pago, SUM(TB2.valor), TB1.vigencia FROM ccpetrp AS TB1, ccpetrp_detalle AS TB2 WHERE TB1.vigencia = TB2.vigencia AND TB1.consvigencia = TB2.consvigencia $critFecha AND TB1.tipo_mov = TB2.tipo_mov AND TB1.tipo_mov = '201' GROUP BY TB1.consvigencia, TB2.cuenta, TB2.indicador_producto, TB2.bpim, TB2.fuente, TB2.seccion_presupuestal, TB2.codigo_vigenciag, TB2.medio_pago";
        $res = mysqli_query($linkbd, $sqlr);
        while($row = mysqli_fetch_assoc($res)){
            $valorRp = 0;
            $saldoPorEjecu = 0;
            $rubro = '';
            $nombreRubro = '';
            $tercero = '';

            $sqlrRev = "SELECT SUM(valor) FROM ccpetrp_detalle WHERE cuenta = '$row[cuenta]' AND indicador_producto = '$row[indicador_producto]' AND bpim = '$row[bpim]' AND fuente = '$row[fuente]' AND seccion_presupuestal = '$row[seccion_presupuestal]' AND codigo_vigenciag = '$row[codigo_vigenciag]' AND medio_pago = '$row[medio_pago]' AND consvigencia = '$row[consvigencia]' AND vigencia = '$row[vigencia]' AND SUBSTRING(tipo_mov, 1, 1) LIKE '4%'";
            $resRev = mysqli_query($linkbd, $sqlrRev);
            $rowRev = mysqli_fetch_assoc($resRev);

            $valorRp = $row['SUM(TB2.valor)'] - $rowRev['SUM(valor)'];


            if(substr($row['cuenta'],0,3) != '2.3'){
                $rubro = $row['cuenta'];
                $nombreRubro = buscacuentaccpetgastos($row['cuenta'], $maxVersion);
            }else{
                $rubro = $row['indicador_producto'].' - '.$row['bpim'].' - '.$row['fuente'].' - '.$row['seccion_presupuestal'].' - '.$row['codigo_vigenciag'].' - '.$row['medio_pago'];
                $nombreRubro = buscaindicadorccpet($row['indicador_producto']).' - '.buescarNombreProyecto($row['bpim'], $row['vigencia']).' - '.buscafuenteccpet($row['fuente']).' - '.buescarNombreVigenciaGasto($row['codigo_vigenciag']);
            }

            $tercero = $row['tercero'].' - '.buscatercero($row['tercero']);

            $det = array();
            array_push($det, $rubro);
            array_push($det, $nombreRubro);
            array_push($det, $row['fuente']);
            array_push($det, buscafuenteccpet($row['fuente']));
            array_push($det, $row['consvigencia']);
            array_push($det, $row['idcdp']);
            array_push($det, $row['fecha']);
            array_push($det, $tercero);            
            array_push($det, $row['contrato']);
            array_push($det, $row['detalle']);            
            array_push($det, $valorRp);

            array_push($contratos, $det);
            
            /* array_push($contratos, $row); */
        }
        $out['contratos'] = $contratos;
    }

   
    header("Content-type: application/json");
    echo json_encode($out);
    die();