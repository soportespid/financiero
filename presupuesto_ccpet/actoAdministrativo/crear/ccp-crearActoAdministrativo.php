<?php
    require_once '../../../comun.inc';
    require '../../../funciones.inc';
    session_start();

    $linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");

    $out = array('error' => false);

    $action = "show";

    if(isset($_GET['action'])){
        $action = $_GET['action'];
    }
    
    if ($action == 'validaConsecutivo') {

       $numero = "";
       $tipoActo = "";
       $fecha = "";

       $numero = $_GET['numero'];
       $tipoActo = $_GET['acto'];
       //preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_GET["fecha"], $f);
       $vigencia = explode("-", $_GET["fecha"])[0];

       $sqlValidacion = "SELECT * FROM ccpetacuerdos WHERE consecutivo = '$numero' AND tipo_acto_adm = '$tipoActo' AND vigencia = '$vigencia'";
       $resValidacion = mysqli_query($linkbd, $sqlValidacion);
       $valida = mysqli_num_rows($resValidacion);

        if ($valida == 0) {
            $out['validaCod'] = false;
        }
        else {
            $out['validaCod'] = true;
        }
    }

    if ($action == "guardarInicial") {
        
        //preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST["fecha"], $f);
		$fecha = $_POST["fecha"];
        $vigencia = explode("-", $_POST["fecha"])[0];
        $tipoActo = $_POST['tipoActo'];
        $numero = $_POST['numero'];
        $descripcionActo = $_POST['descripcionActo'];
        $tipoAcuerdo = $_POST['tipoAcuerdo'];
        $valorInicial = $_POST['valorInicial'];
        $valorAdicion = 0;
        $valorReduccion = 0;
        $valorTraslado = 0;

        $sqlAcuerdo = "INSERT INTO ccpetacuerdos (consecutivo, numero_acuerdo, fecha, vigencia, valoradicion, valorreduccion, valortraslado, valorinicial, estado, tipo, tipo_acto_adm) VALUES ($numero, '$descripcionActo', '$fecha', '$vigencia', $valorAdicion, $valorReduccion, $valorTraslado, $valorInicial, 'S', '$tipoAcuerdo', $tipoActo)";
        mysqli_query($linkbd, $sqlAcuerdo);

        $out['insertaBien'] = true;
    }

    if ($action == "guardarModificacion") {
        //preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST["fecha"], $f);
		$fecha = $_POST["fecha"];
        $vigencia = explode("-", $_POST["fecha"])[0];
        $tipoActo = $_POST['tipoActo'];
        $numero = $_POST['numero'];
        $descripcionActo = $_POST['descripcionActo'];
        $tipoAcuerdo = $_POST['tipoAcuerdo'];
        $valorInicial = 0;
        $valorAdicion = $_POST['valorAdicion'];
        $valorReduccion = $_POST['valorReduccion'];
        $valorTraslado = $_POST['valorTraslados'];

        $sqlAcuerdo = "INSERT INTO ccpetacuerdos (consecutivo, numero_acuerdo, fecha, vigencia, valoradicion, valorreduccion, valortraslado, valorinicial, estado, tipo, tipo_acto_adm) VALUES ($numero, '$descripcionActo', '$fecha', '$vigencia', $valorAdicion, $valorReduccion, $valorTraslado, $valorInicial, 'S', '$tipoAcuerdo', $tipoActo)";
        mysqli_query($linkbd, $sqlAcuerdo);

        $out['insertaBien'] = true;
    }

    header("Content-type: application/json");
    echo json_encode($out);
    die();