var app = new Vue({
    el: '#myapp',
	data:{

        error: '',
        loading: false,
        
        tipoActo: '-1',
        consecutivo: '',
        descripcionActo: '',
        tipoAcuerdo: 'M',
        valorInicial: 0,
        valorInicialFormat: 0,
        valorAdicion: 0,
        valorAdicionFormat: 0,
        valorReduccion: 0,
        valorReduccionFormat: 0,
        valorTraslados: 0,
        valorTrasladosFormat: 0,
        fecha: new Date().toISOString().split("T")[0],
    },

    mounted: function(){
        //this.cargarFecha();
    },

    methods: 
    {
        toFormData: function(obj){
            var form_data = new FormData();
            for(var key in obj){
                form_data.append(key, obj[key]);
            }
            return form_data;
        },

        /* cargarFecha: function(){
            
            const fechaAct = new Date().toJSON().slice(0,10).replace(/-/g,'/');
            const fechaArr = fechaAct.split('/');
            const fechaV = fechaArr[2]+'/'+fechaArr[1]+'/'+fechaArr[0];
            document.getElementById('fecha').value = fechaV;
        }, */

        limpiaNumero: function() {

            this.consecutivo = "";
        },

        validaNumero: async function() {

            if (this.tipoActo != "") {
                
                const numero = parseInt(this.consecutivo);
                const tipoActo = this.tipoActo;
                //const fecha = document.getElementById('fecha').value;

                await axios.post('presupuesto_ccpet/actoAdministrativo/crear/ccp-crearActoAdministrativo.php?action=validaConsecutivo&numero='+numero+'&acto='+tipoActo+'&fecha='+this.fecha)
                .then((response) => {
                    
                    if (response.data.validaCod == true) {
                        Swal.fire({
                            icon: 'error',
                            title: 'Datos duplicados',
                            text: 'Ya existe un número con el mismo tipo de acto.',
                        }).then((result) => { 
                            this.consecutivo = "";
                        });
                    }
                }).catch((error) => {
                    this.error = true;
                    console.log(error)
                });
            }
            else {
                Swal.fire('Selecciona primero el tipo de acto', '', 'info');
                this.consecutivo = "";
            }
        },

        guardar: function() {

            //var fecha = document.getElementById('fecha').value;

            if (this.fecha != "" && this.tipoActo && this.numero != "" && this.descripcionActo != "" && this.tipoAcuerdo != "") {
                if (this.tipoAcuerdo == "I") {

                    if (this.valorInicial >= 0) {

                        Swal.fire({
                            icon: 'question',
                            title: 'Seguro que quieres guardar?',
                            showDenyButton: true,
                            confirmButtonText: 'Guardar',
                            denyButtonText: 'Cancelar',
                            }).then((result) => {
                            if (result.isConfirmed) 
                            {
                                if (result.isConfirmed) {
                                    this.loading = true;
                                    var formData = new FormData();
        
                                    formData.append("fecha", this.fecha);
                                    formData.append("tipoActo", this.tipoActo);
                                    formData.append("numero", this.consecutivo);
                                    formData.append("descripcionActo", this.descripcionActo);
                                    formData.append("tipoAcuerdo", this.tipoAcuerdo);
                                    formData.append("valorInicial", this.valorInicial);
                                    
                                    axios.post('presupuesto_ccpet/actoAdministrativo/crear/ccp-crearActoAdministrativo.php?action=guardarInicial', formData)
                                    .then((response) => {
                                        
                                        console.log(response.data);
                                        if(response.data.insertaBien){
                                            this.loading = false;
                                            Swal.fire({
                                                icon: 'success',
                                                title: 'Se ha guardado el acto con exito!',
                                                showConfirmButton: false,
                                                timer: 1500
                                                }).then((response) => {
                                                    this.redireccionar();
                                                });
                                        }
                                        else {
                                            Swal.fire(
                                                'Error!',
                                                'No se pudo guardar.',
                                                'error'
                                            );
                                        }
                                    });
                                    
                                }
                            } 
                            else if (result.isDenied) 
                            {
                                Swal.fire('Guardar cancelado', '', 'info');
                            }
                        })
                    }   
                    else {
                        Swal.fire('Valor inicial esta en cero', '', 'info');
                    }
                }
                else if (this.tipoAcuerdo == "M") {

                    if (this.valorAdicion >= 0 || this.valorReduccion >= 0 || this.valorTraslados >= 0) {

                        Swal.fire({
                            icon: 'question',
                            title: 'Seguro que quieres guardar?',
                            showDenyButton: true,
                            confirmButtonText: 'Guardar',
                            denyButtonText: 'Cancelar',
                            }).then((result) => {
                            if (result.isConfirmed) 
                            {
                                if (result.isConfirmed) {
                                    this.loading = true;
                                    var formData = new FormData();
        
                                    formData.append("fecha", this.fecha);
                                    formData.append("tipoActo", this.tipoActo);
                                    formData.append("numero", this.consecutivo);
                                    formData.append("descripcionActo", this.descripcionActo);
                                    formData.append("tipoAcuerdo", this.tipoAcuerdo);
                                    formData.append("valorAdicion", this.valorAdicion);
                                    formData.append("valorReduccion", this.valorReduccion);
                                    formData.append("valorTraslados", this.valorTraslados);
                                    
                                    axios.post('presupuesto_ccpet/actoAdministrativo/crear/ccp-crearActoAdministrativo.php?action=guardarModificacion', formData)
                                    .then((response) => {
                                        
                                        if(response.data.insertaBien){
                                            this.loading = false;
                                            Swal.fire({
                                                icon: 'success',
                                                title: 'Se ha guardado el acto con exito!',
                                                showConfirmButton: false,
                                                timer: 1500
                                                }).then((response) => {
                                                    this.redireccionar();
                                                });
                                        }
                                        else {
                                            Swal.fire(
                                                'Error!',
                                                'No se pudo guardar.',
                                                'error'
                                            );
                                        }
                                    });
                                    
                                }
                            } 
                            else if (result.isDenied) 
                            {
                                Swal.fire('Guardar cancelado', '', 'info');
                            }
                        })
                    }
                    else {
                        Swal.fire('Valores en cero', '', 'info');
                    }
                }
            }
            else {
                Swal.fire('Llena los datos completos', '', 'info');
            }
        },

        redireccionar: function()
        {
            location.reload();  
        },

        formatInputNumber(conFormato, sinFormato) {
            this[conFormato] = this.formatNumber(this[conFormato]);
            this[sinFormato] = this[conFormato].replace(/[^0-9.]/g, "");
        },
        formatNumber(number) {
            if (!number) return ""; // Si no hay entrada, retorna vacío
    
            // Elimina caracteres no válidos (excepto números y el punto decimal)
            number = number.replace(/[^0-9.]/g, "");
    
            // Divide la parte entera y decimal
            const [integerPart, decimalPart] = number.split(".");
    
            // Formatear la parte entera con separador de miles
            const formattedInteger = integerPart.replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    
            // Si hay parte decimal, la une; si no, devuelve solo la parte entera
            return decimalPart !== undefined
                ? `${formattedInteger}.${decimalPart.slice(0, 2)}` // Limita a 2 decimales si es necesario
                : formattedInteger;
        },
    }
});