<?php
    require_once '../../../comun.inc';
    require '../../../funciones.inc';
    session_start();

    $linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");

    $out = array('error' => false);

    $action = "show";

    if(isset($_GET['action'])){
        $action = $_GET['action'];
    }
    
    if ($action == "buscaActos") {

        if ($_POST['fechaInicial'] != "") {
            preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST["fechaInicial"], $f);
            $fechaInicial = $f[3]."-".$f[2]."-".$f[1];
        }
        else {
            $fechaInicial = "";
        }
        
        if ($_POST['fechaFinal'] != "") {

            preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST["fechaFinal"], $f);
		    $fechaFinal = $f[3]."-".$f[2]."-".$f[1];
        }
        else {
            $fechaFinal = "";
        }

        $numero = $_POST['numero'];

        if ($numero != "" && $fechaInicial != "" && $fechaFinal != "") {

            $cond = "WHERE consecutivo = $numero AND fecha BETWEEN '$fechaInicial' AND '$fechaFinal'";
        }
        else if ($numero == "" && $fechaInicial != "" && $fechaFinal != "") {

            $cond = "WHERE fecha BETWEEN '$fechaInicial' AND '$fechaFinal'";
        }
        else if ($numero != "" && $fechaInicial == "" && $fechaFinal == "") {

            $cond = "WHERE consecutivo = $numero";
        }

        $actos = array();
        
        $sqlActos = "SELECT * FROM ccpetacuerdos $cond ORDER BY id_acuerdo DESC";
        $resActos = mysqli_query($linkbd, $sqlActos);
        while ($rowActos = mysqli_fetch_row($resActos)) {

            array_push($actos, $rowActos);
        }

        $out['actos'] = $actos;
    }
    
    if ($action == "anularActo") {

        $actoId = $_POST['actoId'];

        $sqlActo = "UPDATE ccpetacuerdos SET estado = 'N' WHERE id_acuerdo = $actoId";
        mysqli_query($linkbd, $sqlActo);

        $out['insertaBien'] = true;
    }

    header("Content-type: application/json");
    echo json_encode($out);
    die();