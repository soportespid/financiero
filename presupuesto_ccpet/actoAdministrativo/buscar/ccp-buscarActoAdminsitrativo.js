var app = new Vue({
    el: '#myapp',
	data:{

        error: '',
        loading: false,

        numero: '',
        actosAdministrativos: [],
    },

    mounted() {
        this.loading = false;


    let today = new Date();

    // `getDate()` devuelve el día del mes (del 1 al 31)
    let day = today.getDate();

    // `getMonth()` devuelve el mes (de 0 a 11)
    let month = today.getMonth() + 1;


    // `getFullYear()` devuelve el año completo
    let year = today.getFullYear();

    document.getElementById('fc_1198971545').value = '01'+'/'+'01'+'/'+year;

    // muestra la fecha de hoy en formato `MM/DD/YYYY`

    let dayFormat = day.toString().padStart(2, '0');
    let monthFormat = month.toString().padStart(2, '0');

    document.getElementById('fc_1198971546').value = `${dayFormat}/${monthFormat}/${year}`;
    },

    computed: {
        existeInformacion(){
          const det = JSON.parse(JSON.stringify(this.actosAdministrativos));
          return det.length > 0 ? true : false
        },
    },

    methods:
    {
        toFormData: function(obj){
            var form_data = new FormData();
            for(var key in obj){
                form_data.append(key, obj[key]);
            }
            return form_data;
        },

        formatonumero: function(valor){
            return new Intl.NumberFormat("en-US", {style: "currency", currency: "USD"}).format(valor);
        },

        buscarActo: function(tipoActo) {

            switch (tipoActo) {
                case "1":
                    return "Por acuerdo";
                break;

                case "2":
                    return "Por resolución";
                break;

                case "3":
                    return "Por decreto";
                break;

                default:
                    return "Error";
                break;
            }
        },

        buscaEstado: function(estado) {

            switch (estado) {
                case 'S':
                    return "Activo";
                break;

                case 'N':
                    return "Anulado";
                break;

                case 'F':
                    return "Finalizado";
                break;

                default:
                    return "Error";
                break;
            }
        },

        anular: function(estado, actoId) {

            switch (estado) {
                case "S":
                    Swal.fire({
                        icon: 'question',
                        title: 'Seguro que quieres anular?',
                        showDenyButton: true,
                        confirmButtonText: 'Anular',
                        denyButtonText: 'Cancelar',
                        }).then((result) => {
                        if (result.isConfirmed)
                        {
                            if (result.isConfirmed) {

                                var formData = new FormData();
                                formData.append("actoId", actoId);

                                axios.post('presupuesto_ccpet/actoAdministrativo/buscar/ccp-buscarActoAdministrativo.php?action=anularActo', formData)
                                .then((response) => {

                                    if(response.data.insertaBien){
                                        Swal.fire({
                                            icon: 'success',
                                            title: 'Se ha anulado el acto administrativo con exito!',
                                            showConfirmButton: false,
                                            timer: 1500
                                            }).then((response) => {
                                                this.redireccionar();
                                            });
                                    }
                                    else {
                                        Swal.fire(
                                            'Error!',
                                            'No se pudo anular.',
                                            'error'
                                        );
                                    }
                                });
                            }
                        }
                        else if (result.isDenied)
                        {
                            Swal.fire('Anular cancelado', '', 'info');
                        }
                    })
                break;

                case "F":
                    Swal.fire('Información', 'El acto administrativo ya finalizo, ya no se puede anular', 'info');
                break;

                case "N":
                    Swal.fire('Información', 'El acto administrativo ya fue anulado', 'info');
                break;

                default:

                break;
            }
        },

        redireccionar: function() {
            location.reload();
        },

        seleccionaActo: function(acto) {

            location.href="ccp-editaActoAdministrativo.php?cod="+acto[0];
        },

        buscarActos: async function() {

            var fechaInicial = document.getElementById('fc_1198971545').value;
            var fechaFinal = document.getElementById('fc_1198971546').value;

            if (this.numero != '' || (fechaInicial != "" && fechaFinal != "")) {

                var formData = new FormData();
                formData.append("numero", this.numero);
                formData.append("fechaInicial", fechaInicial);
                formData.append("fechaFinal", fechaFinal);

                await axios.post('presupuesto_ccpet/actoAdministrativo/buscar/ccp-buscarActoAdministrativo.php?action=buscaActos', formData)
                .then((response) => {

                    app.actosAdministrativos = response.data.actos;
                    // console.log(response.data);
                }).catch((error) => {
                    this.error = true;
                    console.log(error)
                }).finally(() => {

                });
            }
            else {
                Swal.fire('Falta información', 'Utilice correctamente el campo de filtros', 'info');
            }
        },
    }
});
