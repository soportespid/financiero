<?php
    require_once '../../../comun.inc';
    require '../../../funciones.inc';
    session_start();

    $linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");

    $out = array('error' => false);

    $action = "show";

    if(isset($_GET['action'])){
        $action = $_GET['action'];
    }
    
    if ($action == "traerDatos") {

        $actoId = $_POST['actoId'];
        $actoAdministrativo = array();
        
        $sqlAcuerdo = "SELECT * FROM ccpetacuerdos WHERE id_acuerdo = $actoId";
        $rowAcuerdo = mysqli_fetch_row(mysqli_query($linkbd, $sqlAcuerdo));

        array_push($actoAdministrativo, $rowAcuerdo);

        $out['actoAdministrativo'] = $actoAdministrativo;
    }

    if ($action == "guardarInicial") {

        preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST["fecha"], $f);
        $actoId = $_POST['actoId'];
		$fecha = $f[3]."-".$f[2]."-".$f[1];
        $vigencia = $f[3];
        $tipoActo = $_POST['tipoActo'];
        $numero = $_POST['numero'];
        $descripcionActo = $_POST['descripcionActo'];
        $tipoAcuerdo = $_POST['tipoAcuerdo'];
        $valorInicial = $_POST['valorInicial'];
        $valorAdicion = 0;
        $valorReduccion = 0;
        $valorTraslado = 0;

        $sqlActo = "UPDATE ccpetacuerdos SET fecha = '$fecha', vigencia = '$vigencia', numero_acuerdo = '$descripcionActo', tipo = '$tipoAcuerdo', valorinicial = $valorInicial, valoradicion = $valorAdicion, valorreduccion = $valorReduccion, valortraslado = $valorTraslado WHERE id_acuerdo = $actoId";
        mysqli_query($linkbd, $sqlActo);

        $out['insertaBien'] = true;
    }

    if ($action == "guardarModificacion") {

        preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST["fecha"], $f);
        $actoId = $_POST['actoId'];
		$fecha = $f[3]."-".$f[2]."-".$f[1];
        $vigencia = $f[3];
        $tipoActo = $_POST['tipoActo'];
        $numero = $_POST['numero'];
        $descripcionActo = $_POST['descripcionActo'];
        $tipoAcuerdo = $_POST['tipoAcuerdo'];
        $valorInicial = 0;
        $valorAdicion = $_POST['valorAdicion'];
        $valorReduccion = $_POST['valorReduccion'];
        $valorTraslado = $_POST['valorTraslados'];

        $sqlActo = "UPDATE ccpetacuerdos SET fecha = '$fecha', vigencia = '$vigencia', numero_acuerdo = '$descripcionActo', tipo = '$tipoAcuerdo', valorinicial = $valorInicial, valoradicion = $valorAdicion, valorreduccion = $valorReduccion, valortraslado = $valorTraslado WHERE id_acuerdo = $actoId";
        mysqli_query($linkbd, $sqlActo);

        $out['insertaBien'] = true;
    }

    header("Content-type: application/json");
    echo json_encode($out);
    die();