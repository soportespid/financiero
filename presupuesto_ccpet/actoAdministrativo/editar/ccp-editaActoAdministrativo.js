var app = new Vue({
    el: '#myapp',
	data:{

        error: '',
        loading: false,
        
        actoId: '',
        tipoActo: '',
        consecutivo: '',
        descripcionActo: '',
        tipoAcuerdo: '',
        valorInicial: 0,
        valorAdicion: 0,
        valorReduccion: 0,
        valorTraslados: 0,
        estado: '',
    },

    mounted: function(){
        
        this.traeInformacionBase();
    },

    methods: 
    {
        toFormData: function(obj){
            var form_data = new FormData();
            for(var key in obj){
                form_data.append(key, obj[key]);
            }
            return form_data;
        },

        traeParametros: function(name){
            //Captura parametros
            name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
            var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
            results = regex.exec(location.search);
            return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
        },

        traeInformacionBase: async function() {

            this.actoId = this.traeParametros('cod');
            var formData = new FormData();
            formData.append("actoId", this.actoId);

            await axios.post('presupuesto_ccpet/actoAdministrativo/editar/ccp-editaActoAdministrativo.php?action=traerDatos', formData)
            .then((response) => {
                this.tipoActo = response.data.actoAdministrativo[0][11];
                this.consecutivo = response.data.actoAdministrativo[0][1];
                this.descripcionActo = response.data.actoAdministrativo[0][2];
                this.tipoAcuerdo = response.data.actoAdministrativo[0][10];
                this.valorInicial = response.data.actoAdministrativo[0][8];
                this.valorAdicion = response.data.actoAdministrativo[0][5];
                this.valorReduccion = response.data.actoAdministrativo[0][6];
                this.valorTraslados = response.data.actoAdministrativo[0][7];
                this.estado = response.data.actoAdministrativo[0][9];
                var fecha = response.data.actoAdministrativo[0][3].split('-'); 
                document.getElementById('fecha').value = fecha[2]+'/'+fecha[1]+'/'+fecha[0];
            }).catch((error) => {
                this.error = true;
                console.log(error)
            }).finally(() => {
                
            });     
        },

        guardar: function() {

            switch (this.estado) {
                case 'S':
                    var fecha = document.getElementById('fecha').value;

                    if (fecha != "" && this.tipoActo && this.numero != "" && this.descripcionActo != "" && this.tipoAcuerdo != "") {
                        if (this.tipoAcuerdo == "I") {

                            if (this.valorInicial >= 0) {

                                Swal.fire({
                                    icon: 'question',
                                    title: 'Seguro que quieres guardar?',
                                    showDenyButton: true,
                                    confirmButtonText: 'Guardar',
                                    denyButtonText: 'Cancelar',
                                    }).then((result) => {
                                    if (result.isConfirmed) 
                                    {
                                        if (result.isConfirmed) {
                                            this.loading = true;
                                            var formData = new FormData();
                
                                            formData.append("actoId", this.actoId);
                                            formData.append("fecha", fecha);
                                            formData.append("tipoActo", this.tipoActo);
                                            formData.append("numero", this.consecutivo);
                                            formData.append("descripcionActo", this.descripcionActo);
                                            formData.append("tipoAcuerdo", this.tipoAcuerdo);
                                            formData.append("valorInicial", this.valorInicial);
                                            
                                            axios.post('presupuesto_ccpet/actoAdministrativo/editar/ccp-editaActoAdministrativo.php?action=guardarInicial', formData)
                                            .then((response) => {
                                                
                                                console.log(response.data);
                                                if(response.data.insertaBien){
                                                    this.loading = false;
                                                    Swal.fire({
                                                        icon: 'success',
                                                        title: 'Se ha actualizado el acto con exito!',
                                                        showConfirmButton: false,
                                                        timer: 1500
                                                        }).then((response) => {
                                                            this.redireccionar();
                                                        });
                                                }
                                                else {
                                                    Swal.fire(
                                                        'Error!',
                                                        'No se pudo guardar.',
                                                        'error'
                                                    );
                                                }
                                            });
                                            
                                        }
                                    } 
                                    else if (result.isDenied) 
                                    {
                                        Swal.fire('Guardar cancelado', '', 'info');
                                    }
                                })
                            }   
                            else {
                                Swal.fire('Valor inicial esta en cero', '', 'info');
                            }
                        }
                        else if (this.tipoAcuerdo == "M") {

                            if (this.valorAdicion >= 0 || this.valorReduccion >= 0 || this.valorTraslados >= 0) {

                                Swal.fire({
                                    icon: 'question',
                                    title: 'Seguro que quieres guardar?',
                                    showDenyButton: true,
                                    confirmButtonText: 'Guardar',
                                    denyButtonText: 'Cancelar',
                                    }).then((result) => {
                                    if (result.isConfirmed) 
                                    {
                                        if (result.isConfirmed) {
                                            this.loading = true;
                                            var formData = new FormData();
                
                                            formData.append("actoId", this.actoId);
                                            formData.append("fecha", fecha);
                                            formData.append("tipoActo", this.tipoActo);
                                            formData.append("numero", this.consecutivo);
                                            formData.append("descripcionActo", this.descripcionActo);
                                            formData.append("tipoAcuerdo", this.tipoAcuerdo);
                                            formData.append("valorAdicion", this.valorAdicion);
                                            formData.append("valorReduccion", this.valorReduccion);
                                            formData.append("valorTraslados", this.valorTraslados);
                                            
                                            axios.post('presupuesto_ccpet/actoAdministrativo/editar/ccp-editaActoAdministrativo.php?action=guardarModificacion', formData)
                                            .then((response) => {
                                                
                                                if(response.data.insertaBien){
                                                    this.loading = false;
                                                    Swal.fire({
                                                        icon: 'success',
                                                        title: 'Se ha actualizado el acto con exito!',
                                                        showConfirmButton: false,
                                                        timer: 1500
                                                        }).then((response) => {
                                                            this.redireccionar();
                                                        });
                                                }
                                                else {
                                                    Swal.fire(
                                                        'Error!',
                                                        'No se pudo guardar.',
                                                        'error'
                                                    );
                                                }
                                            });
                                            
                                        }
                                    } 
                                    else if (result.isDenied) 
                                    {
                                        Swal.fire('Guardar cancelado', '', 'info');
                                    }
                                })
                            }
                            else {
                                Swal.fire('Valores en cero', '', 'info');
                            }
                        }
                    }
                    else {
                        Swal.fire('Llena los datos completos', '', 'info');
                    }
                break;

                case 'F':
                    Swal.fire('Información', 'El acto administrativo ya finalizo, ya no se puede anular', 'error');
                break;

                case 'N':
                    Swal.fire('Información', 'El acto administrativo ya fue anulado', 'error');
                break;
            
                default:
                    break;
            }
        },

        redireccionar: function()
        {
            location.reload();  
        },
    }
});