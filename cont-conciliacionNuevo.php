<?php

	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require 'comun.inc';
	require 'funciones.inc';
	session_start();
	if(empty($_SESSION)){
		header("location: index.php");
	}
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html lang=es>
	<head>
	<meta charset="utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1"/>
		<meta name="viewport" content="user-scalable=no">
		<title>:: IDEAL 10 - Contabilidad</title>
		<link href="favicon.ico" rel="shortcut icon"/>
		<link href="css/css2z.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/tabs.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
		<link href="css/style.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
		<link rel="stylesheet" href="sweetalert2/dist/sweetalert2.min.css">
		<script type="text/javascript" src="css/programas.js"></script>
		<script src="sweetalert2/dist/sweetalert2.min.js"></script>
		<script src="vue/vue.min.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script src="css/calendario.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<style>
			.radio-label {
				display: flex;
				align-items: center;
				gap: 8px;
			}
			.radio-label input[type="radio"] {
				margin: 0;
			}
		</style>
	</head>
	<body>
		<header>
			<table>
				<tr><script>barra_imagenes("cont");</script><?php cuadro_titulos();?></tr>
			</table>
		</header>

		<form name="form2" method="post" action="">
			<section id="myapp" v-cloak >
				<div class="loading-container" v-show="isLoading" >
					<p class="text-loading"data-loading="IDEAL 10">IDEAL 10</p>
				</div>
				<nav>
					<table>
						<tr><?php menu_desplegable("cont");?></tr>
					</table>
					<div class="bg-white group-btn p-1">
						<button type="button" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center" @click="save()">
							<span>Guardar</span>
							<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960"><path d="M840-680v480q0 33-23.5 56.5T760-120H200q-33 0-56.5-23.5T120-200v-560q0-33 23.5-56.5T200-840h480l160 160Zm-80 34L646-760H200v560h560v-446ZM480-240q50 0 85-35t35-85q0-50-35-85t-85-35q-50 0-85 35t-35 85q0 50 35 85t85 35ZM240-560h360v-160H240v160Zm-40-86v446-560 114Z"></path></svg>
						</button>
						<button type="button" onclick="mypop=window.open('cont-principal.php','','');mypop.focus();" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
							<span>Nueva ventana</span>
							<svg class="fill-black group-hover:fill-white w-5 h-5" xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960"><path d="M320-120v-80h80v-80H160q-33 0-56.5-23.5T80-360v-400q0-33 23.5-56.5T160-840h640q33 0 56.5 23.5T880-760v400q0 33-23.5 56.5T800-280H560v80h80v80H320ZM160-360h640v-400H160v400Zm0 0v-400 400Z"></path></svg>
						</button>
						<button type="button" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center" @click="mypop=window.open('cont-conciliacionNuevo','','');mypop.focus();">
							<span class="group-hover:text-white">Duplicar pantalla</span>
							<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960"><path d="M640-160v-360H160v360h480Zm80-200v-80h80v-360H320v200h-80v-200q0-33 23.5-56.5T320-880h480q33 0 56.5 23.5T880-800v360q0 33-23.5 56.5T800-360h-80ZM160-80q-33 0-56.5-23.5T80-160v-360q0-33 23.5-56.5T160-600h480q33 0 56.5 23.5T720-520v360q0 33-23.5 56.5T640-80H160Zm400-603ZM400-340Z"></path></svg>
						</button>
						<button type="button" @click="exportData(1)" class="btn btn-danger btn-danger-hover d-flex justify-between align-items-center">
							<span>Exportar PDF</span>
							<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><!-- !Font Awesome Free 6.5.1 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license/free Copyright 2024 Fonticons, Inc. --><path d="M64 464l48 0 0 48-48 0c-35.3 0-64-28.7-64-64L0 64C0 28.7 28.7 0 64 0L229.5 0c17 0 33.3 6.7 45.3 18.7l90.5 90.5c12 12 18.7 28.3 18.7 45.3L384 304l-48 0 0-144-80 0c-17.7 0-32-14.3-32-32l0-80L64 48c-8.8 0-16 7.2-16 16l0 384c0 8.8 7.2 16 16 16zM176 352l32 0c30.9 0 56 25.1 56 56s-25.1 56-56 56l-16 0 0 32c0 8.8-7.2 16-16 16s-16-7.2-16-16l0-48 0-80c0-8.8 7.2-16 16-16zm32 80c13.3 0 24-10.7 24-24s-10.7-24-24-24l-16 0 0 48 16 0zm96-80l32 0c26.5 0 48 21.5 48 48l0 64c0 26.5-21.5 48-48 48l-32 0c-8.8 0-16-7.2-16-16l0-128c0-8.8 7.2-16 16-16zm32 128c8.8 0 16-7.2 16-16l0-64c0-8.8-7.2-16-16-16l-16 0 0 96 16 0zm80-112c0-8.8 7.2-16 16-16l48 0c8.8 0 16 7.2 16 16s-7.2 16-16 16l-32 0 0 32 32 0c8.8 0 16 7.2 16 16s-7.2 16-16 16l-32 0 0 48c0 8.8-7.2 16-16 16s-16-7.2-16-16l0-64 0-64z"></path></svg>
						</button>
						<button type="button" @click="exportData(2)" class="btn btn-success btn-success-hover d-flex justify-between align-items-center">
							<span>Exportar Excel</span>
							<svg class="fill-black group-hover:fill-white w-4 h-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 384 512"><!--!Font Awesome Free 6.5.1 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license/free Copyright 2024 Fonticons, Inc. --><path d="M48 448V64c0-8.8 7.2-16 16-16H224v80c0 17.7 14.3 32 32 32h80V448c0 8.8-7.2 16-16 16H64c-8.8 0-16-7.2-16-16zM64 0C28.7 0 0 28.7 0 64V448c0 35.3 28.7 64 64 64H320c35.3 0 64-28.7 64-64V154.5c0-17-6.7-33.3-18.7-45.3L274.7 18.7C262.7 6.7 246.5 0 229.5 0H64zm90.9 233.3c-8.1-10.5-23.2-12.3-33.7-4.2s-12.3 23.2-4.2 33.7L161.6 320l-44.5 57.3c-8.1 10.5-6.3 25.5 4.2 33.7s25.5 6.3 33.7-4.2L192 359.1l37.1 47.6c8.1 10.5 23.2 12.3 33.7 4.2s12.3-23.2 4.2-33.7L222.4 320l44.5-57.3c8.1-10.5 6.3-25.5-4.2-33.7s-25.5-6.3-33.7 4.2L192 280.9l-37.1-47.6z"></path></svg>
						</button>
						<button type="button" @click="redirect()" class="btn btn-primary btn-primary-hover d-flex justify-between align-items-center">
							<span>Auxiliar contable</span>
						</button>
						<div class="form-control flex-row w-25 align-items-center m-0">
							<label for="labelCheckExport" class="me-2">Exportar conciliados: </label>
							<div class="d-flex align-items-center ">
								<label for="labelCheckExport" class="form-switch">
									<input type="checkbox" id="labelCheckExport" v-model="isExport">
									<span></span>
								</label>
							</div>
						</div>
					</div>
				</nav>
				<article>
					<div  class="bg-white">
						<div>
							<h2 class="titulos m-0">Conciliacion Bancaria NICSP</h2>
							<div class="d-flex">
								<div class="form-control align-items-center flex-row w-50">
									<label class="form-label m-0 w-50" for="">Fecha inicial:</label>
									<input type="date" v-model="txtFechaInicial" class="w-75">
								</div>
								<div class="form-control align-items-center flex-row w-50">
									<label class="form-label m-0 w-50" for="">Fecha final:</label>
									<input type="date" v-model="txtFechaFinal" class="w-75">
								</div>
								<div class="form-control align-items-center flex-row">
									<label class="form-label m-0" for="">Cuenta:</label>
									<div class="d-flex w-100">
										<input type="text" class="bg-warning w-25 cursor-pointer" @dblclick="isModal=true" v-model="objCuenta.codigo" @change="search('cod_cuenta')">
										<input type="text" disabled v-model="objCuenta.nombre">
									</div>
								</div>
								<div class="form-control align-items-center flex-row w-50">
									<label class="form-label m-0 w-50" for="">Saldo extracto bancario:</label>
									<div class="form-number number-primary">
										<span></span>
										<input type="number" v-model="txtSaldoExtracto" @change="changeDiferencia()" class="text-right">
									</div>
								</div>
								<div class="form-control flex-row w-25">
									<label class="form-label m-0" for=""></label>
									<button type="button" class="btn btn-primary me-2" @click="generate()">Conciliar</button>
								</div>
							</div>
							<table class="table fw-normal">
								<tbody>
									<tr>
										<td class="fw-bold" >Saldo I libros</td>
										<td class="text-right">{{formatNum(txtSaldoInicial)}}</td>
										<td class="fw-bold">Saldo F libros</td>
										<td class="text-right">{{formatNum(txtSaldoFinal)}}</td>
										<td class="fw-bold bg-primary text-white" >No conciliados:</td>
										<td class="fw-bold" >Débito</td>
										<td class="text-right">{{ formatNum(txtDebitoNoConciliado)}}</td>
										<td class="fw-bold" >Crédito</td>
										<td class="text-right">{{ formatNum(txtCreditoNoConciliado)}}</td>
										<td class="fw-bold bg-primary text-white" >Conciliados:</td>
										<td class="fw-bold" >Débito</td>
										<td class="text-right">{{ formatNum(txtDebitoConciliado)}}</td>
										<td class="fw-bold" >Crédito</td>
										<td class="text-right">{{ formatNum(txtCreditoConciliado)}}</td>
									</tr>
									<tr>

										<td class="fw-bold" >Saldo extracto calculado</td>
										<td class="text-right" >{{ formatNum(txtSaldoExtractoCalculo)}}</td>
										<td class="fw-bold">Diferencia</td>
										<td class="text-right" >{{formatNum(txtDiferencia)}}</td>
										<td class="fw-bold">Estado</td>
										<td class="text-center" ><span class="badge" :class="txtDiferencia >= 0 && txtDiferencia <= 1 ? 'badge-success' : 'badge-danger'">{{txtDiferencia >= 0 && txtDiferencia <= 1 ? 'Conciliado' : 'No conciliado'}}</span></td>
										<td class="fw-bold">Observaciónes Generales:</td>
										<td><img src="imagenes/notaf.png" class="icobut" @click="abrirRegistro('G','',txtFechaInicial,)" title="Notas"/></td>
									</tr>
								</tbody>
							</table>
							<h2 class="titulos m-0">Conciliar Cuentas</h2>
						</div>
						<div class="table-responsive" style="height:55vh">
							<table  class="table fw-normal">
								<thead>
									<tr class="text-center">
										<th>Id</th>
										<th>Fecha</th>
										<th>Nombre comprobante</th>
										<th>Nro</th>
										<th>C.C</th>
										<th>Tercero</th>
										<th>Detalle</th>
										<th>Débito</th>
										<th>Crédito</th>
										<th>Fecha conciliación</th>
										<th>Registro</th>
										<th class="d-flex justify-center">
											<div class="d-flex align-items-center ">
												<label for="labelCheckAll" class="form-switch">
													<input type="checkbox" id="labelCheckAll" v-model="isCheckAll" @change="changeAll()">
													<span></span>
												</label>
											</div>
										</th>
									</tr>
								</thead>
								<tbody>
									<tr v-for="(data,index) in arrData" :key="index">
										<td>{{data.id_det}}</td>
										<td class="text-nowrap">{{data.fecha_format}}</td>
										<td>{{data.nombre_comp}}</td>
										<td class="text-center">{{data.numerotipo}}</td>
										<td class="text-center">{{data.centrocosto}}</td>
										<td>{{data.nombre_tercero}}</td>
										<td>{{data.concepto}}</td>
										<td class="text-right">{{formatNum(data.debito)}}</td>
										<td class="text-right">{{formatNum(data.credito)}}</td>
										<td class="text-center">{{data.periodo_format}}</td>
										<td style="text-align: center;"><img src="imagenes/notaf.png" class="icobut" @click="abrirRegistro('D', data.tipo_comp, data.fecha_format, data.nombre_comp, data.numerotipo)" title="Notas"/></td>
										<td class="d-flex justify-center">
											<div class="d-flex align-items-center" v-if="data.is_show">
												<label :for="'labelCheckName'+index" class="form-switch">
													<input type="checkbox" :id="'labelCheckName'+index" :checked="data.is_checked" @change="changeStatus(index)">
													<span></span>
												</label>
											</div>
										</td>
									</tr>
								</tbody>
								<tfoot>
									<tr>
										<td class="fw-bold text-right bg-secondary" colspan="7">Total:</td>
										<td class="text-right bg-secondary">{{formatNum(txtTotalDebito)}}</td>
										<td class="text-right bg-secondary">{{formatNum(txtTotalCredito)}}</td>
										<td class="bg-secondary"></td>
										<td class="bg-secondary"></td>
										<td class="bg-secondary"></td>
									</tr>
								</tfoot>
							</table>
						</div>
					</div>
					<div v-show="isModal" class="modal"><!-- Modal Cuentas -->
						<div class="modal-dialog modal-lg" >
							<div class="modal-content">
								<div class="modal-header">
									<h5 class="modal-title">Buscar cuentas</h5>
									<button type="button" @click="isModal=false;" class="btn btn-close"><div></div><div></div></button>
								</div>
								<div class="modal-body">
									<div class="d-flex flex-column">
										<div class="form-control m-0 mb-3">
											<input type="search" v-model="txtSearch" @keyup="search('modal')" placeholder="Buscar cuenta, cuenta bancaria o nombre">
										</div>
										<div class="form-control m-0 p-2">
											<label class="form-label" for="labelInputName">Resultados: <span class="fw-bold">{{txtResultados}}</span></label>
										</div>
									</div>
									<div class="overflow-auto max-vh-50 overflow-x-hidden " >
										<table class="table table-hover fw-normal p-2">
											<thead>
												<tr>
													<th class="text-center">Item</th>
													<th class="text-center">Nombre</th>
													<th class="text-center">Cuenta</th>
													<th class="text-center">Cuenta banco</th>
												</tr>
											</thead>
											<tbody>
												<tr v-for="(data,index) in arrCuentasCopy" :key="index" @dblclick="selectItem(data)">
													<td>{{index+1}}</td>
													<td>{{data.nombre}}</td>
													<td>{{data.codigo}}</td>
													<td>{{data.cuenta_banco}}</td>
												</tr>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div v-show="isModal2" class="modal"><!-- Modal Notas Registros -->
						<div class="modal-dialog modal-lg" >
							<div class="modal-content">
								<div class="modal-header">
									<h5 class="modal-title">Registro de Diferencias Conciliatorias</h5>
									<button type="button" @click="cerrarNotas()" class="btn btn-close"><div></div><div></div></button>
								</div>
								<div class="modal-body">
									<div class="d-flex flex-column">
										<div class="form-control w-100">
											<label class="form-label text-center">
												<strong>Cuenta:</strong> {{ objNota.numCuenta }}&nbsp;&nbsp;
												<strong>Fecha:</strong> {{ objNota.fecha }}&nbsp;&nbsp;
												<strong>Tipo:</strong> {{ objNota.tipoNota === 'G' ? 'Observación General' : objNota.nomComprobante }}&nbsp;&nbsp;
												<span v-if="objNota.tipoNota !== 'G'">
													<strong>No.</strong> {{ objNota.numComprobante }}
												</span>
											</label>
										</div>
										<div class="d-flex w-100">
											<div class="form-control w-50">
												<label class="form-label" for="">Fecha:</label>
												<input type="date" v-model="notaNueva.fecha" class="w-75">
											</div>
											<div class="form-control number-primary w-50">
												<label class="form-label" for="">Valor:</label>
												<input type="number" v-model="notaNueva.valor" class="text-right w-75">
											</div>
										</div>
										<div class="d-flex w-100">
											<div class="form-control w-100">
												<label class="form-label" for="">Descripcion:</label>
												<textarea v-model="notaNueva.registro" rows='3' cols='100' >{{notaNueva.registro}}</textarea>
											</div>
										</div>
										<div class="d-flex w-100">
											<div class="form-control w-15">
												<label class="form-label">Movimiento:</label>
												<label class="radio-label">
													<input type="radio" v-model="notaNueva.tipoMovimiento" value="debito" class="w-15"/>
													<span>Débito</span>
												</label>
											</div>
											<div class="form-control w-15">
												<label class="form-label">&nbsp;&nbsp;</label>
												<label class="radio-label">
													<input type="radio" v-model="notaNueva.tipoMovimiento" value="credito" class="w-15"/>
													<span>Crédito</span>
												</label>
											</div>
											<div class="form-control w-25">
												<label class="form-label m-0" for="">&nbsp;&nbsp;</label>
												<button type="button" class="btn btn-primary me-2" @click="guardarNotas()">Agregar Nota</button>
											</div>
											<div class="form-control w-25">
												<label class="form-label m-0" for="">&nbsp;&nbsp;</label>
												<button type="button" class="btn btn-white me-2" @click="limpiarNotas()">Limpiar Campos</button>
											</div>
										</div>
									</div>
									<div class="overflow-auto max-vh-50 overflow-x-hidden " >
										<table class="table table-hover fw-normal p-2">
											<thead>
												<tr>
													<th class="text-center">Fecha</th>
													<th class="text-center">Descripción</th>
													<th class="text-center">Débito</th>
													<th class="text-center">Crédito</th>
													<th class="text-center">Eliminar</th>
												</tr>
											</thead>
											<tbody>
												<tr v-for="(data,index) in arrNotas" :key="index">
													<td>{{data.fechaaux}}</td>
													<td>{{data.nota}}</td>
													<td>{{data.debito}}</td>
													<td>{{data.credito}}</td>
													<td><div class='garbageX' title='Eliminar' @click="eliminarNotas(data.id)"></td>
												</tr>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</article>
			</section>
		</form>

		<script src="node_modules/read-excel-file/bundle/read-excel-file.min.js"></script>
		<script src="node_modules/xlsx/dist/xlsx.full.min.js"></script>
		<script src="Librerias/vue/vue.min.js"></script>
		<script src="Librerias/vue/axios.min.js"></script>
		<script type="module" src="contabilidad_vue/procesos/js/functions_conciliacion.js?<?= date('d_m_Y_h_i_s');?>"></script>

	</body>
</html>
