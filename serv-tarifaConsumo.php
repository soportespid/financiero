<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require 'comun.inc';
	require 'funciones.inc';
	session_start();
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
	titlepag();
?>
<!DOCTYPE >
<html lang=es>
	<head>
		<meta charset="utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1"/>
		<meta name="viewport" content="user-scalable=no">
		<title>:: IDEAL 10 - Servicios Públicos</title>
		<link href="css/css2z.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/tabs.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
		<script src="vue/vue.min.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script src="css/calendario.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<style>
			input[type=number]::-webkit-inner-spin-button,
			input[type=number]::-webkit-outer-spin-button { 
				-webkit-appearance: none; 
				margin: 0; 
			}
			input[type=number]{
				text-align:right;
				box-sizing: border-box;
				font: 120% sans-serif;
				width: 100% !important;
			}
			[v-cloak]{display : none;}

			.centrarSelect {

				height: 30px !important;
				text-align-last:center !important;
			}

			.aumentarTamaño {

				font-size:15px;
			}

            .tamano01 {
                text-align: center !important;
            }
		</style>
		<script src="sweetalert2/dist/sweetalert2.min.js"></script>
		<link rel="stylesheet" href="sweetalert2/dist/sweetalert2.min.css">
	</head>
	<body>
		<header>
			<table>
				<tr><script>barra_imagenes("serv");</script><?php cuadro_titulos();?></tr>
			</table>
		</header>

		<form name="form2" method="post" action="">
			<section id="myapp" v-cloak >
				<nav>
					<table>
						<tr><?php menu_desplegable("serv");?></tr>
						<tr>
							<td colspan="3" class="cinta">
								<img src="imagenes/add.png" class="mgbt" v-on:click="location.href='serv-rangoConsumo.php'" title="Nuevo">
								<img src="imagenes/guarda.png" v-on:click="guardar" title="Guardar"  class="mgbt">
								<img src="imagenes/buscad.png" class="mgbt1" title="Buscar">
								<img src="imagenes/nv.png" onClick="mypop=window.open('serv-principal.php','',''); mypop.focus();" class="mgbt" title="Nueva Ventana">
							</td>
						</tr>
					</table>
				</nav>
				<article>
					<table class="inicio ancho">
						<tr>
							<td class="titulos" colspan="6">Rangos de consumo</td>
							<td class="cerrar" style="width:7%" onClick="location.href='serv-principal.php'">Cerrar</td>
						</tr>

                        <tr>
							<td class="tamano01" style="width: 10%;">Servicio: </td>
							<td style="width: 10%;">
								<select v-model="servicio" class="centrarSelect">
									<option class="aumentarTamaño" v-for="servicio in servicios" v-bind:value = "servicio[0]">
										{{ servicio[1] }}
									</option>
								</select>
							</td>

                            <td class="tamano01" style="width: 10%;">Clase de uso: </td>
							<td style="width: 15%;">
								<input type="text" v-model="nombreClase" v-on:dblclick="despliegaModalClases" style="width: 100%; height: 30px; text-align:center;" class="colordobleclik" readonly>
							</td>

                            <td colspan="2" style="padding-bottom:0px">
								<em class="botonflechaverde" v-on:Click="buscarTarifas">Mostrar tarifas</em>
							</td>
						</tr>
					</table>

					<div class='subpantalla' style='height:66vh; width:99.2%; margin-top:0px; overflow:hidden'>
						<table class='tablamv'>
							<thead>
								<tr style="text-align:Center;">
                                    <th class="titulosnew00">Ubicación</th>
									<th class="titulosnew00" style="width:25%;">Rango</th>
									<th class="titulosnew00" style="width:15%;">Valor</th>
									<th class="titulosnew00" style="width:15%;">Subsidio</th>
                                    <th class="titulosnew00" style="width:15%;">Contribución</th>
								</tr>
							</thead>
							<tbody>
								<tr v-for="(tarifa, index) in tarifas" v-bind:class="index % 2 ? 'saludo1a' : 'saludo2'" style='text-rendering: optimizeLegibility; cursor: pointer !important;'>
									<td style="font: 120% sans-serif; padding-left:5px; text-align:center;">{{ tarifa[1] }}</td>
									<td style="width:25%; font: 120% sans-serif; padding-left:5px; text-align:center;">{{ tarifa[3] }}</td>
									<td style="width:15%; font: 120% sans-serif; padding-left:5px; text-align:center;">
										<input type="number" v-model="valor[index]" v-on:change="validaValor(index)" style="text-align: center;">
									</td>
									<td style="width:15%; font: 120% sans-serif; padding-left:5px; text-align:center;">
										<input type="number" v-model="subsidio[index]" v-on:change="validaSubsidio(index)" style="text-align: center;">
									</td>
									<td style="width:15%; font: 120% sans-serif; padding-left:5px; text-align:center;">
										<input type="number" v-model="contribucion[index]" v-on:change="validaContribucion(index)" style="text-align: center;">
									</td>
								</tr>
							</tbody>
						</table>
					</div>

					<div id="cargando" v-if="loading" class="loading">
						<span>Cargando...</span>
					</div>

                    <div v-show="showClases">
						<transition name="modal">
							<div class="modal-mask">
								<div class="modal-wrapper">
									<div class="modal-container1">
										<table class="inicio ancho">
											<tr>
												<td class="titulos" colspan="2" >Clases de uso</td>
												<td class="cerrar" style="width:7%" @click="showClases = false">Cerrar</td>
											</tr>
											<tr>
												<td class="tamano01" style="width:3cm">Clase:</td>
												<td>
													<input type="text" v-model="searchClase.keywordClase" class="form-control" placeholder="Buscar por codigo o nombre de clase" v-on:keyup="searchMonitorClase" style="width:100%" />
												</td>
											</tr>
										</table>
										<table class='tablamv'>
											<thead>
												<tr style="text-align:center;">
													<th class="titulosnew02" >Resultados Busqueda</th>
												</tr>
												<tr style="text-align:center;">
													<th class="titulosnew00" style="width:12%;">Código <br> clase</th>
													<th class="titulosnew00">Nombre</th>
												</tr>
											</thead>
											<tbody>
												<tr v-for="(clase,index) in clases" v-on:click="seleccionaClase(clase)" v-bind:class="index % 2 ? 'saludo1a' : 'saludo2'" style='text-rendering: optimizeLegibility; cursor: pointer !important;'>
													<td style="font: 120% sans-serif; padding-left:10px; width:12%; text-align:center;">{{ clase[0] }}</td>
													<td style="font: 120% sans-serif; padding-left:10px; text-align:center;">{{ clase[1] }}</td>
												</tr>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</transition>
					</div>

				</article>
			</section>
		</form>
        
		<script src="Librerias/vue/vue.min.js"></script>
		<script src="Librerias/vue/axios.min.js"></script>
		<script src="servicios_publicos/tarifa_consumo/serv-tarifaConsumo.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
	</body>
</html>