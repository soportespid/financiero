<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require 'comun.inc';
	require 'funciones.inc';
	require 'validaciones.inc';
	require 'conversor.php';
	session_start();
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html lang=es>
	<head>
		<meta charset="utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1"/>
		<meta name="viewport" content="user-scalable=no">
		<title>:: IDEAL 10 - Gesti&oacute;n humana</title>
		<link href="favicon.ico" rel="shortcut icon"/>
		<link href="css/css2z.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet"/>
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet"/>
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet"/>
		<link href="sweetalert2/dist/sweetalert2.min.css" rel="stylesheet">
		<script src="sweetalert2/dist/sweetalert2.min.js"></script>
		<script src="vue/vue.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script src="css/calendario.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
	</head>
	<body>
		<header>
			<table>
				<tr><script>barra_imagenes("hum");</script><?php cuadro_titulos();?></tr>
			</table>
		</header>
		<section id="myapp" v-cloak>
			<nav>
				<table>
					<tr><?php menu_desplegable("hum");?></tr>
					<tr>
						<td colspan="3" class="cinta">
						<img src="imagenes/add.png" v-on:Click="location.href='hum-aprobarnomina.php'" class="mgbt" title="Nuevo" >
							<img src="imagenes/guardad.png" class="mgbt1">
							<img src="imagenes/busca.png" onClick="location.href='hum-aprobarnominabuscar.php'" class="mgbt" title="Buscar">
							<img src="imagenes/nv.png" onClick="mypop=window.open('hum-principal.php','',''); mypop.focus();" class="mgbt" title="Nueva Ventana">
							<img src='imagenes/iratras.png' title="Atr&aacute;s" v-on:Click="retornabuscar();" class="mgbt">
						</td>
					</tr>
				</table>
			</nav>
			<article>
				<table class="inicio ancho">
					<tr>
						<td class="titulos" colspan="7" >Aprobar Nómina</td>
						<td class="cerrar" style="width:7%" onClick="location.href='hum-principal.php'">Cerrar</td>
					</tr>
					<tr>
						<td class="textonew01" style="width:3cm;">N° Aprobación:</td>
						<td style="width:12%;"><img src="imagenes/back.png" v-on:Click="atrasc()" class="icobut" title="Anterior" style="height: 23px;">&nbsp;<input type="text" v-model="idcomp" style="width: 60%;" :value="idcomp=<?php echo $_GET['idr']; ?>" readonly>&nbsp;<img src="imagenes/next.png" v-on:Click="adelante()" class="icobut" title="Sigiente" style="height: 23px;"></td>
						<td class="textonew01" style="width:2.2cm;">Fecha:</td>
						<td style="width:12%;"><input type="text" v-model="fecha" readonly></td>
						<td class="textonew01" style="width:3cm;">N° Liquidación:</td>
						<td colspan="2"><input type="text" v-model="idliq" style="width: 100%;" readonly></td>
					</tr>
					<tr>
						<td class="textonew01">N° CDP:</td>
						<td><input type="text" v-model="idcdp" style="width: 100%;" readonly></td>
						<td class="textonew01">N° RP:</td>
						<td><input type="text" v-model="idrp" style="width: 100%;" readonly></td>
						<td colspan="3"><input type="text" v-model="desrp" style="width: 100%;" readonly></td>
					</tr>
					<tr>
						<td class="textonew01">Valor N&oacute;mina:</td>
						<td><input type="text" v-model="valnomina" style="width: 100%;" class="colordobleclik" autocomplete="off" v-on:DblClick="vernomina();" title='Ver Nómina' readonly></td>
						<td class="textonew01">Valor CDP:</td>
						<td><input type="text" v-model="valcdp" style="width: 100%;" class="colordobleclik" autocomplete="off" v-on:DblClick="vercdp();" title='Ver CDP' readonly></td>
						<td class="textonew01">Valor RP:</td>
						<td style="width:12%;"><input type="text" v-model="valrp" class="colordobleclik" autocomplete="off" v-on:DblClick="verrp();" style="width: 100%;" title='Ver RP' readonly></td>
						<td></td>
						
					</tr>
					<tr>
						<td class="textonew01">Tercero:</td>
						<td><input type="text" v-model="vtercero" style="width:100%;" readonly></td>
						<td colspan="4"><input type="text" v-model="vntercero"  style="width:100%;" readonly></td>
						<td></td>
					</tr>
				</table>
				<div v-show="showModalTerceros">
					<transition name="modal">
						<div class="modal-mask">
							<div class="modal-wrapper">
								<div class="modal-container2">
									<table class="inicio ancho">
										<tr>
											<td class="titulos" colspan="2" >SELECCIONAR TERCERO</td>
											<td class="cerrar" style="width:7%" @click="showModalTerceros = false">Cerrar</td>
										</tr>
										<tr>
											<td class="tamano01" style="width:3cm">Documento:</td>
											<td><input type="text" class="form-control" placeholder="Buscar por nombre o documento" v-on:keyup="buscarTerceros" v-model="searchTercero.keywordTercero" style="width:100%" /></td>
										</tr>
									</table>
									<table class='tablamv'>
										<thead>
											<tr style="text-align:Center;">
												<th class="titulosnew02" >Resultados Busqueda</th>
											</tr>
											<tr style="text-align:Center;">
												<th class="titulosnew00" style="width:6%;">Item</th>
												<th class="titulosnew00">Razon Social</th>
												<th class="titulosnew00" style="width:15%;">Primer Apellido</th>
												<th class="titulosnew00" style="width:15%;">Segundo Apellido</th>
												<th class="titulosnew00" style="width:15%;">Primer Nombre</th>
												<th class="titulosnew00" style="width:15%;">Segundo Nombre</th>
												<th class="titulosnew00" style="width:15%; ">Documento</th>
											</tr>
										</thead>
										<tbody>
											<tr v-for="(infotercero,index) in infoterceros" v-on:click="ingresarTercero(infotercero[0], infotercero[1], infotercero[2], infotercero[3], infotercero[4], infotercero[5])" v-bind:class="index % 2 ? 'saludo1a' : 'saludo2'" style='text-rendering: optimizeLegibility; cursor: pointer !important;'>
											<td style="font: 120% sans-serif; padding-left:10px; width:6%;">{{ index + 1 }}</td>
												<td style="font: 120% sans-serif; padding-left:10px;">{{ infotercero[0] }}</td>
												<td style="font: 120% sans-serif; padding-left:10px; width:15%;">{{ infotercero[1] }}</td>
												<td style="font: 120% sans-serif; padding-left:10px; width:15%;">{{ infotercero[2] }}</td>
												<td style="font: 120% sans-serif; padding-left:10px; width:15%;">{{ infotercero[3] }}</td>
												<td style="font: 120% sans-serif; padding-left:10px; width:15%;">{{ infotercero[4] }}</td>
												<td style="font: 120% sans-serif; padding-left:10px; width:15%; text-align:right;">{{ infotercero[5] }}</td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</transition>
				</div>
				<div id="cargando" v-if="loading" class="loading" style="z-index: 9999;">
					<span>Cargando...</span>
				</div>
			</article>
			<input type="hidden" v-model="idmayor"/>
			<input type="hidden" v-model="idmenor"/>
			<input type="hidden" v-model="idbuscar" :value="idbuscar=2"/>
			<input type="hidden" v-model="fechagt1" :value="fechagt1='<?php echo $_GET['fechaini']?>'"/>
			<input type="hidden" v-model="fechagt2" :value="fechagt2='<?php echo $_GET['fechafin']?>'"/>
			<input type="hidden" v-model="scrtop" :value="scrtop='<?php echo $_GET['scrtop']?>'"/>
		</section>
		<script src="Librerias/vue/vue.min.js"></script>
		<script src="Librerias/vue/axios.min.js"></script>
		<script src="vue/gestion_humana/hum-aprobarnomina.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
	</body>
</html>