<?php
	require "comun.inc";
	require "funciones.inc";
	session_start();
	$linkbd_v7 = conectar_v7();
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	header("Cache-control: private"); // Arregla IE 6
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es">
	<head>
        <meta http-equiv="Content-Type" content="text/html" />
        <meta http-equiv="X-UA-Compatible" content="IE=9" />
        <title>:: IDEAL 10 - Informes</title>
        <link href="css/css2z.css" rel="stylesheet" type="text/css" />
		<link href="css/css3.css" rel="stylesheet" type="text/css" />
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="css/programas.js"></script>
		<script type="text/javascript" src="css/calendario.js"></script>
		<script type="text/javascript" src="JQuery/jquery-2.1.4.min.js"></script>
        <script type="text/javascript" src="JQuery/autoNumeric-master/autoNumeric-min.js"></script>
		<?php titlepag();?>
		<script>
		$(window).load(function () { $('#cargando').hide();});
		function generar()
		{
			document.form2.genbal.value=1;
			//document.form2.gbalance.value=0;
			document.form2.submit();
		}
		function eliminar(variable)
		{
			if (confirm("Esta Seguro de Eliminar"))
			{
				document.form2.elimina.value=variable;
				document.form2.oculto.value=9;
				document.form2.submit();
			}
		}

		function agregardetalle()
		{
			if(document.form2.cuenta.value!="")
			{
				/* var cuentasAgregadas = document.getElementsByClassName("dcuentas");
				console.log(cuentasAgregadas); */

				document.form2.agregadet.value=1;
				document.form2.oculto.value=9;
				document.form2.submit();
			}
			else
			{
				alert("Falta informacion para poder Agregar");
			}
		}

		function despliegamodal2(_valor,v)
		{
			document.getElementById("bgventanamodal2").style.visibility=_valor;
			if(_valor=="hidden"){
				document.getElementById('ventana2').src="";
				document.form2.submit();
			}
			else {

				if(v==1){
					document.getElementById('ventana2').src="cuentasin-ventana2.php";
				}
				else if(v==2)
				{
					document.getElementById('ventana2').src="cun-ventana01.php?objeto=tercero&nobjeto=ntercero";
				}
			}
		}

		function validar3(formulario)
		{
			if(document.form2.cuenta.value!="" || document.form2.tercero.value!="")
			{
				document.form2.action="cont-marcarcuentasreciprocas.php";
				document.form2.ncuen.value=1;
				document.form2.defecto.value='1';
				document.form2.submit();
			}
		}

		function guardar()
		{
			if (confirm("Esta Seguro de Guardar"))
			{
				document.form2.oculto.value=2;
				document.form2.submit();
			}
		}
		</script>
	</head>
	<body>
		<IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
		<table>
			<tr><script>barra_imagenes("info");</script><?php cuadro_titulos();?></tr>
			<tr><?php menu_desplegable("info");?></tr>
			<tr>
  				<td colspan="3" class="cinta">
					<a class="mgbt"><img src="imagenes/add2.png"/></a>
					<a onClick="guardar()" class="mgbt"><img src="imagenes/guarda.png"  title="Guardar" /></a>
					<a class="mgbt"><img src="imagenes/buscad.png"/></a>
					<a onClick="mypop=window.open('plan-agenda.php','','');mypop.focus()" class="mgbt"><img src="imagenes/agenda1.png" title="Agenda" /></a>
					<a onClick="<?php echo paginasnuevas("info");?>" class="mgbt"><img src="imagenes/nv.png" title="Nueva ventana"></a>
                    <a onClick="mypop=window.open('<?php echo $url2; ?>','','');mypop.focus();" class="tooltip bottom mgbt"><img src="imagenes/duplicar_pantalla.png" title="Duplicar pesta&ntilde;a"></a>
					<a href='cont-gestioninformecgr.php' class="mgbt"><img src="imagenes/iratras.png" title="Atr&aacute;s"></a>
				</td>
			</tr>
		</table>
		<div id="bgventanamodalm" class="bgventanamodalm">
            <div id="ventanamodalm" class="ventanamodalm">
                <IFRAME src="" name="ventanam" marginWidth=0 marginHeight=0 frameBorder=0 id="ventanam" frameSpacing=0 style=" width:700px; height:130px; top:200; overflow:hidden;">
                </IFRAME>
            </div>
        </div>
		<form name="form2" action="cont-marcarcuentasreciprocas.php"  method="post" enctype="multipart/form-data" >
			<div class="loading" id="divcarga"><span>Cargando...</span></div>
 			<?php
 				$vigusu = vigencia_usuarios($_SESSION['cedulausu']);
 				if($_POST['reglas'] == 1){$chkchip = " checked";}
 				else {$chkchip = " ";}
				if(!$_POST['oculto'])
				{
					$_POST['oculto']=1;
					echo"<script>document.getElementById('divcarga').style.display='none';</script>";
				}
				if($_POST['oculto'] == 1)
				{
					echo"<script>document.getElementById('divcarga').style.display='none';</script>";
				}
   				$vact = $vigusu;
	  			//*** PASO 2
				$sqlr="select *from configbasica where estado='S'";
				$res = mysqli_query($linkbd_v7, $sqlr);
				while($row = mysqli_fetch_row($res))
	 			{
  					$_POST['nitentidad'] = $row[0];
  					$_POST['entidad'] = $row[1];
					$_POST['codent'] = $row[8];
				}
				if($_POST['ncuen'] == 1)
				{
					if(!empty($_POST['cuenta']))
					{
						$sqlr="SELECT distinct nombre from cuentasnicsp where  cuenta like'%".$_POST['cuenta']."%' order by cuenta";
						$res = mysqli_query($linkbd_v7, $sqlr);
						$row = mysqli_fetch_row($res);
						$_POST['ncuenta']=$row[0];
					}
					else
					{
						$_POST['ncuenta']="";
					}

					if(!empty($_POST['tercero']))
					{
						$sqlr="SELECT distinct nombre from codigoscun where  id_entidad like'%".$_POST['tercero']."%' order by id_entidad";
						$res = mysqli_query($linkbd_v7, $sqlr);
						$row = mysqli_fetch_row($res);
						$_POST['ntercero']=$row[0];
					}
					else
					{
						$_POST['ntercero']="";
					}

				}
				if(($_POST['oculto']!="2")&&($_POST['oculto']!="9"))
				{
					unset($_POST['dcuentas']);
					unset($_POST['dncuentas']);
					unset($_POST['dterceros']);
					unset($_POST['dvalores']);
					unset($_POST['dnterceros']);
					$sqlr="SELECT cuenta, tercero, valor FROM  cuentasreciprocas  WHERE estado='S' ORDER BY id desc";
					$res = mysqli_query($linkbd_v7, $sqlr);
					$cont = 0;
					while ($row = mysqli_fetch_assoc($res))
					{
						$_POST['dcuentas'][$cont]=$row["cuenta"];
						$_POST['dncuentas'][$cont]=existecuentanicsp($row["cuenta"]);
						$_POST['dterceros'][$cont]=$row["tercero"];
						$_POST['dvalores'][$cont]=$row["valor"];
						$_POST['dnterceros'][$cont]=buscacun($row["tercero"]);
						$cont=$cont+1;
					}
				}
	 		?>
            <input type="hidden" name="oculto" id="oculto" value="<?php echo $_POST['oculto'] ?>">
            <input type="hidden" name="periodo" id="periodo" value="<?php echo $_POST['periodo'] ?>">
			<table class="inicio" align="center">
				<tr>
					<td class="titulos" colspan="10">Marcar cuentas reciprocas</td>
					<td class="cerrar" style="width:7%;"><a href="info-principal.php">&nbsp;Cerrar</a></td>
				</tr>
				<tr>
					<td class="saludo1" style="width:8%">Cuenta: </td>
          			<td style="width:10%" valign="middle" >
						<input name="cuenta" id="cuenta" type="text"  value="<?php echo $_POST['cuenta']?>" onKeyUp="return tabular(event,this) " style="background-color:#40f3ff;width:98%;height:25px!important;" onBlur="validar3()" autocomplete="off" ondblclick="despliegamodal2('visible',1);">
						<input name="cuenta_" type="hidden" value="<?php echo $_POST['cuenta_']?>" >
						<input type="hidden" name="ncuen" value="<?php echo $_POST['ncuen']; ?>" />
					</td>
					<td>
						<input name="ncuenta" type="text" value="<?php echo $_POST['ncuenta']?>" style="width:98%;height:25px!important;" readonly>
					</td>
					<td class="saludo1" style="width:8%">Entidad: </td>
          			<td style="width:10%" valign="middle" >
						<input name="tercero" id="tercero" type="text"  value="<?php echo $_POST['tercero']?>" onKeyUp="return tabular(event,this) " style="background-color:#40f3ff;width:98%;height:25px!important;" onchange="validar3()" autocomplete="off" ondblclick="despliegamodal2('visible',2);">
					</td>

					<td>
						<input name="ntercero" id="ntercero" type="text" value="<?php echo $_POST['ntercero']?>" style="width:98%;height:25px!important;" readonly>
						<input name="defecto" type="hidden" value="<?php echo $_POST['defecto']?>">
					</td>
					<td class="saludo1" style="width:8%">Valor: </td>
          			<td style="width:10%" valign="middle" >
						<input name="valor" id="valor" type="text"  value="<?php echo $_POST['valor']?>" onKeyUp="return tabular(event,this) " style="width:98%;height:25px!important;"  autocomplete="off">
					</td>
					<td>
						<input type="button" name="agrega" value="  Agregar  " onClick="agregardetalle()" >
						<input type="hidden" value="0" name="agregadet">
					</td>
				</tr>
			</table>
			<div class="subpantallap" style="height:60%; width:99.6%; overflow-x:hidden;">
				<table class="inicio">
					<tr>
						<td class="titulos" colspan="8">Detalle Cuentas reciprocas agregadas</td>
					</tr>
					<tr>
						<td class="titulos2" style="width: 5%; text-align: center; text-transform: uppercase">Cuenta</td>
						<td class="titulos2" style="width: 40%; text-align: center; text-transform: uppercase">Nombre Cuenta</td>
						<td class="titulos2" style="width: 5%; text-align: center; text-transform: uppercase">Tercero</td>
						<td class="titulos2" style="text-align: center; text-transform: uppercase">Nombre Tercero</td>
						<td class="titulos2" style="width: 10%; text-align: center; text-transform: uppercase">Valor reciproca</td>
						<td class="titulos2" style="width: 3%; text-align: center; "><img src="imagenes/del.png" ><input type='hidden' name='elimina' id='elimina'></td>
					</tr>
					<?php

					if ($_POST['elimina']!='')
		 			{
						$posi=$_POST['elimina'];
						unset($_POST['dcuentas'][$posi]);
						unset($_POST['dncuentas'][$posi]);
						unset($_POST['dterceros'][$posi]);
						unset($_POST['dnterceros'][$posi]);
						unset($_POST['dvalores'][$posi]);
						$_POST['dcuentas']= array_values($_POST['dcuentas']);
						$_POST['dncuentas'] = array_values($_POST['dncuentas']);
						$_POST['dterceros'] = array_values($_POST['dterceros']);
						$_POST['dnterceros'] = array_values($_POST['dnterceros']);
						$_POST['dvalores'] = array_values($_POST['dvalores']);
						$_POST['elimina']='';
		 			}

					if ($_POST['agregadet']=='1')
		 			{
						$_POST['dcuentas'][]=$_POST['cuenta'];
						$_POST['dncuentas'][]=$_POST['ncuenta'];
						$_POST['dterceros'][]=$_POST['tercero'];
						$_POST['dnterceros'][]=$_POST['ntercero'];
						$_POST['dvalores'][]=$_POST['valor'];
						echo "
							<script>
								document.form2.cuenta.value='';
								document.form2.ncuenta.value='';
								document.form2.tercero.value='';
								document.form2.ntercero.value='';
								document.form2.valor.value='';
							</script>";
		 				$_POST['agregadet']=0;
		 			}
		 			$iter='saludo1a';
		 			$iter2='saludo2';
		 			for ($x=0;$x< count($_POST['dcuentas']);$x++)
		 			{
						echo "<tr class='$iter'>
								<td>
									<input name='dcuentas[]' id='dcuentas[]' value='".$_POST['dcuentas'][$x]."' type='text' style='width: 100%' readonly class='inpnovisibles'>
								</td>
								<td>
									<input name='dncuentas[]' id='dncuentas[]' value='".$_POST['dncuentas'][$x]."' type='text' style='width: 100%' readonly class='inpnovisibles'>
								</td>
								<td>
									<input name='dterceros[]' id='dterceros[]' value='".$_POST['dterceros'][$x]."' type='text' style='width: 100%' readonly class='inpnovisibles'>
								</td>
								<td>
									<input name='dnterceros[]' id='dnterceros[]' value='".$_POST['dnterceros'][$x]."' type='text' style='width: 100%' readonly class='inpnovisibles'>
								</td>
								<td>
									<input name='dvalores[]' id='dvalores[]' value='".$_POST['dvalores'][$x]."' type='text' style='text-align:right' class='inpnovisibles'>
								</td>
								<td style='text-align:center'>
									<a href='#' onclick='eliminar($x)'><img src='imagenes/del.png'></a>
								</td>
						</tr>";
						$aux=$iter;
						$iter=$iter2;
						$iter2=$aux;
		 			}
		 			?>
				</table>
			</div>
			<?php
				echo "<script>document.getElementById('divcarga').style.display='none';</script>";
			?>
		</form>
		<?php
		//********** GUARDAR EL COMPROBANTE ***********
		if($_POST['oculto']=='2')
		{
			//rutina de guardado cabecera
			$sql = "DELETE FROM cuentasreciprocas WHERE estado='S'";
			mysqli_query($linkbd_v7, $sql);
			//**** crear el detalle del concepto
			for($x=0;$x<count($_POST['dcuentas']);$x++)
			{
				$sqlr="INSERT INTO cuentasreciprocas (cuenta,tercero, valor, estado) VALUES ('".$_POST['dcuentas'][$x]."','".$_POST['dterceros'][$x]."','".$_POST['dvalores'][$x]."','S')";
				$res = mysqli_query($linkbd_v7, $sqlr);
			}
			echo "<table class='inicio'><tr><td class='saludo1a'><center>Se ha Almacenado con Exito Las Cuenas Reciprocas</center></td></tr></table>";
			echo "<script>document.form2.oculto.value='';</script>";
	   	}
	?>
		<div id="bgventanamodal2">
			<div id="ventanamodal2">
				<IFRAME  src="" name="buster" marginWidth=0 marginHeight=0 frameBorder=0 id="ventana2" frameSpacing=0 style="left:500px; width:900px; height:500px; top:200;">
				</IFRAME>
			</div>
		</div>
	</body>
