<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require "comun.inc";
	require "funciones.inc";
	require "funcionesSP.inc.php";
	session_start();
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8"); 
	cargarcodigopag($_GET['codpag'],$_SESSION['nivel']);
    date_default_timezone_set("America/Bogota");
	titlepag();
			
?>

<!DOCTYPE >

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es">
	<head>
		<meta http-equiv="Content-type" content="text/html; charset=utf8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=9"/>
		<title>:: IDEAL 10 - Servicios P&uacute;blicos</title>
		<link href="css/css2.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/cssSP.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/tabs.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/style.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
		<script type="text/javascript" src="css/calendario.js"></script>
        <script type="text/javascript" src="css/programas.js"></script>
		<script type="text/javascript" src="jquery-1.11.0.min.js"></script> 
        
		<script>
			function despliegamodalm(_valor,_tip,mensa,pregunta)
			{
				document.getElementById("bgventanamodalm").style.visibility=_valor;
                if(_valor == "hidden")
                {
                    document.getElementById('ventanam').src="";
                }
				else
				{
					switch(_tip)
					{
						case "1":	document.getElementById('ventanam').src="ventana-mensaje1.php?titulos="+mensa;break;
						case "2":	document.getElementById('ventanam').src="ventana-mensaje3.php?titulos="+mensa;break;
						case "3":	document.getElementById('ventanam').src="ventana-mensaje2.php?titulos="+mensa;break;
						case "4":	document.getElementById('ventanam').src="ventana-consulta1.php?titulos="+mensa+"&idresp="+pregunta;break;	
					}
				}
            }

			function despliegamodal(estado, modal, pos)
			{
				document.getElementById("bgventanamodal2").style.visibility=estado;

				if(estado == "hidden")
				{
					document.getElementById('ventana2').src="";
				}
				else
				{
					switch (modal) {
						case 'suscriptores':
							document.getElementById('ventana2').src = 'serv-modalSuscriptores.php';
						break;
					
						case 'estratos':
							document.getElementById('ventana2').src = 'serv-modalEstratos.php';
						break;

						case 'barrios':
							document.getElementById('ventana2').src = 'serv-modalBarrios.php';
						break;

						case 'rutas':
							document.getElementById('ventana2').src = 'serv-modalRutas.php';
						break;

						case 'zonas':
							document.getElementById('ventana2').src = 'serv-modalZonas.php';
						break;

						case 'lados':
							document.getElementById('ventana2').src = 'serv-modalLados.php';
						break;

						case 'estratos2':
							document.getElementById('ventana2').src = 'serv-modalestratos-servicios.php?pos='+pos;
						break;

						default:
							break;
					}
				}
			}

			function cambiocheck($tipo,$id)
			{
				var namecheck = $tipo + '[]';
				var estado = document.getElementsByName(namecheck).item($id).value;

				if (estado == 'S')
				{
					document.getElementsByName(namecheck).item($id).value = 'N';
				}
				else 
				{
					document.getElementsByName(namecheck).item($id).value = 'S';
				}
			}

			function cambioUsuario()
			{
				document.getElementById('cambio').value = "2";
				document.form2.submit();
			}

			function redirige(id)
			{
				location.href="serv-editaUsuario.php?id=" +id;
			}

			function funcionmensaje()
			{
				var x = window.location.search;
				location.href = "serv-editaUsuario.php"+x;
            }

			function guardar()
			{
                despliegamodalm('visible','4','Esta Seguro de Modificar','1');   
            }

			function respuestaconsulta(pregunta)
			{
				switch(pregunta)
				{
					case "1":	
						document.form2.oculto.value='2';
						document.form2.submit();
					break;
				}
				document.form2.submit();
            }

			function cambioConsumo(pos)
			{
				var ant = document.getElementsByName('lecturaAnt[]').item(pos).value;
				var act = document.getElementsByName('lecturaAct[]').item(pos).value;
				var consumo = act - ant;
				document.getElementsByName('consumoAct[]').item(pos).value = consumo;
			}
        </script>
                
	</head>
	<body>
		<IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
		<span id="todastablas2"></span>
		<table>
			<tr>
                <script>barra_imagenes("serv");</script><?php cuadro_titulos();?>
            </tr>

			<tr><?php menu_desplegable("serv");?></tr>
			<tr>
				<td colspan="3" class="cinta">
					<a href="serv-crearUsuario.php" class="mgbt"><img src="imagenes/add.png"/></a>
					<a onclick="guardar();" class="mgbt"><img src="imagenes/guarda.png" title="Guardar"/></a>
					<a href="serv-buscaUsuarios.php" class="mgbt"><img src="imagenes/busca.png" title="Buscar"/></a>
					<a onclick="mypop=window.open('plan-agenda.php','','');mypop.focus()" class="mgbt"><img src="imagenes/agenda1.png" title="Agenda" /></a>
					<a onclick="mypop=window.open('serv-principal.php','','');mypop.focus();" class="mgbt"><img src="imagenes/nv.png" title="Nueva ventana"></a>
					<a onclick="mypop=window.open('<?php echo $url2; ?>','','');mypop.focus();" class="mgbt"><img src="imagenes/duplicar_pantalla.png" title="Duplicar pesta&ntilde;a"></a>
					<a href="serv-buscaUsuarios.php" class="mgbt"><img src="imagenes/iratras.png" title="Atrás"></a>
                </td>
			</tr>
        </table>
        
		<div id="bgventanamodalm" class="bgventanamodalm">
			<div id="ventanamodalm" class="ventanamodalm">
				<IFRAME src="" name="ventanam" marginWidth=0 marginHeight=0 frameBorder=0 id="ventanam" frameSpacing=0 style=" width:700px; height:130px; top:200; overflow:hidden;"></IFRAME>
			</div>
		</div>

		<form name="form2" method="post" action="">
			<?php
				$_POST['tabgroup1'] = 1;

				switch($_POST['tabgroup1'])
				{
					case 1:	
						$check1='checked';
					break;

					case 2:	
						$check2='checked';
					break;
				}

				if(@ $_POST['oculto'] == '')
				{
					$_POST['id_cliente'] = $_GET['id'];

					$sqlCliente = "SELECT id, cod_catastral, id_tercero, fecha_creacion, id_estrato, id_barrio, id_ruta, codigo_ruta, id_zona, id_lado, cod_usuario FROM srvclientes WHERE id = '$_POST[id_cliente]'";
					$rowCliente = mysqli_fetch_row(mysqli_query($linkbd,$sqlCliente));

					$_POST['codUsuario'] = $rowCliente[10];
					$_POST['cod_usuario_act'] = $rowCliente[10];
					$_POST['codCastastral'] = $rowCliente[1];
					$_POST['documentoTercero'] = buscaDocumentoTerceroConId($rowCliente[2]);
					$_POST['nombreTercero'] = buscaNombreTerceroConId($rowCliente[2]);
					$_POST['id_tercero'] = $rowCliente[2];
					$_POST['id_tercero_act'] = $rowCliente[2];
					$_POST['fechaInscripcion'] = date('d/m/Y',strtotime($rowCliente[3]));
					$_POST['direccion'] = encuentraDireccionConIdCliente($rowCliente[0]);
					$_POST['direccion_act'] = encuentraDireccionConIdCliente($rowCliente[0]);
					$_POST['nombreEstrato'] = buscarNombreEstrato($rowCliente[4]);
					$_POST['id_estrato'] = $rowCliente[4];
					$_POST['id_estrato_act'] = $rowCliente[4];
					$_POST['nombreBarrio'] = buscaNombreBarrio($rowCliente[5]);
					$_POST['id_barrio'] = $rowCliente[5];
					$_POST['id_barrio_act'] = $rowCliente[5];
					$_POST['nombreRuta'] = buscaNombreRuta($rowCliente[6]);
					$_POST['id_ruta'] = $rowCliente[6];
					$_POST['id_ruta_act'] = $rowCliente[6];
					$_POST['codigoRuta'] = $rowCliente[7];
					$_POST['codigoRuta_act'] = $rowCliente[7];
					$_POST['nombreZona'] = buscaZona($rowCliente[8]);
					$_POST['id_zona'] = $rowCliente[8];
					$_POST['id_zona_act'] = $rowCliente[8];
					$_POST['nombreLado'] = buscaLado($rowCliente[9]);
					$_POST['id_lado'] = $rowCliente[9];
					$_POST['id_lado_act'] = $rowCliente[9];

					$corteActual = buscaCorteActivo();
					$corteAnterior = $corteActual - 1;
					
					$x = 0;
					$sqlServicios = "SELECT id, nombre FROM srvservicios WHERE estado = 'S' ORDER BY id ASC";
					$resServicios = mysqli_query($linkbd,$sqlServicios);

					while($rowServicios = mysqli_fetch_row($resServicios))
					{
						$sqlAsignacion = "SELECT cargo_fijo, consumo, id_medidor, tarifa_medidor, id_servicio, aforo, id_estrato FROM srvasignacion_servicio WHERE id_clientes = '$_POST[id_cliente]' AND id_servicio = '$rowServicios[0]'";
						$resAsignacion = mysqli_query($linkbd,$sqlAsignacion);
						$rowAsignacion = mysqli_fetch_row($resAsignacion);
						
						if($rowAsignacion[0] == 'S'){$_POST['cargoFijo'][$x] = $rowAsignacion[0];}
						else {$_POST['cargoFijo'][$x] = 'N';}

						if($rowAsignacion[1] == 'S'){$_POST['consumo'][$x] = $rowAsignacion[1];}
						else {$_POST['consumo'][$x] = 'N';}

						if($rowAsignacion[2] != ''){$_POST['medidor'][$x] = $rowAsignacion[2];}
						else {$_POST['medidor'][$x] = '';}

						if($rowAsignacion[3] == 'S'){$_POST['tarifa'][$x] = $rowAsignacion[3];}
						else {$_POST['tarifa'][$x] = 'N';}

						if($rowAsignacion[5] == 'S'){$_POST['promedio'][$x] = $rowAsignacion[5];}
						else {$_POST['promedio'][$x] = 'N';}

						if($rowAsignacion[6] != ''){$_POST['id_estrato2'][$x] = $rowAsignacion[6];}
						else {$_POST['id_estrato2'][$x] = '0';}

						$sqlEstratos = "SELECT nombre_clase FROM srv_clases WHERE id_clase = '".$_POST['id_estrato2'][$x]."' ";
						$resEstratos = mysqli_query($linkbd,$sqlEstratos);
						$rowEstratos = mysqli_fetch_row($resEstratos);

						if($rowEstratos[0] != ''){$_POST['estrato2'][$x] = $rowEstratos[0];}
						else {$_POST['estrato2'][$x] = 'No Aplica';}
					
						echo"
							<input type='hidden' name='cargoFijo[]' value='".$_POST['cargoFijo'][$x]."'>
							<input type='hidden' name='consumo[]' value='".$_POST['consumo'][$x]."'>
							<input type='hidden' name='tarifa[]' value='".$_POST['tarifa'][$x]."'>
							<input type='hidden' name='aforo[]' value='".$_POST['aforo'][$x]."'>
						";

						if($_POST['cargoFijo'][$x] == 'S'){$_POST["cfcheck$x"] = 'checked';}
						else {$_POST["cfcheck$x"] = '';}

						if($_POST['consumo'][$x] == 'S'){$_POST["cscheck$x"] = 'checked';}
						else {$_POST["cscheck$x"] = '';}

						if($_POST['tarifa'][$x] == 'S'){$_POST["tfcheck$x"] = 'checked';}
						else {$_POST["tfcheck$x"] = '';}

						if($_POST['aforo'][$x] == 'S'){$_POST["afcheck$x"] = 'checked';}
						else {$_POST["afcheck$x"] = '';}

						$x++;
					} 


					$x = 0;
					$sqlServicios = "SELECT id, nombre FROM srvservicios WHERE estado = 'S' AND unidad_medida <> 'GASTOS DE OPERACION' ORDER BY id ASC";
					$resServicios = mysqli_query($linkbd,$sqlServicios);

					while($rowServicios = mysqli_fetch_row($resServicios))
					{
						$sqlCorteAct = "SELECT lectura_medidor, consumo FROM srvlectura WHERE corte = $corteActual AND id_cliente = '$_POST[id_cliente]' AND id_servicio = '$rowServicios[0]'";
						$resCorteAct = mysqli_query($linkbd,$sqlCorteAct);
						$rowCorteAct = mysqli_fetch_row($resCorteAct);

						if($rowCorteAct[0] != ''){$_POST['lecturaAct'][$x] = $rowCorteAct[0];}
						else {$_POST['lecturaAct'][$x] = 0;}

						if($rowCorteAct[1] != ''){$_POST['consumoAct'][$x] = $rowCorteAct[1];}
						else {$_POST['consumoAct'][$x] = 0;}


						$sqlCorteAnt = "SELECT lectura_medidor, consumo FROM srvlectura WHERE corte = $corteAnterior AND id_cliente = '$_POST[id_cliente]' AND id_servicio = '$rowServicios[0]'";
						$resCorteAnt = mysqli_query($linkbd,$sqlCorteAnt);
						$rowCorteAnt = mysqli_fetch_row($resCorteAnt);

						if($rowCorteAnt[0] != ''){$_POST['lecturaAnt'][$x] = $rowCorteAnt[0];}
						else {$_POST['lecturaAnt'][$x] = 0;}

						if($rowCorteAnt[1] != ''){$_POST['consumoAnt'][$x] = $rowCorteAnt[1];}
						else {$_POST['consumoAnt'][$x] = 0;}

						echo"
							<input type='hidden' name='consumoAnt[]' value='".$_POST['consumoAnt'][$x]."'>
						";

						$x++;
					}

					$x = 0;
				}
				
            ?>

			<div>
				<table class="inicio">
					<tr>
						<td class="titulos" colspan="7">Buscar Usuarios</td>
						<td class="cerrar" style="width:7%" onClick="location.href='serv-principal.php'">Cerrar</td>
					</tr>

					<tr>
						<td class="tamano01" style='width:4cm;'>C&oacute;digo de Usuario:</td>
						<td>
							<input type="search" name="busqueda_cliente" id="busqueda_cliente" value="<?php echo @$_POST['busqueda_cliente'];?>" style='width:100%;' onkeydown = "if (event.keyCode == 13){cambioUsuario()}";/>
						</td>

						<td style="padding-bottom:0px">
							<em class="botonflecha" id="filtro" onclick="cambioUsuario();">Buscar</em>
						</td>
					</tr> 
				</table>

				<div class="subpantalla" style="height:67%; width:49%; overflow:hidden; float:left;">
					<table class="inicio">
						<tr>
							<td class="titulos" colspan="4">.: Editar Usuario</td>
						</tr>

						<tr>
							<td class="tamano01" style="width: 10%;">Código Usuario:</td> 
							<td>
								<input type="text" name='codUsuario' id='codUsuario' value="<?php echo @$_POST['codUsuario'];?>" style="text-align:center;"/>
								<input type="hidden" name="cod_usuario_act" id="cod_usuario_act" value="<?php echo $_POST['cod_usuario_act'] ?>">
								<input type="hidden" name="id_cliente" id="id_cliente" value="<?php echo $_POST['id_cliente'] ?>">
							</td>

							<td class="tamano01" style="width: 10%;">Código Catastral:</td>
							<td style="width: 25%;">
								<input type="text" name="codCastastral" id="codCastastral" value="<?php echo @ $_POST['codCastastral'];?>" style="text-align: center;"/>
							</td>
						</tr>

						<tr>
							<td class="tamano01">Suscriptor:</td>

							<td style="width:15%;">
								<input type="text" name="documentoTercero" id="documentoTercero" ondblclick="despliegamodal('visible', 'suscriptores')" value="<?php echo @$_POST['documentoTercero']?>" style="text-align:center" class="colordobleclik" readonly/>
								<input type="hidden" name="id_tercero" id="id_tercero" value="<?php echo $_POST['id_tercero'] ?>">
								<input type="hidden" name="id_tercero_act" id="id_tercero_act" value="<?php echo $_POST['id_tercero_act'] ?>">
							</td>

							<td colspan="2">
								<?php
									if($_POST['nombreTercero'] == ""){$editer = " class='icobut1' src='imagenes/usereditd.png'";}
									else{$editer = " class='icobut' src='imagenes/useredit.png' onClick=\"mypop=window.open('serv-tercerosEditar.php?id=".$_POST['id_tercero']."','','');mypop.focus();\"";}
								?>

								<input type="text" name="nombreTercero" id="nombreTercero" value="<?php echo @$_POST['nombreTercero']?>" style="width:76%;" readonly />
								<img <?php echo $editer; ?> title="Editar Suscriptor" />

								
							</td>
						</tr>

						<tr>
							<td class="tamano01" style="width:5%;">Fecha Inscripción:</td>
							<td style="width:15%;">
								<input type="text" name="fechaInscripcion" id="fechaInscripcion" value="<?php echo @ $_POST['fechaInscripcion']?>" style="text-align:center" readonly />
							</td>

							<td class="tamano01">Dirección:</td>
							<td>
								<input type="text" name="direccion" id="direccion" value="<?php echo @$_POST['direccion']?>" />	
								<input type="hidden" name="direccion_act" id="direccion_act" value="<?php echo $_POST['direccion_act'] ?>">
							</td>
						</tr>

						<tr>
							<td class="tamano01">Estrato:</td>
							<td>
								<input type="text" name='nombreEstrato' id='nombreEstrato' value="<?php echo $_POST['nombreEstrato']?>" ondblclick="despliegamodal('visible', 'estratos')" style="text-align:center;" class="colordobleclik" readonly/>
								<input type="hidden" name='id_estrato' id='id_estrato' value="<?php echo $_POST['id_estrato']?>"/>
								<input type="hidden" name="id_estrato_act" id="id_estrato_act" value="<?php echo $_POST['id_estrato_act'] ?>"/>
							</td>

							<td class="tamano01" style="width:3cm;">Barrio:</td>
							<td>
								<input type="text" name="nombreBarrio" id="nombreBarrio" value="<?php echo $_POST['nombreBarrio']?>" ondblclick="despliegamodal('visible', 'barrios')" style="text-align:center;" class="colordobleclik" readonly/>
								<input type="hidden" name="id_barrio" id="id_barrio" value="<?php echo @ $_POST['id_barrio']?>"/>
								<input type="hidden" name="id_barrio_act" id="id_barrio_act" value="<?php echo @ $_POST['id_barrio_act']?>"/>
							</td>
						</tr>

						<tr>
							<td class="tamano01">Ruta:</td>
							<td>
								<input type="text" name="nombreRuta" id="nombreRuta" onKeyUp="return tabular(event,this)" value="<?php echo $_POST['nombreRuta']?>" ondblclick="despliegamodal('visible', 'rutas')" style="text-align:center;" class="colordobleclik" readonly/>
								<input type="hidden" name="id_ruta" id="id_ruta" value="<?php echo $_POST['id_ruta']?>"/>
								<input type="hidden" name="id_ruta_act" id="id_ruta_act" value="<?php echo $_POST['id_ruta_act'] ?>"/>
							</td>

							<td class="tamano01">Código Ruta:</td>
							<td>
								<input type="text" name="codigoRuta" id="codigoRuta" value="<?php echo @$_POST['codigoRuta'] ?>" style="text-align:center;"/>
								<input type="hidden" name="codigoRuta_act" id="codigoRuta_act" value="<?php echo $_POST['codigoRuta_act'] ?>"/>
							</td>
						</tr>

						<tr>
							<td class="tamano01">Zona:</td>
							<td>
								<input type="text" name="nombreZona" id="nombreZona" value="<?php echo $_POST['nombreZona']?>" ondblclick="despliegamodal('visible', 'zonas')" style="text-align:center;" class="colordobleclik" readonly>
								<input type="hidden" name="id_zona" id="id_zona" value="<?php echo $_POST['id_zona']?>">
								<input type="hidden" name="id_zona_act" id="id_zona_act" value="<?php echo $_POST['id_zona_act'] ?>">
							</td>

							<td class="tamano01">Lado:</td>
							<td>
								<input type="text" name="nombreLado" id="nombreLado" value="<?php echo $_POST['nombreLado']?>" ondblclick="despliegamodal('visible', 'lados')" style="text-align:center;" class="colordobleclik" readonly>
								<input type="hidden" name="id_lado" id="id_lado" value="<?php echo $_POST['id_lado']?>">
								<input type="hidden" name="id_lado_act" id="id_lado_act" value="<?php echo $_POST['id_lado_act'] ?>">
							</td>
						</tr>
					</table>
				</div>	

				<div class="subpantalla" style="height:35%; width:50%; overflow:hidden; float:left;">
					<div class="tabscontra" style="height: 100%; width:100%; float:left;">
						<div class="tab">
							<input type="radio" id="tab-1" name="tabgroup1" value="1" <?php echo $check1;?>/>
							<label for="tab-1">Asignación Servicios</label>

							<div class="content" style="overflow:hidden">
								<table class="inicio" style="width:99%;">
									<tr>
										<td class="titulos" colspan="9">.: Servicios Usuario</td>
									</tr>

									<tr class="titulos2" style='text-align:center;'>
										<td style="width: 5%;">Código Servicio</td>
										<td style="width: 20%;">Nombre Servicio</td>
										<td style="width: 10%">Clase de uso</td>
										<td style="width: 10%">Cargo Fijo</td>
										<td style="width: 10%">Consumo</td>
										<td style="width: 10%">Tarifa Medidor</td>
										<td style="width: 10%">Aforo</td>
										<td style="width: 10%">Numero Medidor</td>
										<td style="width: 10%">Promedio por Aforo</td>
									</tr>

									<?php
										$sqlServicios = "SELECT id, nombre FROM srvservicios WHERE estado = 'S' ORDER BY id ASC";
										$resServicios = mysqli_query($linkbd,$sqlServicios);
					
										$iter = 'saludo1a';
										$iter2 = 'saludo2';
										$filas = 1;
										$x = 0;
					
										while($rowServicios = mysqli_fetch_row($resServicios))
										{
									?>
											<tr class='<?php echo $iter ?>' style='text-transform:uppercase; text-align:center;'>
												<td><?php echo $rowServicios[0] ?></td>
												<td><?php echo $rowServicios[1] ?></td>
												<td>
													<input type='text' name='estrato2[]' value="<?php echo $_POST['estrato2'][$x] ?>" style='text-align:center; width: 100%' class='colordobleclik' ondblclick="despliegamodal('visible', 'estratos2', <?php echo $x ?>)" readonly>
													<input type='hidden' name='id_estrato2[]' value="<?php echo $_POST['id_estrato2'][$x] ?>" style='text-align:center; width: 100%' class='colordobleclik'>
												</td>
									<?php
											echo "
												<td>
													<div class='rcheck2' style='align-items: center;  display: flex; justify-content: center; !important'>
														<input type='checkbox' id='cfcheck$x' name='cfcheck$x' value='".$_POST["cfcheck$x"]."' ".$_POST["cfcheck$x"]." onClick=\"cambiocheck('cargoFijo','$x');\">
														<label for='cfcheck$x'></label>
													<div>
												</td>

												<td>
													<div class='rcheck2' style='align-items: center;  display: flex; justify-content: center; !important'>
														<input type='checkbox' id='cscheck$x' name='cscheck$x' value='".$_POST["cscheck$x"]."' ".$_POST["cscheck$x"]." onClick=\"cambiocheck('consumo','$x');\">
														<label for='cscheck$x'></label>
													<div>
												</td>

												<td>
													<div class='rcheck2' style='align-items: center;  display: flex; justify-content: center; !important'>
														<input type='checkbox' id='tfcheck$x' name='tfcheck$x' value='".$_POST["tfcheck$x"]."' ".$_POST["tfcheck$x"]." onClick=\"cambiocheck('tarifa','$x');\">
														<label for='tfcheck$x'></label>
													<div>
												</td>

												<td>
													<div class='rcheck2' style='align-items: center;  display: flex; justify-content: center; !important'>
														<input type='checkbox' id='afcheck$x' name='afcheck$x' value='".$_POST["afcheck$x"]."' ".$_POST["afcheck$x"]." onClick=\"cambiocheck('aforo','$x');\">
														<label for='afcheck$x'></label>
													<div>
												</td>

												<td>
													<input type='text' name='medidor[]' value='".$_POST['medidor'][$x]."' style='text-align:center; width: 100%'>
												</td>

												<td>
													<input type='text' name='consumoPromedio[]' value='".$_POST['consumoPromedio'][$x]."' style='text-align:center; width: 100%'>
												</td>
											
											</tr>
											";

											$aux = $iter;
											$iter = $iter2;
											$iter2 = $aux;	
											$x++;
										}
									?>
								</table>
							</div>
						</div>

						<div class="tab">
							<input type="radio" id="tab-2" name="tabgroup1" value="2" <?php echo $check2;?>/>
							<label for="tab-2">Historico Lecturas</label>

							<div class="content" style="overflow:hidden">
								<table class="inicio" style="width:99%;">
									<tr>
										<td class="titulos" colspan="6">.: Lecturas Usuario</td>
									</tr>

									<tr class="titulos2" style='text-align:center;'>
										<td style="width: 10%;">Código Servicio</td>
										<td style="width: 30%;">Nombre Servicio</td>
										<td style="width: 10%">Lectura Anterior</td>
										<td style="width: 10%">Lectura Actual</td>
										<td style="width: 10%">Consumo</td>
									</tr>

									<?php
										$sqlServicios = "SELECT id, nombre FROM srvservicios WHERE estado = 'S' AND unidad_medida <> 'GASTOS DE OPERACION' ORDER BY id ASC";
										$resServicios = mysqli_query($linkbd,$sqlServicios);
					
										$iter = 'saludo1a';
										$iter2 = 'saludo2';
										$filas = 1;
										$x = 0;
					
										while($rowServicios = mysqli_fetch_row($resServicios))
										{
									?>
											<tr class='<?php echo $iter ?>' style='text-transform:uppercase; text-align:center;'>
												<td><?php echo $rowServicios[0] ?></td>
												<td><?php echo $rowServicios[1] ?></td>
									<?php
											echo "
												<td>
													<input type='text' name='lecturaAnt[]' value='".$_POST['lecturaAnt'][$x]."' style='text-align:center;' readonly>
												</td>

												<td>
													<input type='text' name='lecturaAct[]' value='".$_POST['lecturaAct'][$x]."' style='text-align:center;' onblur='cambioConsumo($x);' readonly>
												</td>

												<td>
													<input type='text' name='consumoAct[]' value='".$_POST['consumoAct'][$x]."' style='text-align:center;' readonly>
												</td>
											</tr>";


											$aux = $iter;
											$iter = $iter2;
											$iter2 = $aux;	
											$x++;
										}
									?>
								</table>
							</div>
						</div>
					</div>
				</div>

				
			</div>	
			<?php
				if($_POST['cambio'] == '2')
				{
					$codigo = "'$_POST[busqueda_cliente]'";
					$validaCodigo = validaCodigoUsuario($codigo);		

					if($validaCodigo == 'S')
					{
						$h = encontrarIdClienteCodUsuario($_POST['busqueda_cliente']);
						echo "<script>redirige($h);</script>";
					}
					else
					{
						echo"<script>despliegamodalm('visible','2','No se encontro a este usuario');</script>";
					}
				}

				if($_POST['oculto'] == '2')
				{
					$corteActual = buscaCorteActivo();
					$corteAnterior = $corteActual - 1;
					$fecha = date('d-m-Y');

					if($_POST['codUsuario'] === $_POST['cod_usuario_act'])
					{
						$sqlCliente = "UPDATE srvclientes SET cod_catastral = '$_POST[codCastastral]', id_tercero = '$_POST[id_tercero]', id_estrato = '$_POST[id_estrato]', id_barrio = '$_POST[id_barrio]', id_ruta = '$_POST[id_ruta]', codigo_ruta = '$_POST[codigoRuta]', id_zona = '$_POST[id_zona]', id_lado = '$_POST[id_lado]' WHERE id = '$_POST[id_cliente]'";
						mysqli_query($linkbd,$sqlCliente);

						$sqlDireccion = "UPDATE srvdireccion_cliente SET direccion = '$_POST[direccion]' WHERE id_cliente = '$_POST[id_cliente]'";
						mysqli_query($linkbd,$sqlDireccion);

						$sqlEliminasServicios = "DELETE FROM srvasignacion_servicio WHERE id_clientes = '$_POST[id_cliente]'";
						mysqli_query($linkbd,$sqlEliminasServicios);

						$sqlEliminaLecturaAct = "DELETE FROM srvlectura WHERE corte = $corteActual AND id_cliente = '$_POST[id_cliente]'";
						//mysqli_query($linkbd,$sqlEliminaLecturaAct);

						$sqlEliminaLecturaAnt = "DELETE FROM srvlectura WHERE corte = $corteAnterior AND id_cliente = '$_POST[id_cliente]'";
						//mysqli_query($linkbd,$sqlEliminaLecturaAnt);

						$x = 0;
						$sqlServicios = "SELECT id, nombre FROM srvservicios WHERE estado = 'S' ORDER BY id ASC";
						$resServicios = mysqli_query($linkbd,$sqlServicios);
						while ($rowServicios = mysqli_fetch_row($resServicios)) {
							$servicio = $rowServicios[0];	
							$sqlAsigna = "INSERT INTO srvasignacion_servicio (id_clientes, id_servicio, id_medidor, tarifa_medidor, cargo_fijo, consumo, estado, id_estrato) VALUES ('$_POST[id_cliente]', '$servicio', '".$_POST['medidor'][$x]."', '".$_POST['tarifa'][$x]."', '".$_POST['cargoFijo'][$x]."', '".$_POST['consumo'][$x]."', 'S', '".$_POST['id_estrato2'][$x]."')";
							mysqli_query($linkbd,$sqlAsigna);
							$x++;
						}


						$sql2 = "SELECT MIN(id) FROM srvservicios WHERE unidad_medida <> 'GASTOS DE OPERACION' ORDER BY id ASC";
						$res2 = mysqli_query($linkbd,$sql2);
						$row2 = mysqli_fetch_row($res2);

						$servicio2 = $row2[0];

						for ($x=0; $x < count($_POST['lecturaAct']); $x++) 
						{ 
							$sqlLectura1 = "INSERT INTO srvlectura (corte, id_cliente, id_servicio, lectura_medidor, consumo) VALUES ($corteActual, '$_POST[id_cliente]', $servicio2, '".$_POST['lecturaAct'][$x]."', '".$_POST['consumoAct'][$x]."')";
							//mysqli_query($linkbd,$sqlLectura1);

							$servicio2++;
						}

						$sql3 = "SELECT MIN(id) FROM srvservicios WHERE unidad_medida <> 'GASTOS DE OPERACION' ORDER BY id ASC";
						$res3 = mysqli_query($linkbd,$sql3);
						$row3 = mysqli_fetch_row($res3);

						$servicio3 = $row3[0];

						for ($x=0; $x < count($_POST['lecturaAnt']); $x++) 
						{ 
							$sqlLectura2 = "INSERT INTO srvlectura (corte, id_cliente, id_servicio, lectura_medidor, consumo) VALUES ($corteAnterior, '$_POST[id_cliente]', $servicio3, '".$_POST['lecturaAnt'][$x]."', '".$_POST['consumoAnt'][$x]."')";
							//mysqli_query($linkbd,$sqlLectura2);

							$servicio3++;
						}
						
						if(mysqli_query($linkbd,$sqlCliente)) 
						{
							echo "<script>despliegamodalm('visible','1','Se ha Editado con Exito');</script>";
						}
						else 
						{
							echo"<script>despliegamodalm('visible','2','No se pudo ejecutar la peticion');</script>";
						}
					}
					else
					{
						$query = "SELECT cod_usuario FROM srvclientes WHERE cod_usuario = '$_POST[codUsuario]'";
						$resp = mysqli_query($linkbd,$query);
						$row = mysqli_num_rows($resp);

						if($row == 0)
						{
							$sqlCliente = "UPDATE srvclientes SET cod_catastral = '$_POST[codCastastral]', id_tercero = '$_POST[id_tercero]', id_estrato = '$_POST[id_estrato]', id_barrio = '$_POST[id_barrio]', id_ruta = '$_POST[id_ruta]', codigo_ruta = '$_POST[codigoRuta]', id_zona = '$_POST[id_zona]', id_lado = '$_POST[id_lado]', cod_usuario = '$_POST[codUsuario]' WHERE id = '$_POST[id_cliente]'";
							mysqli_query($linkbd,$sqlCliente);

							$sqlDireccion = "UPDATE srvdireccion_cliente SET direccion = '$_POST[direccion]' WHERE id_cliente = '$_POST[id_cliente]'";
							mysqli_query($linkbd,$sqlDireccion);

							$sqlEliminasServicios = "DELETE FROM srvasignacion_servicio WHERE id_clientes = '$_POST[id_cliente]'";
							mysqli_query($linkbd,$sqlEliminasServicios);

							$sqlEliminaLecturaAct = "DELETE FROM srvlectura WHERE corte = $corteActual AND id_cliente = '$_POST[id_cliente]'";
							//mysqli_query($linkbd,$sqlEliminaLecturaAct);

							$sqlEliminaLecturaAnt = "DELETE FROM srvlectura WHERE corte = $corteAnterior AND id_cliente = '$_POST[id_cliente]'";
							//mysqli_query($linkbd,$sqlEliminaLecturaAct);

							$x = 0;
							$sqlServicios = "SELECT id, nombre FROM srvservicios WHERE estado = 'S' ORDER BY id ASC";
							$resServicios = mysqli_query($linkbd,$sqlServicios);
							while ($rowServicios = mysqli_fetch_row($resServicios)) {
								$servicio = $rowServicios[0];	
								$sqlAsigna = "INSERT INTO srvasignacion_servicio (id_clientes, id_servicio, id_medidor, tarifa_medidor, cargo_fijo, consumo, estado, id_estrato) VALUES ('$_POST[id_cliente]', '$servicio', '".$_POST['medidor'][$x]."', '".$_POST['tarifa'][$x]."', '".$_POST['cargoFijo'][$x]."', '".$_POST['consumo'][$x]."', 'S', '".$_POST['id_estrato2'][$x]."')";
								mysqli_query($linkbd,$sqlAsigna);
								$x++;
							}

							$sql2 = "SELECT MIN(id) FROM srvservicios WHERE unidad_medida <> 'GASTOS DE OPERACION' ORDER BY id ASC";
							$res2 = mysqli_query($linkbd,$sql2);
							$row2 = mysqli_fetch_row($res2);

							$servicio2 = $row2[0];

							for ($x=0; $x < count($_POST['lecturaAct']); $x++) 
							{ 
								$sqlLectura1 = "INSERT INTO srvlectura (corte, id_cliente, id_servicio, lectura_medidor, consumo) VALUES ($corteActual, $_POST[id_cliente], $servicio2, '".$_POST['lecturaAct'][$x]."', '".$_POST['consumoAct'][$x]."')";
								//mysqli_query($linkbd,$sqlLectura1);

								$servicio2++;
							}

							$sql3 = "SELECT MIN(id) FROM srvservicios WHERE unidad_medida <> 'GASTOS DE OPERACION' ORDER BY id ASC";
							$res3 = mysqli_query($linkbd,$sql3);
							$row3 = mysqli_fetch_row($res3);

							$servicio3 = $row3[0];

							for ($x=0; $x < count($_POST['lecturaAnt']); $x++) 
							{ 
								$sqlLectura2 = "INSERT INTO srvlectura (corte, id_cliente, id_servicio, lectura_medidor, consumo) VALUES ($corteAnterior, $_POST[id_cliente], $servicio3, '".$_POST['lecturaAnt'][$x]."', '".$_POST['consumoAnt'][$x]."')";
								//mysqli_query($linkbd,$sqlLectura2);

								$servicio3++;
							}

							$usuario = $_SESSION["usuario"];
							$cedula = $_SESSION["cedulausu"];
							$ip = getRealIP();
							$hoy = date("Y-m-d H:i:s");
					
							$sqlAuditoria = "INSERT INTO srv_auditoria (id_srv_funciones, accion, cedula, usuario, ip, fecha_hora, consecutivo) VALUES (1, 'Actualizar', '$cedula', '$usuario', '$ip', '$hoy', '$_POST[id_cliente]')";
							mysqli_query($linkbd, $sqlAuditoria);
							
							if(mysqli_query($linkbd,$sqlCliente)) 
							{
								echo "<script>despliegamodalm('visible','1','Se ha Editado con Exito');</script>";
							}
							else 
							{
								echo"<script>despliegamodalm('visible','2','No se pudo ejecutar la peticion');</script>";
							}
						}
						else
						{
							echo "<script>despliegamodalm('visible', '2', 'Codigo de usuario ya esta en uso.');</script>";
						}
					}
				}
			?>

			<input type="hidden" name="cambio" id="cambio" value="1"/>
			<input type="hidden" name="oculto" id="oculto" value="1"/>
			
			<div id="bgventanamodal2">
				<div id="ventanamodal2">
					<IFRAME  src="" name="buster" marginWidth=0 marginHeight=0 frameBorder=0 id="ventana2" frameSpacing=0 style="left:500px; width:900px; height:500px; top:200;"></IFRAME>
				</div>
			</div>
			
			
        </form>                          
    </body>   
</html>
