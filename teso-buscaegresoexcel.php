<?php  
require_once 'PHPExcel/Classes/PHPExcel.php';// Incluir la libreria PHPExcel
include 'PHPExcel/Classes/PHPExcel/IOFactory.php';// PHPExcel_IOFactory
require "comun.inc";
require "funciones.inc";
session_start();
$objPHPExcel = new PHPExcel();// Crea un nuevo objeto PHPExcel

$linkbd = conectar_v7();
$linkbd -> set_charset("utf8");


//----Propiedades----
$objPHPExcel->getProperties()
        ->setCreator("IDEAL")
        ->setLastModifiedBy("IDEAL")
        ->setTitle("Exportar Excel con PHP")
        ->setSubject("Documento de prueba")
        ->setDescription("Documento generado con PHPExcel")
        ->setKeywords("usuarios phpexcel")
        ->setCategory("reportes");

//----Cuerpo de Documento----
$objPHPExcel->getActiveSheet()->mergeCells('A1:H1');
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A1', 'Cuentas por pagar');

$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A2', 'Item')
            ->setCellValue('B2', 'Vigencia')
            ->setCellValue('C2', 'N RP')
            ->setCellValue('D2', 'Tercero')
            ->setCellValue('E2', 'Detalle')
            ->setCellValue('F2', 'Valor')
            ->setCellValue('G2', 'Fecha')
            ->setCellValue('H2', 'Estado');

            $fech1=explode("/",$_POST['fecha']);
            $fech2=explode("/",$_POST['fecha2']);
            $f1=$fech1[2]."-".$fech1[1]."-".$fech1[0];
            $f2=$fech2[2]."-".$fech2[1]."-".$fech2[0];
            if ($_POST['nombre']!="")
            $crit1="and concat_ws(' ', id_orden, conceptorden) LIKE '%$_POST[nombre]%'";
            if($_POST['fecha']!='')
                $crit=" and tesoordenpago.fecha BETWEEN '$f1' AND '$f2'";
            $sqlr="select * from tesoordenpago where tesoordenpago.id_orden>-1 ".$crit." ".$crit1." order by tesoordenpago.id_orden desc";
            $resp = mysqli_query($linkbd, $sqlr);
$i=3;
while ($row =mysqli_fetch_row($resp))
{
    if($row[13]=='S')
        $est="ACTIVO";
    elseif($row[13]=='N')
        $est="ANULADO";
    elseif($row[13]=='R')
        $est="REVERSADO";
    elseif($row[13]=='P')
        $est="PAGO";

    $ntr=buscatercero($row[6]);
    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0,$i,$row[0]);
    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1,$i,$row[3]);
    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2,$i,$row[4]);
    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3,$i,$row[6] . " - " . $ntr);
    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4,$i,iconv($_SESSION["VERCARPDFINI"], $_SESSION["VERCARPDFFIN"]."//TRANSLIT",$row[7]));
    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(5,$i,$row[10]);
    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(6,$i,$row[2]);
    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(7,$i,$est);
        /*echo "<input name='iteme[]'  type='hidden'  value='$row[0]' >
                <input name='vigenciae[]'  type='hidden'  value='$row[3]' >
                <input name='nrpe[]'  type='hidden'  value='$row[4]' >
                <input name='detalee[]'  type='hidden'  value='$row[7]' >
                <input name='evalor[]'  type='hidden'  value='$row[10]' >
                <input name='efecha[]'  type='hidden'  value='$row[2]' >
                <input name='eestado[]'  type='hidden'  value='$row[13]' >";
        }*/
    $i+=1;
}

/*for( $i=0;$i<count($_POST[iteme]);$i++){
    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0,$i+3,$_POST[iteme][$i]);
    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1,$i+3,$_POST[vigenciae][$i]);
    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2,$i+3,$_POST[nrpe][$i]);
    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3,$i+3,$_POST[detalee][$i]);
    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4,$i+3,$_POST[evalor][$i]);
    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(5,$i+3,$_POST[efecha][$i]);
    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(6,$i+3,$_POST[eestado][$i]);

}*/
//----Propiedades de la hoja
$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true); 
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true); 
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true); 
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(50);
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(50);
$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true); 
$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);  
$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);  
$objPHPExcel->getActiveSheet()->setTitle('Teso-Predial');
$objPHPExcel->setActiveSheetIndex(0);

//----Guardar documento----

header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="Teso-Cuentas-por-Pagar.xlsx"');
header('Cache-Control: max-age=0');
header ('Expires: Mon, 15 Dic 2015 09:31:00 GMT'); // Date in the past
header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header ('Pragma: public'); // HTTP/1.0
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$objWriter->save('php://output');
exit;

?>