<!--V 1.0 24/02/2015-->
<?php
	require"comun.inc";
	require"funciones.inc";
	require "validaciones.inc";
	require 'funcionessp.inc';
	session_start();
	$linkbd=conectar_bd();	
	cargarcodigopag($_GET[codpag],$_SESSION["nivel"]);
	header("Cache-control: private"); // Arregla IE 6
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es">
	<head>
	 	<meta http-equiv="Content-Type" content="text/html" charset="iso-8859-1"/>
        <meta http-equiv="X-UA-Compatible" content="IE=9"/>
		<title>:: Spid - Servicios P&uacute;blicos</title>
        <link href="css/css2.css" rel="stylesheet" type="text/css" />
		<link href="css/css3.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="css/programas.js"></script>
		<script>
		function retornobuscar()
		{	
			location.href="serv-facturasbuscar.php?cusu="+document.form2.codusu.value;
		}
		function fimprimir ()
			{
				document.form2.action="serv-imprimirfacturapdf.php";
				document.form2.target="_BLANK";
				document.form2.submit(); 
				document.form2.action="";
				document.form2.target="";
			}
		</script>
        <?php titlepag();?>
	</head>
	<body>
		<IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
		<span id="todastablas2"></span>
		<table>
            <tr><script>barra_imagenes("serv");</script><?php cuadro_titulos();?></tr>	 
            <tr><?php menu_desplegable("serv");?></tr>
			<tr>
  				<td colspan="3" class="cinta"><img src="imagenes/add2.png" class="mgbt1"/><img src="imagenes/guardad.png" class="mgbt1"/><img src="imagenes/busca.png" title="Buscar" class="mgbt" onClick="location.href='serv-facturasbuscar.php'"/><img src="imagenes/nv.png" title="Nueva Ventana" onClick="mypop=window.open('serv-principal.php','','');mypop.focus();" class="mgbt"/><img src="imagenes/print.png" title="Imprimir" onClick="fimprimir()" class="mgbt"/><img src='imagenes/iratras.png' title="Men&uacute; Nomina" onClick="retornobuscar()" class='mgbt'></td>
			</tr>
  		</table>
		<div id="bgventanamodalm" class="bgventanamodalm">
            <div id="ventanamodalm" class="ventanamodalm">
                <IFRAME src="" name="ventanam" marginWidth=0 marginHeight=0 frameBorder=0 id="ventanam" frameSpacing=0 style=" width:700px; height:130px; top:200; overflow:hidden;"> 
                </IFRAME>
            </div>
        </div>
        <form name="form2" method="post" action="">
			<?php
                if($_POST[oculto]=="")
                {
					$_POST[idfactura]=$_GET[idfac];
				 	$sqlr="SELECT fecha,estado,liquidaciongen,mes,mesfin,codusuario,vigencia FROM servliquidaciones WHERE id_liquidacion='$_POST[idfactura]'";
					$resp = mysql_query($sqlr,$linkbd);
                    $row =mysql_fetch_row($resp);
					$sqlr1="SELECT terceroactual,nombretercero,codcatastral,direccion,barrio,estrato FROM servclientes WHERE codigo='$row[5]'";
					$resp1 = mysql_query($sqlr1,$linkbd);
                    $row1 =mysql_fetch_row($resp1);
					$_POST[fecha]=$row[0];
					$_POST[estadofac]=$row[1];
					switch ($row[1]) 
					{
						case 'S': 	$_POST[esfactura]="Vigente";break;
						case 'P':	$_POST[esfactura]="Cancelada";break;
						case 'V':	$_POST[esfactura]="Vencida";break;
						case 'L':	$_POST[esfactura]="Liquidada";break;
						default:	$_POST[esfactura]="Desconocido";
					}
					$_POST[cortefac]=$row[2];
					$_POST[mesini]=mesletras($row[3]);
					$_POST[mesfin]=mesletras($row[4]);
					$_POST[codusu]=$row[5];
					$_POST[docusu]=$row1[0];
					$_POST[nomusu]=$row1[1];
					$_POST[codcat]=$row1[2];
					$_POST[barriofa]=buscar_barrio($row1[4]);
					$_POST[rutafa]=buscar_ruta($row1[4]);
					$_POST[usofac]=buscar_usosuelo($row1[5]);
					$_POST[estfac2]=$row1[5];
					$_POST[direcc]=$row1[3];
					switch ($row1[5]) 
					{
						case 11: $_POST[estfac]="UNO";break;
						case 12: $_POST[estfac]="DOS";break;
						case 13: $_POST[estfac]="TRES";break;
                	}
					$_POST[vigenciafac]=$row[6];
				}
            ?>
           <table class="inicio" >
      			<tr>
        			<td class="titulos" colspan="9">.: Informaci&oacute;n General Factura</td>
                    <td class="cerrar" style='width:7%'><a onClick="location.href='serv-principal.php'">&nbsp;Cerrar</a></td>
      			</tr>
                <tr>
                	<td class="saludo1" style="width:3cm;">.: N&deg; Factura:</td>
                    <td style="width:10%;"><input type="text" name="idfactura" id="idfactura" value="<?php echo $_POST[idfactura];?>" style="width:100%;" readonly/></td>
                    <td class="saludo1" style="width:3cm;">.: Fecha:</td>
                    <td style="width:10%;"><input type="date" name="fecha" id="fecha" value="<?php echo $_POST[fecha];?>" style="width:100%;" readonly/></td>
                    <td class="saludo1" style="width:3cm;">.: No Ciclo:</td>
                    <td style="width:10%;"><input type="text" name="cortefac" id="cortefac" value="<?php echo $_POST[cortefac];?>" style="width:100%;" readonly/></td>
                    <td class="saludo1" style="width:3cm;">.: Estado:</td>
                    <td style="width:10%;"><input type="text" name="esfactura" id="esfactura" value="<?php echo $_POST[esfactura];?>" style="width:100%;" readonly/></td>
                    <td></td>
                </tr>
                <tr>
                	<td class="saludo1">.: Cod. Usuario:</td>
                    <td><input type="text" name="codusu" id="codusu" value="<?php echo $_POST[codusu];?>" style="width:100%;" readonly/></td>
                   	<td class="saludo1">.: Ruta:</td>
                    <td><input type="text" name="rutafa" id="rutafa" value="<?php echo $_POST[rutafa];?>" style="width:100%;" readonly/></td>
                    <td class="saludo1">.: Mes Inicial:</td>
                    <td><input type="text" name="mesini" id="mesini" value="<?php echo $_POST[mesini];?>" style="width:100%;" readonly/></td>
                    <td class="saludo1">.: Mes Final:</td>
                    <td><input type="text" name="mesfin" id="mesfin" value="<?php echo $_POST[mesfin];?>" style="width:100%;" readonly/></td>
                    <td></td>
                </tr>
                
          	</table>
            <table class="inicio">
            	<tr><td class="titulos" colspan="9">.: Informaci&oacute;n General Predio</td> </tr>
                <tr>
                    <td class="saludo1">.: Direcci&oacute;n:</td>
                	<td colspan="7"><input type="text" name="direcc" id="direcc" value="<?php echo $_POST[direcc];?>" style="width:100%;" readonly/></td>
                    <td></td>
                </tr>
               	<tr>
                    <td class="saludo1" style="width:3cm;">.: Cod. Catastral:</td>
                    <td style="width:10%;"><input type="text" name="codcat" id="codcat" value="<?php echo $_POST[codcat];?>" style="width:100%;" readonly/></td>
                    <td class="saludo1" style="width:3cm;">.: Barrio:</td>
                    <td colspan="3"><input type="text" name="barriofa" id="barriofa" value="<?php echo $_POST[barriofa];?>" style="width:100%;" readonly/></td>
                    <td class="saludo1" style="width:3cm;">.: Uso:</td>
                    <td style="width:10%;"><input type="text" name="usofac" id="usofac" value="<?php echo $_POST[usofac];?>" style="width:100%;" readonly/></td>
                   <td></td>
                </tr>
                <tr>
                	<td class="saludo1">.: Estrato:</td>
                    <td><input type="text" name="estfac" id="estfac" value="<?php echo $_POST[estfac];?>" style="width:100%;" readonly/></td>
                	<td class="saludo1" style="width:3cm;">.: Doc. Usuario:</td>
                    <td style="width:3cm;"><input type="text" name="docusu" id="docusu" value="<?php echo $_POST[docusu];?>" style="width:100%;" readonly/></td>
                    <td class="saludo1" style="width:3cm;">.: Nombre:</td>
                    <td colspan="3"><input type="text" name="nomusu" id="nomusu" value="<?php echo $_POST[nomusu];?>" style="width:100%;" readonly/></td>
                    <td></td>
            	</tr>
			</table>  
   			<table class="inicio">
            	<tr><td class="titulos" colspan="10">.: Informaci&oacute;n Servicios</td></tr>  
                <tr class="titulos2">
                	<td>C&oacute;digo</td>
                    <td>Concepto</td>
                    <td>Tarifa</td>
                    <td>Subsidio</td>
                    <td>Descuentos</td>
                    <td>Contribucion</td>
                    <td>saldos</td>
                    <td>Interes</td>
                    <td>valor real</td>
                    <td>abonos</td>
                </tr>  
                <?php
					$sqlr="SELECT servicio,tarifa,subsidio,descuento,contribucion,saldo,valorliquidacion,abono,intereses FROM servliquidaciones_det WHERE id_liquidacion='$_POST[idfactura]'";
					$resp = mysql_query($sqlr,$linkbd);
					$iter='saludo1a';
					$iter2='saludo2';
					$tottarifa=$totsubsidio=$totdescuentos=$totcontribucion=$totsaldos=$todvalorreal=$totabonos=0;
					$totalinteres=$contaplan=$totalrp=0;
					$sqlrrp="SELECT SUM(saldo), SUM(valorliquidacion) FROM servliquidaciones_det WHERE id_liquidacion='$_POST[idfactura]'";
					$resprp = mysql_query($sqlrrp,$linkbd);
					$rowrp =mysql_fetch_row($resprp);
                    while ($row =mysql_fetch_row($resp)) 
					{
						if($rowrp[0]<=0){$interes=0;}
						else {$interes=$row[6]-($row[1]-$row[2]-$row[3]+$row[4]+$row[5]);}
						$totalinteres+=$interes;
						if(($rowrp[1]+$rowrp[0])>=0){$valorreal2=$row[6];}
						else{$valorreal2=0;}
						$sqlrsv="SELECT nombre FROM servservicios WHERE codigo='$row[0]'";
						$rowsv =mysql_fetch_row(mysql_query($sqlrsv,$linkbd));
						echo"
						<input type='hidden' name='varcodigos[$contaplan]' value='$row[0]'/>
						<input type='hidden' name='varconceptos[$contaplan]' value='$rowsv[0]'/>
						<input type='hidden' name='vartarifas[$contaplan]' value='$row[1]'/>
						<input type='hidden' name='varsubsidios[$contaplan]' value='$row[2]'/>
						<input type='hidden' name='vardescuentos[$contaplan]' value='$row[3]'/>
						<input type='hidden' name='varcontribuciones[$contaplan]' value='$row[4]'/>
						<input type='hidden' name='varsaldos[$contaplan]' value='$row[5]'/>
						<input type='hidden' name='varvaloresreales[$contaplan]' value='$valorreal2'/>
						<input type='hidden' name='varabonos[$contaplan]' value='$row[7]'/>
						<tr class='$iter' >
							<td>$row[0]</td>
							<td>$rowsv[0]</td>
							<td style='text-align:right;'>$ ".number_format($row[1],2,',','.')."</td>
							<td style='text-align:right;'>$ ".number_format($row[2],2,',','.')."</td>
							<td style='text-align:right;'>$ ".number_format($row[3],2,',','.')."</td>
							<td style='text-align:right;'>$ ".number_format($row[4],2,',','.')."</td>
							<td style='text-align:right;'>$ ".number_format($row[5],2,',','.')."</td>
							<td style='text-align:right;'>$ ".number_format($interes,2,',','.')."</td>
							<td style='text-align:right;'>$ ".number_format($valorreal2,2,',','.')."</td>
							<td style='text-align:right;'>$ ".number_format($row[7],2,',','.')."</td>
						</tr>";
						$aux=$iter;
	 					$iter=$iter2;
	 					$iter2=$aux;
						$tottarifa+=$row[1];
						$totsubsidio+=$row[2];
						$totdescuentos+=$row[3];
						$totcontribucion+=$row[4];
						$totsaldos+=$row[5];
						$todvalorreal+=$valorreal2;
						$totabonos+=$row[7];
						$contaplan++;
					}
					echo"
						<tr class='titulos2'>
							<td style='text-align:right;' colspan='2'>Total:</td>
							<td style='text-align:right;'>$ ".number_format($tottarifa,2,',','.')."</td>
							<td style='text-align:right;'>$ ".number_format($totsubsidio,2,',','.')."</td>
							<td style='text-align:right;'>$ ".number_format($totdescuentos,2,',','.')."</td>
							<td style='text-align:right;'>$ ".number_format($totcontribucion,2,',','.')."</td>
							<td style='text-align:right;'>$ ".number_format($totsaldos,2,',','.')."</td>
							<td style='text-align:right;'>$ ".number_format($totalinteres,2,',','.')."</td>
							<td style='text-align:right;'>$ ".number_format($todvalorreal,2,',','.')."</td>
							<td style='text-align:right;'>$ ".number_format($totabonos,2,',','.')."</td>
						</tr>";
					$_POST[totalrec]=$todvalorreal;
					$_POST[totalsub]=$totsubsidio;
					$_POST[totalsald]=$totsaldos;
					$_POST[totalcontr]=$totcontribucion;
					$_POST[totaltarifa]=$tottarifa;
				?>
           	</table>      
            <input type="hidden" name="oculto" id="oculto" value="1"/>
            <input type="hidden" name="estfac2" id="estfac2" value="<?php echo $_POST[estfac2];?>"/>
            <input type="hidden" name="totalrec" id="totalrec" value="<?php echo $_POST[totalrec];?>"/>
            <input type="hidden" name="totaltarifa" id="totaltarifa" value="<?php echo $_POST[totaltarifa];?>"/>
            <input type="hidden" name="totalsub" id="totalsub" value="<?php echo $_POST[totalsub];?>"/>
            <input type="hidden" name="totalsald" id="totalsald" value="<?php echo $_POST[totalsald];?>"/>
            <input type="hidden" name="totalcontr" id="totalcontr" value="<?php echo $_POST[totalcontr];?>"/>
            <input type="hidden" name="estadofac" id="estadofac" value="<?php echo $_POST[estadofac];?>"/>
            <input type="hidden" name="vigenciafac" id="vigenciafac" value="<?php echo $_POST[vigenciafac];?>"/>
    		
      			
			</div>
		</form>
	</body>
</html>