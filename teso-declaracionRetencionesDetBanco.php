<?php

    header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require 'comun.inc';
	require 'funciones.inc';
	session_start();
    if(empty($_SESSION)){
        header("location: index.php");
    }
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html lang=es>
	<head>
    <meta charset="utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1"/>
		<meta name="viewport" content="user-scalable=no">
		<title>:: IDEAL 10 - Tesorería</title>
		<link href="favicon.ico" rel="shortcut icon"/>
		<link href="css/css2z.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
        <link href="css/tabs.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
        <link href="css/style.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" href="sweetalert2/dist/sweetalert2.min.css">
        <script type="text/javascript" src="css/programas.js"></script>
        <script src="sweetalert2/dist/sweetalert2.min.js"></script>
		<script src="vue/vue.min.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script src="css/calendario.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
	</head>
	<body>
		<header>
			<table>
				<tr><script>barra_imagenes("teso");</script><?php cuadro_titulos();?></tr>
			</table>
		</header>

		<form name="form2" method="post" action="">
			<section id="myapp" v-cloak >
                <div class="loading-container" v-show="isLoading" >
                    <p class="text-loading"data-loading="IDEAL 10">IDEAL 10</p>
                </div>
				<nav>
                    <table>
						<tr><?php menu_desplegable("teso");?></tr>
					</table>
                    <div class="bg-white group-btn p-1">
                        <button type="button" onclick="mypop=window.open('teso-principal.php','','');mypop.focus();" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
                            <span>Nueva ventana</span>
                            <svg class="fill-black group-hover:fill-white w-5 h-5" xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960"><path d="M320-120v-80h80v-80H160q-33 0-56.5-23.5T80-360v-400q0-33 23.5-56.5T160-840h640q33 0 56.5 23.5T880-760v400q0 33-23.5 56.5T800-280H560v80h80v80H320ZM160-360h640v-400H160v400Zm0 0v-400 400Z"></path></svg>
                        </button>
                        <button type="button" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center" @click="mypop=window.open('teso-declaracionRetencionesDetBanco','','');mypop.focus();">
                            <span class="group-hover:text-white">Duplicar pantalla</span>
                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960"><path d="M640-160v-360H160v360h480Zm80-200v-80h80v-360H320v200h-80v-200q0-33 23.5-56.5T320-880h480q33 0 56.5 23.5T880-800v360q0 33-23.5 56.5T800-360h-80ZM160-80q-33 0-56.5-23.5T80-160v-360q0-33 23.5-56.5T160-600h480q33 0 56.5 23.5T720-520v360q0 33-23.5 56.5T640-80H160Zm400-603ZM400-340Z"></path></svg>
                        </button>
                        <button type="button" @click="isModal = true;changeMode(1);" class="btn btn-danger btn-danger-hover d-flex justify-between align-items-center">
                            <span>Exportar PDF</span>
                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><!-- !Font Awesome Free 6.5.1 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license/free Copyright 2024 Fonticons, Inc. --><path d="M64 464l48 0 0 48-48 0c-35.3 0-64-28.7-64-64L0 64C0 28.7 28.7 0 64 0L229.5 0c17 0 33.3 6.7 45.3 18.7l90.5 90.5c12 12 18.7 28.3 18.7 45.3L384 304l-48 0 0-144-80 0c-17.7 0-32-14.3-32-32l0-80L64 48c-8.8 0-16 7.2-16 16l0 384c0 8.8 7.2 16 16 16zM176 352l32 0c30.9 0 56 25.1 56 56s-25.1 56-56 56l-16 0 0 32c0 8.8-7.2 16-16 16s-16-7.2-16-16l0-48 0-80c0-8.8 7.2-16 16-16zm32 80c13.3 0 24-10.7 24-24s-10.7-24-24-24l-16 0 0 48 16 0zm96-80l32 0c26.5 0 48 21.5 48 48l0 64c0 26.5-21.5 48-48 48l-32 0c-8.8 0-16-7.2-16-16l0-128c0-8.8 7.2-16 16-16zm32 128c8.8 0 16-7.2 16-16l0-64c0-8.8-7.2-16-16-16l-16 0 0 96 16 0zm80-112c0-8.8 7.2-16 16-16l48 0c8.8 0 16 7.2 16 16s-7.2 16-16 16l-32 0 0 32 32 0c8.8 0 16 7.2 16 16s-7.2 16-16 16l-32 0 0 48c0 8.8-7.2 16-16 16s-16-7.2-16-16l0-64 0-64z"></path></svg>
                        </button>
                        <button type="button" @click="isModal = true;selectMode=2;changeMode(2);" class="btn btn-success btn-success-hover d-flex justify-between align-items-center">
                            <span>Exportar Excel</span>
                            <svg class="fill-black group-hover:fill-white w-4 h-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 384 512"><!--!Font Awesome Free 6.5.1 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license/free Copyright 2024 Fonticons, Inc. --><path d="M48 448V64c0-8.8 7.2-16 16-16H224v80c0 17.7 14.3 32 32 32h80V448c0 8.8-7.2 16-16 16H64c-8.8 0-16-7.2-16-16zM64 0C28.7 0 0 28.7 0 64V448c0 35.3 28.7 64 64 64H320c35.3 0 64-28.7 64-64V154.5c0-17-6.7-33.3-18.7-45.3L274.7 18.7C262.7 6.7 246.5 0 229.5 0H64zm90.9 233.3c-8.1-10.5-23.2-12.3-33.7-4.2s-12.3 23.2-4.2 33.7L161.6 320l-44.5 57.3c-8.1 10.5-6.3 25.5 4.2 33.7s25.5 6.3 33.7-4.2L192 359.1l37.1 47.6c8.1 10.5 23.2 12.3 33.7 4.2s12.3-23.2 4.2-33.7L222.4 320l44.5-57.3c8.1-10.5 6.3-25.5-4.2-33.7s-25.5-6.3-33.7 4.2L192 280.9l-37.1-47.6z"></path></svg>
                        </button>
                        <button type="button" onclick="window.location.href='teso-declaracionretenciones.php'" class="btn btn-success btn-success-hover d-flex justify-between align-items-center">
                            <span>Atras</span>
                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960"><path d="M760-200v-160q0-50-35-85t-85-35H273l144 144-57 56-240-240 240-240 57 56-144 144h367q83 0 141.5 58.5T840-360v160h-80Z"></path></svg>
                        </button>
                    </div>
				</nav>
				<article>
                    <div  class="bg-white">
                        <div>
                            <h2 class="titulos m-0">Declaración de retenciones bancos</h2>
                            <div class="d-flex">
                                <div class="d-flex w-25">
                                    <div class="form-control">
                                        <label class="form-label m-0" for="">Fecha inicial:</label>
                                        <input type="date" v-model="txtFechaInicial" @change="filter()">
                                    </div>
                                    <div class="form-control">
                                        <label class="form-label m-0" for="">Fecha final:</label>
                                        <input type="date" v-model="txtFechaFinal" @change="filter()">
                                    </div>
                                </div>
                                <div class="d-flex w-75">
                                    <div class="form-control w-50">
                                        <label class="form-label m-0" for="">Destino económico:</label>
                                        <select v-model="selectDestino" @change="filter">
                                            <option value="">Otros</option>
                                            <option value="N">Nacional</option>
                                            <option value="D">Departamental</option>
                                            <option value="M">Municipal</option>
                                        </select>
                                    </div>
                                    <div class="form-control">
                                        <label class="form-label m-0" for="">Tipo:</label>
                                        <div class="d-flex">
                                            <select class="w-25" v-model="selectTipo" @change="filter">
                                                <option value="T">Todos</option>
                                                <option value="R">Retenciones</option>
                                                <option value="I">Ingresos</option>
                                            </select>
                                            <select class="w-50" v-model="selectRetenciones">
                                                <option value="">Todos</option>
                                                <option v-for="(data,index) in arrDataCopy" :key="index" :value="data.codigo">{{data.sigla+"-"+data.codigo+"-"+data.nombre}}</option>
                                            </select>
                                            <button type="button" @click="add" class="btn btn-primary">Actualizar</button>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>

                        <!--TABS-->
                        <div ref="rTabs" class="nav-tabs p-1">
                            <div class="nav-item active" @click="showTab(1)">Retenciones e ingresos</div>
                            <div class="nav-item" @click="showTab(2)">Detalle retenciones por banco</div>
                            <div class="nav-item" @click="showTab(3)">Detalle por bancos</div>
                        </div>
                        <!--CONTENIDO TABS-->
                        <div ref="rTabsContent" class="nav-tabs-content">
                            <div class="nav-content active">
                                <div class="table-responsive">
                                    <table  class="table fw-normal">
                                        <thead>
                                            <tr>
                                                <th>Tipo</th>
                                                <th>Nombre</th>
                                                <th>Opciones</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr v-for="(data,index) in arrRetenciones" :key="index">
                                                <td>{{data.sigla+"-"+data.codigo}}</td>
                                                <td>{{data.nombre}}</td>
                                                <td><button type="button" class="btn btn-sm btn-danger" @click="del(data.codigo)">Eliminar</button></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="nav-content">
                                <div>
                                    <div class="table-responsive">
                                        <table  class="table fw-normal">
                                            <thead>
                                                <tr>
                                                    <th>Retenciones/ingresos</th>
                                                    <th>Cuenta bancaria</th>
                                                    <th>Banco</th>
                                                    <th>Base</th>
                                                    <th>Valor</th>
                                                </tr>
                                            </thead>
                                            <tbody ref="tableData"></tbody>
                                            <tfoot>
                                                <tr>
                                                    <td class="fw-bold">Valor en letras</td>
                                                    <td >{{txtTotalLetras}}</td>
                                                    <td class="text-right fw-bold">Total</td>
                                                    <td>{{formatNumero(txtCuentas)}}</td>
                                                    <td>{{formatNumero(txtTotal)}}</td>
                                                </tr>
                                            </tfoot>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="nav-content">
                                <div class="w-100">
                                    <div class="table-responsive">
                                        <table  class="table fw-normal">
                                            <thead>
                                                <tr>
                                                    <th>Cuenta bancaria</th>
                                                    <th>Banco</th>
                                                    <th>Base</th>
                                                    <th>Valor</th>
                                                </tr>
                                            </thead>
                                            <tbody ref="tableDataBanco"></tbody>
                                            <tfoot>
                                                <tr>

                                                    <td colspan="2" class="text-right fw-bold">Total</td>
                                                    <td class="text-right">{{formatNumero(txtCuentas)}}</td>
                                                    <td class="text-right">{{formatNumero(txtTotal)}}</td>
                                                </tr>
                                                <tr>
                                                    <td class="fw-bold">Valor en letras:</td>
                                                    <td colspan="3">{{txtTotalLetras}}</span></td>
                                                </tr>
                                            </tfoot>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--MODALES-->
                    <div v-show="isModal" class="modal">
                        <div class="modal-dialog modal-sm" >
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title">{{txtTitle}}</h5>
                                    <button type="button" @click="isModal=false;" class="btn btn-close"><div></div><div></div></button>
                                </div>
                                <div class="modal-body">
                                    <div class="d-flex">
                                        <div class="form-control" v-show="selectMode == 1">
                                            <select v-model="selectPrint">
                                                <option value="1">Detalle retenciones por bancos</option>
                                                <option value="2">Detalle por bancos</option>
                                            </select>
                                        </div>
                                        <div class="form-control" v-show="selectMode == 2">
                                            <select v-model="selectExcel">
                                                <option value="1">Detalle retenciones por bancos</option>
                                                <option value="2">Detalle por bancos</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="d-flex justify-end">
                                        <button type="button" class="btn btn-primary" @click="exportData()">{{txtBtn}}</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
				</article>
			</section>
		</form>

		<script src="node_modules/read-excel-file/bundle/read-excel-file.min.js"></script>
		<script src="node_modules/xlsx/dist/xlsx.full.min.js"></script>
        <script src="Librerias/vue/vue.min.js"></script>
		<script src="Librerias/vue/axios.min.js"></script>
		<script type="module" src="tesoreria/declaracion_retenciones/js/functions_declaracion_retencion_banco.js?<?= date('d_m_Y_h_i_s');?>"></script>

	</body>
</html>
