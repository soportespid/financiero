<!--V 1.0 24/02/2015-->
<?php
	require"comun.inc";
	require"funciones.inc";
	session_start();
	$linkbd=conectar_bd();	
	cargarcodigopag($_GET[codpag],$_SESSION["nivel"]);
	header("Cache-control: private"); // Arregla IE 6
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es">
	<head>
	 	<meta http-equiv="Content-Type" content="text/html" charset="iso-8859-1"/>
        <meta http-equiv="X-UA-Compatible" content="IE=9"/>
		<title>:: Spid - Servicios P&uacute;blicos</title>
        <link href="css/css2.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<script type="text/javascript" src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script>
			function despliegamodal2(_valor,_num)
			{
				document.getElementById("bgventanamodal2").style.visibility=_valor;
				if(_valor=="hidden"){document.getElementById('ventana2').src="";}
				else 
				{
					switch(_num)
					{
						case '0':	document.getElementById('ventana2').src="clientes-ventana04.php";break;
					}
				}
			}
			function despliegamodalm(_valor,_tip,mensa,pregunta)
			{
				document.getElementById("bgventanamodalm").style.visibility=_valor;
				if(_valor=="hidden"){document.getElementById('ventanam').src="";}
				else
				{
					switch(_tip)
					{
						case "1":	document.getElementById('ventanam').src="ventana-mensaje1.php?titulos="+mensa;break;
						case "2":	document.getElementById('ventanam').src="ventana-mensaje3.php?titulos="+mensa;break;
						case "3":	document.getElementById('ventanam').src="ventana-mensaje2.php?titulos="+mensa;break;
						case "4":	document.getElementById('ventanam').src="ventana-consulta1.php?titulos="+mensa+"&idresp="+pregunta;break;	
					}
				}
			}
			function crearfactura()
			{
				if(document.getElementById('descuento1').value>0)
				{despliegamodalm('visible','4','Esta Seguro de realizar la nueva factura con la prescripción','1');}
				else{despliegamodalm('visible','2','No se puede generar una nueva factura, favor revisar la información');}
			}
			function respuestaconsulta(pregunta)
			{
				switch(pregunta)
				{
					case "1":	document.form2.oculto.value='2';document.form2.submit();break;
				}
			}
			function funcionmensaje()
			{
				document.form2.submit();
			}
		</script>
        <?php titlepag();?>
	</head>
	<body>
		<IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
		<span id="todastablas2"></span>
		<table>
            <tr><script>barra_imagenes("serv");</script><?php cuadro_titulos();?></tr>	 
            <tr><?php menu_desplegable("serv");?></tr>
			<tr>
  				<td colspan="3" class="cinta"><img src="imagenes/add.png" class="mgbt" onClick="location.href='serv-prescripciones.php'"/><img src="imagenes/guardad.png" class="mgbt1"/><img src="imagenes/busca.png" class="mgbt" onClick="location.href='serv-prescripcionesbuscar.php'" title="Buscar" /><img src="imagenes/nv.png" title="Nueva Ventana" onClick="mypop=window.open('serv-principal.php','','');mypop.focus();" class="mgbt"/><img src="imagenes/printd.png" class="mgbt1"/><img src='imagenes/iratras.png' title="Men&uacute; Nomina" onClick="location.href='serv-menufacturacion.php'" class='mgbt'></td>
			</tr>
  		</table>
		<div id="bgventanamodalm" class="bgventanamodalm">
            <div id="ventanamodalm" class="ventanamodalm">
                <IFRAME src="" name="ventanam" marginWidth=0 marginHeight=0 frameBorder=0 id="ventanam" frameSpacing=0 style=" width:700px; height:130px; top:200; overflow:hidden;"> 
                </IFRAME>
            </div>
        </div>
        <form name="form2" method="post" action="">
            <table  class="inicio" align="center" >
                <tr>
                    <td class="titulos" colspan="7">:: Prescripci&oacute;n de Facturaci&oacute;n</td>
                    <td class="cerrar" style="width:7%;"><a onClick="location.href='serv-principal.php'">&nbsp;Cerrar</a></td>
                </tr>
                <tr>
                    <td class="saludo1" style='width:3cm;'>C&oacute;digo Usuario:</td>
                    <td style="width:15%;"><input type="input" name="codusu" id="codusu" value="<?php echo $_POST[codusu];?>" style='width:80%;' onBlur="document.form2.submit();"/><img class="icobut" src="imagenes/find02.png" onClick="despliegamodal2('visible','0');" title="lista Usuarios"/></td>
                    <td></td>
             	</tr>
            </table>  
            <input type="hidden" name="oculto" id="oculto"  value="1"/>
            <table class="iniciox" style="height:68.5%; width:99.8%;">
           	 	<tr>
                	<td class="titulos" style="width:50%">:: Informaci&oacute;n Usuario</td>
                    <td class="titulos" >:: Informaci&oacute;n Facturas</td>
            	</tr>
                <tr>
                	<td>
                    	<?php
							if($_POST[codusu]!="")
							{
								$sqlr="SELECT codcatastral,terceroactual,nombretercero,direccion,barrio,zona,lado,estrato,fechacreacion FROM servclientes T1 WHERE codigo='$_POST[codusu]'";
								$resp = mysql_query($sqlr,$linkbd);
								$row =mysql_fetch_row($resp);
								$_POST[codcat]=$row[0];
								$_POST[docusu]=$row[1];
								$_POST[nomusu]=buscatercero($row[1]); 
								$_POST[nomcli]=$row[2];
								$_POST[dirusu]=$row[3];
								$sqlr1="SELECT nombre FROM servbarrios WHERE id='$row[4]'";
								$row1 =mysql_fetch_row(mysql_query($sqlr1,$linkbd));
								$_POST[barrio]=$row1[0];
								$_POST[zona]=$row[5];
								$_POST[lado]=$row[6];
								$sqlr1="SELECT tipo,descripcion,id FROM servestratos WHERE id='$row[7]'";
								$row1 =mysql_fetch_row(mysql_query($sqlr1,$linkbd));
								$_POST[estrato]=strtoupper("$row1[0] - $row1[1]");
								$_POST[idestrato]=$row1[2];
								$_POST[fecreg]=date('d-m-Y',strtotime($row[8]));
							}
						?>
                    	<div class="subpantalla" style="height:100%; width:99.4%; overflow-x:hidden;">
                        	<table class='inicio' >
                            	<tr>
                                	<td class='saludo1' style="width:3cm;">.: C&oacute;digo Catastral:</td>
                                    <td colspan="2"><input type="text" name="codcat" id="codcat" value="<?php echo $_POST[codcat];?>" style="width:100%;" readonly/></td>
                             	</tr>
                            	<tr>
                                	<td class='saludo1'>.: Documento:</td>
                                    <td colspan="2"><input type="text" name="docusu" id="docusu" value="<?php echo $_POST[docusu];?>" style="width:100%;" readonly/></td>
                             	</tr>
                            	<tr>
                                	<td class='saludo1'>.: Propietario:</td>
                                    <td colspan="2"><input type="text" name="nomusu" id="nomusu" value="<?php echo $_POST[nomusu];?>" style="width:100%;" readonly/></td>
                             	</tr>
                                <tr>
                                	<td class='saludo1'>.: Cliente</td>
                                    <td colspan="2"><input type="text" name="nomcli" id="nomcli" value="<?php echo $_POST[nomcli];?>" style="width:100%;" readonly/></td>
                             	</tr>
                                <tr>
                                	<td class='saludo1'>.: Direcci&oacute;n:</td>
                                    <td colspan="2"><input type="text" name="dirusu" id="dirusu" value="<?php echo $_POST[dirusu];?>" style="width:100%;" readonly/></td>
                             	</tr>
                                <tr>
                                	<td class='saludo1'>.: Barrio:</td>
                                    <td colspan="2"><input type="text" name="barrio" id="barrio" value="<?php echo $_POST[barrio];?>" style="width:100%;" readonly/></td>
                             	</tr>
                                <tr>
                                	<td class='saludo1'>.: zona:</td>
                                    <td colspan="2"><input type="text" name="zona" id="zona" value="<?php echo $_POST[zona];?>" style="width:100%;" readonly/></td>
                             	</tr>
                                <tr>
                                	<td class='saludo1'>.: Lado:</td>
                                    <td colspan="2"><input type="text" name="lado" id="lado" value="<?php echo $_POST[lado];?>" style="width:100%;" readonly/></td>
                             	</tr>
                                <tr>
                                	<td class='saludo1'>.: Estrato:</td>
                                    <td colspan="2"><input type="text" name="estrato" id="estrato" value="<?php echo $_POST[estrato];?>" style="width:100%;" readonly/></td>
                             	</tr>
                              	<tr>
                                	<td class='saludo1'>.: Fecha Registro:</td>
                                    <td colspan="2"><input type="text" name="fecreg" id="fecreg" value="<?php echo $_POST[fecreg];?>" style="width:100%;" readonly/></td>
                             	</tr>
                                <input type="hidden" name="idestrato" id="idestrato" value="<?php echo $_POST[idestrato];?>"/>
                                <?php
									if($_POST[codusu]!="")
									{
										$sumser=$valnet= $sumadescuntos=0;
										$sqlr="SELECT servicio, valorliquidacion, tarifa, subsidio, id_liquidacion FROM servliquidaciones_det WHERE codusuario = '$_POST[codusu]' AND id_liquidacion = (SELECT MAX(id_liquidacion) FROM servliquidaciones_det WHERE codusuario = '$_POST[codusu]')";
										$resp = mysql_query($sqlr,$linkbd);
										while ($row =mysql_fetch_row($resp)) 
										{
											$sqlr1="SELECT nombre FROM servservicios WHERE codigo='$row[0]'";
											$row1 =mysql_fetch_row(mysql_query($sqlr1,$linkbd));
											$sumser=$sumser+$row[1];
											$valneto=$row[2]-$row[3];
											$valnet=$valnet+$valneto;
											$_POST[ultimafac]=$row[4];
											$sqlr2="SELECT mesfin,vigencia FROM servliquidaciones WHERE id_liquidacion='$row[4]'";
											$row2 =mysql_fetch_row(mysql_query($sqlr2,$linkbd));
											$fechafin="$row2[1]-$row2[0]-1";
											$fechaini = strtotime ('-59 month', strtotime ( $fechafin ));
											$fechainimes = date ('m',$fechaini );
											$fechaini = date ('Y-m-j',$fechaini );  
											$sqlr3="SELECT SUM(total) FROM sertarifaspres WHERE CONCAT(ano,'-',mes,'-01') BETWEEN CAST('$fechaini' AS DATE) AND CAST('$fechafin' AS DATE) AND servicio='$row[0]' AND estrato='$_POST[idestrato]' AND estado='S'";
											$row3 =mysql_fetch_row(mysql_query($sqlr3,$linkbd));
											$sumadescuntos+=$row3[0];
											echo "
											<tr>
												<td class='saludo1'>.: Servicio $row[0]:</td>
												<td style='width:60%;'><input type='text'name='nomserv[]' value='$row1[0]' style='width:100%;' readonly/></td>
												<td><input type='text' name='valserv[]' value='$ ".number_format($row[1],2,',','.')."' style='width:100%;text-align:right;' readonly/></td>
											</tr>
											<input type='hidden' name='netoserv[]' value='$valneto'/>";
										}
										$descuen=$sumser-($sumadescuntos);
										if($descuen>0){$_POST[descuento]=$_POST[descuento1]=$descuen;}
										else{$_POST[descuento]=$_POST[descuento1]=0;}
										$_POST[netotot]=$valnet;
										$_POST[deuda]=$sumser;
										$_POST[totalpag]=$_POST[deuda]-$_POST[descuento];
										
									}
								?>
                                <tr>
                                	<td class='saludo1' style="width:3cm;">.: Deuda Total:</td>
                                    <td colspan="2"><input type="text" name="deuda" id="deuda" value="<?php echo "$ ".number_format($_POST[deuda],2,',','.');?>" style="width:100%;text-align:right;" readonly/></td>
                             	</tr>
                                 <tr>
                                	<td class='saludo1' style="width:3cm;">.: Descuento:</td>
                                    <td colspan="2"><input type="text" name="descuento" id="descuento" value="<?php echo "-$ ".number_format($_POST[descuento],2,',','.');?>" style="width:100%;text-align:right;" readonly/></td>
                             	</tr>
                                <tr>
                                	<td class='saludo1' style="width:3cm;">.: Total a Pagar:</td>
                                    <td colspan="2"><input type="text" name="totalpag" id="totalpag" value="<?php echo "$ ".number_format($_POST[totalpag],2,',','.');?>" style="width:100%;text-align:right;" readonly/></td>
                             	</tr>
                     			<input type="hidden" name="descuento1" id="descuento1" value="<?php echo $_POST[descuento1];?>" />
           						<input type="hidden" name="ultimafac" id="ultimafac" value="<?php echo $_POST[ultimafac];?>" />
                                <tr>
                                	<td colspan="3" style='text-align:right;'><input type="button" name="crearfac" id="crearfac" value="&nbsp;Nueva factura&nbsp;" onClick="crearfactura();"/></td>
                                </tr>
                                
                                <input type="hidden" name="netotot" id="netotot" value="<?php echo $_POST[netotot];?>"/>
                                
  							</table>
                        </div>
					</td>
                    <td>
                        <div class="subpantalla" style="height:100%; width:99.4%; overflow-x:hidden;">
                            <?php
								if($_POST[codusu]!="")
								{
									$sqlr="SELECT T1.id_liquidacion,T1.liquidaciongen,T1.vigencia,T1.codusuario,T1.estado,(SELECT concat_ws('<->',SUM(T3.valorliquidacion),SUM(T3.abono)) FROM servliquidaciones_det T3 WHERE T3.id_liquidacion=T1.id_liquidacion) FROM servliquidaciones T1 WHERE T1.codusuario='$_POST[codusu]'  ORDER BY T1.id_liquidacion DESC ";
									$resp = mysql_query($sqlr,$linkbd);
									echo "
									<table class='inicio' >
										<tr class='titulos2'>
											<td style='width:8%;'>N&deg; Factura</td>
											<td style='width:5%;'>N&deg; Corte</td>
											<td style='width:5%;'>Vigencia</td>
											<td style='width:10%;'>Valor</td>
											<td style='width:8%;'>Pagos y Abonos</td>
											<td style='width:5%;'>Estado</td>
										</tr>";	
									$iter='saludo1a';
									$iter2='saludo2';
									while ($row =mysql_fetch_row($resp)) 
									{
										$divvalores=explode('<->', $row[5]);
										$abonos=$divvalores[1];
										switch ($row[4]) 
										{
											case 'S': 	$imgest="<img src='imagenes/sema_amarilloON.jpg' title='Pago Pendiente' style='width:20px;'/>";break;
											case 'P':	$imgest="<img src='imagenes/sema_verdeON.jpg' title='Pago Realizado' style='width:20px;'/>";
														break;
											case 'V':	$imgest="<img src='imagenes/sema_rojoON.jpg' title='Pago Vencido' style='width:20px;'/>";break;
											case 'L':	$imgest="<img src='imagenes/sema_amarilloOFF.jpg' title='Factura Sin Realizar' style='width:20px;'/>";break;
											case 'A':	$imgest="<img src='imagenes/sema_azulON.jpg' title='Abono' style='width:20px;'/>";break;
											case 'R':	$imgest="<img src='imagenes/sema_azulON.jpg' title='Prescrita' style='width:20px;'/>";break;
											default:	$imgest="";
										}
										echo"
										<tr class='$iter' >
											<td>".str_pad($row[0],10,"0", STR_PAD_LEFT)."</td>
											<td>$row[1]</td>
											<td>$row[2]</td>
											<td style='text-align:right;'>$ ".number_format($divvalores[0],2,',','.')."</td>
											<td style='text-align:right;'>$ ".number_format($abonos,2,',','.')."</td>
											<td style='text-align:center;'>$imgest</td>            
										</tr>";
										$con+=1;
										$aux=$iter;
										$iter=$iter2;
										$iter2=$aux;
										$filas++;
									}
									echo"</table>";
								}
                            ?>			
                        </div>
                    </td>
                </tr>
            </table>
            <?php 
				if($_POST[oculto]=="2")
				{
					$fechaactual=date('Y-m-d');
					$sqlr0="INSERT INTO servprescripcion (id,codigo,valorini,valorfin,fecha,usuario) VALUES ('','$_POST[ultimafac]','$_POST[deuda]', '$_POST[totalpag]','$fechaactual','$_SESSION[usuario]')";
					$row0 =mysql_fetch_row(mysql_query($sqlr0,$linkbd));
					$sqlr1="SELECT * FROM servliquidaciones WHERE id_liquidacion='$_POST[ultimafac]'";
					$row1 =mysql_fetch_row(mysql_query($sqlr1,$linkbd));
					$nuevocons=selconsecutivo('servliquidaciones','id_liquidacion');
					$sqlr2="INSERT INTO servliquidaciones (id_liquidacion,fecha,vigencia,mes,mesfin,codusuario,tercero,valortotal,saldo,estado, 	liquidaciongen,factura) VALUES ('$nuevocons','$row1[1]','$row1[2]','$row1[3]','$row1[4]','$row1[5]','$row1[6]','$row1[7]','$row1[8]','S', '$row1[10]','$nuevocons')";
					mysql_query($sqlr2,$linkbd);
					$sqlr3 ="UPDATE servliquidaciones SET estado='R' WHERE id_liquidacion='$_POST[ultimafac]'";
					mysql_query($sqlr3,$linkbd);
					$sqlr4="SELECT * FROM servliquidaciones_det WHERE  id_liquidacion = '$_POST[ultimafac]'";
					$resp4 = mysql_query($sqlr4,$linkbd);
					while ($row4 =mysql_fetch_row($resp4)) 
					{
						$nuconsdet=selconsecutivo('servliquidaciones_det','id_det');
						$sqlr9="SELECT SUM(total) FROM sertarifaspres WHERE CONCAT(ano,'-',mes,'-01') BETWEEN CAST('$fechaini' AS DATE) AND CAST('$fechafin' AS DATE) AND servicio='$row4[4]' AND estrato='$row4[5]' AND estado='S'";
						$row9 =mysql_fetch_row(mysql_query($sqlr9,$linkbd));
						$descuento=$row4[12]-$row9[0];
						$nuevosaldo=$row4[12]-$descuento;
						$sqlr5="INSERT INTO servliquidaciones_det (id_det,id_liquidacion,codusuario,tercero,servicio,estrato,medicion,cargofijo, tarifa,subsidio,descuento,contribucion,valorliquidacion,saldo,abono,abonogen,intereses,inabono,estado) VALUES ('$nuconsdet','$nuevocons','$row4[2]', '$row4[3]','$row4[4]','$row4[5]','$row4[6]','$row4[7]','$row4[8]','$row4[9]','$descuento','$row4[11]','$nuevosaldo','$row4[13]','$row4[14]', '$row4[15]','$row4[16]','$row4[17]','')";
						mysql_query($sqlr5,$linkbd);
						$sqlr6 ="UPDATE servliquidaciones_det SET estado='R' WHERE id_det='$row4[0]'";
						mysql_query($sqlr6,$linkbd);
					}
					$sqlr7="INSERT INTO servfacturas (id_factura,vigencia,mes,mes2,codusuario,fecha,servicio,sistema,estado,liquidaciongen) VALUES ('$nuevocons', '$row1[2]','$row1[3]','$row1[4]','$row1[5]','$row1[1]','','86070388','S','$row1[10]')";
					mysql_query($sqlr7,$linkbd);
					echo "<script>despliegamodalm('visible','1','Se creo la nueva factura con Exito');</script>";
				}
			?>
		</form>
        <div id="bgventanamodal2">
			<div id="ventanamodal2">
				<IFRAME  src="" name="buster" marginWidth=0 marginHeight=0 frameBorder=0 id="ventana2" frameSpacing=0 style="left:500px; width:900px; height:500px; top:200;"></IFRAME>
        	</div>
		</div>
	</body>
</html>