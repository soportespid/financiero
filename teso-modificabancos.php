<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require "comun.inc";
	require "funciones.inc";
	session_start();
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html lang="es">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1">
		<meta name="viewport" content="user-scalable=no">
		<title>:: IDEAL 10 - Tesorer&iacute;a</title>
		<link href="favicon.ico" rel="shortcut icon"/>
        <script src="css/programas.js"></script>
		<script src="css/calendario.js"></script>
		<link href="css/css2z.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet"/>
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet"/>
	</head>
	<body>
		<IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
		<span id="todastablas2"></span>
		<table>
            <tr><script>barra_imagenes("teso");</script><?php cuadro_titulos();?></tr>
			<tr><?php menu_desplegable("teso");?></tr>
			<tr>
				<td colspan="3" class="cinta">
					<img src="imagenes/add2.png" class="mgbt1">
					<img src="imagenes/guardad.png"  class="mgbt1">
					<img src="imagenes/buscad.png"  class="mgbt1">
					<img src="imagenes/nv.png" title="Nueva Ventana"  onClick="mypop=window.open('teso-principal.php','','');mypop.focus();" class="mgbt">
				</td>
			</tr>
		</table>
		<form name="form2" method="post" action="">
			<table class="inicio">
				<tr>
					<td class="titulos" colspan="2">.: Cambiar Banco </td>
					<td class="cerrar" style="width:7%" onClick="location.href='teso-principal.php'">Cerrar</td>
				</tr>
				<tr>
					<td style="background-repeat:no-repeat; background-position:center;" class="saludo1">
						<ol id="lista2">
							<li class='mgbt3' onClick="location.href='teso-buscaegresobanco.php'">Modificar Banco Egreso</li>
							<li class='mgbt3' onClick="location.href='teso-buscaegresonominabanco.php'">Modificar Banco Egreso Nomina</li>
							<li class='mgbt3' onClick="location.href='teso-modificarBancoReciboCajaBuscar.php'">Modificar Banco Recibo Caja</li>
							<li class='mgbt3' onClick="location.href='teso-modificarBancoRecaudoBuscar.php'" style="cursor:pointer;">Modificar Banco Recaudo Transferencia</li>
							<li class='mgbt3' onClick="location.href='teso-modificarBancoIngresosBuscar.php'">Modificar Banco Ingresos Internos</li>
						</ol>
					</td>
				</tr>
			</table>
		</form>
	</body>
</html>
