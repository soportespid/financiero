<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require 'comun.inc';
	require 'funciones.inc';
	session_start();
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
	titlepag();
?>

<!DOCTYPE >
<html lang=es>
	<head>
		<meta charset="utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1"/>
		<meta name="viewport" content="user-scalable=no">
		<title>:: IDEAL 10 - Presupuesto</title>
		<link href="css/css2z.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<script src="vue/vue.min.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script src="css/calendario.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
        <link href="css/style.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
        <style>

            .titulo-form{
                background-color: #559CFC;
                height: 30px;
                margin: 1px;
                padding: 2px 4px;
                color: white;
            }

            .fondo-form{
                background-color: #F6F6F6;
                margin-bottom: 0px;
                /* margin-top: 10px; */
                /* padding-top: 4px; */
            }

            input[type=number]::-webkit-inner-spin-button,
			input[type=number]::-webkit-outer-spin-button {
				-webkit-appearance: none;
				margin: 0;
			}
			input[type=number]{
				text-align:right;
				box-sizing: border-box;
				font: 120% sans-serif;
				width: 100% !important;
			}

            .c9 input[type="checkbox"]:not(:checked),
            .c9 input[type="checkbox"]:checked {
                position: absolute !important;
                left: -9999px !important;
            }
            .c9 input[type="checkbox"]:not(:checked) +  #t9,
            .c9 input[type="checkbox"]:checked +  #t9 {
                position: relative !important;
                padding-left: 1.95em !important;
                cursor: pointer !important;
            }

            /* checkbox aspect */
            .c9 input[type="checkbox"]:not(:checked) +  #t9:before,
            .c9 input[type="checkbox"]:checked +  #t9:before {
                content: '' !important;
                position: absolute !important;
                left: 0 !important; top: -2 !important;
                width: 1.55em !important; height: 1.55em !important;
                border: 2px solid #ccc !important;
                background: #fff !important;
                border-radius: 4px !important;
                box-shadow: inset 0 1px 3px rgba(0,0,0,.1) !important;
            }
            /* checked mark aspect */
            .c9 input[type="checkbox"]:not(:checked) +  #t9:after,
            .c9 input[type="checkbox"]:checked + #t9:after {
                content: url(imagenes/tilde.png) !important;
                position: absolute !important;
                top: .1em; left: .3em !important;
                font-size: 1.3em !important;
                line-height: 0.8 !important;
                color: #09ad7e !important;
                transition: all .2s !important;
            }
            /* checked mark aspect changes */
            .c9 input[type="checkbox"]:not(:checked) +  #t9:after {
                opacity: 0 !important;
                transform: scale(0) !important;
            }
            .c9 input[type="checkbox"]:checked +  #t9:after {
                opacity: 1 !important;
                transform: scale(1) !important;
            }
            /* disabled checkbox */
            .c9 input[type="checkbox"]:disabled:not(:checked) +  #t9:before,
            .c9 input[type="checkbox"]:disabled:checked +  #t9:before {
                box-shadow: none !important;
                border-color: #bbb !important;
                background-color: #ddd !important;
            }
            .c9 input[type="checkbox"]:disabled:checked +  #t9:after {
                color: #999 !important;
            }
            .c9 input[type="checkbox"]:disabled +  #t9 {
                color: #aaa !important;
            }
            /* accessibility */
            .c9 input[type="checkbox"]:checked:focus + #t9:before,
            .c9 input[type="checkbox"]:not(:checked):focus + #t9:before {
                border: 2px dotted blue !important;
            }

            /* hover style just for information */
            .c9 #t9:hover:before {
                border: 2px solid #4778d9 !important;
            }
            #t9{
                background-color: white !important;
            }

            [v-cloak]{
                display : none;
            }
        </style>
        <script src="sweetalert2/dist/sweetalert2.min.js"></script>
		<link rel="stylesheet" href="sweetalert2/dist/sweetalert2.min.css">

    </head>
    <body>
        <!-- <IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME> -->
        <header>
			<table>
				<tr><script>barra_imagenes("ccpet");</script><?php cuadro_titulos();?></tr>
			</table>
		</header>
        <section id="myapp" v-cloak >
            <nav>
				<table>
					<tr><?php menu_desplegable("ccpet");?></tr>
					<tr>
						<td colspan="3" class="cinta">
							<img src="imagenes/add.png" onClick="location.href='ccp-formatof07c.php'" class="mgbt" title="Nuevo">
							<img src="imagenes/guardad.png" title="Guardar"  class="mgbt1">
							<img src="imagenes/buscad.png" class="mgbt" title="Buscar">
							<img src="imagenes/nv.png" onClick="mypop=window.open('ccp-principal.php','',''); mypop.focus();" class="mgbt" title="Nueva Ventana">
                            <img src="imagenes/excel.png" title="Excel" @click="downloadExl" class="mgbt">
                            <img src="imagenes/csv.png" title="Csv" @click="downloadCsv" class="mgbt">
                            <img src="imagenes/iratras.png" title="Atr&aacute;s" onClick="location.href='ccp-ejecucionpresupuestal.php'" class="mgbt">
						</td>
					</tr>
				</table>
			</nav>

            <article>

                <table class="inicio ancho">
                    <tr>
                        <td class="titulos" colspan="10" style = "text-align: center">Modificiones presupuesto de gastos. - FORMATO F07C</td>
                        <td class="cerrar" style="width:7%" onClick="location.href='ccp-principal.php'">Cerrar</td>
                    </tr>
                    <tr style = "margin-bottom: 10px">
                        <td class="" style="width:2.5cm;">
                            <label class="labelR">Fecha Inicial:</label>
                        </td>
                        <td style="width:5%;">
                            <input type="text" name="fecha"  value="<?php echo $_POST['fecha']?>" onKeyUp="return tabular(event,this)" id="fc_1198971545" title="DD/MM/YYYY" onDblClick="displayCalendarFor('fc_1198971545');" class="colordobleclik" autocomplete="off" onChange="" readonly>
                        </td>
                        <td class="" style="width:2.5cm;" >
                            <label class="labelR">Fecha Final:</label>
                        </td>
                        <td style="width:5%;">
                            <input type="text" name="fecha2" value="<?php echo $_POST['fecha2']?>" onKeyUp="return tabular(event,this)" id="fc_1198971546" title="DD/MM/YYYY" onDblClick="displayCalendarFor('fc_1198971546');" class="colordobleclik" autocomplete="off" onChange="" readonly>
                        </td>


                        <td colspan="2" style="padding-bottom:0px">
                            <em class="botonflechaverde" @click="buscarSaldos">Buscar</em>
                        </td>
                        <td></td>
                    </tr>
                </table>

                <div class='subpantalla' style='height:66vh; width:99.2%; margin-top:0px;'>
                    <table class='tablamv1' id="tableId">
                        <thead>
                            <tr style="text-align:Center;">
                                <th class="titulosnew00" style="width:10%;">N&uacute;mero</th>
                                <th class="titulosnew00" >Acto Administrativo</th>
                                <th class="titulosnew00" >N Acto Administrativo</th>
                                <th class="titulosnew00" >Fecha</th>
                                <th class="titulosnew00" >Adición</th>
                                <th class="titulosnew00" >Reducción</th>
                                <th class="titulosnew00" >Credito</th>
                                <th class="titulosnew00" >Contracredito</th>
                                <th class="titulosnew00" >Nombre Archivo Anexo</th>


                            </tr>
                        </thead>
                        <tbody>

                            <tr v-if="!existeInformacion">
                                <td colspan="18">
                                    <div style="text-align: center; color:turquoise; font-size:large" class="h4 text-primary text-center">
                                        Utilice los filtros para buscar informaci&oacute;n.
                                    </div>
                                </td>
                            </tr>
                            <tr v-for="(detalle, index) in detalles" v-bind:class="index % 2 ? 'contenidonew00' : 'contenidonew01'" style='text-rendering: optimizeLegibility; cursor: pointer !important;'>
                                <td  style=" font: 120% sans-serif; padding-left:5px; text-align:center;">{{ detalle[0] }}</td>
                                <td style="font: 120% sans-serif; padding-left:5px; text-align:center;">{{ detalle[1] }}</td>
                                <td style="font: 120% sans-serif; padding-left:5px;">{{ detalle[2] }}</td>
                                <td style="font: 120% sans-serif; padding-left:5px;text-align:center;">{{ detalle[3] }}</td>
                                <td style="font: 120% sans-serif; padding-left:5px;text-align:center;">{{ formatonumero(detalle[4]) }}</td>
                                <td style="font: 120% sans-serif; padding-left:5px;text-align:center;">{{ formatonumero(detalle[5]) }}</td>
                                <td style="font: 120% sans-serif; padding-left:5px;text-align:center;">{{ formatonumero(detalle[6]) }}</td>
                                <td style="font: 120% sans-serif; padding-left:5px;text-align:center;">{{ formatonumero(detalle[7]) }}</td>
                                <td style="font: 120% sans-serif; padding-left:5px;text-align:center;">{{ detalle[8] }}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div id="cargando" v-if="loading" class="loading">
                    <span>Cargando...</span>
                </div>
            </article>
        </section>
        <!-- <script type="module" src="./ejemplo.js"></script> -->
        <!-- <script src="Librerias/vue3/dist/vue.global.js"></script> -->
        <script src="xlsx/dist/xlsx.min.js"></script>
        <script src="file-saver/dist/FileSaver.min.js"></script>
        <script type="module" src="./presupuesto_ccpet/formatos/ccp-formatof07c.js"></script>
		<!-- <script src="Librerias/vue/axios.min.js"></script> -->
        <script src="Librerias/vue/axios.min.js"></script>
        <script src="https://unpkg.com/vue@3/dist/vue.global.js"></script>

    </body>
</html>
