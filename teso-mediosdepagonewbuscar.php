<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require 'comun.inc';
	require 'funciones.inc';
	session_start();
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
	titlepag();
?>
<!DOCTYPE >
<html lang=es>
	<head>
		<meta charset="utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1"/>
		<meta name="viewport" content="user-scalable=no">
		<title>:: IDEAL 10 - Parametrización</title>
		<link href="css/css2z.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/tabs.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
		<script src="vue/vue.min.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script src="sweetalert2/dist/sweetalert2.min.js"></script>
		<link rel="stylesheet" href="sweetalert2/dist/sweetalert2.min.css">
	</head>
	<body>

		<span id="todastablas2"></span>
		<header>
			<table>
				<tr><script>barra_imagenes("para");</script><?php cuadro_titulos();?></tr>
			</table>
		</header>
		<section id="myapp" v-cloak >
			<nav>
				<table>
					<tr><?php menu_desplegable("para");?></tr>
					<tr>
						<td colspan="3" class="cinta">
							<img src="imagenes/add.png" v-on:Click="location.href='teso-mediosdepagonew.php'" class="mgbt" title="Nuevo" >
							<img src="imagenes/guardad.png" title="Guardar" class="mgbt1">
							<img src="imagenes/busca.png" class="mgbt" title="Buscar" v-on:Click="location.href='teso-mediosdepagonewbuscar.php'">
							<img src="imagenes/nv.png" v-on:Click="mypop=window.open('para-principal.php','',''); mypop.focus();" class="mgbt" title="Nueva Ventana">
							<img src="imagenes/duplicar_pantalla.png" title="Duplicar pesta&ntilde;a" v-on:Click="mypop=window.open('<?php echo $url2; ?>','','');mypop.focus();" class="tooltip bottom mgbt">
						</td>
					</tr>
				</table>
			</nav>
			<article>
				<div class="tabsmeci" style="height:74vh; width:99.6%" >
					<div class="tab" >
						<input type="radio" id="tab-1" name="tabgroup1"  v-model="tabgroup1"  value="1">
						<label for="tab-1">Tesoreria</label>
						<div class="content" style="overflow:hidden;">
							<table class="inicio ancho">
								<tr>
									<td class="titulos" colspan="2" >BUSCAR MEDIOS DE PAGO</td>
									<td class="cerrar" style="width:7%" v-on:Click="location.href='teso-principal.php'">Cerrar</td>
								</tr>
								<tr>
									<td class="tamano01" style="width:3cm">Nombre:</td>
									<td><input type="text" class="form-control" placeholder="Buscar por Nombre de medio de pago" v-on:keyup="buscarCodigo" v-model="searchCodigo.keywordCodigo" style="width:100%" /></td>
								</tr>
							</table>
							<table class='tablamv'>
								<thead>
									<tr style="text-align:left;">
										<th class="titulos">Resultados Busqueda:</th>
									</tr>
									<tr style="text-align:Center;">
										<th class="titulosnew00" style="width:8%;">C&oacute;digo</th>
										<th class="titulosnew00" >Nombre</th>
										<th class="titulosnew00" style="width:10%;">Cuenta</th>
										<th class="titulosnew00" style="width:30%;">Tercero</th>
										<th class="titulosnew00" style="width:8%;">Tipo</th>
										<th class="titulosnew00" style="width:10%;">Estado</th>
										<th class="titulosnew00" style="width:8%;">Deshacer</th>
									</tr>
								</thead>
								<tbody>
									<tr v-for="(infocodigo,index) in infocodigos" v-on:dblclick="moveraeditar(infocodigo[0],'1')" v-bind:class="index % 2 ? 'saludo1a' : 'saludo2'" style='text-rendering: optimizeLegibility; cursor: pointer !important;'>
										<td style="font: 120% sans-serif; padding-left:10px; width:8%;">{{ infocodigo[0] }}</td>
										<td style="font: 120% sans-serif; padding-left:10px;">{{ infocodigo[1] }}</td>
										<td style="font: 120% sans-serif; padding-left:10px; width:10%; text-align:Center;">{{ infocodigo[2] }}</td>
										<td style="font: 120% sans-serif; padding-left:10px; width:30%; text-align:Center;">{{ infocodigo[3] }}</td>
										<td style="font: 120% sans-serif; padding-left:10px; width:8%; text-align:Center;">{{ infocodigo[5] }}</td>
										<td style="font: 120% sans-serif; padding-left:10px; width:10%; text-align:Center;">{{ infocodigo[4] }}</td>
										<td style="font: 120% sans-serif; padding-left:10px; width:8%; text-align:Center;"></td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>

					<div class="tab" >
						<input type="radio" id="tab-2" name="tabgroup1" v-model="tabgroup1" value="2">
						<label for="tab-2">Nomina</label>
						<div class="content" style="overflow:hidden;">
							<table class="inicio ancho">
								<tr>
									<td class="titulos" colspan="2" >BUSCAR MEDIOS DE PAGO</td>
									<td class="cerrar" style="width:7%" v-on:Click="location.href='teso-principal.php'">Cerrar</td>
								</tr>
								<tr>
									<td class="tamano01" style="width:3cm">Nombre:</td>
									<td><input type="text" class="form-control" placeholder="Buscar por Nombre de medio de pago" v-on:keyup="buscarCodigo1" v-model="searchCodigo1.keywordCodigo1" style="width:100%" /></td>
								</tr>
							</table>
							<table class='tablamv'>
								<thead>
									<tr style="text-align:left;">
										<th class="titulos">Resultados Busqueda:</th>
									</tr>
									<tr style="text-align:Center;">
										<th class="titulosnew00" style="width:8%;">C&oacute;digo</th>
										<th class="titulosnew00" >Nombre</th>
										<th class="titulosnew00" style="width:10%;">Cuenta</th>
										<th class="titulosnew00" style="width:30%;">Tercero</th>
										<th class="titulosnew00" style="width:8%;">Tipo</th>
										<th class="titulosnew00" style="width:10%;">Estado</th>
										<th class="titulosnew00" style="width:8%;">Deshacer</th>
									</tr>
								</thead>
								<tbody>
									<tr v-for="(infocodigo1,index) in infocodigos1" v-on:dblclick="moveraeditar(infocodigo1[0],'2')" v-bind:class="index % 2 ? 'saludo1a' : 'saludo2'" style='text-rendering: optimizeLegibility; cursor: pointer !important;'>
										<td style="font: 120% sans-serif; padding-left:10px; width:8%;">{{ infocodigo1[0] }}</td>
										<td style="font: 120% sans-serif; padding-left:10px;">{{ infocodigo1[1] }}</td>
										<td style="font: 120% sans-serif; padding-left:10px; width:10%; text-align:Center;">{{ infocodigo1[2] }}</td>
										<td style="font: 120% sans-serif; padding-left:10px; width:30%; text-align:Center;">{{ infocodigo1[3] }}</td>
										<td style="font: 120% sans-serif; padding-left:10px; width:8%; text-align:Center;">{{ infocodigo1[5] }}</td>
										<td style="font: 120% sans-serif; padding-left:10px; width:10%; text-align:Center;">{{ infocodigo1[4] }}</td>
										<td style="font: 120% sans-serif; padding-left:10px; width:8%; text-align:Center;"></td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</article>
			<input type="hidden" v-model="idbuscar" :value="idbuscar=1"/>
		</section>
		<script src="Librerias/vue/vue.min.js"></script>
		<script src="Librerias/vue/axios.min.js"></script>
		<script src="vue/tesoreria/teso-mediosdepagonew.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
	</body>
</html>