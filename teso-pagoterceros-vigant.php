<?php
	require "comun.inc";
	require "funciones.inc";
	require "conversor.php";
	require "funcionesSP.inc.php";
	session_start();
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
	titlepag();
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
?>
<html lang="es">
	<head>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<title>:: IDEAL 10 - Tesorer&iacute;a</title>

		<script>
			function generar(){
				document.form2.oculto.value='1';
				document.form2.submit();
			}
			function validar(){
				document.form2.submit();
			}
			function buscarp(e){
				if (document.form2.rp.value!=""){
					document.form2.brp.value='1';
					document.form2.submit();
				}
			}
			function agregardetalle(){
				if(document.form2.numero.value!="" &&  document.form2.valor.value>0 &&  document.form2.banco.value!=""){
					document.form2.agregadet.value = 1;
					document.form2.submit();
				}else{
					alert("Falta informacion para poder Agregar");
				}
			}
			function agregardetalled(){
				if(document.form2.retencion.value!="" &&  document.form2.valor.value!=""  &&  document.form2.cc.value!=""){
					document.form2.agregadetdes.value=1;
					document.form2.submit();
				}else{
					despliegamodalm('visible','2','Falta informacion para poder Agregar');
					//alert("Falta informacion para poder Agregar");
				}
			}
			function agregardetalleRetencion(){
				if(document.form2.retencionDesc.value!="" &&  document.form2.vporcentaje.value!=""  ){
					document.form2.agregadetdesRet.value=1;
					document.form2.calculaRetencion.value = 1;
					document.form2.submit();
				}
				else {alert("Falta informacion para poder Agregar");}
			}
			function eliminar(variable){
				document.form2.elimina.value=variable;
				//eli=document.getElementById(elimina);
				vvend=document.getElementById('elimina');
				//eli.value=elimina;
				vvend.value=variable;
				document.form2.submit();
			}
			function eliminarRetencion(variable){
				document.form2.eliminaRetencion.value=variable;
				//eli=document.getElementById(elimina);
				vvend=document.getElementById('eliminaRetencion');
				//eli.value=elimina;
				vvend.value=variable;
				document.form2.submit();
			}
			function guardar(){
				var tipo_pago = document.form2.tipop.value;
				if(tipo_pago == 'transferencia'){
					if (document.form2.fecha.value!='' && document.form2.cc.value!=''  && document.form2.tipop.value!='' && document.form2.tercero.value!='' && document.form2.nbanco.value!='' && document.form2.ntransfe.value != ''){

						despliegamodalm('visible','4','Esta Seguro de Guardar','2');
					}else{

						despliegamodalm('visible','2','Faltan datos para completar el registro');
					}
				}else{
					if (document.form2.fecha.value!='' && document.form2.cc.value!=''  && document.form2.tipop.value!='' && document.form2.tercero.value!='' && document.form2.nbanco.value!=''){

						despliegamodalm('visible','4','Esta Seguro de Guardar','2');
					}else{

						despliegamodalm('visible','2','Faltan datos para completar el registro');
					}
				}
			}
			function calcularpago(){
				valorp=document.form2.valor.value;
				descuentos=document.form2.totaldes.value;
				valorc=valorp-descuentos;
				document.form2.valorcheque.value=valorc;
				document.form2.valoregreso.value=valorp;
				document.form2.valorretencion.value=descuentos;
			}
			function eliminard(variable){
				if (confirm("Esta Seguro de Eliminar")){
					document.form2.eliminad.value=variable;
					vvend=document.getElementById('eliminad');
					vvend.value=variable;
					document.form2.submit();
				}
			}

			function pdf(){
				document.form2.action="pdfpagotercerosvigant.php";
				document.form2.target="_BLANK";
				document.form2.submit();
				document.form2.action="";
				document.form2.target="";
			}
			function despliegamodal2(_valor,scr){
				if(scr=="1"){
					var url="cuentasbancarias-ventana02.php?tipoc=D&obj01=banco&obj02=nbanco&obj03=&obj04=cb&obj05=ter";
				}
				if(scr=="2"){
					var url="cuentasbancarias-ventana02.php?tipoc=C&obj01=banco&obj02=nbanco&obj03=&obj04=cb&obj05=ter";
				}
				if(scr=="3"){
					var url="tercerosgral-ventana01.php?objeto=tercero&nobjeto=ntercero&nfoco=cc";
				}
				document.getElementById("bgventanamodal2").style.visibility=_valor;
				if(_valor=="hidden"){
					document.getElementById('ventana2').src="";
				}else{
					document.getElementById('ventana2').src=url;
				}
			}
			function despliegamodalm(_valor,_tip,mensa,pregunta){
				document.getElementById("bgventanamodalm").style.visibility=_valor;
				if(_valor=="hidden"){
					document.getElementById('ventanam').src="";
					switch(document.getElementById('valfocus').value){
						case "1":	document.getElementById('valfocus').value='';
									document.getElementById('tercero').focus();
									document.getElementById('tercero').select();
									break;
						case "2":	document.getElementById('valfocus').value='';
									document.getElementById('banco').value='';
									document.getElementById('banco').focus();
									document.getElementById('banco').select();
					}
				}else{
					switch(_tip){
						case "1":
							document.getElementById('ventanam').src="ventana-mensaje1.php?titulos="+mensa;break;
						case "2":
							document.getElementById('ventanam').src="ventana-mensaje3.php?titulos="+mensa;break;
						case "3":
							document.getElementById('ventanam').src="ventana-mensaje2.php?titulos="+mensa;break;
						case "4":
							document.getElementById('ventanam').src="ventana-consulta1.php?titulos="+mensa+"&idresp="+pregunta;break;
					}
				}
			}
			function funcionmensaje(){
				var numdocar=document.getElementById('idcomp').value;
				document.location.href = "teso-editapagotercerosvigant.php?idpago="+numdocar;
			}
			function respuestaconsulta(pregunta){
				switch(pregunta){
					case "1":	document.form2.oculto.value='3';
								document.form2.submit();
								break;
					case "2":	document.form2.oculto.value='2';
								document.form2.submit();
								break;
				}
			}
		</script>
		<script src="css/calendario.js"></script><script src="css/programas.js"></script>
		<link href="css/css2.css" rel="stylesheet" type="text/css" />
		<link href="css/css3.css" rel="stylesheet" type="text/css" />
		<link href="css/tabs.css" rel="stylesheet" type="text/css" />
		<link rel="stylesheet" href="bootstrap/fontawesome.5.11.2/css/all.css">
	</head>
	<body>
		<IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
		<span id="todastablas2"></span>
		<table>
			<tr><script>barra_imagenes("teso");</script><?php cuadro_titulos();?></tr>
			<tr><?php menu_desplegable("teso");?></tr>
			<tr>
				<td colspan="3" class="cinta">
					<a href="teso-pagoterceros-vigant.php" class="mgbt"><img src="imagenes/add.png" title="Nuevo"/></a>
					<a onClick="guardar()" class="mgbt"><img src="imagenes/guarda.png" title="Guardar" /></a>
					<a href="teso-buscapagoterceros-vigant.php" class="mgbt"><img src="imagenes/busca.png" title="Buscar"/></a>
					<a onClick="mypop=window.open('plan-agenda.php','','');mypop.focus()" class="mgbt"><img src="imagenes/agenda1.png" title="Agenda" /></a>
					<a onClick="mypop=window.open('teso-principal.php','','');mypop.focus();" class="mgbt"><img src="imagenes/nv.png" title="Nueva ventana"></a>
					<a onClick="mypop=window.open('<?php echo $url2; ?>','','');mypop.focus();" class="mgbt"><img src="imagenes/duplicar_pantalla.png" title="Duplicar pesta&ntilde;a"></a>

				</td>
			</tr>
		</table>
		<tr>
			<td colspan="3" class="tablaprin" align="center">
				<?php

					$sqlr = "SELECT MAX(id_pago) FROM tesopagotercerosvigant";
					$res = mysqli_query($linkbd, $sqlr);
					$r = mysqli_fetch_row($res);
					$_POST['idcomp'] = $r[0]+1;

					//*********** cuenta origen va al credito y la destino al debito
					if(!$_POST['oculto'])
					{
						$check1 = "checked";
						$fec = date("d/m/Y");
						$_POST['fecha'] = $fec;
						$_POST['vigencia'] = date("Y");
					}
					switch($_POST['tabgroup1'])
					{
						case 1:
						$check1='checked';
						break;
						case 2:
						$check2='checked';
						break;
						case 3:
						$check3='checked';
					}
				?>
				<div id="bgventanamodalm" class="bgventanamodalm">
					<div id="ventanamodalm" class="ventanamodalm">
						<IFRAME src="" name="ventanam" marginWidth=0 marginHeight=0 frameBorder=0 id="ventanam" frameSpacing=0 style=" width:700px; height:130px; top:200; overflow:hidden;">
						</IFRAME>
					</div>
				</div>
				<form name="form2" method="post" action="">
					<?php
						$meses = array('','Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre');
					?>

					<div class="container">
						<div class="tabsic" style="height:20%; width:99.6%;">
   							<div class="tab">
								<input type="radio" id="tab-1" name="tabgroup1" value="1" <?php echo $check1;?> >
								<label for="tab-1">Otro Egreso</label>
								<div class="content" style="overflow-x:hidden;">
									<table class="inicio" align="center" >
										<tr>
											<td style="width:95%;" colspan="2" class="titulos">Otros Egresos</td>
											<td style="width:5%;" class="cerrar" ><a href="teso-principal.php">Cerrar</a></td>
										</tr>
										<tr>
											<td style="width:80%;">
												<table>
													<tr>
														<td  class="saludo1" style="width:15%;">Numero Pago:</td>
														<td style="width:12%;">
															<input name="idcomp" id="idcomp" type="text" style="width:96%; text-align:center;" value="<?php echo $_POST['idcomp']?>" readonly>
														</td>
														<td class="saludo1" style="width:6%;">Fecha:</td>
														<td style="width:10%;">
															<input name="fecha" type="text" style="width:70%;" onchange="" value="<?php echo $_POST['fecha']?>" maxlength="10" onKeyPress="javascript:return solonumeros(event)" onKeyUp="return tabular(event,this)"  id="fc_1198971545" onKeyDown="mascara(this,'/',patron,true)" title="DD/MM/YYYY">
															<a onClick="displayCalendarFor('fc_1198971545');"><img src="imagenes/calendario04.png" style="width:20px; cursor:pointer"/></a>
														</td>
														<td style = "width:12%;" class="saludo1">Vigencia: </td>
														<td style = "width:10%;">
															<input name="vigencia" type="text" value="<?php echo $_POST['vigencia']?>" maxlength="2"  onKeyPress="javascript:return solonumeros(event)" onKeyUp="return tabular(event,this)" readonly>
														</td>
														<td class="saludo1"  style = "width:15%;">Forma de Pago:</td>
														<td  style = "width:15%;">
															<select name="tipop" onChange="validar();" style="width:100%;">
																<option value="">Seleccione ...</option>
																<option value="cheque" <?php if($_POST['tipop']=='cheque') echo "SELECTED"?>>Cheque</option>
																<option value="transferencia" <?php if($_POST['tipop']=='transferencia') echo "SELECTED"?>>Transferencia</option>
																<option value="caja" <?php if($_POST['tipop']=='caja') echo "SELECTED"?>>Efectivo</option>
															</select>
														</td>
													</tr>
													<?php
														//**** if del cheques
														if($_POST['tipop']=='cheque')
														{
															echo "<tr>
															<td class='saludo1'>Cuenta :</td>
															<td style='width:20%;'>
																<input type='text' name='cb' id='cb' value='$_POST[cb]' style='width:80%;'/>&nbsp;
																<a onClick=\"despliegamodal2('visible','2');\"  style='cursor:pointer;' title='Listado Cuentas Bancarias'>
																	<img src='imagenes/find02.png' style='width:20px;'/>
																</a>
															</td>
															<td colspan='4'>
																	<input type='text' id='nbanco' name='nbanco' style='width:100%;' value='$_POST[nbanco]'  readonly>
															</td>
															<td class='saludo1'>Cheque:</td>
															<td>
																<input type='text' id='ncheque' name='ncheque' value='$_POST[ncheque]' style='width:100%;'>
															</td>
																<input type='hidden' name='banco' id='banco' value='$_POST[banco]'/>
																<input type='hidden' id='ter' name='ter' value='$_POST[ter]'/>
															</tr>";


															if($_POST['cb']!=''){

															}
														}//cierre del if de cheques

														//**** if del transferencias
														if($_POST['tipop']=='transferencia')
														{
															echo "<tr>
																<td class='saludo1'>Cuenta :</td>
																<td style='width:20%;'>
																	<input type='text' name='cb' id='cb' value='$_POST[cb]' style='width:80%;'/>&nbsp;
																	<a onClick=\"despliegamodal2('visible','1');\"  style='cursor:pointer;' title='Listado Cuentas Bancarias'>
																		<img src='imagenes/find02.png' style='width:20px;'/>
																	</a>
																</td>
																<td colspan='4'>
																		<input type='text' id='nbanco' name='nbanco' style='width:100%;' value='$_POST[nbanco]'  readonly>
																<td class='saludo1'>No Transferencia:</td>
																<td >
																	<input type='text' id='ntransfe' name='ntransfe' value='$_POST[ntransfe]' style='width:100%;'>
																</td>
																	<input type='hidden' name='banco' id='banco' value='$_POST[banco]'/>
																	<input type='hidden' id='ter' name='ter' value='$_POST[ter]'/>
																</tr>";

														}//cierre del if de cheques
														if($_POST['tipop']=='caja')
														{
															if($_POST['escuentas']=='' || $_POST['escuentas']=='che')
															{
																$_POST['escuentas']='tran';
																$_POST['cb']='';
																$_POST['nbanco']='';
																$_POST['banco']='';
																$_POST['tcta']='';
																$_POST['ter']='';
																$_POST['ntransfe']='';
															}

															$sqlr = "SELECT cuentacaja FROM tesoparametros";
															$res = mysqli_query($linkbd, $sqlr);
															while ($row = mysqli_fetch_row($res))
															{
																$_POST['banco'] = $row[0];
																$_POST['nbanco'] = buscacuenta($row[0]);
															}
															echo"
															<tr>
																<td class='saludo1'>Cuenta Caja:</td>
																<td>
																	<input type='text' name='banco' id='banco' value='$_POST[banco]' style='width:80%' readonly/>
																&nbsp;
																</td>
																<td colspan='6'><input type='text' id='nbanco' name='nbanco' value='$_POST[nbanco]' style='width:100%' readonly></td>

															</tr>
															<input type='hidden' name='banco' id='banco' value='$_POST[banco]'/>";
														}
													?>
													<tr>
														<td  class="saludo1" >Tercero:</td>
														<td>
															<input id="tercero" type="text" name="tercero" style="width:80%;" onKeyUp="return tabular(event,this)" onBlur="buscater(event)" value="<?php echo $_POST['tercero']?>" >
															<a onClick="despliegamodal2('visible','3');"><img src="imagenes/find02.png" style="width:20px;cursor:pointer;"></a>

															</a>
														</td>
														<td colspan="4">
															<input name="ntercero" type="text" id="ntercero" style="width:100%;" value="<?php echo $_POST['ntercero']?>" readonly>
															<input type="hidden" value="0" name="bt">
														</td>
														<td class="saludo1" style="width:8%;">Centro Costo:</td>
														<td colspan="2">
															<select name="cc" style="width:100%;" onChange="validar()" onKeyUp="return tabular(event,this)">
																<option value="">Seleccione ...</option>
																<?php
																$sqlr = "SELECT id_cc, nombre FROM centrocosto WHERE estado = 'S' AND entidad = 'S'";
																$res = mysqli_query($linkbd, $sqlr);
																while ($row = mysqli_fetch_row($res))
																{
																	echo "<option value=$row[0] ";
																	$i=$row[0];

																	if($i==$_POST['cc'])
																	{
																		echo "SELECTED";
																	}
																	echo ">".$row[0]." - ".$row[1]."</option>";
																}
																?>
															</select>
														</td>
													</tr>
													<tr>
														<td class="saludo1">Concepto:</td>
														<td colspan="5">
															<input type="hidden" value="<?php echo "1"?>" name="oculto">
															<input type="text" name="concepto" value="<?php echo $_POST['concepto']?>" style="width:100%;">
														</td>
														<td class="saludo1" style="width:8%;">Valor a Pagar:</td>
														<td >
															<input name="valorpagar" style="width:100%;" type="text" id="valorpagar" onKeyUp="return tabular(event,this)" value="<?php echo $_POST['valorpagar']?>" size="20" readonly>
														</td>
													</tr>
													<tr>
														<td class="saludo1">Retenciones e Ingresos:</td>
														<td colspan="4">
															<select name="retencion"  style="width:100%;" onChange="validar()" onKeyUp="return tabular(event,this)">
																<option value="">Seleccione ...</option>
																<?php
																//PARA LA PARTE CONTABLE SE TOMA DEL DETALLE DE LA PARAMETRIZACION LAS CUENTAS QUE INICIAN EN 2**********************

																$sqlr = "SELECT id, codigo, nombre FROM tesoretenciones WHERE estado = 'S' AND terceros = '1' ";
																$res = mysqli_query($linkbd, $sqlr);
																while ($row =mysqli_fetch_row($res))
																{
																	echo "<option value='R-$row[0]' ";
																	$i=$row[0];

																	if('R-'.$i==$_POST['retencion'])
																	{
																		echo "SELECTED";
																		$_POST['nretencion']='R - '.$row[1]." - ".$row[2];
																	}
																	echo ">R - ".$row[1]." - ".$row[2]."</option>";
																}

																$sqlr = "SELECT codigo, nombre FROM tesoingresos WHERE estado = 'S' AND (terceros != '' OR is_tercero = 1)";
																$res = mysqli_query($linkbd, $sqlr);
																while ($row = mysqli_fetch_row($res))
																{
																	echo "<option value='I-$row[0]' ";
																	$i = $row[0];

																	if('I-'.$i==$_POST['retencion'])
																	{
																		echo "SELECTED";
																		$_POST['nretencion']='I - '.$row[0]." - ".$row[1];
																	}
																	echo ">I - ".$row[0]." - ".$row[1]."</option>";
																}
																?>
															</select>
														</td>
														<td class="saludo1" style="width:8%;">Valor:</td>
														<td style="width:8%;">
															<input name="valor" style="width:100%;" type="text" id="valor" onKeyUp="return tabular(event,this)" value="<?php echo $_POST['valor']?>" >
														</td>
														<td style="width:10%;">
															<input  type="hidden" value="<?php echo $_POST['nretencion']?>" name="nretencion">
															<input type="button" name="agregard" id="agregard" value="   Agregar   " onClick="agregardetalled()" style="width:100%;">
															<input type="hidden" value="0" name="agregadetdes">
														</td>
													</tr>
												</table>
											</td>
											<td colspan="3" style="width:20%; background:url(imagenes/siglasideal.png); background-repeat:no-repeat; background-position:center; background-size: 50% 100%;" ></td>
										</tr>
									</table>
								</div>
	 						</div>
							<div class="tab">
								<input type="radio" id="tab-2" name="tabgroup1" value="2" <?php echo $check2;?>>
								<label for="tab-2">Retenciones</label>
								<div class="content" style="overflow-x:hidden;">

										<table class="inicio" align="center" >
											<tr>
												<td class="titulos" colspan="10">Retenciones</td>
												<td class="cerrar" style="width:7%;"><a href="teso-principal.php">&nbsp;Cerrar</a></td>
											</tr>
											<tr>
												<td class="saludo1" style="width:12%;">Retencion y Descuento:</td>
												<td style="width:15%;">
													<select name="retencionDesc" onChange="validar()" onKeyUp="return tabular(event,this)">
														<option value="">Seleccione ...</option>
														<?php
															$sqlr="select *from tesoretenciones where estado='S'";
															$res = mysqli_query($linkbd, $sqlr);
															while ($row = mysqli_fetch_row($res))
															{
																if("$row[0]" == $_POST['retencionDesc'])
																{
																	echo "<option value='$row[0]' SELECTED>$row[1] - $row[2]</option>";
																	$_POST['porcentaje']=$row[5];
																	$_POST['nretencionDesc']=$row[1]." - ".$row[2];
																}
																else
																{
																	echo "<option value='$row[0]'>$row[1] - $row[2]</option>";
																}
															}
														?>
													</select>
													<input type="hidden" value="<?php echo $_POST['nretencionDesc']?>" name="nretencionDesc">
												</td>

												<td style="width:6%;"><input id="porcentaje" name="porcentaje" type="text" size="5" value="<?php echo $_POST['porcentaje']?>" readonly>%</td>

												<td class="saludo1" style="width:4%;">Valor:</td>
												<td>
													<input class='inputnum' id="vporcentaje" name="vporcentaje" type="text" size="10" value="<?php echo $_POST['vporcentaje']?>" >
													<input type="hidden" name="calculaRetencion" id="calculaRetencion" value="<?php echo $_POST['calculaRetencion'] ?>">
												</td>
												<td style="width:10%;">
													<input type='hidden' class='inputnum' id="totaldes" name="totaldes" size="10" value="<?php echo $_POST['totaldes']?>" readonly>
													<input type="button" name="agregard" id="agregard" value="   Agregar   " onClick="agregardetalleRetencion()" ><input type="hidden" value="0" name="agregadetdesRet">
												</td>
											</tr>
											<?php
												if ($_POST['eliminaRetencion'] != '')
												{
													$posi = $_POST['eliminaRetencion'];
													unset($_POST['ddescuentosRet'][$posi]);
													unset($_POST['dndescuentosRet'][$posi]);
													unset($_POST['dporcentajes'][$posi]);
													unset($_POST['ddesvaloresRet'][$posi]);
													unset($_POST['dmanual'][$posi]);
													$_POST['ddescuentosRet'] = array_values($_POST['ddescuentosRet']);
													$_POST['dndescuentosRet'] = array_values($_POST['dndescuentosRet']);
													$_POST['dporcentajes'] = array_values($_POST['dporcentajes']);
													$_POST['ddesvaloresRet'] = array_values($_POST['ddesvaloresRet']);
												}
												if ($_POST['agregadetdesRet']=='1')
												{
													$_POST['ddescuentosRet'][]=$_POST['retencionDesc'];
													$_POST['dndescuentosRet'][]=$_POST['nretencionDesc'];
													$_POST['dporcentajesRet'][]=$_POST['porcentaje'];
													$_POST['ddesvaloresRet'][]=$_POST['vporcentaje'];
													$_POST['agregadetdes']='0';
													echo"
													<script>
														document.form2.porcentaje.value=0;
														document.form2.vporcentaje.value=0;
														document.form2.retencionDesc.value='';
													</script>";
												}
											?>
										</table>
										<table class="inicio" style="overflow:scroll">
											<tr>
												<td class="titulos">Descuento</td>
												<td class="titulos">%</td>
												<td class="titulos">Valor</td>
												<td class="titulos2"><img src="imagenes/del.png" ><input type='hidden' name='eliminaRetencion' id='eliminaRetencion'></td>
											</tr>
											<?php
												$totaldes = 0;
												if(isset($_POST['ddescuentosRet'])){
													for ($x = 0; $x<count($_POST['ddescuentosRet']); $x++)
													{
														echo "
														<input type='hidden' name='dndescuentosRet[]' value='".$_POST['dndescuentosRet'][$x]."'>
														<input type='hidden' name='ddescuentosRet[]' value='".$_POST['ddescuentosRet'][$x]."' >
														<input type='hidden' name='dporcentajesRet[]' value='".$_POST['dporcentajesRet'][$x]."'>
														<input type='hidden' name='ddesvaloresRet[]' value='".($_POST['ddesvaloresRet'][$x])."'>
														<tr>
															<td class='saludo2'>".$_POST['dndescuentosRet'][$x]."</td>
															<td class='saludo2'>".$_POST['dporcentajesRet'][$x]."</td>
															<td class='saludo2'>".($_POST['ddesvaloresRet'][$x])."</td>
															<td class='saludo2'><a href='#' onclick='eliminarRetencion($x)'><img src='imagenes/del.png'></a></td>
														</tr>";

                                                        $totaldes += $_POST['ddesvaloresRet'][$x];
													}
												}
												if(isset($totaldes)){
													$_POST['valorretencion'] = $totaldes;
													echo"
													<script>
														document.form2.totaldes.value='$totaldes';
														document.form2.valorretencion.value='$totaldes';
													</script>";
													echo "<tr>
														<td colspan = '2' style='font-size:18px; text-align: center; color:gray !important; ' class='saludo1'>TOTAL:</td>
														<td class='saludo1'>
															$totaldes
														</td>
														<td></td>
													</tr>";
												}
											?>
                                            <input type='hidden' name='totalDescuentos' value="<?php echo $totaldes; ?>">
										</table>
									</div>
							</div>
						</div>
						<div class="subpantallac6" style="width:99.6%; height:40%; overflow-x:hidden;">
							<table class="inicio" style="overflow:scroll">
								<?php
								if ($_POST['eliminad']!='')
								{
									//echo "<TR><TD>ENTROS :".$_POST[elimina]."</TD></TR>";
									$posi = $_POST['eliminad'];
									unset($_POST['dcontable'][$posi]);
									unset($_POST['ddescuentos'][$posi]);
									unset($_POST['dtdescuentos'][$posi]);
									unset($_POST['dndescuentos'][$posi]);
									unset($_POST['dfvalores'][$posi]);
									unset($_POST['dvalores'][$posi]);
									$_POST['dcontable'] = array_values($_POST['dcontable']);
									$_POST['dtdescuentos'] = array_values($_POST['dtdescuentos']);
									$_POST['ddescuentos'] = array_values($_POST['ddescuentos']);
									$_POST['dndescuentos'] = array_values($_POST['dndescuentos']);
									$_POST['dfvalores'] = array_values($_POST['dfvalores']);
									$_POST['dvalores'] = array_values($_POST['dvalores']);
								}

								if ($_POST['agregadetdes'] == '1')
								{
									preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST['fecha'], $fecha);
									$fechaf = $fecha[3]."-".$fecha[2]."-".$fecha[1];

									$vcont='';
									$tm = strlen($_POST['retencion']);

									if(substr($_POST['retencion'],0,1) == 'R')
									{
										$sqlr = "SELECT conceptoingreso, modulo, tipoconce FROM tesoretenciones_det WHERE codigo = '".substr($_POST['retencion'],2,$tm-2)."'";

										$res2 = mysqli_query($linkbd, $sqlr);
										while($row2 = mysqli_fetch_row($res2))
										{
											$rest = substr($row2[2],-2);
											if($_POST['cc'] == '')
											{
												$_POST['cc'] = '01';
											}
											$cuentasContables = concepto_cuentasn2($row2[0], $rest, $row2[1], $_POST['cc'], "$fechaf");

											foreach($cuentasContables as $cuentaCont)
											{
												if(substr($cuentaCont[0],0,1) == 2 || substr($cuentaCont[0],0,1) == 4 || substr($cuentaCont[0],0,1) == 3 || substr($cuentaCont[0],0,1) == 5 || substr($cuentaCont[0],0,1) == 1 || substr($cuentaCont[0],0,2) == 19)
												{
													$vcont = $cuentaCont[0];
												}
											}
										}
									}
									//************
									if(substr($_POST['retencion'],0,1)=='I')
									{
										$sqlr = "SELECT concepto, porcentaje FROM tesoingresos_det WHERE codigo = '".substr($_POST['retencion'],2,$tm-2)."' AND vigencia = (SELECT MAX(vigencia) FROM tesoingresos_det WHERE codigo = '".substr($_POST['retencion'],2,$tm-2)."')";

										$res2 = mysqli_query($linkbd, $sqlr);
										while($row2 =mysqli_fetch_row($res2))
										{
											if($_POST['cc'] == '')
											{
												$_POST['cc'] = '01';
											}
											$cuentasContables = concepto_cuentasn2($row2[0], 'C', 4, $_POST['cc'], "$fechaf");

											foreach($cuentasContables as $cuentaCont)
											{
												if(substr($cuentaCont[0],0,1) == 2 || substr($cuentaCont[0],0,1) == 4 || substr($cuentaCont[0],0,1) == 3 || substr($cuentaCont[0],0,1) == 1)
												{
													$vpor = $row2[1];
													$vcont = $cuentaCont[0];
												}
											}

										}
									}

									if($vcont != ''){

										$_POST['dtdescuentos'][] = substr($_POST['retencion'],0,1);
										$_POST['ddescuentos'][] = $_POST['retencion'];
										$_POST['dndescuentos'][] = $_POST['nretencion'];
										$_POST['dcontable'][] = $vcont;
										$_POST['dvalores'][] = $_POST['valor'];
										$_POST['dcc'][] = $_POST['cc'];
										$_POST['dfvalores'][] = number_format($_POST['valor'],2);
										$_POST['agregadetdes'] = '0';
										?>
										<script>
											document.form2.retencion.value = '';
											document.form2.valor.value = '';
										</script>
										<?php

									}else{

										echo "<script>despliegamodalm('visible','2','No hay parametrizacion contable para este codigo de ingreso/retencion o centro de costo');</script>";
										//echodespliegamodalm('visible','2', "");

									}
								}
								?>
								<tr>
									<td class="titulos">Retenciones e Ingresos</td>
									<td class="titulos">Contable</td>
									<td class="titulos">Centro costo</td>
									<td class="titulos">Valor</td>
									<td class="titulos2"><img src="imagenes/del.png" ><input type='hidden' name='eliminad' id='eliminad'></td>
								</tr>
								<?php

								$totalpagar=0;



								for ($x = 0; $x < count($_POST['ddescuentos']); $x++)
								{

									//**********
									echo "<tr>
												<td class='saludo2'>
													<input name='dtdescuentos[]' value='".$_POST['dtdescuentos'][$x]."' type='hidden'>
													<input name='dndescuentos[]' value='".$_POST['dndescuentos'][$x]."' class='inpnovisibles' type='text' size='100' readonly>
													<input name='ddescuentos[]' value='".$_POST['ddescuentos'][$x]."' type='hidden'>
												</td>
												<td class='saludo2'>
													<input name='dcontable[]' value='".$_POST['dcontable'][$x]."' class='inpnovisibles' type='text' size='20' readonly>
												</td>
												<td class='saludo2'>
													<input name='dcc[]' value='".$_POST['dcc'][$x]."' class='inpnovisibles' type='text' size='20' readonly>
												</td>
												<td class='saludo2'>
													<input name='dfvalores[]' value='".$_POST['dfvalores'][$x]."' class='inpnovisibles' type='text' size='20' readonly>
													<input name='dvalores[]' value='".$_POST['dvalores'][$x]."' type='hidden'>
												</td>";
									echo "<td class='saludo2'><a href='#' onclick='eliminard($x)'><img src='imagenes/del.png'></a></td></tr>";
									$totalpagar+=$_POST['dvalores'][$x];
								}
								$_POST['valorretencion'] = $totaldes;

								$resultado = convertir($totalpagar);
								$_POST['letras'] = $resultado." PESOS M/CTE";
								echo "<tr>
											<td></td>
											<td></td>
											<td>Total:</td>
											<td class='saludo2'>
												<input type='hidden' name='totalpago2' value='$totalpagar' >
												<input type='text' name='totalpago' class='inpnovisibles' value='".number_format($totalpagar,2)."' size='20' readonly>
											</td>
										</tr>";
								echo "<tr>
										<td colspan='3' class='salud2'>
											<input name='letras' class='inpnovisibles' type='text' value='$_POST[letras]' readonly style='width:100%' >
										</td>";
								?>
								<script>
									document.form2.valorpagar.value=<?php echo $totalpagar;?>;
								</script>
							</table>
						</div>

						<?php
						//  echo "oculto".$_POST[oculto];
						if($_POST['oculto'] == '2')
						{
							//**verificacion de guardado anteriormente *****
							$sqlr = "SELECT COUNT(*) FROM tesopagotercerosvigant  WHERE id_pago = $_POST[idcomp]";
							$res = mysqli_query($linkbd, $sqlr);
							$r = mysqli_fetch_row($res);
							$numerorecaudos = $r[0];


							if($numerorecaudos == 0)
							{
								preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST['fecha'], $fecha);
								$fechaf = $fecha[3]."-".$fecha[2]."-".$fecha[1];

								//************CREACION DEL COMPROBANTE CONTABLE ************************
								$sqlr = "INSERT INTO tesopagotercerosvigant (`id_pago`, `tercero`, `banco`, `cheque`, `transferencia`, `valor`, `mes`, `concepto`, `cc`, `estado`, `fecha`) VALUES ($_POST[idcomp], '$_POST[tercero]', '$_POST[banco]', '$_POST[ncheque]', '$_POST[ntransfe]', $totalpagar, '$_POST[mes]', '".$_POST['concepto']."', '$_POST[cc]', 'S', '$fechaf')";
								mysqli_query($linkbd, $sqlr);

								//***busca el consecutivo del comprobante contable
								$sqlr = "INSERT INTO comprobante_cab (numerotipo, tipo_comp, fecha, concepto, total, total_debito, total_credito, diferencia,  estado) VALUES ($_POST[idcomp], 15, '$fechaf', '$_POST[concepto]', 0, $totalpagar, $totalpagar, 0, 1)";
								mysqli_query($linkbd, $sqlr);
								$cuentaDebRete = '';
								for ($x = 0; $x < count($_POST['ddescuentos']); $x++)
								{

                                    $valorEgreso = ($_POST['totalDescuentos'] > 0) ? round($_POST['dvalores'][$x] - (($_POST['dvalores'][$x]/$_POST['totalpago2']) * $_POST['totalDescuentos']) , 2) : round($_POST['dvalores'][$x] , 2);

									$sqlr = "INSERT INTO comprobante_det (id_comp, cuenta, tercero, centrocosto, detalle, cheque, valdebito, valcredito, estado, vigencia) VALUES ('15 $_POST[idcomp]', '".$_POST['dcontable'][$x]."', '".$_POST['tercero']."', '".$_POST['dcc'][$x]."', '".$_POST['concepto']." - ".$_POST['dndescuentos'][$x]."', '$_POST[ncheque]$_POST[ntransfe]', ".$valorEgreso.", 0, '1', '".$_POST['vigencia']."')";
									mysqli_query($linkbd, $sqlr);

									//*** Cuenta BANCO **
									$sqlr = "INSERT INTO comprobante_det (id_comp, cuenta, tercero, centrocosto, detalle, cheque, valdebito, valcredito, estado, vigencia) VALUES ('15 $_POST[idcomp]', '".$_POST['banco']."', '".$_POST['tercero']."', '".$_POST['dcc'][$x]."', '".$_POST['concepto']." - ".$_POST['dndescuentos'][$x]."', '$_POST[ncheque]$_POST[ntransfe]', 0, ".$valorEgreso.", '1', '".$_POST['vigencia']."')";
									mysqli_query($linkbd, $sqlr);

									$tm = strlen($_POST['ddescuentos'][$x]);
									$sqlr = "INSERT INTO tesopagotercerosvigant_det (`id_pago`, `movimiento`, `tipo`, `valor`, `cuenta`, `estado`) VALUES ($_POST[idcomp], '".substr($_POST['ddescuentos'][$x],2,$tm-2)."', '".$_POST['dtdescuentos'][$x]."', ".$_POST['dvalores'][$x].", '".$_POST['dcontable'][$x]."','S')";
									mysqli_query($linkbd, $sqlr);

									$cuentaDebRete = $_POST['dcontable'][$x];
								}
								// Retenciones
								$cc = $_POST['cc'];
								$cantDescuentos = count($_POST['ddescuentosRet']);
								for($x = 0; $x<$cantDescuentos; $x++)
								{
									$dd = $_POST['ddescuentosRet'][$x];
									$sqlr = "select * from tesoretenciones, tesoretenciones_det where tesoretenciones_det.codigo=tesoretenciones.id and tesoretenciones.id='".$dd."' ORDER BY porcentaje DESC";

									$resdes = mysqli_query($linkbd, $sqlr);
									$valordes = 0;
									$valorProcentaje = 0;
									$idRetencion = '';

									while($rowdes = mysqli_fetch_assoc($resdes))
									{
										$valordes = 0;
										if($idRetencion == $rowdes['codigo']){
											$valorProcentaje = $val2 + $rowdes['porcentaje'];
										}else{
											$valorProcentaje = $rowdes['porcentaje'];
											$idRetencion = $rowdes['codigo'];
										}
										$nomDescuento = $_POST['dndescuentosRet'][$x];

										$codigoIngreso = $rowdes['conceptoingreso'];
										if($codigoIngreso != "-1")
										{
											$codigoRetencion = $rowdes['conceptoingreso'];
											$rest = substr($rowdes['tipoconce'],-2);
											$val2 = $rowdes['porcentaje'];
										}

										$val2 = 0;
										$val2 = $rowdes['porcentaje'];

										$val3 = $_POST['ddesvaloresRet'][$x];
										$valordes = round((doubleVal($val2)/100)*doubleVal($val3),0);


										$sq = "select fechainicial from conceptoscontables_det where codigo='$codigoRetencion' and modulo='".$rowdes['modulo']."' and tipo='$rest' and fechainicial<'$fechaf' and cuenta!='' order by fechainicial asc";
										$re = mysqli_query($linkbd, $sq);
										while($ro = mysqli_fetch_assoc($re))
										{
											$_POST['fechacausa'] = $ro["fechainicial"];
										}

										$sqlr="select * from conceptoscontables_det where codigo='$codigoRetencion' and modulo='".$rowdes['modulo']."' and cc='".$cc."' and tipo='$rest' and fechainicial='".$_POST['fechacausa']."'";
										//echo $sqlr."<br>";
										$rst = mysqli_query($linkbd, $sqlr);
										$row1 = mysqli_fetch_assoc($rst);

										if($row1['cuenta']!='' && doubleVal($valordes)>0)
										{
											if($valorProcentaje <= 100){
												$sqlr="insert into comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle, cheque,valdebito, valcredito, estado, vigencia) values ('15 $_POST[idcomp]','".$row1['cuenta']."','".$_POST['tercero']."' ,'".$cc."' , 'Descuento ".$nomDescuento."','',0,".$valordes.",'1' ,'".$_POST['vigencia']."')";
												//echo $sqlr."<br>";
												mysqli_query($linkbd, $sqlr);

												$sqlr="insert into comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque,valdebito, valcredito,estado,vigencia) values ('15 $_POST[idcomp]','".$cuentaDebRete."','$_POST[tercero]','".$cc."','Descuento ".$nomDescuento."','','".$valordes."','0','1', '$_POST[vigencia]')";
												mysqli_query($linkbd, $sqlr);
											}
										}
									}
									$sqlrRet = "INSERT INTO tesopagotercerosvigant_retenciones(id_retencion, id_egreso, valor, estado) VALUES($dd, ".$_POST['idcomp'].", '".$_POST['ddesvaloresRet'][$x]."', 'S')";
									mysqli_query($linkbd, $sqlrRet);
								}
								$sql_funcion_auditoria = "SELECT id FROM teso_funciones WHERE nombre = 'Otros Egresos'";
								$row_funcion_auditoria = mysqli_fetch_assoc(mysqli_query($linkbd, $sql_funcion_auditoria));
								$conse = $_POST["idcomp"];
								$intFuncion = $row_funcion_auditoria["id"];
								insertAuditoria('teso_auditoria', "teso_funciones_id", $intFuncion, "Crear", $conse);
								echo "<script>despliegamodalm('visible','1','Se ha almacenado el Egreso con Exito');</script>";
								//echo "<table class='inicio'><tr><td class='saludo1'><center>Se ha almacenado el Recaudo a Terceros con Exito <img src='imagenes/confirm.png'><script>pdf()</script></center></td></tr></table>";
							}//*** if de guardado
							else
							{
								echo "<table class='inicio'><tr><td class='saludo1'><center><img src='imagenes/alert.png'>Ya Se ha almacenado un documento con ese consecutivo</center></td></tr></table>";
							}
						}
						?>
					</div>
					<div id="bgventanamodal2">
						<div id="ventanamodal2">
							<IFRAME  src="" name="buster" marginWidth=0 marginHeight=0 frameBorder=0 id="ventana2" frameSpacing=0 style="left:500px; width:900px; height:500px; top:200;">
							</IFRAME>
						</div>
					</div>
				</form>
			</td>
		</tr>
	</body>
</html>
