<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=uft8");
	require "comun.inc";
	require "funciones.inc";
    require "funcionesSP.inc.php";
	ini_set('max_execution_time',99999999);
	session_start();
	$linkbd=conectar_v7();
	$linkbd -> set_charset("utf8");
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es">
	<head>
		<meta http-equiv="Content-Type" content="text/html" charset="utf8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=9"/>
		<title>:: Reporte Liquidación</title>
		<link href="css/css2.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
        <link href="css/cssSP.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<script type="text/javascript" src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
        <script type="text/javascript" src="jquery-1.11.0.min.js"></script> 
        
		<script>
			$(window).load(function () {
				$('#cargando').hide();
			});

			function generarfacturas()
			{
				var corte = parseInt(document.getElementById('corte').value);

				if((corte != '-1'))
				{
					document.form2.oculto.value='2';
					document.form2.submit();
				}
			}

			function actualizar()
			{
				document.form2.submit();
			}

			function excell()
			{
				document.form2.action="serv-reporteLiquidacionSIGLOXXIExcel.php";
				document.form2.target="_BLANK";
				document.form2.submit(); 
				document.form2.action="";
				document.form2.target="";
			}
		</script> 
		<?php titlepag();?>
	</head>
	<body>
        <IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
		<span id="todastablas2"></span>
		<table>
			<tr><script>barra_imagenes("serv");</script><?php cuadro_titulos();?></tr>	 
			<tr><?php menu_desplegable("serv");?></tr>
			<tr>
				<td colspan="3" class="cinta">
					<a class="mgbt"><img src="imagenes/add2.png"/></a>
					<a class="mgbt"><img src="imagenes/guardad.png" title="Guardar"/></a>
					<a href="" class="mgbt"><img src="imagenes/busca.png" title="Buscar"/></a>
					<a onclick="mypop=window.open('plan-agenda.php','','');mypop.focus()" class="mgbt"><img src="imagenes/agenda1.png" title="Agenda" /></a>
					<a onclick="mypop=window.open('serv-principal.php','','');mypop.focus();" class="mgbt"><img src="imagenes/nv.png" title="Nueva ventana"></a>
					<a onclick="mypop=window.open('<?php echo $url2; ?>','','');mypop.focus();" class="mgbt"><img src="imagenes/duplicar_pantalla.png" title="Duplicar pesta&ntilde;a"></a>
					<a href=""><img src="imagenes/excel.png" title="Excel" onClick="excell()" class="mgbt"></td></a>
                </td>
			</tr>
		</table>

		<form name="form2" method="post">
			<table class="inicio ancho" style="width:99.5%">
				<tr>
					<td class="titulos" colspan="3">Reporte Liquidación Servicios Públicos</td>
					<td class="cerrar" style="width:7%" onClick="location.href='serv-principal.php'">Cerrar</td>
				</tr>
				
				<tr>
					<td class="tamano01" style="width: 3cm;">Corte Liquidado:</td>
                    <td style="width: 20%;">
                        <select name="corte" id="corte" class="centrarSelect" style="width: 100%;" onchange="actualizar();">
                            <option value="-1" class="aumentarTamaño">SELECCIONE CORTE</option>
                            <?php
                                $sql = "SET lc_time_names = 'es_ES'";
                                mysqli_query($linkbd,$sql);

								$sqlr = "SELECT numero_corte, UPPER(MONTHNAME(fecha_inicial)), UPPER(MONTHNAME(fecha_final)), YEAR(fecha_inicial), YEAR(fecha_final) FROM srvcortes WHERE numero_corte > 0";
								$resp = mysqli_query($linkbd,$sqlr);
								while ($row = mysqli_fetch_row($resp))
								{
									if(@ $_POST['corte'] == $row[0])
									{
										echo "<option class='aumentarTamaño' value='$row[0]' SELECTED>$row[1] $row[3] - $row[2] $row[4]</option>";
									}
									else{echo "<option class='aumentarTamaño' value='$row[0]'>$row[1] $row[3] - $row[2] $row[4]</option>";}
								}
							?>
                        </select>
                    </td>

					<td style="padding-bottom:0px;height:35px;"><em class="botonflecha" onclick="generarfacturas()">Generar Reporte</em></td>
				</tr>
			</table>

            <div id="cargando" style=" position:absolute;left: 46%; bottom: 45%">
				<img src="imagenes/loading.gif" style=" width: 80px; height: 80px"/>
			</div>

			<div class="subpantalla" style="height:71%; width:99.2%;">
				<table class='inicio' align='center' width='99%'>
					<tr>
						<td colspan='90' class='titulos'>Resultados Busqueda: </td>
					</tr>
				
					<tr class='titulos2' style='text-align:center;'>
                        <td style="width: 5%;">Numero Factura</td>
						<td style="width: 5%;">Código Usuario</td>
						<td>Nombre Suscriptor</td>
						<td style="width: 5%;">Barrio</td>
                        <td style="width: 9%;">Dirección</td>
						<td style="width: 2%;">Ruta</td>
						<td style="width: 9%;">Código Ruta</td>

						<!-- servicio 1 -->
						<td style="width: 5%;">Servicio</td>
						<td style="width: 5%;">Estrato</td>
                        <td style="width: 2%;">Cargo Fijo</td>
						<td style="width: 2%;">Consumo</td>
                        <td style="width: 5%;">Medidor</td>
                        <td style="width: 4%;">Lectura Anterior</td>
                        <td style="width: 4%;">Lectura Actual</td>
                        <td style="width: 4%;">Consumo</td>
						<td style="width: 5%;">Consumo Basico</td>
						<td style="width: 5%;">Consumo Complementario</td>
						<td style="width: 5%;">Consumo Suntuario</td>
						<td style="width: 5%;">Alumbrado</td>
						<td style="width: 5%;">Subsidio CB</td>
						<td style="width: 5%;">Subsidio CC</td>
						<td style="width: 5%;">Deuda Anterior</td>
						<td style="width: 5%;">Abonos</td>
						<td style="width: 5%;">Acuerdo Pago</td>
						<td style="width: 5%;">Venta Medidores</td>
						<td style="width: 5%;">Interes Mora</td>

						<!-- servicio 2 -->
						<td style="width: 5%;">Servicio</td>
						<td style="width: 5%;">Estrato</td>
                        <td style="width: 2%;">Cargo Fijo</td>
						<td style="width: 2%;">Consumo</td>
                        <td style="width: 5%;">Medidor</td>
                        <td style="width: 4%;">Lectura Anterior</td>
                        <td style="width: 4%;">Lectura Actual</td>
                        <td style="width: 4%;">Consumo</td>
						<td style="width: 5%;">Cargo Fijo</td>
						<td style="width: 5%;">Consumo Basico</td>
						<td style="width: 5%;">Consumo Complementario</td>
						<td style="width: 5%;">Consumo Suntuario</td>
                        <td style="width: 5%;">Subsidio CF</td>
						<td style="width: 5%;">Subsidio CB</td>
						<td style="width: 5%;">Contribución CF</td>
						<td style="width: 5%;">Contribución CB</td>
						<td style="width: 5%;">Deuda Anterior</td>
						<td style="width: 5%;">Abonos</td>
						<td style="width: 5%;">Acuerdo Pago</td>
						<td style="width: 5%;">Venta Medidores</td>
						<td style="width: 5%;">Interes Mora</td>

						<!-- servicio 3 -->
						<td style="width: 5%;">Servicio</td>
						<td style="width: 5%;">Estrato</td>
                        <td style="width: 2%;">Cargo Fijo</td>
						<td style="width: 2%;">Consumo</td>
						<td style="width: 5%;">TBL</td>
						<td style="width: 5%;">TRT</td>
						<td style="width: 5%;">TDT</td>
						<td style="width: 5%;">TFR</td>
                        <td style="width: 5%;">Subsidio</td>
						<td style="width: 5%;">Contribución</td>
						<td style="width: 5%;">Deuda Anterior</td>
						<td style="width: 5%;">Abonos</td>
						<td style="width: 5%;">Acuerdo Pago</td>
						<td style="width: 5%;">Interes Mora</td>


						<td style="width: 5%;">Total Factura</td>
					</tr>

					<?php
						if(@ $_POST['oculto']=="2")
						{
							$iter  = 'saludo1a';
							$iter2 = 'saludo2';
							$corteAnt = $_POST['corte'] - 1;
							$cont = 0;

							$sqlUsuariosCorte = "SELECT DISTINCT id_cliente FROM srvcortes_detalle WHERE id_corte = '$_POST[corte]' ORDER BY numero_facturacion ASC";	
							$resUsuariosCorte = mysqli_query($linkbd,$sqlUsuariosCorte);
							$encontrados = mysqli_num_rows($resUsuariosCorte);
							while ($rowUsuariosCorte = mysqli_fetch_row($resUsuariosCorte))
							{
								$sqlInformacionUsuario = "SELECT cod_usuario, id_barrio, id_estrato, id_ruta, codigo_ruta FROM srvclientes WHERE id = '$rowUsuariosCorte[0]'";
								$resInformacionUsuario = mysqli_query($linkbd,$sqlInformacionUsuario);
								$rowInformacionUsuario = mysqli_fetch_row($resInformacionUsuario);

								$sqlVisualizarFactura = "SELECT num_factura, nombre_suscriptor FROM srvfacturas WHERE cliente = '$rowUsuariosCorte[0]' AND corte = '$_POST[corte]'";
								$resVisualizarFactura = mysqli_query($linkbd,$sqlVisualizarFactura);
								$rowVisualizarFactura = mysqli_fetch_row($resVisualizarFactura);

								$numeroFactura = $rowVisualizarFactura[0];
								$codigoUsuario = $rowInformacionUsuario[0];
								$nombreSuscriptor = $rowVisualizarFactura[1];
								
								$nombreBarrio = 'Barrio no encontrado';

								$sqlBarrio = "SELECT nombre FROM srvbarrios WHERE id = $rowInformacionUsuario[1]";
								$rowBarrio = mysqli_fetch_row(mysqli_query($linkbd,$sqlBarrio));
						
								if($rowBarrio[0] != '')
								{
									$nombreBarrio = $rowBarrio[0];
								}

								$barrio = $nombreBarrio;
								$ruta = $rowInformacionUsuario[3];

								$codigoRuta = 0;

								if($rowInformacionUsuario[4] != '')
								{
									$codigoRuta = $rowInformacionUsuario[4];	
								}

								$sqlDireccion = "SELECT direccion FROM srvdireccion_cliente WHERE id_cliente = '$rowUsuariosCorte[0]'";
								$resDireccion = mysqli_query($linkbd,$sqlDireccion);
								$rowDireccion = mysqli_fetch_assoc($resDireccion);
						
								$direccion = $rowDireccion['direccion'];

								echo "
									<input type='hidden' name='numFactura[]' value='$numeroFactura'>
									<input type='hidden' name='codUsuario[]' value='$codigoUsuario'>
									<input type='hidden' name='nombreSus[]' value='$nombreSuscriptor'>
									<input type='hidden' name='barrio[]' value='$barrio'>
									<input type='hidden' name='direccion[]' value='$direccion'>
									<input type='hidden' name='ruta[]' value='$ruta'>
									<input type='hidden' name='codigoRuta[]' value='$codigoRuta'>
								";
					?>			
								<tr class='<?php echo $iter ?>' style='text-align:center; text-transform:uppercase;'>
									<td><?php echo $numeroFactura ?></td>
									<td><?php echo $codigoUsuario ?></td>
									<td><?php echo $nombreSuscriptor ?></td>
									<td><?php echo $barrio ?></td>
									<td><?php echo $direccion ?></td>
									<td><?php echo $ruta ?></td>
									<td><?php echo $codigoRuta ?></td>

					<?php

								$sqlServicios = "SELECT id, nombre FROM srvservicios ORDER BY id ASC";
								$resServicios = mysqli_query($linkbd,$sqlServicios);
								while ($rowServicios = mysqli_fetch_row($resServicios))
								{
									$sqlAsignacionServicio = "SELECT id_estrato FROM srvasignacion_servicio WHERE id_clientes = $rowUsuariosCorte[0] AND id_servicio = $rowServicios[0]";
									$rowAsignacionServicio = mysqli_fetch_row(mysqli_query($linkbd,$sqlAsignacionServicio));

									$query = "SELECT descripcion FROM srvestratos WHERE id = $rowAsignacionServicio[0]";
									$resp = mysqli_query($linkbd,$query);
									$row = mysqli_fetch_row($resp);

									if ($row[0] != '' && isset($row[0]))
									{
										$resultado = $row[0];
									}
									else 
									{
										$resultado = "NO APLICA";
									}

									$estrato = $resultado;

									$sqlVisualizarFactura = "SELECT num_factura, nombre_suscriptor FROM srvfacturas WHERE cliente = '$rowUsuariosCorte[0]' AND corte = '$_POST[corte]' AND id_servicio = '$rowServicios[0]'";
									$resVisualizarFactura = mysqli_query($linkbd,$sqlVisualizarFactura);
									$rowVisualizarFactura = mysqli_fetch_row($resVisualizarFactura);

									$servicio = $rowServicios[1];

									$estado = 'No';
        
									$sqlAsignacionServicio = "SELECT cargo_fijo FROM srvasignacion_servicio WHERE id_clientes = $rowUsuariosCorte[0] AND id_servicio = $rowServicios[0]";
									$rowAsignacionServicio = mysqli_fetch_row(mysqli_query($linkbd,$sqlAsignacionServicio));

									if($rowAsignacionServicio[0] == 'S')
									{
										$estado = 'Si';
									}

									$estadoCF = $estado;


									$estado = 'No';
        
									$sqlAsignacionServicio = "SELECT consumo FROM srvasignacion_servicio WHERE id_clientes = $rowUsuariosCorte[0] AND id_servicio = $rowServicios[0]";
									$rowAsignacionServicio = mysqli_fetch_row(mysqli_query($linkbd,$sqlAsignacionServicio));

									if($rowAsignacionServicio[0] == 'S')
									{
										$estado = 'Si';
									}

									$estadoCS = $estado;

									$medidor = 0;

									$sql = "SELECT id_medidor FROM srvasignacion_servicio WHERE id_clientes = $rowUsuariosCorte[0] AND id_servicio = $rowServicios[0]";
									$res = mysqli_query($linkbd,$sql);
									$row = mysqli_fetch_row($res);
									
									if($row[0] != '')
									{
										$medidor = $row[0];
									}


									$lecturaAnt = 0;

									$sqlLecturaAnt = "SELECT lectura_medidor FROM srvlectura WHERE corte = $corteAnt AND id_cliente = $rowUsuariosCorte[0] AND id_servicio = '$rowServicios[0]'";
									$resLecturaAnt = mysqli_query($linkbd,$sqlLecturaAnt);
									$rowLecturaAnt = mysqli_fetch_row($resLecturaAnt);

									if ($rowLecturaAnt[0] != '') {
										$lecturaAnt = $rowLecturaAnt[0];
									} 

									$lecturaAct = 0;

									$sqlLecturaAct = "SELECT lectura_medidor FROM srvlectura WHERE corte = $_POST[corte] AND id_cliente = $rowUsuariosCorte[0] AND id_servicio = '$rowServicios[0]'";
									$resLecturaAct = mysqli_query($linkbd,$sqlLecturaAct);
									$rowLecturaAct = mysqli_fetch_row($resLecturaAct);

									if ($rowLecturaAct[0] != '') {
										$lecturaAct = $rowLecturaAct[0];
									} 

									$consumo = 0;

									$sqlConsumo = "SELECT consumo FROM srvlectura WHERE corte = $_POST[corte] AND id_cliente = $rowUsuariosCorte[0] AND id_servicio = '$rowServicios[0]'";
									$resConsumo = mysqli_query($linkbd,$sqlConsumo);
									$rowConsumo = mysqli_fetch_row($resConsumo);

									if ($rowConsumo[0] != '') {
										$consumo = $rowConsumo[0];
									} 

									$sqlFacturas = "SELECT cargo_f, consumo_b, consumo_c, consumo_s, subsidio_cf, subsidio_cb, subsidio_cc, subsidio_cs, contribucion_cf, contribucion_cb, contribucion_cc, contribucion_cs, deuda_anterior, abonos, acuerdo_pago, venta_medidor, interes_mora, alumbrado, estado FROM srvfacturas WHERE cliente = $rowUsuariosCorte[0] AND corte = $_POST[corte] AND id_servicio = $rowServicios[0]";
									$resFacturas = mysqli_query($linkbd,$sqlFacturas);
									$rowFacturas = mysqli_fetch_row($resFacturas);

									$cf = 0;
									$cb = 0;
									$cc = 0;
									$cs = 0;
									$sb_cf = 0;
									$sb_cb = 0;
									$sb_cc = 0;
									$sb_cs = 0;
									$cb_cf = 0;
									$cb_cb = 0;
									$cb_cc = 0;
									$cb_cs = 0;
									$deuda_anterior = 0;
									$abonos = 0;
									$acuero_p = 0;
									$venta_m = 0;
									$interes_m = 0;
									$alumbrado = 0;

									if($rowServicios[0] == 1)
									{
										$cf = 0;
										$cb = number_format($rowFacturas[1], 2, '.', '');
										$cc = number_format($rowFacturas[2], 2, '.', '');
										$cs = number_format($rowFacturas[3], 2, '.', '');
										$sb_cf = 0;
										$sb_cb = number_format($rowFacturas[5], 2, '.', '');
										$sb_cc = number_format($rowFacturas[6], 2, '.', '');
										$sb_cs = 0;
										$cb_cf = 0;
										$cb_cb = 0;
										$cb_cc = 0;
										$cb_cs = 0;
										$deuda_anterior = number_format($rowFacturas[12], 2, '.', '');
										$abonos = number_format($rowFacturas[13], 2, '.', '');
										$acuero_p = number_format($rowFacturas[14], 2, '.', '');
										$venta_m = number_format($rowFacturas[15], 2, '.', '');
										$interes_m = number_format($rowFacturas[16], 2, '.', '');
										$alumbrado = number_format($rowFacturas[17], 2, '.', '');
									}

									if($rowServicios[0] == 2)
									{
										$cf = number_format($rowFacturas[0], 2, '.', '');
										$cb = number_format($rowFacturas[1], 2, '.', '');
										$cc = number_format($rowFacturas[2], 2, '.', '');
										$cs = number_format($rowFacturas[3], 2, '.', '');
										$sb_cf = number_format($rowFacturas[4], 2, '.', '');
										$sb_cb = number_format($rowFacturas[5], 2, '.', '');
										$sb_cc = 0;
										$sb_cs = 0;
										$cb_cf = number_format($rowFacturas[8], 2, '.', '');
										$cb_cb = $rowFacturas[9] + $rowFacturas[10] + $rowFacturas[11];
										$cb_cb = number_format($cb_cb, 2, '.', '');
										$cb_cc = 0;
										$cb_cs = 0;
										$deuda_anterior = number_format($rowFacturas[12], 2, '.', '');
										$abonos = number_format($rowFacturas[13], 2, '.', '');
										$acuero_p = number_format($rowFacturas[14], 2, '.', '');
										$venta_m = number_format($rowFacturas[15], 2, '.', '');
										$interes_m = number_format($rowFacturas[16], 2, '.', '');
										$alumbrado = 0;
									}

									if($rowServicios[0] == 3)
									{	
										$valor = 0;

										$sqlcomponentesTarifarios = "SELECT TBL, TRT, TDT, TFR FROM srvcomponentes_tarifarios_aseo";
										$rescomponentesTarifarios = mysqli_query($linkbd,$sqlcomponentesTarifarios);
										$rowcomponentesTarifarios = mysqli_fetch_row($rescomponentesTarifarios);
										
										$TBL = $rowcomponentesTarifarios[0] / 100;
										$TRT = $rowcomponentesTarifarios[1] / 100;
										$TDT = $rowcomponentesTarifarios[2] / 100;
										$TFR = $rowcomponentesTarifarios[3] / 100;

										$valor = $rowFacturas[0];
										$cf = $valor * $TBL;
										$cb = $valor * $TRT;
										$cc = $valor * $TDT;
										$cs = $valor * $TFR;

										$cf = number_format($cf, 2, '.', '');
										$cb = number_format($cb, 2, '.', '');
										$cc = number_format($cc, 2, '.', '');
										$cs = number_format($cs, 2, '.', '');

										$sb_cf = number_format($rowFacturas[4], 2, '.', '');
										$sb_cb = 0;
										$sb_cc = 0;
										$sb_cs = 0;
										$cb_cf = number_format($rowFacturas[8], 2, '.', '');
										$cb_cb = 0;
										$cb_cc = 0;
										$cb_cs = 0;
										$deuda_anterior = number_format($rowFacturas[12], 2, '.', '');
										$abonos = number_format($rowFacturas[13], 2, '.', '');
										$acuero_p = number_format($rowFacturas[14], 2, '.', '');
										$venta_m = 0;
										$interes_m = number_format($rowFacturas[16], 2, '.', '');
										$alumbrado = 0;
									}
									
									$total = $cf + $cb + $cc + $cs + $cb_cf + $cb_cb + $cb_cc + $cb_cs + $deuda_anterior + $acuero_p + $venta_m + $interes_m + $alumbrado - $sb_cf - $sb_cb - $sb_cc - $sb_cs - $abonos;

									$conta = 0;

									echo "
										<input type='hidden' name='estadoCF[$cont][]' value='$estadoCF'>
										<input type='hidden' name='estadoCS[$cont][]' value='$estadoCS'>
										<input type='hidden' name='medidor[$cont][]' value='$medidor'>
										<input type='hidden' name='lecturaAnt[$cont][]' value='$lecturaAnt'>
										<input type='hidden' name='lecturaAct[$cont][]' value='$lecturaAct'>
										<input type='hidden' name='consumo[$cont][]' value='$consumo'>
										<input type='hidden' name='cf[$cont][]' value='$cf'>
										<input type='hidden' name='cb[$cont][]' value='$cb'>
										<input type='hidden' name='cc[$cont][]' value='$cc'>
										<input type='hidden' name='cs[$cont][]' value='$cs'>
										<input type='hidden' name='alumbrado[$cont][]' value='$alumbrado'>
										<input type='hidden' name='sb_cf[$cont][]' value='$sb_cf'>
										<input type='hidden' name='sb_cb[$cont][]' value='$sb_cb'>
										<input type='hidden' name='sb_cc[$cont][]' value='$sb_cc'>
										<input type='hidden' name='sb_cs[$cont][]' value='$sb_cs'>
										<input type='hidden' name='cb_cf[$cont][]' value='$cb_cf'>
										<input type='hidden' name='cb_cb[$cont][]' value='$cb_cb'>
										<input type='hidden' name='cb_cc[$cont][]' value='$cb_cc'>
										<input type='hidden' name='cb_cs[$cont][]' value='$cb_cs'>
										<input type='hidden' name='deuda_anterior[$cont][]' value='$deuda_anterior'>
										<input type='hidden' name='abonos[$cont][]' value='$abonos'>
										<input type='hidden' name='acuero_p[$cont][]' value='$acuero_p'>
										<input type='hidden' name='venta_m[$cont][]' value='$venta_m'>
										<input type='hidden' name='interes_m[$cont][]' value='$interes_m'>
										<input type='hidden' name='servicio[$cont][]' value='$servicio'>
										<input type='hidden' name='estrato[$cont][]' value='$estrato'>
									";
									
									if($rowServicios[0] == 1)
									{
								?>
										<td><?php echo $servicio ?></td>
										<td><?php echo $estrato ?></td>
										<td><?php echo $estadoCF ?></td>
										<td><?php echo $estadoCS ?></td>
										<td><?php echo $medidor ?></td>
										<td><?php echo $lecturaAnt ?></td>
										<td><?php echo $lecturaAct ?></td>
										<td><?php echo $consumo ?></td>
										<td><?php echo $cb ?></td>
										<td><?php echo $cc ?></td>
										<td><?php echo $cs ?></td>
										<td><?php echo $alumbrado ?></td>
										<td><?php echo $sb_cb ?></td>
										<td><?php echo $sb_cc ?></td>
										<td><?php echo $deuda_anterior ?></td>
										<td><?php echo $abonos ?></td>
										<td><?php echo $acuero_p ?></td>
										<td><?php echo $venta_m ?></td>
										<td><?php echo $interes_m ?></td>
								<?php
									}

									if($rowServicios[0] == 2)
									{
								?>	
										<td><?php echo $servicio ?></td>
										<td><?php echo $estrato ?></td>
										<td><?php echo $estadoCF ?></td>
										<td><?php echo $estadoCS ?></td>
										<td><?php echo $medidor ?></td>
										<td><?php echo $lecturaAnt ?></td>
										<td><?php echo $lecturaAct ?></td>
										<td><?php echo $consumo ?></td>
										<td><?php echo $cf ?></td>
										<td><?php echo $cb ?></td>
										<td><?php echo $cc ?></td>
										<td><?php echo $cs ?></td>
										<td><?php echo $sb_cf ?></td>
										<td><?php echo $sb_cb ?></td>
										<td><?php echo $cb_cf ?></td>
										<td><?php echo $cb_cb ?></td>
										<td><?php echo $deuda_anterior ?></td>
										<td><?php echo $abonos ?></td>
										<td><?php echo $acuero_p ?></td>
										<td><?php echo $venta_m ?></td>
										<td><?php echo $interes_m ?></td>
								<?php
									}

									if ($rowServicios[0] == 3)
									{
								?>	
										<td><?php echo $servicio ?></td>
										<td><?php echo $estrato ?></td>
										<td><?php echo $estadoCF ?></td>
										<td><?php echo $estadoCS ?></td>
										<td><?php echo $cf ?></td>
										<td><?php echo $cb ?></td>
										<td><?php echo $cc ?></td>
										<td><?php echo $cs ?></td>
										<td><?php echo $sb_cf ?></td>
										<td><?php echo $cb_cf ?></td>
										<td><?php echo $deuda_anterior ?></td>
										<td><?php echo $abonos ?></td>
										<td><?php echo $acuero_p ?></td>
										<td><?php echo $interes_m ?></td>
								<?php
									}
								}

								$total = consultaValorTotalSRVFACTURAS($numeroFactura);
								$total = ($total / 100);
								$total = ceil($total);
								$total = $total * 100;	
								$total = number_format($total, 2, '.', '');

								echo "
									<input type='hidden' name='total[]' value='$total'>
								";
					?>
									<td><?php echo $total ?></td>
								</tr>
					<?php
								$aux=$iter;
								$iter=$iter2;
								$iter2=$aux;
								$conta++;
								$cont++;
							}
								
							if ($encontrados == 0)
							{
					?>
								<table class='inicio'>
									<tr>
										<td class='saludo1' style='text-align:center;'>
											<img src='imagenes\alert.png' style='width:25px; height: 30px;'> No se han encontrado registros <img src='imagenes\alert.png' style='width:25px; height: 30px;'>
										</td>
									</tr>
								</table>
					<?php
							}
						}
					?>
				</table>
			</div>

			<input type="hidden" name="oculto" id="oculto" value="1"/>
		</form>
	</body>
</html>
