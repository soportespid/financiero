<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require "comun.inc";
	require "funciones.inc";
	setlocale(LC_ALL,"es_ES");
	session_start();
	$linkbd = conectar_v7();	
	$linkbd -> set_charset("utf8");		
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html lang="es">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1">
		<meta name="viewport" content="user-scalable=no">
		<title>:: IDEAL 10 - Tesorer&iacute;a</title>
		<link href="favicon.ico" rel="shortcut icon"/>
		<link href="css/css2.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet"/>
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet"/>
		<link href="css/tabs.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet"/>
		<script type="text/javascript" src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script type="text/javascript" src="css/calendario.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script type="text/javascript" src="JQuery/jquery-2.1.4.min.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script>
			function verUltimaPos(idcta, filas, filtro1, filtro2, filtro3, filtro4){
				var scrtop=$('#divdet').scrollTop();
				var altura=$('#divdet').height();
				var numpag=$('#nummul').val();
				var limreg=$('#numres').val();
				if((numpag<=0)||(numpag=="")){numpag=0;}
				if((limreg==0)||(limreg=="")){limreg=10;}
				numpag++;
				location.href="teso-predialver.php?idpredial="+idcta+"&scrtop="+scrtop+"&totreg="+filas+"&altura="+altura+"&numpag="+numpag+"&limreg="+limreg+"&filtro1="+filtro1+"&filtro2="+filtro2+"&filtro3="+filtro3+"&filtro4="+filtro4;
			}
			function eliminar(idr){
				if (confirm("Esta Seguro de Eliminar")){
					document.form2.oculto.value=2;
					document.form2.var1.value=idr;
					document.form2.submit();
				}
			}
			function crearexcel(){
				document.form2.action="teso-buscapredialexcel.php";
				document.form2.target="_BLANK";
				document.form2.submit();
				document.form2.action="";
				document.form2.target="";
			}
			function funordenar(var01){
				if(document.getElementById(''+var01).value==0){document.getElementById(''+var01).value=1;}
				else if(document.getElementById(''+var01).value==1) {document.getElementById(''+var01).value=2;}
				else{document.getElementById(''+var01).value=0;}
				document.form2.submit();
			}
			function buscarbotonfiltro(){
				if((document.form2.fecha.value != "" && document.form2.fecha2.value == "") || (document.form2.fecha.value == "" && document.form2.fecha2.value != "")){
					alert("Falta digitar fecha");
				}else{
					document.getElementById('numpos').value=0;
					document.getElementById('nummul').value=0;
					document.form2.submit();
				}
			}
		</script>
	</head>
	<body>
		<IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
		<span id="todastablas2"></span>
		<table>
			<tr><script>barra_imagenes("teso");</script><?php cuadro_titulos();?></tr>	 
			<tr><?php menu_desplegable("teso");?></tr>
			<tr>
				<td colspan="3" class="cinta">
					<a onClick="location.href='teso-predial.php'" class="mgbt"><img src="imagenes/add.png" title="Nuevo"/></a>
					<a class="mgbt1"><img src="imagenes/guardad.png"/></a>
					<a class="mgbt1"><img src="imagenes/buscad.png"/></a>
					<a onClick="mypop=window.open('plan-agenda.php','','');mypop.focus()" class="mgbt"><img src="imagenes/agenda1.png" title="Agenda" /></a>
					<a onClick="<?php echo paginasnuevas("teso");?>" class="mgbt"><img src="imagenes/nv.png" title="Nueva ventana"/></a>
					<a onClick="mypop=window.open('<?php echo $url2; ?>','','');mypop.focus();" class="tooltip bottom mgbt"><img src="imagenes/duplicar_pantalla.png" title="Duplicar pesta&ntilde;a"></a>
					<a onclick="crearexcel()" class="mgbt"><img src="imagenes/excel.png" title="Exportar Excel"/></a>
				</td>
			</tr>	
		</table>
		<form name="form2" method="post" action="">
			<?php 
				if($_GET['numpag']!=""){
					$oculto=$_POST['oculto'];
					if($oculto!=2){
					$_POST['numres']=$_GET['limreg'];
					$_POST['numpos']=$_GET['limreg']*($_GET['numpag']-1);
					$_POST['nummul']=$_GET['numpag']-1;
					}
				}else{
					if($_POST['nummul']==""){
						$_POST['numres']=10;
						$_POST['numpos']=0;
						$_POST['nummul']=0;
					}
				}
				if($_POST['bandera'] == ""){
					if(isset($_GET['filtro1'])){
						$_POST['nLiquid'] = $_GET['filtro1'];
					}
					if(isset($_GET['filtro2'])){
						$_POST['codCatas'] = $_GET['filtro2'];
					}
					if(isset($_GET['filtro3'])){  
						$_POST['fecha'] = $_GET['filtro3'];
					}
					if(isset($_GET['filtro4'])){
						$_POST['fecha2']=$_GET['filtro4'];
					}
					$_POST['bandera'] = 1;
					echo "<script>document.form2.bandera.value = 1; 
					document.form2.submit();</script>";
				}
				preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST['fecha'],$fech1);
				preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST['fecha2'],$fech2);
				$f1=$fech1[3]."-".$fech1[2]."-".$fech1[1];
				$f2=$fech2[3]."-".$fech2[2]."-".$fech2[1];
				$scrtop=$_GET['scrtop'];
				if($scrtop=="") $scrtop=0;
				echo"<script>
					window.onload=function(){
						$('#divdet').scrollTop(".$scrtop.")
					}
				</script>";
				$gidcta=$_GET['idcta'];
			?>   
			<input type="hidden" name="bandera" id="bandera" value="1"/>
			<input type="hidden" name="numres" id="numres" value="<?php echo $_POST['numres'];?>"/>
			<input type="hidden" name="numpos" id="numpos" value="<?php echo $_POST['numpos'];?>"/>
			<input type="hidden" name="nummul" id="nummul" value="<?php echo $_POST['nummul'];?>"/>
			<table  class="inicio" align="center" >
				<tr>
					<td class="titulos" colspan="13">:. Buscar Liquidacion Predial</td>
					<td class="cerrar" style="width:2cm;"><a onClick="location.href='teso-principal.php'">&nbsp;Cerrar</a></td>
				</tr>
				<tr >
					<td class="saludo1" style="width:3.5cm;" >No Liquidaci&oacute;n: </td>
					<td style="width:20%;" ><input type="serch" name="nLiquid" id="nLiquid" value="<?php echo $_POST['nLiquid'];?>" style="width:98%;" ></td>

					<td class="saludo1" style="width:3.5cm;">C&oacute;digo Catastral: </td>
					<td><input type="serch" name="codCatas" id="codCatas" value="<?php echo $_POST['codCatas']?>"  style="width:98%;"></td>

					<td class="saludo1" style="width:2.2cm;">Fecha inicial:</td>
					<td style="width:9%;"><input name="fecha"  type="text" value="<?php echo $_POST['fecha']?>" maxlength="10"  onKeyPress="javascript:return solonumeros(event)" onKeyUp="return tabular(event,this)" onchange="" id="fc_1198971545" onKeyDown="mascara(this,'/',patron,true)" style="width:75%;height:30px;" title="DD/MM/YYYY" placeholder="DD/MM/YYYY"/>&nbsp;<a href="#" onClick="displayCalendarFor('fc_1198971545');" title="Calendario"><img src="imagenes/calendario04.png" title="Calendario" class="icobut"/></a></td>
					<td class="saludo1" style="width:2.2cm;">Fecha final:</td>        			
					<td style="width:9%;"><input name="fecha2" type="text" value="<?php echo $_POST['fecha2']?>" maxlength="10" onchange="" onKeyPress="javascript:return solonumeros(event)" onKeyUp="return tabular(event,this)"  id="fc_1198971546" onKeyDown="mascara(this,'/',patron,true)" style="width:75%;height:30px;" title="DD/MM/YYYY" placeholder="DD/MM/YYYY"/>&nbsp;<a href="#" onClick="displayCalendarFor('fc_1198971546');" title="Calendario"><img src="imagenes/calendario04.png" title="Calendario" class="icobut"/></a></td>
					<td style="padding-bottom:0px"><em class="botonflecha" onClick="buscarbotonfiltro();">Buscar</em></td>
				</tr>                       
			</table>    
			<input type="hidden" name="oculto" id="oculto"  value = '1'  >
			<input type="hidden" name="var1" id="var1"  value="<?php echo $_POST['var1'] ?>" />
			<div class="subpantallac5" style="height:68.5%; width:99.6%; overflow-x:hidden;" id="divdet">
				<?php
					if($_POST['oculto']==2){
						$sqlr="UPDATE tesoliquidapredial SET estado='N' WHERE idpredial=$_POST[var1]";
						mysqli_query($linkbd,$sqlr);
						$sqlr="UPDATE tesoliquidapredial_det SET estado='N' WHERE idpredial=$_POST[var1]";
						mysqli_query($linkbd,$sqlr);
					}
					$crit1 = "";
					$crit2 = "";
					if ($_POST['nLiquid'] != "")
					{
						$crit1="AND idpredial LIKE '$_POST[nLiquid]'";
					}
					if ($_POST['codCatas'] != "")
					{
						$crit2="AND codigocatastral LIKE '$_POST[codCatas]'";
					}
					//sacar el consecutivo
					$sqlr="SELECT * FROM tesoliquidapredial WHERE tesoliquidapredial.idpredial>-1 $crit1 $crit2";
					if(isset($_POST['fecha']) && isset($_POST['fecha2'])){
						if(!empty($_POST['fecha']) && !empty($_POST['fecha2'])){
							$sqlr="SELECT * FROM tesoliquidapredial WHERE tesoliquidapredial.idpredial>-1 $crit1 $crit2 AND fecha BETWEEN '$f1' AND '$f2' ORDER BY idpredial DESC";
						}
					}
					$resp = mysqli_query($linkbd,$sqlr);
					$ntr = mysqli_num_rows($resp);
					$_POST['numtop']= $ntr;
					$nuncilumnas=ceil($_POST['numtop']/$_POST['numres']);
					$cond2="";
					if ($_POST['numres'] != "-1"){ $cond2="LIMIT $_POST[numpos], $_POST[numres]";}
					$sqlr="SELECT * FROM tesoliquidapredial WHERE tesoliquidapredial.idpredial>-1 $crit1 $crit2 ORDER BY idpredial DESC $cond2 ";
					if(isset($_POST['fecha']) && isset($_POST['fecha2'])){
						if(!empty($_POST['fecha']) && !empty($_POST['fecha2'])){
							$sqlr="SELECT * FROM tesoliquidapredial WHERE tesoliquidapredial.idpredial>-1 $crit1 $crit2 AND fecha BETWEEN '$f1' AND '$f2' ORDER BY idpredial DESC $cond2";
						}
					}
					$resp = mysqli_query($linkbd,$sqlr);
					$numcontrol=$_POST['nummul']+1;
					if(($nuncilumnas==$numcontrol)||($_POST['numres']=="-1")){
						$imagenforward="<img src='imagenes/forward02.png' style='width:17px'>";
						$imagensforward="<img src='imagenes/skip_forward02.png' style='width:16px' >";
					}else{
						$imagenforward="<img src='imagenes/forward01.png' style='width:17px' title='Siguiente' onClick='numsiguiente()'>";
						$imagensforward="<img src='imagenes/skip_forward01.png' style='width:16px' title='Fin' onClick='saltocol(\"$nuncilumnas\")'>";
					}
					if(($_POST['numpos']==0)||($_POST['numres']=="-1")){
						$imagenback="<img src='imagenes/back02.png' style='width:17px'>";
						$imagensback="<img src='imagenes/skip_back02.png' style='width:16px'>";
					}else{
						$imagenback="<img src='imagenes/back01.png' style='width:17px' title='Anterior' onClick='numanterior();'>";
						$imagensback="<img src='imagenes/skip_back01.png' style='width:16px' title='Inicio' onClick='saltocol(\"1\")'>";
					}
					$con=1;
					echo "
					<table class='inicio' align='center'>
						<tr>
							<td colspan='7' class='titulos'>.: Resultados Busqueda:</td>
							<td class='submenu'>
								<select name='renumres' id='renumres' onChange='cambionum();' style='width:100%'>
									<option value='10'"; if ($_POST['renumres']=='10'){echo 'selected';} echo ">10</option>
									<option value='20'"; if ($_POST['renumres']=='20'){echo 'selected';} echo ">20</option>
									<option value='30'"; if ($_POST['renumres']=='30'){echo 'selected';} echo ">30</option>
									<option value='50'"; if ($_POST['renumres']=='50'){echo 'selected';} echo ">50</option>
									<option value='100'"; if ($_POST['renumres']=='100'){echo 'selected';} echo ">100</option>
									<option value='-1'"; if ($_POST['renumres']=='-1'){echo 'selected';} echo ">Todos</option>
								</select>
							</td>
						</tr>
						<tr>
							<td colspan='8' class='saludo3' id='RecEnc'>Recaudos Encontrados: $ntr2</td>
						</tr>
						<tr>
							<td width='150' class='titulos2'>No Liquidacion</td>
							<td class='titulos2'>Codigo Catastral</td>
							<td class='titulos2'>Fecha</td>
							<td class='titulos2'>Contribuyente</td>
							<td class='titulos2'>Valor</td>
							<td class='titulos2'>Estado</td>
							<td class='titulos2' width='5%'><center>Anular</td>
							<td class='titulos2' width='5%'><center>Ver</td>
						</tr>";	
					$iter='saludo1a';
					$iter2='saludo2';
					$filas=1;
					if($_POST['fecha'] == '' && $_POST['fecha2'] == '' && $_POST['nLiquid'] == '' && $_POST['codCatas'] == ''){
						echo "
						<table class='inicio'>
							<tr>
								<td class='saludo1' style='text-align:center;width:100%;font-size:25px'>Utilice el filtro de busqueda</td>
							</tr>
						</table>";
						$nuncilumnas = 0;
					}elseif(mysqli_num_rows($resp) == 0 || mysqli_num_rows($resp) == '0'){
						echo "
						<table class='inicio'>
							<tr>
								<td class='saludo1' style='text-align:center;width:100%;font-size:25px'>No hay resultados de su busqueda.</td>
							</tr>
						</table>";
					}else{
						while ($row = mysqli_fetch_row($resp)){
							$ntr2 = $ntr;
							echo "<script>document.getElementById('RecEnc').innerHTML = 'Recaudos Encontrados: $ntr2'</script>";
							$sqlr="select sum(totaliquidavig) from  tesoliquidapredial_det where idpredial=$row[0]";
							$resp2=mysqli_query($linkbd,$sqlr);
							$r2=mysqli_fetch_row($resp2);
							$nter=buscatercero($row[4]);
							if($gidcta!=""){
								if($gidcta==$row[0]){$estilo='background-color:yellow';}
								else {$estilo="";}
							}else{
								$estilo="";
							}	
							$idcta="'$row[0]'";
							$numfil="'$filas'";
							$filtro1 = "'$_POST[nLiquid]'";
							$filtro2 = "'$_POST[codCatas]'";
							$filtro3 = "'$_POST[fecha]'";
							$filtro4 = "'$_POST[fecha2]'";
							echo"
								<input type='hidden' name='nliqui[]' value='".$row[0]."'>
								<input type='hidden' name='codCatastral[]' value='".$row[1]."'>
								<input type='hidden' name='fechat[]' value='".$row[2]."'>
								<input type='hidden' name='numContribuyente[]' value='".$row[4]."'>
								<input type='hidden' name='nomContribuyente[]' value='".$nter."'>
								<input type='hidden' name='valor[]' value='".$r2[0]."'>
							";
							if ($row[18]=='S'){
								echo"
								<input type='hidden' name='estado[]' value='ACTIVO'>";
							}
							if ($row[18]=='N'){
								echo"
								<input type='hidden' name='estado[]' value='ANULADO'>";
							}
							if ($row[18]=='P'){
								echo"
								<input type='hidden' name='estado[]' value='PAGO'>";
							}
							echo"               
								<tr class='$iter' onDblClick=\"verUltimaPos($idcta, $numfil, $filtro1, $filtro2, $filtro3, $filtro4)\" style='text-transform:uppercase; $estilo' >
									<td >$row[0]</td>
									<td >$row[1]</td>
									<td>$row[2]</td>
									<td>$row[4] - $nter</td>
									<td >".number_format($r2[0],2,",",".")."</td>";
							if ($row[18]=='S')
							echo "
									<td><center><img src='imagenes/confirm.png'></center></td>
									<td><a href='#' onClick=eliminar($row[0])><center><img src='imagenes/borrar01.png' style='Width:19px;' title='Anular'></center></a></td>";
							if ($row[18]=='N')
							echo "
									<td><center><img src='imagenes/cross.png'></center></td>
									<td></td>";
							if ($row[18]=='P')
							echo "
									<td><center><img src='imagenes/dinero3.png'></center></td>
									<td></td>";
							echo"
									<td style='text-align:center;'><a onClick=\"verUltimaPos($idcta, $numfil, $filtro1, $filtro2, $filtro3, $filtro4)\" style='cursor:pointer;'><img src='imagenes/lupa02.png' style='width:18px' title='Ver'></a></td>
								</tr>";
							$con+=1;
							$aux=$iter;
							$iter=$iter2;
							$iter2=$aux;
							$filas++;
						}
					}
					echo"</table>
					<table class='inicio'>
						<tr>
							<td style='text-align:center;'>
								<a href='#'>$imagensback</a>&nbsp;
								<a href='#'>$imagenback</a>&nbsp;&nbsp;";
								if($nuncilumnas<=9){$numfin=$nuncilumnas;}
								else{$numfin=9;}
								for($xx = 1; $xx <= $numfin; $xx++)
								{
									if($numcontrol<=9){$numx=$xx;}
									else{$numx=$xx+($numcontrol-9);}
									if($numcontrol==$numx){echo"<a href='#' onClick='saltocol(\"$numx\")'; style='color:#24D915'> $numx </a>";}
									else {echo"<a href='#' onClick='saltocol(\"$numx\")'; style='color:#000000'> $numx </a>";}
								}
								echo"&nbsp;&nbsp;<a href='#'>$imagenforward</a>
									&nbsp;<a href='#'>$imagensforward</a>
							</td>
						</tr>
					</table>";
				?>
			</div>
		</form> 
	</body>
</html>