<?php

    header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require 'comun.inc';
	require 'funciones.inc';
	session_start();
    if(empty($_SESSION)){
        header("location: index.php");
    }
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html lang=es>
	<head>
    <meta charset="utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1"/>
		<meta name="viewport" content="user-scalable=no">
		<title>:: IDEAL 10 - Contabilidad</title>
		<link href="favicon.ico" rel="shortcut icon"/>
		<link href="css/css2z.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
        <link href="css/tabs.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
        <link href="css/style.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" href="sweetalert2/dist/sweetalert2.min.css">
        <script type="text/javascript" src="css/programas.js"></script>
        <script src="sweetalert2/dist/sweetalert2.min.js"></script>
		<script src="vue/vue.min.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script src="css/calendario.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
	</head>
	<body>
		<header>
			<table>
				<tr><script>barra_imagenes("cont");</script><?php cuadro_titulos();?></tr>
			</table>
		</header>

		<form name="form2" method="post" action="">
			<section id="myapp" v-cloak >
                <div class="loading-container" v-show="isLoading" >
                    <p class="text-loading"data-loading="IDEAL 10">IDEAL 10</p>
                </div>
				<nav>
                    <table>
						<tr><?php menu_desplegable("cont");?></tr>
					</table>
                    <div class="bg-white group-btn p-1">
                        <button type="button" onclick="mypop=window.open('cont-principal.php','','');mypop.focus();" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
                            <span>Nueva ventana</span>
                            <svg class="fill-black group-hover:fill-white w-5 h-5" xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960"><path d="M320-120v-80h80v-80H160q-33 0-56.5-23.5T80-360v-400q0-33 23.5-56.5T160-840h640q33 0 56.5 23.5T880-760v400q0 33-23.5 56.5T800-280H560v80h80v80H320ZM160-360h640v-400H160v400Zm0 0v-400 400Z"></path></svg>
                        </button>
                        <button type="button" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center" @click="mypop=window.open('cont-balanceprueba-nicsp','','');mypop.focus();">
                            <span class="group-hover:text-white">Duplicar pantalla</span>
                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960"><path d="M640-160v-360H160v360h480Zm80-200v-80h80v-360H320v200h-80v-200q0-33 23.5-56.5T320-880h480q33 0 56.5 23.5T880-800v360q0 33-23.5 56.5T800-360h-80ZM160-80q-33 0-56.5-23.5T80-160v-360q0-33 23.5-56.5T160-600h480q33 0 56.5 23.5T720-520v360q0 33-23.5 56.5T640-80H160Zm400-603ZM400-340Z"></path></svg>
                        </button>
                        <button type="button" @click="exportData(1)" class="btn btn-danger btn-danger-hover d-flex justify-between align-items-center">
                            <span>Exportar PDF</span>
                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><!-- !Font Awesome Free 6.5.1 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license/free Copyright 2024 Fonticons, Inc. --><path d="M64 464l48 0 0 48-48 0c-35.3 0-64-28.7-64-64L0 64C0 28.7 28.7 0 64 0L229.5 0c17 0 33.3 6.7 45.3 18.7l90.5 90.5c12 12 18.7 28.3 18.7 45.3L384 304l-48 0 0-144-80 0c-17.7 0-32-14.3-32-32l0-80L64 48c-8.8 0-16 7.2-16 16l0 384c0 8.8 7.2 16 16 16zM176 352l32 0c30.9 0 56 25.1 56 56s-25.1 56-56 56l-16 0 0 32c0 8.8-7.2 16-16 16s-16-7.2-16-16l0-48 0-80c0-8.8 7.2-16 16-16zm32 80c13.3 0 24-10.7 24-24s-10.7-24-24-24l-16 0 0 48 16 0zm96-80l32 0c26.5 0 48 21.5 48 48l0 64c0 26.5-21.5 48-48 48l-32 0c-8.8 0-16-7.2-16-16l0-128c0-8.8 7.2-16 16-16zm32 128c8.8 0 16-7.2 16-16l0-64c0-8.8-7.2-16-16-16l-16 0 0 96 16 0zm80-112c0-8.8 7.2-16 16-16l48 0c8.8 0 16 7.2 16 16s-7.2 16-16 16l-32 0 0 32 32 0c8.8 0 16 7.2 16 16s-7.2 16-16 16l-32 0 0 48c0 8.8-7.2 16-16 16s-16-7.2-16-16l0-64 0-64z"></path></svg>
                        </button>
                        <button type="button" @click="exportData(2)" class="btn btn-success btn-success-hover d-flex justify-between align-items-center">
                            <span>Exportar Excel</span>
                            <svg class="fill-black group-hover:fill-white w-4 h-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 384 512"><!--!Font Awesome Free 6.5.1 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license/free Copyright 2024 Fonticons, Inc. --><path d="M48 448V64c0-8.8 7.2-16 16-16H224v80c0 17.7 14.3 32 32 32h80V448c0 8.8-7.2 16-16 16H64c-8.8 0-16-7.2-16-16zM64 0C28.7 0 0 28.7 0 64V448c0 35.3 28.7 64 64 64H320c35.3 0 64-28.7 64-64V154.5c0-17-6.7-33.3-18.7-45.3L274.7 18.7C262.7 6.7 246.5 0 229.5 0H64zm90.9 233.3c-8.1-10.5-23.2-12.3-33.7-4.2s-12.3 23.2-4.2 33.7L161.6 320l-44.5 57.3c-8.1 10.5-6.3 25.5 4.2 33.7s25.5 6.3 33.7-4.2L192 359.1l37.1 47.6c8.1 10.5 23.2 12.3 33.7 4.2s12.3-23.2 4.2-33.7L222.4 320l44.5-57.3c8.1-10.5 6.3-25.5-4.2-33.7s-25.5-6.3-33.7 4.2L192 280.9l-37.1-47.6z"></path></svg>
                        </button>
                    </div>
				</nav>
				<article>
                    <div  class="bg-white">
                        <div>
                            <h2 class="titulos m-0">Balance de prueba NICSP</h2>
                            <div class="d-flex w-100">
                                <div class="w-50 d-flex">
                                    <div class="form-control">
                                        <label class="form-label m-0 w-50" for="">Nivel:</label>
                                        <select v-model="selectNivel">
                                            <option v-for="(data,index) in arrNiveles" :key="index" :value="data.posiciones">{{data.id_nivel}}</option>
                                        </select>
                                    </div>
                                    <div class="form-control">
                                        <label class="form-label m-0 w-50" for="">Fecha inicial:</label>
                                        <input type="date" v-model="txtFechaInicial">
                                    </div>
                                    <div class="form-control">
                                        <label class="form-label m-0 w-50" for="">Fecha final:</label>
                                        <input type="date" v-model="txtFechaFinal">
                                    </div>
                                </div>
                                <div class="w-50 d-flex">
                                    <div class="form-control" >
                                        <label for="labelCheckAgregado" class="me-2">Agregado: </label>
                                        <div class="d-flex align-items-center ">
                                            <label for="labelCheckAgregado" class="form-switch">
                                                <input type="checkbox" id="labelCheckAgregado" v-model="checkAgregado" @change="changeChecks(1)">
                                                <span></span>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="form-control">
                                        <label for="labelCheckCierre" class="me-2">Cierre: </label>
                                        <div class="d-flex align-items-center ">
                                            <label for="labelCheckCierre" class="form-switch">
                                                <input type="checkbox" id="labelCheckCierre" v-model="checkCierre">
                                                <span></span>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="form-control" >
                                        <label for="labelCheckExterno" class="me-2">Externo: </label>
                                        <div class="d-flex align-items-center ">
                                            <label for="labelCheckExterno" class="form-switch">
                                                <input type="checkbox" id="labelCheckExterno" v-model="checkExterno" @change="changeChecks(2)">
                                                <span></span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="d-flex">
                                <div class="w-50 d-flex">
                                    <div class="form-control">
                                        <label class="form-label m-0" for="">Cuenta inicial:</label>
                                        <div class="d-flex w-100">
                                            <input type="text" class="bg-warning w-25 cursor-pointer" @dblclick="isModal=true;typeCuenta=1" v-model="objCuentaInicial.codigo" @change="search('cod_cuenta')">
                                            <input type="text" disabled v-model="objCuentaInicial.nombre">
                                        </div>
                                    </div>
                                    <div class="form-control">
                                        <label class="form-label m-0" for="">Cuenta final:</label>
                                        <div class="d-flex w-100">
                                            <input type="text" class="bg-warning w-25 cursor-pointer" @dblclick="isModal=true;typeCuenta=2" v-model="objCuentaFinal.codigo" @change="search('cod_cuenta_final')">
                                            <input type="text" disabled v-model="objCuentaFinal.nombre">
                                        </div>
                                    </div>
                                </div>
                                <div class="w-50 d-flex">
                                    <div class="form-control w-100" >
                                        <label class="form-label m-0 w-50" for="">Centro costo:</label>
                                        <div class="select-wrapper" v-if="checkCierre && !checkAgregado && !checkExterno || !checkExterno && !checkAgregado">
                                            <div class="select-multiple" id="selectMultiple1" onclick="showMultipleSelect(this)"> Seleccione</div>
                                            <ul class="select-multiple-content d-none">
                                                <li >
                                                    <label for="multipleCheckAll1" >
                                                        <input type="checkbox"  value="all" onchange="getCheckMultipleSelect(this,true)" id="multipleCheckAll1">
                                                        Todos
                                                    </label>
                                                </li>
                                                <li  v-for="(data,index) in arrCentros" :key="index">
                                                    <label :for="'multipleCheck'+index" >
                                                        <input type="checkbox"  onchange="getCheckMultipleSelect(this)" :value="data.codigo" :id="'multipleCheck'+index">
                                                        {{data.codigo+" - "+data.nombre}}
                                                    </label>
                                                </li>
                                            </ul>
                                        </div>
                                        <select v-model="selectCentro" v-if="checkExterno">
                                            <option value="-1">Seleccione</option>
                                            <option  v-for="(data,index) in arrCentrosExterno" :key="index" :value="data.codigo">{{data.codigo+" - "+data.nombre}}</option>
                                        </select>
                                    </div>
                                    <div class="form-control w-25 justify-between">
                                        <label class="form-label m-0" for=""></label>
                                        <button type="button" class="btn btn-primary me-2" @click="generate()">Generar</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="table-responsive " style="height:55vh">
                            <table  class="table fw-normal table-hover">
                                <thead>
                                    <tr class="text-center ">
                                        <th class="bg-primary text-white">Codigo</th>
                                        <th class="bg-primary text-white">Cuenta</th>
                                        <th class="bg-primary text-white">Saldo anterior</th>
                                        <th class="bg-primary text-white">Debito</th>
                                        <th class="bg-primary text-white">Credito</th>
                                        <th class="bg-primary text-white">Saldo final</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr v-for="(data,index) in arrData.data" :key="index"
                                    :class="[data.comprobante.length < arrData.nivel ? 'bg-secondary fw-bold' : '']"
                                    @dblclick="getAuxiliar(data)">
                                        <td>{{data.comprobante}}</td>
                                        <td>{{data.nombre}}</td>
                                        <td class="text-right">{{formatNumero(data.saldo)}}</td>
                                        <td class="text-right">{{formatNumero(data.debito)}}</td>
                                        <td class="text-right">{{formatNumero(data.credito)}}</td>
                                        <td class="text-right">{{formatNumero(data.saldo_final)}}</td>
                                    </tr>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <td class="fw-bold text-right bg-secondary" colspan="2">Total:</td>
                                        <td class="text-right bg-secondary fw-bold">{{formatNumero(arrData.saldo)}}</td>
                                        <td class="text-right bg-secondary fw-bold">{{formatNumero(arrData.debito)}}</td>
                                        <td class="text-right bg-secondary fw-bold">{{formatNumero(arrData.credito)}}</td>
                                        <td class="text-right bg-secondary fw-bold">{{formatNumero(arrData.saldo_final)}}</td>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                    <div v-show="isModal" class="modal">
                        <div class="modal-dialog modal-lg" >
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title">Buscar cuentas</h5>
                                    <button type="button" @click="isModal=false;" class="btn btn-close"><div></div><div></div></button>
                                </div>
                                <div class="modal-body">
                                    <div class="d-flex flex-column">
                                        <div class="form-control m-0 mb-3">
                                            <input type="search" v-model="txtSearch" @keyup="search('modal')" placeholder="Buscar cuenta, cuenta bancaria o nombre">
                                        </div>
                                        <div class="form-control m-0 p-2">
                                            <label class="form-label" for="labelInputName">Resultados: <span class="fw-bold">{{txtResultados}}</span></label>
                                        </div>
                                    </div>
                                    <div class="overflow-auto max-vh-50 overflow-x-hidden " >
                                        <table class="table table-hover fw-normal p-2">
                                            <thead>
                                                <tr>
                                                    <th class="text-center">Cuenta</th>
                                                    <th class="text-center">Nombre</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr v-for="(data,index) in arrCuentasCopy" :key="index" @dblclick="selectItem(data)">
                                                    <td>{{data.codigo}}</td>
                                                    <td>{{data.nombre}}</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
				</article>
			</section>
		</form>

		<script src="node_modules/read-excel-file/bundle/read-excel-file.min.js"></script>
		<script src="node_modules/xlsx/dist/xlsx.full.min.js"></script>
        <script src="Librerias/vue/vue.min.js"></script>
		<script src="Librerias/vue/axios.min.js"></script>
		<script type="module" src="contabilidad_vue/reportes/js/functions_balance.js?<?= date('d_m_Y_h_i_s');?>"></script>

	</body>
</html>
