<?php
		require 'comun.inc';
		require 'funciones.inc';
		session_start();
		cargarcodigopag($_GET['codpag'],$_SESSION['nivel']);
		header('Cache-control: private'); // Arregla IE 6
		header("Cache-control: no-cache, no-store, must-revalidate");
		header("Content-Type: text/html;charset=utf8");
		date_default_timezone_set('America/Bogota');
		titlepag();
?>
<!DOCTYPE >
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es">
	<head>
		<meta http-equiv="Content-type" content="text/html; charset=utf8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=9"/>
		<title>:: IDEAL 10 - Servicios P&uacute;blicos</title>
		<link href="css/css2.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/cssSP.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/tabs.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
        <script type="text/javascript" src="css/calendario.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script src="vue/vue.js"></script>
		<script type="text/javascript" src="css/programas.js"></script>        

		<!-- sweetalert2 -->
		<script src="sweetalert2/dist/sweetalert2.min.js"></script>
		<link rel="stylesheet" href="sweetalert2/dist/sweetalert2.min.css">

		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">

		<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
		<script type="text/javascript" src="bootstrap/fontawesome.5.11.2/js/all.js"></script>

		<style>
			.modal-mask {
			position: fixed;
			z-index: 9998;
			top: 0;
			left: 0;
			width: 100%;
			height: 100%;
			background-color: rgba(0, 0, 0, .5);
			display: table;
			transition: opacity .3s ease;
			}

			.modal-wrapper {
			display: table-cell;
			vertical-align: middle;
			}
			.modal-body{
				max-height: 500px;
				overflow-y: scroll;
			}
			.modal-intermetio{
				margin: 0 15px;
				font-family: helvética !important;
				font-size: 26px !important;
				padding: 10px 0;
			}
			.modal-intermedio-agregar{
				text-align:right;
				padding: 4px;
				margin-top: 6px;
				margin-right: 20px
			}
			.modal-body_1{
				padding-top: 15px;
				height: 40px;
			}
			.loader-table{
				/* background-color: #dff9fb;
				opacity: .5; */
				display: flex;
				align-items: center;
				justify-content: center;
				height: 75%;
			}
			.spinner{
				border: 4px solid rgba(0, 0, 0, 0.2);
				border-left-color: #39C;
				border-radius: 50%;
				width: 50px;
				height: 50px;
				animation: spin .9s linear infinite;
			}
			@keyframes spin {
				to { transform: rotate(360deg); }
			}
			.modal-container1
			{
				width: 50%;
				margin: 0px auto;
				padding: 20px 30px;
				text-align: left;
				background:linear-gradient(#99bbcc, #B6CEDA);
				border-radius: 10px;
				box-shadow: 0 2px 8px rgba(0, 0, 0, .33);
				transition: all .3s ease;
			}

            [v-cloak]{
                display : none;
            }

            label{
                font-size:13px;
            }

            input{
                height: calc(1em + 0.6rem + 0.5px) !important; 
                font-size: 14px !important; 
                margin-top: 4px !important;
            }

        </style>

		<style scoped>
			#axiosForm{  /* Components Root Element ID */
				position: relative;
			}
			.loader{  /* Loader Div Class */
				position: absolute;
				top:0px;
				right:0px;
				width:100%;
				height:100%;
				background-color:#eceaea;
				background-image: url('../assets/ajax-loader.gif');
				background-size: 50px;
				background-repeat:no-repeat;
				background-position:center;
				z-index:10000000;
				opacity: 0.4;
				filter: alpha(opacity=40);
			}
		</style>
	</head>
	<body>
		<form name="form2" method="post" action="">
			<div class="subpantalla" style="height:640px; width:99.6%; overflow:hidden;">
				<div id="myapp" style="height:inherit;" v-cloak>
					<div>
						<table>
							<tr>
								<script>barra_imagenes("serv");</script>
								<?php cuadro_titulos();?>
							</tr>

							<tr>
								<?php menu_desplegable("serv");?>
							</tr>

							<tr>
								<td colspan="3" class="cinta">
									<a href="serv-lecturas.php" class="mgbt"><img src="imagenes/add.png"/></a>
									<a v-on:click="guardar();" class="mgbt"><img src="imagenes/guarda.png" title="Guardar"/></a>
									<a href="serv-buscaLecturas.php" class="mgbt"><img src="imagenes/busca.png" title="Buscar"/></a>
									<a onclick="mypop=window.open('plan-agenda.php','','');mypop.focus()" class="mgbt"><img src="imagenes/agenda1.png" title="Agenda" /></a>
									<a onclick="mypop=window.open('serv-principal.php','','');mypop.focus();" class="mgbt"><img src="imagenes/nv.png" title="Nueva ventana"></a>
									<a onclick="mypop=window.open('<?php echo $url2; ?>','','');mypop.focus();" class="mgbt"><img src="imagenes/duplicar_pantalla.png" title="Duplicar pesta&ntilde;a"></a>
									<img src="imagenes/excel.png" title="Excel" class="mgbt" v-on:click="excel">
								</td>
							</tr>
						</table>
					</div>

					<table class="inicio grande">
						<tr>
							<td class="titulos" colspan="9">.: Ingresar Lecturas Acueducto y Alcantarillado</td>
							<td class="cerrar" style="width:7%" onClick="location.href='serv-principal.php'">Cerrar</td>
						</tr>

						<tr>
							<td class="tamano01" style="width: 3cm;">Corte:</td>

							<td style="width: 5%;">
								<input type="text" name='corte' id='corte' v-model='corte' style="width: 100%; height: 30px; text-align:center;" readonly>
							</td>

							<td class="tamano01" style="width: 3cm;">Ruta:</td>

							<td>
								<select v-model="ruta" class="centrarSelect" v-on:change="" style="width: 98%;">
									<option class="aumentarTamaño" v-for="ruta in rutas" v-bind:value = "ruta[0]">
										{{ ruta[0] }} - {{ ruta[1] }}
									</option>
								</select>
							</td>

							<td style=" height: 30px;"><em class="botonflecha" v-on:click="buscaUsuarios()">Mostrar Listado</em></td>
						</tr>
					</table>

					<div id="cargando" v-if="loading" class="loading">
						<span>Cargando...</span>
					</div>

					<div class="subpantalla" style="height: 65%;">
						<table>
							<thead>
								<tr>
									<td class="titulos" colspan="10" height="25">Usuarios</td>
								</tr>

								<tr class="titulos2">
									<td style="text-align: center; width: 4%">Cod Usuario</td>
									<td style="text-align: center; width: 7%">Suscriptor</td>
									<td style="text-align: center; width: 7%">Dirección</td>
									<td style="text-align: center; width: 7%">Seral Medidor</td>
									<td style="text-align: center; width: 3%">Consumo 3</td>
									<td style="text-align: center; width: 3%">Consumo 2</td>
									<td style="text-align: center; width: 3%">Consumo Anterior</td>
									<td style="text-align: center; width: 3%">Lectura Anterior</td>
									<td style="text-align: center; width: 3%">Lectura Actual</td>
									<td style="text-align: center; width: 3%">Consumo</td>
								</tr>
							</thead>
							

							<tbody>
								<?php
									$co ='zebra1';
									$co2='zebra2';
									$x = 0;
								?>
								<tr v-for="(usuario,index) in usuariosLecturas" v-bind:class="index % 2 ? 'saludo1a' : 'saludo2'" style='text-rendering: optimizeLegibility; text-align:center;'>
									<td style="font: 110% sans-serif; padding-left:10px">{{ usuario[1] }}</td>
									<td style="font: 110% sans-serif; padding-left:10px">{{ usuario[2] }}</td>
									<td style="font: 110% sans-serif; padding-left:10px">{{ usuario[3] }}</td>
									<td style="font: 110% sans-serif; padding-left:10px">{{ usuario[4] }}</td>
									<td style="font: 110% sans-serif; padding-left:10px">{{ usuario[11] }}</td>
									<td style="font: 110% sans-serif; padding-left:10px">{{ usuario[10] }}</td>
									<td style="font: 110% sans-serif; padding-left:10px">{{ usuario[9] }}</td>
									<td style="font: 110% sans-serif; padding-left:10px">{{ usuario[5] }}</td>
									<td style="font: 120% sans-serif; padding-left:10px;">
										<input type="number" id="lecturaActual" v-model="lecturaActual[index]" v-on:change="calcularConsumo(index)" style="text-align: center; width: 100%;">
									</td>
									<td style="font: 120% sans-serif; padding-left:10px;">
										<input type="number" id="consumo" v-model="consumo[index]" v-bind:style="estilos[index]" style="text-align: center; width: 100%;">
									</td>
									

									<input type='hidden' name='ordenRuta[]' v-model="usuario[0]">	
									<input type='hidden' name='codUsuario[]' v-model="usuario[1]">
									<input type='hidden' name='suscriptor[]' v-model="usuario[2]">
									<input type='hidden' name='direccion[]' v-model="usuario[3]">
									<input type='hidden' name='medidor[]' v-model="usuario[4]">
									<input type='hidden' name='lecturaAnterior[]' v-model="usuario[5]">
									<input type='hidden' name='lecturaActual[]' v-model="lecturaActual[index]">
									<input type='hidden' name='consumo[]' v-model="consumo[index]">
									<input type='hidden' name='consumoAnterior[]' v-model="usuario[9]">
									<input type='hidden' name='consumo2[]' v-model="usuario[10]">
									<input type='hidden' name='consumo3[]' v-model="usuario[11]">
								</tr>
							</tbody>
						</table>
					</div>

				</div>	
			</div>
		</form>
		
		<script src="Librerias/vue/vue.min.js"></script>
		<script src="Librerias/vue/axios.min.js"></script>
		<script src="vue/servicios_publicos/serv-lecturas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>	
	</body>
</html>