<?php
    
    require_once 'PHPExcel/Classes/PHPExcel.php';
	require "comun.inc";
	require "funciones.inc";
    require 'funcionesSP.inc.php';

    session_start();
    if($_GET){
        $objPHPExcel = new PHPExcel();
        $obj = new EstadoCartera();
        $arrData = $obj->getMovimientos($_GET['codigo']);
        $arrMovimientos = $arrData['movimientos'];
        $arrUsuario = $arrData['usuario'];

        $objPHPExcel->getProperties()
        ->setCreator("IDEAL 10")
        ->setLastModifiedBy("IDEAL 10")
        ->setTitle("Exportar Excel con PHP")
        ->setSubject("Documento de prueba")
        ->setDescription("Documento generado con PHPExcel")
        ->setKeywords("usuarios phpexcel")
        ->setCategory("reportes");

        //----Cuerpo de Documento----
        $objPHPExcel->setActiveSheetIndex(0)
        ->mergeCells('A1:f1')
        ->mergeCells('A2:F2')
        ->mergeCells("B3:F3")
        ->mergeCells("B4:F4")
        ->mergeCells("B5:F5")
        ->mergeCells("B6:F6")
        ->mergeCells("B7:F7")
        ->setCellValue('A1', 'ESTADO DE CARTERA')
        ->setCellValue('A2', 'REPORTE DE MOVIMIENTOS')
        ->setCellValue("A3","Código de usuario:")
        ->setCellValue("A4","Nombre/Razón social:")
        ->setCellValue("A5","Barrio:")
        ->setCellValue("A6","Estrato:")
        ->setCellValue("A7","Dirección:")
        ->setCellValue("B3",$arrUsuario['codigo'])
        ->setCellValue("B4",$arrUsuario['nombre'])
        ->setCellValue("B5",$arrUsuario['barrio'])
        ->setCellValue("B6",$arrUsuario['estrato'])
        ->setCellValue("B7",$arrUsuario['direccion']);
        $objPHPExcel-> getActiveSheet ()
        -> getStyle ("A1")
        -> getFill ()
        -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
        -> getStartColor ()
        -> setRGB ('C8C8C8');
        $objPHPExcel-> getActiveSheet ()
        -> getStyle ("A1")
        -> getFont ()
        -> setBold ( true ) 
        -> setName ( 'Verdana' ) 
        -> setSize ( 10 ) 
        -> getColor ()
        -> setRGB ('000000');
        $objPHPExcel-> getActiveSheet ()
        -> getStyle ('A1:A2')
        -> getAlignment ()
        -> applyFromArray (array ( 'horizontal'  =>  PHPExcel_Style_Alignment :: HORIZONTAL_CENTER ,) );
        $objPHPExcel-> getActiveSheet ()
        -> getStyle ('A3:f3')
        -> getAlignment ()
        -> applyFromArray (array ( 'horizontal'  =>  PHPExcel_Style_Alignment :: VERTICAL_JUSTIFY ,) ); 
        $objPHPExcel-> getActiveSheet ()
        -> getStyle ("A2")
        -> getFill ()
        -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
        -> getStartColor ()
        -> setRGB ('A6E5F3');
        $objPHPExcel-> getActiveSheet ()
        -> getStyle ("A8:f8")
        -> getFill ()
        -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
        -> getStartColor ()
        -> setRGB ('22C6CB');
        $borders = array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                'color' => array('argb' => 'FF000000'),
                )
            ),
        );
        $objPHPExcel->getActiveSheet()->getStyle('A1:f1')->applyFromArray($borders);
        $objPHPExcel->getActiveSheet()->getStyle('A2:f2')->applyFromArray($borders);
        $objPHPExcel->getActiveSheet()->getStyle('A3:f3')->applyFromArray($borders);
        $objPHPExcel->getActiveSheet()->getStyle('A4:f4')->applyFromArray($borders);
        $objPHPExcel->getActiveSheet()->getStyle('A5:f5')->applyFromArray($borders);
        $objPHPExcel->getActiveSheet()->getStyle('A6:f6')->applyFromArray($borders);
        $objPHPExcel->getActiveSheet()->getStyle('A7:f7')->applyFromArray($borders);

        $objWorksheet = $objPHPExcel->getActiveSheet();

        $objPHPExcel->setActiveSheetIndex(0)
        ->setCellValue('A8', 'Tipo de movimiento')
        ->setCellValue('B8', 'Consecutivo')
        ->setCellValue('C8', 'Fecha')
        ->setCellValue('D8', 'Cobro')
        ->setCellValue('E8', 'Pago')
        ->setCellValue('F8', 'Saldo');

        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true); 
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true); 
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true); 
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true); 
        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true); 
        $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true); 

        $rowExcel = 9;
        foreach ($arrMovimientos as $data) {
            $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValueExplicit ("A$rowExcel", $data['tipo'], PHPExcel_Cell_DataType :: TYPE_STRING)
            ->setCellValueExplicit ("B$rowExcel", $data['consecutivo'], PHPExcel_Cell_DataType :: TYPE_STRING)
            ->setCellValueExplicit ("C$rowExcel", $data['fecha'], PHPExcel_Cell_DataType :: TYPE_STRING)
            ->setCellValueExplicit ("D$rowExcel", $data['cobro'], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
            ->setCellValueExplicit ("E$rowExcel", $data['pago'], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
            ->setCellValueExplicit ("F$rowExcel", $data['saldo'], PHPExcel_Cell_DataType :: TYPE_NUMERIC);
            $rowExcel++;
        }
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="reporte-estado-cartera.xlsx"');
        header('Cache-Control: max-age=0');
        $objWriter=PHPExcel_IOFactory::createWriter($objPHPExcel,'Excel2007');
        $objWriter->save('php://output');
        die();
    }

    class EstadoCartera{
        private $linkbd;
        private $strCodigoUsuario;
        private $intIdTercero;
        private $intIdBarrio;
        private $intIdEstrato;
        private $intIdCliente;
        public function __construct() {
            $this->linkbd = conectar_v7();
            $this->linkbd->set_charset("utf8");
        }

        public function getMovimientos(string $codigo){ 
            $this->strCodigoUsuario = $codigo;
            $sql="SELECT id,id_tercero,id_barrio,id_estrato,estado,cod_usuario FROM srvclientes WHERE cod_usuario=$this->strCodigoUsuario";
            $requestUser = mysqli_query($this->linkbd,$sql)->fetch_assoc();
            if(!empty($requestUser)){
                if($requestUser['estado']=="S"){
                    $this->intIdTercero = $requestUser['id_tercero'];
                    $this->intIdBarrio = $requestUser['id_barrio'];
                    $this->intIdEstrato = $requestUser['id_estrato'];
                    $this->intIdCliente = $requestUser['id'];

                    //DATOS USUARIO
                    $request['usuario']['codigo'] = $requestUser['cod_usuario'];

                    $request['usuario']['nombre'] = mysqli_query($this->linkbd,"SELECT CASE WHEN razonsocial IS NULL OR razonsocial = '' 
                    THEN CONCAT(nombre1,' ',nombre2,' ',apellido1,' ',apellido2) 
                    ELSE razonsocial END AS nombre FROM terceros WHERE id_tercero = $this->intIdTercero")->fetch_assoc()['nombre'];
                    
                    $request['usuario']['barrio'] = mysqli_query($this->linkbd,
                    "SELECT nombre FROM srvbarrios WHERE id = $this->intIdBarrio")->fetch_assoc()['nombre'];

                    $request['usuario']['estrato']= mysqli_query($this->linkbd,
                    "SELECT descripcion FROM srvestratos WHERE id = $this->intIdEstrato")->fetch_assoc()['descripcion'];

                    $request['usuario']['direccion']= mysqli_query($this->linkbd,
                    "SELECT direccion FROM srvdireccion_cliente  WHERE id = $this->intIdCliente")->fetch_assoc()['direccion'];
                    //DATOS FACTURAS
                    $arrFacturas = mysqli_fetch_all(mysqli_query($this->linkbd,
                    "SELECT id_corte,numero_facturacion FROM srvcortes_detalle WHERE id_cliente = $this->intIdCliente"),MYSQLI_ASSOC);
                    $rows = count($arrFacturas);
                    for ($i=0; $i < $rows; $i++) { 
                        $idCorte = $arrFacturas[$i]['id_corte'];
                        $arrFacturas[$i]['fecha'] = mysqli_query($this->linkbd,
                        "SELECT fecha_impresion as fecha FROM srvcortes WHERE numero_corte = $idCorte")->fetch_assoc()['fecha'];
                        if($idCorte == 1){
                            $valorFactura =  consultaValorFactura($arrFacturas[$i]['numero_facturacion']);
                        }else{
                            $valorFactura = consultaValorFacturaSinDeuda($arrFacturas[$i]['numero_facturacion']);
                        }
                        $arrFacturas[$i]['intereses'] = consultaValorIntereses($arrFacturas[$i]['numero_facturacion']);
                        $arrFacturas[$i]['valor_factura'] = $valorFactura;
                    }
                    $request['movimientos']['facturas'] = $arrFacturas;
                    $request['movimientos']['pagos'] = mysqli_fetch_all(mysqli_query($this->linkbd,
                    "SELECT codigo_recaudo, valor_pago, fecha_recaudo as fecha 
                    FROM srv_recaudo_factura WHERE estado != 'REVERSADO' AND id_cliente = $this->intIdCliente"),MYSQLI_ASSOC);

                    $request['movimientos']['acuerdos_pago'] = mysqli_fetch_all(mysqli_query($this->linkbd,
                    "SELECT codigo_acuerdo, valor_abonado, fecha_acuerdo as fecha 
                    FROM srv_acuerdo_cab WHERE estado_acuerdo != 'Reversado' AND id_cliente = $this->intIdCliente"),MYSQLI_ASSOC);
                    
                    $request['movimientos'] = $this->calcMovimientos($request['movimientos']);
                    return $request;
                }
            }
        }

        public function calcMovimientos(array $data){
            $facturas = $data['facturas'];
            $pagos = $data['pagos'];
            $acuerdosPago = $data['acuerdos_pago'];
            $arrMovimientos = array_merge($facturas,$pagos,$acuerdosPago);
            $saldo=0;
            usort($arrMovimientos,function($a,$b){
                $fechaA = new DateTime($a['fecha']);
                $fechaB = new DateTime($b['fecha']);
                if($fechaA == $fechaB){
                    return 0;
                }
                return ($fechaA < $fechaB) ? -1 : 1;
            });
            $arrLength = count($arrMovimientos);
            for ($i=0; $i < $arrLength; $i++) { 
                $arrDate = explode("-",$arrMovimientos[$i]['fecha']);
                $arrMovimientos[$i]['fecha'] = $arrDate[2]."/".$arrDate[1]."/".$arrDate[0];
                $arrMovimientos[$i]['cobro'] = 0;
                $arrMovimientos[$i]['pago'] = 0;
                
                if(isset($arrMovimientos[$i]['numero_facturacion'])){
                    $arrMovimientos[$i]['tipo'] = "FACTURACION";
                    $arrMovimientos[$i]['consecutivo'] = $arrMovimientos[$i]['numero_facturacion'];
                    $valorAnterior = $arrMovimientos[$i-1]['valor_factura'];
                    $valorActual = $arrMovimientos[$i]['valor_factura'];
                    $interes = $arrMovimientos[$i]['intereses'];
                    $interesReal = abs($arrMovimientos[$i-1]['intereses']-$interes);
                    $valorActualTotal = $valorActual+$interesReal;
                    $arrMovimientos[$i]['valor_factura']=$valorActualTotal+$valorAnterior;
                    $arrMovimientos[$i]['cobro'] =$valorActualTotal;
                    $arrMovimientos[$i]['saldo'] =$arrMovimientos[$i]['valor_factura'];
                }elseif(isset($arrMovimientos[$i]['codigo_recaudo'])){
                    $arrMovimientos[$i]['tipo'] = "PAGO DE FACTURA";
                    $arrMovimientos[$i]['consecutivo'] = $arrMovimientos[$i]['codigo_recaudo'];
                    $arrMovimientos[$i]['pago'] =$arrMovimientos[$i]['valor_pago'];
                }elseif(isset($arrMovimientos[$i]['codigo_acuerdo'])){
                    $arrMovimientos[$i]['tipo'] = "ACUERDO DE PAGO";
                    $arrMovimientos[$i]['consecutivo'] = $arrMovimientos[$i]['codigo_acuerdo'];
                    $arrMovimientos[$i]['pago'] =$arrMovimientos[$i]['valor_abonado'];
                }
                $saldo = $arrMovimientos[$i]['saldo'];
                $pago = $arrMovimientos[$i]['pago'];
                if($arrMovimientos['tipo'] == "pago"){  
                    $saldo-=$pago;
                }
                $arrMovimientos[$i]['pago'] = ceil($arrMovimientos[$i]['pago']/100)*100;
                $arrMovimientos[$i]['cobro'] = ceil($arrMovimientos[$i]['cobro']/100)*100;
                $arrMovimientos[$i]['saldo'] = ceil($saldo/100)*100;
            }
            return $arrMovimientos;
        }
    }
?>