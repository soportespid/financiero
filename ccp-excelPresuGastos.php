<?php
	require_once 'PHPExcel/Classes/PHPExcel.php';
    include '/PHPExcel/Classes/PHPExcel/IOFactory.php';
	require "comun.inc";
	require "funciones.inc";
    ini_set('max_execution_time',99999999);
    header("Content-type: application/json");
	session_start();
	$linkbd=conectar_v7();
	$linkbd -> set_charset("utf8");

    $sqlr="select *from configbasica where estado='S' ";
    $res=mysqli_query($linkbd, $sqlr);
    while($row=mysqli_fetch_row($res))
    {
        $nit=$row[0];
        $rs=$row[1];
    }

	$objPHPExcel = new PHPExcel();
	
	$objPHPExcel->getProperties()
		->setCreator("IDEAL10")
		->setLastModifiedBy("IDEAL10")
		->setTitle("AUXILIAR PRESUPUESTAL")
		->setSubject("CCP")
		->setDescription("CCP")
		->setKeywords("CCP")
		->setCategory("PRESUPUESTO CCP");
	$objPHPExcel->getActiveSheet()->mergeCells('A1:T1');
	$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A1', 'Informe de ejecución presupuestal gastos - Funcionamiento - Servicio de la deuda - CPC');
	$objFont=$objPHPExcel->getActiveSheet()->getStyle('A1')->getFont();
	$objFont->setName('Courier New');
	$objFont->setSize(15);
	$objFont->setBold(true);
	$objFont->setUnderline(PHPExcel_Style_Font::UNDERLINE_SINGLE);
	$objFont->getColor()->setARGB( PHPExcel_Style_Color::COLOR_WHITE);
	$objAlign=$objPHPExcel->getActiveSheet()->getStyle('A1')->getAlignment(); 
	$objAlign->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
	$objAlign->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);

	$objPHPExcel->getActiveSheet()->mergeCells('A2:T2');
	$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A2', $nit.' - '.$rs);
	$objFont=$objPHPExcel->getActiveSheet()->getStyle('A2')->getFont();
	$objFont->setName('Courier New');
	$objFont->setSize(15);
	$objFont->setBold(true);
	$objFont->setUnderline(PHPExcel_Style_Font::UNDERLINE_SINGLE);
	$objFont->getColor()->setARGB( PHPExcel_Style_Color::COLOR_WHITE);
	$objAlign=$objPHPExcel->getActiveSheet()->getStyle('A2')->getAlignment(); 
	$objAlign->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
	$objAlign->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);

	$objPHPExcel->getActiveSheet()->mergeCells('A3:T3');
	$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A3', 'Fecha inicial: '.$_POST['fechaIni'].' - Fecha final: '.$_POST['fechaFin']);
	$objFont=$objPHPExcel->getActiveSheet()->getStyle('A3')->getFont();
	$objFont->setName('Courier New');
	$objFont->setSize(15);
	$objFont->setBold(true);
	$objFont->setUnderline(PHPExcel_Style_Font::UNDERLINE_SINGLE);
	$objFont->getColor()->setARGB( PHPExcel_Style_Color::COLOR_WHITE);
	$objAlign=$objPHPExcel->getActiveSheet()->getStyle('A3')->getAlignment(); 
	$objAlign->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
	$objAlign->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);

	$objPHPExcel-> getActiveSheet ()
		-> getStyle ("A4:T4")
		-> getFill ()
		-> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
		-> getStartColor()
		-> setRGB ('368986');

	$objPHPExcel-> getActiveSheet ()
		-> getStyle ("A1")	
		-> getFill ()
		-> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
		-> getStartColor ()
		-> setRGB ('e79a32');

    $objPHPExcel-> getActiveSheet ()
		-> getStyle ("A2")	
		-> getFill ()
		-> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
		-> getStartColor ()
		-> setRGB ('e79a32');
    
    $objPHPExcel-> getActiveSheet ()
		-> getStyle ("A3")	
		-> getFill ()
		-> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
		-> getStartColor ()
		-> setRGB ('e79a32');

	$bordersTitle = array(
		'borders' => array(
			'allborders' => array(
				'style' => PHPExcel_Style_Border::BORDER_THIN,
				'color' => array('argb' => 'ffffffff'),
			)
		),
		'font' => array(
			'bold' => true,
			'size' => 12,
			'color' => array('argb' => 'ffffffff'),
		),
	);

	$borders = array(
		'borders' => array(
			'allborders' => array(
				'style' => PHPExcel_Style_Border::BORDER_THIN,
				'color' => array('argb' => 'FF000000'),
			)
		),
	);

    $borders2 = array(
		'borders' => array(
			'allborders' => array(
				'style' => PHPExcel_Style_Border::BORDER_THIN,
				'color' => array('argb' => 'FF000000'),
			)
		),
        'font' => array(
            'bold' => true
        ),
	);

	$objPHPExcel->getActiveSheet()->getStyle('A4:T4')->applyFromArray($bordersTitle);
	$objPHPExcel->setActiveSheetIndex(0)
        ->setCellValue('A4', 'Vig gasto')
        ->setCellValue('B4', 'Dependencia')
        ->setCellValue('C4', 'Codigo cuenta')
        ->setCellValue('D4', 'Nombre cuenta')
        ->setCellValue('E4', 'Codigo fuente')
		->setCellValue('F4', 'Nombre fuente')
        ->setCellValue('G4', 'Medio de pago')
        ->setCellValue('H4', 'Presupuesto Inicial')
        ->setCellValue('I4', 'Adiciones')
        ->setCellValue('J4', 'Reducciones')
        ->setCellValue('K4', 'Credito')
        ->setCellValue('L4', 'Contracredito')
        ->setCellValue('M4', 'Definitivo')
        ->setCellValue('N4', 'CPC')
		->setCellValue('O4', 'Nombre CPC')
        ->setCellValue('P4', 'Disponibilidad')
        ->setCellValue('Q4', 'Compromisos')
        ->setCellValue('R4', 'Obligación')
        ->setCellValue('S4', 'Egresos')
        ->setCellValue('T4', 'Saldo');

	$i=5;
    
	for($ii=0;$ii<count ($_POST['codigo']);$ii++)
	{
		$objPHPExcel->setActiveSheetIndex(0)
		->setCellValueExplicit ("A$i", $_POST['vigGasto'][$ii], PHPExcel_Cell_DataType :: TYPE_STRING)
		->setCellValueExplicit ("B$i", $_POST['secPresuestal'][$ii], PHPExcel_Cell_DataType :: TYPE_STRING)
		->setCellValueExplicit ("C$i", $_POST['codigo'][$ii], PHPExcel_Cell_DataType :: TYPE_STRING)
		->setCellValueExplicit ("D$i", $_POST['nombre'][$ii], PHPExcel_Cell_DataType :: TYPE_STRING)
        ->setCellValueExplicit ("E$i", $_POST['fuente'][$ii], PHPExcel_Cell_DataType :: TYPE_STRING)
		->setCellValueExplicit ("F$i", $_POST['nomFuente'][$ii], PHPExcel_Cell_DataType :: TYPE_STRING)
        ->setCellValueExplicit ("G$i", $_POST['medioPago'][$ii], PHPExcel_Cell_DataType :: TYPE_STRING)
        ->setCellValueExplicit ("H$i", $_POST['presuInicial'][$ii], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
        ->setCellValueExplicit ("I$i", $_POST['adicion'][$ii], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
        ->setCellValueExplicit ("J$i", $_POST['reduccion'][$ii], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
        ->setCellValueExplicit ("K$i", $_POST['credito'][$ii], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
        ->setCellValueExplicit ("L$i", $_POST['contraCredito'][$ii], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
        ->setCellValueExplicit ("M$i", $_POST['definitivo'][$ii], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
        ->setCellValueExplicit ("N$i", $_POST['cpc'][$ii], PHPExcel_Cell_DataType :: TYPE_STRING)
		->setCellValueExplicit ("O$i", $_POST['nomCpc'][$ii], PHPExcel_Cell_DataType :: TYPE_STRING)
        ->setCellValueExplicit ("P$i", $_POST['dosponibilidad'][$ii], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
        ->setCellValueExplicit ("Q$i", $_POST['compromisos'][$ii], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
        ->setCellValueExplicit ("R$i", $_POST['obligacion'][$ii], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
        ->setCellValueExplicit ("S$i", $_POST['egresos'][$ii], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
        ->setCellValueExplicit ("T$i", $_POST['saldo'][$ii], PHPExcel_Cell_DataType :: TYPE_NUMERIC);
		
        if ($_POST['tipo'][$ii] == "A") {
            $objPHPExcel->getActiveSheet()->getStyle("A$i:T$i")->applyFromArray($borders2);
        }
        else if($_POST['tipo'][$ii] == "D"){
            $objPHPExcel->getActiveSheet()->getStyle("A$i:T$i")-> getFont()->setItalic(true)->setBold(true)->setSize(10);
			
			$objPHPExcel-> getActiveSheet()
				-> getStyle ("A$i:T$i")
				-> getFill ()
				-> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
				-> getStartColor()
				-> setRGB ('ACECFE');
        }else{
			$objPHPExcel->getActiveSheet()->getStyle("A$i:T$i")->applyFromArray($borders);
		}

		if( substr($_POST['codigo'][$ii], 0, 8) == '2.1.2.02' && $_POST['tipo'][$ii] != "D"){
			$objPHPExcel-> getActiveSheet()
				-> getStyle ("A$i:T$i")
				-> getFill ()
				-> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
				-> getStartColor()
				-> setRGB ('80DBF5');
		}
		
        $i++;
	}

	$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(8);
	$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(10);
	$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(80);
    $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(20);
    $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(10);
    $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('K')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('L')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('M')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('N')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('O')->setWidth(80);
    $objPHPExcel->getActiveSheet()->getColumnDimension('P')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('Q')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('R')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('S')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('T')->setAutoSize(true);

	$objPHPExcel->getActiveSheet()->setTitle('CCP');
	//header('Content-Type: application/vnd.ms-excel');
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
	header('Content-Disposition: attachment;filename="informeEjecucionGastos.xlsx"');
	header('Cache-Control: max-age=0');
	$objWriter=PHPExcel_IOFactory::createWriter($objPHPExcel,'Excel2007');
	$objWriter->save('php://output');
	exit;
?>