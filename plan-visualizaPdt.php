<?php

    header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require 'comun.inc';
	require 'funciones.inc';
	session_start();
    if(empty($_SESSION)){
        header("location: index.php");
    }
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html lang=es>
	<head>
    <meta charset="utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1"/>
		<meta name="viewport" content="user-scalable=no">
		<title>:: IDEAL 10 - Planeación estrategica</title>
		<link href="favicon.ico" rel="shortcut icon"/>
		<link href="css/css2z.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
        <link href="css/tabs.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
        <link href="css/style.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" href="sweetalert2/dist/sweetalert2.min.css">
        <script type="text/javascript" src="css/programas.js"></script>
        <script src="sweetalert2/dist/sweetalert2.min.js"></script>
		<script src="vue/vue.min.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script src="css/calendario.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
        <style>
            body {
                font-family: Arial, Helvetica, sans-serif;
                font-size: 16px; /* Tamaño estándar */
                line-height: 1.6; /* Espaciado cómodo */
                color: #333; /* Color del texto */
                background-color: #f9f9f9; /* Fondo suave */
                margin: 0;
                padding: 0;
            }
        </style>
	</head>
	<body>
		<header>
			<table>
				<tr><script>barra_imagenes("plan");</script><?php cuadro_titulos();?></tr>
			</table>
		</header>

        <section id="myapp" v-cloak>
            <input type="hidden" value = "3" ref="pageType">
            <div class="loading-container" v-show="isLoading" >
                <p class="text-loading"data-loading="IDEAL 10">IDEAL 10</p>
            </div>
            <nav>
                <table>
                    <tr><?php menu_desplegable("plan");?></tr>
                </table>
                <div class="bg-white group-btn p-1" id="newNavStyle">
                    <button type="button" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center" @click="location.href='plan-creaPdt'">
                        <span>Nuevo</span>
                        <svg viewBox="0 -960 960 960" ><path d="M440-440H200v-80h240v-240h80v240h240v80H520v240h-80v-240Z"/></svg>
                    </button>
                    <button type="button" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center" @click="location.href='plan-buscaPdt'">
                        <span>Buscar</span>
                        <svg class="fill-black group-hover:fill-white w-5 h-5" viewBox="0 -960 960 960" ><path d="M784-120 532-372q-30 24-69 38t-83 14q-109 0-184.5-75.5T120-580q0-109 75.5-184.5T380-840q109 0 184.5 75.5T640-580q0 44-14 83t-38 69l252 252-56 56ZM380-400q75 0 127.5-52.5T560-580q0-75-52.5-127.5T380-760q-75 0-127.5 52.5T200-580q0 75 52.5 127.5T380-400Z"/></svg>
                    </button>
                    <button type="button" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center" @click="mypop=window.open('plan-principal.php','',''); mypop.focus();">
                        <span>Nueva ventana</span>
                        <svg class="fill-black group-hover:fill-white w-5 h-5" viewBox="0 -960 960 960" ><path d="M320-120v-80h80v-80H160q-33 0-56.5-23.5T80-360v-400q0-33 23.5-56.5T160-840h640q33 0 56.5 23.5T880-760v400q0 33-23.5 56.5T800-280H560v80h80v80H320ZM160-360h640v-400H160v400Zm0 0v-400 400Z"/></svg>
                    </button>
                    <button type="button" class="btn btn-success d-flex justify-between align-items-center" @click="window.location.href='plan-menuPlanEstrategico'">
                        <span>Atrás</span>
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960"><path d="M760-200v-160q0-50-35-85t-85-35H273l144 144-57 56-240-240 240-240 57 56-144 144h367q83 0 141.5 58.5T840-360v160h-80Z"/></svg>
                    </button>
                </div>
            </nav>
            <article>
                <div ref="rTabs" class="nav-tabs bg-white p-1">
                    <div class="nav-item" :class="typeVista == 1 ? 'active' : ''" @click="typeVista = 1">Plan de desarrollo</div>
                    <div class="nav-item" :class="typeVista == 2 ? 'active' : ''" @click="typeVista = 2">Linea estrategica</div>
                    <div class="nav-item" :class="typeVista == 3 ? 'active' : ''" @click="typeVista = 3">Indicadores de resultado</div>
                    <div class="nav-item" :class="typeVista == 4 ? 'active' : ''" @click="typeVista = 4">Indicadores de producto</div>
                    <div class="nav-item" :class="typeVista == 5 ? 'active' : ''" @click="typeVista = 5">Iniciativa SGR</div>
                </div>

                <div class="bg-white" v-show="typeVista == 1">
                    <div>
                        <h2 class="titulos m-0">Plan de desarrollo territorial</h2>
                        
                        <div>
                            <div class="d-flex">
                                <div class="form-control">
                                    <h2 class="m-0">Plan de desarrollo municipal 2024 - 2027</h2>
                                    <h3>Periodo gobierno 2024 - 2027</h3>
                                </div>

                                <div class="form-control w-25">
                                    <h2 class="m-0">Avance general: 33%</h2>
                                </div>
                            </div>

                            <div class="d-flex">
                                <div class="form-control">
                                    <label class="form-label m-0" for="">Mandatario: {{representante}}</label>
                                    <label class="form-label m-0" for="">Fecha inicio: {{fechaIni}}</label>
                                    <label class="form-label m-0" for="">Fecha de fin: {{fechaFin}}</label>
                                </div>

                                <div class="form-control w-50">
                                    <label class="form-label m-0" for="">Presupuesto total: {{viewFormatNumber(presupuestoTotal)}}</label>
                                    <progress value="50" max="100"></progress>
                                </div>
                            </div>

                            <div class="d-flex">
                                <div class="form-control">
                                    <label class="form-label m-0" for="">Visión</label>
                                    <textarea v-model="vision" style="resize: vertical; height: 200px;"></textarea>
                                </div>
                                <div class="form-control w-15">
                                </div>
                                <div class="form-control">
                                    <label class="form-label m-0" for="">Objetivos</label>
                                    <ul v-for="(obj, index) in objetivos">
                                        <li>{{obj}}</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>                       
                </div>

                <div class="bg-white" v-show="typeVista == 2">
                    <div class="nav-tabs bg-white p-1">
                        <div class="nav-item" :class="vistaEjes == 1 ? 'active' : ''" @click="vistaEjes = 1">Tabla</div>
                        <div class="nav-item" :class="vistaEjes == 2 ? 'active' : ''" @click="vistaEjes = 2">Gráfico</div>
                    </div>
                    
                    <div v-show="vistaEjes == 1">
                        <div class="table-responsive">
                            <table class="table table-hover fw-normal">
                                <thead>
                                    <tr class="text-center">
                                        <th>Item</th>
                                        <th class="text-left">Linea estrategica</th>
                                        <th>Nivel de prioridad</th>
                                        <th>Presupuesto (%)</th>
                                        <th>Avance (%)</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr v-for="(linea,index) in lineasEstrategicas" :key="index" class="text-center">
                                        <td>LE-{{linea.id}}</td>
                                        <td class="text-left">{{linea.nombre}}</td>
                                        <td>{{linea.prioridad}}</td>
                                        <td>40%</td>
                                        <td>
                                            <div>
                                                <progress value="35" max="100"></progress>
                                                35%
                                            </div>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <div v-show="vistaEjes == 2">
                        <p>Grafico</p>
                    </div>                      
                </div>

                <div class="bg-white" v-show="typeVista == 3">
                    <div class="nav-tabs bg-white p-1">
                        <div class="nav-item" :class="vistaResultados == 1 ? 'active' : ''" @click="vistaResultados = 1">Tabla</div>
                        <div class="nav-item" :class="vistaResultados == 2 ? 'active' : ''" @click="vistaResultados = 2">Gráfico</div>
                    </div>
                    
                    <div v-show="vistaResultados == 1">
                        <div class="table-responsive">
                            <table class="table table-hover fw-normal">
                                <thead>
                                    <tr class="text-center">
                                        <th>Item</th>
                                        <th class="text-left">Indicador</th>
                                        <th>Linea base</th>
                                        <th>Meta cuatrienio</th>
                                        <th>Avance</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr v-for="(IR,index) in indicadoresResultado" :key="index" class="text-center">
                                        <td>IR-{{IR.id}}</td>
                                        <td class="text-left">{{IR.nombre_indicador}}</td>
                                        <td>{{IR.linea_base}}</td>
                                        <td>{{IR.meta_cuatrienio}}</td>
                                        <td>0</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <div v-show="vistaResultados == 2">
                        <p>Grafico</p>
                    </div>
                </div>

                <div class="bg-white" v-show="typeVista == 4">
                    <div class="nav-tabs bg-white p-1">
                        <div class="nav-item" :class="vistaIndicadores == 1 ? 'active' : ''" @click="vistaIndicadores = 1">Tabla</div>
                        <div class="nav-item" :class="vistaIndicadores == 2 ? 'active' : ''" @click="vistaIndicadores = 2">Gráfico</div>
                    </div>               

                    <div v-show="vistaIndicadores == 1">
                        <div class="table-responsive">
                            <table class="table table-hover fw-normal">
                                <thead>
                                    <tr class="text-center">
                                        <th>Item</th>
                                        <th>Indicador</th>
                                        <th>Linea base</th>
                                        <th>Meta cuatrienio</th>
                                        <th>Valor cuatrienio</th>
                                        <th>Avance</th>
                                        <th>Progreso</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr v-for="(IP,index) in indicadoresProducto" :key="index" class="text-center">
                                        <td>IP-{{IP.id}}</td>
                                        <td class="text-left">{{IP.nombre}}</td>
                                        <td>{{IP.linea_base}}</td>
                                        <td>{{IP.meta_cuatrienio}}</td>
                                        <td>{{viewFormatNumber(IP.valor_cuatrienio)}}</td>
                                        <td>400</td>
                                        <td>
                                            <progress value="50" max="100"></progress>
                                            50%
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <div v-show="vistaIndicadores == 2">
                        <p>Grafico</p>
                    </div>
                </div>

                <div class="bg-white" v-show="typeVista == 5">
                    <div class="nav-tabs bg-white p-1">
                        <div class="nav-item" :class="vistaIniciativas == 1 ? 'active' : ''" @click="vistaIniciativas = 1">Tabla</div>
                        <div class="nav-item" :class="vistaIniciativas == 2 ? 'active' : ''" @click="vistaIniciativas = 2">Gráfico</div>
                    </div>                    
                    
                    <div v-show="vistaIniciativas == 1">
                        <div class="table-responsive">
                            <table class="table table-hover fw-normal">
                                <thead>
                                    <tr class="text-center">
                                        <th>Item</th>
                                        <th class="text-left">Iniciativa</th>
                                        <th>Tipo</th>
                                        <th>Presupuesto</th>
                                        <th>Avance</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr v-for="(SGR,index) in iniciativasSGR" :key="index" class="text-center">
                                        <td>ISGR-{{SGR.id}}</td>
                                        <td class="text-left">{{SGR.nombre}}</td>
                                        <td>{{SGR.tipo}}</td>
                                        <td>{{viewFormatNumber(SGR.recurso_indicativo)}}</td>
                                        <td>
                                            <progress value="50" max="100"></progress>50%
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <div v-show="vistaIniciativas == 2">
                        <p>Grafico</p>
                    </div>
                </div>

            </article>
        </section>

        <script src="Librerias/vue/vue.min.js"></script>
		<script src="Librerias/vue/axios.min.js"></script>
		<script type="module" src="planeacion/planDesarrollo/js/planDesarrollo_functions.js"></script>

	</body>
</html>
