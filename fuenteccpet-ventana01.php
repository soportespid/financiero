<?php 
	require "comun.inc";
    require "funciones.inc";
    
    $linkbd = conectar_v7();
?>

<!DOCTYPE >
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es">
	<head>
	 	<meta http-equiv="Content-Type" content="text/html" charset="iso-8859-1"/>
        <meta http-equiv="X-UA-Compatible" content="IE=9"/>
		<title>:: Ideal - Presupuesto</title>
        <link href="css/css2.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="css/programas.js"></script>
		<script>
			function ponprefijo(pref,opc)
			{ 
				parent.document.form2.fuente.value =pref  ;
				parent.document.form2.nfuente.value =opc ;
				parent.document.form2.fuente.focus();
				parent.document.form2.valorvl.value='';
				parent.document.form2.valorvl.focus();
				parent.despliegamodal2("hidden");
			} 
		</script> 
		<?php titlepag();?>
	</head>
    <body>
        <form action="" method="post" name="form2">
			<?php 
				if($_POST['oculto']=="")
				{
					$_POST['numpos']=0;
					$_POST['numres']=10;
					$_POST['nummul']=0;
				}
			?>
			<table class="inicio" style="width:99.4%;">
    			<tr>
      				<td height="25" colspan="3" class="titulos" >Buscar Fuentes</td>
                    <td class="cerrar"><a onClick="parent.despliegamodal2('hidden');">&nbsp;Cerrar</a></td>
    			</tr>
			</table>
			<?php 
			$_POST['cuenta'] = $_GET['cuentaCcpet'];
			$_POST['vigencia'] = $_GET['vigencia'];
			$_POST['medioPago'] = $_GET['medioPago'];
			$_POST['tipoGasto'] = $_GET['tipoGasto'];
			$_POST['proyecto'] = $_GET['proyecto'];
			?>
			<input type="hidden" name="oculto" id="oculto" value="1"/>
            <input type="hidden" name="numres" id="numres" value="<?php echo $_POST['numres'];?>"/>
    		<input type="hidden" name="numpos" id="numpos" value="<?php echo $_POST['numpos'];?>"/>
       		<input type="hidden" name="nummul" id="nummul" value="<?php echo $_POST['nummul'];?>"/>
       		<input type="hidden" name="cuenta" id="cuenta" value="<?php echo $_POST['cuenta'];?>"/>
       		<input type="hidden" name="vigencia" id="vigencia" value="<?php echo $_POST['vigencia'];?>"/>
       		<input type="hidden" name="medioPago" id="medioPago" value="<?php echo $_POST['medioPago'];?>"/>
			<input type="hidden" name="tipoGasto" id="tipoGasto" value="<?php echo $_POST['tipoGasto']?>"/>
			<input type="hidden" name="proyecto" id="proyecto" value="<?php echo $_POST['proyecto']?>"/>

            <div  class="subpantalla" style="height:80.5%; width:99.2%; overflow-x:hidden;">
                <?php

					/* if($_POST['tipoGasto'] == '4')
					{
						$sqlr = "SELECT F.id_fuente, F.fuente_financiacion FROM ccpet_fuentes AS F, ccpproyectospresupuesto_presupuesto  AS CP WHERE CP.id_fuente = F.id_fuente AND CP.codproyecto = '$_POST[proyecto]' UNION SELECT FC.codigo_fuente, FC.nombre FROM ccpet_fuentes_cuipo AS FC, ccpproyectospresupuesto_presupuesto  AS CP WHERE CP.id_fuente = FC.codigo_fuente AND CP.codproyecto = '$_POST[proyecto]'";
					}
					else
					{ */
						$sqlr = "SELECT id_fuente, fuente_financiacion FROM ccpet_fuentes  UNION SELECT codigo_fuente, nombre FROM ccpet_fuentes_cuipo";
					//}
                    $resp = mysqli_query($linkbd, $sqlr);
                    
					$co = 'saludo1a';
					$co2 = 'saludo2';	
					$i = 1;
					
					
					echo"
					<table class='inicio'>
    					<tr>
      						<td colspan='4' class='titulos'>Resultados Busqueda</td>
      					</tr>
						<tr><td colspan='4'></td></tr>
    					<tr>
							<td class='titulos2' style='width:8%;'>Item</td>
							<td class='titulos2' style='width:20%;'>Subclase</td>
							<td class='titulos2'>Descripcion</td>
							<td class='titulos2' style='width:8%;'>Estado</td>	  	  
    					</tr>";
					while ($r = mysqli_fetch_row($resp)) 
					{
						if(tieneValor($r[0]))
						{
							echo "
							<tr class='$co' onMouseOver=\"anterior=this.style.backgroundColor;this.style.backgroundColor='#40b3ff';\" onMouseOut=\"this.style.backgroundColor=anterior\" "; echo" onClick=\"javascript:ponprefijo('$r[0]','$r[1]')\""; 
							echo">
									<td>$i</td>
									<td >$r[0]</td>
									<td>".ucwords(strtolower($r[1]))."</td>
									<td> S </td>
							</tr>";
							$aux=$co;
							$co=$co2;
							$co2=$aux;
							$i=1+$i;
						}
						
					}
					function tieneValor($fuente)
					{
						$linkbd1 = conectar_v7();

						$sqlrBT = 'SELECT * FROM ccpetinicialigastosbienestransportables WHERE cuenta="'.$_POST['cuenta'].'" AND vigencia="'.$_POST['vigencia'].'"  AND fuente="'.$fuente.'" AND medioPago="'.$_POST['medioPago'].'"';
						$resBT = mysqli_query($linkbd1, $sqlrBT);

						$sqlrS = 'SELECT * FROM ccpetinicialservicios WHERE cuenta="'.$_POST['cuenta'].'" AND vigencia="'.$_POST['vigencia'].'" AND fuente="'.$fuente.'" AND medioPago="'.$_POST['medioPago'].'"';
						$resS = mysqli_query($linkbd1, $sqlrS);

						$sqlrV = 'SELECT * FROM ccpetinicialvalorgastos WHERE cuenta="'.$_POST['cuenta'].'" AND vigencia="'.$_POST['vigencia'].'" AND fuente="'.$fuente.'" AND medioPago="'.$_POST['medioPago'].'"';
						$resV = mysqli_query($linkbd1, $sqlrV);

						$sqlrP = 'SELECT * FROM ccpproyectospresupuesto_presupuesto WHERE rubro="'.$_POST['cuenta'].'" AND id_fuente="'.$fuente.'"  AND medio_pago="'.$_POST['medioPago'].'"';
						$resP = mysqli_query($linkbd1, $sqlrP);//echo $sqlrP.'<br>';
						//var_dump( mysqli_num_rows($resBT));

						$sqlrA = 'SELECT * FROM ccpetadicion_inversion_detalles WHERE rubro="'.$_POST['cuenta'].'" AND id_fuente="'.$fuente.'"  AND medio_pago="'.$_POST['medioPago'].'"';
						$resA = mysqli_query($linkbd1, $sqlrA);

						$sqlrAF = 'SELECT * FROM ccpetadiciones WHERE cuenta="'.$_POST['cuenta'].'" AND fuente="'.$fuente.'"  AND mediopago="'.$_POST['medioPago'].'"';
						$resAF = mysqli_query($linkbd1, $sqlrAF);

						if(mysqli_num_rows($resV) > 0 || mysqli_num_rows($resS) > 0 || mysqli_num_rows($resBT) > 0 || mysqli_num_rows($resP) > 0 || mysqli_num_rows($resA) > 0 || mysqli_num_rows($resAF) > 0)
						{
							return true;
						}
						else
						{
							return false;
						}
					}
					 echo"
					</table>
					<table class='inicio'>
						<tr>
							<td style='text-align:center;'>
								<a href='#'>$imagensback</a>&nbsp;
								<a href='#'>$imagenback</a>&nbsp;&nbsp;";
					if($nuncilumnas<=9){$numfin=$nuncilumnas;}
					else{$numfin=9;}
					for($xx = 1; $xx <= $numfin; $xx++)
					{
						if($numcontrol<=9){$numx=$xx;}
						else{$numx=$xx+($numcontrol-9);}
						if($numcontrol==$numx){echo"<a href='#' onClick='saltocol(\"$numx\")'; style='color:#24D915'> $numx </a>";}
						else {echo"<a href='#' onClick='saltocol(\"$numx\")'; style='color:#000000'> $numx </a>";}
					}
					echo"			&nbsp;&nbsp;<a href='#'>$imagenforward</a>
									&nbsp;<a href='#'>$imagensforward</a>
								</td>
							</tr>
						</table>";
				?>

            </div>
        </form>
    </body>
</html>