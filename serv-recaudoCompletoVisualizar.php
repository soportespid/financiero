<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=iso-8859-1");
	require "comun.inc";
	require "funciones.inc";
    require "funcionesSP.inc.php";
	require "conversor.php";
	session_start();
	$linkbd = conectar_v7();
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
	//error_reporting(E_ALL);
?>
<!DOCTYPE >
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es">
	<head>
		<meta http-equiv="Content-type" content="text/html; charset=iso-8859-1"/>
		<meta http-equiv="X-UA-Compatible" content="IE=9"/>
		<title>:: IDEAL 10 - Servicios P&uacute;blicos</title>
		<link href="css/css2.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
        <link href="css/cssSP.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/style.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
		<script type="text/javascript" src="css/programas.js"></script>
        <script type="text/javascript" src="css/calendario.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script>
			function despliegamodalm(_valor,_tip,mensa,pregunta)
			{
				document.getElementById("bgventanamodalm").style.visibility=_valor;
				if(_valor=="hidden"){document.getElementById('ventanam').src="";}
				else
				{
					switch(_tip)
					{
						case "1":	document.getElementById('ventanam').src="ventana-mensaje1.php?titulos="+mensa;break;
						case "2":	document.getElementById('ventanam').src="ventana-mensaje3.php?titulos="+mensa;break;
						case "3":	document.getElementById('ventanam').src="ventana-mensaje2.php?titulos="+mensa;break;
						case "4":	document.getElementById('ventanam').src="ventana-consulta1.php?titulos="+mensa+"&idresp="+pregunta;break;	
					}
				}
			}
			function funcionmensaje()
			{
				document.location.href = "serv-recaudoCompleto.php";
			}
			function actualizar()
			{
				document.form2.submit();
			}
			function guardar()
			{	
                var validacionRecaudo = document.getElementById('modoRecaudo').value;
                var numeroFactura = document.getElementById('numeroFactura').value;
                var valor = document.getElementById('valor').value;
                var tipoMovimiento = document.getElementById('tipoMovimiento').value;

				if (validacionRecaudo != '-1' && numeroFactura != '' && valor != '' && tipoMovimiento != '-1') 
                {
                    despliegamodalm('visible','4','Esta Seguro de Guardar','1');
                }
				else 
                {
                    despliegamodalm('visible','2','Falta información para crear el recibo de caja');
                }
			}
            function respuestaconsulta(pregunta)
			{
				switch(pregunta)
				{
					case "1":	
						document.form2.oculto.value = '2';
						document.form2.submit();
					break;
				}
			}
			function buscarFactura()
			{
				if (document.form2.numeroFactura.value!="")
				{
					document.form2.bc.value='1';
					document.form2.submit();
				}
			}
            function validar(){document.form2.submit();}

			function pdf()
			{
				document.form2.action="serv-pdfReciboCaja.php";
				document.form2.target="_BLANK";
				document.form2.submit(); 
				document.form2.action="";
				document.form2.target="";
			}

			function reversar()
			{
				var estado = document.getElementById('estadoPago').value;
				var consecutivo = document.getElementById('consecutivo').value;
				var corte = document.getElementById('corte').value;

				if(estado == 'PAGO REALIZADO')
                {
                    location.href="serv-reversarRecaudo.php?cs="+consecutivo+'&corte='+corte;
                }
                else
                {
                    despliegamodalm('visible','2','No se puede realizar reversion a esta factura');
                }
			}
		</script>
		<?php titlepag();?>
	</head>
	<body>
		<IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
		<span id="todastablas2"></span>
		<table>
			<tr><script>barra_imagenes("serv");</script><?php cuadro_titulos();?></tr>

			<tr><?php menu_desplegable("serv");?></tr>

			<tr>
				<td colspan="3" class="cinta">
                    <a href="serv-recaudoCompleto.php" class="mgbt"><img src="imagenes/add.png"/></a>

					<a class="mgbt"><img src="imagenes/guardad.png" title="Guardar"/></a>

					<a href="serv-recaudoCompletoBuscar.php" class="mgbt"><img src="imagenes/busca.png" title="Buscar"/></a>

					<a onclick="mypop=window.open('plan-agenda.php','','');mypop.focus()" class="mgbt"><img src="imagenes/agenda1.png" title="Agenda" /></a>

					<a onclick="mypop=window.open('serv-principal.php','','');mypop.focus();" class="mgbt"><img src="imagenes/nv.png" title="Nueva ventana"></a>

					<a onclick="mypop=window.open('<?php echo $url2; ?>','','');mypop.focus();" class="mgbt"><img src="imagenes/duplicar_pantalla.png" title="Duplicar pesta&ntilde;a"></a>

					<a href="serv-recaudoCompletoBuscar.php" class="mgbt"><img src="imagenes/iratras.png" title="Atr&aacute;s"></a>

					<a onClick="pdf()" class="mgbt"><img src="imagenes/print.png" style="width:29px;height:25px;" title="Imprimir" /></a>
					
					<a onclick="reversar()" class="mgbt"><img src="imagenes/reversado.png" alt="Reversar Documento"></a>
                </td>
			</tr>
		</table>

		<div id="bgventanamodalm" class="bgventanamodalm">
			<div id="ventanamodalm" class="ventanamodalm">
				<IFRAME src="" name="ventanam" marginWidth=0 marginHeight=0 frameBorder=0 id="ventanam" frameSpacing=0 style=" width:700px; height:130px; top:200; overflow:hidden;"></IFRAME>
			</div>
		</div>

		<form name="form2" method="post" action="">

			<?php 
				if(@$_POST['oculto']=="")
				{
                    $_POST['consecutivo'] = $_GET['idban'];

					//declaracion variables
					$cargoFijo = array();
					$consumoBasico = array();
					$consumoConMedidor = array();
					$interesMoratorio = array();
					$subsidio = array();
					$exoneracion = array();
					$contribucion = array();
					$saldoFacturaVencida = array();
					$acuerdoPago = array();
					$codigoServicio = array();
					$nombreServicio = array();
					$valorReal = array();
					$c = 0;

                    //base srvrecibo_caja
                    $sqlReciboCaja = "SELECT * FROM srvrecibo_caja WHERE consecutivo = '$_POST[consecutivo]'";
                    $resReciboCaja = mysqli_query($linkbd,$sqlReciboCaja);
                    $rowReciboCaja = mysqli_fetch_assoc($resReciboCaja);

                    $_POST['fecha_pago'] = $rowReciboCaja['fecha'];
                    $_POST['modoRecaudo'] = $rowReciboCaja['tipo_pago'];
                    $_POST['tipoMovimiento'] = $rowReciboCaja['tipo_movimiento'];
                    $_POST['numeroFactura'] = $rowReciboCaja['numero_factura'];
                    $_POST['concepto'] = $rowReciboCaja['concepto'];
                    $_POST['valor'] = $rowReciboCaja['valor_pago'];
                    $_POST['documento'] = encuentraDocumentoTerceroConIdCliente($rowReciboCaja['id_cliente']);
                    $_POST['nombreCliente'] = encuentraNombreTerceroConIdCliente($rowReciboCaja['id_cliente']);
					$_POST['banco'] = $rowReciboCaja['numero_cuenta'];
					$_POST['nbanco'] = buscaCuentaEnCuentasnicsp($rowReciboCaja['numero_cuenta']);
					$totalg = number_format($_POST['valor'],2,'.','');
                    $_POST['letras'] = convertirdecimal($totalg,'.');

					switch($rowReciboCaja['estado'])
					{
						case 'S':
							$_POST['estadoPago'] = 'PAGO REALIZADO';
							$color = 'width: 100%; text-align:center; background: #8FFF4B;';
							break;
						
						case 'N':
							$_POST['estadoPago'] = 'PAGO REVERSADO';
							$color = 'width: 100%; text-align:center; background: #ACACAC;';
							break;
					}

				
                    //base srvcortes_detalles
                    $sqlCorteDetalles = "SELECT * FROM srvcortes_detalle WHERE numero_facturacion = '$_POST[numeroFactura]'";
                    $resCorteDetalles = mysqli_query($linkbd,$sqlCorteDetalles);
                    $rowCorteDetalles = mysqli_fetch_assoc($resCorteDetalles);

                    $_POST['corte'] = $rowCorteDetalles['id_corte'];


					//Nombre de servicio
					$sqlDetallesFacturacion = "SELECT id_servicio FROM srvdetalles_facturacion WHERE numero_facturacion = '$rowReciboCaja[numero_factura]' AND tipo_movimiento = '101' GROUP BY (id_servicio)";
					$resDetallesFacturacion = mysqli_query($linkbd,$sqlDetallesFacturacion);
					while($rowDetallesFacturacion = mysqli_fetch_assoc($resDetallesFacturacion))
					{
						$codigoServicio[] = $rowDetallesFacturacion['id_servicio'];

						$sqlServicio = "SELECT nombre FROM srvservicios WHERE id = '$rowDetallesFacturacion[id_servicio]'";
						$resServicio = mysqli_query($linkbd,$sqlServicio);
						while($rowServicio = mysqli_fetch_assoc($resServicio))
						{
							$nombreServicio[] = $rowServicio['nombre'];
						}
					}

					//base srvdetalles_facturacion
					$sqlDetallesFacturacion = "SELECT id_servicio, id_tipo_cobro, credito, debito FROM srvdetalles_facturacion WHERE numero_facturacion = '$rowReciboCaja[numero_factura]' AND tipo_movimiento = '101'";
					$resDetallesFacturacion = mysqli_query($linkbd,$sqlDetallesFacturacion);
					while($rowDetallesFacturacion = mysqli_fetch_assoc($resDetallesFacturacion))
					{
						switch($rowDetallesFacturacion['id_tipo_cobro'])
						{
							case 1:
								$cargoFijo[] = $rowDetallesFacturacion['credito'];
								break;

							case 2:
								$consumoBasico[] = $rowDetallesFacturacion['credito'];
								break;

							case 3:
								$consumoConMedidor[] = $rowDetallesFacturacion['credito'];
								break;
							
							case 5:
								$subsidio[] = $rowDetallesFacturacion['debito'];
								break;

							case 6: 
								$exoneracion[] = $rowDetallesFacturacion['debito'];
								break;
							
							case 7:
								$contribucion[] = $rowDetallesFacturacion['credito'];
								break;
							
							case 8:
								$saldoFacturaVencida[] = $rowDetallesFacturacion['credito'];
								break;
							
							case 9: 
								$acuerdoPago[] = $rowDetallesFacturacion['credito'];
								break;
							case 10:
								$saldoFacturaVencida[] = $rowDetallesFacturacion['credito'];
								break;

							case 11:
								$interesMoratorio[] = $rowDetallesFacturacion['credito'];
								break;
						}
						$c++;
					}

					for ($i=0; $i <count($cargoFijo) ; $i++) 
					{ 
						//valor total
						$valorReal[] = $cargoFijo[$i] + $consumoBasico[$i] + $consumoConMedidor[$i] + $contribucion[$i] + $saldoFacturaVencida[$i] + $acuerdoPago[$i] - $subsidio[$i] - $exoneracion[$i];

						$_POST['codigoServicio'][]   = $codigoServicio[$i];
						$_POST['nombreServicio'][]   = $nombreServicio[$i];
						$_POST['cargoFijo'][]	     = $cargoFijo[$i];
						$_POST['conMedidor'][]	     = $consumoConMedidor[$i];
						$_POST['sinMedidor'][]	     = $consumoBasico[$i];
						$_POST['interesMoratorio'][] = $interesMoratorio[$i];
						$_POST['contribuciones'][]   = $contribucion[$i];
						$_POST['subsidios'][] 	     = $subsidio[$i];
						$_POST['exoneraciones'][]    = $exoneracion[$i];
						$_POST['saldo'][]		     = $saldoFacturaVencida[$i];
						$_POST['acuerdo'][]		     = $acuerdoPago[$i];
						$_POST['valorReal'][]	     = $valorReal[$i];	
					}
				}
			?>

			<table class="inicio ancho">
				<tr>
					<td class="titulos" colspan="8">.: Recibo de Caja</td>

					<td class="cerrar" style="width:7%" onClick="location.href='serv-principal.php'">Cerrar</td>
				</tr>

				<tr>
                    <td class="tamano01" style="width:10%;">Consecutivo:</td>

                    <td style="width:15%;">
                        <input  type="text" name="consecutivo" id="consecutivo" value="<?php echo @ $_POST['consecutivo'];?>" style="width:100%;height:30px;text-align:center;" readonly/>
                    </td>

                    <td class="tamano01" style="width:10%;">Corte:</td>

                    <td style="width:15%;">
						<input  type="text" name="corte" id="corte" value="<?php echo @ $_POST['corte'];?>" style="width:100%;height:30px;text-align:center;" readonly/>
					</td>

                    <td class="tamano01" style="width:10%;">Fecha de Pago</td>

					<td style="width:15%;">
                        <input type="text" name="fecha_pago" id="fecha_pago" value="<?php echo @ $_POST['fecha_pago']?>" maxlength="10" onKeyUp="return tabular(event,this)"  id="fc_1198971545" onKeyDown="mascara(this,'/',patron,true)" title="DD/MM/YYYY" style="height:30px;text-align:center;width:85%;" readonly/>
                    </td>

                    <td class="tamano01" style="width: 8%;">Estado de Pago:</td>

                    <td>
                        <input type="text" name="estadoPago" id="estadoPago" style="<?php echo $color;?>" value="<?php echo @$_POST['estadoPago']; ?>" readonly>
                    </td>
                    
                </tr>
                
                <tr>
					<td class="tamano01">Recaudo en: </td>

                    <td>
                        <input type="text" name="modoRecaudo" id="modoRecaudo" value="<?php echo $_POST['modoRecaudo'] ?>" style="width:100%; height:30px; text-align:center;" readonly>
					</td>

                    <td class="tamano01">Tipo de Movimiento:</td>

                    <td>
                        <input type="text" name="tipoMovimiento" id="tipoMovimiento" style="width: 100%; height: 30px; text-align:center;" value="<?php echo $_POST['tipoMovimiento']?>" readonly>
                    </td>

                    <td class="tamano01" style="width:2cm;">N&deg; de Factura:</td>

					<td style="width:15%;">
						<input  type="text" name="numeroFactura" id="numeroFactura" value="<?php echo @ $_POST['numeroFactura'];?>" style="width:100%; height:30px; text-align:center" onChange='buscarFactura()' readonly />

						<input type="hidden" value="" name="bc" id="bc">
					</td>
                </tr>

				<tr>
					<td class="saludo1">Cuenta Recaudo: </td> 

					<td>
						<input type="text" id="banco" name="banco" style="width:100%; text-align:center;" onKeyPress="javascript:return solonumeros(event)" onKeyUp="return tabular(event,this)" onBlur="" value="<?php echo $_POST['banco']?>" onClick="document.getElementById('banco').focus();document.getElementById('banco').select();" readonly>

                        <input type="hidden" name="cuentaBancaria" id="cuentaBancaria" value="">
					</td>

					<td colspan="2">
						<input type="text" name="nbanco" id="nbanco" style="width:100%;" value="<?php echo $_POST['nbanco']?>"  readonly>
					</td>
				</tr>

				<tr>
					<td class="tamano01" style="width:2cm;">Concepto:</td>

					<td style="width:15%;" colspan="3">
						<input  type="text" name="concepto" id="concepto" value="<?php echo @ $_POST['concepto'];?>" style="width:100%;height:30px;" readonly/>
					</td>

					<td class="tamano01" style="width:2cm;">Valor:</td>

					<td style="width:15%;">
						<input  type="text" name="valor" id="valor" value="<?php echo @ $_POST['valor'];?>" style="width:100%; height:30px; text-align:center;" readonly/>
					</td>
				</tr>

				<tr>
					<td class="tamano01" style="width:2cm;">Documento:</td>

					<td style="width:15%;">
						<input  type="text" name="documento" id="documento" value="<?php echo @ $_POST['documento'];?>" style="width:100%; height:30px; text-align:center;" readonly/>
					</td>

					<td class="tamano01" style="width:2cm;">Contribuyente:</td>

					<td style="width:15%;" colspan="3">
						<input type="text" name="nombreCliente" id="nombreCliente" value="<?php echo @ $_POST['nombreCliente'];?>" style="width:100%;height:30px;"/>
					</td>
				</tr>

				<tr>
                    <td class="tamano01" style="width: 3cm;">Descripci&oacute;n:</td>
                    
                    <td colspan="7">
                        <input type="text" name="descripcion" id="descripcion" value="<?php echo $_POST['descripcion'] ?>" style="width:100%;height:30px;"> 
                    </td>
                </tr>
			</table>

			<table class="inicio">

                <?php
                    if(@$_POST['oculto'])
                    {
                        
                    }
                ?>

                <tr><td class="titulos" colspan="12">.: Detalles de la Factura Pagada</td></tr>

                <tr class="titulos2">
                    <td style="text-align:center;">C&oacute;digo</td>
                    <td style="text-align:center;">Servicio</td>
                    <td style="text-align:center;">Cargo Fijo</td>
                    <td style="text-align:center;">Cobro con medidor</td>
                    <td style="text-align:center;">Cobro sin medidor</td>
                    <td style="text-align:center;">Contribuciones</td>
                    <td style="text-align:center;">Subsidios</td>
                    <td style="text-align:center;">Exoneraciones</td>
                    <td style="text-align:center;">Saldos</td>
					<td style="text-align: center;">Interes Moratorio</td>
					<td style="text-align:center;">Acuerdos de Pago</td>
                    <td style="text-align:center;">Valor real</td>
                </tr>

                <?php
					echo "
						
					";


					for ($x=0;$x< count($_POST['codigoServicio']);$x++)
					{
						echo "
							<input type='hidden' name='codigoServicio[]' value='".$_POST['codigoServicio'][$x]."'/>
							<input type='hidden' name='nombreServicio[]' value='".$_POST['nombreServicio'][$x]."'/>
							<input type='hidden' name='valorReal[]' value='".$_POST['valorReal'][$x]."'/>

							<tr class='saludo1a'>
								<td class='' style='text-align:center;width:5%;'>".$_POST['codigoServicio'][$x]."</td>

								<td class='' style='text-align:center;width:10%;'>".$_POST['nombreServicio'][$x]."</td>

								<td class='' style='text-align:center;width:9%;'>".number_format($_POST['cargoFijo'][$x],2,',','.')."</td>
								
								<td class='' style='text-align:center;width:9%;'>".number_format($_POST['conMedidor'][$x],2,',','.')."</td>
							
								<td class='' style='text-align:center;width:9%;'>".number_format($_POST['sinMedidor'][$x],2,',','.')."</td>

								<td class='' style='text-align:center;width:9%;'>".number_format($_POST['contribuciones'][$x],2,',','.')."</td>
									
								<td class='' style='text-align:center;width:9%;'>".number_format($_POST['subsidios'][$x],2,',','.')."</td>
					
								<td class='' style='text-align:center;width:9%;'>".number_format($_POST['exoneraciones'][$x],2,',','.')."</td>
							
								<td class='' style='text-align:center;width:9%;'>".number_format($_POST['saldo'][$x],2,',','.')."</td>

								<td class='' style='text-align:center;width:9%;'>".number_format($_POST['interesMoratorio'][$x],2,',','.')."</td>
								
								<td class='' style='text-align:center;width:9%;'>".number_format($_POST['acuerdo'][$x],2,',','.')."</td>
							
								<td class='' style='text-align:center;width:9%;'>".number_format($_POST['valorReal'][$x],2,',','.')."</td>
							</tr>";
					}
					
					echo "
					<tr class='titulos2'>
						<td style='text-align:center;' colspan='2'>Total:</td>

						<td style='text-align:center;'>".number_format($_POST['totalCargoFijo'],2,',','.')."</td>
						
						<td style='text-align:center;'>".number_format($_POST['totalCobroConMedidor'],2,',','.')."</td>
						
						<td style='text-align:center;'>".number_format($_POST['totalCobroSinMedidor'],2,',','.')."</td>

						<td style='text-align:center;'>".number_format($_POST['totalContribuciones'],2,',','.')."</td>
							
						<td style='text-align:center;'>".number_format($_POST['totalSubsidios'],2,',','.')."</td>
							
						<td style='text-align:center;'>".number_format($_POST['totalExoneraciones'],2,',','.')."</td>

						<td style='text-align:center;'>".number_format($_POST['totalSaldo'],2,',','.')."</td>

						<td style='text-align:center;'>".number_format($_POST['totalInteresMoratorio'],2,',','.')."</td>

						<td style='text-align:center;'>".number_format($_POST['totalAcuerdo'],2,',','.')."</td>

						<td style='text-align:center;'>".number_format($_POST['totalValorReal'],2,',','.')."</td>
					</tr>";    
			?>
            </table>
			
			<input type="hidden" name="oculto" id="oculto" value="1"/>
            <input type="hidden" name="id_cliente" id="id_cliente" value="<?php echo $_POST['id_cliente'] ?>"/>
			<input type="hidden" name="letras" id="letras" value="<?php echo @$_POST['letras']?>"/>

			<?php 
				if(@$_POST['oculto']=="2")
				{
					
				}
			?>
		</form>
		<div id="bgventanamodal2">
			<div id="ventanamodal2">
				<IFRAME  src="" name="buster" marginWidth=0 marginHeight=0 frameBorder=0 id="ventana2" frameSpacing=0 style="left:500px; width:900px; height:500px; top:200;"></IFRAME>
			</div>
		</div>
	</body>
</html>