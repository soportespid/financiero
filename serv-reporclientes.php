<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=iso-8859-1");
	require"comun.inc";
	require"funciones.inc";
	require"serviciospublicos.inc";
	session_start();
	$linkbd=conectar_v7();
	cargarcodigopag($_GET[codpag],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es">
	<head>
		<meta http-equiv="Content-Type" content="text/html" charset="iso-8859-1"/>
		<meta http-equiv="X-UA-Compatible" content="IE=9"/>
		<title>:: SPID - Servicios Publicos</title>
		<link href="css/css2.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<script type="text/javascript" src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script type="text/javascript" src="css/calendario.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script>
			function validar()
			{
				if (isNaN(form2.cod_usuario.value))
				{
					alert ("Solo se pueden digitar numeros");
					form2.cod_usuario.focus();
				}
				if(form2.cod_usuario.value!="")
				{
					document.form2.oculto.value='1'
					document.form2.submit();}
			}
			function pdf()
			{
				document.form2.action="pdfreporteclientes.php";
				document.form2.target="_BLANK";
				document.form2.submit(); 
				document.form2.action="";
				document.form2.target="";
			}
		</script>
		<?php titlepag();?>
	</head>
	<body>
		<IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
		<span id="todastablas2"></span>
		<table>
			<tr><script>barra_imagenes("serv");</script><?php cuadro_titulos();?></tr>
			<tr><?php menu_desplegable("serv");?></tr>
			<tr>
				<td colspan="3" class="cinta"><img src="imagenes/add.png" title="Nuevo" onClick="location.href='serv-reporclientes.php'" class="mgbt"/><img src="imagenes/guardad.png" class="mgbt1"/><img src="imagenes/buscad.png" title="Buscar" class="mgbt1"/><img src="imagenes/nv.png" title="Nueva Ventana" onClick="mypop=window.open('serv-principal.php','','');mypop.focus();" class="mgbt"/><img src="imagenes/csv.png" title="csv"  onClick="location.href='<?php echo "archivos/".$_SESSION['usuario']."reporteclientes.csv"; ?>'" class="mgbt"/><img src="imagenes/print.png" title="imprimir" onClick="pdf()" class="mgbt"/></td>
			</tr>
		</table>
		<form name="form2" method="post" action="serv-reporclientes.php">
			<table class="inicio" align="center" >
				<tr>
					<td class="titulos" colspan="10">:. Buscar Clientes </td>
					<td class="cerrar" style="width:7%" onClick="location.href='serv-principal.php'">Cerrar</td>
				</tr>
				<tr>
					<td width="80" class="saludo1">Cod. Usuario:</td>
					<td><input name="cod_usuario" type="text" id="cod_usuario" value="<?php echo @ $_POST['cod_usuario']?>"/>&nbsp;<img src="imagenes/buscarep.png" title="Buscar" onClick="mypop=window.open('clientes_ventana.php?ti=1','','menubar=0,scrollbars=yes, toolbar=no, location=no, width=700px,height=500px');mypop.focus();"/></td>
					<td width="100" class="saludo1">Nombre Usuario: </td>
					<td><input name="cedula_usuario" type="text" id="cedula_usuario" value="<?php echo @ $_POST['cedula_usuario']?>" size="15" readonly/><input name="nombre_usuario" type="text" id="nombre_usuario" value="<?php echo @ $_POST['nombre_usuario']?>" size="40" readonly/></td>
					<td width="100" class="saludo1">Estado Facturas: </td>
					<td>
						<select name="estado_factura" id"estado_factura"> 
							<option value="">::: Seleccione Estado ::: </option>
							<option value="S">ACTIVAS</option>
							<option value="P">PAGADAS </option>
							<option value="V">VENCIDAS </option>
							<option value="N">ANULADAS</option>
						</select>
					</td>
					<input name="oculto" type="hidden" value="0">
					<td><input type="button" name="buscar" id="buscar" value="  Buscar  " onClick="validar()"></td>    
				</tr>
			</table>
			<div class="subpantalla" style="height:68%; width:99.6%; overflow-x:hidden;">
				<?php
					if($_POST['oculto']=='1')
					{
						$crit1=" ";
						$con=1;
						$_POST['tercero'] = $_POST['nombre_usuario'];
						//$_POST['tercero'] = buscatercero($_POST['cedula_usuario']);
						$sqlr1="SELECT T1.id_factura,T1.estado,T1.liquidaciongen,T1.vigencia,T1.mes,T1.mes2,T2.id_recibos, T2.fecha,T2.estado, (SELECT SUM(valorliquidacion) FROM servliquidaciones_det WHERE id_liquidacion = T1.id_factura and codusuario='".$_POST['cod_usuario']."'), (SELECT SUM(saldo) FROM servliquidaciones WHERE id_liquidacion = T1.id_factura and codusuario='".$_POST['cod_usuario']."'), T2.valor FROM servfacturas AS T1 LEFT JOIN servreciboscaja AS T2 ON T1.id_factura = T2.id_recaudo WHERE T1.codusuario = '".$_POST['cod_usuario']."' and T1.estado LIKE '%".$_POST['estado_factura']."%' ORDER BY T1.id_factura DESC";
						/*
						*/
						$resp1 = mysqli_query($linkbd,$sqlr1);
						$ntr = mysqli_num_rows($resp1);
						echo "
						<table class='inicio' align='center' >
							<tr><td colspan='14' class='titulos'>.: Resultados Busqueda:</td></tr>
							<tr><td colspan='2'>Ingresos Encontrados: $ntr</td></tr>
							<tr>
								<td width='' class='titulos2'>Item</td>
								<td width='' class='titulos2'>Numero Factura</td>
								<td width='' class='titulos2'>Ciclo</td>
								<td width='' class='titulos2'>Vigencia</td>
								<td width='' class='titulos2'>Mes Inicial</td>
								<td width='' class='titulos2'>Mes Final</td>
								<td width='' class='titulos2'>Documento Usuario</td>
								<td width=''class='titulos2'>Nombre Usuario</td>
								<td width='' class='titulos2'>Valor</td>
								<td width='' class='titulos2'>Estado Factura</td>
								<td width='' class='titulos2'>Recibo Caja</td>
								<td width='' class='titulos2'>Valor Recibo</td>
								<td width='' class='titulos2'>Fecha Recibo</td>
								<td width='' class='titulos2'>Estado Recibo</td>
							</tr>";
						$iter='saludo1a';
						$iter2='saludo2';
						$namearch="archivos/".$_SESSION['usuario']."reporteclientes.csv";
						$Descriptor1 = fopen($namearch,"w+"); 
						fputs($Descriptor1,"NUMERO FACTURA;CICLO;VIGENCIA;MES INICIAL;MES FINAL;COD USUARIO;NOMBRE USUARIO;VALOR;ESTADO FACTURA;RECIBO CAJA;VALOR RECIBO;FECHA RECIBO;ESTADO RECIBO\r\n");
						while ($row =mysqli_fetch_row($resp1))
						{
							if ($row[1]=='P'){$estadofac = "<img src='imagenes/dinero3.png'title='Pagada' />";}
							if ($row[1]=='A'){$estadofac = "<img src='imagenes/abonos.png' title='Abono' />";}
							if ($row[1]=='V'){$estadofac = "<img src='imagenes/dinerob3.png' title='Vencida' />";}
							if ($row[1]=='S'){$estadofac = "<img src='imagenes/confirm22.png' title='Activa' />";}
							if ($row[1]=='N'){$estadofac = "<img src='imagenes/del2.png' title='Anulada' />";}
							if ($row[8]== '' || $row[8] == NULL){$estadorec = "------";}
							if ($row[8]=='S'){$estadorec ="<img src='imagenes/confirm22.png' title='Pagada' />";}
							if ($row[6]== '' || $row[6] == NULL){$row[6]= "------";}
							if ($row[7]== '' || $row[7] == NULL){$row[7]= "------";}
							echo "
							<input type='hidden' name='numeros_fac[]' value= '".$row[0]."'/>
							<input type='hidden' name='ciclos[]' value= '".$row[2]."'/>
							<input type='hidden' name='vigencias[]' value= '".$row[3]."'/>
							<input type='hidden' name='meses[]' value= '".$row[4]."'/>
							<input type='hidden' name='mesesfinales[]' value= '".$row[5]."'>
							<input type='hidden' name='codigos[]' value= '".$_POST['cod_usuario']."'>
							<input type='hidden' name='terceros' value= '".$_POST['tercero']."'>
							<input type='hidden' name='valores[]' value= '".$row[9]."'>
							<input type='hidden' name='valoresreci[]' value= '".$row[11]."'>
							<input type='hidden' name='estados_fac[]' value= '".$row[1]."'>
							<input type='hidden' name='recibos_caja[]' value= '".$row[6]."'>
							<input type='hidden' name='fechas_recibos[]' value= '".$row[7]."'>
							<input type='hidden' name='estados_rec[]' value= '".$row[8]."'>
							<tr class='$iter'style='text-transform:uppercase'>
								<td>$con</td>
								<td>$row[0]</td>
								<td>$row[2]</td>
								<td>$row[3]</td>
								<td>$row[4]</td>
								<td>$row[5]</td>
								<td>".$_POST['cedula_usuario']."</td>
								<td>".$_POST['tercero']."</td>
								<td align='right'>$".number_format(($row[9]+$row[10]),2,",",".")."</td>
								<td align='center'>$estadofac</td>
								<td align='center'>$row[6]</td>
								<td align='right'>$".number_format($row[11],2,",",".")."</td>
								<td align='center'>$row[7]</td>
								<td align='center'>$estadorec</td>
							</tr>";
							fputs($Descriptor1,$row[0].";".$row[2].";".$row[3].";".$row[4].";".$row[5].";".$_POST['cod_usuario']. ";".$_POST['tercero'].";".($row[9]+$row[10]).";".$row[1].";".$row[6].";".$row[11].";".$row[7].";". $row[8]."\r\n");
							$con+=1;
							$aux=$iter;
							$iter=$iter2;
							$iter2=$aux;
						}
						fclose($Descriptor1);
						echo"</table>";
					}
				?>
			</div>
		</form>
	</body>
</html>