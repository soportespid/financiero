<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require 'comun.inc';
	require 'funciones.inc';
	$linkbd=conectar_v7();
	$linkbd -> set_charset("utf8");
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html lang=es>
	<head>
		<meta charset="utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1"/>
		<meta name="viewport" content="user-scalable=no">
		<title>:: SieS</title>
		<link href="css/css2z.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="sweetalert2/dist/sweetalert2.min.css" rel="stylesheet" />
		<script src="sweetalert2/dist/sweetalert2.min.js"></script>
		<script type="text/javascript" src="css/calendario.js"></script>
		<script type="text/javascript" src="css/programas.js"></script>
		<script>
			function guardar (){
				Swal.fire({
					title: '¿Seguro que desea guardar?',
					showDenyButton: true,
					confirmButtonText: 'Guardar',
					confirmButtonColor: '#01CC42',
					denyButtonText: 'Cancelar',
					denyButtonColor: '#FF121A',
				}).then(
					(result) => {
						if (result.isConfirmed){
							document.form2.oculto.value = 2; 
							document.form2.submit();
						}
						else if (result.isDenied){
							Swal.fire({
								title: 'No se guardo la información',
								confirmButtonText: 'Continuar',
								confirmButtonColor: '#FF121A',
								timer: 2500
							});
						}
					}
				)
			}
		</script> 
		<?php titlepag();?>
	</head>
	<body>
		<form name="form2" method="post">
			<?php
				if($_POST['oculto']==""){
					$_POST['ncuenta'] = $_GET['cuenta'];
					$sql = "SELECT detalle_sectorial FROM ccpecuentasdsectorial WHERE cuenta = '".$_POST['ncuenta']."'";
					$res = mysqli_query($linkbd,$sql);
					$_POST['nuevodoc'] = mysqli_num_rows($res);
					if($_POST['nuevodoc'] > 0){
						$row = mysqli_fetch_row($res);
						$_POST['dsectorial'] = $row[0];
					}
				}
			?>
			<table class="inicio ancho" style="width:99.5%">
				<tr>
					<td class="titulos" colspan="6">:: Informacion Normas</td>
					<td class="cerrar" style="width:7%;"><a onClick="window.close();">&nbsp;Cerrar</a></td>
				</tr>
				<tr>
					<td class="tamano01" style='width:2.5cm;'>:: Cuenta:</td>
					<td colspan="3"><input type="input" name="ncuenta" id="ncuenta" value="<?php echo $_POST['ncuenta'];?>" style='width:100%;' readonly/></td>
					<td >
				</tr>
				<tr>
					<td class="tamano01">Detalle Sectorial:</td>
					<td colspan="3">
						<select name="dsectorial" id="dsectorial" style="width:100%;height:30px;">
							<option value="">Seleccione ....</option>
							<?php
								$sqlr="SELECT codigo, concepto, grupo, sector FROM ppto_codigos_sectoriales";
								$resp = mysqli_query($linkbd,$sqlr);
								while ($row = mysqli_fetch_row($resp)){
									if($row[0] == $_POST['dsectorial']){
										echo "<option value='$row[0]' SELECTED>$row[0] - $row[1]</option>";
									}else{
										echo "<option value='$row[0]'>$row[0] - $row[1]</option>";
									}
								}
							?>
						</select>
					</td>
				</tr>
				<tr>
					<td colspan="2" style="height:35px;"><em class="botonflechaverde" onClick="guardar()">Guardar</em></td>
				</tr>
			</table> 
			<input type="hidden" name="oculto" id="oculto" value="1"/>
			<input type="hidden" name="nuevodoc" id="nuevodoc" value="<?php echo $_POST['nuevodoc']; ?>"/>
			<?php
				if($_POST['oculto'] == 2){
					if($_POST['nuevodoc'] == 0){
						$sqlr = "INSERT INTO ccpecuentasdsectorial(cuenta, detalle_sectorial, estado) VALUES ('".$_POST['ncuenta']."', '".$_POST['dsectorial']."', 'S')";
						mysqli_query($linkbd,$sqlr);
					}else{
						$sqlr = "UPDATE ccpecuentasdsectorial SET detalle_sectorial = '".$_POST['dsectorial']."' WHERE cuenta = '".$_POST['ncuenta']."'";
						mysqli_query($linkbd,$sqlr);
					}
					echo"
						<script>
							Swal.fire({
								icon: 'success',
								title: 'Se guardo con exito  la información de la cuenta N°:".$_POST['ncuenta']."',
								confirmButtonText: 'Continuar',
								confirmButtonColor: '#FF121A',
								timer: 3000
							});
						</script>";
				}
			?>
		</form>
	</body>
</html>
