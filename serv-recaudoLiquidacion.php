<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=uft8");
	require "comun.inc";
	require "funciones.inc";
    require "funcionesSP.inc.php";
	session_start();
	$linkbd=conectar_v7();
	$linkbd -> set_charset("utf8");
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es">
	<head>
		<meta http-equiv="Content-Type" content="text/html" charset="utf8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=9"/>
		<title>:: Reporte Liquidación</title>
		<link href="css/css2.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
        <link href="css/cssSP.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<script type="text/javascript" src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
        <script type="text/javascript" src="jquery-1.11.0.min.js"></script> 
        
		<script>
			$(window).load(function () {
				$('#cargando').hide();
			});

			function generarfacturas()
			{
				var corte = parseInt(document.getElementById('corte').value);

				if((corte != '-1'))
				{
					document.form2.oculto.value='2';
					document.form2.submit();
				}
			}

			function actualizar()
			{
				document.form2.submit();
			}

			function excell()
			{
				document.form2.action="serv-reporteLiquidacionSIGLOXXIExcel.php";
				document.form2.target="_BLANK";
				document.form2.submit(); 
				document.form2.action="";
				document.form2.target="";
			}
		</script> 
		<?php titlepag();?>
	</head>
	<body>
        <IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
		<span id="todastablas2"></span>
		<table>
			<tr><script>barra_imagenes("serv");</script><?php cuadro_titulos();?></tr>	 
			<tr><?php menu_desplegable("serv");?></tr>
			<tr>
				<td colspan="3" class="cinta">
					<a class="mgbt"><img src="imagenes/add2.png"/></a>
					<a class="mgbt"><img src="imagenes/guardad.png" title="Guardar"/></a>
					<a href="" class="mgbt"><img src="imagenes/busca.png" title="Buscar"/></a>
					<a onclick="mypop=window.open('plan-agenda.php','','');mypop.focus()" class="mgbt"><img src="imagenes/agenda1.png" title="Agenda" /></a>
					<a onclick="mypop=window.open('serv-principal.php','','');mypop.focus();" class="mgbt"><img src="imagenes/nv.png" title="Nueva ventana"></a>
					<a onclick="mypop=window.open('<?php echo $url2; ?>','','');mypop.focus();" class="mgbt"><img src="imagenes/duplicar_pantalla.png" title="Duplicar pesta&ntilde;a"></a>
					<a href=""><img src="imagenes/excel.png" title="Excel" onClick="excell()" class="mgbt"></td></a>
                </td>
			</tr>
		</table>

		<form name="form2" method="post">
			<table class="inicio ancho" style="width:99.5%">
				<tr>
					<td class="titulos" colspan="6">Recaudo Ventanilla: </td>
					<td class="cerrar" style="width:7%" onClick="location.href='serv-principal.php'">Cerrar</td>
				</tr>

                <tr>
                    <td class="tamano01" style="width:3cm;">Código de Factura:</td>

					<td>
						<input type="text" name="numeroFactura" id="numeroFactura" value="<?php echo @ $_POST['numeroFactura'];?>" style="width:85%;height:30px;" onblur='buscarFactura()'/>
                        <a title="Facturas disponibles" onClick="" style="cursor:pointer;">
                        <img src="imagenes/find02.png" style="width:20px;"/></a>
						<input type="hidden" value="" name="bc" id="bc">
					</td>
                </tr>

                <tr>
                    <td class="tamano01" style="width:10%;">Consecutivo:</td>

                    <td style="width:15%;">
                        <input  type="text" name="consecutivo" id="consecutivo" value="<?php echo @ $_POST['consecutivo'];?>" style="width:100%;height:30px;text-align:center;" readonly/>
                    </td>

                    <td class="tamano01" style="width:10%;">Corte Liquidado:</td>

                    <td style="width:15%;">
						<input  type="text" name="corte" id="corte" value="<?php echo @ $_POST['corte'];?>" style="width:100%;height:30px;text-align:center;" readonly/>
					</td>

                    <td class="tamano01" style="width:10%;">Fecha de Pago</td>

					<td style="width:15%;">
                        <input type="text" name="fecha_pago" id="fecha_pago" value="<?php echo @ $_POST['fecha_pago']?>" maxlength="10" onKeyUp="return tabular(event,this)"  id="fc_1198971545" onKeyDown="mascara(this,'/',patron,true)" title="DD/MM/YYYY" style="height:30px;text-align:center;width:85%;" readonly/>
                        
                        <a href="#" onClick="displayCalendarFor('fecha_pago');" title="Calendario">
                            <img src="imagenes/calendario04.png" style="width:25px;">
                        </a>
                    </td>
                </tr>
			</table>

            <div id="cargando" style=" position:absolute;left: 46%; bottom: 45%">
				<img src="imagenes/loading.gif" style=" width: 80px; height: 80px"/>
			</div>

			<div class="subpantalla" style="height:71%; width:99.2%;">
				<table class='inicio' align='center' width='99%'>
					<tr>
						<td colspan='90' class='titulos'>Resultados Busqueda: </td>
					</tr>
				
					<tr class='titulos2' style='text-align:center;'>
                        <td style="width: 5%;">Numero Factura</td>
						<td style="width: 5%;">Código Usuario</td>
						<td>Nombre Suscriptor</td>
						<td style="width: 5%;">Barrio</td>
                        <td style="width: 9%;">Dirección</td>
						<td style="width: 2%;">Ruta</td>
						<td style="width: 9%;">Código Ruta</td>
						<td style="width: 5%;">Total Factura</td>
					</tr>

					<?php
						if(@ $_POST['oculto']=="2")
						{
						
						}
					?>
				</table>
			</div>

			<input type="hidden" name="oculto" id="oculto" value="1"/>
		</form>
	</body>
</html>
