<!DOCTYPE HTML>
    <html>
        <head>
            <title>Timeline</title>
            <script src="https://cdn.jsdelivr.net/npm/vue@2.5.16/dist/vue.js"></script>
            <script src="https://unpkg.com/vue-multiselect@2.1.0"></script>
            <link rel="stylesheet" href="https://unpkg.com/vue-multiselect@2.1.0/dist/vue-multiselect.min.css">
            
        </head>

        <body>
            <div id="app">

                <div>
                    <multiselect
                    v-model="value"
                    placeholder="city name?"
                    label="country" track-by="country"
                    :options="options"
                    :multiple="true"
                    :taggable="false"
                    ></multiselect>
                </div>

                <pre class="language-json"><code>{{ value  }}</code></pre>

            </div>

            <script type="module" src="./multiselect.js"></script>
        </body>
    </html>