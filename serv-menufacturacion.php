<?php
	require"comun.inc";
	require"funciones.inc";
	session_start();
	$linkbd=conectar_bd();	
	cargarcodigopag($_GET[codpag],$_SESSION["nivel"]);
	header("Cache-control: private"); // Arregla IE 6
	header("Content-Type: text/html;charset=iso-8859-1");
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es">
	<head>
	 	<meta http-equiv="Content-type" content="text/html; charset=iso-8859-1"/>
        <meta http-equiv="X-UA-Compatible" content="IE=9"/>
		<title>:: Spid - Servicios P&uacute;blicos</title>
        <link href="css/css2.css" rel="stylesheet" type="text/css" />
        <link href="css/css3.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="css/programas.js"></script>
		<?php titlepag();?>
    </head>
    <body>
        <IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
        <span id="todastablas2"></span>
        <table>
            <tr><script>barra_imagenes("serv");</script><?php cuadro_titulos();?></tr>	 
            <tr><?php menu_desplegable("serv");?></tr>
        	<tr>
          		<td colspan="3" class="cinta"><img src="imagenes/add2.png" class="mgbt1"/><img src="imagenes/guardad.png" style="width:24px;" class="mgbt1"/><img src="imagenes/buscad.png" class="mgbt1"/><img src="imagenes/nv.png" title="Nueva Ventana" class="mgbt" onClick="mypop=window.open('serv-principal.php','','');mypop.focus();"/></td>
        	</tr>
        </table>
 		<form name="form2" method="post" action="">
    		<table class="inicio">
      			<tr>
        			<td class="titulos" colspan="2">.: Men&uacute; Tramites Facturaci&oacute;n</td>
                    <td class="cerrar" style="width:7%"><a onClick="location.href='serv-principal.php'">Cerrar</a></td>
      			</tr>
                <tr>
                	<td class="titulos2">&nbsp;Gesti&oacute;n Facturaci&oacute;n</td>
                    <td class="titulos2">&nbsp;Par&aacute;metros Facturaci&oacute;n</td>
                </tr>
				<tr>
                    <td class='saludo1' width='45%'>
             			<ol id="lista2">
  							<li onClick="location.href='serv-cortefact.php?menu=SI'" style="cursor:pointer">Corte Facturaci&oacute;n</li>
                 			<li onClick="location.href='serv-liquidarservicios.php?menu=SI'" style="cursor:pointer">Liquidar Servici&oacute;s</li>
                            <li onClick="location.href='serv-facturarservicios.php?menu=SI'" style="cursor:pointer">Facturar Servici&oacute;s</li>
                            <li onClick="location.href='serv-facturaver.php?menu=SI'" style="cursor:pointer">Imprimir Facturas</li>
                            <li onClick="location.href='serv-facturasbuscar.php?menu=SI'" style="cursor:pointer">Ver y Editar Facturas</li>
                            <li onClick="location.href='serv-prescripciones.php?menu=SI'" style="cursor:pointer">Prescripci&oacute;n de Facturas</li>
                        </ol>
                    </td>
                    <td class='saludo1'>
                        <ol id="lista2">
                        	
                        </ol>
                    <td>
				</tr>							
    		</table>
		</form>
	</body>
</html>