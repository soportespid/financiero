<!--V 1.0 24/02/2015-->
<?php
	require"comun.inc";
	require"funciones.inc";
	session_start();
	$linkbd=conectar_bd();	
	cargarcodigopag($_GET[codpag],$_SESSION["nivel"]);
	header("Cache-control: private"); // Arregla IE 6
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es">
	<head>
	 	<meta http-equiv="Content-Type" content="text/html" charset="iso-8859-1"/>
        <meta http-equiv="X-UA-Compatible" content="IE=9"/>
		<title>:: Spid - Servicios P&uacute;blicos</title>
        <link href="css/css2.css" rel="stylesheet" type="text/css" />
		<link href="css/css3.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="css/programas.js"></script>
		<script>
			function fimprimir ()
			{
				document.form2.action="serv-facturasbuscarpdf.php";
				document.form2.target="_BLANK";
				document.form2.submit(); 
				document.form2.action="";
				document.form2.target="";
			}
			function fexportar ()
			{
				document.form2.action="serv-facturasbuscarexcel.php";
				document.form2.target="_BLANK";
				document.form2.submit(); 
				document.form2.action="";
				document.form2.target="";
			}
		</script>
        <?php titlepag();?>
	</head>
	<body>
		<IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
		<span id="todastablas2"></span>
		<table>
            <tr><script>barra_imagenes("serv");</script><?php cuadro_titulos();?></tr>	 
            <tr><?php menu_desplegable("serv");?></tr>
			<tr>
  				<td colspan="3" class="cinta"><img src="imagenes/add2.png" class="mgbt1"/><img src="imagenes/guardad.png" class="mgbt1"/><img src="imagenes/busca.png" title="Buscar" class="mgbt" onClick=""/><img src="imagenes/nv.png" title="Nueva Ventana" onClick="mypop=window.open('serv-principal.php','','');mypop.focus();" class="mgbt"/><img src="imagenes/print.png" title="Imprimir" onClick="fimprimir()" class="mgbt"/><img src="imagenes/excel.png" title="Exportar" onClick="fexportar()" class="mgbt"/><img src='imagenes/iratras.png' title="Men&uacute; Nomina" onClick="location.href='serv-menufacturacion.php'" class='mgbt'></td>
			</tr>
  		</table>
		<div id="bgventanamodalm" class="bgventanamodalm">
            <div id="ventanamodalm" class="ventanamodalm">
                <IFRAME src="" name="ventanam" marginWidth=0 marginHeight=0 frameBorder=0 id="ventanam" frameSpacing=0 style=" width:700px; height:130px; top:200; overflow:hidden;"> 
                </IFRAME>
            </div>
        </div>
        <form name="form2" method="post" action="">
			<?php
                if($_POST[oculto]=="")
                {
					$_POST[codusu]=$_GET[cusu];
                    if($_POST[nummul]=="")
                    {
                        $_POST[numres]=10;
                        $_POST[numpos]=$_POST[nummul]=0;
                    }
                }
            ?>
            <table  class="inicio" align="center" >
                <tr>
                    <td class="titulos" colspan="7">:: Buscar Factura</td>
                    <td class="cerrar" style="width:7%;"><a onClick="location.href='serv-principal.php'">&nbsp;Cerrar</a></td>
                </tr>
                <tr>
                    <td class="saludo1" style='width:3cm;'>N&uacute;mero Factura:</td>
                    <td style="width:10%;"><input type="search" name="nfactura" id="nfactura" value="<?php echo $_POST[nfactura];?>" style='width:100%;'/></td>
                    <td class="saludo1" style='width:3cm;'>C&oacute;digo Usuario:</td>
                    <td style="width:10%;"><input type="search" name="codusu" id="codusu" value="<?php echo $_POST[codusu];?>" style='width:100%;'/></td>
                    <td class="saludo1" style='width:3cm;'>N&deg; Documento:</td>
                    <td style="width:10%;"><input type="search" name="docusu" id="docusu" value="<?php echo $_POST[docusu];?>" style='width:100%;'/></td>
                    <td><input type="button" name="bboton" onClick="limbusquedas();" value="&nbsp;&nbsp;Buscar&nbsp;&nbsp;" /></td>
                </tr>                       
            </table>  
            <input type="hidden" name="oculto" id="oculto"  value="1"/>
            <input type="hidden" name="numres" id="numres" value="<?php echo $_POST[numres];?>"/>
    		<input type="hidden" name="numpos" id="numpos" value="<?php echo $_POST[numpos];?>"/>
       		<input type="hidden" name="nummul" id="nummul" value="<?php echo $_POST[nummul];?>"/>
    		<div class="subpantalla" style="height:68.5%; width:99.6%; overflow-x:hidden;" id="divdet">
      			<?php
					$crit1=$crit2=$crit3=$crit4="";
					if ($_POST[nfactura]!=""){$crit1="WHERE T1.id_liquidacion LIKE '$_POST[nfactura]' ";}
					if ($_POST[codusu]!="")
					{
						if ($_POST[nfactura]!=""){$crit2="AND T1.codusuario LIKE '%$_POST[codusu]%' ";}
						else {$crit2="WHERE T1.codusuario LIKE '%$_POST[codusu]%' ";}
					}
					if ($_POST[docusu]!="")
					{
						if ($_POST[nfactura]!="" || $_POST[codusu]!=""){$crit3="AND T1.tercero LIKE '".str_pad($_POST[docusu],10,"0", STR_PAD_LEFT)."' ";}
						else{$crit3="WHERE T1.tercero LIKE '".str_pad($_POST[docusu],10,"0", STR_PAD_LEFT)."' ";}
					}
					$sqlr="SELECT T1.id_liquidacion,(SELECT T2.nombretercero FROM servclientes T2 WHERE T2.codigo=T1.codusuario),(SELECT SUM(T3.valorliquidacion) FROM servliquidaciones_det T3 WHERE T3.id_liquidacion=T1.id_liquidacion) FROM servliquidaciones T1 $crit1 $crit2 $crit3";
					$resp = mysql_query($sqlr,$linkbd);
					$_POST[numtop]=mysql_num_rows($resp);
					$nuncilumnas=ceil($_POST[numtop]/$_POST[numres]);
					$cond2="";
					if ($_POST[numres]!="-1"){$cond2="LIMIT $_POST[numpos], $_POST[numres]";}
					$sqlr="SELECT T1.id_liquidacion,T1.liquidaciongen,T1.vigencia,T1.codusuario,T1.estado,(SELECT  concat_ws('<->', T2.nombretercero,T2.direccion) FROM servclientes T2 WHERE T2.codigo=T1.codusuario),(SELECT concat_ws('<->',SUM(T3.valorliquidacion),SUM(T3.abono)) FROM servliquidaciones_det T3 WHERE T3.id_liquidacion=T1.id_liquidacion) FROM servliquidaciones T1 $crit1 $crit2 $crit3 ORDER BY T1.id_liquidacion DESC $cond2";
					$resp = mysql_query($sqlr,$linkbd);
					$con=1;
					$numcontrol=$_POST[nummul]+1;
					if(($nuncilumnas==$numcontrol)||($_POST[numres]=="-1"))
					{
						$imagenforward="<img src='imagenes/forward02.png' style='width:17px;cursor:default;'>";
						$imagensforward="<img src='imagenes/skip_forward02.png' style='width:16px;cursor:default;' >";
					}
					else 
					{
						$imagenforward="<img src='imagenes/forward01.png' style='width:17px;cursor:pointer;' title='Siguiente' onClick='numsiguiente()'>";
						$imagensforward="<img src='imagenes/skip_forward01.png' style='width:16px;cursor:pointer;' title='Fin' onClick='saltocol(\"$nuncilumnas\")'>";
					}
					if(($_POST[numpos]==0)||($_POST[numres]=="-1"))
					{
						$imagenback="<img src='imagenes/back02.png' style='width:17px;cursor:default;'>";
						$imagensback="<img src='imagenes/skip_back02.png' style='width:16px;cursor:default;'>";
					}
					else
					{
						$imagenback="<img src='imagenes/back01.png' style='width:17px;cursor:pointer;' title='Anterior' onClick='numanterior();'>";
						$imagensback="<img src='imagenes/skip_back01.png' style='width:16px;cursor:pointer;' title='Inicio' onClick='saltocol(\"1\")'>";
					}
					echo "
					<table class='inicio' align='center' width='80%'>
						<tr>
							<td colspan='9' class='titulos'>.: Resultados Busqueda:</td>
							<td class='submenu'>
								<select name='renumres' id='renumres' onChange='cambionum();' style='width:100%'>
									<option value='10'"; if ($_POST[renumres]=='10'){echo 'selected';} echo ">10</option>
									<option value='20'"; if ($_POST[renumres]=='20'){echo 'selected';} echo ">20</option>
									<option value='30'"; if ($_POST[renumres]=='30'){echo 'selected';} echo ">30</option>
									<option value='50'"; if ($_POST[renumres]=='50'){echo 'selected';} echo ">50</option>
									<option value='100'"; if ($_POST[renumres]=='100'){echo 'selected';} echo ">100</option>
									<option value='-1'"; if ($_POST[renumres]=='-1'){echo 'selected';} echo ">Todos</option>
								</select>
							</td>
						</tr>
						<tr><td colspan='10'>Centro de Costos Encontrados: $_POST[numtop]</td></tr>
						<tr class='titulos2'>
							<td style='width:8%;'>N&deg; Factura</td>
							<td style='width:5%;'>N&deg; Corte</td>
							<td style='width:5%;'>Vigencia</td>
							<td style='width:8%;'>C&oacute;digo Usuario</td>
							<td>Propietario</td>
							<td>Direcci&oacute;n</td>
							<td style='width:10%;'>Valor</td>
							<td style='width:8%;'>Pagos y Abonos</td>
							<td style='width:5%;'>Estado</td>
							<td style='width:5%;'>Ver</td>
						</tr>";	
					$iter='saludo1a';
					$iter2='saludo2';
					$filas=1;
					while ($row =mysql_fetch_row($resp)) 
					{
						$sqlrrp="SELECT SUM(saldo), SUM(valorliquidacion) FROM servliquidaciones_det WHERE id_liquidacion='$row[0]'";
						$resprp = mysql_query($sqlrrp,$linkbd);
						$rowrp =mysql_fetch_row($resprp);
						if(($rowrp[1]+$rowrp[0])>=0)
						{
							$divvalores=explode('<->', $row[6]);
							$abonos=$divvalores[1];
						}
						else
						{
							$divvalores=0;
							$abonos=0;
						}
						$datuser = explode('<->', utf8_encode($row[5]));
						switch ($row[4]) 
						{
							case 'S': 	$imgest="<img src='imagenes/sema_amarilloON.jpg' title='Pago Pendiente' style='width:20px;'/>";break;
							case 'P':	$imgest="<img src='imagenes/sema_verdeON.jpg' title='Pago Realizado' style='width:20px;'/>";
										break;
							case 'V':	$imgest="<img src='imagenes/sema_rojoON.jpg' title='Pago Vencido' style='width:20px;'/>";break;
							case 'L':	$imgest="<img src='imagenes/sema_amarilloOFF.jpg' title='Factura Sin Realizar' style='width:20px;'/>";break;
							case 'A':	$imgest="<img src='imagenes/sema_azulON.jpg' title='Abono' style='width:20px;'/>";break;
							default:	$imgest="";
						}
						echo"
						<tr class='$iter' >
							<td>".str_pad($row[0],10,"0", STR_PAD_LEFT)."</td>
							<td>$row[1]</td>
							<td>$row[2]</td>
							<td>$row[3]</td>
							<td>$datuser[0]</td>
							<td>$datuser[1]</td>
							<td style='text-align:right;'>$ ".number_format($divvalores[0],2,',','.')."</td>
							<td style='text-align:right;'>$ ".number_format($abonos,2,',','.')."</td>
							<td style='text-align:center;'>$imgest</td>
							<td style='text-align:center;'><img class='icoop' src='imagenes/lupa02.png' title='Mirar Factura'  onClick=\"location.href='serv-facturaseditar.php?idfac=$row[0]'\"/></td>
						</tr>";
	 					$con+=1;
	 					$aux=$iter;
	 					$iter=$iter2;
	 					$iter2=$aux;
						$filas++;
 					}
					if ($_POST[numtop]==0)
					{
						echo "
						<table class='inicio'>
							<tr>
								<td class='saludo1' style='text-align:center;width:100%'><img src='imagenes\alert.png' style='width:25px'>No hay coincidencias en la b&uacute;squeda $tibusqueda<img src='imagenes\alert.png' style='width:25px'></td>
							</tr>
						</table>";
					}
 					echo"
						</table>
						<table class='inicio'>
							<tr>
								<td style='text-align:center;'>
									<a>$imagensback</a>&nbsp;
									<a>$imagenback</a>&nbsp;&nbsp;";
					if($nuncilumnas<=9){$numfin=$nuncilumnas;}
					else{$numfin=9;}
					for($xx = 1; $xx <= $numfin; $xx++)
					{
						if($numcontrol<=9){$numx=$xx;}
						else{$numx=$xx+($numcontrol-9);}
						if($numcontrol==$numx){echo"<a onClick='saltocol(\"$numx\")'; style='color:#24D915;cursor:pointer;'> $numx </a>";}
						else {echo"<a onClick='saltocol(\"$numx\")'; style='color:#000000;cursor:pointer;'> $numx </a>";}
					}
					echo"			&nbsp;&nbsp;<a>$imagenforward</a>
									&nbsp;<a>$imagensforward</a>
								</td>
							</tr>
						</table>";
						
			?>			
			</div>
            <input type="hidden" name="numtop" id="numtop" value="<?php echo $_POST[numtop];?>" />
		</form>
	</body>
</html>