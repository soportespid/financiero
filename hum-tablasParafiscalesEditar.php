<?php

    header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require 'comun.inc';
	require 'funciones.inc';
	session_start();
    if(empty($_SESSION)){
        header("location: index.php");
    }
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html lang=es>
	<head>
    <meta charset="utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1"/>
		<meta name="viewport" content="user-scalable=no">
		<title>:: IDEAL 10 - Parametrización</title>
		<link href="favicon.ico" rel="shortcut icon"/>
		<link href="css/css2z.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
        <link href="css/tabs.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
        <link href="css/style.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" href="sweetalert2/dist/sweetalert2.min.css">
        <script type="text/javascript" src="css/programas.js"></script>
        <script src="sweetalert2/dist/sweetalert2.min.js"></script>
		<script src="vue/vue.min.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script src="css/calendario.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
	</head>
	<body>
		<header>
			<table>
				<tr><script>barra_imagenes("para");</script><?php cuadro_titulos();?></tr>
			</table>
		</header>

		<form name="form2" method="post" action="">
			<section id="myapp" v-cloak >
                <div id="cargando" v-show="isLoading" class="loading" style="z-index: 10000;">
                    <span>Cargando...</span>
                </div>
				<nav>
					<table>
						<tr><?php menu_desplegable("para");?></tr>
                        <tr>
							<td colspan="3" class="cinta">
                                <img src="imagenes/add.png" @click="window.location.href='hum-tablasParafiscalesCrear'"class="mgbt" title="Nuevo">
								<img src="imagenes/guarda.png" @click="save()" title="Guardar"  class="mgbt">
								<img src="imagenes/busca.png" @click="window.location.href='hum-tablasParafiscalesBuscar'"   class="mgbt" title="Buscar">
								<img src="imagenes/nv.png" @click="mypop=window.open('para-principal.php','',''); mypop.focus();" class="mgbt" title="Nueva Ventana">
                                <a href="hum-tablasParafiscalesBuscar"  class="mgbt"><img src="imagenes/iratras.png" title="Atr&aacute;s"></a>
							</td>
						</tr>
					</table>
				</nav>
				<article>
                    <div class="bg-white">
                        <h2 class="titulos m-0">Parafiscal</h2>
                        <div>
                            <p class="m-2 mb-0">Todos los campos con (<span class="text-danger fw-bolder">*</span>) son obligatorios</p>
                            <div class="d-flex w-100">
                                <div class="form-control w-25">
                                    <label class="form-label m-0" for="">Código:</label>
                                    <div class="d-flex">
                                        <button type="button" class="btn btn-primary" @click="editItem('prev')"><</button>
                                        <input type="text"  style="text-align:center;" v-model="txtConsecutivo" @change="editItem()">
                                        <button type="button" class="btn btn-primary" @click="editItem('next')">></button>
                                    </div>
                                </div>
                                <div class="form-control">
                                    <label class="form-label mb-0" for="">Nombre<span class="text-danger fw-bolder">*</span>:</label>
                                    <input type="text" v-model="txtNombre">
                                </div>
                                <div class="form-control w-25">
                                    <label class="form-label mb-0" for="">Porcentaje<span class="text-danger fw-bolder">*</span>:</label>
                                    <input type="number" v-model="txtPorcentaje">
                                </div>
                                <div class="form-control">
                                    <label class="form-label mb-0" for="labelSelectName2">Tipo<span class="text-danger fw-bolder">*</span>:</label>
                                    <select id="labelSelectName2" v-model="selectTipo">
                                        <option value="0" selected>Seleccione</option>
                                        <option value="D" >Descuento</option>
                                        <option value="A" >Aporte</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div>
                            <h2 class="titulos m-0">Detalle variable de pago</h2>
                            <div>
                                <div class="d-flex">
                                    <div class="form-control">
                                        <label class="form-label mb-0" for="labelSelectName2">Funcionamiento:</label>
                                        <select id="labelSelectName2" v-model="selectFuncionamiento">
                                            <option value="0" selected>Seleccione</option>
                                            <option v-for="(data,index) in arrFuncionamiento" :key="index" :value="data.codigo">
                                                {{ data.nombre}}
                                            </option>
                                        </select>
                                    </div>
                                    <div class="form-control">
                                        <label class="form-label mb-0" for="labelSelectName2">Temporal funcionamiento:</label>
                                        <select id="labelSelectName2" v-model="selectTempFuncionamiento">
                                            <option value="0" selected>Seleccione</option>
                                            <option v-for="(data,index) in arrTempFuncionamiento" :key="index" :value="data.codigo">
                                                {{ data.nombre}}
                                            </option>
                                        </select>
                                    </div>
                                    <div class="form-control">
                                        <label class="form-label mb-0" for="labelSelectName2">Inversión:</label>
                                        <select id="labelSelectName2" v-model="selectInversion">
                                            <option value="0" selected>Seleccione</option>
                                            <option v-for="(data,index) in arrInversion" :key="index" :value="data.codigo">
                                                {{ data.nombre}}
                                            </option>
                                        </select>
                                    </div>
                                    <div class="form-control">
                                        <label class="form-label mb-0" for="labelSelectName2">Inversión temporal:</label>
                                        <select id="labelSelectName2" v-model="selectTempInversion">
                                            <option value="0" selected>Seleccione</option>
                                            <option v-for="(data,index) in arrTempInversion" :key="index" :value="data.codigo">
                                                {{ data.nombre}}
                                            </option>
                                        </select>
                                    </div>
                                </div>
                                <div class="d-flex">
                                    <div class="form-control">
                                        <label class="form-label mb-0" for="labelSelectName2">Gastos comercialización:</label>
                                        <select id="labelSelectName2" v-model="selectComercio">
                                            <option value="0" selected>Seleccione</option>
                                            <option v-for="(data,index) in arrComercio" :key="index" :value="data.codigo">
                                                {{ data.nombre}}
                                            </option>
                                        </select>
                                    </div>
                                    <div class="form-control">
                                        <label class="form-label mb-0" for="labelSelectName2">Temporal gastos comercialización:</label>
                                        <select id="labelSelectName2" v-model="selectTempComercio">
                                            <option value="0" selected>Seleccione</option>
                                            <option v-for="(data,index) in arrTempComercio" :key="index" :value="data.codigo">
                                                {{ data.nombre}}
                                            </option>
                                        </select>
                                    </div>
                                    <div class="form-control">
                                        <label class="form-label m-0" for="labelSelectName2">Seleccionar sector:</label>
                                        <div class="d-flex">
                                            <select v-model="selectVariable" id="labelSelectName2" class="w-50">
                                                <option value="N/A" selected>N/A</option>
                                                <option value="publico">Público</option>
                                                <option value="privado">Privado</option>
                                            </select>
                                            <button type="button" class="btn btn-primary" @click="addVariable">Agregar</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="overflow-auto max-vh-50 overflow-x-hidden" >
                            <h2 class="titulos m-0">Detalle variable - Parametrización</h2>
                            <table class="table fw-normal">
                                <thead>
                                    <tr>
                                        <th>Funcionamiento</th>
                                        <th>Funcionamiento Temporal</th>
                                        <th>Inversión</th>
                                        <th>Inversión Temporal</th>
                                        <th>Gastos Comercialización</th>
                                        <th>Gastos Comercialización Temporal</th>
                                        <th>Sector</th>
                                        <th>Opciones</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr v-for="(data,index) in arrSelVariables" :key="index">
                                        <td>{{data.funcionamiento.codigo+"-"+data.funcionamiento.nombre}}</td>
                                        <td>{{data.temp_funcionamiento.codigo+"-"+data.temp_funcionamiento.nombre}}</td>
                                        <td>{{data.inversion.codigo+"-"+data.inversion.nombre}}</td>
                                        <td>{{data.temp_inversion.codigo+"-"+data.temp_inversion.nombre}}</td>
                                        <td>{{data.comercio ? data.comercio.codigo+"-"+data.comercio.nombre : ""}}</td>
                                        <td>{{data.temp_comercio ? data.temp_comercio.codigo+"-"+data.temp_comercio.nombre : ""}}</td>
                                        <td>{{data.sector}}</td>
                                        <td>
                                            <button type="button" class="btn btn-danger m-1" @click="delVariable(index)">Eliminar</button>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
				</article>
			</section>
		</form>

		<script src="node_modules/read-excel-file/bundle/read-excel-file.min.js"></script>
		<script src="node_modules/xlsx/dist/xlsx.full.min.js"></script>
        <script src="Librerias/vue/vue.min.js"></script>
		<script src="Librerias/vue/axios.min.js"></script>
		<script type="module" src="gestion_humana/tablas_parafiscales/editar/hum-tablasParafiscales.js?<?= date('d_m_Y_h_i_s');?>"></script>

	</body>
</html>
