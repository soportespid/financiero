<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require "comun.inc";
	require "funciones.inc";
	sesion();
	$linkbd=conectar_v7();
	$linkbd -> set_charset("utf8");
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es">
	<head>
		<meta http-equiv="Content-type" content="text/html; charset=utf8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=9"/>
		<title>:: IDEAL 10 - Contabilidad</title>
		<link href="css/css2.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
		<script type="text/javascript" src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<?php titlepag();?>
	</head>
	<body>
		<IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
		<span id="todastablas2"></span>
        
		<table>
			<tr><script>barra_imagenes("cont");</script><?php cuadro_titulos();?></tr>	 
			<tr><?php menu_desplegable("cont");?></tr>
			<tr>
				<td colspan="3" class="cinta">
                    <a href="#"><img src="imagenes/add2.png" class="mgbt1"/></a>
                    <a href="#"><img src="imagenes/guardad.png" class="mgbt1" style="width:24px;"/></a>
                    <a href="#"><img src="imagenes/buscad.png" class="mgbt1"/></a>
                    <a href="#"><img src="imagenes/nv.png" title="Nueva Ventana" onClick="mypop=window.open('cont-principal.php','','');mypop.focus();" class="mgbt"/></a>
                    <a href="cont-estadocomprobantes.php"><img src="imagenes/iratras.png" title="Atrás"></a>
                </td>
			</tr>
		</table>

		<form name="form2" method="post" action="">
            <div style="height:76%; width:99.6%; overflow-x:hidden;">
                <table class="inicio">
                    <tr>
                        <td class="titulos" colspan="2">.: Trazabilidad </td>
                        <td class="cerrar" style="width:7%" onClick="location.href='ccp-principal.php'">Cerrar</td>
                    </tr>

                    <tr>
                        <td style="background-repeat:no-repeat; background-position:center;">
                            <ol id="lista2">
                                <li onClick="location.href='cont-trazabilidadRadicados.php'" style="cursor:pointer;">Trazabilidad documentos radicados stock de almacen</li>
                                <li onClick="location.href='cont-trazabilidadEntradasAlmacen.php'" style="cursor:pointer;">Trazabilidad entradas almacen</li>
                            </ol>
                        </td>
                    </tr>
                </table>
            </div>
		</form>
	</body>
</html>