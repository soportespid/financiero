<?php

header("Cache-control: no-cache, no-store, must-revalidate");
header("Content-Type: text/html;charset=utf8");
require 'comun.inc';
require 'funciones.inc';
session_start();
if (empty($_SESSION)) {
    header("location: index.php");
}
$linkbd = conectar_v7();
$linkbd->set_charset("utf8");
cargarcodigopag($_GET['codpag'], $_SESSION["nivel"]);
date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE>
<html lang=es>

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1" />
    <meta name="viewport" content="user-scalable=no">
    <title>:: IDEAL 10 - Tesorería</title>
    <link href="favicon.ico" rel="shortcut icon" />
    <link href="css/css2z.css?<?php echo date('d_m_Y_h_i_s'); ?>" rel="stylesheet" />
    <link href="css/css3.css?<?php echo date('d_m_Y_h_i_s'); ?>" rel="stylesheet" />
    <link href="css/css4.css?<?php echo date('d_m_Y_h_i_s'); ?>" rel="stylesheet" />
    <link href="css/tabs.css?<?php echo date('d_m_Y_h_i_s'); ?>" rel="stylesheet" type="text/css" />
    <link href="css/style.css?<?php echo date('d_m_Y_h_i_s'); ?>" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="sweetalert2/dist/sweetalert2.min.css">
    <script type="text/javascript" src="css/programas.js"></script>
    <script src="sweetalert2/dist/sweetalert2.min.js"></script>
    <script src="vue/vue.min.js?<?php echo date('d_m_Y_h_i_s'); ?>"></script>
    <script src="css/programas.js?<?php echo date('d_m_Y_h_i_s'); ?>"></script>
    <script src="css/calendario.js?<?php echo date('d_m_Y_h_i_s'); ?>"></script>
</head>

<body>
    <header>
        <table>
            <tr>
                <script>barra_imagenes("teso");</script><?php cuadro_titulos(); ?>
            </tr>
        </table>
    </header>

    <form name="form2" method="post" action="">
        <section id="myapp" v-cloak>
            <div id="cargando" v-show="isLoading" class="loading" style="z-index: 10000;">
                <span>Cargando...</span>
            </div>
            <nav>
                <table>
                    <tr><?php menu_desplegable("teso"); ?></tr>
                </table>
                <div class="bg-white group-btn p-1">
                    <button type="button" @click="window.location.reload()"
                        class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
                        <span>Nuevo</span>
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960">
                            <path d="M440-440H200v-80h240v-240h80v240h240v80H520v240h-80v-240Z"></path>
                        </svg>
                    </button>
                    <button type="button" @click="save()"
                        class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
                        <span>Guardar</span>
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960">
                            <path
                                d="M840-680v480q0 33-23.5 56.5T760-120H200q-33 0-56.5-23.5T120-200v-560q0-33 23.5-56.5T200-840h480l160 160Zm-80 34L646-760H200v560h560v-446ZM480-240q50 0 85-35t35-85q0-50-35-85t-85-35q-50 0-85 35t-35 85q0 50 35 85t85 35ZM240-560h360v-160H240v160Zm-40-86v446-560 114Z">
                            </path>
                        </svg>
                    </button>
                    <button type="button" @click="window.location.href='teso-inscripcionIndustriaBuscar.php'"
                        class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
                        <span>Buscar</span>
                        <svg class="fill-black group-hover:fill-white w-5 h-5" xmlns="http://www.w3.org/2000/svg"
                            viewBox="0 -960 960 960">
                            <path
                                d="M784-120 532-372q-30 24-69 38t-83 14q-109 0-184.5-75.5T120-580q0-109 75.5-184.5T380-840q109 0 184.5 75.5T640-580q0 44-14 83t-38 69l252 252-56 56ZM380-400q75 0 127.5-52.5T560-580q0-75-52.5-127.5T380-760q-75 0-127.5 52.5T200-580q0 75 52.5 127.5T380-400Z">
                            </path>
                        </svg>
                    </button>
                    <button type="button" onclick="mypop=window.open('teso-principal.php','','');mypop.focus();"
                        class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
                        <span>Nueva ventana</span>
                        <svg class="fill-black group-hover:fill-white w-5 h-5" xmlns="http://www.w3.org/2000/svg"
                            viewBox="0 -960 960 960">
                            <path
                                d="M320-120v-80h80v-80H160q-33 0-56.5-23.5T80-360v-400q0-33 23.5-56.5T160-840h640q33 0 56.5 23.5T880-760v400q0 33-23.5 56.5T800-280H560v80h80v80H320ZM160-360h640v-400H160v400Zm0 0v-400 400Z">
                            </path>
                        </svg>
                    </button>
                    <button type="button" onclick="window.location.href='teso-inscripcionIndustriaBuscar.php'"
                        class="btn btn-success btn-success-hover d-flex justify-between align-items-center">
                        <span>Atras</span>
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960">
                            <path
                                d="M760-200v-160q0-50-35-85t-85-35H273l144 144-57 56-240-240 240-240 57 56-144 144h367q83 0 141.5 58.5T840-360v160h-80Z">
                            </path>
                        </svg>
                    </button>
                </div>
            </nav>
            <article>
                <!--TABS-->
                <div ref="rTabs" class="nav-tabs inicio">
                    <div class="nav-item active" @click="showTab(1)">Información General</div>
                    <div class="nav-item" @click="showTab(2)">Representantes y actividades</div>
                    <div class="nav-item" @click="showTab(3)">Establecimientos</div>
                    <div class="nav-item" @click="showTab(4)">Formularios y anexos</div>
                </div>
                <!--CONTENIDO TABS-->
                <div ref="rTabsContent" class="nav-tabs-content">
                    <div class="nav-content active">
                        <div class="inicio">
                            <div>
                                <h2 class="titulos m-0">.: Información contribuyente <span
                                        class="text-danger fw-bolder">*</span></h2>
                                <div class="d-flex justify-between">
                                    <div class="d-flex justify-between w-50">
                                        <div class="form-control">
                                            <label class="form-label">.: Consecutivo:</label>
                                            <input type="text" style="text-align:center;" v-model="txtConsecutivo"
                                                disabled readonly>
                                        </div>
                                        <div class="form-control">
                                            <label class="form-label" for="">.: Fecha de registro <span
                                                    class="text-danger fw-bolder">*</span>:</label>
                                            <input type="text" id="fechaReg" name="fechaReg"
                                                onKeyUp="return tabular(event,this)" id="fechaReg" title="DD/MM/YYYY"
                                                onDblClick="displayCalendarFor('fechaReg');" class="colordobleclik"
                                                autocomplete="off" onChange="" readonly>
                                        </div>
                                        <div class="form-control">
                                            <label class="form-label">.: Tipo de matrícula <span
                                                    class="text-danger fw-bolder">*</span>:</label>
                                            <select v-model="selectMatricula">
                                                <option disabled selected>Seleccione</option>
                                                <option value="1">Común</option>
                                                <option value="2">Simple</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="d-flex justify-between w-50">
                                        <div class="form-control">
                                            <label class="form-label" for="">.: Matrícula mercantil <span
                                                    class="text-danger fw-bolder">*</span>:</label>
                                            <input type="text" v-model="txtMatricula">
                                        </div>
                                        <div class="form-control">
                                            <label class="form-label" for="">.: Fecha matrícula mercantil <span
                                                    class="text-danger fw-bolder">*</span>:</label>
                                            <input type="text" id="fechaMatricula" name="fechaMatricula"
                                                onKeyUp="return tabular(event,this)" id="fechaMatricula"
                                                title="DD/MM/YYYY" onDblClick="displayCalendarFor('fechaMatricula');"
                                                class="colordobleclik" autocomplete="off" onChange="" readonly>
                                        </div>
                                        <div class="form-control">
                                            <label class="form-label" for="">.: Fecha inicio de actividades municipio
                                                <span class="text-danger fw-bolder">*</span>:</label>
                                            <input type="text" id="fechaInicio" name="fechaInicio"
                                                onKeyUp="return tabular(event,this)" id="fechaInicio" title="DD/MM/YYYY"
                                                onDblClick="displayCalendarFor('fechaInicio');" class="colordobleclik"
                                                autocomplete="off" onChange="" readonly>
                                        </div>
                                    </div>
                                </div>
                                <div class="d-flex justify-between">
                                    <div class="d-flex w-50">
                                        <div class="form-control">
                                            <label class="form-label" for="">.: Nombre/Razón social <span
                                                    class="text-danger fw-bolder">*</span>:</label>
                                            <div class="d-flex">
                                                <input type="text" @dblclick="isModal =true;selectRepMode=1;"
                                                    v-model="objTerceroPrin.documento" class="colordobleclik w-25"
                                                    readonly>
                                                <input type="text" v-model="objTerceroPrin.nombre" disabled readonly>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="d-flex w-50">
                                        <div class="form-control">
                                            <label class="form-label" for="">.: Tipo de persona <span
                                                    class="text-danger fw-bolder">*</span>:</label>
                                            <select disabled readonly v-model="objTerceroPrin.tipo">
                                                <option disabled selected>Seleccione</option>
                                                <option value="1">Natural</option>
                                                <option value="2">Jurídica</option>
                                            </select>
                                        </div>
                                        <div class="form-control">
                                            <label class="form-label" for="">.: Régimen <span
                                                    class="text-danger fw-bolder">*</span>:</label>
                                            <select disabled readonly v-model="objTerceroPrin.regimen">
                                                <option disabled selected>Seleccione</option>
                                                <option value="1">Responsable de IVA</option>
                                                <option value="2">No responsable de IVA</option>
                                            </select>
                                        </div>

                                    </div>
                                </div>
                                <div class="d-flex justify-between">
                                    <div class="form-checkbox ">
                                        <label for="labelCheckName1">¿Gran Contribuyente?</label>
                                        <input type="checkbox" id="labelCheckName1" v-model="checkGran">
                                    </div>
                                    <div class="form-checkbox">
                                        <label for="labelCheckName2">¿Declara Alumbrado?</label>
                                        <input type="checkbox" id="labelCheckName2" v-model="checkAlumbrado">
                                    </div>
                                    <div class="form-checkbox">
                                        <label for="labelCheckName3">¿Agente Retenedor?</label>
                                        <input type="checkbox" id="labelCheckName3" v-model="checkRetenedor">
                                    </div>
                                    <div class="form-checkbox">
                                        <label for="labelCheckName4">¿Agente Auto-Retenedor?</label>
                                        <input type="checkbox" id="labelCheckName4" v-model="checkAuto">
                                    </div>
                                    <div class="form-checkbox">
                                        <label for="labelCheckName5">¿Contribuyente Temporal?</label>
                                        <input type="checkbox" id="labelCheckName5" v-model="checkTemporal">
                                    </div>
                                </div>
                            </div>
                            <div>
                                <h2 class="titulos m-0">.: Información del contador o revisor fiscal (opcional)</h2>
                                <div class="d-flex">
                                    <div class="form-control w-25">
                                        <label class="form-label" for="labelSelectName2">.: Tipo documento:</label>
                                        <select id="labelSelectName2" v-model="objRevisor.tipo">
                                            <option disabled selected>Seleccione</option>
                                            <option value="13">Cedula de Ciudadania</option>
                                            <option value="22">Cedula Extranjera</option>
                                            <option value="31">NIT</option>
                                            <option value="41">Pasaporte</option>
                                            <option value="12">Tarjeta de Identidad</option>
                                            <option value="21">Tarjeta de Extranjeria</option>
                                            <option value="11">Registro Civil de Nacimiento</option>
                                        </select>
                                    </div>
                                    <div class="form-control w-25">
                                        <label class="form-label" for="">.: Nro. documento:</label>
                                        <input type="text" v-model="objRevisor.documento">
                                    </div>
                                    <div class="form-control">
                                        <label class="form-label" for="">.: Nombres y apellidos:</label>
                                        <input type="text" v-model="objRevisor.nombre">
                                    </div>
                                </div>
                            </div>
                            <div>
                                <h2 class="titulos m-0">.: Información de notificación judicial <span
                                        class="text-danger fw-bolder">*</span></h2>
                                <div class="d-flex">
                                    <div class="form-control">
                                        <label class="form-label" for="">.: Dirección <span
                                                class="text-danger fw-bolder">*</span>:</label>
                                        <input type="text" disabled readonly v-model="objTerceroPrin.direccion">
                                    </div>
                                    <div class="form-control">
                                        <label class="form-label" for="">.: Teléfono <span
                                                class="text-danger fw-bolder">*</span>:</label>
                                        <input type="text" disabled readonly v-model="objTerceroPrin.telefono">
                                    </div>
                                    <div class="form-control">
                                        <label class="form-label" for="">.: Celular <span
                                                class="text-danger fw-bolder">*</span>:</label>
                                        <input type="text" disabled readonly v-model="objTerceroPrin.celular">
                                    </div>
                                    <div class="form-control">
                                        <label class="form-label" for="">.: Correo electrónico <span
                                                class="text-danger fw-bolder">*</span>:</label>
                                        <input type="email" disabled readonly v-model="objTerceroPrin.correo">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="nav-content">
                        <div class="inicio">
                            <div>
                                <h2 class="titulos m-1">.: Representantes</h2>
                                <div class="d-flex">
                                    <div class="d-flex w-50">
                                        <div class="form-control flex-row align-items-center">
                                            <label class="form-label w-25" for="">.: Representante:</label>
                                            <div class="d-flex w-100">
                                                <input type="text" class="colordobleclik w-50"
                                                    @dblclick="isModal =true;selectRepMode=2;"
                                                    v-model="objTerceroSec.documento" readonly>
                                                <input type="text" disabled readonly v-model="objTerceroSec.nombre">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="d-flex">
                                        <div class="form-control flex-row align-items-center">
                                            <label class="form-label" for="">.: Tipo representante:</label>
                                            <div class="d-flex">
                                                <select v-model="selectTipoRepresentante">
                                                    <option value="Principal">Principal</option>
                                                    <option value="Suplente">Suplente</option>
                                                    <option value="Poseedor">Poseedor</option>
                                                </select>
                                                <button type="button" @click="addTercero"
                                                    class="btn btn-primary">Agregar</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="overflow-auto max-vh-15 overflow-x-hidden p-2">
                                    <table class="table fw-normal">
                                        <thead>
                                            <tr>
                                                <th>Número de documento</th>
                                                <th>Nombre completo</th>
                                                <th>Tipo</th>
                                                <th>Opciones</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr v-for="(data,index) in arrTerceros" :key="data.index">
                                                <td>{{ data.documento }}</td>
                                                <td>{{ data.nombre }}</td>
                                                <td>{{ data.tipo }}</td>
                                                <td>
                                                    <button type="button" @click="delItem(index,'tercero')"
                                                        class="btn btn-danger m-1">Eliminar</button>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="inicio">
                            <div>
                                <h2 class="titulos m-1">.: Actividad Económica <span
                                        class="text-danger fw-bolder">*</span></h2>
                                <div class="d-flex">
                                    <div class="d-flex w-50">
                                        <div class="form-control w-50 flex-row align-items-center">
                                            <label class="form-label w-50" for="">.: Fecha inicio <span
                                                    class="text-danger fw-bolder">*</span>:</label>
                                            <input type="text" id="fechaInicioAct" name="fechaInicioAct"
                                                onKeyUp="return tabular(event,this)" id="fechaInicioAct"
                                                title="DD/MM/YYYY" onDblClick="displayCalendarFor('fechaInicioAct');"
                                                class="colordobleclik" autocomplete="off" onChange="" readonly>
                                        </div>
                                        <div class="form-control flex-row align-items-center">
                                            <label class="form-label w-25" for="">.: Actividad <span
                                                    class="text-danger fw-bolder">*</span>:</label>
                                            <div class="d-flex  w-100">
                                                <input type="text" class="colordobleclik w-25"
                                                    @dblclick="isModalActividad =true" v-model="objActividad.codigo"
                                                    readonly>
                                                <input type="text" disabled readonly v-model="objActividad.nombre">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="w-50 form-control flex-row align-items-center">
                                        <label class="form-label" for="labelSelectName2">.: Tipo de actividad <span
                                                class="text-danger fw-bolder">*</span>:</label>
                                        <div class="d-flex">
                                            <select id="labelSelectName2" v-model="selectActividad">
                                                <option disabled selected>Seleccione</option>
                                                <option value="Principal">Principal</option>
                                                <option value="Secundaria">Secundaria</option>
                                            </select>
                                            <button type="button" @click="addActividad"
                                                class="btn btn-primary">Agregar</button>
                                        </div>
                                    </div>
                                </div>
                                <div class="overflow-auto max-vh-15 overflow-x-hidden p-2">
                                    <table class="table fw-normal">
                                        <thead>
                                            <tr>
                                                <th>Código</th>
                                                <th>Nombre</th>
                                                <th>Tipo</th>
                                                <th>Fecha inicio</th>
                                                <th>Opciones</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr v-for="(data,index) in arrCodigos" :key="data.index">
                                                <td>{{ data.codigo }}</td>
                                                <td>{{ data.nombre }}</td>
                                                <td>{{ data.tipo }}</td>
                                                <td>{{ data.fecha }}</td>
                                                <td>
                                                    <button type="button" @click="delItem(index,'actividad')"
                                                        class="btn btn-danger m-1">Eliminar</button>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="nav-content">
                        <div class="inicio">
                            <div>
                                <h2 class="titulos m-0">.: Establecimientos</h2>
                                <div class="d-flex justify-between">
                                    <div class="form-control w-inherit">
                                        <label class="form-label" for="">.: Fecha apertura <span
                                                class="text-danger fw-bolder">*</span>:</label>
                                        <input type="text" id="fechaApertura" name="fechaApertura"
                                            onKeyUp="return tabular(event,this)" id="fechaApertura" title="DD/MM/YYYY"
                                            onDblClick="displayCalendarFor('fechaApertura');" class="colordobleclik"
                                            autocomplete="off" onChange="" readonly>
                                    </div>
                                    <div class="form-control w-50">
                                        <label class="form-label">.: Razón social <span
                                                class="text-danger fw-bolder">*</span>:</label>
                                        <input type="text" v-model="txtEstNombre">
                                    </div>
                                    <div class="form-control w-50">
                                        <label class="form-label">.: Dirección <span
                                                class="text-danger fw-bolder">*</span>:</label>
                                        <input type="text" v-model="txtEstDireccion">
                                    </div>
                                </div>
                                <div class="d-flex justify-between">
                                    <div class="form-control w-50">
                                        <label class="form-label">.: Teléfono <span
                                                class="text-danger fw-bolder">*</span>:</label>
                                        <input type="number" v-model="txtEstTelefono">
                                    </div>
                                    <div class="form-control w-50">
                                        <label class="form-label">.: Correo electrónico:</label>
                                        <input type="email" v-model="txtEstCorreo">
                                    </div>
                                    <div class="form-control w-25">
                                        <label class="form-label">.: Nro empleados:</label>
                                        <input type="number" v-model="txtEstEmpleados">
                                    </div>
                                    <div class="form-control w-25">
                                        <label class="form-label">.: Valor activos:</label>
                                        <input type="number" v-model="txtEstActivos">
                                    </div>
                                </div>
                                <div class="d-flex justify-center">
                                    <button type="button" @click="addEst" class="btn btn-primary">Agregar</button>
                                </div>
                                <div class="overflow-auto max-vh-25 overflow-x-hidden p-2">
                                    <table class="table fw-normal">
                                        <thead>
                                            <tr>
                                                <th>Fecha apertura</th>
                                                <th>Razón social</th>
                                                <th>dirección</th>
                                                <th>Telefono</th>
                                                <th>Correo</th>
                                                <th>Empleados</th>
                                                <th>Activos</th>
                                                <th>Opciones</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr v-for="(data,index) in arrEstablecimientos" :key="data.id">
                                                <td>{{ data.fecha_apertura }}</td>
                                                <td>{{ data.nombre }}</td>
                                                <td>{{ data.direccion }}</td>
                                                <td>{{ data.telefono }}</td>
                                                <td>{{ data.correo }}</td>
                                                <td>{{ data.empleados }}</td>
                                                <td>{{ formatNumero(data.activos) }}</td>
                                                <td>
                                                    <button type="button" @click="delItem(index,'establecimiento')"
                                                        class="btn btn-danger m-1">Eliminar</button>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="nav-content">
                        <div class="inicio">
                            <div>
                                <h2 class="titulos m-0">.: Anexos</h2>
                                <div class="d-flex justify-center">
                                    <div class="form-control w-25">
                                        <label class="form-label">.: Descargue el formulario con los datos anteriormente
                                            diligenciados:</label>
                                        <button type="button" class="btn btn-primary"
                                            @click="printPDF()">Descargar</button>
                                        <!--<p class="text-center text-danger">Para finalizar el proceso de Inscripción adjuntar en (.pdf) los siguientes anexos.</p>-->
                                    </div>
                                </div>
                                <div class="d-flex justify-start">
                                    <div class="form-control w-50">
                                        <label class="form-label">.: Formulario de inscripción firmado:</label>
                                        <input type="file" accept="application/pdf"
                                            @change="uploadFile(this,'pdfInscripcion')">
                                    </div>
                                    <div class="form-control w-50">
                                        <label class="form-label">.: Documento de identidad del
                                            propietario/representante legal:</label>
                                        <input type="file" accept="application/pdf"
                                            @change="uploadFile(this,'pdfDocumento')">
                                    </div>
                                </div>
                                <div class="d-flex justify-start">
                                    <div class="form-control w-50">
                                        <label class="form-label">.: RUT:</label>
                                        <input type="file" accept="application/pdf" @change="uploadFile(this,'pdfRut')">
                                    </div>
                                    <div class="form-control w-50">
                                        <label class="form-label">.: Cámara de comercio:</label>
                                        <input type="file" accept="application/pdf"
                                            @change="uploadFile(this,'pdfComercio')">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </article>
            <!--MODALES-->
            <div v-show="isModal">
                <transition name="modal">
                    <div class="modal-mask">
                        <div class="modal-wrapper">
                            <div class="modal-container">
                                <table class="inicio ancho">
                                    <tr>
                                        <td class="titulos" colspan="2">.: Buscar tercero</td>
                                        <td class="cerrar" style="width:7%" @click="isModal = false">Cerrar</td>
                                    </tr>
                                </table>
                                <div class="bg-white">
                                    <div class="form-control m-0 p-2 w-inherit">
                                        <input type="search" placeholder="Buscar" @keyup="search('modal_terceros')"
                                            v-model="txtSearchTercero">
                                    </div>
                                    <p class="fw-bolder m-0 p-2">Resultados de búsqueda: {{txtResultsTerceros}}</p>
                                    <div class="overflow-auto max-vh-50 overflow-x-hidden p-2">
                                        <table class="table table-hover fw-normal">
                                            <thead>
                                                <tr>
                                                    <th>ID</th>
                                                    <th>Documento</th>
                                                    <th>Nombre/Razón social</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr v-for="data in arrTercerosModal" :key="data.id_tercero"
                                                    @dblclick="selectItemModal(data,'tercero')">
                                                    <td>{{ data.id_tercero }}</td>
                                                    <td>{{ data.documento }}</td>
                                                    <td>{{ data.nombre }}</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </transition>
            </div>
            <div v-show="isModalActividad">
                <transition name="modal">
                    <div class="modal-mask">
                        <div class="modal-wrapper">
                            <div class="modal-container">
                                <table class="inicio ancho">
                                    <tr>
                                        <td class="titulos" colspan="2">.: Buscar actividad</td>
                                        <td class="cerrar" style="width:7%" @click="isModalActividad = false">Cerrar
                                        </td>
                                    </tr>
                                </table>
                                <div class="bg-white">
                                    <div class="form-control m-0 p-2 w-inherit">
                                        <input type="search" placeholder="Buscar" @keyup="search('modal_actividad')"
                                            v-model="txtSearchCodigo">
                                    </div>
                                    <p class="fw-bolder m-0 p-2">Resultados de búsqueda: {{txtResults}}</p>
                                    <div class="overflow-auto max-vh-50 overflow-x-hidden p-2">
                                        <table class="table table-hover fw-normal">
                                            <thead>
                                                <tr>
                                                    <th>ID</th>
                                                    <th>Código</th>
                                                    <th>Nombre</th>
                                                    <th>Estado</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr v-for="data in arrCodigosModal" :key="data.id"
                                                    @dblclick="selectItemModal(data,'actividad')">
                                                    <td>{{data.id}}</td>
                                                    <td>{{data.codigo}}</td>
                                                    <td>{{data.nombre}}</td>
                                                    <td>{{data.porcentaje}}</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </transition>
            </div>
        </section>
    </form>

    <script src="node_modules/read-excel-file/bundle/read-excel-file.min.js"></script>
    <script src="node_modules/xlsx/dist/xlsx.full.min.js"></script>
    <script src="Librerias/vue/vue.min.js"></script>
    <script src="Librerias/vue/axios.min.js"></script>
    <script type="module"
        src="tesoreria/inscripcion_industria/crear/teso-inscripcionCrear.js?<?= date('d_m_Y_h_i_s'); ?>"></script>

</body>

</html>
