<?php
    require_once("tcpdf/tcpdf_include.php");
	require('comun.inc');
	require "funciones.inc";
    require 'funcionesSP.inc.php';
	date_default_timezone_set("America/Bogota");
    /*ini_set('display_errors', '1');
    ini_set('display_startup_errors', '1');
    error_reporting(E_ALL);*/
	session_start();

    class Plantilla{
        private $linkbd;
        private $arrData;
        public function __construct() {
            $this->linkbd = conectar_v7();
            $this->linkbd->set_charset("utf8");
        }
        public function search(string $search,int $currentPage){
            if(!empty($_SESSION)){
                $request = $this->selectData($search,$currentPage);
                echo json_encode($request,JSON_UNESCAPED_UNICODE);
            }
            die();
        }
        public function getData(){
            if(!empty($_SESSION)){
                $request = $this->selectData();
                echo json_encode($request,JSON_UNESCAPED_UNICODE);
            }
            die();
        }
        public function save(string $codigo){
            if(!empty($_SESSION)){
                $request = $this->insertData($codigo);
                if(!empty($request)){
                    $arrResponse = array("status"=>true,"data"=>$request);
                }else{
                    $arrResponse = array("status"=>false);
                }
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
            die();
        }
        public function insertData(string $codigo){
            $usuario = $_SESSION['cedulausu'];
            $sql = "SELECT codigo_catastral FROM tesopredios_certificados WHERE codigo_catastral = '$codigo'";
            $request = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
            if(empty($request)){
                $ip = getRealIP();
                $sql = "INSERT INTO tesopredios_certificados (codigo_catastral,usuario,ip) VALUES('$codigo','$usuario','$ip')";
                mysqli_query($this->linkbd,$sql);
                $sql = "SELECT codigo_catastral FROM tesopredios_certificados WHERE codigo_catastral = '$codigo'";
                $request = mysqli_query($this->linkbd,$sql);
            }else{
                $sql = "SELECT codigo_catastral FROM tesopredios_certificados WHERE codigo_catastral = '$codigo'";
                $request = mysqli_query($this->linkbd,$sql);
            }
            return $request;
        }
        public function selectData($codigo){
            $sql="SELECT tp.cedulacatastral,tp.ord,tp.tot,tp.d as destino,
            tp.documento,tp.nombrepropietario,tp.direccion,tp.ha,tp.met2,
            tp.areacon,tp.avaluo,tp.vigencia,tp.tipopredio,c.id as consecutivo,c.usuario,c.ip,DATE_FORMAT(c.fecha,'%d/%m/%Y') as fecha
            FROM tesopredios tp
            INNER JOIN tesopredios_certificados c
            ON tp.cedulacatastral = c.codigo_catastral
            WHERE tp.cedulacatastral = '$codigo'";
            $request = mysqli_query($this->linkbd,$sql)->fetch_assoc();
            return $request;
        }
    }
	class MYPDF extends TCPDF{

		public function Header(){
			//Parte Izquierda
			$this->Image('imagenes/escudo.jpg',  13, 13, 20, 20, 'JPG', '', 'T', true, 300, '', false, false, 0, false, false, false);// Logo
			$this->SetFont('helvetica','B',8);
			$this->SetY(10);
			$this->RoundedRect(10, 10, 190, 25, 1,'');
			$this->Cell(0.1);
			$this->Cell(26,25,'','R',0,'L');
			$this->SetY(8);
			$this->SetX(80);
			$this->SetFont('helvetica','B',9);
			$this->Cell(50,15,strtoupper(MUNICIPIO['razonsocial']),0,0,'C');
			$this->SetFont('helvetica','B',7);
			$this->SetY(12);
			$this->SetX(80);
			$this->Cell(50,15,'NIT: '.MUNICIPIO['nit'],0,0,'C');
			//*****************************************************************************************************************************
			$this->SetFont('helvetica','B',9);
			$this->SetY(23);
			$this->SetX(36);
            $this->Cell(164,12,"CERTIFICADO DE VALORIZACIÓN PREDIAL",'T',0,'C');


            $this->SetFont('helvetica','B',6);
			$this->SetY(10);
			$this->SetX(170);
			$this->Cell(30,7,"CONSECUTIVO: ".CABECERA['consecutivo'],"L",0,'C');
			$this->SetY(17);
			$this->SetX(170);
			$this->Cell(30,6,"FECHA: ".CABECERA['fecha'],"L",0,'C');

			//**********************************************************
			$this->SetFont('times','B',10);
			$this->ln(12);
			//**********************************************************
		}
		public function Footer()
		{
			$linkbd = conectar_v7();
			$linkbd -> set_charset("utf8");
			$sqlr="SELECT direccion,telefono,web,email FROM configbasica WHERE estado='S'";
			$resp=mysqli_query($linkbd, $sqlr);
            $cedula = USUARIO['cedula'];
			$fecha = date("d/m/Y H:i:s");
			$ip = $_SERVER['REMOTE_ADDR'];
			while($row=mysqli_fetch_row($resp))
			{
				$direcc=strtoupper($row[0]);
				$telefonos=$row[1];
				$dirweb=strtoupper($row[3]);
				$coemail=strtoupper($row[2]);
			}
			if($direcc!=''){$vardirec="Dirección: $direcc, ";}
			else {$vardirec="";}
			if($telefonos!=''){$vartelef="Telefonos: $telefonos";}
			else{$vartelef="";}
			if($dirweb!=''){$varemail="Email: $dirweb, ";}
			else {$varemail="";}
			if($coemail!=''){$varpagiw="Pagina Web: $coemail";}
			else{$varpagiw="";}
			$this->SetFont('helvetica', 'I', 8);
			$txt = <<<EOD
			$vardirec $vartelef
			$varemail $varpagiw
			EOD;
			$this->SetFont('helvetica', 'I', 6);
			$this->Cell(277,10,'','T',0,'T');
			$this->ln(2);
			$this->Write(0, $txt , '', 0, 'C', true, 0, false, false, 0);


            $sqlrcc="SELECT * FROM `usuarios` WHERE `cc_usu` = '$cedula'";
            $request=mysqli_query($linkbd, $sqlrcc)->fetch_assoc();
            //echo $rowcc[1];

			$this->Cell(50, 10, 'Hecho por: '.$request['nom_usu'], 00, false, 'C', 0, '', 0, false, 'T', 'M');
			$this->Cell(30, 10, 'Impreso por: '.$_SESSION['nickusu'], 0, false, 'C', 0, '', 0, false, 'T', 'M');
			$this->Cell(30, 10, 'IP: '.getRealIP(), 0, false, 'C',0, '', 0, false, 'T', 'M');
			$this->Cell(30, 10, 'Fecha: '.$fecha, 0, false, 'C', 0, '', 0, false, 'T', 'M');
			$this->Cell(30, 10, 'IDEAL.10 S.A.S', 0, false, 'C', 0, '', 0, false, 'T', 'M');
            $this->Cell(20, 10, 'Pagina '.$this->getAliasNumPage().'/'.$this->getAliasNbPages(), 0, false, 'C', 0, '', 0, false, 'T', 'M');
		}
	}

	if($_GET['codigo']){
        $linkbd = conectar_v7();
        $linkbd -> set_charset("utf8");
        $sqlr="select *from configbasica where estado='S' ";
        //echo $sqlr;
        $res=mysqli_query($linkbd, $sqlr)->fetch_assoc();
        define("MUNICIPIO",$res);
        $obj = new Plantilla();
        $request = $obj->selectData($_GET['codigo']);
        define("CABECERA",array(
            "consecutivo"=>$request['consecutivo'],
            "fecha"=>$request['fecha']
        ));
        define("USUARIO",array("cedula"=>$request['usuario'],"ip"=>$request['ip']));

        $tipo_predio = $request['tipopredio'] == "urbano" ? "urbana" : "rural";
        $descripcion = 'Que en el '.strtoupper(MUNICIPIO['razonsocial']).' no se han realizado obras sujetas al pago del IMPUESTO DE VALORIZACIÓN MUNICIPAL.';
        $descripcion.= " En consecuencia, el predio ubicado en la zona ".$tipo_predio." denominado ".$request['direccion'].";";
        $descripcion.=" código catastral ".$request['cedulacatastral']."; nombre de propietario ".$request['nombrepropietario'];
        $descripcion.=" con número de documento ".$request['documento']."; extensión de ".$request['ha']."ha, ".$request['met2']."m² y";
        $descripcion.=" ".$request['areacon']."m²; avalúo catastrál de $".number_format($request['avaluo'],0,",",".").",";
        $descripcion.= " no ha sido sujeto a pago de IMPUESTO DE VALORIZACIÓN en razón a que dicho impuesto no aplica en el municipio.";

        $arrMeses = array(
            "01"=>"Enero","02"=> "Febrero","03"=> "Marzo", "04"=>"Abril", "05"=>"Mayo","06"=> "Junio",
            "07"=> "Julio", "08"=>"Agosto", "09"=>"Septiembre", "10"=>"Octubre", "11"=>"Noviembre", "12"=>"Diciembre"
        );

        $arrFechaExp = explode("/",$request['fecha']);
        $expide ="Se expide, el día ".$arrFechaExp[0]." del mes de ".$arrMeses[$arrFechaExp[1]]." del año ".$arrFechaExp[2].",";

        $sql="SELECT funcionario, nomcargo as cargo FROM firmaspdf_det WHERE idfirmas='8' AND estado ='S' AND fecha < '".date("Y-m-d")."'";
        $arrFuncionario = mysqli_query($linkbd,$sql)->fetch_assoc();

        $pdf = new MYPDF('P','mm','Letter', true, 'iso-8859-1', false);
        $pdf->SetDocInfoUnicode (true);
        // set document information
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('IDEALSAS');
        $pdf->SetTitle('CERTIFICADO DE VALORIZACIÓN PREDIAL');

        $pdf->SetSubject('CERTIFICADO DE VALORIZACIÓN PREDIAL');
        $pdf->SetKeywords('TCPDF, PDF, example, test, guide');
        $pdf->SetMargins(10, 38, 10);// set margins
        $pdf->SetHeaderMargin(38);// set margins
        $pdf->SetFooterMargin(17);// set margins
        $pdf->SetAutoPageBreak(TRUE, 20);// set auto page breaks
        // set some language-dependent strings (optional)
        if (@file_exists(dirname(__FILE__).'/lang/spa.php'))
        {
            require_once(dirname(__FILE__).'/lang/spa.php');
            $pdf->setLanguageArray($l);
        }
        $pdf->AddPage();

        $pdf->ln(10);
        $pdf->SetFont('helvetica','B',12);
        $pdf->cell(190,4,'EL '.strtoupper($arrFuncionario['cargo']).' DEL '.strtoupper(MUNICIPIO['razonsocial']),'',0,'C',0);
        $pdf->ln(20);
        $pdf->cell(190,4,'CERTIFICA','',0,'C',0);
        $pdf->ln();
        $pdf->SetFont('helvetica','',12);
        $pdf->ln();
        $pdf->MultiCell(190,30,$descripcion,"",'L',false,0,'','',true,0,false,true,0,'T',true);
        $pdf->ln();
        $pdf->ln();
        $pdf->cell(95,4,$expide,'',0,'L',0);
        $pdf->ln();
        $pdf->ln();
        $pdf->ln();
        $pdf->ln();
        $pdf->ln();
        $pdf->ln();
        $pdf->ln();
        $pdf->ln();
        $pdf->SetFont('helvetica','B',8);
        $pdf->setX(55);
        $pdf->cell(100,4,"",'B',0,'C',0);
        $pdf->ln();
        $pdf->setX(55);
        $pdf->cell(100,4,$arrFuncionario['funcionario'],'',0,'C',0);
        $pdf->ln();
        $pdf->setX(55);
        $pdf->cell(100,4,$arrFuncionario['cargo'],'',0,'C',0);
        $pdf->ln();
        $pdf->Output('certificado_predial_'.CABECERA['consecutivo'].'.pdf', 'I');
    }
?>
