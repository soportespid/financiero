<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require 'comun.inc';
	require 'funciones.inc';
	session_start();
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html lang=es>
	<head>
		<meta charset="utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1"/>
		<meta name="viewport" content="user-scalable=no">
		<title>:: Spid - Administracion</title>
		<link href="css/css2z.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="favicon.ico" rel="shortcut icon" />
		<link rel="stylesheet" href="sweetalert2/dist/sweetalert2.min.css">
		<script src="sweetalert2/dist/sweetalert2.min.js"></script>
		<script type="text/javascript" src="css/calendario03.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script type="text/javascript" src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script>
			function checktodos(){
				cali = document.getElementsByName('bloqueados[]');
				for (var i=0;i < cali.length;i++){ 
					if (document.getElementById("todos").checked == true){
						cali.item(i).checked = true;
						document.getElementById("todos").value = 1;	 
					}else{
						cali.item(i).checked = false;
						document.getElementById("todos").value = 0;
					}
				}	
			}
			function guardar(){
				Swal.fire({
						icon: 'question',
						title: '¿Seguro que quieres guardar la información?',
						showDenyButton: true,
						confirmButtonText: 'Guardar',
						confirmButtonColor: '#01CC42',
						denyButtonText: 'Cancelar',
						denyButtonColor: '#FF121A',
					}).then(
						(result) => {
							if (result.isConfirmed){
								document.form2.oculto.value = 2;
								document.form2.submit();
							}else if (result.isDenied){
								Swal.fire({
									icon: 'info',
									title: 'No se guardo la información',
									confirmButtonText: 'Continuar',
									confirmButtonColor: '#FF121A',
									timer: 2500
								});
							}
						}
					)
			}
        </script>
	</head>
	<body>
		<IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
		<span id="todastablas2"></span>
		<table>
			<tr><script>barra_imagenes("adm");</script><?php cuadro_titulos();?></tr>	 
			<tr><?php menu_desplegable("adm");?></tr>
			<tr>
				<td colspan="3" class="cinta">
					<img src="imagenes/add.png" onClick="location.href='adm-bloqueos.php'" class="mgbt" title="Nuevo"/>
					<img src="imagenes/guarda.png" title="Guardar" onClick="guardar();" class="mgbt"/>
					<img src="imagenes/busca.png" title="Buscar" onClick="document.form2.submit();" class="mgbt"/>
					<img src="imagenes/nv.png" title="Nueva Ventana" onClick="mypop=window.open('adm-principal.php','','');mypop.focus();" class="mgbt"/>
				</td>
			</tr>
		</table>
		<form name="form2" method="post" action="">
			<?php
				if (!$_POST['oculto']){
					$sqlr = "SELECT valor_inicial,valor_final FROM dominios WHERE nombre_dominio = 'FECHA_LIMITE_MODIFICA_DOC'";
					$resp = mysqli_query($linkbd,$sqlr);
					$fila = mysqli_fetch_row($resp);
					preg_match("/([0-9]{4})\-([0-9]{2})\-([0-9]{2})/", $fila[0],$fecha);
					$fechaf = "$fecha[3]/$fecha[2]/$fecha[1]";
					$_POST['nfecha1'] = $_POST['fecha'] = $fechaf;
					$_POST['nvigencia1'] = $_POST['ages'] = $fila[1];
					$chekt = "  ";
				}
				$sqlr = "SELECT * FROM parametros WHERE estado = 'S'";
				$res = mysqli_query($linkbd,$sqlr);
				while($r = mysqli_fetch_row($res)){
					$_POST['vigenciaact'] = $r[1];
				}
			?>
			<table class="inicio ancho">
				<tr>
					<td class="titulos" colspan="4">:: Bloqueos </td>
					<td class="cerrar" style="width:7%" onClick="location.href='adm-principal.php'">Cerrar</td>
				</tr>
				<tr>
					<td class="textonew01" style="width:3.5cm;">:&middot; Fecha General:</td>
					<td style="width:20%;">
						<input name="fecha" type="text" value="<?php echo $_POST['fecha']?>" maxlength="10" onKeyPress="javascript:return solonumeros(event)" onKeyUp="return tabular(event,this)"  id="fc_1198971545" onKeyDown="mascara(this,'/',patron,true)" title="DD/MM/YYYY" onDblClick="displayCalendarFor('fc_1198971545');" class="colordobleclik" autocomplete="off" onChange="" readonly>   
					</td>
					<td class="textonew01" style="width:5cm;">:&middot; Vigencia Trabajo Gral:</td>
					<td>
						<select name="ages">
							<option value="">Seleccione...</option>
							<?php
								for($x=($_POST['vigenciaact']-20);$x<=($_POST['vigenciaact']+5);$x++){
									
									if($_POST['ages'] == $x){
										echo "<option value='$x' SELECTED>$x</option>";
									}else{
										echo "<option value='$x'>$x</option>";
									}
								}
							?>
						</select>
					</td>
				</tr>
			</table>
			<input type="hidden" name="oculto" id="oculto" value="1"> 
			<input type="hidden" name="nfecha1" id="nfecha1" value="<?php echo $_POST['nfecha1']?>"> 
			<input type="hidden" name="nvigencia1" id="nvigencia1" value="<?php echo $_POST['nvigencia1']?>"> 
			<?php
				$oculto = $_POST['oculto'];
				if($_POST['oculto'] == '2')
				{
					$cambios = 0;
					$tam = 0;
					if($_POST['ages'] != $_POST['nvigencia1']){
						$sqlr = "UPDATE dominios SET valor_final = '".$_POST['ages']."' WHERE nombre_dominio = 'FECHA_LIMITE_MODIFICA_DOC'";
						mysqli_query($linkbd,$sqlr);
						cargartrazas('adm', 'dominios', 'editar', '', '0', '0', $_POST['ages'], '', $sqlr);
						$cambios = 1;
						
					}
					if($_POST['fecha'] != $_POST['nfecha1']){
						preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST['fecha'],$fechas);
						$fechan = "$fechas[3]-$fechas[2]-$fechas[1]";
						$sqlr = "UPDATE dominios SET valor_inicial = '$fechan' WHERE nombre_dominio = 'FECHA_LIMITE_MODIFICA_DOC'";
						mysqli_query($linkbd,$sqlr);
						cargartrazas('adm', 'dominios', 'editar', '', '0', '0', $_POST['ages'], '', $sqlr);
						$cambios = 1;
					}
					$tam = count($_POST['id']);		
					for ($x=0;$x<$tam;$x++){
						if(!esta_en_array($_POST['bloqueados'],$_POST['id'][$x])){
							$fechad = $_POST['fechau'][$x];
							$vigusu = $_POST['vigt'][$x];
						}else{
							$fechad = $_POST['fecha'];
							$vigusu = $_POST['ages'];
						}
						if ($_POST['nvigencia'][$x] != $vigusu){
							$sqlr ="UPDATE dominios SET tipo = '$vigusu' WHERE nombre_dominio = 'PERMISO_MODIFICA_DOC' AND valor_inicial = '".$_POST['id'][$x]."'";
							mysqli_query($linkbd,$sqlr);
							cargartrazas('adm', 'dominios', 'editar', $_POST['id'][$x], '0', '0', $vigusu, '', $sqlr);
							$cambios = 1;
						}
						if($_POST['nfecha'][$x] != $fechad){
							preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/",$fechad,$fechas);
							$fechad = "$fechas[3]-$fechas[2]-$fechas[1]";
							$sqlr="UPDATE dominios SET valor_final = '$fechad' WHERE nombre_dominio = 'PERMISO_MODIFICA_DOC' AND valor_inicial = '".$_POST['id'][$x]."'";
							mysqli_query($linkbd,$sqlr);
							cargartrazas('adm', 'dominios', 'editar', $_POST['id'][$x], '0', '0', $vigusu, '', $sqlr);
							$cambios = 1;
						}
					}
					if($cambios > 0){
						echo"
						<script>
							Swal.fire({
								icon: 'success',
								title: 'Se ha Actualizado las Fechas de Bloqueo',
								confirmButtonText: 'Continuar',
								confirmButtonColor: '#FF121A',
								timer: 3000
							});
						</script>";
					}else{
						echo"
						<script>
							Swal.fire({
								icon: 'error',
								title: 'Error!',
								text: 'No se ha Actualizado por que no se digitaron cambios',
								confirmButtonText: 'Continuar',
								confirmButtonColor: '#FF121A',
								timer: 2500
							});
						</script>";
					}
				}
			?>
			<table class='tablamv' >
				<thead>
					<tr><th class="titulos" colspan="8">:: Usuarios </td></tr>
					<tr>
						<th class="titulosnew00" style="width:3%;">Id</th>
						<th class="titulosnew00" style="width:20%;">Nombre</th>
						<th class="titulosnew00" style="width:10%;">Documento</th>
						<th class="titulosnew00" style="width:10%;">Usuario</th>
						<th class="titulosnew00" style="width:15%;">Perfil</th>
						<th class="titulosnew00" style="width:15%;">Fecha Bloqueo</th>
						<th class="titulosnew00" style="width:15%;">Vigencia Trabajo Ppto</th>
						<th class="titulosnew00" >Bloqueo Gral<input id="todos" type="checkbox" name="todos" value="1" onClick="checktodos()" <?php echo $chekt;  ?>></th>
					</tr>
				</thead>
				<tbody style="height:53vh !important;">
					<?php
						$sqlr = "SELECT T2.valor_inicial, T2.descripcion_valor, T2.valor_final, T1.usu_usu, T3.nom_rol, T2.tipo FROM usuarios AS T1, dominios AS T2, roles AS T3 WHERE T2.nombre_dominio = 'PERMISO_MODIFICA_DOC' AND T2.valor_inicial = T1.cc_usu AND T1.id_rol = T3.id_rol";
						$resp = mysqli_query($linkbd,$sqlr);
						$i = 1;
						$idf = 1198971546;
						$co = "saludo1";
						$co2 = "saludo2";
						while($r = mysqli_fetch_row($resp)){
							$idf+=1;
							preg_match("/([0-9]{4})\-([0-9]{2})\-([0-9]{2})/", $r[2],$fecha);
							$fechab = "$fecha[3]/$fecha[2]/$fecha[1]";
							$chk = " ";
							$vig = $r[5];
							if(esta_en_array($_POST['bloqueados'],$r[0]) || !$_POST['oculto']){
								$chk="  ";
							}
							echo "
							<input type='hidden' name='nfecha[]' value='$fechab'>
							<input type='hidden' name='nvigencia[]' value='$r[5]'>
							<input type='hidden' name='id[]' value='$r[0]'>
							<input type='hidden' name='nombres[]' value='$r[1]'>
							<tr class='$co'>
								<td style='width:3%;'>$i</td>
								<td style='width:20%;'>$r[1]</td>
								<td style='width:10%;'>$r[0]</td>
								<td style='width:10%;'>$r[3]</td>
								<td style='width:15%;'>$r[4]</td>
								<td style='width:15%;text-align:Center;'><input name='fechau[]' type='text' value='$fechab' maxlength='10' onKeyPress='javascript:return solonumeros(event)' onKeyUp='return tabular(event,this)'  id='fc_".$idf."' onKeyDown='mascara(this,//,patron,true)' title='DD/MM/YYYY' class='colordobleclik' autocomplete='off' onChange='' onDblClick=\"displayCalendarFor('fc_$idf');\" readonly></td>
								<td style='width:15%;text-align:Center;'>
									<select name='vigt[]'>
										<option value=''>Seleccione...</option>";
							for($x=($_POST['vigenciaact']-20);$x<=($_POST['vigenciaact']+5);$x++){
								if($vig==$x){
									echo "<option value='$x' SELECTED>$x</option>";	
								}else{
									echo "<option value='$x'>$x</option>";	
								}
							}
							echo "
								</td>";
							echo "
								<td style='text-align:Center;'><input type='checkbox' name='bloqueados[]' value='$r[0]' ".$chk."></td>
							</tr>";
							$i+=1;
							$aux=$co;
							$co=$co2;
							$co2=$aux;
						}
					?>
				</tbody>
			</table>
		</form>      
	</body>
</html>