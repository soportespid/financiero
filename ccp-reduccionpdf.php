<?php
	require_once("tcpdf/tcpdf_include.php");
	require('comun.inc');
	require "funciones.inc";
	date_default_timezone_set("America/Bogota");
	session_start();	
    $linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");

	$sqlr = "SELECT id_cargo, id_comprobante FROM pptofirmas WHERE id_comprobante='6' AND vigencia='".$_POST['vigencia']."'";
	$res = mysqli_query($linkbd,$sqlr);
	while($row=mysqli_fetch_assoc($res))
	{
		if($row["id_cargo"] == '0')
		{
			$_POST['ppto'][] = buscatercero($_POST['tercero']);
			$_POST['nomcargo'][]='BENEFICIARIO';
		}
		else
		{
			$sqlr1="SELECT cedulanit,(SELECT nombrecargo FROM planaccargos WHERE codcargo='".$row["id_cargo"]."') FROM planestructura_terceros WHERE codcargo='".$row["id_cargo"]."' AND estado='S'";
			$res1 = mysqli_query($linkbd,$sqlr1);
			$row1 = mysqli_fetch_row($res1);
			$_POST['ppto'][] = buscatercero($row1[0]);
			$_POST['nomcargo'][] = $row1[1];
		}
	}
	class MYPDF extends TCPDF 
	{
		public function Header() 
		{
			$linkbd = conectar_v7();
			$linkbd -> set_charset("utf8");
			$sqlr="select *from configbasica where estado='S' ";
			//echo $sqlr;
			$res=mysqli_query($linkbd, $sqlr);
			while($row=mysqli_fetch_row($res))
			{
				$nit=$row[0];
				$rs=$row[1];
				$nalca=$row[6];
			}
			$detallegreso = $_POST['detallegreso'];
			//Parte Izquierda
			$this->Image('imagenes/escudo.jpg',  13, 13, 20, 20, 'JPG', '', 'T', true, 300, '', false, false, 0, false, false, false);// Logo
			$this->SetFont('helvetica','B',8);
			$this->SetY(10);
			$this->RoundedRect(10, 10, 277, 25, 1,'1001');
			$this->Cell(0.1);
			$this->Cell(26,25,'','R',0,'L'); 
			$this->SetY(8);
			$this->SetX(80);
			$this->SetFont('helvetica','B',9);
			$this->Cell(160,15,strtoupper("$rs"),0,0,'C'); 
			$this->SetFont('helvetica','B',7);
			$this->SetY(12);
			$this->SetX(80);
			$this->Cell(160,15,'NIT: '.$nit,0,0,'C');
			//*****************************************************************************************************************************
			$this->SetFont('helvetica','B',9);			 
			$this->SetY(23);
			$this->SetX(36);
			$this->Cell(251,12,"CERTIFICADO DE DISPONIBILIDAD PRESUPUESTAL",'T',0,'C'); 
			$this->SetFont('helvetica','B',6);
			$this->SetY(10);
			$this->SetX(257);
			$this->Cell(30,4," Acto Administrativo: ".$_POST['actoAdmId'],"L",0,'L');
			$this->SetY(14);
			$this->SetX(257);
			$this->Cell(35,5," Fecha: ".$_POST['fecha'],"L",0,'L');
			$this->SetY(19);
			$this->SetX(257);
			$this->Cell(35,4," Vigencia: ".$_POST['vigencia'],"L",1,'L');
            $this->SetY(35);
            $this->SetX(10);
			$this->SetFont('helvetica','B',10);
			$lineas = $this->getNumLines($_POST['actoAdm'],251);
			$lineas2 = $lineas * 5;
			$this->Cell(27,$lineas2,'Acto adm:','LB',0,'C');
			$this->SetFont('helvetica','',10);
			$this->MultiCell(180,$lineas2,ucfirst(strtolower($_POST['actoAdm'])),'B','L',0,0,'','');
			$this->SetFont('helvetica','B',10);
            $this->MultiCell(40,$lineas2,'Estado reducción:','B','R',0,0,'','');
			$this->SetFont('helvetica','',10);
            $this->MultiCell(30,$lineas2,$_POST['estado'],'RB','C',0,1,'','');
        }
		public function Footer() 
		{
			$linkbd = conectar_v7();
			$linkbd -> set_charset("utf8");
			$sqlr="SELECT direccion,telefono,web,email FROM configbasica WHERE estado='S'";
			$resp=mysqli_query($linkbd, $sqlr);
			$user = $_SESSION['nickusu'];	
			$fecha = date("Y-m-d H:i:s");
			$ip = $_SERVER['REMOTE_ADDR'];
			$useri = $_POST['user'];
			while($row=mysqli_fetch_row($resp))
			{
				$direcc=strtoupper($row[0]);
				$telefonos=$row[1];
				$dirweb=strtoupper($row[3]);
				$coemail=strtoupper($row[2]);
			}
			if($direcc!=''){$vardirec="Dirección: $direcc, ";}
			else {$vardirec="";}
			if($telefonos!=''){$vartelef="Telefonos: $telefonos";}
			else{$vartelef="";}
			if($dirweb!=''){$varemail="Email: $dirweb, ";}
			else {$varemail="";}
			if($coemail!=''){$varpagiw="Pagina Web: $coemail";}
			else{$varpagiw="";}
			$this->SetFont('helvetica', 'I', 8);
			$txt = <<<EOD
			$vardirec $vartelef
			$varemail $varpagiw
			EOD;
			$this->SetFont('helvetica', 'I', 6);
			$this->Cell(277,10,'','T',0,'T');
			$this->ln(2);
			$this->Write(0, $txt , '', 0, 'C', true, 0, false, false, 0);
			//$this->Cell(25, 10, 'Hecho por: '.$useri, 0, false, 'C', 0, '', 0, false, 'T', 'M');
			$this->Cell(25, 10, 'Impreso por: '.$user, 0, false, 'C', 0, '', 0, false, 'T', 'M');
            $this->SetX(25);
			$this->Cell(107, 10, 'IP: '.$ip, 0, false, 'C', 0, '', 0, false, 'T', 'M');
			$this->Cell(35, 10, 'Fecha: '.$fecha, 0, false, 'C', 0, '', 0, false, 'T', 'M');
            $this->Cell(107, 10, 'IDEAL.10 S.A.S', 0, false, 'C', 0, '', 0, false, 'T', 'M');
			$this->Cell(20, 10, 'Pagina '.$this->getAliasNumPage().'/'.$this->getAliasNbPages(), 0, false, 'C', 0, '', 0, false, 'T','M');

		}
	}
	$pdf = new MYPDF('L','mm','Letter', true, 'iso-8859-1', false);
	$pdf->SetDocInfoUnicode (true); 
	// set document information
	$pdf->SetCreator(PDF_CREATOR);
	$pdf->SetAuthor('IDEALSAS');
	$pdf->SetTitle('Certificados');
	$pdf->SetSubject('CERTIFICADO DE REDUCCIÓN PRESUPUESTAL');
	$pdf->SetKeywords('TCPDF, PDF, example, test, guide');
	$pdf->SetMargins(10, 43, 10);// set margins
	$pdf->SetHeaderMargin(43);// set margins
	$pdf->SetFooterMargin(17);// set margins
	$pdf->SetAutoPageBreak(TRUE, 20);// set auto page breaks
	// set some language-dependent strings (optional)
	if (@file_exists(dirname(__FILE__).'/lang/spa.php')) 
	{
		require_once(dirname(__FILE__).'/lang/spa.php');
		$pdf->setLanguageArray($l);
	}	
	$pdf->AddPage();
    $y=$pdf->GetY()+5;	
	$pdf->SetFillColor(222,222,222);
	$pdf->SetFont('helvetica','B',6);
    $pdf->SetY($y);
    $pdf->Cell(14,5,'Tipo cuenta',0,0,'C',1); 
	$pdf->SetY($y);
	$pdf->Cell(15);
	$pdf->Cell(20,5,'Tipo de gasto',0,0,'C',1);
	$pdf->SetY($y);
	$pdf->Cell(36);
	$pdf->Cell(20,5,'Sec. presupuestal ',0,0,'C',1);
	$pdf->SetY($y);
	$pdf->Cell(57);
	$pdf->Cell(16,5,'Medio de pago',0,0,'C',1);
	$pdf->SetY($y);
	$pdf->Cell(74);
	$pdf->Cell(25,5,'Vig. del gasto',0,0,'C',1);
	$pdf->SetY($y);
	$pdf->Cell(100);
	$pdf->Cell(35,5,'Proyecto',0,0,'C',1);
	$pdf->SetY($y);
	$pdf->Cell(136);
	$pdf->Cell(35,5,'Programático',0,0,'C',1);
	$pdf->SetY($y);
	$pdf->Cell(172);
	$pdf->Cell(30,5,'Cuenta',0,0,'C',1);
	$pdf->SetY($y);
	$pdf->Cell(203);
	$pdf->Cell(25,5,'Fuente',0,0,'C',1);
    $pdf->SetY($y);
	$pdf->Cell(229);
	$pdf->Cell(24,5,'Valor ingresos',0,0,'C',1);
	$pdf->SetY($y);
	$pdf->Cell(254);
	$pdf->Cell(23,5,'Valor gastos',0,1,'C',1);
	$con=0;
	while ($con<count($_POST['tipoCuenta']))
	{
	
		if ($con%2==0){
			$pdf->SetFillColor(255,255,255);
		}else{
			$pdf->SetFillColor(245,245,245);
		}
        $alturasec = $pdf->getNumLines($_POST['secPresupuestal'][$con],21);
        $alturaproy = $pdf->getNumLines($_POST['proyecto'][$con],36);
        $alturaprog = $pdf->getNumLines($_POST['programatico'][$con],36);
        $alturacu = $pdf->getNumLines($_POST['cuenta'][$con],31);
        $alturafu = $pdf->getNumLines($_POST['fuente'][$con],26);
        $alturamax = max($alturasec,$alturaproy,$alturaprog,$alturacu,$alturafu);
		$altura = $alturamax * 3;
		//mostrar tabla
		$pdf->SetFont('helvetica','',6);
		$pdf->MultiCell(15,$altura,$_POST['tipoCuenta'][$con],0,'C',true,0,'','',true,0,false,true,0,'M',true);
		$pdf->MultiCell(21,$altura,$_POST['tipoGasto'][$con],0,'C',true,0,'','',true,0,false,true,0,'M',true);
		$pdf->MultiCell(21,$altura,$_POST['secPresupuestal'][$con],0,'C',true,0,'','',true,0,false,true,0,'M',true);
		$pdf->MultiCell(17,$altura,$_POST['medioPago'][$con],0,'C',true,0,'','',true,0,false,true,0,'M',true);
		$pdf->MultiCell(26,$altura,$_POST['vigGasto'][$con],0,'C',true,0,'','',true,0,false,true,0,'M',true);
		$pdf->MultiCell(36,$altura,$_POST['proyecto'][$con],0,'C',true,0,'','',true,0,false,true,0,'M',true);
		$pdf->MultiCell(36,$altura,$_POST['programatico'][$con],0,'C',true,0,'','',true,0,false,true,0,'M',true);
		$pdf->MultiCell(31,$altura,$_POST['cuenta'][$con],0,'C',true,0,'','',true,0,false,true,0,'M',true);
		$pdf->MultiCell(26,$altura,$_POST['fuente'][$con],0,'C',true,0,'','',true,0,false,true,0,'M',true);
		$pdf->MultiCell(25,$altura,"$".number_format($_POST['valorIngreso'][$con],2,$_SESSION["spdecimal"],$_SESSION["spmillares"]),0,'R',true,0,'','',true,0,false,true,0,'M',true);
		$pdf->MultiCell(23,$altura,"$ ".number_format($_POST['valorGasto'][$con],2,$_SESSION["spdecimal"],$_SESSION["spmillares"]),0,'R',true,1,'','',true,0,false,true,0,'M',true);
		$con=$con+1;
	}
    $pdf->ln(1);
	$pdf->SetFont('helvetica','B',7);
    $pdf->Cell(199,5,'SON: '.strtoupper($_POST['letras']." M/CTE"),'T',0,'L');
    $pdf->SetFont('helvetica','B',7);
    $pdf->Cell(30,5,'Total:','T',0,'R');
    $pdf->SetFont('helvetica','',7);
    $pdf->Cell(25,5,"$".number_format($_POST['totalGasto'],2,$_SESSION["spdecimal"],$_SESSION["spmillares"]),'T',0,'R');
	$pdf->Cell(23,5,"$".number_format($_POST['totalIngreso'],2,$_SESSION["spdecimal"],$_SESSION["spmillares"]),'T',1,'R');
    $sql="SELECT user FROM pptocdp WHERE consvigencia='".$_POST['numero']."' AND vigencia='".$_POST['vigencia']."' ";
	$res=mysqli_query($linkbd,$sql);
	$row = mysqli_fetch_row($res);
    for($x=0;$x<count($_POST['ppto']);$x++)
	{
		$pdf->ln(20);
		$v=$pdf->gety();
		if($v>=180){
			$pdf->AddPage();
			$pdf->ln(20);
			$v=$pdf->gety();
		}
		$pdf->setFont('times','B',8);
		if (($x%2)==0) {
			if(isset($_POST['ppto'][$x+1])){
				$pdf->Line(17,$v,107,$v);
				$pdf->Line(112,$v,202,$v);
				$v2=$pdf->gety();
				$pdf->Cell(104,4,''.$_POST['ppto'][$x],0,1,'C',false,0,0,false,'T','C');
				$pdf->Cell(200,4,''.$_POST['nomcargo'][$x],0,1,'C',false,0,0,false,'T','C');
				$pdf->SetY($v2);
				$pdf->Cell(295,4,''.$_POST['ppto'][$x+1],0,1,'C',false,0,0,false,'T','C');
				$pdf->Cell(200,4,''.$_POST['nomcargo'][$x+1],0,1,'C',false,0,0,false,'T','C');
			}else{
				$pdf->Line(100,$v,200,$v);
				$pdf->Cell(280,4,''.$_POST['ppto'][$x],0,1,'C',false,0,0,false,'T','C');
				$pdf->Cell(280,4,''.$_POST['nomcargo'][$x],0,0,'C',false,0,0,false,'T','C');
			}
			$v3=$pdf->gety();
		}
		$pdf->SetY($v3);
		$pdf->SetFont('helvetica','',7);
	}
    $pdf->Output();