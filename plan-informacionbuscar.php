<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require 'comun.inc';
	require 'funciones.inc';
	sesion();
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
	cargarcodigopag($_GET['codpag'],$_SESSION['nivel']);
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html lang="es">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=9">
		<meta name="viewport" content="user-scalable=no">
		<title>:: IDEAL 10 - Planeación Estrategica</title>
		<link href="css/css2.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css">
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css">
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css">
		<link href="css/tabs.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css">
		<link rel="stylesheet" href="sweetalert2/dist/sweetalert2.min.css">
		<script src="sweetalert2/dist/sweetalert2.min.js"></script>
		<script type="text/javascript" src="css/calendario.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script type="text/javascript" src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script type='text/javascript' src='JQuery/jquery-2.1.4.min.js'></script>
		<script>
			function cambioswitch(id,valor){
				document.getElementById('idestado').value = id;
				if(valor==1){
					Swal.fire({
						icon: 'question',
						title: '¿Desea Activar esta Publicación?',
						showDenyButton: true,
						confirmButtonText: 'Cambiar',
						confirmButtonColor: '#01CC42',
						denyButtonText: 'Cancelar',
						denyButtonColor: '#FF121A',
						timer: 3500
					}).then((result) => {
						if (result.isConfirmed) {
							document.form2.cambioestado.value = "1";
							document.form2.submit();
						} else {
							document.form2.nocambioestado.value= "0";
							document.form2.submit();
						}
					});
				}else{
					Swal.fire({
						icon: 'question',
						title: '¿Desea Desactivar esta Publicación?',
						showDenyButton: true,
						confirmButtonText: 'Cambiar',
						confirmButtonColor: '#01CC42',
						denyButtonText: 'Cancelar',
						denyButtonColor: '#FF121A',
						timer: 3500
					}).then((result) => {
						if (result.isConfirmed) {
							document.form2.cambioestado.value = "0";
							document.form2.submit();
							
						} else {
							document.form2.nocambioestado.value = "1";
							document.form2.submit();
						}
					});
				}
			}
			function eliminar_registro(id){
				Swal.fire({
						icon: 'question',
						title: '¿Desea eliminar esta Publicación?',
						showDenyButton: true,
						confirmButtonText: 'Eliminar',
						confirmButtonColor: '#01CC42',
						denyButtonText: 'Cancelar',
						denyButtonColor: '#FF121A',
						timer: 3500
					}).then((result) => {
						if (result.isConfirmed) {
							alert(id);
							
						} else {
							
						}
					});
			}
		</script>
	</head>
	<body>
		<IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
		<span id="todastablas2"></span>
		<table>
			<tr><script>barra_imagenes("meci");</script><?php cuadro_titulos();?></tr>	 
			<tr><?php menu_desplegable("meci");?></tr>
			<tr>
				<td colspan="3" class="cinta"><a href="plan-informacionguardar.php" class="mgbt"><img src="imagenes/add.png" title="Nuevo"/></a><a class="mgbt"><img src="imagenes/guardad.png" /></a><a href="#" onClick="document.form2.submit();" class="mgbt"><img src="imagenes/busca.png" title="Buscar" /></a><a href="#" onClick="mypop=window.open('meci-principal.php','','');mypop.focus();" class="mgbt"><img src="imagenes/nv.png" title="Nueva Ventana"></a></td>
			</tr>
		</table>
		<div id="bgventanamodalm" class="bgventanamodalm">
			<div id="ventanamodalm" class="ventanamodalm">
				<IFRAME src="" name="ventanam" marginWidth=0 marginHeight=0 frameBorder=0 id="ventanam" frameSpacing=0 style=" width:700px; height:130px; top:200; overflow:hidden;"> 
				</IFRAME>
			</div>
		</div>	
		<form  name="form2" method="post" action=""> 
			<input type="hidden" name="cambioestado" id="cambioestado" value="<?php echo $_POST['cambioestado'];?>"/>
			<input type="hidden" name="nocambioestado" id="nocambioestado" value="<?php echo $_POST['nocambioestado'];?>"/>
			<?php
				if($_POST['oculto'] == ""){
					$_POST['numpos'] = 0;
					$_POST['numres'] = 10;
					$_POST['nummul'] = 0;
					$_POST['oculto2'] = "0";
					$_POST['cambioestado'] = "";
					$_POST['nocambioestado'] = "";
				}
				if($_POST['cambioestado']!=""){
					if($_POST['cambioestado']=="1"){
						$sqlr = "UPDATE infor_interes SET estado='S' WHERE indices='".$_POST['idestado']."'";
						mysqli_fetch_row(mysqli_query($linkbd,$sqlr)); 
					}else{
						$sqlr ="UPDATE infor_interes SET estado='N' WHERE indices='$_POST[idestado]'";
						mysqli_fetch_row(mysqli_query($linkbd,$sqlr)); 
					}
					echo"<script>document.form2.cambioestado.value=''</script>";
				}
				//*****************************************************************
				if($_POST['nocambioestado']!=""){
					if($_POST['nocambioestado']=="1"){$_POST['lswitch1'][$_POST['idestado']]=1;}
					else {$_POST['lswitch1'][$_POST['idestado']]=0;}
					echo"<script>document.form2.nocambioestado.value=''</script>";
				}
			?>
			<table class="inicio">
				<tr>
					<td colspan="4" class="titulos" >:.Buscar Informaci&oacute;n de Inter&eacute;s </td>
					<td class="cerrar" style="width:7%" ><a href="meci-principal.php">&nbsp;Cerrar</a></td>
				</tr>
				<tr >
					<td width="13%" class="saludo1" >:&middot; Prioridad:</td>
					<td ><input type="text" name="numero" >
					<input type="button" name="bboton" onClick="limbusquedas();" value="&nbsp;&nbsp;Buscar&nbsp;&nbsp;" />
						
					</td>
				</tr>
			</table>
			<input type="hidden" name="oculto"  id="oculto" value="1"/>
			<input type="hidden" name="ocudel" id="ocudel" value="1">
			<input type="hidden" name="iddel" id="iddel" value="1">
			<input type="hidden" name="idestado" id="idestado" value="<?php echo $_POST['idestado'];?>"/> 
			<input type="hidden" name="numres" id="numres" value="<?php echo $_POST['numres'];?>"/>
			<input type="hidden" name="numpos" id="numpos" value="<?php echo $_POST['numpos'];?>"/>
			<input type="hidden" name="nummul" id="nummul" value="<?php echo $_POST['nummul'];?>"/>
			<div class="subpantallap" style="height:69%; width:99.6%; overflow-x:hidden;">
				<?php 
					$cond = "WHERE titulos like'%".$_POST['numero']."%'";
					$sqlr = "SELECT * FROM infor_interes  $cond  ORDER BY indices ASC"; 
					$resp = mysqli_query($linkbd,$sqlr);
					$_POST['numtop'] = mysqli_num_rows($resp);
					$nuncilumnas = ceil($_POST['numtop']/$_POST['numres']);
					$sqlr = "SELECT * FROM infor_interes  $cond  ORDER BY indices ASC"; 
					$res = mysqli_query($linkbd,$sqlr);
					$numcontrol = $_POST['nummul']+1;
					if($nuncilumnas==$numcontrol){
						$imagenforward = "<img src='imagenes/forward02.png' style='width:17px'>";
						$imagensforward = "<img src='imagenes/skip_forward02.png' style='width:16px' >";
					}else{
						$imagenforward = "<img src='imagenes/forward01.png' style='width:17px' title='Siguiente' onClick='numsiguiente()'>";
						$imagensforward = "<img src='imagenes/skip_forward01.png' style='width:16px' title='Fin' onClick='saltocol(\"$nuncilumnas\")'>";
					}
					if($_POST['numpos']==0){
						$imagenback = "<img src='imagenes/back02.png' style='width:17px'>";
						$imagensback = "<img src='imagenes/skip_back02.png' style='width:16px'>";
					}else{
						$imagenback = "<img src='imagenes/back01.png' style='width:17px' title='Anterior' onClick='numanterior();'>";
						$imagensback = "<img src='imagenes/skip_back01.png' style='width:16px' title='Inicio' onClick='saltocol(\"1\")'>";
					}
					echo"
					<table class='inicio'>        
						<tr>
							<td class='titulos' colspan='6'>:: Publicaciones</td>
							<td class='submenu'>
								<select name='renumres' id='renumres' onChange='cambionum();' style='width:100%'>
									<option value='10'"; if ($_POST['renumres']=='10'){echo 'selected';} echo ">10</option>
									<option value='20'"; if ($_POST['renumres']=='20'){echo 'selected';} echo ">20</option>
									<option value='30'"; if ($_POST['renumres']=='30'){echo 'selected';} echo ">30</option>
									<option value='50'"; if ($_POST['renumres']=='50'){echo 'selected';} echo ">50</option>
									<option value='100'"; if ($_POST['renumres']=='100'){echo 'selected';} echo ">100</option>
								</select>
							</td>
						</tr>
						<tr><td colspan='7'>Articulos Encontrados: $_POST[numtop]</td></tr>
						<tr >
							<td class='titulos2'># Publicacion</td>
							<td class='titulos2'>Titulo</td>
							<td class='titulos2'>Imagen</td>
							<td class='titulos2'>Texto</td>
							<td class='titulos2' colspan='2' style='width:6%'>Estado</td>
							<td class='titulos2' style='width:5%'>Editar</td>
							<td class='titulos2' style='width:5%'>Eliminar</td>
						</tr>";
					$iter = 'saludo1a';
					$iter2 = 'saludo2';
					while ($row = mysqli_fetch_row($res)){
						if($row[10]=='S') {
							$imgsem = "src='imagenes/sema_verdeON.jpg' title='Activo'";$coloracti="#0F0";$_POST['lswitch1'][$row[0]]=0;
						}else{
							$imgsem = "src='imagenes/sema_rojoON.jpg' title='Inactivo'";$coloracti="#C00";$_POST['lswitch1'][$row[0]]=1;
						}
						
						if($row[0] == $_GET['codigo']){
							echo "
							<tr class='' style='background-color: yellow;' onDblClick=\"location.href='plan-informacioneditar.php?codigo=$row[0]'\" style='text-transform:uppercase'>";
						}else{
							echo "
							<tr class='$iter' onDblClick=\"location.href='plan-informacioneditar.php?codigo=$row[0]'\" style='text-transform:uppercase'>";
						}
						echo "
							<td>$row[0]</td>
							<td>$row[2]</td>
							<td>$row[3]</td>
							<td>$row[5]</td>
							<td style='text-align:center;'><img $imgsem style='width:20px'/></td>
							<td><input type='range' name='lswitch1[]' value='".$_POST['lswitch1'][$row[0]]."' min ='0' max='1' step ='1' style='background:$coloracti; width:60%' onChange='cambioswitch(\"$row[0]\",\"".$_POST['lswitch1'][$row[0]]."\")' /></td>
							<td style='text-align:center;'><a href='plan-informacioneditar.php?codigo=$row[0]'><img src='imagenes/b_edit.png'></a></td>
							<td><a href='#'><img src='imagenes/del.png' onClick=\"eliminar_registro('$row[0]');\" /></a></td>
						</tr>";
						$aux = $iter;
						$iter = $iter2;
						$iter2 = $aux;
					}
					if ($_POST['numtop'] == 0){
						echo "
						<table class='inicio'>
							<tr>
								<td class='saludo1' style='text-align:center;width:100%'><img src='imagenes\alert.png' style='width:25px'>No hay coincidencias en la b&uacute;squeda $tibusqueda<img src='imagenes\alert.png' style='width:25px'></td>
							</tr>
						</table>";
					}
					echo"
						</table>
						<table class='inicio'>
							<tr>
								<td style='text-align:center;'>
									<a href='#'>$imagensback</a>&nbsp;
									<a href='#'>$imagenback</a>&nbsp;&nbsp;";
					if($nuncilumnas<=9){$numfin=$nuncilumnas;}
					else{$numfin=9;}
					for($xx = 1; $xx <= $numfin; $xx++)
					{
						if($numcontrol<=9){$numx=$xx;}
						else{$numx=$xx+($numcontrol-9);}
						if($numcontrol==$numx){echo"<a href='#' onClick='saltocol(\"$numx\")'; style='color:#24D915'> $numx </a>";}
						else {echo"<a href='#' onClick='saltocol(\"$numx\")'; style='color:#000000'> $numx </a>";}
					}
					echo "		&nbsp;&nbsp;<a href='#'>$imagenforward</a>
									&nbsp;<a href='#'>$imagensforward</a>
								</td>
							</tr>
						</table>";
				?>
			</div>
			<input type="hidden" name="numtop" id="numtop" value="<?php echo $_POST['numtop'];?>" />
		</form>
	</body>
</html>