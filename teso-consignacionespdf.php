<?php
	require_once("tcpdf/tcpdf_include.php");    
	require('comun.inc');
	session_start();	
	date_default_timezone_set("America/Bogota");
    class MYPDF extends TCPDF 
	{
        public function Header() 
		{
            $linkbd=conectar_v7();
			$linkbd -> set_charset("utf8");
			$sqlr = "SELECT * FROM configbasica WHERE estado='S'";
			$res = mysqli_query($linkbd,$sqlr);
			while($row=mysqli_fetch_row($res)){
				$nit=$row[0];
				$rs=$row[1];
			}
			$this->Image('imagenes/escudo.jpg', 12, 12, 25, 25, 'JPG', '', 'T', true, 300, '', false, false, 0, false, false, false);// Logo
			$this->SetFont('helvetica','B',8);
			$this->SetY(10);
            $this->RoundedRect(10, 10, 190, 29, 1,'1001' );
			$this->Cell(30,29,'','R',0,'L'); 
			$this->SetY(10);
			$this->SetX(40);
			$this->SetFont('helvetica','B',12);
			$this->Cell(127,13,"$rs",0,0,'C'); 
			$this->SetY(16);
			$this->SetX(40);
			$this->SetFont('helvetica','B',11);
			$this->Cell(127,10,"$nit",0,0,'C');
			//*****************************************************************************************************************************
			$this->SetFont('helvetica','B',14);
			$this->SetY(10);
			$this->Cell(50.1);
			$this->Cell(149,31,'',0,1,'C'); 
			$this->SetY(8);
			$this->Cell(50.1);
			//************************************
			$this->SetFont('helvetica','B',10);
			$this->SetY(10);
			$this->Cell(157);
			$this->Cell(33,15,'','LB',0,'L');
			$this->SetY(10);
			$this->Cell(157.5);
			$this->Cell(33,8,'Número: '.$_POST['idcomp'],0,0,'L');
			$this->SetY(18);
			$this->Cell(157.5);
			$this->Cell(33,7,'Fecha: '.$_POST['fecha'],0,0,'L');
			$this->SetY(20);
            $this->SetY(25);
            $this->SetX(40);
        	$this->SetFont('helvetica','B',12);
            $this->Cell(127,14,'COMPROBANTE CONSIGNACIONES','T',1,'C'); 
			$this->SetFont('helvetica','B',10);
			$lineas = $this->getNumLines($_POST['concepto'],160);
			$lineas2 = $lineas * 5;
			$this->Cell(30,$lineas2,'DETALLE:','LB',0,'C');
			$this->SetFont('helvetica','',10);
			$this->MultiCell(160,$lineas2,$_POST['concepto'],'RB','L',0,1,'','');

		}
        public function Footer() 
		{
			$linkbd = conectar_v7();
			$linkbd -> set_charset("utf8");
			$sqlr="SELECT direccion,telefono,web,email FROM configbasica WHERE estado='S'";
			$resp=mysqli_query($linkbd, $sqlr);
			$user = $_SESSION['nickusu'];	
			$fecha = date("Y-m-d H:i:s");
			$ip = $_SERVER['REMOTE_ADDR'];
			$useri = $_POST['user'];
			while($row=mysqli_fetch_row($resp))
			{
				$direcc=strtoupper($row[0]);
				$telefonos=$row[1];
				$dirweb=strtoupper($row[3]);
				$coemail=strtoupper($row[2]);
			}
			if($direcc!=''){$vardirec="Dirección: $direcc, ";}
			else {$vardirec="";}
			if($telefonos!=''){$vartelef="Telefonos: $telefonos";}
			else{$vartelef="";}
			if($dirweb!=''){$varemail="Email: $dirweb, ";}
			else {$varemail="";}
			if($coemail!=''){$varpagiw="Pagina Web: $coemail";}
			else{$varpagiw="";}
			
			$txt = <<<EOD
			$vardirec $vartelef
			$varemail $varpagiw
			EOD;
			$this->SetFont('helvetica', 'I', 5);
			$this->Cell(190,7,'','T',0,'T');
			$this->ln(1);
			$this->Write(0, $txt , '', 0, 'C', true, 0, false, false, 0);
			//$this->Cell(12, 7, 'Hecho por: '.$useri, 0, false, 'C', 0, '', 0, false, 'T', 'M');
			$this->Cell(15, 7, 'Impreso por: '.$user, 0, false, 'C', 0, '', 0, false, 'T', 'M');
			$this->Cell(57, 7, 'IP: '.$ip, 0, false, 'C', 0, '', 0, false, 'T', 'M');
			$this->Cell(50, 7, 'Fecha: '.$fecha, 0, false, 'C', 0, '', 0, false, 'T', 'M');
			$this->Cell(54, 7, 'IDEAL.10 S.A.S', 0, false, 'C', 0, '', 0, false, 'T', 'M');
			$this->Cell(25, 7, 'Pagina '.$this->getAliasNumPage().'/'.$this->getAliasNbPages(), 0, false, 'C', 0, '', 0, false, 'T', 'M');

        }
    }
    $pdf = new MYPDF('P','mm','Letter', true, 'utf8', false);
	$pdf->SetDocInfoUnicode (true); 
	// set document information
	$pdf->SetCreator(PDF_CREATOR);
	$pdf->SetAuthor('IDEAL10');
	$pdf->SetTitle('Comprobante');
	$pdf->SetSubject('Comprobante');
	$pdf->SetKeywords('Comprobante');
    $pdf->SetMargins(10, 45, 10);// set margins
    $pdf->SetHeaderMargin(45);// set margins
    $pdf->SetFooterMargin(17);// set margins
	$pdf->SetAutoPageBreak(TRUE, 20);// set auto page breaks
	// set some language-dependent strings (optional)
	if (@file_exists(dirname(__FILE__).'/lang/spa.php')) 
	{
		require_once(dirname(__FILE__).'/lang/spa.php');
		$pdf->setLanguageArray($l);
	}
	$pdf->AddPage();	
	$pdf ->ln(5);
	$yy=$pdf->GetY();	
	$pdf->SetY($yy);
	$yy=$pdf->GetY();	
	$pdf->SetFillColor(222,222,222);
	$pdf->SetFont('helvetica','B',8);
	$pdf->Cell(0.1);
	$pdf->Cell(15,5,'CC',0,0,'C',1); 
	$pdf->SetY($yy);
	$pdf->Cell(16);
	$pdf->Cell(35,5,'CONSIGNACIÓN',0,0,'C',1);
	$pdf->SetY($yy);
	$pdf->Cell(52);
	$pdf->Cell(40,5,'CUENTA BANCARIA',0,0,'C',1);
	$pdf->SetY($yy);
	$pdf->SetFont('helvetica','B',7);
	$pdf->Cell(93);
	$pdf->Cell(68,5,'BANCO',0,0,'C',1);
	
	$pdf->SetY($yy);
	$pdf->Cell(162);
	$pdf->MultiCell(28,5,'VALOR',0,'C',1,1,'','',true,'',false,true,0,'M',true);
	$pdf->SetFont('helvetica','',7);
	$pdf->ln(1);
	$con=0;
	while ($con<count($_POST['dccs']))
	{	
		if($vv>=170)
        { 
            $pdf->AddPage();
            $vv=$pdf->gety();
        }
		if ($con%2==0)
		{
			$pdf->SetFillColor(245,245,245);
		}
		else
		{
			$pdf->SetFillColor(255,255,255);
		}	
		$pdf->MultiCell(16,4,''.$_POST['dccs'][$con],0,'C',true,0,'','',true,0,false,true,0,'M',true);
		$pdf->MultiCell(36,4,''.$_POST['dconsig'][$con],0,'C',true,0,'','',true,0,false,true,0,'M',true);
		$pdf->MultiCell(41,4,$_POST['dcbs'][$con],0,'C',true,0,'','',true,0,false,true,0,'M',true);
		$pdf->MultiCell(69,4,$_POST['dnbancos'][$con],0,'L',true,0,'','',true,0,false,true,0,'M',true);
		$pdf->MultiCell(28,4,'$'.number_format($_POST['dvalores'][$con],2),0,'R',true,1,'','',true,0,false,true,0,'M',true);
		$con=$con+1;
	}
	$pdf->ln(2);
    $pdf->Cell(164,5,'Total','T',0,'R');
    $pdf->SetX(174);
	$pdf->Cell(26,5,$_POST['totalcf'],'T',0,'R');
	$pdf ->ln(5);	
	$pdf->SetFont('helvetica','B',9);
	$pdf->MultiCell(190,4,'SON: '.$_POST['letras'],'T','L');
	$pdf->ln(25);
	$pdf->Cell(55);
	$pdf->Cell(80,4,''.$_SESSION['usuario'],'T',1,'C');
	$pdf->cell(55);
	$pdf->Cell(80,5,'ELABORO','',0,'C');

	$pdf->Output();