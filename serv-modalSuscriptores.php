<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require 'comun.inc';
	require 'funciones.inc';
    require 'funcionesSP.inc.php';
	$linkbd = conectar_v7();
    $linkbd -> set_charset("utf8"); 
    titlepag();
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es">
	<head>
		<meta http-equiv="Content-Type" content="text/html" charset="utf8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=9"/>
		<title>:: Suscriptores</title>
		<link href="css/css2.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
        <link href="css/cssSP.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<script type="text/javascript" src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
        <script type="text/javascript" src="jquery-1.11.0.min.js"></script> 
		<script>
			function agregar(id, documento, nombre)
			{
				parent.document.form2.documentoTercero.value = documento;
                parent.document.form2.id_tercero.value = id;
                parent.document.form2.nombreTercero.value = nombre;
				parent.despliegamodal("hidden");
			}

            $(window).load(function () {
				$('#cargando').hide();
			});
		</script> 
	</head>
	<body>
		<form name="form2" method="post">
			<table class="inicio ancho" style="width:99.5%">
				<tr>
					<td class="titulos" colspan="3">Suscriptores - Servicios Publicos</td>
					<td class="cerrar" style="width:7%" onClick="parent.despliegamodal('hidden');">Cerrar</td>
				</tr>
				
				<tr>
					<td class="tamano01" style='width:3cm;'>Cedula o Nombre:</td>
					<td>
						<input type="search" name="codnom" id="codnom" value="<?php echo @$_POST['codnom'];?>" style='width:100%;'/>
					</td>

					<td style="padding-bottom:0px;height:35px;"><em class="botonflecha" onClick="document.form2.submit();">Buscar</em></td>
				</tr>
			</table>

            <div id="cargando" style=" position:absolute;left: 46%; bottom: 45%">
				<img src="imagenes/loading.gif" style=" width: 80px; height: 80px"/>
			</div>

			<div class="subpantalla" style="height:82%; width:99.2%; overflow-x:hidden;">
				<table class='inicio' align='center' width='99%'>	
                    <tr>
						<td colspan='6' class='titulos'>Resultados Busqueda: </td>
					</tr>

					<tr class='titulos2' style='text-align:center;'>
						<td>Razón Social</td>
						<td style="width: 15%;">Primer Nombre</td>
						<td style="width: 15%;">Segundo Nombre</td>
						<td style="width: 15%;">Primer Apellido</td>
                        <td style="width: 15%;">Segundo Apellido</td>
                        <td style="width: 10%;">Cedula / Nit</td>
					</tr>

					<?php
						$crit1="";
						if (@$_POST['codnom']!="")
                        {
                            $nombredividido = array();
                            $nombredividido = explode(" ", $_POST['codnom']);
                            
                            for ($i=0; $i < count($nombredividido); $i++) 
                            { 
                                $busqueda = '';
                                $busqueda = "AND concat_ws(' ', nombre1, nombre2, apellido1, apellido2, razonsocial, cedulanit) LIKE '%$nombredividido[$i]%' ";

                                $crit1 = $crit1 . $busqueda;
                            }
                        }

						$sqlTerceros = "SELECT id_tercero, nombre1, nombre2, apellido1, apellido2, razonsocial, cedulanit FROM terceros WHERE estado = 'S' $crit1 ORDER BY id_tercero DESC";
						$resTerceros = mysqli_query($linkbd,$sqlTerceros);
						$encontrados = mysqli_num_rows($resTerceros);

						$iter  = 'saludo1a';
						$iter2 = 'saludo2';
						$conta = 1;

						while ($rowTerceros = mysqli_fetch_row($resTerceros))
						{
                            if($rowTerceros[5] == '')
                            {
                                $nombreTercero = $rowTerceros[1].' '.$rowTerceros[2].' '.$rowTerceros[3].' '.$rowTerceros[4];
                            }
                            else
                            {
                                $nombreTercero = $rowTerceros[5];
                            }

                            $nombreTercero = Quitar_Espacios($nombreTercero);
					?>
							<tr class='<?php echo $iter ?>' style='text-align:center; text-transform:uppercase;' onclick="agregar('<?php echo $rowTerceros[0]; ?>', '<?php echo $rowTerceros[6]; ?>', '<?php echo $nombreTercero; ?>')">
								<td><?php echo $rowTerceros[5] ?></td>
								<td><?php echo $rowTerceros[1] ?></td>
								<td><?php echo $rowTerceros[2] ?></td>
								<td><?php echo $rowTerceros[3] ?></td>
                                <td><?php echo $rowTerceros[4] ?></td>
                                <td><?php echo $rowTerceros[6] ?></td>
							</tr>
					<?php
							$aux=$iter;
							$iter=$iter2;
							$iter2=$aux;
							$conta++;
						}

						if ($encontrados == 0)
						{
					?>
							<table class='inicio'>
								<tr>
									<td class='saludo1' style='text-align:center;'>
										<img src='imagenes\alert.png' style='width:25px; height: 30px;'> No se han encontrado registros <img src='imagenes\alert.png' style='width:25px; height: 30px;'>
									</td>
								</tr>
							</table>
					<?php
						}
					?>
				</table>
			</div>
		</form>
	</body>
</html>
