<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require 'comun.inc';
	require 'funciones.inc';
	session_start();
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
	titlepag();
?>

<!DOCTYPE >
<html lang=es>
	<head>
		<meta charset="utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1"/>
		<meta name="viewport" content="user-scalable=no">
		<title>:: IDEAL 10 - Presupuesto</title>
		<link href="css/css2z.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<script src="vue/vue.min.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script src="css/calendario.js?<?php echo date('d_m_Y_h_i_s');?>"></script>

        <!-- <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">-->

        <!-- <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
        <script src="./Librerias/bootstrap-5.2.0-beta1-dist/js/bootstrap.bundle.min.js"></script>
        <link rel="stylesheet" href="./Librerias/bootstrap-5.2.0-beta1-dist/css/bootstrap.min.css">
        <script type="text/javascript" src="bootstrap/fontawesome.5.11.2/js/all.js"></script>  -->


        <style>

            .titulo-form{
                background-color: #559CFC;
                height: 30px;
                margin: 1px;
                padding: 2px 4px;
                color: white;
            }

            .fondo-form{
                background-color: #F6F6F6;
                margin-bottom: 0px;
                /* margin-top: 10px; */
                /* padding-top: 4px; */
            }

            input[type=number]::-webkit-inner-spin-button,
			input[type=number]::-webkit-outer-spin-button { 
				-webkit-appearance: none; 
				margin: 0; 
			}
			input[type=number]{
				text-align:right;
				box-sizing: border-box;
				font: 120% sans-serif;
				width: 100% !important;
			}

            [v-cloak]{
                display : none;
            }
        </style>
        <script src="sweetalert2/dist/sweetalert2.min.js"></script>
		<link rel="stylesheet" href="sweetalert2/dist/sweetalert2.min.css">
        
    </head>
    <body>
        <header>
			<table>
				<tr><script>barra_imagenes("ccpet");</script><?php cuadro_titulos();?></tr>
			</table>
		</header>
        <section id="myapp" v-cloak >
            <nav>
				<table>
					<tr><?php menu_desplegable("ccpet");?></tr>
					<tr>
						<td colspan="3" class="cinta">
							<img src="imagenes/add.png" onClick="location.href='ccp-reduccion-vue.php'" class="mgbt" title="Nuevo">
							<img src="imagenes/guardad.png" title="Guardar"  class="mgbt1">
							<img src="imagenes/busca.png" onClick="location.href='ccp-buscarReduccion-vue.php'" class="mgbt" title="Buscar">
							<img src="imagenes/nv.png" onClick="mypop=window.open('ccp-principal.php','',''); mypop.focus();" class="mgbt" title="Nueva Ventana">
						</td>
					</tr>
				</table>
			</nav>
        
            <article>

                <table class="inicio ancho">
                    <tr>
                        <td class="titulos" colspan="8" >Buscar Reducci&oacute;n</td>
                        <td class="cerrar" style="width:7%" onClick="location.href='ccp-principal.php'">Cerrar</td>
                    </tr>
                    <tr>
                        <td class="tamano01" style="width:3.5cm;">N&uacute;m. Acto adm:</td>
                        <td style="width:10%;">
                            <input type="text" name="numActo" v-model = "numActo" id="numActo" placeholder="Num del acto adm">
                        </td>
                        <td class="tamano01" style="width:2.5cm;">Fecha Inicial:</td>
                        <td style="width:10%;">
                            <input type="text" name="fecha"  value="<?php echo $_POST['fecha']?>" onKeyUp="return tabular(event,this)" id="fc_1198971545" title="DD/MM/YYYY" onDblClick="displayCalendarFor('fc_1198971545');" class="colordobleclik" autocomplete="off" onChange="" readonly>
                        </td>
                        <td class="tamano01" style="width:2.5cm;" >Fecha Final:</td>
                        <td style="width:10%;">
                            <input type="text" name="fecha2" value="<?php echo $_POST['fecha2']?>" onKeyUp="return tabular(event,this)" id="fc_1198971546" title="DD/MM/YYYY" onDblClick="displayCalendarFor('fc_1198971546');" class="colordobleclik" autocomplete="off" onChange="" readonly>
                        </td>
                        <td colspan="2" style="padding-bottom:0px">
                            <em class="botonflechaverde" @click="buscarReduccion">Buscar</em>
                        </td>
                        <td></td>
                    </tr>
                </table>

                <div class='subpantalla' style='height:66vh; width:99.2%; margin-top:0px; overflow:hidden'>
                    <table class='tablamv'>
                        <thead>
                            <tr style="text-align:Center;">
                                <th class="titulosnew00" style="width:10%;">Id_acuerdo</th>
                                <th class="titulosnew00" style="width:10%;">N&uacute;m. Acto adm</th>
                                <th class="titulosnew00" style="width:30%;">Concepto acuerdo</th>
                                <th class="titulosnew00" style="width:10%;">Fecha</th>
                                <th class="titulosnew00" style="width:10%;">Valor</th>
                                <th class="titulosnew00" style="width:10%;">Estado</th>
                                <th class="titulosnew00" style="width:10%;">Finalizado</th>
                            </tr>
                        </thead>
                        <tbody>
                            
                            <tr v-show="!existeInformacion">
                                <td colspan="7">
                                    <div style="text-align: center; color:turquoise; font-size:large" class="h4 text-primary text-center">
                                        Utilice los filtros para buscar informaci&oacute;n.
                                    </div>
                                </td>
                            </tr>
                            <tr v-for="(detalle, index) in detalles" v-on:click="seleccionar(detalle)" v-bind:class="index % 2 ? 'contenidonew00' : 'contenidonew01'" style='text-rendering: optimizeLegibility; cursor: pointer !important;'>
                                <td  style="width:10%; font: 120% sans-serif; padding-left:5px; text-align:center;">{{ detalle[0] }}</td>
                                <td style="width:10%;font: 120% sans-serif; padding-left:5px; text-align:center;">{{ detalle[1] }}</td>
                                <td style="width:30%;font: 120% sans-serif; padding-left:5px;">{{ detalle[2] }}</td>
                                <td style="width:10%;font: 120% sans-serif; padding-left:5px;text-align:center;">{{ detalle[3] }}</td>
                                <td style="width:10%; font: 120% sans-serif; padding-left:5px; text-align:right;">{{ formatonumero(detalle[6]) }}</td>
                                <td style="width:10%; font: 120% sans-serif; padding-left:5px;text-align:center;">{{ detalle[9] }}</td>
                                <td style="width:10%; font: 120% sans-serif; padding-left:5px;text-align:center;">{{ comprobarEstado(detalle[9]) }}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div id="cargando" v-if="loading" class="loading">
                    <span>Cargando...</span>
                </div>
            </article>
        </section>
        <!-- <script type="module" src="./ejemplo.js"></script> -->
        <!-- <script src="Librerias/vue3/dist/vue.global.js"></script> -->
        <script type="module" src="./presupuesto_ccpet/reduccion/buscar/ccp-buscarReduccion-vue.js"></script>
		<script src="Librerias/vue/axios.min.js"></script>

    </body>
</html>