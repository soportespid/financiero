<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");

	require "comun.inc";
	require "funciones.inc";

	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");

	session_start();
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html lang="es">
	<head>
		<meta charset="utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1"/>
		<meta name="viewport" content="user-scalable=no">
		<title>:: IDEAL 10 - Informes</title>
		<link href="favicon.ico" rel="shortcut icon"/>
        <link href="css/css2.css" rel="stylesheet" type="text/css" />
        <link href="css/css3.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="css/programas.js"></script>
    </head>

	<style>

	</style>
    <body>
        <IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
        <span id="todastablas2"></span>
        <table>
            <tr><script>barra_imagenes("info");</script><?php cuadro_titulos();?></tr>
            <tr><?php menu_desplegable("info");?></tr>
        	<tr>
          		<td colspan="3" class="cinta">
				    <a class="mgbt"><img src="imagenes/add2.png" /></a>
				    <a class="mgbt"><img src="imagenes/guardad.png"/></a>
				    <a class="mgbt"><img src="imagenes/buscad.png"/></a>
                    <a onClick="mypop=window.open('plan-agenda.php','','');mypop.focus()" class="mgbt"><img src="imagenes/agenda1.png" title="Agenda"/></a>
				    <a onClick="<?php echo paginasnuevas("info");?>" class="mgbt"><img src="imagenes/nv.png" title="Nueva ventana"></a>
                    <a onClick="mypop=window.open('<?php echo $url2; ?>','','');mypop.focus();" class="tooltip bottom mgbt"><img src="imagenes/duplicar_pantalla.png" title="Duplicar pesta&ntilde;a"></a>
				    <a href="teso-gestionexogena.php" class="mgbt"><img src="imagenes/iratras.png" title="Atr&aacute;s"></a>
                </td>
        	</tr>
        </table>
 		<form name="form2" method="post" action="">
    		<table class="inicio">
      			<tr>
        			<td class="titulos" colspan="2">.: Formatos Ex&oacute;gena Tesorer&iacute;a</td>
                    <td class="cerrar" style="width:7%"><a onClick="location.href='info-principal.php'">Cerrar</a></td>
      			</tr>


				<tr>
				<td class='saludo1' width='70%'>
				<ol id="lista2">
					<li onClick="location.href='cont-columnasExogena2276Buscar.php'" style="cursor:pointer">Columnas 2276</li>
					<li onClick="location.href='teso-exogena10012276.php'" style="cursor:pointer">Formato Ex&oacute;gena 1001 - Parafiscales</li>
					<li onClick="location.href='teso-exogena1001.php'" style="cursor:pointer">Formato Ex&oacute;gena 1001</li>
					<li onClick="location.href='teso-exogena1009.php'" style="cursor:pointer">Formato Ex&oacute;gena 1009</li>
					<li onClick="location.href='teso-exogena2276.php'" style="cursor:pointer">Formato Ex&oacute;gena 2276</li>
					<li onClick="location.href='teso-exogena1476.php'" style="cursor:pointer">Formato Ex&oacute;gena Predial 1476</li>
					<li onClick="location.href='teso-exogena1481.php'" style="cursor:pointer">Formato Ex&oacute;gena Industria y Comercio 1481</li>
                    <li onClick="location.href='cont-certificadoIngresosRet220.php'" style="cursor:pointer">
                        Certificado de ingresos y retenciones por rentas de trabajo y pensiones exógena 2276
                    </li>
                    <li onClick="location.href='cont-certificadoIngresosRet220_1001.php'" style="cursor:pointer">
                        Certificado de ingresos y retenciones por rentas de trabajo y pensiones exógena 1001
                    </li>
				</ol>
				</td>  <td colspan="2" rowspan="1" style="background:url(imagenes/dian.png); background-repeat:no-repeat; background-position:center; background-size: 100% 100%"></td>
				</tr>
    		</table>
		</form>
	</body>
</html>
