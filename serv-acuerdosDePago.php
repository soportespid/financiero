<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=iso-8859-1");
	require "comun.inc";
	require "funciones.inc";
	session_start();
	$linkbd = conectar_v7();
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es">
	<head>
		<meta http-equiv="Content-type" content="text/html; charset=iso-8859-1"/>
		<meta http-equiv="X-UA-Compatible" content="IE=9"/>
		<title>:: IDEAL 10 - Servicios P&uacute;blicos</title>
		<link href="css/css2.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/cssSP.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/style.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
		<script type="text/javascript" src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
        <script type="text/javascript" src="css/calendario.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<style>
		</style>

		<script>
            function despliegamodal2(_valor, _table, cliente)
			{
                document.getElementById('bgventanamodal2').style.visibility = _valor;
                var cliente = document.getElementById('cliente').value;
                
                if (_table == 'srvclientes')
                {
                    document.getElementById('ventana2').src = 'ventana-clienteservicio.php?table=' + _table;
                }
                else if(_table == 'srvfacturas')
                {
                    if(cliente != '')
                    {
                        document.getElementById('ventana2').src = 'serv-facturasDisponiblesCliente-ventana.php?cliente=' + cliente;
                    }
                    else
                    {
                        despliegamodalm('visible','2','Falta seleccionar el cliente');
                        parent.despliegamodal2("hidden");
                    }
                }
				else if (_table == 'cuentasBancarias') {
					document.getElementById('ventana2').src="cuentasBancarias-ventana.php";
				}
            }

			function despliegamodalm(_valor,_tip,mensa,pregunta)
			{
				document.getElementById("bgventanamodalm").style.visibility=_valor;

				if(_valor=="hidden")
				{
					document.getElementById('ventanam').src="";
				}
				else
				{
					switch(_tip)
					{
						case "1":	
							document.getElementById('ventanam').src="ventana-mensaje1.php?titulos="+mensa;
						break;

						case "2":	
							document.getElementById('ventanam').src="ventana-mensaje3.php?titulos="+mensa;
						break;

						case "3":	
							document.getElementById('ventanam').src="ventana-mensaje2.php?titulos="+mensa;
						break;

						case "4":	
							document.getElementById('ventanam').src="ventana-consulta1.php?titulos="+mensa+"&idresp="+pregunta;
						break;	
					}
				}
			}

			function respuestaModalBusqueda2(tabla, id, nombre, serial, referencia)
			{
				switch(tabla)
				{
					case 'srvclientes': 
						document.getElementById('cliente').value = id;
						document.getElementById('nCliente').value = nombre;
						document.form2.submit();
					break;	
                }
            }

			function funcionmensaje()
			{
				var idban=document.getElementById('codban').value;

				document.location.href = "serv-acuerdosDePagoVisualizar.php?idban="+idban;
			}

			function respuestaconsulta(pregunta)
			{
				switch(pregunta)
				{
					case "1":	
						document.form2.oculto.value='2';
						document.form2.submit();
					break;
				}
			}

			function guardar()
			{
				var codigo = document.getElementById('codban').value;
                var cliente = document.getElementById('cliente').value;
                var factura = document.getElementById('numeroFactura').value;
				var valor = document.getElementById('valor').value;
                var tipoMovimiento = document.getElementById('tipoMovimiento').value;
				let modoRecaudo = document.getElementById("modoRecaudo").value;

				if (codigo == "" || cliente == "" || factura == "" || valor == "" || tipoMovimiento == "-1" || modoRecaudo == "-1") {
					despliegamodalm('visible','2','Falta información para crear el acuerdo.');
					return false;
				}

				var cuotas = parseInt(document.getElementById('cuotas').value);

				if (cuotas <= 0) {
					despliegamodalm('visible','2','Si el cobro va hacer financiado el numero de cuotas debe ser mayor a 0');
					return false;
				}

				if (modoRecaudo == "banco" && document.getElementById("banco").value == "") {
					despliegamodalm('visible','2','Debe seleccionar un banco');
					return false;
				}

				despliegamodalm('visible','4','Esta Seguro de Guardar','1');		
			}

			function actualizar()
			{
				document.form2.submit();
			}

			function cambiocheck2()
			{
				if(document.getElementById('financiado').value=='S')
				{
					document.getElementById('financiado').value='N';
				}
				else
				{
					document.getElementById('financiado').value='S';
				}

				document.form2.submit();
			}

            function valorcuotas(e)
            {
                var cuotas = parseInt(document.getElementById('cuotas').value);
    
                if(parseInt(cuotas) > 0)
                {
                    document.form2.oculto.value = '1';
                    document.form2.submit();
                }
                else
                {
                    document.getElementById('cuotas').value = '';
                    despliegamodalm('visible','2','No puede colocar cuotas menores o iguales a cero');
                }
            }

			function calculaValorAcuerdo()
			{
				var validacion01 = document.getElementById('abono').value;

				if(validacion01 != '' && validacion01 != 0)
				{
					document.form2.oculto.value='3';
					document.form2.submit();
				}
			}
			function validar(){document.form2.submit();}
		</script>

		<?php titlepag();?>

	</head>

	<body>
		<IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
		<span id="todastablas2"></span>
		<table>
			<tr><script>barra_imagenes("serv");</script><?php cuadro_titulos();?></tr>

			<tr><?php menu_desplegable("serv");?></tr>

			<tr>
				<td colspan="3" class="cinta">
					<a href="serv-acuerdosDePago.php" class="mgbt"><img src="imagenes/add.png"/></a>

					<a onclick="guardar()" class="mgbt"><img src="imagenes/guarda.png" title="Guardar"/></a>

					<a href="serv-acuerdosDePagoBuscar.php" class="mgbt"><img src="imagenes/busca.png" title="Buscar"/></a>

					<a onclick="mypop=window.open('plan-agenda.php','','');mypop.focus()" class="mgbt"><img src="imagenes/agenda1.png" title="Agenda" /></a>

					<a onclick="mypop=window.open('serv-principal.php','','');mypop.focus();" class="mgbt"><img src="imagenes/nv.png" title="Nueva ventana"></a>

					<a onclick="mypop=window.open('<?php echo $url2; ?>','','');mypop.focus();" class="mgbt"><img src="imagenes/duplicar_pantalla.png" title="Duplicar pesta&ntilde;a"></a>
                </td>
			</tr>
		</table>

		<div id="bgventanamodalm" class="bgventanamodalm">
			<div id="ventanamodalm" class="ventanamodalm">
				<IFRAME src="" name="ventanam" marginWidth=0 marginHeight=0 frameBorder=0 id="ventanam" frameSpacing=0 style=" width:700px; height:130px; top:200; overflow:hidden;"></IFRAME>
			</div>
		</div>

		<form name="form2" method="post" action="">
			<?php 
				if(@$_POST['oculto']=="")
				{
					$_POST['onoffswitch'] = "S";
					
					$_POST['codban'] = selconsecutivo('srvacuerdos_pago','id');

					$añoActual = date('Y');
				}

                if(@$_POST['oculto'] == '1')
                {
                    $valorFactura = $_POST['valorAcuerdo'];
                    $numeroCuotas = $_POST['cuotas'];
					$valorCuota = $valorFactura / $numeroCuotas;
					$valorCuota = ($valorCuota / 100);
					$valorCuota = round($valorCuota,2);
					$valorCuota = $valorCuota * 100;
					$_POST['valorCuota'] = $valorCuota;
                }

				if(@$_POST['oculto'] == '3')
                {
                    
					$valorFactura = $_POST['valor'];
					$abono = $_POST['abono'];

					$valorAcuerdo = $valorFactura - $abono;

					$_POST['valorAcuerdo'] = round($valorAcuerdo,2);
                }
			?>

			<table class="inicio ancho">
				<tr>
					<td class="titulos" colspan="10">.: Crear Acuerdo de Pago para Factura</td>

					<td class="cerrar" style="width:7%" onClick="location.href='serv-principal.php'">Cerrar</td>
				</tr>

				<tr>
					<td class="tamano01" style="width:3cm;">C&oacute;digo:</td>

					<td style="width:15%;">
                        <input type="text" name="codban" id="codban" onKeyUp="return tabular(event,this)" value="<?php echo @ $_POST['codban'];?>" style="width:100%;height:30px;text-align:center;" readonly/>
                    </td>
				
                    <td class="saludo1">Cliente:</td>

					<td>
						<input type="text" name='cliente' id='cliente'  value="<?php echo @$_POST['cliente']?>" style="text-align:center; width:100%;" onclick = "despliegamodal2('visible', 'srvclientes');"  class="colordobleclik" readonly>
                    </td>

                    <td>
						<input type="text" name="nCliente" id="nCliente" value="<?php echo @$_POST['nCliente']?>" style="width:100%;height:30px;" readonly>
					</td>
				</tr>

				<tr>
                    <td class="saludo1">N&deg; Factura:</td>

					<td>
						<input type="text" name='numeroFactura' id='numeroFactura' value="<?php echo @$_POST['numeroFactura']?>" style="width: 100%; height: 30px; text-align:center;" onclick = "despliegamodal2('visible', 'srvfacturas');"  class="colordobleclik" readonly>
                    </td>
                    
					<td class="saludo1" style="width:3cm;">Fecha:</td>
                    
					<td style="width:15%;">
                        <input type="text" name="fecha" value="<?php echo @ $_POST['fecha']?>" maxlength="10" onKeyPress="javascript:return solonumeros(event)" onKeyUp="return tabular(event,this)" id="fc_1198971545" onKeyDown="mascara(this,'/',patron,true)" title="DD/MM/YYYY" style="width:80%; text-align:center;" onchange="">&nbsp;
                        <img src="imagenes/calendario04.png" onClick="displayCalendarFor('fc_1198971545');" title="Calendario" class="icobut"/>
                    </td>
				</tr>

				<tr>
					<td class="saludo1">Valor de la factura:</td>

					<td>
						<input type="text" name="valor" id="valor" value="<?php echo @$_POST['valor']?>" style="height:30px; width: 197px; text-align:center;" readonly>
					</td>

                    <td class="saludo1" style="width: 68px; height: 30px">Valor de Abono: </td>
                    <td>
                        <input type="text" name="abono" id="abono" value="<?php echo $_POST['abono'] ?>" style="text-align:center; height: 30px;" onblur="calculaValorAcuerdo();">
                    </td>
                </tr>

				<tr>
					<td class="saludo1" style="width: 68px; height: 30px">Valor Acuerdo: </td>
                    <td>
                        <input type="text" name="valorAcuerdo" id="valorAcuerdo" value="<?php echo $_POST['valorAcuerdo'] ?>" style="text-align:center; height: 30px;" readonly>
                    </td>

					<td class="tamano01">Tipo de Movimiento:</td>
					<td>
                        <select  name="tipoMovimiento" id="tipoMovimiento" style="width:100%;height:30px;">
							<option class='aumentarTamaño' value="-1">:: SELECCIONE MOVIMIENTO ::</option>
							<?php
								$sqlr = "SELECT * FROM srvtipo_movimiento WHERE codigo = '02' AND tipo_movimiento = '2' ";
								$resp = mysqli_query($linkbd,$sqlr);
								while ($row = mysqli_fetch_row($resp))
								{
									if(@ $_POST['tipoMovimiento'] == $row[0])
									{
										echo "<option class='aumentarTamaño' value='$row[1]$row[0]' SELECTED>$row[1]$row[0] - $row[2]</option>";
									}
									else{echo "<option class='aumentarTamaño' value='$row[1]$row[0]'>$row[1]$row[0] - $row[2]</option>";}
								}
							?>
						</select>
                    </td>
				</tr>

                <tr>
					<td class="saludo1" style="width: 68px; height: 30px">Numero de Cuotas</td>
                    <td>
                        <input type="text" name="cuotas" id="cuotas" value="<?php echo $_POST['cuotas'] ?>" maxlength="2" style="text-align:center; height: 30px;" onKeyPress="javascript:return solonumeros(event)" onKeyUp="return tabular(event,this)" onblur="valorcuotas(event);">
                    </td>

                    <td class="saludo1">Valor de la cuota:</td>

                    <td>
                        <input type="text" name="valorCuota" id="valorCuota" value="<?php echo @$_POST['valorCuota']?>" style="height:30px; width: 197px; text-align:center;" readonly>
                    </td>
                </tr>

				<tr>
					<td class="tamano01">Recaudo en: </td>

					<td>
						<select  name="modoRecaudo" id="modoRecaudo" style="width:100%;height:30px;" onChange="validar()" class="centrarSelect">
							<option class="aumentarTamaño" value="-1">:: SELECCIONE TIPO DE RECAUDO ::</option>
							<option class="aumentarTamaño" value="caja" <?php if(@$_POST['modoRecaudo']=='caja') echo "SELECTED"; ?>>Caja</option>
							<option class="aumentarTamaño" value="banco" <?php if(@$_POST['modoRecaudo']=='banco') echo "SELECTED"; ?>>Banco</option>
						</select>
					</td>

					<?php
						if (@$_POST['modoRecaudo'] == 'banco')
						{
					?>
							<td class="saludo1">Cuenta Banco: </td> 

							<td>
								<input type="text" id="banco" name="banco" style="width:88%" onKeyPress="javascript:return solonumeros(event)" onKeyUp="return tabular(event,this)" onBlur="" value="<?php echo $_POST['banco']?>" onClick="document.getElementById('banco').focus();document.getElementById('banco').select();">

								<input type="hidden" name="cuentaBancaria" id="cuentaBancaria" value="<?php echo $_POST['cuentaBancaria'] ?>">

							<a title="Bancos" onClick="despliegamodal2('visible', 'cuentasBancarias');" style="cursor:pointer;"><img src="imagenes/find02.png" style="width:20px;"/></a>
							</td>

							<td colspan="2">
								<input type="text" name="nbanco" id="nbanco" style="width:100%;" value="<?php echo $_POST['nbanco']?>"  readonly>
							</td>	
					<?php
						}
					?>
				</tr>
			</table>

			<input type="hidden" name="oculto" id="oculto" value="1"/>
            <input type="hidden" name="corte" id="corte" value="<?php echo $_POST['corte']?>"/>

			<?php 
				if(@$_POST['oculto'] == "2")
				{
                    preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST['fecha'],$fecha);
					$fechaAcuerdo="$fecha[3]-$fecha[2]-$fecha[1]";

					if ($_POST['modoRecaudo'] == 'caja') {

						$sqlCuentaCaja = "SELECT cuentacaja FROM tesoparametros";
						$resCuentaCaja = mysqli_query($linkbd,$sqlCuentaCaja);
						$rowCuentaCaja = mysqli_fetch_row($resCuentaCaja);

						$cuentaCaja = $rowCuentaCaja[0];
					}
					else if ($_POST['modoRecaudo'] == 'banco') {

						$cuentaCaja = $_POST['cuentaBancaria'];
					}
					
					//Cuenta puente para extinción
					$concepto2 = concepto_cuentasn2('01','AP',10,'02',$fechaAcuerdo);
					$cuentaPuente = $concepto2[0][0];

					//validar si ya tiene un acuerdo vigente este cliente
					$sqlvalidar = "SELECT id FROM srvacuerdos_pago WHERE id_cliente = '$_POST[cliente]' AND estado = 'S' ";
					$resvalidar = mysqli_query($linkbd,$sqlvalidar);
					$rowvalidar = mysqli_fetch_row($resvalidar);

					if(!isset($rowvalidar[0]))
					{
						$sqlCorteDetalles = "UPDATE srvcortes_detalle SET estado_pago = 'A' WHERE id_corte = '$_POST[corte]' AND numero_facturacion = '$_POST[numeroFactura]' ";
						mysqli_query($linkbd,$sqlCorteDetalles);
						
						//Extinción de deuda
						$sql = "SELECT credito FROM srvdetalles_facturacion WHERE numero_facturacion = '$_POST[numeroFactura]' AND id_tipo_cobro = 8 AND tipo_movimiento = '101'";
						$res = mysqli_query($linkbd,$sql);
						$row = mysqli_fetch_row($res);

						if($row[0] > 0)//validar cuando la factura tenga saldo
						{
							$sql = "SELECT * FROM srvcortes_detalle WHERE estado_pago = 'P' AND id_cliente = '$_POST[cliente]' ORDER BY id DESC LIMIT 1"; //buscamos el ultimo corte que se pago
							$res = mysqli_query($linkbd,$sql);
							$row = mysqli_fetch_row($res);

							if($row[0] != '')//si el cliente ya tiene pagos solo tomara las facturas que tenga vencida y aún no ha pagado
							{
								$sql = "SELECT * FROM srvcortes_detalle WHERE estado_pago = 'V' AND id_cliente = '$_POST[cliente]' AND id > $row[0] ";//buscamos los cortes vencidos luego del que se pago
								$res = mysqli_query($linkbd,$sql);
								while($row = mysqli_fetch_row($res))
								{
									$sqlDetalles = "SELECT id_tipo_cobro, credito, debito FROM srvdetalles_facturacion WHERE numero_facturacion = '$row[3]' ";
									$resDetalles = mysqli_query($linkbd,$sqlDetalles);
									while($rowDetalles = mysqli_fetch_row($resDetalles))
									{
										switch($rowDetalles[0]) 
										{
											case 1:
												$saldoCargoFijo = $saldoCargoFijo + $rowDetalles[1];
											break;
											
											case 2:
												$saldoConsumoBasico = $saldoConsumoBasico + $rowDetalles[1];
											break;
		
											case 3:
												$saldoConsumoConLectura = $saldoConsumoConLectura + $rowDetalles[1];
											break;
		
											case 4:
												$saldoOtroCobro = $saldoOtroCobro + $rowDetalles[1];
											break;

											case 5:
												$saldoSubsidio = $saldoSubsidio + $rowDetalles[2];
												break;
		
											case 7:
												$saldoContribucion = $saldoContribucion + $rowDetalles[1];
											break;
										}
									}
								}
							}
							else //Si el cliente no ha tenido ningun pago el programa tomara todas las facturas vencidas
							{
								$sql = "SELECT * FROM srvcortes_detalle WHERE estado_pago = 'V' AND id_cliente = '$_POST[cliente]'";//buscamos los cortes vencidos luego del que se pago
								$res = mysqli_query($linkbd,$sql);
								while($row = mysqli_fetch_row($res))
								{
									$sqlDetalles = "SELECT id_tipo_cobro, credito, debito FROM srvdetalles_facturacion WHERE numero_facturacion = '$row[3]' ";
									$resDetalles = mysqli_query($linkbd,$sqlDetalles);
									while($rowDetalles = mysqli_fetch_row($resDetalles))
									{
										switch($rowDetalles[0]) 
										{
											case 1:
												$saldoCargoFijo = $saldoCargoFijo + $rowDetalles[1];
											break;
											
											case 2:
												$saldoConsumoBasico = $saldoConsumoBasico + $rowDetalles[1];
											break;
		
											case 3:
												$saldoConsumoConLectura = $saldoConsumoConLectura + $rowDetalles[1];
											break;
		
											case 4:
												$saldoOtroCobro = $saldoOtroCobro + $rowDetalles[1];
											break;

											case 5:
												$saldoSubsidio = $saldoSubsidio + $rowDetalles[2];
												break;
		
											case 7:
												$saldoContribucion = $saldoContribucion + $rowDetalles[1];
											break;
										}
									}
								}
							}
						}

						$sqlConsultaTipoCobro = "SELECT id_servicio, id_tipo_cobro, credito, debito FROM srvdetalles_facturacion WHERE corte = $_POST[corte] AND numero_facturacion = $_POST[numeroFactura] ORDER BY id_tipo_cobro";
						$resConsultaTipoCobro = mysqli_query($linkbd,$sqlConsultaTipoCobro);

						$codigoComprobante = '39';

						while($rowConsultaTipoCobro = mysqli_fetch_row($resConsultaTipoCobro))
						{
							switch($rowConsultaTipoCobro[1]) 
							{
								case 1: //comprobante det cargo fijo
									//echo "hola";	
									$cargoFijo = $rowConsultaTipoCobro[2] + $saldoCargoFijo;
									
									$sqlServicio = "SELECT concepto,cc FROM srvservicios WHERE id = '$rowConsultaTipoCobro[0]' ";
									$resServicio = mysqli_query($linkbd,$sqlServicio);
									$rowServicio = mysqli_fetch_row($resServicio);

									$sqlCliente = "SELECT id_tercero FROM srvclientes WHERE id = '$_POST[cliente]' ";
									$resCliente = mysqli_query($linkbd,$sqlCliente);
									$rowCliente = mysqli_fetch_row($resCliente);

									$sqlTerceros = "SELECT cedulanit FROM terceros WHERE id_tercero = '$rowCliente[0]' ";
									$resTerceros = mysqli_query($linkbd,$sqlTerceros);
									$rowTerceros = mysqli_fetch_row($resTerceros);

									$concepto = concepto_cuentasn2($rowServicio[0],'SS',10,$rowServicio[1],$fechaAcuerdo);
									
									//credito la cuenta credito va contra la caja o banco depende que elija la persona
									$sqlr = "INSERT INTO comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque,valdebito, valcredito,estado,vigencia) VALUES ('$codigoComprobante $_POST[codban]', '$cuentaPuente','$rowTerceros[0]','$rowServicio[1]','Acuerdo de Pago de la factura $_POST[numeroFactura] del corte $_POST[corte] Cargo Fijo', '', '$cargoFijo', 0, '1', $fecha[3])";
									mysqli_query($linkbd,$sqlr);
									
									//debito va con la cuenta que trae de la parametrización
									$sqlr = "INSERT INTO comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque,valdebito, valcredito,estado,vigencia) VALUES ('$codigoComprobante $_POST[codban]','".$concepto[0][0]."','$rowTerceros[0]','$rowServicio[1]','Acuerdo de Pago de la factura $_POST[numeroFactura] del corte $_POST[corte] Cargo Fijo', '', 0, '$cargoFijo', '1' ,'$fecha[3]')";
									mysqli_query($linkbd,$sqlr);
								break;
								
								case 2: //comprobante det consumo basico
									$consumoBasico = $rowConsultaTipoCobro[2] + $saldoConsumoBasico;
									$sqlCliente = "SELECT id_tercero, id_estrato FROM srvclientes WHERE id = '$_POST[cliente]' ";
									$resCliente = mysqli_query($linkbd,$sqlCliente);
									$rowCliente = mysqli_fetch_row($resCliente);

									$sqlTerceros = "SELECT cedulanit FROM terceros WHERE id_tercero = '$rowCliente[0]' ";
									$resTerceros = mysqli_query($linkbd,$sqlTerceros);
									$rowTerceros = mysqli_fetch_row($resTerceros);

									$vigencia = date('Y');

									$sqlServicio = "SELECT concepto_contable,centro_costo FROM srvcostos_estandar WHERE id_servicio = '$rowConsultaTipoCobro[0]' AND id_estrato = '$rowCliente[1]'";
									$resServicio = mysqli_query($linkbd,$sqlServicio);
									$rowServicio = mysqli_fetch_row($resServicio);

									$concepto = concepto_cuentasn2($rowServicio[0],'CB',10,$rowServicio[1],$fechaAcuerdo);

									$sqlr="INSERT INTO comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque,valdebito, valcredito,estado,vigencia) VALUES ('$codigoComprobante $_POST[codban]', '$cuentaPuente','$rowTerceros[0]','$rowServicio[1]','Acuerdo de Pago de la factura $_POST[numeroFactura] del corte $_POST[corte] Consumo Basico', '', '$consumoBasico', 0, '1', $fecha[3])";
									mysqli_query($linkbd,$sqlr);	
								
									$sqlr="INSERT INTO comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque,valdebito, valcredito,estado,vigencia) VALUES ('$codigoComprobante $_POST[codban]','".$concepto[0][0]."','$rowTerceros[0]','$rowServicio[1]','Acuerdo de Pago de la factura $_POST[numeroFactura] del corte $_POST[corte] Consumo Basico', '', 0, '$consumoBasico', '1' ,'$fecha[3]')";
									mysqli_query($linkbd,$sqlr);
								break;

								case 3://comprobante det consumo con lectura
									$consumoConLectura = $rowConsultaTipoCobro[2] + $saldoConsumoConLectura;
									
									$sqlCliente = "SELECT id_tercero, id_estrato FROM srvclientes WHERE id = '$_POST[cliente]' ";
									$resCliente = mysqli_query($linkbd,$sqlCliente);
									$rowCliente = mysqli_fetch_row($resCliente);

									$sqlTerceros = "SELECT cedulanit FROM terceros WHERE id_tercero = '$rowCliente[0]' ";
									$resTerceros = mysqli_query($linkbd,$sqlTerceros);
									$rowTerceros = mysqli_fetch_row($resTerceros);

									$sqlServicio = "SELECT concepto_contable,centro_costo FROM srvtarifas WHERE id_servicio = '$rowConsultaTipoCobro[0]' AND id_estrato = '$rowCliente[1]' ";
									$resServicio = mysqli_query($linkbd,$sqlServicio);
									$rowServicio = mysqli_fetch_row($resServicio);

									$concepto = concepto_cuentasn2($rowServicio[0],'CL',10,$rowServicio[1],$fechaAcuerdo);
										
									$sqlr="INSERT INTO comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque,valdebito, valcredito,estado,vigencia) VALUES ('$codigoComprobante $_POST[codban]', '$cuentaPuente','$rowTerceros[0]','$rowServicio[1]','Acuerdo de Pago de la factura $_POST[numeroFactura] del corte $_POST[corte] Consumo con Lectura', '', '$consumoConLectura', 0, '1', $fecha[3])";
									mysqli_query($linkbd,$sqlr);	
								
									$sqlr="INSERT INTO comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque,valdebito, valcredito,estado,vigencia) VALUES ('$codigoComprobante $_POST[codban]','".$concepto[0][0]."','$rowTerceros[0]','$rowServicio[1]','Acuerdo de Pago de la factura $_POST[numeroFactura] del corte $_POST[corte] Consumo con Lectura', '', 0, '$consumoConLectura', '1' ,'$fecha[3]')";
									mysqli_query($linkbd,$sqlr);
								break;

								case 4://comprobante det otros cobros
									$otroCobro = $rowConsultaTipoCobro[2] + $saldoOtroCobro;

									$sqlAsignacionOtroCobro = "SELECT id_otro_cobro FROM srvasignacion_otroscobros WHERE id_cliente = '$_POST[cliente]' ";
									$resAsignacionOtroCobro = mysqli_query($linkbd,$sqlAsignacionOtroCobro);
									$rowAsignacionOtroCobro = mysqli_fetch_row($resAsignacionOtroCobro);

									$sqlOtroCobro = "SELECT concepto FROM srvotroscobros WHERE id = '$rowAsignacionOtroCobro[0]' ";
									$resOtroCobro = mysqli_query($linkbd,$sqlOtroCobro);
									$rowOtroCobro = mysqli_fetch_row($resOtroCobro);

									$sqlServicio = "SELECT cc FROM srvservicios WHERE id = '$rowConsultaTipoCobro[0]' ";
									$resServicio = mysqli_query($linkbd,$sqlServicio);
									$rowServicio = mysqli_fetch_row($resServicio);

									$sqlCliente = "SELECT id_tercero FROM srvclientes WHERE id = '$_POST[cliente]' ";
									$resCliente = mysqli_query($linkbd,$sqlCliente);
									$rowCliente = mysqli_fetch_row($resCliente);

									$sqlTerceros = "SELECT cedulanit FROM terceros WHERE id_tercero = '$rowCliente[0]' ";
									$resTerceros = mysqli_query($linkbd,$sqlTerceros);
									$rowTerceros = mysqli_fetch_row($resTerceros);

									$concepto = concepto_cuentasn2($rowOtroCobro[0],'SO',10,$rowServicio[0],$fechaAcuerdo);
									
									//credito la cuenta credito va contra la caja o banco depende que elija la persona
									$sqlr="INSERT INTO comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque,valdebito, valcredito,estado,vigencia) VALUES ('$codigoComprobante $_POST[codban]', '$cuentaPuente','$rowTerceros[0]','$rowServicio[0]','Acuerdo de Pago de la factura $_POST[numeroFactura] del corte $_POST[corte] Otro Cobro', '', '$otroCobro', 0, '1', $fecha[3])";
									mysqli_query($linkbd,$sqlr);
								
									//debito va con la cuenta que trae de la parametrización
									$sqlr="INSERT INTO comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque,valdebito, valcredito,estado,vigencia) VALUES ('$codigoComprobante $_POST[codban]','".$concepto[0][0]."','$rowTerceros[0]','$rowServicio[0]','Acuerdo de Pago $_POST[numeroFactura] del corte $_POST[corte] Otro Cobro', '', 0, '$otroCobro', '1' ,'$fecha[3]')";			
									mysqli_query($linkbd,$sqlr);																	
								break;

								case 5://comprobante det subsidio

									$añoActual = date('Y');
									$subsidio = $rowConsultaTipoCobro[3] + $saldoSubsidio;

                                    $sqlCliente = "SELECT id_tercero,id_estrato FROM srvclientes WHERE id = '$_POST[cliente]' ";
									$resCliente = mysqli_query($linkbd,$sqlCliente);
									$rowCliente = mysqli_fetch_row($resCliente);

									$sqlTerceros = "SELECT cedulanit FROM terceros WHERE id_tercero = '$rowCliente[0]' ";
									$resTerceros = mysqli_query($linkbd,$sqlTerceros);
									$rowTerceros = mysqli_fetch_row($resTerceros);

									$sqlServicio = "SELECT cc FROM srvservicios WHERE id = '$rowConsultaTipoCobro[0]' ";
									$resServicio = mysqli_query($linkbd,$sqlServicio);
									$rowServicio = mysqli_fetch_row($resServicio);
									
									$sqlSubsidio = "SELECT concepto FROM srvsubsidios WHERE id_estrato = '$rowCliente[1]' AND vigencia = '$añoActual'";
									$resSubsidio = mysqli_query($linkbd,$sqlSubsidio);
									$rowSubsidio = mysqli_fetch_row($resSubsidio);

									$concepto = concepto_cuentasn2($rowSubsidio[0],'SB',10,$rowServicio[0],$fechaAcuerdo);
									
									$sqlr="INSERT INTO comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque,valdebito,valcredito,estado,vigencia) VALUES ('$codigoComprobante $_POST[codban]', '$cuentaPuente','$rowTerceros[0]','$rowServicio[0]','Acuerdo de Pago de la factura $_POST[numeroFactura] del corte $_POST[corte] Subsidio', '', 0, '$subsidio', '1', $fecha[3])";
									mysqli_query($linkbd,$sqlr);
                                                
									//aqui va le cuenta 13
									$sqlr="INSERT INTO comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque,valdebito,valcredito,estado,vigencia) VALUES ('$codigoComprobante $_POST[codban]', '".$concepto[1][0]."','$rowTerceros[0]','$rowServicio[0]','Acuerdo de Pago $_POST[numeroFactura] del corte $_POST[corte] Subsidio', '', '$subsidio', 0, '1', $fecha[3])";
									mysqli_query($linkbd,$sqlr);				
								break;

								case 7: //comprobante det contribución
									$contribucion = $rowConsultaTipoCobro[2] + $saldoContribucion;

									$sqlCliente = "SELECT id_tercero,id_estrato FROM srvclientes WHERE id = '$_POST[cliente]' ";
									$resCliente = mysqli_query($linkbd,$sqlCliente);
									$rowCliente = mysqli_fetch_row($resCliente);

									$sqlTerceros = "SELECT cedulanit FROM terceros WHERE id_tercero = '$rowCliente[0]' ";
									$resTerceros = mysqli_query($linkbd,$sqlTerceros);
									$rowTerceros = mysqli_fetch_row($resTerceros);

									$sqlServicio = "SELECT cc FROM srvservicios WHERE id = '$rowConsultaTipoCobro[0]' ";
									$resServicio = mysqli_query($linkbd,$sqlServicio);
									$rowServicio = mysqli_fetch_row($resServicio);

									$sqlContribuciones = "SELECT concepto FROM srvcontribuciones WHERE id_estrato = '$rowCliente[1]' ";
									$resContribuciones = mysqli_query($linkbd,$sqlContribuciones);
									$rowContribuciones = mysqli_fetch_row($resContribuciones);

									$concepto = concepto_cuentasn2($rowContribuciones[0],'SC',10,$rowServicio[0],$fechaAcuerdo);

									//credito la cuenta credito va contra la caja o banco depende que elija la persona
									$sqlr="INSERT INTO comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque,valdebito, valcredito,estado,vigencia) VALUES ('$codigoComprobante $_POST[codban]', '$cuentaPuente','$rowTerceros[0]','$rowServicio[0]','Acuerdo de Pago de la factura $_POST[numeroFactura] del corte $_POST[corte] Contribucion', '', '$contribucion', 0, '1', $fecha[3])";
									mysqli_query($linkbd,$sqlr);
								
									//debito va con la cuenta que trae de la parametrización
									$sqlr="INSERT INTO comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque,valdebito, valcredito,estado,vigencia) VALUES ('$codigoComprobante $_POST[codban]','".$concepto[0][0]."','$rowTerceros[0]','$rowServicio[0]','Acuerdo de Pago de la factura $_POST[numeroFactura] del corte $_POST[corte] Contribucion', '', 0, '$contribucion', '1' ,'$fecha[3]')";
									mysqli_query($linkbd,$sqlr);									
								break; 

								case 10:
									$cargoFijo = $rowConsultaTipoCobro[2] + $saldoCargoFijo;
									
									$sqlServicio = "SELECT concepto,cc FROM srvservicios WHERE id = '$rowConsultaTipoCobro[0]' ";
									$resServicio = mysqli_query($linkbd,$sqlServicio);
									$rowServicio = mysqli_fetch_row($resServicio);

									$sqlCliente = "SELECT id_tercero FROM srvclientes WHERE id = '$_POST[cliente]' ";
									$resCliente = mysqli_query($linkbd,$sqlCliente);
									$rowCliente = mysqli_fetch_row($resCliente);

									$sqlTerceros = "SELECT cedulanit FROM terceros WHERE id_tercero = '$rowCliente[0]' ";
									$resTerceros = mysqli_query($linkbd,$sqlTerceros);
									$rowTerceros = mysqli_fetch_row($resTerceros);

									$concepto = concepto_cuentasn2($rowServicio[0],'SS',10,$rowServicio[1],$fechaAcuerdo);
									
									//credito la cuenta credito va contra la caja o banco depende que elija la persona
									$sqlr = "INSERT INTO comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque,valdebito, valcredito,estado,vigencia) VALUES ('$codigoComprobante $_POST[codban]', '$cuentaPuente','$rowTerceros[0]','$rowServicio[1]','Acuerdo de Pago de la factura $_POST[numeroFactura] del corte $_POST[corte] Saldo Inicial', '', '$cargoFijo', 0, '1', $fecha[3])";
									mysqli_query($linkbd,$sqlr);
									
									//debito va con la cuenta que trae de la parametrización
									$sqlr = "INSERT INTO comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque,valdebito, valcredito,estado,vigencia) VALUES ('$codigoComprobante $_POST[codban]','".$concepto[0][0]."','$rowTerceros[0]','$rowServicio[1]','Acuerdo de Pago de la factura $_POST[numeroFactura] del corte $_POST[corte] Saldo Inicial', '', 0, '$cargoFijo', '1' ,'$fecha[3]')";
									mysqli_query($linkbd,$sqlr);

								default:
									
								break;
							}

							if($rowConsultaTipoCobro[1] != 1 && $rowConsultaTipoCobro[1] != 2 && $rowConsultaTipoCobro[1] != 3 && $rowConsultaTipoCobro[1] != 4 && $rowConsultaTipoCobro[1] != 7 && $rowConsultaTipoCobro[1] != 8 && $rowConsultaTipoCobro != 10 && $saldoOtroCobro > 0)//Otros cobros no es un pago que aparece mensualmente entonces en dado caso que la persona no haya pagado un pago de otro cobro y lo este pagando este mes con este if comprueba que no exista cobro de este mes con 7 y que si haya un saldo de otro cobro
							{
								$otroCobro = $saldoOtroCobro;
								$sqlAsignacionOtroCobro = "SELECT id_otro_cobro FROM srvasignacion_otroscobros WHERE id_cliente = '$_POST[cliente]' ";
								$resAsignacionOtroCobro = mysqli_query($linkbd,$sqlAsignacionOtroCobro);
								$rowAsignacionOtroCobro = mysqli_fetch_row($resAsignacionOtroCobro);

								$sqlOtroCobro = "SELECT concepto FROM srvotroscobros WHERE id = '$rowAsignacionOtroCobro[0]' ";
								$resOtroCobro = mysqli_query($linkbd,$sqlOtroCobro);
								$rowOtroCobro = mysqli_fetch_row($resOtroCobro);

								$sqlServicio = "SELECT cc FROM srvservicios WHERE id = '$rowConsultaTipoCobro[0]' ";
								$resServicio = mysqli_query($linkbd,$sqlServicio);
								$rowServicio = mysqli_fetch_row($resServicio);

								$sqlCliente = "SELECT id_tercero FROM srvclientes WHERE id = '$_POST[cliente]' ";
								$resCliente = mysqli_query($linkbd,$sqlCliente);
								$rowCliente = mysqli_fetch_row($resCliente);

								$sqlTerceros = "SELECT cedulanit FROM terceros WHERE id_tercero = '$rowCliente[0]' ";
								$resTerceros = mysqli_query($linkbd,$sqlTerceros);
								$rowTerceros = mysqli_fetch_row($resTerceros);

								$concepto = concepto_cuentasn2($rowOtroCobro[0],'SO',10,$rowServicio[0],$fechaAcuerdo);

								//credito la cuenta credito va contra la caja o banco depende que elija la persona	
								$sqlr="INSERT INTO comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque,valdebito, valcredito,estado,vigencia) VALUES ('$codigoComprobante $_POST[codban]', '$cuentaPuente','$rowTerceros[0]','$rowServicio[0]','Acuerdo de Pago de la factura $_POST[numeroFactura] del corte $_POST[corte] Otro Cobro', '', '$otroCobro', 0, '1', $fecha[3])";
								mysqli_query($linkbd,$sqlr);
							
								//debito va con la cuenta que trae de la parametrización
								$sqlr="INSERT INTO comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque,valdebito, valcredito,estado,vigencia) VALUES ('$codigoComprobante $_POST[codban]','".$concepto[0][0]."','$rowTerceros[0]','$rowServicio[0]','Acuerdo de Pago de la factura $_POST[numeroFactura] del corte $_POST[corte] Otro Cobro', '', 0, '$otroCobro', '1' ,'$fecha[3]')";
								mysqli_query($linkbd,$sqlr);						
							}
						}

						$sqlr="INSERT INTO comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque,valdebito, valcredito,estado,vigencia) VALUES ('$codigoComprobante $_POST[codban]', '$cuentaPuente','$rowTerceros[0]','$rowServicio[1]','Acuerdo de Pago de la factura $_POST[numeroFactura] del corte $_POST[corte] Consumo con Lectura', '', '$consumoConLectura', 0, '1', $fecha[3])";
						mysqli_query($linkbd,$sqlr);
						
						$totalCabecera = $cargoFijo + $consumoBasico + $consumoConLectura + $otroCobro + $contribucion;

						$sqlComprobanteCabecera = "INSERT INTO comprobante_cab (numerotipo, tipo_comp, fecha, concepto, total_debito, total_credito, estado) VALUES ('$_POST[codban]', '$codigoComprobante', '$fechaAcuerdo', 'Acuerdo de Pago de la factura $_POST[numeroFactura] del corte $_POST[corte]', $totalCabecera, $totalCabecera, '1') ";
						mysqli_query($linkbd,$sqlComprobanteCabecera);

						$sqlDetallesFacturacion = "SELECT id_servicio, id_tipo_cobro, credito, debito FROM srvdetalles_facturacion WHERE numero_facturacion = $_POST[numeroFactura] ORDER BY id_tipo_cobro";
						$resDetallesFacturacion = mysqli_query($linkbd,$sqlDetallesFacturacion);
						while($rowDetallesFacturacion = mysqli_fetch_row($resDetallesFacturacion))
						{
							if($rowDetallesFacturacion[2] > 0)
							{
								$sqlCredito = "INSERT INTO srvdetalles_facturacion (corte, id_cliente, numero_facturacion, tipo_movimiento, fecha_movimiento, id_servicio, id_tipo_cobro, credito, debito, saldo, estado) VALUES ('$_POST[corte]', '$_POST[cliente]', '$_POST[numeroFactura]', '$_POST[tipoMovimiento]', '$fechaAcuerdo', $rowDetallesFacturacion[0], $rowDetallesFacturacion[1], 0, $rowDetallesFacturacion[2], 0, 'S')";
								mysqli_query($linkbd,$sqlCredito);
							}
							elseif($rowDetallesFacturacion[3] > 0)
							{
								$sqlDebito = "INSERT INTO srvdetalles_facturacion (corte, id_cliente, numero_facturacion, tipo_movimiento, fecha_movimiento, id_servicio, id_tipo_cobro, credito, debito, saldo, estado) VALUES ('$_POST[corte]', '$_POST[cliente]', '$_POST[numeroFactura]', '$_POST[tipoMovimiento]', '$fechaAcuerdo', $rowDetallesFacturacion[0], $rowDetallesFacturacion[1], $rowDetallesFacturacion[3], 0, 0, 'S')";
								mysqli_query($linkbd,$sqlDebito);
							}
						}

						if($_POST['abono'] > 0)
						{
							$sqlr="INSERT INTO comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque,valdebito, valcredito,estado,vigencia) VALUES ('$codigoComprobante $_POST[codban]', '$cuentaCaja','$rowTerceros[0]','$rowServicio[1]','Abono de Acuerdo de Pago de la factura $_POST[numeroFactura] del corte $_POST[corte]', '', '$_POST[abono]', 0, '1', $fecha[3])";
							mysqli_query($linkbd,$sqlr);

							$sqlr="INSERT INTO comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque,valdebito, valcredito,estado,vigencia) VALUES ('$codigoComprobante $_POST[codban]', '$cuentaPuente','$rowTerceros[0]','$rowServicio[1]','Abono de Acuerdo de Pago de la factura $_POST[numeroFactura] del corte $_POST[corte]', '', 0, '$_POST[abono]', '1', $fecha[3])";
							mysqli_query($linkbd,$sqlr);
						}

						$sqlAcuerdo = "INSERT INTO srvacuerdos_pago (id_cliente, numero_factura, fecha, cuotas_inicial, valor_inicial, estado, tipo_movimiento, valor_abono, valor_acuerdo, medio_pago, cuenta) VALUES ('$_POST[cliente]', '$_POST[numeroFactura]', '$fechaAcuerdo', '$_POST[cuotas]', '$_POST[valor]', 'S', '202', '$_POST[abono]', '$_POST[valorAcuerdo]', '$_POST[modoRecaudo]', '$cuentaCaja')";
						if(!mysqli_query($linkbd,$sqlAcuerdo))
						{
							echo"<script>despliegamodalm('visible','2','No se pudo ejecutar la peticion');</script>";
						}
						else 
						{
							echo "<script>despliegamodalm('visible','1','Se ha almacenado con Exito');</script>";
						}
					}
					else
					{
						echo "<script>despliegamodalm('visible','2','El Cliente ya tiene un acuerdo de pago en vigencia.');</script>";
					}
						
				}
			?>
		</form>
		<div id="bgventanamodal2">
			<div id="ventanamodal2">
				<IFRAME  src="" name="buster" marginWidth=0 marginHeight=0 frameBorder=0 id="ventana2" frameSpacing=0 style="left:500px; width:900px; height:500px; top:200;"></IFRAME>
			</div>
		</div>
	</body>
</html>
