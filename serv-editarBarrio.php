<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require 'comun.inc';
	require 'funciones.inc';
	session_start();
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
	titlepag();
?>
<!DOCTYPE >
<html lang=es>
	<head>
		<meta charset="utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1"/>
		<meta name="viewport" content="user-scalable=no">
		<title>:: IDEAL 10 - Servicios Públicos</title>
		<link href="css/css2z.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/tabs.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
		<link href="css/style.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
		<script src="vue/vue.min.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script src="css/calendario.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<style>
			input[type=number]::-webkit-inner-spin-button,
			input[type=number]::-webkit-outer-spin-button { 
				-webkit-appearance: none; 
				margin: 0; 
			}
			input[type=number]{
				text-align:right;
				box-sizing: border-box;
				font: 120% sans-serif;
				width: 100% !important;
			}
			[v-cloak]{display : none;}

			.centrarSelect {

				height: 30px !important;
				text-align-last:center !important;
			}

			.aumentarTamaño {

				font-size:15px;
				text-align: center;
			}
		</style>
		<script src="sweetalert2/dist/sweetalert2.min.js"></script>
		<link rel="stylesheet" href="sweetalert2/dist/sweetalert2.min.css">
	</head>
	<body>
		<header>
			<table>
				<tr><script>barra_imagenes("serv");</script><?php cuadro_titulos();?></tr>
			</table>
		</header>
		<form name="form2" method="post" action="">
			<section id="myapp" v-cloak >
				<nav>
					<table>
						<tr><?php menu_desplegable("serv");?></tr>
					</table>

					<div class="bg-white group-btn p-1" id="newNavStyle">
						<button type="button" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center" @click="location.href='serv-crearBarrio.php'">
							<span>Nuevo</span>
							<svg viewBox="0 -960 960 960" ><path d="M440-440H200v-80h240v-240h80v240h240v80H520v240h-80v-240Z"/></svg>
						</button>

						<button type="button" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center" @click="editarGuardar()">
							<span>Guardar</span>
							<svg viewBox="0 -960 960 960" ><path d="M840-680v480q0 33-23.5 56.5T760-120H200q-33 0-56.5-23.5T120-200v-560q0-33 23.5-56.5T200-840h480l160 160Zm-80 34L646-760H200v560h560v-446ZM480-240q50 0 85-35t35-85q0-50-35-85t-85-35q-50 0-85 35t-35 85q0 50 35 85t85 35ZM240-560h360v-160H240v160Zm-40-86v446-560 114Z"/></svg>
                    	</button>

						<button type="button" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center" @click="location.href='serv-buscarBarrios.php'">
							<span>Buscar</span>
							<svg class="fill-black group-hover:fill-white w-5 h-5" viewBox="0 -960 960 960" ><path d="M784-120 532-372q-30 24-69 38t-83 14q-109 0-184.5-75.5T120-580q0-109 75.5-184.5T380-840q109 0 184.5 75.5T640-580q0 44-14 83t-38 69l252 252-56 56ZM380-400q75 0 127.5-52.5T560-580q0-75-52.5-127.5T380-760q-75 0-127.5 52.5T200-580q0 75 52.5 127.5T380-400Z"/></svg>
						</button>

						<button type="button" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center" @click="mypop=window.open('serv-principal.php','',''); mypop.focus();">
							<span>Nueva ventana</span>
							<svg class="fill-black group-hover:fill-white w-5 h-5" viewBox="0 -960 960 960" ><path d="M320-120v-80h80v-80H160q-33 0-56.5-23.5T80-360v-400q0-33 23.5-56.5T160-840h640q33 0 56.5 23.5T880-760v400q0 33-23.5 56.5T800-280H560v80h80v80H320ZM160-360h640v-400H160v400Zm0 0v-400 400Z"/></svg>
						</button>
					</div>
				</nav>
				<article>
					<table class="inicio ancho">
						<tr>
							<td class="titulos" colspan="6">Editar barrio</td>
							<td class="cerrar" style="width:7%" onClick="location.href='serv-principal.php'">Cerrar</td>
						</tr>

						<tr>
							<td class="tamano01" style="width: 10%;">Código barrio: </td>
							<td style="width: 15%;">
								<input type="text" v-model="codigoBarrio" style="width: 100%; height: 30px; text-align:center;" readonly>
							</td>

                            <td class="tamano01" style="width: 10%;">Nombre barrio: </td>
							<td style="width: 15%;" colspan="3">
								<input type="text" v-model="nombreBarrio" style="width: 100%; height: 30px;">
							</td>
						</tr>

                        <tr>
                            <td class="tamano01" style="width: 10%;">Departamento: </td>
							<td style="width: 15%;">
								<input type="text" v-model="nombreDepartamento" v-on:dblclick="despliegaDepartamentos" style="width: 100%; height: 30px; text-align:center;" class="colordobleclik" readonly>
							</td>

                            <td class="tamano01" style="width: 10%;">Municipio: </td>
							<td style="width: 15%;">
								<input type="text" v-model="nombreMunicipio" v-on:dblclick="llamaMunicipios" style="width: 100%; height: 30px; text-align:center;" class="colordobleclik" readonly>
							</td>

                            <td class="tamano01" style="width: 10%;">Centro poblado: </td>
							<td style="width: 15%;">
								<input type="text" v-model="nombreCentroPoblado" v-on:dblclick="llamaCentroPoblado" style="width: 100%; height: 30px; text-align:center;" class="colordobleclik" readonly>
							</td>
                        </tr>

						<tr>
							<td class="tamano01" style="width: 10%;">Ubicaciones: </td>
							<td>
								<select v-model="ubicacion" class="centrarSelect" style="width: 100%;">
									<option class="aumentarTamaño" v-for="ubicacion in ubicaciones" v-bind:value = "ubicacion[0]">
										{{ ubicacion[1] }}
									</option>
								</select>
							</td>
						</tr>
					</table>

					<div id="cargando" v-if="loading" class="loading">
						<span>Cargando...</span>
					</div>

					<div v-show="showDepartamentos">
						<transition name="modal">
							<div class="modal-mask">
								<div class="modal-wrapper">
									<div class="modal-container1">
										<table class="inicio ancho">
											<tr>
												<td class="titulos" colspan="2" >Departamentos</td>
												<td class="cerrar" style="width:7%" @click="showDepartamentos = false">Cerrar</td>
											</tr>
											<tr>
												<td class="tamano01" style="width:3cm">Departamento:</td>
												<td>
													<input type="text" v-model="searchDepartamento.keywordDepartamento" class="form-control" placeholder="Buscar por codigo o nombre departamento" v-on:keyup="searchMonitorDepartamento" style="width:100%" />
												</td>
											</tr>
										</table>
										<table class='tablamv'>
											<thead>
												<tr style="text-align:center;">
													<th class="titulosnew02" >Resultados Busqueda</th>
												</tr>
												<tr style="text-align:center;">
													<th class="titulosnew00" style="width:12%;">Código <br> departamento</th>
													<th class="titulosnew00">Nombre</th>
												</tr>
											</thead>
											<tbody>
												<tr v-for="(departamento,index) in departamentos" v-on:click="seleccionaDepartamento(departamento)" v-bind:class="index % 2 ? 'saludo1a' : 'saludo2'" style='text-rendering: optimizeLegibility; cursor: pointer !important;'>
													<td style="font: 120% sans-serif; padding-left:10px; width:12%; text-align:center;">{{ departamento[0] }}</td>
													<td style="font: 120% sans-serif; padding-left:10px; text-align:center;">{{ departamento[1] }}</td>
												</tr>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</transition>
					</div>

                    <div v-show="showMunicipios">
						<transition name="modal">
							<div class="modal-mask">
								<div class="modal-wrapper">
									<div class="modal-container1">
										<table class="inicio ancho">
											<tr>
												<td class="titulos" colspan="2" >Municipios</td>
												<td class="cerrar" style="width:7%" @click="showMunicipios = false">Cerrar</td>
											</tr>
											<tr>
												<td class="tamano01" style="width:3cm">Municipio:</td>
												<td>
													<input type="text" v-model="searchMunicipio.keywordMunicipio" class="form-control" placeholder="Buscar nombre de municipio" v-on:keyup="searchMonitorMunicipio" style="width:100%" />
												</td>
											</tr>
										</table>
										<table class='tablamv'>
											<thead>
												<tr style="text-align:center;">
													<th class="titulosnew02" >Resultados Busqueda</th>
												</tr>
												<tr style="text-align:center;">
													<th class="titulosnew00" style="width:12%;">Código <br> Municipio</th>
													<th class="titulosnew00">Nombre</th>
												</tr>
											</thead>
											<tbody>
												<tr v-for="(municipio,index) in municipios" v-on:click="seleccionaMunicipio(municipio)" v-bind:class="index % 2 ? 'saludo1a' : 'saludo2'" style='text-rendering: optimizeLegibility; cursor: pointer !important;'>
													<td style="font: 120% sans-serif; padding-left:10px; width:12%; text-align:center;">{{ municipio[0] }}</td>
													<td style="font: 120% sans-serif; padding-left:10px; text-align:center;">{{ municipio[1] }}</td>
												</tr>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</transition>
					</div>

                    <div v-show="showCentroPoblado">
						<transition name="modal">
							<div class="modal-mask">
								<div class="modal-wrapper">
									<div class="modal-container1">
										<table class="inicio ancho">
											<tr>
												<td class="titulos" colspan="2" >Centros Poblados</td>
												<td class="cerrar" style="width:7%" @click="showCentroPoblado = false">Cerrar</td>
											</tr>
											<tr>
												<td class="tamano01" style="width:3cm">Centro Poblado:</td>
												<td>
													<input type="text" v-model="searchCentroPoblado.keywordCentroPoblado" class="form-control" placeholder="Buscar nombre de centro poblado" v-on:keyup="searchMonitorCentroPoblado" style="width:100%" />
												</td>
											</tr>
										</table>
										<table class='tablamv'>
											<thead>
												<tr style="text-align:center;">
													<th class="titulosnew02" >Resultados Busqueda</th>
												</tr>
												<tr style="text-align:center;">
													<th class="titulosnew00" style="width:12%;">Código <br> Centro <br> Poblado</th>
													<th class="titulosnew00">Nombre</th>
												</tr>
											</thead>
											<tbody>
												<tr v-for="(centrosPoblado,index) in centrosPoblados" v-on:click="seleccionaCentroPoblado(centrosPoblado)" v-bind:class="index % 2 ? 'saludo1a' : 'saludo2'" style='text-rendering: optimizeLegibility; cursor: pointer !important;'>
													<td style="font: 120% sans-serif; padding-left:10px; width:12%; text-align:center;">{{ centrosPoblado[0] }}</td>
													<td style="font: 120% sans-serif; padding-left:10px; text-align:center;">{{ centrosPoblado[1] }}</td>
												</tr>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</transition>
					</div>

				</article>
			</section>
		</form>
        
		<script src="Librerias/vue/vue.min.js"></script>
		<script src="Librerias/vue/axios.min.js"></script>
		<script src="servicios_publicos/barrios/editar/serv-editarBarrio.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
        
	</body>
</html>