<?php  
	require_once 'PHPExcel/Classes/PHPExcel.php';
	require 'comun.inc';
	require 'funciones.inc';
	session_start();
    $listaRecibos = unserialize($_POST['lista_recibos']);
	$listaNumero = unserialize($_POST['lista_numero']);
	$listaNombre = unserialize($_POST['lista_nombre']);
	$listaConcepto = unserialize($_POST['lista_concepto']);
	$listaFecha = unserialize($_POST['lista_fecha']);	
	$listaUsuario = unserialize($_POST['lista_usuario']);
	$listaValor = unserialize($_POST['lista_valor']);
	$listaLiquidacion = unserialize($_POST['lista_liquidacion']);
	$listaTipo = unserialize($_POST['lista_tipo']);
	$listaEstado = unserialize($_POST['lista_estado']);

	$objPHPExcel = new PHPExcel();
	$objPHPExcel->getProperties()
		->setCreator("SPID")
		->setLastModifiedBy("SPID")
		->setTitle("Reporte General de Descuentos")
		->setSubject("Nomina")
		->setDescription("Nomina")
		->setKeywords("Nomina")
		->setCategory("Gestion Humana");
	$objPHPExcel->getActiveSheet()->mergeCells('A1:J1');
	$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A1', 'DESCUENTOS');
	$objFont=$objPHPExcel->getActiveSheet()->getStyle('A1')->getFont();
	$objFont->setName('Courier New');
	$objFont->setSize(15); 
	$objFont->setBold(true);
	$objFont->setUnderline(PHPExcel_Style_Font::UNDERLINE_SINGLE);
	$objFont->getColor()->setARGB( PHPExcel_Style_Color::COLOR_BLACK);
	$objAlign=$objPHPExcel->getActiveSheet()->getStyle('A1')->getAlignment(); 
	$objAlign->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); 
	$objAlign->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER); 
	$objPHPExcel-> getActiveSheet ()
		-> getStyle ("A2:J2")
		-> getFill ()
		-> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
		-> getStartColor ()
		-> setRGB ('A6E5F3');
	$objPHPExcel-> getActiveSheet ()
		-> getStyle ("A1")
		-> getFill ()
		-> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
		-> getStartColor ()
		-> setRGB ('A6E5F3');
	$borders = array(
		'borders' => array(
			'allborders' => array(
				'style' => PHPExcel_Style_Border::BORDER_THIN,
				'color' => array('argb' => 'FF000000'),
			)
		),
	);
	$objPHPExcel->getActiveSheet()->getStyle('A2:J2')->applyFromArray($borders);
	$objPHPExcel->setActiveSheetIndex(0)
		->setCellValue('A2', 'No Recibo')
		->setCellValue('B2', 'No Ingresos')
		->setCellValue('C2', 'Nombre Ingreso')
		->setCellValue('D2', 'Concepto')
		->setCellValue('E2', 'Fecha')
		->setCellValue('F2', 'Usuario')
		->setCellValue('G2', 'Valor')
		->setCellValue('H2', 'No Liquidación')
		->setCellValue('I2', 'Tipo')
		->setCellValue('J2', 'ESTADO');
	$i=3;
    for($ii=0;$ii<count($listaRecibos);$ii++){
        $objPHPExcel->setActiveSheetIndex(0)
		->setCellValueExplicit ("A$i", $listaRecibos[$ii], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
		->setCellValueExplicit ("B$i", $listaNumero[$ii], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
		->setCellValueExplicit ("C$i", $listaNombre[$ii], PHPExcel_Cell_DataType :: TYPE_STRING)
		->setCellValueExplicit ("D$i", $listaConcepto[$ii], PHPExcel_Cell_DataType :: TYPE_STRING)
		->setCellValueExplicit ("E$i", $listaFecha[$ii], PHPExcel_Cell_DataType :: TYPE_STRING)
		->setCellValueExplicit ("F$i", $listaUsuario[$ii], PHPExcel_Cell_DataType :: TYPE_STRING)
		->setCellValueExplicit ("G$i", $listaValor[$ii], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
		->setCellValueExplicit ("H$i", $listaLiquidacion[$ii], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
		->setCellValueExplicit ("I$i", $listaTipo[$ii], PHPExcel_Cell_DataType :: TYPE_STRING)
		->setCellValueExplicit ("J$i", $listaEstado[$ii], PHPExcel_Cell_DataType :: TYPE_STRING);
		$objPHPExcel->getActiveSheet()->getStyle("A$i:J$i")->applyFromArray($borders);
        $i++;
    }
	
	//----Propiedades de la hoja 1
	$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->setTitle('DESCUENTOS');
	$objPHPExcel->setActiveSheetIndex(0);

//----Guardar documento----
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename="REPORTE GENERAL DESCUENTOS.xls"');
header('Cache-Control: max-age=0');
$objWriter=PHPExcel_IOFactory::createWriter($objPHPExcel,'Excel5');
$objWriter->save('php://output');
exit;

?>