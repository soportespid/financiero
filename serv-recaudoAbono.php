<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=iso-8859-1");
	require "comun.inc";
	require "funciones.inc";
	require "conversor.php";
	session_start();
	$linkbd = conectar_v7();
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es">
	<head>
		<meta http-equiv="Content-type" content="text/html; charset=iso-8859-1"/>
		<meta http-equiv="X-UA-Compatible" content="IE=9"/>
		<title>:: IDEAL 10 - Servicios P&uacute;blicos</title>
		<link href="css/css2.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/cssSP.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<script type="text/javascript" src="JQuery/jquery-2.1.4.min.js"></script>
        <script type="text/javascript" src="JQuery/autoNumeric-master/autoNumeric-min.js"></script>
		<script type="text/javascript" src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
        <script type="text/javascript" src="css/calendario.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<style>
		</style>

		<script>
            function despliegamodal2(_valor, _table)
			{
                document.getElementById('bgventanamodal2').style.visibility = _valor;
                
                if (_table == 'srvclientes')
                {
                    document.getElementById('ventana2').src = 'serv-ventanaClientesRecaudoAnticipo.php';
                }
				else if(_table == 'srvcuentasbancarias')
				{
					document.getElementById('ventana2').src = 'cuentasBancarias-ventana.php';
				}
            }

			function despliegamodalm(_valor,_tip,mensa,pregunta)
			{
				document.getElementById("bgventanamodalm").style.visibility=_valor;

				if(_valor=="hidden")
				{
					document.getElementById('ventanam').src="";
				}
				else
				{
					switch(_tip)
					{
						case "1":	
							document.getElementById('ventanam').src="ventana-mensaje1.php?titulos="+mensa;
						break;

						case "2":	
							document.getElementById('ventanam').src="ventana-mensaje3.php?titulos="+mensa;
						break;

						case "3":	
							document.getElementById('ventanam').src="ventana-mensaje2.php?titulos="+mensa;
						break;

						case "4":	
							document.getElementById('ventanam').src="ventana-consulta1.php?titulos="+mensa+"&idresp="+pregunta;
						break;	
					}
				}
			}

			function funcionmensaje()
			{
				document.location.href = "serv-recaudoAbono.php";
			}

			function respuestaconsulta(pregunta)
			{
				switch(pregunta)
				{
					case "1":	
						document.form2.oculto.value='2';
						document.form2.submit();
					break;
				}
			}

			function guardar()
			{
				var codigo = document.getElementById('consecutivo').value;
                var usuario = document.getElementById('codUsuario').value;
				var recaudo = document.getElementById('valor').value;
				var fecha = document.getElementById('fecha').value;

				if (usuario != '' && recaudo != '' && fecha != '') {

					if(recaudo > 0 ) {

						despliegamodalm('visible','4','Esta Seguro de Guardar','1');
					}
					else {

						despliegamodalm('visible','2','El valor debe ser mayor a cero.');
					}		   
				}
				else 
				{
                    despliegamodalm('visible','2','Falta información para crear el acuerdo.');
                }
			}

			function actualizar()
			{
				document.form2.submit();
			}

			jQuery(function($){ $('#valorvl').autoNumeric('init');});
		</script>

		<?php titlepag();?>

	</head>

	<body>
		<IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
		<span id="todastablas2"></span>
		<table>
			<tr><script>barra_imagenes("serv");</script><?php cuadro_titulos();?></tr>

			<tr><?php menu_desplegable("serv");?></tr>

			<tr>
				<td colspan="3" class="cinta">
					<a href="" class="mgbt"><img src="imagenes/add.png"/></a>
					<a onclick="guardar();" class="mgbt"><img src="imagenes/guarda.png" title="Guardar"/></a>
					<a href="" class="mgbt"><img src="imagenes/busca.png" title="Buscar"/></a>
					<a onclick="mypop=window.open('plan-agenda.php','','');mypop.focus()" class="mgbt"><img src="imagenes/agenda1.png" title="Agenda" /></a>
					<a onclick="mypop=window.open('serv-principal.php','','');mypop.focus();" class="mgbt"><img src="imagenes/nv.png" title="Nueva ventana"></a>
					<a onclick="mypop=window.open('<?php echo $url2; ?>','','');mypop.focus();" class="mgbt"><img src="imagenes/duplicar_pantalla.png" title="Duplicar pesta&ntilde;a"></a>
                    <a href="" class="mgbt"><img src="imagenes/iratras.png" title="Atr&aacute;s"></a>
                </td>
			</tr>
		</table>

		<div id="bgventanamodalm" class="bgventanamodalm">
			<div id="ventanamodalm" class="ventanamodalm">
				<IFRAME src="" name="ventanam" marginWidth=0 marginHeight=0 frameBorder=0 id="ventanam" frameSpacing=0 style=" width:700px; height:130px; top:200; overflow:hidden;"></IFRAME>
			</div>
		</div>

		<form name="form2" method="post" action="">
			<?php 
				if(@$_POST['oculto'] == "")
				{				
					$_POST['consecutivo'] = selconsecutivo('srvabonos_cab','id_abono');
				}
			?>
			<div>
				<table class="inicio ancho">
					<tr>
						<td class="titulos" colspan="7">.: Recaudo Abono</td>
						<td class="cerrar" style="width:7%" onClick="location.href='serv-principal.php'">Cerrar</td>
					</tr>

					<tr>
						<td class="tamano01" style="width:3cm;">Consecutivo:</td>
						<td style="text-align: center; ">
							<input type="text" name="consecutivo" id="consecutivo" onKeyUp="return tabular(event,this)" value="<?php echo @ $_POST['consecutivo'];?>" style="width:50%;height:30px;text-align:center;" readonly/>
						</td>

						<td class="tamano01" style="width: 5cm;">Usuario:</td>

						<td style="width: 15%;">
							<input type="text" name='codUsuario' id='codUsuario'  value="<?php echo @$_POST['codUsuario']?>" style="text-align:center; width:100%;" ondblclick = "despliegamodal2('visible','srvclientes');" class="colordobleclik" readonly>
							<input type="hidden" name="idCliente" id="idCliente" value="<?php echo @$_POST['idCliente'] ?>">
							<input type="hidden" name="documento" id="documento" value="<?php echo @$_POST['documento'] ?>">
						</td>

						<td colspan="3">
							<input type="text" name="nombreUsuario" id="nombreUsuario" value="<?php echo @$_POST['nombreUsuario']?>" style="width:100%;height:30px;" readonly>
						</td>
					</tr>

					<tr>
						<td class="tamano01" style="width:10%;">Fecha:</td>
						<td style="width:15%;">
							<input type="text" name="fecha" id="fecha" value="<?php echo @ $_POST['fecha']?>" onchange="" maxlength="10" onKeyUp="return tabular(event,this)" onKeyDown="mascara(this,'/',patron,true)" title="DD/MM/YYYY" placeholder="DD/MM/YYYY" style="height:30px;text-align:center;width:85%;"/>
							
							<a href="#" onClick="displayCalendarFor('fecha');" title="Calendario">
								<img src="imagenes/calendario04.png" style="width:25px;">
							</a>
						</td>

						<td class="tamano01">Recaudo en:</td>
						<td>
							<select  name="modoRecaudo" id="modoRecaudo" style="width:100%;height:30px;" onChange="actualizar()" class="centrarSelect">
								<option class="aumentarTamaño" value="-1">:: SELECCIONE TIPO DE RECAUDO ::</option>
								<option class="aumentarTamaño" value="caja" <?php if(@$_POST['modoRecaudo']=='caja') echo "SELECTED"; ?>>Caja</option>
								<option class="aumentarTamaño" value="banco" <?php if(@$_POST['modoRecaudo']=='banco') echo "SELECTED"; ?>>Banco</option>
							</select>
						</td>

						<?php
							if (@$_POST['modoRecaudo'] == 'banco')
							{
						?>
								<td class="tamano01" style="width: 3cm;">Cuenta Banco: </td> 
								<td style="width: 10%;">
									<input type="text" id="banco" name="banco" style="text-align:center;" onKeyPress="javascript:return solonumeros(event)" onKeyUp="return tabular(event,this)" onBlur="" value="<?php echo $_POST['banco']?>" onClick="document.getElementById('banco').focus();document.getElementById('banco').select();" readonly>
									<input type="hidden" name="cuentaBancaria" id="cuentaBancaria" value="">
									<a title="Bancos" onClick="despliegamodal2('visible','srvcuentasbancarias');" style="cursor:pointer;"><img src="imagenes/find02.png" style="width:20px;"/></a>
								</td>

								<td>
									<input type="text" name="nbanco" id="nbanco" style="width:100%;" value="<?php echo $_POST['nbanco']?>"  readonly>
								</td>	
						<?php
							}
						?>
					</tr>

					<tr>
						<td class="tamano01" style="width: 150px;">Valor:</td>
						<td>
							<input type="hidden" id="valor" name="valor" value="<?php echo $_POST['valor']?>">
                            <input type="text" name="valorvl" id="valorvl" data-a-sign="$" data-a-dec="<?php echo $_SESSION["spdecimal"];?>" data-a-sep="<?php echo $_SESSION["spmillares"];?>" data-v-min='0' onKeyUp="sinpuntitos2('valor','valorvl');return tabular(event,this);" value="<?php echo $_POST['valorvl']; ?>" style='width:98%;text-align:center;' />
						</td>
					</tr>
				</table>
			</div>				

			<input type="hidden" name="oculto" id="oculto" value="1"/>

			<?php 
				if(@$_POST['oculto'] == "2")
				{
					$usuario = $_SESSION['usuario'];
					$codigoComprobante = '40';
					$fec = explode("/", $_POST['fecha']);
                	$fecha = $fec[2].'-'.$fec[1].'-'.$fec[0];

					$sqlConceptoContable = "SELECT cuenta, cc FROM conceptoscontables_det WHERE tipo = 'SA' AND modulo = '10'";
					$resConceptoContable = mysqli_query($linkbd,$sqlConceptoContable);
					$rowConceptoContable = mysqli_fetch_row($resConceptoContable);

					if ($_POST['modoRecaudo'] == 'caja') {

						$sqlCuentaCaja = "SELECT cuentacaja FROM tesoparametros";
						$resCuentaCaja = mysqli_query($linkbd,$sqlCuentaCaja);
						$rowCuentaCaja = mysqli_fetch_row($resCuentaCaja);

						$cuenta = $rowCuentaCaja[0];
					} else {

						$cuenta = $_POST['cuentaBancaria'];
					}

					$sqlComprobanteCabecera = "INSERT INTO comprobante_cab (numerotipo, tipo_comp, fecha, concepto, total_debito, total_credito, estado) VALUES ('$_POST[consecutivo]', '$codigoComprobante', '$fecha', 'Pago por Anticipo codigo de usuario $_POST[codUsuario]', '$_POST[valor]', '$_POST[valor]', '1') ";
					mysqli_query($linkbd,$sqlComprobanteCabecera);

					$sqlComprobanteDebito = "INSERT INTO comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque,valdebito, valcredito,estado,vigencia) VALUES ('$codigoComprobante $_POST[consecutivo]', '$cuenta','$_POST[documento]','$rowConceptoContable[cc]','Pago por Anticipo del cliente $_POST[codUsuario]', '', '$_POST[valor]', 0, '1', $fec[2])";
					mysqli_query($linkbd,$sqlComprobanteDebito);

					$sqlComprobanteCredito = "INSERT INTO comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque,valdebito, valcredito,estado,vigencia) VALUES ('$codigoComprobante $_POST[consecutivo]', '$rowConceptoContable[0]','$_POST[documento]','$rowConceptoContable[cc]','Pago por Anticipo del cliente $_POST[codUsuario]', '', 0, '$_POST[valor]', '1', $fec[2])";
					mysqli_query($linkbd,$sqlComprobanteCredito);

					$sqlCabecera = "INSERT INTO srvabonos_cab (id_abono, id_cliente, cod_usuario, fecha, recaudo_en, cuenta, valor_abono, valor_gastado, estado) VALUES ('$_POST[consecutivo]', '$_POST[idCliente]', '$_POST[codUsuario]', '$fecha', '$_POST[modoRecaudo]', '$cuenta', '$_POST[valor]', 0, 'ACTIVO')";
					
					if (mysqli_query($linkbd,$sqlCabecera)) {
						echo "<script>despliegamodalm('visible','1','Se ha almacenado con Exito');</script>";
					}
					else {
						echo"<script>despliegamodalm('visible','2','No se pudo realizar el guardado.');</script>";
					}	
				}
			?>
		</form>

		<div id="bgventanamodal2">
			<div id="ventanamodal2">
				<IFRAME  src="" name="buster" marginWidth=0 marginHeight=0 frameBorder=0 id="ventana2" frameSpacing=0 style="left:500px; width:900px; height:500px; top:200;"></IFRAME>
			</div>
		</div>

	</body>
</html>
