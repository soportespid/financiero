<?php
	require_once 'PHPExcel/Classes/PHPExcel.php';
    include '/PHPExcel/Classes/PHPExcel/IOFactory.php';
	require "comun.inc";
	require "funciones.inc";
    ini_set('max_execution_time',99999999);
    header("Content-type: application/json");
	session_start();
	$linkbd=conectar_v7();

	$objPHPExcel = new PHPExcel();
	
	$objPHPExcel->getProperties()
		->setCreator("IDEAL10")
		->setLastModifiedBy("IDEAL10")
		->setTitle("LECTURA ACUEDUCTO Y ALCANTARILLADO")
		->setSubject("SP")
		->setDescription("SP")
		->setKeywords("SP")
		->setCategory("SERVICIOS PUBLICOS");
	$objPHPExcel->getActiveSheet()->mergeCells('A1:J1');
	$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A1', 'LECTURAS ACUEDUCTO Y ALCANTARILLADO');
	$objFont=$objPHPExcel->getActiveSheet()->getStyle('A1')->getFont();
	$objFont->setName('Courier New');
	$objFont->setSize(15);
	$objFont->setBold(true);
	$objFont->setUnderline(PHPExcel_Style_Font::UNDERLINE_SINGLE);
	$objFont->getColor()->setARGB( PHPExcel_Style_Color::COLOR_BLACK);
	$objAlign=$objPHPExcel->getActiveSheet()->getStyle('A1')->getAlignment(); 
	$objAlign->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
	$objAlign->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
	$objPHPExcel-> getActiveSheet ()
		-> getStyle ("A2:J2")
		-> getFill ()
		-> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
		-> getStartColor ()
		-> setRGB ('A6E5F3');
	$objPHPExcel-> getActiveSheet ()
		-> getStyle ("A1")	
		-> getFill ()
		-> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
		-> getStartColor ()
		-> setRGB ('A6E5F3');
	$borders = array(
		'borders' => array(
			'allborders' => array(
				'style' => PHPExcel_Style_Border::BORDER_THIN,
				'color' => array('argb' => 'FF000000'),
			)
		),
	);
	$objPHPExcel->getActiveSheet()->getStyle('A2:J2')->applyFromArray($borders);
	$objPHPExcel->setActiveSheetIndex(0)
        ->setCellValue('A2', 'Codigo usuario')
        ->setCellValue('B2', 'Suscriptor')
        ->setCellValue('C2', 'Dirección')
        ->setCellValue('D2', 'Serial medidor')
		->setCellValue('E2', 'Consumo 3')
		->setCellValue('F2', 'Consumo 2')
		->setCellValue('G2', 'Consumo anterior')
        ->setCellValue('H2', 'Lectura anterior')
        ->setCellValue('I2', 'Lectura actual')
        ->setCellValue('J2', 'Consumo');     
	$i=3;
    
	for($ii=0;$ii<count ($_POST['codUsuario']);$ii++)
	{
		$objPHPExcel->setActiveSheetIndex(0)
		->setCellValueExplicit ("A$i", $_POST['codUsuario'][$ii], PHPExcel_Cell_DataType :: TYPE_STRING)
		->setCellValueExplicit ("B$i", $_POST['suscriptor'][$ii], PHPExcel_Cell_DataType :: TYPE_STRING)
		->setCellValueExplicit ("C$i", $_POST['direccion'][$ii], PHPExcel_Cell_DataType :: TYPE_STRING)
        ->setCellValueExplicit ("D$i", $_POST['medidor'][$ii], PHPExcel_Cell_DataType :: TYPE_STRING)
		->setCellValueExplicit ("E$i", $_POST['consumo3'][$ii], PHPExcel_Cell_DataType :: TYPE_STRING)
		->setCellValueExplicit ("F$i", $_POST['consumo2'][$ii], PHPExcel_Cell_DataType :: TYPE_STRING)
		->setCellValueExplicit ("G$i", $_POST['consumoAnterior'][$ii], PHPExcel_Cell_DataType :: TYPE_STRING)
        ->setCellValueExplicit ("H$i", $_POST['lecturaAnterior'][$ii], PHPExcel_Cell_DataType :: TYPE_STRING)
        ->setCellValueExplicit ("I$i", $_POST['lecturaActual'][$ii], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
        ->setCellValueExplicit ("J$i", $_POST['consumo'][$ii], PHPExcel_Cell_DataType :: TYPE_NUMERIC);

        $objPHPExcel->getActiveSheet()->getStyle("A$i:J$i")->applyFromArray($borders);

        $i++;
	}

	$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);

	$objPHPExcel->getActiveSheet()->setTitle('SP');
	//header('Content-Type: application/vnd.ms-excel');
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
	header('Content-Disposition: attachment;filename="lecturaACyALC.xlsx"');
	header('Cache-Control: max-age=0');
	$objWriter=PHPExcel_IOFactory::createWriter($objPHPExcel,'Excel2007');
	$objWriter->save('php://output');
	exit;
?>