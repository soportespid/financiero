<?php
	require "comun.inc";
	require "funciones.inc";
	require "validaciones.inc";
	require "conversor.php";
	session_start();
	$linkbd=conectar_bd();	
	cargarcodigopag($_GET[codpag],$_SESSION["nivel"]);
	header("Cache-control: private"); // Arregla IE 6
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es">
	<head>
	 	<meta http-equiv="Content-Type" content="text/html" charset="iso-8859-1"/>
        <meta http-equiv="X-UA-Compatible" content="IE=9"/>
		<title>:: SPID- Servicios Publicos</title>
        <link href="css/css2.css" rel="stylesheet" type="text/css" />
		<link href="css/css3.css" rel="stylesheet" type="text/css" />
        <link href="css/tabs.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="css/programas.js"></script>
        <script type="text/javascript" src="css/calendario.js"></script>
		<script>
			function validar(){document.form2.submit();}
			function guardar()
			{
				if (document.form2.fecha.value!='' && ((document.form2.modorec.value=='banco' && document.form2.banco.value!='') || (document.form2.modorec.value=='caja' && document.form2.cuentacaja.value!='')) )
  				{despliegamodalm('visible','4','Esta Seguro de Reflejar','1');}
  				else {despliegamodalm('visible','2','Faltan datos para completar el registro');}
			}
			function pdf()
			{
				document.form2.action="serv-pdfrecaja.php";
				document.form2.target="_BLANK";
				document.form2.submit(); 
				document.form2.action="";
				document.form2.target="";
			}
			function adelante()
			{
				if(parseFloat(document.form2.ncomp.value)<parseFloat(document.form2.maximo.value))
 				{
					document.form2.oculto.value=1;
					document.form2.ncomp.value=parseFloat(document.form2.ncomp.value)+1;
					document.form2.idcomp.value=parseFloat(document.form2.idcomp.value)+1;
					document.form2.action="serv-recaudo_reflejar.php";
					document.form2.submit();
				}
				else {alert();}
			}
			function atrasc()
			{
				if(document.form2.ncomp.value>1)
 				{
					document.form2.oculto.value=1;
					document.form2.ncomp.value=document.form2.ncomp.value-1;
					document.form2.idcomp.value=document.form2.idcomp.value-1;
					document.form2.action="serv-recaudo_reflejar.php";
					document.form2.submit();
 				}
			}
			function validar2()
			{
				document.form2.oculto.value=1;
				document.form2.ncomp.value=document.form2.idcomp.value;
				document.form2.action="serv-recaudo_reflejar.php";
				document.form2.submit();
			}
			function despliegamodalm(_valor,_tip,mensa,pregunta)
			{
				document.getElementById("bgventanamodalm").style.visibility=_valor;
				if(_valor=="hidden")
				{
					document.getElementById('ventanam').src="";
					if(document.getElementById('valfocus').value=="2")
					{
						document.getElementById('valfocus').value='1';
						document.getElementById('documento').focus();
					}
				}
				else
				{
					switch(_tip)
					{
						case "1":
							document.getElementById('ventanam').src="ventana-mensaje1.php?titulos="+mensa;break;
						case "2":
							document.getElementById('ventanam').src="ventana-mensaje3.php?titulos="+mensa;break;
						case "3":
							document.getElementById('ventanam').src="ventana-mensaje2.php?titulos="+mensa;break;
						case "4":
							document.getElementById('ventanam').src="ventana-consulta1.php?titulos="+mensa+"&idresp="+pregunta;break;	
					}
				}
			}
			function funcionmensaje(){}
			function respuestaconsulta(pregunta)
			{
				switch(pregunta)
				{
					case "1":
						document.form2.oculto.value=2;
  						document.form2.submit();
						break;
				}
			}
		</script>
		<?php titlepag();?>
	</head>
	<body>
		<IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
		<span id="todastablas2"></span>
		<table>
			<tr><script>barra_imagenes("serv");</script><?php cuadro_titulos();?></tr>	 
			<tr><?php menu_desplegable("serv");?></tr>
			<tr>
  				<td colspan="3" class="cinta"><img src="imagenes/add.png" title="Nuevo" onClick="location.href='serv-recaudo_reflejar.php'" class='mgbt'/><img src="imagenes/guardad.png" class='mgbt1'/><img src="imagenes/buscad.png" class='mgbt1'/><img src="imagenes/nv.png" title="Nueva Ventana"  onClick="mypop=window.open('serv-principal.php','','');mypop.focus();"  class='mgbt'/><img src="imagenes/reflejar1.png" title="Reflejar" onClick="guardar()"  class='mgbt'/></td>
			</tr>		  
		</table>
        <div id="bgventanamodalm" class="bgventanamodalm">
            <div id="ventanamodalm" class="ventanamodalm">
                <IFRAME src="" name="ventanam" marginWidth=0 marginHeight=0 frameBorder=0 id="ventanam" frameSpacing=0 style=" width:700px; height:130px; top:200; overflow:hidden;"> 
                </IFRAME>
            </div>
        </div>
        <form name="form2" method="post" action=""> 
			<?php
                $vigusu=vigencia_usuarios($_SESSION[cedulausu]);
                $vigencia=$vigusu;
                $sqlr="select valor_inicial from dominios where nombre_dominio='CUENTA_CAJA'";
                $res=mysql_query($sqlr,$linkbd);
                while ($row =mysql_fetch_row($res)){$_POST[cuentacaja]=$row[0];}
                $_POST[tipovista]=1;	
                //*********** 11050501	CAJA PRINCIPAL esta es la cuenta que va a credito en todas las consignacones
                if(!$_POST[oculto])
                {   
                    $check1="checked";
                    $fec=date("d/m/Y");
                    $_POST[vigencia]=$vigencia;
                    $sqlr="select valor_inicial from dominios where nombre_dominio='CUENTA_CAJA'";
                    $res=mysql_query($sqlr,$linkbd);
                    while ($row =mysql_fetch_row($res)){$_POST[cuentacaja]=$row[0];}	
                    $sqlr="select max(id_recibos) from servreciboscaja ";
                    $res=mysql_query($sqlr,$linkbd);
                    $consec=0;
                    while($r=mysql_fetch_row($res)){$_POST[maximo]=$r[0];}
                    $_POST[idcomp]=$_GET[idr];
                    $_POST[ncomp]=$_POST[idcomp]; 
                }
                $sqlr="select * from servreciboscaja where id_recibos=$_POST[idcomp]";
                $res=mysql_query($sqlr,$linkbd);
                $consec=0;
                while($r=mysql_fetch_row($res))
                {
                    $_POST[idrecaudo]=$r[4];	 
                    $_POST[fecha]=$r[2];
                    $_POST[modorec]=$r[5];
                    $_POST[banco]=$r[7];	   
                    $_POST[estado]=$r[9];
                    $_POST[tiporec]=$r[10];
                    if($_POST[estado]=='S'){$_POST[estadoc]="ACTIVO";}
                    else {$_POST[estadoc]="ANULADO";}
                }
                switch($_POST[tabgroup1])
                {
                    case 1:	$check1='checked'; break;
                    case 2:	$check2='checked'; break;
                    case 3:	$check3='checked';
                }
            ?>
 		
			<input name="encontro" type="hidden" value="<?php echo $_POST[encontro]?>" >
       		<input name="cobrorecibo" type="hidden" value="<?php echo $_POST[cobrorecibo]?>" >
       		<input name="vcobrorecibo" type="hidden" value="<?php echo $_POST[vcobrorecibo]?>" >
            <input name="tcobrorecibo" type="hidden" value="<?php echo $_POST[tcobrorecibo]?>" > 
       		<input name="codcatastral" type="hidden" value="<?php echo $_POST[codcatastral]?>" >
 			<?php 
 				if($_POST[oculto])
				{
					switch($_POST[tiporec]) 
  					{
	  					case 4:	$sqlr="select *from  servfacturas where  servfacturas.id_factura=$_POST[idrecaudo]  and 4=$_POST[tiporec]";
  	  							$_POST[encontro]="";
	  							$res=mysql_query($sqlr,$linkbd);
								while ($row =mysql_fetch_row($res)) 
	 							{
	      							$nliqui=$row[0];
		  							$tercero=$row[0];
								}
								$sqlr="select servliquidaciones_det.servicio, servliquidaciones_det.valorliquidacion, servliquidaciones_det.estrato, servliquidaciones.saldo, servliquidaciones.codusuario, servliquidaciones.tercero, servliquidaciones.id_liquidacion from servliquidaciones, servliquidaciones_det where servliquidaciones.factura=$_POST[idrecaudo] and servliquidaciones.id_liquidacion=servliquidaciones_det.id_liquidacion ";
								$res=mysql_query($sqlr,$linkbd);
								while ($row =mysql_fetch_row($res)) 
	 							{	
									$_POST[intereses]=$row[3];
									$_POST[codcatastral]=$row[1];		
									$_POST[concepto]=$row[17].'USUARIO '.$row[4];	
									$_POST[valorecaudo]=$row[8];		
									$_POST[totalc]=$row[8];	
									$_POST[tercero]=$row[5];	
									$_POST[liquidacion]=$row[6];
									$_POST[codigousuario]=$row[4];
									$_POST[ntercero]=buscatercero($row[5]);
									if ($_POST[ntercero]=='')
		 							{
										$sqlr2="select *from tesopredios where cedulacatastral='".$row[1]."' ";
		  								$resc=mysql_query($sqlr2,$linkbd);
		  								$rowc =mysql_fetch_row($resc);
		   								$_POST[ntercero]=$rowc[6];
		 							}	
	  								$_POST[encontro]=1;
								}
	  							break;
	 					case 5:	$sqlr="select *from  servfacturas where  servfacturas.id_factura=$_POST[idrecaudo]  and 5=$_POST[tiporec]";
  	  							$_POST[encontro]="";
	  							$res=mysql_query($sqlr,$linkbd);
								while ($row =mysql_fetch_row($res)) 
	 							{
	      							$nliqui=$row[0];
		  							$tercero=$row[0];
								}
								$sqlr="select servliquidaciones_det.servicio, servliquidaciones_det.valorliquidacion, servliquidaciones_det.estrato, servliquidaciones.saldo, servliquidaciones.codusuario, servliquidaciones.tercero, servliquidaciones.id_liquidacion from servliquidaciones, servliquidaciones_det where servliquidaciones.factura=$_POST[idrecaudo] and servliquidaciones.id_liquidacion=servliquidaciones_det.id_liquidacion ";
								$res=mysql_query($sqlr,$linkbd);
								while ($row =mysql_fetch_row($res)) 
	 							{	
									$_POST[intereses]=$row[3];
									$_POST[codcatastral]=$row[1];		
									$_POST[concepto]=$row[17].'USUARIO '.$row[4];	
									$_POST[valorecaudo]=$row[8];		
									$_POST[totalc]=$row[8];	
									$_POST[tercero]=$row[5];	
									$_POST[liquidacion]=$row[6];
									$_POST[codigousuario]=$row[4];
									$_POST[ntercero]=buscatercero($row[5]);		
									$_POST[encontro]=1;
								}
	  							break;
	  							case 2:
	  							$sqlr="select *from tesoindustria where tesoindustria.id_industria=$_POST[idrecaudo] and estado ='S' and 2=$_POST[tiporec]";
  	  							$_POST[encontro]="";
	  							$res=mysql_query($sqlr,$linkbd);
								while ($row =mysql_fetch_row($res)) 
	 							{
	  								$_POST[concepto]="Liquidacion Industria y Comercio avisos y tableros - ".$row[3];	
									$_POST[valorecaudo]=$row[6];		
									$_POST[totalc]=$row[6];	
									$_POST[tercero]=$row[5];	
									$_POST[ntercero]=buscatercero($row[5]);	
									$_POST[encontro]=1;
									$_POST[cuotas]=$row[9]+1;
									$_POST[tcuotas]=$row[8];
	 							}
	  							break;
	  					case 3:	$sqlr="select *from tesorecaudos where tesorecaudos.id_recaudo=$_POST[idrecaudo] and estado ='S' and 3=$_POST[tiporec]";
  	  							$_POST[encontro]="";
	  							$res=mysql_query($sqlr,$linkbd);
								while ($row =mysql_fetch_row($res)) 
	 							{
	  								$_POST[concepto]=$row[6];	
									$_POST[valorecaudo]=$row[7];		
									$_POST[totalc]=$row[5];	
									$_POST[tercero]=$row[4];	
									$_POST[ntercero]=buscatercero($row[4]);	
									$_POST[encontro]=1;
	 							}
								break;	
					}
 				}
			?>
    		<table class="inicio" align="center" >
      			<tr >
        			<td class="titulos" colspan="9">Reflejar Recaudo Ventanilla</td>
        			<td class="cerrar" style="width:7%;"><a onClick="location.href='serv-principal.php'">&nbsp;Cerrar</a></td>
      			</tr>
      			<tr >
        			<td  class="saludo1" >No Recibo:</td>
                    <input name="codigousuario" type="hidden" value="<?php echo $_POST[codigousuario]?>" size='10' >
                    <input name="liquidacion" type="hidden" value="<?php echo $_POST[liquidacion]?>" size='3' >
                    <input name="intereses" type="hidden" value="<?php echo $_POST[intereses]?>" >
                    <input name="tipovista" type="hidden" value="<?php echo $_POST[tipovista]?>" >
                    <input name="cuentacaja" type="hidden" value="<?php echo $_POST[cuentacaja]?>" >
       				<td  ><a href="#" onClick="atrasc()"><img src="imagenes/back.png" alt="anterior" align="absmiddle"></a><input name="idcomp" type="text" size="5" value="<?php echo $_POST[idcomp]?>" onKeyUp="return tabular(event,this) "  onBlur="validar2()" ><input name="ncomp" type="hidden" value="<?php echo $_POST[ncomp]?>"><a href="#" onClick="adelante()"><img src="imagenes/next.png" alt="siguiente" align="absmiddle"></a> <input type="hidden" value="a" name="atras" ><input type="hidden" value="s" name="siguiente" ><input type="hidden" value="<?php echo $_POST[maximo]?>" name="maximo"></td>
	  				<td  class="saludo1">Fecha:</td>
        			<td ><input name="fecha" type="date" value="<?php echo $_POST[fecha]?>" maxlength="10" size="10" onKeyPress="javascript:return solonumeros(event)" onKeyUp="return tabular(event,this)" readonly></td>
         			<td class="saludo1">Vigencia:</td>
		  			<td >
                    	<input type="text" id="vigencia" name="vigencia" size="8" onKeyPress="javascript:return solonumeros(event)" 
		  onKeyUp="return tabular(event,this)"  value="<?php echo $_POST[vigencia]?>" onClick="document.getElementById('tipocta').focus();document.getElementById('tipocta').select();" readonly>  
		  				<input type="hidden" name="estado" value="<?php echo $_POST[estado]?>">    
		  				<input type="text" name="estadoc" value="<?php echo $_POST[estadoc]?>" readonly>   
                    </td> 		  
        		</tr>
      			<tr>
                	<td class="saludo1"> Recaudo:</td>
                    <td> 
                    	<select name="tiporec" id="tiporec" onKeyUp="return tabular(event,this)" onChange="validar()" >
         					<option value="4" <?php if($_POST[tiporec]=='4') echo "SELECTED"; ?>>Factura Servicios</option> 
		 					<option value="5" <?php if($_POST[tiporec]=='5') echo "SELECTED"; ?>>Abono Factura Servicios</option>         
        				</select>
          </td>
        <td class="saludo1">No Factura:</td>
        <td><input type="text" id="idrecaudo" name="idrecaudo" value="<?php echo $_POST[idrecaudo]?>" size="30" onKeyUp="return tabular(event,this)" onBlur="validar()" readonly> </td>
	 <td class="saludo1">Recaudado en:</td><td> <select name="modorec" id="modorec" onKeyUp="return tabular(event,this)" onChange="validar()" >
         <option value="caja" <?php if($_POST[modorec]=='caja') echo "SELECTED"; ?>>Caja</option>
          <option value="banco" <?php if($_POST[modorec]=='banco') echo "SELECTED"; ?>>Banco</option>
        </select>
        <?php
		  if ($_POST[modorec]=='banco')
		   {
		?>
         <select id="banco" name="banco"  onChange="validar()" onKeyUp="return tabular(event,this)">
	      <option value="">Seleccione....</option>
		  <?php
	$linkbd=conectar_bd();
	$sqlr="select tesobancosctas.estado,tesobancosctas.cuenta,tesobancosctas.ncuentaban,tesobancosctas.tipo,terceros.razonsocial,tesobancosctas.tercero from tesobancosctas,terceros where tesobancosctas.tercero=terceros.cedulanit and tesobancosctas.estado='S' ";
	$res=mysql_query($sqlr,$linkbd);
	while ($row =mysql_fetch_row($res)) 
				    {
					echo "<option value=$row[1] ";
					$i=$row[1];
					 if($i==$_POST[banco])
			 			{
						 echo "SELECTED";
						 $_POST[nbanco]=$row[4];
						  $_POST[ter]=$row[5];
						 $_POST[cb]=$row[2];
						 }
					  echo ">".$row[2]." - Cuenta ".$row[3]."</option>";	 	 
					}	 	
	?>
            </select>
       <input name="cb" type="hidden" value="<?php echo $_POST[cb]?>" ><input type="hidden" id="ter" name="ter" value="<?php echo $_POST[ter]?>" >           </td>
       		<td> <input type="text" id="nbanco" name="nbanco" value="<?php echo $_POST[nbanco]?>" size="40" readonly>
          </td>
        <?php
		   }
		?> 
       </tr>
	  <tr><td class="saludo1" width="71">Concepto:</td>
      <td colspan="3"><input name="concepto" size="90" type="text" value="<?php echo $_POST[concepto] ?>" onKeyUp="return tabular(event,this)" readonly></td>
      <?php
	  if($_POST[tiporec]==2)
	   {
		?>   
      <td class="saludo1">No Cuota:</td><td><input name="cuotas" size="1" type="text" value="<?php echo $_POST[cuotas] ?>" readonly>/<input type="text" id="tcuotas" name="tcuotas" value="<?php echo $_POST[tcuotas]?>" size="1"  readonly ></td>
      <?php
	   }
	  ?>
	  </tr>
      <tr><td class="saludo1" width="71">Valor:</td><td><input type="text" id="valorecaudo" name="valorecaudo" value="<?php echo $_POST[valorecaudo]?>" size="30" onKeyUp="return tabular(event,this)" readonly ></td><td  class="saludo1">Documento: </td>
        <td ><input name="tercero" type="text" value="<?php echo $_POST[tercero]?>" size="20" onKeyUp="return tabular(event,this)" readonly>
         </td>
			  <td class="saludo1">Contribuyente:</td>
	  <td  ><input type="text" id="ntercero" name="ntercero" value="<?php echo $_POST[ntercero]?>" size="50" onKeyUp="return tabular(event,this) "  readonly><input type="hidden" id="cb" name="cb" value="<?php echo $_POST[cb]?>" ><input type="hidden" id="ct" name="ct" value="<?php echo $_POST[ct]?>" >
	  </td><td>
	    <input type="hidden" value="1" name="oculto">
		<input type="hidden" value="<?php echo $_POST[trec]?>"  name="trec">
	    <input type="hidden" value="0" name="agregadet"></td></tr>
     
      </table>
     <div class="subpantallac7">
      <?php 
	
 
  switch($_POST[tiporec]) 
  	 {
	    case 4: ///*****************otros recaudos *******************
	  	 $_POST[trec]='Factura Servicios';	 
			// echo $_POST[tcobrorecibo];
		 $_POST[dcoding]= array(); 		 
		 $_POST[dncoding]= array(); 		 
		 $_POST[dvalores]= array(); 
		 $_POST[destratos]= array(); 
		 $_POST[dcargosfijos]= array(); 		 
		 $_POST[dtarifas]= array(); 		 
		 $_POST[dsubsidios]= array(); 
		 $_POST[ddescuentos]= array(); 
		 $_POST[dcontribuciones]= array(); 		 
		 $_POST[dsaldos]= array(); 		 
		 $_POST[dinteres]= array(); 
		 $_POST[dtotales]= array(); 		 
		 $sqlr="select * from servreciboscaja_det where id_recibos=$_POST[idcomp] order by ingreso";
		//echo "$sqlr";				 
		  $res=mysql_query($sqlr,$linkbd);
		  while ($row =mysql_fetch_row($res)) 
			{
			$_POST[dcoding][]=$row[2];	
			$_POST[dncoding][]=buscar_servicio($row[2]);			 		
		    $_POST[dcargofijos][]=$row[3];	
			$_POST[dtarifas][]=$row[4];	
		    $_POST[dsubsidios][]=$row[5];	
		    $_POST[ddescuentos][]=$row[6];							
			$_POST[dcontribuciones][]=$row[7];	
			$_POST[dsaldos][]=$row[8];	
			$_POST[dvalores][]=$row[10];	
			$_POST[dtotales][]=$row[11];	
			$_POST[dinteres][]=$row[9];	
			//$tam=count($_POST[dcoding]);
			//$valorint=round($_POST[intereses]/$tam,2);					
			//$_POST[dinteres][]=$valorint;	
			//echo 	$valorint; 	
			}
		break;
	case 5: ///*****************otros recaudos *******************
	  	 $_POST[trec]='Abono Factura Servicios';	 
			// echo $_POST[tcobrorecibo];
		 $_POST[dcoding]= array(); 		 
		 $_POST[dncoding]= array(); 		 
		 $_POST[dvalores]= array(); 
		 $_POST[destratos]= array(); 
		 $_POST[dcargosfijos]= array(); 		 
		 $_POST[dtarifas]= array(); 		 
		 $_POST[dsubsidios]= array(); 
		 $_POST[ddescuentos]= array(); 
		 $_POST[dcontribuciones]= array(); 		 
		 $_POST[dsaldos]= array(); 		 
		 $_POST[dinteres]= array(); 
		 $_POST[dtotales]= array(); 		 
		 $sqlr="select * from servreciboscaja_det where id_recibos=$_POST[idcomp] order by ingreso";
		//echo "$sqlr";				 
		  $res=mysql_query($sqlr,$linkbd);
		  while ($row =mysql_fetch_row($res)) 
			{
			$_POST[dcoding][]=$row[2];	
			$_POST[dncoding][]=buscar_servicio($row[2]);			 		
		    $_POST[dcargofijos][]=$row[3];	
			$_POST[dtarifas][]=$row[4];	
		    $_POST[dsubsidios][]=$row[5];	
		    $_POST[ddescuentos][]=$row[6];							
			$_POST[dcontribuciones][]=$row[7];	
			$_POST[dsaldos][]=$row[8];	
			$_POST[dvalores][]=$row[10];	
			$_POST[dtotales][]=$row[11];	
			$_POST[dinteres][]=$row[9];	
			//$tam=count($_POST[dcoding]);
			//$valorint=round($_POST[intereses]/$tam,2);					
			//$_POST[dinteres][]=$valorint;	
			//echo 	$valorint; 	
			}
		break;	
   }

 ?>
	   <table class="inicio">
	   	   <tr><td colspan="11" class="titulos">Detalle Factura</td></tr>                  
		<tr><td class="titulos2">Codigo</td><td class="titulos2">Servicio</td><td class="titulos2">Cargo Fijo</td><td class="titulos2">Tarifa</td><td class="titulos2">Subsidio</td><td class="titulos2">Descuento</td><td class="titulos2">Contribucion</td><td class="titulos2">Saldo Anterior</td><td class="titulos2">Valor</td><td class="titulos2">Intereses</td><td class="titulos2">Total</td></tr>
		  <?php
		  $_POST[totalc]=0;
		$iter1="zebra1";
		$iter2="zebra2";
		$tam=count($_POST[dcoding]);
		$valorint=round($_POST[intereses]/$tam,2);
		 for ($x=0;$x<count($_POST[dcoding]);$x++)
		 {		 
		 //$_POST[dinteres][]=$valorint;	
		// 	$total=$valorint+$_POST[dvalores][$x];
		 echo "<tr class='$iter1'><td ><input name='dcoding[]' value='".$_POST[dcoding][$x]."' type='text' size='2' readonly></td><td><input name='dncoding[]' value='".$_POST[dncoding][$x]."' type='text' size='40' readonly></td><td><input name='dcargofijos[]' value='".$_POST[dcargofijos][$x]."' type='text' size='12' readonly></td><td><input name='dtarifas[]' value='".$_POST[dtarifas][$x]."' type='text' size='12' readonly></td><td><input name='dsubsidios[]' value='".$_POST[dsubsidios][$x]."' type='text' size='15' readonly></td><td><input name='ddescuentos[]' value='".$_POST[ddescuentos][$x]."' type='text' size='12' readonly></td><td><input name='dcontribuciones[]' value='".$_POST[dcontribuciones][$x]."' type='text' size='12' readonly></td><td><input name='dsaldos[]' value='".$_POST[dsaldos][$x]."' type='text' size='12' readonly></td><td><input name='dvalores[]' value='".$_POST[dvalores][$x]."' type='text' size='12' readonly></td><td><input name='dinteres[]' value='".$_POST[dinteres][$x]."' type='text' size='12' readonly></td><td><input name='dtotales[]' value='".$_POST[dtotales][$x]."' type='text' size='12' readonly></td></tr>";
		 $_POST[totalc]=$_POST[totalc]+$_POST[dtotales][$x];
		 $_POST[totalcf]=number_format($_POST[totalc],0);
		 $aux=$iter1;
		 $iter1=$iter2;
		 $iter2=$aux;
		 }
 			$resultado = convertir($_POST[totalc],"PESOS");
			$_POST[letras]=$resultado." PESOS M/CTE";
		 echo "<tr class='$iter1'><td colspan='9'></td><td >Total</td><td ><input name='totalcf' type='text' value='$_POST[totalcf]' size='12' readonly><input name='totalc' type='hidden' value='$_POST[totalc]'></td></tr><tr><td class='saludo1'>Son:</td><td colspan='10' ><input name='letras' type='text' value='$_POST[letras]' size='200'></td></tr>";
		?> 
	   </table></div>
	  <?php
if($_POST[oculto]=='2')
{
	$linkbd=conectar_bd();
	ereg( "([0-9]{1,2})/([0-9]{1,2})/([0-9]{2,4})", $_POST[fecha],$fecha);
	$fechaf=$fecha[3]."-".$fecha[2]."-".$fecha[1];
	$bloq=bloqueos($_SESSION[cedulausu],$_POST[fecha]);	
	if($bloq>=1)
	{
	//************VALIDAR SI YA FUE GUARDADO ************************
	switch($_POST[tiporec]) 
  	{
		case 4: 	//**************OTROS RECAUDOS
					$fechaf=$_POST[fecha];
					//*********************CREACION DEL COMPROBANTE CONTABLE ***************************
					//***busca el consecutivo del comprobante contable
					$concecc=0;
					$sqlr="delete from pptocomprobante_cab where tipo_comp=16 and numerotipo='$_POST[idcomp]' ";
					mysql_query($sqlr,$linkbd);
					$sqlr="delete from pptocomprobante_det where tipo_comp=16 and numerotipo='$_POST[idcomp]' ";
					mysql_query($sqlr,$linkbd);
					$sqlr="delete from comprobante_cab where tipo_comp=5 and numerotipo='$_POST[idcomp]' ";
					mysql_query($sqlr,$linkbd);
					$sqlr="delete from comprobante_det where tipo_comp=5 and numerotipo='$_POST[idcomp]' ";
					mysql_query($sqlr,$linkbd);	
		   			if ($_POST[estado]=='N'){$estado=0;}
		   			else {$estado=1;}
					$sqlr="insert into comprobante_cab (numerotipo,tipo_comp,fecha,concepto,total,total_debito,total_credito,diferencia,estado) values ($_POST[idcomp],5,'$fechaf','$_POST[concepto]',0,$_POST[totalc],$_POST[totalc],0,'$estado')";
					mysql_query($sqlr,$linkbd);	
	 				$sqlr="insert into  pptocomprobante_cab (numerotipo,tipo_comp,fecha,concepto,vigencia,total_debito,total_credito,diferencia, estado) values($_POST[idcomp],16,'$fechaf','RECIBO DE CAJA',$_POST[vigencia],0,0,0,'$estado')";
	 				mysql_query($sqlr,$linkbd);			   
					//***cabecera comprobante
 					echo "<table class='saludo1'><tr><td class='saludo1'><center>Se ha almacenado el Recibo de Caja con Exito <img src='imagenes/confirm.png'></center></td></tr></table>";
					//******************* DETALLE DEL COMPROBANTE CONTABLE *********************
					for($x=0;$x<count($_POST[dcoding]);$x++)
	 				{
		 				//***** BUSQUEDA INGRESO ********
						$sqlri="Select * from servservicios where codigo='".$_POST[dcoding][$x]."' ";
	 					$resi=mysql_query($sqlri,$linkbd);
						while($rowi=mysql_fetch_row($resi))
		 				{
	    					//**** busqueda cuenta presupuestal*****
							//busqueda concepto contable ///SERVICIO *****************
							$sqlrc="SELECT DISTINCT cuenta,tipocuenta,debito,credito,cc FROM conceptoscontables_det T2 WHERE tipo='SP' AND codigo='$rowi[2]' AND modulo='10' AND fechainicial=(SELECT MAX(fechainicial) FROM conceptoscontables_det WHERE fechainicial <= '$fechaf' AND modulo='10' AND tipo='SP' AND codigo='$rowi[2]')";
	 	 					$resc=mysql_query($sqlrc,$linkbd);	  
							while($rowc=mysql_fetch_row($resc))
		 					{
			  					$columna= $rowc[2];	
			  					$cuentacont=$rowc[0];			 
								if($columna=='S')
			  					{				 
									$valorcred=$_POST[dvalores][$x];
									$valordeb=0;
									if($rowc[1]=='N')
			    					{
			   							//*****inserta del concepto contable  
			   							//***********MODIFICAR CUENTA PPTAL DE INGRESO AGREGARLE EL RECAUDO *********
			 							$sqlr="insert into  pptocomprobante_det (cuenta,tercero,detalle,valdebito,valcredito,estado,vigencia, tipo_comp,numerotipo) values('$rowi[3]','','RECIBO CAJA ','$valorcred',0,1,'$_POST[vigencia]',16,'$_POST[idcomp]')";
	 	  								mysql_query($sqlr,$linkbd); 
										$vi=$_POST[dvalores][$x];
										//************ FIN MODIFICACION PPTAL
										$sqlr="insert into comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque,valdebito, valcredito,estado,vigencia) values ('5 $_POST[idcomp]','$cuentacont','$_POST[tercero]','$rowc[4]','Ingreso ".strtoupper($_POST[dncoding][$x])."','',0,'$vi','1','$_POST[vigencia]')";
										mysql_query($sqlr,$linkbd);
										//***cuenta caja o banco
										if($_POST[modorec]=='caja')
			  							{				 
											$cuentacb=$_POST[cuentacaja];
											$cajas=$_POST[cuentacaja];
											$cbancos="";
			  							}
										if($_POST[modorec]=='banco')
			   		 					{
											$cuentacb=$_POST[banco];				
											$cajas="";
											$cbancos=$_POST[banco];
			    						}
										$sqlr="insert into comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque,valdebito,valcredito, estado,vigencia) values ('5 $_POST[idcomp]','$cuentacb','$_POST[tercero]','$rowc[4]','Ingreso ".strtoupper($_POST[dncoding][$x])."','',$vi,0,'1','$_POST[vigencia]')";
										mysql_query($sqlr,$linkbd);
									}
			  					}
		   					}	
		  					//*****intereses ************* 
		   					//busqueda concepto contable ///SERVICIO *****************
		   					if($_POST[dinteres][$x]>0)
		   					{
		 						$sqlrc="SELECT DISTINCT cuenta,tipocuenta,debito,credito,cc FROM conceptoscontables_det T2 WHERE tipo='SP' AND codigo='$rowi[7]' AND modulo='10' AND fechainicial=(SELECT MAX(fechainicial) FROM conceptoscontables_det WHERE fechainicial <= '$fechaf' AND modulo='10' AND tipo='SP' AND codigo='$rowi[7]')";
	 	 						$resc=mysql_query($sqlrc,$linkbd);	  
								while($rowc=mysql_fetch_row($resc))
		 						{
			  						$columna= $rowc[2];	
			  						$cuentacont=$rowc[0];			 
									if($columna=='N')
			  						{				 
										$valorcred=$_POST[dvalores][$x];
										$valordeb=0;
										if($rowc[1]=='N')
			    						{
			   								//*****inserta del concepto contable  
			   								//***********MODIFICAR CUENTA PPTAL DE INGRESO AGREGARLE EL RECAUDO *********
			 								$sqlr="insert into  pptocomprobante_det (cuenta,tercero,detalle,valdebito,valcredito,estado,vigencia, tipo_comp,numerotipo) values('$rowi[3]','','RECIBO CAJA ','$valorcred',0,1,'$_POST[vigencia]',16,'$_POST[idcomp]')";
	 	  									mysql_query($sqlr,$linkbd); 
											$vi=$_POST[dinteres][$x];
											//************ FIN MODIFICACION PPTAL
											$sqlr="insert into comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque,valdebito, valcredito,estado,vigencia) values ('5 $_POST[idcomp]','$cuentacont','$_POST[tercero]','$rowc[4]','Ingreso INTERESES ".strtoupper($_POST[dncoding][$x])."','',0,'$vi','1','$_POST[vigencia]')";
											mysql_query($sqlr,$linkbd);
											//echo $sqlr."<br>";						
											//***cuenta caja o banco
											if($_POST[modorec]=='caja')
			  								{				 
												$cuentacb=$_POST[cuentacaja];
												$cajas=$_POST[cuentacaja];
												$cbancos="";
			  								}
											if($_POST[modorec]=='banco')
			   		 						{
												$cuentacb=$_POST[banco];				
												$cajas="";
												$cbancos=$_POST[banco];
			    							}
											$sqlr="insert into comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque,valdebito, valcredito,estado,vigencia) values ('5 $_POST[idcomp]','$cuentacb','$_POST[tercero]','$rowc[4]','Ingreso INTERESES ".strtoupper($_POST[dncoding][$x])."','','$vi',0,'1','$_POST[vigencia]')";
											mysql_query($sqlr,$linkbd);
										}
			  						}
		   						}	
		  					}//********fin intereses
						}	
					}
					break;
	
		case 5:	//**************OTROS RECAUDOS
				$fechaf=$_POST[fecha];
				//*********************CREACION DEL COMPROBANTE CONTABLE ***************************
				//***busca el consecutivo del comprobante contable
				$concecc=0;
				$sqlr="delete from pptocomprobante_cab where tipo_comp=16 and numerotipo='$_POST[idcomp]' ";
				mysql_query($sqlr,$linkbd);
				$sqlr="delete from pptocomprobante_det where tipo_comp=16 and numerotipo='$_POST[idcomp]' ";
				mysql_query($sqlr,$linkbd);
				$sqlr="delete from comprobante_cab where tipo_comp=30 and numerotipo='$_POST[idcomp]' ";
				mysql_query($sqlr,$linkbd);
				$sqlr="delete from comprobante_det where tipo_comp=30 and numerotipo='$_POST[idcomp]' ";
				
				mysql_query($sqlr,$linkbd);	
		   		if ($_POST[estado]=='N'){$estado=0;}
		   		else {$estado=1;}
				$sqlr="insert into comprobante_cab (numerotipo,tipo_comp,fecha,concepto,total,total_debito,total_credito,diferencia,estado) values ($_POST[idcomp],30,'$fechaf','$_POST[concepto]',0,$_POST[totalc],$_POST[totalc],0,'$estado')";
				mysql_query($sqlr,$linkbd);	
	   			// echo "$sqlr <br>";	 	
	 			$sqlr="insert into  pptocomprobante_cab (numerotipo,tipo_comp,fecha,concepto,vigencia,total_debito,total_credito,diferencia,estado) values($_POST[idcomp],16,'$fechaf','RECIBO DE CAJA',$_POST[vigencia],0,0,0,'$estado')";
				mysql_query($sqlr,$linkbd);	   
	 			//echo "$sqlr <br>";	 
	 			// $consec=$concecc;
				//***cabecera comprobante
				echo "<script>despliegamodalm('visible','1','Se ha Reflejado el Recibo de Caja con Exito');</script>";
				//******************* DETALLE DEL COMPROBANTE CONTABLE *********************
				for($x=0;$x<count($_POST[dcoding]);$x++)
	 			{
		 			//***** BUSQUEDA INGRESO ********
					$sqlri="Select * from servservicios where codigo='".$_POST[dcoding][$x]."' ";
	 				$resi=mysql_query($sqlri,$linkbd);
					//	echo "$sqlri <br>";	    
					while($rowi=mysql_fetch_row($resi))
		 			{
	    				//**** busqueda cuenta presupuestal*****
						//busqueda concepto contable
						$sqlrc="SELECT DISTINCT cuenta,tipocuenta,debito,credito,cc FROM conceptoscontables_det T2 WHERE tipo='SP' AND codigo='$rowi[2]' AND modulo='10' AND fechainicial=(SELECT MAX(fechainicial) FROM conceptoscontables_det WHERE fechainicial <= '$fechaf' AND modulo='10' AND tipo='SP' AND codigo='$rowi[2]')"; 
						$resc=mysql_query($sqlrc,$linkbd);	  
						while($rowc=mysql_fetch_row($resc))
						{
							$columna= $rowc[2];	
							$cuentacont=$rowc[0];			 
							if($columna=='S')
							{				 
								$valorcred=$_POST[dtotales][$x];
								$valordeb=0;
								if($rowc[1]=='N')
								{
									$vi=$_POST[dtotales][$x]-$_POST[dinteres][$x];
									//*****inserta del concepto contable  
									//***********MODIFICAR CUENTA PPTAL DE INGRESO AGREGARLE EL RECAUDO *********
									$sqlr="insert into  pptocomprobante_det (cuenta,tercero,detalle,valdebito,valcredito,estado,vigencia,tipo_comp, numerotipo) values('$rowi[3]','','RECIBO CAJA ','$vi',0,1,'$_POST[vigencia]',16,'$_POST[idcomp]')";
									mysql_query($sqlr,$linkbd); 
									//************ FIN MODIFICACION PPTAL
									$sqlr="insert into comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque,valdebito,valcredito, estado,vigencia) values ('30 $_POST[idcomp]','$cuentacont','$_POST[tercero]','$rowc[5]','Ingreso ".strtoupper($_POST[dncoding][$x])."','',0,'$vi','1','$_POST[vigencia]')";
									mysql_query($sqlr,$linkbd);
									//***cuenta caja o banco
									if($_POST[modorec]=='caja')
									{				 
										$cuentacb=$_POST[cuentacaja];
										$cajas=$_POST[cuentacaja];
										$cbancos="";
									}
									if($_POST[modorec]=='banco')
									{
										$cuentacb=$_POST[banco];				
										$cajas="";
										$cbancos=$_POST[banco];
									}
									$sqlr="insert into comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque,valdebito,valcredito, estado,vigencia) values ('30 $_POST[idcomp]','$cuentacb','$_POST[tercero]','$rowc[5]','Ingreso ".strtoupper($_POST[dncoding][$x])."','','$vi',0,'1','$_POST[vigencia]')";
									mysql_query($sqlr,$linkbd);
								}
							}
						}	
		    			//*****intereses ************* 
		   				//busqueda concepto contable ///SERVICIO *****************
		   				if($_POST[dinteres][$x]>0)
		   				{
							$sqlrc="SELECT DISTINCT cuenta,tipocuenta,debito,credito,cc FROM conceptoscontables_det T2 WHERE tipo='SP' AND codigo='$rowi[7]' AND modulo='10' AND fechainicial=(SELECT MAX(fechainicial) FROM conceptoscontables_det WHERE fechainicial <= '$fechaf' AND modulo='10' AND tipo='SP' AND codigo='$rowi[7]')";
	 	 					$resc=mysql_query($sqlrc,$linkbd);	  
							while($rowc=mysql_fetch_row($resc))
		 					{
			  					$columna= $rowc[2];	
			  					$cuentacont=$rowc[0];			 
								if($columna=='N')
			  					{		
									$vi=$_POST[dinteres][$x];
									$valordeb=0;
									if($rowc[1]=='N')
			    					{
			   							//*****inserta del concepto contable  
			   							//***********MODIFICAR CUENTA PPTAL DE INGRESO AGREGARLE EL RECAUDO *********
			 							$sqlr="insert into  pptocomprobante_det (cuenta,tercero,detalle,valdebito,valcredito,estado,vigencia, tipo_comp,numerotipo) values('$rowi[3]','','RECIBO CAJA ''$vi',0,1,'$_POST[vigencia]',16,'$_POST[idcomp]')";
	 	  								mysql_query($sqlr,$linkbd); //interes
										//************ FIN MODIFICACION PPTAL
										$sqlr="insert into comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque,valdebito,valcredito, estado,vigencia) values ('30 $_POST[idcomp]','".$cuentacont."','".$_POST[tercero]."','$rowc[4]','Ingreso INTERESES ".strtoupper($_POST[dncoding][$x])."','',0,'$vi','1','$_POST[vigencia]')";
										mysql_query($sqlr,$linkbd);//interes
										//***cuenta caja o banco
										if($_POST[modorec]=='caja')
			  							{				 
											$cuentacb=$_POST[cuentacaja];
											$cajas=$_POST[cuentacaja];
											$cbancos="";
			  							}
										if($_POST[modorec]=='banco')
			   		 					{
											$cuentacb=$_POST[banco];				
											$cajas="";
											$cbancos=$_POST[banco];
			    						}
										$sqlr="insert into comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque,valdebito,valcredito, estado,vigencia) values ('30 $_POST[idcomp]','$cuentacb','$_POST[tercero]','$rowc[4]','Ingreso INTERESES ".strtoupper($_POST[dncoding][$x])."','','$vi',0,'1','$_POST[vigencia]')";
										mysql_query($sqlr,$linkbd);//interes
									}
			  					}
		   					}	
		  				}//********fin intereses	
					}
				}	 
				break;
			} //*****fin del switch	
	   //********************* INDUSTRIA Y COMERCIO
	} //fin de la verificacion
	else {echo"<script>despliegamodalm('visible','2','No tiene permisos para modificar este documento');</script>";}	
  //****fin if bloqueo  
  }//**fin del oculto 
?>	
</form>
 </td></tr>
</table>
</body>
</html>