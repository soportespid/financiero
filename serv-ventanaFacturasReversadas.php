<?php 
	require "comun.inc";
	require "funciones.inc";
    require "funcionesSP.inc.php";
	session_start();
	$linkbd = conectar_v7();	
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	header("Cache-control: private"); // Arregla IE 6
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es">
	<head>
	 	<meta http-equiv="Content-Type" content="text/html" charset="iso-8859-1"/>
        <meta http-equiv="X-UA-Compatible" content="IE=9"/>
		<title>:: IDEAL 10 - Servicios P&uacute;bilicos</title>
        <link href="css/css2.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="css/calendario.js"></script>
		<script>
			var anterior;
			function enviaFactura(factura, id)
			{
				parent.document.form2.facturaReversada.value = factura;
				parent.document.form2.id_reversion.value = id;
                parent.document.form2.reversado.value = '2';
				parent.despliega_modales("hidden");
                parent.document.form2.submit();
			} 
		</script> 
		<?php titlepag();?>
	</head>
	<body>
		<?php
			
		?>
	<form action="" method="post" enctype="multipart/form-data" name="form1">
		<table class="inicio">
			<tr>
				<td height="25" colspan="4" class="titulos">Facturas reversadas este corte</td>

				<td class="cerrar">
                    <a onClick="parent.despliega_modales('hidden');">Cerrar</a>
                </td>
			</tr>

			<tr>
				<td class="saludo1">Numero de Factura:</td>

				<td colspan="3">
                    <input name="facturaBuscar" id="facturaBuscar" type="text" style="height: 30x;" size="30">
					<input type="hidden" name="oculto" id="oculto" value="<?php echo $_POST['oculto'] = '1' ?>">
					<input type="submit" name="Submit" value="Buscar">
				</td>
			</tr>
		</table>

		<div class="subpantalla" style="height:73.5%; width:99.6%; overflow-x:hidden;">
			<table class="inicio">
				<tr class="titulos2">
					<td width="7%" style="text-align: center;">Factura Reversada</td>
					<td style="text-align: center;">Fecha Reversion</td>
					<td>Motivo</td>	 
				</tr>

				<?php
				$vigusu = vigencia_usuarios($_SESSION['cedulausu']); 

				$oculto=$_POST['oculto'];
				$corteActivo = buscaCorteActivo();

				if($oculto == "1")
				{
					$linkbd = conectar_v7();

					if($_POST['facturaBuscar'] != '')
                    {
                        $filtroBusqueda = "AND factura_origen = '$_POST[facturaBuscar]'";
                    }
                    else
                    {
                        $filtroBusqueda = '';
                    }
                    
                    $sqlReversado = "SELECT factura_origen, motivo, fecha, id FROM srvreversion_factura WHERE corte = '$corteActivo' AND factura_nueva_reversada = '0' $filtroBusqueda";
                    $resReversado = mysqli_query($linkbd,$sqlReversado);
                    $_POST['numtop'] = mysqli_num_rows($resReversado);

					$co = 'saludo1a';
					$co2 = 'saludo2';	
					
					while($rowReversado = mysqli_fetch_row($resReversado)) 
					{
						echo 
						"<tr class='$co' onMouseOver=\"anterior=this.style.backgroundColor;this.style.backgroundColor='#40b3ff';\" onMouseOut=\"this.style.backgroundColor=anterior\" "; 
						
					    echo" onClick=\"javascript:enviaFactura('$rowReversado[0]', '$rowReversado[3]')\"";
					 
						echo"
                            <td></td>
                            <td style='text-align: center;'>$rowReversado[0]</td>
                            <td style='text-align: center;'>$rowReversado[2]</td>
                            <td>$rowReversado[1]</td>
						</tr> ";
						
						$aux=$co;
						$co=$co2;
						$co2=$aux;
					}
                    if (@$_POST['numtop']==0)
						{
							echo "
                                <table class='inicio'>
                                    <tr>
                                        <td class='saludo1' style='text-align:center;width:100%'><img src='imagenes\alert.png' style='width:25px'>No hay facturas pendientes para este cliente<img src='imagenes\alert.png' style='width:25px'></td>
                                    </tr>
                                </table>";
						}
					$_POST['oculto']="";
				}
				?>
			</table>
		</div>
        <input type="hidden" name="numtop" id="numtop" value="<?php echo @ $_POST['numtop'];?>" />
	</form>
</body>
</html> 
