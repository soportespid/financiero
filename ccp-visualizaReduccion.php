<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require 'comun.inc';
	require 'funciones.inc';
	session_start();
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html lang=es>
	<head>
		<meta charset="utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1"/>
		<meta name="viewport" content="user-scalable=no">
		<title>:: IDEAL 10 - Presupuesto CCPET</title>
		<link href="favicon.ico" rel="shortcut icon"/>
		<link href="css/css2z.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
        <link href="css/tabs.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" href="sweetalert2/dist/sweetalert2.min.css">
        <script src="sweetalert2/dist/sweetalert2.min.js"></script>
		<script src="vue/vue.min.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script src="css/calendario.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
        <script>
            function pdf()
			{
				document.form2.action = "ccp-reduccionpdf";
				document.form2.target = "_BLANK";
				document.form2.submit(); 
				document.form2.action = "";
				document.form2.target = "";
			}
        </script>
		<style>
			input[type=number]::-webkit-inner-spin-button,
			input[type=number]::-webkit-outer-spin-button { 
				-webkit-appearance: none; 
				margin: 0; 
			}
			input[type=number]{
				text-align:right;
				box-sizing: border-box;
				font: 120% sans-serif;
				/* width: 100% !important; */
			}
			[v-cloak]{display : none;}

			.centrarSelect {

				height: 30px !important;
				text-align-last:center !important;
			}

			.aumentarTamaño {

				font-size:15px;
			}

            .tamano01 {
                text-align: center !important;
            }

            td {
				font-family: "Courier New", monospace;
            }
		</style>
	</head>
	<body>
		<header>
			<table>
				<tr><script>barra_imagenes("ccpet");</script><?php cuadro_titulos();?></tr>
			</table>
		</header>

		<form name="form2" method="post" action="">
			<section id="myapp" v-cloak >
				<nav>
					<table>
						<tr><?php menu_desplegable("ccpet");?></tr>
						<tr>
							<td colspan="3" class="cinta">
								<img src="imagenes/add.png" v-on:click="location.href='ccp-reduccion-vue'" class="mgbt" title="Nuevo">
								<img src="imagenes/guarda.png" title="Guardar" v-on:click="" class="mgbt">
								<img src="imagenes/busca.png" v-on:click="location.href='ccp-buscarReduccion-vue'" class="mgbt" title="Buscar">
								<img src="imagenes/nv.png" onClick="mypop=window.open('ccp-principal','',''); mypop.focus();" class="mgbt" title="Nueva Ventana">
                                <img src="imagenes/print.png" title="Imprimir" onClick="pdf()" class="mgbt">
								<img src="imagenes/excel.png" title="Excel" class="mgbt" v-on:click="excel">
							</td>
						</tr>
					</table>
				</nav>

				<article>
                    <div>
                        <table class="inicio grande">
                            <tr>
                                <td class="titulos" colspan="9">.: Reducción Presupuestal</td>
                            	<td class="cerrar" style="width:4%" onClick="location.href='ccp-principal.php'">Cerrar</td>
                            </tr>

                            <tr>
                                <td style="width: 10%;">Fecha:</td>
                                <td style="width: 15%;">
                                    <input type="text" name="fecha" v-model="fecha" style="text-align: center;" placeholder="dd/mm/aaaa" readonly>
									<input type="hidden" name="vigencia" v-model="vigencia">
                                </td>

                                <td style="width: 10%;">Acto adm:</td>
                                <td>
                                    <input type="text" name="actoAdm" v-model="actoAdm" style="width: 90%;" readonly>
									<input type="hidden" name="actoAdmId" v-model="actoAdmId">
									<input type="hidden" name="estado" v-model="estado">
                                </td>

                                <td style="width: 10%;">Valor Adición:</td>
                                <td style="width: 15%;">
                                    <input type="text" name="valorAdicion" v-model="formatonumero(valorAdicion)" style="text-align: right;" readonly>
									<input type="hidden" name="letras" v-model="letras">
									<input type="hidden" name="totalGasto" v-model="totalGasto">
									<input type="hidden" name="totalIngreso" v-model="totalIngreso">
                                </td>
                            </tr>

                          
                        </table>

						<div class='subpantalla' style='height:65vh; width:99.2%; margin-top:0px; overflow: hidden;'>
							<table>
								<tr>
									<td class="titulos" colspan="10">.: Cuenta CCPET y clasificadores complementarios</td>
								</tr>
							</table>

                            <table class='tablamv'>
                                <thead>
                                    <tr style="text-align:Center;">
                                        <th class="titulosnew00" style="width:6%;">Tipo cuenta</th>
                                        <th class="titulosnew00" style="width:7%;">Tipo de gasto</th>
                                        <th class="titulosnew00" style="width:10%;">Sec. presupuestal</th>
                                        <th class="titulosnew00" style="width:6%;">Medio pago</th>
                                        <th class="titulosnew00" style="width:7%;">Vig. del gasto</th>
										<th class="titulosnew00" style="width:10%;">Proyecto</th>
										<th class="titulosnew00" style="width:10%;">Programático</th>
										<th class="titulosnew00" style="width:10%;">Cuenta</th>
										<th class="titulosnew00" style="width:10%;">Fuente</th>
										<th class="titulosnew00" style="width:15%;">Valor ingresos</th>
										<th class="titulosnew00" style="width:15%;">Valor gastos</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr v-for="(det,index) in detalles" v-bind:class="index % 2 ? 'saludo1a' : 'saludo2'" style='text-rendering: optimizeLegibility; cursor: pointer !important;'>
										<td style="width:6%; font: 120% sans-serif; padding-left:5px; text-align:center;">{{ det[0] }}</td>
										<td style="width:7%; font: 120% sans-serif; padding-left:5px; text-align:center;">{{ det[1] }}</td>
										<td style="width:10%; font: 120% sans-serif; padding-left:5px; text-align:center;">{{ det[2] }}</td>
										<td style="width:6%; font: 120% sans-serif; padding-left:5px; text-align:center;">{{ det[3] }}</td>
										<td style="width:7%; font: 120% sans-serif; padding-left:5px; text-align:center;">{{ det[4] }}</td>
										<td style="width:10%; font: 120% sans-serif; padding-left:5px; text-align:center;">{{ det[5] }}</td>
										<td style="width:10%; font: 120% sans-serif; padding-left:5px; text-align:center;">{{ det[6] }}</td>
										<td style="width:10%; font: 120% sans-serif; padding-left:5px; text-align:center;">{{ det[7] }}</td>
										<td style="width:10%; font: 120% sans-serif; padding-left:5px; text-align:center;">{{ det[8] }}</td>
										<td style="width:15%; font: 120% sans-serif; padding-left:5px; text-align:center;">{{ formatonumero(det[9]) }}</td>
										<td style="width:15%; font: 120% sans-serif; padding-left:5px; text-align:center;">{{ formatonumero(det[10]) }}</td>

										
										<input type='hidden' name='tipoCuenta[]' v-model="det[0]">
										<input type='hidden' name='tipoGasto[]' v-model="det[1]">
										<input type='hidden' name='secPresupuestal[]' v-model="det[2]">
										<input type='hidden' name='medioPago[]' v-model="det[3]">
										<input type='hidden' name='vigGasto[]' v-model="det[4]">
										<input type='hidden' name='proyecto[]' v-model="det[5]">
										<input type='hidden' name='programatico[]' v-model="det[6]">
										<input type='hidden' name='cuenta[]' v-model="det[7]">
										<input type='hidden' name='fuente[]' v-model="det[8]">
										<input type='hidden' name='valorIngreso[]' v-model="det[9]">
										<input type='hidden' name='valorGasto[]' v-model="det[10]">

										<input type='hidden' name='codigoCuenta[]' v-model="det[11]">
										<input type='hidden' name='nombreCuenta[]' v-model="det[12]">
										<input type='hidden' name='codigoProyecto[]' v-model="det[13]">
										<input type='hidden' name='nombreProyecto[]' v-model="det[14]">
										<input type='hidden' name='codigoProgramatico[]' v-model="det[15]">
										<input type='hidden' name='codigoFuente[]' v-model="det[16]">
										<input type='hidden' name='codigoSec[]' v-model="det[17]">
										<input type='hidden' name='codigoVig[]' v-model="det[18]">
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>

					<div id="cargando" v-if="loading" class="loading">
						<span>Cargando...</span>
					</div>
				</article>
			</section>
		</form>

		<script src="node_modules/read-excel-file/bundle/read-excel-file.min.js"></script>
		<script src="node_modules/xlsx/dist/xlsx.full.min.js"></script>
		<script src="Librerias/vue/vue.min.js"></script>
		<script src="Librerias/vue/axios.min.js"></script>
		<script src="presupuesto_ccpet/reduccion\visualizar/ccp-visualizarReduccion.js?"></script>
        
	</body>
</html>