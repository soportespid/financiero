<?php
header("Cache-control: no-cache, no-store, must-revalidate");
header("Content-Type: text/html;charset=utf8");
require 'comun.inc';
require 'funciones.inc';
session_start();
cargarcodigopag(@$_GET['codpag'], @$_SESSION['nivel']);
header('Cache-control: private');
date_default_timezone_set('America/Bogota')
?>
<!DOCTYPE >
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es">

<head>
    <meta http-equiv="Content-Type" content="text/html" charset="uft8" />
    <meta http-equiv="X-UA-Compatible" content="IE=9" />
    <title>:: IDEAL 10 - Servicios Públicos</title>
    <link href="css/css2.css" rel="stylesheet" type="text/css" />
    <link href="css/css3.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="css/programas.js"></script>
    <script type="text/javascript" src="css/calendario.js"></script>

    <?php titlepag(); ?>
</head>

<body>
    <IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
    <span id="todastablas2"></span>
    <table>
        <tr>
            <script>
                barra_imagenes("serv");
            </script><?php cuadro_titulos(); ?>
        </tr>
        <tr><?php menu_desplegable("serv"); ?></tr>
        <tr>
            <td colspan="3" class="cinta">
                <a class="mgbt"><img src="imagenes/add2.png" /></a>
                <a class="mgbt"><img src="imagenes/guardad.png" title="Guardar" /></a>
                <a class="mgbt"><img src="imagenes/buscad.png" /></a><a href="#" class="mgbt" onClick="mypop=window.open('serv-principal.php','','');mypop.focus();"><img src="imagenes/nv.png" title="Nueva Ventana"></a>
            </td>
        </tr>
    </table>
        <div class="subpantalla" style="height:70%; overflow-x: hidden; float:left;">
            <table class="inicio">
                <tr>
                    <td class="titulos" colspan="2">.: Parametros Facturación </td>
                    <td class="cerrar" style="width:7%;"><a href="serv-principal.php">Cerrar</a></td>
                </tr>

                <tr>
                    <td>
                        <ol id="lista2">
                            <li onClick="location.href='serv-informeUsuarios'" style="cursor:pointer;">Informe de usuarios</li>
                            <li onClick="location.href='serv-informeIngresosNew'" style="cursor:pointer;">Informe de recaudo generalizado por servicio</li>
                            <li onClick="location.href='serv-reporte_facturacion'" style="cursor:pointer;">Informe de Facturación</li>
                            <li onClick="location.href='serv-informeSubsidios'" style="cursor:pointer;">Informe de subsidios</li>
                            <li onClick="location.href='serv-informeSubsidiosPorUsuario.php'" style="cursor:pointer;">Informe de subsidios por usuario</li>
                            <li onClick="location.href='serv-reporte-suspensiones'" style="cursor:pointer;">Informe suspensiones</li>
                            <li onClick="location.href='serv-reporte_cartera'" style="cursor:pointer;">Informe cartera por usuario</li>
                            <li onClick="location.href='serv-reporteCarteraIntereses'" style="cursor:pointer;">Informe cartera por usuario capital e intereses</li>
                            <li onClick="location.href='serv-menuSUI'" style="cursor:pointer;">Informes SUI</li>
                            <li onClick="location.href='serv-historicoConsumos'" style="cursor:pointer;">Historico de lecturas</li>
                            <li onClick="location.href='serv-verEstadoCartera'" style="cursor:pointer;">Estado de cartera</li>
                            <li onClick="location.href='serv-reporteRecaudos'" style="cursor:pointer;">Reporte de recaudos por periodo</li>
                            <li onClick="location.href='serv-reporteRecaudosPorServicio'" style="cursor:pointer;">Informe de recaudo por servicio y periodo</li>
                            <li onClick="location.href='serv-reporteDesincentivoAcueducto'" style="cursor:pointer;">Informe de desincentivo en acueducto por periodo liquidado</li>
                            <li onClick="location.href='serv-reporteVIATAseo'" style="cursor:pointer;">Informe de VIAT en aseo por periodo liquidado</li>
                            <li onClick="location.href='serv-reporteSeguimientoRecaudos'" style="cursor:pointer;">Informe de seguimiento de recaudos</li>
                            <li onclick="location.href='serv-informeRecaudosConceptos.php'" style="cursor:pointer;">Informe de recaudos por conceptos</li>
                        </ol>
                    </td>
                </tr>
            </table>
        </div>
    
</body>

</html>