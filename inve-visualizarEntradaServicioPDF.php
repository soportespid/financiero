<?php
	require_once("tcpdf/tcpdf_include.php");
	require('comun.inc');
	require "funciones.inc";
	require 'funcionesnomima.inc';
    require 'funcionesSP.inc.php';
    /*ini_set('display_errors', '1');
    ini_set('display_startup_errors', '1');
    error_reporting(E_ALL);*/
	session_start();
	class MYPDF extends TCPDF
	{
        public function Header()
		{
			$linkbd = conectar_v7();
			$linkbd -> set_charset("utf8");
			$sqlr = "SELECT * FROM configbasica WHERE estado='S'";
			$res = mysqli_query($linkbd,$sqlr);
			$row = mysqli_fetch_row($res);
			$nit = $row[0];
			$rs = $row[1];
			$consecutivo = $_POST['consecutivo'];
            $descrip="ACTA DE RECIBO N° $consecutivo";
			//Parte Izquierda
			$this->Image('imagenes/escudo.jpg',  13, 13, 20, 20, 'JPG', '', 'T', true, 300, '', false, false, 0, false, false, false);// Logo
			$this->SetFont('helvetica','B',8);
			$this->SetY(10);
			$this->RoundedRect(10, 10, 260, 25, 1,'');
			$this->Cell(0.1);
			$this->Cell(26,25,'','R',0,'L');
			$this->SetY(8);
			$this->SetX(80);
			$this->SetFont('helvetica','B',9);
			$this->Cell(145,15,strtoupper("$rs"),0,0,'C');
			$this->SetFont('helvetica','B',7);
			$this->SetY(12);
			$this->SetX(80);
			$this->Cell(145,15,'NIT: '.$nit,0,0,'C');
			//*****************************************************************************************************************************
			$this->SetFont('helvetica','B',9);
			$this->SetY(23);
			$this->SetX(36);
            $this->Cell(234,12,$descrip,'T',0,'C');
		}
        public function ColoredTable($data) {
            // Colors, line width and bold font
            $this->SetFillColor(222, 222, 222);
            $this->SetTextColor(000);
            //$this->SetDrawColor(128, 0, 0);
            $this->SetLineWidth(0.3);
            $this->SetFont('helvetica','B',8);
            // Header
            $w = array(65, 85, 45,65);
            $header =array(
                "Código",
                "Nombre",
                "Cantidad",
                "Valor",
            );
            $num_headers = count($header);
            for ($i=0; $i < $num_headers; $i++) {
                if($i == 1 || $i == 3){
                    $this->Cell($w[$i], 7, $header[$i], 0, 0, 'L', 1);
                }else{
                    $this->Cell($w[$i], 7, $header[$i], 0, 0, 'C', 1);
                }
            }
            $this->SetFont('helvetica','',8);
            $this->Ln();
            // Color and font restoration
            $this->SetFillColor(245,245,245);
            $this->SetTextColor(0);
            $this->SetFont('');
            // Data
            $fill = 0;
            $total = 0;
            foreach($data as $row) {
                $alturas = 7;
                $this->MultiCell($w[0],$alturas,$row['cod_articulo'],0,'C',$fill,0,'','',true,0,false,true,0,'M',true);
                $this->MultiCell($w[1],$alturas,$row['nom_articulo'],0,'L',$fill,0,'','',true,0,false,true,0,'M',true);
                $this->MultiCell($w[2],$alturas,$row['cantidad'],0,'C',$fill,0,'','',true,0,false,true,0,'M',true);
                $this->MultiCell($w[3],$alturas,'$'.number_format($row['valor'],2,",","."),0,'L',$fill,0,'','',true,0,false,true,0,'M',true);
                $total+=$row['valor'];
                $this->Ln();
                if($this->GetY()>170){
                    $this->AddPage();
                }
                $fill=!$fill;
            }
            $this->Cell(array_sum($w), 0, '', '');
            $this->Ln();
            $this->SetFont('helvetica','B',8);
            $this->SetFillColor(245,245,245);
            $this->Cell(array_sum($w)-($w[2]+$w[3]), 6, '', '', 0, 'R');
            $this->Cell($w[count($w)-2], 6, 'TOTAL:', '', 0, 'R');
            $this->Cell($w[count($w)-1], 6, '$'.number_format($total, 2), '', 0, 'L');
            $getY = $this->getY();
            if($getY >= 169){
                $this->addPage();
            }
        }
		public function Footer()
		{
			$linkbd = conectar_v7();
			$linkbd -> set_charset("utf8");
			$sql1="SELECT lema FROM interfaz01 ";
			$res1 = mysqli_query($linkbd,$sql1);
			$row1 = mysqli_fetch_row($res1);
			$lema = $row1[0];

            $user = $_SESSION['nickusu'];
			$fecha = date("Y-m-d H:i:s");
			$ip = $_SERVER['REMOTE_ADDR'];


			$sqlr = "SELECT direccion,telefono,web,email FROM configbasica WHERE estado='S'";
			$resp = mysqli_query($linkbd,$sqlr);
			$row = mysqli_fetch_row($resp);
			$direcc = $row[0];
			$telefonos = $row[1];
			$dirweb = $row[3];
			$coemail = $row[2];
			if($direcc!=''){$vardirec="Dirección: $direcc, ";}
			else {$vardirec="";}
			if($telefonos!=''){$vartelef="Telefonos: $telefonos";}
			else{$vartelef="";}
			if($dirweb!=''){$varemail="Email: $dirweb, ";}
			else {$varemail="";}
			if($coemail!=''){$varpagiw="Pagina Web: $coemail";}
			else{$varpagiw="";}
			//$this->SetY(-16);
			$this->SetFont('helvetica', 'I', 8);
			$txt = <<<EOD
            $lema
            $vardirec $vartelef
            $varemail $varpagiw
            EOD;
            $this->SetFont('helvetica', 'I', 6);
			$this->Cell(277,10,'','T',0,'T');
			$this->ln(2);
			$this->Write(0, $txt , '', 0, 'C', true, 0, false, false, 0);

            $sqlrcc="SELECT * FROM `usuarios` WHERE `cc_usu` = '$_SESSION[cedulausu]'";
            $respcc=mysqli_query($linkbd, $sqlrcc);
            $rowcc=mysqli_fetch_row($respcc);
            //echo $rowcc[1];

			$this->Cell(50, 10, 'Hecho por: '.$rowcc[1], 00, false, 'C', 0, '', 0, false, 'T', 'M');
			$this->Cell(60, 10, 'Impreso por: '.$user, 0, false, 'C', 0, '', 0, false, 'T', 'M');
			$this->Cell(50, 10, 'IP: '.$ip, 0, false, 'C',0, '', 0, false, 'T', 'M');
			$this->Cell(40, 10, 'Fecha: '.$fecha, 0, false, 'C', 0, '', 0, false, 'T', 'M');
			$this->Cell(30, 10, 'IDEAL.10 S.A.S', 0, false, 'C', 0, '', 0, false, 'T', 'M');
            $this->Cell(30, 10, 'Pagina '.$this->getAliasNumPage().'/'.$this->getAliasNbPages(), 0, false, 'C', 0, '', 0, false, 'T', 'M');
		}
	}
	$pdf = new MYPDF('L','mm','LETTER', true, 'UTF-8', false);// create new PDF document
	$pdf->SetDocInfoUnicode (true);
	// set document information
	$pdf->SetCreator(PDF_CREATOR);
	$pdf->SetAuthor('G&CSAS');
	$pdf->SetTitle('Certificados');
	$pdf->SetSubject('Certificado de Disponibilidad');
	$pdf->SetKeywords('TCPDF, PDF, example, test, guide');
	$pdf->SetMargins(10, 45, 10);// set margins
	$pdf->SetHeaderMargin(45);// set margins
	$pdf->SetFooterMargin(20);// set margins
	$pdf->SetAutoPageBreak(TRUE, 20);// set auto page breaks
	// set some language-dependent strings (optional)
	if (@file_exists(dirname(__FILE__).'/lang/spa.php'))
	{
		require_once(dirname(__FILE__).'/lang/spa.php');
		$pdf->setLanguageArray($l);
	}
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
	// ---------------------------------------------------------
	$pdf->AddPage();

	//Información entrada servicio
	$consecutivo = $_POST['consecutivo'];
	$sql1 = "SELECT fecha, user, certifica_alm FROM  alm_entrada_servicio_cab WHERE consecutivo = '$consecutivo'";
	$res1 = mysqli_query($linkbd,$sql1);
	$row1 = mysqli_fetch_row($res1);
	$dia1 = date('d',strtotime($row1[0]));
	$dmes1 = date('m',strtotime($row1[0]));
	$dmesl1 = mesletras((int)$dmes1);
	$ano1 = date('Y',strtotime($row1[0]));


	$descripcion1 = $_POST['descripcion'];

	//Información RP
	$fechadiv = explode('-', $_POST['fechaRP']);
	$vigenciarp = $fechadiv[0];
	$idrp = $_POST['rp'];
	$sql1 = "SELECT tercero, detalle, supervisor FROM  ccpetrp WHERE vigencia = '$vigenciarp' AND consvigencia='$idrp'";
	$res1 = mysqli_query($linkbd,$sql1);
	$row1 = mysqli_fetch_row($res1);

	$sqlNameAlm = "SELECT funcionario, nomcargo FROM firmaspdf_det WHERE idfirmas='6' AND estado ='S'";
	$rowNameAlm = mysqli_fetch_assoc(mysqli_query($linkbd, $sqlNameAlm));
	$funAlm = $rowNameAlm["funcionario"];
	$cargoAlm = $rowNameAlm["nomcargo"];

	$fulanito1 = buscatercero($row1[2]);;
	$documento1 = $row1[2];

	$fulanito2 = buscatercero($row1[0]);
	$documento2 = number_format($row1[0],2,',','.');

	$descripcion2 = $row1[1];

	$sql1 = "SELECT nombrecargo FROM  planaccargos AS T1 INNER JOIN planestructura_terceros AS T2 ON T1.codcargo = T2.codcargo WHERE T2.cedulanit = '$documento1' AND T1.estado = 'S'";
	$res1 = mysqli_query($linkbd,$sql1);
	$row1 = mysqli_fetch_row($res1);
	$cargof1 = $row1[0];

	$sql2 = "SELECT cod_articulo FROM alm_entrada_servicio_articulos WHERE consecutivo = '$consecutivo'";
	$res2 = mysqli_query($linkbd,$sql2);
	$totalarticulos = mysqli_num_rows($res2);

	if($totalarticulos > 0){
		$infoadicional = '<label> de la siguiente forma:</label>';
	}else{
		$infoadicional = '';
	}
	$pdf->ln();
	if ($row1[2] == 1) {
		$html = "
		<table>
			<tr>
				<td  style=\"width: 100%; font-size:9px; font-weight:normal; text-align: justify; text-justify: inter-word; \"><label>Yo, </label><label style=\"font-weight:bold;\">$funAlm</label><label> como </label><label style=\"font-weight:bold;\">$cargoAlm</label><label> y el supervisor $fulanito1 del </label><label style=\"text-transform:lowercase\">$descripcion1</label><label>, por medio del presente documento hago constar que </label><label style=\"font-weight:bold;\">$fulanito2</label><label>, se identifica con el nit $documento2 CUMPLIÓ a cabalidad con lo estipulado en el </label><label style=\"font-weight:bold;\">$descripcion1</label><label> con Objeto  </label><label style=\"font-weight:bold;\">$descripcion2</label>$infoadicional
				</td>
			</tr>
		</table>";
	}
	else {
		$html = "
		<table>
			<tr>
				<td  style=\"width: 100%; font-size:9px; font-weight:normal; text-align: justify; text-justify: inter-word; \"><label>Yo, </label><label style=\"font-weight:bold;\">$fulanito1</label><label> como </label><label style=\"font-weight:bold;\">supervisor</label><label> del </label><label style=\"text-transform:lowercase\">$descripcion1</label><label>, por medio del presente documento hago constar que </label><label style=\"font-weight:bold;\">$fulanito2</label><label>, se identifica con el nit $documento2 CUMPLIÓ a cabalidad con lo estipulado en el </label><label style=\"font-weight:bold;\">$descripcion1</label><label> con Objeto  </label><label style=\"font-weight:bold;\">$descripcion2</label>$infoadicional
				</td>
			</tr>
		</table>";
	}

	$pdf->writeHTML($html, true, false, true, false,'');

	if($totalarticulos > 0){
		$pdf->ln();
		$sql1 = "SELECT cod_articulo, nom_articulo, cantidad, valor FROM alm_entrada_servicio_articulos WHERE consecutivo = '$consecutivo' ORDER BY  CONVERT(cod_articulo, INT)";
		$res1 = mysqli_fetch_all(mysqli_query($linkbd,$sql1),MYSQLI_ASSOC);
        //dep($res1);exit;
        $pdf->ColoredTable($res1);
	}
	$pdf->ln();
	$v = $pdf->gety();
	if($v >= 251){
		$pdf->AddPage();
	}
	$pdf->SetFont('helvetica','',9);
	$pdf->SetX(19);
	if($dia1 == '01' || $dia1 == '1'){
		$rete = "el primer día";
	}else{
		$rete = "a los ".$dia1." días";
	}
    if($pdf->getY() > 110){
        $pdf->addPage();
    }
	$pdf->Cell(149,5,"Lo anterior se firma $rete del mes de $dmesl1 de $ano1",0,1,'L',0,'',0);
	$pdf->ln();
	$pdf->SetX(50);
	$pdf->SetFont('helvetica','',7);
	$pdf->Cell(80,5,"SUPERVISOR",0,0,'C',0,'',0);
	$pdf->SetX(150);
	$pdf->Cell(80,5,"QUIEN ENTREGA",0,0,'C',0,'',0);
	$pdf->ln(20);
	$pdf->SetX(50);
	$pdf->SetFont('helvetica','B',8);
	$pdf->Cell(80,5,$fulanito1,'T',0,'C',0,'',0);
	$pdf->SetX(150);
	$pdf->Cell(80,5,$fulanito2,'T',2,'C',0,'',0);
	$pdf->SetFont('helvetica','',8);
	$pdf->SetX(19);
	$pdf->Cell(138,5,$cargof1,0,0,'C',0,'',0);
	$pdf->SetX(150);
	$pdf->Cell(80,5,$documento2,0,1,'C',0,'',0);

    $sql1 = "SELECT certifica_alm FROM  alm_entrada_servicio_cab WHERE consecutivo = '$consecutivo'";
	$res1 = mysqli_query($linkbd,$sql1);
	$row1 = mysqli_fetch_row($res1);

	if($totalarticulos > 0){
		if ($row1[0] == 1) {
			$sqlr="SELECT funcionario, nomcargo FROM firmaspdf_det WHERE idfirmas='6' AND estado ='S'";
			$res=mysqli_query($linkbd,$sqlr);
			$row=mysqli_fetch_row($res);
			$fulanito3 = $row[0];
			$cargof3 = $row[1];
			$pdf->ln(16);
			$v = $pdf->gety();
			if($v>=251){
				$pdf->AddPage();
				$pdf->ln(20);
				$v=$pdf->gety();
			}
			$pdf->SetFont('helvetica','',8);
			$pdf->Cell(250,5,"QUIEN CERTIFICA",0,0,'C',0,'',0);
			$pdf->ln(20);
			$v = $pdf->gety();
			$pdf->Line(85,$v,180,$v);

			$pdf->SetFont('helvetica','B',8);
			$pdf->Cell(250,4,$fulanito3,0,1,'C',false,0,0,false,'T','C');
			$pdf->SetFont('helvetica','',8);
			$pdf->Cell(250,4,$cargof3,0,0,'C',false,0,0,false,'T','C');
		}
	}


	$pdf->Output('Acta de Recibo.pdf', 'I');
?>


