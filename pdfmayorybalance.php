<?php
	//V 1000 12/12/16 
	require('fpdf.php');
	require('comun.inc');
	require('funciones.inc');
	session_start();
	date_default_timezone_set("America/Bogota");
	//*****las variables con los contenidos***********
	//**********pdf*******
	//$pdf=new FPDF('P','mm','Letter'); 
	class PDF extends FPDF
	{
		//Cabecera de página
		function Header()
		{
    		$linkbd = conectar_v7();
			$linkbd -> set_charset("utf8");
			$sqlr="select *from configbasica where estado='S'";
			//echo $sqlr;
			$res=mysqli_query($linkbd, $sqlr);
			while($row=mysqli_fetch_row($res))
			{
				$nit=$row[0];
				$rs=$row[1];
 			}
	  		//Parte Izquierda
			$this->Image('imagenes/eng.jpg',23,13,25,25);
			$this->SetFont('Arial','B',12);
			$this->SetY(11);
			$this->RoundedRect(10, 10, 260, 31, 2.5,'' );
			$this->Cell(0.1);
			$this->Cell(50,31,'','R',0,'L'); 
			$this->SetY(10);
			$this->Cell(50.1);
			$this->Cell(200,5,''.$rs,0,0,'C'); 
			$this->SetFont('Arial','B',8);
			$this->SetY(15);
			$this->Cell(50.1);
			$this->Cell(200,5,'NIT: '.$nit,0,0,'C'); //Cuadro Izquierda
	
			//*****************************************************************************************************************************
			$this->SetFont('Arial','B',12);
			$this->SetY(15);
			$this->Cell(50.1);
			$this->Cell(200,21,'LIBRO MAYOR Y BALANCE',0,1,'C'); 
			$this->SetFont('Arial','B',10);
			$this->SetY(28);
			$this->Cell(50.1);
			$this->Cell(200,5,'PERIODO: '.$_POST['periodonom1'],0,0,'C');
			$this->SetY(35);
			$this->Cell(50.1);
			$this->Cell(200,5,'VIGENCIA: '.$_POST['anio'],0,0,'C');
	
	
			//********************************************************************************************************************************
			$this->line(10.1,42,269,42);
			$this->RoundedRect(10,43, 260, 5, 1.2,'' );	
			$this->SetFont('Arial','B',9);
			$this->SetY(43);
			$this->Cell(0.1);
			$this->Cell(25,5,'Cod. Cuenta ',0,0,'C'); 
			$this->SetY(43);
			$this->Cell(35);
			$this->Cell(25,5,'Nombre de la Cuenta',0,0,'C');
			$this->SetY(43);
        	$this->Cell(75.1);
			$this->Cell(25,5,'Saldo Anterior deb',0,0,'C');
			$this->Cell(10.1);
			$this->Cell(25,5,'Saldo Anterior Cred',0,0,'C');
			
			
        	$this->Cell(5);
			$this->Cell(25,5,'Debitos',0,0,'C');
        	$this->Cell(5);
			$this->Cell(25,5,'Creditos',0,0,'C');

			$this->Cell(5);
			$this->Cell(25,5,'Saldo Final deb',0,0,'C');
			$this->Cell(4);
			$this->Cell(30,5,'Saldo Final Cred',0,0,'C');
			$this->line(10.1,53,269,53);
			$this->ln(6);
			//***********************************************************************************************************************************
		}
		//Pie de página
		function Footer()
		{
			$this->SetY(-15);
			$this->SetFont('Arial','I',10);
			$this->Cell(0,10,'Pagina '.$this->PageNo().' de {nb}',0,0,'R'); // el parametro {nb} 
		}
	}

	//Creación del objeto de la clase heredada
	//$pdf=new PDF('P','mm',array(210,140));
	$pdf=new PDF('L','mm','Letter'); 
	$pdf->AliasNbPages();
	$pdf->AddPage();

	$pdf->SetFont('Times','',10);

	$pdf->SetAutoPageBreak(true,20);

	$pdf->SetY(54);   
	$con=0;

	//while ($con<15)
	while ($con<count($_POST['dcuentas']))
	{	
		if ($con%2==0)
		{
			$pdf->SetFillColor(255,255,255);
		}
    	else
		{
			$pdf->SetFillColor(245,245,245);
		}

		$pdf->Cell(22,4,''.$_POST['dcuentas'][$con],0,0,'L',1);//descrip

		$pdf->Cell(53,4,substr(''.$_POST['dncuentas'][$con],0,25),0,0,'L',1);//descrip

		$pdf->Cell(30,4,''.number_format($_POST['dsaldoantdeb'][$con],2),0,0,'R',1);//descrip

		$pdf->Cell(30,4,''.number_format($_POST['dsaldoantcred'][$con],2),0,0,'R',1);//descrip

		$pdf->Cell(30,4,''.number_format($_POST['ddebitos'][$con],2),0,0,'R',1);//descrip

		$pdf->Cell(30,4,''.number_format($_POST['dcreditos'][$con],2),0,0,'R',1);//descrip

    	$pdf->Cell(30,4,''.number_format($_POST['dsaldodeb'][$con],2),0,0,'R',1);//descrip

    	$pdf->Cell(35,4,''.number_format($_POST['dsaldocred'][$con],2),0,0,'R',1);//descrip

  		/* $pdf->Cell(31,4,''.number_format($_POST['dsaldo'][$con],2),0,0,'R',1); */
		$pdf->ln(4);	

		$con=$con+1;
	}
	$pdf->ln(6);	
	$pdf->SetFont('Times','B',8);

	$pdf->Cell(50,5,'Total','T',0,'R');
	$pdf->Cell(25,5,'','T',0,'R');
	$pdf->Cell(30,5,''.number_format($_POST['tsaldoantdeb'],2,".",","),'T',0,'R');
	$pdf->Cell(30,5,''.number_format($_POST['tsaldoantcred'],2,".",","),'T',0,'R');
	$pdf->Cell(30,5,''.number_format($_POST['tdebito'],2,".",","),'T',0,'R');
	$pdf->Cell(30,5,''.number_format($_POST['tcredito'],2,".",","),'T',0,'R');
	$pdf->Cell(30,5,''.number_format($_POST['tsaldofinaldeb'],2,".",","),'T',0,'R');
	$pdf->Cell(35,5,''.number_format($_POST['tsaldofinalcred'],2,".",","),'T',0,'R');

	$pdf->Output();
?> 


