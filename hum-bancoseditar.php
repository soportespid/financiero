<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require 'comun.inc';
	require 'funciones.inc';
	session_start();
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html lang="es">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="ie=edge"/>
		<title>:: IDEAL 10 - Gesti&oacute;n humana</title>
        <link href="css/css2.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="css/calendario.js"></script>
		<script type="text/javascript" src="css/programas.js"></script>
        <style>
			.onoffswitch 
			{
				position: relative; width: 71px;
				-user-select:none; 
				
			}
			.onoffswitch-checkbox {display: none;}
			.onoffswitch-label 
			{
				display: block; 
				overflow: hidden; 
				cursor: pointer;
				border: 2px solid #DDE6E2; 
				border-radius: 20px;
			}
			.onoffswitch-inner 
			{
				display: block; 
				width: 200%; 
				margin-left: -100%;
				transition: margin 0.3s ease-in 0s;
			}
			.onoffswitch-inner:before, .onoffswitch-inner:after 
			{
				display: block; 
				float: left; 
				width: 50%; 
				height: 23px; 
				padding: 0; 
				line-height: 23px;
				font-size: 14px; 
				color: white; 
				font-family: Trebuchet, Arial, sans-serif; 
				font-weight: bold;
				box-sizing: border-box;
			}
			.onoffswitch-inner:before 
			{
				content: "SI";
				padding-left: 10px;
				background-color: #51C3E0; 
				color: #FFFFFF;
			}
			.onoffswitch-inner:after 
			{
				content: "NO";
				padding-right: 10px;
				background-color: #EEEEEE; color: #999999;
				text-align: right;
			}
			.onoffswitch-switch 
			{
				display: block; 
				width: 17px; 
				margin: 3px;
				background: #FFFFFF;
				position: absolute; 
				top: 0; 
				bottom: 0;
				right: 44px;
				border: 2px solid #DDE6E2; 
				border-radius: 20px;
				transition: all 0.3s ease-in 0s; 
			}
			.onoffswitch-checkbox:checked + .onoffswitch-label .onoffswitch-inner {margin-left: 0;}
			.onoffswitch-checkbox:checked + .onoffswitch-label .onoffswitch-switch {right: 0px;}
		</style>
        <script>
			function despliegamodalm(_valor,_tip,mensa,pregunta)
			{
				document.getElementById("bgventanamodalm").style.visibility=_valor;
				if(_valor=="hidden"){document.getElementById('ventanam').src="";}
				else
				{
					switch(_tip)
					{
						case "1":	document.getElementById('ventanam').src="ventana-mensaje1.php?titulos="+mensa;break;
						case "2":	document.getElementById('ventanam').src="ventana-mensaje3.php?titulos="+mensa;break;
						case "3":	document.getElementById('ventanam').src="ventana-mensaje2.php?titulos="+mensa;break;
						case "4":	document.getElementById('ventanam').src="ventana-consulta1.php?titulos="+mensa+"&idresp="+pregunta;break;	
					}
				}
			}
			function funcionmensaje()
			{
				padg=document.getElementById('codban').value;
				document.location.href = "hum-bancoseditar.php?idban="+padg;
			}
			function respuestaconsulta(pregunta)
			{
				switch(pregunta)
				{
					case "1":	document.form2.oculto.value='2';
								document.form2.submit();
								break;
				}
			}
			function guardar()
			{
				var validacion01=document.getElementById('codban').value;
				var validacion02=document.getElementById('nomban').value;
				if (validacion01.trim()!='' && validacion02.trim()) {despliegamodalm('visible','4','Esta Seguro de Modificar','1');}
				else {despliegamodalm('visible','2','Falta información para crear el banco');}
			}
			function cambiocheck()
			{
				if(document.getElementById('myonoffswitch').value=='S'){document.getElementById('myonoffswitch').value='N';}
				else{document.getElementById('myonoffswitch').value='S';}
				document.form2.submit();
			}
			function validacodigo()
			{
				document.getElementById('validaco').value=1;
				document.form2.submit();
			}
		</script>
		<?php titlepag();?>
	</head>
	<body>
		<IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
		<span id="todastablas2"></span>
		<table>
			<tr><script>barra_imagenes("hum");</script><?php cuadro_titulos();?></tr>	 
			<tr><?php menu_desplegable("hum");?></tr>
			<tr>
				<td colspan="3" class="cinta">
					<a href='hum-bancos.php' class="mgbt"><img src="imagenes/add.png" title="Nuevo"/></a>
					<a onClick="guardar()" class="mgbt"><img src="imagenes/guarda.png" title="Guardar"/></a>
					<a href='hum-bancosbuscar.php' class="mgbt"><img src="imagenes/busca.png" title="Buscar"/></a>
					<a onClick="mypop=window.open('plan-agenda.php','','');mypop.focus()" class="mgbt"><img src="imagenes/agenda1.png" title="Agenda"/></a>
					<a onClick="mypop=window.open('cont-principal.php','','');mypop.focus();" class="mgbt"><img src="imagenes/nv.png" title="Nueva ventana"></a>
					<a onClick="mypop=window.open('<?php echo $url2; ?>','','');mypop.focus();" class="tooltip bottom mgbt"><img src="imagenes/duplicar_pantalla.png" title="Duplicar pesta&ntilde;a"></a>
					<a href='hum-bancosbuscar.php' class="mgbt"><img src="imagenes/iratras.png" title="Atr&aacute;s"/></a>
				</td>
			</tr>
		</table>
		<div id="bgventanamodalm" class="bgventanamodalm">
			<div id="ventanamodalm" class="ventanamodalm">
				<IFRAME src="" name="ventanam" marginWidth=0 marginHeight=0 frameBorder=0 id="ventanam" frameSpacing=0 style=" width:700px; height:130px; top:200; overflow:hidden;"> 
				</IFRAME>
			</div>
		</div>
		<form name="form2" method="post" action="">
			<?php 
				if($_POST['oculto']=="")
				{
					$sqlr="SELECT * FROM hum_bancosfun WHERE codigo='$_GET[idban]'";
					$resp = mysqli_query($linkbd,$sqlr);
					$row =mysqli_fetch_row($resp); 
					$_POST['codban']=$_POST['codaux']=$row[0];
					$_POST['nomban']=$row[1];		
					$_POST['onoffswitch']=$row[2];	
					$accion="INGRESO A EDITAR BANCO No $row[0]";
					$origen=getUserIpAddr();
					generaLogs($_SESSION["nickusu"],'HUM','V',$accion,$origen);		
				}
				if($_POST['validaco']=="1")
				{	if($_POST['codaux']!=$_POST['codban'])
					{
						$sqlr="SELECT codigo FROM hum_bancosfun WHERE codigo='$_POST[codban]'";
						$resultado=mysqli_query($linkbd,$sqlr);
						if (mysqli_num_rows($resultado)>0)
						{
							echo"<script>despliegamodalm('visible','2','Ya existe este código de banco Nº $_POST[codban]');</script>";
							$_POST['validaco']="0";
							$_POST['codban']=$_POST['codaux'];
						} 
					}
				}
			?>
			<table class="inicio">
				<tr>
					<td class="titulos" colspan="4">.: Editar Banco</td>
					<td class="cerrar" style="width:7%" onClick="location.href='hum-principal.php'">Cerrar</td>
				</tr>
				<tr>
					<td class="saludo1" style="width:3cm;">C&oacute;digo:</td>
					<td style="width:15%;"><input type="text" name="codban" id="codban" onKeyUp="return tabular(event,this)" value="<?php echo $_POST['codban'];?>" style="width:100%"  onChange="validacodigo();"/></td>
					<td class="saludo1" style="width:3cm;">Nombre:</td>
					<td ><input type="text" name="nomban" id="nomban" value="<?php echo $_POST['nomban'];?>" style="width:100%;text-transform:uppercase"/></td>
				</tr>
				<tr>
					<td class="saludo1">C. Nacional:</td>
					<td>
						<div class="onoffswitch">
							<input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox" id="myonoffswitch" value="<?php echo $_POST['onoffswitch'];?>" <?php if($_POST['onoffswitch']=='S'){echo "checked";}?> onChange="cambiocheck();"/>
							<label class="onoffswitch-label" for="myonoffswitch">
								<span class="onoffswitch-inner"></span>
								<span class="onoffswitch-switch"></span>
							</label>
						</div>
					</td>
				</tr>
			</table>
			<input type="hidden" name="oculto" id="oculto" value="1"/>
			<input type="hidden" name="validaco" id="validaco" value="0"/>
			<input type="hidden" name="codaux" id="codaux" value="<?php echo $_POST['codaux'];?>"/>
			<?php
				if($_POST['oculto']=="2")
				{
					$sqlr ="UPDATE hum_bancosfun SET codigo='$_POST[codban]',nombre='$_POST[nomban]' WHERE codigo='$_POST[codaux]'";
					if (!mysqli_query($linkbd,$sqlr))
					{
						//$e =mysqli_error(mysqli_query($linkbd,$sqlr));
						echo"<script>despliegamodalm('visible','2','No se pudo ejecutar la petición: $e');</script>";
					}
					else 
					{
						$accion="MODIFICO BANCO: $sqlr";
						$origen=getUserIpAddr();
						generaLogs($_SESSION["nickusu"],'HUM','M',$accion,$origen);
						echo "<script>despliegamodalm('visible','1','Se ha modificado con éxito');</script>";
					}
				}
			?>
		</form>
		<div id="bgventanamodal2">
			<div id="ventanamodal2">
				<IFRAME  src="" name="buster" marginWidth=0 marginHeight=0 frameBorder=0 id="ventana2" frameSpacing=0 style="left:500px; width:900px; height:500px; top:200;"></IFRAME>
			</div>
		</div>
    </body>
</html>
