<?php
	header("Cache-control: private"); // Arregla IE 6
	header("Content-Type: text/html;charset=iso-8859-1");
	require"comun.inc";
	require"funciones.inc";
	require 'funcionessp.inc';
	session_start();
	$linkbd=conectar_bd();	
	cargarcodigopag($_GET[codpag],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es">
	<head>
	 	<meta http-equiv="Content-type" content="text/html; charset=iso-8859-1"/>
        <meta http-equiv="X-UA-Compatible" content="IE=9"/><title>:: SPID - Servicios Publicos</title>
        <link href="css/css2.css" rel="stylesheet" type="text/css" />
        <link href="css/css3.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="css/programas.js"></script>
		<script type="text/javascript" src="css/calendario.js"></script>
		<script>
			function buscacta(e)
 			{
				if (document.form2.cuenta.value!="")
				{
 					document.form2.bc.value='1';
					document.form2.submit();
 				}
 			}
			function validar(){document.form2.submit();}
			function agregardetalle()
			{
				if(document.form2.medidor.value!=""  )
				{ 
					document.form2.agregadet.value=1;
					document.form2.submit();
 				}
 				else {alert("Falta informacion para poder Agregar");}
			}
			function eliminar(variable)
			{
				if (confirm("Esta Seguro de Eliminar"))
  				{
					document.form2.elimina.value=variable;
					vvend=document.getElementById('elimina');
					vvend.value=variable;
					document.form2.submit();
				}
			}
			function guardar()
			{
				if ( document.form2.tercero.value!='' && document.form2.estrato.value!='')
				{		
						digcod=document.form2.codcat.value;
						if (digcod.length!=15 && digcod.length!=0)
						{despliegamodalm('visible','2','El codigo ingresado tiene menos de 15 digitos');}
						else
						{
							if (confirm("Esta Seguro de Guardar"))
							{
								document.form2.oculto.value=2;
								document.form2.submit();
							}
						}
				}
				else
				{
					alert('Faltan datos para completar el registro');
					document.form2.tercero.focus();
					document.form2.tercero.select();
				}
			}
			function buscater(e)
 			{
				if (document.form2.tercero.value!="")
				{
 					document.form2.bt.value='1';
 					document.form2.submit();
 				}
 			}
			function buscar()
 			{
 				document.form2.buscav.value='1';
 				document.form2.submit();
 			}
			function despliegamodal2(_valor,_num)
			{
				document.getElementById("bgventanamodal2").style.visibility=_valor;
				if(_valor=="hidden"){document.getElementById('ventana2').src="";}
				else 
				{
					switch(_num)
					{
						case '1':	document.getElementById('ventana2').src="tercerosgral-ventana04.php?objeto=tercero&nobjeto=ntercero&nfoco=direccion&valsub=SI&ndid=idterc"; break;
						case '2':	document.getElementById('ventana2').src="catastral-ventana01.php";break;
						case '3':	document.getElementById('ventana2').src="medidores-ventana01.php";break;
					}
				}
			}
			function despliegamodalm(_valor,_tip,mensa,pregunta)
			{
				document.getElementById("bgventanamodalm").style.visibility=_valor;
				if(_valor=="hidden"){document.getElementById('ventanam').src="";}
				else
				{
					switch(_tip)
					{
						case "1":	document.getElementById('ventanam').src="ventana-mensaje1.php?titulos="+mensa;break;
						case "2":	document.getElementById('ventanam').src="ventana-mensaje3.php?titulos="+mensa;break;
						case "3":	document.getElementById('ventanam').src="ventana-mensaje2.php?titulos="+mensa;break;
						case "4":	document.getElementById('ventanam').src="ventana-consulta1.php?titulos="+mensa+"&idresp="+pregunta;break;	
					}
				}
			}
			function funcionmensaje()
			{
				var _cons=document.getElementById('egreso').value;
				document.location.href = "teso-girarchequesver.php?idegre="+_cons+"&scrtop=0&numpag=1&limreg=10&filtro1=&filtro2=";
			}
			function respuestaconsulta(pregunta)
			{
				switch(pregunta)
				{
					case "1":	document.form2.oculto.value='2';document.form2.submit();break;
				}
			}
			function validar(){document.form2.submit();}
		</script>
		<?php titlepag();?>
	</head>
	<body>
		<IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
		<span id="todastablas2"></span>
		<table>
			<tr><script>barra_imagenes("serv");</script><?php cuadro_titulos();?></tr>	 
			<tr><?php menu_desplegable("serv");?></tr>
			<tr>
  				<td colspan="3" class="cinta"><img src="imagenes/add.png" title="Nuevo" onClick="location.href='serv-predios_basico.php'" class="mgbt"/><img src="imagenes/guarda.png" title="Guardar" onClick="guardar();" class="mgbt"/><img src="imagenes/busca.png" title="Buscar" onClick="location.href='serv-buscapredios_basico.php'" class="mgbt"/><img src="imagenes/nv.png" title="Nueva Ventana" onClick="mypop=window.open('serv-principal.php','','');mypop.focus();" class="mgbt"/></a></td>
			</tr>		  
		</table>
        <div id="bgventanamodalm" class="bgventanamodalm">
            <div id="ventanamodalm" class="ventanamodalm">
                <IFRAME src="" name="ventanam" marginWidth=0 marginHeight=0 frameBorder=0 id="ventanam" frameSpacing=0 style=" width:700px; height:130px; top:200; overflow:hidden;"> 
                </IFRAME>
            </div>
        </div>
        <form  name="form2" method="post" action="">
			<?php
				$vigencia=date(Y);
				if(!$_POST[oculto])
				{
 					$sqlr="select * from servclientes where codigo='$_GET[idr]'";
	 				$resp=mysql_query($sqlr,$linkbd);
					while($r=mysql_fetch_row($resp))
	  				{
						$_POST[prefijo]=$r[1];
						$_POST[consecadd]=$r[0];
						$_POST[catastral]=$r[2];
						$_POST[codanterior]=$r[6];		
						$_POST[tercero]=$r[5];
						$_POST[terceroact]=$r[5];
						$divtercero=buscaterceroid($r[5]);	
						$_POST[ntercero]=$divtercero[0];
						$_POST[idterc]=$divtercero[1];
						$_POST[nomcliente]=$r[12];
						$_POST[direccion]=$r[4];
						$_POST[barrios]=$r[8];
						$_POST[estrato]=$r[3];
						$_POST[fecha]=$r[7];
						$_POST[zona]=$r[13];
						$_POST[lado]=$r[14];
						$sqlr="SELECT * FROM servmedidores where CLIENTE='$r[0]'";
						$res=mysql_query($sqlr,$linkbd);
						while($rowEmp =mysql_fetch_row($res))
						{
							$_POST[dcodmedidor][]=$rowEmp[0];	
							if($rowEmp[0]!='')
							{
								$sqlr="SELECT * FROM servanomalias where id='$r[10]' order by id DESC";
								$resm=mysql_query($sqlr,$linkbd);
								$rowm = mysql_fetch_row($resm);		
								$_POST[ddestmed][]=$rowm[1];
								$_POST[ddestmedc][]=$r[10];
								$sqlr="SELECT * FROM servmedidores_servicios where codigo='$rowEmp[0]'";
								$resr=mysql_query($sqlr,$linkbd);
								$servs=" ";
								while($rowsr =mysql_fetch_row($resr)){$servs.=$rowsr[1]." ";}
								$_POST[dservmed][]=$servs;	
							}
						}
	  				}	 	  
	   				$sqlr="SELECT * FROM terceros_servicios where consecutivo='$_POST[consecadd]' AND ESTADO='S' order by servicio";
					$resp=mysql_query($sqlr,$linkbd);
					while($rowEmp =mysql_fetch_row($resp))
					{
						$_POST[servicios][]=$rowEmp[3];
						if($rowEmp[7] == NULL or $rowEmp[7] == ""){$_POST[promval][]=0;}
						else {$_POST[promval][]=$rowEmp[7];}
						if($rowEmp[8] == NULL or $rowEmp[8] == ""){$_POST[saldoval][]=0;}
						else {$_POST[saldoval][]=$rowEmp[8];}
					}
				}
				if ($_POST[chacuerdo]=='2')
				{
				    $_POST[dcuentas]=array();
				    $_POST[dncuetas]=array();
				    $_POST[dingresos]=array();
				    $_POST[dgastos]=array();
					$_POST[diferencia]=0;
					$_POST[cuentagas]=0;
					$_POST[cuentaing]=0;																			
				}	
				if($_POST[bt]=='1')
			 	{
					$divtercero=buscaterceroid($_POST[tercero]);	
					
			  		if($divtercero[0]!='')
					{
						$_POST[ntercero]=$divtercero[0];
						$_POST[idterc]=$divtercero[1];
					}
			 		else {$_POST[ntercero]=$_POST[idterc]="";}
				}
				if($_POST[buscav]=='1')
 				{
	 				$_POST[prediocon]=array();
	 				$_POST[terceros]=array();
					$_POST[fechain]=array();		 
					$_POST[fechafin]=array();
					$_POST[descrips]=array();
					$_POST[estados]=array();
					$_POST[modificar]=array();	  
	 				$sqlr="select *from tesopredios where cedulacatastral='$_POST[codcat]' ";
	 				$res=mysql_query($sqlr,$linkbd);
					$colbus=mysql_num_rows($res);
					if($colbus>0)
					{
						while($row=mysql_fetch_row($res))
						{
							$_POST[catastral]=$row[0];
							$_POST[ntercerop]=$row[6];
							$_POST[tercerop]=$row[5];
							$_POST[direccion]=$row[7];
							$_POST[ha]=$row[8];
							$_POST[mt2]=$row[9];
							$_POST[areac]=$row[10];
							$_POST[avaluo]=number_format($row[11],2);
							$_POST[tipop]=$row[14];
							if($_POST[tipop]=='urbano'){$_POST[estrato]=$row[15];}
							else {$_POST[rangos]=$row[15];}
							$_POST[dtcuentas][]=$row[1];		 
							$_POST[dvalores][]=$row[5];
							$_POST[buscav]="";
						}
						//**** buscar terceros  ****
						$sqlr="select *from terceros_servicios where codcatastral='$_POST[codcat]' order by consecutivo asc";
						$res=mysql_query($sqlr,$linkbd);
						$_POST[ncons]=-1;
						while($row=mysql_fetch_row($res))
						{
							$nter=buscatercero($row[0]); 
							$_POST[prediocon][]="$row[1]-$row[2]";
							$_POST[terceros][]=$row[0];
							$_POST[servact][]=$row[3];		 
							$_POST[fechain][]=$row[4];
							$_POST[fechafin][]=$row[5];
							$_POST[descrips][]="REGISTRADO";
							$_POST[estados][]=$row[6];
							$_POST[ncons]=$row[2];
						}
					}
					else
					{
						$digcod=strlen($_POST[codcat]);
						if ($digcod!=15 && $digcod!=0)
						{echo"<script>despliegamodalm('visible','2','El codigo ingresado tiene menos de 15 digitos');</script>";}
						
					}
  				}
			?>
    		<table class="inicio" align="center" >
                <tr>
                    <td class="titulos" colspan="9">.: Crear Clientes</td>
                    <td class="cerrar" style="width:7%" onClick="location.href='serv-principal.php'">Cerrar</td>
                </tr>
				<?php
  			  		if($_POST[bt]=='1')
			 		{				 			
			 			$nresul=buscatercero($_POST[tercero]);			 
			  			if($nresul!='')
			   			{
			  				$_POST[ntercero]=$nresul;
   							echo"<script>document.getElementById('agrega').focus();document.getElementById('agrega').select();</script>";
			  			}
			 			else
			 			{
			 				$_POST[ntercero]="";
			  				echo"<script>alert('Tercero Incorrecto o no Existe');document.form2.tercero.focus();</script>";
			  			}
			 		}
				?>					
				<tr>
                	<td class="saludo1">C&oacute;digo Cliente:</td>
                    <td style='width:12%'><input type="text" name="prefijo" id="prefijo" value="<?php echo $_POST[prefijo]; ?>" style='text-align:right;width:30%' readonly/><input type="text" name="consecadd" id="consecadd"  value="<?php echo $_POST[consecadd]; ?>" style='width:65%' readonly/></td>
     				<td  class="saludo1" style='width:3cm'>C&oacute;digo Catastral:</td>
	 				<td style='width:20%' colspan="2"><input type="text" name="catastral" id="catastral" onBlur="buscater(event)" onClick="document.getElementById('catastral').focus();document.getElementById('catastral').select();" onKeyUp="return tabular(event,this)" value="<?php echo $_POST[catastral]?>" style='width:100%' readonly></td>             
                    <td  class="saludo1">Nuevo C&oacute;digo:</td>
          			<td style='width:15%'><input type="text" id="codcat" name="codcat" onKeyUp="return tabular(event,this)" onBlur="buscar(event)" value="<?php echo $_POST[codcat]?>" onClick="document.getElementById('codcat').focus();document.getElementById('codcat').select();" style='width:80%'/>&nbsp;<img class="icobut" src="imagenes/find02.png"  title="Listado C�digos Catastrales" onClick="despliegamodal2('visible','2');"/></a></td>
                      <td class="saludo1">C&oacute;digo Anterior:</td>
                    <td ><input type="text" name="codanterior" id="codanterior" value="<?php echo $_POST[codanterior];?>" style='width:100%'/></td>
                </tr>
                <tr>
					<td class="saludo1">Tercero:</td>
          			<td><input type="text" id="tercero" name="tercero" onKeyUp="return tabular(event,this)" onBlur="buscater(event)" value="<?php echo $_POST[tercero]?>" onClick="document.getElementById('tercero').focus();document.getElementById('tercero').select();" style='width:80%'/>&nbsp;<img class="icobut" src="imagenes/find02.png"  title="Listado Terceros"  onClick="despliegamodal2('visible','1');"/></a></td>
                    <td colspan="2"><input type="text" name="ntercero" id="ntercero" value="<?php echo $_POST[ntercero]?>" style='width:100%' readonly/></td>
                    <td style="width:5%">
						<?php
                            if($_POST[ntercero]==""){$editer=" class='icobut1' src='imagenes/usereditd.png'";}
                            else{$editer=" class='icobut' src='imagenes/useredit.png' onClick=\"mypop=window.open('serv-editaterceros.php?idter=$_POST[idterc]','','');mypop.focus();\"";}
                        ?>
                    	<img class="icobut" src="imagenes/usuarion.png" title="Crear Tercero" onClick="mypop=window.open('serv-terceros.php','','');mypop.focus();"/>&nbsp;<img <?php echo $editer; ?> title="Editar Tercero" />
                  	</td>
					<td class="saludo1">Cliente:</td>  
                    <td colspan="3"><input type="text" name="nomcliente" id="nomcliente" value="<?php echo $_POST[nomcliente]?>" style='width:100%' readonly/></td>
            	</tr>
	  			<tr> 
	  				<td class="saludo1">Direccion:</td>
	  				<td colspan="4"><input type="text" name="direccion" id="direccion" onKeyUp="return tabular(event,this)" value="<?php echo $_POST[direccion]?>" style='width:100%'/></td>
         			<td class="saludo1">Barrio</td>
                    <td>      
      					<select name="barrios" style='text-transform:uppercase;width:100%'>
       						<option value="">Seleccione ...</option>
            				<?php
								$sqlr="select *from servbarrios where estado='S'";
								$res=mysql_query($sqlr,$linkbd);
								while ($row =mysql_fetch_row($res)) 
				   	 			{
					 				if($row[0]==$_POST[barrios]){echo "<option value='$row[0]' SELECTED>$row[0] $row[1]</option>";}
					 				else {echo "<option value='$row[0]'>$row[0] $row[1]</option>";}	 	 
								}	 	
							?>            
						</select>  
      				</td>			 
					<td class="saludo1">Zona:</td>
                    <td>      
      					<select name="zona" style='text-transform:uppercase'>
       						<option value="">Seleccione ...</option>
            				<?php
								$sqlr="select *from servzonas where estado='S'";
								$res=mysql_query($sqlr,$linkbd);
								while ($row =mysql_fetch_row($res)) 
				    			{
					 				if($row[0]==$_POST[zona]){echo "<option value='$row[0]' SELECTED>$row[0] $row[1]</option>";}
					 				else{echo "<option value='$row[0]'>$row[0] $row[1]</option>";} 	 
								}	 	
							?>            
						</select>  
      				</td>
           		</tr>
                <tr>
	  				<td class="saludo1">Lado:</td>
                    <td>      
      					<select name="lado" style='width:95%'>
       						<option value="">Seleccione ...</option>
							<option value="A" <?php  if($_POST[lado]=='A') echo "Selected"?>>Lado A</option>
							<option value="B" <?php  if($_POST[lado]=='B') echo "Selected"?>>Lado B</option>
							<option value="C" <?php  if($_POST[lado]=='C') echo "Selected"?>>Lado C</option>
							<option value="D" <?php  if($_POST[lado]=='D') echo "Selected"?>>Lado D</option>                    
						</select>  
      				</td>
                    <td class="saludo1">Estratos:</td>
                    <td colspan="2">
                        <select name="estrato" style='text-transform:uppercase;width:100%'>
                            <option value="">Seleccione ...</option>
                            <?php
                                $sqlr="select *from servestratos where estado='S'";
                                $res=mysql_query($sqlr,$linkbd);
                                while ($row =mysql_fetch_row($res)) 
                                {
                                    if($row[0]==$_POST[estrato])
                                    {
                                        echo "<option value='$row[0]' SELECTED>$row[2] - $row[1]</option>";
                                        $_POST[nestrato]=$row[1];
                                    }
                                    else {echo "<option value='$row[0]'>$row[2] - $row[1]</option>";}
                                }	 	
                            ?>            
                        </select>  
                        <input type="hidden" value="<?php echo $_POST[nestrato]?>" name="nestrato">
                    </td> 
                    <td class="saludo1">Fecha Registro:</td>
                    <td><input name="fecha" type="date" value="<?php echo $_POST[fecha]?>" maxlength="10" onKeyPress="javascript:return solonumeros(event)" onKeyUp="return tabular(event,this)"  id="fc_1198971545" onKeyDown="mascara(this,'/',patron,true)" title="DD/MM/YYYY"></td> 
                </tr> 
                <tr>
                    <td class="saludo1" >Medidor:</td>
                    <td><input type="text" name="medidor" value="<?php echo $_POST[medidor] ?>" style='width:80%' readonly/>&nbsp;<img class="icobut" src="imagenes/find02.png"  title="Listado Medidores"  onClick="despliegamodal2('visible','3');"></td>
                    <td class="saludo1">Estado Medidor</td>
                    <td colspan="2"><input type="text" name="estadomed" value="<?php echo $_POST[estadomed]?>" style='width:100%' readonly></td>
                    <input type="hidden" name="estadomedoc" value="<?php echo $_POST[estadomedoc]?>"  readonly>
                    <td><input type="button" name="agregamedidor" value="Agregar Medidor" onClick="agregardetalle()"></td>
                    <input type="hidden" value="0" name="agregadet">
                </tr>
            </table>
            <input type="hidden" value="<?php echo $_POST[nbanco]?>" name="nbanco">
            <input type="hidden" name="terceroact" id="terceroact" value="<?php echo $_POST[terceroact]?>"/>
			<input type="hidden" name="bt" value="0"/> 
			<input type="hidden" name="ncons" value="<?php $_POST[ncons]?>"/>
            <input type="hidden" name="chacuerdo" value="1"/>
            <input type="hidden" name="oculto" id="oculto" value="1"/> 
			<input type="hidden" value="<?php echo $_POST[buscav]?>" name="buscav"/>
            <input type="hidden" name="idterc" id="idterc" value="<?php echo $_POST[idterc];?>"/>
            <table class="iniciop">
                <tr>
                    <td class="titulos">Codigo Medidor</td>
                    <td class="titulos">Estado Medidor</td>
                    <td class="titulos">Servicios</td>
                    <td class="titulos"><img src="imagenes/del.png" ><input type='hidden' name='elimina' id='elimina'></td>
                </tr>
                <?php
                    if ($_POST[elimina]!='')
                    { 
                        $posi=$_POST[elimina];		  
                        unset($_POST[dcodmedidor][$posi]);	
                        unset($_POST[ddestmed][$posi]);			 
                        unset($_POST[dservmed][$posi]);			 		 
                        unset($_POST[ddestmedc][$posi]);	
                        $_POST[dcodmedidor]= array_values($_POST[dcodmedidor]); 		 
                        $_POST[ddestmed]= array_values($_POST[ddestmed]); 		 		 
                        $_POST[dservmed]= array_values($_POST[dservmed]); 
                        $_POST[ddestmedc]= array_values($_POST[ddestmedc]); 		 
                    }
                    if ($_POST[agregadet]=='1')
                    {
                        $_POST[dcodmedidor][]=$_POST[medidor];
                        $_POST[ddestmed][]=$_POST[estadomed];	
                        $_POST[dservmed][]=buscarmedidor_servicios($_POST[medidor]);
                        $_POST[ddestmedc][]=$_POST[estadomedoc];		
                        $_POST[agregadet]=0;
                    }
                    for ($x=0;$x<count($_POST[dcodmedidor]);$x++)
                    {		 
                        echo "
                        <tr>
                            <input type='hidden' name='dcodmedidor[]' value='".$_POST[dcodmedidor][$x]."'/>
                            <input type='hidden' name='ddestmed[]' value='".$_POST[ddestmed][$x]."'/>
                            <input type='hidden' name='ddestmedc[]' value='".$_POST[ddestmedc][$x]."'/>
                            <input type='hidden' name='dservmed[]' value='".$_POST[dservmed][$x]."'/>
                            <td class='saludo1'>".$_POST[dcodmedidor][$x]."</td>
                            <td class='saludo1'>".$_POST[ddestmed][$x]."</td>
                            <td class='saludo1'>".$_POST[dservmed][$x]."</td>
                            <td class='saludo1'><a href='#' onclick='eliminar($x)'><img src='imagenes/del.png'></a></td>
                        </tr>";
                    }
                ?>	 
            </table>       
			<?php
                if($_POST[oculto]=='2')
                {
                    $fechafin=date('Y-m-d');
                    $fechaf=$_POST[fecha];
					$preg01=$preg02=$preg03=$preg04=$preg05=$preg06=$preg07="";
					if($_POST[codcat]!=""){$preg01=", codcatastral='$_POST[codcat]', codigo_anterior='$_POST[catastral]'";}
					if($_POST[direccion]!=""){$preg02=", direccion='$_POST[direccion]'";}
					if($_POST[ntercero]!=""){$preg03=", terceroactual='$_POST[tercero]', nombretercero='$_POST[ntercero]'";}
					if($_POST[barrios]!=""){$preg04=", barrio='$_POST[barrios]'";}
					if($_POST[estrato]!=""){$preg05=", estrato='$_POST[estrato]'";}
					if($_POST[zona]!=""){$preg06=", zona='$_POST[zona]'";}
					if($_POST[lado]!=""){$preg07=", lado='$_POST[lado]'";}
                    $sqlr="UPDATE servclientes SET fechacreacion='$fechaf'$preg01$preg02$preg03$preg04$preg05$preg06$preg07 WHERE codigo='$_POST[consecadd]'";		
                    if (!mysql_query($sqlr,$linkbd))
                    {
                        echo "<table class='inicio'><tr><td class='saludo1'><center><font color=blue><img src='imagenes\alert.png'> Manejador de Errores de la Clase BD<br><font size=1></font></font><br><p align=center>No se pudo ejecutar la petici�n: <br><font color=red><b>$sqlr</b></font></p>";	
                    }
                    else
                    {
						if($_POST[ntercero]!="")
						{
                        	$sqlr="update terceros_servicios set cedulanit='$_POST[tercero]', codcatastral='$_POST[catastral]' where consecutivo='$_POST[consecadd]'";
                       		mysql_query($sqlr,$linkbd);
						}
                        echo "<div class='saludo2'><div class='saludo1'><img src='imagenes\confirm.png'>Se ha Actualizado con Exito</div></div>";
                    }	 	 
                }
            ?>	
		</form>
        <div id="bgventanamodal2">
			<div id="ventanamodal2">
				<IFRAME  src="" name="buster" marginWidth=0 marginHeight=0 frameBorder=0 id="ventana2" frameSpacing=0 style="left:500px; width:900px; height:500px; top:200;"></IFRAME>
        	</div>
		</div>
	</body>
</html>