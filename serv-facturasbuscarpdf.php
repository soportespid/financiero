<?php
	require_once("tcpdf2/tcpdf_include.php");
	require('comun.inc');
	require"funciones.inc";
	session_start();
	class MYPDF extends TCPDF 
	{
		public function Header() 
		{
			$linkbd=conectar_bd();
			$sqlr="SELECT nit, razonsocial FROM configbasica WHERE estado='S'";
			$resp=mysql_query($sqlr,$linkbd);
			while($row=mysql_fetch_row($resp)){$nit=$row[0];$rs=utf8_encode(strtoupper($row[1]));}
			$this->Image('imagenes/eng.jpg', 25, 10, 25, 23.9, 'JPG', '', 'T', true, 300, '', false, false, 0, false, false, false);// Logo
			$this->SetFont('helvetica','B',8);
			$this->SetY(10);
			$this->RoundedRect(10, 10, 265, 31, 2.5,''); //Borde del encabezado
			$this->Cell(52,31,'','R',0,'L'); //Linea que separa el encabazado verticalmente
			$this->SetY(32.5);
			$this->Cell(52,5,''.$rs,0,0,'C',false,0,1,false,'T','B'); //Nombre Municipio
			$this->SetFont('helvetica','B',8);
			$this->SetY(36.5);
			$this->Cell(52,5,''.$nit,0,0,'C',false,0,1,false,'T','C'); //Nit
			$this->SetFont('helvetica','B',14);
			$this->SetY(10);
			$this->SetX(62);
			$this->Cell(175,17,'LISTADO FACTURACION',0,0,'C'); 
			$this->SetFont('helvetica','I',10);
			$this->SetY(27);
			$this->SetX(62);
			$this->Cell(175,7,"",'T',0,'L',false,0,1); 
			$this->SetY(31.2);
			$this->SetX(62);
			$this->Cell(175,7,"",0,0,'L',false,0,1);
			$this->SetFont('helvetica','B',9);
			$this->SetY(10);
			$this->SetX(237);
			$this->Cell(37.8,30.7,'','L',0,'L');
			$this->SetY(29);
			$this->SetX(237.5);
			$this->Cell(35,5," FECHA: ".date("d-m-Y"),0,0,'L');
			$this->SetY(34);
			$this->SetX(237.5);
			$this->Cell(35,5," HORA: ".date('h:i:s a'),0,0,'L');
			//-----------------------------------------------------
			$this->SetY(44);
			$this->Cell(20,5,'Factura',1,0,'C',false,0,0,false,'T','C');
			$this->Cell(15,5,'Corte',1,0,'C',false,0,0,false,'T','C');
			$this->Cell(15,5,'Vigencia',1,0,'C',false,0,0,false,'T','C');
			$this->Cell(21,5,'Codigo Usu.',1,0,'C',false,0,0,false,'T','C');
			$this->Cell(70,5,'Propietario',1,0,'C',false,0,0,false,'T','C');
			$this->Cell(70,5,'Dirección',1,0,'C',false,0,0,false,'T','C');
			$this->Cell(27,5,'Valor',1,0,'C',false,0,0,false,'T','C');
			$this->Cell(27,5,'Abonos',1,0,'C',false,0,0,false,'T','C');
		}
		public function Footer() 
		{
			$linkbd=conectar_bd();
			$sqlr="SELECT direccion,telefono,web,email FROM configbasica WHERE estado='S'";
			$resp=mysql_query($sqlr,$linkbd);
			while($row=mysql_fetch_row($resp))
			{
				$direcc=utf8_encode(strtoupper($row[0]));
				$telefonos=$row[1];
				$dirweb=utf8_encode(strtoupper($row[3]));
				$coemail=utf8_encode(strtoupper($row[2]));
			}
			if($direcc!=''){$vardirec="Dirección: $direcc, ";}
			else {$vardirec="";}
			if($telefonos!=''){$vartelef="Telefonos: $telefonos";}
			else{$vartelef="";}
			if($dirweb!=''){$varemail="Email: $dirweb, ";}
			else {$varemail="";}
			if($coemail!=''){$varpagiw="Pagina Web: $coemail";}
			else{$varpagiw="";}
			$this->SetY(-16);
			$this->SetFont('helvetica', 'BI', 8);
			$txt = <<<EOD
$vardirec $vartelef
$varemail $varpagiw
EOD;
			$this->Write(0, $txt, '', 0, 'C', true, 0, false, false, 0);
			$this->SetY(-13);
			$this->Cell(0, 10, 'Page '.$this->getAliasNumPage().'/'.$this->getAliasNbPages(), 0, false, 'C', 0, '', 0, false, 'T', 'M');
			$this->Line(10, 205, 265, 205,$styleline);
		}
	}
	$pdf = new MYPDF('L','mm','Letter', true, 'iso-8859-1', false);// create new PDF document
	$pdf->SetDocInfoUnicode (true); 
	// set document information
	$pdf->SetCreator(PDF_CREATOR);
	$pdf->SetAuthor('G&CSAS');
	$pdf->SetTitle('Listado Facturas');
	$pdf->SetSubject('RP General');
	$pdf->SetKeywords('TCPDF, PDF, example, test, guide');
	$pdf->SetMargins(10, 49, 10);// set margins
	$pdf->SetHeaderMargin(49);// set margins
	$pdf->SetFooterMargin(20);// set margins
	$pdf->SetAutoPageBreak(TRUE, 20);// set auto page breaks
	// set some language-dependent strings (optional)
	if (@file_exists(dirname(__FILE__).'/lang/spa.php')) 
	{
		require_once(dirname(__FILE__).'/lang/spa.php');
		$pdf->setLanguageArray($l);
	}
	// ---------------------------------------------------------
	$pdf->AddPage();
	$pdf->SetFont('helvetica','I',9);
	$linkbd=conectar_bd();
	$crit1=$crit2=$crit3=$crit4="";
	if ($_POST[nfactura]!=""){$crit1="WHERE T1.id_liquidacion LIKE '$_POST[nfactura]' ";}
	if ($_POST[codusu]!="")
	{
		if ($_POST[nfactura]!=""){$crit2="AND T1.codusuario LIKE '%$_POST[codusu]%' ";}
		else {$crit2="WHERE T1.codusuario LIKE '%$_POST[codusu]%' ";}
	}
	if ($_POST[docusu]!="")
	{
		if ($_POST[nfactura]!="" || $_POST[codusu]!=""){$crit3="AND T1.tercero LIKE '".str_pad($_POST[docusu],10,"0", STR_PAD_LEFT)."' ";}
		else{$crit3="WHERE T1.tercero LIKE '".str_pad($_POST[docusu],10,"0", STR_PAD_LEFT)."' ";}
	}
	$sqlr="SELECT T1.id_liquidacion,T1.liquidaciongen,T1.vigencia,T1.codusuario,T1.estado,(SELECT  concat_ws('<->', T2.nombretercero,T2.direccion) FROM servclientes T2 WHERE T2.codigo=T1.codusuario),(SELECT concat_ws('<->',SUM(T3.valorliquidacion),SUM(T3.abono)) FROM servliquidaciones_det T3 WHERE T3.id_liquidacion=T1.id_liquidacion) FROM servliquidaciones T1 $crit1 $crit2 $crit3 ORDER BY T1.id_liquidacion DESC ";
	$resp = mysql_query($sqlr,$linkbd);
	while ($row =mysql_fetch_row($resp))
	{
		$datuser = explode('<->', $row[5]);
		$divvalores=explode('<->', $row[6]);
		if($row[4]=='P'){$abonos=$divvalores[0];}
		else {$abonos=$divvalores[1];}
		if ($concolor==0){$pdf->SetFillColor(200,200,200);$concolor=1;}
		else {$pdf->SetFillColor(255,255,255);$concolor=0;}
		$pdf->Cell(20,6,str_pad($row[0],10,"0", STR_PAD_LEFT),1,0,'C',true,0,0,false,'T','C');
		$pdf->Cell(15,6,$row[1],1,0,'C',true,0,0,false,'T','C');
		$pdf->Cell(15,6,$row[2],1,0,'C',true,0,0,false,'T','C');
		$pdf->Cell(21,6,$row[3],1,0,'C',true,0,0,false,'T','C');
		$pdf->Cell(70,6,$datuser[0],1,0,'L',true,0,0,false,'T','C');
		$pdf->Cell(70,6,$datuser[1],1,0,'L',true,0,0,false,'T','C');
		$pdf->Cell(27,6,"$ ".number_format($divvalores[0],2,',','.')." ",1,0,'R',true,0,0,false,'T','C');
		$pdf->Cell(27,6,"$ ".number_format($abonos,2,',','.')." ",1,1,'R',true,0,0,false,'T','C');
	}
	// ---------------------------------------------------------
	$pdf->Output('Listadofacturas.pdf', 'I');//Close and output PDF document
?>