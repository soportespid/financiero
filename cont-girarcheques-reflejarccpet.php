<?php
header("Cache-control: no-cache, no-store, must-revalidate");
header("Content-Type: text/html;charset=utf8");

require "comun.inc";
require "funciones.inc";
require "conversor.php";
require "validaciones.inc";

$linkbd_V7 = conectar_v7();
$linkbd_V7->set_charset("utf8");

session_start();
date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE>
<html lang="es">

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1" />
    <meta name="viewport" content="user-scalable=no">
    <title>:: IDEAL 10 - Contabilidad</title>
    <link href="favicon.ico" rel="shortcut icon" />
    <link href="css/css2.css?<?php echo date('d_m_Y_h_i_s'); ?>" rel="stylesheet" type="text/css" />
    <link href="css/css3.css?<?php echo date('d_m_Y_h_i_s'); ?>" rel="stylesheet" type="text/css" />
    <link href="css/tabs.css?<?php echo date('d_m_Y_h_i_s'); ?>" rel="stylesheet" type="text/css" />
    <link href="css/style.css?<?php echo date('d_m_Y_h_i_s'); ?>" rel="stylesheet" type="text/css" />
    <link href="sweetalert2/dist/sweetalert2.min.css" rel="stylesheet">
    <script src="sweetalert2/dist/sweetalert2.min.js"></script>
    <script type="text/javascript" src="css/programas.js"></script>
    <script type="text/javascript" src="css/calendario.js"></script>
    <script>
        function validar() {
            document.form2.submit();
        }
        function guardar() {
            if (document.form2.fecha.value != '') {
                Swal.fire({
                    icon: 'question',
                    title: '¿Seguro que quieres reflejar la información?',
                    showDenyButton: true,
                    confirmButtonText: 'Guardar',
                    confirmButtonColor: '#01CC42',
                    denyButtonText: 'Cancelar',
                    denyButtonColor: '#FF121A',
                }).then(
                    (result) => {
                        if (result.isConfirmed) {
                            document.form2.oculto.value = 2;
                            document.form2.submit();
                        }
                        else if (result.isDenied) {
                            Swal.fire({
                                icon: 'info',
                                title: 'No se reflejo la información',
                                confirmButtonText: 'Continuar',
                                confirmButtonColor: '#FF121A',
                                timer: 2500
                            });
                        }
                    }
                )
            }
            else {
                Swal.fire({
                    icon: 'error',
                    title: 'Error!',
                    text: 'Faltan datos para poder reflejar',
                    confirmButtonText: 'Continuar',
                    confirmButtonColor: '#FF121A',
                    timer: 2500
                });
            }
        }
        function calcularpago() {
            valorp = document.form2.valor.value;
            descuentos = document.form2.totaldes.value;
            valorc = valorp - descuentos;
            document.form2.valorcheque.value = valorc;
            document.form2.valoregreso.value = valorp;
            document.form2.valorretencion.value = descuentos;
        }
        function pdf() {
            document.form2.action = "pdfegreso.php";
            document.form2.target = "_BLANK";
            document.form2.submit();
            document.form2.action = "";
            document.form2.target = "";
        }
        function adelante() {
            if (parseFloat(document.form2.ncomp.value) < parseFloat(document.form2.maximo.value)) {
                document.form2.oculto.value = 1;
                document.form2.ncomp.value = parseFloat(document.form2.ncomp.value) + 1;
                document.form2.egreso.value = parseFloat(document.form2.egreso.value) + 1;
                document.form2.action = "cont-girarcheques-reflejarccpet.php";
                document.form2.submit();
            }
        }
        function atrasc() {
            if (document.form2.ncomp.value > 1) {
                document.form2.oculto.value = 1;
                document.form2.ncomp.value = document.form2.ncomp.value - 1;
                document.form2.egreso.value = document.form2.egreso.value - 1;
                document.form2.action = "cont-girarcheques-reflejarccpet.php";
                document.form2.submit();
            }
        }
        function validar2() {
            document.form2.oculto.value = 1;
            document.form2.ncomp.value = document.form2.egreso.value;
            document.form2.action = "cont-girarcheques-reflejarccpet.php";
            document.form2.submit();
        }
        function direccionaCuentaGastos(row) {
            window.open("cont-editarcuentagastos.php?idcta=" + row);
        }
        function despliegamodalm(_valor, _tip, mensa, pregunta) {
            document.getElementById("bgventanamodalm").style.visibility = _valor;
            if (_valor == "hidden") { document.getElementById('ventanam').src = ""; }
            else {
                switch (_tip) {
                    case "1":
                        document.getElementById('ventanam').src = "ventana-mensaje1.php?titulos=" + mensa; break;
                    case "2":
                        document.getElementById('ventanam').src = "ventana-mensaje3.php?titulos=" + mensa; break;
                    case "3":
                        document.getElementById('ventanam').src = "ventana-mensaje2.php?titulos=" + mensa; break;
                    case "4":
                        document.getElementById('ventanam').src = "ventana-consulta1.php?titulos=" + mensa + "&idresp=" + pregunta; break;
                }
            }
        }
        function respuestaconsulta(pregunta) {
            switch (pregunta) {
                case "1":
                    document.form2.oculto.value = 2;
                    document.form2.submit();
                    break;
            }
        }
        function funcionmensaje() { }
    </script>
</head>

<body>
    <IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
    <span id="todastablas2"></span>
    <table>
        <tr>
            <script>barra_imagenes("cont");</script><?php cuadro_titulos(); ?>
        </tr>
        <tr><?php menu_desplegable("cont"); ?></tr>
    </table>
    <div class="bg-white group-btn p-1"><button type="button" onclick="window.location.href='#'" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
            <span>Buscar</span>
            <svg class="fill-black group-hover:fill-white w-5 h-5" xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960"><path d="M784-120 532-372q-30 24-69 38t-83 14q-109 0-184.5-75.5T120-580q0-109 75.5-184.5T380-840q109 0 184.5 75.5T640-580q0 44-14 83t-38 69l252 252-56 56ZM380-400q75 0 127.5-52.5T560-580q0-75-52.5-127.5T380-760q-75 0-127.5 52.5T200-580q0 75 52.5 127.5T380-400Z"></path></svg>
        </button><button type="button" onclick="mypop=window.open('plan-agenda.php','','');mypop.focus()" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
            <span class="group-hover:text-white">Agenda</span>
            <svg class="fill-black group-hover:fill-white w-5 h-5" xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960"><path d="M560-564v-68q33-14 67.5-21t72.5-7q26 0 51 4t49 10v64q-24-9-48.5-13.5T700-600q-38 0-73 9.5T560-564Zm0 220v-68q33-14 67.5-21t72.5-7q26 0 51 4t49 10v64q-24-9-48.5-13.5T700-380q-38 0-73 9t-67 27Zm0-110v-68q33-14 67.5-21t72.5-7q26 0 51 4t49 10v64q-24-9-48.5-13.5T700-490q-38 0-73 9.5T560-454ZM260-320q47 0 91.5 10.5T440-278v-394q-41-24-87-36t-93-12q-36 0-71.5 7T120-692v396q35-12 69.5-18t70.5-6Zm260 42q44-21 88.5-31.5T700-320q36 0 70.5 6t69.5 18v-396q-33-14-68.5-21t-71.5-7q-47 0-93 12t-87 36v394Zm-40 118q-48-38-104-59t-116-21q-42 0-82.5 11T100-198q-21 11-40.5-1T40-234v-482q0-11 5.5-21T62-752q46-24 96-36t102-12q58 0 113.5 15T480-740q51-30 106.5-45T700-800q52 0 102 12t96 36q11 5 16.5 15t5.5 21v482q0 23-19.5 35t-40.5 1q-37-20-77.5-31T700-240q-60 0-116 21t-104 59ZM280-494Z"></path></svg>
        </button><button type="button" onclick="mypop=window.open('cont-principal.php','','');mypop.focus();" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
            <span>Nueva ventana</span>
            <svg class="fill-black group-hover:fill-white w-5 h-5" xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960"><path d="M320-120v-80h80v-80H160q-33 0-56.5-23.5T80-360v-400q0-33 23.5-56.5T160-840h640q33 0 56.5 23.5T880-760v400q0 33-23.5 56.5T800-280H560v80h80v80H320ZM160-360h640v-400H160v400Zm0 0v-400 400Z"></path></svg>
        </button><button type="button" onclick="guardar()" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
            <span>Reflejar</span>
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960"><path d="M400-280h160v-80H400v80Zm0-160h280v-80H400v80ZM280-600h400v-80H280v80Zm200 120ZM265-80q-79 0-134.5-55.5T75-270q0-57 29.5-102t77.5-68H80v-80h240v240h-80v-97q-37 8-61 38t-24 69q0 46 32.5 78t77.5 32v80Zm135-40v-80h360v-560H200v160h-80v-160q0-33 23.5-56.5T200-840h560q33 0 56.5 23.5T840-760v560q0 33-23.5 56.5T760-120H400Z"></path></svg>
        </button><button type="button" onclick="pdf()" class="btn btn-danger btn-danger-hover d-flex justify-between align-items-center">
            <span>Exportar PDF</span>
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><!-- !Font Awesome Free 6.5.1 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license/free Copyright 2024 Fonticons, Inc. --><path d="M64 464l48 0 0 48-48 0c-35.3 0-64-28.7-64-64L0 64C0 28.7 28.7 0 64 0L229.5 0c17 0 33.3 6.7 45.3 18.7l90.5 90.5c12 12 18.7 28.3 18.7 45.3L384 304l-48 0 0-144-80 0c-17.7 0-32-14.3-32-32l0-80L64 48c-8.8 0-16 7.2-16 16l0 384c0 8.8 7.2 16 16 16zM176 352l32 0c30.9 0 56 25.1 56 56s-25.1 56-56 56l-16 0 0 32c0 8.8-7.2 16-16 16s-16-7.2-16-16l0-48 0-80c0-8.8 7.2-16 16-16zm32 80c13.3 0 24-10.7 24-24s-10.7-24-24-24l-16 0 0 48 16 0zm96-80l32 0c26.5 0 48 21.5 48 48l0 64c0 26.5-21.5 48-48 48l-32 0c-8.8 0-16-7.2-16-16l0-128c0-8.8 7.2-16 16-16zm32 128c8.8 0 16-7.2 16-16l0-64c0-8.8-7.2-16-16-16l-16 0 0 96 16 0zm80-112c0-8.8 7.2-16 16-16l48 0c8.8 0 16 7.2 16 16s-7.2 16-16 16l-32 0 0 32 32 0c8.8 0 16 7.2 16 16s-7.2 16-16 16l-32 0 0 48c0 8.8-7.2 16-16 16s-16-7.2-16-16l0-64 0-64z"></path></svg>
        </button><button type="button" onclick="window.location.href='cont-reflejardocsccpet.php'" class="btn btn-success btn-success-hover d-flex justify-between align-items-center">
            <span>Atras</span>
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960"><path d="M760-200v-160q0-50-35-85t-85-35H273l144 144-57 56-240-240 240-240 57 56-144 144h367q83 0 141.5 58.5T840-360v160h-80Z"></path></svg>
        </button></div>
        <?php
        $maxVersion = ultimaVersionGastosCCPET();
        $vigencia = date('Y');
        $vigusu = vigencia_usuarios($_SESSION['cedulausu']);
        $sqlr = "select *from cuentapagar where estado='S' ";
        $res = mysqli_query($linkbd_V7, $sqlr);
        while ($row = mysqli_fetch_row($res)) {
            $_POST['cuentapagar'] = $row[1];
        }
        $sqlr = "select * from admbloqueoanio";
        $res = mysqli_query($linkbd_V7, $sqlr);
        $_POST['anio'] = array();
        $_POST['bloqueo'] = array();
        while ($row = mysqli_fetch_row($res)) {
            $_POST['anio'][] = $row[0];
            $_POST['bloqueo'][] = $row[1];
        }
        if ($_GET['consecutivo'] != "") {
            echo "<script>document.getElementById('codrec').value=$_GET[consecutivo];</script>";
        }
        //*********** cuenta origen va al credito y la destino al debito
        if (!$_POST['oculto']) {
            $sqlr = "select *from cuentapagar where estado='S' ";
            $res = mysqli_query($linkbd_V7, $sqlr);
            while ($row = mysqli_fetch_row($res)) {
                $_POST['cuentapagar'] = $row[1];
            }
            $sqlr = "select * from tesoegresos ORDER BY id_egreso DESC";
            $res = mysqli_query($linkbd_V7, $sqlr);
            $r = mysqli_fetch_row($res);
            $_POST['maximo'] = $r[0];
            if ($_POST['codrec'] != "" || $_GET['consecutivo'] != "") {
                if ($_POST['codrec'] != "") {
                    $sqlr = "select * from tesoegresos where id_egreso='$_POST[codrec]' AND  tipo_mov='201'";
                } else {
                    $sqlr = "select * from tesoegresos where id_egreso='$_GET[consecutivo]' AND tipo_mov='201'";
                }
            } else {
                $sqlr = "select * from tesoegresos WHERE tipo_mov='201' ORDER BY id_egreso DESC";
            }
            $res = mysqli_query($linkbd_V7, $sqlr);
            $r = mysqli_fetch_row($res);
            $_POST['ncomp'] = $r[0];
            $_POST['egreso'] = $r[0];
            $check3 = "checked";
        }
        $sqlr = "select * from tesoegresos where id_egreso=$_POST[ncomp] and tipo_mov='201'";
        $res = mysqli_query($linkbd_V7, $sqlr);
        $consec = 0;
        while ($r = mysqli_fetch_row($res)) {
            $consec = $r[0];
            $_POST['orden'] = $r[2];
            $_POST['tipop'] = $r[14];
            $_POST['banco'] = $r[9];
            $_POST['estado'] = $r[13];
            $_POST['vigencia'] = substr($r[3], 0, 4);
            if ($_POST['estado'] == 'N') {
                $estadoc = "ANULADO";
            } else if ($_POST['estado'] == 'S' || $_POST['estado'] == 'P' || $_POST['estado'] == 'C') {
                $estadoc = "ACTIVO";
            } else {
                $estadoc = "REVERSADO";
            }
            $_POST['ncheque'] = $r[10];
            $_POST['cb'] = $r[12];
            $_POST['transferencia'] = $r[12];
            $_POST['fecha'] = $r[3];
            $_POST['codingreso'] = $r[15];
            $nombreCodigo = buscaingreso($r[15]);
            if ($nombreCodigo != '') {
                $_POST['ningreso'] = $nombreCodigo;
            } else {
                $_POST['ningreso'] = buscaEntidadAdministradora($r[15]);
                $_POST['entidadAdministradora'] = 'S';
            }
        }
        preg_match("/([0-9]{4})\-([0-9]{2})\-([0-9]{2})/", $_POST['fecha'], $fecha);
        $fechaf = $fecha[3] . "/" . $fecha[2] . "/" . $fecha[1];
        $_POST['fecha'] = $fechaf;
        $_POST['vigencia2'] = $fecha[1];
        $_POST['egreso'] = $consec;

        switch ($_POST['tabgroup1']) {
            case 1:
                $check1 = 'checked';
                break;
            case 2:
                $check2 = 'checked';
                break;
            case 3:
                $check3 = 'checked';
        }
        $check6 = 'checked';
        ?>
        <div id="bgventanamodalm" class="bgventanamodalm">
            <div id="ventanamodalm" class="ventanamodalm">
                <IFRAME src="" name="ventanam" marginWidth=0 marginHeight=0 frameBorder=0 id="ventanam" frameSpacing=0 style=" width:700px; height:130px; top:200; overflow:hidden;">
                </IFRAME>
            </div>
        </div>
        <form name="form2" method="post" action="">
        <input type="hidden" name="anio[]" id="anio[]" value="<?php echo $_POST['anio'] ?>">
        <input type="hidden" name="anioact" id="anioact" value="<?php echo $_POST['anioact'] ?>">
        <input type="hidden" name="bloqueo[]" id="bloqueo[]" value="<?php echo $_POST['bloqueo'] ?>">
        <?php
        $_POST['dcuentas'] = array();
        $_POST['dvaloresoc'] = array();
        $_POST['dvalores'] = array();
        $_POST['dncuentas'] = array();
        $_POST['dvigenciaGasto'] = array();
        $_POST['dseccion_presupuestal'] = array();
        $_POST['dtipo_gasto'] = array();
        $_POST['ddivipola'] = array();
        $_POST['dchip'] = array();
        $_POST['rubros'] = array();
        $_POST['inicios'] = array();
        $_POST['drecursos'] = array();
        $_POST['productoServicio'] = array();
        $_POST['indicador_producto'] = array();
        $_POST['medio_pago'] = array();
        $_POST['bpim'] = array();
        $_POST['dnrecursos'] = array();
        $_POST['sector'] = array();
        $_POST['dcuentasCheck'] = array();
        $_POST['nombreSector'] = array();
        if ($_POST['orden'] != '') {
            //*** busca detalle cdp
            $sqlr = "select *from tesoordenpago where id_orden=$_POST[orden] AND tipo_mov='201'";
            //echo $sqlr;
            $resp = mysqli_query($linkbd_V7, $sqlr);
            $row = mysqli_fetch_row($resp);
            $_POST['concepto'] = $row[7];
            $_POST['tercero'] = $row[6];
            $_POST['vigenciaop'] = $row[3];
            $_POST['ntercero'] = buscatercero($_POST['tercero']);
            $_POST['valororden'] = $row[10];
            $_POST['cc'] = $row[5];
            $_POST['retenciones'] = $row[12];
            $_POST['base'] = $row[14];
            $_POST['iva'] = $row[15];
            $_POST['vigenciadoc'] = $row[3];
            $_POST['fechacxp'] = $row[2];
            $_POST['totaldes'] = number_format($_POST['retenciones'], 2);
            $_POST['valorpagar'] = $_POST['valororden'] - $_POST['retenciones'];
            $_POST['bop'] = "";
            $_POST['medioDePago'] = $row[19];
            if ($_POST['medioDePago'] == '') {
                $_POST['medioDePago'] = '-1';
            }
            $sqlr = "select *from tesoordenpago_det where id_orden=$_POST[orden] and tipo_mov='201' ";
            $res = mysqli_query($linkbd_V7, $sqlr);
            while ($r = mysqli_fetch_row($res)) {
                $_POST['dcuentas'][] = $r[2];
                $_POST['dvaloresoc'][] = $r[4];
                $_POST['dvalores'][] = $r[4];
                $_POST['dncuentas'][] = buscacuentaccpetgastos($r[2], $maxVersion);
                $_POST['dvigenciaGasto'][] = $r[12];
                $_POST['dseccion_presupuestal'][] = $r[15];
                $_POST['dtipo_gasto'][] = $r[16];
                $_POST['ddivipola'][] = $r[17];
                $_POST['dchip'][] = $r[18];
                $_POST['rubros'][] = $r[2];
                $_POST['inicios'][] = $r[2];
                $nfuente = buscafuenteccpet($r[8]);
                $cdfuente = substr($nfuente, 0, strpos($nfuente, "_"));
                $_POST['drecursos'][] = $r[8];
                $_POST['productoServicio'][] = $r[9];
                $_POST['indicador_producto'][] = $r[10];
                $_POST['medio_pago'][] = $r[11];
                $_POST['bpim'][] = $r[14];
                $indicadorProducto = substr($r[10], 0, 2);
                $_POST['dnrecursos'][] = $nfuente;
                $_POST['sector'][] = $indicadorProducto;
                $_POST['nombreSector'][] = buscaNombreSector($indicadorProducto);
                $tamProductoServicio = strlen($r[9]);
                if ($tamProductoServicio == 5) {
                    $_POST['nombreProductoServicio'][] = buscaservicioccpetgastos($r[9]);
                } else {
                    $_POST['nombreProductoServicio'][] = buscaproductoccpetgastos($r[9]);
                }
            }
        } else {
            $_POST['cdp'] = "";
            $_POST['detallecdp'] = "";
            $_POST['tercero'] = "";
            $_POST['ntercero'] = "";
            $_POST['bop'] = "";
        }
        ?>
        <div class="tabsic" style="height:210px;">
            <div class="tab">
                <input type="radio" id="tab-6" name="tabgroup2" value="6" <?php echo $check6; ?> >
                <label for="tab-6">Egreso</label>
                <div class="content" style="height:100%; overflow:hidden;">
                    <table class="inicio" >
                        <tr>
                            <td colspan="6" class="titulos">Comprobante de egreso</td>
                            <td style="width:7%;" class="cerrar" ><a href="cont-principal.php">Cerrar</a></td>
                        </tr>
                        <tr>
                            <td style="width:2.5cm" class="saludo1">No Egreso:</td>
                            <td style="width:10%">
                                <a href="#" onClick="atrasc()"><img src="imagenes/back.png" alt="anterior" align="absmiddle"></a>
                                <input name="cuentapagar" type="hidden" value="<?php echo $_POST['cuentapagar'] ?>" >
                                <input name="egreso" type="text" value="<?php echo $_POST['egreso'] ?>" style="width:50%" onKeyUp="return tabular(event,this)" onBlur="validar2()"  >
                                <input name="ncomp" type="hidden" value="<?php echo $_POST['ncomp'] ?>">
                                <a href="#" onClick="adelante()"><img src="imagenes/next.png" alt="siguiente" align="absmiddle"></a>
                                <input type="hidden" value="a" name="atras" ><input type="hidden" value="s" name="siguiente" >
                                <input type="hidden" value="<?php echo $_POST['maximo'] ?>" name="maximo">
                                <input type="hidden" value="<?php echo $_POST['codrec'] ?>" name="codrec" id="codrec">
                            </td>
                            <td style="width:2.5cm" class="saludo1">Fecha: </td>
                            <td >
                                <input id="fc_1198971545" name="fecha" type="text" value="<?php echo $_POST['fecha'] ?>"  style="width:100%;" onKeyUp="return tabular(event,this)" readonly>
                            </td>
                        <?php
                        if ($_POST['medioDePago'] != '2') {
                            ?>
                                <td style="width:3.5cm" class="saludo1">Forma de Pago:</td>
                                <td style="width:30%">
                                    <select name="tipop" onChange="validar();" ="return tabular(event,this)">
                                        <?php
                                        if ($_POST['tipop'] == 'cheque') {
                                            echo '<option value="cheque" selected>Cheque</option>';
                                        } else if ($_POST['tipop'] == 'transferencia') {
                                            echo '<option value="transferencia" selected>Transferencia</option>';
                                        } else {
                                            echo '<option value="caja" selected>Efectivo</option>';
                                        }
                                        ?>
                                        </select>
                            <?php
                        } else {
                            $_POST['tipop'] = '';
                            ?>
                                <td class="saludo1" style="width:10%">Medio de Pago:</td>
                                <td style="width:50%;">
                                    <input type="hidden" id="codingreso" name="codingreso" value="<?php echo $_POST['codingreso'] ?>"  onKeyUp="return tabular(event,this)" onBlur="buscaing(event)" style="width:100%;" readonly>
                                    <input type="text" name="ningreso" id="ningreso" value="<?php echo $_POST['codingreso'] . " - " . $_POST['ningreso'] ?>" style="width:100%;" readonly>
                                    <input type="hidden" name="entidadAdministradora" id="entidadAdministradora" value="<?php echo $_POST['entidadAdministradora'] ?>" >
                                </td>
                            <?php
                        }
                        ?>
                            <input type="hidden" name="estado"  value="<?php echo $_POST['estado'] ?>" readonly>
                            <?php
                            if ($_POST['estado'] == "S") {
                                $valuees = "ACTIVO";
                                $stylest = "width:80%; background-color:#0CD02A; color:white; text-align:center;";
                            } else if ($_POST['estado'] == "N") {
                                $valuees = "ANULADO";
                                $stylest = "width:80%; background-color:#FF0000; color:white; text-align:center;";
                            } else if ($_POST['estadoc'] == "P") {
                                $valuees = "PAGO";
                                $stylest = "width:80%; background-color:#0404B4; color:white; text-align:center;";
                            } else if ($_POST['estado'] == "R") {
                                $valuees = "REVERSADO";
                                $stylest = "width:80%; background-color:#FF0000; color:white; text-align:center;";
                            }
                            ?>
                                <input name="vigencia" type="hidden" value="<?php echo $_POST['vigencia'] ?>" size="10" onKeyUp="return tabular(event,this)" readonly>
                                <input name="vigenciaop" type="hidden" value="<?php echo $_POST['vigenciaop'] ?>" size="10" onKeyUp="return tabular(event,this)" readonly>
                            </td>
                        </tr>
                        <tr>
                            <td class="saludo1">No Orden Pago:</td>
                            <td>
                                <input name="orden" type="text" value="<?php echo $_POST['orden'] ?>" style="width:100%;" onKeyUp="return tabular(event,this)" readonly >
                                <input type="hidden" value="0" name="bop">
                            </td>
                            <td class="saludo1">Tercero:</td>
                            <td>
                                <input id="tercero" type="text" name="tercero" style="width:100%;" onKeyUp="return tabular(event,this)" onBlur="buscater(event)" value="<?php echo $_POST['tercero'] ?>" readonly>
                            </td>
                            <td colspan="2">
                                <input name="ntercero" type="text" value="<?php echo $_POST['ntercero'] ?>" style="width:100%;" readonly>
                            </td>
                        </tr>
                        <tr>
                            <td class="saludo1">Concepto:</td>
                            <td colspan="5">
                                <input name="concepto" type="text" value="<?php echo $_POST['concepto'] ?>" style="width:100%;" readonly>
                            </td>
                        </tr>
                        <?php
                        //**** if del cheques
                        if ($_POST['tipop'] == 'cheque') {
                            ?>
                            <tr>
                                <td class="saludo1">Cuenta Bancaria:</td>
                                <td colspan="3">
                                    <select id="banco" name="banco" onKeyUp="return tabular(event,this)">
                                        <?php
                                        $sqlr = "select tesobancosctas.estado,tesobancosctas.cuenta,tesobancosctas.ncuentaban,tesobancosctas.tipo,terceros.razonsocial,tesobancosctas.tercero from tesobancosctas,terceros where tesobancosctas.tercero=terceros.cedulanit and tesobancosctas.estado='S'  and tesobancosctas.tipo='Corriente' ";
                                        $res = mysqli_query($linkbd_V7, $sqlr);
                                        while ($row = mysqli_fetch_row($res)) {
                                            if ($row[1] == $_POST['banco']) {
                                                echo "<option value=$row[1] SELECTED>" . $row[2] . " - Cuenta " . $row[3] . " - " . $row[4] . "</option>";
                                                $_POST['nbanco'] = $row[4];
                                                $_POST['ter'] = $row[5];
                                                $_POST['cb'] = $row[2];
                                            }
                                        }
                                        ?>
                                    </select>
                                    <input name="cb" type="hidden" value="<?php echo $_POST['cb'] ?>" >
                                    <input type="hidden" id="ter" name="ter" value="<?php echo $_POST['ter'] ?>" >
                                    <input type="hidden" id="nbanco" name="nbanco" value="<?php echo $_POST['nbanco'] ?>">
                                </td>
                                <td class="saludo1">Cheque:</td>
                                <td>
                                    <input type="text" id="ncheque" name="ncheque" value="<?php echo $_POST['ncheque'] ?>" style="width:100%;" readonly>
                                </td>
                            </tr>
                        <?php
                        }//cierre del if de cheques
                        ?>
                    <?php
                    //**** if del transferencias
                    if ($_POST['tipop'] == 'transferencia') {
                        ?>
                            <tr>
                                <td class="saludo1">Cuenta Bancaria:</td>
                                <td colspan="3">
                                    <select id="banco" name="banco"  onChange="validar()" onKeyUp="return tabular(event,this)">
                                    <option value="">Seleccione....</option>
                                    <?php
                                    $sqlr = "select tesobancosctas.estado,tesobancosctas.cuenta,tesobancosctas.ncuentaban,tesobancosctas.tipo,terceros.razonsocial,tesobancosctas.tercero from tesobancosctas,terceros where tesobancosctas.tercero=terceros.cedulanit and tesobancosctas.estado='S' ";
                                    $res = mysqli_query($linkbd_V7, $sqlr);
                                    while ($row = mysqli_fetch_row($res)) {
                                        if ($row[1] == $_POST['banco']) {
                                            echo "<option value=$row[1] SELECTED>" . $row[2] . " - Cuenta " . $row[3] . " - " . $row[4] . "</option>";
                                            $_POST['nbanco'] = $row[4];
                                            $_POST['ter'] = $row[5];
                                            $_POST['cb'] = $row[2];
                                        }
                                    }
                                    ?>
                                </select>
                                <input name="cb" type="hidden" value="<?php echo $_POST['cb'] ?>" >
                                <input type="hidden" id="ter" name="ter" value="<?php echo $_POST['ter'] ?>" >
                                    <input type="hidden" id="nbanco" name="nbanco" value="<?php echo $_POST['nbanco'] ?>">
                                </td>
                                <td class="saludo1">No Transferencia:</td>
                                <td >
                                    <input type="text" id="ntransfe" name="ntransfe" value="<?php echo $_POST['ntransfe'] ?>" style="width:100%;" readonly >
                                </td>
                            </tr>
                        <?php
                    }//cierre del if de cheques
                    if ($_POST['tipop'] == 'caja')//**** if del transferencias
                    {
                        ?>
                                        <tr>
                                            <td class="saludo1">Cuenta Caja:</td>
                                            <td >
                                                <select id="banco" name="banco"  onChange="validar()" onKeyUp="return tabular(event,this)">
                                                    <option value="">Seleccione....</option>
                                                    <?php
                                                    $sqlr = "select cuentacaja from tesoparametros";
                                                    $res = mysqli_query($linkbd_V7, $sqlr);
                                                    while ($row = mysqli_fetch_row($res)) {
                                                        $_POST['nbanco'] = buscacuenta($row[0]);
                                                        echo "";
                                                        $i = $row[0];
                                                        if ($i == $_POST['banco']) {
                                                            echo "<option value='$row[0]' SELECTED>$row[0] - Cuenta $_POST[nbanco]</option>";

                                                        } else {
                                                            echo "<option value='$row[0]'>$row[0] - Cuenta $_POST[nbanco]</option>";
                                                        }
                                                    }
                                                    ?>
                                                </select>
                                            </td>
                                            <td colspan="2"><input type="text" id="nbanco" name="nbanco" value="<?php echo $_POST['nbanco'] ?>" style="width:100%" readonly></td>
                                        </tr>
                                <?php
                    }//cierre del if de efectivo
                    ?>
                        <tr>
                            <td class="saludo1">Valor Orden:</td>
                            <td>
                                <input name="valor" type="hidden" value="<?php echo $_POST['valororden'] ?>">
                                <input name="valororden" type="text" id="valororden" onKeyUp="return tabular(event,this)" value="<?php echo number_format($_POST['valororden'], 2, ',', '.') ?>" style="width:100%; text-align:right;" readonly>
                            </td>
                            <td class="saludo1">Retenciones:</td>
                            <td>
                                <input name="retenciones" type="text" id="retenciones" onKeyUp="return tabular(event,this)" value="<?php echo number_format($_POST['retenciones'], 2, ',', '.') ?>" style="width:100%; text-align:right;" readonly>
                            </td>
                            <td class="saludo1">Valor a Pagar:</td>
                            <td>
                                <input name="valorpagar" type="text" id="valorpagar" onKeyUp="return tabular(event,this)" value="<?php echo number_format($_POST['valorpagar'], 2, ',', '.') ?>" style="width:100%; text-align:right;" readonly>
                                <input type="hidden" value="<?php $_POST['oculto'] ?>" name="oculto">
                            </td>
                        </tr>
                        <tr>
                            <td class="saludo1" >Base:</td>
                            <td>
                                <input type="text" id="base" name="base" value="<?php echo number_format($_POST['base'], 2, ',', '.') ?>"  style="width:100%; text-align:right;" onKeyUp="return tabular(event,this)" readonly>
                            </td>
                            <td class="saludo1" >Iva:</td>
                            <td>
                                <input type="text" id="iva" name="iva" value="<?php echo number_format($_POST['iva'], 2, ',', '.') ?>" style="width:100%; text-align:right;" onKeyUp="return tabular(event,this)" onChange='' readonly>
                                <input type="hidden" id="regimen" name="regimen" value="<?php echo $_POST['regimen'] ?>" >
                                <input type="hidden" id="cc" name="cc" value="<?php echo $_POST['cc'] ?>" >
                            </td>
                                                <td><input type='text' name='estado' id='estado' value='<?php echo $valuees ?>' style='<?php echo $stylest ?>' readonly /></td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
        <div class="tabsic">
            <div class="tab">
                <input type="radio" id="tab-1" name="tabgroup1" value="1" <?php echo $check1; ?> >
                <label for="tab-1">Detalle</label>
                <div class="content" style="overflow-x:hidden; height:250px;" >
                    <table class="inicio">
                        <tr>
                            <td colspan="10" class="titulos">Detalle Orden de Pago</td>
                        </tr>
                        <tr>
                            <td class='titulos2' style='text-align:center;'>Vig. Gasto</td>
                            <td class='titulos2' style='text-align:center;'>Sec. presupuestal</td>
                            <td class='titulos2' style='text-align:center;'>Medio pago</td>
                            <td class="titulos2">Cuenta</td>
                            <td class="titulos2">Nombre Cuenta</td>
                            <td class="titulos2">Fuente</td>
                            <td class='titulos2' style='text-align:center;'>Producto/Servicio</td>
                            <td class='titulos2' style='width:10%;text-align:center;'>Programatico MGA</td>
                            <td class='titulos2' style='text-align:center;'>BPIM</td>
                            <td class="titulos2">Valor</td>
                        </tr>
                        <?php
                        $_POST['totalc'] = 0;
                        $iter = 'saludo1a';
                        $iter2 = 'saludo2';
                        for ($x = 0; $x < count($_POST['dcuentas']); $x++) {
                            echo "
								<input type='hidden' name='dcuentas[]' value='" . $_POST['dcuentas'][$x] . "'/>
								<input type='hidden' name='dregalias[]' value='" . $ar[0] . "'/>
								<input type='hidden' name='dncuentas[]' value='" . $_POST['dncuentas'][$x] . "'/>
								<input type='hidden' name='dvigenciaGasto[]' value='" . $_POST['dvigenciaGasto'][$x] . "'/>
								<input type='hidden' name='dseccion_presupuestal[]' value='" . $_POST['dseccion_presupuestal'][$x] . "'/>
								<input type='hidden' name='dtipo_gasto[]' value='" . $_POST['dtipo_gasto'][$x] . "'/>
								<input type='hidden' name='ddivipola[]' value='" . $_POST['ddivipola'][$x] . "'/>
								<input type='hidden' name='dchip[]' value='" . $_POST['dchip'][$x] . "'/>
								<input type='hidden' name='drecursos[]' value='" . $_POST['drecursos'][$x] . "'/>
								<input type='hidden' name='dnrecursos[]' value='" . $_POST['dnrecursos'][$x] . "'/>
								<input type='hidden' name='productoServicio[]' value='" . $_POST['productoServicio'][$x] . "'/>
								<input type='hidden' name='nombreProductoServicio[]' value='" . $_POST['nombreProductoServicio'][$x] . "'/>
								<input type='hidden' name='indicador_producto[]' value='" . $_POST['indicador_producto'][$x] . "'/>
								<input type='hidden' name='bpim[]' value='" . $_POST['bpim'][$x] . "'/>
								<input type='hidden' name='medio_pago[]' value='" . $_POST['medio_pago'][$x] . "'/>
								<input type='hidden' name='sector[]' value='" . $_POST['sector'][$x] . "'/>
								<input type='hidden' name='nombreSector[]' value='" . $_POST['nombreSector'][$x] . "'/>
								<input type='hidden' name='dvalores[]' value='" . $_POST['dvalores'][$x] . "'/>
								<input type='hidden' name='dvaloresoc[]' value='" . $_POST['dvaloresoc'][$x] . "'/>
								<tr  class='$iter' style='text-transform:uppercase'>

									<td>" . $_POST['dvigenciaGasto'][$x] . "</td>
									<td>" . $_POST['dseccion_presupuestal'][$x] . "</td>
									<td>" . $_POST['medio_pago'][$x] . "</td>
									<td style='width:10%;'>" . $_POST['dcuentas'][$x] . "</td>
									<td style='width:30%;'>" . $_POST['dncuentas'][$x] . "</td>
									<td style='width:15%;'>" . $_POST['drecursos'][$x] . " - " . $_POST['dnrecursos'][$x] . "</td>
									<td style='width:15%;'>" . $_POST['productoServicio'][$x] . " - " . $_POST['nombreProductoServicio'][$x] . "</td>
									<td style='width:6%;'>" . $_POST['indicador_producto'][$x] . "</td>
									<td style='width:8%;'>" . $_POST['bpim'][$x] . "</td>

									<td style='text-align:right;width:15%;' ";
                            if ($ch == '1') {
                                echo "onDblClick='llamarventanaegre(this,$x);'";
                            }
                            echo " >$ " . number_format($_POST['dvalores'][$x], 2, '.', ',') . "</td>
								</tr>";

                            $_POST['totalc'] = $_POST['totalc'] + $_POST['dvalores'][$x];
                            $_POST['totalcf'] = number_format($_POST['totalc'], 2, ".", ",");
                            $aux = $iter;
                            $iter = $iter2;
                            $iter2 = $aux;
                        }
                        $resultado = convertir($_POST['valorpagar']);
                        $_POST['letras'] = $resultado . " PESOS M/CTE";
                        echo "<tr class='titulos2'>
							<td colspan='8'></td>
							<td>Total</td>
							<td align='right'>
								<input name='totalcf' type='hidden' value='$_POST[totalcf]'>
								<input name='totalc' type='hidden' value='$_POST[totalc]'>" . number_format($_POST['totalc'], 2, ',', '.') . "
							</td>
						</tr>
						<tr class='titulos2'>
							<td>Son:</td>
							<td colspan='10'>
								<input name='letras' type='hidden' value='$_POST[letras]'>" . $_POST['letras'] . "
							</td>
						</tr>";
                        ?>
                        <script>
                            document.form2.valor.value=<?php echo $_POST['totalc']; ?>;
                        </script>
                    </table>
                </div>
            </div>
            <div class="tab">
                <input type="radio" id="tab-2" name="tabgroup1" value="2" <?php echo $check2; ?>>
                <label for="tab-2">Retenciones</label>
                <div class="content" style="overflow-x:hidden; height:250px;">
                    <table class="inicio" style="overflow:scroll">
                        <tr>
                            <td class="titulos" colspan="8">Retenciones</td>
                        </tr>
                        <tr>
                            <td></td>
                            <td class="saludo1" align="right">Total:</td>
                            <td class="saludo1" align="right">
                                <input id="totaldes" name="totaldes" type="text" size="10" value="<?php echo $_POST['totaldes'] ?>" readonly>
                                <?php
                                /* echo $_POST['totaldes']; */
                                ?>
                            </td>
                        </tr>
                        <tr>
                            <td class="titulos2">Descuento</td>
                            <td class="titulos2" align="center">%</td>
                            <td class="titulos2" align="center">Valor</td>
                        </tr>
                        <?php
                        if ($_POST['oculto'] != '2') {
                            $totaldes = 0;
                            $_POST['dndescuentos'] = array();
                            $_POST['ddescuentos'] = array();
                            $_POST['dporcentajes'] = array();
                            $_POST['ddesvalores'] = array();
                            $_POST['codigos'] = array();

                            $gdndescuentos = array();
                            $gddescuentos = array();
                            $gdporcentajes = array();
                            $gddesvalores = array();
                            $cr = 0;
                            $sqlr = "select *from tesoordenpago_retenciones where id_orden=$_POST[orden] and estado='S'";
                            $resd = mysqli_query($linkbd_V7, $sqlr);
                            while ($rowd = mysqli_fetch_row($resd)) {
                                $sqlr2 = "SELECT *from tesoretenciones where id=" . $rowd[0];
                                $resd2 = mysqli_query($linkbd_V7, $sqlr2);
                                $rowd2 = mysqli_fetch_row($resd2);
                                $gdndescuentos[$cr] = "$rowd2[1] - $rowd2[2]";
                                $gddescuentos[$cr] = $rowd2[1];
                                $gdporcentajes[$cr] = $rowd[2];
                                $gddesvalores[$cr] = round($rowd[3], 0);
                                echo "<tr>
									<td class='saludo2'>
										<input name='dndescuentos[]' value='" . $rowd2[1] . " - " . $rowd2[2] . "' type='hidden'>
										<input name='codigos[]' value='" . $rowd2[1] . "' type='hidden'>
										<input name='ddescuentos[]' value='" . $rowd[0] . "' type='hidden'>" . $rowd2[1] . " - " . $rowd2[2] . "
									</td>";
                                echo "<td class='saludo2' align='center'>
										<input name='dporcentajes[]' value='" . $rowd[2] . "' type='hidden'>" . $rowd[2] . "
									</td>";
                                echo "<td class='saludo2' align='right'>
										<input name='ddesvalores[]' value='" . $rowd[3] . "' type='hidden'>" . number_format(round($rowd[3], 0), 2) . "
									</td>
								</tr>";
                                $totaldes = $totaldes + ($rowd[3]);
                                $cr = $cr + 1;
                            }
                        }
                        ?>
                        <script>
                            document.form2.totaldes.value=<?php echo $totaldes; ?>;
                            calcularpago();
                        </script>
                    </table>
                </div>
            </div>
            <div class="tab">
                <input type="radio" id="tab-3" name="tabgroup1" value="3" <?php echo $check3; ?>>
                <label for="tab-3" style="background-color: #D93C31; color: white;">Contabilidad</label>
                <div class="content" style="overflow-x:hidden; height:250px;">
                    <table class="inicio">
                        <tr><td colspan="14" class="titulos">Parametrizar Progranacion Contable</td></tr>
                        <?php
                        if ($_POST['todos'] == 1) {
                            $checkt = 'checked';
                        } else {
                            $checkt = '';
                        }
                        if ($_POST['inicios'] == 1) {
                            $checkint = 'checked';
                        } else {
                            $checkint = '';
                        }
                        ?>
                        <tr>
                            <td class='titulos2' style='text-align:center;'>Vig. Gasto</td>
                            <td class='titulos2' style='text-align:center;'>Sec. presupuestal</td>
                            <td class='titulos2' style='text-align:center;'>Cuenta</td>
                            <td class='titulos2' style='text-align:center;'>Nombre Cuenta</td>
                            <td class='titulos2' style='text-align:center;'>Fuente</td>
                            <td class='titulos2' style='text-align:center;'>Sector</td>
                            <td class='titulos2' style='text-align:center;'>Producto/Servicio</td>
                            <td class='titulos2' style='text-align:center;'>Programativo MGA</td>
                            <td class='titulos2' style='text-align:center;'>Medio de Pago</td>
                            <td class='titulos2' style='text-align:center;'>Valor</td>
                            <td class='titulos2' style='text-align:center;'>Cuenta debito</td>
                        </tr>
                        <?php
                        $_POST['totalc'] = 0;
                        $iter = 'saludo1a';
                        $iter2 = 'saludo2';
                        for ($x = 0; $x < count($_POST['dcuentas']); $x++) {
                            $chk = '';
                            $sqlrCuentaCont = "SELECT cuenta_cred FROM tesoordenpago_cuenta WHERE id_orden = '$_POST[orden]' AND rubro = '" . $_POST['dcuentas'][$x] . "' AND productoservicio = '" . $_POST['productoServicio'][$x] . "' AND fuente = '" . $_POST['drecursos'][$x] . "' AND indicador_producto = '" . $_POST['indicador_producto'][$x] . "' AND codigo_vigenciag = '" . $_POST['dvigenciaGasto'][$x] . "' AND medio_pago = '" . $_POST['medio_pago'][$x] . "' AND seccion_presupuestal = '" . $_POST['dseccion_presupuestal'][$x] . "' AND bpim = '" . $_POST['bpim'][$x] . "'";//echo $sqlrCuentaCont."<br>";
                            $resCuentaCont = mysqli_query($linkbd_V7, $sqlrCuentaCont);
                            $rowCuentaCont = mysqli_fetch_row($resCuentaCont);
                            echo "
								<tr  class='$iter' style='text-transform:uppercase'>
									<td style='width:10%;'>" . $_POST['dvigenciaGasto'][$x] . "</td>
									<td style='width:10%;'>" . $_POST['dseccion_presupuestal'][$x] . "</td>
									<td style='width:10%;'>" . $_POST['dcuentas'][$x] . "</td>
									<td style='width:20%;'>" . $_POST['dncuentas'][$x] . "</td>
									<td style='width:20%;'>" . $_POST['drecursos'][$x] . " - " . $_POST['dnrecursos'][$x] . "</td>
									<td style='width:15%;'>" . $_POST['sector'][$x] . " - " . $_POST['nombreSector'][$x] . "</td>
									<td style='width:15%;'>" . $_POST['productoServicio'][$x] . " - " . $_POST['nombreProductoServicio'][$x] . "</td>
									<td style='width:8%;'>" . $_POST['indicador_producto'][$x] . "</td>
									<td style='width:4%;'>" . $_POST['medio_pago'][$x] . "</td>
									<td style='width:10%;'>" . number_format($_POST['dvalores'][$x], 2, ',', '.') . "</td>
									<td style='width:10%;'><input name='cuentaDebito[]' class='inpnovisibles' value='" . $rowCuentaCont[0] . "' readonly></td>
									";

                            echo "
								</tr>";

                            $aux = $iter;
                            $iter = $iter2;
                            $iter2 = $aux;
                        }
                        $resultado = convertir($_POST['totalc']);
                        $_POST['letras'] = $resultado . " PESOS M/CTE";
                        $_POST['totalcf'] = number_format($_POST['totalc'], 2, ".", ",");
                        if (isset($faltaAtributoContable) && $faltaAtributoContable) {
                            echo "
								<tr class='titulos2'>
									<td>Nota:</td>
									<td colspan='5'><h2>Falta contabilizar la cuenta por pagar para que automaticamente salga la cuenta debito en la columna de atributo contable</h2></td>
								</tr>";
                        }

                        ?>
                        <script>
                            document.form2.valor.value=<?php echo $_POST['totalc']; ?>;
                            document.form2.valoregreso.value=<?php echo $_POST['totalc']; ?>;
                            document.form2.valorcheque.value=document.form2.valor.value-document.form2.valorretencion.value;
                        </script>
                    </table>
                </div>
            </div>
        </div>
        <?php
        function generaRetenciones($orden, $valor)
        {
            $linkbd_V7 = conectar_v7();
            $linkbd_V7->set_charset("utf8");
            $arreglocuenta = array();
            $total = 0;
            $sqlRete = "SELECT id_retencion, valor, porcentaje FROM tesoordenpago_retenciones WHERE id_orden = '$orden' ";
            $resRete = mysqli_query($linkbd_V7, $sqlRete);
            while ($rowRete = mysqli_fetch_row($resRete)) {
                $sqlr = "select tesoretenciones_det.porcentaje, tesoretenciones.iva, tesoretenciones.retencion, tesoretenciones.codigo from tesoretenciones_det,tesoretenciones where tesoretenciones_det.codigo=tesoretenciones.id and tesoretenciones.id='$rowRete[0]' ORDER BY tesoretenciones_det.porcentaje DESC";
                $resdes = mysqli_query($linkbd_V7, $sqlr);
                $valordes = 0;
                $valorProcentaje = 0;
                $idRetencion = '';
                $val2 = 0;
                while ($rowdes = mysqli_fetch_row($resdes)) {

                    if ($idRetencion == $rowdes[3]) {
                        $valorProcentaje = $val2 + $rowdes[0];
                    } else {
                        $valorProcentaje = $rowdes[0];
                        $idRetencion = $rowdes[3];
                    }

                    $val2 = 0;
                    $val2 = $rowdes[0];

                    if ($valorProcentaje <= 100) {
                        if ($rowdes[1] == 1) {

                            $valordes = round(($valor / $_POST['valororden']) * ($_POST['iva'] * ($rowdes[2] / 100)), 2);
                            $total += $valordes;
                        } else {
                            $valordes = round(($valor / $_POST['valororden']) * (($rowdes[0] / 100) * $rowRete[1]), 2);
                            $total += $valordes;
                        }
                    }

                }
            }
            return $total;
        }
        if ($_POST['oculto'] == '2') {
            $query = "SELECT conta_pago FROM tesoparametros";
            $resultado = mysqli_query($linkbd_V7, $query);
            $arreglo = mysqli_fetch_row($resultado);
            $opcion = $arreglo[0];

            preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST['fecha'], $fecha);
            $fechaf = "$fecha[3]-$fecha[2]-$fecha[1]";
            $bloq = bloqueos($_SESSION['cedulausu'], $fechaf);
            $vigegreso = $fecha[3];
            if ($bloq >= 1) {
                //************CREACION DEL COMPROBANTE CONTABLE ************************
                $sqlr = "select count(*) from tesoegresos where id_egreso=$_POST[egreso] and estado ='S'";
                $res = mysqli_query($linkbd_V7, $sqlr);
                $row = mysqli_fetch_row($res);
                //***********crear el contabilidad
                $ideg = $_POST['egreso'];
                $sqlr = "delete from comprobante_cab where numerotipo=$ideg and tipo_comp=6";
                mysqli_query($linkbd_V7, $sqlr);
                $sqlr = "delete from comprobante_det where id_comp='6 $ideg' ";
                mysqli_query($linkbd_V7, $sqlr);
                if ($_POST['estado'] == 'N')
                    $estado = 0;
                else
                    $estado = 1;
                $sqlr = "insert into comprobante_cab (numerotipo,tipo_comp,fecha,concepto,total,total_debito,total_credito,diferencia,estado) values ($ideg,6,'$fechaf','$_POST[concepto]',0,$_POST[valororden],$_POST[valororden],0,'$estado')";
                mysqli_query($linkbd_V7, $sqlr);
                $idcomp = mysqli_insert_id($linkbd_V7);
                $totaldes = 0;
                $numrub = 0;
                $totpago = 0;
                for ($x = 0; $x < count($_POST['dcuentas']); $x++) {
                    if ($_POST['dvalores'][$x] > 0) {
                        $numrub += 1;
                        $totpago += $_POST['dvalores'][$x];
                    }
                }
                $sqt = "SELECT *FROM tesoordenpago WHERE id_orden='$_POST[orden]' AND tipo_mov='201'";
                $resp = mysqli_query($linkbd_V7, $sqt);
                $rw = mysqli_fetch_assoc($resp);
                for ($j = 0; $j < count($_POST['dcuentas']); $j++) {
                    if ($opcion == "2") {
                        $cantDescuentos = count($_POST['dndescuentos']);
                        for ($x = 0; $x < $cantDescuentos; $x++) {
                            $sqlr = "select * from tesoretenciones,tesoretenciones_det where tesoretenciones_det.codigo=tesoretenciones.id and tesoretenciones.codigo='" . $_POST['codigos'][$x] . "' ORDER BY porcentaje DESC";
                            $resdes = mysqli_query($linkbd_V7, $sqlr);
                            $valordes = 0;
                            $val2 = 0;
                            $valorProcentaje = 0;
                            $idRetencion = '';
                            while ($rowdes = mysqli_fetch_assoc($resdes)) {
                                $valordes = 0;

                                if ($idRetencion == $rowdes['codigo']) {
                                    $valorProcentaje = $val2 + $rowdes['porcentaje'];
                                } else {
                                    $valorProcentaje = $rowdes['porcentaje'];
                                    $idRetencion = $rowdes['codigo'];
                                }
                                $val2 = 0;
                                $val2 = $rowdes['porcentaje'];
                                if ($rowdes['iva'] > 0 && $rowdes['terceros'] == 1) {
                                    $val1 = $_POST['dvalores'][$j];
                                    $val3 = $_POST['ddesvalores'][$x];
                                    $valordes = round(($val1 / $_POST['valororden']) * ($val2 / 100) * $val3, 2);
                                } else {
                                    $val1 = $_POST['dvalores'][$j];
                                    $val3 = $_POST['ddesvalores'][$x];
                                    $valordes = round(($val1 / $_POST['valororden']) * ($val2 / 100) * $val3, 2);
                                }
                                /* echo $valorProcentaje."<br>"; */
                                if ($valorProcentaje <= 100) {
                                    $valopb += $valordes;
                                }

                                $sec_presu = intval($_POST['dseccion_presupuestal'][$j]);
                                $sqlrCc = "SELECT id_cc FROM centrocostos_seccionpresupuestal WHERE (id_sp = $sec_presu OR id_sp = '" . $_POST['dseccion_presupuestal'][$j] . "')";
                                $resCc = mysqli_query($linkbd_V7, $sqlrCc);
                                $rowCc = mysqli_fetch_row($resCc);
                                $cc = $rowCc[0];
                                $codigoRetencion = 0;
                                $rest = 0;
                                $codigoCausa = 0;
                                if ($_POST['medioDePago'] != '1') {
                                    //concepto contable //********************************************* */
                                    $rest = substr($rowdes['tipoconce'], 0, 2);
                                    $sq = "select fechainicial from conceptoscontables_det where codigo='" . $rowdes['conceptocausa'] . "' and modulo='" . $rowdes['modulo'] . "' and tipo='$rest' and fechainicial<'$fechaf' and cuenta!='' order by fechainicial asc";
                                    $re = mysqli_query($linkbd_V7, $sq);
                                    while ($ro = mysqli_fetch_assoc($re)) {
                                        $_POST['fechacausa'] = $ro["fechainicial"];
                                    }
                                    $sqlr = "select * from conceptoscontables_det where codigo='" . $rowdes['conceptocausa'] . "' and modulo='" . $rowdes['modulo'] . "' and cc='" . $cc . "' and tipo='$rest' and fechainicial='" . $_POST['fechacausa'] . "'";
                                    //echo $sqlr." hoal <br>";
                                    $rst = mysqli_query($linkbd_V7, $sqlr);
                                    $row1 = mysqli_fetch_assoc($rst);
                                    if ($row1['cuenta'] != '' && $valordes > 0) {
                                        //echo "Hola 5  ";
                                        $sqlr = "insert into comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle, cheque,valdebito, valcredito, estado, vigencia) values ('6 $ideg','" . $row1['cuenta'] . "','" . $_POST['tercero'] . "' ,'" . $cc . "' , 'Descuento " . $_POST['dndescuentos'][$x] . "',''," . round($valordes, 2) . ",0,'1' ,'" . $vigegreso . "')";
                                        mysqli_query($linkbd_V7, $sqlr);
                                    }

                                    //concepto contable //********************************************* */
                                    $rest = substr($rowdes['tipoconce'], -2);
                                    $sq = "select fechainicial from conceptoscontables_det where codigo='" . $rowdes['conceptoingreso'] . "' and modulo='" . $rowdes['modulo'] . "' and tipo='$rest' and fechainicial<'$fechaf' and cuenta!='' order by fechainicial asc";
                                    $re = mysqli_query($linkbd_V7, $sq);
                                    while ($ro = mysqli_fetch_assoc($re)) {
                                        $_POST['fechacausa'] = $ro["fechainicial"];
                                    }
                                    $sqlr = "select * from conceptoscontables_det where codigo='" . $rowdes['conceptoingreso'] . "' and modulo='" . $rowdes['modulo'] . "' and cc='" . $cc . "' and tipo='$rest' and fechainicial='" . $_POST['fechacausa'] . "'";
                                    //echo $sqlr." hoal <br>";
                                    $rst = mysqli_query($linkbd_V7, $sqlr);
                                    $row1 = mysqli_fetch_assoc($rst);
                                    if ($row1['cuenta'] != '' && $valordes > 0) {
                                        //echo "Hola 5  ";
                                        $sqlr = "insert into comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle, cheque,valdebito, valcredito, estado, vigencia) values ('6 $ideg','" . $row1['cuenta'] . "','" . $_POST['tercero'] . "' ,'" . $cc . "' , 'Descuento " . $_POST['dndescuentos'][$x] . "','',0," . round($valordes, 2) . ",'1' ,'" . $vigegreso . "')";
                                        mysqli_query($linkbd_V7, $sqlr);

                                        //$sqlr="insert into comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque,valdebito, valcredito,estado,vigencia) values ('6 $ideg','".$_POST['cuentaCredito'][$_POST['dcuentas'][$x].''.$_POST['drecursos'][$x].''.$_POST['productoServicio'][$x].''.$_POST['indicador_producto'][$x]]."','$_POST[tercero]','$_POST[cc]','Descuento ".$gdndescuentos[$x]."','','".$valordes."','0','1', '".$vigegreso."')";
                                        //mysqli_query($linkbd_V7, $sqlr);
                                    }

                                    //concepto contable //********************************************* */
                                    $rest = "SR";
                                    $sq = "select fechainicial from conceptoscontables_det where codigo='" . $rowdes['conceptosgr'] . "' and modulo='" . $rowdes['modulo'] . "' and tipo='$rest' and fechainicial<'$fechaf' and cuenta!='' order by fechainicial asc";
                                    $re = mysqli_query($linkbd_V7, $sq);
                                    while ($ro = mysqli_fetch_assoc($re)) {
                                        $_POST['fechacausa'] = $ro["fechainicial"];
                                    }
                                    $sqlr = "select * from conceptoscontables_det where codigo='" . $rowdes['conceptosgr'] . "' and modulo='" . $rowdes['modulo'] . "' and cc='" . $cc . "' and tipo='$rest' and fechainicial='" . $_POST['fechacausa'] . "'";
                                    //echo $sqlr." hoal <br>";
                                    $rst = mysqli_query($linkbd_V7, $sqlr);
                                    $row1 = mysqli_fetch_assoc($rst);
                                    if ($row1['cuenta'] != '' && $valordes > 0) {
                                        //echo "Hola 5  ";
                                        $sqlr = "insert into comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle, cheque,valdebito, valcredito, estado, vigencia) values ('6 $ideg','" . $row1['cuenta'] . "','" . $_POST['tercero'] . "' ,'" . $cc . "' , 'Descuento " . $_POST['dndescuentos'][$x] . "','',0," . round($valordes, 2) . ",'1' ,'" . $vigegreso . "')";
                                        mysqli_query($linkbd_V7, $sqlr);
                                    }

                                    continue;

                                } else {
                                    $codigoIngreso = $rowdes['conceptoingreso'];
                                    if ($codigoIngreso != "-1") {
                                        $codigoRetencion = $rowdes['conceptoingreso'];
                                        $rest = substr($rowdes['tipoconce'], -2);
                                        $val2 = $rowdes['porcentaje'];
                                    }
                                }
                                $sq = "select fechainicial from conceptoscontables_det where codigo='$codigoRetencion' and modulo='" . $rowdes['modulo'] . "' and tipo='$rest' and fechainicial<'$fechaf' and cuenta!='' order by fechainicial asc";
                                $re = mysqli_query($linkbd_V7, $sq);
                                while ($ro = mysqli_fetch_assoc($re)) {
                                    $_POST['fechacausa'] = $ro["fechainicial"];
                                }
                                $sqlr = "select * from conceptoscontables_det where codigo='$codigoRetencion' and modulo='" . $rowdes['modulo'] . "' and cc='" . $cc . "' and tipo='$rest' and fechainicial='" . $_POST['fechacausa'] . "'";
                                $rst = mysqli_query($linkbd_V7, $sqlr);
                                $row1 = mysqli_fetch_assoc($rst);
                                //TERMINA BUSQUEDA CUENTA CONTABLE////////////////////////
                                if ($row1['cuenta'] != '' && $valordes > 0) {
                                    $sqlr = "insert into comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle, cheque,valdebito, valcredito, estado, vigencia) values ('6 $ideg','" . $row1['cuenta'] . "','" . $_POST['tercero'] . "' ,'" . $cc . "' , 'Descuento " . $_POST['dndescuentos'][$x] . "','',0," . round($valordes, 2) . ",'1' ,'" . $vigegreso . "')";
                                    mysqli_query($linkbd_V7, $sqlr);
                                }

                                $realizaCausacion = 0;
                                if ($rowdes['terceros'] != 1 || $rowdes['tipo'] == 'C') {
                                    $realizaCausacion = 1;

                                    if ($rowdes['destino'] != 'M' && $rowdes['tipo'] == 'C') {
                                        $realizaCausacion = 0;
                                    }
                                }
                                if ($rowdes['conceptocausa'] != '-1' && $_POST['medioDePago'] == '1' && $realizaCausacion == 1) {
                                    //concepto contable //********************************************* */
                                    $rest = substr($rowdes['tipoconce'], 0, 2);
                                    $sq = "select fechainicial from conceptoscontables_det where codigo='" . $rowdes['conceptocausa'] . "' and modulo='" . $rowdes['modulo'] . "' and tipo='$rest' and fechainicial<'$fechaf' and cuenta!='' order by fechainicial asc";
                                    $re = mysqli_query($linkbd_V7, $sq);
                                    while ($ro = mysqli_fetch_assoc($re)) {
                                        $_POST['fechacausa'] = $ro["fechainicial"];
                                    }
                                    $sqlr = "select * from conceptoscontables_det where codigo='" . $rowdes['conceptocausa'] . "' and modulo='" . $rowdes['modulo'] . "' and cc='" . $cc . "' and tipo='$rest' and fechainicial='" . $_POST['fechacausa'] . "'";
                                    //echo $sqlr." hoal <br>";
                                    $rst = mysqli_query($linkbd_V7, $sqlr);
                                    $row1 = mysqli_fetch_assoc($rst);
                                    if ($row1['cuenta'] != '' && $valordes > 0) {
                                        //echo "Hola 5  ";
                                        $sqlr = "insert into comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle, cheque,valdebito, valcredito, estado, vigencia) values ('6 $ideg','" . $row1['cuenta'] . "','" . $_POST['tercero'] . "' ,'" . $cc . "' , 'Descuento " . $_POST['dndescuentos'][$x] . "',''," . round($valordes, 2) . ",0,'1' ,'" . $vigegreso . "')";
                                        mysqli_query($linkbd_V7, $sqlr);
                                        if ($valorProcentaje <= 100) {
                                            $sqlr = "insert into comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle, cheque,valdebito, valcredito, estado, vigencia) values ('6 $ideg','" . $row1['cuenta'] . "','" . $_POST['tercero'] . "' ,'" . $cc . "' , 'Descuento " . $_POST['dndescuentos'][$x] . "','',0," . round($valordes, 2) . ",'1' ,'" . $vigegreso . "')";
                                            mysqli_query($linkbd_V7, $sqlr);
                                        }
                                    }
                                }

                            }
                        }
                    }
                }
                $cantCuentas = count($_POST['dcuentas']);
                $cuentaResta = 0;
                $vigusu = $_POST['vigencia'];
                for ($x = 0; $x < $cantCuentas; $x++) {
                    $sec_presu = intval($_POST['dseccion_presupuestal'][$x]);
                    $sqlrCc = "SELECT id_cc FROM centrocostos_seccionpresupuestal WHERE (id_sp = $sec_presu OR id_sp = '" . $_POST['dseccion_presupuestal'][$x] . "')";
                    $resCc = mysqli_query($linkbd_V7, $sqlrCc);
                    $rowCc = mysqli_fetch_row($resCc);
                    $cc = $rowCc[0];
                    $valneto = $_POST['dvalores'][$x];

                    if ($opcion == "1") {
                        $valneto -= generaRetenciones($_POST['orden'], $_POST['dvalores'][$x]);
                    }
                    if ($valneto > 0) {
                        $sqlr = "insert into comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque,valdebito, valcredito, estado, vigencia) values ('6 $ideg','" . $_POST['cuentaDebito'][$x] . "','$_POST[tercero]','$cc','Pago " . $_POST['dncuentas'][$x] . "','','" . round($valneto, 2) . "',0,'1' ,'$vigegreso')";
                        mysqli_query($linkbd_V7, $sqlr);
                        ;
                        $valorades += round($valneto, 2);
                    }
                    $totdes = round($valneto, 2) - (round((round($valopb, 2) * round($valneto, 2) / round($_POST['valororden'], 2)), 2));
                    if ($_POST['codingreso'] != '') {
                        if ($_POST['entidadAdministradora'] == 'S') {
                            $sqlri = "SELECT * FROM tesomediodepago WHERE estado='S' AND id = '$_POST[codingreso]'";
                            $resi = mysqli_query($linkbd_V7, $sqlri);
                            $rowi = mysqli_fetch_row($resi);
                            if ($totdes > 0) {
                                $sqlr = "insert into comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque, valdebito,valcredito,estado,vigencia) values ('6 $ideg','" . $rowi[2] . "','" . $_POST['tercero'] . "' ,'$cc' , 'Pago Banco " . $_POST['dncuentas'][$x] . "','$_POST[cheque]  $_POST[ntransfe]',0," . round($totdes, 2) . ",'1' ,'" . $_POST['vigenciaop'] . "')";
                                mysqli_query($linkbd_V7, $sqlr);
                            }
                        } else {
                            $sqlri = "Select * from tesoingresos_det where codigo='" . $_POST['codingreso'] . "' and vigencia='$vigegreso'";
                            $resi = mysqli_query($linkbd_V7, $sqlri);
                            while ($rowi = mysqli_fetch_row($resi)) {
                                //**** busqueda concepto contable*****
                                $sq = "select fechainicial from conceptoscontables_det where codigo='$rowi[2]' and modulo='4' and tipo='C' and fechainicial<'$fechaf' and cuenta!='' order by fechainicial asc";
                                $re = mysqli_query($linkbd_V7, $sq);
                                while ($ro = mysqli_fetch_assoc($re)) {
                                    $_POST['fechacausa'] = $ro["fechainicial"];
                                }
                                $sqlrc = "Select * from conceptoscontables_det where estado='S' and modulo='4' AND codigo='$rowi[2]' and tipo='C' and fechainicial='" . $_POST['fechacausa'] . "'";
                                $resc = mysqli_query($linkbd_V7, $sqlrc);
                                while ($rowc = mysqli_fetch_row($resc)) {
                                    if ($cc == $rowc[5]) {
                                        if ($rowc[3] == 'N') {
                                            if ($rowc[6] == 'S') {
                                                if ($totdes > 0) {
                                                    $sqlr = "insert into comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque, valdebito,valcredito,estado,vigencia) values ('6 $ideg','" . $rowc[4] . "','" . $_POST['tercero'] . "' ,'$cc' , 'Pago Banco " . $_POST['dncuentas'][$x] . "','$_POST[cheque]  $_POST[ntransfe]',0," . round($totdes, 2) . ",'1' ,'" . $_POST['vigenciaop'] . "')";
                                                    //echo "5 $sqlr </br>";
                                                    mysqli_query($linkbd_V7, $sqlr);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    } else {
                        if ($totdes > 0) {
                            $sqlr = "insert into comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque, valdebito,valcredito,estado,vigencia) values ('6 $ideg','" . $_POST['banco'] . "','" . $_POST['tercero'] . "' ,'$cc' , 'Pago Banco " . $_POST['dncuentas'][$x] . "','$_POST[cheque]  $_POST[ntransfe]',0," . round($totdes, 2) . ",'1' ,'" . $_POST['vigenciaop'] . "')";
                            //echo "5 $sqlr </br>";
                            mysqli_query($linkbd_V7, $sqlr);
                        }
                    }
                }
                $sqlr = "update tesoegresos set id_comp=$idcomp where id_egreso=$ideg ";
                mysqli_query($linkbd_V7, $sqlr);
                $ideg = $_POST['egreso'];
                echo "
					<script>
						Swal.fire({
							icon: 'success',
							title: 'Se reflejo con exito el comprobante de egreso N°: $ideg',
							confirmButtonText: 'Continuar',
							confirmButtonColor: '#FF121A',
							timer: 3000
						});
					</script>";
            } else {
                echo "
					<script>
						Swal.fire({
							icon: 'error',
							title: 'Error!',
							text: 'No Tiene los Permisos para Modificar este Documento',
							confirmButtonText: 'Continuar',
							confirmButtonColor: '#FF121A',
							timer: 3000
						});
					</script>";
            }
        }
        ?>
        </form>
    </body>
</html>
