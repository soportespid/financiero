<?php 
	header("Content-Type: text/html;charset=utf-8");
    require_once 'PHPExcel/Classes/PHPExcel.php';
    require "comun.inc";
	
    session_start();
	$search = $_GET['search'];
	$total=0;

	$linkbd = conectar_v7();
    $objPHPExcel = new PHPExcel();
	$objPHPExcel->getActiveSheet()->getStyle('A:J')->applyFromArray(
		array(
			'alignment' => array(
				'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
				'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
			)
		)
	);
	$objPHPExcel->getProperties()
	->setCreator("IDEAL 10")
	->setLastModifiedBy("IDEAL 10")
	->setTitle("Exportar Excel con PHP")
	->setSubject("Documento de prueba")
	->setDescription("Documento generado con PHPExcel")
	->setKeywords("usuarios phpexcel")
	->setCategory("reportes");

	//----Cuerpo de Documento----
	$objPHPExcel->setActiveSheetIndex(0)
	->mergeCells('A1:e1')
	->mergeCells('A2:e2')
	->setCellValue('A1', 'ALMACÉN')
	->setCellValue('A2', 'REPORTE DE ARTÍCULOS');
	$objPHPExcel-> getActiveSheet ()
	-> getStyle ("A1")
	-> getFill ()
	-> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
	-> getStartColor ()
	-> setRGB ('C8C8C8');
	$objPHPExcel-> getActiveSheet ()
	-> getStyle ("A1:A2")
	-> getFont ()
	-> setBold ( true ) 
	-> setName ( 'Verdana' ) 
	-> setSize ( 10 ) 
	-> getColor ()
	-> setRGB ('000000');
	$objPHPExcel-> getActiveSheet ()
	-> getStyle ('A1:A2')
	-> getAlignment ()
	-> applyFromArray (array ( 'horizontal'  =>  PHPExcel_Style_Alignment :: HORIZONTAL_CENTER ,) );
	$objPHPExcel-> getActiveSheet ()
	-> getStyle ('A3:E3')
	-> getAlignment ()
	-> applyFromArray (array ( 'horizontal'  =>  PHPExcel_Style_Alignment :: VERTICAL_JUSTIFY ,) ); 
	$objPHPExcel-> getActiveSheet ()
	-> getStyle ("A2")
	-> getFill ()
	-> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
	-> getStartColor ()
	-> setRGB ('A6E5F3');
	$objPHPExcel-> getActiveSheet ()
	-> getStyle ("A3:E3")
	-> getFill ()
	-> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
	-> getStartColor ()
	-> setRGB ('22C6CB');
	$borders = array(
		'borders' => array(
			'allborders' => array(
				'style' => PHPExcel_Style_Border::BORDER_THIN,
			'color' => array('argb' => 'FF000000'),
			)
		),
	);
	$objPHPExcel->getActiveSheet()->getStyle('A1:E1')->applyFromArray($borders);
	$objPHPExcel->getActiveSheet()->getStyle('A2:E2')->applyFromArray($borders);
	$objPHPExcel->getActiveSheet()->getStyle('A3:E3')->applyFromArray($borders);
	$objWorksheet = $objPHPExcel->getActiveSheet();

	$objPHPExcel->setActiveSheetIndex(0)
	->setCellValue('A3', 'Código de artículo')
	->setCellValue('B3', 'Grupo de inventario')
	->setCellValue('C3', 'Código CPC')
	->setCellValue('D3', 'Nombre')
	->setCellValue('E3', 'Unidad de medida');

	$sql = "SELECT codigo,nombre,codunspsc,grupoinven,estado, CONCAT(grupoinven,codigo) as codigo_articulo
            FROM almarticulos 
            WHERE nombre like '$search%' OR CONCAT(grupoinven,codigo) like '$search%' 
            ORDER BY length(codigo),codigo ASC";
	$request = mysqli_fetch_all(mysqli_query($linkbd,$sql),MYSQLI_ASSOC);
    $total = count($request);   
    for ($i=0; $i < $total ; $i++) { 
        $strCodigoGrupo = $request[$i]['grupoinven'];
		$strCodigoArticulo = $strCodigoGrupo.$request[$i]['codigo'];
		$strCodigoBien = $request[$i]['codunspsc'];
		$sql = "SELECT unidad FROM almarticulos_det WHERE articulo = '$strCodigoArticulo'";
		$request[$i]['unidad'] = mysqli_query($linkbd,$sql)->fetch_assoc()['unidad'];
		$request[$i]['grupoinven'] = $strCodigoGrupo." - ".mysqli_query(
			$linkbd,"SELECT nombre FROM almgrupoinv WHERE codigo ='$strCodigoGrupo'"
		)->fetch_assoc()['nombre'];
		$request[$i]['codunspsc'] = $strCodigoBien." - ".mysqli_query(
			$linkbd,
			"SELECT titulo FROM ccpetbienestransportables 
			WHERE version = (SELECT MAX(version) FROM ccpetbienestransportables)  AND LENGTH(grupo) = 7 AND grupo ='$strCodigoBien'"
		)->fetch_assoc()['titulo'];
    }
	$rowExcel = 4;
	
	foreach ($request as $data) {
		$objPHPExcel->setActiveSheetIndex(0)
		->setCellValueExplicit ("A$rowExcel", $data['codigo_articulo'], PHPExcel_Cell_DataType :: TYPE_STRING)
		->setCellValueExplicit ("B$rowExcel", $data['grupoinven'], PHPExcel_Cell_DataType :: TYPE_STRING)
		->setCellValueExplicit ("C$rowExcel", $data['codunspsc'], PHPExcel_Cell_DataType :: TYPE_STRING)
		->setCellValueExplicit ("D$rowExcel", strtoupper($data['nombre']), PHPExcel_Cell_DataType :: TYPE_STRING)
		->setCellValueExplicit ("E$rowExcel", $data['unidad'], PHPExcel_Cell_DataType :: TYPE_STRING);
		$objPHPExcel->getActiveSheet()->getStyle("A$rowExcel:E$rowExcel")->applyFromArray($borders);
		$rowExcel++;
	}
	
	$objPHPExcel->getActiveSheet()->getStyle("A$rowExcel")->getFont()->setBold(true);
	$objPHPExcel->getActiveSheet()->getStyle("A$rowExcel:H$rowExcel")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
	$objPHPExcel->getActiveSheet()->getStyle("H4:H$rowExcel")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
	$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true); 
	$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true); 
	$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true); 
	$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true); 
	$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true); 
	$objPHPExcel->getActiveSheet()->setTitle('Reporte articulos');
	$objPHPExcel->setActiveSheetIndex(0);
	
    //----Guardar documento----
    header('Content-Type: application/vnd.ms-excel');
	header('Content-Disposition: attachment;filename="reporte-articulos.xlsx"');
	header('Cache-Control: max-age=0');
    $objWriter=PHPExcel_IOFactory::createWriter($objPHPExcel,'Excel2007');
    $objWriter->save('php://output');
    die();
?>