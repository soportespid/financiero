<?php

    header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require 'comun.inc';
	require 'funciones.inc';
	session_start();
    if(empty($_SESSION)){
        header("location: index.php");
    }
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html lang=es>
	<head>
    <meta charset="utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1"/>
		<meta name="viewport" content="user-scalable=no">
		<title>:: IDEAL 10 - Servicios publicos</title>
		<link href="favicon.ico" rel="shortcut icon"/>
		<link href="css/css2z.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
        <link href="css/tabs.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
        <link href="css/style.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" href="sweetalert2/dist/sweetalert2.min.css">
        <script type="text/javascript" src="css/programas.js"></script>
        <script src="sweetalert2/dist/sweetalert2.min.js"></script>
		<script src="vue/vue.min.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script src="css/calendario.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
	</head>
	<body>
		<header>
			<table>
				<tr><script>barra_imagenes("serv");</script><?php cuadro_titulos();?></tr>
			</table>
		</header>

        <section id="myapp" v-cloak>
            <div class="loading-container" v-show="isLoading" >
                <p class="text-loading"data-loading="IDEAL 10">IDEAL 10</p>
            </div>
            <nav>
                <table>
                    <tr><?php menu_desplegable("serv");?></tr>
                </table>
                <div class="bg-white group-btn p-1" id="newNavStyle">
                    <button type="button" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center" @click="mypop=window.open('serv-principal.php','',''); mypop.focus();">
                        <span>Nueva ventana</span>
                        <svg class="fill-black group-hover:fill-white w-5 h-5" viewBox="0 -960 960 960" ><path d="M320-120v-80h80v-80H160q-33 0-56.5-23.5T80-360v-400q0-33 23.5-56.5T160-840h640q33 0 56.5 23.5T880-760v400q0 33-23.5 56.5T800-280H560v80h80v80H320ZM160-360h640v-400H160v400Zm0 0v-400 400Z"/></svg>
                    </button>
                    <!-- <button type="button" class="btn btn-danger btn-danger-hover d-flex justify-between align-items-center" @click="downloadFormat('pdf')">
                        <span>Exportar PDF</span>
                        <svg viewBox="0 0 512 512"><path d="M64 464l48 0 0 48-48 0c-35.3 0-64-28.7-64-64L0 64C0 28.7 28.7 0 64 0L229.5 0c17 0 33.3 6.7 45.3 18.7l90.5 90.5c12 12 18.7 28.3 18.7 45.3L384 304l-48 0 0-144-80 0c-17.7 0-32-14.3-32-32l0-80L64 48c-8.8 0-16 7.2-16 16l0 384c0 8.8 7.2 16 16 16zM176 352l32 0c30.9 0 56 25.1 56 56s-25.1 56-56 56l-16 0 0 32c0 8.8-7.2 16-16 16s-16-7.2-16-16l0-48 0-80c0-8.8 7.2-16 16-16zm32 80c13.3 0 24-10.7 24-24s-10.7-24-24-24l-16 0 0 48 16 0zm96-80l32 0c26.5 0 48 21.5 48 48l0 64c0 26.5-21.5 48-48 48l-32 0c-8.8 0-16-7.2-16-16l0-128c0-8.8 7.2-16 16-16zm32 128c8.8 0 16-7.2 16-16l0-64c0-8.8-7.2-16-16-16l-16 0 0 96 16 0zm80-112c0-8.8 7.2-16 16-16l48 0c8.8 0 16 7.2 16 16s-7.2 16-16 16l-32 0 0 32 32 0c8.8 0 16 7.2 16 16s-7.2 16-16 16l-32 0 0 48c0 8.8-7.2 16-16 16s-16-7.2-16-16l0-64 0-64z"/></svg>
                    </button> -->
                    <!-- <button type="button" class="btn btn-success btn-success-hover d-flex justify-between align-items-center" @click="">
                        <span>Exportar Excel</span>
                        <svg class="fill-black group-hover:fill-white w-4 h-4" viewBox="0 0 384 512"><path d="M48 448V64c0-8.8 7.2-16 16-16H224v80c0 17.7 14.3 32 32 32h80V448c0 8.8-7.2 16-16 16H64c-8.8 0-16-7.2-16-16zM64 0C28.7 0 0 28.7 0 64V448c0 35.3 28.7 64 64 64H320c35.3 0 64-28.7 64-64V154.5c0-17-6.7-33.3-18.7-45.3L274.7 18.7C262.7 6.7 246.5 0 229.5 0H64zm90.9 233.3c-8.1-10.5-23.2-12.3-33.7-4.2s-12.3 23.2-4.2 33.7L161.6 320l-44.5 57.3c-8.1 10.5-6.3 25.5 4.2 33.7s25.5 6.3 33.7-4.2L192 359.1l37.1 47.6c8.1 10.5 23.2 12.3 33.7 4.2s12.3-23.2 4.2-33.7L222.4 320l44.5-57.3c8.1-10.5 6.3-25.5-4.2-33.7s-25.5-6.3-33.7 4.2L192 280.9l-37.1-47.6z"/></svg>
                    </button> -->
                    <button type="button" class="btn btn-success d-flex justify-between align-items-center" @click="location.href='serv-menuInformes'">
                        <span>Atrás</span>
                        <svg viewBox="0 -960 960 960"><path d="M760-200v-160q0-50-35-85t-85-35H273l144 144-57 56-240-240 240-240 57 56-144 144h367q83 0 141.5 58.5T840-360v160h-80Z"/></svg>
                    </button>
                </div>
            </nav>
            <article>
                <div class="bg-white">
                    <div>
                        <div class="d-flex">
                            <div class="form-control w-15">
                                <label class="form-label m-0" for="">Fecha inicial <span class="text-danger fw-bolder">*</span>:</label>
                                <input type="date" class="text-center" v-model="fechaIni">
                            </div>

                            <div class="form-control w-15">
                                <label class="form-label m-0" for="">Fecha final <span class="text-danger fw-bolder">*</span>:</label>
                                <input type="date" class="text-center" v-model="fechaFin">
                            </div>
                            
                            <div class="form-control justify-between w-15">
                                <label for=""></label>
                                <button type="button" class="btn btn-primary" @click="searchData()">Generar reporte</button>
                            </div>
                        </div>
                        <h2 class="titulos m-0">Reporte seguimiento recaudos:</h2>
                    </div>

                    <div class="table-responsive">
                        <table class="table table-hover fw-normal">
                            <thead>
                                <tr class="text-center">
                                    <th>Id</th>
                                    <th>Tipo de recaudo</th>
                                    <th>Consecutivo</th>
                                    <th>Fecha</th>
                                    <th>Servicios publicos</th>
                                    <th>Presupuesto</th>
                                    <th>Reflejar <button type="button" class="btn btn-primary btn-sm" v-if="valorTotalModulo != valorTotalPresu" @click="refejaMasivoPptoRecaudo()" >Reflejar</button></th>
                                    <th>Contabilidad</th>
                                    <th>Reflejar</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr v-for="(dato,index) in datos" :key="index">
                                    <td class="text-center">{{index+1}}</td>
                                    <td class="text-center">{{dato.tipoRecaudo}}</td>
                                    <td class="text-center">{{dato.consecutivo}}</td>
                                    <td class="text-center">{{dato.fecha}}</td>
                                    <td class="text-right">{{formatNumero(dato.value_sp)}}</td>
                                    <td class="text-right">
                                        <span v-bind:class="[dato.value_ppto != dato.value_sp ? 'text-danger' : '']">
                                            {{formatNumero(dato.value_ppto)}}
                                        </span>
                                    </td>
                                    <td class="text-center">
                                        <button type="button" class="btn btn-primary btn-sm" v-if="dato.value_ppto != dato.value_sp" @click="reflejarPresu(dato.tipoRecaudo, dato.consecutivo)" >Reflejar</button>
                                    </td>
                                    <td class="text-right">
                                        <span v-bind:class="[dato.value_cont != dato.value_sp ? 'text-danger' : '']">
                                            {{formatNumero(dato.value_cont)}}
                                        </span>
                                    </td>
                                    <td class="text-center">
                                        <button type="button" class="btn btn-primary btn-sm" v-if="dato.value_cont != dato.value_sp" @click="reflejarCont(dato.tipoRecaudo, dato.consecutivo)" >Reflejar</button>
                                    </td>
                                </tr>
                            </tbody>
                            <tfoot>
                                <tr class="fw-bold">
                                    <td class="text-right" colspan="4">Total:</td>
                                    <td class="text-right">{{formatNumero(valorTotalModulo)}}</td>
                                    <td class="text-right">{{formatNumero(valorTotalPresu)}}</td>
                                    <td></td> 
                                    <td class="text-right">{{formatNumero(valorTotalCont)}}</td>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </article>
        </section>

		<script src="node_modules/read-excel-file/bundle/read-excel-file.min.js"></script>
		<script src="node_modules/xlsx/dist/xlsx.full.min.js"></script>
        <script src="Librerias/vue/vue.min.js"></script>
		<script src="Librerias/vue/axios.min.js"></script>
		<script type="module" src="servicios_publicos/reporteSeguimientoRecaudos/serv-reporteSeguimientoRecaudos.js"></script>

	</body>
</html>
