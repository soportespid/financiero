<?php

    header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require 'comun.inc';
	require 'funciones.inc';
	session_start();
    if(empty($_SESSION)){
        header("location: index.php");
    }
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html lang=es>
	<head>
    <meta charset="utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1"/>
		<meta name="viewport" content="user-scalable=no">
		<title>:: IDEAL 10 - Informes</title>
		<link href="favicon.ico" rel="shortcut icon"/>
		<link href="css/css2z.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
        <link href="css/tabs.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
        <link href="css/style.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" href="sweetalert2/dist/sweetalert2.min.css">
        <script type="text/javascript" src="css/programas.js"></script>
        <script src="sweetalert2/dist/sweetalert2.min.js"></script>
		<script src="vue/vue.min.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script src="css/calendario.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
	</head>
	<body>
		<header>
			<table>
				<tr><script>barra_imagenes("info");</script><?php cuadro_titulos();?></tr>
			</table>
		</header>

        <main id="myapp" v-cloak>
            <div class="loading-container" v-show="isLoading" >
                <p class="text-loading"data-loading="IDEAL 10">IDEAL 10</p>
            </div>
            <nav>
                <table>
                    <tr><?php menu_desplegable("info");?></tr>
                </table>
                <div class="bg-white group-btn p-1" id="newNavStyle">
                    <button type="button" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center" @click="mypop=window.open('info-principal.php','',''); mypop.focus();">
                        <span>Nueva ventana</span>
                        <svg class="fill-black group-hover:fill-white w-5 h-5" xmlns="http://www.w3.org/2000/svg"  viewBox="0 -960 960 960" ><path d="M320-120v-80h80v-80H160q-33 0-56.5-23.5T80-360v-400q0-33 23.5-56.5T160-840h640q33 0 56.5 23.5T880-760v400q0 33-23.5 56.5T800-280H560v80h80v80H320ZM160-360h640v-400H160v400Zm0 0v-400 400Z"/></svg>
                    </button>
                    <button type="button" @click="mypop=window.open('ccp-cascadaEjecucionMensual.php','','');mypop.focus();" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
                        <span class="group-hover:text-white">Duplicar pantalla</span>
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960"><path d="M640-160v-360H160v360h480Zm80-200v-80h80v-360H320v200h-80v-200q0-33 23.5-56.5T320-880h480q33 0 56.5 23.5T880-800v360q0 33-23.5 56.5T800-360h-80ZM160-80q-33 0-56.5-23.5T80-160v-360q0-33 23.5-56.5T160-600h480q33 0 56.5 23.5T720-520v360q0 33-23.5 56.5T640-80H160Zm400-603ZM400-340Z"></path></svg>
                    </button>
                </div>
            </nav>
            <section class="bg-white">
                <div>
                    <h2 class="titulos m-0">Cascada de recursos - ejecución mensual</h2>
                    <p class="mb-0 ms-2 ">Todos los campos con (<span class="text-danger fw-bolder">*</span>) son obligatorios</p>
                    <div class="d-flex w-50">
                        <div class="form-control">
                            <label class="form-label">Vigencia:</label>
                            <select v-model="selectVigencia" @change="getData">
                                <option v-for="(data,index) in arrFechas" :key="index" :value="data">{{ data}}</option>
                            </select>
                        </div>
                        <div class="form-control">
                            <label class="form-label">Mes:</label>
                            <select v-model="selectMes" @change="getData">
                                <option v-for="(data,index) in arrMeses" :key="index" :value ="data.valor">{{data.nombre}}</option>
                            </select>
                        </div>
                        <div class="form-control">
                            <label class="form-label">Medio de pago:</label>
                            <select v-model="selectPago" @change="getData">
                                <option value="">Todos</option>
                                <option value="CSF">CSF</option>
                                <option value="SSF">SSF</option>
                            </select>
                        </div>
                        <div class="form-control">
                            <label class="form-label">Vigencia de gasto:</label>
                            <select v-model="selectGasto" @change="getData">
                                <option value="">Todos</option>
                                <option value="1">Vigencia actual</option>
                                <option value="2">Reservas</option>
                                <option value="3">Cuentas por pagar</option>
                                <option value="4">Vigencias futuras - vigencia actual</option>
                                <option value="5">Vigencias futuras - reservas</option>
                                <option value="6">Vigencias futuras - cuentas por pagar</option>
                            </select>
                        </div>
                    </div>
                    <div>
                        <table class="table">
                            <thead>
                                <th></th>
                                <th>Inversión</th>
                                <th>Funcionamiento</th>
                                <th>Operación</th>
                                <th>Deuda</th>
                                <th>Total</th>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>Aprop. Inicial + Novedades:</td>
                                    <td class="text-right">{{ objInicial.inversion }}</td>
                                    <td class="text-right">{{ objInicial.funcionamiento }}</td>
                                    <td class="text-right">{{ objInicial.operacion }}</td>
                                    <td class="text-right">{{ objInicial.deuda }}</td>
                                    <td class="text-right">{{ objInicial.total }}</td>
                                </tr>
                                <tr>
                                    <td>R.P.s/Compromisos:</td>
                                    <td class="text-right">{{ objRps.inversion }}</td>
                                    <td class="text-right">{{ objRps.funcionamiento }}</td>
                                    <td class="text-right">{{ objRps.operacion }}</td>
                                    <td class="text-right">{{ objRps.deuda }}</td>
                                    <td class="text-right">{{ objRps.total }}</td>
                                </tr>
                                <tr>
                                    <td>Saldo Disponible:</td>
                                    <td class="text-right">{{objSaldos.inversion}}</td>
                                    <td class="text-right">{{objSaldos.funcionamiento}}</td>
                                    <td class="text-right">{{objSaldos.operacion}}</td>
                                    <td class="text-right">{{objSaldos.deuda}}</td>
                                    <td class="text-right">{{objSaldos.total}}</td>
                                </tr>
                                <tr>
                                    <td colspan="6" class="bg-secondary text-center">{{arrMeses[selectMes].nombre +" - "+selectVigencia}}</td>
                                </tr>
                                <tr>
                                    <td>C.D.P.s:</td>
                                    <td class="text-right">{{objActual.cdps.inversion}}</td>
                                    <td class="text-right">{{objActual.cdps.funcionamiento}}</td>
                                    <td class="text-right">{{objActual.cdps.operacion}}</td>
                                    <td class="text-right">{{objActual.cdps.deuda}}</td>
                                    <td class="text-right">{{objActual.cdps.total}}</td>
                                </tr>
                                <tr>
                                    <td>R.P.s/Compromisos:</td>
                                    <td class="text-right">{{objActual.rps.inversion}}</td>
                                    <td class="text-right">{{objActual.rps.funcionamiento}}</td>
                                    <td class="text-right">{{objActual.rps.operacion}}</td>
                                    <td class="text-right">{{objActual.rps.deuda}}</td>
                                    <td class="text-right">{{objActual.rps.total}}</td>
                                </tr>
                                <tr>
                                    <td>Obligaciones:</td>
                                    <td class="text-right">{{objActual.obligaciones.inversion}}</td>
                                    <td class="text-right">{{objActual.obligaciones.funcionamiento}}</td>
                                    <td class="text-right">{{objActual.obligaciones.operacion}}</td>
                                    <td class="text-right">{{objActual.obligaciones.deuda}}</td>
                                    <td class="text-right">{{objActual.obligaciones.total}}</td>
                                </tr>
                                <tr>
                                    <td>Pagos:</td>
                                    <td class="text-right">{{objActual.pagos.inversion}}</td>
                                    <td class="text-right">{{objActual.pagos.funcionamiento}}</td>
                                    <td class="text-right">{{objActual.pagos.operacion}}</td>
                                    <td class="text-right">{{objActual.pagos.deuda}}</td>
                                    <td class="text-right">{{objActual.pagos.total}}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </section>
        </main>
		<script src="node_modules/read-excel-file/bundle/read-excel-file.min.js"></script>
		<script src="node_modules/xlsx/dist/xlsx.full.min.js"></script>
        <script src="Librerias/vue/vue.min.js"></script>
		<script src="Librerias/vue/axios.min.js"></script>
		<script type="module" src="presupuesto_ccpet/cascada_recursos/js/functions_ejecucion.js?<?= date('d_m_Y_h_i_s');?>"></script>
	</body>
</html>
