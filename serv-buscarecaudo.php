<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require"comun.inc";
	require"funciones.inc";
	$linkbd=conectar_v7();
	$linkbd -> set_charset("utf8");
	cargarcodigopag($_GET[codpag],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es">
	<head>
		<meta http-equiv="Content-Type" content="text/html" charset="utf8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=9"/>
		<title>:: SPID - Servicios Publicos</title>
		<link href="css/css2.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<script type="text/javascript" src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script type="text/javascript" src="css/calendario.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script type="text/javascript" src="jquery-1.11.0.min.js"></script> 
		<script>
			function verUltimaPos(idcta, filas, filtro)
			{
				var scrtop=$('#divdet').scrollTop();
				var altura=$('#divdet').height();
				var numpag=$('#nummul').val();
				var limreg=$('#numres').val();
				if((numpag<=0)||(numpag==""))
					numpag=0;
				if((limreg==0)||(limreg==""))
					limreg=10;
				numpag++;
				location.href="serv-recaudo_regrabar.php?idr="+idcta+"&scrtop="+scrtop+"&totreg="+filas+"&altura="+altura+"&numpag="+numpag+"&limreg="+limreg+"&filtro="+filtro;
			}
			function despliegamodalm(_valor,_tip,mensa,pregunta)
			{
				document.getElementById("bgventanamodalm").style.visibility=_valor;
				if(_valor=="hidden"){document.getElementById('ventanam').src="";}
				else
				{
					switch(_tip)
					{
						case "1":	document.getElementById('ventanam').src="ventana-mensaje1.php?titulos="+mensa;break;
						case "2":	document.getElementById('ventanam').src="ventana-mensaje3.php?titulos="+mensa;break;
						case "3":	document.getElementById('ventanam').src="ventana-mensaje2.php?titulos="+mensa;break;
						case "4":	document.getElementById('ventanam').src="ventana-consulta1.php?titulos="+mensa+"&idresp="+pregunta;break;
					}
				}
			}
			function botanular(idrec,idfac)
			{
				document.getElementById('delrecibo').value=idrec;
				document.getElementById('delfac').value=idfac;
				despliegamodalm('visible','4','Esta Seguro de ANULAR el Recibo de Recaudo N� '+idrec,'1');
			}
			function respuestaconsulta(pregunta)
			{
				switch(pregunta)
				{
					case "1":	document.getElementById('oculto').value='3';
								document.form2.submit();
								break;
				}
			}
			function pdf()
			{
				/*
				document.form2.action="serv-pdfrecaja.php";
				document.form2.target="_BLANK";
				document.form2.submit(); 
				document.form2.action="";
				document.form2.target="";
				*/
			}
			function fexportar ()
			{
				document.form2.action="serv-recaudosbuscarexcel.php";
				document.form2.target="_BLANK";
				document.form2.submit(); 
				document.form2.action="";
				document.form2.target="";
			}
		</script>
		<?php
			titlepag();
			$scrtop=$_GET['scrtop'];
			if($scrtop=="") $scrtop=0;
			echo"<script>
				window.onload=function(){
					$('#divdet').scrollTop(".$scrtop.")
				}
			</script>";
			$gidcta=$_GET['idcta'];
			if(isset($_GET['filtro']))
			$_POST['nombre']=$_GET['filtro'];
		?>
	</head>
	<body>
		<IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
		<span id="todastablas2"></span>
		<table>
			<tr><script>barra_imagenes("serv");</script><?php cuadro_titulos();?></tr>
			<tr><?php menu_desplegable("serv");?></tr>
			<tr>
				<td colspan="3" class="cinta"><img src="imagenes/add.png" title="Nuevo" onClick="location.href='serv-recaudo.php'" class="mgbt"/> <img src="imagenes/guardad.png" class="mgbt1"/><img src="imagenes/busca.png" title="Buscar" onClick="document.form2.submit();" class="mgbt"/><img src="imagenes/nv.png" title="Nueva Ventana" class="mgbt" onClick="mypop=window.open('serv-principal.php','','');mypop.focus();"/><img src="imagenes/excel.png" title="Exportar" onClick="fexportar()" class="mgbt"/></td>
			</tr>
		</table>
		<div id="bgventanamodalm" class="bgventanamodalm">
			<div id="ventanamodalm" class="ventanamodalm">
				<IFRAME src="" name="ventanam" marginWidth=0 marginHeight=0 frameBorder=0 id="ventanam" frameSpacing=0 style=" width:700px; height:130px; top:200; overflow:hidden;"></IFRAME>
			</div>
		</div>
		<form name="form2" method="post" action="serv-buscarecaudo.php">
			<?php
				if($_GET['numpag']!="")
				{
					$oculto=$_POST['oculto'];
					if($oculto!=2)
					{
						$_POST['numres']=$_GET['limreg'];
						$_POST['numpos']=$_GET['limreg']*($_GET['numpag']-1);
						$_POST['nummul']=$_GET['numpag']-1;
					}
				}
				else
				{
					if($_POST['nummul']=="")
					{
						$_POST['numres']=10;
						$_POST['numpos']=0;
						$_POST['nummul']=0;
					}
				}
			?>
			<table  class="inicio" align="center" >
				<tr>
					<td class="titulos" colspan="6">:. Buscar Recaudos </td>
					<td class="cerrar" style="width:7%" onClick="location.href='serv-principal.php'">Cerrar</td>
				</tr>
				<tr>
					<td width="162" class="saludo1">No Recibo:</td>
					<td><input name="numero" type="text" value="<?php echo @ $_POST['numero'];?>" size="10"></td>
					<td width="131" class="saludo1">Factura: </td>
					<td><input name="nombre" type="text" value="<?php echo @ $_POST['nombre'];?>" size="40" ></td>
					
				</tr>
			</table>
			<input type="hidden" name="oculto" id="oculto" value="1">
			<input type="hidden" name="numres" id="numres" value="<?php echo @ $_POST['numres'];?>"/>
			<input type="hidden" name="numpos" id="numpos" value="<?php echo @ $_POST['numpos'];?>"/>
			<input type="hidden" name="nummul" id="nummul" value="<?php echo @ $_POST['nummul'];?>"/>
			<input type="hidden" name="delrecibo" id="delrecibo" value="<?php echo @ $_POST['delrecibo'];?>"/>
			<input type="hidden" name="delfac" id="delfac" value="<?php echo @ $_POST['delfac'];?>"/>
			<div class="subpantalla" style="height:68.5%; width:99.6%;overflow-x:hidden" id="divdet">
				<?php
					if(@ $_POST['oculto']=="3")
					{
						$sqlr="SELECT MAX(numinicial) FROM servliquidaciones_gen ";
						$resp = mysqli_query($linkbd,$sqlr);
						$row =mysqli_fetch_row($resp);
						if ($_POST['delfac']>=$row[0])
						{
							$totalgeneral=0;
							$sqlr="UPDATE servreciboscaja SET estado='N' WHERE id_recibos='".$_POST['delrecibo']."'";
							mysqli_query($linkbd,$sqlr);
							$sqlr="SELECT ingreso,valor FROM servreciboscaja_det WHERE id_recibos='".$_POST['delrecibo']."'";
							$resp = mysqli_query($linkbd,$sqlr);
							while ($row =mysqli_fetch_row($resp))
							{
								$sqlr1="SELECT abono FROM servliquidaciones_det WHERE id_liquidacion='".$_POST['delfac']."' AND servicio='$row[0]'";
								$resp1 = mysqli_query($linkbd,$sqlr1);
								$row1 =mysqli_fetch_row($resp1);
								$valsaldo=$row1[0]-$row[1];
								$totalgeneral=$totalgeneral+$valsaldo;
								if($valsaldo>=0)
								{
									if($valsaldo==0){$valestado='';}
									else{$valestado='A';}
									$sqlr2="UPDATE servliquidaciones_det SET abono='$valsaldo',estado='$valestado' WHERE id_liquidacion='".$_POST['delfac']."' AND servicio='$row[0]'";
									mysqli_query($linkbd,$sqlr2);
								}
							}
							if($totalgeneral==0){$valestado2='S';}
							else{$valestado2='A';}
							$sqlr="UPDATE servliquidaciones SET estado='$valestado2' WHERE id_liquidacion='".$_POST['delfac']."'";
							mysqli_query($linkbd,$sqlr);
							$sqlr="UPDATE servfacturas SET estado='$valestado2' WHERE id_factura='".$_POST['delfac']."'";
							mysqli_query($linkbd,$sqlr);
						}
						else
						{
							echo "<script>despliegamodalm('visible','2','El Recibo de Recaudo pertenece a un Ciclo de Facturacion que ya tiene Cierre');</script>";
						}
						
					}
					$crit1=" ";
					$crit2=" ";
					if (@$_POST['numero']!="")
					{$crit1="WHERE id_recibos like '%".$_POST['numero']."%' ";}
					if ($_POST[nombre]!="")
					{$crit2=" WHERE  id_recaudo like '%".$_POST['nombre']."%'  ";}
					$sqlr="SELECT * FROM servreciboscaja $crit1 $crit2";
					$resp = mysqli_query($linkbd,$sqlr);
					$_POST['numtop']=mysqli_num_rows($resp);
					$nuncilumnas=ceil($_POST['numtop']/$_POST['numres']);
					$cond2="";
					if ($_POST['numres']!="-1"){$cond2="LIMIT ".$_POST['numpos'].", ".$_POST['numres'];}
					$sqlr="SELECT * FROM servreciboscaja $crit1 $crit2 ORDER BY id_recibos DESC $cond2";
					$resp = mysqli_query($linkbd,$sqlr);
					$con=1;
					$numcontrol=$_POST['nummul']+1;
					if(($nuncilumnas==$numcontrol)||($_POST['numres']=="-1"))
					{
						$imagenforward="<img src='imagenes/forward02.png' style='width:17px'>";
						$imagensforward="<img src='imagenes/skip_forward02.png' style='width:16px' >";
					}
					else 
					{
						$imagenforward="<img src='imagenes/forward01.png' style='width:17px' title='Siguiente' onClick='numsiguiente()'>";
						$imagensforward="<img src='imagenes/skip_forward01.png' style='width:16px' title='Fin' onClick='saltocol(\"$nuncilumnas\")'>";
					}
					if(($_POST['numpos']==0)||($_POST['numres']=="-1"))
					{
						$imagenback="<img src='imagenes/back02.png' style='width:17px'>";
						$imagensback="<img src='imagenes/skip_back02.png' style='width:16px'>";
					}
					else
					{
						$imagenback="<img src='imagenes/back01.png' style='width:17px' title='Anterior' onClick='numanterior();'>";
						$imagensback="<img src='imagenes/skip_back01.png' style='width:16px' title='Inicio' onClick='saltocol(\"1\")'>";
					}
					echo "
					<table class='inicio' align='center' >
						<tr>
							<td colspan='7' class='titulos'>.: Resultados Busqueda:</td>
							<td class='submenu'>
								<select name='renumres' id='renumres' onChange='cambionum();' style='width:100%'>
									<option value='10'"; if ($_POST[renumres]=='10'){echo 'selected';} echo ">10</option>
									<option value='20'"; if ($_POST[renumres]=='20'){echo 'selected';} echo ">20</option>
									<option value='30'"; if ($_POST[renumres]=='30'){echo 'selected';} echo ">30</option>
									<option value='50'"; if ($_POST[renumres]=='50'){echo 'selected';} echo ">50</option>
									<option value='100'"; if ($_POST[renumres]=='100'){echo 'selected';} echo ">100</option>
									<option value='-1'"; if ($_POST[renumres]=='-1'){echo 'selected';} echo ">Todos</option>
								</select>
							</td>
						</tr>
						<tr><td colspan='7'>Variables Encontradas: $_POST[numtop]</td></tr>
						<tr>
							<td class='titulos2' style='width:10%;'>No Recibo</td>
							<td class='titulos2' style='width:10%;'>Factura</td>
							<td class='titulos2' style='width:10%;'>Fecha</td>
							<td class='titulos2'>Cliente</td>
							<td class='titulos2' style='width:15%;'>Valor</td>
							<td class='titulos2' width='5%'>Estado</td>
							<td class='titulos2' width='5%'><center>Anular</td>
							<td class='titulos2' width='5%'><center>Editar</td>
						</tr>";	
					$iter='saludo1a';
					$iter2='saludo2';
					while ($row =mysqli_fetch_row($resp)) 
					{
						if($row[9]=='S')
						{
							$imgsem="src='imagenes/sema_verdeON.jpg' title='Activo'";
							$imganu="src='imagenes/anular.png' title='Anular' onClick=\"botanular('$row[0]','$row[4]');\"";
						}
						else 
						{
							$imgsem="src='imagenes/sema_rojoON.jpg' title='Anulado'";
							$imganu="src='imagenes/anulard.png' title='Anulado'";
						}
						$sqlr2="SELECT T1.nombretercero FROM servclientes AS T1 INNER JOIN servliquidaciones AS T2 ON T2.codusuario=T1.codigo WHERE T2.id_liquidacion='$row[4]'";
						$resp2 = mysqli_query($linkbd,$sqlr2);
						$row2 = mysqli_fetch_row($resp2);
						$cliente = $row2[0];
						echo "
							<tr class='$iter'>
								<td class='icoop'>$row[0]</td>
								<td class='icoop'>$row[4]</td>
								<td class='icoop'>$row[2]</td>
								<td class='icoop'>$cliente</td>
								<td class='icoop'>$row[8]</td>
								<td class='icoop' style='text-align:center;'><img $imgsem style='width:20px'/></td>
								<td class='icoop' style='text-align:center;'><img $imganu style='width:20px'/></td>
								<td class='icoop' style='text-align:center;'><img src='imagenes/buscarep.png' onClick=\"location.href='serv-recaudo_regrabar.php?idr=$row[0]'\"></td>
							</tr>";
						$con+=1;
						$aux=$iter;
						$iter=$iter2;
						$iter2=$aux;
					}
					if ($_POST['numtop']==0)
					{
						echo "
						<table class='inicio'>
							<tr>
								<td class='saludo1' style='text-align:center;width:100%'><img src='imagenes\alert.png' style='width:25px'>No hay coincidencias en la b&uacute;squeda $tibusqueda<img src='imagenes\alert.png' style='width:25px'></td>
							</tr>
						</table>";
					}
					echo"
					</table>
					<table class='inicio'>
						<tr>
							<td style='text-align:center;'>
								<a href='#'>$imagensback</a>&nbsp;
								<a href='#'>$imagenback</a>&nbsp;&nbsp;";
					if($nuncilumnas<=9){$numfin=$nuncilumnas;}
					else{$numfin=9;}
					for($xx = 1; $xx <= $numfin; $xx++)
					{
						if($numcontrol<=9){$numx=$xx;}
						else{$numx=$xx+($numcontrol-9);}
						if($numcontrol==$numx){echo"<a href='#' onClick='saltocol(\"$numx\")'; style='color:#24D915'> $numx </a>";}
						else {echo"<a href='#' onClick='saltocol(\"$numx\")'; style='color:#000000'> $numx </a>";}
					}
					echo"			&nbsp;&nbsp;<a href='#'>$imagenforward</a>
									&nbsp;<a href='#'>$imagensforward</a>
								</td>
							</tr>
						</table>";
			?>
			</div>
			<input type="hidden" name="numtop" id="numtop" value="<?php echo $_POST['numtop'];?>" />
		</form>
	</body>
</html>