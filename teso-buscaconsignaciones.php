<?php
header("Cache-control: no-cache, no-store, must-revalidate");
header("Content-Type: text/html;charset=iso-8859-1");
require "comun.inc";
require "funciones.inc";
session_start();
$linkbd = conectar_v7();
$linkbd->set_charset("utf8");
cargarcodigopag($_GET['codpag'], $_SESSION["nivel"]);
date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es">

<head>
    <meta http-equiv="Content-Type" content="text/html" charset="iso-8859-1" />
    <meta http-equiv="X-UA-Compatible" content="IE=9" />
    <title>:: IDEAL 10 - Tesorer&iacute;a</title>
    <link href="favicon.ico" rel="shortcut icon" />
    <link href="css/css2.css" rel="stylesheet" type="text/css" />
    <link href="css/css3.css" rel="stylesheet" type="text/css" />
    <link href="css/style.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="jquery-1.11.0.min.js"></script>
    <script type="text/javascript" src="css/programas.js"></script>
    <script type="text/javascript" src="css/calendario.js"></script>
    <script>
        function verUltimaPos(idcta, dc, filas, filtro) {
            var scrtop = $('#divdet').scrollTop();
            var altura = $('#divdet').height();
            var numpag = $('#nummul').val();
            var limreg = $('#numres').val();
            if ((numpag <= 0) || (numpag == ""))
                numpag = 0;
            if ((limreg == 0) || (limreg == ""))
                limreg = 10;
            numpag++;
            location.href = "teso-editaconsignaciones.php?idr=" + idcta + "&dc=" + dc + "&scrtop=" + scrtop + "&totreg=" + filas + "&altura=" + altura + "&numpag=" + numpag + "&limreg=" + limreg + "&filtro=" + filtro;
        }
    </script>
    <script>
        function eliminar(idr) {
            document.form2.var1.value = idr;
            despliegamodalm('visible', '4', 'Esta Seguro de Eliminar la Consignacion ' + idr, '1');
        }
        function despliegamodalm(_valor, _tip, mensa, pregunta) {
            document.getElementById("bgventanamodalm").style.visibility = _valor;
            if (_valor == "hidden") { document.getElementById('ventanam').src = ""; }
            else {
                switch (_tip) {
                    case "1":
                        document.getElementById('ventanam').src = "ventana-mensaje1.php?titulos=" + mensa; break;
                    case "2":
                        document.getElementById('ventanam').src = "ventana-mensaje3.php?titulos=" + mensa; break;
                    case "3":
                        document.getElementById('ventanam').src = "ventana-mensaje2.php?titulos=" + mensa; break;
                    case "4":
                        document.getElementById('ventanam').src = "ventana-consulta1.php?titulos=" + mensa + "&idresp=" + pregunta; break;
                }
            }
        }
        function respuestaconsulta(pregunta) {
            switch (pregunta) {
                case "1":
                    document.form2.oculto.value = "2";
                    document.form2.submit();
                    break;
            }
        }
        function crearexcel() {
            document.form2.action = "teso-consignacionesexcel.php";
            document.form2.target = "_BLANK";
            document.form2.submit();
            document.form2.action = "";
            document.form2.target = "";
        }
    </script>
    <?php
    $scrtop = $_GET['scrtop'];
    if ($scrtop == "")
        $scrtop = 0;
    echo "<script>
			window.onload=function(){
				$('#divdet').scrollTop(" . $scrtop . ")
			}
		</script>";
    $gidcta = $_GET['idcta'];
    if (isset($_GET['filtro']))
        $_POST['numero'] = $_GET['filtro'];
    ?>
</head>

<body>
    <IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
    <span id="todastablas2"></span>
    <table>
        <tr>
            <script>barra_imagenes("teso");</script><?php cuadro_titulos(); ?>
        </tr>
        <tr><?php menu_desplegable("teso"); ?></tr>
    </table>
    <div class="bg-white group-btn p-1"><button type="button" onclick="window.location.href='teso-consignaciones.php'" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
            <span>Nuevo</span>
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960">
                <path d="M440-440H200v-80h240v-240h80v240h240v80H520v240h-80v-240Z"></path>
            </svg>
        </button>
        <button type="button" onclick="mypop=window.open('plan-agenda.php','','');mypop.focus()"
            class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
            <span class="group-hover:text-white">Agenda</span>
            <svg class="fill-black group-hover:fill-white w-5 h-5" xmlns="http://www.w3.org/2000/svg"
                viewBox="0 -960 960 960">
                <path d="M560-564v-68q33-14 67.5-21t72.5-7q26 0 51 4t49 10v64q-24-9-48.5-13.5T700-600q-38 0-73 9.5T560-564Zm0 220v-68q33-14 67.5-21t72.5-7q26 0 51 4t49 10v64q-24-9-48.5-13.5T700-380q-38 0-73 9t-67 27Zm0-110v-68q33-14 67.5-21t72.5-7q26 0 51 4t49 10v64q-24-9-48.5-13.5T700-490q-38 0-73 9.5T560-454ZM260-320q47 0 91.5 10.5T440-278v-394q-41-24-87-36t-93-12q-36 0-71.5 7T120-692v396q35-12 69.5-18t70.5-6Zm260 42q44-21 88.5-31.5T700-320q36 0 70.5 6t69.5 18v-396q-33-14-68.5-21t-71.5-7q-47 0-93 12t-87 36v394Zm-40 118q-48-38-104-59t-116-21q-42 0-82.5 11T100-198q-21 11-40.5-1T40-234v-482q0-11 5.5-21T62-752q46-24 96-36t102-12q58 0 113.5 15T480-740q51-30 106.5-45T700-800q52 0 102 12t96 36q11 5 16.5 15t5.5 21v482q0 23-19.5 35t-40.5 1q-37-20-77.5-31T700-240q-60 0-116 21t-104 59ZM280-494Z">
                </path>
            </svg>
        </button>
        <button type="button" onclick="mypop=window.open('teso-principal.php','','');mypop.focus();" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
            <span>Nueva ventana</span>
            <svg class="fill-black group-hover:fill-white w-5 h-5" xmlns="http://www.w3.org/2000/svg"
                viewBox="0 -960 960 960">
                <path
                    d="M320-120v-80h80v-80H160q-33 0-56.5-23.5T80-360v-400q0-33 23.5-56.5T160-840h640q33 0 56.5 23.5T880-760v400q0 33-23.5 56.5T800-280H560v80h80v80H320ZM160-360h640v-400H160v400Zm0 0v-400 400Z">
                </path>
            </svg>
        </button>
        <button type="button" onclick="mypop=window.open('/financiero/teso-buscaconsignaciones.php','','');mypop.focus();" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
            <span class="group-hover:text-white">Duplicar pantalla</span>
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960">
                <path d="M640-160v-360H160v360h480Zm80-200v-80h80v-360H320v200h-80v-200q0-33 23.5-56.5T320-880h480q33 0 56.5 23.5T880-800v360q0 33-23.5 56.5T800-360h-80ZM160-80q-33 0-56.5-23.5T80-160v-360q0-33 23.5-56.5T160-600h480q33 0 56.5 23.5T720-520v360q0 33-23.5 56.5T640-80H160Zm400-603ZM400-340Z">
                </path>
            </svg>
        </button><button type="button" onclick="crearexcel()" class="btn btn-success btn-success-hover d-flex justify-between align-items-center">
            <span>Exportar Excel</span>
            <svg class="fill-black group-hover:fill-white w-4 h-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 384 512"><!--!Font Awesome Free 6.5.1 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license/free Copyright 2024 Fonticons, Inc. -->
                <path d="M48 448V64c0-8.8 7.2-16 16-16H224v80c0 17.7 14.3 32 32 32h80V448c0 8.8-7.2 16-16 16H64c-8.8 0-16-7.2-16-16zM64 0C28.7 0 0 28.7 0 64V448c0 35.3 28.7 64 64 64H320c35.3 0 64-28.7 64-64V154.5c0-17-6.7-33.3-18.7-45.3L274.7 18.7C262.7 6.7 246.5 0 229.5 0H64zm90.9 233.3c-8.1-10.5-23.2-12.3-33.7-4.2s-12.3 23.2-4.2 33.7L161.6 320l-44.5 57.3c-8.1 10.5-6.3 25.5 4.2 33.7s25.5 6.3 33.7-4.2L192 359.1l37.1 47.6c8.1 10.5 23.2 12.3 33.7 4.2s12.3-23.2 4.2-33.7L222.4 320l44.5-57.3c8.1-10.5 6.3-25.5-4.2-33.7s-25.5-6.3-33.7 4.2L192 280.9l-37.1-47.6z">
                </path>
            </svg>
        </button>
    </div>
    <div id="bgventanamodalm" class="bgventanamodalm">
        <div id="ventanamodalm" class="ventanamodalm">
            <IFRAME src="" name="ventanam" marginWidth=0 marginHeight=0 frameBorder=0 id="ventanam" frameSpacing=0 style=" width:700px; height:130px; top:200; overflow:hidden;">
            </IFRAME>
        </div>
    </div>
    <?php
    if ($_GET['numpag'] != "") {
        $oculto = $_POST['oculto'];
        if ($oculto != 2) {
            $_POST['numres'] = $_GET['limreg'];
            $_POST['numpos'] = $_GET['limreg'] * ($_GET['numpag'] - 1);
            $_POST['nummul'] = $_GET['numpag'] - 1;
        }
    } else {
        if ($_POST['nummul'] == "") {
            $_POST['numres'] = 10;
            $_POST['numpos'] = 0;
            $_POST['nummul'] = 0;
        }
    }
    ?>
    <form name="form2" method="post" action="teso-buscaconsignaciones.php">
        <input type="hidden" name="numres" id="numres" value="<?php echo $_POST['numres']; ?>" />
        <input type="hidden" name="numpos" id="numpos" value="<?php echo $_POST['numpos']; ?>" />
        <input type="hidden" name="nummul" id="nummul" value="<?php echo $_POST['nummul']; ?>" />
        <input type="hidden" name="cambioestado" id="cambioestado" value="<?php echo $_POST['cambioestado']; ?>" />
        <input type="hidden" name="nocambioestado" id="nocambioestado"
            value="<?php echo $_POST['nocambioestado']; ?>" />
        <?php
        if ($_POST['oculto'] == "") {
            $_POST['cambioestado'] = "";
            $_POST['nocambioestado'] = "";
        }
        ?>
        <table class="inicio" align="center">
            <tr>
                <td class="titulos" colspan="6">:. Buscar Comprobante de Consignaci&oacute;n</td>
                <td class="cerrar" style="width:7%;"><a href="teso-principal.php">&nbsp;Cerrar</a></td>
            </tr>
            <tr>
                <td class="saludo1" style="width:3cm;">No Comprobante:</td>
                <td style="width:15%;"><input type="search" name="numero" id="numero"
                        value="<?php echo $_POST['numero']; ?>" style="width:95%;" /></td>
                <td class="saludo1" style="width:2.5cm;">Fecha Inicial: </td>
                <td style="width:12%;">
                    <input id="fc_1198971545" title="DD/MM/YYYY" name="fechaini" type="text"
                        value="<?php echo $_POST['fechaini'] ?>" maxlength="10" size="10"
                        onKeyPress="javascript:return solonumeros(event)" onKeyUp="return tabular(event,this)"
                        onKeyDown="mascara(this,'/',patron,true)">
                    <a href="#" onClick="displayCalendarFor('fc_1198971545');"><img src="imagenes/calendario04.png"
                            style="width:20px" align="absmiddle" border="0"></a>
                </td>
                <td class="saludo1" style="width:2.5cm;">Fecha Final: </td>
                <td>
                    <input id="fc_1198971546" title="DD/MM/YYYY" name="fechafin" type="text"
                        value="<?php echo $_POST['fechafin'] ?>" maxlength="10" size="10"
                        onKeyPress="javascript:return solonumeros(event)" onKeyUp="return tabular(event,this)"
                        onKeyDown="mascara(this,'/',patron,true)">
                    <a href="#" onClick="displayCalendarFor('fc_1198971546');"><img src="imagenes/calendario04.png"
                            style="width:20px" align="absmiddle" border="0"></a>
                    <input type="button" name="bboton" onClick="limbusquedas();"
                        value="&nbsp;&nbsp;Buscar&nbsp;&nbsp;" />
                </td>
            </tr>
        </table>
        <input type="hidden" name="oculto" id="oculto" value="1" />
        <input type="hidden" name="var1" id="var1" value="<?php echo $_POST['var1']; ?>" />
        <div class="subpantallap" style="height:68.5%; width:99.6%; overflow-x:hidden;" id="divdet">
            <?php

            if ($_POST['oculto'] == 2) {

                $sqlr = "select * from tesoconsignaciones_cab where id_consignacion=$_POST[var1]";
                $resp = mysqli_query($linkbd, $sqlr);
                $row = mysqli_fetch_row($resp);
                // //********Comprobante contable en 000000000000
                $sqlr = "update comprobante_cab set total_debito='0',total_credito='0',estado='0' where tipo_comp='8' and numerotipo='$row[0]'";
                mysqli_query($linkbd, $sqlr);
                $sqlr = "update comprobante_det set valdebito='0',valcredito='0' where id_comp='8 $row[0]'";
                mysqli_query($linkbd, $sqlr);
                $sqlr = "update tesoconsignaciones_cab set estado='N' where id_consignacion=$row[0]";
                mysqli_query($linkbd, $sqlr);
            }
            $crit1 = " ";
            $crit2 = " ";
            preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST['fechaini'], $fecha1);
            $fechaf = $fecha1[3] . "-" . $fecha1[2] . "-" . $fecha1[1];
            preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST['fechafin'], $fecha2);
            $fechaf2 = $fecha2[3] . "-" . $fecha2[2] . "-" . $fecha2[1];
            if ($_POST['numero'] != "") {
                $crit1 = "AND id_consignacion LIKE '%$_POST[numero]%'";
            }
            if ($_POST['fechaini'] != "" and $_POST['fechafin'] != "") {
                $crit2 = "AND fecha BETWEEN '$fechaf' AND '$fechaf2'";
            }
            $sqlr = "SELECT * FROM tesoconsignaciones_cab WHERE estado<>'' $crit1 $crit2 ";
            $resp = mysqli_query($linkbd, $sqlr);
            $_POST['numtop'] = mysqli_num_rows($resp);
            $nuncilumnas = ceil($_POST['numtop'] / $_POST['numres']);
            $cond2 = "";
            if ($_POST['numres'] != "-1") {
                $cond2 = "LIMIT $_POST[numpos], $_POST[numres]";
            }

            $sqlr = "SELECT * FROM tesoconsignaciones_cab WHERE estado<>'' $crit1 $crit2 ORDER BY id_consignacion desc $cond2";
            $resp = mysqli_query($linkbd, $sqlr);
            $ntr = mysqli_num_rows($resp);
            $con = 1;
            $numcontrol = $_POST['nummul'] + 1;
            if ($nuncilumnas == $numcontrol) {
                $imagenforward = "<img src='imagenes/forward02.png' style='width:17px'>";
                $imagensforward = "<img src='imagenes/skip_forward02.png' style='width:16px' >";
            } else {
                $imagenforward = "<img src='imagenes/forward01.png' style='width:17px' title='Siguiente' onClick='numsiguiente()'>";
                $imagensforward = "<img src='imagenes/skip_forward01.png' style='width:16px' title='Fin' onClick='saltocol(\"$nuncilumnas\")'>";
            }
            if ($_POST['numpos'] == 0) {
                $imagenback = "<img src='imagenes/back02.png' style='width:17px'>";
                $imagensback = "<img src='imagenes/skip_back02.png' style='width:16px'>";
            } else {
                $imagenback = "<img src='imagenes/back01.png' style='width:17px' title='Anterior' onClick='numanterior();'>";
                $imagensback = "<img src='imagenes/skip_back01.png' style='width:16px' title='Inicio' onClick='saltocol(\"1\")'>";
            }
            echo "
					<table class='inicio' align='center' >
						<tr>
							<td colspan='6' class='titulos'>.: Resultados Busqueda:</td>
							<td class='submenu'>
								<select name='renumres' id='renumres' onChange='cambionum();' style='width:100%'>
									<option value='10'";
            if ($_POST['renumres'] == '10') {
                echo 'selected';
            }
            echo ">10</option>
									<option value='20'";
            if ($_POST['renumres'] == '20') {
                echo 'selected';
            }
            echo ">20</option>
									<option value='30'";
            if ($_POST['renumres'] == '30') {
                echo 'selected';
            }
            echo ">30</option>
									<option value='50'";
            if ($_POST['renumres'] == '50') {
                echo 'selected';
            }
            echo ">50</option>
									<option value='100'";
            if ($_POST['renumres'] == '100') {
                echo 'selected';
            }
            echo ">100</option>
									<option value='-1'";
            if ($_POST['renumres'] == '-1') {
                echo 'selected';
            }
            echo ">Todos</option>
								</select>
							</td>
						</tr>
						<tr><td colspan='7' class='saludo1'>Comprobante de Consignacion Encontrados: $_POST[numtop]</td></tr>
						<tr>
							<td width='150' class='titulos2'>No Consignacion</td>
							<td class='titulos2'>Fecha</td>
							<td class='titulos2'>Concepto Consignacion</td>
							<td class='titulos2'>Valor</td>
							<td class='titulos2' style='width:5%'>Estado</td>
							<td class='titulos2' style='width:5%'><center>Anular</td>
							<td class='titulos2' style='width:5%'>Editar</td>
						</tr>";
            $iter = 'saludo1a';
            $iter2 = 'saludo2';
            $filas = 1;
            while ($row = mysqli_fetch_row($resp)) {
                $sqlr = "select sum(valor) from tesoconsignaciones where id_consignacioncab=$row[0]";
                $resp2 = mysqli_query($linkbd, $sqlr);
                $row2 = mysqli_fetch_row($resp2);
                if ($row[4] == "S") {
                    $imgsem = "src='imagenes/sema_verdeON.jpg' title='Activo'";
                } else {
                    $imgsem = "src='imagenes/sema_rojoON.jpg' title='Inactivo'";
                }
                if ($gidcta != "") {
                    if ($gidcta == $row[0]) {
                        $estilo = 'background-color:#FF9';
                    } else {
                        $estilo = "";
                    }
                } else {
                    $estilo = "";
                }
                $idcta = "'$row[0]'";
                $dc = "'$row[1]'";
                $numfil = "'$filas'";
                $filtro = "'$_POST[numero]'";

                echo "

						<input type='hidden' name='nConsigE[]' value='" . $row[0] . "'>
						<input type='hidden' name='fechaE[]' value='" . $row[2] . "'>
						<input type='hidden' name='conceptoE[]' value='" . $row[5] . "'>
						<input type='hidden' name='valorE[]' value='" . $row2[0] . "'>
						";

                if ($row[4] == 'S') {
                    echo "
							<input type='hidden' name='estadoE[]' value='ACTIVO'>";
                }
                if ($row[4] == 'N') {
                    echo "
							<input type='hidden' name='estadoE[]' value='INACTIVO'>";
                }
                if ($row[4] == 'R') {
                    echo "
							<input type='hidden' name='estadoE[]' value='INACTIVO'>";
                }

                echo "<tr class='$iter' onMouseOver=\"anterior=this.style.backgroundColor;this.style.backgroundColor='#40b3ff';\"
			onMouseOut=\"this.style.backgroundColor=anterior\" onDblClick=\"verUltimaPos($idcta, $dc, $numfil, $filtro)\" style='text-transform:uppercase; $estilo' >
							<td>$row[0]</td>
							<td>$row[2]</td>
							<td>$row[5]</td>
							<td>$row2[0]</td>
							<td style='text-align:center;'><img $imgsem style='width:18px'></td>";
                if ($row[4] == 'S')
                    echo "<td style='text-align:center;'><a href='#' onClick=eliminar($row[0])><img src='imagenes/anular.png'></a></td>";
                if ($row[4] == 'N')
                    echo "<td></td>";
                echo "<td style='text-align:center;'>
								<a onClick=\"verUltimaPos($idcta, $dc, $numfil, $filtro)\" style='cursor:pointer;'>
									<img src='imagenes/lupa02.png' style='width:20px' title='Ver'>
								</a>
							</td>
						</tr>";
                $con += 1;
                $filas++;
                $aux = $iter;
                $iter = $iter2;
                $iter2 = $aux;
            }
            if ($_POST['numtop'] == 0) {
                if ($_POST['fechaini'] > $_POST['fechafin']) {
                    $menerror = "La fecha inicial no debe ser mayor a la fecha final";
                } else {
                    $menerror = "No hay coincidencias en la b&uacute;squeda";
                }
                echo "
						<table class='inicio'>
							<tr>
								<td class='saludo1' style='text-align:center;width:100%'><img src='imagenes\alert.png' style='width:25px'>$menerror<img src='imagenes\alert.png' style='width:25px'></td>
							</tr>
						</table>";
            }
            echo "
						</table>
						<table class='inicio'>
							<tr>
								<td style='text-align:center;'>
									<a href='#'>$imagensback</a>&nbsp;
									<a href='#'>$imagenback</a>&nbsp;&nbsp;";
            if ($nuncilumnas <= 9) {
                $numfin = $nuncilumnas;
            } else {
                $numfin = 9;
            }
            for ($xx = 1; $xx <= $numfin; $xx++) {
                if ($numcontrol <= 9) {
                    $numx = $xx;
                } else {
                    $numx = $xx + ($numcontrol - 9);
                }
                if ($numcontrol == $numx) {
                    echo "<a href='#' onClick='saltocol(\"$numx\")'; style='color:#24D915'> $numx </a>";
                } else {
                    echo "<a href='#' onClick='saltocol(\"$numx\")'; style='color:#000000'> $numx </a>";
                }
            }
            echo "		&nbsp;&nbsp;<a href='#'>$imagenforward</a>
									&nbsp;<a href='#'>$imagensforward</a>
								</td>
							</tr>
						</table>";
            ?>
        </div>
        <input type="hidden" name="numtop" id="numtop" value="<?php echo $_POST['numtop']; ?>" />
    </form>
</body>

</html>
