<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require 'comun.inc';
	require 'funciones.inc';
	session_start();
	if(empty($_SESSION)){
		header("location: index.php");
	}
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html lang=es>
	<head>
		<meta charset="utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1"/>
		<meta name="viewport" content="user-scalable=no">
		<title>:: IDEAL 10 - Parametrización</title>
		<link href="favicon.ico" rel="shortcut icon"/>
		<link href="css/css2z.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/style.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
		<link rel="stylesheet" href="sweetalert2/dist/sweetalert2.min.css">
		<script src="sweetalert2/dist/sweetalert2.min.js"></script>
		<script type="text/javascript" src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
	</head>
	<body>
		<section id="myapp" v-cloak>
			<div class="loading-container" v-show="isLoading" >
				<p class="text-loading"data-loading="IDEAL 10">IDEAL 10</p>
			</div>
			<div class="main-container">
				<header>
					<table>
						<tr><script>barra_imagenes("para");</script><?php cuadro_titulos();?></tr>
					</table>
				</header>
				<nav>
					<?php menu_desplegable("para");?>
					<div class="bg-white group-btn p-1">
						<button type="button" onclick="location.href='mipg-cargosAdmCrear'" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
							<span>Nuevo</span>
							<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960"><path d="M440-440H200v-80h240v-240h80v240h240v80H520v240h-80v-240Z"></path></svg>
						</button>
						<button type="button" @click="save" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
							<span>Guardar</span>
							<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960"><path d="M840-680v480q0 33-23.5 56.5T760-120H200q-33 0-56.5-23.5T120-200v-560q0-33 23.5-56.5T200-840h480l160 160Zm-80 34L646-760H200v560h560v-446ZM480-240q50 0 85-35t35-85q0-50-35-85t-85-35q-50 0-85 35t-35 85q0 50 35 85t85 35ZM240-560h360v-160H240v160Zm-40-86v446-560 114Z"></path></svg>
						</button>
						<button type="button" onclick="location.href='mipg-cargosAdmBuscar.php'" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
							<span>Buscar</span>
							<svg class="fill-black group-hover:fill-white w-5 h-5" xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960"><path d="M784-120 532-372q-30 24-69 38t-83 14q-109 0-184.5-75.5T120-580q0-109 75.5-184.5T380-840q109 0 184.5 75.5T640-580q0 44-14 83t-38 69l252 252-56 56ZM380-400q75 0 127.5-52.5T560-580q0-75-52.5-127.5T380-760q-75 0-127.5 52.5T200-580q0 75 52.5 127.5T380-400Z"></path></svg>
						</button>
						<button type="button" @click="mypop=window.open('para-principal.php','',''); mypop.focus();" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
							<span>Nueva ventana</span>
							<svg class="fill-black group-hover:fill-white w-5 h-5" xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960"><path d="M320-120v-80h80v-80H160q-33 0-56.5-23.5T80-360v-400q0-33 23.5-56.5T160-840h640q33 0 56.5 23.5T880-760v400q0 33-23.5 56.5T800-280H560v80h80v80H320ZM160-360h640v-400H160v400Zm0 0v-400 400Z"></path></svg>
						</button>
						<button type="button" @click="mypop=window.open('mipg-cargosAdmCrear.php','','');mypop.focus();" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
							<span class="group-hover:text-white">Duplicar pantalla</span>
							<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960"><path d="M640-160v-360H160v360h480Zm80-200v-80h80v-360H320v200h-80v-200q0-33 23.5-56.5T320-880h480q33 0 56.5 23.5T880-800v360q0 33-23.5 56.5T800-360h-80ZM160-80q-33 0-56.5-23.5T80-160v-360q0-33 23.5-56.5T160-600h480q33 0 56.5 23.5T720-520v360q0 33-23.5 56.5T640-80H160Zm400-603ZM400-340Z"></path></svg>
						</button>
						<button type="button" @click="iratras" class="btn btn-success btn-success-hover d-flex justify-between align-items-center">
							<span>Atras</span>
							<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960"><path d="M760-200v-160q0-50-35-85t-85-35H273l144 144-57 56-240-240 240-240 57 56-144 144h367q83 0 141.5 58.5T840-360v160h-80Z"></path></svg>
						</button>
					</div>
				</nav>
				<article>
					<div class="bg-white">
                        <div class="d-flex">
                            <div :class="[arrRequisitos.length > 0 ? 'w-50': 'w-100' ]" class="me-2">
                                <h2 class="titulos m-0">Editar Cargo</h2>
                                <div class="w-100">
                                    <p class="m-2">Todos los campos con (<span class="text-danger fw-bolder">*</span>) son obligatorios</p>
                                    <div class="d-flex w-100">
                                        <div class="form-control w-15">
                                            <label class="form-label" for="">Código<span class="text-danger fw-bolder">*</span>:</label>
                                            <div class="d-flex">
                                                <button type="button" class="btn btn-primary" @click="editItem('prev')"><</button>
                                                <input type="text" class="text-center" v-model="txtConsecutivo" @change="editItem()">
                                                <button type="button" class="btn btn-primary" @click="editItem('next')">></button>
                                            </div>
                                        </div>
                                        <div class="form-control">
                                            <label class="form-label" for="">Nombre Cargo<span class="text-danger fw-bolder">*</span>:</label>
                                            <input type="text" v-model="txtNombreCargo">
                                        </div>
                                    </div>
                                    <div class="d-flex w-100">
                                        <div class="form-control">
                                            <label class="form-label" for="">Jefe directo:</label>
                                            <select id="labelSelectName2" v-model="selectJefe" >
                                            <option value="0" selected>NINGUNO</option>
                                                <option v-for="(data,index) in arrJefe" :key="index" :value="data.codcargo">
                                                {{ data.codcargo}} - {{ data.nombrecargo}}
                                                </option>
                                            </select>
                                        </div>
                                        <div class="form-control">
                                            <label class="form-label" for="">Dependencia<span class="text-danger fw-bolder">*</span>:</label>
                                            <select id="labelSelectName2" v-model="selectDependencia">
                                                <option value="0" selected>Seleccione...</option>
                                                <option v-for="(data,index) in arrDependencia" :key="index" :value="data.codarea">
                                                {{ data.codarea}} - {{ data.nombrearea}}
                                                </option>
                                            </select>
                                        </div>
                                        <div class="form-control">
                                            <label class="form-label" for="">Nivel Salarial<span class="text-danger fw-bolder">*</span>:</label>
                                            <select id="labelSelectName2" v-model="selectNSalarial">
                                                <option value="0" selected>Seleccione...</option>
                                                <option v-for="(data,index) in arrNSalarial" :key="index" :value="data.id_nivel">
                                                {{ data.id_nivel}} - {{ data.nombre}}
                                                </option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="w-50" v-if="arrRequisitos.length > 0">
                                <h2 class="titulos m-0">Requisitos del cargo</h2>
                                <div class="overflow-auto d-flex mt-2" style="height:40vh">
                                    <div v-for="(area,i) in arrRequisitos" :key="i" class="me-4">
                                        <div class="form-control" >
                                            <div class="d-flex align-items-center justify-between" >
                                                <label class="me-2 fw-bold" :for="'labelCheckName' + i">{{area.nombre}}</label>
                                                <label :for="'labelCheckName' + i" class="form-switch">
                                                    <input type="checkbox" :id="'labelCheckName' + i" v-model="area.is_checked">
                                                    <span></span>
                                                </label>
                                            </div>
                                        </div>
                                        <hr>
                                        <div v-if="area.is_checked" class="ms-4" >
                                            <div v-for="(tipos,j) in area.tipos" :key="j">
                                                <div class="form-control mt-3" >
                                                    <div class="d-flex align-items-center justify-between" >
                                                        <label class="me-2 text-black-light fw-bold" :for="'labelCheckName' +i+j">{{tipos.nombre}}</label>
                                                        <label :for="'labelCheckName' +i+j" class="form-switch">
                                                            <input type="checkbox" :id="'labelCheckName'+i+j" v-model="tipos.is_checked">
                                                            <span></span>
                                                        </label>
                                                    </div>
                                                </div>
                                                <hr>
                                                <div v-if="tipos.is_checked" class="ms-4" >
                                                    <div class="form-control" v-for="(requisitos,k) in tipos.requisitos" :key="k">
                                                        <div class="d-flex align-items-center justify-between" >
                                                            <label class="me-2" :for="'labelCheckName' +i+j+k">{{requisitos.nombre}}</label>
                                                            <label :for="'labelCheckName' +i+j+k" class="form-switch">
                                                                <input type="checkbox" :id="'labelCheckName'+i+j+k" v-model="requisitos.is_checked">
                                                                <span></span>
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
					</div>

				</article>
			</div>
		</section>
		<script src="node_modules/read-excel-file/bundle/read-excel-file.min.js"></script>
		<script src="node_modules/xlsx/dist/xlsx.full.min.js"></script>
		<script src="Librerias/vue/vue.min.js"></script>
		<script src="Librerias/vue/axios.min.js"></script>
		<script type="module" src="vue/herramientas_mipg/cargos/editar/mipg-cargosAdmEditar.js?<?= date('d_m_Y_h_i_s');?>"></script>
	</body>
</html>
