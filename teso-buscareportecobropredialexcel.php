<?php
	require_once 'PHPExcel/Classes/PHPExcel.php';
	require "comun.inc";
	require "funciones.inc";
	session_start();
	$objPHPExcel = new PHPExcel();

	//----Propiedades----
	$objPHPExcel->getProperties()
	->setCreator("IDEAL 10")
	->setLastModifiedBy("IDEAL 10")
	->setTitle("Exportar Excel con PHP")
	->setSubject("Documento de prueba")
	->setDescription("Documento generado con PHPExcel")
	->setKeywords("usuarios phpexcel")
	->setCategory("reportes");

	//----Cuerpo de Documento----
	$objPHPExcel->setActiveSheetIndex(0)
	->mergeCells('A1:G1')
	->mergeCells('A2:G2')
	->setCellValue('A1', 'TESORERÍA - FACTURA')
	->setCellValue('A2', 'FACTURAS');
	$objPHPExcel-> getActiveSheet ()
	-> getStyle ("A1")
	-> getFill ()
	-> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
	-> getStartColor ()
	-> setRGB ('C8C8C8');
	$objPHPExcel-> getActiveSheet ()
	-> getStyle ("A1:A2")
	-> getFont ()
	-> setBold ( true ) 
	-> setName ( 'Verdana' ) 
	-> setSize ( 10 ) 
	-> getColor ()
	-> setRGB ('000000');
	$objPHPExcel-> getActiveSheet ()
	-> getStyle ('A1:A2')
	-> getAlignment ()
	-> applyFromArray (array ( 'horizontal'  =>  PHPExcel_Style_Alignment :: HORIZONTAL_CENTER ,) );
	$objPHPExcel-> getActiveSheet ()
	-> getStyle ('A3:G3')
	-> getAlignment ()
	-> applyFromArray (array ( 'horizontal'  =>  PHPExcel_Style_Alignment :: VERTICAL_JUSTIFY ,) ); 
	$objPHPExcel-> getActiveSheet ()
	-> getStyle ("A2")
	-> getFill ()
	-> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
	-> getStartColor ()
	-> setRGB ('A6E5F3');
	$objPHPExcel-> getActiveSheet ()
	-> getStyle ("A3:G3")
	-> getFill ()
	-> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
	-> getStartColor ()
	-> setRGB ('22C6CB');
	$borders = array(
		'borders' => array(
			'allborders' => array(
				'style' => PHPExcel_Style_Border::BORDER_THIN,
			'color' => array('argb' => 'FF000000'),
			)
		),
	);
	$objPHPExcel->getActiveSheet()->getStyle('A1:G1')->applyFromArray($borders);
	$objPHPExcel->getActiveSheet()->getStyle('A2:G2')->applyFromArray($borders);
	$objPHPExcel->getActiveSheet()->getStyle('A3:G3')->applyFromArray($borders);
	$objWorksheet = $objPHPExcel->getActiveSheet();

	$objPHPExcel->setActiveSheetIndex(0)
	->setCellValue('A3', 'Vigencia')
	->setCellValue('B3', 'Codigo Catastral')
	->setCellValue('C3', 'Documento')
	->setCellValue('D3', 'Nombre Propietario')
	->setCellValue('E3', 'Direccion')
	->setCellValue('F3', 'Num. De factura')
	->setCellValue('G3', 'Total');

	$i = 4;

	for($x = 0; $x < count($_POST['codcatastral']); $x++ )
	{
		$objPHPExcel->setActiveSheetIndex(0)
		
		->setCellValueExplicit ("A$i", $_POST['vigencias1'][$x].'-'.$_POST['vigencias2'][$x], PHPExcel_Cell_DataType :: TYPE_STRING)
		->setCellValueExplicit ("B$i", $_POST['codcatastral'][$x], PHPExcel_Cell_DataType :: TYPE_STRING)
		->setCellValueExplicit ("C$i", $_POST['documento'][$x], PHPExcel_Cell_DataType :: TYPE_STRING)
		->setCellValueExplicit ("D$i", $_POST['nombrepropietario'][$x], PHPExcel_Cell_DataType :: TYPE_STRING)
		->setCellValueExplicit ("E$i", $_POST['direccion'][$x], PHPExcel_Cell_DataType :: TYPE_STRING)
		->setCellValueExplicit ("F$i", $_POST['codigo'][$x], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
		->setCellValueExplicit ("G$i", $_POST['total'][$x], PHPExcel_Cell_DataType :: TYPE_NUMERIC);
		
		$objPHPExcel->getActiveSheet()->getStyle("A$i:G$i")->applyFromArray($borders);
		
		$i++;

	}
	
	//----Propiedades de la hoja
	$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true); 
	$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true); 
	$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true); 
	$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true); 
	$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true); 
	$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true); 
	$objPHPExcel->getActiveSheet()->setTitle('Facturas');
	$objPHPExcel->setActiveSheetIndex(0);

//----Guardar documento----
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename="facturacion-Predial.xlsx"');
header('Cache-Control: max-age=0');
$objWriter=PHPExcel_IOFactory::createWriter($objPHPExcel,'Excel2007');
$objWriter->save('php://output');
exit;
?>