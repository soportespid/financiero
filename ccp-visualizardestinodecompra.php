<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require "comun.inc";
	require "funciones.inc";
	require "conversor.php";
	require "validaciones.inc";
	session_start();
	$linkbd=conectar_v7();
	$linkbd -> set_charset("utf8");
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
?>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es">
	<head>
		<meta http-equiv="Content-Type" content="text/html" charset="iso-8859-1"/>
		<meta http-equiv="X-UA-Compatible" content="IE=9"/>
		<title>:: IDEAL 10 - Presupuesto</title>
		<link href="css/css2.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<script type="text/javascript" src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script type="text/javascript" src="css/calendario.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script type="text/javascript" src="JQuery/jquery-2.1.4.min.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script>
			            
			function buscarp(e)
			{
				if (document.form2.rp.value!="")
				{
					document.form2.brp.value = '1';
					document.form2.submit();
				}
			}

			function adelante()
			{
				var actual = parseFloat(document.form2.numero.value);
				var maximo = parseFloat(document.form2.maximo.value);
				var editablead = parseFloat(document.form2.editablead.value);

				if(maximo > actual)
				{
					actual = parseFloat(actual)+1;
					if(editablead == 0)
					{
						location.href="ccp-editardestinodecompra.php?idnumero=" +actual;
					}
					else
					{
						location.href="ccp-visualizardestinodecompra.php?idnumero=" +actual;	
					}	
					
				}

			}

			function atras(){
				var actual = parseFloat(document.form2.numero.value);
				var minimo = parseFloat(document.form2.minimo.value);
				var editableat = parseFloat(document.form2.editableat.value);

				if(actual > minimo)
				{
					actual = parseFloat(actual)-1;
					if(editableat == 0)
					{
						location.href="ccp-editardestinodecompra.php?idnumero=" +actual;
					}
					else
					{
						location.href="ccp-visualizardestinodecompra.php?idnumero=" +actual;	
					}	
				}
			}

		</script>
		<?php titlepag();?>
		<?php 

			if($_POST['oculto']==""){

				$_POST['numero']= $_GET['idnumero'];

				$sqlr="SELECT MIN(id), MAX(id), editable FROM ccpetdc";
				$res=mysqli_query($linkbd,$sqlr);
				$r=mysqli_fetch_row($res);
				$_POST['minimo'] = $r[0];
				$_POST['maximo'] = $r[1];
				$_POST['editable'] = $r[2];

				$sqlediat = "SELECT editable FROM ccpetdc WHERE id = $_POST[numero]-1" ;
				$resediat = mysqli_query($linkbd,$sqlediat);
				$rowediat = mysqli_fetch_row($resediat);
				$_POST['editableat']=$rowediat[0];

				$sqlediad = "SELECT editable FROM ccpetdc WHERE id = $_POST[numero]+1";
				$resediad = mysqli_query($linkbd,$sqlediad);
				$rowediad = mysqli_fetch_row($resediad);
				$_POST['editablead']=$rowediad[0];

				$sqlEdiCab = "SELECT * FROM ccpetdc WHERE id = $_POST[numero]";
				$resEdiCab = mysqli_query($linkbd,$sqlEdiCab);
            	$rowEdiCab = mysqli_fetch_row($resEdiCab); 

				$_POST['rp'] = $rowEdiCab[2];
				$_POST['fecha'] = $rowEdiCab[4];
				$_POST['vigencia'] = $rowEdiCab[1];
				$_POST['contrato'] = $rowEdiCab[8];
				$_POST['idcdp'] = $rowEdiCab[9];
				$_POST['ntercero'] = $rowEdiCab[5];
				$_POST['valorrp'] = $rowEdiCab[6];
				$_POST['saldo'] = $rowEdiCab[7]; 
				$_POST['tipomov'] = $rowEdiCab[10];

				$sqlrcdp = "SELECT solicita, objeto FROM ccpetcdp WHERE id_cdp = $rowEdiCab[9]";
				$rescdp = mysqli_query($linkbd,$sqlrcdp);
            	$rowcdp = mysqli_fetch_row($rescdp);
				$_POST['solicita'] = $rowcdp[0];
				$_POST['objeto'] = $rowcdp[1];
				
				$sqlrter = "SELECT nombre1, nombre2, apellido1, apellido2, razonsocial FROM terceros WHERE cedulanit = '$_POST[ntercero]' ";
				$rester = mysqli_query($linkbd,$sqlrter);
				$rowter = mysqli_fetch_row($rester); 
				if($rowter[4] != "")
				{
					$_POST['nomtercero']= $rowter[4];
				}else{
					$_POST['nomtercero']= $rowter[0]." ".$rowter[1]." ".$rowter[2]." ".$rowter[3];
				}

				switch($rowEdiCab[3])
				{
					case 'S':	$_POST['estado']='ACTIVO';
								$color=" style='background-color:#0CD02A ;color:#fff'";
								break;
					case 'C':	$_POST['estado']='COMPLETO'; 	 				
								$color=" style='background-color:#00CCFF ; color:#fff'";
								break;
					case 'N':	$_POST['estado']='ANULADO'; 
								$color=" style='background-color:#aa0000 ; color:#fff'";
								break;
					case 'R':	$_POST['estado']='REVERSADO'; 
								$color=" style='background-color:#aa0000 ; color:#fff'";
								break;
				}

				$sqlEdiDet = "SELECT * FROM ccpetdc_detalle WHERE consvigencia = $_POST[numero]";
				$resEdiDet = mysqli_query($linkbd, $sqlEdiDet);
				
				$_POST['codVigenciag'] = array();
				$_POST['nomVigenciag'] = array();
				$_POST['codCuenta'] = array();
				$_POST['nomCuenta'] = array();
				$_POST['codFuente'] = array();
				$_POST['nomFuente'] = array();
				$_POST['codProductos'] = array();
				$_POST['nomProductos'] = array();
				$_POST['codIndicador'] = array();
				$_POST['nomIndicador'] = array();
				$_POST['codPoliticap'] = array();
				$_POST['nomPoliticap'] = array();
				$_POST['medioPago'] = array();
				$_POST['valor'] = array();
				$_POST['iddestcompra'] = array();
                $_POST['nomDestcompra'] = array();
				
				$d = 0;

				while($rowEdiDet = mysqli_fetch_row($resEdiDet)){
					
					$sqlcodVigenciag = " SELECT nombre FROM ccpet_vigenciadelgasto WHERE codigo = '$rowEdiDet[10]'";
					$rescodVigenciag = mysqli_query($linkbd,$sqlcodVigenciag);
					$rowcodVigenciag = mysqli_fetch_row($rescodVigenciag);
					$_POST['nomVigenciag'][] = $rowcodVigenciag[0]; 
					$_POST['codVigenciag'][] = $rowEdiDet[10];

					$_POST['codCuenta'][] = $rowEdiDet[3];
					$nomCuentares = buscacuentaccpetgastos($rowEdiDet[3]);
					$_POST['nomCuenta'][] = $nomCuentares;
					
					$_POST['codFuente'][] = $rowEdiDet[5];
					$nomFuenteres = buscafuenteccpet($rowEdiDet[5]);
					$_POST['nomFuente'][] = $nomFuenteres;

					$_POST['codIndicador'][] = $rowEdiDet[8];
					$nomIndicadorres = buscaindicadorccpet($rowEdiDet[8]);
					$_POST['nomIndicador'][] = $nomIndicadorres;

					$_POST['codProductos'][] = $rowEdiDet[4];
					$nomProductosres = buscaservicioccpetgastos($rowEdiDet[4]);
					if($nomProductosres == ''){
						$nomProductosres = buscaproductoccpetgastos($rowEdiDet[4]);
					}
					$_POST['nomProductos'][] = $nomProductosres;

					$sqlcodPoliticap = " SELECT nombre FROM ccpet_politicapublica WHERE codigo = '$rowEdiDet[9]'";
					$rescodPoliticap = mysqli_query($linkbd,$sqlcodPoliticap);
					$rowcodPoliticap = mysqli_fetch_row($rescodPoliticap);
					$_POST['nomPoliticap'][] = $rowcodPoliticap[0];
					$_POST['codPoliticap'][] = $rowEdiDet[9];

					$_POST['medioPago'][] = $rowEdiDet[14];

					$_POST['valor'][] = $rowEdiDet[11];

					$_POST["destcompra$d"] = $rowEdiDet[16];

					$_POST['iddestcompra'][] = $rowEdiDet[0];

                    $sqlDest="SELECT nombre FROM almdestinocompra WHERE codigo = '$rowEdiDet[16]' ";
                    $resDest=mysqli_query($linkbd,$sqlDest);
                    $rowDest = mysqli_fetch_row($resDest);
                    $_POST['nomDestcompra'][] = $rowDest[0];
					
					$co="zebra1";
					$co2="zebra2";
					$d++;
                    
                }
			}
		?>
	</head>

	<body >
		<IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
		<span id="todastablas2"></span>
		<table>
			<tr><script>barra_imagenes("ccpet");</script><?php cuadro_titulos();?></tr>	 
			<tr><?php menu_desplegable("ccpet");?></tr>
			<tr>
				<td colspan="3" class="cinta">
                    <a href="ccp-destinodecompra.php" class="mgbt"><img src="imagenes/add.png" title="Nuevo"/></a>
                    <a class="mgbt"><img src="imagenes/guardad.png"/></a>
                    <a href="ccp-buscardestinodecompra.php" class="mgbt"><img src="imagenes/busca.png" title="Buscar"/></a>
                    <a onClick="mypop=window.open('plan-agenda.php','','');mypop.focus()" class="mgbt"><img src="imagenes/agenda1.png" title="Agenda"/></a>
                    <a onClick="<?php echo paginasnuevas("ccpet");?>" class="mgbt"><img src="imagenes/nv.png" title="Nueva ventana"/></a>
                    <a onClick="mypop=window.open('<?php echo $url2; ?>','','');mypop.focus();" class="tooltip bottom mgbt"><img src="imagenes/duplicar_pantalla.png" title="Duplicar pesta&ntilde;a"></a>
                    <a href="ccp-buscardestinodecompra.php" class="mgbt"><img src="imagenes/iratras.png" title="Atr&aacute;s"/></a>
                </td>
			</tr>
		</table>

		<div id="bgventanamodalm" class="bgventanamodalm">
			<div id="ventanamodalm" class="ventanamodalm">
				<IFRAME src="" name="ventanam" marginWidth=0 marginHeight=0 frameBorder=0 id="ventanam" frameSpacing=0 style=" width:700px; height:130px; top:200; overflow:hidden;"></IFRAME>
			</div>
		</div>

		<form name="form2" method="post" action="ccp-editardestinodecompra.php">
			<table class="inicio" width="99.6%" >
				<tr>
					<td class="titulos" colspan="11">.: Registro destino de compra </td>
					<td class="cerrar" style='width:7%' href='ccp-principal.php'>Cerrar</td>
				</tr>
				<tr>
					<td class="saludo1" style="width:10%">N&uacute;mero:</td>
					<td style="width:9%">
						<img src="imagenes/back.png" title="Anterior" onClick="atras()" class="icobut"/>&nbsp;<input type="text" name="numero" size="5" value="<?php echo $_POST['numero'] ?>" style="text-align:center;" onKeyUp="return tabular(event,this)" readonly >&nbsp;<img src="imagenes/next.png" title="Siguiente" onClick="adelante()" class="icobut"/>
					</td>
					<td class="saludo1">RP:</td> 
                    <td>
                        <input type="text" name="rp" id="rp" onKeyUp="return tabular(event,this)" value="<?php echo $_POST['rp'] ?>" readonly> 
                    </td>
                    <td class="saludo1" style="width:2.5cm;">Fecha:</td>
                    <td style="width:15%;">
                        <input name="fecha" type="text" id="fc_1198971545" title="YYYY/MM/DD" placeholder="YYYY/MM/DD" value="<?php echo $_POST['fecha']; ?>" maxlength="10" style="width:80%;" readonly />&nbsp;
                    </td>
					<td width="130" class="saludo1">Vigencia:</td>
					<td width="50"><input size="4" type="text" id="vigencia" name="vigencia" value="<?php echo $_POST['vigencia'] ?>" readonly></td>
					<td width="30" class="saludo1">Contrato:</td>
					<td width="60" ><input id="contrato" type="text" name="contrato" size="6" onKeyUp="return tabular(event,this)"  onKeyPress="javascript:return solonumeros(event)"  value="<?php echo $_POST['contrato'] ?>" readonly></td>
				</tr>
				<tr>
					<td class="saludo1">Numero CDP:</td>
					<td width="298"><input name="idcdp" type="text" id="idcdp" onKeyUp="return tabular(event,this)" value="<?php echo $_POST['idcdp'] ?>" size="20" readonly></td>
					<td class="saludo1">Tercero:</td>
                    <td>
						<input type="text" name="ntercero" id="ntercero" value="<?php echo $_POST['ntercero'] ?>" style="width:98%" readonly >&nbsp; 
                    </td>
                    <td colspan="4">
                        <input type="text" name="nomtercero" id="nomtercero" value="<?php echo $_POST['nomtercero'] ?>" style="width:100%" readonly >
                    </td>
				</tr>
				<tr>
					<input type="hidden" value="1" name="oculto">
					<td class="saludo1">Solicita:</td>
					<td colspan="3"><input name="solicita" type="text" id="solicita" onKeyUp="return tabular(event,this)" value="<?php echo $_POST['solicita'] ?>" size="75" readonly></td>
					<td class="saludo1">Objeto:</td>
					<td colspan='5'><input name="objeto" type="text" id="objeto" onKeyUp="return tabular(event,this)" value="<?php echo $_POST['objeto'] ?>" size="90" readonly> </td>
				</tr>
				<tr>
					<td class="saludo1">Valor RP:</td>
					<td><input name="valorrp" type="text" value="<?php echo $_POST['valorrp'] ?>" readonly></td>
					<td class="saludo1">Saldo:</td>
					<td><input name="saldo" type="text" value="<?php echo $_POST['saldo'] ?>" size="30" readonly></td>
                    <td width="25" class="saludo1">Estado:</td>
					<td style = 'width:10%' ><input name="estado" type="text" id="estado" value="<?php echo $_POST['estado'] ?>" readonly <?php echo $color; ?>/>&nbsp;</td>
                    <td class="saludo1">Tipo movimiento:</td>
                    <td><input name="tipomov" type="text" value="<?php echo $_POST['tipomov'] ?>" readonly></td>
				</tr>
			</table>

			<input type="hidden" name="maximo" id="maximo" value="<?php echo $_POST['maximo']?>"/>
			<input type="hidden" name="minimo" id="minimo" value="<?php echo $_POST['minimo']?>"/>
			<input type="hidden" name="editable" id="editable" value="<?php echo $_POST['editable']?>"/>
			<input type="hidden" name="editableat" id="editableat" value="<?php echo $_POST['editableat']?>"/>
			<input type="hidden" name="editablead" id="editablead" value="<?php echo $_POST['editablead']?>"/>
			
			<div class="subpantalla" style="height:55.5%; width:99.6%; overflow-x:hidden;">
				<table class="inicio" width="99%">
					<tr><td class="titulos" colspan="10">.: Detalle destino de comprar</td></tr>
					<tr>
                        <td class="titulos2"><center>Vigencia del Gasto</center></td>
						<td class="titulos2" style='width:10%'><center>Cuenta</center></td>
						<td class="titulos2"><center>Nombre Cuenta</center></td>
						<td class="titulos2"><center>Fuente</center></td>
						<td class="titulos2"><center>Producto/Servicio</center></td>
						<td class="titulos2" style='width:10%'><center>Indicador Producto</center></td>
						<td class="titulos2" style='width:10%'><center>Pol&iacute;tica P&uacute;plica</center></td>
                        <td class="titulos2"><center>Medio de Pago</center></td>
						<td class="titulos2" style='width:10%'><center>Valor</center></td>
                        <td class="titulos2" style='width:5%'><center>Destino de compra</center></td>
					</tr>
					

					<?php
						$_POST['cuentagas'] = 0;
						for($x = 0; $x < count($_POST['nomCuenta']); $x++){
							echo"
							<tr class='$co'>
							
								<input type='hidden' name='codVigenciag[]' value='".$_POST['codVigenciag'][$x]."'/>
								<input type='hidden' name='nomVigenciag[]' value='".$_POST['nomVigenciag'][$x]."'/>
								<input type='hidden' name='codCuenta[]' value='".$_POST['codCuenta'][$x]."' />
								<input type='hidden' name='nomCuenta[]' value='".$_POST['nomCuenta'][$x]."'/>
								<input type='hidden' name='codFuente[]' value='".$_POST['codFuente'][$x]."'/>
								<input type='hidden' name='nomFuente[]' value='".$_POST['nomFuente'][$x]."'/>
								<input type='hidden' name='codProductos[]' value='".$_POST['codProductos'][$x]."'/>
								<input type='hidden' name='nomProductos[]' value='".$_POST['nomProductos'][$x]."'/>
								<input type='hidden' name='codIndicador[]' value='".$_POST['codIndicador'][$x]."'/>
								<input type='hidden' name='nomIndicador[]' value='".$_POST['nomIndicador'][$x]."'/>
								<input type='hidden' name='codPoliticap[]' value='".$_POST['codPoliticap'][$x]."'/>
								<input type='hidden' name='nomPoliticap[]' value='".$_POST['nomPoliticap'][$x]."'/>
								<input type='hidden' name='medioPago[]' value='".$_POST['medioPago'][$x]."'/>
								<input type='hidden' name='valor[]' value='".$_POST['valor'][$x]."'>
								<input type='hidden' name='iddestcompra[]' value='".$_POST['iddestcompra'][$x]."'>
                                <input type='hidden' name='nomDestcompra[]' value='".$_POST['nomDestcompra'][$x]."'>

								<td style='width=10%'>".$_POST['codVigenciag'][$x]." - ".$_POST['nomVigenciag'][$x]."</td>
                                <td style='width=10%'>".$_POST['codCuenta'][$x]."</td>
								<td style='width=10%'>".$_POST['nomCuenta'][$x]."</td>
								<td style='width=10%'>".$_POST['codFuente'][$x]." - ".$_POST['nomFuente'][$x]."</td>
								<td style='width=10%'>".$_POST['codProductos'][$x]." - ".$_POST['nomProductos'][$x]."</td>
								<td style='width=10%'>".$_POST['codIndicador'][$x]." - ".$_POST['nomIndicador'][$x]."</td>
                                <td style='width=10%'>".$_POST['codPoliticap'][$x]." - ".$_POST['nomPoliticap'][$x]."</td>
								<td style='width=10%'>".$_POST['medioPago'][$x]."</td>
																
								<td style='text-align:right;'>$ ".number_format($_POST['valor'][$x],2,$_SESSION["spdecimal"],$_SESSION["spmillares"])."</td>
								<td style='width=10%'>".$_POST['nomDestcompra'][$x]."</td>
							</tr>";

							$gas=$_POST['valor'][$x];
							$_POST['cuentagas']=$_POST['cuentagas']+$gas;
							$resultado = convertir($_POST['cuentagas']);
							$_POST['letras']=$resultado." PESOS";
							$aux=$co;
							$co=$co2;
							$co2=$aux;
						}

						echo "						
						
						<input type='hidden' name='cuentagas' id='cuentagas'  value='$_POST[cuentagas]'/>
						<input type='hidden' name='cuentagas2' id='cuentagas2' value='$_POST[cuentagas2]'/>
						<input type='hidden'  name='letras' id='letras' value='$_POST[letras]'/>
						<tr class='$co' style='text-align:right;'>
							<td colspan='8'>Total:</td>
							<td>$ ".number_format($_POST['cuentagas'],2,$_SESSION["spdecimal"],$_SESSION["spmillares"])."</td>
						</tr>
						<tr>
							<td class='saludo1'>Son:</td>
							<td class='saludo1' colspan= '9'>$_POST[letras]</td>
						</tr>";
		            ?>
		        </table>
				<?php
					if($_POST['oculto'] =='2')
					{

						switch($_POST['estado'])
						{
							case 'ACTIVO':	
								$estado = 'S';
							break;

							case 'COMPLETO':
								$estado = 'C';
							break;

							case 'ANULADO':
								$estado = 'N'; 
							break;

							case 'REVERSADO':
								$estado = 'R';
							break;
						}

	 					$sqlCabecera = "UPDATE ccpetdc SET editable = '1' WHERE id = '".$_POST['numero']."' ";
						mysqli_query($linkbd,$sqlCabecera);
						
						for($x=0;$x<count($_POST['nomCuenta']);$x++)
						{
							$sqlDetalle="UPDATE ccpetdc_detalle SET destino_compra = '".$_POST["destcompra$x"]."' WHERE id = '".$_POST['iddestcompra'][$x]."' ";
							
							if(!mysqli_query($linkbd,$sqlDetalle))
							{
								echo"
								<script>
									despliegamodalm('visible','2','No se pudo ejecutar la accion: ');
								</script>";
							}
							else 
							{
								echo "
								<script>
									despliegamodalm('visible','1','Se ha almacenado con exito');
								</script>";
							}
						}
					}
            	?>
            </div>
        </form>
        <div id="bgventanamodal2">
            <div id="ventanamodal2">
                <IFRAME  src="" name="buster" marginWidth=0 marginHeight=0 frameBorder=0 id="ventana2" frameSpacing=0 style="left:500px; width:900px; height:500px; top:200;"></IFRAME>
            </div>
        </div>
    </body>
</html>
