<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require "comun.inc";
	require "funciones.inc";
	sesion();
	$linkbd=conectar_v7();
	$linkbd -> set_charset("utf8");
	cargarcodigopag($_GET[codpag],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es">
	<head>
		<meta http-equiv="Content-type" content="text/html; charset=utf8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=9"/>
		<title>:: Ideal - Presupuesto</title>
		<link href="css/css2.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
		<script type="text/javascript" src="css/calendario.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script type="text/javascript" src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script type='text/javascript' src='JQuery/jquery-2.1.4.min.js'></script>
		<script>
			function guardar()
			{
				document.getElementById("tscrtop").value = $('#divdet').scrollTop();
				document.form2.oculto.value = '2';
				document.form2.submit();
			}
			function despliegamodal2(_valor)
			{
				document.getElementById("bgventanamodal2").style.visibility = _valor;
				if(_valor == "hidden"){document.getElementById('ventana2').src = "";}
				else 
				{
					document.getElementById('ventana2').src = "ccp-cuentasinegresostiponorma-ventana01.php";
				}
			}
			function despliegamodalm(_valor,_tip,mensa,pregunta)
			{
				document.getElementById("bgventanamodalm").style.visibility = _valor;
				if(_valor == "hidden"){document.getElementById('ventanam').src = "";}
				else
				{
					switch(_tip)
					{
						case "1":	document.getElementById('ventanam').src = "ventana-mensaje1.php?titulos=" + mensa; break;
						case "2":	document.getElementById('ventanam').src = "ventana-mensaje3.php?titulos=" + mensa; break;
						case "3":	document.getElementById('ventanam').src = "ventana-mensaje2.php?titulos=" + mensa; break;
						case "4":	document.getElementById('ventanam').src = "ventana-consulta2.php?titulos=" + mensa + "&idresp=" + pregunta; break;
					}
				}
			}
			function funcionmensaje(){}
			function respuestaconsulta(estado,pregunta)
			{
				if(estado == "S")
				{
					switch(pregunta)
					{
						case "1":	document.form2.cambioestado.value = "1"; break;
						case "2":	document.form2.cambioestado.value = "0"; break;
					}
				}
				else
				{
					switch(pregunta)
					{
						case "1":	document.form2.nocambioestado.value = "1"; break;
						case "2":	document.form2.nocambioestado.value = "0"; break;
					}
				}
				document.form2.submit();
			}
			function agregasino(posi)
			{
				document.getElementById("ventanasino").style.visibility = 'visible';
				document.getElementById("posixx").value = posi;
			}
			function respuestasino(valor)
			{
				document.getElementById("ventanasino").style.visibility = 'hidden';
				var vposi = document.getElementById("posixx").value;
				if(document.getElementsByName('celdasino[]').item(vposi).value != valor)
				{
					document.getElementsByName('celdasino[]').item(vposi).value = valor;
					cambioestado(vposi);
				}
			}
			function cambioestado(posi)
			{
				if(document.getElementsByName('estados[]').item(posi).value != 'S')
				{document.getElementsByName('estados[]').item(posi).value = 'S';}
			}
			var ctrlPressed = false;
			var teclaSI = 83;
			var teclaNO = 78;
			$(document).keydown(function(e)
			{
				if (e.keyCode == teclaSI){respuestasino('SI');}
				if (e.keyCode == teclaNO){respuestasino('NO');}
			});
		</script>
		<?php 
			titlepag();
			$scrtop = $_POST['tscrtop'];
			if($scrtop == "") $scrtop = 0;
			echo"<script>
				window.onload=function(){
					$('#divdet').scrollTop(".$scrtop.")
				}
			</script>";
		?>
	</head>
	<body>
		<IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
		<span id="todastablas2"></span>
		<table>
			<tr><script>barra_imagenes("ccpet");</script><?php cuadro_titulos();?></tr>
			<tr><?php menu_desplegable("ccpet");?></tr>
			<tr>
				<td colspan="3" class="cinta">
					<img src="imagenes/add.png" title="Nuevo" onClick="mypop=window.open('ccp-normasagregar.php','','');mypop.focus()" class="mgbt"/><img src="imagenes/guarda.png" onClick="guardar();" class="mgbt"/><img src="imagenes/busca.png" title="Buscar" onClick="mypop=window.open('ccp-normasbuscar.php','','');mypop.focus()" class="mgbt"/><img src="imagenes/nv.png" title="Nueva ventana" onClick="mypop=window.open('ccp-principal.php','','');mypop.focus();" class="mgbt"/><img src="imagenes/iratras.png" title="Atr&aacute;s" onClick="mypop=window.open('ccp-principal.php','',''); mypop.focus();" class="mgbt">
				</td>
			</tr>
			</table>
		<?php
			$maxVersion = ultimaVersionIngresosCCPET();
		?>
		<div id="bgventanamodalm" class="bgventanamodalm">
			<div id="ventanamodalm" class="ventanamodalm">
				<IFRAME src="" name="ventanam" marginWidth=0 marginHeight=0 frameBorder=0 id="ventanam" frameSpacing=0 style=" width:700px; height:130px; top:200; overflow:hidden;"></IFRAME>
			</div>
		</div>
		<form name="form2" method="post" action="">
			<table class="inicio">
				<tr>
					<td class="titulos" colspan="4">:: Cuentas de ingresos</td>
					<td class="cerrar" style="width:7%" onClick="location.href='ccp-principal.php'">Cerrar</td>
				</tr>
				<tr>
					<td class="saludo1" style='width:4cm;'>:: N&uacute;mero :</td>
					<td >
						<input class="input01" type="search" name="nombre" id="nombre" value="<?php echo $_POST['nombre'];?>" style='width:50%;'/>
						<input type="button" name="bboton" onClick="limbusquedas();" value="&nbsp;&nbsp;Buscar&nbsp;&nbsp;" />
					</td>
				</tr> 
			</table>
			<div class="subpantallac5" style="height:69%; width:99.6%; margin-top:0px; overflow-x:hidden" id="divdet">
			<?php
				if ($_POST['nombre'] != ""){$crit1 = "WHERE concat_ws(' ', codigo,tipo) LIKE '%".$_POST['nombre']."%'";}
				$sqlr="SELECT codigo, nombre, tipo FROM cuentasingresosccpet WHERE municipio=1 AND version = '$maxVersion' ORDER BY id ASC";
				$resp = mysqli_query($linkbd,$sqlr);
				echo "
					<table class='inicio'>
						<tr><td colspan='8' class='titulos'>.: Resultados Busqueda:</td></tr>
						<tr><td colspan='8'>Cuentas Encontrados: ".$_POST['numtop']."</td></tr>
						<tr>
							<td class='titulos2' style='width:10%;'>CUENTA</td>
							<td class='titulos2'>NOMBRE</td>
							<td class='titulos2' style='width:5%;'>TIPO</td>
							<td class='titulos2' style='width:10%;'>APLICA DESTINACIÓN ESPECIFICA</td>
							<td class='titulos2' style='width:15%;'>TIPO DE NORMA QUE DEFINE LA DESTINACION</td>
							<td class='titulos2' style='width:15%;'>NUMERO DE LA NORMA</td>
							<td class='titulos2' style='width:10%;'>FECHA DE LA NORMA</td>
						</tr>";
						$iter = 'filas01';
						$iter2 = 'filas02';
						$filas = 1;
						$con = 0;
						$contabula = 1;
						while ($row = mysqli_fetch_row($resp)) 
						{
							if($row[2] == 'C')
							{
								if($_POST['oculto'] == "")
								{
									$sqlcom = "SELECT aplicade,tiponorma,numeronorma,fechanorma FROM ccpecuentastiponormas WHERE cuenta = '$row[0]'";
									$rescom = mysqli_query($linkbd,$sqlcom);
									$rowcom = mysqli_fetch_row($rescom);
									$_POST['celdasino'][$con] = $rowcom[0];
									$_POST["tiponorma$con"] = $rowcom[1];
									$_POST['celdascodigonorma1'][$con] = $rowcom[2];
									$_POST["fecha"][$con] = $rowcom[3];
								}
								$varcolor1 = "style='background-color:yellow;text-align:center;'";
								$varcolor2 = "style='background-color:yellow;'";
								$varagregar1 = "onClick='agregasino($con)'";
								$celdasino = "<input class='input01' type='text' name='celdasino[]' value='".$_POST['celdasino'][$con]."' onClick='agregasino($con)' style='text-align:center;background-color:yellow;border-style: none;' readonly/>";
								$celdacodigo = "<input class='input01' type='text' name='celdascodigonorma1[]' value='".$_POST['celdascodigonorma1'][$con]."' onChange=\"cambioestado('$con')\"/>";
								$sel00 = $sel01 = $sel02 = $sel03 = $sel04 = "";
								switch($_POST["tiponorma$con"])
								{
									case "0":	$sel00 = "SELECTED";break;
									case "1":	$sel01 = "SELECTED";break;
									case "2":	$sel02 = "SELECTED";break;
									case "3":	$sel03 = "SELECTED";break;
									case "4":	$sel04 = "SELECTED";break;
								}
								$celdatipo = "
								<select class='input02' name='tiponorma$con' id='tiponorma$con' style='width:100%;' onChange=\"cambioestado('$con')\">
									<option class='input02' value=''>Seleccione ....</option>
									<option class='input02' value='0' $sel00>0 - NO APLICA</option>
									<option class='input02' value='1' $sel01>1 - DECRETO</option>
									<option class='input02' value='2' $sel02>2 - ACUERDO</option>
									<option class='input02' value='3' $sel03>3 - ORDENANZA</option>
									<option class='input02' value='4' $sel03>4 - RESOLUCION</option>
								</select>";
								$celdafecha = "<input class='input01' type='date'name='fecha[]' value='".$_POST["fecha"][$con]."' maxlength='10' onKeyUp=\"return tabular(event,this)\" title='DD/MM/YYYY' style='width:95%' onChange=\"cambioestado('$con')\"/>";
								echo"
								<input type='hidden' name='cuenta[]' value='$row[0]'/>
								<input type='hidden' name='estados[]' value='".$_POST['estados'][$con]."'/>";
								$varnegrita = '';
								$vartabula = "tabindex='$contabula' onKeyUp='return tabular(event,this)'";
								$contabula++;
							}
							else
							{
								$varnegrita = "style='font-weight:bold;'";
								$varcolor1 = $varcolor2 = $varagregar1 = $celdasino = $vartabula = $celdacodigo = $celdatipo = $celdafecha = "";
							}
							$idcta = "'$row[0]'";
							$numfil = "'$filas'";
							$filtro = "'".$_POST['nombre']."'";
							echo"<tr class='$iter' $varnegrita>
								<td class='icoop'>$row[0]</td>
								<td class='icoop'>$row[1]</td>
								<td class='icoop'>$row[2]</td>
								<td class='icoop' $vartabula $varcolor1 $varagregar1>$celdasino</td>
								<td class='icoop' $varcolor2>$celdatipo</td>
								<td class='icoop' $varcolor2>$celdacodigo</td>
								<td class='icoop' $varcolor2>$celdafecha</td>
							</tr>";
							$aux = $iter;
							$iter = $iter2;
							$iter2 = $aux;
							$filas++;
							if($row[2] == 'C'){$con+=1;}
						}
						echo"
					</table>";
				if(@$_POST['oculto'] == 2)
				{
					for ($x = 0;$x < count($_POST['cuenta']);$x++)
					{
						if($_POST['estados'][$x] == "S")
						{
							$sqlr = "SELECT id FROM ccpecuentastiponormas WHERE cuenta='".$_POST['cuenta'][$x]."'";
							$resp = mysqli_query($linkbd,$sqlr);
							$row =mysqli_fetch_row($resp);
							if($row[0] != '')
							{
								$sqlup = "UPDATE ccpecuentastiponormas SET aplicade = '".$_POST['celdasino'][$x]."', tiponorma = '".$_POST["tiponorma$x"]."', numeronorma = '".$_POST['celdascodigonorma1'][$x]."', fechanorma = '".$_POST["fecha"][$x]."' WHERE id = '$row[0]'";
								mysqli_query($linkbd,$sqlup);
							}
							else
							{
								$consec = selconsecutivo('ccpecuentastiponormas','id');
								$sqlup = "INSERT INTO ccpecuentastiponormas (id,cuenta, aplicade, tiponorma, numeronorma, fechanorma, estado) VALUES ('$consec', '".$_POST['cuenta'][$x]."', '".$_POST['celdasino'][$x]."', '".$_POST["tiponorma$x"]."', '".$_POST['celdascodigonorma1'][$x]."', '".$_POST["fecha"][$x]."', 'S')";
								mysqli_query($linkbd,$sqlup);
							}
						}
						echo "<script>despliegamodalm('visible','3','Se ha almacenado con Exito');</script>";
					}
				}
			?>
			</div>
			<input type="hidden" name="posixx" id="posixx" value="<?php echo $_POST['posixx'];?>"/>
			<input type="hidden" name="ocules" id="ocules" value="<?php echo $_POST['ocules'];?>"/>
			<input type="hidden" name="actdes" id="actdes" value="<?php echo $_POST['actdes'];?>"/>
			<input type="hidden" name="numtop" id="numtop" value="<?php echo $_POST['numtop'];?>" />
			<input type="hidden" name="tscrtop" id="tscrtop" value="<?php echo $_POST['tscrtop'];?>" />
			<input type="hidden" name="oculto" id="oculto" value="1"/>
		</form>
		<div id="bgventanamodal2">
			<div id="ventanamodal2">
				<IFRAME  src="" name="buster" marginWidth=0 marginHeight=0 frameBorder=0 id="ventana2" frameSpacing=0 style="left:500px; width:900px; height:500px; top:200;"></IFRAME>
			</div>
		</div>
		<div class="ventanasino" id="ventanasino">
			<div class="content">
				<div class="content-text"><div class="marcodiv01">APLICA DESTINACIÓN ESPECIFICA</div>
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<em class="botonflechaverde" onClick="respuestasino('SI')"><u>S</u>I</em> &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;<em class="botonflecharoja" onClick="respuestasino('NO')"><u>N</u>O</em>
				</div>
			</div>
		</div>
	</body>
</html>
