<?php
	require 'comun.inc';
	require 'funciones.inc';
	session_start();
	cargarcodigopag($_GET['codpag'],$_SESSION['nivel']);
	header('Cache-control: private'); // Arregla IE 6
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	date_default_timezone_set('America/Bogota');
	titlepag();
?>
<!DOCTYPE >
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es">
	<head>
		<meta http-equiv="Content-type" content="text/html; charset=utf8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=9"/>
		<meta name="viewport" content="user-scalable=no">
		<title>:: IDEAL 10 - Almacen</title>
		<link href="css/css2.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/cssSP.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/tabs.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
        <script type="text/javascript" src="css/calendario.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script src="vue/vue.js"></script>
		<script type="text/javascript" src="css/programas.js"></script>        

		<!-- sweetalert2 -->
		<script src="sweetalert2/dist/sweetalert2.min.js"></script>
		<link rel="stylesheet" href="sweetalert2/dist/sweetalert2.min.css">
	</head>
	<body>
		<header>
			<table>
				<tr><script>barra_imagenes("inve");</script><?php cuadro_titulos();?></tr>
			</table>
		</header>

		<form name="form2" method="post" action="">
			<section id="myapp" v-cloak>
				<nav>
					<table>
						<tr><?php menu_desplegable("inve");?></tr>
						<tr>
							<td colspan="3" class="cinta">
								<a href="#" class="mgbt"><img src="imagenes/add2.png"/></a>
								<a class="mgbt"><img src="imagenes/guardad.png" title="Guardar"/></a>
								<a href="#" class="mgbt"><img src="imagenes/buscad.png" title="Buscar"/></a>
								<a onclick="mypop=window.open('plan-agenda.php','','');mypop.focus()" class="mgbt"><img src="imagenes/agenda1.png" title="Agenda" /></a>
								<a onclick="mypop=window.open('inve-principal.php','','');mypop.focus();" class="mgbt"><img src="imagenes/nv.png" title="Nueva ventana"></a>
								<a onclick="mypop=window.open('<?php echo $url2; ?>','','');mypop.focus();" class="mgbt"><img src="imagenes/duplicar_pantalla.png" title="Duplicar pesta&ntilde;a"></a>
								<img src="imagenes/excel.png" v-on:click="excel" title="Excel" class="mgbt">
							</td>
						</tr>
					</table>
				</nav>

				<article>
					<div>
						<table class="inicio grande">
							<tr>
								<td class="titulos" colspan="10">.: Disponibilidad de inventario</td>
							</tr>

							<tr>
								<td class="textonew01" style="width: 10%;">Grupo: </td>
								<td>
									<select v-model="grupo">
										<option value="">Seleccionar grupo</option>
										<option v-for="grupo in grupos" v-bind:value = "grupo[0]">
											{{ grupo[0] }} - {{ grupo[1] }}
										</option>
									</select>
								</td>

								<td class="textonew01" style="width: 10%;">Bodega: </td>
								<td>
									<select v-model="bodega">
										<option value="">Seleccionar bodega</option>
										<option v-for="bodega in bodegas" v-bind:value = "bodega[0]">
											{{ bodega[1] }}
										</option>
									</select>
								</td>

								<td class="textonew01" style="width: 10%;">Centro Costo: </td>
								<td>
									<select v-model="cc">
										<option value="">Seleccionar centro costos</option>
										<option v-for="centro in centroCostos" v-bind:value = "centro[0]">
											{{ centro[0] }} - {{ centro[1] }}
										</option>
									</select>
								</td>

								<td>
									<button type="button" class="botonflechaverde" v-on:click="buscarDatos">Generar reporte</button>
								</td>
							</tr>
						</table>

						<div id="cargando" v-if="loading" class="loading">
							<span>Cargando...</span>
						</div>

						<div class='subpantalla' style='height:66vh; width:99.2%; margin-top:0px; overflow:hidden'>
							<table class="tablamv">
								<thead>
									<tr>
										<th class="titulosnew00" width="10%">Codigo</th>
										<th class="titulosnew00">Nombre</th>
										<th class="titulosnew00" width="20%">Grupo inventario</th>
										<th class="titulosnew00" width="10%">Disponible</th>
										<th class="titulosnew00" width="10%">Valor Unitario</th>
										<th class="titulosnew00" width="10%">Valor Total</th>
										<th class="titulosnew00" width="10%">Unidad</th>
										<th class="titulosnew00" width="5%">Bodega</th>
										<th class="titulosnew00" width="5%">CC</th>
										<th class="titulosnew00" width="1%"></th>
									</tr>
								</thead>
								

								<tbody>
									<tr v-for="(articulo,index) in listadoArticulos" v-on:dblclick="visualizar(articulo)" v-bind:class="index % 2 ? 'saludo1a' : 'saludo2'" style='text-rendering: optimizeLegibility; '>
										<td style="text-align:center; width: 10%;">{{ articulo[0] }}</td>
										<td>{{ articulo[1] }}</td>
										<td width="20%">{{ articulo[2] }}</td>
										<td style="text-align:center; width: 10%;">{{ articulo[3] }}</td>
										<td style="text-align:center; width: 10%;">{{ formatonumero(articulo[7]) }}</td>
										<td style="text-align:center; width: 10%;">{{ formatonumero(articulo[10]) }}</td>
										<td style="text-align:center; width: 10%;">{{ articulo[4] }}</td>
										<td style="text-align:center; width: 5%;">{{ articulo[5] }}</td>
										<td style="text-align:center; width: 5%;">{{ articulo[6] }}</td>

										<input type='hidden' name='codigoArticulo[]' v-model="articulo[0]">	
										<input type='hidden' name='nombre[]' v-model="articulo[1]">
										<input type='hidden' name='grupoInve[]' v-model="articulo[2]">
										<input type='hidden' name='disponible[]' v-model="articulo[3]">
										<input type='hidden' name='valorUni[]' v-model="articulo[7]">
										<input type='hidden' name='valorTotal[]' v-model="articulo[10]">
										<input type='hidden' name='unidad[]' v-model="articulo[4]">
										<input type='hidden' name='bodega[]' v-model="articulo[5]">
										<input type='hidden' name='cc[]' v-model="articulo[6]">
									</tr>
								</tbody>
							</table>
						</div>	
					</div>
				</article>
			</section>
		</form>
		
		<script src="Librerias/vue/vue.min.js"></script>
		<script src="Librerias/vue/axios.min.js"></script>
		<script src="almacen/informeDisponibilidad/inve-informeDisponibilidad.js"></script>	
	</body>
</html>