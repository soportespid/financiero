-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 14-12-2019 a las 16:53:14
-- Versión del servidor: 10.1.37-MariaDB
-- Versión de PHP: 5.6.40

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `cubarral20193011`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pptoacuerdos`
--

CREATE TABLE `pptoacuerdos` (
  `id_acuerdo` smallint(6) NOT NULL,
  `consecutivo` varchar(20) NOT NULL,
  `numero_acuerdo` varchar(50) NOT NULL,
  `fecha` date NOT NULL,
  `vigencia` varchar(4) NOT NULL,
  `valoradicion` double NOT NULL,
  `valorreduccion` double NOT NULL,
  `valortraslado` double NOT NULL,
  `valorinicial` double NOT NULL,
  `estado` varchar(1) NOT NULL,
  `tipo` varchar(1) NOT NULL,
  `tipo_acto_adm` smallint(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `pptoacuerdos`
--
ALTER TABLE `pptoacuerdos`
  ADD PRIMARY KEY (`id_acuerdo`),
  ADD UNIQUE KEY `numero_acuerdo` (`numero_acuerdo`),
  ADD KEY `id_acuerdo` (`id_acuerdo`,`fecha`,`estado`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `pptoacuerdos`
--
ALTER TABLE `pptoacuerdos`
  MODIFY `id_acuerdo` smallint(6) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
