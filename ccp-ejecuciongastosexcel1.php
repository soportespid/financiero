<?php  
	require_once 'PHPExcel/Classes/PHPExcel.php';
	require "comun.inc";
	require "funciones.inc";
	session_start();
	$linkbd=conectar_v7(); 
	$linkbd_V7=conectar_v7(); 

	$objPHPExcel = new PHPExcel();
	//----Propiedades----
	$objPHPExcel->getProperties()
        ->setCreator("IDEAL")
        ->setLastModifiedBy("IDEAL")
        ->setTitle("Reporte Ejecucion Presupuestal Gastos")
        ->setSubject("Presupuesto")
        ->setDescription("Presupuesto")
        ->setKeywords("Presupuesto")
        ->setCategory("Presupuesto");
	//----Cuerpo de Documento----
	$objPHPExcel->getActiveSheet()->mergeCells('A1:O1');
	$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A1', 'Ejecucion presupuestal gastos');

	$objFont=$objPHPExcel->getActiveSheet()->getStyle('A1')->getFont();
	$objFont->setName('Courier New'); 
	$objFont->setSize(15); 
	$objFont->setBold(true); 
	$objFont->setUnderline(PHPExcel_Style_Font::UNDERLINE_SINGLE);
	$objFont->getColor()->setARGB( PHPExcel_Style_Color::COLOR_BLACK);

	$objAlign=$objPHPExcel->getActiveSheet()->getStyle('A1')->getAlignment(); 
	$objAlign->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); 
	$objAlign->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER); 
	$objPHPExcel-> getActiveSheet ()
		-> getStyle ("A2:O2")	
        -> getFill ()
        -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
        -> getStartColor ()
        -> setRGB ('A6E5F3');
	$objPHPExcel-> getActiveSheet ()
		-> getStyle ("A1")	
        -> getFill ()
        -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
        -> getStartColor ()
        -> setRGB ('A6E5F3');
	$borders = array(
      'borders' => array(
        'allborders' => array(
          'style' => PHPExcel_Style_Border::BORDER_THIN,
          'color' => array('argb' => 'FF000000'),
        )
      ),
	);
	
	$styleArray = array(
		"font"  => array(
		"bold" => false,
		"color" => array("rgb" => "FF0000"),
		"size"  => 12,
		"name" => "Verdana"
		));
	$objPHPExcel->getActiveSheet()->getStyle('A2:O2')->applyFromArray($borders);
	$objPHPExcel->setActiveSheetIndex(0)
	->setCellValue('A2', 'CODIGO')
	->setCellValue('B2', 'NOMBRE')
	->setCellValue('C2', 'PRESUPUESTO INICIAL')
	->setCellValue('D2', 'ADICION')
	->setCellValue('E2', 'REDUCCION')
	->setCellValue('F2', 'CREDITO')
	->setCellValue('G2', 'CONTRACREDITO')
	->setCellValue('H2', 'DEFINITIVO')
	->setCellValue('I2', 'DISPONIBILIDAD')
	->setCellValue('J2', 'COMPROMISOS')
	->setCellValue('K2', 'OBLIGACION')
	->setCellValue('L2', 'COMPROMISOS EN EJECUCION')
	->setCellValue('M2', 'PAGOS')
	->setCellValue('N2', 'CUENTAS POR PAGAR')
	->setCellValue('O2', 'SALDO');
	
	$i=3;
	//echo count($_POST[codigo])."holaaa";
	$crit = '';
	
    for($xx=0; $xx<count($_POST['codigo']); $xx++)
    {
        $objPHPExcel->setActiveSheetIndex(0)
		->setCellValueExplicit ("A$i", $_POST['codigo'][$xx], PHPExcel_Cell_DataType :: TYPE_STRING)
		->setCellValueExplicit ("B$i", $_POST['nombre'][$xx], PHPExcel_Cell_DataType :: TYPE_STRING)
		->setCellValueExplicit ("C$i", $_POST['presupuestoInicial'][$xx], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
		->setCellValueExplicit ("D$i", $_POST['adicion'][$xx], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
		->setCellValueExplicit ("E$i", $_POST['reduccion'][$xx], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
		->setCellValueExplicit ("F$i", $_POST['credito'][$xx], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
		->setCellValueExplicit ("G$i", $_POST['contracredito'][$xx], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
		->setCellValueExplicit ("H$i", $_POST['presupuestoDefinitivo'][$xx], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
		->setCellValueExplicit ("I$i", $_POST['cdp'][$xx], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
		->setCellValueExplicit ("J$i", $_POST['rp'][$xx], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
		->setCellValueExplicit ("K$i", $_POST['cxp'][$xx], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
		->setCellValueExplicit ("L$i", $_POST['compromisoEnEjecucion'][$xx], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
		->setCellValueExplicit ("M$i", $_POST['egreso'][$xx], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
		->setCellValueExplicit ("N$i", $_POST['cuentasPorPagar'][$xx], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
		->setCellValueExplicit ("O$i", $_POST['saldo'][$xx], PHPExcel_Cell_DataType :: TYPE_NUMERIC);
		if($_POST['tipo'][$xx] == 'A'){
			$objPHPExcel->getActiveSheet()->getStyle("A$i:O$i")->getFont()->setBold(true);
		}else{
			$objPHPExcel->getActiveSheet()->getStyle("A$i:O$i");
		}

		$i++;
    }
		
	//----Propiedades de la hoja 1
	$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth('50');
	$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('N')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('O')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->setTitle('EJECUCION');
	$objPHPExcel->setActiveSheetIndex(0);

//----Guardar documento----
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename="REPORTE EJECUCION GASTOS.xls"');
header('Cache-Control: max-age=0');
 
$objWriter=PHPExcel_IOFactory::createWriter($objPHPExcel,'Excel5');
$objWriter->save('php://output');
exit;

?>