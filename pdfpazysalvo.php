<?php
	require_once("tcpdf/tcpdf_include.php");
	require('comun.inc');
	require('funciones.inc');
	session_start();
	date_default_timezone_set("America/Bogota");
	class MYPDF extends TCPDF {
		public function Header() {
			$linkbd=conectar_v7();
			$linkbd -> set_charset("utf8");
			$sqlr = "SELECT * FROM configbasica WHERE estado='S'";
			$res = mysqli_query($linkbd,$sqlr);
			while($row=mysqli_fetch_row($res)){
				$nit=$row[0];
				$rs=$row[1];
			}
			$this->Image('imagenes/escudo.jpg', 12, 12, 25, 25, 'JPG', '', 'T', true, 300, '', false, false, 0, false, false, false);// Logo
			$this->SetFont('helvetica','B',8);
			$this->SetY(10);
            $this->RoundedRect(10, 10, 190, 29, 1,'1111' );
			$this->Cell(30,29,'','R',0,'L');
			$this->SetY(10);
			$this->SetX(40);
			$this->SetFont('helvetica','B',12);
			$this->Cell(127,13,"$rs",0,0,'C');
			$this->SetY(16);
			$this->SetX(40);
			$this->SetFont('helvetica','B',11);
			$this->Cell(127,10,"$nit",0,0,'C');
			//*****************************************************************************************************************************
			$this->SetFont('helvetica','B',14);
			$this->SetY(10);
			$this->Cell(50.1);
			$this->Cell(149,31,'',0,1,'C');
			$this->SetY(8);
			$this->Cell(50.1);
			//************************************
			$this->SetFont('helvetica','B',10);
			$this->SetY(10);
			$this->Cell(157);
			$this->Cell(33,15,'','LB',0,'L');
			$this->SetY(10);
			$this->Cell(157);
			$this->Cell(33,5,'Número: '.$_POST['numpredial'],0,0,'C');
			$this->SetY(15);
			$this->Cell(157);
			$this->Cell(33,5,'Fecha Expedición',0,0,'C');
			$this->SetY(20);
			$this->Cell(157);
			$this->SetFont('helvetica','B',8);
			$this->Cell(33,5,$_POST['fecha'],0,0,'C');
			$ncc=buscacentro($_POST['cc']);
			if($ncc=='')
			$ncc='TODOS';
			if($_POST['recibo'] =='' || $_POST['recibo'] == 0){
				$this->SetY(25);
				$this->SetX(40);
				$this->SetFont('helvetica','B',12);
				$this->Cell(127,8,'PAZ Y SALVO','T',0,'C');
				$this->SetY(33);
				$this->SetX(40);
				$this->SetFont('helvetica','B',10);
				$this->multiCell(127,6,'Cod Catastral: '.$_POST['codcat'],0,'C');
			}else{
				$this->SetY(25);
				$this->SetX(40);
				$this->Cell(127,5,'PAZ Y SALVO','T',0,'C');
				$this->SetY(30);
				$this->SetX(40);
				$this->multiCell(127,5,'Cod Catastral: '.$_POST['codcat'],0,'C');
				$this->SetY(35);
				$this->SetX(40);
				$this->multiCell(127,4,'Recibo de Caja No: '.$_POST['recibo'],0,'C');
			}
		}
		public function Footer() {
			$linkbd = conectar_v7();
			$linkbd -> set_charset("utf8");
			$sqlr="SELECT direccion,telefono,web,email FROM configbasica WHERE estado='S'";
			$resp=mysqli_query($linkbd, $sqlr);
			$user = $_SESSION['nickusu'];
			$fecha = date("Y-m-d H:i:s");
			$ip = $_SERVER['REMOTE_ADDR'];
			$useri = $_POST['user'];
			while($row=mysqli_fetch_row($resp))
			{
				$direcc=strtoupper($row[0]);
				$telefonos=$row[1];
				$dirweb=strtoupper($row[3]);
				$coemail=strtoupper($row[2]);
			}
			if($direcc!=''){$vardirec="Dirección: $direcc, ";}
			else {$vardirec="";}
			if($telefonos!=''){$vartelef="Telefonos: $telefonos";}
			else{$vartelef="";}
			if($dirweb!=''){$varemail="Email: $dirweb, ";}
			else {$varemail="";}
			if($coemail!=''){$varpagiw="Pagina Web: $coemail";}
			else{$varpagiw="";}

			$txt = <<<EOD
			$vardirec $vartelef
			$varemail $varpagiw
			EOD;
			$this->SetFont('helvetica', 'I', 5);
			$this->Cell(190,7,'','T',0,'T');
			$this->ln(1);
			$this->Write(0, $txt , '', 0, 'C', true, 0, false, false, 0);
			//$this->Cell(12, 7, 'Hecho por: '.$useri, 0, false, 'C', 0, '', 0, false, 'T', 'M');
			$this->Cell(15, 7, 'Impreso por: '.$user, 0, false, 'C', 0, '', 0, false, 'T', 'M');
			$this->Cell(57, 7, 'IP: '.$ip, 0, false, 'C', 0, '', 0, false, 'T', 'M');
			$this->Cell(50, 7, 'Fecha: '.$fecha, 0, false, 'C', 0, '', 0, false, 'T', 'M');
			$this->Cell(54, 7, 'IDEAL.10 S.A.S', 0, false, 'C', 0, '', 0, false, 'T', 'M');
			$this->Cell(25, 7, 'Pagina '.$this->getAliasNumPage().'/'.$this->getAliasNbPages(), 0, false, 'C', 0, '', 0, false, 'T', 'M');

		}
	}

	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
	$sqlr = "SELECT * FROM configbasica WHERE estado='S'";
	$res=mysqli_query($linkbd,$sqlr);
	while($row=mysqli_fetch_row($res)){
		$nit=$row[0];
		$rs=$row[1];
	}
	$pdf = new MYPDF('P','mm','Letter', true, 'UTF-8', false);
	$pdf->SetMargins(10, 40, 10);// set margins
    $pdf->SetHeaderMargin(40);// set margins
    $pdf->SetFooterMargin(17);// set margins
	$pdf->SetAutoPageBreak(TRUE, 20);// set auto page breaks
	if (@file_exists(dirname(__FILE__).'/lang/spa.php'))
	{
		require_once(dirname(__FILE__).'/lang/spa.php');
		$pdf->setLanguageArray($l);
	}
	$pdf->AddPage();

	$sqlr = "SELECT id_cargo, id_comprobante FROM pptofirmas TB1, pptotipo_comprobante TB2 WHERE TB1.id_comprobante = TB2.id_tipo AND TB2.tipo = 'P01' AND TB1.vigencia = '".$_POST['vigencia']."'";
	$res = mysqli_query($linkbd,$sqlr);
	while($row = mysqli_fetch_assoc($res)){
		if($row["id_cargo"]=='0'){
			$_POST['ppto'][]=buscatercero($_POST['tercero']);
			$_POST['nomcargo'][]='BENEFICIARIO';
		}else{
			$sqlr1="select cedulanit,(select nombrecargo from planaccargos where codcargo='".$row["id_cargo"]."') from planestructura_terceros where codcargo='".$row["id_cargo"]."' and estado='S'";
			$res1=mysqli_query($linkbd,$sqlr1);
			$row1=mysqli_fetch_row($res1);
			$_POST['ppto'][]=buscar_empleado($row1[0]);
			$_POST['nomcargo'][]=$row1[1];
		}
	}

	$pdf->SetFont('helvetica','B',14);
	$pdf->SetY(43.7);
	$pdf->ln(18);
    $pdf->Cell(20);
	$partesCargo = explode(" ", $_POST['nomcargo'][0]);
	if(detectaGenero($partesCargo[0]) == "Mas"){
		//$pdf->Cell(150,4,"EL ".$_POST['nomcargo'][0]." DEL ".strtoupper($rs).' CERTIFICA','',1,'C');
		$pdf->multiCell(150,4,"EL ".$_POST['nomcargo'][0]." DEL ".strtoupper($rs).' CERTIFICA',0,'C',0,1,'','',true,1,false,true,0,'M',false);
	}else{
		$pdf->multiCell(150,4,"LA ".$_POST['nomcargo'][0]." DEL ".strtoupper($rs).' CERTIFICA',0,'C',0,1,'','',true,1,false,true,0,'M',false);
	}

	$pdf->ln(14);
	$pdf->SetFont('helvetica','',12);
	$otros="";
	if($_POST['tot']>'001')
	$otros=" y OTROS ";

	$texto="Que, el predio identificado con numero de cedula catastral ".$_POST['codcat']." , inscrito en el listado de catastro para este municipio, a nombre de ".$_POST['ntercero']."  con numero de documento ".$_POST['tercero'].$otros." Denominado ".$_POST['direccion']." con una Extension de ".$_POST['ha']." Hectareas, ".$_POST['mt2']." Metros y ".$_POST['areac']." AC. Avaluo de $".$_POST['avaluo']." Se halla a PAZ Y SALVO con el Tesoro Municipal, Por concepto de IMPUESTO PREDIAL hasta el Treinta y Uno (31) de Diciembre de ".substr($_POST['fecha'],6,4).". no se genera cobro por valorizacion para la vigencia actual ni vigencias anteriores
	";

	$pdf->MultiCell(0,4,$texto,0,'J',0,1,'','',true,1,false,true,0,'M',false);
    if($_POST['destino']){
        $pdf->ln(10);
        if($_POST['destino']!='')
        {
            $texto= "Valido para: ".$_POST['destino'];
            $pdf->MultiCell(0,4,$texto,0,'L');
        }
    }

	$pdf->ln(2);

	$sql1="SELECT * FROM tesopredios WHERE cedulacatastral='$_POST[codcat]'";
	$res1=mysqli_query($linkbd,$sql1);
	$nota=true;
	$texto='';
	while($row1=mysqli_fetch_row($res1))
	{
		if ($row1[2]>=002) {
			if ($nota) {
				$texto = $texto."Nota: El predio No. ".$_POST['codcat']." tiene ".($row1[2]+0)." propietarios: ".$row1[6].' identificado con el documento '.$row1[5];
				$nota=false;
			}else{
				$texto = $texto.', '.$row1[6].' identificado con el documento '.$row1[5].".";
			}

		}
	}
	$pdf->SetFont('helvetica','I',11);
	$pdf->MultiCell(0,4,$texto,0,'J',0,1,'','',true,1,false,true,0,'M',false);
	$pdf->SetFont('times','',12);
	$pdf->ln(6);

	$meses=array("","Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
	$mm=substr($_POST['fecha'],3,2)*1;
	$texto="Se Expide, a los ".substr($_POST['fecha'],0,2)." dias del Mes de ".$meses[$mm]." de ".substr($_POST['fecha'],6,4);
	$pdf->MultiCell(0,4,$texto,0,'L');

	for($x=0;$x<count($_POST['ppto']);$x++){
		$pdf->ln(15);
		$v=$pdf->gety();
		$pdf->setFont('helvetica','B',10);
		$pdf->Line(50,$v,160,$v);
		$pdf->Cell(190,6,''.$_POST['ppto'][$x],0,1,'C',false,0,0,false,'T','C');
		$pdf->Cell(190,6,''.$_POST['nomcargo'][$x],0,0,'C',false,0,0,false,'T','C');
		$pdf->SetFont('helvetica','',7);
	}

$pdf->Output();
?>
