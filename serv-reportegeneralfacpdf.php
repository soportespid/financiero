<?php
	require_once("tcpdf2/tcpdf_include.php");
	require('comun.inc');
	require"funciones.inc";
	session_start();
	class MYPDF extends TCPDF 
	{
		public function Header() 
		{
			$linkbd=conectar_bd();
			$sqlr="SELECT nit, razonsocial FROM configbasica WHERE estado='S'";
			$resp=mysql_query($sqlr,$linkbd);
			while($row=mysql_fetch_row($resp)){$nit=$row[0];$rs=utf8_encode(strtoupper($row[1]));}
			$this->Image('imagenes/eng.jpg', 25, 10, 25, 23.9, 'JPG', '', 'T', true, 300, '', false, false, 0, false, false, false);// Logo
			$this->SetFont('helvetica','B',8);
			$this->SetY(10);
			$this->RoundedRect(10, 10, 200, 31, 2.5,''); //Borde del encabezado
			$this->Cell(52,31,'','R',0,'L'); //Linea que separa el encabazado verticalmente
			$this->SetY(32.5);
			$this->Cell(52,5,''.$rs,0,0,'C',false,0,1,false,'T','B'); //Nombre Municipio
			$this->SetFont('helvetica','B',8);
			$this->SetY(36.5);
			$this->Cell(52,5,''.$nit,0,0,'C',false,0,1,false,'T','C'); //Nit
			$this->SetFont('helvetica','B',14);
			$this->SetY(10);
			$this->SetX(62);
			$this->Cell(116,17,'REPORTE GENERAL FACTURACIÓN',1,0,'C'); 
			$this->SetFont('helvetica','I',10);
			$this->SetY(27);
			$this->SetX(62);
			$this->Cell(116,7,"",'T',0,'L',false,0,1); 
			$this->SetY(31.2);
			$this->SetX(62);
			$this->Cell(116,7,"",0,0,'L',false,0,1);
			$this->SetFont('helvetica','B',9);
			$this->SetY(10);
			$this->SetX(178);
			$this->Cell(37.8,30.7,'','L',0,'L');
			$this->SetY(29);
			$this->SetX(178.5);
			$this->Cell(35,5," FECHA: ".date("d-m-Y"),0,0,'L');
			$this->SetY(34);
			$this->SetX(178.5);
			$this->Cell(35,5," HORA: ".date('h:i:s a'),0,0,'L');
			//-----------------------------------------------------
			$this->SetY(44);
			$this->Cell(20,5,'Vigencia',1,0,'C',false,0,0,false,'T','C');
			$this->Cell(40,5,'Mes',1,0,'C',false,0,0,false,'T','C');
			$this->Cell(60,5,'Servicio',1,0,'C',false,0,0,false,'T','C');
			$this->Cell(40,5,'Valor Recaudado',1,0,'C',false,0,0,false,'T','C');
			$this->Cell(40,5,'Deuda Anterior',1,0,'C',false,0,0,false,'T','C');
		}
		public function Footer() 
		{
			$linkbd=conectar_bd();
			$sqlr="SELECT direccion,telefono,web,email FROM configbasica WHERE estado='S'";
			$resp=mysql_query($sqlr,$linkbd);
			while($row=mysql_fetch_row($resp))
			{
				$direcc=utf8_encode(strtoupper($row[0]));
				$telefonos=$row[1];
				$dirweb=utf8_encode(strtoupper($row[3]));
				$coemail=utf8_encode(strtoupper($row[2]));
			}
			if($direcc!=''){$vardirec="Dirección: $direcc, ";}
			else {$vardirec="";}
			if($telefonos!=''){$vartelef="Telefonos: $telefonos";}
			else{$vartelef="";}
			if($dirweb!=''){$varemail="Email: $dirweb, ";}
			else {$varemail="";}
			if($coemail!=''){$varpagiw="Pagina Web: $coemail";}
			else{$varpagiw="";}
			$this->SetY(-16);
			$this->SetFont('helvetica', 'BI', 8);
			$txt = <<<EOD
$vardirec $vartelef
$varemail $varpagiw
EOD;
			$this->Write(0, $txt, '', 0, 'C', true, 0, false, false, 0);
			$this->SetY(-13);
			$this->Cell(0, 10, 'Page '.$this->getAliasNumPage().'/'.$this->getAliasNbPages(), 0, false, 'C', 0, '', 0, false, 'T', 'M');
			$this->Line(10, 260, 210, 260,$styleline);
		}
	}
	$pdf = new MYPDF('P','mm','Letter', true, 'iso-8859-1', false);// create new PDF document
	$pdf->SetDocInfoUnicode (true); 
	// set document information
	$pdf->SetCreator(PDF_CREATOR);
	$pdf->SetAuthor('G&CSAS');
	$pdf->SetTitle('Reporte General Recaudos');
	$pdf->SetSubject('RP General');
	$pdf->SetKeywords('TCPDF, PDF, example, test, guide');
	$pdf->SetMargins(10, 49, 10);// set margins
	$pdf->SetHeaderMargin(49);// set margins
	$pdf->SetFooterMargin(20);// set margins
	$pdf->SetAutoPageBreak(TRUE, 20);// set auto page breaks
	// set some language-dependent strings (optional)
	if (@file_exists(dirname(__FILE__).'/lang/spa.php')) 
	{
		require_once(dirname(__FILE__).'/lang/spa.php');
		$pdf->setLanguageArray($l);
	}
	// ---------------------------------------------------------
	$pdf->AddPage();
	$pdf->SetFont('helvetica','I',9);
	$linkbd=conectar_bd();
	$crips01="";
	$totalfac=0;
	$totalfac1=0;
	$totalfacg=0;
	$totaldeuda=0;
	$vsum=1;
	if($_POST[cdc]!=""){$crips01="AND T2.servicio='$_POST[cdc]'";}
	$sqlr="SELECT T1.mes,T2.servicio, SUM(T2.valorliquidacion),T1.fecha,T1.mesfin,SUM(T2.tarifa),SUM(T2.subsidio) FROM servliquidaciones T1, servliquidaciones_det T2 WHERE  T1.id_liquidacion=T2.id_liquidacion AND T1.vigencia='$_POST[vigencia]' AND (CAST(T1.mes as UNSIGNED) BETWEEN $_POST[mesini] AND $_POST[mesfin]) $crips01 GROUP BY T1.mes,T2.servicio ORDER BY CAST(T1.mes as UNSIGNED)";
	$resp = mysql_query($sqlr,$linkbd);
	while ($row =mysql_fetch_row($resp))
	{
		$totalfac1+=$row[5]-$row[6];
		if ($vsum<=3){$totalfac+=($row[5]-$row[6]);}
		else{$totalfac=0;$vsum=1;$totalfac+=($row[5]-$row[6]);}
		$vsum++;
		if($row[0]!=$row[4]){$mesfac=mesletras($row[0])." a ".mesletras($row[4]);}
		else{$mesfac=mesletras($row[0]);}                                   
		$sqlrsv="SELECT nombre FROM servservicios WHERE codigo='$row[1]'";
		$rowsv =mysql_fetch_row(mysql_query($sqlrsv,$linkbd));
		if ($concolor==0){$pdf->SetFillColor(200,200,200);$concolor=1;}
		else {$pdf->SetFillColor(255,255,255);$concolor=0;}
		$pdf->Cell(20,$altura,"$_POST[vigencia]",1,0,'C',true,0,0,false,'T','C');
		$pdf->Cell(40,$altura," $mesfac",1,0,'L',true,0,0,false,'T','C');
		$pdf->Cell(60,$altura," $rowsv[0]",1,0,'L',true,0,0,false,'T','C');
		$pdf->Cell(40,$altura,"$ ".number_format((float)($row[5]-$row[6]),0,",",".")." ",1,0,'R',true,0,0,false,'T','C');
		$pdf->Cell(40,$altura,"$ ".number_format((float)(($row[2]-($row[5]-$row[6]))),0,",",".")." ",1,1,'R',true,0,0,false,'T','C');
	}
		if ($concolor==0){$pdf->SetFillColor(200,200,200);$concolor=1;}
		else {$pdf->SetFillColor(255,255,255);$concolor=0;}
		$pdf->Cell(120,$altura,"Total Facturado:",1,0,'R',true,0,0,false,'T','C');
		$pdf->Cell(40,$altura,"$ ".number_format((float)$totalfac1,0,",",".")." ",1,1,'R',true,0,0,false,'T','C');
		
	// ---------------------------------------------------------
	$pdf->Output('reportergeneralrec.pdf', 'I');//Close and output PDF document
?>