<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require "comun.inc";
	require "funciones.inc";
	require "conversor.php";
	session_start();
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html lang="es">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1">
		<meta name="viewport" content="user-scalable=no">
		<title>:: IDEAL 10 - Tesorer&iacute;a</title>
		<link href="favicon.ico" rel="shortcut icon"/>
		<link href="css/css2z.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet"/>
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet"/>
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet"/>
		<link href="css/tabs.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet"/>
		<link href="sweetalert2/dist/sweetalert2.min.css" rel="stylesheet">
		<script src="sweetalert2/dist/sweetalert2.min.js"></script>
		<script type="text/javascript" src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script type="text/javascript" src="css/calendario.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script>
			function formatonumero(valor){
				//return new Intl.NumberFormat("en-US", {style: "currency", currency: "USD"}).format(valor);
				return new Intl.NumberFormat("es-CO", {style: "currency", currency: "COP"}).format(valor);
			}
			
			function pdf() {
				document.form2.action = "teso-pdfIngresoBienesServicios.php";
				document.form2.target = "_BLANK";
				document.form2.submit(); 
				document.form2.action = "";
				document.form2.target = "";
			}
		</script>
	</head>
	<body>
		<IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
		<span id="todastablas2"></span>
		<table>
			<tr><script>barra_imagenes("teso");</script><?php cuadro_titulos();?></tr>	 
			<tr><?php menu_desplegable("teso");?></tr>
			<tr>
				<td colspan="3" class="cinta">
					<img src="imagenes/add.png" title="Nuevo" onClick="location.href='teso-ingresoBienesServicios.php'" class="mgbt"/>
					<img src="imagenes/guarda.png" title="Guardar" onClick="guardar()" class="mgbt"/>
					<img src="imagenes/busca.png" title="Buscar" class="mgbt" onClick="location.href='teso-buscarIngresosBienesServicios.php'"/>
					<img src="imagenes/agenda1.png" title="Agenda" onClick="mypop=window.open('plan-agenda.php','','');mypop.focus()" class="mgbt"/>
					<img src="imagenes/nv.png" title="Nueva ventana" onClick="mypop=window.open('teso-principal.php','',''); mypop.focus();" class="mgbt"/>
					<img src="imagenes/duplicar_pantalla.png" title="Duplicar pesta&ntilde;a" onClick="mypop=window.open('<?php echo $url2; ?>','','');mypop.focus();" class="tooltip bottom mgbt"/>
					<img src="imagenes/print.png" title="inprimir" class="mgbt" onClick="pdf()"/>
				</td>
			</tr>		  
		</table>
		<div id="bgventanamodalm" class="bgventanamodalm">
			<div id="ventanamodalm" class="ventanamodalm">
				<IFRAME src="" name="ventanam" marginWidth=0 marginHeight=0 frameBorder=0 id="ventanam" frameSpacing=0 style=" width:700px; height:130px; top:200; overflow:hidden;"> 
				</IFRAME>
			</div>
		</div>
		<form name="form2" method="post" action=""> 
			<?php
				if($_POST['oculto'] == ""){ 
					$check1 = "checked";
					
                    $posicion = "";
                    $posicion = $_GET['pos'];

                    $sqlCab = "SELECT id_recibos, fecha, vigencia, cuentabanco, concepto, id_recaudo, valor, tercero, retencion, iva, neto_pagar FROM teso_ingreso_bienes_servicios WHERE id_recibos = $posicion";
                    $resCab = mysqli_query($linkbd, $sqlCab);
                    $rowCab = mysqli_fetch_row($resCab);

                    $_POST['idcomp'] = $rowCab[0];
                    $fechaComoEntero = strtotime($rowCab[1]);
                    $_POST['fecha'] = date("d/m/Y", $fechaComoEntero);
                    $_POST['vigencia'] = $rowCab[2];
                    $_POST['idrecaudo'] = $rowCab[5];
                    $_POST['concepto'] = $rowCab[4];
                    $_POST['valorecaudo'] = $rowCab[6];
                    $_POST['tercero'] = $rowCab[7];
                    $_POST['ntercero'] = buscatercero($rowCab[7]);
                    $_POST['total_retencion'] = number_format($rowCab[8],2,".",",");
                    $_POST['total_iva'] = number_format($rowCab[9],2,".",",");
                    $_POST['total_ingreso'] = number_format($rowCab[10],2,".",",");
					$resultado = convertir($rowCab[10]);
					$_POST['letras']=$resultado." PESOS M/CTE";

                    if ($rowCab[3] != "") {
                        $_POST['modorec'] = "banco";
                        $_POST['cb'] = $rowCab[3];
                        $sqlBanco = "SELECT ncuentaban, tercero, tipo FROM tesobancosctas WHERE cuenta = '$rowCab[3]'";
                        $rowBanco = mysqli_fetch_row(mysqli_query($linkbd, $sqlBanco));
                        $_POST['nbanco'] = $rowBanco[0];

						$nombreCuenta = buscatercero($rowBanco[1]);
						$_POST['nombreCuenta'] = $nombreCuenta . " " . $rowBanco[2] . " " . $rowBanco[0];
                    }
                    else {
                        $_POST['modorec'] = "caja";
                    }

                    $sqlDet = "SELECT codigo, nombre, tipo, porcentaje, valor, cc, fuente FROM teso_ingreso_bienes_servicios_det WHERE id_recibos = $posicion AND tipo = 'D'";
                    $resDet = mysqli_query($linkbd, $sqlDet);
                    while ($rowDet = mysqli_fetch_row($resDet)) {
                        
                        $_POST['codigos'][] = $rowDet[0];
                        $_POST['nombres'][] = $rowDet[1];
                        $_POST['valores'][] = $rowDet[4];
                        $_POST['ccs'][] = $rowDet[5];
                        $_POST['fuentes'][] = $rowDet[6];
                    }

					$sqlDet = "SELECT codigo, nombre, tipo, porcentaje, valor, cc, fuente FROM teso_ingreso_bienes_servicios_det WHERE id_recibos = $posicion AND tipo LIKE 'R%'";
                    $resDet = mysqli_query($linkbd, $sqlDet);
                    while ($rowDet = mysqli_fetch_row($resDet)) {
                        
                        $_POST['codigos2'][] = $rowDet[0];
                        $_POST['nombres2'][] = $rowDet[1];
                        $_POST['valores2'][] = $rowDet[4];
                        $_POST['ccs2'][] = $rowDet[5];
                        $_POST['fuentes2'][] = $rowDet[6];
                    }

					$sqlDet = "SELECT codigo, nombre, tipo, porcentaje, valor, cc, fuente FROM teso_ingreso_bienes_servicios_det WHERE id_recibos = $posicion AND tipo LIKE 'I'";
                    $resDet = mysqli_query($linkbd, $sqlDet);
                    while ($rowDet = mysqli_fetch_row($resDet)) {
                        
                        $_POST['codigos3'][] = $rowDet[0];
                        $_POST['nombres3'][] = $rowDet[1];
                        $_POST['valores3'][] = $rowDet[4];
                        $_POST['ccs3'][] = $rowDet[5];
                        $_POST['fuentes3'][] = $rowDet[6];
                    }
				}
				switch($_POST['tabgroup1']){
					case 1:	$check1 = 'checked';break;
					case 2:	$check2 = 'checked';break;
					case 3:	$check3 = 'checked';
				}
			?>
			<input type="hidden" name="vcambios" id="vcambios" value="<?php echo $_POST['vcambios']?>"/>
			<input type="hidden" name="encontro" value="<?php echo $_POST['encontro']?>"/>
			<input type="hidden" name="cobrorecibo" value="<?php echo $_POST['cobrorecibo']?>"/>
			<input type="hidden" name="vcobrorecibo" value="<?php echo $_POST['vcobrorecibo']?>"/>
			<input type="hidden" name="tcobrorecibo" value="<?php echo $_POST['tcobrorecibo']?>"/> 
			<input type="hidden" name="codcatastral" value="<?php echo $_POST['codcatastral']?>"/>
			<input type="hidden" name="nombreCuenta" value="<?php echo $_POST['nombreCuenta']?>"/>
			<input type="hidden" name="letras" value="<?php echo $_POST['letras']?>"/>

			<table class="inicio ancho">
				<tr>
					<td class="titulos" colspan="2">Ingresos Bienes y Servicios</td>
					<td class="cerrar" style="width:7%" onClick="location.href='teso-principal.php'">Cerrar</td>
				</tr>
				<tr>
					<td style="width:80%;">
						<table>
							<tr>
								<td style="width:2.5cm" class="saludo1" >No Recibo:</td>
								<td style="width:15%">
									<input type="hidden" name="cuentacaja" value="<?php echo $_POST['cuentacaja']?>"/>
									<input name="idcomp" id="idcomp" style="width:100%;" value="<?php echo $_POST['idcomp']?>" style="text-align:center;" readonly/>
								</td>
								<td style="width:2.5cm" class="saludo1">Fecha:</td>
								<td style="width:15%"><input name="fecha" type="text" value="<?php echo $_POST['fecha']?>" style="text-align:center;" readonly/></td>
								<td style="width:2.5cm" class="saludo1">Vigencia:</td>
								<td style="width:10%">
									<input type="text" id="vigencia" name="vigencia" style="width:30%; text-align:center;" value="<?php echo $_POST['vigencia']?>" readonly>     
								</td> 
							</tr>
							<tr>
								<td class="saludo1"> Recaudo:</td>
								<td>
                                    <input type="text" id="tiporec" name="tiporec" value="Bienes y Servicios" style="width:100%" onKeyUp="return tabular(event,this)" readonly>
                                </td>

								<td class="saludo1">Recaudado en:</td>
								
								<td>
									<select name="modorec" id="modorec" onKeyUp="return tabular(event,this)" style="width:100%;" onChange="validar()" >
										<option value="" >Seleccione...</option>
										<option value="caja" <?php if($_POST['modorec']=='caja') echo "SELECTED"; ?>>Caja</option>
										<option value="banco" <?php if($_POST['modorec']=='banco') echo "SELECTED"; ?>>Banco</option>
									</select>
								</td>
								<td class="saludo1">No Liquid:</td>
								<td style="width:22%;">
									<select name="idrecaudo" id="idrecaudo" onKeyUp="return tabular(event,this)" style="width:100%;" onChange="cambioliquidacion()">
										<option value="" >Seleccione...</option>
										<?php
											$sqlr = "SELECT id_recaudo, concepto FROM tesobienesservicios WHERE id_recaudo = '$_POST[idrecaudo]'";
											$resp = mysqli_query($linkbd,$sqlr);
											while ($row = mysqli_fetch_row($resp)){
												if($row[0]==$_POST['idrecaudo']){
													echo "<option value='$row[0]' SELECTED>$row[0] - $row[1]</option>";
												}else{
													echo "<option value='$row[0]'>$row[0] - $row[1]</option>";
												}
											}
										?>
									</select>
								</td>
							</tr>
							<?php
								if ($_POST['modorec']=='banco'){
									echo "<tr>
										<td class='saludo1'>Cuenta :</td>
										<td>
											<input type='text' name='cb' id='cb' value='".$_POST['cb']."' style='width:80%;' readonly/>&nbsp;
										</td>
										<td colspan='4'>
												<input type='text' id='nbanco' name='nbanco' style='width:100%;' value='".$_POST['nbanco']."'  readonly>
										</td>
										<input type='hidden' name='banco' id='banco' value='".$_POST['banco']."'/>
										<input type='hidden' id='ter' name='ter' value='".$_POST['ter']."'/></td>
									</tr>";
								}
							?> 
							<tr>
								<td class="saludo1">Concepto:</td>
								<td colspan="5"><input name="concepto" style="width:100%" type="text" value="<?php echo $_POST['concepto'] ?>" readonly></td>
							</tr>
							<tr>
								<td class="saludo1">Valor:</td>
								<td><input type="text" id="valorecaudo" name="valorecaudo" value="<?php echo $_POST['valorecaudo']?>" style="width:100%" onKeyUp="return tabular(event,this)" readonly ></td>
								<td  class="saludo1">Documento: </td>
								<td ><input name="tercero" type="text" value="<?php echo $_POST['tercero']?>" style="width:100%" onKeyUp="return tabular(event,this)" readonly></td>
								<td class="saludo1">Contribuyente:</td>
								<td><input type="text" id="ntercero" name="ntercero" value="<?php echo $_POST['ntercero']?>" style="width:100%" onKeyUp="return tabular(event,this) "  readonly></td>
							</tr>
							<tr>
								<td class="saludo1">Retenciones:</td>
								<td><input type="text" id="total_retencion" name="total_retencion" value="<?php echo $_POST['total_retencion']?>" style="width:100%" readonly ></td>
								<td  class="saludo1">IVA: </td>
								<td ><input type="text" id="total_iva" name="total_iva" value="<?php echo $_POST['total_iva']?>" style="width:100%" onKeyUp="return tabular(event,this)" readonly></td>
								<td class="saludo1">Total:</td>
								<td><input type="text" id="total_ingreso" name="total_ingreso" value="<?php echo $_POST['total_ingreso']?>" style="width:100%" onKeyUp="return tabular(event,this) "  readonly></td>
							</tr>
						</table>
						<input type="hidden" id="valorecaudo1" name="valorecaudo1" value="<?php echo $_POST['valorecaudo1']?>">
						<input type="hidden" id="total_retencion1" name="total_retencion1" value="<?php echo $_POST['total_retencion1']?>">
						<input type="hidden" id="total_iva1" name="total_iva1" value="<?php echo $_POST['total_iva1']?>">
						<input type="hidden" id="total_ingreso1" name="total_ingreso1" value="<?php echo $_POST['total_ingreso1']?>">
						<input type="hidden" id="ct" name="ct" value="<?php echo $_POST['ct']?>" >
						<input type="hidden" name="oculto" id="oculto" value="1">
						<input type="hidden" value="<?php echo $_POST['trec']?>"  name="trec">
						<input type="hidden" value="0" name="agregadet">
					</td>
					<td colspan="2" style="width:20%; background:url(imagenes/siglasideal.png); background-repeat:no-repeat; background-position:left; background-size: 70% 100%;" >
					</td>  
				</tr>
			</table>

				<div class="tabs" style="height:38%; width:99.7%;" id="myDiv">
					<div class="tab" >
						<input type="radio" id="tab-1" name="tabgroup1" value="1" <?php echo $check1;?> >
						<label id="clabel" for="tab-1">Orden de pago</label>
						<div class="content" style="overflow:hidden;height:93%;">
							<table class='tablamv3' style="height:99%;">
								<thead>
									<tr>
										<th class="titulos" style="text-align:left;">Detalle Recaudo</th>
									</tr>
									<tr >
										<th class="titulosnew02" style="width: 5%;">Código</th>
										<th class="titulosnew02">Nombre</th>
										<th class="titulosnew02" style="width: 15%;">Valor</th>
										<th class="titulosnew02" style="width: 10%;">CC</th>
										<th class="titulosnew02" style="width: 15%;">Fuente</th>
									</tr>
								</thead>
								<tbody>
									<?php
										$iter='saludo1a';
										$iter2='saludo2';
										
										echo "
											<input type='hidden' name='codigos[]' value='$_POST[codigos][]'>
											<input type='hidden' name='nombres[]' value='$_POST[nombres][]'>
											<input type='hidden' name='valores[]' value='$_POST[valores][]'>
											<input type='hidden' name='ccs[]' value='$_POST[ccs][]'>
											<input type='hidden' name='fuentes[]' value='$_POST[fuentes][]'>
										";
										for ($x=0;$x<count($_POST['codigos']);$x++){	

											$total += $_POST['valores'][$x];
											echo "											
											<tr class='$iter'>
												<td style='width: 5%;text-align:center;''>".$_POST['codigos'][$x]."</td>
												<td>".$_POST['nombres'][$x]."</td>
												<td style='width: 15%;text-align:center;'>".number_format($_POST['valores'][$x],2,".",",")."</td>
												<td style='width: 10%;text-align:center;''>".$_POST['ccs'][$x]."</td>
												<td style='width: 15%;text-align:center;''>".$_POST['fuentes'][$x]."</td>
											</tr>
											<tr>
												<td style='width: 8%;text-align:right;'>Total</td>
												<td style='width: 15%;text-align:center;'>".number_format($total,2,".",",")."</td>
											</tr>
											";
										}
									?>
								</tbody>
							</table>
						</div>
					</div>

					<div class="tab" >
						<input type="radio" id="tab-2" name="tabgroup1" value="2" <?php echo $check2;?> >
						<label id="clabel" for="tab-2">Retenciones</label>
						<div class="content" style="overflow:hidden;height:93%;">
							<table class='tablamv3' style="height:99%;">
								<thead>
									<tr>
										<th class="titulos" style="text-align:left;">Detalle Recaudo</th>
									</tr>
									<tr >
										<th class="titulosnew02" style="width: 5%;">Código</th>
										<th class="titulosnew02">Nombre</th>
										<th class="titulosnew02" style="width: 15%;">Valor</th>
										<th class="titulosnew02" style="width: 10%;">CC</th>
										<th class="titulosnew02" style="width: 15%;">Fuente</th>
									</tr>
								</thead>
								<tbody>
									<?php
										$iter='saludo1a';
										$iter2='saludo2';
										
										echo "
											<input type='hidden' name='codigos2[]' value='$_POST[codigos2][]'>
											<input type='hidden' name='nombres2[]' value='$_POST[nombres2][]'>
											<input type='hidden' name='valores2[]' value='$_POST[valores2][]'>
											<input type='hidden' name='ccs2[]' value='$_POST[ccs2][]'>
											<input type='hidden' name='fuentes2[]' value='$_POST[fuentes2][]'>
										";
										for ($x=0;$x<count($_POST['codigos2']);$x++){	

											$total2 += $_POST['valores2'][$x];
											echo "											
											<tr class='$iter'>
												<td style='width: 5%;text-align:center;''>".$_POST['codigos2'][$x]."</td>
												<td>".$_POST['nombres2'][$x]."</td>
												<td style='width: 15%;text-align:center;'>".number_format($_POST['valores2'][$x],2,".",",")."</td>
												<td style='width: 10%;text-align:center;''>".$_POST['ccs2'][$x]."</td>
												<td style='width: 15%;text-align:center;''>".$_POST['fuentes2'][$x]."</td>
											</tr>";
										}

										echo "
											<tr>
												<td style='width: 8%;text-align:right;'>Total</td>
												<td style='width: 15%;text-align:center;'>".number_format($total2,2,".",",")."</td>
											</tr>";
									?>
								</tbody>
							</table>
						</div>
					</div>

					<div class="tab" >
						<input type="radio" id="tab-3" name="tabgroup1" value="3" <?php echo $check3;?> >
						<label id="clabel" for="tab-3">IVA</label>
						<div class="content" style="overflow:hidden;height:93%;">
							<table class='tablamv3' style="height:99%;">
								<thead>
									<tr>
										<th class="titulos" style="text-align:left;">Detalle Recaudo</th>
									</tr>
									<tr >
										<th class="titulosnew02" style="width: 5%;">Código</th>
										<th class="titulosnew02">Nombre</th>
										<th class="titulosnew02" style="width: 15%;">Valor</th>
										<th class="titulosnew02" style="width: 10%;">CC</th>
										<th class="titulosnew02" style="width: 15%;">Fuente</th>
									</tr>
								</thead>
								<tbody>
									<?php
										$iter='saludo1a';
										$iter2='saludo2';
										
										echo "
											<input type='hidden' name='codigos3[]' value='$_POST[codigos3][]'>
											<input type='hidden' name='nombres3[]' value='$_POST[nombres3][]'>
											<input type='hidden' name='valores3[]' value='$_POST[valores3][]'>
											<input type='hidden' name='ccs3[]' value='$_POST[ccs3][]'>
											<input type='hidden' name='fuentes3[]' value='$_POST[fuentes3][]'>
										";
										for ($x=0;$x<count($_POST['codigos3']);$x++){	

											$total3 += $_POST['valores3'][$x];
											echo "											
											<tr class='$iter'>
												<td style='width: 5%;text-align:center;''>".$_POST['codigos3'][$x]."</td>
												<td>".$_POST['nombres3'][$x]."</td>
												<td style='width: 15%;text-align:center;'>".number_format($_POST['valores3'][$x],2,".",",")."</td>
												<td style='width: 10%;text-align:center;''>".$_POST['ccs3'][$x]."</td>
												<td style='width: 15%;text-align:center;''>".$_POST['fuentes3'][$x]."</td>
											</tr>";
										}

										echo "
											<tr>
												<td style='width: 8%;text-align:right;'>Total</td>
												<td style='width: 15%;text-align:center;'>".number_format($total3,2,".",",")."</td>
											</tr>";
									?>
								</tbody>
							</table>
						</div>
					</div>

				</div>
			

				<div id="bgventanamodal2">
					<div id="ventanamodal2">
						<IFRAME  src="" name="buster" marginWidth=0 marginHeight=0 frameBorder=0 id="ventana2" frameSpacing=0 style="left:500px; width:900px; height:500px; top:200;"> 
						</IFRAME>
					</div>
				</div>
			</div>
		</form>
	</body>
</html>