<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require 'comun.inc';
	require 'funciones.inc';
	require 'validaciones.inc';
	require 'conversor.php';
	session_start();
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE html>
<html lang=es>
	<head>
		<meta charset="utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1"/>
		<meta name="viewport" content="user-scalable=no">
		<title>:: IDEAL 10 - Gesti&oacute;n humana</title>
		<link href="favicon.ico" rel="shortcut icon"/>
		<link href="css/css2z.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<script src="vue/vue.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<style>
			input[type=number]::-webkit-inner-spin-button,
			input[type=number]::-webkit-outer-spin-button { 
				-webkit-appearance: none; 
				margin: 0; 
			}
			input[type=number]{
				text-align:right;
				box-sizing: border-box;
				font: 120% sans-serif;
				width: 100% !important;
			}
		</style>
		<script src="sweetalert2/dist/sweetalert2.min.js"></script>
		<link rel="stylesheet" href="sweetalert2/dist/sweetalert2.min.css">
	</head>
	<body>
		<header>
			<table>
				<tr><script>barra_imagenes("hum");</script><?php cuadro_titulos();?></tr>
			</table>
		</header>
		<section id="myapp" v-cloak >
			<nav>
				<table>
					<tr><?php menu_desplegable("hum");?></tr>
					<tr>
						<td colspan="3" class="cinta">
							<img src="imagenes/add2.png"  class="mgbt1" title="Nuevo">
							<img src="imagenes/guarda.png" title="Guardar" v-on:click="guardar()" class="mgbt">
							<img src="imagenes/busca.png" onClick="location.href='hum-liquidarnominabuscar.php'" class="mgbt" title="Buscar">
							<img src="imagenes/nv.png" onClick="mypop=window.open('hum-principal.php','',''); mypop.focus();" class="mgbt" title="Nueva Ventana">
						</td>
					</tr>
				</table>
			</nav>
			<article>
				<table class="inicio ancho">
				<tr>
					<td class="titulos" colspan="8" >Corregir Redondeos Seguridad Social y Parafiscales</td>
					<td class="cerrar" style="width:7%" onClick="location.href='hum-principal.php'">Cerrar</td>
				</tr>
				</table>
				<div class='subpantalla' style='height:68vh; width:99.5%; margin-top:0px; overflow:hidden'>
					<table class='tablamv'>
						<thead>
							<tr style="text-align:Center;">
								<th class="titulosnew00" style="width:7%;">CLASE</th>
								<th class="titulosnew00" style="width:7%;">TIPO</th>
								<th class="titulosnew00" style="width:7%;">DOC.</th>
								<th class="titulosnew00">FUNCIONARIO</th>
								<th class="titulosnew00" style="width:7%;">SALUD</th>
								<th class="titulosnew00" style="width:7%;">PENSION</th>
								<th class="titulosnew00" style="width:7%;">FONDO</br> SOLIDARIDAD</th>
								<th class="titulosnew00" style="width:7%;">ARL</th>
								<th class="titulosnew00" style="width:7%;">CCF</td>
								<th class="titulosnew00" style="width:7%;">SENA</th>
								<th class="titulosnew00" style="width:7%;">ICBF</th>
								<th class="titulosnew00" style="width:7%;">INS. TEC.</th>
								<th class="titulosnew00" style="width:7%;">ESAP</th>
							</tr>
						</thead>
						<tbody style='max-height: 60vh;'>
							<tr v-for="(vcnomina, index) in infonomina" v-bind:class="index % 2 ? 'saludo1a' : 'saludo2'" style='text-rendering: optimizeLegibility; cursor: pointer !important;'>
								<td style="width:7%; font: 120% sans-serif; padding-left:5px;">{{ vcnomina[0] }}</td>
								<td style="width:7%; font: 120% sans-serif; padding-left:5px;">{{ vcnomina[1] }}</td>
								<td style="width:7%; font: 120% sans-serif; padding-left:5px;">{{ vcnomina[2] }}</td>
								<td style="font: 120% sans-serif; padding-left:5px">{{ vcnomina[3] }}</td>
								<td style="width:7%;">
									<input type="number" id="valsaludtotal" v-model="valsaludtotal[index]" v-on:change="validaringreso('6',index)" class='inpnovisibles2' step="100">
								</td>
								<td style="width:7%;">
									<input type="number" id="valpensiontotal" v-model="valpensiontotal[index]" v-on:change="validaringreso('9',index)" class='inpnovisibles2' step="100">
								</td>
								<td style="width:7%;">
									<input type="number" id="valfondosoli" v-model="valfondosoli[index]" v-on:change="validaringreso('10',index)" class='inpnovisibles2' step="100">
								</td>
								<td style="width:7%;">
									<input type="number" id="valarl" v-model="valarl[index]" v-on:change="validaringreso('11',index)" class='inpnovisibles2' step="100">
								</td>
								<td style="width:7%;">
									<input type="number" id="valccf" v-model="valccf[index]" v-on:change="validaringreso('12',index)" class='inpnovisibles2' step="100">
								</td>
								<td style="width:7%;">
									<input type="number" id="valsena" v-model="valsena[index]" v-on:change="validaringreso('13',index)" class='inpnovisibles2' step="100">
								</td>
								<td style="width:7%;">
									<input type="number" id="valicbf" v-model="valicbf[index]" v-on:change="validaringreso('14',index)" class='inpnovisibles2' step="100">
								</td>
								<td style="width:7%;">
									<input type="number" id="valinte" v-model="valinte[index]" v-on:change="validaringreso('15',index)" class='inpnovisibles2' step="100">
								</td>
								<td style="width:7%;">
									<input type="number" id="valesap" v-model="valesap[index]" v-on:change="validaringreso('16',index)" class='inpnovisibles2' step="100">
								</td>
							</tr>
						</tbody>
					</table>
				</div>
				<div id="cargando" v-if="loading" class="loading">
					<span>Cargando...</span>
				</div>
			</article>

			<input type="hidden" v-model="numnomina" :value="numnomina=<?php echo $_GET['idnomi']; ?>"/>
		</section>
		<script src="Librerias/vue/vue.min.js"></script>
		<script src="Librerias/vue/axios.min.js"></script>
		<script src="vue/gestion_humana/hum-ajusteredondeos.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
	</body>
</html>