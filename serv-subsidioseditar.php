<?php
	require "comun.inc";
	require "funciones.inc";
	require "funcionesSP.inc.php";
	session_start();
	$linkbd = conectar_v7();
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
	$scroll=$_GET['scrtop'];
	$totreg=$_GET['totreg'];
	$idcta=$_GET['idcta'];
	$altura=$_GET['altura'];
	$filtro="'".$_GET['filtro']."'";
?>
<!DOCTYPE >
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es">
	<head>
		<meta http-equiv="Content-type" content="text/html; charset=iso-8859-1"/>
		<meta http-equiv="X-UA-Compatible" content="IE=9"/>
		<title>:: IDEAL 10 - Servicios P&uacute;blicos</title>
		<link href="css/css2.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/cssSP.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<script type="text/javascript" src="css/calendario.js"></script>
		<script type="text/javascript" src="css/programas.js"></script>
		<style>
		</style>

		<script>
			function despliegamodalm(_valor,_tip,mensa,pregunta)
			{
				document.getElementById("bgventanamodalm").style.visibility=_valor;
				if(_valor=="hidden"){document.getElementById('ventanam').src="";}
				else
				{
					switch(_tip)
					{
						case "1":	document.getElementById('ventanam').src="ventana-mensaje1.php?titulos="+mensa;break;
						case "2":	document.getElementById('ventanam').src="ventana-mensaje3.php?titulos="+mensa;break;
						case "3":	document.getElementById('ventanam').src="ventana-mensaje2.php?titulos="+mensa;break;
						case "4":	document.getElementById('ventanam').src="ventana-consulta1.php?titulos="+mensa+"&idresp="+pregunta;break;	
					}
				}
			}

			function despliegamodal2(_valor, modal)
			{
				document.getElementById("bgventanamodal2").style.visibility=_valor;
				if(_valor=="hidden")
				{
					document.getElementById('ventana2').src="";
				}
				else 
				{
					if(modal == 'presupuesto')
					{
						document.getElementById('ventana2').src="cuentasPresupuestalesIngresos-ventana.php?ti=1";
					}
					if(modal == 'estrato')
					{
						document.getElementById('ventana2').src="serv-ventanaEstratos.php";
					}
				}
			}

			function funcionmensaje(){}

			function respuestaconsulta(pregunta)
			{
				switch(pregunta)
				{
					case "1":	document.form2.oculto.value='2';
								document.form2.submit();
								break;
				}
			}

			function guardar()
			{
				var validacion01=document.getElementById('codban').value;
				var validacion02=document.getElementById('nomban').value;
				if (validacion01.trim()!='' && validacion02.trim()) {despliegamodalm('visible','4','Esta Seguro de Modificar','1');}
				else {despliegamodalm('visible','2','Falta informaci�n para modificar la Frecuencia de Cobro');}
			}

			function actualizar()
			{
				document.form2.submit();
			}

			function cambiocheck()
			{
				if(document.getElementById('myonoffswitch').value=='S'){document.getElementById('myonoffswitch').value='N';}
				else{document.getElementById('myonoffswitch').value='S';}
				document.form2.submit();
			}

			function iratras(scrtop, numpag, limreg, filtro)
			{
				var idcta=document.getElementById('codban').value;
				location.href="serv-subsidiosbuscar.php?idcta="+idcta+"&scrtop="+scrtop+"&numpag="+numpag+"&limreg="+limreg+ "&filtro="+filtro;
			}

			function adelante(scrtop, numpag, limreg, filtro)
			{
				var maximo=document.getElementById('maximo').value;
				var actual=document.getElementById('codban').value;
				actual=parseFloat(actual)+1;
				if(actual<=parseFloat(maximo))
				{
					if(actual<10){actual="0"+actual;}
					location.href="serv-subsidioseditar.php?idban=" +actual+ "&scrtop=" +scrtop+ "&numpag=" +numpag+ "&limreg=" +limreg+ "&filtro=" +filtro;
				}
			}

			function atrasc(scrtop, numpag, limreg, filtro, prev)
			{
				var minimo=document.getElementById('minimo').value;
				var actual=document.getElementById('codban').value;
				actual=parseFloat(actual)-1;
				if(actual>=parseFloat(minimo))
				{
					if(actual<10){actual="0"+actual;}
					location.href="serv-subsidioseditar.php?idban=" +actual+ "&scrtop=" +scrtop+ "&numpag=" +numpag+ "&limreg=" +limreg+ "&filtro=" +filtro;
				}
			}
		</script>

		<?php titlepag();?>

	</head>

	<body>
		<IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
		<span id="todastablas2"></span>
		<?php
			$numpag = @$_GET['numpag'];
			$limreg = @$_GET['limreg'];
			$scrtop = 26*$totreg;
		?>
		<table>
			<tr><script>barra_imagenes("serv");</script><?php cuadro_titulos();?></tr>	 

			<tr><?php menu_desplegable("serv");?></tr>

			<tr>
				<td colspan="3" class="cinta">
					<a href="serv-subsidios.php" class="mgbt"><img src="imagenes/add.png"/></a>

					<a onclick="guardar()" class="mgbt"><img src="imagenes/guarda.png" title="Guardar"/></a>

					<a href="serv-subsidiosbuscar.php" class="mgbt"><img src="imagenes/busca.png" title="Buscar"/></a>

					<a onclick="mypop=window.open('plan-agenda.php','','');mypop.focus()" class="mgbt"><img src="imagenes/agenda1.png" title="Agenda" /></a>

					<a onclick="mypop=window.open('serv-principal.php','','');mypop.focus();" class="mgbt"><img src="imagenes/nv.png" title="Nueva ventana"></a>

					<a onclick="mypop=window.open('<?php echo $url2; ?>','','');mypop.focus();" class="mgbt"><img src="imagenes/duplicar_pantalla.png" title="Duplicar pesta&ntilde;a"></a>

					<a onClick="iratras(<?php echo "$scrtop, $numpag, $limreg, $filtro"; ?>)" class="mgbt"><img src="imagenes/iratras.png" title="Atr&aacute;s"></a>
				</td>
			</tr>
		</table>

		<div id="bgventanamodalm" class="bgventanamodalm">
			<div id="ventanamodalm" class="ventanamodalm">
				<IFRAME src="" name="ventanam" marginWidth=0 marginHeight=0 frameBorder=0 id="ventanam" frameSpacing=0 style=" width:700px; height:130px; top:200; overflow:hidden;"></IFRAME>
			</div>
		</div>

		<form name="form2" method="post" action="">
			<?php
				if(@$_POST['oculto'] == "")
				{
					$sqlr = "SELECT MIN(id), MAX(id) FROM srvsubsidios";
					$res = mysqli_query($linkbd,$sqlr);
					$r = mysqli_fetch_row($res);

					$_POST['minimo'] = $r[0];
					$_POST['maximo'] = $r[1];

					$sqlr="SELECT id, nombre, id_estrato, porcentaje, concepto, estado, id_servicio, cuenta_presupuestal, clasificacion, clasificador, vigencia FROM srvsubsidios WHERE id = '".$_GET['idban']."'";
					$resp = mysqli_query($linkbd,$sqlr);
					$row = mysqli_fetch_row($resp); 

					$_POST['codban'] 	    = $row[0];
					$_POST['nomban']        = $row[1];
					$_POST['id_estrato']    = $row[2];
					$_POST['nombreEstrato'] = buscaEstratoConClase($row[2]);
					$_POST['porcentaje']    = $row[3];
					$_POST['concecont']     = $row[4];
					$_POST['onoffswitch']   = $row[5];
					$_POST['servicio']	    = $row[6];
					$_POST['cuenta'] 	    = $row[7];
					$_POST['vigencia'] 		= $row[10];

					switch($row[8])
					{
						case '1':
							$_POST['cuin'] = $row[9];
							$_POST['ncuin'] = buscaNombreCLasificador($row[9],1);
						break;

						case '2':
							$_POST['clasificador'] = $row[9];
							$_POST['nclasificador'] = buscaNombreCLasificador($row[9],2);
						break;

						case '3':
							$_POST['clasificadorServicios'] = $row[9];
							$_POST['nclasificadorServicios'] = buscaNombreCLasificador($row[9],3);
						break;
					}
				}

				if(@$_POST['bc'] == '')
				{
					$nresul=buscaNombreCuentaCCPET(@$_POST['cuenta'], '1');			

					if($nresul != '')
					{
						$_POST['ncuenta'] = $nresul;
					}
					else
					{
						$_POST['ncuenta'] = '';	
					}
				}
			?>

			<table class="inicio ancho">
				<tr>
					<td class="titulos" colspan="6">.: Editar Subsidio</td>

					<td class="cerrar" style="width:7%" onClick="location.href='serv-principal.php'">Cerrar</td>
				</tr>

				<tr>
					<td class="tamano01" style="width:3cm;">C&oacute;digo:</td>

					<td style="width:15%; text-align:center;">
						<a onClick="atrasc(<?php echo "$scrtop, $numpag, $limreg, $filtro"; ?>)" class="icobut" title="Anterior"><img src="imagenes/back.png"/></a>

						<input type="text" name="codban" id="codban" onKeyUp="return tabular(event,this)" value="<?php echo @ $_POST['codban'];?>" style="width:60%;height:30px;text-align:center;" readonly/>&nbsp;
						
						<a onClick="adelante(<?php echo "$scrtop, $numpag, $limreg, $filtro" ?>);" class="icobut" title="Sigiente"><img src="imagenes/next.png"/></a>
					</td>

					<td class="tamano01" style="width:3cm;">Nombre:</td>

					<td colspan="3">
						<input type="text" name="nomban" id="nomban" value="<?php echo @ $_POST['nomban'];?>" style="width:100%;height:30px;text-transform:uppercase"/>
					</td>
				</tr>
				
				<tr>
					<td class="tamano01">Cuenta Presupuestal:</td> 

					<td>
						<input type="text" id="cuenta" name="cuenta" style="width:88%" onKeyPress="javascript:return solonumeros(event)" onKeyUp="return tabular(event,this)" onBlur="actualizar()" value="<?php echo @$_POST['cuenta']?>" onClick="document.getElementById('cuenta').focus();document.getElementById('cuenta').select();">

						<a title="Cuentas presupuestales" onClick="despliegamodal2('visible', 'presupuesto');" style="cursor:pointer;"><img src="imagenes/find02.png" style="width:20px;"/></a>

						<input type="hidden" value="" name="bc" id="bc">
					</td>

					<td colspan="4">
						<input name="ncuenta" type="text" style="width:100%;" value="<?php echo @ $_POST['ncuenta']?>" readonly>
					</td>
				</tr>

				<?php
				$buscarClasificador = buscaClasificadorPresupuestal($_POST['cuenta'],1);
				
				if($buscarClasificador == '1')
				{
				?>
					<tr>
						<td style="width:3cm;" class="saludo1">CUIN: </td> 

						<td>
							<input type="text" id="cuin" name="cuin" style="width:88%" onKeyPress="javascript:return solonumeros(event)" onKeyUp="return tabular(event,this)" onBlur="buscacta2(event,1)" value="<?php echo $_POST['cuin']?>" onClick="document.getElementById('cuin').focus();document.getElementById('cuin').select();">

							<a title="CUIN" onClick="despliegamodal5('visible');" style="cursor:pointer;"><img src="imagenes/find02.png" style="width:20px;"/></a>
						</td>

						<td colspan="4">
							<input type="text" name="ncuin" id="ncuin" style="width:100%;" value="<?php echo $_POST['ncuin']?>"  readonly>
						</td>
					</tr>
					
				<?php
				}

				if($buscarClasificador == '2')
				{
				?>
					<tr>
						<td style="width: 3cm;" class="saludo1">Bienes Transportables: </td> 

						<td>
							<input type="text" id="clasificador" name="clasificador" style="width:88%" onKeyPress="javascript:return solonumeros(event)" onKeyUp="return tabular(event,this)" onBlur="buscacta2(event,2)" value="<?php echo $_POST['clasificador']?>" onClick="document.getElementById('clasificador').focus();document.getElementById('clasificador').select();">

							<a title="Cuentas presupuestales" onClick="despliegamodal3('visible');" style="cursor:pointer;"><img src="imagenes/find02.png" style="width:20px;"/></a>
						</td>

						<td colspan="4">
							<input type="text" name="nclasificador" id="nclasificador" style="width:100%;" value="<?php echo $_POST['nclasificador']?>"  readonly>
						</td>
					</tr>
				<?php
				}

				if($buscarClasificador == '3')
				{
				?>
					<tr>
						<td style="width: 3cm;" class="saludo1">Servicios: </td> 

						<td>
							<input type="text" id="clasificadorServicios" name="clasificadorServicios" style="width:88%" onKeyPress="javascript:return solonumeros(event)" onKeyUp="return tabular(event,this)" onBlur="buscacta2(event,3)" value="<?php echo $_POST['clasificadorServicios']?>" onClick="document.getElementById('clasificadorServicios').focus();document.getElementById('clasificadorServicios').select();">

							<a title="Clasificador Servicios" onClick="despliegamodal4('visible');" style="cursor:pointer;"><img src="imagenes/find02.png" style="width:20px;"/></a>
						</td>

						<td colspan="4">
							<input type="text" name="nclasificadorServicios" id="nclasificadorServicios" style="width:100%;" value="<?php echo $_POST['nclasificadorServicios']?>"  readonly>
						</td>
					</tr>
				<?php
				}
				?>

				<tr>
					<td class="tamano01">Estrato:</td>

					<td>
						<input type="text" name="id_estrato" id="id_estrato" value="<?php echo $_POST['id_estrato'] ?>" class="colordobleclik" style="height: 30px; width: 98%; text-align:center;" ondblclick="despliegamodal2('visible', 'estrato')" readonly>
					</td>

					<td colspan="4">
						<input type="text" name="nombreEstrato" id="nombreEstrato" value="<?php echo $_POST['nombreEstrato'] ?>" style="height: 30px; width: 100%;" readonly>
					</td>
				</tr>

				<tr>
					<td class="tamano01">Servicio:</td>
					
					<td>
						<select name="servicio" id="servicio" style="width:100%;" onchange='actualizar()' class="centrarSelect">
							<option class='aumentarTamaño' value="-1">SELECCIONE SERVICIO</option>
							<?php
								$sqlr="SELECT * FROM srvservicios";
								$resp = mysqli_query($linkbd,$sqlr);
								while ($row =mysqli_fetch_row($resp))
								{
									if(@ $_POST['servicio']==$row[0])
									{
										echo "<option class='aumentarTamaño' value='$row[0]' SELECTED>$row[0] - $row[1]</option>";
									}
									else{echo "<option class='aumentarTamaño' value='$row[0]'>$row[0] - $row[1]</option>";}
								}
							?>
						</select>
					</td>

					<td class="tamano01">Porcentaje:</td>

					<td>
						<input type="text" name="porcentaje" id="porcentaje" onKeyUp="return tabular(event,this)" value="<?php echo @ $_POST['porcentaje'];?>" style="width:90%;height:30px;text-align:center;" onKeyPress="javascript:return solonumeros(event)" onChange="document.form2.submit();" <?php if(@ $_POST['valor']!='' && @$_POST['valor'] != '0'){echo 'readonly';}?>/>
						%
					</td>

					<td class="tamano01" style="width:3cm;">Concepto:</td>
					<td>
						<select name="concecont" id="concecont" style="width:100%;" class="centrarSelect">
							<option class='aumentarTamaño' value="-1">SELECCIONE CONCEPTO</option>
							<?php
								$sqlr="SELECT * FROM conceptoscontables WHERE modulo='10' AND tipo='SB' ORDER BY codigo";
								$resp = mysqli_query($linkbd,$sqlr);
								while ($row =mysqli_fetch_row($resp))
								{
									if(@ $_POST['concecont']==$row[0])
									{
										echo "<option class='aumentarTamaño' value='$row[0]' SELECTED>$row[0] - $row[3] - $row[1]</option>";
									}
									else{echo "<option class='aumentarTamaño' value='$row[0]'>$row[0] - $row[3] - $row[1]</option>";}
								}
							?>
						</select>
					</td>
				</tr>

				<tr>
					<td class="tamano01" style="width: 3cm; text-align:center;">Vigencia:</td>

					<td style="width: 15%;">
						<select name="vigencia" id="vigencia" style="width: 98% !important;" class="centrarSelect">
                            <option class="aumentarTamaño" value="-1">SELECCIONE VIGENCIA</option>
							<?php
                                for($y = 0; $y < 10; $y++) {
                                    $anini = date('Y');
                                    $anfin = $anini - $y;

                                    if($anfin == $_POST['vigencia']) {
                                        echo "<option class='aumentarTamaño' value='$anfin' SELECTED>$anfin</option>";
									}
                                    else {
                                        echo "<option class='aumentarTamaño' value='$anfin'>$anfin</option>";
                                    }
                                }
							?>
						</select>
					</td>

					<td class="tamano01">Estado:</td>

					<td>
						<div class="onoffswitch">
							<input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox" id="myonoffswitch" value="<?php echo @ $_POST['onoffswitch'];?>" <?php if(@ $_POST['onoffswitch']=='S'){echo "checked";}?> onChange="cambiocheck();"/>
							<label class="onoffswitch-label" for="myonoffswitch">
								<span class="onoffswitch-inner"></span>
								<span class="onoffswitch-switch"></span>
							</label>
						</div>
					</td>
				</tr>
			</table>

			<input type="hidden" name="maximo" id="maximo" value="<?php echo @$_POST['maximo']?>"/>
			<input type="hidden" name="minimo" id="minimo" value="<?php echo @$_POST['minimo']?>"/>
			<input type="hidden" name="oculto" id="oculto" value="1"/>

			<?php
				if(@$_POST['oculto']=="2")
				{
					if (@$_POST['onoffswitch']!='S')
					{
						$valest='N';
					}
					else 
					{
						$valest='S';
					}

					//Saber si se va agregar CUIN, Bienes Transportable o Servicios
					$cuentaClasificadora = '';
					$clasificacion = '';

					if(isset($_POST['cuin']))
					{
						$clasificacion = '1';
						$cuentaClasificadora = $_POST['cuin'];
					}
					elseif(isset($_POST['clasificador']))
					{
						$clasificacion = '2';
						$cuentaClasificadora = $_POST['clasificador'];
					}
					elseif(isset($_POST['clasificadorServicios']))
					{
						$clasificacion = '3';
						$cuentaClasificadora = $_POST['clasificadorServicios'];
					}
					else
					{
						$clasificacion = '0';
						$cuentaClasificadora = '';
					}

					$sqlr ="UPDATE srvsubsidios SET nombre='$_POST[nomban]',id_estrato='$_POST[id_estrato]',porcentaje='$_POST[porcentaje]', concepto='$_POST[concecont]',estado='$valest', id_servicio = '$_POST[servicio]', aplicacion = '$_POST[aplicacion]',cuenta_presupuestal='$_POST[cuenta]',clasificacion=$clasificacion,clasificador=$cuentaClasificadora, vigencia = '$_post[vigencia]' WHERE id='$_POST[codban]' ";
					
					if (mysqli_query($linkbd,$sqlr)) {

						echo "<script>despliegamodalm('visible','3','Se ha Edito con Exito');</script>";
					}
					else {
						
						echo"<script>despliegamodalm('visible','2','No se pudo ejecutar la peticion');</script>";
					}
				}
			?>
		</form>

		<div id="bgventanamodal2">
			<div id="ventanamodal2">
				<IFRAME  src="" name="buster" marginWidth=0 marginHeight=0 frameBorder=0 id="ventana2" frameSpacing=0 style="left:500px; width:900px; height:500px; top:200;"></IFRAME>
			</div>
		</div>

	</body>
</html>