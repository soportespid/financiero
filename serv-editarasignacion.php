<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=iso-8859-1");
	require "comun.inc";
	require "funciones.inc";
	session_start();
	$linkbd = conectar_v7();
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
	$scroll=$_GET['scrtop'];
	$totreg=$_GET['totreg'];
	$idcta=$_GET['idcta'];
	$altura=$_GET['altura'];
	$filtro="'".$_GET['filtro']."'";
?>
<!DOCTYPE >
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es">
	<head>
		<meta http-equiv="Content-type" content="text/html; charset=iso-8859-1"/>
		<meta http-equiv="X-UA-Compatible" content="IE=9"/>
		<title>:: IDEAL 10 - Servicios P&uacute;blicos</title>
		<link href="css/css2.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/cssSP.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<script type="text/javascript" src="css/calendario.js"></script>
		<script type="text/javascript" src="css/programas.js"></script>
		
		<style></style>

		<script>
			 function despliegamodal2(_valor, _table)
			{
                document.getElementById('bgventanamodal2').style.visibility = _valor;

				if(_table == 'srvmedidores')
				{
					document.getElementById('ventana2').src = 'medidores-ventana.php?table=' + _table;
				}
                else if (_table == 'srvclientes')
                {
                    document.getElementById('ventana2').src = 'ventana-clienteservicio.php?table=' + _table;
                }
            }

			function despliegamodalm(_valor,_tip,mensa,pregunta)
			{
				document.getElementById("bgventanamodalm").style.visibility=_valor;

				if(_valor=="hidden"){document.getElementById('ventanam').src="";}
				else
				{
					switch(_tip)
					{
						case "1":	document.getElementById('ventanam').src="ventana-mensaje1.php?titulos="+mensa;break;
						case "2":	document.getElementById('ventanam').src="ventana-mensaje3.php?titulos="+mensa;break;
						case "3":	document.getElementById('ventanam').src="ventana-mensaje2.php?titulos="+mensa;break;
						case "4":	document.getElementById('ventanam').src="ventana-consulta1.php?titulos="+mensa+"&idresp="+pregunta;break;	
					}
				}
			}

			function respuestaModalBusqueda2(tabla, id, nombre, serial, referencia)
			{
				switch(tabla)
				{
					case 'srvmedidores': 
						document.getElementById('id_medidor').value = id;
						document.getElementById('serial').value = serial;
						document.getElementById('referencia').value = referencia;
						document.form2.submit();
					break;	
                }
            }
			
			function respuestaconsulta(pregunta)
			{
				switch(pregunta)
				{
					case "1":	
						document.form2.oculto.value='2';
						document.form2.submit();
					break;
				}
			}

			function guardar()
			{
				var validacion01 = document.getElementById('codban').value;
				var validacion02 = document.getElementById('cliente').value;
				var validacion03 = document.getElementById('servicio').value;

				if (validacion01.trim()!='' && validacion02.trim() && validacion03 != '-1') 
				{
					despliegamodalm('visible','4','Esta Seguro de Modificar','1');
				}
				else 
				{
					despliegamodalm('visible','2','Falta información para modificar la Asignación');
				}
			}

			function cambiocheck()
			{
				if(document.getElementById('myonoffswitch').value=='S'){document.getElementById('myonoffswitch').value='N';}
				else{document.getElementById('myonoffswitch').value='S';}
				document.form2.submit();
			}

			function cambiocheck2()
			{
				if(document.getElementById('tarifaMedidor').value=='S')
				{
					document.getElementById('tarifaMedidor').value='N';
				}
				else
				{
					document.getElementById('tarifaMedidor').value='S';
				}

				document.form2.submit();
			}

			function iratras(scrtop, numpag, limreg, filtro)
			{
				var idcta=document.getElementById('codban').value;

				location.href="serv-buscaasignacion.php?idcta="+idcta+"&scrtop="+scrtop+"&numpag="+numpag+"&limreg="+limreg+ "&filtro="+filtro;
			}

			function adelante(scrtop, numpag, limreg, filtro)
			{
				var maximo=document.getElementById('maximo').value;
				var actual=document.getElementById('codban').value;

				actual=parseFloat(actual)+1;

				if(actual<=parseFloat(maximo))
				{
					if(actual<10){actual="0"+actual;}
					location.href="serv-editarasignacion.php?idban=" +actual+ "&scrtop=" +scrtop+ "&numpag=" +numpag+ "&limreg=" +limreg+ "&filtro=" +filtro;
				}
			}

			function atrasc(scrtop, numpag, limreg, filtro, prev)
			{
				var minimo=document.getElementById('minimo').value;
				var actual=document.getElementById('codban').value;
				actual=parseFloat(actual)-1;
				if(actual>=parseFloat(minimo))
				{
					if(actual<10){actual="0"+actual;}
					location.href="serv-editarasignacion.php?idban=" +actual+ "&scrtop=" +scrtop+ "&numpag=" +numpag+ "&limreg=" +limreg+ "&filtro=" +filtro;
				}
			}
		</script>

		<?php titlepag();?>

	</head>
	
	<body>
		<IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
		<span id="todastablas2"></span>
		<?php
			$numpag=@ $_GET['numpag'];
			$limreg=@ $_GET['limreg'];
			$scrtop=26*$totreg;
		?>

		<table>
			<tr><script>barra_imagenes("serv");</script><?php cuadro_titulos();?></tr>	 
			
			<tr><?php menu_desplegable("serv");?></tr>

			<tr>
				<td colspan="3" class="cinta">
					<a href="serv-asignacion.php" class="mgbt"><img src="imagenes/add.png"/></a>

					<a onclick="guardar()" class="mgbt"><img src="imagenes/guarda.png" title="Guardar"/></a>

					<a href="serv-buscaasignacion.php" class="mgbt"><img src="imagenes/busca.png" title="Buscar"/></a>

					<a onclick="mypop=window.open('plan-agenda.php','','');mypop.focus()" class="mgbt"><img src="imagenes/agenda1.png" title="Agenda" /></a>

					<a onclick="mypop=window.open('serv-principal.php','','');mypop.focus();" class="mgbt"><img src="imagenes/nv.png" title="Nueva ventana"></a>

					<a onclick="mypop=window.open('<?php echo $url2; ?>','','');mypop.focus();" class="mgbt"><img src="imagenes/duplicar_pantalla.png" title="Duplicar pesta&ntilde;a"></a>

					<a onClick="iratras(<?php echo "$scrtop, $numpag, $limreg, $filtro"; ?>)" class="mgbt"><img src="imagenes/iratras.png" title="Atr&aacute;s"></a>
				</td>
			</tr>
		</table>

		<div id="bgventanamodalm" class="bgventanamodalm">
			<div id="ventanamodalm" class="ventanamodalm">
				<IFRAME src="" name="ventanam" marginWidth=0 marginHeight=0 frameBorder=0 id="ventanam" frameSpacing=0 style=" width:700px; height:130px; top:200; overflow:hidden;"></IFRAME>
			</div>
		</div>

		<form name="form2" method="post" action="">
			<?php
				if(@$_POST['oculto']=="")
				{
					$sqlr="SELECT MIN(id), MAX(id) FROM srvasignacion_servicio";
					$res=mysqli_query($linkbd,$sqlr);
					$r=mysqli_fetch_row($res);
					$_POST['minimo']=$r[0];
					$_POST['maximo']=$r[1];
					
					$sqlr="SELECT * FROM srvasignacion_servicio WHERE id='".$_GET['idban']."'";
					$resp = mysqli_query($linkbd,$sqlr);
					$row =mysqli_fetch_row($resp); 
					$_POST['codban']				=	$row[0];
					$_POST['cliente']				=	$row[1];
					$_POST['frecuencia']			=	$row[2];
					$_POST['id_medidor']			=	$row[3];
					$_POST['servicio']				=	$row[4];
					$_POST['fecha_activacion']		=	date('d/m/Y',strtotime($row[5]));
					$_POST['fecha_cancelacion']		=	date('d/m/Y',strtotime($row[6]));
					$_POST['tarifaMedidor']			=   $row[7];
					$_POST['novedad_cancelacion']	=	$row[8];
					$_POST['onoffswitch']			=	$row[9];

					$sqlr="SELECT * FROM srvmedidores WHERE id='".$_POST['id_medidor']."' AND estado='S'";
					$resp = mysqli_query($linkbd,$sqlr);
					$row =mysqli_fetch_row($resp);
					$_POST['serial'] 		= $row[4];
					$_POST['referencia'] 	= $row[5];
					$_POST['medidor_actual']= $row[0];

					$sqlServicio = "SELECT nombre FROM srvservicios WHERE id='$_POST[servicio]' ";
					$resServicio = mysqli_query($linkbd,$sqlServicio);
					$rowServicio = mysqli_fetch_row($resServicio);

					$_POST['servicio'] = $rowServicio[0];
				}
			?>
			<table class="inicio ancho">
				<tr>
					<td class="titulos" colspan="7">.: Editar Asignaci&oacute;n</td>
					<td class="cerrar" style="width:7%" onClick="location.href='serv-principal.php'">Cerrar</td>
				</tr>
				<tr>
					<td class="tamano01">C&oacute;digo:</td>
					<td style="width:15%;">
						<img src="imagenes/back.png" onClick="atrasc(<?php echo "$scrtop, $numpag, $limreg, $filtro"; ?>)" class="icobut" title="Anterior"/>&nbsp;
						<input type="text" name="codban" id="codban" onKeyUp="return tabular(event,this)" value="<?php echo @ $_POST['codban'];?>" style="width:60%;height:30px;text-align: center;" readonly/>&nbsp;
						<img src="imagenes/next.png" onClick="adelante(<?php echo "$scrtop, $numpag, $limreg, $filtro" ?>);" class="icobut" title="Sigiente"/>
					</td>
					<td class="tamano01">Cliente:</td>
					<td>
						<input type="text" name="cliente" id="cliente" value="<?php echo str_pad($_POST['cliente'], 10, "0", STR_PAD_LEFT);?>" style="width:100%; height:30px;text-transform:uppercase" readonly/>
						<?php
							$sqlr="SELECT id_tercero FROM srvclientes WHERE id='".$_POST['cliente']."'";
							$resp = mysqli_query($linkbd,$sqlr);
							$row =mysqli_fetch_row($resp);
							
							$sqlr="SELECT nombre1, nombre2, apellido1, apellido2, razonsocial FROM terceros WHERE id_tercero='".$row[0]."'";
							$resp = mysqli_query($linkbd,$sqlr);
							$row =mysqli_fetch_assoc($resp);

							$nombreCompleto = $row['nombre1'].' '.$row['nombre2'].' '.$row['apellido1'].' '.$row['apellido2'];
                            if ($row['razonsocial']!='') {
                                $nombreCompleto = $row['razonsocial'];
                            }

							$_POST['ntercero'] = $nombreCompleto;
						?>
					</td>
					<td>
						<input type="text" name="ntercero" id="ntercero" value="<?php echo $_POST['ntercero'];?>" style="width:100%; height:30px;text-transform:uppercase" readonly/>
					</td>
					<td class="tamano01" style="width:3cm;">Fecha Activaci&oacute;n:</td>
					<td style="width:15%;">
                        <input type="text" name="fecha_activacion" value="<?php echo @ $_POST['fecha_activacion']?>" style="width: 100%; text-align:center;" readonly>
                    </td>
				</tr>
				
				<tr>
                    <td class="tamano01">Servicio:</td>

					<td>
						<input type="text" name="servicio" id="servicio" value="<?php echo @$_POST['servicio'] ?>" style="width: 100%;" readonly>
					</td>
				</tr>

				<tr>
					<td class="tamano01">Tarifa con medidor:</td>

					<td>
						<div class="onoffswitch">
							<input type="checkbox" name="tarifaMedidor" class="onoffswitch-checkbox" id="tarifaMedidor" value="<?php echo @ $_POST['tarifaMedidor'];?>" <?php if(@ $_POST['tarifaMedidor']=='S'){echo "checked";}?> onChange="cambiocheck2();"/>
							<label class="onoffswitch-label" for="tarifaMedidor">
								<span class="onoffswitch-inner"></span>
								<span class="onoffswitch-switch"></span>
							</label>
						</div>
					</td>

					<?php
						if($_POST['tarifaMedidor'] == 'S')
						{
					?>
							<td class="saludo1">Medidor:</td>
							
							<td>
								<input type="text" name='serial' id='serial'  value="<?php echo @$_POST['serial']?>" style="width: 100%; height: 30px;" onclick = "despliegamodal2('visible', 'srvmedidores');"  class="colordobleclik" readonly>
								<input type="hidden" name='id_medidor' id='id_medidor' value="<?php echo @$_POST['id_medidor']?>">
								<input type="hidden" name='medidor_actual' id='medidor_actual' value="<?php echo $_POST['medidor_actual']?>">
							</td>
					<?php
						}
					?>
				</tr>

				<tr>
				<?php
					if (@ $_POST['onoffswitch'] == '' || $_POST['onoffswitch'] == 'N' ) {
				?>
					<td class="tamano01" style="width:10%;">Fecha Cancelacion:</td>
					<td style="width:15%;">
						<input type="text" name="fecha_cancelacion" value="<?php echo @ $_POST['fecha_cancelacion']?>" maxlength="10" onKeyPress="javascript:return solonumeros(event)" onKeyUp="return tabular(event,this)" id="fc_1198971546" onKeyDown="mascara(this,'/',patron,true)" title="DD/MM/YYYY" style="width:80%;height:30px;">&nbsp;
						<img src="imagenes/calendario04.png" onClick="displayCalendarFor('fc_1198971546');" title="Calendario" class="icobut"/>
					</td>
					<td class="tamano01">Novedad:</td>
					<td>
						<input type="text" name="novedad_cancelacion" id="novedad_cancelacion" value="<?php echo @ $_POST['novedad_cancelacion']?>" style="width:100%;height:30px;" />	
					</td>
				<?php
					}
				?>
					<td class="tamano01">Estado:</td>
					<td>
						<div class="onoffswitch">
							<input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox" id="myonoffswitch" value="<?php echo @ $_POST['onoffswitch'];?>" <?php if(@ $_POST['onoffswitch']=='S'){echo "checked";}?> onChange="cambiocheck();"/>
							<label class="onoffswitch-label" for="myonoffswitch">
								<span class="onoffswitch-inner"></span>
								<span class="onoffswitch-switch"></span>
							</label>
						</div>
					</td>
				</tr>
			</table>
			<input type="hidden" name="maximo" id="maximo" value="<?php echo @ $_POST['maximo']?>"/>
			<input type="hidden" name="minimo" id="minimo" value="<?php echo @ $_POST['minimo']?>"/>
			<input type="hidden" name="oculto" id="oculto" value="1"/>
			<?php
				if(@ $_POST['oculto']=="2")
				{
					if (@ $_POST['onoffswitch']!='S')
					{
						$valest='N';
					}
					else 
					{
						$valest='S';
					}

					if (@ $_POST['tarifaMedidor']!='S')
					{
						$tarifaMedidor='N';
					}
					else 
					{
						$tarifaMedidor='S';
					}

					if ($_POST['id_medidor'] != $_POST['medidor_actual']) 
					{
						$sqlr ="UPDATE srvmedidores SET asignacion='S' WHERE id='".$_POST['id_medidor']."'";
						mysqli_query($linkbd,$sqlr);

						$sqlr ="UPDATE srvmedidores SET asignacion='N' WHERE id='".$_POST['medidor_actual']."'";
						mysqli_query($linkbd,$sqlr);
					}

					$sqlr='';

					if (@$_POST['novedad_cancelacion']!='' && @$_POST['fecha_cancelacion']!='' && $valest=='N') 
					{
						preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST['fecha_cancelacion'],$f);

						$fecha="$f[3]-$f[2]-$f[1]";

						$sqlr ="UPDATE srvasignacion_servicio SET id_medidor='".$_POST['id_medidor']."', fecha_cancelacion='$fecha', tarifa_medidor='$tarifaMedidor',novedad='".@$_POST['novedad_cancelacion']."', estado='$valest' WHERE id='".$_POST['codban']."'";
					}
					elseif($valest=='S') 
					{
						$sqlr ="UPDATE srvasignacion_servicio SET id_medidor='".$_POST['id_medidor']."', fecha_cancelacion='', novedad='', estado='$valest',tarifa_medidor='$tarifaMedidor' WHERE id='".$_POST['codban']."'";
					}

					if ($sqlr!='') 
					{
						mysqli_query($linkbd,$sqlr);
						echo "<script>despliegamodalm('visible','3','Se ha Editado con Exito');</script>";
					}
					else 
					{
						echo"<script>despliegamodalm('visible','2','No se pudo ejecutar la petición');</script>";
					}
				}
			?>
		</form>
		<div id="bgventanamodal2">
			<div id="ventanamodal2">
				<IFRAME  src="" name="buster" marginWidth=0 marginHeight=0 frameBorder=0 id="ventana2" frameSpacing=0 style="left:500px; width:900px; height:500px; top:200;"></IFRAME>
			</div>
		</div>
	</body>
</html>