<?php

    header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require 'comun.inc';
	require 'funciones.inc';
	session_start();
    if(empty($_SESSION)){
        header("location: index.php");
    }
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html lang=es>
	<head>
    <meta charset="utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1"/>
		<meta name="viewport" content="user-scalable=no">
		<title>:: IDEAL 10 - Tesorería</title>
		<link href="favicon.ico" rel="shortcut icon"/>
		<link href="css/css2z.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
        <link href="css/tabs.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
        <link href="css/style.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" href="sweetalert2/dist/sweetalert2.min.css">
        <script type="text/javascript" src="css/programas.js"></script>
        <script src="sweetalert2/dist/sweetalert2.min.js"></script>
		<script src="vue/vue.min.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script src="css/calendario.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
	</head>
	<body>
		<header>
			<table>
				<tr><script>barra_imagenes("teso");</script><?php cuadro_titulos();?></tr>
			</table>
		</header>

		<form name="form2" method="post" action="">
			<section id="myapp" v-cloak >
                <div id="cargando" v-show="isLoading" class="loading" style="z-index: 10000;">
                    <span>Cargando...</span>
                </div>
				<nav>
					<table>
						<tr><?php menu_desplegable("teso");?></tr>
                        <tr>
							<td colspan="3" class="cinta">
                                <img src="imagenes/add2.png"  class="mgbt" title="Nuevo">
								<img src="imagenes/guardad.png"   title="Guardar"  class="mgbt">
								<img src="imagenes/busca.png" @click="window.location.href='teso-modificarBancoIngresosBuscar.php'"   class="mgbt" title="Buscar">
                                <a @click="mypop=window.open('plan-agenda.php','','');mypop.focus()" class="mgbt"><img src="imagenes/agenda1.png" title="Agenda" /></a>
								<img src="imagenes/nv.png" @click="mypop=window.open('teso-principal.php','',''); mypop.focus();" class="mgbt" title="Nueva Ventana">
                                <a @click="mypop=window.open('teso-teso-modificabancos.php','','');mypop.focus();" class="tooltip bottom mgbt"><img src="imagenes/duplicar_pantalla.png" title="Duplicar pesta&ntilde;a"></a>
                                <a href="teso-modificabancos.php"  class="mgbt"><img src="imagenes/iratras.png" title="Atr&aacute;s"></a>
                            </td>
						</tr>
					</table>
				</nav>
				<article>
                    <!--TABS-->
                    <div ref="rTabs" class="nav-tabs inicio">
                        <div class="nav-item active" @click="showTab(1)">Recibos General</div>
                        <div class="nav-item" @click="showTab(2)">Historial de cambios</div>
                    </div>
                    <div ref="rTabsContent" class="nav-tabs-content">
                        <div class="nav-content active">
                            <div class="inicio">
                                <div>
                                    <h2 class="titulos m-0">.: Cambio de bancos para ingresos internos</h2>
                                    <div class="form-control w-50">
                                        <div class="d-flex">
                                            <input type="search" placeholder="Buscar por número de recibo" @keyup="search('recibos',1)" v-model="txtSearch">
                                        </div>
                                    </div>
                                    <h2 class="titulos m-0">.: Resultados: {{ txtResults}} </h2>
                                </div>
                                <div class="overflow-auto max-vh-50 overflow-x-hidden p-2" >
                                    <table class="table table-hover text-center fw-normal">
                                        <thead>
                                            <tr>
                                                <th>No Recibo</th>
                                                <th>Concepto</th>
                                                <th>Fecha</th>
                                                <th>Documento</th>
                                                <th>Contribuyente</th>
                                                <th>Valor</th>
                                                <th>No.liquidación</th>
                                                <th class="text-center">Estado</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr @dblclick="editItem(data.id_recibos)" v-for="data in arrData" :key="data.id_recibos">
                                                <td>{{ data.id_recibos }}</td>
                                                <td class="text-left">{{ data.descripcion}}</td>
                                                <td>{{ data.fecha}}</td>
                                                <td class="text-left">{{ data.tercero.documento}}</td>
                                                <td class="text-left">{{ data.tercero.nombre}}</td>
                                                <td class="text-right">{{data.valor}}</td>
                                                <td class="text-center">{{data.id_recaudo}}</td>
                                                <td class="text-center">
                                                    <span :class="[data.estado =='S' ? 'badge-success' : 'badge-danger']"class="badge">{{ data.estado == "S"  ? "Activo" : "Inactivo"}}</span>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div v-if="arrData !=''" class="inicio">
                                <p style="text-align:center">Página {{ intPage }} de {{ intTotalPages }}</p>
                                <ul style="list-style:none; padding:0;display:flex;justify-content: center;align-items:center">
                                    <li v-show="intPage > 1" @click="search('recibos',intPage = 1)" style="cursor:pointer;padding:4px 10px;background:#fff; border:1px solid #39c"><< </li>
                                    <li v-show="intPage > 1" @click="search('recibos',--intPage)" style="cursor:pointer;padding:4px 10px;background:#fff; border:1px solid #39c" ><</li>
                                    <li v-show="intPage < intTotalPages" @click="search('recibos',++intPage)" style="cursor:pointer;padding:4px 10px;background:#fff; border:1px solid #39c" >></li>
                                    <li v-show="intPage < intTotalPages" @click="search('recibos',intPage = intTotalPages)" style="cursor:pointer;padding:4px 10px;background:#fff; border:1px solid #39c">>></li>
                                </ul>
                            </div>
                        </div>
                        <div class="nav-content">
                            <div class="inicio">
                                <div>
                                    <h2 class="titulos m-0">.: Historial de cambios de bancos para otros ingresos</h2>
                                    <div class="form-control w-50">
                                        <div class="d-flex">
                                            <input type="search" placeholder="Buscar por número de recibo" @keyup="search('historial',1)" v-model="txtSearchHistorial">
                                        </div>
                                    </div>
                                    <h2 class="titulos m-0">.: Resultados: {{ txtResultsHistorial}} </h2>
                                </div>
                                <div class="overflow-auto max-vh-50 overflow-x-hidden p-2" >
                                    <table class="table text-center fw-normal">
                                        <thead>
                                            <tr>
                                                <th>No Recibo</th>
                                                <th>Fecha de cambio</th>
                                                <th>Usuario</th>
                                                <th>Cuenta anterior</th>
                                                <th>Cuenta nueva</th>
                                                <th>Concepto</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr v-for="data in arrHistorial" :key="data.id_recibos">
                                                <td>{{ data.id_recibos }}</td>
                                                <td class="text-left">{{ data.fecha}}</td>
                                                <td class="text-left">{{ data.nom_usu}}</td>
                                                <td class="text-left">{{ data.cuentabanco_ant}}</td>
                                                <td class="text-left">{{ data.cuentabanco_nu}}</td>
                                                <td class="text-left">{{data.concepto}}</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div v-if="arrHistorial !=''" class="inicio">
                                <p style="text-align:center">Página {{ intPageHistorial }} de {{ intTotalPagesHistorial }}</p>
                                <ul style="list-style:none; padding:0;display:flex;justify-content: center;align-items:center">
                                    <li v-show="intPageHistorial > 1" @click="search('historial',intPageHistorial = 1)" style="cursor:pointer;padding:4px 10px;background:#fff; border:1px solid #39c"><< </li>
                                    <li v-show="intPageHistorial > 1" @click="search('historial',--intPageHistorial)" style="cursor:pointer;padding:4px 10px;background:#fff; border:1px solid #39c" ><</li>
                                    <li v-show="intPageHistorial < intTotalPagesHistorial" @click="search('historial',++intPageHistorial)" style="cursor:pointer;padding:4px 10px;background:#fff; border:1px solid #39c" >></li>
                                    <li v-show="intPageHistorial < intTotalPagesHistorial" @click="search('historial',intPageHistorial = intTotalPagesHistorial)" style="cursor:pointer;padding:4px 10px;background:#fff; border:1px solid #39c">>></li>
                                </ul>
                            </div>
                        </div>
                    </div>
				</article>
			</section>
		</form>

		<script src="node_modules/read-excel-file/bundle/read-excel-file.min.js"></script>
		<script src="node_modules/xlsx/dist/xlsx.full.min.js"></script>
        <script src="Librerias/vue/vue.min.js"></script>
		<script src="Librerias/vue/axios.min.js"></script>
		<script type="module" src="tesoreria/modificarbanco_ingresos/buscar/teso-ingresosBuscar.js?<?= date('d_m_Y_h_i_s');?>"></script>

	</body>
</html>
