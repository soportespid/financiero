<?php
header("Cache-control: no-cache, no-store, must-revalidate");
header("Content-Type: text/html;charset=utf8");
require "comun.inc";
require "funciones.inc";
require "conversor.php";
require 'funcionesnomima.inc';
session_start();
$linkbd = conectar_v7();
$linkbd->set_charset("utf8");
cargarcodigopag($_GET['codpag'], $_SESSION["nivel"]);
date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE>
<html lang=es>

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1" />
    <meta name="viewport" content="user-scalable=no">
    <title>:: IDEAL 10 - Contabilidad</title>
    <link href="css/css2.css?<?php echo date('d_m_Y_h_i_s'); ?>" rel="stylesheet" type="text/css" />
    <link href="css/css3.css?<?php echo date('d_m_Y_h_i_s'); ?>" rel="stylesheet" type="text/css" />
    <link href="css/tabs.css?<?php echo date('d_m_Y_h_i_s'); ?>" rel="stylesheet" type="text/css" />
    <link href="css/style.css?<?php echo date('d_m_Y_h_i_s'); ?>" rel="stylesheet" type="text/css" />
    <link href="sweetalert2/dist/sweetalert2.min.css" rel="stylesheet">
    <script src="sweetalert2/dist/sweetalert2.min.js"></script>
    <script type="text/javascript" src="css/programas.js"></script>
    <script type="text/javascript" src="css/calendario.js"></script>
    <script type='text/javascript' src='JQuery/jquery-2.1.4.min.js'></script>
    <script>
        function validar() { document.form2.submit(); }
        function buscaop(e) {
            if (document.form2.orden.value != "") {
                document.form2.bop.value = '1';
                document.form2.submit();
            }
        }
        function guardar() {
            if (document.form2.fecha.value != '') {
                Swal.fire({
                    icon: 'question',
                    title: '¿Seguro que quieres reflejar la información?',
                    showDenyButton: true,
                    confirmButtonText: 'Guardar',
                    confirmButtonColor: '#01CC42',
                    denyButtonText: 'Cancelar',
                    denyButtonColor: '#FF121A',
                }).then(
                    (result) => {
                        if (result.isConfirmed) {
                            document.form2.oculto.value = 2;
                            document.form2.submit();
                        }
                        else if (result.isDenied) {
                            Swal.fire({
                                icon: 'info',
                                title: 'No se reflejo la información',
                                confirmButtonText: 'Continuar',
                                confirmButtonColor: '#FF121A',
                                timer: 2500
                            });
                        }
                    }
                )
            } else {
                Swal.fire({
                    icon: 'error',
                    title: 'Error!',
                    text: 'Faltan datos para poder reflejar',
                    confirmButtonText: 'Continuar',
                    confirmButtonColor: '#FF121A',
                    timer: 2500
                });
            }
        }
        function calcularpago() {
            valorp = document.form2.valor.value;
            descuentos = document.form2.totaldes.value;
            valorc = valorp - descuentos;
            document.form2.valorcheque.value = valorc;
            document.form2.valoregreso.value = valorp;
            document.form2.valorretencion.value = descuentos;
        }
        function pdf() {
            document.form2.action = "pdfegresonomina.php";
            document.form2.target = "_BLANK";
            document.form2.submit();
            document.form2.action = "";
            document.form2.target = "";
        }
        function adelante() {
            if (parseFloat(document.form2.ncomp.value) < parseFloat(document.form2.maximo.value)) {
                document.form2.oculto.value = 1;
                document.form2.ncomp.value = parseFloat(document.form2.ncomp.value) + 1;
                document.form2.egreso.value = parseFloat(document.form2.egreso.value) + 1;
                document.form2.action = "cont-egresosnomina-reflejarccpet.php";
                document.form2.submit();
            }
        }
        function atrasc() {
            if (document.form2.ncomp.value > 1) {
                document.form2.oculto.value = 1;
                document.form2.ncomp.value = document.form2.ncomp.value - 1;
                document.form2.egreso.value = document.form2.egreso.value - 1;
                document.form2.action = "cont-egresosnomina-reflejarccpet.php";
                document.form2.submit();
            }
        }
        function validar2() {
            document.form2.oculto.value = 1;
            document.form2.ncomp.value = document.form2.egreso.value;
            document.form2.action = "cont-egresosnomina-reflejarccpet.php";
            document.form2.submit();
        }
        function despliegamodalm(_valor, _tip, mensa, pregunta) {
            document.getElementById("bgventanamodalm").style.visibility = _valor;
            if (_valor == "hidden") { document.getElementById('ventanam').src = ""; }
            else {
                switch (_tip) {
                    case "1":
                        document.getElementById('ventanam').src = "ventana-mensaje1.php?titulos=" + mensa; break;
                    case "2":
                        document.getElementById('ventanam').src = "ventana-mensaje3.php?titulos=" + mensa; break;
                    case "3":
                        document.getElementById('ventanam').src = "ventana-mensaje2.php?titulos=" + mensa; break;
                    case "4":
                        document.getElementById('ventanam').src = "ventana-consulta1.php?titulos=" + mensa + "&idresp=" + pregunta; break;
                }
            }
        }
        function respuestaconsulta(pregunta) {
            switch (pregunta) {
                case "1":
                    document.form2.oculto.value = 2;
                    document.form2.submit();
                    break;
            }
        }
        function funcionmensaje() { }
        var ctrlPressed = false;
        var tecla01 = 17, tecla02 = 37, tecla03 = 39;
        $(document).keydown(function (e) {
            if (e.keyCode == tecla01) { ctrlPressed = true; }
            if (ctrlPressed && (e.keyCode == tecla02)) { atrasc(); }
        });
        $(document).keydown(function (e) {
            if (ctrlPressed && (e.keyCode == tecla03)) { adelante(); }
        });
        $(document).keyup(function (e) {
            if (e.keyCode == tecla01) { ctrlPressed = false; }
        });
    </script>
    <?php titlepag(); ?>
</head>

<body>
    <IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
    <span id="todastablas2"></span>
    <table>
        <tr>
            <script>barra_imagenes("cont");</script><?php cuadro_titulos(); ?>
        </tr>
        <tr><?php menu_desplegable("cont"); ?></tr>
    </table>
    <div class="bg-white group-btn p-1"><button type="button"
            onclick="mypop=window.open('cont-principal.php','','');mypop.focus();"
            class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
            <span>Nueva ventana</span>
            <svg class="fill-black group-hover:fill-white w-5 h-5" xmlns="http://www.w3.org/2000/svg"
                viewBox="0 -960 960 960">
                <path
                    d="M320-120v-80h80v-80H160q-33 0-56.5-23.5T80-360v-400q0-33 23.5-56.5T160-840h640q33 0 56.5 23.5T880-760v400q0 33-23.5 56.5T800-280H560v80h80v80H320ZM160-360h640v-400H160v400Zm0 0v-400 400Z">
                </path>
            </svg>
        </button><button type="button"
            onclick="mypop=window.open('/financiero/cont-egresosnomina-reflejarccpet.php','','');mypop.focus();"
            class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
            <span class="group-hover:text-white">Duplicar pantalla</span>
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960">
                <path
                    d="M640-160v-360H160v360h480Zm80-200v-80h80v-360H320v200h-80v-200q0-33 23.5-56.5T320-880h480q33 0 56.5 23.5T880-800v360q0 33-23.5 56.5T800-360h-80ZM160-80q-33 0-56.5-23.5T80-160v-360q0-33 23.5-56.5T160-600h480q33 0 56.5 23.5T720-520v360q0 33-23.5 56.5T640-80H160Zm400-603ZM400-340Z">
                </path>
            </svg>
        </button><button type="button" onclick="guardar()"
            class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
            <span>Reflejar</span>
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960">
                <path
                    d="M400-280h160v-80H400v80Zm0-160h280v-80H400v80ZM280-600h400v-80H280v80Zm200 120ZM265-80q-79 0-134.5-55.5T75-270q0-57 29.5-102t77.5-68H80v-80h240v240h-80v-97q-37 8-61 38t-24 69q0 46 32.5 78t77.5 32v80Zm135-40v-80h360v-560H200v160h-80v-160q0-33 23.5-56.5T200-840h560q33 0 56.5 23.5T840-760v560q0 33-23.5 56.5T760-120H400Z">
                </path>
            </svg>
        </button><button type="button" onclick="location.href='cont-reflejardocsccpet.php'"
            class="btn btn-success btn-success-hover d-flex justify-between align-items-center">
            <span>Atras</span>
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960">
                <path
                    d="M760-200v-160q0-50-35-85t-85-35H273l144 144-57 56-240-240 240-240 57 56-144 144h367q83 0 141.5 58.5T840-360v160h-80Z">
                </path>
            </svg>
        </button></div>
    <div id="bgventanamodalm" class="bgventanamodalm">
        <div id="ventanamodalm" class="ventanamodalm">
            <IFRAME src="" name="ventanam" marginWidth=0 marginHeight=0 frameBorder=0 id="ventanam" frameSpacing=0
                style=" width:700px; height:130px; top:200; overflow:hidden;"></IFRAME>
        </div>
    </div>
    <?php
    $sqlr = "SELECT * FROM admbloqueoanio";
    $res = mysqli_query($linkbd, $sqlr);
    $_POST['anio'] = array();
    $_POST['bloqueo'] = array();
    while ($row = mysqli_fetch_row($res)) {
        $_POST['anio'][] = $row[0];
        $_POST['bloqueo'][] = $row[1];
    }
    $sqlr = "SELECT sueldo, cajacompensacion, icbf, sena, iti, esap, arp, salud_empleador, salud_empleado, pension_empleador,pension_empleado, sub_alimentacion, aux_transporte FROM humparametrosliquida";
    $resp = mysqli_query($linkbd, $sqlr);
    while ($row = mysqli_fetch_row($resp)) {
        $_POST['salmin'] = '01';
        $_POST['cajacomp'] = $row[1];
        $_POST['icbf'] = $row[2];
        $_POST['sena'] = $row[3];
        $_POST['iti'] = $row[4];
        $_POST['esap'] = $row[5];
        $_POST['arp'] = $row[6];
        $_POST['salud_empleador'] = $row[7];
        $_POST['salud_empleado'] = $row[8];
        $_POST['pension_empleador'] = $row[9];
        $_POST['pension_empleado'] = $row[10];
        $_POST['auxtran'] = $row[12];
        $_POST['auxalim'] = $row[11];
    }
    if ($_POST['oculto'] == '2') {
        preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST['fecha'], $fecha);
        $fechaf = "$fecha[3]-$fecha[2]-$fecha[1]";
        $bloq = bloqueos($_SESSION['cedulausu'], $fechaf);
        if ($bloq >= 1) {
            //***********crear el contabilidad
            $ideg = $_POST['egreso'];
            $sqlr = "DELETE FROM comprobante_cab WHERE numerotipo = '$ideg' AND tipo_comp = '17'";
            mysqli_query($linkbd, $sqlr);
            $sqlr = "DELETE FROM comprobante_det WHERE id_comp='17 $ideg' ";
            mysqli_query($linkbd, $sqlr);
            //************CREACION DEL COMPROBANTE CONTABLE ************************
            $sqlr = "SELECT count(*) FROM tesoegresosnomina WHERE id_egreso='" . $_POST['egreso'] . "' and estado ='S'";
            $res = mysqli_query($linkbd, $sqlr);
            $row = mysqli_fetch_row($res);
            $nreg = $row[0];
            if ($nreg >= 0) {
                $sqlr = "INSERT INTO tesoegresosnomina_cheque (id_cheque,id_egreso,estado,motivo) values ('" . $_POST['ncheque'] . "','$ideg','S','')";
                mysqli_query($linkbd, $sqlr);
                //***********crear el contabilidad
                $sqlr = "INSERT INTO comprobante_cab (numerotipo, tipo_comp, fecha, concepto, total, total_debito, total_credito, diferencia, estado) values ('$ideg','17', '$fechaf', '" . $_POST['concepto'] . "', '0', '" . $_POST['valpalgar'] . "', '" . $_POST['valpalgar'] . "', '0', '" . $_POST['estadoc'] . "')";
                mysqli_query($linkbd, $sqlr);
                $idcomp = mysqli_insert_id($linkbd);
                $estado_cuentas = 0;
                for ($y = 0; $y < count($_POST['tedet']); $y++) {
                    switch ($_POST['tedet'][$y]) {
                        case '':
                            break;
                        case 'SR':	//**** Tipo SR: Pago Salud Empresa ****
                        {
                            $vnom = $_POST['devalores'][$y];
                            if ($vnom > 0) {
                                $ctacont1 = cuentascontablesccpet::cuentadebito_egreso($_POST['derecursos'][$y], $_POST['deccs'][$y], $_POST['fechafi'], '');
                                $ccosto = $_POST['deccs'][$y];
                                $ncppto = nombrevariblespagonomina($_POST['tedet'][$y]);
                                $sqlr = "INSERT INTO comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque,valdebito,valcredito,estado,vigencia, numacti,cantarticulo) values ('17 $ideg','$ctacont1','" . $_POST['tercero'] . "','" . $_POST['deccs'][$y] . "','Pago Salud Empleador','','$vnom','0','1','" . $_POST['vigencia'] . "','0','0')";
                                mysqli_query($linkbd, $sqlr);
                                if (trim($ctacont1) == '') {
                                    $estado_cuentas++;
                                }
                                //***************************
                                $valorp = $_POST['devalores'][$y];
                                $sqlr = "insert into comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque, valdebito,valcredito,estado,vigencia, numacti,cantarticulo) values ('17 $ideg','" . $_POST['banco'] . "','" . $_POST['tercero'] . "','" . $_POST['deccs'][$y] . "','Pago Banco Salud Empleador', '" . $_POST['cheque'] . " " . $_POST['ntransfe'] . "','0','$vnom','1','" . $_POST['vigencia'] . "','0','0')";
                                mysqli_query($linkbd, $sqlr);
                                if (trim($_POST['banco']) == '') {
                                    $estado_cuentas++;
                                }
                            }
                        }
                            break;
                        case 'SE':	//**** Tipo SE: Pago Salud Empleado ****
                        {
                            $vnom = $_POST['devalores'][$y];
                            if ($vnom > 0) {
                                $ctacont1 = cuentascontablesccpet::cuentadebito_egreso($_POST['derecursos'][$y], $_POST['deccs'][$y], $_POST['fechafi'], $_POST['salud_empleado']);
                                $ccosto = $_POST['deccs'][$y];
                                $ncppto = nombrevariblespagonomina($_POST['tedet'][$y]);
                                $sqlr = "INSERT INTO comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque,valdebito,valcredito,estado,vigencia, numacti,cantarticulo) values ('17 $ideg','$ctacont1','" . $_POST['tercero'] . "','" . $_POST['deccs'][$y] . "','Pago Salud Funcionario','','$vnom','0','1','" . $_POST['vigencia'] . "','0','0')";
                                mysqli_query($linkbd, $sqlr);
                                if (trim($ctacont1) == '') {
                                    $estado_cuentas++;
                                }
                                //***************************
                                $valorp = $_POST['devalores'][$y];
                                $sqlr = "insert into comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque, valdebito,valcredito,estado,vigencia, numacti,cantarticulo) values ('17 $ideg','" . $_POST['banco'] . "','" . $_POST['tercero'] . "','" . $_POST['deccs'][$y] . "','Pago Banco Salud Funcionario', '" . $_POST['cheque'] . " " . $_POST['ntransfe'] . "','0','$vnom','1','" . $_POST['vigencia'] . "','0','0')";
                                mysqli_query($linkbd, $sqlr);
                                if (trim($_POST['banco']) == '') {
                                    $estado_cuentas++;
                                }
                            }
                        }
                            break;
                        case 'FS':	//**** Tipo PE: Pago Fondo Solidaridad ****
                        {
                            $vnom = $_POST['devalores'][$y];
                            if ($vnom > 0) {
                                $ctacont1 = cuentascontablesccpet::cuentadebito_egreso($_POST['derecursos'][$y], $_POST['deccs'][$y], $_POST['fechafi'], $_POST['pension_empleado']);
                                $ccosto = $_POST['deccs'][$y];
                                $ncppto = nombrevariblespagonomina($_POST['tedet'][$y]);
                                $sqlr = "INSERT INTO comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque,valdebito,valcredito,estado,vigencia, numacti,cantarticulo) values ('17 $ideg','$ctacont1','" . $_POST['tercero'] . "','" . $_POST['deccs'][$y] . "','Pago Fondo Solidaridad','','$vnom','0','1','" . $_POST['vigencia'] . "','0','0')";
                                mysqli_query($linkbd, $sqlr);
                                if (trim($ctacont1) == '') {
                                    $estado_cuentas++;
                                }
                                //***************************
                                $valorp = $_POST['devalores'][$y];
                                $sqlr = "insert into comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque, valdebito,valcredito,estado,vigencia, numacti,cantarticulo) values ('17 $ideg','" . $_POST['banco'] . "','" . $_POST['tercero'] . "','" . $_POST['deccs'][$y] . "','Pago Banco Fondo Solidaridad', '" . $_POST['cheque'] . " " . $_POST['ntransfe'] . "','0','$vnom','1','" . $_POST['vigencia'] . "','0','0')";
                                mysqli_query($linkbd, $sqlr);
                                if (trim($_POST['banco']) == '') {
                                    $estado_cuentas++;
                                }
                            }
                        }
                            break;
                        case 'PE':	//**** Tipo PE: Pago Pension Empleado ****
                        {
                            $vnom = $_POST['devalores'][$y];
                            if ($vnom > 0) {
                                $ctacont1 = cuentascontablesccpet::cuentadebito_egreso($_POST['derecursos'][$y], $_POST['deccs'][$y], $_POST['fechafi'], $_POST['pension_empleado']);
                                $ccosto = $_POST['deccs'][$y];
                                $ncppto = nombrevariblespagonomina($_POST['tedet'][$y]);
                                $sqlr = "INSERT INTO comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque,valdebito,valcredito,estado,vigencia, numacti,cantarticulo) values ('17 $ideg','$ctacont1','" . $_POST['tercero'] . "','" . $_POST['deccs'][$y] . "','Pago Pension Funcionario','','$vnom','0','1','" . $_POST['vigencia'] . "','0','0')";
                                mysqli_query($linkbd, $sqlr);
                                if (trim($ctacont1) == '') {
                                    $estado_cuentas++;
                                }
                                //***************************
                                $valorp = $_POST['devalores'][$y];
                                $sqlr = "insert into comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque, valdebito,valcredito,estado,vigencia, numacti,cantarticulo) values ('17 $ideg','" . $_POST['banco'] . "','" . $_POST['tercero'] . "','" . $_POST['deccs'][$y] . "','Pago Banco Pension Funcionario', '" . $_POST['cheque'] . " " . $_POST['ntransfe'] . "','0','$vnom','1','" . $_POST['vigencia'] . "','0','0')";
                                mysqli_query($linkbd, $sqlr);
                                if (trim($_POST['banco']) == '') {
                                    $estado_cuentas++;
                                }
                            }
                        }
                            break;
                        case 'PR':	//**** Tipo PR: Pago Pension Empresa ****
                        {
                            $vnom = 0;
                            $vnom = $_POST['devalores'][$y];
                            if ($vnom > 0) {
                                $ctacont1 = cuentascontablesccpet::cuentadebito_egreso($_POST['derecursos'][$y], $_POST['deccs'][$y], $_POST['fechafi'], $_POST['pension_empleador']);
                                $ccosto = $_POST['deccs'][$y];
                                $ncppto = nombrevariblespagonomina($_POST['tedet'][$y]);
                                $sqlr = "INSERT INTO comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque,valdebito,valcredito,estado,vigencia, numacti,cantarticulo) values ('17 $ideg','$ctacont1','" . $_POST['tercero'] . "','" . $_POST['deccs'][$y] . "','Pago Pension Empleador','','$vnom','0','1','" . $_POST['vigencia'] . "','0','0')";
                                mysqli_query($linkbd, $sqlr);
                                if (trim($ctacont1) == '') {
                                    $estado_cuentas++;
                                }
                                //***************************
                                $valorp = $_POST['devalores'][$y];
                                $sqlr = "insert into comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque, valdebito,valcredito,estado,vigencia, numacti,cantarticulo) values ('17 $ideg','" . $_POST['banco'] . "','" . $_POST['tercero'] . "','" . $_POST['deccs'][$y] . "','Pago Banco Pension Empleador', '" . $_POST['cheque'] . " " . $_POST['ntransfe'] . "','0','$vnom','1','" . $_POST['vigencia'] . "','0','0')";
                                mysqli_query($linkbd, $sqlr);
                                if (trim($_POST['banco']) == '') {
                                    $estado_cuentas++;
                                }
                            }
                        }
                            break;
                        case 'F':	//**** Tipo F: Pago Parafiscales ****
                        {
                            $vnom = 0;
                            $vnom = $_POST['devalores'][$y];
                            if ($vnom > 0) {
                                $ctacont1 = cuentascontablesccpet::cuentadebito_egreso($_POST['derecursos'][$y], $_POST['deccs'][$y], $_POST['fechafi'], '');
                                $ccosto = $_POST['deccs'][$y];
                                $ncppto = nombrevariblespagonomina($_POST['tedet'][$y]);
                                $sqlr = "INSERT INTO comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque,valdebito,valcredito,estado,vigencia, numacti,cantarticulo) values ('17 $ideg','$ctacont1','" . $_POST['tercero'] . "','" . $_POST['deccs'][$y] . "','Pago parafiscales $ncppto','','$vnom','0','1','" . $_POST['vigencia'] . "','0','0')";
                                mysqli_query($linkbd, $sqlr);
                                if (trim($ctacont1) == '') {
                                    $estado_cuentas++;
                                }
                                //***************************
                                $valorp = $_POST['devalores'][$y];
                                $sqlr = "insert into comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque, valdebito,valcredito,estado,vigencia, numacti,cantarticulo) values ('17 $ideg','" . $_POST['banco'] . "','" . $_POST['tercero'] . "','" . $_POST['deccs'][$y] . "','Pago Banco Parafiscales $ncppto', '" . $_POST['cheque'] . " " . $_POST['ntransfe'] . "','0','$vnom','1','" . $_POST['vigencia'] . "','0','0')";
                                mysqli_query($linkbd, $sqlr);
                                if (trim($_POST['banco']) == '') {
                                    $estado_cuentas++;
                                }
                            }

                        }
                            break;
                        case 'DS':	//***+ Tipo D: Pago Descuentos Empleado ****
                        {
                            $vnom = $_POST['devalores'][$y];
                            if ($vnom > 0) {
                                $sqlr = "SELECT descripcion, id_retencion, modo_pago, tipopago FROM humretenempleados WHERE id='" . $_POST['idedescuento'][$y] . "'";
                                $respr = mysqli_query($linkbd, $sqlr);
                                $rret = mysqli_fetch_row($respr);
                                if ($rret[2] == 'CSF') {
                                    $sqlrcu = "SELECT DISTINCT T1.nombre,T1.beneficiario,T2.cuenta FROM humvariablesretenciones T1, humvariablesretenciones_det T2 WHERE T1.codigo = '$rret[1]' AND T1.codigo = T2.codigo AND T2.credito = 'S' AND fechainicial=(SELECT MAX(T3.fechainicial) FROM humvariablesretenciones_det T3 WHERE T3.codigo=T2.codigo AND T3.credito = 'S' AND T3.fechainicial<='" . $_POST['fechafi'] . "')";
                                    $respcu = mysqli_query($linkbd, $sqlrcu);
                                    while ($rowcu = mysqli_fetch_row($respcu)) {
                                        $ctaconcepto = $rowcu[2];
                                        $docbenefi = $rowcu[1];
                                        $nomdescu = strtolower($rowcu[0]);
                                    }
                                    $ctabanco = $_POST['banco'];
                                    $desdecuento = "Pago Banco $rowdes[0] $nomdescu";
                                    $deschequetransfe = $_POST['cheque'] . " " . $_POST['ntransfe'];
                                } else {
                                    $sqlrcu = "SELECT nombre, cuentacontable FROM tesomediodepagossf WHERE id = '$rret[1]'";
                                    $respcu = mysqli_query($linkbd, $sqlrcu);
                                    $rowcu = mysqli_fetch_row($respcu);
                                    $ctabanco = $rowcu[1];
                                    $nomdescu = strtolower($rowcu[0]);
                                    //$ctabanco = cuentascontablesccpet::cuentadebito_egreso($_POST['derecursos'][$y],$_POST['deccs'][$y],$_POST['fechafi'],'');
                                    $sqlrcu = "SELECT DISTINCT T1.nombre,T1.beneficiario,T2.cuenta FROM humvariablesretenciones T1, humvariablesretenciones_det T2 WHERE T1.codigo = '01' AND T1.codigo = T2.codigo AND T2.credito = 'S' AND fechainicial=(SELECT MAX(T3.fechainicial) FROM humvariablesretenciones_det T3 WHERE T3.codigo=T2.codigo AND T3.credito = 'S' AND T3.fechainicial<='" . $_POST['fechafi'] . "')";
                                    $respcu = mysqli_query($linkbd, $sqlrcu);
                                    while ($rowcu = mysqli_fetch_row($respcu)) {
                                        $ctaconcepto = $rowcu[2];
                                    }
                                    $desdecuento = "Pago Sin SF";
                                    $deschequetransfe = '';
                                }
                                $ccosto = $_POST['deccs'][$y];
                                $ncppto = $rret[0];
                                $sqlr = "INSERT INTO comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque,valdebito,valcredito,estado,vigencia, numacti,cantarticulo) values ('17 $ideg','$ctaconcepto','" . $_POST['tercero'] . "','$ccosto','Pago $rowdes[0] $nomdescu','','$vnom','0','1','" . $_POST['vigencia'] . "','0','0')";
                                mysqli_query($linkbd, $sqlr);

                                $sqlr = "INSERT INTO comprobante_det (id_comp, cuenta, tercero, centrocosto, detalle, cheque, valdebito, valcredito, estado, vigencia, numacti, cantarticulo) VALUES ('17 $ideg', '$ctabanco', '" . $_POST['tercero'] . "', '$ccosto', '$desdecuento', '$deschequetransfe', '0', '$vnom', '1', '" . $_POST['vigencia'] . "','0','0')";

                                mysqli_query($linkbd, $sqlr);
                                if (trim($ctaconcepto) == '') {
                                    $estado_cuentas++;
                                }
                                //***************************
                            }
                        }
                            break;
                        default:	//**** Tipo : Pago Salarios y otros pagos ****
                        {
                            $vnom = 0;
                            $vnom = $_POST['devalores'][$y];
                            if ($vnom > 0) {
                                $ctacont1 = cuentascontablesccpet::cuentadebito_egreso($_POST['derecursos'][$y], $_POST['deccs'][$y], $_POST['fechafi'], '');
                                $ccosto = $_POST['deccs'][$y];
                                $ncppto = nombrevariblespagonomina($_POST['tedet'][$y]);
                                $sqlr = "INSERT INTO comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque,valdebito,valcredito,estado,vigencia, numacti,cantarticulo) values ('17 $ideg','$ctacont1','" . $_POST['tercero'] . "','" . $_POST['deccs'][$y] . "','Pago $rowdes[0] $ncppto','','$vnom','0','1','" . $_POST['vigencia'] . "','0','0')";
                                mysqli_query($linkbd, $sqlr);
                                if (trim($ctacont1) == '') {
                                    $estado_cuentas++;
                                }
                                //***************************
                                if ($_POST['tipop'] != 'caja') {
                                    $valorp = $_POST['devalores'][$y];
                                    $sqlr = "insert into comprobante_det (id_comp, cuenta, tercero, centrocosto, detalle, cheque, valdebito, valcredito, estado, vigencia, numacti, cantarticulo) values ('17 $ideg', '" . $_POST['banco'] . "', '" . $_POST['tercero'] . "', '" . $_POST['deccs'][$y] . "', 'Pago Banco $rowdes[0] $ncppto', '" . $_POST['cheque'] . " " . $_POST['ntransfe'] . "', '0', '$vnom', '1', '" . $_POST['vigencia'] . "', '0', '0')";
                                    mysqli_query($linkbd, $sqlr);
                                    if (trim($_POST['banco']) == '') {
                                        $estado_cuentas++;
                                    }
                                } else {
                                    $sqlr = "insert into comprobante_det (id_comp, cuenta, tercero, centrocosto, detalle, cheque, valdebito, valcredito, estado, vigencia, numacti, cantarticulo) values ('17 $ideg', '" . $_POST['cb'] . "', '" . $_POST['tercero'] . "', '" . $_POST['deccs'][$y] . "', 'Pago Caja " . $_POST['tcta'] . "', '', '0', '$vnom', '1', '" . $_POST['vigencia'] . "', '0', '0')";
                                    mysqli_query($linkbd, $sqlr);
                                    if (trim($_POST['cb']) == '') {
                                        $estado_cuentas++;
                                    }
                                }
                            }
                        }
                    }
                }
                if ($estado_cuentas == 0) {
                    echo "
							<script>
								Swal.fire({
									icon: 'success',
									title: 'Se reflejo con exito el egreso de nomina N°: $ideg',
									confirmButtonText: 'Continuar',
									confirmButtonColor: '#FF121A',
									timer: 3000
								});
							</script>";
                } else {
                    if ($estado_cuentas == 1) {
                        $deserror = "Se reflejo elegreso de nomina N°: $ideg, con error en una cuenta";
                    } else {
                        $deserror = "Se reflejo el egreso de nomina N°: $ideg, con error en $estado_cuentas cuentas";
                    }
                    echo "
							<script>
								Swal.fire({
									icon: 'success',
									title: '$deserror',
									confirmButtonText: 'Continuar',
									confirmButtonColor: '#FF121A',
									timer: 3000
								});
							</script>";
                }
            } else {
                echo "
							<script>
								Swal.fire({
									icon: 'success',
									title: 'No se puede almacenar, no existe un egreso para orden orden N°: $ideg',
									confirmButtonText: 'Continuar',
									confirmButtonColor: '#FF121A',
									timer: 3000
								});
							</script>";
            }
        } else {
            echo "
					<script>
						Swal.fire({
							icon: 'error',
							title: 'Error!',
							text: 'No Tiene los Permisos para Modificar este Documento',
							confirmButtonText: 'Continuar',
							confirmButtonColor: '#FF121A',
							timer: 3000
						});
					</script>";
        }
    }//************ FIN DE IF OCULTO************
    //*********** cuenta origen va al credito y la destino al debito
    if ($_GET['idegre'] != "") {
        echo "<script>document.getElementById('codrec').value=" . $_GET['idegre'] . ";</script>";
    }
    if (!$_POST['oculto']) {
        $sqlr = "SELECT * FROM tesoegresosnomina WHERE fecha >= '2021-01-01' ORDER BY id_EGRESO DESC";
        $res = mysqli_query($linkbd, $sqlr);
        $r = mysqli_fetch_row($res);
        $_POST['maximo'] = $r[0];
        if ($_POST['codrec'] != "" || $_GET['idegre'] != "") {
            if ($_POST['codrec'] != "") {
                $sqlr = "SELECT * FROM tesoegresosnomina WHERE id_EGRESO = '" . $_POST['codrec'] . "' AND fecha >= '2021-01-01'";
            } else {
                $sqlr = "SELECT * FROM tesoegresosnomina WHERE id_EGRESO = '" . $_GET['idegre'] . "' AND fecha >= '2021-01-01'";
            }
        } else {
            $sqlr = "SELECT * FROM tesoegresosnomina WHERE fecha >= '2021-01-01' ORDER BY id_EGRESO DESC";
        }
        $res = mysqli_query($linkbd, $sqlr);
        $r = mysqli_fetch_row($res);
        $_POST['ncomp'] = $r[0];
        $_POST['egreso'] = $r[0];
    }
    if ($_POST['oculto'] == '1' || !$_POST['oculto']) {
        $sqlr = "SELECT * FROM tesoegresosnomina WHERE id_egreso='" . $_POST['egreso'] . "' AND fecha >= '2021-01-01'";
        $res = mysqli_query($linkbd, $sqlr);
        $consec = 0;
        while ($r = mysqli_fetch_row($res)) {
            $consec = $r[0];
            $_POST['orden'] = $r[2];
            $_POST['estado'] = $r[13];
            if ($r[13] == 'S') {
                $_POST['estadoc'] = 1;
            }
            if ($r[13] == 'N') {
                $_POST['estadoc'] = 0;
            }
            $_POST['tipop'] = $r[14];
            if ($r[15] == 'CSF') {
                if ($_POST['tipop'] == 'caja') {
                    $sqlcuenta = "SELECT nombre, cuentacontable FROM tesomediodepago WHERE id = '$r[12]'";
                    $rescuenta = mysqli_query($linkbd, $sqlcuenta);
                    $rowcuenta = mysqli_fetch_row($rescuenta);
                    $_POST['tcta'] = $rowcuenta[1] . ' - ' . $rowcuenta[0];
                    $_POST['cb'] = $rowcuenta[1];
                } else {
                    $_POST['banco'] = $r[9];
                    $_POST['ncheque'] = $r[10];
                    $_POST['cb'] = $r[12];
                    $_POST['transferencia'] = $r[12];
                    $_POST['ntransfe'] = $r[10];
                }

            } else {
                $sqlcuenta = "SELECT nombre, cuentacontable FROM tesomediodepagossf WHERE id = '$r[12]'";
                $rescuenta = mysqli_query($linkbd, $sqlcuenta);
                $rowcuenta = mysqli_fetch_row($rescuenta);
                $_POST['tcta'] = $rowcuenta[1] . ' - ' . $rowcuenta[0];
                $_POST['cb'] = $rowcuenta[1];
            }
            $_POST['fecha'] = $r[3];
            $_POST['fechafi'] = $r[3];
            $_POST['fechafl'] = $r[3];
            $_POST['modopago'] = $r[15];
        }
        preg_match("/([0-9]{4})\-([0-9]{2})\-([0-9]{2})/", $_POST['fecha'], $fecha);
        $fechaf = "$fecha[3]/$fecha[2]/$fecha[1]";
        $_POST['fecha'] = $fechaf;
        $_POST['vigencia'] = $fecha[1];
        $_POST['egreso'] = $consec;
    }
    ?>
    <form name="form2" method="post" action="">
        <input type="hidden" name="anio[]" id="anio[]" value="<?php echo $_POST['anio'] ?>" />
        <input type="hidden" name="anioact" id="anioact" value="<?php echo $_POST['anioact'] ?>" />
        <input type="hidden" name="bloqueo[]" id="bloqueo[]" value="<?php echo $_POST['bloqueo'] ?>" />
        <input type="hidden" id="cajacomp" name="cajacomp" value="<?php echo $_POST['cajacomp'] ?>" />
        <input type="hidden" id="codrec" name="codrec" value="<?php echo $_POST['codrec'] ?>" />
        <input type="hidden" id="icbf" name="icbf" value="<?php echo $_POST['icbf'] ?>" />
        <input type="hidden" id="sena" name="sena" value="<?php echo $_POST['sena'] ?>" />
        <input type="hidden" id="esap" name="esap" value="<?php echo $_POST['esap'] ?>" />
        <input type="hidden" id="iti" name="iti" value="<?php echo $_POST['iti'] ?>" />
        <input type="hidden" id="arp" name="arp" value="<?php echo $_POST['arp'] ?>" />
        <input type="hidden" id="salud_empleador" name="salud_empleador"
            value="<?php echo $_POST['salud_empleador'] ?>" />
        <input type="hidden" id="salud_empleado" name="salud_empleado" value="<?php echo $_POST['salud_empleado'] ?>" />
        <input type="hidden" id="transp" name="transp" value="<?php echo $_POST['transp'] ?>" />
        <input type="hidden" id="pension_empleador" name="pension_empleador"
            value="<?php echo $_POST['pension_empleador'] ?>" />
        <input type="hidden" id="pension_empleado" name="pension_empleado"
            value="<?php echo $_POST['pension_empleado'] ?>" />
        <input type="hidden" id="salmin" name="salmin" value="<?php echo $_POST['salmin'] ?>" />
        <input type="hidden" id="auxalim" name="auxalim" value="<?php echo $_POST['auxalim'] ?>" />
        <input type="hidden" id="auxtran" name="auxtran" value="<?php echo $_POST['auxtran'] ?>" />
        <?php
        if ($_POST['orden'] != '') {
            //*** busca detalle cdp
            $sqlr = "SELECT * FROM tesoegresosnomina WHERE id_egreso='" . $_POST['ncomp'] . "' AND fecha >= '2021-01-01'";
            $resp = mysqli_query($linkbd, $sqlr);
            $row = mysqli_fetch_row($resp);
            $_POST['concepto'] = $row[8];
            $_POST['tercero'] = $row[11];
            $_POST['ntercero'] = buscatercero($_POST['tercero']);
            $_POST['valororden'] = $row[7];
            $_POST['retenciones'] = 0;
            $_POST['totaldes'] = number_format($_POST['retenciones'], 2);
            $_POST['valorpagar'] = $_POST['valororden'] - $_POST['retenciones'];
            $_POST['valpalgar'] = $_POST['valororden'] - $_POST['retenciones'];
            $_POST['bop'] = "";
        } else {
            $_POST['cdp'] = "";
            $_POST['detallecdp'] = "";
            $_POST['tercero'] = "";
            $_POST['ntercero'] = "";
            $_POST['bop'] = "";
        }
        ?>
        <input type="hidden" id="valpalgar" name="valpalgar" value="<?php echo $_POST['valpalgar'] ?>">
        <table class="inicio" align="center">
            <tr>
                <td colspan="8" class="titulos">Comprobante de Egreso Nomina</td>
                <td class="cerrar" style="width:7%;"><a onClick="location.href='cont-principal.php'">&nbsp;Cerrar</a>
                </td>
            </tr>
            <tr>
                <td style="width:3.5cm;" class="saludo1">No Egreso:</td>
                <td style="width:10%;">
                    <a href="#" onClick="atrasc()"><img src="imagenes/back.png" alt="anterior" align="absmiddle"></a>
                    <input name="egreso" type="text" value="<?php echo $_POST['egreso'] ?>" style="width:50%"
                        onKeyUp="return tabular(event,this)" onBlur="validar2()">
                    <input name="ncomp" type="hidden" value="<?php echo $_POST['ncomp'] ?>">
                    <a href="#" onClick="adelante()"><img src="imagenes/next.png" alt="siguiente" align="absmiddle"></a>
                    <input type="hidden" value="a" name="atras">
                    <input type="hidden" value="s" name="siguiente">
                    <input type="hidden" value="<?php echo $_POST['maximo'] ?>" name="maximo">
                </td>
                <td style="width:2.5cm;" class="saludo1">Fecha: </td>
                <td style="width:10%;"><input id="fc_1198971545" name="fecha" type="text"
                        value="<?php echo $_POST['fecha'] ?>" maxlength="10" style="width:100%;"
                        onKeyUp="return tabular(event,this)" readonly></td>
                <input type="hidden" name="fechafi" id="fechafi" value="<?php echo $_POST['fechafi']; ?>" />
                <td style="width:3.5cm;" class="saludo1">Modo de Pago:</td>
                <td style="width:10%;">
                    <select name="modopago" onChange="validar();" style="width:100%">
                        <?php
                        if ($_POST['modopago'] == 'CSF') {
                            echo '<option value="CSF" selected>Con SF</option>';
                        } else {
                            echo '<option value="SSF" selected>Sin SF</option>';
                        }
                        ?>
                    </select>
                </td>
                <input type="hidden" name="fechafl" id="fechafl" value="<?php echo $_POST['fechafl'] ?>" />
                <td style="width:3.5cm;" class="saludo1">Estado:</td>
                <td style="width:15%;">
                    <?php
                    if ($_POST['estado'] == "S") {
                        $valuees = "ACTIVO";
                        $stylest = "width:68%; background-color:#0CD02A; color:white; text-align:center;";
                    } else if ($_POST['estado'] == "N") {
                        $valuees = "ANULADO";
                        $stylest = "width:68%; background-color:#FF0000; color:white; text-align:center;";
                    } else if ($_POST['estado'] == "P") {
                        $valuees = "PAGO";
                        $stylest = "width:68%; background-color:#0404B4; color:white; text-align:center;";
                    }
                    echo "<input type='text' name='estado' id='estado' value='$valuees' style='$stylest' readonly />";
                    ?>
                    <input name="estadoc" type="hidden" value="<?php echo $_POST['estadoc'] ?>" size="5" readonly>
                    <input name="vigencia" type="text" value="<?php echo $_POST['vigencia'] ?>" size="4" readonly>
                </td>
            </tr>
            <tr>
                <td style="width:3.5cm" class="saludo1">No Orden Pago:</td>
                <td style="width:10%"><input name="orden" type="text" value="<?php echo $_POST['orden'] ?>"
                        style="width:62%" onKeyUp="return tabular(event,this)" onBlur="buscaop(event)" readonly></td>
                <input type="hidden" value="0" name="bop">
                <td class="saludo1">Tercero:</td>
                <td><input id="tercero" type="text" name="tercero" style="width:100%"
                        onKeyUp="return tabular(event,this)" onBlur="buscater(event)"
                        value="<?php echo $_POST['tercero'] ?>" readonly></td>
                <td colspan="4"><input name="ntercero" type="text" value="<?php echo $_POST['ntercero'] ?>"
                        style="width:100%" readonly></td>
            </tr>
            <tr>
                <td class="saludo1">Concepto:</td>
                <td colspan="5"><input name="concepto" type="text" value="<?php echo $_POST['concepto'] ?>"
                        style="width:100%" readonly></td>
                <td style="width:3.5cm;" class="saludo1">Forma de Pago:</td>
                <td style="width:10%;">
                    <select name="tipop" onChange="validar();"="return tabular(event,this)" style="width:100%">
                        <?php
                        if ($_POST['tipop'] == 'cheque') {
                            echo '<option value="cheque" selected>Cheque</option>';
                        } else if ($_POST['tipop'] == 'transferencia') {
                            echo '<option value="transferencia" selected>Transferencia</option>';
                        } else {
                            echo '<option value="caja" selected>Caja</option>';
                        }
                        ?>
                    </select>
                </td>
            </tr>
            <?php
            if ($_POST['modopago'] == 'CSF') {
                //**** if del cheques
                if ($_POST['tipop'] == 'cheque') {
                    echo "
								<tr>
									<td class='saludo1'>Cuenta Bancaria:</td>
									<td colspan='5'>
										<select id='banco' name='banco' onChange='validar()' onKeyUp='return tabular(event,this)'>";
                    $sqlr = "SELECT tesobancosctas.estado, tesobancosctas.cuenta, tesobancosctas.ncuentaban, tesobancosctas.tipo, terceros.razonsocial, tesobancosctas.tercero FROM tesobancosctas, terceros WHERE tesobancosctas.tercero = terceros.cedulanit AND tesobancosctas.estado = 'S' AND tesobancosctas.tipo = 'Corriente'";
                    $res = mysqli_query($linkbd, $sqlr);
                    while ($row = mysqli_fetch_row($res)) {
                        if ($row[1] == $_POST['banco']) {
                            echo "<option value = $row[1] SELECTED>$row[2] - Cuenta $row[3] - $row[4]</option>";
                            $_POST['nbanco'] = $row[4];
                            $_POST['ter'] = $row[5];
                            $_POST['cb'] = $row[2];
                            $_POST['tcta'] = $row[3];
                        }
                    }
                    echo "
										</select>
										<input type='hidden' name='tcta' value='" . $_POST['tcta'] . "'/>
										<input type='hidden' name='cb' value='" . $_POST['cb'] . "'/>
										<input type='hidden' name='ter' id='ter' value='" . $_POST['ter'] . "' >
										<input type='hidden' name='nbanco' id='nbanco' value='" . $_POST['nbanco'] . "' size='50' readonly/>
									</td>
									<td class='saludo1'>Cheque:</td>
									<td><input type='text' name='ncheque' id='ncheque' value='" . $_POST['ncheque'] . "' size='20' readonly/></td>
								</tr>";
                }
                //**** if del transferencias
                if ($_POST['tipop'] == 'transferencia') {
                    echo "
								<tr>
									<td class='saludo1'>Cuenta Bancaria:</td>
									<td colspan='3'>
										<select id='banco' name='banco' onChange='validar()' onKeyUp='return tabular(event,this)'>";
                    $sqlr = "SELECT tesobancosctas.estado,tesobancosctas.cuenta,tesobancosctas.ncuentaban,tesobancosctas.tipo, terceros.razonsocial,tesobancosctas.tercero FROM tesobancosctas,terceros WHERE tesobancosctas.tercero=terceros.cedulanit AND tesobancosctas.estado='S' ";
                    $res = mysqli_query($linkbd, $sqlr);
                    while ($row = mysqli_fetch_row($res)) {
                        if ($row[1] == $_POST['banco']) {
                            echo "<option value='$row[1]' SELECTED>$row[2] - Cuenta $row[3] - $row[4]</option>";
                            $_POST['nbanco'] = $row[4];
                            $_POST['ter'] = $row[5];
                            $_POST['cb'] = $row[2];
                            $_POST['tcta'] = $row[3];
                        }
                    }
                    echo "
									</select>
										<input type='hidden' name='tcta' value='" . $_POST['tcta'] . "'/>
										<input type='hidden 'name='cb' value='" . $_POST['cb'] . "'/>
										<input type='hidden' id='ter' name='ter' value='" . $_POST['ter'] . "'/>
										<input type='hidden' id='nbanco' name='nbanco' value='" . $_POST['nbanco'] . "'/>
									</td>
									<td class='saludo1'>No Transferencia:</td>
									<td><input type='text' id='ntransfe' name='ntransfe' value='" . $_POST['ntransfe'] . "' style='width:100%' readonly/></td>
								</tr>";
                }
                if ($_POST['tipop'] == 'caja') {
                    echo "
								<tr>
									<td class='saludo1'>Cuenta:</td>
									<td colspan='5'>
										<input type='text' name='tcta' value='" . $_POST['tcta'] . "'  style='width:100%;'/>
										<input type='hidden' name='cb' value='" . $_POST['cb'] . "'/>
									</td>
								</tr>";
                }
            } else {
                echo "
							<tr>
								<td class='saludo1'>Cuenta:</td>
								<td colspan='5'>
									<input type='text' name='tcta' value='" . $_POST['tcta'] . "'  style='width:100%;'/>
									<input type='hidden' name='cb' value='" . $_POST['cb'] . "'/>
								</td>
							</tr>";
            }
            ?>
            <tr>
                <td class="saludo1">Valor Orden:</td>
                <td><input type="text" name="valororden" id="valororden" onKeyUp="return tabular(event,this)"
                        value="<?php echo number_format($_POST['valororden'], 2, ',', '.') ?>"
                        style="width:100%; text-align:right;" readonly /></td>
                <td class="saludo1">Retenciones:</td>
                <td><input name="retenciones" type="text" id="retenciones" onKeyUp="return tabular(event,this)"
                        value="<?php echo number_format($_POST['retenciones'], 2, ',', '.') ?>"
                        style="width:100%; text-align:right;" readonly /></td>
                <td class="saludo1">Valor a Pagar:</td>
                <td><input name="valorpagar" type="text" id="valorpagar" onKeyUp="return tabular(event,this)"
                        value="<?php echo number_format($_POST['valorpagar'], 2, ',', '.') ?>"
                        style="width:100%; text-align:right;" readonly /></td>
                <input type="hidden" name="oculto" value="1" />
            </tr>
        </table>
        <div class="subpantallac4" style="width:100%; height:55%">
            <table class="inicio">
                <tr>
                    <td colspan="7" class="titulos">Detalle Egreso Nomina</td>
                </tr>
                <tr class="titulos2">
                    <td>No</td>
                    <td>Nit</td>
                    <td>Tercero</td>
                    <td>Detalle</td>
                    <td>CC</td>
                    <td>Cta Presupuestal</td>
                    <td>Valor</td>
                </tr>
                <?php
                if ($_POST['elimina'] != '') {
                    $posi = $_POST['elimina'];
                    unset($_POST['dccs'][$posi]);
                    unset($_POST['dvalores'][$posi]);
                    $_POST['dccs'] = array_values($_POST['dccs']);
                }
                if ($_POST['agregadet'] == '1') {
                    $_POST['dccs'][] = $_POST['cc'];
                    $_POST['agregadet'] = '0';
                    echo "
								<script>
									document.form2.banco.value='';
									document.form2.nbanco.value='';
									document.form2.banco2.value='';
									document.form2.nbanco2.value='';
									document.form2.cb.value='';
									document.form2.cb2.value=';
									document.form2.valor.value='';
									document.form2.numero.value='';
									document.form2.agregadet.value='0';
									document.form2.numero.select();
									document.form2.numero.focus();
								</script>";
                }
                $_POST['totalc'] = 0;
                /*$sqlrul="SELECT * FROM tesoegresosnomina_det WHERE id_egreso='".$_POST['egreso']."' AND estado='S' AND (tipo='F' OR tipo='DS')";
                                     $respul = mysqli_query($linkbd,$sqlrul);
                                     //if (mysqli_num_rows($respul)>0){
                                         $sqlr="SELECT * FROM tesoegresosnomina_det WHERE id_egreso='".$_POST['egreso']."' AND estado='S'";
                                     }else{*/
                $sqlr = "SELECT id_det, id_egreso, id_orden, tipo, tercero, ntercero_det, cuentap, cc, valor, estado, ndes, valordevengado, medio_pago FROM tesoegresosnomina_det WHERE id_egreso='" . $_POST['egreso'] . "' AND estado='S' ";
                //}
                $dcuentas[] = array();
                $dncuentas[] = array();
                $resp2 = mysqli_query($linkbd, $sqlr);
                $iter = 'saludo1a';
                $iter2 = 'saludo2';
                while ($row2 = mysqli_fetch_row($resp2)) {
                    $vauxali = $vauxtra = 0;
                    $mediopagos = '';
                    if ($row2[3] == '01') {
                        $sqlrnom = "SELECT auxalim, auxtran FROM humnomina_det WHERE id_nom='" . $_POST['orden'] . "' AND cedulanit='$row2[4]' AND tipopago='01'";
                        $resn = mysqli_query($linkbd, $sqlrnom);
                        $rown = mysqli_fetch_row($resn);
                        $vauxali = $rown[0];
                        $vauxtra = $rown[1];
                        $valorneto = $row2[8] - $vauxali - $vauxtra;
                        $mediopagos = $row2[12];
                    } else {
                        $valorneto = $row2[8];
                        $mediopagos = $row2[12];
                    }
                    $nombre = buscacuentapres($row2[6], 2);
                    $tercero = buscatercero($row2[4]);
                    if ($row2[10] == '') {
                        switch ($row2[3]) {
                            case 'SR':
                                $nomdetalle = 'Pago Salud Empresa';
                                break;
                            case 'SE':
                                $nomdetalle = 'Pago Salud Empleado';
                                break;
                            case 'PR':
                                $nomdetalle = 'Pago Pension Empresa';
                                break;
                            case 'PE':
                                $nomdetalle = 'Pago Pension Empleado';
                                break;
                            case 'FS':
                                $nomdetalle = 'Pago Fondo Solidaridad';
                                break;
                            default:
                                $sqlrdes = "SELECT nombre FROM ccpethumvariables WHERE estado='S' AND codigo='$row2[3]'";
                                $resdes = mysqli_query($linkbd, $sqlrdes);
                                $rowdes = mysqli_fetch_row($resdes);
                                $nomdetalle = $rowdes[0];
                        }
                    } else if ($row2[3] == 'F') {
                        $sqlrdes = "SELECT nombre FROM humparafiscalesccpet WHERE estado='S' AND codigo='$row2[10]'";
                        $resdes = mysqli_query($linkbd, $sqlrdes);
                        $rowdes = mysqli_fetch_row($resdes);
                        $nomdetalle = $rowdes[0];
                    } else {

                    }
                    echo "
								<tr class='$iter'>
									<input type='hidden' name='tedet[]' value='$row2[3]'/>
									<input type='hidden' name='ttipo[]' value='$row2[10]'/>
									<input type='hidden' name='decuentas[]' value='$row2[4]'/>
									<input type='hidden' name='idedescuento[]' value='$row2[10]'/>
									<input type='hidden' name='deccs[]' value='$row2[7]'/>
									<input type='hidden' name='derecursos[]' value='$row2[6]'/>
									<input type='hidden' name='devalores[]' value='$valorneto'/>
									<input type='hidden' name='mediopago[]' value='$mediopagos'/>
									<td>$row2[3] - $row2[10]</td>
									<td>$row2[4]</td>
									<td>$tercero </td>
									<td>$nomdetalle</td>
									<td>$row2[7]</td>
									<td>$row2[6]</td>
									<td style='text-align:right;'>$" . number_format($valorneto, 2, ',', '.') . "</td>
								</tr>";
                    $aux = $iter;
                    $iter = $iter2;
                    $iter2 = $aux;
                    $_POST['totalc'] = $_POST['totalc'] + $valorneto;
                    if ($vauxali != 0) {
                        $sqlrdes = "SELECT nombre FROM ccpethumvariables WHERE estado='S' AND codigo='07'";
                        $resdes = mysqli_query($linkbd, $sqlrdes);
                        $rowdes = mysqli_fetch_row($resdes);
                        $nomdetalle = $rowdes[0];
                        $sqlrcp = "SELECT cuentapres FROM ccpethumvariables_det WHERE codigo='07' AND cc='$row2[7]' AND estado='S' AND vigencia='" . $_POST['vigencia'] . "'";
                        $rescp = mysqli_query($linkbd, $sqlrcp);
                        $rcp = mysqli_fetch_row($rescp);
                        echo "
									<tr class='$iter'>
										<input type='hidden' name='tedet[]' value='07'/>
										<input type='hidden' name='ttipo[]' value='$row2[10]'/>
										<input type='hidden' name='decuentas[]' value='$row2[4]'/>
										<input type='hidden' name='idedescuento[]' value='$row2[10]'/>
										<input type='hidden' name='deccs[]' value='$row2[7]'/>
										<input type='hidden' name='derecursos[]' value='$rcp[0]'/>
										<input type='hidden' name='devalores[]' value='$vauxali'/>
										<td>07 - $row2[10]</td>
										<td>$row2[4]</td>
										<td>$tercero</td>
										<td>$nomdetalle</td>
										<td>$row2[7]</td>
										<td>$rcp[0]</td>
										<td style='text-align:right;'>$" . number_format($vauxali, 2, ',', '.') . "</td>
									</tr>";
                        $aux = $iter;
                        $iter = $iter2;
                        $iter2 = $aux;
                        $_POST['totalc'] = $_POST['totalc'] + $vauxali;
                    }
                    $_POST['totalcf'] = number_format($_POST['totalc'], 2, ".", ",");
                }
                $resultado = convertir($_POST['valorpagar']);
                $_POST['letras'] = $resultado . " PESOS M/CTE";
                echo "<tr class='titulos2'>
			<td colspan='5'></td>
			<td>Total</td>
			<td align='right'>
				<input name='totalcf' type='hidden' value='" . $_POST['totalcf'] . "'>" . number_format($_POST['totalc'], 2, ',', '.') . "
				<input name='totalc' type='hidden' value='" . $_POST['totalc'] . "'>
			</td>
		</tr>
		<tr class='titulos2'>
			<td >Son:</td>
			<td colspan='6'>
				<input name='letras' type='hidden' value='" . $_POST['letras'] . "'>" . $_POST['letras'] . "
			</td>
		</tr>";
                ?>
            </table>
        </div>
    </form>
</body>

</html>
