<?php

    header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require 'comun.inc';
	require 'funciones.inc';
	session_start();
    if(empty($_SESSION)){
        header("location: index.php");
    }
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html lang=es>
	<head>
    <meta charset="utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1"/>
		<meta name="viewport" content="user-scalable=no">
		<title>:: IDEAL 10 - Informes</title>
		<link href="favicon.ico" rel="shortcut icon"/>
		<link href="css/css2z.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
        <link href="css/tabs.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
        <link href="css/style.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" href="sweetalert2/dist/sweetalert2.min.css">
        <script type="text/javascript" src="css/programas.js"></script>
        <script src="sweetalert2/dist/sweetalert2.min.js"></script>
		<script src="vue/vue.min.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script src="css/calendario.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
	</head>
	<body>
		<header>
			<table>
				<tr><script>barra_imagenes("info");</script><?php cuadro_titulos();?></tr>
			</table>
		</header>

		<form name="form2" method="post" action="">
			<section id="myapp" v-cloak >
                <div id="cargando" v-show="isLoading" class="loading" style="z-index: 10000;">
                    <span>Cargando...</span>
                </div>
				<nav>
					<table>
						<tr><?php menu_desplegable("info");?></tr>
                        <tr>
							<td colspan="3" class="cinta">
                                <img src="imagenes/add.png"  @click="window.location.href='cont-parametrosExogenaCrear.php'" class="mgbt" title="Nuevo">
								<img @click="save()" src="imagenes/guarda.png"   title="Guardar"  class="mgbt">
								<img src="imagenes/busca.png" @click="window.location.href='cont-parametrosExogenaBuscar.php'"   class="mgbt" title="Buscar">
								<img src="imagenes/nv.png" @click="mypop=window.open('info-principal.php','',''); mypop.focus();" class="mgbt" title="Nueva Ventana">
                                <a href="cont-parametrosExogenaBuscar.php"  class="mgbt"><img src="imagenes/iratras.png" title="Atr&aacute;s"></a>
							</td>
						</tr>
					</table>
				</nav>
				<article>
                    <!--TABS-->
                    <h2 class="titulos m-1">.: Editar Configuración de formatos exógena</h2>
                    <div ref="rTabs" class="nav-tabs inicio">
                        <div class="nav-item active" @click="showTab(1)">Formato</div>
                        <div class="nav-item" @click="showTab(2)">Conceptos</div>
                    </div>
                    <!--CONTENIDO TABS-->
                    <div ref="rTabsContent" class="nav-tabs-content">
                        <div class="nav-content active">
                            <div class="inicio">
                                <div class="form-control w-50">
                                    <label for="form-label">.: Formato <span class="text-danger fw-bolder">*</span></label>
                                    <select v-model="selectFormato" @change="getConceptos()">
                                        <option selected value="0">Seleccione</option>
                                        <option v-for="(data,index) in arrFormatos" :key="index" :value="data.formato">
                                            {{ data.formato+" - "+data.nombre}}
                                        </option>

                                    </select>
                                </div>
                                <div class="d-flex">
                                    <div class="w-100">
                                        <h2 class="titulos m-1">.: Columnas fijas</h2>
                                        <div class="overflow-auto max-vh-50 overflow-x-hidden p-2" >
                                            <table class="table fw-normal">
                                                <thead>
                                                    <tr>
                                                        <th>Nombre</th>
                                                        <th>Visible</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr v-for="(data,index) in arrColumnasFijas" :key="index">
                                                        <td>{{ data.nombre}}</td>
                                                        <td><input type="checkbox" v-model="data.check"></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <div class="w-100">
                                        <h2 class="titulos m-1">.: Columnas calculadas</h2>
                                        <div class="overflow-auto max-vh-50 overflow-x-hidden p-2" >
                                            <table class="table fw-normal">
                                                <thead>
                                                    <tr>
                                                        <th>Nombre</th>
                                                        <th>Valor límite</th>
                                                        <th>Visible</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr v-for="(data,index) in arrColumnasCalculadas" :key="index">
                                                        <td>{{ data.nombre}}</td>
                                                        <td><input type="number" v-model="data.valor"></td>
                                                        <td><input type="checkbox" v-model="data.check" @change="getConceptos"></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="nav-content">
                            <div class="inicio">
                                <div class="overflow-auto max-vh-50 overflow-x-hidden p-2" >
                                    <table class="table fw-normal">
                                        <thead>
                                            <tr>
                                                <th>Concepto</th>
                                                <th>Nombre</th>
                                                <th>Cuenta</th>
                                                <th>Nombre cuenta</th>
                                                <th>Columna calculada</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr v-for="(data,i) in arrConceptosFormato" :key="i">
                                                <td>{{ data.codigo_concepto}}</td>
                                                <td>{{ data.nombre_concepto}}</td>
                                                <td>{{ data.cuenta}}</td>
                                                <td>{{ data.nombre}}</td>
                                                <td>
                                                    <select v-model="data.selected_col">
                                                        <option selected value="0">Seleccione</option>
                                                        <option v-for="(col,j) in data.columnas" :key="j" :value="col.id">
                                                            {{col.nombre}}
                                                        </option>
                                                    </select>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
				</article>
			</section>
		</form>

		<script src="node_modules/read-excel-file/bundle/read-excel-file.min.js"></script>
		<script src="node_modules/xlsx/dist/xlsx.full.min.js"></script>
        <script src="Librerias/vue/vue.min.js"></script>
		<script src="Librerias/vue/axios.min.js"></script>
		<script type="module" src="contabilidad_vue/exogena_parametros/editar/cont-parametrosEditar.js?<?= date('d_m_Y_h_i_s');?>"></script>

	</body>
</html>
