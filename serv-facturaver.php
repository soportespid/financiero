<?php
	header("Cache-control: private"); // Arregla IE 6
	header("Content-Type: text/html;charset=iso-8859-1");
	require "comun.inc";
	require "funciones.inc";
	require "conversor.php";
	require "validaciones.inc";
	require 'funcionessp.inc';
	session_start();
	$linkbd=conectar_bd();	
	cargarcodigopag($_GET[codpag],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es">
	<head>
		<meta http-equiv="Content-type" content="text/html; charset=iso-8859-1"/>
		<meta http-equiv="X-UA-Compatible" content="IE=9"/>
		<title>:: SPID - Servicios Publicos</title>
		<link href="css/css2.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/tabs.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<script type="text/javascript" src="css/calendario.js"></script>
		<script type="text/javascript" src="css/programas.js"></script>
		<script>
			function validar(){document.form2.submit();}
			function pdf()
			{
				
			}
			function buscaprediales()
 			{
				document.form2.oculto.value='1';
				document.form2.submit();
 			}
			function buscavigencias(objeto)
 			{
				vvigencias=document.getElementsByName('dselvigencias[]');
				vtotalpred=document.getElementsByName("dpredial[]"); 	
				vtotaliqui=document.getElementsByName("dhavaluos[]"); 	
				vtotalbomb=document.getElementsByName("dimpuesto1[]"); 	
				vtotalmedio=document.getElementsByName("dimpuesto2[]"); 	
				vtotalintp=document.getElementsByName("dipredial[]"); 	
				vtotalintb=document.getElementsByName("dinteres1[]"); 	
				vtotalintma=document.getElementsByName("dinteres2[]"); 	
				vtotaldes=document.getElementsByName("ddescuentos[]"); 	
				sumar=0;
				sumarp=0;
				sumarb=0;
				sumarma=0;
				sumarint=0;
				sumarintp=0;
				sumarintb=0;
				sumarintma=0;
				sumardes=0;
				for(x=0;x<vvigencias.length;x++)
				{
					 if(vvigencias.item(x).checked)
					 {
						 sumar=sumar+parseFloat(vtotaliqui.item(x).value);
						 sumarp=sumarp+parseFloat(vtotalpred.item(x).value);
						 sumarb=sumarb+parseFloat(vtotalbomb.item(x).value);
						 sumarma=sumarma+parseFloat(vtotalmedio.item(x).value);
						 sumarint=sumarint+parseFloat(vtotalintp.item(x).value)+parseFloat(vtotalintb.item(x).value)+parseFloat(vtotalintma.item(x).value);
						 sumarintp=sumarintp+parseFloat(vtotalintp.item(x).value);
						 sumarintb=sumarintb+parseFloat(vtotalintb.item(x).value);
						 sumarintma=sumarintma+parseFloat(vtotalintma.item(x).value);	 	 
						 sumardes=sumardes+parseFloat(vtotaldes.item(x).value);
					 }
				}
				document.form2.totliquida.value=sumar;
				document.form2.totliquida2.value=sumar;
				document.form2.totpredial.value=sumarp;
				document.form2.totbomb.value=sumarb;
				document.form2.totamb.value=sumarma;
				document.form2.totint.value=sumarint;
				document.form2.intpredial.value=sumarintp;
				document.form2.intbomb.value=sumarintb;
				document.form2.intamb.value=sumarintma;
				document.form2.totdesc.value=sumardes;
 			}
			function callprogress(vValor)
			{
 				document.getElementById("getprogress").innerHTML = vValor;
 				document.getElementById("getProgressBarFill").innerHTML = '<div class="ProgressBarFill" style="width: '+vValor+'%;"></div>';				
				document.getElementById("titulog1").style.display='block';
   				document.getElementById("progreso").style.display='block';
     			document.getElementById("getProgressBarFill").style.display='block';
				if (vValor==100){document.getElementById("titulog2").style.display='block';}
			}
			function tipoformulario(_valor)
			{
				document.getElementById("bgventanamodalm").style.visibility=_valor;
				if(_valor=="hidden"){document.getElementById('ventanam').src="";}
				else
				{
					document.getElementById('ventanam').src="ventana-tipoformulario.php";
				}
			}
			function verformulario(tipo)
			{
				if(tipo=='A')
				{
					document.form2.action="serv-imprimirfacturagrouppdf.php"
					document.form2.target="_BLANK";
					document.form2.submit(); 
					document.form2.action="";
					document.form2.target="";
				}
				else
				{
					document.form2.action="serv-imprimirfacturagroupnpdf.php"
					document.form2.target="_BLANK";
					document.form2.submit(); 
					document.form2.action="";
					document.form2.target="";
				}
			}
		</script>
		<?php titlepag();?>
	</head>
	<body>
		<IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
		<span id="todastablas2"></span>
		<table>
			<tr><script>barra_imagenes("serv");</script><?php cuadro_titulos();?></tr>	 
			<tr><?php menu_desplegable("serv");?></tr>
			<tr>
 				<td colspan="3" class="cinta"><img src="imagenes/add.png" title="Nuevo" onClick="location.href='serv-facturaver.php?'" class="mgbt"/><img src="imagenes/guardad.png" class="mgbt1"/><img src="imagenes/buscad.png" class="mgbt1"/><img src="imagenes/nv.png" title="Nueva Ventana" onClick="mypop=window.open('serv-principal.php','','');mypop.focus();" class="mgbt"/><img src="imagenes/print.png" title="Imprimir" onClick="tipoformulario('visible');" class="mgbt"/><img src='imagenes/iratras.png' title="Men&uacute; Nomina" onClick="location.href='serv-menufacturacion.php'" class='mgbt'></td>
			</tr>		  
		</table>
        <div id="bgventanamodalm" class="bgventanamodalm">
			<div id="ventanamodalm" class="ventanamodalm">
				<IFRAME src="" name="ventanam" marginWidth=0 marginHeight=0 frameBorder=0 id="ventanam" frameSpacing=0 style=" width:700px; height:130px; top:200; overflow:hidden; border-radius:5px;"></IFRAME>
			</div>
		</div>
        <form name="form2" method="post" action="">
			<?php
				//*** cargar parametros de liquidacion
 				$sqlr="Select * from servparametros";
		 		$resp = mysql_query($sqlr,$linkbd);
				while ($row =mysql_fetch_row($resp)) 
				{
					$_POST[tipo]=$row[1];
					$_POST[rangofact]=$row[2];
 					$_POST[diacorte]=$row[3];
					$_POST[cargof]=$row[4];
					$_POST[diaplazo]=$row[6];
					$_POST[obs]=$row[7];
				}
				if($_POST[facini]=="")
				{
					$sqlr="Select Max(id_liquidacion), Min(id_liquidacion) from servliquidaciones where liquidaciongen=$_POST[liquigen]";
					$res=mysql_query($sqlr,$linkbd);
					while ($row =mysql_fetch_row($res)) 
					{
						$_POST[facini]=$row[1];
						$_POST[facfin]=$row[0];
 					}
				}
				$vigusu=vigencia_usuarios($_SESSION[cedulausu]);
				$vact=$vigusu;
				$sqlr="select *from servservicios ";
				$res=mysql_query($sqlr,$linkbd);
				while ($row =mysql_fetch_row($res)) 
				{
					echo "<input type='hidden' name='servcods[]' value='".$row[0]."'>";
					echo "<input type='hidden' name='servnoms[]' value='".$row[1]."'>";
				}
			?> 
      		<table class="inicio">
     			 <tr>
                 	<td class="titulos" colspan="13">Facturacion Servicios</td>
                 	<td class="cerrar" style="width:7%" onClick="location.href='serv-principal.php'">Cerrar</td>
               	</tr>
      			<tr>
                	<td class="saludo3">Liquidacion Generada</td>
                	<td> 
                        <select name="liquigen" id="liquigen" onChange="validar()" >
                            <option value="">Sel..</option>
                            <?php
                                $sqlr="Select * from  servliquidaciones_gen order by id_cab desc ";
                                $resp = mysql_query($sqlr,$linkbd);
                                while ($row =mysql_fetch_row($resp)) 
                                {
                                    if($row[0]==$_POST[liquigen])
                                    {
                                        echo "<option value='$row[0]' SELECTED>$row[0]</option>";
                                        $_POST[vigencias]=$row[1];
                                        $_POST[periodo]=$row[2];
                                        $_POST[periodo2]=$row[3];
                                        $_POST[servicios]=$row[5];
                                        $_POST[fecha]=$row[4];
                                    }
                                    else {echo "<option value='$row[0]'>$row[0]</option>";}
                                }   
                            ?>
                        </select>
                    </td>
                    <td class="saludo3">Fecha Facturacion</td>
                    <input type="hidden" name="obs" value="<?php echo $_POST[obs]; ?>">
                    <input type="hidden" name="diaplazo" value="<?php echo $_POST[diaplazo]; ?>">
                    <td><input type="date" name="fecha" value="<?php echo $_POST[fecha]; ?>" readonly/></td>
                    <td class="saludo3">Vigencia</td>
                    <td> 
                        <select name="vigencias" id="vigencias" >
                            <?php	  
                                for($x=$vact;$x>=$vact-2;$x--)
                                {
                                    if($x==$_POST[vigencias]){echo "<option value='$x' SELECTED>$x</option>";}
                                }
                            ?>
                        </select>
                    </td>
                    <td class="saludo3">Mes:</td>
                    <td>
                        <select name="periodo" id="periodo"  >
                            <?php
                                $sqlr="Select * from meses where estado='S' ";
                                $resp = mysql_query($sqlr,$linkbd);
                                while ($row =mysql_fetch_row($resp)) 
                                {
                                    if($row[0]==$_POST[periodo])
                                    {
                                        echo "<option value='$row[0]' SELECTED>$row[1]</option>";
                                        $_POST[periodonom]=$row[1];
                                        $_POST[periodonom]=$row[2];
                                    }
                                }   
                            ?>
                        </select> 
                    </td>
                    <td class="saludo3">Mes Final:</td>
                    <td>
                        <select name="periodo2" id="periodo2" onChange="validar()"  >
                            <?php
                                $sqlr="Select * from meses where estado='S' ";
                                $resp = mysql_query($sqlr,$linkbd);
                                while ($row =mysql_fetch_row($resp)) 
                                {
                                    if($row[0]==$_POST[periodo2])
                                    {
                                        echo "<option value='$row[0]' SELECTED>$row[1]</option>";
                                        $_POST[periodonom2]=$row[1];
                                        $_POST[periodonom2]=$row[2];
                                    }
                                }   
                            ?>
                        </select> 
                    </td>
                    <td class="saludo1">Servicio:</td>
                    <td>
                        <select name="servicios"   onKeyUp="return tabular(event,this)">
                            <option value="">Todos ....</option>
                            <?php
                                $sqlr="select * from servservicios ";
                                $res=mysql_query($sqlr,$linkbd);
                                while ($row =mysql_fetch_row($res)) 
                                {
                                    if(0==strcmp($row[0],$_POST[servicios]))
                                    {echo "<option value='$row[0]' SELECTED>$row[0] - $row[1]</option>";}
                                }	 	
                            ?>
                        </select>
                    </td>
                    <td><input name="buscapredios" type="button" value=" Ver Liquidacion " onClick="buscaprediales()"> </td>
                </tr>
                <input type="hidden" name="oculto" id="oculto" value="<?php echo $_POST[oculto] ?>"/>
                <input type="hidden" name="facturacion" value="<?php echo $_POST[facturacion] ?>"/>
                <input type="hidden" name="totalreg" value="<?php echo $_POST[totalreg] ?>"/>
                <tr>
                    <td class="saludo1">Fact Inicial</td>
                    <td><input type="text" name="facini" value="<?php echo $_POST[facini]; ?>" size="4"></td>
                    <td class="saludo1">Fact Inicial</td>
                    <td><input type="text" name="facfin" value="<?php echo $_POST[facfin]; ?>" size="4"></td>
                    <td colspan="4">	
         				<div id='titulog1' style='display:none; float:left'></div>
                        <div id='progreso' class='ProgressBar' style='display:none; float:left'>
                            <div class='ProgressBarText'><span id='getprogress'></span>&nbsp;% </div>
                            <div id='getProgressBarFill'></div>
                        </div>
                   	</td>
                </tr>
            </table>
            <div class="subpantallac5">
                <table class="inicio">
                    <tr><td class="titulos" colspan="19">Clientes a Facturar</td></tr>
                    <tr>
                        <td class="titulos2">Factura</td>
                        <td class="titulos2">Cod Usuario</td>
                        <td class="titulos2">Barrio</td>
                        <td class="titulos2">Ruta</td>
                        <td class="titulos2">Liquidacion</td>
                        <td class="titulos2">Direccion</td>
                        <td class="titulos2">Cedula/Nit</td>
                        <td class="titulos2">Propietario</td>
                        <td class="titulos2">Servicio</td>
                        <td class="titulos2">Estrato</td>
                        <td class="titulos2">Medicion</td>
                        <td class="titulos2">Saldo Anterior</td>
                        <td class="titulos2">Cargo Fijo</td>
                        <td class="titulos2">Tarifa</td>
                        <td class="titulos2">Subsidio</td>
                        <td class="titulos2">Valor Subsidiado</td>
                        <td class="titulos2">Descuento</td>
                        <td class="titulos2">Contribuciones</td>
                        <td class="titulos2">Valor Liquidacion</td>
                    </tr>
                    <?php
                        if($_POST[oculto]>=1)
                        {
							ini_set('max_execution_time', 14000);
							$multimeses=($_POST[periodo2]-$_POST[periodo])+1;
							$nuevo="";
							$actual="";
							$conf=1;
							$contador=0;
							$ngen=0;		   
							$iter='zebra1s';
							$iter2='zebra2s';
							//***** numero de liquidaciones 
							
							
							//echo "<br>".$sqlr."<br>";	
							$sqlr="select * from servliquidaciones where factura >= $_POST[facini] and factura<=$_POST[facfin]";
							$respn=mysql_query($sqlr,$linkbd);
							$c=1;
							$totalcli=mysql_affected_rows ($linkbd);
							//echo "<br>".$sqlr."<br>";
							while ($rown =mysql_fetch_row($respn)) 
							{	
								$porcentaje = $c * 100 / $totalcli; 
								echo"<script>progres='".round($porcentaje)."';callprogress(progres);</script>"; 
								flush();
								ob_flush();
								usleep(5);
								$c+=1;
								$multimeses=($rown[4]-$rown[3])+1;
								$idliquidacion=$rown[0];
								$contador=$rown[11];
								$estadofac=$rown[9];
								if ($estadofac=='V')
								$estexto='VENCIDA';
								if ($estadofac=='P')
								$estexto='PAGO';
								if ($estadofac=='S')
								$estexto='';
								/*if($_POST[oculto]==2 && $_POST[facturacion]==2)
								{
								//Factura cabecera	
								 $sqlr="insert into servfacturas (id_factura,vigencia,mes,mes2,codusuario,fecha,servicio,sistema,estado,liquidaciongen) values (".($contador+1).",'".$_POST[vigencias]."','".$_POST[periodo]."','".$_POST[periodo2]."','".$rown[5]."','".$_POST[fecha]."','".$_POST[servicios]."','".$_SESSION[cedulausu]."','S',$ngen)";
								 mysql_query($sqlr,$linkbd);	
								 //echo "<br>".$sqlr."<br>";
								}*/
								//$contador+=1;
								$sqlr="select * from servliquidaciones_det where id_liquidacion=$idliquidacion";
								$resp=mysql_query($sqlr,$linkbd);
								$valortotalfac=0;
								$saldoanterior=0;
								$intereses=0;
								//echo "<br>".$sqlr."<br>";
								while ($row =mysql_fetch_row($resp)) 
								{
								//$saldoanterior=buscasaldoanterior($row[4],$row[2]);
								$saldoanterior=$row[13];
								//***factura detalle	
								//***datos	
								$estratos=$row[5];
								$servicios=$row[4];
								$subsidio=$row[0];
								$tarifas=$row[0];
								$cargofijo=$row[0];
								$descuentos=$row[0];
								$intereses= buscar_intereses($rown[5]);
								$actual=$rown[0];
								$codcat=buscar_codcatservicios($rown[5]);
								$tipoliqser=buscaservicio_liquida($row[4]);
								$sqlr="SELECT nombretercero FROM servclientes WHERE codigo='$codigo'";
								$res=mysql_query($sqlr,$linkbd);
								while($r=mysql_fetch_row($res)){$nomter=$r[0];};
								//$nomter=buscar_nomtercerosp($rown[5]);
								$subsidio=buscasubsidio_valor($row[4],$row[5],$tipoliqser,0);
								$porsubsidio=$subsidio/100;
								//hacer funcion de calculo deudas
								
								$tarifa=buscatarifa_valor($row[4],$row[5],$tipoliqser,0);
								$descuento=buscadescuento_valor($row[4],$row[5],$tipoliqser,0)*$multimeses;
								$porcontribucion=buscacontribucion_valor($row[4],$row[5],$tipoliqser,0);
								$tarifa[0]=$multimeses*$tarifa[0];
								$tarifa[1]=$multimeses*$tarifa[1];	
								$porcontribucion=$porcontribucion/100;
								$contribucion=$tarifa[0]*$porcontribucion;
								$valorsub=$tarifa[0]*$porsubsidio;
								$valortarcf=$tarifa[0]+$tarifa[1];
								//$valorliq=($saldoanterior+$tarifa[0]-$valorsub+$tarifa[1]-$descuento+$contribucion);
								$valorliq=$row[12];
								$centrocosto=buscaservicio_cc($row[4]);
								$bardir=buscar_barriodirecc($rown[5]);
								$barrio=buscar_barrio($bardir[0]);
								$ruta=buscar_ruta($bardir[0]);
								$estrato=buscaestrato_servicios($row[5]);
								$uso=buscar_usosuelo($estrato);
								if($nuevo!=$actual )
								{
									$nrecibo='000000000'.$contador;
									$nrecibo=substr($nrecibo,-9);
									$codigobar="50223".$row[2]."".$nrecibo;
									$codigof="fila_".$nuevo."";	
									$codigoc="conta_".$nuevo."";
									$codigolk="vinculo_".$nuevo."";
									$codigolk2="vinculo2_".$nuevo."";
									$codvalfac=substr('0000000'.$valortotalfac,-10);
									$codigobar="50223".$row[0]."".$nrecibo."".$codvalfac;	
									$vinculo="serv-facturaindividual.php?idfac=".$codigobar."";	
									?>
									<script>
									document.getElementById("<?php echo $codigof?>").rowSpan="<?php echo $conf?>";
									document.getElementById("<?php echo $codigoc?>").rowSpan="<?php echo $conf?>"; 
									document.getElementById("<?php echo $codigolk?>").href="<?php echo $vinculo?>";
									document.getElementById("<?php echo $codigolk2?>").href="<?php echo $vinculo?>";
									</script>
									<?php	
									$valortotalfac=0;
									$conf=1;	
									$nrecibo='000000000'.$contador;
									$nrecibo=substr($nrecibo,-9);
									$codigobar="50223".$row[2]."".$nrecibo;
									echo"
									<input type='hidden' name='codigocat[$contador]' value='$codcat'/>
									<input type='hidden' name='estfac[$contador]' value='$rown[9]'/> 
									<input type='hidden' name='codigousu[$contador]' value='$rown[5]'/> 
									<input type='hidden' name='facturas[$contador]' value='$contador'/>
									<input type='hidden' name='fnomter[$contador]' value='$nomter'/>
									<input type='hidden' name='fdire[$contador]' value='$bardir[1]'/>
									<input type='hidden' name='fbarrio[$contador]' value='$barrio'/>
									<input type='hidden' name='frutas[$contador]' value='$ruta'/>
									<input type='hidden' name='fuso[$contador]' value='$uso'/>
									<input type='hidden' name='festrato[$contador]' value='$estrato'/>
									<input type='hidden' name='fintereses[$contador]' value='$intereses'/>
									<input type='hidden' name='totalcontri[$contador][$row[4]]' value='$row[11]'/>
									<input type='hidden' name='totalsaldoant[$contador][$row[4]]' value='$row[13]'/>
									<input type='hidden' name='totalfact2[$contador][$row[4]]' value='$row[12]'/>
									<input type='hidden' name='totalfact[$contador][$row[4]]' value='$row[8]'/>
									<input type='hidden' name='totalsubs[$contador][$row[4]]' value='$row[9]'/>
									<tr class=$iter>
										<td id=conta_$actual rowspan=$conf>$contador</td>
										<td id=fila_$actual rowspan=$conf>$rown[5]</td>
										<td>$barrio</td>
										<td>$ruta</td>
										<td>$rown[0]</td>
										<td>$bardir[1]</td>
										<td>$row[3]</td>
										<td>$nomter</td>
										<td>$row[4]</td>
										<td>$row[5]</td>
										<td>$row[6]</td>
										<td>$row[13]</td>
										<td>$row[7]</td>
										<td>$row[8]</td>
										<td>$subsidio %</td>
										<td>$row[9]</td>
										<td>$row[10]</td>
										<td>$row[11]</td>
										<td>$row[12]</td>
									</tr>";				
									$valortotalfac+=$valorliq;
									/*if($_POST[oculto]==20 && $_POST[facturacion]==20)
									{				
										$sqlr="UPDATE servliquidaciones set factura=$contador where id_liquidacion=$rown[0]";
										mysql_query($sqlr,$linkbd);	
										$sqlr="insert into comprobante_cab (numerotipo,tipo_comp,fecha,concepto,total,total_debito,total_credito,diferencia,estado) values ('".$contador."','29','".$_POST[fecha]."','LIQUIDACION SERVICIOS PUBLICOS USUARIO ".$row[2]." ".$nomter."',0,0,0,0,'1')";
										mysql_query($sqlr,$linkbd);
									}*/
									$aux=$iter;
									$iter=$iter2;
									$iter2=$aux;
									$nuevo=$actual;		
								}
								else
								{
									if($nuevo==$actual)
									{
										echo "
										<input type='hidden' name='totalsaldoant[$contador][$row[4]]' value='$row[13]'/>
										<input type='hidden' name='totalfact[$contador][$row[4]]' value='$row[8]'/>
										<input type='hidden' name='totalsubs[$contador][$row[4]]' value='$row[9]'/>
										<input type='hidden' name='totalcontri[$contador][$row[4]]' value='$row[11]'/>
										<input type='hidden' name='totalfact2[$contador][$row[4]]' value='$row[12]'/>
										<tr class=$iter>
											<td>$barrio</td>
											<td>$ruta</td>
											<td>$rown[0]</td>
											<td>$bardir[1]</td>
											<td>$row[3]</td>
											<td>$nomter</td>
											<td>$row[4]</td>
											<td>$row[5]</td>
											<td>$row[6]</td>
											<td>$row[13]</td>
											<td>$row[7]</td>
											<td>$row[8]</td>
											<td>$subsidio %</td>
											<td>$row[9]</td>
											<td>$row[10]</td>
											<td>$row[11]</td>
											<td>$row[12]</td>
										</tr>";
										$valortotalfac+=$valorliq;
										$aux=$iter;
										$iter=$iter2;
										$iter2=$aux;
										$conf+=1;
										$codigof="fila_".$actual."";
									}
								}
								if($nuevo!=$actual )
								{
									if($nuevo!=""){$conf=1;}
								}/*
								if($_POST[oculto]==2 && $_POST[facturacion]==2)
								{
									//tarifas
									$concepto=buscaconcepto_sp($row[4],'TR');					
									$cuentascon=concepto_cuentas($concepto,'SP','10',$centrocosto);
									$tm=count($cuentascon);
									for($x=0;$x<$tm;$x++)
									{
										if($cuentascon[$x][1]=='N' && $cuentascon[$x][2]=='S'){$debito=$valortarcf;$credito=0;}
										if($cuentascon[$x][1]=='N' && $cuentascon[$x][2]=='N'){$debito=0;$credito=$valortarcf;}				
										$sqlr="insert into comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque,valdebito, valcredito, estado, vigencia) values ('29 $contador','".$cuentascon[$x][0]."','$row[3]' ,'$centrocosto' , 'TARIFA SERVICIO:".$row[4]."','','$debito','$credito','1' ,'$_POST[vigencias]')";
										mysql_query($sqlr,$linkbd);
									}
									//subsidios
									$concepto=buscaconcepto_sp($row[4],'SB');					
									$cuentascon=concepto_cuentas($concepto,'SP','10',$centrocosto);
									$tm=count($cuentascon);
									$debito=0;
									$credito=0;
									for($x=0;$x<$tm;$x++)
									{
										if($cuentascon[$x][1]=='N' && $cuentascon[$x][2]=='S'){$debito=$valorsub;$credito=0;}
										if($cuentascon[$x][1]=='N' && $cuentascon[$x][2]=='N'){$debito=0;$credito=$valorsub;}				
										$sqlr="insert into comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque,valdebito, valcredito, estado, vigencia) values ('29 $contador','".$cuentascon[$x][0]."','$row[3]' ,'$centrocosto' , 'SUBSIDIO SERVICIO:".$row[4]."','','$debito','$credito','1' ,'$_POST[vigencias]')";
										mysql_query($sqlr,$linkbd);
									}
								}*/
							}
						}
						$codigof="fila_".$nuevo."";
						$codigoc="conta_".$nuevo."";
						$codigolk="vinculo_".$nuevo."";
						$codigolk2="vinculo2_".$nuevo."";
						$codvalfac=substr('0000000'.$valortotalfac,-10);
						$codigobar="50223".$row[0]."".$nrecibo."".$codvalfac;
						$valortotalfac=0;
						$vinculo="serv-facturaindividual.php?idfac=".$codigobar."";	
						?>
							<script>
								document.getElementById("<?php echo $codigof?>").rowSpan="<?php echo $conf?>";
								document.getElementById("<?php echo $codigoc?>").rowSpan="<?php echo $conf?>";
								document.getElementById("<?php echo $codigolk?>").href="<?php echo $vinculo?>";
								document.getElementById("<?php echo $codigolk2?>").href="<?php echo $vinculo?>";
							</script>
						<?php	
						$np=0;
						}
      					$_POST[totalreg]=$c-1;
                    ?>
                    <script>document.form2.totalreg.value="<?php echo $c-1?>"</script>
                </table>
            </div>      
			<?php	
			if ($_POST[oculto]=='2')
			 {
				ereg( "([0-9]{1,2})/([0-9]{1,2})/([0-9]{2,4})", $_POST[fecha],$fecha);
				$fechaf=$fecha[3]."-".$fecha[2]."-".$fecha[1];
				$linkbd=conectar_bd();
				$maximo=0;
				$numecomp=round($np/40,0);	 
			 }
			?>
		</form>
	</body>
</html>