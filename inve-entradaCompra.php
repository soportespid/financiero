<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require 'comun.inc';
	require 'funciones.inc';
	session_start();
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html lang=es>
	<head>
		<meta charset="utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1"/>
		<meta name="viewport" content="user-scalable=no">
		<title>:: IDEAL 10 - Almacen</title>
		<link href="favicon.ico" rel="shortcut icon"/>
		<link href="css/css2z.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
        <link href="css/tabs.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" href="sweetalert2/dist/sweetalert2.min.css">
        <script src="sweetalert2/dist/sweetalert2.min.js"></script>
		<script src="vue/vue.min.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script src="css/calendario.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
        <script>
            
        </script>
		<style>
			input[type=number]::-webkit-inner-spin-button,
			input[type=number]::-webkit-outer-spin-button { 
				-webkit-appearance: none; 
				margin: 0; 
			}
			input[type=number]{
				text-align:right;
				box-sizing: border-box;
				font: 120% sans-serif;
				/* width: 100% !important; */
			}
			[v-cloak]{display : none;}

			.centrarSelect {

				height: 30px !important;
				text-align-last:center !important;
			}

			.aumentarTamaño {

				font-size:15px;
			}

            .tamano01 {
                text-align: center !important;
            }
		</style>
	</head>
	<body>
		<header>
			<table>
				<tr><script>barra_imagenes("inve");</script><?php cuadro_titulos();?></tr>
			</table>
		</header>

		<form name="form2" method="post" action="">
			<section id="myapp" v-cloak >
				<nav>
					<table>
						<tr><?php menu_desplegable("inve");?></tr>
						<tr>
							<td colspan="3" class="cinta">
								<img src="imagenes/add.png" v-on:click="location.href=''" class="mgbt" title="Nuevo">
								<img src="imagenes/guarda.png" title="Guardar" v-on:click="" class="mgbt">
								<img src="imagenes/busca.png" v-on:click="location.href=''" class="mgbt" title="Buscar">
								<img src="imagenes/nv.png" onClick="mypop=window.open('inve-principal.php','',''); mypop.focus();" class="mgbt" title="Nueva Ventana">
							</td>
						</tr>
					</table>
				</nav>

				<article>
                    <div>
                        <table class="inicio">
                            <tr>
                                <td class="titulos" colspan="8">.: Entrada por compra</td>
                            	<td class="cerrar" style="width:4%" onClick="location.href='inve-principal.php'">Cerrar</td>
                            </tr>

                            <tr>
								<td style="width: 3cm;">Consecutivo :</td>
								<td style="width: 10%;">
									<input type="text" readonly>
								</td>

                                <td style="width: 3cm;">Fecha de entrada :</td>
                                <td style="width: 10%;">
									<input type="text" name="fechaIni"  value="<?php echo $_POST['fechaIni']?>" onKeyUp="return tabular(event,this)" id="fechaIni" title="DD/MM/YYYY" onDblClick="displayCalendarFor('fechaIni');" class="colordobleclik" autocomplete="off" onChange="" readonly>
                                </td>

								<td style="width: 3cm;">Descripción:</td>
								<td colspan="3">
									<textarea placeholder="Detalle de entrada" style="width: 100%;"></textarea>
								</td>
                            </tr>

							<tr>
								<td style="width: 3cm;">Registro presupuestal :</td>
								<td style="width: 10%;">
									<input type="text" class="colordobleclik" readonly>
								</td>

								<td colspan="4">
									<textarea placeholder="Descripción registro presupuestal" style="width: 100%;" readonly></textarea>
								</td>

								<td style="width: 3cm;">Valor disponible :</td>
								<td>
									<input type="text" readonly>
								</td>
							</tr>
                        </table>

						<table class="inicio">
							<tr>
                                <td class="titulos" colspan="8">.: Detalles de ingreso</td>
                            </tr>

							<tr>
								<td style="width: 3cm;">Bodega:</td>
								<td style="width: 10%;">
									<select style="width: 100%;">
                                        <option value="">Todas</option>
                                        <option>
                                            
                                        </option>
                                    </select>
								</td>

								<td style="width: 3cm;">Centro de costos:</td>
								<td style="width: 10%;">
									<select style="width: 100%;">
                                        <option value="">Todas</option>
                                        <option>
                                            
                                        </option>
                                    </select>
								</td>

								<td style="width: 3cm;">Articulo :</td>
								<td style="width: 10%;">
									<input type="text" class="colordobleclik" readonly>
								</td>

								<td colspan="2">
									<input type="text" style="width: 100%;" readonly>
								</td>
							</tr>

							<tr>
								<td style="width: 3cm;">Cantidad :</td>
								<td style="width: 10%;">
									<input type="text" >
								</td>

								<td style="width: 3cm;">Valor unitario :</td>
								<td style="width: 10%;">
									<input type="text">
								</td>

								<td style="width: 3cm;">Unidad de medida :</td>
								<td style="width: 10%;">
									<input type="text" readonly>
								</td>
							</tr>
						</table>

						<table>
							<tr>
								<td class="titulos" colspan="20">.: Detalles informe</td>
							</tr>
						</table>
						<div class='subpantalla' style='height:40vh; width:99.2%; margin-top:0px; '>
                            <table>
                                <thead>
                                    <tr style="text-align:Center;">
										
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
									
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>

					<div id="cargando" v-if="loading" class="loading">
						<span>Cargando...</span>
					</div>
				</article>
			</section>
		</form>

		<script src="node_modules/read-excel-file/bundle/read-excel-file.min.js"></script>
		<script src="node_modules/xlsx/dist/xlsx.full.min.js"></script>
		<script src="Librerias/vue/vue.min.js"></script>
		<script src="Librerias/vue/axios.min.js"></script>
		<script src="almacen/entradaCompra/crear/inve-entradaCompra.js?"></script>
        
	</body>
</html>