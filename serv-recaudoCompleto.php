<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require "comun.inc";
	require "funciones.inc";
	session_start();
	$linkbd = conectar_v7();
    $linkbd -> set_charset("utf8");
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
	//error_reporting(E_ALL);
?>
<!DOCTYPE >
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es">
	<head>
		<meta http-equiv="Content-type" content="text/html; charset=iso-8859-1"/>
		<meta http-equiv="X-UA-Compatible" content="IE=9"/>
		<title>:: IDEAL 10 - Servicios P&uacute;blicos</title>
		<link href="css/css2.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
        <link href="css/cssSP.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
        <link href="css/style.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
		<script type="text/javascript" src="css/programas.js"></script>
        <script type="text/javascript" src="css/calendario.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script>
			function despliegamodalm(_valor,_tip,mensa,pregunta)
			{
				document.getElementById("bgventanamodalm").style.visibility=_valor;
				if(_valor=="hidden"){document.getElementById('ventanam').src="";}
				else
				{
					switch(_tip)
					{
						case "1":	document.getElementById('ventanam').src="ventana-mensaje1.php?titulos="+mensa;break;
						case "2":	document.getElementById('ventanam').src="ventana-mensaje3.php?titulos="+mensa;break;
						case "3":	document.getElementById('ventanam').src="ventana-mensaje2.php?titulos="+mensa;break;
						case "4":	document.getElementById('ventanam').src="ventana-consulta1.php?titulos="+mensa+"&idresp="+pregunta;break;	
					}
				}
			}
			function funcionmensaje()
			{
                var consecutivo = document.getElementById('consecutivo').value;
				document.location.href = "serv-recaudoCompletoVisualizar.php?idban="+consecutivo;
			}
			function actualizar()
			{
				document.form2.submit();
			}
			function guardar()
			{	
                var validacionRecaudo = document.getElementById('modoRecaudo').value;
                var numeroFactura = document.getElementById('numeroFactura').value;
                var valor = document.getElementById('valor').value;
                var tipoMovimiento = document.getElementById('tipoMovimiento').value;

                if (numeroFactura == "" || valor == "" || tipoMovimiento == "" || validacionRecaudo == "-1") {
                    despliegamodalm('visible','2','Falta información para crear el recibo de caja');
                    return false;
                }

                if (validacionRecaudo == "banco" && document.getElementById('banco').value == "") {
                    despliegamodalm('visible','2','Debe seleccionar cuenta bancaria');
                    return false;
                }

                despliegamodalm('visible','4','Esta Seguro de Guardar','1');
			}
			function despliegamodal2(_valor, modal)
			{
				document.getElementById("bgventanamodal2").style.visibility=_valor;
				if(_valor=="hidden")
				{
					document.getElementById('ventana2').src="";
				}
				else 
				{
					if(modal == 'cuentasBancarias')
                    {
                        document.getElementById('ventana2').src="cuentasBancarias-ventana.php";
                    }
                    if(modal == 'clientes')
                    {
                        document.getElementById('ventana2').src="serv-ventana-recaudoClientes.php";
                    }
				}
			}
            function respuestaconsulta(pregunta)
			{
				switch(pregunta)
				{
					case "1":	
						document.form2.oculto.value = '2';
						document.form2.submit();
					break;
				}
			}
			function buscarFactura()
			{
				if (document.form2.numeroFactura.value!="")
				{
					document.form2.bc.value='1';
					document.form2.submit();
				}
			}
            function validar(){document.form2.submit();}
		</script>
		<?php titlepag();?>
	</head>
	<body>
		<IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
		<span id="todastablas2"></span>
		<table>
			<tr><script>barra_imagenes("serv");</script><?php cuadro_titulos();?></tr>

			<tr><?php menu_desplegable("serv");?></tr>

			<tr>
				<td colspan="3" class="cinta">
                    <a href="serv-recaudoCompleto.php" class="mgbt"><img src="imagenes/add.png"/></a>

					<a onclick="guardar()" class="mgbt"><img src="imagenes/guarda.png" title="Guardar"/></a>

					<a href="serv-recaudoCompletoBuscar.php" class="mgbt"><img src="imagenes/busca.png" title="Buscar"/></a>

					<a onclick="mypop=window.open('plan-agenda.php','','');mypop.focus()" class="mgbt"><img src="imagenes/agenda1.png" title="Agenda" /></a>

					<a onclick="mypop=window.open('serv-principal.php','','');mypop.focus();" class="mgbt"><img src="imagenes/nv.png" title="Nueva ventana"></a>

					<a onclick="mypop=window.open('<?php echo $url2; ?>','','');mypop.focus();" class="mgbt"><img src="imagenes/duplicar_pantalla.png" title="Duplicar pesta&ntilde;a"></a>

					<a href="serv-menuRecaudo.php" class="mgbt"><img src="imagenes/iratras.png" title="Atr&aacute;s"></a>
                </td>
			</tr>
		</table>

		<div id="bgventanamodalm" class="bgventanamodalm">
			<div id="ventanamodalm" class="ventanamodalm">
				<IFRAME src="" name="ventanam" marginWidth=0 marginHeight=0 frameBorder=0 id="ventanam" frameSpacing=0 style=" width:700px; height:130px; top:200; overflow:hidden;"></IFRAME>
			</div>
		</div>

		<form name="form2" method="post" action="">
			<?php 
                $vigusu=vigencia_usuarios($_SESSION['cedulausu']);

				if(@ $_POST['oculto']=="")
				{
                    $_POST['fecha_pago'] = date("d/m/Y");
                    $_POST['consecutivo'] = selconsecutivo('srvrecibo_caja','consecutivo');
				}
                
				if(@ $_POST['bc'] == '1')
				{
                    $sqlConsultaFactura = "SELECT estado_pago FROM srvcortes_detalle WHERE numero_facturacion = '$_POST[numeroFactura]' ";
                    $resConsultaFactura = mysqli_query($linkbd,$sqlConsultaFactura);
                    $rowConsultaFactura = mysqli_fetch_row($resConsultaFactura);

                    if($rowConsultaFactura[0] == 'S')
                    {
                        $sqlDetallesFacturacion = "SELECT id_cliente FROM srvdetalles_facturacion WHERE numero_facturacion = '".$_POST['numeroFactura']."' ";
                        $resDetallesFacturacion = mysqli_query($linkbd,$sqlDetallesFacturacion);
                        $rowDetallesFacturacion2 = mysqli_fetch_row($resDetallesFacturacion);

                        $sqlCorteDetalles = "SELECT id_cliente,numero_facturacion,estado_pago,id_corte FROM srvcortes_detalle WHERE numero_facturacion = '".$_POST['numeroFactura']."' ";
                        $resCorteDetalles = mysqli_query($linkbd,$sqlCorteDetalles);
                        $rowCorteDetalles = mysqli_fetch_row($resCorteDetalles);

                        $_POST['id_cliente'] = $rowCorteDetalles[0];

                        if($rowCorteDetalles[2] == 'S')
                        {
                            $_POST['estadoPago'] = "PAGO PENDIENTE";
                        }
                        else
                        {
                            $_POST['estadoPago'] = "FACTURA NO DISPONIBLE";
                        }

                        $_POST['corte'] = $rowCorteDetalles[3];

                        $sqlCorte = "SELECT numero_corte,fecha_inicial,fecha_final,fecha_limite_pago,fecha_impresion,servicio FROM srvcortes WHERE numero_corte = '$rowCorteDetalles[3]' ";
                        $resCorte = mysqli_query($linkbd,$sqlCorte);
                        $rowCorte = mysqli_fetch_row($resCorte);

                        $sqlCliente = "SELECT id_tercero,cod_usuario,cod_catastral,id_barrio,id_estrato FROM srvclientes WHERE id = '$rowDetallesFacturacion2[0]' ";
                        $resCliente = mysqli_query($linkbd,$sqlCliente);
                        $rowCliente = mysqli_fetch_row($resCliente);

                        $sqlDireccion = "SELECT direccion FROM srvdireccion_cliente WHERE id_cliente = '$rowCorteDetalles[0]' ";
                        $resDireccion = mysqli_query($linkbd,$sqlDireccion);
                        $rowDireccion = mysqli_fetch_row($resDireccion);

                        $sqlTercero = "SELECT nombre1,nombre2,apellido1,apellido2,razonsocial,cedulanit FROM terceros WHERE id_tercero = '$rowCliente[0]' ";
                        $resTercero = mysqli_query($linkbd,$sqlTercero);
                        $rowTercero = mysqli_fetch_row($resTercero);

                        $nombreCompleto = $rowTercero[0].' '.$rowTercero[1].' '.$rowTercero[2].' '.$rowTercero[3];

                        if ($rowTercero[4] != '') 
                        {
                            $nombreCompleto = $rowTercero[4];
                        }

                        $_POST['nombreCliente'] = $nombreCompleto;
                        $_POST['documento'] = $rowTercero[5];
                        $_POST['concepto'] = "PAGO TOTAL DE FACTURA ".$_POST['numeroFactura']." DEL USUARIO ".$nombreCompleto;

                        $sqlAsignacionServicios = "SELECT id_servicio FROM srvasignacion_servicio WHERE id_clientes = '$rowCorteDetalles[0]' AND estado = 'S' ORDER BY id_servicio ";
                        $resAsignacionServicios = mysqli_query($linkbd,$sqlAsignacionServicios);
                        while($rowAsignacionServicios = mysqli_fetch_row($resAsignacionServicios))
                        {
                            /*
                            Tipo de cobro
                            1. CARGO FIJO
                            2. TARIFA SIN MEDIDOR
                            3. TARIFA CON MEDIDOR
                            4. OTROS COBROS
                            5. SUBSIDIOS
                            6. EXONERACIONES
                            */

                            $sqlServicios = "SELECT nombre FROM srvservicios WHERE id = '$rowAsignacionServicios[0]' ";
                            $resServicios = mysqli_query($linkbd,$sqlServicios);
                            $rowServicios = mysqli_fetch_row($resServicios);

                            
                            
                        }
                    
                        $sqlDetallesFacturacion = "SELECT SUM(credito), SUM(debito) FROM srvdetalles_facturacion WHERE corte = '$rowCorte[0]' AND id_cliente = '$rowCorteDetalles[0]' AND numero_facturacion = '$rowCorteDetalles[1]' AND (tipo_movimiento = '101' OR tipo_movimiento = '204')";
                        $resDetallesFacturacion = mysqli_query($linkbd,$sqlDetallesFacturacion);
                        $rowDetallesFacturacion = mysqli_fetch_row($resDetallesFacturacion);
                        $_POST['valor'] = $rowDetallesFacturacion[0] - $rowDetallesFacturacion[1];

                    }
                    else
                    {
                        switch ($rowConsultaFactura[0]) 
                        {
                            case 'V':
                                $_POST['corte'] = '';
                                $_POST['estadoPago'] = '';
                                $_POST['concepto'] = '';
                                $_POST['valor'] = '';
                                $_POST['documento'] = '';
                                $_POST['nombreCliente'] = '';
                                $_POST['numeroFactura'] = '';
                                $_POST['banco'] = '';
                                $_POST['nbanco'] = '';

                                echo"<script>despliegamodalm('visible','2','Esta factura ya esta VENCIDA.');</script>";        
                            break;
                            
                            case 'P': 
                                $_POST['corte'] = '';
                                $_POST['estadoPago'] = '';
                                $_POST['concepto'] = '';
                                $_POST['valor'] = '';
                                $_POST['documento'] = '';
                                $_POST['nombreCliente'] = '';
                                $_POST['numeroFactura'] = '';
                                $_POST['banco'] = '';
                                $_POST['nbanco'] = '';

                                echo"<script>despliegamodalm('visible','2','Esta factura ya fue PAGADA.');</script>";
                            break;

                            case 'A': 
                                $_POST['corte'] = '';
                                $_POST['estadoPago'] = '';
                                $_POST['concepto'] = '';
                                $_POST['valor'] = '';
                                $_POST['documento'] = '';
                                $_POST['nombreCliente'] = '';
                                $_POST['numeroFactura'] = '';
                                $_POST['banco'] = '';
                                $_POST['nbanco'] = '';

                                echo"<script>despliegamodalm('visible','2','Esta factura se le realizo un ACUERDO DE PAGO.');</script>";
                            break;

                            default:    
                                $_POST['corte'] = '';
                                $_POST['estadoPago'] = '';
                                $_POST['concepto'] = '';
                                $_POST['valor'] = '';
                                $_POST['documento'] = '';
                                $_POST['nombreCliente'] = '';
                                $_POST['numeroFactura'] = '';
                                $_POST['banco'] = '';
                                $_POST['nbanco'] = '';

                                echo"<script>despliegamodalm('visible','2','Esta no se encuentra creada.');</script>";
                            break;
                        }
                    }
				}	
				
			?>
			<table class="inicio ancho">
				<tr>
					<td class="titulos" colspan="8">.: Recibo de Caja</td>

					<td class="cerrar" style="width:7%" onClick="location.href='serv-principal.php'">Cerrar</td>
				</tr>

				<tr>
                    <td class="tamano01" style="width:10%;">Consecutivo:</td>

                    <td style="width:15%;">
                        <input  type="text" name="consecutivo" id="consecutivo" value="<?php echo @ $_POST['consecutivo'];?>" style="width:100%;height:30px;text-align:center;" readonly/>
                    </td>

                    <td class="tamano01" style="width:10%;">Corte:</td>

                    <td style="width:15%;">
						<input  type="text" name="corte" id="corte" value="<?php echo @ $_POST['corte'];?>" style="width:100%;height:30px;text-align:center;" readonly/>
					</td>

                    <td class="tamano01" style="width:10%;">Fecha de Pago</td>

					<td style="width:15%;">
                        <input type="text" name="fecha_pago" id="fecha_pago" value="<?php echo @ $_POST['fecha_pago']?>" maxlength="10" onKeyUp="return tabular(event,this)"  id="fc_1198971545" onKeyDown="mascara(this,'/',patron,true)" title="DD/MM/YYYY" style="height:30px;text-align:center;width:85%;" onchange="" readonly/>
                        
                        <a href="#" onClick="displayCalendarFor('fecha_pago');" title="Calendario">
                            <img src="imagenes/calendario04.png" style="width:25px;">
                        </a>
                    </td>

                    <td class="tamano01" style="width: 10%;">Estado de Pago:</td>

                    <td>
                        <input type="text" name="estadoPago" id="estadoPago" style="width: 100%;" value="<?php echo @$_POST['estadoPago']; ?>" readonly>
                    </td>
                    
                </tr>
                
                <tr>
					<td class="tamano01">Recaudo en: </td>

                    <td>
                        <select  name="modoRecaudo" id="modoRecaudo" style="width:100%;height:30px;" onChange="validar()" class="centrarSelect">
							<option class="aumentarTamaño" value="-1">:: SELECCIONE TIPO DE RECAUDO ::</option>
                            <option class="aumentarTamaño" value="caja" <?php if(@$_POST['modoRecaudo']=='caja') echo "SELECTED"; ?>>Caja</option>
							<option class="aumentarTamaño" value="banco" <?php if(@$_POST['modoRecaudo']=='banco') echo "SELECTED"; ?>>Banco</option>
						</select>
					</td>

                    <td class="tamano01">Tipo de Movimiento:</td>

                    <td>
                        <select  name="tipoMovimiento" id="tipoMovimiento" style="width:100%;height:30px;" value="<?php echo $_POST['tipoMovimiento'] ?>">
							<option class='aumentarTamaño' value="201" >PAGO DE FACTURA TOTAL</option>
						</select>
                    </td>

                    <td class="tamano01" style="width:2cm;">N&deg; de Factura:</td>

					<td style="width:15%;">
						<input type="text" name="numeroFactura" id="numeroFactura" value="<?php echo @ $_POST['numeroFactura'];?>" style="width:85%;height:30px;" onblur='buscarFactura()'/>
                        <a title="Cuentas presupuestales" onClick="despliegamodal2('visible', 'clientes');" style="cursor:pointer;"><img src="imagenes/find02.png" style="width:20px;"/></a>
						<input type="hidden" value="" name="bc" id="bc">
					</td>
                </tr>

			<?php
				if (@$_POST['modoRecaudo'] == 'banco')
				{
			?>
				<tr>
					<td class="saludo1">Cuenta Banco: </td> 

					<td>
						<input type="text" id="banco" name="banco" style="width:88%" onKeyPress="javascript:return solonumeros(event)" onKeyUp="return tabular(event,this)" onBlur="" value="<?php echo $_POST['banco']?>" onClick="document.getElementById('banco').focus();document.getElementById('banco').select();">

                        <input type="hidden" name="cuentaBancaria" id="cuentaBancaria" value="<?php echo $_POST['cuentaBancaria'] ?>">

                    <a title="Bancos" onClick="despliegamodal2('visible', 'cuentasBancarias');" style="cursor:pointer;"><img src="imagenes/find02.png" style="width:20px;"/></a>
					</td>

					<td colspan="2">
						<input type="text" name="nbanco" id="nbanco" style="width:100%;" value="<?php echo $_POST['nbanco']?>"  readonly>
					</td>
				</tr>
			<?php
				}
			?>
				<tr>
					<td class="tamano01" style="width:2cm;">Concepto:</td>

					<td style="width:15%;" colspan="3">
						<input  type="text" name="concepto" id="concepto" value="<?php echo @ $_POST['concepto'];?>" style="width:100%;height:30px;" />
					</td>

					<td class="tamano01" style="width:2cm;">Valor:</td>

					<td style="width:15%;">
						<input  type="text" name="valor" id="valor" value="<?php echo @ $_POST['valor'];?>" style="width:100%;height:30px;" readonly/>
					</td>
				</tr>

				<tr>
					<td class="tamano01" style="width:2cm;">Documento:</td>

					<td style="width:15%;">
						<input  type="text" name="documento" id="documento" value="<?php echo @ $_POST['documento'];?>" style="width:100%;height:30px;" readonly/>
					</td>

					<td class="tamano01" style="width:2cm;">Contribuyente:</td>

					<td style="width:15%;" colspan="3">
						<input type="text" name="nombreCliente" id="nombreCliente" value="<?php echo @ $_POST['nombreCliente'];?>" style="width:100%;height:30px;" readonly/>
					</td>
				</tr>
			</table>

            <div class="subpantalla">
                <table class="inicio">

                    <?php
                        if(@$_POST['oculto'])
                        {
                            $sqlDetallesFacturacion = "SELECT id_cliente FROM srvdetalles_facturacion WHERE numero_facturacion = '".$_POST['numeroFactura']."' ";
                            $resDetallesFacturacion = mysqli_query($linkbd,$sqlDetallesFacturacion);
                            $rowDetallesFacturacion2 = mysqli_fetch_row($resDetallesFacturacion);

                            $sqlCorteDetalles = "SELECT id_cliente,numero_facturacion,estado_pago,id_corte FROM srvcortes_detalle WHERE numero_facturacion = '".$_POST['numeroFactura']."' ";
                            $resCorteDetalles = mysqli_query($linkbd,$sqlCorteDetalles);
                            $rowCorteDetalles = mysqli_fetch_row($resCorteDetalles);

                            $sqlCorte = "SELECT numero_corte,fecha_inicial,fecha_final,fecha_limite_pago,fecha_impresion,servicio FROM srvcortes WHERE numero_corte = '$rowCorteDetalles[3]' ";
                            $resCorte = mysqli_query($linkbd,$sqlCorte);
                            $rowCorte = mysqli_fetch_row($resCorte);

                            $sqlCliente = "SELECT id_tercero,cod_usuario,cod_catastral,id_barrio,id_estrato FROM srvclientes WHERE id = '$rowDetallesFacturacion2[0]' ";
                            $resCliente = mysqli_query($linkbd,$sqlCliente);
                            $rowCliente = mysqli_fetch_row($resCliente);

                            $sqlDireccion = "SELECT direccion FROM srvdireccion_cliente WHERE id_cliente = '$rowCorteDetalles[0]' ";
                            $resDireccion = mysqli_query($linkbd,$sqlDireccion);
                            $rowDireccion = mysqli_fetch_row($resDireccion);

                            $sqlTercero = "SELECT nombre1,nombre2,apellido1,apellido2,razonsocial,cedulanit FROM terceros WHERE id_tercero = '$rowCliente[0]' ";
                            $resTercero = mysqli_query($linkbd,$sqlTercero);
                            $rowTercero = mysqli_fetch_row($resTercero);

                            $sqlAsignacionServicios = "SELECT id_servicio FROM srvasignacion_servicio WHERE id_clientes = '$rowCorteDetalles[0]' AND estado = 'S' ORDER BY id_servicio ";
                            $resAsignacionServicios = mysqli_query($linkbd,$sqlAsignacionServicios);
                            while($rowAsignacionServicios = mysqli_fetch_row($resAsignacionServicios))
                            {
                                /*
                                Tipo de cobro
                                1. CARGO FIJO
                                2. TARIFA SIN MEDIDOR
                                3. TARIFA CON MEDIDOR
                                4. OTROS COBROS
                                5. SUBSIDIOS
                                6. EXONERACIONES
                                */

                                $sqlServicios = "SELECT nombre FROM srvservicios WHERE id = '$rowAsignacionServicios[0]' ";
                                $resServicios = mysqli_query($linkbd,$sqlServicios);
                                $rowServicios = mysqli_fetch_row($resServicios);

                                //Codigo y nombre de servicio
                                $_POST['codigoServicio'][] = $rowAsignacionServicios[0];
                                $_POST['nombreServicio'][] = $rowServicios[0];

                                $sqlDetallesFacturacion = "SELECT credito FROM srvdetalles_facturacion WHERE corte = '$rowCorte[0]' AND id_cliente = '$rowCorteDetalles[0]' AND numero_facturacion = '$rowCorteDetalles[1]' AND id_servicio = '$rowAsignacionServicios[0]' AND id_tipo_cobro = 1 ";
                                $resDetallesFacturacion = mysqli_query($linkbd,$sqlDetallesFacturacion);
                                $rowDetallesFacturacion = mysqli_fetch_row($resDetallesFacturacion);

                                //Cargo Fijo del servicio
                                $cargoFijo = $rowDetallesFacturacion[0]; 

                                $sqlDetallesFacturacion = "SELECT credito FROM srvdetalles_facturacion WHERE corte = '$rowCorte[0]' AND id_cliente = '$rowCorteDetalles[0]' AND numero_facturacion = '$rowCorteDetalles[1]' AND id_servicio = '$rowAsignacionServicios[0]' AND id_tipo_cobro = 3 ";
                                $resDetallesFacturacion = mysqli_query($linkbd,$sqlDetallesFacturacion);
                                $rowDetallesFacturacion = mysqli_fetch_row($resDetallesFacturacion);

                                //Valida si existe cobro por medio de medidor
                                if(isset($rowDetallesFacturacion[0]))
                                {
                                    $cobroConMedidor = $rowDetallesFacturacion[0]; 
                                } 
                                else
                                {
                                    $cobroConMedidor = 0;
                                }

                                $sqlDetallesFacturacion = "SELECT credito FROM srvdetalles_facturacion WHERE corte = '$rowCorte[0]' AND id_cliente = '$rowCorteDetalles[0]' AND numero_facturacion = '$rowCorteDetalles[1]' AND id_servicio = '$rowAsignacionServicios[0]' AND id_tipo_cobro = 2 ";
                                $resDetallesFacturacion = mysqli_query($linkbd,$sqlDetallesFacturacion);
                                $rowDetallesFacturacion = mysqli_fetch_row($resDetallesFacturacion);

                                //Valida si existe cobro sin medidor
                                if(isset($rowDetallesFacturacion[0]))
                                {
                                    $cobroSinMedidor = $rowDetallesFacturacion[0]; 
                                } 
                                else
                                {
                                    $cobroSinMedidor = 0;
                                }

                                $sqlDetallesFacturacion = "SELECT credito FROM srvdetalles_facturacion WHERE corte = '$rowCorte[0]' AND id_cliente = '$rowCorteDetalles[0]' AND numero_facturacion = '$rowCorteDetalles[1]' AND id_servicio = '$rowAsignacionServicios[0]' AND id_tipo_cobro = 11 ";
                                $resDetallesFacturacion = mysqli_query($linkbd,$sqlDetallesFacturacion);
                                $rowDetallesFacturacion = mysqli_fetch_row($resDetallesFacturacion);

                                //Valida si existe otros cobros
                                if(isset($rowDetallesFacturacion[0]))
                                {
                                    $interesMoratorio = $rowDetallesFacturacion[0]; 
                                } 
                                else
                                {
                                    $interesMoratorio = 0;
                                }

                                $sqlDetallesFacturacion = "SELECT debito FROM srvdetalles_facturacion WHERE corte = '$rowCorte[0]' AND id_cliente = '$rowCorteDetalles[0]' AND numero_facturacion = '$rowCorteDetalles[1]' AND id_servicio = '$rowAsignacionServicios[0]' AND id_tipo_cobro = 5 ";
                                $resDetallesFacturacion = mysqli_query($linkbd,$sqlDetallesFacturacion);
                                $rowDetallesFacturacion = mysqli_fetch_row($resDetallesFacturacion);

                                //Valida si existe subsidios
                                if(isset($rowDetallesFacturacion[0]))
                                {
                                    $subsidios = $rowDetallesFacturacion[0]; 
                                } 
                                else
                                {
                                    $subsidios = 0;
                                }

                                $sqlDetallesFacturacion = "SELECT debito FROM srvdetalles_facturacion WHERE corte = '$rowCorte[0]' AND id_cliente = '$rowCorteDetalles[0]' AND numero_facturacion = '$rowCorteDetalles[1]' AND id_servicio = '$rowAsignacionServicios[0]' AND id_tipo_cobro = 6 ";
                                $resDetallesFacturacion = mysqli_query($linkbd,$sqlDetallesFacturacion);
                                $rowDetallesFacturacion = mysqli_fetch_row($resDetallesFacturacion);

                                //Valida si existe exoneraciones
                                if(isset($rowDetallesFacturacion[0]))
                                {
                                    $exoneraciones = $rowDetallesFacturacion[0]; 
                                } 
                                else
                                {
                                    $exoneraciones = 0;
                                }

                                $sqlDetallesFacturacion = "SELECT credito FROM srvdetalles_facturacion WHERE corte = '$rowCorte[0]' AND id_cliente = '$rowCorteDetalles[0]' AND numero_facturacion = '$rowCorteDetalles[1]' AND id_servicio = '$rowAsignacionServicios[0]' AND id_tipo_cobro = 7 ";
                                $resDetallesFacturacion = mysqli_query($linkbd,$sqlDetallesFacturacion);
                                $rowDetallesFacturacion = mysqli_fetch_row($resDetallesFacturacion);

                                //Valida si existe contribuciones
                                if(isset($rowDetallesFacturacion[0]))
                                {
                                    $contribuciones = $rowDetallesFacturacion[0]; 
                                } 
                                else
                                {
                                    $contribuciones = 0;
                                }

                                $saldo = 0;

                                $sqlDetallesFacturacion = "SELECT credito FROM srvdetalles_facturacion WHERE corte = '$rowCorte[0]' AND id_cliente = '$rowCorteDetalles[0]' AND numero_facturacion = '$rowCorteDetalles[1]' AND id_servicio = '$rowAsignacionServicios[0]' AND id_tipo_cobro = 8";
                                $resDetallesFacturacion = mysqli_query($linkbd,$sqlDetallesFacturacion);
                                $rowDetallesFacturacion = mysqli_fetch_row($resDetallesFacturacion);

                                if(isset($rowDetallesFacturacion[0]))
                                {
                                    $saldo = $saldo + $rowDetallesFacturacion[0]; 
                                } 

                                $sqlDetallesFacturacion = "SELECT credito FROM srvdetalles_facturacion WHERE corte = '$rowCorte[0]' AND id_cliente = '$rowCorteDetalles[0]' AND numero_facturacion = '$rowCorteDetalles[1]' AND id_servicio = '$rowAsignacionServicios[0]' AND id_tipo_cobro = 10";
                                $resDetallesFacturacion = mysqli_query($linkbd,$sqlDetallesFacturacion);
                                $rowDetallesFacturacion = mysqli_fetch_row($resDetallesFacturacion);

                                if(isset($rowDetallesFacturacion[0]))
                                {
                                    $saldo = $saldo + $rowDetallesFacturacion[0]; 
                                } 

                                $sql = "SELECT credito FROM srvdetalles_facturacion WHERE corte = $rowCorte[0] AND numero_facturacion = '$rowCorteDetalles[1]' AND id_servicio = $rowAsignacionServicios[0] AND id_tipo_cobro = 9";
                                $res = mysqli_query($linkbd,$sql);
                                $row = mysqli_fetch_row($res);

                                if(isset($row[0]))
                                {
                                    $acuerdoPago = $row[0];
                                } 
                                else
                                {
                                    $acuerdoPago = 0;
                                }

                                $valorReal = $cargoFijo + $cobroConMedidor + $cobroSinMedidor + $interesMoratorio + $contribuciones + $saldo + $acuerdoPago - $subsidios - $exoneraciones;

                                $_POST['cargoFijo'][]        = $cargoFijo;
                                $_POST['conMedidor'][]       = $cobroConMedidor;
                                $_POST['sinMedidor'][]       = $cobroSinMedidor;
                                $_POST['interesMoratorio'][] = $interesMoratorio;
                                $_POST['subsidios'][]        = $subsidios;
                                $_POST['exoneraciones'][]    = $exoneraciones;
                                $_POST['contribuciones'][]   = $contribuciones;
                                $_POST['saldo'][]            = $saldo;
                                $_POST['acuerdo'][]          = $acuerdoPago;
                                $_POST['valorReal'][]        = $valorReal;


                                $_POST['totalCargoFijo']        = $_POST['totalCargoFijo'] + $cargoFijo;
                                $_POST['totalCobroConMedidor']  = $_POST['totalCobroConMedidor'] + $cobroConMedidor;
                                $_POST['totalCobroSinMedidor']  = $_POST['totalCobroSinMedidor'] + $cobroSinMedidor;
                                $_POST['totalInteresMoratorio'] = $_POST['totalInteresMoratorio'] + $interesMoratorio;
                                $_POST['totalContribuciones']   = $_POST['totalContribuciones'] + $contribuciones;
                                $_POST['totalSubsidios']        = $_POST['totalSubsidios'] + $subsidios;
                                $_POST['totalExoneraciones']    = $_POST['totalExoneraciones'] + $exoneraciones;
                                $_POST['totalSaldo']            = $_POST['totalSaldo'] + $saldo;
                                $_POST['totalAcuerdo']          = $_POST['totalAcuerdo'] + $acuerdoPago;
                                $_POST['totalValorReal']        = $_POST['totalValorReal'] + $valorReal;
                            }
                        }
                    ?>

                    <tr>
                        <td class="titulos" colspan="12">.: Informaci&oacute;n Servicios</td>
                    </tr>

                    <tr class="titulos2">
                        <td style="text-align:center;">C&oacute;digo</td>
                        <td style="text-align:center;">Servicio</td>
                        <td style="text-align:center;">Cargo Fijo</td>
                        <td style="text-align:center;">Cobro con medidor</td>
                        <td style="text-align:center;">Cobro sin medidor</td>
                        <td style="text-align:center;">Contribuciones</td>
                        <td style="text-align:center;">Subsidios</td>
                        <td style="text-align:center;">Exoneraciones</td>
                        <td style="text-align:center;">Saldos</td>
                        <td style="text-align:center;">Interes Moratorio</td>
                        <td style="text-align:center;">Acuerdos de Pago</td>
                        <td style="text-align:center;">Valor real</td>
                    </tr>

                    <?php
                    for ($x=0;$x< count($_POST['codigoServicio']);$x++)
                    {
                        echo "
                            <tr class='saludo1a'>
                                <td class='' style='text-align:center;width:5%;'>".$_POST['codigoServicio'][$x]."</td>

                                <td class='' style='text-align:center;width:10%;'>".$_POST['nombreServicio'][$x]."</td>

                                <td class='' style='text-align:center;width:9%;'>".number_format($_POST['cargoFijo'][$x],2,',','.')."</td>
                                
                                <td class='' style='text-align:center;width:9%;'>".number_format($_POST['conMedidor'][$x],2,',','.')."</td>
                            
                                <td class='' style='text-align:center;width:9%;'>".number_format($_POST['sinMedidor'][$x],2,',','.')."</td>

                                <td class='' style='text-align:center;width:9%;'>".number_format($_POST['contribuciones'][$x],2,',','.')."</td>
                                    
                                <td class='' style='text-align:center;width:9%;'>".number_format($_POST['subsidios'][$x],2,',','.')."</td>
                    
                                <td class='' style='text-align:center;width:9%;'>".number_format($_POST['exoneraciones'][$x],2,',','.')."</td>
                            
                                <td class='' style='text-align:center;width:9%;'>".number_format($_POST['saldo'][$x],2,',','.')."</td>

                                <td class='' style='text-align:center;width:9%;'>".number_format($_POST['interesMoratorio'][$x],2,',','.')."</td>

                                <td class='' style='text-align:center;width:9%;'>".number_format($_POST['acuerdo'][$x],2,',','.')."</td>
                            
                                <td class='' style='text-align:center;width:9%;'>".number_format($_POST['valorReal'][$x],2,',','.')."</td>
                            </tr>";
                    }
                    
                    echo "
                    <tr class='titulos2'>
                        <td style='text-align:center;' colspan='2'>Total:</td>

                        <td style='text-align:center;'>".number_format($_POST['totalCargoFijo'],2,',','.')."</td>
                        
                        <td style='text-align:center;'>".number_format($_POST['totalCobroConMedidor'],2,',','.')."</td>
                        
                        <td style='text-align:center;'>".number_format($_POST['totalCobroSinMedidor'],2,',','.')."</td>

                        <td style='text-align:center;'>".number_format($_POST['totalContribuciones'],2,',','.')."</td>
                            
                        <td style='text-align:center;'>".number_format($_POST['totalSubsidios'],2,',','.')."</td>
                            
                        <td style='text-align:center;'>".number_format($_POST['totalExoneraciones'],2,',','.')."</td>

                        <td style='text-align:center;'>".number_format($_POST['totalSaldo'],2,',','.')."</td>

                        <td style='text-align:center;'>".number_format($_POST['totalInteresMoratorio'],2,',','.')."</td>

                        <td style='text-align:center;'>".number_format($_POST['totalAcuerdo'],2,',','.')."</td>

                        <td style='text-align:center;'>".number_format($_POST['totalValorReal'],2,',','.')."</td>
                    </tr>";    
                ?>
                </table>
            </div>

			<input type="hidden" name="oculto" id="oculto" value="1"/>
            <input type="hidden" name="id_cliente" id="id_cliente" value="<?php echo $_POST['id_cliente'] ?>"/>

			<?php 
				if(@$_POST['oculto']=="2")
				{
                    $año = date('Y');
					preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST['fecha_pago'],$fecha);
                    $fechaPago = "$fecha[3]-$fecha[2]-$fecha[1]";
                    $consecutivo = selconsecutivo('srvrecibo_caja','consecutivo');

                    $sqlCuentaCaja = "SELECT cuentacaja FROM tesoparametros";
                    $resCuentaCaja = mysqli_query($linkbd,$sqlCuentaCaja);
                    $rowCuentaCaja = mysqli_fetch_row($resCuentaCaja);

                    $sql = "SELECT credito FROM srvdetalles_facturacion WHERE numero_facturacion = '$_POST[numeroFactura]' AND id_tipo_cobro = 8";
                    $res = mysqli_query($linkbd,$sql);
                    $row = mysqli_fetch_row($res);

                    if($row[0] > 0)//validar cuando la factura tenga saldo
                    {
                        $sql = "SELECT * FROM srvcortes_detalle WHERE estado_pago = 'P' AND id_cliente = '$_POST[id_cliente]' ORDER BY id DESC LIMIT 1"; //buscamos el ultimo corte que se pago
                        $res = mysqli_query($linkbd,$sql);
                        $row = mysqli_fetch_row($res);

                        if($row[0] != '')//si el cliente ya tiene pagos solo tomara las facturas que tenga vencida y aún no ha pagado
                        {
                            $sql = "SELECT * FROM srvcortes_detalle WHERE estado_pago = 'V' AND id_cliente = '$_POST[id_cliente]' AND id > $row[0] ";//buscamos los cortes vencidos luego del que se pago
                            $res = mysqli_query($linkbd,$sql);
                            while($row = mysqli_fetch_row($res))
                            {
                                $sqlDetalles = "SELECT id_tipo_cobro, credito, debito FROM srvdetalles_facturacion WHERE numero_facturacion = '$row[3]' ";
                                $resDetalles = mysqli_query($linkbd,$sqlDetalles);
                                while($rowDetalles = mysqli_fetch_row($resDetalles))
                                {
                                    switch($rowDetalles[0]) 
                                    {
                                        case 1:
                                            $saldoCargoFijo = $saldoCargoFijo + $rowDetalles[1];
                                            break;
                                        
                                        case 2:
                                            $saldoConsumoBasico = $saldoConsumoBasico + $rowDetalles[1];
                                            break;
    
                                        case 3:
                                            $saldoConsumoConLectura = $saldoConsumoConLectura + $rowDetalles[1];
                                            break;
    
                                        case 4:
                                            $saldoOtroCobro = $saldoOtroCobro + $rowDetalles[1];
                                            break;
    
                                        case 7:
                                            $saldoContribucion = $saldoContribucion + $rowDetalles[1];
                                            break;

                                        case 9: 
                                            $saldoAcuerdos = $saldoAcuerdos + $rowDetalles[1];
                                            break;

                                        case 10:
                                            $saldoInicial = $saldoInicial + $rowDetalles[1];
                                            break;
                                            
                                        case 11:
                                            $saldoMora = $saldoMora + $rowDetalles[1];
                                    }
                                }
                            }
                        }
                        else //Si el cliente no ha tenido ningun pago el programa tomara todas las facturas vencidas
                        {
                            $sql = "SELECT * FROM srvcortes_detalle WHERE estado_pago = 'V' AND id_cliente = '$_POST[id_cliente]'";//buscamos los cortes vencidos luego del que se pago
                            $res = mysqli_query($linkbd,$sql);
                            while($row = mysqli_fetch_row($res))
                            {
                                $sqlDetalles = "SELECT id_tipo_cobro, credito, debito FROM srvdetalles_facturacion WHERE numero_facturacion = '$row[3]' ";
                                $resDetalles = mysqli_query($linkbd,$sqlDetalles);
                                while($rowDetalles = mysqli_fetch_row($resDetalles))
                                {
                                    switch($rowDetalles[0]) 
                                    {
                                        case 1:
                                            $saldoCargoFijo = $saldoCargoFijo + $rowDetalles[1];
                                            break;
                                        
                                        case 2:
                                            $saldoConsumoBasico = $saldoConsumoBasico + $rowDetalles[1];
                                            break;
    
                                        case 3:
                                            $saldoConsumoConLectura = $saldoConsumoConLectura + $rowDetalles[1];
                                            break;
    
                                        case 4:
                                            $saldoOtroCobro = $saldoOtroCobro + $rowDetalles[1];
                                            break;

                                        case 5:
                                            $saldoSubsidio = $saldoSubsidio + $rowDetalles[2];
                                            break;
    
                                        case 7:
                                            $saldoContribucion = $saldoContribucion + $rowDetalles[1];
                                            break;
                                        
                                        case 9:
                                            $saldoAcuerdos = $saldoAcuerdos + $rowDetalles[1];
                                            break;
                                        
                                        case 10:
                                            $saldoInicial = $saldoInicial + $rowDetalles[1];
                                            break;
                                        
                                        case 11: 
                                            $saldoMora = $saldoMora + $rowDetalles[1];
                                            break;
                                    }
                                }
                            }
                        }
                    }

                    $sqlConsultaTipoCobro = "SELECT id_servicio, id_tipo_cobro, credito, debito FROM srvdetalles_facturacion WHERE corte = $_POST[corte] AND numero_facturacion = '$_POST[numeroFactura]' AND tipo_movimiento = '101' ORDER BY id_tipo_cobro";
                    $resConsultaTipoCobro = mysqli_query($linkbd,$sqlConsultaTipoCobro);

                    while($rowConsultaTipoCobro = mysqli_fetch_row($resConsultaTipoCobro))
                    {
                        if($rowConsultaTipoCobro[2] > 0)
                        {
                            $sql = "INSERT INTO srvdetalles_facturacion (corte, id_cliente, numero_facturacion, tipo_movimiento, fecha_movimiento, id_servicio, id_tipo_cobro, credito, debito, saldo, estado) VALUES ('$_POST[corte]', '$_POST[id_cliente]', '$_POST[numeroFactura]', '$_POST[tipoMovimiento]', '$fechaPago', $rowConsultaTipoCobro[0], $rowConsultaTipoCobro[1], 0, $rowConsultaTipoCobro[2], 0, 'S')";
                            mysqli_query($linkbd,$sql);
                        }
                        elseif($rowConsultaTipoCobro[3] > 0)
                        {
                            $sql = "INSERT INTO srvdetalles_facturacion (corte, id_cliente, numero_facturacion, tipo_movimiento, fecha_movimiento, id_servicio, id_tipo_cobro, credito, debito, saldo, estado) VALUES ('$_POST[corte]', '$_POST[id_cliente]', '$_POST[numeroFactura]', '$_POST[tipoMovimiento]', '$fechaPago', $rowConsultaTipoCobro[0], $rowConsultaTipoCobro[1], $rowConsultaTipoCobro[3], 0, 0, 'S')";
                            mysqli_query($linkbd,$sql);
                        }

                        $codigoComprobante = '30';

                        //Casos de tipo de cobro 
                        switch ($rowConsultaTipoCobro[1]) 
                        {
                            case 1: //comprobante det cargo fijo
                                $cargoFijo = $rowConsultaTipoCobro[2] + $saldoCargoFijo;

                                $sqlServicio = "SELECT concepto,cc FROM srvservicios WHERE id = '$rowConsultaTipoCobro[0]' ";
                                $resServicio = mysqli_query($linkbd,$sqlServicio);
                                $rowServicio = mysqli_fetch_row($resServicio);

                                $sqlCliente = "SELECT id_tercero FROM srvclientes WHERE id = '$_POST[id_cliente]' ";
                                $resCliente = mysqli_query($linkbd,$sqlCliente);
                                $rowCliente = mysqli_fetch_row($resCliente);

                                $sqlTerceros = "SELECT cedulanit FROM terceros WHERE id_tercero = '$rowCliente[0]' ";
                                $resTerceros = mysqli_query($linkbd,$sqlTerceros);
                                $rowTerceros = mysqli_fetch_row($resTerceros);

                                $concepto = concepto_cuentasn2($rowServicio[0],'SS',10,$rowServicio[1],$fechaPago);

                                for($z=0; $z<count($concepto); $z++)
                                {
                                    if($concepto[$z][3] == 'S' and $concepto[$z][2] == 'N')
                                    {
                                        //credito la cuenta credito va contra la caja o banco depende que elija la persona
                                        if($_POST['modoRecaudo'] == 'caja')
                                        {
                                            $sqlr="INSERT INTO comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque,valdebito, valcredito,estado,vigencia) VALUES ('$codigoComprobante $consecutivo', '$rowCuentaCaja[0]','$rowTerceros[0]','$rowServicio[1]','Pago factura $_POST[numeroFactura] del corte $_POST[corte] Cargo Fijo', '', '$cargoFijo', 0, '1', $fecha[3])";
                                            mysqli_query($linkbd,$sqlr);
                                        }
                                        elseif($_POST['modoRecaudo'] == 'banco')
                                        {
                                            $sqlr="INSERT INTO comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque,valdebito, valcredito,estado,vigencia) VALUES ('$codigoComprobante $consecutivo', '$_POST[cuentaBancaria]','$rowTerceros[0]','$rowServicio[1]','Pago factura $_POST[numeroFactura] del corte $_POST[corte] Cargo Fijo', '', '$cargoFijo', 0, '1', $fecha[3])";                                            mysqli_query($linkbd,$sqlr);
                                        }
                                    }
                                    if ($concepto[$z][3] == 'N' and $concepto[$z][2] == 'S') 
                                    {
                                        //debito va con la cuenta que trae de la parametrización
                                        $sqlr="INSERT INTO comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque,valdebito, valcredito,estado,vigencia) VALUES ('$codigoComprobante $consecutivo','".$concepto[$z][0]."','$rowTerceros[0]','$rowServicio[1]','Pago factura $_POST[numeroFactura] del corte $_POST[corte] Cargo Fijo', '', 0, '$cargoFijo', '1' ,'$fecha[3]')";
                                        mysqli_query($linkbd,$sqlr);
                                    }
                                }
                            break;
                            
                            case 2: //comprobante det consumo basico
                                $consumoBasico = $rowConsultaTipoCobro[2] + $saldoConsumoBasico;

                                $sqlCliente = "SELECT id_tercero, id_estrato FROM srvclientes WHERE id = '$_POST[id_cliente]' ";
                                $resCliente = mysqli_query($linkbd,$sqlCliente);
                                $rowCliente = mysqli_fetch_row($resCliente);

                                $sqlTerceros = "SELECT cedulanit FROM terceros WHERE id_tercero = '$rowCliente[0]' ";
                                $resTerceros = mysqli_query($linkbd,$sqlTerceros);
                                $rowTerceros = mysqli_fetch_row($resTerceros);

                                $sqlServicio = "SELECT concepto_contable,centro_costo FROM srvcostos_estandar WHERE id_servicio = '$rowConsultaTipoCobro[0]' AND id_estrato = '$rowCliente[1]'";
                                $resServicio = mysqli_query($linkbd,$sqlServicio);
                                $rowServicio = mysqli_fetch_row($resServicio);

                                $concepto = concepto_cuentasn2($rowServicio[0],'CB',10,$rowServicio[1],$fechaPago);
                                
                                for($z=0; $z<count($concepto); $z++)
                                {
                                    if($concepto[$z][3] == 'S' AND $concepto[$z][2] == 'N')
                                    {
                                        if($_POST['modoRecaudo'] == 'caja')
                                        {
                                            $sqlr="INSERT INTO comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque,valdebito, valcredito,estado,vigencia) VALUES ('$codigoComprobante $consecutivo', '$rowCuentaCaja[0]','$rowTerceros[0]','$rowServicio[1]','Pago factura $_POST[numeroFactura] del corte $_POST[corte] Consumo Basico', '', '$consumoBasico', 0, '1', $fecha[3])";
                                            mysqli_query($linkbd,$sqlr);
                                        }
                                        elseif($_POST['modoRecaudo'] == 'banco')
                                        {
                                            $sqlr="INSERT INTO comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque,valdebito, valcredito,estado,vigencia) VALUES ('$codigoComprobante $consecutivo', '$_POST[cuentaBancaria]','$rowTerceros[0]','$rowServicio[1]','Pago factura $_POST[numeroFactura] del corte $_POST[corte] Consumo Basico', '', '$consumoBasico', 0, '1', $fecha[3])";
                                            mysqli_query($linkbd,$sqlr);
                                        }
                                    }
                                    if ($concepto[$z][3] == 'N' and $concepto[$z][2] == 'S') 
                                    {
                                        $sqlr="INSERT INTO comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque,valdebito, valcredito,estado,vigencia) VALUES ('$codigoComprobante $consecutivo','".$concepto[$z][0]."','$rowTerceros[0]','$rowServicio[1]','Pago factura $_POST[numeroFactura] del corte $_POST[corte] Consumo Basico', '', 0, '$consumoBasico', '1' ,'$fecha[3]')";
                                        mysqli_query($linkbd,$sqlr);
                                    }
                                }
                                break;

                            case 3://comprobante det consumo con lectura
                                $consumoConLectura = $rowConsultaTipoCobro[2] + $saldoConsumoConLectura;
                                
                                $sqlCliente = "SELECT id_tercero, id_estrato FROM srvclientes WHERE id = '$_POST[id_cliente]' ";
                                $resCliente = mysqli_query($linkbd,$sqlCliente);
                                $rowCliente = mysqli_fetch_row($resCliente);

                                $sqlTerceros = "SELECT cedulanit FROM terceros WHERE id_tercero = '$rowCliente[0]' ";
                                $resTerceros = mysqli_query($linkbd,$sqlTerceros);
                                $rowTerceros = mysqli_fetch_row($resTerceros);

                                $sqlServicio = "SELECT concepto_contable,centro_costo FROM srvtarifas WHERE id_servicio = '$rowConsultaTipoCobro[0]' AND id_estrato = '$rowCliente[1]' ";
                                $resServicio = mysqli_query($linkbd,$sqlServicio);
                                $rowServicio = mysqli_fetch_row($resServicio);

                                $concepto = concepto_cuentasn2($rowServicio[0],'CL',10,$rowServicio[1],$fechaPago);

                                for($z=0; $z<count($concepto); $z++)
                                {
                                    if($concepto[$z][3] == 'S' AND $concepto[$z][2] == 'N')
                                    {
                                        if($_POST['modoRecaudo'] == 'caja')
                                        {
                                            $sqlr="INSERT INTO comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque,valdebito, valcredito,estado,vigencia) VALUES ('$codigoComprobante $consecutivo', '$rowCuentaCaja[0]','$rowTerceros[0]','$rowServicio[1]','Pago factura $_POST[numeroFactura] del corte $_POST[corte] Consumo con Lectura', '', '$consumoConLectura', 0, '1', $fecha[3])";
                                            mysqli_query($linkbd,$sqlr);
                                        }
                                        elseif($_POST['modoRecaudo'] == 'banco')
                                        {
                                            $sqlr="INSERT INTO comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque,valdebito, valcredito,estado,vigencia) VALUES ('$codigoComprobante $consecutivo', '$_POST[cuentaBancaria]','$rowTerceros[0]','$rowServicio[1]','Pago factura $_POST[numeroFactura] del corte $_POST[corte] Consumo con Lectura', '', '$consumoConLectura', 0, '1', $fecha[3])";
                                            mysqli_query($linkbd,$sqlr);
                                        }
                                    }
                                    if ($concepto[$z][3] == 'N' and $concepto[$z][2] == 'S') 
                                    {
                                        $sqlr="INSERT INTO comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque,valdebito, valcredito,estado,vigencia) VALUES ('$codigoComprobante $consecutivo','".$concepto[$z][0]."','$rowTerceros[0]','$rowServicio[1]','Pago factura $_POST[numeroFactura] del corte $_POST[corte] Consumo con Lectura', '', 0, '$consumoConLectura', '1' ,'$fecha[3]')";
                                        mysqli_query($linkbd,$sqlr);
                                    }
                                }
                                break;

                            case 4://comprobante det otros cobros
                                $otroCobro = $rowConsultaTipoCobro[2] + $saldoOtroCobro;

                                $sqlAsignacionOtroCobro = "SELECT id_otro_cobro FROM srvasignacion_otroscobros WHERE id_cliente = '$_POST[id_cliente]' ";
                                $resAsignacionOtroCobro = mysqli_query($linkbd,$sqlAsignacionOtroCobro);
                                $rowAsignacionOtroCobro = mysqli_fetch_row($resAsignacionOtroCobro);

                                $sqlOtroCobro = "SELECT concepto FROM srvotroscobros WHERE id = '$rowAsignacionOtroCobro[0]' ";
                                $resOtroCobro = mysqli_query($linkbd,$sqlOtroCobro);
                                $rowOtroCobro = mysqli_fetch_row($resOtroCobro);

                                $sqlServicio = "SELECT cc FROM srvservicios WHERE id = '$rowConsultaTipoCobro[0]' ";
                                $resServicio = mysqli_query($linkbd,$sqlServicio);
                                $rowServicio = mysqli_fetch_row($resServicio);

                                $sqlCliente = "SELECT id_tercero FROM srvclientes WHERE id = '$_POST[id_cliente]' ";
                                $resCliente = mysqli_query($linkbd,$sqlCliente);
                                $rowCliente = mysqli_fetch_row($resCliente);

                                $sqlTerceros = "SELECT cedulanit FROM terceros WHERE id_tercero = '$rowCliente[0]' ";
                                $resTerceros = mysqli_query($linkbd,$sqlTerceros);
                                $rowTerceros = mysqli_fetch_row($resTerceros);

                                $concepto = concepto_cuentasn2($rowOtroCobro[0],'SO',10,$rowServicio[0],$fechaPago);

                                for($z=0; $z<count($concepto); $z++)
                                {
                                    if($concepto[$z][3] == 'S' and $concepto[$z][2] == 'N')
                                    {
                                        //credito la cuenta credito va contra la caja o banco depende que elija la persona
                                        if($_POST['modoRecaudo'] == 'caja')
                                        {
                                            $sqlr="INSERT INTO comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque,valdebito, valcredito,estado,vigencia) VALUES ('$codigoComprobante $consecutivo', '$rowCuentaCaja[0]','$rowTerceros[0]','$rowServicio[0]','Pago factura $_POST[numeroFactura] del corte $_POST[corte] Otro Cobro', '', '$otroCobro', 0, '1', $fecha[3])";
                                            mysqli_query($linkbd,$sqlr);
                                        }
                                        elseif($_POST['modoRecaudo'] == 'banco')
                                        {
                                            $sqlr="INSERT INTO comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque,valdebito, valcredito,estado,vigencia) VALUES ('$codigoComprobante $consecutivo', '$_POST[cuentaBancaria]','$rowTerceros[0]','$rowServicio[0]','Pago factura $_POST[numeroFactura] del corte $_POST[corte] Otro Cobro', '', '$otroCobro', 0, '1', $fecha[3])";
                                            mysqli_query($linkbd,$sqlr);
                                        }
                                    }
                                    if ($concepto[$z][3] == 'N' and $concepto[$z][2] == 'S') 
                                    {
                                        //debito va con la cuenta que trae de la parametrización
                                        $sqlr="INSERT INTO comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque,valdebito, valcredito,estado,vigencia) VALUES ('$codigoComprobante $consecutivo','".$concepto[$z][0]."','$rowTerceros[0]','$rowServicio[0]','Pago factura $_POST[numeroFactura] del corte $_POST[corte] Otro Cobro', '', 0, '$otroCobro', '1' ,'$fecha[3]')";
                                        mysqli_query($linkbd,$sqlr);
                                    }
                                }
                                break;

                            case 5://comprobante det subsidios
                                    $subsidio = $rowConsultaTipoCobro[3] + $saldoSubsidio;

                                    $sqlCliente = "SELECT id_tercero,id_estrato FROM srvclientes WHERE id = '$_POST[id_cliente]' ";
									$resCliente = mysqli_query($linkbd,$sqlCliente);
									$rowCliente = mysqli_fetch_row($resCliente);

									$sqlTerceros = "SELECT cedulanit FROM terceros WHERE id_tercero = '$rowCliente[0]' ";
									$resTerceros = mysqli_query($linkbd,$sqlTerceros);
									$rowTerceros = mysqli_fetch_row($resTerceros);

									$sqlServicio = "SELECT cc FROM srvservicios WHERE id = '$rowConsultaTipoCobro[0]' ";
									$resServicio = mysqli_query($linkbd,$sqlServicio);
									$rowServicio = mysqli_fetch_row($resServicio);
									
									$sqlSubsidio = "SELECT concepto FROM srvsubsidios WHERE id_estrato = '$rowCliente[1]' AND vigencia = '$año'";
									$resSubsidio = mysqli_query($linkbd,$sqlSubsidio);
									$rowSubsidio = mysqli_fetch_row($resSubsidio);

									$concepto = concepto_cuentasn2($rowSubsidio[0],'SB',10,$rowServicio[0],$fechaPago);
                                    $cuentaTrece = $concepto[1][0];
                                    
									for($z=0; $z<count($concepto); $z++)
									{
										if($concepto[$z][3] == 'S' and $concepto[$z][2] == 'N')
										{
                                            //aquí la banco o caja
                                            if($_POST['modoRecaudo'] == 'caja')
                                            {
                                                $sqlr="INSERT INTO comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque,valdebito,valcredito,estado,vigencia) VALUES ('$codigoComprobante $consecutivo', '$rowCuentaCaja[0]','$rowTerceros[0]','$rowServicio[0]','Pago factura $_POST[numeroFactura] del corte $_POST[corte] Subsidio', '', 0, '$subsidio', '1', $fecha[3])";
                                                mysqli_query($linkbd,$sqlr);
                                            }
                                            elseif($_POST['modoRecaudo'] == 'banco')
                                            {
                                                $sqlr="INSERT INTO comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque,valdebito,valcredito,estado,vigencia) VALUES ('$codigoComprobante $consecutivo', '$_POST[cuentaBancaria]','$rowTerceros[0]','$rowServicio[1]','Pago factura $_POST[numeroFactura] del corte $_POST[corte] Subsidio', '', 0, '$subsidio', '1', $fecha[3])";
                                                mysqli_query($linkbd,$sqlr);
                                            }
										}
										if ($concepto[$z][3] == 'N' and $concepto[$z][2] == 'S') 
										{
                                            //aqui va le cuenta 13
											$sqlr="INSERT INTO comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque,valdebito,valcredito,estado,vigencia) VALUES ('$codigoComprobante $consecutivo', '$cuentaTrece','$rowTerceros[0]','$rowServicio[0]','Pago factura $_POST[numeroFactura] del corte $_POST[corte] Subsidio', '', '$subsidio', 0, '1', $fecha[3])";
											mysqli_query($linkbd,$sqlr);
										}
									}
                                break;

                            case 7: //comprobante det contribución
                                $contribucion = $rowConsultaTipoCobro[2] + $saldoContribucion;

                                $sqlCliente = "SELECT id_tercero,id_estrato FROM srvclientes WHERE id = '$_POST[id_cliente]' ";
                                $resCliente = mysqli_query($linkbd,$sqlCliente);
                                $rowCliente = mysqli_fetch_row($resCliente);

                                $sqlTerceros = "SELECT cedulanit FROM terceros WHERE id_tercero = '$rowCliente[0]' ";
                                $resTerceros = mysqli_query($linkbd,$sqlTerceros);
                                $rowTerceros = mysqli_fetch_row($resTerceros);

                                $sqlServicio = "SELECT cc FROM srvservicios WHERE id = '$rowConsultaTipoCobro[0]' ";
                                $resServicio = mysqli_query($linkbd,$sqlServicio);
                                $rowServicio = mysqli_fetch_row($resServicio);

                                $sqlContribuciones = "SELECT concepto FROM srvcontribuciones WHERE id_estrato = '$rowCliente[1]' ";
                                $resContribuciones = mysqli_query($linkbd,$sqlContribuciones);
                                $rowContribuciones = mysqli_fetch_row($resContribuciones);

                                $concepto = concepto_cuentasn2($rowContribuciones[0],'SC',10,$rowServicio[0],$fechaPago);

                                for($z=0; $z<count($concepto); $z++)
                                {
                                    if($concepto[$z][3] == 'S' and $concepto[$z][2] == 'N')
                                    {
                                        //credito la cuenta credito va contra la caja o banco depende que elija la persona
                                        if($_POST['modoRecaudo'] == 'caja')
                                        {
                                            $sqlr="INSERT INTO comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque,valdebito, valcredito,estado,vigencia) VALUES ('$codigoComprobante $consecutivo', '$rowCuentaCaja[0]','$rowTerceros[0]','$rowServicio[0]','Pago factura $_POST[numeroFactura] del corte $_POST[corte] Contribucion', '', '$contribucion', 0, '1', $fecha[3])";
                                            mysqli_query($linkbd,$sqlr);
                                        }
                                        elseif($_POST['modoRecaudo'] == 'banco')
                                        {
                                            $sqlr="INSERT INTO comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque,valdebito, valcredito,estado,vigencia) VALUES ('$codigoComprobante $consecutivo', '$_POST[cuentaBancaria]','$rowTerceros[0]','$rowServicio[0]','Pago factura $_POST[numeroFactura] del corte $_POST[corte] Contribucion', '', '$contribucion', 0, '1', $fecha[3])";
                                            mysqli_query($linkbd,$sqlr);
                                        }
                                    }
                                    if ($concepto[$z][3] == 'N' and $concepto[$z][2] == 'S') 
                                    {
                                        //debito va con la cuenta que trae de la parametrización
                                        $sqlr="INSERT INTO comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque,valdebito, valcredito,estado,vigencia) VALUES ('$codigoComprobante $consecutivo','".$concepto[$z][0]."','$rowTerceros[0]','$rowServicio[0]','Pago factura $_POST[numeroFactura] del corte $_POST[corte] Contribucion', '', 0, '$contribucion', '1' ,'$fecha[3]')";
                                        mysqli_query($linkbd,$sqlr);
                                    }
                                }

                                break;

                            case 8:
                                if($saldoInicial != 0)
                                {
                                    $deudaInicial = $saldoInicial;

                                    $sqlServicio = "SELECT concepto,cc FROM srvservicios WHERE id = '$rowConsultaTipoCobro[0]' ";
                                    $resServicio = mysqli_query($linkbd,$sqlServicio);
                                    $rowServicio = mysqli_fetch_row($resServicio);

                                    $sqlCliente = "SELECT id_tercero FROM srvclientes WHERE id = '$_POST[id_cliente]' ";
                                    $resCliente = mysqli_query($linkbd,$sqlCliente);
                                    $rowCliente = mysqli_fetch_row($resCliente);

                                    $sqlTerceros = "SELECT cedulanit FROM terceros WHERE id_tercero = '$rowCliente[0]' ";
                                    $resTerceros = mysqli_query($linkbd,$sqlTerceros);
                                    $rowTerceros = mysqli_fetch_row($resTerceros);

                                    $concepto = concepto_cuentasn2($rowServicio[0],'SS',10,$rowServicio[1],$fechaPago);

                                    for($z=0; $z<count($concepto); $z++)
                                        {
                                            if($concepto[$z][3] == 'S' and $concepto[$z][2] == 'N')
                                            {
                                                //credito la cuenta credito va contra la caja o banco depende que elija la persona
                                                if($_POST['modoRecaudo'] == 'caja')
                                                {
                                                    $sqlr="INSERT INTO comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque,valdebito, valcredito,estado,vigencia) VALUES ('$codigoComprobante $consecutivo', '$rowCuentaCaja[0]','$rowTerceros[0]','$rowServicio[1]','Pago factura $_POST[numeroFactura] del corte $_POST[corte] Saldo Inicial', '', '$deudaInicial', 0, '1', $fecha[3])";
                                                    mysqli_query($linkbd,$sqlr);
                                                }
                                                elseif($_POST['modoRecaudo'] == 'banco')
                                                {
                                                    $sqlr="INSERT INTO comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque,valdebito, valcredito,estado,vigencia) VALUES ('$codigoComprobante $consecutivo', '$_POST[cuentaBancaria]','$rowTerceros[0]','$rowServicio[1]','Pago factura $_POST[numeroFactura] del corte $_POST[corte] Saldo Inicial', '', '$deudaInicial', 0, '1', $fecha[3])";
                                                    mysqli_query($linkbd,$sqlr);
                                                }
                                            }
                                            if ($concepto[$z][3] == 'N' and $concepto[$z][2] == 'S') 
                                            {
                                                //debito va con la cuenta que trae de la parametrización
                                                $sqlr="INSERT INTO comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque,valdebito, valcredito,estado,vigencia) VALUES ('$codigoComprobante $consecutivo','".$concepto[$z][0]."','$rowTerceros[0]','$rowServicio[1]','Pago factura $_POST[numeroFactura] del corte $_POST[corte] Saldo Inicial', '', 0, '$deudaInicial', '1' ,'$fecha[3]')";
                                                mysqli_query($linkbd,$sqlr);
                                            }
                                        }
                                }
                                break;
                            
                            case 9: //comprobante det acuerdos de pago
                                //va la cuenta puente y la cuenta de caja o banco
                                $acuerdoPago = $rowConsultaTipoCobro[2] + $saldoAcuerdos;

                                $sqlServicio = "SELECT concepto,cc FROM srvservicios WHERE id = '$rowConsultaTipoCobro[0]' ";
                                $resServicio = mysqli_query($linkbd,$sqlServicio);
                                $rowServicio = mysqli_fetch_row($resServicio);

                                $sqlCliente = "SELECT id_tercero FROM srvclientes WHERE id = '$_POST[id_cliente]' ";
                                $resCliente = mysqli_query($linkbd,$sqlCliente);
                                $rowCliente = mysqli_fetch_row($resCliente);

                                $sqlTerceros = "SELECT cedulanit FROM terceros WHERE id_tercero = '$rowCliente[0]' ";
                                $resTerceros = mysqli_query($linkbd,$sqlTerceros);
                                $rowTerceros = mysqli_fetch_row($resTerceros);

                                $cuentapp = concepto_cuentasn2($rowServicio[0],'AP',10,$rowServicio[1],$fechaPago);
                                $cuentaPuente = $cuentapp[0][0];
 
                                if($_POST['modoRecaudo'] == 'caja')
                                {
                                    $sqlr="INSERT INTO comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque,valdebito, valcredito,estado,vigencia) VALUES ('$codigoComprobante $consecutivo', '$rowCuentaCaja[0]','$rowTerceros[0]','$rowServicio[1]','Pago factura $_POST[numeroFactura] del corte $_POST[corte] Acuerdo de Pago', '', '$acuerdoPago', 0, '1', $fecha[3])";
                                    mysqli_query($linkbd,$sqlr);
                                }
                                elseif($_POST['modoRecaudo'] == 'banco')
                                {
                                    $sqlr="INSERT INTO comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque,valdebito, valcredito,estado,vigencia) VALUES ('$codigoComprobante $consecutivo', '$_POST[cuentaBancaria]','$rowTerceros[0]','$rowServicio[1]','Pago factura $_POST[numeroFactura] del corte $_POST[corte] Acuerdo de Pago', '', '$acuerdoPago', 0, '1', $fecha[3])";
                                    mysqli_query($linkbd,$sqlr);
                                }
                            
                                
                                $sqlr="INSERT INTO comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque,valdebito, valcredito,estado,vigencia) VALUES ('$codigoComprobante $consecutivo','".$cuentaPuente."','$rowTerceros[0]','$rowServicio[1]','Pago factura $_POST[numeroFactura] del corte $_POST[corte] Acuerdo de Pago', '', 0, '$acuerdoPago', '1' ,'$fecha[3]')";
                                mysqli_query($linkbd,$sqlr);   
                                break;

                            case 10:
                                $deudaInicial = $rowConsultaTipoCobro[2];

                                $sqlServicio = "SELECT concepto,cc FROM srvservicios WHERE id = '$rowConsultaTipoCobro[0]' ";
                                $resServicio = mysqli_query($linkbd,$sqlServicio);
                                $rowServicio = mysqli_fetch_row($resServicio);

                                $sqlCliente = "SELECT id_tercero FROM srvclientes WHERE id = '$_POST[id_cliente]' ";
                                $resCliente = mysqli_query($linkbd,$sqlCliente);
                                $rowCliente = mysqli_fetch_row($resCliente);

                                $sqlTerceros = "SELECT cedulanit FROM terceros WHERE id_tercero = '$rowCliente[0]' ";
                                $resTerceros = mysqli_query($linkbd,$sqlTerceros);
                                $rowTerceros = mysqli_fetch_row($resTerceros);

                                $concepto = concepto_cuentasn2($rowServicio[0],'SS',10,$rowServicio[1],$fechaPago);

                                for($z=0; $z<count($concepto); $z++)
                                    {
                                        if($concepto[$z][3] == 'S' and $concepto[$z][2] == 'N')
                                        {
                                            //credito la cuenta credito va contra la caja o banco depende que elija la persona
                                            if($_POST['modoRecaudo'] == 'caja')
                                            {
                                                $sqlr="INSERT INTO comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque,valdebito, valcredito,estado,vigencia) VALUES ('$codigoComprobante $consecutivo', '$rowCuentaCaja[0]','$rowTerceros[0]','$rowServicio[1]','Pago factura $_POST[numeroFactura] del corte $_POST[corte] Saldo Inicial', '', '$deudaInicial', 0, '1', $fecha[3])";
                                                mysqli_query($linkbd,$sqlr);
                                            }
                                            elseif($_POST['modoRecaudo'] == 'banco')
                                            {
                                                $sqlr="INSERT INTO comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque,valdebito, valcredito,estado,vigencia) VALUES ('$codigoComprobante $consecutivo', '$_POST[cuentaBancaria]','$rowTerceros[0]','$rowServicio[1]','Pago factura $_POST[numeroFactura] del corte $_POST[corte] Saldo Inicial', '', '$deudaInicial', 0, '1', $fecha[3])";
                                                mysqli_query($linkbd,$sqlr);
                                            }
                                        }
                                        if ($concepto[$z][3] == 'N' and $concepto[$z][2] == 'S') 
                                        {
                                            //debito va con la cuenta que trae de la parametrización
                                            $sqlr="INSERT INTO comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque,valdebito, valcredito,estado,vigencia) VALUES ('$codigoComprobante $consecutivo','".$concepto[$z][0]."','$rowTerceros[0]','$rowServicio[1]','Pago factura $_POST[numeroFactura] del corte $_POST[corte] Saldo Inicial', '', 0, '$deudaInicial', '1' ,'$fecha[3]')";
                                            mysqli_query($linkbd,$sqlr);
                                        }
                                    }
                                break;
                            
                            case 11:
                                $interesMora = $rowConsultaTipoCobro[2] + $saldoInteresMora;
                                
                                $sqlServicio = "SELECT concepto,cc FROM srvservicios WHERE id = '$rowConsultaTipoCobro[0]' ";
                                $resServicio = mysqli_query($linkbd,$sqlServicio);
                                $rowServicio = mysqli_fetch_row($resServicio);

                                $sqlCliente = "SELECT id_tercero FROM srvclientes WHERE id = '$_POST[id_cliente]' ";
                                $resCliente = mysqli_query($linkbd,$sqlCliente);
                                $rowCliente = mysqli_fetch_row($resCliente);

                                $sqlTerceros = "SELECT cedulanit FROM terceros WHERE id_tercero = '$rowCliente[0]' ";
                                $resTerceros = mysqli_query($linkbd,$sqlTerceros);
                                $rowTerceros = mysqli_fetch_row($resTerceros);

                                $concepto = concepto_cuentasn2($rowServicio[0],'SM',10,$rowServicio[1],$fechaPago);

                                //credito la cuenta credito va contra la caja o banco depende que elija la persona
                                if($_POST['modoRecaudo'] == 'caja')
                                {
                                    $sqlr="INSERT INTO comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque,valdebito, valcredito,estado,vigencia) VALUES ('$codigoComprobante $consecutivo', '$rowCuentaCaja[0]','$rowTerceros[0]','$rowServicio[1]','Pago factura $_POST[numeroFactura] del corte $_POST[corte] Interes Mora', '', '$interesMora', 0, '1', $fecha[3])";
                                    mysqli_query($linkbd,$sqlr);
                                }
                                elseif($_POST['modoRecaudo'] == 'banco')
                                {
                                    $sqlr="INSERT INTO comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque,valdebito, valcredito,estado,vigencia) VALUES ('$codigoComprobante $consecutivo', '$_POST[cuentaBancaria]','$rowTerceros[0]','$rowServicio[1]','Pago factura $_POST[numeroFactura] del corte $_POST[corte] Interes Mora', '', '$interesMora', 0, '1', $fecha[3])";
                                    mysqli_query($linkbd,$sqlr);
                                }
                               
                                //debito va con la cuenta que trae de la parametrización
                                $sqlr="INSERT INTO comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque,valdebito, valcredito,estado,vigencia) VALUES ('$codigoComprobante $consecutivo','".$concepto[0][0]."','$rowTerceros[0]','$rowServicio[1]','Pago factura $_POST[numeroFactura] del corte $_POST[corte] Interes Mora', '', 0, '$interesMora', '1' ,'$fecha[3]')";
                                mysqli_query($linkbd,$sqlr);
                               
                                break;    
                        }

                        if($rowConsultaTipoCobro[1] != 1 && $rowConsultaTipoCobro[1] != 2 && $rowConsultaTipoCobro[1] != 3 && $rowConsultaTipoCobro[1] != 4 && $rowConsultaTipoCobro[1] != 7 && $saldoOtroCobro > 0)//Otros cobros no es un pago que aparece mensualmente entonces en dado caso que la persona no haya pagado un pago de otro cobro y lo este pagando este mes con este if comprueba que no exista cobro de este mes con 7 y que si haya un saldo de otro cobro
                        {
                            $otroCobro = $saldoOtroCobro;

                            $sqlAsignacionOtroCobro = "SELECT id_otro_cobro FROM srvasignacion_otroscobros WHERE id_cliente = '$_POST[id_cliente]' ";
                            $resAsignacionOtroCobro = mysqli_query($linkbd,$sqlAsignacionOtroCobro);
                            $rowAsignacionOtroCobro = mysqli_fetch_row($resAsignacionOtroCobro);

                            $sqlOtroCobro = "SELECT concepto FROM srvotroscobros WHERE id = '$rowAsignacionOtroCobro[0]' ";
                            $resOtroCobro = mysqli_query($linkbd,$sqlOtroCobro);
                            $rowOtroCobro = mysqli_fetch_row($resOtroCobro);

                            $sqlServicio = "SELECT cc FROM srvservicios WHERE id = '$rowConsultaTipoCobro[0]' ";
                            $resServicio = mysqli_query($linkbd,$sqlServicio);
                            $rowServicio = mysqli_fetch_row($resServicio);

                            $sqlCliente = "SELECT id_tercero FROM srvclientes WHERE id = '$_POST[id_cliente]' ";
                            $resCliente = mysqli_query($linkbd,$sqlCliente);
                            $rowCliente = mysqli_fetch_row($resCliente);

                            $sqlTerceros = "SELECT cedulanit FROM terceros WHERE id_tercero = '$rowCliente[0]' ";
                            $resTerceros = mysqli_query($linkbd,$sqlTerceros);
                            $rowTerceros = mysqli_fetch_row($resTerceros);

                            $concepto = concepto_cuentasn2($rowOtroCobro[0],'SO',10,$rowServicio[0],$fechaPago);

                            for($z=0; $z<count($concepto); $z++)
                            {
                                if($concepto[$z][3] == 'S' and $concepto[$z][2] == 'N')
                                {
                                    //credito la cuenta credito va contra la caja o banco depende que elija la persona
                                    if($_POST['modoRecaudo'] == 'caja')
                                    {
                                        $sqlr="INSERT INTO comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque,valdebito, valcredito,estado,vigencia) VALUES ('$codigoComprobante $consecutivo', '$rowCuentaCaja[0]','$rowTerceros[0]','$rowServicio[0]','Pago factura $_POST[numeroFactura] del corte $_POST[corte] Otro Cobro', '', '$otroCobro', 0, '1', $fecha[3])";
                                        mysqli_query($linkbd,$sqlr);
                                    }
                                    elseif($_POST['modoRecaudo'] == 'banco')
                                    {
                                        $sqlr="INSERT INTO comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque,valdebito, valcredito,estado,vigencia) VALUES ('$codigoComprobante $consecutivo', '$_POST[cuentaBancaria]','$rowTerceros[0]','$rowServicio[0]','Pago factura $_POST[numeroFactura] del corte $_POST[corte] Otro Cobro', '', '$otroCobro', 0, '1', $fecha[3])";
                                        mysqli_query($linkbd,$sqlr);
                                    }
                                }
                                if ($concepto[$z][3] == 'N' and $concepto[$z][2] == 'S') 
                                {
                                    //debito va con la cuenta que trae de la parametrización
                                    $sqlr="INSERT INTO comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque,valdebito, valcredito,estado,vigencia) VALUES ('$codigoComprobante $consecutivo','".$concepto[$z][0]."','$rowTerceros[0]','$rowServicio[1]','Pago factura $_POST[numeroFactura] del corte $_POST[corte] Otro Cobro', '', 0, '$otroCobro', '1' ,'$fecha[3]')";
                                    mysqli_query($linkbd,$sqlr);
                                }
                            }
                        }
                    }
                    
                    $totalCabecera = $cargoFijo + $consumoBasico + $consumoConLectura + $otroCobro + $contribucion;

                    $sqlComprobanteCabecera = "INSERT INTO comprobante_cab (numerotipo, tipo_comp, fecha, concepto, total_debito, total_credito, estado) VALUES ('$consecutivo', '$codigoComprobante', '$fechaPago', 'Pago de corte $_POST[corte] de la factura numero $_POST[numeroFactura]', $totalCabecera, $totalCabecera, '1') ";
                    mysqli_query($linkbd,$sqlComprobanteCabecera);

                    // if($_POST['modoRecaudo'] == 'banco')
                    // {
                    //     $sqlRecaudoCab = "INSERT INTO srvrecaudo_cab (corte, id_cliente, recaudo_en, cuenta_banco, tipo_movimiento, numero_factura, concepto, fecha_pago, valor) VALUES ('$_POST[corte]', '$_POST[id_cliente]', '$_POST[modoRecaudo]', '$_POST[cuentaBancaria]', '$_POST[tipoMovimiento]', '$_POST[numeroFactura]', '$_POST[concepto]', '$fechaPago', '$_POST[valor]')";
                    // }
                    // elseif($_POST['modoRecaudo'] == 'caja')
                    // {
                    //     $sqlRecaudoCab = "INSERT INTO srvrecaudo_cab (corte, id_cliente, recaudo_en, cuenta_banco, tipo_movimiento, numero_factura, concepto, fecha_pago, valor) VALUES ('$_POST[corte]', '$_POST[id_cliente]', '$_POST[modoRecaudo]', '$rowCuentaCaja[0]', '$_POST[tipoMovimiento]', '$_POST[numeroFactura]', '$_POST[concepto]', '$fechaPago', '$_POST[valor]')";
                    // }

                    // mysqli_query($linkbd,$sqlRecaudoCab);

                    $sqlCorteDetalles = "UPDATE srvcortes_detalle SET estado_pago = 'P' WHERE id_corte = '$_POST[corte]' AND numero_facturacion = '$_POST[numeroFactura]' ";
                    mysqli_query($linkbd,$sqlCorteDetalles);

                    //Trae el nombre de usuario
                    $usuario = $_SESSION['usuario'];

                    if($_POST['modoRecaudo'] == 'banco')
                    {
                        $sqlReciboCaja = "INSERT INTO srvrecibo_caja (consecutivo, numero_factura, id_cliente, tipo_pago, numero_cuenta, concepto, tipo_movimiento, fecha, valor_pago, usuario) VALUES ('$consecutivo', '$_POST[numeroFactura]', '$_POST[id_cliente]', '$_POST[modoRecaudo]', '$_POST[cuentaBancaria]', '$_POST[concepto]', '$_POST[tipoMovimiento]', '$fechaPago', '$_POST[valor]', '$usuario')";    
                    }
                    elseif($_POST['modoRecaudo'] == 'caja')
                    {
                        $sqlReciboCaja = "INSERT INTO srvrecibo_caja (consecutivo, numero_factura, id_cliente, tipo_pago, numero_cuenta, concepto, tipo_movimiento, fecha, valor_pago, usuario) VALUES ('$consecutivo', '$_POST[numeroFactura]', '$_POST[id_cliente]', '$_POST[modoRecaudo]', '$rowCuentaCaja[0]', '$_POST[concepto]', '$_POST[tipoMovimiento]', '$fechaPago', '$_POST[valor]', '$usuario')";    
                    }

                    if(mysqli_query($linkbd,$sqlReciboCaja))
                    {
                        echo "<script>despliegamodalm('visible','1','Se ha almacenado el pago con exito');</script>";
                    }
                    else
                    {
                        echo"<script>despliegamodalm('visible','2','No se pudo ejecutar el recibo de pago, contacte con soporte');</script>";
                    }
				}
			?>
		</form>
		<div id="bgventanamodal2">
			<div id="ventanamodal2">
				<IFRAME  src="" name="buster" marginWidth=0 marginHeight=0 frameBorder=0 id="ventana2" frameSpacing=0 style="left:500px; width:900px; height:500px; top:200;"></IFRAME>
			</div>
		</div>
	</body>
</html>