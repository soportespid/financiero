<?php
ob_end_clean();
require 'zxy.inc.php';
function datosiniciales(){
	$zxy = funcion_empalme();
	$datin[0] = base64_decode($zxy[0]);
	$datin[1] = base64_decode($zxy[1]);
	$datin[2] = base64_decode($zxy[2]);
	$datin[3] = base64_decode($zxy[3]);
	return $datin;
}
function sesion(){
	session_start();
}
function conectar_v7(){
	$datin = datosiniciales();
	$conexion = mysqli_connect($datin[1], $datin[2], $datin[3], $datin[0]);
	if (!$conexion) {die("no se puede conectar: " . mysqli_connect_error());}
	return $conexion;
}

function db_v8() {
    $connection_params = datosiniciales();
    $mysqli = new mysqli(
		$connection_params[1],
		$connection_params[2],
		$connection_params[3],
		$connection_params[0]
	);

	if ($mysqli->connect_error) {
        die('Error de Conexión (' . $mysqli->connect_errno . ') '
            . $mysqli->connect_error);
    }

    return $mysqli;
}

function conectar_Multi($base,$usuario){
	$datin = datosiniciales();
	if($usuario != ''){
		$nom_user = $usuario;
	}else{
		$nom_user = $datin[2];
	}
	$conexion = mysqli_connect($datin[1], $nom_user, $datin[3], $base);
	if (!$conexion) {die("no se puede conectar: " . mysqli_connect_error());}
	return $conexion;
}
function desconectar_bd(){
	//mysql_close();
}
function esta_en_array($objetos, $elemento){
	$i = 0;
	$encontrado = false;
	while(($i<count($objetos))&& !$encontrado){
		if(0==strcmp($objetos[$i],$elemento)){
			$encontrado=1;
		}
		$i++;
	}
	return $encontrado;
}
function pos_en_array($objetos, $elemento){
	$i = 0;
	while(($i<count($objetos))){
		if ($objetos[$i] == $elemento){
			$pos = $i;
		}
		$i++;
	}
	return $pos;
}
function titlepag(){
	echo '<link rel="shortcut icon" href="favicon.ico"/>';
}
function validasusuarioypass($usuario,$passw){
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
	$sqlr = "SELECT us.nom_usu FROM usuarios us, roles ro WHERE us.usu_usu='$usuario' AND us.pass_usu='$passw' AND us.id_rol=ro.id_rol AND us.est_usu='1'";
	$res= mysqli_query($linkbd,$sqlr);
	$r = mysqli_fetch_row($res);
	return $r[0];
}
function view($sql,$return=''){
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
	$query = mysqli_query($linkbd,$sql);
	if ($return=='id'){
		$data = mysqli_insert_id($linkbd);
	}elseif ($return=='confirm') {
		$data = $query;
	}else{
		while($row = mysqli_fetch_assoc($query)){
			$data[] = $row;
		}
	}
	return $data;
}
function paginasnuevas($modulo){
	switch ($modulo){
		case "cont":
			$pagina = "window.open('cont-principal');";
			break;
		case "meci":
			$pagina = "window.open('meci-principal');";
			break;
		case "teso":
			$pagina = "window.open('teso-principal');";
			break;
		case "hum":
			$pagina = "window.open('hum-principal');";
			break;
		case "plan":
			$pagina = "window.open('plan-principal');";
			break;
		case "inve":
			$pagina = "window.open('inve-principal');";
			break;
		case "adm":
			$pagina = "window.open('adm-principal');";
			break;
		case "presu":
			$pagina = "window.open('presu-principal');";
			break;
		case "contra":
			$pagina = "window.open('contra-principal');";
			break;
		case "serv":
			$pagina = "window.open('serv-principal');";
			break;
		case "ccpet":
			$pagina = "window.open('ccp-principal');";
			break;
        case "para":
            $pagina = "window.open('para-principal');";
            break;
        case "info":
            $pagina = "window.open('info-principal');";
            break;
	}
	return $pagina;
}
function conectar_bd(){//conexion obsoleta
	$datin=datosiniciales();
	if(!($conexion=mysql_connect($datin[1],$datin[2],$datin[3])))
	die("no se puede conectar");
	if(!mysql_select_db($datin[0]))
	die("no se puede seleccionar bd");
	return $conexion;
}
function conectar(){//conexion obsoleta
	$datin=datosiniciales();
	if(!($conexion=mysql_connect($datin[1],$datin[2],$datin[3])))
		if(!mysql_select_db($datin[0]))
		die("no se puede seleccionar bd");
	return $conexion;
}
?>
