<?php
    require_once("tcpdf/tcpdf_include.php");
	require('comun.inc');
	require "funciones.inc";
    require 'funcionesSP.inc.php';
	date_default_timezone_set("America/Bogota");
	session_start();
     /* ini_set('display_errors', '1');
    ini_set('display_startup_errors', '1');
    error_reporting(E_ALL); */
	class MYPDF extends TCPDF {
        public $hechoPor = '';
        public $tercero = '';
        public $i = 0;

		public function Header() {
			$linkbd = conectar_v7();
			$linkbd -> set_charset("utf8");
			$sqlr="SELECT * FROM configbasica WHERE estado='S'";
			$res=mysqli_query($linkbd, $sqlr);
			while($row=mysqli_fetch_row($res)) {
				$nit = $row[0];
				$rs  = $row[1];
			}
			//Parte Izquierda
			$this->Image('imagenes/escudo.jpg',  13, 13, 20, 20, 'JPG', '', 'T', true, 300, '', false, false, 0, false, false, false);// Logo
			$this->SetFont('helvetica','B',8);
			$this->SetY(10);
			$this->RoundedRect(10, 10, 190, 25, 1,'');
			$this->Cell(0.1);
			$this->Cell(26,25,'','R',0,'L');
			$this->SetY(8);
			$this->SetX(40);
			$this->SetFont('helvetica','B',9);
			$this->Cell(140,15,strtoupper("$rs"),0,0,'C');
			$this->SetFont('helvetica','B',7);
			$this->SetY(12);
			$this->SetX(40);
			$this->Cell(140,15,'NIT: '.$nit,0,0,'C');
			//*****************************************************************************************************************************
			$this->SetFont('helvetica','B',9);
			$this->SetY(23);
			$this->SetX(36);
            $this->Cell(164,12,"EGRESO",'T',0,'C');

			//**********************************************************
			$this->SetFont('times','B',10);
			$this->ln(12);
			//**********************************************************
            $arrData = json_decode($_POST['data'],true);
            $estado = $arrData[$this->i]["estado"];

            if($estado=="R"){
                $img_file = "assets/img/reversado.png";
                $this->SetAlpha(0.35);
                $this->Image($img_file, 0, 20, 250, 280, '', '', '', false, 300, '', false, false, 300);
                $this->SetAlpha(1);
            }

            $this->i = $this->i + 1;
		}
		public function Footer()
		{
			$linkbd = conectar_v7();
			$linkbd -> set_charset("utf8");
			$sqlr="SELECT direccion,telefono,web,email FROM configbasica WHERE estado='S'";
			$resp=mysqli_query($linkbd, $sqlr);
			$user = $_SESSION['nickusu'];
            $cedula = $_SESSION['cedulausu'];
			$fecha = date("Y-m-d H:i:s");
			$ip = $_SERVER['REMOTE_ADDR'];
			while($row=mysqli_fetch_row($resp))
			{
				$direcc=strtoupper($row[0]);
				$telefonos=$row[1];
				$dirweb=strtoupper($row[3]);
				$coemail=strtoupper($row[2]);
			}
			if($direcc!=''){$vardirec="Dirección: $direcc, ";}
			else {$vardirec="";}
			if($telefonos!=''){$vartelef="Telefonos: $telefonos";}
			else{$vartelef="";}
			if($dirweb!=''){$varemail="Email: $dirweb, ";}
			else {$varemail="";}
			if($coemail!=''){$varpagiw="Pagina Web: $coemail";}
			else{$varpagiw="";}

            $this->setY(280);
			$this->SetFont('helvetica', 'I', 8);
			$txt = <<<EOD
			$vardirec $vartelef
			$varemail $varpagiw
			EOD;
			$this->SetFont('helvetica', 'I', 6);
			$this->Cell(277,10,'','T',0,'T');
			$this->ln(2);
			$this->Write(0, $txt , '', 0, 'C', true, 0, false, false, 0);


            $sqlrcc="SELECT * FROM `usuarios` WHERE `cc_usu` = '$cedula'";
            $respcc=mysqli_query($linkbd, $sqlrcc);
            $rowcc=mysqli_fetch_row($respcc);
            //echo $rowcc[1];

			$this->Cell(50, 10, 'Hecho por: '. $this->hechoPor, 00, false, 'C', 0, '', 0, false, 'T', 'M');
			$this->Cell(30, 10, 'Impreso por: '.$user, 0, false, 'C', 0, '', 0, false, 'T', 'M');
			$this->Cell(30, 10, 'IP: '.$ip, 0, false, 'C',0, '', 0, false, 'T', 'M');
			$this->Cell(30, 10, 'Fecha: '.$fecha, 0, false, 'C', 0, '', 0, false, 'T', 'M');
			$this->Cell(30, 10, 'IDEAL.10 S.A.S', 0, false, 'C', 0, '', 0, false, 'T', 'M');
            $this->Cell(20, 10, 'Página ' . $this->getPageNumGroupAlias() . ' de ' . $this->getPageGroupAlias(), 0, false, 'C');
		}
	}

	
    $data = json_decode($_POST['data'],true);
    $pdf = new MYPDF('P','mm','Letter', true, 'iso-8859-1', false);
    $pdf->SetDocInfoUnicode (true);
    // set document information
    $pdf->SetCreator(PDF_CREATOR);
    $pdf->SetAuthor('IDEALSAS');
    $pdf->SetTitle('EGRESO');
    $pdf->SetSubject('EGRESO');
    $pdf->SetKeywords('TCPDF, PDF, example, test, guide');
    $pdf->SetMargins(10, 38, 10);// set margins
    $pdf->SetHeaderMargin(38);// set margins
    $pdf->SetFooterMargin(17);// set margins
    $pdf->SetAutoPageBreak(TRUE, 20);// set auto page breaks
    // set some language-dependent strings (optional)
    if (@file_exists(dirname(__FILE__).'/lang/spa.php'))
    {
        require_once(dirname(__FILE__).'/lang/spa.php');
        $pdf->setLanguageArray($l);
    }
    $pdf->SetFillColor(255,255,255);
    $pdf->AddPage();

    $linkbd = conectar_v7();
    $linkbd -> set_charset("utf8");
    
    $data = json_decode($_POST['data'],true);
    $cantidadEgresos = count($data);
    $currentIndex = 0;
    foreach ($data as $d) {
        $currentIndex++;
        $fun = $inv = $fun_inv = $nombreGasto = $contrato = $acta = "";
        $consecutivo = $d["consecutivo"];
        $vigencia = $d["vigencia"];
        $fecha = $d["fecha"];
        $codObligacion = $d["cod_obligacion"];
        $detalle = $d["detalle"];
        $valorTotal = $d["valortotal"];
        $valorAPagar = $d["valorpago"];
        $valorRetencion = $d["valor_retenciones"];
        $docBeneficiario = $d["tercero"];
        $pdf->tercero = $d["tercero"];
        $nomBeneficiario = $d["nombre"];
        $cuentaBancaria = "$d[cuenta_bancaria_beneficiario]";
        $cuentaBancariaEntidad = $d["cuenta_bancaria_entidad"];
        $formaPago = $d["forma_pago"];
        $estado = $d["estado"];
        $tipoComprobante = 6;    
        $sql_hecho = "SELECT nom_usu FROM usuarios WHERE usu_usu = '$d[usuario]' LIMIT 1";
        $row_hecho = mysqli_fetch_assoc(mysqli_query($linkbd, $sql_hecho));
        $pdf->hechoPor = $row_hecho["nom_usu"];

        $arrDescuentos = $d["retenciones"];

        $sqlr_obl = "SELECT id_rp FROM tesoordenpago WHERE id_orden = '$codObligacion'";
        $res_obl = mysqli_query($linkbd, $sqlr_obl);
        $row_obl = mysqli_fetch_row($res_obl);

        $sql_rp_cab = "SELECT idcdp AS cdp FROM ccpetrp WHERE consvigencia = '$row_obl[0]' AND vigencia = '$vigencia'";
        $row_rp_cab = mysqli_fetch_assoc(mysqli_query($linkbd, $sql_rp_cab));
        $codCdp = $row_rp_cab["cdp"];

        $sql_relacion_solicitud = "SELECT id_solicitud_cdp FROM ccpet_solicitud_cdp WHERE consecutivo = '$codCdp' AND vigencia = '$vigencia'";
        $row_relacion_solicitud = mysqli_fetch_assoc(mysqli_query($linkbd, $sql_relacion_solicitud));

        $sql_solicitud_cdp = "SELECT consecutivo, id_paa, tipo_presupuesto, tipo_gasto, sector, tipo_contrato_o_acto, tipo_contrato, numero_acto, fecha_acto FROM plan_solicitud_cdp WHERE id = $row_relacion_solicitud[id_solicitud_cdp]";
        $row_solicitud_cdp = mysqli_fetch_assoc(mysqli_query($linkbd, $sql_solicitud_cdp));

        $paaId = $row_solicitud_cdp["id_paa"];
        $tipoPresupuesto = $row_solicitud_cdp["tipo_presupuesto"];
        $tipoDocumento = $row_solicitud_cdp["tipo_contrato_o_acto"];

        $sql_plan_compras = "SELECT p.codplan AS codigo, m.nombre AS nombre_modalidad FROM contraplancompras AS p INNER JOIN plan_modalidad_seleccion AS m ON p.modalidad = m.codigo WHERE p.id = $paaId AND p.estado = 'S'";
        $row_plan_compras = mysqli_fetch_assoc(mysqli_query($linkbd, $sql_plan_compras));

        $paaCod = $row_plan_compras["codigo"];
        $modalidad = strtoupper($row_plan_compras["nombre_modalidad"]);

        if ($tipoPresupuesto == 1) {
            $fun = "X";
            $sql_version = "SELECT MAX(version) AS version FROM cuentasccpet";
            $row_version = mysqli_fetch_assoc(mysqli_query($linkbd, $sql_version));
            $version = $row_version["version"];

            $tipoGasto = $row_solicitud_cdp["tipo_gasto"];
            $sql_tipo_gasto = "SELECT nombre FROM cuentasccpet WHERE codigo = '$tipoGasto' AND nivel = 3 AND version = $version";
            $row_tipo_gasto = mysqli_fetch_assoc(mysqli_query($linkbd, $sql_tipo_gasto));
            $nombreGasto = strtoupper($row_tipo_gasto["nombre"]);    
        } else if ($tipoPresupuesto == 2) {
            $inv = "X";
            $codSector = $row_solicitud_cdp["sector"];
            $sql_sector = "SELECT nombre FROM ccpetsectores WHERE codigo = '$codSector'";
            $row_sector = mysqli_fetch_assoc(mysqli_query($linkbd, $sql_sector));
            $nomSector = strtoupper($row_sector["nombre"]);
        } else {
            $fun_inv = "X";
        }

        if ($tipoDocumento == 1) {
            $contrato = "X";
        } else {
            $acta = "X";
        }

        $sql_comprobantes = "SELECT cuenta, detalle, SUM(valdebito) AS valdebito, SUM(valcredito) AS valcredito FROM comprobante_det WHERE tipo_comp = '$tipoComprobante' AND numerotipo = '$consecutivo' GROUP BY cuenta ORDER BY cuenta ASC";
        $row_comprobantes = mysqli_fetch_all(mysqli_query($linkbd, $sql_comprobantes), MYSQLI_ASSOC);

        foreach ($row_comprobantes as $key => $comprobante) {
            $sql_cuenta = "SELECT nombre FROM cuentasnicsp WHERE cuenta = '$comprobante[cuenta]'";
            $row_cuenta = mysqli_fetch_assoc(mysqli_query($linkbd, $sql_cuenta));
            $row_comprobantes[$key]["nombre_cuenta"] = $row_cuenta["nombre"] != "" ? $row_cuenta["nombre"] : "Cuenta no creada";
        }

        $pdf->SetFont('helvetica','B',9);
        $pdf->SetFillColor(153,221,255);
        $pdf->MultiCell(190,10,"EGRESO","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
        $pdf->ln();
        $pdf->SetFillColor(255,255,255);
        $pdf->SetTextColor(0,0,0);
        $pdf->SetFont('Helvetica','',8);
        $pdf->MultiCell(20,5,"CONSECUTIVO:","LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
        $pdf->MultiCell(15,5,$consecutivo,"LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
        $pdf->MultiCell(25,5,"FECHA:","LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
        $pdf->MultiCell(25,5,$fecha,"LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
        $pdf->MultiCell(25,5,"OBLIGACIÓN:","LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
        $pdf->MultiCell(25,5,$codObligacion,"LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
        $pdf->MultiCell(20,5,"VALOR A PAGAR:","LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
        $pdf->MultiCell(35,5,"$".number_format($valorAPagar, 2),"LRBT",'R',true,0,'','',true,0,false,true,0,'M',true);
        $pdf->ln();
        $pdf->SetFont('helvetica','B',9);
        $pdf->SetFillColor(153,221,255);
        $pdf->MultiCell(190,5,"INFORMACIÓN GENERAL","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
        $pdf->ln();
        $pdf->SetFont('Helvetica','',8);
        $pdf->SetFillColor(255,255,255);
        $pdf->MultiCell(35,5,"PLAN DE COMPRAS:","LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
        $pdf->MultiCell(25,5,$paaCod,"LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
        $pdf->MultiCell(50,5,"MODALIDAD DE CONTRATACIÓN:","LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
        $pdf->MultiCell(80,5,$modalidad,"LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
        $pdf->ln();
        $pdf->MultiCell(50,5,"TIPO DE PRESUPUESTO ASIGNADO:","LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
        $pdf->MultiCell(25,5,"FUNCIONAMIENTO","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
        $pdf->MultiCell(21,5,$fun,"RBT",'C',true,0,'','',true,0,false,true,0,'M',true);
        $pdf->MultiCell(25,5,"INVERSION","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
        $pdf->MultiCell(22,5,$inv,"RBT",'C',true,0,'','',true,0,false,true,0,'M',true);
        $pdf->MultiCell(25,5,"FUN/INV","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
        $pdf->MultiCell(22,5,$fun_inv,"LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
        $pdf->ln();
        if ($tipoPresupuesto == 1) {
            $pdf->MultiCell(50,5,"TIPO DE GASTO:","LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
            $pdf->MultiCell(140,5,$nombreGasto,"LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
        } else if ($tipoPresupuesto == 2) {
            $pdf->MultiCell(40,5,"CODIGO SECTOR:","LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
            $pdf->MultiCell(20,5,$codSector,"LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
            $pdf->MultiCell(40,5,"NOMBRE SECTOR:","LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
            $pdf->MultiCell(90,5,$nomSector,"LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
        } else {
            
        }
        $pdf->ln();
        $pdf->MultiCell(50,5,"TIPO DE DOCUMENTO:","LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
        $pdf->MultiCell(45,5,"TIPO DE CONTRATO","LBT",'C',true,0,'','',true,0,false,true,0,'M',true);
        $pdf->MultiCell(25,5,$contrato,"RBT",'C',true,0,'','',true,0,false,true,0,'M',true);
        $pdf->MultiCell(45,5,"ACTO ADMINISTRATIVO","LBT",'C',true,0,'','',true,0,false,true,0,'M',true);
        $pdf->MultiCell(25,5,$acta,"RBT",'C',true,0,'','',true,0,false,true,0,'M',true);
        $pdf->ln();

        if ($tipoDocumento == 1) {
            $pdf->MultiCell(50,5,"TIPO DE CONTRATO:","LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
            $tipoContrato = $row_solicitud_cdp["tipo_contrato"];
            if ($tipoContrato == 1) {
                $nombreTipoContrato = "OBRA";
            } else if ($tipoContrato == 2){
                $nombreTipoContrato = "CONSULTORA DE SERVICIO";
            } else if ($tipoContrato == 3){
                $nombreTipoContrato = "SUMINISTRO Y/O COMPRAVENTA";
            } else if ($tipoContrato == 4){
                $nombreTipoContrato = "PRESTACIÓN DE SERVICIOS";
            } else {
                $nombreTipoContrato = "OTRO";
            }
            $pdf->MultiCell(140,5,$nombreTipoContrato,"LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
        } else {
            $numeroActo = $row_solicitud_cdp["numero_acto"];
            $fechaActo = DateTime::createFromFormat('Y-m-d', $row_solicitud_cdp["fecha_acto"])->format('d/m/Y');
            $pdf->MultiCell(50,5,"NÚMERO DE ACTO:","LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
            $pdf->MultiCell(40.5,5,$numeroActo,"LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
            $pdf->MultiCell(50,5,"FECHA DE ACTO:","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
            $pdf->MultiCell(49.5,5,$fechaActo,"LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
        }
        $pdf->ln();
        $pdf->MultiCell(30,5,"CUENTA BANCARIA ENTIDAD:","LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
        $pdf->MultiCell(90,5,$cuentaBancariaEntidad,"LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
        $pdf->MultiCell(30,5,"FORMA PAGO:","LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
        $pdf->MultiCell(40,5,$formaPago,"LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
        $pdf->ln();
        $pdf->SetFont('helvetica','B',9);
        $pdf->SetFillColor(153,221,255);
        $pdf->MultiCell(190,5,"INFORMACIÓN BENEFICIARIO","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
        $pdf->ln();
        $pdf->SetFillColor(255,255,255);
        $pdf->SetTextColor(0,0,0);
        $pdf->SetFont('Helvetica','',8);
        $pdf->MultiCell(30,5,"BENEFICIARIO:","LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
        $pdf->MultiCell(90,5,$nomBeneficiario,"LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
        $pdf->MultiCell(30,5,"DOCUMENTO:","LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
        $pdf->MultiCell(40,5,$docBeneficiario,"LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
        $pdf->ln();
        $pdf->MultiCell(30,5,"CUENTA BANCARIA:","LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
        $pdf->MultiCell(160,5,$cuentaBancaria,"LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
        $pdf->ln();
        $pdf->MultiCell(30,16,"DETALLE:","LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
        $pdf->MultiCell(160,16,$detalle,"LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
        $pdf->ln();
        $pdf->MultiCell(23,5,"VALOR ORDEN:","LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
        $pdf->MultiCell(40,5,"$".number_format($valorTotal, 2),"LRBT",'R',true,0,'','',true,0,false,true,0,'M',true);
        $pdf->MultiCell(24,5,"RETENCIONES:","LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
        $pdf->MultiCell(40,5,"$".number_format($valorRetencion, 2),"LRBT",'R',true,0,'','',true,0,false,true,0,'M',true);
        $pdf->MultiCell(23,5,"VALOR A PAGAR:","LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
        $pdf->MultiCell(40,5,"$".number_format($valorAPagar, 2),"LRBT",'R',true,0,'','',true,0,false,true,0,'M',true);
        $pdf->ln();
        $pdf->ln();

        if (!empty($arrDescuentos)) {
            $pdf->SetFont('helvetica','B',8);
            $pdf->SetFillColor(153,221,255);
            $pdf->MultiCell(190,5,"RETENCIONES APLICADAS","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
            $pdf->ln();
            $pdf->MultiCell(80,10,"Nombre retención","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
            $pdf->MultiCell(30,10,"Cantidad","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
            $pdf->MultiCell(40,10,"Porcentaje","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
            $pdf->MultiCell(40,10,"Valor retención","LRBT",'R',true,0,'','',true,0,false,true,0,'M',true);
            $pdf->ln();

            foreach ($arrDescuentos as $descuento) {
                $height = $pdf->getNumLines($descuento["nombre"])*5;
                $pdf->SetFont('helvetica','',8);
                $pdf->SetFillColor(255,255,255);
                $pdf->MultiCell(80,$height,"$descuento[codigo] - $descuento[nombre]","LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
                $pdf->MultiCell(30,$height,"1","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
                $pdf->MultiCell(40,$height,"$descuento[porcentaje]%","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
                $pdf->MultiCell(40,$height,"$".number_format($descuento['valor'], 2),"LRBT",'R',true,0,'','',true,0,false,true,0,'M',true);      
    
                $pdf->ln();
    
                $getY = $pdf->getY();
                if ($getY > 220) {
                    $pdf->AddPage();
                }
            }
        }

        if (!empty($row_comprobantes)) {
            $pdf->SetFont('helvetica','B',8);
            $pdf->SetFillColor(153,221,255);
            $pdf->MultiCell(190,5,"DISCRIMINACIÓN CONTABLE","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
            $pdf->ln();
            $pdf->MultiCell(30,10,"Cuenta","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
            $pdf->MultiCell(40,10,"Nombre","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
            $pdf->MultiCell(60,10,"Detalle","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
            $pdf->MultiCell(30,10,"Debito","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
            $pdf->MultiCell(30,10,"Credito","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
            $pdf->ln();
            $pdf->SetFont('helvetica','',8);
            $pdf->SetFillColor(255,255,255);
            $totalDebito = $totalCredito = 0;
            foreach ($row_comprobantes as $comprobante) {
                $saldoCuenta = round($comprobante["valdebito"],2) - round($comprobante["valcredito"], 2);
                if($saldoCuenta < 0){
                    $ValDebito = 0;
                    $ValCredito = $saldoCuenta*(-1);
                }else{
                    $ValDebito = $saldoCuenta;
                    $ValCredito = 0;
                }
                $height = $pdf->getNumLines($comprobante['detalle'])*7;
                $pdf->MultiCell(30,$height,"$comprobante[cuenta]","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
                $pdf->MultiCell(40,$height,"$comprobante[nombre_cuenta]","LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
                $pdf->MultiCell(60,$height,"$comprobante[detalle]","LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
                $pdf->MultiCell(30,$height,"$".number_format($ValDebito, 2),"LRBT",'R',true,0,'','',true,0,false,true,0,'M',true);      
                $pdf->MultiCell(30,$height,"$".number_format($ValCredito, 2),"LRBT",'R',true,0,'','',true,0,false,true,0,'M',true);      

                $totalDebito += $ValDebito;
                $totalCredito += $ValCredito;
                $pdf->ln();
                $getY = $pdf->getY();
                if ($getY > 220) {
                    $pdf->AddPage();
                }
            }

            $pdf->SetFont('helvetica','B',8);
            $pdf->MultiCell(130,5,"TOTALES:","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
            $pdf->MultiCell(30,5,"$".number_format($totalDebito, 2),"LRBT",'R',true,0,'','',true,0,false,true,0,'M',true);
            $pdf->MultiCell(30,5,"$".number_format($totalCredito, 2),"LRBT",'R',true,0,'','',true,0,false,true,0,'M',true);
        }

        //Firmas
        $firmas = [];
        $sql_firmas = "SELECT id_cargo, id_comprobante FROM pptofirmas WHERE id_comprobante = '11' AND vigencia = '$vigencia'";
        $row_firmas = mysqli_fetch_all(mysqli_query($linkbd, $sql_firmas), MYSQLI_ASSOC);

        foreach ($row_firmas as $firma) {
            if ($firma["id_cargo"] == 0) {
                $data = [
                    "nombre" => buscatercero($docBeneficiario),
                    "cargo" => "Beneficiario"
                ];

                array_push($firmas, $data);
            } else if ($firma["id_cargo"] == 40) {
                $data = [
                    "nombre" => $pdf->hechoPor,
                    "cargo" => "Profesional"
                ];
                array_push($firmas, $data);
            }else {
                $sql_funcionario ="SELECT cedulanit,(SELECT nombrecargo FROM planaccargos WHERE codcargo = '$firma[id_cargo]') AS cargo FROM planestructura_terceros where codcargo = '$firma[id_cargo]' AND estado = 'S'";
                $row_funcionario = mysqli_fetch_assoc(mysqli_query($linkbd, $sql_funcionario));
                $data = [
                    "nombre" => buscar_empleado($row_funcionario["cedulanit"]),
                    "cargo" => $row_funcionario["cargo"]
                ];
                array_push($firmas, $data);
            }
        }

        $cantidadFirmas = count($firmas);
        $anchoMaximo = 190;

        $pdf->setY(240);

        if ($cantidadFirmas == 1) {
            $anchoFirma = min($anchoMaximo, 80); // Ancho máximo de 95 si hay una firma
        } elseif ($cantidadFirmas == 2) {
            $anchoFirma = min($anchoMaximo / 2, 80); // Ancho máximo de 95 para cada firma si hay dos firmas
        } else {
            $anchoFirma = $anchoMaximo / $cantidadFirmas; // Distribuir el ancho equitativamente si hay más de dos firmas
        }

        $pdf->SetFont('helvetica','B',6);
        $pdf->SetFillColor(153,221,255);

        if ($cantidadFirmas == 1) {
            $pdf->setX(60);
        } else if ($cantidadFirmas == 2) {
            $pdf->setX(20);
        }

        foreach ($firmas as $firma) {
            $pdf->Cell($anchoFirma, 5, "Datos personales", 1, 0, 'C', true);
        }

        $pdf->Ln();
        $pdf->SetFont('helvetica','B',6);
        $pdf->SetFillColor(255,255,255);

        if ($cantidadFirmas == 1) {
            $pdf->setX(60);
        } else if ($cantidadFirmas == 2) {
            $pdf->setX(20);
        }

        foreach ($firmas as $firma) {
            $pdf->Cell($anchoFirma, 5, " Nombre: $firma[nombre]", 1, 0, 'L', true);
        }

        $pdf->Ln();

        if ($cantidadFirmas == 1) {
            $pdf->setX(60);
        } else if ($cantidadFirmas == 2) {
            $pdf->setX(20);
        }

        foreach ($firmas as $firma) {
            $pdf->Cell($anchoFirma, 5, " Cargo: $firma[cargo]", 1, 0, 'L', true);
        }

        $pdf->Ln();

        if ($cantidadFirmas == 1) {
            $pdf->setX(60);
        } else if ($cantidadFirmas == 2) {
            $pdf->setX(20);
        }

        foreach ($firmas as $firma) {
            $pdf->cell($anchoFirma,15,'Firma','LRTB',0,'C',1);
        }

        if ($currentIndex != $cantidadEgresos) {
            $pdf->startPageGroup();
            $pdf->AddPage();
        }
    
    }
    
    $pdf->Output('egreso'.'.pdf', 'I');
?>
