<?php
    require_once("tcpdf/tcpdf_include.php");
	require('comun.inc');
	require "funciones.inc";
    require 'funcionesSP.inc.php';
	date_default_timezone_set("America/Bogota");
	session_start();
	class MYPDF extends TCPDF
	{
		public function Header()
		{
			$linkbd = conectar_v7();
			$linkbd -> set_charset("utf8");
			$sqlr="select *from configbasica where estado='S' ";
			//echo $sqlr;
			$res=mysqli_query($linkbd, $sqlr);
			while($row=mysqli_fetch_row($res))
			{
				$nit=$row[0];
				$rs=$row[1];

			}
			//Parte Izquierda
			$this->Image('imagenes/escudo.jpg',  13, 13, 20, 20, 'JPG', '', 'T', true, 300, '', false, false, 0, false, false, false);// Logo
			$this->SetFont('helvetica','B',8);
			$this->SetY(10);
			$this->RoundedRect(10, 10, 190, 25, 1,'');
			$this->Cell(0.1);
			$this->Cell(26,25,'','R',0,'L');
			$this->SetY(8);
			$this->SetX(80);
			$this->SetFont('helvetica','B',9);
			$this->Cell(50,15,strtoupper("$rs"),0,0,'C');
			$this->SetFont('helvetica','B',7);
			$this->SetY(12);
			$this->SetX(80);
			$this->Cell(50,15,'NIT: '.$nit,0,0,'C');
			//*****************************************************************************************************************************
			$this->SetFont('helvetica','B',9);
			$this->SetY(23);
			$this->SetX(36);
            $this->Cell(164,12,"FORMULARIO DE INSCRIPCIÓN DE INDUSTRIA Y COMERCIO",'T',0,'C');


            $this->SetFont('helvetica','B',6);
			$this->SetY(10);
			$this->SetX(170);
			$this->Cell(30,7,"CONSECUTIVO: ".CABECERA['consecutivo'],"L",0,'C');
			$this->SetY(17);
			$this->SetX(170);
			$this->Cell(30,6,"FECHA: ".CABECERA['fecha_registro'],"L",0,'C');

			//**********************************************************
			$this->SetFont('times','B',10);
			$this->ln(12);
			//**********************************************************
		}
		public function Footer()
		{
			$linkbd = conectar_v7();
			$linkbd -> set_charset("utf8");
			$sqlr="SELECT direccion,telefono,web,email FROM configbasica WHERE estado='S'";
			$resp=mysqli_query($linkbd, $sqlr);
			$user = $_SESSION['nickusu'];
            $cedula = $_SESSION['cedulausu'];
			$fecha = date("Y-m-d H:i:s");
			$ip = $_SERVER['REMOTE_ADDR'];
			while($row=mysqli_fetch_row($resp))
			{
				$direcc=strtoupper($row[0]);
				$telefonos=$row[1];
				$dirweb=strtoupper($row[3]);
				$coemail=strtoupper($row[2]);
			}
			if($direcc!=''){$vardirec="Dirección: $direcc, ";}
			else {$vardirec="";}
			if($telefonos!=''){$vartelef="Telefonos: $telefonos";}
			else{$vartelef="";}
			if($dirweb!=''){$varemail="Email: $dirweb, ";}
			else {$varemail="";}
			if($coemail!=''){$varpagiw="Pagina Web: $coemail";}
			else{$varpagiw="";}
			$this->SetFont('helvetica', 'I', 8);
			$txt = <<<EOD
			$vardirec $vartelef
			$varemail $varpagiw
			EOD;
			$this->SetFont('helvetica', 'I', 6);
			$this->Cell(277,10,'','T',0,'T');
			$this->ln(2);
			$this->Write(0, $txt , '', 0, 'C', true, 0, false, false, 0);


            $sqlrcc="SELECT * FROM `usuarios` WHERE `cc_usu` = '$cedula'";
            $respcc=mysqli_query($linkbd, $sqlrcc);
            $rowcc=mysqli_fetch_row($respcc);
            //echo $rowcc[1];

			$this->Cell(50, 10, 'Hecho por: '.$rowcc[1], 00, false, 'C', 0, '', 0, false, 'T', 'M');
			$this->Cell(30, 10, 'Impreso por: '.$user, 0, false, 'C', 0, '', 0, false, 'T', 'M');
			$this->Cell(30, 10, 'IP: '.$ip, 0, false, 'C',0, '', 0, false, 'T', 'M');
			$this->Cell(30, 10, 'Fecha: '.$fecha, 0, false, 'C', 0, '', 0, false, 'T', 'M');
			$this->Cell(30, 10, 'IDEAL.10 S.A.S', 0, false, 'C', 0, '', 0, false, 'T', 'M');
            $this->Cell(20, 10, 'Pagina '.$this->getAliasNumPage().'/'.$this->getAliasNbPages(), 0, false, 'C', 0, '', 0, false, 'T', 'M');
		}
	}

	if($_GET['data']){
        $data = json_decode($_GET['data'],true);

        //dep(json_decode($_GET['data'],true));exit;
        define("CABECERA",$data['cabecera']);
        define("CONTRIBUYENTE",$data['cabecera']['contribuyente']);
        define("REVISOR",$data['cabecera']['revisor']);
        define("TIPOPERSONA",array(1=>"Natural",2=>"Jurídica"));
        define("TIPOREGIMEN",array(1=>"Responsable de IVA",2=>"No responsable de IVA"));
        define("TIPOMATRICULA",array(1=>"Común",2=>"Simple"));
        define("REPRESENTANTES",$data['representantes']);
        define("ACTIVIDADES",$data['actividades']);
        define("ESTABLECIMIENTOS",$data['establecimientos']);
        define("TIPODOC",array(
            12=>"Tarjeta de Identidad",
            21=>"Tarjeta de Extranjeria",
            11=>"Registro Civil de Nacimiento",
            13=>"Cedula de Ciudadania",
            22=>"Cedula Extranjera",
            31=>"NIT",
            41=>"Pasaporte"
        ));
        $arrEstado = array("S"=>"Inscripción de matrícula","A"=>"Actualización de matrícula","N"=>"Cancelación de matrícula");
        $pdf = new MYPDF('P','mm','Letter', true, 'iso-8859-1', false);
        $pdf->SetDocInfoUnicode (true);
        // set document information
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('IDEALSAS');
        $pdf->SetTitle('FORMULARIO DE INSCRIPCIÓN DE INDUSTRIA Y COMERCIO');

        $pdf->SetSubject('FORMULARIO DE INSCRIPCIÓN DE INDUSTRIA Y COMERCIO');
        $pdf->SetKeywords('TCPDF, PDF, example, test, guide');
        $pdf->SetMargins(10, 38, 10);// set margins
        $pdf->SetHeaderMargin(38);// set margins
        $pdf->SetFooterMargin(17);// set margins
        $pdf->SetAutoPageBreak(TRUE, 20);// set auto page breaks
        // set some language-dependent strings (optional)
        if (@file_exists(dirname(__FILE__).'/lang/spa.php'))
        {
            require_once(dirname(__FILE__).'/lang/spa.php');
            $pdf->setLanguageArray($l);
        }
        $pdf->AddPage();

        $pdf->ln(2);
        $pdf->SetFillColor(153,221,255);
        $pdf->SetFont('helvetica','B',9);
        //Información general
        $pdf->cell(190,4,'Información general','LRTB',0,'L',1);
        $pdf->ln(4);
        $pdf->SetFillColor(255,255,255);
        $pdf->SetFont('helvetica','B',7);
        $pdf->cell(60,4,'Fecha de novedad:','LRT',0,'L',1);
        $pdf->cell(60,4,'Tipo de novedad:','LRT',0,'L',1);
        $pdf->cell(70,4,'Fecha inicio de actividades en municipio:','LRT',0,'L',1);
        $pdf->ln(4);
        $pdf->SetFont('helvetica','',7);
        $pdf->cell(60,4,CABECERA['fecha_novedad'],'LRB',0,'L',1);
        $pdf->cell(60,4,$arrEstado[CABECERA['estado']],'LRB',0,'L',1);
        $pdf->cell(70,4,CABECERA['fecha_inicio'],'LRB',0,'L',1);
        $pdf->ln(4);
        $pdf->SetFont('helvetica','B',7);
        $pdf->cell(60,4,'Fecha de matrícula:','LRT',0,'L',1);
        $pdf->cell(60,4,'Tipo de matrícula:','LRT',0,'L',1);
        $pdf->cell(70,4,'No. Matrícula mercantíl:','LRT',0,'L',1);
        $pdf->ln(4);
        $pdf->SetFont('helvetica','',7);
        $pdf->cell(60,4,CABECERA['fecha_matricula'],'LRB',0,'L',1);
        $pdf->cell(60,4,TIPOMATRICULA[CABECERA['tipo_matricula']],'LRB',0,'L',1);
        $pdf->cell(70,4,CABECERA['matricula'],'LRB',0,'L',1);
        $pdf->ln(4);
        $pdf->SetFillColor(153,221,255);
        $pdf->SetFont('helvetica','B',9);
        //Información contribuyente
        $pdf->cell(190,4,'Información del contribuyente','LRTB',0,'L',1);
        $pdf->ln(4);
        $pdf->SetFillColor(255,255,255);
        $pdf->SetFont('helvetica','B',7);
        $pdf->cell(30,4,'Tipo documento:','LRT',0,'L',1);
        $pdf->cell(50,4,'No. documento:','LRT',0,'L',1);
        $pdf->cell(50,4,'Tipo persona:','LRT',0,'L',1);
        $pdf->cell(60,4,'Régimen industria y comercio:','LRT',0,'L',1);
        $pdf->ln(4);
        $pdf->SetFont('helvetica','',7);
        $pdf->cell(30,4,TIPODOC[CONTRIBUYENTE['tipo_documento']],'LRB',0,'L',1);
        $pdf->cell(50,4,CONTRIBUYENTE['documento'],'LRB',0,'L',1);
        $pdf->cell(50,4,TIPOPERSONA[CONTRIBUYENTE['tipo']],'LRB',0,'L',1);
        $pdf->cell(60,4,TIPOREGIMEN[CONTRIBUYENTE['regimen']],'LRB',0,'L',1);
        $pdf->ln(4);
        $pdf->SetFont('helvetica','B',7);
        $pdf->cell(190,4,'Razón social/Nombre:','LRT',0,'L',1);
        $pdf->ln(4);
        $pdf->SetFont('helvetica','',7);
        $pdf->cell(190,4,CONTRIBUYENTE['nombre'],'LRB',0,'L',1);
        $pdf->ln(4);
        $pdf->SetFont('helvetica','B',7);
        $pdf->cell(38,4,"Gran contribuyente",'LTR',0,'L',1);
        $pdf->cell(38,4,"Declara Alumbrado",'LTR',0,'L',1);
        $pdf->cell(38,4,"Agente Retenedor",'LTR',0,'L',1);
        $pdf->cell(38,4,"Agente Auto-Retenedor",'LTR',0,'L',1);
        $pdf->cell(38,4,"Contribuyente Temporal",'LTR',0,'L',1);
        $pdf->ln(4);
        $pdf->SetFont('helvetica','',7);
        $pdf->cell(38,4,CABECERA['gran'] ? "Si" : "No",'LRB',0,'L',1);
        $pdf->cell(38,4,CABECERA['alumbrado'] ? "Si" : "No",'LRB',0,'L',1);
        $pdf->cell(38,4,CABECERA['retendor'] ? "Si" : "No",'LRB',0,'L',1);
        $pdf->cell(38,4,CABECERA['auto'] ? "Si" : "No",'LRB',0,'L',1);
        $pdf->cell(38,4,CABECERA['temporal'] ? "Si" : "No",'LRB',0,'L',1);
        //Representantes legales
        if(!empty(REPRESENTANTES)){
            $pdf->ln(4);
            $pdf->SetFillColor(153,221,255);
            $pdf->SetFont('helvetica','B',9);
            $pdf->cell(190,4,'Representantes legales','LRTB',0,'L',1);
            $pdf->ln(4);
            $pdf->SetFillColor(255,255,255);
            $pdf->SetFont('helvetica','B',7);
            $pdf->cell(30,4,"Tipo representante",'LTRB',0,'L',1);
            $pdf->cell(30,4,"Tipo documento",'LTRB',0,'L',1);
            $pdf->cell(40,4,"Documento",'LTRB',0,'L',1);
            $pdf->cell(60,4,"Nombres y apellidos",'LTRB',0,'L',1);
            $pdf->cell(30,4,"Teléfono",'LTRB',0,'L',1);
            $pdf->ln(4);
            $pdf->SetFont('helvetica','',7);
            foreach (REPRESENTANTES as $rep) {
                $lnNombre = $pdf->getNumLines($rep['nombre'], 30);
                $alturas = 5;
                $pdf->MultiCell(30,$alturas,$rep['tipo'],"LR",'L',true,0,'','',true,0,false,true,0,'M',true);
                $pdf->MultiCell(30,$alturas,TIPODOC[$rep['tipo_documento']],"LR",'L',true,0,'','',true,0,false,true,0,'M',true);
                $pdf->MultiCell(40,$alturas,$rep['documento'],"LR",'L',true,0,'','',true,0,false,true,0,'M',true);
                $pdf->MultiCell(60,$alturas,$rep['nombre'],"LR",'L',true,0,'','',true,0,false,true,0,'M',true);
                $pdf->MultiCell(30,$alturas,$rep['telefono'],"LR",'L',true,0,'','',true,0,false,true,0,'M',true);
                $pdf->ln();
            }
        }else{
            $pdf->ln(4);
        }
        //Información del revisor
        $pdf->SetFillColor(153,221,255);
        $pdf->SetFont('helvetica','B',9);
        $pdf->cell(190,4,'Información del contador o revisor fiscal','LRTB',0,'L',1);
        $pdf->ln(4);
        $pdf->SetFillColor(255,255,255);
        $pdf->SetFont('helvetica','B',7);
        $pdf->cell(50,4,"Tipo documento:",'LTR',0,'L',1);
        $pdf->cell(50,4,"No. Documento:",'LTR',0,'L',1);
        $pdf->cell(90,4,"Nombres y apellidos:",'LTR',0,'L',1);
        $pdf->ln(4);
        $pdf->SetFont('helvetica','',7);
        $pdf->cell(50,4,TIPODOC[REVISOR['tipo']],'LRB',0,'L',1);
        $pdf->cell(50,4,REVISOR['documento'],'LRB',0,'L',1);
        $pdf->cell(90,4,REVISOR['nombre'],'LRB',0,'L',1);

        //Información notificación jurídica
        $pdf->ln(4);
        $pdf->SetFillColor(153,221,255);
        $pdf->SetFont('helvetica','B',9);
        $pdf->cell(190,4,'Información de notificación jurídica','LRTB',0,'L',1);
        $pdf->ln(4);
        $pdf->SetFillColor(255,255,255);
        $pdf->SetFont('helvetica','B',7);
        $pdf->cell(190,4,"Dirección:",'LTR',0,'L',1);
        $pdf->ln(4);
        $pdf->SetFont('helvetica','',7);
        $pdf->cell(190,4,CONTRIBUYENTE['direccion'],'LRB',0,'L',1);
        $pdf->ln(4);
        $pdf->cell(50,4,"Teléfono:",'LTR',0,'L',1);
        $pdf->cell(50,4,"Celular:",'LTR',0,'L',1);
        $pdf->cell(90,4,"Correo:",'LTR',0,'L',1);
        $pdf->ln(4);
        $pdf->cell(50,4,CONTRIBUYENTE['telefono'],'LBR',0,'L',1);
        $pdf->cell(50,4,CONTRIBUYENTE['celular'],'LBR',0,'L',1);
        $pdf->cell(90,4,CONTRIBUYENTE['correo'],'LBR',0,'L',1);
        //Actividades económicas
        $pdf->ln(4);
        $pdf->SetFillColor(153,221,255);
        $pdf->SetFont('helvetica','B',9);
        $pdf->cell(190,4,'Actividades económicas','LRTB',0,'L',1);
        $pdf->ln(4);
        $pdf->SetFillColor(255,255,255);
        $pdf->SetFont('helvetica','B',7);
        $pdf->cell(30,4,"Código:",'LTRB',0,'L',1);
        $pdf->cell(100,4,"Nombre:",'LTRB',0,'L',1);
        $pdf->cell(30,4,"Tipo:",'LTRB',0,'L',1);
        $pdf->cell(30,4,"Fecha inicio:",'LTRB',0,'L',1);
        $pdf->ln(4);
        $pdf->SetFont('helvetica','',7);
        foreach (ACTIVIDADES as $rep) {
            $lnNombre = $pdf->getNumLines($rep['nombre'], 30);
            $alturas = 4;
            $pdf->MultiCell(30,$alturas,$rep['codigo'],"LR",'L',true,0,'','',true,0,false,true,0,'M',true);
            $pdf->MultiCell(100,$alturas,$rep['nombre'],"LR",'L',true,0,'','',true,0,false,true,0,'M',true);
            $pdf->MultiCell(30,$alturas,$rep['tipo'],"LR",'L',true,0,'','',true,0,false,true,0,'M',true);
            $pdf->cell(30,$alturas,$rep['fecha'],'LR',0,'L',1);
            $pdf->ln();
        }
        $pdf->cell(190,4,"",'T',0,'L',1);
        $pdf->ln(0);
        //Representantes legales
        if(!empty(ESTABLECIMIENTOS)){

            $pdf->SetFillColor(153,221,255);
            $pdf->SetFont('helvetica','B',9);
            $pdf->cell(190,4,'Establecimientos de comercio','LRTB',0,'L',1);
            $pdf->ln(4);
            $pdf->SetFillColor(255,255,255);
            $pdf->SetFont('helvetica','B',7);
            $pdf->cell(50,4,"Nombre",'LTRB',0,'L',1);
            $pdf->cell(40,4,"Dirección",'LTRB',0,'L',1);
            $pdf->cell(20,4,"Correo",'LTRB',0,'L',1);
            $pdf->cell(20,4,"Teléfono",'LTRB',0,'L',1);
            $pdf->cell(20,4,"Empleados",'LTRB',0,'L',1);
            $pdf->cell(20,4,"Activos",'LTRB',0,'L',1);
            $pdf->cell(20,4,"Fecha apertura",'LTRB',0,'L',1);
            $pdf->ln(4);
            $pdf->SetFont('helvetica','',7);
            foreach (ESTABLECIMIENTOS as $rep) {
                $lnNombre = $pdf->getNumLines($rep['nombre'], 28);
                $lnDireccion = $pdf->getNumLines($rep['direccion'], 27);
                $lnCorreo = $pdf->getNumLines($rep['correo'], 27);
                $max = max($lnNombre,$lnDireccion,$lnCorreo);
                $alturas = $max*5;
                $pdf->MultiCell(50,$alturas,$rep['nombre'],"LR",'L',true,0,'','',true,0,false,true,0,'M',true);
                $pdf->MultiCell(40,$alturas,$rep['direccion'],"LR",'L',true,0,'','',true,0,false,true,0,'M',true);
                $pdf->MultiCell(20,$alturas,$rep['correo'],"LR",'L',true,0,'','',true,0,false,true,0,'M',true);
                $pdf->MultiCell(20,$alturas,$rep['telefono'],"LR",'L',true,0,'','',true,0,false,true,0,'M',true);
                $pdf->cell(20,$alturas,$rep['empleados'],'LR',0,'L',1);
                $pdf->cell(20,$alturas,"$".number_format($rep['activos'],0),'LR',0,'L',1);
                $pdf->cell(20,$alturas,$rep['fecha_apertura'],'LR',0,'L',1);
                $pdf->ln();
            }
            //$pdf->cell(190,4,"",'T',0,'L',1);
        }else{
            $pdf->ln(4);
        }
        $pdf->SetFont('helvetica','B',9);
        $pdf->cell(190,4,'','T',0,'L',1);
        $pdf->ln();
        $pdf->SetFont('helvetica','I',9);
        $pdf->MultiCell(190,10,'Al firmar declaro que la información que suministré, en el formulario diligenciado y los documentos aportados dentro del proceso de inscripción de
        Industria y Comercio ante el Municipio, es de total veracidad y se encuentra bajo la responsabilidad del firmante',"",'C',true,0,'','',true,0,false,true,0,'M',true);
        $pdf->ln();
        $pdf->ln();

        //Firma funcionario
        $getY = $pdf->getY();
        $pdf->SetFont('helvetica','B',9);
        $pdf->SetFillColor(153,221,255);
        $pdf->cell(95,4,'Espacio para recibido y firma de funcionario','LRTB',0,'L',1);
        $pdf->ln();
        $pdf->SetFont('helvetica','',7);
        $pdf->SetFillColor(255,255,255);
        $pdf->cell(95,20,'','LRT',0,'L',1);
        $pdf->ln();
        $pdf->cell(95,4,'','LR',0,'L',1);
        $pdf->ln();
        $pdf->cell(95,4,'','LRB',0,'L',1);

        //Firma contribuyente
        $pdf->setY($getY);
        $pdf->setX(105);
        $pdf->SetFont('helvetica','B',9);
        $pdf->SetFillColor(153,221,255);
        $pdf->cell(95,4,'Firma de representante Legal o contribuyente','LRTB',0,'L',1);
        $pdf->ln();
        $pdf->SetFont('helvetica','',7);
        $pdf->SetFillColor(255,255,255);
        $pdf->setX(105);
        $pdf->cell(95,20,'','LRTB',0,'L',1);
        $pdf->ln();
        $pdf->setX(105);
        $pdf->cell(95,4,'Nombre: ','LRT',0,'L',1);
        $pdf->ln();
        $pdf->setX(105);
        $pdf->cell(95,4,'No. Documento: ','LRB',0,'L',1);
        $pdf->ln();
        $pdf->ln();
        $pdf->ln();
        $pdf->SetFont('helvetica','I',8);
        $pdf->cell(190,4,'Este Formulario no tiene ningún costo','',0,'C',1);
        $pdf->Output('formulario_inscripcion_'.CABECERA['consecutivo'].'.pdf', 'I');
    }
?>
