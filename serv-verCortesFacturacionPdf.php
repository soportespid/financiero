<?php
    require_once("tcpdf/tcpdf_include.php");
    require './funciones.inc';
    require './funcionesSP.inc.php';

    session_start();
    class MYPDF extends TCPDF {

        // Load table data from file

        public function Header() 
        {
            if ($_POST['estado']=='R'){$this->Image('imagenes/reversado02.png',75,41.5,50,15);}
            $linkbd = conectar_v7();
            $linkbd -> set_charset("utf8");
            $sqlr="select *from configbasica where estado='S' ";
            //echo $sqlr;
            $res=mysqli_query($linkbd, $sqlr);
            while($row=mysqli_fetch_row($res))
            {
                $nit=$row[0];
                $rs=$row[1];
                $nalca=$row[6];
            }
            $strCodigo = $_GET['codigo_recaudo'];
            $strFecha = $_GET['fecha'];
            $detallegreso = $_POST['detallegreso'];
            //Parte Izquierda
            $this->Image('imagenes/escudo.jpg',  13, 13, 20, 20, 'JPG', '', 'T', true, 300, '', false, false, 0, false, false, false);// Logo
            $this->SetFont('helvetica','B',8);
            $this->SetY(10);
            $this->RoundedRect(10, 10, 190, 25, 1,'');
            $this->Cell(0.1);
            $this->Cell(26,25,'','R',0,'L'); 
            $this->SetY(11);
            $this->SetX(60);
            $this->SetFont('helvetica','B',8);
            
            //$this->MultiCell(100, 5, "ADMINISTRACIóN PúBLICA COOPERATIVA DE ACUEDUCTO", '', 'C', 0, 0, '', '', true);
            $this->MultiCell(100, 5, strtoupper("$rs"), '', 'C', 0, 0, '', '', true);
            //$this->Cell(40,15,strtoupper("$rs"),0,0,'C'); 
            $this->SetFont('helvetica','B',6.5);
            $this->SetY(12.5);
            $this->SetX(85);
            $this->Cell(40,15,'NIT: '.$nit,0,0,'C');
            //*****************************************************************************************************************************
            $this->SetFont('helvetica','B',9);		
            $this->SetY(23);
            $this->SetX(36);	
            $this->Cell(164,12,"",'T',0,'C');  
            $this->SetY(23);
            $this->SetX(36);
            $this->Cell(160,12,"REPORTE DE PERIODOS LIQUIDADOS",'',0,'C'); 
            $this->SetY(10);
            $this->SetX(135);
            $mov='';
            if(isset($_POST['movimiento']))
            {
                if(!empty($_POST['movimiento']))
                {
                    if($_POST['movimiento']=='401'){$mov="DOCUMENTO DE REVERSION";}
                }
            }

            $this->SetFont('helvetica','B',6);
            
            $this->SetY(10);
            $this->SetX(170);
            $this->Cell(35,6.8," FECHA: ".date("d/m/Y"),"L",0,'L');
            $this->SetY(17);
            $this->SetX(170);
            $this->Cell(35,6," VIGENCIA: ".date("Y"),"L",0,'L');
            //**********************************************************
            $this->SetFont('times','B',10);
            $this->ln(12);
            //**********************************************************
        }
        public function Footer() {
        
            $linkbd = conectar_v7();
            $linkbd -> set_charset("utf8");
            $sqlr="SELECT direccion,telefono,web,email FROM configbasica WHERE estado='S'";
            $resp=mysqli_query($linkbd, $sqlr);
            $user = $_SESSION['nickusu'];	
            $fecha = date("Y-m-d H:i:s");
            $ip = $_SERVER['REMOTE_ADDR'];
            $useri = $_POST['user'];
            while($row=mysqli_fetch_row($resp))
            {
                $direcc=strtoupper($row[0]);
                $telefonos=$row[1];
                $dirweb=strtoupper($row[3]);
                $coemail=strtoupper($row[2]);
            }
            if($direcc!=''){$vardirec="Dirección: $direcc, ";}
            else {$vardirec="";}
            if($telefonos!=''){$vartelef="Telefonos: $telefonos";}
            else{$vartelef="";}
            if($dirweb!=''){$varemail="Email: $dirweb, ";}
            else {$varemail="";}
            if($coemail!=''){$varpagiw="Pagina Web: $coemail";}
            else{$varpagiw="";}
            $this->SetFont('helvetica', 'I', 8);
            $txt = <<<EOD
            $vardirec $vartelef
            $varemail $varpagiw
            EOD;
            $this->SetFont('helvetica', 'I', 6);
            $this->Cell(277,10,'','T',0,'T');
            $this->ln(2);
            $this->Write(0, $txt , '', 0, 'C', true, 0, false, false, 0);
            //$this->Cell(25, 10, 'Hecho por: '.$useri, 0, false, 'C', 0, '', 0, false, 'T', 'M');
            $this->Cell(25, 10, 'Impreso por: '.$user, 0, false, 'C', 0, '', 0, false, 'T', 'M');
            $this->SetX(25);
            $this->Cell(107, 10, 'IP: '.$ip, 0, false, 'C', 0, '', 0, false, 'T', 'M');
            $this->Cell(35, 10, 'Fecha: '.$fecha, 0, false, 'C', 0, '', 0, false, 'T', 'M');
            $this->Cell(107, 10, 'IDEAL.10 S.A.S', 0, false, 'C', 0, '', 0, false, 'T', 'M');
            $this->Cell(20, 10, 'Pagina '.$this->getAliasNumPage().'/'.$this->getAliasNbPages(), 0, false, 'C', 0, '', 0, false, 'T','M');

            
        }
        // Colored table
        public function ColoredTable($data) {
            // Colors, line width and bold font
            $this->SetFillColor(222, 222, 222);
            $this->SetTextColor(000);
            $this->SetDrawColor(128, 0, 0);
            $this->SetLineWidth(0.3);
            $this->SetFont('helvetica','',6);
            // Header
            $w = array(20, 25, 25,25,25,25,25,20);
            $header =array(
                "Periodo liquidado",
                "Fecha inicial",
                "Fecha final",
                "Fecha de impresion",
                "Fecha limite de pago",
                "Fecha de suspensión",
                "Estado liquidación",
                "Tarifas aplicadas"
            );
            $num_headers = count($header);
            for ($i=0; $i < $num_headers; $i++) { 
                $this->Cell($w[$i], 7, $header[$i], 0, 0, 'C', 1);
            }
            $this->Ln();
            // Color and font restoration
            $this->SetFillColor(245,245,245);
            $this->SetTextColor(0);
            $this->SetFont('');
            // Data
            $fill = 0;
            foreach($data as $row) {
                $status = $row['estado'] == "S" ? "ACTIVO" : "CERRADO";
                $this->Cell($w[0], 6, $row['periodo'], '', 0, 'C', $fill);
                $this->Cell($w[1], 6, $row['inicial'], '', 0, 'C', $fill);
                $this->Cell($w[2], 6, $row['final'], '', 0, 'C', $fill);
                $this->Cell($w[3], 6, $row['impresion'], '', 0, 'C', $fill);
                $this->Cell($w[4], 6, $row['limite'], '', 0, 'C', $fill);
                $this->Cell($w[5], 6, $row['suspension'], '', 0, 'C', $fill);
                $this->Cell($w[6], 6, $status, '', 0, 'C', $fill);
                $this->Cell($w[7], 6, $row['tarifa'], '', 0, 'C', $fill);
               
                $this->Ln();
                $fill=!$fill;
            }
            $this->Cell(array_sum($w), 0, '', '');
            $this->Ln();
            $this->SetFont('helvetica','B',8);
        }
    }
    class CorteFacturacion{
        private $linkbd;

        public function __construct() {
            $this->linkbd = conectar_v7();
            $this->linkbd->set_charset("utf8");
        }

        public function show(){
            $sql = "SELECT 
            numero_corte as periodo,
            DATE_FORMAT(fecha_inicial,'%d/%m/%Y')  as inicial,
            DATE_FORMAT(fecha_final,'%d/%m/%Y')  as final,
            DATE_FORMAT(fecha_impresion,'%d/%m/%Y')  as impresion,
            DATE_FORMAT(fecha_limite_pago,'%d/%m/%Y')  as limite,
            DATE_FORMAT(fecha_corte,'%d/%m/%Y')  as suspension,
            estado,
            version_tarifa as tarifa
            FROM srvcortes ORDER BY numero_corte DESC";

            $request = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
            return $request;
        }
    }

    $obj = new CorteFacturacion();
    $arrData = $obj->show();
    
    if(!empty($arrData)){
        $pdf = new MYPDF('P','mm','Letter', true, 'UTF-8', false);
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('IDEALSAS');
        $pdf->SetTitle('CORTES FACTURACION');
        $pdf->SetSubject('CORTES FACTURACION');
        $pdf->SetKeywords('TCPDF, PDF, example, test, guide');
        
        // set margins
        $pdf->SetMargins(10, 38, 10);// set margins
        $pdf->SetHeaderMargin(38);// set margins
        $pdf->SetFooterMargin(17);// set margins
        $pdf->SetAutoPageBreak(TRUE, 20);
        
        // set some language-dependent strings (optional)
        if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
            require_once(dirname(__FILE__).'/lang/eng.php');
            $pdf->setLanguageArray($l);
        }
        
        // ---------------------------------------------------------
        // add a page
        $pdf->AddPage();
        $pdf->ColoredTable($arrData);
        // column titles
        $header = array('Country', 'Capital', 'Area (sq km)', 'Pop. (thousands)');
        $pdf->Output('reporte-cortes-facturacion.pdf', 'I');
        
    }
?>