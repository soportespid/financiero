<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=uft8");
	require "comun.inc";
	require "funciones.inc";
    require "funcionesSP.inc.php";
	session_start();
	$linkbd=conectar_v7();
	$linkbd -> set_charset("utf8");
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es">
	<head>
		<meta http-equiv="Content-Type" content="text/html" charset="utf8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=9"/>
		<title>:: Reporte Subsidios</title>
		<link href="css/css2.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
        <link href="css/cssSP.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<script type="text/javascript" src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
        <script type="text/javascript" src="css/calendario.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
        <script type='text/javascript' src='JQuery/jquery-2.1.4.min.js'></script>
        <script src="sweetalert2/dist/sweetalert2.min.js"></script>
		<link rel="stylesheet" href="sweetalert2/dist/sweetalert2.min.css">
		<script type="text/javascript" src="css/programas.js"></script>
        
		<script>
			$(window).load(function () {
				$('#cargando').hide();
			});

			function generarInforme()
			{
				var fechaInicial = document.getElementById('fechaini').value;
                var fechaFinal = document.getElementById('fechafin').value;

				if (fechaInicial != '' && fechaFinal != '') {

                    document.form2.oculto.value='2';
					document.form2.submit();
                } 
                else {
                    Swal.fire({
                        icon: 'warning',
                        title: 'Ingrese ambas fechas'
                    })
                }
			}

			function callprogress(vValor)
			{
				document.getElementById("getprogress").innerHTML = vValor;
				document.getElementById("getProgressBarFill").innerHTML = '<div class="ProgressBarFill" style="width: '+vValor+'%;"></div>';
				document.getElementById("titulog1").style.display='block';
				document.getElementById("progreso").style.display='block';
				document.getElementById("getProgressBarFill").style.display='block';
			}  

			function excell()
			{
				document.form2.action="serv-excel-reporteIGAC-acueducto.php";
				document.form2.target="_BLANK";
				document.form2.submit();
			}
		</script> 
		<?php titlepag();?>
	</head>
	<body>
        <IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
		<span id="todastablas2"></span>
		<table>
			<tr><script>barra_imagenes("serv");</script><?php cuadro_titulos();?></tr>	 
			<tr><?php menu_desplegable("serv");?></tr>
			<tr>
				<td colspan="3" class="cinta">
					<a href="#" class="mgbt"><img src="imagenes/add2.png"/></a>
					<a href="#" class="mgbt"><img src="imagenes/guardad.png" title="Guardar"/></a>
					<a href="#" class="mgbt"><img src="imagenes/buscad.png" title="Buscar"/></a>
					<a onclick="mypop=window.open('plan-agenda.php','','');mypop.focus()" class="mgbt"><img src="imagenes/agenda1.png" title="Agenda" /></a>
					<a onclick="mypop=window.open('serv-principal.php','','');mypop.focus();" class="mgbt"><img src="imagenes/nv.png" title="Nueva ventana"></a>
					<a onclick="mypop=window.open('<?php echo $url2; ?>','','');mypop.focus();" class="mgbt"><img src="imagenes/duplicar_pantalla.png" title="Duplicar pesta&ntilde;a"></a>
					<a href="serv-menuInformes"><img src="imagenes/iratras.png" class="mgbt" alt="Atrás"></a>
                </td>
			</tr>
		</table>

		<form name="form2" method="post">
			<table class="inicio ancho" style="width:99.5%">
				<tr>
					<td class="titulos" colspan="7">Reporte Ingresos Generales</td>
					<td class="cerrar" style="width:7%" onClick="location.href='serv-principal.php'">Cerrar</td>
				</tr>
				
				<tr>
                    <td  class="tamano01" >Fecha Inicial: </td>
					<td>
						<input type="search" name="fechaini" id="fechaini" title="DD/MM/YYYY" value="<?php echo $_POST['fechaini']; ?>" onKeyUp="return tabular(event,this)" onKeyDown="mascara(this,'/',patron,true)" maxlength="10" class="colordobleclik" placeholder="DD/MM/YYYY" onDblClick="displayCalendarFor('fechaini');" autocomplete="off" onChange="">
					</td>

					<td class="tamano01" >Fecha Final: </td>
					<td>
						<input type="search" name="fechafin"  id="fechafin" title="DD/MM/YYYY"  value="<?php echo $_POST['fechafin']; ?>" onKeyUp="return tabular(event,this) " onKeyDown="mascara(this,'/',patron,true)" maxlength="10" class="colordobleclik" placeholder="DD/MM/YYYY" onDblClick="displayCalendarFor('fechafin');" autocomplete="off" onChange="">
					</td> 

					<td style="padding-bottom:0px">
                        <em class="botonflechaverde" id="filtro" onclick="generarInforme();">Generar Informe</em>
                    </td>
					
					<?php
						echo"
							<td>
								<div id='titulog1' style='display:none; float:left'></div>
								<div id='progreso' class='ProgressBar' style='display:none; float:left'>
									<div class='ProgressBarText'><span id='getprogress'></span>&nbsp;% </div>
									<div id='getProgressBarFill'></div>
								</div>
							</td>";
					?>
				</tr>
			</table>

            <div id="cargando" style=" position:absolute;left: 46%; bottom: 45%">
				<img src="imagenes/loading.gif" style=" width: 80px; height: 80px"/>
			</div>

            <div class="subpantalla" style="height:60%; width:99.2%;">
				<table class='inicio' align='center' width='99%'>
                    <tr>
                        <td colspan='90' class='titulos'>Resultados Busqueda:</td>
                    </tr>
                    <tr class="titulos2" style='text-align:center;'>
                            <td style="width: 5%;">Código de servicio</td>
                            <td>Nombre servicio</td>
                            <td>Total recaudado</td>							
                        </tr>
                    <?php
                        if(@ $_POST['oculto']=="2")
                        {
                            $contador = 0;
                            $iter  = 'saludo1a';
							$iter2 = 'saludo2';

                            $sqlServicios = "SELECT id, nombre FROM srvservicios";
                            $resServicios = mysqli_query($linkbd, $sqlServicios);
                            $serviciosEncontrados = mysqli_num_rows($resServicios);
                            while ($rowServicios = mysqli_fetch_assoc($resServicios)) {

                                $fec = explode("/", $_POST['fechaini']);
                                $fechaInicial = $fec[2].'-'.$fec[1].'-'.$fec[0];

                                $fec = explode("/", $_POST['fechafin']);
                                $fechaFinal = $fec[2].'-'.$fec[1].'-'.$fec[0];

                                $debito = 0;
                                $credito = 0;
                                $totalRecaudado = 0;

                                $sqlDetallesIngresos = "SELECT SUM(DF.debito) FROM srv_recaudo_factura AS RF INNER JOIN srvdetalles_facturacion AS DF ON RF.numero_factura = DF.numero_facturacion WHERE RF.fecha_recaudo BETWEEN '$fechaInicial' AND '$fechaFinal' AND DF.id_servicio = '$rowServicios[id]' AND DF.tipo_movimiento = '201' AND DF.id_tipo_cobro = '12' AND RF.estado = 'ACTIVO'";
                                $resDetallesIngresos = mysqli_query($linkbd, $sqlDetallesIngresos);
                                $rowDetallesIngresos = mysqli_fetch_row($resDetallesIngresos); 

								$sqlAcuerdo = "SELECT COALESCE(SUM(valor_abonado),0) FROM srv_acuerdo_cab WHERE fecha_acuerdo BETWEEN '$fechaInicial' AND '$fechaFinal' AND estado_acuerdo != 'Anulado'";
								$resAcuerdo = mysqli_query($linkbd, $sqlAcuerdo);
								$rowAcuerdo = mysqli_fetch_row($resAcuerdo);
                                $valorAcuerdo = round(($rowAcuerdo[0] / $serviciosEncontrados),2);

                                $totalRecaudado = $rowDetallesIngresos[0] + $valorAcuerdo;
                    ?>
                                <tr class='<?php echo $iter ?>' style='text-transform:uppercase; text-align:center;'>
                                    <td> <?php echo $rowServicios['id'] ?> </td>
                                    <td> <?php echo $rowServicios['nombre'] ?> </td>
                                    <td> <?php echo "$".number_format($totalRecaudado,2) ?> </td>
							    </tr>
                    <?php
                                $contador++;
                                $porcentaje = $contador * 100 / $serviciosEncontrados;

                                echo"
                                    <script>
                                        progres='".round($porcentaje)."';callprogress(progres);
                                        
                                    </script>";

                                flush();
                                ob_flush();
                                usleep(5);
                            }						
                        }
                    ?>
                </table>
            </div>

			<input type="hidden" name="oculto" id="oculto" value="1"/>
		</form>
	</body>
</html>