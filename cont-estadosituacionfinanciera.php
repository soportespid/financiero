<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	ini_set('max_execution_time',3600);

	require "comun.inc";
	require "funciones.inc";
	require "validaciones.inc";
	
	session_start();
	$linkbd_v7 = conectar_v7();
	$linkbd_v7 -> set_charset("utf8");

	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");

?>
<!DOCTYPE >
<html lang="es">
	<head>
		<meta charset="utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1"/>
		<meta name="viewport" content="user-scalable=no">
		<title>:: IDEAL 10 - Contabilidad</title>
		<link href="favicon.ico" rel="shortcut icon"/>
        <link href="css/css2.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="css/calendario.js"></script>
		<script type="text/javascript" src="css/programas.js"></script>
		<script type="text/javascript" src="jquery-1.11.0.min.js"></script> 
		<script>
		$(window).load(function () { $('#cargando').hide();});
		function despliegamodal2(_valor,v,cuenta,saldo)
			{
				document.getElementById("bgventanamodal2").style.visibility=_valor;
				num_periodo=document.getElementById("periodo").value;

				if(_valor=="hidden"){document.getElementById('ventana2').src="";}
				else 
				{
					if(v==1){
						document.getElementById('ventana2').src="cuentasbancarias-ventana02.php?tipoc=D&obj01=banco&obj02=nbanco&obj03=&obj04=cb&obj05=ter";
					}else{
						//document.getElementById('ventana2').src="notascontabilidad.php?periodo1="+num_periodo+"&cuenta="+cuenta+"&saldo="+saldo;
					}
				}
				
			}

			function excell()
            {
                document.form2.action="cont-var_trimestra_excel.php";
                document.form2.target="_BLANK";
                document.form2.submit(); 
                document.form2.action="";
                document.form2.target="";
            }

			function pdf()
			{
				document.form2.action="pdfbalance.php";
				document.form2.target="_BLANK";
				document.form2.submit();  
				document.form2.action="";
				document.form2.target="";
			}
			function validar(){document.form2.submit(); }
			function validarchip(){document.form2.vchip.value=1;document.form2.submit();}
			function next(){document.form2.pasos.value=parseFloat(document.form2.pasos.value)+1; document.form2.submit();}
			function generar(){document.form2.genbal.value=1;document.form2.submit();}
			function guardarbalance(){document.form2.gbalance.value=1;document.form2.submit();}
		
			function cargarotro(){document.form2.cargabal.value=1; document.form2.submit();}
			function checktodos()
			{
				cali=document.getElementsByName('ctes[]');
				for (var i=0;i < cali.length;i++) 
				{ 
					if (document.getElementById("todoscte").checked == true)
					{cali.item(i).checked = true;document.getElementById("todoscte").value=1;}
					else{cali.item(i).checked = false;document.getElementById("todoscte").value=0;}
				}	
			}
			
			
			
			function checktodosn()
			{
				cali=document.getElementsByName('nctes[]');
				for (var i=0;i < cali.length;i++) 
				{ 
					if (document.getElementById("todoscten").checked == true) 
					{cali.item(i).checked = true;document.getElementById("todoscten").value=1;	 }
					else{cali.item(i).checked = false;document.getElementById("todoscten").value=0;}
				}	
			}
		</script>
	</head>
	<body>
		<div class="loading" id="divcarga"><span>Cargando...</span></div>
        <IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
        <span id="todastablas2"></span>
		<table>
    		<tr><script>barra_imagenes("cont");</script><?php cuadro_titulos();?></tr>	 
    		<tr><?php menu_desplegable("cont");?></tr>
			<tr>
  				<td colspan="3" class="cinta">
					<a href="cont-estadosituacionfinanciera.php" class="mgbt"><img src="imagenes/add.png" title="Nuevo"/></a>
					<a class="mgbt"><img src="imagenes/guardad.png"/></a>
					<a class="mgbt"><img src="imagenes/buscad.png"/></a>
					<a onClick="mypop=window.open('plan-agenda.php','','');mypop.focus()" class="mgbt"><img src="imagenes/agenda1.png" title="Agenda" /></a>
					<a onClick="mypop=window.open('cont-principal.php','','');mypop.focus();" class="mgbt"><img src="imagenes/nv.png" title="Nueva ventana"></a>
                    <a onClick="mypop=window.open('<?php echo $url2; ?>','','');mypop.focus();" class="tooltip bottom mgbt"><img src="imagenes/duplicar_pantalla.png" title="Duplicar pesta&ntilde;a"></a>
					<a onClick="pdf()" class="mgbt"><img src="imagenes/print.png" title="Imprimir"></a>
					
					<a href="cont-estadosfinancieros.php" class="mgbt"><img src="imagenes/iratras.png" title="Atr&aacute;s"></a>
					<img src="imagenes/excel.png" title="Excel" onClick='excell()' class="mgbt"/>
					<a><img class="icorot" src="imagenes/reload.png" title="Refrescar" onClick="generar()"/></a>
				</td>
         	</tr>
    	</table>
  		<form name="form2" action="cont-estadosituacionfinanciera.php"  method="post" enctype="multipart/form-data" >
		  	
 			<?php
                
 				$vigusu=vigencia_usuarios($_SESSION['cedulausu']);
 				if($_POST['reglas']==1){$chkchip=" checked";}
 				else {$chkchip=" ";}
 				if(!$_POST['oculto']){
                    $_POST['pasos']=1;
                    $_POST['oculto']=1;
                    $_POST['fechaIni'] = date("d/m/Y");
                }
   				$vact=$vigusu;    
	  			//*** PASO 2		  
				$sqlr = "select *from configbasica where estado='S'";
				$res = mysqli_query($linkbd_v7, $sqlr);
				while($row = mysqli_fetch_row($res))
	 			{
  					$_POST['nitentidad'] = $row[0];
  					$_POST['entidad'] = $row[1];
					$_POST['codent'] = $row[8];
				}
				if($_POST['oculto']==""){echo"<script>document.getElementById('divcarga').style.display='none';</script>";}
	 		?> 
            <input type="hidden" name="oculto" id="oculto" value="<?php echo $_POST['oculto'] ?>">
            <input type="hidden" name="pasos" id="pasos" value="<?php echo $_POST['pasos'] ?>">
            <input type="hidden" name="periodo" id="periodo" value="<?php echo $_POST['periodo'] ?>"> 
			
    		<table class="inicio" align="center"> 
      		<tr>
        		<td class="titulos" colspan="10">Variacion Trimestral</td>
        		<td class="cerrar" style="width:7%;"><a href="cont-principal.php">&nbsp;Cerrar</a></td>
     		</tr>   
      		<tr>
            	
                <td class="saludo1" style="width:10%;">Fecha:</td>
                <td style="width:10%;">
                    <input type="text" name="fechaIni"  value="<?php echo $_POST['fechaIni']?>" onKeyUp="return tabular(event,this)" id="fc_1198971545" title="DD/MM/YYYY" onDblClick="displayCalendarFor('fc_1198971545');" class="colordobleclik" autocomplete="off" onChange="" readonly>
                </td>
                
                <td></td>
                <td class="saludo3" style="width:11%;">Codigo Entidad</td>
                <td><input name="codent" type="text" value="<?php echo $_POST['codent']?>" readonly></td>
         	</tr>
      		<tr>
            	<td class="saludo3">Nit:</td>
                <td>
                	<input type="hidden" name="nivel" value="2">
                    <input type="hidden" name="genbal"  value=" <?php echo $_POST['genbal']?>">
                    <input type="text" name="nitentidad" value="<?php echo $_POST['nitentidad']?>" style="width:98%;"readonly>
               	</td>
                <td class="saludo3">Entidad:</td>
                <td colspan="3">
                	<input type="text" name="entidad" value="<?php echo $_POST['entidad']?>" style="width:100%;"  readonly></td>
                <td colspan="2"> 
                	<input type="button" name="genera" value=" Generar " onClick="generar()"> 
                </td>
       		</tr>  
    	</table>   
		<div id="divdet" class="subpantallap" style="height:58.2%; width:99.6%; overflow-x:hidden;"> 	
   			<?php
				//echo "$_POST[genbal]";
  				//**** para sacar la consulta del balance se necesitan estos datos ********
  				//**** nivel, mes inicial, mes final, cuenta inicial, cuenta final, cc inicial, cc final  

				
				
				if($_POST['genbal']==1 && $_POST['gbalance']==0 )
				{	
 					//**** para sacar la consulta del balance se necesitan estos datos ********
  					//**** nivel, mes inicial, mes final, cuenta inicial, cuenta final, cc inicial, cc final  
					$oculto=$_POST['oculto'];
					if($_POST['oculto'])
					{
						$critcons=" and comprobante_det.tipo_comp <> 19 ";
						
						$sqlrcc = "select id_cc from centrocosto where entidad='N'";
						$rescc = mysqli_query($linkbd_v7, $sqlrcc);
						while($rowcc = mysqli_fetch_row($rescc))
						{ $critcons2.=" and comprobante_det.centrocosto <> '$rowcc[0]' ";}

						$_POST['cuenta1']='1';
						$_POST['cuenta2']='9999999999999';
						$horaini=date('h:i:s');		
  						$niveles=array();
  						$sqlr="Select * from nivelesctas  where estado='S' order by id_nivel";
						$resp = mysqli_query($linkbd_v7, $sqlr);
						while ($row = mysqli_fetch_row($resp)){$niveles[]=$row[4];}
						/*
						ereg( "([0-9]{1,2})/([0-9]{1,2})/([0-9]{2,4})", $_POST[fecha],$fecha);
						$fechaf1=$fecha[3]."-".$fecha[2]."-".$fecha[1];
						ereg( "([0-9]{1,2})/([0-9]{1,2})/([0-9]{2,4})", $_POST[fecha2],$fecha);
						$fechaf2=$fecha[3]."-".$fecha[2]."-".$fecha[1];
						ereg( "([0-9]{1,2})/([0-9]{1,2})/([0-9]{2,4})", $_POST[fecha],$fecha);
						$fechaf=$fecha[3]."-".$fecha[2]."-".$fecha[1];
						$fechafa2=mktime(0,0,0,$fecha[2],$fecha[1],$fecha[3]);
						$f1=$fechafa2;	
						$f2=mktime(0,0,0,$fecha[2],$fecha[1],$fecha[3]);
						$fechafa=$vigusu."-01-01";
						$fechafa2=date('Y-m-d',$fechafa2-((24*60*60)));*/

                        $partsFecha = explode('/', $_POST['fechaIni']);

						$mes1=$partsFecha[1];
						$mes2=$partsFecha[1];
                        $_POST['vigencias'] = $partsFecha[2];
						$vig=$_POST['vigencias']-1;
						$numero = cal_days_in_month(CAL_GREGORIAN,$mes2,$vig);
						
						$_POST['fecha']='01'.'/'.$mes1.'/'.$_POST['vigencias'];
						$_POST['fecha3']='01'.'/'.$mes1.'/'.$vig;
						$_POST['fecha4']=$numero.'/'.$mes2.'/'.$vig;
						$_POST['fecha2']=intval(date("t",$mes2)).'/'.$mes2.'/'.$_POST['vigencias'];	
						/* echo "Fechas:".$_POST['fecha'].'  '.$_POST['fecha2']."<br>";	
						echo "Fechas:".$_POST['fecha3'].'  '.$_POST['fecha4']."<br>"; */	
						/* ereg( "([0-9]{1,2})/([0-9]{1,2})/([0-9]{2,4})", $_POST['fecha'],$fecha);
						$fechaf1=$fecha[3]."-".$fecha[2]."-".$fecha[1]; */

						preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST['fecha'], $fecha);
						$fechaf1 = $fecha[3]."-".$fecha[2]."-".$fecha[1];

						$fechafa2=mktime(0,0,0,$fecha[2],$fecha[1],$fecha[3]);
						$fechafa2=date('Y-m-d',$fechafa2-((24*60*60)));
					
						/* ereg( "([0-9]{1,2})/([0-9]{1,2})/([0-9]{2,4})", $_POST['fecha2'],$fecha);
						$fechaf2=$fecha[3]."-".$fecha[2]."-".$fecha[1]; */

						preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST['fecha2'], $fecha);
						$fechaf2 = $fecha[3]."-".$fecha[2]."-".$fecha[1];

						/* ereg( "([0-9]{1,2})/([0-9]{1,2})/([0-9]{2,4})", $_POST['fecha3'],$fecha);
						$fechaf=$fecha[3]."-".$fecha[2]."-".$fecha[1]; */

						preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST['fecha3'], $fecha);
						$fechaf3 = $fecha[3]."-".$fecha[2]."-".$fecha[1];

						$fechafa3=mktime(0,0,0,$fecha[2],$fecha[1],$fecha[3]);
						$fechafa3=date('Y-m-d',$fechafa3-((24*60*60)));

						/* $fechafa2=mktime(0,0,0,$fecha[2],$fecha[1],$fecha[3]);
						$f1=$fechafa2; */	
						$f2=mktime(0,0,0,$fecha[2],$fecha[1],$fecha[3]);	
						$fechafa=$_POST['vigencias']."-01-01";
						/* $fechafa2=date('Y-m-d',$fechafa2); */

						
						//Borrar el balance de prueba anterior
						/* ereg( "([0-9]{1,2})/([0-9]{1,2})/([0-9]{2,4})", $_POST['fecha4'],$fecha);
						$fechaf = $fecha[3]."-".$fecha[2]."-".$fecha[1]; */
						preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST['fecha4'], $fecha);
						$fechaf4 = $fecha[3]."-".$fecha[2]."-".$fecha[1];

						/* echo "Fechas2:".$fechafa3.'  '.$fechaf4.'  '.$fechaf3;	 */
						
						$sqlr2="select distinct digitos, posiciones from nivelesctas where estado='S' ORDER BY id_nivel DESC ";
						$resn=mysqli_query($linkbd_v7, $sqlr2);
						$rown=mysqli_fetch_row($resn);
						$nivmax=$rown[0];
						$dignivmax=$rown[1];
						//continuar**** creacion balance de prueba
						//$namearch="archivos/".$_SESSION[usuario]."balanceprueba.csv";
						//$Descriptor1 = fopen($namearch,"w+"); 
						//fputs($Descriptor1,"CODIGO;CUENTA;SALDO ANTERIOR;DEBITO;CREDITO;SALDO FINAL\r\n");
						$bases = [];
						$usuariosBase = [];
						$sqlrBases = "SELECT base, usuario FROM redglobal";
						$resBases = mysqli_query($linkbd_v7, $sqlrBases);
						while($rowBases = mysqli_fetch_row($resBases)){
							array_push($bases, $rowBases[0]);
							array_push($usuariosBase, $rowBases[1]);
						}

  						echo "<table class='inicio' >
								<tr>
									<td colspan='8' class='titulos'>Balance de Prueba</td>
								</tr>";
  						echo "<tr>
									<td align='center' class='titulos2'>Codigo</td>
									<td align='center' class='titulos2'>Cuenta</td>
									<td align='center' class='titulos2'>Saldo Final - Vigencia Anterior</td>
									<td align='center' class='titulos2'>Saldo Final - Vigencia Actual</td>
							</tr>";
                        $tam=$niveles[$_POST['nivel']-1];
						$crit1=" and left(cuenta,$tam)>='$_POST[cuenta1]' and left(cuenta,$tam)<='$_POST[cuenta2]' ";
						$sqlr2 = "select distinct cuenta,tipo from cuentasnicsp where estado ='S' and length(cuenta)=$tam ".$crit1." group by cuenta,tipo order by cuenta ";
						$rescta = mysqli_query($linkbd_v7, $sqlr2);
						$i=0;
						//echo $sqlr2;
						$pctas=array();
						$pctasb[]=array();
						while ($row = mysqli_fetch_row($rescta)) 
						{
							$pctas[]=$row[0];
							$pctasb["$row[0]"][0]=$row[0];
						  	$pctasb["$row[0]"][1]=0;
						  	$pctasb["$row[0]"][2]=0;
						  	$pctasb["$row[0]"][3]=0;
						}
						mysqli_free_result($rescta);
						$tam=$niveles[$_POST['nivel']-1];
						// MOVIMIENTOS DEL PERIODO
						$sqlr3="SELECT DISTINCT
						SUBSTR(comprobante_det.cuenta,1,$tam),
						sum(comprobante_det.valdebito)-
						sum(comprobante_det.valcredito)
						FROM comprobante_det, comprobante_cab
						WHERE comprobante_cab.tipo_comp = comprobante_det.tipo_comp
						AND comprobante_det.numerotipo = comprobante_cab.numerotipo
						AND comprobante_cab.estado = 1
						AND (comprobante_det.valdebito > 0
						OR comprobante_det.valcredito > 0)
						AND comprobante_cab.fecha BETWEEN '$fechaf1' AND '$fechaf2'
						AND comprobante_det.tipo_comp <> 7 AND comprobante_det.tipo_comp <> 102 AND comprobante_det.tipo_comp <> 100 AND comprobante_det.tipo_comp <> 101 AND comprobante_det.tipo_comp <> 103 AND comprobante_det.tipo_comp<>104 AND comprobante_det.tipo_comp <> 13 ".$critcons." ".$critconscierre."
						AND SUBSTR(comprobante_det.cuenta,1,$tam) >= '$_POST[cuenta1]' AND SUBSTR(comprobante_det.cuenta,1,$tam) <='$_POST[cuenta2]'
						$busqueda
						GROUP BY SUBSTR(comprobante_det.cuenta,1,$tam)
						ORDER BY comprobante_det.cuenta";

						foreach($bases as $key => $base){
							$linkmulti = conectar_Multi($base, $usuariosBase[$key]);
							$linkmulti -> set_charset("utf8");
							$res = mysqli_query($linkmulti, $sqlr3);
							while ($row =mysqli_fetch_row($res)){
								$pctasb["$row[0]"][0]=$row[0];
								$pctasb["$row[0]"][2]+=$row[1];
							}
						}

						$sqlrTipoComp = "SELECT codigo FROM tipo_comprobante WHERE codigo=102";
						$resTipoComp = mysqli_query($linkbd_v7, $sqlrTipoComp);
						$rowTipoComp = mysqli_fetch_row($resTipoComp);
						if($rowTipoComp[0]!='')
						{
							$tipo_comp = 102;
						}
						else
						{
							$tipo_comp = 7;
						}

						//**** SALDO INICIAL ***
						$sqlr3="SELECT DISTINCT
						SUBSTR(comprobante_det.cuenta,1,$tam),
						sum(comprobante_det.valdebito)-
						sum(comprobante_det.valcredito)
     					FROM comprobante_det, comprobante_cab
    					WHERE comprobante_cab.tipo_comp = comprobante_det.tipo_comp
						AND comprobante_det.numerotipo = comprobante_cab.numerotipo
						AND comprobante_cab.estado = 1
						AND (comprobante_det.valdebito > 0
               			OR comprobante_det.valcredito > 0)         
						AND comprobante_det.tipo_comp = $tipo_comp
						AND SUBSTR(comprobante_det.cuenta,1,$tam) >= '$_POST[cuenta1]' AND SUBSTR(comprobante_det.cuenta,1,$tam) <='$_POST[cuenta2]'      
		  		  		AND comprobante_det.centrocosto like '%$_POST[cc]%' ".$critcons." ".$critcons2."
		  				GROUP BY SUBSTR(comprobante_det.cuenta,1,$tam)
   						ORDER BY comprobante_det.cuenta";

						foreach($bases as $key => $base){
							$linkmulti = conectar_Multi($base, $usuariosBase[$key]);
							$linkmulti -> set_charset("utf8");
							$res = mysqli_query($linkmulti, $sqlr3);
							while ($row =mysqli_fetch_row($res)){
								$pctasb["$row[0]"][0]=$row[0];
								$pctasb["$row[0]"][2]+=$row[1];
							}
						}

   						/* $res=mysqli_query($linkbd_v7, $sqlr3);
						while ($row = mysqli_fetch_row($res)) 
 						{
							$pctasb["$row[0]"][0]=$row[0];
							$pctasb["$row[0]"][1]=$row[1];
 						} */
						//*******MOVIMIENTOS PREVIOS PERIODO
						if($fechafa2>='2018-01-01')
						{
							$fecini='2018-01-01';
							$sqlr3="SELECT DISTINCT
							SUBSTR(comprobante_det.cuenta,1,$tam),
							sum(comprobante_det.valdebito)-
							sum(comprobante_det.valcredito)
							FROM comprobante_det, comprobante_cab
							WHERE comprobante_cab.tipo_comp = comprobante_det.tipo_comp
							AND comprobante_det.numerotipo = comprobante_cab.numerotipo
							AND comprobante_cab.estado = 1
							AND (comprobante_det.valdebito > 0
							OR comprobante_det.valcredito > 0)
							AND comprobante_det.tipo_comp <> 100
							AND comprobante_det.tipo_comp <> 101
							AND comprobante_det.tipo_comp <> 103
							AND comprobante_det.tipo_comp <> 102
							AND comprobante_det.tipo_comp <> 104
							AND comprobante_det.cuenta!=''
							AND comprobante_det.tipo_comp <> 7  $critcons $critcons2
							AND comprobante_cab.fecha BETWEEN '$fecini' AND '$fechafa2'
							AND SUBSTR(comprobante_det.cuenta,1,$tam) >= '$_POST[cuenta1]' AND SUBSTR(comprobante_det.cuenta,1,$tam) <='$_POST[cuenta2]'
							AND comprobante_det.centrocosto like '%$_POST[cc]%'
							GROUP BY SUBSTR(comprobante_det.cuenta,1,$tam)
							ORDER BY comprobante_det.cuenta";

							foreach($bases as $key => $base){
								$linkmulti = conectar_Multi($base, $usuariosBase[$key]);
								$linkmulti -> set_charset("utf8");
								$res = mysqli_query($linkmulti, $sqlr3);
								while ($row =mysqli_fetch_row($res)){
									$pctasb["$row[0]"][0]=$row[0];
									$pctasb["$row[0]"][2]+=$row[1];
								}
							}

							/* $res=mysqli_query($linkbd_v7, $sqlr3);
							//  sort($pctasb[]);
							while ($row =mysqli_fetch_row($res)) 
							{
								$pctasb["$row[0]"][0]=$row[0];
								$pctasb["$row[0]"][1]+=$row[1]; 
							} */ 
						}
						/*$sqlr3="SELECT DISTINCT
								SUBSTR(comprobante_det.cuenta,1,$tam),
								sum(comprobante_det.valdebito)-
								sum(comprobante_det.valcredito)
							FROM comprobante_det, comprobante_cab
							WHERE     comprobante_cab.tipo_comp = comprobante_det.tipo_comp
								AND comprobante_det.numerotipo = comprobante_cab.numerotipo
								AND comprobante_cab.estado = 1
								AND (   comprobante_det.valdebito > 0
									OR comprobante_det.valcredito > 0)
								AND comprobante_det.tipo_comp <> 7  
								AND comprobante_cab.fecha BETWEEN '' AND '$fechafa2'
								AND SUBSTR(comprobante_det.cuenta,1,$tam) >= '$_POST[cuenta1]' AND SUBSTR(comprobante_det.cuenta,1,$tam) <='$_POST[cuenta2]'		
								AND comprobante_det.centrocosto like '%$_POST[cc]%'
								GROUP BY SUBSTR(comprobante_det.cuenta,1,$tam)
						ORDER BY comprobante_det.cuenta";
						$res=mysql_query($sqlr3,$linkbd);
						// echo $sqlr3;
						//  sort($pctasb[]);
						while ($row =mysql_fetch_row($res)) 
						{
						$pctasb["$row[0]"][0]=$row[0];
						$pctasb["$row[0]"][1]+=$row[1]; 
						} */

						// MOVIMIENTOS DEL PERIODO
						$sqlr3="SELECT DISTINCT
						SUBSTR(comprobante_det.cuenta,1,$tam),
						sum(comprobante_det.valdebito)-
						sum(comprobante_det.valcredito)
						FROM comprobante_det, comprobante_cab
						WHERE comprobante_cab.tipo_comp = comprobante_det.tipo_comp
						AND comprobante_det.numerotipo = comprobante_cab.numerotipo
						AND comprobante_cab.estado = 1
						AND (comprobante_det.valdebito > 0
						OR comprobante_det.valcredito > 0)
						AND comprobante_cab.fecha BETWEEN '$fechaf3' AND '$fechaf4'
						AND comprobante_det.tipo_comp <> 7 AND comprobante_det.tipo_comp <> 102 AND comprobante_det.tipo_comp <> 100 AND comprobante_det.tipo_comp <> 101 AND comprobante_det.tipo_comp <> 103 AND comprobante_det.tipo_comp<>104 AND comprobante_det.tipo_comp <> 13 ".$critcons." ".$critconscierre."
						AND SUBSTR(comprobante_det.cuenta,1,$tam) >= '$_POST[cuenta1]' AND SUBSTR(comprobante_det.cuenta,1,$tam) <='$_POST[cuenta2]'
						$busqueda
						GROUP BY SUBSTR(comprobante_det.cuenta,1,$tam)
						ORDER BY comprobante_det.cuenta";

						foreach($bases as $key => $base){
							$linkmulti = conectar_Multi($base, $usuariosBase[$key]);
							$linkmulti -> set_charset("utf8");
							$res = mysqli_query($linkmulti, $sqlr3);
							while ($row =mysqli_fetch_row($res)){
								$pctasb["$row[0]"][0]=$row[0];
								$pctasb["$row[0]"][1]+=$row[1];
							}
						}
 
						//**** SALDO INICIAL ***
						$sqlr3="SELECT DISTINCT
						SUBSTR(comprobante_det.cuenta,1,$tam),
						sum(comprobante_det.valdebito)-
						sum(comprobante_det.valcredito)
     					FROM comprobante_det, comprobante_cab
    					WHERE comprobante_cab.tipo_comp = comprobante_det.tipo_comp
						AND comprobante_det.numerotipo = comprobante_cab.numerotipo
						AND comprobante_cab.estado = 1
						AND (comprobante_det.valdebito > 0
               			OR comprobante_det.valcredito > 0)         
						AND comprobante_det.tipo_comp = $tipo_comp 
						AND SUBSTR(comprobante_det.cuenta,1,$tam) >= '$_POST[cuenta1]' AND SUBSTR(comprobante_det.cuenta,1,$tam) <='$_POST[cuenta2]'      
		  		  		AND comprobante_det.centrocosto like '%$_POST[cc]%' ".$critcons." ".$critcons2."
		  				GROUP BY SUBSTR(comprobante_det.cuenta,1,$tam)
   						ORDER BY comprobante_det.cuenta";

						foreach($bases as $key => $base){
							$linkmulti = conectar_Multi($base, $usuariosBase[$key]);
							$linkmulti -> set_charset("utf8");
							$res = mysqli_query($linkmulti, $sqlr3);
							while ($row =mysqli_fetch_row($res)){
								$pctasb["$row[0]"][0]=$row[0];
								$pctasb["$row[0]"][1]+=$row[1];
							}
						}

   						/* $res=mysqli_query($linkbd_v7, $sqlr3);
  						// echo $sqlr3;
						while ($row =mysqli_fetch_row($res)) 
						{
							$pctasb["$row[0]"][0]=$row[0];
							$pctasb["$row[0]"][2]=$row[1];
 						} */
						//*******MOVIMIENTOS PREVIOS PERIODO

						if($fechafa3>='2018-01-01')
						{
							$fecini='2018-01-01';
							$sqlr3="SELECT DISTINCT
							SUBSTR(comprobante_det.cuenta,1,$tam),
							sum(comprobante_det.valdebito)-
							sum(comprobante_det.valcredito)
							FROM comprobante_det, comprobante_cab
							WHERE comprobante_cab.tipo_comp = comprobante_det.tipo_comp
							AND comprobante_det.numerotipo = comprobante_cab.numerotipo
							AND comprobante_cab.estado = 1
							AND (comprobante_det.valdebito > 0
							OR comprobante_det.valcredito > 0)
							AND comprobante_det.tipo_comp <> 100
							AND comprobante_det.tipo_comp <> 101
							AND comprobante_det.tipo_comp <> 103
							AND comprobante_det.tipo_comp <> 102
							AND comprobante_det.tipo_comp <> 104
							AND comprobante_det.cuenta!=''
							AND comprobante_det.tipo_comp <> 7 $critcons $critcons2
							AND comprobante_cab.fecha BETWEEN '$fecini' AND '$fechafa3' 
							AND SUBSTR(comprobante_det.cuenta,1,$tam) >= '$_POST[cuenta1]' AND SUBSTR(comprobante_det.cuenta,1,$tam) <='$_POST[cuenta2]'		
							AND comprobante_det.centrocosto like '%$_POST[cc]%'
							GROUP BY SUBSTR(comprobante_det.cuenta,1,$tam)
							ORDER BY comprobante_det.cuenta";

							foreach($bases as $key => $base){
								$linkmulti = conectar_Multi($base, $usuariosBase[$key]);
								$linkmulti -> set_charset("utf8");
								$res = mysqli_query($linkmulti, $sqlr3);
								while ($row =mysqli_fetch_row($res)){
									$pctasb["$row[0]"][0]=$row[0];
									$pctasb["$row[0]"][1]+=$row[1];
								}
							}
						}
/* 
   						$res=mysqli_query($linkbd_v7, $sqlr3);
						// echo $sqlr3;
						//  sort($pctasb[]);
						while ($row = mysqli_fetch_row($res)) 
						{
							$pctasb["$row[0]"][0]=$row[0];
							$pctasb["$row[0]"][2]+=$row[1]; 
 						} */ 
						
						for ($y=0;$y<$_POST['nivel'];$y++)
						{ 
							$lonc=count($pctasb);
							//foreach($pctasb as $k => $valores )
							$k=0;
							// echo "lonc:".$lonc;
							//   while($k<$lonc)
							foreach($pctasb as $k => $valores )
							{
								if (strlen($pctasb[$k][0])>=$niveles[$y-1])
								{		 
									$ncuenta=substr($pctasb[$k][0],0,$niveles[$y-1]);
									if($ncuenta!='')
	  								{
										//echo "<br>N:".$niveles[$y-1]." : cuenta:".$k." NC:".$ncuenta."  ".$pctasb["$ncuenta"][1]."  ".$pctasb["$ncuenta"][2]."  ".$pctasb["$ncuenta"][3];	
										$pctasb["$ncuenta"][0]=$ncuenta;
										$pctasb["$ncuenta"][1]+=$pctasb[$k][1];
										$pctasb["$ncuenta"][2]+=$pctasb[$k][2];
										//echo "<br>N:".$niveles[$y-1]." : cuenta:".$k." NC:".$ncuenta."  ".$pctasb["$ncuenta"][1]."  ".$pctasb["$ncuenta"][2]."  ".$pctasb["$ncuenta"][3];	
	  								}
	 							}
	   							$k++;
							}
 						}
 						$sqlr="create  temporary table usr_session (id int(11),cuenta varchar(20),nombrecuenta varchar(100),saldoinicial double,debito double,credito double,saldofinal double)";
						mysqli_query($linkbd_v7, $sqlr);
						$i=1;
						foreach($pctasb as $k => $valores )
						{	 
							if(($pctasb[$k][1]<0 || $pctasb[$k][1]>0) || ($pctasb[$k][2]<0 || $pctasb[$k][2]>0))
							{
								//echo "<br> hola $k --> ".$pctasb[$k][1];
								
                                
                                if(strlen($pctasb[$k][0]) == 2){
                                    $nomc=existecuentanicsp($pctasb[$k][0]);
                                    $sqlr="insert into usr_session (id,cuenta,nombrecuenta,saldoinicial,saldofinal) values($i,'".$pctasb[$k][0]."','".$nomc."',".$pctasb[$k][1].",".$pctasb[$k][2].")";
                                    // $sqlr="insert into usr_session (id,cuenta,nombrecuenta,saldoinicial,debito,credito,saldofinal) values($i,'".$pctasb[$k][0]."','".$nomc."',".$viva.",".$pctasb[$k][2].",".$pctasb[$k][3].",".$saldofinal.")";
                                    mysqli_query($linkbd_v7, $sqlr);
                                    //echo "<br>".$sqlr;
                                    $i+=1;

                                }
								
							}
	 						//echo "<br>cuenta:".$k."  ".$pctasb[$k][1]."  ".$pctasb[$k][2]."  ".$pctasb[$k][3];	
						}
						$sqlr="select *from usr_session order by cuenta";
						$res=mysqli_query($linkbd_v7, $sqlr);
						$_POST['tsaldoant']=0;
	 					$_POST['tsaldofinal']=0;
						$cuentachipno=array();
						$namearch2="archivos/VARIACIONES_TRIMESTRALES.txt";
						$Descriptor2 = fopen($namearch2,"w+");
						fputs($Descriptor2,"S\t".$_POST['codent']."\t".$_POST['periodo']."\t".$_POST['vigencias']."\tCGN2016C01_VARIACIONES_TRIMESTRALES_SIGNIFICATIVAS\r\n");

	 					$namearch="archivos/".$_SESSION['usuario']."balanceprueba-nivel$_POST[nivel].csv";
						$Descriptor1 = fopen($namearch,"w+"); 
						fputs($Descriptor1,"CODIGO;CUENTA;SALDO ANTERIOR;SALDO FINAL\r\n");
						$co='saludo1a';
						$co2='saludo2';
						$totalrelativo = 0;
						while($row=mysqli_fetch_assoc($res))
						{	  //echo "hola ".$row["saldofinal"];
							if(strlen($row["cuenta"])==6)
							{
								$sqlrchip="select count(*) from chipcuentas where cuenta='".$row['cuenta']."'";
								$reschip=mysqli_query($linkbd_v7, $sqlrchip);
								$rowchip=mysqli_fetch_row($reschip);
								if($rowchip[0]==0)
	    						{
									$cuentachipno[]=$row["cuenta"];
									// $ncuentachipno[]=buscacuenta($row[1]);		 
								}
	  						}
	   						$negrilla="";
							$notas="";
							if (strlen($row["cuenta"])==($dignivmax) )
							{
								// $negrilla=" "; 
								//$_POST[tsaldoant]+=$row[3];
								//$_POST[tdebito]+=$row[4];
								//$_POST[tcredito]+=$row[5];
		 					}
		 					
	 						if($niveles[$_POST['nivel']-1]==strlen($row["cuenta"]))
		  					{
								$negrilla=" ";  
								$_POST['tsaldoant']+=$row["saldoinicial"];			  
								$_POST['tsaldofinal']+=$row["saldofinal"];			  	
		  					}
							/* $total=(($row["saldofinal"]-$row["saldoinicial"])/$row["saldoinicial"])*100;
							$estilo='';
							if(abs($total)>15)
							{
								$estilo='background-color:yellow;';
							}
		   */
		  
							echo "<tr class='$co' style='text-transform:uppercase; $estilo' onMouseOver=\"anterior=this.style.backgroundColor;this.style.backgroundColor='#40b3ff';\"
							onMouseOut=\"this.style.backgroundColor=anterior\" >
							<td align='center' width='8%' $negrilla>".$row['cuenta']."</td>
							<td $negrilla>".$row['nombrecuenta']."</td>
							<td width='12%' $negrilla align='right'>".number_format($row["saldoinicial"],2,".",",")."</td>
							<td width='12%' $negrilla align='right'>".number_format($row["saldofinal"],2,".",",")."</td>";
							echo "<input type='hidden' name='dcuentas[]' value= '".$row["cuenta"]."'> 
									<input type='hidden' name='dncuentas[]' value= '".$row["nombrecuenta"]."'>
									<input type='hidden' name='dsaldoant[]' value= '".round($row["saldoinicial"],2)."'> 
									<input type='hidden' name='dsaldo[]' value= '".round($row["saldofinal"],2)."'>
								</td>
							</tr>" ;
	 						fputs($Descriptor1,$row["cuenta"].";".$row["nombrecuenta"].";".number_format($row["saldoinicial"],3,",","").";".number_format($row["saldofinal"],3,",","")."\r\n");

	  						fputs($Descriptor2,"D\t".$row["cuenta"]."\t".$row["nombrecuenta"]."\t".round($row["saldoinicial"],2)."\t".round($row["saldofinal"],2)."\r\n");
							$aux=$co;
							$co=$co2;
							$co2=$aux;
							$i=1+$i;
						}
						fclose($Descriptor1);
						fclose($Descriptor2);
  						echo "<tr class='$co'>
							<td colspan='2'>
							</td>
							<td class='$co' align='right'>".number_format($_POST['tsaldoant'],2,".",",")."
								<input type='hidden' name='tsaldoant' value= '$_POST[tsaldoant]'>
							</td>
							<td class='$co' align='right'>".number_format($_POST['tsaldofinal'],2,".",",")."
								<input type='hidden' name='tsaldofinal' value= '$_POST[tsaldofinal]'>
							</td>
			
			
						</tr>";  
  						$horafin=date('h:i:s');	
  						//echo "<DIV class='ejemplo'>INICIO:$horaini FINALIZO: $horafin</DIV>";
					}

					if ($_POST['vchip']==1)
 					{
						?>
						<div class="inicio"> 
						<div class="titulos">ERROR CUENTAS INEXISTENTES VALIDACION CHIP: <input type="text" name="erroresent" size="5" value="<?php echo count($cuentachipno) ?>" readonly></div>
						<?php
 					}

					foreach($cuentachipno as $cch)
	 				{		
	  					$ncta=existecuenta($cch);
	  					//echo "<div class='saludo3'>".strtoupper($cch)."  -  ".strtoupper($ncta)."</div>";		
	 				}
					?>
				</div>
 				<?php
	 		}
	 		echo "<script>document.getElementById('divcarga').style.display='none';</script>";
			if($_POST['gbalance']==1 && $_POST['gchip']!='1' )
			{
		    	$fechab=date('Y-m-d');
				$sqlr="Delete from chipcarga_cab where vigencia=$_POST[vigencias] and periodo=$_POST[periodo]";
				mysqli_query($linkbd_v7, $sqlr);
				$sqlr="Delete from chipcarga_det where vigencia=$_POST[vigencias] and periodo=$_POST[periodo]";
				mysqli_query($linkbd_v7, $sqlr);	
				$sqlr="insert into chipcarga_cab (entidad, nombre_entidad, vigencia, fecha, periodo, estado) values ('$_POST[nitentidad]','$_POST[entidad]','$_POST[vigencias]','$fechab','$_POST[periodo]','S') ";
				mysqli_query($linkbd_v7, $sqlr);	
				//echo "Error".mysql_error($linkbd); 
				$cerror=0;
				$cexit=0;
				for($x=0;$x<count($_POST['dcuentas']);$x++)
				{
					$sqlr="insert into chipcarga_det (entidad, vigencia, periodo, cuenta, saldoinicial, debitos, creditos, saldofinal, saldofincte, saldofincteno) values ('$_POST[nitentidad]','$_POST[vigencias]','$_POST[periodo]',".$_POST['dcuentas'][$x].",".$_POST['dsaldoant'][$x].",".$_POST['ddebitos'][$x].",".$_POST['dcreditos'][$x].",".$_POST['dsaldo'][$x].",'','') ";
		 			if(mysqli_query($linkbd_v7, $sqlr))	
		   			{
						$cexit+=1;
		   			}
		  			else
		    		{
						$cerror+=1;
						//echo "$sqlr<br>Error".mysql_error($linkbd); 
					}
		  		}	
		  		echo "<div class='inicio'>CUENTAS INSERTADAS:".$cexit." <img src='imagenes\confirm.png'></div>";
  		  		echo "<div class='inicio'>CUENTAS NO INSERTADAS:".$cerror." <img src='imagenes\alert.png'></div>";		 
	   		} 
			//**FIN PASO 2
			//***** PASO 3 ****			 
			//**** FIN PASO 3 ****
			//***** PASO 3 ****		 
	  		?>      	
     	
      	<table class="inicio">
    		<tr class="saludo3"><td></td><td></td><td><?php echo $totalsalini ?></td><td><?php echo $totaldeb ?></td><td><?php echo $totalcred ?></td><td><?php echo $totalsalfin ?>  <input type="hidden" name="gchip" value="<?php //echo $_POST[gchip] ?>"></td></tr>
      	</table>
	</div>
	

	<div id="bgventanamodal2">
		<div id="ventanamodal2">
			<IFRAME  src="" name="buster" marginWidth=0 marginHeight=0 frameBorder=0 id="ventana2" frameSpacing=0 style="left:500px; width:900px; height:500px; top:200;"> 
			</IFRAME>
		</div>
	</div>
		

		
</form></td></tr>
<tr><td></td></tr>      
</table>
</body>
</html>
