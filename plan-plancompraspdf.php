<?php
    require_once 'Librerias/core/Helpers.php';
    require_once 'tcpdf/tcpdf.php';
    require_once 'Librerias/core/Mysql.php';
    session_start();
    // ini_set('display_errors', '1');
    // ini_set('display_startup_errors', '1');
    // error_reporting(E_ALL);
    class RegistroExportController {
        public function exportPdf(){
            if(!empty($_SESSION)){
                $con = new Mysql();            
				$data = json_decode($_POST['data'],true);

				$pdf = new MYPDF('P','mm','Letter', true, 'iso-8859-1', false);
				$pdf->SetDocInfoUnicode (true);
				// set document information
				$pdf->SetCreator(PDF_CREATOR);
				$pdf->SetAuthor('IDEALSAS');
				$pdf->SetTitle('PAA');
				$pdf->SetSubject('PAA');
				$pdf->SetKeywords('TCPDF, PDF, example, test, guide');
				$pdf->SetMargins(10, 38, 10);// set margins
				$pdf->SetHeaderMargin(38);// set margins
				$pdf->SetFooterMargin(17);// set margins
				$pdf->SetAutoPageBreak(TRUE, 20);// set auto page breaks
				// set some language-dependent strings (optional)
				if (@file_exists(dirname(__FILE__).'/lang/spa.php'))
				{
					require_once(dirname(__FILE__).'/lang/spa.php');
					$pdf->setLanguageArray($l);
				}
				$pdf->SetFillColor(255,255,255);
				$pdf->AddPage();
				$pdf->SetFont('helvetica','B',9);
				$pdf->SetFillColor(153,221,255);
				$pdf->MultiCell(190,5,"PLAN ANUAL DE ADQUISICIONES","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
				$pdf->ln();
				$pdf->SetFont('helvetica','',8);

				$pdf->MultiCell(12,8,"Código","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
				$pdf->MultiCell(40,8,"Descripción","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
				$pdf->MultiCell(17,8,"Fecha inicio","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
				$pdf->MultiCell(15,8,"Duración estimada","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
				$pdf->MultiCell(20,8,"Modalidad","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
				$pdf->MultiCell(18,8,"Fuente","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
				$pdf->MultiCell(21,8,"Valor estimado","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
				$pdf->MultiCell(21,8,"Valor vig actual","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
				$pdf->MultiCell(26,8,"Responsable","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
				$pdf->ln();
				$pdf->SetFillColor(245,245,245);
				$pdf->SetFont('helvetica','',8);
		
				foreach ($data as $d) {
					$height = $pdf->getNumLines($d['descripcion']) * 8;

					$pdf->SetFont('helvetica','',8);
					$pdf->SetFillColor(255,255,255);
					$pdf->MultiCell(12,$height,$d["codigo"],"LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
					$pdf->MultiCell(40,$height,$d["descripcion"],"LRBT",'J',true,0,'','',true,0,false,true,0,'T',true);
					$pdf->MultiCell(17,$height,$d["fecha"],"LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
					$pdf->MultiCell(15,$height,"$d[duracion_estimada] Meses","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
					$pdf->MultiCell(20,$height,$d["nombre_modalidad"],"LRBT",'J',true,0,'','',true,0,false,true,0,'T',true);
					$pdf->MultiCell(18,$height,$d["nombre_fuente"],"LRBT",'J',true,0,'','',true,0,false,true,0,'T',true);
					$pdf->MultiCell(21,$height,"$".number_format($d["valor_estimado"], 0),"LRBT",'R',true,0,'','',true,0,false,true,0,'M',true);
					$pdf->MultiCell(21,$height,"$".number_format($d["valor_vigencia"], 0),"LRBT",'R',true,0,'','',true,0,false,true,0,'M',true);
					$pdf->MultiCell(26,$height,$d["contacto_respon"],"LRBT",'J',true,1,'','',true,0,false,true,0,'T',true);
		
					$getY = $pdf->getY();
					if ($getY > 240) {
						$pdf->AddPage();
					}
				}
				
				$strFile = 'plan_anual_adquisiciones';
				$pdf->Output($strFile.'.pdf', 'I');
            }
            die();
        }

    }
    class MYPDF extends TCPDF {

		public function Header(){
			$request = configBasica();
            $strNit = $request['nit'];
            $strRazon = $request['razonsocial'];

			//Parte Izquierda
			$this->Image('../../../imagenes/escudo.jpg',  13, 13, 20, 20, 'JPG', '', 'T', true, 300, '', false, false, 0, false, false, false);// Logo
			$this->SetFont('helvetica','B',8);
			$this->SetY(10);
			$this->RoundedRect(10, 10, 190, 25, 1,'');
			$this->Cell(0.1);
			$this->Cell(26,25,'','R',0,'L');
			$this->SetY(8);
			$this->SetX(40);
			$this->SetFont('helvetica','B',9);
			$this->Cell(140,15,strtoupper("$strRazon"),0,0,'C');
			$this->SetFont('helvetica','B',7);
			$this->SetY(12);
			$this->SetX(40);
			$this->Cell(140,15,'NIT: '.$strNit,0,0,'C');
			//*****************************************************************************************************************************
			$this->SetFont('helvetica','B',9);
			$this->SetY(23);
			$this->SetX(36);
            $this->Cell(164,12,"LISTADO DE PLAN ANUAL DE ADQUISICIONES",'T',0,'C');

			//**********************************************************
			$this->SetFont('times','B',10);
			$this->ln(12);
           
			//**********************************************************
            if(ESTADO=="N"){
                $img_file = "../../../assets/img/anulado.png";
                $this->SetAlpha(0.35);
                $this->Image($img_file, 0, 20, 250, 280, '', '', '', false, 300, '', false, false, 300);
                $this->SetAlpha(1);
            }
		}
		public function Footer(){

			$request = configBasica();
            $strDireccion = $request['direccion'] != "" ? "Dirección: ".strtoupper($request['direccion']) : "";
            $strWeb = $request['web'] != "" ? "Pagina web: ".strtoupper($request['web']) :"";
            $strEmail = $request['email'] !="" ? "Email: ".strtoupper($request['email']) :"";
            $strTelefono = $request['telefono'] != "" ? "Telefonos: ".$request['telefono'] : "";
            $strUsuario = searchUser($_SESSION['cedulausu'])['nom_usu'];
			$strNick = $_SESSION['nickusu'];
			$strFecha = date("d/m/Y H:i:s");
			$strIp = $_SERVER['REMOTE_ADDR'];

            $this->setY(280);
			$this->SetFont('helvetica', 'I', 8);
			$txt = <<<EOD
			$strDireccion $strTelefono
			$strEmail $strWeb
			EOD;
			$this->SetFont('helvetica', 'I', 6);
			$this->Cell(190,10,'','T',0,'T');
			$this->ln(2);
			$this->Write(0, $txt , '', 0, 'C', true, 0, false, false, 0);

			$this->Cell(50, 10, 'Hecho por: '.$strUsuario, 00, false, 'C', 0, '', 0, false, 'T', 'M');
			$this->Cell(30, 10, 'Impreso por: '.$strNick, 0, false, 'C', 0, '', 0, false, 'T', 'M');
			$this->Cell(30, 10, 'IP: '.$strIp, 0, false, 'C',0, '', 0, false, 'T', 'M');
			$this->Cell(30, 10, 'Fecha: '.$strFecha, 0, false, 'C', 0, '', 0, false, 'T', 'M');
			$this->Cell(30, 10, 'IDEAL.10 S.A.S', 0, false, 'C', 0, '', 0, false, 'T', 'M');
            $this->Cell(20, 10, 'Pagina '.$this->getAliasNumPage().'/'.$this->getAliasNbPages(), 0, false, 'C', 0, '', 0, false, 'T', 'M');
		}
	}

    if($_POST){
        $obj = new RegistroExportController();
        if($_POST['action'] == "pdf"){
            $obj->exportPdf();
        }
    }

?>
