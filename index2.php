<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require 'comun.inc';
	require 'funciones.inc';
	require 'funcionesSP.inc.php';
	require_once 'PHPExcel/Classes/PHPExcel.php';
	$archivo = "informacion/listacamposnuevos.xlsx";
	$inputFileType = PHPExcel_IOFactory::identify($archivo);
	$objReader = PHPExcel_IOFactory::createReader($inputFileType);
	$objPHPExcel = $objReader->load($archivo);
	$sheet = $objPHPExcel->getSheet(0);
	$highestRow = $sheet->getHighestRow();
	$highestColumn = $sheet->getHighestColumn();
	session_start();
	session_destroy();
	date_default_timezone_set("America/Bogota");
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
	$datin = datosiniciales();
	$sqlr="select *from configbasica where estado='S' ";
	$razonsocial=strtoupper(replaceChar((mysqli_query($linkbd, $sqlr)->fetch_assoc()['razonsocial'])));
?>
<!DOCTYPE >
<html lang="es">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1">
		<meta name="viewport" content="user-scalable=no">
		<title>:: FINANCIERO</title>
		<link href="css/css2.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/style.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="sweetalert2/dist/sweetalert2.min.css" rel="stylesheet">
		<script src="sweetalert2/dist/sweetalert2.min.js"></script>
		<script type='text/javascript' src='JQuery/jquery-2.1.4.min.js'></script>

		<script>
			$(document).ready(() => {
				$('body').ripples({ resolution: 512, dropRadius: 10, perturbance: 0.04 });
			});

			function despliegamodalm(_valor,mensa){
				document.getElementById("bgventanamodalm").style.visibility = _valor;

				if (_valor == "hidden") {
					document.getElementById('ventanam').src = "";
				} else {
					document.getElementById('ventanam').src = "ventana-mensaje3.php?titulos=" + mensa;
				}
			}

			$(document).keydown(e => {
				if (e.key == 'Enter') {
					document.form1.submit();
				}
			});
		</script>
		<?php titlepag();?>
	</head>
	<body class="login-body">
		<div class="login-container">
			<form name="form1" method="post" action="index2.php" class="login-form">
				<input type="hidden" name="vistab" id="vistab" value="<?php echo $_POST['vistab'];?>"/>
				<input type="hidden" name="oculto" id="oculto" value="1"/>
				<div class="login-img d-flex justify-center mb-3">
					<img src="./assets/img/logo_ideal.jpeg" alt="" height="100px">
				</div>
				<h2 class="text-center fs-3">BIENVENIDO A <?=$razonsocial?>!</h2>
				<p class="mb-0 mt-2 text-center" >Inicia sesión para acceder a nuestros módulos.</p>
				<div class="form-control mt-4 mb-3">
					<label class="form-label m-0" for="">Nombre de usuario</label>
					<div class="form-icon">
						<span>
							<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960"><path d="M480-480q-66 0-113-47t-47-113q0-66 47-113t113-47q66 0 113 47t47 113q0 66-47 113t-113 47ZM160-160v-112q0-34 17.5-62.5T224-378q62-31 126-46.5T480-440q66 0 130 15.5T736-378q29 15 46.5 43.5T800-272v112H160Zm80-80h480v-32q0-11-5.5-20T700-306q-54-27-109-40.5T480-360q-56 0-111 13.5T260-306q-9 5-14.5 14t-5.5 20v32Zm240-320q33 0 56.5-23.5T560-640q0-33-23.5-56.5T480-720q-33 0-56.5 23.5T400-640q0 33 23.5 56.5T480-560Zm0-80Zm0 400Z"/></svg>
						</span>
						<input type="text" name="user" id="user" value="<?php echo @$_POST['user'];?>">
					</div>
				</div>
				<div class="form-control">
					<label class="form-label m-0" for="">Contraseña</label>
					<div class="form-icon">
						<span>
							<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960"><path d="M240-80q-33 0-56.5-23.5T160-160v-400q0-33 23.5-56.5T240-640h40v-80q0-83 58.5-141.5T480-920q83 0 141.5 58.5T680-720v80h40q33 0 56.5 23.5T800-560v400q0 33-23.5 56.5T720-80H240Zm0-80h480v-400H240v400Zm240-120q33 0 56.5-23.5T560-360q0-33-23.5-56.5T480-440q-33 0-56.5 23.5T400-360q0 33 23.5 56.5T480-280ZM360-640h240v-80q0-50-35-85t-85-35q-50 0-85 35t-35 85v80ZM240-160v-400 400Z"/></svg>
						</span>
						<input type="password" name="pass" id="pass" value="<?php echo @$_POST['pass'];?>">
					</div>
				</div>
				<div class="form-control fw-bold mb-3">
					<button class="mt-2 btn btn-blue w-100" onClick="document.form1.submit();">Iniciar sesión</button>
				</div>
				<p class="text-center" style="font-size:12px">Este Software es propiedad de IDEAL 10 SAS, su uso debe ser autorizado.</p>
				<?php
					if($_POST['oculto'] == ""){
						$_POST['vistab'] = 'hidden';
						$archivo = '.env';
						$contenido = file_get_contents($archivo);
						$contenido = explode("\n",$contenido);
						$_POST['basesfun'] = str_replace('DB_NAME=','',$contenido[1]);
						//actualizar tablas
						$num = 0;
						for ($row = 2; $row <= $highestRow; $row++){
							$num++;
							$tipo = $sheet->getCell("A".$row)->getValue();
							$nomtabla = $sheet->getCell("B".$row)->getValue();
							$nomcampo = $sheet->getCell("C".$row)->getValue();
							$instruccion = $sheet->getCell("D".$row)->getValue();
							$nomcampo2 = $sheet->getCell("E".$row)->getValue();
							$instruccion2 = $sheet->getCell("F".$row)->getValue();
							switch ($tipo){
								//******Crear campos en la tabla si no existen
								case "1":{
									if($nomtabla != '' && $nomcampo != ''){
										$sqlrcb = "SHOW COLUMNS FROM $nomtabla LIKE '$nomcampo'";
										$rescb = mysqli_query($linkbd,$sqlrcb);
										if ( mysqli_num_rows($rescb) == 1 ){}
										else{
											if($instruccion != ''){
												$sqlrat = $instruccion;
												mysqli_query($linkbd,$sqlrat);
											}
										}
									}
								}break;
								//******Crear la tabla si no existen
								case "2":{
									if($nomtabla != ''){
										$sqlrcb = "SHOW TABLES LIKE '$nomtabla'";
										$rescb = mysqli_query($linkbd,$sqlrcb);
										if ( mysqli_num_rows($rescb) == 1 ){}
										else{
											if($instruccion != ''){
												$partestablas = explode('<->', $instruccion); ///dividir
												$numpartes = count($partestablas);
												for($x=0;$x<$numpartes;$x++){
													$sqlrat = $partestablas[$x];
													mysqli_query($linkbd,$sqlrat);
												}
											}
										}
									}
								}break;
								//******modificar nombre campo o Crear campo en la tabla si no existen
								case "3":{
									if($nomtabla != '' && $nomcampo != ''){
										$sqlrcb = "SHOW COLUMNS FROM $nomtabla LIKE '$nomcampo'";
										$rescb = mysqli_query($linkbd,$sqlrcb);
										if ( mysqli_num_rows($rescb) == 1 ){
											if($instruccion != ''){
												$sqlrat = $instruccion;
												mysqli_query($linkbd,$sqlrat);
											}
										}else{
											if($nomcampo2 != ''){
												$sqlrcb = "SHOW COLUMNS FROM $nomtabla LIKE '$nomcampo2'";
												$rescb = mysqli_query($linkbd,$sqlrcb);
												if ( mysqli_num_rows($rescb) == 1 ){}
												else{
													if($instruccion2 != ''){
														$sqlrat = $instruccion2;
														mysqli_query($linkbd,$sqlrat);
													}
												}
											}
										}
									}
								}break;
								//******modificar el tipo de campo en una tabla
								case "4":{
									if($nomtabla != '' && $nomcampo != ''){
										$sqlrcb = "SELECT COLUMN_TYPE FROM information_schema.columns where TABLE_SCHEMA='$datin[0]' and TABLE_NAME='$nomtabla' AND COLUMN_NAME='$nomcampo'";
										$rescb = mysqli_query($linkbd,$sqlrcb);
										if ( mysqli_num_rows($rescb) == 1 ){
											$rows = mysqli_fetch_row($rescb);
											if($rows[0] != $nomcampo2){
												$sqlrat = $instruccion;
												mysqli_query($linkbd,$sqlrat);
											}
										}
									}
								}break;
								//******guardar en informacion en una tabla si no existe
								case "5":{
									if($nomtabla != '' && $nomcampo != ''){
										$sqlrcb = "SELECT 1 FROM $nomtabla where $nomcampo";
										$rescb = mysqli_query($linkbd,$sqlrcb);
										if ( mysqli_num_rows($rescb) >= 1 ){}
										else{
											if($instruccion != ''){
												$sqlrat = $instruccion;
												mysqli_query($linkbd,$sqlrat);
											}
										}
									}
								}break;
								//******Crear funcion MySQL si no existen
								case "6":{
									if($nomtabla != ''){
										$sqlrcb = "SHOW CREATE FUNCTION $nomtabla";
										$rescb = mysqli_query($linkbd,$sqlrcb);
										if ( mysqli_num_rows($rescb) == 1 ){
											echo "sale";
										}else{
											if($instruccion != ''){
												//$sqlrat=$instruccion;
												//mysqli_query($linkbd,$sqlrat);
												$dbname = $datin[0];
												$dbhost = $datin[1];
												$dbuser = $datin[2];
												$dbpass = $datin[3];
												$backupFile1 = "funcionesmysql/$instruccion";
												$mysqldumppath1 = '"../../mysql/bin/mysql.exe"';
												//$command1 = "$mysqldumppath1 -h $dbhost -u$dbuser -p$dbpass $dbname < $backupFile1";
												//system($command1);
											}
										}
									}
								}break;
								//******Crear campos en la tabla si no existen y rellenar campos si es necesario
								case "7":{
									if($nomtabla != '' && $nomcampo != ''){
										$sqlrcb = "SHOW COLUMNS FROM $nomtabla LIKE '$nomcampo'";
										$rescb = mysqli_query($linkbd,$sqlrcb);
										if ( mysqli_num_rows($rescb) == 1 ){}
										else{
											if($instruccion != ''){
												$partestablas= explode('<->', $instruccion); ///dividir
												$numpartes=count($partestablas);
												for($x=0;$x<$numpartes;$x++){
													$sqlrat=$partestablas[$x];
													mysqli_query($linkbd,$sqlrat);
												}
											}
										}
									}
								}break;
								//******modificar el archivo del vinculo en opciones
								case "8":{
									if($nomtabla != '' && $nomcampo != ''){
										$sql= "UPDATE opciones SET ruta_opcion = '$instruccion' WHERE nom_opcion = '$nomtabla' AND modulo = '$nomcampo'";
										mysqli_query($linkbd,$sql);
									}
								}break;
								//******modificar el archivo del vinculo en opciones buscando nombre de Link
								case "9":{
									if($nomtabla != '' && $nomcampo != ''){
										$sql= "UPDATE opciones SET ruta_opcion = '$nomcampo' WHERE ruta_opcion = '$nomtabla' ";
										mysqli_query($linkbd,$sql);
									}
								}break;
                                case "10":{
									if($nomtabla != '' && $instruccion != ''){
										$sql= "DELETE FROM $nomtabla WHERE $instruccion";
										mysqli_query($linkbd,$sql);
									}
								}break;
							}
						}
					}
				?>

				<?php
					if ($_POST['user'] != "" && $_POST['pass'] != "" && $_POST['oculto']=="1"){

						$user = $linkbd->real_escape_string($_POST['user']);
						$pass = $linkbd->real_escape_string($_POST['pass']);

						$valap = validasusuarioypass($user, $pass);
						if ($valap == ""){
							echo"
							<script>
								Swal.fire({
									icon: 'error',
									title: 'Error!',
									text: 'El Usuario o la Contraseña no son correctos',
									confirmButtonText: 'Continuar',
									confirmButtonColor: '#FF121A',
									timer: 3000
								});
							</script>";
						}else{
							echo"<script>document.form1.action='principal';document.form1.submit();</script>";
						}
					}else if($_POST['user']=="" && $_POST['oculto']=="1"){
						echo"
							<script>
								Swal.fire({
									icon: 'error',
									title: 'Error!',
									text: 'Debe Ingresar Nombre del Usuario',
									confirmButtonText: 'Continuar',
									confirmButtonColor: '#FF121A',
									timer: 3000
								});
							</script>
						";
					}else if($_POST['oculto']=="1"){
						echo"
						<script>
							Swal.fire({
								icon: 'error',
								title: 'Error!',
								text: 'Debe Ingresar La Clave Asignada',
								confirmButtonText: 'Continuar',
								confirmButtonColor: '#FF121A',
								timer: 3000
							});
						</script>
						";
					}
				?>

				<?php
					if($_POST['oculto']=="4")
					{
						$archivo = 'comun.inc';
						$contenido = file_get_contents($archivo);
						$contenido = explode("\n",$contenido);
						$contenido[2] = "//".$_POST['basesfun']."//";
						$contenido[5] = '	$datin[0] = \''.$_POST['basesfun'].'\';';
						$contenido = implode("\n",$contenido);
						$abrir = fopen($archivo,'w');
						fwrite($abrir,$contenido);
						fclose($abrir);
						$archivo = '.env';
						$contenido = file_get_contents($archivo);
						$contenido = explode("\n",$contenido);
						$contenido[1] = 'DB_NAME='.$_POST['basesfun'];
						$contenido = implode("\n",$contenido);
						$abrir = fopen($archivo,'w');
						fwrite($abrir,$contenido);
						fclose($abrir);
						echo"<script>location.href='index2.php'</script>";
					}
					//Funciones auditoria
					/*
					Para agregar nuevos registros en la tabla funciones auditoria,
					debe indicarle el nombre de la tabla y un array con los nuevos registros
					*/

					insertFuncionesAuditoria("trans_funciones",["Registro automotor"]);
					insertFuncionesAuditoria("trans_funciones",["Recaudo impuesto"]);
				?>
			</form>
		</div>
	</body>
</html>
