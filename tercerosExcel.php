<?php
	require_once 'PHPExcel/Classes/PHPExcel.php';
    include '/PHPExcel/Classes/PHPExcel/IOFactory.php';
	require "comun.inc";
	require "funciones.inc";
    require "Librerias/core/Helpers.php";
    ini_set('max_execution_time',99999999);
    header("Content-type: application/json");
	session_start();
	$linkbd=conectar_v7();

	$objPHPExcel = new PHPExcel();
	
	$objPHPExcel->getProperties()
		->setCreator("IDEAL10")
		->setLastModifiedBy("IDEAL10")
		->setTitle("Listado de terceros")
		->setSubject("SP")
		->setDescription("SP")
		->setKeywords("SP")
		->setCategory("SERVICIOS PUBLICOS");
	$objPHPExcel->getActiveSheet()->mergeCells('A1:D1');
	$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A1', "Listado de terceros");
	$objFont=$objPHPExcel->getActiveSheet()->getStyle('A1')->getFont();
	$objFont->setName('Courier New');
	$objFont->setSize(15);
	$objFont->setBold(true);
	$objFont->setUnderline(PHPExcel_Style_Font::UNDERLINE_SINGLE);
	$objFont->getColor()->setARGB( PHPExcel_Style_Color::COLOR_BLACK);
	$objAlign=$objPHPExcel->getActiveSheet()->getStyle('A1')->getAlignment(); 
	$objAlign->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
	$objAlign->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
	$objPHPExcel-> getActiveSheet ()
		-> getStyle ("A2:D2")
		-> getFill ()
		-> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
		-> getStartColor ()
		-> setRGB ('A6E5F3');
	$objPHPExcel-> getActiveSheet ()
		-> getStyle ("A1")	
		-> getFill ()
		-> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
		-> getStartColor ()
		-> setRGB ('A6E5F3');
	$borders = array(
		'borders' => array(
			'allborders' => array(
				'style' => PHPExcel_Style_Border::BORDER_THIN,
				'color' => array('argb' => 'FF000000'),
			)
		),
	);

    $datos = json_decode($_POST['datos'], true);
    
	$objPHPExcel->getActiveSheet()->getStyle('A2:D2')->applyFromArray($borders);
	$objPHPExcel->setActiveSheetIndex(0)
        ->setCellValue('A2', 'Item')
        ->setCellValue('B2', 'Documento')
        ->setCellValue('C2', 'Razón social')
        ->setCellValue('D2', 'Nombre');     
	$i=3;
    $index = 1;
	foreach ($datos as $key => $dato) {
        
		$objPHPExcel->setActiveSheetIndex(0)

		->setCellValueExplicit ("A$i", $index, PHPExcel_Cell_DataType :: TYPE_STRING)
		->setCellValueExplicit ("B$i", $dato["cedulanit"], PHPExcel_Cell_DataType :: TYPE_STRING)
		->setCellValueExplicit ("C$i", $dato["razonsocial"], PHPExcel_Cell_DataType :: TYPE_STRING)
		->setCellValueExplicit ("D$i", $dato["nombre"], PHPExcel_Cell_DataType :: TYPE_STRING);

        $objPHPExcel->getActiveSheet()->getStyle("A$i:D$i")->applyFromArray($borders);
        $index++;
        $i++;
	}


	$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);

	$objPHPExcel->getActiveSheet()->setTitle('SP');
	//header('Content-Type: application/vnd.ms-excel');
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
	header('Content-Disposition: attachment;filename="listado_terceros_ideal.xlsx"');
	header('Cache-Control: max-age=0');
	$objWriter=PHPExcel_IOFactory::createWriter($objPHPExcel,'Excel2007');
	$objWriter->save('php://output');
	exit;
?>