<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require 'comun.inc';
	require "funciones.inc";
	require 'funcionesSP.inc.php';
	session_start();
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");

	date_default_timezone_set("America/Bogota");
	$fec = date("d/m/Y");
	software();
	$sqlr="select *from configbasica where estado='S' ";
	$razonsocial=strtoupper(replaceChar((mysqli_query($linkbd, $sqlr)->fetch_assoc()['razonsocial'])));
	$vigencia = date("Y");

	$sqlTareas="SELECT DATE_FORMAT(tb1.fechasig,'%d/%m/%Y') as fecha,tb2.descripcionr as tarea,
	tb1.codigo,tb1.codradicacion,tb1.proceso,tb1.tipot,tb1.estado
	FROM planacresponsables tb1
	INNER JOIN planacradicacion tb2 ON tb2.numeror = tb1.codradicacion
	WHERE tb1.estado='AN' AND tb1.usuariocon = '$_SESSION[cedulausu]' AND YEAR(tb1.fechasig) = '$vigencia'
	AND tb2.usuarior = '$_SESSION[cedulausu]'
	ORDER BY tb1.codigo ASC;";
	$sqlPublicacion = "SELECT *,DATE_FORMAT(fecha_inicio,'%d/%m/%Y') as fecha FROM infor_interes ORDER BY DATE(fecha_inicio) DESC";
	$arrPublicacion = mysqli_fetch_all(mysqli_query($linkbd,$sqlPublicacion),MYSQLI_ASSOC);
	$arrTareas = mysqli_fetch_all(mysqli_query($linkbd,$sqlTareas),MYSQLI_ASSOC);
	$totalTareas = count($arrTareas);
	$totalPublicacion = count($arrPublicacion);
	if (!isset($_SESSION['usuario'])){
		$sqlr = "SELECT * FROM parametros WHERE estado='S'";
		$res = mysqli_query($linkbd,$sqlr);
		while($r = mysqli_fetch_row($res)){$vigencia = $r[1];}
		//*** verificar el username y el pass
		$users = $_POST['user'];
		$pass = $_POST['pass'];
		$sqlr = "SELECT U.nom_usu, R.nom_rol, U.id_rol, U.id_usu, U.foto_usu, U.usu_usu, U.cc_usu FROM usuarios U, roles R WHERE U.usu_usu = '$users' AND U.pass_usu = '$pass' AND U.id_rol = R.id_rol AND U.est_usu='1'";
		$res = mysqli_query($linkbd,$sqlr);
		while($r = mysqli_fetch_row($res)){
			$user = $r[0];
			$perf = $r[1];
			$niv = $r[2];
			$idusu = $r[3];
			$nick = $r[5];
			$dirfoto = $r[4];
			$cedusu = $r[6];
		}
		if ($user == ""){
			header("location: index2.php");
		}else{

			//login correcto
			$_SESSION['vigencia'] = $vigencia;
			$_SESSION['usuario'] = array();
			$_SESSION['usuario'] = $user;
			$_SESSION['perfil'] = $perf;
			$_SESSION['idusuario'] = $idusu;
			$_SESSION['nickusu'] = $nick;
			$_SESSION['cedulausu'] = $cedusu;
			$_SESSION['nivel'] = $niv;
			$_SESSION['linkmod'] = array();
			if($dirfoto == ""){
				$_SESSION['fotousuario'] = "imagenes/usuario_on.png";
			}else{
				$_SESSION["fotousuario"] = "informacion/fotos_usuarios/$dirfoto";
			}
			$corresmensaje = "1";
			$sqlr = "SELECT * FROM usuarios_privilegios WHERE id_usu='$idusu'";
			$row = mysqli_fetch_row(mysqli_query($linkbd,$sqlr));
			//$_SESSION["prcrear"]=$row[1];
			//$_SESSION["preditar"]=$row[2];
			//$_SESSION["prdesactivar"]=$row[3];
			//$_SESSION["preliminar"]=$row[4];
			$_SESSION['prcrear'] = 1;
			$_SESSION['preditar'] = 1;
			$_SESSION['prdesactivar'] = 1;
			$_SESSION['preliminar'] = 1;
			$sqlr = "SELECT valor_inicial, valor_final, descripcion_valor FROM dominios WHERE nombre_dominio='SEPARADOR_NUMERICO'";
			$row = mysqli_fetch_row(mysqli_query($linkbd,$sqlr));
			if($row[0]!=''){
				$_SESSION['spdecimal'] = $row[0];
			}else{
				$_SESSION['spdecimal']= '.';
			}
			if($row[1]!=''){
				$_SESSION['spmillares'] = $row[1];
			}else{
				$_SESSION['spmillares'] = ',';
			}
			if($row[2]!=''){
				$_SESSION['ndecimales'] = $row[2];
			}
			else{
				$_SESSION['ndecimales'] = 2;
			}
			//******************* menuss ************************
			//***** NUEVO REVISION DE LOS MODULOS ***************
			$sqlr = "SELECT DISTINCT (modulos.nombre),modulos.id_modulo,modulos.libre FROM modulos, modulo_rol WHERE modulo_rol.id_rol = $niv AND modulos.id_modulo = modulo_rol.id_modulo GROUP BY (modulos.nombre),modulos.id_modulo,modulos.libre ORDER BY modulos.id_modulo";
            $res = mysqli_query($linkbd,$sqlr);
			while($roww = mysqli_fetch_row($res)){
				$_SESSION['linkmod'][$roww[1]].=$roww[2];
			}
			//verificació de tipo de caracteres
			$sqlr="SELECT valor_inicial, valor_final FROM dominios WHERE nombre_dominio='TIPO_CARACTER_VERPHP'";
			$row = mysqli_fetch_row(mysqli_query($linkbd,$sqlr));
			$_SESSION['VERCARPHPINI'] = $row[0];
			$_SESSION['VERCARPHPFIN'] = $row[1];
			$sqlr = "SELECT valor_inicial, valor_final FROM dominios WHERE nombre_dominio='TIPO_CARACTER_VERPDF'";
			$row = mysqli_fetch_row(mysqli_query($linkbd,$sqlr));
			$_SESSION['VERCARPDFINI'] = $row[0];
			$_SESSION['VERCARPDFFIN'] = $row[1];
			//crear y borrar tabla """temporal"""
			$datin = datosiniciales();
			$hoy = getdate();
			$auxtablatemporal1 = "usr_session_".$hoy['year'];
			$auxtablatemporal2 = "usr_session_".$hoy['year'].$hoy['mon'].$hoy['mday'];
			$_SESSION['tablatemporal'] = $auxtablatemporal2."_".$hoy['hours'].$hoy['minutes'].$hoy['seconds'];
			if($auxtablatemporal1 != '' && $auxtablatemporal2 != ''){
				$sqlr = "SELECT CONCAT('DROP TABLE ' , GROUP_CONCAT(table_name),';') FROM information_schema.tables WHERE table_schema = '$datin[0]' AND table_name LIKE '$auxtablatemporal1%' AND NOT table_name LIKE '$auxtablatemporal2%'";
				$row = mysqli_fetch_row(mysqli_query($linkbd,$sqlr));
				$sqlrdel = $row[0];
				mysqli_query($linkbd,$sqlr);
			}
			//registro log
			$accion = "USUARIO INICIA SESION EN EL SISTEMA";
			$origen = getUserIpAddr();
			generaLogs($_SESSION['nickusu'],'PRI','V',$accion,$origen);
		}
	}
	$infoUser = '
    <p class="text-white fw-normal m-0">
        Usuario: '.substr(ucwords((strtolower($_SESSION['usuario']))),0,14).' |
        Perfil: '.substr(ucwords((strtolower($_SESSION["perfil"]))),0,14).'
    </p>';
    $htmlBtn = '
        <button type="button" onclick="showDropdown(this)" class="btn btn-white d-flex justify-between align-items-center">
            <span class="group-hover:text-white">Perfil</span>
            <svg xmlns="http://www.w3.org/2000/svg"  viewBox="0 -960 960 960" ><path d="M480-480q-66 0-113-47t-47-113q0-66 47-113t113-47q66 0 113 47t47 113q0 66-47 113t-113 47ZM160-160v-112q0-34 17.5-62.5T224-378q62-31 126-46.5T480-440q66 0 130 15.5T736-378q29 15 46.5 43.5T800-272v112H160Zm80-80h480v-32q0-11-5.5-20T700-306q-54-27-109-40.5T480-360q-56 0-111 13.5T260-306q-9 5-14.5 14t-5.5 20v32Zm240-320q33 0 56.5-23.5T560-640q0-33-23.5-56.5T480-720q-33 0-56.5 23.5T400-640q0 33 23.5 56.5T480-560Zm0-80Zm0 400Z"/></svg>
        </button>
        <div class="d-none dropdown-content border border-radius bg-white">
            <ul>
                <li>Usuario: '.substr(ucwords((strtolower($_SESSION['usuario']))),0,14).'</li>
                <li>Perfil: '.substr(ucwords((strtolower($_SESSION["perfil"]))),0,14).'</li>
                <li>Fecha ingreso: '.' '.$fec=date("d/m/Y").'</li>
                <li>Hora ingreso: '.' '.date ( "h:i:s" , $hora ).'</li>
                <hr>
                <li class="d-flex align-items-center">
                    <a href="index2.php"> Cerrar sesion</a>
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960"><path d="M200-120q-33 0-56.5-23.5T120-200v-560q0-33 23.5-56.5T200-840h280v80H200v560h280v80H200Zm440-160-55-58 102-102H360v-80h327L585-622l55-58 200 200-200 200Z"/></svg>
                </li>
            </ul>
        </div>
    ';
    if(empty($_SESSION)){

        $infoUser ='<p class="text-danger m-0 fw-normal">Se ha cerrado la sesion</p>';
        $htmlBtn = '
        <button type="button" onclick="window.location.href='."'index2.php'".'" class="btn btn-white d-flex justify-between align-items-center">
            <span class="group-hover:text-white">Iniciar sesion</span>
        </button>
    ';
    }
?>
<!DOCTYPE >
<html lang="es">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1">
		<meta name="viewport" content="user-scalable=no">
		<title>FINANCIERO</title>
		<link href="css/css2.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
		<link href="css/style.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<script type="text/javascript" src="css/programas.js?v=<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script type="text/javascript" src="funcioneshf.js"></script>
		<script type='text/javascript' src='JQuery/jquery-2.1.4.min.js'></script>
		<?php titlepag();?>

		<style>
			[v-cloak]{display : none;}
		</style>
	</head>
	<body>
        <section id="myapp" v-cloak >
            <div class="main-container">
                <div id="website" @click="window.open('https://ideal-10.com/enlaces_de_interes', '_blank');">
                    <svg fill="#000000" height="800px" width="800px" version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 249.84 249.84" xmlns:xlink="http://www.w3.org/1999/xlink" enable-background="new 0 0 249.84 249.84">
                        <path d="m248.714,152.802c-2.258-8.425-7.66-15.466-15.218-19.829-0.972-0.56-1.985-1.077-2.963-1.514-9.563-4.359-24.283-6.479-45.004-6.479-15.936,0-32.185,1.282-44.013,2.496 14.681-20.208 38.876-56.522 41.043-79.073 0.116-1.125 0.176-2.263 0.176-3.384 0-18.004-14.647-32.652-32.652-32.652-10.107,0-19.157,4.617-25.151,11.853-5.994-7.235-15.045-11.853-25.152-11.853-18.004,0-32.651,14.648-32.651,32.652 0,1.127 0.059,2.266 0.169,3.329 2.171,22.591 26.359,58.915 41.036,79.125-11.841-1.218-28.113-2.504-44.064-2.504-20.699,0-35.406,2.118-44.908,6.45-1.021,0.456-2.035,0.974-3.021,1.543-7.553,4.361-12.955,11.402-15.213,19.827-2.257,8.424-1.098,17.223 3.263,24.775 4.89,8.47 13.333,14.242 22.818,15.865-0.352,0.954-0.663,1.929-0.929,2.923-2.258,8.424-1.099,17.224 3.262,24.776 5.815,10.072 16.659,16.329 28.3,16.329 5.703,0 11.342-1.514 16.307-4.381 0.979-0.566 1.936-1.186 2.795-1.809 18.463-13.165 37.81-52.219 47.98-75.042 10.167,22.826 29.502,61.883 47.934,75.026 0.914,0.661 1.872,1.281 2.842,1.84 4.962,2.865 10.599,4.379 16.303,4.379 11.641,0 22.484-6.257 28.3-16.329 5.052-8.75 5.581-18.89 2.315-27.697 9.491-1.618 17.943-7.392 22.837-15.868 4.358-7.55 5.516-16.349 3.259-24.774zm-166.586-107.782c1.42109e-14-9.733 7.918-17.652 17.651-17.652s17.652,7.919 17.652,17.652c0,4.142 3.357,7.5 7.5,7.5 4.143,0 7.5-3.358 7.5-7.5 0-9.733 7.918-17.652 17.651-17.652s17.652,7.919 17.652,17.652c0,0.606-0.032,1.225-0.102,1.894-1.87,19.457-27.639,57.558-42.71,77.771-15.066-20.208-40.825-58.304-42.701-77.827-0.061-0.607-0.093-1.225-0.093-1.838zm-13.941,174.069c-0.49,0.355-1.011,0.693-1.543,1-2.684,1.55-5.729,2.37-8.804,2.37-6.299,0-12.165-3.383-15.31-8.829-2.357-4.083-2.983-8.84-1.764-13.394 1.221-4.554 4.142-8.361 8.225-10.718 2.406-1.389 3.752-3.911 3.751-6.504 0-1.271-0.324-2.561-1.006-3.741-2.07-3.588-6.656-4.816-10.245-2.745-2.684,1.549-5.728,2.369-8.802,2.369-6.3,0-12.166-3.384-15.31-8.831-4.866-8.429-1.969-19.246 6.456-24.11 0.538-0.31 1.091-0.592 1.695-0.863 5.126-2.337 16.007-5.123 38.738-5.123 18.813,0 38.581,1.927 49.955,3.252-9.976,23.161-30.087,64.495-46.036,75.867zm164.275-49.01c-3.145,5.447-9.01,8.83-15.308,8.83 0,0-0.001,0-0.002,0-3.074-0.001-6.118-0.82-8.802-2.37-3.59-2.072-8.175-0.843-10.246,2.744-0.682,1.181-1.006,2.469-1.006,3.741-0.001,2.593 1.344,5.115 3.75,6.504 8.429,4.868 11.327,15.686 6.461,24.115-3.145,5.446-9.011,8.829-15.309,8.829-3.074,0-6.118-0.819-8.807-2.372-0.528-0.305-1.049-0.641-1.587-1.031-15.909-11.344-36.011-52.683-45.983-75.846 11.363-1.322 31.104-3.243 49.906-3.243 22.759,0 33.651,2.788 38.831,5.149 0.561,0.251 1.114,0.533 1.64,0.837 4.084,2.357 7.005,6.163 8.225,10.718 1.22,4.555 0.594,9.312-1.763,13.395z"/>
                    </svg>
                </div>
				<div class="d-flex flex-column justify-between h-100">
					<div class="bg-blue w-100 pt-1">
						<header class="header-content bg-blue me-3 ms-3 pb-1 d-flex justify-between">
							<div class="d-flex align-items-center">
								<div class="border-rounded border-success p-2 me-3 header-logo">
									<img  class="img-fluid img-contain" src="./assets/img/logo_ideal.jpeg" alt="" height="30">
								</div>
								<div >
									<h2 class="text-white fw-bold m-0">IDEAL 10 SAS</h2>
									<p class="text-white m-0">Información Digital Estratégica Administrativa en Línea</p>
								</div>
							</div>
							<div class="d-flex align-items-center justify-end">
								<div class="d-flex flex-column justify-between text-right me-3">
									<h3 class="fs-3 fw-bold text-white m-0 "><?=$razonsocial?></h3>
									<?=$infoUser?>
								</div>
								<div class="dropdown-container"><?=$htmlBtn?></div>
							</div>
						</header>
                        <nav class="d-flex pt-1 pb-1 pe-3 ps-3 justify-end bg-blue-light border-bottom-blue">
                            <div class="d-flex">
                                <div class="d-flex">
                                    <div class="form-control m-0">
                                        <div class="d-flex">
                                            <input type="search" placeholder="Digite atajo" onkeypress="shortcut(event)" id="strShortcut" class="h-100">
                                            <button type="button" class="btn btn-dropdown" onclick="shortcut(event)">
                                                <svg class="m-0" xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960" width="15px"><path d="M784-120 532-372q-30 24-69 38t-83 14q-109 0-184.5-75.5T120-580q0-109 75.5-184.5T380-840q109 0 184.5 75.5T640-580q0 44-14 83t-38 69l252 252-56 56ZM380-400q75 0 127.5-52.5T560-580q0-75-52.5-127.5T380-760q-75 0-127.5 52.5T200-580q0 75 52.5 127.5T380-400Z"/></svg>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </nav>
					</div>
					<div class="containernew mt-5">
						<div class="main-content flex-column">
							<div class="d-flex flex-column h-100">
								<div class="card-container bg-transparent d-flex justify-between flex-wrap">
									<div class="d-flex w-100">
										<div class="cursor-pointer card mb-3 " onclick="window.location.href='<?php echo $_SESSION['linkmod'][1];?>'">
											<div class="d-flex align-items-center p-4">
												<div class="me-4 p-2">
													<svg xmlns="http://www.w3.org/2000/svg" height="35px" viewBox="0 -960 960 960" fill="#000"><path d="M400-400h160v-80H400v80Zm0-120h320v-80H400v80Zm0-120h320v-80H400v80Zm-80 400q-33 0-56.5-23.5T240-320v-480q0-33 23.5-56.5T320-880h480q33 0 56.5 23.5T880-800v480q0 33-23.5 56.5T800-240H320Zm0-80h480v-480H320v480ZM160-80q-33 0-56.5-23.5T80-160v-560h80v560h560v80H160Zm160-720v480-480Z"/></svg>
												</div>
												<div>
													<h2 class="fs-3 fw-bold m-0">Contabilidad</h2>
													<p>Control y registro de gastos e ingresos.</p>
												</div>
											</div>
										</div>
										<div class="cursor-pointer card mb-3 ms-4 " onclick="window.location.href='<?php echo $_SESSION['linkmod'][8];?>'">
											<div class="d-flex align-items-center p-4">
												<div class="me-4 p-2">
													<svg xmlns="http://www.w3.org/2000/svg" height="35px" viewBox="0 -960 960 960" fill="#000"><path d="M240-80q-50 0-85-35t-35-85v-120h120v-560h600v680q0 50-35 85t-85 35H240Zm480-80q17 0 28.5-11.5T760-200v-600H320v480h360v120q0 17 11.5 28.5T720-160ZM360-600v-80h360v80H360Zm0 120v-80h360v80H360ZM240-160h360v-80H200v40q0 17 11.5 28.5T240-160Zm0 0h-40 400-360Z"/></svg>
												</div>
												<div>
													<h2 class="fs-3 fw-bold m-0">Contratación</h2>
													<p>Procesos de contratación</p>
												</div>
											</div>
										</div>
										<div class="cursor-pointer card mb-3 me-4 ms-4 " onclick="window.location.href='<?php echo $_SESSION['linkmod'][2];?>'">
											<div class="d-flex align-items-center p-4">
												<div class="me-4 p-2">
													<svg xmlns="http://www.w3.org/2000/svg" height="35px" viewBox="0 -960 960 960" fill="#000"><path d="M400-480q-66 0-113-47t-47-113q0-66 47-113t113-47q66 0 113 47t47 113q0 66-47 113t-113 47ZM80-160v-112q0-33 17-62t47-44q51-26 115-44t141-18h14q6 0 12 2-8 18-13.5 37.5T404-360h-4q-71 0-127.5 18T180-306q-9 5-14.5 14t-5.5 20v32h252q6 21 16 41.5t22 38.5H80Zm560 40-12-60q-12-5-22.5-10.5T584-204l-58 18-40-68 46-40q-2-14-2-26t2-26l-46-40 40-68 58 18q11-8 21.5-13.5T628-460l12-60h80l12 60q12 5 22.5 11t21.5 15l58-20 40 70-46 40q2 12 2 25t-2 25l46 40-40 68-58-18q-11 8-21.5 13.5T732-180l-12 60h-80Zm40-120q33 0 56.5-23.5T760-320q0-33-23.5-56.5T680-400q-33 0-56.5 23.5T600-320q0 33 23.5 56.5T680-240ZM400-560q33 0 56.5-23.5T480-640q0-33-23.5-56.5T400-720q-33 0-56.5 23.5T320-640q0 33 23.5 56.5T400-560Zm0-80Zm12 400Z"/></svg>
												</div>
												<div>
													<h2 class="fs-3 fw-bold m-0">Gestión humana</h2>
													<p>Administración de personal</p>
												</div>
											</div>
										</div>
                                        <div class="cursor-pointer card mb-3" onclick="window.location.href='<?php echo $_SESSION['linkmod'][9];?>'">
                                            <div class="d-flex align-items-center p-4">
                                                <div class="me-4 p-2">
                                                    <svg xmlns="http://www.w3.org/2000/svg" height="35px" viewBox="0 -960 960 960"  fill="#000"><path d="M220-520 80-600v-160l140-80 140 80v160l-140 80Zm0-92 60-34v-68l-60-34-60 34v68l60 34Zm440 123v-93l140 82v280L560-80 320-220v-280l140-82v93l-60 35v188l160 93 160-93v-188l-60-35Zm-140 89v-480h360l-80 120 80 120H600v240h-80Zm40 69ZM220-680Z"/></svg>
                                                </div>
                                                <div>
                                                    <h2 class="fs-3 fw-bold m-0">Planeación estratégica</h2>
                                                    <p>Proceso de desarrollo de planes estratégicos</p>
                                                </div>
                                            </div>
                                        </div>
									</div>
									<div class="d-flex w-100">
										<div class="cursor-pointer card mb-3" onclick="window.location.href='<?php echo $_SESSION['linkmod'][11];?>'" >
											<div class="d-flex align-items-center p-4">
												<div class="me-4 p-2">
													<svg xmlns="http://www.w3.org/2000/svg" height="35px" viewBox="0 -960 960 960" fill="#000"><path d="M440-200h80v-40h40q17 0 28.5-11.5T600-280v-120q0-17-11.5-28.5T560-440H440v-40h160v-80h-80v-40h-80v40h-40q-17 0-28.5 11.5T360-520v120q0 17 11.5 28.5T400-360h120v40H360v80h80v40ZM240-80q-33 0-56.5-23.5T160-160v-640q0-33 23.5-56.5T240-880h320l240 240v480q0 33-23.5 56.5T720-80H240Zm280-560v-160H240v640h480v-480H520ZM240-800v160-160 640-640Z"/></svg>
												</div>
												<div>
													<h2 class="fs-3 fw-bold m-0">Presupuesto</h2>
													<p>Plan de las operaciones y recursos de la entidad</p>
												</div>
											</div>
										</div>
										<div class="cursor-pointer card mb-3 ms-4" onclick="window.location.href='<?php echo $_SESSION['linkmod'][7];?>'">
											<div class="d-flex align-items-center p-4">
												<div class="me-4 p-2">
													<svg xmlns="http://www.w3.org/2000/svg" height="35px" viewBox="0 -960 960 960" fill="#000"><path d="M756-120 537-339l84-84 219 219-84 84Zm-552 0-84-84 276-276-68-68-28 28-51-51v82l-28 28-121-121 28-28h82l-50-50 142-142q20-20 43-29t47-9q24 0 47 9t43 29l-92 92 50 50-28 28 68 68 90-90q-4-11-6.5-23t-2.5-24q0-59 40.5-99.5T701-841q15 0 28.5 3t27.5 9l-99 99 72 72 99-99q7 14 9.5 27.5T841-701q0 59-40.5 99.5T701-561q-12 0-24-2t-23-7L204-120Z"/></svg>
												</div>
												<div>
													<h2 class="fs-3 fw-bold m-0">Herramientas MIPG</h2>
													<p>Modelo Integrado de Planeación y Gestión</p>
												</div>
											</div>
										</div>
                                        <div class="cursor-pointer card mb-3 me-4 ms-4" onclick="window.location.href='<?php echo $_SESSION['linkmod'][4];?>'">
                                            <div class="d-flex align-items-center p-4">
                                                <div class="me-4 p-2">
                                                    <svg xmlns="http://www.w3.org/2000/svg" height="35px" viewBox="0 -960 960 960" fill="#000"><path d="M336-120q-91 0-153.5-62.5T120-336q0-38 13-74t37-65l142-171-97-194h530l-97 194 142 171q24 29 37 65t13 74q0 91-63 153.5T624-120H336Zm144-200q-33 0-56.5-23.5T400-400q0-33 23.5-56.5T480-480q33 0 56.5 23.5T560-400q0 33-23.5 56.5T480-320Zm-95-360h190l40-80H345l40 80Zm-49 480h288q57 0 96.5-39.5T760-336q0-24-8.5-46.5T728-423L581-600H380L232-424q-15 18-23.5 41t-8.5 47q0 57 39.5 96.5T336-200Z"/></svg>
                                                </div>
                                                <div>
                                                    <h2 class="fs-3 fw-bold m-0">Tesorería</h2>
                                                    <p>Gestión de caja y movimientos bancarios</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="cursor-pointer card mb-3" onclick="window.location.href='<?php echo $_SESSION['linkmod'][6];?>'">
                                            <div class="d-flex align-items-center p-4">
                                                <div class="me-4 p-2">
                                                    <svg xmlns="http://www.w3.org/2000/svg" height="35px" viewBox="0 -960 960 960"  fill="#000"><path d="M760-400v-260L560-800 360-660v60h-80v-100l280-200 280 200v300h-80ZM560-800Zm20 160h40v-40h-40v40Zm-80 0h40v-40h-40v40Zm80 80h40v-40h-40v40Zm-80 0h40v-40h-40v40ZM280-220l278 76 238-74q-5-9-14.5-15.5T760-240H558q-27 0-43-2t-33-8l-93-31 22-78 81 27q17 5 40 8t68 4q0-11-6.5-21T578-354l-234-86h-64v220ZM40-80v-440h304q7 0 14 1.5t13 3.5l235 87q33 12 53.5 42t20.5 66h80q50 0 85 33t35 87v40L560-60l-280-78v58H40Zm80-80h80v-280h-80v280Z"/></svg>
                                                </div>
                                                <div>
                                                    <h2 class="fs-3 fw-bold m-0">Activos fijos</h2>
                                                    <p>Proceso y seguimiento de activos físicos de la entidad.</p>
                                                </div>
                                            </div>
                                        </div>
									</div>
									<div class="d-flex w-100">
										<div class="cursor-pointer card mb-3" onclick="window.location.href='<?php echo $_SESSION['linkmod'][5];?>'">
											<div class="d-flex align-items-center p-4">
												<div class="me-4 p-2">
													<svg xmlns="http://www.w3.org/2000/svg" height="35px" viewBox="0 -960 960 960"  fill="#000"><path d="M160-200h80v-320h480v320h80v-426L480-754 160-626v426Zm-80 80v-560l400-160 400 160v560H640v-320H320v320H80Zm280 0v-80h80v80h-80Zm80-120v-80h80v80h-80Zm80 120v-80h80v80h-80ZM240-520h480-480Z"/></svg>
												</div>
												<div>
													<h2 class="fs-3 fw-bold m-0">Almacén</h2>
													<p>Gestión de inventario, control de salidas y entradas.</p>
												</div>
											</div>
										</div>
										<div class="cursor-pointer card mb-3 ms-4" onclick="window.location.href='<?php echo $_SESSION['linkmod'][13];?>'">
											<div class="d-flex align-items-center p-4">
												<div class="me-4 p-2">
													<svg xmlns="http://www.w3.org/2000/svg" height="35px" viewBox="0 -960 960 960" fill="#000"><path d="m370-80-16-128q-13-5-24.5-12T307-235l-119 50L78-375l103-78q-1-7-1-13.5v-27q0-6.5 1-13.5L78-585l110-190 119 50q11-8 23-15t24-12l16-128h220l16 128q13 5 24.5 12t22.5 15l119-50 110 190-103 78q1 7 1 13.5v27q0 6.5-2 13.5l103 78-110 190-118-50q-11 8-23 15t-24 12L590-80H370Zm70-80h79l14-106q31-8 57.5-23.5T639-327l99 41 39-68-86-65q5-14 7-29.5t2-31.5q0-16-2-31.5t-7-29.5l86-65-39-68-99 42q-22-23-48.5-38.5T533-694l-13-106h-79l-14 106q-31 8-57.5 23.5T321-633l-99-41-39 68 86 64q-5 15-7 30t-2 32q0 16 2 31t7 30l-86 65 39 68 99-42q22 23 48.5 38.5T427-266l13 106Zm42-180q58 0 99-41t41-99q0-58-41-99t-99-41q-59 0-99.5 41T342-480q0 58 40.5 99t99.5 41Zm-2-140Z"/></svg>
												</div>
												<div>
													<h2 class="fs-3 fw-bold m-0">Parametrización</h2>
													<p>Configuración inicial del sistema</p>
												</div>
											</div>
										</div>
                                        <div class="cursor-pointer card mb-3 me-4 ms-4 " onclick="window.location.href='<?php echo $_SESSION['linkmod'][0];?>'">
                                            <div class="d-flex align-items-center p-4">
                                                <div class="me-4 p-2">
                                                <svg xmlns="http://www.w3.org/2000/svg" height="35px" viewBox="0 -960 960 960" fill="#000"><path d="M680-280q25 0 42.5-17.5T740-340q0-25-17.5-42.5T680-400q-25 0-42.5 17.5T620-340q0 25 17.5 42.5T680-280Zm0 120q31 0 57-14.5t42-38.5q-22-13-47-20t-52-7q-27 0-52 7t-47 20q16 24 42 38.5t57 14.5ZM480-80q-139-35-229.5-159.5T160-516v-244l320-120 320 120v227q-19-8-39-14.5t-41-9.5v-147l-240-90-240 90v188q0 47 12.5 94t35 89.5Q310-290 342-254t71 60q11 32 29 61t41 52q-1 0-1.5.5t-1.5.5Zm200 0q-83 0-141.5-58.5T480-280q0-83 58.5-141.5T680-480q83 0 141.5 58.5T880-280q0 83-58.5 141.5T680-80ZM480-494Z"/></svg>
                                                </div>
                                                <div>
                                                    <h2 class="fs-3 fw-bold m-0">Administración</h2>
                                                    <p>Gestión general de la entidad</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="cursor-pointer card mb-3 " onclick="window.location.href='<?php echo $_SESSION['linkmod'][12];?>'">
                                            <div class="d-flex align-items-center p-4">
                                                <div class="me-4 p-2">
                                                <svg xmlns="http://www.w3.org/2000/svg" height="35px" viewBox="0 -960 960 960" fill="#000"><path d="M160-120q-33 0-56.5-23.5T80-200v-640l67 67 66-67 67 67 67-67 66 67 67-67 67 67 66-67 67 67 67-67 66 67 67-67v640q0 33-23.5 56.5T800-120H160Zm0-80h280v-240H160v240Zm360 0h280v-80H520v80Zm0-160h280v-80H520v80ZM160-520h640v-120H160v120Z"/></svg>
                                                </div>
                                                <div>
                                                    <h2 class="fs-3 fw-bold m-0">Informes</h2>
                                                    <p>Enlaces y archivos de interés actualizados</p>
                                                </div>
                                            </div>
                                        </div>
									</div>
                                    <div class="d-flex w-100">
                                        <div class="cursor-pointer card mb-3 " onclick="window.location.href='<?php echo $_SESSION['linkmod'][10];?>'">
											<div class="d-flex align-items-center p-4">
												<div class="me-4 p-2">
													<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" height="35px" fill="#000"><!--!Font Awesome Free 6.6.0 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license/free Copyright 2024 Fonticons, Inc.--><path d="M352 256H313.4c-15.7-13.4-35.5-23.1-57.4-28V180.4l-32-3.4-32 3.4V228c-21.9 5-41.7 14.6-57.4 28H16A16 16 0 0 0 0 272v96a16 16 0 0 0 16 16h92.8C129.4 421.7 173 448 224 448s94.6-26.3 115.2-64H352a32 32 0 0 1 32 32 32 32 0 0 0 32 32h64a32 32 0 0 0 32-32A160 160 0 0 0 352 256zM81.6 159.9l142.4-15 142.4 15c9.4 1 17.6-6.8 17.6-16.8V112.9c0-10-8.2-17.8-17.6-16.8L256 107.7V80a16 16 0 0 0 -16-16H208a16 16 0 0 0 -16 16v27.7L81.6 96.1C72.2 95.1 64 102.9 64 112.9v30.2C64 153.1 72.2 160.9 81.6 159.9z"/></svg>
												</div>
												<div>
													<h2 class="fs-3 fw-bold m-0">Servicios públicos</h2>
													<p>Gestión de servicios públicos</p>
												</div>
											</div>
										</div>
										<div class="cursor-pointer card mb-3 ms-4" onclick="window.location.href='<?php echo $_SESSION['linkmod'][14];?>'">
                                            <div class="d-flex align-items-center p-4">
												<div class="me-4 p-2">
                                                    <svg xmlns="http://www.w3.org/2000/svg" height="35px" viewBox="0 -960 960 960" fill="#000"><path d="M80-80q-17 0-28.5-11.5T40-120v-320l85-203q7-17 22-27t33-10h360q18 0 33 10t22 27l85 203v320q0 17-11.5 28.5T640-80h-40q-17 0-28.5-11.5T560-120v-40H160v40q0 17-11.5 28.5T120-80H80Zm72-440h415l-33-80H186l-34 80Zm-32 280h480v-200H120v200Zm100-40q25 0 42.5-17.5T280-340q0-25-17.5-42.5T220-400q-25 0-42.5 17.5T160-340q0 25 17.5 42.5T220-280Zm280 0q25 0 42.5-17.5T560-340q0-25-17.5-42.5T500-400q-25 0-42.5 17.5T440-340q0 25 17.5 42.5T500-280Zm220 80v-344l-73-176H227l18-43q7-17 22-27t33-10h360q18 0 33 10t22 27l85 203v320q0 17-11.5 28.5T760-200h-40Zm120-120v-344l-73-176H347l18-43q7-17 22-27t33-10h360q18 0 33 10t22 27l85 203v320q0 17-11.5 28.5T880-320h-40Zm-480-20Z"/></svg>
												</div>
												<div>
													<h2 class="fs-3 fw-bold m-0">Area de movilidad</h2>
													<p>Gestión de servicios de TT</p>
												</div>
											</div>
                                        </div>
										<div class="cursor-pointer card mb-3 me-4 ms-4 " ></div>
                                        <div class=" cursor-pointer card mb-3" ></div>
									</div>
								</div>
							</div>
							<div class="info-content d-flex justify-between border-radius p-3 bg-white">
								<div class="w-100">
									<div class="d-flex align-items-center cursor-pointer" onclick="mypop=window.open('plan-actareasbusca.php','','');mypop.focus()">
										<span class="me-1 fs-3">Tareas</span>
										<svg xmlns="http://www.w3.org/2000/svg" height="22px" viewBox="0 -960 960 960" fill="#000"><path d="M655-200 513-342l56-56 85 85 170-170 56 57-225 226Zm0-320L513-662l56-56 85 85 170-170 56 57-225 226ZM80-280v-80h360v80H80Zm0-320v-80h360v80H80Z"/></svg>
										<span class="ms-1 fs-3">(<span class="<?=$activeTareas?>"><?=$totalTareas?></span>)</span>
									</div>
									<div class="border border-radius overflow-hidden relative" style="height:7rem;">
										<ul class="marquee" >
											<?php for ($i=0; $i < $totalTareas ; $i++) {
												$url="plan-actareasresponder.php?idradicado=".$arrTareas[$i]['codradicacion']."&idresponsable=".
												$arrTareas[$i]['codigo']."&tipoe=".$arrTareas[$i]['estado']."&tiporad=".$arrTareas[$i]['tipot']
											?>
											<li><a href="<?=$url?>" class="fs-3 fw-normal" target="_blank"><?=$arrTareas[$i]['fecha']." - ". $arrTareas[$i]['tarea']?></a></li>
											<?php } ?>
										</ul>
									</div>
								</div>
								<div class="w-100 me-3 ms-3">
									<div class="d-flex align-items-center cursor-pointer" onclick="mypop=window.open('plan-agenda.php','','');mypop.focus()">
										<span class="me-1 fs-3">Agenda</span>
										<svg class="fill-black group-hover:fill-white w-5 h-5" xmlns="http://www.w3.org/2000/svg" height="22px" viewBox="0 -960 960 960">
											<path d="M560-564v-68q33-14 67.5-21t72.5-7q26 0 51 4t49 10v64q-24-9-48.5-13.5T700-600q-38 0-73 9.5T560-564Zm0 220v-68q33-14 67.5-21t72.5-7q26 0 51 4t49 10v64q-24-9-48.5-13.5T700-380q-38 0-73 9t-67 27Zm0-110v-68q33-14 67.5-21t72.5-7q26 0 51 4t49 10v64q-24-9-48.5-13.5T700-490q-38 0-73 9.5T560-454ZM260-320q47 0 91.5 10.5T440-278v-394q-41-24-87-36t-93-12q-36 0-71.5 7T120-692v396q35-12 69.5-18t70.5-6Zm260 42q44-21 88.5-31.5T700-320q36 0 70.5 6t69.5 18v-396q-33-14-68.5-21t-71.5-7q-47 0-93 12t-87 36v394Zm-40 118q-48-38-104-59t-116-21q-42 0-82.5 11T100-198q-21 11-40.5-1T40-234v-482q0-11 5.5-21T62-752q46-24 96-36t102-12q58 0 113.5 15T480-740q51-30 106.5-45T700-800q52 0 102 12t96 36q11 5 16.5 15t5.5 21v482q0 23-19.5 35t-40.5 1q-37-20-77.5-31T700-240q-60 0-116 21t-104 59ZM280-494Z">
											</path>
										</svg>
										<span class="ms-1 fs-3">(0)</span>
									</div>
									<div class="border border-radius overflow-hidden relative" style="height:7rem;">
										<ul class="marquee" >

										</ul>
									</div>
								</div>
								<div class="w-100" >
									<div class="d-flex align-items-center cursor-pointer" onclick="mypop=window.open('mipg-historial_publicaciones.php','','');mypop.focus()">
										<span class="me-1 fs-3">Publicaciones</span>
											<svg xmlns="http://www.w3.org/2000/svg" height="22px" viewBox="0 -960 960 960" fill="#000"><path d="M120-120v-720h720v720H120Zm600-160H240v60h480v-60Zm-480-60h480v-60H240v60Zm0-140h480v-240H240v240Zm0 200v60-60Zm0-60v-60 60Zm0-140v-240 240Zm0 80v-80 80Zm0 120v-60 60Z"/></svg>
										<span class="ms-1 fs-3">(<span class="<?=$activePublicacion?>"><?=$totalPublicacion?></span>)</span>
									</div>
									<div class="border border-radius overflow-hidden relative" style="height:7rem;">
										<ul class="marquee" >
											<?php
												for ($i=0; $i < $totalPublicacion ; $i++) {
													$url = "plan-mostrainformacion.php?idinfo=".$arrPublicacion[$i]['indices'];
													echo "<li><a href='#' class='fs-3 fw-normal' @click=\"loadContentIntoModal('$url')\">".$arrPublicacion[$i]['fecha']." - ". $arrPublicacion[$i]['titulos']."</a></li>";
												}
											?>
										</ul>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div v-show="showModal_programaticoProyectos">
				<transition name="modal">
					<div class="modal-mask">
						<div class="modal-wrapper">
							<div class="modal-dialog modal-xl" style="max-width: 1200px !important;" role="document">
								<div class="modal-content" style="width: 100% !important;" scrollable>
									<div class="modal-header">
									<h5 class="modal-title">Publicaciones Ideal 10</h5>
									<button type="button" @click="showModal_programaticoProyectos=false;" class="btn btn-close">
										<div></div>
										<div></div>
									</button>
									</div>
									<div class="modal-body overflow-hidden"></div>
								</div>
							</div>
						</div>
					</div>
				</transition>
			</div>
		</section>
		<script src="Librerias/vue/vue.min.js"></script>
		<script src="Librerias/vue/axios.min.js"></script>
		<script src="vue/principal.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
	</body>
</html>
