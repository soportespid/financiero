<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require 'comun.inc';
	require 'funciones.inc';
	session_start();
	if(empty($_SESSION)){
		header("location: index.php");
	}
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html lang=es>
	<head>
		<meta charset="utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1"/>
		<meta name="viewport" content="user-scalable=no">
		<title>:: IDEAL 10 - Administración</title>
		<link href="favicon.ico" rel="shortcut icon"/>
		<link href="css/css2.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/style.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
		<link rel="stylesheet" href="sweetalert2/dist/sweetalert2.min.css">
		<script src="sweetalert2/dist/sweetalert2.min.js"></script>
		<script type="text/javascript" src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/Sortable/1.14.0/Sortable.min.js"></script>
		<style>
			.menu-container, .submenu-container {
				display: inline-block;
				vertical-align: top;
				width: 45%;
				margin: 20px;
				position: relative;
			}
			.dropdown-menu, .submenu {
				list-style: none;
				padding: 0;
				margin: 0;
				border: 1px solid #ccc;
				border-radius: 8px;
				background: #ffffff;
				box-shadow: 0 4px 8px rgba(0, 0, 0, 0.1);
			}
			.dropdown-menu li, .submenu li {
				padding: 10px 15px;
				cursor: pointer;
				border-bottom: 1px solid #eee;
				border-radius: 5px;
				transition: background-color 0.3s ease, box-shadow 0.3s ease;
				position: relative;
				font-family: inherit;
				font-size: 16px !important;
			}
			.dropdown-menu li:last-child, .submenu li:last-child {
				border-bottom: none;
			}
			.dropdown-menu li:hover, .submenu li:hover {
				background-color: #f1f1f1;
				box-shadow: inset 0 0 10px rgba(0, 0, 0, 0.1);
			}
			.dropdown-menu .submenu {
				display: none;
				position: absolute;
				top: 0;
				list-style: none;
				padding: 0;
				margin: 0;
				border: 1px solid #ccc;
				border-radius: 8px;
				background: #f9f9f9;
				box-shadow: 0 4px 8px rgba(0, 0, 0, 0.1);
			}
			.dropdown-menu li:hover .submenu {
				display: block;
			}
			.selected {
				background-color: #2563EB;
				color: #ffffff;
			}
			.modal {
				display: block;
				position: fixed;
				z-index: 1;
				left: 0;
				top: 0;
				width: 100%;
				height: 100%;
				overflow: auto;
				background-color: rgb(0, 0, 0);
				background-color: rgba(0, 0, 0, 0.4);
				border-radius: 8px;
			}
			.modal-content {
				background-color: #fefefe;
				margin: 15% auto;
				padding: 20px;
				border: 1px solid #888;
				width: 80%;
			}
			.submenu {
				max-height: 300px;
				overflow-y: auto; 
			}
		</style>
	</head>
	<body>
		<section id="myapp" v-cloak>
			<div class="loading-container" v-show="isLoading" >
				<p class="text-loading"data-loading="IDEAL 10">IDEAL 10</p>
			</div>
			<div class="main-container">
				<header>
					<table>
						<tr><script>barra_imagenes("adm");</script><?php cuadro_titulos();?></tr>
					</table>
				</header>
				<nav>
					<?php menu_desplegable("adm");?>
					<div class="bg-white group-btn p-1">
						<button type="button"  @click="save" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
							<span>Guardar</span>
							<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960"><path d="M840-680v480q0 33-23.5 56.5T760-120H200q-33 0-56.5-23.5T120-200v-560q0-33 23.5-56.5T200-840h480l160 160Zm-80 34L646-760H200v560h560v-446ZM480-240q50 0 85-35t35-85q0-50-35-85t-85-35q-50 0-85 35t-35 85q0 50 35 85t85 35ZM240-560h360v-160H240v160Zm-40-86v446-560 114Z"></path></svg>
						</button>
						<button type="button" @click="mypop=window.open('adm-principal.php','',''); mypop.focus();" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
							<span>Nueva ventana</span>
							<svg class="fill-black group-hover:fill-white w-5 h-5" xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960"><path d="M320-120v-80h80v-80H160q-33 0-56.5-23.5T80-360v-400q0-33 23.5-56.5T160-840h640q33 0 56.5 23.5T880-760v400q0 33-23.5 56.5T800-280H560v80h80v80H320ZM160-360h640v-400H160v400Zm0 0v-400 400Z"></path></svg>
						</button>
						<button type="button" @click="mypop=window.open('adm-organizarMenus.php','','');mypop.focus();" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
							<span class="group-hover:text-white">Duplicar pantalla</span>
							<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960"><path d="M640-160v-360H160v360h480Zm80-200v-80h80v-360H320v200h-80v-200q0-33 23.5-56.5T320-880h480q33 0 56.5 23.5T880-800v360q0 33-23.5 56.5T800-360h-80ZM160-80q-33 0-56.5-23.5T80-160v-360q0-33 23.5-56.5T160-600h480q33 0 56.5 23.5T720-520v360q0 33-23.5 56.5T640-80H160Zm400-603ZM400-340Z"></path></svg>
						</button>
					</div>
				</nav>
				<article>
					<div class="bg-white">
						<h2 class="titulos m-0">Menus Entidades</h2>
						<div class="w-100">
							<div class="d-flex w-100">
								<div class="form-control w-25">
									<label class="form-label" for="">Seleccionar Entidad:</label>
									<select id="labelSelectName2" v-model="selectEntidades" @change="cambioSelectEntidades">
										<option v-for="(data,index) in arrEntidades" :key="index" :value="data.id">
										{{ data.id}} - {{ data.entidad}}
										</option>
									</select>
								</div>
							</div>
						</div>
					</div>
					<div class="bg-white">
						<div class="w-100">
							<div class="d-flex w-100">
								<div class="form-control w-25"> 
									<label class="form-label" for="">Seleccionar Módulo:</label> 
									<select id="labelSelectName2" v-model="selectModulos1" @change="cambioSelectModulos1"> 
										<option v-for="(data,index) in arrModulos1" :key="index" :value="data.id_modulo"> {{ data.id_modulo}} - {{ data.nombre}} </option> 
									</select> 
								</div> 
								<div class="form-control w-15"></div>
								<div class="form-control w-25"> 
									<label class="form-label" for="">Seleccionar Módulo 2:</label> 
									<select id="labelSelectName3" v-model="selectModulos2" @change="cambioSelectModulos2"> 
										<option v-for="(data,index) in arrModulos2" :key="index" :value="data.id_modulo"> {{ data.id_modulo}} - {{ data.nombre}} </option> 
									</select> 
								</div>
							</div>
						</div>
					</div>
					<div class="bg-white">
						<div class="w-100">
							<div class="d-flex w-100">
								<div class="form-control w-15">
									<ul class="dropdown-menu">
										<li v-for="item in arrModulosDetalles" :key="`1-${item.id_item}`" :data-id="`1-${item.id_detalle}`" @click="toggleSubmenu(item.id_detalle)">
											{{ item.nombre }}
										</li>
									</ul>
								</div>
								<div class="form-control w-25">
									<ul class="submenu" v-if="arrOpciones[selectedDetalle]" ref="sortable1">
										<li v-for="(opcion, index) in arrOpciones[selectedDetalle]" :key="opcion.id_opcion" @dblclick="openModal(opcion)" class="draggable">
										- {{ opcion.nom_opcion }} - {{ opcion.comando }}
										</li>
									</ul>
								</div>
								<div class="form-control w-15">
									<ul class="dropdown-menu">
										<li v-for="item in arrModulosDetalles2" :key="`2-${item.id_item}`" :data-id="`2-${item.id_detalle}`" @click="toggleSubmenu2(item.id_detalle)">
											{{ item.nombre }}
										</li>
									</ul>
								</div>
								<div class="form-control w-25">
									<ul class="submenu" v-if="arrOpciones2[selectedDetalle2]" ref="sortable2">
										<li v-for="(opcion, index) in arrOpciones2[selectedDetalle2]" :key="opcion.id_opcion" @dblclick="openModal(opcion)" class="draggable">
										- {{ opcion.nom_opcion }} - {{ opcion.comando }}
										</li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</article>
			</div>
			<div v-if="showModal" class="modal">
				<transition name="modal">
					<div class="modal-mask">
						<div class="modal-wrapper">
							<div class="modal-dialog modal-xl" style="max-width: 1200px !important;" role="document">
								<div class="modal-content" style="width: 100% !important;" scrollable>
									<div class="modal-header">
										<h5 class="modal-title">Editar Opción</h5>
									</div>
									<div class="modal-body overflow-hidden">
										<div class="bg-white">
											<h2 class="titulos m-0">Campos Opciones</h2>
											<div class="w-100">
												<div class="d-flex w-100">
													<div class="form-control w-25">
														<label class="form-label" for="">Nombre Opción:</label>
														<input type="text" class="text-center" id="nom_opcion" v-model="currentOption.nom_opcion">
													</div>
													<div class="form-control w-25">
														<label class="form-label" for="">Ruta Opción:</label>
														<input type="text" class="text-center" id="ruta_opcion" v-model="currentOption.ruta_opcion">
													</div>
													<div class="form-control w-25">
														<label class="form-label" for="">Comando:</label>
														<input type="text" class="text-center" id="comando" v-model="currentOption.comando">
													</div>
													<div class="form-control w-25">
														<label class="form-label" for="">Comando:</label>
														<input type="number" class="text-center" id="orden" v-model="currentOption.orden">
													</div>
												</div>
												<div class="d-flex w-100">
													<div class="form-control w-100"></div>
												</div>
												<div class="d-flex w-100">
													<div class="form-control w-100"></div>
												</div>
												<div class="d-flex w-100">
													<div class="form-control w-15">
														<button type="button" @click="confirmSave" class="btn btn-success me-2">Guardar</button>
													</div>
													<div class="form-control w-15">
														<button type="button" @click="confirmDelete" class="btn btn-danger me-2">Eliminar</button>
													</div>
													<div class="form-control w-15">
														<button type="button" @click="cancelEdit"class="btn btn-white me-2">Cancelar</button>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</transition>
			</div>
		</section>
		<script src="Librerias/vue/vue.min.js"></script>
		<script src="Librerias/vue/axios.min.js"></script>
		<script src="vue/administracion/menus/adm-organizarMenus.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
	</body>
</html>