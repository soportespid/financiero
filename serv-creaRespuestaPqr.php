<?php
    header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require 'comun.inc';
	require 'funciones.inc';
	session_start();
    if(empty($_SESSION)){
        header("location: index.php");
    }
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html lang=es>
	<head>
    <meta charset="utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1"/>
		<meta name="viewport" content="user-scalable=no">
		<title>:: IDEAL 10 - Servicios públicos</title>
		<link href="favicon.ico" rel="shortcut icon"/>
		<link href="css/css2z.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
        <link href="css/tabs.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
        <link href="css/style.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" href="sweetalert2/dist/sweetalert2.min.css">
        <script type="text/javascript" src="css/programas.js"></script>
        <script src="sweetalert2/dist/sweetalert2.min.js"></script>
		<script src="vue/vue.min.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script src="css/calendario.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
	</head>
	<body>
		<header>
			<table>
				<tr><script>barra_imagenes("serv");</script><?php cuadro_titulos();?></tr>
			</table>
		</header>

        <section id="myapp" v-cloak>
            <input type="hidden" value = "1" ref="pageType">
            <div class="loading-container" v-show="isLoading" >
                <p class="text-loading"data-loading="IDEAL 10">IDEAL 10</p>
            </div>
            <nav>
                <table>
                    <tr><?php menu_desplegable("serv");?></tr>
                </table>
                <div class="bg-white group-btn p-1" id="newNavStyle">
                    <button type="button" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center" @click="window.location.reload()">
                        <span>Nuevo</span>
                        <svg viewBox="0 -960 960 960" ><path d="M440-440H200v-80h240v-240h80v240h240v80H520v240h-80v-240Z"/></svg>
                    </button>
                    <button type="button" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center" @click="save()">
                        <span>Guardar</span>
                        <svg viewBox="0 -960 960 960" ><path d="M840-680v480q0 33-23.5 56.5T760-120H200q-33 0-56.5-23.5T120-200v-560q0-33 23.5-56.5T200-840h480l160 160Zm-80 34L646-760H200v560h560v-446ZM480-240q50 0 85-35t35-85q0-50-35-85t-85-35q-50 0-85 35t-35 85q0 50 35 85t85 35ZM240-560h360v-160H240v160Zm-40-86v446-560 114Z"/></svg>
                    </button>
                    <button type="button" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center" @click="location.href='serv-buscaRespuestasPqr'">
                        <span>Buscar</span>
                        <svg class="fill-black group-hover:fill-white w-5 h-5" viewBox="0 -960 960 960" ><path d="M784-120 532-372q-30 24-69 38t-83 14q-109 0-184.5-75.5T120-580q0-109 75.5-184.5T380-840q109 0 184.5 75.5T640-580q0 44-14 83t-38 69l252 252-56 56ZM380-400q75 0 127.5-52.5T560-580q0-75-52.5-127.5T380-760q-75 0-127.5 52.5T200-580q0 75 52.5 127.5T380-400Z"/></svg>
                    </button>
                    <button type="button" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center" @click="mypop=window.open('serv-principal.php','',''); mypop.focus();">
                        <span>Nueva ventana</span>
                        <svg class="fill-black group-hover:fill-white w-5 h-5" viewBox="0 -960 960 960" ><path d="M320-120v-80h80v-80H160q-33 0-56.5-23.5T80-360v-400q0-33 23.5-56.5T160-840h640q33 0 56.5 23.5T880-760v400q0 33-23.5 56.5T800-280H560v80h80v80H320ZM160-360h640v-400H160v400Zm0 0v-400 400Z"/></svg>
                    </button>
                    <button type="button" class="btn btn-success d-flex justify-between align-items-center" @click="window.location.href='serv-menuPQR'">
                        <span>Atrás</span>
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960"><path d="M760-200v-160q0-50-35-85t-85-35H273l144 144-57 56-240-240 240-240 57 56-144 144h367q83 0 141.5 58.5T840-360v160h-80Z"/></svg>
                    </button>
                </div>
            </nav>
            <article>
                <div ref="rTabs" class="nav-tabs bg-white p-1">
                    <div class="nav-item" :class="respuesta == true ? 'active' : ''" @click="respuesta=true;anexo=false;">Respuesta</div>
                    <div class="nav-item" :class="respuesta == false ? 'active' : ''" @click="respuesta=false;anexo=true;">Anexar documentos</div>
                </div>

                <div class="bg-white" v-show="respuesta">
                    <div>
                        <h2 class="titulos m-0">Crear respuesta PQR</h2>
                        <p class="m-0 ms-2">Todos los campos con (<span class="text-danger fw-bolder">*</span>) son obligatorios</p>
                        
                        <div>
                            <div class="d-flex">
                                <div class="form-control w-15">
                                    <label class="form-label m-0" for="">Fecha<span class="text-danger fw-bolder">*</span>:</label>
                                    <input type="date" class="text-center" v-model="data.fecha">
                                </div>
    
                                <div class="form-control w-25">
                                    <label class="form-label m-0" for="">Consecutivo radicado<span class="text-danger fw-bolder">*</span>:</label>
                                    <input type="text" class="colordobleclik" v-model="data.consecutivoRadicado" @dblclick="modalRadicados=true" readonly>
                                </div>                                

                                <div class="form-control w-25">
                                    <label class="form-label m-0" for="">Tipo tramite:</label>
                                    <input type="text" v-model="data.tipoTramite" readonly>
                                </div>

                                <div class="form-control w-25">
                                    <label class="form-label m-0" for="">Tipo causal:</label>
                                    <input type="text" v-model="data.tipoCausal" readonly>
                                </div>
                            </div>

                            <div class="d-flex">
                                <div class="form-control">
                                    <label class="form-label m-0" for="">Detalle causal:</label>
                                    <input type="text" v-model="data.detalleCausal" readonly>
                                </div>

                                <div class="form-control">
                                    <label class="form-label m-0" for="">Descripción radicar:</label>
                                    <input type="text" v-model="data.descripcionRadicado" readonly>
                                </div>
                            </div>

                            <div class="d-flex">
                                <div class="form-control">
                                    <label class="form-label m-0" for="">Tipo respuesta<span class="text-danger fw-bolder">*</span>:</label>
                                    <select v-model="data.tipoRespuestaId">
                                        <option value="0" selected>Seleccione</option>
                                        <option v-for="(respuesta,index) in respuestas" :key="index" :value="respuesta.id">
                                            {{respuesta.codigo_respuesta}} - {{respuesta.nombre}}
                                        </option>
                                    </select>
                                </div>

                                <div class="form-control">
                                    <label class="form-label m-0" for="">Tipo notificación<span class="text-danger fw-bolder">*</span>:</label>
                                    <select v-model="data.tipoNotificacionId">
                                        <option value="0" selected>Seleccione</option>
                                        <option v-for="(notificacion,index) in notificaciones" :key="index" :value="notificacion.id">
                                            {{notificacion.codigo_notificacion}} - {{notificacion.nombre}}
                                        </option>
                                    </select>
                                </div>
                            </div>

                            <div class="d-flex">
                                <div class="form-control">
                                    <label class="form-label m-0" for="">Descripción<span class="text-danger fw-bolder">*</span>:</label>
                                    <textarea v-model="data.descripcion" style="resize: vertical; height: 100px; max-height: 200px;"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>                       
                </div>

                <div class="bg-white" v-show='anexo'>
                    <h2 class="titulos m-0">Anexar imagenes</h2>
                    <p class="m-0 ms-2">Todos los campos con (<span class="text-danger fw-bolder">*</span>) son obligatorios</p>
                        
                    <div>
                        <div class="d-flex">
                            <div class="form-control">
                                <label class="form-label m-0" for="">Anexos<span class="text-danger fw-bolder">*</span>:</label>
                                <input type="file" id="foto" accept="image/jpeg,image/png,image/x-eps,application/pdf" multiple @change="previewImages">
                            </div>
                        </div>
    
                        <div class="d-flex flex-wrap">
                            <div v-for="(image, index) in imagesPreview" :key="index" class="me-2 relative">
                                <img :src="image" alt="Vista previa" style="width: 100px; height: 100px;">
                                <span @click="deleteImageAdded(index)" class="cursor-pointer bg-danger text-white border-rounded pe-2 ps-2 absolute top-0 end-0">x</button>
                            </div>
                        </div>  
                    </div>
                </div>

                <!-- modales -->

                <div v-show="modalRadicados" class="modal">
                    <div class="modal-dialog modal-xl" >
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title">Listado de radicados PQR</h5>
                                <button type="button" @click="modalRadicados=false;" class="btn btn-close"><div></div><div></div></button>
                            </div>
                            <div class="modal-body">
                                <div class="d-flex flex-column">
                                    <div class="form-control m-0 mb-3">
                                        <input type="search" v-model="txtSearch" @keyup="searchData('modalRadicados')" placeholder="Busca consecutivo o tipo de tramite
                                        ">
                                    </div>
                                    <div class="form-control m-0 p-2">
                                        <label class="form-label" for="labelInputName">Resultados: <span class="fw-bold">{{txtResultados}}</span></label>
                                    </div>
                                </div>
                                <div class="overflow-auto max-vh-50 overflow-x-hidden" >
                                    <table class="table table-hover fw-normal">
                                        <thead>
                                            <tr>
                                                <th>Consecutivo</th>
                                                <th>Tipo tramite</th>
                                                <th>Tipo causal</th>
                                                <th>Detalle causal</th>
                                                <th>Descripción</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr v-for="(radicado,index) in pqrRadicadosCopy" :key="index" @click="selectItemModal(radicado, 'modalRadicados')">
                                                <td class="text-center">{{radicado.consecutivo}}</td>
                                                <td>{{radicado.nombreTramite}}</td>
                                                <td>{{radicado.nombreCausal}}</td>
                                                <td>{{radicado.detalleCausal}}</td>
                                                <td>{{radicado.descripcion}}</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </article>
        </section>

        <script src="Librerias/vue/vue.min.js"></script>
		<script src="Librerias/vue/axios.min.js"></script>
		<script type="module" src="servicios_publicos/pqr/js/respuestaPqr_functions.js"></script>

	</body>
</html>
