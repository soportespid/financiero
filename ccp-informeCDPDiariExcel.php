<?php
    require_once 'comun.inc';
    require 'funciones.inc';
    require 'funcionesSP.inc.php';
    /*ini_set('display_errors', '1');
    ini_set('display_startup_errors', '1');
    error_reporting(E_ALL);*/
    class Plantilla{
        private $linkbd;
        private $arrData;
        public function __construct() {
            $this->linkbd = conectar_v7();
            $this->linkbd->set_charset("utf8");
        }
        public function getData(){
            if(!empty($_SESSION)){
                $fechaInicial = explode("/",$_GET['fecha_inicial']);
                $fechaInicial = $fechaInicial[2]."-".$fechaInicial[1]."-".$fechaInicial[0];
                $fechaFinal = explode("/",$_GET['fecha_final']);
                $fechaFinal = $fechaFinal[2]."-".$fechaFinal[1]."-".$fechaFinal[0];
                return $this->selectData($fechaInicial,$fechaFinal);
            }
            die();
        }
        public function selectData($fechaInicial,$fechaFinal){
            $sql = "SELECT cab.consvigencia as id,cab.objeto as detalle,
            cab.fecha,
            det.codigo_vigenciag as vigencia,
            ho.fuente_diari as fuente,
            det.valor,
            CASE
                    WHEN SUBSTRING(det.cuenta, 1, 3) = '2.3' THEN CONCAT(det.indicador_producto,'-',det.bpim)
                    ELSE det.cuenta
            END AS rubro,
            CASE
                WHEN SUBSTRING(det.cuenta, 1, 3) IN ('2.1', '2.2') THEN (
                    SELECT nombre
                    FROM cuentasccpet
                    WHERE cuentasccpet.codigo = det.cuenta
                    LIMIT 1
                )
                WHEN SUBSTRING(det.cuenta, 1, 3) = '2.3' THEN (
                    SELECT nombre
                    FROM ccpproyectospresupuesto
                    WHERE ccpproyectospresupuesto.codigo = det.bpim
                    LIMIT 1
                )
                ELSE NULL
            END AS nombre
            FROM ccpetcdp_detalle det
            INNER JOIN ccpetcdp cab
            ON cab.consvigencia = det.consvigencia
            INNER JOIN ccpet_homologarfuentesdiari ho
            ON det.fuente = ho.fuente_cuipo
            WHERE (SUBSTRING(det.cuenta, 1, 3) = '2.1' OR SUBSTRING(det.cuenta, 1, 3) = '2.2' OR SUBSTRING(det.cuenta, 1, 3) = '2.3')
            AND cab.estado = 'S' AND det.estado = 'S' AND cab.fecha BETWEEN '$fechaInicial' AND '$fechaFinal'
            GROUP BY det.id_cdpdetalle ORDER BY DATE(cab.fecha) DESC;";
            $arrData = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
            return $arrData;
        }
    }

    if($_GET){
        $obj = new Plantilla();
        $request = $obj->getData();
        $delimiter = "|";
        $fileName = "RELACION_DE_CDP_".date("Y-m-d").".csv";
        //Abro archivo en memoria para escritura
        $filePointer = fopen('php://memory',"w");
        //Agrego encabezados
        $headers = array("idCdp","detalleCdpObjeto","vigencia","macroCampo","codigoPresupuestal","fuente","nombreRubro","fechaCdp","valorCdp");
        fputcsv($filePointer,$headers,$delimiter);

        //lleno las demas filas
        $totalRows = count($request);
        for ($i=0; $i < $totalRows; $i++) {
            $row = array(
                $request[$i]['id'],
                replaceChar($request[$i]['detalle']),
                $request[$i]['vigencia'],
                2,
                $request[$i]['rubro'],
                $request[$i]['fuente'],
                replaceChar($request[$i]['nombre']),
                $request[$i]['fecha'],
                $request[$i]['valor']
            );
            fwrite($filePointer, implode($delimiter, $row) . "\n");
        }
        //Reinicio el puntero que estaba usando para escribir un archivo
        fseek($filePointer, 0);

        //Encabezados para exportar a csv
        header("Content-type:text/csv");
        header('Content-Disposition:attachment;filename="'.$fileName.'"');

        //Agrego toda la información escrita al archivo que se acaba de abrir
        fpassthru($filePointer);

        //Cierro el archivo
        fclose($filePointer);
        die();
    }
?>
