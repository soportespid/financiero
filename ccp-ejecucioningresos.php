<?php
    ini_set('max_execution_time',3600);
    header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require "comun.inc";
    require "funciones.inc"; 
    $linkbd = conectar_v7();
    $linkbd -> set_charset("utf8");
	session_start();
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");

    $maxVersion = ultimaVersionIngresosCCPET();
?>
<!DOCTYPE >
<html lang="es">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1">
		<meta name="viewport" content="user-scalable=no">
		<title>:: Ideal - Presupuesto</title>
        <link href="css/css2.css" rel="stylesheet" type="text/css" />
        <link href="css/css3.css" rel="stylesheet" type="text/css" />
        <link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="css/programas.js"></script>
        <script type="text/javascript" src="css/funciones.js"></script>
        <script type="text/javascript" src="jquery-1.11.0.min.js"></script> 
        <script type="text/javascript" src="JQuery/jquery-1.12.0.min.js"></script> 
        <script src="css/calendario.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<link rel="stylesheet" href="bootstrap/css/bootstrap.css">
        <link rel="stylesheet" href="bootstrap/css/estilos.css">
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">

        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
        <script type="text/javascript" src="bootstrap/fontawesome.5.11.2/js/all.js"></script>
        <?php titlepag();?> 

        <script>

            $(window).load(function () { $('#cargando').hide();});

            function excell()
            {
                document.form2.action="ccp-ejecucioningresosexcel.php";
                document.form2.target="_BLANK";
                document.form2.submit(); 
                document.form2.action="";
                document.form2.target="";
            }
            function excell1()
            {
                document.form2.action="ccp-ejecuciongastosexcel1.php";
                document.form2.target="_BLANK";
                document.form2.submit(); 
                document.form2.action="";
                document.form2.target="";
            }
            function validar()
            {
                document.form2.submit();
            }

            function generarEjecucion()
            {
                document.form2.oculto.value = '3';
                document.form2.submit();
            }
        </script>

        <style>
            label{
                font-size:13px;
            }

            input{
                height: calc(1em + 0.6rem + 0.5px) !important; 
                font-size: 14px !important; 
                margin-top: 4px !important;
            }
        </style>
    </head>
    <body>
        <div class="loading" id="divcarga"><span>Cargando...</span></div>
        <IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
        <span id="todastablas2"></span>
        <table>
            <tr><script>barra_imagenes("ccpet");</script><?php cuadro_titulos();?></tr>	 
            <tr><?php menu_desplegable("ccpet");?></tr>
        	<tr>
          		<td colspan="3" class="cinta">
					<a><img src="imagenes/add.png" title="Nuevo" onClick="location.href='#'" class="mgbt"/></a>
					<a class="mgbt"><img src="imagenes/guardad.png"/></a>
					<a><img src="imagenes/busca.png" title="Buscar"  onClick="location.href='#'" class="mgbt"/></a>
					<a href="#" onClick="mypop=window.open('ccp-principal.php','','');mypop.focus();" class="mgbt"><img src="imagenes/nv.png" title="Nueva Ventana"></a>
                    <img src="imagenes/excel.png" title="Excel" onClick='excell()' class="mgbt"/>
					<img src="imagenes/iratras.png" title="Atr&aacute;s" onClick="location.href='ccp-ejecucionpresupuestal.php'" class="mgbt"/>
				</td>
        	</tr>
		</table>
        
                <form name="form2" method="post">
                    
                    <input type="hidden" name="oculto" value="<?php echo $_POST['oculto'] ?>">

                    <div class="row" style="margin: 1px 10px 0px;">
                        <div class="col-12">
                            <div class="row" style="border-radius:2px; background-color: #E1E2E2;">
                                <div class="col-md-2" style="display: grid; align-content:center;">
                                    <label for="" style="margin-bottom: 0;">Secci&oacute;n Presupuestal: </label>
                                </div>
                                <div class="col-md-2" style="padding: 4px">

                                    <select name="unidadEjecutora" id="unidadEjecutora" style="font-size: 10px; margin-top:4px" class="form-control select" onChange="validar()" onKeyUp="return tabular(event,this)">
                                        <option value=''>Todos</option>
                                        <?php
                                            $sqlr = "select *from pptoseccion_presupuestal where estado='S'";
                                            $res = mysqli_query($linkbd, $sqlr);
                                            while ($row = mysqli_fetch_row($res))
                                            {
                                                echo "<option value=$row[0] ";
                                                $i=$row[0];
                                                if($i==$_POST['unidadEjecutora'])
                                                {
                                                    echo "SELECTED";
                                                }
                                                echo ">".$row[0]." - ".$row[2]."</option>";	 	 
                                            }
                                        ?>
                                    </select>
                                </div>
                                <div class="col-md-1" style="display: grid; align-content:center;">
                                    <label for="" style="margin-bottom: 0;">Medio pago: </label>
                                </div>
                                <div class="col-md-1" style="padding: 4px">

                                    <select name="medioDePago" id="medioDePago" style="font-size: 10px; margin-top:4px" class="form-control select" onChange="validar()" onKeyUp="return tabular(event,this)">
                                        <option value=''>Todos</option>
                                        <option value='CSF' <?php if($_POST['medioDePago']=='CSF') echo "SELECTED"; ?>>CSF</option>
                                        <option value='SSF' <?php if($_POST['medioDePago']=='SSF') echo "SELECTED"; ?>>SSF</option>
                                    </select>

                                </div>

                                <div class="col-md-2" style="display: grid; align-content:center;">
                                    <label for="" style="margin-bottom: 0; ">Vigencia del gasto: </label>
                                </div>
                                <div class="col-md-2" style="padding: 4px">
                                    <select name = "selectVigenciaGasto"  id = "selectVigenciaGasto" onChange="validar()" class="form-control select" style = "font-size: 10px; margin-top:4px">
                                        <option value=''>Todas</option>
                                        <?php
                                            $sqlr = "select *from ccpet_vigenciadelgasto ";
                                            $res = mysqli_query($linkbd, $sqlr);
                                            while ($row = mysqli_fetch_row($res))
                                            {
                                                echo "<option value=$row[1] ";
                                                $i=$row[1];
                                                if($i==$_POST['selectVigenciaGasto'])
                                                {
                                                    echo "SELECTED";
                                                }
                                                echo ">".$row[1]." - ".$row[2]."</option>";	 	 
                                            }
                                        ?>
                                    </select>
                                </div>

                                <!-- <div class="col-md-1" style="display: grid; align-content:center;">
                                    <label for="" style="margin-bottom: 0;">Vigencia: </label>
                                </div>
                                <div class="col-md-1" style="display: grid; align-content:center;">
                                    <select v-model="vigencia" style="width:100%" v-on:Change="cambiaCriteriosBusqueda" class="form-control" style = "font-size: 10px; margin-top:4px">
                                        
                                        <option v-for="year in years" :value="year[0]">{{ year[0] }}</option>
                                    </select>
                                </div> -->

                            </div>

                            <div class="row" style="border-radius:2px; background-color: #E1E2E2; ">
                                <div class="col-md-2" style="display: grid; align-content:center;">
                                    <label for="" style="margin-bottom: 0;">Fecha inicial:</label>
                                </div>
                                <div class="col-md-2" style="padding: 1px">
                                    <div class="row">
                                        <div class="col-md-8" style="padding: 1px; margin-left: 15px;">
                                            <input type="text" id="fc_1198971545" name="fechaini" value = "<?php echo $_POST['fechaini']; ?>" class="form-control input" title="DD/MM/YYYY" onChange = ""  placeholder="DD/MM/YYYY">
                                        </div>
                                        <div class="col-md-2" style="padding: 2px; padding-top: 8px">
                                            <a href="#" onClick="displayCalendarFor('fc_1198971545'); ocultarTabla()" tabindex="3" title="Calendario"><img src="imagenes/calendario04.png" align="absmiddle" style="width:20px;"></a>
                                        </div>
                                    </div>
                                    
                                </div>

                                <div class="col-md-1" style="display: grid; align-content:center;">
                                    <label for="" style="margin-bottom: 0;">Fecha Final:</label>
                                </div>
                                <div class="col-md-2" style="padding: 1px">
                                    <div class="row">
                                        <div class="col-md-8" style="padding: 1px; margin-left: 15px;">
                                            <input type="text" id="fc_1198971546" name="fechafin" class="form-control input" value = "<?php echo $_POST['fechafin']; ?>" title="DD/MM/YYYY" onChange = "" placeholder="DD/MM/YYYY">
                                        </div>
                                        <div class="col-md-2" style="padding: 2px; padding-top: 8px">
                                            <a href="#" onClick="displayCalendarFor('fc_1198971546'); ocultarTabla()" tabindex="3" title="Calendario"><img src="imagenes/calendario04.png" align="absmiddle" style="width:20px;"></a>
                                        </div>
                                    </div>
                                    
                                </div>
                                <div class="col-md-2">
                                    <button type="button" class="btn btn-success btn-sm" onClick="generarEjecucion()">Generar</button>
                                </div>

                            </div>
                        </div>
                    </div>
                
            <div class="subpantalla" style="height:420px; width:99.6%; padding:10px !important; background-color: white;">
            <table v-if="show_table_search" class="table table-hover">
                <thead >
                    <tr style="font-size: 90%; background-color: #2ECCFA; ">
                        <th style="border-radius: 5px 0px 0px 0px !important;">Detalle</th>
                        <th>C&oacute;digo</th>
                        <th>Nombre</th>
                        <th>Presupuesto inicial</th>
                        <th>Adici&oacute;n</th>
                        <th>Reducci&oacute;n</th>
                        <th>Definitivo</th>
                        <th>Superavit fiscal</th>
                        <th>Recaudos Anteriores</th>
                        <th>Recaudos de consulta</th>
                        <th>Total recaudos</th>
                        <th>Saldo por recaudar</th>
                        <th style="border-radius: 0px 5px 0px 0px !important;">En ejecucion</th>
                    </tr>
                </thead>
                <tbody>
                    <?php

                        if($_POST['oculto']==""){echo"<script>document.getElementById('divcarga').style.display='none';</script>";}

                        if($_POST['oculto'] == '3')
                        {
                            $crit2 = '';
                            $vigencia = '';
                            $critinv2 = '';

                            preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST['fechaini'],$fecha);
					        $fechaIni=''.$fecha[3].'-'.$fecha[2].'-'.$fecha[1].'';

                            preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST['fechafin'],$fechaf);
					        $fechaFin=''.$fechaf[3].'-'.$fechaf[2].'-'.$fechaf[1].'';

                            $vigencia = $fecha[3];

                            $sql = "SELECT codigo, nombre, tipo FROM cuentasingresosccpet WHERE municipio=1 AND version='$maxVersion' ORDER BY id ASC";
                            
                            $res = mysqli_query($linkbd, $sql);
                            $i = 0;
                
                            while($row = mysqli_fetch_row($res))
                            {
                                $recaudoPorCuenta = 0;
                                $totalRecaudoCuenta = 0;
                                $saldoPorRecaudar = 0;
                                $rubro = '';
                                $negrilla = 'font-weight: normal;';
                                $rubro = $row[0];

                                if($row[2] == 'A')
                                {
                                    $rubro = $row[0].'%';
                                    $negrilla = 'font-weight: bold;';
                                }

                                if($_POST['unidadEjecutora'] == '')
                                {
                                    $_POST['unidadEjecutora'] = '%';
                                }
                                
                                if($_POST['selectVigenciaGasto'] == '')
                                {
                                    $_POST['selectVigenciaGasto'] = '%';
                                }

                                $arregloGastos = reporteIngresosCcpet($rubro, $_POST['unidadEjecutora'], $_POST['medioDePago'], $vigencia, $fechaIni, $fechaFin, '%', $_POST['selectVigenciaGasto']);

                                $recaudoPorCuenta = generaRecaudoCcpet($row[0],$vigencia,$vigencia,$fechaIni,$fechaFin);

                                $totalRecaudoCuenta = $recaudoPorCuenta;

                                $saldoPorRecaudar = $arregloGastos[3] - $totalRecaudoCuenta;

                                $enEjecucion = round((($totalRecaudoCuenta/$arregloGastos[3])*100),2);

                                if($arregloGastos[0] > 0 || $arregloGastos[1] > 0)
                                {
                                    $nombreCuenta = buscaNombreCuentaCCPET($row[0], 1);
                                    $cuentas[$row[0]]["numCuenta"]=$row[0];
                                    $cuentas[$row[0]]["nomCuenta"]="$nombreCuenta";
                                    $cuentas[$row[0]]["tipo"]=$row[2];
                                    $cuentas[$row[0]]["presuInicial"]=$arregloGastos[0];
                                    $cuentas[$row[0]]["adicion"]=$arregloGastos[1];
                                    $cuentas[$row[0]]["reduccion"]=$arregloGastos[2];
                                    $cuentas[$row[0]]["presuDefinitivo"]=$arregloGastos[3];
                                    $cuentas[$row[0]]["superavit"]=0;
                                    $cuentas[$row[0]]["recaudosAnteriores"]=0;
                                    $cuentas[$row[0]]["RecaudoPorCuenta"]=$recaudoPorCuenta;
                                    $cuentas[$row[0]]["TotalRecaudo"]=$totalRecaudoCuenta;
                                    $cuentas[$row[0]]["saldo"]=$saldoPorRecaudar;
                                    $cuentas[$row[0]]["enEjecucion"]=$enEjecucion;
                                }
                                
                            }

                            $i = 0;

                            foreach ($cuentas as $key => $value) 
					        {
                                $numeroCuenta=$cuentas[$key]['numCuenta'];
                                $nombreCuenta=$cuentas[$key]['nomCuenta'];
                                $tipo=$cuentas[$key]['tipo'];
                                $presupuestoInicial=$cuentas[$key]['presuInicial'];
                                $adicion=$cuentas[$key]['adicion'];
                                $reduccion=$cuentas[$key]['reduccion'];
                                $presupuestoDefinitivo=$cuentas[$key]['presuDefinitivo'];
                                $superavit=$cuentas[$key]['superavit'];
                                $recaudosAnteriores=$cuentas[$key]['recaudosAnteriores'];
                                $recaudoPorCuenta=$cuentas[$key]['RecaudoPorCuenta'];
                                $totalRecaudo=$cuentas[$key]['TotalRecaudo'];
                                $saldo=$cuentas[$key]['saldo']; 
                                $enEjecucion=$cuentas[$key]['enEjecucion']; 
                                $style='';
                                $negrilla = 'font-weight: normal;';
                                if($tipo == 'A')
                                {
                                    $negrilla = 'font-weight: bold;';
                                }
                                if($_POST['unidadEjecutora'] == '%')
                                {
                                    $_POST['unidadEjecutora'] = '';
                                }


                                echo "
                                <tr style='$negrilla font-size: 90%; $style'>
                                    <td style='padding-left: 40px; padding-top: 15px'>";
                                        if($tipo == 'C'){
                                            echo "<a onClick='verDetalleIngresos($i, \"$numeroCuenta\",\"$_POST[unidadEjecutora]\",\"$_POST[medioDePago]\",\"$vigencia\",\"$fechaIni\",\"$fechaFin\")' style='cursor:pointer;'>
                                            <img id='img".$i."' style='width: 15px;' src='imagenes/plus.gif'>
                                            </a> 
                                            ";
                                        }
                                    echo "
                                    </td>
                                    <td>$numeroCuenta</td>
                                    <td>$nombreCuenta</td>
                                    <td style='width:300px% !important;'>".number_format($presupuestoInicial, 2, ',', '.')."</td>
                                    <td style='width:300px% !important;'>".number_format($adicion, 2, ',', '.')."</td>
                                    <td style='width:300px% !important;'>".number_format($reduccion, 2, ',', '.')."</td>
                                    <td style='width:300px% !important;'>".number_format($presupuestoDefinitivo, 2, ',', '.')."</td>
                                    <td style='width:300px% !important;'>".number_format($superavit, 2, ',', '.')."</td>
                                    <td style='width:300px% !important;'>".number_format($recaudosAnteriores, 2, ',', '.')."</td>
                                    <td style='width:300px% !important;'>".number_format($recaudoPorCuenta, 2, ',', '.')."</td>
                                    <td style='width:300px% !important;'>".number_format($totalRecaudo, 2, ',', '.')."</td>
                                    <td style='width:300px% !important;'>".number_format($saldo, 2, ',', '.')."</td>
                                    <td style='width: 300px'>".$enEjecucion."%</td>
                                    
                                </tr>
                                <input type='hidden' name='codigo[]' value='$numeroCuenta'>
                                <input type='hidden' name='nombre[]' value='$nombreCuenta'>
                                <input type='hidden' name='tipo[]' value='$tipo'>
                                <input type='hidden' name='presupuestoInicial[]' value='$presupuestoInicial'>
                                <input type='hidden' name='adicion[]' value='$adicion'>
                                <input type='hidden' name='reduccion[]' value='$reduccion'>
                                <input type='hidden' name='presupuestoDefinitivo[]' value='$presupuestoDefinitivo'>
                                <input type='hidden' name='superavit[]' value='$superavit'>
                                <input type='hidden' name='recaudosAnteriores[]' value='$recaudosAnteriores'>
                                <input type='hidden' name='recaudoPorCuenta[]' value='$recaudoPorCuenta'>
                                <input type='hidden' name='totalRecaudo[]' value='$totalRecaudo'>
                                <input type='hidden' name='saldo[]' value='$saldo'>
                                <input type='hidden' name='enEjecucion[]' value='$enEjecucion'>
                                
                                <tr cellspacing='0' cellpadding='0' style = ''>
                                    <td align='center' style='padding: 0px !important; border: 0px'></td>
                                    <td colspan='13' align='right' style='padding: 0px !important; border: 0px'>
                                        <div id='detalle".$i."' style='display:none;'></div>
                                    </td>
                                </tr>";

                                $i++;
                            }

                            echo"<script>document.getElementById('divcarga').style.display='none';</script>";
                            
                        }
                    ?>
                    </form>
                </tbody>
            </table>
        </div>
    </body>
</html>