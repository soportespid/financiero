<?php
ini_set('display_errors', 1);
error_reporting(E_ALL);

require __DIR__ . '/vendor/autoload.php';
require 'comun.inc';
require 'funciones.inc';

header("Cache-control: no-cache, no-store, must-revalidate");
header("Content-Type: text/html;charset=utf8");

use Laravel\Route;
use Laravel\DB;

$tenant_db = DB::connectWithDomain(Route::currentDomain());

$stmt = $tenant_db->prepare('SELECT * FROM `predios`');
if (! $stmt->execute()) {
    echo($tenant_db->error);
}
$result = $stmt->get_result();
print_r($result->fetch_array(MYSQLI_NUM));
?>
