<?php
header("Cache-control: no-cache, no-store, must-revalidate");
header("Content-Type: text/html;charset=utf8");

require 'comun.inc';
require 'funciones.inc';
require 'funcionesSP.inc.php';

$linkbd = conectar_v7();
$linkbd->set_charset("utf8");

session_start();
date_default_timezone_set("America/Bogota");
?>

<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1" />
    <meta name="viewport" content="user-scalable=no">
    <title>:: IDEAL 10 - Parametrización</title>
    <link href="favicon.ico" rel="shortcut icon" />
    <link href="css/css2n.css?<?php echo date('d_m_Y_h_i_s'); ?>" rel="stylesheet" type="text/css" />
    <link href="css/css3n.css?<?php echo date('d_m_Y_h_i_s'); ?>" rel="stylesheet" type="text/css" />
    <link href="css/css4.css?<?php echo date('d_m_Y_h_i_s'); ?>" rel="stylesheet" type="text/css" />
    <link href="css/cssSP.css?<?php echo date('d_m_Y_h_i_s'); ?>" rel="stylesheet" type="text/css" />
    <link href="css/style.css?<?php echo date('d_m_Y_h_i_s'); ?>" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="css/programas.js?<?php echo date('d_m_Y_h_i_s'); ?>"></script>
    <script type="text/javascript" src="css/calendario.js?<?php echo date('d_m_Y_h_i_s'); ?>"></script>
    <script type="text/javascript" src="css/sweetalert.js?<?php echo date('d_m_Y_h_i_s'); ?>"></script>
    <script type="text/javascript" src="css/sweetalert.min.js"></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <link href="css/sweetalert.css" rel="stylesheet" type="text/css" />

    <script>
        function despliegamodal2(_valor, _table) {
            document.getElementById('bgventanamodal2').style.visibility = _valor;
            var cliente = document.getElementById('cliente').value;

            if (_table == '') {
                document.getElementById('ventana2').src = '';
            }
            else if (_table == '') {
                document.getElementById('ventana2').src = '';
            }
        }

        function despliegamodalm(_valor, _tip, mensa, pregunta) {
            document.getElementById("bgventanamodalm").style.visibility = _valor;

            if (_valor == "hidden") {
                document.getElementById('ventanam').src = "";
            }
            else {
                switch (_tip) {
                    case "1":
                        document.getElementById('ventanam').src = "ventana-mensaje1.php?titulos=" + mensa;
                        break;

                    case "2":
                        document.getElementById('ventanam').src = "ventana-mensaje3.php?titulos=" + mensa;
                        break;

                    case "3":
                        document.getElementById('ventanam').src = "ventana-mensaje2.php?titulos=" + mensa;
                        break;

                    case "4":
                        document.getElementById('ventanam').src = "ventana-consulta1.php?titulos=" + mensa + "&idresp=" + pregunta;
                        break;
                }
            }
        }

        //ventanas servicios publicos
        function respuestaModalBusqueda2() {

        }

        function funcionmensaje() {
            location.href = "cont-crearUnionCCySP.php";
        }

        function respuestaconsulta(pregunta) {
            switch (pregunta) {
                case "1":
                    document.form2.oculto.value = '2';
                    document.form2.submit();
                    break;
            }
        }

        function guardar() {
            despliegamodalm('visible', '4', 'Esta seguro de guardar?', '1');
        }

        function upgrade() {
            document.form2.submit();
        }

        function add_details() {

            var codSP = document.getElementById('seccionPresupuestal').value;
            var nomSP = document.getElementById('nombre_SP').value;
            var codCC = document.getElementById('centroCosto').value;
            var nomCC = document.getElementById('nombre_CC').value;

            if (codSP != '-1' && nomSP != '') {

                if (codCC != '-1' && nomSP != '') {
                    document.form2.oculto.value = '4';
                    document.form2.submit();
                }
                else {
                    swal("Error", "NO HAS SELECCIONADO CENTRO DE COSTOS!", "error");
                }
            }
            else {
                swal("Error", "NO HAS SELECCIONADO SECCIÓN PRESUPUESTAL!", "error");
            }
        }

        function remove(item) {
            swal({
                title: "ESTA SEGURO DE ELIMINAR?",
                icon: "warning",
                buttons: ["No", "Ok"]


            }).then((response) => {
                if (response == true) {
                    document.getElementById('delete').value = item;
                    document.form2.oculto.value = '3';
                    document.form2.submit();
                }
            });
        }

        function modals(value, message) {

            switch (value) {

                case '1':
                    swal("Error", message, "error");
                    break;

                default:
                    alert('No existe este caso de modal');
                    break;
            }
        }
    </script>
</head>

<body>
    <IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
    <span id="todastablas2"></span>
    <table>
        <tr>
            <script>barra_imagenes("para");</script><?php cuadro_titulos(); ?>
        </tr>

        <tr><?php menu_desplegable("para"); ?></tr>
    </table>
    <div class="bg-white group-btn p-1"><button type="button" onclick="window.location.href=''"
            class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
            <span>Nuevo</span>
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960">
                <path d="M440-440H200v-80h240v-240h80v240h240v80H520v240h-80v-240Z"></path>
            </svg>
        </button><button type="button" onclick="guardar()"
            class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
            <span>Guardar</span>
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960">
                <path
                    d="M840-680v480q0 33-23.5 56.5T760-120H200q-33 0-56.5-23.5T120-200v-560q0-33 23.5-56.5T200-840h480l160 160Zm-80 34L646-760H200v560h560v-446ZM480-240q50 0 85-35t35-85q0-50-35-85t-85-35q-50 0-85 35t-35 85q0 50 35 85t85 35ZM240-560h360v-160H240v160Zm-40-86v446-560 114Z">
                </path>
            </svg>
        </button><button type="button" onclick="mypop=window.open('plan-agenda.php','','');mypop.focus()"
            class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
            <span class="group-hover:text-white">Agenda</span>
            <svg class="fill-black group-hover:fill-white w-5 h-5" xmlns="http://www.w3.org/2000/svg"
                viewBox="0 -960 960 960">
                <path
                    d="M560-564v-68q33-14 67.5-21t72.5-7q26 0 51 4t49 10v64q-24-9-48.5-13.5T700-600q-38 0-73 9.5T560-564Zm0 220v-68q33-14 67.5-21t72.5-7q26 0 51 4t49 10v64q-24-9-48.5-13.5T700-380q-38 0-73 9t-67 27Zm0-110v-68q33-14 67.5-21t72.5-7q26 0 51 4t49 10v64q-24-9-48.5-13.5T700-490q-38 0-73 9.5T560-454ZM260-320q47 0 91.5 10.5T440-278v-394q-41-24-87-36t-93-12q-36 0-71.5 7T120-692v396q35-12 69.5-18t70.5-6Zm260 42q44-21 88.5-31.5T700-320q36 0 70.5 6t69.5 18v-396q-33-14-68.5-21t-71.5-7q-47 0-93 12t-87 36v394Zm-40 118q-48-38-104-59t-116-21q-42 0-82.5 11T100-198q-21 11-40.5-1T40-234v-482q0-11 5.5-21T62-752q46-24 96-36t102-12q58 0 113.5 15T480-740q51-30 106.5-45T700-800q52 0 102 12t96 36q11 5 16.5 15t5.5 21v482q0 23-19.5 35t-40.5 1q-37-20-77.5-31T700-240q-60 0-116 21t-104 59ZM280-494Z">
                </path>
            </svg>
        </button><button type="button" onclick="mypop=window.open('para-principal.php','','');mypop.focus();"
            class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
            <span>Nueva ventana</span>
            <svg class="fill-black group-hover:fill-white w-5 h-5" xmlns="http://www.w3.org/2000/svg"
                viewBox="0 -960 960 960">
                <path
                    d="M320-120v-80h80v-80H160q-33 0-56.5-23.5T80-360v-400q0-33 23.5-56.5T160-840h640q33 0 56.5 23.5T880-760v400q0 33-23.5 56.5T800-280H560v80h80v80H320ZM160-360h640v-400H160v400Zm0 0v-400 400Z">
                </path>
            </svg>
        </button><button type="button"
            onclick="mypop=window.open('/financiero/cont-crearUnionCCySP.php','','');mypop.focus();"
            class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
            <span class="group-hover:text-white">Duplicar pantalla</span>
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960">
                <path
                    d="M640-160v-360H160v360h480Zm80-200v-80h80v-360H320v200h-80v-200q0-33 23.5-56.5T320-880h480q33 0 56.5 23.5T880-800v360q0 33-23.5 56.5T800-360h-80ZM160-80q-33 0-56.5-23.5T80-160v-360q0-33 23.5-56.5T160-600h480q33 0 56.5 23.5T720-520v360q0 33-23.5 56.5T640-80H160Zm400-603ZM400-340Z">
                </path>
            </svg>
        </button></div>
    <div id="bgventanamodalm" class="bgventanamodalm">
        <div id="ventanamodalm" class="ventanamodalm">
            <IFRAME src="" name="ventanam" marginWidth=0 marginHeight=0 frameBorder=0 id="ventanam" frameSpacing=0
                style=" width:700px; height:130px; top:200; overflow:hidden;"></IFRAME>
        </div>
    </div>

    <form name="form2" method="post" action="">
        <?php
        if (@$_POST['oculto'] == "") {
            $sql = "SELECT CS.id, CS.id_sp, SP.nombre, CS.id_cc, CC.nombre FROM centrocostos_seccionpresupuestal AS CS INNER JOIN pptoseccion_presupuestal AS SP ON CS.id_sp = SP.id_seccion_presupuestal INNER JOIN centrocosto AS CC ON CS.id_cc = CC.id_cc";
            $res = mysqli_query($linkbd, $sql);
            while ($row = mysqli_fetch_row($res)) {
                $_POST['id'][] = $row[0];
                $_POST['codeSP'][] = $row[1];
                $_POST['nameSP'][] = $row[2];
                $_POST['codeCC'][] = $row[3];
                $_POST['nameCC'][] = $row[4];
            }
        }
        ?>
        <div>
            <table class="inicio grande">
                <tr>
                    <td class="titulos" colspan="6">.: Unión Centro de Costos y Sección Presupuestal</td>

                    <td class="cerrar" style="width:7%" onclick="location.href='serv-principal.php'">Cerrar</td>
                </tr>

                <tr>
                    <td class="tamano01" style="width: 3cm; text-align:center;">Sección Presupuestal:</td>

                    <td>
                        <select name="seccionPresupuestal" id="seccionPresupuestal" class="centrarSelect"
                            style="width:100%;" onchange="upgrade();">
                            <option class='aumentarTamaño' value="-1">SELECCIONE SECCION PRESUPUESTAL</option>
                            <?php
                            $query = "SELECT id_seccion_presupuestal, nombre FROM pptoseccion_presupuestal WHERE estado = 'S'";
                            $resp = mysqli_query($linkbd, $query);
                            while ($row = mysqli_fetch_row($resp)) {

                                if (@$_POST['seccionPresupuestal'] == $row[0]) {
                                    echo "<option class='aumentarTamaño' value='$row[0]' SELECTED>$row[0] - $row[1]</option>";
                                    $_POST['nombre_SP'] = $row[1];
                                } else {
                                    echo "<option class='aumentarTamaño' value='$row[0]'>$row[0] - $row[1]</option>";
                                }
                            }
                            ?>
                        </select>

                        <input type="hidden" name="nombre_SP" id="nombre_SP" value="<?php echo @$_POST['nombre_SP'] ?>">
                    </td>

                    <td class="tamano01" style="width: 3cm; text-align:center;">Centro de Costos:</td>

                    <td>
                        <select name="centroCosto" id="centroCosto" class="centrarSelect" style="width:100%;"
                            onchange="upgrade();">
                            <option class='aumentarTamaño' value="-1">SELECCIONE CENTRO DE COSTOS</option>
                            <?php
                            $query = "SELECT id_cc, nombre FROM centrocosto WHERE estado = 'S'";
                            $resp = mysqli_query($linkbd, $query);
                            while ($row = mysqli_fetch_row($resp)) {

                                if (@$_POST['centroCosto'] == $row[0]) {
                                    echo "<option class='aumentarTamaño' value='$row[0]' SELECTED>$row[0] - $row[1]</option>";
                                    $_POST['nombre_CC'] = $row[1];
                                } else {
                                    echo "<option class='aumentarTamaño' value='$row[0]'>$row[0] - $row[1]</option>";
                                }
                            }
                            ?>
                        </select>

                        <input type="hidden" name="nombre_CC" id="nombre_CC" value="<?php echo @$_POST['nombre_CC'] ?>">
                    </td>

                    <td style="padding-bottom:0px">
                        <em class="botonflecha" onclick="add_details();">Agregar</em>
                    </td>
                </tr>
            </table>

            <div class="subpantalla" style="height:25em; width:99.6%; overflow-x:hidden;">
                <table class="inicio" width="99%">
                    <tr>
                        <td class="titulos" colspan="6" height="25">Detalles</td>
                    </tr>

                    <tr class="titulos2" style="text-align:center;">
                        <td>Item</td>
                        <td>C&oacute;digo Sección Presupuestal</td>
                        <td>Nombre Sección Presupuestal</td>
                        <td>C&oacute;digo Centro de Costos</td>
                        <td>Nombre Centro de Costos</td>
                        <td>Eliminar</td>
                    </tr>

                    <?php
                    if ($_POST['oculto'] == '4') {

                        for ($x = 0; $x < count($_POST['codeSP']); $x++) {

                            if (($_POST['seccionPresupuestal'] == $_POST['codeSP'][$x]) && ($_POST['centroCosto'] == $_POST['codeCC'][$x])) {
                                $validacion01 = 1;
                                echo "<script>modals('1', 'ERROR DATOS DUPLICADOS!');</script>";
                            }
                        }

                        if (isset($validacion01) != 1) {
                            $_POST['codeSP'][] = $_POST['seccionPresupuestal'];
                            $_POST['nameSP'][] = $_POST['nombre_SP'];
                            $_POST['codeCC'][] = $_POST['centroCosto'];
                            $_POST['nameCC'][] = $_POST['nombre_CC'];
                        }
                    }

                    if ($_POST['oculto'] == '3') {
                        $position = $_POST['delete'];

                        unset($_POST['codeSP'][$position]);
                        unset($_POST['nameSP'][$position]);
                        unset($_POST['codeCC'][$position]);
                        unset($_POST['nameCC'][$position]);

                        $_POST['codeSP'] = array_values($_POST['codeSP']);
                        $_POST['nameSP'] = array_values($_POST['nameSP']);
                        $_POST['codeCC'] = array_values($_POST['codeCC']);
                        $_POST['nameCC'] = array_values($_POST['nameCC']);
                        $_POST['delete'] = '';
                    }
                    ?>

                    <input type='hidden' name='delete' id='delete' />

                    <?php

                    $co = "saludo1a";
                    $co2 = "saludo2";

                    for ($x = 0; $x < count($_POST['codeSP']); $x++) {

                        echo "

                            <input type='hidden' name='codeSP[]' value='" . $_POST['codeSP'][$x] . "'/>
                            <input type='hidden' name='nameSP[]' value='" . $_POST['nameSP'][$x] . "'/>
                            <input type='hidden' name='codeCC[]' value='" . $_POST['codeCC'][$x] . "'/>
                            <input type='hidden' name='nameCC[]' value='" . $_POST['nameCC'][$x] . "'/>

                            <tr class='$co' style='text-align:center; height:30px;'>
                                <td>" . ($x + 1) . "</td>
                                <td>" . $_POST['codeSP'][$x] . "</td>
                                <td>" . $_POST['nameSP'][$x] . "</td>
                                <td>" . $_POST['codeCC'][$x] . "</td>
                                <td>" . $_POST['nameCC'][$x] . "</td>
                                <td style='text-align:center;'>
                                    <a onClick='remove($x)' style='cursor:pointer;'>
                                        <img src='imagenes/newdelete.png' style='height:30px;' title='Eliminar'>
                                    </a>
                                </td>

                            </tr>";

                        $aux = $co;
                        $co = $co2;
                        $co2 = $aux;
                    }
                    ?>
                </table>
            </div>

            <input type="hidden" name="oculto" id="oculto" value="1" />
        </div>
        <?php
        if (@$_POST['oculto'] == "2") {
            $clearTable = "DELETE FROM centrocostos_seccionpresupuestal";
            mysqli_query($linkbd, $clearTable);

            for ($x = 0; $x < count($_POST['codeSP']); $x++) {

                $length = 2;
                $centroCosto = substr(str_repeat(0, $length) . $_POST['codeCC'][$x], -$length);
                $seccionPresupuestal = $_POST['codeSP'][$x];

                $query = "INSERT INTO centrocostos_seccionpresupuestal (id_sp, id_cc) VALUES ('$seccionPresupuestal', '$centroCosto')";
                $resp = mysqli_query($linkbd, $query);
            }

            if ($resp) {
                echo "<script>despliegamodalm('visible','1','Se ha almacenado con Exito');</script>";
            } else {
                echo "<script>despliegamodalm('visible','2','No se pudo ejecutar la peticion');</script>";
            }
        }
        ?>
    </form>

    <div id="bgventanamodal2">
        <div id="ventanamodal2">
            <IFRAME src="" name="buster" marginWidth=0 marginHeight=0 frameBorder=0 id="ventana2" frameSpacing=0
                style="left:500px; width:900px; height:500px; top:200;"></IFRAME>
        </div>
    </div>
</body>

</html>
