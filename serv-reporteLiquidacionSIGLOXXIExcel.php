<?php
	require_once 'PHPExcel/Classes/PHPExcel.php';
	require "comun.inc";
	require "funciones.inc";
    ini_set('max_execution_time',99999999);
	session_start();
	$linkbd=conectar_v7();
	$objPHPExcel = new PHPExcel();
	//----Propiedades----
	$objPHPExcel->getProperties()
		->setCreator("IDEAL10")
		->setLastModifiedBy("IDEAL10")
		->setTitle("REPORTE LIQUIDACION")
		->setSubject("SP")
		->setDescription("SP")
		->setKeywords("SP")
		->setCategory("SERVICIOS PUBLICOS");
	$objPHPExcel->getActiveSheet()->mergeCells('A1:BJ1');
	$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A1', 'REPORTE LIQUIDACION PERIODO DE FACTURACION: '.$_POST['corte']);
	$objFont=$objPHPExcel->getActiveSheet()->getStyle('A1')->getFont();
	$objFont->setName('Courier New');
	$objFont->setSize(15);
	$objFont->setBold(true);
	$objFont->setUnderline(PHPExcel_Style_Font::UNDERLINE_SINGLE);
	$objFont->getColor()->setARGB( PHPExcel_Style_Color::COLOR_BLACK);
	$objAlign=$objPHPExcel->getActiveSheet()->getStyle('A1')->getAlignment(); 
	$objAlign->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
	$objAlign->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
	$objPHPExcel-> getActiveSheet ()
		-> getStyle ("A2:BJ2")
		-> getFill ()
		-> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
		-> getStartColor ()
		-> setRGB ('A6E5F3');
	$objPHPExcel-> getActiveSheet ()
		-> getStyle ("A1")	
		-> getFill ()
		-> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
		-> getStartColor ()
		-> setRGB ('A6E5F3');
	$borders = array(
		'borders' => array(
			'allborders' => array(
				'style' => PHPExcel_Style_Border::BORDER_THIN,
				'color' => array('argb' => 'FF000000'),
			)
		),
	);
	$objPHPExcel->getActiveSheet()->getStyle('A2:BJ2')->applyFromArray($borders);
	$objPHPExcel->setActiveSheetIndex(0)
			->setCellValue('A2', 'Numero Factura')
			->setCellValue('B2', 'Código Usuario')
			->setCellValue('C2', 'Nombre Suscriptor')
			->setCellValue('D2', 'Barrio')
			->setCellValue('E2', 'Dirección')
			->setCellValue('F2', 'Ruta')
			->setCellValue('G2', 'Código Ruta')

            ->setCellValue('H2', 'Servicio')
            ->setCellValue('I2', 'Estrato')
			->setCellValue('J2', 'Cargo Fijo')
			->setCellValue('K2', 'Consumo')
			->setCellValue('L2', 'Medidor')
			->setCellValue('M2', 'Lectura Anterior')
			->setCellValue('N2', 'Lectura Actual')
            ->setCellValue('O2', 'Consumo')
            ->setCellValue('P2', 'Consumo Basico')
            ->setCellValue('Q2', 'Consumo Complementario')
            ->setCellValue('R2', 'Consumo Suntuario')
            ->setCellValue('S2', 'Alumbrado')
            ->setCellValue('T2', 'Subsidio CB')
            ->setCellValue('U2', 'Subsidio CC')
            ->setCellValue('V2', 'Deuda Anterior')
            ->setCellValue('W2', 'Abonos')
            ->setCellValue('X2', 'Acuerdos de Pago')
            ->setCellValue('Y2', 'Venta de Medidores')
            ->setCellValue('Z2', 'Interes Moratorio')
            
            ->setCellValue('AA2', 'Servicio')
            ->setCellValue('AB2', 'Estrato')
			->setCellValue('AC2', 'Cargo Fijo')
			->setCellValue('AD2', 'Consumo')
			->setCellValue('AE2', 'Medidor')
			->setCellValue('AF2', 'Lectura Anterior')
			->setCellValue('AG2', 'Lectura Actual')
            ->setCellValue('AH2', 'Consumo')
            ->setCellValue('AI2', 'Cargo Fijo')
            ->setCellValue('AJ2', 'Consumo Basico')
            ->setCellValue('AK2', 'Consumo Complementario')
            ->setCellValue('AL2', 'Consumo Suntuario')
            ->setCellValue('AM2', 'Subsidio CF')
            ->setCellValue('AN2', 'Subsidio CB')
            ->setCellValue('AO2', 'Contribucion CF')
            ->setCellValue('AP2', 'Contribucion CB')
            ->setCellValue('AQ2', 'Deuda Anterior')
            ->setCellValue('AR2', 'Abonos')
            ->setCellValue('AS2', 'Acuerdos de Pago')
            ->setCellValue('AT2', 'Venta de Medidores')
            ->setCellValue('AU2', 'Interes Moratorio')
            
            ->setCellValue('AV2', 'Servicio')
            ->setCellValue('AW2', 'Estrato')
			->setCellValue('AX2', 'Cargo Fijo')
			->setCellValue('AY2', 'Consumo')
            ->setCellValue('AZ2', 'TBL')
            ->setCellValue('BA2', 'TRT')
            ->setCellValue('BB2', 'TDT')
            ->setCellValue('BC2', 'TFR')
            ->setCellValue('BD2', 'Subsidio')
            ->setCellValue('BE2', 'Contribucion')
            ->setCellValue('BF2', 'Deuda Anterior')
            ->setCellValue('BG2', 'Abonos')
            ->setCellValue('BH2', 'Acuerdos de Pago')
            ->setCellValue('BI2', 'Interes Moratorio')

            ->setCellValue('BJ2', 'Total Factura');

            
           
	$i=3;
	for($ii=0;$ii<count ($_POST['numFactura']);$ii++)
	{
		$objPHPExcel->setActiveSheetIndex(0)
		->setCellValueExplicit ("A$i", $_POST['numFactura'][$ii], PHPExcel_Cell_DataType :: TYPE_STRING)
		->setCellValueExplicit ("B$i", $_POST['codUsuario'][$ii], PHPExcel_Cell_DataType :: TYPE_STRING)
		->setCellValueExplicit ("C$i", $_POST['nombreSus'][$ii], PHPExcel_Cell_DataType :: TYPE_STRING)
		->setCellValueExplicit ("D$i", $_POST['barrio'][$ii], PHPExcel_Cell_DataType :: TYPE_STRING)
		->setCellValueExplicit ("E$i", $_POST['direccion'][$ii], PHPExcel_Cell_DataType :: TYPE_STRING)
		->setCellValueExplicit ("F$i", $_POST['ruta'][$ii], PHPExcel_Cell_DataType :: TYPE_STRING)
		->setCellValueExplicit ("G$i", $_POST['codigoRuta'][$ii], PHPExcel_Cell_DataType :: TYPE_STRING);

        for($jj=0;$jj<count ($_POST['servicio'][$ii]);$jj++)
        {
            if($jj == 0)
            {
                $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValueExplicit ("H$i", $_POST['servicio'][$ii][$jj], PHPExcel_Cell_DataType :: TYPE_STRING)
                ->setCellValueExplicit ("I$i", $_POST['estrato'][$ii][$jj], PHPExcel_Cell_DataType :: TYPE_STRING)
                ->setCellValueExplicit ("J$i", $_POST['estadoCF'][$ii][$jj], PHPExcel_Cell_DataType :: TYPE_STRING)
                ->setCellValueExplicit ("K$i", $_POST['estadoCS'][$ii][$jj], PHPExcel_Cell_DataType :: TYPE_STRING)
                ->setCellValueExplicit ("L$i", $_POST['medidor'][$ii][$jj], PHPExcel_Cell_DataType :: TYPE_STRING)
                ->setCellValueExplicit ("M$i", $_POST['lecturaAnt'][$ii][$jj], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
                ->setCellValueExplicit ("N$i", $_POST['lecturaAct'][$ii][$jj], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
                ->setCellValueExplicit ("O$i", $_POST['consumo'][$ii][$jj], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
                ->setCellValueExplicit ("P$i", $_POST['cb'][$ii][$jj], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
                ->setCellValueExplicit ("Q$i", $_POST['cc'][$ii][$jj], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
                ->setCellValueExplicit ("R$i", $_POST['cs'][$ii][$jj], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
                ->setCellValueExplicit ("S$i", $_POST['alumbrado'][$ii][$jj], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
                ->setCellValueExplicit ("T$i", $_POST['sb_cb'][$ii][$jj], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
                ->setCellValueExplicit ("U$i", $_POST['sb_cc'][$ii][$jj], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
                ->setCellValueExplicit ("V$i", $_POST['deuda_anterior'][$ii][$jj], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
                ->setCellValueExplicit ("W$i", $_POST['abonos'][$ii][$jj], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
                ->setCellValueExplicit ("X$i", $_POST['acuero_p'][$ii][$jj], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
                ->setCellValueExplicit ("Y$i", $_POST['venta_m'][$ii][$jj], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
                ->setCellValueExplicit ("Z$i", $_POST['interes_m'][$ii][$jj], PHPExcel_Cell_DataType :: TYPE_NUMERIC);
            }

            if($jj == 1)
            {
                $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValueExplicit ("AA$i", $_POST['servicio'][$ii][$jj], PHPExcel_Cell_DataType :: TYPE_STRING)
                ->setCellValueExplicit ("AB$i", $_POST['estrato'][$ii][$jj], PHPExcel_Cell_DataType :: TYPE_STRING)
                ->setCellValueExplicit ("AC$i", $_POST['estadoCF'][$ii][$jj], PHPExcel_Cell_DataType :: TYPE_STRING)
                ->setCellValueExplicit ("AD$i", $_POST['estadoCS'][$ii][$jj], PHPExcel_Cell_DataType :: TYPE_STRING)
                ->setCellValueExplicit ("AE$i", $_POST['medidor'][$ii][$jj], PHPExcel_Cell_DataType :: TYPE_STRING)
                ->setCellValueExplicit ("AF$i", $_POST['lecturaAnt'][$ii][$jj], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
                ->setCellValueExplicit ("AG$i", $_POST['lecturaAct'][$ii][$jj], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
                ->setCellValueExplicit ("AH$i", $_POST['consumo'][$ii][$jj], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
                ->setCellValueExplicit ("AI$i", $_POST['cf'][$ii][$jj], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
                ->setCellValueExplicit ("AJ$i", $_POST['cb'][$ii][$jj], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
                ->setCellValueExplicit ("AK$i", $_POST['cc'][$ii][$jj], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
                ->setCellValueExplicit ("AL$i", $_POST['cs'][$ii][$jj], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
                ->setCellValueExplicit ("AM$i", $_POST['sb_cf'][$ii][$jj], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
                ->setCellValueExplicit ("AN$i", $_POST['sb_cb'][$ii][$jj], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
                ->setCellValueExplicit ("AO$i", $_POST['cb_cf'][$ii][$jj], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
                ->setCellValueExplicit ("AP$i", $_POST['cb_cb'][$ii][$jj], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
                ->setCellValueExplicit ("AQ$i", $_POST['deuda_anterior'][$ii][$jj], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
                ->setCellValueExplicit ("AR$i", $_POST['abonos'][$ii][$jj], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
                ->setCellValueExplicit ("AS$i", $_POST['acuero_p'][$ii][$jj], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
                ->setCellValueExplicit ("AT$i", $_POST['venta_m'][$ii][$jj], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
                ->setCellValueExplicit ("AU$i", $_POST['interes_m'][$ii][$jj], PHPExcel_Cell_DataType :: TYPE_NUMERIC);
            }
            

            if($jj == 2)
            {
                $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValueExplicit ("AV$i", $_POST['servicio'][$ii][$jj], PHPExcel_Cell_DataType :: TYPE_STRING)
                ->setCellValueExplicit ("AW$i", $_POST['estrato'][$ii][$jj], PHPExcel_Cell_DataType :: TYPE_STRING)
                ->setCellValueExplicit ("AX$i", $_POST['estadoCF'][$ii][$jj], PHPExcel_Cell_DataType :: TYPE_STRING)
                ->setCellValueExplicit ("AY$i", $_POST['estadoCS'][$ii][$jj], PHPExcel_Cell_DataType :: TYPE_STRING)
                ->setCellValueExplicit ("AZ$i", $_POST['cf'][$ii][$jj], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
                ->setCellValueExplicit ("BA$i", $_POST['cb'][$ii][$jj], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
                ->setCellValueExplicit ("BB$i", $_POST['cc'][$ii][$jj], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
                ->setCellValueExplicit ("BC$i", $_POST['cs'][$ii][$jj], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
                ->setCellValueExplicit ("BD$i", $_POST['sb_cf'][$ii][$jj], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
                ->setCellValueExplicit ("BE$i", $_POST['cb_cf'][$ii][$jj], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
                ->setCellValueExplicit ("BF$i", $_POST['deuda_anterior'][$ii][$jj], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
                ->setCellValueExplicit ("BG$i", $_POST['abonos'][$ii][$jj], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
                ->setCellValueExplicit ("BH$i", $_POST['acuero_p'][$ii][$jj], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
                ->setCellValueExplicit ("BI$i", $_POST['interes_m'][$ii][$jj], PHPExcel_Cell_DataType :: TYPE_NUMERIC);
            }
        }

        
        $objPHPExcel->setActiveSheetIndex(0)
        ->setCellValueExplicit ("BJ$i", $_POST['total'][$ii], PHPExcel_Cell_DataType :: TYPE_NUMERIC);
        $i++;
	}

    $objPHPExcel->getActiveSheet()->getStyle("A$i:BJ$i")->applyFromArray($borders);
	$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('K')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('L')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('M')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('N')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('O')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('P')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('Q')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('R')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('S')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('T')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('U')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('V')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('W')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('X')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('Y')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('Z')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('AA')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('AB')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('AC')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('AD')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('AE')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('AF')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('AG')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('AH')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('AI')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('AJ')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('AK')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('AL')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('AM')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('AN')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('AO')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('AP')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('AQ')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('AR')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('AS')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('AT')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('AU')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('AV')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('AW')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('AX')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('AY')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('AZ')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('BA')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('BB')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('BC')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('BD')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('BE')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('BF')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('BG')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('BH')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('BI')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('BJ')->setAutoSize(true);

	$objPHPExcel->getActiveSheet()->setTitle('SP');
	header('Content-Type: application/vnd.ms-excel');
	header('Content-Disposition: attachment;filename="Reporte LIQUIDACION.xlsx"');
	header('Cache-Control: max-age=0');
	$objWriter=PHPExcel_IOFactory::createWriter($objPHPExcel,'Excel2007');
	$objWriter->save('php://output');
	exit;
?>