<?php

    header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require 'comun.inc';
	require 'funciones.inc';
	session_start();
    if(empty($_SESSION)){
        header("location: index.php");
    }
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html lang=es>
	<head>
    <meta charset="utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1"/>
		<meta name="viewport" content="user-scalable=no">
		<title>:: IDEAL 10 - Tesorería</title>
		<link href="favicon.ico" rel="shortcut icon"/>
		<link href="css/css2z.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
        <link href="css/tabs.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
        <link href="css/style.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" href="sweetalert2/dist/sweetalert2.min.css">
        <script type="text/javascript" src="css/programas.js"></script>
        <script src="sweetalert2/dist/sweetalert2.min.js"></script>
		<script src="vue/vue.min.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script src="css/calendario.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
	</head>
	<body>
		<header>
			<table>
				<tr><script>barra_imagenes("teso");</script><?php cuadro_titulos();?></tr>
			</table>
		</header>

        <main id="myapp" v-cloak>
            <input type="hidden" value = "1" ref="pageType">
            <div class="loading-container" v-show="isLoading" >
                <p class="text-loading"data-loading="IDEAL 10">IDEAL 10</p>
            </div>
            <nav>
                <table>
                    <tr><?php menu_desplegable("teso");?></tr>
                </table>
                <div class="bg-white group-btn p-1" id="newNavStyle">
                    <button type="button" @click="save()" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
                        <span>Guardar</span>
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960">
                            <path d="M840-680v480q0 33-23.5 56.5T760-120H200q-33 0-56.5-23.5T120-200v-560q0-33 23.5-56.5T200-840h480l160 160Zm-80 34L646-760H200v560h560v-446ZM480-240q50 0 85-35t35-85q0-50-35-85t-85-35q-50 0-85 35t-35 85q0 50 35 85t85 35ZM240-560h360v-160H240v160Zm-40-86v446-560 114Z">
                            </path>
                        </svg>
                    </button>
                    <button type="button" @click="window.location.href='teso-ingresosLiquidacionIngresosBuscar'" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
                        <span>Buscar</span>
                        <svg class="fill-black group-hover:fill-white w-5 h-5" xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960"><path d="M784-120 532-372q-30 24-69 38t-83 14q-109 0-184.5-75.5T120-580q0-109 75.5-184.5T380-840q109 0 184.5 75.5T640-580q0 44-14 83t-38 69l252 252-56 56ZM380-400q75 0 127.5-52.5T560-580q0-75-52.5-127.5T380-760q-75 0-127.5 52.5T200-580q0 75 52.5 127.5T380-400Z"></path></svg>
                    </button>
                    <button type="button" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center" @click="mypop=window.open('teso-principal.php','',''); mypop.focus();">
                        <span>Nueva ventana</span>
                        <svg class="fill-black group-hover:fill-white w-5 h-5" xmlns="http://www.w3.org/2000/svg"  viewBox="0 -960 960 960" ><path d="M320-120v-80h80v-80H160q-33 0-56.5-23.5T80-360v-400q0-33 23.5-56.5T160-840h640q33 0 56.5 23.5T880-760v400q0 33-23.5 56.5T800-280H560v80h80v80H320ZM160-360h640v-400H160v400Zm0 0v-400 400Z"/></svg>
                    </button>
                    <button type="button" @click="mypop=window.open('teso-ingresosLiquidacionIngresosCrear','','');mypop.focus();" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
                        <span class="group-hover:text-white">Duplicar pantalla</span>
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960"><path d="M640-160v-360H160v360h480Zm80-200v-80h80v-360H320v200h-80v-200q0-33 23.5-56.5T320-880h480q33 0 56.5 23.5T880-800v360q0 33-23.5 56.5T800-360h-80ZM160-80q-33 0-56.5-23.5T80-160v-360q0-33 23.5-56.5T160-600h480q33 0 56.5 23.5T720-520v360q0 33-23.5 56.5T640-80H160Zm400-603ZM400-340Z"></path></svg>
                    </button>
                    <button type="button" @click="window.location.href='teso-ingresosLiquidacionIngresosBuscar'" class="btn btn-success btn-success-hover d-flex justify-between align-items-center">
                        <span>Atras</span>
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960"><path d="M760-200v-160q0-50-35-85t-85-35H273l144 144-57 56-240-240 240-240 57 56-144 144h367q83 0 141.5 58.5T840-360v160h-80Z"></path></svg>
                    </button>
                </div>
            </nav>
            <section class="bg-white">

                <!--TABS-->
                <div ref="rTabs" class="nav-tabs p-1">
                    <div class="nav-item active" @click="showTab(1)">Información general</div>
                    <div class="nav-item" @click="showTab(2)">Parafiscales</div>
                    <div class="nav-item" @click="showTab(3)">Clausula de obligaciones</div>
                    <div class="nav-item" @click="showTab(4)">Otras cláusulas</div>
                    <div class="nav-item exclude bg-success text-white" @click="exportData()">Visualizar contrato</div>
                </div>

                <!--CONTENIDO TABS-->
                <div ref="rTabsContent" class="nav-tabs-content">
                    <div class="nav-content active">
                        <div>
                            <h2 class="titulos m-0">Creación de contrato</h2>
                            <p class="m-0 ms-2">Todos los campos con (<span class="text-danger fw-bolder">*</span>) son obligatorios.</p>
                            <div class="d-flex w-100">
                                <div class="w-50 d-flex">
                                    <div class="form-control">
                                        <label class="form-label">Tipo de contrato <span class="text-danger fw-bolder">*</span>:</label>
                                        <select v-model="selectContrato">
                                            <option value="1">Término indefinido</option>
                                            <option value="2">Término fijo</option>
                                            <option value="3">Ocasional o transitorio</option>
                                            <option value="4">Obra o labor</option>
                                            <option value="5">Prestación de servicios</option>
                                        </select>
                                    </div>
                                    <div class="form-control">
                                        <label class="form-label">Periodo de pago <span class="text-danger fw-bolder">*</span>:</label>
                                        <select v-model="selectPeriodo">
                                            <option v-for="(data,index) in arrPeriodos" :value="data.dias">{{data.nombre}}</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="w-50 d-flex">
                                    <div class="form-control">
                                        <label class="form-label">Fecha de ingreso <span class="text-danger fw-bolder">*</span>:</label>
                                        <input type="date" v-model="txtFechaIngreso">
                                    </div>
                                    <div class="form-control" v-if="selectContrato == 2">
                                        <label class="form-label">Fecha de terminación <span class="text-danger fw-bolder">*</span>:</label>
                                        <input type="date" v-model="txtFechaTerminacion">
                                    </div>
                                </div>
                            </div>
                            <div class="d-flex">
                                <div class="form-control">
                                    <label class="form-label">Cuenta bancaria <span class="text-danger fw-bolder">*</span>:</label>
                                    <div class="d-flex">
                                        <select v-model="selectBancos">
                                            <option v-for="(data,index) in arrBancos" :key="index" :value="data.codigo">
                                                {{ data.codigo+" - "+data.nombre }}
                                            </option>
                                        </select>
                                        <input type="text" placeholder="Número de cuenta" v-model="txtCuentaBancaria">
                                    </div>
                                </div>
                                <div class="form-control">
                                    <label class="form-label">Tercero <span class="text-danger fw-bolder">*</span>:</label>
                                    <div class="d-flex">
                                        <input type="text" class="w-25 bg-warning cursor-pointer" v-model="objTercero.codigo" @dblclick="isModalTercero=true;terceroType=1" @change="search('cod_tercero')" >
                                        <input type="text" v-model="objTercero.nombre" disabled>
                                    </div>
                                </div>
                            </div>
                            <div class="w-100 d-flex">
                                <div class="w-50">
                                    <div class="d-flex">
                                        <div class="form-control">
                                            <label class="form-label">Cargo <span class="text-danger fw-bolder">*</span>:</label>
                                            <input type="text" class="bg-warning cursor-pointer" :value="objCargo.nombre" @dblclick="isModalCargo=true" >
                                        </div>
                                        <div class="form-control w-25">
                                            <label class="form-label">Escala</label>
                                            <input type="number" :value="objCargo.escala" class="text-center" disabled>
                                        </div>

                                        <div class="form-control">
                                            <label class="form-label">Salario</label>
                                            <input type="text" :value="formatNum(objCargo.valor)" class="text-right" disabled>
                                        </div>
                                    </div>
                                </div>
                                <div class="w-50 d-flex">
                                    <div class="form-control">
                                        <label class="form-label">Nivel salarial</label>
                                        <input type="text" :value="objCargo.nivel_salarial"  disabled>
                                    </div>
                                </div>
                            </div>
                            <div class="w-100 d-flex">
                                <div class="d-flex w-50">
                                    <div class="form-control">
                                        <label class="form-label">Jornada laboral <span class="text-danger fw-bolder">*</span>:</label>
                                        <select v-model="selectJornada">
                                            <option value="1">Lunes a viernes</option>
                                            <option value="2">Lunes a sábado</option>
                                            <option value="3">Lunes a domingo</option>
                                        </select>
                                    </div>
                                    <div class="form-control">
                                        <label class="form-label">Horario <span class="text-danger fw-bolder">*</span>:</label>
                                        <div class="d-flex align-items-center justify-between">
                                            <input type="time" v-model="txtJornadaLvInicio">
                                            <span class="me-1 ms-1">-</span>
                                            <input type="time" v-model="txtJornadaLvFinal">
                                        </div>
                                    </div>
                                </div>
                                <div class="w-50 d-flex">
                                    <div class="form-control" v-if="selectJornada == 2 || selectJornada == 3">
                                        <label class="form-label">Horario sábado <span class="text-danger fw-bolder">*</span>:</label>
                                        <div class="d-flex align-items-center justify-between">
                                            <input type="time" v-model="txtJornadaLsInicio">
                                            <span class="me-1 ms-1">-</span>
                                            <input type="time" v-model="txtJornadaLsFinal">
                                        </div>
                                    </div>
                                    <div class="form-control" v-if="selectJornada == 3">
                                        <label class="form-label">Horario domingo <span class="text-danger fw-bolder">*</span>:</label>
                                        <div class="d-flex align-items-center justify-between">
                                            <input type="time" v-model="txtJornadaLdInicio">
                                            <span class="me-1 ms-1">-</span>
                                            <input type="time" v-model="txtJornadaLdFinal">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <h2 class="titulos m-0">Afectación presupuestal</h2>
                            <div class="d-flex">
                                <div class="form-control">
                                    <label class="form-label">Vinculación <span class="text-danger fw-bolder">*</span>:</label>
                                    <select>
                                        <option value="T">Temporal</option>
                                        <option value="P">Permanente</option>
                                    </select>
                                </div>
                                <div class="form-control">
                                    <label class="form-label">Presupuesto salarios <span class="text-danger fw-bolder">*</span>:</label>
                                    <select>
                                        <option value="F">Funcionamiento</option>
                                        <option value="I">Inversión</option>
                                        <option value="G">Gastos comercialización</option>
                                    </select>
                                </div>
                                <div class="form-control">
                                    <label class="form-label">Presupuesto otros pagos <span class="text-danger fw-bolder">*</span>:</label>
                                    <select>
                                        <option value="F">Funcionamiento</option>
                                        <option value="I">Inversión</option>
                                        <option value="G">Gastos comercialización</option>
                                    </select>
                                </div>
                                <div class="form-control">
                                    <label class="form-label">Unidad ejecutora <span class="text-danger fw-bolder">*</span>:</label>
                                    <select v-model="selectUnidad" @change="changeUnidad()">
                                        <option value="-1" selected>Seleccione</option>
                                        <option v-for="(data,index) in arrUnidades" :value="data.codigo">{{data.codigo+" - "+data.nombre}}</option>
                                    </select>
                                </div>
                                <div class="form-control">
                                    <label class="form-label">Sección presupuestal <span class="text-danger fw-bolder">*</span>:</label>
                                    <select v-model="selectSeccion" @change="changeUnidad('seccion')">
                                        <option value="-1" selected>Seleccione</option>
                                        <option v-for="(data,index) in arrSeccionesCopy" :value="data.codigo">{{data.codigo+" - "+data.nombre}}</option>
                                    </select>
                                </div>
                                <div class="form-control">
                                    <label class="form-label">Centro de costo <span class="text-danger fw-bolder">*</span>:</label>
                                    <select v-model="selectCentro">
                                        <option value="-1" selected>Seleccione</option>
                                        <option v-for="(data,index) in arrCentrosCopy" :value="data.codigo">{{data.codigo+" - "+data.nombre}}</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="nav-content">
                        <div class="d-flex">
                            <div class="form-control">
                                <label class="form-label">EPS <span class="text-danger fw-bolder">*</span>:</label>
                                <div class="d-flex">
                                    <input type="text" class="w-25 bg-warning cursor-pointer" v-model="objEps.codigo" @dblclick="isModalTercero=true;terceroType=2" @change="search('cod_tercero')" >
                                    <input type="text" v-model="objEps.nombre" disabled>
                                </div>
                            </div>
                            <div class="form-control w-25">
                                <label class="form-label">Fecha EPS <span class="text-danger fw-bolder">*</span>:</label>
                                <input type="date">
                            </div>
                        </div>
                        <div class="d-flex">
                            <div class="form-control">
                                <label class="form-label">ARL <span class="text-danger fw-bolder">*</span>:</label>
                                <div class="d-flex">
                                    <input type="text" class="w-25 bg-warning cursor-pointer" v-model="objArl.codigo" @dblclick="isModalTercero=true;terceroType=3" @change="search('cod_tercero')" >
                                    <input type="text" v-model="objArl.nombre" disabled>
                                </div>
                            </div>
                            <div class="form-control w-25">
                                <label class="form-label">Fecha ARL <span class="text-danger fw-bolder">*</span>:</label>
                                <input type="date">
                            </div>
                        </div>
                        <div class="d-flex">
                            <div class="form-control">
                                <label class="form-label">AFP <span class="text-danger fw-bolder">*</span>:</label>
                                <div class="d-flex">
                                    <input type="text" class="w-25 bg-warning cursor-pointer" v-model="objAfp.codigo" @dblclick="isModalTercero=true;terceroType=4" @change="search('cod_tercero')" >
                                    <input type="text" v-model="objAfp.nombre" disabled>
                                </div>
                            </div>
                            <div class="form-control w-25">
                                <label class="form-label">Fecha AFP <span class="text-danger fw-bolder">*</span>:</label>
                                <input type="date">
                            </div>
                        </div>
                        <div class="d-flex">
                            <div class="form-control">
                                <label class="form-label">Fondo de censantías <span class="text-danger fw-bolder">*</span>:</label>
                                <div class="d-flex">
                                    <input type="text" class="w-25 bg-warning cursor-pointer" v-model="objFondo.codigo" @dblclick="isModalTercero=true;terceroType=5" @change="search('cod_tercero')" >
                                    <input type="text" v-model="objFondo.nombre" disabled>
                                </div>
                            </div>
                            <div class="form-control w-25">
                                <label class="form-label">Fecha cesantías <span class="text-danger fw-bolder">*</span>:</label>
                                <input type="date">
                            </div>
                        </div>
                        <div class="d-flex">
                            <div class="form-control">
                                <label class="form-label">Pago de cesantías <span class="text-danger fw-bolder">*</span>:</label>
                                <select>
                                    <option value="1">Mensual</option>
                                    <option value="2">Quincenal</option>
                                </select>
                            </div>
                            <div class="form-control">
                                <label class="form-label">Nivel de riesgo <span class="text-danger fw-bolder">*</span>:</label>
                                <select>
                                    <option value="1">Mensual</option>
                                    <option value="2">Quincenal</option>
                                </select>
                            </div>
                        </div>
                        <h2 class="titulos m-0">Porcentajes</h2>
                        <div class="d-flex">
                            <div class="form-control">
                                <label class="form-label">Salud funcionario <span class="text-danger fw-bolder">*</span>:</label>
                                <input type="number" v-model="txtParaSaludFuncionario">
                            </div>
                            <div class="form-control">
                                <label class="form-label">Salud empleador <span class="text-danger fw-bolder">*</span>:</label>
                                <input type="number" v-model="txtParaSaludEmpleador">
                            </div>
                            <div class="form-control">
                                <label class="form-label">Pensión funcionario <span class="text-danger fw-bolder">*</span>:</label>
                                <input type="number" v-model="txtParaPensionFuncionario">
                            </div>
                            <div class="form-control">
                                <label class="form-label">Pensión empleador <span class="text-danger fw-bolder">*</span>:</label>
                                <input type="number" v-model="txtParaPensionEmpleador">
                            </div>
                            <div class="form-control">
                                <label class="form-label">CCF <span class="text-danger fw-bolder">*</span>:</label>
                                <input type="number" v-model="txtParaCcf">
                            </div>
                            <div class="form-control">
                                <label class="form-label">ICBF <span class="text-danger fw-bolder">*</span>:</label>
                                <input type="number" v-model="txtParaIcbf">
                            </div>
                            <div class="form-control">
                                <label class="form-label">SENA <span class="text-danger fw-bolder">*</span>:</label>
                                <input type="number" v-model="txtParaSena">
                            </div>
                            <div class="form-control">
                                <label class="form-label">Interés cesantías <span class="text-danger fw-bolder">*</span>:</label>
                                <input type="number" v-model="txtParaInterCesantias">
                            </div>
                            <div class="form-control">
                                <label class="form-label">ESAP <span class="text-danger fw-bolder">*</span>:</label>
                                <input type="number" v-model="txtParaEsap">
                            </div>
                        </div>
                    </div>
                    <div class="nav-content">
                        <div class="d-flex me-1 ms-1">
                            <div class="w-50 me-2">
                                <h2 class="titulos m-0">Obligaciones empleador</h2>
                                <div class="d-flex">
                                    <div class="form-control m-0 mt-2 mb-2">
                                        <div class="d-flex">
                                            <input type="text" v-model="txtObligacionEmpleador">
                                            <button type="button" @click="addObligacion()" class="btn btn-primary">Agregar</button>
                                        </div>
                                    </div>
                                </div>
                                <h2 class="titulos m-0">Obligaciones creadas</h2>
                                <div class="overflow-auto" style="height:50vh">
                                    <div class="form-control m-0 mb-1 mt-1 flex-row" v-for="(data,index) in arrObligaciones" :key="index" v-if="data.type == 1">
                                        <input type="text" v-model="data.value">
                                        <button type="button" class="btn btn-danger" @click="delItem(index)">Eliminar</button>
                                    </div>
                                </div>
                            </div>
                            <div class="w-50">
                                <h2 class="titulos m-0">Obligaciones empleado</h2>
                                <div class="d-flex">
                                    <div class="form-control m-0 mt-2 mb-2">
                                        <div class="d-flex">
                                            <input type="text" v-model="txtObligacionEmpleado">
                                            <button type="button" @click="addObligacion(2)" class="btn btn-primary">Agregar</button>
                                        </div>
                                    </div>
                                </div>
                                <h2 class="titulos m-0">Obligaciones creadas</h2>
                                <div class="overflow-auto" style="height:50vh">
                                    <div class="form-control m-0 mb-1 mt-1 flex-row" v-for="(data,index) in arrObligaciones" :key="index" v-if="data.type == 2">
                                        <input type="text" v-model="data.value">
                                        <button type="button" class="btn btn-danger" @click="delItem(index)">Eliminar</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="nav-content">
                        <div class="d-flex">
                            <div class="form-control">
                                <label class="form-label">Cláusulas:</label>
                                <div class="d-flex">
                                    <textarea  v-model="txtClausula"></textarea>
                                </div>
                            </div>
                            <div class="form-control">
                                <label class="form-label">Parágrafo (opcional)</label>
                                <div class="d-flex">
                                    <textarea  v-model="txtParagrafo"></textarea>
                                    <button type="button" class="btn btn-primary" @click="addClausula()">Agregar</button>
                                </div>
                            </div>
                        </div>
                        <h2 class="titulos m-0">Claúsulas creadas</h2>
                        <div class="overflow-auto" style="height:50vh">
                            <div v-for="(data,index) in arrClausulas" :key="index">
                                <div class="d-flex">
                                    <div class="form-control">
                                        <label class="form-label fw-bold">Artículo.</label>
                                        <textarea  v-model="data.clausula"></textarea>
                                    </div>
                                    <div class="form-control">
                                        <label class="form-label fw-bold">Parágrafo</label>
                                        <div class="d-flex">
                                            <textarea  v-model="data.paragrafo"></textarea>
                                            <button type="button" class="btn btn-danger" @click="delItem(index,2)">Eliminar</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="nav-content"></div>
                </div>
            </section>

            <!-- MODALES-->
            <div v-show="isModalTercero" class="modal">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title">Buscar terceros</h5>
                            <button type="button" @click="isModalTercero=false;" class="btn btn-close"><div></div><div></div></button>
                        </div>
                        <div class="modal-body">
                            <div class="d-flex flex-column">
                                <div class="form-control m-0 mb-3">
                                    <input type="search" placeholder="Buscar" v-model="txtSearchTercero" @keyup="search('modal_tercero')" id="labelInputName">
                                </div>
                                <div class="form-control m-0 mb-3">
                                    <label class="form-label" for="labelInputName">Resultados: <span class="fw-bold">{{txtResultadosTerceros}}</span></label>
                                </div>
                            </div>
                            <div class="overflow-auto max-vh-50 overflow-x-hidden " >
                                <table class="table table-hover fw-normal">
                                    <thead>
                                        <tr>
                                            <th>Código</th>
                                            <th>Nombre</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr v-for="(data,index) in arrTercerosCopy" :key="index" @dblclick="selectItem(data,'tercero')">
                                            <td>{{data.codigo}}</td>
                                            <td>{{data.nombre}}</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div v-show="isModalCargo" class="modal">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title">Buscar cargos</h5>
                            <button type="button" @click="isModalCargo=false;" class="btn btn-close"><div></div><div></div></button>
                        </div>
                        <div class="modal-body">
                            <div class="d-flex flex-column">
                                <div class="form-control m-0 mb-3">
                                    <input type="search" placeholder="Buscar" v-model="txtSearchCargo" @keyup="search('modal_cargo')" id="labelInputName">
                                </div>
                                <div class="form-control m-0 mb-3">
                                    <label class="form-label" for="labelInputName">Resultados: <span class="fw-bold">{{txtResultadosCargos}}</span></label>
                                </div>
                            </div>
                            <div class="overflow-auto max-vh-50 overflow-x-hidden " >
                                <table class="table table-hover fw-normal">
                                    <thead class="text-center">
                                        <tr>
                                            <th>Código</th>
                                            <th>Nombre</th>
                                            <th>Valor</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr v-for="(data,index) in arrCargosCopy" :key="index" @dblclick="selectItem(data,'cargo')">
                                            <td>{{data.codigo}}</td>
                                            <td>{{data.nombre}}</td>
                                            <td class="text-right">{{formatNum(data.valor)}}</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </main>
		<script src="node_modules/read-excel-file/bundle/read-excel-file.min.js"></script>
		<script src="node_modules/xlsx/dist/xlsx.full.min.js"></script>
        <script src="Librerias/vue/vue.min.js"></script>
		<script src="Librerias/vue/axios.min.js"></script>
		<script type="module" src="gestion_humana/seguridad_social/js/functions_seguridad.js?<?= date('d_m_Y_h_i_s');?>"></script>
	</body>
</html>
