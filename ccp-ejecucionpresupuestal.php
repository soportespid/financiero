<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");

	require "comun.inc";
	require "funciones.inc";
	require "conversor.php";
	require "validaciones.inc";

	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");

	session_start();
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html lang="es">
	<head>
		<meta charset="utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1"/>
		<meta name="viewport" content="user-scalable=no">
		<title>:: IDEAL 10 - Presupuesto</title>
		<link href="favicon.ico" rel="shortcut icon"/>
		<link href="css/css2.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css">
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css">
		<script type="text/javascript" src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script type='text/javascript' src='JQuery/jquery-2.1.4.min.js?<?php echo date('d_m_Y_h_i_s');?>'></script>
        <link href="css/style.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
	</head>
	<body>
		<IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
		<span id="todastablas2"></span>
		<table>
			<tr><script>barra_imagenes("ccpet");</script><?php cuadro_titulos();?></tr>
			<tr><?php menu_desplegable("ccpet");?></tr>
		</table>
        <div class="bg-white group-btn p-1"><button type="button" onclick="mypop=window.open('ccp-principal.php','','');mypop.focus();" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
            <span>Nueva ventana</span>
            <svg class="fill-black group-hover:fill-white w-5 h-5" xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960"><path d="M320-120v-80h80v-80H160q-33 0-56.5-23.5T80-360v-400q0-33 23.5-56.5T160-840h640q33 0 56.5 23.5T880-760v400q0 33-23.5 56.5T800-280H560v80h80v80H320ZM160-360h640v-400H160v400Zm0 0v-400 400Z"></path></svg>
        </button></div>
		<form name="form2" method="post" action="" >
			<div style="margin: 0px 10px 10px; border-radius: 0 0 0 5px; height: 500px; overflow: scroll; overflow-x: hidden; resize: vertical">
				<table class="inicio">
					<tr>
						<td class="titulos" colspan="2">.:  Ejecuci&oacute;n Presupuestal </td>
						<td class="cerrar" style="width:7%;" ><a href="ccp-principal.php">&nbsp;Cerrar</a></td>
					</tr>
					<td style="background-repeat:no-repeat; background-position:center;">
						<ol id="lista2">
                            <li onClick="location.href='ccp-ejecuPresuIngresos.php'" style="cursor:pointer;">Ejecuci&oacute;n presupuestal de ingresos</li>
							<!-- <li onClick="location.href='ccp-ejecucionpresupuestalcuipo.php'" style="cursor:pointer;">Ejecuci&oacute;n presupuestal CCPET - Gastos</li> -->
                            <?php
                                $sqlr = "SELECT orden FROM configbasica";
                                $resr = mysqli_query($linkbd, $sqlr);
                                $rowr = mysqli_fetch_array($resr);
                                $orden = $rowr['orden'];
                                if ($orden == 'Dptal') {
                                    ?>
                                        <li onClick="location.href='ccp-ejecucionpresupuestal-vue-copy.php'" style="cursor:pointer;">Ejecuci&oacute;n presupuestal de gastos CCPET - General</li>
                                    <?php
                                }else{
                                    ?>
                                        <li onClick="location.href='ccp-ejecucionpresupuestal-vue.php'" style="cursor:pointer;">Ejecuci&oacute;n presupuestal de gastos CCPET - General</li>
                                    <?php
                                }
                            ?>
							<!-- <li onClick="location.href='ccp-ejecucionpresupuestal-vue.php'" style="cursor:pointer;">Ejecuci&oacute;n presupuestal de gastos CCPET - General</li> -->
							<li onClick="location.href='ccp-ejecucionpresupuestal-inv.php'" style="cursor:pointer;">Ejecuci&oacute;n presupuestal de gastos CCPET - Por Sectores - Inversión</li>
							<!-- <li onClick="location.href='ccp-auxiliarGastos.php'" style="cursor:pointer;">Ejecuci&oacute;n presupuestal CCPET - Auxiliar por Cuentas Gastos</li> -->
							<li onClick="location.href='ccp-ejecuPresuGastos.php'" style="cursor:pointer;">Ejecuci&oacute;n presupuestal de gastos CCPET - CPC - Funcionamiento, Servicio de la deuda</li>
                            <li onClick="location.href='ccp-ejecucionpresupuestal-nuevo'" style="cursor:pointer;">Ejecuci&oacute;n presupuestal de gastos CCPET - General - Por dependencias</li>
                            <li onClick="location.href='ccp-reporteFuentesRecursos'" style="cursor:pointer;">Reporte por fuente de recursos</li>
                            <li onClick="location.href='ccp-reportepac'" style="cursor:pointer;">Reporte PAC</li>
                            <li onClick="location.href='ccp-ejecuPresuIngresosGraf'" style="cursor:pointer;">
                                Graficos de la ejecución de ingresos &emsp;
                                <svg xmlns="http://www.w3.org/2000/svg" height="15px" viewBox="0 0 512 512" fill="#000"><path d="M64 64c0-17.7-14.3-32-32-32S0 46.3 0 64L0 400c0 44.2 35.8 80 80 80l400 0c17.7 0 32-14.3 32-32s-14.3-32-32-32L80 416c-8.8 0-16-7.2-16-16L64 64zm406.6 86.6c12.5-12.5 12.5-32.8 0-45.3s-32.8-12.5-45.3 0L320 210.7l-57.4-57.4c-12.5-12.5-32.8-12.5-45.3 0l-112 112c-12.5 12.5-12.5 32.8 0 45.3s32.8 12.5 45.3 0L240 221.3l57.4 57.4c12.5 12.5 32.8 12.5 45.3 0l128-128z"/></svg>
                            </li>
                            <li onClick="location.href='ccp-ejecucionpresupuestal-inv-graf'" style="cursor:pointer;">
                                Graficos de la ejecución de Gastos &emsp;
                                <svg xmlns="http://www.w3.org/2000/svg" height="15px" viewBox="0 0 512 512" fill="#000"><path d="M64 64c0-17.7-14.3-32-32-32S0 46.3 0 64L0 400c0 44.2 35.8 80 80 80l400 0c17.7 0 32-14.3 32-32s-14.3-32-32-32L80 416c-8.8 0-16-7.2-16-16L64 64zm406.6 86.6c12.5-12.5 12.5-32.8 0-45.3s-32.8-12.5-45.3 0L320 210.7l-57.4-57.4c-12.5-12.5-32.8-12.5-45.3 0l-112 112c-12.5 12.5-12.5 32.8 0 45.3s32.8 12.5 45.3 0L240 221.3l57.4 57.4c12.5 12.5 32.8 12.5 45.3 0l128-128z"/></svg>
                            </li>
							<!-- <li onClick="location.href='ccp-ejecucioningresos.php'" style="cursor:pointer;">Ejecuci&oacute;n presupuesto ingresos</li> -->
							<!-- <li onClick="location.href='ccp-ejecuciongastos.php'" style="cursor:pointer;">Ejecuci&oacute;n presupuesto gastos a nivel cuentas</li>
							<li onClick="location.href='ccp-estadoregistro.php'" style="cursor:pointer;">Estado de registros</li>
							<li onClick="location.href='ccp-reportesaldopresupuestal.php'" style="cursor:pointer;">Reportes saldo presupuestal</li> -->
                            <li onClick="location.href='ccp-auxiliarGastosCuenta'" style="cursor:pointer;">Reporte auxiliar por cuenta de gastos</li>
                            <li onClick="location.href='ccp-auxiliarIngresosCuenta'" style="cursor:pointer;">Reporte auxiliar por cuenta de ingresos</li>
							<li onClick="location.href='ccp-buscarContratos.php'" style="cursor:pointer;">Reportes Registros Presupuestales - Contratos</li>
							<li onClick="location.href='ccp-saldosrp.php'" style="cursor:pointer;">Reportes saldos Registros Presupuestales</li>
							<li onClick="location.href='ccp-saldoscuentasejecucion.php'" style="cursor:pointer;">Valor de rp's por rubros presupuestales</li>
                            <li onClick="location.href='ccp-homologarfuentesf05.php'" style="cursor:pointer;">Homologar fuentes F05</li>
							<li onClick="location.href='ccp-formatof05.php'" style="cursor:pointer;">Formato F05</li>
							<li onClick="location.href='ccp-formatof06c.php'" style="cursor:pointer;">Formato F06C - Modificaciones al presupuesto de ingresos</li>
							<li onClick="location.href='ccp-formatof07c.php'" style="cursor:pointer;">Formato F07C - Modificiones presupuesto de gastos.</li>
                            <li onClick="location.href='ccp-formatof15.php'" style="cursor:pointer;">Formato F15 - CONSTITUCION CUENTAS POR PAGAR.</li>
							<li onClick="location.href='ccp-formatof16.php'" style="cursor:pointer;">Formato F16 - CONSTITUCION DE RESERVAS PRESUPUESTALES.</li>
							<li onClick="location.href='ccp-formatof16a.php'" style="cursor:pointer;">Formato F16A - CANCELACION DE RESERVAS PRESUPUESTALES.</li>
						</ol>
					</td>
				</table>
			</div>
		</form>
	</body>
	<script>
	jQuery(function($)
	{
		var user ="<?php echo $_SESSION['cedulausu']; ?>";
		var bloque='';
		$.post('peticionesjquery/seleccionavigencia.php',{usuario: user},selectresponse);
		$('#cambioVigencia').change(function(event)
		{
			var valor= $('#cambioVigencia').val();
			var user ="<?php echo $_SESSION['cedulausu']; ?>";
			var confirma=confirm('�Realmente desea cambiar la vigencia?');
			if(confirma)
			{
				var anobloqueo=bloqueo.split("-");
				var ano=anobloqueo[0];
				if(valor < ano)
				{
					if(confirm("Tenga en cuenta va a entrar a un periodo bloqueado. Desea continuar"))
					{$.post('peticionesjquery/cambiovigencia.php',{valor: valor,usuario: user},updateresponse);}
					else {location.reload();}
				}
				else {$.post('peticionesjquery/cambiovigencia.php',{valor: valor,usuario: user},updateresponse);}
			}
			else {location.reload();}
		});
		function updateresponse(data)
		{
			json=eval(data);
			if(json[0].respuesta=='2'){alert("Vigencia modificada con exito");}
			else if(json[0].respuesta=='3'){alert("Error al modificar la vigencia");}
			location.reload();
		}
		function selectresponse(data)
		{
			json=eval(data);
			$('#cambioVigencia').val(json[0].vigencia);
			bloqueo=json[0].bloqueo;
		}
	});

	</script>
</html>

