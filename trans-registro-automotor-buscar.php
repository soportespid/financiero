<?php

    header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require 'comun.inc';
	require 'funciones.inc';
	session_start();
    if(empty($_SESSION)){
        header("location: index.php");
    }
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html lang=es>
	<head>
        <meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1"/>
		<meta name="viewport" content="user-scalable=no">
		<title>:: IDEAL 10 - Parametrización</title>
		<link href="favicon.ico" rel="shortcut icon"/>
		<link href="css/css2z.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
        <link href="css/style.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" href="sweetalert2/dist/sweetalert2.min.css">
        <script src="sweetalert2/dist/sweetalert2.min.js"></script>
		<script src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
	</head>
	<body>
		<header>
			<table>
				<tr>
                    <script>barra_imagenes("trans");</script><?php cuadro_titulos();?>
                </tr>
			</table>
		</header>

        <section id="myapp" v-cloak>
            <!-- variables -->
            <input type="hidden" value = "2" ref="pageType">

            <!-- loading -->
            <div class="loading-container" v-show="isLoading" >
                <p class="text-loading"data-loading="IDEAL 10">IDEAL 10</p>
            </div>

            <nav>
                <table>
                    <tr><?php menu_desplegable("trans");?></tr>
                </table>
                <div class="bg-white group-btn p-1" id="newNavStyle">
                    <button type="button" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center" @click="location.href='trans-registro-automotor-crear'">
                        <span>Nuevo</span>
                        <svg viewBox="0 -960 960 960" ><path d="M440-440H200v-80h240v-240h80v240h240v80H520v240h-80v-240Z"/></svg>
                    </button>
                    <button type="button" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center" @click="window.location.reload()">
                        <span>Buscar</span>
                        <svg class="fill-black group-hover:fill-white w-5 h-5" viewBox="0 -960 960 960" ><path d="M784-120 532-372q-30 24-69 38t-83 14q-109 0-184.5-75.5T120-580q0-109 75.5-184.5T380-840q109 0 184.5 75.5T640-580q0 44-14 83t-38 69l252 252-56 56ZM380-400q75 0 127.5-52.5T560-580q0-75-52.5-127.5T380-760q-75 0-127.5 52.5T200-580q0 75 52.5 127.5T380-400Z"/></svg>
                    </button>
                    <button type="button" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center" @click="mypop=window.open('trans-principal','',''); mypop.focus();">
                        <span>Nueva ventana</span>
                        <svg class="fill-black group-hover:fill-white w-5 h-5" viewBox="0 -960 960 960" ><path d="M320-120v-80h80v-80H160q-33 0-56.5-23.5T80-360v-400q0-33 23.5-56.5T160-840h640q33 0 56.5 23.5T880-760v400q0 33-23.5 56.5T800-280H560v80h80v80H320ZM160-360h640v-400H160v400Zm0 0v-400 400Z"/></svg>
                    </button>
                </div>
            </nav>
            <article class="bg-white">
                <div>
                    <div>
                        <h2 class="titulos m-0">Buscar - Registro automotor</h2>
                        
                        <div class="d-flex">
                            <div class="form-control">
                                <label class="form-label m-0" for="">Buscar</label>
                                <input type="text" v-model="txtSearch" placeholder="Buscar por placa o propietario">
                            </div>

                            <div class="form-control w-50">
                                <label class="form-label m-0" for="">Clase</label>
                                <select v-model="claseId">
                                    <option value="">Seleccione</option>
                                    <option v-for="clase in arrClases" :value="clase.clase_id">{{ clase.nombre }}</option>
                                </select>
                            </div>

                            <div class="form-control w-50">
                                <label class="form-label m-0" for="">Marca</label>
                                <select v-model="marcaId">
                                    <option value="">Seleccione</option>
                                    <option v-for="marca in arrMarcas" :value="marca.marca_id">{{ marca.nombre }}</option>
                                </select>
                            </div>

                            <div class="form-control w-50">
                                <label class="form-label m-0" for="">Linea</label>
                                <select v-model="lineaId">
                                    <option value="">Seleccione</option>
                                    <option v-for="linea in arrLineas" :value="linea.linea_id">{{ linea.nombre }}</option>
                                </select>
                            </div>

                            <div class="form-control w-50">
                                <label class="form-label m-0" for="">Modelo</label>
                                <select v-model="modelo">
                                    <option value="">Seleccione</option>
                                    <option v-for="modelo in arrModelos" :value="modelo.modelo">{{ modelo.modelo }}</option>
                                </select>
                            </div>

                            <div class="form-control w-25">
                                <label class="form-label m-0" for="">Por página:</label>
                                <select class="text-center" v-model="selectPorPagina">
                                    <option value="25">25</option>
                                    <option value="50">50</option>
                                    <option value="100">100</option>
                                    <option value="250">250</option>
                                    <option value="500">500</option>
                                    <option value="1000">1000</option>
                                    <option value="">Todo</option>
                                </select>
                            </div>

                            <div class="form-control justify-between text-center w-25">
                                <label class="form-label m-0" for=""></label>
                                
                                    <button type="button" @click="getDataSearch()" class="btn btn-primary">Buscar</button>
                                </div>
                            </div>
                        </div>
                    </div>                       
                </div>

                <div class="table-responsive overflow-auto" style="height:60vh">
                    <table class="table table-hover fw-normal">
                        <thead>
                            <tr class="text-center">
                                <th>Item</th>
                                <th>Placa</th>
                                <th>Dpto</th>
                                <th>Mnpio</th>
                                <th>Fecha tramite</th>
                                <th>Propietario</th>
                                <th>Clase</th>
                                <th>Marca</th>
                                <th>Linea</th>
                                <th>Modelo</th>
                                <th>Cilindraje</th>
                                <th>Carga</th>
                                <th>Pasajeros</th>
                                <th>Tipo público</th>
                                <th>Carrocería</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr v-for="(data,index) in arrDataSearch" :key="index" class="text-center" @dblclick="window.location.href='trans-registro-automotor-editar?id='+data.id">
                                <td>{{ data.id }}</td>
                                <td>{{ data.placa }}</td>
                                <td>{{ data.nombre_departamento }}</td>
                                <td>{{ data.nombre_municipio }}</td>
                                <td>{{ formatFecha(data.fecha_tramite) }}</td>
                                <td class="text-left">{{ data.propietario }} - {{ data.nombre }}</td>
                                <td>{{ data.nombre_clase }}</td>
                                <td>{{ data.nombre_marca }}</td>
                                <td>{{ data.nombre_linea }}</td>
                                <td>{{ data.modelo }}</td>
                                <td>{{ data.nombre_cilindraje }}</td>
                                <td>{{ data.capacidad_carga }}</td>
                                <td>{{ data.cantidad_pasajeros }}</td>
                                <td>{{ data.nombre_publico }}</td>
                                <td>{{ data.nombre_carroceria }}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>

                <div v-if="arrDataSearch.length > 0" class="list-pagination-container">
                    <p>Página {{ paginaActual }} de {{ intTotalPaginas }}</p>
                    <ul class="list-pagination">
                        <li v-show="paginaActual > 1" @click="getDataSearch(paginaActual = 1)"><< </li>
                        <li v-show="paginaActual > 1" @click="getDataSearch(--paginaActual)"><</li>
                        <li v-for="(pagina,index) in arrBotones" :class="paginaActual == pagina ? 'active' : ''" @click="getDataSearch(pagina)" :key="index">{{pagina}}</li>
                        <li v-show="paginaActual < intTotalPaginas" @click="getDataSearch(++paginaActual)">></li>
                        <li v-show="paginaActual < intTotalPaginas" @click="getDataSearch(paginaActual = intTotalPaginas)" >>></li>
                    </ul>
                </div>

            </article>
        </section>

        <script src="Librerias/vue/vue.min.js"></script>
		<script src="Librerias/vue/axios.min.js"></script>
		<script type="module" src="transporte/js/functions_registro_automotor.js"></script>

	</body>
</html>
