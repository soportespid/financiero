<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=iso-8859-1");
	require "comun.inc";
	require "funciones.inc";
	session_start();
	$linkbd = conectar_v7();
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es">
	<head>
		<meta http-equiv="Content-type" content="text/html; charset=iso-8859-1"/>
		<meta http-equiv="X-UA-Compatible" content="IE=9"/>
		<title>:: IDEAL 10 - Servicios P&uacute;blicos</title>
		<link href="css/css2.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/cssSP.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<script type="text/javascript" src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
        <script type="text/javascript" src="css/calendario.js?<?php echo date('d_m_Y_h_i_s');?>"></script>

		<style></style>

		<script>
            function despliegamodal2(_valor, _table)
			{
                document.getElementById('bgventanamodal2').style.visibility = _valor;

				if(_table == 'srvmedidores')
				{
					document.getElementById('ventana2').src = 'medidores-ventana.php?table=' + _table;
				}
                else if(_table == 'srvclientes')
                {
                    document.getElementById('ventana2').src = 'ventana-clienteservicio.php?table=' + _table;
                }
            }

			function despliegamodalm(_valor,_tip,mensa,pregunta)
			{
				document.getElementById("bgventanamodalm").style.visibility=_valor;

				if(_valor=="hidden")
				{
					document.getElementById('ventanam').src="";
				}
				else
				{
					switch(_tip)
					{
						case "1":	document.getElementById('ventanam').src="ventana-mensaje1.php?titulos="+mensa;break;
						case "2":	document.getElementById('ventanam').src="ventana-mensaje3.php?titulos="+mensa;break;
						case "3":	document.getElementById('ventanam').src="ventana-mensaje2.php?titulos="+mensa;break;
						case "4":	document.getElementById('ventanam').src="ventana-consulta1.php?titulos="+mensa+"&idresp="+pregunta;break;	
					}
				}
			}

			function respuestaModalBusqueda2(tabla, id, nombre, serial, referencia)
			{
				switch(tabla)
				{
					case 'srvclientes': 
						document.getElementById('cliente').value = id;
						document.getElementById('ntercero').value = nombre;
						document.form2.submit();
					break;	

					case 'srvmedidores': 
						document.getElementById('id_medidor').value = id;
						document.getElementById('serial').value = serial;
						document.getElementById('referencia').value = referencia;
						document.form2.submit();
					break;	
                }
            }

			function cambiocheck()
			{
				if(document.getElementById('myonoffswitch').value=='S')
				{
					document.getElementById('myonoffswitch').value='N';
				}
				else
				{
					document.getElementById('myonoffswitch').value='S';
				}

				document.form2.submit();
			}

			function funcionmensaje()
			{
				var idban=document.getElementById('codban').value;

				document.location.href = "serv-asignacion.php";
			}

			function respuestaconsulta(pregunta)
			{
				switch(pregunta)
				{
					case "1":	
						document.form2.oculto.value='2';
						document.form2.submit();
					break;
				}
			}

			function guardar()
			{
				var v1=document.getElementById('codban').value;
				var v4=document.getElementById('servicio').value;
				var v5=document.getElementById('cliente').value;

				if (v1 != "" && v4 != "-1" && v5 > 0) {
                    despliegamodalm('visible','4','Esta Seguro de Guardar','1');
                }
				else {
                    despliegamodalm('visible','2','Falta información para crear la Asignación');
                }
			}

			function actualizar()
			{
				document.form2.submit();
			}
		</script>
		<?php titlepag();?>
	</head>
	<body>
		<IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
		<span id="todastablas2"></span>
		<table>
			<tr><script>barra_imagenes("serv");</script><?php cuadro_titulos();?></tr>
			<tr><?php menu_desplegable("serv");?></tr>
			<tr>
				<td colspan="3" class="cinta">
					<a href="serv-asignacion.php" class="mgbt"><img src="imagenes/add.png"/></a>

					<a onclick="guardar()" class="mgbt"><img src="imagenes/guarda.png" title="Guardar"/></a>

					<a href="serv-buscaasignacion.php" class="mgbt"><img src="imagenes/busca.png" title="Buscar"/></a>

					<a onclick="mypop=window.open('plan-agenda.php','','');mypop.focus()" class="mgbt"><img src="imagenes/agenda1.png" title="Agenda" /></a>

					<a onclick="mypop=window.open('serv-principal.php','','');mypop.focus();" class="mgbt"><img src="imagenes/nv.png" title="Nueva ventana"></a>

					<a onclick="mypop=window.open('<?php echo $url2; ?>','','');mypop.focus();" class="mgbt"><img src="imagenes/duplicar_pantalla.png" title="Duplicar pesta&ntilde;a"></a>

					<a href="serv-menuAsignaciones.php" class="mgbt"><img src="imagenes/iratras.png" title="Atr&aacute;s"></a>
                </td>    
			</tr>
		</table>
		<div id="bgventanamodalm" class="bgventanamodalm">
			<div id="ventanamodalm" class="ventanamodalm">
				<IFRAME src="" name="ventanam" marginWidth=0 marginHeight=0 frameBorder=0 id="ventanam" frameSpacing=0 style=" width:700px; height:130px; top:200; overflow:hidden;"></IFRAME>
			</div>
		</div>
		<form name="form2" method="post" action="">
			<?php 
				if(@ $_POST['oculto']=="")
				{
					$_POST['onoffswitch'] = "N";
					$_POST['codban']=selconsecutivo('srvasignacion_servicio','id');
				}
			?>
			<table class="inicio ancho">
				<tr>
					<td class="titulos" colspan="8">.: Ingresar Asignaci&oacute;n</td>
					<td class="cerrar" style="width:7%" onClick="location.href='serv-principal.php'">Cerrar</td>
				</tr>
				<tr>
					<td class="tamano01" style="width: 2px;">C&oacute;digo:</td>
					<td style="width:15%;">
                        <input type="text" name="codban" id="codban" onKeyUp="return tabular(event,this)" value="<?php echo @ $_POST['codban'];?>" style="width:100%;height:30px;text-align:center;" readonly/>
                    </td>
                    <td class="saludo1">Cliente:</td>
					<td>
						<input type="text" name='cliente' id='cliente'  value="<?php echo @$_POST['cliente']?>" style="width: 100%; height: 30px;" onclick = "despliegamodal2('visible', 'srvclientes');"  class="colordobleclik" readonly>
                    </td>
                    <td colspan="2">
						<input type="text" name="ntercero" id="ntercero" value="<?php echo @$_POST['ntercero']?>" style="width:100%;height:30px;" readonly>
					</td>
				</tr>
				<tr>
                    <td class="tamano01">Servicio:</td>
					<td>
						<select name="servicio" id="servicio" style="width:100%;">
							<option value="-1">:: Seleccione Servicio ::</option>
							<option value="0">TODOS LOS SERVICIOS</option>
							<?php
								$sqlr="SELECT id,nombre FROM srvservicios";
								$resp = mysqli_query($linkbd,$sqlr);
								while ($row = mysqli_fetch_row($resp))
								{
									$sqlAsignacion = "SELECT id_servicio FROM srvasignacion_servicio WHERE id_clientes = '$_POST[cliente]' AND id_servicio = $row[0] ";
									$resAsignacion = mysqli_query($linkbd,$sqlAsignacion);
									$rowAsignacion = mysqli_fetch_row($resAsignacion);

									if($row[0] != $rowAsignacion[0])
									{
										if(@ $_POST['servicio']==$row[0])
										{
											echo "<option value='$row[0]' SELECTED>$row[0] - $row[1]</option>";
										}
										else{echo "<option value='$row[0]'>$row[0] - $row[1]</option>";}
									}
								}
							?>
						</select>
					</td>

					<td class="tamano01" style="width:3cm;">Fecha Activaci&oacute;n:</td>
					<td style="width:15%;">
                        <input type="text" name="fecha" value="<?php echo @ $_POST['fecha']?>" maxlength="10" onKeyPress="javascript:return solonumeros(event)" onKeyUp="return tabular(event,this)" id="fc_1198971545" onKeyDown="mascara(this,'/',patron,true)" title="DD/MM/YYYY" style="width:80%;">&nbsp;
                        <img src="imagenes/calendario04.png" onClick="displayCalendarFor('fc_1198971545');" title="Calendario" class="icobut"/>
                    </td>
				</tr>

				<tr>
					<td class="tamano01">Servicio con medidor:</td>
					<td>
						<div class="onoffswitch">
							<input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox" id="myonoffswitch" value="<?php echo @ $_POST['onoffswitch'];?>" <?php if(@ $_POST['onoffswitch']=='S'){echo "checked";}?> onChange="cambiocheck();"/>
							<label class="onoffswitch-label" for="myonoffswitch">
								<span class="onoffswitch-inner"></span>
								<span class="onoffswitch-switch"></span>
							</label>
						</div>
					</td>

                    <?php
						if($_POST['onoffswitch'] == 'S')
						{
					?>
							<td class="saludo1">Medidor:</td>
							<td>
								<input type="text" name='serial' id='serial'  value="<?php echo @$_POST['serial']?>" style="width: 100%; height: 30px;" onclick = "despliegamodal2('visible', 'srvmedidores');"  class="colordobleclik" readonly>
								<input type="hidden" name='id_medidor' id='id_medidor' value="<?php echo $_POST['id_medidor']?>">
							</td>
					<?php
						}
                    ?>
				</tr>
			</table>
			<input type="hidden" name="oculto" id="oculto" value="1"/>
			<?php 
				if(@ $_POST['oculto']=="2")
				{
					if (@$_POST['onoffswitch']!='S')
					{
						$valest='N';
					}
					else 
					{
						$valest='S';
					} 

					$servicio = $_POST['servicio'];

                    preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST['fecha'],$fecha);
					$fechaActivacion="$fecha[3]-$fecha[2]-$fecha[1]";

					$_POST['codban']=selconsecutivo('srvasignacion_servicio','id');

					if ($valest == 'S') 
					{
						$sqlr = " UPDATE srvmedidores SET asignacion='S' WHERE id='".$_POST['id_medidor']."' ";
						mysqli_query($linkbd,$sqlr);
					}

					if($servicio == '0')
					{
						$sqlValidacion = "SELECT COUNT('id_servicio') FROM srvasignacion_servicio WHERE id_clientes = '$_POST[cliente]' ";
						$resValidacion = mysqli_query($linkbd,$sqlValidacion);
						$rowValidacion = mysqli_fetch_row($resValidacion);

						if($rowValidacion[0] == 0)
						{
							$sqlServicios = "SELECT id FROM srvservicios";
							$resServicios = mysqli_query($linkbd,$sqlServicios);
							
							while($rowServicios = mysqli_fetch_row($resServicios))
							{
								$sqlr="INSERT INTO srvasignacion_servicio (id_clientes, id_medidor, id_servicio, fecha_activacion,tarifa_medidor,estado) VALUES ('".$_POST['cliente']."', '".@$_POST['id_medidor']."','".$rowServicios[0]."','$fechaActivacion','$valest','S')";
								
								if (!mysqli_query($linkbd,$sqlr))
								{
									echo"
									<script>
										despliegamodalm('visible','2','No se pudo ejecutar la petici&oacute;n');
									</script>";
								}
								else 
								{
									echo "<script>despliegamodalm('visible','1','Se ha almacenado con Exito');</script>";
								}
							}
						}
						else
						{
							echo"
							<script>
								despliegamodalm('visible','2','No se pudo ejecutar la peticion porque ya tiene un servicio asignado');
							</script>";
						}
					}
					else
					{
						$sqlr="INSERT INTO srvasignacion_servicio (id, id_clientes, id_medidor, id_servicio, fecha_activacion,tarifa_medidor,estado) VALUES ('".$_POST['codban']."','".$_POST['cliente']."', '".@$_POST['id_medidor']."','".$_POST['servicio']."','$fechaActivacion','$valest','S')";
						if (!mysqli_query($linkbd,$sqlr))
						{
							echo"
							<script>
								despliegamodalm('visible','2','No se pudo ejecutar la petici&oacute;n');
							</script>";
						}
						else 
						{
							echo "<script>despliegamodalm('visible','1','Se ha almacenado con Exito');</script>";
						}
					}			
				}
			?>
		</form>
		<div id="bgventanamodal2">
			<div id="ventanamodal2">
				<IFRAME  src="" name="buster" marginWidth=0 marginHeight=0 frameBorder=0 id="ventana2" frameSpacing=0 style="left:500px; width:900px; height:500px; top:200;"></IFRAME>
			</div>
		</div>
	</body>
</html>
