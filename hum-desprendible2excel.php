<?php  
	require_once 'PHPExcel/Classes/PHPExcel.php';
	require 'comun.inc';
	require 'funciones.inc';
	session_start();
	$objPHPExcel = new PHPExcel();
	$objPHPExcel->getProperties()
		->setCreator("SPID")
		->setLastModifiedBy("SPID")
		->setTitle("Reporte General de Descuentos")
		->setSubject("Nomina")
		->setDescription("Nomina")
		->setKeywords("Nomina")
		->setCategory("Gestion Humana");
	$objPHPExcel->getActiveSheet()->mergeCells('A1:Y1');
	$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A1', 'REPORTE');
	$objFont=$objPHPExcel->getActiveSheet()->getStyle('A1')->getFont();
	$objFont->setName('Courier New');
	$objFont->setSize(15); 
	$objFont->setBold(true);
	$objFont->setUnderline(PHPExcel_Style_Font::UNDERLINE_SINGLE);
	$objFont->getColor()->setARGB( PHPExcel_Style_Color::COLOR_BLACK);
	$objAlign=$objPHPExcel->getActiveSheet()->getStyle('A1')->getAlignment(); 
	$objAlign->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); 
	$objAlign->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER); 
	$objPHPExcel-> getActiveSheet ()
		-> getStyle ("A2:Y2")
		-> getFill ()
		-> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
		-> getStartColor ()
		-> setRGB ('A6E5F3');
	$objPHPExcel-> getActiveSheet ()
		-> getStyle ("A1")
		-> getFill ()
		-> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
		-> getStartColor ()
		-> setRGB ('A6E5F3');
	$borders = array(
		'borders' => array(
			'allborders' => array(
				'style' => PHPExcel_Style_Border::BORDER_THIN,
				'color' => array('argb' => 'FF000000'),
			)
		),
	);
	$objPHPExcel->getActiveSheet()->getStyle('A2:Y2')->applyFromArray($borders);
	$objPHPExcel->setActiveSheetIndex(0)
		->setCellValue('A2', 'UNIDAD EJECUTORA')
		->setCellValue('B2', 'CARGO')
		->setCellValue('C2', 'DOCUMENTO')
		->setCellValue('D2', 'FUNCIONARIO')
		->setCellValue('E2', 'ASIGNACIÓN BÁSICA ANUAL')
		->setCellValue('F2', 'GASTOS DE REPRESENTACIÓN ANUAL')
		->setCellValue('G2', 'PRIMA TÉCNICA ANUAL')
		->setCellValue('H2', 'PRIMA DE GESTIÓN ANUAL')
		->setCellValue('I2', 'PRIMA DE LOCALIZACIÓN ANUAL')
        ->setCellValue('J2', 'PRIMA DE COORDINACIÓN ANUAL')
		->setCellValue('K2', 'PRIMA DE RIESGO ANUAL')
        ->setCellValue('L2', 'PRIMA EXTRAORDINARIA ANUAL')
        ->setCellValue('M2', 'PRIMA O SUBSID ALIMENTAC ANUAL')
        ->setCellValue('N2', 'AUXILIO DE TRANSPORTE ANUAL')
        ->setCellValue('O2', 'PRIMA DE ANTIGÜEDAD ANUAL')
        ->setCellValue('P2', 'BONIFICACIÓN DIRECCIÓN ANUAL')
        ->setCellValue('Q2', 'PRIMA DE SERVICIOS ANUAL')
        ->setCellValue('R2', 'PRIMA DE NAVIDAD ANUAL')
        ->setCellValue('S2', 'BONIFIC POR SERVICIOS ANUAL')
        ->setCellValue('T2', 'BONIFIC DE RECREACIÓN ANUAL')
        ->setCellValue('U2', 'BONIFIC GESTIÓN TERRITORIA ANUAL')
        ->setCellValue('V2', 'PRIMA DE VACACIONES ANUAL')
        ->setCellValue('W2', 'OTRAS PRIMAS ANUAL')
        ->setCellValue('X2', 'CESANTÍAS ANUAL')
        ->setCellValue('Y2', 'INTERESES SOBRE CESANTÍAS ANUAL');
    
    $totallista = count($_POST['nom01']);
    $i=3;
    for($x=0; $x < $totallista;$x++){
		$objPHPExcel->setActiveSheetIndex(0)
		->setCellValueExplicit ("A$i", $_POST['nom01'][$x], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
		->setCellValueExplicit ("B$i", $_POST['nom02'][$x], PHPExcel_Cell_DataType :: TYPE_STRING)
		->setCellValueExplicit ("C$i", $_POST['nom03'][$x], PHPExcel_Cell_DataType :: TYPE_STRING)
		->setCellValueExplicit ("D$i", $_POST['nom04'][$x], PHPExcel_Cell_DataType :: TYPE_STRING)
		->setCellValueExplicit ("E$i", $_POST['nom05'][$x], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
		->setCellValueExplicit ("F$i", $_POST['nom06'][$x], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
		->setCellValueExplicit ("G$i", 0, PHPExcel_Cell_DataType :: TYPE_NUMERIC)
		->setCellValueExplicit ("H$i", 0, PHPExcel_Cell_DataType :: TYPE_NUMERIC)
		->setCellValueExplicit ("I$i", 0, PHPExcel_Cell_DataType :: TYPE_NUMERIC)
		->setCellValueExplicit ("J$i", 0, PHPExcel_Cell_DataType :: TYPE_NUMERIC)
        ->setCellValueExplicit ("K$i", 0, PHPExcel_Cell_DataType :: TYPE_NUMERIC)
		->setCellValueExplicit ("L$i", 0, PHPExcel_Cell_DataType :: TYPE_NUMERIC)
		->setCellValueExplicit ("M$i", $_POST['nom07'][$x], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
		->setCellValueExplicit ("N$i", $_POST['nom08'][$x], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
		->setCellValueExplicit ("O$i", 0, PHPExcel_Cell_DataType :: TYPE_NUMERIC)
		->setCellValueExplicit ("P$i", 0, PHPExcel_Cell_DataType :: TYPE_NUMERIC)
		->setCellValueExplicit ("Q$i", $_POST['nom09'][$x], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
		->setCellValueExplicit ("R$i", $_POST['nom10'][$x], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
		->setCellValueExplicit ("S$i", $_POST['nom11'][$x], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
		->setCellValueExplicit ("T$i", $_POST['nom12'][$x], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
        ->setCellValueExplicit ("U$i", $_POST['nom13'][$x], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
		->setCellValueExplicit ("V$i", $_POST['nom14'][$x], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
		->setCellValueExplicit ("W$i", 0, PHPExcel_Cell_DataType :: TYPE_NUMERIC)
		->setCellValueExplicit ("X$i", $_POST['nom15'][$x], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
        ->setCellValueExplicit ("Y$i", 0, PHPExcel_Cell_DataType :: TYPE_NUMERIC);
		$objPHPExcel->getActiveSheet()->getStyle("A$i:Y$i")->applyFromArray($borders);
		$i++;
	}
	//----Propiedades de la hoja 1
	$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('K')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('N')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('O')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('P')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('Q')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('R')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('S')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('T')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('U')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('V')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('W')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('X')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('Y')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->setTitle('REPORTE');
	$objPHPExcel->setActiveSheetIndex(0);

//----Guardar documento----
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename="REPORTE GENERAL DESCUENTOS.xls"');
header('Cache-Control: max-age=0');

$objWriter=PHPExcel_IOFactory::createWriter($objPHPExcel,'Excel5');
$objWriter->save('php://output');
exit;

?>