<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=iso-8859-1");
	require 'comun.inc';
	require 'funciones.inc';
	require 'validaciones.inc';
	require 'funcionesnomima.inc';
	session_start();
	$linkbd=conectar_v7();
	$linkbd -> set_charset("utf8");
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es">
	<head>
		<meta http-equiv="Content-type" content="text/html; charset=iso-8859-1"/>
		<meta http-equiv="X-UA-Compatible" content="IE=9"/>
		<title>:: Spid - Tesoreria</title>
		<link href="css/css2.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<style>
			.example1
			{
				font-size: 3em;
				color: limegreen;
				position: absolute;
				width: 100%;
				height: 100%;
				margin: 0;
				line-height: 50px;
				text-align: center;
				transform:translateX(100%);/* Starting position */
				animation: example1 15s linear infinite;/* Apply animation to this element */
			}
			@keyframes example1 /* Move it (define the animation) */
			{
				0% {transform: translateX(100%);}
				100% {transform: translateX(-100%);}
			}
		</style>
		<script type="text/javascript" src="css/programas.js"></script>
		<script>
			function procesos(){document.form2.oculto.value='2';document.form2.submit();}
		</script>
		<?php titlepag();?>
	</head>
	<body>
		<IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
		<span id="todastablas2"></span>
		<table>
			<tr><script>barra_imagenes("hum");</script><?php cuadro_titulos();?></tr>
			<tr><?php menu_desplegable("hum");?></tr>
		</table>
		<form name="form2" method="post" action="">
			<table class="inicio">
				<tr>
					<td class="titulos" colspan="7">ARREGLO CACHARROS VARIOS</td>
					<td class="cerrar" style="width:7%" onClick="location.href='teso-principal.php'">Cerrar</td>
				</tr>
				<tr>
					<td class="saludo1" style="width:2.5cm;">.: Inicio:</td>
					<td style="width:10%;"><input type="text" name="varini" id="varini" value="<?php echo @$_POST['varini']?>" style="width:100%"/></td>
					<td class="saludo1" style="width:2.5cm;">.: Fin:</td>
					<td style="width:10%;"><input type="text" name="varfin" id="varfin" value="<?php echo @$_POST['varfin']?>" style="width:100%"/></td>
					<td></td>
				</tr>
				<tr>
					<td class="saludo1">.: Proceso:</td>
					<td colspan="4" style='height:28px;'>
						<select name="selecob" id="selecob" style='text-transform:uppercase; width:100%; height:22px;'>
							<option value="">....</option>
							<option value='1' <?php if(@$_POST['selecob']=='1'){echo "SELECTED";}?>>1: Recalcular Salud y Pension (ingresar No Nomina)</option>
							<option value='2' <?php if(@$_POST['selecob']=='2'){echo "SELECTED";}?>>2: Recalcular Parafiscales (ingresar No Nomina)</option>
							<option value='3' <?php if(@$_POST['selecob']=='3'){echo "SELECTED";}?>>3: Recalcular Cuentas Nomina (ingresar No Nomina)</option>
							<option value='4' <?php if(@$_POST['selecob']=='4'){echo "SELECTED";}?>>4: Recalcular Presupuesto (ingresar No Nomina)</option>
							<option value='5' <?php if(@$_POST['selecob']=='5'){echo "SELECTED";}?>>5: Recalcular Descuentos Nomina (ingresar No Nomina)</option>
							<option value='7' <?php if(@$_POST['selecob']=='7'){echo "SELECTED";}?>>7: Agregar cod usuario y tipo a descuentos nomina (ingresar No Nomina inicio y fin)</option>
							<option value='8' <?php if(@$_POST['selecob']=='8'){echo "SELECTED";}?>>8: Agregar Fuente a egresos de nomina (ingresar egreso ini y egreso fin)</option>
							<option value='9' <?php if(@$_POST['selecob']=='9'){echo "SELECTED";}?>>9: Agregar indicador producto en presupuesto nomina (ingresar SI en ini)</option>
							<option value='10' <?php if(@$_POST['selecob']=='10'){echo "SELECTED";}?>>10: Borrar Politicas Publicas en nomina</option>
							<option value='11' <?php if(@$_POST['selecob']=='11'){echo "SELECTED";}?>>11: Recalcular CDP y RP</option>
							<option value='12' <?php if(@$_POST['selecob']=='12'){echo "SELECTED";}?>>12: Ingresar CC en  descuentos y retenciones</option>
						</select>
					<td style="padding-bottom:5px" colspan="2"><em class="botonflecha" onClick="procesos();">Correr</em></td>
					<td></td>
				</tr>
			</table>
			<input type="hidden" name="oculto" id="oculto" value="1"/>
			<div class="subpantalla" style="height:68.5%; width:99.6%;overflow-x:hidden" id="divdet">
			<?php
				if($_POST['oculto'] == 2)
				{
					switch ($_POST['selecob'])
					{						
						case '1':{ //Recalcular salud y pension nomina
							if($_POST['varini'] != ''){
								$sqlrtp = "SELECT * FROM humparametrosliquida";
								$resptp = mysqli_query($linkbd,$sqlrtp);
								$rowtp = mysqli_fetch_row($resptp);
								$tsaludemr = $rowtp[23];
								$tsaludemp = $rowtp[24];
								$tpensionemr = $rowtp[25];
								$tpensionemp = $rowtp[26];
								if($_POST['varfin'] == 'SI'){
									$sqld = "DELETE FROM humnomina_saludpension WHERE id_nom = '".$_POST['varini']."'";
										mysqli_query($linkbd,$sqld);
								}
								$sqlrg = "SELECT * FROM humnomina WHERE id_nom = '".$_POST['varini']."'";
								$respg = mysqli_query($linkbd,$sqlrg);
								$rowg = mysqli_fetch_row($respg);
								$fecha = $rowg[1];
								$vigencia = $rowg[7];
								$cont = 0;
								$sqlrt = "SELECT * FROM humnomina_det WHERE id_nom='".$_POST['varini']."'";
								$respt = mysqli_query($linkbd,$sqlrt);
								$sumasaludfu = $sumasaludem = $sumapensionfu = $sumapensionem = $sumafondosol = 0;
								while ($rowt = mysqli_fetch_row($respt)){
									$cont++;
									$sumasaludfu += $rowt[10];
									$sumasaludem += $rowt[11];
									$sumapensionfu += $rowt[12];
									$sumapensionem += $rowt[13];
									$sumafondosol += $rowt[14];
									if($_POST['varfin'] == 'SI'){
										$tipopresupuesto = buscartipovinculacion($rowt[0], $rowt[36], $rowt[38]);
										$cuantapresu = $porcentaje = $secpresu =  $programaticopara = $proyectopara = $bpimpara = '';
										$secpresu = itemfuncionarios($rowt[38],'42');
										if($tipopresupuesto[1] == 'IN' || $tipopresupuesto[1] == 'IT'){
											$fuentepara = itemfuncionarios($rowt[38],'44');
											$programaticopara = itemfuncionarios($rowt[38],'43');
											$proyectopara = itemfuncionarios($rowt[38],'31');
											$bpimpara = nombrebpim($proyectopara);
										}
										else{
											$bpimpara = '';
											$programaticopara ='';
											$proyectopara = '';
										}
										//********SALUD EMPLEADO *****
										if($rowt[10] > 0){
											$idsalud = selconsecutivo('humnomina_saludpension','id');
											$cuantapresu = buscarcuentanuevocatalogo1($rowt[36],$tipopresupuesto[1]);
											$doceps = itemfuncionarios($rowt[38],'14');
											if($tipopresupuesto[1] != 'IN' && $tipopresupuesto[1] != 'IT')
											{
												$fuentepara = '';
												$sql = "SELECT fuente FROM ccpetinicialgastosfun WHERE cuenta = '$cuantapresu' AND seccion_presupuestal = '$secpresu' AND vigencia = '$vigencia'";
												$res = mysqli_query($linkbd,$sql);
												$row = mysqli_fetch_row($res);
												$fuentepara = $row[0];
											}
											$sqlrins="INSERT INTO humnomina_saludpension (id_nom, tipo, empleado, tercero, cc, valor, estado, sector, id, cod_fun, cuenta_presu, tipopago, fuente, indicador_producto, bpin, productoservicio, medio_pago, codigo_politicap, tipo_mov, proyecto, seccion_presupuestal) VALUES ('".$_POST['varini']."', 'SE', '$rowt[1]', '$doceps', '$rowt[34]', '$rowt[10]', 'S', '', '$idsalud', '$rowt[38]', '$cuantapresu', '$rowt[36]', '$fuentepara', '$programaticopara', '$bpimpara', '', 'CSF', '1', '201', '$proyectopara', '$secpresu')";
											mysqli_query($linkbd,$sqlrins);
										}
										//********PENSION EMPLEADO *****
										if($rowt[12]>0){
											$idsalud=selconsecutivo('humnomina_saludpension','id');
											$cuantapresu = buscarcuentanuevocatalogo1($rowt[36],$tipopresupuesto[1]);
											$docfp = itemfuncionarios($rowt[38],'18');
											if($tipopresupuesto[1] != 'IN' && $tipopresupuesto[1] != 'IT'){
												$fuentepara = '';
												$sql = "SELECT fuente FROM ccpetinicialgastosfun WHERE cuenta = '$cuantapresu' AND seccion_presupuestal = '$secpresu' AND vigencia = '$vigencia'";
												$res = mysqli_query($linkbd,$sql);
												$row = mysqli_fetch_row($res);
												$fuentepara = $row[0];
											}
											$sqlrins="INSERT INTO humnomina_saludpension (id_nom, tipo, empleado, tercero, cc, valor, estado, sector, id, cod_fun, cuenta_presu, tipopago, fuente, indicador_producto, bpin, productoservicio, medio_pago, codigo_politicap, tipo_mov, proyecto, seccion_presupuestal) VALUES ('".$_POST['varini']."', 'PE', '$rowt[1]', '$docfp', '$rowt[34]', '$rowt[12]', 'S', 'PR', '$idsalud', '$rowt[38]', '$cuantapresu', '$rowt[36]', '$fuentepara', '$programaticopara', '$bpimpara', '', 'CSF', '1', '201', '$proyectopara', '$secpresu')";
											mysqli_query($linkbd,$sqlrins);
										}
										//********FONDO SOLIDARIDAD EMPLEADO *****
										if($rowt[14]>0){
											$idsalud = selconsecutivo('humnomina_saludpension','id');
											$cuantapresu = buscarcuentanuevocatalogo1($rowt[36],$tipopresupuesto[1]);
											$docfp = itemfuncionarios($rowt[38],'18');
											if($tipopresupuesto[1] != 'IN' && $tipopresupuesto[1] != 'IT'){
												$fuentepara = '';
												$sql = "SELECT fuente FROM ccpetinicialgastosfun WHERE cuenta = '$cuantapresu' AND seccion_presupuestal = '$secpresu' AND vigencia = '$vigencia'";
												$res = mysqli_query($linkbd,$sql);
												$row = mysqli_fetch_row($res);
												$fuentepara = $row[0];
											}
											$sqlrins ="INSERT INTO  humnomina_saludpension (id_nom,tipo,empleado,tercero,cc,valor,estado,sector,id,cod_fun,cuenta_presu, tipopago, fuente, indicador_producto, bpin, productoservicio, medio_pago, codigo_politicap, tipo_mov, proyecto, seccion_presupuestal) VALUES ('".$_POST['varini']."','FS','$rowt[1]','$docfp','$rowt[34]','$rowt[14]','S','PR','$idsalud','$rowt[38]', '$cuantapresu', '$rowt[36]', '$fuentepara', '$programaticopara', '$bpimpara', '', 'CSF', '1', '201', '$proyectopara', '$secpresu')";
											mysqli_query($linkbd,$sqlrins);
										}
										//******** SALUD EMPLEADOR *******
										if($rowt[11]>0){
											$idsalud = selconsecutivo('humnomina_saludpension','id');
											$cuantapresu = buscarcuentanuevocatalogo2($tsaludemr,$tipopresupuesto[1],"N/A");
											$doceps = itemfuncionarios($rowt[38],'14');
											if($tipopresupuesto[1] != 'IN' && $tipopresupuesto[1] != 'IT'){
												$fuentepara = '';
												$sql = "SELECT fuente FROM ccpetinicialgastosfun WHERE cuenta = '$cuantapresu' AND seccion_presupuestal = '$secpresu' AND vigencia = '$vigencia'";
												$res = mysqli_query($linkbd,$sql);
												$row = mysqli_fetch_row($res);
												$fuentepara = $row[0];
											}
											$sqlrins="INSERT INTO  humnomina_saludpension (id_nom, tipo, empleado, tercero, cc, valor, estado, sector, id, cod_fun, cuenta_presu, tipopago, fuente, indicador_producto, bpin, productoservicio, medio_pago, codigo_politicap, tipo_mov, proyecto, seccion_presupuestal) VALUES ('".$_POST['varini']."', 'SR','$rowt[1]', '$doceps', '$rowt[34]', '$rowt[11]', 'S', '', '$idsalud', '$rowt[38]', '$cuantapresu', '$rowt[36]', '$fuentepara', '$programaticopara', '$bpimpara', '', 'CSF', '1', '201', '$proyectopara', '$secpresu')";
											mysqli_query($linkbd,$sqlrins);
										}
										//******** PENSIONES EMPLEADOR *******
										if($rowt[13]>0){
											$idsalud=selconsecutivo('humnomina_saludpension','id');
											$cuantapresu = buscarcuentanuevocatalogo2($tpensionemr,$tipopresupuesto[1],"PR");
											$docfp = itemfuncionarios($rowt[38],'18');
											if($tipopresupuesto[1] != 'IN' && $tipopresupuesto[1] != 'IT'){
												$fuentepara = '';
												$sql = "SELECT fuente FROM ccpetinicialgastosfun WHERE cuenta = '$cuantapresu' AND seccion_presupuestal = '$secpresu' AND vigencia = '$vigencia'";
												$res = mysqli_query($linkbd,$sql);
												$row = mysqli_fetch_row($res);
												$fuentepara = $row[0];
											}
											
											$sqlrins="INSERT INTO  humnomina_saludpension (id_nom, tipo, empleado, tercero, cc, valor ,estado, sector, id, cod_fun, cuenta_presu, tipopago, fuente, indicador_producto, bpin, productoservicio, medio_pago, codigo_politicap, tipo_mov, proyecto, seccion_presupuestal) VALUES ('".$_POST['varini']."','PR','$rowt[1]','$docfp','$rowt[34]','$rowt[13]','S','PR','$idsalud','$rowt[38]', '$cuantapresu', '$rowt[36]', '$fuentepara', '$programaticopara', '$bpimpara', '', 'CSF', '1', '201', '$proyectopara', '$secpresu')";
											mysqli_query($linkbd,$sqlrins);
										}
										echo "<h3 class='example1'>Ajustes realizados</h3>";
									}
								}
								
								$sumatotal = $sumasaludfu + $sumasaludem + $sumapensionfu + $sumapensionem + $sumafondosol;
								echo"
								<div class='content' style='overflow-x:hidden'>
										<table class='inicio'>
											<tr class='titulos'>
												<td>C&oacute;digo</td>
												<td>Aportes Salud y pension</td>
												<td style='width:10%'>Porcentaje</td>
												<td style='width:10%'>Valor</td>
												<td>descripci&oacute;n</td>
											</tr>
											<tr class='zebra1'>
												<td>07</td>
												<td>SALUD EMPLEADOR</td>
												<td>8.50000 %</td>
												<td style='text-align:right;'>$ ".number_format($sumasaludem,0)."&nbsp;</td>
												<td>APORTES EMPRESA</td>
											</tr>
											<tr class='zebra2'>
												<td>08</td>
												<td>SALUD EMPLEADO</td>
												<td>4.00000 %</td>
												<td style='text-align:right;'>$ ".number_format($sumasaludfu,0)."&nbsp;</td>
												<td>APORTES EMPLEADO</td>
											</tr>
											<tr class='zebra1'>
												<td>09</td>
												<td>PENSION EMPLEADOR</td>
												<td>12.00000 %</td>
												<td style='text-align:right;'>$ ".number_format($sumapensionem,0)."&nbsp;</td>
												<td>APORTES EMPRESA</td>
											</tr>
											<tr class='zebra2'>
												<td>10</td>
												<td>PENSION EMPLEADO</td>
												<td>4.00000 %</td>
												<td style='text-align:right;'>$ ".number_format($sumapensionfu,0)."&nbsp;</td>
												<td>APORTES EMPLEADO</td>
											</tr>
											<tr class='zebra1'>
												<td>10</td>
												<td>FONDO SOLIDARIDAD</td>
												<td>Ind/Fu</td>
												<td style='text-align:right;'>$ ".number_format($sumafondosol,0)."&nbsp;</td>
												<td>APORTES EMPLEADO</td>
											</tr>
											<tr class='titulos'>
												<td colspan='3' style='text-align:right;'>TOTAL :</td>
												<td style='text-align:right;'>$ ".number_format($sumatotal,0)."&nbsp;</td>
												<td></td>
											</tr>";
								
							}
							else{echo "<h3 class='example1'>Error agrega numero de nomina en inicio</h3>";}
						}break;
						case '2':{ //Recalcular los aportes Parafiscales
							if($_POST['varini']!='')
							{
								$pf[] = array();
								$pfn[] = array();
								//**** carga parametros de nomina
								$sqlrtp = "SELECT * FROM humparametrosliquida";
								$resptp = mysqli_query($linkbd,$sqlrtp);
								$rowtp = mysqli_fetch_row($resptp);
								$tcajacomp = $rowtp[17];
								$ticbf = $rowtp[18];
								$tsena = $rowtp[19];
								$titi = $rowtp[20];
								$tesap = $rowtp[21];
								$tarp = $rowtp[22];
								if($_POST['varfin'] == 'SI')
								{
									//******Borrar tabla parafiscales
									$sqld = "DELETE FROM humnomina_parafiscales WHERE id_nom='".$_POST['varini']."'";
									$resd = mysqli_query($linkbd,$sqld);
								}
								$sqlrg = "SELECT * FROM humnomina WHERE id_nom='".$_POST['varini']."'";
								$respg = mysqli_query($linkbd,$sqlrg);
								$rowg = mysqli_fetch_row($respg);
								$fecha = $rowg[1];
								$vigencia = $rowg[7];
								$sumaarl = $sumaccf = $sumasena = $sumaicbf = $sumaintec = $sumaesap  = 0;
								$sqlrt = "SELECT * FROM humnomina_det WHERE id_nom='".$_POST['varini']."'";
								$respt = mysqli_query($linkbd,$sqlrt);
								while ($rowt = mysqli_fetch_row($respt))
								{
									$tipopresupuesto = buscartipovinculacion($rowt[0], $rowt[36], $rowt[38]);
									$sumaarl += $rowt[30];
									$sumaccf += $rowt[22];
									$sumasena += $rowt[23];
									$sumaicbf += $rowt[24];
									$sumaintec += $rowt[25];
									$sumaesap += $rowt[26];
									if($_POST['varfin']=='SI')
									{
										$cuantapresu = $porcentaje = $secpresu =  $programaticopara = $proyectopara = $bpimpara = '';
										$secpresu = itemfuncionarios($rowt[38],'42');
										if($tipopresupuesto[1] == 'IN' || $tipopresupuesto[1] == 'IT')
										{
											$fuentepara = itemfuncionarios($rowt[38],'44');
											$programaticopara = itemfuncionarios($rowt[38],'43');
											$proyectopara = itemfuncionarios($rowt[38],'31');
											$bpimpara = nombrebpim($proyectopara);
										}
										else
										{
											$bpimpara = '';
											$programaticopara ='';
											$proyectopara = '';
										}
										if($rowt[22] > 0)//CAJAS DE COMPENSACION
										{
											$cuantapresu = buscarcuentanuevocatalogo2($tcajacomp,$tipopresupuesto[1],"N/A");
											$porcentaje = itemfuncionarios($rowt[38],'36');
											if($tipopresupuesto[1] != 'IN' && $tipopresupuesto[1] != 'IT')
											{
												$fuentepara = 0;
												$sql = "SELECT fuente FROM ccpetinicialgastosfun WHERE cuenta = '$cuantapresu' AND seccion_presupuestal = '$secpresu' AND vigencia = '$vigencia'";
												$res = mysqli_query($linkbd,$sql);
												$row = mysqli_fetch_row($res);
												$fuentepara = $row[0];
											}
											$nuevoid = selconsecutivo('humnomina_parafiscales','id');
											$sqlr="INSERT INTO humnomina_parafiscales (id_nom, id_parafiscal, porcentaje, valor, cc, estado, cuentapresu, id, codfun, tipopago, fuente, indicador_producto, bpin, productoservicio, medio_pago, codigo_politicap, tipo_mov, proyecto, seccion_presupuestal) VALUES ('".$_POST['varini']."', '$tcajacomp', '$porcentaje', '$rowt[22]', '$rowt[34]', 'S', '$cuantapresu', '$nuevoid', '$rowt[38]', '$rowt[36]', '$fuentepara', '$programaticopara', '$bpimpara', '', 'CSF', '1', '201', '$proyectopara', '$secpresu')";
											mysqli_query($linkbd,$sqlr);
										}
										if($rowt[24] > 0)//ICBF
										{
											$cuantapresu = buscarcuentanuevocatalogo2($ticbf,$tipopresupuesto[1],"N/A");
											$porcentaje = itemfuncionarios($rowt[38],'37');
											if($tipopresupuesto[1] != 'IN' && $tipopresupuesto[1] != 'IT')
											{
												$fuentepara = 0;
												$sql = "SELECT fuente FROM ccpetinicialgastosfun WHERE cuenta = '$cuantapresu' AND seccion_presupuestal = '$secpresu' AND vigencia = '$vigencia'";
												$res = mysqli_query($linkbd,$sql);
												$row = mysqli_fetch_row($res);
												$fuentepara = $row[0];
											}
											$nuevoid = selconsecutivo('humnomina_parafiscales','id');
											$sqlr = "INSERT INTO humnomina_parafiscales (id_nom, id_parafiscal, porcentaje, valor, cc, estado, cuentapresu, id, codfun, tipopago, fuente, indicador_producto, bpin, productoservicio, medio_pago, codigo_politicap, tipo_mov, proyecto, seccion_presupuestal) VALUES ('".$_POST['varini']."', '$ticbf', '$porcentaje', '$rowt[24]', '$rowt[34]', 'S', '$cuantapresu', '$nuevoid', '$rowt[38]', '$rowt[36]', '$fuentepara', '$programaticopara', '$bpimpara', '', 'CSF', '1', '201', '$proyectopara', '$secpresu')";
											mysqli_query($linkbd,$sqlr);
										}
										if($rowt[23] > 0)//SENA
										{
											$cuantapresu = buscarcuentanuevocatalogo2($tsena,$tipopresupuesto[1],"N/A");
											$porcentaje = itemfuncionarios($rowt[38],'38');
											if($tipopresupuesto[1] != 'IN' && $tipopresupuesto[1] != 'IT')
											{
												$fuentepara = 0;
												$sql = "SELECT fuente FROM ccpetinicialgastosfun WHERE cuenta = '$cuantapresu' AND seccion_presupuestal = '$secpresu' AND vigencia = '$vigencia'";
												$res = mysqli_query($linkbd,$sql);
												$row = mysqli_fetch_row($res);
												$fuentepara = $row[0];
											}
											$nuevoid = selconsecutivo('humnomina_parafiscales','id');
											$sqlr = "INSERT INTO humnomina_parafiscales (id_nom, id_parafiscal, porcentaje, valor, cc, estado, cuentapresu, id, codfun, tipopago, fuente, indicador_producto, bpin, productoservicio, medio_pago, codigo_politicap, tipo_mov, proyecto, seccion_presupuestal) VALUES ('".$_POST['varini']."', '$tsena', '$porcentaje', '$rowt[23]', '$rowt[34]', 'S', '$cuantapresu','$nuevoid','$rowt[38]', '$rowt[36]', '$fuentepara', '$programaticopara', '$bpimpara', '', 'CSF', '1', '201', '$proyectopara', '$secpresu')";echo 
											mysqli_query($linkbd,$sqlr);
										}
										if($rowt[25] > 0)//ITI
										{
											$cuantapresu = buscarcuentanuevocatalogo2($titi,$tipopresupuesto[1],"N/A");
											$porcentaje = itemfuncionarios($rowt[38],'39');
											if($tipopresupuesto[1] != 'IN' && $tipopresupuesto[1] != 'IT')
											{
												$fuentepara = 0;
												$sql = "SELECT fuente FROM ccpetinicialgastosfun WHERE cuenta = '$cuantapresu' AND seccion_presupuestal = '$secpresu' AND vigencia = '$vigencia'";
												$res = mysqli_query($linkbd,$sql);
												$row = mysqli_fetch_row($res);
												$fuentepara = $row[0];
											}
											$nuevoid = selconsecutivo('humnomina_parafiscales','id');
											$sqlr = "INSERT INTO humnomina_parafiscales (id_nom, id_parafiscal, porcentaje, valor, cc, estado, cuentapresu, id, codfun, tipopago, fuente, indicador_producto, bpin, productoservicio, medio_pago, codigo_politicap, tipo_mov, proyecto, seccion_presupuestal) VALUES ('".$_POST['varini']."', '$titi', '$porcentaje', '$rowt[25]', '$rowt[34]', 'S', '$cuantapresu','$nuevoid','$rowt[38]', '$rowt[36]', '$fuentepara', '$programaticopara', '$bpimpara', '', 'CSF', '1', '201', '$proyectopara', '$secpresu')";
											mysqli_query($linkbd,$sqlr);
										}
										if($rowt[26] > 0)//ESAP
										{
											$cuantapresu = buscarcuentanuevocatalogo2($tesap,$tipopresupuesto[1],"N/A");
											$porcentaje = itemfuncionarios($rowt[38],'40');
											if($tipopresupuesto[1] != 'IN' && $tipopresupuesto[1] != 'IT')
											{
												$fuentepara = 0;
												$sql = "SELECT fuente FROM ccpetinicialgastosfun WHERE cuenta = '$cuantapresu' AND seccion_presupuestal = '$secpresu' AND vigencia = '$vigencia'";
												$res = mysqli_query($linkbd,$sql);
												$row = mysqli_fetch_row($res);
												$fuentepara = $row[0];
											}
											$nuevoid = selconsecutivo('humnomina_parafiscales','id');
											$sqlr = "INSERT INTO humnomina_parafiscales (id_nom, id_parafiscal, porcentaje, valor, cc, estado, cuentapresu, id, codfun, tipopago, fuente, indicador_producto, bpin, productoservicio, medio_pago, codigo_politicap, tipo_mov, proyecto, seccion_presupuestal) VALUES ('".$_POST['varini']."', '$tesap', '$porcentaje', '$rowt[26]', '$rowt[34]', 'S','$cuantapresu','$nuevoid','$rowt[38]', '$rowt[36]', '$fuentepara', '$programaticopara', '$bpimpara', '', 'CSF', '1', '201', '$proyectopara', '$secpresu')";
											mysqli_query($linkbd,$sqlr);
										}
										if($rowt[30] > 0)//ARL
										{
											$cuantapresu = buscarcuentanuevocatalogo2($tarp,$tipopresupuesto[1],"N/A");
											$porcentaje = porcentajearl($rowt[38]);
											if($tipopresupuesto[1] != 'IN' && $tipopresupuesto[1] != 'IT')
											{
												$fuentepara = 0;
												$sql = "SELECT fuente FROM ccpetinicialgastosfun WHERE cuenta = '$cuantapresu' AND seccion_presupuestal = '$secpresu' AND vigencia = '$vigencia'";
												$res = mysqli_query($linkbd,$sql);
												$row = mysqli_fetch_row($res);
												$fuentepara = $row[0];
											}
											$nuevoid = selconsecutivo('humnomina_parafiscales','id');
											$sqlr="INSERT INTO humnomina_parafiscales (id_nom, id_parafiscal, porcentaje, valor, cc, estado, cuentapresu, id, codfun, tipopago, fuente, indicador_producto, bpin, productoservicio, medio_pago, codigo_politicap, tipo_mov, proyecto, seccion_presupuestal) VALUES ('".$_POST['varini']."', '$tarp', '$porcentaje', '$rowt[30]', '$rowt[34]', 'S', '$cuantapresu','$nuevoid','$rowt[38]', '$rowt[36]', '$fuentepara', '$programaticopara', '$bpimpara', '', 'CSF', '1', '201', '$proyectopara', '$secpresu')";
											mysqli_query($linkbd,$sqlr);
										}
										echo "<h3 class='example1'>Ajustes realizados</h3>";
									}
								}
								$sumatotal = $sumaarl + $sumaccf + $sumasena + $sumaicbf + $sumaintec + $sumaesap;
								echo"
								<div class='content' style='overflow-x:hidden'>
										<table class='inicio'>
											<tr class='titulos'>
												<td>C&oacute;digo</td>
												<td>Aportes Parafiscales</td>
												<td style='width:10%'>Porcentaje</td>
												<td style='width:10%'>Valor</td>
												<td>descripci&oacute;n</td>
											</tr>
											<tr class='zebra1'>
												<td>$tcajacomp</td>
												<td>CAJAS DE COMPENSACION FAMILIAR</td>
												<td>Ind/Fun</td>
												<td style='text-align:right;'>$ ".number_format($sumaccf,0)."&nbsp;</td>
												<td>APORTES EMPRESA</td>
											</tr>
											<tr class='zebra2'>
												<td>$ticbf</td>
												<td>ICBF</td>
												<td>Ind/Fun</td>
												<td style='text-align:right;'>$ ".number_format($sumaicbf,0)."&nbsp;</td>
												<td>APORTES EMPRESA</td>
											</tr>
											<tr class='zebra1'>
												<td>$tsena</td>
												<td>SENA</td>
												<td>Ind/Fun</td>
												<td style='text-align:right;'>$ ".number_format($sumasena,0)."&nbsp;</td>
												<td>APORTES EMPRESA</td>
											</tr>
											<tr class='zebra2'>
												<td>$titi</td>
												<td>INSTITUTOS TECNICOS</td>
												<td>Ind/Fun</td>
												<td style='text-align:right;'>$ ".number_format($sumaintec,0)."&nbsp;</td>
												<td>APORTES EMPRESA</td>
											</tr>
											<tr class='zebra1'>
												<td>$tesap</td>
												<td>ESAP</td>
												<td>Ind/Fun</td>
												<td style='text-align:right;'>$ ".number_format($sumaesap,0)."&nbsp;</td>
												<td>APORTES EMPRESA</td>
											</tr>
											<tr class='zebra2'>
												<td>$tarp</td>
												<td>ARL</td>
												<td>Ind/Fun</td>
												<td style='text-align:right;'>$ ".number_format($sumaarl,0)."&nbsp;</td>
												<td>APORTES EMPRESA</td>
											</tr>
											<tr class='titulos'>
												<td colspan='3' style='text-align:right;'>TOTAL :</td>
												<td style='text-align:right;'>$ ".number_format($sumatotal,0)."&nbsp;</td>
												<td></td>
											</tr>";
								
							}
							else{echo "<h3 class='example1'>Error agrega numero de nomina en inicio</h3>";}
						}break;
						case '3':{ //Recalcular Cuentas Nomina
							if($_POST['varini'] != '')
							{
								echo"
								<div class='content' style='overflow-x:hidden'>
										<table class='inicio'>
											<tr class='titulos'>
												<td>Funcionario</td>
												<td>Tipo</td>
												<td>Cuenta</td>
												<td>Fuente</td>
												<td>Programatico</td>
												<td>Bpin</td>
												<td>Proyecto</td>
												<td>Sec. Presupuestal</td>
											</tr>";
								$numnomina = $_POST['varini'];
								$sql = "SELECT id, idfuncionario, tipopago FROM humnomina_det WHERE id_nom='$numnomina'";
								$resp = mysqli_query($linkbd,$sql);
								while ($row = mysqli_fetch_row($resp))
								{
									$nombrefun = itemfuncionarios($row[1],'7');
									$nomtipopago = nombrevariblespagonomina($row[2]);
									$tipopresupuesto = buscartipovinculacion($numnomina, $row[2], $row[1]);
									$cuantapresu = buscarcuentanuevocatalogo1($row[2],$tipopresupuesto[0]);
									$secpresu = itemfuncionarios($row[1],'42');
									if($tipopresupuesto[0] == 'IN' || $tipopresupuesto[0] == 'IT')
									{
										$fuente = $programatico = $proyecto = '';
										$fuente = itemfuncionarios($row[1],'44');
										$programatico = itemfuncionarios($row[1],'43');
										$proyecto = itemfuncionarios($row[1],'31');
										$bpim = nombrebpim($proyecto);
									}
									else
									{
										$bpim = $programatico = $proyecto = $fuente = '';
										$sqlvg = "SELECT vigencia FROM humnomina WHERE id_nom='$numnomina'";
										$resvg = mysqli_query($linkbd,$sqlvg);
										$rowvg = mysqli_fetch_row($resvg);
										$sqlfu = "SELECT fuente FROM ccpetinicialgastosfun WHERE cuenta = '$cuantapresu' AND seccion_presupuestal = '$secpresu' AND vigencia = '$rowvg[0]'";
										$resfu = mysqli_query($linkbd,$sqlfu);
										$rowfu = mysqli_fetch_row($resfu);
										$fuente = $rowfu[0];
									}
									echo "
									<tr>
										<td>($row[1]) $nombrefun</td>
										<td>($row[2]) $nomtipopago</td>
										<td>$cuantapresu</td>
										<td>$fuente</td>
										<td>$programatico</td>
										<td>$bpim</td>
										<td>$proyecto</td>
										<td>$secpresu</td>
									</tr>";
									if($_POST['varfin'] == 'SI')
									{
										$sqlup = "UPDATE humnomina_det SET fuente = '$fuente', indicador_producto = '$programatico', bpin = '$bpim', productoservicio = '', medio_pago = 'CSF', codigo_politicap = '1', tipo_mov = '201', proyecto = '$proyecto', seccion_presupuestal = '$secpresu', cuenta_presu = '$cuantapresu' WHERE id = '$row[0]'";
										$resup = mysqli_query($linkbd,$sqlup);
									}
								}
								echo"
									</table>
								</div>";
							}
						}break;
						case '4':{ //Recalcular presupuesto
							if($_POST['varini'] != '')
							{
								//**** carga parametros de nomina
								$sqlrtp = "SELECT * FROM humparametrosliquida";
								$resptp = mysqli_query($linkbd,$sqlrtp);
								$rowtp = mysqli_fetch_row($resptp);
								$tcajacomp = $rowtp[17];
								$ticbf = $rowtp[18];
								$tsena = $rowtp[19];
								$titi = $rowtp[20];
								$tesap = $rowtp[21];
								$tarp = $rowtp[22];
								$tsaludemr = $rowtp[23];
								$tpensionemr = $rowtp[25];
								$pfcp = array();
								$sqlrg = "SELECT * FROM humnomina WHERE id_nom='".$_POST['varini']."'";
								$respg = mysqli_query($linkbd,$sqlrg);
								$rowg = mysqli_fetch_row($respg);
								$fecha = $rowg[1];
								$vigencia = $rowg[7];
								$cont = 0;
								$sqlrt = "SELECT idfuncionario, tipopago, totaldev, saludemp, pensionemp, tipofondopension, arp, cajacf, sena, icbf, instecnicos, esap FROM humnomina_det WHERE id_nom='".$_POST['varini']."'";
								$respt = mysqli_query($linkbd,$sqlrt);
								while ($rowt = mysqli_fetch_row($respt))
								{
									$cont++;
									$tipopresupuesto = buscartipovinculacion($_POST['varini'], $rowt[1], $rowt[0]);
									$secpresu = itemfuncionarios($rowt[0],'42');
									//NETO A PAGAR TIPO PAGO
									{
										$cuantapresu = buscarcuentanuevocatalogo1($rowt[1],$tipopresupuesto[0]);
										$fuentepre = buscarfuentespres($cuantapresu, $secpresu, $vigencia, $tipopresupuesto[0], $rowt[0]);
										$cuentafuente = $secpresu.'<_>'.$cuantapresu.'<_>'.$fuentepre;
										$pfcp[$cuentafuente]+=$rowt[2];
									}
									//SALUD EMPRESA
									{
										$cuantapresu = buscarcuentanuevocatalogo2($tsaludemr,$tipopresupuesto[1],"N/A");
										$fuentepre = buscarfuentespres($cuantapresu, $secpresu, $vigencia, $tipopresupuesto[1], $rowt[0]);
										$cuentafuente = $secpresu.'<_>'.$cuantapresu.'<_>'.$fuentepre;
										$pfcp[$cuentafuente]+=$rowt[3];
									}
									//PENSION EMPRESA
									{
										$cuantapresu = buscarcuentanuevocatalogo2($tpensionemr,$tipopresupuesto[1],"PR");
										$fuentepre = buscarfuentespres($cuantapresu, $secpresu, $vigencia, $tipopresupuesto[1], $rowt[0]);
										$cuentafuente = $secpresu.'<_>'.$cuantapresu.'<_>'.$fuentepre;
										$pfcp[$cuentafuente]+=$rowt[4];
									}
									//RIESGOS LABORALES
									{
										$cuantapresu = buscarcuentanuevocatalogo2($tarp,$tipopresupuesto[1],"N/A");
										$fuentepre = buscarfuentespres($cuantapresu, $secpresu, $vigencia, $tipopresupuesto[1], $rowt[0]);
										$cuentafuente = $secpresu.'<_>'.$cuantapresu.'<_>'.$fuentepre;
										$pfcp[$cuentafuente]+=$rowt[6];
									}
									//CAJA DE COMPENSACION FAMILIAR
									{
										$cuantapresu = buscarcuentanuevocatalogo2($tcajacomp,$tipopresupuesto[1],"N/A");
										$fuentepre = buscarfuentespres($cuantapresu, $secpresu, $vigencia, $tipopresupuesto[1], $rowt[0]);
										$cuentafuente = $secpresu.'<_>'.$cuantapresu.'<_>'.$fuentepre;
										$pfcp[$cuentafuente]+=$rowt[7];
									}
									//SENA
									{
										$cuantapresu = buscarcuentanuevocatalogo2($tsena,$tipopresupuesto[1],"N/A");
										$fuentepre = buscarfuentespres($cuantapresu, $secpresu, $vigencia, $tipopresupuesto[1], $rowt[0]);
										$cuentafuente = $secpresu.'<_>'.$cuantapresu.'<_>'.$fuentepre;
										$pfcp[$cuentafuente]+=$rowt[8];
									}
									//ICBF
									{
										$cuantapresu = buscarcuentanuevocatalogo2($ticbf,$tipopresupuesto[1],"N/A");
										$fuentepre = buscarfuentespres($cuantapresu, $secpresu, $vigencia, $tipopresupuesto[1], $rowt[0]);
										$cuentafuente = $secpresu.'<_>'.$cuantapresu.'<_>'.$fuentepre;
										$pfcp[$cuentafuente] += $rowt[9];
									}
									//INSTITUTOS TECNICOS
									{
										$cuantapresu = buscarcuentanuevocatalogo2($titi,$tipopresupuesto[1],"N/A");
										$fuentepre = buscarfuentespres($cuantapresu, $secpresu, $vigencia, $tipopresupuesto[1], $rowt[0]);
										$cuentafuente = $secpresu.'<_>'.$cuantapresu.'<_>'.$fuentepre;
										$pfcp[$cuentafuente]+=$rowt[10];
									}
									//ESAP
									{
										$cuantapresu = buscarcuentanuevocatalogo2($tesap,$tipopresupuesto[1],"N/A");
										$fuentepre = buscarfuentespres($cuantapresu, $secpresu, $vigencia, $tipopresupuesto[1], $rowt[0]);
										$cuentafuente = $secpresu.'<_>'.$cuantapresu.'<_>'.$fuentepre;
										$pfcp[$cuentafuente]+=$rowt[11];
									}
								}
								echo"
									<div class='content' style='overflow-x:hidden'>
										<table class='inicio'>
											<tr>
												<td class='titulos'>Tipo Gasto</td>
												<td class='titulos'>Sec. Presupuestal</td>
												<td class='titulos'>Medio de Pago</td>
												<td class='titulos'>Vig. de Gasto</td>
												<td class='titulos'>Proyecto</td>
												<td class='titulos'>Programatico</td>
												<td class='titulos'>Cuenta Presupuestal</td>
												<td class='titulos'>Nombre Cuenta Presupuestal</td>
												<td class='titulos'>Fuente</td>
												<td class='titulos'>BPIM</td>
												<td class='titulos'>Valor</td>
												<td class='titulos' style='width:5%'>Saldo</td>
											</tr>";
								$totalrubro=0;
								$iter="zebra1";
								$iter2="zebra2";
								foreach($pfcp as $k => $valrubros)
								{
									$datoscuenta = explode('<_>', $k);
									$ncta=nombrecuentapresu($datoscuenta[1]);
									if($valrubros>0)
									{
										$saldo = "";
										if($datoscuenta[4] != '')
										{
											$nomproyec = nombreproyecto($datoscuenta[4]);
											$numbpim = nombrebpim($datoscuenta[4]);
											$numindicador = $datoscuenta[3];
										}
										else 
										{
											$nomproyec = '';
											$numbpim = '';
											$numindicador = '';
										}
										$valfuente = $valrubros;
										$tipogasto = substr($datoscuenta[1],0,3);
										$mediopago = 'CSF';
										$viggasto = '1';
										$parametros = array(
											'rubro' => $datoscuenta[1],
											'fuente' => $datoscuenta[2],
											'vigencia' => $vigencia,
											'tipo_gasto' => substr($datoscuenta[1],2,1),
											'seccion_presupuestal' => $datoscuenta[0],
											'medio_pago' => $mediopago,
											'vigencia_gasto' => '1',
											'codProyecto' => $numbpim,
											'programatico' => $numindicador
										);
										$valsaldo = saldoPorRubro($parametros);
										if($valfuente > $valsaldo)
										{
											$_POST['saldocuentas'] = "1";
											$saldo="SIN SALDO";
											$color=" style='text-align:center;background-color :#901; color:#fff' ";
										}
										else
										{
											$saldo = "OK";
											$color = " style='text-align:center;background-color :#092; color:#fff' ";
										}
										echo "
										<input type='hidden' name='valindicadorc[]' value='$numindicador'/>
										<input type='hidden' name='valbpinc[]' value='$numbpim'/>
										<input type='hidden' name='secpresu[]' value='$datoscuenta[0]'/>
										<input type='hidden' name='rubrosp[]' value='$datoscuenta[1]'/>
										<input type='hidden' name='nrubrosp[]' value='".strtoupper($ncta)."'/>
										<input type='hidden' name='fuente[]' value='$datoscuenta[2]'/>
										<input type='hidden' name='nomproyecto[]' value='$nomproyec'/>
										<input type='hidden' name='numproyecto[]' value='$datoscuenta[4]'/>
										<input type='hidden' name='vrubrosp[]' value='$valfuente'/>
										<input type='hidden' name='vsaldo[]' value='$saldo'/>
										<input type='hidden' name='nproyecto[]' value='$saldo'/>
										<tr class='$iter'>
											<td>$tipogasto</td>
											<td>$datoscuenta[0]</td>
											<td>CSF</td>
											<td>1 - Vigencia Actual</td>
											<td>$nomproyec</td>
											<td>$numindicador</td>
											<td>$datoscuenta[1]</td>
											<td>".strtoupper($ncta)."</td>
											<td>$datoscuenta[2]</td>
											<td>$numbpim</td>
											<td style='text-align:right;'>".number_format($valfuente,0)."</td>
											<td $color>".$saldo."</td>
										</tr>";
										$totalrubro+=$valfuente;
										$aux=$iter;
										$iter=$iter2;
										$iter2=$aux;
									}
								}
								echo"
										<tr class='$iter'>
											<td style='text-align:right;' colspan='10'>Total:</td>
											<td style='text-align:right;'>".number_format($totalrubro,2)."</td>
										</tr>
									</tabla>
								</div>";
								if($_POST['varfin']=='SI')
								{
									$sqld="DELETE FROM humnom_presupuestal WHERE id_nom = '".$_POST['varini']."'";
									mysqli_query($linkbd,$sqld);
									$conta = count($_POST['rubrosp']);
									$numnomi = $_POST['varini'];
									for($x = 0; $x < $conta; $x++)
									{
										$idnompresu = selconsecutivo('humnom_presupuestal','id');
										$incuentap = $_POST['rubrosp'][$x];
										$valorrubros = $_POST['vrubrosp'][$x];
										$infuentes = $_POST['fuente'][$x];
										$numproy = $_POST['numproyecto'][$x];
										$valbpin = $_POST['valbpinc'][$x];
										$valindicaro = $_POST['valindicadorc'][$x];
										$valtipogasto = substr($_POST['rubrosp'][$x],0,3);
										$valsecpresu = $_POST['secpresu'][$x];
										$sqlhum_presu = "INSERT INTO humnom_presupuestal (id_nom, cuenta, valor, estado, fuente, proyecto, medio_pago, id, producto, indicador, bpin, tipo_gasto, seccion_presupuestal, vigencia_gasto, divipola, chip) VALUES ('$numnomi', '$incuentap', '$valorrubros', 'S', '$infuentes', '$numproy', 'CSF', '$idnompresu', '', '$valindicaro', '$valbpin', '$valtipogasto', '$valsecpresu','1','','')";
										mysqli_query($linkbd,$sqlhum_presu);
									}
									echo "<h3 class='example1'>Ajustes realizados</h3>";
								}
							}
						}break;
						case '5':{ //Recalcular Descuentos Nomina
							if($_POST['varini'] != '')
							{
								$sql1 = "SELECT id_des, tipopago, cod_fun FROM humnominaretenemp WHERE id_nom = '".$_POST['varini']."'";
								$res1 = mysqli_query($linkbd,$sql1);
								while ($row1 = mysqli_fetch_row($res1))
								{
									$sql2 = "SELECT fuente, indicador_producto, bpin, proyecto, seccion_presupuestal, cuenta_presu FROM humnomina_det WHERE id_nom = '".$_POST['varini']."' AND tipopago = '$row1[1]' AND idfuncionario = '$row1[2]'";
									$res2 = mysqli_query($linkbd,$sql2);
									$row2 = mysqli_fetch_row($res2);
									
									$sql3 = "UPDATE humnominaretenemp SET fuente ='$row2[0]', indicador_producto = '$row2[1]', bpin = '$row2[2]', codigo_politicap = '1', tipo_mov = '201', proyecto = '$row2[3]', seccion_presupuestal = '$row2[4]', cuenta_presu = '$row2[5]' WHERE id_des = '$row1[0]'";
									$res3 = mysqli_query($linkbd,$sql3);
									$row3 = mysqli_fetch_row($res3);
								}
							}
						}break;
						case '7':{ //Agregar cod usuario y tipo a descuentos nomina
							if($_POST['varini'] != '' && $_POST['varfin'] != '')
							{
								for($x = $_POST['varini']; $x <= $_POST['varfin']; $x++)
								{
									$sql1 = "SELECT id, id_des FROM humnominaretenemp WHERE id_nom = '$x'";
									$res1 = mysqli_query($linkbd,$sql1);
									while ($row1 = mysqli_fetch_row($res1))
									{
										$sql2 = "SELECT tipopago, idfuncionario, id_retencion FROM humretenempleados WHERE id = '$row1[0]'";
										$res2 = mysqli_query($linkbd,$sql2);
										$row2 = mysqli_fetch_row($res2);

										$sql3 = "UPDATE humnominaretenemp SET tipopago = '$row2[0]', cod_fun = '$row2[1]', idtercero = '$row2[2]' WHERE id_des = '$row1[1]'";
										$res3 = mysqli_query($linkbd,$sql3);
										$row3 = mysqli_fetch_row($res3);
									}
								}
							}
						}break;
						case '8':{
							if($_POST['varini']!='' && $_POST['varfin']!='')
							{
								$sqlr="SELECT id_orden,cuentap,id_det FROM tesoegresosnomina_det WHERE id_egreso BETWEEN '".$_POST['varini']."' AND '".$_POST['varfin']."'";
								$res = mysqli_query($linkbd,$sqlr);
								while ($row =mysqli_fetch_row($res)) 
								{
									$sqlr1="SELECT fuente FROM humnom_presupuestal WHERE id_nom = '$row[0]' AND cuenta = '$row[1]'";
									$res1 = mysqli_query($linkbd,$sqlr1);
									$row1 = mysqli_fetch_row($res1);
									$sqlr2="UPDATE tesoegresosnomina_det SET fuente = '$row1[0]' WHERE id_det = '$row[2]'";
									$res2 = mysqli_query($linkbd,$sqlr2);
								}
								echo "<h3 class='example1'>Ajustes realizados</h3>";
							}
						}break;
						case '9':{
							if($_POST['varini']=='SI')
							{
								$sqlr="SELECT id,cuenta,proyecto FROM humnom_presupuestal WHERE proyecto <> ''";
								$res = mysqli_query($linkbd,$sqlr);
								while ($row =mysqli_fetch_row($res))
								{
									$sqlr1="SELECT indicador_producto FROM ccpproyectospresupuesto_presupuesto WHERE codproyecto = '$row[2]' AND rubro = '$row[1]'";
									$res1 = mysqli_query($linkbd,$sqlr1);
									$row1 =mysqli_fetch_row($res1);
									$producto = substr($row1[0], 0, 7);
									$sqlr2="UPDATE humnom_presupuestal SET producto = '$producto',indicador = '$row1[0]' WHERE id = '$row[0]'";
									$res2 = mysqli_query($linkbd,$sqlr2);
								}echo "<h3 class='example1'>Ajustes realizados</h3>";
							}
						}break;
						case '10':{ //Borrar Politicas Publicas en nomina
							if($_POST['varini'] != ''){
								$sql = "SELECT cdp, rp, vigencia, nomina FROM hum_nom_cdp_rp WHERE nomina >= '".$_POST['varini']."'";
								$res = mysqli_query($linkbd,$sql);
								while ($row = mysqli_fetch_row($res)){
									if($row[0] != 0 && $row[1] != 0){
										//cdp
										$sqlup = "UPDATE ccpetcdp_detalle SET codigo_politicap = '' WHERE vigencia = '$row[2]' and consvigencia = '$row[0]';";
										mysqli_query($linkbd,$sqlup);
										//rp
										$sqlup = "UPDATE ccpetrp_detalle SET codigo_politicap = '' WHERE vigencia = '$row[2]' and consvigencia = '$row[1]';";
										mysqli_query($linkbd,$sqlup);
										//egreso nomina
										$sqlup = "UPDATE tesoegresosnomina_det SET codigo_politicap = '' WHERE 	id_orden = '$row[3]';";
										mysqli_query($linkbd,$sqlup);
									}
								}
							}
						}break;
						case '11':{ // Recalcular CDP y RP
							if($_POST['varini'] != ''){
								$sql = "SELECT cdp, rp, vigencia, nomina FROM hum_nom_cdp_rp WHERE nomina = '".$_POST['varini']."'";
								$res = mysqli_query($linkbd,$sql);
								while ($row = mysqli_fetch_row($res)){
									if($row[0] != 0 ){
										$sumtotalcdp = 0;
										$sqld = "DELETE FROM ccpetcdp_detalle WHERE vigencia = '$row[2]' AND consvigencia = '$row[0]'";
										mysqli_query($linkbd,$sqld);
										$sqlh = "SELECT cuenta, producto, fuente, valor, indicador, medio_pago, proyecto, vigencia_gasto, bpin, seccion_presupuestal, tipo_gasto FROM humnom_presupuestal WHERE id_nom = '$row[3]'";
										$resh = mysqli_query($linkbd,$sqlh);
										while ($rowh = mysqli_fetch_row($resh)){
											$sqlupcdp = "INSERT INTO ccpetcdp_detalle (id_cdpdetalle, vigencia, consvigencia, cuenta, productoservicio, fuente, valor, estado, saldo, saldo_liberado, tipo_mov, indicador_producto, medio_pago, proyecto, codigo_vigenciag, codigo_politicap, bpim, seccion_presupuestal, tipo_gasto, divipola, chip) VALUES (null, '$row[2]', '$row[0]', '$rowh[0]', '$rowh[1]', '$rowh[2]', '$rowh[3]', 'S', '$rowh[3]', '0', '201', '$rowh[4]', '$rowh[5]', '$rowh[6]', '$rowh[7]', '', '$rowh[8]', '$rowh[9]', '$rowh[10]','','')";
											mysqli_query($linkbd,$sqlupcdp);
											$sumtotalcdp += $rowh[3];
										}
										$sqlrescdp = "UPDATE ccpetcdp SET valor = '$sumtotalcdp', saldo = '$sumtotalcdp' WHERE vigencia = '$row[2]' AND consvigencia = '$row[0]'";
										mysqli_query($linkbd,$sqlrescdp);
									}
									if($row[1] != 0 ){
										$sumtotalrp = 0;
										$sqld = "DELETE FROM ccpetrp_detalle WHERE vigencia = '$row[2]' AND consvigencia = '$row[1]'";
										mysqli_query($linkbd,$sqld);
										$sqlh = "SELECT cuenta, producto, fuente, valor, indicador, medio_pago, proyecto, vigencia_gasto, bpin, seccion_presupuestal, tipo_gasto FROM humnom_presupuestal WHERE id_nom = '$row[3]'";
										$resh = mysqli_query($linkbd,$sqlh);
										while ($rowh = mysqli_fetch_row($resh)){
											$sqluprp= "INSERT INTO ccpetrp_detalle (id_cdpdetalle, vigencia, consvigencia, cuenta, productoservicio, fuente, valor, estado, saldo, saldo_liberado, tipo_mov, indicador_producto, 	medio_pago, proyecto, codigo_vigenciag, codigo_politicap, bpim, seccion_presupuestal, tipo_gasto, 	divipola, chip) VALUES (null, '$row[2]', '$row[1]', '$rowh[0]', '$rowh[1]', '$rowh[2]', '$rowh[3]', 'S', '$rowh[3]', '0', '201', '$rowh[4]', '$rowh[5]', '$rowh[6]', '$rowh[7]', '', '$rowh[8]', '$rowh[9]', '$rowh[10]', '', '')";
											mysqli_query($linkbd,$sqluprp);
											$sumtotalrp += $rowh[3];
										}
										$sqlresrp = "UPDATE ccpetrp SET valor = '$sumtotalrp', saldo = '$sumtotalrp' WHERE vigencia = '$row[2]' AND consvigencia = '$row[1]'";
										mysqli_query($linkbd,$sqlresrp);
									}
									echo "<h3 class='example1'>Ajustes realizados</h3>";
								}
							}
						}break;
						case '12':{ //Ingresar CC en  descuentos y retenciones
							if($_POST['varfin'] == 'SI'){

								$sql1 = "SELECT T1.codfun, GROUP_CONCAT(T1.descripcion ORDER BY CONVERT(T1.codrad, SIGNED INTEGER) SEPARATOR '<->') FROM hum_funcionarios T1 WHERE  T1.item = 'NUMCC' AND T1.estado = 'S' AND (SELECT T2.codfun FROM hum_funcionarios T2 WHERE T2.descripcion  LIKE  '40449881' AND T2.estado='S' AND T2.codfun=T1.codfun AND (T2.item='DOCTERCERO')) GROUP BY T1.codfun ORDER BY CONVERT(T1.codfun, SIGNED INTEGER) DESC LIMIT 1";

							}
						}
					}
				}
			?> 
			</div>
		</form>
	</body>
</html>