<?php
	require_once("tcpdf2/tcpdf_include.php");
	require('comun.inc');
	require"funciones.inc";
	require 'funcionessp.inc';
	session_start();
	class MYPDF extends TCPDF 
	{
		public function Header() {}
		public function Footer() 
		{
			
		}
	}
	$pdf = new MYPDF('P','mm','A4', true, 'iso-8859-1', false);// create new PDF document
	$pdf->SetDocInfoUnicode (true); 
	// set document information
	$pdf->SetCreator(PDF_CREATOR);
	$pdf->SetAuthor('G&CSAS');
	$pdf->SetTitle('Factura');
	$pdf->SetSubject('Factura');
	$pdf->SetKeywords('');
	$pdf->SetMargins(0, 0, 0);// set margins
	$pdf->SetHeaderMargin(0);// set margins
	$pdf->SetFooterMargin(0);// set margins
	$pdf->SetAutoPageBreak(TRUE, 0);// set auto page breaks
	// set some language-dependent strings (optional)
	if (@file_exists(dirname(__FILE__).'/lang/spa.php')) 
	{
		require_once(dirname(__FILE__).'/lang/spa.php');
		$pdf->setLanguageArray($l);
	}
	// ---------------------------------------------------------
	ini_set('max_execution_time', 7200);
	ereg( "([0-9]{2,4})-([0-9]{1,2})-([0-9]{1,2})", $_POST[fecha],$fecha);
	$fechaf=$fecha[3]."-".$fecha[2]."-".$fecha[1];
	$fechaimp=mktime(0,0,0,$fecha[2],$fecha[3],$fecha[1]);
	$_POST[fecha]=$fechaimp;
	$fechaini=mktime(0,0,0,$_POST[periodo],"01",$_POST[vigencias]);
	$ultdia=date("d",mktime(0,0,0,$_POST[periodo2]+1,0,$_POST[vigencias]));
	$fechafin=mktime(0,0,0,$_POST[periodo2],$ultdia,$_POST[vigencias]);
	$fechapagop=$fechaimp+($_POST[diaplazo]*24*60*60);
	$totalrec=count($_POST[totalfact]);
	$tam=$_POST[facfin];
	$tmin=$_POST[facini];
	$sqlrb="SELECT nombre FROM  hum_bancosfun WHERE codigo=$row[4]";
	$resb=mysql_query($sqlrb,$linkbd);
	$rowb=mysql_fetch_row($resb);
	$nombanco=$rowb[0];
	$cod00=$row[1];
	if($row[0]!='' || $row[0]!= NULL){$cod01=$row[0];/*codigo GS1 Asignado*/}
	else {$cod01='0000000000000';/*codigo sin asignar*/}
	$cod01="7709998328310";
	for($yx=$tmin;$yx<=$tam;$yx++)
	{	
		$pdf->AddPage();
		$style = array(
			'position' => 'C',
			'align' => 'C',
			'stretch' => false,
			'fitwidth' => true,
			'cellfitalign' => '',
			'border' => false,
			'hpadding' => 'auto',
			'vpadding' => 'auto',
			'fgcolor' => array(0,0,0),
			'bgcolor' => false, //array(255,255,255),
			'text' => false,
			'font' => 'helvetica',
			'fontsize' => 8,
			'stretchtext' => 1);
		$sumafactura=0;
		$sumafactura2=0;
		$sumaintereses=0;
		$sumasubsidios=0;
		$sumasaldoant=0;
		$sumacontri=0;
		$sumafactura2=array_sum($_POST[totalfact2][$yx]);
		$sumafactura=array_sum($_POST[totalfact][$yx]);
		$sumasubsidios=array_sum($_POST[totalsubs][$yx]);
		$sumasaldoant=array_sum($_POST[totalsaldoant][$yx]);	
		$sumacontri=array_sum($_POST[totalcontri][$yx]);	
		$sumaintereses=$_POST[fintereses][$yx];
		$mxcor=0;//margen de correccion horizontal
		$mycor=0;//margen de correccion veritcal
		$nfactura=substr("000000".$_POST[facturas][$yx],-10);//Ingresar Numero de Factura
		$estadofac=$_POST[estfac][$yx];
		$ruta=$_POST[frutas][$yx]; //Ingresar Ruta
		$cedcatastral=$_POST[codigocat][$yx];//Ingresar Cedula catastral
		$codsuscriptor=$_POST[codigousu][$yx];//Ingresar el Codigo del Suscriptor
		$ciclo=$_POST[liquigen];//ingresar Ciclo
		$nomsuscriptor=utf8_decode($_POST[fnomter][$yx]);//Ingresar el Nombre del Suscriptor
		$direccion=$_POST[fdire][$yx];//ingresar Direccio
		$barrio=$_POST[fbarrio][$yx];//Ingresar Barrio
		$uso=$_POST[fuso][$yx];//Ingresar Barrio
		$tes=strlen(strrchr($_POST[festrato][$yx],"-"));
		$estrato=substr(strrchr($_POST[festrato][$yx],"-"),2,$tes);
		//Ingresar Estrato
		$nmedidor="";//Ingresar el Numero de Medidor
		$feciperiodo=date("d-m-Y",$fechaini);//teIngresar Fecha Inicial Periodo Facturado
		$fecfperiodo=date("d-m-Y",$fechafin);//Ingresar Fecha Final Periodo Facturado
		$lecactual="";//Ingresar Lectura Actual
		$lecanterior="";//Ingresar Lectura Anteriror
		$consumo=$lecactual-$lecanterior;//Ingresar El Consumo
		$justificacion="";//Ingresar La Justificacion
		$u6consumo1="";//Ultimo Seis consumos, Consumo 1
		$u6consumo2="";//Ultimo Seis consumos, Consumo 2
		$u6consumo3="";//Ultimo Seis consumos, Consumo 3
		$u6consumo4="";//Ultimo Seis consumos, Consumo 4
		$u6consumo5="";//Ultimo Seis consumos, Consumo 5
		$u6consumo6="";//Ultimo Seis consumos, Consumo 6
		$promediocom=(($u6consumo1+$u6consumo2+$u6consumo3+$u6consumo4+$u6consumo5+$u6consumo6)/6);
		$fecimpresion=date("d-m-Y",$_POST[fecha]);//Ingresar Fecha de Impresion
		$fecpagoopt=date("d-m-Y",$fechapagop);//Ingresar Fecha de Pago Oportuno
		$pagorecargo=date("d-m-Y",$fechapagop);//Ingresar Pago con Recargo
		$totalacueducto=$_POST[totalfact][$yx]["01"];//Ingresar total acueducto
		$totalalcanta=$_POST[totalfact][$yx]["02"];//Ingresar Total Alcantarillado
		$totalaseo=$_POST[totalfact][$yx]["03"];//Ingresar Total Aseo
		$totalotros="0";//Ingresar Tolal Otros pagos
		$totalsubsidios=$sumasubsidios;//Ingresar Total Subsidios
		$totalsobrecos=$sumacontri;//Ingresar Total Sobre Costos
		$totaldeudant=$sumasaldoant;//sin Intereses
		$comentarios=$_POST[obs];
		$pageen="CAJA";//PAGUESE EN 
		$cfacueducto="2000";//Ingresar Cargo Fijo Acueducto
		$cfsubsidio="2000";//Ingresar Cargo Fijo Subsidio
		$cfsobrecosto="2000";//Ingresar Cargo Fijo Sobrecosto
		$totalpago=$totalpagor=$sumafactura2;
		//Ingresar Informacion Estado de Cuenta y Financiacion
		$limecf=4;
		$_POST[comceptoecf]=array();
		$_POST[valorecf]=array();
		for($x=0;$x<$limecf;$x++)
		{
			$_POST[comceptoecf][]="123456789012345678901234";
			$_POST[valorecf][]="15000";
		}
		//Ingresar Informacion Liquidacion Servicio Acueducto
		$limlsa=3;
		$_POST[consumolsa]=array();
		$_POST[acueductolsa]=array();
		$_POST[subsidiolsa]=array();
		$_POST[sobrecostolsa]=array();
		//Ingresar Informacion de Otros Cobros
		$limlsa=5;
		$cser=count($_POST[servcods]);
		$_POST[occodigo]=array();
		$_POST[occomcepto]=array();
		$_POST[ocvalor]=array();
		$_POST[ocsubsidio]=array();
		$_POST[ocsobrecosto]=array();
		$_POST[ocvalorreal]=array();
		$_POST[ocdeuda]=array();
		for($s=0;$s<$cser;$s++)
		{
			if($_POST[servcods][$s]!="")
			{
				$servi=$_POST[servcods][$s];
				if($_POST[totalfact][$yx][$servi]>0 || $_POST[totalfact][$yx+1][$servi]!="")
				{
					$_POST[occodigo][]=$_POST[servcods][$s];
					$_POST[occomcepto][]=$_POST[servnoms][$s];
					$_POST[ocvalor][]=$_POST[totalfact][$yx][$servi];
					$_POST[ocsubsidio][]=$_POST[totalsubs][$yx][$servi];
					$_POST[ocsobrecosto][]=$_POST[totalcontri][$yx][$servi];
					$_POST[ocdeuda][]=$_POST[totalsaldoant][$yx][$servi];
					$_POST[ocvalorreal][]=$_POST[totalfact][$yx][$servi]-$_POST[totalsubs][$yx][$servi]+$_POST[totalcontri][$yx][$servi];
				}
			}
		}
		
		$fecdi=explode('-', $_POST[fecha]);
		$cod02=str_pad($_POST[facturas][$yx],18,"0", STR_PAD_LEFT);//Numero Liquidacion	
		$cod03=str_pad(number_format($totalpago,0,"",""),10,"0", STR_PAD_LEFT);//total a pagar
		$cod04=date("Ymd",$fechapagop);//fecha limite
		$cod00=415;
		$codtotn="($cod00)$cod01(8020)$cod02(3900)$cod03(96)$cod04";
		$codtot=chr(241)."$cod00".$cod01."8020".$cod02.chr(241)."3900".$cod03.chr(241)."96".$cod04;
		//******Cuadro inicial
		$pdf->SetFont('helvetica', '', 10);
		$y=$pdf->GetY();
		//Numero Facura
		$pdf->SetY($y+$mycor);
		$pdf->Cell(164+$mxcor);
		$pdf->Cell(30,0,str_pad($nfactura,10,"0", STR_PAD_LEFT),0,0,'R',FALSE);
		$y=$y+1;
		//Código Barras
		//$pdf->write1DBarcode(''.$codtot, 'C128', 104+$mxcor, ''.$y+1.8+$mycor,100, 15, 0.4, $style, 'N');
		//Numero Código
		$pdf->SetFont('helvetica', '', 7);
		$pdf->SetY($y+15.5+$mycor);
		$pdf->Cell(106+$mxcor);
		//$pdf->Cell(95.5,0,$codtotn,0,0,'C',FALSE);
		//Nombre del Suscriptor
		$pdf->SetFont('helvetica', '', 10);
		$pdf->SetY($y+18+$mycor);
		$pdf->Cell(120+$mxcor);
		$pdf->Cell(62,0,$nomsuscriptor,0,0,'L',FALSE);
		//ingresar La Direccion
		$pdf->SetY($y+22+$mycor);
		$pdf->Cell(120+$mxcor);
		$pdf->Cell(62,0,substr($direccion,0,40),0,0,'L',FALSE);
		//ingresar El Barrio
		$pdf->SetY($y+26+$mycor);
		$pdf->Cell(120+$mxcor);
		$pdf->Cell(60,0,substr($barrio,0,28),0,0,'L',FALSE);
		//ingresar La Ruta
		$pdf->Cell(21.5,0,$ruta,0,0,'L',FALSE);
		//ingresar Estrato o Uso
		$pdf->SetY($y+30+$mycor);
		$pdf->Cell(125+$mxcor);
		if($estrato!=""){$pdf->Cell(55,0,$estrato,0,0,'L',FALSE);}
		else {$pdf->Cell(55,0,substr($uso,0,21),0,0,'L',FALSE);}
		//ingresar El Codigo del Suscriptor
		$pdf->Cell(36,0,$codsuscriptor,0,0,'L',FALSE);
		//ingresar El Codigo Catastral
		$pdf->SetY($y+34+$mycor);
		$pdf->Cell(133+$mxcor);
		$pdf->Cell(37,0,$cedcatastral,0,0,'L',FALSE);
		//******Cuadros consumo
		//ingresar El Ciclo
		$pdf->SetY($y+45+$mycor);
		$pdf->Cell(10+$mxcor);
		$pdf->Cell(9,0,$ciclo,0,0,'L',FALSE);
		//Ingresar Fecha Inicial Periodo Facturado
		$lastday = mktime (0,0,0,substr($feciperiodo,3,2),1,substr($feciperiodo,7,4));
		$vmes=mesletras(strftime('%m',$lastday));	
		$pdf->Cell(20,0,$vmes." - ".substr($feciperiodo,6,4),0,0,'L',FALSE);
		//Ingresar Lectura Actual
		$pdf->SetFont('helvetica', '', 8);
		$pdf->SetY($y+47+$mycor);
		$pdf->Cell(57+$mxcor);
		$pdf->Cell(17,0,$lecanterior,0,0,'R',FALSE);
		//Ingresar Lectura Anterior
		$pdf->Cell(20,0,$lecactual,0,0,'R',FALSE);
		//Ingresar El Consumo
		$pdf->Cell(1);
		$pdf->Cell(24,0,$consumo,0,0,'R',FALSE);
		//Ultimo Consumo periodo 1
		$pdf->Cell(1);
		$pdf->Cell(21,0,$u6consumo4,0,0,'R',FALSE);
		//Ultimo Consumo periodo 2
		$pdf->Cell(1);
		$pdf->Cell(22,0,$u6consumo5,0,0,'R',FALSE);
		//Ultimo Consumo periodo 3
		$pdf->Cell(1);
		$pdf->Cell(24,0,$u6consumo6,0,0,'R',FALSE);
		//Ultimo Seis Consumos, Ingresar promedio
		$pdf->Cell(1);
		$pdf->Cell(25,0,$promediocom,0,0,'R',FALSE);
		//ingresar La Fecha de Pago Oportuno
		$pdf->SetFont('helvetica', '', 10);
		$pdf->SetY($y+53+$mycor);
		$pdf->Cell(10+$mxcor);
		$pdf->Cell(37.5,0,'FECHA PAGO OPORTUNO:  '.$fecpagoopt,0,0,'l',FALSE);
		//***estado fac
		switch ($estadofac) 
		{
			case 'S': 	$estexto='';break;
			case 'P':	$estexto='PAGO';break;
			case 'V':	$estexto='VENCIDA';break;
			case 'L':	$estexto='LIQUIDADA SIN FACTURAR';break;
		}
		$pdf->SetY($y+57+$mycor);
		$pdf->Cell(10+$mxcor);
		$pdf->Cell(30,0,$estexto,0,0,'L',FALSE);
		//***************Bloque Informacion Otros Cobros  ********************
		$pdf->SetFont('helvetica', '', 8);
		$con=0;
		$totalsubsidios=$totalotros=0;
		while ($con<count($_POST[occodigo]))
		{	
			switch($_POST[occodigo][$con])
			{
				case '01':	$pdf->SetY($y+75+$mycor);
							$pdf->Cell(74+$mxcor);
							$pdf->Cell(19,0,number_format($_POST[ocvalor][$con],0,".",","),0,0,'R',false,0,0,false,'T','C');
							if($_POST[ocsubsidio][$con]>0)
							{$pdf->Cell(19,0,'-'.number_format($_POST[ocsubsidio][$con],0,".",","),0,0,'R',false,0,0,false,'T','C');}
							elseif($_POST[ocsobrecosto][$con]>0)
							{$pdf->Cell(19,0,number_format($_POST[ocsobrecosto][$con],0,".",","),0,0,'R',false,0,0,false,'T','C');}
							else
							{$pdf->Cell(19,0,'0',0,0,'R',false,0,0,false,'T','C');}
							$pdf->Cell(26,0,number_format($_POST[ocvalor][$con],0,".",","),0,0,'R',false,0,0,false,'T','C');
							$pdf->SetY($y+75+$mycor);
							$pdf->Cell(134+$mxcor);
							$pdf->Cell(19,0,number_format($_POST[ocdeuda][$con],0,".",","),0,0,'R',false,0,0,false,'T','C');
							$totalservi=$_POST[ocvalor][$con]-$_POST[ocsubsidio][$con]+$_POST[ocsobrecosto][$con]+$_POST[ocdeuda][$con];
							$pdf->SetY($y+75+$mycor);
							$pdf->Cell(177+$mxcor);
							$pdf->Cell(19,0,number_format($totalservi,0,".",","),0,0,'R',false,0,0,false,'T','C');
							$pdf->SetY($y+91+$mycor);
							$pdf->Cell(177+$mxcor);
							$pdf->Cell(19,0,number_format($totalservi,0,".",","),0,0,'R',false,0,0,false,'T','C');
							break;
				case '02':	$pdf->SetY($y+113+$mycor);
							$pdf->Cell(74+$mxcor);
							$pdf->Cell(19,0,number_format($_POST[ocvalor][$con],0,".",","),0,0,'R',false,0,0,false,'T','C');
							if($_POST[ocsubsidio][$con]>0)
							{$pdf->Cell(19,0,'-'.number_format($_POST[ocsubsidio][$con],0,".",","),0,0,'R',false,0,0,false,'T','C');}
							elseif($_POST[ocsobrecosto][$con]>0)
							{$pdf->Cell(19,0,number_format($_POST[ocsobrecosto][$con],0,".",","),0,0,'R',false,0,0,false,'T','C');}
							else
							{$pdf->Cell(19,0,'0',0,0,'R',false,0,0,false,'T','C');}
							$pdf->Cell(26,0,number_format($_POST[ocvalor][$con],0,".",","),0,0,'R',false,0,0,false,'T','C');
							$pdf->SetY($y+113+$mycor);
							$pdf->Cell(134+$mxcor);
							$pdf->Cell(19,0,number_format($_POST[ocdeuda][$con],0,".",","),0,0,'R',false,0,0,false,'T','C');
							$totalservi=$_POST[ocvalor][$con]-$_POST[ocsubsidio][$con]+$_POST[ocsobrecosto][$con]+$_POST[ocdeuda][$con];
							$pdf->SetY($y+113+$mycor);
							$pdf->Cell(177+$mxcor);
							$pdf->Cell(19,0,number_format($totalservi,0,".",","),0,0,'R',false,0,0,false,'T','C');
							$pdf->SetY($y+134+$mycor);
							$pdf->Cell(177+$mxcor);
							$pdf->Cell(19,0,number_format($totalservi,0,".",","),0,0,'R',false,0,0,false,'T','C');
							break;
				case '03':	$pdf->SetY($y+144+$mycor);
							$pdf->Cell(30+$mxcor);
							$pdf->Cell(19,0,'0',0,0,'L',false,0,0,false,'T','C');
							$pdf->SetY($y+147+$mycor);
							$pdf->Cell(30+$mxcor);
							$pdf->Cell(21,0,$ruta,0,0,'L',FALSE);
							$pdf->SetY($y+144+$mycor);
							$pdf->Cell(108+$mxcor);
							$pdf->Cell(19,0,'2 VECES POR SEMANA',0,0,'L',false,0,0,false,'T','C');
							$pdf->SetY($y+147+$mycor);
							$pdf->Cell(108+$mxcor);
							$pdf->Cell(19,0,'2 VECES POR SEMANA',0,0,'L',false,0,0,false,'T','C');
							$pdf->SetY($y+163+$mycor);
							$pdf->Cell(100+$mxcor);
							$pdf->Cell(19,0,number_format(($_POST[ocvalor][$con]/2),0,".",","),0,0,'L',false,0,0,false,'T','C');
							$pdf->SetY($y+163+$mycor);
							$pdf->Cell(116+$mxcor);
							$pdf->Cell(19,0,number_format(($_POST[ocvalor][$con]/2),0,".",","),0,0,'L',false,0,0,false,'T','C');
							$pdf->SetY($y+190+$mycor);
							$pdf->Cell(116+$mxcor);
							$pdf->Cell(19,0,number_format($_POST[ocvalor][$con],0,".",","),0,0,'R',false,0,0,false,'T','C');
							$pdf->SetY($y+194+$mycor);
							$pdf->Cell(116+$mxcor);
							$pdf->Cell(19,0,'-'.number_format($_POST[ocsubsidio][$con],0,".",","),0,0,'R',false,0,0,false,'T','C');
							$pdf->SetY($y+198+$mycor);
							$pdf->Cell(116+$mxcor);
							$pdf->Cell(19,0,number_format($_POST[ocsobrecosto][$con],0,".",","),0,0,'R',false,0,0,false,'T','C');
							$pdf->SetY($y+154+$mycor);
							$pdf->Cell(134+$mxcor);
							$pdf->Cell(19,0,number_format($_POST[ocdeuda][$con],0,".",","),0,0,'R',false,0,0,false,'T','C');
							$pdf->SetY($y+154+$mycor);
							$pdf->Cell(177+$mxcor);
							$totalservi=$_POST[ocvalor][$con]-$_POST[ocsubsidio][$con]+$_POST[ocsobrecosto][$con]+$_POST[ocdeuda][$con];
							$pdf->Cell(19,0,number_format($totalservi,0,".",","),0,0,'R',false,0,0,false,'T','C');
							$pdf->SetY($y+202+$mycor);
							$pdf->Cell(177+$mxcor);
							$pdf->Cell(19,0,number_format($totalservi,0,".",","),0,0,'R',false,0,0,false,'T','C');
			}
			$con=$con+1;
		}  
		$totalsubsidios=array_sum($_POST[ocsubsidio]);
		$totalotros=array_sum($_POST[ocsobrecosto]);
		$octotal=array_sum($_POST[ocvalorreal]);
		$oclugar=$pageen;
		//Informacion de interes
		$pdf->SetFont('helvetica', '', 10);
		$pdf->SetY($y+214+$mycor);
		$pdf->Cell(10+$mxcor);
		$pdf->Cell(30,0,'EL AGUA NO ES APTA PARA CONSUMO HUMANO',0,1,'L',false,0,0,false,'T','C');
		$pdf->Cell(10+$mxcor);
		$pdf->Cell(30,0,'PAGAR EN CORRESPONSAL BANCOLOMBIA',0,1,'L',false,0,0,false,'T','C');
		$pdf->Cell(10+$mxcor);
		$pdf->Cell(30,0,'CUENTA CORRIENTE No. 890-66419-78',0,0,'L',false,0,0,false,'T','C');
		//total factura
		$pdf->SetFont('helvetica', 'B', 12);
		$pdf->SetY($y+228+$mycor);
		$pdf->Cell(177+$mxcor);
		$pdf->Cell(19,0,"$".number_format($totalpago,0,".",","),0,0,'R',FALSE);
		$ajustesy=10;
		//Numero Facura
		$pdf->SetFont('helvetica', 'B', 8);
		$pdf->SetY($y+242+$mycor+$ajustesy);
		$pdf->Cell(10+$mxcor);
		$pdf->Cell(55,0,str_pad("Factura No: ".$nfactura,10,"0", STR_PAD_LEFT),0,0,'L',FALSE);
		//ingresar Nombre Suscriptor
		$pdf->Cell(145,0,"Nombre: ".$nomsuscriptor,0,0,'L',FALSE);
		//Ingresar Fecha Inicial Periodo Facturado
		$pdf->SetY($y+245+$mycor+$ajustesy);
		$pdf->Cell(10+$mxcor);
		$pdf->Cell(55,0,"Periodo Facturado: ".$vmes." - ".substr($feciperiodo,6,4),0,0,'L',FALSE);
		//ingresar El Barrio
		$pdf->Cell(145,0,"Dirección: ".$direccion." ".$barrio,0,0,'L',FALSE);
		//ingresar Fecha Impesion
		$pdf->SetY($y+248+$mycor+$ajustesy);
		$pdf->Cell(10+$mxcor);
		$pdf->Cell(55,0,"Fecha Expedición: ".$fecimpresion,0,0,'L',FALSE);
		//ingresar El Codigo del Suscriptor
		$pdf->Cell(145,0,"Código Usuario: ".$codsuscriptor,0,0,'L',FALSE);
		//ingresar total recaudo
		$pdf->SetY($y+251+$mycor+$ajustesy);
		$pdf->Cell(10+$mxcor);
		$pdf->Cell(55,0,"Total A Pagar: $".number_format($totalpago,0,".",","),0,0,'L',FALSE);
		//codigos de barras
		$pdf->write1DBarcode($codtot, 'C128', 105+$mxcor, ''.$y+256+$mycor+$ajustesy,160, 18, 0.25, $style, 'N');
		//Numero Código
		$pdf->SetFont('helvetica', '', 9);
		$pdf->SetY($y+274+$mycor+$ajustesy);
		$pdf->Cell(210,0,$codtotn,0,0,'C',FALSE);
	}
	// ---------------------------------------------------------
	$pdf->Output('Listadofacturas.pdf', 'I');//Close and output PDF document
?>