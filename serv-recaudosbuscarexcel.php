<?php  
	require_once 'PHPExcel/Classes/PHPExcel.php';
	require"comun.inc";
	require"funciones.inc";
	session_start();
	$linkbd=conectar_v7();
	$linkbd -> set_charset("utf8");
	$objPHPExcel = new PHPExcel();
	//----Propiedades----
	$objPHPExcel->getProperties()
				->setCreator("SPID")
				->setLastModifiedBy("SPID")
				->setTitle("Exportar Excel con PHP")
				->setSubject("Documento de prueba")
				->setDescription("Documento generado con PHPExcel")
				->setKeywords("usuarios phpexcel")
				->setCategory("reportes");
	//----Cuerpo de Documento----
	$objPHPExcel->getActiveSheet()->mergeCells('A1:F1');
	$objPHPExcel->setActiveSheetIndex(0)
				->setCellValue('A1', 'Listado Recaudos');
	$objFont=$objPHPExcel->getActiveSheet()->getStyle('A1')->getFont();
	$objFont->setName('Courier New'); 
	$objFont->setSize(15); 
	$objFont->setBold(true); 
	$objFont->setUnderline(PHPExcel_Style_Font::UNDERLINE_SINGLE);
	$objFont->getColor()->setARGB( PHPExcel_Style_Color::COLOR_BLACK);
	$objAlign=$objPHPExcel->getActiveSheet()->getStyle('A1')->getAlignment(); 
	$objAlign->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); 
	$objAlign->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER); 
	$objPHPExcel->setActiveSheetIndex(0)
				->setCellValue('A2', 'No RECIBO')
				->setCellValue('B2', 'FACTURA')
				->setCellValue('C2', 'FECHA')
				->setCellValue('D2', 'CLIENTE')
				->setCellValue('E2', 'VALOR')
				->setCellValue('F2', 'ESTADO');
	
	$sqlr="SELECT * FROM servreciboscaja ORDER BY id_recibos DESC";
	$resp = mysqli_query($linkbd,$sqlr);
	$i=3;
	while ($row =mysqli_fetch_row($resp))
	{
		if($row[9]=='S'){$estfac="Activo";}
		else {$estfac="Anulado";}
		$sqlr2="SELECT T1.nombretercero FROM servclientes AS T1 INNER JOIN servliquidaciones AS T2 ON T2.codusuario=T1.codigo WHERE T2.id_liquidacion='$row[4]'";
		$resp2 = mysqli_query($linkbd,$sqlr2);
		$row2 = mysqli_fetch_row($resp2);
		$cliente = $row2[0];
		$objPHPExcel->setActiveSheetIndex(0)
		->setCellValueExplicit ("C".$i,$row[2], PHPExcel_Cell_DataType :: TYPE_STRING)
		->setCellValueExplicit ("D".$i,$cliente, PHPExcel_Cell_DataType :: TYPE_STRING)
		->setCellValueExplicit ("F".$i,$estfac, PHPExcel_Cell_DataType :: TYPE_STRING);
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0,$i,$row[0]);
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1,$i,$row[4]);
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4,$i,$row[8]);
		$i++;
	}
	//----Propiedades de la hoja
	$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true); 
	$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true); 
	$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true); 
	$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true); 
	$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true); 
	$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true); 
	$objPHPExcel->getActiveSheet()->setTitle('Listado Facturas');
	//----Guardar documento----
	header('Content-Type: application/vnd.ms-excel');
	header('Content-Disposition: attachment;filename="Listado Recaudos.xlsx"');
	header('Cache-Control: max-age=0');
	$objWriter=PHPExcel_IOFactory::createWriter($objPHPExcel,'Excel2007');
	$objWriter->save('php://output');
	exit;
?>