<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require "comun.inc";
	require "funciones.inc";
	session_start();
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
	$scroll = $_GET['scrtop'];
	$totreg = $_GET['totreg'];
	$idcta = $_GET['idcta'];
	$altura = $_GET['altura'];
	$filtro = "'".$_GET['filtro']."'";
?>
<!DOCTYPE >
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es">
	<head>
		<meta http-equiv="Content-Type" content="text/html" charset="utf8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=9"/>
		<title>:: IDEAL 10 - Gesti&oacute;n humana</title>
		<link href="css/css2.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<script type="text/javascript" src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script>
			function agregardetalle()
			{
				if(document.form2.funcionamiento.value != '-1' && document.form2.funtemporal.value != '-1' && document.form2.inversion.value != '-1' && document.form2.invtemporal.value != '-1')
				{
					document.form2.agregadet.value=1;
					document.form2.submit();
				}
				else{despliegamodalm('visible','2','Faltan datos para completar el registro');}
			}
			function eliminar(variable)
			{
					document.form2.elimina.value=variable;
					despliegamodalm('visible','4','Esta seguro de eliminar detalle','1');
			}
			function guardar()
			{
				var validacion01=document.getElementById('nombre').value;
				if (document.form2.codigo.value!='' && validacion01.trim()!='' && document.form2.condeta.value >0)
				{despliegamodalm('visible','4','Esta seguro de guardar','2')}
				else{despliegamodalm('visible','2','Faltan datos para completar el registro');}
 			}
			function despliegamodal2(_valor)
			{
				document.getElementById("bgventanamodal2").style.visibility=_valor;
				if(_valor=="hidden"){document.getElementById('ventana2').src="";}
				else 
				{document.getElementById('ventana2').src="scuentasppto-ventana01.php?ti=2";}
			}
			function despliegamodalm(_valor,_tip,mensa,pregunta)
			{
				document.getElementById("bgventanamodalm").style.visibility=_valor;
				if(_valor=="hidden")
				{
					document.getElementById('ventanam').src = "";
					if (document.getElementById('valfocus').value != "0")
					{
						document.getElementById('valfocus').value = '0';
						document.getElementById('cuentap').focus();
						document.getElementById('cuentap').select();
					}
				}
				else
				{
					switch(_tip)
					{
						case "1":	document.getElementById('ventanam').src="ventana-mensaje1.php?titulos="+mensa;break;
						case "2":	document.getElementById('ventanam').src="ventana-mensaje3.php?titulos="+mensa;break;
						case "3":	document.getElementById('ventanam').src="ventana-mensaje2.php?titulos="+mensa;break;
						case "4":	document.getElementById('ventanam').src="ventana-consulta1.php?titulos="+mensa+"&idresp="+pregunta;break;	
					}
				}
			}
			function funcionmensaje(){document.location.href = "";}
			function respuestaconsulta(pregunta)
			{
				switch(pregunta)
				{
					case "1":	//if(document.getElementById('condeta').value=='1'){document.getElementById('bloqueo01').value='0'}
								document.getElementById('oculto').value="6";
								document.form2.submit();break;
					case "2":	document.form2.oculto.value="2";
								document.form2.submit();break;
				}
			}
			function adelante(scrtop, numpag, limreg, filtro, next)
			{
				var maximo = document.getElementById('maximo').value;
				var actual = document.getElementById('codigo').value;
				
				if(parseFloat(maximo)>parseFloat(actual))
				{
					location.href = "hum-editaparafiscalesccpet.php?idr=" + next + "&scrtop=" + scrtop + "&numpag=" + numpag + "&limreg=" + limreg + "&filtro=" + filtro;
				}
			}
			function atrasc(scrtop, numpag, limreg, filtro, prev)
			{
				var minimo = document.getElementById('minimo').value;
				var actual = document.getElementById('codigo').value;
				if(parseFloat(minimo)<parseFloat(actual))
				{
					location.href = "hum-editaparafiscalesccpet.php?idr=" + prev + "&scrtop=" + scrtop + "&numpag=" + numpag + "&limreg=" + limreg + "&filtro=" + filtro;
				}
			}
			function iratras(scrtop, numpag, limreg)
			{
				var idcta = document.getElementById('codigo').value;
				location.href = "hum-buscaparafiscalesccpet.php?idcta=" + idcta + "&scrtop=" + scrtop + "&numpag=" + numpag + "&limreg=" + limreg;
			}
		</script>
		<?php titlepag();?>
	</head>
	<body>
		<IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
		<span id="todastablas2"></span>
		<?php
			$numpag = $_GET['numpag'];
			$limreg = $_GET['limreg'];
			$scrtop = 26 * $totreg;
			function nomconceto($codi,$tipo)
			{
				$linkbd = conectar_v7();
				$linkbd -> set_charset("utf8");
				$sqlr = "SELECT nombre FROM conceptoscontables WHERE codigo = '$codi' AND modulo = '2' AND tipo = '$tipo'";
				$resp = mysqli_query($linkbd,$sqlr);
				$row = mysqli_fetch_row($resp);
				$nombre = "$codi - $row[0] - $tipo";
				return($nombre);
			}
		?>
		<table>
			<tr><script>barra_imagenes("hum");</script><?php cuadro_titulos();?></tr>
			<tr><?php menu_desplegable("hum");?></tr>
			<tr>
				<td colspan="3" class="cinta"><img src="imagenes/add.png" title="Nuevo" onClick="location.href='hum-tablasparafiscalesccpet.php'" class="mgbt"><img src="imagenes/guarda.png" title="Guardar" onClick="guardar();" class="mgbt"><img src="imagenes/busca.png" title="Buscar" onClick="location.href='hum-buscaparafiscalesccpet.php'" class="mgbt"><img src="imagenes/agenda1.png" title="Agenda" onClick="mypop=window.open('plan-agenda.php','','');mypop.focus()" class="mgbt"><img src="imagenes/nv.png" title="Nueva ventana" onClick="mypop=window.open('hum-principal.php','','');mypop.focus();" class="mgbt"><img src="imagenes/duplicar_pantalla.png" title="Duplicar pesta&ntilde;a" onClick="mypop=window.open('<?php echo $url2; ?>','','');mypop.focus();" class="tooltip bottom mgbt"><img src="imagenes/iratras.png" title="Atr&aacute;s"  onClick="iratras(<?php echo $scrtop; ?>, <?php echo $numpag; ?>, <?php echo $limreg; ?>)" class="mgbt"></td>
			</tr>
		</table>
		<div id="bgventanamodalm" class="bgventanamodalm">
			<div id="ventanamodalm" class="ventanamodalm">
				<IFRAME src="" name="ventanam" marginWidth=0 marginHeight=0 frameBorder=0 id="ventanam" frameSpacing=0 style=" width:700px; height:130px; top:200; overflow:hidden;"> 
				</IFRAME>
			</div>
		</div>
		<form name="form2" method="post" action="">
			<input type="hidden" name="valfocus" id="valfocus" value="1"/>
			<?php
				$sqlr = "SELECT MIN(codigo), MAX(codigo) FROM humparafiscalesccpet ORDER BY codigo";
				$res = mysqli_query($linkbd,$sqlr);
				$r = mysqli_fetch_row($res);
				$_POST['minimo'] = $r[0];
				$_POST['maximo'] = $r[1];
				$sqln = "SELECT codigo FROM humparafiscalesccpet WHERE codigo > '".$_GET['idr']."' ORDER BY codigo ASC LIMIT 1";//Siguiente
				$resn = mysqli_query($linkbd,$sqln);
				$row = mysqli_fetch_row($resn);
				$next = "'$row[0]'";
				$sqlp = "SELECT codigo FROM humparafiscalesccpet WHERE codigo < '".$_GET['idr']."' ORDER BY codigo DESC LIMIT 1";//Anterior
				$resp = mysqli_query($linkbd,$sqlp);
				$row = mysqli_fetch_row($resp);
				$prev = "'$row[0]'";
				if(!$_POST['oculto'])
				{
					$sqlr = "SELECT * FROM humparafiscalesccpet WHERE codigo = '".$_GET['idr']."' ";
					$cont = 0;
 					$resp = mysqli_query($linkbd,$sqlr);
					while ($row = mysqli_fetch_row($resp))
					{
						$_POST['codigo'] = $row[1];
						$_POST['nombre'] = $row[2];
						$_POST['tipo'] = $row[3];
						$_POST['porcentaje'] = $row[4];
						$cont = $cont + 1; 
					}
					$_POST['idfuncionamiento'] = array();
					$_POST['nomfuncionamiento'] = array();
					$_POST['idfuncionamientotemp'] = array();
					$_POST['nomfuncionamientotemp'] = array();
					$_POST['idinversion'] = array();
					$_POST['nominversion'] = array();
					$_POST['idinversiontemp'] = array();
					$_POST['nominversiontemp'] = array();
					$_POST['idgastoscomerc'] = array();
					$_POST['nomgastoscomerc'] = array();
					$_POST['idgastoscomerctemp'] = array();
					$_POST['nomgastoscomerctemp'] = array();
					$_POST['idsector'] = array();
					$sqlr="SELECT * FROM humparafiscalesccpet_det WHERE codigo = '".$_GET['idr']."'";
					$res = mysqli_query($linkbd,$sqlr);
					$cont = 0;
					while ($row = mysqli_fetch_row($res))
					{
						$_POST['idfuncionamiento'][] = $row[2];
						$_POST['nomfuncionamiento'][] = nomconceto($row[2],'F');
						$_POST['idfuncionamientotemp'][] = $row[3];
						$_POST['nomfuncionamientotemp'][] = nomconceto($row[3],'FT');
						$_POST['idinversion'][] = $row[4];
						$_POST['nominversion'][] = nomconceto($row[4],'IN');
						$_POST['idinversiontemp'][] = $row[5];
						$_POST['nominversiontemp'][] = nomconceto($row[5],'IT');
						$_POST['idgastoscomerc'][] = $row[6];
						$_POST['nomgastoscomerc'][] = nomconceto($row[6],'CP');
						$_POST['idgastoscomerctem'][] = $row[7];
						$_POST['nomgastoscomerctem'][] = nomconceto($row[7],'CT');
						$_POST['idsector'][] = $row[8];
						$cont = $cont + 1;
					}
				}
			?>
			<table class="inicio ancho">
				<tr>
					<td class="titulos" colspan="8">.: Agregar Parafiscal</td>
					<td class="cerrar" style="width:7%;"><a href="hum-principal.php">&nbsp;Cerrar</a></td>
				</tr>
				<tr>
					<td class="saludo1" style="width:7%;">C&oacute;digo:</td>
					<td style="width:10%;"><img src="imagenes/back.png" onClick="atrasc(<?php echo "$scrtop, $numpag, $limreg, $filtro, $prev";?>)" class="icobut" title="Anterior">&nbsp;<input type="text" name="codigo" id="codigo" value="<?php echo @ $_POST['codigo']?>" maxlength="2"  onKeyPress="javascript:return solonumeros(event)" onKeyUp="return tabular(event,this)" style="width:35%;">&nbsp;<img src="imagenes/next.png" onClick="adelante(<?php echo "$scrtop, $numpag, $limreg, $filtro, $next"?>);" class="icobut" title="Sigiente"></td>
					<td class="saludo1" style="width:7%;">Nombre:</td>
					<td style="width:35%;"><input type="text" name="nombre" id="nombre" value="<?php echo @ $_POST['nombre']?>" onKeyUp="return tabular(event,this)" style="width:99%;"></td>
					<td class="saludo1" style="width:2%;">%</td>
					<td style="width:8%;"><input type="text" name="porcentaje" id="porcentaje" value="<?php echo @ $_POST['porcentaje']?>" onKeyUp="return tabular(event,this)" onKeyPress="javascript:return solonumeros(event)" style="width:90%;"></td>
					<td class="saludo1" style="width:7%;">Tipo:</td>
					<td>
						<select name="tipo" id="tipo" >
							<option value=''>Seleccione</option>
							<option value='D' <?php if($_POST['tipo'] == 'D') echo 'SELECTED'; ?>>Descuento</option>
							<option value='A' <?php if($_POST['tipo'] == 'A') echo 'SELECTED'; ?>>Aporte</option>
						</select>
					</td>
				</tr>
			</table>
			<input type="hidden" name="maximo" id="maximo" value="<?php echo @ $_POST['maximo']?>"/>
			<input type="hidden" name="minimo" id="minimo" value="<?php echo @ $_POST['minimo']?>"/>
			<input type="hidden" name="oculto" id="oculto" value="1">
			<table class="inicio ancho">
				<tr><td colspan='8' class='titulos'>Agregar detalle variable de pago</td></tr>
				<tr>
					<td class="tamano01" style="width: 4cm;">Funcionamiento: </td>
					<td style="width: 25%;">
						<select name="funcionamiento" id="funcionamiento" onChange="validar();" style="width:100%;" class="tamano02">
							<option value="-1">Seleccione...</option>
							<?php
								$sqlr="SELECT codigo, nombre, tipo FROM conceptoscontables WHERE modulo = '2' AND tipo = 'F' ORDER BY codigo";
								$resp = mysqli_query($linkbd,$sqlr);
								while ($row =mysqli_fetch_row($resp))
								{
									if($row[0]==$_POST['funcionamiento'])
									{
										echo "<option value='$row[0]' SELECTED>$row[0] - $row[2] - $row[1]</option>";
										$_POST['funcionamientonom']="$row[0] - $row[2] - $row[1]";
									}
									else{echo "<option value='$row[0]'>$row[0] - $row[2] - $row[1]</option>";}
								}
							?>
						</select>
						<input type="hidden" name="funcionamientonom" id="funcionamientonom" value="<?php echo $_POST['funcionamientonom']?>">
					</td>
					<td class="tamano01" style="width: 4.5cm;">Funcionamiento Temporal: </td>
					<td style="width: 25%;">
						<select name="funtemporal" id="funtemporal" onChange="validar();" style="width:100%;" class="tamano02">
							<option value="-1">Seleccione...</option>
							<?php
								$sqlr = "SELECT codigo, nombre, tipo FROM conceptoscontables WHERE modulo = '2' AND tipo = 'FT' ORDER BY codigo";
								$resp = mysqli_query($linkbd,$sqlr);
								while ($row =mysqli_fetch_row($resp))
								{
									if($row[0] == $_POST['funtemporal'])
									{
										echo "<option value='$row[0]' SELECTED>$row[0] - $row[2] - $row[1]</option>";
										$_POST['funtemporalnom']="$row[0] - $row[2] - $row[1]";
									}
									else{echo "<option value='$row[0]'>$row[0] - $row[2] - $row[1]</option>";}
								}
							?>
						</select>
						<input type="hidden" name="funtemporalnom" id="funtemporalnom" value="<?php echo $_POST['funtemporalnom']?>">	
					</td>
					<td class="tamano01" style="width:6%;">Sector:</td>
					<td>
						<select name='sector' id="sector">
							<option value='N/A' <?php if(@ $_POST['sector']=='N/A') echo 'SELECTED'; ?>>N/A</option>
							<option value='publico' <?php if(@ $_POST['sector']=='publico') echo 'SELECTED'; ?>>Publico</option>
							<option value='privado' <?php if(@ $_POST['sector']=='privado') echo 'SELECTED'; ?>>Privado</option>
						</select>
					</td>
				</tr>
				<tr>
					<td class="tamano01" >Inversi&oacute;n: </td>
					<td>
						<select name="inversion" id="inversion" onChange="validar();" style="width:100%;" class="tamano02">
							<option value="-1">Seleccione...</option>
							<?php
								$sqlr="SELECT codigo, nombre, tipo FROM conceptoscontables WHERE modulo='2' AND tipo='IN' ORDER BY codigo";
								$resp = mysqli_query($linkbd,$sqlr);
								while ($row = mysqli_fetch_row($resp))
								{
									if($row[0] == $_POST['inversion'])
									{
										echo "<option value='$row[0]' SELECTED>$row[0] - $row[2] - $row[1]</option>";
										$_POST['inversionnom'] = "$row[0] - $row[2] - $row[1]";
									}
									else{echo "<option value='$row[0]'>$row[0] - $row[2] - $row[1]</option>";}
								}
							?>
						</select>
						<input type="hidden" name="inversionnom" id="inversionnom" value="<?php echo @ $_POST['inversionnom']?>">
					</td>
					<td class="tamano01">Inversi&oacute;n Temporal: </td>
					<td>
						<select name="invtemporal" id="invtemporal" onChange="validar();" style="width:100%;" class="tamano02">
							<option value="-1">Seleccione...</option>
							<?php
								$sqlr="SELECT codigo, nombre, tipo FROM conceptoscontables WHERE modulo='2' AND tipo='IT' ORDER BY codigo";
								$resp = mysqli_query($linkbd,$sqlr);
								while ($row =mysqli_fetch_row($resp))
								{
									if($row[0]==$_POST['invtemporal'])
									{
										echo "<option value='$row[0]' SELECTED>$row[0] - $row[2] - $row[1]</option>";
										$_POST['invtemporalnom'] = "$row[0] - $row[2] - $row[1]";
									}
									else{echo "<option value='$row[0]'>$row[0] - $row[2] - $row[1]</option>";}
								}
							?>
						</select>
						<input type="hidden" name="invtemporalnom" id="invtemporalnom" value="<?php echo @ $_POST['invtemporalnom']?>">
					</td>
				</tr>
				<tr>
					<td class="tamano01" >Gastos Comercialización </td>
					<td>
						<select name="gastoscomer" id="gastoscomer" onChange="validar();" style="width:100%;" class="tamano02">
							<option value="-1">Seleccione...</option>
							<?php
								$sqlr = "SELECT codigo, nombre, tipo FROM conceptoscontables WHERE modulo = '2' AND tipo = 'CP' ORDER BY codigo";
								$resp = mysqli_query($linkbd,$sqlr);
								while ($row = mysqli_fetch_row($resp))
								{
									if($row[0] == $_POST['gastoscomer'])
									{
										echo "<option value='$row[0]' SELECTED>$row[0] - $row[2] - $row[1]</option>";
										$_POST['gastoscomernom'] = "$row[0] - $row[2] - $row[1]";
									}
									else{echo "<option value='$row[0]'>$row[0] - $row[2] - $row[1]</option>";}
								}
							?>
						</select>
						<input type="hidden" name="gastoscomernom" id="gastoscomernom" value="<?php echo @ $_POST['gastoscomernom']?>">
					</td>
					<td class="tamano01">Gastos Comercialización Temporal: </td>
					<td>
						<select name="gastoscomertem" id="gastoscomertem" onChange="validar();" style="width:100%;" class="tamano02">
							<option value="-1">Seleccione...</option>
							<?php
								$sqlr="SELECT codigo, nombre, tipo FROM conceptoscontables WHERE modulo = '2' AND tipo = 'CT' ORDER BY codigo";
								$resp = mysqli_query($linkbd,$sqlr);
								while ($row = mysqli_fetch_row($resp))
								{
									if($row[0]==$_POST['gastoscomertem'])
									{
										echo "<option value='$row[0]' SELECTED>$row[0] - $row[2] - $row[1]</option>";
										$_POST['gastoscomertemnom'] = "$row[0] - $row[2] - $row[1]";
									}
									else{echo "<option value='$row[0]'>$row[0] - $row[2] - $row[1]</option>";}
								}
							?>
						</select>
						<input type="hidden" name="gastoscomertemnom" id="gastoscomertemnom" value="<?php echo @ $_POST['gastoscomertemnom']?>">
					</td>
					<td style="padding-bottom:0px" colspan="2"><em class="botonflecha" onClick="agregardetalle();">Agregar</em></td>
				</tr>
			</table>
			<div class="subpantalla" style="height:43.5%; width:99.6%; overflow-x:hidden;">
				<table  class="inicio ancho">
					<tr><td class="titulos" colspan="10">Detalle Variable - Parametrizacion</td></tr>
					<tr class="titulos2">
						<td style='width:15%;'>Funcionamiento</td>
						<td style='width:15%;'>Funcionamiento Temporal</td>
						<td style='width:15%;'>Inversi&oacute;n</td>
						<td style='width:15%;'>Inversi&oacute;n Temporal</td>
						<td style='width:15%;'>Gastos Comercialización</td>
						<td style='width:15%;'>Gastos Comercialización Temporal</td>
						<td>Sector</td>
						<td style='width:5%;'>Eliminar<input type='hidden' name='elimina' id='elimina'></td>
					</tr>
					<?php
						if ($_POST['oculto']=='6')
						{ 
							$posi=$_POST['elimina'];
							unset($_POST['idfuncionamiento'][$posi]);
							unset($_POST['nomfuncionamiento'][$posi]);
							unset($_POST['idfuncionamientotemp'][$posi]);
							unset($_POST['nomfuncionamientotemp'][$posi]);
							unset($_POST['idinversion'][$posi]);
							unset($_POST['nominversion'][$posi]);
							unset($_POST['idinversiontemp'][$posi]);
							unset($_POST['nominversiontemp'][$posi]);
							unset($_POST['idgastoscomerc'][$posi]);
							unset($_POST['nomgastoscomerc'][$posi]);
							unset($_POST['idgastoscomerctem'][$posi]);
							unset($_POST['nomgastoscomerctem'][$posi]);
							unset($_POST['idsector'][$posi]);
							$_POST['idfuncionamiento'] = array_values($_POST['idfuncionamiento']);
							$_POST['nomfuncionamiento'] = array_values($_POST['nomfuncionamiento']);
							$_POST['idfuncionamientotemp'] = array_values($_POST['idfuncionamientotemp']);
							$_POST['nomfuncionamientotemp'] = array_values($_POST['nomfuncionamientotemp']);
							$_POST['idinversion'] = array_values($_POST['idinversion']);
							$_POST['nominversion'] = array_values($_POST['nominversion']);
							$_POST['idinversiontemp'] = array_values($_POST['idinversiontemp']); 
							$_POST['nominversiontemp'] = array_values($_POST['nominversiontemp']);
							$_POST['idgastoscomerc'] = array_values($_POST['idgastoscomerc']);
							$_POST['nomgastoscomerc'] = array_values($_POST['nomgastoscomerc']);
							$_POST['idgastoscomerctem'] = array_values($_POST['idgastoscomerctem']); 
							$_POST['nomgastoscomerctem'] = array_values($_POST['nomgastoscomerctem']);
						}
						if ($_POST['agregadet']=='1')
						{
							$_POST['idfuncionamiento'][]=$_POST['funcionamiento'];
							$_POST['nomfuncionamiento'][]=$_POST['funcionamientonom'];
							$_POST['idfuncionamientotemp'][]=$_POST['funtemporal'];
							$_POST['nomfuncionamientotemp'][]=$_POST['funtemporalnom'];
							$_POST['idinversion'][]=$_POST['inversion'];
							$_POST['nominversion'][]=$_POST['inversionnom'];
							$_POST['idinversiontemp'][]=$_POST['invtemporal'];
							$_POST['nominversiontemp'][]=$_POST['invtemporalnom'];
							$_POST['idgastoscomerc'][] = $_POST['gastoscomer'];
							$_POST['nomgastoscomerc'][] = $_POST['gastoscomernom'];
							$_POST['idgastoscomerctem'][] = $_POST['gastoscomertem'];
							$_POST['nomgastoscomerctem'][] = $_POST['gastoscomertemnom'];
							$_POST['idsector'][]=$_POST['sector'];
							echo"
							<script>
								document.form2.funcionamiento.value='-1';
								document.form2.funcionamientonom.value='';
								document.form2.funtemporal.value='-1';
								document.form2.funtemporalnom.value='';
								document.form2.inversion.value='-1';
								document.form2.inversionnom.value='';
								document.form2.invtemporal.value='-1';
								document.form2.invtemporalnom.value='';
								document.form2.gastoscomer.value='-1';
								document.form2.gastoscomernom.value='';
								document.form2.gastoscomertem.value='-1';
								document.form2.gastoscomertemnom.value='';
								document.form2.sector.value='N/A';
							</script>";
						}
						$iter = 'saludo1a';
						$iter2 = 'saludo2';
						$_POST['condeta'] = $cdtll = count($_POST['idfuncionamiento']);
						for ($x=0;$x< $cdtll;$x++)
						{
							echo"
							<input type='hidden' name='idfuncionamiento[]' value='".$_POST['idfuncionamiento'][$x]."'/>
							<input type='hidden' name='nomfuncionamiento[]' value='".$_POST['nomfuncionamiento'][$x]."'/>
							<input type='hidden' name='idfuncionamientotemp[]' value='".$_POST['idfuncionamientotemp'][$x]."'/>
							<input type='hidden' name='nomfuncionamientotemp[]' value='".$_POST['nomfuncionamientotemp'][$x]."'/>
							<input type='hidden' name='idinversion[]' value='".$_POST['idinversion'][$x]."'/>
							<input type='hidden' name='nominversion[]' value='".$_POST['nominversion'][$x]."'/>
							<input type='hidden' name='idinversiontemp[]' value='".$_POST['idinversiontemp'][$x]."'/>
							<input type='hidden' name='nominversiontemp[]' value='".$_POST['nominversiontemp'][$x]."'/>
							<input type='hidden' name='idgastoscomerc[]' value='".$_POST['idgastoscomerc'][$x]."'/>
							<input type='hidden' name='nomgastoscomerc[]' value='".$_POST['nomgastoscomerc'][$x]."'/>
							<input type='hidden' name='idgastoscomerctem[]' value='".$_POST['idgastoscomerctem'][$x]."'/>
							<input type='hidden' name='nomgastoscomerctem[]' value='".$_POST['nomgastoscomerctem'][$x]."'/>
							<input type='hidden' name='idsector[]' value='".$_POST['idsector'][$x]."'/>
							<tr class='$iter'>
								<td>".$_POST['nomfuncionamiento'][$x]."</td>
								<td>".$_POST['nomfuncionamientotemp'][$x]."</td>
								<td>".$_POST['nominversion'][$x]."</td>
								<td>".$_POST['nominversiontemp'][$x]."</td>
								<td>".$_POST['nomgastoscomerc'][$x]."</td>
								<td>".$_POST['nomgastoscomerctem'][$x]."</td>
								<td>".$_POST['idsector'][$x]."</td>
								<td><a href='#' onclick='eliminar($x)'><img src='imagenes/del.png'></a></td>
							</tr>";
							$aux=$iter;
							$iter=$iter2;
							$iter2=$aux;
						}
					?>
				</table>
			</div>
			<input type="hidden" name="agregadet" value="0">
			<input type="hidden" name="condeta" id="condeta" value="<?php echo $_POST['condeta'];?>"/>
			<?php
			if($_POST['oculto'] == '2')
			{
				if ($_POST['nombre'] != "")
				{
					$nr="1";
					
					$sqlr="UPDATE humparafiscalesccpet SET nombre = '".$_POST['nombre']."', tipo = '".$_POST['tipo']."', porcentaje = '".$_POST['porcentaje']."' WHERE codigo = '".$_POST['codigo']."'";
					if (!mysqli_query($linkbd,$sqlr))
					{
						despliegamodalm('visible','2','Manejador de errores BD, No se pudo ejecutar la petición humparafiscales');
					}
					else
					{
						$sqlr="DELETE FROM humparafiscalesccpet_det WHERE codigo='".$_POST['codigo']."'";
						mysqli_query($linkbd,$sqlr);
						for($x = 0; $x < count($_POST['idfuncionamiento']); $x++)
						{
							$numid_det = selconsecutivo('humparafiscalesccpet_det','id_det');
							$sqlr="INSERT INTO humparafiscalesccpet_det (id_det, codigo, funcionamiento, funcionamientotemp, inversion, inversiontemp, gastoscomerc, gastoscomerctemp, sector, estado)VALUES ('$numid_det', '".$_POST['codigo']."', '".$_POST['idfuncionamiento'][$x]."', '".$_POST['idfuncionamientotemp'][$x]."', '".$_POST['idinversion'][$x]."', '".$_POST['idinversiontemp'][$x]."','".$_POST['idgastoscomerc'][$x]."', '".$_POST['idgastoscomerctem'][$x]."','".$_POST['idsector'][$x]."','S')";echo $sqlr;
							if (!mysqli_query($linkbd,$sqlr))
							{despliegamodalm('visible','2','Manejador de errores BD, No se pudo ejecutar la petición humparafiscales_det');}
							$c = $c + 1;
						}
						?>
						<script>
						despliegamodalm("visible","3","Se ha almacenado registros en el detalle de la variable con éxito")
						</script>;
						<?php
					}
				}
				else {despliegamodalm('visible','2','Falta información para crear la variable');}
				$_POST['oculto']='';
			}
			?>
			<div id="bgventanamodal2">
				<div id="ventanamodal2">
					<IFRAME  src="" name="buster" marginWidth=0 marginHeight=0 frameBorder=0 id="ventana2" frameSpacing=0 style="left:500px; width:900px; height:500px; top:200;"> 
					</IFRAME>
				</div>
			</div>
		</form>
	</body>
</html>
