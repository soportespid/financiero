<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=uft8");
	require "comun.inc";
	require "funciones.inc";
    require "funcionesSP.inc.php";
	session_start();
	$linkbd=conectar_v7();
	$linkbd -> set_charset("utf8");
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es">
	<head>
		<meta http-equiv="Content-Type" content="text/html" charset="utf8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=9"/>
		<title>:: IDEAL 10 - Servicios Públicos</title>
		<link href="css/css2.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
        <link href="css/cssSP.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<script type="text/javascript" src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
        <script type="text/javascript" src="jquery-1.11.0.min.js"></script> 
		<script src="sweetalert2/dist/sweetalert2.min.js"></script>
		<link rel="stylesheet" href="sweetalert2/dist/sweetalert2.min.css">
        
		<script>
			$(window).load(function () {
				$('#cargando').hide();
			});

			function generarfacturas()
			{
				var corte = parseInt(document.getElementById('corte').value);

				if((corte != '-1')) {
					document.form2.oculto.value='2';
					document.form2.submit();
				}
				else {
					Swal.fire("Error", "Debes seleccionar un corte liquidado", "error");
				}
			}

			function actualizar()
			{
				document.form2.submit();
			}

			function excell()
			{
				document.form2.action="serv-excel-reporte-suspensiones.php";
				document.form2.target="_BLANK";
				document.form2.submit();
			}
		</script> 
		<?php titlepag();?>
	</head>
	<body>
        <IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
		<span id="todastablas2"></span>
		<table>
			<tr><script>barra_imagenes("serv");</script><?php cuadro_titulos();?></tr>	 
			<tr><?php menu_desplegable("serv");?></tr>
			<tr>
				<td colspan="3" class="cinta">
					<a class="mgbt"><img src="imagenes/add2.png"/></a>
					<a class="mgbt"><img src="imagenes/guardad.png" title="Guardar"/></a>
					<a href="" class="mgbt"><img src="imagenes/busca.png" title="Buscar"/></a>
					<a onclick="mypop=window.open('plan-agenda.php','','');mypop.focus()" class="mgbt"><img src="imagenes/agenda1.png" title="Agenda" /></a>
					<a onclick="mypop=window.open('serv-principal.php','','');mypop.focus();" class="mgbt"><img src="imagenes/nv.png" title="Nueva ventana"></a>
					<a onclick="mypop=window.open('<?php echo $url2; ?>','','');mypop.focus();" class="mgbt"><img src="imagenes/duplicar_pantalla.png" title="Duplicar pesta&ntilde;a"></a>
					<a href=""><img src="imagenes/excel.png" title="Excel" onClick="excell()" class="mgbt"></a>
					<a href="serv-menuInformes"><img src="imagenes/iratras.png" class="mgbt" alt="Atrás"></a>
                </td>
			</tr>
		</table>

		<form name="form2" method="post">

			<?php
				$_POST['fecha'] = date('d-m-Y'); 
			?>

			<table class="inicio ancho" style="width:99.5%">
				<tr>
					<td class="titulos" colspan="5">Reporte predios con mora</td>
					<td class="cerrar" style="width:7%" onClick="location.href='serv-principal.php'">Cerrar</td>
				</tr>
				
				<tr>
					<td class="tamano01" style="width: 3cm;">Fecha:</td>
					<td style="width: 10%;">
						<input type="text" name="fecha" id="fecha" value="<?php echo $_POST['fecha'] ?>" style="text-align: center;" readonly>
					</td>

					<td class="tamano01" style="width: 3cm;">Corte Liquidado:</td>
                    <td style="">
                        <select name="corte" id="corte" class="centrarSelect" style="width: 100%;" onchange="actualizar();">
                            <option value="-1" class="aumentarTamaño">SELECCIONE CORTE</option>
                            <?php
                                $sql = "SET lc_time_names = 'es_ES'";
                                mysqli_query($linkbd,$sql);

								$sqlr = "SELECT numero_corte, UPPER(MONTHNAME(fecha_inicial)), UPPER(MONTHNAME(fecha_final)), YEAR(fecha_inicial), YEAR(fecha_final) FROM srvcortes WHERE numero_corte > 0 ORDER BY numero_corte DESC";
								$resp = mysqli_query($linkbd,$sqlr);
								while ($row = mysqli_fetch_row($resp))
								{
									if(@ $_POST['corte'] == $row[0])
									{
										echo "<option class='aumentarOption' value='$row[0]' SELECTED>$row[0]:  $row[1] $row[3] - $row[2] $row[4]</option>";
									}
									else{echo "<option class='aumentarOption' value='$row[0]'>$row[0]: $row[1]  $row[3] - $row[2] $row[4]</option>";}
								}
							?>
                        </select>
                    </td>

					<td style="padding-bottom:0px;height:35px;"><em class="botonflecha" onclick="generarfacturas()">Generar Reporte</em></td>
					
				</tr>
			</table>

            <div id="cargando" style=" position:absolute;left: 46%; bottom: 45%">
				<img src="imagenes/loading.gif" style=" width: 80px; height: 80px"/>
			</div>

			<div class="subpantalla" style="height:60%; width:99.2%;">
				<table class='inicio' align='center' width='99%'>
					<?php
						if(@ $_POST['oculto']=="2")
						{
							$iter  = 'saludo1a';
							$iter2 = 'saludo2';
					?>

					<tr>
						<td colspan='90' class='titulos'>Resultados del reporte: </td>
					</tr>
				
					<tr class='titulos2' style='text-align:center;'>
						<td style="width: 8%;">Código de usuario </td>
						<td style="width: 8%;">Documento </td>
						<td style="width: 30%;">Nombre completo </td>
						<td style="width: 15%;">Barrio </td>
						<td style="width: 25%;">Dirección </td>
						<td style="width: 10%;">Cantidad de facturas con mora </td>
						<td style="width: 10%;">Ultima factura </td>
						<td style="width: 10%;">Total deuda </td>
					</tr>
					<?php
							$corte = $_POST['corte'];

							$sqlFiltro = "SELECT C.cod_usuario, C.id_tercero, DC.direccion, CD.id_cliente, CD.numero_facturacion, C.id_barrio FROM srvcortes_detalle AS CD INNER JOIN srvclientes AS C ON CD.id_cliente = C.id INNER JOIN srvdireccion_cliente AS DC ON CD.id_cliente = DC.id_cliente WHERE CD.id_corte = $corte AND (CD.estado_pago = 'S' OR CD.estado_pago = 'V')";
							$resFiltro = mysqli_query($linkbd, $sqlFiltro);
							while ($rowFiltro = mysqli_fetch_row($resFiltro)) {

								$sqlTercero = "SELECT nombre1, nombre2, apellido1, apellido2, razonsocial, cedulanit FROM terceros WHERE id_tercero = $rowFiltro[1]";
								$resTercero = mysqli_query($linkbd,$sqlTercero);
								$rowTercero = mysqli_fetch_assoc($resTercero);

								if($rowTercero['razonsocial'] == '') {
									$nombre = $rowTercero['nombre1']. ' ' .$rowTercero['nombre2']. ' ' .$rowTercero['apellido1']. ' ' .$rowTercero['apellido2'];
								} else {
									$nombre = $rowTercero['razonsocial'];
								}

								$nombre = Quitar_Espacios($nombre);
								$documento = $rowTercero['cedulanit'];

								$sqlUltPago = "SELECT id FROM srvcortes_detalle WHERE id_cliente = $rowFiltro[3] AND estado_pago = 'P' ORDER BY id DESC LIMIT 1";
								$resUltPago = mysqli_query($linkbd, $sqlUltPago);
								$rowUltPago = mysqli_fetch_row($resUltPago); 

								if ($rowUltPago[0] <> "") {

									$sqlMora = "SELECT estado_pago FROM srvcortes_detalle WHERE id_cliente = $rowFiltro[3] AND (estado_pago = 'V' OR estado_pago = 'S') AND id > $rowUltPago[0]";
									$resMora = mysqli_query($linkbd, $sqlMora);
									$mesesVencidos = mysqli_num_rows($resMora);
								}
								else {
									$sqlMora = "SELECT estado_pago FROM srvcortes_detalle WHERE id_cliente = $rowFiltro[3] AND (estado_pago = 'V' OR estado_pago = 'S') ";
									$resMora = mysqli_query($linkbd, $sqlMora);
									$mesesVencidos = mysqli_num_rows($resMora);
								}

								$sqlDetallesFactura = "SELECT SUM(credito), SUM(debito) FROM srvdetalles_facturacion WHERE numero_facturacion = $rowFiltro[4] AND (tipo_movimiento = '101' OR tipo_movimiento = '301')";
								$rowDetallesFactura = mysqli_fetch_row(mysqli_query($linkbd, $sqlDetallesFactura));

								$totalDeuda = $rowDetallesFactura[0] - $rowDetallesFactura[1];
								$total = round($totalDeuda);

								$sqlBarrio = "SELECT nombre FROM srvbarrios WHERE id = $rowFiltro[5]";
								$rowBarrio = mysqli_fetch_row(mysqli_query($linkbd, $sqlBarrio));
								echo "
								<input type='hidden' name='codUsuario[]' value='$rowFiltro[0]'>
								<input type='hidden' name='documento[]' value='$documento'>
								<input type='hidden' name='nombre[]' value='$nombre'>
								<input type='hidden' name='barrio[]' value='$rowBarrio[0]'>
								<input type='hidden' name='direccion[]' value='$rowFiltro[2]'>
								<input type='hidden' name='numFacturas[]' value='$mesesVencidos'>
								<input type='hidden' name='ultimaFactura[]' value='$rowFiltro[4]'>
								<input type='hidden' name='valorDeuda[]' value='$total'> 
							";
						?>
									<tr class='<?php echo $iter ?>' style='text-align:center; text-transform:uppercase;'>
										<td> <?php echo $rowFiltro[0] ?> </td>
										<td> <?php echo $documento ?> </td>
										<td> <?php echo $nombre ?> </td>
										<td> <?php echo $rowBarrio[0] ?> </td>
										<td> <?php echo $rowFiltro[2] ?> </td>
										<td> <?php echo $mesesVencidos ?> </td>
										<td> <?php echo $rowFiltro[4] ?> </td>
										<td> <?php echo number_format(round($totalDeuda),2,',','.') ?> </td>
									</tr>
						<?php
								$aux = $iter;
								$iter = $iter2;
								$iter2 = $aux;
							}
						}
					
					?>
				</table>
			</div>

			<input type="hidden" name="oculto" id="oculto" value="1"/>
		</form>
	</body>
</html>