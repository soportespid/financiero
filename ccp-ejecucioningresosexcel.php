<?php  
	require_once 'PHPExcel/Classes/PHPExcel.php';
	require "comun.inc";
	require "funciones.inc";
	session_start();
	$linkbd=conectar_v7(); 
	$linkbd -> set_charset("utf8"); 
	$objPHPExcel = new PHPExcel();
	//----Propiedades----
	$objPHPExcel->getProperties()
        ->setCreator("SPID")
        ->setLastModifiedBy("SPID")
        ->setTitle("Reporte Ejecucion Presupuestal Ingresos")
        ->setSubject("Presupuesto")
        ->setDescription("Presupuesto")
        ->setKeywords("Presupuesto")
        ->setCategory("Presupuesto");
	//----Cuerpo de Documento----
	$objPHPExcel->getActiveSheet()->mergeCells('A1:Q1');
	$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A1', 'Ejecucion presupuestal ingresos');

	$objFont=$objPHPExcel->getActiveSheet()->getStyle('A1')->getFont();
	$objFont->setName('Courier New'); 
	$objFont->setSize(15); 
	$objFont->setBold(true); 
	$objFont->setUnderline(PHPExcel_Style_Font::UNDERLINE_SINGLE);
	$objFont->getColor()->setARGB( PHPExcel_Style_Color::COLOR_BLACK);

	$objAlign=$objPHPExcel->getActiveSheet()->getStyle('A1')->getAlignment(); 
	$objAlign->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); 
	$objAlign->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER); 
	$objPHPExcel-> getActiveSheet ()
		-> getStyle ("A2:Q2")	
        -> getFill ()
        -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
        -> getStartColor ()
        -> setRGB ('A6E5F3');
	$objPHPExcel-> getActiveSheet ()
		-> getStyle ("A1")	
        -> getFill ()
        -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
        -> getStartColor ()
        -> setRGB ('A6E5F3');
	$borders = array(
      'borders' => array(
        'allborders' => array(
          'style' => PHPExcel_Style_Border::BORDER_THIN,
          'color' => array('argb' => 'FF000000'),
        )
      ),
	);
	
	$styleArray = array(
		"font"  => array(
		"bold" => false,
		"color" => array("rgb" => "FF0000"),
		"size"  => 12,
		"name" => "Verdana"
		));
	$objPHPExcel->getActiveSheet()->getStyle('A2:Q2')->applyFromArray($borders);
	$objPHPExcel->setActiveSheetIndex(0)
	->setCellValue('A2', 'MEDIO PAGO')
	->setCellValue('B2', 'UNIDAD')
	->setCellValue('C2', 'FUENTE')
	->setCellValue('D2', 'CODIGO')
	->setCellValue('E2', 'CUIN/CLASIFICADOR')
	->setCellValue('F2', 'BIENES/SERVICIOS')
	->setCellValue('G2', 'NOMBRE')
	->setCellValue('H2', 'PRESUPUESTO INICIAL')
	->setCellValue('I2', 'ADICION')
	->setCellValue('J2', 'REDUCCION')
	->setCellValue('K2', 'DEFINITIVO')
	->setCellValue('L2', 'SUPERAVIT FISCAL')
	->setCellValue('M2', 'RECAUDOS ANTERIORES')
	->setCellValue('N2', 'RECAUDOS DE CONSULTA')
	->setCellValue('O2', 'TOTAL RECAUDOS')
	->setCellValue('P2', 'SALDO POR RECAUDAR')
	->setCellValue('Q2', 'EN EJECUCION');
	
	$i=3;
	//echo count($_POST[vigencia])."holaaa";
	$crit = '';
    if($_POST['unidadEjecutora'] != ''){
        $crit = "AND unidad = '$_POST[unidadEjecutora]'";
    }

    $crit1 = '';
    if($_POST['medioDePago'] != ''){
        $crit1 = "AND medioPago = '$_POST[medioDePago]'";
	}
	
	preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST['fechaini'],$fecha);
	$fechaIni=''.$fecha[3].'-'.$fecha[2].'-'.$fecha[1].'';

	preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST['fechafin'],$fechaf);
	$fechaFin=''.$fechaf[3].'-'.$fechaf[2].'-'.$fechaf[1].'';

	$vigencia = $fecha[3];


    for($xx=0; $xx<count($_POST['codigo']); $xx++)
    {
		$valor = $_POST['presupuestoInicial'][$xx];

		$recaudoPorCuenta = generaRecaudoCcpet($_POST['codigo'][$xx],$vigencia,$vigencia,$fechaIni,$fechaFin);

		$totalRecaudoCuenta = $recaudoPorCuenta;

		$saldoPorRecaudar = $_POST['presupuestoDefinitivo'][$xx] - $totalRecaudoCuenta;
		$enEjecucion = round((($totalRecaudoCuenta/$_POST['presupuestoDefinitivo'][$xx])*100),0);

		

        $objPHPExcel->setActiveSheetIndex(0)
		->setCellValueExplicit ("D$i", $_POST['codigo'][$xx], PHPExcel_Cell_DataType :: TYPE_STRING)
		->setCellValueExplicit ("G$i", $_POST['nombre'][$xx], PHPExcel_Cell_DataType :: TYPE_STRING)
		->setCellValueExplicit ("H$i", $valor, PHPExcel_Cell_DataType :: TYPE_NUMERIC)
		->setCellValueExplicit ("I$i", $_POST['adicion'][$xx], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
		->setCellValueExplicit ("J$i", $_POST['reduccion'][$xx], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
		->setCellValueExplicit ("K$i", $_POST['presupuestoDefinitivo'][$xx], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
		->setCellValueExplicit ("L$i", 0, PHPExcel_Cell_DataType :: TYPE_NUMERIC)
		->setCellValueExplicit ("M$i", 0, PHPExcel_Cell_DataType :: TYPE_NUMERIC)
		->setCellValueExplicit ("N$i", $recaudoPorCuenta, PHPExcel_Cell_DataType :: TYPE_NUMERIC)
		->setCellValueExplicit ("O$i", $totalRecaudoCuenta, PHPExcel_Cell_DataType :: TYPE_NUMERIC)
		->setCellValueExplicit ("P$i", $saldoPorRecaudar, PHPExcel_Cell_DataType :: TYPE_NUMERIC)
		->setCellValueExplicit ("Q$i", $enEjecucion, PHPExcel_Cell_DataType :: TYPE_NUMERIC);
		if($_POST['tipo'][$xx] == 'A'){
			$objPHPExcel->getActiveSheet()->getStyle("A$i:Q$i")->getFont()->setBold(true);
		}else{
			$objPHPExcel->getActiveSheet()->getStyle("A$i:Q$i");
		}

		$sqlr = "SELECT fuente, medio_pago, producto_servicio, seccion_presupuestal, valor, vigencia_gasto FROM ccpetinicialing  WHERE cuenta = '".$_POST['codigo'][$xx]."' AND vigencia='$vigencia' $crit $crit1";
		$result = mysqli_query($linkbd, $sqlr);
		if(mysqli_num_rows($result) > 0){
			while($row = mysqli_fetch_array($result)){

				$arregloGastos = reporteIngresosCcpet($_POST['codigo'][$xx], $row[3], $row[1], $vigencia, $fechaIni, $fechaFin, $row[0], $row[5]);

				$recaudoPorCuenta = generaRecaudoCcpet($_POST['codigo'][$xx],$vigencia,$vigencia,$fechaIni,$fechaFin, $row[0], $row[2], $row[1], $row[3]);

				$totalRecaudoCuenta = $recaudoPorCuenta;

				$saldoPorRecaudar = $arregloGastos[3] - $totalRecaudoCuenta;
				$enEjecucion = round((($totalRecaudoCuenta/$arregloGastos[3])*100),0);

				$sqlr_fuente = "SELECT nombre FROM ccpet_fuentes_cuipo WHERE codigo_fuente = '$row[0]'";
				$result_fuente = mysqli_query($linkbd, $sqlr_fuente);
				$row_fuente = mysqli_fetch_array($result_fuente);

				$sqlr_bienes = "SELECT titulo FROM ccpetbienestransportables WHERE grupo = '$row[2]'";
				$result_bienes = mysqli_query($linkbd, $sqlr_bienes);
				$row_bienes = mysqli_fetch_array($result_bienes);

				$sqlr_unidad = "SELECT nombre FROM pptoseccion_presupuestal WHERE id_seccion_presupuestal = '$row[3]'";
				$result_unidad = mysqli_query($linkbd, $sqlr_unidad);
				$row_unidad = mysqli_fetch_array($result_unidad);

				$i++;
				$objPHPExcel->setActiveSheetIndex(0)
				->setCellValueExplicit ("A$i", $row[1], PHPExcel_Cell_DataType :: TYPE_STRING)
				->setCellValueExplicit ("B$i", $row[3] .' - '. $row_unidad[0], PHPExcel_Cell_DataType :: TYPE_STRING)
				->setCellValueExplicit ("C$i", $row[0] .' - '. $row_fuente[0], PHPExcel_Cell_DataType :: TYPE_STRING)
				->setCellValueExplicit ("D$i", $_POST['codigo'][$xx], PHPExcel_Cell_DataType :: TYPE_STRING)
				->setCellValueExplicit ("E$i", '', PHPExcel_Cell_DataType :: TYPE_STRING)
				->setCellValueExplicit ("F$i", $row[2], PHPExcel_Cell_DataType :: TYPE_STRING)
				->setCellValueExplicit ("G$i", $row_bienes[0], PHPExcel_Cell_DataType :: TYPE_STRING)
				->setCellValueExplicit ("H$i", $arregloGastos[0], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
				->setCellValueExplicit ("I$i", $arregloGastos[1], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
				->setCellValueExplicit ("J$i", $arregloGastos[2], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
				->setCellValueExplicit ("K$i", $arregloGastos[3], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
				->setCellValueExplicit ("L$i", 0, PHPExcel_Cell_DataType :: TYPE_NUMERIC)
				->setCellValueExplicit ("M$i", 0, PHPExcel_Cell_DataType :: TYPE_NUMERIC)
				->setCellValueExplicit ("N$i", $recaudoPorCuenta, PHPExcel_Cell_DataType :: TYPE_NUMERIC)
				->setCellValueExplicit ("O$i", $totalRecaudoCuenta, PHPExcel_Cell_DataType :: TYPE_NUMERIC)
				->setCellValueExplicit ("P$i", $saldoPorRecaudar, PHPExcel_Cell_DataType :: TYPE_NUMERIC)
				->setCellValueExplicit ("Q$i", $enEjecucion.'%', PHPExcel_Cell_DataType :: TYPE_STRING);
				$objPHPExcel->getActiveSheet()->getStyle("A$i:Q$i")->applyFromArray($styleArray);


				//$objPHPExcel-> getActiveSheet()->getStyle ("A$i:H$i")-> getStartColor ()-> setRGB ('A6E5F3');
			}

		}

		/* $sqlr = "SELECT fuente, medioPago, unidad, valor FROM ccpetinicialvaloringresos WHERE cuenta like '".$_POST['codigo'][$xx]."' AND vigencia='$vigencia' $crit $crit1 GROUP BY fuente, medioPago, unidad UNION SELECT CA.id_fuente, CA.medio_pago, 01, CA.valorcsf FROM ccpetadicion_inversion_detalles AS CA WHERE CA.rubro = '".$_POST['codigo'][$xx]."' AND NOT EXISTS (SELECT 1 FROM ccpetinicialvaloringresos AS CBT WHERE CBT.cuenta = '".$_POST['codigo'][$xx]."' AND CBT.vigencia='$vigencia' AND CA.id_fuente = CBT.fuente AND CBT.medioPago = CA.medio_pago) AND NOT EXISTS (SELECT 1 FROM ccpetinicialserviciosingresos AS CS WHERE CS.cuenta = '".$_POST['codigo'][$xx]."' AND CS.vigencia='$vigencia' AND CA.id_fuente = CS.fuente AND CS.medioPago = CA.medio_pago) GROUP BY CA.id_fuente, CA.medio_pago, CA.vigencia_gasto";
		$result = mysqli_query($linkbd, $sqlr);
		if(mysqli_num_rows($result) > 0){
			while($row = mysqli_fetch_array($result)){

				if(strlen($row[2]) == 1)
				{
					$row[2] = '0'.$row[2];
				}

				$arregloGastos = reporteIngresosCcpet($_POST['codigo'][$xx], $row[2], $row[1], $vigencia, $fechaIni, $fechaFin, $row[0]);

				$recaudoPorCuenta = generaRecaudoCcpet($_POST['codigo'][$xx],$vigencia,$vigencia,$fechaIni,$fechaFin, $row[0]);

				$totalRecaudoCuenta = $recaudoPorCuenta;

				$saldoPorRecaudar = $arregloGastos[3] - $totalRecaudoCuenta;
				$enEjecucion = round((($totalRecaudoCuenta/$arregloGastos[3])*100),0);

				$sqlr_fuente = "SELECT nombre FROM ccpet_fuentes_cuipo WHERE codigo_fuente = '$row[0]'";
				$result_fuente = mysqli_query($linkbd, $sqlr_fuente);
				$row_fuente = mysqli_fetch_array($result_fuente);

				$sqlr_unidad = "SELECT nombre FROM pptouniejecu WHERE id_cc = '$row[2]'";
				$result_unidad = mysqli_query($linkbd, $sqlr_unidad);
				$row_unidad = mysqli_fetch_array($result_unidad);
				$i++;
				$objPHPExcel->setActiveSheetIndex(0)
				->setCellValueExplicit ("A$i", $row[1], PHPExcel_Cell_DataType :: TYPE_STRING)
				->setCellValueExplicit ("B$i", $row[2] .' - '. $row_unidad[0], PHPExcel_Cell_DataType :: TYPE_STRING)
				->setCellValueExplicit ("C$i", $row[0] .' - '. $row_fuente[0], PHPExcel_Cell_DataType :: TYPE_STRING)
				->setCellValueExplicit ("D$i", $_POST['codigo'][$xx], PHPExcel_Cell_DataType :: TYPE_STRING)
				->setCellValueExplicit ("E$i", '', PHPExcel_Cell_DataType :: TYPE_STRING)
				->setCellValueExplicit ("F$i", '', PHPExcel_Cell_DataType :: TYPE_STRING)
				->setCellValueExplicit ("G$i", '', PHPExcel_Cell_DataType :: TYPE_STRING)
				->setCellValueExplicit ("H$i", $arregloGastos[0], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
				->setCellValueExplicit ("I$i", $arregloGastos[1], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
				->setCellValueExplicit ("J$i", $arregloGastos[2], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
				->setCellValueExplicit ("K$i", $arregloGastos[3], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
				->setCellValueExplicit ("L$i", 0, PHPExcel_Cell_DataType :: TYPE_NUMERIC)
				->setCellValueExplicit ("M$i", 0, PHPExcel_Cell_DataType :: TYPE_NUMERIC)
				->setCellValueExplicit ("N$i", $recaudoPorCuenta, PHPExcel_Cell_DataType :: TYPE_NUMERIC)
				->setCellValueExplicit ("O$i", $totalRecaudoCuenta, PHPExcel_Cell_DataType :: TYPE_NUMERIC)
				->setCellValueExplicit ("P$i", $saldoPorRecaudar, PHPExcel_Cell_DataType :: TYPE_NUMERIC)
				->setCellValueExplicit ("Q$i", $enEjecucion.'%', PHPExcel_Cell_DataType :: TYPE_STRING);
				$objPHPExcel->getActiveSheet()->getStyle("A$i:Q$i")->applyFromArray($styleArray);

				//$objPHPExcel-> getActiveSheet()->getStyle ("A$i:H$i")-> getStartColor ()-> setRGB ('A6E5F3');
			}
		} */

		/* $sqlr = "SELECT fuente, medioPago, unidad, valor, cuin FROM ccpetinicialingresoscuin WHERE cuenta = '".$_POST[codigo][$xx]."' AND vigencia='$_POST[vigencia]' $crit $crit1";
		$result = mysqli_query($linkbd, $sqlr);
		if(mysqli_num_rows($result) > 0){
			while($row = mysqli_fetch_array($result)){
				$sqlr_fuente = "SELECT fuente_financiacion FROM ccpet_fuentes WHERE id_fuente = '$row[0]'";
				$result_fuente = mysqli_query($linkbd, $sqlr_fuente);
				$row_fuente = mysqli_fetch_array($result_fuente);

				$sqlr_bienes = "SELECT nombre FROM ccpet_cuin WHERE codigo_cuin = '$row[2]'";
				$result_bienes = mysqli_query($linkbd, $sqlr_bienes);
				$row_bienes = mysqli_fetch_array($result_bienes);

				$sqlr_unidad = "SELECT nombre FROM pptouniejecu WHERE id_cc = '$row[3]'";
				$result_unidad = mysqli_query($linkbd, $sqlr_unidad);
				$row_unidad = mysqli_fetch_array($result_unidad);

				$i++;
				$objPHPExcel->setActiveSheetIndex(0)
				->setCellValueExplicit ("A$i", $row[1], PHPExcel_Cell_DataType :: TYPE_STRING)
				->setCellValueExplicit ("B$i", $row[2] .' - '. $row_unidad[0], PHPExcel_Cell_DataType :: TYPE_STRING)
				->setCellValueExplicit ("C$i", $row[0] .' - '. $row_fuente[0], PHPExcel_Cell_DataType :: TYPE_STRING)
				->setCellValueExplicit ("D$i", $_POST[codigo][$xx], PHPExcel_Cell_DataType :: TYPE_STRING)
				->setCellValueExplicit ("E$i", $row[4], PHPExcel_Cell_DataType :: TYPE_STRING)
				->setCellValueExplicit ("F$i", '', PHPExcel_Cell_DataType :: TYPE_STRING)
				->setCellValueExplicit ("G$i", $row_bienes[0], PHPExcel_Cell_DataType :: TYPE_STRING)
				->setCellValueExplicit ("H$i", $row[3], PHPExcel_Cell_DataType :: TYPE_NUMERIC);
				$objPHPExcel->getActiveSheet()->getStyle("A$i:H$i")->applyFromArray($styleArray);

				//$objPHPExcel-> getActiveSheet()->getStyle ("A$i:H$i")-> getStartColor ()-> setRGB ('A6E5F3');
			}
		} */

		/* $sqlr = "SELECT fuente, medioPago, unidad, valor, cuenta_clasificador FROM ccpetinicialingresosclasificador WHERE cuenta = '".$_POST[codigo][$xx]."' AND vigencia='$vigencia' $crit $crit1";
		$result = mysqli_query($linkbd, $sqlr);
		if(mysqli_num_rows($result) > 0){
			while($row = mysqli_fetch_array($result)){
				$sqlr_fuente = "SELECT fuente_financiacion FROM ccpet_fuentes WHERE id_fuente = '$row[0]'";
				$result_fuente = mysqli_query($linkbd, $sqlr_fuente);
				$row_fuente = mysqli_fetch_array($result_fuente);

				$maxVersion = ultimaVersionIngresosCCPET();

				$sqlr_ingresos = "SELECT nombre FROM cuentasingresosccpet WHERE codigo = '$row[4]' AND version = '$maxVersion'";
				$result_ingresos = mysqli_query($linkbd, $sqlr_ingresos);
				$row_ingresos = mysqli_fetch_array($result_ingresos);

				$sqlr_unidad = "SELECT nombre FROM pptouniejecu WHERE id_cc = '$row[2]'";
				$result_unidad = mysqli_query($linkbd, $sqlr_unidad);
				$row_unidad = mysqli_fetch_array($result_unidad);
				$i++;
				$objPHPExcel->setActiveSheetIndex(0)
				->setCellValueExplicit ("A$i", $row[1], PHPExcel_Cell_DataType :: TYPE_STRING)
				->setCellValueExplicit ("B$i", $row[2] .' - '. $row_unidad[0], PHPExcel_Cell_DataType :: TYPE_STRING)
				->setCellValueExplicit ("C$i", $row[0] .' - '. $row_fuente[0], PHPExcel_Cell_DataType :: TYPE_STRING)
				->setCellValueExplicit ("D$i", $_POST[codigo][$xx], PHPExcel_Cell_DataType :: TYPE_STRING)
				->setCellValueExplicit ("E$i", $row[4], PHPExcel_Cell_DataType :: TYPE_STRING)
				->setCellValueExplicit ("F$i", '', PHPExcel_Cell_DataType :: TYPE_STRING)
				->setCellValueExplicit ("G$i", $row_ingresos[0], PHPExcel_Cell_DataType :: TYPE_STRING)
				->setCellValueExplicit ("H$i", $row[3], PHPExcel_Cell_DataType :: TYPE_NUMERIC);
				$objPHPExcel->getActiveSheet()->getStyle("A$i:H$i")->applyFromArray($styleArray);

				//$objPHPExcel-> getActiveSheet()->getStyle ("A$i:H$i")-> getStartColor ()-> setRGB ('A6E5F3');
			}
		} */
		
		/*
		->setCellValueExplicit ("D$i", iconv($_SESSION["VERCARPDFINI"], $_SESSION["VERCARPDFFIN"]."//TRANSLIT",$_POST[detalle][$xx]), PHPExcel_Cell_DataType :: TYPE_STRING)
		->setCellValueExplicit ("E$i", iconv($_SESSION["VERCARPDFINI"], $_SESSION["VERCARPDFFIN"]."//TRANSLIT",$fuenteRp), PHPExcel_Cell_DataType :: TYPE_STRING)
		->setCellValueExplicit ("F$i", $_POST[terceroT][$xx], PHPExcel_Cell_DataType :: TYPE_STRING)
		->setCellValueExplicit ("G$i", iconv($_SESSION["VERCARPDFINI"], $_SESSION["VERCARPDFFIN"]."//TRANSLIT",$terceroB), PHPExcel_Cell_DataType :: TYPE_STRING)
		->setCellValueExplicit ("H$i", $_POST[cuenta][$xx], PHPExcel_Cell_DataType :: TYPE_STRING)
		->setCellValueExplicit ("I$i", iconv($_SESSION["VERCARPDFINI"], $_SESSION["VERCARPDFFIN"]."//TRANSLIT",$rubro), PHPExcel_Cell_DataType :: TYPE_STRING)
		->setCellValueExplicit ("J$i", $_POST[fecha][$xx], PHPExcel_Cell_DataType :: TYPE_STRING)
		->setCellValueExplicit ("K$i", $_POST[valor][$xx], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
		->setCellValueExplicit ("L$i", $estadoContrato, PHPExcel_Cell_DataType :: TYPE_STRING)
		->setCellValueExplicit ("M$i", $clasificacion, PHPExcel_Cell_DataType :: TYPE_STRING);
		$objPHPExcel->getActiveSheet()->getStyle("A$i:M$i")->applyFromArray($borders);*/
		$i++;
    }
		
	//----Propiedades de la hoja 1
	$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth('15');
	$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth('15');
	$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth('50');
	$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('N')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('O')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('P')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('Q')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->setTitle('EJECUCION');
	$objPHPExcel->setActiveSheetIndex(0);

//----Guardar documento----
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename="REPORTE EJECUCION INGRESOS.xls"');
header('Cache-Control: max-age=0');
 
$objWriter=PHPExcel_IOFactory::createWriter($objPHPExcel,'Excel5');
$objWriter->save('php://output');
exit;

?>