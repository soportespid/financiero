<?php
    header("Cache-control: no-cache, no-store, must-revalidate");
    header("Content-Type: text/html;charset=utf8");
    require 'comun.inc';
    require 'funciones.inc';
    session_start();
    if(empty($_SESSION)){
        header("location: index.php");
    }
    $linkbd = conectar_v7();
    $linkbd -> set_charset("utf8");
    cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
    date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html lang=es>
    <head>
        <meta charset="utf-8"/>
        <meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1"/>
        <meta name="viewport" content="user-scalable=no">
        <title>:: IDEAL 10 - Parametrización</title>
        <link href="favicon.ico" rel="shortcut icon"/>
        <link href="css/css2.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
        <link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
        <link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
        <link href="css/style.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
        <link href="css/tabs.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" href="sweetalert2/dist/sweetalert2.min.css">
        <script src="sweetalert2/dist/sweetalert2.min.js"></script>
        <script type="text/javascript" src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
    </head>
    <body>
        <section id="myapp" v-cloak>
            <div class="loading-container" v-show="isLoading" >
                <p class="text-loading"data-loading="IDEAL 10">IDEAL 10</p>
            </div>
            <div class="main-container">
                <header>
                    <table>
                        <tr><script>barra_imagenes("para");</script><?php cuadro_titulos();?></tr>
                    </table>
                </header>
                <nav>
                    <?php menu_desplegable("para");?>
                    <div class="bg-white group-btn p-1">
                        <button type="button"  @click="confirmSave" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
                            <span>Guardar</span>
                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960"><path d="M840-680v480q0 33-23.5 56.5T760-120H200q-33 0-56.5-23.5T120-200v-560q0-33 23.5-56.5T200-840h480l160 160Zm-80 34L646-760H200v560h560v-446ZM480-240q50 0 85-35t35-85q0-50-35-85t-85-35q-50 0-85 35t-35 85q0 50 35 85t85 35ZM240-560h360v-160H240v160Zm-40-86v446-560 114Z"></path></svg>
                        </button>
                        <button type="button" @click="mypop=window.open('para-principal','',''); mypop.focus();" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
                            <span>Nueva ventana</span>
                            <svg class="fill-black group-hover:fill-white w-5 h-5" xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960"><path d="M320-120v-80h80v-80H160q-33 0-56.5-23.5T80-360v-400q0-33 23.5-56.5T160-840h640q33 0 56.5 23.5T880-760v400q0 33-23.5 56.5T800-280H560v80h80v80H320ZM160-360h640v-400H160v400Zm0 0v-400 400Z"></path></svg>
                        </button>
                        <button type="button" @click="mypop=window.open('mipg-infoEntidad','','');mypop.focus();" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
                            <span class="group-hover:text-white">Duplicar pantalla</span>
                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960"><path d="M640-160v-360H160v360h480Zm80-200v-80h80v-360H320v200h-80v-200q0-33 23.5-56.5T320-880h480q33 0 56.5 23.5T880-800v360q0 33-23.5 56.5T800-360h-80ZM160-80q-33 0-56.5-23.5T80-160v-360q0-33 23.5-56.5T160-600h480q33 0 56.5 23.5T720-520v360q0 33-23.5 56.5T640-80H160Zm400-603ZM400-340Z"></path></svg>
                        </button>
                    </div>
                </nav>
                <article>
                    <!--TABS-->
                    <div ref="rTabs" class="nav-tabs bg-white p-1">
                        <div class="nav-item active" @click="showTab(1)">&nbsp&nbspInformación Entidad&nbsp&nbsp</div>
                        <div class="nav-item"  @click="showTab(2)">&nbsp&nbspEmpresas de Parafiscales&nbsp&nbsp</div>
                    </div>
                    <!--CONTENIDO TABS-->
                    <div ref="rTabsContent" class="nav-tabs-content bg-white">
                        <div class="nav-content active">
                            <h2 class="titulos m-0">Configuración Entidad</h2>
                            <div class="bg-white">
                                <p class="m-2">Todos los campos con (<span class="text-danger fw-bolder">*</span>) son obligatorios</p>
                                <div class="d-flex">
                                    <div class="form-control">
                                        <label class="form-label" for="">Razón Social<span class="text-danger fw-bolder">*</span>:</label>
                                        <input type="text" class="text-center" v-model="txtRazon">
                                    </div>
                                    <div class="form-control">
                                        <label class="form-label" for="">NIT<span class="text-danger fw-bolder">*</span>:</label>
                                        <input type="text" v-model="txtNit">
                                    </div>
                                    <div class="form-control">
                                        <label class="form-label" for="">Sigla<span class="text-danger fw-bolder">*</span>:</label>
                                        <input type="text" v-model="txtSigla">
                                    </div>
                                </div>
                                <div class="d-flex">
                                    <div class="form-control w-75">
                                        <label class="form-label" for="">Dirección<span class="text-danger fw-bolder">*</span>:</label>
                                        <input type="text" class="text-center" v-model="txtDireccion">
                                    </div>
                                    <div class="form-control w-25">
                                        <label class="form-label" for="">Tipo de Entidad<span class="text-danger fw-bolder">*</span>:</label>
                                        <select id="labelSelectName2" v-model="selectTEntidad">
                                            <option value="">:::: Seleccione Tipo de Entidad :::</option>
                                            <option value="EG">ENTIDAD DE GOBIERNO</option>
                                            <option value="EO">EMPRESAS Y OTROS</option>
                                            <option value="RG">REGALIAS</option>
                                            <option value="RS">RESGUARDOS</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="d-flex">
                                    <div class="form-control">
                                        <label class="form-label" for="">Departamento<span class="text-danger fw-bolder">*</span>: </label>
                                        <select id="labelSelectName2" v-model="selectDpto" @Change="iniMunicipio">
                                            <option value="0">:::: Seleccione Departamento :::</option>
                                            <option v-for="(data,index) in arrDpto" :key="index" :value="data.danedpto">
                                            {{ data.nombredpto}} ({{ data.danedpto}})
                                            </option>
                                        </select>
                                    </div>
                                    <div class="form-control">
                                        <label class="form-label" for="">Municipio<span class="text-danger fw-bolder">*</span>: </label>
                                        <select id="labelSelectName2" v-model="selectMpio">
                                            <option value="0">:::: Seleccione Municipio :::</option>
                                            <option v-for="(data,index) in arrMpio" :key="index" :value="data.danemnpio">
                                            {{ data.nom_mnpio}} ({{ data.danemnpio}})
                                            </option>
                                        </select>
                                    </div>
                                    <div class="form-control">
                                        <label class="form-label" for="">Telefono<span class="text-danger fw-bolder">*</span>:</label>
                                        <input type="text" v-model="txtTelefono">
                                    </div>
                                </div>
                                <div class="d-flex">
                                    <div class="form-control">
                                        <label class="form-label" for="">Email<span class="text-danger fw-bolder">*</span>:</label>
                                        <input type="text" class="text-center" v-model="txtEmail">
                                    </div>
                                    <div class="form-control">
                                        <label class="form-label" for="">Web<span class="text-danger fw-bolder">*</span>:</label>
                                        <input type="text" class="text-center" v-model="txtWeb">
                                    </div>
                                </div>
                                <div class="d-flex">
                                    <div class="form-control">
                                        <label class="form-label" for="">IGAC<span class="text-danger fw-bolder">*</span>:</label>
                                        <input type="text" class="text-center" v-model="txtIgac">
                                    </div>
                                    <div class="form-control">
                                        <label class="form-label" for="">Cod CGR<span class="text-danger fw-bolder">*</span>:</label>
                                        <input type="text" v-model="txtCcgr">
                                    </div>
                                    <div class="form-control">
                                        <label class="form-label" for="">Orden<span class="text-danger fw-bolder">*</span>:</label>
                                        <select id="labelSelectName2" v-model="selectOrden">
                                            <option value="">:::: Seleccione Orden :::</option>
                                            <option value="Nacional">NACIONAL</option>
                                            <option value="Dptal">DEPARTAMENTAL</option>
                                            <option value="Mnpal">MUNICIPAL</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="d-flex">
                                    <div class="form-control w-25">
                                        <label class="form-label" for="">Liquidación<span class="text-danger fw-bolder">*</span>:</label>
                                        <select id="labelSelectName2" v-model="selectLiquidacion">
                                            <option value="">:::: Seleccione Liquidación :::</option>
                                            <option value="S">SI</option>
                                            <option value="N">NO</option>
                                        </select>
                                    </div>
                                    <div class="form-control w-25">
                                        <label class="form-label" for="">Doc. Rep. Legal<span class="text-danger fw-bolder">*</span>:</label>
                                        <input type="text"  class="colordobleclik" @dblclick="auxTercero=1; isModal=true" @change="search('documento', 'objTercero')" v-model="objTercero.documento">
                                    </div>
                                    <div class="form-control">
                                        <label class="form-label" for="">Nombre Representante Legal<span class="text-danger fw-bolder">*</span>:</label>
                                        <input type="text" v-model="objTercero.nombre" disabled readonly>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div class="nav-content">
                            <h2 class="titulos m-0">Empresas prestadoras de servicios</h2>
                            <div class="bg-white">
                                <div class="d-flex">
                                    <div class="form-control w-25">
                                        <label class="form-label" for="">ARL:</label>
                                        <input type="text"  class="colordobleclik" @dblclick="auxTercero=2; isModal=true" @change="search('documento', 'objArl')" v-model="objArl.documento">
                                    </div>
                                    <div class="form-control">
                                        <label class="form-label" for="">&nbsp</label>
                                        <input type="text" v-model="objArl.nombre" disabled readonly>
                                    </div>
                                    <div class="form-control w-25">
                                        <label class="form-label" for="">ICBF:</label>
                                        <input type="text"  class="colordobleclik" @dblclick="auxTercero=3; isModal=true" @change="search('documento', 'objIcbf')" v-model="objIcbf.documento">
                                    </div>
                                    <div class="form-control">
                                        <label class="form-label" for="">&nbsp</label>
                                        <input type="text" v-model="objIcbf.nombre" disabled readonly>
                                    </div>
                                </div>
                                <div class="d-flex">
                                    <div class="form-control w-25">
                                        <label class="form-label" for="">SENA:</label>
                                        <input type="text"  class="colordobleclik" @dblclick="auxTercero=4; isModal=true" @change="search('documento', 'objSena')" v-model="objSena.documento">
                                    </div>
                                    <div class="form-control">
                                        <label class="form-label" for="">&nbsp</label>
                                        <input type="text" v-model="objSena.nombre" disabled readonly>
                                    </div>
                                    <div class="form-control w-25">
                                        <label class="form-label" for="">Institutos Técnicos:</label>
                                        <input type="text"  class="colordobleclik" @dblclick="auxTercero=5; isModal=true" @change="search('documento', 'objInstec')" v-model="objInstec.documento">
                                    </div>
                                    <div class="form-control">
                                        <label class="form-label" for="">&nbsp</label>
                                        <input type="text" v-model="objInstec.nombre" disabled readonly>
                                    </div>
                                </div>
                                <div class="d-flex">
                                    <div class="form-control w-25">
                                        <label class="form-label" for="">Caja Compensación:</label>
                                        <input type="text"  class="colordobleclik" @dblclick="auxTercero=6; isModal=true" @change="search('documento', 'objCcf')" v-model="objCcf.documento">
                                    </div>
                                    <div class="form-control">
                                    <label class="form-label" for="">&nbsp</label>
                                        <input type="text" v-model="objCcf.nombre" disabled readonly>
                                    </div>
                                    <div class="form-control w-25">
                                        <label class="form-label" for="">ESAP:</label>
                                        <input type="text"  class="colordobleclik" @dblclick="auxTercero=7; isModal=true" @change="search('documento', 'objEsap')" v-model="objEsap.documento">
                                    </div>
                                    <div class="form-control">
                                        <label class="form-label" for="">&nbsp</label>
                                        <input type="text" v-model="objEsap.nombre" disabled readonly>
                                    </div>
                                </div>

                                <div class="d-flex">
                                    <div class="form-control w-25">
                                        <label class="form-label" for="">Operador Planilla:</label>
                                        <select v-model="operadorPlanilla">
                                            <option v-for="operador in operadorPlanillas">{{ operador }}</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </article>
                <!-- MODALES -->
                <div v-show="isModal" class="modal">
                    <div class="modal-dialog modal-lg" >
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title">Buscar terceros</h5>
                                <button type="button" @click="isModal=false;" class="btn btn-close"><div></div><div></div></button>
                            </div>
                            <div class="modal-body">
                                <div class="d-flex flex-column">
                                    <div class="form-control m-0 mb-3">
                                        <input type="search" placeholder="Buscar" v-model="txtBuscar" @keyup="search()" id="labelInputName">
                                    </div>
                                    <div class="form-control m-0 mb-3">
                                        <label class="form-label" for="labelInputName">Resultados: <span class="fw-bold">{{txtResultados}}</span></label>
                                    </div>
                                </div>
                                <div class="overflow-auto max-vh-50 overflow-x-hidden" >
                                    <table class="table table-hover fw-normal">
                                        <thead>
                                            <tr>
                                                <th>Item</th>
                                                <th>Nombre</th>
                                                <th>Documento</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr v-for="(data,index) in arrTercerosCopy" @dblclick="selectItem(data)" :key="index">
                                                <td>{{index+1}}</td>
                                                <td>{{data.nombre}}</td>
                                                <td>{{data.documento}}</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <script src="Librerias/vue/vue.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
        <script src="vue/herramientas_mipg/entidad/mipg-infoEntidad.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
    </body>
</html>
