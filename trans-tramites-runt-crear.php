<?php

	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require 'comun.inc';
	require 'funciones.inc';
	session_start();
	if(empty($_SESSION)){
		header("location: index.php");
	}
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html lang=es>
	<head>
	<meta charset="utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1"/>
		<meta name="viewport" content="user-scalable=no">
		<title></title>
		<link href="favicon.ico" rel="shortcut icon"/>
		<link href="css/css2z.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/style.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
		<link rel="stylesheet" href="sweetalert2/dist/sweetalert2.min.css">
		<script src="sweetalert2/dist/sweetalert2.min.js"></script>
		<script src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>

	</head>
	<body>
		<header>
			<table>
				<tr><script>barra_imagenes("trans");</script><?php cuadro_titulos();?></tr>
			</table>
		</header>
		<section id="myapp" v-cloak >
			<input type="hidden" value = "1" ref="pageType">
			<div class="loading-container" v-show="isLoading" >
				<p class="text-loading"data-loading="IDEAL 10">IDEAL 10</p>
			</div>
			<nav>
				<table>
					<tr><?php menu_desplegable("trans");?></tr>
				</table>
				<div class="bg-white group-btn p-1">
					<button type="button" @click="save()" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
						<span>Guardar</span>
						<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960">
							<path
								d="M840-680v480q0 33-23.5 56.5T760-120H200q-33 0-56.5-23.5T120-200v-560q0-33 23.5-56.5T200-840h480l160 160Zm-80 34L646-760H200v560h560v-446ZM480-240q50 0 85-35t35-85q0-50-35-85t-85-35q-50 0-85 35t-35 85q0 50 35 85t85 35ZM240-560h360v-160H240v160Zm-40-86v446-560 114Z">
							</path>
						</svg>
					</button>
					<button type="button" @click="window.location.href='trans-tramites-runt-buscar'"  class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
						<span>Buscar</span>
						<svg class="fill-black group-hover:fill-white w-5 h-5" xmlns="http://www.w3.org/2000/svg"
							viewBox="0 -960 960 960">
							<path
								d="M784-120 532-372q-30 24-69 38t-83 14q-109 0-184.5-75.5T120-580q0-109 75.5-184.5T380-840q109 0 184.5 75.5T640-580q0 44-14 83t-38 69l252 252-56 56ZM380-400q75 0 127.5-52.5T560-580q0-75-52.5-127.5T380-760q-75 0-127.5 52.5T200-580q0 75 52.5 127.5T380-400Z">
							</path>
						</svg>
					</button>
					<button type="button" onclick="mypop=window.open('trans-principal','','');mypop.focus();" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
						<span>Nueva ventana</span>
						<svg class="fill-black group-hover:fill-white w-5 h-5" xmlns="http://www.w3.org/2000/svg"
							viewBox="0 -960 960 960">
							<path
								d="M320-120v-80h80v-80H160q-33 0-56.5-23.5T80-360v-400q0-33 23.5-56.5T160-840h640q33 0 56.5 23.5T880-760v400q0 33-23.5 56.5T800-280H560v80h80v80H320ZM160-360h640v-400H160v400Zm0 0v-400 400Z">
							</path>
						</svg>
					</button>
					<button type="button" @click="mypop=window.open('trans-tramites-runt-crear','','');mypop.focus();" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
						<span class="group-hover:text-white">Duplicar pantalla</span>
						<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960"><path d="M640-160v-360H160v360h480Zm80-200v-80h80v-360H320v200h-80v-200q0-33 23.5-56.5T320-880h480q33 0 56.5 23.5T880-800v360q0 33-23.5 56.5T800-360h-80ZM160-80q-33 0-56.5-23.5T80-160v-360q0-33 23.5-56.5T160-600h480q33 0 56.5 23.5T720-520v360q0 33-23.5 56.5T640-80H160Zm400-603ZM400-340Z"></path></svg>
					</button>
					<button type="button" @click="window.location.href='trans-recaudo-impuesto-buscar'" class="btn btn-success btn-success-hover d-flex justify-between align-items-center">
						<span>Atras</span>
						<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960">
							<path
								d="M760-200v-160q0-50-35-85t-85-35H273l144 144-57 56-240-240 240-240 57 56-144 144h367q83 0 141.5 58.5T840-360v160h-80Z">
							</path>
						</svg>
					</button>
				</div>
			</nav>
			<article class="bg-white">
				<div>
					<h2 class="titulos m-0">Crear Tramite RUNT</h2>
					<div ref="rTabs" class="nav-tabs p-1">
						<div class="nav-item active" @click="showTab(1)">Componente 1</div>
						<div class="nav-item" @click="showTab(2)">Componente 2</div>
						<div class="nav-item" @click="showTab(3)">Componente 3</div>
					</div>
					<div ref="rTabsContent" class="nav-tabs-content">
						<div class="nav-content active">
							<div>
								<div>
									<p class="m-2">Todos los campos con (<span class="text-danger fw-bolder">*</span>) son obligatorios</p>
									<div class="w-100 d-flex">
										<div class="w-50 d-flex">
											<div class="form-control">
												<label class="form-label">Nombre <span class="text-danger fw-bolder">*</span>:</label>
												<input type="text" v-model="strNombre1">
											</div>
										</div>
										<div class="w-50 d-flex">
											<div class="form-control">
												<label class="form-label">Precio venta <span class="text-danger fw-bolder">*</span>:</label>
												<input type="number" v-model="intPrecio1" class="text-right">
											</div>
											<div class="form-control">
												<label class="form-label">Terceros <span class="text-danger fw-bolder">*</span>:</label>
												<select v-model="selectTerceros1">
													<option value="2" selected>No</option>
												</select>
											</div>
										</div>
									</div>
									<div class="w-100 d-flex">
										<div class="w-50 d-flex">
											<div class="form-control">
												<label class="form-label">Tipo <span class="text-danger fw-bolder">*</span>:</label>
												<select v-model="selectSimple1">
													<option value="C" selected>Compuesto</option>
												</select>
											</div>
											<div class="form-control">
												<label class="form-label">Destino <span class="text-danger fw-bolder">*</span>:</label>
												<select v-model="selectDestino1">
													<option value="" selected>Otros</option>
													<option value="D" selected>Departamental</option>
													<option value="N">Nacional</option>
													<option value="M">Municipal</option>
												</select>
											</div>
										</div>
										<div class="w-50 d-flex">
											<div class="form-control">
												<label class="form-label">Tipo ingreso <span class="text-danger fw-bolder">*</span>:</label>
												<select v-model="selectTipoIngreso1">
													<option value="" selected>Otro</option>
													<option value="normal" selected>Normal</option>
													<option value="sgr">SGR</option>
													<option value="sgp">SGP</option>
													<option value="salud">Salud ssf</option>
													<option value="educacion">Educacion ssf</option>
												</select>
											</div>
											<div class="form-control">
												<label class="form-label">Contabilizar liquidacion <span class="text-danger fw-bolder">*</span>:</label>
												<select v-model="selectContabilizar1">
													<option value="S" selected>Si</option>
												</select>
											</div>
										</div>
									</div>
								</div>
								<h2 class="titulos m-0">Detalle</h2>
								<div class="w-100 d-flex">
									<div class="w-50 d-flex">
										<div class="form-control">
											<label class="form-label">Concepto contable <span class="text-danger fw-bolder">*</span>:</label>
											<select v-model="selectConcepto1">
												<option disabled selected>Seleccione</option>
												<option v-for="(data,index) in arrConceptos" :key="index" :value="data.codigo">
													{{ data.codigo+"-"+data.nombre}}
												</option>
											</select>
										</div>
										<div class="form-control" >
											<label class="form-label">¿Afecta presupuesto? <span class="text-danger fw-bolder">*</span>:</label>
											<select v-model="selectIsPresupuesto1">
												<option value="2">Si</option>
											</select>
										</div>
									</div>
									<div class="w-50 d-flex">
										<div class="form-control">
											<label class="form-label" for="">Cuenta presupuestal<span class="text-danger fw-bolder">*</span>:</label>
											<div class="d-flex">
												<input type="text"  class="colordobleclik w-25" v-model="objCuenta1.codigo" @dblclick="isModal=true" readonly>
												<input type="text" v-model="objCuenta1.nombre" disabled readonly>
											</div>
										</div>
										<div class="form-control" v-show="intType1 == 1">
											<label class="form-label" for="">CUIN <span class="text-danger fw-bolder">*</span>:</label>
											<div class="d-flex">
												<input type="text"  class="colordobleclik w-25" v-model="objCuin1.codigo" @dblclick="isModalCuin=true" readonly>
												<input type="text" v-model="objCuin1.nombre" disabled readonly>
											</div>
										</div>
										<div class="form-control" v-show="intType1 == 2">
											<label class="form-label" for="">Bienes transportables <span class="text-danger fw-bolder">*</span>:</label>
											<div class="d-flex">
												<input type="text"  class="colordobleclik w-25" v-model="objBienes1.codigo" @dblclick="isModalBienes=true" readonly>
												<input type="text" v-model="objBienes1.nombre" disabled readonly>
											</div>
										</div>
										<div class="form-control" v-show="intType1 == 3 ">
											<label class="form-label" for="">Servicios <span class="text-danger fw-bolder">*</span>:</label>
											<div class="d-flex">
												<input type="text"  class="colordobleclik w-25" v-model="objServicio1.codigo" @dblclick="isModalServicios=true" readonly>
												<input type="text" v-model="objServicio1.nombre" disabled readonly>
											</div>
										</div>
									</div>
								</div>
								<div class="w-100 d-flex">
									<div class="w-50 d-flex">
										<div class="form-control">
											<label class="form-label">Porcentaje <span class="text-danger fw-bolder">*</span>:</label>
											<input type="number" v-model="intPorcentaje1" class="text-right">
										</div>
										<div class="form-control" >
											<label class="form-label">Sección presupuestal <span class="text-danger fw-bolder">*</span>:</label>
											<select v-model="selectSeccion1">
												<option disabled selected>Seleccione</option>
												<option v-for="(data,index) in arrSecciones" :key="index" :value="data.codigo">
													{{ data.codigo+"-"+data.nombre}}
												</option>
											</select>
										</div>
									</div>
									<div class="w-50 d-flex">
										<div class="form-control" >
											<label class="form-label" for="">Fuentes <span class="text-danger fw-bolder">*</span>:</label>
											<div class="d-flex">
												<input type="text"  class="colordobleclik w-25" v-model="objFuenteComp1.codigo" @dblclick="isModalFuenteComp=true" readonly>
												<input type="text" v-model="objFuenteComp1.nombre" disabled readonly>
											</div>
										</div>
										<div class="form-control justify-between w-25">
											<label for=""></label>
											<button type="button" class="btn btn-primary" @click="add()">Agregar</button>
										</div>
									</div>
								</div>
								<div class="table-responsive" style="max-height:30vh;">
									<table class="table table-hover fw-normal">
										<thead>
											<tr class="text-center">
												<th>Cuenta CCPET</th>
												<th>Seccion presupuestal</th>
												<th>Fuente</th>
												<th>Concepto</th>
												<th>Cuenta clasificadora</th>
												<th>Porcentaje</th>
												<th>
													<div class="d-flex justify-center">
														<button type="button" @click="del()" class="btn btn-danger">x</button>
													</div>
												</th>
											</tr>
										</thead>
										<tbody>
											<tr v-for="(data,index) in arrDetalleIngreso1" :key="index">
												<td>{{ data.cuenta }} - {{ data.nombre_cuenta }}</td>
												<td>{{ data.seccion }} - {{ data.nombre_seccion }}</td>
												<td>{{ data.fuente }} - {{ data.nombre_fuente }}</td>
												<td>{{ data.concepto.codigo+" - "+data.concepto.nombre }}</td>
												<td>{{ data.cuenta_clasificadora }}</td>
												<td>{{ data.porcentaje }}%</td>
												<td>
													<div class="d-flex justify-center">
														<button type="button" @click="del(index)" class="btn btn-danger">x</button>
													</div>
												</td>
											</tr>
										</tbody>
										<tfoot>
											<tr class="bg-secondary fw-bold">
												<td colspan="5" class="text-right">Total:</td>
												<td class="text-center">{{intTotalPorcentaje1}}%</td>
											</tr>
										</tfoot>
									</table>
								</div>
							</div>
						</div>
						<div class="nav-content">
							<div>
								<div>
									<p class="m-2">Todos los campos con (<span class="text-danger fw-bolder">*</span>) son obligatorios</p>
									<div class="w-100 d-flex">
										<div class="w-50 d-flex">
											<div class="form-control">
												<label class="form-label">Nombre <span class="text-danger fw-bolder">*</span>:</label>
												<input type="text" v-model="strNombre2">
											</div>
										</div>
										<div class="w-50 d-flex">
											<div class="form-control">
												<label class="form-label">Precio venta <span class="text-danger fw-bolder">*</span>:</label>
												<input type="number" v-model="intPrecio2" class="text-right">
											</div>
											<div class="form-control">
												<label class="form-label">Terceros <span class="text-danger fw-bolder">*</span>:</label>
												<select v-model="selectTerceros2">
													<option value="2" selected>No</option>
												</select>
											</div>
										</div>
									</div>
									<div class="w-100 d-flex">
										<div class="w-50 d-flex">
											<div class="form-control">
												<label class="form-label">Tipo <span class="text-danger fw-bolder">*</span>:</label>
												<select v-model="selectSimple2">
													<option value="C" selected>Compuesto</option>
												</select>
											</div>
											<div class="form-control">
												<label class="form-label">Destino <span class="text-danger fw-bolder">*</span>:</label>
												<select v-model="selectDestino2">
													<option value="" selected>Otros</option>
													<option value="D" selected>Departamental</option>
													<option value="N">Nacional</option>
													<option value="M">Municipal</option>
												</select>
											</div>
										</div>
										<div class="w-50 d-flex">
											<div class="form-control">
												<label class="form-label">Tipo ingreso <span class="text-danger fw-bolder">*</span>:</label>
												<select v-model="selectTipoIngreso2">
													<option value="" selected>Otro</option>
													<option value="normal" selected>Normal</option>
													<option value="sgr">SGR</option>
													<option value="sgp">SGP</option>
													<option value="salud">Salud ssf</option>
													<option value="educacion">Educacion ssf</option>
												</select>
											</div>
											<div class="form-control">
												<label class="form-label">Contabilizar liquidacion <span class="text-danger fw-bolder">*</span>:</label>
												<select v-model="selectContabilizar1">
													<option value="S" selected>Si</option>
												</select>
											</div>
										</div>
									</div>
								</div>
								<h2 class="titulos m-0">Detalle</h2>
								<div class="w-100 d-flex">
									<div class="w-50 d-flex">
										<div class="form-control">
											<label class="form-label">Concepto contable <span class="text-danger fw-bolder">*</span>:</label>
											<select v-model="selectConcepto2">
												<option disabled selected>Seleccione</option>
												<option v-for="(data,index) in arrConceptos" :key="index" :value="data.codigo">
													{{ data.codigo+"-"+data.nombre}}
												</option>
											</select>
										</div>
										<div class="form-control">
											<label class="form-label">¿Afecta presupuesto? <span class="text-danger fw-bolder">*</span>:</label>
											<select v-model="selectIsPresupuesto2">
												<option value="2">Si</option>
											</select>
										</div>
									</div>
									<div class="w-50 d-flex">
										<div class="form-control">
											<label class="form-label" for="">Cuenta presupuestal<span class="text-danger fw-bolder">*</span>:</label>
											<div class="d-flex">
												<input type="text"  class="colordobleclik w-25" v-model="objCuenta2.codigo" @dblclick="isModal=true" readonly>
												<input type="text" v-model="objCuenta2.nombre" disabled readonly>
											</div>
										</div>
										<div class="form-control" v-show="intType2 == 1">
											<label class="form-label" for="">CUIN <span class="text-danger fw-bolder">*</span>:</label>
											<div class="d-flex">
												<input type="text"  class="colordobleclik w-25" v-model="objCuin2.codigo" @dblclick="isModalCuin=true" readonly>
												<input type="text" v-model="objCuin2.nombre" disabled readonly>
											</div>
										</div>
										<div class="form-control" v-show="intType2 == 2">
											<label class="form-label" for="">Bienes transportables <span class="text-danger fw-bolder">*</span>:</label>
											<div class="d-flex">
												<input type="text"  class="colordobleclik w-25" v-model="objBienes2.codigo" @dblclick="isModalBienes=true" readonly>
												<input type="text" v-model="objBienes2.nombre" disabled readonly>
											</div>
										</div>
										<div class="form-control" v-show="intType2 == 3">
											<label class="form-label" for="">Servicios <span class="text-danger fw-bolder">*</span>:</label>
											<div class="d-flex">
												<input type="text"  class="colordobleclik w-25" v-model="objServicio2.codigo" @dblclick="isModalServicios=true" readonly>
												<input type="text" v-model="objServicio2.nombre" disabled readonly>
											</div>
										</div>
									</div>
								</div>
								<div class="w-100 d-flex">
									<div class="w-50 d-flex">
										<div class="form-control">
											<label class="form-label">Porcentaje <span class="text-danger fw-bolder">*</span>:</label>
											<input type="number" v-model="intPorcentaje2" class="text-right">
										</div>
										<div class="form-control" >
											<label class="form-label">Sección presupuestal <span class="text-danger fw-bolder">*</span>:</label>
											<select v-model="selectSeccion2">
												<option disabled selected>Seleccione</option>
												<option v-for="(data,index) in arrSecciones" :key="index" :value="data.codigo">
													{{ data.codigo+"-"+data.nombre}}
												</option>
											</select>
										</div>
									</div>
									<div class="w-50 d-flex">
										<div class="form-control" >
											<label class="form-label" for="">Fuentes <span class="text-danger fw-bolder">*</span>:</label>
											<div class="d-flex">
												<input type="text"  class="colordobleclik w-25" v-model="objFuenteComp2.codigo" @dblclick="isModalFuenteComp=true" readonly>
												<input type="text" v-model="objFuenteComp2.nombre" disabled readonly>
											</div>
										</div>
										<div class="form-control justify-between w-25">
											<label for=""></label>
											<button type="button" class="btn btn-primary" @click="add()">Agregar</button>
										</div>
									</div>
								</div>
								<div class="table-responsive" style="max-height:30vh;" >
									<table class="table table-hover fw-normal">
										<thead>
											<tr class="text-center">
												<th>Cuenta CCPET</th>
												<th>Seccion presupuestal</th>
												<th>Fuente</th>
												<th>Concepto</th>
												<th>Cuenta clasificadora</th>
												<th>Porcentaje</th>
												<th>
													<div class="d-flex justify-center">
														<button type="button" @click="del()" class="btn btn-danger">x</button>
													</div>
												</th>
											</tr>
										</thead>
										<tbody>
											<tr v-for="(data,index) in arrDetalleIngreso2" :key="index">
												<td>{{ data.cuenta }} - {{ data.nombre_cuenta }}</td>
												<td>{{ data.seccion }} - {{ data.nombre_seccion }}</td>
												<td>{{ data.fuente }} - {{ data.nombre_fuente }}</td>
												<td>{{ data.concepto.codigo+" - "+data.concepto.nombre }}</td>
												<td>{{ data.cuenta_clasificadora }}</td>
												<td>{{ data.porcentaje }}%</td>
												<td>
													<div class="d-flex justify-center">
														<button type="button" @click="del(index)" class="btn btn-danger">x</button>
													</div>
												</td>
											</tr>
										</tbody>
										<tfoot>
											<tr class="bg-secondary fw-bold">
												<td colspan="5" class="text-right">Total:</td>
												<td class="text-center">{{intTotalPorcentaje2}}%</td>
											</tr>
										</tfoot>
									</table>
								</div>
							</div>
						</div>
						<div class="nav-content">
							<div>
								<div>
									<p class="m-2">Todos los campos con (<span class="text-danger fw-bolder">*</span>) son obligatorios</p>
									<div class="w-100 d-flex">
										<div class="w-50 d-flex">
											<div class="form-control">
												<label class="form-label">Nombre <span class="text-danger fw-bolder">*</span>:</label>
												<input type="text" v-model="strNombre3">
											</div>
										</div>
										<div class="w-50 d-flex">
											<div class="form-control">
												<label class="form-label">Precio venta <span class="text-danger fw-bolder">*</span>:</label>
												<input type="number" v-model="intPrecio3" class="text-right">
											</div>
											<div class="form-control">
												<label class="form-label">Terceros <span class="text-danger fw-bolder">*</span>:</label>
												<select v-model="selectTerceros3">
													<option value="2" selected>No</option>
												</select>
											</div>
										</div>
									</div>
									<div class="w-100 d-flex">
										<div class="w-50 d-flex">
											<div class="form-control">
												<label class="form-label">Tipo <span class="text-danger fw-bolder">*</span>:</label>
												<select v-model="selectSimple3">
													<option value="C" selected>Compuesto</option>
												</select>
											</div>
											<div class="form-control">
												<label class="form-label">Destino <span class="text-danger fw-bolder">*</span>:</label>
												<select v-model="selectDestino3">
													<option value="" selected>Otros</option>
													<option value="D" selected>Departamental</option>
													<option value="N">Nacional</option>
													<option value="M">Municipal</option>
												</select>
											</div>
										</div>
										<div class="w-50 d-flex">
											<div class="form-control">
												<label class="form-label">Tipo ingreso <span class="text-danger fw-bolder">*</span>:</label>
												<select v-model="selectTipoIngreso3">
													<option value="" selected>Otro</option>
													<option value="normal" selected>Normal</option>
													<option value="sgr">SGR</option>
													<option value="sgp">SGP</option>
													<option value="salud">Salud ssf</option>
													<option value="educacion">Educacion ssf</option>
												</select>
											</div>
											<div class="form-control">
												<label class="form-label">Contabilizar liquidacion <span class="text-danger fw-bolder">*</span>:</label>
												<select v-model="selectContabilizar3">
													<option value="S" selected>Si</option>
												</select>
											</div>
										</div>
									</div>
								</div>
								<h2 class="titulos m-0">Detalle</h2>
								<div class="w-100 d-flex">
									<div class="w-50 d-flex">
										<div class="form-control">
											<label class="form-label">Concepto contable <span class="text-danger fw-bolder">*</span>:</label>
											<select v-model="selectConcepto3">
												<option disabled selected>Seleccione</option>
												<option v-for="(data,index) in arrConceptos" :key="index" :value="data.codigo">
													{{ data.codigo+"-"+data.nombre}}
												</option>
											</select>
										</div>
										<div class="form-control">
											<label class="form-label">¿Afecta presupuesto? <span class="text-danger fw-bolder">*</span>:</label>
											<select v-model="selectIsPresupuesto3">
												<option value="2">Si</option>
											</select>
										</div>
									</div>
									<div class="w-50 d-flex">
										<div class="form-control">
											<label class="form-label" for="">Cuenta presupuestal<span class="text-danger fw-bolder">*</span>:</label>
											<div class="d-flex">
												<input type="text"  class="colordobleclik w-25" v-model="objCuenta3.codigo" @dblclick="isModal=true" readonly>
												<input type="text" v-model="objCuenta3.nombre" disabled readonly>
											</div>
										</div>
										<div class="form-control" v-show="intType3 == 1">
											<label class="form-label" for="">CUIN <span class="text-danger fw-bolder">*</span>:</label>
											<div class="d-flex">
												<input type="text"  class="colordobleclik w-25" v-model="objCuin3.codigo" @dblclick="isModalCuin=true" readonly>
												<input type="text" v-model="objCuin3.nombre" disabled readonly>
											</div>
										</div>
										<div class="form-control" v-show="intType3 == 2">
											<label class="form-label" for="">Bienes transportables <span class="text-danger fw-bolder">*</span>:</label>
											<div class="d-flex">
												<input type="text"  class="colordobleclik w-25" v-model="objBienes3.codigo" @dblclick="isModalBienes=true" readonly>
												<input type="text" v-model="objBienes3.nombre" disabled readonly>
											</div>
										</div>
										<div class="form-control" v-show="intType3 == 3">
											<label class="form-label" for="">Servicios <span class="text-danger fw-bolder">*</span>:</label>
											<div class="d-flex">
												<input type="text"  class="colordobleclik w-25" v-model="objServicio3.codigo" @dblclick="isModalServicios=true" readonly>
												<input type="text" v-model="objServicio3.nombre" disabled readonly>
											</div>
										</div>
									</div>
								</div>
								<div class="w-100 d-flex">
									<div class="w-50 d-flex">
										<div class="form-control">
											<label class="form-label">Porcentaje <span class="text-danger fw-bolder">*</span>:</label>
											<input type="number" v-model="intPorcentaje3" class="text-right">
										</div>
										<div class="form-control" >
											<label class="form-label">Sección presupuestal <span class="text-danger fw-bolder">*</span>:</label>
											<select v-model="selectSeccion3">
												<option disabled selected>Seleccione</option>
												<option v-for="(data,index) in arrSecciones" :key="index" :value="data.codigo">
													{{ data.codigo+"-"+data.nombre}}
												</option>
											</select>
										</div>
									</div>
									<div class="w-50 d-flex" >
										<div class="form-control" >
											<label class="form-label" for="">Fuentes <span class="text-danger fw-bolder">*</span>:</label>
											<div class="d-flex">
												<input type="text"  class="colordobleclik w-25" v-model="objFuenteComp3.codigo" @dblclick="isModalFuenteComp=true" readonly>
												<input type="text" v-model="objFuenteComp3.nombre" disabled readonly>
											</div>
										</div>
										<div class="form-control justify-between w-25">
											<label for=""></label>
											<button type="button" class="btn btn-primary" @click="add()">Agregar</button>
										</div>
									</div>
								</div>
								<div class="table-responsive" style="max-height:30vh;">
									<table class="table table-hover fw-normal">
										<thead>
											<tr class="text-center">
												<th>Cuenta CCPET</th>
												<th>Seccion presupuestal</th>
												<th>Fuente</th>
												<th>Concepto</th>
												<th>Cuenta clasificadora</th>
												<th>Porcentaje</th>
												<th>
													<div class="d-flex justify-center">
														<button type="button" @click="del()" class="btn btn-danger">x</button>
													</div>
												</th>
											</tr>
										</thead>
										<tbody>
											<tr v-for="(data,index) in arrDetalleIngreso3" :key="index">
												<td>{{ data.cuenta }} - {{ data.nombre_cuenta }}</td>
												<td>{{ data.seccion }} - {{ data.nombre_seccion }}</td>
												<td>{{ data.fuente }} - {{ data.nombre_fuente }}</td>
												<td>{{ data.concepto.codigo+" - "+data.concepto.nombre }}</td>
												<td>{{ data.cuenta_clasificadora }}</td>
												<td>{{ data.porcentaje }}%</td>
												<td>
													<div class="d-flex justify-center">
														<button type="button" @click="del(index)" class="btn btn-danger">x</button>
													</div>
												</td>
											</tr>
										</tbody>
										<tfoot>
											<tr class="bg-secondary fw-bold">
												<td colspan="5" class="text-right">Total:</td>
												<td class="text-center">{{intTotalPorcentaje3}}%</td>
											</tr>
										</tfoot>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</article>
			<!--MODALES-->
			<div v-show="isModal">
				<transition name="modal">
					<div class="modal-mask">
						<div class="modal-wrapper">
							<div class="modal-container">
								<table class="inicio ancho">
									<tr>
										<td class="titulos" colspan="2" >.: Buscar Cuentas Presupuestales</td>
										<td class="cerrar" style="width:7%" @click="isModal = false">Cerrar</td>
									</tr>
									<tr>
										<td>
											<input type="search" v-model="strSearch" @keyup="search('modal_cuenta')" placeholder="Buscar por cuenta o descripción" style="width: 100%;">
										</td>
									</tr>
									<tr>
										<td class="titulos" colspan="3" >.: Resultados de búsqueda</td>
									</tr>
									<tr>
										<td>Total: {{ intResults}}</td>
									</tr>
								</table>
								<table class='tablamv'>
									<thead>
										<tr>
											<th style="width:25%" class="titulosnew00" >Cuenta</th>
											<th style="width:55%" class="titulosnew00" >Descripción</th>
											<th style="width:10%" class="titulosnew00" >Tipo</th>
										</tr>
									</thead>
									<tbody>
										<tr v-for="(data,index) in arrModalCuentasCopy" @click="selectItem(data,'cuenta');isModal=false;"  :key="index" v-bind:class="index % 2 ? 'saludo1a' : 'saludo2'">
											<td style="width:25%">{{ data.codigo}}</td>
											<td style="width:55%">{{ data.nombre}}</td>
											<td align="center" style="width:10%">{{ data.tipo}}</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</transition>
			</div>
			<div v-show="isModalServicios">
				<transition name="modal">
					<div class="modal-mask">
						<div class="modal-wrapper">
							<div class="modal-container">
								<table class="inicio ancho">
									<tr>
										<td class="titulos" colspan="2" >.: Buscar Servicios</td>
										<td class="cerrar" style="width:7%" @click="isModalServicios = false">Cerrar</td>
									</tr>
									<tr>
										<td>
											<input type="search" v-model="strSearch" @keyup="search('modal_servicio')"placeholder="Buscar por código o nombre" style="width: 100%;">
										</td>
									</tr>
									<tr>
										<td class="titulos" colspan="3" >.: Resultados de búsqueda</td>
									</tr>
									<tr>
										<td>Total: {{ intResultsServicios}}</td>
									</tr>
								</table>
								<table class='tablamv'>
									<thead>
										<tr>
											<th style="width:25%" class="titulosnew00" >Código</th>
											<th style="width:65%" class="titulosnew00" >Nombre</th>
										</tr>
									</thead>
									<tbody>
										<tr v-for="(data,index) in arrModalServicios" @click="selectItem(data,'servicio');isModalServicios=false;"  :key="index" v-bind:class="index % 2 ? 'saludo1a' : 'saludo2'">
											<td style="width:25%">{{ data.codigo}}</td>
											<td style="width:65%">{{ data.nombre}}</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</transition>
			</div>
			<div v-show="isModalBienes">
				<transition name="modal">
					<div class="modal-mask">
						<div class="modal-wrapper">
							<div class="modal-container">
								<table class="inicio ancho">
									<tr>
										<td class="titulos" colspan="2" >.: Buscar bienes transportables</td>
										<td class="cerrar" style="width:7%" @click="isModalBienes = false">Cerrar</td>
									</tr>
									<tr>
										<td>
											<input type="search" v-model="strSearch" @keyup="search('modal_bienes')"placeholder="Buscar por código o nombre" style="width: 100%;">
										</td>
									</tr>
									<tr>
										<td class="titulos" colspan="3" >.: Resultados de búsqueda</td>
									</tr>
									<tr>
										<td>Total: {{ intResultsBienes}}</td>
									</tr>
								</table>
								<table class='tablamv'>
									<thead>
										<tr>
											<th style="width:25%" class="titulosnew00" >Código</th>
											<th style="width:65%" class="titulosnew00" >Nombre</th>
										</tr>
									</thead>
									<tbody>
										<tr v-for="(data,index) in arrModalBienes" @click="selectItem(data,'bienes');isModalBienes=false;"  :key="index" v-bind:class="index % 2 ? 'saludo1a' : 'saludo2'">
											<td style="width:25%">{{ data.codigo}}</td>
											<td style="width:65%">{{ data.nombre}}</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</transition>
			</div>
			<div v-show="isModalCuin">
				<transition name="modal">
					<div class="modal-mask">
						<div class="modal-wrapper">
							<div class="modal-container">
								<table class="inicio ancho">
									<tr>
										<td class="titulos" colspan="2" >.: Buscar CUIN</td>
										<td class="cerrar" style="width:7%" @click="isModalCuin = false">Cerrar</td>
									</tr>
									<tr>
										<td>
											<input type="search" v-model="strSearch" @keyup="search('modal_cuin')"placeholder="Buscar por código o nombre" style="width: 100%;">
										</td>
									</tr>
									<tr>
										<td class="titulos" colspan="3" >.: Resultados de búsqueda</td>
									</tr>
									<tr>
										<td>Total: {{ intResultsCuin}}</td>
									</tr>
								</table>
								<table class='tablamv'>
									<thead>
										<tr>
											<th style="width:25%" class="titulosnew00" >Código</th>
											<th style="width:65%" class="titulosnew00" >Nombre</th>
										</tr>
									</thead>
									<tbody>
										<tr v-for="(data,index) in arrModalCuin" @click="selectItem(data,'cuin');isModalCuin=false;"  :key="index" v-bind:class="index % 2 ? 'saludo1a' : 'saludo2'">
											<td style="width:25%">{{ data.codigo}}</td>
											<td style="width:65%">{{ data.nombre}}</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</transition>
			</div>
			<div v-show="isModalFuente">
				<transition name="modal">
					<div class="modal-mask">
						<div class="modal-wrapper">
							<div class="modal-container">
								<table class="inicio ancho">
									<tr>
										<td class="titulos" colspan="2" >.: Buscar fuente</td>
										<td class="cerrar" style="width:7%" @click="isModalFuente = false">Cerrar</td>
									</tr>
									<tr>
										<td>
											<input type="search" v-model="strSearch" @keyup="search('modal_fuente')"placeholder="Buscar por código o nombre" style="width: 100%;">
										</td>
									</tr>
									<tr>
										<td class="titulos" colspan="3" >.: Resultados de búsqueda</td>
									</tr>
									<tr>
										<td>Total: {{ intResultsFuente}}</td>
									</tr>
								</table>
								<table class='tablamv'>
									<thead>
										<tr>
											<th style="width:25%" class="titulosnew00" >Código</th>
											<th style="width:65%" class="titulosnew00" >Nombre</th>
										</tr>
									</thead>
									<tbody>
										<tr v-for="(data,index) in arrModalFuente" @click="selectItem(data,'fuente');isModalFuente=false;"  :key="index" v-bind:class="index % 2 ? 'saludo1a' : 'saludo2'">
											<td style="width:25%">{{ data.codigo}}</td>
											<td style="width:65%">{{ data.nombre}}</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</transition>
			</div>
			<div v-show="isModalFuenteComp">
				<transition name="modal">
					<div class="modal-mask">
						<div class="modal-wrapper">
							<div class="modal-container">
								<table class="inicio ancho">
									<tr>
										<td class="titulos" colspan="2" >.: Buscar fuente</td>
										<td class="cerrar" style="width:7%" @click="isModalFuenteComp = false">Cerrar</td>
									</tr>
									<tr>
										<td>
											<input type="search" v-model="strSearch" @keyup="search('modal_fuente')"placeholder="Buscar por código o nombre" style="width: 100%;">
										</td>
									</tr>
									<tr>
										<td class="titulos" colspan="3" >.: Resultados de búsqueda</td>
									</tr>
									<tr>
										<td>Total: {{ intResultsFuente}}</td>
									</tr>
								</table>
								<table class='tablamv'>
									<thead>
										<tr>
											<th style="width:25%" class="titulosnew00" >Código</th>
											<th style="width:65%" class="titulosnew00" >Nombre</th>
										</tr>
									</thead>
									<tbody>
										<tr v-for="(data,index) in arrModalFuenteCopy" @click="selectItem(data,'fuenteComp');isModalFuenteComp=false;"  :key="index" v-bind:class="index % 2 ? 'saludo1a' : 'saludo2'">
											<td style="width:25%">{{ data.codigo}}</td>
											<td style="width:65%">{{ data.nombre}}</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</transition>
			</div>
		</section>

		<script src="Librerias/vue/vue.min.js"></script>
		<script type="module" src="transporte/js/functions_tramites.js?<?= date('d_m_Y_h_i_s');?>"></script>

	</body>
</html>
