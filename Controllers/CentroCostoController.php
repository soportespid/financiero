<?php
/* include_once ($_SERVER['DOCUMENT_ROOT'].'/financiero/dirs.php');
require_once (MODELS_PATH.'CentroCosto.php');
require_once (ROOT_PATH.'conexion.php'); */

class CentroCostoController
{
    public $cc;
    public function __construct()
    {
    }

    public function generarCentroCosto()
    {
        $detalles = [];

        $linkbd = conectar_v7();
        $linkbd -> set_charset("utf8");

        $sqlrD = "SELECT * FROM centrocosto WHERE estado = 'S' AND entidad = 'S'";
        $resD = mysqli_query($linkbd, $sqlrD);
        while($rowD = mysqli_fetch_assoc($resD)){
            array_push($detalles, $rowD);
        }

        $this->cc = $detalles;
    }
}
