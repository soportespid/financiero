<?php
/* include_once ($_SERVER['DOCUMENT_ROOT'].'/financiero/dirs.php'); */
require_once './../Models/TesoParametros.php';
require_once './../Models/TesoIngresos.php';
require_once './../Models/Dominios.php';
require_once './../Models/CuentasNicsp.php';
require_once './../comun.inc';
/* require_once (MODELS_PATH.'Transaccion.php');
require_once (ROOT_PATH.'conexion.php'); */
/**
 * Clase controlador de tesoreria
 * Importa modelos relacionados a tesoreria
 */
class  TesoController{
	public function __construct(){}

	/**
	 * Función para buscar información de tesoreria
	 * Se evalua los detalles a consultar
	 * Se retorna información de los parametros
	 * @param Array $datos
	 */
	public function buscarParametrosTeso($datos=''){
		
		$resTesoParametros = array();
		if(is_array($datos)){
			array_shift($datos);
			
			foreach ($datos['parametros'] as $field => $value) {
				switch($value){
					case 'TESOPARAMETROS':
						$condiciones = [
							'estado' => 'S'
						];
						$res = TesoParametros::parametros($condiciones);
						$resTesoParametros[$value] = $res;
					break;
					case 'TESOINGRESOS':
						$condiciones = [
							'estado' => 'S'
						];
						$res = TesoIngresos::ingresos($condiciones);
						//var_dump($res);
						$resTesoParametros[$value] = $res;
					break;
					case 'BASE_PREDIAL':
					case 'BASE_PREDIALAMB':
					case 'COBRO_RECIBOS':
					case 'COBRO_ALUMBRADO':
					case 'CUENTA_MILES':
					case 'CUENTA_TRASLADO':
					case 'DESC_INTERESES':
					case 'DESCUENTO_CON_DEUDA':
					case 'NORMA_PREDIAL':
						$condiciones = [
							'nombre_dominio' => $value
						];
						$res = Dominios::dominio($condiciones);
						$resTesoParametros[$value] = $res;
					break;
				}
			}
		}
		return $resTesoParametros;
	}

	/**
	 * Función para buscar información de tesoreria
	 * Se evalua los detalles a consultar
	 * Se retorna información de los parametros
	 * @param Array $datos
	 */
	public function buscarCuentasTeso($datos=''){
		$almTesoCuentas = null;
		$resTesoCuentas = array();
		if(is_array($datos)){
			array_shift($datos);
			foreach ($datos as $field => $value) {
				$condiciones=[
					'cuenta' => $value
				];
				$resTesoCuentas[$field] = CuentasNicsp::cuentaNicsp($condiciones);
				
			}
		}
		return $resTesoCuentas;
	}

	/**
	 * Función para guardar información de los parametros del control activos físico
	 * Se evalua si existe parametros
	 * Se retorna valor para validación de la operación
	 * @param Array $datos
	 */
	public function guardarParametrosTeso($datos=''){
		$verifTesoParametros = $almTesoParametros = null;
		if(is_array($datos)){
			array_shift($datos);
			foreach ($datos as $field => $value){
				switch ($field) {
					case 'BASE_PREDIAL':
					case 'BASE_PREDIALAMB':
					case 'CUENTA_TRASLADO':
					case 'DESCUENTO_CON_DEUDA':
					case 'NORMA_PREDIAL':
						
						$condiciones = ['nombre_dominio' => $field];
						$actualizacion = ['valor_inicial' => @$value];

						if(count(Dominios::dominio($condiciones))>0){
							Dominios::updateDominio($condiciones, $actualizacion);
						}else{
							$datosSave = [
										'nombre_dominio' => $field,
										'valor_inicial' => @$value
									 ];
							Dominios::saveDominio($datosSave);

							/* $almDominio = new Dominios();
							$almDominio -> valor_inicial = @$value;
							$almDominio -> nombre_dominio = $field;
							$almDominio -> save(); */
						}
						unset($datos[$field]);
						break;
					case 'CUENTA_MILES':
						$condiciones = ['nombre_dominio' => $field];
						$actualizacion = ['valor_inicial' => @$value[0], 'descripcion_valor' => @$value[1]];

						if(count(Dominios::dominio($condiciones))>0){
							Dominios::updateDominio2($condiciones, $actualizacion);
							//Dominios::where($condiciones)->update($actualizacion);
						}else{
							$datosSave = [
											'nombre_dominio' => $field,
											'descripcion_valor' => @$value[1],
											'valor_inicial' => @$value[0]
										 ];
							Dominios::saveDominio2($datosSave);

							/* $almDominio = new Dominios();
							$almDominio -> valor_inicial = @$value[0];
							$almDominio -> descripcion_valor = @$value[1];
							$almDominio -> nombre_dominio = $field;
							$almDominio -> save(); */
						}
						unset($datos[$field]);
						break;
					case 'COBRO_RECIBOS':
					case 'DESC_INTERESES':
					case 'COBRO_ALUMBRADO':
						$condiciones = ['nombre_dominio' => $field];
						$actualizacion = ['valor_inicial' => @$value[0], 'valor_final' => @$value[1], 'tipo' => @$value[2]];

						if(count(Dominios::dominio($condiciones))>0){
							Dominios::updateDominio3($condiciones, $actualizacion);
							//$almDominio = Dominios::where($condiciones)->orderBy('descripcion_valor','desc')->take(1);
							//$almDominio->update($actualizacion);
						} else {
							$datosSave = [
											'nombre_dominio' => $field,
											'valor_inicial' => @$value[0],
											'valor_final' => @$value[1],
											'tipo' => @$value[2]
							 			 ];
							Dominios::saveDominio3($datosSave);
							/* $almDominio = new Dominios();
							$almDominio -> valor_inicial = @$value[0];
							$almDominio -> valor_final = @$value[1];
							$almDominio -> tipo = @$value[2];
							$almDominio -> nombre_dominio = $field;
							$almDominio -> save(); */
						}
						unset($datos[$field]);
						break;
				}
			}
			
			//$almTesoParametros = TesoParametros::findParametro($datos['id']);
			
			/* if(!$almTesoParametros){

				$almTesoParametros = [];
				foreach ($datos as $field => $value){
					echo $value;
					array_push($almTesoParametros[$value], $datos[$field]);
					//$almTesoParametros -> $field = $datos[$field];
				}
			} */
			$verifTesoParametros = TesoParametros::saveParametros($datos);
			if($verifTesoParametros == 1)
				return 0;

		}
	}
}