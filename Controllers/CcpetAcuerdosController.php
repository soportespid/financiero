<?php
include_once ($_SERVER['DOCUMENT_ROOT'].'/financiero/dirs.php');
require_once (MODELS_PATH.'CcpetAcuerdos.php');
require_once (ROOT_PATH.'conexion.php');

class CcpetAcuerdosController{

	public function __construct(){}

	public function buscarAcuerdos($datos=''){
		$resAcuerdos = null;
		if(is_array($datos)){
			if(intval(@$datos['consecutivo']) == -1)
				$resAcuerdos = CcpetAcuerdos::where('estado','!=','A')
				->orderBy('fecha','desc')->orderBy('vigencia','desc')->get();
			else{
				array_shift($datos);
				$sql = 'CcpetAcuerdos::';
				$init = false;
				foreach ($datos as $field => $value) {
					if($field == 'sql_like'){
						list($field,$value) = explode('=',$datos['sql_like']);
						($init) ? $sql = $sql.'->' : $init = true;
						$sql = $sql."where('$field','LIKE','%$value%')";
						unset($datos['sql_like']);
					} else if($field == 'sql_between'){
						preg_match_all("/\d{4}\-\d{1,2}\-\d{1,2}/",$datos['sql_between'], $dates);
						($init) ? $sql = $sql.'->' : $init = true;
						$sql = $sql."whereBetween('fecha',['".$dates[0][0]."','".$dates[0][1]."'])";
						unset($datos['sql_between']);
					}
				}

				if(count(@$datos) > 0){
					($init) ? $sql = $sql.'->' : $init = true;
					$data = str_replace(["{","}",":"],["[","]","=>"], json_encode($datos));
					$sql = $sql."where($data)->get();";
				}else{
					($init) ? $sql = $sql.'->' : $init = true;
					$sql = $sql."where([])->get();";
				}

				eval('$resAcuerdos = '.$sql.';');
			}
		}
		return $resAcuerdos;
	}

	public function guardarAcuerdos($datos){
		$fecha = [];
		preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/",$_POST['fecha'],$fecha);
		if(is_array($datos)){
			if ($datos['proceso'] == 'CCPETACUERDOS_GUARDAR'){
				$almCcpetAcuerdos = new CcpetAcuerdos();
				$almCcpetAcuerdos -> consecutivo = $datos['consecutivo'];
				$almCcpetAcuerdos -> numero_acuerdo = $datos['acuerdo'];
				$almCcpetAcuerdos -> fecha = "$fecha[3]-$fecha[2]-$fecha[1]";
				$almCcpetAcuerdos -> vigencia = $fecha[3];
				$almCcpetAcuerdos -> estado = $datos['estado'];
				$almCcpetAcuerdos -> tipo = $datos['tipo'];
				$almCcpetAcuerdos -> tipo_acto_adm = $datos['tipo_acto_adm'];
				$almCcpetAcuerdos -> valorinicial = $datos['valor_inicial'];
				$almCcpetAcuerdos -> valoradicion = $datos['valor_adicion'];
				$almCcpetAcuerdos -> valorreduccion = $datos['valor_reduccion'];
				$almCcpetAcuerdos -> valortraslado = $datos['valor_traslado'];
				$verifPptoAcuerdos = $almCcpetAcuerdos -> save();
			} else if ($datos['proceso'] == 'CCPETACUERDOS_EDITAR'){
				$almCcpetAcuerdos = CcpetAcuerdos::find($datos['id_acuerdo']);
				$almCcpetAcuerdos -> consecutivo = $datos['consecutivo'];
				$almCcpetAcuerdos -> numero_acuerdo = $datos['acuerdo'];
				$almCcpetAcuerdos -> fecha = "$fecha[3]-$fecha[2]-$fecha[1]";
				$almCcpetAcuerdos -> vigencia = $fecha[3];
				$almCcpetAcuerdos -> estado = $datos['estado'];
				$almCcpetAcuerdos -> tipo = $datos['tipo'];
				$almCcpetAcuerdos -> tipo_acto_adm = $datos['tipo_acto_adm'];
				$almCcpetAcuerdos -> valorinicial = $datos['valor_inicial'];
				$almCcpetAcuerdos -> valoradicion = $datos['valor_adicion'];
				$almCcpetAcuerdos -> valorreduccion = $datos['valor_reduccion'];
				$almCcpetAcuerdos -> valortraslado = $datos['valor_traslado'];
				$verifPptoAcuerdos = $almCcpetAcuerdos -> save();
			}

			if($verifPptoAcuerdos == 1)
				return 0;
		}
	}

	public function anularAcuerdos($datos){
		if(is_array($datos)){
			$almPptoAcuerdos = CcpetAcuerdos::find($datos['id_acuerdo']);
			$almPptoAcuerdos -> estado = $datos['estado'];
			$verifPptoAcuerdos = $almPptoAcuerdos -> save();
			if($verifPptoAcuerdos == 1)
				return 0;
		}
	}
}
?>
