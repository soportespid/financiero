<?php
//include_once ($_SERVER['DOCUMENT_ROOT'].'/financiero/dirs.php');
require_once './Models/TesoGastosBancarios.php';
//require_once (ROOT_PATH.'conexion.php');

class GastoBancarioController
{
    private $gastoBancario;
    public function __construct()
    {
        $this->generarGastoBancario();
    }

    private function generarGastoBancario()
    {
        $condiciones = ['estado' => 'S'];
        $this->gastoBancario = TesoGastosBancarios::gastosBancarios($condiciones);
    }

    public function getGastoBancario()
    {
        return $this->gastoBancario;
    }
}