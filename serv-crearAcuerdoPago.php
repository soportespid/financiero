<?php

    header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require 'comun.inc';
	require 'funciones.inc';
	session_start();
    if(empty($_SESSION)){
        header("location: index.php");
    }
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html lang=es>
	<head>
    <meta charset="utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1"/>
		<meta name="viewport" content="user-scalable=no">
		<title>:: IDEAL 10 - Servicios publicos</title>
		<link href="favicon.ico" rel="shortcut icon"/>
		<link href="css/css2z.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
        <link href="css/tabs.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
        <link href="css/style.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" href="sweetalert2/dist/sweetalert2.min.css">
        <script type="text/javascript" src="css/programas.js"></script>
        <script src="sweetalert2/dist/sweetalert2.min.js"></script>
		<script src="vue/vue.min.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script src="css/calendario.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
	</head>
	<body>
		<header>
			<table>
				<tr><script>barra_imagenes("serv");</script><?php cuadro_titulos();?></tr>
			</table>
		</header>

        <section id="myapp" v-cloak>
            <div class="loading-container" v-show="isLoading" >
                <p class="text-loading"data-loading="IDEAL 10">IDEAL 10</p>
            </div>
            <nav>
                <table>
                    <tr><?php menu_desplegable("serv");?></tr>
                </table>
                <div class="bg-white group-btn p-1" id="newNavStyle">
                    <button type="button" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center" @click="window.location.reload()">
                        <span>Nuevo</span>
                        <svg viewBox="0 -960 960 960" ><path d="M440-440H200v-80h240v-240h80v240h240v80H520v240h-80v-240Z"/></svg>
                    </button>
                    <button type="button" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center" @click="save()">
                        <span>Guardar</span>
                        <svg viewBox="0 -960 960 960" ><path d="M840-680v480q0 33-23.5 56.5T760-120H200q-33 0-56.5-23.5T120-200v-560q0-33 23.5-56.5T200-840h480l160 160Zm-80 34L646-760H200v560h560v-446ZM480-240q50 0 85-35t35-85q0-50-35-85t-85-35q-50 0-85 35t-35 85q0 50 35 85t85 35ZM240-560h360v-160H240v160Zm-40-86v446-560 114Z"/></svg>
                    </button>
                    <button type="button" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center" @click="location.href='serv-buscarAcuerdoPago'">
                        <span>Buscar</span>
                        <svg class="fill-black group-hover:fill-white w-5 h-5" viewBox="0 -960 960 960" ><path d="M784-120 532-372q-30 24-69 38t-83 14q-109 0-184.5-75.5T120-580q0-109 75.5-184.5T380-840q109 0 184.5 75.5T640-580q0 44-14 83t-38 69l252 252-56 56ZM380-400q75 0 127.5-52.5T560-580q0-75-52.5-127.5T380-760q-75 0-127.5 52.5T200-580q0 75 52.5 127.5T380-400Z"/></svg>
                    </button>
                    <button type="button" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center" @click="mypop=window.open('serv-principal.php','',''); mypop.focus();">
                        <span>Nueva ventana</span>
                        <svg class="fill-black group-hover:fill-white w-5 h-5" viewBox="0 -960 960 960" ><path d="M320-120v-80h80v-80H160q-33 0-56.5-23.5T80-360v-400q0-33 23.5-56.5T160-840h640q33 0 56.5 23.5T880-760v400q0 33-23.5 56.5T800-280H560v80h80v80H320ZM160-360h640v-400H160v400Zm0 0v-400 400Z"/></svg>
                    </button>
                    <button type="button" class="btn btn-danger btn-danger-hover d-flex justify-between align-items-center" @click="pdf()">
                        <span>Exportar PDF</span>
                        <svg viewBox="0 0 512 512"><path d="M64 464l48 0 0 48-48 0c-35.3 0-64-28.7-64-64L0 64C0 28.7 28.7 0 64 0L229.5 0c17 0 33.3 6.7 45.3 18.7l90.5 90.5c12 12 18.7 28.3 18.7 45.3L384 304l-48 0 0-144-80 0c-17.7 0-32-14.3-32-32l0-80L64 48c-8.8 0-16 7.2-16 16l0 384c0 8.8 7.2 16 16 16zM176 352l32 0c30.9 0 56 25.1 56 56s-25.1 56-56 56l-16 0 0 32c0 8.8-7.2 16-16 16s-16-7.2-16-16l0-48 0-80c0-8.8 7.2-16 16-16zm32 80c13.3 0 24-10.7 24-24s-10.7-24-24-24l-16 0 0 48 16 0zm96-80l32 0c26.5 0 48 21.5 48 48l0 64c0 26.5-21.5 48-48 48l-32 0c-8.8 0-16-7.2-16-16l0-128c0-8.8 7.2-16 16-16zm32 128c8.8 0 16-7.2 16-16l0-64c0-8.8-7.2-16-16-16l-16 0 0 96 16 0zm80-112c0-8.8 7.2-16 16-16l48 0c8.8 0 16 7.2 16 16s-7.2 16-16 16l-32 0 0 32 32 0c8.8 0 16 7.2 16 16s-7.2 16-16 16l-32 0 0 48c0 8.8-7.2 16-16 16s-16-7.2-16-16l0-64 0-64z"/></svg>
                    </button>
                    <!-- <button type="button" class="btn btn-success btn-success-hover d-flex justify-between align-items-center" @click="">
                        <span>Exportar Excel</span>
                        <svg class="fill-black group-hover:fill-white w-4 h-4" viewBox="0 0 384 512"><path d="M48 448V64c0-8.8 7.2-16 16-16H224v80c0 17.7 14.3 32 32 32h80V448c0 8.8-7.2 16-16 16H64c-8.8 0-16-7.2-16-16zM64 0C28.7 0 0 28.7 0 64V448c0 35.3 28.7 64 64 64H320c35.3 0 64-28.7 64-64V154.5c0-17-6.7-33.3-18.7-45.3L274.7 18.7C262.7 6.7 246.5 0 229.5 0H64zm90.9 233.3c-8.1-10.5-23.2-12.3-33.7-4.2s-12.3 23.2-4.2 33.7L161.6 320l-44.5 57.3c-8.1 10.5-6.3 25.5 4.2 33.7s25.5 6.3 33.7-4.2L192 359.1l37.1 47.6c8.1 10.5 23.2 12.3 33.7 4.2s12.3-23.2 4.2-33.7L222.4 320l44.5-57.3c8.1-10.5 6.3-25.5-4.2-33.7s-25.5-6.3-33.7 4.2L192 280.9l-37.1-47.6z"/></svg>
                    </button>
                    <button type="button" class="btn btn-success d-flex justify-between align-items-center" @click="">
                        <span>Atrás</span>
                        <svg viewBox="0 -960 960 960"><path d="M760-200v-160q0-50-35-85t-85-35H273l144 144-57 56-240-240 240-240 57 56-144 144h367q83 0 141.5 58.5T840-360v160h-80Z"/></svg>
                    </button> -->
                </div>
            </nav>
            <article>
                <div class="bg-white">
                    <div>
                        <h2 class="titulos m-0">Tipo de movimiento</h2>
                        <div class="d-flex">
                            <div class="form-control">
                                <select class="w-25" v-model="tipoMov">
                                    <option value="101">Crear acuerdo</option>
                                    <option value="102">Anular acuerdo</option>
                                    <option value="103">Finalizar acuerdo</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="bg-white" v-if="tipoMov == '101'">
                    <div>
                        <h2 class="titulos m-0">Crear acuerdo de pago</h2>
                        <p class="m-0 ms-2">Todos los campos con (<span class="text-danger fw-bolder">*</span>) son obligatorios</p>
                        <div class="w-75">
                            <div class="d-flex">
                                <div class="form-control">
                                    <label class="form-label m-0" for="">Consecutivo acuerdo: </label>
                                    <input type="text" class="text-center" v-model="codAcuerdo" readonly>
                                </div>

                                <div class="form-control">
                                    <label class="form-label m-0" for="">Fecha recaudo <span class="text-danger fw-bolder">*</span>:</label>
                                    <input type="date" class="text-center" v-model="fechaAcuerdo">
                                </div>

                                <div class="form-control">
                                    <label class="form-label m-0" for="">Número de factura <span class="text-danger fw-bolder">*</span>:</label>
                                    <div class="d-flex">
                                        <input type="number" class="text-center" v-model="numFactura" v-on:keyup.enter="searchDataFactura">
                                    </div>
                                </div>
                            </div>

                            <div class="d-flex">
                                <div class="form-control w-25">
                                    <label class="form-label m-0" for="">Codigo de usuario: </label>
                                    <input type="text" class="text-center" v-model="dataUsu.codUsuario" readonly>
                                </div>

                                <div class="form-control w-25">
                                    <label class="form-label m-0" for="">Documento:</label>
                                    <input type="text" class="text-center" v-model="dataUsu.documento" readonly>
                                </div>

                                <div class="form-control">
                                    <label class="form-label m-0" for="">Nombre tercero:</label>
                                    <div class="d-flex">
                                        <input type="text" v-model="dataUsu.name" readonly>
                                    </div>
                                </div>
                            </div>

                            <div class="d-flex">
                                <div class="form-control">
                                    <label class="form-label m-0" for="">Valor factura:</label>
                                    <div class="form-number number-primary">
                                        <span></span>
                                        <input type="text" class="text-center" v-model="formatNumero(valorFactura)" readonly>
                                    </div>
                                </div>

                                <div class="form-control align-items-center">
                                    <label class="form-label m-0" for="">Descuento de intereses:</label>
                                    <label class="form-switch" @change="changeStateDiscount">
                                        <input type="checkbox" v-model="descuentoIntereses">
                                        <span></span>
                                    </label>
                                </div>

                                <div class="form-control" v-if="descuentoIntereses == true">
                                    <label class="form-label m-0" for="">Porcentaje descuento:</label>
                                    <select v-model="porcentajeDescuento" @change="calculateDiscount">
                                        <option value="0">Seleccione</option>
                                        <option v-bind:value="10">10%</option>
                                        <option v-bind:value="20">20%</option>
                                        <option v-bind:value="30">30%</option>
                                        <option v-bind:value="40">40%</option>
                                        <option v-bind:value="50">50%</option>
                                        <option v-bind:value="60">60%</option>
                                        <option v-bind:value="70">70%</option>
                                        <option v-bind:value="80">80%</option>
                                        <option v-bind:value="90">90%</option>
                                        <option v-bind:value="100">100%</option>
                                    </select>
                                </div>

                                <div class="form-control" v-if="descuentoIntereses == false">
                                    <label class="form-label m-0" for="">Porcentaje descuento:</label>
                                    <select v-model="porcentajeDescuento">
                                        <option value="0">Seleccione</option>
                                    </select>
                                </div>
                            </div>

                            <div class="d-flex">
                                <div class="form-control">
                                    <label class="form-label m-0" for="">Valor factura para Acuerdo:</label>
                                    <div class="form-number number-primary">
                                        <span></span>
                                        <input type="text" class="text-center" v-model="formatNumero(valorFacturaAcuerdo)" readonly>
                                    </div>
                                </div>

                                <div class="form-control">
                                    <label class="form-label m-0" for="">Valor abono <span class="text-danger fw-bolder">*</span>:</label> 
                                    <div class="form-number number-primary">
                                        <span></span>
                                        <input type="number" class="text-center" v-model="valorAbono" v-on:keyup.enter="calculateValorAcuerdo">
                                    </div>
                                </div>

                                <div class="form-control">
                                    <label class="form-label m-0" for="">Valor acuerdo:</label>
                                    <div class="form-number number-primary">
                                        <span></span>
                                        <input type="text" class="text-center" v-model="formatNumero(valorAcuerdo)" readonly>
                                    </div>
                                </div>

                                <div class="form-control">
                                    <label class="form-label m-0" for="">Numero de cuotas <span class="text-danger fw-bolder">*</span>:</label>
                                    <input type="number" class="text-center" v-model="numeroCuotas" v-on:keyup.enter="calculateValueCuota">
                                </div>

                                <div class="form-control">
                                    <label class="form-label m-0" for="">Valor cuota:</label>
                                    <div class="form-number number-primary">
                                        <span></span>
                                        <input type="text" class="text-center" v-model="formatNumero(valorCuota)" readonly>
                                    </div>
                                </div>
                            </div>

                            <div class="d-flex">
                                <div class="form-control w-25">
                                    <label class="form-label m-0" for="">Medio de pago <span class="text-danger fw-bolder">*</span>:</label>
                                    <select v-model="medioPago">
                                        <option value="">Seleccione</option>
                                        <option value="banco">Banco</option>
                                        <option value="caja">Caja</option>
                                    </select>
                                </div>

                                <div class="form-control" v-if="medioPago == 'banco'">
                                    <label class="form-label m-0" for="">Banco <span class="text-danger fw-bolder">*</span>:</label>
                                    <select v-model="banco">
                                        <option value="">Seleccione</option>
                                        <option v-for="data in bancos" v-bind:value="data.cuentaContable">
                                            {{ data.cuentaBancaria+"-"+data.nombre }}
                                        </option>
                                    </select>
                                </div>

                                <div class="form-control" v-if="medioPago != 'banco'">
                                    <label class="form-label m-0" for="">Banco <span class="text-danger fw-bolder">*</span>:</label>
                                    <select v-model="banco">
                                        <option value="">Seleccione</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>                       
                </div>

                <div class="bg-white" v-if="tipoMov == '102'">
                    <div>
                        <h2 class="titulos m-0">Anular acuerdo de pago</h2>
                        <p class="m-0 ms-2">Todos los campos con (<span class="text-danger fw-bolder">*</span>) son obligatorios</p>

                        <div>
                            <div class="d-flex">
                                <div class="form-control">
                                    <label class="form-label m-0" for="">Consecutivo acuerdo <span class="text-danger fw-bolder">*</span>:</label>
                                    <input type="number" class="text-center" v-model="codAcuerdoRev" v-on:keyup.enter="searchAcuerdo">
                                </div>

                                <div class="form-control">
                                    <label class="form-label m-0" for="">Codigo de usuario:</label>
                                    <input type="text" class="text-center" v-model="dataUsuRev.codUsu" readonly>
                                </div>

                                <div class="form-control">
                                    <label class="form-label m-0" for="">Número de factura:</label>
                                    <div class="d-flex">
                                        <input type="text" class="text-center" v-model="dataUsuRev.numFactura" readonly>
                                    </div>
                                </div>

                                <div class="form-control">
                                    <label class="form-label m-0" for="">Valor de abono:</label>
                                    <div class="form-number number-primary">
                                        <span></span>
                                        <input type="text" class="text-center" v-model="formatNumero(valorRev)" readonly>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="bg-white" v-if="tipoMov == '103'">
                    <div>
                        <h2 class="titulos m-0">Finalizar acuerdo de pago</h2>
                        <p class="m-0 ms-2">Todos los campos con (<span class="text-danger fw-bolder">*</span>) son obligatorios</p>

                        <div>
                            <div class="d-flex">
                                <div class="form-control">
                                    <label class="form-label m-0" for="">Consecutivo acuerdo <span class="text-danger fw-bolder">*</span>:</label>
                                    <input type="number" class="text-center" v-model="codAcuerdoFinalizar" v-on:keyup.enter="searchAcuerdoFinalizar">
                                </div>

                                <div class="form-control">
                                    <label class="form-label m-0" for="">Codigo de usuario:</label>
                                    <input type="text" class="text-center" v-model="dataUsuFinalizar.codUsuario" readonly>
                                </div>

                                <div class="form-control">
                                    <label class="form-label m-0" for="">Numero factura:</label>
                                    <input type="text" class="text-center" v-model="dataUsuFinalizar.numFactura" readonly>
                                </div>

                                <div class="form-control">
                                    <label class="form-label m-0" for="">Valor acuerdo:</label>
                                    <div class="form-number number-primary">
                                        <span></span>
                                        <input type="text" class="text-center" v-model="dataUsuFinalizar.valorAcuerdo" readonly>
                                    </div>
                                </div>
                            </div>

                            <div class="d-flex">
                                <div class="form-control">
                                    <label class="form-label m-0" for="">Numero de cuotas:</label>
                                    <input type="text" class="text-center" v-model="dataUsuFinalizar.numCuotas" readonly>
                                </div>

                                <div class="form-control">
                                    <label class="form-label m-0" for="">Valor cuota:</label>
                                    <div class="form-number number-primary">
                                        <span></span>
                                        <input type="text" class="text-center" v-model="dataUsuFinalizar.valorCuota" readonly>
                                    </div>
                                </div>

                                <div class="form-control">
                                    <label class="form-label m-0" for="">Cuotas facturadas:</label>
                                    <input type="text" class="text-center" v-model="dataUsuFinalizar.cuotasFacturadas" readonly>
                                </div>

                                <div class="form-control">
                                    <label class="form-label m-0" for="">Cuotas pendientes:</label>
                                    <input type="text" class="text-center" v-model="dataUsuFinalizar.cuotasPendientes" readonly>
                                </div>

                                <div class="form-control">
                                    <label class="form-label m-0" for="">Valor pendiente:</label>
                                    <div class="form-number number-primary">
                                        <span></span>
                                        <input type="text" class="text-center" v-model="dataUsuFinalizar.valorPendiente" readonly>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </article>
        </section>

		<script src="node_modules/read-excel-file/bundle/read-excel-file.min.js"></script>
		<script src="node_modules/xlsx/dist/xlsx.full.min.js"></script>
        <script src="Librerias/vue/vue.min.js"></script>
		<script src="Librerias/vue/axios.min.js"></script>
		<script type="module" src="servicios_publicos/acuerdoPago/crear/serv-crearAcuerdoPago.js"></script>

	</body>
</html>
