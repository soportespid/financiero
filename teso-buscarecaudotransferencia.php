<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require "comun.inc";
	require "funciones.inc";
	session_start();
	$linkbd=conectar_v7();	
	$linkbd -> set_charset("utf8");		
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es">
	<head>
	 	<meta http-equiv="Content-Type" content="text/html" charset="iso-8859-1"/>
        <meta http-equiv="X-UA-Compatible" content="IE=9"/>
		<title>:: IDEAL 10 - Tesorer&iacute;a</title>
        <link href="css/css2.css" rel="stylesheet" type="text/css" />
        <link href="css/css3.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="css/programas.js"></script>
        <script type="text/javascript" src="css/calendario.js"></script>
		<script>
			//************* ver reporte ************
			//***************************************
			function verep(idfac)
			{
				document.form1.oculto.value=idfac;
				document.form1.submit();
			}
			//************* genera reporte ************
			//***************************************
			function genrep(idfac)
			{
				document.form2.oculto.value=idfac;
				document.form2.submit();
			}
			function buscacta(e)
			{
				if (document.form2.cuenta.value!="")
				{
					document.form2.bc.value='1';
					document.form2.submit();
				}
			}
			function validar()
			{
				document.form2.submit();
			}
			function buscater(e)
			{
				if (document.form2.tercero.value!="")
				{
					document.form2.bt.value='1';
					document.form2.submit();
				}
			}
			function agregardetalle()
			{
				if(document.form2.numero.value!="" &&  document.form2.valor.value>0 &&  document.form2.banco.value!=""  )
				{ 
					document.form2.agregadet.value=1;
		//			document.form2.chacuerdo.value=2;
					document.form2.submit();
				}
				else 
				{
					alert("Falta informacion para poder Agregar");
				}
			}
			//************* genera reporte ************
			//***************************************
			function eliminar(idr)
			{
				if (confirm("Esta Seguro de Eliminar el Recaudo Transferencia "+idr))
				{
					document.form2.oculto.value=2;
					document.form2.var1.value=idr;
					document.form2.submit();
				}
			}
			//************* genera reporte ************
			//***************************************
			function guardar()
			{
				if (document.form2.fecha.value!='')
				{
					if (confirm("Esta Seguro de Guardar"))
					{
						document.form2.oculto.value=2;
						document.form2.submit();
					}
				}
				else
				{
					alert('Faltan datos para completar el registro');
					document.form2.fecha.focus();
					document.form2.fecha.select();
				}
			}
			function pdf()
			{
				document.form2.action="teso-pdfconsignaciones.php";
				document.form2.target="_BLANK";
				document.form2.submit(); 
				document.form2.action="";
				document.form2.target="";
			}

			function verUltimaPos(idcta){
				/*var scrtop=$('#divdet').scrollTop();
				var altura=$('#divdet').height();
				var numpag=$('#nummul').val();
				var limreg=$('#numres').val();
				if((numpag<=0)||(numpag==""))
					numpag=0;
				if((limreg==0)||(limreg==""))
					limreg=10;
				numpag++;*/
				location.href="teso-editarecaudotransferencia.php?idrecaudo="+idcta;
			}

			function crearexcel(){
				document.form2.action="teso-recaudotransferenciaexcel.php";
				document.form2.target="_BLANK";
				document.form2.submit();
				document.form2.action="";
				document.form2.target="";
			}

		</script>
		<?php titlepag();?>
	</head>
	<body>
		<IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
		<span id="todastablas2"></span>

		<table>
			<tr><script>barra_imagenes("teso");</script><?php cuadro_titulos();?></tr>	 
			<tr><?php menu_desplegable("teso");?></tr>
			<tr>
				<td colspan="3" class="cinta">
					<a href="teso-recaudotransferencia.php" class="mgbt"><img src="imagenes/add.png" title="Nuevo"/></a>
					<a class="mgbt"><img src="imagenes/guardad.png"/></a>
					<a class="mgbt"><img src="imagenes/buscad.png"/></a>
					<a onClick="mypop=window.open('plan-agenda.php','','');mypop.focus()" class="mgbt"><img src="imagenes/agenda1.png" title="Agenda"/></a>
					<a onClick="mypop=window.open('teso-principal.php','','');mypop.focus();" class="mgbt"><img src="imagenes/nv.png" title="Nueva ventana"></a>
					<a onClick="mypop=window.open('<?php echo $url2; ?>','','');mypop.focus();" class="tooltip bottom mgbt"><img src="imagenes/duplicar_pantalla.png" title="Duplicar pesta&ntilde;a"></a> 
					<a onclick="crearexcel()" class="mgbt"><img src="imagenes/excel.png" title="Excel"></a> 
				</td>
			</tr>	
		</table>

		<?php 
			if($_POST['nummul']=="")
			{
				$_POST['numres']=10;
				$_POST['numpos']=0;
				$_POST['nummul']=0;
			}
		?>

		<form name="form2" method="post" action="teso-buscarecaudotransferencia.php">
			<table  class="inicio" align="center" >
				<tr>
					<td class="titulos" colspan="16">:. Buscar Recaudos Transferencia</td>
					<td width="70" class="cerrar" ><a href="teso-principal.php"> Cerrar</a></td>
				</tr>
				<tr>
					<td class="tamano01" style="width:3.6cm;">Numero recaudo:</td>
					<td colspan="4"><input type="search" name="numero" id="numero" value="<?php echo $_POST['numero'];?>" style="width:100%;"/></td>
					<td class="tamano01" style="width:3.6cm;">Concepto recaudo:</td>
					<td colspan="4"><input type="search" name="nombre" id="nombre" value="<?php echo $_POST['nombre'];?>" style="width:100%;"/></td>
					<td class="tamano01">Fecha inicial: </td>
					<td style="width:10%;"><input type="search" name="fecha" id="fc_1198971545" title="DD/MM/YYYY" placeholder="DD/MM/YYYY" value="<?php echo $_POST['fecha'];?>" onKeyUp="return tabular(event,this)" onchange="" onKeyDown="mascara(this,'/',patron,true)" maxlength="10" style="width:75%">&nbsp;<img src="imagenes/calendario04.png" onClick="displayCalendarFor('fc_1198971545');" class="icobut" title="Calendario"></td>
					<td class="tamano01" >Fecha final: </td>
					<td style="width:10%;"><input type="search" name="fecha2"  id="fc_1198971546" title="DD/MM/YYYY" placeholder="DD/MM/YYYY" value="<?php echo $_POST['fecha2'];?>" onKeyUp="return tabular(event,this) " onchange="" onKeyDown="mascara(this,'/',patron,true)" maxlength="10" style="width:75%"/>&nbsp;<img src="imagenes/calendario04.png" onClick="displayCalendarFor('fc_1198971546');"  class="icobut" title="Calendario"></td>  
					<td style="padding-bottom:1px"><em class="botonflecha" onClick="limbusquedas();">Buscar</em></td>
				</tr>                     
			</table>    

			<input type="hidden" name="oculto" id="oculto" value="1">
			<input type="hidden" name="var1" value=<?php echo $_POST['var1'];?>>  
			<input type="hidden" name="numres" id="numres" value="<?php echo $_POST['numres'];?>"/>
			<input type="hidden" name="numpos" id="numpos" value="<?php echo $_POST['numpos'];?>"/>
			<input type="hidden" name="nummul" id="nummul" value="<?php echo $_POST['nummul'];?>"/>

    		<div class="subpantallap" style="height:66%; width:99.6%; overflow-x:hidden;">

				<?php
					$oculto=$_POST['oculto'];

					if($_POST['oculto']==2)
					{

						$sqlr="select * from tesorecaudotransferencia where id_recaudo=$_POST[var1]  ";
						$resp = mysqli_query($linkbd,$sqlr);
						$row=mysqli_fetch_row($resp);
						//********Comprobante contable en 000000000000
						$sqlr="update comprobante_cab set total_debito=0,total_credito=0,estado='0' where numerotipo=$row[0] AND tipo_comp=14";
						// echo $sqlr;
						mysqli_query($linkbd,$sqlr);
						$sqlr="update comprobante_det set valdebito=0,valcredito=0 where id_comp='14 $row[0]'";
						mysqli_query($linkbd,$sqlr);
						
						$sqlr="update pptocomprobante_cab set estado='0' where numerotipo=$row[0] AND tipo_comp=19";
						mysqli_query($linkbd,$sqlr);
						
						//******** RECIBO DE CAJA ANULAR 'N'	 
						$sqlr="update tesorecaudotransferencia set estado='N' where id_recaudo=$row[0]";
						mysqli_query($linkbd,$sqlr);
						$sqlr="select * from pptoingtranppto where idrecibo=$row[0]";
						$resp=mysqli_query($linkbd,$sqlr);
						while($r=mysqli_fetch_row($resp))
						{
							$sqlr="update pptocuentaspptoinicial set ingresos=ingresos-$r[3] where cuenta='$r[1]'";
							mysqli_query($linkbd,$sqlr);
						}	
						$sqlr="delete from pptoingtranppto where idrecibo=$row[0]";
						$resp=mysqli_query($linkbd,$sqlr); 
					}
				?>
		
				<?php
					$oculto=$_POST['oculto'];
					
					preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST["fecha"], $fech1);
					preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST["fecha2"], $fech2);
					$f1=$fech1[3]."-".$fech1[2]."-".$fech1[1];
					$f2=$fech2[3]."-".$fech2[2]."-".$fech2[1];

					$crit1=" ";
					$crit2=" ";
					$crit3=" ";

					if ($_POST['numero']!="")
					{
						$crit1=" and tesorecaudotransferencia.id_recaudo  like '".$_POST['numero']."' ";
					}

					if ($_POST['nombre']!="")
					{		
						$crit2=" and tesorecaudotransferencia.concepto like '".$_POST['nombre']."'  ";
					}

					if($_POST['fecha']!="" && $_POST['fecha2']!="")
					{
						$crit3 = " AND tesorecaudotransferencia.fecha BETWEEN '$f1' AND '$f2'";
					}
					//sacar el consecutivo 
					//$sqlr="select *from pptosideforigen where".$crit1.$crit2." order by pptosideforigen.codigo";
					$sqlr="select *from tesorecaudotransferencia where tesorecaudotransferencia.id_recaudo>-1 ".$crit1.$crit2.$crit3." order by tesorecaudotransferencia.id_recaudo desc";

					// echo "<div><div>sqlr:".$sqlr."</div></div>";
					$resp = mysqli_query($linkbd,$sqlr);
					$ntr = mysqli_num_rows($resp);
					$_POST['numtop']=$ntr;
					$nuncilumnas=ceil($_POST['numtop']/$_POST['numres']);
					$cond2="";
					if ($_POST['numres']!="-1"){
						$cond2="LIMIT $_POST[numpos], $_POST[numres]";
					}
					$sqlr="select *from tesorecaudotransferencia where tesorecaudotransferencia.id_recaudo>-1 ".$crit1.$crit2.$crit3." order by tesorecaudotransferencia.id_recaudo desc $cond2";
					$resp = mysqli_query($linkbd,$sqlr);
					$con=1;
					$numcontrol=$_POST['nummul']+1;
					if(($nuncilumnas==$numcontrol)||($_POST['numres']=="-1"))
					{
						$imagenforward="<img src='imagenes/forward02.png' style='width:17px'>";
						$imagensforward="<img src='imagenes/skip_forward02.png' style='width:16px' >";
					}
					else
					{
						$imagenforward="<img src='imagenes/forward01.png' style='width:17px' title='Siguiente' onClick='numsiguiente()'>";
						$imagensforward="<img src='imagenes/skip_forward01.png' style='width:16px' title='Fin' onClick='saltocol(\"$nuncilumnas\")'>";
					}
					if(($_POST['numpos']==0)||($_POST['numres']=="-1"))
					{
						$imagenback="<img src='imagenes/back02.png' style='width:17px'>";
						$imagensback="<img src='imagenes/skip_back02.png' style='width:16px'>";
					}
					else
					{
						$imagenback="<img src='imagenes/back01.png' style='width:17px' title='Anterior' onClick='numanterior();'>";
						$imagensback="<img src='imagenes/skip_back01.png' style='width:16px' title='Inicio' onClick='saltocol(\"1\")'>";
					}
					echo "<table class='inicio' align='center' >
						<tr>
							<td colspan='8' class='titulos'>.: Resultados Busqueda:</td>
							<td class='submenu'>
								<select name='renumres' id='renumres' onChange='cambionum();' style='width:100%'>
									<option value='10'"; if ($_POST['renumres']=='10'){echo 'selected';} echo ">10</option>
									<option value='20'"; if ($_POST['renumres']=='20'){echo 'selected';} echo ">20</option>
									<option value='30'"; if ($_POST['renumres']=='30'){echo 'selected';} echo ">30</option>
									<option value='50'"; if ($_POST['renumres']=='50'){echo 'selected';} echo ">50</option>
									<option value='100'"; if ($_POST['renumres']=='100'){echo 'selected';} echo ">100</option>
									<option value='-1'"; if ($_POST['renumres']=='-1'){echo 'selected';} echo ">Todos</option>
								</select>
							</td>
						</tr>
						<tr>
							<td colspan='2' id='RecEnc'>Recaudos encontrados: $ntr2</td>
						</tr>
						<tr>
							<td class='titulos2' style='width:8%'>Codigo</td>
							<td class='titulos2' style='width:8%'>Liquidacion</td>
							<td class='titulos2'>Nombre</td>
							<td class='titulos2' style='width:8%'>Fecha</td>
							<td class='titulos2'>Contribuyente</td>
							<td class='titulos2' style='width:10%'>Valor</td>
							<td class='titulos2' style='width:8%'>Estado</td>
							<td class='titulos2' style='width:8%'><center>Anular</td>
							<td class='titulos2' ><center>Ver</td>
						</tr>";	
					//echo "nr:".$nr;
					$iter='zebra1';
					$iter2='zebra2';
					$id=$_GET['id'];
					//echo $id;
					if($_POST['fecha'] == '' && $_POST['fecha2'] == '' && $_POST['numero'] == '' && $_POST['nombre'] == '')
					{
						echo "
						<table class='inicio'>
							<tr>
								<td class='saludo1' style='text-align:center;width:100%;font-size:25px'>Utilice el filtro de busqueda</td>
							</tr>
						</table>";
						$nuncilumnas = 0;
					}
					elseif(mysqli_num_rows($resp) == 0 || mysqli_num_rows($resp) == '0')
					{
						echo "
						<table class='inicio'>
							<tr>
								<td class='saludo1' style='text-align:center;width:100%;font-size:25px'>No hay resultados de su busqueda.</td>
							</tr>
						</table>";
					}
					else
					{
						while ($row =mysqli_fetch_row($resp)) 
						{
							$ntr2 = $ntr;

							echo "<script>document.getElementById('RecEnc').innerHTML = 'Recaudos encontrados: $ntr2'</script>";

							$estilo='';
							if($id==$row[0]){
								$estilo='background-color:#FF9';
							}
							$nter=buscatercero($row[7]);
							if($row[10]=='S'){$imgsem="src='imagenes/sema_verdeON.jpg' title='Activo'"; 	}			  
							if($row[10]=='N'){$imgsem="src='imagenes/sema_rojoON.jpg' title='Anulado'";}	

							echo"

							<input type='hidden' name='codigoE[]' value='".$row[0]."'>
							<input type='hidden' name='nliquidE[]' value='".$row[1]."'>
							<input type='hidden' name='nombreE[]' value='".$row[6]."'>
							<input type='hidden' name='fechaE[]' value='".$row[2]."'>
							<input type='hidden' name='nomContribuyenteE[]' value='".$nter."'>
							<input type='hidden' name='valorE[]' value='".number_format($row[9],2)."'>
							";

							if ($row[10]=='S')
							{
								echo"
								<input type='hidden' name='estadoE[]' value='ACTIVO'>";
							}
							if ($row[10]=='N')
							{
								echo"
								<input type='hidden' name='estadoE[]' value='INACTIVO'>";
							}
							if ($row[10]=='R')
							{
								echo"
								<input type='hidden' name='estadoE[]' value='INACTIVO'>";
							}

							echo "<tr class='$iter' onDblClick=\"verUltimaPos($row[0])\" style='$estilo'>
								<td>$row[0]</td>
								<td>$row[1]</td>
								<td>$row[6]</td>
								<td>$row[2]</td>
								<td>$nter</td>
								<td style='text-align:right;'>".number_format($row[9],2)."</td>
								<td style='text-align:center;'>
									<img $imgsem style='width:18px'/>
								</td>";
								if($row[10]=='S')
								echo "<td><a href='#'  onClick=eliminar($row[0])><center><img src='imagenes/anular.png'></center></a></td>";		 
								if($row[10]=='N')
								echo "<td></td>";	 
								$sqlrMedioPago = "SELECT medio_pago FROM tesorecaudotransferencialiquidar WHERE id_recaudo=$row[1]";
								$respMedioPago = mysqli_query($linkbd, $sqlrMedioPago);
								$rowMedioPago = mysqli_fetch_row($respMedioPago);
								if($rowMedioPago[0]=='2')
									$medioPago = "SSF";
								else
									$medioPago = "CSF";
								
								echo"<td style='text-align:center;'>".$medioPago."</td>";
								echo"<input type='hidden' name='medioPagoE[]' value='".$medioPago."'>";
							//echo "<td><a href='teso-editarecaudotransferencia.php?idrecaudo=$row[0]'><center><img src='imagenes/buscarep.png'></center></a></td></tr>";
							$con+=1;
							$aux=$iter;
							$iter=$iter2;
							$iter2=$aux;
						}
					}
					
					echo"</table>
					<table class='inicio'>
						<tr>
							<td style='text-align:center;'>
								<a href='#'>$imagensback</a>&nbsp;
								<a href='#'>$imagenback</a>&nbsp;&nbsp;";
								if($nuncilumnas<=9){$numfin=$nuncilumnas;}
								else{$numfin=9;}
								for($xx = 1; $xx <= $numfin; $xx++)
								{
									if($numcontrol<=9){$numx=$xx;}
									else{$numx=$xx+($numcontrol-9);}
									if($numcontrol==$numx){echo"<a href='#' onClick='saltocol(\"$numx\")'; style='color:#24D915'> $numx </a>";}
									else {echo"<a href='#' onClick='saltocol(\"$numx\")'; style='color:#000000'> $numx </a>";}
								}
								echo"&nbsp;&nbsp;<a href='#'>$imagenforward</a>
									&nbsp;<a href='#'>$imagensforward</a>
							</td>
						</tr>
					</table>";
				?>
			</div>	
		</form> 
	</body>
</html>