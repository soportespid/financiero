<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");

	require 'comun.inc';
	require 'funciones.inc';

	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");

	session_start();
    date_default_timezone_set("America/Bogota");

	$scroll=$_GET['scrtop'];
	$totreg=$_GET['totreg'];
	$idcta=$_GET['idcta'];
	$altura=$_GET['altura'];
	$filtro="'".$_GET['filtro']."'";
?>
<!DOCTYPE >
<html lang="es">
	<head>
		<meta charset="utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1"/>
		<meta name="viewport" content="user-scalable=no">
		<title>:: IDEAL 10 - Parametrización</title>
		<link href="favicon.ico" rel="shortcut icon"/>
		<link href="css/css2.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
		<script type="text/javascript" src="css/calendario.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script type="text/javascript" src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script>
			function despliegamodalm(_valor,_tip,mensa,pregunta,variable)
			{
				document.getElementById("bgventanamodalm").style.visibility=_valor;
				if(_valor=="hidden")
				{
					document.getElementById('ventanam').src="";
					if(document.getElementById('valfocus').value=="2")
					{
						document.getElementById('valfocus').value='1';
						document.getElementById('documento').focus();
						document.getElementById('documento').select();
					}
				}
				else
				{
					switch(_tip)
					{
						case "1":
							document.getElementById('ventanam').src="ventana-mensaje1.php?titulos="+mensa;break;
						case "2":
							document.getElementById('ventanam').src="ventana-mensaje3.php?titulos="+mensa;break;
						case "3":
							document.getElementById('ventanam').src="ventana-mensaje2.php?titulos="+mensa;break;
						case "4":
							document.getElementById('ventanam').src="ventana-consulta1.php?titulos="+mensa+"&idresp="+pregunta;break;
						case "5":
							document.getElementById('ventanam').src="ventana-elimina1.php?titulos="+mensa+"&idresp="+pregunta+"&variable="+variable;break;
					}
				}
			}
			function funcionmensaje()
			{
				//document.location.href="inve-buscadestinocompra.php";
			}
			function respuestaconsulta(pregunta)
			{
				switch(pregunta)
				{
					case "1":
						document.form2.oculto.value = "2";
						document.form2.submit();
						break;
					case "2":
						document.getElementById('oculto').value="6";
						document.form2.submit();break;
					break;
				}
			}
			function guardar()
			{
				var val1 = document.getElementById('numero').value;
				var val2 = document.getElementById('nombre').value;
				var val3 = document.getElementById('cuenta').value;
				if((val1.trim()!='')&&(val2.trim()!=''))
				{
					despliegamodalm('visible','4','Esta Seguro de Guardar','1');
				}
				else
				{
					document.form2.numero.focus();
					document.form2.numero.select();
					despliegamodalm('visible','2','Faltan Datos para Completar el Registro');
				}
			}
			function agregardetalle()
			{
				var val1 = document.getElementById('cuenta').value;
				var val2 = document.getElementById('cuentaF').value;
				
				if((val1.trim() != '')&&(val2.trim() != ''))
				{
					document.form2.agregadet.value = 1;
					document.form2.submit();
				}
				else
				{
					despliegamodalm('visible','2','Faltan informacion para el detalle');
				}
			}
			function buscacta(e)
			{
				if (document.form2.cuenta.value != "")
				{
					document.form2.bcnt.value='1';
					document.form2.submit();
				}
			}
			function buscacta2(e)
			{
				if (document.form2.cuentaF.value != "")
				{
					document.form2.bcnt2.value='1';
					document.form2.submit();
				}
			}
			function despliegamodal2(_valor,_nomve)
			{
				document.getElementById("bgventanamodal2").style.visibility=_valor;
				if(_valor=="hidden")
				{
					document.getElementById('ventana2').src="";
				}
				else 
				{
					switch(_nomve)
					{
						case "1":
							document.getElementById('ventana2').src="cuentas-ventana04.php?cuenta=cuenta&ncuenta=ncuenta";
							break;
						case "2":
							if(document.getElementById('cuenta').value != '')
							{
								document.getElementById('ventana2').src="cuentas-ventana04.php?cuenta=cuentaF&ncuenta=nomCuentaF&limite="+document.getElementById('cuenta').value;
							}
							else
							{
								document.getElementById("bgventanamodal2").style.visibility='hidden';
								despliegamodalm('visible','2','Error, Se debe ingresar una cuenta inicial ');
							}
							break;
					}
				}
			}
			
			function adelante(scrtop, numpag, limreg, filtro, next)
			{
				var maximo=document.getElementById('maximo').value;
				var actual=document.getElementById('numero').value;
				actual=parseFloat(actual)+1;
				if(actual<=parseFloat(maximo))
				{
					if(actual<10){actual="0"+actual;}
					location.href="inve-editadestinocompra.php?is=" +actual+ "&scrtop=" +scrtop+ "&numpag=" +numpag+ "&limreg=" +limreg+ "&filtro=" +filtro;
				}
			}
			function atrasc(scrtop, numpag, limreg, filtro, prev)
			{
				var minimo=document.getElementById('minimo').value;
				var actual=document.getElementById('numero').value;
				if(parseFloat(minimo)<parseFloat(actual))
				{
					actual=parseFloat(actual)-1;
					if(actual>=parseFloat(minimo))
					{
						if(actual<10){actual="0"+actual;}
						location.href="inve-editadestinocompra.php?is=" +actual+ "&scrtop=" +scrtop+ "&numpag=" +numpag+ "&limreg=" +limreg+ "&filtro=" +filtro;
					}
				}
			}
			function iratras(scrtop, numpag, limreg, filtro)
			{
				var idcta=document.getElementById('numero').value;
				location.href="inve-buscadestinocompra.php?idcta="+idcta+"&scrtop="+scrtop+"&numpag="+numpag+"&limreg="+limreg+"&filtro="+filtro;
			}
			function agregarcuentas()
			{
				var val1 = document.getElementById('cuenta').value;
				var val2 = document.getElementById('cuentaF').value;
				if((val1.trim()!='')&&(val2.trim()!=''))
				{
					document.form2.agregadet.value=1;
					document.form2.submit();
				}
				else
				{
					despliegamodalm('visible','2','Error, Se debe ingresar toda la información de las cuentas para agregar ');
				}
				
			}
			function eliminar(variable)
			{
				document.form2.elimina.value=variable;
				despliegamodalm('visible','4','Esta seguro de eliminar detalle','2');
			}
		</script>
	</head>
	<body>
		<IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
		<span id="todastablas2"></span>
		<?php
			$numpag=$_GET['numpag'];
			$limreg=$_GET['limreg'];
			$scrtop=26*$totreg;
		?>
		<table>
			<tr><script>barra_imagenes("para");</script><?php cuadro_titulos();?></tr>
			<tr><?php menu_desplegable("para");?></tr>
			<tr>
				<td colspan="3" class="cinta"><img src="imagenes/add.png"  title="Nuevo" onClick="location.href='inve-destinocompra.php'" class="mgbt"/><img src="imagenes/guarda.png" title="Guardar" onClick="guardar()" class="mgbt"/><img src="imagenes/busca.png" title="Buscar" onClick="location.href='inve-buscadestinocompra.php'" class="mgbt"/><img src="imagenes/agenda1.png" title="Agenda" onClick="mypop=window.open('plan-agenda.php','','');mypop.focus()" class="mgbt"/><img src="imagenes/nv.png" title="Nueva ventana" onClick="mypop=window.open('para-principal.php','','');mypop.focus();" class="mgbt"/><img src="imagenes/duplicar_pantalla.png" title="Duplicar pesta&ntilde;a" onClick="mypop=window.open('<?php echo $url2; ?>','','');mypop.focus();" class="mgbt"/><img src="imagenes/iratras.png" title="Atr&aacute;s" onClick="iratras(<?php echo "$scrtop, $numpag, $limreg, $filtro"; ?>)" class="mgbt"/></td>
			</tr>
		</table>
		<tr>
			<td colspan="3" class="tablaprin"> 
			<?php

				$sqlr="select MIN(CONVERT(codigo, SIGNED INTEGER)), MAX(CONVERT(codigo, SIGNED INTEGER)) from almdestinocompra ORDER BY CONVERT(codigo, SIGNED INTEGER)";
				$res=mysqli_query($linkbd,$sqlr);
				$r=mysqli_fetch_row($res);
				$_POST['minimo']=$r[0];
				$_POST['maximo']=$r[1];

				if($_POST['oculto']=="")
				{
					$_POST['vareliminadas']='';
					$sqlCab = "SELECT * FROM almdestinocompra WHERE codigo = '$_GET[is]' ";
					$resCab = mysqli_query($linkbd,$sqlCab);
					$rowCab = mysqli_fetch_row($resCab);
					$_POST['numero'] = $rowCab[0];
					$_POST['nombre'] = $rowCab[1];

					$_POST['cuenta1']=array();
					$_POST['nombre1']=array();
					$_POST['cuenta2']=array();
					$_POST['nombre2']=array();
					$_POST['fechadet']=array();
					
					$sqlr="SELECT cuenta_inicial, cuenta_final, estado, fecha, id_det FROM almdestinocompra_det WHERE codigo='".$_GET['is']."' ORDER BY id_det ";
					$res=mysqli_query($linkbd,$sqlr); 
					$cont=0;
					while ($row=mysqli_fetch_row($res))
					{
						$_POST['iddetalle'][] = $row[4];
						$_POST['cuenta1'][] = $row[0];
						$sqlNom1 = "SELECT nombre FROM cuentasnicsp WHERE cuenta = '$row[0]'";
						$resNom1 = mysqli_query($linkbd,$sqlNom1);
						$rowNom1 = mysqli_fetch_row($resNom1);
						$_POST['nombre1'][] = $rowNom1[0];
						$_POST['cuenta2'][] = $row[1];
						$sqlNom1 = "SELECT nombre FROM cuentasnicsp WHERE cuenta = '$row[1]'";
						$resNom1 = mysqli_query($linkbd,$sqlNom1);
						$rowNom1 = mysqli_fetch_row($resNom1);
						$_POST['nombre2'][] = $rowNom1[0];
						$cont=$cont+1;
					}
				}
			?>
			<div id="bgventanamodalm" class="bgventanamodalm">
				<div id="ventanamodalm" class="ventanamodalm">
					<IFRAME src="" name="ventanam" marginWidth=0 marginHeight=0 frameBorder=0 id="ventanam" frameSpacing=0 style=" width:700px; height:130px; top:200; overflow:hidden;"></IFRAME>
				</div>
			</div>

			<div id="bgventanamodal2" class="bgventanamodalm2">
				<div id="ventanamodal2" class="bgventanamodalm2">
					<IFRAME  src="" name="buster" marginWidth=0 marginHeight=0 frameBorder=0 id="ventana2" frameSpacing=0 style="left:500px; width:900px; height:500px; top:200;"></IFRAME>
				</div>
			</div> 
			<form name="form2" method="post" action=""> 
				<?php 
					if($_POST['bcnt'] == '1')
					{
						$sqlNCA = "SELECT nombre, tipo, estado FROM cuentasnicsp WHERE cuenta = '".$_POST['cuenta']."'";
						$resNCA = mysqli_query($linkbd,$sqlNCA);
						$nca = mysqli_fetch_row($resNCA);
						if($nca[0]!=''){$_POST['ncuenta'] = $nca[0];}
						else 
						{
							echo "<script>despliegamodalm('visible','2','Error, cuenta No ".$_POST['cuenta']." no existe ');</script>";
							$_POST['cuenta']='';
						}
					}
					if($_POST['bcnt2'] == '1')
					{
						if($_POST['cuenta']!='')
						{
							$sqlNCA = "SELECT nombre, tipo, estado FROM cuentasnicsp WHERE cuenta = '".$_POST['cuentaF']."'";
							$resNCA = mysqli_query($linkbd,$sqlNCA);
							$nca = mysqli_fetch_row($resNCA);
							if($nca[0]!=''){$_POST['nomCuentaF'] = $nca[0];}
							else 
							{
								echo "<script>despliegamodalm('visible','2','Error, cuenta No ".$_POST['cuentaF']." no existe ');</script>";
								$_POST['cuentaF']='';
							}
						}
						else
						{
							echo "<script>despliegamodalm('visible','2','Error, debe ingresar primero la cuenta inicial ');</script>";
							$_POST['cuentaF']='';
						}
					}
				?>

				<table class="inicio" align="center"  >
					<tr >
						<td class="titulos" colspan="8">.: Editar Destino Compra</td>
						<td class="cerrar" style="width:7%" onClick="location.href='para-principal.php'">Cerrar</td>
					</tr>
					<tr>
						<td style="width:2.5cm" class="tamano01">C&oacute;digo:</td>
						<td style="width:15%" valign="middle" >
							<img src="imagenes/back.png" onClick="atrasc(<?php echo "$scrtop, $numpag, $limreg, $filtro, $prev";?>)" class="icobut" title="Anterior"/>&nbsp;<input type="text" id="numero" name="numero" style="width:50%;height:30px;" onKeyPress="javascript:return solonumeros(event)" onKeyUp="return tabular(event,this)" value="<?php echo $_POST['numero']?>" onClick="document.getElementById('numero').focus();document.getElementById('numero').select();"readonly>&nbsp;<img src="imagenes/next.png" onClick="adelante(<?php echo "$scrtop, $numpag, $limreg, $filtro, $next";?>)" class="icobut" title="Sigiente"/>
							<input type="hidden" name="maximo" id="maximo" value="<?php echo $_POST['maximo']?>"/>
							<input type="hidden" name="minimo" id="minimo" value="<?php echo $_POST['minimo']?>"/>
							<input type="hidden" name="codrec" id="codrec" value="<?php echo $_POST['codrec']?>"/>
						</td>
						<td style="width:2.5cm" class="tamano01">Nombre:</td>
						<td ><input type="text" id="nombre" name="nombre" onKeyUp="return tabular(event,this)" value="<?php echo $_POST['nombre']?>" style="width:100%;height:30px;" onClick="document.getElementById('acuerdo').focus();document.getElementById('acuerdo').select();"></td>
					</tr>
				</table>
				<input type="hidden" id="oculto" value="0" name="oculto">
				<table class="inicio">
					<tr>
						<td colspan="8" class="titulos2">Crear Detalle Destino Compra</td>
					</tr>
					<tr>
						<td class="tamano01" style="width:2.5cm;">Cuenta de:</td>
						<td style="width:10%;"><input class="colordobleclik" type="text" id="cuenta" name="cuenta" style="width:100%; height: 30px;" onKeyPress="javascript:return solonumeros(event)" onKeyUp="return tabular(event,this)" onChange="buscacta(event)" onDblClick="despliegamodal2('visible','1');" value="<?php echo $_POST['cuenta']?>" autocomplete="off"/></td>
						<input type="hidden" name="bcnt" id="bcnt" value="0">
						<td ><input type="text" name="ncuenta" id="ncuenta" value="<?php echo $_POST['ncuenta']?>" style="width:98%; height:30px;" readonly></td>
						<td class="tamano01" style="width:2.5cm;">Cuenta hasta:</td>
						<td style="width:10%;"><input class="colordobleclik" type="text" name="cuentaF" id="cuentaF" value="<?php echo $_POST['cuentaF']?>" onKeyPress="javascript:return solonumeros(event)" onKeyUp="return tabular(event,this)" onChange="buscacta2(event)" onDblClick="despliegamodal2('visible','2');" style="width:100%; height:30px;" autocomplete="off"/></td>
						<input type="hidden" name="bcnt2" id="bcnt2" value="0">
						<td><input type="text" name="nomCuentaF" id="nomCuentaF" value="<?php echo $_POST['nomCuentaF']?>" style="width:98%; height:30px;" readonly/></td>

						<td style="padding-bottom:0px"><em class="botonflecha" onClick="agregarcuentas();">Agregar</em></td>
					</tr>
				</table>
				<div class="subpantalla" style="height:48.5%; width:99.6%; overflow-x:hidden;">
					<table class="inicio ancho">
						<tr><td class="titulos" colspan="10">Destinos de compra creados</td></tr>
						<tr class="titulos2">
							<td style='width:20%;'>Cuenta de</td>
							<td style='width:20%;'>Nombre Cuenta</td>
							<td style='width:20%;'>Cuenta hasta</td>
							<td style='width:20%;'>Nombre Cuental</td>
							<td style='width:5%;'>Eliminar</td>
						</tr>
						<input type='hidden' name='elimina' id='elimina'/>
						<input type='hidden' name='vareliminadas' id='vareliminadas' value="<?php echo $_POST['vareliminadas'];?>"/>
						<?php
							if ($_POST['oculto']=='6')
							{
								$posi=$_POST['elimina'];
								if($_POST['vareliminadas']!='')
								{
									$_POST['vareliminadas'] = $_POST['vareliminadas'] + "<->" + $_POST['iddetalle'][$posi];
									echo "<script>document.getElementById('vareliminadas').value=document.getElementById('vareliminadas').value + '<->' + ".$_POST['iddetalle'][$posi].";</script>";
								}
								else 
								{
									echo "<script>document.getElementById('vareliminadas').value=".$_POST['iddetalle'][$posi].";</script>";
								}
								
								unset($_POST['iddetalle'][$posi]);
								unset($_POST['cuenta1'][$posi]);
								unset($_POST['nombre1'][$posi]);
								unset($_POST['cuenta2'][$posi]);
								unset($_POST['nombre2'][$posi]);
								$_POST['iddetalle'] = array_values($_POST['iddetalle']);
								$_POST['cuenta1'] = array_values($_POST['cuenta1']);
								$_POST['nombre1'] = array_values($_POST['nombre1']);
								$_POST['cuenta2'] = array_values($_POST['cuenta2']);
								$_POST['nombre2'] = array_values($_POST['nombre2']);
							}
							if ($_POST['agregadet'] == '1')
							{
								$_POST['iddetalle'][] = '';
								$_POST['cuenta1'][] = $_POST['cuenta'];
								$_POST['nombre1'][] = $_POST['ncuenta'];
								$_POST['cuenta2'][] = $_POST['cuentaF'];
								$_POST['nombre2'][] = $_POST['nomCuentaF'];
								echo"
								<script>
									document.getElementById('cuenta').value = '';
									document.getElementById('cuentaF').value  = '';
									document.getElementById('ncuenta').value = '';
									document.getElementById('nomCuentaF').value  = '';
								</script>";
							}
							$iter='saludo1a';
							$iter2='saludo2';
							$cdtll = count($_POST['cuenta1']);
							for ($x=0;$x< $cdtll;$x++)
							{
								echo"
								<input type='hidden' name='iddetalle[]' value='".$_POST['iddetalle'][$x]."'/>
								<input type='hidden' name='cuenta1[]' value='".$_POST['cuenta1'][$x]."'/>
								<input type='hidden' name='nombre1[]' value='".$_POST['nombre1'][$x]."'/>
								<input type='hidden' name='cuenta2[]' value='".$_POST['cuenta2'][$x]."'/>
								<input type='hidden' name='nombre2[]' value='".$_POST['nombre2'][$x]."'/>
								<tr class='$iter'>
									<td>".$_POST['cuenta1'][$x]."</td>
									<td>".$_POST['nombre1'][$x]."</td>
									<td>".$_POST['cuenta2'][$x]."</td>
									<td>".$_POST['nombre2'][$x]."</td>
									<td><a href='#' onclick='eliminar($x)'><img src='imagenes/del.png'></a></td>
								</tr>";
								$aux=$iter;
								$iter=$iter2;
								$iter2=$aux;
							}
						?>
					</table>
				</div>
				<input type="hidden" name="agregadet" id="agregadet" value="0">
			</form>
			<?php
				if($_POST['oculto']=='2')
				{
					$sql = "UPDATE almdestinocompra SET nombre  = '".$_POST['nombre']."' WHERE codigo = '".$_POST['numero']."' ";
					mysqli_query($linkbd,$sql);
					$cdtll = count($_POST['cuenta1']);
					for ($x=0;$x< $cdtll;$x++)
					{
						if($_POST['iddetalle'][$x] == '')
						{
							$numid_det=selconsecutivo('almdestinocompra_det','id_det');
							$sqlr="INSERT INTO almdestinocompra_det (id_det,codigo,cuenta_inicial,cuenta_final,estado) VALUES ('$numid_det','".$_POST['numero']."','".$_POST['cuenta1'][$x]."', '".$_POST['cuenta2'][$x]."', 'S')";
							mysqli_query($linkbd,$sqlr);
						}
					}
					$eliminadas = explode('<->', $_POST['vareliminadas']);
					$cdtll = count($eliminadas);
					for ($x=0;$x< $cdtll;$x++)
					{
						$sqlr="DELETE FROM almdestinocompra_det WHERE id_det='".$eliminadas[$x]."'";
						mysqli_query($linkbd,$sqlr);
					}
					echo "<script>despliegamodalm('visible','3','Se ha almacenado con exito');</script>";
				}
			?>	
			</td>
		</tr>   
		 
	</body>
</html>