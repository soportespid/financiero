<?php
	header("Cache-control: no-cache, no-store, must-revalidate"); 
	header("Content-Type: text/html;charset=iso-8859-1");
	require "comun.inc";
	require "funciones.inc";
	require "conversor.php";
	require "validaciones.inc";
	session_start();
	$linkbd=conectar_v7();
	cargarcodigopag($_GET[codpag],$_SESSION["nivel"]);
	error_reporting(E_ALL);
	ini_set('display_errors', '1');
?>
<!DOCTYPE >
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es">
	<head>
		<meta http-equiv="Content-type" content="text/html; charset=iso-8859-1"/>
		<meta http-equiv="X-UA-Compatible" content="IE=9"/>
		<title>:: SPID - Servicios P&uacute;blicos</title>
		<link href="css/css2.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<script type="text/javascript" src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script>
			function fbuscar()
			{
				document.getElementById('oculto').value='3';
				document.form2.submit();
			}
			function fimprimir()
			{
				document.form2.action="serv-reportegeneralrecciclopdf.php";
				document.form2.target="_BLANK";
				document.form2.submit(); 
				document.form2.action="";
				document.form2.target="";
			}
			function retornomenu(){location.href="serv-menureportes.php";}
			function validar(){document.form2.submit();}
		</script>
		<?php titlepag(); ?>
	</head>
	<body>
		<IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
		<span id="todastablas2"></span>
		<table>
			<tr><script>barra_imagenes("serv");</script><?php cuadro_titulos();?></tr>	 
			<tr><?php menu_desplegable("serv");?></tr>
			<tr>
				<td colspan="3" class="cinta"><img src="imagenes/add2.png" title="Nuevo" onClick="location.href='serv-reportegeneralrecaudociclo.php'" class="mgbt"/><img src="imagenes/guardad.png" class="mgbt1"/><img src="imagenes/buscad.png" class="mgbt1"/><img src="imagenes/nv.png" title="Nueva Ventana" onClick="mypop=window.open('serv-principal.php','','');mypop.focus();" class="mgbt"/><img src="imagenes/print.png" title="Imprimir" onClick="fimprimir()" class="mgbt"/><img src='imagenes/iratras.png' title="Men&uacute; Nomina" onClick="retornomenu()" class='mgbt'></td>
			</tr>
		</table>
		<div id="bgventanamodalm" class="bgventanamodalm">
			<div id="ventanamodalm" class="ventanamodalm">
				<IFRAME src="" name="ventanam" marginWidth=0 marginHeight=0 frameBorder=0 id="ventanam" frameSpacing=0 style=" width:700px; height:130px; top:200; overflow:hidden;"> 
				</IFRAME>
			</div>
		</div>
		<form name="form2" method="post" action="">
			<table class="inicio">
				<tr>
					<td class="titulos" colspan="11">Reporte General Recaudados</td>
					<td class="cerrar" style='width:7%'><a onClick="location.href='serv-principal.php'">&nbsp;Cerrar</a></td>
				</tr>
				<tr>
					<td class="saludo1" style="width:3.5cm;">Ciclo:</td>
					<td style="width:7%">
						<select name="numciclo" id="numciclo" onChange="validar()" >
							<option value="">Sel..</option>
							<?php
								$sqlr="SELECT * FROM servliquidaciones_gen ORDER BY id_cab DESC";
								$resp = mysqli_query($linkbd,$sqlr);
								while ($row =mysqli_fetch_row($resp)) 
								{
									if($row[0]==$_POST['numciclo'])
									{
										echo "<option value='$row[0]' SELECTED>$row[0]</option>";
										$_POST['vigencia']=$row[1];
										$_POST['mesini']=mesletras($row[2]);
										$_POST['mesfin']=mesletras($row[3]);
										$_POST['fecha']=date('d-m-Y',strtotime($row[4]));
										$_POST['numeini']=$row[8];
										$_POST['numefin']=$row[9];
									}
									else {echo "<option value='$row[0]'>$row[0]</option>";}
								}
							?>
						</select>
					</td>
					<td class="saludo1" style="width:2cm;">fecha:</td>
					<td style="width:8%"><input type="text" name="fecha" id="fecha" value="<?php echo @ $_POST['fecha'];?>" style="width:100%" readonly/></td>
					<td class="saludo1" style="width:2cm;">vigencia:</td>
					<td style="width:8%"><input type="text" name="vigencia" id="vigencia" value="<?php echo @ $_POST['vigencia'];?>" style="width:100%" readonly/></td>
					<td class="saludo1" style="width:2.5cm;">Mes Inicial:</td> 
					<td style="width:12%"><input type="text" name="mesini" id="mesini" value="<?php echo @ $_POST['mesini'];?>" style="width:100%" readonly/></td>
					<td class="saludo1" style="width:2.5cm;" >Mes final:</td> 
					<td style="width:12%"><input type="text" name="mesfin" id="mesfin" value="<?php echo @ $_POST['mesfin'];?>" style="width:100%" readonly/></td>
					<td></td>
				</tr>
				<tr>
					<td class="saludo1">Centro de Costos:</td> 
					 <td colspan="3">
						<select name="cdc" id="cdc" style="width:100%">
							<option value="" >Todos</option>
							<?php
								$sqlr="SELECT * FROM servservicios";
								$resp = mysqli_query($linkbd,$sqlr);
								while ($row =mysqli_fetch_row($resp)) 
								{
									if($row[0]==$_POST['cdc']){echo "<option value='$row[0]' SELECTED>$row[1]</option>";}
									else {echo "<option value='$row[0]'>$row[1]</option>";}	  
								}
							?>
						</select>
					</td>
					<td>&nbsp;<input type="button" name="buscar" id="bbuscar" value="  BUSCAR  " onClick="fbuscar();"/> </td>
				</tr>
			</table>
			<input type="hidden" name="oculto" id="oculto" value="1"/>
			<input type="hidden" name="tirespuesta" id="tirespuesta" value="<?php echo @ $_POST['tirespuesta']?>"/>
			<input type="hidden" name="numeini" id="numeini" value="<?php echo @ $_POST['numeini']?>"/>
			<input type="hidden" name="numefin" id="numefin" value="<?php echo @ $_POST['numefin']?>"/>
			<div class="subpantalla" style="height:68.5%; width:99.6%; overflow-x:hidden;">
				<?php
					if (@ $_POST['oculto']=="3")
					{
						$crips01="";
						if(@ $_POST['cdc']!=""){$crips01="AND T2.ingreso='".$_POST['cdc']."'";}
						$sqlr="SELECT MONTH(T1.fecha),T2.ingreso, SUM(T2.valor),YEAR(T1.fecha) FROM servreciboscaja T1, servreciboscaja_det T2 WHERE T1.id_recibos=T2.id_recibos AND T1.estado='S' AND (T1.tipo='4' OR T1.tipo='5') AND T1.id_recaudo BETWEEN $_POST[numeini] AND $_POST[numefin] $crips01 GROUP BY MONTH(T1.fecha),T2.ingreso ORDER BY CAST(MONTH(T1.fecha) as UNSIGNED)";
						$resp = mysqli_query($linkbd,$sqlr);
						$row=mysqli_num_rows($resp);
						echo"
						<table class='inicio' align='center' width='99%'>
							<tr><td colspan='6' class='titulos'>.: Resultados Busqueda:</td></tr>
							<tr><td colspan='6'>Facturaci&oacute;n Encontrados: </td></tr>
							<tr>
								<td class='titulos2' width='8%'>VIGENCIA</td>
								<td class='titulos2' width='10%'>MES</td>
								<td class='titulos2' width='25%'>SERVICIO</td>
								<td class='titulos2' width='14%'>VALOR RECAUDADO</td>
								<td class='titulos2'></td>
							</tr>";	
						$iter='saludo1a';
						$iter2='saludo2';
						$totalrec=0;
						$ti01=0;
						$ti02="";
						while ($row =mysqli_fetch_row($resp)) 
						{
							$totalrec+=$row[2];
							$sqlrsv="SELECT nombre FROM servservicios WHERE codigo='$row[1]'";
							$rowsv =mysqli_fetch_row(mysqli_query($linkbd,$sqlrsv));
							echo"
							<input type='hidden' name='nmvigencia[]' value='$row[3]'/>
							<input type='hidden' name='nmes[]' value='".mesletras($row[0])."'/>
							<input type='hidden' name='nservicio[]' value='".$rowsv[0]."'/>
							<input type='hidden' name='nvalorre[]' value='".number_format($row[2],0,',','.')."'/>
							<tr class='$iter' style='text-transform:uppercase;'>
								<td>".$row[3]."</td>
								<td>".mesletras($row[0])."</td>
								<td>$rowsv[0]</td>
								<td style='text-align:right;'>$ ".number_format($row[2],0,',','.')."</td>
								<td></td>
							</tr>";
							$aux=$iter;
							$iter=$iter2;
							$iter2=$aux;
						}
							echo"
							<tr class='titulos2'>
								<td style='text-align:right;' colspan='3'>Total:</td>
								<td style='text-align:right;'>$ ".number_format($totalrec,0,',','.')."</td>
								<td></td>
							</tr>
							</table>";
							$_POST['valtotal']=$totalrec;
					}	
				?>
				<input type="hidden" name="valtotal" id="valtotal" value="<?php echo @ $_POST['valtotal']?>"/>
			</div>
		</form>
	</body>
</html>