<?php
require_once ("tcpdf/tcpdf_include.php");
require ('comun.inc');
session_start();
date_default_timezone_set("America/Bogota");
class MYPDF extends TCPDF
{
    public function Header()
    {
        $linkbd = conectar_v7();
        $linkbd->set_charset("utf8");
        $sqlr = "select *from configbasica where estado='S'";
        $res = mysqli_query($linkbd, $sqlr);
        while ($row = mysqli_fetch_row($res)) {
            $nit = $row[0];
            $rs = $row[1];
            $nalca = $row[6];
        }
        //Parte Izquierda
        $this->Image('imagenes/escudo.jpg', 12, 12, 25, 25, 'JPG', '', 'T', true, 300, '', false, false, 0, false, false, false);// Logo
        $this->SetFont('helvetica', 'B', 8);
        $this->SetY(10);
        $this->RoundedRect(10, 10, 190, 29, 1, '1111');
        $this->Cell(30, 29, '', 'R', 0, 'L');
        $this->SetY(10);
        $this->SetFont('helvetica', 'B', 12);

        $this->SetX(35);
        $this->Cell(149, 15, "$rs", 0, 0, 'C');
        $this->SetY(17);

        $this->SetX(35);
        $this->SetFont('helvetica', 'B', 11);
        $this->Cell(149, 10, "$nit", 0, 0, 'C');
        $this->SetY(21);
        $this->SetX(35);
        $this->Cell(149, 14, "VALIDAR SALDOS CHIP", 0, 0, 'C');

        $arrper = array('', 'Ene-Mar', 'Abr-Jun', 'Jul-Sep', 'Oct-Dic');
        $this->SetY(27);
        $this->SetX(35);
        $this->Cell(149, 14, 'Trimestre: ' . $arrper[$_POST['nperiodo']] . ' - Vigencia: ' . $_POST['vigencias'], 0, 0, 'C');

    }
    public function Footer()
    {
        $linkbd = conectar_v7();
        $linkbd->set_charset("utf8");
        $sqlr = "SELECT direccion,telefono,web,email FROM configbasica WHERE estado='S'";
        $resp = mysqli_query($linkbd, $sqlr);
        $user = $_SESSION['nickusu'];
        $fecha = date("Y-m-d H:i:s");
        $ip = $_SERVER['REMOTE_ADDR'];
        $useri = $_POST['user'];
        while ($row = mysqli_fetch_row($resp)) {
            $direcc = strtoupper($row[0]);
            $telefonos = $row[1];
            $dirweb = strtoupper($row[3]);
            $coemail = strtoupper($row[2]);
        }
        if ($direcc != '') {
            $vardirec = "Dirección: $direcc, ";
        } else {
            $vardirec = "";
        }
        if ($telefonos != '') {
            $vartelef = "Telefonos: $telefonos";
        } else {
            $vartelef = "";
        }
        if ($dirweb != '') {
            $varemail = "Email: $dirweb, ";
        } else {
            $varemail = "";
        }
        if ($coemail != '') {
            $varpagiw = "Pagina Web: $coemail";
        } else {
            $varpagiw = "";
        }

        $txt = <<<EOD
        $vardirec $vartelef
        $varemail $varpagiw
        EOD;
        $this->SetFont('helvetica', 'I', 5);
        $this->Cell(190, 7, '', 'T', 0, 'T');
        $this->ln(1);
        $this->Write(0, $txt, '', 0, 'C', true, 0, false, false, 0);
        //$this->Cell(12, 7, 'Hecho por: '.$useri, 0, false, 'C', 0, '', 0, false, 'T', 'M');
        $this->Cell(15, 7, 'Impreso por: ' . $user, 0, false, 'C', 0, '', 0, false, 'T', 'M');
        $this->Cell(57, 7, 'IP: ' . $ip, 0, false, 'C', 0, '', 0, false, 'T', 'M');
        $this->Cell(50, 7, 'Fecha: ' . $fecha, 0, false, 'C', 0, '', 0, false, 'T', 'M');
        $this->Cell(54, 7, 'IDEAL.10 S.A.S', 0, false, 'C', 0, '', 0, false, 'T', 'M');
        $this->Cell(25, 7, 'Pagina ' . $this->getAliasNumPage() . '/' . $this->getAliasNbPages(), 0, false, 'C', 0, '', 0, false, 'T', 'M');

    }
}
$arrper = array('', 'Ene-Mar', 'Abr-Jun', 'Jul-Sep', 'Oct-Dic');
if ($_POST['nperiodo'] < 4) {
    $trimchip = $_POST['nperiodo'] - 1;
    $vigchip = $_POST['vigencias'];
} else {
    $trimchip = 4;
    $vigchip = $_POST['vigencias'] - 1;
}
$pdf = new MYPDF('P', 'mm', 'Letter', true, 'utf8', false);
$pdf->SetDocInfoUnicode(true);
// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('IDEAL10');
$pdf->SetTitle('VALIDAR SALDOS CHIP');
$pdf->SetSubject('VALIDAR SALDOS CHIP');
$pdf->SetKeywords('VALIDAR SALDOS CHIP');
$pdf->SetMargins(10, 40, 10);// set margins
$pdf->SetHeaderMargin(40);// set margins
$pdf->SetFooterMargin(19);// set margins
$pdf->SetAutoPageBreak(TRUE, 19);// set auto page breaks
$pdf->AddPage();

$pdf->ln(7);
$yy = $pdf->GetY();
$pdf->SetY($yy);
$yy = $pdf->GetY();
$pdf->SetFillColor(222, 222, 222);
$pdf->SetFont('helvetica', 'B', 7);
$pdf->Cell(0.1);
$pdf->Cell(25, 5, 'Cuenta', 0, 0, 'C', 1);
$pdf->SetY($yy);
$pdf->Cell(26);
$pdf->Cell(45, 5, 'Nombre de la Cuenta', 0, 0, 'C', 1);
$pdf->SetY($yy);
$pdf->Cell(72);
$pdf->SetFont('helvetica', 'B', 6);
$pdf->Cell(40, 5, 'Saldo Inicial Balance ' . $arrper[$_POST['nperiodo']] . ' ' . $_POST['vigencias'], 0, 0, 'C', 1);
$pdf->SetY($yy);
$pdf->Cell(113);
$pdf->Cell(40, 5, 'Saldo Final CHIP ' . $_POST['nomperi'] . ' ' . $_POST['vigeperi'], 0, 0, 'C', 1);
$pdf->SetY($yy);
$pdf->SetFont('helvetica', 'B', 7);
$pdf->Cell(154);
$pdf->MultiCell(36, 5, 'Diferencia', 0, 'C', 1, 1, '', '', true, '', false, true, 0, 'M', true);
$pdf->SetFont('helvetica', '', 6);
$pdf->ln(1);

$con = 0;
while ($con < count($_POST['dcuentas'])) {
    if ($con % 2 == 0) {
        $pdf->SetFillColor(255, 255, 255);
    } else {
        $pdf->SetFillColor(245, 245, 245);
    }
    $val = $_POST['dncuentas'][$con];
    $lineasnombres = $pdf->getNumLines($val, 46);
    $alturas = $lineasnombres * 3;
    $vv = $pdf->GetY();
    if ($vv >= 274) {
        $pdf->AddPage();
        $vv = $pdf->gety();
    }
    $miles = $_POST['dsaldoant'][$con];
    $pdf->MultiCell(26, $alturas, $_POST['dcuentas'][$con], 0, 'C', true, 0, '', '', true, 0, false, true, 0, 'M', true);
    $pdf->MultiCell(46, $alturas, $_POST['dncuentas'][$con], 0, 'L', true, 0, '', '', true, 0, false, true, 0, 'M', true);
    $pdf->MultiCell(41, $alturas, '$' . number_format($miles, 0, ',', '.'), 0, 'R', true, 0, '', '', true, 0, false, true, 0, 'M', true);
    $pdf->MultiCell(41, $alturas, '$' . number_format($_POST['dsaldochip'][$con], 0, ',', '.'), 0, 'R', true, 0, '', '', true, 0, false, true, 0, 'M', true);
    $pdf->MultiCell(36, $alturas, '$' . number_format($_POST['dsaldif'][$con], 0, ',', '.'), 0, 'R', true, 1, '', '', true, 0, false, true, 0, 'M', true);
    $con++;
}
$pdf->Output();
