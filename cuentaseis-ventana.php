<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require "comun.inc";
	require"funciones.inc";
	session_start();
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html lang=es>
	<head>
		<meta charset="utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1"/>
		<meta name="viewport" content="user-scalable=no">
        <title>:: SPID</title>
		<link href="css/css2z.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
        <script type="text/javascript" src="css/programas.js"></script>
		<script>
			function ponprefijo(pref,opc,objeto,nobjeto){  
				parent.document.getElementById(''+objeto).value =pref;
				if (document.getElementById('tnobjeto').value!="000")
				{parent.document.getElementById(''+nobjeto).value =opc;}
				parent.despliegamodal2("hidden");
			} 
		</script> 
		<?php titlepag();?>
	</head>
	<body>
		<form action="" method="post" name="form2">
			<?php 
				if($_POST['oculto'] == "")
				{
					$_POST['tobjeto'] = $_GET['objeto'];
					$_POST['tnobjeto'] = $_GET['nobjeto'];
					$_POST['numpos'] = 0;
					$_POST['numres'] = 10;
					$_POST['nummul'] = 0;
				}
			?>
			<table class="inicio ancho">
				<tr>
					<td class="titulos" colspan="3" >Buscar CUENTAS</td>
					<td class="cerrar" style="width:7%" onClick="parent.despliegamodal2('hidden');">Cerrar</td>
				</tr>
				<tr><td class="titulos2" colspan="4">:&middot; Por Descripcion </td></tr>
				<tr>
					<td class="saludo1" style='width:4cm;'>::N&deg; Cuenta o Descripci&oacute;n:</td>
					<td style='width:20%;'><input type="search" name="nombre" id="nombre" value="<?php echo $_POST['nombre'];?>" style='width:100%;'/></td>
					<td style="padding-bottom:0px"><em class="botonflechaverde" onclick="limbusquedas()">Buscar</em></td>	
				</tr>      
			</table>
			<input type="hidden" name="oculto" id="oculto" value="1" >
			<input type="hidden" name="tobjeto" id="tobjeto" value="<?php echo $_POST['tobjeto']?>">
			<input type="hidden" name="tnobjeto" id="tnobjeto" value="<?php echo $_POST['tnobjeto']?>">
			<input type="hidden" name="numres" id="numres" value="<?php echo $_POST['numres'];?>"/>
			<input type="hidden" name="numpos" id="numpos" value="<?php echo $_POST['numpos'];?>"/>
			<input type="hidden" name="nummul" id="nummul" value="<?php echo $_POST['nummul'];?>"/>
			<?php
				$cond="";
				if ($_POST['nombre']!=""){
					$cond="WHERE concat_ws(' ', tabla.cuenta, tabla.nombre) LIKE '%$_POST[nombre]%'";$cond1="WHERE concat_ws(' ', cuenta, nombre) LIKE '%$_POST[nombre]%'"; 
				}
				$sqlr="SELECT * FROM (SELECT cn1.cuenta,cn1.nombre,cn1.naturaleza,cn1.centrocosto,cn1.tercero,cn1.tipo,cn1.estado FROM cuentasnicsp AS cn1 INNER JOIN cuentasnicsp AS cn2 ON cn2.tipo='Auxiliar'  AND cn2.cuenta LIKE CONCAT( cn1.cuenta,  '%' ) WHERE cn1.tipo='Mayor' AND cn1.cuenta LIKE '6%' GROUP BY cn1.cuenta UNION SELECT cuenta,nombre,naturaleza,centrocosto,tercero,tipo,estado FROM cuentasnicsp WHERE tipo='Auxiliar' AND cuenta LIKE '6%') AS tabla $cond ORDER BY 1 ";
				$resp = mysqli_query($linkbd,$sqlr);
				$_POST['numtop']=mysqli_num_rows($resp);
				$co='saludo1a';
				$co2='saludo2';	
				$i=1;
				echo"
				<table class='tablamv' style='height:80vh;'>
					<thead>
						<tr>
							<th colspan='5' class='titulos'>Resultados Busqueda: ".$_POST['numtop']."</th>
						</tr>
						<tr>
							<th style='width:4%'  class='titulos2' >Item</th>
							<th style='width:12%' class='titulos2' >Cuenta </th>
							<th class='titulos2' >Descripcion</th>
							<th style='width:14%' class='titulos2' >Tipo</th>
							<th style='width:6%' class='titulos2' >Estado</th>	
						</tr>
					</thead>
					<tbody>";
					while ($r = mysqli_fetch_row($resp)) 
					{	
						$con2=$i+ $_POST['numpos'];	
						echo"<tr class='$co' style='text-transform:uppercase'";
						if ($r[5]=='Auxiliar'){echo "onClick=\"javascript:ponprefijo('$r[0]','$r[1]','$_POST[tobjeto]','$_POST[tnobjeto]')\"";} 
						echo ">
						<td style='width:4%'>$con2</td>
						<td style='width:12%'>$r[0]</td>
						<td>$r[1]</td>
						<td style='width:14%'>$r[5]</td>
						<td style='text-align:center;width:6%;'>$r[6]</td></tr>";
						$aux=$co;
						$co=$co2;
						$co2=$aux;
						$i=1+$i;
					}
					echo"
						</tbody>
					</table>";
					
				?>
            <input type="hidden" name="numtop" id="numtop" value="<?php echo $_POST['numtop'];?>"/>
		</form>
	</body>
</html>