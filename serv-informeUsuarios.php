<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require 'comun.inc';
	require 'funciones.inc';
	session_start();
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html lang=es>
	<head>
    <meta charset="utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1"/>
		<meta name="viewport" content="user-scalable=no">
		<title>:: IDEAL 10 - Servicios públicos</title>
		<link href="favicon.ico" rel="shortcut icon"/>
		<link href="css/css2z.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
        <link href="css/tabs.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" href="sweetalert2/dist/sweetalert2.min.css">
        <script type="text/javascript" src="css/programas.js"></script>
        <script src="sweetalert2/dist/sweetalert2.min.js"></script>
		<script src="vue/vue.min.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script src="css/calendario.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
	</head>
	<body>
		<header>
			<table>
				<tr><script>barra_imagenes("serv");</script><?php cuadro_titulos();?></tr>
			</table>
		</header>

		<form name="form2" method="post" action="">
			<section id="myapp" v-cloak >
				<nav>
					<table>
						<tr><?php menu_desplegable("serv");?></tr>
						<tr>
							<td colspan="3" class="cinta">
								<img src="imagenes/add2.png" class="mgbt" title="Nuevo">
								<img src="imagenes/guardad.png" title="Guardar"  class="mgbt">
								<img src="imagenes/buscad.png" class="mgbt" title="Buscar">
								<img src="imagenes/nv.png" onClick="mypop=window.open('serv-principal','',''); mypop.focus();" class="mgbt" title="Nueva Ventana">
								<img src="imagenes/excel.png" title="Excel" @click="bajarExcel" class="mgbt">
								<a href="serv-menuInformes"><img src="imagenes/iratras.png" class="mgbt" alt="Atrás"></a>
							</td>
						</tr>
					</table>
				</nav>

				<article>
					<div>
						<table class="inicio">
							<tr>
								<td class="titulos" colspan="8">.: Informe de usuarios:</td>
                            	<td class="cerrar" style="width:4%" onClick="location.href='serv-principal.php'">Cerrar</td>
							</tr>
						</table>
					</div>

                    <div>
                        <div class='subpantalla' style='height:66vh; width:99.2%; margin-top:0px; overflow:hidden'>
                            <table class='tablamv'>        
                                <thead>
                                    <tr>
                                        <th class="titulosnew00" width="10%">Código usuario</th>
                                        <th class="titulosnew00" width="10%">Documento</th>
                                        <th class="titulosnew00" width="10%">Cedula catastral</th>
                                        <th class="titulosnew00">Nombre suscriptor</th>
                                        <th class="titulosnew00" width="20%">Dirección</th>
                                        <th class="titulosnew00" width="10%">Barrio</th>
                                        <th class="titulosnew00" width="10%">Estrato predio</th>
                                    </tr>
                                </thead>

                                <tbody>
                                    <tr v-for="(usuario,index) in usuarios" v-bind:class="(index % 2 ? 'saludo1a' : 'saludo2')" style="font: 100% sans-serif;">
										<td style="text-align:center; width:10%;">{{ usuario['Codigo'] }}</td>
										<td style="text-align:center; width:10%;">{{ usuario['Documento'] }}</td>
										<td style="text-align:center; width:10%;">{{ usuario['Catastral'] }}</td>
										<td style="text-align:left;">{{ usuario['Nombre'] }}</td>
										<td style="text-align:center; width:20%;">{{ usuario['Direccion'] }}</td>
										<td style="text-align:center; width:10%;">{{ usuario['Barrio'] }}</td>
										<td style="text-align:center; width:10%;">{{ usuario['Estrato'] }}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>

					<div id="cargando" v-if="loading" class="loading">
						<span>Cargando...</span>
					</div>

				</article>
			</section>
		</form>

		<script src="node_modules/read-excel-file/bundle/read-excel-file.min.js"></script>
		<script src="node_modules/xlsx/dist/xlsx.full.min.js"></script>
        <script src="Librerias/vue/vue.min.js"></script>
		<script src="Librerias/vue/axios.min.js"></script>
		<script type="module" src="servicios_publicos/informeUsuarios/serv-informeUsuario.js"></script>
        
	</body>
</html>