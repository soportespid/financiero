<?php
	require_once("tcpdf/tcpdf_include.php");    
	require('comun.inc');
    require "funciones.inc";
	require "conversor.php";
	session_start();	
	date_default_timezone_set("America/Bogota");
    class MYPDF extends TCPDF 
	{
		public function Header() 
		{
            $linkbd=conectar_v7();
			$linkbd -> set_charset("utf8");
			$sqlr = "SELECT * FROM configbasica WHERE estado='S'";
			$res = mysqli_query($linkbd,$sqlr);
			while($row=mysqli_fetch_row($res)){
				$nit=$row[0];
				$rs=$row[1];
			}
			$this->Image('imagenes/escudo.jpg', 12, 12, 25, 25, 'JPG', '', 'T', true, 300, '', false, false, 0, false, false, false);// Logo
			$this->SetFont('helvetica','B',8);
			$this->SetY(10);
            $this->RoundedRect(10, 10, 190, 29, 1,'1001' );
			$this->Cell(30,29,'','R',0,'L'); 
			$this->SetY(10);
			$this->SetX(40);
			$this->SetFont('helvetica','B',12);
			$this->Cell(127,13,"$rs",0,0,'C'); 
			$this->SetY(16);
			$this->SetX(40);
			$this->SetFont('helvetica','B',11);
			$this->Cell(127,10,"$nit",0,0,'C');
			//*****************************************************************************************************************************
			$this->SetFont('helvetica','B',14);
			$this->SetY(10);
			$this->Cell(50.1);
			$this->Cell(149,31,'',0,1,'C'); 
			$this->SetY(8);
			$this->Cell(50.1);
			//************************************
			$this->SetFont('helvetica','B',10);
			$this->SetY(10);
			$this->Cell(157);
			$this->Cell(33,15,'','LB',0,'L');
			$this->SetY(10);
			$this->Cell(157.5);
			$this->Cell(33,8,'Numero: '.$_POST['ncomp'],0,0,'L');
			$this->SetY(18);
			$this->Cell(157.5);
			$this->Cell(33,7,'Fecha: '.$_POST['fecha'],0,0,'L');
   
			$this->SetY(20);
            $this->SetY(25);
            $this->SetX(40);
        	$this->SetFont('helvetica','B',12);
            $sql = "SELECT nombre FROM tipo_comprobante WHERE codigo='".$_POST['tipocomprobante']."' AND estado='S' ";
			$res = mysqli_query($linkbd, $sql);
			$fila = mysqli_fetch_row($res);
            $this->Cell(127,14,$fila[0],'T',1,'C'); 
            $this->SetFont('helvetica','B',10);
			$lineas = $this->getNumLines($_POST['concepto'],160);
			$lineas2 = $lineas * 5;
			$this->Cell(30,$lineas2,'CONCEPTO:','LB',0,'C');
			$this->SetFont('helvetica','',10);
			$this->MultiCell(160,$lineas2,$_POST['concepto'],'RB','L',0,1,'','');
		

		}
		public function Footer() 
		{
			$linkbd = conectar_v7();
			$linkbd -> set_charset("utf8");
			$sqlr="SELECT direccion,telefono,web,email FROM configbasica WHERE estado='S'";
			$resp=mysqli_query($linkbd, $sqlr);
			$user = $_SESSION['nickusu'];	
			$fecha = date("Y-m-d H:i:s");
			$ip = $_SERVER['REMOTE_ADDR'];
			$useri = $_POST['user'];
			while($row=mysqli_fetch_row($resp))
			{
				$direcc=strtoupper($row[0]);
				$telefonos=$row[1];
				$dirweb=strtoupper($row[3]);
				$coemail=strtoupper($row[2]);
			}
			if($direcc!=''){$vardirec="Dirección: $direcc, ";}
			else {$vardirec="";}
			if($telefonos!=''){$vartelef="Telefonos: $telefonos";}
			else{$vartelef="";}
			if($dirweb!=''){$varemail="Email: $dirweb, ";}
			else {$varemail="";}
			if($coemail!=''){$varpagiw="Pagina Web: $coemail";}
			else{$varpagiw="";}
			
			$txt = <<<EOD
			$vardirec $vartelef
			$varemail $varpagiw
			EOD;
			$this->SetFont('helvetica', 'I', 5);
			$this->Cell(190,7,'','T',0,'T');
			$this->ln(1);
			$this->Write(0, $txt , '', 0, 'C', true, 0, false, false, 0);
			//$this->Cell(12, 7, 'Hecho por: '.$useri, 0, false, 'C', 0, '', 0, false, 'T', 'M');
			$this->Cell(15, 7, 'Impreso por: '.$user, 0, false, 'C', 0, '', 0, false, 'T', 'M');
			$this->Cell(57, 7, 'IP: '.$ip, 0, false, 'C', 0, '', 0, false, 'T', 'M');
			$this->Cell(50, 7, 'Fecha: '.$fecha, 0, false, 'C', 0, '', 0, false, 'T', 'M');
			$this->Cell(54, 7, 'IDEAL.10 S.A.S', 0, false, 'C', 0, '', 0, false, 'T', 'M');
			$this->Cell(25, 7, 'Pagina '.$this->getAliasNumPage().'/'.$this->getAliasNbPages(), 0, false, 'C', 0, '', 0, false, 'T', 'M');

		}
	}
    
	

	$pdf = new MYPDF('P','mm','Letter', true, 'utf8', false);
	$pdf->SetDocInfoUnicode (true); 
	// set document information
	$pdf->SetCreator(PDF_CREATOR);
	$pdf->SetAuthor('IDEAL10');
	$pdf->SetTitle('COMPROBANTE');
	$pdf->SetSubject('COMPROBANTE');
	$pdf->SetKeywords('COMPROBANTE');
    $pdf->SetMargins(10, 45, 10);// set margins
    $pdf->SetHeaderMargin(45);// set margins
    $pdf->SetFooterMargin(17);// set margins
	$pdf->SetAutoPageBreak(TRUE, 20);// set auto page breaks
	// set some language-dependent strings (optional)
	if (@file_exists(dirname(__FILE__).'/lang/spa.php')) 
	{
		require_once(dirname(__FILE__).'/lang/spa.php');
		$pdf->setLanguageArray($l);
	}
	$pdf->AddPage();
    //inicia tabla
	$pdf ->ln(15);
	$yy=$pdf->GetY();	
	$pdf->SetY($yy);
	$yy=$pdf->GetY();	
	$pdf->SetFillColor(222,222,222);
	$pdf->SetFont('helvetica','B',9);
	$pdf->Cell(0.1);
	$pdf->Cell(20,5,'CODIGO',0,0,'C',1); 
	$pdf->SetY($yy);
	$pdf->Cell(21);
	$pdf->Cell(45,5,'CUENTA',0,0,'C',1);
	$pdf->SetY($yy);
	$pdf->Cell(67);
	$pdf->Cell(25,5,'TERCERO',0,0,'C',1);
	$pdf->SetY($yy);
	$pdf->Cell(93);
	$pdf->Cell(10,5,'C.C.',0,0,'C',1);
	$pdf->SetY($yy);
	$pdf->Cell(104);
	$pdf->Cell(35,5,'DETALLE',0,0,'C',1);
    $pdf->SetY($yy);
	$pdf->Cell(140);
	$pdf->Cell(25,5,'DEBITO',0,0,'C',1);
	$pdf->SetY($yy);
	$pdf->Cell(166);
	$pdf->MultiCell(24,5,'CREDITO',0,'C',1,1,'','',true,'',false,true,0,'M',true);
	$pdf->SetFont('helvetica','',7);
	$pdf->ln(1);
	$con=0;
	while($con<count($_POST['dcuentas'])){
		if ($con%2==0)
		{
			$pdf->SetFillColor(245,245,245);
		}
		else
		{
			$pdf->SetFillColor(255,255,255);
		}
        $var = ucfirst(strtolower($_POST['dncuentas'][$con]));
		$lineascod = $pdf-> getNumLines($var, 46);
		$lineaspred = $pdf-> getNumLines($_POST['ddetalles'][$con], 36);
		$max = max($lineascod, $lineaspred);
		$altura = $max * 3;
		$pdf->MultiCell(21,$altura,$_POST['dcuentas'][$con],0,'C',true,0,'','',true,0,false,true,0,'M',true);
        $pdf->MultiCell(46,$altura,$var,0,'C',true,0,'','',true,0,false,true,0,'M',true);
		$pdf->MultiCell(26,$altura,''.substr(ucfirst(strtolower($_POST['dterceros'][$con])),0,25),0,'C',true,0,'','',true,0,false,true,0,'M',true);
        $pdf->MultiCell(11,$altura,''.$_POST['dccs'][$con],0,'C',true,0,'','',true,0,false,true,0,'M',true);
        $pdf->MultiCell(36,$altura,''.substr(ucfirst(strtolower($_POST['ddetalles'][$con])),0,25),0,'C',true,0,'','',true,0,false,true,0,'M',true);
        $pdf->MultiCell(25,$altura,'$'.number_format($_POST['ddebitos'][$con],2,".",","),0,'R',true,0,'','',true,0,false,true,0,'M',true);
        $pdf->MultiCell(25,$altura,'$'.number_format($_POST['dcreditos'][$con],2,".","."),0,'R',true,1,'','',true,0,false,true,0,'M',true);
		$con++;
	}
	//termina tabla
    $pdf->ln(2);
    $pdf->Cell(140,5,'Total','T',0,'R');
    $pdf->SetX(150);
    $pdf->Cell(25,5,'$'.$_POST['cuentadeb'],'T',0,'R');
    $pdf->Cell(26,5,'$'.$_POST['cuentacred'],'T',0,'R');
    $pdf->ln(5);
    
	$pdf->Output();