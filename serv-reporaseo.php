<?php
	require"comun.inc";
	require"funciones.inc";
	require"serviciospublicos.inc";
	session_start();
	$linkbd=conectar_bd();	
	cargarcodigopag($_GET[codpag],$_SESSION["nivel"]);
	header("Cache-control: private"); // Arregla IE 6
	header("Content-Type: text/html;charset=iso-8859-1");
	date_default_timezone_set("America/Bogota");
?>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es">
	<head>
	 	<meta http-equiv="Content-type" content="text/html; charset=iso-8859-1"/>
        <meta http-equiv="X-UA-Compatible" content="IE=9"/>
        <title>:: SPID - Servicios P&uacute;blicos</title>
		<link href="css/css2.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
        <link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="css/programas.js"></script>
        <script type="text/javascript" src="css/calendario.js"></script>
		<script>
			function validar()
			{	
				if (document.form2.mes.value=="")
				{
					alert ("Por favor seleccione una vigencia");
				}
				else if (document.form2.vigencias.value=="")
				{alert ("Por favor seleccione un mes");
				}						
				else 
				{
					document.form2.oculto.value='1'
					document.form2.submit();
				}
			}
			function fpdf()
			{
				document.form2.action="pdfsubsidios.php";
				document.form2.target="_BLANK";
				document.form2.submit(); 
				document.form2.action="";
				document.form2.target="";
			}
			function fexcel()
			{
				document.form2.action="serv-reporaseoexcel.php";//serv-reporaseocsv.php
				document.form2.target="_BLANK";
				document.form2.submit(); 
				document.form2.action="";
				document.form2.target="";
			}
			function callprogress(vValor)
			{
 				document.getElementById("getprogress").innerHTML = vValor;
 				document.getElementById("getProgressBarFill").innerHTML = '<div class="ProgressBarFill" style="width: '+vValor+'%;"></div>';				
				document.getElementById("titulog1").style.display='block';
   				document.getElementById("progreso").style.display='block';
     			document.getElementById("getProgressBarFill").style.display='block';
				if (vValor==100){document.getElementById("titulog2").style.display='block';}
			} 
		</script>
	</head>
	<body>
		<IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
		<span id="todastablas2"></span>
        <table>
            <tr><script>barra_imagenes("serv");</script><?php cuadro_titulos();?></tr>	 
            <tr><?php menu_desplegable("serv");?></tr>
        	<tr>
  				<td colspan="3" class="cinta"><img src="imagenes/add.png" title="Nuevo" class="mgbt" onClick="location.href='serv-reportesubs.php'"/><img src="imagenes/guardad.png" class="mgbt1"/><img src="imagenes/buscad.png" class="mgbt1"/><img src="imagenes/nv.png" title="Nueva Ventana" onClick="mypop=window.open('serv-principal.php','','');mypop.focus();" class="mgbt"/><img src="imagenes/excel.png" title="Excel" class="mgbt" onClick="fexcel()"/><img src="imagenes/print.png" title="imprimir" onClick="fpdf()"></td>
        	</tr>	
		</table>
 		<form name="form2" method="post" action="serv-reporaseo.php">
			<?php
                $meses=array('','Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre');
                $vigusu=vigencia_usuarios($_SESSION[cedulausu]);
                $vigencia=$vigusu;
                $vact=$vigusu;  
            ?>
			<table  class="inicio" align="center" >
      			<tr >
        			<td class="titulos" colspan="10">:. Reporte Aseo - Usuarios </td>
        			<td class="cerrar" style="width:7%" onClick="location.href='serv-principal.php'">Cerrar</td>
      			</tr>
      			<tr >
                    <td class="saludo1" style="width:2cm">vigencia:</td>
                    <td style="width:5%">
                        <select name="vigencias" id="vigencias" >
                            <option value="">Sel..</option>
                            <?php	  
                                for($x=$vact;$x>=$vact-2;$x--)
                                {
                                    if($x==$_POST[vigencias]){echo "<option value='$x' SELECTED>$x</option>";}
                                    else {echo "<option value='$x'>$x</option>";}
                                }
                            ?>
                        </select> 
                    </td>
                    <td class="saludo1" style="width:2cm">Mes:</td>
                    <td style="width:8%">
                        <select name="mes" id="mes">
                            <option value="">Seleccione ...</option>
                            <?php
                                for($x=1;$x<=12;$x++)
                                {
                                    if($_POST[mes]==$x){echo "<option value='$x' SELECTED>$meses[$x]</option>";}
                                    else {echo "<option value='$x'>$meses[$x]</option>";}
                                }
                            ?>  
                        </select> 
                        <input name="oculto" type="hidden" value="0">  
                    </td>  
                    <td><input type="button" name="buscar" id="buscar" value="  Buscar  " onClick="validar()"></td>  
                    <td>
						<div id='titulog1' style='display:none; float:left'></div>
						<div id='progreso' class='ProgressBar' style='display:none; float:left'>
							<div class='ProgressBarText'><span id='getprogress'></span>&nbsp;% </div>
							<div id='getProgressBarFill'></div>
						</div>
					</td>  
                </tr>
   			</table>     
			<div class="subpantalla" style="height:70%">
      			<?php
	  				$vigusu=vigencia_usuarios($_SESSION[cedulausu]);
	  				$oculto=$_POST[oculto];
	  				if($_POST[oculto]=='1')
					{
         				$crit1=" ";
         				$con=1;
						$crit2 = "WHERE T1.id_liquidacion=T2.id_liquidacion AND T1.servicio LIKE '03'";
						$crit4 = "AND T2.vigencia = '$_POST[vigencias]' AND (T2.mes='$_POST[mes]' OR  T2.mesfin='$_POST[mes]')";
						$crit5 = "ORDER BY T2.codusuario, T2.id_liquidacion";
						$sqlr1="SELECT DISTINCT T2.vigencia,T2.mes,T2.mesfin,T2.codusuario,T2.id_liquidacion,T1.servicio,T1.estrato,T1.tarifa, T1.subsidio,T1.contribucion, T2.fecha,T1.valorliquidacion,T1.abono,T1.saldo FROM servliquidaciones_det T1, servliquidaciones T2 $crit2 $crit3 $crit4 $crit5";		
						$resp1 = mysql_query($sqlr1,$linkbd);
						$ntr = mysql_num_rows($resp1);
						$totalcli=mysql_affected_rows ($linkbd);
						echo "
						<table class='inicio' align='center' >
							<tr>
								<td colspan='39' class='titulos'>.: Resultados Busqueda:</td>
							</tr>
							<tr>
								<td colspan='39'>Usuarios Encontrados: $ntr</td>
							</tr>
							<tr>
								<td width='150' class='titulos2'>NUID</td>
								<td width='150' class='titulos2'>No.Cuenta</td>
								<td width='50' class='titulos2'>Codigo Depto</td>
								<td width='50' class='titulos2'>Codigo Mpio</td>
								<td width='50' class='titulos2'>Zona IGAC</td>
								<td width='50' class='titulos2'>Sector IGAC</td>
								<td width='50' class='titulos2'>Mza IGAC</td>
								<td width='50' class='titulos2'>Predio IGAC</td>
								<td width='50' class='titulos2'>Cond. IGAC</td>
								<td width='150' class='titulos2'>Fecha Expedici&oacute;n</td>
								<td width='50' class='titulos2'>NUAP</td>
								<td width='150' class='titulos2'>Inicio Per&iacute;odo</td>
								<td width='100' class='titulos2'>Factura</td>
								<td width='50' class='titulos2'>Multiusuario</td>
								<td width='50' class='titulos2'>Refacturacion</td>
								<td width='50' class='titulos2'>No. Factura</td>
								<td width='50' class='titulos2'>Ubicacion</td>
								<td width='50' class='titulos2'>Clase de Uso</td>
								<td width='50' class='titulos2'>Cod. Clase de Uso</td>
								<td width='50' class='titulos2'>Cod. Producción</td>
								<td width='50' class='titulos2'>Hogar Comunitario</td>
								<td width='50' class='titulos2'>Días facturados</td>
								<td width='50' class='titulos2'>Tipo de Aforo</td>
								<td width='50' class='titulos2'>Producción del Suscriptor</td>
								<td class='titulos2'>Subsidio o Contribucio&oacute;</td>
								<td class='titulos2'>Reliquidacion </td>
								<td class='titulos2'>Mora </td>
								<td class='titulos2'>Intereses </td>
								<td class='titulos2'>Saldo a Favor Usuario </td>
								<td class='titulos2'>Valor Total</td>
								<td class='titulos2'>Pagos del Cliente</td>
								<td class='titulos2'>Fx Recolección</td>
								<td class='titulos2'>Fx Barrido</td>
								<td class='titulos2'>Tarifa</td>
								<td class='titulos2'>Dirección</td>
								<td class='titulos2'>Facturación Conjunta</td>
								<td class='titulos2'>Facturación Subsidio</td>
								<td class='titulos2'>Dcto Recolección</td>
								<td class='titulos2'>Km de cuneta adicional</td>
							</tr>";	
						$iter='zebra1';
						$iter2='zebra2';
						$total=0;
						$c=0;
 						while ($row =mysql_fetch_row($resp1)) 
						{	
							$c+=1;
							$porcentaje = $c * 100 / $totalcli; 
							echo"<script>progres='".round($porcentaje)."';callprogress(progres);</script>"; 
							flush();
							ob_flush();
							usleep(5);//microsegundos
 							$sqlr="select *from servclientes where codigo='$row[3]'";
							$respc=mysql_query($sqlr,$linkbd);
							$rowc =mysql_fetch_row($respc);
							echo "
							<tr class='$iter' >
								<td>$rowc[2]</td>
								<td>$row[3]</td>
								<td>50</td>
								<td>223</td>
								<td>".substr($rowc[2],0,2)."</td>
								<td>".substr($rowc[2],2,2)."</td>
								<td>".substr($rowc[2],4,4)."</td>
								<td>".substr($rowc[2],8,4)."</td>
								<td>".substr($rowc[2],12,3)."</td>
								<td>$row[10]</td>
								<td>27488</td>
								<td>1-$row[1]-$row[0]</td>
								<td>$row[4]</td>
								<td></td>
								<td></td>
								<td></td>
								<td>2</td>";
							if(($row[6]>10)&&($row[6]<15)){echo"<td>R</td>";}
							else {echo"<td>NR</td>";}
							switch ($row[6])
                            {
								case '11':	echo"<td>1 Bajo-Bajo</td>";break;
                                case '12': 	echo"<td>2 Bajo</td>";break;
                                case '13': 	echo"<td>3 Medio-Bajo</td>";break;
                                case '141': echo"<td>4 Medio</td>";break;
								case '14': 	echo"<td>Oficial</td>";break;
                                default:	echo"<td>No Residencial</td>";
							}
						echo"
							<td>1</td>
							<td></td>
							<td>30</td>
							<td>1</td>
							<td>000000000.0000</td>";
						if($row[8]>0){echo"<td>-$row[8]</td>";}
						else {echo"<td>$row[9]</td>";}
						echo"	
							<td>0</td>
							<td>$row[13]</td>
							<td>0</td>
							<td>0</td>
							<td>$row[11]</td>
							<td>$row[12]</td>
							<td>2</td>
							<td>2</td>
							<td>$row[7]</td>
							<td>$rowc[4]</td>
							<td>0</td>
							<td>0</td>
							<td>0</td>
							<td>0</td>
						</tr>";
						$con+=1;
						$aux=$iter;
						$iter=$iter2;
						$iter2=$aux;
						$total+=$row[3];
 					}
 						echo "<tr><td colspan='5'></td><td>".number_format($total,2,",",".")."</td></tr>";
 						echo"</table>";
					}
				?>
          	</div>
 		</form>
	</body>
</html>