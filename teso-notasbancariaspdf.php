<?php
	require_once("tcpdf/tcpdf_include.php");
	require('comun.inc');
	require"funciones.inc";
	date_default_timezone_set("America/Bogota");
	session_start();
	$val = 0;
	class MYPDF extends TCPDF{
		public function Header(){
			$linkbd = conectar_v7();
			$linkbd -> set_charset("utf8");
			$numnota = $_GET['idnota'];
			$sql = "SELECT estado, tipo_mov, fecha FROM tesonotasbancarias_cab WHERE id_comp = '$numnota'";
			$res = mysqli_query($linkbd,$sql);
			$row = mysqli_fetch_row($res);
			$estado = $row[0];
			$tipomov = $row[1];
			$fechanota = date('d/m/Y',strtotime($row[2]));
			$vigencia = date('Y',strtotime($row[2]));
			if ($estado == 'R'){
				$this->Image('imagenes/reversado02.png',75,41.5,50,15);
			}
			$sql = "SELECT * FROM configbasica WHERE estado = 'S' ";
			$res = mysqli_query($linkbd,$sql);
			$row = mysqli_fetch_row($res);
			$nit = $row[0];
			$rs = $row[1];
			//Parte Izquierda
			$this->Image('imagenes/escudo.jpg', 22, 12, 25, 23.9, 'JPG', '', 'T', true, 300, '', false, false, 0, false, false, false);// Logo
			$this->SetFont('helvetica','B',8);
			$this->SetY(10);
			$this->RoundedRect(10, 10, 199, 31, 1,'' );
			$this->Cell(0.1);
			$this->Cell(50,31,'','R',0,'L'); 
			$this->SetY(11);
			$this->SetX(60);
			$this->SetFont('helvetica','B',12);
			$this->Cell(149,12,strtoupper($rs),0,0,'C'); 
			$this->SetFont('helvetica','B',8);
			$this->SetY(18);
			$this->SetX(60);
			$this->SetFont('helvetica','B',11);
			$this->Cell(149,10,"$nit",0,0,'C');
			//*****************************************************************************************************************************
			$this->SetFont('helvetica','B',14);
			$this->SetY(10);
			$this->Cell(50.1);
			$this->Cell(149,31,'',0,1,'C'); 
			$this->SetY(27);
			$this->SetX(60);
			$this->Cell(111,14,"NOTAS BANCARIAS",0,0,'C'); 
			$mov='';
			if(isset($tipomov)){
				if(!empty($tipomov)){
					if($tipomov == '401'){
						$mov = "DOCUMENTO DE REVERSION";
					}
				}
			}
			$this->SetFont('helvetica','B',10);
			$this->SetY(15);
			$this->Cell(50.1);
			$this->Cell(149,20,$mov,0,0,'C'); 
			//************************************
			$this->SetFont('helvetica','I',7);
			$this->SetY(27);
			$this->Cell(50.2);
			$this->multiCell(110.7,3,'','T','L');
			$this->SetFont('helvetica','B',10);
			$this->SetY(27);
			$this->Cell(161.1);
			$this->Cell(37.8,14,'','TL',0,'L');
			$this->SetY(27);
			$this->Cell(162);
			$this->Cell(35,5,'NUMERO : '.$numnota,0,0,'L');
			$this->SetY(31);
			$this->Cell(162);
			$this->Cell(35,5,'FECHA: '.$fechanota,0,0,'L');
			$this->SetY(35);
			$this->Cell(162);
			$this->Cell(35,5,'VIGENCIA: '.$vigencia,0,0,'L');
			$this->SetY(27);
			$this->Cell(50.2);
			$this->MultiCell(105.7,4,'',0,'L');		
			//**********************************************************
			$this->SetFont('times','B',10);
			$this->ln(12);
			//**********************************************************
		}
		public function Footer(){
			$linkbd = conectar_v7();
			$linkbd -> set_charset("utf8");
			$sqlr = "SELECT direccion, telefono ,web, email FROM configbasica WHERE estado='S'";
			$resp = mysqli_query($linkbd,$sqlr);
			while($row = mysqli_fetch_row($resp)){
				$direcc = strtoupper($row[0]);
				$telefonos = $row[1];
				$dirweb = strtoupper($row[3]);
				$coemail = strtoupper($row[2]);
			}
			if($direcc != ''){
				$vardirec = "Dirección: $direcc, ";
			}else {
				$vardirec="";
			}
			if($telefonos != ''){
				$vartelef = "Telefonos: $telefonos";
			}else{
				$vartelef = "";
			}
			if($dirweb != ''){
				$varemail = "Email: $dirweb, ";
			}else {
				$varemail = "";
			}
			if($coemail!=''){
				$varpagiw = "Pagina Web: $coemail";
			}else{
				$varpagiw = "";
			}
			$this->SetFont('helvetica', 'I', 8);
			$txt = <<<EOD
$vardirec $vartelef
$varemail $varpagiw
EOD;
			$this->Write(0, $txt, '', 0, 'C', true, 0, false, false, 0);
			$this->Cell(0, 10, 'Page '.$this->getAliasNumPage().'/'.$this->getAliasNbPages(), 0, false, 'C', 0, '', 0, false, 'T', 'M');
		}
	}
	$pdf = new MYPDF('P','mm','Letter', true, 'utf8', false);
	$pdf->SetDocInfoUnicode (true); 
	// set document information
	$pdf->SetCreator(PDF_CREATOR);
	$pdf->SetAuthor('G&CSAS');
	$pdf->SetTitle('Certificados');
	$pdf->SetSubject('Notas bancarias');
	$pdf->SetKeywords('TCPDF, PDF, example, test, guide');
	$pdf->SetMargins(10, 50, 10);// set margins
	$pdf->SetHeaderMargin(101);// set margins
	$pdf->SetFooterMargin(20);// set margins
	$pdf->SetAutoPageBreak(TRUE, 20);// set auto page breaks
	// set some language-dependent strings (optional)
	if (@file_exists(dirname(__FILE__).'/lang/spa.php')) 
	{
		require_once(dirname(__FILE__).'/lang/spa.php');
		$pdf->setLanguageArray($l);
	}
	$pdf->AddPage();
	$pdf->SetFont('Times','',10);
	$pdf->SetAutoPageBreak(true,20);
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
	$numnota = $_GET['idnota'];
	$sql = "SELECT estado, tipo_mov, fecha, concepto FROM tesonotasbancarias_cab WHERE id_comp = '$numnota'";
	$res = mysqli_query($linkbd,$sql);
	$row = mysqli_fetch_row($res);
	$estado = $row[0];
	$tipomov = $row[1];
	$detallegreso = $row[3];

	$pdf->ln(5);
	$pdf->cell(0.2);
	$pdf->SetFillColor(255,255,255);

	$lineas = $pdf->getNumLines($detallegreso, 174);
	$alturadt=(5*$lineas);
	$pdf->SetFont('helvetica','B',10);
	$pdf->MultiCell(25,$alturadt,'CONCEPTO: ', '', 'J', 0, 0, '', '', true, 0, false, true, $alturadt, 'T');
	$pdf->SetFont('helvetica','',10);
	$pdf->MultiCell(174,$alturadt,"$detallegreso",'','L',false,1,'','',true,0,false,true,$alturadt,'T',false);

	$pdf->line(10.1,66,209,66);
	$pdf->RoundedRect(10,69, 199, 5, 1.2,'' );
	$pdf->SetFont('helvetica','B',10);
	$pdf->SetY(69);
	$pdf->Cell(0.1);
	$pdf->Cell(16,5,'CC',0,0,'C'); 
	$pdf->Cell(25,5,'DOC BANC.',0,0,'C');
	$pdf->Cell(35,5,'BANCO',0,0,'C');
	$pdf->Cell(95,5,'GASTO BANCARIO',0,0,'C');
	$pdf->Cell(28,5,'VALOR',0,0,'C');
	$pdf->ln(4);
		
	$sqlr = "SELECT * FROM tesonotasbancarias_det WHERE id_notabancab = '$numnota'";
	$res = mysqli_query($linkbd,$sqlr);
	while($row = mysqli_fetch_assoc($res)){
		$sqlgb = "SELECT nombre, tipo FROM tesogastosbancarios WHERE codigo = '".$row['gastoban']."'";
		$resgb = mysqli_query($linkbd, $sqlgb);
		$rowgb = mysqli_fetch_row($resgb);

		$pdf->ln(2);
		if ($con%2==0){$pdf->SetFillColor(255,255,255);}
		else{$pdf->SetFillColor(245,245,245);}
		$pdf->SetFont('helvetica','',10);
		$pdf->Cell(16,4,''.$row['cc'],'0',0,'C',1);
		$pdf->Cell(25,4,$row['docban'],'0',0,'C',1);
		$pdf->Cell(35,4,$row['ncuentaban'],'0',0,'C',1);
		$pdf->Cell(95,4,$rowgb[1]."-".$row['gastoban']."-".$rowgb[0],'0',0,'',1);
		$pdf->Cell(28,4,'$ '.$row['valor'],'0',1,'R',1);
		$con=$con+1;
	}
	$y = $pdf->GetY();
	$pdf->line(10.1,$y + 5,209,$y + 5);
	$pdf->ln(8);

	$vigusu = vigencia_usuarios($_SESSION['cedulausu']);

	$sqlr = "SELECT id_cargo, id_comprobante FROM pptofirmas WHERE id_comprobante = '20' AND vigencia = '".$vigusu."'";
	$res = mysqli_query($linkbd,$sqlr);
	while($row = mysqli_fetch_assoc($res)){
		if($row["id_cargo"] == '0'){
			$_POST['ppto'][] = buscatercero($_POST['tercero']);
			$_POST['nomcargo'][] = 'BENEFICIARIO';
		}else{
			$sqlr1 = "SELECT cedulanit,(SELECT nombrecargo FROM planaccargos WHERE codcargo='".$row["id_cargo"]."') FROM planestructura_terceros WHERE codcargo='".$row["id_cargo"]."' AND estado='S'";
			$res1 = mysqli_query($linkbd,$sqlr1);
			$row1 = mysqli_fetch_row($res1);
			$_POST['ppto'][] = buscar_empleado($row1[0]);
			$_POST['nomcargo'][] = $row1[1];
		}
	};
	for($x = 0; $x < count($_POST['ppto']); $x++){
		$pdf->ln(20);
		$v = $pdf->gety();
		if($v>=251){ 
			$pdf->AddPage();
			$pdf->ln(20);
			$v = $pdf->gety();
		}
		$pdf->setFont('times','B',8);
		if (($x%2)==0){
			if(isset($_POST['ppto'][$x+1])){
				$pdf->Line(17,$v,107,$v);
				$pdf->Line(112,$v,202,$v);
				$v2=$pdf->gety();
				$pdf->Cell(104,4,''.$_POST['ppto'][$x],0,1,'C',false,0,0,false,'T','C');
				$pdf->Cell(104,4,''.$_POST['nomcargo'][$x],0,1,'C',false,0,0,false,'T','C');
				$pdf->SetY($v2);
				$pdf->Cell(295,4,''.$_POST['ppto'][$x+1],0,1,'C',false,0,0,false,'T','C');
				$pdf->Cell(295,4,''.$_POST['nomcargo'][$x+1],0,1,'C',false,0,0,false,'T','C');
			}else{
				$pdf->Line(50,$v,160,$v);
				$pdf->Cell(190,4,''.$_POST['ppto'][$x],0,1,'C',false,0,0,false,'T','C');
				$pdf->Cell(190,4,''.$_POST['nomcargo'][$x],0,0,'C',false,0,0,false,'T','C');
			}
			$v3=$pdf->gety();
		}
		$pdf->SetY($v3);
		$pdf->SetFont('helvetica','',7);
	}
$pdf->Output();
?>