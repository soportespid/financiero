<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=uf8");
	require "comun.inc";
	require "funciones.inc";
	require "funcionesSP.inc.php";
	session_start();
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
    titlepag();
?>
<!DOCTYPE >
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
		<title>:: IDEAL 10 - Servicios P&uacute;blicos</title>
		<link href="css/css2n.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/css3n.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/cssSP.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/style.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
		<script type="text/javascript" src="jquery-1.11.0.min.js"></script> 
		<script type="text/javascript" src="css/programas.js"></script>
		<script>
			function editaUsuario(id)
			{
				location.href="serv-editaUsuario.php?id="+id;
			}

		</script>

		<?php

			$scrtop = @ $_GET['scrtop'];
			if($scrtop == "") $scrtop=0;
			echo"<script>
					window.onload=function()
					{
						$('#divdet').scrollTop(".$scrtop.")
					}
				</script>";
		?>
	</head>
	<body>
		<IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
		<span id="todastablas2"></span>
		<table>
			<tr><script>barra_imagenes("serv");</script><?php cuadro_titulos();?></tr>	 
			<tr><?php menu_desplegable("serv");?></tr>
			<tr>
				<td colspan="3" class="cinta">
					<a href="serv-crearUsuario.php" class="mgbt"><img src="imagenes/add.png"/></a>
					<a class="mgbt"><img src="imagenes/guardad.png" title="Guardar"/></a>
					<a href="serv-buscaCliente.php" class="mgbt"><img src="imagenes/busca.png" title="Buscar"/></a>
					<a onclick="mypop=window.open('plan-agenda.php','','');mypop.focus()" class="mgbt"><img src="imagenes/agenda1.png" title="Agenda" /></a>
					<a onclick="mypop=window.open('serv-principal.php','','');mypop.focus();" class="mgbt"><img src="imagenes/nv.png" title="Nueva ventana"></a>
					<a onclick="mypop=window.open('<?php echo $url2; ?>','','');mypop.focus();" class="mgbt"><img src="imagenes/duplicar_pantalla.png" title="Duplicar pesta&ntilde;a"></a>
				</td>
			</tr>
		</table>

		<?php
			if(@ $_POST['oculto'] == "")
			{
				$_POST['numres'] = 10;
				$_POST['numpos'] = 0;
				$_POST['nummul'] = 0;
			}

			if(@ $_GET['numpag'] != "")
			{
				if(@ $_POST['oculto'] != 2)
				{
					$_POST['numres'] = $_GET['limreg'];
					$_POST['numpos'] = $_GET['limreg'] * ($_GET['numpag']-1);
					$_POST['nummul'] = $_GET['numpag'] - 1;
				}
			}
			else
			{
				if(@ $_POST['nummul'] == "")
				{
					$_POST['numres'] = 10;
					$_POST['numpos'] = 0;
					$_POST['nummul'] = 0;
				}
			}
		?>

		<div id="bgventanamodalm" class="bgventanamodalm">
			<div id="ventanamodalm" class="ventanamodalm">
				<IFRAME src="" name="ventanam" marginWidth=0 marginHeight=0 frameBorder=0 id="ventanam" frameSpacing=0 style=" width:700px; height:130px; top:200; overflow:hidden;"></IFRAME>
			</div>
		</div>

		<form name="form2" method="post" action="">

			<input type="hidden" name="numres" id="numres" value="<?php echo @ $_POST['numres'];?>"/>
			<input type="hidden" name="numpos" id="numpos" value="<?php echo @ $_POST['numpos'];?>"/>
			<input type="hidden" name="nummul" id="nummul" value="<?php echo @ $_POST['nummul'];?>"/>
			<input type="hidden" name="oculto" id="oculto" value="1">
			<input type="hidden" name="cambioestado" id="cambioestado" value="<?php echo @ $_POST['cambioestado'];?>">
			<input type="hidden" name="nocambioestado" id="nocambioestado" value="<?php echo @ $_POST['nocambioestado'];?>">
			<input type="hidden" name="idestado" id="idestado" value="<?php echo @ $_POST['idestado'];?>">

			<table class="inicio">
				<tr>
					<td class="titulos" colspan="7">Buscar Usuarios</td>
					<td class="cerrar" style="width:7%" onClick="location.href='serv-principal.php'">Cerrar</td>
				</tr>

				<tr>
				    <td class="tamano01" style='width:4cm;'>C&oacute;digo de Usuario:</td>
					<td>
                        <input type="search" name="busqueda_cliente" id="busqueda_cliente" value="<?php echo @$_POST['busqueda_cliente'];?>" style='width:100%;height:30px;' onkeydown = "if (event.keyCode == 13){document.getElementById('filtro').click()}";/>
                    </td>

                    <td class="tamano01" style='width:4cm;'>:: Direcci&oacute;n:</td>
					<td>
                        <input type="search" name="busqueda_direccion" id="busqueda_direccion" value="<?php echo @$_POST['busqueda_direccion'];?>" style='width:100%;height:30px;' onkeydown = "if (event.keyCode == 13){document.getElementById('filtro').click()}";/>
                    </td>

                    <td class="tamano01" style='width:4cm;'>:: Nombre o Cedula:</td>
					<td>
                        <input type="search" name="busqueda_tercero" id="busqueda_tercero" value="<?php echo @$_POST['busqueda_tercero'];?>" style='width:100%;height:30px;' onkeydown = "if (event.keyCode == 13){document.getElementById('filtro').click()}";/>
                    </td>

                    <td style="padding-bottom:0px">
                        <em class="botonflecha" id="filtro" onClick="limbusquedas();">Buscar</em>
                    </td>
				</tr> 
			</table>

			<?php
				if ($_POST['busqueda_cliente']!='')
                {
                    $crit1 = "AND concat_ws('',C.id, C.cod_usuario) LIKE '%".$_POST['busqueda_cliente']."%' ";
                }

                if($_POST['busqueda_direccion']!='')
                {
                    $crit1 = "AND concat_ws('',D.direccion) LIKE '%".$_POST['busqueda_direccion']."%' ";
                }

				if($_POST['busqueda_tercero']!='')
                {
					$nombredividido = array();
					$nombredividido = explode(" ", $_POST['busqueda_tercero']);
					
					for ($i=0; $i < count($nombredividido); $i++) 
					{ 
						$busqueda = '';
						$busqueda = "AND concat_ws(' ', T.nombre1, T.nombre2, T.apellido1, T.apellido2, T.razonsocial, T.cedulanit) LIKE '%$nombredividido[$i]%' ";

						$crit1 = $crit1 . $busqueda;
					}
				}

				$sqlr = " SELECT C.id, C.cod_catastral, T.nombre1, T.nombre2, T.apellido1, T.apellido2, T.razonsocial, D.direccion, C.fecha_creacion, T.cedulanit, C.estado, C.cod_usuario FROM srvclientes C INNER JOIN terceros T ON C.id_tercero = T.id_tercero INNER JOIN srvdireccion_cliente D ON C.id = D.id_cliente WHERE C.cod_usuario != '' $crit1 ORDER BY C.id DESC $cond2";
				$resp = mysqli_query($linkbd,$sqlr);

				$_POST['numtop'] = mysqli_num_rows($resp);

				$nuncilumnas = ceil($_POST['numtop'] / $_POST['numres']);

				$cond2 = "";

				if (@ $_POST['numres'] != "-1")
				{
					$cond2 = " LIMIT ".$_POST['numpos'].", ".$_POST['numres']; 
				}

				$sqlr = " SELECT C.id, C.cod_catastral, T.nombre1, T.nombre2, T.apellido1, T.apellido2, T.razonsocial, D.direccion, C.fecha_creacion, T.cedulanit, C.estado, C.cod_usuario, C.id_estrato, C.id_ruta, C.codigo_ruta FROM srvclientes C INNER JOIN terceros T ON C.id_tercero = T.id_tercero INNER JOIN srvdireccion_cliente D ON C.id = D.id_cliente WHERE C.cod_usuario != '' $crit1 ORDER BY C.id DESC $cond2";
				$resp = mysqli_query($linkbd,$sqlr);

				$con = 1;
				
				$numcontrol = $_POST['nummul'] + 1;

				if(($nuncilumnas == $numcontrol) || (@$_POST['numres'] == "-1"))
				{
					$imagenforward = "<img src='imagenes/forward02.png' style='width:17px;cursor:default;'>";
					$imagensforward = "<img src='imagenes/skip_forward02.png' style='width:16px;cursor:default;' >";
				}
				else 
				{
					$imagenforward = "<img src='imagenes/forward01.png' style='width:17px;cursor:pointer;' title='Siguiente' onClick='numsiguiente()'>";
					$imagensforward = "<img src='imagenes/skip_forward01.png' style='width:16px;cursor:pointer;' title='Fin' onClick='saltocol(\"$nuncilumnas\")'>";
				}
				if((@$_POST['numpos'] == 0) || (@$_POST['numres'] == "-1"))
				{
					$imagenback = "<img src='imagenes/back02.png' style='width:17px;cursor:default;'>";
					$imagensback = "<img src='imagenes/skip_back02.png' style='width:16px;cursor:default;'>";
				}
				else
				{
					$imagenback = "<img src='imagenes/back01.png' style='width:17px;cursor:pointer;' title='Anterior' onClick='numanterior();'>";
					$imagensback = "<img src='imagenes/skip_back01.png' style='width:16px;cursor:pointer;' title='Inicio' onClick='saltocol(\"1\")'>";
				}

				$con = 1;
            ?>
                <div class="subpantalla" style="height:66%; width:99.6%; margin-top:0px; overflow-x:hidden">
					<table class='inicio'>
						<tr>
							<td colspan='10' class='titulos'>.: Resultados Busqueda: <label style='float:right;'><img src='imagenes/sema_verdeON.jpg' style='width:16px'/> Usuario activo &nbsp;&nbsp;&nbsp;<img src='imagenes/sema_rojoON.jpg' style='width:16px'/> Usuario desactivado&nbsp;&nbsp;&nbsp;</label></td>
							    <td class='submenu'>
                                    <select name='renumres' id='renumres' onChange='cambionum();' style='width:100%' class='centrarSelect'>
                                        <option class='centrarOption' value='10' <?php if (@$_POST['renumres']=='10'){echo 'selected';} ?>>10</option>
                                        <option class='centrarOption' value='20' <?php if (@$_POST['renumres']=='20'){echo 'selected';} ?>>20</option>
                                        <option class='centrarOption' value='30' <?php if (@$_POST['renumres']=='30'){echo 'selected';} ?>>30</option>
                                        <option class='centrarOption' value='50' <?php if (@$_POST['renumres']=='50'){echo 'selected';} ?>>50</option>
                                        <option class='centrarOption' value='100' <?php if (@$_POST['renumres']=='100'){echo 'selected';} ?>>100</option>
                                        <option class='centrarOption' value='-1' <?php if (@$_POST['renumres']=='-1'){echo 'selected';} ?>>Todos</option>
                                    </select>
						        </td>
                            </td>
						</tr>
						<tr>
							<td colspan='8'>Clientes Encontrados: <?php echo $_POST['numtop'] ?></td>
						</tr>

						<tr class="titulos2" style='text-align:center;'>
							<td>ID</td>
							<td>Código Usuario</td>
							<td>Código Catastral</td>
							<td>Nombre Suscriptor</td>
							<td>Documento</td>
							<td>Dirección</td>
                            <td>Estrato</td>
							<td>Ruta</td>
							<td>Código de Ruta</td>
							<td>Fecha de inscripci&oacute;n</td>
							<td style='width:5%;'>Estado</td>
						</tr>
            <?php
						$iter = 'saludo1a';
						$iter2 = 'saludo2';
						$filas = 1;
						
						while ($row = mysqli_fetch_row($resp))
						{
							$con2 = $con + $_POST['numpos'];

							if($row[10] == 'S')
							{
								$imgsem = "src='imagenes/sema_verdeON.jpg' title='Activo'";
								$coloracti = "#0F0";
							}
							else
							{
								$imgsem = "src='imagenes/sema_rojoON.jpg' title='Inactivo'";
								$coloracti = "#C00";
							}
							

							$nombreCompleto = $row[2].' '.$row[3].' '.$row[4].' '.$row[5];
                            if ($row[6]!='') {
                                $nombreCompleto = $row[6];
                            }

							$nombreCompleto = Quitar_Espacios($nombreCompleto);
							$estrato = buscarNombreEstrato($row[12]);
							$codigoUsuario = "'$row[11]'";
                    ?>
							<tr class='<?php echo $iter ?>' ondblclick="editaUsuario(<?php echo $row[0]; ?>)" style='text-transform:uppercase; <?php echo $estilo ?>; text-align:center;'>
								<td><?php echo $row[0] ?></td>
								<td><?php echo $row[11] ?></td>
								<td><?php echo $row[1] ?></td>
								<td><?php echo $nombreCompleto ?></td>
								<td><?php echo $row[9] ?></td>
								<td><?php echo $row[7] ?></td>
								<td><?php echo $estrato ?></td>
								<td><?php echo $row[13] ?></td>
								<td><?php echo $row[14] ?></td>
								<td><?php echo $row[8] ?></td>
								<td style='text-align:center;'>
                                    <img <?php echo $imgsem ?> style='width:20px'/>
                                </td>
							</tr>
                    <?php
							$con += 1;
							$aux = $iter;
							$iter = $iter2;
							$iter2 = $aux;
							$filas++;
						}
                        
						if (@$_POST['numtop'] == 0)
						{
                    ?>
							<table class='inicio'>
								<tr>
									<td class='saludo1' style='text-align:center;width:100%'><img src='imagenes\alert.png' style='width:25px'>No hay coincidencias en la búsqueda<img src='imagenes\alert.png' style='width:25px'></td>
								</tr>
							</table>
                    <?php
						}
						echo"
					</table>

					<table class='inicio'>
						<tr>
							<td style='text-align:center;'>
								<a>$imagensback</a>&nbsp;
								<a>$imagenback</a>&nbsp;&nbsp;";

				if($nuncilumnas <= 9)
				{
					$numfin = $nuncilumnas;
				}
				else
				{
					$numfin = 9;
				}
				for($xx = 1; $xx <= $numfin; $xx++)
				{
					if($numcontrol <= 9)
					{
						$numx = $xx;
					}
					else
					{
						$numx = $xx + ($numcontrol - 9);
					}

					if($numcontrol == $numx)
					{
						echo"<a onClick='saltocol(\"$numx\")'; style='color:#24D915;cursor:pointer;'> $numx </a>";
					}
					else 
					{
						echo"<a onClick='saltocol(\"$numx\")'; style='color:#000000;cursor:pointer;'> $numx </a>";
					}
				}
				echo"			&nbsp;&nbsp;<a>$imagenforward</a>
								&nbsp;<a>$imagensforward</a>
							</td>
						</tr>
					</table>";	
			?>

			<input type="hidden" name="ocules" id="ocules" value="<?php echo @ $_POST['ocules'];?>"/>
			<input type="hidden" name="actdes" id="actdes" value="<?php echo @ $_POST['actdes'];?>"/>
			<input type="hidden" name="numtop" id="numtop" value="<?php echo @ $_POST['numtop'];?>"/>

		</form>

		<div id="bgventanamodal2">
			<div id="ventanamodal2">
				<IFRAME  src="" name="buster" marginWidth=0 marginHeight=0 frameBorder=0 id="ventana2" frameSpacing=0 style="left:500px; width:900px; height:500px; top:200;"></IFRAME>
			</div>
		</div>

	</body>
</html>