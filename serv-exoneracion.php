<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=iso-8859-1");
	require "comun.inc";
	require "funciones.inc";
	require_once 'funcionesSP.inc.php';
	session_start();
	$linkbd = conectar_v7();
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es">
	<head>
		<meta http-equiv="Content-type" content="text/html; charset=iso-8859-1"/>
		<meta http-equiv="X-UA-Compatible" content="IE=9"/>
		<title>:: IDEAL 10 - Servicios P&uacute;blicos</title>
		<link href="css/css2.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/style.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
		<script type="text/javascript" src="css/programas.js"></script>
		<style>
			.onoffswitch
			{
				position: relative; width: 71px;
				-webkit-user-select:none;
				-moz-user-select:none;
				-ms-user-select: none;
			}
			.onoffswitch-checkbox {display: none;}
			.onoffswitch-label 
			{
				display: block;
				overflow: hidden;
				cursor: pointer;
				border: 2px solid #DDE6E2;
				border-radius: 20px;
			}
			.onoffswitch-inner 
			{
				display: block;
				width: 200%;
				margin-left: -100%;
				transition: margin 0.3s ease-in 0s;
			}
			.onoffswitch-inner:before, .onoffswitch-inner:after
			{
				display: block;
				float: left;
				width: 50%;
				height: 23px;
				padding: 0;
				line-height: 23px;
				font-size: 14px;
				color: white;
				font-family: Trebuchet, Arial, sans-serif;
				font-weight: bold;
				box-sizing: border-box;
			}
			.onoffswitch-inner:before
			{
				content: "SI";
				padding-left: 10px;
				background-color: #51C3E0;
				color: #FFFFFF;
			}
			.onoffswitch-inner:after 
			{
				content: "NO";
				padding-right: 10px;
				background-color: #EEEEEE; color: #999999;
				text-align: right;
			}
			.onoffswitch-switch 
			{
				display: block;
				width: 17px; 
				margin: 3px;
				background: #FFFFFF;
				position: absolute;
				top: 0; 
				bottom: 0;
				right: 44px;
				border: 2px solid #DDE6E2;
				border-radius: 20px;
				transition: all 0.3s ease-in 0s;
			}
			.onoffswitch-checkbox:checked + .onoffswitch-label .onoffswitch-inner {margin-left: 0;}
			.onoffswitch-checkbox:checked + .onoffswitch-label .onoffswitch-switch {right: 0px;}
		</style>

		<script>
			function despliegamodalm(_valor,_tip,mensa,pregunta)
			{
				document.getElementById("bgventanamodalm").style.visibility=_valor;
				if(_valor=="hidden")
				{
					document.getElementById('ventanam').src="";
				}
				else
				{
					switch(_tip)
					{
						case "1":	document.getElementById('ventanam').src="ventana-mensaje1.php?titulos="+mensa;break;
						case "2":	document.getElementById('ventanam').src="ventana-mensaje3.php?titulos="+mensa;break;
						case "3":	document.getElementById('ventanam').src="ventana-mensaje2.php?titulos="+mensa;break;
						case "4":	document.getElementById('ventanam').src="ventana-consulta1.php?titulos="+mensa+"&idresp="+pregunta;break;	
					}
				}
			}

			function despliegamodal2(_valor)
			{
				document.getElementById("bgventanamodal2").style.visibility=_valor;

				if(_valor=="hidden")
				{
					document.getElementById('ventana2').src="";
				}
				else 
				{
					document.getElementById('ventana2').src="cuentasPresupuestalesGastos-ventana.php";
				}
			}

			function despliegamodal3(_valor)
			{
				document.getElementById("bgventanamodal2").style.visibility=_valor;
				if(_valor=="hidden")
				{
					document.getElementById('ventana2').src="";
				}
				else 
				{
					document.getElementById('ventana2').src="bienesTransportables-ventana.php";
				}
			}
			
			function despliegamodal4(_valor)
			{
				document.getElementById("bgventanamodal2").style.visibility=_valor;
				if(_valor=="hidden")
				{
					document.getElementById('ventana2').src="";
				}
				else 
				{
					document.getElementById('ventana2').src="servicios-ventana.php";
				}
			}

			function despliegamodal5(_valor)
			{
				document.getElementById("bgventanamodal2").style.visibility=_valor;
				if(_valor=="hidden")
				{
					document.getElementById('ventana2').src="";
				}
				else 
				{
					document.getElementById('ventana2').src="cuin-ventana.php";
				}
			}

			function funcionmensaje()
			{
				document.location.href = "serv-exoneracion.php";
			}

			function respuestaconsulta(pregunta)
			{
				switch(pregunta)
				{
					case "1":	
						document.form2.oculto.value='2';
						document.form2.submit();
					break;
				}
			}

			function actualizar()
			{
				document.form2.submit();
			}

			function guardar()
			{
				var codigo = document.getElementById('codban').value;
				var nombre = document.getElementById('nomban').value;
				var servicio = document.getElementById('servicio').value;

				if(codigo.trim() != '' && nombre.trim() && servicio != '')
				{
					despliegamodalm('visible','4','Esta Seguro de Guardar','1');
				}
				else
				{
					despliegamodalm('visible','2','Falta informacion para crear la exoneracion');
				}
			}

			function cambiocheck()
			{
				if(document.getElementById('myonoffswitch').value=='S')
				{
					document.getElementById('myonoffswitch').value='N';
				}
				else
				{
					document.getElementById('myonoffswitch').value='S';
				}

				document.form2.submit();
			}
		</script>

		<?php titlepag();?>

	</head>

	<body>
		<IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
		<span id="todastablas2"></span>
		<table>
			<tr><script>barra_imagenes("serv");</script><?php cuadro_titulos();?></tr>

			<tr><?php menu_desplegable("serv");?></tr>

			<tr>
				<td colspan="3" class="cinta">
					<a href="serv-exoneracion.php" class="mgbt"><img src="imagenes/add.png"/></a>

					<a onclick="guardar()" class="mgbt"><img src="imagenes/guarda.png" title="Guardar"/></a>

					<a href="serv-exoneracionbuscar.php" class="mgbt"><img src="imagenes/busca.png" title="Buscar"/></a>

					<a onclick="mypop=window.open('plan-agenda.php','','');mypop.focus()" class="mgbt"><img src="imagenes/agenda1.png" title="Agenda" /></a>

					<a onclick="mypop=window.open('serv-principal.php','','');mypop.focus();" class="mgbt"><img src="imagenes/nv.png" title="Nueva ventana"></a>

					<a onclick="mypop=window.open('<?php echo $url2; ?>','','');mypop.focus();" class="mgbt"><img src="imagenes/duplicar_pantalla.png" title="Duplicar pesta&ntilde;a"></a>
				</td>
			</tr>
		</table>

		<div id="bgventanamodalm" class="bgventanamodalm">
			<div id="ventanamodalm" class="ventanamodalm">
				<IFRAME src="" name="ventanam" marginWidth=0 marginHeight=0 frameBorder=0 id="ventanam" frameSpacing=0 style=" width:700px; height:130px; top:200; overflow:hidden;"></IFRAME>
			</div>
		</div>

		<form name="form2" method="post" action="">
			<?php 
				if(@$_POST['oculto'] == "")
				{
					$_POST['onoffswitch'] = "S";

					$_POST['codban'] = selconsecutivo('srvexoneraciones','id');
				}

				if(@$_POST['bc'] == '')
				{
					$nresul = buscaNombreCuentaCCPET(@$_POST['cuenta'], '2');			

					if($nresul != '')
					{
						$_POST['ncuenta'] = $nresul;
					}
					else
					{
						$_POST['ncuenta'] = '';	
					}
				}
			?>

			<table class="inicio ancho">
				<tr>
					<td class="titulos" colspan="4">.: Crear Exoneraci&oacute;n</td>

					<td class="cerrar" style="width:7%" onClick="location.href='serv-principal.php'">Cerrar</td>
				</tr>

				<tr>
					<td class="tamano01" style="width:3cm;">C&oacute;digo:</td>

					<td style="width:15%;">
						<input type="text" name="codban" id="codban" onKeyUp="return tabular(event,this)" value="<?php echo @$_POST['codban'];?>" style="width:100%;height:30px;text-align:center" readonly/>
					</td>

					<td class="tamano01" style="width:3cm;">Nombre:</td>

					<td>
						<input type="text" name="nomban" id="nomban" value="<?php echo @$_POST['nomban'];?>" style="width:100%;height:30px;text-transform:uppercase"/>
					</td>
				</tr>

				<tr>
					<td class="tamano01" style="width: 3cm;">Cuenta Presupuestal:</td> 

					<td>
						<input type="text" id="cuenta" name="cuenta" style="width:88%" onKeyPress="javascript:return solonumeros(event)" onKeyUp="return tabular(event,this)" onBlur="actualizar()" value="<?php echo @$_POST['cuenta']?>" onClick="document.getElementById('cuenta').focus();document.getElementById('cuenta').select();">

						<a title="Cuentas presupuestales" onClick="despliegamodal2('visible');" style="cursor:pointer;"><img src="imagenes/find02.png" style="width:20px;"/></a>

						<input type="hidden" value="" name="bc" id="bc">
					</td>

					<td colspan="2">
						<input name="ncuenta" type="text" style="width:100%;" value="<?php echo @$_POST['ncuenta']?>" readonly>
					</td>
				</tr>

				<?php
				$buscarClasificador = buscaClasificadorPresupuestal($_POST['cuenta'],2);

				if($buscarClasificador == '1')
				{
				?>
					<tr>
						<td style="width:3cm;" class="saludo1">CUIN:</td> 

						<td>
							<input type="text" id="cuin" name="cuin" style="width:88%" onKeyPress="javascript:return solonumeros(event)" onKeyUp="return tabular(event,this)" onBlur="buscacta2(event,1)" value="<?php echo $_POST['cuin']?>" onClick="document.getElementById('cuin').focus();document.getElementById('cuin').select();" readonly>

							<a title="CUIN" onClick="despliegamodal5('visible');" style="cursor:pointer;"><img src="imagenes/find02.png" style="width:20px;"/></a>
						</td>

						<td colspan="4">
							<input type="text" name="ncuin" id="ncuin" style="width:100%;" value="<?php echo $_POST['ncuin']?>"  readonly>
						</td>
					</tr>
					
				<?php
				}

				if($buscarClasificador == '2')
				{
				?>
					<tr>
						<td style="width: 3cm;" class="saludo1">Bienes Transportables: </td> 

						<td>
							<input type="text" id="clasificador" name="clasificador" style="width:88%" onKeyPress="javascript:return solonumeros(event)" onKeyUp="return tabular(event,this)" onBlur="buscacta2(event,2)" value="<?php echo $_POST['clasificador']?>" onClick="document.getElementById('clasificador').focus();document.getElementById('clasificador').select();" readonly>

							<a title="Cuentas presupuestales" onClick="despliegamodal3('visible');" style="cursor:pointer;"><img src="imagenes/find02.png" style="width:20px;"/></a>
						</td>

						<td colspan="4">
							<input type="text" name="nclasificador" id="nclasificador" style="width:100%;" value="<?php echo $_POST['nclasificador']?>"  readonly>
						</td>
					</tr>
				<?php
				}

				if($buscarClasificador == '3')
				{
				?>
					<tr>
						<td style="width: 3cm;" class="saludo1">Servicios: </td> 

						<td>
							<input type="text" id="clasificadorServicios" name="clasificadorServicios" style="width:88%" onKeyPress="javascript:return solonumeros(event)" onKeyUp="return tabular(event,this)" onBlur="buscacta2(event,3)" value="<?php echo $_POST['clasificadorServicios']?>" onClick="document.getElementById('clasificadorServicios').focus();document.getElementById('clasificadorServicios').select();" readonly>

							<a title="Clasificador Servicios" onClick="despliegamodal4('visible');" style="cursor:pointer;"><img src="imagenes/find02.png" style="width:20px;"/></a>
						</td>

						<td colspan="4">
							<input type="text" name="nclasificadorServicios" id="nclasificadorServicios" style="width:100%;" value="<?php echo $_POST['nclasificadorServicios']?>"  readonly>
						</td>
					</tr>
				<?php
				}
				?>

				<tr>
					<td class="tamano01">Servicio:</td>
					<td>
						<select name="servicio" id="servicio" style="width:100%;" onchange='actualizar()'>
							<option value="-1">:: Seleccione Servicio ::</option>
							<?php
								$sqlr="SELECT * FROM srvservicios";
								$resp = mysqli_query($linkbd,$sqlr);
								while ($row =mysqli_fetch_row($resp))
								{
									if(@ $_POST['servicio']==$row[0])
									{
										echo "<option value='$row[0]' SELECTED>$row[0] - $row[1]</option>";
									}
									else{echo "<option value='$row[0]'>$row[0] - $row[1]</option>";}
								}
							?>
						</select>
					</td>

					<td class="tamano01" style="width:3cm;">Concepto:</td>

					<td>
						<select name="concecont" id="concecont" style="width:45%;" >
							<option value="-1">:: Seleccione Concepto ::</option>
							<?php
								$sqlr="SELECT * FROM conceptoscontables WHERE modulo='10' AND tipo='SE' ORDER BY codigo";
								$resp = mysqli_query($linkbd,$sqlr);
								while ($row =mysqli_fetch_row($resp))
								{
									if(@ $_POST['concecont']==$row[0])
									{
										echo "<option value='$row[0]' SELECTED>$row[0] - $row[3] - $row[1]</option>";
									}
									else{echo "<option value='$row[0]'>$row[0] - $row[3] - $row[1]</option>";}
								}
							?>
						</select>
					</td>
				</tr>

				<tr>
					<td class="tamano01">Estado:</td>

					<td>
						<div class="onoffswitch">
							<input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox" id="myonoffswitch" value="<?php echo @ $_POST['onoffswitch'];?>" <?php if(@ $_POST['onoffswitch']=='S'){echo "checked";}?> onChange="cambiocheck();"/>
							<label class="onoffswitch-label" for="myonoffswitch">
								<span class="onoffswitch-inner"></span>
								<span class="onoffswitch-switch"></span>
							</label>
						</div>
					</td>
				</tr>
			</table>

			<input type="hidden" name="oculto" id="oculto" value="1"/>

			<?php 
				if(@$_POST['oculto']=="2")
				{
					if(@$_POST['onoffswitch']!='S')
					{
						$valest='N';
					}
					else
					{
						$valest='S';
					}

					//Saber si se va agregar CUIN, Bienes Transportable o Servicios
					$cuentaClasificadora = '';
					$clasificacion = '';

					if(isset($_POST['cuin']))
					{
						$clasificacion = '1';
						$cuentaClasificadora = $_POST['cuin'];
					}
					elseif(isset($_POST['clasificador']))
					{
						$clasificacion = '2';
						$cuentaClasificadora = $_POST['clasificador'];
					}
					elseif(isset($_POST['clasificadorServicios']))
					{
						$clasificacion = '3';
						$cuentaClasificadora = $_POST['clasificadorServicios'];
					}
					else
					{
						$clasificacion = '0';
						$cuentaClasificadora = '';
					}

					$_POST['codban'] = selconsecutivo('srvexoneraciones','id');

					$sqlr = "INSERT INTO srvexoneraciones (id,nombre,concepto,estado,id_servicio,cuenta_presupuestal,clasificacion,clasificador) VALUES ('".$_POST['codban']."','".$_POST['nomban']."','".$_POST['concecont']."', '$valest', '".$_POST['servicio']."', '".$_POST['cuenta']."','$clasificacion','$cuentaClasificadora')";

					if(!mysqli_query($linkbd,$sqlr))
					{
						echo"<script>despliegamodalm('visible','2','No se pudo ejecutar la peticion');</script>";
					}
					else 
					{
						echo "<script>despliegamodalm('visible','1','Se ha almacenado con Exito');</script>";
					}
				}
			?>
		</form>

		<div id="bgventanamodal2">
			<div id="ventanamodal2">
				<IFRAME  src="" name="buster" marginWidth=0 marginHeight=0 frameBorder=0 id="ventana2" frameSpacing=0 style="left:500px; width:900px; height:500px; top:200;"></IFRAME>
			</div>
		</div>

	</body>
</html>