<?php

    header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require 'comun.inc';
	require 'funciones.inc';
	session_start();
    if(empty($_SESSION)){
        header("location: index.php");
    }
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html lang=es>
	<head>
    <meta charset="utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1"/>
		<meta name="viewport" content="user-scalable=no">
		<title>:: IDEAL 10 - Parametrización</title>
		<link href="favicon.ico" rel="shortcut icon"/>
		<link href="css/css2z.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
        <link href="css/tabs.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
        <link href="css/style.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" href="sweetalert2/dist/sweetalert2.min.css">
        <script type="text/javascript" src="css/programas.js"></script>
        <script src="sweetalert2/dist/sweetalert2.min.js"></script>
		<script src="vue/vue.min.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script src="css/calendario.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
	</head>
	<body>
		<header>
			<table>
				<tr><script>barra_imagenes("para");</script><?php cuadro_titulos();?></tr>
			</table>
		</header>

        <main id="myapp" v-cloak>
            <input type="hidden" value = "3" ref="pageType">
            <input type="hidden" value = "TT" ref="txtType">
            <input type="hidden" value = "cont-programacionTransitoEditarTt" ref="txtRoute">
            <input type="hidden" value = "14" ref="txtModule">
            <div class="loading-container" v-show="isLoading" >
                <p class="text-loading"data-loading="IDEAL 10">IDEAL 10</p>
            </div>
            <nav>
                <table>
                    <tr><?php menu_desplegable("para");?></tr>
                </table>
                <div class="bg-white group-btn p-1" id="newNavStyle">
                    <button type="button" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center" @click="window.location.href='cont-programacionTransitoCrearTt'">
                        <span>Nuevo</span>
                        <svg  xmlns="http://www.w3.org/2000/svg"  viewBox="0 -960 960 960" ><path d="M440-440H200v-80h240v-240h80v240h240v80H520v240h-80v-240Z"/></svg>
                    </button>
                    <button type="button" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center" @click="save()">
                        <span>Guardar</span>
                        <svg xmlns="http://www.w3.org/2000/svg"  viewBox="0 -960 960 960" ><path d="M840-680v480q0 33-23.5 56.5T760-120H200q-33 0-56.5-23.5T120-200v-560q0-33 23.5-56.5T200-840h480l160 160Zm-80 34L646-760H200v560h560v-446ZM480-240q50 0 85-35t35-85q0-50-35-85t-85-35q-50 0-85 35t-35 85q0 50 35 85t85 35ZM240-560h360v-160H240v160Zm-40-86v446-560 114Z"/></svg>
                    </button>
                    <button type="button" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center" @click="window.location.href='cont-programacionTransitoBuscarTt'">
                        <span>Buscar</span>
                        <svg class="fill-black group-hover:fill-white w-5 h-5" xmlns="http://www.w3.org/2000/svg"  viewBox="0 -960 960 960" ><path d="M784-120 532-372q-30 24-69 38t-83 14q-109 0-184.5-75.5T120-580q0-109 75.5-184.5T380-840q109 0 184.5 75.5T640-580q0 44-14 83t-38 69l252 252-56 56ZM380-400q75 0 127.5-52.5T560-580q0-75-52.5-127.5T380-760q-75 0-127.5 52.5T200-580q0 75 52.5 127.5T380-400Z"/></svg>
                    </button>
                    <button type="button" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center" @click="mypop=window.open('para-principal.php','',''); mypop.focus();">
                        <span>Nueva ventana</span>
                        <svg class="fill-black group-hover:fill-white w-5 h-5" xmlns="http://www.w3.org/2000/svg"  viewBox="0 -960 960 960" ><path d="M320-120v-80h80v-80H160q-33 0-56.5-23.5T80-360v-400q0-33 23.5-56.5T160-840h640q33 0 56.5 23.5T880-760v400q0 33-23.5 56.5T800-280H560v80h80v80H320ZM160-360h640v-400H160v400Zm0 0v-400 400Z"/></svg>
                    </button>
                    <button type="button" class="btn btn-success d-flex justify-between align-items-center" @click="window.location.href='cont-programacionTransitoBuscarTt'">
                        <span>Atrás</span>
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960"><path d="M760-200v-160q0-50-35-85t-85-35H273l144 144-57 56-240-240 240-240 57 56-144 144h367q83 0 141.5 58.5T840-360v160h-80Z"/></svg>
                    </button>
                </div>
            </nav>
            <section class="bg-white">
                <div>
                    <h2 class="titulos m-0">Transito y transporte - editar conceptos contables de tramites</h2>
                    <p class="mb-0 ms-2 ">Todos los campos con (<span class="text-danger fw-bolder">*</span>) son obligatorios</p>
                    <div class="d-flex w-75">
                        <div class="form-control w-25">
                            <label class="form-label" for="">Código:</label>
                            <div class="d-flex">
                                <button type="button" class="btn btn-primary" @click="nextItem('prev')"><</button>
                                <input type="text"  style="text-align:center;" v-model="txtConsecutivo" @change="nextItem()">
                                <button type="button" class="btn btn-primary" @click="nextItem('next')">></button>
                            </div>
                        </div>
                        <div class="form-control">
                            <label class="form-label" for="">Nombre <span class="text-danger fw-bolder">*</span>:</label>
                            <input type="text" v-model="txtNombre">
                        </div>
                    </div>
                </div>
                <div>
                    <h2 class="titulos m-0">Detalle concepto</h2>
                    <div class="w-75">
                        <div class="d-flex">
                            <div class="form-control w-25">
                                <label class="form-label">Fecha:</label>
                                <input type="date" v-model="txtFecha">
                            </div>
                            <div class="form-control w-25">
                                <label class="form-label">Tipo de cuenta:</label>
                                <select v-model="selectTipo">
                                    <option value="1">Débito</option>
                                    <option value="2">Crédito</option>
                                </select>
                            </div>
                            <div class="form-control">
                                <label class="form-label" for="">Cuenta contable:</label>
                                <div class="d-flex">
                                    <input type="text"  class="colordobleclik w-25" @dblclick="isModal=true;" v-model="objCuenta.codigo" @change="search('cod_cuenta')">
                                    <input type="text" disabled readonly v-model="objCuenta.nombre">
                                </div>
                            </div>
                            <div class="form-control w-50">
                                <label class="form-label" for="">Centro de costos:</label>
                                <div class="d-flex">
                                    <select v-model="selectCentro">
                                        <option v-for="(data,index) in arrCentros" :value="data.codigo">{{data.codigo+"-"+data.nombre}}</option>
                                    </select>
                                    <button type="button" class="btn btn-primary" @click="add()" >Agregar</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="overflow-auto" style="height:50vh">
                        <table  class="table fw-normal">
                            <thead>
                                <tr>
                                    <th>Item</th>
                                    <th>Fecha</th>
                                    <th>Centro de costos</th>
                                    <th>Cuenta</th>
                                    <th>Débito</th>
                                    <th>Crédito</th>
                                    <th>Opciones</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr v-for="(data,index) in arrDetalles" :key="index">
                                    <td>{{index+1}}</td>
                                    <td>{{data.fecha}}</td>
                                    <td>{{data.centro.codigo+"-"+data.centro.nombre}}</td>
                                    <td>{{data.cuenta.codigo+"-"+data.cuenta.nombre}}</td>
                                    <td>{{data.debito}}</td>
                                    <td>{{data.credito}}</td>
                                    <td><button type="button" class="btn btn-sm btn-danger" @click="del(index)">Eliminar</button></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </section>
            <!-- MODALES -->
            <div v-show="isModal" class="modal">
                <div class="modal-dialog modal-lg" >
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title">Buscar cuentas</h5>
                            <button type="button" @click="isModal=false;" class="btn btn-close"><div></div><div></div></button>
                        </div>
                        <div class="modal-body">
                            <div class="d-flex flex-column">
                                <div class="form-control m-0 mb-3">
                                    <input type="search" placeholder="Buscar" v-model="txtSearch" @keyup="search('modal')" id="labelInputName">
                                </div>
                                <div class="form-control m-0 mb-3">
                                    <label class="form-label" for="labelInputName">Resultados: <span class="fw-bold">{{txtResultados}}</span></label>
                                </div>
                            </div>
                            <div class="overflow-auto max-vh-50 overflow-x-hidden " >
                                <table class="table table-hover fw-normal">
                                    <thead>
                                        <tr>
                                            <th>Item</th>
                                            <th>Cuenta</th>
                                            <th>Nombre</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr v-for="(data,index) in arrCuentasCopy" @dblclick="selectItem(data)" :key="index">
                                            <td>{{index+1}}</td>
                                            <td>{{data.codigo}}</td>
                                            <td>{{data.nombre}}</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </main>
		<script src="node_modules/read-excel-file/bundle/read-excel-file.min.js"></script>
		<script src="node_modules/xlsx/dist/xlsx.full.min.js"></script>
        <script src="Librerias/vue/vue.min.js"></script>
		<script src="Librerias/vue/axios.min.js"></script>
		<script type="module" src="contabilidad_vue/parametros_contables/js/functions_tesoreria.js?<?= date('d_m_Y_h_i_s');?>"></script>
	</body>
</html>
