<?php

    header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require 'comun.inc';
	require 'funciones.inc';
	session_start();
    if(empty($_SESSION)){
        header("location: index.php");
    }
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html lang=es>
	<head>
    <meta charset="utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1"/>
		<meta name="viewport" content="user-scalable=no">
		<title>:: IDEAL 10 - Presupuesto</title>
		<link href="favicon.ico" rel="shortcut icon"/>
		<link href="css/css2z.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
        <link href="css/tabs.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
        <link href="css/style.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" href="sweetalert2/dist/sweetalert2.min.css">
        <script type="text/javascript" src="css/programas.js"></script>
        <script src="sweetalert2/dist/sweetalert2.min.js"></script>
		<script src="vue/vue.min.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script src="css/calendario.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
        <style>[v-cloak]{display : none;}</style>
	</head>
	<body>
		<header>
			<table>
				<tr><script>barra_imagenes("ccpet");</script><?php cuadro_titulos();?></tr>
			</table>
		</header>

        <section id="myapp" v-cloak>
            <input type="hidden" value = "3" ref="pageType">
            <div class="loading-container" v-show="isLoading" >
                <p class="text-loading"data-loading="IDEAL 10">IDEAL 10</p>
            </div>
            <nav>
                <table>
                    <tr><?php menu_desplegable("ccpet");?></tr>
                </table>
                <div class="bg-white group-btn p-1" id="newNavStyle">
                    <button type="button" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center" @click="location.href='plan-creaSolicitudCdp'">
                        <span>Nuevo</span>
                        <svg viewBox="0 -960 960 960" ><path d="M440-440H200v-80h240v-240h80v240h240v80H520v240h-80v-240Z"/></svg>
                    </button>
                    <button type="button" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center" @click="location.href='plan-buscaSolicitudCdp'">
                        <span>Buscar</span>
                        <svg class="fill-black group-hover:fill-white w-5 h-5" viewBox="0 -960 960 960" ><path d="M784-120 532-372q-30 24-69 38t-83 14q-109 0-184.5-75.5T120-580q0-109 75.5-184.5T380-840q109 0 184.5 75.5T640-580q0 44-14 83t-38 69l252 252-56 56ZM380-400q75 0 127.5-52.5T560-580q0-75-52.5-127.5T380-760q-75 0-127.5 52.5T200-580q0 75 52.5 127.5T380-400Z"/></svg>
                    </button>
                    <button type="button" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center" @click="mypop=window.open('ccp-principal.php','',''); mypop.focus();">
                        <span>Nueva ventana</span>
                        <svg class="fill-black group-hover:fill-white w-5 h-5" viewBox="0 -960 960 960" ><path d="M320-120v-80h80v-80H160q-33 0-56.5-23.5T80-360v-400q0-33 23.5-56.5T160-840h640q33 0 56.5 23.5T880-760v400q0 33-23.5 56.5T800-280H560v80h80v80H320ZM160-360h640v-400H160v400Zm0 0v-400 400Z"/></svg>
                    </button>
                    <button type="button" class="btn btn-white btn-white-hover d-flex justify-between align-items-center" @click="pdf();">
                        <span>Exportar PDF</span>
                        <svg viewBox="0 0 512 512"><path d="M64 464l48 0 0 48-48 0c-35.3 0-64-28.7-64-64L0 64C0 28.7 28.7 0 64 0L229.5 0c17 0 33.3 6.7 45.3 18.7l90.5 90.5c12 12 18.7 28.3 18.7 45.3L384 304l-48 0 0-144-80 0c-17.7 0-32-14.3-32-32l0-80L64 48c-8.8 0-16 7.2-16 16l0 384c0 8.8 7.2 16 16 16zM176 352l32 0c30.9 0 56 25.1 56 56s-25.1 56-56 56l-16 0 0 32c0 8.8-7.2 16-16 16s-16-7.2-16-16l0-48 0-80c0-8.8 7.2-16 16-16zm32 80c13.3 0 24-10.7 24-24s-10.7-24-24-24l-16 0 0 48 16 0zm96-80l32 0c26.5 0 48 21.5 48 48l0 64c0 26.5-21.5 48-48 48l-32 0c-8.8 0-16-7.2-16-16l0-128c0-8.8 7.2-16 16-16zm32 128c8.8 0 16-7.2 16-16l0-64c0-8.8-7.2-16-16-16l-16 0 0 96 16 0zm80-112c0-8.8 7.2-16 16-16l48 0c8.8 0 16 7.2 16 16s-7.2 16-16 16l-32 0 0 32 32 0c8.8 0 16 7.2 16 16s-7.2 16-16 16l-32 0 0 48c0 8.8-7.2 16-16 16s-16-7.2-16-16l0-64 0-64z"/></svg>
                    </button>
                </div>
            </nav>
            <article>
                <div class="bg-white">
                    <div>
                        <h2 class="titulos m-0">Visualizar solicitud de CDP</h2>
                        
                        <div class="d-flex">
                            <div class="form-control w-25">
                                <label class="form-label m-0" for="">Consecutivo:</label>
                                <div class="d-flex">
                                    <button type="button" class="btn btn-primary" @click="nextItem('prev')"><</button>
                                    <input type="text"  style="text-align:center;" v-model="consecutivo" readonly>
                                    <button type="button" class="btn btn-primary" @click="nextItem('next')">></button>
                                </div>
                            </div>
                            <div class="form-control w-25">
                                <label class="form-label m-0" for="">Fecha solicitud<span class="text-danger fw-bolder">*</span></label>
                                <input type="date" class="text-center" v-model="fechaSolicitud" readonly>
                            </div>

                            <div class="form-control w-25">
                                <label class="form-label m-0" for="">Fecha de vencimiento</label>
                                <input type="date" class="text-center" v-model="fechaVencimiento" readonly>
                            </div>

                            <div class="form-control w-25">
                                <label class="form-label m-0" for="">Código PAA<span class="text-danger fw-bolder">*</span></label>
                                <input type="text" class="text-center" v-model="codPaa" readonly>
                            </div>

                            <div class="form-control w-50">
                                <label class="form-label m-0" for="">Modalidad de contratación</label>
                                <input type="text" v-model="modalidad" readonly>
                            </div>

                            <div class="form-control justify-between">
                                <label class="form-label m-0" for=""></label>
                                <input type="text" v-model="nomPaa" readonly>
                            </div>
                        </div>

                        <div class="d-flex">
                            <div class="form-control w-25">
                                <label class="form-label m-0" for="">Area solicitante<span class="text-danger fw-bolder">*</span></label>
                                <input type="text" v-model="nomArea" readonly>
                            </div>

                            <div class="form-control w-25">
                                <label class="form-label m-0" for="">Tipo de presupuesto<span class="text-danger fw-bolder">*</span></label>
                                <select v-model="tipoPresupuesto" disabled>
                                    <option value="">Seleccione</option>
                                    <option value="1">Funcionamiento</option>
                                    <option value="2">Inversión</option>
                                    <option value="3">Funcionamiento/Inversión</option>
                                </select>
                            </div>

                            <div class="form-control w-25" v-show="tipoPresupuesto==2">
                                <label class="form-label m-0" for="">Sector<span class="text-danger fw-bolder">*</span></label>
                                <input type="text" class="text-center colordobleclik" v-model="codSector" @dblclick="modalSectores=true;" readonly>
                            </div>

                            <div class="form-control justify-between w-25" v-show="tipoPresupuesto==2">
                                <label class="form-label m-0" for=""></label>
                                <input type="text" v-model="nomSector">
                            </div>


                            <div class="form-control w-25">
                                <label class="form-label m-0" for="">Contrato / Act. Adm<span class="text-danger fw-bolder">*</span></label>
                                <select v-model="tipoDocumento" disabled>
                                    <option value="">Seleccione</option>
                                    <option value="1">Tipo de contrato</option>
                                    <option value="2">Acto administrativo</option>
                                </select>
                            </div>

                            <div class="form-control w-25" v-show="tipoDocumento==1">
                                <label class="form-label m-0" for="">Tipo de contrato<span class="text-danger fw-bolder">*</span></label>
                                <select v-model="tipoContrato" disabled>
                                    <option value="">Seleccione</option>
                                    <option value="1">Obra</option>
                                    <option value="2">Consultora servicio</option>
                                    <option value="3">Suministro y/o compraventa</option>
                                </select>
                            </div>

                            <div class="form-control w-25" v-show="tipoDocumento==2">
                                <label class="form-label m-0" for="">Número acto<span class="text-danger fw-bolder">*</span></label>
                                <input type="text" class="text-center" v-model="numeroActo" readonly>
                            </div>

                            <div class="form-control w-25" v-show="tipoDocumento==2">
                                <label class="form-label m-0" for="">Fecha acto<span class="text-danger fw-bolder">*</span></label>
                                <input type="date" class="text-center" v-model="fechaActo" readonly>
                            </div>
                        </div>

                        <div class="d-flex">
                            <div class="form-control">
                                <label class="form-label m-0" for="">Objeto<span class="text-danger fw-bolder">*</span></label>
                                <input type="text" v-model="objeto" readonly>
                            </div>
                        </div>
                    </div>         
                    
                    <div>
                        <h5 class="titulos m-0">Cuenta CCPET y clasificadores complementarios</h5>
                    </div>

                    <div class="table-responsive" style="height: 30%;">
                        <table class="table table-hover fw-normal">
                            <thead>
                                <tr class="text-center">
                                    <th>Tipo de presupuesto</th>
                                    <th>Dependendencia</th>
                                    <th>Medio pago</th>
                                    <th>Vig de gasto</th>
                                    <th>Cuenta</th>
                                    <th>Fuente</th>
                                    <th>CPC</th>
                                    <th>Indicador</th>
                                    <th>BPIN</th>
                                    <th>Detalle sectorial</th>
                                    <th>Sectorial gasto</th>
                                    <th>Politica</th>
                                    <th>Valor</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr tr v-for="(det,index) in arrayDetalles" :key="index" class="text-center">
                                    <td>{{(det.tipoPresupuesto == 1) ? "Funcionamiento" : (det.tipoPresupuesto == 2) ? "Inversión" : "Fun/inv"}}</td>
                                    <td>{{det.nameDependencia}}</td>
                                    <td>{{det.medioPago}}</td>
                                    <td>{{det.nameVigGasto}}</td>
                                    <td>{{det.cuenta}}</td>
                                    <td>{{det.codFuente}}</td>
                                    <td>{{det.cpc}}</td>
                                    <td>{{det.codIndicador}}</td>
                                    <td>{{det.bpin}}</td>
                                    <td>{{det.codSectorial}}</td>
                                    <td>{{det.codSectorialGasto}}</td>
                                    <td>{{det.codPoliticaPublica}}</td>
                                    <td class="text-right">{{viewFormatNumber(det.valor)}}</td>
                                </tr>
                            </tbody>
                            <tfoot>
                                <tr class="fw-bold">
                                    <td class="text-right" colspan="12">Total:</td>
                                    <td class="text-right">{{viewFormatNumber(valorTotalDet)}}</td>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>

                <!-- modales -->
            </article>
        </section>

        <script src="Librerias/vue/vue.min.js"></script>
		<script src="Librerias/vue/axios.min.js"></script>
		<script type="module" src="planeacion/solicitudCdp/js/cdp_functions.js"></script>

	</body>
</html>