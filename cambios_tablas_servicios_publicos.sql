
-- Este cambio se realiza para poder imprimir la facturacion en orden de zona y lado y que los clientes que no tienen asignado ninguna de estas dos van de ultimas
ALTER TABLE srvclientes CHANGE id_zona id_zona INT(11) NULL, CHANGE id_lado id_lado INT(5) NULL;

-- Creación de tipos de novedades 
CREATE TABLE srvtipo_novedades ( id INT NOT NULL AUTO_INCREMENT , nombre VARCHAR(50) NOT NULL , estado ENUM('S','N') NOT NULL , PRIMARY KEY (id)) ENGINE = MyISAM;


CREATE TABLE srvmedidores ( id INT NOT NULL AUTO_INCREMENT , serial VARCHAR(15) NOT NULL , fabricante VARCHAR(20) NOT NULL , marca VARCHAR(15) NOT NULL , fecha_activacion DATE NOT NULL , estado ENUM('S','N') NOT NULL , PRIMARY KEY (id))) ENGINE = MyISAM;

-- Historial novedades clientes 
CREATE TABLE srv_historial_clientes ( id INT NOT NULL AUTO_INCREMENT , id_cliente INT(11) NOT NULL , id_tercero INT(11) NOT NULL , cod_usuario VARCHAR(20) NOT NULL, direccion VARCHAR(200) NOT NULL , id_estrato INT(11) NOT NULL , id_zona INT(11) NOT NULL , id_lado INT(11) NOT NULL , id_ruta INT(11) NOT NULL , fecha_cambio DATE NOT NULL , usuario VARCHAR(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL , PRIMARY KEY (id)) ENGINE = MyISAM;


-- Cambio en tabla de srvcambiotercero agrega columna usuario para registrar quien hizo el cambio
ALTER TABLE srvcambio_tercero ADD usuario VARCHAR(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL AFTER novedad;

ALTER TABLE srv_acuerdo_cab ADD medio_pago ENUM('caja','banco') NOT NULL AFTER valor_liquidado, ADD numero_cuenta VARCHAR(25) NOT NULL AFTER medio_pago;


CREATE TABLE srv_acuerdo_cab ( id INT NOT NULL AUTO_INCREMENT , numero_acuerdo INT(11) NOT NULL , id_cliente INT(11) NOT NULL , cod_usuario VARCHAR(20) NOT NULL , nombre_suscriptor VARCHAR(200) NOT NULL , documento VARCHAR(15) NOT NULL , fecha_acuerdo DATE NOT NULL , numero_factura INT(11) NOT NULL , valor_factura DOUBLE NOT NULL , valor_abono DOUBLE NOT NULL , valor_acuerdo DOUBLE NOT NULL , numero_cuotas INT(11) NOT NULL , valor_cuota DOUBLE NOT NULL , cuotas_liquidadas INT(11) NOT NULL , valor_liquidado DOUBLE NOT NULL , estado_acuerdo ENUM('ACTIVO','ANULADO') NOT NULL , PRIMARY KEY (id)) ENGINE = MyISAM;


CREATE TABLE srv_acuerdo_det ( id INT NOT NULL AUTO_INCREMENT , numero_acuerdo INT(11) NOT NULL , cuota_liquidada INT(11) NOT NULL , valor_cuota DOUBLE NOT NULL , fecha DATE NOT NULL , estado ENUM('S','N') NOT NULL , PRIMARY KEY (id)) ENGINE = MyISAM;

ALTER TABLE srvparametros ADD porcentaje_acuerdo_pago DOUBLE NOT NULL AFTER facturas_vencidas;

CREATE TABLE srv_recaudo_factura ( id INT(11) NOT NULL AUTO_INCREMENT , codigo_recaudo INT(11) NOT NULL , id_cliente INT(11) NOT NULL , cod_usuario VARCHAR(20) NOT NULL , suscriptor VARCHAR(100) NOT NULL , documento VARCHAR(20) NOT NULL , fecha_recaudo DATE NOT NULL , medio_pago ENUM('caja','banco') NOT NULL , numero_cuenta VARCHAR(25) NOT NULL , concepto VARCHAR(200) NOT NULL , valor_pago DOUBLE NOT NULL , usuario VARCHAR(100) NOT NULL , estado VARCHAR(12) NOT NULL , PRIMARY KEY (id)) ENGINE = MyISAM;