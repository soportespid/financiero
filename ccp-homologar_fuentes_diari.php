<?php
    header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");

	require 'comun.inc';
	require 'funciones.inc';

	session_start();
    date_default_timezone_set("America/Bogota");

?>
<!DOCTYPE >
<html lang="es">
	<head>
		<meta charset="utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1"/>
		<meta name="viewport" content="user-scalable=no">
		<title>:: Ideal - Informes</title>
		<link href="favicon.ico" rel="shortcut icon"/>

		<link href="css/css2.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/css2z.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
        <link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
        <link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
        <script type="text/javascript" src="css/programas.js"></script>
        <script type="text/javascript" src="css/calendario.js"></script>
        <script type="text/javascript" src="JQuery/jquery-2.1.4.min.js"></script>
        <link href="css/style.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
    </head>
    <body>
        <IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
        <span id="todastablas2"></span>
        <table>
            <tr><script>barra_imagenes("info");</script><?php cuadro_titulos();?></tr>
            <tr><?php menu_desplegable("info");?></tr>
        	<tr>
          		<td colspan="3" class="cinta">
					<a><img src="imagenes/add.png" title="Nuevo" onClick="location.href='ccp-homologar_fuentes_diari.php'" class="mgbt"/></a>
					<a class="mgbt"><img src="imagenes/guardad.png"/></a>
					<a><img src="imagenes/buscad.png" title="Buscar" class="mgbt"/></a>
					<a href="#" onClick="mypop=window.open('info-principal.php','','');mypop.focus();" class="mgbt"><img src="imagenes/nv.png" title="Nueva Ventana"></a>
					<img src="imagenes/iratras.png" title="Atr&aacute;s" onClick="location.href='ccp-informesdiari.php'" class="mgbt"/>
				</td>
        	</tr>
		</table>
		<div class="h-100" >
			<div id="myapp" v-cloak>
                <h2 class="titulos m-0">Homologar fuentes DIARI</h2>
                <div class="d-flex">
                    <div class="form-control">
                        <label class="form-label">Buscar fuente:</label>
                        <input v-on:keyup="buscarFuente" v-model="buscar_fuente" type="text" placeholder="Buscar Fuente.">
                    </div>
                    <div class="form-control">
                        <label class="form-label">Vigencia:</label>
                        <select v-model="vigencia"  v-on:Change="cargarParametros">
                            <option v-for="year in years" :value="year[0]">{{ year[0] }}</option>
                        </select>
                    </div>
                </div>
                <button v-show="puedeGuardar" class="btn btn-success ms-2 mt-1 mb-1" @click = "guardarFuentesf04">Guardar Cambios</button>
                <div class="overflow-y" style="height:60%">
                    <table>
                        <thead>
                            <tr>
                                <td class='titulos' width="20%"  style="font: 160% sans-serif; border-radius: 5px 0px 0px 0px;">Fuente CUIPO</td>
                                <td class='titulos' style="font: 160% sans-serif;">
                                    Nombre
                                </td>
                                <td class='titulos' width = "20%" style="text-align: center;">
                                    Fuente Diari
                                </td>

                                <td class='titulos' width = "1%"></td>
                            </tr>
                        </thead>
                        <tbody v-if="show_resultados">

                            <tr v-for="(result, index) in results" :style="estaEnArray(result) == true ? myStyle : ''" v-bind:class="index % 2 ? 'contenidonew00' : 'contenidonew01'" >
                                <td width="20%" style="font: 160% sans-serif; padding: 5px;">{{ result[0].split('-')[0] }}</td>
                                <td colspan="2" style="font: 160% sans-serif;">{{ result[1] }}</td>

                                <td width="20%" style="text-align: center;">
                                    <div v-on:dblclick="agregarFuenteDiari(result[0],result[1])" title="Doble click para desplegar ventana emergente" style='text-rendering: optimizeLegibility; text-align:center; background-color: #dbeafe; cursor: pointer !important; border-radius: 5px; height: 5vh; display: flex; flex-direction: column; justify-content: center'>
                                        {{ selFuenteHomologar && selFuenteHomologar[result[0]] }}
                                    </div>
                                </td>

                            </tr>
                        </tbody>
                        <tbody v-else>
                            <tr>
                                <td width="20%"style="font: 120% sans-serif; padding-left:10px; text-align:center;" colspan="3">Sin resultados</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
				<div v-show="showModal_fuentes_diari">
                    <transition name="modal">
                        <div class="modal-mask">
                            <div class="modal-wrapper">
                                <div class="modal-dialog modal-lg" style = "max-width: 1200px !important;" role="document">
                                    <div class="modal-content"  style = "width: 100% !important;" scrollable>
                                        <div class="modal-header">
                                            <h5 class="modal-title">{{ rubroVentanaEmergente }}</h5>
                                            <button type="button" @click="showModal_fuentes_diari = false" class="btn btn-close p-2"><div></div><div></div></button>
                                        </div>
                                        <div class="modal-body">
                                            <div style="margin: 2px 0 0 0;">
                                                <div class="row" style="margin: 4px; border-radius:4px; background-color: #F6F6F6; ">
                                                    <div class="col-md-2" style="padding: 12px 0px 0px 30px; font: 140% sans-serif;">
                                                        <label for="">Fuente DIARI:</label>
                                                    </div>

                                                    <div class="col-md-6 col-md-offset-6" style="padding: 4px">
                                                        <input type="text" class="form-control" placeholder="Buscar por nombre" style="font: sans-serif; " v-on:keyup="searchMonitorFuenteDiari" v-model="searchFuenteDiari.keywordFuenteDiari">
                                                    </div>
                                                </div>
                                                <table>
                                                    <thead>
                                                        <tr>
                                                            <td width="20%" class='titulos' style="font: 120% sans-serif; padding-left:10px; border-radius: 5px 0 0 0;">Fuente</td>
                                                            <td class='titulos' style="font: 120% sans-serif; ">Nombre</td>

                                                        </tr>
                                                    </thead>
                                                </table>
                                            </div>
                                            <div style="margin: 2px 0 0 0; border-radius: 0 0 0 6px; height: 200px; overflow: scroll; overflow-x: hidden; background: white; ">
                                                <table class='inicio inicio--no-shadow'>
                                                    <tbody>
                                                        <?php
                                                            $co ='zebra1';
                                                            $co2='zebra2';
                                                        ?>
                                                        <tr v-for="fuentediari in fuentesdiari" v-on:click="seleccionarFuenteDiari(fuentediari)" class='<?php echo $co; ?>' style='text-rendering: optimizeLegibility; cursor: pointer !important; style=\"cursor: hand\"'>
                                                            <td width="20%" style="font: 120% sans-serif; padding-left:10px">{{ fuentediari[1] }}</td>
                                                            <td  style="font: 120% sans-serif; padding-left:10px">{{ fuentediari[2] }}</td>
                                                            <?php
                                                            $aux=$co;
                                                            $co=$co2;
                                                            $co2=$aux;
                                                            ?>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <div class="modal-footer d-flex justify-end p-2">
                                            <button type="button" class="btn btn-secondary" @click="showModal_fuentes_diari = false">Cerrar</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </transition>
                </div>



                <div id="cargando" v-show="loading" class="loading">
                    <span>Cargando...</span>
                </div>

			</div>
		</div>

        <script type="module" src="./presupuesto_ccpet/reportes/ccp-homologar_fuentes_diari.js"></script>
        <script src="Librerias/vue/axios.min.js"></script>
        <script src="sweetalert2/dist/sweetalert2.min.js"></script>
        <link rel="stylesheet" href="sweetalert2/dist/sweetalert2.min.css">
        <script src="https://unpkg.com/vue@3/dist/vue.global.js"></script>
	</body>
</html>
