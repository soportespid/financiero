<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");

	require "comun.inc";
	require "funciones.inc";

	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");

	session_start();
	date_default_timezone_set("America/Bogota");
	ini_set('max_execution_time', 3600);
?>
<!DOCTYPE > 
<html lang="es">
	<head>
		<meta charset="utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1"/>
		<meta name="viewport" content="user-scalable=no">
		<title>:: IDEAL 10 - Contabilidad</title>
		<link href="favicon.ico" rel="shortcut icon"/>
		<link href="css/css2.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<script src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
	</head>
	<body>
		<IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
		<span id="todastablas2"></span>
		<table>
			<tr><script>barra_imagenes("cont");</script><?php cuadro_titulos();?></tr>	 
			<tr><?php menu_desplegable("cont");?></tr>
			<tr>
				<td colspan="3" class="cinta">
                    <a class="mgbt"><img src="imagenes/add2.png"/></a>
                    <a class="mgbt"><img src="imagenes/guardad.png"/></a>
                    <a class="mgbt"><img src="imagenes/buscad.png"/></a>
                    <a onClick="mypop=window.open('plan-agenda.php','','');mypop.focus()" class="mgbt"><img src="imagenes/agenda1.png" title="Agenda"/></a>
                    <a class="mgbt" onClick="<?php echo paginasnuevas("cont");?>"><img src="imagenes/nv.png" title="Nueva ventana"></a>
                    <a onClick="mypop=window.open('<?php echo $url2; ?>','','');mypop.focus();" class="tooltip bottom mgbt"><img src="imagenes/duplicar_pantalla.png" title="Duplicar pesta&ntilde;a"></a>
                    <a href="teso-gestionexogena.php" class="mgbt"><img src="imagenes/iratras.png" title="Atr&aacute;s"></a>
                </td>
			</tr>
		</table>
		<form name="form2" method="post" action="">
			<table class="inicio">
				<tr>
					<td class="titulos" colspan="2">.: Clasificaci&oacute;n contable</td>
					<td class="cerrar" style="width:7%" onClick="location.href='cont-principal.php'">Cerrar</td>
				</tr>
				<tr>
				<td class='saludo1' width='70%'>
					<ol id="lista2">
						<li onClick="location.href='cont-contexoformatosbuscar.php'" style="cursor:pointer">Formatos</li>
						<li onClick="location.href='cont-contexoconceptosdianbuscar.php'" style="cursor:pointer">Conceptos DIAN</li>
						<li onClick="location.href='cont-codigosint1001buscar.php'" style="cursor:pointer">Columnas 1001-N</li>
						<li onClick="location.href='cont-exogenacolumnas.php'" style="cursor:pointer">Columnas fijas - Exogena</li>
						<li onClick="location.href='cont-contexoconceptosbuscar.php'" style="cursor:pointer">Marcar cuentas</li>
						<li onClick="location.href='cont-generareportebuscar.php'" style="cursor:pointer">Generar Reportes</li>
					</ol>
				</td> 
				<td colspan="2" rowspan="1" style="background:url(imagenes/dian.png); background-repeat:no-repeat; background-position:center; background-size: 100% 100%"></td>
				</tr>
			</table>
		</form>
	</body>
</html>
