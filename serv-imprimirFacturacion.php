<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require "comun.inc";
	require "funciones.inc";
	ini_set('max_execution_time',99999999);
	session_start();
	$linkbd=conectar_v7();
	$linkbd -> set_charset("utf8");
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es">
	<head>
		<meta http-equiv="Content-type" content="text/html; charset=iso-8859-1"/>
		<meta http-equiv="X-UA-Compatible" content="IE=9"/>
		<title>:: IDEAL 10 - Servicios P&uacute;blicos</title>
		<link href="css/css2.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/cssSP.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/style.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
		<script type="text/javascript" src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script type="text/javascript" src="css/calendario.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<style>
			.orderby {text-align-last: center;}
			option {font-size:15px;}
		</style>
		<script>
			<?php
				$sqlfactura = "SELECT nombre FROM srvarchivospdf WHERE id='1' ";
				$resfactura = mysqli_query($linkbd,$sqlfactura);
				$rowfactura = mysqli_fetch_row($resfactura);
				echo "
				function pdf()
				{
					document.form2.action='".$rowfactura[0]."';
					document.form2.target='_BLANK';
					document.form2.submit(); 
					document.form2.action='';
					document.form2.target='';
				}";
			?>
			function despliegamodalm(_valor,_tip,mensa,pregunta)
			{
				document.getElementById("bgventanamodalm").style.visibility=_valor;
				if(_valor=="hidden"){document.getElementById('ventanam').src="";}
				else
				{
					switch(_tip)
					{
						case "1":	document.getElementById('ventanam').src="ventana-mensaje1.php?titulos="+mensa;break;
						case "2":	document.getElementById('ventanam').src="ventana-mensaje3.php?titulos="+mensa;break;
						case "3":	document.getElementById('ventanam').src="ventana-mensaje2.php?titulos="+mensa;break;
						case "4":	document.getElementById('ventanam').src="ventana-consulta1.php?titulos="+mensa+"&idresp="+pregunta;
									break;
					}
				}
			}
			function funcionmensaje()
			{
				document.location.href = "serv-imprimirFacturacion.php";
			}
			function generarfacturas()
			{
				var facturaInicial = parseInt(document.getElementById('facturaInicial').value,10);
				var facturaFinal = parseInt(document.getElementById('facturaFinal').value,10);
				if((facturaInicial <= facturaFinal) && (facturaInicial != '') && (facturaFinal != ''))
				{
					document.form2.oculto.value='2';
					document.form2.submit();
				}
				else
				{
					despliegamodalm('visible','2','Las facturas a imprimir no pertenecen a este corte.');
				}
			}
			function actualizar()
			{
				document.form2.submit();
			}
			function callprogress(vValor)
			{
				document.getElementById("getprogress").innerHTML = vValor;
				document.getElementById("getProgressBarFill").innerHTML = '<div class="ProgressBarFill" style="width: '+vValor+'%;"></div>';
				document.getElementById("titulog1").style.display='block';
				document.getElementById("progreso").style.display='block';
				document.getElementById("getProgressBarFill").style.display='block';
			}
			
			function atras() {
				var link = document.referrer;

				location.href = link;
			} 
			
		</script><?php echo $_SESSION['FACTURASP'];?>
		<?php titlepag();?>
	</head>
	<body>
		<IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
		<span id="todastablas2"></span>
		<table>
			<tr><script>barra_imagenes("serv");</script><?php cuadro_titulos();?></tr>
			<tr><?php menu_desplegable("serv");?></tr>
			<tr>
				<td colspan="3" class="cinta">
					<img src="imagenes/add.png" title="Nuevo" onClick="location.href='serv-imprimirFacturacion.php'" class="mgbt"/>
					<img src="imagenes/guardad.png" title="Guardar" class="mgbt"/>
					<img src="imagenes/buscad.png" title="Buscar" class="mgbt"/>
					<img src="imagenes/nv.png" title="Nueva Ventana" onClick="<?php echo paginasnuevas("serv");?>" class="mgbt">
					<img src='imagenes/iratras.png' title="Men&uacute; Nomina" onclick="atras();" class='mgbt'>
				</td>
			</tr>
		</table>
		<div id="bgventanamodalm" class="bgventanamodalm">
			<div id="ventanamodalm" class="ventanamodalm">
				<IFRAME src="" name="ventanam" marginWidth=0 marginHeight=0 frameBorder=0 id="ventanam" frameSpacing=0 style=" width:700px; height:130px; top:200; overflow:hidden;"></IFRAME>
			</div>
		</div>
		<form name="form2" method="post" action="">
			<table class="inicio" align="center">
				<tr>
					<td class="titulos" colspan="11">.: Facturaci&oacute;n Servicios</td>
					<td  class="cerrar" style="width: 5%;"><a href="serv-principal.php">Cerrar</a></td>
				</tr>
				<tr>
					<td class="tamano01" style='width:3cm;'>Generar Corte:</td>
					<td style='width:10%;'>
						<select name="idCorte" id="idCorte" class="centrarSelect" style="width:98%;height:30px;align-content:center;" onchange="actualizar()">
							<option class='aumentarTamaño' value="-1">Seleccione Corte</option>
							<?php
								$sql = "SET lc_time_names = 'es_ES'";
								mysqli_query($linkbd,$sql);
								$sqlCorte= "SELECT numero_corte,fecha_inicial,fecha_final,fecha_limite_pago,fecha_impresion,servicio, UPPER(MONTHNAME(fecha_inicial)), UPPER(MONTHNAME(fecha_final)), YEAR(fecha_inicial), YEAR(fecha_final) FROM srvcortes WHERE numero_corte > 0 ORDER BY numero_corte DESC";
								$respCorte = mysqli_query($linkbd,$sqlCorte);
								while ($rowCorte =mysqli_fetch_row($respCorte))
								{
									$sqlServicio = "SELECT nombre FROM srvservicios WHERE id = '$rowCorte[5]' ";
									$respServicio = mysqli_query($linkbd,$sqlServicio);
									$rowServicio = mysqli_fetch_row($respServicio);
									$sqlCorteDetalles = "SELECT MIN(numero_facturacion),MAX(numero_facturacion) FROM srvcortes_detalle WHERE id_corte = '$rowCorte[0]' AND estado_pago <> 'N'";
									$respCorteDetalles = mysqli_query($linkbd,$sqlCorteDetalles);
									$rowCorteDetalles = mysqli_fetch_row($respCorteDetalles);
									if ($rowServicio[0] == 0) 
									{
										$rowServicio[0] = 'TODOS';
									}
									if($_POST['idCorte'] == $rowCorte[0])
									{
										echo "<option value='$rowCorte[0]' SELECTED>$rowCorte[0]: $rowCorte[6] $rowCorte[8] - $rowCorte[7] $rowCorte[9]</option>";
										if($rowCorte[0] != $_POST['idCorteaux'])
										{
											$_POST['servicios'] = $rowServicio[0];
											$_POST['facturaInicial'] = $rowCorteDetalles[0];
											$_POST['facturaFinal'] = $rowCorteDetalles[1];
											$_POST['fechalimite'] = $rowCorte[3];
											$_POST['idCorteaux'] = $rowCorte[0];
										}
										
									}
									else {echo "<option value='$rowCorte[0]'>$rowCorte[0]: $rowCorte[6] $rowCorte[8] - $rowCorte[7] $rowCorte[9]</option>no";}
								}
							?>
						</select>
						<input type="hidden" name="idCorteaux" id="idCorteaux" value="<?php echo @$_POST['idCorteaux']; ?>"
					</td>
					<td class="tamano01">Servicios a Imprimir:</td>
					<td>
						<input type="text" name="servicios" style="text-align:center;height:30px;" value="<?php echo @$_POST['servicios']; ?>" readonly/>
					</td>
					<td class="tamano01">Factura Inicial:</td>
					<td>
						<input type="text" name="facturaInicial" id="facturaInicial" style="text-align:center;height:30px;" value="<?php echo @$_POST['facturaInicial']; ?>" />
					</td>
					<td class="tamano01">Factura Final:</td>
					<td>
						<input type="text" name="facturaFinal" id="facturaFinal" style="text-align:center;height:30px;" value="<?php echo @$_POST['facturaFinal']; ?>"/>
					</td>
					<td style="padding-bottom:0px"><em class="botonflecha" onClick="generarfacturas();">Buscar</em></td>
				</tr>
				<input type="hidden" name="fechalimite" id="fechalimite" style="text-align:center;height:30px;" value="<?php echo @$_POST['fechalimite']; ?>"/>
				<?php 
				if(@ $_POST['oculto']=="2")
				{
					echo"
					<tr>
						<td class='tamano01'>Total Facturas:</td>
						<td  >
							<input type='text' name='totalfacturas' id='totalfacturas' style='text-align:center;height:30px;width:98%;' value='".$_POST['totalfacturas']."' readonly/>
						</td>
						<td>
							<div id='titulog1' style='display:none; float:left'></div>
							<div id='progreso' class='ProgressBar' style='display:none; float:left'>
								<div class='ProgressBarText'><span id='getprogress'></span>&nbsp;% </div>
								<div id='getProgressBarFill'></div>
							</div>
						</td>
						<td ><em class='botonflecha' onClick='pdf();' >Imprimir</em></td>
					</tr>";
					$sqlcorte = "SELECT fecha_impresion,fecha_inicial,fecha_final,fecha_corte,version_tarifa FROM srvcortes WHERE numero_corte = '$_POST[idCorte]'";
					$rescorte = mysqli_query($linkbd,$sqlcorte);
					$rowcorte = mysqli_fetch_row($rescorte);
					echo "
					<input type='hidden' name='fechaimpresion' id='fechaimpresion' value='$rowcorte[0]'/>
					<input type='hidden' name='fechainicial' id='fechainicial' value='$rowcorte[1]'/>
					<input type='hidden' name='fechafinal' id='fechafinal' value='$rowcorte[2]'/>
					<input type='hidden' name='fechasuspension' id='fechasuspension' value='$rowcorte[3]'/>
					";
					$c=$totalfact=$bancliente=0;
					$totalcli=$_POST['facturaFinal']-$_POST['facturaInicial']+1;
					for($x = $_POST['facturaInicial']; $x <= $_POST['facturaFinal'];$x++)
					{
						
						$totalfact++;
						$c+=1;
						$sqlser = "SELECT id FROM srvservicios ORDER BY id";
						$respser = mysqli_query($linkbd,$sqlser);
						while ($rowser = mysqli_fetch_row($respser))
						{
							$sqldet = "SELECT id_tipo_cobro,credito,debito,id_cliente,tipo_movimiento FROM srvdetalles_facturacion WHERE numero_facturacion='$x' AND corte = '$_POST[idCorte]' AND id_servicio='$rowser[0]' AND tipo_movimiento = '101' ORDER BY id ASC";
							$resdet = mysqli_query($linkbd,$sqldet);
							while ($rowdet = mysqli_fetch_row($resdet))
							{
								$dettotal=$rowdet[1]-$rowdet[2];
								echo"<input type='hidden' name='servicio".$x."[".$rowser[0]."][".$rowdet[0]."_".$rowdet[4]."]' value='$dettotal'/>";
								if($bancliente != $rowdet[3])
								{
									$bancliente = $rowdet[3];
									$sqlmedidor = "SELECT id_medidor FROM srvasignacion_servicio WHERE id_clientes = '$rowdet[3]' AND id_servicio = '1' AND estado = 'S'";
									$resmedidor = mysqli_query($linkbd,$sqlmedidor);
									$rowmedidor = mysqli_fetch_row($resmedidor);
									echo "<input type='hidden' name='medidorfactura[$x]' id='medidorfactura[$x]' value='$rowmedidor[0]'/>";
									
									$sqlusoacueducto = "SELECT cargo_fijo, consumo, id_estrato FROM srvasignacion_servicio WHERE id_clientes = '$rowdet[3]' AND id_servicio = '1' AND estado = 'S'";
									$resusoacueducto = mysqli_query($linkbd,$sqlusoacueducto);
									$rowusoacueducto = mysqli_fetch_row($resusoacueducto);
									echo "<input type='hidden' name='usoacueductocf[$x]' id='usoacueductocf[$x]' value='$rowusoacueducto[0]'/>";
									echo "<input type='hidden' name='usoacueductocs[$x]' id='usoacueductocs[$x]' value='$rowusoacueducto[1]'/>";
									
									$sqlusoalcantarillado = "SELECT cargo_fijo, consumo, id_estrato FROM srvasignacion_servicio WHERE id_clientes = '$rowdet[3]' AND id_servicio = '2' AND estado = 'S'";
									$resusoalcantarillado = mysqli_query($linkbd,$sqlusoalcantarillado);
									$rowusoalcantarillado = mysqli_fetch_row($resusoalcantarillado);
									echo "<input type='hidden' name='usoalcantarilladocf[$x]' id='usoalcantarilladocf[$x]' value='$rowusoalcantarillado[0]'/>";
									echo "<input type='hidden' name='usoalcantarilladocs[$x]' id='usoalcantarilladocs[$x]' value='$rowusoalcantarillado[1]'/>";
									
									$sqlusaseo = "SELECT cargo_fijo, consumo, id_estrato FROM srvasignacion_servicio WHERE id_clientes = '$rowdet[3]' AND id_servicio = '3' AND estado = 'S'";
									$resusoaseo = mysqli_query($linkbd,$sqlusaseo);
									$rowusoaseo = mysqli_fetch_row($resusoaseo);
									echo "<input type='hidden' name='usoaseocf[$x]' id='usoaseocf[$x]' value='$rowusoaseo[0]'/>";
									echo "<input type='hidden' name='usoaseocs[$x]' id='usoaseocs[$x]' value='$rowusoaseo[1]'/>";
									
									$sqlcliente = "SELECT cod_usuario, id_estrato, id_tercero, id_barrio, cod_catastral, estado, id_ruta FROM srvclientes WHERE id='$rowdet[3]'";
									$rescliente = mysqli_query($linkbd,$sqlcliente);
									$rowcliente = mysqli_fetch_row($rescliente);
									echo "<input type='hidden' name='codigocliente[$x]' id='codigocliente[$x]' value='$rowcliente[0]'/>";
									
									echo "<input type='hidden' name='codigocatastral[$x]' id='codigocatastral[$x]' value='$rowcliente[4]'/>";
									
									echo "<input type='hidden' name='estadocliente[$x]' id=estadocliente[$x]' value='$rowcliente[5]'/>";
									
									echo "<input type='hidden' name='codruta[$x]' id=codruta[$x]' value='$rowcliente[6]'/>";
									
									$sqlestrato = "SELECT descripcion,tipo FROM srvestratos WHERE id='$rowcliente[1]'";
									$resestrato = mysqli_query($linkbd,$sqlestrato);
									$rowestrato = mysqli_fetch_row($resestrato);
									echo "<input type='hidden' name='estratocliente[$x]' id='estratocliente[$x]' value='$rowestrato[0]'/>";
									
									echo "<input type='hidden' name='estratoclientenum[$x]' id='estratoclientenum[$x]' value='$rowcliente[1]'/>";
									
									$sqlusosuelo = "SELECT nombre FROM srvtipo_uso WHERE id='$rowestrato[1]'";
									$resusosuelo = mysqli_query($linkbd,$sqlusosuelo);
									$rowusosuelo = mysqli_fetch_row($resusosuelo);
									echo "<input type='hidden' name='usosuelocliente[$x]' id='usosuelocliente[$x]' value='$rowusosuelo[0]'/>";
									
									$sqltercero = "SELECT nombre1,nombre2,apellido1,apellido2,razonsocial FROM terceros WHERE id_tercero='$rowcliente[2]'";
									$restercero = mysqli_query($linkbd,$sqltercero);
									$rowtercero = mysqli_fetch_row($restercero);
									if($rowtercero[4] != '') {echo "<input type='hidden' name='tercerocliente[$x]' id='tercerocliente[$x]' value='$rowtercero[4]'/>";}
									else {echo "<input type='hidden' name='tercerocliente[$x]' id='tercerocliente' value='".$rowtercero[0]." ".$rowtercero[1]." ".$rowtercero[2]." ".$rowtercero[3]."'/>";}
									
									$sqldireccion = "SELECT direccion FROM srvdireccion_cliente WHERE id='$rowdet[3]'";
									$resdireccion = mysqli_query($linkbd,$sqldireccion);
									$rowdireccion = mysqli_fetch_row($resdireccion);
									echo "<input type='hidden' name='direccioncliente[$x]' id='direccioncliente[$x]' value='$rowdireccion[0]'/>";
									
									$sqlbarrio = "SELECT nombre FROM srvbarrios WHERE id='$rowcliente[3]'";
									$resbarrio = mysqli_query($linkbd,$sqlbarrio);
									$rowbarrio = mysqli_fetch_row($resbarrio);
									echo "<input type='hidden' name='barriocliente[$x]' id='barriocliente[$x]' value='$rowbarrio[0]'/>";
									
									$sqlconsumo = "SELECT GROUP_CONCAT(consumo ORDER BY corte DESC SEPARATOR '<->') FROM srvlectura WHERE id_servicio = '1' AND id_cliente = '$rowdet[3]' AND corte <= ".$_POST['idCorte'];
									$resconsumo = mysqli_query($linkbd,$sqlconsumo);
									$rowconsumo = mysqli_fetch_row($resconsumo);
									echo "<input type='hidden' name='consumocliente[$x]' id='consumocliente[$x]' value='$rowconsumo[0]'/>";

									if ($rowconsumo[0] == 0) {
										$sqlconsumo = "SELECT GROUP_CONCAT(consumo ORDER BY corte DESC SEPARATOR '<->') FROM srvlectura WHERE id_servicio = '2' AND id_cliente = '$rowdet[3]' AND corte <= ".$_POST['idCorte'];
										$resconsumo = mysqli_query($linkbd,$sqlconsumo);
										$rowconsumo = mysqli_fetch_row($resconsumo);
										echo "<input type='hidden' name='consumocliente[$x]' id='consumocliente[$x]' value='$rowconsumo[0]'/>";
									}

									$sqlobservacion = "SELECT codigo_observacion FROM srv_asigna_novedades WHERE id_cliente = '$rowdet[3]' AND corte = '$_POST[idCorte]' AND estado = 'S' ";
									$resobservacion = mysqli_query($linkbd,$sqlobservacion);
									$rowobservacion = mysqli_fetch_row($resobservacion);
									
									$sqlNovedadCab = "SELECT descripcion, afecta_facturacion FROM srv_tipo_observacion WHERE codigo_observacion = '$rowobservacion[0]'";
									$rowNovedadCab = mysqli_fetch_row(mysqli_query($linkbd, $sqlNovedadCab));

									echo "<input type='hidden' name='observacion[$x]' id='observacion[$x]' value='$rowNovedadCab[0]'/>";

									if ($rowNovedadCab[1] == 'S') {

										$sqlNovedadDet = "SELECT cargo_fijo, consumo FROM srv_observacion_det WHERE codigo_observacion = '$rowobservacion[0]' AND id_servicio = 1 AND estado = 'S' ";
										$rowNovedadDet = mysqli_fetch_row(mysqli_query($linkbd, $sqlNovedadDet));

										echo "<input type='hidden' name='novedadAcueductoCF[$x]' id='novedadAcueductoCF[$x]' value='$rowNovedadDet[0]'/>";
										echo "<input type='hidden' name='novedadAcueductoCS[$x]' id='novedadAcueductoCS[$x]' value='$rowNovedadDet[1]'/>";

										$sqlNovedadDet = "SELECT cargo_fijo, consumo FROM srv_observacion_det WHERE codigo_observacion = '$rowobservacion[0]' AND id_servicio = 2 AND estado = 'S' ";
										$rowNovedadDet = mysqli_fetch_row(mysqli_query($linkbd, $sqlNovedadDet));

										echo "<input type='hidden' name='novedadAlcantarilladoCF[$x]' id='novedadAlcantarilladoCF[$x]' value='$rowNovedadDet[0]'/>";
										echo "<input type='hidden' name='novedadAlcantarilladoCS[$x]' id='novedadAlcantarilladoCS[$x]' value='$rowNovedadDet[1]'/>";

										$sqlNovedadDet = "SELECT cargo_fijo, consumo FROM srv_observacion_det WHERE codigo_observacion = '$rowobservacion[0]' AND id_servicio = 3 AND estado = 'S' ";
										$rowNovedadDet = mysqli_fetch_row(mysqli_query($linkbd, $sqlNovedadDet));

										echo "<input type='hidden' name='novedadAseoCF[$x]' id='novedadAseoCF[$x]' value='$rowNovedadDet[0]'/>";
										echo "<input type='hidden' name='novedadAseoCS[$x]' id='novedadAseoCS[$x]' value='$rowNovedadDet[1]'/>";
									}
									else {
										echo "<input type='hidden' name='novedadAcueductoCF[$x]' id='novedadAcueductoCF[$x]' value='S'/>";
										echo "<input type='hidden' name='novedadAcueductoCS[$x]' id='novedadAcueductoCS[$x]' value='S'/>";

										echo "<input type='hidden' name='novedadAlcantarilladoCF[$x]' id='novedadAlcantarilladoCF[$x]' value='S'/>";
										echo "<input type='hidden' name='novedadAlcantarilladoCS[$x]' id='novedadAlcantarilladoCS[$x]' value='S'/>";

										echo "<input type='hidden' name='novedadAseoCF[$x]' id='novedadAseoCF[$x]' value='S'/>";
										echo "<input type='hidden' name='novedadAseoCS[$x]' id='novedadAseoCS[$x]' value='S'/>";
									}

									$sqlAcuerdoPago = "SELECT valor_cuota, valor_acuerdo, valor_liquidado FROM srv_acuerdo_cab WHERE id_cliente = '$rowdet[3]' AND estado_acuerdo = 'Activo'";
									$resAcuerdoPago = mysqli_query($linkbd,$sqlAcuerdoPago);
									$rowAcuerdoPago = mysqli_fetch_row($resAcuerdoPago);

									if($rowAcuerdoPago[0] != '')
									{
										$saldoPendiente = $rowAcuerdoPago[1] - $rowAcuerdoPago[2];
									}
									else
									{
										$saldoPendiente = 0;
									}

									echo "<input type='hidden' name='saldoPendiente[$x]' id='saldoPendiente[$x]' value='$saldoPendiente'/>";

									$sqlLectura = "SELECT GROUP_CONCAT(lectura_medidor ORDER BY corte DESC SEPARATOR '<->') FROM srvlectura WHERE id_servicio = '1' AND id_cliente = '$rowdet[3]' AND corte <= ".$_POST['idCorte'];
									$resLectura = mysqli_query($linkbd,$sqlLectura);
									$rowLectura = mysqli_fetch_row($resLectura);
									echo "<input type='hidden' name='lecturaMedidor[$x]' id='lecturaMedidor[$x]' value='$rowLectura[0]'/>";
									
									$sqlpordescf = "SELECT subsidio FROM srvcargo_fijo WHERE id_estrato = '$rowusoacueducto[2]' AND id_servicio = '1' AND version = $rowcorte[4]";
									$respordescf = mysqli_query($linkbd,$sqlpordescf);
									$rowpordescf = mysqli_fetch_row($respordescf);
									echo "<input type='hidden' name='porsubacueductofijo[$x]' id='porsubacueductofijo[$x]' value='$rowpordescf[0]'/>";
									
									$sqlpordescc = "SELECT subsidio, costo_unidad FROM srvtarifas WHERE id_estrato = '$rowusoacueducto[2]' AND id_servicio = '1' AND version = $rowcorte[4]";
									$respordescc = mysqli_query($linkbd,$sqlpordescc);
									$rowpordescc = mysqli_fetch_row($respordescc);
									echo "<input type='hidden' name='porsubacueducto[$x]' id='porsubacueducto[$x]' value='$rowpordescc[0]'/>";
									echo "<input type='hidden' name='costosubacueducto[$x]' id='costosubacueducto[$x]' value='$rowpordescc[1]'/>";
									
									$sqlpordescf = "SELECT subsidio FROM srvcargo_fijo WHERE id_estrato = '$rowusoacueducto[2]' AND id_servicio = '2' AND version = $rowcorte[4]";
									$respordescf = mysqli_query($linkbd,$sqlpordescf);
									$rowpordescf = mysqli_fetch_row($respordescf);
									echo "<input type='hidden' name='porsubalcantarilladofijo[$x]' id='porsubalcantarilladofijo[$x]' value='$rowpordescf[0]'/>";
									
									$sqlpordescc = "SELECT subsidio, costo_unidad, contribucion FROM srvtarifas WHERE id_estrato = '$rowusoacueducto[2]' AND id_servicio = '2' AND version = $rowcorte[4]";
									$respordescc = mysqli_query($linkbd,$sqlpordescc);
									$rowpordescc = mysqli_fetch_row($respordescc);
									echo "<input type='hidden' name='porsubalcantarillado[$x]' id='porsubalcantarillado[$x]' value='$rowpordescc[0]'/>";
									echo "<input type='hidden' name='costosubalcantarillado[$x]' id='costosubalcantarillado[$x]' value='$rowpordescc[1]'/>";
									echo "<input type='hidden' name='contribucionalcantarillado[$x]' id='contribucionalcantarillado[$x]' value='$rowpordescc[2]'/>";
									
									$sqlpordesaseo = "SELECT subsidio, costo_unidad FROM srvcostos_estandar WHERE id_estrato = '$rowusoaseo[2]' AND id_servicio = '3' AND version = $rowcorte[4]";
									$respordesaseo = mysqli_query($linkbd,$sqlpordesaseo);
									$rowpordesaseo = mysqli_fetch_row($respordesaseo);
									echo "<input type='hidden' name='porsubaseo[$x]' id='porsubaseo[$x]' value='$rowpordesaseo[0]'/>";
									echo "<input type='hidden' name='costosubaseo[$x]' id='costosubaseo[$x]' value='$rowpordesaseo[1]'/>";

									$contadorFacturasVencidas = 0;
									$sqlFacturasVencidas = "SELECT estado_pago FROM srvcortes_detalle WHERE id_cliente = $rowdet[3] AND id_corte < '$_POST[idCorte]' ORDER BY id_corte DESC ";
									$resFacturasVencidas = mysqli_query($linkbd, $sqlFacturasVencidas);
									while ($rowFacturasVencidas = mysqli_fetch_row($resFacturasVencidas)){

										if ($rowFacturasVencidas[0] == 'V') {
											$contadorFacturasVencidas += 1;
										}
										else {
											break;
										}
									}
									echo "<input type='hidden' name='facVencidas[$x]' id='facVencidas[$x]' value='$contadorFacturasVencidas'/>";
								}
							}
						}
						$porcentaje = $c * 100 / $totalcli; 
						echo"
						<script>
							progres='".round($porcentaje)."';callprogress(progres);
							document.getElementById('totalfacturas').value=$totalfact;
						</script>";
						flush();
						ob_flush();
						usleep(5);
					}
				}
			?>
			</table>
			
			<input type="hidden" name="oculto" id="oculto" value="1"/>
			
		</form>
		<div id="bgventanamodal2">
			<div id="ventanamodal2">
				<IFRAME  src="" name="buster" marginWidth=0 marginHeight=0 frameBorder=0 id="ventana2" frameSpacing=0 style="left:500px; width:900px; height:500px; top:200;"></IFRAME>
			</div>
		</div>
	</body>
</html>