<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require "comun.inc";
	require "funciones.inc";
	session_start();
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html lang="es">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="ie=edge"/>
		<meta name="viewport" content="user-scalable=no">
		<title>IDEAL 10 - Tesoreria</title>
		<link href="favicon.ico" rel="shortcut icon"/>
		<link href="css/css2z.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet"/>
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet"/>
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet"/>
		<link href="sweetalert2/dist/sweetalert2.min.css" rel="stylesheet">
		<script src="sweetalert2/dist/sweetalert2.min.js"></script>
		<script type="text/javascript" src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script type="text/javascript" src="css/calendario.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script>
			function buscacta(e){
				if (document.form2.cuenta.value!=""){
					document.form2.bc.value='1';
					document.form2.submit();
				}
			}
			function validar(){
				document.form2.submit();
			}
			function buscatercero(e){
				if (document.form2.tercero.value != ""){
					document.form2.bc.value='1';
					document.form2.submit();
				}
			}
			function pdf(){
				document.form2.action="pdfreporegresos.php";
				document.form2.target="_BLANK";
				document.form2.submit(); 
				document.form2.action="";
				document.form2.target="";
			}
			function visualizarNotas(id){
				let fecha1 = document.getElementById('fc_1198971545').value;
				let fecha2 = document.getElementById('fc_1198971546').value;
				window.open("teso-editanotasbancarias_new.php?id=" + id + "&fechab1=" + fecha1 + "&fechab2=" + fecha2);
			}
			
		</script>
	</head>
	<body>
		<IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
		<span id="todastablas2"></span>
		<table>
			<tr><script>barra_imagenes("teso");</script><?php cuadro_titulos();?></tr>	 
			<tr><?php menu_desplegable("teso");?></tr>
			<tr>
				<td colspan="3" class="cinta">
					<img src="imagenes/add2.png" title="Nuevo" class="mgbt1"/>
					<img src="imagenes/guardad.png" title="Guardar" class="mgbt1"/>
					<img src="imagenes/agenda1.png" title="Agenda" onClick="mypop=window.open('plan-agenda.php','','');mypop.focus()" class="mgbt"/>
					<img src="imagenes/nv.png" title="Nueva Ventana" onClick="mypop=window.open('teso-principal.php','','');mypop.focus();" class="mgbt">
					<a href="teso-reportenotasbancariasexcell.php?notaban=<?php echo $_POST['nbancarias']?>&fecha1=<?php echo $_POST['fecha1']?>&fecha2=<?php echo $_POST['fecha2']?>" target="_blank"><img src="imagenes/excel.png"  class="mgbt" alt="excel"></a>
					<a href="teso-informestesoreria.php" class="mgbt"><img src="imagenes/iratras.png" title="Atr&aacute;s"></a>
				</td>
			</tr>
		</table>
		<form name="form2" method="post" action="teso-reportenotasbancarias.php">
			<?php
				if($_POST['oculto'] == ''){
					$_POST['fecha1'] = date("01/m/Y");
					$_POST['fecha2'] = date("d/m/Y");
				}
				if($_POST['bc']=='1'){
					$nresul = buscatercero($_POST['tercero']);
					if($nresul!=''){
						$_POST['ntercero'] = $nresul;
					}else{
						$_POST['ntercero'] = "";
					}
				}
			?>
			<table class="inicio ancho">
				<tr >
					<td class="titulos" colspan="8">:. Buscar Pagos</td>
					<td class="cerrar" style="width:7%" onClick="location.href='teso-principal.php'">Cerrar</td>
				</tr>
				<tr>
					<td class="saludo1">Notas Bancarias:</td>
					<td style="width:25%;">
						<select name="nbancarias" style="width:100%;" onKeyUp='return tabular(event,this)'>
							<option value="">Todas las Nota Bancarias</option>	  
							<?php
								$sqlr = "SELECT * FROM tesogastosbancarios WHERE estado='S' ORDER BY codigo";
								$resp = mysqli_query($linkbd,$sqlr);
								while ($row = mysqli_fetch_row($resp)){
									if($row[0] == $_POST['nbancarias']){
										$_POST['ntipocomp'] = $row[1];
										echo "<option value='$row[0]' SELECTED >$row[0] - $row[1]</option>";
									}else{
										echo "<option value='$row[0]'>$row[0] - $row[1]</option>";
									} 
								}
							?>
						</select>
						<input type="hidden" name="clasecomp" value="<?php echo $_POST['clasecomp']?>">
					</td>
					<td class="saludo1">Fecha Inicial:</td>
					<td>
						<input type="hidden" value="<?php echo $ $vigusu ?>" name="vigencias">
						<input type="text" name="fecha1" id="fc_1198971545" title="DD/MM/YYYY" value="<?php echo $_POST['fecha1']; ?>" onKeyUp="return tabular(event,this) " onKeyDown="mascara(this,'/',patron,true)"  maxlength="10" onDblClick="displayCalendarFor('fc_1198971545');" class="colordobleclik" autocomplete="off" onchange=""></td>
					<td class="saludo1">Fecha Final: </td>
					<td ><input  type="text" name="fecha2" id="fc_1198971546" title="DD/MM/YYYY" value="<?php echo $_POST['fecha2']; ?>" onKeyUp="return tabular(event,this) " onKeyDown="mascara(this,'/',patron,true)"  maxlength="10" onDblClick="displayCalendarFor('fc_1198971546');" class="colordobleclik" autocomplete="off" onchange=""></td>
					<td colspan="2" style="padding-bottom:0px"><em class="botonflechaverde" onClick="document.form2.submit();">Generar</em></td>
				</tr>
			</table>  
			<input type="hidden" name="oculto" id="oculto" value="1">
			<?php 
				if($_POST['bc'] == '1'){
					$nresul = buscatercero($_POST['tercero']);
					if($nresul != ''){
						$_POST['ntercero'] = $nresul;
						echo"
						<script>
							document.form2.fecha.focus();
							document.form2.fecha.select();
						</script>";
					}else{
						$_POST['ntercero'] = "";
						echo"
						<script>
							Swal.fire({
								icon: 'error',
								title: 'Error!',
								text: 'Tercero Incorrecto',
								confirmButtonText: 'Continuar',
								confirmButtonColor: '#FF121A',
								timer: 2500
							});
							document.form2.tercero.focus();
						</script>";
					}
				}
				if($_POST['oculto']){
					preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST['fecha1'],$fecha);
					$fechaf=$fecha[3]."-".$fecha[2]."-".$fecha[1];
					preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST['fecha2'],$fecha);
					$fechaf2=$fecha[3]."-".$fecha[2]."-".$fecha[1];	
					$crit1 = " ";
					$crit2 = " ";
					$crit20 = "";
					$crit21 = "";
					if ($_POST['nbancarias'] != ""){
						$crit1 = "AND T2.gastoban LIKE '%".$_POST['nbancarias']."%' ";
					}
					if($_POST['fecha1'] != "" && $_POST['fecha2']!=""){
						$crit2 = "AND T1.fecha BETWEEN '$fechaf' AND '$fechaf2'";
					}
					//sacar el consecutivo 
					$sqlr = "SELECT * FROM tesonotasbancarias_cab AS T1 INNER JOIN tesonotasbancarias_det AS T2 ON T1.id_comp = T2.id_notabancab WHERE T1.estado <> '' $crit1 $crit2 ORDER BY T1.id_comp DESC";
					$resp = mysqli_query($linkbd,$sqlr);
					$ntr = mysqli_num_rows($resp);
					$con = 1;
					echo "
					<table class='tablamv3' style='height:66%; width:99.7%;'>
						<thead>
							<tr>
								<th class='titulos' style='text-align:left;'>.: Resultados Busqueda: $ntr</th>
							</tr>
							<tr>
								<th class='titulos2' style='width:5%;'>No Nota</th>
								<th class='titulos2' style='width:8%;'>Fecha</th>
								<th class='titulos2' style='width:20%;'>Concepto Nota Bancaria</th>
								<th class='titulos2' style='width:8%;'>Valor</th>
								<th class='titulos2' style='width:10%;'>Cuenta bancaria</th>
								<th class='titulos2' style='width:30%;'>Nombre cuenta</th>
								<th class='titulos2' style='width:5%;'>Concepto</th>
								<th class='titulos2' style='width:8%;'>Tipo Nota</th>
								<th class='titulos2'>Estado</th>
							</tr>
						</thead>
						<tbody style='height:83%;'>";	
					$iter = 'saludo1a';
					$iter2 = 'saludo2';
					while ($row = mysqli_fetch_row($resp)){
						if($row[4]=='S'){
							$imgsem = "src='imagenes/sema_verdeON.jpg' title='Activo'";
						}
						if($row[4]=='N'){
							$imgsem = "src='imagenes/sema_rojoON.jpg' title='Inactivo'";
						}
						$sqlrd = "SELECT sum(valor),ncuentaban FROM tesonotasbancarias_det WHERE id_notabancab='$row[1]'";
						$resn = mysqli_query($linkbd,$sqlrd);
						$rn = mysqli_fetch_row($resn);
						$sqlr1 = "SELECT * FROM tesobancosctas tb1 INNER JOIN cuentasnicsp tb2 ON tb1.cuenta = tb2.cuenta WHERE tb1.ncuentaban = '$rn[1]'";
						$res1 = mysqli_query($linkbd,$sqlr1);
						$rn1 = mysqli_fetch_assoc($res1);
						$sqltn = "SELECT nombre, tipo FROM tesogastosbancarios WHERE codigo = '$row[14]'";
						$restn = mysqli_query($linkbd,$sqltn);
						$rowtn = mysqli_fetch_row($restn);
						if($rowtn[1] == 'G'){
							$tipnota = "GASTO";
						}else{
							$tipnota = "INGRESO";
						}
						echo "
							<input type='hidden' name='idnota[]' value='$row[1]'>
							<input type='hidden' name='fecha[]' value='$row[2]'>
							<input type='hidden' name='concepto[]' value='$row[5]'>
							<input type='hidden' name='valor[]' value='$rn[0]'>
							<input type='hidden' name='banco[]' value='".$rn1["ncuentaban"]."'>
							<input type='hidden' name='nbanco[]' value='".$rn1["nombre"]."'>
							<tr class='$iter' ondblclick=\"visualizarNotas('$row[1]')\">
								<td style='width:5%;'>$row[1]</td>
								<td style='width:8%;text-align:center;'>$row[2]</td>
								<td style='width:20.2%;'>$row[5]</td>
								<td style='width:8.3%;text-align:right;'>".number_format($rn[0],0)."</td>
								<td style='width:10%;text-align:center;'>".$rn1["ncuentaban"]."</td>
								<td style='width:30.5%;'>".$rn1["nombre"]."</td>
								<td style='width:5%;text-align:center;' title='$rowtn[0]'>$row[14]</td>
								<td style='width:8.3%;text-align:center;'>$tipnota</td>
								<td style='text-align:center;'><img $imgsem style='width:18px' ></td>
							</tr>";
						$con+=1;
						$aux=$iter;
						$iter=$iter2;
						$iter2=$aux;
					}
					echo"
						</tbody>
					</table>";
				}
			?>
		</form>
	</body>
</html>