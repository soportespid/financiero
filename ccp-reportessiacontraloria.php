<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");

	require "comun.inc";
	require "funciones.inc";
	require "conversor.php";
	require "validaciones.inc";

	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");

	session_start();
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html lang="es">
	<head>
		<meta charset="utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1"/>
		<meta name="viewport" content="user-scalable=no">
		<title>:: IDEAL 10 - Presupuesto</title>
		<link href="favicon.ico" rel="shortcut icon"/>
		<link href="css/css2.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css">
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css">
		<script type="text/javascript" src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script type='text/javascript' src='JQuery/jquery-2.1.4.min.js?<?php echo date('d_m_Y_h_i_s');?>'></script>
	</head>
	<body>
		<IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
		<span id="todastablas2"></span>
		<table>
			<tr><script>barra_imagenes("ccpet");</script><?php cuadro_titulos();?></tr>
			<tr><?php menu_desplegable("ccpet");?></tr>
			<tr>
				<td colspan="3" class="cinta"><img src="imagenes/add2.png" class="mgbt1"><img src="imagenes/guardad.png" style="width:24px;" class="mgbt1"><img src="imagenes/buscad.png" class="mgbt1"><img src="imagenes/nv.png" title="Nueva Ventana" onClick="mypop=window.open('ccp-principal.php','','');mypop.focus();" class="mgbt"></td>
			</tr>
		</table>
		<form name="form2" method="post" action="">
			<table class="inicio">
				<tr>
					<td class="titulos" colspan="2">.: Reportes SIA contraloria. </td>
					<td class="cerrar" style="width:7%;" ><a href="ccp-principal.php">&nbsp;Cerrar</a></td>
				</tr>
					<td style="background-repeat:no-repeat; background-position:center;">
						<ol id="lista2">
							<li onClick="location.href='ccp-f03cuentasbancarias.php'" style="cursor:pointer;">F03_AGR - CUENTAS BANCARIAS</li>
							<!-- <li onClick="location.href='ccp-f04cuentasbancarias.php'" style="cursor:pointer;">F04_AGR - CUENTAS BANCARIAS</li> -->
							<!-- <li onClick="location.href='ccp-auxiliarGastos.php'" style="cursor:pointer;">Ejecuci&oacute;n presupuestal CCPET - Auxiliar por Cuentas Gastos</li> -->
							<!-- <li onClick="location.href='ccp-ejecuPresuGastos.php'" style="cursor:pointer;">Ejecución presupuestal CCPET - Auxiliar por Cuentas Gastos Nuevo</li>
							<li onClick="location.href='ccp-ejecucioningresos.php'" style="cursor:pointer;">Ejecuci&oacute;n presupuesto ingresos</li>
							<li onClick="location.href='ccp-ejecuPresuIngresos.php'" style="cursor:pointer;">Ejecuci&oacute;n presupuesto ingresos con auxiliares</li> -->
							<!-- <li onClick="location.href='ccp-ejecuciongastos.php'" style="cursor:pointer;">Ejecuci&oacute;n presupuesto gastos a nivel cuentas</li>
							<li onClick="location.href='ccp-estadoregistro.php'" style="cursor:pointer;">Estado de registros</li>
							<li onClick="location.href='ccp-reportesaldopresupuestal.php'" style="cursor:pointer;">Reportes saldo presupuestal</li> --> 
							<!-- <li onClick="location.href='ccp-buscarContratos.php'" style="cursor:pointer;">Reportes Registros Presupuestales - Contratos</li>   
							<li onClick="location.href='ccp-saldosrp.php'" style="cursor:pointer;">Reportes saldos Registros Presupuestales</li>  
							<li onClick="location.href='ccp-saldoscuentasejecucion.php'" style="cursor:pointer;">Valor de rp's por rubros presupuestales</li>  
							<li onClick="location.href='ccp-formatof05.php'" style="cursor:pointer;">Formato F05</li>   -->
						</ol>
				</td>
				</tr>
			</table>
		</form>
	</body>
</html>