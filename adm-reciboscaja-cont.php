<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");

	require "comun.inc";
	require "funciones.inc";

	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");

	session_start();
    date_default_timezone_set("America/Bogota");
    $_POST['fechaini'] = isset($_POST['fechaini']) && $_POST['fechaini'] != "" ? $_POST['fechaini'] : date("Y")."/01/01";
    $_POST['fechafin'] = isset($_POST['fechafin']) && $_POST['fechafin'] != "" ? $_POST['fechafin'] : date("Y/m/d");
	$_POST['oculto2']=$_GET['oculto2'];
?>
<!DOCTYPE >
<html lang="es">
	<head>
		<meta charset="utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1"/>
		<meta name="viewport" content="user-scalable=no">
		<title>:: IDEAL 10 - Contabilidad</title>
		<link href="favicon.ico" rel="shortcut icon"/>
        <link href="css/css2.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<script type="text/javascript" src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script type="text/javascript" src="css/calendario.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script type="text/javascript" src="jquery-1.11.0.min.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
        <script>
			$(window).load(function () {
				$('#cargando').hide();
			});

			function crearexcel(){

			}
			function validar()
			{
				document.getElementById('oculto').value='3';
				document.form2.submit();
			}
			function direccionaComprobante(idCat,tipo_compro,num_compro)
			{
				window.open("cont-buscacomprobantes.php?idCat="+idCat+"&tipo_compro="+tipo_compro+"&num_compro="+num_compro);
			}
        </script>
	</head>

	<body>
		<IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
        <span id="todastablas2"></span>
		<div id="cargando" style=" position:absolute;left: 46%; bottom: 45%">
			<img src="imagenes/loading.gif" style=" width: 80px; height: 80px"/>
		</div>
        <table>
            <tr><script>barra_imagenes("cont");</script><?php cuadro_titulos();?></tr>
            <tr><?php menu_desplegable("cont");?></tr>
        	<tr>
  				<td colspan="3" class="cinta">
					<a href="adm-reciboscaja-cont.php" class="mgbt"><img src="imagenes/add.png" title="Nuevo"/></a>
					<a class="mgbt"><img src="imagenes/guardad.png"/></a>
					<a onClick="document.form2.submit();" href="#" class="mgbt"><img src="imagenes/busca.png" title="Buscar" /></a>
					<a href="#" class="mgbt" onClick="mypop=window.open('cont-principal.php','',''); mypop.focus();"><img src="imagenes/nv.png" title="Nueva ventana"></a>
					<a onClick="mypop=window.open('<?php echo $url2; ?>','','');mypop.focus();" class="tooltip bottom mgbt"><img src="imagenes/duplicar_pantalla.png" title="Duplicar pesta&ntilde;a">
					<a href="cont-estadoComprobantesComparacion.php" class="mgbt"><img src="imagenes/iratras.png" title="Atr&aacute;s"></a>
				</td>
         	</tr>
		</table>

 		<form name="form2" method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">
			<input type="hidden" name="nn[]" id="nn[]" value="<?php echo $_POST['nn'] ?>" >
                <?php
					$iter='saludo1b';
					$iter2='saludo2b';
					$filas=1;
				?>
			<table width="100%" align="center"  class="inicio" >
                <tr>
                    <td class="titulos" colspan="9">:: Buscar .: Recibos de Caja </td>
                    <td class="cerrar" style='width:7%' onClick="location.href='presu-principal.php'">Cerrar</td>
                    <input type="hidden" name="oculto" id="oculto" value="1">
                    <input type="hidden" name="iddeshff" id="iddeshff" value="<?php echo $_POST['iddeshff'];?>">
                </tr>
                <tr>
                    <td class="saludo1" >Fecha Inicial: </td>
                    <td style="width:10%;">
						<input type="text" name="fechaini" value="<?php echo $_POST['fechaini']?>" onchange="" maxlength="10" onKeyUp="return tabular(event,this)"  id="fc_1198971545" onKeyDown="mascara(this,'/',patron,true)" title="YYYY/MM/DD" style="height:30px;text-align:center;width:100%;" onDblClick="displayCalendarFor('fc_1198971545');" class="colordobleclik" readonly/>
					</td>

                    <td class="saludo1" >Fecha Final: </td>
					<td style="width:10%;">
						<input type="text" name="fechafin" value="<?php echo $_POST['fechafin']?>" onchange=""  maxlength="10" onKeyUp="return tabular(event,this)"  id="fc_1198971546" onKeyDown="mascara(this,'/',patron,true)" title="YYYY/MM/DD" style="height:30px;text-align:center;width:100%;" onDblClick="displayCalendarFor('fc_1198971546');" class="colordobleclik" readonly/>
					</td>

					<td class="saludo1" style="width:2.5cm;">Tipo Recaudo:</td>
                    <td>
						<select name="tiporec" id="tiporec" onKeyUp="return tabular(event,this)" onChange="validar()" style="width:95%;height:30px;">
							<option value=""> Seleccione ...</option>
							<option value="1" <?php if($_POST['tiporec']=='1') echo "SELECTED"; ?>>Predial</option>
							<option value="2" <?php if($_POST['tiporec']=='2') echo "SELECTED"; ?>>Industria y Comercio</option>
							<option value="3" <?php if($_POST['tiporec']=='3') echo "SELECTED"; ?>>Otros Recaudos</option>
						</select>
					</td>

                    <td>
						<em class='botonflechaverde' onClick="validar()" style='float:rigth;'>Buscar</em>
					</td>
                </tr>
			</table>

			<?php
			if($_POST['oculto']==3)
			{
				echo "
					<div class='subpanta' style='width:100%; margin-top:0px; overflow-x:hidden;overflow-y:scroll' id='divdet'>
						<table class='inicio' align='center'>
							<tr>
								<td colspan='7' class='titulos'>.: Resultados Busqueda:</td>
							</tr>

							<tr class='titulos ' style='text-align:center;'>
								<td ></td>
								<td ></td>
								<td >Tesoreria</td>
								<td colspan='2' >Contabilidad</td>
							</tr>

							<tr class='titulos' style='text-align:center;'>
								<td id='col1' style='width:7%;'>Id Recaudo</td>
								<td id='col2'>Concepto</td>
								<td id='col3' style='width:10%;'>Valor Total</td>
								<td id='col6' style='width:10%;'>Valor Total</td>
								<td id='col7' style='width:10%;'>Diferencia</td>
							</tr>
						</table>
					</div>";

					echo "
					<div class='subpantallac5' style='height:50%; width:99.6%; margin-top:0px; overflow-x:hidden' id='divdet'>
						<table class='inicio' align='center' id='valores' >
							<tbody>";

								$queryDate="";

								if(isset($_POST['fechafin']) and isset($_POST['fechaini'])){

									if(!empty($_POST['fechaini']) and !empty($_POST['fechafin'])){
										$fechaInicial=date('Y-m-d',strtotime($_POST['fechaini']));
										$fechaFinal=date('Y-m-d',strtotime($_POST['fechafin']));
										$queryDate=" T.fecha>='".$fechaInicial."' and T.fecha<='".$fechaFinal."'";
									}
								}

								if($_POST['tiporec']!=""){$crit2=" AND T.tipo='$_POST[tiporec]' ";}
								$crit3 = '';
								$sqlrBase = "SELECT base FROM redglobal WHERE base = 'u657666394_alcaldiarosa'";
								$respBase = mysqli_query($linkbd, $sqlrBase);
								$rowBase = mysqli_fetch_row($respBase);
								if($rowBase[0]=='u657666394_alcaldiarosa'){
									$crit3=" AND T.id_recibos != 1188 AND T.id_recibos != 1189 ";
								}
								$sqlr="select T.id_recibos, T.valor, T.estado,T.id_comp,T.descripcion from tesoreciboscaja T where  $queryDate $crit2 $crit3 group by T.id_recibos";

								$resp=mysqli_query($linkbd, $sqlr);
								$arreglo=Array();
								$co="zebra1";
								$co2="zebra2";
								while ($row = mysqli_fetch_row($resp))
								{
									//$valorReciboCaja = 0;

									$sq="select TD.ingreso,T.codigo,T.terceros, SUM(TD.valor) from tesoreciboscaja_det TD,tesoingresos T where id_recibos='$row[0]' and TD.ingreso=T.codigo AND TD.estado = 'S'";
									$re=mysqli_query($linkbd,$sq);
									$ro=mysqli_fetch_row($re);
									$estilo="";
									$stado="";

									$sql="select C.numerotipo,sum(C.valdebito),C.cuenta,T.estado,sum(C.valcredito) from comprobante_det C, comprobante_cab T  where T.numerotipo=C.numerotipo and T.tipo_comp=C.tipo_comp and C.numerotipo='$row[0]' and C.tipo_comp='5' and LEFT(cuenta,2)='11'";
									$rs=mysqli_query($linkbd,$sql);
									$rw=mysqli_fetch_row($rs);

									$dif=round($row[1])-round($rw[1])+round($rw[4]);

									if($row[2]=='N')
									{
										$stado="color:red";
									}
									if ($dif!=0)
									{
										$estilo='background-color:yellow';
									}

									if($rw[3]=='1' || $rw[3]==1)
									{
										$estadoCont = 'S';
									}
									else
									{
										$estadoCont = 'N';
									}
									if ($row[2]!= $estadoCont)
									{
										$estilo='background-color:#9F33FF';
									}
									$sqlr1="SELECT * FROM tipo_comprobante WHERE codigo='5'";
									$res2=mysqli_query($linkbd,$sqlr1);
									$row2=mysqli_fetch_row($res2);

										echo"
									<tr class='$co' ondblclick='direccionaComprobante($row2[5],$row2[3],$row[0])' style='text-transform:uppercase; $estilo; $stado' >
										<td style='width:7%;' id='1'>$row[0]</td>
										<td id='2'>$row[4]</td>
										<td style='text-align:right;width:10%;' id='3'>$".number_format(round($row[1]),2,',','.')."</td>
										<td  style='text-align:right;width:10%;' id='6'>$".number_format(round($rw[1]),2,',','.')."</td>
										<td  style='text-align:right;width:10%;' id='7'>$".number_format($dif,2,',','.')."</td>
									</tr>";

										$aux=$co;
										$co=$co2;
										$co2=$aux;
										$filas++;
										$resultadoSuma=0.0;

								}
						echo "
							</tbody>
						</table>
            		</div>";

}

					//FIN CODIGO PARA COMPARAR EL VALOR DEL EGRESO DE TESORERIA Y PRESUPUESTO

                ?>

        </form>
        <script type="text/javascript">
        	jQuery(function($){
        		if(jQuery){
        				$('#valores tbody tr:first-child td').each(function(index, el) {
        					if($(this).attr('id')=='1'){

        						$('#col1').css('width',$(this).css('width'));
        					}
        					if($(this).attr('id')=='2'){

        						$('#col2').css('width',$(this).css('width'));
        					}
        					if($(this).attr('id')=='3'){

        						$('#col3').css('width',$(this).css('width'));
        					}

        					if($(this).attr('id')=='6'){

        						$('#col6').css('width',$(this).css('width'));
        					}
        					if($(this).attr('id')=='7'){

        						$('#col7').css('width',$(this).css('width'));
        					}


        				});
        				$(window).resize(function() {
						   location.reload();
						});
        			}

        	});
        </script>
</body>
</html>
