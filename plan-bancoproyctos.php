<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require 'comun.inc';
	require 'funciones.inc';
	require 'validaciones.inc';
	require 'conversor.php';
	session_start();
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html lang="es">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="ie=edge"/>
		<title>IDEAL 10 - Planeaci&oacute;n Estrat&eacute;gica</title>
		<link href="favicon.ico" rel="shortcut icon"/>
		<link href="css/css2.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
		<link href="css/tabs.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
		<link href="css/style.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
		<link rel="stylesheet" href="sweetalert2/dist/sweetalert2.min.css">
		<script src="sweetalert2/dist/sweetalert2.min.js"></script>
		<script src="vue/vue.min.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script type="text/javascript" src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"> </script>

		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">

		<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">

		<script type="text/javascript" src="bootstrap/fontawesome.5.11.2/js/all.js"></script>

		<link href="css/style.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
		<style>
			.modal-mask{
				position: fixed;
				z-index: 9998;
				top: 0;
				left: 0;
				width: 100%;
				height: 100%;
				background-color: rgba(0, 0, 0, .5);
				display: table;
				transition: opacity .3s ease;
			}
			.modal-wrapper{
				display: table-cell;
				vertical-align: middle;
			}
			.modal-container{
				width: 60%;
				margin: 0px auto;
				padding: 20px 30px;
				text-align: left;
				background:linear-gradient(#99bbcc, #B6CEDA);
				border-radius: 2px;
				box-shadow: 0 2px 8px rgba(0, 0, 0, .33);
				transition: all .3s ease;
			}
			.modal-container1{
				width: 50%;
				margin: 0px auto;
				padding: 20px 30px;
				text-align: left;
				background:linear-gradient(#99bbcc, #B6CEDA);
				border-radius: 10px;
				box-shadow: 0 2px 8px rgba(0, 0, 0, .33);
				transition: all .3s ease;
			}
			.modal-container2{
				width: 80%;
				margin: 0px auto;
				padding: 20px 30px;
				text-align: left;
				background:linear-gradient(#99bbcc, #B6CEDA);
				border-radius: 2px;
				box-shadow: 0 2px 8px rgba(0, 0, 0, .33);
				transition: all .3s ease;
			}
			.modal-container3{
				width: 90%;
				margin: 0px auto;
				padding: 20px 30px;
				text-align: left;
				background:linear-gradient(#99bbcc, #B6CEDA);
				border-radius: 2px;
				box-shadow: 0 2px 8px rgba(0, 0, 0, .33);
				transition: all .3s ease;
			}
			footer{
				text-align: right;
			}
			.c9 input[type="checkbox"]:not(:checked),
            .c9 input[type="checkbox"]:checked {
                position: absolute !important;
                left: -9999px !important;
            }
            .c9 input[type="checkbox"]:not(:checked) +  #t9,
            .c9 input[type="checkbox"]:checked +  #t9 {
                position: relative !important;
                padding-left: 1.95em !important;
                cursor: pointer !important;
            }

            /* checkbox aspect */
            .c9 input[type="checkbox"]:not(:checked) +  #t9:before,
            .c9 input[type="checkbox"]:checked +  #t9:before {
                content: '' !important;
                position: absolute !important;
                left: 0 !important; top: -2 !important;
                width: 1.55em !important; height: 1.55em !important;
                border: 2px solid #ccc !important;
                background: #fff !important;
                border-radius: 4px !important;
                box-shadow: inset 0 1px 3px rgba(0,0,0,.1) !important;
            }
            /* checked mark aspect */
            .c9 input[type="checkbox"]:not(:checked) +  #t9:after,
            .c9 input[type="checkbox"]:checked + #t9:after {
                content: url(imagenes/tilde.png) !important;
                position: absolute !important;
                top: .1em; left: .3em !important;
                font-size: 1.3em !important;
                line-height: 0.8 !important;
                color: #09ad7e !important;
                transition: all .2s !important;
            }
            /* checked mark aspect changes */
            .c9 input[type="checkbox"]:not(:checked) +  #t9:after {
                opacity: 0 !important;
                transform: scale(0) !important;
            }
            .c9 input[type="checkbox"]:checked +  #t9:after {
                opacity: 1 !important;
                transform: scale(1) !important;
            }
            /* disabled checkbox */
            .c9 input[type="checkbox"]:disabled:not(:checked) +  #t9:before,
            .c9 input[type="checkbox"]:disabled:checked +  #t9:before {
                box-shadow: none !important;
                border-color: #bbb !important;
                background-color: #ddd !important;
            }
            .c9 input[type="checkbox"]:disabled:checked +  #t9:after {
                color: #999 !important;
            }
            .c9 input[type="checkbox"]:disabled +  #t9 {
                color: #aaa !important;
            }
            /* accessibility */
            .c9 input[type="checkbox"]:checked:focus + #t9:before,
            .c9 input[type="checkbox"]:not(:checked):focus + #t9:before {
                border: 2px dotted blue !important;
            }

            /* hover style just for information */
            .c9 #t9:hover:before {
                border: 2px solid #4778d9 !important;
            }
            #t9{
                background-color: white !important;
            }

            .c10 input[type="checkbox"]:not(:checked),
            .c10 input[type="checkbox"]:checked {
                position: absolute !important;
                left: -9999px !important;
            }
            .c10 input[type="checkbox"]:not(:checked) +  #t10,
            .c10 input[type="checkbox"]:checked +  #t10 {
                position: relative !important;
                padding-left: 1.95em !important;
                cursor: pointer !important;
            }

            /* checkbox aspect */
            .c10 input[type="checkbox"]:not(:checked) +  #t10:before,
            .c10 input[type="checkbox"]:checked +  #t10:before {
                content: '' !important;
                position: absolute !important;
                left: 0 !important; top: -2 !important;
                width: 1.55em !important; height: 1.55em !important;
                border: 2px solid #ccc !important;
                background: #fff !important;
                border-radius: 4px !important;
                box-shadow: inset 0 1px 3px rgba(0,0,0,.1) !important;
            }
            /* checked mark aspect */
            .c10 input[type="checkbox"]:not(:checked) +  #t10:after,
            .c10 input[type="checkbox"]:checked + #t10:after {
                content: url(imagenes/tilde.png) !important;
                position: absolute !important;
                top: .1em; left: .3em !important;
                font-size: 1.3em !important;
                line-height: 0.8 !important;
                color: #09ad7e !important;
                transition: all .2s !important;
            }
            /* checked mark aspect changes */
            .c10 input[type="checkbox"]:not(:checked) +  #t10:after {
                opacity: 0 !important;
                transform: scale(0) !important;
            }
            .c10 input[type="checkbox"]:checked +  #t10:after {
                opacity: 1 !important;
                transform: scale(1) !important;
            }
            /* disabled checkbox */
            .c10 input[type="checkbox"]:disabled:not(:checked) +  #t10:before,
            .c10 input[type="checkbox"]:disabled:checked +  #t10:before {
                box-shadow: none !important;
                border-color: #bbb !important;
                background-color: #ddd !important;
            }
            .c10 input[type="checkbox"]:disabled:checked +  #t10:after {
                color: #999 !important;
            }
            .c10 input[type="checkbox"]:disabled +  #t10 {
                color: #aaa !important;
            }
            /* accessibility */
            .c10 input[type="checkbox"]:checked:focus + #t10:before,
            .c10 input[type="checkbox"]:not(:checked):focus + #t10:before {
                border: 2px dotted blue !important;
            }

            /* hover style just for information */
            .c10 #t10:hover:before {
                border: 2px solid #4778d9 !important;
            }
            #t10{
                background-color: white !important;
            }

            .background_active_color{
				background: #16a085;
			}
			.background_active{
				/* font: 115% sans-serif !important; */
    			/*font-weight: 700 !important;*/
				/*font-family: "Constantia", serif !important;*/
				font-family: calibri !important;
				font-weight: bold !important;
				font-size:20px !important;
			}
			.background_active_1{
				/* font: 115% sans-serif !important; */
    			/*font-weight: 700 !important;*/
				/*font-family: "Constantia", serif !important;*/
				font-family: helvética !important;
				font-size:20px !important;
			}
			.inicio--no-shadow{
				box-shadow: none;
			}
			.btn-delete{
				background: red;
				color: white;
				border-radius: 5px;
				border: none;
				font-size: 13px;
			}
			.btn-delete:hover, .btn-delete:focus{
				background: white;
				color: red;
			}
			.modal-mask {
			position: fixed;
			z-index: 9998;
			top: 0;
			left: 0;
			width: 100%;
			height: 100%;
			background-color: rgba(0, 0, 0, .5);
			display: table;
			transition: opacity .3s ease;
			}

			.modal-wrapper {
			display: table-cell;
			vertical-align: middle;
			}
			.modal-body{
				max-height: 500px;
				overflow-y: scroll;
			}
			.modal-intermetio{
				margin: 0 15px;
				font-family: helvética !important;
				font-size: 26px !important;
				padding: 10px 0;
			}
			.modal-intermedio-agregar{
				text-align:right;
				padding: 4px;
				margin-top: 6px;
				margin-right: 20px
			}
			.modal-body_1{
				padding-top: 15px;
				height: 40px;
			}
			.loader-table{
				/* background-color: #dff9fb;
				opacity: .5; */
				display: flex;
				align-items: center;
				justify-content: center;
				height: 75%;
			}
			.spinner{
				border: 4px solid rgba(0, 0, 0, 0.2);
				border-left-color: #39C;
				border-radius: 50%;
				width: 50px;
				height: 50px;
				animation: spin .9s linear infinite;
			}
			@keyframes spin {
				to { transform: rotate(360deg); }
			}
			.modal-container1
			{
				width: 50%;
				margin: 0px auto;
				padding: 20px 30px;
				text-align: left;
				background:linear-gradient(#99bbcc, #B6CEDA);
				border-radius: 10px;
				box-shadow: 0 2px 8px rgba(0, 0, 0, .33);
				transition: all .3s ease;
			}

            [v-cloak]{
                display : none;
            }

            label{
                font-size:13px;
            }

            input{
                height: calc(1em + 0.6rem + 0.5px) !important;
                font-size: 14px !important;
                margin-top: 4px !important;
            }
		</style>
		<?php titlepag();?>
	</head>
	<body>
		<header>
			<table>
				<tr><script>barra_imagenes("plan");</script><?php cuadro_titulos();?></tr>
			</table>
		</header>
		<section id="myapp" v-cloak >
			<div class="loading-container" v-show="isLoading" >
				<p class="text-loading"data-loading="IDEAL 10">IDEAL 10</p>
			</div>
			<nav>
				<?php menu_desplegable("plan");?>
				<div class="bg-white group-btn p-1">
					<button type="button" @click="preguntaguardar('1')" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
						<span>Guardar</span>
						<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960"><path d="M840-680v480q0 33-23.5 56.5T760-120H200q-33 0-56.5-23.5T120-200v-560q0-33 23.5-56.5T200-840h480l160 160Zm-80 34L646-760H200v560h560v-446ZM480-240q50 0 85-35t35-85q0-50-35-85t-85-35q-50 0-85 35t-35 85q0 50 35 85t85 35ZM240-560h360v-160H240v160Zm-40-86v446-560 114Z"></path></svg>
					</button>
					<button type="button" @click="window.location.href='plan-buscabancoproyectos.php'" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
						<span>Buscar</span>
						<svg class="fill-black group-hover:fill-white w-5 h-5" xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960"><path d="M784-120 532-372q-30 24-69 38t-83 14q-109 0-184.5-75.5T120-580q0-109 75.5-184.5T380-840q109 0 184.5 75.5T640-580q0 44-14 83t-38 69l252 252-56 56ZM380-400q75 0 127.5-52.5T560-580q0-75-52.5-127.5T380-760q-75 0-127.5 52.5T200-580q0 75 52.5 127.5T380-400Z"></path></svg>
					</button>
					<button type="button" @click="mypop=window.open('plan-principal.php','','');mypop.focus();" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
						<span>Nueva ventana</span>
						<svg class="fill-black group-hover:fill-white w-5 h-5" xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960"><path d="M320-120v-80h80v-80H160q-33 0-56.5-23.5T80-360v-400q0-33 23.5-56.5T160-840h640q33 0 56.5 23.5T880-760v400q0 33-23.5 56.5T800-280H560v80h80v80H320ZM160-360h640v-400H160v400Zm0 0v-400 400Z"></path></svg>
					</button>
					<button type="button" @click="mypop=window.open('plan-bancoproyctos.php','','');mypop.focus();" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
						<span class="group-hover:text-white">Duplicar pantalla</span>
						<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960"><path d="M640-160v-360H160v360h480Zm80-200v-80h80v-360H320v200h-80v-200q0-33 23.5-56.5T320-880h480q33 0 56.5 23.5T880-800v360q0 33-23.5 56.5T800-360h-80ZM160-80q-33 0-56.5-23.5T80-160v-360q0-33 23.5-56.5T160-600h480q33 0 56.5 23.5T720-520v360q0 33-23.5 56.5T640-80H160Zm400-603ZM400-340Z"></path></svg>
					</button>
				</div>
			</nav>
			<table class="inicio ancho">
				<tr>
					<td class="titulos" colspan="8" >Ingresar Proyecto</td>
					<td class="cerrar" style="width:7%" onClick="location.href='plan-principal.php'">Cerrar</td>
				</tr>
				<tr>
					<td class="tamano01" style="width:4cm">Secci&oacute;n Presupuestal:</td>
					<td>
						<input type="text" v-model="unidadejecutora" v-on:dblclick='toggleModalUnidadEje' style="width:100%;height:30px;" v-bind:class="unidadejecutoradobleclick"  autocomplete="off" @paste="onPaste" readonly/>
						<input type="hidden" v-model="cunidadejecutora"/>
					</td>
					<td class="tamano01" style="width:7%">Vigencia:</td>
					<td style="width:7%">
						<select v-model="vigencia" style="width:100%">
							<option v-for="year in years" :value="year[0]">{{ year[0] }}</option>
						</select>
					</td>
				</tr>
				<tr>
					<td class="tamano01" style="width:3cm">C&oacute;digo BPIM:</td>
					<td style="width:20%">
                        <input type="text" v-model="codigo" style="width:100%;height:30px;" ref="codigo" maxlength="17" @keypress="onlyNumber" v-on:dblclick='ventanaRubroCredito' v-bind:class="unidadejecutoradobleclick"  autocomplete="off" @paste="onPaste">
                    </td>

					<td class="tamano01" style="width:3cm">Nombre Proyecto:</td>
					<td colspan="3"><input type="text" v-model="nombre" style="width:100%;height:30px;" ref="nombre"/></td>
				</tr>
				<tr>
					<td class="tamano01">Descripci&oacute;n:</td>
					<td colspan="5"><input type="text" v-model="descripcion" style="width:100%;height:30px;" ref="descripcion"/></td>
				</tr>
				<tr>
					<td class="tamano01">Sector:</td>
					<td colspan="3"><input type="text" v-model="sector" v-on:dblclick='toggleModal' style="width:100%;height:30px;" v-bind:class="sectordobleclick" autocomplete="off" readonly/><input type="hidden" v-model="csector"/></td>
					<td class="tamano01">Programa:</td>
					<td colspan="3"><input type="text"  v-model="programa" v-on:dblclick='toggleModal2' style="width:100%;height:30px;" v-bind:class="programadobleclick" readonly/><input type="hidden" v-model="cprograma"/></td>
				</tr>
				<tr>
					<td class="tamano01">Subprograma:</td>
					<td colspan="3"><input type="text"  v-model="subprograma" style="width:100%;height:30px;" readonly/><input type="hidden" v-model="csubprograma"/></td>
					<td class="tamano01">Indicador Producto:</td>
					<td colspan="3"><input type="text" v-model="indicadorpro" v-on:dblclick='toggleModal3' style="width:100%;height:30px;" v-bind:class=" indicadordobleclick" autocomplete="off" v-on:keyup="validaindicadorproducto(indicadorpro)"/><input type="hidden" v-model="cindicadorpro"/></td>
				</tr>
				<tr>
					<td class="tamano01">Producto:</td>
					<td colspan="4"><input type="text"  v-model="producto" style="width:100%;height:30px;"  autocomplete="off" readonly/><input type="hidden" v-model="cproducto"/></td>
					<td style=" height: 30px;"><em class="botonflecha" v-on:click="agregarproducto()">Agregar</em></td>
				</tr>
			</table>
			<div class='subpantalla' style='height:39%; width:99.5%; margin-top:0px; overflow-x:hidden'>
				<table class='inicio inicio--no-shadow'>
					<tbody>
						<tr class="titulos">
							<td>Producto</td>
							<td>Indicador</td>
							<td></td>
						</tr>
						<tr v-for="(vcproducto, index) in selecproductosa" v-bind:class="index % 2 ? 'saludo1a' : 'saludo2'" style='text-rendering: optimizeLegibility; cursor: pointer !important;' >
							<td width="45%" style="font: 120% sans-serif; padding-left:10px">{{ vcproducto[1] }}</td>
							<td style="font: 120% sans-serif; padding-left:10px">{{ vcproducto[3] }}</td>
							<td  v-on:click="eliminaproducto(index)"><img src='imagenes/del.png'></td>
						</tr>
					</tbody>
				</table>
			</div>
			<div v-show="showMensaje">
				<transition name="modal">
					<div class="modal-mask">
						<div class="modal-wrapper">
							<div class="modal-container1">
								<table id='ventanamensaje1' class='inicio' style="border-radius: 10px;">
									<tr >
										<td class="titulosmensajes1" v-bind:style="{color:colortitulosmensaje,}" style=" text-shadow: 7px 4px 5px grey;font-style: italic;border-radius: 50px;">{{titulomensaje}}</td>
									</tr>
									<tr>
										<td class='.cuerpomensajes1' style="text-align:center;"><h3 style="font-size: 20px;font-family:Gotham, 'Helvetica Neue', Helvetica, Arial, 'sans-serif';font-style: italic;">{{ contenidomensaje }}</h3></td>
									</tr>
									<tr>
										<td class='.cuerpomensajes1' style="padding: 14px;text-align:center">
											<em name="continuar" id="continuar" class="botonflecha" @click="toggleMensaje()">Continuar</em>
										</td>
									</tr>
								</table>
							</div>
						</div>
					</div>
				</transition>
			</div>
			<div v-show="showModalUnidadEj">
				<transition name="modal">
					<div class="modal-mask">
						<div class="modal-wrapper">
							<div class="modal-container">
								<table class="inicio ancho">
									<tr>
										<td class="titulos" colspan="2" >SELECCIONAR UNIDAD EJECUTORA</td>
										<td class="cerrar" style="width:7%" @click="showModalUnidadEj = false">Cerrar</td>
									</tr>
									<!-- <tr>
										<td class="tamano01" style="width:3cm">Unidad Ejecutora:</td>
										<td><input type="text" class="form-control" placeholder="Buscar por nombre de unidad ejecutora" v-on:keyup="searchMonitorUnidadEj" v-model="search.keyword" style="width:100%" /></td>
									</tr> -->
								</table>
								<table>
									<thead>
										<tr>
											<td class='titulos' style="font: 160% sans-serif; border-radius: 5px 0 0 0; width:20%;">C&oacute;digo</td>
											<td class='titulos' style="font: 160% sans-serif; width:60%;">Nombre</td>
											<td class='titulos' style="font: 160% sans-serif; border-radius: 0 5px 0 0;">Estado</td>
										</tr>
									</thead>
								</table>
								<div style='margin: 0px 5px 5px; border-radius: 0 0 5px 5px; height: 200px; overflow: scroll; overflow-x: hidden; background: white;'>
									<table class='inicio inicio--no-shadow'>
										<tbody>
											<tr v-for="(unidadeejecutora,index) in unidadesejecutoras" v-on:click="cargaunidadejecutora(unidadeejecutora.codigo, unidadeejecutora.idUE, unidadeejecutora.nombre)" v-bind:class="index % 2 ? 'saludo1a' : 'saludo2'" style='text-rendering: optimizeLegibility; cursor: pointer !important;'>
												<td style="font: 120% sans-serif; padding-left:10px; width:20%;">{{ unidadeejecutora.codigo }}</td>
												<td style="font: 120% sans-serif; padding-left:10px; width:60%;">{{ unidadeejecutora.nombre }}</td>
												<td style="font: 120% sans-serif; padding-left:10px">{{ unidadeejecutora.estado }}</td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</transition>
			</div>
			<div v-show="showModal">
				<transition name="modal">
					<div class="modal-mask">
					<div class="modal-wrapper">
						<div class="modal-container">
						<table class="inicio ancho">
							<tr>
							<td class="titulos" colspan="2">SELECCIONAR SECTOR</td>
							<td class="cerrar" style="width:7%" @click="showModal = false">Cerrar</td>
							</tr>
							<tr>
							<td class="tamano01" style="width:3cm">Código o Sector:</td>
							<td>
								<input type="text" class="form-control" placeholder="Buscar por código o nombre de sector" v-on:keyup="searchMonitor" v-model="search.keyword" style="width:100%" />
							</td>
							</tr>
						</table>
						<table>
							<thead>
							<tr>
								<td class="titulos" style="font: 160% sans-serif; border-radius: 5px 0 0 0; width:20%;">Código</td>
								<td class="titulos" style="font: 160% sans-serif; width:60%;">Nombre</td>
								<td class="titulos" style="font: 160% sans-serif; border-radius: 0 5px 0 0;">Aplicación</td>
							</tr>
							</thead>
						</table>
						<div style="margin: 0px 5px 5px; border-radius: 0 0 5px 5px; height: 400px; overflow: scroll; overflow-x: hidden; background: white;">
							<table class="inicio inicio--no-shadow">
							<tbody>
								<tr v-for="(sector, index) in sectores" v-on:click="cargasector(sector.codigo, sector.nombre)" v-bind:class="index % 2 ? 'saludo1a' : 'saludo2'" style="text-rendering: optimizeLegibility; cursor: pointer !important;">
								<td style="font: 120% sans-serif; padding-left:10px; width:20%;">{{ sector.codigo }}</td>
								<td style="font: 120% sans-serif; padding-left:10px; width:60%;">{{ sector.nombre }}</td>
								<td style="font: 120% sans-serif; padding-left:10px">{{ sector.aplicacion }}</td>
								</tr>
							</tbody>
							</table>
						</div>
						</div>
					</div>
					</div>
				</transition>
			</div>
			<div v-show="showModal2">
				<transition name="modal">
					<div class="modal-mask">
						<div class="modal-wrapper">
							<div class="modal-container2">
								<table class="inicio ancho">
									<tr>
										<td class="titulos" colspan="2">SELECCIONAR PROGRAMA</td>
										<td class="cerrar" style="width:7%" @click="showModal2 = false">Cerrar</td>
									</tr>
									<tr>
										<td class="tamano01" style="width:5cm">Código o Programa:</td>
										<td>
											<input type="text" class="form-control" placeholder="Buscar por código o nombre del programa" v-on:keyup="searchMonitorPrograms" v-model="searchProgram.keywordProgram" style="width:100%" />
										</td>
									</tr>
								</table>
								<table>
									<thead>
										<tr>
											<td width="10%" class='titulos' style="font: 160% sans-serif; border-radius: 5px 0 0 0;">Código</td>
											<td width="30%" class='titulos' style="font: 160% sans-serif;">Nombre programa</td>
											<td width="20%" class='titulos' style="font: 160% sans-serif;">Código subprograma</td>
											<td width="20%" class='titulos' style="font: 160% sans-serif;">Nombre subprograma</td>
											<td width="20%" class='titulos' style="font: 160% sans-serif; border-radius: 0 5px 0 0;">Aplicación</td>
										</tr>
									</thead>
								</table>
								<div style='margin: 0px 5px 5px; border-radius: 0 0 5px 5px; height: 200px; overflow: scroll; overflow-x: hidden; background: white;'>
									<table class='inicio inicio--no-shadow'>
										<tbody>
											<tr v-for="(programa, index) in programas_subprogramas"
												v-on:click="cargaprograma(programa.codigo, programa.nombre, programa.codigo_subprograma, programa.nombre_subprograma, programa.aplicacion)"
												v-bind:class="index % 2 ? 'saludo1a' : 'saludo2'"
												style='text-rendering: optimizeLegibility; cursor: pointer !important;'>
												<td width="10%" style="font: 120% sans-serif; padding-left:10px">
													{{ programa.codigo }}
												</td>
												<td width="30%" style="font: 120% sans-serif; padding-left:10px">
													{{ programa.nombre }}
												</td>
												<td width="20%" style="font: 120% sans-serif; padding-left:10px">
													{{ programa.codigo_subprograma }}
												</td>
												<td width="20%" style="font: 120% sans-serif; padding-left:10px">
													{{ programa.nombre_subprograma }}
												</td>
												<td width="20%" style="font: 120% sans-serif; padding-left:10px">
													{{ programa.aplicacion }}
												</td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</transition>
			</div>
			<div v-show="showModal3">
				<transition name="modal">
					<div class="modal-mask">
						<div class="modal-wrapper">
							<div class="modal-container3">
								<table class="inicio ancho">
									<tr>
										<td class="titulos" colspan="2" >SELECCIONAR PRODUCTOS</td>
										<td class="cerrar" style="width:7%" @click="showModal3 = false">Cerrar</td>
									</tr>
									<tr>
										<td class="tamano01" style="width:8cm">C&oacute;digo, producto o indicador producto:</td>
										<td><input type="text" class="form-control" placeholder="Buscar por c&oacute;digo, nombre de producto o indicador producto" v-on:keyup="searchMonitorProducts" v-model="searchProduct.keywordProduct" style="width:100%" /></td>
									</tr>
								</table>
								<table>
									<thead>
										<tr>
											<td width="10%" class='titulos' style="font: 160% sans-serif; padding-left:10px; border-radius: 5px 0 0 0;">C&oacute;digo</td>
											<td width="20%" class='titulos' style="font: 160% sans-serif; ">Producto</td>
											<td width="30%" class='titulos' style="font: 160% sans-serif; ">Descripci&oacute;n</td>
											<td width="10%" class='titulos' style="font: 160% sans-serif; ">Medio a traves</td>
											<td width="5%" class='titulos' style="font: 160% sans-serif; ">C&oacute;digo indicador</td>
											<td width="10%" class='titulos' style="font: 160% sans-serif;">Indicador producto</td>
											<td width="10%" class='titulos' style="font: 160% sans-serif;">Unidad medida</td>
											<td width="5%" class='titulos' style="font: 160% sans-serif; padding-right:10px; border-radius: 0 5px 0 0;">Indicador principal</td>
										</tr>
									</thead>
								</table>
								<div style='margin: 0px 5px 5px; border-radius: 0 0 5px 5px; height: 400px; overflow: scroll; overflow-x: hidden; background: white;'>
									<table class='inicio inicio--no-shadow'>
										<tbody>
											<tr v-for="(producto,index) in productos" v-on:click="cargaproducto(producto.cod_producto, producto.producto, producto.codigo_indicador, producto.indicador_producto)" v-bind:class="index % 2 ? 'saludo1a' : 'saludo2'" style='text-rendering: optimizeLegibility; cursor: pointer !important;'>
												<td width="10%" style="font: 120% sans-serif; padding-left:10px">{{ producto.cod_producto }}</td>
												<td width="20%" style="font: 120% sans-serif; padding-left:10px">{{ producto.producto }}</td>
												<td width="30%" style="font: 120% sans-serif; padding-left:10px">{{ producto.descripcion }}</td>
												<td width="10%" style="font: 120% sans-serif; padding-left:10px">{{ producto.medio_a_traves }}</td>
												<td width="5%" style="font: 120% sans-serif; padding-left:10px">{{ producto.codigo_indicador }}</td>
												<td width="10%" style="font: 120% sans-serif; padding-left:10px">{{ producto.indicador_producto  }}</td>
												<td width="10%" style="font: 120% sans-serif; padding-left:10px">{{ producto.unidad_medida }}</td>
												<td width="5%" style="font: 120% sans-serif; padding-left:10px">{{ producto.indicador_principal }}</td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</transition>
			</div>
			<div v-show="showModal_programaticoProyectos">
				<transition name="modal">
					<div class="modal-mask">
						<div class="modal-wrapper">
							<div class="modal-dialog modal-xl" style = "max-width: 1200px !important;" role="document">
								<div class="modal-content"  style = "width: 100% !important;" scrollable>
									<div class="modal-header">
										<h5 class="modal-title">Buscar rubros</h5>
										<button type="button" @click="showModal_programaticoProyectos=false;" class="btn btn-close"><div></div><div></div></button>
									</div>
									<div class="modal-body overflow-hidden">
										<div class="d-flex mb-3">
											<div class="form-control border-none">
												<label class="form-label m-0" for="">Sector:</label>
												<select @change="filter('sector')" v-model="selectSector">
													<option value="0" disabled selected>Seleccione</option>
													<option v-for="(data,index) in arrSectores" :key="index" :value="data.codigo">
														{{data.codigo+"-"+data.nombre}}
													</option>
												</select>
											</div>
											<div class="form-control border-none">
												<label class="form-label m-0" for="">Programa:</label>
												<select @change="filter('programa')"  v-model="selectPrograma">
													<option value="0" disabled selected>Seleccione</option>
													<option v-for="(data,index) in arrProgramasFilter" :key="index" :value="data.codigo">
														{{data.codigo+"-"+data.nombre}}
													</option>
												</select>
											</div>
											<div class="form-control border-none">
												<label class="form-label m-0" for="">Producto:</label>
												<select @change="filter('producto')"  v-model="selectProducto">
													<option value="0" disabled selected>Seleccione</option>
													<option v-for="(data,index) in arrProductosFilter" :key="index" :value="data.codigo">
														{{data.codigo+"-"+data.nombre}}
													</option>
												</select>
											</div>
										</div>
										<div class="overflow-auto" style="max-height:55vh">
											<table class='table table-hover fw-normal'>
												<thead>
													<tr>
														<th>Rubro</th>
														<th>Proyecto</th>
														<th>Fuente</th>
														<th>Vigencia</th>
														<th>Seccion presupuestal</th>
													</tr>
												</thead>
												<tbody>
													<tr v-for="cuenta in proyectosProgramaticosCopy" v-on:click="seleccionarProgramaticoProyecto(cuenta[10])" class='<?php echo $co; ?>' style='text-rendering: optimizeLegibility; cursor: pointer !important; style=\"cursor: hand\"'>
														<td class="text-nowrap">{{ cuenta[0] }}</td>
														<td >{{ cuenta[1] }}</td>
														<td >{{ cuenta[2] }}</td>
														<td >{{ cuenta[3] }}</td>
														<td >{{ cuenta[4] }}</td>
													</tr>
												</tbody>
											</table>
										</div>
									</div>
									<div class="modal-footer">
										<button type="button" class="btn btn-secondary" @click="showModal_programaticoProyectos = false">Cerrar</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</transition>
			</div>
			<input type="hidden" v-model="idproyecto"/>
		</section>
		<script src="Librerias/vue/vue.min.js"></script>
		<script src="Librerias/vue/axios.min.js"></script>
		<script src="vue/presupuesto_ccp/plan-bancoproyectos.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
	</body>
</html>
