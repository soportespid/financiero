<?php
	require_once("tcpdf/tcpdf_include.php");
	require('comun.inc');
	require('funciones.inc');
	session_start();
	$nomuser = $_SESSION['usuario'];
	$linkbd_v7 = conectar_v7();
	$linkbd_v7 -> set_charset("utf8");
	class MYPDF extends TCPDF {
		public function Header(){
			$linkbd_v7 = conectar_v7();
			$linkbd_v7 -> set_charset("utf8");
			$sqlr = "SELECT * FROM configbasica WHERE estado='S'";
			$res = mysqli_query($linkbd_v7, $sqlr);
			while($row = mysqli_fetch_row($res))
			{
				$nit = $row[0];
				$rs = $row[1];
			}
			$this->Image('imagenes/escudo.jpg', 16, 12, 39, 30, 'JPG', '', 'T', true, 300, '', false, false, 0, false, false, false);
			$this->SetFont('helvetica','B',7);
			$this->SetY(10);
			$this->RoundedRect(10, 10, 260, 38, 5,'' );
			$this->Line(60,10,60,48);
			$this->SetY(10);
			$this->SetX(60);
			$this->SetFont('helvetica','B',12);
			$this->Cell(170,15,"$rs",0,0,'C'); 
			$this->SetY(16);
			$this->SetX(58);
			$this->SetFont('helvetica','B',11);
			$this->Cell(170,10,"$nit",0,0,'C');
			$this->SetFont('helvetica','B',8);
			$this->SetY(12);
			$this->Cell(223);
			$this->Cell(35,5,'NUMERO : '.$_POST['numero'],0,0,'L');
			$this->SetY(16);
			$this->Cell(223);
			$this->Cell(35,5,'SOLICITUD : '.$_POST['docum'],0,0,'L');
			$this->SetY(20);
			$this->Cell(223);
			$this->Cell(35,5,'FECHA: '.$_POST['fecha'],0,0,'L');
			$this->Line(230,10,230,27);
			//*****************************************************************************************************************************
			if($_POST['tipomov']==1){$tipomov = 'Entrada de almacen';}
			else{
				if($_POST['tipomov']==2){$tipomov = 'Salida';}
				else{
					if($_POST['tipomov']==3){$tipomov = 'Reversion de Entrada';}
					else{
						if($_POST['tipomov']==4){$tipomov = 'Reversion de Salida'.$_POST['tipomov'];}
					}
				}
			}
			$this->SetY(27);
			$this->Cell(50.2);
			$this->SetFont('helvetica','B',12);
			$this->Cell(210,8,$tipomov,'T',1,'C');
			$this->Cell(50.2);
			$this->SetFont('helvetica','B',10);
			$this->MultiCell(210,11,$_POST['nombre'],0,'C');
			//*****************************************************************************************************************************
			$this->SetY(27);
			$this->Cell(50.2);
			$this->MultiCell(110.7,4,'',0,'L');
			if($_POST['movr']=='1')
			{
				if ($_POST['entr'] == '01') {
					
					$this->ln(4);
					$fec = explode("/", $_POST['fecha']);
					$sqlr = "SELECT tercero FROM ccpetrp WHERE consvigencia = '".$_POST['docum']."' AND vigencia = $fec[2] ";
					$res = mysqli_query($linkbd_v7, $sqlr);
					$row = mysqli_fetch_row($res);
					$nresul = buscatercero($row[0]);
					$sqlrTer = "SELECT direccion, depto, mnpio FROM terceros  WHERE cedulanit = '$row[0]'";
					$resTer = mysqli_query($linkbd_v7, $sqlrTer);
					$rowTer = mysqli_fetch_row($resTer);
					$this->SetFont('helvetica','B',10);
					$this->Cell(30,34,'Proveedor: ',0,0,'L');
					$this->SetFont('helvetica','',10);
					$this->Cell(125,34,$nresul,0,0,'L');
					$this->ln(2);
					$this->SetFont('helvetica','B',10);
					$this->Cell(30,40,'Nit/Cedula: ',0,0,'L');
					$this->SetFont('helvetica','',10);
					$this->Cell(125,40,$row[0],0,0,'L');
					$this->ln(2);
					$this->SetFont('helvetica','B',10);
					$this->Cell(30,46,'Direccion: ',0,0,'L');
					$this->SetFont('helvetica','',10);
					$this->Cell(125,46,$rowTer[0],0,0,'L');
					$sqlrCiudad = "SELECT nom_mnpio FROM danemnpio WHERE danedpto = '$rowTer[1]' AND danemnpio = '$rowTer[2]'";
					$resCiudad = mysqli_query($linkbd_v7, $sqlrCiudad);
					$rowCiudad = mysqli_fetch_row($resCiudad);
					$this->ln(2);
					$this->SetFont('helvetica','B',10);
					$this->Cell(30,52,'Ciudad: ',0,0,'L');
					$this->SetFont('helvetica','',10);
					$this->Cell(125,52,$rowCiudad[0],0,0,'L');
				}

				if ($_POST['entr'] == '04') {
					
					$this->ln(4);
					$fec = explode("/", $_POST['fecha']);
					
					$nresul = buscatercero($_POST['doctercero']);
					$sqlrTer = "SELECT direccion, depto, mnpio FROM terceros  WHERE cedulanit = '$_POST[doctercero]'";
					$resTer = mysqli_query($linkbd_v7, $sqlrTer);
					$rowTer = mysqli_fetch_row($resTer);
					$this->SetFont('helvetica','B',10);
					$this->Cell(30,34,'Proveedor: ',0,0,'L');
					$this->SetFont('helvetica','',10);
					$this->Cell(125,34,$nresul,0,0,'L');
					$this->ln(2);
					$this->SetFont('helvetica','B',10);
					$this->Cell(30,40,'Nit/Cedula: ',0,0,'L');
					$this->SetFont('helvetica','',10);
					$this->Cell(125,40,$_POST['doctercero'],0,0,'L');
					$this->ln(2);
					$this->SetFont('helvetica','B',10);
					$this->Cell(30,46,'Direccion: ',0,0,'L');
					$this->SetFont('helvetica','',10);
					$this->Cell(125,46,$rowTer[0],0,0,'L');
					$sqlrCiudad = "SELECT nom_mnpio FROM danemnpio WHERE danedpto = '$rowTer[1]' AND danemnpio = '$rowTer[2]'";
					$resCiudad = mysqli_query($linkbd_v7, $sqlrCiudad);
					$rowCiudad = mysqli_fetch_row($resCiudad);
					$this->ln(2);
					$this->SetFont('helvetica','B',10);
					$this->Cell(30,52,'Ciudad: ',0,0,'L');
					$this->SetFont('helvetica','',10);
					$this->Cell(125,52,$rowCiudad[0],0,0,'L');
				}
				
			}
			//*****************************************************************************************************************************
			$this->ln(14);
			$this->line(10.1,72,269,72);
			$this->RoundedRect(10,73, 260, 9, 1.2,'' );	
			$this->SetFont('helvetica','B',9);
			$this->SetY(75);
			$this->Cell(10,5,'Item',0,0,'C'); 
			$this->Cell(20,5,'UNSPSC ',0,0,'C'); 
			$this->Cell(27,5,'Codigo Articulo',0,0,'C');
			$this->Cell(40,5,'Nombre del Articulo',0,0,'C');
			$this->Cell(26,5,'Marca',0,0,'C');
			$this->Cell(26,5,'Modelo',0,0,'C');
			$this->Cell(26,5,'Serial',0,0,'C');
			$this->Cell(15,5,'Cantidad',0,0,'C');
			$this->Cell(20,5,'U. Medida',0,0,'C');
			$this->Cell(25,5,'Vr.Unitario',0,0,'C');
			$this->Cell(25,5,'Vr.Total',0,0,'C');
			$this->line(10.1,83,269,83);
			$this->ln(4);
		}
		public function Footer(){
			$linkbd=conectar_v7();
			$linkbd -> set_charset("utf8");
			$sqlr="SELECT direccion,telefono,web,email FROM configbasica WHERE estado='S'";
			$resp=mysqli_query($linkbd,$sqlr);
			while($row=mysqli_fetch_row($resp))
			{
				$direcc = strtoupper($row[0]);
				$telefonos = $row[1];
				$dirweb = strtoupper($row[3]);
				$coemail = strtoupper($row[2]);
			}
			$nomuser = $_SESSION['usuario'];
			$fechaActual = date("d-m-Y h:i:s");

			if($direcc!=''){$vardirec="Dirección: $direcc, ";}
			else {$vardirec="";}
			if($telefonos!=''){$vartelef="Telefonos: $telefonos";}
			else{$vartelef="";}
			if($dirweb!=''){$varemail="Email: $dirweb, ";}
			else {$varemail="";}
			if($coemail!=''){$varpagiw="Pagina Web: $coemail";}
			else{$varpagiw="";}
			$this->SetFont('helvetica', 'I', 8);
			$txt = <<< EOD
			$vardirec $vartelef
			$varemail $varpagiw
			$nomuser - $fechaActual
EOD;
			$this->Write(0, $txt, '', 0, 'C', true, 0, false, false, 0);
			$this->Cell(0, 10, 'Page '.$this->getAliasNumPage().'/'.$this->getAliasNbPages(), 0, false, 'C', 0, '', 0, false, 'T', 'M');
			
		}
	}
	
	$pdf = new MYPDF('L','mm','Letter', true, 'UTF-8', false);
	$pdf->SetDocInfoUnicode (true); 
	$pdf->SetCreator(PDF_CREATOR);
	$pdf->SetAuthor('IDEAL10');
	$pdf->SetMargins(10, 76, 10);
	$pdf->SetHeaderMargin(101);
	$pdf->SetFooterMargin(20);
	$pdf->SetAutoPageBreak(TRUE, 10);
	if (@file_exists(dirname(__FILE__).'/lang/spa.php'))
	{
		require_once(dirname(__FILE__).'/lang/spa.php');
		$pdf->setLanguageArray($l);
	}
	$pdf->AddPage();
	$pdf->SetFont('Times','',8);
	$pdf->SetY(80);
	$con = 0;
	$total = 0;
	$cont = 1;
	$totalart = count($_POST['codunsd']);
	for($xx = 0; $xx < $totalart; $xx++){
		$pdf->ln(6);
		$v = $pdf->gety();
		if($v >= 180){
			$pdf->AddPage();
			$pdf->ln(10);
			$v = $pdf->gety();
		}
		if ($con%2==0){$pdf->SetFillColor(255,255,255);}
		else{$pdf->SetFillColor(245,245,245);}
		$pdf->SetY($v);
		$pdf->Cell(10,4,$cont,0,0,'L',1);//Item
		$pdf->Cell(20,4,$_POST['codunsd'][$xx],0,0,'L',1);//UNSPSC
		$pdf->Cell(27,4,$_POST['codinard'][$xx],0,0,'L',1);//Codigo Articulo
		$pdf->MultiCell(40,4,$_POST['nomartd'][$xx],0,'L',true);//Nombre del Articulo
		$pdf->SetY($v);
		$pdf->SetX(107);
		$pdf->Cell(26,4,$_POST['marcaart'][$xx],0,0,'L',1);//Marca
		$pdf->Cell(26,4,$_POST['modeloart'][$xx],0,0,'L',1);//Modelo
		$pdf->Cell(26,4,$_POST['serieart'][$xx],0,0,'L',1);//Serial
		$pdf->Cell(15,4,$_POST['cantidadd'][$xx],0,0,'L',1);//Cantidad
		$pdf->Cell(20,4,$_POST['unidadd'][$xx],0,0,'R',1);//U. Medida
		$pdf->Cell(25,4,number_format($_POST['valunit'][$xx],2,',','.'),0,0,'R',1);//Vr.Unitario
		$pdf->Cell(25,4,number_format($_POST['valtotal'][$xx],2,',','.'),0,1,'R',1);//Vr.Total
		$con=$con+1;
		$cont+=1;
		$w=$pdf->gety();
		$pdf->SetY($w);
	}
	$pdf->ln(4);
	$v=$pdf->gety();
	$x=$pdf->getx();
	$pdf->line(10.1,$v-2,269,$v-2);
	$pdf->Cell(208);
	$pdf->SetFont('Times','B',9);
	$pdf->Cell(30,4,'Total',0,0,'C');
	$pdf->Cell(26,4,''.number_format($_POST['totalart'],2,',','.'),0,1,'C');
	$pdf->ln(3);

	if($_POST['tipomov']==1)
	{
		$v=$pdf->gety();
		if($v>=160)
		{
			$pdf->AddPage();
			$pdf->ln(10);
			$v=$pdf->gety();
		}
		$fec = explode("/", $_POST['fecha']);
		$sqlr="select id_cargo,id_comprobante from pptofirmas where id_comprobante='26' and vigencia='".$fec[2]."'";
		$res=mysqli_query($linkbd_v7, $sqlr);
		while($row = mysqli_fetch_assoc($res))
		{
			$sqlr1="select cedulanit,(select nombrecargo from planaccargos where codcargo='".$row["id_cargo"]."') from planestructura_terceros where codcargo='".$row["id_cargo"]."' and estado='S'";
			$res1=mysqli_query($linkbd_v7, $sqlr1);
			$row1=mysqli_fetch_row($res1);
			$nombreAlmacenista = buscatercero($row1[0]);
			$cedulaAlmacenista = $row1[0];
			$nomcargo = $row1[1];
		}
		$x=$pdf->getx();
		$pdf->RoundedRect($x+7, $v, 120 , 32, 1.2,'' );
		$pdf->Cell(8);
		$pdf->Cell(125,6,'RESPONSABLE: ',0,0,'L');
		$pdf->ln(8.5);
		$pdf->Cell(8);
		$pdf->Cell(125,6,'Firma: ',0,0,'L');
		$w=$pdf->gety();
		$pdf->line(45,$w+4,120,$w+4);
		$pdf->ln(5.5);
		$pdf->Cell(8);
		$pdf->Cell(30,6,'Nombre: ',0,0,'L');
		$w=$pdf->gety();
		$pdf->line(45,$w+4,120,$w+4);
		$pdf->ln(5.5);
		$pdf->Cell(8);
		$pdf->SetFont('helvetica','B',9);
		$pdf->Cell(30,6,'C.C: ',0,0,'L');
		$w=$pdf->gety();
		$pdf->line(45,$w+4,120,$w+4);
		$pdf->ln(5.5);
		$pdf->Cell(8);
		$pdf->SetFont('helvetica','B',9);
		$pdf->Cell(30,6,'Cargo: ',0,0,'L');
		$w=$pdf->gety();
		$pdf->line(45,$w+4,120,$w+4);
		$pdf->ln(5.5);
		for($x=0; $x<$_GET['beneficiario'];$x++)
		{
			if($x == 0){$nombretercero = $_POST['nombre2'];$numcc = $_POST['doctercero'];}
			else{$nombretercero = '';$numcc = '';}
			$v=$pdf->gety();
			if($v>=160)
			{
				$pdf->AddPage();
				$pdf->ln(10);
				$v=$pdf->gety();
			}
			$x=$pdf->getx();
			$pdf->RoundedRect($x+7, $v, 120 , 32, 1.2,'' );
			$pdf->RoundedRect($x+130, $v, 120 , 32, 1.2,'' );
			$pdf->Cell(8);
			$pdf->Cell(125,6,'ENTREGA: ',0,0,'L');
			$pdf->Cell(200,6,'RECIBIO: ',0,0,'L');
			$pdf->ln(5.5);
			$pdf->Cell(8);
			$pdf->Cell(125,6,'Firma: ',0,0,'L');
			$pdf->Cell(125,6,'Firma: ',0,0,'L');
			$w=$pdf->gety();
			$pdf->line(35,$w+4,100,$w+4);
			$pdf->line(160,$w+4,225,$w+4);
			$pdf->ln(5.5);
			$pdf->Cell(8);
			$pdf->Cell(125,6,"Nombre: ",0,0,'L');
			$pdf->Cell(16,"Nombre:",0,0,'L');
			$pdf->Cell(100,6,$nombretercero,0,0,'L');
			$w=$pdf->gety();
			$pdf->line(35,$w+4,100,$w+4);
			$pdf->line(160,$w+4,225,$w+4);
			$pdf->ln(5.5);
			$pdf->Cell(8);
			$pdf->Cell(125,6,'C.C: ',0,0,'L');
			$pdf->Cell(16,6,"C.C:",0,0,'L');
			$pdf->Cell(100,6,$numcc,0,0,'L');
			$w=$pdf->gety();
			$pdf->line(35,$w+4,100,$w+4);
			$pdf->line(160,$w+4,225,$w+4);
			$pdf->ln(5.5);
			$pdf->Cell(8);
			$pdf->Cell(125,6,'Cargo: ',0,0,'L');
			$pdf->Cell(200,6,'Cargo: ',0,0,'L');
			$w=$pdf->gety();
			$pdf->line(35,$w+4,100,$w+4);
			$pdf->line(160,$w+4,225,$w+4);
			$pdf->ln(5.5);
		}
	}
	else
	{
		$v=$pdf->gety();
		if($v>=160)
		{
			$pdf->AddPage();
			$pdf->ln(10);
			$v=$pdf->gety();
		}
		$nombretercero = $_POST['nombre2'];
		$numcc = $_POST['doctercero'];
		$x=$pdf->getx();
		$pdf->RoundedRect($x+7, $v, 120 , 32, 1.2,'' );
		$pdf->RoundedRect($x+130, $v, 120 , 32, 1.2,'' );
		$pdf->Cell(8);
		$pdf->Cell(125,6,'ENTREGA: ',0,0,'L');
		$pdf->Cell(200,6,'RECIBIO: ',0,0,'L');
		$pdf->ln(5.5);
		$pdf->Cell(8);
		$pdf->Cell(125,6,'Firma: ',0,0,'L');
		$pdf->Cell(125,6,'Firma: ',0,0,'L');
		$w=$pdf->gety();
		$pdf->line(35,$w+4,100,$w+4);
		$pdf->line(160,$w+4,225,$w+4);
		$pdf->ln(5.5);
		$pdf->Cell(8);
		$pdf->Cell(125,6,'Nombre:',0,0,'L');
		$pdf->Cell(16,6,"Nombre:",0,0,'L');
		$pdf->Cell(100,6,$nombretercero,0,0,'L');
		$w=$pdf->gety();
		$pdf->line(35,$w+4,100,$w+4);
		$pdf->line(160,$w+4,225,$w+4);
		$pdf->ln(5.5);
		$pdf->Cell(8);
		$pdf->Cell(125,6,'C.C: ',0,0,'L');
		$pdf->Cell(16,6,"C.C:",0,0,'L');
		$pdf->Cell(100,6,$numcc,0,0,'L');
		$w=$pdf->gety();
		$pdf->line(35,$w+4,100,$w+4);
		$pdf->line(160,$w+4,225,$w+4);
		$pdf->ln(5.5);
		$pdf->Cell(8);
		$pdf->Cell(125,6,'Cargo: ',0,0,'L');
		$pdf->Cell(200,6,'Cargo: ',0,0,'L');
		$w=$pdf->gety();
		$pdf->line(35,$w+4,100,$w+4);
		$pdf->line(160,$w+4,225,$w+4);
		$pdf->ln(5.5);
		$contabene = $_GET['beneficiario']-1;
		for($z=0; $z<$contabene;$z++)
		{
			$v=$pdf->gety();
			if($v>=160)
			{
				$pdf->AddPage();
				$pdf->ln(10);
				$v=$pdf->gety();
			}
			$x=$pdf->getx();
			if($contabene%2=='0')
			{
				$pdf->RoundedRect($x+7, $v, 120 , 32, 1.2,'' );
				$pdf->RoundedRect($x+130, $v, 120 , 32, 1.2,'' );
				$pdf->Cell(8);
				$pdf->Cell(125,6,'ENTREGA: ',0,0,'L');
				$pdf->Cell(200,6,'RECIBIO: ',0,0,'L');
				$pdf->ln(5.5);
				$pdf->Cell(8);
				$pdf->Cell(125,6,'Firma: ',0,0,'L');
				$pdf->Cell(125,6,'Firma: ',0,0,'L');
				$w=$pdf->gety();
				$pdf->line(35,$w+4,100,$w+4);
				$pdf->line(160,$w+4,225,$w+4);
				$pdf->ln(5.5);
				$pdf->Cell(8);
				$pdf->Cell(125,6,"Nombre:",0,0,'L');
				$pdf->Cell(16,6,"Nombre: ",0,0,'L');
				$pdf->Cell(100,6,$nombretercero,0,0,'L');
				$w=$pdf->gety();
				$pdf->line(35,$w+4,100,$w+4);
				$pdf->line(160,$w+4,225,$w+4);
				$pdf->ln(5.5);
				$pdf->Cell(8);
				$pdf->Cell(125,6,'C.C: ',0,0,'L');
				$pdf->Cell(200,6,"C.C:",0,0,'L');
				$pdf->Cell(100,6,$numcc,0,0,'L');
				$w=$pdf->gety();
				$pdf->line(35,$w+4,100,$w+4);
				$pdf->line(160,$w+4,225,$w+4);
				$pdf->ln(5.5);
				$pdf->Cell(8);
				$pdf->Cell(125,6,'Cargo: ',0,0,'L');
				$pdf->Cell(200,6,'Cargo: ',0,0,'L');
				$w=$pdf->gety();
				$pdf->line(35,$w+4,100,$w+4);
				$pdf->line(160,$w+4,225,$w+4);
				$pdf->ln(5.5);
				$z=$z+1;
			}
			else
			{
				if($contabene-$z < '2')
				{
					$pdf->RoundedRect($x+7, $v, 120 , 22, 1.2,'' );
					$pdf->Cell(8);
					$pdf->Cell(125,6,'RECIBIO: ',0,0,'L');
					$pdf->ln(5.5);
					$pdf->Cell(8);
					$pdf->Cell(125,6,'Firma: ',0,0,'L');
					$w=$pdf->gety();
					$pdf->line(30,$w+4,80,$w+4);
					$pdf->ln(5.5);
					$pdf->Cell(8);
					$pdf->Cell(16,6,"Nombre:",0,0,'L');
					$pdf->Cell(100,6,$nombretercero,0,0,'L');
					$w=$pdf->gety();
					$pdf->line(35,$w+4,130,$w+4);
					$pdf->ln(5.5);
					$pdf->Cell(8);
					$pdf->Cell(16,6,"C.C:",0,0,'L');
					$pdf->Cell(100,6,$numcc,0,0,'L');
					$pdf->line(50,$w+4,130,$w+4);
					$pdf->ln(10);
				}
				else
				{
					$pdf->RoundedRect($x+7, $v, 120 , 22, 1.2,'' );
					$pdf->RoundedRect($x+130, $v, 120 , 22, 1.2,'' );
					$pdf->Cell(8);
					$pdf->Cell(125,6,'RECIBIO: ',0,0,'L');
					$pdf->Cell(200,6,'RECIBIO: ',0,0,'L');
					$pdf->ln(5.5);
					$pdf->Cell(8);
					$pdf->Cell(125,6,'Firma: ',0,0,'L');
					$pdf->Cell(125,6,'Firma: ',0,0,'L');
					$w=$pdf->gety();
					$pdf->line(30,$w+4,80,$w+4);
					$pdf->line(155,$w+4,210,$w+4);
					$pdf->ln(5.5);
					$pdf->Cell(8);
					$pdf->Cell(125,6,"Nombre:",0,0,'L');
					$pdf->Cell(16,6,"Nombre:",0,0,'L');
					$pdf->Cell(100,6,$nombretercero,0,0,'L');
					$w=$pdf->gety();
					$pdf->line(35,$w+4,130,$w+4);
					$pdf->line(250,$w+4,160,$w+4);
					$pdf->ln(5.5);
					$pdf->Cell(8);
					$pdf->Cell(125,6,'C.C: ',0,0,'L');
					$pdf->Cell(16,6,"C.C:",0,0,'L');
					$pdf->Cell(100,6,$numcc,0,0,'L');
					$pdf->line(50,$w+4,130,$w+4);
					$pdf->ln(10);
					$z=$z+1;
				}
			}
		}
	}
$pdf->Output();
?> 