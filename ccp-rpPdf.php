<?php
require_once ("tcpdf/tcpdf_include.php");
require ('comun.inc');
require "funciones.inc";
session_start();
date_default_timezone_set("America/Bogota");
$linkbd = conectar_v7();
$linkbd->set_charset("utf8");
class MYPDF extends TCPDF
{
    public function Header()
    {
        $linkbd = conectar_v7();
        $linkbd->set_charset("utf8");
        $sqlr = "select *from configbasica where estado='S' ";
        $res = mysqli_query($linkbd, $sqlr);
        while ($row = mysqli_fetch_row($res)) {
            $nit = $row[0];
            $rs = $row[1];
            $nalca = $row[6];
        }
        $detallegreso = $_POST['detallegreso'];

        if ($_POST['estadoc'] == 'ANULADO') {
            $this->Image('imagenes/reversado02.png', 220, 25, 40, 10);
        }
        //Parte Izquierda
        $this->Image('imagenes/escudo.jpg', 13, 13, 20, 20, 'JPG', '', 'T', true, 300, '', false, false, 0, false, false, false);// Logo
        $this->SetFont('helvetica', 'B', 8);
        $this->SetY(10);
        $this->RoundedRect(10, 10, 277, 25, 1, '1001');
        $this->Cell(0.1);
        $this->Cell(26, 25, '', 'R', 0, 'L');
        $this->SetY(8);
        $this->SetX(80);
        $this->SetFont('helvetica', 'B', 9);
        $this->Cell(160, 15, strtoupper("$rs"), 0, 0, 'C');
        $this->SetFont('helvetica', 'B', 7);
        $this->SetY(12);
        $this->SetX(80);
        $this->Cell(160, 15, 'NIT: ' . $nit, 0, 0, 'C');
        //*****************************************************************************************************************************
        $this->SetFont('helvetica', 'B', 9);
        $this->SetY(23);
        $this->SetX(36);
        $this->Cell(251, 12, "REGISTRO PRESUPUESTAL DE COMPROMISO", 'T', 0, 'C');
        $this->SetFont('helvetica', 'B', 6);
        $this->SetY(10);
        $this->SetX(257);
        $this->Cell(30, 4, " NÚMERO RP: " . $_POST['numero'], "L", 0, 'L');
        $this->SetY(14);
        $this->SetX(257);
        $this->Cell(35, 5, " FECHA: " . $_POST['fecha'], "L", 0, 'L');
        $this->SetY(19);
        $this->SetX(257);
        $this->Cell(35, 4, " VIGENCIA: " . $_POST['vigencia'], "L", 1, 'L');
        $this->SetY(35);
        $this->Cell(92.2, 4, " CDP No: " . $_POST['numerocdp'], 'LB', 0, 'C');
        $this->Cell(92.2, 4, " EXPEDIDO EL: " . $_POST['fechacdp'], 'B', 0, 'C');
        $this->Cell(92.6, 4, " CONTRATO: " . $_POST['ncontrato'], 'RB', 0, 'C');

    }
    public function Footer()
    {
        $linkbd = conectar_v7();
        $linkbd->set_charset("utf8");
        $sqlr = "SELECT direccion,telefono,web,email FROM configbasica WHERE estado='S'";
        $resp = mysqli_query($linkbd, $sqlr);
        $user = $_SESSION['nickusu'];
        $fecha = date("Y-m-d H:i:s");
        $ip = $_SERVER['REMOTE_ADDR'];
        $useri = $_POST['user'];
        while ($row = mysqli_fetch_row($resp)) {
            $direcc = strtoupper($row[0]);
            $telefonos = $row[1];
            $dirweb = strtoupper($row[3]);
            $coemail = strtoupper($row[2]);
        }
        if ($direcc != '') {
            $vardirec = "Dirección: $direcc, ";
        } else {
            $vardirec = "";
        }
        if ($telefonos != '') {
            $vartelef = "Telefonos: $telefonos";
        } else {
            $vartelef = "";
        }
        if ($dirweb != '') {
            $varemail = "Email: $dirweb, ";
        } else {
            $varemail = "";
        }
        if ($coemail != '') {
            $varpagiw = "Pagina Web: $coemail";
        } else {
            $varpagiw = "";
        }
        $this->SetFont('helvetica', 'I', 8);
        $txt = <<<EOD
			$vardirec $vartelef
			$varemail $varpagiw
			EOD;
        $this->SetFont('helvetica', 'I', 6);
        $this->Cell(277, 10, '', 'T', 0, 'T');
        $this->ln(2);
        $this->Write(0, $txt, '', 0, 'C', true, 0, false, false, 0);
        $this->Cell(25, 10, 'Hecho por: ' . $useri, 0, false, 'C', 0, '', 0, false, 'T', 'M');
        $this->Cell(100, 10, 'Impreso por: ' . $user, 0, false, 'C', 0, '', 0, false, 'T', 'M');
        $this->Cell(30, 10, 'IP: ' . $ip, 0, false, 'C', 0, '', 0, false, 'T', 'M');
        $this->Cell(102, 10, '', 0, false, 'C', 0, '', 0, false, 'T', 'M');
        /* $this->Cell(102, 10, 'Fecha: ' . $fecha, 0, false, 'C', 0, '', 0, false, 'T', 'M'); */
        $this->Cell(5, 10, 'IDEAL.10 S.A.S    Pagina ' . $this->getAliasNumPage() . '/' . $this->getAliasNbPages(), 0, false, 'C', 0, '', 0, false, 'T', 'M');

    }
}
$pdf = new MYPDF('L', 'mm', 'Letter', true, 'iso-8859-1', false);
$pdf->SetDocInfoUnicode(true);
// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('IDEALSAS');
$pdf->SetTitle('Certificados');
$pdf->SetSubject('Certificado de Disponibilidad');
$pdf->SetMargins(10, 39, 10);// set margins
$pdf->SetHeaderMargin(39);// set margins
$pdf->SetFooterMargin(17);// set margins
$pdf->SetAutoPageBreak(TRUE, 20);// set auto page breaks
$pdf->AddPage();
$linkbd = conectar_v7();
$linkbd->set_charset("utf8");
$sqlr = "SELECT id_cargo,id_comprobante FROM pptofirmas WHERE id_comprobante='7' AND vigencia='".$_POST['vigencia']."'";
$res = mysqli_query($linkbd, $sqlr);

while ($row = mysqli_fetch_assoc($res)) {
    if ($row["id_cargo"] == '0') {
        $_POST['ppto'][] = buscatercero($_POST['tercero']);
        $_POST['nomcargo'][] = 'BENEFICIARIO';
    } else {
        $sqlr1 = "select cedulanit,(select nombrecargo from planaccargos where codcargo='" . $row["id_cargo"] . "') from planestructura_terceros where codcargo='" . $row["id_cargo"] . "' and estado='S'";
        $res1 = mysqli_query($linkbd, $sqlr1);
        $row1 = mysqli_fetch_row($res1);
        $_POST['ppto'][] = buscar_empleado($row1[0]);
        $_POST['nomcargo'][] = $row1[1];
    }
}
$pdf->ln(2);
$y = $pdf->GetY();

$pdf->SetFont('helvetica', 'B', 8);
$pdf->SetFillColor(245, 245, 245);
$pdf->Cell(25, 7, "BENEFICIARIO:", 'RB', 0, 'L', true);
$pdf->SetFont('helvetica', '', 7);
$pdf->Cell(252, 7, $_POST['ntercero'], 0, 1, 'L', false, 0, 0, false, '', 'C');
$pdf->SetFont('helvetica', 'B', 8);
$pdf->SetFillColor(245, 245, 245);
$pdf->Cell(25, 7, "CEDULA/NIT:", 'RBT', 0, 'L', true);
$pdf->SetFont('helvetica', '', 7);
$pdf->Cell(252, 7, $_POST['tercero'], 'T', 1, 'L', false, 0, 0, false, '', 'C');
$pdf->SetFont('helvetica', 'B', 8);
$contobj = $pdf->getNumLines($_POST['objeto'], 252);
$obj = $pdf->getNumLines('OBJETO:', 25);
$maxaltobj = max($contobj, $obj);
$altobj = $maxaltobj * 3;
$pdf->SetFillColor(245, 245, 245);
$pdf->Cell(25, $altobj, "OBJETO:", 'R', 0, 'L', true);
$pdf->SetFont('helvetica', '', 7);
$pdf->MultiCell(252, $altobj, $_POST['objeto'], 'T', 'L', false, 1, '', '', true, '', false, true, '', 'M', false);
$pdf->RoundedRect(10, $y, 277, $altobj + 14.5, 1, '1111');

$pdf->ln(2);
$y = $pdf->GetY();
$pdf->SetFillColor(222, 222, 222);
$pdf->SetFont('helvetica', 'B', 6);

$pdf->Cell(20, 5, 'Rubro', 0, 0, 'C', 1);
$pdf->SetY($y);
$pdf->Cell(21);
$pdf->Cell(30, 5, 'Dependencia', 0, 0, 'C', 1);
$pdf->SetY($y);
$pdf->Cell(52);
$pdf->Cell(20, 5, 'Medio pago', 0, 0, 'C', 1);
$pdf->SetY($y);
$pdf->Cell(73);
$pdf->Cell(25, 5, 'Vig gasto', 0, 0, 'C', 1);
$pdf->SetY($y);
$pdf->Cell(99);
$pdf->Cell(40, 5, 'Proyecto', 0, 0, 'C', 1);
$pdf->SetY($y);
$pdf->Cell(140);
$pdf->Cell(25, 5, 'Programático', 0, 0, 'C', 1);
$pdf->SetY($y);
$pdf->Cell(166);
$pdf->Cell(30, 5, 'CCPET', 0, 0, 'C', 1);
$pdf->SetY($y);
$pdf->Cell(197);
$pdf->Cell(30, 5, 'Fuente', 0, 0, 'C', 1);
$pdf->SetY($y);
$pdf->Cell(228);
$pdf->Cell(25, 5, 'CPC', 0, 0, 'C', 1);
$pdf->SetY($y);
$pdf->Cell(254);
$pdf->Cell(23, 5, 'Valor', 0, 1, 'C', 1);
$pdf->ln(1);



$con = 0;
while ($con < count($_POST['dcuenta'])) {
    if ($con % 2 == 0) {
        $pdf->SetFillColor(255, 255, 255);
    } else {
        $pdf->SetFillColor(245, 245, 245);
    }
    $rubro = '';
    $partsFuente = explode('-', $_POST['dfuente'][$con]);
    if ($_POST['dbpim'][$con] != '') {

        $rubro = $_POST['dprogramatico'][$con] . "-" . $_POST['dbpim'][$con] . "-" . $partsFuente[0];
    } else {
        $partsCuenta = explode("-", $_POST['dcuenta'][$con]);
        $rubro = $partsCuenta[0] . "-" . $partsFuente[0];
    }
    $contadorRubro = $pdf->getNumLines($rubro, 21);
    $contadorProyecto = $pdf->getNumLines($_POST['dbpim'][$con] . ' - ' . strtolower($_POST['nombreProyecto'][$con]), 41);
    $contadorProgramatico = $pdf->getNumLines($_POST['dprogramatico'][$con] . ' - ' . $_POST['indpro'][$con], 26);
    $contadorCCPET = $pdf->getNumLines($_POST['dcuenta'][$con], 31);
    $contadorFuente = $pdf->getNumLines($_POST['dfuente'][$con], 31);
    $contadorCPC = $pdf->getNumLines($_POST['dfuente'][$con], 26);
    $alturamax = max($contadorRubro, $contadorProyecto, $contadorProgramatico, $contadorCCPET, $contadorFuente, $contadorCPC);
    $altura = $alturamax * 3;
    $pdf->SetFont('helvetica', '', 6);
    $pdf->MultiCell(20, $altura, $rubro, 0, 'C', true, 0, '', '', true, 0, false, true, 0, 'M', true);
    $pdf->MultiCell(31, $altura, strtolower($_POST['dsecPresupuestal'][$con]), 0, 'C', true, 0, '', '', true, 0, false, true, 0, 'M', true);
    $pdf->MultiCell(21, $altura, $_POST['dmedioPago'][$con], 0, 'C', true, 0, '', '', true, 0, false, true, 0, 'M', true);
    $pdf->MultiCell(26, $altura, $_POST['dvigGasto'][$con], 0, 'C', true, 0, '', '', true, 0, false, true, 0, 'M', true);
    $pdf->MultiCell(41, $altura, $_POST['dbpim'][$con] . ' - ' . strtolower($_POST['nombreProyecto'][$con]), 0, 'C', true, 0, '', '', true, 0, false, true, 0, 'M', true);
    $pdf->MultiCell(26, $altura, $_POST['dprogramatico'][$con] . ' - ' . $_POST['indpro'][$con], 0, 'C', true, 0, '', '', true, 0, false, true, 0, 'M', true);
    $pdf->MultiCell(31, $altura, $_POST['dcuenta'][$con], 0, 'C', true, 0, '', '', true, 0, false, true, 0, 'M', true);
    $pdf->MultiCell(31, $altura, $_POST['dfuente'][$con], 0, 'C', true, 0, '', '', true, 0, false, true, 0, 'M', true);
    $pdf->MultiCell(26, $altura, $_POST['dproductoservicio'][$con] . ' - ' . $_POST['nomCpc'][$con], 0, 'C', true, 0, '', '', true, 0, false, true, 0, 'M', true);
    $pdf->MultiCell(24, $altura, "$ " . number_format($_POST['dvalor'][$con], 2, $_SESSION["spdecimal"], $_SESSION["spmillares"]), 0, 'R', true, 1, '', '', true, 0, false, true, 0, 'M', true);
    $con = $con + 1;
}
$pdf->ln(1);
$pdf->SetFont('helvetica', 'B', 7);
$pdf->Cell(223, 5, 'SON: ' . strtoupper($_POST['letras'] . " M/CTE"), 'T', 0, 'L');
$pdf->SetFont('helvetica', '', 7);
$pdf->Cell(30, 5, 'Total:', 'T', 0, 'R');
$pdf->Cell(24, 5, '$' . number_format($_POST['cuentagas2'], 2, $_SESSION["spdecimal"], $_SESSION["spmillares"]), 'T', 1, 'R');
$pdf->ln(20);
for ($x = 0; $x < count($_POST['ppto']); $x++) {
    $pdf->setFont('helvetica', 'B', 6);
    $pdf->ln(4);
    $v = $pdf->gety();
    if ($v >= 180) {
        $pdf->AddPage();
        $pdf->ln(15);
        $v = $pdf->gety();
    }
    $pdf->setFont('helvetica', 'B', 6);
    if (($x % 2) == 0) {
        if (isset($_POST['ppto'][$x + 1])) {

            $pdf->Cell(25, 4, '', 0, 0, 'C', false, 0, 0, false, 'T', 'C');
            $pdf->Cell(100, 4, '' . $_POST['ppto'][$x], 'T', 0, 'C', false, 0, 0, false, 'T', 'C');
            $pdf->Cell(25, 4, '', 0, 0, 'C', false, 0, 0, false, 'T', 'C');
            $pdf->Cell(100, 4, '' . $_POST['ppto'][$x + 1], 'T', 1, 'C', false, 0, 0, false, 'T', 'C');
            $pdf->Cell(25, 4, '', 0, 0, 'C', false, 0, 0, false, 'T', 'C');
            $pdf->Cell(100, 4, '' . $_POST['nomcargo'][$x], 0, 0, 'C', false, 0, 0, false, 'T', 'C');
            $pdf->Cell(25, 4, '', 0, 0, 'C', false, 0, 0, false, 'T', 'C');
            $pdf->Cell(100, 4, '' . $_POST['nomcargo'][$x + 1], 0, 1, 'C', false, 0, 0, false, 'T', 'C');
        } else {
            $v = $v + 5;
            $pdf->ln(5);
            $pdf->Line(105, $v, 195, $v);
            $pdf->Cell(280, 4, '' . $_POST['ppto'][$x], 0, 1, 'C', false, 0, 0, false, 'T', 'C');
            $pdf->Cell(280, 4, '' . $_POST['nomcargo'][$x], 0, 0, 'C', false, 0, 0, false, 'T', 'C');
        }
        $v3 = $pdf->gety();
    }
    $pdf->SetY($v3);
    $pdf->SetFont('helvetica', '', 7);
}
$pdf->Output();
