<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require 'comun.inc';
	require 'funciones.inc';
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
	date_default_timezone_set("America/Bogota");

?>
<!DOCTYPE >
<html lang="es">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="ie=edge"/>
		<title>:: SieS</title>
		<link href="css/css2.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css">
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css">
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css">
		<script type="text/javascript" src="css/calendario.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script type="text/javascript" src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		
		<script>
			function fagregar(documento,idfun)
			{	
				if(document.getElementById('tcodfun').value!='' && document.getElementById('tcodfun').value != null)
				{
					tcodfun = document.getElementById('tcodfun').value;
					parent.document.getElementById(''+tcodfun).value=idfun;
				}
				tobjeto=document.getElementById('tobjeto').value;
				parent.document.getElementById(''+tobjeto).value = documento;
				parent.document.getElementById(''+tobjeto).select();
				parent.document.getElementById(''+tobjeto).blur();
				parent.despliegamodal2("hidden");
			}
		</script> 
		<?php titlepag();?>
	</head>
	<body>
		<form name="form2" method="post">
			<?php
				if(@$_POST['oculto']=="")
				{
					$_POST['tobjeto'] = $_GET['objeto'];
					$_POST['tcodfun'] = $_GET['vcodfun'];
				}
			?>
			<table class="inicio ancho" style="width:99.5%">
				<tr>
					<td class="titulos" colspan="4">:: Buscar Funcionario</td>
					<td class="cerrar" style="width:7%;" onClick="parent.despliegamodal2('hidden');">Cerrar</td>
					
				</tr>
				<tr>
					<td class="saludo1" style='width:4cm;'>:: Documento o Nombre:</td>
					<td ><input type="search" name="nombre" id="nombre" value="<?php echo $_POST['nombre'];?>" style='width:100%;'/> </td>
					<td style="padding-bottom:0px;height:35px;"><em class="botonflecha" onClick="document.form2.submit();">Buscar</em></td>
				</tr>
			</table> 
			<input type="hidden" name="oculto" id="oculto" value="1"/>
			<input type="hidden" name="tobjeto" id="tobjeto" value="<?php echo $_POST['tobjeto']?>"/>
			<input type="hidden" name="tcodfun" id="tcodfun" value="<?php echo $_POST['tcodfun']?>"/>
			<div class="subpantalla" style="height:82%; width:99.2%; overflow-x:hidden;">
				<?php 
					if ($_POST['nombre'] != "")
					{$crit1 = "AND (SELECT T2.codfun FROM hum_funcionarios T2 WHERE T2.descripcion  LIKE  '%".$_POST['nombre']."%' AND T2.estado = 'S' AND T2.codfun = T1.codfun AND (T2.item = 'NOMTERCERO' OR T2.item='DOCTERCERO'))";}
					else {$crit1 = "";}
					$sqlr="
					SELECT T1.codfun, 
					GROUP_CONCAT(T1.descripcion ORDER BY CONVERT(T1.valor, SIGNED INTEGER) SEPARATOR '<->')
					FROM hum_funcionarios T1
					WHERE (T1.item = 'VALESCALA' OR T1.item = 'DOCTERCERO' OR T1.item = 'NOMTERCERO' OR T1.item = 'ESTGEN' OR T1.item = 'NOMCC') AND T1.estado='S' $crit1 
					GROUP BY T1.codfun
					ORDER BY CONVERT(T1.codfun, SIGNED INTEGER)";
					$resp = mysqli_query($linkbd,$sqlr);
					$con=mysqli_num_rows($resp);
					echo "
					<table class='inicio' align='center' width='99%'>
						<tr><td colspan='6' class='titulos'>.: Resultados Busqueda:</td></tr>
						<tr><td colspan='7'>funcionarios Encontrados: </td></tr>
						<tr class='titulos2' >
							<td>No</td>
							<td width='2%'>ID</td>
							<td width='12%'>DOCUMENTO</td>
							<td >NOMBRE</td>
							<td width='15%'>SALARIO BASICO</td>
							<td width='25%'>CENTRO COSTO</td>
						</tr>";	
					$iter='saludo1a';
					$iter2='saludo2';
					$conta=1;
					while ($row =mysqli_fetch_row($resp)) 
					{
						$datos = explode('<->',$row[1]);
						if(@$datos[4]=="S")
						{
							echo "
							<tr class='$iter' onClick=\"javascript:fagregar('$datos[1]','$row[0]')\">
								<td>$conta</td>
								<td>$row[0]</td>
								<td style='text-align:right;'>".number_format($datos[1],0,'','.')."</td>
								<td>$datos[2]</td>
								<td style='text-align:right;'>$ ".number_format($datos[0],0,'','.')."</td>
								<td>$datos[3]</td>
							</tr>
							";
							$aux=$iter;
							$iter=$iter2;
							$iter2=$aux;
							$conta++;
						}
					}
					echo"</table>";
				?>
			</div>
		</form>
	</body>
</html>
