<?php

    header("Cache-control: no-cache, no-store, must-revalidate");
    header("Content-Type: text/html;charset=utf8");

    require 'comun.inc';
    require 'funciones.inc';

    session_start();
    date_default_timezone_set("America/Bogota");

?>
<html lang="es">
    <head>
        <meta charset="utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1"/>
		<meta name="viewport" content="user-scalable=no">
        <title>:: IDEAL 10 - Presupuesto</title>
        <link href="favicon.ico" rel="shortcut icon"/>
        <link href="css/css2.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
        <link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
        <link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
        <script type="text/javascript" src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
        <script type="text/javascript" src="css/calendario.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script type="text/javascript" src="JQuery/jquery-2.1.4.min.js"></script>
        <link href="css/style.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />

        <!-- <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">

        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
        <script type="text/javascript" src="bootstrap/fontawesome.5.11.2/js/all.js"></script> -->

        <script>
            function ocultarTabla(){
                app.mostrarEjecucion = false;
            }
        </script>
        <style>
            [v-cloak]{
                display : none;
            }

            /* label{
                font-size:13px;
            }

            input{
                height: calc(1em + 0.6rem + 0.5px) !important;
                font-size: 14px !important;
                margin-top: 4px !important;
            }



            .ancho{
                width: 60px;
            }

             */

            label{
                font-size:13px;
            }

            input{
                height: calc(1em + 0.6rem + 0.5px) !important;
                font-size: 14px !important;
                margin-top: 4px !important;
            }

            .captura{
                font-weight: normal;
                cursor: pointer !important;
            }

            .captura:hover{
                background: linear-gradient(#40f3ff , #40b3ff 70%, #40f3ff );
            }

            .colorFila{
                background-color: #F0FFFD !important;
            }

            .colorFila1{
                background-color: #FFF02A !important;
                font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, "Helvetica Neue", Arial, "Noto Sans", sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol", "Noto Color Emoji";
                font-size:10px;
                font-weight:normal;
                border:#eeeeee 1px solid;
                padding-left:3px;
                padding-right:3px;
                margin-bottom:1px;
                margin-top:1px;
                height:23.5px;
                border-radius: 1px;
            }

            .colorFila1:hover{
                background: linear-gradient(#40f3ff , #40b3ff 70%, #40f3ff );
            }

            .agregado{
                font-weight: 700;
            }

            .dobleclickCelda:hover{
                color: brown;
            }

            .sumTotal{
                /* background-color: #8bd1a9; */
                background-color: #31ada1;
                font-weight: 700;
                color: black;
            }
            .sumTotalFuncionamiento{
                background-color: #8bd1a9;
                /* background-color: #59d999; */
                font-weight: 700;
                color:black;
            }

        </style>
    </head>
    <body>
        <IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
        <span id="todastablas2"></span>
        <header>
			<table>
				<tr><script>barra_imagenes("ccpet");</script><?php cuadro_titulos();?></tr>
			</table>
		</header>
        <form name="form2" method="post" action="">
            <section id="myapp" v-cloak >
                <nav>
                    <table>
                        <tr><?php menu_desplegable("ccpet");?></tr>
                        <tr>
                            <td colspan="3" class="cinta">
                                <img src="imagenes/add.png"  class="mgbt1" onClick="location.href='ccp-ejecucionpresupuestal-inv.php'" title="Nuevo">
                                <img src="imagenes/guardad.png" title="Guardar"  class="mgbt1">
                                <img src="imagenes/buscad.png" class="mgbt1" title="Buscar">
                                <img src="imagenes/nv.png" onClick="mypop=window.open('ccp-principal.php','',''); mypop.focus();" class="mgbt" title="Nueva Ventana">
                                <img src="imagenes/excel.png" title="Excel" v-on:click="downloadExl" class="mgbt">
                                <img src="imagenes/iratras.png" title="Atr&aacute;s" onClick="location.href='ccp-ejecucionpresupuestal.php'" class="mgbt">
                            </td>
                        </tr>
                    </table>
                </nav>
                <article>
                    <table class="inicio ancho">
                        <tr>
                            <td class="titulos" colspan="10" style="text-align:center; font-weight: 600;">.: Ejecuci&oacute;n presupuestal de inversi&oacute;n</td>
                            <td class="cerrar" style="width:7%" onClick="location.href='ccp-principal.php'">Cerrar</td>
                        </tr>
                        <tr>
                            <td style = "width: 10%;">
                                <label class="labelR">Dependencia</label>
                            </td>
                            <td style = "width: 12%;">
                                <select style = "width: 98.5%;" v-model="selectSeccion" v-on:change="cambiaCriteriosBusqueda">
                                    <option value="-1">Todas</option>
                                    <option v-for="seccion in seccionesPresupuestales" v-bind:value="seccion[0]">
                                        {{ seccion[0] }} - {{ seccion[1] }}
                                    </option>
                                </select>
                            </td>
                            <td style = "width: 10%;">
                                <label class="labelR">CSF/SSF:</label>
                            </td>
                            <td style = "width: 12%;">
                                <select style = "width: 98.5%;" v-model="selectMedioPago" v-on:change="cambiaCriteriosBusqueda">
                                    <option v-for="option in optionsMediosPagos" v-bind:value="option.value">
                                        {{ option.text }}
                                    </option>
                                </select>
                            </td>
                            <td style = "width: 8%;">
                                <label class="labelR">Vig. gasto:</label>
                            </td>
                            <td style = "width: 12%;">
                                <select style = "width: 98.5%;" v-model="selectVigenciaGasto" v-on:change="cambiaCriteriosBusqueda">
                                    <option value="-1">Todas</option>
                                    <option v-for="vigenciaDeGasto in vigenciasdelgasto" v-bind:value="vigenciaDeGasto[0]">
                                        {{ vigenciaDeGasto[1] }} - {{ vigenciaDeGasto[2] }}
                                    </option>
                                </select>
                            </td>
                            <td style = "width: 8%;">
                                <label class="labelR" for="">Fuente:</label>
                            </td>
                            <td colspan="2">
                                <div>
                                    <multiselect
                                        v-model="valueFuentes"
                                        placeholder="Seleccione una o varias fuentes"
                                        label="fuente" track-by="fuente"
                                        :custom-label="fuenteConNombre"
                                        :options="optionsFuentes"
                                        :multiple="true"
                                        :taggable="false"
                                        @input = "cambiaCriteriosBusqueda"
                                    ></multiselect>
                                </div>
                                <!-- <select style="width:90%" v-model="vigencia" v-on:Change="cambiaCriteriosBusqueda">
                                    <option v-for="year in years" :value="year[0]">{{ year[0] }}</option>
                                </select> -->
                            </td>

                        </tr>


                        <tr>
                            <td>
                                <label class="labelR" for="">Sector:</label>
                            </td>
                            <td>
                                <div>
                                    <multiselect
                                        v-model="valueSectores"
                                        placeholder="Sel sector"
                                        label="sector" track-by="sector"
                                        :custom-label="sectorConNombre"
                                        :options="optionsSectores"
                                        :multiple="true"
                                        :taggable="false"
                                        @input = "changeSector"
                                    ></multiselect>
                                </div>
                            </td>
                            <td>
                                <label class="labelR" for="">Programa:</label>
                            </td>
                            <td>
                                <div>
                                    <multiselect
                                        v-model="valueProgramas"
                                        placeholder="Sel programa"
                                        label="programa" track-by="programa"
                                        :custom-label="programaConNombre"
                                        :options="optionsProgramas"
                                        :multiple="true"
                                        :taggable="false"
                                        @input = "changePrograma"
                                    ></multiselect>
                                </div>
                            </td>
                            <td>
                                <label class="labelR" for="">Subprograma:</label>
                            </td>
                            <td>
                                <div>
                                    <multiselect
                                        v-model="valueSubProgramas"
                                        placeholder="Sel subprogramas"
                                        label="codigo" track-by="codigo"
                                        :custom-label="subProgramaConNombre"
                                        :options="optionsSubProgramas"
                                        :multiple="true"
                                        :taggable="false"
                                        @input = "changeSubPrograma"
                                    ></multiselect>
                                </div>
                            </td>
                            <td>
                                <label class="labelR" for="">Producto:</label>
                            </td>
                            <td>
                                <div>
                                    <multiselect
                                        v-model="valueProductos"
                                        placeholder="Sel productos"
                                        label="producto" track-by="producto"
                                        :custom-label="productoConNombre"
                                        :options="optionsProductos"
                                        :multiple="true"
                                        :taggable="false"
                                        @input = "changeProducto"
                                    ></multiselect>
                                </div>
                            </td>

                        </tr>


                        <tr>
                            <td>
                                <label class="labelR">
                                    Fecha inicial:
                                </label>
                            </td>
                            <td>
                                <input type="text" style = "width: 98.5%;" name="fechaini" value="<?php echo $_POST['fechaini']?>" onKeyUp="return tabular(event,this)" id="fc_1198971545" title="DD/MM/YYYY" onDblClick="displayCalendarFor('fc_1198971545');" class="colordobleclik" autocomplete="off" onChange="" readonly>
                            </td>
                            <td>
                                <label class="labelR">
                                    Fecha final:
                                </label>
                            </td>
                            <td>
                                <input type="text" style = "width: 98.5%;" name="fechafin" value="<?php echo $_POST['fechafin']?>" onKeyUp="return tabular(event,this)" id="fc_1198971546" title="DD/MM/YYYY" onDblClick="displayCalendarFor('fc_1198971546');" class="colordobleclik" autocomplete="off" onChange="" readonly>
                            </td>

                            <td>
                                <label class="labelR">Nivel:</label>
                            </td>
                            <td>
                                <select style = "width: 98.5%;" v-model="selectNivel" v-on:change="cambiaCriteriosBusqueda">
                                    <option v-for="nivel in niveles" v-bind:value="nivel.value">
                                        {{ nivel.text }}
                                    </option>
                                </select>
                            </td>

                            <td>
                                <button type="button" class="botonflechaverde" v-on:click="generarEjecucion">Generar</button>
                            </td>
                        </tr>
                    </table>
                    <div class='subpantalla estilos-scroll' v-show="mostrarEjecucion" style='height:50vh; margin-top:0px; resize: vertical;'>
                        <table class='tabla-ejecucion' id="tableId">
                            <thead>
                                <tr style="text-align:Center;">
                                    <th class="titulosnew00" style="width:2%;">Vig. gasto</th>
                                    <th class="titulosnew00" style="width:2%;">Dependencia</th>
                                    <th class="titulosnew00" style="width:5%;">Rubro</th>
                                    <th class="titulosnew00">Nombre</th>
                                    <th class="titulosnew00" style="width:2%;">Tipo</th>
                                    <th class="titulosnew00" style="width:4%;">Fuente</th>
                                    <th class="titulosnew00" style="width:8%;">Nombre Fuente</th>
                                    <th class="titulosnew00" style="width:3%;">CSF</th>
                                    <th class="titulosnew00" style="width:6%;">Inicial</th>
                                    <th class="titulosnew00" style="width:6%;">Adici&oacute;n</th>
                                    <th class="titulosnew00" style="width:6%;">Reducci&oacute;n</th>
                                    <th class="titulosnew00" style="width:6%;">Credito</th>
                                    <th class="titulosnew00" style="width:6%;">Contracredito</th>
                                    <th class="titulosnew00" style="width:6%;">Definitivo</th>
                                    <th class="titulosnew00" style="width:6%;">Disponibilidad</th>
                                    <th class="titulosnew00" style="width:6%;">Compromisos</th>
                                    <th class="titulosnew00" style="width:6%;">Oblicaci&oacute;n</th>
                                    <th class="titulosnew00" style="width:6%;">Egreso</th>
                                    <th class="titulosnew00" style="width:6%;">Saldo</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr v-for="(row,index) in sumaTotalGastosInversion" :key="index" v-bind:class="[ 'contenidonew01', 'sumTotalFuncionamiento', 'agregado']">
                                    <td style="width:2%;">{{ row[0] }}</td>
                                    <td style="width:2%;">{{ row[1] }}</td>
                                    <td style="width:5%;">{{ row[2] }}</td>
                                    <td>{{ row[3] }}</td>
                                    <td style="width:2%;">{{ row[4] }}</td>
                                    <td style="width:4%;">{{ row[5] }}</td>
                                    <td style="width:8%;"></td>
                                    <td style="width:3%;">{{ row[6] }}</td>
                                    <td style="width:6%; text-align: right;">{{ formatonumero(row[7]) }}</td>
                                    <td style="width:6%; text-align: right;">{{ formatonumero(row[8]) }}</td>
                                    <td style="width:6%; text-align: right;">{{ formatonumero(row[9]) }}</td>
                                    <td style="width:6%; text-align: right;">{{ formatonumero(row[10]) }}</td>
                                    <td style="width:6%; text-align: right;">{{ formatonumero(row[11]) }}</td>
                                    <td style="width:6%; text-align: right;">{{ formatonumero(row[12]) }}</td>
                                    <td style="width:6%; text-align: right;">{{ formatonumero(row[13]) }}</td>
                                    <td style="width:6%; text-align: right;">{{ formatonumero(row[14]) }}</td>
                                    <td style="width:6%; text-align: right;">{{ formatonumero(row[15]) }}</td>
                                    <td style="width:6%; text-align: right;">{{ formatonumero(row[16]) }}</td>
                                    <td style="width:6%; text-align: right;">{{ formatonumero(row[17]) }}</td>

                                    <input type='hidden' name='vigencia_gastoInv[]' v-model="row[0]">
                                    <input type='hidden' name='sec_presupuestalInv[]' v-model="row[1]">
                                    <input type='hidden' name='rubroInv[]' v-model="row[2]">
                                    <input type='hidden' name='nombreRubroInv[]' v-model="row[3]">
                                    <input type='hidden' name='tipoInv[]' v-model="row[4]">
                                    <input type='hidden' name='fuenteInv[]' v-model="row[5]">
                                    <!-- <input type='hidden' name='nombreFuente[]' v-model=""> -->
                                    <input type='hidden' name='csfInv[]' v-model="row[6]">
                                    <input type='hidden' name='inicialInv[]' v-model="row[7]">
                                    <input type='hidden' name='adicionInv[]' v-model="row[8]">
                                    <input type='hidden' name='reduccionInv[]' v-model="row[9]">
                                    <input type='hidden' name='creditoInv[]' v-model="row[10]">
                                    <input type='hidden' name='contraCreditoInv[]' v-model="row[11]">
                                    <input type='hidden' name='definitivoInv[]' v-model="row[12]">
                                    <input type='hidden' name='disponibilidadInv[]' v-model="row[13]">
                                    <input type='hidden' name='compromisoInv[]' v-model="row[14]">
                                    <input type='hidden' name='obligacionInv[]' v-model="row[15]">
                                    <input type='hidden' name='egresoInv[]' v-model="row[16]">
                                    <input type='hidden' name='saldoInv[]' v-model="row[17]">

                                </tr>

                                <tr v-for="(row,index) in arbol" :key="index" v-bind:class="[row[2] === 'C' ? 'captura' : 'agregado' , Math.round(row[17]) < 0 ? 'colorFila1' : (Math.trunc(row[13]) < Math.trunc(row[14]) ? 'colorFila1' : (Math.trunc(row[14]) < Math.trunc(row[15]) ? 'colorFila1' : (Math.trunc(row[15]) < Math.trunc(row[16]) ? 'colorFila1' : (index % 2 ? 'contenidonew00' : 'contenidonew01'))))]">
                                    <td style="width:2%;">{{ row[5] }}</td>
                                    <td style="width:2%;">{{ row[4] }}</td>
                                    <td style="width:5%;">{{ row[0] }}</td>
                                    <td>{{ row[1] }}</td>
                                    <td style="width:2%;">{{ row[2] }}</td>
                                    <td style="width:4%;">{{ row[3] }}</td>
                                    <td style="width:8%;">{{ buscarFuente(row[3]) }}</td>
                                    <td style="width:3%;">{{ row[6] }}</td>
                                    <td style="width:6%; text-align: right;">{{ formatonumero(row[7]) }}</td>
                                    <td style="width:6%; text-align: right;">{{ formatonumero(row[8]) }}</td>
                                    <td style="width:6%; text-align: right;">{{ formatonumero(row[9]) }}</td>
                                    <td style="width:6%; text-align: right;">{{ formatonumero(row[10]) }}</td>
                                    <td style="width:6%; text-align: right;">{{ formatonumero(row[11]) }}</td>
                                    <td style="width:6%; text-align: right;">{{ formatonumero(row[12]) }}</td>
                                    <td style="width:6%; text-align: right;" v-on:dblclick="auxiliarPorComprobante('disponibilidad', row[3], row[0])" v-bind:class="row[2] === 'C' ? 'dobleclickCelda' : ''" title="Doble click, genera auxiliar">{{ formatonumero(row[13]) }}</td>
                                    <td style="width:6%; text-align: right;" v-on:dblclick="auxiliarPorComprobante('compromiso', row[3], row[0])" v-bind:class="row[2] === 'C' ? 'dobleclickCelda' : ''" title="Doble click, genera auxiliar">{{ formatonumero(row[14]) }}</td>
                                    <td style="width:6%; text-align: right;" v-on:dblclick="auxiliarPorComprobante('obligacion', row[3], row[0])" v-bind:class="row[2] === 'C' ? 'dobleclickCelda' : ''" title="Doble click, genera auxiliar">{{ formatonumero(row[15]) }}</td>
                                    <td style="width:6%; text-align: right;" v-on:dblclick="auxiliarPorComprobante('egreso', row[3], row[0])" v-bind:class="row[2] === 'C' ? 'dobleclickCelda' : ''" title="Doble click, genera auxiliar">{{ formatonumero(row[16]) }}</td>
                                    <td style="width:6%; text-align: right;">{{ formatonumero(row[17]) }}</td>

                                    <input type='hidden' name='vigencia_gasto[]' v-model="row[5]">
                                    <input type='hidden' name='sec_presupuestal[]' v-model="row[4]">
                                    <input type='hidden' name='rubro[]' v-model="row[0]">
                                    <input type='hidden' name='nombreRubro[]' v-model="row[1]">
                                    <input type='hidden' name='tipo[]' v-model="row[2]">
                                    <input type='hidden' name='fuente[]' v-model="row[3]">
                                    <input type='hidden' name='nombreFuente[]' v-model="buscarFuente(row[3])">
                                    <input type='hidden' name='csf[]' v-model="row[6]">
                                    <input type='hidden' name='inicial[]' v-model="row[7]">
                                    <input type='hidden' name='adicion[]' v-model="row[8]">
                                    <input type='hidden' name='reduccion[]' v-model="row[9]">
                                    <input type='hidden' name='credito[]' v-model="row[10]">
                                    <input type='hidden' name='contraCredito[]' v-model="row[11]">
                                    <input type='hidden' name='definitivo[]' v-model="row[12]">
                                    <input type='hidden' name='disponibilidad[]' v-model="row[13]">
                                    <input type='hidden' name='compromiso[]' v-model="row[14]">
                                    <input type='hidden' name='obligacion[]' v-model="row[15]">
                                    <input type='hidden' name='egreso[]' v-model="row[16]">
                                    <input type='hidden' name='saldo[]' v-model="row[17]">
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div id="cargando" v-if="loading" class="loading">
                        <span>Cargando...</span>
                    </div>
                </article>
                <!-- <button @click = "downloadExl"> Exportar </button> -->
            </section>
        </form>

        <script src="https://cdn.jsdelivr.net/npm/vue@2.5.16/dist/vue.js"></script>
        <script src="https://unpkg.com/vue-multiselect@2.1.0"></script>
        <!-- <link rel="stylesheet" href="https://unpkg.com/vue-multiselect@2.1.0/dist/vue-multiselect.min.css"> -->
        <link rel="stylesheet" href="multiselect.css">

        <!-- <script src="Librerias/vue/vue.min.js"></script> -->
        <script src="Librerias/vue/axios.min.js"></script>
		<script src="xlsx/dist/xlsx.min.js"></script>
		<script src="file-saver/dist/FileSaver.min.js"></script>
		<script src="vue/presupuesto_ccp/ccp-ejecucionpresupuestal-inv.js"></script>
        <script src="sweetalert2/dist/sweetalert2.min.js"></script>
        <link rel="stylesheet" href="sweetalert2/dist/sweetalert2.min.css">




    </body>
</html>
