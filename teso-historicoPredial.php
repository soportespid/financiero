<?php

    header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require 'comun.inc';
	require 'funciones.inc';
	session_start();
    if(empty($_SESSION)){
        header("location: index.php");
    }
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html lang=es>
	<head>
    <meta charset="utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1"/>
		<meta name="viewport" content="user-scalable=no">
		<title>:: IDEAL 10 - Tesorería</title>
		<link href="favicon.ico" rel="shortcut icon"/>
		<link href="css/css2z.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
        <link href="css/tabs.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
        <link href="css/style.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" href="sweetalert2/dist/sweetalert2.min.css">
        <script type="text/javascript" src="css/programas.js"></script>
        <script src="sweetalert2/dist/sweetalert2.min.js"></script>
		<script src="vue/vue.min.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script src="css/calendario.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
	</head>
	<body>
		<header>
			<table>
				<tr><script>barra_imagenes("teso");</script><?php cuadro_titulos();?></tr>
			</table>
		</header>

		<form name="form2" method="post" action="">
			<section id="myapp" v-cloak >
                <div id="cargando" v-show="isLoading" class="loading" style="z-index: 10000;">
                    <span>Cargando...</span>
                </div>
				<nav>
					<table>
						<tr><?php menu_desplegable("teso");?></tr>
                        <tr>
							<td colspan="3" class="cinta">
                                <img src="imagenes/add2.png"  class="mgbt" title="Nuevo">
								<img src="imagenes/guardad.png"   title="Guardar"  class="mgbt">
								<img src="imagenes/buscad.png"   class="mgbt" title="Buscar">
								<img src="imagenes/nv.png" @click="mypop=window.open('teso-principal.php','',''); mypop.focus();" class="mgbt" title="Nueva Ventana">
                                <a @click="mypop=window.open('teso-historicoPredial.php','','');mypop.focus();" class="tooltip bottom mgbt"><img src="imagenes/duplicar_pantalla.png" title="Duplicar pesta&ntilde;a"></a>
                                <img src="imagenes/print.png" title="Imprimir" @click="printPDF()" class="mgbt">
                            </td>
						</tr>
					</table>
				</nav>
				<article>
                    <!--TABS-->
                    <div ref="rTabs" class="nav-tabs bg-white p-1">
                        <div class="nav-item active" @click="showTab(1)">Información general</div>
                        <div class="nav-item" @click="showTab(2)">Propietarios</div>
                    </div>
                     <!--CONTENIDO TABS-->
                     <div ref="rTabsContent" class="nav-tabs-content bg-white">
                        <div class="nav-content active">
                            <h2 class="titulos m-0">Buscar predio</h2>
                            <div class="form-control w-50">
                                <div class="d-flex">
                                    <input type="search" placeholder="Buscar por código catastral, documento o nombre" @keyup="search()" v-model="txtSearch">
                                </div>
                            </div>
                            <div>

                                <h2 class="titulos m-0">Información general:  </h2>
                                <div class="d-flex">
                                    <div class="form-control">
                                        <label class="form-label">Código catastral:</label>
                                        <input type="text"  style="text-align:center;" v-model="txtCodigo" disabled readonly>
                                    </div>
                                    <div class="form-control">
                                        <label class="form-label">Nro Documento:</label>
                                        <input type="text"  style="text-align:center;" v-model="txtDocumento" disabled readonly>
                                    </div>
                                    <div class="form-control">
                                        <label class="form-label">Nombre propietario:</label>
                                        <input type="text"  style="text-align:center;" v-model="txtNombre" disabled readonly>
                                    </div>
                                    <div class="form-control">
                                        <label class="form-label">Área del terreno:</label>
                                        <input type="text"  style="text-align:center;" v-model="txtArea" disabled readonly>
                                    </div>
                                    <div class="form-control">
                                        <label class="form-label">Área construida:</label>
                                        <input type="text"  style="text-align:center;" v-model="txtConstruida" disabled readonly>
                                    </div>
                                    <div class="form-control">
                                        <label class="form-label">Dirección:</label>
                                        <input type="text"  style="text-align:center;" v-model="txtDireccion" disabled readonly>
                                    </div>
                                </div>
                                <div class="d-flex">
                                    <div class="form-control">
                                        <label class="form-label">Total:</label>
                                        <input type="text"  style="text-align:center;" v-model="txtTotal" disabled readonly>
                                    </div>
                                    <div class="form-control">
                                        <label class="form-label">Orden:</label>
                                        <input type="text"  style="text-align:center;" v-model="txtOrden" disabled readonly>
                                    </div>
                                    <div class="form-control">
                                        <label class="form-label">Vigencia:</label>
                                        <input type="text"  style="text-align:center;" v-model="txtVigencia" disabled readonly>
                                    </div>
                                    <div class="form-control">
                                        <label class="form-label">Avaluo vigente:</label>
                                        <input type="text"  style="text-align:center;" v-model="txtAvaluo" disabled readonly>
                                    </div>
                                    <div class="form-control">
                                        <label class="form-label">Tipo predio:</label>
                                        <input type="text"  style="text-align:center;" v-model="txtTipo" disabled readonly>
                                    </div>
                                    <div class="form-control">
                                        <label class="form-label">Destino económico:</label>
                                        <input type="text"  style="text-align:center;" v-model="txtDestino" disabled readonly>
                                    </div>
                                </div>
                            </div>
                            <h2 class="titulos m-0">Histórico:  </h2>
                            <div class="overflow-auto max-vh-50 overflow-x-hidden p-2" >
                                <table v-show="arrData.length > 0" class="table fw-normal">
                                    <thead>
                                        <tr>
                                            <th>No Liquidación</th>
                                            <th>Fecha liquidación</th>
                                            <th>Recibo</th>
                                            <th>Fecha recibo</th>
                                            <th>Cuenta</th>
                                            <th>Periodo</th>
                                            <th>Avaluo</th>
                                            <th>Tasa x mil</th>
                                            <th>Predial</th>
                                            <th>Interés predial</th>
                                            <th>Bomberil</th>
                                            <th>Interés bomberil</th>
                                            <th>Ambiente</th>
                                            <th>Interés ambiente</th>
                                            <th>Descuentos</th>
                                            <th>Total liquidado</th>
                                            <th class="text-center">Estado</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr v-for="(data,index) in arrData" :key="index">
                                            <td>{{data.idpredial}}</td>
                                            <td>{{data.fechali}}</td>
                                            <td>{{data.estado == 'P' ? data.recibo.id_recibos : " - " }}</td>
                                            <td>{{data.estado == 'P' ? data.recibo.fecha : " - " }}</td>
                                            <td>{{data.estado == 'P' ? data.recibo.cuenta : " - " }}</td>
                                            <td>{{data.vigliquidada}}</td>
                                            <td>{{formatNumero(data.avaluo)}}</td>
                                            <td>{{data.tasav}}</td>
                                            <td>{{formatNumero(data.predial)}}</td>
                                            <td>{{formatNumero(data.intpredial)}}</td>
                                            <td>{{formatNumero(data.bomberil)}}</td>
                                            <td>{{formatNumero(data.intbomb)}}</td>
                                            <td>{{formatNumero(data.medioambiente)}}</td>
                                            <td>{{formatNumero(data.intmedioambiente)}}</td>
                                            <td>{{formatNumero(data.descuentos)}}</td>
                                            <td>{{formatNumero(data.totaliquidavig)}}</td>
                                            <td class="text-center">
                                                <span class="badge badge-success" v-show="data.estado == 'P'">Pago</span>
                                                <span class="badge badge-danger" v-show="data.estado == 'N'">Anulado</span>
                                                <span class="badge badge-warning" v-show="data.estado == 'S'">Por pagar</span>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                                <table v-show="arrData.length == 0"class="table fw-normal">
                                    <tbody>
                                        <tr>
                                            <td class="fw-bolder text-center">No hay información =( </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="nav-content">
                            <table class="table fw-normal">
                                <thead>
                                    <tr>
                                        <th>Nombre</th>
                                        <th>Documento</th>
                                        <th class="text-center">Orden</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr v-for="(data,index) in arrPropietarios" :key="index">
                                        <td>{{data.nombre}}</td>
                                        <td>{{data.documento}}</td>
                                        <td class="text-center">{{data.orden}}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
				</article>
			</section>
		</form>

		<script src="node_modules/read-excel-file/bundle/read-excel-file.min.js"></script>
		<script src="node_modules/xlsx/dist/xlsx.full.min.js"></script>
        <script src="Librerias/vue/vue.min.js"></script>
		<script src="Librerias/vue/axios.min.js"></script>
		<script type="module" src="tesoreria/historico_predial/buscar/teso-historicoBuscar.js?<?= date('d_m_Y_h_i_s');?>"></script>

	</body>
</html>
