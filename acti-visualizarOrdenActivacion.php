<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require 'comun.inc';
	require 'funciones.inc';
	session_start();
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html lang=es>
	<head>
    <meta charset="utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1"/>
		<meta name="viewport" content="user-scalable=no">
		<title>:: IDEAL 10 - Activos fijos</title>
		<link href="favicon.ico" rel="shortcut icon"/>
		<link href="css/css2z.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
        <link href="css/tabs.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" href="sweetalert2/dist/sweetalert2.min.css">
        <script type="text/javascript" src="css/programas.js"></script>
        <script src="sweetalert2/dist/sweetalert2.min.js"></script>
		<script src="vue/vue.min.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script src="css/calendario.js?<?php echo date('d_m_Y_h_i_s');?>"></script>

		<style>
			input[type=number]::-webkit-inner-spin-button,
			input[type=number]::-webkit-outer-spin-button { 
				-webkit-appearance: none; 
				margin: 0; 
			}
			input[type=number]{
				text-align:right;
				box-sizing: border-box;
				font: 120% sans-serif;
				/* width: 100% !important; */
			}
			[v-cloak]{display : none;}

			.centrarSelect {

				height: 30px !important;
				text-align-last:center !important;
			}

			.aumentarTamaño {

				font-size:15px;
			}

            .tamano01 {
                text-align: center !important;
            }
            .checkbox-wrapper-31:hover .check {
                stroke-dashoffset: 0;
            }
        
            .checkbox-wrapper-31 {
                position: relative;
                display: inline-block;
                width: 40px;
                height: 40px;
            }
            .checkbox-wrapper-31 .background {
                fill: #ccc;
                transition: ease all 0.6s;
                -webkit-transition: ease all 0.6s;
            }
            .checkbox-wrapper-31 .stroke {
                fill: none;
                stroke: #fff;
                stroke-miterlimit: 10;
                stroke-width: 2px;
                stroke-dashoffset: 100;
                stroke-dasharray: 100;
                transition: ease all 0.6s;
                -webkit-transition: ease all 0.6s;
            }
            .checkbox-wrapper-31 .check {
                fill: none;
                stroke: #fff;
                stroke-linecap: round;
                stroke-linejoin: round;
                stroke-width: 2px;
                stroke-dashoffset: 22;
                stroke-dasharray: 22;
                transition: ease all 0.6s;
                -webkit-transition: ease all 0.6s;
            }
            .checkbox-wrapper-31 input[type=checkbox] {
                position: absolute;
                width: 100%;
                height: 100%;
                left: 0;
                top: 0;
                margin: 0;
                opacity: 0;
                -appearance: none;
            }
            .checkbox-wrapper-31 input[type=checkbox]:hover {
                cursor: pointer;
            }
            .checkbox-wrapper-31 input[type=checkbox]:checked + svg .background {
                fill: #6cbe45;
            }
            .checkbox-wrapper-31 input[type=checkbox]:checked + svg .stroke {
                stroke-dashoffset: 0;
            }
            .checkbox-wrapper-31 input[type=checkbox]:checked + svg .check {
                stroke-dashoffset: 0;
            }

            SELECT{
                font-size:11px;
                font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, "Helvetica Neue", Arial, "Noto Sans", sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol", "Noto Color Emoji";
                font-weight:bold;
                outline:none;
                border-radius:3px;
                border:1px solid rgba(0,0,0, 0.2);
                color:222;
                /* background-color:#B2BFD9; */
            }
            option{
                font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, "Helvetica Neue", Arial, "Noto Sans", sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol", "Noto Color Emoji"; 
                font-size: 10px; 
                color: 333;
                /* background-color:#eee; */
                color:#222;
            }
		</style>
	</head>
	<body>
		<header>
			<table>
				<tr><script>barra_imagenes("acti");</script><?php cuadro_titulos();?></tr>
			</table>
		</header>

		<form name="form2" method="post" action="">
			<section id="myapp" v-cloak >
				<nav>
					<table>
						<tr><?php menu_desplegable("acti");?></tr>
						<tr>
							<td colspan="3" class="cinta">
								<img src="imagenes/add.png" v-on:click="location.href='acti-ordenActivacion'" class="mgbt" title="Nuevo">
								<img src="imagenes/guardad.png" title="Guardar" class="mgbt1">
								<img src="imagenes/busca.png" v-on:click="location.href='acti-buscarOrdenesActivacion'" class="mgbt" title="Buscar">
								<img src="imagenes/nv.png" onClick="mypop=window.open('acti-principal','',''); mypop.focus();" class="mgbt" title="Nueva Ventana">
                                <a href="acti-buscarOrdenesActivacion"><img src="imagenes/iratras.png" class="mgbt" alt="Atrás"></a>
							</td>
						</tr>
					</table>
				</nav>

				<article>
                    <div>
                        <table class="inicio">
                            <tr>
                                <td class="titulos" colspan="12">.: Visualizar orden de activación</td>
                            </tr>
                            
                            <tr>
                                <td class="textonew01" style="width:3.5cm;">.: Orden de activación:</td>
                                <td>
                                    <input type="text" v-model="orden" style="text-align: center;" readonly>
                                </td>

                                <td class="textonew01" style="width:2.5cm;">.: Origen:</td>
                                <td>
                                    <input type="text" v-model="origen" style="text-align: center;" readonly>
                                </td>

                                <td class="textonew01" style="width:2.5cm;">.: Documento:</td>
                                <td>
                                    <input type="text" v-model="documento" style="text-align: center;" readonly>
                                </td>

                                <td class="textonew01" style="width:2.5cm;">.: Fecha:</td>
                                <td>
                                    <input type="text" v-model="fechaRegistro" style="text-align: center;" readonly>
                                </td>

                                <td class="textonew01" style="width:2.5cm;">.: Valor: </td>
                                <td>
                                    <input type="text" v-model="formatonumero(valor)" style="text-align: right;" readonly>
                                </td>
                            </tr>
                        </table>

                        <div class='subpantalla' style='height:54vh; width:100%; margin-top:0px;'>
                            <table class=''>        
                                <thead>
                                    <tr>
                                        <th class="titulosnew00">Placa</th>
                                        <th class="titulosnew00">Nombre</th>
                                        <th class="titulosnew00">Clase</th>
                                        <th class="titulosnew00">Grupo</th>
                                        <th class="titulosnew00">Tipo</th>
                                        <th class="titulosnew00">Prototipo</th>
                                        <th class="titulosnew00">Dependencia</th>
                                        <th class="titulosnew00">Ubicación</th>
                                        <th class="titulosnew00">Centro costo</th>
                                        <th class="titulosnew00">Disposición</th>
                                        <th class="titulosnew00">Valor</th>
                                        <th class="titulosnew00">Referencia</th>
                                        <th class="titulosnew00">Modelo</th>
                                        <th class="titulosnew00">Serial</th>
                                        <th class="titulosnew00">Estado</th>
                                        <th class="titulosnew00">Fecha compra</th>
                                        <th class="titulosnew00">Fecha activación</th>
                                        <th class="titulosnew00">Unidad de medida</th>
                                        <th class="titulosnew00">Depreciación en bloque</th>
                                        <th class="titulosnew00">Depreciación individual</th>
                                        <th class="titulosnew00">Foto</th>
                                        <th class="titulosnew00">Ficha tecnica</th>
                                    </tr>
                                </thead>

                                <tbody>
                                    <tr v-for="(activo,index) in activos" v-bind:class="(index % 2 ? 'saludo1a' : 'saludo2')" style="font: 100% sans-serif;" >
                                        <td style="text-align:center;">{{ activo[0] }}</td>
                                        <td style="text-align:center;">{{ activo[1] }}</td>
                                        <td style="text-align:center;">{{ activo[2] }}</td>
                                        <td style="text-align:center;">{{ activo[3] }}</td>
                                        <td style="text-align:center;">{{ activo[4] }}</td>
                                        <td style="text-align:center;">{{ activo[5] }}</td>
                                        <td style="text-align:center;">{{ activo[6] }}</td>
                                        <td style="text-align:center;">{{ activo[7] }}</td>
                                        <td style="text-align:center;">{{ activo[8] }}</td>
                                        <td style="text-align:center;">{{ activo[9] }}</td>
                                        <td style="text-align:center;">{{ formatonumero(activo[10]) }}</td>
                                        <td style="text-align:center;">{{ activo[11] }}</td>
                                        <td style="text-align:center;">{{ activo[12] }}</td>
                                        <td style="text-align:center;">{{ activo[13] }}</td>
                                        <td style="text-align:center;">{{ activo[14] }}</td>
                                        <td style="text-align:center;">{{ activo[15] }}</td>
                                        <td style="text-align:center;">{{ activo[16] }}</td>
                                        <td style="text-align:center;">{{ activo[17] }}</td>
                                        <td style="text-align:center;">{{ activo[18] }}</td>
                                        <td style="text-align:center;">{{ activo[19] }}</td>
                                        <td style="text-align:center;">
                                            <a v-bind:href="activo[20]" target="_blank">
                                                <img v-bind:src="activo[22]" width="40px" height="40px">
                                            </a>
                                        </td>
                                        <td style="text-align:center;">
                                            <a v-bind:href="activo[21]" target="_blank">
                                                <img v-bind:src="activo[23]" width="40px" height="40px">
                                            </a>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>

					<div id="cargando" v-if="loading" class="loading">
						<span>Cargando...</span>
					</div>

				</article>
			</section>
		</form>
        
		<script src="node_modules/read-excel-file/bundle/read-excel-file.min.js"></script>
		<script src="node_modules/xlsx/dist/xlsx.full.min.js"></script>
        <script src="Librerias/vue/vue.min.js"></script>
		<script src="Librerias/vue/axios.min.js"></script>
		<script type="module" src="activos_fijos/ordenActivacion/visualizar/acti-visualizarOrdenActivacion.js"></script>
        
	</body>
</html>