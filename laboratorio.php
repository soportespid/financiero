<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");

	date_default_timezone_set("America/Bogota");
?>
<html lang="es">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1">
		<meta name="viewport" content="user-scalable=no">
		<title>:: Spid - Tesoreria</title>
		<link href="css/css2.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<script type="text/javascript" src="css/programas.js"></script>
		<script>
			function encripta(){
				document.form2.oculto.value='2';
				document.form2.submit();
			}
			function desencripta(){
				document.form2.oculto.value='3';
				document.form2.submit();
			}
		</script>

	</head>
	<body>
	
		<table>
			
		</table>
		<form name="form2" method="post" action="">
			<?php
				if($_POST['oculto']== '2'){
					$_POST['varfin'] = base64_encode($_POST['varini']);
				}
				if($_POST['oculto']== '3'){
					$_POST['varfin2'] = base64_decode($_POST['varini2']);
				}
			?>
			<table class="inicio">
				<tr>
					<td class="titulos" colspan="7">ENCRIPTAR</td>
					<td class="cerrar" style="width:7%" onClick="location.href='teso-principal.php'">Cerrar</td>
				</tr>
				<tr>
					<td class="saludo1" style="width:2.5cm;">.: Inicio:</td>
					<td style="width:50%;"><input type="text" name="varini" id="varini" value="<?php echo $_POST['varini']?>" style="width:100%" onchange="encripta()"/></td>
					<td></td>
				</tr>
				<tr>
					<td class="saludo1" style="width:2.5cm;">.: Fin:</td>
					<td style="width:50%;"><input type="text" name="varfin" id="varfin" value="<?php echo $_POST['varfin']?>" style="width:100%"/></td>
					<td></td>
				</tr>
				<tr><td class="titulos" colspan="7">DESENCRIPTAR</td></tr>
				<tr>
					<td class="saludo1" style="width:2.5cm;">.: Inicio:</td>
					<td style="width:50%;"><input type="text" name="varini2" id="varini2" value="<?php echo $_POST['varini2']?>" style="width:100%" onchange="desencripta()"/></td>
					<td></td>
				</tr>
				<tr>
					<td class="saludo1" style="width:2.5cm;">.: Fin:</td>
					<td style="width:50%;"><input type="text" name="varfin2" id="varfin2" value="<?php echo $_POST['varfin2']?>" style="width:100%"/></td>
					<td></td>
				</tr>
			</table>
			<input type="hidden" name="oculto" id="oculto" value="1"/>
			
		</form>
	</body>
</html>