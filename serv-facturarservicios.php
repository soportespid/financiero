<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=iso-8859-1");
	require 'comun.inc';
	require 'funciones.inc';
	require 'funcionessp.inc';
	require 'conversor.php';
	require 'validaciones.inc';
	session_start();
	$linkbd=conectar_v7();
	cargarcodigopag($_GET[codpag],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
	error_reporting(E_ALL);
	ini_set('display_errors', '1');
?>
<!DOCTYPE >
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es">
	<head>
		<meta http-equiv="Content-type" content="text/html; charset=iso-8859-1"/>
		<meta http-equiv="X-UA-Compatible" content="IE=9"/>
		<title>:: SPID - Servicios P&uacute;blicos</title>
		<link href="css/css2.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<script type="text/javascript" src="css/calendario.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script type="text/javascript" src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script>
			function procesos($tip)
			{
				switch ($tip)
				{
					case 1:	if (document.form2.liquigen.value!='')
							{
									document.form2.oculto.value='1';
									document.form2.submit();
							}
							else{despliegamodalm('visible','2','Faltan seleccionar un periodo de facturaci�n para cerrar');}
							break;
					case 2:	despliegamodalm('visible','4','Se generar cierre de facturaci�n de este periodo, �Esta Seguro??','1');
							break;
				}
 			}
			function despliegamodalm(_valor,_tip,mensa,pregunta)
			{
				document.getElementById("bgventanamodalm").style.visibility=_valor;
				if(_valor=="hidden"){document.getElementById('ventanam').src="";}
				else
				{
					switch(_tip)
					{
						case "1":	document.getElementById('ventanam').src="ventana-mensaje1.php?titulos="+mensa;break;
						case "2":	document.getElementById('ventanam').src="ventana-mensaje3.php?titulos="+mensa;break;
						case "3":	document.getElementById('ventanam').src="ventana-mensaje2.php?titulos="+mensa;break;
						case "4":	document.getElementById('ventanam').src="ventana-consulta1.php?titulos="+mensa+"&idresp="+pregunta;break;
					}
				}
			}
			function funcionmensaje(){}
			function respuestaconsulta(pregunta)
			{
				switch(pregunta)
				{
					case "1":	document.form2.oculto.value=3;
  								document.form2.submit();break;
				}
			}
			function callprogress(vValor)
			{
				document.getElementById("getprogress").innerHTML = vValor;
				document.getElementById("getProgressBarFill").innerHTML = '<div class="ProgressBarFill" style="width: '+vValor+'%;"></div>';
				document.getElementById("botcerrar").style.display='none';
				document.getElementById("titulog1").style.display='block';
				document.getElementById("progreso").style.display='block';
				document.getElementById("getProgressBarFill").style.display='block';
				if (vValor==100){document.getElementById("titulog2").style.display='block';}
			}
			function validar() {document.form2.submit();}
			function pdf()
			{
				document.form2.action="serv-facturagrupal.php";
				document.form2.target="_BLANK";
				document.form2.submit(); 
				document.form2.action="";
				document.form2.target="";
			}
		</script>
		<?php 
			titlepag(); 
			if (@$_POST['oculto']==""){$_POST['menuact']=$_GET['menu'];}
			if (@$_POST['menuact']=="SI")
			{$boatras="<img src='imagenes/iratras.png' title='Men&uacute; Nomina' onClick=\"location.href='serv-menufacturacion.php'\" class='mgbt'>";}
			else {$boatras="";}
		?>
	</head>
	<body>
		<IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
		<span id="todastablas2"></span>
		<table>
			<tr><script>barra_imagenes("serv");</script><?php cuadro_titulos();?></tr>	 
			<tr><?php menu_desplegable("serv");?></tr>
			<tr>
				<td colspan="3" class="cinta"><img src="imagenes/add.png" title="Nuevo" onClick="location.href='serv-facturarservicios.php'" class="mgbt"/><img src="imagenes/guarda.png" title="Guardar" onClick="guardar()" class="mgbt"/><img src="imagenes/busca.png" title="Buscar" onClick="location.href='serv-buscaliquidarservicios.php'" class="mgbt"/><img src="imagenes/nv.png" title="Nueva Ventana" class="mgbt" onClick="mypop=window.open('serv-principal.php','','');mypop.focus();"/><img src="imagenes/print.png" title="Buscar" onClick="pdf()" class="mgbt"/><?php echo $boatras;?></td>
			</tr>
		</table>
		<div id="bgventanamodalm" class="bgventanamodalm">
			<div id="ventanamodalm" class="ventanamodalm">
				<IFRAME src="" name="ventanam" marginWidth=0 marginHeight=0 frameBorder=0 id="ventanam" frameSpacing=0 style=" width:700px; height:130px; top:200; overflow:hidden;"></IFRAME>
			</div>
		</div>
		<form name="form2" method="post" action="">
			<?php
				//*** cargar parametros de liquidacion
				$sqlr="SELECT * FROM servparametros";
				$resp = mysqli_query($linkbd,$sqlr);
				while ($row =mysqli_fetch_row($resp)) 
				{
					$_POST['tipo']=$row[1];
					$_POST['rangofact']=$row[2];
					$_POST['diacorte']=$row[3];
					$_POST['cargof']=$row[4];
					$_POST['diaplazo']=$row[6];
					$_POST['obs']=$row[7];
				}
				$_POST['nomarchivo']="facturacion/".@$_POST['vigencias'].@$_POST['periodo'].@$_POST['periodo2'].@$_POST['servicios'];
				$vigusu=vigencia_usuarios($_SESSION['cedulausu']);
				$vact=$vigusu;
				$sqlr="SELECT * FROM servservicios";
				$res=mysqli_query($linkbd,$sqlr);
				while ($row =mysqli_fetch_row($res))
				{
					echo "<input type='hidden' name='servcods[]' value='$row[0]'>";
					echo "<input type='hidden' name='servnoms[]' value='$row[1]'>";
				}
			?>
			<table class="inicio">
				<tr>
					<td class="titulos" colspan="13">Facturacion Servicios</td>
					<td  class="cerrar" ><a href="serv-principal.php">Cerrar</a></td>
				</tr>
				<input type="hidden" name="obs" value="<?php echo @$_POST['obs']; ?>"/>
				<input type="hidden" name="diaplazo" value="<?php echo @$_POST['diaplazo']; ?>"/>
				<tr>
					<td class="saludo1" style="width:4cm;">Liquidaci&oacute;n Generada</td>
					<td> 
						<select name="liquigen" id="liquigen" onChange="validar()" >
							<option value="">Sel..</option>
							<?php
								$sqlr="SELECT * FROM servliquidaciones_gen WHERE estado='L' ";
								$resp = mysqli_query($linkbd,$sqlr);
								while ($row =mysqli_fetch_row($resp)) 
								{
									if($row[0]==@$_POST['liquigen'])
									{
										echo "<option value='$row[0]' SELECTED>$row[0]</option>";
										$_POST['vigencias']=$row[1];
										$_POST['periodo']=$row[2];
										$_POST['periodo2']=$row[3];
										$_POST['servicios']=$row[5];
									}
									else {echo "<option value='$row[0]'>$row[0]</option>";}
								}
							?>
						</select>
					</td>
					<td><input name="buscapredios" type="button" value=" Ver Liquidacion " onClick="procesos(1)"> </td>
				</tr>
			</table>
			<input type="hidden" name="oculto" id="oculto" value="<?php echo @$_POST['oculto'] ?>">
			<input type="hidden" name="facturacion" value="<?php echo @$_POST['facturacion'] ?>">
			<input type="hidden" name="nomarchivo" value="<?php echo @$_POST['nomarchivo']?>">
			<input type="hidden" name="menuact" id="menuact" value="<?php echo @$_POST['menuact'];?>"/>
			<?php
				if(@$_POST['oculto']>=1)
				{
					if(@$_POST['oculto']==1)
					{
							$sqlr="SELECT vigencia,mes,mesfin,fecha,servicio,numinicial,numfinal FROM servliquidaciones_gen WHERE id_cab = '".$_POST['liquigen']."'";
							$resp=mysqli_query($linkbd,$sqlr);
							while ($row =mysqli_fetch_row($resp))
							{
								$_POST['oculto']=2;
								echo"<script>document.getElementById('oculto').value=2;</script>";
								$_POST['vigencias']="$row[0]";
								$_POST['periodo']="$row[1]";
								$_POST['periodo2']="$row[2]";
								$_POST['fecha']="$row[3]";
								$_POST['servicios']=="$row[4]";
								$_POST['facturaini']="$row[5]";
								$_POST['facturafin']="$row[6]";
								if($row[4]==""){$_POST['nomservicio']="TODOS";}
								else
								{
									$sqlrsv="SELECT nombre FROM servservicios WHERE codigo='$row[5]'";
									$resv=mysqli_query($linkbd,$sqlrsv);
									$rowsv =mysqli_fetch_row($resv); 
									$_POST['nomservicio']="$rowsv[0]";
								}
									 
							}	
							$_POST['facturatot']=(float)$_POST['facturafin']-(float)$_POST['facturaini']+1;
					}
					echo"
						<input type='hidden' name='facturaini' id='facturaini' value='".@$_POST['facturaini']."'/>
						<input type='hidden' name='facturafin' id='facturafin' value='".@$_POST['facturafin']."'/>
						<input type='hidden' name='facturatot' id='facturatot' value='".@$_POST['facturatot']."'/>
						<input type='hidden' name='fecha' id='fecha' value='".@$_POST['fecha']."'/>
						<input type='hidden' name='vigencias' id='vigencias' value='".@$_POST['vigencias']."'/>
						<input type='hidden' name='periodo' id='periodo' value='".@$_POST['periodo']."'/>
						<input type='hidden' name='periodo2' id='periodo2' value='".@$_POST['periodo2']."'/>
						<input type='hidden' name='servicios' id='servicios' value='".@$_POST['servicios']."'/>
						<input type='hidden' name='nomservicio' id='nomservicio' value='".@$_POST['nomservicio']."'/>
					";
					if(@$_POST['servicios']==""){$nombreser="Todos";}
					else {$nombreser=$_POST['nomservicio'];}
					echo"
					<table class='inicio'>
						<tr><td class='titulos' colspan='8'>Facturaci&oacute;n sin Corte</td></tr>
						<tr>
							<td class='titulos2' style='width:8%'>N&deg; Liquidaci&oacute;n</td>
							<td class='titulos2' style='width:8%'>Fecha</td>
							<td class='titulos2' style='width:6%'>Vigencia</td>
							<td class='titulos2' style='width:15%'>Periodos</td>
							<td class='titulos2' style='width:14%'>N&deg; Facturaci&oacute;n</td>
							<td class='titulos2' style='width:8%'>Total Facturas</td>
							<td class='titulos2' style='width:15%'>Sevicios</td>
							<td class='titulos2'>Proceso</td>
						</tr> 
						<tr class='saludo2'>
							<td>".$_POST['liquigen']."</td>
							<td>".$_POST['fecha']."</td>
							<td>".$_POST['vigencias']."</td>
							<td>".mesletras($_POST['periodo'])." - ".mesletras($_POST['periodo2'])."</td>
							<td>".$_POST['facturaini']." - ".$_POST['facturafin']."</td>
							<td>".$_POST['facturatot']."</td>
							<td>$nombreser</td>
							<td>
								<input type='button' name='botcerrar' id='botcerrar' value=' Crear Facturaci�n ' onClick='procesos(2)' style='display:block;'/>
								<div id='titulog1' style='display:none; float:left'></div>
								<div id='progreso' class='ProgressBar' style='display:none; float:left'>
									<div class='ProgressBarText'><span id='getprogress'></span>&nbsp;% </div>
									<div id='getProgressBarFill'></div>
								</div>
							</td>
						</tr>
					</table>";
				}?>
				<div class="subpantalla" style="height:40.5%; width:99.6%;"	>
				<?php
				if(@$_POST['oculto']==3)
				{
					ini_set('max_execution_time', 7200);
					$multimeses=($_POST['periodo2']-$_POST['periodo'])+1;
					$nuevo="";
					$actual="";
					$conf=1;
					$contador=0;
					$ngen=0;
					//***** numero de liquidaciones 
					$sqlr="SELECT MAX(id_factura) FROM servfacturas";
					$resp=mysqli_query($linkbd,$sqlr);
					while ($row =mysqli_fetch_row($resp)) 
					{
						$contador=$row[0];
						$facturaminima=$row[0];
						$_POST['factmin'] =$row[0];
					}
					$sqlr="SELECT * FROM servliquidaciones_gen WHERE id_cab='".$_POST['liquigen']."'";
					$resp=mysqli_query($linkbd,$sqlr);
					while ($row =mysqli_fetch_row($resp)){$ngen=$row[0];}
					$sqlr="SELECT * FROM servliquidaciones WHERE liquidaciongen='".$_POST['liquigen']."' AND estado='S' ORDER BY id_liquidacion";
					$respn=mysqli_query($linkbd,$sqlr);
					$c=1;
					$totalcli=mysqli_affected_rows ($linkbd);
					while ($rown =mysqli_fetch_row($respn)) 
					{
						$porcentaje = $c * 100 / $totalcli; 
						echo"<script>progres='".round($porcentaje)."';callprogress(progres);</script>"; 
						flush();
						@ob_flush();
						usleep(5);
						$c+=1;echo "$idliquidacion<br>";
						$idliquidacion=$rown[0];
						//Factura cabecera	
						$sqlr="INSERT INTO servfacturas (id_factura,vigencia,mes,mes2,codusuario,fecha,servicio,sistema,estado, liquidaciongen) VALUES (".($contador+1).",'".$_POST['vigencias']."','".$_POST['periodo']."', '".$_POST['periodo2']."','$rown[5]','".$_POST['fecha']."','".$_POST['servicios']."', '".$_SESSION['cedulausu']."','S',$ngen)";
						mysqli_query($linkbd,$sqlr);
						$sqlr="SELECT * FROM servliquidaciones_det WHERE id_liquidacion=$idliquidacion ORDER BY servicio";
						$resp=mysqli_query($linkbd,$sqlr);
						$saldoanterior=0;
						$intereses=0;
						while ($row =mysqli_fetch_row($resp)) 
						{
							$saldoanterior=$row[13];
							//***factura detalle
							//***datos
							/*
							$estratos=$row[5];	
							$servicios=$row[4];
							$subsidio=$row[9];
							$tarifa=array();
							$cargofijo=$row[7];
							if($row[10]!='' && $row[10]!=null){$descuentos=$row[10];}
							else{$descuentos=0;}
							$intereses=$rown[8];
							$servicio=buscar_servicio($row[4]);
							$actual=$rown[0];
							$codcat=buscar_codcatservicios($rown[5]);
							$tipoliqser=buscaservicio_liquida($row[4]);
							$nomter=buscar_nomtercerosp($rown[5]);
							$tarifa[1]=$row[7];
							$tarifa[0]=$row[8];	
							$contribucion=$row[11];
							$valorsub=$subsidio;
							$valortarcf=$tarifa[0]+$tarifa[1];
							$valorliq=($saldoanterior+$tarifa[0]-$valorsub+$tarifa[1]-$descuentos+$contribucion);
							$centrocosto=buscaservicio_cc($row[4]);
							$bardir=buscar_barriodirecc($rown[5]);
							$barrio=buscar_barrio($bardir[0]);
							$ruta=buscar_ruta($bardir[0]);
							$estrato=buscaestrato_servicios($row[5]);
							$uso=buscar_usosuelo($estrato);*/
							if($nuevo!=$actual )
							{
								$contador+=1;
								$sqlr="UPDATE servliquidaciones SET factura='$contador' WHERE id_liquidacion='$rown[0]'";
								mysqli_query($linkbd,$sqlr);
								//$sqlr="INSERT INTO comprobante_cab (numerotipo,tipo_comp,fecha,concepto,total,total_debito, total_credito,diferencia,estado) values ('$contador','29','".$_POST['fecha']."','LIQUIDACION SERVICIOS PUBLICOS USUARIO $row[2] $nomter',0,0,0,0,'1')";
								//mysqli_query($linkbd,$sqlr);
								$nuevo=$actual;
							}
							if($nuevo!=$actual){$contador+=1;}	
							//tarifas
							/*
							$concepto=buscaconcepto_sp($row[4],'TR');
							$cuentascon=concepto_cuentas($concepto,'SP','10',$centrocosto,$_POST['fecha']);
							$tm=count($cuentascon);
							for($x=0;$x<$tm;$x++)
							{
								if($cuentascon[$x][1]=='N' && $cuentascon[$x][2]=='S'){$debito=$valortarcf;$credito=0;}
								if($cuentascon[$x][1]=='N' && $cuentascon[$x][2]=='N'){$debito=0;$credito=$valortarcf;}
								$sqlr="insert into comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque,valdebito,valcredito, estado,vigencia,tipo_comp, numerotipo) values ('29 $contador','".$cuentascon[$x][0]."','$row[3]','$centrocosto', 'TARIFA SERVICIO:".$row[4]."','','$debito', '$credito','1' ,'".$_POST['vigencias']."','29','$contador')";
								mysqli_query($linkbd,$sqlr);
							}
							//subsidios
							$concepto=buscaconcepto_sp($row[4],'SB');
							$cuentascon=concepto_cuentas($concepto,'SP','10',$centrocosto,$_POST['fecha']);
							$tm=count($cuentascon);
							$debito=0;
							$credito=0;
							for($x=0;$x<$tm;$x++)
							{
								if($cuentascon[$x][1]=='N' && $cuentascon[$x][2]=='S'){$debito=$valorsub;$credito=0;}
								if($cuentascon[$x][1]=='N' && $cuentascon[$x][2]=='N'){$debito=0;$credito=$valorsub;}
								$sqlr="insert into comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque,valdebito, valcredito, estado, vigencia,tipo_comp,numerotipo) values ('29 $contador','".$cuentascon[$x][0]."','$row[3]' ,'$centrocosto' , 'SUBSIDIO SERVICIO:".$row[4]."','','$debito', '$credito','1' ,'".$_POST['vigencias']."','29','$contador')";
								mysqli_query($linkbd,$sqlr);
							}
							*/
						}
					}
					$sqlr ="UPDATE servliquidaciones_gen SET estado='S' WHERE id_cab='".$_POST['liquigen']."'";
					mysqli_query($linkbd,$sqlr);
				}
					?>
					<input name="factmin" type="hidden" value="<?php echo @$_POST['factmin'];?>">
					<script>document.form2.factmin.value=<?php echo $facturaminima?></script>
				
			</div>
		</form>
	</body>
</html>