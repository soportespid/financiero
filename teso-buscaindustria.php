<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require "comun.inc";
	require "funciones.inc";
	session_start();
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html lang="es">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1">
		<meta name="viewport" content="user-scalable=no">
		<title>:: IDEAL 10 - Tesorer&iacute;a</title>
		<link href="favicon.ico" rel="shortcut icon"/>
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/css2.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<script type="text/javascript" src="css/calendario.js"></script>
		<script type="text/javascript" src="jquery-1.11.0.min.js"></script> 
		<script type="text/javascript" src="css/programas.js"></script>
		<script>
			function verUltimaPos(idcta, filas, filtro1, filtro2, filtro3)
			{
				var scrtop=$('#divdet').scrollTop();
				var altura=$('#divdet').height();
				var numpag=$('#nummul').val();
				var limreg=$('#numres').val();
				if((numpag<=0)||(numpag==""))
					numpag=0;
				if((limreg==0)||(limreg==""))
					limreg=10;
				numpag++;
				location.href="teso-industriaver.php?idrecaudo="+idcta+"&scrtop="+scrtop+"&totreg="+filas+"&altura="+altura+"&numpag="+numpag+"&limreg="+limreg+"&filtro1="+filtro1+"&filtro2="+filtro2+"&filtro3="+filtro3;
			}
			function buscarbotonfiltro()
			{
				if((document.form2.fecha.value != "" && document.form2.fecha2.value == "") || (document.form2.fecha.value == "" && document.form2.fecha2.value != "")){
					alert("Falta digitar fecha");
				}else{
					document.getElementById('numpos').value=0;
					document.getElementById('nummul').value=0;
					document.form2.submit();
				}
				
			}
			function eliminar(idr)
			{
				if (confirm("Esta Seguro de Anular la Liquidacion de Industria y Comercio No "+idr))
				{
				document.form2.oculto.value=2;
				document.form2.var1.value=idr;
				document.form2.submit();
				}
			}
			function crearexcel(){
				document.form2.action="teso-buscaindustriaexcel.php";
				document.form2.target="_BLANK";
				document.form2.submit();
				document.form2.action="";
				document.form2.target="";
			}
			function validar(){
				document.form2.submit();
			}
		</script>
	</head>
	<body>
		<IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
		<span id="todastablas2"></span>
		<table>
			<tr><script>barra_imagenes("teso");</script><?php cuadro_titulos();?></tr>	 
			<tr><?php menu_desplegable("teso");?></tr>
			<tr>
				<td colspan="3" class="cinta">
					<a href="teso-industria.php" class="mgbt"><img src="imagenes/add.png" title="Nuevo"/></a>
					<a class="mgbt"><img src="imagenes/guardad.png"/></a>
					<a class="mgbt"><img src="imagenes/buscad.png"/></a>
					<a onClick="mypop=window.open('plan-agenda.php','','');mypop.focus()" class="mgbt"><img src="imagenes/agenda1.png" title="Agenda" /></a>
					<a onClick="mypop=window.open('teso-principal.php','','');mypop.focus();" class="mgbt"><img src="imagenes/nv.png" title="Nueva Ventana"></a>
					<a onClick="mypop=window.open('<?php echo $url2; ?>','','');mypop.focus();" class="tooltip bottom mgbt"><img src="imagenes/duplicar_pantalla.png" title="Duplicar pesta&ntilde;a"></a>
					<a onclick="crearexcel()" class="mgbt"><img src="imagenes/excel.png" title="Excel"></a>
				</td>
			</tr>	
		</table>
		<?php
			if($_GET['numpag']!=""){
				$oculto=$_POST['oculto'];
				if($oculto!=2){
					$_POST['numres']=$_GET['limreg'];
					$_POST['numpos']=$_GET['limreg']*($_GET['numpag']-1);
					$_POST['nummul']=$_GET['numpag']-1;
				}
			}
			else{
				if($_POST['nummul']==""){
					$_POST['numres']=10;
					$_POST['numpos']=0;
					$_POST['nummul']=0;
				}
			}
		?>
		<form name="form2" method="post" action="teso-buscaindustria.php">
			<?php
				if($_POST['bandera'] == "")
				{
					if(isset($_GET['filtro1']))
					{
						$_POST['nombre'] = $_GET['filtro1'];
					}
						
					if(isset($_GET['filtro2']))
					{  
						$_POST['fecha'] = $_GET['filtro2'];
					}

					if(isset($_GET['filtro3']))
					{
						$_POST['fecha2']=$_GET['filtro3'];
					}

					$_POST['bandera'] = 1;
					echo "<script>document.form2.bandera.value = 1; 
					document.form2.submit();</script>";
				}
				preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST['fecha'],$fecha);
				$f1="$fecha[3]-$fecha[2]-$fecha[1]";
				preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST['fecha2'],$fecha);
				$f2="$fecha[3]-$fecha[2]-$fecha[1]";

				$scrtop=$_GET['scrtop'];
				if($scrtop=="") $scrtop=0;
				echo"<script>
					window.onload=function(){
						$('#divdet').scrollTop(".$scrtop.")
					}
				</script>";
				$gidcta=$_GET['idrecaudo'];
				
			?>
			<input type="hidden" name="bandera" id="bandera" value="1"/>
			<input type="hidden" name="numres" id="numres" value="<?php echo $_POST['numres'];?>"/>
			<input type="hidden" name="numpos" id="numpos" value="<?php echo $_POST['numpos'];?>"/>
			<input type="hidden" name="nummul" id="nummul" value="<?php echo $_POST['nummul'];?>"/>
			<table  class="inicio" >
				<tr>
					<td class="titulos" colspan="9">:. Buscar Liquidaci&oacute;n Industria y Comercio</td>
					<td style="width:2cm;" class="cerrar" ><a href="teso-principal.php">Cerrar</a></td>
				</tr>
				<tr>
					<td style="width:4cm" class="saludo1">No liquidaci&oacute;n:</td>
					<td style="width:30%"><input type="serch" name="nombre" id="nombre" style="width:95%" value="<?php echo $_POST['nombre']?>"/></td>
					<td class="saludo1" style="width:2cm;">Fecha inicial:</td>
					<td style="width:10%;">
						<input name="fecha" type="text" value="<?php echo $_POST['fecha']?>" maxlength="10"  onKeyPress="javascript:return solonumeros(event)" onKeyUp="return tabular(event,this)"  id="fc_1198971545" onKeyDown="mascara(this,'/',patron,true)" style="width:75%;height:30px;" title="DD/MM/YYYY" placeholder="DD/MM/YYYY" onchange=""/>&nbsp;<img src="imagenes/calendario04.png" onClick="displayCalendarFor('fc_1198971545');" title="Calendario" class="icobut"/>
					</td>
					<td class="saludo1" style="width:2cm;">Fecha final:</td>
					<td style="width:10%;">
						<input name="fecha2" type="text" value="<?php echo $_POST['fecha2']?>" maxlength="10"  onKeyPress="javascript:return solonumeros(event)" onKeyUp="return tabular(event,this)"  id="fc_1198971546" onKeyDown="mascara(this,'/',patron,true)" style="width:75%;height:30px;" title="DD/MM/YYYY" placeholder="DD/MM/YYYY" onchange=""/>&nbsp;<img src="imagenes/calendario04.png" onClick="displayCalendarFor('fc_1198971546');" title="Calendario" class="icobut"/>
					</td>
					<td style="padding-bottom:0px"><em class="botonflecha" onClick="buscarbotonfiltro();">Buscar</em></td>
				</tr>                       
			</table>
			<input name="var1" id="var1" type="hidden" value=<?php echo $_POST['var1'];?>>
			<input name="oculto" id="oculto" type="hidden" value="1">   
			<div class="subpantalla" style="height:68.5%; width:99.6%; overflow-x:hidden;" id="divdet">
				<?php
					$oculto=$_POST['oculto'];
					if($_POST['oculto']==2){
						$sqlr="select * from tesoindustria where id_industria=$_POST[var1]";
						$resp = mysqli_query($linkbd,$sqlr);
						$row=mysqli_fetch_row($resp);
						//********Comprobante contable en 000000000000
						$sqlr="update comprobante_cab set total_debito=0,total_credito=0,estado='0' where tipo_comp='3' and numerotipo=$_POST[var1]";
						mysqli_query($linkbd,$sqlr);
						$sqlr="update comprobante_det set valdebito=0,valcredito=0 where id_comp='3 $row[0]'";
						mysqli_query($linkbd,$sqlr);
						//********PREDIAL O RECAUDO SE ACTIVA COLOCAR 'S'	
						$sqlr="update tesoindustria set estado='N' where id_industria=$row[0]";
						mysqli_query($linkbd,$sqlr);	
					}
					$crit2="";
					if ($_POST['nombre'] != "")
					{
						$crit2="AND id_industria LIKE '$_POST[nombre]'";
					}
						
					//sacar el consecutivo 
					$sqlr="SELECT * FROM tesoindustria WHERE tesoindustria.id_industria>-1 ".$crit2." ORDER BY tesoindustria.id_industria";
					if(isset($_POST['fecha']) && isset($_POST['fecha2'])){
						if(!empty($_POST['fecha']) && !empty($_POST['fecha2'])){
							$sqlr="SELECT * FROM tesoindustria WHERE tesoindustria.id_industria>-1 ".$crit2." AND fecha BETWEEN '$f1' AND '$f2' ORDER BY tesoindustria.id_industria";
						}
					}

					$resp = mysqli_query($linkbd,$sqlr);
					$ntr = mysqli_num_rows($resp);

					$_POST['numtop'] = $ntr;

					$nuncilumnas=ceil($_POST['numtop']/$_POST['numres']);

					$cond2="";

					if ($_POST['numres']!="-1")
					{ 
						$cond2="LIMIT $_POST[numpos], $_POST[numres]"; 
					}

					$sqlr="SELECT * FROM tesoindustria WHERE tesoindustria.id_industria>-1 ".$crit2." ORDER BY tesoindustria.id_industria DESC $cond2";
					if(isset($_POST['fecha']) && isset($_POST['fecha2'])){
						if(!empty($_POST['fecha']) && !empty($_POST['fecha2'])){
							$sqlr="SELECT * FROM tesoindustria WHERE tesoindustria.id_industria>-1 ".$crit2." AND fecha BETWEEN '$f1' AND '$f2' ORDER BY tesoindustria.id_industria DESC $cond2";
						}
					}  

					$resp = mysqli_query($linkbd,$sqlr);

					$numcontrol=$_POST['nummul']+1;

					if(($nuncilumnas==$numcontrol)||($_POST['numres']=="-1")){
						$imagenforward="<img src='imagenes/forward02.png' style='width:17px'>";
						$imagensforward="<img src='imagenes/skip_forward02.png' style='width:16px' >";
					}
					else{
						$imagenforward="<img src='imagenes/forward01.png' style='width:17px' title='Siguiente' onClick='numsiguiente()'>";
						$imagensforward="<img src='imagenes/skip_forward01.png' style='width:16px' title='Fin' onClick='saltocol(\"$nuncilumnas\")'>";
					}
					if(($_POST['numpos']==0)||($_POST['numres']=="-1")){
						$imagenback="<img src='imagenes/back02.png' style='width:17px'>";
						$imagensback="<img src='imagenes/skip_back02.png' style='width:16px'>";
					}
					else{
						$imagenback="<img src='imagenes/back01.png' style='width:17px' title='Anterior' onClick='numanterior();'>";
						$imagensback="<img src='imagenes/skip_back01.png' style='width:16px' title='Inicio' onClick='saltocol(\"1\")'>";
					}

					$con=1;
					echo "<table class='inicio' align='center' >
						<tr>
							<td colspan='7' class='titulos'>.: Resultados Busqueda:</td>
							<td class='submenu'>
								<select name='renumres' id='renumres' onChange='cambionum();' style='width:100%'>
									<option value='10'"; if ($_POST['renumres']=='10'){echo 'selected';} echo ">10</option>
									<option value='20'"; if ($_POST['renumres']=='20'){echo 'selected';} echo ">20</option>
									<option value='30'"; if ($_POST['renumres']=='30'){echo 'selected';} echo ">30</option>
									<option value='50'"; if ($_POST['renumres']=='50'){echo 'selected';} echo ">50</option>
									<option value='100'"; if ($_POST['renumres']=='100'){echo 'selected';} echo ">100</option>
									<option value='-1'"; if ($_POST['renumres']=='-1'){echo 'selected';} echo ">Todos</option>
								</select>
							</td>
						</tr>
						<tr>
							<td colspan='8' id='RecEnc'>Recaudos Encontrados: $ntr2</td>
						</tr>
						<tr>
							<td width='150' class='titulos2'>Codigo</td>
							<td class='titulos2'>Periodo Liquidado</td>
							<td class='titulos2'>Fecha</td>
							<td class='titulos2'>Valor </td>
							<td class='titulos2'>Contribuyente</td>
							<td class='titulos2'><center>Estado</td>
							<td class='titulos2' width='5%'><center>Anular</td>
							<td class='titulos2' width='5%'><center>Ver</td>
						</tr>";	
						$iter='saludo1a';
						$iter2='saludo2';
						$filas=1;

						if($_POST['fecha'] == '' && $_POST['fecha2'] == '' && $_POST['nombre'] == '')
						{
							echo "
							<table class='inicio'>
								<tr>
									<td class='saludo1' style='text-align:center;width:100%;font-size:25px'>Utilice el filtro de busqueda</td>
								</tr>
							</table>";
							$nuncilumnas = 0;
						}
						elseif(mysqli_num_rows($resp) == 0 || mysqli_num_rows($resp) == '0')
						{
							echo "
							<table class='inicio'>
								<tr>
									<td class='saludo1' style='text-align:center;width:100%;font-size:25px'>No hay resultados de su busqueda.</td>
								</tr>
							</table>";
						}
						else
						{
							while($row =mysqli_fetch_row($resp))
							{
								
								$ntr2 = $ntr;

								echo "<script>document.getElementById('RecEnc').innerHTML = 'Recaudos Encontrados: $ntr2'</script>";
								$tr=buscatercero($row[5]);
								
								if($gidcta!=""){
									if($gidcta==$row[0]){
										$estilo='background-color:yellow';
									}
									else{
										$estilo="";
									}
								}
								else{
									$estilo="";
								}

								$idcta="'".$row[0]."'";
								$numfil="'".$filas."'";
								$filtro1="'".$_POST['nombre']."'";
								$filtro2="'".$_POST['fecha']."'";
								$filtro3="'".$_POST['fecha2']."'";	

								echo"

								<input type='hidden' name='CodigoE[]' value='".$row[0]."'/>
								<input type='hidden' name='periodoLiquidE[]' value='".$row[3]."' />
								<input type='hidden' name='fechaE[]' value='".$row[1]."' />
								<input type='hidden' name='valorE[]' value='".$row[6]."' />
								<input type='hidden' name='nomContribuyenteE[]' value='$tr' />							
								";
								
								if ($row[7]=='S')
								{
									echo"
									<input type='hidden' name='estadoE[]' value='ACTIVO'>";
								}
								if ($row[7]=='N')
								{
									echo"
									<input type='hidden' name='estadoE[]' value='ANULADO'>";
								}
								if ($row[7]=='P')
								{
									echo"
									<input type='hidden' name='estadoE[]' value='PAGO'>";
								}

								echo
								"<tr class='$iter' onDblClick=\"verUltimaPos($idcta, $numfil, $filtro1, $filtro2, $filtro3)\" style='text-transform:uppercase; $estilo' >
									<td>$row[0]</td>
									<td>$row[3]</td>
									<td>$row[1]</td>
									<td>$row[6]</td>
									<td>$tr</td>";
									if ($row[7]=='S')
										echo "<td >
											<center><img src='imagenes/confirm.png'></center>
										</td>";
									if ($row[7]=='N')
										echo "<td >
											<center><img src='imagenes/cross.png'></center>
										</td>";
									if ($row[7]=='P')
										echo "<td >
											<center><img src='imagenes/dinero3.png'></center>
										</td>";
									if($row[7]=='S')
										echo "<td style='text-align:center;'><a href='#' onClick=eliminar($row[0])><img src='imagenes/anular.png'></a></td>";
									if ($row[7]!='S')
										echo "<td ></td>";	 
										echo"
										<td style='text-align:center;'>
											<a onClick=\"verUltimaPos($idcta, $numfil, $filtro1, $filtro2, $filtro3)\" style='cursor:pointer;'>
												<img src='imagenes/lupa02.png' style='width:18px' title='Ver'>
											</a>
										</td>
									</tr>";
								$con+=1;
								$aux=$iter;
								$iter=$iter2;
								$iter2=$aux;
								$filas++;
							}
						}
							echo"</table>
							<table class='inicio'>
								<tr>
									<td style='text-align:center;'>
										<a href='#'>$imagensback</a>&nbsp;
										<a href='#'>$imagenback</a>&nbsp;&nbsp;";
								if($nuncilumnas<=9){$numfin=$nuncilumnas;}
								else{$numfin=9;}
								for($xx = 1; $xx <= $numfin; $xx++)
								{
									if($numcontrol<=9){$numx=$xx;}
									else{$numx=$xx+($numcontrol-9);}
									if($numcontrol==$numx){echo"<a href='#' onClick='saltocol(\"$numx\")'; style='color:#24D915'> $numx </a>";}
									else {echo"<a href='#' onClick='saltocol(\"$numx\")'; style='color:#000000'> $numx </a>";}
								}
								echo "		&nbsp;&nbsp;<a href='#'>$imagenforward</a>
												&nbsp;<a href='#'>$imagensforward</a>
											</td>
										</tr>
									</table>";

				?>
			</div>
		</form>
	</body>
</html>