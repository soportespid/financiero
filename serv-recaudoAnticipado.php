<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=iso-8859-1");
	require "comun.inc";
	require "funciones.inc";
	require "funcionesSP.inc.php";
	session_start();
	$linkbd = conectar_v7();
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es">
	<head>
		<meta http-equiv="Content-type" content="text/html; charset=iso-8859-1"/>
		<meta http-equiv="X-UA-Compatible" content="IE=9"/>
		<title>:: IDEAL 10 - Servicios P&uacute;blicos</title>
		<link href="css/css2.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/cssSP.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/style.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
		<script type="text/javascript" src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
        <script type="text/javascript" src="css/calendario.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<style>
		</style>

		<script>
            function despliegamodal2(_valor, _table, cliente)
			{
                document.getElementById('bgventanamodal2').style.visibility = _valor;
                var cliente = document.getElementById('cliente').value;
                
                if (_table == 'srvclientes')
                {
                    document.getElementById('ventana2').src = 'serv-ventanaAnticiposDOCE.php';
                }
                else if(_table == 'srvfacturas')
                {
                    if(cliente != '')
                    {
                        document.getElementById('ventana2').src = 'serv-facturasDisponiblesCliente-ventana.php?cliente=' + cliente;
                    }
                    else
                    {
                        despliegamodalm('visible','2','Falta seleccionar el cliente');
                        parent.despliegamodal2("hidden");
                    }
                }
				else if(_table == 'srvcuentasbancarias')
				{
					document.getElementById('ventana2').src = 'cuentasBancarias-ventana.php';
				}
            }

			function despliegamodalm(_valor,_tip,mensa,pregunta)
			{
				document.getElementById("bgventanamodalm").style.visibility=_valor;

				if(_valor=="hidden")
				{
					document.getElementById('ventanam').src="";
				}
				else
				{
					switch(_tip)
					{
						case "1":	
							document.getElementById('ventanam').src="ventana-mensaje1.php?titulos="+mensa;
						break;

						case "2":	
							document.getElementById('ventanam').src="ventana-mensaje3.php?titulos="+mensa;
						break;

						case "3":	
							document.getElementById('ventanam').src="ventana-mensaje2.php?titulos="+mensa;
						break;

						case "4":	
							document.getElementById('ventanam').src="ventana-consulta1.php?titulos="+mensa+"&idresp="+pregunta;
						break;	
					}
				}
			}

			function respuestaModalBusqueda2(id_cliente, nombre)
			{
				document.getElementById('cliente').value = id_cliente;
				document.getElementById('nCliente').value = nombre;
				document.form2.buscaValorFactura.value = '1';
				document.form2.submit();	
            }

			function funcionmensaje()
			{
				var idban=document.getElementById('codban').value;

				document.location.href = "serv-recaudoAnticipadoVisualizar.php?idban="+idban;
			}

			function respuestaconsulta(pregunta)
			{
				switch(pregunta)
				{
					case "1":	
						document.form2.oculto.value='2';
						document.form2.submit();
					break;
				}
			}

			function guardar()
			{
				var codigo = document.getElementById('codban').value;
                var cliente = document.getElementById('cliente').value;
                var mesesAbonados = document.getElementById('meses_abonados').value;
				var recaudo = document.getElementById('modoRecaudo').value;
				var fecha = document.getElementById('fecha_pago').value;

				if (codigo == "" || cliente == "" || mesesAbonados == "" || recaudo == "-1" || fecha == "") {
					despliegamodalm('visible','2','Falta información para crear el recaudo anticipado.');
					return false;
				}

				if (recaudo == "banco" && document.getElementById('banco').value == "") {
					despliegamodalm('visible','2','Debes seleccionar el banco.');
					return false;
				}

				document.form2.calcularValorMeses.value = '1';
				document.form2.buscaValorFactura.value = '1';
				despliegamodalm('visible','4','Esta Seguro de Guardar','1'); 
			}

			function actualizar()
			{
				document.form2.calcularValorMeses.value = '1';
				document.form2.buscaValorFactura.value = '1';
				document.form2.submit();
			}

            function calcularValorTotal()
            {
                var cuotas = parseInt(document.getElementById('meses_abonados').value);
				var cliente = document.getElementById('cliente').value;

				if(cliente != '')
				{
					if(parseInt(cuotas) > 0)
					{
						document.form2.calcularValorMeses.value = '1';
						document.form2.buscaValorFactura.value = '1';
						document.form2.submit();
					}
					else
					{
						document.getElementById('meses_abonados').value = '';
						despliegamodalm('visible','2','No puede colocar meses menores o iguales a cero');
					}
				}
				else
				{
					document.getElementById('meses_abonados').value = '';
					despliegamodalm('visible','2','Seleccione primero el cliente');
				}
            }
		</script>

		<?php titlepag();?>

	</head>

	<body>
		<IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
		<span id="todastablas2"></span>
		<table>
			<tr><script>barra_imagenes("serv");</script><?php cuadro_titulos();?></tr>

			<tr><?php menu_desplegable("serv");?></tr>

			<tr>
				<td colspan="3" class="cinta">
					<a href="serv-recaudoAnticipado.php" class="mgbt"><img src="imagenes/add.png"/></a>

					<a onclick="guardar()" class="mgbt"><img src="imagenes/guarda.png" title="Guardar"/></a>

					<a href="serv-recaudoAnticipadoBuscar.php" class="mgbt"><img src="imagenes/busca.png" title="Buscar"/></a>

					<a onclick="mypop=window.open('plan-agenda.php','','');mypop.focus()" class="mgbt"><img src="imagenes/agenda1.png" title="Agenda" /></a>

					<a onclick="mypop=window.open('serv-principal.php','','');mypop.focus();" class="mgbt"><img src="imagenes/nv.png" title="Nueva ventana"></a>

					<a onclick="mypop=window.open('<?php echo $url2; ?>','','');mypop.focus();" class="mgbt"><img src="imagenes/duplicar_pantalla.png" title="Duplicar pesta&ntilde;a"></a>

                    <a href="serv-menuRecaudo.php" class="mgbt"><img src="imagenes/iratras.png" title="Atr&aacute;s"></a>
                </td>
			</tr>
		</table>

		<div id="bgventanamodalm" class="bgventanamodalm">
			<div id="ventanamodalm" class="ventanamodalm">
				<IFRAME src="" name="ventanam" marginWidth=0 marginHeight=0 frameBorder=0 id="ventanam" frameSpacing=0 style=" width:700px; height:130px; top:200; overflow:hidden;"></IFRAME>
			</div>
		</div>

		<form name="form2" method="post" action="">
			<?php 
				if(@$_POST['oculto'] == "")
				{				
					$_POST['codban'] = selconsecutivo('srvrecaudo_anticipo','id');
				}

                if(@$_POST['buscaValorFactura'] == '1')
                {
					//inicio de variables
					$cargoFijo = 0;
					$consumoBasico = 0;
					$contribuciones = 0;
					$subsidios = 0;
					$porcentaje = 0;
					$total = 0;
					$año = '2022';

					//consulto estrato
					$sqlCliente = "SELECT id_estrato FROM srvclientes WHERE id = '$_POST[cliente]'";
					$resCliente = mysqli_query($linkbd,$sqlCliente);
					$rowCliente = mysqli_fetch_row($resCliente);

					$vigencia = '2022';

					$sql = "SELECT MAX(version) FROM srvcargo_fijo";
					$row = mysqli_fetch_row(mysqli_query($linkbd, $sql));
					
					$versionTarifas = $row[0];

					//consulta servicios asignados
					$sqlServicios = "SELECT id_servicio FROM srvasignacion_servicio WHERE id_clientes = '$_POST[cliente]'";
					$resServicios = mysqli_query($linkbd,$sqlServicios);
					while($rowServicios = mysqli_fetch_row($resServicios))
					{
						//consulto cargo fijo
						$sqlCargoFijo = "SELECT costo_unidad FROM srvcargo_fijo WHERE id_servicio = '$rowServicios[0]' AND id_estrato = '$rowCliente[0]' AND estado = 'S' AND version = '$versionTarifas'";
						$resCargoFijo = mysqli_query($linkbd,$sqlCargoFijo);
						$rowCargoFijo = mysqli_fetch_row($resCargoFijo);

						$cargoFijo = $rowCargoFijo[0];

						//consulta consumo basico
						$sqlConsumoBasico = "SELECT costo_unidad FROM srvcostos_estandar WHERE id_servicio = '$rowServicios[0]' AND id_estrato = '$rowCliente[0]' AND estado = 'S' AND version = '$versionTarifas'";	
						$resConsumoBasico = mysqli_query($linkbd,$sqlConsumoBasico);
						$rowConsumoBasico = mysqli_fetch_row($resConsumoBasico);

						$consumoBasico = $rowConsumoBasico[0];

						$cargoFijoAndConsumoBasico = $cargoFijo + $consumoBasico;

						//consulta contribuciones
						$sqlContribuciones = "SELECT porcentaje, valor FROM srvcontribuciones WHERE id_estrato = '$rowCliente[0]' AND id_servicio = '$rowServicios[0]'";
						$resContribuciones = mysqli_query($linkbd,$sqlContribuciones);
						$rowContribuciones = mysqli_fetch_row($resContribuciones);

						if(isset($rowContribuciones[0]))
						{
							$porcentaje = $rowContribuciones[0] / 100;

							if($rowContribuciones[0] != 0)
							{
								$contribuciones = $cargoFijoAndConsumoBasico * $porcentaje;
							}
							else
							{
								$contribuciones = $cargoFijoAndConsumoBasico - $rowContribuciones[1];
							}
						}

						//subsidios
						$sqlSubsidio = "SELECT porcentaje, valor FROM srvsubsidios WHERE id_estrato = '$rowCliente[0]' AND id_servicio = '$rowServicios[0]' AND vigencia = '$año'";
						$resSubsidio = mysqli_query($linkbd,$sqlSubsidio);
						$rowSubsidio = mysqli_fetch_row($resSubsidio);

						if(isset($rowSubsidio[0]))
						{
							$porcentaje = $rowSubsidio[0] / 100;
							
							if($rowSubsidio[0] != 0)
							{
								
								$subsidios = $cargoFijoAndConsumoBasico * $porcentaje;
							}
							else
							{
								$subsidios = $cargoFijoAndConsumoBasico - $rowSubsidio[1];
							}
						}

						//nombre servicios
						$sqlNombreServicios = "SELECT nombre FROM srvservicios WHERE id = '$rowServicios[0]'";
						$resNombreServicios = mysqli_query($linkbd,$sqlNombreServicios);
						$rowNombreServicios = mysqli_fetch_row($resNombreServicios);

						$total = $cargoFijo + $consumoBasico + $contribuciones - $subsidios;

						$_POST['codigoServicio'][] = $rowServicios[0];
						$_POST['nombreServicio'][] = $rowNombreServicios[0];
						$_POST['cargoFijo'][]      = $cargoFijo;
						$_POST['consumoBasico'][]  = $consumoBasico;
						$_POST['subsidios'][]      = $subsidios;
						$_POST['contribuciones'][] = $contribuciones;
						$_POST['valorReal'][]      = $total;

						$_POST['totalCargoFijo']       = $_POST['totalCargoFijo'] + $cargoFijo;
                        $_POST['totalConsumoBasico']   = $_POST['totalConsumoBasico'] + $consumoBasico;
                        $_POST['totalContribuciones']  = $_POST['totalContribuciones'] + $contribuciones;
                        $_POST['totalSubsidios']       = $_POST['totalSubsidios'] + $subsidios;
                        $_POST['totalValorReal']       = $_POST['totalValorReal'] + $total;
                        
					}

					$_POST['valorUnitario'] = $total;

					
                }

				if(@$_POST['calcularValorMeses'] == '1')
				{
					$_POST['valorTotal'] = $_POST['valorUnitario'] * $_POST['meses_abonados'];
				}
			?>
			<div>
				<table class="inicio ancho">
					<tr>
						<td class="titulos" colspan="6">.: Recaudo por Anticipo</td>

						<td class="cerrar" style="width:7%" onClick="location.href='serv-principal.php'">Cerrar</td>
					</tr>

					<tr>
						<td class="tamano01" style="width:3cm;">C&oacute;digo:</td>

						<td style="width:15%;">
							<input type="text" name="codban" id="codban" onKeyUp="return tabular(event,this)" value="<?php echo @ $_POST['codban'];?>" style="width:100%;height:30px;text-align:center;" readonly/>
						</td>

						<td class="saludo1">Cliente:</td>

						<td style="width: 250px;">
							<input type="text" name='cliente' id='cliente'  value="<?php echo @$_POST['cliente']?>" style="text-align:center; width:100%;" onclick = "despliegamodal2('visible', 'srvclientes');"  class="colordobleclik" readonly>
						</td>

						<td colspan="2">
							<input type="text" name="nCliente" id="nCliente" value="<?php echo @$_POST['nCliente']?>" style="width:100%;height:30px;" readonly>
						</td>
					</tr>

					<tr>
						<td class="tamano01">Valor Mensual</td>

						<td>
							<input type="text" name="valorUnitario" id="valorUnitario" value="<?php echo $_POST['valorUnitario'] ?>" style="height: 30px; text-align:center; width:100%;" readonly>
						</td>

						<td class="tamano01" style="width: 150px;">Meses a abonar</td>

						<td>
							<input type="text" name="meses_abonados" id="meses_abonados" value="<?php echo $_POST['meses_abonados'] ?>" style="height: 30px; text-align:center; width:100%;" onblur="calcularValorTotal()">
						</td>

						<td class="tamano01" style="width: 150px;">Valor Total:</td>

						<td>
							<input type="text" name="valorTotal" id="valorTotal" value="<?php echo $_POST['valorTotal'] ?>" style="height: 30px; text-align:center; width:20%;" readonly>
						</td>
					</tr>

					<tr>
						<td class="tamano01">Recaudo en: </td>

						<td>
							<select  name="modoRecaudo" id="modoRecaudo" style="width:100%;height:30px;" onChange="actualizar()" class="centrarSelect">
								<option class="aumentarTamaño" value="-1">:: SELECCIONE TIPO DE RECAUDO ::</option>
								<option class="aumentarTamaño" value="caja" <?php if(@$_POST['modoRecaudo']=='caja') echo "SELECTED"; ?>>Caja</option>
								<option class="aumentarTamaño" value="banco" <?php if(@$_POST['modoRecaudo']=='banco') echo "SELECTED"; ?>>Banco</option>
							</select>
						</td>

						<?php
							if (@$_POST['modoRecaudo'] == 'banco')
							{
						?>
								<td class="saludo1">Cuenta Banco: </td> 

								<td>
									<input type="text" id="banco" name="banco" style="width:88%" onKeyPress="javascript:return solonumeros(event)" onKeyUp="return tabular(event,this)" onBlur="" value="<?php echo $_POST['banco']?>" onClick="document.getElementById('banco').focus();document.getElementById('banco').select();" readonly>

									<input type="hidden" name="cuentaBancaria" id="cuentaBancaria" value="">

									<a title="Bancos" onClick="despliegamodal2('visible','srvcuentasbancarias');" style="cursor:pointer;"><img src="imagenes/find02.png" style="width:20px;"/></a>
								</td>

								<td colspan="2">
									<input type="text" name="nbanco" id="nbanco" style="width:100%;" value="<?php echo $_POST['nbanco']?>"  readonly>
								</td>	
						<?php
							}
						?>
					</tr>

					<tr>
						<td class="tamano01" style="width:10%;">Fecha de Pago</td>

						<td style="width:15%;">
							<input type="text" name="fecha_pago" id="fecha_pago" value="<?php echo @ $_POST['fecha_pago']?>" maxlength="10" onKeyUp="return tabular(event,this)"  id="fc_1198971545" onKeyDown="mascara(this,'/',patron,true)" title="DD/MM/YYYY" style="height:30px;text-align:center;width:85%;" onchange="" readonly/>
							
							<a href="#" onClick="displayCalendarFor('fecha_pago');" title="Calendario">
								<img src="imagenes/calendario04.png" style="width:25px;">
							</a>
						</td>
					</tr>
				</table>
			</div>

			<div>
				<table class="inicio">
					<tr><td class="titulos" colspan="12">.: Informaci&oacute;n Factura Mensual</td></tr>

					<tr class="titulos2">
						<td style="text-align:center;">C&oacute;digo</td>
						<td style="text-align:center;">Servicio</td>
						<td style="text-align:center;">Cargo Fijo</td>
						<td style="text-align:center;">Consumo Basico</td>
						<td style="text-align:center;">Contribuciones</td>
						<td style="text-align:center;">Subsidios</td>
						<td style="text-align:center;">Valor real</td>
					</tr>

					<?php
					for ($x=0;$x< count($_POST['codigoServicio']);$x++)
					{
						echo "
							<tr class='saludo1a'>
								<td style='text-align:center;width:5%;'>".$_POST['codigoServicio'][$x]."</td>

								<td style='text-align:center;width:10%;'>".$_POST['nombreServicio'][$x]."</td>

								<td style='text-align:center;width:9%;'>".number_format($_POST['cargoFijo'][$x],2,',','.')."</td>
							
								<td style='text-align:center;width:9%;'>".number_format($_POST['consumoBasico'][$x],2,',','.')."</td>

								<td style='text-align:center;width:9%;'>".number_format($_POST['contribuciones'][$x],2,',','.')."</td>
									
								<td style='text-align:center;width:9%;'>".number_format($_POST['subsidios'][$x],2,',','.')."</td>
							
								<td style='text-align:center;width:9%;'>".number_format($_POST['valorReal'][$x],2,',','.')."</td>
							</tr>";
					}
					
					echo "
					<tr class='titulos2'>
						<td style='text-align:center;' colspan='2'>Total:</td>

						<td style='text-align:center;'>".number_format($_POST['totalCargoFijo'],2,',','.')."</td>
						
						<td style='text-align:center;'>".number_format($_POST['totalConsumoBasico'],2,',','.')."</td>

						<td style='text-align:center;'>".number_format($_POST['totalContribuciones'],2,',','.')."</td>
							
						<td style='text-align:center;'>".number_format($_POST['totalSubsidios'],2,',','.')."</td>

						<td style='text-align:center;'>".number_format($_POST['totalValorReal'],2,',','.')."</td>
					</tr>";    
				?>
				</table>
			</div>

			<input type="hidden" name="oculto" id="oculto" value="1"/>
			<input type="hidden" name="buscaValorFactura" id="buscaValorFactura" value=""/>
			<input type="hidden" name="calcularValorMeses" id="calcularValorMeses" value=""/>

			<?php 
				if(@$_POST['oculto'] == "2")
				{
                    preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST['fecha_pago'],$fecha);
					$fechaAnticipo="$fecha[3]-$fecha[2]-$fecha[1]";
					
					$sqlCuentaCaja = "SELECT cuentacaja FROM tesoparametros";
					$resCuentaCaja = mysqli_query($linkbd,$sqlCuentaCaja);
					$rowCuentaCaja = mysqli_fetch_row($resCuentaCaja);

					//contabilización de anticipo
					$codigoComprobante = '40';
					//cuenta de la parametrización de concepto contable
					$sqlConceptoContable = "SELECT cuenta, cc FROM conceptoscontables_det WHERE tipo = 'SA' AND modulo = '10'";
					$resConceptoContable = mysqli_query($linkbd,$sqlConceptoContable);
					$rowConceptoContable = mysqli_fetch_assoc($resConceptoContable);
	
					$sqlComprobanteCabecera = "INSERT INTO comprobante_cab (numerotipo, tipo_comp, fecha, concepto, total_debito, total_credito, estado) VALUES ('$_POST[codban]', '$codigoComprobante', '$fechaAnticipo', 'Pago por Anticipo', '$_POST[valorTotal]', '$_POST[valorTotal]', '1') ";
                    mysqli_query($linkbd,$sqlComprobanteCabecera);
					
					$documentoTercero = encuentraDocumentoTerceroConIdCliente($_POST['cliente']);
	
					if($_POST['modoRecaudo'] == 'caja')
					{
						$sqlr="INSERT INTO comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque,valdebito, valcredito,estado,vigencia) VALUES ('$codigoComprobante $_POST[codban]', '$rowCuentaCaja[0]','$documentoTercero','$rowConceptoContable[cc]','Pago por Anticipo del cliente $_POST[cliente]', '', '$_POST[valorTotal]', 0, '1', $fecha[3])";
						mysqli_query($linkbd,$sqlr);
					}
					elseif($_POST['modoRecaudo'] == 'banco')
					{
						$sqlr="INSERT INTO comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque,valdebito, valcredito,estado,vigencia) VALUES ('$codigoComprobante $_POST[codban]', '$_POST[cuentaBancaria]','$documentoTercero','$rowConceptoContable[cc]','Pago por Anticipo del cliente $_POST[cliente]', '', '$_POST[valorTotal]', 0, '1', $fecha[3])";
						mysqli_query($linkbd,$sqlr);
					}
				
					//debito va con la cuenta que trae de la parametrización
					$sqlr="INSERT INTO comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque,valdebito, valcredito,estado,vigencia) VALUES ('$codigoComprobante $_POST[codban]','$rowConceptoContable[cuenta]','$documentoTercero','$rowConceptoContable[cc]','Pago por Anticipo del cliente $_POST[cliente]', '', 0, '$_POST[valorTotal]', '1' ,'$fecha[3]')";
					mysqli_query($linkbd,$sqlr);
					
					$usuario = $_SESSION['usuario'];
		
					//guardado tabla de anticipo
					if($_POST['modoRecaudo'] == 'caja')
					{
						$sqlRecaudoAnticipo = "INSERT INTO srvrecaudo_anticipo(id_cliente, tipo_movimiento, fecha, recaudo_en, cuenta_banco, meses_abonados, valor_mensual, valor_total, estado, user) VALUES('$_POST[cliente]', '204', '$fechaAnticipo', '$_POST[modoRecaudo]', '$rowCuentaCaja[0]', '$_POST[meses_abonados]', '$_POST[valorUnitario]', '$_POST[valorTotal]', 'S', '$usuario')";	
					}
					elseif($_POST['modoRecaudo'] == 'banco')
					{
						$sqlRecaudoAnticipo = "INSERT INTO srvrecaudo_anticipo(id_cliente, tipo_movimiento, fecha, recaudo_en, cuenta_banco, meses_abonados, valor_mensual, valor_total, estado, user) VALUES('$_POST[cliente]', '204', '$fechaAnticipo', '$_POST[modoRecaudo]', '$_POST[cuentaBancaria]', '$_POST[meses_abonados]', '$_POST[valorUnitario]', '$_POST[valorTotal]', 'S', '$usuario')";	
					}

					if(mysqli_query($linkbd,$sqlRecaudoAnticipo))
					{
						echo "<script>despliegamodalm('visible','1','Se ha almacenado el pago con exito');</script>";
					}
					else
					{
						echo"<script>despliegamodalm('visible','2','No se pudo ejecutar el recibo de pago, contacte con soporte');</script>";
					}
				}
			?>
		</form>
		<div id="bgventanamodal2">
			<div id="ventanamodal2">
				<IFRAME  src="" name="buster" marginWidth=0 marginHeight=0 frameBorder=0 id="ventana2" frameSpacing=0 style="left:500px; width:900px; height:500px; top:200;"></IFRAME>
			</div>
		</div>
	</body>
</html>
