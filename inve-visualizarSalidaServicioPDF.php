<?php
	require_once("tcpdf/tcpdf_include.php");
	require('comun.inc');
	require "funciones.inc";
	require 'funcionesnomima.inc';
	session_start();
	class MYPDF extends TCPDF 
	{
        public $rs;
		public function Header() 
		{
			$linkbd = conectar_v7();
			$linkbd -> set_charset("utf8");
			$sqlr = "SELECT * FROM configbasica WHERE estado='S'";
			$res = mysqli_query($linkbd,$sqlr);
			$row = mysqli_fetch_row($res);
			$nit = $row[0];
			$this->rs = $row[1];
			$consecutivo = $_POST['consecutivo'];
			$this->Image('imagenes/escudo.jpg', 25, 12, 25, 23.9, 'JPG', '', 'T', true, 300, '', false, false, 0, false, false, false);// Logo
			$this->SetFont('helvetica','B',8);
			$this->SetY(10);
			$this->RoundedRect(15, 10, 245, 31, 1,'' );
			$this->Cell(0.1);
			$this->Cell(50,31,'','R',0,'L'); 
			$this->SetY(11);
			$this->SetX(90);
			$this->SetFont('helvetica','B',14);
			$this->Cell(141,12,strtoupper($this->rs),0,0,'C'); 
			$this->SetFont('helvetica','B',8);
			$this->SetY(18);
			$this->SetX(90);
			$this->SetFont('helvetica','B',11);
			$this->Cell(141,10,"$nit",0,0,'C');
			$this->SetY(27);
			$this->SetX(60);
			$this->Cell(200,9,"",'T',1,'C');
			$this->SetFont('helvetica','B',14);
			$this->SetY(27);
			$this->SetX(90);
			$descrip="ACTA DE SALIDA N° $consecutivo";
			$numerapagina = $this->getAliasNumPage().'/'.$this->getAliasNbPages();
			$this->Cell(141,9,"$descrip",'',1,'C');
			$this->SetFont('helvetica', 'B', 9);
			$this->SetX(100);
			$this->Cell(141, 5, "Página ".$numerapagina  , 0, 0, 'C');
		}
		public function Footer() 
		{
			$linkbd = conectar_v7();
			$linkbd -> set_charset("utf8");
			$sql1="SELECT lema FROM interfaz01 ";
			$res1 = mysqli_query($linkbd,$sql1);
			$row1 = mysqli_fetch_row($res1);
			$lema = $row1[0];

			$sqlr = "SELECT direccion,telefono,web,email FROM configbasica WHERE estado='S'";
			$resp = mysqli_query($linkbd,$sqlr);
			$row = mysqli_fetch_row($resp);
			$direcc = $row[0];
			$telefonos = $row[1];
			$dirweb = $row[3];
			$coemail = $row[2];
			if($direcc!=''){$vardirec="Dirección: $direcc, ";}
			else {$vardirec="";}
			if($telefonos!=''){$vartelef="Telefonos: $telefonos";}
			else{$vartelef="";}
			if($dirweb!=''){$varemail="Email: $dirweb, ";}
			else {$varemail="";}
			if($coemail!=''){$varpagiw="Pagina Web: $coemail";}
			else{$varpagiw="";}
			//$this->SetY(-16);
			$this->SetFont('helvetica', 'I', 8);
			$txt = <<<EOD
$lema
$vardirec $vartelef
$varemail $varpagiw
EOD;
			$this->Write(0, $txt, '', 0, 'C', true, 0, false, false, 0);
		}
	}
	$pdf = new MYPDF('L','mm','LETTER', true, 'UTF-8', false);// create new PDF document
	$pdf->SetDocInfoUnicode (true); 
	// set document information
	$pdf->SetCreator(PDF_CREATOR);
	$pdf->SetAuthor('G&CSAS');
	$pdf->SetTitle('Certificados');
	$pdf->SetSubject('Certificado de Disponibilidad');
	$pdf->SetKeywords('TCPDF, PDF, example, test, guide');
	$pdf->SetMargins(10, 45, 10);// set margins
	$pdf->SetHeaderMargin(45);// set margins
	$pdf->SetFooterMargin(20);// set margins
	$pdf->SetAutoPageBreak(TRUE, 20);// set auto page breaks
	// set some language-dependent strings (optional)
	if (@file_exists(dirname(__FILE__).'/lang/spa.php')) 
	{
		require_once(dirname(__FILE__).'/lang/spa.php');
		$pdf->setLanguageArray($l);
	}
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
	// ---------------------------------------------------------
	$pdf->AddPage();

	//Información entrada servicio
	$consecutivo = $_POST['consecutivo'];
	$sql1 = "SELECT fecha, user, certifica_alm FROM  alm_entrada_servicio_cab WHERE consecutivo = '$consecutivo'";
	$res1 = mysqli_query($linkbd,$sql1);
	$row1 = mysqli_fetch_row($res1);
	$dia1 = date('d',strtotime($row1[0]));
	$dmes1 = date('m',strtotime($row1[0]));
	$dmesl1 = mesletras((int)$dmes1);
	$ano1 = date('Y',strtotime($row1[0]));

	
	$descripcion1 = $_POST['descripcion'];
	$descripcion1a = $_POST['descripcion'];
	
	//Información RP
	$fechadiv = explode('-', $_POST['fechaRP']);
	$vigenciarp = $fechadiv[0];
	$idrp = $_POST['rp'];
	$sql1 = "SELECT tercero, detalle, supervisor FROM  ccpetrp WHERE vigencia = '$vigenciarp' AND consvigencia='$idrp'";
	$res1 = mysqli_query($linkbd,$sql1);
	$row1 = mysqli_fetch_row($res1);

	$fulanito1 = buscatercero($row1[2]);;
	$documento1 = $row1[2];
	$fulanito2 = buscatercero($row1[0]);
	$documento2 = number_format($row1[0],2,',','.');
	$descripcion2 = $row1[1];

	$sqlAlmacen="SELECT funcionario, nomcargo FROM firmaspdf_det WHERE idfirmas='6' AND estado ='S'";
	$resAlmacen=mysqli_query($linkbd,$sqlAlmacen);
	$rowAlmacen=mysqli_fetch_row($resAlmacen);
	$fulanito3 = $rowAlmacen[0];
	$cargof3 = $rowAlmacen[1];

	$sql1 = "SELECT nombrecargo FROM  planaccargos AS T1 INNER JOIN planestructura_terceros AS T2 ON T1.codcargo = T2.codcargo WHERE T2.cedulanit = '$documento1' AND T1.estado = 'S'";
	$res1 = mysqli_query($linkbd,$sql1);
	$row1 = mysqli_fetch_row($res1);
	$cargof1 = $row1[0];

	$sql2 = "SELECT cod_articulo FROM alm_entrada_servicio_articulos WHERE consecutivo = '$consecutivo'";
	$res2 = mysqli_query($linkbd,$sql2);
	$totalarticulos = mysqli_num_rows($res2);

	if($totalarticulos > 0){
		$infoadicional = '<label> de la siguiente manera:</label>';
	}else{
		$infoadicional = '';
	}
	$pdf->ln(6);
	$html = '
		<table style="padding:0 0.5cm">
			<tr>
				<td style="font-size:12px;font-weight:normal;text-align:justify;">
					<p style="margin:0">
						La entidad de '.$pdf->rs.' se permite realizar el acta de salida con la descripción
						'.$descripcion1.', con entrega a la dirección o ubicación
						<span>________________________________________________, con el contrato denominado: </span>
						<b>'.$descripcion2.'</b>, de la siguiente manera:
					</p>
				</td>
			</tr>
		</table>
	';
	$pdf->writeHTML($html, true, false, true, false,'');
    $contador=0;
	if($totalarticulos > 0){
		$pdf->ln(4);
		$pdf->SetX(15);
		$pdf->SetFont('helvetica','B',10);
		$pdf->Cell(25,5,"Código",1,0,'C',0,'',0);
		$pdf->Cell(45,5,"Nombre",1,0,'C',0,'',0);
		$pdf->Cell(40,5,"Marca",1,0,'C',0,'',0);
		$pdf->Cell(40,5,"Modelo",1,0,'C',0,'',0);
		$pdf->Cell(33,5,"Serial",1,0,'C',0,'',0);
		$pdf->Cell(20,5,"Cantidad",1,0,'C',0,'',0);
		$pdf->Cell(41,5,"Valor",1,1,'C',0,'',0);
		$pdf->SetFont('helvetica','',10);
		$sql1 = "SELECT cod_articulo, nom_articulo, cantidad, valor, marca, modelo, serial FROM alm_entrada_servicio_articulos WHERE consecutivo = '$consecutivo' ORDER BY  CONVERT(cod_articulo, INT)";
		$res1 = mysqli_query($linkbd,$sql1);
        
		while($row1 = mysqli_fetch_row($res1)){
            $contador++;
			$altura = 5;
			$altini = 6;
			$ancini = 25;
			$altaux = 0;
			$colst01 = strlen($row1[1]);
			if($colst01 > $ancini){
				$cant_espacios = $colst01/$ancini;
				$rendondear = ceil($cant_espacios);
				$altaux = $altini * $rendondear;
			}
			if($altaux > $altura){
				$altura=$altaux;
			}
			if ($concolor == 0){
				$pdf->SetFillColor(200,200,200);
				$concolor=1;
			}else{
				$pdf->SetFillColor(255,255,255);
				$concolor=0;
			}
			$v = $pdf->gety();
			if($v >= 270){ 
				$pdf->AddPage();
			}
			$pdf->SetX(15);
			$pdf->Cell(25,$altura,$row1[0],1,0,'C',1,'',0);//codigo
			$pdf->MultiCell(45,$altura,$row1[1],1,'L',true,0,'','',true,4,false,true,$altura);//nombre
			$pdf->Cell(40,$altura,$row1[4],1,0,'C',1,'',0);//marca
			$pdf->Cell(40,$altura,$row1[5],1,0,'C',1,'',0);//modelo
			$pdf->Cell(33,$altura,$row1[6],1,0,'C',1,'',0);//serial
			$pdf->Cell(20,$altura,$row1[2],1,0,'C',1,'',0);//cantidad
			$pdf->Cell(41,$altura, "$".number_format($row1[3],2,',','.').' ',1,1,'R',1,'',0);//valor

		}
	}
	$pdf->ln(12);
	$v = $pdf->gety();
	if($v >= 251){ 
		$pdf->AddPage();
	}
	$pdf->SetFont('helvetica','',12);
	$pdf->SetX(19);
	if($dia1 == '01' || $dia1 == '1'){
		$rete = "el primer día";
	}else{
		$rete = "a los ".$dia1." días";
	}
	$sqlr="SELECT funcionario, nomcargo FROM firmaspdf_det WHERE idfirmas='6' AND estado ='S'";
	$res=mysqli_query($linkbd,$sqlr);
	$row=mysqli_fetch_row($res);


	
	$pdf->Cell(149,5,"Lo anterior se firma $rete del mes de $dmesl1 de $ano1",0,1,'L',0,'',0);
	$pdf->ln(10);
	$pdf->SetX(50);
	$pdf->SetFont('helvetica','',8);
	$pdf->Cell(80,5,"QUIEN RECIBE",0,0,'C',0,'',0);
	$pdf->SetX(160);
	$pdf->Cell(80,5,"QUIEN ENTREGA",0,0,'C',0,'',0);
	$pdf->ln(15);
	$pdf->SetX(50);
	$pdf->SetFont('helvetica','B',8);
	$pdf->Cell(80,5,"C.C",'T',0,'C',0,'',0);
	// $pdf->ln(3);
	$pdf->SetX(160);
	$pdf->Cell(80,5,"C.C",'T',2,'C',0,'',0);
	$pdf->SetFont('helvetica','B',8);
	$pdf->SetX(50);
	
	
	
	$fulanito3 = $row[0];
	$cargof3 = $row[1];
	$pdf->ln(16);
	if($contador>2){
		$pdf->ln(20);
	}
	
	$v = $pdf->gety();
	if($v>=170){ 
		$pdf->AddPage();
		$pdf->ln(20);
		$v=$pdf->gety();
	}
	if ($row1[2] == 1) {
		$pdf->SetFont('helvetica','',8);
		$pdf->SetX(50);
		$pdf->Cell(190,5,"QUIEN CERTIFICA",0,0,'C',0,'',0);
		$pdf->ln(15);
		$v = $pdf->gety();
		$pdf->Line(80,$v,200,$v);
		$pdf->SetFont('helvetica','B',8);
		$pdf->SetX(50);
	}

	$pdf->Output('Acta de Recibo.pdf', 'I');
?>


