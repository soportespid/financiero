<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");

	require "comun.inc";
	require "funciones.inc";
	require "conversor.php";
	require "validaciones.inc";

	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");

	session_start();
    date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html lang="es">
	<head>
		<meta charset="utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1"/>
		<meta name="viewport" content="user-scalable=no">
		<title>:: IDEAL 10 - Presupuesto</title>
		<link href="favicon.ico" rel="shortcut icon"/>
        <link href="css/css2.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<script type="text/javascript" src="jquery-1.11.0.min.js"></script> 
        <script type="text/javascript" src="css/programas.js"></script>
        <script type="text/javascript" src="css/calendario.js"></script>
        <script>
		$(window).load(function () {
				$('#cargando').hide();
			});
			function buscar()
			{
				var fechaini = document.getElementById("fechaini").value;
				var fechafin = document.getElementById("fechafin").value;
				
				if(fechaini!='' && fechafin!=''){
					document.form2.oculto.value='3';
					document.form2.submit(); 
				}else{
					despliegamodalm('visible','2','Debe existir una fecha inicial y una fecha final');
				}
				
			}
			
			function reflejar(){
				var numrecaudos = document.getElementsByName("idrecibo[]");
				if(numrecaudos.length >0){
					document.form2.oculto.value='2';
					document.form2.submit(); 
				}else{
					despliegamodalm('visible','2','No existen recibos para reflejar');
				}
			}
			
			function despliegamodalm(_valor,_tip,mensa,pregunta)
			{
				document.getElementById("bgventanamodalm").style.visibility=_valor;
				if(_valor=="hidden"){document.getElementById('ventanam').src="";}
				else
				{
					switch(_tip)
					{
						case "1":
							document.getElementById('ventanam').src="ventana-mensaje1.php?titulos="+mensa;break;
						case "2":
							document.getElementById('ventanam').src="ventana-mensaje3.php?titulos="+mensa;break;
						case "3":
							document.getElementById('ventanam').src="ventana-mensaje2.php?titulos="+mensa;break;
						case "4":
							document.getElementById('ventanam').src="ventana-consulta1.php?titulos="+mensa+"&idresp="+pregunta;break;	
					}
				}
			}
			
			function funcionmensaje()
			{
				
			}
			
			function respuestaconsulta(pregunta)
			{
				switch(pregunta)
				{
					case "1":	document.form2.oculto.value=2;
								document.form2.submit();
								break;
				}
			}
			
        </script>
		 
	</head>
	<body>
		<div id="cargando" style=" position:absolute;left: 40%; bottom: 45%">
			<img src="imagenes/cargando.gif" style=" width: 250px; height: 20px"/>
		</div>
		<IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
        <span id="todastablas2"></span>
        <table>
            <tr><script>barra_imagenes("adm");</script><?php cuadro_titulos();?></tr>	 
            <tr><?php menu_desplegable("adm");?></tr>
        	<tr>
  				<td colspan="3" class="cinta">
				<a href="ccp-reflejaringresosinternosmasivo.php" class="mgbt"><img src="imagenes/add.png" title="Nuevo"/></a>
				<a onClick="document.form2.submit();" href="#" class="mgbt"><img src="imagenes/busca.png" title="Buscar" /></a>
				<a href="#" class="mgbt" onClick="mypop=window.open('adm-principal.php','',''); mypop.focus();"><img src="imagenes/nv.png" title="Nueva Ventana"></a> 
				<a href="adm-comparacomprobantes-presu.php" class="mgbt"><img src="imagenes/iratras.png" title="Atr&aacute;s"></a></td>
         	</tr>	
		</table>
		
		 <div id="bgventanamodalm" class="bgventanamodalm">
            <div id="ventanamodalm" class="ventanamodalm">
                <IFRAME src="" name="ventanam" marginWidth=0 marginHeight=0 frameBorder=0 id="ventanam" frameSpacing=0 style=" width:700px; height:130px; top:200; overflow:hidden;"> 
                </IFRAME>
            </div>
        </div>
		
 		<form name="form2" method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">  
			
			<?php
				$iter='saludo1b';
				$iter2='saludo2b';
			?>
			<table width="100%" align="center"  class="inicio" >
                <tr>
                    <td class="titulos" colspan="9">:: Buscar .: Ingresos internos </td>
                    <td class="cerrar" style='width:7%' onClick="location.href='cont-principal.php'">Cerrar</td>
                    <input type="hidden" name="oculto" id="oculto" value="<?php echo $_POST['oculto']; ?>">
                    <input type="hidden" name="iddeshff" id="iddeshff" value="<?php echo $_POST['iddeshff'];?>">	 
                </tr>                       
                <tr>
                    <td  class="saludo1" >Fecha Inicial: </td>
                    <td><input type="search" name="fechaini" id="fechaini" title="YYYY/MM/DD"  value="<?php echo $_POST['fechaini']; ?>" onchange="" onKeyUp="return tabular(event,this)" onKeyDown="mascara(this,'/',patron,true)"  maxlength="10">&nbsp;<img src="imagenes/calendario04.png" style="width:20px" onClick="displayCalendarFor('fechaini');" class="icobut" title="Calendario"></td>
                    <td  class="saludo1" >Fecha Final: </td>
                    <td ><input type="search" name="fechafin" id="fechafin" title="YYYY/MM/DD"  value="<?php echo $_POST['fechafin']; ?>" onchange="" onKeyUp="return tabular(event,this) " onKeyDown="mascara(this,'/',patron,true)"  maxlength="10">&nbsp;<img src="imagenes/calendario04.png" style="width:20px" onClick="displayCalendarFor('fechafin');"  class="icobut" title="Calendario"></td>  
                    <td style=" padding-bottom: 0em"><em class="botonflecha" onClick="buscar()">Buscar</em></td>
					<td style=" padding-bottom: 0em"><em class="botonflecha" onClick="reflejar()">Reflejar</em></td>
                </tr>
			</table>
			
			<?php
				
				if($_POST['oculto']==3){
					//Variables ocultas para información de tablas
					unset($_POST['idrecibo']);
					unset($_POST['idrecaudo']);
					unset($_POST['vigencia']);
					unset($_POST['valorTesoreria']);

					unset($_POST['valorPresupuesto']);
					unset($_POST['concepto']);
					unset($_POST['fecha']);
					unset($_POST['diferencia']);
					
					$_POST['idrecibo']= array_values($_POST['idrecibo']); 
					$_POST['idrecaudo']= array_values($_POST['idrecaudo']); 
					$_POST['vigencia']= array_values($_POST['vigencia']); 
					$_POST['valorTesoreria']= array_values($_POST['valorTesoreria']); 

					$_POST['valorPresupuesto']= array_values($_POST['valorPresupuesto']); 
					$_POST['concepto']= array_values($_POST['concepto']); 
					$_POST['fecha']= array_values($_POST['fecha']); 
					$_POST['diferencia']= array_values($_POST['diferencia']); 
				
					$queryDate="";
					if(isset($_POST['fechafin']) and isset($_POST['fechaini'])){

						if(!empty($_POST['fechaini']) and !empty($_POST['fechafin'])){
							$fechaInicial=date('Y-m-d',strtotime($_POST['fechaini']));
							$fechaFinal=date('Y-m-d',strtotime($_POST['fechafin']));
							$queryDate="AND TB1.fecha>='".$fechaInicial."' and TB1.fecha<='".$fechaFinal."'";
						}
					}
                    /* $sqlr = "SELECT id_recibos, id_recaudo FROM tesosinreciboscaja WHERE estado != 'N' $queryDate ORDER BY id_recibos DESC"; */
					//valor recaudo, centro de costo, codigo de ingreso
                    $sqlr = "SELECT TB1.id_recibos, TB1.id_recaudo, TB1.valor, TB1.fecha, TB2.concepto, TB1.vigencia FROM tesosinreciboscaja AS TB1, tesosinrecaudos AS TB2 WHERE TB1.estado != 'N' AND TB1.id_recaudo = TB2.id_recaudo $queryDate GROUP BY TB1.id_recibos ORDER BY TB1.id_recibos DESC";
                    /* $sqlr="select T.id_recaudo, T.valortotal, T.estado,T.idcomp,T.concepto,T.vigencia from tesorecaudotransferencia T where T.estado!='N' $queryDate group by T.id_recaudo"; */
                    //echo $sqlr;
					$resp=mysqli_query($linkbd, $sqlr);
					while ($row =mysqli_fetch_row($resp)) 
					{
						$estilo="";
						$stado="";

                        
                        /* $sqlrRecaudo = "SELECT SUM(valor), cc, ingreso FROM tesosinrecaudos_det WHERE id_recaudo = '$row[1]' GROUP BY cc";
                        $respRecaudo = mysqli_query($linkbd, $sqlrRecaudo);
                        $rowRecaudo = mysqli_fetch_row($respRecaudo); */

                        


						$sqlrSinReciboPresu = "SELECT idrecibo, SUM(valor) FROM pptosinrecibocajappto WHERE idrecibo = $row[0] GROUP BY idrecibo"; 
						/* $sql="select C.idrecibo,sum(C.valor),C.cuenta from pptoingtranppto C  where C.idrecibo=$row[0] AND cuenta!=''"; */
						//echo $sql."<br>";
						$respSinReciboPresu=mysqli_query($linkbd, $sqlrSinReciboPresu);
						$rowSinReciboPresu=mysqli_fetch_row($respSinReciboPresu);

						//echo $rw[1]." $rw[0] <br>";
						if($rowSinReciboPresu[0] != null){
							//echo "hola $row[1] - $rw[1]";
							$dif=$row[2]-$rowSinReciboPresu[1];
							$difround = round($dif);
							if ($difround!=0 || $rowSinReciboPresu[2])
							{
								$_POST['idrecibo'][] = $row[0];
								$_POST['idrecaudo'][] = $row[1];
								$_POST['vigencia'][] = $row[5];
								$_POST['valorTesoreria'][] = $row[2];
								$_POST['valorPresupuesto'][] = $rowSinReciboPresu[1];
								$_POST['concepto'][] = $row[4];
								$_POST['fecha'][] = $row[3];
								$_POST['diferencia'][] = $difround;
							}
						}else{
							$_POST['idrecibo'][] = $row[0];
							$_POST['idrecaudo'][] = $row[1];
							$_POST['vigencia'][] = $row[5];
							$_POST['valorTesoreria'][] = $row[2];
							$_POST['valorPresupuesto'][] = 0;
							$_POST['concepto'][] = $row[4];
							$_POST['fecha'][] = $row[3];
							$_POST['diferencia'][] = $row[2];
						}
					
					} 				
				}

			
			echo "<div class='subpantallac5' style='height:55%; width:99.6%; margin-top:0px; overflow-x:hidden' id='divdet'>
				<table class='inicio' align='center' id='valores' >
				<tbody>";
				echo "<tr class='titulos'><td colspan='12'>.:Resultados: ".count($_POST['idrecibo'])."</td></tr>";
				echo "<tr class='titulos ' style='text-align:center;'>
							<td ></td>
							<td ></td>
							<td ></td>
							<td >Tesoreria</td>
							<td >Presupuesto</td>
							<td ></td>
						</tr>
						<tr class='titulos' style='text-align:center;'>
							<td id='col1'>Id Recibo</td>
							<td id='col2'>Concepto</td>
							<td id='col2'>fecha</td>
							<td id='col3'>Valor Total</td>
							<td id='col6'>Valor Total</td>
							<td id='col7'>Diferencia</td>
						</tr>";

				for($k=0; $k<count($_POST['idrecibo']);$k++){
					echo "<input type='hidden' name='idrecibo[]' value='".$_POST['idrecibo'][$k]."'/>";
					echo "<input type='hidden' name='idrecaudo[]' value='".$_POST['idrecaudo'][$k]."'/>";
					echo "<input type='hidden' name='vigencia[]' value='".$_POST['vigencia'][$k]."'/>";
					echo "<input type='hidden' name='valorTesoreria[]' value='".$_POST['valorTesoreria'][$k]."'/>";
					echo "<input type='hidden' name='valorPresupuesto[]' value='".$_POST['valorPresupuesto'][$k]."'/>";
					echo "<input type='hidden' name='concepto[]' value='".$_POST['concepto'][$k]."'/>";
					echo "<input type='hidden' name='fecha[]' value='".$_POST['fecha'][$k]."'/>";
					
					echo"<tr class='$iter' style='text-transform:uppercase;background-color:yellow; ' >
						<td style='width:5%;' id='1'>".$_POST['idrecibo'][$k]."</td>
						<td id='2'>".$_POST['concepto'][$k]."</td>
						<td style='width:10%;' id='3'>".$_POST['fecha'][$k]."</td>
						<td style='text-align:right;width:6%;' id='4'>$".number_format($_POST['valorTesoreria'][$k],2,',','.')."</td>
						<td  style='text-align:right;width:6%;' id='5'>$".number_format($_POST['valorPresupuesto'][$k],2,',','.')."</td>
						<td  style='text-align:right;width:6%;' id='6'>$".number_format($_POST['diferencia'][$k],2,',','.')."</td></tr>";
					$aux=$iter;
					$iter=$iter2;
					$iter2=$aux;
					$resultadoSuma=0.0;
					
				}

				echo "</table></tbody></div>";				
				
			?>

			<?php
			
			if($_POST['oculto']==2){
				//Se actualizan a vacio las variables

				$recibos = "";
				$recibosfallidos = "";
				
				for($n=0; $n<count($_POST['idrecibo']); $n++)
				{
					/* $vigusu=vigencia_usuarios($_SESSION['cedulausu']);
					$vigencia=$vigusu; */
					$codrecibo = $_POST['idrecibo'][$n];
					//*********************CREACION DEL COMPROBANTE CONTABLE ***************************
					$idcomp=mysqli_insert_id($linkbd);

					$sqlr="DELETE FROM pptosinrecibocajappto where idrecibo=$codrecibo";
					mysqli_query($linkbd, $sqlr);	

					$sqlrRecaudo = "SELECT ingreso, valor, cc, fuente FROM tesosinrecaudos_det WHERE id_recaudo = '".$_POST['idrecaudo'][$n]."'";
					$respRecaudo = mysqli_query($linkbd, $sqlrRecaudo);
					while($rowRecaudo = mysqli_fetch_row($respRecaudo)){
						//seccion presupuestal
						$sqlrSeccion = "SELECT id_sp FROM centrocostos_seccionpresupuestal WHERE id_cc = '$rowRecaudo[2]'";
						$respSeccion = mysqli_query($linkbd, $sqlrSeccion);
						$rowSeccion = mysqli_fetch_row($respSeccion);

						$sqlrIng = "SELECT terceros FROM tesoingresos WHERE codigo = '$row[4]'";
						$respIng = mysqli_query($linkbd, $sqlrIng);
						$rowIng = mysqli_fetch_row($respIng);

						if($rowIng[0] == 1)
							continue;

						//cuenta presupuestal, cpc = cuenta clasificadora
						$sqlrIngCuentaCpc = "SELECT cuentapres, cuenta_clasificadora FROM tesoingresos_det WHERE codigo = '$rowRecaudo[0]' AND vigencia = (SELECT MAX(vigencia) FROM tesoingresos_det WHERE codigo = '$rowRecaudo[0]' AND cuentapres != '')";
						$respIngCuentaCpc = mysqli_query($linkbd, $sqlrIngCuentaCpc);
						$rowIngCuentaCpc = mysqli_fetch_row($respIngCuentaCpc);

						//fuente
						$fuente = '';
						if($rowRecaudo[3] == ''){
							$sqlrFuente = "SELECT fuente FROM tesoingresos_fuentes WHERE codigo = '$rowRecaudo[0]' AND estado = 'S'";
							$respFuente = mysqli_query($linkbd, $sqlrFuente);
							$rowFuente = mysqli_fetch_row($respFuente);
							$fuente = $rowFuente[0];
						}else{
							$fuente = $rowRecaudo[3];
						}


						if($rowIngCuentaCpc[0] != '' && $fuente != ''){
							$sqlr="INSERT into pptosinrecibocajappto (cuenta,idrecibo,valor,vigencia, fuente, productoservicio, medio_pago, seccion_presupuestal, vigencia_gasto) values('$rowIngCuentaCpc[0]',$codrecibo,'$rowRecaudo[1]', '".$_POST['vigencia'][$n]."', '$fuente', '$rowIngCuentaCpc[1]', 'CSF', '$rowSeccion[0]', '1')";
							mysqli_query($linkbd, $sqlr);
							$recibos.=($_POST['idrecibo'][$n])." ";
						}else{
							$recibosfallidos.=($_POST['idrecibo'][$n])." ";	
						}

					}

					
					//******************* DETALLE DEL COMPROBANTE CONTABLE *********************
					

					//Busqueda para saber si el codigo de ingresos es para tercero, no lleva presupuesto y continua con la siquiente iteracion.

					

					
					/* 
					
					$sqlrRecaudoDet = "SELECT ingreso,valor From tesorecaudotransferencia_det WHERE id_recaudo=$codrecibo";
					//echo $sqlrRecaudoDet." -- <br>";
					$resRecaudoDet = mysqli_query($linkbd, $sqlrRecaudoDet);	 */
					
					/* while($rowRecaudoDet = mysqli_fetch_row($resRecaudoDet))
					{
						//***** BUSQUEDA INGRESO ********
						$sqlri="SELECT * FROM tesoingresos_det WHERE codigo='".$rowRecaudoDet[0]."' and vigencia=(SELECT MAX(vigencia) FROM tesoingresos_det WHERE codigo='".$rowRecaudoDet[0]."') ";
						if(!$resi=mysqli_query($linkbd, $sqlri))
						{
							$recibosfallidos.=($_POST['recaudocc'][$n])." ";	
						}
						//echo "$sqlri <br>";	    
						while($rowi=mysqli_fetch_row($resi))
						{
							//**** busqueda concepto contable*****
							if($rowi[6]!="")
							{
								$porce=$rowi[5];
								$vi=$rowRecaudoDet[1]*($porce/100);
								//****creacion documento presupuesto ingresos
								$sqlr="INSERT into pptoingtranppto (cuenta,idrecibo,valor,vigencia) values('$rowi[6]',$codrecibo,$vi,'".$_POST['vigenciaComp'][$n]."')";
								mysqli_query($linkbd, $sqlr);				  
								//echo "Conc: $sqlr <br>";
							}else{
								$recibosfallidos.=($_POST['recaudocc'][$n])." ";	
							}
						}
					} */
					
				}
		echo "<table class='inicio'><tr><td class='saludo1'><center>Se han reflejado los Recibos de Caja: $recibos <img src='imagenes/confirm.png'><script></script></center></td></tr></table>"; 
		echo "<table class='inicio'><tr><td class='saludo1'><center>No se pudieron reflejar los Recibos de Caja: $recibosfallidos <img src='imagenes/del.png'><script></script></center></td></tr></table>"; 
							
	}
			
?>
			
		
        </form> 

</body>
</html>