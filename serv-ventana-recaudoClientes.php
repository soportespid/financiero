<?php
require "comun.inc";
require "funciones.inc";
require "funcionesSP.inc.php";
session_start();
$linkbd = conectar_v7();	
cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
header("Cache-control: private");
date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Clientes - Servicios P&uacute;blicos</title>
    <link href="css/css2.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
    <link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="css/calendario.js"></script>
    
    <script>
        function ponprefijo(codigoFactura)
        {
            parent.document.form2.numeroFactura.value = codigoFactura;
            parent.document.form2.numeroFactura.focus();
            parent.despliegamodal2("hidden");
            paren.document.form2.submit();
        }
    </script>

    <?php titlepag();?>
</head>
<body>
    <form action="" method="post">
        <div>
            <table class="inicio">
                <tr>
                    <td class="titulos" colspan="6">Clientes con facturas pendientes</td>

                    <td class="cerrar">
                        <a onClick="parent.despliegamodal2('hidden');">&nbsp;Cerrar</a>
                    </td>
                </tr>

                <tr>
                    <td colspan="7" class="titulos2">Por Descripcion</td>
                </tr>

                <tr>
                    <td class="saludo1">Nombre o Documento:</td>

                    <td>
                        <input name="nombre_documento" type="text">
                        <input type="hidden" name="oculto" id="oculto" value="1" >
                    </td>

                    <td class="saludo1">C&oacute;digo de Usuario:</td>

                    <td>
                        <input type="text" name="cod_usuario">
                    </td>

                    <td class="saludo1">Direcci&oacute;n</td>

                    <td>
                        <input type="text" name="direccion">
                        <input type="submit" name="Submit" value="Buscar">
                    </td>
                </tr>
            </table>
        </div>

        <div class="subpantalla" style="height: 400px;">
            <table class="inicio">
                <tr>
					<td height="25" colspan="8" class="titulos">:. Resultados Busqueda</td>
				</tr>

                <tr>
					<td class="titulos2" width="10%" style="text-align: center;">Codigo Usuario</td>
                    <td class="titulos2" width="10%" style="text-align: center;">Codigo Catastral</td>
					<td class="titulos2" style="text-align: center;">Nombre</td>
					<td class="titulos2" style="text-align: center;">Documento</td>	  
					<td class="titulos2" style="text-align: center;">Barrio</td>
                    <td class="titulos2" style="text-align: center;">Direcci&oacute;n</td>
                    <td class="titulos2" style="text-align: center;">Estrato</td>
				</tr>

                <?php
                    //condiciones
                    if($_POST['nombre_documento'] != '')
                    {
                        $nombredividido = array();
                        $nombredividido = explode(" ", $_POST['nombre_documento']);
                        
                        for ($i=0; $i < count($nombredividido); $i++) 
                        { 
                            $busqueda = '';
                            $busqueda = "AND concat_ws(' ', T.nombre1, T.nombre2, T.apellido1, T.apellido2, T.razonsocial) LIKE '%$nombredividido[$i]%' ";

                            $condicion = $condicion . $busqueda;
                        }
                    }
                    elseif($_POST['cod_usuario'] != '')
                    {
                        $condicion = "AND C.cod_usuario LIKE '%$_POST[cod_usuario]%'";
                    }
                    elseif($_POST['direccion'] != '')
                    {
                        $condicion = "AND D.direccion LIKE '%$_POST[direccion]%'";
                    }

                    //estilos
                    $co = 'saludo1a';
					$co2 = 'saludo2';	

                    //query
                    $corte = buscaCorteActivo();

                    $sqlr = "SELECT C.cod_usuario, C.cod_catastral, T.nombre1, T.nombre2, T.apellido1, T.apellido2, T.razonsocial, T.cedulanit, B.nombre, D.direccion, E.descripcion, F.estado_pago, F.numero_facturacion FROM srvclientes AS C INNER JOIN terceros AS T ON C.id_tercero = T.id_tercero INNER JOIN srvbarrios AS B ON C.id_barrio = B.id INNER JOIN srvdireccion_cliente AS D ON C.id = D.id_cliente INNER JOIN srvestratos AS E ON C.id_estrato = E.id INNER JOIN srvcortes_detalle AS F ON F.id_cliente = C.id WHERE F.estado_pago = 'S' $condicion ORDER BY F.numero_facturacion";
                    $resp = mysqli_query($linkbd,$sqlr);
                    while($row = mysqli_fetch_row($resp))
                    {
                        if($row[6] != '')
                        {
                            $nombre = $row[6];
                        }
                        else
                        {
                            $nombre = $row[2]. ' ' .$row[3]. ' ' .$row[4]. ' ' .$row[5];
                        }

                        echo" 
						<tr class='$co' onClick=\"javascript:ponprefijo('$row[12]')\">
                            <td>$row[0]</td>
                            <td>$row[1]</td>
                            <td>$nombre</td>
                            <td>$row[7]</td>
                            <td>$row[8]</td>
                            <td>$row[9]</td>
                            <td>$row[10]</td>
						</tr> ";

                        $aux=$co;
						$co=$co2;
						$co2=$aux;
                    }
                ?>
            </table>
        </div>
    </form>
</body>
</html>