<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require 'comun.inc';
	require 'funciones.inc';
	session_start();
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html lang=es>
	<head>
		<meta charset="utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1"/>
		<meta name="viewport" content="user-scalable=no">
		<title>:: IDEAL 10 - Activos Fijos</title>
		<link href="favicon.ico" rel="shortcut icon"/>
		<link href="css/css2z.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
        <link href="css/tabs.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" href="sweetalert2/dist/sweetalert2.min.css">
        <script src="sweetalert2/dist/sweetalert2.min.js"></script>
		<script src="vue/vue.min.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script src="css/calendario.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
        <script>
            function pdf(){
				document.form2.action = "acti-certificadoResponsablePDF.php";
				document.form2.target = "_BLANK";
				document.form2.submit(); 
				document.form2.action = "";
				document.form2.target = "";
			}
        </script>
		<style>
			input[type=number]::-webkit-inner-spin-button,
			input[type=number]::-webkit-outer-spin-button { 
				-webkit-appearance: none; 
				margin: 0; 
			}
			input[type=number]{
				text-align:right;
				box-sizing: border-box;
				font: 120% sans-serif;
				/* width: 100% !important; */
			}
			[v-cloak]{display : none;}

			.centrarSelect {

				height: 30px !important;
				text-align-last:center !important;
			}

			.aumentarTamaño {

				font-size:15px;
			}

            .tamano01 {
                text-align: center !important;
            }
		</style>
	</head>
	<body>
		<header>
			<table>
				<tr><script>barra_imagenes("acti");</script><?php cuadro_titulos();?></tr>
			</table>
		</header>

		<form name="form2" method="post" action="">
			<section id="myapp" v-cloak >
				<nav>
					<table>
						<tr><?php menu_desplegable("acti");?></tr>
						<tr>
							<td colspan="3" class="cinta">
								<img src="imagenes/add2.png" class="mgbt1" title="Nuevo">
								<img src="imagenes/guardad.png" title="Guardar" class="mgbt1">
								<img src="imagenes/buscad.png" v-on:click="location.href=''" class="mgbt1" title="Buscar">
								<img src="imagenes/nv.png" onClick="mypop=window.open('acti-principal.php','',''); mypop.focus();" class="mgbt" title="Nueva Ventana">
								<a href="acti-gestiondelosactivos"><img src="imagenes/iratras.png" class="mgbt" alt="Atrás"></a>
							</td>
						</tr>
					</table>
				</nav>

				<article>
                    <div>
                        <table class="inicio">
                            <tr>
                                <td class="titulos" colspan="12">.: Certificado de activos</td>
								<td class="cerrar" style="width:4%" onClick="location.href='acti-principal.php'">Cerrar</td>
                            </tr>

                            <tr>
                                <td class="textonew01" style="width:3.5cm;">.: Número de Activo:</td>
                                <td style="width: 11%;">
                                    <input type="text" name="numeroActivo" v-model="numeroActivo" @dblclick="despliegoModalActivos" style="text-align: center;" class="colordobleclik" readonly>
                                </td>
                                <td>
                                    <input type="text" name="nomActivo" v-model="nomActivo" style="width: 100%;" readonly>
                                </td>

                                <td class="textonew01" style="width:3.5cm;">.: Fecha:</td>
                                <td>
                                    <input type="text" name="fechaRegistro"  value="<?php echo $_POST['fechaRegistro']?>" onKeyUp="return tabular(event,this)" id="fechaRegistro" title="DD/MM/YYYY" onDblClick="displayCalendarFor('fechaRegistro');" class="colordobleclik" autocomplete="off" onChange=""  readonly>
                                </td>
                            </tr>

                            <tr>
                                <td class="textonew01" style="width:3.5cm;">.: Actual responsable:</td>
                                <td style="width: 11%;">
                                    <input type="text" name="actualResponsable" v-model="actualResponsable" style="text-align: center;" readonly>
                                </td>
                                <td>
                                    <input type="text" name="actualNomResponsable" v-model="actualNomResponsable" style="width: 100%;" readonly>
                                </td>

                                <td class="textonew01" style="width:3.5cm;">.: Nuevo responsable:</td>
                                <td style="width: 11%;">
                                    <input type="text" name="responsable" v-model="responsable" style="text-align: center;" @dblclick="despliegoTerceros" class="colordobleclik" readonly>
                                </td>
                                <td>
                                    <input type="text" name="nomResponsable" v-model="nomResponsable" style="width: 100%;" readonly>
                                </td>
                            </tr>

                            <tr>
                                <td class="textonew01" style="width:3.5cm;">.: Motivo de cambio:</td>
                                <td colspan="2">
                                    <textarea name="motivo" v-model="motivo" style="width:100%"></textarea>
                                </td>
								<td></td>
								
								<td style=" height: 30px;"><em class="botonflecha" v-on:click="generaYguarda">Generar PDF</em></td>
                            </tr>
                        </table>

						<div class='subpantalla' style='height:50vh; width:99.2%; margin-top:0px; resize: vertical;'>
                            <table class=''>
                                <thead>
                                    <tr style="text-align:Center;">
                                        <th class="titulosnew02"  colspan="10">Historial de responsables</th>
                                    </tr>
                                    <tr>
										<th class="titulosnew00" style="width:30%;">Responsable</th>
										<th class="titulosnew00" style="width:10%;">Fecha</th>
                                        <th class="titulosnew00">Motivo</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr v-for="(historial,index) in historialResponsables" v-bind:class="(index % 2 ? 'saludo1a' : 'saludo2')" style="font: 100% sans-serif;">
										<td style="width:30%; text-align:center;">{{ historial[0] }}</td>
										<td style="width:10%; text-align:center;">{{ historial[1] }}</td>
										<td style="text-align:center;">{{ historial[2] }}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div> 

					<div id="cargando" v-if="loading" class="loading">
						<span>Cargando...</span>
					</div>

                    <div v-show="showModalResponsables">
						<transition name="modal">
							<div class="modal-mask">
								<div class="modal-wrapper">
									<div class="modal-container">
										<table class="inicio ancho">
											<tr>
												<td class="titulos" colspan="2" >Listado terceros</td>
												<td class="cerrar" style="width:7%" @click="showModalResponsables = false">Cerrar</td>
											</tr>

                                            <tr>
                                                <td class="textonew01" style="text-align:center;">Cedula:</td>
                                                <td>
                                                    <input type="text" v-model="searchCedula" v-on:keyup="filtroResponsable" placeholder="Buscar por número de cedula" style="width: 100%;">
                                                </td>
                                            </tr>
										</table>
										<table class='tablamv'>
											<thead>
												<tr style="text-align:center;">
													<th class="titulosnew00" style="width: 20%;">Documento</th>
													<th class="titulosnew00">Nombre o razón social</th>
                                                    <th style="width: 1%;"></th>
												</tr>
											</thead>
											<tbody>
												<tr v-for="(tercero,index) in terceros" v-bind:class="index % 2 ? 'saludo1a' : 'saludo2'" v-on:click="seleccionaResponsable(tercero[0], tercero[1])">
													<td style="width:20%; text-align:center;"> {{ tercero[0] }} </td>
													<td style="text-align:left;"> {{ tercero[1] }} </td>
												</tr>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</transition>
					</div>  

					<div v-show="showModalActivos">
						<transition name="modal">
							<div class="modal-mask">
								<div class="modal-wrapper">
									<div class="modal-container">
										<table class="inicio ancho">
											<tr>
												<td class="titulos" colspan="2" >Listado de activos</td>
												<td class="cerrar" style="width:7%" @click="showModalActivos = false">Cerrar</td>
											</tr>

                                            <tr>
                                                <td class="textonew01" style="text-align:center;">Activo:</td>
                                                <td>
                                                    <input type="text" v-model="searchActivo" v-on:keyup="filtroActivo" placeholder="Buscar por placa o nombre" style="width: 100%;">
                                                </td>
                                            </tr>
										</table>
										<table class='tablamv'>
											<thead>
												<tr style="text-align:center;">
													<th class="titulosnew00" style="width: 20%;">Número de activo</th>
													<th class="titulosnew00">Nombre</th>
                                                    <th style="width: 1%;"></th>
												</tr>
											</thead>
											<tbody>
												<tr v-for="(activo,index) in activos" v-bind:class="index % 2 ? 'saludo1a' : 'saludo2'" v-on:click="seleccionaActivo(activo)">
													<td style="width:20%; text-align:center;"> {{ activo[0] }} </td>
													<td style="text-align:left;"> {{ activo[1] }} </td>
												</tr>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</transition>
					</div>  

				</article>
			</section>
		</form>
        
		<script src="node_modules/read-excel-file/bundle/read-excel-file.min.js"></script>
		<script src="node_modules/xlsx/dist/xlsx.full.min.js"></script>
		<script src="Librerias/vue/vue.min.js"></script>
		<script src="Librerias/vue/axios.min.js"></script>
		<script type="module" src="activos_fijos/cambioResponsable/acti-trasladoResponsable.js"></script>
        
	</body>
</html>