<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");

	require 'comun.inc';
	require 'funciones.inc';

	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");

	session_start();
    date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html lang="es">
	<head>
		<meta charset="utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1"/>
		<meta name="viewport" content="user-scalable=no">
		<title>:: IDEAL 10 - Contabilidad</title>
		<link href="favicon.ico" rel="shortcut icon"/>
		<link href="css/css2.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<script type = "text/javascript" src = "jquery-1.11.0.min.js"></script>
		<script type="text/javascript" src="css/calendario.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script type="text/javascript" src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script>
			function editdc(num, edit, filas, filtro)
			{
				var scrtop=$('#divdet').scrollTop();
				var altura=$('#divdet').height();
				var numpag=$('#nummul').val();
				var limreg=$('#numres').val();
				if((numpag <= 0)||(numpag == "")){numpag = 0;}
				if((limreg == 0)||(limreg == "")){limreg = 10;}
				numpag++;
				if(edit == 0){location.href="ccp-destinodecompraeditar.php?id=" + num + "&scrtop=" + scrtop + "&totreg=" + filas + "&altura=" + altura + "&numpag=" + numpag + "&limreg=" + limreg + "&filtro=" + filtro;}
				else{location.href="ccp-visualizardestinodecompra.php?idnumero="+num;}
			}
			function buscarbotonfiltro()
			{
				if((document.form2.fecha1.value != "" && document.form2.fecha2.value == "") || (document.form2.fecha1.value == "" && document.form2.fecha2.value != "")){alert("Falta digitar fecha");}
				else
				{
					document.getElementById('numpos').value=0;
					document.getElementById('nummul').value=0;
					document.form2.submit();
				}
			}
		</script>
		<?php
			titlepag();
			$scrtop=$_GET['scrtop'];
			if($scrtop == "") $scrtop = 0;
			echo"<script>
				window.onload=function(){
					$('#divdet').scrollTop(".$scrtop.")
				}
			</script>";
			$gidcta = $_GET['idcta'];
			if(isset($_GET['filtro'])){$_POST['nombre'] = $_GET['filtro'];}
		?>
	</head>
	<body>
		<IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
		<span id="todastablas2"></span>
		<table>
			<tr><script>barra_imagenes("cont");</script><?php cuadro_titulos();?></tr>
			<tr><?php menu_desplegable("cont");?></tr>
		</table>
        <div class="bg-white group-btn p-1"><button type="button" onclick="location.href='ccp-destinodecompra.php'" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
            <span>Nuevo</span>
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960"><path d="M440-440H200v-80h240v-240h80v240h240v80H520v240h-80v-240Z"></path></svg>
        </button><button type="button" onclick="mypop=window.open('plan-agenda.php','','');mypop.focus()" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
            <span class="group-hover:text-white">Agenda</span>
            <svg class="fill-black group-hover:fill-white w-5 h-5" xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960"><path d="M560-564v-68q33-14 67.5-21t72.5-7q26 0 51 4t49 10v64q-24-9-48.5-13.5T700-600q-38 0-73 9.5T560-564Zm0 220v-68q33-14 67.5-21t72.5-7q26 0 51 4t49 10v64q-24-9-48.5-13.5T700-380q-38 0-73 9t-67 27Zm0-110v-68q33-14 67.5-21t72.5-7q26 0 51 4t49 10v64q-24-9-48.5-13.5T700-490q-38 0-73 9.5T560-454ZM260-320q47 0 91.5 10.5T440-278v-394q-41-24-87-36t-93-12q-36 0-71.5 7T120-692v396q35-12 69.5-18t70.5-6Zm260 42q44-21 88.5-31.5T700-320q36 0 70.5 6t69.5 18v-396q-33-14-68.5-21t-71.5-7q-47 0-93 12t-87 36v394Zm-40 118q-48-38-104-59t-116-21q-42 0-82.5 11T100-198q-21 11-40.5-1T40-234v-482q0-11 5.5-21T62-752q46-24 96-36t102-12q58 0 113.5 15T480-740q51-30 106.5-45T700-800q52 0 102 12t96 36q11 5 16.5 15t5.5 21v482q0 23-19.5 35t-40.5 1q-37-20-77.5-31T700-240q-60 0-116 21t-104 59ZM280-494Z"></path></svg>
        </button><button type="button" onclick="window.open('cont-principal');" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
            <span>Nueva ventana</span>
            <svg class="fill-black group-hover:fill-white w-5 h-5" xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960"><path d="M320-120v-80h80v-80H160q-33 0-56.5-23.5T80-360v-400q0-33 23.5-56.5T160-840h640q33 0 56.5 23.5T880-760v400q0 33-23.5 56.5T800-280H560v80h80v80H320ZM160-360h640v-400H160v400Zm0 0v-400 400Z"></path></svg>
        </button><button type="button" onclick="mypop=window.open('/financiero/ccp-buscardestinodecompra.php','','');mypop.focus();" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
            <span class="group-hover:text-white">Duplicar pantalla</span>
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960"><path d="M640-160v-360H160v360h480Zm80-200v-80h80v-360H320v200h-80v-200q0-33 23.5-56.5T320-880h480q33 0 56.5 23.5T880-800v360q0 33-23.5 56.5T800-360h-80ZM160-80q-33 0-56.5-23.5T80-160v-360q0-33 23.5-56.5T160-600h480q33 0 56.5 23.5T720-520v360q0 33-23.5 56.5T640-80H160Zm400-603ZM400-340Z"></path></svg>
        </button><button type="button" onclick="location.href='ccp-destinodecompra.php'" class="btn btn-success btn-success-hover d-flex justify-between align-items-center">
            <span>Atras</span>
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960"><path d="M760-200v-160q0-50-35-85t-85-35H273l144 144-57 56-240-240 240-240 57 56-144 144h367q83 0 141.5 58.5T840-360v160h-80Z"></path></svg>
        </button></div>
		<form name="form2" method="post" action="">
			<?php
				if($_POST['oculto']=="")
				{

				}
				if($_GET['numpag']!="")
				{
					if($_POST['oculto']!=2)
					{
						$_POST['numres']=$_GET['limreg'];
						$_POST['numpos']=$_GET['limreg']*($_GET['numpag']-1);
						$_POST['nummul']=$_GET['numpag']-1;
					}
				}
				else
				{
					if($_POST['nummul']=="")
					{
						$_POST['numres']=10;
						$_POST['numpos']=0;
						$_POST['nummul']=0;
					}
				}
			?>
			<table width="100%" align="center"  class="inicio" >
				<tr>
					<td class="titulos" colspan="9">:: Buscar destino de compra </td>
					<td class="cerrar" style='width:7%' onClick="location.href='cont-principal.php'">Cerrar</td>
					<input type="hidden" name="oculto" id="oculto" value="1">
				</tr>
				<tr>
					<td class="tamano01">N&uacute;mero o nombre:</td>
					<td><input type="text" name="nombre" id="nombre" value="<?php echo $_POST['nombre'];?>" style="width:98%;height:30px;"/></td>
					<td class="tamano01">Fecha inicial:</td>
					<td><input name="fecha1" type="text" value="<?php echo $_POST['fecha1']?>" maxlength="8"  onKeyPress="javascript:return solonumeros(event)" onKeyUp="return tabular(event,this)"  id="fc_1198971545" onKeyDown="mascara(this,'/',patron,true)" style="width:75%;height:30px;" title="DD/MM/YYYY" placeholder="DD/MM/YYYY"/>&nbsp;<img src="imagenes/calendario04.png" onClick="displayCalendarFor('fc_1198971545');" title="Calendario" class="icobut"/></td>
					<td class="tamano01">Fecha final:</td>
					<td><input name="fecha2" type="text" value="<?php echo $_POST['fecha2']?>" maxlength="8"  onKeyPress="javascript:return solonumeros(event)" onKeyUp="return tabular(event,this)"  id="fc_1198971546" onKeyDown="mascara(this,'/',patron,true)" style="width:75%;height:30px;" title="DD/MM/YYYY" placeholder="DD/MM/YYYY"/>&nbsp;<img src="imagenes/calendario04.png" onClick="displayCalendarFor('fc_1198971546');" title="Calendario" class="icobut"/></td>
					<td><input type="button" name="bboton" onClick="buscarbotonfiltro();" value="&nbsp;&nbsp;Buscar&nbsp;&nbsp;" /></td>
				</tr>
			</table>
			<input type="hidden" name="numres" id="numres" value="<?php echo $_POST['numres'];?>"/>
			<input type="hidden" name="numpos" id="numpos" value="<?php echo $_POST['numpos'];?>"/>
			<input type="hidden" name="nummul" id="nummul" value="<?php echo $_POST['nummul'];?>"/>
			<div class="subpantalla" style="height:65.5%; width:99.5%; overflow-x:hidden;" id="divdet">
				<?php
					preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST['fecha1'],$fecha);
					$fechai="$fecha[3]-$fecha[2]-$fecha[1]";
					preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST['fecha2'],$fecha);
					$fechaf="$fecha[3]-$fecha[2]-$fecha[1]";
					$crit1="";
					if ($_POST['nombre'] != ""){$crit1="AND concat_ws(' ', id, consvigencia, detalle) LIKE '%".$_POST['nombre']."%'";}
					$sqlr="SELECT id, consvigencia, tercero, detalle, fecha, vigencia, estado, editable FROM ccpetdc WHERE estado !='' $crit1 ORDER BY id DESC";
					if(isset($_POST['fecha1']) && isset($_POST['fecha2']))
					{
						if(!empty($_POST['fecha1']) && !empty($_POST['fecha2']))
						{
							$sqlr="SELECT id, consvigencia, tercero, detalle, fecha, vigencia, estado, editable FROM ccpetdc WHERE estado != '' $crit1 AND fecha BETWEEN '$fechai' AND '$fechaf' ORDER BY id DESC";
						}
					}
					$resp = mysqli_query($linkbd,$sqlr);
					$_POST['numtop']=mysqli_num_rows($resp);
					$nuncilumnas=ceil($_POST['numtop']/$_POST['numres']);
					$cond2="";
					if ($_POST['numres']!="-1"){$cond2="LIMIT ".$_POST['numpos'].",".$_POST['numres'];}
					$sqlr = "SELECT id, consvigencia, tercero, detalle, fecha, vigencia, estado, editable FROM ccpetdc WHERE estado<>'' $crit1 ORDER BY id DESC $cond2";
					if(isset($_POST['fecha1']) && isset($_POST['fecha2']))
					{
						if(!empty($_POST['fecha1']) && !empty($_POST['fecha2']))
						{
							$sqlr="SELECT id, consvigencia, tercero, detalle, fecha, vigencia, estado, editable FROM ccpetdc WHERE estado<>'' $crit1 AND fecha BETWEEN '$fechai' AND '$fechaf' ORDER BY id DESC $cond2";
						}
					}
					$resp = mysqli_query($linkbd,$sqlr);
					$con=1;
					$numcontrol=$_POST['nummul']+1;
					if(($nuncilumnas==$numcontrol)||($_POST['numres']=="-1"))
					{
						$imagenforward="<img src='imagenes/forward02.png' style='width:17px;cursor:default;'>";
						$imagensforward="<img src='imagenes/skip_forward02.png' style='width:16px;cursor:default;'>";
					}
					else
					{
						$imagenforward="<img src='imagenes/forward01.png' style='width:17px;cursor:pointer;' title='Siguiente' onClick='numsiguiente()'>";
						$imagensforward="<img src='imagenes/skip_forward01.png' style='width:16px;cursor:pointer;' title='Fin' onClick='saltocol(\"$nuncilumnas\")'>";
					}
					if(($_POST['numpos']==0)||($_POST['numres']=="-1"))
					{
						$imagenback="<img src='imagenes/back02.png' style='width:17px;cursor:default;'>";
						$imagensback="<img src='imagenes/skip_back02.png' style='width:16px;cursor:default;'>";
					}
					else
					{
						$imagenback="<img src='imagenes/back01.png' style='width:17px;cursor:pointer;' title='Anterior' onClick='numanterior();'>";
						$imagensback="<img src='imagenes/skip_back01.png' style='width:16px;cursor:pointer;' title='Inicio' onClick='saltocol(\"1\")'>";
					}
					echo "
					<table class='inicio' align='center' width='99%'>
						<tr>
							<td colspan='8' class='titulos'>.: Resultados Busqueda:</td>
							<td class='submenu'>
								<select name='renumres' id='renumres' onChange='cambionum();' style='width:100%'>
									<option value='10'"; if ($_POST['renumres']=='10'){echo 'selected';} echo ">10</option>
									<option value='20'"; if ($_POST['renumres']=='20'){echo 'selected';} echo ">20</option>
									<option value='30'"; if ($_POST['renumres']=='30'){echo 'selected';} echo ">30</option>
									<option value='50'"; if ($_POST['renumres']=='50'){echo 'selected';} echo ">50</option>
									<option value='100'"; if ($_POST['renumres']=='100'){echo 'selected';} echo ">100</option>
									<option value='-1'"; if ($_POST['renumres']=='-1'){echo 'selected';} echo ">Todos</option>
								</select>
							</td>
						</tr>
						<tr><td colspan='3'>Archivos encontrados: $_POST[numtop]</td></tr>
						<tr>
							<td class='titulos2' width='45'><center>ITEM</center></td>
							<td class='titulos2' width='45'><center>N&deg; RP</center></td>
							<td class='titulos2' ><center>N&deg; TERCERO</center></td>
							<td class='titulos2' ><center>NOMBRE TERCERO</center></td>
							<td class='titulos2' ><center>DETALLE</center></td>
							<td class='titulos2' width='80'><center>FECHA</center></td>
							<td class='titulos2' ><center>VIGENCIA</center></td>
							<td class='titulos2' ><center>ESTADO</center></td>
							<td class='titulos2' ><center>EDITABLE</center></td>
						</tr>";
					$iter='saludo1a';
					$iter2='saludo2';
					$filas = 1;
					while ($row =mysqli_fetch_row($resp))
					{
						$con2=$con+ $_POST['numpos'];
						$sqlterc = "SELECT nombre1, nombre2, apellido1, apellido2, razonsocial FROM terceros WHERE cedulanit = $row[2] ";
						$resterc = mysqli_query($linkbd,$sqlterc);
						$rowter = mysqli_fetch_row($resterc);
						if($rowter[4] != ""){$tercero = $rowter[4];}
						else {$tercero = "$rowter[0] $rowter[1] $rowter[2] $rowter[3]";}
						switch($row[6])
						{
							case 'S':	$estadoimg="<img src='imagenes/sema_verdeON.jpg' title='ACTIVO' style='width:20px;'/>";
										break;
							case 'C':	$estadoimg="<img src='imagenes/sema_azul1ON.jpg' title='COMPLETO' style='width:20px;'/>";
										break;
							case 'N':	$estadoimg="<img src='imagenes/sema_rojoON.jpg' title='ANULADO' style='width:20px;'/>";
										break;
							case 'R':	$estadoimg="<img src='imagenes/sema_rojoON.jpg' title='REVERSADO' style='width:20px;'/>";
										break;
						}
						switch($row[7])
						{
							case 0:		$guardaredit = "onClick=\"javascript:editdc('$row[0]')";
										$editarimg = "<img src='imagenes/b_edit.png' title='EDITABLE' style='width:20px;'class='icobut'/>";
										$edit = 0;
										break;
							case 1:		$guardaredit = "";
										$editarimg = "<img src='imagenes/b_editd.png' title='NO EDITABLE' style='width:20px;'/>";
										$edit = 1;
										break;
						}
						$numfil = $filas;
						$filtro = $_POST['nombre'];
						if($gidcta != "")
						{
							if($gidcta == $row[0]){$estilo = 'background-color:yellow';}
							else {$estilo = "";}
						}
						else {$estilo = "";}
						echo "
						<tr class='$iter' onClick=\"javascript:editdc('$row[0]', '$edit', '$numfil', '$filtro')\" style='text-transform:uppercase; $estilo'>
							<td style='text-align:center;'>".strtoupper($row[0])."</td>
							<td style='text-align:center;'>".strtoupper($row[1])."</td>
							<td style='text-align:center;'>".strtoupper($row[2])."</td>
							<td>".strtoupper($tercero)."</td>
							<td>".strtoupper($row[3])."</td>
							<td style='text-align:center;'>".strtoupper($row[4])."</td>
							<td style='text-align:center;'>".strtoupper($row[5])."</td>
							<td style='text-align:center;'>".strtoupper($estadoimg)."</td>
							<td  style='text-align:center;'>".strtoupper($editarimg)."</td>
						</tr>";
						$con+=1;
						$aux=$iter;
						$iter=$iter2;
						$iter2=$aux;
						$filas++;
					}
					if ($_POST['numtop']==0)
					{
						echo "
						<table class='inicio'>
							<tr>
								<td class='saludo1' style='text-align:center;width:100%'><img src='imagenes\alert.png' style='width:25px'>No hay coincidencias en la b&uacute;squeda $tibusqueda<img src='imagenes\alert.png' style='width:25px'></td>
							</tr>
						</table>";
					}
 					echo"
						</table>
						<table class='inicio'>
							<tr>
								<td style='text-align:center;'>
									<a>$imagensback</a>&nbsp;
									<a>$imagenback</a>&nbsp;&nbsp;";
					if($nuncilumnas<=9){$numfin=$nuncilumnas;}
					else{$numfin=9;}
					for($xx = 1; $xx <= $numfin; $xx++)
					{
						if($numcontrol<=9){$numx=$xx;}
							else{$numx=$xx+($numcontrol-9);}
						if($numcontrol==$numx){echo"<a onClick='saltocol(\"$numx\")'; style='color:#24D915;cursor:pointer;'> $numx </a>";}
							else {echo"<a onClick='saltocol(\"$numx\")'; style='color:#000000;cursor:pointer;'> $numx </a>";}
					}
					echo "		&nbsp;&nbsp;<a>$imagenforward</a>
									&nbsp;<a>$imagensforward</a>
								</td>
							</tr>
						</table>";
				?>
			</div>
			<input type="hidden" name="numtop" id="numtop" value="<?php echo $_POST['numtop'];?>" />
		</form>
	</body>
</html>
