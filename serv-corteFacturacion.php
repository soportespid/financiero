<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=iso-8859-1");
	require 'comun.inc';
	require 'funciones.inc';
	require 'funcionesSP.inc.php';
	require 'conversor.php';
	require 'validaciones.inc';
	session_start();
	$linkbd = conectar_v7();
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es">
	<head>
		<meta http-equiv="Content-type" content="text/html; charset=iso-8859-1"/>
		<meta http-equiv="X-UA-Compatible" content="IE=9"/>
		<title>:: IDEAL 10 - Servicios P&uacute;blicos</title>
		<link href="css/css2.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/cssSP.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/style.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
		<script type="text/javascript" src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script type="text/javascript" src="css/calendario.js"></script>
		<script>
			function procesos($tip)
			{	
				switch ($tip)
				{
					case 1:	
						if (document.form2.numeroCorte.value!='') {

							document.form2.oculto.value='1';
							document.form2.submit();
						}
						else {
                        	despliegamodalm('visible','2','Faltan seleccionar un periodo de facturacion para consultar');
                        }
						break;

					case 2:	
						despliegamodalm('visible','4','Se generar cierre de facturacion de este periodo, Esta Seguro?','1');
						break;
				}
 			}
			function despliegamodalm(_valor,_tip,mensa,pregunta)
			{
				document.getElementById("bgventanamodalm").style.visibility=_valor;
				if(_valor=="hidden"){document.getElementById('ventanam').src="";}
				else
				{
					switch(_tip)
					{
						case "1":	document.getElementById('ventanam').src="ventana-mensaje1.php?titulos="+mensa;break;
						case "2":	document.getElementById('ventanam').src="ventana-mensaje3.php?titulos="+mensa;break;
						case "3":	document.getElementById('ventanam').src="ventana-mensaje2.php?titulos="+mensa;break;
						case "4":	document.getElementById('ventanam').src="ventana-consulta1.php?titulos="+mensa+"&idresp="+pregunta;break;	
					}
				}
			}
			function funcionmensaje()
			{
				document.location.href = "serv-corteFacturacion.php";
			}
			function respuestaconsulta(pregunta)
			{
				switch(pregunta)
				{
					case "1":	
						document.form2.oculto.value=3;	
						document.form2.submit();
					break;
				}
			}
			function callprogress(vValor)
			{
				document.getElementById("getprogress").innerHTML = vValor;
				document.getElementById("getProgressBarFill").innerHTML = '<div class="ProgressBarFill" style="width: '+vValor+'%;"></div>';
				document.getElementById("titulog1").style.display='block';
				document.getElementById("progreso").style.display='block';
				document.getElementById("getProgressBarFill").style.display='block';
			}

			function cambiocheck()
			{
                if(document.getElementById('myonoffswitch').value == 'S')
                {
                    document.getElementById('myonoffswitch').value = 'N';
                }
                else
                {
                    document.getElementById('myonoffswitch').value = 'S';
                }

				document.form2.submit();
            }

			function atras() {
				var link = document.referrer;

				location.href = link;
			}
		</script>

        <?php titlepag();?>

	</head>

	<body>
		<IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
		<span id="todastablas2"></span>
		<table>
			<tr><script>barra_imagenes('serv');</script><?php cuadro_titulos();?></tr>	 

			<tr><?php menu_desplegable('serv');?></tr>

			<tr>
				<td colspan="3" class="cinta">
					<a class="mgbt"><img src="imagenes/add2.png"/></a>
					<a class="mgbt"><img src="imagenes/guardad.png" title="Guardar"/></a>
					<img src="imagenes/busca.png" onClick="window.location.href='serv-verCortesFacturacion.php'"  class="mgbt" title="Buscar">
					<a onclick="mypop=window.open('plan-agenda.php','','');mypop.focus()" class="mgbt"><img src="imagenes/agenda1.png" title="Agenda" /></a>
					<a onclick="mypop=window.open('serv-principal.php','','');mypop.focus();" class="mgbt"><img src="imagenes/nv.png" title="Nueva ventana"></a>
					<a onclick="mypop=window.open('<?php echo $url2; ?>','','');mypop.focus();" class="mgbt"><img src="imagenes/duplicar_pantalla.png" title="Duplicar pesta&ntilde;a"></a>
					<a onclick="atras();" class="mgbt"><img src="imagenes/iratras.png" title="Atr&aacute;s"></a>
                </td>
			</tr>
		</table>

		<div id="bgventanamodalm" class="bgventanamodalm">
			<div id="ventanamodalm" class="ventanamodalm">
				<IFRAME src="" name="ventanam" marginWidth=0 marginHeight=0 frameBorder=0 id="ventanam" frameSpacing=0 style=" width:700px; height:130px; top:200; overflow:hidden;"> 
				</IFRAME>
			</div>
		</div>

		<form name="form2" method="post" action="">
			<table class="inicio">
				<tr>
					<td class="titulos" colspan="5">Corte Facturaci&oacute;n Servicios</td>

					<td  class="cerrar" ><a href="serv-principal.php">Cerrar</a></td>
				</tr>

				<tr>
					<td class="saludo1" style="width:3cm;">Facturaci&oacute;n vigente:</td>
					<td>
						<select class="centrarSelect" name="numeroCorte" id="numeroCorte" style="width: 98%; height: 40px;">
							<option class="aumentarTamaño" value="">:: SELECCIONAR CORTE VIGENTE ::</option>
							<?php
								$sqlr = "SELECT numero_corte, fecha_impresion, servicio FROM srvcortes WHERE estado = 'S'";
								$resp = mysqli_query($linkbd,$sqlr);
								while ($row =mysqli_fetch_row($resp)) 
								{
									if($row[2] == 0)
									{
										$servicios = "TODOS";
									}

									if($row[0]==$_POST['numeroCorte'])
									{
										echo "<option class='aumentarTamaño' value='$row[0]' SELECTED>CORTE $row[0] - $row[1] - $servicios</option>";
									}
									else
									{
										echo "<option class='aumentarTamaño' value='$row[0]'>CORTE $row[0] - $row[1] - $servicios</option>";
									}
								}
							?>
						</select>
					</td>

					<td>
						<input type="button" name="buscapredios" style="height: 40px; width: 100px;" value="Consultar" onClick="procesos(1)"/>  
					</td>
				</tr>
			</table>
			<input type="hidden" name="oculto" id="oculto" value="<?php echo @$_POST['oculto'];?>"/>
			<input type="hidden" name="facturacion" id="facturacion" value="<?php echo @$_POST['facturacion'];?>"/>
			<input type="hidden" name="totalreg" id="totalreg" value="<?php echo @$_POST['totalreg'];?>"/>
			<input type="hidden" name="nomarchivo" id="nomarchivo" value="<?php echo @$_POST['nomarchivo']?>"/>
			<input type="hidden" name="menuact" id="menuact" value="<?php echo @$_POST['menuact'];?>"/>
			<?php
				if(@$_POST['oculto']>=1)
				{
					if(@$_POST['oculto']==1)
					{
						$sqlCorte="SELECT fecha_inicial, fecha_final, fecha_limite_pago, fecha_impresion, servicio FROM srvcortes WHERE numero_corte = '".$_POST['numeroCorte']."'";

						$respCorte=mysqli_query($linkbd,$sqlCorte);

						$contador = 0;

						while ($rowCorte = mysqli_fetch_row($respCorte))
						{	
                            $_POST['fechaInicial']	 = $rowCorte[0];
                            $_POST['fechaFinal'] 	 = $rowCorte[1];
                            $_POST['fechaLimite']	 = $rowCorte[2];
							$_POST['fechaImpresion'] = $rowCorte[3];


							$sqlCorteDetalles = "SELECT MAX(numero_facturacion), MIN(numero_facturacion) FROM srvcortes_detalle WHERE id_corte = '".$_POST['numeroCorte']."'";

							$respCorteDetalles = mysqli_query($linkbd,$sqlCorteDetalles);

							$rowCorteDetalles = mysqli_fetch_row($respCorteDetalles); 

							$_POST['facturaini'] = $rowCorteDetalles[1];
							$_POST['facturafin'] = $rowCorteDetalles[0];

							if($row[4] == 0)
							{
								$_POST['nomservicio'] = "TODOS";
							}
							else
							{
								$sqlNombreServicio="SELECT nombre FROM servservicios WHERE codigo='$row[4]'";

								$respNombreServicio = mysqli_query($linkbd,$sqlNombreServicio);

								$rowNombreServicio = mysqli_fetch_row($respNombreServicio); 

								$_POST['nomservicio']="$rowNombreServicio[0]";
							}
							$contador++;
						}	

						$_POST['facturatot'] = (float)$_POST['facturafin'] - (float)$_POST['facturaini'] + 1;
						echo "<script>document.getElementById('divcarga').style.display='none';</script>";
					}
					echo"
						<input type='hidden' name='facturaini' id='facturaini' value='".$_POST['facturaini']."'/>
						<input type='hidden' name='facturafin' id='facturafin' value='".$_POST['facturafin']."'/>
						<input type='hidden' name='facturatot' id='facturatot' value='".$_POST['facturatot']."'/>
						<input type='hidden' name='fecha' id='fecha' value='".$_POST['fecha']."'/>
						<input type='hidden' name='vigencias' id='vigencias' value='".$_POST['vigencias']."'/>
						<input type='hidden' name='periodo' id='periodo' value='".$_POST['periodo']."'/>
						<input type='hidden' name='periodo2' id='periodo2' value='".$_POST['periodo2']."'/>
						<input type='hidden' name='servicios' id='servicios' value='".@$_POST['servicios']."'/>
						<input type='hidden' name='nomservicio' id='nomservicio' value='".$_POST['nomservicio']."'/>

                        <input type='hidden' name='' id='fechaInicial' value='".$_POST['fechaInicial']."'/>
                        <input type='hidden' name='fecha' id='fechaFinal' value='".$_POST['fechaFinal']."'/>
                        <input type='hidden' name='fecha' id='fechaLimite' value='".$_POST['fechaLimite']."'/>
						<input type='hidden' name='fecha' id='fechaImpresion' value='".$_POST['fechaImpresion']."'/>
                        
					";
					if(@$_POST['servicios']=="")
					{
						$nombreser="TODOS";
					}
					else 
					{
						$nombreser=$_POST['nomservicio'];
					}
					echo"
					<table class='inicio'>
						<tr><td class='titulos' colspan='10'>Facturaci&oacute;n sin Corte</td></tr>
						<tr>
							<td class='titulos2' style='width:8%; text-align:center;'>N&deg; Corte</td>
							<td class='titulos2' style='width:8%; text-align:center;'>Fecha Inicial</td>
							<td class='titulos2' style='width:8%; text-align:center;'>Fecha Final</td>
							<td class='titulos2' style='width:8%; text-align:center;'>Fecha limite de Pago</td>
							<td class='titulos2' style='width:8%; text-align:center;'>Fecha de Impresion</td>
							<td class='titulos2' style='width:12%; text-align:center;'>N&deg; Facturaci&oacute;n</td>
							<td class='titulos2' style='width:12%; text-align:center;'>Total Facturas</td>
							<td class='titulos2' style='width:5%; text-align:center;'>Sevicios</td>
							<td class='titulos2' style='width:15%; text-align:center;'>Proceso</td>
							<td class='titulos2' style='width:15%; text-align:center;'>Finalizar</td>
						</tr> 
						<tr class='saludo2'>
							<td style='height: 40px; text-align:center;'>".$_POST['numeroCorte']."</td>
							<td style='text-align:center;'>".$_POST['fechaInicial']."</td>
							<td style='text-align:center;'>".$_POST['fechaFinal']."</td>
							<td style='text-align:center;'>".$_POST['fechaLimite']."</td>
							<td style='text-align:center;'>".$_POST['fechaImpresion']."</td>
							<td style='text-align:center;'>".$_POST['facturaini']." - ".$_POST['facturafin']."</td>
							<td style='text-align:center;'>".$_POST['facturatot']."</td>
							<td style='text-align:center;'>$nombreser</td>
							<td>
								<div id='titulog1' style='display:none; float:left'></div>
								<div id='progreso' class='ProgressBar' style='display:none; float:left'>
									<div class='ProgressBarText'><span id='getprogress'></span>&nbsp;% </div>
									<div id='getProgressBarFill'></div>
								</div>
							</td>
							<td>
								<input type='button' name='botcerrar' id='botcerrar' value='Corte Facturaci&oacute;n' onClick='procesos(2)' style='width: 190px;'/>
							</td>
						</tr>
					</table>";
				}
				if(@$_POST['oculto']==3)
				{
					$sqlCorte = "UPDATE srvcortes SET estado = 'C' WHERE numero_corte = '".$_POST['numeroCorte']."' AND estado = 'S' ";

					if (mysqli_query($linkbd, $sqlCorte)) {
						
						$sqlCorteDetalles = "UPDATE srvcortes_detalle SET estado_pago = 'V' WHERE id_corte = '".$_POST['numeroCorte']."' AND estado_pago = 'S' ";

						if(mysqli_query($linkbd,$sqlCorteDetalles)) {

							echo "<script>despliegamodalm('visible','1','SE HA FINALIZADO EL CORTE');</script>";
						}
						else {
							echo"<script>despliegamodalm('visible','2','No se pudo cerrar el corte: error');</script>";
						}
					}
				}
			  ?>
		</form>
	</body>
</html>