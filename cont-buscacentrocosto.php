<?php
header("Cache-control: no-cache, no-store, must-revalidate");
header("Content-Type: text/html;charset=utf8");

require 'comun.inc';
require 'funciones.inc';

$linkbd = conectar_v7();
$linkbd->set_charset("utf8");

session_start();
date_default_timezone_set("America/Bogota");

?>

<!DOCTYPE>
<html lang="es">

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1" />
    <meta name="viewport" content="user-scalable=no">
    <title>:: IDEAL 10 - Parametrización</title>
    <link href="favicon.ico" rel="shortcut icon" />
    <link href="css/css2.css?<?php echo date('d_m_Y_h_i_s'); ?>" rel="stylesheet" type="text/css">
    <link href="css/css3.css?<?php echo date('d_m_Y_h_i_s'); ?>" rel="stylesheet" type="text/css">
    <link href="css/css4.css?<?php echo date('d_m_Y_h_i_s'); ?>" rel="stylesheet" type="text/css">
    <link href="css/style.css?<?php echo date('d_m_Y_h_i_s'); ?>" rel="stylesheet" />
    <script type="text/javascript" src="css/programas.js?<?php echo date('d_m_Y_h_i_s'); ?>"></script>
    <script type="text/javascript" src="jquery-1.11.0.min.js"></script>
    <script>
        function verUltimaPos(idcta, filas, filtro) {
            var scrtop = $('#divdet').scrollTop();
            var altura = $('#divdet').height();
            var numpag = $('#nummul').val();
            var limreg = $('#numres').val();
            if ((numpag <= 0) || (numpag == "")) { numpag = 0; }
            if ((limreg == 0) || (limreg == "")) { limreg = 10; }
            numpag++;
            location.href = "cont-editarcentrocosto.php?idtipocom=" + idcta + "&scrtop=" + scrtop + "&totreg=" + filas + "&altura=" + altura + "&numpag=" + numpag + "&limreg=" + limreg + "&filtro=" + filtro;
        }
    </script>
    <script>
        function cambioswitch(id, valor) {
            document.getElementById('idestado').value = id;
            if (valor == 1) { despliegamodalm('visible', '4', 'Desea activar Estado', '1'); }
            else { despliegamodalm('visible', '4', 'Desea Desactivar Estado', '2'); }
        }
        function despliegamodalm(_valor, _tip, mensa, pregunta) {
            document.getElementById("bgventanamodalm").style.visibility = _valor;
            if (_valor == "hidden") { document.getElementById('ventanam').src = ""; }
            else {
                switch (_tip) {
                    case "1":
                        document.getElementById('ventanam').src = "ventana-mensaje1.php?titulos=" + mensa; break;
                    case "2":
                        document.getElementById('ventanam').src = "ventana-mensaje3.php?titulos=" + mensa; break;
                    case "3":
                        document.getElementById('ventanam').src = "ventana-mensaje2.php?titulos=" + mensa; break;
                    case "4":
                        document.getElementById('ventanam').src = "ventana-consulta2.php?titulos=" + mensa + "&idresp=" + pregunta; break;
                }
            }
        }
        function funcionmensaje() { }
        function respuestaconsulta(estado, pregunta) {
            if (estado == "S") {
                switch (pregunta) {
                    case "1": document.form2.cambioestado.value = "1"; break;
                    case "2": document.form2.cambioestado.value = "0"; break;
                    case "3": document.form2.ac.value = 2; break;
                }
            }
            else {
                switch (pregunta) {
                    case "1": document.form2.nocambioestado.value = "1"; break;
                    case "2": document.form2.nocambioestado.value = "0"; break;
                    case "3": break;
                }
            }
            document.form2.submit();
        }
        function anular(id) {
            document.form2.cod.value = id;
            despliegamodalm('visible', '4', "Esta Seguro de Eliminar la Cuenta " + id, '3');
        }
    </script>
</head>

<body>
    <IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
    <span id="todastablas2"></span>
    <table>
        <tr>
            <script>barra_imagenes("para");</script><?php cuadro_titulos(); ?>
        </tr>
        <tr><?php menu_desplegable("para"); ?></tr>
    </table>
    <div class="bg-white group-btn p-1">
        <button type="button" onclick="location.href='cont-centrocostos.php'"
            class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
            <span>Nuevo</span>
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960">
                <path d="M440-440H200v-80h240v-240h80v240h240v80H520v240h-80v-240Z"></path>
            </svg>
        </button>
        <button type="button" onclick="mypop=window.open('plan-agenda.php','','');mypop.focus()"
            class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
            <span class="group-hover:text-white">Agenda</span>
            <svg class="fill-black group-hover:fill-white w-5 h-5" xmlns="http://www.w3.org/2000/svg"
                viewBox="0 -960 960 960">
                <path
                    d="M560-564v-68q33-14 67.5-21t72.5-7q26 0 51 4t49 10v64q-24-9-48.5-13.5T700-600q-38 0-73 9.5T560-564Zm0 220v-68q33-14 67.5-21t72.5-7q26 0 51 4t49 10v64q-24-9-48.5-13.5T700-380q-38 0-73 9t-67 27Zm0-110v-68q33-14 67.5-21t72.5-7q26 0 51 4t49 10v64q-24-9-48.5-13.5T700-490q-38 0-73 9.5T560-454ZM260-320q47 0 91.5 10.5T440-278v-394q-41-24-87-36t-93-12q-36 0-71.5 7T120-692v396q35-12 69.5-18t70.5-6Zm260 42q44-21 88.5-31.5T700-320q36 0 70.5 6t69.5 18v-396q-33-14-68.5-21t-71.5-7q-47 0-93 12t-87 36v394Zm-40 118q-48-38-104-59t-116-21q-42 0-82.5 11T100-198q-21 11-40.5-1T40-234v-482q0-11 5.5-21T62-752q46-24 96-36t102-12q58 0 113.5 15T480-740q51-30 106.5-45T700-800q52 0 102 12t96 36q11 5 16.5 15t5.5 21v482q0 23-19.5 35t-40.5 1q-37-20-77.5-31T700-240q-60 0-116 21t-104 59ZM280-494Z">
                </path>
            </svg>
        </button>
        <button type="button" onclick="window.open('para-principal');"
            class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
            <span>Nueva ventana</span>
            <svg class="fill-black group-hover:fill-white w-5 h-5" xmlns="http://www.w3.org/2000/svg"
                viewBox="0 -960 960 960">
                <path
                    d="M320-120v-80h80v-80H160q-33 0-56.5-23.5T80-360v-400q0-33 23.5-56.5T160-840h640q33 0 56.5 23.5T880-760v400q0 33-23.5 56.5T800-280H560v80h80v80H320ZM160-360h640v-400H160v400Zm0 0v-400 400Z">
                </path>
            </svg>
        </button>
        <button type="button" onclick="mypop=window.open('/financiero/cont-buscacentrocosto.php','','');mypop.focus();"
            class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
            <span class="group-hover:text-white">Duplicar pantalla</span>
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960">
                <path
                    d="M640-160v-360H160v360h480Zm80-200v-80h80v-360H320v200h-80v-200q0-33 23.5-56.5T320-880h480q33 0 56.5 23.5T880-800v360q0 33-23.5 56.5T800-360h-80ZM160-80q-33 0-56.5-23.5T80-160v-360q0-33 23.5-56.5T160-600h480q33 0 56.5 23.5T720-520v360q0 33-23.5 56.5T640-80H160Zm400-603ZM400-340Z">
                </path>
            </svg>
        </button>
    </div>
    <div id="bgventanamodalm" class="bgventanamodalm">
        <div id="ventanamodalm" class="ventanamodalm">
            <IFRAME src="" name="ventanam" marginWidth=0 marginHeight=0 frameBorder=0 id="ventanam" frameSpacing=0
                style=" width:700px; height:130px; top:200; overflow:hidden;">
            </IFRAME>
        </div>
    </div>
    <?php
    if ($_POST['oculto'] == "") {
        $_POST['scrtop'] = $_GET['scrtop'];
        if ($_POST['scrtop'] == "") {
            $_POST['scrtop'] = 0;
        }
        $_POST['gidcta'] = $_GET['idcta'];
        if (isset($_GET['filtro'])) {
            $_POST['numero'] = $_GET['filtro'];
        }
    }
    echo "<script>window.onload=function(){ $('#divdet').scrollTop(" . $_POST['scrtop'] . ")}</script>";
    if ($_GET['numpag'] != "") {
        $oculto = $_POST['oculto'];
        if ($oculto != 2) {
            $_POST['numres'] = $_GET['limreg'];
            $_POST['numpos'] = $_GET['limreg'] * ($_GET['numpag'] - 1);
            $_POST['nummul'] = $_GET['numpag'] - 1;
        }
    } else {
        if ($_POST['nummul'] == "") {
            $_POST['numres'] = 10;
            $_POST['numpos'] = 0;
            $_POST['nummul'] = 0;
        }
    }
    ?>
    <form name="form2" method="post" action="cont-buscacentrocosto.php">
        <?php
        if ($_POST['oculto2'] == "") {
            $_POST['oculto2'] = "0";
            $_POST['cambioestado'] = "";
            $_POST['nocambioestado'] = "";
        }
        if ($_POST['cambioestado'] != "") {
            if ($_POST['cambioestado'] == "1") {
                $sqlr = "UPDATE centrocosto SET estado='S' WHERE id_cc='" . $_POST['idestado'] . "'";
                mysqli_fetch_row(mysqli_query($linkbd, $sqlr));
            } else {
                $sqlr = "UPDATE centrocosto SET estado='N' WHERE id_cc='" . $_POST['idestado'] . "'";
                mysqli_fetch_row(mysqli_query($linkbd, $sqlr));
            }
            echo "<script>document.form2.cambioestado.value=''</script>";
        }
        if ($_POST['nocambioestado'] != "") {
            if ($_POST['nocambioestado'] == "1") {
                $_POST['lswitch1'][$_POST['idestado']] = 1;
            } else {
                $_POST['lswitch1'][$_POST['idestado']] = 0;
            }
            echo "<script>document.form2.nocambioestado.value=''</script>";
        }
        ?>
        <table class="inicio ancho" align="center">
            <tr>
                <td class="titulos" colspan="4">:: Buscar Centro de Costos </td>
                <td class="cerrar" style="width:7%" onClick="location.href='cont-principal.php'">Cerrar</td>
            </tr>
            <tr>
                <td class="saludo1" style='width:3cm;'>C&oacute;digo o Nombre:</td>
                <td style='width:50%;'><input type="search" name="nombre" id="nombre"
                        value="<?php echo $_POST['nombre']; ?>" style='width:100%;' /></td>
                <td><em class="botonflechaverde" onClick="limbusquedas();">Buscar</em></td>
            </tr>
        </table>
        <input name="oculto" id="oculto" type="hidden" value="1" />
        <input type="hidden" name="oculto2" id="oculto2" value="<?php echo $_POST['oculto2']; ?>" />
        <input type="hidden" name="cambioestado" id="cambioestado" value="<?php echo $_POST['cambioestado']; ?>" />
        <input type="hidden" name="nocambioestado" id="nocambioestado"
            value="<?php echo $_POST['nocambioestado']; ?>" />
        <input type="hidden" name="idestado" id="idestado" value="<?php echo $_POST['idestado']; ?>" />
        <input type="hidden" name="numres" id="numres" value="<?php echo $_POST['numres']; ?>" />
        <input type="hidden" name="numpos" id="numpos" value="<?php echo $_POST['numpos']; ?>" />
        <input type="hidden" name="nummul" id="nummul" value="<?php echo $_POST['nummul']; ?>" />
        <input type="hidden" name="scrtop" id="scrtop" value="<?php echo $_POST['scrtop']; ?>" />
        <input type="hidden" name="gidcta" id="gidcta" value="<?php echo $_POST['gidcta']; ?>" />
        <div class="subpantalla" style="height:65.5%; width:99.5%; overflow-x:hidden;" id="divdet">
            <?php
            $oculto = $_POST['oculto'];
            $crit1 = "";
            if ($_POST['nombre'] != "") {
                $crit1 = "WHERE concat_ws(' ', id_cc, nombre) LIKE '%" . $_POST['nombre'] . "%'";
            }
            $sqlr = "SELECT * FROM centrocosto $crit1";
            $resp = mysqli_query($linkbd, $sqlr);
            $_POST['numtop'] = mysqli_num_rows($resp);
            $nuncilumnas = ceil($_POST['numtop'] / $_POST['numres']);
            $cond2 = "";
            if ($_POST['numres'] != "-1") {
                $cond2 = "LIMIT " . $_POST['numpos'] . ", " . $_POST['numres'];
            }
            $sqlr = "SELECT * FROM centrocosto $crit1 ORDER BY id_cc " . $cond2;
            $resp = mysqli_query($linkbd, $sqlr);
            $con = 1;
            $numcontrol = $_POST['nummul'] + 1;
            if (($nuncilumnas == $numcontrol) || ($_POST['numres'] == "-1")) {
                $imagenforward = "<img src='imagenes/forward02.png' style='width:17px;cursor:default;'>";
                $imagensforward = "<img src='imagenes/skip_forward02.png' style='width:16px;cursor:default;' >";
            } else {
                $imagenforward = "<img src='imagenes/forward01.png' style='width:17px;cursor:pointer;' title='Siguiente' onClick='numsiguiente()'>";
                $imagensforward = "<img src='imagenes/skip_forward01.png' style='width:16px;cursor:pointer;' title='Fin' onClick='saltocol(\"$nuncilumnas\")'>";
            }
            if (($_POST['numpos'] == 0) || ($_POST['numres'] == "-1")) {
                $imagenback = "<img src='imagenes/back02.png' style='width:17px;cursor:default;'>";
                $imagensback = "<img src='imagenes/skip_back02.png' style='width:16px;cursor:default;'>";
            } else {
                $imagenback = "<img src='imagenes/back01.png' style='width:17px;cursor:pointer;' title='Anterior' onClick='numanterior();'>";
                $imagensback = "<img src='imagenes/skip_back01.png' style='width:16px;cursor:pointer;' title='Inicio' onClick='saltocol(\"1\")'>";
            }
            $ntips1 = 8;
            $ntips2 = 9;
            if ($_SESSION["prdesactivar"] != 1) {
                $ntips1 = $ntips1 - 1;
                $ntips2 = $ntips2 - 1;
            }
            if ($_SESSION["preditar"] != 1) {
                $ntips1 = $ntips1 - 1;
                $ntips2 = $ntips2 - 1;
            }
            if ($_SESSION["preliminar"] != 1) {
                $ntips1 = $ntips1 - 1;
                $ntips2 = $ntips2 - 1;
            }
            echo "
				<table class='inicio' align='center' width='80%'>
					<tr>
						<td colspan='$ntips1' class='titulos'>.: Resultados Busqueda:</td>
						<td class='submenu'>
							<select name='renumres' id='renumres' onChange='cambionum();' style='width:100%'>
								<option value='10'";
            if ($_POST['renumres'] == '10') {
                echo 'selected';
            }
            echo ">10</option>
								<option value='20'";
            if ($_POST['renumres'] == '20') {
                echo 'selected';
            }
            echo ">20</option>
								<option value='30'";
            if ($_POST['renumres'] == '30') {
                echo 'selected';
            }
            echo ">30</option>
								<option value='50'";
            if ($_POST['renumres'] == '50') {
                echo 'selected';
            }
            echo ">50</option>
								<option value='100'";
            if ($_POST['renumres'] == '100') {
                echo 'selected';
            }
            echo ">100</option>
								<option value='-1'";
            if ($_POST['renumres'] == '-1') {
                echo 'selected';
            }
            echo ">Todos</option>
							</select>
						</td>
					</tr>
					<tr><td colspan='$ntips2'>Centro de Costos Encontrados: " . $_POST['numtop'] . "</td></tr>
					<tr>
						<td class='titulos2' style='width:5%;'>Item</td>
						<td class='titulos2' style='width:5%;'>Codigo</td>
						<td class='titulos2'>Nombre</td>
						<td class='titulos2' style='width:5%;'>Tipo</td>
						<td class='titulos2' style='width:8%;'>Cuenta Patrimonio</td>";
            if ($_SESSION["prdesactivar"] == 1) {
                echo "<td class='titulos2' colspan='2' style='width:6%;' >Estado</td>";
            } else {
                echo "<td class='titulos2' style='width:6%;' >Estado</td>";
            }
            if ($_SESSION["preditar"] == 1) {
                echo "<td class='titulos2' style='width:4%;'>Editar</td>";
            }
            if ($_SESSION["preliminar"] == 1) {
                echo "<td class='titulos2' style='width:4%;' >Eliminar</td>";
            }
            echo "</tr>";
            $iter = 'saludo1a';
            $iter2 = 'saludo2';
            $filas = 1;
            while ($row = mysqli_fetch_row($resp)) {
                $tipo = "";
                $con2 = $con + $_POST['numpos'];
                if ($row[2] == 'S') {
                    $imgsem = "src='imagenes/sema_verdeON.jpg' title='Activo'";
                    $coloracti = "#0F0";
                    $_POST['lswitch1'][$row[0]] = 0;
                } else {
                    $imgsem = "src='imagenes/sema_rojoON.jpg' title='Inactivo'";
                    $coloracti = "#C00";
                    $_POST['lswitch1'][$row[0]] = 1;
                }
                if ($row[3] == 'S') {
                    $tipo = "Entidad";
                }
                if ($row[3] == 'N') {
                    $tipo = "Externo";
                }
                if ($_POST['gidcta'] != "") {
                    if ($_POST['gidcta'] == $row[0]) {
                        $estilo = 'background-color:yellow';
                    } else {
                        $estilo = "";
                    }
                } else {
                    $estilo = "";
                }
                $idcta = "'$row[0]'";
                $numfil = "'$filas'";
                $filtro = "'" . $_POST['nombre'] . "'";
                if ($_SESSION["preditar"] == 1) {
                    echo "<tr class='$iter' onDblClick=\"verUltimaPos($idcta, $numfil, $filtro)\" style='text-transform:uppercase; $estilo' >";
                } else {
                    echo "<tr class='$iter' style='text-transform:uppercase; $estilo' >";
                }
                echo "
					<td>$con2</td>
					<td>$row[0]</td>
					<td >$row[1]</td>
					<td>$tipo</td>
					<td style='text-align:center;'>$row[4]</td>
					<td style='text-align:center;'><img $imgsem style='width:20px'/></td>";
                if ($_SESSION["prdesactivar"] == 1) {
                    echo "<td style='text-align:center;'><input type='range' name='lswitch1[]' value='" . $_POST['lswitch1'][$row[0]] . "' min ='0' max='1' step ='1' style='background:$coloracti; width:60%' onChange='cambioswitch(\"$row[0]\",\"" . $_POST['lswitch1'][$row[0]] . "\")' /></td>";
                }
                if ($_SESSION["preditar"] == 1) {
                    echo "<td style='text-align:center;'>
								<a onClick=\"verUltimaPos($idcta, $numfil, $filtro)\" style='cursor:pointer;'>
									<img src='imagenes/b_edit.png' style='width:18px' title='Editar'>
								</a>
							</td>";
                }
                if ($_SESSION["preliminar"] == 1) {
                    echo "<td style='text-align:center;cursor:pointer;'><a onClick=anular(id=$row[0])><img src='imagenes/anular.png' style='width:22px' title='Eliminar'></a></td>";
                }
                $con += 1;
                $aux = $iter;
                $iter = $iter2;
                $iter2 = $aux;
                $filas++;
            }
            if ($_POST['numtop'] == 0) {
                echo "
					<table class='inicio'>
						<tr>
							<td class='saludo1' style='text-align:center;width:100%'><img src='imagenes\alert.png' style='width:25px'>No hay coincidencias en la b&uacute;squeda $tibusqueda<img src='imagenes\alert.png' style='width:25px'></td>
						</tr>
					</table>";
            }
            echo "
					</table>
					<table class='inicio'>
						<tr>
							<td style='text-align:center;'>
								<a>$imagensback</a>&nbsp;
								<a>$imagenback</a>&nbsp;&nbsp;";
            if ($nuncilumnas <= 9) {
                $numfin = $nuncilumnas;
            } else {
                $numfin = 9;
            }
            for ($xx = 1; $xx <= $numfin; $xx++) {
                if ($numcontrol <= 9) {
                    $numx = $xx;
                } else {
                    $numx = $xx + ($numcontrol - 9);
                }
                if ($numcontrol == $numx) {
                    echo "<a onClick='saltocol(\"$numx\")'; style='color:#24D915;cursor:pointer;'> $numx </a>";
                } else {
                    echo "<a onClick='saltocol(\"$numx\")'; style='color:#000000;cursor:pointer;'> $numx </a>";
                }
            }
            echo "			&nbsp;&nbsp;<a>$imagenforward</a>
								&nbsp;<a>$imagensforward</a>
							</td>
						</tr>
					</table>";

            ?>
        </div>
        <input type="hidden" name="numtop" id="numtop" value="<?php echo $_POST['numtop']; ?>" />
    </form>
</body>

</html>
