<?php

    header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require 'comun.inc';
	require 'funciones.inc';
	session_start();
    if(empty($_SESSION)){
        header("location: index.php");
    }
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html lang=es>
	<head>
    <meta charset="utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1"/>
		<meta name="viewport" content="user-scalable=no">
		<title>:: IDEAL 10 - Parametrización</title>
		<link href="favicon.ico" rel="shortcut icon"/>
		<link href="css/css2z.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
        <link href="css/style.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" href="sweetalert2/dist/sweetalert2.min.css">
        <script src="sweetalert2/dist/sweetalert2.min.js"></script>
		<script src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
	</head>
	<body>
		<header>
			<table>
				<tr><script>barra_imagenes("trans");</script><?php cuadro_titulos();?></tr>
			</table>
		</header>

        <main id="myapp" v-cloak>
            <input type="hidden" value = "3" ref="pageType">
            <div class="loading-container" v-show="isLoading" >
                <p class="text-loading"data-loading="IDEAL 10">IDEAL 10</p>
            </div>
            <nav>
                <table>
                    <tr><?php menu_desplegable("trans");?></tr>
                </table>
                <div class="bg-white group-btn p-1" id="newNavStyle">
                    <button type="button" @click="window.location.href='trans-recaudo-impuesto-crear'" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
                        <span>Nuevo</span>
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960"><path d="M440-440H200v-80h240v-240h80v240h240v80H520v240h-80v-240Z"></path></svg>
                    </button>
                    <button type="button" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center" @click="mypop=window.open('trans-principal.php','',''); mypop.focus();">
                        <span>Nueva ventana</span>
                        <svg class="fill-black group-hover:fill-white w-5 h-5" xmlns="http://www.w3.org/2000/svg"  viewBox="0 -960 960 960" ><path d="M320-120v-80h80v-80H160q-33 0-56.5-23.5T80-360v-400q0-33 23.5-56.5T160-840h640q33 0 56.5 23.5T880-760v400q0 33-23.5 56.5T800-280H560v80h80v80H320ZM160-360h640v-400H160v400Zm0 0v-400 400Z"/></svg>
                    </button>
                    <button type="button" @click="mypop=window.open('trans-recaudo-impuesto-buscar','','');mypop.focus();" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
                        <span class="group-hover:text-white">Duplicar pantalla</span>
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960"><path d="M640-160v-360H160v360h480Zm80-200v-80h80v-360H320v200h-80v-200q0-33 23.5-56.5T320-880h480q33 0 56.5 23.5T880-800v360q0 33-23.5 56.5T800-360h-80ZM160-80q-33 0-56.5-23.5T80-160v-360q0-33 23.5-56.5T160-600h480q33 0 56.5 23.5T720-520v360q0 33-23.5 56.5T640-80H160Zm400-603ZM400-340Z"></path></svg>
                    </button>
                    <button type="button" @click="window.location.href='trans-menu-ingresos'" class="btn btn-success btn-success-hover d-flex justify-between align-items-center">
                        <span>Atras</span>
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960">
                            <path
                                d="M760-200v-160q0-50-35-85t-85-35H273l144 144-57 56-240-240 240-240 57 56-144 144h367q83 0 141.5 58.5T840-360v160h-80Z">
                            </path>
                        </svg>
                    </button>
                </div>
            </nav>
            <section class="bg-white">
                <div>
                    <h2 class="titulos m-0">Buscar recaudo impuesto</h2>
                    <div class="d-flex">
                        <div class="form-control w-25">
                            <label class="form-label" for="">Por página:</label>
                            <select v-model="selectPorPagina">
                                <option value="25">25</option>
                                <option value="50">50</option>
                                <option value="100">100</option>
                                <option value="250">250</option>
                                <option value="500">500</option>
                                <option value="1000">1000</option>
                                <option value="">Todo</option>
                            </select>
                        </div>
                        <div class="form-control justify-between">
                            <div></div>
                            <div class="d-flex">
                                <input type="search" v-model="strBuscar" placeholder="buscar" >
                                <button type="button" @click="getSearch()" class="btn btn-primary">Buscar</button>
                            </div>
                        </div>
                        <div class="form-control w-25">
                            <label class="form-label">Convenciones:</label>
                            <div class="d-flex">
                                <span class="badge badge-warning me-2"> Cuenta sin asignar</span>
                                <span class="badge badge-danger"> Cuenta no habilitada</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="table-responsive overflow-auto" style="height:50vh">
                    <table class="table table-hover fw-normal">
                        <thead>
                            <tr class="text-center">
                                <th>Código</th>
                                <th>Nombre</th>
                                <th>Tipo</th>
                                <th>Presupuesto</th>
                                <th>Destino/Tercero</th>
                                <th>Afecta presupuesto</th>
                                <th>Concepto</th>
                                <th>Tipo ingreso</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr @dblclick="window.location.href='trans-recaudo-impuesto-editar?id='+data.codigo"  v-for="(data,index) in arrData"  :key="index"
                            :class="[(data.cuentapres == '' && data.is_tercero==2 && data.is_cuenta==2 ? 'bg-warning text-black' : ''),
                            (data.estado_cuenta == 2  && data.is_tercero== 2 && data.is_cuenta== 2 ? 'bg-danger text-white' : '')]">
                                <td class="text-center">{{ data.codigo}}</td>
                                <td >{{ data.nombre}}</td>
                                <td class="text-center">{{ data.tipo == "S" ? "Simple" : "Compuesto"}}</td>
                                <td >{{ data.tipo == "S" ? data.cuentapres : ""}}</td>
                                <td >{{data.terceros}}</td>
                                <td class="text-center">{{data.is_cuenta == "1" && data.tipo == "S" ? "No" : data.is_cuenta !=null && data.is_cuenta == 2 && data.tipo == "S"? "Si" : "No"}}</td>
                                <td class="text-center">{{ data.concepto}}</td>
                                <td class="text-center">{{ data.tipoingreso}}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div v-if="arrData.length > 0" class="list-pagination-container">
                    <p>Página {{ intPagina }} de {{ intTotalPaginas }}</p>
                    <ul class="list-pagination">
                        <li v-show="intPagina > 1" @click="getSearch(intPagina = 1)"><< </li>
                        <li v-show="intPagina > 1" @click="getSearch(--intPagina)"><</li>
                        <li v-for="(pagina,index) in arrBotones" :class="intPagina == pagina ? 'active' : ''" @click="getSearch(pagina)" :key="index">{{pagina}}</li>
                        <li v-show="intPagina < intTotalPaginas" @click="getSearch(++intPagina)">></li>
                        <li v-show="intPagina < intTotalPaginas" @click="getSearch(intPagina = intTotalPaginas)" >>></li>
                    </ul>
                </div>
            </section>
        </main>
        <script src="Librerias/vue/vue.min.js"></script>
		<script type="module" src="transporte/js/functions_recaudos.js?<?= date('d_m_Y_h_i_s');?>"></script>
	</body>
</html>
