<?php

    require_once '../../comun.inc';
    require '../../funciones.inc';
 
    session_start();

	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");

    $maxVersion = ultimaVersionIngresosCCPET();

    $out = array('error' => false);

    $action = "show";

    if(isset($_GET['action'])){
        $action = $_GET['action'];
    }

    
    
    if($action == 'show'){

        $rubrosInicial = [];
        $fuentesUsadas = [];
        $fuentes = [];

        $vigencia = $_POST['vigencia'];

        $sqlrFuentes = "SELECT codigo_fuente, nombre FROM ccpet_fuentes_cuipo";
        $respFuentes = mysqli_query($linkbd, $sqlrFuentes);
        while($rowFuentes = mysqli_fetch_row($respFuentes)){
            array_push($fuentes, $rowFuentes);
        }

        $sqlrRps = "SELECT fuente FROM ccpetrp_detalle WHERE vigencia = '$vigencia' AND fuente != '' GROUP BY fuente";
        $respRps = mysqli_query($linkbd, $sqlrRps);
        while($rowRps = mysqli_fetch_row($respRps)){
            array_push($fuentesUsadas, $rowRps[0]);
        }

        $sqlrFuenteHomologar = "SELECT fuenteCuipo, fuentef05 FROM ccpet_homologarfuentesf05";
        $respFuenteHomologar = mysqli_query($linkbd, $sqlrFuenteHomologar);
        while($rowFuenteHomologar = mysqli_fetch_row($respFuenteHomologar)){
            $selFuenteHomologar[$rowFuenteHomologar[0]] = $rowFuenteHomologar[1];
        }

        $out['fuentesUsadas'] = $fuentesUsadas;
        $out['fuentes'] = $fuentes;
        $out['selFuenteHomologar'] = $selFuenteHomologar;
        
    }

    if($action == 'cargarFuentesf05'){

        $fuentesF05 = [];
        $sqlr = "SELECT * FROM ccpet_fuentesf05 ORDER BY id ASC";
        $res = mysqli_query($linkbd, $sqlr);
        while($row = mysqli_fetch_row($res)){
            array_push($fuentesF05, $row);
        }

        $out['fuentesf05'] = $fuentesF05;

    }



    if($action == 'filtrarFuentesf05'){

        $keywordFuentef05=$_POST['keywordFuentef05'];

        $fuentesf05 = array();

        $sqlr = "SELECT * FROM ccpet_fuentesf05 WHERE concat_ws(' ', fuente) LIKE '%$keywordFuentef05%' ORDER BY id ASC";//echo $sqlr;
        $res = mysqli_query($linkbd, $sqlr);
        while($row = mysqli_fetch_row($res)){
            array_push($fuentesf05, $row);
        }

        $out['fuentesf05'] = $fuentesf05;
    }


    if($action == 'guardarFuentesf05'){

        $user = $_SESSION['nickusu'];

        $sqlrVaciar = "TRUNCATE TABLE ccpet_homologarfuentesf05";
        mysqli_query($linkbd, $sqlrVaciar);

        $tam = count($_POST["fuentesf05"]);

        for($x = 0; $x < $tam; $x++){

            $sqlrInsert = "INSERT INTO ccpet_homologarfuentesf05(fuenteCuipo, fuentef05) VALUES ('".trim($_POST["fuentes"][$x])."', '".trim($_POST["fuentesf05"][$x])."')";
            mysqli_query($linkbd, $sqlrInsert);

        }
        $out['insertaBien'] = true;
    }


    header("Content-type: application/json");
    echo json_encode($out);
    die();