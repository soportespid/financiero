<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");

	require "comun.inc";
	require "funciones.inc";
	require "conversor.php";
	require "validaciones.inc";

	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");

	session_start();
	date_default_timezone_set("America/Bogota");
//**niveles menu: Administracion (0) - Consultas (1) - Herramientas (2) - Reportes (3)
?>
<!DOCTYPE > 
<html lang="es">
	<head>
		<meta charset="utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1"/>
		<meta name="viewport" content="user-scalable=no">
		<title>:: IDEAL 10 - Contabilidad</title>
		<link href="favicon.ico" rel="shortcut icon"/>
		<link href="css/css2.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<script type="text/javascript" src="css/calendario.js"></script>
		<script type="text/javascript" src="css/programas.js"></script>
		<script>
			//************* ver reporte ************
			//***************************************
			function verep(idfac)
			{
				document.form1.oculto.value=idfac;
				document.form1.submit();
  			}
			//************* genera reporte ************
			//***************************************
			function genrep(idfac)
			{
				document.form2.oculto.value=idfac;
				document.form2.submit();
			}
			//************* genera reporte ************
			//***************************************
			function guardar()
			{
				if (document.form2.vigencia.value!='' && document.form2.fecha.value!='' && document.form2.acuerdo.value!='')
				{
					if (confirm("Esta Seguro de Guardar"))
					{
					document.form2.oculto.value=2;
					document.form2.submit();
					}
				}
				else{
					alert('Faltan datos para completar el registro');
					document.form2.fecha.focus();
					document.form2.fecha.select();
				}
			}

			function clasifica(formulario)
			{
				//document.form2.action="presu-recursos.php";
				document.form2.submit();
			}
		</script>

	</head>
	<body>
		<IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
		<span id="todastablas2"></span>
		<table>
			<tr><script>barra_imagenes("cont");</script><?php cuadro_titulos();?></tr>	 
			<tr><?php menu_desplegable("cont");?></tr>
			<tr>
				<td colspan="3" class="cinta">
					<a class="mgbt"><img src="imagenes/add2.png"/></a>
					<a class="mgbt"><img src="imagenes/guardad.png"/></a>
					<a class="mgbt"><img src="imagenes/buscad.png"/></a>
					<a onClick="mypop=window.open('plan-agenda.php','','');mypop.focus()" class="mgbt"><img src="imagenes/agenda1.png" title="Agenda" /></a>
					<a onClick="mypop=window.open('cont-principal.php','','');mypop.focus();" class="mgbt"><img src="imagenes/nv.png" title="Nueva ventana"></a>
					<a onClick="mypop=window.open('<?php echo $url2; ?>','','');mypop.focus();" class="tooltip bottom mgbt"><img src="imagenes/duplicar_pantalla.png" title="Duplicar pesta&ntilde;a"></a>
				</td> 
			</tr>
		</table>
		<tr>
			<td colspan="3" class="tablaprin"> 
				<?php
				$vigusu=vigencia_usuarios($_SESSION['cedulausu']);
				$vigencia=$vigusu;
				if($_POST['consolidado']=='')
				$chkcomp=' ';
				else
				$chkcomp=' checked ';
				if($_POST['cierre']=='')
				{
					$chkcierre=' ';
				}
				else
				{ 
					$chkcierre	=' checked ';
				}

				if(!$_POST['oculto'])
				{
					$fec=date("d/m/Y");
					$_POST['fecha']=$fec; 	
					$_POST['vigencia']=$vigencia;
					$_POST['valoradicion']=0;
					$_POST['valorreduccion']=0;
					$_POST['valortraslados']=0;		 		  			 
					$_POST['valor']=0;		 
				}
				?>
 				<form name="form2" method="post" action="">
					<table class="inicio">
						<tr>
							<td class="titulos" colspan="2">.: Configuracion contable </td>
							<td class="cerrar" style="width:7%;" ><a href="cont-principal.php">&nbsp;Cerrar</a></td>
						</tr>
							<td style="background-repeat:no-repeat; background-position:center;">
								<ol id="lista2">
									<li onClick="location.href='cont-mayorybalance.php'" style="cursor:pointer;">Libro Mayor y Balances</li>
									<li onClick="location.href='cont-librodiario.php'" style="cursor:pointer;">Libro Diario</li>                  
								</ol>
							</td>                 
						</tr>							
    				</table>
   
				</form>
			</table>
		</body>
</html>
