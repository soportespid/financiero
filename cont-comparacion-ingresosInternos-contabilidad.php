<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");

	require "comun.inc";
	require "funciones.inc";
	require "funcionesSP.inc.php";

	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");

	session_start();
    date_default_timezone_set("America/Bogota");
    $_POST['fechaini'] = isset($_POST['fechaini']) && $_POST['fechaini'] != "" ? $_POST['fechaini'] : "01/01/".date("Y");
    $_POST['fechafin'] = isset($_POST['fechafin']) && $_POST['fechafin'] != "" ? $_POST['fechafin'] : date("d/m/Y");
?>
<!DOCTYPE >
<html lang="en">
	<head>
		<meta charset="utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1"/>
		<meta name="viewport" content="user-scalable=no">
		<title>:: IDEAL 10 - Contabilidad</title>
		<link href="favicon.ico" rel="shortcut icon"/>
		<link href="css/css2n.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/css3n.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/cssSP.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<script type="text/javascript" src="css/calendario.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script type="text/javascript" src="jquery-1.11.0.min.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script type="text/javascript" src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
        <script src="sweetalert2/dist/sweetalert2.min.js"></script>
		<link rel="stylesheet" href="sweetalert2/dist/sweetalert2.min.css">
		<script>
            function generarInforme() {

                var fechaInicial = document.getElementById('fechaini').value;
                var fechaFinal = document.getElementById('fechafin').value;

                if (fechaInicial != '' && fechaFinal != '') {

                    if (fechaInicial <= fechaFinal) {

                        document.form2.oculto.value='2';
					    document.form2.submit();
                    }
                    else {
                        Swal.fire({
                            icon: 'warning',
                            title: 'Fecha inicial debe ser menor a la fecha final'
                        })
                    }
                }
                else {
                    Swal.fire({
                        icon: 'warning',
                        title: 'Ingrese ambas fechas'
                    })
                }
            }

            function direccionaComprobante(idCat,tipo_compro,num_compro) {

				window.open("cont-buscacomprobantes.php?idCat="+idCat+"&tipo_compro="+tipo_compro+"&num_compro="+num_compro);
			}

            $(window).load(function () {
				$('#cargando').hide();
			});
		</script>

		<?php

			$scrtop = @ $_GET['scrtop'];
			if($scrtop == "") $scrtop=0;
			echo"<script>
					window.onload=function()
					{
						$('#divdet').scrollTop(".$scrtop.")
					}
				</script>";
		?>
	</head>
	<body>
		<IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
		<span id="todastablas2"></span>
		<div id="cargando" style=" position:absolute;left: 46%; bottom: 45%">
			<img src="imagenes/loading.gif" style=" width: 80px; height: 80px"/>
		</div>
		<table>
			<tr><script>barra_imagenes("cont");</script><?php cuadro_titulos();?></tr>
			<tr><?php menu_desplegable("cont");?></tr>
			<tr>
				<td colspan="3" class="cinta">
					<a href="#" class="mgbt"><img src="imagenes/add.png"/></a>
					<a class="mgbt"><img src="imagenes/guardad.png" title="Guardar"/></a>
					<a href="#" class="mgbt"><img src="imagenes/busca.png" title="Buscar"/></a>
					<a onclick="mypop=window.open('plan-agenda.php','','');mypop.focus()" class="mgbt"><img src="imagenes/agenda1.png" title="Agenda" /></a>
					<a onclick="mypop=window.open('cont-principal.php','','');mypop.focus();" class="mgbt"><img src="imagenes/nv.png" title="Nueva ventana"></a>
					<a onclick="mypop=window.open('<?php echo $url2; ?>','','');mypop.focus();" class="mgbt"><img src="imagenes/duplicar_pantalla.png" title="Duplicar pesta&ntilde;a"></a>
                    <a href="cont-estadoComprobantesComparacion.php" class="mgbt"><img src="imagenes/iratras.png" title="Atrás"></a>
				</td>
			</tr>
		</table>

		<div id="bgventanamodalm" class="bgventanamodalm">
			<div id="ventanamodalm" class="ventanamodalm">
				<IFRAME src="" name="ventanam" marginWidth=0 marginHeight=0 frameBorder=0 id="ventanam" frameSpacing=0 style=" width:700px; height:130px; top:200; overflow:hidden;"></IFRAME>
			</div>
		</div>

		<form name="form2" method="post" action="">
			<table class="inicio ancho">
				<tr>
					<td class="titulos" colspan="7">Comparación ingresos internos tesoreria - contabilidad</td>
					<td class="cerrar" style="width:7%" onClick="location.href='cont-principal.php'">Cerrar</td>
				</tr>

                <tr>
                    <td  class="tamano01" >Fecha Inicial: </td>
					<td>
						<input type="search" name="fechaini" id="fechaini" title="DD/MM/YYYY" value="<?php echo $_POST['fechaini']; ?>" onKeyUp="return tabular(event,this)" onKeyDown="mascara(this,'/',patron,true)" maxlength="10" class="colordobleclik" placeholder="DD/MM/YYYY" onDblClick="displayCalendarFor('fechaini');" autocomplete="off" onChange="" style="text-align: center;" />
					</td>

					<td class="tamano01" >Fecha Final: </td>
					<td>
						<input type="search" name="fechafin"  id="fechafin" title="DD/MM/YYYY"  value="<?php echo $_POST['fechafin']; ?>" onKeyUp="return tabular(event,this) " onKeyDown="mascara(this,'/',patron,true)" maxlength="10" class="colordobleclik" placeholder="DD/MM/YYYY" onDblClick="displayCalendarFor('fechafin');" autocomplete="off" onChange="" style="text-align: center;" />
					</td>

					<td style="padding-bottom:0px">
                        <em class="botonflechaverde" id="filtro" onclick="generarInforme();">Generar Informe</em>
                    </td>
				</tr>

				<tr>
					<?php
						echo"
							<td>
								<div id='titulog1' style='display:none; float:left'></div>
								<div id='progreso' class='ProgressBar' style='display:none; float:left'>
									<div class='ProgressBarText'><span id='getprogress'></span>&nbsp;% </div>
									<div id='getProgressBarFill'></div>
								</div>
							</td>";
					?>
				</tr>
			</table>

            <div class="subpantalla" style="height:69%; width:99.6%; margin-top:0px; overflow-x:hidden">
                <table class='inicio'>
                    <tr>
                        <td colspan='90' class='titulos'>Resultados Busqueda:</td>
                    </tr>
                    <tr class='titulos ' style='text-align:center;'>
                        <td colspan="4">Tesoreria</td>
                        <td colspan="1">Contabilidad</td>
						<td colspan="1">Herramientas</td>
                    </tr>
                    <tr class="titulos2" style='text-align:center;'>
                        <td style="width: 10%;">Id recaudo</td>
                        <td style="width: 10%;">Fecha</td>
                        <td>Concepto</td>
                        <td style="width: 10%;">Valor Ingreso</td>
                        <td style="width: 10%;">Valor contabilidad</td>
                        <td style="width: 10%;">Diferencia</td>
                    </tr>

                    <?php
						$co="zebra1";
						$co2="zebra2";

						$codigoComprobante = 25;
						$sqlIdCat = "SELECT id_cat FROM tipo_comprobante WHERE codigo = '$codigoComprobante'";
						$rowIdCat = mysqli_fetch_assoc(mysqli_query($linkbd, $sqlIdCat));


						$fec = explode("/", $_POST['fechaini']);
						$fechaInicial = $fec[2].'-'.$fec[1].'-'.$fec[0];

						$fec = explode("/", $_POST['fechafin']);
						$fechaFinal = $fec[2].'-'.$fec[1].'-'.$fec[0];

						$sqlReciboCaja = "SELECT id_recibos, fecha FROM tesosinreciboscaja WHERE estado = 'S' AND fecha BETWEEN '$fechaInicial' AND '$fechaFinal' ORDER BY id_recibos ASC";
						$resReciboCaja = mysqli_query($linkbd, $sqlReciboCaja);
						while ($rowReciboCaja = mysqli_fetch_assoc($resReciboCaja)) {

                            $valorTotalRecibo = 0;

							$sqlComprobanteCab = "SELECT concepto FROM comprobante_cab WHERE tipo_comp = $codigoComprobante AND numerotipo = $rowReciboCaja[id_recibos]";
							$rowComprobanteCab = mysqli_fetch_assoc(mysqli_query($linkbd, $sqlComprobanteCab));

                            $sqlReciboCajaDetalles = "SELECT SUM(valor) FROM tesosinreciboscaja_det WHERE id_recibos = $rowReciboCaja[id_recibos] AND estado  = 'S'";
                            $rowReciboCajaDetalles = mysqli_fetch_row(mysqli_query($linkbd, $sqlReciboCajaDetalles));

                            $valorTotalRecibo = $rowReciboCajaDetalles[0];

							$sqlComprobanteDet = "SELECT SUM(valdebito), SUM(valcredito) FROM comprobante_det WHERE tipo_comp = $codigoComprobante AND numerotipo = $rowReciboCaja[id_recibos] AND LEFT(cuenta,2)='11'";
							$rowComprobanteDet = mysqli_fetch_assoc(mysqli_query($linkbd, $sqlComprobanteDet));

							$valorBanco = $rowComprobanteDet['SUM(valdebito)'] - $rowComprobanteDet['SUM(valcredito)'];

							$diferencia = $valorTotalRecibo - $valorBanco;

							$estilo = "";

							if ($diferencia <= -100 || $diferencia >= 100) {
							    $estilo = "background-color: #FE5050;";
							}

                    ?>
							<tr class='<?php echo $co ?>' style="text-transform:uppercase; text-align:center; <?php echo $estilo; ?>" onclick="direccionaComprobante(<?php echo $rowIdCat['id_cat'] ?>, <?php echo $codigoComprobante ?>, <?php echo $rowReciboCaja['id_recibos'] ?>);">
								<td> <?php echo $rowReciboCaja['id_recibos'] ?> </td>
								<td> <?php echo date('d-m-Y',strtotime($rowReciboCaja['fecha'])) ?> </td>
								<td style="text-align: left;"> <?php echo $rowComprobanteCab['concepto'] ?> </td>
								<td> $ <?php echo number_format(round($valorTotalRecibo),2,',','.') ?> </td>
								<td> $ <?php echo number_format($valorBanco,2,',','.') ?> </td>
								<td> $ <?php echo number_format(round($diferencia),2,',','.') ?> </td>
							</tr>
                    <?php
							$aux=$co;
							$co=$co2;
							$co2=$aux;
						}
                    ?>

                </table>
            </div>

            <input type="hidden" name="oculto" id="oculto" value="1" />
			<input type="hidden" name="diferencia" id="diferencia" value="0" />
			<input type="hidden" name="codigoComprobante" id="codigoComprobante" value="0">

		</form>

		<div id="bgventanamodal2">
			<div id="ventanamodal2">
				<IFRAME  src="" name="buster" marginWidth=0 marginHeight=0 frameBorder=0 id="ventana2" frameSpacing=0 style="left:500px; width:900px; height:500px; top:200;"></IFRAME>
			</div>
		</div>

	</body>
</html>
