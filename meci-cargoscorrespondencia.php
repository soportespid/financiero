<?php //V 1000 12/12/16 ?> 
<!--V 1.0 24/02/2015-->
<?php
	require"comun.inc";
	require"funciones.inc";
	session_start();
	$linkbd = conectar_bd();
	$linkbdV7 = conectar_v7();
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	header("Cache-control: private"); // Arregla IE 6 
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es">
	<head>
		<?php require "head.php"; ?>
		<title>:: Meci - Calidad</title>
		<link href="css/css2.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<script type="text/javascript" src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"> </script>
		<script>
			function guardar()
			{
				var validacion01=document.getElementById('granombre').value;
				if (validacion01.trim()!='' && document.getElementById('nomdependencia').value!='' )
					{despliegamodalm('visible','4','Esta Seguro de Guardar','1');}
				else
				{
					despliegamodalm('visible','2','Faltan datos para completar el registro');
					document.form2.nombre.focus();document.form2.nombre.select();
				}
			}
			function despliegamodalm(_valor,_tip,mensa,pregunta)
			{
				document.getElementById("bgventanamodalm").style.visibility=_valor;
				if(_valor=="hidden"){document.getElementById('ventanam').src="";}
				else
				{
					switch(_tip)
					{
						case "1":
							document.getElementById('ventanam').src="ventana-mensaje1.php?titulos="+mensa;break;
						case "2":
							document.getElementById('ventanam').src="ventana-mensaje3.php?titulos="+mensa;break;
						case "3":
							document.getElementById('ventanam').src="ventana-mensaje2.php?titulos="+mensa;break;
						case "4":
							document.getElementById('ventanam').src="ventana-consulta1.php?titulos="+mensa+"&idresp="+pregunta;break;	
					}
				}
			}
			function funcionmensaje()
			{
				document.location.href = "meci-cargoscorrespondencia.php";
			}
			function respuestaconsulta(pregunta)
			{
				switch(pregunta)
				{
					case "1":	document.form2.oculto.value='2';document.form2.submit();break;
					case "2":	document.getElementById('oculto').value="6";
								document.form2.submit();break;
				}
			}
			function agregardetalle()
			{
				var validacion01=document.getElementById('nresponsable').value;
				if(validacion01.trim()!='')
				{document.form2.agregadet.value=1;document.form2.submit();}
				else {despliegamodalm('visible','2','Falta información para poder Agregar Responsable');}
			}
			function eliminar(variable)
			{
				document.form2.elimina.value=variable;
				despliegamodalm('visible','4','Esta Seguro de Eliminar Responsable','2');
			}
			function despliegamodal2(_valor)
			{
				document.getElementById("bgventanamodal2").style.visibility=_valor;
				if(_valor=="hidden"){document.getElementById('ventana2').src="";}
				else 
				{document.getElementById('ventana2').src="plan-acresponsables.php";}
			}
		</script>
	</head>
	<body>
		<IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
		<span id="todastablas2"></span>
		<table>
			<tr><script>barra_imagenes("meci");</script><?php cuadro_titulos();?></tr>	 
			<tr><?php menu_desplegable("meci");?></tr>
			<tr>
				<td colspan="3" class="cinta">
					<a onclick="location.href='meci-cargoscorrespondencia.php'" class="tooltip bottom mgbt"><img src="imagenes/add.png" /><span class="tiptext">Nuevo</span></a>
					<a onClick="guardar()" class="tooltip bottom mgbt"><img src="imagenes/guarda.png"/><span class="tiptext">Guardar</span></a>
					<a onclick="location.href='meci-cargoscorrespondenciabuscar.php'" class="tooltip bottom mgbt"><img src="imagenes/busca.png" /><span class="tiptext">Buscar</span></a>
					<a onClick="mypop=window.open('meci-principal.php','','');mypop.focus();" class="tooltip bottom mgbt"><img src="imagenes/nv.png"><span class="tiptext">Nueva Ventana</span></a>
					<a onClick="mypop=window.open('<?php echo $url2; ?>','','');mypop.focus();" class="tooltip bottom mgbt"><img src="imagenes/duplicar_pantalla.png"><span class="tiptext">Duplicar pesta&ntilde;a</span></a>
				</td>
			</tr>
		</table>
		<div id="bgventanamodalm" class="bgventanamodalm">
			<div id="ventanamodalm" class="ventanamodalm">
				<IFRAME src="" name="ventanam" marginWidth=0 marginHeight=0 frameBorder=0 id="ventanam" frameSpacing=0 style=" width:700px; height:130px; top:200; overflow:hidden;"> 
				</IFRAME>
			</div>
		</div>
		<form name="form2" method="post" action="" >
			<table class="inicio ancho" >
				<tr>
					<td class="titulos" colspan="4" width='100%'>:: Ingresar Cargo Administrativo para correspondecia</td>
					<td class="boton02" onClick="meci-principal.php">Cerrar</a>
				<tr>
					<td class="saludo1" style="width:10%">:&middot; Nombre Cargo:</td>
					<td style="width:30%"><input type="text" name="granombre" id="granombre" style="width:100%" value="<?php echo $_POST['granombre'];?>"/></td>
					<td class="saludo1" style="width:10%">:&middot; Dependencia:</td>
					<td>
						<select id="nomdependencia" name="nomdependencia" class="Listahorasmen" style="width:100%"  >
						<option value="">Seleccione....</option>
						<option value=0>&#8226; Ninguno</option>
							<?php
								$sqlr="SELECT * FROM planacareas WHERE estado='S'";
								$res=mysqli_query($linkbdV7, $sqlr);
								while ($row =mysqli_fetch_row($res)) 
								{
									if($row[0]==$_POST['nomdependencia'])
									{echo "<option value='$row[0]' SELECTED> &#8226; $row[1]</option>";}
									else{echo "<option value='$row[0]'> &#8226; $row[1]</option>";}	 	 
								}
							?> 
						</select>
					</td>
				</tr>
			</table>
			<table class="inicio ancho" >
				<tr><td colspan="6" class="titulos">Agregar Responsable</td></tr>
				<tr>
					<td class="tamano01" style="width:10%;">Tercero: </td>
					<td colspan="1"  valign="middle" style="width:25%;"><input type="text" id="responsable" name="responsable" class="tamano02" onKeyPress="javascript:return solonumeros(event)" onKeyUp="return tabular(event,this)" value="<?php echo $_POST['responsable']?>" style="width:80%;height: 30px;"/>&nbsp;<img class="icobut" src="imagenes/find02.png"  title="Listado Terceros" onClick="despliegamodal2('visible','2');"/></td>
					<td><input type="text" name="nresponsable" id="nresponsable" value="<?php echo $_POST['nresponsable']?>" style="width:100%;" class="tamano02" readonly/></td>
					<td style="width:10%;height: 30px;"><em class="botonflecha" onClick="agregardetalle()">Agregar</em></td>
				</tr>
			</table>
			<input type="hidden" name="cargotercero" id="cargotercero" value="0" >
			<input type="hidden" name="agregadet" value="0" >
			<input type="hidden" id="oculto" name="oculto" value="1"/>
			<div class="subpantalla" style="height:55.5%; width:99.5%;overflow-x:hidden;">
				<table class="inicio">
					<tr><td class="titulos" colspan="7">Responsables Agregados</td></tr>
					<tr>
						<td class="titulos2" style="width:10%;">Documento</td>
						<td class="titulos2">Nombre</td>
						<td class="titulos2" style="width:5%;text-align:center;"><img src="imagenes/del.png"/></td>
					</tr>
					<input type='hidden' name='elimina' id='elimina'>
					<?php
						if ($_POST['oculto']=='6')
						{ 
							$posi=$_POST['elimina'];
							unset($_POST['dtercero'][$posi]);
							unset($_POST['dntercero'][$posi]);
							$_POST['dtercero']= array_values($_POST['dtercero']);
							$_POST['dntercero']= array_values($_POST['dntercero']);
							$_POST['elimina'] = '';
						}
						if ($_POST['agregadet']=='1')
						{
							$_POST['dtercero'][]=$_POST['responsable'];
							$_POST['dntercero'][]=$_POST['nresponsable'];
							$_POST['agregadet']=0;
							echo"
								<script>
									document.form2.responsable.value='';
									document.form2.nresponsable.value='';
							</script>";
						}
						$iter='saludo1a';
						$iter2='saludo2';
						$_POST['condeta']=$cdtll=count($_POST['dtercero']);
						for ($x=0;$x< $cdtll;$x++)
						{
							echo "
							<input type='hidden' name='dtercero[]' value='".$_POST['dtercero'][$x]."'/>
							<input type='hidden' name='dntercero[]' value='".$_POST['dntercero'][$x]."'/>
							<tr class='$iter'>
								<td>".$_POST['dtercero'][$x]."</td>
								<td>".$_POST['dntercero'][$x]."</td>
								<td style='text-align:center;'><img src='imagenes/del.png' onclick='eliminar($x)' class='icomen1'/></td>
							</tr>";
							$aux=$iter;
							$iter=$iter2;
							$iter2=$aux;
						}
					?>
					<tr></tr>
				</table>
			</div>
			<input type="hidden" name="condeta" id="condeta" value="<?php echo $_POST['condeta'];?>"/>
			<?php echo $_POST['oculto'];
				if ($_POST['oculto']=="2")
				{
					$mxa=selconsecutivo('meci_cargoscorrespondencia','codcargo');
					$sqlr = "INSERT INTO meci_cargoscorrespondencia (codcargo,nombrecargo,dependencia,estado) VALUES ('$mxa','".$_POST['granombre']."','".$_POST['nomdependencia']."','S')";
					if (!mysqli_query($linkbdV7, $sqlr)){echo"<script>despliegamodalm('visible','2',''Error no se almaceno el ');</script>";}
					else 
					{
						for ($x=0;$x< $_POST['condeta'];$x++)
						{
							$sqlr1 = "INSERT INTO meci_cargoscorrespondencia_responsables (codcargo,dependencia,tercero,estado) VALUES ('$mxa','".$_POST['nomdependencia']."','".$_POST['dtercero'][$x]."','S')";
							mysqli_query($linkbdV7, $sqlr1);
						}
						echo"<script>despliegamodalm('visible','1','El Cargo se Guardo con exito');</script>";
					}
				}
			?>
			<div id="bgventanamodal2">
				<div id="ventanamodal2">
					<IFRAME  src="" name="buster" marginWidth=0 marginHeight=0 frameBorder=0 id="ventana2" frameSpacing=0 style="left:500px; width:900px; height:500px; top:200;"> 
					</IFRAME>
				</div>
			</div>
		</form>
	</body>
</html>