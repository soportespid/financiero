<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require 'comun.inc';
	require 'funciones.inc';
	session_start();
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html lang=es>
	<head>
    <meta charset="utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1"/>
		<meta name="viewport" content="user-scalable=no">
		<title>:: IDEAL 10 - Almacén</title>
		<link href="favicon.ico" rel="shortcut icon"/>
		<link href="css/css2z.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
        <link href="css/tabs.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" href="sweetalert2/dist/sweetalert2.min.css">
        <script type="text/javascript" src="css/programas.js"></script>
        <script src="sweetalert2/dist/sweetalert2.min.js"></script>
		<script src="vue/vue.min.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script src="css/calendario.js?<?php echo date('d_m_Y_h_i_s');?>"></script>

        <style>
            .checkbox-wrapper-31:hover .check {
                stroke-dashoffset: 0;
            }

            .checkbox-wrapper-31 {
                position: relative;
                display: inline-block;
                width: 40px;
                height: 40px;
            }
            .checkbox-wrapper-31 .background {
                fill: #ccc;
                transition: ease all 0.6s;
                -webkit-transition: ease all 0.6s;
            }
            .checkbox-wrapper-31 .stroke {
                fill: none;
                stroke: #fff;
                stroke-miterlimit: 10;
                stroke-width: 2px;
                stroke-dashoffset: 100;
                stroke-dasharray: 100;
                transition: ease all 0.6s;
                -webkit-transition: ease all 0.6s;
            }
            .checkbox-wrapper-31 .check {
                fill: none;
                stroke: #fff;
                stroke-linecap: round;
                stroke-linejoin: round;
                stroke-width: 2px;
                stroke-dashoffset: 22;
                stroke-dasharray: 22;
                transition: ease all 0.6s;
                -webkit-transition: ease all 0.6s;
            }
            .checkbox-wrapper-31 input[type=checkbox] {
                position: absolute;
                width: 100%;
                height: 100%;
                left: 0;
                top: 0;
                margin: 0;
                opacity: 0;
                -appearance: none;
            }
            .checkbox-wrapper-31 input[type=checkbox]:hover {
                cursor: pointer;
            }
            .checkbox-wrapper-31 input[type=checkbox]:checked + svg .background {
                fill: #6cbe45;
            }
            .checkbox-wrapper-31 input[type=checkbox]:checked + svg .stroke {
                stroke-dashoffset: 0;
            }
            .checkbox-wrapper-31 input[type=checkbox]:checked + svg .check {
                stroke-dashoffset: 0;
            }
        </style>
	</head>
	<body>
		<header>
			<table>
				<tr><script>barra_imagenes("inve");</script><?php cuadro_titulos();?></tr>
			</table>
		</header>

		<form name="form2" method="post" action="">
			<section id="myapp" v-cloak >
                <div id="cargando" v-show="isLoading" class="loading" style="z-index: 10000;">
                    <span>Cargando...</span>
                </div>
				<nav>
					<table>
						<tr><?php menu_desplegable("inve");?></tr>
                        <tr>
							<td colspan="3" class="cinta">
                                <img src="imagenes/add.png" @click="window.location.href='inve-grupoInventarioCrear.php'" class="mgbt" title="Nuevo">
								<img src="imagenes/guardad.png"  title="Guardar"  class="mgbt">
								<img src="imagenes/buscad.png"   class="mgbt" title="Buscar">
								<img src="imagenes/nv.png" @click="mypop=window.open('inve-principal.php','',''); mypop.focus();" class="mgbt" title="Nueva Ventana">
							</td>
						</tr>
					</table>
				</nav>
				<article>
                    <div>
                        <table  class="inicio" align="center" >
                            <tbody>
                                <tr>
                                    <td class="titulos" colspan="4">:: Buscar grupo de inventario</td>
                                    <td class="cerrar" style="width:7%;"><a onClick="location.href='contra-principal.php'">Cerrar</a></td>
                                </tr>
                                <tr>
                                    <td>
                                        <input type="search"  placeholder="buscar por nombre" @keyup="search()" v-model="strSearch" style="width:55%">&nbsp;
                                        <input type="button" @click="search()"  value="Buscar"  />
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <div class='subpantalla' style='height:50vh; width:100%; margin-top:0px;  overflow-x:hidden'>
                            <table class='inicio' align='center'>
                                <tbody>
                                    <tr>
                                        <td colspan='12' class='titulos'>.: Resultados Busqueda:</td>
                                    </tr>
                                    <tr>
                                        <td colspan='12'>Total: {{intResults}}</td>
                                    </tr>
                                </tbody>
                            </table>
                            <table class='inicio'>
                                <thead>
                                    <tr>
                                        <th class="titulosnew00" style="width: 10%;">Código</th>
                                        <th class="titulosnew00" align="left" style="width: 80%;">Nombre</th>
                                        <th class="titulosnew00" align="left" style="width: 10%;">Estado</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr @dblclick="editItem(data.codigo)" style="height:50px;" v-for="(data,index) in arrData"  :key="index" v-bind:class="index % 2 ? 'saludo1a' : 'saludo2'">
                                        <td style="text-align:center">{{ data.codigo}}</td>
                                        <td style="text-align:center">{{ data.nombre}}</td>
                                        <td style="text-align:center;">
                                            <span v-if="data.estado == 'S'" style="padding:4px; border-radius:5px; background:#008000;color:#fff;font-weight:bold">ACTIVO</span>
                                            <span v-else style="padding:4px; border-radius:5px; background:#ff0000;color:#fff;font-weight:bold">INACTIVO</span>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div v-if="arrData !=''" class="inicio">
                            <p style="text-align:center">Página {{ intPage }} de {{ intTotalPages }}</p>
                            <ul style="list-style:none; padding:0;display:flex;justify-content: center;align-items:center">
                                <li v-show="intPage > 1" @click="search(intPage = 1)" style="cursor:pointer;padding:4px 10px;background:#fff; border:1px solid #39c"><< </li>
                                <li v-show="intPage > 1" @click="search(--intPage)" style="cursor:pointer;padding:4px 10px;background:#fff; border:1px solid #39c" ><</li>
                                <li v-show="intPage < intTotalPages" @click="search(++intPage)" style="cursor:pointer;padding:4px 10px;background:#fff; border:1px solid #39c" >></li>
                                <li v-show="intPage < intTotalPages" @click="search(intPage = intTotalPages)" style="cursor:pointer;padding:4px 10px;background:#fff; border:1px solid #39c">>></li>
                            </ul>
                        </div>
                    </div>
				</article>
			</section>
		</form>

		<script src="node_modules/read-excel-file/bundle/read-excel-file.min.js"></script>
		<script src="node_modules/xlsx/dist/xlsx.full.min.js"></script>
        <script src="Librerias/vue/vue.min.js"></script>
		<script src="Librerias/vue/axios.min.js"></script>
		<script type="module" src="almacen/GrupoInventario/buscar/inve-grupoInventarioBuscar.js?<?= date('d_m_Y_h_i_s');?>"></script>

	</body>
</html>
