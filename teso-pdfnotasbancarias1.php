<?php
    require_once("tcpdf/tcpdf_include.php");
    require('comun.inc');
    require"funciones.inc";
    date_default_timezone_set("America/Bogota");
    session_start();
    $val = 0;
      class MYPDF extends TCPDF 
      {
		public function Header() 
		{
			if ($_POST['estado']=='R'){$this->Image('imagenes/reversado02.png',75,41.5,50,15);}
			$linkbd=conectar_bd();
			$sqlr="select *from configbasica where estado='S' ";
			//echo $sqlr;
			$res=mysql_query($sqlr,$linkbd);
			while($row=mysql_fetch_row($res))
			{
				$nit=$row[0];
				$rs=$row[1];
				$nalca=$row[6];
			}
			//Parte Izquierda
			$this->Image('imagenes/escudo.jpg', 22, 12, 25, 23.9, 'JPG', '', 'T', true, 300, '', false, false, 0, false, false, false);// Logo
			$this->SetFont('helvetica','B',8);
			$this->SetY(10);
			$this->RoundedRect(10, 10, 199, 31, 1,'' );
			$this->Cell(0.1);
			$this->Cell(50,31,'','R',0,'L'); 
			$this->SetY(11);
			$this->SetX(60);
			$this->SetFont('helvetica','B',12);
			$this->Cell(149,12,strtoupper(iconv($_SESSION["VERCARPDFINI"], $_SESSION["VERCARPDFFIN"]."//TRANSLIT","$rs")),0,0,'C'); 
			$this->SetFont('helvetica','B',8);
			$this->SetY(18);
			$this->SetX(60);
			$this->SetFont('helvetica','B',11);
			$this->Cell(149,10,"$nit",0,0,'C');
			//*****************************************************************************************************************************
			$this->SetFont('helvetica','B',14);
			$this->SetY(10);
			$this->Cell(50.1);
			$this->Cell(149,31,'',0,1,'C'); 
			$this->SetY(27);
			$this->SetX(60);
			$this->Cell(111,14,"NOTAS BANCARIAS",0,0,'C'); 
			$mov='';
			if(isset($_POST['movimiento']))
			{
				if(!empty($_POST['movimiento']))
				{
					if($_POST['movimiento']=='401'){$mov="DOCUMENTO DE REVERSION";}
				}
			}

			$fechaPor = explode("/", $_POST['fecha']);
			$vigencia = $fechaPor[2];

			$this->SetFont('helvetica','B',10);
			$this->SetY(15);
			$this->Cell(50.1);
			$this->Cell(149,20,$mov,0,0,'C'); 
			//************************************
			$this->SetFont('helvetica','I',7);
			$this->SetY(27);
			$this->Cell(50.2);
			$this->multiCell(110.7,3,''.strtoupper($detallegreso),'T','L');
			$this->SetFont('helvetica','B',10);
			$this->SetY(27);
			$this->Cell(161.1);
			$this->Cell(37.8,14,'','TL',0,'L');
			$this->SetY(27);
			$this->Cell(162);
			$this->Cell(35,5,'NUMERO : '.$_POST['numeroNota'],0,0,'L');
			$this->SetY(31);
			$this->Cell(162);
			$this->Cell(35,5,'FECHA: '.$_POST['fecha'],0,0,'L');
			$this->SetY(35);
			$this->Cell(162);
			$this->Cell(35,5,'VIGENCIA: '.$vigencia,0,0,'L');
			$this->SetY(27);
			$this->Cell(50.2);
			$this->MultiCell(105.7,4,'',0,'L');		
			//**********************************************************
			$this->SetFont('times','B',10);
			$this->ln(12);
			//**********************************************************
		}
		public function Footer() 
		{
			$linkbd=conectar_bd();
			$sqlr="SELECT direccion,telefono,web,email FROM configbasica WHERE estado='S'";
			$resp=mysql_query($sqlr,$linkbd);
			while($row=mysql_fetch_row($resp))
			{
				$direcc=iconv($_SESSION["VERCARPDFINI"], $_SESSION["VERCARPDFFIN"]."//TRANSLIT",strtoupper($row[0]));
				$telefonos=$row[1];
				$dirweb=iconv($_SESSION["VERCARPDFINI"], $_SESSION["VERCARPDFFIN"]."//TRANSLIT",strtoupper($row[3]));
				$coemail=iconv($_SESSION["VERCARPDFINI"], $_SESSION["VERCARPDFFIN"]."//TRANSLIT",strtoupper($row[2]));
			}
			if($direcc!=''){$vardirec="Dirección: $direcc, ";}
			else {$vardirec="";}
			if($telefonos!=''){$vartelef="Telefonos: $telefonos";}
			else{$vartelef="";}
			if($dirweb!=''){$varemail="Email: $dirweb, ";}
			else {$varemail="";}
			if($coemail!=''){$varpagiw="Pagina Web: $coemail";}
			else{$varpagiw="";}
			$this->SetFont('helvetica', 'I', 8);
			$txt = <<<EOD
$vardirec $vartelef
$varemail $varpagiw
EOD;
			$this->Write(0, $txt, '', 0, 'C', true, 0, false, false, 0);
			$this->Cell(0, 10, 'Page '.$this->getAliasNumPage().'/'.$this->getAliasNbPages(), 0, false, 'C', 0, '', 0, false, 'T', 'M');
			
		}
	}
//$this->multiCell(110.7,7,''.strtoupper($_POST[concepto]),'T','L');
    $pdf = new MYPDF('P','mm','Letter', true, 'iso-8859-1', false);
    $pdf->SetDocInfoUnicode (true); 
	// set document information
	$pdf->SetCreator(PDF_CREATOR);
	$pdf->SetAuthor('G&CSAS');
	$pdf->SetTitle('Certificados');
	$pdf->SetSubject('Notas bancarias');
	$pdf->SetKeywords('TCPDF, PDF, example, test, guide');
	$pdf->SetMargins(10, 50, 10);// set margins
	$pdf->SetHeaderMargin(101);// set margins
	$pdf->SetFooterMargin(20);// set margins
	$pdf->SetAutoPageBreak(TRUE, 20);// set auto page breaks
	// set some language-dependent strings (optional)
	if (@file_exists(dirname(__FILE__).'/lang/spa.php')) 
	{
		require_once(dirname(__FILE__).'/lang/spa.php');
		$pdf->setLanguageArray($l);
	}
	$pdf->AddPage();
$pdf->SetFont('Times','',10);
$pdf->SetAutoPageBreak(true,20);


$pdf->ln(5);

$pdf->cell(0.2);
$pdf->SetFillColor(255,255,255);
$detallegreso = $_POST['concepto'];

$lineas = $pdf->getNumLines($detallegreso, 174);
$alturadt=(5*$lineas);
$pdf->SetFont('helvetica','B',10);
$pdf->MultiCell(25,$alturadt,'CONCEPTO: ', '', 'J', 0, 0, '', '', true, 0, false, true, $alturadt, 'T');
$pdf->SetFont('helvetica','',10);
$pdf->MultiCell(174,$alturadt,"$detallegreso",'','L',false,1,'','',true,0,false,true,$alturadt,'T',false);



/* $pdf->ln(6);
$y=$pdf->GetY();
$pdf->SetY($y);
$pdf->SetFillColor(222,222,222);
$pdf->SetFont('helvetica','B',10);
$pdf->Cell(0.1);
$pdf->Cell(199,5,'DETALLE',0,0,'C',1);
$pdf->ln(6); 
$y=$pdf->GetY();	
$pdf->SetFillColor(222,222,222);
$pdf->SetFont('helvetica','B',10);
$pdf->SetY($y);
$pdf->Cell(0.1);
$pdf->Cell(154,5,'Nombre',0,0,'C',1);
$pdf->SetY($y);
$pdf->Cell(155);
$pdf->Cell(44,5,'Valor',0,0,'C',1);
$pdf->SetFont('helvetica','',8);
$cont=0; */


//********************************************************************************************************************************
$pdf->line(10.1,66,209,66);
$pdf->RoundedRect(10,69, 199, 5, 1.2,'' );
$pdf->SetFont('helvetica','B',10);
$pdf->SetY(69);
$pdf->Cell(0.1);
$pdf->Cell(16,5,'CC',0,1,'C'); 
$pdf->SetY(69);
$pdf->Cell(18.1);
$pdf->Cell(30,5,'DOC BANCARIO',0,1,'C');
$pdf->SetY(69);
$pdf->Cell(56.1);
$pdf->Cell(30,5,'BANCO',0,1,'C');
$pdf->SetY(69);
$pdf->Cell(118.1);
$pdf->Cell(30,5,'GASTO BANCARIO',0,1,'C');
$pdf->SetY(69);
$pdf->Cell(159.1);
$pdf->Cell(20,5,'VALOR',0,1,'C');

$pdf->ln(4);
        
//***********************************************************************************************************************************
$linkbd=conectar_bd();
$sqlr = "SELECT * FROM tesonotasbancarias_det WHERE id_notabancab = $_POST[numeroNota]";
$res=mysql_query($sqlr,$linkbd);
while($row=mysql_fetch_assoc($res))
{
	$pdf->ln(2);
	if ($con%2==0){$pdf->SetFillColor(255,255,255);}
    else{$pdf->SetFillColor(245,245,245);}
	$pdf->SetFont('helvetica','',10);
	$pdf->Cell(1.1);
	$pdf->Cell(16,4,''.$row['cc'],'',0,'L',1);
	$pdf->Cell(8.1);
	$pdf->Cell(30,4,$row['docban'],'',0,'',1);
	$pdf->Cell(8.1);
	$pdf->Cell(30,4,$row['ncuentaban'],'',0,'',1);
	$pdf->Cell(30.1);
	$pdf->Cell(30,4,$row['gastoban'],'',0,'',1);
	$pdf->Cell(2.1);
	$pdf->Cell(20,4,'$ '.$row['valor'],'',1,'R',1);
	$con=$con+1;
}

$y=$pdf->GetY();
$pdf->line(10.1,$y + 5,209,$y + 5);
/* for($x=0;$x<count($_POST['ddescuentos']);$x++)
{
    if ($con%2==0){$pdf->SetFillColor(255,255,255);}
    else{$pdf->SetFillColor(245,245,245);}
		$pdf->Cell(154,4,''.$_POST['dndescuentos'][$x],'',0,'L',1);
		$pdf->Cell(44,4,'$'.$_POST['dfvalores'][$x],'',1,'R',1);
		$con=$con+1;
   } */
   
    $pdf->ln(8);
    $linkbd=conectar_bd();

	/* $fechaPor = explode("/", $_POST['fecha']);
	$vigencia = $fechaPor[2]; */

	$vigusu=vigencia_usuarios($_SESSION['cedulausu']);

	$sqlr="select id_cargo,id_comprobante from pptofirmas where id_comprobante='20' and vigencia='".$vigusu."'";
	$res=mysql_query($sqlr,$linkbd);
	
	while($row=mysql_fetch_assoc($res))
	{
		if($row["id_cargo"]=='0')
		{
			$_POST[ppto][]=buscatercero($_POST[tercero]);
			$_POST[nomcargo][]='BENEFICIARIO';
		}
		else
		{
			$sqlr1="select cedulanit,(select nombrecargo from planaccargos where codcargo='".$row["id_cargo"]."') from planestructura_terceros where codcargo='".$row["id_cargo"]."' and estado='S'";
			$res1=mysql_query($sqlr1,$linkbd);
			$row1=mysql_fetch_row($res1);
			$_POST[ppto][]=buscar_empleado($row1[0]);
			$_POST[nomcargo][]=$row1[1];
		}
	};
	for($x=0;$x<count($_POST[ppto]);$x++)
	{
		$pdf->ln(20);
		$v=$pdf->gety();
		if($v>=251)
		{ 
			$pdf->AddPage();
			$pdf->ln(20);
			$v=$pdf->gety();
		}
		$pdf->setFont('times','B',8);
		if (($x%2)==0) 
		{
			if(isset($_POST[ppto][$x+1]))
			{
				$pdf->Line(17,$v,107,$v);
				$pdf->Line(112,$v,202,$v);
				$v2=$pdf->gety();
				$pdf->Cell(104,4,''.iconv($_SESSION["VERCARPDFINI"], $_SESSION["VERCARPDFFIN"]."//TRANSLIT",$_POST[ppto][$x]),0,1,'C',false,0,0,false,'T','C');
				$pdf->Cell(104,4,''.iconv($_SESSION["VERCARPDFINI"], $_SESSION["VERCARPDFFIN"]."//TRANSLIT",$_POST[nomcargo][$x]),0,1,'C',false,0,0,false,'T','C');
				$pdf->SetY($v2);
				$pdf->Cell(295,4,''.iconv($_SESSION["VERCARPDFINI"], $_SESSION["VERCARPDFFIN"]."//TRANSLIT",$_POST[ppto][$x+1]),0,1,'C',false,0,0,false,'T','C');
				$pdf->Cell(295,4,''.iconv($_SESSION["VERCARPDFINI"], $_SESSION["VERCARPDFFIN"]."//TRANSLIT",$_POST[nomcargo][$x+1]),0,1,'C',false,0,0,false,'T','C');
			}
			else
			{
				$pdf->Line(50,$v,160,$v);
				$pdf->Cell(190,4,''.iconv($_SESSION["VERCARPDFINI"], $_SESSION["VERCARPDFFIN"]."//TRANSLIT",$_POST[ppto][$x]),0,1,'C',false,0,0,false,'T','C');
				$pdf->Cell(190,4,''.iconv($_SESSION["VERCARPDFINI"], $_SESSION["VERCARPDFFIN"]."//TRANSLIT",$_POST[nomcargo][$x]),0,0,'C',false,0,0,false,'T','C');
			}
			$v3=$pdf->gety();
		}
		$pdf->SetY($v3);
		$pdf->SetFont('helvetica','',7);
    }
$pdf->Output();
?>