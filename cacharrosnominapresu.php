<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=iso-8859-1");
	require 'comun.inc';
	require 'funciones.inc';
	require 'validaciones.inc';
	require 'funcionesnomima.inc';
	session_start();
	$linkbd=conectar_v7();
	$linkbd -> set_charset("utf8");
	cargarcodigopag($_GET[codpag],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");//prueba
?>
<!DOCTYPE >
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es">
	<head>
		<meta http-equiv="Content-type" content="text/html; charset=iso-8859-1"/>
		<meta http-equiv="X-UA-Compatible" content="IE=9"/>
		<title>:: Spid - Tesoreria</title>
		<link href="css/css2.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<style>
			.example1
			{
				font-size: 3em;
				color: limegreen;
				position: absolute;
				width: 100%;
				height: 100%;
				margin: 0;
				line-height: 50px;
				text-align: center;
				transform:translateX(100%);/* Starting position */
				animation: example1 15s linear infinite;/* Apply animation to this element */
			}
			@keyframes example1 /* Move it (define the animation) */
			{
				0% {transform: translateX(100%);}
				100% {transform: translateX(-100%);}
			}
		</style>
		<script type="text/javascript" src="css/programas.js"></script>
		<script>
			function procesos(){document.form2.oculto.value='2';document.form2.submit();}
		</script>
		<?php titlepag();?>
	</head>
	<body>
		<IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
		<span id="todastablas2"></span>
		<table>
			<tr><script>barra_imagenes("hum");</script><?php cuadro_titulos();?></tr>
			<tr><?php menu_desplegable("hum");?></tr>
		</table>
		<form name="form2" method="post" action="">
			<table class="inicio">
				<tr>
					<td class="titulos" colspan="7">ARREGLO CACHARROS VARIOS</td>
					<td class="cerrar" style="width:7%" onClick="location.href='teso-principal.php'">Cerrar</td>
				</tr>
				<tr>
					<td class="saludo1" style="width:2.5cm;">.: Inicio:</td>
					<td style="width:10%;"><input type="text" name="varini" id="varini" value="<?php echo @$_POST['varini']?>" style="width:100%"/></td>
					<td class="saludo1" style="width:2.5cm;">.: Fin:</td>
					<td style="width:10%;"><input type="text" name="varfin" id="varfin" value="<?php echo @$_POST['varfin']?>" style="width:100%"/></td>
					<td></td>
				</tr>
				<tr>
					<td class="saludo1">.: Proceso:</td>
					<td colspan="4" style='height:28px;'>
						<select name="selecob" id="selecob" style='text-transform:uppercase; width:100%; height:22px;'>
							<option value="">....</option>
							<option value='1' <?php if(@$_POST['selecob']=='1'){echo "SELECTED";}?>>1: Separar numero de proyecto del numero decuenta</option>
							<option value='2' <?php if(@$_POST['selecob']=='2'){echo "SELECTED";}?>>2: Completar la informacion presupuesto nomina</option>
							<option value='3' <?php if(@$_POST['selecob']=='3'){echo "SELECTED";}?>>3: Completar fuente funcionamiento egreso nomina 2021</option>
							
						</select>
					<td style="padding-bottom:5px" colspan="2"><em class="botonflecha" onClick="procesos();">Correr</em></td>
					<td></td>
				</tr>
			</table>
			<input type="hidden" name="oculto" id="oculto" value="1"/>
			<div class="subpantalla" style="height:68.5%; width:99.6%;overflow-x:hidden" id="divdet">
			<?php
				if(@$_POST['oculto']==2)
				{
					switch ($_POST['selecob'])
					{
						case '1':	//Separar numero de proyecto del numero decuenta
						{
							if($_POST['varini']=='SI')
							{
								$sql01 ="SELECT cuentap, id_det FROM tesoegresosnomina_det WHERE cuentap LIKE '%<_>%' ORDER BY id_egreso DESC";
								$res01 = mysqli_query($linkbd,$sql01);
								while ($row01 = mysqli_fetch_row($res01))
								{
									$egrenomi = explode("<_>",$row01[0]);
									$sql02 = "UPDATE tesoegresosnomina_det SET cuentap = '$egrenomi[0]', proyecto = '$egrenomi[1]' WHERE id_det = '$row01[1]'";
									mysqli_query($linkbd,$sql02);
									echo "(id) $row01[1], (cuentap) $egrenomi[0], (proyecto) $egrenomi[1] - ";
								}
								$sql01 ="SELECT cuentap, id_det FROM tesoegresosnomina_det WHERE cuentap LIKE '%<_>%' ORDER BY id_egreso DESC";
								$res01 = mysqli_query($linkbd,$sql01);
								while ($row01 = mysqli_fetch_row($res01))
								{
									$egrenomi = explode("<_>",$row01[0]);
									$sql02 = "UPDATE tesoegresosnomina_det SET cuentap = '$egrenomi[0]', proyecto = '$egrenomi[1]' WHERE id_det = '$row01[1]'";
									mysqli_query($linkbd,$sql02);
									echo "(id) $row01[1], (cuentap) $egrenomi[0], (proyecto) $egrenomi[1] - ";
								}
							}
						}break;
						case '2': //Completar la informacion presupuesto nomina
						{
							if($_POST['varini']=='SI')
							{
								$sql01 ="SELECT proyecto, cuenta, id FROM humnom_presupuestal ORDER BY id_nom DESC";
								$res01 = mysqli_query($linkbd,$sql01);
								while ($row01 = mysqli_fetch_row($res01))
								{
									if($row01[0]<>'')
									{
										$sql02 = "SELECT codigo FROM ccpproyectospresupuesto WHERE id = '$row01[0]'";
										$res02 = mysqli_query($linkbd,$sql02);
										$row02 = mysqli_fetch_row($res02);
										
										$sql03 = "UPDATE humnom_presupuestal SET bpin = '$row02[0]' WHERE id = '$row01[2]'";
										mysqli_query($linkbd,$sql03);
										
										$sql02 = "SELECT indicador_producto, id_fuente FROM ccpproyectospresupuesto_presupuesto WHERE codproyecto = '$row01[0]' AND rubro = '$row01[1]'";
										$res02 = mysqli_query($linkbd,$sql02);
										$row02 = mysqli_fetch_row($res02);
										
										$sql03 = "UPDATE humnom_presupuestal SET indicador = '$row02[0]' WHERE id = '$row01[2]'";
										mysqli_query($linkbd,$sql03);
										
										$sql04 = "UPDATE humnom_presupuestal SET fuente = '$row02[1]' WHERE id = '$row01[2]' AND fuente = ''";
										mysqli_query($linkbd,$sql04);
									}
								}
								
								$sql01 ="SELECT nomina, cdp, rp FROM hum_nom_cdp_rp WHERE vigencia = '2021' ORDER BY id DESC";
								$res01 = mysqli_query($linkbd,$sql01);
								while ($row01 = mysqli_fetch_row($res01))
								{
									$sql02 ="SELECT cuenta, fuente, indicador, bpin, proyecto FROM humnom_presupuestal WHERE id_nom = '$row01[0]' ORDER BY id_nom DESC";
									$res02 = mysqli_query($linkbd,$sql02);
									while ($row02 = mysqli_fetch_row($res02))
									{
										$sql03 = "UPDATE ccpetcdp_detalle SET indicador_producto = '$row02[2]', bpim = '$row02[3]' WHERE vigencia = '2021' AND consvigencia  = '$row01[1]' AND cuenta = '$row02[0]' AND fuente = '$row02[1]'";
										mysqli_query($linkbd,$sql03);
										
										$sql03 = "UPDATE ccpetrp_detalle SET indicador_producto = '$row02[2]', bpim = '$row02[3]' WHERE vigencia = '2021' AND consvigencia  = '$row01[2]' AND cuenta = '$row02[0]' AND fuente = '$row02[1]'";
										mysqli_query($linkbd,$sql03);
										
										$sql03 = "UPDATE tesoegresosnomina_det SET indicador_producto = '$row02[2]', bpin = '$row02[3]' proyecto = '$row02[4]' WHERE id_orden = '$row01[0]' AND cuentap = '$row02[0]'";
										mysqli_query($linkbd,$sql03);
										
										$sql04 = "UPDATE tesoegresosnomina_det SET fuente = '$row02[1]' WHERE id_orden = '$row01[0]' AND cuentap = '$row02[0]' AND fuente = ''";
										mysqli_query($linkbd,$sql04);
									}
								}
							}
						}break;
						case '3': //Completar fuente funcionamiento egreso nomina 2021
						{
							if($_POST['varini'] != '' && $_POST['varfin'] != '')
							{
								for($x = $_POST['varini']; $x <= $_POST['varfin']; $x++)
								{
									$sql01 ="SELECT cuentap, id_det, fuente FROM tesoegresosnomina_det WHERE id_egreso = '$x'";
									$res01 = mysqli_query($linkbd,$sql01);
									while ($row01 = mysqli_fetch_row($res01))
									{
										if($row01[2] == '')
										{
											$sql02 ="SELECT fuente FROM ccpetinicialvalorgastos WHERE cuenta = '$row01[0]' AND vigencia = '2021'";
											$res02 = mysqli_query($linkbd,$sql02);
											$row02 = mysqli_fetch_row($res02);
											$sql03 = "UPDATE tesoegresosnomina_det SET fuente = '$row02[0]' WHERE id_det = '$row01[1]'";
											mysqli_query($linkbd,$sql03);
											echo "$x-";
										}
									}
									
								}
							}
						}
					}
				}
			?> 
			</div> 
		</form>
	</body>
</html>