<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require 'comun.inc';
	require 'funciones.inc';
	session_start();
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html lang=es>
	<head>
		<meta charset="utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1"/>
		<meta name="viewport" content="user-scalable=no">
		<title>:: IDEAL 10 - Presupuesto CCPET</title>
		<link href="favicon.ico" rel="shortcut icon"/>
		<link href="css/css2z.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
        <link href="css/tabs.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" href="sweetalert2/dist/sweetalert2.min.css">
        <script src="sweetalert2/dist/sweetalert2.min.js"></script>
		<script src="vue/vue.min.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script src="css/calendario.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
        <link href="css/style.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
        <script>

        </script>
		<style>
			.c9 input[type="checkbox"]:not(:checked),
            .c9 input[type="checkbox"]:checked {
                position: absolute !important;
                left: -9999px !important;
            }
            .c9 input[type="checkbox"]:not(:checked) +  #t9,
            .c9 input[type="checkbox"]:checked +  #t9 {
                position: relative !important;
                padding-left: 1.95em !important;
                cursor: pointer !important;
            }

            /* checkbox aspect */
            .c9 input[type="checkbox"]:not(:checked) +  #t9:before,
            .c9 input[type="checkbox"]:checked +  #t9:before {
                content: '' !important;
                position: absolute !important;
                left: 0 !important; top: -2 !important;
                width: 1.55em !important; height: 1.55em !important;
                border: 2px solid #ccc !important;
                background: #fff !important;
                border-radius: 4px !important;
                box-shadow: inset 0 1px 3px rgba(0,0,0,.1) !important;
            }
            /* checked mark aspect */
            .c9 input[type="checkbox"]:not(:checked) +  #t9:after,
            .c9 input[type="checkbox"]:checked + #t9:after {
                content: url(imagenes/tilde.png) !important;
                position: absolute !important;
                top: .1em; left: .3em !important;
                font-size: 1.3em !important;
                line-height: 0.8 !important;
                color: #09ad7e !important;
                transition: all .2s !important;
            }
            /* checked mark aspect changes */
            .c9 input[type="checkbox"]:not(:checked) +  #t9:after {
                opacity: 0 !important;
                transform: scale(0) !important;
            }
            .c9 input[type="checkbox"]:checked +  #t9:after {
                opacity: 1 !important;
                transform: scale(1) !important;
            }
            /* disabled checkbox */
            .c9 input[type="checkbox"]:disabled:not(:checked) +  #t9:before,
            .c9 input[type="checkbox"]:disabled:checked +  #t9:before {
                box-shadow: none !important;
                border-color: #bbb !important;
                background-color: #ddd !important;
            }
            .c9 input[type="checkbox"]:disabled:checked +  #t9:after {
                color: #999 !important;
            }
            .c9 input[type="checkbox"]:disabled +  #t9 {
                color: #aaa !important;
            }
            /* accessibility */
            .c9 input[type="checkbox"]:checked:focus + #t9:before,
            .c9 input[type="checkbox"]:not(:checked):focus + #t9:before {
                border: 2px dotted blue !important;
            }

            /* hover style just for information */
            .c9 #t9:hover:before {
                border: 2px solid #4778d9 !important;
            }
            #t9{
                background-color: white !important;
            }

			.checkout-estilos{
				display: flex;
				flex-direction: row;
				gap: 10px;
				font-family: 'Times New Roman', Times, serif;
				font-weight: 600px;
				font-size: 14px;
			}

			input[type=number]::-webkit-inner-spin-button,
			input[type=number]::-webkit-outer-spin-button {
				-webkit-appearance: none;
				margin: 0;
			}
			input[type=number]{
				text-align:right;
				box-sizing: border-box;
				font: 120% sans-serif;
				/* width: 100% !important; */
			}
			[v-cloak]{display : none;}

			.centrarSelect {

				height: 30px !important;
				text-align-last:center !important;
			}

			.aumentarTamaño {

				font-size:15px;
			}

            .tamano01 {
                text-align: center !important;
            }

			.captura{
				color: black !important;
                font-weight: normal;
                cursor: pointer !important;
            }

            .captura:hover{
                background: linear-gradient(#40f3ff , #40b3ff 70%, #40f3ff );
            }

            .colorFila{
                background-color: #F0FFFD !important;
            }

            .colorFila1{
                background-color: #FFF02A !important;
                font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, "Helvetica Neue", Arial, "Noto Sans", sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol", "Noto Color Emoji";
                font-size:10px;
                font-weight:normal;
                border:#eeeeee 1px solid;
                padding-left:3px;
                padding-right:3px;
                margin-bottom:1px;
                margin-top:1px;
                height:23.5px;
                border-radius: 1px;
            }

            .colorFila1:hover{
                background: linear-gradient(#40f3ff , #40b3ff 70%, #40f3ff );
            }

            .agregado{
				color: black !important;
                font-weight: 700 !important;
            }

			.bienesServicios{
				font-weight: 600 !important;
				font-style: oblique;
				cursor: pointer !important;
			}

		</style>
	</head>
	<body>
		<header>
			<table>
				<tr><script>barra_imagenes("ccpet");</script><?php cuadro_titulos();?></tr>
			</table>
		</header>

		<form name="form2" method="post" action="">
			<section id="myapp" v-cloak >
				<nav>
					<table>
						<tr><?php menu_desplegable("ccpet");?></tr>
						<tr>
							<td colspan="3" class="cinta">
								<img src="imagenes/add2.png" v-on:click="location.href=''" class="mgbt1" title="Nuevo">
								<img src="imagenes/guardad.png" title="Guardar" v-on:click="" class="mgbt1">
								<img src="imagenes/buscad.png" v-on:click="location.href=''" class="mgbt1" title="Buscar">
								<img src="imagenes/nv.png" onClick="mypop=window.open('ccp-principal.php','',''); mypop.focus();" class="mgbt" title="Nueva Ventana">
								<img src="imagenes/excel.png" v-on:click="excel" title="Excel" class="mgbt">
								<img src="imagenes/iratras.png" title="Atr&aacute;s" onClick="location.href='ccp-ejecucionpresupuestal.php'" class="mgbt">
							</td>
						</tr>
					</table>
				</nav>

				<article>
                    <div>
                        <table class="inicio">
                            <tr>
                                <td class="titulos" colspan="12" style="text-align:center; font-weight: 600;">.: Ejecución presupuestal de gastos - Funcionamiento - Servicio de la deuda - CPC</td>
                            	<td class="cerrar" style="width:4%" onClick="location.href='ccp-principal.php'">Cerrar</td>
                            </tr>

                            <tr>
                                <td style="width: 12%;">
									<label class="labelR" for="">Dependencia:</label>
								</td>
                                <td style="width: 6%;">
                                    <select v-model="secPresu">
                                        <option value="">Todas</option>
                                        <option v-for="sec in seccionPresupuestal" v-bind:value="sec[0]">
                                            {{ sec[0] }} - {{ sec[1] }}
                                        </option>
                                    </select>
                                </td>

                                <td style="width: 10%;">
									<label class="labelR" for="">Situción/Fondo:</label>
								</td>
                                <td style="width: 5%;">
                                    <select v-model="medioPago" style="width:98%;">
                                        <option value="">Todos</option>
                                        <option value="CSF">CSF</option>
                                        <option value="SSF">SSF</option>
                                    </select>
                                </td>

                                <td style="width: 14%;">
									<label class="labelR" for="">Vig. gasto:</label>
								</td>
                                <td style="width: 8%;">
                                    <select v-model="vigGasto">
                                        <option value="">Todas</option>
                                        <option v-for="vg in vigenciaGasto" v-bind:value="vg[0]">
                                            {{ vg[0 ]}} - {{ vg[1] }}
                                        </option>
                                    </select>
                                </td>

								<td style = "width: 6%;">
									<label class="labelR" for="">Fuente:</label>
								</td>
								<td style = "width: 20%;">
									<div>
										<multiselect
											v-model="valueFuentes"
											placeholder="Seleccione una o varias fuentes"
											label="fuente" track-by="fuente"
											:custom-label="fuenteConNombre"
											:options="optionsFuentes"
											:multiple="true"
											:taggable="false"
											@input = "cambiaCriteriosBusqueda"
										></multiselect>
									</div>
								</td>
								<td></td>
                            </tr>

                            <tr>
                                <td>
									<label class="labelR" for="">Fecha Inicial:</label>
								</td>
                                <td>
                                    <input type="text" name="fechaIni"  value="<?php echo $_POST['fechaIni']?>" onKeyUp="return tabular(event,this)" id="fechaIni" title="DD/MM/YYYY" onDblClick="displayCalendarFor('fechaIni');" class="colordobleclik" autocomplete="off" onChange="" readonly>
                                </td>

                                <td>
									<label class="labelR" for="">Fecha Final:</label>
								</td>
                                <td>
                                    <input type="text" name="fechaFin" value="<?php echo $_POST['fechaFin']?>" onKeyUp="return tabular(event,this)" id="fechaFin" title="DD/MM/YYYY" onDblClick="displayCalendarFor('fechaFin');" class="colordobleclik" autocomplete="off" onChange="" readonly>
                                </td>
								<td colspan="2">
									<div class="checkout-estilos">
										<span>Bienes y servicios diferentes de activos: </span>
										<div class="c9">
											<input type="checkbox"  id="checkbox" v-model="checked" v-on:change="filtrarRubrosConCpc">
											<label for="checkbox" id="t9" ></label>
										</div>
									</div>
								</td>
								<td>
                            		<button type="button" class="botonflechaverde" v-on:click="traeDatosFormulario">Generar</button>
                        		</td>
                            </tr>
                        </table>

						<div class='subpantalla estilos-scroll' style='height:54vh; margin-top:0px; resize: vertical;'>
                            <table class='tabla-ejecucion'>
                                <thead>
                                    <tr>
										<th class="titulosnew00" style="width:3%;">Vig. gasto</th>
										<th class="titulosnew00" style="width:3%;">Dependencia</th>
                                        <th class="titulosnew00" style="width:5%;">Codigo</th>
                                        <th class="titulosnew00">Nombre</th>
										<th class="titulosnew00" style="width:5%;">Fuente</th>
										<th class="titulosnew00" style="width:6%;">Nom. fuente</th>
										<th class="titulosnew00" style="width:3%;">Medio Pago</th>
                                        <th class="titulosnew00" style="width:6%;">Presupuesto Inicial</th>
                                        <th class="titulosnew00" style="width:6%;">Adición</th>
                                        <th class="titulosnew00" style="width:6%;">Reducción</th>
										<th class="titulosnew00" style="width:6%;">Credito</th>
										<th class="titulosnew00" style="width:6%;">Contracredito</th>
										<th class="titulosnew00" style="width:6%;">Definitivo</th>
										<th class="titulosnew00" style="width:3%;">CPC</th>
										<th class="titulosnew00" style="width:6%;">Nom. CPC</th>
										<th class="titulosnew00" style="width:6%;">Disponibilidad</th>
										<th class="titulosnew00" style="width:6%;">Compromisos</th>
										<th class="titulosnew00" style="width:6%;">Obligacion</th>
										<th class="titulosnew00" style="width:6%;">Egresos</th>
										<th class="titulosnew00" style="width:6%;">Saldo</th>
										<th class="titulosnew00" style="width:1%;"></th>
                                    </tr>
                                </thead>
                                <tbody>

                                    <!-- <tr v-for="(det,index) in detalles" v-bind:class="(det[0] == 'A' ? 'saludo2a' : (det[0] == 'C' ? 'saludo3a' : 'saludo1a'))" v-on:dblclick="auxiliarDetalle(det)"> -->
                                    <tr v-for="(det,index) in detalles" :style="esBienesServicios(det[3]) == true ? myStyle : ''" v-bind:class="[det[0] === 'C' ? 'captura' : (det[0] === 'D' ? 'bienesServicios' : 'agregado'), parseInt(det[18]) < 0 ? 'colorFila1' : (parseInt(det[14]) < parseInt(det[15]) ? 'colorFila1' : (parseInt(det[15]) < parseInt(det[16]) ? 'colorFila1' : (parseInt(det[16]) < parseInt(det[17]) ? 'colorFila1' : (index % 2 ? 'contenidonew00' : 'contenidonew01'))))]" v-on:dblclick="auxiliarDetalle(det)">
										<td style="width:3%; text-align:center; ">{{ det[1] }}</td>
										<td style="width:3%; text-align:center; ">{{ det[2] }}</td>
										<td style="width:5%;">{{ det[3] }}</td>
										<td style="text-align:left; ">{{ det[4] }}</td>
										<td style="width:5%; text-align:center; ">{{ det[5] }}</td>
										<td style="width:5%; text-align:left; ">{{ det[19] }}</td>
										<td style="width:3%; text-align:center; ">{{ det[6] }}</td>
										<td style="width:6%; text-align:right; ">{{ formatonumero(det[7]) }}</td>
										<td style="width:6%; text-align:right; ">{{ formatonumero(det[8]) }}</td>
										<td style="width:6%; text-align:right; ">{{ formatonumero(det[9]) }}</td>
										<td style="width:6%; text-align:right; ">{{ formatonumero(det[10]) }}</td>
										<td style="width:6%; text-align:right; ">{{ formatonumero(det[11]) }}</td>
										<td style="width:6%; text-align:right; ">{{ formatonumero(det[12]) }}</td>
										<td style="width:3%; text-align:center; ">{{ det[13] }}</td>
										<td style="width:3%; text-align:left; ">{{ det[20] }}</td>
										<td style="width:6%; text-align:right; ">{{ formatonumero(det[14]) }}</td>
										<td style="width:6%; text-align:right; ">{{ formatonumero(det[15]) }}</td>
										<td style="width:6%; text-align:right; ">{{ formatonumero(det[16]) }}</td>
										<td style="width:6%; text-align:right; ">{{ formatonumero(det[17]) }}</td>
										<td style="width:6%; text-align:right; ">{{ formatonumero(det[18]) }}</td>

										<input type='hidden' name='tipo[]' v-model="det[0]">
										<input type='hidden' name='vigGasto[]' v-model="det[1]">
										<input type='hidden' name='secPresuestal[]' v-model="det[2]">
										<input type='hidden' name='codigo[]' v-model="det[3]">
										<input type='hidden' name='nombre[]' v-model="det[4]">
										<input type='hidden' name='fuente[]' v-model="det[5]">
										<input type='hidden' name='nomFuente[]' v-model="det[19]">
										<input type='hidden' name='medioPago[]' v-model="det[6]">
										<input type='hidden' name='presuInicial[]' v-model="det[7]">
										<input type='hidden' name='adicion[]' v-model="det[8]">
										<input type='hidden' name='reduccion[]' v-model="det[9]">
										<input type='hidden' name='credito[]' v-model="det[10]">
										<input type='hidden' name='contraCredito[]' v-model="det[11]">
										<input type='hidden' name='definitivo[]' v-model="det[12]">
										<input type='hidden' name='cpc[]' v-model="det[13]">
										<input type='hidden' name='nomCpc[]' v-model="det[20]">
										<input type='hidden' name='dosponibilidad[]' v-model="det[14]">
										<input type='hidden' name='compromisos[]' v-model="det[15]">
										<input type='hidden' name='obligacion[]' v-model="det[16]">
										<input type='hidden' name='egresos[]' v-model="det[17]">
										<input type='hidden' name='saldo[]' v-model="det[18]">
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>

					<div id="cargando" v-if="loading" class="loading">
						<span>Cargando...</span>
					</div>
				</article>
			</section>
		</form>

		<script src="https://cdn.jsdelivr.net/npm/vue@2.5.16/dist/vue.js"></script>
        <script src="https://unpkg.com/vue-multiselect@2.1.0"></script>
        <link rel="stylesheet" href="multiselect.css">

		<script src="node_modules/read-excel-file/bundle/read-excel-file.min.js"></script>
		<script src="node_modules/xlsx/dist/xlsx.full.min.js"></script>
		<!-- <script src="Librerias/vue/vue.min.js"></script> -->
		<script src="Librerias/vue/axios.min.js"></script>
		<script type="module" src="presupuesto_ccpet/ejecucionGastos/ccp-ejecuPresuGastos.js?"></script>

	</body>
</html>
