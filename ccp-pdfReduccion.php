<?php
//V 1000 12/12/16  
	require_once("tcpdf/tcpdf_include.php");
	require('comun.inc');
	require "funciones.inc";
	session_start();
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");

	$sqlr = "SELECT id_cargo, id_comprobante FROM pptofirmas WHERE id_comprobante='6' AND vigencia='".$_POST['vigencia']."'";
	$res = mysqli_query($linkbd,$sqlr);
	while($row=mysqli_fetch_assoc($res))
	{
		if($row["id_cargo"] == '0')
		{
			$_POST['ppto'][] = buscatercero($_POST['tercero']);
			$_POST['nomcargo'][]='BENEFICIARIO';
		}
		else
		{
			$sqlr1="SELECT cedulanit,(SELECT nombrecargo FROM planaccargos WHERE codcargo='".$row["id_cargo"]."') FROM planestructura_terceros WHERE codcargo='".$row["id_cargo"]."' AND estado='S'";
			$res1 = mysqli_query($linkbd,$sqlr1);
			$row1 = mysqli_fetch_row($res1);
			$_POST['ppto'][] = buscatercero($row1[0]);
			$_POST['nomcargo'][] = $row1[1];
		}
	}
	//var_dump($_POST['nomcargo']);
	class MYPDF extends TCPDF 
	{
		public function Header() 
		{
			$linkbd = conectar_v7();
			$linkbd -> set_charset("utf8");
			$sqlr="SELECT nit, razonsocial FROM configbasica WHERE estado='S'";
			$resp=mysqli_query($linkbd,$sqlr);
			while($row=mysqli_fetch_row($resp)){$nit=$row[0];$rs=$row[1];}

            //-------------- RECUADRO DEL ENCABEZADO  --------------------------------
			$this->Image('imagenes/escudo.jpg', 22, 12, 25, 23.9, 'JPG', '', 'T', true, 300, '', false, false, 0, false, false, false);// Logo
			$this->SetFont('helvetica','B',8);
			$this->SetY(10);
			$this->RoundedRect(10, 10, 280, 31, 2.5,''); //Borde del encabezado - Recuadro del encabezado
			$this->Cell(50,31,'','R',0,'L'); //Linea que separa el encabazado verticalmente
			$this->SetY(10);
			$this->SetFont('helvetica','B',12);

			if(strlen($rs)<40)
			{
				$this->SetX(58);
				$this->Cell(230,15,$rs,0,0,'C');  //Posicion Municipio
				$this->SetY(16);
			}
			else
			{
				$this->Cell(71);
				$this->MultiCell(200,15,$rs,0,'C',false,1,'','',true,4,false,true,19,'T',false);
				$this->SetY(18);
			}
			
            //--------------------- CONTENIDO DENTRO DEL RECUADRO DEL ENCABEZADO -----------------------------------------

			$this->SetX(58);  //Posicion Numero Nit
			$this->SetFont('helvetica','B',11);
			$this->Cell(230,10,"NIT: $nit",0,0,'C'); // Posicion Numero Nit
			$this->SetY(27);                    // Posicion Certificado
			$this->SetX(60);                    // Posicion Certificado
			$this->Cell(192,14,"CERTIFICADO DE REDUCCIÓN PRESUPUESTAL ",1,0,'C');        // Recuadro del certificado
			$this->SetFont('helvetica','I',10);
			$this->SetY(27);
			$this->SetX(62);
			$this->SetFont('helvetica','B',9);
			$this->SetY(27);
			$this->SetX(253);                                      // Posicion Numero  
			$this->Cell(37,5," Acto Administrativo: ".$_POST['actoAdmId'],'T',0,'L');
			$this->SetY(31);
			$this->SetX(253);                                      // Posicion Fecha     
			$this->Cell(35,6," FECHA: ".$_POST['fecha'],0,0,'L');
			$this->SetY(36);
			$this->SetX(253);                                      //  Posicion Vigencia 
			$this->Cell(35,5," VIGENCIA: ".$_POST['vigencia'],0,0,'L');
		}

		public function Footer() 
		{
			$linkbd = conectar_v7();
			$linkbd -> set_charset("utf8");
			$sqlr="SELECT direccion,telefono,web,email FROM configbasica WHERE estado='S'";
			$resp=mysqli_query($linkbd,$sqlr);
			while($row=mysqli_fetch_row($resp))
			{
				$direcc=strtoupper($row[0]);
				$telefonos=$row[1];
				$dirweb=strtoupper($row[3]);
				$coemail=$row[2];
			}
			if($direcc!=''){$vardirec="Dirección: $direcc, ";}
			else {$vardirec="";}
			if($telefonos!=''){$vartelef="Telefonos: $telefonos";}
			else{$vartelef="";}
			if($dirweb!=''){$varemail="Email: $dirweb, ";}
			else {$varemail="";}
			if($coemail!=''){$varpagiw="Pagina Web: $coemail";}
			else{$varpagiw="";}
			//$this->SetY(-16);
			$this->SetFont('helvetica', 'I', 8);
			$txt = <<<EOD
$vardirec $vartelef
$varemail $varpagiw
EOD;
			$this->Write(0, $txt, '', 0, 'C', true, 0, false, false, 0);
			//$this->SetY(-13);
			$this->Cell(0, 10, 'Page '.$this->getAliasNumPage().'/'.$this->getAliasNbPages(), 0, false, 'C', 0, '', 0, false, 'T', 'M');
			
		}
	}
	
    //----------------------------------------------------------------------

	
    
	$pdf = new MYPDF('L','mm','Letter', true, 'utf8', false);// create new PDF document // Orientacion del pdf
	$pdf->SetDocInfoUnicode (true); 
	// set document information
	$pdf->SetCreator(PDF_CREATOR);
	$pdf->SetAuthor('Ideal10sas');
	$pdf->SetTitle('Certificados');
	$pdf->SetSubject('Certificado de Disponibilidad');
	$pdf->SetKeywords('TCPDF, PDF, example, test, guide');
	$pdf->SetMargins(10, 60, 10);// set margins // Posicion de los valores de cada columna
	$pdf->SetHeaderMargin(90);// set margins
	$pdf->SetFooterMargin(20);// set margins
	$pdf->SetAutoPageBreak(TRUE, 20);// set auto page breaks
	// set some language-dependent strings (optional)
	if (@file_exists(dirname(__FILE__).'/lang/spa.php')) 
	{
		require_once(dirname(__FILE__).'/lang/spa.php');
		$pdf->setLanguageArray($l);
	}

	// ---------------------------------------------------------
	$pdf->AddPage();
	$pdf->SetFont('helvetica','I',9);
	
	$crit1=" ";
	$crit2=" ";
	$crit3=" ";
	
    $pdf->SetY(44);
    $pdf->SetFont('times','B',10);
	$pdf->Cell(41,5,"ESTADO REDUCCIÓN:",0,0,'L');
	$pdf->SetFont('times','',10);
	$pdf->Cell(10,5,$_POST['estado'],0,1,'C',false,0,0,false,'T','C');

	// $pdf->SetFont('times','B',12);
	// $pdf->SetY(44);
	// $pdf->MultiCell(280,5,'ESTADO ADICIÓN:','','L');
	// $pdf->SetFont('times','B',12);
    // $pdf->Cell(100,3,"PRUEBA",0,1,'C',false,0,0,false,'T','L');
	$pdf->ln(4);
	// $pdf->SetFont('times','B',10);
	// $pdf->Cell(41,5,"SOLICITANTE:",0,0,'L');
	// $pdf->SetFont('times','',10);
	// $pdf->Cell(25,5,$_POST['solicita'],0,1,'C',false,0,0,false,'T','C');
	// $pdf->SetFont('times','B',10);
	// $pdf->Cell(17,5,"OBJETO:",0,0,'L');
	// $pdf->SetFont('times','',10);
	// MultiCell($w, $h, $txt, $border=0, $align='J', $fill=0, $ln=1, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=0)
	// $pdf->MultiCell(250,16.6,$_POST['objeto'],0,'L',false,1,40,'',true,0,false,true,19,'T',false);
	/* $pdf->SetFont('helvetica','B',7);
	$pdf->MultiCell(169,4,$_POST['meta'].' '.$_POST['nmeta'],0,'L',false,1,'','',true,0,false,true,0,'T',false);
	$pdf->ln(2); */

	$pdf->SetFont('helvetica','B',6);
	$posy=$pdf->GetY();
	$pdf->SetY($posy+1);

	$pdf->Cell(15,5,'Tipo cuenta',1,0,'C',false,'T','C');
    $pdf->Cell(20,5,'Tipo de gasto',1,0,'C',false,0,0,false,'T','C');
    $pdf->Cell(20,5,'Sec. presupuestal',1,0,'C',false,0,0,false,'T','C');
    $pdf->Cell(16,5,'Medio de pago',1,0,'C',false,0,0,false,'T','C');
    $pdf->Cell(20,5,'Vig. del gasto',1,0,'C',false,0,0,false,'T','C');
    $pdf->Cell(30,5,'Proyecto',1,0,'C',false,0,0,false,'T','C');
    $pdf->Cell(30,5,'Programático',1,0,'C',false,0,0,false,'T','C');
    $pdf->Cell(30,5,'Cuenta',1,0,'C',false,0,0,false,'T','C');
    $pdf->Cell(30,5,'Fuente',1,0,'C',false,0,0,false,'T','C');
    $pdf->Cell(30,5,'Valor ingresos',1,0,'C',false,0,0,false,'T','C');
    $pdf->Cell(30,5,'Valor gastos',1,1,'C',false,0,0,false,'T','C');
	$con=0;

	while ($con<count($_POST['tipoCuenta']))
	{
		$altura=6;
		$altini=6;
		$ancini=20;
		$altaux=0;
		$colst01=strlen($_POST['cuenta'][$con]);
		$colst02=strlen($_POST['fuente'][$con]);

		if($colst01>$colst02){$cantidad_lineas= $colst01;}
		else{$cantidad_lineas= $colst02;}
		if($cantidad_lineas > $ancini)
		{
			$cant_espacios = $cantidad_lineas/$ancini;
			$rendondear=ceil($cant_espacios);
			$altaux=$altini*$rendondear;
		}
		if($altaux>$altura){$altura=$altaux;}
		if ($concolor==0){$pdf->SetFillColor(200,200,200);$concolor=1;}
		else {$pdf->SetFillColor(255,255,255);$concolor=0;}
		$pdf->SetFont('times','',9);
		
		$v=$pdf->gety();
		if($v>=155){ 
			$pdf->AddPage();
		}

		$pdf->MultiCell(15,$altura,$_POST['tipoCuenta'][$con],1,'L',true,0,'','',true,0,false,true,$altura,'M',false);
		$pdf->MultiCell(20,$altura,$_POST['tipoGasto'][$con],1,'L',true,0,'','',true,0,false,true,$altura,'M',false);
		$pdf->MultiCell(20,$altura,$_POST['secPresupuestal'][$con],1,'L',true,0,'','',true,0,false,true,$altura,'M',false);
		$pdf->MultiCell(16,$altura,$_POST['medioPago'][$con],1,'L',true,0,'','',true,0,false,true,$altura,'M',false);
		$pdf->MultiCell(20,$altura,$_POST['vigGasto'][$con],1,'L',true,0,'','',true,0,false,true,$altura,'M',false);
		$pdf->MultiCell(30,$altura,$_POST['proyecto'][$con],1,'L',true,0,'','',true,0,false,true,$altura,'M',false);
		$pdf->MultiCell(30,$altura,$_POST['programatico'][$con],1,'L',true,0,'','',true,0,false,true,$altura,'M',false);
		$pdf->MultiCell(30,$altura,$_POST['cuenta'][$con],1,'L',true,0,'','',true,0,false,true,$altura,'M',false);
		$pdf->MultiCell(30,$altura,$_POST['fuente'][$con],1,'L',true,0,'','',true,0,false,true,$altura,'M',false);
		$pdf->MultiCell(30,$altura,"$ ".number_format($_POST['valorIngreso'][$con],2,$_SESSION["spdecimal"],$_SESSION["spmillares"])."   ",1,'L',true,0,'','',true,0,false,true,$altura,'M',false);
		$pdf->Cell(30,$altura,"$ ".number_format($_POST['valorGasto'][$con],2,$_SESSION["spdecimal"],$_SESSION["spmillares"])."   ",1,1,'R',true,0,0,false,'T','C');

		$con++;
	}

    //-------------------  Nombre/ Cargo / Valor total ----------------
	$sql="SELECT user FROM pptocdp WHERE consvigencia='".$_POST['numero']."' AND vigencia='".$_POST['vigencia']."' ";
	$res=mysqli_query($linkbd,$sql);
	$row = mysqli_fetch_row($res);
	$pdf->Cell(211,6,'TOTAL:',0,0,'R',false,0,0,false,'T','C');
	$pdf->setFont('times','B',9);
	$pdf->Cell(30,6,"$ ".number_format($_POST['totalGasto'],2,$_SESSION["spdecimal"],$_SESSION["spmillares"])."  ",1,0,'R',false,0,0,false,'T','C');
    $pdf->Cell(30,6,"$ ".number_format($_POST['totalIngreso'],2,$_SESSION["spdecimal"],$_SESSION["spmillares"])."  ",1,1,'R',false,0,0,false,'T','C');
	$pdf->ln(8);
	$v=$pdf->gety();
	if($v>=180){ 
		$pdf->AddPage();
		$v=$pdf->gety();
	}
	$pdf->MultiCell(280,8,'SON: '.strtoupper($_POST['letras']." M/CTE"),1,'L',false,1,'','',true,0,false,true,8,'M',false); // Valor letra
	
	for($x=0;$x<count($_POST['ppto']);$x++)
	{
		$pdf->ln(20);
		$v=$pdf->gety();
		if($v>=180){
			$pdf->AddPage();
			$pdf->ln(20);
			$v=$pdf->gety();
		}
		$pdf->setFont('times','B',8);
		if (($x%2)==0) {
			if(isset($_POST['ppto'][$x+1])){
				$pdf->Line(17,$v,107,$v);
				$pdf->Line(112,$v,202,$v);
				$v2=$pdf->gety();
				$pdf->Cell(104,4,''.$_POST['ppto'][$x],0,1,'C',false,0,0,false,'T','C');
				$pdf->Cell(200,4,''.$_POST['nomcargo'][$x],0,1,'C',false,0,0,false,'T','C');
				$pdf->SetY($v2);
				$pdf->Cell(295,4,''.$_POST['ppto'][$x+1],0,1,'C',false,0,0,false,'T','C');
				$pdf->Cell(200,4,''.$_POST['nomcargo'][$x+1],0,1,'C',false,0,0,false,'T','C');
			}else{
				$pdf->Line(100,$v,200,$v);
				$pdf->Cell(280,4,''.$_POST['ppto'][$x],0,1,'C',false,0,0,false,'T','C');
				$pdf->Cell(280,4,''.$_POST['nomcargo'][$x],0,0,'C',false,0,0,false,'T','C');
			}
			$v3=$pdf->gety();
		}
		$pdf->SetY($v3);
		$pdf->SetFont('helvetica','',7);
	}
	// ---------------------------------------------------------
	$pdf->Output('reporteAdicion.pdf', 'I');//Close and output PDF document
?>
