<?php

    header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require 'comun.inc';
	require 'funciones.inc';
	session_start();
    if(empty($_SESSION)){
        header("location: index.php");
    }
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html lang=es>
	<head>
    <meta charset="utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1"/>
		<meta name="viewport" content="user-scalable=no">
		<title>:: IDEAL 10 - Servicios publicos</title>
		<link href="favicon.ico" rel="shortcut icon"/>
		<link href="css/css2z.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
        <link href="css/tabs.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
        <link href="css/style.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" href="sweetalert2/dist/sweetalert2.min.css">
        <script type="text/javascript" src="css/programas.js"></script>
        <script src="sweetalert2/dist/sweetalert2.min.js"></script>
		<script src="vue/vue.min.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script src="css/calendario.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
	</head>
	<body>
		<header>
			<table>
				<tr><script>barra_imagenes("serv");</script><?php cuadro_titulos();?></tr>
			</table>
		</header>

        <section id="myapp" v-cloak>
            <div class="loading-container" v-show="isLoading" >
                <p class="text-loading"data-loading="IDEAL 10">IDEAL 10</p>
            </div>
            <nav>
                <table>
                    <tr><?php menu_desplegable("serv");?></tr>
                </table>
                <div class="bg-white group-btn p-1" id="newNavStyle">
                    <button type="button" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center" @click="mypop=window.open('serv-principal.php','',''); mypop.focus();">
                        <span>Nueva ventana</span>
                        <svg class="fill-black group-hover:fill-white w-5 h-5" viewBox="0 -960 960 960" ><path d="M320-120v-80h80v-80H160q-33 0-56.5-23.5T80-360v-400q0-33 23.5-56.5T160-840h640q33 0 56.5 23.5T880-760v400q0 33-23.5 56.5T800-280H560v80h80v80H320ZM160-360h640v-400H160v400Zm0 0v-400 400Z"/></svg>
                    </button>
                    <button type="button" class="btn btn-success d-flex justify-between align-items-center" @click="location.href='serv-menuFacturacionNew'">
                        <span>Atrás</span>
                        <svg viewBox="0 -960 960 960"><path d="M760-200v-160q0-50-35-85t-85-35H273l144 144-57 56-240-240 240-240 57 56-144 144h367q83 0 141.5 58.5T840-360v160h-80Z"/></svg>
                    </button>
                </div>
            </nav>
            <article>
                <div class="bg-white">
                    <div>
						<h2 class="titulos m-0">Imprimir facturas</h2>
						<p class="m-0 ms-2">Todos los campos con (<span class="text-danger fw-bolder">*</span>) son obligatorios</p>

                        <div class="d-flex">
                            <div class="form-control w-25">
                                <label class="form-label m-0" for="">Periodo liquidado<span class="text-danger fw-bolder">*</span>:</label>
                                <input type="text" v-model="periodo" @dblclick="modalPeriodo=true" class="colordobleclik">
                            </div>

                            <div class="form-control w-15">
                                <label class="form-label m-0" for="">Ruta<span class="text-danger fw-bolder">*</span>:</label>
                                <select v-model="rutaId">
									<option value="0">Todas las rutas</option>
                                    <option v-for="(r,index) in rutas" :key="index" :value="r.id">{{r.id}} - {{r.nombre}}</option>
								</select>
                            </div>
                            
							<div class="form-control justify-between w-15">
                                <label for=""></label>
                                <button type="button" class="btn btn-primary" @click="searchInfo()">Generar información</button>
                            </div>
							
                            <div class="form-control justify-between w-15">
                                <label for=""></label>
                                <button type="button" class="btn btn-primary" @click="pdf()">Generar PDF</button>
                            </div>
                        </div>
                        <!-- <h2 class="titulos m-0">Reporte seguimiento recaudos:</h2> -->
                    </div>

                    <div class="table-responsive">
                        <table class="table table-hover fw-normal w-50">
                            <thead>
                                <tr class="text-center">
                                    <th>Item</th>
                                    <th>Codigo usuario</th>
                                    <th>Número de factura</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr v-for="(d,index) in data" :key="index" class="text-center">
                                    <td>{{index+1}}</td>
                                    <td>{{d.codigoUsuario}}</td>
                                    <td>{{d.numeroFactura}}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>

                <!-- modales -->
                <div v-show="modalPeriodo" class="modal">
                    <div class="modal-dialog modal-lg" >
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title">Listado de periodos facturados</h5>
                                <button type="button" @click="modalPeriodo=false;" class="btn btn-close"><div></div><div></div></button>
                            </div>
                            <div class="modal-body">
                                <div class="d-flex flex-column">
                                    <div class="form-control m-0 mb-3">
                                        <input type="search" v-model="txtSearch" @keyup="searchData('modalPeriodo')" placeholder="Buscar por código, mes o año">
                                    </div>
                                    <div class="form-control m-0 p-2">
                                        <label class="form-label" for="labelInputName">Resultados: <span class="fw-bold">{{txtResultados}}</span></label>
                                    </div>
                                </div>
                                <div class="overflow-auto max-vh-50 overflow-x-hidden" >
                                    <table class="table table-hover fw-normal">
                                        <thead>
                                            <tr class="text-center">
                                                <th>Id periodo</th>
                                                <th>Periodo</th>
                                                <th>Fecha inicial</th>
                                                <th>Fecha final</th>
                                                <th>Fecha impresión</th>
                                                <th>Fecha limite pago</th>
                                                <th>Estado</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr v-for="(p,index) in periodos_copy" :key="index" @click="selectItemModal(p, 'periodoModel')" class="text-center">
                                                <td>{{p.numero_corte}}</td>
                                                <td class="text-left">{{p.periodoName}}</td>
                                                <td>{{formatFecha(p.fecha_inicial)}}</td>
                                                <td>{{formatFecha(p.fecha_final)}}</td>
                                                <td>{{formatFecha(p.fecha_impresion)}}</td>
                                                <td>{{formatFecha(p.fecha_limite_pago)}}</td>
                                                <td>
                                                    <span :class="p.estado == 'S' ? 'badge-success' : 'badge-danger'" class="badge">
                                                        {{ (p.estado == 'S') ? "Activo" : "Cerrado" }}
                                                    </span>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </article>
        </section>

		<script src="node_modules/read-excel-file/bundle/read-excel-file.min.js"></script>
		<script src="node_modules/xlsx/dist/xlsx.full.min.js"></script>
        <script src="Librerias/vue/vue.min.js"></script>
		<script src="Librerias/vue/axios.min.js"></script>
		<script type="module" src="servicios_publicos/facturas/js/imprimirFacturas_functions.js"></script>

	</body>
</html>
