<?php
header("Cache-control: no-cache, no-store, must-revalidate");
header("Content-Type: text/html;charset=utf8");
require "comun.inc";
require "funciones.inc";
require "conversor.php";
sesion();
$linkbd = conectar_v7();
$linkbd->set_charset("utf8");
cargarcodigopag($_GET['codpag'], $_SESSION["nivel"]);
date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE>
<html lang="es">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1">
		<meta name="viewport" content="user-scalable=no">
		<title>:: IDEAL 10 - Tesorer&iacute;a</title>
		<link href="favicon.ico" rel="shortcut icon" />
		<link href="css/css2z.css?<?php echo date('d_m_Y_h_i_s'); ?>" rel="stylesheet" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s'); ?>" rel="stylesheet" />
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s'); ?>" rel="stylesheet" />
		<link href="css/style.css?<?php echo date('d_m_Y_h_i_s'); ?>" rel="stylesheet" />
		<link href="sweetalert2/dist/sweetalert2.min.css" rel="stylesheet">
		<script src="sweetalert2/dist/sweetalert2.min.js"></script>
		<script type="text/javascript" src="css/calendario.js?<?php echo date('d_m_Y_h_i_s'); ?>"></script>
		<script type="text/javascript" src="css/programas.js?<?php echo date('d_m_Y_h_i_s'); ?>"></script>
		<script>
			function despliegamodal2(_valor,_num)
				{
					document.getElementById("bgventanamodal2").style.visibility = _valor;
					if(_valor == "hidden"){
						document.getElementById('ventana2').src = "";
					} else {
						switch(_num){
							case '1':	document.getElementById('ventana2').src = "trasladosSSFactivos-ventana01.php";break;
						}
					}
				}
			function buscacta(e) {
				if (document.form2.cuenta.value != "") {
					document.form2.bc.value = '1';
					document.form2.submit();
				}
			}
			function validar() {
				document.form2.submit();
			}
			function buscater(e) {
				if (document.form2.tercero.value != "") {
					document.form2.bt.value = '1';
					document.form2.submit();
				}
			}
			function agregardetalle() {
				if (document.form2.numero.value != "" && document.form2.valor.value > 0 && document.form2.banco.value != "" && document.form2.banco2.value != "") {
					Swal.fire({
						icon: 'question',
						title: '¿Seguro que desea agregar este traslado?',
						showDenyButton: true,
						confirmButtonText: 'Agregar',
						confirmButtonColor: '#01CC42',
						denyButtonText: 'Cancelar',
						denyButtonColor: '#FF121A',
					}).then(
						(result) => {
							if (result.isConfirmed) {
								document.form2.agregadet.value = 1;
								document.form2.submit();
							} else if (result.isDenied) {
								Swal.fire({
									icon: 'info',
									title: 'No se agrego el traslado',
									confirmButtonText: 'Continuar',
									confirmButtonColor: '#FF121A',
									timer: 2500
								});
							}
						}
					)
				} else {
					Swal.fire({
						icon: 'error',
						title: 'Error!',
						text: 'Falta informacion para poder Agregar',
						confirmButtonText: 'Continuar',
						confirmButtonColor: '#FF121A',
						timer: 2500
					});
				}
			}
			function eliminar(variable) {
				Swal.fire({
					icon: 'question',
					title: '¿Esta Seguro de Eliminar?',
					showDenyButton: true,
					confirmButtonText: 'Eliminar',
					confirmButtonColor: '#01CC42',
					denyButtonText: 'Cancelar',
					denyButtonColor: '#FF121A',
				}).then(
					(result) => {
						if (result.isConfirmed) {
							document.form2.elimina.value = variable;
							document.form2.oculto.value = 9;
							document.form2.submit();
						} else if (result.isDenied) {
							Swal.fire({
								icon: 'info',
								title: 'No se elimino',
								confirmButtonText: 'Continuar',
								confirmButtonColor: '#FF121A',
								timer: 2500
							});
						}
					}
				)
			}
			function guardar() {
				var ntraslados = document.getElementsByName('dbancos[]').length;
				if ((document.form2.fecha.value != '') && (ntraslados > 0)) {
					Swal.fire({
						icon: 'question',
						title: '¿Esta seguro de guardar?',
						showDenyButton: true,
						confirmButtonText: 'Guardar',
						confirmButtonColor: '#01CC42',
						denyButtonText: 'Cancelar',
						denyButtonColor: '#FF121A',
					}).then(
						(result) => {
							if (result.isConfirmed) {
								document.form2.oculto.value = 2;
								document.form2.submit();
							} else if (result.isDenied) {
								Swal.fire({
									icon: 'info',
									title: 'No se guardo',
									confirmButtonText: 'Continuar',
									confirmButtonColor: '#FF121A',
									timer: 2500
								});
							}
						}
					)
				} else {
					Swal.fire({
						icon: 'error',
						title: 'Error!',
						text: 'Faltan datos para completar el registro',
						confirmButtonText: 'Continuar',
						confirmButtonColor: '#FF121A',
						timer: 2500
					});
					document.form2.fecha.focus();
					document.form2.fecha.select();
				}
			}
			function pdf() {
				document.form2.action = "teso-pdftraslados.php";
				document.form2.target = "_BLANK";
				document.form2.submit();
				document.form2.action = "";
				document.form2.target = "";
			}
		</script>
	</head>

	<body>
		<IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
		<span id="todastablas2"></span>
		<table>
			<tr>
				<script>barra_imagenes("teso");</script><?php cuadro_titulos(); ?>
			</tr>
			<tr><?php menu_desplegable("teso"); ?></tr>
		</table>
		<div class="bg-white group-btn p-1"><button type="button" onclick="location.href='teso-traslados.php'"
				class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
				<span>Nuevo</span>
				<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960">
					<path d="M440-440H200v-80h240v-240h80v240h240v80H520v240h-80v-240Z"></path>
				</svg>
			</button><button type="button" onclick="guardar()"
				class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
				<span>Guardar</span>
				<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960">
					<path
						d="M840-680v480q0 33-23.5 56.5T760-120H200q-33 0-56.5-23.5T120-200v-560q0-33 23.5-56.5T200-840h480l160 160Zm-80 34L646-760H200v560h560v-446ZM480-240q50 0 85-35t35-85q0-50-35-85t-85-35q-50 0-85 35t-35 85q0 50 35 85t85 35ZM240-560h360v-160H240v160Zm-40-86v446-560 114Z">
					</path>
				</svg>
			</button><button type="button" onclick="location.href='teso-buscatraslados.php'"
				class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
				<span>Buscar</span>
				<svg class="fill-black group-hover:fill-white w-5 h-5" xmlns="http://www.w3.org/2000/svg"
					viewBox="0 -960 960 960">
					<path
						d="M784-120 532-372q-30 24-69 38t-83 14q-109 0-184.5-75.5T120-580q0-109 75.5-184.5T380-840q109 0 184.5 75.5T640-580q0 44-14 83t-38 69l252 252-56 56ZM380-400q75 0 127.5-52.5T560-580q0-75-52.5-127.5T380-760q-75 0-127.5 52.5T200-580q0 75 52.5 127.5T380-400Z">
					</path>
				</svg>
			</button><button type="button" onclick="mypop=window.open('plan-agenda.php','','');mypop.focus()"
				class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
				<span class="group-hover:text-white">Agenda</span>
				<svg class="fill-black group-hover:fill-white w-5 h-5" xmlns="http://www.w3.org/2000/svg"
					viewBox="0 -960 960 960">
					<path
						d="M560-564v-68q33-14 67.5-21t72.5-7q26 0 51 4t49 10v64q-24-9-48.5-13.5T700-600q-38 0-73 9.5T560-564Zm0 220v-68q33-14 67.5-21t72.5-7q26 0 51 4t49 10v64q-24-9-48.5-13.5T700-380q-38 0-73 9t-67 27Zm0-110v-68q33-14 67.5-21t72.5-7q26 0 51 4t49 10v64q-24-9-48.5-13.5T700-490q-38 0-73 9.5T560-454ZM260-320q47 0 91.5 10.5T440-278v-394q-41-24-87-36t-93-12q-36 0-71.5 7T120-692v396q35-12 69.5-18t70.5-6Zm260 42q44-21 88.5-31.5T700-320q36 0 70.5 6t69.5 18v-396q-33-14-68.5-21t-71.5-7q-47 0-93 12t-87 36v394Zm-40 118q-48-38-104-59t-116-21q-42 0-82.5 11T100-198q-21 11-40.5-1T40-234v-482q0-11 5.5-21T62-752q46-24 96-36t102-12q58 0 113.5 15T480-740q51-30 106.5-45T700-800q52 0 102 12t96 36q11 5 16.5 15t5.5 21v482q0 23-19.5 35t-40.5 1q-37-20-77.5-31T700-240q-60 0-116 21t-104 59ZM280-494Z">
					</path>
				</svg>
			</button><button type="button" onclick="mypop=window.open('teso-principal.php','','');mypop.focus();"
				class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
				<span>Nueva ventana</span>
				<svg class="fill-black group-hover:fill-white w-5 h-5" xmlns="http://www.w3.org/2000/svg"
					viewBox="0 -960 960 960">
					<path
						d="M320-120v-80h80v-80H160q-33 0-56.5-23.5T80-360v-400q0-33 23.5-56.5T160-840h640q33 0 56.5 23.5T880-760v400q0 33-23.5 56.5T800-280H560v80h80v80H320ZM160-360h640v-400H160v400Zm0 0v-400 400Z">
					</path>
				</svg>
			</button><button type="button"
				onclick="mypop=window.open('/financiero/teso-traslados.php','','');mypop.focus();"
				class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
				<span class="group-hover:text-white">Duplicar pantalla</span>
				<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960">
					<path
						d="M640-160v-360H160v360h480Zm80-200v-80h80v-360H320v200h-80v-200q0-33 23.5-56.5T320-880h480q33 0 56.5 23.5T880-800v360q0 33-23.5 56.5T800-360h-80ZM160-80q-33 0-56.5-23.5T80-160v-360q0-33 23.5-56.5T160-600h480q33 0 56.5 23.5T720-520v360q0 33-23.5 56.5T640-80H160Zm400-603ZM400-340Z">
					</path>
				</svg>
			</button></div>
		<div id="bgventanamodalm" class="bgventanamodalm">
			<div id="ventanamodalm" class="ventanamodalm">
				<IFRAME src="" name="ventanam" marginWidth=0 marginHeight=0 frameBorder=0 id="ventanam" frameSpacing=0
					style=" width:700px; height:130px; top:200; overflow:hidden;"></IFRAME>
			</div>
		</div>
		<?php
		if (!$_POST['oculto']) {
			$_POST['tipotras'] = 1;
			$fec = date("d/m/Y");
			$sqlr = "SELECT cuentatraslado FROM tesoparametros WHERE estado = 'S'";
			$res = mysqli_query($linkbd, $sqlr);
			while ($row = mysqli_fetch_row($res)) {
				$_POST['cuentatraslado'] = $row[0];
			}
			$_POST['fecha'] = $fec;
			$_POST['valor'] = 0;
			$_POST['idcomp'] = selconsecutivo('tesotraslados_cab', 'id_consignacion');
			$sqlr = "SELECT MAX(id_consignacion) FROM tesotraslados_cab";
			$res = mysqli_query($linkbd, $sqlr);
			$sqlr = "SELECT codigo FROM redglobal WHERE tipo = 'IN'";
			$res = mysqli_query($linkbd, $sqlr);
			$row = mysqli_fetch_row($res);
			$_POST['dorigen'] = $row[0];
			$_POST['tipo_documento'] = '201';
			$_POST['tipopago'] = 'CSF';
			$_POST['corresponder'] = 'N';
		}
		preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST['fecha'], $fecha);
		$_POST['vigencia'] = $fecha[3];
		?>
		<form name="form2" method="post" action="">
			<table class="inicio ancho">
				<tr>
					<td class="titulos" colspan="7">.: Agregar Traslados</td>
					<td>
						<select name="tipo_documento" id="tipo_documento" onKeyUp="return tabular(event,this)"
							onChange="ir_reversar()" style="width:100%;">
							<option value="1" <?php if ($_POST['tipo_documento'] == '201')
								echo "SELECTED"; ?>>201 - CREAR
								DOCUMENTO</option>
							<option value="2" <?php if ($_POST['tipo_documento'] == '401')
								echo "SELECTED"; ?>>401 - REVERSAR
								DOCUMENTO</option>
						</select>
					</td>
					<td class="cerrar" style="width:7%" onClick="location.href='teso-principal.php'">Cerrar</td>
				</tr>
				<tr>
					<td class="textonew01" style="width:3cm;">N&uacute;mero Comp:</td>
					<td style="width:20%"><input type="text" name="idcomp" value="<?php echo $_POST['idcomp'] ?>"
							style="width:98%;" readonly></td>
					<input type="hidden" name="cuentatraslado" value="<?php echo $_POST['cuentatraslado'] ?>">
					<td class="textonew01" style="width:3cm;">Fecha:</td>
					<td style="width:12%"><input type="text" id="fc_1198971545" title="DD/MM/YYYY" name="fecha"
							value="<?php echo $_POST['fecha'] ?>" maxlength="10"
							onKeyPress="javascript:return solonumeros(event)" onKeyUp="return tabular(event,this)"
							onKeyDown="mascara(this,'/',patron,true)" style="width:98%;"
							onDblClick="displayCalendarFor('fc_1198971545');" class="colordobleclik" autocomplete="off"
							onChange="" readonly></td>
					<td class="textonew01" style='width:3cm;'>Tipo Traslado:</td>
					<td style="width:12%">
						<select name="tipotras" id="tipotras" onKeyUp="return tabular(event,this)" onChange="validar()"
							style="width:98%;">
							<option value="1" <?php if ($_POST['tipotras'] == '1')
								echo "SELECTED"; ?>>Interno</option>
							<option value="2" <?php if ($_POST['tipotras'] == '2')
								echo "SELECTED"; ?>>Externo</option>
						</select>
					</td>
					<?php
					if ($_POST['tipotras'] == 2) {
						echo "
								<td class='textonew01' style='width:2.5cm;'>Destino:</td>
								<td>
									<select name='tipotrasext' id='tipotrasext' onKeyUp=\"return tabular(event,this)\" onChange=\"validar()\" style='width:100%;'>
									<option value=''>Seleccione....</option>";
						$sqlr = "SELECT codigo, base, nombre, usuario FROM redglobal WHERE tipo = 'EX' ORDER BY id";
						$res = mysqli_query($linkbd, $sqlr);
						while ($row = mysqli_fetch_row($res)) {
							if ($_POST['tipotrasext'] == $row[0]) {
								echo "<option value='$row[0]' SELECTED> $row[0] - $row[2]</option>";
								$_POST['baseext'] = $row[1];
								$_POST['ndestino'] = $row[2];
								$_POST['usuariobase'] = $row[3];
							} else {
								echo "<option value='$row[0]'> $row[0] - $row[2]</option>";
							}
						}
						echo "</select>
								</td>
								<input type='hidden' name='ndestino' id='ndestino' value='" . $_POST['ndestino'] . "'>
								<input type='hidden' name='baseext' id='baseext' value='" . $_POST['baseext'] . "'>
								<input type='hidden' name='usuariobase' id='usuariobase' value='" . $_POST['usuariobase'] . "'>";
					} else {
						echo "
								<td style='width:2.5cm;'></td>
								<td></td>";
					}
					?>
				</tr>
				<tr>
					<td class="textonew01">N&uacute;mero Transacci&oacute;n:</td>
					<td><input type="text" name="numero" value="<?php echo $_POST['numero'] ?>"
							onKeyUp="return tabular(event,this)" style="width:98%;"></td>
					<td class="textonew01">Concepto Traslado:</td>
					<td colspan="5"><input type="text" name="concepto" value="<?php echo $_POST['concepto'] ?>"
							onKeyUp="return tabular(event,this)" style="width:100%;"></td>
					<td></td>
				</tr>
				<tr>
					<td class="textonew01">CC Origen :</td>
					<td>
						<select name="cc" onChange="validar()" onKeyUp="return tabular(event,this)" style="width:98%;">
							<option value="">Seleccione....</option>
							<?php
							$sqlr = "SELECT * FROM centrocosto WHERE estado='S'";
							$res = mysqli_query($linkbd, $sqlr);
							while ($row = mysqli_fetch_row($res)) {
								if ($_POST['cc'] == $row[0]) {
									echo "<option value='$row[0]' SELECTED> $row[0] - $row[1]</option>";
									$_POST['ccnombre'] = $row[1];
								} else {
									echo "<option value='$row[0]'> $row[0] - $row[1]</option>";
								}
							}
							?>
						</select>
					</td>
					<input type="hidden" id="ccnombre" name="ccnombre" value="<?php echo $_POST['ccnombre'] ?>">
					<td class="textonew01">Cuenta Origen:</td>
					<td colspan="2">
						<select id="banco" name="banco" onChange="validar()" onKeyUp="return tabular(event,this)"
							style="width:98%;">
							<option value="">Seleccione....</option>
							<?php
							$sqlr = "SELECT T1.estado, T1.cuenta, T1.ncuentaban, T1.tipo, T2.razonsocial, T1.tercero FROM tesobancosctas AS T1, terceros AS T2, cuentasnicsp AS T3 WHERE T1.tercero = T2.cedulanit AND T1.estado = 'S' AND T3.cuenta = T1.cuenta";
							$res = mysqli_query($linkbd, $sqlr);
							while ($row = mysqli_fetch_row($res)) {
								if ($_POST['banco'] == $row[1]) {
									echo "<option value='$row[1]' SELECTED>$row[2] - Cuenta $row[3]</option>";
									$_POST['nbanco'] = $row[4];
									$_POST['ter'] = $row[5];
									$_POST['cb'] = $row[2];
								} else {
									echo "<option value='$row[1]'>$row[2] - Cuenta $row[3]</option>";
								}
							}
							?>
						</select>
					</td>
					<td colspan="3"><input type="text" id="nbanco" name="nbanco" value="<?php echo $_POST['nbanco'] ?>"
							style="width:100%;" readonly></td>
					<input type="hidden" id="cb" name="cb" value="<?php echo $_POST['cb'] ?>">
					<input type="hidden" id="ter" name="ter" value="<?php echo $_POST['ter'] ?>">
				</tr>
				<tr>
					<td class="textonew01">CC Destino:</td>
					<td>
						<select name='cc2' onChange="validar()" onKeyUp="return tabular(event,this)" style='width:98%;'>
							<option value="">Seleccione....</option>
							<?php
							if ($_POST['tipotras'] == 1) {
								$sqlr = "SELECT * FROM centrocosto WHERE estado = 'S'";
								$res = mysqli_query($linkbd, $sqlr);
								while ($row = mysqli_fetch_row($res)) {
									if ($_POST['cc2'] == $row[0]) {
										echo "<option value='$row[0]' SELECTED>$row[0] - $row[1]</option>";
										$_POST['ccnombre2'] = $row[1];
									} else {
										echo "<option value='$row[0]'>$row[0] - $row[1]</option>";
									}
								}
							} else {
								$linkmulti = conectar_Multi($_POST['baseext'], $_POST['usuariobase']);
								$linkmulti->set_charset("utf8");
								$sqlr = "SELECT * FROM centrocosto WHERE estado = 'S'";
								$res = mysqli_query($linkmulti, $sqlr);
								while ($row = mysqli_fetch_row($res)) {
									if ($_POST['cc2'] == $row[0]) {
										echo "<option value='$row[0]' SELECTED>$row[0] - $row[1]</option>";
										$_POST['ccnombre2'] = $row[1];
									} else {
										echo "<option value='$row[0]'>$row[0] - $row[1]</option>";
									}
								}
							}
							?>
						</select>
					</td>
					<input type="hidden" id="ccnombre2" name="ccnombre2" value="<?php echo $_POST['ccnombre2'] ?>">
					<td class="textonew01">Cuenta Destino:</td>
					<td colspan="2">
						<select id="banco2" name="banco2" onChange="validar()" onKeyUp="return tabular(event,this)"
							style="width:98%;">
							<option value="">Seleccione....</option>
							<?php
							if ($_POST['tipotras'] == 1) {
								$sqlr = "
										SELECT T1.estado, T1.cuenta, T1.ncuentaban, T1.tipo, T2.razonsocial, T1.tercero
										FROM tesobancosctas AS T1
										INNER JOIN terceros AS T2
										ON T1.tercero = T2.cedulanit
										INNER JOIN cuentasnicsp AS T3
										ON T3.cuenta = T1.cuenta
										WHERE T1.estado = 'S' ";
								$res = mysqli_query($linkbd, $sqlr);
								while ($row = mysqli_fetch_row($res)) {
									if ($_POST['banco2'] == $row[1]) {
										if ($_POST['cc'] == $_POST['cc2']) {
											if ($_POST['banco'] != $row[1]) {
												echo "<option value='$row[1]' SELECTED>$row[2] - Cuenta $row[3]</option>";
												$_POST['nbanco2'] = $row[4];
												$_POST['cb2'] = $row[2];
												$_POST['ter2'] = $row[5];
											} else {
												$_POST['nbanco2'] = '';
												$_POST['cb2'] = '';
												$_POST['ter2'] = '';
											}
										} else {
											echo "<option value='$row[1]' SELECTED>$row[2] - Cuenta $row[3]</option>";
											$_POST['nbanco2'] = $row[4];
											$_POST['cb2'] = $row[2];
											$_POST['ter2'] = $row[5];
										}

									} elseif ($_POST['cc'] == $_POST['cc2']) {
										if ($_POST['banco'] != $row[1]) {
											echo "<option value='$row[1]'>$row[2] - Cuenta $row[3]</option>";
										}
									} else {
										echo "<option value='$row[1]'>$row[2] - Cuenta $row[3]</option>";
									}
								}
							} else {
								$linkmulti = conectar_Multi($_POST['baseext'], $_POST['usuariobase']);
								$linkmulti->set_charset("utf8");
								$sqlr = "
										SELECT T1.estado, T1.cuenta, T1.ncuentaban, T1.tipo, T2.razonsocial, T1.tercero
										FROM tesobancosctas AS T1
										INNER JOIN terceros AS T2
										ON T1.tercero = T2.cedulanit
										INNER JOIN cuentasnicsp AS T3
										ON T3.cuenta = T1.cuenta
										WHERE T1.estado = 'S' ";
								$res = mysqli_query($linkmulti, $sqlr);
								while ($row = mysqli_fetch_row($res)) {
									if ($_POST['banco2'] == $row[1]) {
										echo "<option value='$row[1]' SELECTED>$row[2] - Cuenta $row[3]</option>";
										$_POST['nbanco2'] = $row[4];
										$_POST['cb2'] = $row[2];
										$_POST['ter2'] = $row[5];
									} else {
										echo "<option value='$row[1]'>$row[2] - Cuenta $row[3]</option>";
									}
								}
							}
							?>
						</select>
					</td>
					<td colspan="3"><input type="text" id="nbanco2" name="nbanco2" value="<?php echo $_POST['nbanco2'] ?>"
							style="width:100%;" readonly></td>
					<input type="hidden" id="cb2" name="cb2" value="<?php echo $_POST['cb2'] ?>">
					<input type="hidden" id="ter2" name="ter2" value="<?php echo $_POST['ter2'] ?>">
				</tr>
				<tr>
					<td class="textonew01" >Tipo Pago:</td>
					<td style="width:12%">
						<select name="tipopago" id="tipopago" onKeyUp="return tabular(event,this)" onChange="validar()"
							style="width:98%;">
							<?php 
								if ($_POST['tipopago'] == 'CSF'){
									echo "<option value='CSF' SELECTED>CSF</option>";
								}else{
									echo "<option value='CSF'>CSF</option>";
								}
								if($_POST['tipotras'] == 1){
									if ($_POST['tipopago'] == 'SSF'){
										echo "<option value='SSF' SELECTED>SSF</option>";
									}else{
										echo "<option value='SSF'>SSF</option>";
									}
								}
							?>
						</select>
					</td>
					<?php
					if($_POST['tipopago'] == 'CSF'){
						echo "<td class='textonew01' >Corresponder Traslado SSF:</td>
						<td>
							<select name='corresponder' id='corresponder' onChange='validar()' style='width:98%;'>";
								if ($_POST['corresponder'] == 'N'){
									echo "<option value='N' SELECTED>NO</option>";
								}else{
									echo "<option value='N'>NO</option>";
								}
								if($_POST['tipotras'] == 1){
									if ($_POST['corresponder'] == 'S'){
										echo "<option value='S' SELECTED>SI</option>>";
									}else{
										echo "<option value='S'>SI</option>";
									}
								}
						echo"
							</select>
						</td> ";
						if ($_POST['corresponder'] == 'S'){
							echo"
								<td><input type='text' name='trasladoSSF' id='trasladoSSF' value='".$_POST['trasladoSSF']."' style='width:100%;text-transform:uppercase' class='colordobleclik' onDblClick=\"despliegamodal2('visible','1');\" readonly /></td>";
						}
					}else{
						echo "<td class='textonew01' colspan='4'>Traslado pendiente por con y realizar efectivamente la devolución de los recursos</td>";
					}

					?>
				</tr>
				<tr>
					<td class="textonew01">Valor:</td>
					<td><input type="text" id="valor" name="valor" value="<?php echo $_POST['valor'] ?>"
							onKeyUp="return tabular(event,this)" style='width:98%;'></td>
					<td colspan="5"><em class="botonflechaverde" onClick="agregardetalle()">Agregar</em></td>
				</tr>
			</table>
			<input type="hidden" name="agregadet" value="0">
			<input type="hidden" name="oculto" value="1">
			<input type="hidden" name="vigencia" value="<?php echo $_POST['vigencia']; ?>">
			<input type='hidden' name='dorigen' value="<?php echo $_POST['dorigen']; ?>">
			<script>
				document.form2.valor.focus();
				document.form2.valor.select();
			</script>
			<table class='tablamv'>
				<thead>
					<tr>
						<th colspan="9" class="titulos">Detalle Traslados</th>
					</tr>
					<tr>
						<th class="titulosnew00" style="width:5%;">Tipo</th>
						<th class="titulosnew00" style="width:5%;">Destino</th>
						<th class="titulosnew00" style="width:10%;">No Transacci&oacute;n</th>
						<th class="titulosnew00" style="width:16%;">CC-Origen</th>
						<th class="titulosnew00" style="width:16%;">Cuenta Bancaria Origen</th>
						<th class="titulosnew00" style="width:16%;">CC-Destino</th>
						<th class="titulosnew00" style="width:16%;">Cuenta Bancaria Destino</th>
						<th class="titulosnew00">Valor</th>
						<th class="titulosnew00" style="width:5%;"><img src="imagenes/del.png"><input type='hidden'
								name='elimina' id='elimina'></th>
					</tr>
				</thead>
				<tbody style='max-height:33vh;'>
					<?php
					if ($_POST['oculto'] == '9') {
						$posi = $_POST['elimina'];
						unset($_POST['dccs'][$posi]);
						unset($_POST['dccs2'][$posi]);
						unset($_POST['dconsig'][$posi]);
						unset($_POST['dbancos'][$posi]);
						unset($_POST['dnbancos'][$posi]);
						unset($_POST['dbancos2'][$posi]);
						unset($_POST['dnbancos2'][$posi]);
						unset($_POST['dcbs'][$posi]);
						unset($_POST['dcbs2'][$posi]);
						unset($_POST['dcts'][$posi]);
						unset($_POST['dcts2'][$posi]);
						unset($_POST['dvalores'][$posi]);
						unset($_POST['dtipotraslado'][$posi]);
						unset($_POST['ddestino'][$posi]);
						unset($_POST['dndestino'][$posi]);
						unset($_POST['dbase'][$posi]);
						unset($_POST['duserbase'][$posi]);
						unset($_POST['dccnombre'][$posi]);
						unset($_POST['dccnombre2'][$posi]);
						unset($_POST['dtipopago'][$posi]);
						unset($_POST['dtrasSSF'][$posi]);
						unset($_POST['didSSF'][$posi]);
						$_POST['dccs'] = array_values($_POST['dccs']);
						$_POST['dccs2'] = array_values($_POST['dccs2']);
						$_POST['dconsig'] = array_values($_POST['dconsig']);
						$_POST['dbancos'] = array_values($_POST['dbancos']);
						$_POST['dnbancos'] = array_values($_POST['dnbancos']);
						$_POST['dbancos2'] = array_values($_POST['dbancos2']);
						$_POST['dnbancos2'] = array_values($_POST['dnbancos2']);
						$_POST['dcbs'] = array_values($_POST['dcbs']);
						$_POST['dcbs2'] = array_values($_POST['dcbs2']);
						$_POST['dcts'] = array_values($_POST['dcts']);
						$_POST['dcts2'] = array_values($_POST['dcts2']);
						$_POST['dvalores'] = array_values($_POST['dvalores']);
						$_POST['dtipotraslado'] = array_values($_POST['dtipotraslado']);
						$_POST['ddestino'] = array_values($_POST['ddestino']);
						$_POST['dndestino'] = array_values($_POST['ddnestino']);
						$_POST['dbase'] = array_values($_POST['dbase']);
						$_POST['duserbase'] = array_values($_POST['duserbase']);
						$_POST['dccnombre'] = array_values($_POST['dccnombre']);
						$_POST['dccnombre2'] = array_values($_POST['dccnombre2']);
						$_POST['dtipopago'] = array_values($_POST['dtipopago']);
						$_POST['dtrasSSF'] = array_values($_POST['dtrasSSF']);
						$_POST['didSSF'] = array_values($_POST['didSSF']);
					}
					if ($_POST['agregadet'] == '1') {
						$cpatri1 = $cpatri2 = $bansi = '';
						if (($_POST['tipotras'] == '1') && ($_POST['cc'] != $_POST['cc2'])) {
							$cpatri1 = buscarcuentapatrimonio($_POST['cc']);
							$cpatri2 = buscarcuentapatrimonio($_POST['cc2']);
							if ($cpatri1 == '' && $cpatri2 == '') {
								$bansi = '0';
							} elseif ($cpatri1 == '') {
								$bansi = '1';
							} elseif ($cpatri2 == '') {
								$bansi = '2';
							}
						}
						switch ($bansi) {
							case '': {
								$_POST['dccs'][] = $_POST['cc'];
								$_POST['dccs2'][] = $_POST['cc2'];
								$_POST['dconsig'][] = $_POST['numero'];
								$_POST['dbancos'][] = $_POST['banco'];
								$_POST['dnbancos'][] = $_POST['nbanco'];
								$_POST['dbancos2'][] = $_POST['banco2'];
								$_POST['dnbancos2'][] = $_POST['nbanco2'];
								$_POST['dcbs'][] = $_POST['cb'];
								$_POST['dcbs2'][] = $_POST['cb2'];
								$_POST['dcts'][] = $_POST['ter'];
								$_POST['dcts2'][] = $_POST['ter2'];
								$_POST['dvalores'][] = $_POST['valor'];
								$_POST['dtipotraslado'][] = $_POST['tipotras'];
								$_POST['ddestino'][] = $_POST['tipotrasext'];
								$_POST['dndestino'][] = $_POST['ndestino'];
								$_POST['dbase'][] = $_POST['baseext'];
								$_POST['duserbase'][] = $_POST['usuariobase'];
								$_POST['dccnombre'][] = $_POST['ccnombre'];
								$_POST['dccnombre2'][] = $_POST['ccnombre2'];
								$_POST['dtipopago'][] = $_POST['tipopago'];
								if($_POST['tipopago'] == 'SSF'){
									$_POST['dtrasSSF'][] = 'N';
									$_POST['didSSF'][] = '';
								}else {
									$_POST['dtrasSSF'][] = $_POST['corresponder'];
									if ($_POST['corresponder'] == 'S'){
										$_POST['didSSF'][] = $_POST['trasladoSSF'];
									} else {
										$_POST['didSSF'][] = '';
									}
									
								}
								
								$_POST['agregadet'] = '0';
								echo "
										<script>
											document.form2.cc.value = '';
											document.form2.cc2.value = '';
											document.form2.banco.value = '';
											document.form2.nbanco.value = '';
											document.form2.banco2.value = '';
											document.form2.nbanco2.value = '';
											document.form2.cb.value = '';
											document.form2.cb2.value = '';
											document.form2.ter.value = '';
											document.form2.ter2.value = '';
											document.form2.valor.value = '';
											document.form2.numero.value = '';
											document.form2.ccnombre.value = '';
											document.form2.ccnombre2.value = '';
											document.form2.agregadet.value = '0';
											document.form2.trasladoSSF.value = '';
											document.form2.corresponder.value = 'N';
											document.form2.tipopago.value = 'CSF;
											document.form2.numero.select();
											document.form2.numero.focus();
										</script>";
							}
								break;
							case '0': {
								echo "
										<script>
											Swal.fire({
												icon: 'error',
												title: 'Error!',
												text: 'Falta por parametrizar las cuentas de patrimonio de los dos centros de costo',
												confirmButtonText: 'Continuar',
												confirmButtonColor: '#FF121A',
												timer: 2500
											});
										</script>";
							}
								break;
							case '1': {
								echo "
										<script>
											Swal.fire({
												icon: 'error',
												title: 'Error!',
												text: 'Falta parametrizar la cuenta de patrimonio del centro de costo: " . $_POST['cc'] . "',
												confirmButtonText: 'Continuar',
												confirmButtonColor: '#FF121A',
												timer: 2500
											});
										</script>";
							}
								break;
							case '2': {
								echo "
										<script>
											Swal.fire({
												icon: 'error',
												title: 'Error!',
												text: 'Falta parametrizar la cuenta de patrimonio del centro de costo: " . $_POST['cc2'] . "',
												confirmButtonText: 'Continuar',
												confirmButtonColor: '#FF121A',
												timer: 2500
											});
										</script>";
							}
								break;
						}
					}
					$_POST['totalc'] = 0;
					$iter = 'saludo1a';
					$iter2 = 'saludo2';
					for ($x = 0; $x < count($_POST['dbancos']); $x++) {
						if ($_POST['dtipotraslado'][$x] == 1) {
							$tipotraslado = 'Interno';
							$nomdestino = '--';
						} else {
							$tipotraslado = 'Externo';
							$nomdestino = $_POST['ddestino'][$x] . " - " . $_POST['dndestino'][$x];
						}
						echo "
									<input type='hidden' name='dnbancos[]' value='" . $_POST['dnbancos'][$x] . "'>
									<input type='hidden' name='dconsig[]' value='" . $_POST['dconsig'][$x] . "'>
									<input type='hidden' name='dccs[]' value='" . $_POST['dccs'][$x] . "'>
									<input type='hidden' name='dccnombre[]' value='" . $_POST['dccnombre'][$x] . "'>
									<input type='hidden' name='dcbs[]' value='" . $_POST['dcbs'][$x] . "'>
									<input type='hidden' name='dbancos[]' value='" . $_POST['dbancos'][$x] . "'>
									<input type='hidden' name='dcts[]' value='" . $_POST['dcts'][$x] . "'>
									<input type='hidden' name='dccs2[]' value='" . $_POST['dccs2'][$x] . "'>
									<input type='hidden' name='dccnombre2[]' value='" . $_POST['dccnombre2'][$x] . "'>
									<input type='hidden' name='dcts2[]' value='" . $_POST['dcts2'][$x] . "'>
									<input type='hidden' name='dnbancos2[]' value='" . $_POST['dnbancos2'][$x] . "'>
									<input type='hidden' name='dbancos2[]' value='" . $_POST['dbancos2'][$x] . "'>
									<input type='hidden' name='dcbs2[]' value='" . $_POST['dcbs2'][$x] . "'>
									<input type='hidden' name='dvalores[]' value='" . $_POST['dvalores'][$x] . "'>
									<input type='hidden' name='dtipotraslado[]' value='" . $_POST['dtipotraslado'][$x] . "'>
									<input type='hidden' name='ddestino[]' value='" . $_POST['ddestino'][$x] . "'>
									<input type='hidden' name='dndestino[]' value='" . $_POST['dndestino'][$x] . "'>
									<input type='hidden' name='dbase[]' value='" . $_POST['dbase'][$x] . "'>
									<input type='hidden' name='duserbase[]' value='" . $_POST['duserbase'][$x] . "'>
									<input type='hidden' name='dtipopago[]' value='" . $_POST['dtipopago'][$x] . "'>
									<input type='hidden' name='dtrasSSF[]' value='" . $_POST['dtrasSSF'][$x] . "'>
									<input type='hidden' name='didSSF[]' value='" . $_POST['didSSF'][$x] . "'>
								<tr class='$iter'>
									<td style='width:5%;'>$tipotraslado</td>
									<td style='width:5%;'>$nomdestino</td>
									<td style='width:10%;'>" . $_POST['dconsig'][$x] . "</td>
									<td style='width:16%;'>" . $_POST['dccs'][$x] . " - " . $_POST['dccnombre'][$x] . "</td>
									<td style='width:16%;'>" . $_POST['dcbs'][$x] . " - " . $_POST['dnbancos'][$x] . "</td>
									<td style='width:16%;'>" . $_POST['dccs2'][$x] . " - " . $_POST['dccnombre2'][$x] . "</td>
									<td style='width:16%;'>" . $_POST['dcbs2'][$x] . " - " . $_POST['dnbancos2'][$x] . "</td>
									<td style='width:11%;text-align:right;'>$ " . number_format($_POST['dvalores'][$x], 0) . "&nbsp;</td>
									<td style='width:4%;text-align:center;'><a href='#' onclick='eliminar($x)'><img src='imagenes/del.png'></a></td>
								</tr>";
						$_POST['totalc'] = $_POST['totalc'] + $_POST['dvalores'][$x];
						$_POST['totalcf'] = number_format($_POST['totalc'], 2, ".", ",");
						$aux = $iter;
						$iter = $iter2;
						$iter2 = $aux;
					}
					$resultado = convertir($_POST['totalc']);
					$_POST['letras'] = $resultado . " Pesos";
					echo "
							<input type='hidden' name='totalcf' type='text' value='" . $_POST['totalcf'] . "'>
							<input type='hidden' name='totalc' value='" . $_POST['totalc'] . "'>
							<input type='hidden' name='letras' value='" . $_POST['letras'] . "'>
							<tr>
								<td class='saludo2' style='width:84%;text-align:right;'>Total</td>
								<td class='saludo2' style='width:12%;text-align:right;'>" . $_POST['totalcf'] . "</td>
								<td></td>
							</tr>
							<tr><td colspan='5'>Son: " . $_POST['letras'] . "</td></tr>";
					?>
				<tbody>
			</table>
			<?php
				if ($_POST['oculto'] == '2') {
					preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST['fecha'], $fecha);
					$fechaf = $fecha[3] . "-" . $fecha[2] . "-" . $fecha[1];
					$bloq = bloqueos($_SESSION['cedulausu'], $fechaf);
					if ($bloq >= 1) {
						$sqlr = "select count(*) from tesotraslados_cab where id_consignacion = '" . $_POST['idcomp'] . "'";
						$res = mysqli_query($linkbd, $sqlr);
						while ($r = mysqli_fetch_row($res)) {
							$numerorecaudos = $r[0];
						}
						if ($numerorecaudos == 0) {
							$consec = 0;
							$consec = $_POST['idcomp'];
							$sqlr = "INSERT INTO comprobante_cab (numerotipo, tipo_comp, fecha, concepto, total, total_debito, total_credito, diferencia, estado) VALUES ('$consec','10', '$fechaf', '" . $_POST['concepto'] . "', '0', '" . $_POST['totalc'] . "', '" . $_POST['totalc'] . "', '0', '1')";
							mysqli_query($linkbd, $sqlr);

							$idcomp = selconsecutivo('tesotraslados_cab', 'id_comp');
							$idtraslados = selconsecutivo('tesotraslados_cab', 'id_consignacion');
							$sqlr = "INSERT INTO tesotraslados_cab (id_consignacion, id_comp, fecha, vigencia, estado, concepto, origen, medio_pago, id_destino, descripcion_rev) VALUES ('$idtraslados', '$idcomp', '$fechaf', '" . $_POST['vigencia'] . "', 'S', '" . $_POST['concepto'] . "','" . $_POST['dorigen'] . "', '201', '', '')";
							mysqli_query($linkbd, $sqlr);

							echo "<input type='hidden' name='ncomp' value='$idcomp'>";
							if ($_POST['tipotras'] == '1') {
								$sqlr = "INSERT INTO tesotraslados_cab (id_consignacion, id_comp, fecha, vigencia, estado, concepto, origen, medio_pago, id_destino, descripcion_rev) VALUES ('$idtraslados', '$idcomp', '$fechaf', '" . $_POST['vigencia'] . "', 'S', '" . $_POST['concepto'] . "','" . $_POST['dorigen'] . "', '201', '0', '')";
								mysqli_query($linkbd, $sqlr);
								for ($x = 0; $x < count($_POST['dbancos']); $x++) {
									//**** consignacion  BANCARIA*****
									$sqlr = "INSERT INTO comprobante_det (id_comp, cuenta, tercero, centrocosto, detalle, cheque, valdebito, valcredito, estado, vigencia) VALUES ('10 $consec', '" . $_POST['dbancos'][$x] . "', '" . $_POST['dcts'][$x] . "', '" . $_POST['dccs'][$x] . "', 'Traslado " . $_POST['dconsig'][$x] . " " . $_POST['dnbancos'][$x] . "', '', 0, " . round($_POST['dvalores'][$x], 2) . ", '1', '" . $_POST['vigencia'] . "')";
									mysqli_query($linkbd, $sqlr);
									//*** Cuenta CAJA **
									$sqlr = "INSERT INTO comprobante_det (id_comp, cuenta, tercero, centrocosto, detalle, cheque, valdebito, valcredito, estado, vigencia) VALUES ('10 $consec', '" . $_POST['dbancos2'][$x] . "', '" . $_POST['dcts2'][$x] . "', '" . $_POST['dccs2'][$x] . "', 'Traslado " . $_POST['dconsig'][$x] . " " . $_POST['dnbancos2'][$x] . "', '', " . round($_POST['dvalores'][$x], 2) . ", 0, '1', '" . $_POST['vigencia'] . "')";
									mysqli_query($linkbd, $sqlr);
									if ($_POST['dccs'][$x] != $_POST['dccs2'][$x]) {
										//**** consignacion  BANCARIA*****
										$cuentapatrimonio1 = buscarcuentapatrimonio($_POST['dccs'][$x]);
										$sqlr = "INSERT INTO comprobante_det (id_comp, cuenta, tercero, centrocosto, detalle, cheque, valdebito, valcredito, estado, vigencia) VALUES ('10 $consec', '$cuentapatrimonio1', '" . $_POST['dcts'][$x] . "', '" . $_POST['dccs'][$x] . "', 'Traslado " . $_POST['dconsig'][$x] . " " . $_POST['dnbancos'][$x] . "', '', " . round($_POST['dvalores'][$x], 2) . ", 0, '1', '" . $_POST['vigencia'] . "')";
										mysqli_query($linkbd, $sqlr);
										//*** Cuenta CAJA **
										$cuentapatrimonio2 = buscarcuentapatrimonio($_POST['dccs2'][$x]);
										$sqlr = "INSERT INTO comprobante_det (id_comp, cuenta, tercero, centrocosto, detalle, cheque, valdebito, valcredito, estado, vigencia) VALUES ('10 $consec', '$cuentapatrimonio2', '" . $_POST['dcts2'][$x] . "', '" . $_POST['dccs2'][$x] . "', 'Traslado " . $_POST['dconsig'][$x] . " " . $_POST['dnbancos2'][$x] . "', '', 0, " . round($_POST['dvalores'][$x], 2) . ", '1', '" . $_POST['vigencia'] . "')";
										mysqli_query($linkbd, $sqlr);
									}
								}
								for ($x = 0; $x < count($_POST['dbancos']); $x++) {
									$sqlr = "INSERT INTO tesotraslados (id_trasladocab, fecha, ntransaccion, cco, ncuentaban1, tercero1, ccd, ncuentaban2, tercero2, valor, estado, tipo_traslado, destino_ext, tipo_mov, tipo_pago, trasladoSSF, idSSF) VALUES ('$idtraslados', '$fechaf','" . $_POST['dconsig'][$x] . "', '" . $_POST['dccs'][$x] . "', '" . $_POST['dcbs'][$x] . "', '" . $_POST['dcts'][$x] . "', '" . $_POST['dccs2'][$x] . "', '" . $_POST['dcbs2'][$x] . "', '" . $_POST['dcts2'][$x] . "', " . $_POST['dvalores'][$x] . ", 'S','INT','','201', '".$_POST['dtipopago'][$x]."', '".$_POST['dtrasSSF'][$x]."', '".$_POST['didSSF'][$x]."')";
									if (!mysqli_query($linkbd, $sqlr)) {
										echo "
												<script>
													Swal.fire({
														icon: 'error',
														title: 'Error!',
														text: 'No se pudo ejecutar la petición',
														confirmButtonText: 'Continuar',
														confirmButtonColor: '#FF121A',
														timer: 2500
													});
												</script>";
									} else {
										echo "
												<script>
													document.form2.numero.value = '';
													document.form2.valor.value = 0;
													document.form2.oculto.value = 1;
													Swal.fire({
														icon: 'success',
														title: 'Se ha almacenado los Traslados con Exito',
														showConfirmButton: true,
														confirmButtonText: 'Continuar',
														confirmButtonColor: '#01CC42',
														timer: 3500
													}).then((response) => {
														location.href='teso-trasladosvisualizar.php?idr=" . $_POST['idcomp'] . "&dc=" . $_POST['idcomp'] . "&scrtop=0';
													});
												</script>";
									}
								}
							} else {
								$linkmulti = conectar_Multi($_POST['baseext'], $_POST['usuariobase']);
								$linkmulti->set_charset("utf8");
								$consecsal = selconsecutivomulti('tesotraslados_cab', 'id_consignacion', $_POST['baseext'], $_POST['usuariobase']);
								$idcompsal = selconsecutivomulti('tesotraslados_cab', 'id_comp', $_POST['baseext'], $_POST['usuariobase']);
								$idtrasladossal = $consecsal;

								$sqlr = "INSERT INTO comprobante_cab (numerotipo, tipo_comp, fecha, concepto, total, total_debito, total_credito, diferencia, estado) VALUES ('$consecsal','10', '$fechaf', '" . $_POST['concepto'] . "', '0', '" . $_POST['totalc'] . "', '" . $_POST['totalc'] . "', '0', '1')";
								mysqli_query($linkmulti, $sqlr);

								$sqlr = "INSERT INTO tesotraslados_cab (id_consignacion, id_comp, fecha, vigencia, estado, concepto, origen, medio_pago, id_destino, descripcion_rev) VALUES ('$consecsal', '$idcompsal', '$fechaf', '" . $_POST['vigencia'] . "', 'S', '" . $_POST['concepto'] . "','" . $_POST['dorigen'] . "', '201', '" . $_POST['tipotrasext'] . "', '$idtraslados')";
								mysqli_query($linkmulti, $sqlr);

								$sqlr = "UPDATE tesotraslados_cab SET id_destino = '" . $_POST['tipotrasext'] . "', descripcion_rev = '$consecsal' WHERE id_consignacion = '$idtraslados'";
								mysqli_query($linkbd, $sqlr);

								for ($x = 0; $x < count($_POST['dbancos']); $x++) {
									$debito = 0;
									$credito = $_POST['dvalores'][$x];
									$sqlr = "INSERT INTO comprobante_det (id_comp, cuenta, tercero, centrocosto, detalle, cheque, valdebito, valcredito, estado, vigencia) VALUES ('10 $consec', '" . $_POST['dbancos'][$x] . "', '" . $_POST['dcts'][$x] . "', '" . $_POST['dccs'][$x] . "', 'Traslado " . $_POST['dconsig'][$x] . " " . $_POST['dnbancos'][$x] . "', ''," . round($debito, 2) . ", " . round($credito, 2) . ", '1', '" . $_POST['vigencia'] . "')";
									mysqli_query($linkbd, $sqlr);
									$sqlr = "INSERT INTO comprobante_det (id_comp, cuenta, tercero, centrocosto, detalle, cheque, valdebito, valcredito, estado, vigencia) VALUES ('10 $consec', '" . $_POST['cuentatraslado'] . "', '" . $_POST['dcts'][$x] . "', '" . $_POST['dccs'][$x] . "', 'Traslado " . $_POST['dconsig'][$x] . " " . $_POST['dnbancos'][$x] . "', ''," . round($credito, 2) . ", " . round($debito, 2) . ", '1', '" . $_POST['vigencia'] . "')";
									mysqli_query($linkbd, $sqlr);
									$debito = $_POST['dvalores'][$x];
									$credito = 0;
									$sqlr = "INSERT INTO comprobante_det (id_comp, cuenta, tercero, centrocosto, detalle, cheque, valdebito, valcredito, estado, vigencia) VALUES ('10 $consecsal', '" . $_POST['dbancos2'][$x] . "', '" . $_POST['dcts'][$x] . "', '" . $_POST['dccs'][$x] . "', 'Traslado " . $_POST['dconsig'][$x] . " " . $_POST['dnbancos'][$x] . "', ''," . round($debito, 2) . ", " . round($credito, 2) . ", '1', '" . $_POST['vigencia'] . "')";
									mysqli_query($linkmulti, $sqlr);
									$sqlct = "SELECT cuentatraslado FROM tesoparametros WHERE estado = 'S'";
									$resct = mysqli_query($linkmulti, $sqlct);
									$rowct = mysqli_fetch_row($resct);
									$sqlr = "INSERT INTO comprobante_det (id_comp, cuenta, tercero, centrocosto, detalle, cheque, valdebito, valcredito, estado, vigencia) VALUES ('10 $consecsal', '$rowct[0]', '" . $_POST['dcts'][$x] . "', '" . $_POST['dccs'][$x] . "', 'Traslado " . $_POST['dconsig'][$x] . " " . $_POST['dnbancos'][$x] . "', '', " . round($credito, 2) . ", " . round($debito, 2) . ", '1', '" . $_POST['vigencia'] . "')";
									mysqli_query($linkmulti, $sqlr);
								}
								//***************CREACION DE LA TABLA EN CTRASLADOS *********************
								for ($x = 0; $x < count($_POST['dbancos']); $x++) {
									$sqlr = "INSERT INTO tesotraslados (id_trasladocab, fecha, ntransaccion, cco, ncuentaban1, tercero1, ccd, ncuentaban2, tercero2, valor, estado, tipo_traslado, destino_ext, tipo_mov, tipo_pago, trasladoSSF, idSSF) VALUES ('$idtraslados', '$fechaf', '" . $_POST['dconsig'][$x] . "', '" . $_POST['dccs'][$x] . "', '" . $_POST['dcbs'][$x] . "', '" . $_POST['dcts'][$x] . "', '" . $_POST['dccs2'][$x] . "', '" . $_POST['dcbs2'][$x] . "', '" . $_POST['dcts2'][$x] . "', " . round($_POST['dvalores'][$x], 2) . ", 'S', 'EXT','" . $_POST['ddestino'][$x] . "','201', '".$_POST['dtipopago'][$x]."', '".$_POST['dtrasSSF'][$x]."', '".$_POST['didSSF'][$x]."')";
									mysqli_query($linkbd, $sqlr);
									$sqlr = "INSERT INTO tesotraslados (id_trasladocab, fecha, ntransaccion, cco, ncuentaban1, tercero1, ccd, ncuentaban2, tercero2, valor, estado, tipo_traslado, destino_ext, tipo_mov, tipo_pago, trasladoSSF, idSSF) VALUES ('$idtrasladossal', '$fechaf', '" . $_POST['dconsig'][$x] . "', '" . $_POST['dccs'][$x] . "', '" . $_POST['dcbs'][$x] . "', '" . $_POST['dcts'][$x] . "', '" . $_POST['dccs2'][$x] . "', '" . $_POST['dcbs2'][$x] . "', '" . $_POST['dcts2'][$x] . "', " . round($_POST['dvalores'][$x], 2) . ", 'S', 'EXT','" . $_POST['ddestino'][$x] . "','201', '".$_POST['dtipopago'][$x]."', '".$_POST['dtrasSSF'][$x]."', '".$_POST['didSSF'][$x]."')";
									mysqli_query($linkmulti, $sqlr);
								}
								echo "
										<script>
											Swal.fire({
												icon: 'success',
												title: 'Se ha almacenado los Traslados con Exito',
												showConfirmButton: true,
												confirmButtonText: 'Continuar',
												confirmButtonColor: '#01CC42',
												timer: 3500
											}).then((response) => {
												location.href='teso-trasladosvisualizar.php?idr=" . $_POST['idcomp'] . "&dc=" . $_POST['idcomp'] . "&scrtop=0';
											});
										</script>";
							}
						} else {
							echo "
									<script>
										Swal.fire({
											icon: 'error',
											title: 'Error!',
											text: 'Ya existe un traslado con este consecutivo',
											confirmButtonText: 'Continuar',
											confirmButtonColor: '#FF121A',
											timer: 2500
										});
									</script>";
						}
					} else {
						echo "
								<script>
									Swal.fire({
										icon: 'error',
										title: 'Error!',
										text: 'Periodo bloqueado, informe enviado.',
										confirmButtonText: 'Continuar',
										confirmButtonColor: '#FF121A',
										timer: 2500
									});
								</script>";
					}
				}
			?>
		</form>
		<div id="bgventanamodal2">
			<div id="ventanamodal2">
				<IFRAME  src="" name="buster" marginWidth=0 marginHeight=0 frameBorder=0 id="ventana2" frameSpacing=0 style="left:500px; width:900px; height:500px; top:200;"></IFRAME>
			</div>
		</div>
	</body>

</html>
