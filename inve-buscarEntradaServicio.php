<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require 'comun.inc';
	require 'funciones.inc';
	session_start();
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
	titlepag();
?>
<!DOCTYPE >
<html lang=es>
	<head>
		<meta charset="utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1"/>
		<meta name="viewport" content="user-scalable=no">
		<title>:: IDEAL 10 - Servicios Públicos</title>
		<link href="css/css2z.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/tabs.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
		<script src="vue/vue.min.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script src="css/calendario.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<style>
			input[type=number]::-webkit-inner-spin-button,
			input[type=number]::-webkit-outer-spin-button { 
				-webkit-appearance: none; 
				margin: 0; 
			}
			input[type=number]{
				text-align:right;
				box-sizing: border-box;
				font: 120% sans-serif;
				width: 100% !important;
			}
			[v-cloak]{display : none;}
		</style>
		<script src="sweetalert2/dist/sweetalert2.min.js"></script>
		<link rel="stylesheet" href="sweetalert2/dist/sweetalert2.min.css">
	</head>
	<body>
		<header>
			<table>
				<tr><script>barra_imagenes("inve");</script><?php cuadro_titulos();?></tr>
			</table>
		</header>
		<form name="form2" method="post" action="">
			<section id="myapp" v-cloak >
				<nav>
					<table>
						<tr><?php menu_desplegable("inve");?></tr>
						<tr>
							<td colspan="3" class="cinta">
								<img src="imagenes/add.png" class="mgbt" v-on:click="location.href='inve-crearEntradaServicio.php'" title="Nuevo">
								<img src="imagenes/guardad.png" title="Guardar" class="mgbt1">
								<img src="imagenes/busca.png" v-on:click="location.href='inve-buscarEntradaServicio.php'" class="mgbt" title="Buscar">
								<img src="imagenes/nv.png" onClick="mypop=window.open('inve-principal.php','',''); mypop.focus();" class="mgbt" title="Nueva Ventana">
							</td>
						</tr>
					</table>
				</nav>
				<article>
					<table class="inicio ancho">
						<tr>
							<td class="titulos" colspan="10" >Buscar entrada:</td>
							<td class="cerrar" style="width:7%" onClick="location.href='serv-principal.php'">Cerrar</td>
						</tr>

                        <tr>
                            <td class="tamano01" style="width: 10%;">Consecutivo entrada:</td>
                            <td>
                                <input type="text" v-model="codigoEntrada" style="width: 98%;" autocomplete="off">
                            </td>

                            <td class="tamano01" style="width: 10%;">Fecha inicial</td>
                            <td>
                                <input type="text" name="fechaInicial" id="fechaInicial" onchange="" maxlength="10" onKeyUp="return tabular(event,this)" onKeyDown="mascara(this,'/',patron,true)" title="DD/MM/YYYY" style="text-align:center;" autocomplete="off" class="colordobleclik" ondblclick="displayCalendarFor('fechaInicial');">
                            </td>

                            <td class="tamano01" style="width: 10%;">Fecha final</td>
                            <td>
                                <input type="text" name="fechaFinal" id="fechaFinal" onchange="" maxlength="10" onKeyUp="return tabular(event,this)" onKeyDown="mascara(this,'/',patron,true)" title="DD/MM/YYYY" style="text-align:center;" autocomplete="off" class="colordobleclik" ondblclick="displayCalendarFor('fechaFinal');">
                            </td>

                            <td style="padding-bottom:0px">
                                <em class="botonflechaverde" v-on:Click="buscar">Buscar</em>
                            </td>
                        </tr>
					</table>

					<div class='subpantalla' style='height:66vh; width:99.2%; margin-top:0px; overflow:hidden'>
						<table class='tablamv'>
							<thead>
								<tr style="text-align:Center;">
									<th class="titulosnew00" style="width:6%;">Consecutivo</th>
									<th class="titulosnew00">Descripcion</th>
									<th class="titulosnew00" style="width:15%;">Fecha</th>
                                    <th class="titulosnew00" style="width:15%;">RP</th>
                                    <th class="titulosnew00" style="width:15%;">Valor</th>
									<th class="titulosnew00" style="width:6%;">Estado</th>
									<th class="titulosnew00" style="width:6%;">Eliminar</th>
									<th class="titulosnew00" style="width:1%"></th>
								</tr>
							</thead>
                            
							<tbody>
                                <tr v-show="existeInformacion == true">
                                    <td colspan="7">
                                        <div style="text-align: center; color:turquoise; font-size:large" class="h4 text-primary text-center">
                                            Utilice los filtros para buscar información.
                                        </div>
                                    </td>
                                </tr>

								<tr v-for="(dato, index) in datos" @dblclick="seleccionaEntrada(dato)" v-bind:class="index % 2 ? 'saludo1a' : 'saludo2'" style='text-rendering: optimizeLegibility; cursor: pointer !important;'>
                                    <td style="width:6%; font: 120% sans-serif; padding-left:5px; text-align:center;">{{ dato[0] }}</td>
                                    <td style="font: 120% sans-serif; padding-left:5px; text-align:center;">{{ dato[2] }}</td>
									<td style="width:15%; font: 120% sans-serif; padding-left:5px; text-align:center;">{{ dato[1] }}</td>
                                    <td style="width:15%; font: 120% sans-serif; padding-left:5px; text-align:center;">{{ dato[3] }}</td>
                                    <td style="width:15%; font: 120% sans-serif; padding-left:5px; text-align:center;">{{ formatonumero(dato[4]) }}</td>
                                    <td style="width:6%; font: 120% sans-serif; padding-left:5px; text-align:center;">{{ comprobarEstado(dato[5]) }}</td>
									<td style="width:6%; text-align: center;"><img src="imagenes/eliminar.png" alt="Eliminar" @click="eliminarEntrada(dato)" height="25px" width="25px"></td>
								</tr>
							</tbody>
						</table>
					</div>

					<div id="cargando" v-if="loading" class="loading">
						<span>Cargando...</span>
					</div>

				</article>
			</section>
		</form>
		
		<script src="Librerias/vue/vue.min.js"></script>
		<script src="Librerias/vue/axios.min.js"></script>
		<script src="almacen/EntradaServicio/buscar/inve-buscarEntradaServicio.js"></script>
        
	</body>
</html>