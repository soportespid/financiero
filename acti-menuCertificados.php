<?php
header("Cache-control: no-cache, no-store, must-revalidate");
header("Content-Type: text/html;charset=utf8");
require "comun.inc";
require "funciones.inc";
session_start();
$linkbd=conectar_v7();
cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
header("Cache-control: private"); // Arregla IE 6
date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es">

<head>
    <meta http-equiv="Content-Type" content="text/html" charset="iso-8859-1" />
    <meta http-equiv="X-UA-Compatible" content="IE=9" />
    <title>:: IDEAL 10 - Activos fijos</title>
    <link href="favicon.ico" rel="shortcut icon"/>
    <link href="css/css2.css" rel="stylesheet" type="text/css" />
    <link href="css/css3.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="css/programas.js"></script>
</head>
<style>

</style>

<body>
    <IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
    <span id="todastablas2"></span>
    <table>
        <tr>
            <script>
            barra_imagenes("acti");
            </script><?php cuadro_titulos();?>
        </tr>
        <tr><?php menu_desplegable("acti");?></tr>
        <tr>
            <td colspan="3" class="cinta">
                <a class="mgbt"><img src="imagenes/add2.png"/></a>
                <a class="mgbt"><img src="imagenes/guardad.png"/></a>
                <a class="mgbt"><img src="imagenes/buscad.png"/></a>
                <a onClick="mypop=window.open('plan-agenda.php','','');mypop.focus()" class="mgbt"><img src="imagenes/agenda1.png" title="Agenda" /></a>
                <a onClick="mypop=window.open('acti-principal.php','','');mypop.focus();" class="mgbt"><img src="imagenes/nv.png" title="Nueva ventana"></a>
                <a onClick="mypop=window.open('<?php echo $url2; ?>','','');mypop.focus();" class="tooltip bottom mgbt"><img src="imagenes/duplicar_pantalla.png" title="Duplicar pesta&ntilde;a"></a>
            </td>
        </tr>
    </table>
    <form name="form2" method="post" action="">
        <table class="inicio">
            <tr>
                <td class="titulos" colspan="2">.: Certificados: </td>
                <td class="cerrar" style="width:7%;"><a href="acti-principal.php">&nbsp;Cerrar</a></td>
            </tr>
            <tr>
                <td style="background-repeat:no-repeat; background-position:center; width:70%">
                    <ol id="lista2">
                        <table>
                            <tr>
                                <td style="width:50%;">
                                    <li onClick="location.href='acti-certificadoResponsable'" style="cursor:pointer;">Certificado de activos por responsable</li>
                                    <li onClick="location.href='acti-buscaCertificadoDevolucionActivos'" style="cursor:pointer;">Certificado de devolución de activos</li>
                                    <li onClick="location.href='acti-buscaCertificadosPazySalvo'" style="cursor:pointer;">Certificado de paz y salvo de activos</li>
                                </td>
                            </tr>
                        </table>
                    </ol>
                </td>
                <td style="width:23%;">
                </td>
            </tr>
        </table>
    </form>
</body>

</html>
