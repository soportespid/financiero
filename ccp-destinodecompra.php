<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");

	require 'comun.inc';
	require 'funciones.inc';
	require "conversor.php";
	require "validaciones.inc";

	$linkbd = conectar_v7();
	$linkbd->set_charset("utf8");

	session_start();
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE>
<html lang="es">
<head>
	<meta charset="utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1" />
	<meta name="viewport" content="user-scalable=no">
	<title>:: IDEAL 10 - Contabilidad</title>
	<link href="favicon.ico" rel="shortcut icon" />
	<link href="css/css2.css?<?php echo date('d_m_Y_h_i_s'); ?>" rel="stylesheet" type="text/css" />
	<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s'); ?>" rel="stylesheet" type="text/css" />
	<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s'); ?>" rel="stylesheet" type="text/css" />
	<link href="css/tabs2.css?<?php echo date('d_m_Y_h_i_s'); ?>" rel="stylesheet" type="text/css" />
	<script type="text/javascript" src="css/programas.js?<?php echo date('d_m_Y_h_i_s'); ?>"></script>
	<script type="text/javascript" src="css/calendario.js?<?php echo date('d_m_Y_h_i_s'); ?>"></script>
	<script type="text/javascript" src="JQuery/jquery-2.1.4.min.js?<?php echo date('d_m_Y_h_i_s'); ?>"></script>
	<script>
		function guardar() {
			var rp = document.form2.rp.value;
			var tercero = document.form2.ntercero.value;
			var retencion = document.form2.retencion.value;
			var cuentasCont = document.getElementsByName('cuentaDebito[]');
			var retenciones_det = document.getElementsByName('ddescuentos[]');
			var cuentaContable = true;
			for (var x = 0; x < cuentasCont.length; x++) {
				if (cuentasCont[x].value == '') {
					cuentaContable = false;
				}
			}
			if (retencion != "" || retenciones_det.length > 0) {
				if (rp != "" && tercero != "" && cuentaContable) {
					despliegamodalm('visible', '4', '¿Esta seguro que desea guardar?', '1'); 
				} else { 
					despliegamodalm('visible', '2', 'Faltan datos por ingresar'); 
				}
			} else {
				despliegamodalm('visible', '2', 'Si no tiene retenciones, escoger no aplica en la pestaña de retenciones.');
			}
		}
		function despliegamodalm(_valor, _tip, msj, ask) {
			document.getElementById("bgventanamodalm").style.visibility = _valor;
			if (_valor == "hidden") { document.getElementById('ventanam').src = ""; }
			else {
				switch (_tip) {
					case "1": document.getElementById('ventanam').src = "ventana-mensaje1.php?titulos=" + msj; break;
					case "2": document.getElementById('ventanam').src = "ventana-mensaje3.php?titulos=" + msj; break;
					case "3": document.getElementById('ventanam').src = "ventana-mensaje2.php?titulos=" + msj; break;
					case "4": document.getElementById('ventanam').src = "ventana-consulta1.php?titulos=" + msj + "&idresp=" + ask; break;
					case "5": document.getElementById('ventanam').src = "ventana-mensaje2b.php?titulos=" + msj + "&accionx=" + ask; break;
				}
			}
		}
		function funcionmensaje() {
			var idcod = document.getElementById('numero').value
			document.location.href = "ccp-destinodecompraeditar.php?id=" + idcod;
		}
		function respuestaconsulta(ask) {
			switch (ask) {
				case "1":
					document.form2.oculto.value = '2';
					document.form2.submit();
					break;
			}
		}
		function despliegamodal2(_valor, v, _vaux = '', pos = '', sector = '', destino = '', seccion = '', inputIndex = '') {
			document.getElementById("bgventanamodal2").style.visibility = _valor;
			if (_valor == "hidden") { document.getElementById('ventana2').src = ""; }
			else {
				switch (v) {
					case '1':
						var numvigen = document.getElementById('vigencia').value;
						document.getElementById('ventana2').src = "ventana-rp.php?vigencia=" + numvigen;
						break;
					case '2':
						document.getElementById('ventana2').src = "parametrizarCuentaccpet-ventana02.php?cuenta=" + _vaux + "&posicion=" + pos + "&sector=" + sector + "&destino=" + destino + "&seccion=" + seccion;
						break;
					case '3':
						document.getElementById('ventana2').src = `parametrizarCuentaccpet-ventana03.php?cuenta=${_vaux}&posicion=${pos}&sector=${sector}&destino=${destino}&seccion=${seccion}&inputIndex=${inputIndex}`;
				}
			}
		}
		function buscarp(e) {
			if (document.form2.rp.value != "") {
				document.form2.brp.value = '1';
				document.form2.submit();
			}
		}
		function cambiocheck(id) {
			var nomid = 'idswsino' + id;
			if (document.getElementById(nomid).value == 'checked') { document.getElementById(nomid).value = 'N'; }
			else { document.getElementById(nomid).value = 'checked'; }
		}
		function agregardetalled() {
			if (document.form2.permisoac.value != '1') {
				if (document.form2.retencion.value != "" && document.form2.vporcentaje.value != "") {
					document.form2.agregadetdes.value = 1;
					document.form2.submit();
				}
				else { despliegamodalm('visible', '2', 'Falta información para poder Agregar'); }
			}
			else { despliegamodalm('visible', '2', 'No se puede agregar la retención hasta que no se corrija la parametrización'); }
		}
		function eliminard(variable) {
			if (confirm("Esta Seguro de Eliminar")) {
				document.form2.eliminad.value = variable;
				vvend = document.getElementById('eliminad');
				vvend.value = variable;
				document.form2.submit();
			}
		}
		function editarretencion() {
			var id = document.form2.retencion.value;
			var win = window.open("teso-editaretenciones.php?is=" + id, '_blank');
			win.focus();
		}
		function limpiar01(pos) {
			document.getElementsByName('cuentaDebito[]').item(pos).value = '';
			document.getElementsByName('cuentaCredito[]').item(pos).value = '';
			document.form2.cuendevcre.value = pos;
			document.form2.submit();
		}
		function accionventana(idf) {
			switch (idf) {
				case "1": location.href = 'ccp-destinodecompra.php'; break;
			}
		}
		function agregarcampo(x) {
			const tdDebito = document.getElementById(`td-debito-${x}`);
			const tdCredito = document.getElementById(`td-credito-${x}`);

			const cantidadInputsDebito = tdDebito.querySelectorAll('input[name^="cuentaDebitoM["]').length;
			const cantidadInputsCredito = tdCredito.querySelectorAll('input[name^="cuentaCreditoM["]').length;

			const nuevoInputDebitoM = document.createElement('input');
			nuevoInputDebitoM.setAttribute('class', 'colordobleclik');
			nuevoInputDebitoM.setAttribute('name', `cuentaDebitoM[${x}][${cantidadInputsDebito}]`);
			nuevoInputDebitoM.setAttribute('style', 'height:30px;font-weight: bold !important; margin-top: 5px;');
			nuevoInputDebitoM.setAttribute('readonly', true);
			nuevoInputDebitoM.addEventListener('click', () => {
				despliegamodal2('visible', '3', obtenerCodCuenta(x), x, obtenerCodSector(x), obtenerDestCompra(x), obtenerSecPresupuestal(x), cantidadInputsDebito);
			});

			const nuevoInputCreditoM = document.createElement('input');
			nuevoInputCreditoM.setAttribute('class', 'colordobleclik');
			nuevoInputCreditoM.setAttribute('name', `cuentaCreditoM[${x}][${cantidadInputsCredito}]`);
			nuevoInputCreditoM.setAttribute('style', 'height:30px;font-weight: bold !important; margin-top: 5px;');
			nuevoInputCreditoM.setAttribute('readonly', true);
			nuevoInputCreditoM.addEventListener('click', () => {
				despliegamodal2('visible', '3', obtenerCodCuenta(x), x, obtenerCodSector(x), obtenerDestCompra(x), obtenerSecPresupuestal(x), cantidadInputsCredito);
			});

			tdDebito.appendChild(document.createElement('br'));
			tdDebito.appendChild(nuevoInputDebitoM);

			tdCredito.appendChild(document.createElement('br'));
			tdCredito.appendChild(nuevoInputCreditoM);

			guardarEstadoInputs(x);
		}
		function guardarEstadoInputs(x) {
			const tdDebito = document.getElementById(`td-debito-${x}`);
			const tdCredito = document.getElementById(`td-credito-${x}`);

			const inputsDebito = tdDebito.querySelectorAll('input[name^="cuentaDebitoM["]');
			const inputsCredito = tdCredito.querySelectorAll('input[name^="cuentaCreditoM["]');

			const estado = {
				debito: Array.from(inputsDebito).map(input => input.value),
				credito: Array.from(inputsCredito).map(input => input.value)
			};

			localStorage.setItem(`estadoInputs-${x}`, JSON.stringify(estado));
		}
		function quitarcampo(x) {
			const tdDebito = document.getElementById(`td-debito-${x}`);
			const tdCredito = document.getElementById(`td-credito-${x}`);

			const inputsDebito = tdDebito.querySelectorAll('input[name^="cuentaDebitoM["]');
			const inputsCredito = tdCredito.querySelectorAll('input[name^="cuentaCreditoM["]');

			// Verificar si hay inputs dinámicos para eliminar
			if (inputsDebito.length > 0 && inputsCredito.length > 0) {
				// Eliminar el último input de débito y su salto de línea
				const ultimoInputDebito = inputsDebito[inputsDebito.length - 1];
				const saltoLineaDebito = ultimoInputDebito.previousElementSibling;
				tdDebito.removeChild(ultimoInputDebito);
				if (saltoLineaDebito && saltoLineaDebito.tagName === 'BR') {
					tdDebito.removeChild(saltoLineaDebito);
				}

				// Eliminar el último input de crédito y su salto de línea
				const ultimoInputCredito = inputsCredito[inputsCredito.length - 1];
				const saltoLineaCredito = ultimoInputCredito.previousElementSibling;
				tdCredito.removeChild(ultimoInputCredito);
				if (saltoLineaCredito && saltoLineaCredito.tagName === 'BR') {
					tdCredito.removeChild(saltoLineaCredito);
					guardarEstadoInputs(x);
				}
			} else {
				alert('No hay más campos para eliminar.');
			}
		}
		function obtenerCodCuenta(x) {
			const inputsCodCuenta = document.querySelectorAll("input[name='codCuenta[]']");
			if (x >= 0 && x < inputsCodCuenta.length) {
				const codCuenta = inputsCodCuenta[x].value;
				console.log("Valor de codCuenta en la posición", x, ":", codCuenta);
				return codCuenta;
			} else {
				console.error("Índice fuera de rango.");
				return null;
			}
		}
		function obtenerCodSector(x) {
			const inputsCodSector = document.querySelectorAll("input[name='codsector[]']");
			if (x >= 0 && x < inputsCodSector.length) {
				const codSector = inputsCodSector[x].value;
				console.log("Valor de codSector en la posición", x, ":", codSector);
				return codSector;
			} else {
				console.error("Índice fuera de rango.");
				return null;
			}
		}
		function obtenerDestCompra(x) {
			const selectDestCompra = document.querySelector(`select[name='destcompra${x}']`);
			
			if (selectDestCompra) {
				const destCompra = selectDestCompra.value;
				console.log("Valor de destcompra en la posición", x, ":", destCompra);
				return destCompra;
			} else {
				console.error("Índice fuera de rango o <select> no encontrado.");
				return null;
			}
		}
		function obtenerSecPresupuestal(x) {
			const inputsSecPresu = document.querySelectorAll("input[name='seccion_presupuestal[]']");
			if (x >= 0 && x < inputsSecPresu.length) {
				const codSecPresu = inputsSecPresu[x].value;
				console.log("Valor de codSector en la posición", x, ":", codSecPresu);
				return codSecPresu;
			} else {
				console.error("Índice fuera de rango.");
				return null;
			}
		}
		function cargarEstadoInputs(x) {
			const estado = JSON.parse(localStorage.getItem(`estadoInputs-${x}`));

			if (estado) {
				const tdDebito = document.getElementById(`td-debito-${x}`);
				const tdCredito = document.getElementById(`td-credito-${x}`);

				tdDebito.querySelectorAll('input[name^="cuentaDebitoM["]').forEach(input => {
					if (input.name !== 'cuentaDebito[]') {
						tdDebito.removeChild(input);
						if (input.previousElementSibling && input.previousElementSibling.tagName === 'BR') {
							tdDebito.removeChild(input.previousElementSibling);
						}
					}
				});

				tdCredito.querySelectorAll('input[name^="cuentaCreditoM["]').forEach(input => {
					if (input.name !== 'cuentaCredito[]') {
						tdCredito.removeChild(input);
						if (input.previousElementSibling && input.previousElementSibling.tagName === 'BR') {
							tdCredito.removeChild(input.previousElementSibling);
						}
					}
				});

				estado.debito.forEach((valor, index) => {
					const cantidadInputsDebito = tdDebito.querySelectorAll('input[name^="cuentaDebitoM["]').length; 
					const nuevoInputDebitoM = document.createElement('input');
					nuevoInputDebitoM.setAttribute('class', 'colordobleclik');
					nuevoInputDebitoM.setAttribute('name', `cuentaDebitoM[${x}][${index}]`);
					nuevoInputDebitoM.setAttribute('style', 'height:30px;font-weight: bold !important; margin-top: 5px;');
					nuevoInputDebitoM.setAttribute('readonly', true);
					nuevoInputDebitoM.setAttribute('value', valor);
					nuevoInputDebitoM.addEventListener('click', () => {
						despliegamodal2('visible', '3', obtenerCodCuenta(x), x, obtenerCodSector(x), obtenerDestCompra(x), obtenerSecPresupuestal(x), cantidadInputsDebito);
						
					});

					tdDebito.appendChild(document.createElement('br'));
					tdDebito.appendChild(nuevoInputDebitoM);
				});

				estado.credito.forEach((valor, index) => {
					const cantidadInputsCredito = tdCredito.querySelectorAll('input[name^="cuentaCreditoM["]').length;
					const nuevoInputCreditoM = document.createElement('input');
					nuevoInputCreditoM.setAttribute('class', 'colordobleclik');
					nuevoInputCreditoM.setAttribute('name', `cuentaCreditoM[${x}][${index}]`);
					nuevoInputCreditoM.setAttribute('style', 'height:30px;font-weight: bold !important; margin-top: 5px;');
					nuevoInputCreditoM.setAttribute('readonly', true);
					nuevoInputCreditoM.setAttribute('value', valor);
					nuevoInputCreditoM.addEventListener('click', () => {
						despliegamodal2('visible', '3', obtenerCodCuenta(x), x, obtenerCodSector(x), obtenerDestCompra(x), obtenerSecPresupuestal(x), cantidadInputsCredito);
					});

					tdCredito.appendChild(document.createElement('br'));
					tdCredito.appendChild(nuevoInputCreditoM);
				});
			}
		}
		window.addEventListener('load', () => {
			const x = 0;
			cargarEstadoInputs(x);
		});
	</script>
</head>
<body>
	<IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
	<span id="todastablas2"></span>
	<table>
		<tr>
			<script>barra_imagenes("cont");</script><?php cuadro_titulos(); ?>
		</tr>
		<tr><?php menu_desplegable("cont"); ?></tr>
	</table>
	<div class="bg-white group-btn p-1">
		<button type="button" onclick="location.href='ccp-destinodecompra.php'" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
			<span>Nuevo</span>
			<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960">
				<path d="M440-440H200v-80h240v-240h80v240h240v80H520v240h-80v-240Z"></path>
			</svg>
		</button>
		<button type="button" onclick="guardar();"
			class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
			<span>Guardar</span>
			<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960">
				<path
					d="M840-680v480q0 33-23.5 56.5T760-120H200q-33 0-56.5-23.5T120-200v-560q0-33 23.5-56.5T200-840h480l160 160Zm-80 34L646-760H200v560h560v-446ZM480-240q50 0 85-35t35-85q0-50-35-85t-85-35q-50 0-85 35t-35 85q0 50 35 85t85 35ZM240-560h360v-160H240v160Zm-40-86v446-560 114Z">
				</path>
			</svg>
		</button>
		<button type="button" onclick="location.href='ccp-buscardestinodecompra.php'" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
			<span>Buscar</span>
			<svg class="fill-black group-hover:fill-white w-5 h-5" xmlns="http://www.w3.org/2000/svg"
				viewBox="0 -960 960 960">
				<path
					d="M784-120 532-372q-30 24-69 38t-83 14q-109 0-184.5-75.5T120-580q0-109 75.5-184.5T380-840q109 0 184.5 75.5T640-580q0 44-14 83t-38 69l252 252-56 56ZM380-400q75 0 127.5-52.5T560-580q0-75-52.5-127.5T380-760q-75 0-127.5 52.5T200-580q0 75 52.5 127.5T380-400Z">
				</path>
			</svg>
		</button>
		<button type="button" onclick="mypop=window.open('plan-agenda.php','','');mypop.focus()" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
			<span class="group-hover:text-white">Agenda</span>
			<svg class="fill-black group-hover:fill-white w-5 h-5" xmlns="http://www.w3.org/2000/svg"
				viewBox="0 -960 960 960">
				<path d="M560-564v-68q33-14 67.5-21t72.5-7q26 0 51 4t49 10v64q-24-9-48.5-13.5T700-600q-38 0-73 9.5T560-564Zm0 220v-68q33-14 67.5-21t72.5-7q26 0 51 4t49 10v64q-24-9-48.5-13.5T700-380q-38 0-73 9t-67 27Zm0-110v-68q33-14 67.5-21t72.5-7q26 0 51 4t49 10v64q-24-9-48.5-13.5T700-490q-38 0-73 9.5T560-454ZM260-320q47 0 91.5 10.5T440-278v-394q-41-24-87-36t-93-12q-36 0-71.5 7T120-692v396q35-12 69.5-18t70.5-6Zm260 42q44-21 88.5-31.5T700-320q36 0 70.5 6t69.5 18v-396q-33-14-68.5-21t-71.5-7q-47 0-93 12t-87 36v394Zm-40 118q-48-38-104-59t-116-21q-42 0-82.5 11T100-198q-21 11-40.5-1T40-234v-482q0-11 5.5-21T62-752q46-24 96-36t102-12q58 0 113.5 15T480-740q51-30 106.5-45T700-800q52 0 102 12t96 36q11 5 16.5 15t5.5 21v482q0 23-19.5 35t-40.5 1q-37-20-77.5-31T700-240q-60 0-116 21t-104 59ZM280-494Z">
				</path>
			</svg>
		</button>
		<button type="button" onclick="window.open('cont-principal');" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
			<span>Nueva ventana</span>
			<svg class="fill-black group-hover:fill-white w-5 h-5" xmlns="http://www.w3.org/2000/svg"
				viewBox="0 -960 960 960">
				<path
					d="M320-120v-80h80v-80H160q-33 0-56.5-23.5T80-360v-400q0-33 23.5-56.5T160-840h640q33 0 56.5 23.5T880-760v400q0 33-23.5 56.5T800-280H560v80h80v80H320ZM160-360h640v-400H160v400Zm0 0v-400 400Z">
				</path>
			</svg>
		</button>
		<button type="button" onclick="mypop=window.open('/financiero/ccp-destinodecompra.php','','');mypop.focus();" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
			<span class="group-hover:text-white">Duplicar pantalla</span>
			<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960">
				<path d="M640-160v-360H160v360h480Zm80-200v-80h80v-360H320v200h-80v-200q0-33 23.5-56.5T320-880h480q33 0 56.5 23.5T880-800v360q0 33-23.5 56.5T800-360h-80ZM160-80q-33 0-56.5-23.5T80-160v-360q0-33 23.5-56.5T160-600h480q33 0 56.5 23.5T720-520v360q0 33-23.5 56.5T640-80H160Zm400-603ZM400-340Z">
				</path>
			</svg>
		</button>
		<button type="button" onclick="window.location.href=''" class="btn btn-success btn-success-hover d-flex justify-between align-items-center">
			<span>Atras</span>
			<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960">
				<path d="M760-200v-160q0-50-35-85t-85-35H273l144 144-57 56-240-240 240-240 57 56-144 144h367q83 0 141.5 58.5T840-360v160h-80Z">
				</path>
			</svg>
		</button>
	</div>
	<div id="bgventanamodalm" class="bgventanamodalm">
		<div id="ventanamodalm" class="ventanamodalm">
			<IFRAME src="" name="ventanam" marginWidth=0 marginHeight=0 frameBorder=0 id="ventanam" frameSpacing=0
				style=" width:700px; height:130px; top:200; overflow:hidden;"></IFRAME>
		</div>
	</div>
	<form name="form2" id="form2" method="post" action="">
		<?php
		if ($_POST['oculto'] == '') {
			$_POST['tabgroup1'] = 1;
			$sqlr = "SELECT orden FROM configbasica";
			$resp = mysqli_query($linkbd, $sqlr);
			$row = mysqli_fetch_row($resp);
			$_POST['orden'] = substr($row[0], 0, 1);
			$sqlr = "SELECT MAX(anio) FROM admbloqueoanio WHERE bloqueado = 'N'";
			$resp = mysqli_query($linkbd, $sqlr);
			$row = mysqli_fetch_row($resp);
			$_POST['vigencia'] = $row[0];
			echo"
				<script>
					for (let i = 0; i < localStorage.length; i++) {
						const key = localStorage.key(i);
						if (key.startsWith('estadoInputs-')) {
						localStorage.removeItem(key);
						i--;
						}
					}
				</script>";
		}
		$vigusu = $_POST['vigencia'];
		if ($_POST['brp'] == '') {
			$_POST['numero'] = selconsecutivo('ccpetdc', 'id');
		}
		if ($_POST['brp'] == '1') {
			$sqldes = "SELECT 1 FROM ccpetdc WHERE consvigencia = '" . $_POST['rp'] . "' AND vigencia = '" . $_POST['vigencia'] . "'";
			$resdes = mysqli_query($linkbd, $sqldes);
			$todes = mysqli_num_rows($resdes);
			if ($todes == 0) {
				$saldoRP = generaSaldoRPccpet($_POST['rp'], $vigusu);
				if ($saldoRP > 0) {
					$sqlBusq1 = "SELECT 'SI' FROM ccpetdc WHERE consvigencia = '" . $_POST['rp'] . "' AND vigencia = '$vigusu'";
					$resBusq1 = mysqli_query($linkbd, $sqlBusq1);
					$rowBusq1 = mysqli_fetch_row($resBusq1);
					$sqlBusq2 = "SELECT 'SI' FROM hum_nom_cdp_rp WHERE rp = '" . $_POST['rp'] . "' AND vigencia = '$vigusu'";
					$resBusq2 = mysqli_query($linkbd, $sqlBusq2);
					$rowBusq2 = mysqli_fetch_row($resBusq2);
					if ($rowBusq1[0] != 'SI' && $rowBusq2[0] != 'SI') {
						$sqlrp = "SELECT fecha, vigenciacdp, contrato, idcdp, tercero, valor, saldo, estado, tipo_mov FROM ccpetrp WHERE consvigencia = " . $_POST['rp'] . " AND vigencia = '$vigusu'";
						$resrp = mysqli_query($linkbd, $sqlrp);
						$rowrp = mysqli_fetch_row($resrp);
						$_POST['fecha'] = $rowrp[0];
						//$_POST['vigencia'] = $rowrp[1];
						$_POST['contrato'] = $rowrp[2];
						$_POST['idcdp'] = $rowrp[3];
						$_POST['ntercero'] = $rowrp[4];
						$_POST['valorrp'] = $rowrp[5];
						$_POST['saldo'] = $saldoRP;
						$_POST['tipomov'] = $rowrp[8];
						switch ($rowrp[7]) {
							case 'S':
								$_POST['estado'] = 'ACTIVO';
								$color = " style='background-color:#0CD02A; width:98%;height:30px; color:#fff'";
								break;
							case 'C':
								$_POST['estado'] = 'COMPLETO';
								$color = " style='background-color:#00CCFF; width:98%;height:30px; color:#fff'";
								break;
							case 'N':
								$_POST['estado'] = 'ANULADO';
								$color = " style='background-color:#aa0000; width:98%;height:30px; color:#fff'";
								break;
							case 'R':
								$_POST['estado'] = 'REVERSADO';
								$color = " style='background-color:#aa0000; width:98%;height:30px; color:#fff'";
								break;
						}
						$sqlrcdp = "SELECT solicita, objeto FROM ccpetcdp WHERE consvigencia = '$rowrp[3]' AND vigencia = '$vigusu'";
						$rescdp = mysqli_query($linkbd, $sqlrcdp);
						$rowcdp = mysqli_fetch_row($rescdp);
						$_POST['solicita'] = $rowcdp[0];
						$_POST['objeto'] = str_replace("'", "", $rowcdp[1]);
						$sqlrter = "SELECT nombre1, nombre2, apellido1, apellido2, razonsocial FROM terceros WHERE cedulanit = '" . $_POST['ntercero'] . "' ";
						$rester = mysqli_query($linkbd, $sqlrter);
						$rowter = mysqli_fetch_row($rester);
						if ($rowter[4] != "") {
							$_POST['nomtercero'] = $rowter[4];
						} else {
							$_POST['nomtercero'] = $rowter[0] . " " . $rowter[1] . " " . $rowter[2] . " " . $rowter[3];
						}
						unset($_POST['codVigenciag'], $_POST['nomVigenciag'], $_POST['seccion_presupuestal'], $_POST['nomSeccion_presupuestal'], $_POST['bpim'], $_POST['codCuenta'], $_POST['nomCuenta'], $_POST['codFuente'], $_POST['codFuente'], $_POST['nomFuente'], $_POST['codProductos'], $_POST['nomProductos'], $_POST['codIndicador'], $_POST['nomIndicador'], $_POST['codPoliticap'], $_POST['nomPoliticap'], $_POST['medioPago'], $_POST['valor'], $_POST['iddestcompra'], $_POST['codsector'], $_POST['nomsector'], $_POST['cuentaDebito'], $_POST['cuentaCredito']);
						$sqlrcdp = "SELECT solicita, objeto FROM ccpetcdp WHERE id_cdp = $rowrp[3]";
						$_POST['codVigenciag'] = $_POST['seccion_presupuestal'] = $_POST['nomSeccion_presupuestal'] = $_POST['bpim'] = $_POST['nomVigenciag'] = $_POST['codCuenta'] = $_POST['nomCuenta'] = $_POST['codFuente'] = $_POST['nomFuente'] = $_POST['codProductos'] = $_POST['nomProductos'] = $_POST['codIndicador'] = $_POST['nomIndicador'] = $_POST['codPoliticap'] = $_POST['nomPoliticap'] = $_POST['medioPago'] = $_POST['valor'] = $_POST['iddestcompra'] = $_POST['codsector'] = $_POST['nomsector'] = $_POST['cuentaDebito'] = $_POST['cuentaDebito'] = array();
						$d = 0;
						$sqlrdet = "SELECT * FROM ccpetrp_detalle WHERE consvigencia = '" . $_POST['rp'] . "' AND vigencia = '$vigusu'";
						$resdet = mysqli_query($linkbd, $sqlrdet);
						while ($rowdet = mysqli_fetch_row($resdet)) {
							$sqlcodVigenciag = " SELECT nombre FROM ccpet_vigenciadelgasto WHERE codigo = '$rowdet[14]'";
							$rescodVigenciag = mysqli_query($linkbd, $sqlcodVigenciag);
							$rowcodVigenciag = mysqli_fetch_row($rescodVigenciag);
							$_POST['nomVigenciag'][] = $rowcodVigenciag[0];
							$_POST['codVigenciag'][] = $rowdet[14];

							$sec_presu = intval($rowdet[17]);
							$sqlSeccion_presupuestal = "SELECT nombre FROM pptoseccion_presupuestal WHERE (id_seccion_presupuestal = $/* *|MARCADOR_CURSOR|* */
									sec_presu OR id_seccion_presupuestal = $rowdet[17])";
							$resSeccion_presupuestal = mysqli_query($linkbd, $sqlSeccion_presupuestal);
							$rowSeccion_presupuestal = mysqli_fetch_row($resSeccion_presupuestal);
							$_POST['seccion_presupuestal'][] = $rowdet[17];
							$_POST['nomSeccion_presupuestal'][] = $rowSeccion_presupuestal[0];

							$_POST['bpim'][] = $rowdet[16];

							$_POST['codCuenta'][] = $rowdet[3];
							$maxVersion = ultimaVersionGastosCCPET();
							$nomCuentares = buscacuentaccpetgastos($rowdet[3], $maxVersion);
							$_POST['nomCuenta'][] = $nomCuentares;
							$_POST['codFuente'][] = $rowdet[5];
							$nomFuenteres = buscafuenteccpet($rowdet[5]);
							$_POST['nomFuente'][] = $nomFuenteres;
							$_POST['codIndicador'][] = $rowdet[11];
							$nomIndicadorres = buscaindicadorccpet($rowdet[11]);
							$_POST['nomIndicador'][] = $nomIndicadorres;
							$_POST['codProductos'][] = $rowdet[4];
							$nomProductosres = buscaservicioccpetgastos($rowdet[4]);
							if ($nomProductosres == '') {
								$nomProductosres = buscaproductoccpetgastos($rowdet[4]);
							}
							$_POST['nomProductos'][] = $nomProductosres;
							$sqlcodPoliticap = " SELECT nombre FROM ccpet_politicapublica WHERE codigo = '$rowdet[15]'";
							$rescodPoliticap = mysqli_query($linkbd, $sqlcodPoliticap);
							$rowcodPoliticap = mysqli_fetch_row($rescodPoliticap);
							$_POST['nomPoliticap'][] = $rowcodPoliticap[0];
							$_POST['codPoliticap'][] = $rowdet[15];
							$_POST['medioPago'][] = $rowdet[12];
							$_POST['valor'][] = $rowdet[6];
							$_POST["destcompra$d"] = $rowEdiDet[16];
							$_POST['iddestcompra'][] = $rowEdiDet[0];
							$_POST['codsector'][] = substr($rowdet[11], 0, 2);
							$sqlnomsector = " SELECT nombre FROM ccpetsectores WHERE codigo = '" . substr($rowdet[11], 0, 2) . "'";
							$resnomsector = mysqli_query($linkbd, $sqlnomsector);
							$rownomsector = mysqli_fetch_row($resnomsector);
							$_POST['nomsector'][] = $rownomsector[0];
							$_POST["idswsino$d"] = 'checked';
							$d++;
						}
					} else {
						if ($rowBusq1[0] == 'SI') {
							echo "<script>despliegamodalm('visible','2','Este rp ya ha sido creado');</script>";
						} else {
							echo "<script>despliegamodalm('visible','2','selecciono un RP de nomina');</script>";
						}
						unset($_POST['codVigenciag'], $_POST['nomVigenciag'], $_POST['seccion_presupuestal'], $_POST['nomSeccion_presupuestal'], $_POST['bpim'], $_POST['codCuenta'], $_POST['nomCuenta'], $_POST['codFuente'], $_POST['nomFuente'], $_POST['codFuente'], $_POST['codProductos'], $_POST['nomProductos'], $_POST['codIndicador'], $_POST['nomIndicador'], $_POST['codPoliticap'], $_POST['nomPoliticap'], $_POST['medioPago'], $_POST['valor'], $_POST['letras']);
						$_POST['fecha'] = "";
						$_POST['contrato'] = "";
						$_POST['idcdp'] = "";
						$_POST['ntercero'] = "";
						$_POST['valorrp'] = "";
						$_POST['saldo'] = "";
						$_POST['tipomov'] = "";
						$_POST['solicita'] = "";
						$_POST['objeto'] = "";
						$_POST['estado'] = "";
						$_POST['nomtercero'] = "";
					}
				} else {
					echo "<script>despliegamodalm('visible','5','El RP No. " . $_POST['rp'] . " no tiene saldo','1');</script>";
				}
			} else {
				echo "<script>despliegamodalm('visible','5','El RP No. " . $_POST['rp'] . " ya se encuentra radicado','1');</script>";
			}
		}
		switch ($_POST['tabgroup1']) {
			case 1:
				$check1 = 'checked';
				break;
			case 2:
				$check2 = 'checked';
				break;
			case 3:
				$check3 = 'checked';
				break;
			case 4:
				$check4 = 'checked';
				break;
		}
		$co = "zebra1";
		$co2 = "zebra2";
		?>
		<input type="hidden" id="orden" name="orden" value="<?php echo $_POST['orden']; ?>">
		<div class="tabs" style="height:74%; width:99.7%">
			<div class="tab">
				<input type="radio" id="tab-1" name="tabgroup1" value="1" <?php echo $check1; ?> />
				<label id="clabel" for="tab-1">Radicar Destino de Compra</label>
				<div class="content" style="overflow:hidden;">
					<table class="inicio" width="99.6%">
						<tr>
							<td class="titulos" colspan="10">.: Radicar Destino de Compra </td>
							<td class="cerrar" style='width:7%' onClick="location.href='cont-principal.php'">Cerrar</td>
						</tr>
						<tr>
							<td class="tamano01" style="width:3cm;">N&uacute;mero:</td>
							<td style="width:9%"><input type="text" name="numero" id="numero"
									value="<?php echo $_POST['numero'] ?>"
									style="text-align:center;width:98%;height:30px;"
									onKeyUp="return tabular(event,this)" readonly></td>
							<td class="saludo1" style="width:3cm;">RP:</td>
							<td style="width:12%;"><input class="colordobleclik" type="text" name="rp" id="rp"
									onKeyUp="return tabular(event,this)" value="<?php echo $_POST['rp']; ?>"
									onChange="buscarp(event)" style="width:98%;height:30px;" title="Listado RP"
									onDblClick="despliegamodal2('visible','1');" autocomplete="off"></td>
							<td class="tamano01" style="width:2cm;">Fecha:</td>
							<td style="width:10%;"><input name="fecha" type="text" id="fc_1198971545"
									value="<?php echo $_POST['fecha']; ?>" style="width:98%;height:30px;" readonly></td>
							<td class="tamano01" style="width:3cm;">Vigencia:</td>
							<td style="width:8%;">
								<select name="vigencia" id="vigencia" style="width:98%;height:30px;">
									<option value="" disabled="disabled">Año</option>
									<?php
									$sqlr = "SELECT anio FROM admbloqueoanio WHERE bloqueado = 'N' ORDER BY anio DESC";
									$resp = mysqli_query($linkbd, $sqlr);
									while ($row = mysqli_fetch_row($resp)) {
										if ($_POST['vigencia'] == $row[0]) {
											echo "<option value='$row[0]' selected>$row[0]</option>";
										} else {
											echo "<option value='$row[0]'>$row[0]</option>";
										}
									}
									?>
								</select>
							</td>
							<td class="tamano01" style="width:2cm;">Contrato:</td>
							<td><input id="contrato" type="text" name="contrato" onKeyUp="return tabular(event,this)"
									onKeyPress="javascript:return solonumeros(event)"
									value="<?php echo $_POST['contrato'] ?>" style="width:100%;height:30px;" readonly>
							</td>
						</tr>
						<tr>
							<td class="tamano01">Numero CDP:</td>
							<td><input name="idcdp" type="text" id="idcdp" onKeyUp="return tabular(event,this)"
									value="<?php echo $_POST['idcdp'] ?>" style="width:98%;height:30px;" readonly></td>
							<td class="tamano01">Tercero:</td>
							<td>
								<input type="text" name="ntercero" id="ntercero"
									value="<?php echo $_POST['ntercero'] ?>" style="width:98%;height:30px;" readonly>
							</td>
							<td colspan="6">
								<input type="text" name="nomtercero" id="nomtercero"
									value="<?php echo $_POST['nomtercero'] ?>" style="width:100%;height:30px;" readonly>
							</td>
						</tr>
						<tr>
							<td class="tamano01">Solicita:</td>
							<td colspan="3"><input type="text" name="solicita" id="solicita"
									onKeyUp="return tabular(event,this)" value="<?php echo $_POST['solicita'] ?>"
									style="width:99%;height:30px;" readonly></td>
							<td class="tamano01">Objeto:</td>
							<td colspan='5'><input type="text" name="objeto" id="objeto"
									onKeyUp="return tabular(event,this)" value="<?php echo $_POST['objeto'] ?>"
									style="width:100%;height:30px;" readonly> </td>
						</tr>
						<tr>
							<td class="tamano01">Valor RP:</td>
							<td><input name="valorrp" type="text" value="<?php echo $_POST['valorrp'] ?>"
									style="width:98%;height:30px;" readonly></td>
							<td class="tamano01">Saldo:</td>
							<td><input name="saldo" type="text" value="<?php echo $_POST['saldo'] ?>"
									style="width:98%;height:30px;" readonly></td>
							<td class="tamano01">Estado:</td>
							<td><input name="estado" type="text" id="estado" value="<?php echo $_POST['estado'] ?>"
									readonly <?php echo $color; ?> style="width:98%;height:30px;"></td>
							<td class="tamano01">Tipo movimiento:</td>
							<td><input name="tipomov" type="text" value="<?php echo $_POST['tipomov'] ?>"
									style="width:98%;height:30px;" readonly></td>
						</tr>
					</table>
					<input type="hidden" value="0" name="brp">
					<input type="hidden" name="oculto" id="oculto" value="1">
					<div class="subpantalla" style="height:62%; width:99.6%; overflow-x:hidden;">
						<?php
						echo "
								<table class='inicio' width='99%'>
									<tr><td class='titulos' colspan='12'>.: Detalle destino de comprar</td></tr>
									<tr class='titulos2'>
										<td style='text-align:center;'>Vigencia del Gasto</td>
										<td style='text-align:center;'>Sec. presupuestal</td>
										<td style='text-align:center;'>BPIM</td>
										<td style='width:10%;text-align:center;'>Cuenta</td>
										<td style='text-align:center;'>Nombre Cuenta</td>
										<td style='text-align:center;'>Fuente</td>
										<td style='text-align:center;'>Producto/Servicio</td>
										<td style='width:10%;text-align:center;'>Indicador Producto</td>
										<td style='width:10%';text-align:center;>Pol&iacute;tica P&uacute;plica</td>
										<td style='text-align:center;'>Medio de Pago</td>
										<td style='width:10%;text-align:center;'>Valor</td>
										<td style='width:5%;text-align:center;'>Destino de compra</td>
									</tr>";
						$_POST['cuentagas'] = 0;
						for ($x = 0; $x < count($_POST['nomCuenta']); $x++) {
							echo "
									<tr class='$co'>
										<input type='hidden' name='codVigenciag[]' value='" . $_POST['codVigenciag'][$x] . "'>
										<input type='hidden' name='nomVigenciag[]' value='" . $_POST['nomVigenciag'][$x] . "'>
										<input type='hidden' name='seccion_presupuestal[]' value='" . $_POST['seccion_presupuestal'][$x] . "'>
										<input type='hidden' name='nomSeccion_presupuestal[]' value='" . $_POST['nomSeccion_presupuestal'][$x] . "'>
										<input type='hidden' name='bpim[]' value='" . $_POST['bpim'][$x] . "'>
										<input type='hidden' name='codCuenta[]' value='" . $_POST['codCuenta'][$x] . "' >
										<input type='hidden' name='nomCuenta[]' value='" . $_POST['nomCuenta'][$x] . "'>
										<input type='hidden' name='codFuente[]' value='" . $_POST['codFuente'][$x] . "'>
										<input type='hidden' name='nomFuente[]' value='" . $_POST['nomFuente'][$x] . "'>
										<input type='hidden' name='codProductos[]' value='" . $_POST['codProductos'][$x] . "'>
										<input type='hidden' name='nomProductos[]' value='" . $_POST['nomProductos'][$x] . "'>
										<input type='hidden' name='codIndicador[]' value='" . $_POST['codIndicador'][$x] . "'>
										<input type='hidden' name='nomIndicador[]' value='" . $_POST['nomIndicador'][$x] . "'>
										<input type='hidden' name='codPoliticap[]' value='" . $_POST['codPoliticap'][$x] . "'>
										<input type='hidden' name='nomPoliticap[]' value='" . $_POST['nomPoliticap'][$x] . "'>
										<input type='hidden' name='medioPago[]' value='" . $_POST['medioPago'][$x] . "'>
										<input type='hidden' name='valor[]' value='" . $_POST['valor'][$x] . "'>
										<input type='hidden' name='codsector[]' value='" . $_POST['codsector'][$x] . "'>
										<input type='hidden' name='nomsector[]' value='" . $_POST['nomsector'][$x] . "'>
										<td style='width=10%'>" . $_POST['codVigenciag'][$x] . " - " . $_POST['nomVigenciag'][$x] . "</td>
										<td style='width=10%'>" . $_POST['seccion_presupuestal'][$x] . " - " . $_POST['nomSeccion_presupuestal'][$x] . "</td>
										<td style='width=10%'>" . $_POST['bpim'][$x] . "</td>
										<td style='width=10%'>" . $_POST['codCuenta'][$x] . "</td>
										<td style='width=10%'>" . $_POST['nomCuenta'][$x] . "</td>
										<td style='width=10%'>" . $_POST['codFuente'][$x] . " - " . $_POST['nomFuente'][$x] . "</td>
										<td style='width=10%'>" . $_POST['codProductos'][$x] . " - " . $_POST['nomProductos'][$x] . "</td>
										<td style='width=10%'>" . $_POST['codIndicador'][$x] . " - " . $_POST['nomIndicador'][$x] . "</td>
										<td style='width=10%'>" . $_POST['codPoliticap'][$x] . " - " . $_POST['nomPoliticap'][$x] . "</td>
										<td style='width=10%'>" . $_POST['medioPago'][$x] . "</td>
										<td style='text-align:right;'>$ " . number_format($_POST['valor'][$x], 2, $_SESSION["spdecimal"], $_SESSION["spmillares"]) . "</td>
										<td>
											<select name='destcompra$x' onChange=\"limpiar01('$x');\">
												<option value=''>Seleccionar Destino de Compra</option>
									";
							$sqldest = "SELECT * FROM almdestinocompra WHERE estado='S' ORDER BY codigo";
							$resdest = mysqli_query($linkbd, $sqldest);

							while ($rowdest = mysqli_fetch_row($resdest)) {
								if ($_POST["destcompra$x"] == $rowdest[0]) {
									echo "<option value='$rowdest[0]' SELECTED>$rowdest[1]</option>";
								} else {
									echo "<option value='$rowdest[0]'>$rowdest[1]</option>";
								}
							}
							echo "
											</select>
										</td>
									</tr>";
							$gas = $_POST['valor'][$x];
							$_POST['cuentagas'] = $_POST['cuentagas'] + $gas;
							$resultado = convertir($_POST['cuentagas']);
							$_POST['letras'] = $resultado . " PESOS";
							$aux = $co;
							$co = $co2;
							$co2 = $aux;
						}
						echo "
									<input type='hidden' name='cuentagas' id='cuentagas' value='" . $_POST['cuentagas'] . "'/>
									<input type='hidden' name='cuentagas2' id='cuentagas2' value='" . $_POST['cuentagas2'] . "'/>
									<input type='hidden'  name='letras' id='letras' value='" . $_POST['letras'] . "'/>
									<tr class='$co' style='text-align:right;'>
										<td colspan='10'>Total:</td>
										<td>$ " . number_format($_POST['cuentagas'], 2, $_SESSION["spdecimal"], $_SESSION["spmillares"]) . "</td>
									</tr>
									<tr>
										<td class='saludo1'>Son:</td>
										<td class='saludo1' colspan= '11'>" . $_POST['letras'] . "</td>
									</tr>
								</table>";
						?>
					</div>
				</div>
			</div>
			<div class="tab">
				<input type="radio" id="tab-2" name="tabgroup1" value="2" <?php echo $check2; ?> />
				<label id="clabel" for="tab-2">Radicar Programaci&oacute;n Contable</label>
				<div class="content" style="overflow-x:hidden;">
					<?php
					echo "
							<table class='inicio' width='99%'>
								<tr>
									<td class='titulos' colspan='15'>.: Detalle destino de comprar</td>
									<td class='cerrar' style='width:7%' onClick=\"location.href='ccp-principal.php'\">Cerrar</td>
								</tr>
								<tr class='titulos2'>
									<td style='text-align:center;'>Vigencia del Gasto</td>
									<td style='text-align:center;'>Sec. presupuestal</td>
									<td style='text-align:center;'>BPIM</td>
									<td style='width:10%;text-align:center;'>Cuenta</td>
									<td style='text-align:center;'>Nombre Cuenta</td>
									<td style='text-align:center;'>Sector</td>
									<td style='text-align:center;'>Fuente</td>
									<td style='text-align:center;'>Producto/Servicio</td>
									<td style='width:10%;text-align:center;'>Indicador Producto</td>
									<td style='width:10%;text-align:center;'>Pol&iacute;tica P&uacute;plica</td>
									<td style='text-align:center;'>Medio de Pago</td>
									<td style='width:10%;text-align:center;'>Valor</td>
									<td style='text-align:center;'>Cuenta debito</td>
									<td style='text-align:center;' colspan='2'>Cuenta Credito</td>
									<td style='text-align:center;' ></td>
								</tr>";
					$_POST['cuentagas'] = 0;
					for ($x = 0; $x < count($_POST['nomCuenta']); $x++) {
						echo "
								<tr class='$co'>
									<td style='width=10%'>" . $_POST['codVigenciag'][$x] . " - " . $_POST['nomVigenciag'][$x] . "</td>
									<td style='width=10%'>" . $_POST['seccion_presupuestal'][$x] . " - " . $_POST['nomSeccion_presupuestal'][$x] . "</td>
									<td style='width=10%'>" . $_POST['bpim'][$x] . "</td>
									<td style='width=10%'>" . $_POST['codCuenta'][$x] . "</td>
									<td style='width=10%'>" . $_POST['nomCuenta'][$x] . "</td>
									<td style='width=10%'>" . $_POST['codsector'][$x] . " - " . $_POST['nomsector'][$x] . "</td>
									<td style='width=10%'>" . $_POST['codFuente'][$x] . " - " . $_POST['nomFuente'][$x] . "</td>
									<td style='width=10%'>" . $_POST['codProductos'][$x] . " - " . $_POST['nomProductos'][$x] . "</td>
									<td style='width=10%'>" . $_POST['codIndicador'][$x] . " - " . $_POST['nomIndicador'][$x] . "</td>
									<td style='width=10%'>" . $_POST['codPoliticap'][$x] . " - " . $_POST['nomPoliticap'][$x] . "</td>
									<td style='width=10%'>" . $_POST['medioPago'][$x] . "</td>
									<td style='text-align:right;'>$ " . number_format($_POST['valor'][$x], 2, $_SESSION["spdecimal"], $_SESSION["spmillares"]) . "</td>
									<td style='width:10%;' id='td-debito-$x'>
										<input class='colordobleclik' name='cuentaDebito[]' value='" . $_POST['cuentaDebito'][$x] . "' style='height:30px;font-weight: bold !important;' onClick=\"despliegamodal2('visible','2','" . $_POST['codCuenta'][$x] . "','" . $x . "', '" . $_POST['codsector'][$x] . "','" . $_POST["destcompra$x"] . "','" . $_POST["seccion_presupuestal"][$x] . "')\" readonly>
									</td>
									<td colspan='2' style='width:10%;' id='td-credito-$x'>
										<input class='colordobleclik' name='cuentaCredito[]' value='" . $_POST['cuentaCredito'][$x] . "' style='height:30px;font-weight: bold !important;' onClick=\"despliegamodal2('visible','2','" . $_POST['codCuenta'][$x] . "','" . $x . "', '" . $_POST['codsector'][$x] . "','" . $_POST["destcompra$x"] . "','" . $_POST["seccion_presupuestal"][$x] . "')\" readonly>
									</td>
									<td style='width:10%; text-align: center; vertical-align: middle;'>
										<img src='imagenes/plus.gif' alt='Agregar' style='cursor: pointer; width: 20px; height: 20px; margin-right: 5px;' onClick='agregarcampo($x);' />
										<img src='imagenes/minus.gif' alt='Quitar' style='cursor: pointer; width: 20px; height: 20px;' onClick='quitarcampo($x);' />
									</td>
								</tr>";
						$gas = $_POST['valor'][$x];
						$_POST['cuentagas'] = $_POST['cuentagas'] + $gas;
						$resultado = convertir($_POST['cuentagas']);
						$_POST['letras'] = $resultado . " PESOS";
						$aux = $co;
						$co = $co2;
						$co2 = $aux;
					}
					echo "
								<input type='hidden' name='cuentagas' id='cuentagas'  value='" . $_POST['cuentagas'] . "'>
								<input type='hidden' name='cuentagas2' id='cuentagas2' value='" . $_POST['cuentagas2'] . "'>
								<input type='hidden'  name='letras' id='letras' value='" . $_POST['letras'] . "'>
								<tr class='$co' style='text-align:right;'>
									<td colspan='11'>Total:</td>
									<td>$ " . number_format($_POST['cuentagas'], 2, $_SESSION["spdecimal"], $_SESSION["spmillares"]) . "</td>
								</tr>
								<tr>
									<td class='saludo1'>Son:</td>
									<td class='saludo1' colspan= '11'>" . $_POST['letras'] . "</td>
								</tr>
							</table>";
					if ($_POST['cuendevcre'] != '') {
						$xyz = $_POST['cuendevcre'];
						$sqltem = "SELECT id_cuenta FROM ccpet_cuentasectores WHERE id_sector = '" . $_POST['codsector'][$xyz] . "'";
						$restem = mysqli_query($linkbd, $sqltem);
						$rowtem = mysqli_fetch_row($restem);
						$temsector = $rowtem[0];
						$sqltem = "SELECT concepto_cont, tipo FROM ccpetcuentas_concepto_contable WHERE cuenta_p = '" . $_POST['codCuenta'][$xyz] . "' AND estado = 'S'";
						$restem = mysqli_query($linkbd, $sqltem);
						$rowtem = mysqli_fetch_row($restem);
						$temconcepto = $rowtem[0];
						$temtipo = $rowtem[1];
						if (substr($_POST['codCuenta'][$xyz], 0, 5) == '2.1.1') {
							$sqltem = "SELECT cuenta, debito, credito, cc FROM conceptoscontables_det WHERE codigo = '$temconcepto' AND tipo = '$temtipo' AND destino_compra = '" . $_POST["destcompra$xyz"] . "'";
							$restem = mysqli_query($linkbd, $sqltem);
							$cuentasDebito = array();
							$cuentasCredito = array();
							while ($rowtem = mysqli_fetch_row($restem)) {
								if ($rowtem[1] == 'S') {
									$cuentasDebito[] = $rowtem[0];
								} else {
									$cuentasCredito[] = $rowtem[0];
								}
							}
							if (count($cuentasDebito) == 1) {
								echo "
										<script>
											document.getElementsByName('cuentaDebito[]').item('$xyz').value = '$cuentasDebito[0]';
											document.getElementsByName('cuentaCredito[]').item('$xyz').value = '$cuentasCredito[0]';
										</script>";
							}
						} else if (substr($_POST['codCuenta'][$xyz], 0, 5) == '2.3.1') {
							$sqltem = "SELECT cuenta, debito, credito, cc FROM conceptoscontables_det WHERE codigo = '$temconcepto' AND tipo = '$temtipo' AND destino_compra = '" . $_POST["destcompra$xyz"] . "'";
							$restem = mysqli_query($linkbd, $sqltem);
							$cuentasDebito = array();
							$cuentasCredito = array();
							;
							while ($rowtem = mysqli_fetch_row($restem)) {
								if ($rowtem[1] == 'S') {
									$cuentasDebito[] = $rowtem[0];
								} else {
									$cuentasCredito[] = $rowtem[0];
								}
							}
							if (count($cuentasDebito) == 1) {
								echo "
										<script>
											document.getElementsByName('cuentaDebito[]').item('$xyz').value = '$cuentasDebito[0]';
											document.getElementsByName('cuentaCredito[]').item('$xyz').value = '$cuentasCredito[0]';
										</script>";
							}

						} else {
							$sqltem = "SELECT cuenta, asociado_a, cc FROM conceptoscontables_det WHERE codigo = '$temconcepto' AND tipo = '$temtipo' AND debito = 'S' AND destino_compra = '" . $_POST["destcompra$xyz"] . "'";
							$restem = mysqli_query($linkbd, $sqltem);
							$cuentasDebito = array();
							$cuentasCredito = array();
							while ($rowtem = mysqli_fetch_row($restem)) {
								if (substr($rowtem[0], 0, 4) == $temsector || substr($_POST['codCuenta'][$xyz], 0, 3) != '2.3') {
									$cuentasDebito[] = $rowtem[0];
									$cuentasCredito[] = $rowtem[1];
								}
							}
							if (count($cuentasDebito) == 1) {
								echo "
										<script>
											document.getElementsByName('cuentaDebito[]').item('$xyz').value = '$cuentasDebito[0]';
											document.getElementsByName('cuentaCredito[]').item('$xyz').value = '$cuentasCredito[0]';
										</script>";
							}
						}
					}
					?>
				</div>
			</div>
			<div class="tab">
				<input type="radio" id="tab-3" name="tabgroup1" value="3" <?php echo $check3; ?> />
				<label id="clabel" for="tab-3">Retenciones</label>
				<div class="content" style="overflow-x:hidden;">
					<table class="inicio" align="center">
						<tr>
							<td class="titulos" colspan="10">.: Retenciones</td>
							<td class="cerrar" style='width:7%' onClick="location.href='cont-principal.php'">Cerrar</td>
						</tr>
						<tr>
							<td class="tamano01" style="width:2.5cm;">Retencion y Descuento:</td>
							<td>
								<select name="retencion" id="retencion" onChange="document.form2.submit();"
									onKeyUp="return tabular(event,this)" style="width:98%;height:30px;">
									<option value="">Seleccione ...</option>
									<option value="00" <?php if ($_POST['retencion'] == '00') {
										echo "SELECTED";
									} ?>>NO APLICA
									</option>
									<?php
									$sqlr = "SELECT * FROM tesoretenciones WHERE estado='S'";
									$res = mysqli_query($linkbd, $sqlr);
									while ($row = mysqli_fetch_row($res)) {
										if ("$row[0]" == $_POST['retencion']) {
											echo "<option value='$row[0]' SELECTED>$row[1] - $row[2]</option>";
											$_POST['porcentaje'] = $row[5];
											$_POST['porcentajex'] = $row[5] . '%';
											$_POST['nretencion'] = $row[1] . " - " . $row[2];
											$_POST['permisoac'] = 0;
											if ($row[3] == 'S')// Simple (S) o Compuesto (C)
											{
												if ($row[6] == '1')//Tercero si (1)
												{
													if ($row[8] == $_POST['orden'])//Municipal(M), Nacional(N) y Departamental(D)
													{
														echo "<script>despliegamodalm('visible','2','Se debe correguir el orden en retenciones de pagos se marco como tercero y el Destino es igual a la entidad');</script>";
														$_POST['permisoac'] = 1;
														$_POST['vporcentaje'] = 0;
													} else {
														$sqlr1 = "SELECT conceptocausa, conceptoingreso, conceptosgr, id FROM tesoretenciones_det WHERE codigo = '$row[0]'";
														$res1 = mysqli_query($linkbd, $sqlr1);
														$row1 = mysqli_fetch_row($res1);
														if (($row1[0] == '') || ($row1[1] == '') || ($row1[2] == '')) {
															echo "<script>despliegamodalm('visible','2','Se debe correguir el orden en retenciones de pagos faltan uno o mas conceptos por agregar');</script>";
															$_POST['permisoac'] = 1;
															$_POST['vporcentaje'] = 0;
														}
														/* else
																										{
																											$sqlr2="SELECT cuentapres FROM tesoretenciones_det_presu WHERE id_retencion = '$row1[3]' AND codigo = '$row[0]'";
																											$res2 = mysqli_query($linkbd, $sqlr2);
																											$row2 = mysqli_fetch_row($res2);
																											if($row2[0] == '')
																											{
																												echo "<script>despliegamodalm('visible','2','Se debe correguir el orden en retenciones de pagos falta agregar la cuenta presupuestal');</script>";
																												$_POST['permisoac'] = 1;
																												$_POST['vporcentaje'] = 0;
																											}
																										} */

													}
												} else {
													if ($row[8] != $_POST['orden'])//Municipal(M), Nacional(N) y Departamental(D)
													{
														echo "<script>despliegamodalm('visible','2','Se debe correguir el orden en retenciones de pagos no se marco como tercero y el Destino diferente a la entidad');</script>";
														$_POST['permisoac'] = 1;
														$_POST['vporcentaje'] = 0;
													} else {
														$sqlr1 = "SELECT conceptocausa, conceptoingreso, conceptosgr, id FROM tesoretenciones_det WHERE codigo = '$row[0]'";
														$res1 = mysqli_query($linkbd, $sqlr1);
														$row1 = mysqli_fetch_row($res1);
														if (($row1[0] == '') || ($row1[1] == '') || ($row1[2] == '')) {
															echo "<script>despliegamodalm('visible','2','Se debe correguir el orden en retenciones de pagos faltan uno o mas conceptos por agregar');</script>";
															$_POST['permisoac'] = 1;
															$_POST['vporcentaje'] = 0;
														}
														/* else
														{
															$sqlr2="SELECT cuentapres FROM tesoretenciones_det_presu WHERE id_retencion = '$row1[3]' AND codigo = '$row[0]'";
															$res2 = mysqli_query($linkbd, $sqlr2);
															$row2 = mysqli_fetch_row($res2);
															if($row2[0] == '')
															{
																echo "<script>despliegamodalm('visible','2','Se debe correguir el orden en retenciones de pagos falta agregar la cuenta presupuestal');</script>";
																$_POST['permisoac'] = 1;
																$_POST['vporcentaje'] = 0;
															}
														} */
													}
												}
											} else {
												if ($row[6] == '1')//Tercero si (1)
												{
													$sipi = 0;
													$sqlr1 = "SELECT conceptocausa, conceptoingreso, conceptosgr, id, destino FROM tesoretenciones_det WHERE codigo = '$row[0]'";
													$res1 = mysqli_query($linkbd, $sqlr1);
													$totalcli = mysqli_num_rows($res1);
													if ($totalcli == 2) {
														while ($row1 = mysqli_fetch_row($res1)) {
															if (($row1[0] == '') || ($row1[1] == '') || ($row1[2] == '')) {
																if ($_POST['permisoac'] != 1) {
																	echo "<script>despliegamodalm('visible','2','Se debe correguir el orden en retenciones de pagos faltan uno o mas conceptos por agregar');</script>";
																	$_POST['permisoac'] = 1;
																	$_POST['vporcentaje'] = 0;
																}
															}
															/* else
															{
																if($row1[4] == $_POST['orden'])
																{
																	$sipi++;
																	$sqlr2="SELECT cuentapres FROM tesoretenciones_det_presu WHERE id_retencion = '$row2[3]' AND codigo = '$row[0]'";
																	$res2 = mysqli_query($linkbd, $sqlr2);
																	$row2 = mysqli_fetch_row($res2);
																	if($row2[0] == '')
																	{
																		if( $_POST['permisoac'] != 1)
																		{
																			echo "<script>despliegamodalm('visible','2','Se debe correguir el orden en retenciones de pagos falta agregar la cuenta presupuestal');</script>";
																			$_POST['permisoac'] = 1;
																			$_POST['vporcentaje'] = 0;
																		}
																	}
																}
															} */
														}
														if ($sipi != 0 && $_POST['permisoac'] == 0) {
															echo "<script>despliegamodalm('visible','2','Se debe correguir el orden en retenciones de pagos ya que es de tipo compuesto y no se agrego nin gun destino igual a la entidad');</script>";
															$_POST['permisoac'] = 1;
															$_POST['vporcentaje'] = 0;
														}
													} else {
														echo "<script>despliegamodalm('visible','2','Se debe correguir el orden en retenciones de pagos debe contener minimo dos opciones agregadas');</script>";
														$_POST['permisoac'] = 1;
														$_POST['vporcentaje'] = 0;
													}
												} else {
													echo "<script>despliegamodalm('visible','2','Se debe correguir el orden en retenciones de pagos es del tipo ');</script>";
													$_POST['permisoac'] = 1;
													$_POST['vporcentaje'] = 0;
												}
											}
											if ($_POST['permisoac'] != 1) {
												$_POST['vporcentaje'] = $_POST['valorrp'] * ($_POST['porcentaje'] / 100);
											}
										} else {
											echo "<option value='$row[0]'>$row[1] - $row[2]</option>";
										}
									}
									?>
								</select>
								<input type="hidden" name="permisoac" id="permisoac"
									value="<?php echo $_POST['permisoac'] ?>">
								<input type="hidden" name="nretencion" id="nretencion"
									value="<?php echo $_POST['nretencion'] ?>">
							</td>
							<input type="hidden" id="contador" name="contador"
								value="<?php echo $_POST['contador']; ?>">
							<input type="hidden" id="oculto12" name="oculto12"
								value="<?php echo $_POST['oculto12']; ?>">

							<input type="hidden" id="porcentaje" name="porcentaje"
								value="<?php echo $_POST['porcentaje'] ?>">
							<td style="width:6%;"><input type="text" id="porcentajex" name="porcentajex"
									value="<?php echo $_POST['porcentajex'] ?>" style="width:98%;height:30px;" readonly>
							</td>
							<td><img class='icobut2' src="imagenes/tareas.png" id="openedit"
									title="Editar Retenci&oacute;n" onClick="editarretencion();"></td>
							<td class="tamano01" style="width:2cm;">Valor:</td>
							<td><input type="text" class='inputnum' id="vporcentaje" name="vporcentaje"
									style="width:98%;height:30px;" value="<?php echo $_POST['vporcentaje'] ?>" readonly>
							</td>
							<td style="width:3cm;"><em class="botonflecha" onClick="agregardetalled();">Agregar</em>
							</td>
							<td></td>
							<input type="hidden" value="0" name="agregadetdes">
						</tr>
					</table>
					<?php
					if ($_POST['eliminad'] != '') {
						$posi = $_POST['eliminad'];
						unset($_POST['ddescuentos'][$posi]);
						unset($_POST['dndescuentos'][$posi]);
						unset($_POST['dporcentajes'][$posi]);
						unset($_POST['ddesvalores'][$posi]);
						unset($_POST['dmanual'][$posi]);
						$_POST['ddescuentos'] = array_values($_POST['ddescuentos']);
						$_POST['dndescuentos'] = array_values($_POST['dndescuentos']);
						$_POST['dporcentajes'] = array_values($_POST['dporcentajes']);
						$_POST['ddesvalores'] = array_values($_POST['ddesvalores']);
						$_POST['dmanual'] = array_values($_POST['dmanual']);
					}
					if ($_POST['agregadetdes'] == '1') {
						$_POST['ddescuentos'][] = $_POST['retencion'];
						$_POST['dndescuentos'][] = $_POST['nretencion'];
						$_POST['dporcentajes'][] = $_POST['porcentaje'];
						$_POST['ddesvalores'][] = $_POST['vporcentaje'];
						$_POST['dmanual'][] = $_POST['manual'];
						$_POST['agregadetdes'] = '0';
						echo "
								<script>
									document.form2.porcentaje.value=0;
									document.form2.vporcentaje.value=0;
									document.form2.retencion.value='';
								</script>";
					}
					?>
					<table class="inicio" style="overflow:scroll">
						<tr>
							<td class="titulos">Descuento</td>
							<td class="titulos" style="width:5%;">%</td>
							<td class="titulos" style="width:15%;">Valor</td>
							<td class="titulos2" style="width:4%;"><img src="imagenes/del.png"></td>
						</tr>
						<input type='hidden' name='eliminad' id='eliminad'>
						<?php
						$totaldes = 0;
						for ($x = 0; $x < count($_POST['ddescuentos']); $x++) {
							echo "
								<input type='hidden' name='dndescuentos[]' value='" . $_POST['dndescuentos'][$x] . "'>
								<input type='hidden' name='ddescuentos[]' value='" . $_POST['ddescuentos'][$x] . "' >
								<input type='hidden' name='dporcentajes[]' value='" . $_POST['dporcentajes'][$x] . "'>
								<input type='hidden' name='ddesvalores[]' value='" . ($_POST['ddesvalores'][$x]) . "'>
								<input type='hidden' name='dmanual[]' value='" . ($_POST['dmanual'][$x]) . "'>
								<tr class='$co'>
									<td>" . $_POST['dndescuentos'][$x] . "</td>
									<td>" . $_POST['dporcentajes'][$x] . "</td>
									<td>" . ($_POST['ddesvalores'][$x]) . "</td>
									<td><a href='#' onclick='eliminard($x)'><img src='imagenes/del.png'></a></td>
								</tr>";
							$totaldes = $totaldes + ($_POST['ddesvalores'][$x]);
							$aux = $co;
							$co = $co2;
							$co2 = $aux;
						}
						echo "
								<tr class='$co'>
									<td colspan='2'></td>
									<td>$totaldes</td>
									<td></td>
								</tr>";
						?>
					</table>
				</div>
			</div>
			<div class="tab">
				<input type="radio" id="tab-4" name="tabgroup1" value="4" <?php echo $check4; ?> />
				<label id="clabel" for="tab-4">Validar Obligaci&oacute;n</label>
				<div class="content" style="overflow-x:hidden;">
					<?php
					echo "
							<table class='inicio' width='99%'>
								<tr>
									<td class='titulos' colspan='12'>.: Validar Obligaci&oacute;n</td>
									<td class='cerrar' style='width:7%' onClick=\"location.href='ccp-principal.php'\">Cerrar</td>
								</tr>
								<tr class='titulos2'>
									<td style='text-align:center;'>Vigencia del Gasto</td>
									<td style='text-align:center;'>Sec. presupuestal</td>
									<td style='text-align:center;'>BPIM</td>
									<td style='width:10%;text-align:center;'>Cuenta</td>
									<td style='text-align:center;'>Nombre Cuenta</td>
									<td style='text-align:center;'>Sector</td>
									<td style='text-align:center;'>Fuente</td>
									<td style='text-align:center;'>Producto/Servicio</td>
									<td style='width:10%;text-align:center;'>Indicador Producto</td>
									<td style='width:10%;text-align:center;'>Pol&iacute;tica P&uacute;plica</td>
									<td style='text-align:center;'>Medio de Pago</td>
									<td style='width:10%;text-align:center;'>Valor</td>
									<td style='text-align:center;'>Contabiliza</td>
								</tr>";
					$_POST['cuentagas'] = 0;
					for ($x = 0; $x < count($_POST['nomCuenta']); $x++) {
						echo "
								<tr class='$co'>
									<td style='width=10%'>" . $_POST['codVigenciag'][$x] . " - " . $_POST['nomVigenciag'][$x] . "</td>
									<td style='width=10%'>" . $_POST['seccion_presupuestal'][$x] . " - " . $_POST['nomSeccion_presupuestal'][$x] . "</td>
									<td style='width=10%'>" . $_POST['bpim'][$x] . "</td>
									<td style='width=10%'>" . $_POST['codCuenta'][$x] . "</td>
									<td style='width=10%'>" . $_POST['nomCuenta'][$x] . "</td>
									<td style='width=10%'>" . $_POST['codsector'][$x] . " - " . $_POST['nomsector'][$x] . "</td>
									<td style='width=10%'>" . $_POST['codFuente'][$x] . " - " . $_POST['nomFuente'][$x] . "</td>
									<td style='width=10%'>" . $_POST['codProductos'][$x] . " - " . $_POST['nomProductos'][$x] . "</td>
									<td style='width=10%'>" . $_POST['codIndicador'][$x] . " - " . $_POST['nomIndicador'][$x] . "</td>
									<td style='width=10%'>" . $_POST['codPoliticap'][$x] . " - " . $_POST['nomPoliticap'][$x] . "</td>
									<td style='width=10%'>" . $_POST['medioPago'][$x] . "</td>
									<td style='text-align:right;'>$ " . number_format($_POST['valor'][$x], 2, $_SESSION["spdecimal"], $_SESSION["spmillares"]) . "</td>
									<td style='text-align:center;'>
										<div class='swsino'>
											<input type='checkbox' name='idswsino$x' class='swsino-checkbox' id='idswsino$x' value='" . $_POST["idswsino$x"] . "' " . $_POST["idswsino$x"] . " onChange=\"cambiocheck('$x');\">
											<label class='swsino-label' for='idswsino$x'>
												<span class='swsino-inner'></span>
												<span class='swsino-switch'></span>
											</label>
										</div>
									</td>
								</tr>";
						$gas = $_POST['valor'][$x];
						$_POST['cuentagas'] = $_POST['cuentagas'] + $gas;
						$resultado = convertir($_POST['cuentagas']);
						$_POST['letras'] = $resultado . " PESOS";
						$aux = $co;
						$co = $co2;
						$co2 = $aux;
					}
					echo "
								<input type='hidden' name='cuentagas' id='cuentagas'  value='" . $_POST['cuentagas'] . "'>
								<input type='hidden' name='cuentagas2' id='cuentagas2' value='" . $_POST['cuentagas2'] . "'>
								<input type='hidden'  name='letras' id='letras' value='" . $_POST['letras'] . "'>
								<tr class='$co' style='text-align:right;'>
									<td colspan='11'>Total:</td>
									<td>$ " . number_format($_POST['cuentagas'], 2, $_SESSION["spdecimal"], $_SESSION["spmillares"]) . "</td>
								</tr>
								<tr>
									<td class='saludo1'>Son:</td>
									<td class='saludo1' colspan= '11'>" . $_POST['letras'] . "</td>
								</tr>
							</table>";
					?>
				</div>
			</div>
		</div>
		<input type="hidden" id="cuendevcre" name="cuendevcre" value="">
		<?php
		if ($_POST['oculto'] == '2') {
			switch ($_POST['estado']) {
				case 'ACTIVO':
					$estado = 'S';
					break;
				case 'COMPLETO':
					$estado = 'C';
					break;
				case 'ANULADO':
					$estado = 'N';
					break;
				case 'REVERSADO':
					$estado = 'R';
					break;
			}
			$idCabecera = selconsecutivo('ccpetdc', 'id');
			$sqlCabecera = "INSERT INTO ccpetdc (id, vigencia, consvigencia, estado, fecha, tercero, valor, saldo, contrato, idcdp, tipo_mov, detalle) VALUE ('$idCabecera', '" . $_POST['vigencia'] . "', '" . $_POST['rp'] . "', '$estado', '" . $_POST['fecha'] . "', '" . $_POST['ntercero'] . "', '" . $_POST['valorrp'] . "', '" . $_POST['saldo'] . "', '" . $_POST['contrato'] . "', '" . $_POST['idcdp'] . "', '" . $_POST['tipomov'] . "', '" . $_POST['objeto'] . "')";
			mysqli_query($linkbd, $sqlCabecera);
			for ($x = 0; $x < count($_POST['codCuenta']); $x++) {
				if ($_POST["idswsino$x"] != 'checked') {
					$vlsino = 'N';
				} else {
					$vlsino = 'S';
				}
				$iddetalle = selconsecutivo('ccpetdc_detalle', 'id');

				$sec_presupuestal = str_pad($_POST['seccion_presupuestal'][$x], 2, '0', STR_PAD_LEFT);
				$vigenciaTT = $_POST['vigencia'];
				$sqlDetalle = "INSERT INTO ccpetdc_detalle (id, vigencia, consvigencia, cuenta, productoservicio, fuente, tipo_mov, estado, indicador_producto, cod_politicap, cod_vigenciag, valor,saldo, saldo_liberado, medio_pago,idrp, destino_compra, sector, cuenta_debito, cuenta_credito,contabiliza,seccion_presupuestal,bpim) values('$iddetalle','$vigenciaTT','$idCabecera', '" . $_POST['codCuenta'][$x] . "','" . $_POST['codProductos'][$x] . "','" . $_POST['codFuente'][$x] . "','" . $_POST['tipomov'] . "','$estado','" . $_POST['codIndicador'][$x] . "','" . $_POST['codPoliticap'][$x] . "','" . $_POST['codVigenciag'][$x] . "','" . $_POST['valor'][$x] . "','" . $_POST['saldo'] . "',0,'" . $_POST['medioPago'][$x] . "','" . $_POST['rp'] . "','" . $_POST["destcompra$x"] . "','" . $_POST['codsector'][$x] . "','" . $_POST['cuentaDebito'][$x] . "','" . $_POST['cuentaCredito'][$x] . "','$vlsino', '$sec_presupuestal','" . $_POST['bpim'][$x] . "')";
				if (!mysqli_query($linkbd, $sqlDetalle)) {
					echo "<script>despliegamodalm('visible','2','No se pudo ejecutar la accion:');</script>";
				} else {
					if (isset($_POST['cuentaDebitoM'][$x]) && is_array($_POST['cuentaDebitoM'][$x]) && isset($_POST['cuentaCreditoM'][$x]) && is_array($_POST['cuentaCreditoM'][$x])) {
						foreach ($_POST['cuentaDebitoM'][$x] as $y => $valorDebito) {
							$valorCredito = $_POST['cuentaCreditoM'][$x][$y];
							$sqlCuentasAdd = "INSERT INTO ccpetdc_cuentasadd (id_detalle, consvigencia, vigencia, cuenta_debito, cuenta_credito, estado) VALUES ('$iddetalle', '$idCabecera', '$vigenciaTT', '$valorDebito', '$valorCredito', 'S')";
							if (!mysqli_query($linkbd, $sqlCuentasAdd)) {
								echo "<script>despliegamodalm('visible','2','No se pudo guardar en ccpetdc_cuentasadd:');</script>";
							}
						}
					}
					echo "<script>despliegamodalm('visible','1','Se ha almacenado con exito');</script>";
				}
			}
			for ($x = 0; $x < count($_POST['ddescuentos']); $x++) {
				$idretencion = selconsecutivo('ccpetdc_retenciones', 'id');
				$sqlRetencion = "INSERT INTO ccpetdc_retenciones (id, iddestino, idretencion, porcentaje, valor, estado) values('$idretencion','$idCabecera','" . $_POST['ddescuentos'][$x] . "','" . $_POST['dporcentajes'][$x] . "','" . ($_POST['ddesvalores'][$x]) . "','S')";
				if (!mysqli_query($linkbd, $sqlRetencion)) {
					echo "<script>despliegamodalm('visible','2','Error a ingresar las Retenciones');</script>";
				}
			}
		}
		?>
	</form>
	<div id="bgventanamodal2">
		<div id="ventanamodal2">
			<IFRAME src="" name="buster" marginWidth=0 marginHeight=0 frameBorder=0 id="ventana2" frameSpacing=0
				style="left:500px; width:900px; height:500px; top:200;"></IFRAME>
		</div>
	</div>
</body>

</html>
