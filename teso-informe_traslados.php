<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require "comun.inc";
	require "funciones.inc";
	sesion();
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html lang=es>
	<head>
		<meta charset="utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1"/>
		<meta name="viewport" content="user-scalable=no">
		<title>:: IDEAL 10 - Tesorer&iacute;a</title>
		<link href="favicon.ico" rel="shortcut icon"/>
		<link href="css/css2.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css">
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css">
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css">
		<link href="css/style.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
		<script type="text/javascript" src="css/calendario.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script type="text/javascript" src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script src="sweetalert2/dist/sweetalert2.min.js"></script>
		<link rel="stylesheet" href="sweetalert2/dist/sweetalert2.min.css">
		<script>
			function crearexcel(){
				document.form2.action="teso-trasladosbancosexcel.php";
				document.form2.target="_BLANK";
				document.form2.submit();
				document.form2.action="";
				document.form2.target="";
			}
			function validar(){
				document.form2.submit();
			}
			
			function pdf(){
				document.form2.action="teso-pdftraslados.php";
				document.form2.target="_BLANK";
				document.form2.submit(); 
				document.form2.action="";
				document.form2.target="";
			}
		</script>
		<?php 
			titlepag();
			
		?>
	</head>
	<body>
		<IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
		<span id="todastablas2"></span>
		<table>
			<tr><script>barra_imagenes("teso");</script><?php cuadro_titulos();?></tr>	 
			<tr><?php menu_desplegable("teso");?></tr>
			<tr>
				<td colspan="3" class="cinta">
					<img src="imagenes/add.png" title="Nuevo" onClick="location.href='teso-traslados.php'" class="mgbt">
					<img src="imagenes/guardad.png" class="mgbt1">
					<img src="imagenes/busca.png" class="mgbt" onClick="document.form2.submit();">
					<img src="imagenes/agenda1.png" title="Agenda" onClick="mypop=window.open('plan-agenda.php','','');mypop.focus()" class="mgbt"/>
					<img src="imagenes/nv.png" title=" Nueva ventana" onClick="mypop=window.open('teso-principal.php','','');mypop.focus();" class="mgbt">
					<img src="imagenes/duplicar_pantalla.png" title="Duplicar pesta&ntilde;a" onClick="mypop=window.open('<?php echo $url2; ?>','','');mypop.focus();" class="mgbt">
					<img src="imagenes/excel.png" title="Excel" onclick="crearexcel()" class="mgbt">
				</td>
			</tr>
		</table>
		<form name="form2" method="post" action="">
			<div class="loading-container" id="divcarga">
				<p class="text-loading" data-loading="IDEAL 10">IDEAL 10</p>
			</div>
			
			<table class="inicio ancho">
				<tr>
					<td class="titulos" colspan="7">:. Buscar Traslados de Bancos </td>
					<td class="cerrar" style="width:7%" onClick="location.href='teso-principal.php'">Cerrar</td>
				</tr>
				<tr>
					<td class="tamano01" style="width:2.5cm;">Fecha Inicial: </td>
					<td style="width:12%"><input name="fechaini" type="text" id="fc_1198971545" title="DD/MM/YYYY" value="<?php echo $_POST['fechaini']; ?>" onKeyUp="return tabular(event,this) " onKeyDown="mascara(this,'/',patron,true)"  maxlength="10" style="width:100%;;" class="colordobleclik" onDblClick="displayCalendarFor('fc_1198971545');" autocomplete="off" onChange=""></td>
					<td class="tamano01" style="width:2.5cm;">Fecha Final: </td>
					<td style="width:12%"><input name="fechafin" type="text" id="fc_1198971546" title="DD/MM/YYYY" value="<?php echo $_POST['fechafin']; ?>" onKeyUp="return tabular(event,this) " onKeyDown="mascara(this,'/',patron,true)"  maxlength="10" style="width:100%;" class="colordobleclik" onDblClick="displayCalendarFor('fc_1198971546');" autocomplete="off" onChange=""></td>
					<td><em class="botonflechaverde" onClick="document.form2.submit();">Buscar</em></td>
					<td></td>
					<input name="oculto" type="hidden" value="1"><input name="var1" type="hidden" value=<?php echo $_POST['var1'];?>>
				</tr>
			</table>
			<?php
				if($_POST['oculto']!=''){
					if (($_POST['fechaini'] != "" && $_POST['fechafin'] != "") ||( $_POST['numero'] != "") ){
						$crit2 = " ";
						preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST['fechaini'],$fecha);
						$fechai = "$fecha[3]-$fecha[2]-$fecha[1]";
						preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST['fechafin'],$fecha);
						$fechaf = $fecha[3]."-".$fecha[2]."-".$fecha[1];
						$crit2=" AND fecha BETWEEN '$fechai' AND '$fechaf'";
						
						$sqlr = "SELECT * FROM tesotraslados WHERE estado <> '' $crit2 ORDER BY id_trasladocab DESC";
						$resp = mysqli_query($linkbd,$sqlr);
						$ntr = mysqli_num_rows($resp);
						$con = 1;
						echo "
						<table class='tablamv'>
							<thead>
								<tr style='text-align:left;'><th colspan='5' class='titulos'>.: Resultados Busqueda: $ntr</th></tr>
								<tr>
									<th class='titulosnew00' style='width:5%'><center>No Traslado</th>
									<th class='titulosnew00' style='width:8%'><center>Fecha</th>
									<th class='titulosnew00' style='width:20%'>Concepto</th>
									<th class='titulosnew00' style='width:8%'><center>CC Origen</th>
									<th class='titulosnew00' style='width:8%'><center>Cuenta Origen</th>
									<th class='titulosnew00' style='width:8%'><center>Tercero Origen</th>
									<th class='titulosnew00' style='width:8%'><center>CC Destino</th>
									<th class='titulosnew00' style='width:8%'><center>Cuenta Destino</th>
									<th class='titulosnew00' style='width:8%'><center>Tercero Destino</th>
									<th class='titulosnew00' style='width:8%'><center>Destino</th>
									<th class='titulosnew00' style='width:16%'>Valor</th>
								</tr>
							</thead>
							<tbody style='max-height: 52vh;' id='divdet'>";
						$iter = 'saludo3';
						$iter2 = 'saludo3';
						while ($row = mysqli_fetch_row($resp)){
							$sqlr2 = "SELECT concepto, origen FROM tesotraslados_cab WHERE id_consignacion = '$row[1]'";
							$resp2 = mysqli_query($linkbd,$sqlr2);
							$row2 = mysqli_fetch_row($resp2);
							$valor = $row2[0];

							$sqlr4 = "SELECT TB2.nombre FROM tesobancosctas AS TB1 INNER JOIN cuentasnicsp AS TB2 ON TB1.cuenta = TB2.cuenta WHERE TB1.ncuentaban = '$row[5]'";
							$resp4 = mysqli_query($linkbd,$sqlr4);
							$row4 = mysqli_fetch_row($resp4);
							$CuentaOrigen = $row4[0];

							if ($row[12] == 'INT'){
								$sqlr3 = "SELECT nombre FROM redglobal WHERE codigo = '$row2[1]'";
								$resp3 = mysqli_query($linkbd,$sqlr3);
								$row3 = mysqli_fetch_row($resp3);
								$destino = "$row2[1] - $row3[0]";

								$sqlr5 = "SELECT TB2.nombre FROM tesobancosctas AS TB1 INNER JOIN cuentasnicsp AS TB2 ON TB1.cuenta = TB2.cuenta WHERE TB1.ncuentaban = '$row[8]'";
								$resp5 = mysqli_query($linkbd,$sqlr5);
								$row5 = mysqli_fetch_row($resp5);
								$CuentaDestino = $row5[0];
							} else {
								$sqlr3 = "SELECT nombre, base, usuario FROM redglobal WHERE codigo = '$row[13]'";
								$resp3 = mysqli_query($linkbd,$sqlr3);
								$row3 = mysqli_fetch_row($resp3);
								$destino = "$row[13] - $row3[0]";

								$linkmulti = conectar_Multi($row3[1],$row3[2]);
								$linkmulti -> set_charset("utf8");

								$sqlr5 = "SELECT TB2.nombre FROM tesobancosctas AS TB1 INNER JOIN cuentasnicsp AS TB2 ON TB1.cuenta = TB2.cuenta WHERE TB1.ncuentaban = '$row[8]'";
								$resp5 = mysqli_query($linkmulti,$sqlr5);
								$row5 = mysqli_fetch_row($resp5);
								$CuentaDestino = $row5[0];
							}

							if($row[2] == 'N'){
								$estilo = 'background-color:#eba8a0';
								$estado = 'teso-traslados.php';
							}else{
								$estilo = '';
								$estado = '';
							}
							
							echo"
							<input type='hidden' name='nTrasladE[]' value='".$row[0]."'>
							<input type='hidden' name='fechaE[]' value='".$row[2]."'>
							<input type='hidden' name='conceptoTrasladoE[]' value='".$row2[0]."'>
							<input type='hidden' name='destino[]' value='$destino'>
							<input type='hidden' name='valorE[]' value='".number_format($row2[0],2,',','.')."'>
							";
							
							echo "
							<tr class='$iter' style='text-transform:uppercase; $estilo' title='$estado'>
								<td style='text-align:center; width:5%'>$row[1]</td>
								<td style='text-align:center; width:8%'>$row[2]</td>
								<td style='width:20%'>$row2[0]</td>
								<td style='text-align:center; width:8%'>$row[4]</td>
								<td style='text-align:center; width:8%'>$CuentaOrigen</td>
								<td style='text-align:center; width:8%'>$row[6]</td>
								<td style='text-align:center; width:8%'>$row[7]</td>
								<td style='text-align:center; width:8%'>$CuentaDestino</td>
								<td style='text-align:center; width:8%'>$row[9]</td>
								<td style='text-align:center; width:8%'>$destino</td>
								<td style='text-align:right;width:16%'>".number_format($row[10],2,",",".")."</td>
							</tr>";
							$con+= 1;
							$aux = $iter;
							$iter = $iter2;
							$iter2 = $aux;
						}
						echo"
							</tbody>
						</table>";
					}else{
						echo"
						<script>
							Swal.fire(
								'Error!',
								'Se debe ingresar fecha inicial y fecha final',
								'error'
							);
						</script>";
					}
				}
				echo "<script>document.getElementById('divcarga').style.display='none';</script>";
			?>
		</form>
	</body>
</html>