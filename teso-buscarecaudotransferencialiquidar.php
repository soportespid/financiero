<?php
    header("Cache-control: no-cache, no-store, must-revalidate");
    header("Content-Type: text/html;charset=utf8");
	require "comun.inc";
	require "funciones.inc";
	session_start();
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
	cargarcodigopag($_GET['codpag'], $_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html lang="es">
	<head>
        <meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
    	<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<title>:: IDEAL 10 - Tesorer&iacute;a</title>

		<link href="css/css2.css" rel="stylesheet" type="text/css" />
		<link href="css/css3.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="css/programas.js"></script>
        <script type="text/javascript" src="css/calendario.js"></script>
        <script type="text/javascript" src="JQuery/jquery-2.1.4.min.js"></script>
        <script type="text/javascript" src="JQuery/autoNumeric-master/autoNumeric-min.js"></script>
		<script>


            function buscarbotonfiltro()
 			{
                if((document.form2.fecha.value != "" && document.form2.fecha2.value == "") || (document.form2.fecha.value == "" && document.form2.fecha2.value != ""))
                {
                    alert("Falta digitar fecha");
                }
                else
                {
                    document.getElementById('numpos').value=0;
                    document.getElementById('nummul').value=0;
                    document.form2.submit();
                }

 			}

			function eliminar(idr)
			{
				if (confirm("Esta Seguro de Eliminar el Recaudo Transferencia "+idr))
  				{
  					document.form2.oculto.value=2;
  					document.form2.var1.value=idr;
					document.form2.submit();
  				}
			}

			function verUltimaPos(idcta)
            {
				location.href="teso-editarecaudotransferencialiquidar.php?idrecaudo="+idcta;
			}

            function crearexcel()
            {
				document.form2.action="teso-liquidarrecaudotransferenciaexcel.php";
				document.form2.target="_BLANK";
				document.form2.submit();
				document.form2.action="";
				document.form2.target="";
			}

			</script>
			<?php titlepag();?>
            <?php
                if(isset($_GET['fini']) && isset($_GET['ffin'])){
                    if(!empty($_GET['fini']) && !empty($_GET['ffin'])){
                        $_POST['fecha']=$_GET['fini'];
                        $_POST['fecha2']=$_GET['ffin'];
                    }
                }
                preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST["fecha"], $fech1);
			    preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST["fecha2"], $fech2);
                $f1=$fech1[3]."-".$fech1[2]."-".$fech1[1];
                $f2=$fech2[3]."-".$fech2[2]."-".$fech2[1];

                $scrtop=$_GET['scrtop'];
                if($scrtop=="") {$scrtop=0;}
                echo"<script>window.onload=function(){ $('#divdet').scrollTop(".$scrtop.")}</script>";
                $gidcta=$_GET['idcta'];
                if(isset($_GET['filtro'])){$_POST['nombre']=$_GET['filtro'];}
		    ?>
		</head>
		<body>
			<IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
			<span id="todastablas2"></span>
			<table>
				<tr><script>barra_imagenes("teso");</script><?php cuadro_titulos();?></tr>
				<tr><?php menu_desplegable("teso");?></tr>
				<tr>
  					<td colspan="3" class="cinta">
                        <a href="teso-recaudotransferencialiquidar.php" class="mgbt"><img src="imagenes/add.png" title="Nuevo"/></a>
                        <a class="mgbt"><img src="imagenes/guardad.png"/></a>
                        <a class="mgbt"><img src="imagenes/buscad.png"/></a>
                        <a onClick="mypop=window.open('plan-agenda.php','','');mypop.focus()" class="mgbt"><img src="imagenes/agenda1.png" title="Agenda"/></a>
                        <a class="mgbt" onClick="mypop=window.open('teso-principal.php','','');mypop.focus();"><img src="imagenes/nv.png" title="Nueva ventana"></a>
                        <a onClick="mypop=window.open('<?php echo $url2; ?>','','');mypop.focus();" class="tooltip bottom mgbt"><img src="imagenes/duplicar_pantalla.png" title="Duplicar pesta&ntilde;a"></a>
                        <a onclick="crearexcel()" class="mgbt"><img src="imagenes/excel.png" title="Excel"></a>
					</td>
             	</tr>
			</table>
            <?php
                if($_GET['numpag']!="")
                {
                    $oculto=$_POST['oculto'];
                    if($oculto!=2)
                    {
                        $_POST['numres']=$_GET['limreg'];
                        $_POST['numpos']=$_GET['limreg']*($_GET['numpag']-1);
                        $_POST['nummul']=$_GET['numpag']-1;
                    }
                }
                else
                {
                    if($_POST['nummul']=="")
                    {
                        $_POST['numres']=10;
                        $_POST['numpos']=0;
                        $_POST['nummul']=0;
                    }
                }
            ?>
            <tr>
                <form name="form2" method="post" action="teso-buscarecaudotransferencialiquidar.php">
                    <input type="hidden" name="numres" id="numres" value="<?php echo $_POST['numres'];?>"/>
                    <input type="hidden" name="numpos" id="numpos" value="<?php echo $_POST['numpos'];?>"/>
                    <input type="hidden" name="nummul" id="nummul" value="<?php echo $_POST['nummul'];?>"/>
                    <table  class="inicio" align="center" >
                        <tr>
                            <td class="titulos" colspan="16">.: Buscar Liquidacion Recaudos Transferencia</td>
                            <td class="cerrar" width="7%"><a href="teso-principal.php">&nbsp;Cerrar</a></td>
                        </tr>
                        <tr>
                            <td class="tamano01" style="width:4cm;">C&oacute;digo liquidaci&oacute;n:</td>
                            <td colspan="4"><input type="search" name="numero" id="numero" value="<?php echo $_POST['numero'];?>" style="width:100%;"/></td>
                            <td class="tamano01" style="width:4cm;">Nombre liquidaci&oacute;n:</td>
                            <td colspan="4"><input type="search" name="nombre" id="nombre" value="<?php echo $_POST['nombre'];?>" style="width:100%;"/></td>
                            <td class="tamano01">Fecha inicial: </td>
                            <td style="width:10%;"><input type="search" name="fecha" id="fc_1198971545" title="DD/MM/YYYY"  placeholder="DD/MM/YYYY" value="<?php echo $_POST['fecha'];?>" onKeyUp="return tabular(event,this)" onchange="" onKeyDown="mascara(this,'/',patron,true)" maxlength="10" style="width:75%">&nbsp;<img src="imagenes/calendario04.png" onClick="displayCalendarFor('fc_1198971545');" class="icobut" title="Calendario"></td>
                            <td class="tamano01" >Fecha final: </td>
                            <td style="width:10%;"><input type="search" name="fecha2"  id="fc_1198971546" title="DD/MM/YYYY" placeholder="DD/MM/YYYY" value="<?php echo $_POST['fecha2'];?>" onKeyUp="return tabular(event,this) " onchange="" onKeyDown="mascara(this,'/',patron,true)" maxlength="10" style="width:75%"/>&nbsp;<img src="imagenes/calendario04.png" onClick="displayCalendarFor('fc_1198971546');"  class="icobut" title="Calendario"></td>
                            <td style="padding-bottom:1px"><em class="botonflecha" onClick="buscarbotonfiltro();">Buscar</em></td>
                        </tr>
                    </table>

                <input type="hidden" name="oculto" id="oculto"  value="<?php echo $_POST['oculto'] ?>" >
                <input type="hidden" name="var1" id="var1"  value="<?php echo $_POST['var1'] ?>" />
                <input type="hidden" name="fecham1"  id="fecham1" value="<?php echo $_POST['fecham1']; ?>"/>
    		    <input type="hidden" name="fecham2" id="fecham2" value="<?php echo $_POST['fecham2']; ?>"/>

                <div class="subpantallap" style="overflow-x:hidden" id="divdet">
                    <?php
                        $_POST['fecham1']=$f1;
                        $_POST['fecham2']=$f2;

                        $oculto=$_POST['oculto'];
                        if($_POST['oculto']==2)
                        {
                            $sqlr="select * from tesorecaudotransferencialiquidar where id_recaudo=$_POST[var1]  ";
                            $resp = mysqli_query($linkbd,$sqlr);
                            $row=mysqli_fetch_row($resp);
                            //********Comprobante contable en 000000000000
                            $sqlr="update comprobante_cab set total_debito=0,total_credito=0,estado='0' where numerotipo=$row[0] AND tipo_comp=28";
                            // echo $sqlr;
                            mysqli_query($linkbd,$sqlr);
                            $sqlr="update comprobante_det set valdebito=0,valcredito=0 where id_comp='28 $row[0]'";
                            mysqli_query($linkbd,$sqlr);

                            /* $sqlr="update pptocomprobante_cab set estado='0' where numerotipo=$row[0] AND tipo_comp=19";
                            mysql_query($sqlr,$linkbd);
                            */
                            //******** RECIBO DE CAJA ANULAR 'N'
                            $sqlr="update tesorecaudotransferencialiquidar set estado='N' where id_recaudo=$row[0]";
                            mysqli_query($linkbd,$sqlr);
                        }
                    ?>

                    <?php
                        $oculto=$_POST['oculto'];
                        {

                        $crit1="";
                        $crit2="";

                        $sqlr="SELECT * FROM tesorecaudotransferencialiquidar WHERE estado<>'' $crit1 $crit2 ORDER BY id_recaudo DESC";

                        if ($_POST['numero'] != ""){$crit1="AND id_recaudo LIKE '$_POST[numero]'";}

                        if ($_POST['nombre'] != ""){$crit2="AND concepto LIKE '$_POST[nombre]'";}

                        $sqlr="SELECT * FROM tesorecaudotransferencialiquidar WHERE estado<>'' $crit1 $crit2 ORDER BY id_recaudo DESC";
                        if(isset($_POST['fecha']) && isset($_POST['fecha2'])){
                            if(!empty($_POST['fecha']) && !empty($_POST['fecha2'])){
                                $sqlr="SELECT * FROM tesorecaudotransferencialiquidar WHERE estado<>'' $crit1 $crit2 AND fecha BETWEEN '$f1' AND '$f2' ORDER BY id_recaudo DESC";
                            }
                        }

                        $resp = mysqli_query($linkbd,$sqlr);
                        $ntr = mysqli_num_rows($resp);
                        $_POST['numtop']=$ntr;
                        $nuncilumnas=ceil($_POST['numtop']/$_POST['numres']);

                        $cond2="";
                        if ($_POST['numres']!="-1")
                        {
                            $cond2="LIMIT $_POST[numpos], $_POST[numres]";
                        }
                        $sqlr="SELECT * FROM tesorecaudotransferencialiquidar WHERE estado<>'' $crit1 $crit2 ORDER BY id_recaudo DESC $cond2";
                        if(isset($_POST['fecha']) && isset($_POST['fecha2'])){
                            if(!empty($_POST['fecha']) && !empty($_POST['fecha2'])){
                                $sqlr="SELECT * FROM tesorecaudotransferencialiquidar WHERE estado<>'' AND fecha BETWEEN '$f1' AND '$f2' $crit1 $crit2 ORDER BY id_recaudo DESC $cond2";
                            }
                        }

                        $resp = mysqli_query($linkbd,$sqlr);
                        $numcontrol=$_POST['nummul']+1;

                        if(($nuncilumnas==$numcontrol)||($_POST['numres']=="-1"))
                        {

                            $imagenforward="<img src='imagenes/forward02.png' style='width:17px'>";
                            $imagensforward="<img src='imagenes/skip_forward02.png' style='width:16px' >";
                        }
                        else
                        {
                            $imagenforward="<img src='imagenes/forward01.png' style='width:17px' title='Siguiente' onClick='numsiguiente()'>";
                            $imagensforward="<img src='imagenes/skip_forward01.png' style='width:16px' title='Fin' onClick='saltocol(\"$nuncilumnas\")'>";
                        }
                        if(($_POST['numpos']==0)||($_POST['numres']=="-1"))
                        {
                            $imagenback="<img src='imagenes/back02.png' style='width:17px'>";
                            $imagensback="<img src='imagenes/skip_back02.png' style='width:16px'>";
                        }
                        else
                        {
                            $imagenback="<img src='imagenes/back01.png' style='width:17px' title='Anterior' onClick='numanterior();'>";
                            $imagensback="<img src='imagenes/skip_back01.png' style='width:16px' title='Inicio' onClick='saltocol(\"1\")'>";
                        }

                        $con=1;

                            echo "
                            <table class='inicio' align='center' >
                                <tr>
                                    <td colspan='8' class='titulos'>.: Resultados Busqueda:</td>
                                    <td class='submenu'>
                                        <select name='renumres' id='renumres' onChange='cambionum();' style='width:100%'>
											<option value='10'"; if ($_POST['numres']=='10'){echo 'selected';} echo ">10</option>
											<option value='20'"; if ($_POST['numres']=='20'){echo 'selected';} echo ">20</option>
											<option value='30'"; if ($_POST['numres']=='30'){echo 'selected';} echo ">30</option>
											<option value='50'"; if ($_POST['numres']=='50'){echo 'selected';} echo ">50</option>
											<option value='100'"; if ($_POST['numres']=='100'){echo 'selected';} echo ">100</option>
											<option value='-1'"; if ($_POST['numres']=='-1'){echo 'selected';} echo ">Todos</option>
										</select>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan='2' id='RecEnc'>Recaudos encontrados: $ntr2</td>
                                </tr>
                                <tr>
                                    <td width='150' class='titulos2' style='width:7%;text-align:center'>Codigo</td>
                                    <td class='titulos2'>Nombre</td>
                                    <td class='titulos2' style='width:8%;text-align:center'>Fecha</td>
                                    <td class='titulos2' style='width:20%;text-align:center'>Contribuyente</td>
                                    <td class='titulos2' style='width:9%;text-align:center'>Valor</td>
                                    <td class='titulos2'>Medio de pago</td>
                                    <td class='titulos2'>Estado</td>
                                    <td class='titulos2' width='5%'>
                                        <center>Anular
                                    </td>
                                    <td class='titulos2' width='5%'>
                                        <center>Ver
                                    </td>
                                </tr>";
                                    //echo "nr:".$nr;
                            $iter='zebra1';
                            $iter2='zebra2';
                            $id=$_GET['id'];

                            if($_POST['fecha'] == '' && $_POST['fecha2'] == '' && $_POST['numero'] == '' && $_POST['nombre'] == '')
							{
								echo "
								<table class='inicio'>
									<tr>
										<td class='saludo1' style='text-align:center;width:100%;font-size:25px'>Utilice el filtro de busqueda</td>
									</tr>
								</table>";
								$nuncilumnas = 0;
							}
							elseif(mysqli_num_rows($resp) == 0 || mysqli_num_rows($resp) == '0')
							{
								echo "
								<table class='inicio'>
									<tr>
										<td class='saludo1' style='text-align:center;width:100%;font-size:25px'>No hay resultados de su busqueda.</td>
									</tr>
								</table>";
							}
							else
							{

                                while ($row =mysqli_fetch_row($resp))
                                {
                                    $ntr2 = $ntr;

                                    echo "<script>document.getElementById('RecEnc').innerHTML = 'Recaudos encontrados: $ntr2'</script>";

                                    $estilo='';

                                    if($id==$row[0]){
                                        $estilo='background-color:#FF9';
                                    }
                                    $nter=buscatercero($row[7]);

                                    if($row[10]=='S')
                                        $estado='ACTIVO';
                                    if($row[10]=='N')
                                    $estado='ANULADO';

                                    echo"

                                    <input type='hidden' name='codigoE[]' value='".$row[0]."'>
                                    <input type='hidden' name='nombreE[]' value='".$row[6]."'>
                                    <input type='hidden' name='fechaE[]' value='".$row[2]."'>
                                    <input type='hidden' name='nomContribuyenteE[]' value='".$nter."'>
                                    <input type='hidden' name='valorE[]' value='".number_format($row[9],2)."'>
                                    ";

                                    if ($row[10]=='S')
                                    {
                                        echo"
                                        <input type='hidden' name='estadoE[]' value='ACTIVO'>";
                                    }
                                    if ($row[10]=='N')
                                    {
                                        echo"
                                        <input type='hidden' name='estadoE[]' value='INACTIVO'>";
                                    }
                                    if ($row[10]=='R')
                                    {
                                        echo"
                                        <input type='hidden' name='estadoE[]' value='INACTIVO'>";
                                    }

                                    $medio_de_pago = '';

                                    if($row[11] == 2)
                                    {
                                        $medio_de_pago = 'SSF';
                                    }
                                    else
                                    {
                                        $medio_de_pago = 'CSF';
                                    }

                                echo "<tr class='$iter' onDblClick=\"verUltimaPos($row[0])\" style='$estilo'>
                                        <td >$row[0]</td>
                                        <td >$row[6]</td>
                                        <td >$row[2]</td>
                                        <td >$nter</td>
                                        <td >".number_format($row[9],2)."</td>
                                        <td >".$medio_de_pago."</td>";
                                        if ($row[10]=='S'){echo "<td ><center><img src='imagenes/sema_verdeON.jpg' style='width:18px;'></center></td>";}
                                        if ($row[10]=='N'){echo "<td ><center><img src='imagenes/sema_rojoON.jpg' style='width:18px;'></center></td>";}
                                        if($row[10]=='S')
                                        {
                                        $sqlr2="select count(*) from tesorecaudotransferencia where idcomp=$row[0] and estado='S'";
                                        $resp2 = mysqli_query($linkbd,$sqlr2);
                                        $row2 =mysqli_fetch_row($resp2);
                                        //echo $sqlr2;
                                        if($row2[0]>=1)
                                        echo "<td ></td>";
                                        else
                                        echo "<td ><a href='#'  onClick=eliminar($row[0])><center><img src='imagenes/anular.png'></center></a></td>";
                                        }
                                        if($row[10]=='N')
                                        echo "<td ></td>";
                                echo "<td><a href='teso-editarecaudotransferencialiquidar.php?idrecaudo=$row[0]'><center><img src='imagenes/buscarep.png'></center></a></td></tr>";
                                $con+=1;
                                $aux=$iter;
                                $iter=$iter2;
                                $iter2=$aux;
                                }

                            }

                            echo"</table>
                            <table class='inicio'>
                                <tr>
                                    <td style='text-align:center;'>
                                        <a href='#'>$imagensback</a>&nbsp;
                                        <a href='#'>$imagenback</a>&nbsp;&nbsp;";
                                        if($nuncilumnas<=9){$numfin=$nuncilumnas;}
                                        else{$numfin=9;}
                                        for($xx = 1; $xx <= $numfin; $xx++)
                                        {
                                            if($numcontrol<=9){$numx=$xx;}
                                            else{$numx=$xx+($numcontrol-9);}
                                            if($numcontrol==$numx){echo"<a href='#' onClick='saltocol(\"$numx\")'; style='color:#24D915'> $numx </a>";}
                                            else {echo"<a href='#' onClick='saltocol(\"$numx\")'; style='color:#000000'> $numx </a>";}
                                        }
                                        echo"&nbsp;&nbsp;<a href='#'>$imagenforward</a>
                                            &nbsp;<a href='#'>$imagensforward</a>
                                    </td>
                                </tr>
                            </table>";
                        }
                    ?>
                    </form>
                </div>
            </tr>
        </table>
    </body>
</html>
