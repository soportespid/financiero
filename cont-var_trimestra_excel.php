<?php  
	require_once 'PHPExcel/Classes/PHPExcel.php';
	require "comun.inc";
	require "funciones.inc";
	session_start();
	$linkbd=conectar_v7(); 
	$linkbd -> set_charset("utf8"); 
	$objPHPExcel = new PHPExcel();
	//----Propiedades----
	$objPHPExcel->getProperties()
        ->setCreator("IDEAL")
        ->setLastModifiedBy("IDEAL")
        ->setTitle("Reporte Variaciones Trimestrales")
        ->setSubject("contabilidad")
        ->setDescription("contabilidad")
        ->setKeywords("contabilidad")
        ->setCategory("contabilidad");
	//----Cuerpo de Documento----
	$objPHPExcel->getActiveSheet()->mergeCells('A1:F1');
	$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A1', 'Variacion trimestral');

	$objFont=$objPHPExcel->getActiveSheet()->getStyle('A1')->getFont();
	$objFont->setName('Courier New'); 
	$objFont->setSize(15); 
	$objFont->setBold(true); 
	$objFont->setUnderline(PHPExcel_Style_Font::UNDERLINE_SINGLE);
	$objFont->getColor()->setARGB( PHPExcel_Style_Color::COLOR_BLACK);

	$objAlign=$objPHPExcel->getActiveSheet()->getStyle('A1')->getAlignment(); 
	$objAlign->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); 
	$objAlign->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER); 
	$objPHPExcel-> getActiveSheet ()
		-> getStyle ("A2:F2")	
        -> getFill ()
        -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
        -> getStartColor ()
        -> setRGB ('A6E5F3');
	$objPHPExcel-> getActiveSheet ()
		-> getStyle ("A1")	
        -> getFill ()
        -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
        -> getStartColor ()
        -> setRGB ('A6E5F3');
	$borders = array(
      'borders' => array(
        'allborders' => array(
          'style' => PHPExcel_Style_Border::BORDER_THIN,
          'color' => array('argb' => 'FF000000'),
        )
      ),
	);
	
	$styleArray = array(
		"font"  => array(
		"bold" => false,
		"color" => array("rgb" => "FF0000"),
		"size"  => 12,
		"name" => "Verdana"
		));
	$objPHPExcel->getActiveSheet()->getStyle('A2:F2')->applyFromArray($borders);
	$objPHPExcel->setActiveSheetIndex(0)
	->setCellValue('A2', 'CODIGO')
	->setCellValue('B2', 'CUENTA')
	->setCellValue('C2', 'Saldo Final - Vigencia Anterior')
	->setCellValue('D2', 'Saldo Final - Vigencia Actual')
	->setCellValue('E2', 'Variacion Relativa')
	->setCellValue('F2', 'Variacion Porcentual');
	
	$i=3;

    for($xx=0; $xx<count($_POST['dcuentas']); $xx++)
    {
        $total = 0;
        $porcentaje = 0;
        $total = $_POST['dsaldoant'][$xx] - $_POST['dsaldo'][$xx];
        $porcentaje=round(((($_POST['dsaldoant'][$xx] - $_POST['dsaldo'][$xx])/$_POST['dsaldo'][$xx])*100),2);
        $objPHPExcel->setActiveSheetIndex(0)
        ->setCellValueExplicit ("A$i", $_POST['dcuentas'][$xx], PHPExcel_Cell_DataType :: TYPE_STRING)
        ->setCellValueExplicit ("B$i", $_POST['dncuentas'][$xx], PHPExcel_Cell_DataType :: TYPE_STRING)
        ->setCellValueExplicit ("C$i", $_POST['dsaldoant'][$xx], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
        ->setCellValueExplicit ("D$i", $_POST['dsaldo'][$xx], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
        ->setCellValueExplicit ("E$i", $total, PHPExcel_Cell_DataType :: TYPE_NUMERIC)
        ->setCellValueExplicit ("F$i", $porcentaje, PHPExcel_Cell_DataType :: TYPE_NUMERIC);
        if( strlen($_POST['dcuentas'][$xx]) < 6){
			$objPHPExcel->getActiveSheet()->getStyle("A$i:F$i")->getFont()->setBold(true);
		}else{
			$objPHPExcel->getActiveSheet()->getStyle("A$i:F$i");
		}

		$i++;
    }
		
	//----Propiedades de la hoja 1
	$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth('40');
	$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->setTitle('VARIACIONES');
	$objPHPExcel->setActiveSheetIndex(0);

//----Guardar documento----
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename="REPORTE VARIACIONES TRIMESTRALES.xls"');
header('Cache-Control: max-age=0');
 
$objWriter=PHPExcel_IOFactory::createWriter($objPHPExcel,'Excel5');
$objWriter->save('php://output');
exit;

?>