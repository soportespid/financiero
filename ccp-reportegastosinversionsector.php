<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=iso-8859-1");
	require 'comun.inc';
	require 'funciones.inc';
	session_start();
	$linkbd=conectar_v7();
	$linkbd -> set_charset("utf8");
	cargarcodigopag($_GET[codpag],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es">
	<head>
		<meta http-equiv="Content-type" content="text/html; charset=iso-8859-1"/>
		<meta http-equiv="X-UA-Compatible" content="IE=9"/>
		<title>:: Spid - Gesti&oacute;n Humana</title>
		<link href="css/css2.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<script type="text/javascript" src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
        <link href="css/style.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<script>
			function excell()
			{
				document.form2.action="ccp-reportegastosinversionsectorexcel.php";
				document.form2.target="_BLANK";
				document.form2.submit();
				document.form2.action="";
				document.form2.target="";
			}
		</script>
		<?php
			titlepag();
			function nombresector($sector)
			{
				$linkbd=conectar_v7();
				$sql="SELECT MAX(version) FROM ccpetsectores";
				$res=mysqli_query($linkbd,$sql);
				$row=mysqli_fetch_row($res);
				$vermax=$row[0];
				$sql="SELECT nombre FROM ccpetsectores WHERE codigo='$sector' AND version='$vermax'";
				$res=mysqli_query($linkbd,$sql);
				$row=mysqli_fetch_row($res);
				return($row[0]);
			}
			function nombrefuente($fuente)
			{
				$linkbd=conectar_v7();
				$sql="SELECT MAX(version) FROM ccpet_fuentes";
				$res=mysqli_query($linkbd,$sql);
				$row=mysqli_fetch_row($res);
				$vermax=$row[0];
				$sql="SELECT fuente_financiacion FROM ccpet_fuentes WHERE id='$fuente' AND version='$vermax'";
				$res=mysqli_query($linkbd,$sql);
				$row=mysqli_fetch_row($res);
				return($row[0]);
			}
			function nombreproyecto($proyecto)
			{
				$linkbd=conectar_v7();
				$sql="SELECT nombre FROM ccpproyectospresupuesto WHERE id='$proyecto'";
				$res=mysqli_query($linkbd,$sql);
				$row=mysqli_fetch_row($res);
				return($row[0]);
			}
		?>
	</head>
	<body>
		<IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
		<span id="todastablas2"></span>
		<table>
			<tr><script>barra_imagenes("ccpet");</script><?php cuadro_titulos();?></tr>
			<tr><?php menu_desplegable("ccpet");?></tr>
			<tr>
				<td colspan="3" class="cinta"><img src="imagenes/add.png" title="Nuevo" onClick="location.href='ccp-reportegastosinversionsector.php'" class="mgbt"/><img src="imagenes/guardad.png" class="mgbt1"/><img src="imagenes/busca.png" title="Buscar" onClick="document.form2.submit();" class="mgbt"/><img src="imagenes/nv.png" title="Nueva Ventana" class="mgbt" onClick="mypop=window.open('ccp-principal.php','','');mypop.focus();"><img src="imagenes/excel.png" title="Excel" onClick='excell()' class="mgbt"/><img src='imagenes/iratras.png' title='Men&uacute; Nomina' class='mgbt' onClick="location.href='ccp-reportegastosinversion.php'"/></td>
			</tr>
		</table>
		<form name="form2" method="post" action="">
			<input type="hidden" name="iddeshff" id="iddeshff" value="<?php echo $_POST[iddeshff];?>"/>
			<table class="inicio ancho">
				<tr>
					<td class="titulos" colspan="5">.: Sector:</td>
					<td class="cerrar" style="width:7%" onClick="location.href='ccp-principal.php'">Cerrar</td>
				</tr>
				<tr>
					<td class="saludo1" style="width:3cm;">Numero:</td>
					<td style="width:20%;"><input type="search" style="width:100%; height:30px;" name="numero" id="numero" value="<?php echo @ $_POST['numero'];?>"/></td>
					<td style="padding-bottom:0px" colspan="2"><em class="botonflecha" onClick="document.form2.submit();">Buscar</em></td>
					<td colspan="2"></td>
				</tr>
			</table>
			<input type="hidden" name="oculto" id="oculto" value="1" />
			<div class="subpantalla" style="height:64.5%; width:99.6%; overflow-x:hidden;">
				<?php
					if ($_POST['numero']!=""){$crit1="WHERE sector like '".$_POST['numero']."'";}
					else{$crit1=" ";}
					$sqlr="SELECT sector,codproyecto FROM ccpproyectospresupuesto_productos $crit1 GROUP BY sector DESC";
					$resp = mysqli_query($linkbd,$sqlr);
					$ntr = mysqli_num_rows($resp);
					$con=1;
					echo "
					<table class='inicio' align='center'>
						<tr>
							<td colspan='9' class='titulos'>.: Resultados Busqueda:</td>
						</tr>
						<tr><td colspan='9'>Vacaciones Encontradas: $ntr</td></tr>
						<tr>
							<td class='titulos2' style='width:5%'>No</td>
							<td class='titulos2' >Sector</td>
							<td class='titulos2' >Proyecto</td>
							<td class='titulos2' >Fuente</td>
							<td class='titulos2' >Rubro</td>
							<td class='titulos2'>Valor CSF</td>
							<td class='titulos2'>Valor SSF</td>
							<td class='titulos2'>Valor Total CSF</td>
							<td class='titulos2'>Valor Total SSF</td>
						</tr>";
					$iter='saludo1a';
					$iter2='saludo2';
					while ($row =mysqli_fetch_row($resp))
					{
						$sumcsf=$sumssf=0;
						$nomsector=nombresector($row[0]);
						$sqlr2="
						SELECT DISTINCT (T1.id),T1.valorcsf,T1.valorssf,T1.rubro,T1.id_fuente,T1.codproyecto
						FROM ccpproyectospresupuesto_presupuesto AS T1
						INNER JOIN ccpproyectospresupuesto_productos AS T2
						ON T2.codproyecto=T1.codproyecto AND T2.sector='$row[0]'
						ORDER BY T1.codproyecto";
						$resp2 = mysqli_query($linkbd,$sqlr2);
						$ntr2 = mysqli_num_rows($resp2);
						if($ntr2== 0){$nrow=$listantr[]=1;}
						else {$nrow=$listantr[]=$ntr2;}
						$bandera=1;
						$nomfuente="";
						if($ntr2!=0)
						{
							while ($row2 =mysqli_fetch_row($resp2))
							{
								$nomfuente=nombrefuente($row2[4]);
								$nomproyecto=nombreproyecto($row2[5]);
								if($bandera==1)
								{
									$sqlr3="
									SELECT DISTINCT (T1.id),T1.valorcsf,T1.valorssf
									FROM ccpproyectospresupuesto_presupuesto AS T1
									INNER JOIN ccpproyectospresupuesto_productos AS T2
									ON T2.codproyecto=T1.codproyecto AND T2.sector='$row[0]'
									ORDER BY T1.codproyecto";
									$resp3 = mysqli_query($linkbd,$sqlr3);
									while ($row3 =mysqli_fetch_row($resp3))
									{
										$sumcsf+=$row3[1];
										$sumssf+=$row3[2];
									}
									$listasector[]="$row[0] - $nomsector";
									$listaproyecto[]="$row2[5] - $nomproyecto";
									$listafuente[]="$row2[4] - $nomfuente";
									$listarubro[]=$row2[3];
									$listacsf[]=$row2[1];
									$listassf[]=$row2[2];
									$listasumcsf[]=$sumcsf;
									$listasumssf[]=$sumssf;
									echo"
									<tr class='$iter'>
										<td rowspan='$nrow'>$con</td>
										<td rowspan='$nrow'>$row[0] - $nomsector</td>
										<td>$row2[5] - $nomproyecto</td>
										<td>$row2[4] - $nomfuente</td>
										<td>$row2[3]</td>
										<td style='text-align:right;'>$$row2[1]</td>
										<td style='text-align:right;'>$$row2[2]</td>
										<td rowspan='$nrow' style='text-align:right;'>$".number_format($sumcsf,0,',','.')."</td>
										<td rowspan='$nrow' style='text-align:right;'>$".number_format($sumssf,0,',','.')."</td>
									</tr>";
									$bandera=0;
								}
								else
								{
									$listaproyecto[]="$row2[5] - $nomproyecto";
									$listafuente[]="$row2[4] - $nomfuente";
									$listarubro[]=$row2[3];
									$listacsf[]=$row2[1];
									$listassf[]=$row2[2];
									echo"
									<tr class='$iter'>
										<td>$row2[5] - $nomproyecto</td>
										<td>$row2[4] - $nomfuente</td>
										<td>$row2[3]</td>
										<td style='text-align:right;'>$".number_format($row2[1],0,',','.')."</td>
										<td style='text-align:right;'>$".number_format($row2[2],0,',','.')."</td>
									</tr>";
								}
							}
						}
						else
						{
							$listasector[]="$row[0] - $nomsector";
							$listaproyecto[]="---";
							$listafuente[]="---";
							$listarubro[]="---";
							$listacsf[]=0;
							$listassf[]=0;
							$listasumcsf[]=0;
							$listasumssf[]=0;
							echo"
							<tr class='$iter'>
								<td rowspan='$nrow'>$con</td>
								<td rowspan='$nrow'>$row[0] - $nomsector</td>
								<td>---</td>
								<td>---</td>
								<td>---</td>
								<td style='text-align:right;'>$".number_format(0,0,',','.')."</td>
								<td style='text-align:right;'>$".number_format(0,0,',','.')."</td>
								<td rowspan='$nrow' style='text-align:right;'>$".number_format(0,0,',','.')."</td>
								<td rowspan='$nrow' style='text-align:right;'>$".number_format(0,0,',','.')."</td>
							</tr>";
						}
						$aux=$iter;
						$iter=$iter2;
						$iter2=$aux;
						$con++;
					}
					echo"
					</table>
					<input type='hidden' name='lista_ntr' value='".serialize($listantr)."'/>
					<input type='hidden' name='lista_sector' value='".serialize($listasector)."'/>
					<input type='hidden' name='lista_proyecto' value='".serialize($listaproyecto)."'/>
					<input type='hidden' name='lista_fuente' value='".serialize($listafuente)."'/>
					<input type='hidden' name='lista_rubro' value='".serialize($listarubro)."'/>
					<input type='hidden' name='lista_csf' value='".serialize($listacsf)."'/>
					<input type='hidden' name='lista_ssf' value='".serialize($listassf)."'/>
					<input type='hidden' name='lista_sumcsf' value='".serialize($listasumcsf)."'/>
					<input type='hidden' name='lista_sumssf' value='".serialize($listasumssf)."'/>
					";
				?>
			</div>
		</form>
	</body>
</html>
