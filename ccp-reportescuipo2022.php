<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require "comun.inc";
	require "funciones.inc";
	sesion();
	$linkbd=conectar_v7();
	$linkbd -> set_charset("utf8");
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
	ini_set('max_execution_time', 1800);
?>
<!doctype >
<html lang="es">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1">
		<meta name="viewport" content="user-scalable=no">
		<title>:: Ideal 10 - Informes</title>
		<link href="favicon.ico" rel="shortcut icon"/>
		<link href="css/css2z.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet"/>
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css">
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css">
		<script type="text/javascript" src="css/calendario.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script type="text/javascript" src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script type='text/javascript' src='JQuery/jquery-2.1.4.min.js?<?php echo date('d_m_Y_h_i_s');?>'></script>
        <link href="css/style.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
		<script>
			function pdf(){
				document.form2.action = "pdfejecuciongastos.php";
				document.form2.target = "_BLANK";
				document.form2.submit();
				document.form2.action = "";
				document.form2.target = "";
			}
			function excell(){
				document.form2.action = "presu-ejecuciongastosexcel.php";
				document.form2.target = "_BLANK";
				document.form2.submit();
				document.form2.action = "";
				document.form2.target = "";
			}
			function callprogress(vValor){
				document.getElementById("getprogress").innerHTML = vValor;
				document.getElementById("getProgressBarFill").innerHTML = '<div class="ProgressBarFill" style="width: '+vValor+'%;"></div>';
				//document.getElementById("titulog1").style.display='block';
				document.getElementById("progreso").style.display='block';
				document.getElementById("getProgressBarFill").style.display='block';
			}
			function callprogress2(vValor){
				document.getElementById("getprogress2").innerHTML = vValor;
				document.getElementById("getProgressBarFill2").innerHTML = '<div class="ProgressBarFill2" style="width: '+vValor+'%;"></div>';
				document.getElementById("progreso2").style.display='block';
				document.getElementById("getProgressBarFill2").style.display='block';
			}
			function detallea(id, cuenta){
				if($('#detalle'+id).css('display')=='none'){
					$('#detalle'+id).css('display','block');
					$('#img'+id).attr('src','imagenes/minus.gif');
				}
				else{
					$('#detalle'+id).css('display','none');
					$('#img'+id).attr('src','imagenes/plus.gif');
				}
				var toLoad= 'cpp-reportescuipodetalle1_2022.php';
				$.post(toLoad,{cuenta:cuenta},function (data){
					$('#detalle'+id).html(data.detalle);
					return false;
				},'json');
			}
			function detalleb(id, cuenta, cpc, fuente){
				if($('#detalle'+id).css('display')=='none'){
					$('#detalle'+id).css('display','block');
					$('#img'+id).attr('src','imagenes/minus.gif');
				}
				else{
					$('#detalle'+id).css('display','none');
					$('#img'+id).attr('src','imagenes/plus.gif');
				}
				var toLoad= 'cpp-reportescuipodetalle2.php';
				$.post(toLoad,{cuenta:cuenta,cpc:cpc,fuente:fuente},function (data){
					$('#detalle'+id).html(data.detalle);
					return false;
				},'json');
			}
			function detallec(id, cuenta, vigengasto, seccpresu, progrmga, bpin){
				if($('#detalle'+id).css('display')=='none'){
					$('#detalle'+id).css('display','block');
					$('#img'+id).attr('src','imagenes/minus.gif');
				}
				else{
					$('#detalle'+id).css('display','none');
					$('#img'+id).attr('src','imagenes/plus.gif');
				}
				var toLoad= 'cpp-reportescuipodetalle3.php';
				$.post(toLoad,{cuenta:cuenta, vigengasto:vigengasto, vigengasto:vigengasto, seccpresu:seccpresu, progrmga:progrmga, bpin:bpin},function (data){
					$('#detalle'+id).html(data.detalle);
					return false;
				},'json');
			}
			function detalled(id, cuenta, seccpresupuestal, programatico, cpc, fuente, bpin, vigengasto, numrp){
				if($('#detalle'+id).css('display')=='none'){
					$('#detalle'+id).css('display','block');
					$('#img'+id).attr('src','imagenes/minus.gif');
				}
				else{
					$('#detalle'+id).css('display','none');
					$('#img'+id).attr('src','imagenes/plus.gif');
				}
				var toLoad= 'cpp-reportescuipodetalle4.php';
				$.post(toLoad,{cuenta:cuenta,seccpresupuestal:seccpresupuestal,programatico:programatico,cpc:cpc,fuente:fuente,bpin:bpin, vigengasto:vigengasto, numrp:numrp},function (data){
					$('#detalle'+id).html(data.detalle);
					return false;
				},'json');
			}
			function cargararchivo(){
				let numinforme = document.form2.reporte.value;
				if(numinforme != -1){
					let nominforme = '';
					let extarchivo = '';
					let tentidad = document.form2.tipoentidad.value;
					if (tentidad == ''){tentidad = 'EG';}
					switch (tentidad){
						case 'EG':	extarchivo = '.txt';break;
						case 'EO':	extarchivo = 'b.txt';break;
						case 'RS':	extarchivo = '_AESGPRI.txt';break;
						case 'RG':	extarchivo = 'c.txt';
					}
					switch (numinforme){
						case "1":	nominforme = "A_PROGRAMACION_DE_INGRESOS";break;
						case "2":	nominforme = "B_EJECUCION_DE_INGRESOS";break;
						case "3":	nominforme = "C_PROGRAMACION_DE_GASTOS";break;
						case "4":	nominforme = "D_EJECUCION_DE_GASTOS";break;
						case "5":	nominforme = "E_ID_SECCIONES_PRESUPUESTALES_ADICIONALES";break;
						case "6":	nominforme = "TODAS_LAS_CATEGORIAS";break;
					}
					let nomarchivo = "descargartxt.php?id=" + nominforme + extarchivo + "&dire=archivos";
					document.form2.action = nomarchivo;
					document.form2.target = "_BLANK";
					document.form2.submit();
					document.form2.action = "";
					document.form2.target = "";
				}
			}
			function validar(){
				let periodo = document.form2.periodos.value;
				let reporte = document.form2.reporte.value;
				if(periodo != '' && reporte != '-1'){
					generarinf();
				}
			}
			function generarinf(){
				document.form2.oculto.value = "2";
				document.form2.submit();
			}
			function auxNormas(cuenta){
				mypop=window.open('ventana-normascuipo.php?cuenta='+cuenta,'','menubar=0,scrollbars=yes, toolbar=no, location=no, width=900px,height=500px');
				mypop.focus();
			}
			function aux_dsectorial(cuenta){
				mypop=window.open('ventana-dsectorialcuipo.php?cuenta='+cuenta,'','menubar=0,scrollbars=yes, toolbar=no, location=no, width=900px,height=250px');
				//mypop.focus();
			}
			function cargarcsv(ruta) {
				const link = document.createElement('a');
				link.href = ruta;
				link.download = ruta.split('/').pop();
				document.body.appendChild(link);
				link.click();
				document.body.removeChild(link);
			}

		</script>
		<?php
			function buscarfuentecuipo($codigo){
				$linkbd = conectar_v7();
				$linkbd -> set_charset("utf8");
				$sqlmax = "SELECT MAX(version) FROM ccpet_fuentes_cuipo";
				$resmax = mysqli_query($linkbd, $sqlmax);
				$rowmax = mysqli_fetch_row($resmax);
				$sql = "SELECT concuipocpc FROM ccpet_fuentes_cuipo WHERE codigo_fuente = '$codigo' AND version = '$rowmax[0]'";
				$res = mysqli_query($linkbd,$sql);
				$totalcli=mysqli_num_rows($res);
				$row = mysqli_fetch_row($res);
				return($row[0]);
			}
			function buscarcpccuipo($codigo){
				$linkbd=conectar_v7();
				$linkbd -> set_charset("utf8");
				if(substr($codigo,0,1) <= 4){
					$sqlmax = "SELECT MAX(version) FROM ccpetbienestransportables";
					$resmax = mysqli_query($linkbd, $sqlmax);
					$rowmax = mysqli_fetch_row($resmax);
					$sql = "SELECT concuipocpc FROM ccpetbienestransportables WHERE cpc = '$codigo' AND version = '$rowmax[0]'";
				}
				else{
					$sqlmax = "SELECT MAX(version) FROM ccpetservicios";
					$resmax = mysqli_query($linkbd, $sqlmax);
					$rowmax = mysqli_fetch_row($resmax);
					$sql = "SELECT concuipocpc FROM ccpetservicios WHERE cpc = '$codigo' AND version = '$rowmax[0]'";
				}
				$res = mysqli_query($linkbd,$sql);
				$row = mysqli_fetch_row($res);
				return($row[0]);
			}
			function buscarprogramatico($codigo,$tipo){
				$linkbd=conectar_v7();
				$linkbd -> set_charset("utf8");
				//$codm1=substr($codigo,0,7);
				//if($tipo == '1'){$sqlproducto = "SELECT concuipom1 FROM ccpetproductos WHERE cod_producto LIKE '$codm1%'";}
				//else {$sqlproducto = "SELECT concuipom2 FROM ccpetproductos WHERE cod_producto LIKE '$codm1%'";}
				if($tipo == '1'){$sqlproducto = "SELECT concuipom1 FROM ccpetproductos WHERE codigo_indicador LIKE '$codigo'";}
				else {$sqlproducto = "SELECT concuipom2 FROM ccpetproductos WHERE codigo_indicador LIKE '$codigo'";}
				$resproducto = mysqli_query($linkbd,$sqlproducto);
				$rowproducto = mysqli_fetch_row($resproducto);
				return($rowproducto[0]);
			}
			function fcolorlinea($valor1,$valor2,$valor3){
				if($valor1 == $valor2 && $valor2 == $valor3){$color='';}
				else if($valor1 >= $valor2 && $valor2 >= $valor3){$color="style='background-color:rgba(25,151,255,1.00) !important;'";}
				else {$color="style='background-color:rgba(246,17,21,0.93) !important;'";}
				return $color;
			}
			function busca_tercero_chip($tercero){
				$linkbd = conectar_v7();
				$linkbd -> set_charset("utf8");
				$sql = "SELECT codigo FROM ccpet_chip WHERE nit LIKE '$tercero%' AND version = (SELECT MAX(version) FROM ccpet_chip )";
				$res = mysqli_query($linkbd, $sql);
				$row = mysqli_fetch_row($res);
				if($row[0] != ''){
					$codigo = $row[0];
				}else{
					$codigo = 1;
				}
				return $codigo;
			}
			function nombre_tercero_chip($tercero){
				$linkbd = conectar_v7();
				$linkbd -> set_charset("utf8");
				$sql = "SELECT nombre FROM ccpet_chip WHERE codigo LIKE '$tercero%' AND version = (SELECT MAX(version) FROM ccpet_chip )";
				$res = mysqli_query($linkbd, $sql);
				$row = mysqli_fetch_row($res);
				$nombre = $row[0];
				return $nombre;
			}

		?>
	</head>
	<body>
		<IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
		<span id="todastablas2"></span>
		<table>
			<tr>
				<script>barra_imagenes("info");</script><?php cuadro_titulos(); ?></tr>
			<tr><?php menu_desplegable("info"); ?></tr>
			<tr>
				<?php
					$informes = [];
					$informes[1] = "A_PROGRAMACION_DE_INGRESOS";
					$informes[2] = "B_EJECUCION_DE_INGRESOS";
					$informes[3] = "C_PROGRAMACION_DE_GASTOS";
					$informes[4] = "D_EJECUCION_DE_GASTOS";
					$informes[5] = "E_ID_SECCIONES_PRESUPUESTALES_ADICIONALES";
					$informes[6] = "TODAS_LAS_CATEGORIAS";
				?>
				<td colspan="3" class="cinta">
					<img src='imagenes/add.png' title='Nuevo' onClick="location.href='ccp-reportescuipo2022.php'" class="mgbt">
					<img src="imagenes/guardad.png" title="Guardar" class="mgbt1"><img src="imagenes/busca.png" title="Buscar" onClick="document.form2.submit()" class="mgbt"><img src="imagenes/nv.png" title="Nueva Ventana" onClick="mypop=window.open('info-principal.php','','');mypop.focus();" class="mgbt"><img src="imagenes/print.png" title="imprimir" onClick="pdf()" class="mgbt">
					<img src="imagenes/csv.png" title="cs" onclick="cargarcsv('<?php echo  "archivos/" . $_SESSION['usuario'] . "informecgr" . $fec . ".csv"; ?>')">

					<img src="imagenes/contraloria.png" onclick="cargararchivo()" title="Archivo Contraloria" class="mgbt1">
					<img src="imagenes/iratras.png" title="Atr&aacute;s" onClick="location.href='ccp-menucuipo.php'" class="mgbt"/>
				</td>
			</tr>
		</table>
		<form name="form2" method="post" action="">
			<?php
				$sqlr = "SELECT * FROM configbasica WHERE estado = 'S'";
				$res = mysqli_query($linkbd,$sqlr);
				while ($row = mysqli_fetch_row($res)){
					$_POST['nitentidad'] = $row[0];
					$_POST['entidad'] = $row[1];
					$_POST['codent'] = $row[8];
					$_POST['tipoentidad'] = $row[16];
				}
				if($_POST['oculto']==''){
					$_POST['vigeactual'] = vigencia_usuarios($_SESSION['cedulausu']);
					$_POST['tipodepago'] = '2';
				}
				$vigusu = $_POST['vigeactual'];
				if($_POST['tipoentidad'] == 'RS' ){
					$mod00 = 'AESGPR';
				}else{
					$mod00 = '';
				}


			?>
			<table class="inicio">
				<tr>
					<td class="titulos" colspan="9"><p class="neon_titulos">.: Reportes CUIPO</p></td>
					<td class="cerrar" style="width:7%" onClick="location.href='info-principal.php'">Cerrar</td>
				</tr>
				<tr>
					<td class="tamano01" style="width:2.5cm;">Reporte:</td>
					<td>
						<select name="reporte" id="reporte" onChange="validar()" style="width:98%;height:30px;">
							<option value="-1">Seleccione ....</option>
							<option value="1" <?php if ($_POST['reporte'] == '1') echo "selected" ?>>A. PROGRAMACI&Oacute;N DE INGRESOS <?php echo $mod00?></option>
							<option value="2" <?php if ($_POST['reporte'] == '2') echo "selected" ?>>B. EJECUCI&Oacute;N DE INGRESOS <?php echo $mod00?></option>
							<option value="3" <?php if ($_POST['reporte'] == '3') echo "selected" ?>>C. PROGRAMACI&Oacute;N DE GASTOS <?php echo $mod00?></option>
							<option value="4" <?php if ($_POST['reporte'] == '4') echo "selected" ?>>D. EJECUCI&Oacute;N DE GASTOS <?php echo $mod00?></option>
							<option value="5" <?php if ($_POST['reporte'] == '5') echo "selected" ?>>E. ID SECCIONES PRESUPUESTALES ADICIONALES <?php echo $mod00?></option>
						</select>
					</td>
					<td class="tamano01" style="width:2.5cm;">Vigencia:</td>
					<td>
						<select name="vigeactual" style="width:98%;height:30px;">
							<option value="0">Año</option>
							<?php
								for($i=2025;$i>=2020;$i--){
									echo "<option value='$i'";
									if ($_POST['vigeactual'] == $i) echo "selected";
									echo">$i</option>";
								}
							?>
						</select>
					</td>
					<td class="tamano01" style="width:2.5cm;">Periodos:</td>
					<td>
						<select name="periodos" id="periodos" onChange="validar()" style="width:98%;height:30px;">
							<option value="">Sel..</option>
							<?php
								$sqlr = "SELECT * FROM periodos_cuipo WHERE estado='S' ORDER BY id";
								$resp = mysqli_query($linkbd,$sqlr);
								while ($row = mysqli_fetch_row($resp)){
									if ($row[0] == $_POST['periodos']){
										echo "<option value=$row[0] SELECTED>$row[2]</option>";
										$_POST['periodo'] = $row[1];
										$_POST['cperiodo'] = $row[2];
									}
									else {echo "<option value=$row[0]>$row[2]</option>";}
								}
							?>
						</select>
						<input type="hidden" name="periodo" value="<?php echo $_POST['periodo'] ?>">
						<input type="hidden" name="cperiodo" value="<?php echo $_POST['cperiodo'] ?>">
					</td>
					<td class="tamano01" style="width:2.5cm;">Codigo Entidad:</td>
					<td style="width:16%;"><input name="codent" type="text" value="<?php echo $_POST['codent'] ?>" style="width:98%;height:30px;"></td>
					<td style="padding-bottom:0px"><em class="botonflecha" onClick="generarinf()">Generar</em></td>
					<input type="hidden" value="1" name="oculto">

				</tr>
			</table>
			<input type="hidden" name="tipodepago" id="tipodepago" value="<?php echo $_POST['tipodepago'] ?>">
			<input type="hidden" name="tipoentidad" id="tipoentidad" value="<?php echo $_POST['tipoentidad'] ?>">
			<?php
				$oculto = $_POST['oculto'];
				if ($_POST['oculto']){
					$acumulado = 0;
					$contador = 1;
					$cantCategorias = 1;

					echo "<div class='subpantallap' style='height:66.5%; width:99.6%;overflow-x:hidden'>";

					switch ($_POST['reporte']){
						//PROGRAMACIÓN DE INGRESOS
						case 1:{
							$numid = $c = 0;
							$namearch = "archivos/".$_SESSION['usuario']."informecgr".$fec.".csv";
							$Descriptor1 = fopen($namearch, "w+");
							$namearch2 = "archivos/".$informes[$_POST['reporte']].".txt";
							$Descriptor2 = fopen($namearch2, "w+");
							$namearch3 = "archivos/".$informes[$_POST['reporte']]."b.txt";
							$Descriptor3 = fopen($namearch3, "w+");
							$namearch4 = "archivos/".$informes[$_POST['reporte']]."_AESGPRI.txt";
							$Descriptor4 = fopen($namearch4, "w+");
							$namearch5 = "archivos/".$informes[$_POST['reporte']]."c.txt";
							$Descriptor5 = fopen($namearch5, "w+");

                            unset($nomsector);

							$mes2 = substr($_POST['periodo'], 3, 2);
							$_POST['fecha'] = "01/01/$vigusu";
							$_POST['fecha2'] = intval(date("t", $mes2)) . '/' . $mes2 . '/' . $vigusu;
							preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST['fecha'],$fecha);
							$fechai="$fecha[3]-$fecha[2]-$fecha[1]";
							preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST['fecha2'],$fecha);
							$fechaf = "$fecha[3]-$fecha[2]-$fecha[1]";
                            $modDetSet = "";
							if($_POST['tipoentidad'] == 'RS' ){
								fputs($Descriptor1, "S;".$_POST['codent'].";".$_POST['periodo'].";".$vigusu.";A_PROGRAMACION_DE_INGRESOS_AESGPRI\r\n");
								fputs($Descriptor1, "S;ARBOL DE CONCEPTOS; AESGPRIET; PRESUPUESTO INICIAL; PRESUPUESTO DEFINITIVO\r\n");


								$mod01 = "<td colspan='6' class='titulos'><p class='neon_titulos'>.: A. PROGRAMACI&Oacute;N DE INGRESOS AESGPR:</p></td>";
								$mod02 = "<td class='titulos2' style='width:15%;font-size:14px;'>AESGPRIET</td>";
							}elseif($_POST['tipoentidad'] == 'RG'){


								fputs($Descriptor1, "S;".$_POST['codent'].";".$_POST['periodo'].";".$vigusu.";PROGRAMACION_DE_INGRESOS\r\n");
								fputs($Descriptor1, "S;ARBOL DE CONCEPTOS; TIPO RECURSO; TERCEROS; POLITICA PUBLICA; SITUACION DE FONDOS; PRESUPUESTO INICIAL; PRESUPUESTO DEFINITIVO\r\n");
							}else{

								fputs($Descriptor1, "S;".$_POST['codent'].";".$_POST['periodo'].";".$vigusu.";A_PROGRAMACION_DE_INGRESOS\r\n");
								fputs($Descriptor1, "S;ARBOL DE CONCEPTOS; DETALLE SECTORIAL; PRESUPUESTO INICIAL; PRESUPUESTO DEFINITIVO\r\n");
								$mod01 = "<td colspan='6' class='titulos'><p class='neon_titulos'>.: A. PROGRAMACI&Oacute;N DE INGRESOS:</p></td>";
								$mod02 = "";
                                $modDetSet = "<td class='titulos2' style='width:15%;font-size:14px;'>DETALLE SECTORIAL</td>";
							}
							$fecha_actual = date("d-m-Y");
							fputs($Descriptor2, "S\t" . $_POST['codent'] . "\t" . $_POST['periodo'] . "\t" . $vigusu . "\tA_PROGRAMACION_DE_INGRESOS\r\n");
							fputs($Descriptor3, "S\t" . $_POST['codent'] . "\t" . $_POST['periodo'] . "\t" . $vigusu . "\tA_PROGRAMACION_DE_INGRESOS\r\n");
							fputs($Descriptor4, "S\t" . $_POST['codent'] . "\t" . $_POST['periodo'] . "\t" . $vigusu . "\tA_PROGRAMACION_DE_INGRESOS_AESGPRI\r\n");
							fputs($Descriptor5, "S\t" . $_POST['codent'] . "\t" . $_POST['periodo'] . "\t" . $vigusu . "\tPROGRAMACION_DE_INGRESOS"."\t".$fecha_actual."\r\n");
							if($_POST['tipoentidad'] == 'RG'){
								echo "

									<table class='inicio' align='center'>
										<tr>
											<td colspan='9' class='titulos'><p class='neon_titulos'>.: A. PROGRAMACI&Oacute;N DE INGRESOS:</p></td>
										</tr>
										<tr>
											<td style='width:20%;' id='totalcuentas' colspan='2'>Resultados Encontrados:</td>
											<td colspan='3'>
												<div id='progreso' class='ProgressBar' style='display:none; float:left'>
													<div class='ProgressBarText'><span id='getprogress'></span>&nbsp;% </div>
													<div id='getProgressBarFill'></div>
												</div>
											</td>
										</tr>
										<tr style='text-align:center;'>
											<td class='titulos2' style='width:2px;'><img src='imagenes/plus.gif'></td>
											<td class='titulos2' colspan='2'  style='font-size:14px;'>&Aacute;RBOL DE CONCEPTOS</td>
											<td class='titulos2' style='width:12%;font-size:14px;'>TIPO RECURSO</td>
											<td class='titulos2' style='width:12%;font-size:14px;'>TERCEROS</td>
											<td class='titulos2' style='width:14%;font-size:14px;'>POLITICA PUBLICA</td>
											<td class='titulos2' style='width:15%;font-size:14px;'>SITUACION DE FONDOS</td>
											<td class='titulos2' style='width:15%;font-size:14px;'>PRESUPUESTO INICIAL</td>
											<td class='titulos2'style='width:15%;font-size:14px;'>PRESUPUESTO DEFINITIVO</td>
										</tr>";
							}else{
								echo "
									<table class='inicio' align='center'>
										<tr>
											$mod01
										</tr>
										<tr>
											<td style='width:20%;' id='totalcuentas' colspan='2'>Resultados Encontrados:</td>
											<td colspan='3'>
												<div id='progreso' class='ProgressBar' style='display:none; float:left'>
													<div class='ProgressBarText'><span id='getprogress'></span>&nbsp;% </div>
													<div id='getProgressBarFill'></div>
												</div>
											</td>
										</tr>
										<tr style='text-align:center;'>
											<td class='titulos2' style='width:2px;'><img src='imagenes/plus.gif'></td>
											<td class='titulos2' colspan='2'  style='font-size:14px;'>&Aacute;RBOL DE CONCEPTOS</td>
											$mod02
                                            $modDetSet
											<td class='titulos2' style='width:15%;font-size:14px;'>PRESUPUESTO INICIAL</td>
											<td class='titulos2'style='width:15%;font-size:14px;'>PRESUPUESTO DEFINITIVO</td>
										</tr>";
							}
							if($_SESSION['tablatemporal'] != '' && $_SESSION['tablatemporal'] != null){
								$sqlr = "DROP TABLE ".$_SESSION['tablatemporal'];
								mysqli_query($linkbd,$sqlr);
								$sqlr = "CREATE TABLE ".$_SESSION['tablatemporal']." (id int(11),codigocuenta varchar(100), nombrecuenta varchar(200), vinicialing double, vadicion double, vreduccion double, aesgpriet varchar(15), nomsector varchar(100)) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci";
								mysqli_query($linkbd,$sqlr);
							}
							if($_POST['tipoentidad'] == 'RG'){
								$sqlredglobal ="SELECT codigo, base, nombre, tipo, usuario FROM redglobal WHERE tipo = 'IN' AND cuipo ='S'  ORDER BY id ASC";
							}
							else{
								$sqlredglobal ="SELECT codigo, base, nombre, tipo, usuario FROM redglobal WHERE unirreporte = 'S' AND cuipo ='S'  ORDER BY id ASC";
							}
							$resredglobal = mysqli_query($linkbd,$sqlredglobal);
							while ($rowredglobal = mysqli_fetch_row($resredglobal)){
								$numid = $c = 0;
								$linkmulti = conectar_Multi($rowredglobal[1],$rowredglobal[4]);
								$linkmulti -> set_charset("utf8");
								$maxVersion = ultimaVersionIngresosCCPETmulti($rowredglobal[1],$rowredglobal[4]);
								$sqlcta ="SELECT codigo, nombre FROM cuentasingresosccpet WHERE municipio = '1' AND version = '$maxVersion' AND tipo = 'C' AND (codigo_auxiliar = '' OR codigo_auxiliar IS NULL) ORDER BY id ASC";
								$rescta = mysqli_query($linkmulti,$sqlcta);
								$totalcli=mysqli_num_rows($rescta);
								while ($rowcta = mysqli_fetch_row($rescta)){
									$c++;
									if($_POST['tipoentidad'] == 'RS' ){
										unset ($consecpre, $valinicialing, $valadiciones, $valreduccion);
										$sqlinicialing = "SELECT SUM(valor), seccion_presupuestal FROM ccpetinicialing WHERE cuenta = '$rowcta[0]' AND vigencia = '$vigusu' AND vigencia_gasto <> '2' GROUP BY seccion_presupuestal";
										$resinicialing = mysqli_query($linkmulti,$sqlinicialing);
										while($rowinicialing = mysqli_fetch_row($resinicialing)){
											if(esta_en_array($consecpre,$rowinicialing[1]) == false){
												$consecpre[] = $rowinicialing[1];
											}
											if($rowinicialing[0] != ''){
												$valinicialing[$rowinicialing[1]] = round($rowinicialing[0]);
											}else {
												$valinicialing[$rowinicialing[1]] = 0;
											}
										}

										$sqladiciones = "SELECT SUM(valor), seccion_presupuestal FROM ccpet_adiciones WHERE cuenta = '$rowcta[0]' AND vigencia = '$vigusu' AND fecha BETWEEN '$fechai' AND '$fechaf' AND codigo_vigenciag = '1' GROUP BY seccion_presupuestal";
										$resadiciones = mysqli_query($linkmulti,$sqladiciones);
										while($rowadiciones = mysqli_fetch_row($resadiciones)){
											if(esta_en_array($consecpre,$rowadiciones[1]) == false){
												$consecpre[] = $rowadiciones[1];
											}
											if($rowadiciones[0] != ''){
												$valadiciones[$rowadiciones[1]] = round($rowadiciones[0]);
											}else {
												$valadiciones[$rowadiciones[1]] = 0;
											}
										}

										$sqlreduciones = "SELECT SUM(valor), seccion_presupuestal FROM ccpet_reducciones WHERE cuenta = '$rowcta[0]' AND vigencia = '$vigusu' AND fecha BETWEEN '$fechai' AND '$fechaf' AND codigo_vigenciag = '1' GROUP BY seccion_presupuestal";
										$resreduciones = mysqli_query($linkmulti,$sqlreduciones);
										while($rowreduciones = mysqli_fetch_row($resreduciones)){
											if(esta_en_array($consecpre,$rowreduciones[1]) == false){
												$consecpre[] = $rowreduciones[1];
											}
											if($rowreduciones[0] != ''){
												$valreduccion[$rowreduciones[1]]  = round($rowreduciones[0]);
											}else {
												$valreduccion[$rowreduciones[1]]  = 0;
											}
										}

										for($xx = 0; $xx < count($consecpre); $xx++){
											$numid++;
											$sqlaesgpriet = "SELECT aesgpriet FROM pptoseccion_presupuestal WHERE estado = 'S' AND id_seccion_presupuestal = '$consecpre[$xx]'";
											$resaesgpriet = mysqli_query($linkmulti,$sqlaesgpriet);
											$rowaesgpriet = mysqli_fetch_row($resaesgpriet);

											$sqltabla = "INSERT INTO ".$_SESSION['tablatemporal']." (id, codigocuenta, nombrecuenta, vinicialing, vadicion, vreduccion, aesgpriet) VALUES ('$numid', '$rowcta[0]', '$rowcta[1]', '".$valinicialing[$consecpre[$xx]]."', '".$valadiciones[$consecpre[$xx]]."', '".$valreduccion[$consecpre[$xx]]."','$rowaesgpriet[0]')";
											mysqli_query($linkbd,$sqltabla);
										}
									}elseif($_POST['tipoentidad'] == 'RG' ){
										unset ($consecpre, $valinicialing, $valadiciones, $valreduccion);
										$sqlinicialing = "SELECT SUM(valor), medio_pago FROM ccpetinicialing WHERE cuenta = '$rowcta[0]' AND vigencia = '$vigusu' AND vigencia_gasto <> '2' GROUP BY medio_pago";
										$resinicialing = mysqli_query($linkmulti,$sqlinicialing);
										while($rowinicialing = mysqli_fetch_row($resinicialing)){
											if(esta_en_array($consecpre,$rowinicialing[1]) == false){
												$consecpre[] = $rowinicialing[1];
											}
											if($rowinicialing[0] != ''){
												$valinicialing[$rowinicialing[1]] = round($rowinicialing[0]);
											}else {
												$valinicialing[$rowinicialing[1]] = 0;
											}
										}

										$sqladiciones = "SELECT SUM(valor), medio_pago FROM ccpet_adiciones WHERE cuenta = '$rowcta[0]' AND vigencia = '$vigusu' AND fecha BETWEEN '$fechai' AND '$fechaf' AND codigo_vigenciag = '1' GROUP BY medio_pago";
										$resadiciones = mysqli_query($linkmulti,$sqladiciones);
										while($rowadiciones = mysqli_fetch_row($resadiciones)){
											if(esta_en_array($consecpre,$rowadiciones[1]) == false){
												$consecpre[] = $rowadiciones[1];
											}
											if($rowadiciones[0] != ''){
												$valadiciones[$rowadiciones[1]] = round($rowadiciones[0]);
											}else {
												$valadiciones[$rowadiciones[1]] = 0;
											}
										}

										$sqlreduciones = "SELECT SUM(valor), medio_pago FROM ccpet_reducciones WHERE cuenta = '$rowcta[0]' AND vigencia = '$vigusu' AND fecha BETWEEN '$fechai' AND '$fechaf' AND codigo_vigenciag = '1' GROUP BY medio_pago";
										$resreduciones = mysqli_query($linkmulti,$sqlreduciones);
										while($rowreduciones = mysqli_fetch_row($resreduciones)){
											if(esta_en_array($consecpre,$rowreduciones[1]) == false){
												$consecpre[] = $rowreduciones[1];
											}
											if($rowreduciones[0] != ''){
												$valreduccion[$rowreduciones[1]]  = round($rowreduciones[0]);
											}else {
												$valreduccion[$rowreduciones[1]]  = 0;
											}
										}

										for($xx = 0; $xx < count($consecpre); $xx++){
											$numid++;
											$sqltabla = "INSERT INTO ".$_SESSION['tablatemporal']." (id, codigocuenta, nombrecuenta, vinicialing, vadicion, vreduccion, aesgpriet) VALUES ('$numid', '$rowcta[0]', '$rowcta[1]', '".$valinicialing[$consecpre[$xx]]."', '".$valadiciones[$consecpre[$xx]]."', '".$valreduccion[$consecpre[$xx]]."','".$consecpre[$xx]."')";
											mysqli_query($linkbd,$sqltabla);
										}

									}else{
                                        $sqlsector = "SELECT detalle_sectorial FROM ccpecuentastiponormas_cab WHERE cuenta = '$rowcta[0]'";
										$ressector = mysqli_query($linkbd,$sqlsector);
										$rowsector = mysqli_fetch_row($ressector);
										if($rowsector[0] == ''){
											$nomsector = 0;
										}else{
											$nomsector = $rowsector[0];
										}
										$numid++;
										$sqlinicialing = "SELECT SUM(valor) FROM ccpetinicialing WHERE cuenta = '$rowcta[0]' AND vigencia = '$vigusu' AND vigencia_gasto <> '2'";
										$resinicialing = mysqli_query($linkmulti,$sqlinicialing);
										$rowinicialing = mysqli_fetch_row($resinicialing);
										if($rowinicialing[0] != ''){$valinicialing= round($rowinicialing[0]);}
										else {$valinicialing = 0;}

										$sqladiciones = "SELECT SUM(valor) FROM ccpet_adiciones WHERE cuenta = '$rowcta[0]' AND vigencia = '$vigusu' AND fecha BETWEEN '$fechai' AND '$fechaf' AND codigo_vigenciag = '1'";
										$resadiciones = mysqli_query($linkmulti,$sqladiciones);
										$rowadiciones = mysqli_fetch_row($resadiciones);
										if($rowadiciones[0] != ''){$valadiciones = round($rowadiciones[0]);}
										else {$valadiciones = 0;}

										$sqlreduciones = "SELECT SUM(valor) FROM ccpet_reducciones WHERE cuenta = '$rowcta[0]' AND vigencia = '$vigusu' AND fecha BETWEEN '$fechai' AND '$fechaf' AND codigo_vigenciag = '1'";
										$resreduciones = mysqli_query($linkmulti,$sqlreduciones);
										$rowreduciones = mysqli_fetch_row($resreduciones);
										if($rowreduciones[0] != ''){$valreduccion  = round($rowreduciones[0]);}
										else {$valreduccion  = 0;}

										$sqltabla = "INSERT INTO ".$_SESSION['tablatemporal']." (id, codigocuenta, nombrecuenta, vinicialing, vadicion, vreduccion, aesgpriet, nomsector) VALUES ('$numid', '$rowcta[0]', '$rowcta[1]', '$valinicialing', '$valadiciones', '$valreduccion', '-', '$nomsector')";
										mysqli_query($linkbd,$sqltabla);
									}
									$porcentaje = $c * 100 / $totalcli;
									echo"
									<script>
										progres='".round($porcentaje)."';callprogress(progres);
										document.getElementById('totalcuentas').innerHTML='Resultados Encontrados: $numid / $totalcli';
									</script>";
									flush();
									ob_flush();
									usleep(5);
								}
							}
							$xy=1;
							$iter = "filas01";
							$iter2 = "filas02";
							if($_POST['tipoentidad'] == 'RS' ){
								$sqlver = "SELECT id, codigocuenta, nombrecuenta, vinicialing, vadicion, vreduccion, aesgpriet FROM ".$_SESSION['tablatemporal']." ORDER BY id ASC";
							}elseif($_POST['tipoentidad'] == 'RG' ){
								$sqlver = "SELECT id, codigocuenta, nombrecuenta, vinicialing, vadicion, vreduccion, aesgpriet FROM ".$_SESSION['tablatemporal']." ORDER BY id ASC";
							}else{
								$sqlver="SELECT id, codigocuenta, nombrecuenta, SUM(vinicialing), SUM(vadicion), SUM(vreduccion), nomsector FROM ".$_SESSION['tablatemporal']." GROUP BY codigocuenta ORDER BY id ASC";
							}
							$resver = mysqli_query($linkbd,$sqlver);
							while ($rowver = mysqli_fetch_row($resver)){
								$sumaini = $rowver[3] ;
								$sumatotal = $sumaini + $rowver[4] - $rowver[5];
								if(($sumaini > 0)||($sumatotal > 0)){
									if($rowver[6] == "CSF"){
										$tip_situacion = 'C';
									}elseif($rowver[6] == "SSF"){
										$tip_situacion = 'S';
									}else{
										$tip_situacion = '';
									}
									if($_POST['tipoentidad'] == 'RS' ){

										fputs($Descriptor1,"D;".$rowver[1].";".$rowver[6].";".$sumaini.";".$sumatotal."\r\n");
										$mod03 = "<td style='text-align:center;'>$rowver[6]</td>";
									}elseif($_POST['tipoentidad'] == 'RG' ){
										$sqlrSGR = "SELECT cuentasgr FROM rubros_cuentasingsgr WHERE rubro LIKE '$rowver[1]%' AND opcion = '2' LIMIT 1";
										$resSGR = mysqli_query($linkbd, $sqlrSGR);
										$rowSGR = mysqli_fetch_row($resSGR);

										$sqlrTerSGR = "SELECT cuentasgr FROM rubros_cuentasingsgr WHERE rubro LIKE '$rowver[1]%' AND opcion = '3' LIMIT 1";
										$resTerSGR = mysqli_query($linkbd, $sqlrTerSGR);
										$rowTerSGR = mysqli_fetch_row($resTerSGR);
										if($rowTerSGR[0] != ''){
											$num_tercero = $rowTerSGR[0];
										}else{
											$num_tercero = $_POST['codent'];
										}

										$sqlrPoliticaSGR = "SELECT cuentasgr FROM rubros_cuentasingsgr WHERE rubro LIKE '$rowver[1]%' AND opcion = '4' LIMIT 1";
										$resPoliticaSGR = mysqli_query($linkbd, $sqlrPoliticaSGR);
										$rowPoliticaSGR = mysqli_fetch_row($resPoliticaSGR);
										if($rowPoliticaSGR[0] != '' && $rowPoliticaSGR[0] != '1000'){
											$politica_pub = $rowPoliticaSGR[0];
										}else{
											$politica_pub = 0;
										}

										fputs($Descriptor1,"D;".$rowver[1].";".$rowSGR[0].";".$num_tercero.";".$politica_pub.";".$tip_situacion.";".$sumaini.";".$sumatotal."\r\n");
									}else{

										fputs($Descriptor1,"D;".$rowver[1].";".$rowver[6].";".$sumaini.";".$sumatotal."\r\n");
										$mod03 = "<td style='text-align:center;'>$rowver[6]</td>";
									}
									fputs($Descriptor2,"D\t".$rowver[1]."\t".$rowver[6]."\t".$sumaini."\t".$sumatotal."\r\n");
									fputs($Descriptor3,"D\t".$rowver[1]."\t".$rowver[6]."\t".$sumaini."\t".$sumatotal."\r\n");
									fputs($Descriptor4,"D\t".$rowver[1]."\t".$rowver[6]."\t".$sumaini."\t".$sumatotal."\r\n");
									fputs($Descriptor5,"D\t".$rowver[1]."\t".$rowSGR[0]."\t".$num_tercero."\t".$politica_pub."\t".$tip_situacion."\t".$sumaini."\t".$sumatotal."\r\n");
									if($_POST['tipoentidad'] == 'RG' ){
										echo "
										<tr class=$iter>
											<td class='titulos2'>
												<a onClick=\"detallea('$xy','$rowver[1]')\" style='cursor:pointer;'>
												<img id='img$xy' src='imagenes/plus.gif'>
												</a>
											</td>
											<td>$rowver[1]</td>
											<td>$rowver[2]</td>
											<td style='text-align:center;'>$rowSGR[0]</td>
											<td style='text-align:center;'>$num_tercero</td>
											<td style='text-align:center;'>$politica_pub</td>
											<td style='text-align:center;'>$rowver[6]</td>
											<td style='text-align:right;'>$".number_format($sumaini,0,',','.')."</td>
											<td style='text-align:right;'>$".number_format($sumatotal,0,',','.')."</td>
										</tr>
										<tr>
										<td align='center'></td>
											<td colspan='16'>
												<div id='detalle$xy' style='display:none'></div>
											</td>
										</tr>";
									}else{
										echo "
										<tr class=$iter>
											<td class='titulos2'>
												<a onClick=\"detallea('$xy','$rowver[1]')\" style='cursor:pointer;'>
												<img id='img$xy' src='imagenes/plus.gif'>
												</a>
											</td>
											<td>$rowver[1]</td>
											<td>$rowver[2]</td>
											$mod03
											<td style='text-align:right;'>$".number_format($sumaini,0,',','.')."</td>
											<td style='text-align:right;'>$".number_format($sumatotal,0,',','.')."</td>
										</tr>
										<tr>
										<td align='center'></td>
											<td colspan='16'>
												<div id='detalle$xy' style='display:none'></div>
											</td>
										</tr>";
									}
									$aux = $iter;
									$iter = $iter2;
									$iter2 = $aux;

									$xy++;
								}
							}
							echo "
								</table>
							";
							fclose($Descriptor1);
							fclose($Descriptor2);
							fclose($Descriptor3);
							fclose($Descriptor4);
							fclose($Descriptor5);
							echo "<script>document.getElementById('progreso').style.display='none';</script>";

						}break;
						//EJECUCION DE INGRESOS
						case 2:{
							unset ($numerocuenta, $nombrecuenta, $varcpc, $varnorma1, $varnorma2, $varnorma3, $varnorma4, $valdestiesp, $varfuentes, $vartercerochip, $varpolpublicas, $valrecactualscf, $valrecactualccf, $valrecanteriorscf, $valrecanteriorccf, $valtotalcuenta, $tablasinfo, $varfuentes2, $detsectorial, $sec_presupuestal);
							$numid=$c=$idtemporal=0;
							$namearch = "archivos/".$_SESSION['usuario']."informecgr".$fec.".csv";
							$Descriptor1 = fopen($namearch, "w+");
							$namearch2 = "archivos/".$informes[$_POST['reporte']].".txt";
							$Descriptor2 = fopen($namearch2, "w+");
							$namearch3 = "archivos/".$informes[$_POST['reporte']]."b.txt";
							$Descriptor3 = fopen($namearch3, "w+");
							$namearch4 = "archivos/".$informes[$_POST['reporte']]."_AESGPRI.txt";
							$Descriptor4 = fopen($namearch4, "w+");
							$namearch5 = "archivos/".$informes[$_POST['reporte']]."c.txt";
							$Descriptor5 = fopen($namearch5, "w+");

							if($_POST['tipoentidad'] == 'RS' ){

								fputs($Descriptor1, "S;".$_POST['codent'].";".$_POST['periodo'].";".$vigusu.";B_EJECUCION_DE_INGRESOS_AESGPRI\r\n");
								fputs($Descriptor1, "S;ARBOL DE CONCEPTOS;AESGPRIET;TOTAL RECAUDO\r\n");
								$mod01 = "<td colspan='7' class='titulos'><p class='neon_titulos'>.: B. EJECUCI&Oacute;N DE INGRESOS AESGPR:</p></td>";
								$mod02 = "<td class='titulos2'>AESGPRIET</td>";

								echo "
								<table class='inicio' align='center'>
									<tr>
										$mod01
									</tr>
									<tr>
										<td style='width:20%;' colspan='4' id='totalcuentas'></td>
										<td colspan='3'>
											<div id='progreso' class='ProgressBar' style='display:none; float:left'>
												<div class='ProgressBarText'><span id='getprogress'></span>&nbsp;% </div>
												<div id='getProgressBarFill'></div>
											</div>
										</td>
										<td colspan='4'>
											<div id='progreso2' class='ProgressBar2' style='display:none; float:left'>
												<div class='ProgressBarText2'><span id='getprogress2'></span>&nbsp;% </div>
												<div id='getProgressBarFill2'></div>
											</div>
										</td>
										<td colspan='5' id='titulosubproceso'></td>
									</tr>
									<tr>
										<td class='titulos2' style='width:2px;'><img src='imagenes/plus.gif'></td>
										<td class='titulos2' colspan='2'>&Aacute;RBOL DE CONCEPTOS</td>
										$mod02
										<td class='titulos2'>TOTAL RECAUDO</td>
									</tr>";

							}elseif($_POST['tipoentidad'] == 'RG' ){

								echo"<table class='inicio' align='center'>
										<tr>
											<td colspan='9' class='titulos'><p class='neon_titulos'>.: A. EJECUCI&Oacute;N DE INGRESOS:</p></td>
										</tr>
										<tr>
											<td></td>
											<td style='width:20%;' id='totalcuentas' colspan='2'>Resultados Encontrados:</td>
											<td colspan='3'>
												<div id='progreso' class='ProgressBar' style='display:none; float:left'>
													<div class='ProgressBarText'><span id='getprogress'></span>&nbsp;% </div>
													<div id='getProgressBarFill'></div>
												</div>
											</td>
										</tr>
										<tr style='text-align:center;'>
											<td class='titulos2' style='width:2px;'><img src='imagenes/plus.gif'></td>
											<td class='titulos2' colspan='2'  style='font-size:14px;'>&Aacute;RBOL DE CONCEPTOS</td>
											<td class='titulos2' style='width:12%;font-size:14px;'>TIPO RECURSO</td>
											<td class='titulos2' style='width:12%;font-size:14px;'>BPIM</td>
											<td class='titulos2' style='width:12%;font-size:14px;'>TERCEROS</td>
											<td class='titulos2' style='width:14%;font-size:14px;'>POLITICA PUBLICA</td>
											<td class='titulos2' style='width:15%;font-size:14px;'>SITUACION DE FONDOS</td>
											<td class='titulos2' style='width:15%;font-size:14px;'>RECAUDO</td>
										</tr>";

							}else{

								$mod01 = "<td colspan='18' class='titulos'><p class='neon_titulos'>.: B. EJECUCI&Oacute;N DE INGRESOS:</p></td>";
								$mod02 = "<td class='titulos2'>CPC</td>
									<td class='titulos2'>DETALLE SECTORIAL</td>
									<td class='titulos2'>APLICA DESTINACI&Oacute;N ESPECIFICA</td>
									<td class='titulos2'>TIPO DE NORMA QUE DEFINE LA DESTINACI&Oacute;N</td>
									<td class='titulos2'>NUMERO DE LA NORMA</td>
									<td class='titulos2'>FECHA DE LA NORMA</td>
									<td class='titulos2'>VALOR DESTINACI&Oacute;N ESPECIFICA</td>
									<td class='titulos2'>FUENTES DE FINANCIACION</td>
									<td class='titulos2'>TERCEROS (C&Oacute;DIGO CHIP)</td>
									<td class='titulos2'>POLITICA PUBLICA</td>
									<td class='titulos2'>RECAUDO DE VIGENCIA ACTUAL SIN SITUACI&Oacute;N DE FONDOS</td>
									<td class='titulos2'>RECAUDO DE VIGENCIA ACTUAL CON SITUACI&Oacute;N DE FONDOS</td>
									<td class='titulos2'>RECAUDO DE VIGENCIA ANTERIOR SIN SITUACI&Oacute;N DE FONDOS</td>
									<td class='titulos2'>RECAUDO DE VIGENCIA ANTERIOR CON SITUACI&Oacute;N DE FONDOS</td>";

								echo "
								<table class='inicio' align='center'>
									<tr>
										$mod01
									</tr>
									<tr>
										<td style='width:20%;' colspan='4' id='totalcuentas'></td>
										<td colspan='3'>
											<div id='progreso' class='ProgressBar' style='display:none; float:left'>
												<div class='ProgressBarText'><span id='getprogress'></span>&nbsp;% </div>
												<div id='getProgressBarFill'></div>
											</div>
										</td>
										<td colspan='4'>
											<div id='progreso2' class='ProgressBar2' style='display:none; float:left'>
												<div class='ProgressBarText2'><span id='getprogress2'></span>&nbsp;% </div>
												<div id='getProgressBarFill2'></div>
											</div>
										</td>
										<td colspan='5' id='titulosubproceso'></td>
									</tr>
									<tr>
										<td class='titulos2' style='width:2px;'><img src='imagenes/plus.gif'></td>
										<td class='titulos2' colspan='2'>&Aacute;RBOL DE CONCEPTOS</td>
										$mod02
										<td class='titulos2'>TOTAL RECAUDO</td>
									</tr>";

								fputs($Descriptor1, "S;".$_POST['codent'].";".$_POST['periodo'].";".$vigusu.";B_EJECUCION_DE_INGRESOS\r\n");
								fputs($Descriptor1, "S;ARBOL DE CONCEPTOS;CPC;DETALLE SECTORIAL;FUENTES DE FINANCIACION;TIPO DE NORMA QUE DEFINE LA DESTINACION;NUMERO Y FECHA DE LA NORMA;TERCEROS (CODIGO CHIP);POLITICA PUBLICA;RECAUDO DE VIGENCIA ACTUAL SIN SITUACION DE FONDOS;RECAUDO DE VIGENCIA ACTUAL CON SITUACIÓN DE FONDOS;RECAUDO DE VIGENCIA ANTERIOR SIN SITUACION DE FONDOS;RECAUDO DE VIGENCIA ANTERIOR CON SITUACION DE FONDOS;TOTAL RECAUDO\r\n");


							}

							$fecha_actual = date("d-m-Y");
							fputs($Descriptor2, "S\t".$_POST['codent']."\t".$_POST['periodo']."\t".$vigusu."\tB_EJECUCION_DE_INGRESOS\r\n");
							fputs($Descriptor3, "S\t".$_POST['codent']."\t".$_POST['periodo']."\t".$vigusu."\tB_EJECUCION_DE_INGRESOS\r\n");
							fputs($Descriptor4, "S\t".$_POST['codent']."\t".$_POST['periodo']."\t".$vigusu."\tB_EJECUCION_DE_INGRESOS_AESGPRI\r\n");
							fputs($Descriptor5, "S\t" . $_POST['codent'] . "\t" . $_POST['periodo'] . "\t" . $vigusu . "\tEJECUCION_DE_INGRESOS"."\t".$fecha_actual."\r\n");


							$mes2 = substr($_POST['periodo'], 3, 2);
							$_POST['fecha'] = "01/01/$vigusu";
							$_POST['fecha2'] = intval(date("t", $mes2))."/$mes2/$vigusu";
							preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST['fecha'],$fecha);
							$fechai = "$fecha[3]-$fecha[2]-$fecha[1]";
							preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST['fecha2'],$fecha);
							$fechaf = "$fecha[3]-$fecha[2]-$fecha[1]";

							if($_SESSION['tablatemporal'] != '' && $_SESSION['tablatemporal'] != null){
								$sqlr = "DROP TABLE ".$_SESSION['tablatemporal'];
								mysqli_query($linkbd,$sqlr);
								if($_POST['tipoentidad'] == 'RG'){
									$sqlr = "CREATE TABLE ".$_SESSION['tablatemporal']." (id int(11), codigocuenta varchar(100), nombrecuenta varchar(200), tipoRecurso varchar(100), bpim varchar(100), terceros varchar(100), politicaPublica varchar(100), situacionFondos varchar(100), recaudo double) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;";
									mysqli_query($linkbd,$sqlr);
								}
								else{
									$sqlr = "CREATE TABLE ".$_SESSION['tablatemporal']." (id int(11), codigocuenta varchar(100), nombrecuenta varchar(200), cpc varchar(100), aplicadestinacion varchar(2),tiponorma varchar(100), numeronorma varchar(50), fechanorma date, valordestinacionesp double, fuente varchar(20), tercerochip varchar(10), politicapublica varchar(10), recaudoactualscf double, recaudoactualccf double, recaudoanteriorscf double, recaudoanteriorccf double, totalrecaudo double, tipotabla varchar(100), fuente2 varchar(20), codsectorial int(11), aesgpriet varchar(15)) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;";
									mysqli_query($linkbd,$sqlr);
								}
							}
							if($_POST['tipoentidad'] == 'RG'){
								$sqlredglobal ="SELECT codigo, base, nombre, tipo, usuario FROM redglobal WHERE tipo = 'IN' AND cuipo ='S'   ORDER BY id ASC";
							}
							else{
								$sqlredglobal ="SELECT codigo, base, nombre, tipo, usuario FROM redglobal WHERE unirreporte = 'S' AND cuipo ='S'  ORDER BY id ASC";
							}
							$resredglobal = mysqli_query($linkbd,$sqlredglobal);
							while ($rowredglobal = mysqli_fetch_row($resredglobal)){
								$numid = $c = 0;
								$linkmulti = conectar_Multi($rowredglobal[1],$rowredglobal[4]);
								$linkmulti -> set_charset("utf8");
								$maxVersion = ultimaVersionIngresosCCPETmulti($rowredglobal[1],$rowredglobal[4]);
								$sqlcta = "SELECT codigo, nombre FROM cuentasingresosccpet WHERE municipio = 1 AND version = '$maxVersion' AND tipo = 'C' AND (codigo_auxiliar = '' OR codigo_auxiliar IS NULL) ORDER BY id ASC";
								$rescta = mysqli_query($linkmulti,$sqlcta);
								$totalcli=mysqli_num_rows($rescta);
								while ($rowcta = mysqli_fetch_row($rescta)){
									if($_POST['tipoentidad'] == 'RG'){
										$idtemporalSGR = 1;
										$sqlrIniSGR = "SELECT medio_pago, valor, fuente FROM ccpetinicialing WHERE cuenta = '$rowcta[0]'";
										$resrIniSGR = mysqli_query($linkmulti,$sqlrIniSGR);
										while($rowrIniSGR = mysqli_fetch_row($resrIniSGR)){
											$sqlrParamTipoRecursoSGR = "SELECT cuentasgr FROM rubros_cuentasingejecusgr WHERE rubro LIKE '$rowcta[0]%' AND tipo = 'INICIAL' AND fuente = '$rowrIniSGR[2]' AND opcion = '2'";
											$resrParamTipoRecursoSGR = mysqli_query($linkmulti, $sqlrParamTipoRecursoSGR);
											$rowrParamTipoRecursoSGR = mysqli_fetch_row($resrParamTipoRecursoSGR);

											$sqlrParamBpimSGR = "SELECT cuentasgr FROM rubros_cuentasingejecusgr WHERE rubro LIKE '$rowcta[0]%' AND tipo = 'INICIAL' AND fuente = '$rowrIniSGR[2]' AND opcion = '1'";
											$resrParamBpimSGR = mysqli_query($linkmulti, $sqlrParamBpimSGR);
											$rowrParamBpimSGR = mysqli_fetch_row($resrParamBpimSGR);

											$sqlrParamTerceroSGR = "SELECT cuentasgr FROM rubros_cuentasingejecusgr WHERE rubro LIKE '$rowcta[0]%' AND tipo = 'INICIAL' AND fuente = '$rowrIniSGR[2]' AND opcion = '3'";
											$resrParamTerceroSGR = mysqli_query($linkmulti, $sqlrParamTerceroSGR);
											$rowrParamTerceroSGR = mysqli_fetch_row($resrParamTerceroSGR);

											$sqlrParamPoliticaSGR = "SELECT cuentasgr FROM rubros_cuentasingejecusgr WHERE rubro LIKE '$rowcta[0]%' AND tipo = 'INICIAL' AND fuente = '$rowrIniSGR[2]' AND opcion = '4'";
											$resrParamPoliticaSGR = mysqli_query($linkmulti, $sqlrParamPoliticaSGR);
											$rowrParamPoliticaSGR = mysqli_fetch_row($resrParamPoliticaSGR);

											$politcaPublica = 0;
											if($rowrParamPoliticaSGR[0] != '' && $rowrParamPoliticaSGR[0] != null && $rowrParamPoliticaSGR[0] != 1000){
												$politcaPublica = $rowrParamPoliticaSGR[0];
											}

											$situcacionFondos = '';
											if($rowrIniSGR[0] == 'CSF'){
												$situcacionFondos = 'C';
											}else{
												$situcacionFondos = 'S';
											}


											$sqltabla2 = "INSERT INTO ".$_SESSION['tablatemporal']." (id, codigocuenta, nombrecuenta, tipoRecurso, bpim, terceros, politicaPublica, situacionFondos, recaudo) VALUES ('$idtemporalSGR', '$rowcta[0]', '$rowcta[1]', '$rowrParamTipoRecursoSGR[0]', '$rowrParamBpimSGR[0]', '$rowrParamTerceroSGR[0]', '$politcaPublica', '$situcacionFondos', '$rowrIniSGR[1]')";
											mysqli_query($linkbd,$sqltabla2);

											$idtemporalSGR++;
										}

										$sqlrIniSGR = "SELECT medio_pago, valor, fuente FROM ccpet_adiciones WHERE cuenta = '$rowcta[0]' AND tipo_cuenta = 'I'";
										$resrIniSGR = mysqli_query($linkmulti,$sqlrIniSGR);
										while($rowrIniSGR = mysqli_fetch_row($resrIniSGR)){
											$sqlrParamTipoRecursoSGR = "SELECT cuentasgr FROM rubros_cuentasingejecusgr WHERE rubro LIKE '$rowcta[0]%' AND tipo = 'ADICION' AND fuente = '$rowrIniSGR[2]' AND opcion = '2'";
											$resrParamTipoRecursoSGR = mysqli_query($linkmulti, $sqlrParamTipoRecursoSGR);
											$rowrParamTipoRecursoSGR = mysqli_fetch_row($resrParamTipoRecursoSGR);

											$sqlrParamBpimSGR = "SELECT cuentasgr FROM rubros_cuentasingejecusgr WHERE rubro LIKE '$rowcta[0]%' AND tipo = 'ADICION' AND fuente = '$rowrIniSGR[2]' AND opcion = '1'";
											$resrParamBpimSGR = mysqli_query($linkmulti, $sqlrParamBpimSGR);
											$rowrParamBpimSGR = mysqli_fetch_row($resrParamBpimSGR);

											$sqlrParamTerceroSGR = "SELECT cuentasgr FROM rubros_cuentasingejecusgr WHERE rubro LIKE '$rowcta[0]%' AND tipo = 'ADICION' AND fuente = '$rowrIniSGR[2]' AND opcion = '3'";
											$resrParamTerceroSGR = mysqli_query($linkmulti, $sqlrParamTerceroSGR);
											$rowrParamTerceroSGR = mysqli_fetch_row($resrParamTerceroSGR);

											$sqlrParamPoliticaSGR = "SELECT cuentasgr FROM rubros_cuentasingejecusgr WHERE rubro LIKE '$rowcta[0]%' AND tipo = 'ADICION' AND fuente = '$rowrIniSGR[2]' AND opcion = '4'";
											$resrParamPoliticaSGR = mysqli_query($linkmulti, $sqlrParamPoliticaSGR);
											$rowrParamPoliticaSGR = mysqli_fetch_row($resrParamPoliticaSGR);

											$politcaPublica = 0;
											if($rowrParamPoliticaSGR[0] != '' && $rowrParamPoliticaSGR[0] != null && $rowrParamPoliticaSGR[0] != 1000){
												$politcaPublica = $rowrParamPoliticaSGR[0];
											}

											$situcacionFondos = '';
											if($rowrIniSGR[0] == 'CSF'){
												$situcacionFondos = 'C';
											}else{
												$situcacionFondos = 'S';
											}


											$sqltabla2 = "INSERT INTO ".$_SESSION['tablatemporal']." (id, codigocuenta, nombrecuenta, tipoRecurso, bpim, terceros, politicaPublica, situacionFondos, recaudo) VALUES ('$idtemporalSGR', '$rowcta[0]', '$rowcta[1]', '$rowrParamTipoRecursoSGR[0]', '$rowrParamBpimSGR[0]', '$rowrParamTerceroSGR[0]', '$politcaPublica', '$situcacionFondos', '$rowrIniSGR[1]')";
											mysqli_query($linkbd,$sqltabla2);

											$idtemporalSGR++;
										}
									}
									else{
										$numid++;
										$c++;
										$sqlnormas = "SELECT aplicade,tiponorma,numeronorma,fechanorma FROM ccpecuentastiponormas WHERE cuenta = '$rowcta[0]' AND estado='W'";
										$resnormas = mysqli_query($linkmulti,$sqlnormas);
										$rownormas = mysqli_fetch_row($resnormas);
										$sqlsector = "SELECT detalle_sectorial FROM ccpecuentastiponormas_cab WHERE cuenta = '$rowcta[0]'";
										$ressector = mysqli_query($linkbd,$sqlsector);
										$rowsector = mysqli_fetch_row($ressector);
										if($rowsector[0] == ''){
											$nomsector = 0;
										}else{
											$nomsector = $rowsector[0];
										}
										//RECIBOS DE CAJA
										{
											$cx = 0;
											$sqlreccaja="
											SELECT prc.valor, prc.fuente, prc.productoservicio, prc.idrecibo, prc.seccion_presupuestal
											FROM pptorecibocajappto AS prc
											INNER JOIN tesoreciboscaja AS trc
											ON trc.id_recibos = prc.idrecibo AND prc.cuenta = '$rowcta[0]'
											WHERE prc.vigencia = '$vigusu' AND NOT(trc.estado = 'N' OR trc.estado = 'R') AND NOT(prc.tipo = 'R') AND trc.fecha BETWEEN '$fechai' AND '$fechaf'";//1
											$resreccaja = mysqli_query($linkmulti, $sqlreccaja);
											$totalcx = mysqli_num_rows($resreccaja);
											while ($rowreccaja = mysqli_fetch_row($resreccaja)){
												$cx++;
												if($rowreccaja[4] != ''){
													$sec_presupuestal[] = $rowreccaja[4];
												}else{
													$sec_presupuestal[] = "-";
												}

												$detsectorial[] = $nomsector;
												$numerocuenta[] = $rowcta[0];
												$nombrecuenta[] = $rowcta[1];
												$auxcpc = trim($rowreccaja[2]);
												if($auxcpc != ''){$varcpc[] = $auxcpc;}
												else {$varcpc[] = 'NO APLICA';}
												$varnorma1[] = $rownormas[0];
												$varnorma2[] = $rownormas[1];
												$varnorma3[] = $rownormas[2];
												$varnorma4[] = $rownormas[3];
												if($rownormas[0] == 'SI'){$valdestiesp[] = round($rowreccaja[0]);}
												else {$valdestiesp[] = 0;}
												if($rowreccaja[1] != ''){
													$varfuentes2[] = $rowreccaja[1];
													$auxfuente = $rowreccaja[1];
													if($auxfuente != ''){$varfuentes[] = $auxfuente;}
													else {$varfuentes[] = 'NO APLICA';}
												}
												else{
													$varfuentes[] = 'NO APLICA';
													$varfuentes2[] = 'NO APLICA';
												}
												$vartercerochip[] = 1;
												$varpolpublicas[] = '';
												$valrecactualscf[] = 0;
												$valrecactualccf[] = round($rowreccaja[0]);
												$valrecanteriorscf[] = 0;
												$valrecanteriorccf[] = 0;
												$valtotalcuenta[] = round($rowreccaja[0]);
												$tablasinfo[] = "Recibo de Caja No: $rowreccaja[3]";
												$porcentaje2 = $cx * 100 / $totalcx;
												echo"
												<script>
													progres2 = '".round($porcentaje2)."';
													callprogress2(progres2);
													document.getElementById('titulosubproceso').innerHTML = 'Recibos de caja No: $rowreccaja[3] ($rowcta[0])';
												</script>";
												flush();
												ob_flush();
												usleep(1);
											}
											echo "
											<script>
												document.getElementById('progreso2').style.display='none';
												document.getElementById('titulosubproceso').innerHTML='';
											</script>";
										}
										//SIN RECIBOS DE CAJA
										{
											$cx = 0;
											$sqlsinreccaja="
											SELECT psrc.valor, psrc.fuente, psrc.productoservicio, psrc.idrecibo, psrc.medio_pago, psrc.seccion_presupuestal
											FROM pptosinrecibocajappto AS psrc
											INNER JOIN tesosinreciboscaja AS tsrc
											ON tsrc.id_recibos = psrc.idrecibo AND psrc.cuenta = '$rowcta[0]'
											WHERE psrc.vigencia = '$vigusu' AND NOT(tsrc.estado = 'N' OR tsrc.estado = 'R') AND tsrc.fecha BETWEEN '$fechai' AND '$fechaf'";//2
											$ressinreccaja = mysqli_query($linkmulti, $sqlsinreccaja);
											$totalcx = mysqli_num_rows($ressinreccaja);
											while ($rowsinreccaja = mysqli_fetch_row($ressinreccaja)){
												if($rowsinreccaja[5] != ''){
													$sec_presupuestal[] = $rowsinreccaja[5];
												}else{
													$sec_presupuestal[] = "-";
												}
												$detsectorial[] = $nomsector;
												$numerocuenta[] = $rowcta[0];
												$nombrecuenta[] = $rowcta[1];
												$auxcpc = trim($rowsinreccaja[2]);
												if($auxcpc != ''){$varcpc[] = $auxcpc;}
												else {$varcpc[] = 'NO APLICA';}
												$varnorma1[] = $rownormas[0];
												$varnorma2[] = $rownormas[1];
												$varnorma3[] = $rownormas[2];
												$varnorma4[] = $rownormas[3];
												if($rownormas[0] == 'SI'){$valdestiesp[] = round($rowsinreccaja[0]);}
												else {$valdestiesp[] = 0;}
												if($rowsinreccaja[1] != ''){
													$varfuentes2[] = $rowsinreccaja[1];
													$auxfuente = $rowsinreccaja[1];
													if($auxfuente != ''){$varfuentes[] = $auxfuente;}
													else {$varfuentes[] = 'NO APLICA';}
												}
												else{
													$varfuentes[] = 'NO APLICA';
													$varfuentes2[] = 'NO APLICA';
												}
												$vartercerochip[] = 1;
												$varpolpublicas[] = '';
												if($rowsinreccaja[4] == "SSF"){
													$valrecactualscf[] = round($rowsinreccaja[0]);
													$valrecactualccf[] = 0;
												}
												else{
													$valrecactualscf[] = 0;
													$valrecactualccf[] = round($rowsinreccaja[0]);
												}
												$valrecanteriorscf[] = 0;
												$valrecanteriorccf[] = 0;
												$valtotalcuenta[] = round($rowsinreccaja[0]);
												$tablasinfo[] = "Sin Recibo de Caja No: $rowsinreccaja[3]";
												$porcentaje2 = $cx * 100 / $totalcx;
												echo"
												<script>
													progres2 = '".round($porcentaje2)."';
													callprogress2(progres2);
													document.getElementById('titulosubproceso').innerHTML = 'Sin Recibo de Caja No: $rowsinreccaja[3] ($rowcta[0])';
												</script>";
												flush();
												ob_flush();
												usleep(1);
											}
											echo "
											<script>
												document.getElementById('progreso2').style.display='none';
												document.getElementById('titulosubproceso').innerHTML='';
											</script>";
										}
										//INGRESOS SSF
										/*{
											$cx = 0;
											$sqlingssf="
											SELECT pissf.valor, pissf.idrecibo
											FROM pptoingssf AS pissf
											INNER JOIN tesossfingreso_cab AS tissf
											ON pissf.idrecibo = tissf.id_recaudo AND pissf.cuenta = '$rowcta[0]'
											WHERE pissf.vigencia = '$vigusu' AND NOT(tissf.estado = 'N' OR tissf.estado = 'R') AND tissf.vigencia = '$vigusu' AND tissf.fecha BETWEEN '$fechai' AND '$fechaf'";//3
											$resingssf = mysqli_query($linkmulti, $sqlingssf);
											$totalcx = mysqli_num_rows($resingssf);
											while ($rowingssf = mysqli_fetch_row($resingssf)){
												if($rowingssf[5] != ''){
													$sec_presupuestal[] = $rowingssf[5];
												}else{
													$sec_presupuestal[] = "-";
												}
												$detsectorial[] = $nomsector;
												$numerocuenta[] = $rowcta[0];
												$nombrecuenta[] = $rowcta[1];
												$varcpc[] = 'NO APLICA';
												$varnorma1[] = $rownormas[0];
												$varnorma2[] = $rownormas[1];
												$varnorma3[] = $rownormas[2];
												$varnorma4[] = $rownormas[3];
												if($rownormas[0] == 'SI'){$valdestiesp[] = round($rowingssf[0]);}
												else {$valdestiesp[] = 0;}
												$varfuentes[] = 'NO APLICA';
												$varfuentes2[] = 'NO APLICA';
												$vartercerochip[] = 1;
												$varpolpublicas[] = '';
												$valrecactualscf[] = 0;
												$valrecactualccf[] = round($rowingssf[0]);
												$valrecanteriorscf[] = 0;
												$valrecanteriorccf[] = 0;
												$valtotalcuenta[] = round($rowingssf[0]);
												$tablasinfo[] = "Ingreso SSF No: $rowingssf[1]";
												$porcentaje2 = $cx * 100 / $totalcx;
												echo"
												<script>
													progres2 = '".round($porcentaje2)."';
													callprogress2(progres2);
													document.getElementById('titulosubproceso').innerHTML = 'Ingreso SSF No: $rowingssf[1] ($rowcta[0])';
												</script>";
												flush();
												ob_flush();
												usleep(1);
											}
											echo "
											<script>
												document.getElementById('progreso2').style.display='none';
												document.getElementById('titulosubproceso').innerHTML='';
											</script>";
										}*/
										//NOTAS BANCARIAS
										{
											$cx = 0;
											$sqlnotasban="
											SELECT pnb.valor, pnb.idrecibo, pnb.fuente, pnb.seccion_presupuestal
											FROM pptonotasbanppto AS pnb
											INNER JOIN tesonotasbancarias_cab AS tnp
											ON tnp.id_comp = pnb.idrecibo AND pnb.cuenta = '$rowcta[0]'
											WHERE pnb.vigencia = '$vigusu' AND NOT(tnp.estado = 'R' OR tnp.estado = 'N') AND tnp.vigencia = '$vigusu' AND tnp.fecha BETWEEN '$fechai' AND '$fechaf'";//4
											$resnotasban = mysqli_query($linkmulti, $sqlnotasban);
											$totalcx = mysqli_num_rows($resnotasban);
											while ($rownotasban = mysqli_fetch_row($resnotasban)){
												if($rownotasban[3] != ''){
													$sec_presupuestal[] = $rownotasban[3];
												}else{
													$sec_presupuestal[] = "-";
												}
												$detsectorial[] = $nomsector;
												$numerocuenta[] = $rowcta[0];
												$nombrecuenta[] = $rowcta[1];
												$varcpc[] = 'NO APLICA';
												$varnorma1[] = $rownormas[0];
												$varnorma2[] = $rownormas[1];
												$varnorma3[] = $rownormas[2];
												$varnorma4[] = $rownormas[3];
												if($rownormas[0] == 'SI'){$valdestiesp[] = round($rownotasban[0]);}
												else {$valdestiesp[] = 0;}
												if(trim($rownotasban[2]) == ''){
													$varfuentes[] = 'NO APLICA';
													$varfuentes2[] = 'NO APLICA';
												}
												else{
													$varfuentes[] = $rownotasban[2];
													$varfuentes2[] = $rownotasban[2];
												}

												$vartercerochip[] = 1;
												$varpolpublicas[] = '';
												$valrecactualscf[] = 0;
												$valrecactualccf[] = round($rownotasban[0]);
												$valrecanteriorscf[] = 0;
												$valrecanteriorccf[] = 0;
												$valtotalcuenta[] = round($rownotasban[0]);
												$tablasinfo[] = "Nota Bancaria No: $rownotasban[1]";
												$porcentaje2 = $cx * 100 / $totalcx;
												echo"
												<script>
													progres2 = '".round($porcentaje2)."';
													callprogress2(progres2);
													document.getElementById('titulosubproceso').innerHTML = 'Nota Bancaria No: $rownotasban[1] ($rowcta[0])';
												</script>";
												flush();
												ob_flush();
												usleep(1);
											}
											echo "
											<script>
												document.getElementById('progreso2').style.display='none';
												document.getElementById('titulosubproceso').innerHTML='';
											</script>";
										}
										//RECAUDO TRANSFERENCIA
										{
											$cx = 0;
											$sqlrecatrans="
											SELECT pitp.valor, pitp.fuente, pitp.productoservicio, pitp.idrecibo, titp.mediopago, titp.tercero, pitp.seccion_presupuestal
											FROM pptoingtranppto AS pitp
											INNER JOIN tesorecaudotransferencia AS titp
											ON pitp.idrecibo = titp.id_recaudo AND pitp.cuenta = '$rowcta[0]'
											WHERE pitp.vigencia = '$vigusu' AND NOT(titp.estado = 'N' OR titp.estado = 'R') AND titp.fecha BETWEEN '$fechai' AND '$fechaf' ";//5
											$resrecatrans = mysqli_query($linkmulti, $sqlrecatrans);
											$totalcx = mysqli_num_rows($resrecatrans);
											while ($rowrecatrans = mysqli_fetch_row($resrecatrans)){
												if($rowrecatrans[6] != ''){
													$sec_presupuestal[] = $rowrecatrans[6];
												}else{
													$sec_presupuestal[] = "-";
												}
												$detsectorial[] = $nomsector;
												$numerocuenta[] = $rowcta[0];
												$nombrecuenta[] = $rowcta[1];
												$auxcpc = trim($rowrecatrans[2]);
												if($auxcpc != ''){$varcpc[] = $auxcpc;}
												else {$varcpc[] = 'NO APLICA';}
												$varnorma1[] = $rownormas[0];
												$varnorma2[] = $rownormas[1];
												$varnorma3[] = $rownormas[2];
												$varnorma4[] = $rownormas[3];
												if($rownormas[0] == 'SI'){$valdestiesp[] = round($rowrecatrans[0]);}
												else {$valdestiesp[] = 0;}
												if($rowrecatrans[1] != ''){
													$varfuentes2[] = $rowrecatrans[1];
													$auxfuente = $rowrecatrans[1];
													if($auxfuente != ''){$varfuentes[] = $auxfuente;}
													else {$varfuentes[] = 'NO APLICA';}
												}
												else{
													$varfuentes[] = 'NO APLICA';
													$varfuentes2[] = 'NO APLICA';
												}
												$vartercerochip[] = busca_tercero_chip($rowrecatrans[5]);
												$varpolpublicas[] = '';
												if($rowrecatrans[4] == '1'){
													$valrecactualscf[] = 0;
													$valrecactualccf[] = round($rowrecatrans[0]);
												}
												else{
													$valrecactualscf[] = round($rowrecatrans[0]);
													$valrecactualccf[] = 0;
												}

												$valrecanteriorscf[] = 0;
												$valrecanteriorccf[] = 0;
												$valtotalcuenta[] = round($rowrecatrans[0]);
												$tablasinfo[] = "Recaudo Transferencia No: $rowrecatrans[3]";
												$porcentaje2 = $cx * 100 / $totalcx;
												echo"
												<script>
													progres2 = '".round($porcentaje2)."';
													callprogress2(progres2);
													document.getElementById('titulosubproceso').innerHTML = 'Recaudo Transferencia No: $rowrecatrans[3] ($rowcta[0])';
												</script>";
												flush();
												ob_flush();
												usleep(1);
											}
											echo "
											<script>
												document.getElementById('progreso2').style.display='none';
												document.getElementById('titulosubproceso').innerHTML='';
											</script>";
										}
                                        //RETENCIONES DE OTROS EGRESOS
										{
											$cx = 0;
											$sqlretencionE="
											SELECT prc.valor, prc.fuente, prc.productoservicio, prc.idrecibo, prc.seccion_presupuestal
											FROM pptoretencionotrosegresos AS prc
											INNER JOIN tesopagotercerosvigant AS trc
											ON trc.id_pago = prc.idrecibo AND prc.cuenta = '$rowcta[0]'
											WHERE prc.vigencia = '$vigusu' AND NOT(trc.estado='N') AND trc.fecha BETWEEN '$fechai' AND '$fechaf' AND prc.tipo = 'egreso' ";//7
											$resretencionE = mysqli_query($linkmulti, $sqlretencionE);
											$totalcx = mysqli_num_rows($resretencionE);
											while ($rowretencionE = mysqli_fetch_row($resretencionE)){
												if($rowretencionE[4] != ''){
													$sec_presupuestal[] = $rowretencionE[4];
												}else{
													$sec_presupuestal[] = "-";
												}
												$detsectorial[] = $nomsector;
												$numerocuenta[] = $rowcta[0];
												$nombrecuenta[] = $rowcta[1];
												$auxcpc = trim($rowretencionE[2]);
												if($auxcpc!=''){$varcpc[] = $auxcpc;}
												else {$varcpc[]='NO APLICA';}
												$varnorma1[] = $rownormas[0];
												$varnorma2[] = $rownormas[1];
												$varnorma3[] = $rownormas[2];
												$varnorma4[] = $rownormas[3];
												if($rownormas[0] == 'SI'){$valdestiesp[] = round($rowretencionE[0]);}
												else {$valdestiesp[] = 0;}
												if($rowretencionE[1] != ''){
													$varfuentes2[] = $rowretencionE[1];
													$auxfuente = $rowretencionE[1];
													if($auxfuente!=''){$varfuentes[] = $auxfuente;}
													else {$varfuentes[] = 'NO APLICA';}
												}
												else{
													$varfuentes[] = 'NO APLICA';
													$varfuentes2[] = 'NO APLICA';
												}
												$vartercerochip[] = 1;
												$varpolpublicas[] = '';
												$valrecactualscf[] = 0;
												$valrecactualccf[] = round($rowretencionE[0]);
												$valrecanteriorscf[] = 0;
												$valrecanteriorccf[]= 0;
												$valtotalcuenta[] = round($rowretencionE[0]);
												$tablasinfo[] = "Retencion de Otro Egreso No: $rowretencionE[3]";
												$porcentaje2 = $cx * 100 / $totalcx;
												echo"
												<script>
													progres2 = '".round($porcentaje2)."';
													callprogress2(progres2);
													document.getElementById('titulosubproceso').innerHTML = 'Retencion de Pago Egreso No: $rowretencionE[3] ($rowcta[0])';
												</script>";
												flush();
												ob_flush();
												usleep(1);
											}
											echo "
											<script>
												document.getElementById('progreso2').style.display='none';
												document.getElementById('titulosubproceso').innerHTML='';
											</script>";
										}
										//RETENCIONES DE PAGO EGRESOS
										{
											$cx = 0;
											$sqlretencionE="
											SELECT prc.valor, prc.fuente, prc.productoservicio, prc.idrecibo, prc.seccion_presupuestal
											FROM pptoretencionpago AS prc
											INNER JOIN tesoegresos AS trc
											ON trc.id_egreso = prc.idrecibo AND prc.cuenta = '$rowcta[0]'
											WHERE prc.vigencia = '$vigusu' AND NOT(trc.estado='N') AND trc.fecha BETWEEN '$fechai' AND '$fechaf' AND trc.tipo_mov = '201' AND prc.tipo = 'egreso' AND NOT EXISTS (SELECT 1 FROM tesoegresos tra WHERE tra.id_egreso = trc.id_egreso AND tra.tipo_mov = '401') ";//7
											$resretencionE = mysqli_query($linkmulti, $sqlretencionE);
											$totalcx = mysqli_num_rows($resretencionE);
											while ($rowretencionE = mysqli_fetch_row($resretencionE)){
												if($rowretencionE[4] != ''){
													$sec_presupuestal[] = $rowretencionE[4];
												}else{
													$sec_presupuestal[] = "-";
												}
												$detsectorial[] = $nomsector;
												$numerocuenta[] = $rowcta[0];
												$nombrecuenta[] = $rowcta[1];
												$auxcpc = trim($rowretencionE[2]);
												if($auxcpc!=''){$varcpc[] = $auxcpc;}
												else {$varcpc[]='NO APLICA';}
												$varnorma1[] = $rownormas[0];
												$varnorma2[] = $rownormas[1];
												$varnorma3[] = $rownormas[2];
												$varnorma4[] = $rownormas[3];
												if($rownormas[0] == 'SI'){$valdestiesp[] = round($rowretencionE[0]);}
												else {$valdestiesp[] = 0;}
												if($rowretencionE[1] != ''){
													$varfuentes2[] = $rowretencionE[1];
													$auxfuente = $rowretencionE[1];
													if($auxfuente!=''){$varfuentes[] = $auxfuente;}
													else {$varfuentes[] = 'NO APLICA';}
												}
												else{
													$varfuentes[] = 'NO APLICA';
													$varfuentes2[] = 'NO APLICA';
												}
												$vartercerochip[] = 1;
												$varpolpublicas[] = '';
												$valrecactualscf[] = 0;
												$valrecactualccf[] = round($rowretencionE[0]);
												$valrecanteriorscf[] = 0;
												$valrecanteriorccf[]= 0;
												$valtotalcuenta[] = round($rowretencionE[0]);
												$tablasinfo[] = "Retencion de Pago Egreso No: $rowretencionE[3]";
												$porcentaje2 = $cx * 100 / $totalcx;
												echo"
												<script>
													progres2 = '".round($porcentaje2)."';
													callprogress2(progres2);
													document.getElementById('titulosubproceso').innerHTML = 'Retencion de Pago Egreso No: $rowretencionE[3] ($rowcta[0])';
												</script>";
												flush();
												ob_flush();
												usleep(1);
											}
											echo "
											<script>
												document.getElementById('progreso2').style.display='none';
												document.getElementById('titulosubproceso').innerHTML='';
											</script>";
										}
										//RETENCIONES DE PAGO ORDEN DE PAGO
										{
											$cx = 0;
											$sqlretencionO="
											SELECT prc.valor, prc.fuente, prc.productoservicio, prc.idrecibo, trc.medio_pago, prc.seccion_presupuestal
											FROM pptoretencionpago AS prc
											INNER JOIN tesoordenpago AS trc
											ON trc.id_orden = prc.idrecibo AND prc.cuenta = '$rowcta[0]'
											WHERE prc.vigencia = '$vigusu' AND NOT(trc.estado='N') AND trc.fecha BETWEEN '$fechai' AND '$fechaf' AND trc.tipo_mov = '201' AND prc.tipo = 'orden' AND NOT EXISTS (SELECT 1 FROM tesoordenpago tca WHERE tca.id_orden = trc.id_orden  AND tca.tipo_mov = '401') ";//8
											$resretencionO = mysqli_query($linkmulti, $sqlretencionO);
											$totalcx = mysqli_num_rows($resretencionO);
											while ($rowretencionO = mysqli_fetch_row($resretencionO)){
												if($rowretencionO[5] != ''){
													$sec_presupuestal[] = $rowretencionO[5];
												}else{
													$sec_presupuestal[] = "-";
												}
												$detsectorial[] = $nomsector;
												$numerocuenta[] = $rowcta[0];
												$nombrecuenta[] = $rowcta[1];
												$auxcpc = trim($rowretencionO[2]);
												if($auxcpc!=''){$varcpc[] = $auxcpc;}
												else {$varcpc[]='NO APLICA';}
												$varnorma1[] = $rownormas[0];
												$varnorma2[] = $rownormas[1];
												$varnorma3[] = $rownormas[2];
												$varnorma4[] = $rownormas[3];
												if($rownormas[0] == 'SI'){$valdestiesp[] = round($rowretencionO[0]);}
												else {$valdestiesp[] = 0;}
												if($rowretencionO[1] != ''){
													$varfuentes2[] = $rowretencionO[1];
													$auxfuente = $rowretencionO[1];
													if($auxfuente != ''){$varfuentes[] = $auxfuente;}
													else {$varfuentes[] = 'NO APLICA';}
												}
												else{
													$varfuentes[] = 'NO APLICA';
													$varfuentes2[] = 'NO APLICA';
												}
												$vartercerochip[] = 1;
												$varpolpublicas[] = '';
												if($rowretencionO[4] == '2'){
													$valrecactualscf[] = round($rowretencionO[0]);
													$valrecactualccf[] = 0;
												}
												else{
													$valrecactualscf[] = 0;
													$valrecactualccf[] = round($rowretencionO[0]);
												}
												$valrecanteriorscf[] = 0;
												$valrecanteriorccf[] = 0;
												$valtotalcuenta[] = round($rowretencionO[0]);
												$tablasinfo[] = "Retencion de Orden de Pago  No: $rowretencionO[3]";
												$porcentaje2 = $cx * 100 / $totalcx;
												echo"
												<script>
													progres2 = '".round($porcentaje2)."';
													callprogress2(progres2);
													document.getElementById('titulosubproceso').innerHTML = 'Retencion de Orden de Pago  No: $rowretencionO[3] ($rowcta[0])';
												</script>";
												flush();
												ob_flush();
												usleep(1);
											}
											echo "
											<script>
												document.getElementById('progreso2').style.display='none';
												document.getElementById('titulosubproceso').innerHTML='';
											</script>";
										}
										//SUPERAVIT PPTO
										{
											$cx = 0;
											$sqlsuperavit="
											SELECT psd.valor, psd.consvigencia
											FROM pptosuperavit AS ps
											INNER JOIN pptosuperavit_detalle AS psd
											ON ps.consvigencia = psd.consvigencia AND psd.cuenta = '$rowcta[0]';
											WHERE psd.vigencia = '$vigusu' AND NOT(ps.estado = 'N' OR psd.estado = 'R') AND ps.fecha BETWEEN '$fechai' AND '$fechaf' ";//9
											$ressuperavit = mysqli_query($linkmulti, $sqlsuperavit);
											$totalcx = mysqli_num_rows($ressuperavit);
											while ($rowsuperavit = mysqli_fetch_row($ressuperavit)){
												$sec_presupuestal[] = "-";
												$detsectorial[] = $nomsector;
												$numerocuenta[] = $rowcta[0];
												$nombrecuenta[] = $rowcta[1];
												$varcpc[] = 'NO APLICA';
												$varnorma1[] = $rownormas[0];
												$varnorma2[] = $rownormas[1];
												$varnorma3[] = $rownormas[2];
												$varnorma4[] = $rownormas[3];
												if($rownormas[0] == 'SI'){$valdestiesp[] = round($rowsuperavit[0]);}
												else {$valdestiesp[] = 0;}
												$varfuentes[] = 'NO APLICA';
												$varfuentes2[] = 'NO APLICA';
												$vartercerochip[] = 1;
												$varpolpublicas[] = '';
												$valrecactualscf[] = 0;
												$valrecactualccf[] = round($rowsuperavit[0]);
												$valrecanteriorscf[] = 0;
												$valrecanteriorccf[] = 0;
												$valtotalcuenta[] = round($rowsuperavit[0]);
												$tablasinfo[] = "Superavit PPTO No: $rowsuperavit[1]";
												$porcentaje2 = $cx * 100 / $totalcx;
												echo"
												<script>
													progres2 = '".round($porcentaje2)."';
													callprogress2(progres2);
													document.getElementById('titulosubproceso').innerHTML = 'Superavit PPTO No: $rowsuperavit[1] ($rowcta[0])';
												</script>";
												flush();
												ob_flush();
												usleep(1);
											}
											echo "
											<script>
												document.getElementById('progreso2').style.display='none';
												document.getElementById('titulosubproceso').innerHTML='';
											</script>";
										}
										//RESERVAS PPTO
										{
											/* $cx = 0;
											$sqlreservas="
											SELECT psd.valor, psd.consvigencia
											FROM pptoreservas ps
											INNER JOIN pptoreservas_det psd
											ON ps.consvigencia = psd.consvigencia AND psd.cuenta = '$rowcta[0]'
											WHERE psd.vigencia = '$vigusu' AND NOT(ps.estado = 'N' OR psd.estado = 'R') AND ps.fecha BETWEEN '$fechai' AND '$fechaf'";//10
											$resreservas = mysqli_query($linkmulti, $sqlreservas);
											$totalcx = mysqli_num_rows($resreservas);
											while ($rowreservas = mysqli_fetch_row($resreservas)){
												$sec_presupuestal[] = "-";
												$detsectorial[] = $nomsector;
												$numerocuenta[] = $rowcta[0];
												$nombrecuenta[] = $rowcta[1];
												$varcpc[] = 'NO APLICA';
												$varnorma1[] = $rownormas[0];
												$varnorma2[] = $rownormas[1];
												$varnorma3[] = $rownormas[2];
												$varnorma4[] = $rownormas[3];
												if($rownormas[0] == 'SI'){$valdestiesp[] = round($rowreservas[0]);}
												else {$valdestiesp[] = 0;}
												$varfuentes[] = 'NO APLICA';
												$varfuentes2[] = 'NO APLICA';
												$vartercerochip[] = 1;
												$varpolpublicas[] = '';
												$valrecactualscf[] = 0;
												$valrecactualccf[] = round($rowreservas[0]);
												$valrecanteriorscf[] = 0;
												$valrecanteriorccf[] = 0;
												$valtotalcuenta[] = round($rowreservas[0]);
												$tablasinfo[] = "Reservas PPTO No: $rowreservas[1]";
												$porcentaje2 = $cx * 100 / $totalcx;
												echo"
												<script>
													progres2 = '".round($porcentaje2)."';
													callprogress2(progres2);
													document.getElementById('titulosubproceso').innerHTML = 'Reservas PPTO No: $rowreservas[1] ($rowcta[0])';
												</script>";
												flush();
												ob_flush();
												usleep(1);
											}
											echo "
											<script>
												document.getElementById('progreso2').style.display='none';
												document.getElementById('titulosubproceso').innerHTML='';
											</script>"; */
										}
										//SIN RECIBO DE CAJA SP
										{
											$cx = 0;
											$sqlsp="
											SELECT TB2.valor, TB2.idrecibo, TB2.fuente, TB2.productoservicio, TB2.medio_pago
											FROM tesosinreciboscajasp AS TB1
											INNER JOIN pptosinrecibocajaspppto AS TB2
											ON TB1.id_recibos = TB2.idrecibo AND TB1.vigencia = TB2.vigencia AND TB2.cuenta = '$rowcta[0]'
											WHERE TB1.estado = 'S' AND TB2.vigencia = '$vigusu' AND TB1.fecha BETWEEN '$fechai' AND '$fechaf' ";//12
											$ressp = mysqli_query($linkmulti, $sqlsp);
											$totalcx = mysqli_num_rows($ressp);
											while ($rowsp = mysqli_fetch_row($ressp)){
												$sec_presupuestal[] = "-";
												$detsectorial[] = $nomsector;
												$numerocuenta[] = $rowcta[0];
												$nombrecuenta[] = $rowcta[1];
												$auxcpc = trim($rowsp[3]);
												if($auxcpc !=''){$varcpc[] = $auxcpc;}
												else {$varcpc[] = 'NO APLICA';}
												$varnorma1[] = $rownormas[0];
												$varnorma2[] = $rownormas[1];
												$varnorma3[] = $rownormas[2];
												$varnorma4[] = $rownormas[3];
												if($rownormas[0] == 'SI'){$valdestiesp[] = round($rowsp[0]);}
												else {$valdestiesp[] = 0;}
												if($rowsp[2] != ''){
													$varfuentes2[] = $rowsp[2];
													$auxfuente = $rowsp[2];
													if($auxfuente != ''){$varfuentes[] = $auxfuente;}
													else {$varfuentes[] = 'NO APLICA';}
													$varfuentes[] = $auxfuente;
												}
												else{
													$varfuentes[] = 'NO APLICA';
													$varfuentes2[] = 'NO APLICA';
												}
												$vartercerochip[] = 1;
												$varpolpublicas[] = '';
												if($rowsp[4] == "SSF"){
													$valrecactualscf[] = round($rowsp[0]);
													$valrecactualccf[] = 0;
												}
												else{
													$valrecactualscf[] = 0;
													$valrecactualccf[] = round($rowsp[0]);
												}
												$valrecanteriorscf[] = 0;
												$valrecanteriorccf[] = 0;
												$valtotalcuenta[] = round($rowsp[0]);
												$tablasinfo[] = "Sin Recibo de Caja SP No: $rowsp[1]";
												$porcentaje2 = $cx * 100 / $totalcx;
												echo"
												<script>
													progres2 = '".round($porcentaje2)."';
													callprogress2(progres2);
													document.getElementById('titulosubproceso').innerHTML = 'Sin Recibo de Caja SP No: $rowsp[1] ($rowcta[0])';
												</script>";
												flush();
												ob_flush();
												usleep(1);
											}
											echo "
											<script>
												document.getElementById('progreso2').style.display='none';
												document.getElementById('titulosubproceso').innerHTML='';
											</script>";
										}
										//RECIBOS DE CAJA SP
										{

											$cx = 0;
											$sqlrsp="
											SELECT T1.valor, T1.id_recibos, T1.fuente
											FROM servreciboscaja_det AS T1
											INNER JOIN servreciboscaja AS T2
											ON T2.id_recibos = T1.id_recibos AND T1.cuentapres = '$rowcta[0]'
											WHERE T2.estado = 'S' AND T2.fecha BETWEEN '$fechai' AND '$fechaf'";//13
											$resrsp = mysqli_query($linkmulti, $sqlrsp);
											$totalcx = mysqli_num_rows($resrsp);
											while ($rowrsp = mysqli_fetch_row($resrsp)){
												$sec_presupuestal[] = "-";
												$detsectorial[] = $nomsector;
												$numerocuenta[] = $rowcta[0];
												$nombrecuenta[] = $rowcta[1];
												$varcpc[] = 'NO APLICA';
												$varnorma1[] = $rownormas[0];
												$varnorma2[] = $rownormas[1];
												$varnorma3[] = $rownormas[2];
												$varnorma4[] = $rownormas[3];
												if($rownormas[0] == 'SI'){$valdestiesp[] = round($rowrsp[0]);}
												else {$valdestiesp[] = 0;}
												if($rowrsp[2] != ''){
													$varfuentes[] = $rowrsp[2];
													$varfuentes2[] = $rowrsp[2];
												}
												else{
													$varfuentes[] = 'NO APLICA';
													$varfuentes2[] = 'NO APLICA';
												}
												/* $varfuentes[] = 'NO APLICA';
												$varfuentes2[] = 'NO APLICA'; */
												$vartercerochip[] = 1;
												$varpolpublicas[] = '';
												$valrecactualscf[] = 0;
												$valrecactualccf[] = round($rowrsp[0]);
												$valrecanteriorscf[] = 0;
												$valrecanteriorccf[] = 0;
												$valtotalcuenta[] = round($rowrsp[0]);
												$tablasinfo[] = "Recibo de Caja SP No: $rowrsp[1]";
												$porcentaje2 = $cx * 100 / $totalcx;
												echo"
												<script>
													progres2 = '".round($porcentaje2)."';
													callprogress2(progres2);
													document.getElementById('titulosubproceso').innerHTML = 'Recibo de Caja SP No: $rowrsp[1] ($rowcta[0])';
												</script>";
												flush();
												ob_flush();
												usleep(1);
											}
											echo "
											<script>
												document.getElementById('progreso2').style.display='none';
												document.getElementById('titulosubproceso').innerHTML='';
											</script>";
										}
										//SUPERAVIT TESO
										{
											$cx = 0;
											$sqlavit = "
											SELECT tsad.valor, tsad.fuente, tsad.id_tesosuperavit, tsad.seccion_presupuestal
											FROM tesosuperavit AS tsa
											INNER JOIN tesosuperavit_det AS tsad
											ON tsa.id = tsad.id_tesosuperavit AND tsad.rubro = '$rowcta[0]'
											WHERE tsa.estado = 'S' AND tsad.vigencia_gasto = '1' AND tsa.vigencia = '$vigusu' AND tsa.fecha BETWEEN '$fechai' AND '$fechaf'";//14
											$resavit = mysqli_query($linkmulti, $sqlavit);
											$totalcx = mysqli_num_rows($resavit);
											while ($rowavit = mysqli_fetch_row($resavit)){
												if($rowavit[3] != ''){
													$sec_presupuestal[] = $rowavit[3];
												}else{
													$sec_presupuestal[] = "-";
												}
												$detsectorial[] = $nomsector;
												$numerocuenta[] = $rowcta[0];
												$nombrecuenta[] = $rowcta[1];
												$varcpc[] = 'NO APLICA';
												$varnorma1[] = $rownormas[0];
												$varnorma2[] = $rownormas[1];
												$varnorma3[] = $rownormas[2];
												$varnorma4[] = $rownormas[3];
												if($rownormas[0] == 'SI'){$valdestiesp[] = round($rowavit[0]);}
												else {$valdestiesp[] = 0;}
												if($rowavit[1] != ''){
													$varfuentes2[] = $rowavit[1];
													$auxfuente = $rowavit[1];
													if($auxfuente != ''){$varfuentes[] = $auxfuente;}
													else {$varfuentes[] = 'NO APLICA';}
												}
												else{
													$varfuentes[] = 'NO APLICA';
													$varfuentes2[] = 'NO APLICA';
												}
												$vartercerochip[] = 1;
												$varpolpublicas[] = '';
												$valrecactualscf[] = 0;
												$valrecactualccf[] = round($rowavit[0]);
												$valrecanteriorscf[] = 0;
												$valrecanteriorccf[] = 0;
												$valtotalcuenta[] = round($rowavit[0]);
												$tablasinfo[] = "Superavit TESO No: $rowavit[2]";
												$porcentaje2 = $cx * 100 / $totalcx;
												echo"
												<script>
													progres2 = '".round($porcentaje2)."';
													callprogress2(progres2);
													document.getElementById('titulosubproceso').innerHTML = 'Superavit TESO No: $rowavit[2] ($rowcta[0])';
												</script>";
												flush();
												ob_flush();
												usleep(1);
											}
											echo "
											<script>
												document.getElementById('progreso2').style.display='none';
												document.getElementById('titulosubproceso').innerHTML='';
											</script>";
										}
										//RESERVAS TESO
										{
											$cx = 0;
											$sqlReservas = "
											SELECT tsad.valor,tsad.fuente, tsa.id
											FROM tesoreservas AS tsa
											INNER JOIN tesoreservas_det AS tsad
											ON tsa.id = tsad.id_reserva AND tsad.rubro = '$rowcta[0]'
											WHERE tsa.estado = 'S' AND tsad.vigencia_gasto = '1' AND tsa.vigencia = '$vigusu' AND tsa.fecha BETWEEN '$fechai' AND '$fechaf'";//15
											$resReservas = mysqli_query($linkmulti, $sqlReservas);
											$totalcx = mysqli_num_rows($resReservas);
											while ($rowReservas = mysqli_fetch_row($resReservas)){
												$sec_presupuestal[] = "-";
												$detsectorial[] = $nomsector;
												$numerocuenta[] = $rowcta[0];
												$nombrecuenta[] = $rowcta[1];
												$varcpc[] = 'NO APLICA';
												$varnorma1[] = $rownormas[0];
												$varnorma2[] = $rownormas[1];
												$varnorma3[] = $rownormas[2];
												$varnorma4[] = $rownormas[3];
												if($rownormas[0] == 'SI'){$valdestiesp[] = round($rowReservas[0]);}
												else {$valdestiesp[] = 0;}
												if($rowReservas[1] != ''){
													$varfuentes2[] = $rowReservas[1];
													$auxfuente = $rowReservas[1];
													if($auxfuente != ''){$varfuentes[] = $auxfuente;}
													else {$varfuentes[] = 'NO APLICA';}
												}
												else{
													$varfuentes[] = 'NO APLICA';
													$varfuentes2[] = 'NO APLICA';
												}
												$vartercerochip[] = 1;
												$varpolpublicas[] = '';
												$valrecactualscf[] = 0;
												$valrecactualccf[] = round($rowReservas[0]);
												$valrecanteriorscf[] = 0;
												$valrecanteriorccf[] = 0;
												$valtotalcuenta[] = round($rowReservas[0]);
												$tablasinfo[] = 'Reservas Teso. N: '.$rowReservas[2];
												$porcentaje2 = $cx * 100 / $totalcx;
												echo"
												<script>
													progres2 = '".round($porcentaje2)."';
													callprogress2(progres2);
													document.getElementById('titulosubproceso').innerHTML = 'Reservas Teso. N: $rowReservas[2] ($rowcta[0])';
												</script>";
												flush();
												ob_flush();
												usleep(1);
											}
											/* echo "
											<script>
												document.getElementById('progreso2').style.display='none';
												document.getElementById('titulosubproceso').innerHTML='';
											</script>"; */
										}
										//RECAUDOSP
										{
											$cx = 0;
											$sqlrecaudosp = "
											SELECT T2.valor, T2.fuente, T1.codigo_recaudo, T2.producto_servicio
											FROM srv_recaudo_factura AS T1
											INNER JOIN srv_recaudo_factura_ppto	AS T2
											ON T1.codigo_recaudo = T2.codigo_recaudo AND T2.cuenta = '$rowcta[0]'
											WHERE T1.estado = 'ACTIVO' AND T2.vigencia = '$vigusu' AND T1.fecha_recaudo BETWEEN '$fechai' AND '$fechaf'";//15
											$resrecaudosp = mysqli_query($linkmulti, $sqlrecaudosp);
											$totalcx = mysqli_num_rows($resrecaudosp);
											while ($rowrecaudosp = mysqli_fetch_row($resrecaudosp)){
												$sec_presupuestal[] = "-";
												$detsectorial[] = $nomsector;
												$numerocuenta[] = $rowcta[0];
												$nombrecuenta[] = $rowcta[1];
												if($rowrecaudosp[3] != ''){
													$varcpc[] = $rowrecaudosp[3];
												}else{
													$varcpc[] = 'NO APLICA';
												}
												$varnorma1[] = $rownormas[0];
												$varnorma2[] = $rownormas[1];
												$varnorma3[] = $rownormas[2];
												$varnorma4[] = $rownormas[3];
												if($rownormas[0] == 'SI'){
													$valdestiesp[] = round($rowrecaudosp[0]);
												}else {
													$valdestiesp[] = 0;
												}
												if($rowrecaudosp[1] != ''){
													$varfuentes2[] = $rowrecaudosp[1];
													$auxfuente = $rowrecaudosp[1];
													if($auxfuente != ''){$varfuentes[] = $auxfuente;}
													else {$varfuentes[] = 'NO APLICA';}
												}
												else{
													$varfuentes[] = 'NO APLICA';
													$varfuentes2[] = 'NO APLICA';
												}
												$vartercerochip[] = 1;
												$varpolpublicas[] = '';
												$valrecactualscf[] = 0;
												$valrecactualccf[] = round($rowrecaudosp[0]);
												$valrecanteriorscf[] = 0;
												$valrecanteriorccf[] = 0;
												$valtotalcuenta[] = round($rowrecaudosp[0]);
												$tablasinfo[] = 'Recaudos SP. N: '.$rowrecaudosp[2];
												$porcentaje2 = $cx * 100 / $totalcx;
												echo"
												<script>
													progres2 = '".round($porcentaje2)."';
													callprogress2(progres2);
													document.getElementById('titulosubproceso').innerHTML = 'Recaudos SP. N: $rowrecaudosp[2] ($rowcta[0])';
												</script>";
												flush();
												ob_flush();
												usleep(1);
											}
											echo "
											<script>
												document.getElementById('progreso2').style.display='none';
												document.getElementById('titulosubproceso').innerHTML='';
											</script>";
										}
                                        //Acuerdos sp
                                        {
											$cx = 0;
											$sqlacuerdosp = "
											SELECT T2.valor, T2.fuente, T1.codigo_acuerdo, T2.producto_servicio
											FROM srv_acuerdo_cab AS T1
											INNER JOIN srv_acuerdo_ppto	AS T2
											ON T1.codigo_acuerdo = T2.codigo_acuerdo AND T2.cuenta = '$rowcta[0]'
											WHERE T1.estado_acuerdo != 'Reversado' AND T2.vigencia = '$vigusu' AND T1.fecha_acuerdo BETWEEN '$fechai' AND '$fechaf'";//16
											$resacuerdosp = mysqli_query($linkmulti, $sqlacuerdosp);
											$totalcx = mysqli_num_rows($resacuerdosp);
											while ($rowacuerdosp = mysqli_fetch_row($resacuerdosp)){
												$sec_presupuestal[] = "-";
												$detsectorial[] = $nomsector;
												$numerocuenta[] = $rowcta[0];
												$nombrecuenta[] = $rowcta[1];
												if($rowacuerdosp[3] != ''){
													$varcpc[] = $rowacuerdosp[3];
												}else{
													$varcpc[] = 'NO APLICA';
												}
												$varnorma1[] = $rownormas[0];
												$varnorma2[] = $rownormas[1];
												$varnorma3[] = $rownormas[2];
												$varnorma4[] = $rownormas[3];
												if($rownormas[0] == 'SI'){
													$valdestiesp[] = round($rowacuerdosp[0]);
												}else {
													$valdestiesp[] = 0;
												}
												if($rowacuerdosp[1] != ''){
													$varfuentes2[] = $rowacuerdosp[1];
													$auxfuente = $rowacuerdosp[1];
													if($auxfuente != ''){$varfuentes[] = $auxfuente;}
													else {$varfuentes[] = 'NO APLICA';}
												}
												else{
													$varfuentes[] = 'NO APLICA';
													$varfuentes2[] = 'NO APLICA';
												}
												$vartercerochip[] = 1;
												$varpolpublicas[] = '';
												$valrecactualscf[] = 0;
												$valrecactualccf[] = round($rowacuerdosp[0]);
												$valrecanteriorscf[] = 0;
												$valrecanteriorccf[] = 0;
												$valtotalcuenta[] = round($rowacuerdosp[0]);
												$tablasinfo[] = 'Acuerdos SP. N: '.$rowacuerdosp[2];
												$porcentaje2 = $cx * 100 / $totalcx;
												echo"
												<script>
													progres2 = '".round($porcentaje2)."';
													callprogress2(progres2);
													document.getElementById('titulosubproceso').innerHTML = 'Acuerdos SP. N: $rowacuerdosp[2] ($rowcta[0])';
												</script>";
												flush();
												ob_flush();
												usleep(1);
											}
											echo "
											<script>
												document.getElementById('progreso2').style.display='none';
												document.getElementById('titulosubproceso').innerHTML='';
											</script>";
										}
									}
									$porcentaje = $c * 100 / $totalcli;
									echo"
									<script>
										progres='".round($porcentaje)."';callprogress(progres);
										document.getElementById('totalcuentas').innerHTML='Recolectando Información: $numid / $totalcli';
									</script>";
									flush();
									ob_flush();
									usleep(5);
								}
							}
							$iter = "zebra1";
							$iter2 = "zebra2";
							$c=0;
							$totalcli = count($numerocuenta);
							for($xx=0;$xx<$totalcli;$xx++){
								$c++;
								$idtemporal++;
								if($varnorma4[$xx]==0 || $varnorma4[$xx]=="0000-00-00"){$fechacott="1900-01-01";}
								else {$fechacott = $varnorma4[$xx];}

								$sqlaesgpriet = "SELECT aesgpriet FROM pptoseccion_presupuestal WHERE estado = 'S' AND id_seccion_presupuestal = '$sec_presupuestal[$xx]'";
								$resaesgpriet = mysqli_query($linkmulti,$sqlaesgpriet);
								$rowaesgpriet = mysqli_fetch_row($resaesgpriet);
								if($_POST['tipoentidad'] != 'RG' ){
									$sqltabla2 = "INSERT INTO ".$_SESSION['tablatemporal']." (id, codigocuenta, nombrecuenta, cpc, aplicadestinacion, tiponorma, numeronorma, fechanorma, valordestinacionesp, fuente, tercerochip, politicapublica, recaudoactualscf, recaudoactualccf, recaudoanteriorscf, recaudoanteriorccf, totalrecaudo, tipotabla, fuente2, codsectorial, aesgpriet) VALUES ('$idtemporal', '$numerocuenta[$xx]', '$nombrecuenta[$xx]', '$varcpc[$xx]', '$varnorma1[$xx]', '$varnorma2[$xx]', '$varnorma3[$xx]', '$fechacott', '$valdestiesp[$xx]', '$varfuentes[$xx]', '$vartercerochip[$xx]', '$varpolpublicas[$xx]', '$valrecactualscf[$xx]', '$valrecactualccf[$xx]', '$valrecanteriorscf[$xx]', '$valrecanteriorccf[$xx]', '$valtotalcuenta[$xx]', '$tablasinfo[$xx]','$varfuentes2[$xx]', '$detsectorial[$xx]', '$rowaesgpriet[0]')";
									mysqli_query($linkbd,$sqltabla2);
								}

								$porcentaje = $c * 100 / $totalcli;
								echo"
								<script>
									progres='".round($porcentaje)."';callprogress(progres);
									document.getElementById('totalcuentas').innerHTML='Procesando Información $c / $totalcli';
								</script>";
								flush();
								ob_flush();
								usleep(1);
							}
							$c=0;
							if($_POST['tipoentidad'] == 'RS' ){
								$sqlver = "SELECT id, codigocuenta, nombrecuenta, cpc, aplicadestinacion, tiponorma, numeronorma, fechanorma, SUM(valordestinacionesp), fuente, tercerochip, politicapublica, SUM(recaudoactualscf), SUM(recaudoactualccf), SUM(recaudoanteriorscf), SUM(recaudoanteriorccf), SUM(totalrecaudo),fuente2, codsectorial, aesgpriet FROM ".$_SESSION['tablatemporal']." GROUP BY codigocuenta, aesgpriet ORDER BY id ASC";
							}elseif($_POST['tipoentidad'] == 'RG' ){

								$sqlver = "SELECT id, codigocuenta, nombrecuenta, tipoRecurso, bpim, terceros, politicaPublica, situacionFondos, SUM(recaudo) FROM ".$_SESSION['tablatemporal']." GROUP BY codigocuenta, tipoRecurso, bpim, situacionFondos ORDER BY id ASC";
							}else{
								$sqlver = "SELECT id, codigocuenta, nombrecuenta, cpc, aplicadestinacion, tiponorma, numeronorma, fechanorma, SUM(valordestinacionesp), fuente, tercerochip, politicapublica, SUM(recaudoactualscf), SUM(recaudoactualccf), SUM(recaudoanteriorscf), SUM(recaudoanteriorccf), SUM(totalrecaudo),fuente2, codsectorial FROM ".$_SESSION['tablatemporal']." GROUP BY codigocuenta, cpc, fuente2 ORDER BY id ASC";
							}
							$resver = mysqli_query($linkbd,$sqlver);
							$totalcli = mysqli_num_rows($resver);
							while ($rowver = mysqli_fetch_row($resver)){
								$c++;
								$porcentaje = $c * 100 / $totalcli;
								switch ($rowver[5]){
									case "0":	$tiponorma = "No Aplica"; break;
									case "1":	$tiponorma = "Decreto"; break;
									case "2":	$tiponorma = "Acuerdo"; break;
									case "3":	$tiponorma = "Ordenanza"; break;
									case "4":	$tiponorma = "Resolución";
								}
								$fechapartes = explode('-', $rowver[7]);
								$fechainforme = "$fechapartes[2]-$fechapartes[1]-$fechapartes[0]";
								$caltotal1 = $rowver[12];
								$caltotal2 = $rowver[13];
								$caltotal3 = $rowver[14];
								$caltotal4 = $rowver[15];
								$caltotal5 = $rowver[16];
								unset ($aplicadexyz, $tiponormaxyz, $numeronormaxyz, $fechanormaxyz, $detsectorialxyz, $porcentajexyz1,$porcentajexyz2, $porcentajexyz3, $porcentajexyz4, $porcentajexyz5, $fuentesxyz);
								$sqlxy = "SELECT aplicade, tiponorma, numeronorma, fechanorma, detalle_sectorial, porcentaje, fuentes FROM ccpecuentastiponormas WHERE cuenta = '$rowver[1]' AND fuenteid = '$rowver[9]'";
								$resxy = mysqli_query($linkbd,$sqlxy);
								$contnormas = mysqli_num_rows($resxy);
								while ($rowxt = mysqli_fetch_row($resxy)){
									$aplicadexyz[] =  $rowxt[0];
									$tiponormaxyz[] = $rowxt[1];
									$numeronormaxyz[] = $rowxt[2];
									$fechanormaxyz[] = date('d-m-Y',strtotime($rowxt[3]));
									$detsectorialxyz[] = $rowxt[4];
									$fuentesxyz[] = $rowxt[6];
									$porcentajexyz1[] = $rowver[12] * ($rowxt[5]/100);
									$porcentajexyz2[] = $rowver[13] * ($rowxt[5]/100);
									$porcentajexyz3[] = $rowver[14] * ($rowxt[5]/100);
									$porcentajexyz4[] = $rowver[15] * ($rowxt[5]/100);
									$porcentajexyz5[] = $rowver[16] * ($rowxt[5]/100);
									if($rowver[12] > 0){
										$caltotal1 = $caltotal1 - ($rowver[12] * ($rowxt[5]/100));
									}
									if($rowver[13] > 0){
										$caltotal2 = $caltotal2 - ($rowver[13] * ($rowxt[5]/100));
									}
									if($rowver[14] > 0){
										$caltotal3 = $caltotal3 - ($rowver[14] * ($rowxt[5]/100));
									}
									if($rowver[15] > 0){
										$caltotal4 = $caltotal4 - ($rowver[15] * ($rowxt[5]/100));
									}
									if($rowver[16] > 0){
										$caltotal5 = $caltotal5 - ($rowver[16] * ($rowxt[5]/100));
									}
								}
								if($_POST['tipoentidad'] == 'RS' ){
									echo "
									<tr class=$iter >
										<td class='titulos2'>
											<a onClick=\"detalleb('$c','$rowver[1]','$rowver[3]','$rowver[17]')\" style='cursor:pointer;'>
												<img id='img$c' src='imagenes/plus.gif'>
											</a>
										</td>
										<td style='width:10%;'>$rowver[1]</td>
										<td>$rowver[2]</td>
										<td>$rowver[19]</td>
										<td style='text-align:right;' title='$".number_format($rowver[16],0,',','.')."'>$".number_format($caltotal5,0,',','.')."</td>
									</tr>";
								}elseif($_POST['tipoentidad'] == 'RG' ){
									echo "
									<tr class=$iter >
										<td class='titulos2'>
											<a onClick=\"detalleb('$c','$rowver[1]','$rowver[3]','$rowver[17]')\" style='cursor:pointer;'>
												<img id='img$c' src='imagenes/plus.gif'>
											</a>
										</td>
										<td style='width:10%;'>$rowver[1]</td>
										<td>$rowver[2]</td>
										<td>$rowver[3]</td>
										<td>$rowver[4]</td>
										<td>$rowver[5]</td>
										<td>$rowver[6]</td>
										<td>$rowver[7]</td>
										<td style='text-align:right;' title='$".number_format($rowver[8],0,',','.')."'>$".number_format($rowver[8],0,',','.')."</td>
									</tr>";
								}else{
									echo "
									<tr class=$iter >
										<td class='titulos2'>
											<a onClick=\"detalleb('$c','$rowver[1]','$rowver[3]','$rowver[17]')\" style='cursor:pointer;'>
												<img id='img$c' src='imagenes/plus.gif'>
											</a>
										</td>
										<td style='width:10%;'>$rowver[1]</td>
										<td>$rowver[2]</td>
										<td>$rowver[3]</td>
										<td style='text-align:center;' onDblClick=\"auxNormas('$rowver[1]')\">$rowver[18]</td>
										<td onDblClick=\"auxNormas('$rowver[1]')\">$rowver[4]</td>
										<td onDblClick=\"auxNormas('$rowver[1]')\">$tiponorma</td>
										<td onDblClick=\"auxNormas('$rowver[1]')\">$rowver[6]</td>
										<td onDblClick=\"auxNormas('$rowver[1]')\">$fechainforme</td>
										<td style='text-align:right;'>$".number_format($rowver[8],0,',','.')."</td>
										<td>$rowver[17]</td>
										<td title='".nombre_tercero_chip($rowver[10])."'>$rowver[10]</td>
										<td>$rowver[11]</td>
										<td style='text-align:right;' title='$".number_format($rowver[12],0,',','.')."'>$".number_format($caltotal1,0,',','.')."</td>
										<td style='text-align:right;' title='$".number_format($rowver[13],0,',','.')."'>$".number_format($caltotal2,0,',','.')."</td>
										<td style='text-align:right;' title='$".number_format($rowver[14],0,',','.')."'>$".number_format($caltotal3,0,',','.')."</td>
										<td style='text-align:right;' title='$".number_format($rowver[15],0,',','.')."'>$".number_format($caltotal4,0,',','.')."</td>
										<td style='text-align:right;' title='$".number_format($rowver[16],0,',','.')."'>$".number_format($caltotal5,0,',','.')."</td>
									</tr>";
								}
								for ($yz = 0; $yz < $contnormas; $yz++){
									if($_POST['tipoentidad'] == 'RS' ){
										echo "
										<tr class=$iter >
											<td class='titulos2'>
												<a onClick=\"detalleb('$c','$rowver[1]','$rowver[3]','$rowver[17]')\" style='cursor:pointer;'>
													<img id='img$c' src='imagenes/plus.gif'>
												</a>
											</td>
											<td style='width:10%;'>$rowver[1]</td>
											<td>$rowver[2]</td>
											<td>$rowver[19]</td>
											<td style='text-align:right;'>$".number_format($porcentajexyz5[$yz],0,',','.')."</td>
										</tr>";
									}elseif($_POST['tipoentidad'] == 'RG' ){
									}else{
										echo "
										<tr class=$iter >
											<td class='titulos2'>
												<a onClick=\"detalleb('$c','$rowver[1]','$rowver[3]','$rowver[17]')\" style='cursor:pointer;'>
													<img id='img$c' src='imagenes/plus.gif'>
												</a>
											</td>
											<td style='width:10%;'>$rowver[1]</td>
											<td>$rowver[2]</td>
											<td>$rowver[3]</td>
											<td style='text-align:center;'>".$detsectorialxyz[$yz]."</td>
											<td>".$aplicadexyz[$yz]."</td>
											<td>".$tiponormaxyz[$yz]."</td>
											<td>".$numeronormaxyz[$yz]."</td>
											<td>".$fechanormaxyz[$yz]."</td>
											<td style='text-align:right;'>$".number_format($rowver[8],0,',','.')."</td>
											<td>$fuentesxyz[$yz]</td>
											<td title='".nombre_tercero_chip($rowver[10])."'>$rowver[10]</td>
											<td>$rowver[11]</td>
											<td style='text-align:right;'>$".number_format($porcentajexyz1[$yz],0,',','.')."</td>
											<td style='text-align:right;'>$".number_format($porcentajexyz2[$yz],0,',','.')."</td>
											<td style='text-align:right;'>$".number_format($porcentajexyz3[$yz],0,',','.')."</td>
											<td style='text-align:right;'>$".number_format($porcentajexyz4[$yz],0,',','.')."</td>
											<td style='text-align:right;'>$".number_format($porcentajexyz5[$yz],0,',','.')."</td>
										</tr>";
									}
								}
								echo"
								<tr>
									<td align='center'></td>
									<td colspan='16'>
										<div id='detalle$c' style='display:none'></div>
									</td>
								</tr>
								<script>
									progres='".round($porcentaje)."';callprogress(progres);
									document.getElementById('totalcuentas').innerHTML='Visualizar Información $c / $totalcli';
								</script>";
								flush();
								ob_flush();
								usleep(1);
								$aux=$iter;
								$iter=$iter2;
								$iter2=$aux;
								if($rowver[3]=='NO APLICA'){$valorcpc=0;}
								else {$valorcpc=$rowver[3];}
								if($rowver[9]=='NO APLICA'){$valorfuente=0;}
								else{$valorfuente=$rowver[9];}
								if($rowver[4]=='SI'){$valordesesp='true';}
								else {$valordesesp='false';}
								if($rowver[6]==''){$valornumnorma=0;}
								else{$valornumnorma=$rowver[6];}
								if($rowver[10]==''){$valortercerochip=0;}
								else {$valortercerochip=$rowver[10];}
								if($rowver[11]==''){$valorpolitica=0;}
								else {$valorpolitica=$rowver[11];}
								if($_POST['tipoentidad'] == 'RS' ){
									fputs($Descriptor1, "D;".$rowver[1].";".$rowver[19].";".$caltotal5."\r\n");
								}elseif($_POST['tipoentidad'] == 'RG' ){
								}else{
									fputs($Descriptor1, "D;".$rowver[1].";".$valorcpc.";".$rowver[18] .";".$valorfuente.";".$valortercerochip.";".$valorpolitica.";".$valornumnorma.";".$tiponorma.";".$caltotal1.";".$caltotal2.";".$caltotal3.";".$caltotal4.";".$caltotal5."\r\n");
								}
								//fputs($Descriptor1, "D;".$rowver[1].";".$valorcpc.";".$rowver[18] .";".$valorfuente.";".$valortercerochip.";".$valorpolitica.";".$valornumnorma.";".$tiponorma.";".$caltotal1.";".$caltotal2.";".$caltotal3.";".$caltotal4.";".$caltotal5."\r\n");
								fputs($Descriptor2, "D\t".$rowver[1]."\t".$valorcpc."\t".$rowver[18]."\t".$valorfuente."\t".$valortercerochip."\t".$valorpolitica."\t".$valornumnorma."\t0\t".$caltotal1."\t".$caltotal2."\t".$caltotal3."\t".$caltotal4."\t".$caltotal5."\r\n");
								fputs($Descriptor3, "D\t".$rowver[1]."\t".$valorcpc."\t".$valorfuente."\t".$valortercerochip."\t".$valorpolitica."\t".$caltotal1."\t".$caltotal2."\t".$caltotal3."\t".$caltotal4."\t".$caltotal5."\r\n");
								fputs($Descriptor4, "D\t".$rowver[1]."\t".$rowver[19]."\t".$caltotal5."\r\n");
								fputs($Descriptor5, "D\t".$rowver[1]."\t".$rowver[3]."\t".$rowver[4]."\t".$rowver[5]."\t".$rowver[6]."\t".$rowver[7]."\t".round($rowver[8],2)."\r\n");
								for ($yz = 0; $yz < $contnormas; $yz++){

									fputs($Descriptor1, "D;".$rowver[1].";".$valorcpc.";".$detsectorialxyz[$yz].";".$fuentesxyz[$yz].";".$valortercerochip.";".$valorpolitica.";".substr($numeronormaxyz[$yz], 0, 25).";".$tiponormaxyz[$yz].";".$porcentajexyz1[$yz].";".$porcentajexyz2[$yz].";".$porcentajexyz3[$yz].";".$porcentajexyz4[$yz].";".$porcentajexyz5[$yz]."\r\n");

									fputs($Descriptor2, "D\t".$rowver[1]."\t".$valorcpc."\t".$detsectorialxyz[$yz]."\t".$fuentesxyz[$yz]."\t".$valortercerochip."\t".$valorpolitica."\t".substr($numeronormaxyz[$yz], 0, 25)."\t".$tiponormaxyz[$yz]."\t".$porcentajexyz1[$yz]."\t".$porcentajexyz2[$yz]."\t".$porcentajexyz3[$yz]."\t".$porcentajexyz4[$yz]."\t".$porcentajexyz5[$yz]."\r\n");

									fputs($Descriptor3, "D\t".$rowver[1]."\t".$valorcpc."\t".$fuentesxyz[$yz]."\t".$valortercerochip."\t".$valorpolitica."\t".$porcentajexyz1[$yz]."\t".$porcentajexyz2[$yz]."\t".$porcentajexyz3[$yz]."\t".$porcentajexyz4[$yz]."\t".$porcentajexyz5[$yz]."\r\n");

									fputs($Descriptor4, "D\t".$rowver[1]."\t".$rowver[19]."\t".$porcentajexyz5[$yz]."\r\n");
								}
							}
							echo "
								</table>
							";
							fclose($Descriptor1);
							fclose($Descriptor2);
							fclose($Descriptor3);
							fclose($Descriptor4);
							fclose($Descriptor5);
							echo "
							<script>document.getElementById('progreso').style.display='none';</script>";

						}break;
						//PROGRAMACION DE GASTOS
						case 3:{
							unset ($numerocuenta, $nombrecuenta, $secpresupuestal, $vigenciagasto, $sector, $programatico, $bpin, $apropiaini, $apropiadef, $registroinfo, $numerocuentaaux, $sectorial);
							$numid = $c = $idtemporal = 0;
							$namearch = "archivos/".$_SESSION['usuario']."informecgr".$fec.".csv";
							$Descriptor1 = fopen($namearch, "w+");
							$namearch2 = "archivos/".$informes[$_POST['reporte']].".txt";
							$Descriptor2 = fopen($namearch2, "w+");
							$namearch3 = "archivos/".$informes[$_POST['reporte']]."b.txt";
							$Descriptor3 = fopen($namearch3, "w+");
							$namearch4 = "archivos/".$informes[$_POST['reporte']]."_AESGPRI.txt";
							$Descriptor4 = fopen($namearch4, "w+");
							$namearch5 = "archivos/".$informes[$_POST['reporte']]."c.txt";
							$Descriptor5 = fopen($namearch5, "w+");
							if($_POST['tipoentidad'] == 'RS' ){
								echo "
									<table class='inicio' align='center'>
										<tr>
											<td class='titulos' colspan='8' ><p class='neon_titulos' id='titulogento'></p></td>
										</tr>
										<tr>
											<td colspan='4' id='totalcuentas'></td>
											<td colspan='4'>
												<div id='progreso' class='ProgressBar' style='display:none; float:left'>
													<div class='ProgressBarText'><span id='getprogress'></span>&nbsp;% </div>
													<div id='getProgressBarFill'></div>
												</div>
											</td>
										</tr>
										<tr>
											<td class='titulos2' style='width:2px;'><img src='imagenes/plus.gif'></td>
											<td class='titulos2' colspan='2'>&Aacute;RBOL DE CONCEPTOS</td>
											<td class='titulos2'>AESGPRIET</td>
											<td class='titulos2'>VIGENCIA DEL GASTO</td>
											<td class='titulos2'>PROGRAMATICO</td>
											<td class='titulos2'>APROPIACI&Oacute;N INICIAL</td>
											<td class='titulos2'>APROPIACI&Oacute;N DEFINITIVA</td>
										</tr>";
								fputs($Descriptor1, "S;".$_POST['codent'].";".$_POST['periodo'].";".$vigusu.";C_PROGRAMACION_DE_GASTOS_AESGPRI\r\n");
								fputs($Descriptor1, "S;ARBOL DE CONCEPTOS;AESGPRIET;VIGENCIA DEL GASTO;PROGRAMATICO;APROPIACION INICIAL;APROPIACION DEFINITIVA\r\n");
							}elseif($_POST['tipoentidad'] == 'RG' ){
								echo "
									<table class='inicio' align='center'>
										<tr>
											<td class='titulos' colspan='16' ><p class='neon_titulos' id='titulogento'></p></td>
										</tr>
										<tr>
											<td colspan='7' id='totalcuentas'></td>
											<td colspan='7'>
												<div id='progreso' class='ProgressBar' style='display:none; float:left'>
													<div class='ProgressBarText'><span id='getprogress'></span>&nbsp;% </div>
													<div id='getProgressBarFill'></div>
												</div>
											</td>
										</tr>
										<tr>
											<td class='titulos2' style='width:2px;'><img src='imagenes/plus.gif'></td>
											<td class='titulos2'>&Aacute;RBOL DE CONCEPTOS</td>
											<td class='titulos2'>SECTOR</td>
											<td class='titulos2'>FUENTE DE FINANCIACION</td>
											<td class='titulos2'>TIPO RECURSO</td>
											<td class='titulos2'>PROGRAMATICO MGA </td>
											<td class='titulos2'>BPIN</td>
											<td class='titulos2'>TERCEROS</td>
											<td class='titulos2'>POLITICA PUBLICA</td>
											<td class='titulos2'>SITUACION DE FONDOS</td>
											<td class='titulos2'>APROPIACI&Oacute;N INICIAL</td>
											<td class='titulos2'>APROPIACI&Oacute;N DEFINITIVA</td>
										</tr>";
								fputs($Descriptor1, "S;".$_POST['codent'].";".$_POST['periodo'].";".$vigusu.";PROGRAMACION_DE_GASTOS\r\n");
								fputs($Descriptor1, "S;ARBOL DE CONCEPTOS;SECTOR;TIPO RECURSO;PROGRAMATICO MGA; BPIN;>TERCEROS;POLITICA PUBLICA;SITUACION DE FONDOS;APROPIACION INICIAL;APROPIACION DEFINITIVA\r\n");
							}else{
								echo "
									<table class='inicio' align='center'>
										<tr>
											<td class='titulos' colspan='16' ><p class='neon_titulos' id='titulogento'></p></td>
										</tr>
										<tr>
											<td colspan='4' id='totalcuentas'></td>
											<td colspan='4'>
												<div id='progreso' class='ProgressBar' style='display:none; float:left'>
													<div class='ProgressBarText'><span id='getprogress'></span>&nbsp;% </div>
													<div id='getProgressBarFill'></div>
												</div>
											</td>
										</tr>
										<tr>
											<td class='titulos2' style='width:2px;'><img src='imagenes/plus.gif'></td>
											<td class='titulos2' colspan='2'>&Aacute;RBOL DE CONCEPTOS</td>
											<td class='titulos2'>VIGENCIA DEL GASTO</td>
											<td class='titulos2'>SECCIÓN PRESUPUESTAL</td>
											<td class='titulos2'>PROGRAMATICO MGA </td>
                                            <td class='titulos2'>SECTORIAL </td>
											<td class='titulos2'>BPIN</td>
											<td class='titulos2'>APROPIACI&Oacute;N INICIAL</td>
											<td class='titulos2'>APROPIACI&Oacute;N DEFINITIVA</td>
										</tr>";
								fputs($Descriptor1, "S;".$_POST['codent'].";".$_POST['periodo'].";".$vigusu.";C_PROGRAMACION_DE_GASTOS\r\n");
								fputs($Descriptor1, "S;ARBOL DE CONCEPTOS;VIGENCIA DEL GASTO;SECCION PRESUPUESTAL;PROGRAMATICO MGA; SECTORIAL; BPIN;APROPIACION INICIAL;APROPIACION DEFINITIVA\r\n");
							}
							$mes2 = substr($_POST['periodo'], 3, 2);
							$_POST['fecha'] = "01/01/$vigusu";
							$_POST['fecha2'] = intval(date("t", $mes2))."/$mes2/$vigusu";
							preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST['fecha'],$fecha);
							$fechai = "$fecha[3]-$fecha[2]-$fecha[1]";
							preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST['fecha2'],$fecha);
							$fechaf = "$fecha[3]-$fecha[2]-$fecha[1]";
							$fecha_actual = date("d-m-Y");

							if($_POST['tipoentidad'] == 'RS' ){

								/* fputs($Descriptor4, "S\t".$_POST['codent']."\t".$_POST['periodo']."\t".$vigusu. "\tC_PROGRAMACION_DE_GASTOS_AESGPRI\r\n"); */

							}else if($_POST['tipoentidad'] == 'RG' ){


							}else{

							}
							fputs($Descriptor2, "S\t".$_POST['codent']."\t".$_POST['periodo']."\t".$vigusu. "\tC_PROGRAMACION_DE_GASTOS\r\n");
							fputs($Descriptor3, "S\t".$_POST['codent']."\t".$_POST['periodo']."\t".$vigusu. "\tC_PROGRAMACION_DE_GASTOS\r\n");
							fputs($Descriptor4, "S\t".$_POST['codent']."\t".$_POST['periodo']."\t".$vigusu. "\tC_PROGRAMACION_DE_GASTOS_AESGPRI\r\n");
							fputs($Descriptor5, "S\t".$_POST['codent']."\t".$_POST['periodo']."\t".$vigusu. "\tPROGRAMACION_DE_GASTOS"."\t".$fecha_actual."\r\n");

							if($_SESSION['tablatemporal'] != '' && $_SESSION['tablatemporal'] != null){
								$sqlr = "DROP TABLE ".$_SESSION['tablatemporal'];
								mysqli_query($linkbd,$sqlr);
								$sqlr = "CREATE TABLE ".$_SESSION['tablatemporal']." (id int(11), codigocuenta varchar(100), nombrecuenta varchar(200), seccpresupuestal varchar(10), vigenciagastos varchar(10), sector varchar(10), programatico varchar(50), bpin varchar(20),apropiacionini double, apropiaciondef double, registro varchar(100), codigocuentaaux varchar(100), aesgpriet varchar(15), sectorial varchar(100))";
								mysqli_query($linkbd,$sqlr);
							}

							$sqlredglobal ="SELECT codigo, base, nombre, tipo, usuario FROM redglobal WHERE cuipo ='S'  ORDER BY id ASC";
							$resredglobal = mysqli_query($linkbd,$sqlredglobal);
							while ($rowredglobal = mysqli_fetch_row($resredglobal)){
								$c = $numid = 0;
								echo"
								<script>document.getElementById('titulogento').innerHTML='.: C. PROGRAMACI&Oacute;N DE GASTOS: $rowredglobal[2]';</script>";
								$linkmulti = conectar_Multi($rowredglobal[1],$rowredglobal[4]);
								$linkmulti -> set_charset("utf8");
								$maxVersion = ultimaVersionGastosCCPETmulti($rowredglobal[1],$rowredglobal[4]);
								$sqlcta ="SELECT codigo,nombre FROM cuentasccpet WHERE version = '$maxVersion' AND municipio = '1' AND tipo = 'C' ORDER BY id ASC";
								$rescta = mysqli_query($linkmulti,$sqlcta);
								$totalcli= mysqli_num_rows($rescta);
								while ($rowcta = mysqli_fetch_row($rescta)){
									$numid++;
									$c++;
									//INICIAL GASTOS
									{
										$sqlinicialgastosfun = "SELECT vigencia_gasto, valor, id, seccion_presupuestal FROM ccpetinicialgastosfun WHERE cuenta = '$rowcta[0]' AND vigencia = '$vigusu'";
										$resinicialgastosfun = mysqli_query($linkmulti,$sqlinicialgastosfun);
										while ($rowinicialgastosfun = mysqli_fetch_row($resinicialgastosfun)){
											$numerocuenta[] = $rowcta[0];
											$nombrecuenta[] = $rowcta[1];
											$numerocuentaaux[] = $rowcta[0];
											if($rowinicialgastosfun[3] != ''){$uniejecuaux = $rowinicialgastosfun[3];}
											else {$uniejecuaux = $rowredglobal[0];}
											if($_POST['tipoentidad'] == 'RS' ){
												$secpresupuestal[] = $uniejecuaux;
											}elseif($_POST['tipoentidad'] == 'RG' ){
											}else{
												$sqlunieje = "SELECT id_unidad_ejecutora FROM pptoseccion_presupuestal WHERE estado = 'S' AND id_seccion_presupuestal = '$uniejecuaux'";
												$resunieje = mysqli_query($linkmulti,$sqlunieje);
												$rowunieje = mysqli_fetch_row($resunieje);
												$secpresupuestal[] = $rowunieje[0];
											}

											$vigenciagasto[] = $rowinicialgastosfun[0];
											$sector[] = 0;
											$programatico[] = 0;
											$bpin[] = 0;
                                            $sectorial[] = 0;
											$apropiaini[] = round($rowinicialgastosfun[1]);
											$apropiadef[] = round($rowinicialgastosfun[1]);
											$registroinfo[] = "Gasto No: ".$rowinicialgastosfun[2];
										}
									}
									//ADICIONES POR CUENTA
									{
										$sqlAdicion = "SELECT valor, id_adicion, codigo_vigenciag, seccion_presupuestal FROM ccpet_adiciones WHERE cuenta = '$rowcta[0]' AND vigencia = '$vigusu' AND estado != 'N' AND fecha BETWEEN '$fechai' AND '$fechaf' AND codigo_vigenciag <> 7";
										$resAdicion = mysqli_query($linkmulti, $sqlAdicion);
										while ($rowAdicion = mysqli_fetch_row($resAdicion)){
											$numerocuenta[] = $rowcta[0];
											$nombrecuenta[] = $rowcta[1];
											$numerocuentaaux[] = $rowcta[0];
											if($rowAdicion[3] != ''){$uniejecuaux = $rowAdicion[3];}
											else {$uniejecuaux = $rowredglobal[0];}
											if($_POST['tipoentidad'] == 'RS' ){
												$secpresupuestal[] = $uniejecuaux;
											}elseif($_POST['tipoentidad'] == 'RG' ){
											}else{
												$sqlunieje = "SELECT id_unidad_ejecutora FROM pptoseccion_presupuestal WHERE estado = 'S' AND id_seccion_presupuestal = '$uniejecuaux'";
												$resunieje = mysqli_query($linkmulti,$sqlunieje);
												$rowunieje = mysqli_fetch_row($resunieje);
												$secpresupuestal[] = $rowunieje[0];
											}
											$vigenciagasto[] = $rowAdicion[2];
											$sector[] = 0;
											$programatico[] = 0;
											$bpin[] = 0;
                                            $sectorial[] = 0;
											$apropiaini[] = 0;
											$apropiadef[] = round($rowAdicion[0]);
											$registroinfo[] = "Adiciones por Cuenta No: ".$rowAdicion[1];
										}
									}
									//REDUCCIONES
									{
										$sqlReduccion = "SELECT valor, id_adicion, codigo_vigenciag, seccion_presupuestal FROM ccpet_reducciones WHERE cuenta = '$rowcta[0]' AND fecha BETWEEN '$fechai' AND '$fechaf' AND vigencia = '$vigusu' AND estado != 'N' AND codigo_vigenciag <> 7";
										$resReduccion = mysqli_query($linkmulti, $sqlReduccion);
										while ($rowReduccion = mysqli_fetch_row($resReduccion))
										{
											$numerocuenta[] = $rowcta[0];
											$nombrecuenta[] = $rowcta[1];
											$numerocuentaaux[] = $rowcta[0];
											if($rowReduccion[3] != ''){$uniejecuaux = $rowReduccion[3];}
											else {$uniejecuaux = $rowredglobal[0];}
											if($_POST['tipoentidad'] == 'RS' ){
												$secpresupuestal[] = $uniejecuaux;
											}elseif($_POST['tipoentidad'] == 'RG' ){
											}else{
												$sqlunieje = "SELECT id_unidad_ejecutora FROM pptoseccion_presupuestal WHERE estado = 'S' AND id_seccion_presupuestal = '$uniejecuaux'";
												$resunieje = mysqli_query($linkmulti,$sqlunieje);
												$rowunieje = mysqli_fetch_row($resunieje);
												$secpresupuestal[] = $rowunieje[0];
											}
											$vigenciagasto[] = $rowReduccion[2];
											$sector[] = 0;
											$programatico[] = 0;
											$bpin[] = 0;
                                            $sectorial[] = 0;
											$apropiaini[] = 0;
											$apropiadef[] = -1 * round($rowReduccion[0]);
											$registroinfo[] = "Reducciones No: ".$rowReduccion[1];
										}
									}
									//TRASLADOS CREDITO
									{
										$sqlCredito = "SELECT valor, id_traslado, codigo_vigenciag, seccion_presupuestal FROM ccpet_traslados WHERE cuenta = '$rowcta[0]' AND fecha BETWEEN '$fechai' AND '$fechaf' AND vigencia = '$vigusu' AND estado != 'N' AND tipo = 'C' AND codigo_vigenciag <> 7";
										$resCredito = mysqli_query($linkmulti, $sqlCredito);
										while ($rowCredito = mysqli_fetch_row($resCredito)){
											$numerocuenta[] = $rowcta[0];
											$nombrecuenta[] = $rowcta[1];
											$numerocuentaaux[] = $rowcta[0];
											if($rowCredito[3] != ''){$uniejecuaux = $rowCredito[3];}
											else {$uniejecuaux = $rowredglobal[0];}
											if($_POST['tipoentidad'] == 'RS' ){
												$secpresupuestal[] = $uniejecuaux;
											}elseif($_POST['tipoentidad'] == 'RG' ){
											}else{
												$sqlunieje = "SELECT id_unidad_ejecutora FROM pptoseccion_presupuestal WHERE estado = 'S' AND id_seccion_presupuestal = '$uniejecuaux'";
												$resunieje = mysqli_query($linkmulti,$sqlunieje);
												$rowunieje = mysqli_fetch_row($resunieje);
												$secpresupuestal[] = $rowunieje[0];
											}
											$vigenciagasto[] = $rowCredito[2];
											$sector[] = 0;
											$programatico[] = 0;
											$bpin[] = 0;
                                            $sectorial[] = 0;
											$apropiaini[] = 0;
											$apropiadef[] = round($rowCredito[0]);
											$registroinfo[] = "Traslados Credito No: ".$rowCredito[1];
										}
									}
									//TRASLADOS contracredito
									{
										$sqlCredito = "SELECT valor, id_traslado, codigo_vigenciag, seccion_presupuestal FROM ccpet_traslados WHERE cuenta = '$rowcta[0]' AND fecha BETWEEN '$fechai' AND '$fechaf' AND vigencia = '$vigusu' AND estado != 'N' AND tipo = 'R' AND codigo_vigenciag <> 7";
										$resCredito = mysqli_query($linkmulti, $sqlCredito);
										while ($rowCredito = mysqli_fetch_row($resCredito)){
											$numerocuenta[] = $rowcta[0];
											$nombrecuenta[] = $rowcta[1];
											$numerocuentaaux[] = $rowcta[0];
											if($rowCredito[3] != ''){$uniejecuaux = $rowCredito[3];}
											else {$uniejecuaux = $rowredglobal[0];}
											if($_POST['tipoentidad'] == 'RS' ){
												$secpresupuestal[] = $uniejecuaux;
											}elseif($_POST['tipoentidad'] == 'RG' ){
											}else{
												$sqlunieje = "SELECT id_unidad_ejecutora FROM pptoseccion_presupuestal WHERE estado = 'S' AND id_seccion_presupuestal = '$uniejecuaux'";
												$resunieje = mysqli_query($linkmulti,$sqlunieje);
												$rowunieje = mysqli_fetch_row($resunieje);
												$secpresupuestal[] = $rowunieje[0];
											}
											$vigenciagasto[] = $rowCredito[2];
											$sector[] = 0;
											$programatico[] = 0;
											$bpin[] = 0;
                                            $sectorial[] = 0;
											$apropiaini[] = 0;
											$apropiadef[] = -1 * round($rowCredito[0]);
											$registroinfo[] = "Traslados Credito No: ".$rowCredito[1];
										}
									}
									$porcentaje = $c * 100 / $totalcli;
									echo"
									<script>
										progres='".round($porcentaje)."';callprogress(progres);
										document.getElementById('totalcuentas').innerHTML='Recolectando Información: $numid / $totalcli';
									</script>";
									flush();
									ob_flush();
									usleep(5);
								}
								//PROYECTOS PRESUPUESTO
								{
									$sqlrProyectos = "
									SELECT PD.valorcsf, PD.valorssf, PD.indicador_producto, P.codigo, P.id, PD.vigencia_gasto, P.idunidadej, PD.id_fuente
									FROM ccpproyectospresupuesto AS P
									INNER JOIN ccpproyectospresupuesto_presupuesto AS PD
									ON P.id = PD.codproyecto
									WHERE P.vigencia = '$vigusu'";
									$resProyectos = mysqli_query($linkmulti, $sqlrProyectos);
									while ($rowProyectos = mysqli_fetch_row($resProyectos)){

										$sectorProyecto = substr($rowProyectos[2],0, 2);
                                        $programatico2 = substr($rowProyectos[2],0, 7);

										$fuente = $rowProyectos[7];
										$nombrecuenta[] = 'CONSOLIDADO';
										$sector[] = $sectorProyecto;
										$uniejecuaux = $rowredglobal[0];

										if($_POST['tipoentidad'] == 'RS' ){

											$numerocuenta[] = '2.99';
											$numerocuentaaux[] = '2.99';
											$secpresupuestal[] = $rowProyectos[6];
											$codm1 = substr($rowProyectos[2],0,6);

											$vigenciagasto[] = $rowProyectos[5];

										}elseif($_POST['tipoentidad'] == 'RG'){

											$programaticoProyecto = substr($rowProyectos[2],0, 4);
											$numerocuentaauxFuente[] = $fuente;

											$sqlrCuentasSGR = "SELECT cuentasgr FROM rubros_cuentassgr WHERE bpim = '$rowProyectos[3]' AND sector = '$sectorProyecto' AND fuente = '$fuente' AND programatico LIKE '$programaticoProyecto%' AND opcion = ''";
											$resCuentasSGR = mysqli_query($linkmulti, $sqlrCuentasSGR);
											$rowCuentasSGR = mysqli_fetch_row($resCuentasSGR);
											$numerocuenta[] = $rowCuentasSGR[0];

											$sqlrFuenteSGR = "SELECT cuentasgr FROM rubros_cuentassgr WHERE bpim = '$rowProyectos[3]' AND sector = '$sectorProyecto' AND fuente = '$fuente' AND programatico LIKE '$programaticoProyecto%' AND opcion = '1'";
											$resFuenteSGR = mysqli_query($linkmulti, $sqlrFuenteSGR);
											$rowFuenteSGR = mysqli_fetch_row($resFuenteSGR);
											if($rowFuenteSGR[0] != ''){
												$fuente = $rowFuenteSGR[0];
											}

											if($rowProyectos[0] > 0){
												$vigenciagasto[] = 'S';
											}else{
												$vigenciagasto[] = 'C';
											}



											/* $numerocuenta[] = '2.99'; */
											$numerocuentaaux[] = $fuente;


											$codm1 = $programaticoProyecto;

										}else{

											$numerocuenta[] = '2.99';
											$numerocuentaaux[] = '2.99';

											$sqlunieje = "SELECT id_unidad_ejecutora FROM pptoseccion_presupuestal WHERE estado = 'S' AND id_seccion_presupuestal = '$uniejecuaux'";
											$resunieje = mysqli_query($linkmulti,$sqlunieje);
											$rowunieje = mysqli_fetch_row($resunieje);
											$secpresupuestal[] = $rowunieje[0];
											$codm1 = substr($rowProyectos[2],0,4);

											$vigenciagasto[] = $rowProyectos[5];

										}

                                        $sqlrDetSectorial = "SELECT cuentasgr FROM ccpetdetallesectorialprogastos WHERE sector = '$sectorProyecto' AND fuente = '$fuente' AND bpim = '$rowProyectos[3]' AND programatico = '$programatico2'";
                                        $resrDetSectorial = mysqli_query($linkmulti,$sqlrDetSectorial);
                                        $rowrDetSectorial = mysqli_fetch_row($resrDetSectorial);

                                        if($rowrDetSectorial[0] == ''){
                                            $detsectorial = 0;
                                        }else{
                                            $detsectorial= $rowrDetSectorial[0];
                                        }

                                        $sectorial[] = $detsectorial;

										if($codm1 == ''){$programatico[] = 0;}
										else {$programatico[] = $codm1;}

										if($rowProyectos[3] == ''){$bpin[] = 0;}
										else {$bpin[] = $rowProyectos[3];}
										$apropiaini[] = round($rowProyectos[0]+$rowProyectos[1]);
										$apropiadef[] = round($rowProyectos[0]+$rowProyectos[1]);
										$registroinfo[] = "Proyectos No: ".$rowProyectos[3];
									}
								}
								//ADICIONES POR PROYECTO
								{
									$sqlAdicion = "SELECT valor, id_adicion, codigo_vigenciag, programatico, bpim, seccion_presupuestal, fuente, medio_pago FROM ccpet_adiciones WHERE cuenta = '' AND vigencia = '$vigusu' AND estado != 'N' AND fecha BETWEEN '$fechai' AND '$fechaf' AND codigo_vigenciag <> 7";
									$resAdicion = mysqli_query($linkmulti, $sqlAdicion);
									while ($rowAdicion = mysqli_fetch_row($resAdicion)){

										$sectorProyecto = substr($rowAdicion[3],0, 2);
                                        $programatico2 = substr($rowAdicion[3],0, 7);

										$fuente = $rowAdicion[6];
										$nombrecuenta[] = 'CONSOLIDADO';
										$sector[] = $sectorProyecto;

										if($rowAdicion[5] != ''){$uniejecuaux = $rowAdicion[5];}
										else {$uniejecuaux = $rowredglobal[0];}

										if($_POST['tipoentidad'] == 'RS' ){

											$numerocuenta[] = '2.99';
											$numerocuentaaux[] = '2.99';
											$secpresupuestal[] = $rowAdicion [5];
											$codm1 = substr($rowAdicion[3],0,6);

											$vigenciagasto[] = $rowAdicion[2];

										}elseif($_POST['tipoentidad'] == 'RG' ){

											$numerocuentaauxFuente[] = $fuente;

											$programaticoProyecto = substr($rowAdicion[3],0, 4);
											$sqlrCuentasSGR = "SELECT cuentasgr FROM rubros_cuentassgr WHERE bpim = '$rowAdicion[4]' AND sector = '$sectorProyecto' AND fuente = '$fuente' AND programatico LIKE '$programaticoProyecto%' AND opcion = ''";
											$resCuentasSGR = mysqli_query($linkmulti, $sqlrCuentasSGR);
											$rowCuentasSGR = mysqli_fetch_row($resCuentasSGR);
											$numerocuenta[] = $rowCuentasSGR[0];

											$sqlrFuenteSGR = "SELECT cuentasgr FROM rubros_cuentassgr WHERE bpim = '$rowAdicion[4]' AND sector = '$sectorProyecto' AND fuente = '$fuente' AND programatico LIKE '$programaticoProyecto%' AND opcion = '1'";
											$resFuenteSGR = mysqli_query($linkmulti, $sqlrFuenteSGR);
											$rowFuenteSGR = mysqli_fetch_row($resFuenteSGR);
											if($rowFuenteSGR[0] != ''){
												$fuente = $rowFuenteSGR[0];
											}

											/* $numerocuenta[] = '2.99'; */
											$numerocuentaaux[] = $fuente;

											if($rowAdicion[7] == 'CSF'){
												$vigenciagasto[] = 'C';
											}else{
												$vigenciagasto[] = 'S';
											}

											$codm1 = $programaticoProyecto;

										}else{

											$numerocuenta[] = '2.99';
											$numerocuentaaux[] = '2.99';
											$sqlunieje = "SELECT id_unidad_ejecutora FROM pptoseccion_presupuestal WHERE estado = 'S' AND id_seccion_presupuestal = '$uniejecuaux'";
											$resunieje = mysqli_query($linkmulti,$sqlunieje);
											$rowunieje = mysqli_fetch_row($resunieje);
											$secpresupuestal[] = $rowunieje[0];
											$codm1 = substr($rowAdicion[3],0,4);

											$vigenciagasto[] = $rowAdicion[2];

										}

										/* $sector[] = substr($rowAdicion[3],0, 2); */

                                        $sqlrDetSectorial = "SELECT cuentasgr FROM ccpetdetallesectorialprogastos WHERE sector = '$sectorProyecto' AND fuente = '$fuente' AND bpim = '$rowAdicion[4]' AND programatico = '$programatico2'";
                                        $resrDetSectorial = mysqli_query($linkmulti,$sqlrDetSectorial);
                                        $rowrDetSectorial = mysqli_fetch_row($resrDetSectorial);

                                        if($rowrDetSectorial[0] == ''){
                                            $detsectorial = 0;
                                        }else{
                                            $detsectorial= $rowrDetSectorial[0];
                                        }

                                        $sectorial[] = $detsectorial;

										if($codm1 == ''){$programatico[] = 0;}
										else {$programatico[] = $codm1;}
										if($rowAdicion[4] == ''){$bpin[] = 0;}
										else {$bpin[] = $rowAdicion[4];}
										$apropiaini[] = 0;
										$apropiadef[] = round($rowAdicion[0]);
										$registroinfo[] = "Adiciones por Proyecto No: ".$rowAdicion[1];
									}
								}
								//REDUCCIONES POR PROYECTO
								{
									$sqlAdicion = "SELECT valor, id_adicion, codigo_vigenciag, programatico, bpim, seccion_presupuestal, fuente, medio_pago FROM ccpet_reducciones WHERE cuenta = '' AND vigencia = '$vigusu' AND estado != 'N' AND fecha BETWEEN '$fechai' AND '$fechaf' AND codigo_vigenciag <> 7";
									$resAdicion = mysqli_query($linkmulti, $sqlAdicion);
									while ($rowAdicion = mysqli_fetch_row($resAdicion)){

										$sectorProyecto = substr($rowAdicion[3],0, 2);
                                        $programatico2 = substr($rowAdicion[3],0, 7);

										$fuente = $rowAdicion[6];
										$nombrecuenta[] = 'CONSOLIDADO';
										$sector[] = $sectorProyecto;

										if($rowAdicion[5] != ''){$uniejecuaux = $rowAdicion[5];}
										else {$uniejecuaux = $rowredglobal[0];}
										if($_POST['tipoentidad'] == 'RS' ){

											$numerocuenta[] = '2.99';
											$numerocuentaaux[] = '2.99';
											$secpresupuestal[] = $rowAdicion[5];
											$codm1 = substr($rowAdicion[3],0,6);

											$vigenciagasto[] = $rowAdicion[2];

										}elseif($_POST['tipoentidad'] == 'RG' ){

											$numerocuentaauxFuente[] = $fuente;

											$programaticoProyecto = substr($rowAdicion[3],0, 4);
											$sqlrCuentasSGR = "SELECT cuentasgr FROM rubros_cuentassgr WHERE bpim = '$rowAdicion[4]' AND sector = '$sectorProyecto' AND fuente = '$fuente' AND programatico LIKE '$programaticoProyecto%'";
											$resCuentasSGR = mysqli_query($linkmulti, $sqlrCuentasSGR);
											$rowCuentasSGR = mysqli_fetch_row($resCuentasSGR);
											$numerocuenta[] = $rowCuentasSGR[0];

											$sqlrFuenteSGR = "SELECT cuentasgr FROM rubros_cuentassgr WHERE bpim = '$rowAdicion[4]' AND sector = '$sectorProyecto' AND fuente = '$fuente' AND programatico LIKE '$programaticoProyecto%' AND opcion = '1'";
											$resFuenteSGR = mysqli_query($linkmulti, $sqlrFuenteSGR);
											$rowFuenteSGR = mysqli_fetch_row($resFuenteSGR);
											if($rowFuenteSGR[0] != ''){
												$fuente = $rowFuenteSGR[0];
											}
											/* $numerocuenta[] = '2.99'; */
											$numerocuentaaux[] = $fuente;

											if($rowAdicion[7] == 'CSF'){
												$vigenciagasto[] = 'C';
											}else{
												$vigenciagasto[] = 'S';
											}

											$codm1 = $programaticoProyecto;


										}else{

											$numerocuenta[] = '2.99';
											$numerocuentaaux[] = '2.99';

											$sqlunieje = "SELECT id_unidad_ejecutora FROM pptoseccion_presupuestal WHERE estado = 'S' AND id_seccion_presupuestal = '$uniejecuaux'";
											$resunieje = mysqli_query($linkmulti,$sqlunieje);
											$rowunieje = mysqli_fetch_row($resunieje);
											$secpresupuestal[] = $rowunieje[0];
											$codm1 = substr($rowAdicion[3],0,4);

											$vigenciagasto[] = $rowAdicion[2];

										}

										/* $sector[] = substr($rowAdicion[3],0, 2); */

                                        $sqlrDetSectorial = "SELECT cuentasgr FROM ccpetdetallesectorialprogastos WHERE sector = '$sectorProyecto' AND fuente = '$fuente' AND bpim = '$rowAdicion[4]' AND programatico = '$programatico2'";
                                        $resrDetSectorial = mysqli_query($linkmulti,$sqlrDetSectorial);
                                        $rowrDetSectorial = mysqli_fetch_row($resrDetSectorial);

                                        if($rowrDetSectorial[0] == ''){
                                            $detsectorial = 0;
                                        }else{
                                            $detsectorial= $rowrDetSectorial[0];
                                        }

                                        $sectorial[] = $detsectorial;

										if($codm1 == ''){$programatico[] = 0;}
										else {$programatico[] = $codm1;}
										if($rowAdicion[4] == ''){$bpin[] = 0;}
										else {$bpin[] = $rowAdicion[4];}
										$apropiaini[] = 0;
										$apropiadef[] = -1 * round($rowAdicion[0]);
										$registroinfo[] = "reducciones por Proyecto No: ".$rowAdicion[1];
									}
								}
								//TRASLADOS Proyectos
								{
									$sqlCredito = "SELECT valor, id_traslado, codigo_vigenciag, seccion_presupuestal, bpim, programatico, fuente, medio_pago FROM ccpet_traslados WHERE cuenta = '' AND fecha BETWEEN '$fechai' AND '$fechaf' AND vigencia = '$vigusu' AND estado != 'N' AND tipo = 'C' AND codigo_vigenciag <> 7";

									$resCredito = mysqli_query($linkmulti, $sqlCredito);
									while ($rowCredito = mysqli_fetch_row($resCredito)){

										$sectorProyecto = substr($rowCredito[5],0, 2);
                                        $programatico2 = substr($rowCredito[5],0, 7);

										$fuente = $rowCredito[6];
										$nombrecuenta[] = 'CONSOLIDADO';
										$sector[] = $sectorProyecto;

										if($rowCredito[3] != ''){$uniejecuaux = $rowCredito[3];}
										else {$uniejecuaux = $rowredglobal[0];}
										if($_POST['tipoentidad'] == 'RS' ){

											$numerocuenta[] = '2.99';
											$numerocuentaaux[] = '2.99';

											$secpresupuestal[] = $rowCredito[3];
											$codm1 = substr($rowCredito[5],0,6);

											$vigenciagasto[] = $rowCredito[2];

										}elseif($_POST['tipoentidad'] == 'RG' ){

											$numerocuentaauxFuente[] = $fuente;

											$programaticoProyecto = substr($rowCredito[5],0, 4);
											$sqlrCuentasSGR = "SELECT cuentasgr FROM rubros_cuentassgr WHERE bpim = '$rowCredito[4]' AND sector = '$sectorProyecto' AND fuente = '$fuente' AND programatico LIKE '$programaticoProyecto%'";
											$resCuentasSGR = mysqli_query($linkmulti, $sqlrCuentasSGR);
											$rowCuentasSGR = mysqli_fetch_row($resCuentasSGR);
											$numerocuenta[] = $rowCuentasSGR[0];

											$sqlrFuenteSGR = "SELECT cuentasgr FROM rubros_cuentassgr WHERE bpim = '$rowCredito[4]' AND sector = '$sectorProyecto' AND fuente = '$fuente' AND programatico LIKE '$programaticoProyecto%' AND opcion = '1'";
											$resFuenteSGR = mysqli_query($linkmulti, $sqlrFuenteSGR);
											$rowFuenteSGR = mysqli_fetch_row($resFuenteSGR);
											if($rowFuenteSGR[0] != ''){
												$fuente = $rowFuenteSGR[0];
											}

											/* $numerocuenta[] = '2.99'; */
											$numerocuentaaux[] = $fuente;

											if($rowCredito[7] == 'CSF'){
												$vigenciagasto[] = 'C';
											}else{
												$vigenciagasto[] = 'S';
											}

											$codm1 = $programaticoProyecto;

										}else{

											$numerocuenta[] = '2.99';
											$numerocuentaaux[] = '2.99';

											$sqlunieje = "SELECT id_unidad_ejecutora FROM pptoseccion_presupuestal WHERE estado = 'S' AND id_seccion_presupuestal = '$uniejecuaux'";
											$resunieje = mysqli_query($linkmulti,$sqlunieje);
											$rowunieje = mysqli_fetch_row($resunieje);
											$secpresupuestal[] = $rowunieje[0];
											$codm1 = substr($rowCredito[5],0,4);

											$vigenciagasto[] = $rowCredito[2];

										}


										/* $sector[] = 0; */

                                        $sqlrDetSectorial = "SELECT cuentasgr FROM ccpetdetallesectorialprogastos WHERE sector = '$sectorProyecto' AND fuente = '$fuente' AND bpim = '$rowCredito[4]' AND programatico = '$programatico2'";
                                        $resrDetSectorial = mysqli_query($linkmulti,$sqlrDetSectorial);
                                        $rowrDetSectorial = mysqli_fetch_row($resrDetSectorial);

                                        if($rowrDetSectorial[0] == ''){
                                            $detsectorial = 0;
                                        }else{
                                            $detsectorial= $rowrDetSectorial[0];
                                        }

                                        $sectorial[] = $detsectorial;

										if($codm1 == ''){$programatico[] = 0;}
										else {$programatico[] = $codm1;}
										$bpin[] = $rowCredito[4];
										$apropiaini[] = 0;
										$apropiadef[] = round($rowCredito[0]);
										$registroinfo[] = "Traslados Proyectos No: ".$rowCredito[1];
									}
								}
								//TRASLADOS Proyectos
								{
									$sqlCredito = "SELECT valor, id_traslado, codigo_vigenciag, seccion_presupuestal, bpim, programatico, fuente, medio_pago FROM ccpet_traslados WHERE cuenta = '' AND fecha BETWEEN '$fechai' AND '$fechaf' AND vigencia = '$vigusu' AND estado != 'N' AND tipo = 'R' AND codigo_vigenciag <> 7";
									$resCredito = mysqli_query($linkmulti, $sqlCredito);
									while ($rowCredito = mysqli_fetch_row($resCredito)){

										$sectorProyecto = substr($rowCredito[5],0, 2);
                                        $programatico2 = substr($rowCredito[5],0, 7);

										$fuente = $rowCredito[6];
										$nombrecuenta[] = 'CONSOLIDADO';
										$sector[] = $sectorProyecto;

										if($rowCredito[3] != ''){$uniejecuaux = $rowCredito[3];}
										else {$uniejecuaux = $rowredglobal[0];}

										if($_POST['tipoentidad'] == 'RS' ){

											$numerocuenta[] = '2.99';
											$numerocuentaaux[] = '2.99';
											$secpresupuestal[] = $rowCredito[3];
											$codm1 = substr($rowCredito[5],0,6);

											$vigenciagasto[] = $rowCredito[2];

										}elseif($_POST['tipoentidad'] == 'RG' ){

											$numerocuentaauxFuente[] = $fuente;

											$programaticoProyecto = substr($rowCredito[5],0, 4);
											$sqlrCuentasSGR = "SELECT cuentasgr FROM rubros_cuentassgr WHERE bpim = '$rowCredito[4]' AND sector = '$sectorProyecto' AND fuente = '$fuente' AND programatico LIKE '$programaticoProyecto%'";
											$resCuentasSGR = mysqli_query($linkmulti, $sqlrCuentasSGR);
											$rowCuentasSGR = mysqli_fetch_row($resCuentasSGR);
											$numerocuenta[] = $rowCuentasSGR[0];

											$sqlrFuenteSGR = "SELECT cuentasgr FROM rubros_cuentassgr WHERE bpim = '$rowCredito[4]' AND sector = '$sectorProyecto' AND fuente = '$fuente' AND programatico LIKE '$programaticoProyecto%' AND opcion = '1'";
											$resFuenteSGR = mysqli_query($linkmulti, $sqlrFuenteSGR);
											$rowFuenteSGR = mysqli_fetch_row($resFuenteSGR);
											if($rowFuenteSGR[0] != ''){
												$fuente = $rowFuenteSGR[0];
											}
											/* $numerocuenta[] = '2.99'; */
											$numerocuentaaux[] = $fuente;

											if($rowCredito[7] == 'CSF'){
												$vigenciagasto[] = 'C';
											}else{
												$vigenciagasto[] = 'S';
											}

											$codm1 = $programaticoProyecto;

										}else{

											$numerocuenta[] = '2.99';
											$numerocuentaaux[] = '2.99';

											$sqlunieje = "SELECT id_unidad_ejecutora FROM pptoseccion_presupuestal WHERE estado = 'S' AND id_seccion_presupuestal = '$uniejecuaux'";
											$resunieje = mysqli_query($linkmulti,$sqlunieje);
											$rowunieje = mysqli_fetch_row($resunieje);
											$secpresupuestal[] = $rowunieje[0];
											$codm1 = substr($rowCredito[5],0,4);

											$vigenciagasto[] = $rowCredito[2];

										}


										/* $sector[] = 0; */

                                        $sqlrDetSectorial = "SELECT cuentasgr FROM ccpetdetallesectorialprogastos WHERE sector = '$sectorProyecto' AND fuente = '$fuente' AND bpim = '$rowCredito[4]' AND programatico = '$programatico2'";
                                        $resrDetSectorial = mysqli_query($linkmulti,$sqlrDetSectorial);
                                        $rowrDetSectorial = mysqli_fetch_row($resrDetSectorial);

                                        if($rowrDetSectorial[0] == ''){
                                            $detsectorial = 0;
                                        }else{
                                            $detsectorial= $rowrDetSectorial[0];
                                        }

                                        $sectorial[] = $detsectorial;


										if($codm1 == ''){$programatico[] = 0;}
										else {$programatico[] = $codm1;}
										$bpin[] = $rowCredito[4];
										$apropiaini[] = 0;
										$apropiadef[] = -1 * round($rowCredito[0]);
										$registroinfo[] = "Traslados Proyectos No: ".$rowCredito[1];
									}
								}
								//Reducciones Proyectos
								/* {
									$sqlreduccproy = "SELECT valor, id_traslado, codigo_vigenciag, seccion_presupuestal, bpim, programatico FROM ccpet_traslados WHERE cuenta = '' AND fecha BETWEEN '$fechai' AND '$fechaf' AND vigencia = '$vigusu' AND estado != 'N' AND tipo = 'C'";
								} */
							}
							$iter = "zebra1";
							$iter2 = "zebra2";
							$c=0;
							$totalcli=count($numerocuenta);
							for($xx=0;$xx<$totalcli;$xx++){
								$c++;
								$idtemporal++;
								$terceroSGR = '';
								if ($vigenciagasto[$xx] == 7){
									$numvigengasto = 2;
								}else{
									$numvigengasto = $vigenciagasto[$xx];
								}
								if($_POST['tipoentidad'] == 'RS' ){

									$sqlaesgpriet = "SELECT aesgpriet FROM pptoseccion_presupuestal WHERE estado = 'S' AND id_seccion_presupuestal = '$secpresupuestal[$xx]'";
									$resaesgpriet = mysqli_query($linkmulti,$sqlaesgpriet);
									$rowaesgpriet = mysqli_fetch_row($resaesgpriet);
									$secpresupuestalfinal = $rowaesgpriet[0];

								}elseif($_POST['tipoentidad'] == 'RG' ){

									$sqlrTipoRecursoSGR = "SELECT cuentasgr FROM rubros_cuentassgr WHERE bpim = '$bpin[$xx]' AND sector = '$sector[$xx]' AND fuente = '$numerocuentaauxFuente[$xx]' AND programatico LIKE '$programatico[$xx]%' AND opcion = '2'";
									$resTipoRecursoSGR = mysqli_query($linkmulti, $sqlrTipoRecursoSGR);
									$rowTipoRecursoSGR = mysqli_fetch_row($resTipoRecursoSGR);
									$secpresupuestalfinal = $rowTipoRecursoSGR[0];

									$sqlrTerceroSGR = "SELECT cuentasgr FROM rubros_cuentassgr WHERE bpim = '$bpin[$xx]' AND sector = '$sector[$xx]' AND fuente = '$numerocuentaauxFuente[$xx]' AND programatico LIKE '$programatico[$xx]%' AND opcion = '3'";
									$resTerceroSGR = mysqli_query($linkmulti, $sqlrTerceroSGR);
									$rowTerceroSGR = mysqli_fetch_row($resTerceroSGR);
									$terceroSGR = $rowTerceroSGR[0];


								}else{

									$secpresupuestalfinal = $secpresupuestal[$xx];

								}


								$sqltabla2 = "INSERT INTO ".$_SESSION['tablatemporal']." (id, codigocuenta, nombrecuenta, seccpresupuestal, vigenciagastos, sector, programatico, bpin, apropiacionini, apropiaciondef, registro, codigocuentaaux, aesgpriet, sectorial) VALUES ('$idtemporal', '$numerocuenta[$xx]', '$nombrecuenta[$xx]', '$secpresupuestalfinal', '$numvigengasto', '$sector[$xx]', '$programatico[$xx]', '$bpin[$xx]', '$apropiaini[$xx]', '$apropiadef[$xx]','$registroinfo[$xx]','$numerocuentaaux[$xx]', '$terceroSGR', '$sectorial[$xx]')";
								mysqli_query($linkbd,$sqltabla2);
								$porcentaje = $c * 100 / $totalcli;
								echo"
								<script>
									progres='".round($porcentaje)."';callprogress(progres);
									document.getElementById('totalcuentas').innerHTML='Procesando Información $c / $totalcli';
								</script>";
								flush();
								ob_flush();
								usleep(1);
							}
							$c=0;
							if($_POST['tipoentidad'] == 'RS' ){

								$sqlver="SELECT id, codigocuenta, nombrecuenta, seccpresupuestal, vigenciagastos, sector, programatico, bpin, SUM(apropiacionini), SUM(apropiaciondef) FROM ".$_SESSION['tablatemporal']." GROUP BY codigocuenta, seccpresupuestal, programatico, vigenciagastos ORDER BY seccpresupuestal, vigenciagastos, seccpresupuestal ASC";

							}elseif($_POST['tipoentidad'] == 'RG' ){

								$sqlver = "SELECT id, codigocuenta, nombrecuenta, seccpresupuestal, vigenciagastos, sector, programatico, bpin, SUM(apropiacionini), SUM(apropiaciondef), codigocuentaaux, aesgpriet FROM ".$_SESSION['tablatemporal']." GROUP BY codigocuenta, seccpresupuestal, programatico, vigenciagastos, bpin, codigocuentaaux ORDER BY seccpresupuestal, vigenciagastos, seccpresupuestal, aesgpriet ASC";

							}else{

								$sqlver="SELECT id, codigocuenta, nombrecuenta, seccpresupuestal, vigenciagastos, sector, programatico, bpin, SUM(apropiacionini), SUM(apropiaciondef), sectorial FROM ".$_SESSION['tablatemporal']." GROUP BY codigocuenta, seccpresupuestal, programatico,bpin, vigenciagastos, sectorial ORDER BY seccpresupuestal,vigenciagastos,id ASC";

							}
							$resver = mysqli_query($linkbd,$sqlver);
							$totalcli=mysqli_num_rows($resver);
							while ($rowver = mysqli_fetch_row($resver)){
								$c++;
								$porcentaje = $c * 100 / $totalcli;
								if($_POST['tipoentidad'] == 'RS' ){
									echo "
									<tr class=$iter>
										<td class='titulos2'>
											<a onClick=\"detallec('$c','$rowver[1]','$rowver[4]','$rowver[3]','$rowver[6]','$rowver[7]')\" style='cursor:pointer;'>
											<img id='img$c' src='imagenes/plus.gif'>
											</a>
										</td>
										<td style='width:10%;'>$rowver[1]</td>
										<td>$rowver[2]</td>
										<td>$rowver[3]</td>
										<td>$rowver[4]</td>
										<td>$rowver[6]</td>
										<td style='text-align:right;'>$".number_format($rowver[8],0,',','.')."</td>
										<td style='text-align:right;'>$".number_format($rowver[9],0,',','.')."</td>
									</tr>
									<tr>
										<td align='center'></td>
											<td colspan='16'>
												<div id='detalle$c' style='display:none'></div>
											</td>
										</tr>
									<script>
										progres='".round($porcentaje)."';callprogress(progres);
										document.getElementById('totalcuentas').innerHTML='Visualizar Información $c / $totalcli';
									</script>";
								}elseif($_POST['tipoentidad'] == 'RG' ){

									/* TIPO RECURSO	PROGRAMATICO MGA	BPIN	TERCEROS	POLITICA PUBLICA	SITUACION DE FONDOS	APROPIACIÓN INICIAL	APROPIACIÓN DEFINITIVA */

									echo "
									<tr class=$iter>
										<td class='titulos2'>
											<a onClick=\"detallec('$c','$rowver[1]','$rowver[4]','$rowver[3]','$rowver[6]','$rowver[7]')\" style='cursor:pointer;'>
											<img id='img$c' src='imagenes/plus.gif'>
											</a>
										</td>
										<td style='width:10%;'>$rowver[1]</td>
										<td>$rowver[5]</td>
										<td>$rowver[10]</td>
										<td>$rowver[3]</td>
										<td>$rowver[6]</td>
										<td>$rowver[7]</td>
										<td>$rowver[11]</td>
										<td>0</td>
										<td>$rowver[4]</td>
										<td style='text-align:right;'>$".number_format($rowver[8],0,',','.')."</td>
										<td style='text-align:right;'>$".number_format($rowver[9],0,',','.')."</td>
									</tr>
									<tr>
										<td align='center'></td>
											<td colspan='16'>
												<div id='detalle$c' style='display:none'></div>
											</td>
										</tr>
									<script>
										progres='".round($porcentaje)."';callprogress(progres);
										document.getElementById('totalcuentas').innerHTML='Visualizar Información $c / $totalcli';
									</script>";

								}else{
									echo "
									<tr class=$iter>
										<td class='titulos2'>
											<a onClick=\"detallec('$c','$rowver[1]','$rowver[4]','$rowver[3]','$rowver[6]','$rowver[7]')\" style='cursor:pointer;'>
											<img id='img$c' src='imagenes/plus.gif'>
											</a>
										</td>
										<td style='width:10%;'>$rowver[1]</td>
										<td>$rowver[2]</td>
										<td>$rowver[4]</td>
										<td>$rowver[3]</td>
										<td>$rowver[6]</td>
										<td>$rowver[10]</td>
										<td>$rowver[7]</td>
										<td style='text-align:right;'>$".number_format($rowver[8],0,',','.')."</td>
										<td style='text-align:right;'>$".number_format($rowver[9],0,',','.')."</td>
									</tr>
									<tr>
										<td align='center'></td>
											<td colspan='16'>
												<div id='detalle$c' style='display:none'></div>
											</td>
										</tr>
									<script>
										progres='".round($porcentaje)."';callprogress(progres);
										document.getElementById('totalcuentas').innerHTML='Visualizar Información $c / $totalcli';
									</script>";
								}
								flush();
								ob_flush();
								usleep(1);
								$aux=$iter;
								$iter=$iter2;
								$iter2=$aux;
								if(($rowver[8] == 0) && ($rowver[9] == 0)){}
								else{
									if($_POST['tipoentidad'] == 'RS' ){


										fputs($Descriptor1,"D;".$rowver[1].";".$rowver[3].";".$rowver[4].";".$rowver[6].";".$rowver[7].";".$rowver[8].";".$rowver[9]."\r\n");

									}elseif($_POST['tipoentidad'] == 'RG' ){

										fputs($Descriptor1,"D;".$rowver[1].";".$rowver[5].";".$rowver[10].";".$rowver[3].";".$rowver[6].";".$rowver[7].";".$rowver[11].";0;".$rowver[4].";".$rowver[8].";".$rowver[9]."\r\n");

									}else{

										fputs($Descriptor1,"D;".$rowver[1].";".$rowver[4].";".$rowver[3].";".$rowver[6].";".$rowver[10].";".$rowver[7].";".$rowver[8].";".$rowver[9]."\r\n");

									}

									fputs($Descriptor2,"D\t".$rowver[1]."\t".$rowver[4]."\t".$rowver[3]."\t".$rowver[6]."\t".$rowver[10]."\t".$rowver[7]."\t".$rowver[8]."\t".$rowver[9]."\r\n");
									fputs($Descriptor3,"D\t".$rowver[1]."\t".$rowver[4]."\t".$rowver[3]."\t".$rowver[6]."\t".$rowver[8]."\t".$rowver[9]."\r\n");
									fputs($Descriptor4,"D\t".$rowver[1]."\t".$rowver[3]."\t".$rowver[4]."\t".$rowver[6]."\t".$rowver[8]."\t".$rowver[9]."\r\n");
									fputs($Descriptor5,"D\t".$rowver[1]."\t".intVal($rowver[5])."\t".$rowver[10]."\t".$rowver[3]."\t".$rowver[6]."\t".$rowver[7]."\t".$rowver[11]."\t0\t".$rowver[4]."\t".$rowver[8]."\t".$rowver[9]."\r\n");
								}
							}
							echo "
								</table>
							";
							fclose($Descriptor1);
							fclose($Descriptor2);
							fclose($Descriptor3);
							fclose($Descriptor4);
							fclose($Descriptor5);
							echo "
							<script>document.getElementById('progreso').style.display='none';</script>";
						}break;
						case 4: //EJECUCION DE GASTOS - RESERVAS
						{
							unset($detsectorial);
							$numerocuenta = $nombrecuenta = $secpresupuestal = $vigenciagasto = $programatico = $varbpin = $varcpc = $varfuentes = $situaciondondos = $politicapublica = $terceroschip = $compromisos = $obligaciones = $pagos = $tablasinfo = $varfuentes2 = $varcpc2 = $programatico2 = $numero_rp = '';
							$numid = $c = $idtemporal = 0;
							$namearch = "archivos/".$_SESSION['usuario']."informecgr".$fec.".csv";
							$Descriptor1 = fopen($namearch, "w+");
							$namearch2 = "archivos/".$informes[$_POST['reporte']].".txt";
							$Descriptor2 = fopen($namearch2, "w+");
							$namearch3 = "archivos/".$informes[$_POST['reporte']]."b.txt";
							$Descriptor3 = fopen($namearch3, "w+");
							$namearch4 = "archivos/".$informes[$_POST['reporte']]."_AESGPRI.txt";
							$Descriptor4 = fopen($namearch4, "w+");
							$namearch5 = "archivos/".$informes[$_POST['reporte']]."c.txt";
							$Descriptor5 = fopen($namearch5, "w+");
							if($_POST['tipoentidad'] == 'RS' ){
								echo "
									<table class='inicio' align='center'>
										<tr>
											<td class='titulos' colspan='16'><p class='neon_titulos' id='titulogento'></p></td>
										</tr>
										<tr>
											<td colspan='3' id='totalcuentas'></td>
											<td colspan='3'>
												<div id='progreso' class='ProgressBar' style='display:none; float:left'>
													<div class='ProgressBarText'><span id='getprogress'></span>&nbsp;% </div>
													<div id='getProgressBarFill'></div>
												</div>
											</td>
											<td colspan='4'>
												<div id='progreso2' class='ProgressBar2' style='display:none; float:left'>
													<div class='ProgressBarText2'><span id='getprogress2'></span>&nbsp;% </div>
													<div id='getProgressBarFill2'></div>
												</div>
											</td>
											<td colspan='5' id='titulosubproceso'></td>
										</tr>
										<tr>
											<td class='titulos2' style='width:2px;'><img src='imagenes/plus.gif'></td>
											<td class='titulos2' colspan='2'>&Aacute;RBOL DE CONCEPTOS</td>
											<td class='titulos2'>AESGPRIET</td>
											<td class='titulos2'>VIGENCIA DEL GASTO</td>
											<td class='titulos2'>CPC</td>
											<td class='titulos2'>PROGRAMATICO</td>



											<td class='titulos2'>COMPROMISOS</td>
											<td class='titulos2'>OBLIGACIONES</td>
											<td class='titulos2'>PAGOS</td>
										</tr>";
							}elseif($_POST['tipoentidad'] == 'RG' ){
								echo "
									<table class='inicio' align='center'>
										<tr>
											<td class='titulos' colspan='16'><p class='neon_titulos' id='titulogento'></p></td>
										</tr>
										<tr>
											<td colspan='3' id='totalcuentas'></td>
											<td colspan='3'>
												<div id='progreso' class='ProgressBar' style='display:none; float:left'>
													<div class='ProgressBarText'><span id='getprogress'></span>&nbsp;% </div>
													<div id='getProgressBarFill'></div>
												</div>
											</td>
											<td colspan='4'>
												<div id='progreso2' class='ProgressBar2' style='display:none; float:left'>
													<div class='ProgressBarText2'><span id='getprogress2'></span>&nbsp;% </div>
													<div id='getProgressBarFill2'></div>
												</div>
											</td>
											<td colspan='5' id='titulosubproceso'></td>
										</tr>
										<tr>
											<td class='titulos2' style='width:2px;'><img src='imagenes/plus.gif'></td>
											<td class='titulos2' colspan='2'>&Aacute;RBOL DE CONCEPTOS</td>
											<td class='titulos2'>CPC</td>
											<td class='titulos2'>SECTOR</td>
											<td class='titulos2'>FUENTE DE FINANCIAION</td>
											<td class='titulos2'>TIPO DE RECURSO</td>
											<td class='titulos2'>PROGRAMATICO MGA </td>
											<td class='titulos2'>BPIN</td>
											<td class='titulos2'>TERCEROS CHIP</td>
											<td class='titulos2'>POLITICA PUBLICA</td>
											<td class='titulos2'>SITUACION DE FONDOS</td>
											<td class='titulos2'>COMPROMISOS</td>
											<td class='titulos2'>OBLIGACIONES</td>
											<td class='titulos2'>PAGOS</td>
										</tr>";
							}else{
								echo "
									<table class='inicio' align='center'>
										<tr>
											<td class='titulos' colspan='16'><p class='neon_titulos' id='titulogento'></p></td>
										</tr>
										<tr>
											<td colspan='3' id='totalcuentas'></td>
											<td colspan='3'>
												<div id='progreso' class='ProgressBar' style='display:none; float:left'>
													<div class='ProgressBarText'><span id='getprogress'></span>&nbsp;% </div>
													<div id='getProgressBarFill'></div>
												</div>
											</td>
											<td colspan='4'>
												<div id='progreso2' class='ProgressBar2' style='display:none; float:left'>
													<div class='ProgressBarText2'><span id='getprogress2'></span>&nbsp;% </div>
													<div id='getProgressBarFill2'></div>
												</div>
											</td>
											<td colspan='5' id='titulosubproceso'></td>
										</tr>
										<tr>
											<td class='titulos2' style='width:2px;'><img src='imagenes/plus.gif'></td>
											<td class='titulos2' colspan='2'>&Aacute;RBOL DE CONCEPTOS</td>
											<td class='titulos2'>DETALLE SECTORIAL</td>
											<td class='titulos2'>VIGENCIA DEL GASTO</td>
											<td class='titulos2'>SECCIÓN PRESUPUESTAL</td>
											<td class='titulos2'>PROGRAMATICO MGA </td>
											<td class='titulos2'>CPC</td>
											<td class='titulos2'>FUENTES DE FINANCIACI&Oacute;N</td>
											<td class='titulos2'>BPIN</td>
											<td class='titulos2'>SITUACION DE FONDOS</td>
											<td class='titulos2'>POLITICA PUBLICA</td>
											<td class='titulos2'>TERCEROS CHIP</td>
											<td class='titulos2'>COMPROMISOS</td>
											<td class='titulos2'>OBLIGACIONES</td>
											<td class='titulos2'>PAGOS</td>
										</tr>";
							}

							$mes2 = substr($_POST['periodo'], 3, 2);
							$_POST['fecha'] = "01/01/$vigusu";
							$_POST['fecha2'] = intval(date("t", $mes2))."/$mes2/$vigusu";
							preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST['fecha'],$fecha);
							$fechai = "$fecha[3]-$fecha[2]-$fecha[1]";
							preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST['fecha2'],$fecha);
							$fechaf = "$fecha[3]-$fecha[2]-$fecha[1]";

							fputs($Descriptor1, "S;".$_POST['codent'].";".$_POST['periodo'].";".$vigusu.";D_EJECUCION_DE_GASTOS\r\n");
							fputs($Descriptor1, "S;ARBOL DE CONCEPTOS;VIGENCIA DEL GASTO;SECCION PRESUPUESTAL;PROGRAMATICO MGA;CPC;SECTORIAL;FUENTES DE FINANCIACIÓN;BPIN;SITUACION DE FONDOS;POLITICA PUBLICA;TERCEROS CHIP;COMPROMISOS; OBLIGACIONES;PAGOS\r\n");

							if($_POST['tipoentidad'] == 'RS' ){

							}else if($_POST['tipoentidad'] == 'RG' ){

							}else{

							}
							$fecha_actual = date("d-m-Y");
							fputs($Descriptor2, "S\t".$_POST['codent']."\t".$_POST['periodo']."\t".$vigusu. "\tD_EJECUCION_DE_GASTOS\r\n");
							fputs($Descriptor3, "S\t".$_POST['codent']."\t".$_POST['periodo']."\t".$vigusu. "\tD_EJECUCION_DE_GASTOS\r\n");
							fputs($Descriptor4, "S\t".$_POST['codent']."\t".$_POST['periodo']."\t".$vigusu. "\tD_EJECUCION_DE_GASTOS_AESGPRI\r\n");
							fputs($Descriptor5, "S\t".$_POST['codent']."\t".$_POST['periodo']."\t".$vigusu. "\tEJECUCION_DE_GASTOS"."\t".$fecha_actual."\r\n");

							if($_SESSION['tablatemporal'] != '' && $_SESSION['tablatemporal'] != null){
								$sqlr = "DROP TABLE ".$_SESSION['tablatemporal'];
								mysqli_query($linkbd,$sqlr);
								$sqlr = "CREATE TABLE ".$_SESSION['tablatemporal']." (id int(11), codigocuenta varchar(100), nombrecuenta varchar(200), seccpresupuestal varchar(10), vigenciagastos varchar(10), programatico varchar(50), bpin varchar(20), cpc varchar(100), fuente varchar(20), situacionfondo varchar(1), politicapublica varchar(10), tercerochip varchar(10), compromisos double, obligaciones double, pagos double, tipotabla varchar(100), fuente2 varchar(20), cpc2 varchar(100), programatico2 varchar(50), numrp varchar(10), codsectorial varchar(10)) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;";
								mysqli_query($linkbd,$sqlr);
							}
							$sqlredglobal ="SELECT codigo, base, nombre, tipo, usuario FROM redglobal WHERE cuipo ='S' ORDER BY id ASC";
							$resredglobal = mysqli_query($linkbd,$sqlredglobal);
							while ($rowredglobal = mysqli_fetch_row($resredglobal)){
								$c = $numid = 0;
								echo"
								<script>document.getElementById('titulogento').innerHTML='.: C. EJECUCI&Oacute;N DE GASTOS: $rowredglobal[2]';</script>";
								$linkmulti = conectar_Multi($rowredglobal[1],$rowredglobal[4]);
								$linkmulti -> set_charset("utf8");
								$maxVersion = ultimaVersionGastosCCPETmulti($rowredglobal[1],$rowredglobal[4]);
								$sqlcta ="SELECT codigo,nombre FROM cuentasccpet WHERE version = '$maxVersion' AND municipio=1 AND tipo = 'C' ORDER BY id ASC";
								$rescta = mysqli_query($linkmulti,$sqlcta);
								$totalcli=mysqli_num_rows($rescta);
								while ($rowcta = mysqli_fetch_row($rescta)){
									$numid++;
									$c++;
									$sqlsector = "SELECT detalle_sectorial FROM ccpecuentasdsectorial WHERE cuenta = '$rowcta[0]'";
									$ressector = mysqli_query($linkbd,$sqlsector);
									$rowsector = mysqli_fetch_row($ressector);
									if($rowsector[0] == ''){
										$detsectorial = 0;
									}else{
										$detsectorial= $rowsector[0];
									}
									//COMPROMISOS (RP)
									{
										$cx = 0;
										$sqlrp = "
										SELECT T1.consvigencia, T1.productoservicio, T1.fuente, T1.valor, T1.medio_pago, T1.codigo_politicap, T1.bpim, T1.indicador_producto, T1.tipo_mov, T1.codigo_vigenciag, T1.seccion_presupuestal, T1.chip
										FROM ccpetrp_detalle AS T1
										INNER JOIN ccpetrp AS T2
										ON T1.consvigencia = T2.consvigencia AND T1.vigencia = T2.vigencia AND T1.tipo_mov = T2.tipo_mov AND T1.cuenta = '$rowcta[0]'
										WHERE T1.vigencia = '$vigusu' AND NOT(T2.estado = 'N') AND T2.fecha BETWEEN '$fechai' AND '$fechaf' AND T1.codigo_vigenciag <> 7
										ORDER BY consvigencia";
										$resrp = mysqli_query($linkmulti, $sqlrp);
										$totalcx = mysqli_num_rows($resrp);
										while($rowrp = mysqli_fetch_row($resrp)){

											$sectorSgr = '';
											$programaticoSgr = '';
											$cx++;
											$numerocuenta = $rowcta[0];
											$nombrecuenta = $rowcta[1];
											if($rowrp[10] != ''){
												$uniejecuaux = $rowrp[10];
											}
											else{
												$uniejecuaux = $rowredglobal[0];
											}
											if($_POST['tipoentidad'] == 'RS' ){
												$sqlaesgpriet = "SELECT aesgpriet FROM pptoseccion_presupuestal WHERE estado = 'S' AND id_seccion_presupuestal = '$rowrp[10]'";
												$resaesgpriet = mysqli_query($linkmulti,$sqlaesgpriet);
												$rowaesgpriet = mysqli_fetch_row($resaesgpriet);
												$secpresupuestal = $rowaesgpriet[0];
											}elseif($_POST['tipoentidad'] == 'RG' ){
											}else{
												$sqlunieje = "SELECT id_unidad_ejecutora FROM pptoseccion_presupuestal WHERE estado = 'S' AND id_seccion_presupuestal = '$uniejecuaux'";
												$resunieje = mysqli_query($linkmulti,$sqlunieje);
												$rowunieje = mysqli_fetch_row($resunieje);
												$secpresupuestal = $rowunieje[0];
											}

											if($rowrp[9] == ''){
												$vigenciagasto = 1;
											}
											else{
												if ($rowrp[9] == 7){
													$vigenciagasto = 2;
												}else{
													$vigenciagasto = $rowrp[9];
												}
											}
											if($rowrp[7] != '' && $rowrp[7] != null && $rowrp[7] != 0){
												if($_POST['tipoentidad'] == 'RS' ){
													$programatico2 = substr($rowrp[7],0,8);
													$auxprogramatico = substr($rowrp[7],0,8);
												}elseif($_POST['tipoentidad'] == 'RG' ){

													$sectorSgr = substr($rowrp[7],0,2);
													$programaticoSgr = substr($rowrp[7],0,7);
													$sqlrTipoRecurso = "SELECT cuentasgr FROM rubros_cuentassgr WHERE sector = '$sectorSgr' AND fuente = '$rowrp[2]' AND programatico = '$programaticoSgr' AND bpim = '$rowrp[6]' AND opcion = '2'";
													$resrTipoRecurso = mysqli_query($linkmulti,$sqlrTipoRecurso);
													$rowrTipoRecurso = mysqli_fetch_row($resrTipoRecurso);

													$sqlrFuente = "SELECT cuentasgr FROM rubros_cuentassgr WHERE sector = '$sectorSgr' AND fuente = '$rowrp[2]' AND programatico = '$programaticoSgr' AND bpim = '$rowrp[6]' AND opcion = '1'";
													$resrFuente = mysqli_query($linkmulti,$sqlrFuente);
													$rowrFuente = mysqli_fetch_row($resrFuente);

													$sqlrTercero = "SELECT cuentasgr FROM rubros_cuentassgr WHERE sector = '$sectorSgr' AND fuente = '$rowrp[2]' AND programatico = '$programaticoSgr' AND bpim = '$rowrp[6]' AND opcion = '3'";
													$resrTercero = mysqli_query($linkmulti,$sqlrTercero);
													$rowrTercero = mysqli_fetch_row($resrTercero);

												}else{
													$programatico2 = substr($rowrp[7],0,7);
													$auxprogramatico = substr($rowrp[7],0,7);
												}
												if ($auxprogramatico != ''){
													$programatico = $auxprogramatico;
												}
												else{
													$programatico = 'NO APLICA';
												}

												$sectorSec = substr($rowrp[7],0,2);

												$sqlrDetSectorial = "SELECT cuentasgr FROM ccpetdetallesectorial WHERE sector = '$sectorSec' AND fuente = '$rowrp[2]' AND bpim = '$rowrp[6]' AND programatico = '$programatico'";
												$resrDetSectorial = mysqli_query($linkmulti,$sqlrDetSectorial);
												$rowrDetSectorial = mysqli_fetch_row($resrDetSectorial);

												if($rowrDetSectorial[0] == ''){
													$detsectorial = 0;
												}else{
													$detsectorial= $rowrDetSectorial[0];
												}
											}
											else{
												$programatico = 'NO APLICA';
												$programatico2 = 'NO APLICA';
											}
											if($rowrp[6]!=''){
												$varbpin = $rowrp[6];
											}
											else{$varbpin = 0;}
											if($rowrp[1] != '' && $rowrp[1] != null && $rowrp[1] != 0){
												$varcpc2 = $rowrp[1];
												$auxcpc = $rowrp[1];
												if($auxcpc != ''){
													$varcpc = $auxcpc;
												}
												else{
													$varcpc = 'NO APLICA';
												}
											}
											else{
												$varcpc = 'NO APLICA';
												$varcpc2 = 'NO APLICA';
											}
											if($rowrp[2] != '' && $rowrp[2] != null && $rowrp[2] != 0){
												$varfuentes2 = $rowrp[2];
												$auxfuente = $rowrp[2];
												if($auxfuente != '' && $auxfuente != null){
													$varfuentes = $auxfuente;
												}
												else{
													$varfuentes = 'NO APLICA';
												}
											}
											else{
												$varfuentes = 'NO APLICA';
												$varfuentes2 = 'NO APLICA';
											}
											if($rowrp[4] == 'CSF'){
												$situaciondondos = 'C';
											}
											else{
												$situaciondondos = 'S';
											}
											if($rowrp[5]!=''){
												$politicapublica = $rowrp[5];
											}
											else{
												$politicapublica = 'NO APLICA';
											}
											if($rowrp[11] != '' && $rowrp[11] != 0){
												$terceroschip = $rowrp[11];
											}
											else{
												$terceroschip = 1;
											}
											if(substr($rowrp[8],0, 1) == '2'){
												$compromisos = round($rowrp[3], 2);
											}
											else{
												$compromisos = -1 * round($rowrp[3], 2);
											}
											$obligaciones = 0;
											$pagos = 0;
											$numero_rp = $rowrp[0];
											$tablasinfo = "RP No: $rowrp[0] (Mov: $rowrp[8])";
											$porcentaje2 = $cx * 100 / $totalcx;
											$idtemporal++;
											if($_POST['tipoentidad'] == 'RG' ){

												$sqltabla2 = "INSERT INTO ".$_SESSION['tablatemporal']." (id, codigocuenta, nombrecuenta, seccpresupuestal, vigenciagastos, programatico, bpin, cpc, fuente, situacionfondo, politicapublica, tercerochip, compromisos, obligaciones, pagos, tipotabla, fuente2, cpc2, programatico2, numrp, codsectorial) VALUES ('$idtemporal', '$numerocuenta', '$nombrecuenta', '".intVal($sectorSgr)."', '$rowrTipoRecurso[0]', '$programaticoSgr', '$varbpin', '$varcpc', '$rowrFuente[0]', '$situaciondondos', '$politicapublica', '$rowrTercero[0]', '$compromisos', '$obligaciones','$pagos', '$tablasinfo', '$varfuentes2', '$varcpc2', '$programatico2', '$numero_rp', '$detsectorial')";
												mysqli_query($linkbd,$sqltabla2);

											}else{
												$sqltabla2 = "INSERT INTO ".$_SESSION['tablatemporal']." (id, codigocuenta, nombrecuenta, seccpresupuestal, vigenciagastos, programatico, bpin, cpc, fuente, situacionfondo, politicapublica, tercerochip, compromisos, obligaciones, pagos, tipotabla, fuente2, cpc2, programatico2, numrp, codsectorial) VALUES ('$idtemporal', '$numerocuenta', '$nombrecuenta', '$secpresupuestal', '$vigenciagasto', '$programatico', '$varbpin', '$varcpc', '$varfuentes', '$situaciondondos', '$politicapublica', '$terceroschip', '$compromisos', '$obligaciones','$pagos', '$tablasinfo', '$varfuentes2', '$varcpc2', '$programatico2', '$numero_rp', '$detsectorial')";
												mysqli_query($linkbd,$sqltabla2);
											}

											echo"
											<script>
												progres2 = '".round($porcentaje2)."';
												callprogress2(progres2);
												document.getElementById('titulosubproceso').innerHTML = 'RP No: $rowrp[0] ($rowcta[0])';
											</script>";
											flush();
											ob_flush();
											usleep(1);
										}
										echo "
										<script>
											document.getElementById('progreso2').style.display='none';
											document.getElementById('titulosubproceso').innerHTML='';
										</script>";
									}
									//OBLIGACIONES CXP
									{
										$cx = 0;
										$sqlcxp = "
										SELECT T1.id_orden, T1.indicador_producto, T1.bpim, T1.productoservicio , T1.fuente, T1.medio_pago, T1.codigo_politicap, T1.tipo_mov, T1.valor, T2.id_rp, T1.codigo_vigenciag, T1.seccion_presupuestal, T1.chip
										FROM tesoordenpago_det AS T1
										INNER JOIN tesoordenpago AS T2
										ON T1.id_orden = T2.id_orden AND T1.tipo_mov = T2.tipo_mov AND T1.cuentap = '$rowcta[0]'
										WHERE T2.vigencia='$vigusu' AND NOT(T2.estado='N' OR T2.estado='R') AND T2.fecha BETWEEN '$fechai' AND '$fechaf' AND T1.codigo_vigenciag <> 7
										ORDER BY T1.id_orden";
										$rescxp = mysqli_query($linkmulti, $sqlcxp);
										$totalcx = mysqli_num_rows($rescxp);
										while($rowcxp = mysqli_fetch_row($rescxp)){

											$sectorSgr = '';
											$programaticoSgr = '';

											$cx++;
											$numerocuenta = $rowcta[0];
											$nombrecuenta = $rowcta[1];
											if($rowcxp[11] != ''){
												$uniejecuaux = $rowcxp[11];
											}
											else{
												$uniejecuaux = $rowredglobal[0];
											}
											if($_POST['tipoentidad'] == 'RS' ){
												$sqlaesgpriet = "SELECT aesgpriet FROM pptoseccion_presupuestal WHERE estado = 'S' AND id_seccion_presupuestal = '$rowcxp[11]'";
												$resaesgpriet = mysqli_query($linkmulti,$sqlaesgpriet);
												$rowaesgpriet = mysqli_fetch_row($resaesgpriet);
												$secpresupuestal = $rowaesgpriet[0];
											}elseif($_POST['tipoentidad'] == 'RG' ){

												$sectorSgr = substr($rowcxp[1],0,2);
												$programaticoSgr = substr($rowcxp[1],0,7);
												$sqlrTipoRecurso = "SELECT cuentasgr FROM rubros_cuentassgr WHERE sector = '$sectorSgr' AND fuente = '$rowcxp[4]' AND programatico = '$programaticoSgr' AND bpim = '$rowcxp[2]' AND opcion = '2'";
												$resrTipoRecurso = mysqli_query($linkmulti,$sqlrTipoRecurso);
												$rowrTipoRecurso = mysqli_fetch_row($resrTipoRecurso);

												$sqlrFuente = "SELECT cuentasgr FROM rubros_cuentassgr WHERE sector = '$sectorSgr' AND fuente = '$rowcxp[4]' AND programatico = '$programaticoSgr' AND bpim = '$rowcxp[2]' AND opcion = '1'";
												$resrFuente = mysqli_query($linkmulti,$sqlrFuente);
												$rowrFuente = mysqli_fetch_row($resrFuente);

												$sqlrTercero = "SELECT cuentasgr FROM rubros_cuentassgr WHERE sector = '$sectorSgr' AND fuente = '$rowcxp[4]' AND programatico = '$programaticoSgr' AND bpim = '$rowcxp[2]' AND opcion = '3'";
												$resrTercero = mysqli_query($linkmulti,$sqlrTercero);
												$rowrTercero = mysqli_fetch_row($resrTercero);

											}else{
												$sqlunieje = "SELECT id_unidad_ejecutora FROM pptoseccion_presupuestal WHERE estado = 'S' AND id_seccion_presupuestal = '$uniejecuaux'";
												$resunieje = mysqli_query($linkmulti,$sqlunieje);
												$rowunieje = mysqli_fetch_row($resunieje);
												$secpresupuestal = $rowunieje[0];
											}

											if($rowcxp[10] == ''){
												$vigenciagasto = 1;
											}
											else{
												if ($rowcxp[10] == 7){
													$vigenciagasto = 2;
												}else{
													$vigenciagasto = $rowcxp[10];
												}
											}
											if($rowcxp[1] != '' && $rowcxp[1] != null && $rowcxp[1] != 0){
												if($_POST['tipoentidad'] == 'RS' ){
													$programatico2 = substr($rowcxp[1],0,8);
													$auxprogramatico = substr($rowcxp[1],0,9);
												}elseif($_POST['tipoentidad'] == 'RG' ){
												}else{
													$programatico2 = substr($rowcxp[1],0,7);
													$auxprogramatico = substr($rowcxp[1],0,7);

													$sectorSec = substr($rowcxp[1],0,2);

													$sqlrDetSectorial = "SELECT cuentasgr FROM ccpetdetallesectorial WHERE sector = '$sectorSec' AND fuente = '$rowcxp[4]' AND bpim = '$rowcxp[2]' AND programatico = '$programatico2'";
													$resrDetSectorial = mysqli_query($linkmulti,$sqlrDetSectorial);
													$rowrDetSectorial = mysqli_fetch_row($resrDetSectorial);

													if($rowrDetSectorial[0] == ''){
														$detsectorial = 0;
													}else{
														$detsectorial= $rowrDetSectorial[0];
													}
												}
												if ($auxprogramatico != ''){
													$programatico = $auxprogramatico;
												}
												else{
													$programatico = 'NO APLICA';
												}
											}
											else{
												$programatico = 'NO APLICA';
												$programatico2 = 'NO APLICA';
											}
											if($rowcxp[2] != ''){
												$varbpin = $rowcxp[2];
											}
											else{
												$varbpin = 0;
											}
											if($rowcxp[3] != '' && $rowcxp[3] != null && $rowcxp[3] != 0){
												$varcpc2 = $rowcxp[3];
												$auxcpc = $rowcxp[3];
												if($auxcpc != ''){
													$varcpc = $auxcpc;
												}
												else{
													$varcpc = 'NO APLICA';
												}
											}
											else{
												$varcpc = 'NO APLICA';
												$varcpc2 = 'NO APLICA';
											}
											if($rowcxp[4] != '' && $rowcxp[4] != null && $rowcxp[4] != 0){
												$varfuentes2 = $rowcxp[4];
												$auxfuente = $rowcxp[4];
												if($auxfuente != '' && $auxfuente != null){
													$varfuentes = $auxfuente;
												}
												else{
													$varfuentes = 'NO APLICA';
												}
											}
											else{
												$varfuentes = 'NO APLICA';
												$varfuentes2 = 'NO APLICA';
											}
											if($rowcxp[5] == 'CSF'){
												$situaciondondos = 'C';
											}
											else{
												$situaciondondos = 'S';
											}
											if($rowcxp[6]!=''){
												$politicapublica = $rowcxp[6];
											}
											else{
												$politicapublica = 'NO APLICA';
											}
											if($rowcxp[12] != '' && $rowcxp[12] != 0){
												$terceroschip = $rowcxp[12];
											}
											else{
												$terceroschip = 1;
											}
											$compromisos = 0;
											if(substr($rowcxp[7],0, 1) == '2'){
												$obligaciones = round($rowcxp[8], 2);
											}
											else{
												$obligaciones = -1 * round($rowcxp[8], 2);
											}
											$pagos = 0;
											$numero_rp = $rowcxp[9];
											$tablasinfo = "CXP No: $rowcxp[0] (Mov: $rowcxp[7], RP: $rowcxp[9])";
											$porcentaje2 = $cx * 100 / $totalcx;

											$idtemporal++;
											if($_POST['tipoentidad'] == 'RG' ){

												$sqltabla2 = "INSERT INTO ".$_SESSION['tablatemporal']." (id, codigocuenta, nombrecuenta, seccpresupuestal, vigenciagastos, programatico, bpin, cpc, fuente, situacionfondo, politicapublica, tercerochip, compromisos, obligaciones, pagos, tipotabla, fuente2, cpc2, programatico2, numrp, codsectorial) VALUES ('$idtemporal', '$numerocuenta', '$nombrecuenta', '".intVal($sectorSgr)."', '$rowrTipoRecurso[0]', '$programaticoSgr', '$varbpin', '$varcpc', '$rowrFuente[0]', '$situaciondondos', '$politicapublica', '$rowrTercero[0]', '$compromisos', '$obligaciones','$pagos', '$tablasinfo', '$varfuentes2', '$varcpc2', '$programatico2', '$numero_rp', '$detsectorial')";
												mysqli_query($linkbd,$sqltabla2);

											}else{

												$sqltabla2 = "INSERT INTO ".$_SESSION['tablatemporal']." (id, codigocuenta, nombrecuenta, seccpresupuestal, vigenciagastos, programatico, bpin, cpc, fuente, situacionfondo, politicapublica, tercerochip, compromisos, obligaciones, pagos, tipotabla, fuente2, cpc2, programatico2, numrp, codsectorial) VALUES ('$idtemporal', '$numerocuenta', '$nombrecuenta', '$secpresupuestal', '$vigenciagasto', '$programatico', '$varbpin', '$varcpc', '$varfuentes', '$situaciondondos', '$politicapublica', '$terceroschip', '$compromisos', '$obligaciones','$pagos', '$tablasinfo', '$varfuentes2', '$varcpc2', '$programatico2', '$numero_rp', '$detsectorial')";
												mysqli_query($linkbd,$sqltabla2);
											}

											echo"
											<script>
												progres2 = '".round($porcentaje2)."';
												callprogress2(progres2);
												document.getElementById('titulosubproceso').innerHTML = 'CXP No: $rowcxp[0] ($rowcta[0])';
											</script>";
											flush();
											ob_flush();
											usleep(1);
										}
										echo "
										<script>
											document.getElementById('progreso2').style.display='none';
											document.getElementById('titulosubproceso').innerHTML='';
										</script>";
									}
									//OBLIGACIONES NOMINA
									{
										$cx = 0;
										$sqlcxpnom = "
										SELECT T1.nomina, T2.indicador, T2.bpin, T2.producto, T2.fuente, T2.medio_pago, T2.valor, T1.rp, T2.seccion_presupuestal, T2.chip
										FROM hum_nom_cdp_rp AS T1
										INNER JOIN humnom_presupuestal AS T2 ON T1.nomina = T2.id_nom
										INNER JOIN humnomina_aprobado AS T3 ON T1.nomina = T3.id_nom
										WHERE T2.cuenta = '$rowcta[0]' AND T1.vigencia = '$vigusu' AND T3.estado != 'N' AND NOT(T1.estado='N' OR T1.estado='R') AND T2.estado = 'P' AND T3.fecha BETWEEN '$fechai' AND '$fechaf'
										ORDER BY T1.nomina";
										$rescxpnom = mysqli_query($linkmulti, $sqlcxpnom);
										$totalcx = mysqli_num_rows($rescxpnom);
										while($rowcxpnom = mysqli_fetch_row($rescxpnom)){
											$cx++;
											$numerocuenta = $rowcta[0];
											$nombrecuenta = $rowcta[1];
											if($rowcxpnom[8] != ''){
												$uniejecuaux = $rowcxpnom[8];
											}
											else{
												$uniejecuaux = $rowredglobal[0];
											}
											if($_POST['tipoentidad'] == 'RS' ){
												$sqlaesgpriet = "SELECT aesgpriet FROM pptoseccion_presupuestal WHERE estado = 'S' AND id_seccion_presupuestal = '$rowcxpnom[8]'";
												$resaesgpriet = mysqli_query($linkmulti,$sqlaesgpriet);
												$rowaesgpriet = mysqli_fetch_row($resaesgpriet);
												$secpresupuestal = $rowaesgpriet[0];
											}elseif($_POST['tipoentidad'] == 'RG' ){
											}else{
												$sqlunieje = "SELECT id_unidad_ejecutora FROM pptoseccion_presupuestal WHERE estado = 'S' AND id_seccion_presupuestal = '$uniejecuaux'";
												$resunieje = mysqli_query($linkmulti,$sqlunieje);
												$rowunieje = mysqli_fetch_row($resunieje);
												$secpresupuestal = $rowunieje[0];
											}
											$vigenciagasto = 1;
											if($rowcxpnom[1] != '' && $rowcxpnom[1] != null && $rowcxpnom[1] != 0){
												if($_POST['tipoentidad'] == 'RS' ){
													$programatico2 = substr($rowcxpnom[1],0,8);
													$auxprogramatico = substr($rowcxpnom[1],0,8);
												}elseif($_POST['tipoentidad'] == 'RG' ){
												}else{
													$programatico2 = substr($rowcxpnom[1],0,7);
													$auxprogramatico = substr($rowcxpnom[1],0,7);

													$sectorSec = substr($rowcxpnom[1],0,2);

													$sqlrDetSectorial = "SELECT cuentasgr FROM ccpetdetallesectorial WHERE sector = '$sectorSec' AND fuente = '$rowcxpnom[4]' AND bpim = '$rowcxpnom[2]' AND programatico = '$programatico2'";
													$resrDetSectorial = mysqli_query($linkmulti,$sqlrDetSectorial);
													$rowrDetSectorial = mysqli_fetch_row($resrDetSectorial);

													if($rowrDetSectorial[0] == ''){
														$detsectorial = 0;
													}else{
														$detsectorial= $rowrDetSectorial[0];
													}
												}
												if ($auxprogramatico != ''){$programatico = $auxprogramatico;}
												else {$programatico = 'NO APLICA';}
											}
											else{
												$programatico = 'NO APLICA';
												$programatico2 = 'NO APLICA';
											}
											if($rowcxpnom[2] != ''){$varbpin = $rowcxpnom[2];}
											else {$varbpin = 0;}
											if($rowcxpnom[3] != '' && $rowcxpnom[3] != null && $rowcxpnom[3] != 0){
												$varcpc2 = $rowcxpnom[3];
												$auxcpc = $rowcxpnom[3];
												if($auxcpc != ''){$varcpc = $auxcpc;}
												else {$varcpc = 'NO APLICA';}
											}
											else{
												$varcpc = 'NO APLICA';
												$varcpc2 = 'NO APLICA';
											}
											if($rowcxpnom[4] != '' && $rowcxpnom[4] != null && $rowcxpnom[4] != 0){
												$varfuentes2 = $rowcxpnom[4];
												$auxfuente = $rowcxpnom[4];
												if($auxfuente != '' && $auxfuente != null){$varfuentes = $auxfuente;}
												else {$varfuentes = 'NO APLICA';}
											}
											else{
												$varfuentes = 'NO APLICA';
												$varfuentes2 = 'NO APLICA';
											}
											if($rowcxpnom[5] == 'CSF'){$situaciondondos = 'C';}
											else {$situaciondondos = 'S';}
											$politicapublica = 'NO APLICA';
											if($rowcxpnom[9] != 'CSF' && $rowcxpnom[9] != 0){
												$terceroschip = $rowcxpnom[9];
											}
											else{
												$terceroschip = 1;
											}
											$compromisos = 0;
											$obligaciones = round($rowcxpnom[6], 2);
											$pagos = 0;
											$numero_rp = $rowcxpnom[7];
											$tablasinfo = "CXPN No: $rowcxpnom[0] (Mov: 201, RP: $rowcxpnom[7])";
											$porcentaje2 = $cx * 100 / $totalcx;

											$idtemporal++;


											$sqltabla2 = "INSERT INTO ".$_SESSION['tablatemporal']." (id, codigocuenta, nombrecuenta, seccpresupuestal, vigenciagastos, programatico, bpin, cpc, fuente, situacionfondo, politicapublica, tercerochip, compromisos, obligaciones, pagos, tipotabla, fuente2, cpc2, programatico2, numrp, codsectorial) VALUES ('$idtemporal', '$numerocuenta', '$nombrecuenta', '$secpresupuestal', '$vigenciagasto', '$programatico', '$varbpin', '$varcpc', '$varfuentes', '$situaciondondos', '$politicapublica', '$terceroschip', '$compromisos', '$obligaciones','$pagos', '$tablasinfo', '$varfuentes2', '$varcpc2', '$programatico2', '$numero_rp', '$detsectorial')";
											mysqli_query($linkbd, $sqltabla2);



											echo"
											<script>
												progres2 = '".round($porcentaje2)."';
												callprogress2(progres2);
												document.getElementById('titulosubproceso').innerHTML = 'CXPN No: $rowcxpnom[0] ($rowcta[0])';
											</script>";
											flush();
											ob_flush();
											usleep(1);
										}
										echo "
										<script>
											document.getElementById('progreso2').style.display='none';
											document.getElementById('titulosubproceso').innerHTML='';
										</script>";
									}
									//PAGOS EGRESOS
									{
										$cx = 0;
										$sqlegreso ="
										SELECT T2.id_egreso, T1.id_orden, T1.indicador_producto, T1.bpim, T1.productoservicio, T1.fuente, T1.medio_pago, T1.codigo_politicap, T2.tipo_mov, T1.valor, T3.id_rp, T1.codigo_vigenciag, T1.seccion_presupuestal, T1.chip
										FROM tesoordenpago_det AS T1
										INNER JOIN tesoegresos AS T2 ON T1.id_orden = T2.id_orden
										INNER JOIN tesoordenpago AS T3 ON T1.id_orden = T3.id_orden
										WHERE T1.cuentap = '$rowcta[0]' AND T3.vigencia='$vigusu' AND NOT(T2.estado='N') AND NOT(T1.estado='R')AND T2.fecha BETWEEN '$fechai' AND '$fechaf' AND T1.codigo_vigenciag <> 7
										ORDER BY T1.id_orden";
										$resegreso = mysqli_query($linkmulti,$sqlegreso);
										$totalcx = mysqli_num_rows($resegreso);
										while($rowegreso = mysqli_fetch_row($resegreso)){
											$sectorSgr = '';
											$programaticoSgr = '';
											$cx++;
											$numerocuenta = $rowcta[0];
											$nombrecuenta = $rowcta[1];
											if($rowegreso[12] != ''){$uniejecuaux = $rowegreso[12];}
											else {$uniejecuaux = $rowredglobal[0];}
											if($_POST['tipoentidad'] == 'RS' ){
												$sqlaesgpriet = "SELECT aesgpriet FROM pptoseccion_presupuestal WHERE estado = 'S' AND id_seccion_presupuestal = '$rowegreso[12]'";
												$resaesgpriet = mysqli_query($linkmulti,$sqlaesgpriet);
												$rowaesgpriet = mysqli_fetch_row($resaesgpriet);
												$secpresupuestal = $rowaesgpriet[0];
											}elseif($_POST['tipoentidad'] == 'RG' ){

												$sectorSgr = substr($rowegreso[2],0,2);
												$programaticoSgr = substr($rowegreso[2],0,7);
												$sqlrTipoRecurso = "SELECT cuentasgr FROM rubros_cuentassgr WHERE sector = '$sectorSgr' AND fuente = '$rowegreso[5]' AND programatico = '$programaticoSgr' AND bpim = '$rowegreso[3]' AND opcion = '2'";
												$resrTipoRecurso = mysqli_query($linkmulti,$sqlrTipoRecurso);
												$rowrTipoRecurso = mysqli_fetch_row($resrTipoRecurso);

												$sqlrFuente = "SELECT cuentasgr FROM rubros_cuentassgr WHERE sector = '$sectorSgr' AND fuente = '$rowegreso[5]' AND programatico = '$programaticoSgr' AND bpim = '$rowegreso[3]' AND opcion = '1'";
												$resrFuente = mysqli_query($linkmulti,$sqlrFuente);
												$rowrFuente = mysqli_fetch_row($resrFuente);

												$sqlrTercero = "SELECT cuentasgr FROM rubros_cuentassgr WHERE sector = '$sectorSgr' AND fuente = '$rowegreso[5]' AND programatico = '$programaticoSgr' AND bpim = '$rowegreso[3]' AND opcion = '3'";
												$resrTercero = mysqli_query($linkmulti,$sqlrTercero);
												$rowrTercero = mysqli_fetch_row($resrTercero);

											}else{
												$sqlunieje = "SELECT id_unidad_ejecutora FROM pptoseccion_presupuestal WHERE estado = 'S' AND id_seccion_presupuestal = '$uniejecuaux'";
												$resunieje = mysqli_query($linkmulti,$sqlunieje);
												$rowunieje = mysqli_fetch_row($resunieje);
												$secpresupuestal = $rowunieje[0];
											}

											if($rowegreso[11] == ''){
												$vigenciagasto = 1;
											}else {
												if ($rowegreso[11] == 7){
													$vigenciagasto = 2;
												}else{
													$vigenciagasto = $rowegreso[11];
												}
											}
											if($rowegreso[2] != '' && $rowegreso[2] != null && $rowegreso[2] != 0){
												if($_POST['tipoentidad'] == 'RS' ){
													$programatico2 = substr($rowegreso[2],0,8);
													$auxprogramatico = substr($rowegreso[2],0,8);
												}elseif($_POST['tipoentidad'] == 'RG' ){
												}else{
													$programatico2 = substr($rowegreso[2],0,7);
													$auxprogramatico = substr($rowegreso[2],0,7);

													$sectorSec = substr($rowegreso[2],0,2);

													$sqlrDetSectorial = "SELECT cuentasgr FROM ccpetdetallesectorial WHERE sector = '$sectorSec' AND fuente = '$rowegreso[5]' AND bpim = '$rowegreso[3]' AND programatico = '$programatico2'";
													$resrDetSectorial = mysqli_query($linkmulti,$sqlrDetSectorial);
													$rowrDetSectorial = mysqli_fetch_row($resrDetSectorial);

													if($rowrDetSectorial[0] == ''){
														$detsectorial = 0;
													}else{
														$detsectorial= $rowrDetSectorial[0];
													}
												}
												if ($auxprogramatico != ''){$programatico = $auxprogramatico;}
												else {$programatico = 'NO APLICA';}
											}
											else{
												$programatico = 'NO APLICA';
												$programatico2 = 'NO APLICA';
											}
											if($rowegreso[3] != ''){$varbpin = $rowegreso[3];}
											else {$varbpin = 0;}
											if($rowegreso[4] != '' && $rowegreso[4] != null && $rowegreso[4] != 0){
												$varcpc2 = $rowegreso[4];
												$auxcpc = $rowegreso[4];
												if($auxcpc != ''){$varcpc = $auxcpc;}
												else {$varcpc = 'NO APLICA';}
											}
											else{
												$varcpc = 'NO APLICA';
												$varcpc2 = 'NO APLICA';
											}
											if($rowegreso[5] != '' && $rowegreso[5] != null && $rowegreso[5] != 0){
												$varfuentes2 = $rowegreso[5];
												$auxfuente = $rowegreso[5];
												if($auxfuente != '' && $auxfuente != null){$varfuentes = $auxfuente;}
												else {$varfuentes = 'NO APLICA';}
											}
											else{
												$varfuentes = 'NO APLICA';
												$varfuentes2 = 'NO APLICA';
											}
											if($rowegreso[6] == 'CSF'){$situaciondondos = 'C';}
											else {$situaciondondos = 'S';}
											if($rowegreso[7]!=''){$politicapublica = $rowegreso[7];}
											else {$politicapublica = 'NO APLICA';}
											if($rowegreso[13] != '' && $rowegreso[13] != 0){
												$terceroschip = $rowegreso[13];
											}
											else{
												$terceroschip = 1;
											}
											$compromisos = 0;
											$obligaciones = 0;
											if(substr($rowegreso[8],0, 1) == '2'){$pagos = round($rowegreso[9], 2);}
											else {$pagos = -1 * round($rowegreso[9], 2);}
											$numero_rp = $rowegreso[10];
											$tablasinfo = "Egreso No: $rowegreso[0] (Mov: $rowegreso[8], CXP: $rowegreso[1], RP: $rowegreso[10])";
											$porcentaje2 = $cx * 100 / $totalcx;

											$idtemporal++;
											if($_POST['tipoentidad'] == 'RG' ){

												$sqltabla2 = "INSERT INTO ".$_SESSION['tablatemporal']." (id, codigocuenta, nombrecuenta, seccpresupuestal, vigenciagastos, programatico, bpin, cpc, fuente, situacionfondo, politicapublica, tercerochip, compromisos, obligaciones, pagos, tipotabla, fuente2, cpc2, programatico2, numrp, codsectorial) VALUES ('$idtemporal', '$numerocuenta', '$nombrecuenta', '".intVal($sectorSgr)."', '$rowrTipoRecurso[0]', '$programaticoSgr', '$varbpin', '$varcpc', '$rowrFuente[0]', '$situaciondondos', '$politicapublica', '$rowrTercero[0]', '$compromisos', '$obligaciones','$pagos', '$tablasinfo', '$varfuentes2', '$varcpc2', '$programatico2', '$numero_rp', '$detsectorial')";
												mysqli_query($linkbd, $sqltabla2);

											}else{

												$sqltabla2 = "INSERT INTO ".$_SESSION['tablatemporal']." (id, codigocuenta, nombrecuenta, seccpresupuestal, vigenciagastos, programatico, bpin, cpc, fuente, situacionfondo, politicapublica, tercerochip, compromisos, obligaciones, pagos, tipotabla, fuente2, cpc2, programatico2, numrp, codsectorial) VALUES ('$idtemporal', '$numerocuenta', '$nombrecuenta', '$secpresupuestal', '$vigenciagasto', '$programatico', '$varbpin', '$varcpc', '$varfuentes', '$situaciondondos', '$politicapublica', '$terceroschip', '$compromisos', '$obligaciones','$pagos', '$tablasinfo', '$varfuentes2', '$varcpc2', '$programatico2', '$numero_rp', '$detsectorial')";
												mysqli_query($linkbd,$sqltabla2);

											}

											echo"
											<script>
												progres2 = '".round($porcentaje2)."';
												callprogress2(progres2);
												document.getElementById('titulosubproceso').innerHTML = 'Egreso No: $rowegreso[0] ($rowcta[0])';
											</script>";
											flush();
											ob_flush();
											usleep(1);
										}
										echo "
										<script>
											document.getElementById('progreso2').style.display='none';
											document.getElementById('titulosubproceso').innerHTML='';
										</script>";
									}
									//PAGOS EGRESOS NOMINA
									{
										$cx = 0;
										//if ($_POST['tipodepago'] == '2'){
											$sqlegresonom ="
											SELECT T1.id_egreso, T1.id_orden, T2.indicador_producto, T2.bpin, T2.productoservicio, T2.fuente, T2.medio_pago, T2.codigo_politicap, T2.tipo_mov, T2.valordevengado, T2.seccion_presupuestal
											FROM tesoegresosnomina AS T1
											INNER JOIN tesoegresosnomina_det AS T2
											ON T1.id_egreso = T2.id_egreso
											WHERE T2.cuentap = '$rowcta[0]' AND T1.vigencia = '$vigusu' AND NOT(T1.estado='N' OR T1.estado='R') AND NOT(T2.tipo='SE' OR T2.tipo='PE' OR T2.tipo='DS' OR T2.tipo='RE' OR T2.tipo='FS') AND T1.fecha BETWEEN '$fechai' AND '$fechaf'";
										/*}
										else{
											$sqlegresonom ="
											SELECT T1.id_egreso, T1.id_orden, T2.indicador_producto, T2.bpin, T2.productoservicio, T2.fuente, T2.medio_pago, T2.codigo_politicap, T2.tipo_mov, T2.valor, T2.seccion_presupuestal
											FROM tesoegresosnomina AS T1
											INNER JOIN tesoegresosnomina_det AS T2
											ON T1.id_egreso = T2.id_egreso
											WHERE T2.cuentap = '$rowcta[0]' AND T1.vigencia = '$vigusu' AND NOT(T1.estado='N' OR T1.estado='R') AND T1.fecha BETWEEN '$fechai' AND '$fechaf'";
										}*/
										$resegresonom = mysqli_query($linkmulti,$sqlegresonom);
										$totalcx = mysqli_num_rows($resegresonom);
										while($rowegresonom = mysqli_fetch_row($resegresonom)){
											$cx++;
											$numerocuenta = $rowcta[0];
											$nombrecuenta = $rowcta[1];
											$sectorSgr = '';
											if($rowegresonom[10] != ''){
												$uniejecuaux = $rowegresonom[10];
											}else {
												$uniejecuaux = $rowredglobal[0];
											}
											if($_POST['tipoentidad'] == 'RS' ){
												$sqlaesgpriet = "SELECT aesgpriet FROM pptoseccion_presupuestal WHERE estado = 'S' AND id_seccion_presupuestal = '$rowegresonom[10]'";
												$resaesgpriet = mysqli_query($linkmulti,$sqlaesgpriet);
												$rowaesgpriet = mysqli_fetch_row($resaesgpriet);
												$secpresupuestal = $rowaesgpriet[0];
											}elseif($_POST['tipoentidad'] == 'RG' ){

											}else{
												$sqlunieje = "SELECT id_unidad_ejecutora FROM pptoseccion_presupuestal WHERE estado = 'S' AND id_seccion_presupuestal = '$uniejecuaux'";
												$resunieje = mysqli_query($linkmulti,$sqlunieje);
												$rowunieje = mysqli_fetch_row($resunieje);
												$secpresupuestal = $rowunieje[0];
											}
											$vigenciagasto = 1;
											if($rowegresonom[2] != '' && $rowegresonom[2] != null && $rowegresonom[2] != 0){
												if($_POST['tipoentidad'] == 'RS' ){
													$programatico2 = substr($rowegresonom[2],0,8);
													$auxprogramatico = substr($rowegresonom[2],0,8);
												}elseif($_POST['tipoentidad'] == 'RG' ){
												}else{
													$programatico2 = substr($rowegresonom[2],0,7);
													$auxprogramatico = substr($rowegresonom[2],0,7);

													$sectorSec = substr($rowegresonom[2],0,2);

													$sqlrDetSectorial = "SELECT cuentasgr FROM ccpetdetallesectorial WHERE sector = '$sectorSec' AND fuente = '$rowegresonom[5]' AND bpim = '$rowegresonom[3]' AND programatico = '$programatico2'";
													$resrDetSectorial = mysqli_query($linkmulti,$sqlrDetSectorial);
													$rowrDetSectorial = mysqli_fetch_row($resrDetSectorial);

													if($rowrDetSectorial[0] == ''){
														$detsectorial = 0;
													}else{
														$detsectorial= $rowrDetSectorial[0];
													}
												}
												if ($auxprogramatico != ''){$programatico = $auxprogramatico;}
												else {$programatico = 'NO APLICA';}
											}
											else{
												$programatico = 'NO APLICA';
												$programatico2 = 'NO APLICA';
											}
											if($rowegresonom[3] != ''){$varbpin = $rowegresonom[3];}
											else {$varbpin = 0;}
											if($rowegresonom[4] != '' && $rowegresonom[4] != null && $rowegresonom[4] != 0){
												$varcpc2 = $rowegresonom[4];
												$auxcpc = $rowegresonom[4];
												if($auxcpc != ''){$varcpc = $auxcpc;}
												else {$varcpc = 'NO APLICA';}
											}
											else{
												$varcpc = 'NO APLICA';
												$varcpc2 = 'NO APLICA';
											}
											if($rowegresonom[5] != '' && $rowegresonom[5] != null && $rowegresonom[5] != 0){
												$varfuentes2 = $rowegresonom[5];
												$auxfuente = $rowegresonom[5];
												if($auxfuente != '' && $auxfuente != null){$varfuentes = $auxfuente;}
												else {$varfuentes = 'NO APLICA';}
											}
											else{
												$varfuentes = 'NO APLICA';
												$varfuentes2 = 'NO APLICA';
											}
											if($rowegresonom[6] == 'CSF'){$situaciondondos = 'C';}
											else {$situaciondondos = 'S';}
											if($rowegresonom[7]!=''){$politicapublica = $rowegresonom[7];}
											else {$politicapublica = 'NO APLICA';}
											$terceroschip = 1;
											$compromisos = 0;
											$obligaciones = 0;
											if(substr($rowegresonom[8],0, 1) == '2'){$pagos = round($rowegresonom[9], 2);}
											else {$pagos = -1 * round($rowegresonom[9], 2);}
											$sqlrpnom = "SELECT rp FROM hum_nom_cdp_rp WHERE nomina = '$rowegresonom[1]'";
											$resrpnom = mysqli_query($linkmulti, $sqlrpnom);
											$rowrpnom = mysqli_fetch_row($resrpnom);
											$numero_rp = $rowrpnom[0];
											$tablasinfo = "Egreso Nomina No: $rowegresonom[0] (Mov: $rowegresonom[8], CXPN: $rowegresonom[1], RP: $rowrpnom[0])";
											$porcentaje2 = $cx * 100 / $totalcx;

											$idtemporal++;
											$sqltabla2 = "INSERT INTO ".$_SESSION['tablatemporal']." (id, codigocuenta, nombrecuenta, seccpresupuestal, vigenciagastos, programatico, bpin, cpc, fuente, situacionfondo, politicapublica, tercerochip, compromisos, obligaciones, pagos, tipotabla, fuente2, cpc2, programatico2, numrp, codsectorial) VALUES ('$idtemporal', '$numerocuenta', '$nombrecuenta', '$secpresupuestal', '$vigenciagasto', '$programatico', '$varbpin', '$varcpc', '$varfuentes', '$situaciondondos', '$politicapublica', '$terceroschip', '$compromisos', '$obligaciones','$pagos', '$tablasinfo', '$varfuentes2', '$varcpc2', '$programatico2', '$numero_rp', '$detsectorial')";
											mysqli_query($linkbd,$sqltabla2);

											echo"
											<script>
												progres2 = '".round($porcentaje2)."';
												callprogress2(progres2);
												document.getElementById('titulosubproceso').innerHTML = 'Egreso Nomina No: $rowegresonom[0] ($rowcta[0])';
											</script>";
											flush();
											ob_flush();
											usleep(1);
										}
										echo "
										<script>
											document.getElementById('progreso2').style.display='none';
											document.getElementById('titulosubproceso').innerHTML='';
										</script>";
									}
									$porcentaje = $c * 100 / $totalcli;
									echo"
									<script>
										progres='".round($porcentaje)."';callprogress(progres);
										document.getElementById('totalcuentas').innerHTML='Recolectando Información: $numid / $totalcli';
									</script>";
									flush();
									ob_flush();
									usleep(5);
								}
							}
							echo "<script>document.getElementById('titulogento').innerHTML='.: C. EJECUCI&Oacute;N DE GASTOS';</script>";
							$iter = "zebra1";
							$iter2 = "zebra2";
							$c=0;
							if($_POST['tipoentidad'] == 'RS' ){
								$sqlver="
								SELECT id, codigocuenta, nombrecuenta, seccpresupuestal, vigenciagastos, programatico, bpin, cpc, fuente, situacionfondo, politicapublica, tercerochip, SUM(compromisos), SUM(obligaciones), SUM(pagos), fuente2, cpc2, programatico2, numrp, codsectorial
								FROM ".$_SESSION['tablatemporal']."
								GROUP BY codigocuenta, seccpresupuestal, programatico2, cpc2, vigenciagastos
								ORDER BY seccpresupuestal, vigenciagastos, id ASC";
							}elseif($_POST['tipoentidad'] == 'RG' ){

								$sqlver = "SELECT id, codigocuenta, nombrecuenta, cpc, seccpresupuestal, fuente, vigenciagastos, programatico, bpin, tercerochip, politicapublica, situacionfondo, SUM(compromisos), SUM(obligaciones), SUM(pagos) FROM ".$_SESSION['tablatemporal']."
								GROUP BY codigocuenta, seccpresupuestal, programatico, cpc, fuente, situacionfondo";

							}else{
								$sqlver="
								SELECT id, codigocuenta, nombrecuenta, seccpresupuestal, vigenciagastos, programatico, bpin, cpc, fuente, situacionfondo, politicapublica, tercerochip, SUM(compromisos), SUM(obligaciones), SUM(pagos), fuente2, cpc2, programatico2, numrp, codsectorial
								FROM ".$_SESSION['tablatemporal']."
								GROUP BY codigocuenta, seccpresupuestal, programatico2, cpc2, fuente2, bpin, vigenciagastos
								ORDER BY seccpresupuestal, vigenciagastos, id ASC";
							}
							$resver = mysqli_query($linkbd,$sqlver);
							$totalcli = mysqli_num_rows($resver);
							while ($rowver = mysqli_fetch_row($resver)){
								$c++;
								$porcentaje = $c * 100 / $totalcli;
								$colorlinea=fcolorlinea($rowver[12],$rowver[13],$rowver[14]);
								if($_POST['tipoentidad'] == 'RS' ){
									echo "
									<tr class=$iter $colorlinea>
										<td class='titulos2'>
											<a onClick=\"detalled('$c','$rowver[1]','$rowver[3]','$rowver[17]','$rowver[16]','$rowver[15]', '$rowver[6]','$rowver[4]','$rowver[18]')\" style='cursor:pointer;'>
												<img id='img$c' src='imagenes/plus.gif'>
											</a>
										</td>
										<td style='width:10%;'>$rowver[1]</td>
										<td>$rowver[2]</td>
										<td>$rowver[3]</td>
										<td>$rowver[4]</td>
										<td>$rowver[16]</td>
										<td>$rowver[17]</td>
										<td style='text-align:right;'>$".number_format(round($rowver[12]),0,',','.')."</td>
										<td style='text-align:right;'>$".number_format(round($rowver[13]),0,',','.')."</td>
										<td style='text-align:right;'>$".number_format(round($rowver[14]),0,',','.')."</td>
									</tr>
									<tr>
										<td align='center'></td>
										<td colspan='14'>
											<div id='detalle$c' style='display:none'></div>
										</td>
									</tr>
									<script>
										progres='".round($porcentaje)."';callprogress(progres);
										document.getElementById('totalcuentas').innerHTML='Visualizar Información $c / $totalcli';
									</script>";
								}elseif($_POST['tipoentidad'] == 'RG' ){

									echo "
									<tr class=$iter $colorlinea>
										<td class='titulos2'>

										</td>
										<td style='width:10%;'>$rowver[1]</td>
										<td>$rowver[2]</td>
										<td>$rowver[3]</td>
										<td>$rowver[4]</td>
										<td>$rowver[5]</td>
										<td>$rowver[6]</td>
										<td>$rowver[7]</td>
										<td>$rowver[8]</td>
										<td>$rowver[9]</td>
										<td>$rowver[10]</td>
										<td>$rowver[11]</td>
										<td style='text-align:right;'>$".number_format(round($rowver[12]),0,',','.')."</td>
										<td style='text-align:right;'>$".number_format(round($rowver[13]),0,',','.')."</td>
										<td style='text-align:right;'>$".number_format(round($rowver[14]),0,',','.')."</td>
									</tr>
									<tr>
										<td align='center'></td>
										<td colspan='14'>
											<div id='detalle$c' style='display:none'></div>
										</td>
									</tr>
									<script>
										progres='".round($porcentaje)."';callprogress(progres);
										document.getElementById('totalcuentas').innerHTML='Visualizar Información $c / $totalcli';
									</script>";

								}else{
									echo "
									<tr class=$iter $colorlinea>
										<td class='titulos2'>
											<a onClick=\"detalled('$c','$rowver[1]','$rowver[3]','$rowver[17]','$rowver[16]','$rowver[15]', '$rowver[6]','$rowver[4]','$rowver[18]')\" style='cursor:pointer;'>
												<img id='img$c' src='imagenes/plus.gif'>
											</a>
										</td>
										<td style='width:10%;'>$rowver[1]</td>
										<td>$rowver[2]</td>
										<td onDblClick=\"aux_dsectorial('$rowver[1]')\">$rowver[19]</td>
										<td>$rowver[4]</td>
										<td>$rowver[3]</td>
										<td>$rowver[17]</td>
										<td>$rowver[16]</td>
										<td>$rowver[15]</td>
										<td>$rowver[6]</td>
										<td>$rowver[9]</td>
										<td>$rowver[10]</td>
										<td>$rowver[11]</td>
										<td style='text-align:right;'>$".number_format(round($rowver[12]),0,',','.')."</td>
										<td style='text-align:right;'>$".number_format(round($rowver[13]),0,',','.')."</td>
										<td style='text-align:right;'>$".number_format(round($rowver[14]),0,',','.')."</td>
									</tr>
									<tr>
										<td align='center'></td>
										<td colspan='14'>
											<div id='detalle$c' style='display:none'></div>
										</td>
									</tr>
									<script>
										progres='".round($porcentaje)."';callprogress(progres);
										document.getElementById('totalcuentas').innerHTML='Visualizar Información $c / $totalcli';
									</script>";
								}
								flush();
								ob_flush();
								usleep(1);
								$aux=$iter;
								$iter=$iter2;
								$iter2=$aux;
								if(($rowver[12] == 0) && ($rowver[13] == 0) && ($rowver[14] == 0)){}
								else
								{
									if($rowver[5] == 'NO APLICA'){
										$valpromga = 0;
									}
									else{
										$valpromga = $rowver[5];
									}
									if($rowver[7] == 'NO APLICA'){
										$valcpcd=0;
									}
									else{
										$valcpcd = $rowver[7];
									}
									if($rowver[8] == 'NO APLICA'){
										$valfuented=0;
									}
									else{
										$valfuented = $rowver[8];
									}
									if($rowver[10] == 'NO APLICA'){
										$valppd = 0;
									}
									else{
										$valppd = $rowver[10];
									}

									if($_POST['tipoentidad'] == 'RS' ){


									}else if($_POST['tipoentidad'] == 'RG' ){

									}else{

									}

									if($rowver[3] == 'NO APLICA'){
										$valcpcSGR = 0;
									}else{
										$valcpcSGR = $rowver[3];
									}

									if($rowver[10] == 'NO APLICA'){
										$politicaPublicaSGR = 0;
									}else{
										$politicaPublicaSGR = $rowver[10];
									}
									fputs($Descriptor1, "D;".$rowver[1].";".$rowver[4].";".$rowver[3].";".$valpromga.";".$valcpcd.";".$rowver[19].";".$valfuented.";".$rowver[6].";".$rowver[9].";".$valppd.";".$rowver[11].";".round($rowver[12]).";".round($rowver[13]).";".round($rowver[14])."\r\n");
									fputs($Descriptor2, "D\t".$rowver[1]."\t".$rowver[4]."\t".$rowver[3]."\t".$valpromga."\t".$valcpcd."\t".$rowver[19]."\t".$valfuented."\t".$rowver[6]."\t".$rowver[9]."\t".$valppd."\t".$rowver[11]."\t".round($rowver[12])."\t".round($rowver[13])."\t".round($rowver[14])."\r\n");
									fputs($Descriptor3, "D\t".$rowver[1]."\t".$rowver[4]."\t".$rowver[3]."\t".$valpromga."\t".$valcpcd."\t".$valfuented."\t".$rowver[9]."\t".$valppd."\t".$rowver[11]."\t".round($rowver[12])."\t".round($rowver[13])."\t".round($rowver[14])."\r\n");
									fputs($Descriptor4, "D\t".$rowver[1]."\t".$rowver[3]."\t".$rowver[4]."\t".$valcpcd."\t".$valpromga."\t".$rowver[12]."\t".$rowver[13]."\t".$rowver[14]."\r\n");
									fputs($Descriptor5, "D\t".$rowver[1]."\t".$valcpcSGR."\t".$rowver[4]."\t".$rowver[5]."\t".$rowver[6]."\t".$rowver[7]."\t".$rowver[8]."\t".$rowver[9]."\t".$politicaPublicaSGR."\t".$rowver[11]."\t".round($rowver[12])."\t".round($rowver[13])."\t".round($rowver[14])."\r\n");
								}
							}
							echo "
								</table>
							";
							fclose($Descriptor1);
							fclose($Descriptor2);
							fclose($Descriptor3);
							fclose($Descriptor4);
							fclose($Descriptor5);
							echo "
							<script>document.getElementById('progreso').style.display='none';</script>";
						}break;
						case 5: //**EJECUCION DE GASTOS
						{

						}break;
					}

					echo "</div>";
				}
			?>
		</form>
	</body>
</html>
