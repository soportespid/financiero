<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");

	require "comun.inc";
	require "funciones.inc";
	require "conversor.php";
	require "validaciones.inc";

	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");

	session_start();
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html lang="es">
	<head>
		<meta charset="utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1"/>
		<meta name="viewport" content="user-scalable=no">
		<title>:: IDEAL 10 - Contabilidad</title>
		<link href="favicon.ico" rel="shortcut icon"/>
		<link href="css/css2.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
        <link href="css/cssSP.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<script type="text/javascript" src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
        <script type="text/javascript" src="jquery-1.11.0.min.js"></script> 
		<script src="sweetalert2/dist/sweetalert2.min.js"></script>
		<link rel="stylesheet" href="sweetalert2/dist/sweetalert2.min.css">
		<script>
			function agregar(cuenta, descripcion, tipo)
			{
                if (tipo == 'Auxiliar' || tipo == 'AUXILIAR') {

                    parent.document.form2.cuenta.value = cuenta;
                    parent.document.form2.cuenta_.value = cuenta;
                    parent.document.form2.ncuenta.value = descripcion;
                    parent.despliegamodal2("hidden");
                    parent.document.form2.submit();
                }
                else {

                    alert("Selecciona cuenta AUXILIAR");
                }
			}

            $(window).load(function () {
				$('#cargando').hide();
			});
		</script> 
	</head>
	<body>
		<form name="form2" method="post">

			<?php
				if(@ $_POST['oculto'] == "")
				{
					$_POST['numres'] = 10;
					$_POST['numpos'] = 0;
					$_POST['nummul'] = 0;
				}
			?>

			<table class="inicio ancho" style="width:99.5%">
				<tr>
					<td class="titulos" colspan="3">Cuentas NISCP - Almacén</td>
					<td class="cerrar" style="width:7%" onClick="parent.despliegamodal2('hidden');">Cerrar</td>
				</tr>
				
				<tr>
					<td class="tamano01" style='width:3cm;'>Número de cuenta:</td>
					<td>
						<input type="search" name="codnom" id="codnom" value="<?php echo @$_POST['codnom'];?>" style='width:100%;'/>
					</td>

					<td style="padding-bottom:0px;height:35px;"><em class="botonflecha" onClick="document.form2.submit();">Buscar</em></td>
				</tr>
			</table>

            <div id="cargando" style=" position:absolute;left: 46%; bottom: 45%">
				<img src="imagenes/loading.gif" style=" width: 80px; height: 80px"/>
			</div>

			<div class="subpantalla" style="height:82%; width:99.2%; overflow-x:hidden;">
				<table class='inicio' align='center' width='99%'>	

					<tr>
						<td colspan='5' class='titulos'>.: Resultados Busqueda:</td>
					</tr>

					<tr class='titulos2' style='text-align:center;'>
						<td style="width: 10%;">Cuenta</td>
						<td>Descripción</td>
						<td style="width: 15%;">Tipo</td>
                        <td style="width: 15%;">Naturaleza</td>
						<td style="width: 5%;">Estado</td>
					</tr>

					<?php
                        $codigoDestino = $_GET['cod'];
                        $fecha = $_GET['fecha'];

                        /* $codigoDestino = '00';
                        $fecha = "2022-01-01"; */

						$crit1="";
						
						if (@$_POST['codnom']!="")
                        {
                            $crit1 = "WHERE tabla.cuenta = '$_POST[codnom]' ";
                        }
						preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $fecha, $fecha);
						$fechaf = $fecha[3]."-".$fecha[2]."-".$fecha[1];

                        //Codigo de destino de compra y la fecha igual que en creaAdquision
						$sqlAlmacen = "SELECT cuenta_inicial, cuenta_final FROM `almdestinocompra_det_almacen` WHERE fecha = (SELECT MAX(fecha) FROM almdestinocompra_det_almacen WHERE fecha <= '$fechaf' AND codigo = '$codigoDestino') AND codigo = '$codigoDestino'"; 
						$resAlmacen = mysqli_query($linkbd,$sqlAlmacen);

						$iter  = 'saludo1a';
						$iter2 = 'saludo2';
						$conta = 1;

						while ($rowAlmacen = mysqli_fetch_assoc($resAlmacen))
						{		
                            $sqlr = "SELECT * FROM (SELECT cn1.cuenta,cn1.nombre,cn1.naturaleza,cn1.centrocosto,cn1.tercero,cn1.tipo,cn1.estado FROM cuentasnicsp AS cn1 INNER JOIN cuentasnicsp AS cn2 ON cn2.tipo='Auxiliar' AND cn2.naturaleza='DEBITO' AND cn2.cuenta LIKE CONCAT( cn1.cuenta, '%' ) WHERE cn1.tipo='Mayor' AND cn1.naturaleza='DEBITO' AND cn1.cuenta BETWEEN '$rowAlmacen[cuenta_inicial]' AND '$rowAlmacen[cuenta_final]' GROUP BY cn1.cuenta UNION SELECT cuenta,nombre,naturaleza,centrocosto,tercero,tipo,estado FROM cuentasnicsp WHERE tipo='Auxiliar' AND naturaleza='DEBITO' AND cuenta BETWEEN '$rowAlmacen[cuenta_inicial]' AND '$rowAlmacen[cuenta_final]') AS tabla $crit1 ORDER BY 1";
                            $res = mysqli_query($linkbd, $sqlr);
                            while($row = mysqli_fetch_row($res))
                            {
                   				?>
                                <tr class='<?php echo $iter ?>' style='text-align:center; text-transform:uppercase;' onclick="agregar('<?php echo $row[0]; ?>', '<?php echo $row[1]; ?>', '<?php echo $row[5]; ?>');">
                                    <td> <?php echo $row[0] ?> </td>
                                    <td> <?php echo $row[1] ?> </td>
                                    <td> <?php echo $row[5] ?> </td>
                                    <td> <?php echo $row[2] ?> </td>
                                    <td> <?php echo $row[6] ?> </td>
                                </tr>
								<?php
                                $aux=$iter;
                                $iter=$iter2;
                                $iter2=$aux;
                                $conta++;
                            }
						}
					?>
				</table>
			</div>

			<input type="hidden" name="oculto" id="oculto" value="1">
			<input type="hidden" name="numres" id="numres" value="<?php echo $_POST['numres'];?>"/>
			<input type="hidden" name="numpos" id="numpos" value="<?php echo $_POST['numpos'];?>"/>
			<input type="hidden" name="nummul" id="nummul" value="<?php echo $_POST['nummul'];?>"/>

		</form>
	</body>
</html>
