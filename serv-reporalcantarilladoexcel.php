<?php 
	require_once '/PHPExcel/Classes/PHPExcel.php';//Incluir la libreria PHPExcel 
	include '/PHPExcel/Classes/PHPExcel/IOFactory.php';// PHPExcel_IOFactory
	require"comun.inc";
	require"funciones.inc";
	session_start();
	$linkbd=conectar_bd();	
	$objPHPExcel = new PHPExcel();// Crea un nuevo objeto PHPExcel
	$objReader = PHPExcel_IOFactory::createReader('Excel2007');// Leemos un archivo Excel 2007
	$objPHPExcel = $objReader->load("formatos/Reporte Alcantarillado.xlsx");
	$borders = array(
      'borders' => array(
        'allborders' => array(
          'style' => PHPExcel_Style_Border::BORDER_THIN,
          'color' => array('argb' => 'FF000000'),
        )
      ),
    );
	ini_set('max_execution_time', 7200);
	// Agregar Informacion
	$xy=2;
	$vigencias=$_POST['vigencias'];
	$mes=$_POST['mes'];
	$meses=array('','Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre');
	$crit2 = "WHERE T1.id_liquidacion=T2.id_liquidacion AND T1.servicio LIKE '02'";
 	$crit4 = "AND T2.vigencia ='$vigencias' AND (T2.mes='$mes' OR T2.mesfin='$mes')";
 	$crit5 = "ORDER BY T2.codusuario, T2.id_liquidacion";
	$sqlr1 = "SELECT DISTINCT T2.vigencia,T2.mes,T2.mesfin,T2.codusuario,T2.id_liquidacion,T1.servicio,T1.estrato,T1.tarifa,T1.subsidio, T1.contribucion,T2.fecha,T1.valorliquidacion,T1.saldo,T1.abono FROM servliquidaciones_det T1,servliquidaciones T2 $crit2 $crit3 $crit4 $crit5";
    $resp1 = mysql_query($sqlr1,$linkbd);
	while ($row =mysql_fetch_row($resp1)) 
	{
		$sqlr="select *from servclientes where codigo='$row[3]'";
		$respc=mysql_query($sqlr,$linkbd);
		$rowc =mysql_fetch_row($respc);
		$filbor="A".$xy.":AV".$xy;
		switch ($row[6])
		{
			case '11':	$estratot="1 Bajo-Bajo";break;
			case '12': 	$estratot="2 Bajo";break;
			case '13': 	$estratot="3 Medio-Bajo";break;
			case '14': 	$estratot="4 Medio";break;
			default:	$estratot="No Residencial";
		}
		if($row[12]>0)
		{
			$diasmora='30';
			$valormora=$row[12];	
		}
		else
		{
			$diasmora='0';
			$valormora='0';
		}
		$objPHPExcel-> getActiveSheet ()
        -> getStyle ($filbor)
		-> getFont ()
		-> setBold ( false ) 
      	-> setName ('Arial') 
      	-> setSize ( 10 ) 
		-> getColor ()
		-> setRGB ('000000');
		$objPHPExcel->getActiveSheet()->getStyle($filbor)->applyFromArray($borders);
		$objPHPExcel->setActiveSheetIndex(0)
		->setCellValueExplicit ("A".$xy,$rowc[2], PHPExcel_Cell_DataType :: TYPE_STRING)
		->setCellValueExplicit ("B".$xy,$row[3], PHPExcel_Cell_DataType :: TYPE_STRING)
		->setCellValueExplicit ("C".$xy,"50", PHPExcel_Cell_DataType :: TYPE_NUMERIC)
		->setCellValueExplicit ("D".$xy,"223", PHPExcel_Cell_DataType :: TYPE_NUMERIC)
		->setCellValueExplicit ("E".$xy,substr($rowc[2],0,2), PHPExcel_Cell_DataType :: TYPE_STRING)
		->setCellValueExplicit ("F".$xy,substr($rowc[2],2,2), PHPExcel_Cell_DataType :: TYPE_STRING)
		->setCellValueExplicit ("G".$xy,substr($rowc[2],4,4), PHPExcel_Cell_DataType :: TYPE_STRING)
		->setCellValueExplicit ("H".$xy,substr($rowc[2],8,4), PHPExcel_Cell_DataType :: TYPE_STRING)
		->setCellValueExplicit ("I".$xy,substr($rowc[2],12,3), PHPExcel_Cell_DataType :: TYPE_STRING)
		->setCellValueExplicit ("J".$xy,utf8_encode($rowc[4]), PHPExcel_Cell_DataType :: TYPE_STRING)
		->setCellValueExplicit ("K".$xy,$row[4], PHPExcel_Cell_DataType :: TYPE_STRING)
		->setCellValueExplicit ("L".$xy,date('d-m-Y',strtotime($row[10])), PHPExcel_Cell_DataType :: TYPE_STRING)
		->setCellValueExplicit ("M".$xy,"1-$row[1]-$row[0]", PHPExcel_Cell_DataType :: TYPE_STRING)
		->setCellValueExplicit ("N".$xy,"30", PHPExcel_Cell_DataType :: TYPE_STRING)
		->setCellValueExplicit ("O".$xy,$estratot, PHPExcel_Cell_DataType :: TYPE_STRING)
		->setCellValueExplicit ("S".$xy,"3", PHPExcel_Cell_DataType :: TYPE_NUMERIC)
		->setCellValueExplicit ("T".$xy,"2", PHPExcel_Cell_DataType :: TYPE_NUMERIC)
		->setCellValueExplicit ("U".$xy,"0", PHPExcel_Cell_DataType :: TYPE_NUMERIC)
		->setCellValueExplicit ("V".$xy,"0", PHPExcel_Cell_DataType :: TYPE_NUMERIC)
		->setCellValueExplicit ("W".$xy,"0", PHPExcel_Cell_DataType :: TYPE_NUMERIC)
		->setCellValueExplicit ("X".$xy,$row[7], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
		->setCellValueExplicit ("Y".$xy,$row[7], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
		->setCellValueExplicit ("Z".$xy,"0", PHPExcel_Cell_DataType :: TYPE_NUMERIC)
		->setCellValueExplicit ("AA".$xy,"0", PHPExcel_Cell_DataType :: TYPE_NUMERIC)
		->setCellValueExplicit ("AB".$xy,"0", PHPExcel_Cell_DataType :: TYPE_NUMERIC)
		->setCellValueExplicit ("AC".$xy,"0", PHPExcel_Cell_DataType :: TYPE_NUMERIC)
		->setCellValueExplicit ("AD".$xy,$row[7], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
		->setCellValueExplicit ("AE".$xy,$row[8], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
		->setCellValueExplicit ("AF".$xy,$row[9], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
		->setCellValueExplicit ("AG".$xy,"0", PHPExcel_Cell_DataType :: TYPE_NUMERIC)
		->setCellValueExplicit ("AH".$xy,"0", PHPExcel_Cell_DataType :: TYPE_NUMERIC)
		->setCellValueExplicit ("AI".$xy,"0", PHPExcel_Cell_DataType :: TYPE_NUMERIC)
		->setCellValueExplicit ("AJ".$xy,"0", PHPExcel_Cell_DataType :: TYPE_NUMERIC)
		->setCellValueExplicit ("AK".$xy,"0", PHPExcel_Cell_DataType :: TYPE_NUMERIC)
		->setCellValueExplicit ("AL".$xy,"0", PHPExcel_Cell_DataType :: TYPE_NUMERIC)
		->setCellValueExplicit ("AM".$xy,"0", PHPExcel_Cell_DataType :: TYPE_NUMERIC)
		->setCellValueExplicit ("AN".$xy,"0", PHPExcel_Cell_DataType :: TYPE_NUMERIC)
		->setCellValueExplicit ("AO".$xy,$diasmora, PHPExcel_Cell_DataType :: TYPE_NUMERIC)
		->setCellValueExplicit ("AP".$xy,$valormora, PHPExcel_Cell_DataType :: TYPE_NUMERIC)
		->setCellValueExplicit ("AQ".$xy,"0", PHPExcel_Cell_DataType :: TYPE_NUMERIC)
		->setCellValueExplicit ("AR".$xy,"0", PHPExcel_Cell_DataType :: TYPE_NUMERIC)
		->setCellValueExplicit ("AS".$xy,"0", PHPExcel_Cell_DataType :: TYPE_NUMERIC)
		->setCellValueExplicit ("AT".$xy,"0", PHPExcel_Cell_DataType :: TYPE_NUMERIC)
		->setCellValueExplicit ("AU".$xy,$row[11], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
		->setCellValueExplicit ("AV".$xy,$row[13], PHPExcel_Cell_DataType :: TYPE_NUMERIC);
		$xy++;
	}
	// Renombrar Hoja
	//$objPHPExcel->getActiveSheet()->setTitle('Listado Asistencia');
	// Establecer la hoja activa, para que cuando se abra el documento se muestre primero.
	$objPHPExcel->setActiveSheetIndex(0);
	// Se modifican los encabezados del HTTP para indicar que se envia un archivo de Excel.
	header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
	header('Content-Disposition: attachment;filename="Reporte Alcantarillado.xlsx"');
	header('Cache-Control: max-age=0');
	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
	$objWriter->save('php://output');
	exit;
?>
