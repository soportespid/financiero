<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=iso-8859-1");
	require 'comun.inc';
	require 'funciones.inc';
	require 'funcionessp.inc';
	require 'validaciones.inc';
	require 'conversor.php';
	session_start();
	$linkbd=conectar_v7();
	cargarcodigopag($_GET[codpag],$_SESSION["nivel"]);
	header("Cache-control: private"); // Arregla IE 6
	header("Content-Type: text/html;charset=iso-8859-1");
	date_default_timezone_set("America/Bogota");
	//error_reporting(E_ALL);
	//ini_set('display_errors', '1');
?>
<!DOCTYPE >
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es">
	<head>
		<meta http-equiv="Content-type" content="text/html; charset=iso-8859-1"/>
		<meta http-equiv="X-UA-Compatible" content="IE=9"/>
		<title>:: SPID - Servicios Publicos</title>
		<link href="css/css2.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<script type="text/javascript" src="css/calendario.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script type="text/javascript" src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script>
			function procesos($tip)
			{
				switch ($tip)
				{
					case 1:	if (document.form2.fecha.value!='' && document.form2.vigencias.value!="" && document.form2.periodo.value!="" && document.form2.periodo2.value!="")
							{
								if(document.form2.periodo2.value >= document.form2.periodo.value!="")
								{
									document.form2.oculto.value='1';
									document.form2.submit();
								}
								else {despliegamodalm('visible','2','Mes final no debe ser menor al mes inicial');}
							}
							else{despliegamodalm('visible','2','Faltan datos para Calcular la Liquidacion de Facturacion');}
							break;
					case 2:	despliegamodalm('visible','4','Se generara las liquidaciones de este periodo, �Esta Seguro de Guardar?','1');
							break;
				}
			}
			function callprogress(vValor)
			{
				document.getElementById("getprogress").innerHTML = vValor;
				document.getElementById("getProgressBarFill").innerHTML = '<div class="ProgressBarFill" style="width: '+vValor+'%;"></div>';				
				document.getElementById("botliquida").style.display='none';
				document.getElementById("titulog1").style.display='block';
				document.getElementById("progreso").style.display='block';
				document.getElementById("getProgressBarFill").style.display='block';
				if (vValor==100){document.getElementById("titulog2").style.display='block';}
			}
			function despliegamodalm(_valor,_tip,mensa,pregunta)
			{
				document.getElementById("bgventanamodalm").style.visibility=_valor;
				if(_valor=="hidden"){document.getElementById('ventanam').src="";}
				else
				{
					switch(_tip)
					{
						case "1":	document.getElementById('ventanam').src="ventana-mensaje1.php?titulos="+mensa;break;
						case "2":	document.getElementById('ventanam').src="ventana-mensaje3.php?titulos="+mensa;break;
						case "3":	document.getElementById('ventanam').src="ventana-mensaje2.php?titulos="+mensa;break;
						case "4":	document.getElementById('ventanam').src="ventana-consulta1.php?titulos="+mensa+"&idresp="+pregunta;break;
					}
				}
			}
			function funcionmensaje(){}
			function respuestaconsulta(pregunta)
			{
				switch(pregunta)
				{
					case "1":	document.form2.oculto.value=3;
								document.form2.submit();break;
				}
			}
		</script>
		<?php 
			titlepag();
			if (@$_POST['oculto']==""){$_POST['menuact']=@$_GET['menu'];}
			if (@$_POST['menuact']=="SI")
			{$boatras="<img src='imagenes/iratras.png' title='Men&uacute; Nomina' onClick=\"location.href='serv-menufacturacion.php'\" class='mgbt'>";}
			else {$boatras="";}
		?>
	</head>
	<body>
		<IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
		<span id="todastablas2"></span>
		<table>
			<tr><script>barra_imagenes("serv");</script><?php cuadro_titulos();?></tr>	 
			<tr><?php menu_desplegable("serv");?></tr>
			<tr>
 				<td colspan="3" class="cinta"><img src="imagenes/add.png" title="Nuevo" onClick="location.href='serv-liquidarservicios.php'" class="mgbt"/><img src="imagenes/guardad.png" class="mgbt1"/><img src="imagenes/busca.png" title="Buscar" class="mgbt" onClick="location.href='serv-buscaliquidarservicios.php'" /><img src="imagenes/nv.png" onClick="mypop=window.open('serv-principal.php','','');mypop.focus();" title="Nueva Ventana" class="mgbt"><?php echo $boatras;?></td>
			</tr>
		</table>
		<div id="bgventanamodalm" class="bgventanamodalm">
			<div id="ventanamodalm" class="ventanamodalm">
				<IFRAME src="" name="ventanam" marginWidth=0 marginHeight=0 frameBorder=0 id="ventanam" frameSpacing=0 style=" width:700px; height:130px; top:200; overflow:hidden;"></IFRAME>
			</div>
		</div>
		<form name="form2" method="post" action="">
			<?php
				//*** cargar parametros de liquidacion
				if(@$_POST['oculto']=='')
				{
					$_POST['numres']=1000;
					$_POST['numpos']=0;
					$_POST['nummul']=0;
					$_POST['oculto']='0';
				}
				$_POST['idliqui']=selconsecutivo('servliquidaciones_gen','id_cab');
				$sqlr="SELECT * FROM servparametros";
				$resp = mysqli_query($linkbd,$sqlr);
				while ($row =mysqli_fetch_row($resp))
				{
					$_POST['tipo']=$row[1];
					$_POST['rangofact']=$row[2];
					$_POST['diacorte']=$row[3];
					$_POST['cargof']=$row[4];
				}
				$vigusu=vigencia_usuarios($_SESSION['cedulausu']);
				$vact=$vigusu;
			?>	
			<table class="inicio">
				<tr>
					<td class="titulos" colspan="13">Liquidacion Servicios</td>
					<td class="cerrar" style="width:7%" onClick="location.href='serv-principal.php'">Cerrar</td>
				</tr>
				<tr>
					<td class="saludo1" >No Liquidacion</td>
					<td><input name="idliqui" value="<?php echo @$_POST['idliqui']?>" type="text" size="3" readonly></td>
					<td class="saludo1">Fecha Liquidacion</td>
					<td><input type="date" name="fecha" value="<?php echo @$_POST['fecha']; ?>" onKeyUp="return tabular(event,this) "></td>
					<td class="saludo1">Vigencia</td>
					<td>
						<select name="vigencias" id="vigencias">
							<option value="">Sel..</option>
							<?php	  
								for($x=$vact;$x>=$vact-2;$x--)
								{
									if($x==@$_POST['vigencias']){echo "<option value='$x' SELECTED>$x</option>";}
									else {echo "<option value='$x'>$x</option>";}
								}
							?>
						</select>
					</td>
					<td class="saludo1">Mes Inicial:</td>
					<td>
						<select name="periodo" id="periodo">
							<option value="-1">Seleccione ....</option>
							<?php
								$sqlr="SELECT * FROM meses WHERE estado='S'";
								$resp = mysqli_query($linkbd,$sqlr);
								while ($row =mysqli_fetch_row($resp)) 
								{
									if($row[0]==@$_POST['periodo'])
									{
										echo "<option value='$row[0]' SELECTED>$row[1]</option>";
										$_POST['periodonom']=$row[1];
									}
									else {echo "<option value='$row[0]'>$row[1]</option>";}
								}   
							?>
						</select> 
					</td>
					<td class="saludo1">Mes Final:</td>
					<td>
						<select name="periodo2" id="periodo2">
							<option value="-1">Seleccione ....</option>
							<?php
								$sqlr="SELECT * FROM meses WHERE estado='S' ";
								$resp = mysqli_query($linkbd,$sqlr);
								while ($row =mysqli_fetch_row($resp)) 
								{
									if($row[0]==@$_POST['periodo2'])
									{
										echo "<option value='$row[0]' SELECTED>$row[1]</option>";
										$_POST['periodonom2']=$row[1];
									}
									else {echo "<option value='$row[0]'>$row[1]</option>";}
								}
							?>
						</select> 
					</td>
					<td class="saludo1">Servicio:</td>
					<td>
						<select name="servicios"   onKeyUp="return tabular(event,this)">
							<option value="">Todos ....</option>
							<?php
								$sqlr="SELECT * FROM servservicios ";
								$res=mysqli_query($linkbd,$sqlr);
								while ($row =mysqli_fetch_row($res))
								{
									if(0==strcmp($row[0],$_POST['servicios']))
									{
										echo "<option value='$row[0]' SELECTED>$row[0] - $row[1]</option>";
										$_POST['nomservicio']=$row[1];
									}
									else {echo "<option value='$row[0]'>$row[0] - $row[1]</option>";}
								}
							?>
						</select>
					</td>
					<td >
						<input type="button" name="buscapredios"  value=" Consultar " onClick="procesos(1)"/> 
						<input type="hidden" name="oculto" id="oculto" value="<?php echo @$_POST['oculto'] ?>"/>
						<input type="hidden" name="facturatot" id="facturatot" value="<?php echo @$_POST['facturatot'] ?>"/>
						<input type="hidden" name="facturaini" id="facturaini" value="<?php echo @$_POST['facturaini'] ?>"/>
						<input type="hidden" name="facturafin" id="facturafin" value="<?php echo @$_POST['facturafin'] ?>"/>
						<input type="hidden" name="nomservicio" id="nomservicio" value="<?php echo @@$_POST['nomservicio'];?>"/>
						<input type="hidden" name="periodonom" id="periodonom" value="<?php echo @$_POST['periodonom'];?>"/>
						<input type="hidden" name="periodonom2" id="periodonom2" value="<?php echo @$_POST['periodonom2'];?>"/>
						<input type="hidden" name="numres" id="numres" value="<?php echo @$_POST['numres'];?>"/>
						<input type="hidden" name="numpos" id="numpos" value="<?php echo @$_POST['numpos'];?>"/>
						<input type="hidden" name="nummul" id="nummul" value="<?php echo @$_POST['nummul'];?>"/> 
						<input type="hidden" name="menuact" id="menuact" value="<?php echo @$_POST['menuact'];?>"/>
					</td>
				</tr>
			</table>
			<?php
				$contacerradas=0;
				$sqlr="SELECT * FROM servliquidaciones_gen WHERE estado != 'C' ORDER BY id_cab";
				$resp=mysqli_query($linkbd,$sqlr);
				while ($row =mysqli_fetch_row($resp))
				{
					if($contacerradas==0)
					{
						echo "
						<div class='subpantallac5' style='height:30%;overflow-x:hidden;'>
						<table class='inicio'>
							<tr><td class='titulos' colspan='8'>Facturaci&oacute;n sin Cerrar</td></tr>
							<tr>
								<td class='titulos2' style='width:8%'>N&deg; Liquidaci&oacute;n</td>
								<td class='titulos2' style='width:8%'>Fecha</td>
								<td class='titulos2' style='width:6%'>Vigencia</td>
								<td class='titulos2' style='width:15%'>Periodos</td>
								<td class='titulos2' style='width:14%'>N&deg; Facturaci&oacute;n</td>
								<td class='titulos2' style='width:8%'>Total Facturas</td>
								<td class='titulos2'>Sevicios</td>
							</tr> ";
						$contacerradas=1;
					}
					if($row[5]==""){$tipserv="TODOS";}
					else
					{
						$sqlrsv="SELECT nombre FROM servservicios WHERE codigo='$row[5]'";
						$resv=mysqli_query($linkbd,$sqlrsv);
						$rowsv =mysqli_fetch_row($resv); 
						$tipserv="$rowsv[0]";
					}
					$vercorte="onDblClick=\"mypop=window.open('serv-cortefact.php?idcorte=$row[0]','','');mypop.focus();\"";
					echo
					"<tr class='saludo2' $vercorte title='Doble Click para realizar el corte de la liquidaci&oacute;n N&deg; $row[0]'>
						<td>$row[0]</td>
						<td>$row[4]</td>
						<td>$row[1]</td>
						<td>".mesletras($row[2])." -".mesletras($row[3])."</td>
						<td>$row[8] - $row[9]</td>
						<td>".($row[9]-$row[8]+1)."</td>
						<td>$tipserv</td>
					</tr>";
				}
				if($contacerradas==1){echo "</table></div>";}
				if(@$_POST['oculto']>=1)
				{
					if(@$_POST['oculto']==1)
					{
							$sqlr="SELECT MAX(id_liquidacion) FROM servliquidaciones";
							$resp=mysqli_query($linkbd,$sqlr);
							while ($row =mysqli_fetch_row($resp))
							{
								$_POST['facturaini']=(float)$row[0]+1;
								$_POST['oculto']=2;
								echo"
								<script>
									document.getElementById('facturaini').value='".$_POST['facturaini']."';
									document.getElementById('oculto').value=2;
								</script>"; 
							}
						$sqlr="SELECT distinct T2.consecutivo FROM servclientes AS T1, terceros_servicios AS T2 WHERE T1.codigo = T2.consecutivo AND T1.estado = 'S' AND T2.estado = 'S' AND T2.servicio like '%".$_POST['servicios']."%'";
						$resp=mysqli_query($linkbd,$sqlr);
						$ntr = mysqli_num_rows($resp);
						$_POST['facturatot']=$ntr;
						$_POST['facturafin']=(float)$ntr+(float)$_POST['facturaini']-1;
						echo"
						<script>
							document.getElementById('facturafin').value='".$_POST['facturafin']."';
							document.getElementById('facturatot').value='".$_POST['facturatot']."';
						</script>"; 
					}
					if(@$_POST['servicios']==""){$nombreser="Todos";}
					else {$nombreser=$_POST['nomservicio'];}
					echo"
					<table class='inicio'>
						<tr><td class='titulos' colspan='8'>Facturaci&oacute;n para generar</td></tr>
						<tr>
							<td class='titulos2' style='width:8%'>N&deg; Liquidaci&oacute;n</td>
							<td class='titulos2' style='width:8%'>Fecha</td>
							<td class='titulos2' style='width:6%'>Vigencia</td>
							<td class='titulos2' style='width:15%'>Periodos</td>
							<td class='titulos2' style='width:14%'>N&deg; Facturaci&oacute;n</td>
							<td class='titulos2' style='width:8%'>Total Facturas</td>
							<td class='titulos2' style='width:15%'>Sevicios</td>
							<td class='titulos2'>Proceso</td>
						</tr> 
						<tr class='saludo2'>
							<td>".$_POST['idliqui']."</td>
							<td>".$_POST['fecha']."</td>
							<td>".$_POST['vigencias']."</td>
							<td>".$_POST['periodonom']." - ".$_POST['periodonom2']."</td>
							<td>".$_POST['facturaini']." - ".$_POST['facturafin']."</td>
							<td>".$_POST['facturatot']."</td>
							<td>$nombreser</td>
							<td>
								<input type='button' name='botliquida' id='botliquida' value=' Liquidar Servicio ' onClick='procesos(2)' style='display:block;'/>
								<div id='titulog1' style='display:none; float:left'></div>
								<div id='progreso' class='ProgressBar' style='display:none; float:left'>
									<div class='ProgressBarText'><span id='getprogress'></span>&nbsp;% </div>
									<div id='getProgressBarFill'></div>
								</div>
							</td>
						</tr>
					</table>";
				}?>
				<div class="subpantalla" style="height:40.5%; width:99.6%;"	>
				<?php
				if(@$_POST['oculto']==3)
				{
					ini_set('max_execution_time', 14400);
					$_POST['oculto']=2;
					$multimeses=($_POST['periodo2']-$_POST['periodo'])+1;
					$nuevo="";
					$actual="";
					$contador=0;
					$ngen=0;
					$intereses=0;
					//***** numero de liquidaciones 
					$sqlr="SELECT max(id_liquidacion) FROM servliquidaciones";
					$resp=mysqli_query($linkbd,$sqlr);
					while ($row =mysqli_fetch_row($resp)){$contador=$row[0];}
					$ngen=selconsecutivo('servliquidaciones_gen','id_cab');
					$sqlr="INSERT INTO servliquidaciones_gen (id_cab,vigencia,mes,mesfin,fecha,servicio,sistema,estado,numinicial, numfinal) VALUES ('$ngen','".$_POST['vigencias']."','".$_POST['periodo']."','".$_POST['periodo2']."', '".$_POST['fecha']."','".$_POST['servicios']."','".$_SESSION['cedulausu']."','L','".$_POST['facturaini']."', '".$_POST['facturafin']."')";
					mysqli_query($linkbd,$sqlr);
					$sqlr="
					SELECT T2.consecutivo,T1.direccion,T1.terceroactual,T2.servicio,T1.estrato 
					FROM servclientes T1, terceros_servicios T2 
					WHERE T1.codigo=T2.consecutivo AND T1.estado='S' AND T2.estado='S' AND T2.servicio LIKE '%".$_POST['servicios']."%' 
					ORDER BY T1.zona,T1.lado,T1.barrio,T2.codcatastral,T1.codigo,T2.servicio";
					$resp=mysqli_query($linkbd,$sqlr);
					$intereses=0;
					$totalcli=mysqli_affected_rows ($linkbd);
					$cliq=0;
					while ($row =mysqli_fetch_row($resp)) 
					{
						$cliq+=1;
						$porcentaje = $cliq * 100 / $totalcli; 
						echo"<script>progres='".round($porcentaje)."';callprogress(progres);</script>"; 
						flush(); 
						ob_flush();
						usleep(5);
						$actual=$row[0];
						$saldoanterior=0;//hacer funcion de calculo deudas
						$vargen = fgeneral01("$row[3]","$row[4]","0","$row[0]");
						$medicion=0;
						$tipoliqser=$vargen[0];
						$subsidio=$vargen[1];
						$saldoanterior=$vargen[2];
						$porsubsidio=$subsidio/100;
						$intereses=$vargen[3];
						$tarifa=$vargen[4];
						$tarifa[0]=$multimeses*$tarifa[0];
						$tarifa[1]=$multimeses*$tarifa[1];
						$descuento=0;
						$porcontribucion=$vargen[5];
						$porcontribucion=$porcontribucion/100;
						$contribucion=($tarifa[0])*$porcontribucion;
						$valorsub=($tarifa[0])*$porsubsidio;
						$valortarcf=($tarifa[0])+($tarifa[1]);
						$valorliq=($saldoanterior+($tarifa[0])-$valorsub+($tarifa[1])-$descuento+$contribucion)+$intereses;
						if ($valorliq<0)
						{
							$saldoanterior=$valorliq;
							$valorliq=0;
						}
						if($nuevo!=$actual)
						{
							$contador+=1;
							$sqlr1="INSERT INTO servliquidaciones (id_liquidacion,fecha,vigencia,mes,mesfin,codusuario,tercero, valortotal,saldo,estado,liquidaciongen,factura) VALUES ('$contador','".$_POST['fecha']."', '".$_POST['vigencias']."','".$_POST['periodo']."','".$_POST['periodo2']."', '$row[0]','$row[2]','0','0','S', '$ngen','')";
							mysqli_query($linkbd,$sqlr1);
							$nuevo=$actual;
						}
						if($nuevo!=$actual ){$contador+=1;}	
						$sqlr2="INSERT INTO servliquidaciones_det (id_liquidacion,codusuario,tercero,servicio,estrato,medicion,cargofijo, tarifa,subsidio,descuento,contribucion,valorliquidacion,saldo) VALUES ('$contador','$row[0]','$row[2]', '$row[3]','$row[4]','$medicion','$tarifa[1]','$tarifa[0]','$valorsub','$descuento','$contribucion','$valorliq', '$saldoanterior')";
						mysqli_query($linkbd,$sqlr2);
						$ultimoid=$contador;
					}
					
					if(@$ultimoid != @$_POST['facturafin'])
					{
						$_POST['facturafin']=@$ultimoid;
						echo"<script>document.form2.facturafin.value='".@$ultimoid."';</script>";
						$sqlr4 ="UPDATE servliquidaciones_gen SET numfinal='".$_POST['facturafin']."' WHERE id_cab='".$_POST['idliqui']."' AND vigencia='".$_POST['vigencias']."'";
						mysqli_query($linkbd,$sqlr4);
					}
				}
			?>
			</div>
		</form>
	</body>
</html>