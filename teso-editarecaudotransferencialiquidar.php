<?php //V 1000 12/12/16 ?> 
<?php
require "comun.inc";
require "funciones.inc";
require "conversor.php";
session_start();
$linkbd_V7 = conectar_v7();
$linkbd_V7 -> set_charset("utf8");

?>
<html lang="es">
	<head>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
    	<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<title>:: IDEAL 10 - Tesorer&iacute;a</title>

		<script>
			//************* ver reporte ************
			//***************************************
			function verep(idfac)
			{
				document.form1.oculto.value=idfac;
				document.form1.submit();
			}

			//************* genera reporte ************
			//***************************************
			function genrep(idfac)
			{
				document.form2.oculto.value=idfac;
				document.form2.submit();
			}

			function buscacta(e)
			{
				if (document.form2.cuenta.value!="")
				{
					document.form2.bc.value='1';
					document.form2.submit();
				}
			}
		
			function validar()
			{
				document.form2.submit();
			}

			function buscater(e)
 			{
				if (document.form2.tercero.value!="")
				{
					document.form2.bt.value='1';
					document.form2.submit();
 				}
 			}

			function agregardetalle()
			{
				if(document.form2.codingreso.value!="" &&  document.form2.valor.value>0  )
				{ 
					document.form2.agregadet.value=1;
					//document.form2.chacuerdo.value=2;
					document.form2.submit();
 				}
 				else 
				{
 					alert("Falta informacion para poder Agregar");
 				}
			}

			function eliminar(variable)
			{
				document.form2.elimina.value=variable;
				despliegamodalm('visible','4','Esta Seguro de Eliminar','1');
				
				/* if (confirm("Esta Seguro de Eliminar"))
  				{
					document.form2.elimina.value=variable;
					//eli=document.getElementById(elimina);
					vvend=document.getElementById('elimina');
					//eli.value=elimina;
					vvend.value=variable;
					document.form2.submit();
				} */
			}

			//************* genera reporte ************
			//***************************************
			function guardar()
			{
				ingresos2=document.getElementsByName('dcoding[]');
				var validacion01=document.getElementById('concepto').value;
				var validacion02=document.getElementById('ntercero').value;
				if (document.form2.fecha.value!='' && ingresos2.length>0 && (validacion01.trim()!='') && (validacion02.trim()!=''))
  				{
					despliegamodalm('visible','4','Esta Seguro de Guardar','2');
				}
				else
				{
  					despliegamodalm('visible','2',"Falta información para poder guardar")
  					document.form2.fecha.focus();
  					document.form2.fecha.select();
  				}

				/* ingresos2=document.getElementsByName('dcoding[]');
				if (document.form2.fecha.value!='' && ingresos2.length>0)
 				{
					if (confirm("Esta Seguro de Guardar"))
					{
						document.form2.oculto.value=2;
						document.form2.submit();
  					}
  				}
  				else
				{
  					alert('Faltan datos para completar el registro');
					document.form2.fecha.focus();
					document.form2.fecha.select();
  				} */
			}

			function pdf()
			{
				document.form2.action="teso-pdfliquidarecaudostrans.php";
				document.form2.target="_BLANK";
				document.form2.submit(); 
				document.form2.action="";
				document.form2.target="";
			}

			function buscater(e)
 			{
				if (document.form2.tercero.value!="")
				{
					document.form2.bt.value='1';
					document.form2.submit();
 				}
 			}

			function buscaing(e)
 			{
				if (document.form2.codingreso.value!="")
				{
					document.form2.bin.value='1';
					document.form2.submit();
 				}
 			}

			function adelante()
			{
  				//alert("adelante"+document.form2.idcomp.value);
				//document.form2.oculto.value=2;
				if(parseFloat(document.form2.idcomp.value)<parseFloat(document.form2.maximo.value))
 				{
					document.form2.oculto.value='';
					//document.form2.agregadet.value='';
					//document.form2.elimina.value='';
					//document.form2.ncomp.value=parseFloat(document.form2.ncomp.value)+1;
					let id_comprobante = parseFloat(document.form2.idcomp.value)+1;
					document.form2.idcomp.value = id_comprobante;
					document.form2.action="teso-editarecaudotransferencialiquidar.php?idrecaudo="+id_comprobante;
					document.form2.submit();
				}
			}

			function atrasc()
			{
	   			//alert("atras"+document.form2.ncomp.value);
				//document.form2.oculto.value=2;
				if(document.form2.idcomp.value>=1)
 				{
					document.form2.oculto.value='';
					//document.form2.agregadet.value='';
					//document.form2.elimina.value='';
					//document.form2.ncomp.value=document.form2.ncomp.value-1;
					let id_comprobante = parseFloat(document.form2.idcomp.value)-1;
					document.form2.idcomp.value = id_comprobante;
					document.form2.action="teso-editarecaudotransferencialiquidar.php?idrecaudo="+id_comprobante;
					document.form2.submit();
 				}
			}

			function validar2()
			{
				//alert("Balance Descuadrado");
				document.form2.oculto.value='';
				let id_comprobante = document.form2.idcomp.value;
				document.form2.ncomp.value=id_comprobante;

				//document.form2.agregadet.value='';
				//document.form2.elimina.value='';
				document.form2.action="teso-editarecaudotransferencialiquidar.php?idrecaudo="+id_comprobante;
				document.form2.submit();
			}
			function iratras()
			{
				var idcomp=document.form2.idcomp.value;
				location.href="teso-buscarecaudotransferencialiquidar.php?id="+idcomp;
			}

			function despliegamodal2(_valor,_nvent)
			{
				document.getElementById("bgventanamodal2").style.visibility=_valor;
				if(_valor=="hidden"){document.getElementById('ventana2').src="";}
				else 
				{
					if(_nvent=='1')
					{document.getElementById('ventana2').src="tercerosgral-ventana01.php?objeto=tercero&nobjeto=ntercero&nfoco=codingreso";}
					else
					{document.getElementById('ventana2').src="ingresos-ventana01.php?ti=I&modulo=4";}
				}
			}

			function despliegaModalFuentes(_valor) {

				var codigo=document.getElementById('codingreso').value;

				if (codigo != "") {

					document.getElementById("bgventanamodal2").style.visibility=_valor;
					if(_valor=="hidden"){document.getElementById('ventana2').src="";}
					else 
					{
						

						if (codigo != "") {
							document.getElementById('ventana2').src="teso-ventana-fuentes.php?codigo="+codigo;
						}
						else {
							alert("Seleccione primero el código de ingreso");
						}
					}
				}
				else {
					alert("Seleccione primero el código de ingreso");
				}
			}

			function despliegamodalm(_valor,_tip,mensa,pregunta)
			{
				document.getElementById("bgventanamodalm").style.visibility=_valor;
				if(_valor=="hidden")
				{
					document.getElementById('ventanam').src="";
					switch(document.getElementById('valfocus').value)
					{
						case "1":	document.getElementById('valfocus').value='';
									document.getElementById('tercero').focus();
									document.getElementById('tercero').select();
									break;
						case "2":	document.getElementById('valfocus').value='';
									document.getElementById('codingreso').focus();
									document.getElementById('codingreso').select();
									break;
					}
				}
				else
				{
					switch(_tip)
					{
						case "1":
							document.getElementById('ventanam').src="ventana-mensaje1.php?titulos="+mensa;break;
						case "2":
							document.getElementById('ventanam').src="ventana-mensaje3.php?titulos="+mensa;break;
						case "3":
							document.getElementById('ventanam').src="ventana-mensaje2.php?titulos="+mensa;break;
						case "4":
							document.getElementById('ventanam').src="ventana-consulta1.php?titulos="+mensa+"&idresp="+pregunta;break;	
					}
				}
			}

			function funcionmensaje()
			{
				var numdocar=document.getElementById('idcomp').value;
				document.location.href = "teso-editarecaudotransferencialiquidar.php?idrecaudo="+numdocar;
			}

			function respuestaconsulta(pregunta)
			{
				switch(pregunta)
				{
					case "1":	document.form2.oculto.value='3';
								document.form2.submit();
								break;
					case "2":	document.form2.oculto.value='2';
								document.form2.submit();
								break;
				}
			}

		</script>

		<script src="css/programas.js"></script>
		<script src="css/calendario.js"></script>
		<link href="css/css2.css" rel="stylesheet" type="text/css" />
		<link href="css/css3.css" rel="stylesheet" type="text/css" />
		<link href="css/css4.css" rel="stylesheet" type="text/css" />
		<link href="css/tabs.css" rel="stylesheet" type="text/css" />
		<?php titlepag();?>
	</head>
	<body>
		<div id="bgventanamodalm" class="bgventanamodalm">
            <div id="ventanamodalm" class="ventanamodalm">
                <IFRAME src="" name="ventanam" marginWidth=0 marginHeight=0 frameBorder=0 id="ventanam" frameSpacing=0 style=" width:700px; height:130px; top:200; overflow:hidden;"> 
                </IFRAME>
            </div>
        </div>
		<IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
		<span id="todastablas2"></span>
		<table>
			<tr><script>barra_imagenes("teso");</script><?php cuadro_titulos();?></tr>	 
			<tr><?php menu_desplegable("teso");?></tr>
			<tr>
				<td colspan="3" class="cinta">
					<a href="teso-recaudotransferencialiquidar.php" class="mgbt"><img src="imagenes/add.png" title="Nuevo"/></a>
					<a class="mgbt"><img src="imagenes/guardad.png"  title="Guardar" /></a>
					<a href="teso-buscarecaudotransferencialiquidar.php" class="mgbt"> <img src="imagenes/busca.png"  title="Buscar" /></a>
					<a onClick="mypop=window.open('plan-agenda.php','','');mypop.focus()" class="mgbt"><img src="imagenes/agenda1.png" title="Agenda"/></a>  
					<a onClick="mypop=window.open('teso-principal.php','','');mypop.focus();" class="mgbt"><img src="imagenes/nv.png" title="Nueva ventana"></a>
					<a onClick="mypop=window.open('<?php echo $url2; ?>','','');mypop.focus();" class="tooltip bottom mgbt"><img src="imagenes/duplicar_pantalla.png" title="Duplicar pesta&ntilde;a"></a>
					<a onClick="pdf()" class="mgbt"><img src="imagenes/print.png" title="Imprimir" /></a>
					<a onClick="iratras()" class="mgbt"><img src="imagenes/iratras.png" title="Atr&aacute;s"></a>
				</td>
			</tr>		  
		</table>
		<tr>
			<td colspan="3" class="tablaprin" align="center"> 
				<?php
				//$linkbd=conectar_bd();
				/* $vigusu=vigencia_usuarios($_SESSION['cedulausu']);
				$vigencia=$vigusu;
				$_POST['vigencia']=$vigencia; */

				$sqlr = "SELECT MAX(id_recaudo) FROM tesorecaudotransferencialiquidar";
				$res = mysqli_query($linkbd_V7, $sqlr);
				$r = mysqli_fetch_row($res);
				
				$_POST['maximo']=$r[0];
 
	  			//*********** 11050501	CAJA PRINCIPAL esta es la cuenta que va a credito en todas las consignacones
				
				if($_POST['oculto'] == "")
				{
					$_POST['idcomp'] = $_GET['idrecaudo'];
	 				$_POST['ncomp'] = $_POST['idcomp'];

					$sqlr = "SELECT fecha, vigencia, concepto, tercero, cc, medio_pago FROM tesorecaudotransferencialiquidar WHERE id_recaudo = '".$_POST['ncomp']."'";
					$res = mysqli_query($linkbd_V7, $sqlr);
					$row = mysqli_fetch_row($res);

					$p1 = substr($row[0],0,4);
					$p2 = substr($row[0],5,2);
					$p3 = substr($row[0],8,2);
					$_POST['fecha'] = $p3."/".$p2."/".$p1;
					$_POST['vigencia'] = $row[1];
					$_POST['concepto'] = $row[2];
					$_POST['tercero'] = $row[3];
					$_POST['ntercero'] = buscatercero($row[3]);
					$_POST['cc'] = $row[4];
					$_POST['medioDePago'] = $row[5];

					$_POST['dcoding'] = array();
					$_POST['dncoding'] = array();
					$_POST['dfuente'] = array();
					$_POST['dcc'] = array();
					$_POST['dvalores'] = array();

					$total = 0;
					$sqlr_det = "SELECT ingreso, valor, fuente, cc FROM tesorecaudotransferencialiquidar_det WHERE id_recaudo = '".$_POST['ncomp']."'";
					$res_det = mysqli_query($linkbd_V7, $sqlr_det);
					while($row_det = mysqli_fetch_row($res_det))
					{
						$_POST['dcoding'][] = $row_det[0];
						$_POST['dncoding'][] = buscaingreso($row_det[0]);
						$_POST['dfuente'][] = $row_det[2];
						$_POST['dcc'][] = $row_det[3];
						$total = $total+$row_det[1];
						$_POST['dvalores'][] = $row_det[1];
					}
				}
				
				switch($_POST['tabgroup1'])
				{
					case 1:
						$check1='checked';
					break;
					case 2:
						$check2='checked';
					break;
					case 3:
						$check3='checked';
				}
				?>
 				<form name="form2" method="post" action=""> 
 					<?php
 					//***** busca tercero
					if($_POST['bt'] == '1')
					{
			  			$nresul = buscatercero($_POST['tercero']);
			  			if($nresul != '')
			   			{
			  				$_POST['ntercero'] = $nresul;
			  			}
						else
						{
			  				$_POST['ntercero'] = "";
			  			}
			 		}
					//******** busca ingreso *****
					//***** busca tercero
					if($_POST['bin'] == '1')
					{
						$nresul = buscaingreso($_POST['codingreso']);
						if($nresul != '')
			   			{
			  				$_POST['ningreso'] = $nresul;
			  			}
						else
						{
			  				$_POST['ningreso'] = "";
			  			}
			 		}	 
 					?>
 
    				<table class="inicio" align="center" >
						<tr>
							<td class="titulos" colspan="9">Liquidacion Recaudos Transferencias</td>
							<td  class="cerrar" ><a href="teso-principal.php">Cerrar</a></td>
						</tr>
      					<tr>
							<td style="width:10%;" class="saludo1" >Numero Recaudo:</td>
							<td style="width:15%;">
								<a href="#" onClick="atrasc()">
									<img src="imagenes/back.png" alt="anterior" align="absmiddle">
								</a>
								<input name="idcomp" type="text" value="<?php echo $_POST['idcomp']?>" onKeyUp="return tabular(event,this)" style="width:50%; text-align:center" onBlur='validar2()' >
								<input name="ncomp" type="hidden" value="<?php echo $_POST['ncomp']?>">
								<a href="#" onClick="adelante()">
									<img src="imagenes/next.png" alt="siguiente" align="absmiddle">
								</a>
								<input type="hidden" value="a" name="atras" >
								<input type="hidden" value="s" name="siguiente" >
								<input type="hidden" value="<?php echo $_POST['maximo']?>" name="maximo">
        					</td>
	  						<td style="width:10%;" class="saludo1">Fecha:</td>
							<td style="width:10%;">
								<input name="fecha" type="text" id="fc_1198971545" title="DD/MM/YYYY" style="width:70%;" value="<?php echo $_POST['fecha']; ?>" onKeyUp="return tabular(event,this) " onKeyDown="mascara(this,'/',patron,true)"  maxlength="10" readonly>
								         
							</td>
							<td style="width:10%;" class="saludo1">Vigencia:</td>
							<td style="width:10%;">
		  						<input type="text" id="vigencia" name="vigencia" onKeyPress="javascript:return solonumeros(event)" onKeyUp="return tabular(event,this)"  value="<?php echo $_POST['vigencia']?>" onClick="document.getElementById('tipocta').focus(); document.getElementById('tipocta').select();" readonly> 
		  					</td>
		   					<td class="saludo1" style="width:10%;">Causacion Contable:</td>
							<td style="width:4%;">
								<select name="causacion" id="causacion" onKeyUp="return tabular(event,this)"  >
									<option value="1" <?php if($_POST['causacion']=='1') echo "SELECTED"; ?>>Si</option>
									<option value="2" <?php if($_POST['causacion']=='2') echo "SELECTED"; ?>>No</option>         
								</select>
							</td>
							<td class="saludo1" style="width:10%;">Medio de pago: </td>
							<td style="width:10%;">
								<select name="medioDePago" id="medioDePago" onKeyUp="return tabular(event,this)" style="width:100%">
									<option value="1" <?php if(($_POST['medioDePago']=='1')) echo "SELECTED"; ?>>Con SF</option>
									<option value="2" <?php if($_POST['medioDePago']=='2') echo "SELECTED"; ?>>Sin SF</option>         
								</select>
								<input type="hidden" name="vigencia"  value="<?php echo $_POST['vigencia']?>" onKeyUp="return tabular(event,this)" readonly/>
							</td>
							<td></td>    
        				</tr>
						<tr>
							<td class="saludo1">Concepto Recaudo:</td>
							<td colspan="9" >
								<textarea name="concepto" id="concepto" style="width:100%; form-sizing: content; max-height: 100px; min-height: 20px; resize: vertical; background-color:#E6F7FF;" class="estilos-scroll" disabled><?php echo $_POST['concepto']?></textarea>
							</td>
						</tr>  
						<tr>
							<td  class="saludo1">NIT: </td>
							<td>
								<input name="tercero" id="tercero" type="text" style="width:80%;" value="<?php echo $_POST['tercero']?>" onKeyUp="return tabular(event,this)" onBlur="buscater(event)" readonly>
							</td>
							<td class="saludo1">Contribuyente:</td>
							<td colspan="7">
								<input type="text" id="ntercero" name="ntercero" style="width:100%;" value="<?php echo $_POST['ntercero']?>" onKeyUp="return tabular(event,this) "  readonly>
								<input type="hidden" value="0" name="bt">
								<input type="hidden" id="cb" name="cb" value="<?php echo $_POST['cb']?>" >
								<input type="hidden" id="ct" name="ct" value="<?php echo $_POST['ct']?>" >
								<input type="hidden" value="1" name="oculto">
							</td>
						</tr>

    				</table>
       				<?php
           			//***** busca tercero
					if($_POST['bt']=='1')
					{
			  			$nresul=buscatercero($_POST['tercero']);
						if($nresul!='')
						{
			  				$_POST['ntercero']=$nresul;
  							?>
							<script>
			  					document.getElementById('codingreso').focus();document.getElementById('codingreso').select();
							</script>
			  				<?php
			  			}
						else
						{
			  				$_POST['ntercero']="";
			  				?>
							<script>
								alert("Tercero Incorrecto o no Existe")				   		  	
								document.form2.tercero.focus();	
							</script>
			  				<?php
			  			}
			 		}
					//*** ingreso
					if($_POST['bin']=='1')
					{
			  			$nresul=buscaingreso($_POST['codingreso']);
						if($nresul!='')
						{
							$_POST['ningreso']=$nresul;
							?>
			  				<script>
			  					document.getElementById('valor').focus();document.getElementById('valor').select();
							</script>
			  				<?php
			  			}
						else
						{
			  				$_POST['codingreso']="";
			  				?>
			  				<script>alert("Codigo Ingresos Incorrecto");document.form2.codingreso.focus();</script>
			  				<?php
			  			}
			 		}
			 		?>
      
     				<div class="subpantalla estilos-scroll" style = 'height: 360px; resize: vertical;' >
						<table class="inicio">
							<tr>
								<td colspan="6" class="titulos">Detalle  Recaudos Transferencia</td>
							</tr>                  
							<tr>
								<td class="titulos2" style='width:5%;'>Codigo</td>
								<td class="titulos2" style='width:70%;'>Ingreso</td>
								<td class="titulos2" style='width:10%;'>Fuente</td>
								<td class="titulos2" style='width:10%;'>Centro costo</td>
								<td class="titulos2" style='width:10%;'>Valor</td>
								
							</tr>
							<?php 		
							if ($_POST['elimina']!='')
							{ 
								//echo "<TR><TD>ENTROS :".$_POST['elimina']."</TD></TR>";
								$posi=$_POST['elimina'];
								
								unset($_POST['dcoding'][$posi]);	
								unset($_POST['dncoding'][$posi]);			
								unset($_POST['dfuente'][$posi]); 
								unset($_POST['dcc'][$posi]); 
								unset($_POST['dvalores'][$posi]);			  		 
								$_POST['dcoding']= array_values($_POST['dcoding']); 		 
								$_POST['dncoding']= array_values($_POST['dncoding']); 	
								$_POST['dfuente']= array_values($_POST['dfuente']); 		 		 
								$_POST['dcc']= array_values($_POST['dcc']); 		 		 
								$_POST['dvalores']= array_values($_POST['dvalores']); 		
								$_POST['elimina'] = ''; 		 		 		 		 
							}	 
							if ($_POST['agregadet']=='1')
							{
								$_POST['dcoding'][]=$_POST['codingreso'];
								$_POST['dncoding'][]=$_POST['ningreso'];
								$_POST['dfuente'][]=$_POST['fuente'];			 		
								$_POST['dcc'][]=$_POST['cc'];			 		
								$_POST['dvalores'][]=$_POST['valor'];
								$_POST['agregadet']=0;
								?>
								<script>
										//document.form2.cuenta.focus();	
										document.form2.codingreso.value="";
										document.form2.fuente.value="";
										document.form2.nfuente.value="";
										document.form2.valor.value="";	
										document.form2.ningreso.value="";				
										document.form2.codingreso.select();
										document.form2.codingreso.focus();	
								</script>
								<?php
								
							}
							$_POST['totalc']=0;
							for ($x=0;$x<count($_POST['dcoding']);$x++)
							{		 
								echo "<tr>
										<td class='saludo1'>
											<input name='dcoding[]' class='inpnovisibles' value='".$_POST['dcoding'][$x]."' type='text' style='width:100%;'>
										</td>
										<td class='saludo1'>
											<input name='dncoding[]' class='inpnovisibles' value='".$_POST['dncoding'][$x]."' type='text' style='width:100%;'>
										</td>
										<td class='saludo1'>
											<input name='dfuente[]' class='inpnovisibles' value='".$_POST['dfuente'][$x]."' type='text' style='width:100%;'>
										</td>
										<td class='saludo1'>
											<input name='dcc[]' class='inpnovisibles' value='".$_POST['dcc'][$x]."' type='text' style='width:100%;'>
										</td>
										<td class='saludo1'>
											<input name='dvalores[]' class='inpnovisibles' value='".$_POST['dvalores'][$x]."' type='text' style='width:100%;'>
										</td>
										</tr>";
								$_POST['totalc']=$_POST['totalc']+$_POST['dvalores'][$x];
								$_POST['totalcf']=number_format($_POST['totalc'],2);
							}
							$resultado = convertir($_POST['totalc']);
							$_POST['letras']=$resultado." Pesos";
							echo "<tr>
									
										<td colspan = '4' style='font-size:18px; text-align: center; color:gray !important; ' class='saludo1'>TOTAL:</td>
										<td class='saludo1'>
											<input name='totalcf' class='inpnovisibles' type='text' value='$_POST[totalcf]' style='width:100%;'>
											<input name='totalc' class='inpnovisibles' type='hidden' value='$_POST[totalc]' style='width:100%;'>
										</td>
									</tr>
									<tr>
										<td class='saludo1'>Son:</td>
										<td>
											<input name='letras' type='text' class='inpnovisibles' value='$_POST[letras]' style='width:100%;' readonly>
										</td>
									</tr>";
							?> 
						</table>
					</div>
	  				<?php
					
					if($_POST['oculto']=='2')
					{
						preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST['fecha'], $fecha);
						$fechaf = $fecha[3]."-".$fecha[2]."-".$fecha[1];

						$bloq = bloqueos($_SESSION['cedulausu'], $fechaf);
						//echo $bloq;
						if($bloq >= 1)
						{
							$sqlrRecaudo = "SELECT * FROM tesorecaudotransferencia WHERE idcomp = ".$_POST['idcomp']." AND estado != 'N'";
							$resRecaudo = mysqli_query($linkbd_V7, $sqlrRecaudo);
							$rowRecaudo = mysqli_num_rows($resRecaudo);
							
							if($rowRecaudo == 0)
							{

								//*********************CREACION DEL COMPROBANTE CONTABLE ***************************
								$sqlr = "DELETE FROM comprobante_cab WHERE numerotipo='".$_POST['idcomp']."' AND tipo_comp = '28' ";
								mysqli_query($linkbd_V7, $sqlr);

								$sqlr = "DELETE FROM comprobante_det WHERE numerotipo = '".$_POST['idcomp']."' AND tipo_comp = '28'";
								mysqli_query($linkbd_V7, $sqlr);
								//***busca el consecutivo del comprobante contable
								
								if($_POST['causacion'] == '2')
								{
									$_POST['concepto'] = "ESTE DOCUMENTO NO REQUIERE CAUSACION CONTABLE - ".$_POST['concepto'];
								}

								//***cabecera comprobante
								$sqlr = "INSERT INTO comprobante_cab (numerotipo, tipo_comp, fecha, concepto, total, total_debito, total_credito, diferencia, estado) VALUES (".$_POST['idcomp'].", 28, '$fechaf', '".strtoupper($_POST['concepto'])."', 0, $_POST[totalc], $_POST[totalc], 0, '1')";

								if(mysqli_query($linkbd_V7, $sqlr))	
								{
									//echo "$sqlr <br>";	
								}
								else
								{
									echo "error:".mysqli_error($linkbd_V7);
								}
								
								$idcomp = mysqli_insert_id($linkbd_V7);
								//AQUI FINALIZA EL COMPROBANTE DE CABECERA CONTABILIDAD

								// AQUI INICIA EL PROCESO PARA GUARDAR EL ENCAMBEZADO DE TESORECAUDOTRANSFERENCIALIQUIDAR EN TESORERIA

								$sqlr = "DELETE FROM tesorecaudotransferencialiquidar WHERE id_recaudo = '".$_POST['idcomp']."'";

								mysqli_query($linkbd_V7, $sqlr);
			
								$sqlr = "DELETE FROM tesorecaudotransferencialiquidar_det WHERE id_recaudo = '".$_POST['idcomp']."' ";

								mysqli_query($linkbd_V7, $sqlr);

								
								//AQUI FINALIZA LA INSERCION DE LA CABECERA EN TESORERIA

								
								//******************* DETALLE DEL COMPROBANTE CONTABLE *********************
								for($x=0;$x<count($_POST['dcoding']);$x++)
								{
									//AQUI INICIA LA INSERCION DE EL DETALLE TESORERIA

									$sqlr = "INSERT INTO tesorecaudotransferencialiquidar_det (id_recaudo, ingreso, valor, estado, fuente, cc) VALUES (".$_POST['idcomp'].", '".$_POST['dcoding'][$x]."', ".$_POST['dvalores'][$x].", 'S', '".$_POST['dfuente'][$x]."', '".$_POST['dcc'][$x]."')";

									mysqli_query($linkbd_V7, $sqlr);

									//AQUI FINALIZA LA INSERCION DEL DETALLE TESORERIA

									if($_POST['causacion']!='2')
									{
										//***** BUSQUEDA INGRESO ********
										$sqlri = "SELECT concepto, porcentaje FROM tesoingresos_det WHERE codigo = '".$_POST['dcoding'][$x]."' AND vigencia = '".$_POST['vigencia']."' ";
										
										$resi = mysqli_query($linkbd_V7, $sqlri);
										
										while($rowi = mysqli_fetch_row($resi))
										{
											//**** busqueda concepto contable*****
											$cuentasContables = concepto_cuentasn2($rowi[0], 'C', 4, $_POST['cc'], "$fechaf");
											$porce = 0;
											
											foreach($cuentasContables as $cuentaCont)
											{
												$porce = $rowi[1];
												
												if($cuentaCont[1] == 'N')
												{
													if($cuentaCont[2] == 'S')
													{
														$valordeb = $_POST['dvalores'][$x]*($porce/100);
														$valorcred = 0;
													}
													if($cuentaCont[3] == 'S')
													{
														$valorcred = $_POST['dvalores'][$x]*($porce/100);
														$valordeb = 0;
													}
													$sqlr = "INSERT INTO comprobante_det (id_comp, cuenta, tercero, centrocosto, detalle, cheque, valdebito, valcredito, estado, vigencia) VALUES ('28 ".$_POST['idcomp']."', '".$cuentaCont[0]."', '".$_POST['tercero']."', '".$_POST['dcc'][$x]."', 'Recaudo Transferencia".strtoupper($_POST['dncoding'][$x])."', '', ".$valordeb.", ".$valorcred.", '1', '".$_POST['vigencia']."')";
													//echo $sqlr."<br>";
													mysqli_query($linkbd_V7, $sqlr);		   
												}
											}
										}
									}
								}

								$sqlr = "INSERT INTO tesorecaudotransferencialiquidar (id_recaudo, idcomp, fecha, vigencia, concepto, tercero, cc, valortotal, estado, medio_pago) VALUES (".$_POST['idcomp'].", ".$idcomp.", '$fechaf', ".$_POST['vigencia'].", '".strtoupper($_POST['concepto'])."', ".$_POST['tercero'].", '".$_POST['cc']."', ".$_POST['totalc'].", 'S', ".$_POST['medioDePago'].")";

								if (!mysqli_query($linkbd_V7, $sqlr))
								{
									//mysqli_query($linkbd_V7, $sqlr);
									$e = mysqli_error($linkbd_V7);
									echo"<script>despliegamodalm('visible','2','No se pudo ejecutar la petici�n: $e');</script>";
								}
								else
								{
									echo"<script>despliegamodalm('visible','3','Se ha almacenado el Recaudo con Exito');</script>";
								}


								//************ insercion de cabecera recaudos ************
							}
							else
							{
								echo "<script>despliegamodalm('visible','2',' El documento no se puede modificar porque ya tiene un ingreso de recaudo transferencia.');</script>";
							}
						}
						else
						{
							echo "<script>despliegamodalm('visible','2',' La fecha del documento es menor a la fecha de bloqueo.');</script>";
						}
					}
					?>	
					<div id="bgventanamodal2">
						<div id="ventanamodal2">
							<IFRAME  src="" name="buster" marginWidth=0 marginHeight=0 frameBorder=0 id="ventana2" frameSpacing=0 style="left:500px; width:900px; height:500px; top:200;">
							</IFRAME>
						</div>
					</div>
				</form>
 			</td>
		</tr>
	</body>
</html> 		