<?php
header("Cache-control: no-cache, no-store, must-revalidate");
header("Content-Type: text/html;charset=utf8");
require "comun.inc";
ini_set('max_execution_time',99999999);
require "funciones.inc";
require "conversor.php";
session_start();
$linkbd = conectar_v7();
$linkbd->set_charset("utf8");
cargarcodigopag($_GET['codpag'], $_SESSION["nivel"]);
date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE>
<html lang="es">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=9">
    <title>:: IDEAL 10 - Presupuesto</title>
    <link href="css/css2.css" rel="stylesheet" type="text/css" />
    <link href="css/css3.css" rel="stylesheet" type="text/css" />
    <link href="css/tabs.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="css/calendario.js"></script>
    <script type="text/javascript" src="css/programas.js"></script>
    <script type='text/javascript' src='JQuery/jquery-2.1.4.min.js'></script>
    <script>
        function validar(formulario) {
            document.form2.action = "ccp-buscarrp.php"; document.form2.submit();
        }

        function verUltimaPos(is, filas, filtro) {
            var scrtop = $('#divdet').scrollTop();
            var altura = $('#divdet').height();
            var numpag = $('#nummul').val();
            var limreg = $('#numres').val();
            if ((numpag <= 0) || (numpag == ""))
                numpag = 0;
            if ((limreg == 0) || (limreg == ""))
                limreg = 10;
            numpag++;
            location.href = "ccp-rpVisualizar.php?is=" + is + "&vig=" + vigusu;
        }

        function fundeshacer(iddesha) {
            if (confirm("Esta Seguro de Deshacer el RP No " + iddesha)) {
                document.getElementById('oculto').value = '4';
                document.getElementById('iddesh').value = iddesha;
                document.form2.submit();
            }
        }

        var ctrlPressed = false;
        var tecla01 = 16, tecla02 = 80, tecla03 = 81;
        $(document).keydown(
            function (e) {

                if (e.keyCode == tecla01) { ctrlPressed = true; }
                if (e.keyCode == tecla03) { tecla3Pressed = true; }
                if (ctrlPressed && (e.keyCode == tecla02) && tecla3Pressed) {

                    if (document.form2.iddeshff.value == "0") { document.form2.iddeshff.value = "1"; }
                    else { document.form2.iddeshff.value = "0"; }
                    document.form2.submit();
                }
            })
        $(document).keyup(function (e) { if (e.keyCode == tecla01) { ctrlPressed = false; } })
        $(document).keyup(function (e) {
            if (e.keyCode == tecla03) { tecla3Pressed = false; }
        })

        function selexcel() {
            tipocdp = document.form2.tabgroup1.value;
            switch (tipocdp) {
                case "1": document.form2.action = "ccp-buscarpexcel.php"; break;
                case "2": document.form2.action = "ccp-buscarpexcelr.php"; break;
            }
            document.form2.target = "_BLANK";
            document.form2.submit();
            document.form2.action = "";
            document.form2.target = "";
        }

        function selpdf() {
            tipocdp = document.form2.tabgroup1.value;
            switch (tipocdp) {
                case "1": document.form2.action = "ccp-buscarppdf.php"; break;
                case "2": document.form2.action = "ccp-buscarppdfr.php"; break;
            }
            document.form2.target = "_BLANK";
            document.form2.submit();
            document.form2.action = "";
            document.form2.target = "";
        }
    </script>

    <?php titlepag(); ?>

    <?php
    $scrtop = $_GET['scrtop'];
    if ($scrtop == "")
        $scrtop = 0;
    echo "<script>
				window.onload=function(){
					$('#divdet').scrollTop(" . $scrtop . ")
				}
			</script>";
    $gidcta = $_GET['idcta'];
    if (isset($_GET['filtro']))
        $_POST['nombre'] = $_GET['filtro'];
    ?>
</head>

<body>
    <IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
    <span id="todastablas2"></span>

    <table>
        <tr>
            <script>barra_imagenes("ccpet");</script>
            <?php cuadro_titulos(); ?>
        </tr>

        <tr>
            <?php menu_desplegable("ccpet"); ?>
        </tr>
    </table>
    <div class="bg-white group-btn p-1">
        <button type="button" onclick="location.href='ccp-rpbasico.php '" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
            <span>Nuevo</span>
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960">
                <path d="M440-440H200v-80h240v-240h80v240h240v80H520v240h-80v-240Z"></path>
            </svg>
        </button>
        <button type="button" onclick="document.form2.submit();" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
            <span>Buscar</span>
            <svg class="fill-black group-hover:fill-white w-5 h-5" xmlns="http://www.w3.org/2000/svg"
                viewBox="0 -960 960 960">
                <path d="M784-120 532-372q-30 24-69 38t-83 14q-109 0-184.5-75.5T120-580q0-109 75.5-184.5T380-840q109 0 184.5 75.5T640-580q0 44-14 83t-38 69l252 252-56 56ZM380-400q75 0 127.5-52.5T560-580q0-75-52.5-127.5T380-760q-75 0-127.5 52.5T200-580q0 75 52.5 127.5T380-400Z">
                </path>
            </svg>
        </button>
        <button type="button" onclick="window.open('ccp-principal');" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
            <span>Nueva ventana</span>
            <svg class="fill-black group-hover:fill-white w-5 h-5" xmlns="http://www.w3.org/2000/svg"
                viewBox="0 -960 960 960">
                <path
                    d="M320-120v-80h80v-80H160q-33 0-56.5-23.5T80-360v-400q0-33 23.5-56.5T160-840h640q33 0 56.5 23.5T880-760v400q0 33-23.5 56.5T800-280H560v80h80v80H320ZM160-360h640v-400H160v400Zm0 0v-400 400Z">
                </path>
            </svg>
        </button>
        <button type="button" onclick="selpdf()" class="btn btn-danger btn-danger-hover d-flex justify-between align-items-center">
            <span>Exportar PDF</span>
            <svg xmlns="http://www.w3.org/2000/svg"
                viewBox="0 0 512 512"><!-- !Font Awesome Free 6.5.1 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license/free Copyright 2024 Fonticons, Inc. -->
                <path
                    d="M64 464l48 0 0 48-48 0c-35.3 0-64-28.7-64-64L0 64C0 28.7 28.7 0 64 0L229.5 0c17 0 33.3 6.7 45.3 18.7l90.5 90.5c12 12 18.7 28.3 18.7 45.3L384 304l-48 0 0-144-80 0c-17.7 0-32-14.3-32-32l0-80L64 48c-8.8 0-16 7.2-16 16l0 384c0 8.8 7.2 16 16 16zM176 352l32 0c30.9 0 56 25.1 56 56s-25.1 56-56 56l-16 0 0 32c0 8.8-7.2 16-16 16s-16-7.2-16-16l0-48 0-80c0-8.8 7.2-16 16-16zm32 80c13.3 0 24-10.7 24-24s-10.7-24-24-24l-16 0 0 48 16 0zm96-80l32 0c26.5 0 48 21.5 48 48l0 64c0 26.5-21.5 48-48 48l-32 0c-8.8 0-16-7.2-16-16l0-128c0-8.8 7.2-16 16-16zm32 128c8.8 0 16-7.2 16-16l0-64c0-8.8-7.2-16-16-16l-16 0 0 96 16 0zm80-112c0-8.8 7.2-16 16-16l48 0c8.8 0 16 7.2 16 16s-7.2 16-16 16l-32 0 0 32 32 0c8.8 0 16 7.2 16 16s-7.2 16-16 16l-32 0 0 48c0 8.8-7.2 16-16 16s-16-7.2-16-16l0-64 0-64z">
                </path>
            </svg>
        </button>
        <button type="button" onclick="selexcel()" class="btn btn-success btn-success-hover d-flex justify-between align-items-center">
            <span>Exportar Excel</span>
            <svg class="fill-black group-hover:fill-white w-4 h-4" xmlns="http://www.w3.org/2000/svg"
                viewBox="0 0 384 512"><!--!Font Awesome Free 6.5.1 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license/free Copyright 2024 Fonticons, Inc. -->
                <path
                    d="M48 448V64c0-8.8 7.2-16 16-16H224v80c0 17.7 14.3 32 32 32h80V448c0 8.8-7.2 16-16 16H64c-8.8 0-16-7.2-16-16zM64 0C28.7 0 0 28.7 0 64V448c0 35.3 28.7 64 64 64H320c35.3 0 64-28.7 64-64V154.5c0-17-6.7-33.3-18.7-45.3L274.7 18.7C262.7 6.7 246.5 0 229.5 0H64zm90.9 233.3c-8.1-10.5-23.2-12.3-33.7-4.2s-12.3 23.2-4.2 33.7L161.6 320l-44.5 57.3c-8.1 10.5-6.3 25.5 4.2 33.7s25.5 6.3 33.7-4.2L192 359.1l37.1 47.6c8.1 10.5 23.2 12.3 33.7 4.2s12.3-23.2 4.2-33.7L222.4 320l44.5-57.3c8.1-10.5 6.3-25.5-4.2-33.7s-25.5-6.3-33.7 4.2L192 280.9l-37.1-47.6z">
                </path>
            </svg>
        </button>
        <button type="button" onclick="location.href='#'"
            class="btn btn-success btn-success-hover d-flex justify-between align-items-center">
            <span>Atras</span>
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960">
                <path
                    d="M760-200v-160q0-50-35-85t-85-35H273l144 144-57 56-240-240 240-240 57 56-144 144h367q83 0 141.5 58.5T840-360v160h-80Z">
                </path>
            </svg>
        </button>
    </div>

    <form name="form2" method="post" action="ccp-buscarrp.php">
        <?php
        if ($_POST['oculto'] == "") {
            $_POST['iddeshff'] = 0;
            $_POST['tabgroup1'] = 1;
        }
        switch ($_POST['tabgroup1']) {
            case 1:
                $check1 = 'checked';
                break;
            case 2:
                $check2 = 'checked';
                break;
            case 3:
                $check3 = 'checked';
                break;
        }
        ?>

        <table width="100%" align="center" class="inicio">
            <tr>
                <td class="titulos" colspan="9">:: Buscar .: Registro Presupuestal</td>
                <td class="cerrar" style='width:7%' onClick="location.href='ccp-principal.php'">Cerrar</td>

                <input type="hidden" name="oculto" id="oculto" value="1">
                <input type="hidden" name="iddeshff" id="iddeshff" value="<?php echo $_POST['iddeshff']; ?>">
            </tr>
            <tr>
                <td class="saludo1">Vigencia:</td>
                <td>
                    <input type="search" name="vigencia" value="<?php echo $_POST['vigencia'] ?>"
                        onKeyUp="return tabular(event,this)" />
                </td>

                <td class="saludo1">Numero:</td>
                <td>
                    <input type="search" name="numero" id="numero" value="<?php echo $_POST['numero'] ?>"
                        onKeyUp="return tabular(event,this)" />
                </td>

                <td class="saludo1">Fecha Inicial: </td>
                <td>
                    <input type="search" name="fechaini" id="fc_1198971545" title="DD/MM/YYYY"
                        value="<?php echo $_POST['fechaini']; ?>" onKeyUp="return tabular(event,this)"
                        onKeyDown="mascara(this,'/',patron,true)" maxlength="10">&nbsp;<img
                        src="imagenes/calendario04.png" style="width:20px"
                        onClick="displayCalendarFor('fc_1198971545');" class="icobut" title="Calendario">
                </td>

                <td class="saludo1">Fecha Final: </td>
                <td>
                    <input type="search" name="fechafin" id="fc_1198971546" title="DD/MM/YYYY"
                        value="<?php echo $_POST['fechafin']; ?>" onKeyUp="return tabular(event,this) "
                        onKeyDown="mascara(this,'/',patron,true)" maxlength="10">&nbsp;<img
                        src="imagenes/calendario04.png" style="width:20px"
                        onClick="displayCalendarFor('fc_1198971546');" class="icobut" title="Calendario">
                </td>

                <td>
                    <input type="button" name="bboton" onClick="document.form2.submit();"
                        value="&nbsp;&nbsp;Buscar&nbsp;&nbsp;" />
                </td>
            </tr>
        </table>

        <input type="hidden" name="iddesh" id="iddesh" value="<?php echo $_POST['iddesh']; ?>" />
        <?php
        $vigusu = vigencia_usuarios($_SESSION['cedulausu']);
        if ($_POST['oculto'] == '4') {
            if ($_POST['vigencia'] != '') {
                $vigusu = $_POST['vigencia'];
            }

            $sqlr = "SELECT * FROM ccpetrp WHERE idcdp='$_POST[iddesh]' AND vigencia='$vigusu'";
            $resp = mysqli_query($linkbd, $sqlr);
            $row = mysqli_fetch_row($resp); {
                $idrp = $row[1];
                $sqlr = "SELECT * FROM ccpetrp_detalle WHERE consvigencia='$_POST[iddesh]' AND vigencia='$vigusu'";
                $resp2 = mysqli_query($linkbd, $sqlr);
                while ($row2 = mysqli_fetch_row($resp2)) {
                    $sqlr = "UPDATE ccpet_cuentasccpet_inicial SET saldos= saldos + $row2[5] WHERE cuenta='$row2[3]' AND vigencia='$vigusu'";
                    mysqli_query($linkbd, $sqlr);

                    $sqlr = "UPDATE ccpet_cuentasccpet_inicial SET saldoscdprp= saldoscdprp + $row2[5] WHERE cuenta='$row2[3]' AND vigencia='$vigusu'";
                    mysqli_query($linkbd, $sqlr);
                }

                $sqlr = "DELETE FROM ccpet_rp WHERE consvigencia='$_POST[iddesh]' AND vigencia='$vigusu'";
                mysqli_query($linkbd, $sqlr);

                $sqlr = "DELETE FROM ccpet_rp WHERE idcdp='$_POST[iddesh]' AND vigencia='$vigusu'";
                mysqli_query($linkbd, $sqlr);
            }

            $sqlr = "DELETE FROM ccpetrp_detalle WHERE consvigencia='$_POST[iddesh]' AND vigencia='$vigusu'";
            mysqli_query($linkbd, $sqlr);

            $sqlr = "DELETE FROM humnom_rp WHERE consvigencia='$idrp' AND vigencia='$vigusu'";
            mysqli_query($linkbd, $sqlr);

            $sqlr = "UPDATE hum_nom_cdp_rp SET rp='0', cdp='0' WHERE rp='$idrp' AND cdp='$_POST[iddesh]' AND vigencia='$vigusu'";
            mysqli_query($linkbd, $sqlr);
        }
        $crit1 = " ";
        $crit2 = " ";
        $crit3 = " ";
        $crit4 = " ";
        $crit5 = " ";

        if ($_POST['fechaini'] != '') {
            $partesFecha = explode('/', $_POST['fechaini']);
            $_POST['vigencia'] = $partesFecha[2];
        }

        if ($_POST['vigencia'] != "") {
            $crit1 = " AND TB1.vigencia ='$_POST[vigencia]' ";
        } else {
            $crit1 = " AND TB1.vigencia ='$vigusu' ";
        }

        if ($_POST['numero'] != "") {
            $crit2 = " AND TB1.consvigencia like '%$_POST[numero]%' ";
        }

        if ($_POST['fechaini'] != "" and $_POST['fechafin'] != "") {
            preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST['fechaini'], $fecha);
            $fechai = $fecha[3] . "-" . $fecha[2] . "-" . $fecha[1];
            preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST['fechafin'], $fecha);
            $fechaf = $fecha[3] . "-" . $fecha[2] . "-" . $fecha[1];
            $crit3 = " AND TB1.fecha between '$fechai' and '$fechaf'  ";
        }
        ?>
        <div class="tabsmeci" style="height:64.5%; width:99.6%;">
            <div class="tab">
                <input type="radio" id="tab-1" name="tabgroup1" value="1" <?php echo $check1; ?>>
                <label for="tab-1">RP General</label>
                <div class="content" style="overflow-x:hidden;">
                    <?php
                    $sqlr = "SELECT TB1.* FROM ccpetrp TB1 WHERE TB1.tipo_mov='201' $crit1 $crit2 $crit3  ORDER BY TB1.consvigencia DESC";
                    $resp = mysqli_query($linkbd, $sqlr);
                    $ntr = mysqli_num_rows($resp);

                    $con = 1;
                    if ($_POST['iddeshff'] == 1) {
                        $tff01 = 9;
                        $tff02 = 10;
                    } else {
                        $tff01 = 9;
                        $tff02 = 9;
                    }

                    echo "
                            <table class='inicio' align='center'>
                                <tr><td colspan='$tff01' class='titulos'>.: Resultados Busqueda:</td></tr>
                                <tr><td colspan='$tff02'>Certificado Registro Presupuestal Encontrados: $ntr</td></tr>
                                <tr>
                                    <td class='titulos2' style='width:5%'>Vigencia</td>
                                    <td class='titulos2' style='width:4%'>Numero RP</td>
                                    <td class='titulos2' style='width:4%'>CDP</td>
                                    <td class='titulos2' >Objeto</td>
                                    <td class='titulos2' >Tercero</td>
                                    <td class='titulos2'>Valor</td>
                                    <td class='titulos2' style='width:7%'>Fecha</td>
                                    <td class='titulos2' style='width:4%'>Estado</td>";
                    if (strtoupper($_SESSION["perfil"]) == "SUPERMAN" && $_POST['iddeshff'] == 1) {
                        echo "<td class='titulos2' style='width:4%'>Deshacer</td>";
                    }

                    echo "<td class='titulos2' style='width:4%'>Ver</td></tr>";

                    $iter = 'zebra1';
                    $iter2 = 'zebra2';
                    $filas = 1;
                    while ($row = mysqli_fetch_row($resp)) {
                        switch ($row[3]) {
                            case "S":
                                $estado = 'Activo';
                                $imgsem = "src='imagenes/sema_verdeON.jpg' title='Activo'";
                                break;
                            case "N":
                                $estado = 'Anulado';
                                $imgsem = "src='imagenes/sema_verdeR.jpg' title='Anulado'";
                                break;
                            case "C":
                                $estado = 'Completo';
                                $imgsem = "src='imagenes/sema_amarilloON.jpg' title='Completo'";
                                break;
                            case "R":
                                $estado = 'Reversado';
                                $imgsem = "src='imagenes/sema_rojoON.jpg' title='Reversado'";
                                break;
                        }
                        if ($gidcta != "") {
                            if ($gidcta == $row[0]) {
                                $estilo = 'background-color:#FF9';
                            } else {
                                $estilo = "";
                            }
                        } else {
                            $estilo = "";
                        }
                        $idcta = "'$row[0]'";
                        $is = "'$row[2]'";
                        $numfil = "'$filas'";
                        echo "
                                <input type='hidden' name='vigenciaD[]' value='$row[0]' />
                                <input type='hidden' name='numRp[]' value='$row[1]' />
                                <input type='hidden' name='cdp[]' value='$row[2]' />
                                <input type='hidden' name='objeto[]' value='$row[11]' />
                                <input type='hidden' name='tercero[]' value='$row[5] - " . buscatercero($row[5]) . "' />
                                <input type='hidden' name='valor[]' value='$" . number_format($row[6], 2) . "' />
                                <input type='hidden' name='fecha[]' value=' ". date('d-m-Y', strtotime($row[4])) . "' />
                                <input type='hidden' name='estado[]' value='$estado' />

                                <tr class='$iter' onMouseOver=\"anterior=this.style.backgroundColor;this.style.backgroundColor='#40b3ff';\"
			                    onMouseOut=\"this.style.backgroundColor=anterior\" onDblClick=\"location.href='ccp-rpVisualizar.php?is=$row[1]&vig=$row[0]'\" style='text-transform:uppercase; $estilo' >
                                <td>$row[0]</td>
                                <td>$row[1]</td>
                                <td>$row[2]</td>

                                <td>$row[11]</td>
                                <td>$row[5] - " . buscatercero($row[5]) . "</td>
                                <td style='text-align:right;'>$" . number_format($row[6], 2) . "</td>

                                <td style='text-align:center;'>" . date('d-m-Y', strtotime($row[4])) . "</td>
                                <td style='text-align:center;'><img $imgsem style='width:18px'/></td>";
                        if (strtoupper($_SESSION["perfil"]) == "SUPERMAN" && $_POST['iddeshff'] == 1) {
                            echo "<td style='text-align:center;'><img src='imagenes/flechades.png' title='Deshacer No: $row[2]' style='width:18px;cursor:pointer;' onClick=\"fundeshacer('$row[2]')\" /></td>";
                        }
                        echo "
                                    <td style='text-align:center;'><img src='imagenes/lupa02.png' style='width:20px' class='icoop' title='Ver' onClick=\"location.href='ccp-rpVisualizar.php?is=$row[1]&vig=$row[0]'\"/></td>
                                </tr>";
                        $con += 1;
                        $aux = $iter;
                        $iter = $iter2;
                        $iter2 = $aux;
                    }
                    echo "</table>";
                    ?>
                </div>
            </div>
            <div class="tab">
                <input type="radio" id="tab-2" name="tabgroup1" value="2" <?php echo $check2; ?>>
                <label for="tab-2">RP Reversados</label>
                <div class="content" style="overflow-x:hidden;">
                    <?php
                    $sqlr = "SELECT TB1.* FROM ccpetrp TB1 WHERE TB1.tipo_mov LIKE '4%' $crit1 $crit2 $crit3 and estado = 'R' ORDER BY TB1.consvigencia DESC";

                    $resp = mysqli_query($linkbd, $sqlr);
                    $ntr = mysqli_num_rows($resp);
                    $con = 1;
                    if ($_POST['iddeshff'] == 1) {
                        $tff01 = 11;
                        $tff02 = 12;
                    } else {
                        $tff01 = 10;
                        $tff02 = 11;
                    }
                    echo "
                    <table class='inicio' align='center'>
                        <tr><td colspan='$tff01' class='titulos'>.: Resultados Busqueda:</td></tr>
                        <tr><td colspan='$tff02'>Certificado Registro Presupuestal Encontrados: $ntr</td></tr>
                        <tr>
                            <td class='titulos2' style='width:5%'>Vigencia</td>
                            <td class='titulos2' style='width:4%'>Numero RP</td>
                            <td class='titulos2' style='width:4%'>CDP</td>
                            <td class='titulos2' >Objeto</td>
                            <td class='titulos2' >Tercero</td>
                            <td class='titulos2'>Valor</td>
                            <td class='titulos2' style='width:7%'>Fecha</td>
                            <td class='titulos2' style='width:4%'>Estado</td>";
                    if (strtoupper($_SESSION["perfil"]) == "SUPERMAN" && $_POST['iddeshff'] == 1) {
                        echo "<td class='titulos2' style='width:4%'>Deshacer</td>";
                    }

                    echo "<td class='titulos2' style='width:4%'>Ver</td></tr>";
                    $iter = 'zebra1';
                    $iter2 = 'zebra2';
                    while ($row = mysqli_fetch_row($resp)) {
                        $sqlr1 = "SELECT sum(valor) from ccpetrp_detalle where consvigencia=$row[1] and vigencia=$row[0] AND (tipo_mov='401' OR tipo_mov='402')";

                        $resp1 = mysqli_query($linkbd, $sqlr1);
                        $row1 = mysqli_fetch_row($resp1);
                        echo "

                                <tr class='$iter' onMouseOver=\"anterior=this.style.backgroundColor;this.style.backgroundColor='#40b3ff';\" onMouseOut=\"this.style.backgroundColor=anterior\" onDblClick=\"location.href='ccp-rpVisualizar.php?is=$row[1]&vig=$row[0]'\" style='text-transform:uppercase; $estilo' >
                                    <td>$row[0]</td>
                                    <td>$row[1]</td>
                                    <td>$row[2]</td>
                                    <td>$row[11]</td>
                                    <td>$row[5] - " . buscatercero($row[5]) . "</td>
                                    <td style='text-align:right;'>$" . number_format($row1[0], 2) . "</td>

                                    <td style='text-align:center;'>" . date('d-m-Y', strtotime($row[4])) . "</td>
                                    <td style='text-align:center;'><img src='imagenes/sema_rojoON.jpg' title='Reversado' style='width:18px'/></td>";
                                    if (strtoupper($_SESSION["perfil"]) == "SUPERMAN" && $_POST['iddeshff'] == 1) {
                                        echo "<td style='text-align:center;'><img src='imagenes/flechades.png' title='Deshacer No: $row[2]' style='width:18px;cursor:pointer;' onClick=\"fundeshacer('$row[2]')\" /></td>";
                                    }
                                    echo "
                                        <td style='text-align:center;'><img src='imagenes/lupa02.png' style='width:20px' class='icoop' title='Ver' onClick=\"location.href='ccp-rpVisualizar.php?is=$row[1]&vig=$row[0]'\"/></td>
                                </tr>
                                <input type='hidden' name='vigenciaD[]' value='$row[0]' />
                                <input type='hidden' name='numRp[]' value='$row[1]' />
                                <input type='hidden' name='cdp[]' value='$row[2]' />
                                <input type='hidden' name='objeto[]' value='$row[11]' />
                                <input type='hidden' name='tercero[]' value='$row[5] - " . buscatercero($row[5]) . "' />
                                <input type='hidden' name='valor[]' value='$" . number_format($row1[0], 2) . "' />
                                <input type='hidden' name='fecha[]' value=' ". date('d-m-Y', strtotime($row[4])) . "' />
                                <input type='hidden' name='estado[]' value='R' />
                                ";
                        $con += 1;
                        $aux = $iter;
                        $iter = $iter2;
                        $iter2 = $aux;
                    }
                    echo "</table>";
                    ?>
                </div>
            </div>
        </div>
    </form>

    <script>
        
    </script>
</body>

</html>
