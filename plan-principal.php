<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require "comun.inc";
	require "funciones.inc";
	session_start();
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
	if (!isset($_SESSION["usuario"]))//*** verificar el username y el pass
	{
		$users = $_POST['user'];
		$pass = $_POST['pass'];
		$sqlr = "SELECT T1.nom_usu, T2.nom_rol, T1.id_rol, T1.id_usu, T1.foto_usu, T1.usu_usu FROM usuarios AS T1 INNER JOIN roles AS T2 ON T1.id_rol = T2.id_rol WHERE T1.usu_usu = '$users' AND T1.pass_usu = '$pass' AND T1.est_usu = '1'";
		$res = mysqli_query($linkbd,$sqlr);
		while($r = mysqli_fetch_row($res))
		{
			$user = $r[0];
			$perf = $r[1];
			$niv = $r[2];
			$idusu = $r[3];
			$nick = $r[5];
			$dirfoto = $r[4];
		}
		if ($user == "")//login incorrecto
		{
			header("location: index2.php");
		}
		else //login correcto
		{
			$_SESSION["usuario"] = array();
			$_SESSION["usuario"] = $user;
			$_SESSION["perfil"] = $perf;
			$_SESSION["idusuario"] =$idusu;
			$_SESSION["nickusu"] = $nick;
			$_SESSION["nivel"] = $niv;
			$_SESSION["linksetac"]=array();
			if($dirfoto == ""){$dirfoto = "blanco.png";}
			$_SESSION["fotousuario"] = "fotos/".$dirfoto;
			//******************* menuss ************************
			$sqlr = "SELECT DISTINCT (T2.nom_opcion), T2.ruta_opcion, T2.niv_opcion, T2.comando FROM rol_priv AS T1 INNER JOIN opciones AS T2 ON T2.id_opcion = T1.id_opcion WHERE T1.id_rol = '$niv' GROUP BY (T2.nom_opcion), T2.ruta_opcion, T2.niv_opcion, T2.comando ORDER BY T2.orden";
			$linksetac[$x]="";
			$res = mysqli_query($linkbd,$sqlr);
			while($roww = mysqli_fetch_row($res))
			{
				$_SESSION['linksetac'][$roww[2]].='<li> <a onClick="location.href=\''.$roww[1].'\'" style="cursor:pointer;">'.$roww[0].' <span style="float:right">'.$roww[3].'</span></a></li>';
			}
		}
	}
	else
	{
		$_SESSION["linksetpl"]=array();
		$niv = $_SESSION["nivel"];
		$sqlr="SELECT DISTINCT (T2.nom_opcion), T2.ruta_opcion, T2.niv_opcion, T2.comando FROM rol_priv AS T1 INNER JOIN opciones AS T2 ON T2.id_opcion = T1.id_opcion WHERE T1.id_rol = '$niv' AND T2.modulo = '9' GROUP BY (T2.nom_opcion), T2.ruta_opcion, T2.niv_opcion, T2.comando ORDER BY T2.orden";
		$linksetac[$x]="";
		$res = mysqli_query($linkbd,$sqlr);
		while($roww = mysqli_fetch_row($res))
		{
			$_SESSION['linksetpl'][$roww[2]].='<li> <a onClick="location.href=\''.$roww[1].'\'" style="cursor:pointer;">'.$roww[0].' <span style="float:right">'.$roww[3].'</span></a></li>';
		}
	}
?>
<!doctype >
<html lang="es">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=9">
		<title>:: IDEAL 10 - Planeacion Estrategica</title>
		<link href="css/css2.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css">
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css">
		<script type="text/javascript" src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
        <link href="css/style.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
		<?php titlepag();?>
	</head>
	<body>
		<IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
		<span id="todastablas2"></span>
		<table>
			<tr><script>barra_imagenes("plan");</script><?php cuadro_titulos();?></tr>
			<tr><?php menu_desplegable("plan");?></tr>
		</table>
	</body>
</html>
