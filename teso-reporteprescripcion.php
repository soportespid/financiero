<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");

	require "comun.inc";
	require "funciones.inc";
	require "conversor.php";
	require "validaciones.inc";

	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");

	session_start();
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE > 
<html lang="es">
	<head>
		<meta charset="utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1"/>
		<meta name="viewport" content="user-scalable=no">
		<title>:: IDEAL 10 - Tesorer&iacute;a</title>
		<link href="favicon.ico" rel="shortcut icon"/>
        <script type="text/javascript" src="jquery-1.11.0.min.js"></script>

		<script>
			//************* ver reporte ************
			//***************************************
			function verep(idfac)
			{
				document.form1.oculto.value=idfac;
				document.form1.submit();
  			}

			//************* genera reporte ************
			//***************************************
			function genrep(idfac)
			{
			document.form2.oculto.value=idfac;
			document.form2.submit();
			}

			function buscacta(e)
			{
			if (document.form2.cuenta.value!="")
			{
			document.form2.bc.value='1';
			document.form2.submit();
			}
			}
			function validar()
			{
			document.form2.submit();
			}
			function buscater(e)
			{
			if (document.form2.tercero.value!="")
			{
			document.form2.bt.value='1';
			document.form2.submit();
			}
			}
			function agregardetalle()
			{
			if(document.form2.numero.value!="" &&  document.form2.valor.value>0 &&  document.form2.banco.value!=""  )
			{ 
						document.form2.agregadet.value=1;
			//			document.form2.chacuerdo.value=2;
						document.form2.submit();
			}
			else {
			alert("Falta informacion para poder Agregar");
			}
			}
			//************* genera reporte ************
			//***************************************
			function eliminar(idr)
			{
			if (confirm("Esta Seguro de Eliminar el Recibo de Caja"))
			{
			document.form2.oculto.value=2;
			document.form2.var1.value=idr;
			document.form2.submit();
			}
			}
			//************* genera reporte ************
			//***************************************
			function guardar()
			{

			if (document.form2.fecha.value!='')
			{
			if (confirm("Esta Seguro de Guardar"))
			{
			document.form2.oculto.value=2;
			document.form2.submit();
			}
			}
			else{
			alert('Faltan datos para completar el registro');
			document.form2.fecha.focus();
			document.form2.fecha.select();
			}
			}
			function pdf()
			{
			document.form2.action="teso-pdfconsignaciones.php";
			document.form2.target="_BLANK";
			document.form2.submit(); 
			document.form2.action="";
			document.form2.target="";
			}
            $(window).load(function () {
				$('#cargando').hide();
			});
		</script>
		<script src="css/programas.js"></script>
		<script src="css/calendario.js"></script>
		<link href="css/css2.css" rel="stylesheet" type="text/css" />
		<link href="css/css3.css" rel="stylesheet" type="text/css" />
	</head>
	<body>
        <div id="cargando" style=" position:absolute;left: 46%; bottom: 45%">
			<img src="imagenes/loading.gif" style=" width: 80px; height: 80px"/>
		</div>
		<IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
			<span id="todastablas2"></span>
			<table>
				<tr><script>barra_imagenes("teso");</script><?php cuadro_titulos();?></tr>	 
				<tr><?php menu_desplegable("teso");?></tr>
				<tr>
				<td colspan="3" class="cinta">
				<a href="#" class="mgbt"><img src="imagenes/add.png" alt="Nuevo" title="Nuevo"/></a>
				<a href="#" class="mgbt"><img src="imagenes/guardad.png" alt="Guardar" title="Guardar"/> 
				<a onClick="document.form2.submit();" href="#" class="mgbt"><img src="imagenes/busca.png" alt="Buscar" title="Buscar" /></a> 
				<a href="<?php echo "archivos/".$_SESSION['usuario']."-reporteprescripcion.csv"; ?>" download="<?php echo "archivos/".$_SESSION['usuario']."-reporteprescripcion.csv"; ?>" target="_blank" class="mgbt"><img src="imagenes/csv.png"  alt="csv" title="csv"></a>
				<a href="#" onClick="mypop=window.open('teso-principal.php','','');mypop.focus();" class="mgbt"><img src="imagenes/nv.png" alt="nueva ventana" title="Nueva Ventana"></a>
				<a href="teso-informestesoreria.php" class="mgbt"><img src="imagenes/iratras.png" title="Atr&aacute;s"></a></td></tr>	
				</table>
				<tr>
					<td colspan="3" class="tablaprin"> 
 						<form name="form2" method="post" action="teso-reporteprescripcion.php">
							<?php
								if($_POST['conanul']=='S')
								{
									$chkant=' checked ';
								}
							?>
							<table  class="inicio" align="center" >
							<tr >
								<td class="titulos" colspan="7">:. Buscar Prescipcion Predial</td>
								<td width="70" class="cerrar" ><a href="teso-principal.php">Cerrar</a></td>
							</tr>
      						<tr>
         						<td  class="saludo1">Fecha Inicial:</td>
								<td><input type="hidden" value="<?php echo $ $vigusu ?>" name="vigencias"><input id="fc_1198971545" title="DD/MM/YYYY" name="fecha" type="text" value="<?php echo $_POST['fecha']?>" maxlength="10" size="10" onchange="" onKeyPress="javascript:return solonumeros(event)" onKeyUp="return tabular(event,this)" onKeyDown="mascara(this,'/',patron,true)"><a href="#" onClick="displayCalendarFor('fc_1198971545');"><img src="imagenes/calendario04.png" style="width:20px" align="absmiddle" border="0"></a></td>
								<td class="saludo1">Fecha Final: </td>
								<td ><input id="fc_1198971546" title="DD/MM/YYYY" name="fecha2" type="text" value="<?php echo $_POST['fecha2']?>" maxlength="10" size="10" onchange="" onKeyPress="javascript:return solonumeros(event)" onKeyUp="return tabular(event,this)" onKeyDown="mascara(this,'/',patron,true)">   
											<a href="#" onClick="displayCalendarFor('fc_1198971546');"><img src="imagenes/calendario04.png" style="width:20px" align="absmiddle" border="0"></a> 
								</td>
								
								<td>
									<input type="button" name="generar" value="Generar" onClick="document.form2.submit()">
									<input name="oculto" type="hidden" value="1"><input name="var1" type="hidden" value=<?php echo $_POST['var1'];?>></td>
								</td>
        					</tr>                       
    					</table>    
					</form> 
					<div class="subpantallap">
   					<?php
	
						$oculto=$_POST['oculto'];
						if($_POST['oculto'])
						{
	
							
                            $crit1=" ";
                            $crit2=" ";
                            $fechaf=$_POST['fecha'];
                            $fechaf2=$_POST['fecha2'];
                            preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST['fecha'],$fecha1);
                            /* ereg( "([0-9]{1,2})/([0-9]{1,2})/([0-9]{2,4})", $_POST['fecha'],$fecha1); */
                            $fechaf=$fecha1[3]."-".$fecha1[2]."-".$fecha1[1];
                            /* ereg( "([0-9]{1,2})/([0-9]{1,2})/([0-9]{2,4})", $_POST['fecha2'],$fecha2); */
                            preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST['fecha2'],$fecha2);
                            $fechaf2=$fecha2[3]."-".$fecha2[2]."-".$fecha2[1];
                            
						    $sqlr="SELECT * FROM tesoprescripciones,tesoprescripciones_det WHERE tesoprescripciones.estado<>'' AND tesoprescripciones.fecha BETWEEN '$fechaf' AND '$fechaf2' AND tesoprescripciones.id = tesoprescripciones_det.id ORDER BY tesoprescripciones.id ASC";
                            //echo "<div><div>sqlr:".$sqlr."</div></div>";
                            $resp = mysqli_query($linkbd, $sqlr);
                            $ntr = mysqli_num_rows($resp);
                            $con=1;

                            $namearch="archivos/".$_SESSION['usuario']."-reporteprescripcion.csv";
                            $Descriptor1 = fopen($namearch,"w+"); 
                            fputs($Descriptor1,"No PRESCRIPCION;RESOLUCION;CODIGO CATASTRAL;VIGENCIA;FECHA;AVALUO;TASA;PREDIAL;BOMBERIL;MEDIO AMBIENTE;TOTAL\r\n");
						    echo "<table class='inicio' align='center' >
							<tr>
								<td colspan='17' class='titulos'>.: Resultados Busqueda:</td>
							</tr>
							<tr>
								<td colspan='4'>Prescripciones encontradas Encontrados: $ntr</td>
							</tr>
							<tr>
								<td class='titulos2'>No Prescripcion</td>
								<td class='titulos2' style='width:5%;'>Resolucion</td>
								<td class='titulos2' style='width:10%;'>Codigo catastral</td>
								<td class='titulos2' style='width:20%;'>Direccion</td>
								<td class='titulos2' style='width:10%;'>Vigencia</td>
								<td class='titulos2'>Fecha</td>
								<td class='titulos2'>Avaluo</td>

								<td class='titulos2'>Tasa</td>
								<td class='titulos2'>Pedrial</td>
								<td class='titulos2'>Bomberil</td>
								<td class='titulos2'>Medio Ambiente</td>
								<td class='titulos2'>Total</td>
							</tr>";	
							//echo "nr:".$nr;
							$iter='saludo1';
							$iter2='saludo2';

                            $sqlr2="SELECT porcentaje FROM tesoingresos_predial_det WHERE codigo='01' AND modulo='4' AND  estado='S' AND concepto='26' AND vigencia=(SELECT MAX(vigencia) FROM tesoingresos_predial_det WHERE codigo = '01' AND modulo='4' and  estado='S' AND concepto='26')";
							$res3=mysqli_query($linkbd,$sqlr2);
							$r3=mysqli_fetch_row($res3);

							while ($row =mysqli_fetch_row($resp)) 
							{
                                $predial = 0;
                                $bomberil = 0;
                                $total = 0;
                                $sqlrPredialAvaluo = "SELECT tasa, tasa_bomberil FROM tesoprediosavaluos WHERE codigocatastral = '$row[3]' AND vigencia = '$row[6]'";
								$resPredialAvaluo = mysqli_query($linkbd, $sqlrPredialAvaluo);
								$rowPredialAvaluo = mysqli_fetch_row($resPredialAvaluo);

                                $predial = $row[7] * $rowPredialAvaluo[0] / 1000;

                                if($rowPredialAvaluo[1] > 0){
                                    $bomberil = $predial * $rowPredialAvaluo[1] / 100;
                                }else{
                                    $bomberil = $predial * $r3[0] / 100;
                                }

                                $total = $predial + $bomberil;

                                $sqlrDir = "SELECT direccion FROM tesopredios WHERE cedulacatastral = '$row[3]'";
                                $resDir = mysqli_query($linkbd, $sqlrDir);
                                $rowDir = mysqli_fetch_row($resDir);

								echo "<tr>
										<td class='$iter'>".$row[0]."</td>
										<td class='$iter'>".$row[2]."</td>
										<td class='$iter'>".$row[3]."</td>
										<td class='$iter'>".$rowDir[0]."</td>
										<td class='$iter'>".$row[6]."</td>
										<td class='$iter'>".$row[1]."</td>
										<td class='$iter'>".$row[7]."</td>
										<td class='$iter'>".number_format($rowPredialAvaluo[0],2)."</td>
										<td class='$iter'>".number_format($predial,2)."</td>
										<td class='$iter'>".number_format($bomberil,2)."</td>
										<td class='$iter'>0</td>
										<td class='$iter'>$ ".number_format($total,2)."</td>
									</tr>";	
								$con+=1;
								$aux=$iter;
								$iter=$iter2;
								$iter2=$aux;
								fputs($Descriptor1,$row[0].";'".$row[2]."';'".$row[3]."';".$row[6].";".$row[1].";".$row[7].";".round($rowPredialAvaluo[0],0).";".round($predial,0).";".round($bomberil,0).";".round(0,0).";".round($total,0)."\r\n");	
 							}
 							echo"</table>";
						}
?></div>
</td></tr>     
</table>
</body>
</html>