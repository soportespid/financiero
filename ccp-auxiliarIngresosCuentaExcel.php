<?php
    require_once 'PHPExcel/Classes/PHPExcel.php';
	require 'comun.inc';
	require "funciones.inc";
    require 'funcionesSP.inc.php';
    require 'conversor.php';
	date_default_timezone_set("America/Bogota");
	session_start();
    class Plantilla{
        private $linkbd;
        public function __construct() {
            $this->linkbd = conectar_v7();
            $this->linkbd->set_charset("utf8");
        }
        /******************Funciones para generar informe********************* */
        public function getInfo(){
            if(!empty($_SESSION)){
                $arrData = [];
                    $strCuenta = $_GET['cuenta'];
                    $strFuente = $_GET['fuente'];
                    $strFechaInicial = $_GET['fecha_inicial'];
                    $strFechaFinal = $_GET['fecha_final'];
                    $intVigencia = $_GET['vigencia'];
                    $strSeccion = $_GET['seccion'];
                    $strPago = $_GET['pago'];
                    $intCpc = $_GET['cpc'];
                    $arrInicial = $this->selectInicial($strCuenta,$strSeccion,$intVigencia,$strPago,$strFuente,$strFechaInicial,$strFechaFinal);
                    $arrAdicion = $this->selectAdicion($strCuenta,$strSeccion,$intVigencia,$strPago,$strFuente,$strFechaInicial,$strFechaFinal);
                    $arrReduccion = $this->selectReduccion($strCuenta,$strSeccion,$intVigencia,$strPago,$strFuente,$strFechaInicial,$strFechaFinal);
                    $arrReservas = $this->selectReservas($strCuenta,$strSeccion,$intVigencia,$strPago,$strFuente,$strFechaInicial,$strFechaFinal);
                    $arrRecibos = $this->selectRecibos($strCuenta,$strSeccion,$intVigencia,$strPago,$strFuente,$strFechaInicial,$strFechaFinal);
                    $arrSuperavit = $this->selectSuperavit($strCuenta,$strSeccion,$intVigencia,$strPago,$strFuente,$strFechaInicial,$strFechaFinal);
                    $arrSinRecibo = $this->selectSinRecibo($strCuenta,$strSeccion,$intVigencia,$strPago,$strFuente,$strFechaInicial,$strFechaFinal);
                    $arrNotasBancarias = $this->selectNotasBancarias($strCuenta,$strSeccion,$intVigencia,$strPago,$strFuente,$strFechaInicial,$strFechaFinal);
                    $arrTransferencias = $this->selectTransferencias($strCuenta,$strSeccion,$intVigencia,$strPago,$strFuente,$strFechaInicial,$strFechaFinal);
                    $arrCumariboSp = $this->selectCumariboSp($strCuenta,$strSeccion,$intVigencia,$strPago,$strFuente,$strFechaInicial,$strFechaFinal);
                    $arrEgresos = $this->selectRetencionEgresos($strCuenta,$strSeccion,$intVigencia,$strPago,$strFuente,$strFechaInicial,$strFechaFinal);
                    $arrOtrosEgresos = $this->selectRetencionOtrosEgresos($strCuenta,$strSeccion,$intVigencia,$strPago,$strFuente,$strFechaInicial,$strFechaFinal);
                    $arrOrdenPago = $this->selectRetencionOrdenPago($strCuenta,$strSeccion,$intVigencia,$strPago,$strFuente,$strFechaInicial,$strFechaFinal);
                    $arrRecaudoFactura = $this->selectRecaudoFactura($strCuenta,$strSeccion,$intVigencia,$strPago,$strFuente,$strFechaInicial,$strFechaFinal,$intCpc);
                    $arrCumariboSpAntiguo = $this->selectCumariboSpAntiguo($strCuenta,$strSeccion,$intVigencia,$strPago,$strFuente,$strFechaInicial,$strFechaFinal,$intCpc);
                    $arrRecaudoFacturaAcuerdo = $this->selectRecaudoFacturaAcuerdo($strCuenta,$strSeccion,$intVigencia,$strPago,$strFuente,$strFechaInicial,$strFechaFinal,$intCpc);
                    $total = $arrReservas['total']+$arrRecibos['total']+$arrSuperavit['total']+$arrSinRecibo['total']+$arrNotasBancarias['total']+$arrTransferencias['total']
                    +$arrCumariboSp['total']+$arrEgresos['total']+$arrOtrosEgresos['total']+$arrOrdenPago['total']+$arrRecaudoFactura['total']+$arrCumariboSpAntiguo['total']
                    +$arrRecaudoFacturaAcuerdo['total'];

                    $definitivo = $arrInicial['total'] + $arrAdicion['total'] - $arrReduccion['total'];
                    $saldo = $definitivo-$total;
                    //Inicial
                    array_push($arrData,$arrInicial['data']);
                    //Adicion
                    array_push($arrData,$arrAdicion['data']);
                    //Reducción
                    array_push($arrData,$arrReduccion['data']);
                    //Definitivo
                    array_push($arrData,[array("tipo"=>"T","comprobante"=>"Definitivo","valor"=>$definitivo)]);
                    //Reservas
                    array_push($arrData,$arrReservas['data']);
                    //Recibos
                    array_push($arrData,$arrRecibos['data']);
                    //Superavit
                    array_push($arrData,$arrSuperavit['data']);
                    //Sin recibos
                    array_push($arrData,$arrSinRecibo['data']);
                    //Notas bancarias
                    array_push($arrData,$arrNotasBancarias['data']);
                    //Transferencias
                    array_push($arrData,$arrTransferencias['data']);
                    //Cumaribo
                    array_push($arrData,$arrCumariboSp['data']);
                    //Egresos
                    array_push($arrData,$arrEgresos['data']);
                    //Otros egresos
                    array_push($arrData,$arrOtrosEgresos['data']);
                    //Orden pago
                    array_push($arrData,$arrOrdenPago['data']);
                    //Facturas
                    array_push($arrData,$arrRecaudoFactura['data']);
                    //Cumaribo antiguo
                    array_push($arrData,$arrCumariboSpAntiguo['data']);
                    //Facturas acuerdo de pago
                    array_push($arrData,$arrRecaudoFacturaAcuerdo['data']);
                    //Total consulta
                    array_push($arrData,[array("tipo"=>"T","comprobante"=>"Total consulta","valor"=>$total)]);
                    //Total recaudos
                    array_push($arrData,[array("tipo"=>"T","comprobante"=>"Total recaudos","valor"=>$total)]);
                    //Saldo por recaudar
                    array_push($arrData,[array("tipo"=>"T","comprobante"=>"Saldo por recaudar","valor"=>$saldo)]);
                    $arrData = $this->fixArrayInfo($arrData);
                return $arrData;
            }
            die();
        }
        public function fixArrayInfo(array $arrInfo){
            $arrData = [];
            foreach ($arrInfo as $cab) {
                foreach ($cab as $det) {
                    array_push($arrData,$det);
                }
            }
            return $arrData;
        }
        public function selectRecaudoFacturaAcuerdo($cuenta,$secPresupuestal,$vigGasto,$medioPago,$fuente,$strFechaInicial,$strFechaFinal,$cpc){
            $arrData = [];
            $vigenciaIni = explode("-",$strFechaInicial)[0];
            $vigenciaFin = explode("-",$strFechaFinal)[0];

            $vigGasto = $vigGasto != 0 ? " AND prc.vigencia_gasto = '$vigGasto'" : "";
            $secPresupuestal = $secPresupuestal != 0 ? " AND prc.seccion_presupuestal = '$secPresupuestal'" : "";
            $medioPago = $medioPago != 0 ? " AND prc.medio_pago = '$medioPago'" : "";
            $fuente = $fuente != "" ? " AND prc.fuente = '$fuente'" : "";
            $cpc = $cpc != "" ? " AND prc.producto_servicio = '$cpc'" : "";

            $sql = "SELECT prc.codigo_acuerdo, trc.fecha_acuerdo, prc.valor,trc.documento, trc.nombre_suscriptor
            FROM srv_acuerdo_ppto prc
            INNER JOIN srv_acuerdo_cab trc ON trc.codigo_acuerdo=prc.codigo_acuerdo
            WHERE prc.cuenta = '$cuenta' AND (prc.vigencia = '$vigenciaIni' OR prc.vigencia = '$vigenciaFin')
            AND NOT(trc.estado_acuerdo='Reversado') AND trc.fecha_acuerdo between '$strFechaInicial' AND '$strFechaFinal' $secPresupuestal
            $medioPago $vigGasto $fuente $cpc";
            $request = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
            $total = 0;
            if(!empty($request)){
                foreach ($request as $e) {
                    $estado = "";
                    $url = "serv-visualizar-acuerdoPago.php?codAcu=".$e['codigo_acuerdo'];
                    array_push($arrData,array(
                        "url"=>$url,
                        "tipo"=>"D",
                        "comprobante"=>"Recaudo acuerdo servicios públicos",
                        "consecutivo"=>$e['codigo_acuerdo'],
                        "fecha"=>$e['fecha_acuerdo'],
                        "estado"=>$estado,
                        "valor"=>$e['valor'],
                        "tercero"=>$e['documento']."-".$e['nombre_suscriptor'],
                        "objeto"=>""
                    ));
                    $total+=$e['valor'];
                }
            }
            return array("data"=>$arrData,"total"=>$total);
        }
        public function selectCumariboSpAntiguo($cuenta,$secPresupuestal,$vigGasto,$medioPago,$fuente,$strFechaInicial,$strFechaFinal,$cpc){
            $arrData = [];

            $vigGasto = $vigGasto != 0 ? " AND servreciboscaja_det.vigencia_gasto = '$vigGasto'" : "";
            $secPresupuestal = $secPresupuestal != 0 ? " AND servreciboscaja_det.seccion_presupuestal = '$secPresupuestal'" : "";
            $medioPago = $medioPago != 0 ? " AND servreciboscaja_det.medio_pago = '$medioPago'" : "";
            $fuente = $fuente != "" ? " AND servreciboscaja_det.fuente = '$fuente'" : "";

            $sql = "SELECT servreciboscaja_det.id_recibos, servreciboscaja.fecha, servreciboscaja_det.valor
            FROM servreciboscaja_det, servreciboscaja
            WHERE servreciboscaja.estado =  'S' AND servreciboscaja.id_recibos = servreciboscaja_det.id_recibos AND servreciboscaja_det.cuentapres = '$cuenta'
            AND servreciboscaja.fecha BETWEEN '$strFechaInicial' AND '$strFechaFinal' $secPresupuestal $medioPago $vigGasto $fuente";

            $request = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
            $total = 0;
            if(!empty($request)){
                foreach ($request as $e) {
                    $estado = "";
                    $url = "serv-visualizar-recaudo.php?codRec=".$e['id_recibos'];
                    array_push($arrData,array(
                        "url"=>$url,
                        "tipo"=>"D",
                        "comprobante"=>"Recaudo servicios públicos",
                        "consecutivo"=>$e['id_recibos'],
                        "fecha"=>$e['fecha'],
                        "estado"=>$estado,
                        "valor"=>$e['valor'],
                        "tercero"=>"",
                        "objeto"=>""
                    ));
                    $total+=$e['valor'];
                }
            }
            return array("data"=>$arrData,"total"=>$total);
        }
        public function selectRecaudoFactura($cuenta,$secPresupuestal,$vigGasto,$medioPago,$fuente,$strFechaInicial,$strFechaFinal,$cpc){
            $arrData = [];
            $vigenciaIni = explode("-",$strFechaInicial)[0];
            $vigenciaFin = explode("-",$strFechaFinal)[0];

            $vigGasto = $vigGasto != 0 ? " AND prc.vigencia_gasto = '$vigGasto'" : "";
            $secPresupuestal = $secPresupuestal != 0 ? " AND prc.seccion_presupuestal = '$secPresupuestal'" : "";
            $medioPago = $medioPago != 0 ? " AND prc.medio_pago = '$medioPago'" : "";
            $fuente = $fuente != "" ? " AND prc.fuente = '$fuente'" : "";
            $cpc = $cpc != "" ? " AND prc.producto_servicio = '$cpc'" : "";

            $sql = "SELECT prc.codigo_recaudo, trc.fecha_recaudo, prc.valor,trc.documento as tercero,trc.concepto
            FROM srv_recaudo_factura_ppto prc
            INNER JOIN srv_recaudo_factura trc ON trc.codigo_recaudo=prc.codigo_recaudo
            WHERE prc.cuenta = '$cuenta' AND (prc.vigencia = '$vigenciaIni' OR prc.vigencia = '$vigenciaFin')
            AND NOT(trc.estado='REVERSADO') AND trc.fecha_recaudo between '$strFechaInicial' AND '$strFechaFinal' $secPresupuestal
            $medioPago $vigGasto $fuente $cpc";
            $request = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
            $total = 0;
            if(!empty($request)){
                foreach ($request as $e) {
                    $sql="SELECT cedulanit as documento,
                    CASE WHEN razonsocial IS NULL OR razonsocial = ''
                    THEN CONCAT(nombre1,' ',nombre2,' ',apellido1,' ',apellido2)
                    ELSE razonsocial END AS nombre FROM terceros  WHERE cedulanit = '$e[tercero]'";
                    $arrTercero = mysqli_query($this->linkbd,$sql)->fetch_assoc();
                    $estado = "";
                    $url = "serv-visualizar-recaudo.php?codRec=".$e['codigo_recaudo'];
                    array_push($arrData,array(
                        "url"=>$url,
                        "tipo"=>"D",
                        "comprobante"=>"Recaudo servicios públicos",
                        "consecutivo"=>$e['codigo_recaudo'],
                        "fecha"=>$e['fecha_recaudo'],
                        "estado"=>$estado,
                        "valor"=>$e['valor'],
                        "tercero"=>$arrTercero['documento']."-".$arrTercero['nombre'],
                        "objeto"=>$e['conceptorden']
                    ));
                    $total+=$e['valor'];
                }
            }
            return array("data"=>$arrData,"total"=>$total);
        }
        public function selectRetencionOrdenPago($cuenta,$secPresupuestal,$vigGasto,$medioPago,$fuente,$strFechaInicial,$strFechaFinal){
            $arrData = [];
            $vigenciaIni = explode("-",$strFechaInicial)[0];
            $vigenciaFin = explode("-",$strFechaFinal)[0];

            $vigGasto = $vigGasto != 0 ? " AND prc.vigencia_gasto = '$vigGasto'" : "";
            $secPresupuestal = $secPresupuestal != 0 ? " AND prc.seccion_presupuestal = '$secPresupuestal'" : "";
            $medioPago = $medioPago != 0 ? " AND prc.medio_pago = '$medioPago'" : "";
            $fuente = $fuente != "" ? " AND prc.fuente = '$fuente'" : "";

            $sql = "SELECT prc.idrecibo, trc.fecha, prc.valor,trc.tercero,trc.conceptorden
            FROM pptoretencionpago prc,tesoordenpago trc
            WHERE prc.cuenta = '$cuenta' AND (prc.vigencia='$vigenciaIni' OR prc.vigencia = '$vigenciaFin') AND trc.id_orden=prc.idrecibo
            AND NOT(trc.estado='N') AND trc.fecha between '$strFechaInicial' AND '$strFechaFinal' AND trc.tipo_mov='201' $secPresupuestal
            $medioPago $vigGasto $fuente AND prc.tipo='orden'
            AND NOT EXISTS (SELECT 1 FROM tesoordenpago tca WHERE tca.id_orden=trc.id_orden  AND tca.tipo_mov='401')";
            $request = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
            $total = 0;
            if(!empty($request)){
                foreach ($request as $e) {
                    $sql="SELECT cedulanit as documento,
                    CASE WHEN razonsocial IS NULL OR razonsocial = ''
                    THEN CONCAT(nombre1,' ',nombre2,' ',apellido1,' ',apellido2)
                    ELSE razonsocial END AS nombre FROM terceros  WHERE cedulanit = '$e[tercero]'";
                    $arrTercero = mysqli_query($this->linkbd,$sql)->fetch_assoc();
                    $estado = "";
                    $url = "teso-egresoverccpet.php?idop=".$e['idrecibo'];
                    array_push($arrData,array(
                        "url"=>$url,
                        "tipo"=>"D",
                        "comprobante"=>"Orden Pago",
                        "consecutivo"=>$e['idrecibo'],
                        "fecha"=>$e['fecha'],
                        "estado"=>$estado,
                        "valor"=>$e['valor'],
                        "tercero"=>$arrTercero['documento']."-".$arrTercero['nombre'],
                        "objeto"=>$e['conceptorden']
                    ));
                    $total+=$e['valor'];
                }
            }
            return array("data"=>$arrData,"total"=>$total);
        }
        public function selectRetencionOtrosEgresos($cuenta,$secPresupuestal,$vigGasto,$medioPago,$fuente,$strFechaInicial,$strFechaFinal){
            $arrData = [];
            $vigenciaIni = explode("-",$strFechaInicial)[0];
            $vigenciaFin = explode("-",$strFechaFinal)[0];

            $vigGasto = $vigGasto != 0 ? " AND prc.vigencia_gasto = '$vigGasto'" : "";
            $secPresupuestal = $secPresupuestal != 0 ? " AND prc.seccion_presupuestal = '$secPresupuestal'" : "";
            $medioPago = $medioPago != 0 ? " AND prc.medio_pago = '$medioPago'" : "";
            $fuente = $fuente != "" ? " AND prc.fuente = '$fuente'" : "";

            $sql = "SELECT prc.idrecibo, trc.fecha, prc.valor,trc.concepto,trc.tercero
            FROM pptoretencionotrosegresos prc,tesopagotercerosvigant trc
            WHERE prc.cuenta = '$cuenta' AND (prc.vigencia = '$vigenciaIni' OR prc.vigencia = '$vigenciaFin') AND trc.id_pago=prc.idrecibo
            AND NOT(trc.estado='N') AND trc.fecha between '$strFechaInicial' AND '$strFechaFinal' $secPresupuestal
            $medioPago $vigGasto AND prc.tipo='egreso' $fuente";
            $request = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
            $total = 0;
            if(!empty($request)){
                foreach ($request as $e) {
                    $sql="SELECT cedulanit as documento,
                    CASE WHEN razonsocial IS NULL OR razonsocial = ''
                    THEN CONCAT(nombre1,' ',nombre2,' ',apellido1,' ',apellido2)
                    ELSE razonsocial END AS nombre FROM terceros  WHERE cedulanit = '$e[tercero]'";
                    $arrTercero = mysqli_query($this->linkbd,$sql)->fetch_assoc();
                    $estado = "";
                    $url = "teso-editapagotercerosvigant.php?idpago=".$e['idrecibo'];
                    array_push($arrData,array(
                        "url"=>$url,
                        "tipo"=>"D",
                        "comprobante"=>"Otros Egresos",
                        "consecutivo"=>$e['idrecibo'],
                        "fecha"=>$e['fecha'],
                        "estado"=>$estado,
                        "valor"=>$e['valor'],
                        "tercero"=>$arrTercero['documento']."-".$arrTercero['nombre'],
                        "objeto"=>$e['concepto']
                    ));
                    $total+=$e['valor'];
                }
            }
            return array("data"=>$arrData,"total"=>$total);
        }
        public function selectRetencionEgresos($cuenta,$secPresupuestal,$vigGasto,$medioPago,$fuente,$strFechaInicial,$strFechaFinal){
            $arrData = [];
            $vigenciaIni = explode("-",$strFechaInicial)[0];
            $vigenciaFin = explode("-",$strFechaFinal)[0];

            $vigGasto = $vigGasto != 0 ? " AND prc.vigencia_gasto = '$vigGasto'" : "";
            $secPresupuestal = $secPresupuestal != 0 ? " AND prc.seccion_presupuestal = '$secPresupuestal'" : "";
            $medioPago = $medioPago != 0 ? " AND prc.medio_pago = '$medioPago'" : "";
            $fuente = $fuente != "" ? " AND prc.fuente = '$fuente'" : "";

            $sql = "SELECT prc.idrecibo, trc.fecha, prc.valor,trc.concepto,trc.tercero
            FROM pptoretencionpago prc,tesoegresos trc
            WHERE prc.cuenta = '$cuenta' AND (prc.vigencia = '$vigenciaIni' OR prc.vigencia = '$vigenciaFin') AND trc.id_egreso=prc.idrecibo
            AND NOT(trc.estado='N') AND trc.fecha between '$strFechaInicial' AND '$strFechaFinal' AND trc.tipo_mov='201' $secPresupuestal $medioPago
            $vigGasto AND prc.tipo='egreso' AND NOT EXISTS (SELECT 1 FROM tesoegresos tra WHERE tra.id_egreso=trc.id_egreso  AND tra.tipo_mov='401')
            $fuente";
            $request = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
            $total = 0;
            if(!empty($request)){
                foreach ($request as $e) {
                    $sql="SELECT cedulanit as documento,
                    CASE WHEN razonsocial IS NULL OR razonsocial = ''
                    THEN CONCAT(nombre1,' ',nombre2,' ',apellido1,' ',apellido2)
                    ELSE razonsocial END AS nombre FROM terceros  WHERE cedulanit = '$e[tercero]'";
                    $arrTercero = mysqli_query($this->linkbd,$sql)->fetch_assoc();
                    $estado = "";
                    $url = "teso-girarchequesver-ccpet.php?idegre=".$e['idrecibo'];
                    array_push($arrData,array(
                        "url"=>$url,
                        "tipo"=>"D",
                        "comprobante"=>"Egresos",
                        "consecutivo"=>$e['idrecibo'],
                        "fecha"=>$e['fecha'],
                        "estado"=>$estado,
                        "valor"=>$e['valor'],
                        "tercero"=>$arrTercero['documento']."-".$arrTercero['nombre'],
                        "objeto"=>$e['concepto']
                    ));
                    $total+=$e['valor'];
                }
            }
            return array("data"=>$arrData,"total"=>$total);
        }
        public function selectCumariboSp($cuenta,$secPresupuestal,$vigGasto,$medioPago,$fuente,$strFechaInicial,$strFechaFinal){
            $arrData = [];
            $vigenciaIni = explode("-",$strFechaInicial)[0];
            $vigenciaFin = explode("-",$strFechaFinal)[0];

            $vigGasto = $vigGasto != 0 ? " AND TB2.vigencia_gasto = '$vigGasto'" : "";
            $secPresupuestal = $secPresupuestal != 0 ? " AND TB2.seccion_presupuestal = '$secPresupuestal'" : "";
            $medioPago = $medioPago != 0 ? " AND TB2.medio_pago = '$medioPago'" : "";
            $fuente = $fuente != "" ? " AND TB2.fuente = '$fuente'" : "";

            $sql = "SELECT TB2.idrecibo, TB1.fecha, TB2.valor,b.tercero
            from tesosinreciboscajasp TB1,pptosinrecibocajaspppto TB2
            LEFT JOIN tesobancosctas b ON TB1.cuentabanco = b.cuenta
            WHERE TB1.id_recibos=TB2.idrecibo AND TB1.estado='S' AND (TB2.vigencia = '$vigenciaIni' OR TB2.vigencia = '$vigenciaFin')
            AND TB1.vigencia=TB2.vigencia AND TB2.cuenta = '$cuenta' AND TB1.fecha between '$strFechaInicial' AND '$strFechaFinal'
            $secPresupuestal $medioPago $vigGasto $fuente";
            $request = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
            $total = 0;
            if(!empty($request)){
                foreach ($request as $e) {
                    $sql="SELECT cedulanit as documento,
                    CASE WHEN razonsocial IS NULL OR razonsocial = ''
                    THEN CONCAT(nombre1,' ',nombre2,' ',apellido1,' ',apellido2)
                    ELSE razonsocial END AS nombre FROM terceros  WHERE cedulanit = '$e[tercero]'";
                    $arrTercero = mysqli_query($this->linkbd,$sql)->fetch_assoc();
                    $estado = "";
                    array_push($arrData,array(
                        "tipo"=>"D",
                        "comprobante"=>"Recaudos servicios publicos Cumaribo",
                        "consecutivo"=>$e['idrecibo'],
                        "fecha"=>$e['fecha'],
                        "estado"=>$estado,
                        "valor"=>$e['valor'],
                        "tercero"=>$arrTercero['documento']."-".$arrTercero['nombre'],
                        "objeto"=>""
                    ));
                    $total+=$e['valor'];
                }
            }
            return array("data"=>$arrData,"total"=>$total);
        }
        public function selectTransferencias($cuenta,$secPresupuestal,$vigGasto,$medioPago,$fuente,$strFechaInicial,$strFechaFinal){
            $arrData = [];
            $vigenciaIni = explode("-",$strFechaInicial)[0];
            $vigenciaFin = explode("-",$strFechaFinal)[0];

            $vigGasto = $vigGasto != 0 ? " AND pitp.vigencia_gasto = '$vigGasto'" : "";
            $secPresupuestal = $secPresupuestal != 0 ? " AND pitp.seccion_presupuestal = '$secPresupuestal'" : "";
            $medioPago = $medioPago != 0 ? " AND pitp.medio_pago = '$medioPago'" : "";
            $fuente = $fuente != "" ? " AND pitp.fuente = '$fuente'" : "";

            $sql = "SELECT pitp.idrecibo, titp.fecha, pitp.valor,titp.concepto,titp.tercero
            FROM pptoingtranppto pitp,tesorecaudotransferencia titp
            WHERE pitp.cuenta = '$cuenta' AND (pitp.vigencia = '$vigenciaIni' OR pitp.vigencia = '$vigenciaFin') AND pitp.idrecibo=titp.id_recaudo
            AND NOT(titp.estado='N' OR titp.estado='R') AND titp.fecha between '$strFechaInicial' AND '$strFechaFinal' $secPresupuestal
            $medioPago $vigGasto $fuente";
            $request = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
            $total = 0;
            if(!empty($request)){
                foreach ($request as $e) {
                    $sql="SELECT cedulanit as documento,
                    CASE WHEN razonsocial IS NULL OR razonsocial = ''
                    THEN CONCAT(nombre1,' ',nombre2,' ',apellido1,' ',apellido2)
                    ELSE razonsocial END AS nombre FROM terceros  WHERE cedulanit = '$e[tercero]'";
                    $arrTercero = mysqli_query($this->linkbd,$sql)->fetch_assoc();
                    $estado = "";
                    $url = "teso-editarecaudotransferencia.php?idrecaudo=".$e['idrecibo'];
                    $reflejar = "ccp-recaudotransferencia-reflejar.php?idrecaudo=".$e['idrecibo'];
                    array_push($arrData,array(
                        "reflejar"=>$reflejar,
                        "url"=>$url,
                        "tipo"=>"D",
                        "comprobante"=>"Recaudos Transferencias",
                        "consecutivo"=>$e['idrecibo'],
                        "fecha"=>$e['fecha'],
                        "estado"=>$estado,
                        "valor"=>$e['valor'],
                        "tercero"=>$arrTercero['documento']."-".$arrTercero['nombre'],
                        "objeto"=>$e['concepto']
                    ));
                    $total+=$e['valor'];
                }
            }
            return array("data"=>$arrData,"total"=>$total);
        }
        public function selectNotasBancarias($cuenta,$secPresupuestal,$vigGasto,$medioPago,$fuente,$strFechaInicial,$strFechaFinal){
            $arrData = [];
            $vigenciaIni = explode("-",$strFechaInicial)[0];
            $vigenciaFin = explode("-",$strFechaFinal)[0];

            $vigGasto = $vigGasto != 0 ? " AND pnb.vigencia_gasto = '$vigGasto'" : "";
            $secPresupuestal = $secPresupuestal != 0 ? " AND pnb.seccion_presupuestal = '$secPresupuestal'" : "";
            $medioPago = $medioPago != 0 ? " AND pnb.medio_pago = '$medioPago'" : "";
            $fuente = $fuente != "" ? " AND pnb.fuente = '$fuente'" : "";

            $sql = "SELECT pnb.idrecibo, tnp.fecha, pnb.valor,tnp.concepto
            FROM pptonotasbanppto pnb,tesonotasbancarias_cab tnp
            WHERE pnb.cuenta = '$cuenta' AND (pnb.vigencia = '$vigenciaIni' OR pnb.vigencia = '$vigenciaFin')
            AND tnp.id_comp=pnb.idrecibo AND NOT(tnp.estado='R' OR tnp.estado='N')  AND (tnp.vigencia = '$vigenciaIni' OR tnp.vigencia = '$vigenciaFin')
            AND tnp.fecha BETWEEN '$strFechaInicial' AND '$strFechaFinal' $secPresupuestal
            $medioPago $vigGasto $fuente";
            $request = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
            $total = 0;
            if(!empty($request)){
                foreach ($request as $e) {
                    $estado = "";
                    $url = "teso-editanotasbancarias_new.php?id=".$e['idrecibo'];
                    array_push($arrData,array(
                        "url"=>$url,
                        "tipo"=>"D",
                        "comprobante"=>"Notas bancarias",
                        "consecutivo"=>$e['idrecibo'],
                        "fecha"=>$e['fecha'],
                        "estado"=>$estado,
                        "valor"=>$e['valor'],
                        "tercero"=>"",
                        "objeto"=>$e['concepto']
                    ));
                    $total+=$e['valor'];
                }
            }
            return array("data"=>$arrData,"total"=>$total);
        }
        public function selectSinRecibo($cuenta,$secPresupuestal,$vigGasto,$medioPago,$fuente,$strFechaInicial,$strFechaFinal){
            $arrData = [];
            $vigenciaIni = explode("-",$strFechaInicial)[0];
            $vigenciaFin = explode("-",$strFechaFinal)[0];

            $vigGasto = $vigGasto != 0 ? " AND psrc.vigencia_gasto = '$vigGasto'" : "";
            $secPresupuestal = $secPresupuestal != 0 ? " AND psrc.seccion_presupuestal = '$secPresupuestal'" : "";
            $medioPago = $medioPago != 0 ? " AND psrc.medio_pago = '$medioPago'" : "";
            $fuente = $fuente != "" ? " AND psrc.fuente = '$fuente'" : "";

            $sql="SELECT psrc.idrecibo, tsrc.fecha, psrc.valor,b.tercero, tsrc.id_recaudo
            FROM pptosinrecibocajappto psrc,tesosinreciboscaja tsrc
            LEFT JOIN tesobancosctas b ON tsrc.cuentabanco = b.cuenta
            WHERE psrc.cuenta = '$cuenta' AND (psrc.vigencia=$vigenciaIni OR psrc.vigencia = '$vigenciaFin') AND tsrc.id_recibos=psrc.idrecibo
            AND NOT(tsrc.estado='N' OR tsrc.estado='R') AND tsrc.fecha BETWEEN '$strFechaInicial' AND '$strFechaFinal' $medioPago
            $secPresupuestal $vigGasto $fuente";
            $request = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
            $total = 0;
            if(!empty($request)){
                foreach ($request as $e) {
                    $sql="SELECT cedulanit as documento,
                    CASE WHEN razonsocial IS NULL OR razonsocial = ''
                    THEN CONCAT(nombre1,' ',nombre2,' ',apellido1,' ',apellido2)
                    ELSE razonsocial END AS nombre FROM terceros  WHERE cedulanit = '$e[tercero]'";
                    $arrTercero = mysqli_query($this->linkbd,$sql)->fetch_assoc();
                    $estado = "";
                    $sqlObj = "SELECT concepto FROM tesosinrecaudos WHERE id_recaudo = '$e[id_recaudo]'";
                    $requestObj = mysqli_query($this->linkbd,$sqlObj)->fetch_assoc();
                    $url = "teso-ingresoInternoEditar?id=".$e['idrecibo'];
                    $reflejar = "ccp-sinrecibocaja-reflejar.php?idrecibo=".$e['idrecibo'];
                    array_push($arrData,array(
                        "reflejar"=>$reflejar,
                        "url"=>$url,
                        "tipo"=>"D",
                        "comprobante"=>"Ingresos internos",
                        "consecutivo"=>$e['idrecibo'],
                        "fecha"=>$e['fecha'],
                        "estado"=>$estado,
                        "valor"=>$e['valor'],
                        "tercero"=>$arrTercero['documento']."-".$arrTercero['nombre'],
                        "objeto"=>$requestObj['concepto']
                    ));
                    $total+=$e['valor'];
                }
            }
            return array("data"=>$arrData,"total"=>$total);
        }
        public function selectSuperavit($cuenta,$secPresupuestal,$vigGasto,$medioPago,$fuente,$strFechaInicial,$strFechaFinal){
            $arrData = [];
            $vigGasto = $vigGasto != 0 ? " AND vigencia_gasto = '$vigGasto'" : "";
            $secPresupuestal = $secPresupuestal != 0 ? " AND tsad.seccion_presupuestal = '$secPresupuestal'" : "";
            $medioPago = $medioPago != 0 ? " AND ptsad.medio_pago = '$medioPago'" : "";
            $fuente = $fuente != "" ? " AND tsad.fuente = '$fuente'" : "";

            $sql = "SELECT tsa.id, tsa.fecha, tsad.valor,tsa.concepto
            FROM tesosuperavit AS tsa, tesosuperavit_det AS tsad
            WHERE tsa.estado = 'S' AND tsa.id = tsad.id_tesosuperavit AND tsa.fecha
            BETWEEN '$strFechaInicial' AND '$strFechaFinal' AND tsad.rubro = '$cuenta' $vigGasto
            $fuente $secPresupuestal $medioPago";
            $request = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
            $total = 0;
            if(!empty($request)){
                foreach ($request as $e) {
                    $estado = "";
                    $url = "teso-verSuperavit-ccpet.php?is=".$e['id'];
                    array_push($arrData,array(
                        "url"=>$url,
                        "tipo"=>"D",
                        "comprobante"=>"Superavit",
                        "consecutivo"=>$e['id'],
                        "fecha"=>$e['fecha'],
                        "estado"=>$estado,
                        "valor"=>$e['valor'],
                        "tercero"=>"",
                        "objeto"=>$e['concepto']
                    ));
                    $total+=$e['valor'];
                }
            }
            return array("data"=>$arrData,"total"=>$total);
        }
        public function selectRecibos($cuenta,$secPresupuestal,$vigGasto,$medioPago,$fuente,$strFechaInicial,$strFechaFinal){
            $arrData = [];
            $vigenciaIni = explode("-",$strFechaInicial)[0];
            $vigenciaFin = explode("-",$strFechaFinal)[0];

            $vigGasto = $vigGasto != 0 ? " AND prc.vigencia_gasto = '$vigGasto'" : "";
            $secPresupuestal = $secPresupuestal != 0 ? " AND prc.seccion_presupuestal = '$secPresupuestal'" : "";
            $medioPago = $medioPago != 0 ? " AND prc.medio_pago = '$medioPago'" : "";
            $fuente = $fuente != "" ? " AND prc.fuente = '$fuente'" : "";

            $sql = "SELECT prc.idrecibo, trc.fecha, prc.valor, b.tercero,trc.descripcion
            FROM pptorecibocajappto prc,tesoreciboscaja trc
            LEFT JOIN tesobancosctas b ON trc.cuentabanco = b.cuenta
            WHERE prc.cuenta = '$cuenta' AND (prc.vigencia = $vigenciaIni OR prc.vigencia = $vigenciaFin)
            AND trc.id_recibos=prc.idrecibo AND NOT(trc.estado='N' OR trc.estado='R') AND NOT(prc.tipo='R')
            AND trc.fecha between '$strFechaInicial' AND '$strFechaFinal' $secPresupuestal $medioPago $vigGasto $fuente";
            $request = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
            $total = 0;
            if(!empty($request)){
                foreach ($request as $e) {
                    $sql="SELECT cedulanit as documento,
                    CASE WHEN razonsocial IS NULL OR razonsocial = ''
                    THEN CONCAT(nombre1,' ',nombre2,' ',apellido1,' ',apellido2)
                    ELSE razonsocial END AS nombre FROM terceros  WHERE cedulanit = '$e[tercero]'";
                    $arrTercero = mysqli_query($this->linkbd,$sql)->fetch_assoc();
                    $estado = "";
                    $url = "teso-recibocajaver.php?idrecibo=".$e['idrecibo'];
                    $reflejar = "ccp-recibocaja-reflejarppto?idrecibo=".$e['idrecibo'];
                    array_push($arrData,array(
                        "reflejar"=>$reflejar,
                        "url"=>$url,
                        "tipo"=>"D",
                        "comprobante"=>"Recibo de caja",
                        "consecutivo"=>$e['idrecibo'],
                        "fecha"=>$e['fecha'],
                        "estado"=>$estado,
                        "valor"=>$e['valor'],
                        "tercero"=>$arrTercero['documento']."-".$arrTercero['nombre'],
                        "objeto"=>$e['descripcion']
                    ));
                    $total+=$e['valor'];
                }
            }
            return array("data"=>$arrData,"total"=>$total);
        }
        public function selectReservas($cuenta,$secPresupuestal,$vigGasto,$medioPago,$fuente,$strFechaInicial,$strFechaFinal){
            $arrData = [];
            $fuente = $fuente != "" ? " AND tsad.fuente = '$fuente'" : "";
            $vigGasto = $vigGasto != 0 ? " AND tsad.codigo_vigenciag = '$vigGasto'" : "";
            $secPresupuestal = $secPresupuestal != 0 ? " AND tsad.clasificacion = '$secPresupuestal'" : "";
            $medioPago = $medioPago != 0 ? " AND tsad.clasificador = '$medioPago'" : "";

            $sql = "SELECT tsa.id, tsa.fecha, tsa.valor
            FROM tesoreservas AS tsa, tesoreservas_det AS tsad
            WHERE tsa.estado = 'S' AND tsa.id = tsad.id_reserva AND tsa.fecha
            BETWEEN '$strFechaInicial' AND '$strFechaFinal' AND tsad.rubro = '$cuenta' $fuente $secPresupuestal $medioPago $vigGasto
            GROUP BY tsad.fuente, tsad.vigencia_gasto";
            $request = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
            $total = 0;
            if(!empty($request)){
                foreach ($request as $e) {
                    $estado = $e['estado'] == 'S' ? 'Activo' : 'Anulado';
                    $url = "teso-verReservas-ccpet.php?is=".$e['id'];
                    array_push($arrData,array(
                        "url"=>$url,
                        "tipo"=>"D",
                        "comprobante"=>"Reservas",
                        "consecutivo"=>$e['id'],
                        "fecha"=>$e['fecha'],
                        "estado"=>$estado,
                        "valor"=>$e['valor'],
                        "tercero"=>"",
                        "objeto"=>""
                    ));
                    $total+=$e['valor'];
                }
            }
            return array("data"=>$arrData,"total"=>$total);
        }
        public function selectReduccion($cuenta,$secPresupuestal,$vigGasto,$medioPago,$fuente,$strFechaInicial,$strFechaFinal){
            $arrData = [];
            $vigGasto = $vigGasto != 0 ? " AND codigo_vigenciag = '$vigGasto'" : "";
            $secPresupuestal = $secPresupuestal != 0 ? " AND seccion_presupuestal = '$secPresupuestal'" : "";
            $medioPago = $medioPago != 0 ? " AND medio_pago = '$medioPago'" : "";

            $sql = "SELECT id_adicion, fecha, valor, estado FROM ccpet_reducciones
            WHERE cuenta = '$cuenta' AND tipo_cuenta = 'I' $secPresupuestal $medioPago $vigGasto AND fuente = '$fuente'
            AND estado = 'S' AND fecha BETWEEN '$strFechaInicial' AND '$strFechaFinal'";
            $request = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
            $total = 0;
            if(!empty($request)){
                foreach ($request as $e) {
                    $estado = $e['estado'] == 'S' ? 'Activo' : 'Anulado';
                    $url = "ccp-visualizaAdicion.php?id=".$e['id_adicion'];
                    array_push($arrData,array(
                        "url"=>$url,
                        "tipo"=>"D",
                        "comprobante"=>"Reducción",
                        "consecutivo"=>$e['id_adicion'],
                        "fecha"=>$e['fecha'],
                        "estado"=>$estado,
                        "valor"=>$e['valor'],
                        "tercero"=>"",
                        "objeto"=>""
                    ));
                    $total+=$e['valor'];
                }
            }
            array_push($arrData,array(
                "tipo"=>"T",
                "comprobante"=>"Total reducción",
                "valor"=>$total,
            ));
            return array("data"=>$arrData,"total"=>$total);
        }
        public function selectAdicion($cuenta,$secPresupuestal,$vigGasto,$medioPago,$fuente,$strFechaInicial,$strFechaFinal){
            $arrData = [];
            $vigGasto = $vigGasto != 0 ? " AND codigo_vigenciag = '$vigGasto'" : "";
            $secPresupuestal = $secPresupuestal != 0 ? " AND seccion_presupuestal = '$secPresupuestal'" : "";
            $medioPago = $medioPago != 0 ? " AND medio_pago = '$medioPago'" : "";

            $sql = "SELECT id_acuerdo, fecha, valor, estado FROM ccpet_adiciones
            WHERE cuenta = '$cuenta' AND tipo_cuenta = 'I' $secPresupuestal $medioPago $vigGasto AND fuente = '$fuente'
            AND estado = 'S' AND fecha BETWEEN '$strFechaInicial' AND '$strFechaFinal'";
            $request = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
            $total = 0;
            if(!empty($request)){
                foreach ($request as $e) {
                    $estado = $e['estado'] == 'S' ? 'Activo' : 'Anulado';
                    $url = "ccp-visualizaAdicion.php?id=".$e['id_acuerdo'];
                    array_push($arrData,array(
                        "url"=>$url,
                        "tipo"=>"D",
                        "comprobante"=>"Adición",
                        "consecutivo"=>$e['id_acuerdo'],
                        "fecha"=>$e['fecha'],
                        "estado"=>$estado,
                        "valor"=>$e['valor'],
                        "tercero"=>"",
                        "objeto"=>""
                    ));
                    $total+=$e['valor'];
                }
            }
            array_push($arrData,array(
                "tipo"=>"T",
                "comprobante"=>"Total adición",
                "valor"=>$total,
            ));
            return array("data"=>$arrData,"total"=>$total);
        }
        public function selectInicial($cuenta,$secPresupuestal,$vigGasto,$medioPago,$fuente,$vigenciaIni,$vigenciaFin){
            $arrData = [];
            $vigenciaIni = explode("-",$vigenciaIni)[0];
            $vigenciaFin = explode("-",$vigenciaFin)[0];

            $vigGasto = $vigGasto != 0 ? " AND vigencia_gasto = '$vigGasto'" : "";
            $secPresupuestal = $secPresupuestal != 0 ? " AND seccion_presupuestal = '$secPresupuestal'" : "";
            $medioPago = $medioPago != 0 ? " AND medio_pago = '$medioPago'" : "";

            $sql = "SELECT COALESCE(valor,0) as valor, vigencia
            FROM ccpetinicialing
            WHERE cuenta = '$cuenta' $secPresupuestal $vigGasto $medioPago AND fuente = '$fuente'
            AND (vigencia = '$vigenciaIni' OR vigencia = '$vigenciaFin')";
            $request = mysqli_query($this->linkbd,$sql)->fetch_assoc();
            $total = $request['valor'];
            array_push($arrData,array(
                "tipo"=>"D",
                "comprobante"=>"Presupuesto inicial",
                "consecutivo"=>"",
                "fecha"=>$request['vigencia'],
                "estado"=>"",
                "valor"=>$request['valor'],
                "tercero"=>"",
                "objeto"=>""
            ));
            return array("data"=>$arrData,"total"=>$total);
        }
    }

	if($_GET){
        $strCuenta = $_GET['cuenta'];
        $strNombreCuenta = $_GET['nombre_cuenta'];
        $strFuente = $_GET['fuente'];
        $strNombreFuente = $_GET['nombre_fuente'];
        $strFechaInicial = date_format(date_create($_GET['fecha_inicial']),"d/m/Y");
        $strFechaFinal = date_format(date_create($_GET['fecha_final']),"d/m/Y");
        $intVigencia = $_GET['vigencia'];
        $strSeccion = $_GET['seccion'];
        $strPago = $_GET['pago'];
        $intCpc = $_GET['cpc'];
        $obj = new Plantilla();
        $arrData = $obj->getInfo();
        $objPHPExcel = new PHPExcel();
        $objPHPExcel->getActiveSheet()->getStyle('A:F')->applyFromArray(
            array(
                'alignment' => array(
                    'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
                    'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
                )
            )
        );
        $objPHPExcel->getProperties()
        ->setCreator("IDEAL 10")
        ->setLastModifiedBy("IDEAL 10")
        ->setTitle("Exportar Excel con PHP")
        ->setSubject("Documento de prueba")
        ->setDescription("Documento generado con PHPExcel")
        ->setKeywords("usuarios phpexcel")
        ->setCategory("reportes");

        //----Cuerpo de Documento----
        $objPHPExcel->setActiveSheetIndex(0)
        ->mergeCells('A1:F1')
        ->mergeCells('A2:F2')
        ->mergeCells('B3:F3')
        ->mergeCells('B4:F4')
        ->mergeCells('B5:F5')
        ->mergeCells('B6:F6')
        ->mergeCells('B7:F7')
        ->mergeCells('B8:F8')
        ->mergeCells('B9:F9')
        ->setCellValue('A1', 'PRESUPUESTO')
        ->setCellValue('A2', 'REPORTE AUXILIAR POR CUENTA INGRESOS')
        ->setCellValue('A3', 'FECHA')
        ->setCellValue('A4', 'CUENTA')
        ->setCellValue('A5', 'FUENTE')
        ->setCellValue('A6', 'MEDIO DE PAGO')
        ->setCellValue('A7', 'VIGENCIA DE GASTO')
        ->setCellValue('A8', 'SECCIÓN PRESUPUESTAL')
        ->setCellValue('A9', 'CPC')
        ->setCellValue('B3', "Desde ".$strFechaInicial." Hasta ".$strFechaFinal)
        ->setCellValue('B4', $strCuenta."-".$strNombreCuenta)
        ->setCellValue('B5', $strFuente."-".$strNombreFuente)
        ->setCellValue('B6', $strPago)
        ->setCellValue('B7', $intVigencia)
        ->setCellValue('B8', $strSeccion)
        ->setCellValue('B9', $intCpc);
        $objPHPExcel-> getActiveSheet ()
        -> getStyle ("A3")
        -> getFill ()
        -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
        -> getStartColor ()
        -> setRGB ('3399cc');
        $objPHPExcel-> getActiveSheet ()
        -> getStyle ("A4")
        -> getFill ()
        -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
        -> getStartColor ()
        -> setRGB ('3399cc');
        $objPHPExcel-> getActiveSheet ()
        -> getStyle ("A5")
        -> getFill ()
        -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
        -> getStartColor ()
        -> setRGB ('3399cc');
        $objPHPExcel-> getActiveSheet ()
        -> getStyle ("A6")
        -> getFill ()
        -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
        -> getStartColor ()
        -> setRGB ('3399cc');
        $objPHPExcel-> getActiveSheet ()
        -> getStyle ("A7")
        -> getFill ()
        -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
        -> getStartColor ()
        -> setRGB ('3399cc');
        $objPHPExcel-> getActiveSheet ()
        -> getStyle ("A8")
        -> getFill ()
        -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
        -> getStartColor ()
        -> setRGB ('3399cc');
        $objPHPExcel-> getActiveSheet ()
        -> getStyle ("A9")
        -> getFill ()
        -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
        -> getStartColor ()
        -> setRGB ('3399cc');
        $objPHPExcel-> getActiveSheet ()
        -> getStyle ("A1")
        -> getFill ()
        -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
        -> getStartColor ()
        -> setRGB ('C8C8C8');
        $objPHPExcel-> getActiveSheet ()
        -> getStyle ("A1:A2")
        -> getFont ()
        -> setBold ( true )
        -> setName ( 'Verdana' )
        -> setSize ( 10 )
        -> getColor ()
        -> setRGB ('000000');
        $objPHPExcel-> getActiveSheet ()
        -> getStyle ('A1:A2')
        -> getAlignment ()
        -> applyFromArray (array ( 'horizontal'  =>  PHPExcel_Style_Alignment :: HORIZONTAL_CENTER ,) );
        $objPHPExcel-> getActiveSheet ()
        -> getStyle ('A4:F4')
        -> getAlignment ()
        -> applyFromArray (array ( 'horizontal'  =>  PHPExcel_Style_Alignment :: VERTICAL_JUSTIFY ,) );
        $objPHPExcel-> getActiveSheet ()
        -> getStyle ("A2")
        -> getFill ()
        -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID);

        $borders = array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                'color' => array('argb' => 'FF000000'),
                )
            ),
        );
        $objPHPExcel->setActiveSheetIndex(0)
        ->setCellValue('A10', 'Tipo comprobante')
        ->setCellValue('B10', "Consecutivo")
        ->setCellValue('C10', "Tercero")
        ->setCellValue('D10', "Objeto")
        ->setCellValue('E10', "Fecha")
        ->setCellValue('F10', "Valor");
        $objPHPExcel-> getActiveSheet ()
            -> getStyle ("A10:F10")
            -> getFill ()
            -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
            -> getStartColor ()
            -> setRGB ('99ddff');
        $objPHPExcel->getActiveSheet()->getStyle("A10:F10")->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getStyle('A1:F1')->applyFromArray($borders);
        $objPHPExcel->getActiveSheet()->getStyle('A2:F2')->applyFromArray($borders);
        $objPHPExcel->getActiveSheet()->getStyle('A3:F3')->applyFromArray($borders);
        $objPHPExcel->getActiveSheet()->getStyle('A4:F4')->applyFromArray($borders);
        $objPHPExcel->getActiveSheet()->getStyle('A5:F5')->applyFromArray($borders);
        $objPHPExcel->getActiveSheet()->getStyle('A6:F6')->applyFromArray($borders);
        $objPHPExcel->getActiveSheet()->getStyle('A7:F7')->applyFromArray($borders);
        $objPHPExcel->getActiveSheet()->getStyle('A8:F8')->applyFromArray($borders);
        $objPHPExcel->getActiveSheet()->getStyle('A9:F9')->applyFromArray($borders);
        $objPHPExcel->getActiveSheet()->getStyle('A10:F10')->applyFromArray($borders);

        $objPHPExcel->getActiveSheet()->getStyle("A3")->getFont()->getColor()->setRGB("ffffff");
        $objPHPExcel->getActiveSheet()->getStyle("A4")->getFont()->getColor()->setRGB("ffffff");
        $objPHPExcel->getActiveSheet()->getStyle("A5")->getFont()->getColor()->setRGB("ffffff");
        $objPHPExcel->getActiveSheet()->getStyle("A6")->getFont()->getColor()->setRGB("ffffff");
        $objPHPExcel->getActiveSheet()->getStyle("A7")->getFont()->getColor()->setRGB("ffffff");
        $objPHPExcel->getActiveSheet()->getStyle("A8")->getFont()->getColor()->setRGB("ffffff");
        $objPHPExcel->getActiveSheet()->getStyle("A9")->getFont()->getColor()->setRGB("ffffff");

        $objPHPExcel->getActiveSheet()->getStyle("A3")->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getStyle("A4")->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getStyle("A5")->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getStyle("A6")->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getStyle("A7")->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getStyle("A8")->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getStyle("A9")->getFont()->setBold(true);

        $row = 11;
        foreach ($arrData as $data) {
            $strFecha = date_create($data['fecha']);
            $strFechaFormat = date_format($strFecha,"d/m/Y");
            if($data['tipo'] == "T"){
                $objPHPExcel-> getActiveSheet ()
                -> getStyle ("A$row:F$row")
                -> getFill ()
                -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
                -> getStartColor ()
                -> setRGB ('99ddff');
            }
            $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValueExplicit ("A$row", $data['comprobante'], PHPExcel_Cell_DataType :: TYPE_STRING)
            ->setCellValueExplicit ("B$row", $data['tipo'] == "T" ? "" : $data['consecutivo'], PHPExcel_Cell_DataType :: TYPE_STRING)
            ->setCellValueExplicit ("C$row", $data['tipo'] == "T" ? "" : $data['tercero'], PHPExcel_Cell_DataType :: TYPE_STRING)
            ->setCellValueExplicit ("D$row", $data['tipo'] == "T" ? "" : $data['objeto'], PHPExcel_Cell_DataType :: TYPE_STRING)
            ->setCellValueExplicit ("E$row", $data['tipo'] == "T" ? "" : $strFechaFormat, PHPExcel_Cell_DataType :: TYPE_STRING)
            ->setCellValueExplicit ("F$row", $data['valor'], PHPExcel_Cell_DataType :: TYPE_NUMERIC);
            $row++;
        }

        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
        //----Guardar documento----
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="reporte-auxiliar_cuenta_ingresos.xlsx"');
        header('Cache-Control: max-age=0');
        $objWriter=PHPExcel_IOFactory::createWriter($objPHPExcel,'Excel2007');
        $objWriter->save('php://output');
        die();

    }
?>
