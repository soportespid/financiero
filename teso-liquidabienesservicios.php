<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require "comun.inc";
	require "funciones.inc";
	require "conversor.php";
	session_start();
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html lang="es">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1">
		<meta name="viewport" content="user-scalable=no">
		<title>:: IDEAL 10 - Tesorer&iacute;a</title>
		<link href="favicon.ico" rel="shortcut icon"/>
		<link href="css/css2.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
		<link href="css/tabs.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
		<link href="sweetalert2/dist/sweetalert2.min.css" rel="stylesheet"/>
		<script src="sweetalert2/dist/sweetalert2.min.js"></script>
		<script type="text/javascript" src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script type="text/javascript" src="css/calendario.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script>
			function validar() {
				document.form2.submit();
			}
			function buscater(e) {	
				if (document.form2.tercero.value!="") {
					document.form2.bt.value='1';
					document.form2.submit();
				}
			}
			function agregardetalle() {
				if(document.form2.codingreso.value!="" &&  parseFloat(document.form2.valor.value)>0 && document.form2.cc.value!="") {
					document.form2.agregadet.value=1;
					document.form2.submit();
				}else {
					Swal.fire({
						icon: 'error',
						title: 'Error!',
						text: 'Falta informacion para poder Agregar',
						confirmButtonText: 'Continuar',
						confirmButtonColor: '#FF121A',
						timer: 2500
					});
				}
			}
			function agregardetalled(){
				if(document.form2.retencion.value!="" &&  document.form2.vporcentaje.value!=""  ){ 
					document.form2.agregadetdes.value=1;
					document.form2.calculaRetencion.value = 1;
					document.form2.submit();
				}else {
					Swal.fire({
						icon: 'error',
						title: 'Error!',
						text: 'Falta informacion para poder Agregar',
						confirmButtonText: 'Continuar',
						confirmButtonColor: '#FF121A',
						timer: 2500
					});
				}
			}
			function eliminar(variable){
				Swal.fire({
					icon: 'question',
					title: '¿Esta Seguro de Eliminar?',
					showDenyButton: true,
					confirmButtonText: 'Guardar',
					confirmButtonColor: '#01CC42',
					denyButtonText: 'Cancelar',
					denyButtonColor: '#FF121A',
				}).then(
					(result) => {
						if (result.isConfirmed){
							document.form2.elimina.value = variable;
							vvend = document.getElementById('elimina');
							vvend.value = variable;
							document.form2.submit();
						}else if (result.isDenied){
							Swal.fire({
								icon: 'info',
								title: 'No se elimino',
								confirmButtonText: 'Continuar',
								confirmButtonColor: '#FF121A',
								timer: 2500
							});
						}
					}
				)
			}
			function eliminard(variable){
				Swal.fire({
					icon: 'question',
					title: '¿Esta Seguro de Eliminar?',
					showDenyButton: true,
					confirmButtonText: 'Guardar',
					confirmButtonColor: '#01CC42',
					denyButtonText: 'Cancelar',
					denyButtonColor: '#FF121A',
				}).then(
					(result) => {
						if (result.isConfirmed){
							document.form2.eliminad.value = variable;
							document.form2.calculaRetencion.value = 1;
							vvend = document.getElementById('eliminad');
							vvend.value = variable;
							document.form2.submit();
						}else if (result.isDenied){
							Swal.fire({
								icon: 'info',
								title: 'No se elimino',
								confirmButtonText: 'Continuar',
								confirmButtonColor: '#FF121A',
								timer: 2500
							});
						}
					}
				)
			}
			function guardar() {
				ingresos2=document.getElementsByName('dcoding[]');
				if (document.form2.fecha.value != '' && ingresos2.length > 0) {
					Swal.fire({
						icon: 'question',
						title: '¿Esta seguro guardar?',
						showDenyButton: true,
						confirmButtonText: 'Guardar',
						confirmButtonColor: '#01CC42',
						denyButtonText: 'Cancelar',
						denyButtonColor: '#FF121A',
					}).then(
						(result) => {
							if (result.isConfirmed){
								document.form2.oculto.value = 2;
								document.form2.submit();
							}else if (result.isDenied){
								Swal.fire({
									icon: 'info',
									title: 'No se guardo',
									confirmButtonText: 'Continuar',
									confirmButtonColor: '#FF121A',
									timer: 2500
								});
							}
						}
					)
				}else {
					Swal.fire({
						icon: 'error',
						title: 'Error!',
						text: 'Faltan datos para completar el registro',
						confirmButtonText: 'Continuar',
						confirmButtonColor: '#FF121A',
						timer: 2500
					});
					document.form2.fecha.focus();
					document.form2.fecha.select();
				}
			}
			function buscater(e){
				if (document.form2.tercero.value!="") {
					document.form2.bt.value='1';
					document.form2.submit();
				}
			}
			function buscaing(e){
				if (document.form2.codingreso.value!="") {
					
					document.form2.bin.value='1';
					document.form2.submit();
				}
			}
			function despliegamodal2(_valor,_tip){
				document.getElementById("bgventanamodal2").style.visibility=_valor;
				if(_valor=="hidden"){
					document.getElementById('ventana2').src="";
				}else {
					if(_tip=='1'){
						document.getElementById('ventana2').src="tercerosgral-ventana01.php?objeto=tercero&nobjeto=ntercero&nfoco=codingreso";
					}else{
						document.getElementById('ventana2').src="bienesservicios-ventana01.php?ti=I&modulo=4";
					}
				}
			}
			function despliegaModalFuentes(_valor) {
				var codigo=document.getElementById('codingreso').value;
				if (codigo != "") {
					document.getElementById("bgventanamodal2").style.visibility=_valor;
					if(_valor=="hidden"){
						document.getElementById('ventana2').src="";
					}else{
						if (codigo != ""){
							document.getElementById('ventana2').src="bienesserviciosfuentes-ventana.php?codigo="+codigo;
						}else{
							Swal.fire({
								icon: 'error',
								title: 'Error!',
								text: 'Seleccione primero el código de ingreso',
								confirmButtonText: 'Continuar',
								confirmButtonColor: '#FF121A',
								timer: 2500
							});
						}
					}
				}else{
					Swal.fire({
						icon: 'error',
						title: 'Error!',
						text: 'Seleccione primero el código de ingreso',
						confirmButtonText: 'Continuar',
						confirmButtonColor: '#FF121A',
						timer: 2500
					});
				}
			}
			function cambiovigencia(){
				let anno = document.getElementById('fc_1198971545').value.split("/");
				document.getElementById('vigencia').value = anno[2];
			}
		</script>
	</head>
	<body>
		<IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
		<span id="todastablas2"></span>
		<table>
			<tr><script>barra_imagenes("teso");</script><?php cuadro_titulos();?></tr>
			<tr><?php menu_desplegable("teso");?></tr>
			<tr>
				<td colspan="3" class="cinta">
					<img src="imagenes/add.png" title="Nuevo" onClick="location.href='teso-liquidabienesservicios.php'" class="mgbt"/>
					<img src="imagenes/guarda.png" title="Guardar"  onClick="guardar()" class="mgbt"/>
					<img src="imagenes/busca.png" title="Buscar" onClick="location.href='teso-liquidabienesserviciosbuscar.php'" class="mgbt"/>
					<img src="imagenes/agenda1.png" title="Agenda" onClick="mypop=window.open('plan-agenda.php','','');mypop.focus()" class="mgbt"/>
					<img src="imagenes/nv.png" title="Nueva ventana" onClick="mypop=window.open('teso-principal.php','','');mypop.focus();" class="mgbt"/>
					<img src="imagenes/duplicar_pantalla.png" title="Duplicar pesta&ntilde;a" onClick="mypop=window.open('<?php echo $url2; ?>','','');mypop.focus();" class="tooltip bottom mgbt"/>
				</td>
			</tr>		  
		</table>
		<div id="bgventanamodalm" class="bgventanamodalm">
			<div id="ventanamodalm" class="ventanamodalm">
				<IFRAME src="" name="ventanam" marginWidth=0 marginHeight=0 frameBorder=0 id="ventanam" frameSpacing=0 style=" width:700px; height:130px; top:200; overflow:hidden;"> 
				</IFRAME>
			</div>
		</div>
		<?php
			if($_POST['oculto'] == "") {
				$_POST['vigencia'] = date('Y');
				$_POST['dcoding'] = array(); 		 
				$_POST['dncoding'] = array(); 		 
				$_POST['dfuente'] = array();
				$_POST['dvalores'] = array();
				$check1="checked";
				$fec = date("d/m/Y");

				$sqlr = "SELECT cuentacaja FROM tesoparametros";
				$res = mysqli_query($linkbd,$sqlr);
				while ($row = mysqli_fetch_row($res)) {
					$_POST['cuentacaja'] = $row[0];
				}
				$consec = selconsecutivo('tesobienesservicios','id_recaudo');
				$_POST['idcomp'] = $consec;	
				$_POST['fecha'] = date("d/m/Y"); 		 		  			 
				$_POST['valor'] = 0;		 
			}

			if($_POST['tabgroup1']){
				switch($_POST['tabgroup1'])
				{
					case 1:	$check1 = 'checked';break;
					case 2: $check2 = 'checked';break;
				}
			}else{
				$check1="checked";
			}
		?>
		<form name="form2" method="post" action=""> 
			<?php
				//***** busca tercero
				if($_POST['bt'] == '1') {
					$nresul = buscatercero($_POST['tercero']);
					if($nresul != '') {
						$_POST['ntercero'] = $nresul;
					}else{
						$_POST['ntercero'] = "";
					}
				}
				//******** busca ingreso *****
				if($_POST['bin'] == '1') {
					$sql = "SELECT * FROM tesoingresos_ventas WHERE codigo = '".$_POST['codingreso']."' AND estado='S' ORDER BY codigo";
					$row = mysqli_fetch_row(mysqli_query($linkbd, $sql));
					if($row[0] != '') {
						$_POST['ningreso'] = $row[1];
						$_POST['iva19'] = $row[6];
						if ($row[6] == 'N') {
							$_POST['causacion'] = 2;
						}else {
							$_POST['causacion'] = 1;
						}
					}else {
						$_POST['ningreso'] = "";
						$_POST['codingreso'] = "";
						$_POST['iva19'] = '';
						echo"
						<script>
							document.getElementById('valfocus').value='2';
							Swal.fire({
								icon: 'error',
								title: 'Error!',
								text: 'Codigo Ingresos Incorrecto',
								confirmButtonText: 'Continuar',
								confirmButtonColor: '#FF121A',
								timer: 2500
							});
						</script>";
					}
				}	 
			?>
			<input type="hidden" id="iva19" name="iva19" value="<?php echo $_POST['iva19']?>">
			<div class="tabs" style="height:75%; width:99.7%">
				<div class="tab" >
					<input type="radio" id="tab-1" name="tabgroup1" value="1" <?php echo $check1;?> >
					<label id="clabel" for="tab-1">Orden de pago</label>
					<div class="content" style="overflow:hidden;">
						<table class="inicio ancho">
							<tr>
								<td class="titulos" colspan="2">Liquidar Bienes y Servicios</td>
								<td class="cerrar" style="width:7%" onClick="location.href='teso-principal.php'">Cerrar</td>
							</tr>
							<tr>
								<td>
									<table>
										<tr>
											<td style="width:12%;" class="saludo1" >Numero Liquidación:</td>
											<td  style="width:7%;" >
												<input type="text" name="idcomp" value="<?php echo $_POST['idcomp']?>" onKeyUp="return tabular(event,this) " readonly>
											</td>
											<td style="width:5%;"  class="saludo1">Fecha:</td>
											<td  style="width:10%;">
												<input type="text" name="fecha" id="fc_1198971545" title="DD/MM/YYYY"  value="<?php echo $_POST['fecha']; ?>" style="width:100%;" onKeyUp="return tabular(event,this) " onKeyDown="mascara(this,'/',patron,true)"  maxlength="10" onDblClick="displayCalendarFor('fc_1198971545');" class="colordobleclik" autocomplete="off" onchange="cambiovigencia()"> 
											</td>
											<td style="width:5%;" class="saludo1">Vigencia:</td>
											<td style="width:10%;">
												<input type="text" id="vigencia" name="vigencia"  onKeyPress="javascript:return solonumeros(event)" onKeyUp="return tabular(event,this)" style="width:100%;" value="<?php echo $_POST['vigencia']?>" onClick="document.getElementById('tipocta').focus();document.getElementById('tipocta').select();"  readonly>
											</td>
											<td style="width:10%;" class="saludo1">Causacion Contable:</td>
											<td style="width:10%;">
												<select name="causacion" id="causacion" onKeyUp="return tabular(event,this)" style="width:100%;">
													<option value="1" <?php if($_POST['causacion'] == '1') echo "SELECTED"; ?>>Si</option>
													<option value="2" <?php if($_POST['causacion'] == '2') echo "SELECTED"; ?>>No</option>         
												</select>
											</td> 	
											<td class="saludo1">Centro Costo:</td>
											<td>
												<select name="cc" onChange="validar()" onKeyUp="return tabular(event,this)" style="width:100%;">
													<option value="">Seleccione ...</option>
													<?php
														$sqlr="SELECT * FROM centrocosto WHERE estado='S' ORDER BY id_cc";
														$res = mysqli_query($linkbd,$sqlr);
														while ($row = mysqli_fetch_row($res)){
															if("$row[0]" == $_POST['cc']){
																echo "<option value='$row[0]' SELECTED>$row[0] - $row[1]</option>";
															}else{
																echo "<option value='$row[0]'>$row[0] - $row[1]</option>";
															}
														}	 	
													?>
												</select>
											</td>	
										</tr>
										<tr>
											<td class="saludo1">Concepto Liquidacion:</td>
											<td colspan="9" >
												<input type="text" name="concepto" value="<?php echo $_POST['concepto']?>" style="width:100%;"  onKeyUp="return tabular(event,this)">
											</td>
										</tr>  
										<tr>
											<td class="saludo1">Contribuyente: </td>
											<td>
												<input type="text" name="tercero" value="<?php echo $_POST['tercero']?>" style="width:100%;" onchange="buscater(event)" onKeyUp="return tabular(event,this)" onDblClick="mypop=window.open('terceros-ventana.php?ti=1','','menubar=0,scrollbars=yes, toolbar=no, location=no, width=700px,height=500px');mypop.focus();" class="colordobleclik" autocomplete="off"> 
											</td>
											<td colspan="8">
												<input type="text" id="ntercero" name="ntercero" value="<?php echo $_POST['ntercero']?>" style="width:100%;" onKeyUp="return tabular(event,this)" readonly>
												<input type="hidden" value="0" name="bt">
												<input type="hidden" id="cb" name="cb" value="<?php echo $_POST['cb']?>" >
												<input type="hidden" id="ct" name="ct" value="<?php echo $_POST['ct']?>" >
												<input type="hidden" value="1" name="oculto" id="oculto">
												<input type="hidden" name="cobrorecibo" value="<?php echo $_POST['cobrorecibo']?>" >
												<input type="hidden" name="vcobrorecibo" value="<?php echo $_POST['vcobrorecibo']?>" >
												<input type="hidden" name="tcobrorecibo" value="<?php echo $_POST['tcobrorecibo']?>" > 
											</td>
										</tr>
										<tr>
											<td class="saludo1">Cod Ingreso:</td>
											<td>
												<input type="text" id="codingreso" name="codingreso" value="<?php echo $_POST['codingreso']?>" style="width:100%;" onKeyUp="return tabular(event,this)" onchange="buscaing(event)" onDblClick="despliegamodal2('visible','2');" class="colordobleclik" autocomplete="off"> 
											</td>
											<td colspan="8">
												<input type="hidden" name="bin" value="0">
												<input type="text" name="ningreso" id="ningreso" value="<?php echo $_POST['ningreso']?>" style="width:100%;" readonly>
												<input type="hidden" name="conciva" value="<?php echo $_POST['conciva']?>">
												<input type="hidden" name="vconcepto" value="<?php echo $_POST['vconcepto']?>">
											</td>
										</tr>
										<tr>
											<td class="saludo1">Fuente:</td>
											<td>
												<input type="text" id="fuente" name="fuente" value="<?php echo $_POST['fuente'] ?>" style="width:100%;" onDblClick="despliegaModalFuentes('visible')" title="Listado de fuentes" class="colordobleclik" autocomplete="off">
											</td>
											<td colspan="8">
												<input type="text" name="nfuente" id="nfuente" value="<?php echo $_POST['nfuente']?>" style="width:100%;" readonly>
											</td>
										</tr>
										<tr>
											<td class="saludo1" >Valor:</td>
											<td>
												<input type="text" id="valor" name="valor" value="<?php echo $_POST['valor']?>" style="width:100%;" onKeyDown ="return tabular(event,this)" >
											</td>
											<td colspan="2" style="padding-bottom:0px"><em class="botonflechaverde" onClick="agregardetalle();">Agregar</em></td>
											<input type="hidden" value="0" name="agregadet">
										</tr>
									</table>
								</td>
							</tr>
						</table>
						<?php
							//***** busca tercero
							if($_POST['bt'] == '1') {
								$nresul = buscatercero($_POST['tercero']);
								if($nresul != '') {
									$_POST['ntercero'] = $nresul;
									echo"
									<script>
										document.getElementById('codingreso').focus();
										document.getElementById('codingreso').select();
									</script>";
								}
								else {
									$_POST['ntercero'] = "";
									echo"
									<script>
										Swal.fire({
											icon: 'error',
											title: 'Error!',
											text: 'Tercero Incorrecto o no Existe',
											confirmButtonText: 'Continuar',
											confirmButtonColor: '#FF121A',
											timer: 2500
										});
										document.form2.tercero.focus();	
									</script>";
								}
							}
							//*** ingreso
						?>
						<div class="subpantallac7" style="height:45%; width:99.5%; overflow-x:hidden;">
							<table class="inicio">
								<tr>
									<td colspan="8" class="titulos">Detalle Liquidacion Recaudos</td>
								</tr>                  
								<tr class="titulos2">
									<td style="width: 5%;">C&oacute;digo</td>
									<td>Ingreso</td>
									<td style="width: 10%;">Centro Costo</td>
									<td style="width: 15%;">Fuente</td>
									<td style="width: 15%;">Valor</td>
									<td style="width: 15%;">Iva</td>
									<td style="width: 15%;">Total</td>
									<td style="width: 5%;">
										<img src="imagenes/del.png" >
										<input type='hidden' name='elimina' id='elimina'>
									</td>
								</tr>
								<?php 		
									if ($_POST['elimina']!='') { 
										$posi=$_POST['elimina'];
										unset($_POST['dcoding'][$posi]);
										unset($_POST['dncoding'][$posi]);
										unset($_POST['dncc'][$posi]);
										unset($_POST['dfuente'][$posi]); 
										unset($_POST['dvalores'][$posi]);
										unset($_POST['dconciva'][$posi]);
										unset($_POST['dvconcepto'][$posi]);
										unset($_POST['dvcausacion'][$posi]);
										unset($_POST['dporceniva'][$posi]);
										unset($_POST['dnombreiva'][$posi]);
										unset($_POST['dvaloriva'][$posi]);
										unset($_POST['dvalorestotal'][$posi]);
										$_POST['dcoding'] = array_values($_POST['dcoding']);
										$_POST['dncoding'] = array_values($_POST['dncoding']);
										$_POST['dncc'] = array_values($_POST['dncc']);
										$_POST['dfuente'] = array_values($_POST['dfuente']);
										$_POST['dvalores'] = array_values($_POST['dvalores']);
										$_POST['dconciva'] = array_values($_POST['dconciva']);
										$_POST['dvconcepto'] = array_values($_POST['dvconcepto']);
										$_POST['dvcausacion'] = array_values($_POST['dvcausacion']);
										$_POST['dporceniva'] = array_values($_POST['dporceniva']);
										$_POST['dnombreiva'] = array_values($_POST['dnombreiva']);
										$_POST['dvaloriva'] = array_values($_POST['dvaloriva']);
										$_POST['dvalorestotal'] = array_values($_POST['dvalorestotal']);
									}
									if ($_POST['agregadet'] == '1') {
										$_POST['dcoding'][] = $_POST['codingreso'];
										$_POST['dncoding'][] = $_POST['ningreso'];
										$_POST['dncc'][] = $_POST['cc'];	
										$_POST['dfuente'][] = $_POST['fuente'];	
										$_POST['valor'] = str_replace(",","",$_POST['valor']);		 		
										$_POST['dconciva'][] = $_POST['conciva'];
										$_POST['dvconcepto'][] = $_POST['vconcepto'];
										$_POST['dvcausacion'][] = $_POST['causacion'];
										$_POST['dvalorestotal'][] = $_POST['valor'];
										//IVA
										if($_POST['iva19'] == 'S'){
											$sqliva = "SELECT porcentaje, nombre FROM conceptoscontables WHERE modulo = '4' AND tipo = 'IV' AND codigo = '".$_POST['conciva']."'";
											$resiva = mysqli_query($linkbd,$sqliva);
											$rowiva = mysqli_fetch_row($resiva);
											if($rowiva[0] != ''){
												$valporiva = $rowiva[0];
											}else{
												$valporiva = 0;
											}
											$valivacal = ($valporiva + 100)/100;
											$_POST['dporceniva'][] = $valporiva;
											$_POST['dnombreiva'][] = $rowiva[1];
											$_POST['dvaloriva'][] = $valoriva = floatval($_POST['valor']/$valivacal) * (floatval($valporiva) / 100);
										}else{
											$_POST['dporceniva'][] = 0;
											$_POST['dnombreiva'][] = '';
											$_POST['dvaloriva'][] = $valoriva = 0;
										}
										$_POST['dvalores'][] = $_POST['valor'] - $valoriva;
										
										$_POST['agregadet'] = 0;
										echo"
										<script>
											document.form2.codingreso.value = '';
											document.form2.valor.value = '0';	
											document.form2.fuente.value = '';	
											document.form2.ningreso.value = '';
											document.form2.cc.value = '';
											document.form2.conciva.value = '';
											document.form2.vconcepto.value = '';
											document.form2.codingreso.select();
											document.form2.codingreso.focus();	
										</script>";
									}
									$iter='saludo1a';
									$iter2='saludo2';
									for ($x=0;$x<count($_POST['dcoding']);$x++){	
										echo "
										<input type='hidden' name='dcoding[]' value='".$_POST['dcoding'][$x]."'>
										<input type='hidden' name='dncoding[]' value='".$_POST['dncoding'][$x]."'>
										<input type='hidden' name='dncc[]' value='".$_POST['dncc'][$x]."'>
										<input type='hidden' name='dfuente[]' value='".$_POST['dfuente'][$x]."'>
										<input type='hidden' name='dvalores[]' value='".$_POST['dvalores'][$x]."'>
										<input type='hidden' name='dconciva[]' value='".$_POST['dconciva'][$x]."'>
										<input type='hidden' name='dporceniva[]' value='".$_POST['dporceniva'][$x]."'>
										<input type='hidden' name='dnombreiva[]' value='".$_POST['dnombreiva'][$x]."'>
										<input type='hidden' name='dvaloriva[]' value='".$_POST['dvaloriva'][$x]."'>
										<input type='hidden' name='dvconcepto[]' value='".$_POST['dvconcepto'][$x]."'>
										<input type='hidden' name='dvcausacion[]' value='".$_POST['dvcausacion'][$x]."'>
										<input type='hidden' name='dvalorestotal[]' value='".$_POST['dvalorestotal'][$x]."'>
										<tr class='$iter'>
											<td>".$_POST['dcoding'][$x]."</td>
											<td>".$_POST['dncoding'][$x]."</td>
											<td>".$_POST['dncc'][$x]."</td>
											<td>".$_POST['dfuente'][$x]."</td>
											<td style='text-align:right;'>".number_format($_POST['dvalores'][$x],2,".",",")."</td>
											<td style='text-align:right;'>".number_format($_POST['dvaloriva'][$x],2,".",",")."</td>
											<td style='text-align:right;'>".number_format($_POST['dvalorestotal'][$x],2,".",",")."</td>
											
										
											<td>
												<a href='#' onclick='eliminar($x)'><img src='imagenes/del.png'></a>
											</td>
										</tr>";
										$aux = $iter;
										$iter = $iter2;
										$iter2 = $aux;	 
									}
									$total1 = array_sum($_POST['dvalores']);
									$total2 = array_sum($_POST['dvaloriva']);
									$total3 = array_sum($_POST['dvalorestotal']);
									$resultado = convertir($total3);
									$_POST['totalcf'] = number_format($total1,2,".",",");
									$ftotaliva = number_format($total2,2,".",",");
									$ftotalgl = number_format($total3,2,".",",");
									$_POST['letras'] = $resultado." Pesos";
									echo "
										<input type='hidden' name='totalc' value='$total1'>
										<input type='hidden' name='totaliva' value='$total2'>
										<input type='hidden' name='totalgl' value='$total3'>
										<input type='hidden' name='totalcf' value='$_POST[totalcf]'>
										
										<tr>
											<td class='saludo2' style='text-align:right;' colspan='4'>Total : </td>
											<td class='saludo1' style='text-align:right;'>$_POST[totalcf]</td>
											<td class='saludo1' style='text-align:right;'>$ftotaliva</td>
											<td class='saludo1' style='text-align:right;'>$ftotalgl</td>
										</tr>
										<tr>
											<td class='saludo1'>Son:</td>
											<td colspan='5'>
												<input name='letras' type='text' value='$_POST[letras]' >
											</td>
										</tr>";
								?> 
							</table>
						</div>
					</div>
				</div>
				<div class="tab">
					<input type="radio" id="tab-2" name="tabgroup1" value="2" <?php echo $check2;?> >
					<label id="clabel" for="tab-2">Retenciones</label>
					<div class="content" style="overflow-x:hidden;"> 
						<?php
							if(!$_POST['calculaRetencion']){
								$_POST['calculaRetencion'] = 0;
							}
						?>
						<table class="inicio ancho">
							<tr>
								<td class="titulos" colspan="10">Retenciones</td>
								<td class="cerrar" style="width:7%" onClick="location.href='teso-principal.php'">Cerrar</td>
							</tr>
							<tr>
								<td class="saludo1" style="width:12%;">Retencion y Descuento:</td>
								<td style="width:15%;">
									<select name="retencion" onChange="validar()" onKeyUp="return tabular(event,this)">
										<option value="">Seleccione ...</option>
										<?php
											$sqlr="SELECT * FROM tesoretenciones WHERE estado='S'";
											$res = mysqli_query($linkbd, $sqlr);
											while ($row = mysqli_fetch_row($res)){
												if("$row[0]" == $_POST['retencion']){
													echo "<option value='$row[0]' SELECTED>$row[1] - $row[2]</option>";
													$_POST['nretencion'] = $row[1]." - ".$row[2];
													if($_POST['porcentaje'] == ''){
														$_POST['porcentaje'] = $row[5];
													}
												}else {
													echo "<option value='$row[0]'>$row[1] - $row[2]</option>";
												} 	 
											}
										?>
									</select>
									<input type="hidden" value="<?php echo $_POST['nretencion']?>" name="nretencion">
								</td>
								<input type="hidden" id="contador" name="contador"  value="<?php echo $_POST['contador']; ?>" >
								<input type="hidden" id="oculto12" name="oculto12"  value="<?php echo $_POST['oculto12']; ?>">
								<input type="hidden" name="estado"  value="<?php echo $_POST['estado']; ?>" >
								<td style="width:8%;" class="saludo1">Porcentaje:</td>
								<td style="width:6%;"><input type="text" d="porcentaje" name="porcentaje" value="<?php echo $_POST['porcentaje']?>"></td>
								<td class="saludo1" style="width:4%;">Valor:</td>
								<td style="width:10%;">
									<input type="text" id="vporcentaje" name="vporcentaje" value="<?php echo $_POST['vporcentaje']?>" >
									<input type="hidden" name="calculaRetencion" id="calculaRetencion" value="<?php echo $_POST['calculaRetencion'] ?>">
								</td>
								<td ><input type='hidden' id="totaldes" name="totaldes" value="<?php echo $_POST['totaldes']?>" readonly></td>
								<td colspan="2" style="padding-bottom:0px"><em class="botonflechaverde" onClick="agregardetalled();">Agregar</em></td>
							</tr>
							<input type="hidden" name="agregadetdes" value="0" >
							<?php
								if($_POST['calculaRetencion'] == 0){
									$_POST['ddescuentos'] = [];
									$_POST['dndescuentos'] = [];
									$_POST['dporcentajes'] = [];
									$_POST['ddesvalores'] = [];
									
									$valorRetencion = 0;
									$sqlrRetencion = "SELECT idretencion, porcentaje FROM ccpetdc_retenciones WHERE iddestino = '$_POST[idDestinoCompra]'";
									$resRetencion = mysqli_query($linkbd, $sqlrRetencion);
									while($rowRetencion = mysqli_fetch_row($resRetencion)){
										$sqlr = "SELECT iva FROM tesoretenciones WHERE id = '$rowRetencion[0]' AND estado = 'S'";
										$res = mysqli_query($linkbd, $sqlr);
										$row = mysqli_fetch_row($res);
										if($row[0] == 1){
											$valorRetencion = round((doubleVal($_POST['iva']) * doubleVal($rowRetencion[1])) / 100);
										}
										else{
											$valorRetencion = round((doubleVal($_POST['base']) * doubleVal($rowRetencion[1])) / 100);
										}

										
										$_POST['ddescuentos'][] = $rowRetencion[0];
										$_POST['dndescuentos'][] = buscaRetencionConCodigo($rowRetencion[0]);
										$_POST['dporcentajes'][] = $rowRetencion[1];
										$_POST['ddesvalores'][] = $valorRetencion;
										
									}
								}
								$_POST['valoregreso'] = $_POST['valor'];
								$_POST['valorretencion'] = $_POST['totaldes'];
								$_POST['valorcheque'] = doubleVal($_POST['valoregreso'])-doubleVal($_POST['valorretencion']);
								if ($_POST['eliminad'] != ''){ 
									$posi = $_POST['eliminad'];
									unset($_POST['ddescuentos'][$posi]);
									unset($_POST['dndescuentos'][$posi]);
									unset($_POST['dporcentajes'][$posi]);
									unset($_POST['ddesvalores'][$posi]);
									$_POST['ddescuentos'] = array_values($_POST['ddescuentos']); 
									$_POST['dndescuentos'] = array_values($_POST['dndescuentos']); 
									$_POST['dporcentajes'] = array_values($_POST['dporcentajes']); 
									$_POST['ddesvalores'] = array_values($_POST['ddesvalores']); 		 	 
								}	 
								if ($_POST['agregadetdes'] == '1'){
									$_POST['ddescuentos'][] = $_POST['retencion'];
									$_POST['dndescuentos'][] = $_POST['nretencion'];
									$_POST['dporcentajes'][] = $_POST['porcentaje'];
									$_POST['ddesvalores'][] = $_POST['vporcentaje'];
									$_POST['agregadetdes'] = '0';
									echo"
									<script>
										document.form2.porcentaje.value = '';
										document.form2.vporcentaje.value = 0;	
										document.form2.retencion.value = '';	
									</script>";
								}
							?>
						</table>
						<table class="inicio" style="overflow:scroll">
							<tr>
								<td class="titulos">Descuento</td>
								<td class="titulos">%</td>
								<td class="titulos">Valor</td>
								<td class="titulos2"><img src="imagenes/del.png" ><input type='hidden' name='eliminad' id='eliminad'></td>
							</tr>
							<?php
								$totaldes = 0;
								$iter = 'saludo1a';
								$iter2 = 'saludo2';
								if(isset($_POST['ddescuentos'])){
									for ($x = 0; $x<count($_POST['ddescuentos']); $x++){
										echo "
										<input type='hidden' name='dndescuentos[]' value='".$_POST['dndescuentos'][$x]."'>
										<input type='hidden' name='ddescuentos[]' value='".$_POST['ddescuentos'][$x]."' >
										<input type='hidden' name='dporcentajes[]' value='".$_POST['dporcentajes'][$x]."'>
										<input type='hidden' name='ddesvalores[]' value='".$_POST['ddesvalores'][$x]."'>
										<tr class='$iter'>
											<td>".$_POST['dndescuentos'][$x]."</td>
											<td>".$_POST['dporcentajes'][$x]."</td>
											<td>".$_POST['ddesvalores'][$x]."</td>
											<td><a href='#' onclick='eliminard($x)'><img src='imagenes/del.png'></a></td>
										</tr>";
										$totaldes = $totaldes+($_POST['ddesvalores'][$x]);
										$aux = $iter;
										$iter = $iter2;
										$iter2 = $aux;
									}
								}
								if(isset($totaldes)){
									$_POST['valorretencion'] = $totaldes;
									echo"
									<script>
										document.form2.totaldes.value='$totaldes';		
										document.form2.valorretencion.value='$totaldes';
									</script>";
									echo "<tr>
										<td colspan = '2' style='font-size:18px; text-align: center; color:gray !important; ' class='saludo1'>TOTAL:</td>
										<td class='saludo1'>
											$totaldes
										</td>
										<td></td>
									</tr>";	
								}
								$_POST['valorcheque'] = doubleVal($_POST['valoregreso'])-doubleVal($_POST['valorretencion']);
							?>
						</table>
					</div>
				</div>
			</div>
			<?php
				if($_POST['oculto']=='2') {
					preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST['fecha'],$fecha);
					$fechaf=$fecha[3]."-".$fecha[2]."-".$fecha[1];
					$bloq=bloqueos($_SESSION['cedulausu'],$fechaf);	
					if($bloq>=1) {
						$consec = selconsecutivo('tesobienesservicios','id_recaudo');
						if($_POST['causacion'] == '2') {
							$_POST['concepto'] = "ESTE DOCUMENTO NO REQUIERE CAUSACION CONTABLE - ".$_POST['concepto'];
						}
						$sqlr="INSERT INTO comprobante_cab (numerotipo, tipo_comp, fecha, concepto, total, total_debito, total_credito, diferencia, estado) VALUES ($consec, 41, '$fechaf', '".strtoupper($_POST['concepto'])."', 0, $_POST[totalc], $_POST[totalc], 0, '1')";
						mysqli_query($linkbd,$sqlr);
						
						//******************* DETALLE DEL COMPROBANTE CONTABLE *********************
						if($_POST['causacion']!='2'){
							for($x=0;$x<count($_POST['dcoding']);$x++){
								$cuentaiva = '';
								$sql1 = "SELECT * FROM conceptoscontables_det WHERE modulo = '4' AND tipo = 'C' AND fechainicial < '$fechaf' AND cc='".$_POST['dncc'][$x]."' AND codigo = '".$_POST['dvconcepto'][$x]."'AND cuenta != '' ORDER BY fechainicial asc";
								$res1 = mysqli_query($linkbd,$sql1);
								while($row1 = mysqli_fetch_row($res1)){
									if($row1[6] == 'S'){				 
										$valordeb = $_POST['dvalores'][$x];
										$valorcred = 0;
										$cuentaiva = $row1[4];	
									}else{
										$valorcred = $_POST['dvalores'][$x];
										$valordeb = 0;
									}
									$sqlv="INSERT INTO comprobante_det (id_comp,cuenta, tercero, centrocosto, detalle, cheque, valdebito, valcredito, estado, vigencia) VALUES ('41 $consec', '".$row1[4]."', '".$_POST['tercero']."', '".$row1[5]."', 'Causacion ".strtoupper($_POST['dncoding'][$x])."', '', ".$valordeb.", ".$valorcred.", '1', '".$_POST['vigencia']."')";
									mysqli_query($linkbd,$sqlv);
								}
								//IVA
								if($_POST['iva19'] == 'S'){
									$sqliva = "SELECT porcentaje FROM conceptoscontables WHERE modulo = '4' AND tipo = 'IV' AND codigo = '".$_POST['dconciva'][$x]."'";
									$resiva = mysqli_query($linkbd,$sqliva);
									$rowiva = mysqli_fetch_row($resiva);
									$valporiva = $rowiva[0];
									$valtotaliva = floatval($_POST['dvalores'][$x]) * (floatval($valporiva) / 100);
									if($valtotaliva > 0){
										$sql2 = "SELECT  * FROM conceptoscontables_det WHERE credito = 'S' AND modulo = '4' AND tipo = 'IV' AND fechainicial < '$fechaf' AND cc = '".$_POST['dncc'][$x]."' AND codigo = '".$_POST['dconciva'][$x]."'AND cuenta != '' ORDER BY fechainicial asc";
										$res2 = mysqli_query($linkbd,$sql2);
										$row2 = mysqli_fetch_row($res2);

										$sqlv="INSERT INTO comprobante_det (id_comp, cuenta, tercero, centrocosto, detalle, cheque, valdebito, valcredito, estado, vigencia) VALUES ('41 $consec', '".$row2[4]."', '".$_POST['tercero']."', '".$row2[5]."', 'IVA ".$_POST['dconciva'][$x]." ".strtoupper($_POST['dncoding'][$x])."', '', 0, $valtotaliva, '1', '".$_POST['vigencia']."')";
										mysqli_query($linkbd,$sqlv);

										$sqlv="INSERT INTO comprobante_det (id_comp, cuenta, tercero, centrocosto, detalle, cheque, valdebito, valcredito, estado, vigencia) VALUES ('41 $consec', '$cuentaiva', '".$_POST['tercero']."', '".$row2[5]."', 'IVA ".$_POST['dconciva'][$x]." ".strtoupper($_POST['dncoding'][$x])."', '', $valtotaliva, 0, '1', '".$_POST['vigencia']."')";
										mysqli_query($linkbd,$sqlv);
									}
								}
								//Retenciones
								for($xy=0;$xy<count($_POST['ddescuentos']);$xy++){
									$codrete = substr($_POST['dndescuentos'][$xy], 0, 2);
									$sql3 = "SELECT * FROM conceptoscontables_det WHERE debito = 'S' AND modulo = '4' AND tipo = 'RI' AND fechainicial < '$fechaf' AND cc = '".$_POST['dncc'][$x]."' AND codigo = '$codrete' AND cuenta != ''";
									$res3 = mysqli_query($linkbd,$sql3);
									$row3 = mysqli_fetch_row($res3);

									$sqlr="INSERT INTO comprobante_det (id_comp, cuenta, tercero, centrocosto, detalle, cheque, valdebito, valcredito, estado, vigencia) VALUES ('41 $consec', '".$row3[4]."', '".$_POST['tercero']."', '".$row3[5]."', 'Retención $codrete ".strtoupper($_POST['dncoding'][$x])."', '', ".$_POST['ddesvalores'][$xy].", 0, '1', '".$_POST['vigencia']."')";
									mysqli_query($linkbd,$sqlr);

									$sqlr="INSERT INTO comprobante_det (id_comp, cuenta, tercero, centrocosto, detalle, cheque, valdebito, valcredito, estado, vigencia) VALUES ('41 $consec', '$cuentaiva', '".$_POST['tercero']."', '".$row3[5]."', 'Retención $codrete ".strtoupper($_POST['dncoding'][$x])."', '', 0, ".$_POST['ddesvalores'][$xy].", '1', '".$_POST['vigencia']."')";
									mysqli_query($linkbd,$sqlr);
								}
							}	
						}
						//************ insercion de cabecera recaudos ************
						$sqlr="INSERT INTO tesobienesservicios (id_recaudo, id_comp, fecha, vigencia, tercero, valortotal, concepto, estado, cc, tipo_causacion) VALUES ($consec, $consec, '$fechaf', ".$_POST['vigencia'].", '$_POST[tercero]', '$_POST[totalc]', '".strtoupper($_POST['concepto'])."', 'S', '".$_POST['cc']."', '".$_POST['causacion']."')";	  
						mysqli_query($linkbd,$sqlr);
						//$idrec=mysqli_insert_id($linkbd); 
						//************** insercion de consignaciones **************
						for($x=0;$x<count($_POST['dcoding']);$x++){
							$sqlr = "INSERT INTO tesobienesservicios_det (id_recaudo, ingreso, valor, estado, cc, fuente, iva) VALUES ($consec, '".$_POST['dcoding'][$x]."', ".$_POST['dvalores'][$x].", 'S', '".$_POST['dncc'][$x]."', '".$_POST['dfuente'][$x]."', '".$_POST['dvaloriva'][$x]."')";	  
							if (!mysqli_query($linkbd,$sqlr)){
								echo "<table class='inicio'><tr><td class='saludo1'><center><font color=blue><img src='imagenes/alert.png'> Manejador de Errores de la Clase BD<br><font ></font></font><br><p align=center>No se pudo ejecutar la petición: <br><font color=red><b>$sqlr</b></font></p>";
								echo "Ocurrió el siguiente problema:<br>";
								echo "<pre>";
								echo "</pre></center></td></tr></table>";
							}else{
								echo "
								<script>
									Swal.fire({
										icon: 'success',
										title: 'Se ha almacenado el Recaudo con Exito',
										confirmButtonText: 'Continuar',
										confirmButtonColor: '#FF121A',
										timer: 3000
									})
								</script>";
							}
						}
						for($x=0;$x<count($_POST['ddescuentos']);$x++){
							$sqlr = "INSERT INTO tesobienesservicios_retenciones (id_retencion, id_orden, porcentaje, valor, estado, causacion, pasa_ppto) VALUES ('".$_POST['ddescuentos'][$x]."', $consec, '".$_POST['dporcentajes'][$x]."', ".$_POST['ddesvalores'][$x].", 'S', '$causacion', '$estado')";
							mysqli_query($linkbd, $sqlr);
						}	
					}else{
						echo "
						<script>
							Swal.fire({
								icon: 'error',
								title: 'Error!',
								text: 'No Tiene los Permisos para Modificar este Documento',
								confirmButtonText: 'Continuar',
								confirmButtonColor: '#FF121A',
								timer: 2500
							});
						</script>";
					} 
				}
			?>	
			<div id="bgventanamodal2">
				<div id="ventanamodal2">
					<IFRAME  src="" name="buster" marginWidth=0 marginHeight=0 frameBorder=0 id="ventana2" frameSpacing=0 style="left:500px; width:900px; height:500px; top:200;"> 
					</IFRAME>
				</div>
			</div>
		</form>
	</body>
</html> 		