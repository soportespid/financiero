<?php  
	require_once 'PHPExcel/Classes/PHPExcel.php';
	require"comun.inc";
	require"funciones.inc";
	session_start();
	$linkbd=conectar_bd();  
	$objPHPExcel = new PHPExcel();
	//----Propiedades---- 
	$objPHPExcel->getProperties()
 		->setCreator("SPID")
        ->setLastModifiedBy("SPID")
        ->setTitle("Exportar Excel con PHP")
        ->setSubject("Documento de prueba")
        ->setDescription("Documento generado con PHPExcel")
        ->setKeywords("usuarios phpexcel")
        ->setCategory("reportes");
	//----Cuerpo de Documento----
	$objPHPExcel->getActiveSheet()->mergeCells('A1:K1');
	$objPHPExcel->setActiveSheetIndex(0)
            	->setCellValue('A1', 'Listado Usuarios');
	$objFont=$objPHPExcel->getActiveSheet()->getStyle('A1')->getFont();
	$objFont->setName('Courier New'); 
	$objFont->setSize(15); 
	$objFont->setBold(true); 
	$objFont->setUnderline(PHPExcel_Style_Font::UNDERLINE_SINGLE);
	$objFont->getColor()->setARGB( PHPExcel_Style_Color::COLOR_BLACK);
	$objAlign=$objPHPExcel->getActiveSheet()->getStyle('A1')->getAlignment(); 
	$objAlign->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); 
	$objAlign->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER); 
	$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A2', 'CODIGO')
            ->setCellValue('B2', 'TERCERO')
            ->setCellValue('C2', 'DIRECCION')
            ->setCellValue('D2', 'COD. CATASTRAL')
			->setCellValue('E2', 'ESTRATO')
            ->setCellValue('F2', 'ZONA')
            ->setCellValue('G2', 'LADO')
			->setCellValue('H2', 'BARRIO')
			->setCellValue('I2', 'ACUEDUCTO')
			->setCellValue('J2', 'ALCANTARILLADO')
			->setCellValue('K2', 'ASEO');
	$linkbd=conectar_bd();
	$crit1=$crit2=$crit3="";
	$sumceldas=$_POST[cel01] + $_POST[cel02] + $_POST[cel03] + $_POST[cel04] + $_POST[cel05] + $_POST[cel06] + $_POST[cel07] + $_POST[cel08];
	if ($sumceldas >0){$ord00="ORDER BY";}
	else {$ord00="";}
	if($_POST[cel01]==0){$cl01='titulos3';$ord01="";$ico01="";}
	else 
	{
		$cl01='celactiva';
		if($_POST[cel01]==1){$ord01="codigo ASC";$ico01="<img src='imagenes/bullet_arrow_up.png' style='width:24px;'/>";}
		else {$ord01="codigo DESC"; $ico01="<img src='imagenes/bullet_arrow_down.png' style='width:24px;'/>";}
	}
	if($_POST[cel02]==0){$cl02='titulos3';$ord02="";$ico02="";}
	else
	{
		$cl02='celactiva';
		if ($_POST[cel01]>0)
		{
			if($_POST[cel02]==1){$ord02=",nombretercero ASC"; $ico02="<img src='imagenes/bullet_arrow_up.png'  style='width:24px;'/>";}
			else {$ord02=",nombretercero DESC"; $ico02="<img src='imagenes/bullet_arrow_down.png' style='width:24px;'/>";}
		}
		else 
		{
			if($_POST[cel02]==1){$ord02="nombretercero ASC"; $ico02="<img src='imagenes/bullet_arrow_up.png' style='width:24px;'/>";}
			else {$ord02="nombretercero DESC"; $ico02="<img src='imagenes/bullet_arrow_down.png' style='width:24px;'/>";}
		}
	}
	if($_POST[cel03]==0){$cl03='titulos3';$ord03="";$ico03="";}
	else
	{
		$cl03='celactiva';
		if ($_POST[cel01]>0 || $_POST[cel02]>0)
		{
			if($_POST[cel03]==1){$ord03=",direccion ASC"; $ico03="<img src='imagenes/bullet_arrow_up.png'  style='width:24px;'/>";}
			else {$ord03=",direccion DESC"; $ico03="<img src='imagenes/bullet_arrow_down.png' style='width:24px;'/>";}
		}
		else 
		{
			if($_POST[cel03]==1){$ord03="direccion ASC"; $ico03="<img src='imagenes/bullet_arrow_up.png' style='width:24px;'/>";}
			else {$ord03="direccion DESC"; $ico03="<img src='imagenes/bullet_arrow_down.png' style='width:24px;'/>";}
		}
	}
	if($_POST[cel04]==0){$cl04='titulos3';$ord04="";$ico04="";}
	else
	{
		$cl04='celactiva';
		if ($_POST[cel01]>0 || $_POST[cel02]>0 || $_POST[cel03]>0) 
		{
			if($_POST[cel04]==1){$ord04=",codcatastral ASC"; $ico04="<img src='imagenes/bullet_arrow_up.png'  style='width:24px;'/>";}
			else {$ord04=",codcatastral DESC"; $ico04="<img src='imagenes/bullet_arrow_down.png' style='width:24px;'/>";}
		}
		else 
		{
			if($_POST[cel04]==1){$ord04="codcatastral ASC"; $ico04="<img src='imagenes/bullet_arrow_up.png' style='width:24px;'/>";}
			else {$ord04="codcatastral DESC"; $ico04="<img src='imagenes/bullet_arrow_down.png' style='width:24px;'/>";}
		}
	}
	if($_POST[cel05]==0){$cl05='titulos3';$ord05="";$ico05="";}
	else
	{
		$cl05='celactiva';
		if ($_POST[cel01]>0 || $_POST[cel02]>0 || $_POST[cel03]>0 || $_POST[cel04]>0 ) 
		{
			if($_POST[cel05]==1){$ord05=",estrato ASC"; $ico05="<img src='imagenes/bullet_arrow_up.png'  style='width:24px;'/>";}
			else {$ord05=",estrato DESC"; $ico05="<img src='imagenes/bullet_arrow_down.png' style='width:24px;'/>";}
		}
		else 
		{
			if($_POST[cel05]==1){$ord05="estrato ASC"; $ico05="<img src='imagenes/bullet_arrow_up.png' style='width:24px;'/>";}
			else {$ord05="estrato DESC"; $ico05="<img src='imagenes/bullet_arrow_down.png' style='width:24px;'/>";}
		}
	}
	if($_POST[cel06]==0){$cl06='titulos3';$ord06="";$ico06="";}
	else
	{
		$cl06='celactiva';
		if ($_POST[cel01]>0 || $_POST[cel02]>0 || $_POST[cel03]>0 || $_POST[cel04]>0 || $_POST[cel05]>0 ) 
		{
			if($_POST[cel06]==1){$ord06=",zona ASC"; $ico06="<img src='imagenes/bullet_arrow_up.png'  style='width:24px;'/>";}
			else {$ord06=",zona DESC"; $ico06="<img src='imagenes/bullet_arrow_down.png' style='width:24px;'/>";}
		}
		else 
		{
			if($_POST[cel07]==1){$ord06="zona ASC"; $ico06="<img src='imagenes/bullet_arrow_up.png' style='width:24px;'/>";}
			else {$ord06="zona DESC"; $ico06="<img src='imagenes/bullet_arrow_down.png' style='width:24px;'/>";}
		}
	}
	if($_POST[cel07]==0){$cl07='titulos3';$ord07="";$ico07="";}
	else
	{
		$cl07='celactiva';
		if ($_POST[cel01]>0 || $_POST[cel02]>0 || $_POST[cel03]>0 || $_POST[cel04]>0 || $_POST[cel05]>0 || $_POST[cel06]>0) 
		{
			if($_POST[cel07]==1){$ord07=",lado ASC"; $ico07="<img src='imagenes/bullet_arrow_up.png'  style='width:24px;'/>";}
			else {$ord07=",lado DESC"; $ico07="<img src='imagenes/bullet_arrow_down.png' style='width:24px;'/>";}
		}
		else 
		{
			if($_POST[cel07]==1){$ord07="lado ASC"; $ico07="<img src='imagenes/bullet_arrow_up.png' style='width:24px;'/>";}
			else {$ord07="lado DESC"; $ico07="<img src='imagenes/bullet_arrow_down.png' style='width:24px;'/>";}
		}
	}
	if($_POST[cel08]==0){$cl08='titulos3';$ord08="";$ico08="";}
	else
	{
		$cl08='celactiva';
		if ($_POST[cel01]>0 || $_POST[cel02]>0 || $_POST[cel03]>0 || $_POST[cel04]>0 || $_POST[cel05]>0 || $_POST[cel06]>0 || $_POST[cel07]>0) 
		{
			if($_POST[cel08]==1){$ord08=",barrio ASC"; $ico08="<img src='imagenes/bullet_arrow_up.png'  style='width:24px;'/>";}
			else {$ord08=",barrio DESC"; $ico08="<img src='imagenes/bullet_arrow_down.png' style='width:24px;'/>";}
		}
		else 
		{
			if($_POST[cel08]==1){$ord08="barrio ASC"; $ico08="<img src='imagenes/bullet_arrow_up.png' style='width:24px;'/>";}
			else {$ord08="barrio DESC"; $ico08="<img src='imagenes/bullet_arrow_down.png' style='width:24px;'/>";}
		}
	}
	if ($_POST[numero]!=""){$crit1=" and servclientes.codigo like '%".$_POST[numero]."%' ";}
	if ($_POST[nombre]!=""){$crit2=" and servclientes.codcatastral like '%".$_POST[nombre]."%'  ";}
	if ($_POST[annos]!=""){ $crit3="AND YEAR(fechacreacion) <= $_POST[annos]";}
	$sqlr="SELECT codigo, nombretercero, direccion, codcatastral, barrio, estrato, zona, lado FROM servclientes WHERE servclientes.estado <> ' ' $crit1 $crit2 $crit3 $ord00 $ord01 $ord02 $ord03 $ord04 $ord05 $ord06 $ord07 $ord08 $cond2";
	$resp = mysql_query($sqlr,$linkbd);
	$i=3;
	while ($row =mysql_fetch_row($resp))
	{
		$barrio=$estrato="";
		$sqlr01="SELECT nombre FROM servbarrios WHERE id='$row[4]'";
		$res01=mysql_query($sqlr01,$linkbd);
		$row01 =mysql_fetch_row($res01);
		$barrio=utf8_encode($row01[0]);
		$sqlr01="SELECT tipo, descripcion FROM servestratos WHERE id='$row[5]'";
		$res01=mysql_query($sqlr01,$linkbd);
		$row01 =mysql_fetch_row($res01);
		$estrato="$row01[0] - $row01[1]";
		$ser01=$ser02=$ser03="NO";
		$sqlr01="SELECT servicio FROM  terceros_servicios WHERE consecutivo='$row[0]'";
		$res01=mysql_query($sqlr01,$linkbd);
		while ($row01 =mysql_fetch_row($res01)) 
		{
			switch ($row01[0]) 
			{
				case "01": $ser01="SI";break;
				case "02": $ser02="SI";break;
				case "03": $ser03="SI";
			}
		}
		$objPHPExcel->setActiveSheetIndex(0)
		->setCellValueExplicit ("A".$i, str_pad($row[0],10,"0", STR_PAD_LEFT), PHPExcel_Cell_DataType :: TYPE_STRING)
		->setCellValueExplicit ("D".$i,$row[3], PHPExcel_Cell_DataType :: TYPE_STRING);
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1,$i,utf8_encode($row[1]));
    	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2,$i,utf8_encode($row[2]));
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4,$i,$estrato);
    	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(5,$i,$row[6]);
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(6,$i,$row[7]);
    	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(7,$i,$barrio);
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(8,$i,$ser01);
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(9,$i,$ser02);
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(10,$i,$ser03);
		$i++;
	}
	//----Propiedades de la hoja
	$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true); 
	$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true); 
	$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true); 
	$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true); 
	$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true); 
	$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true); 
	$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true); 
	$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true); 
	$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true); 
	$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true); 
	$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setAutoSize(true); 
	$objPHPExcel->getActiveSheet()->setTitle('Listado Usuarios');
	//----Guardar documento----
	header('Content-Type: application/vnd.ms-excel');
	header('Content-Disposition: attachment;filename="Listado Usuarios.xls"');
	header('Cache-Control: max-age=0');
	$objWriter=PHPExcel_IOFactory::createWriter($objPHPExcel,'Excel5');
	$objWriter->save('php://output');
	exit;
?>