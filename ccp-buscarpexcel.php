<?php
require_once 'PHPExcel/Classes/PHPExcel.php';
require "comun.inc";
require "funciones.inc";
require "validaciones.inc";
ini_set('max_execution_time',99999999);
header("Content-type: application/json");


$objPHPExcel = new PHPExcel();

//----Propiedades----
$objPHPExcel->getProperties()
        ->setCreator("IDEAl")
        ->setLastModifiedBy("IDEAL")
        ->setTitle("Exportar Excel con PHP")
        ->setSubject("Documento de prueba")
        ->setDescription("Documento generado con PHPExcel")
        ->setKeywords("usuarios phpexcel")
        ->setCategory("reportes");

//----Cuerpo de Documento----
$objPHPExcel->getActiveSheet()->mergeCells('A1:I1');
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A1', 'Registros Presupuestales');

$objFont=$objPHPExcel->getActiveSheet()->getStyle('A1')->getFont();
$objFont->setName('Courier New');
$objFont->setSize(15);
$objFont->setBold(true);
$objFont->setUnderline(PHPExcel_Style_Font::UNDERLINE_SINGLE);
$objFont->getColor()->setARGB( PHPExcel_Style_Color::COLOR_BLACK);

$objAlign=$objPHPExcel->getActiveSheet()->getStyle('A1')->getAlignment();
$objAlign->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objAlign->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
$objPHPExcel-> getActiveSheet ()
		-> getStyle ("A2:I2")
        -> getFill ()
        -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
        -> getStartColor ()
        -> setRGB ('A6E5F3');
$objPHPExcel-> getActiveSheet ()
		-> getStyle ("A1")
        -> getFill ()
        -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
        -> getStartColor ()
        -> setRGB ('A6E5F3');
$borders = array(
      'borders' => array(
        'allborders' => array(
          'style' => PHPExcel_Style_Border::BORDER_THIN,
          'color' => array('argb' => 'FF000000'),
        )
      ),
    );
$objPHPExcel->getActiveSheet()->getStyle('A2:I2')->applyFromArray($borders);
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A2', 'Vigencia')
            ->setCellValue('B2', 'No RP')
			->setCellValue('C2', 'No CDP')
            ->setCellValue('D2', 'Objeto')
            ->setCellValue('E2', 'Tercero')
            ->setCellValue('F2', 'Valor')
            ->setCellValue('G2', 'Fecha')
            ->setCellValue('H2', 'Estado')
            ->setCellValue('I2', 'Saldo');



$j=3;
for ($x = 0; $x < count($_POST['numRp']); $x++) {

        $saldo = generaSaldoRPccpet($_POST['numRp'][$x],$_POST['vigenciaD'][$x]);
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0,$j,$_POST['vigenciaD'][$x]);
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1,$j,$_POST['numRp'][$x]);
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2,$j,$_POST['cdp'][$x]);
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3,$j,$_POST['objeto'][$x]);
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4,$j,$_POST['tercero'][$x]);
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(5,$j,$_POST['valor'][$x]);
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(6,$j,$_POST['fecha'][$x]);
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(7,$j, $_POST['estado'][$x]);
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(8,$j,$saldo);
        $objPHPExcel->getActiveSheet()->getStyle("A$j:I$j")->applyFromArray($borders);
        $j+=1;

}


//----Propiedades de la hoja
$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(35);
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setTitle('RP');
$objPHPExcel->setActiveSheetIndex(0);

//----Guardar documento----
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename="presu-rp.xls"');
header('Cache-Control: max-age=0');

$objWriter=PHPExcel_IOFactory::createWriter($objPHPExcel,'Excel5');
$objWriter->save('php://output');
exit;

?>
