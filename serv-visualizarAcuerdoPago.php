<?php

    header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require 'comun.inc';
	require 'funciones.inc';
	session_start();
    if(empty($_SESSION)){
        header("location: index.php");
    }
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html lang=es>
	<head>
    <meta charset="utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1"/>
		<meta name="viewport" content="user-scalable=no">
		<title>:: IDEAL 10 - Servicios publicos</title>
		<link href="favicon.ico" rel="shortcut icon"/>
		<link href="css/css2z.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
        <link href="css/tabs.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
        <link href="css/style.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" href="sweetalert2/dist/sweetalert2.min.css">
        <script type="text/javascript" src="css/programas.js"></script>
        <script src="sweetalert2/dist/sweetalert2.min.js"></script>
		<script src="vue/vue.min.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script src="css/calendario.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
	</head>
	<body>
		<header>
			<table>
				<tr><script>barra_imagenes("serv");</script><?php cuadro_titulos();?></tr>
			</table>
		</header>

        <section id="myapp" v-cloak>
            <div class="loading-container" v-show="isLoading" >
                <p class="text-loading"data-loading="IDEAL 10">IDEAL 10</p>
            </div>
            <nav>
                <table>
                    <tr><?php menu_desplegable("serv");?></tr>
                </table>
                <div class="bg-white group-btn p-1" id="newNavStyle">
                    <button type="button" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center" @click="location.href='serv-crearAcuerdoPago'">
                        <span>Nuevo</span>
                        <svg viewBox="0 -960 960 960" ><path d="M440-440H200v-80h240v-240h80v240h240v80H520v240h-80v-240Z"/></svg>
                    </button>
                    <button type="button" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center" @click="location.href='serv-buscarAcuerdoPago'">
                        <span>Buscar</span>
                        <svg class="fill-black group-hover:fill-white w-5 h-5" viewBox="0 -960 960 960" ><path d="M784-120 532-372q-30 24-69 38t-83 14q-109 0-184.5-75.5T120-580q0-109 75.5-184.5T380-840q109 0 184.5 75.5T640-580q0 44-14 83t-38 69l252 252-56 56ZM380-400q75 0 127.5-52.5T560-580q0-75-52.5-127.5T380-760q-75 0-127.5 52.5T200-580q0 75 52.5 127.5T380-400Z"/></svg>
                    </button>
                    <button type="button" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center" @click="mypop=window.open('serv-principal.php','',''); mypop.focus();">
                        <span>Nueva ventana</span>
                        <svg class="fill-black group-hover:fill-white w-5 h-5" viewBox="0 -960 960 960" ><path d="M320-120v-80h80v-80H160q-33 0-56.5-23.5T80-360v-400q0-33 23.5-56.5T160-840h640q33 0 56.5 23.5T880-760v400q0 33-23.5 56.5T800-280H560v80h80v80H320ZM160-360h640v-400H160v400Zm0 0v-400 400Z"/></svg>
                    </button>
                    <button type="button" class="btn btn-danger btn-danger-hover d-flex justify-between align-items-center" @click="pdf()">
                        <span>Exportar PDF</span>
                        <svg viewBox="0 0 512 512"><path d="M64 464l48 0 0 48-48 0c-35.3 0-64-28.7-64-64L0 64C0 28.7 28.7 0 64 0L229.5 0c17 0 33.3 6.7 45.3 18.7l90.5 90.5c12 12 18.7 28.3 18.7 45.3L384 304l-48 0 0-144-80 0c-17.7 0-32-14.3-32-32l0-80L64 48c-8.8 0-16 7.2-16 16l0 384c0 8.8 7.2 16 16 16zM176 352l32 0c30.9 0 56 25.1 56 56s-25.1 56-56 56l-16 0 0 32c0 8.8-7.2 16-16 16s-16-7.2-16-16l0-48 0-80c0-8.8 7.2-16 16-16zm32 80c13.3 0 24-10.7 24-24s-10.7-24-24-24l-16 0 0 48 16 0zm96-80l32 0c26.5 0 48 21.5 48 48l0 64c0 26.5-21.5 48-48 48l-32 0c-8.8 0-16-7.2-16-16l0-128c0-8.8 7.2-16 16-16zm32 128c8.8 0 16-7.2 16-16l0-64c0-8.8-7.2-16-16-16l-16 0 0 96 16 0zm80-112c0-8.8 7.2-16 16-16l48 0c8.8 0 16 7.2 16 16s-7.2 16-16 16l-32 0 0 32 32 0c8.8 0 16 7.2 16 16s-7.2 16-16 16l-32 0 0 48c0 8.8-7.2 16-16 16s-16-7.2-16-16l0-64 0-64z"/></svg>
                    </button>
                </div>
            </nav>
            <article>
                <div class="bg-white">
                    <div>
                        <h2 class="titulos m-0">Visualizar acuerdo de pago</h2>
                        <div class="d-flex">
                            <div class="form-control">
                                <label class="form-label m-0" for="">Estado acuerdo: <span :class="[data.estadoAcuerdo == 'Reversado' ? 'badge-danger' : data.estadoAcuerdo == 'Finalizado' ? 'badge-primary' : data.estadoAcuerdo =='Activo' ? 'badge-success' : 'badge-danger']"class="badge">{{ data.estadoAcuerdo}}</span></label>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="bg-white">
                    <div>
                        <div class="w-75">
                            <div class="d-flex">
                                <div class="form-control">
                                    <label class="form-label m-0" for="">Consecutivo acuerdo: </label>
                                    <input type="text" class="text-center" v-model="data.consecutivo" readonly>
                                </div>

                                <div class="form-control">
                                    <label class="form-label m-0" for="">Fecha recaudo:</label>
                                    <input type="date" class="text-center" v-model="data.fecha" readonly>
                                </div>

                                <div class="form-control">
                                    <label class="form-label m-0" for="">Número de factura:</label>
                                    <div class="d-flex">
                                        <input type="text" class="text-center" v-model="data.numFactura" readonly>
                                    </div>
                                </div>
                            </div>

                            <div class="d-flex">
                                <div class="form-control w-25">
                                    <label class="form-label m-0" for="">Codigo de usuario: </label>
                                    <input type="text" class="text-center" v-model="data.codUsuario" readonly>
                                </div>

                                <div class="form-control w-25">
                                    <label class="form-label m-0" for="">Documento:</label>
                                    <input type="text" class="text-center" v-model="data.documento" readonly>
                                </div>

                                <div class="form-control">
                                    <label class="form-label m-0" for="">Nombre tercero:</label>
                                    <div class="d-flex">
                                        <input type="text" v-model="data.name" readonly>
                                    </div>
                                </div>
                            </div>

                            <div class="d-flex">
                                <div class="form-control">
                                    <label class="form-label m-0" for="">Valor factura:</label>
                                    <div class="form-number number-primary">
                                        <span></span>
                                        <input type="text" class="text-center" v-model="data.valorFactura" readonly>
                                    </div>
                                </div>

                                <div class="form-control">
                                    <label class="form-label m-0" for="">Porcentaje descuento:</label>
                                    <input type="text" class="text-center" v-model="data.porceDescuento" readonly>
                                </div>

                                <div class="form-control">
                                    <label class="form-label m-0" for="">Valor factura para Acuerdo:</label>
                                    <div class="form-number number-primary">
                                        <span></span>
                                        <input type="text" class="text-center" v-model="data.valorFacturaAcuerdo" readonly>
                                    </div>
                                </div>
                            </div>

                            <div class="d-flex">
                                <div class="form-control">
                                    <label class="form-label m-0" for="">Valor abono:</label> 
                                    <div class="form-number number-primary">
                                        <span></span>
                                        <input type="text" class="text-center" v-model="data.valorAbono" readonly>
                                    </div>
                                </div>

                                <div class="form-control">
                                    <label class="form-label m-0" for="">Valor acuerdo:</label>
                                    <div class="form-number number-primary">
                                        <span></span>
                                        <input type="text" class="text-center" v-model="data.valorAcuerdo" readonly>
                                    </div>
                                </div>

                                <div class="form-control">
                                    <label class="form-label m-0" for="">Numero de cuotas:</label>
                                    <input type="text" class="text-center" v-model="data.numCuotas" readonly>
                                </div>

                                <div class="form-control">
                                    <label class="form-label m-0" for="">Valor cuota:</label>
                                    <div class="form-number number-primary">
                                        <span></span>
                                        <input type="text" class="text-center" v-model="data.valorCuota" readonly>
                                    </div>
                                </div>

                                <div class="form-control">
                                    <label class="form-label m-0" for="">Valor pendiente:</label>
                                    <div class="form-number number-primary">
                                        <span></span>
                                        <input type="text" class="text-center" v-model="data.valorPendiente" readonly>
                                    </div>
                                </div>
                            </div>

                            <div class="d-flex">
                                <div class="form-control w-25">
                                    <label class="form-label m-0" for="">Medio de pago:</label>
                                    <input type="text" class="text-center" v-model="data.medioPago" readonly>
                                </div>

                                <div class="form-control">
                                    <label class="form-label m-0" for="">Banco:</label>
                                    <input type="text" v-model="data.banco" readonly>
                                </div>
                            </div>
                        </div>
                    </div>                       
                </div>

            </article>
        </section>

		<script src="node_modules/read-excel-file/bundle/read-excel-file.min.js"></script>
		<script src="node_modules/xlsx/dist/xlsx.full.min.js"></script>
        <script src="Librerias/vue/vue.min.js"></script>
		<script src="Librerias/vue/axios.min.js"></script>
		<script type="module" src="servicios_publicos/acuerdoPago/visualizar/serv-visualizarAcuerdoPago.js"></script>

	</body>
</html>
