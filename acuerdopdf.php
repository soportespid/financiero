<?php
	require_once("tcpdf/tcpdf_include.php");    
	require('comun.inc');
    require "funciones.inc";
	require "conversor.php";
	session_start();	
	date_default_timezone_set("America/Bogota");
    class MYPDF extends TCPDF 
	{
		public function Header() 
		{
            $linkbd=conectar_v7();
			$linkbd -> set_charset("utf8");
			$sqlr = "SELECT * FROM configbasica WHERE estado='S'";
			$res = mysqli_query($linkbd,$sqlr);
			while($row=mysqli_fetch_row($res)){
				$nit=$row[0];
				$rs=$row[1];
			}
			$this->Image('imagenes/escudo.jpg', 12, 12, 25, 25, 'JPG', '', 'T', true, 300, '', false, false, 0, false, false, false);// Logo
			$this->SetFont('helvetica','B',8);
			$this->SetY(10);
            $this->RoundedRect(10, 10, 190, 29, 1,'1111' );
			$this->Cell(30,29,'','R',0,'L'); 
			$this->SetY(10);
			$this->SetX(40);
			$this->SetFont('helvetica','B',12);
			$this->Cell(127,13,"$rs",0,0,'C'); 
			$this->SetY(16);
			$this->SetX(40);
			$this->SetFont('helvetica','B',11);
			$this->Cell(127,10,"$nit",0,0,'C');
			//*****************************************************************************************************************************
			$this->SetFont('helvetica','B',14);
			$this->SetY(10);
			$this->Cell(50.1);
			$this->Cell(149,31,'',0,1,'C'); 
			$this->SetY(8);
			$this->Cell(50.1);
			//************************************
			$this->SetFont('helvetica','B',10);
			$this->SetY(10);
			$this->Cell(157);
			$this->Cell(33,15,'','LB',0,'L');
			$this->SetY(10);
			$this->Cell(157.5);
			$this->Cell(33,5,'No Acuerdo: '.$_POST['numacuerdo'],0,0,'L');
			$this->SetY(15);
			$this->Cell(157.5);
			$this->Cell(33,5,'Fecha: '.$_POST['fecha'],0,0,'L');
            $this->SetY(20);
			$this->Cell(157.5);
			$this->Cell(33,5,'Vigencia: '.$_POST['vigencia'],0,0,'L');
			$this->SetY(20);
            $this->SetY(25);
            $this->SetX(40);
        	$this->SetFont('helvetica','B',12);
            $this->Cell(127,14,'ACUERDO PAGO PREDIAL','T',1,'C'); 
		

		}
		public function Footer() 
		{
			$linkbd = conectar_v7();
			$linkbd -> set_charset("utf8");
			$sqlr="SELECT direccion,telefono,web,email FROM configbasica WHERE estado='S'";
			$resp=mysqli_query($linkbd, $sqlr);
			$user = $_SESSION['nickusu'];	
			$fecha = date("Y-m-d H:i:s");
			$ip = $_SERVER['REMOTE_ADDR'];
			$useri = $_POST['user'];
			while($row=mysqli_fetch_row($resp))
			{
				$direcc=strtoupper($row[0]);
				$telefonos=$row[1];
				$dirweb=strtoupper($row[3]);
				$coemail=strtoupper($row[2]);
			}
			if($direcc!=''){$vardirec="Dirección: $direcc, ";}
			else {$vardirec="";}
			if($telefonos!=''){$vartelef="Telefonos: $telefonos";}
			else{$vartelef="";}
			if($dirweb!=''){$varemail="Email: $dirweb, ";}
			else {$varemail="";}
			if($coemail!=''){$varpagiw="Pagina Web: $coemail";}
			else{$varpagiw="";}
			
			$txt = <<<EOD
			$vardirec $vartelef
			$varemail $varpagiw
			EOD;
			$this->SetFont('helvetica', 'I', 5);
			$this->Cell(190,7,'','T',0,'T');
			$this->ln(1);
			$this->Write(0, $txt , '', 0, 'C', true, 0, false, false, 0);
			//$this->Cell(12, 7, 'Hecho por: '.$useri, 0, false, 'C', 0, '', 0, false, 'T', 'M');
			$this->Cell(15, 7, 'Impreso por: '.$user, 0, false, 'C', 0, '', 0, false, 'T', 'M');
			$this->Cell(57, 7, 'IP: '.$ip, 0, false, 'C', 0, '', 0, false, 'T', 'M');
			$this->Cell(50, 7, 'Fecha: '.$fecha, 0, false, 'C', 0, '', 0, false, 'T', 'M');
			$this->Cell(54, 7, 'IDEAL.10 S.A.S', 0, false, 'C', 0, '', 0, false, 'T', 'M');
			$this->Cell(25, 7, 'Pagina '.$this->getAliasNumPage().'/'.$this->getAliasNbPages(), 0, false, 'C', 0, '', 0, false, 'T', 'M');

		}
	}
    
	

	$pdf = new MYPDF('P','mm','Letter', true, 'utf8', false);
	$pdf->SetDocInfoUnicode (true); 
	// set document information
	$pdf->SetCreator(PDF_CREATOR);
	$pdf->SetAuthor('IDEAL10');
	$pdf->SetTitle('ACUERDO');
	$pdf->SetSubject('ACUERDO');
	$pdf->SetKeywords('ACUERDO');
    $pdf->SetMargins(10, 45, 10);// set margins
    $pdf->SetHeaderMargin(45);// set margins
    $pdf->SetFooterMargin(17);// set margins
	$pdf->SetAutoPageBreak(TRUE, 20);// set auto page breaks
	// set some language-dependent strings (optional)
	if (@file_exists(dirname(__FILE__).'/lang/spa.php')) 
	{
		require_once(dirname(__FILE__).'/lang/spa.php');
		$pdf->setLanguageArray($l);
	}
	$pdf->AddPage();
	$linkbd=conectar_v7();
	$linkbd -> set_charset("utf8");
	$pdf->SetFont('Helvetica','B',11);
    $pdf->MultiCell(190,5,'EL SUSCRITO SECRETARIO DE HACIENDA','','C');
    $pdf->Cell(190,7,'ACUERDA:',0,1,'C');	
    $pdf->SetFont('Helvetica','',9);
	$pdf ->ln(1);
    $pdf->MultiCell(190,10,'Realizar el pago del predio con código catastral No. '.$_POST['catastral'].' de las vigencias'.' '.$vigencias.'por la suma total de $'.number_format(round($_POST['totliquida']),2,".",",").' ('.$_POST['letras'].'), dividido de la siguiente manera:',0,'L');	
	//inicia tabla
	$pdf ->ln(1);
	$yy=$pdf->GetY();	
	$pdf->SetY($yy);
	$yy=$pdf->GetY();	
	$pdf->SetFillColor(222,222,222);
	$pdf->SetFont('helvetica','B',8);
	$pdf->Cell(0.1);
	$pdf->Cell(35,5,'Consecutivo',0,0,'C',1); 
	$pdf->SetY($yy);
	$pdf->Cell(36);
	$pdf->Cell(62,5,'Fecha de pago',0,0,'C',1);
	$pdf->SetY($yy);
	$pdf->Cell(99);
	$pdf->Cell(48,5,'Cuota',0,0,'C',1);
	$pdf->SetY($yy);
	$pdf->Cell(148);
	$pdf->MultiCell(42,5,'Valor',0,'C',1,1,'','',true,'',false,true,0,'M',true);
	$pdf->SetFont('helvetica','',7);
	$pdf->ln(1);
	$sqlrps="SELECT * FROM tesoacuerdopredial_pagos WHERE idacuerdo = '$_POST[numacuerdo]'";
	$resps = mysqli_query($linkbd,$sqlrps);
	$con = 0;
    while ($rowps=mysqli_fetch_row($resps)){
		if ($con%2==0)
		{
			$pdf->SetFillColor(245,245,245);
		}
		else
		{
			$pdf->SetFillColor(255,255,255);
		}	
		$pdf->MultiCell(36,4,$rowps[1],0,'C',true,0,'','',true,0,false,true,0,'M',true);
        $pdf->MultiCell(62,4,$rowps[3],0,'C',true,0,'','',true,0,false,true,0,'M',true);
		$pdf->MultiCell(49,4,$rowps[1],0,'C',true,0,'','',true,0,false,true,0,'M',true);
        $pdf->MultiCell(42,4,'$'.number_format($rowps[4],2),0,'R',true,1,'','',true,0,false,true,0,'M',true);
		$con++;
	}
	//termina tabla
	$sqlr="select fechas from tesoacuerdopredial where tesoacuerdopredial.idacuerdo=$_POST[numacuerdo]  ";
	$res=mysqli_query($linkbd, $sqlr);
	$fechas="";
	$fila=mysqli_fetch_row($res);
	$fechas=substr($fila[0],0,-1);
	
	//Creación del objeto de la clase heredada
	$sqlr="select vigencia from tesoacuerdopredial_det where tesoacuerdopredial_det.idacuerdo=$_POST[numacuerdo]  ";
	$res=mysqli_query($linkbd, $sqlr);
	$vigencias="";
	while($row = mysqli_fetch_row($res)){
		$vigencias.=($row[0].",");
	}
	$vigencias=substr($vigencias,0,-1);		
	$resultado = convertir($_POST['totliquida']);
	$_POST['letras']=$resultado." PESOS M/CTE";	
	
	$nuevo=round($_POST['totliquida']/$_POST['cuotas']);
	$resultado1 = convertir($nuevo);
	$nuevoletras=$resultado1." PESOS M/CTE";
	
	$nresul=buscatercero($_POST['pdftercero']);
	$pdf ->ln(3);
    $pdf->SetFont('Helvetica','',9);
	$pdf->MultiCell(190,7,'Si el contribuyente no cancela en el tiempo estipulado perderá el beneficio y se iniciará el proceso coactivo.',0,'L',false,1);	
    $pdf->SetFont('Helvetica','B',11);
	$pdf->Cell(190,5,'','T',1,'R',false);
	$pdf->MultiCell(190,9,'Detalle Liquidacion',0,'L',false,1);	
	//inicia tabla
	$pdf ->ln(1);
	$yy=$pdf->GetY();	
	$pdf->SetY($yy);
	$yy=$pdf->GetY();	
	$pdf->SetFillColor(222,222,222);
	$pdf->SetFont('helvetica','B',7);
	$pdf->Cell(0.1);
	$pdf->Cell(20,5,'VIGENCIA',0,0,'C',1); 
	$pdf->SetY($yy);
	$pdf->Cell(21);
	$pdf->Cell(35,5,'COD. CATASTRAL',0,0,'C',1);
	$pdf->SetY($yy);
	$pdf->Cell(57);
	$pdf->Cell(35,5,'PREDIAL/INTERES',0,0,'C',1);
	$pdf->SetY($yy);
	$pdf->Cell(93);
	$pdf->Cell(35,5,'BOMBERO/INTERES',0,0,'C',1);
	$pdf->SetY($yy);
	$pdf->Cell(129);
	$pdf->Cell(35,5,'AMBIENTE/INTERES',0,0,'C',1);
	$pdf->SetY($yy);
	$pdf->Cell(165);
	$pdf->MultiCell(25,5,'DESCUENTOS',0,'C',1,1,'','',true,'',false,true,0,'M',true);
	$pdf->SetFont('helvetica','',6);
	$pdf->ln(1);
	$cont=0;
	while($cont<count($_POST['dcodcatas'])){
		if ($cont%2==0)
		{
			$pdf->SetFillColor(245,245,245);
		}
		else
		{
			$pdf->SetFillColor(255,255,255);
		}
		$lineascod = $pdf-> getNumLines($_POST['dcodcatas'][$cont], 36);
		$lineaspred = $pdf-> getNumLines('$ '.number_format($_POST['dpredial'][$cont]).'/ $ '.number_format($_POST['dipredial'][$cont]), 36);
		$max = max($lineascod, $lineaspred);
		$altura = $max * 4;
		$pdf->MultiCell(21,$altura,$_POST['dvigencias'][$cont],0,'C',true,0,'','',true,0,false,true,0,'M',true);
        $pdf->MultiCell(36,$altura,$_POST['dcodcatas'][$cont],0,'C',true,0,'','',true,0,false,true,0,'M',true);
		$pdf->MultiCell(36,$altura,'$ '.number_format($_POST['dpredial'][$cont]).'/ $ '.number_format($_POST['dipredial'][$cont]),0,'C',true,0,'','',true,0,false,true,0,'M',true);
        $pdf->MultiCell(36,$altura,'$ '.number_format($_POST['dimpuesto1'][$cont]).'/ $ '.number_format($_POST['dinteres1'][$cont]),0,'C',true,0,'','',true,0,false,true,0,'M',true);
        $pdf->MultiCell(36,$altura,'$ '.number_format($_POST['dimpuesto2'][$cont]).'/ $ '.number_format($_POST['dinteres2'][$cont]),0,'C',true,0,'','',true,0,false,true,0,'M',true);
        $pdf->MultiCell(25,$altura,'$ '.number_format($_POST['ddescuentos'][$cont]),0,'R',true,1,'','',true,0,false,true,0,'M',true);
		$cont++;
	}
	//termina tabla
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
	$sqlr="select nombreteso from  tesoparametros where estado='S'";
	$res=mysqli_query($linkbd, $sqlr);
	while($row=mysqli_fetch_row($res)){$ppto=$row[0];}
	
	$pdf->ln(50);
	$v=$pdf->gety();
	$pdf->setFont('helvetica','B',9);
	$pdf->Line(50,$v,160,$v);
	$pdf->Cell(190,6,''.$ppto,0,1,'C',false,0,0,false,'T','C');
	$pdf->Cell(190,6,'SECRETARIO DE HACIENDA',0,0,'C',false,0,0,false,'T','C');
	
	$pdf->ln(30);
	$v=$pdf->gety();
	if ($_POST['ntercero'] != ''){
		$pdf->setFont('helvetica','B',9);
		$pdf->Line(50,$v,160,$v);
		$pdf->Cell(190,6,$_POST['ntercero'],0,0,'C',false,0,0,false,'T','C');
	}
	
	$pdf->Output();