<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require 'comun.inc';
	require 'funciones.inc';
	session_start();
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
	titlepag();
?>
<!DOCTYPE >
<html lang=es>
	<head>
		<meta charset="utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1"/>
		<meta name="viewport" content="user-scalable=no">
		<title>:: IDEAL 10 - Parametrización</title>
		<link href="css/css2z.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/tabs.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
		<script src="vue/vue.min.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script src="sweetalert2/dist/sweetalert2.min.js"></script>
		<link rel="stylesheet" href="sweetalert2/dist/sweetalert2.min.css">
	</head>
	<body>
	<IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
		<span id="todastablas2"></span>
		<header>
			<table>
				<tr><script>barra_imagenes("para");</script><?php cuadro_titulos();?></tr>
			</table>
		</header>
		<section id="myapp" v-cloak >
			<nav>
				<table>
					<tr><?php menu_desplegable("para");?></tr>
					<tr>
						<td colspan="3" class="cinta">
							<img src="imagenes/add.png" v-on:Click="location.href='teso-mediosdepagonew.php'" class="mgbt" title="Nuevo" >
							<img src="imagenes/guarda.png" title="Guardar" v-on:click="validarguardar()" class="mgbt">
							<img src="imagenes/busca.png" class="mgbt" title="Buscar" v-on:Click="location.href='teso-mediosdepagonewbuscar.php'">
							<img src="imagenes/nv.png" v-on:Click="mypop=window.open('para-principal.php','',''); mypop.focus();" class="mgbt" title="Nueva Ventana">
							<img src="imagenes/duplicar_pantalla.png" title="Duplicar pesta&ntilde;a" v-on:Click="mypop=window.open('<?php echo $url2; ?>','','');mypop.focus();" class="tooltip bottom mgbt">
						</td>
					</tr>
				</table>
			</nav>
			<article>
				<div class="tabsmeci" style="height:74.5%; width:99.6%" >
					<div class="tab" >
						<input type="radio" id="tab-1" name="tabgroup1"  v-model="tabgroup1"  value="1" >
						<label for="tab-1">Tesoreria</label>
						<div class="content" style="overflow:hidden;">
							<table class="inicio ancho">
								<tr >
									<td class="titulos" colspan="4">Crear Medio de Pago </td>
									<td class="cerrar" style="width:7%" v-on:Click="location.href='para-principal.php'">Cerrar</td>
								</tr>
								<tr>
									<td style="width:2.5cm;" class="tamano01">C&oacute;digo:</td>
									<td style="width:10%;"><input type="text" v-model="vcodigo" maxlength="2" onKeyPress="return solonumeros(event)"></td>
									<td style="width:2.5cm;" class="tamano01">Nombre:</td>
									<td><input type="text" v-model="vnombre" style="width:100%;"></td>
								</tr> 
							</table>
							<table class="inicio ancho">
								<tr><td colspan="5" class="titulos">Detalle Medio de Pago</td></tr>                  
								<tr>
									<td style="width:3cm;" class="tamano01">Cuenta Contable: </td>
									<td style="width:15%;" valign="middle">
										<input type="text" v-model="vcuenta" style="width:100%" onKeyPress="return solonumeros(event)" title='Listado Cuentas Contables' class="colordobleclik" autocomplete="off" v-on:dblclick='toggleModalCuentas' v-on:change="verificacuenta()" >
									</td>
									<td>
										<input type="text" v-model="vncuenta" style="width:100%;" readonly>
									</td>
									<td style="width:7%"></td>
								</tr>
								<tr>
									<td class="tamano01">Tercero:</td>
									<td>
										<input type="text" v-model="vtercero" style="width:100%;" title='Listado Terceros' onKeyPress="javascript:return solonumeros(event)" class="colordobleclik" autocomplete="off" v-on:dblclick='toggleModalTerceros'  v-on:change="verificatercero()" >
									</td>
									<td>
										<input type="text" v-model="vntercero"  style="width:100%;" readonly>
									</td>
									<td style="width:7%"></td>
								</tr> 
							</table>
						</div>
					</div>
					<div class="tab" >
						<input type="radio" id="tab-2" name="tabgroup1" v-model="tabgroup1" value="2" >
						<label for="tab-2">Nomina</label>
						<div class="content" style="overflow:hidden;">
							<table class="inicio ancho">
								<tr >
									<td class="titulos" colspan="4">Crear Medio de Pago </td>
									<td class="cerrar" style="width:7%" v-on:Click="location.href='teso-principal.php'">Cerrar</td>
								</tr>
								<tr>
									<td style="width:2.5cm;" class="tamano01">C&oacute;digo:</td>
									<td style="width:10%;"><input type="text" v-model="vcodigon" maxlength="2" onKeyPress="return solonumeros(event)"></td>
									<td style="width:2.5cm;" class="tamano01">Nombre:</td>
									<td><input type="text" v-model="vnombren" style="width:100%;"></td>
								</tr> 
							</table>
							<table class="inicio ancho">
								<tr><td colspan="5" class="titulos">Detalle Medio de Pago</td></tr>                  
								<tr>
									<td style="width:3cm;" class="tamano01">Cuenta Contable: </td>
									<td style="width:15%;" valign="middle">
										<input type="text" v-model="vcuentan" style="width:100%" onKeyPress="return solonumeros(event)" title='Listado Cuentas Contables' class="colordobleclik" autocomplete="off" v-on:dblclick='toggleModalCuentas' v-on:change="verificacuenta()" >
									</td>
									<td>
										<input type="text" v-model="vncuentan" style="width:100%;" readonly>
									</td>
									<td style="width:7%"></td>
								</tr>
							</table>
						</div>
					</div>
				</div>
				<div v-show="showModalCuentas">
					<transition name="modal">
						<div class="modal-mask">
							<div class="modal-wrapper">
								<div class="modal-container2">
									<table class="inicio ancho">
										<tr>
											<td class="titulos" colspan="2" >SELECCIONAR CUENTA</td>
											<td class="cerrar" style="width:7%" @click="showModalCuentas = false">Cerrar</td>
										</tr>
										<tr>
											<td class="tamano01" style="width:3cm">Cuenta:</td>
											<td><input type="text" class="form-control" placeholder="Buscar por numero de cuenta" v-on:keyup="buscarCuentas" v-model="searchCuenta.keywordCuenta" style="width:100%" /></td>
										</tr>
									</table>
									<table class='tablamv'>
										<thead>
											<tr style="text-align:Center;">
												<th class="titulosnew02" >Resultados Busqueda</th>
											</tr>
											<tr style="text-align:Center;">
												<th class="titulosnew00" style="width:6%;">Item</th>
												<th class="titulosnew00" style="width:15%;">Cuenta</th>
												<th class="titulosnew00" >Descripción</th>
												<th class="titulosnew00" style="width:15%;">Tipo</th>
												<th class="titulosnew00" style="width:6%;">Estado</th>
											</tr>
										</thead>
										<tbody>
											<tr v-for="(infocuenta,index) in infocuentas" v-on:click="infocuenta[5].toUpperCase() == 'AUXILIAR' ? ingresarCuenta(infocuenta[0],infocuenta[1]) : ''" v-bind:class="index % 2 ? 'saludo1a' : 'saludo2'" style='text-rendering: optimizeLegibility; cursor: pointer !important;'>
											<td style="font: 120% sans-serif; padding-left:10px; width:6%;">{{ index + 1 }}</td>
												<td style="font: 120% sans-serif; padding-left:10px; width:15%;">{{ infocuenta[0] }}</td>
												<td style="font: 120% sans-serif; padding-left:10px;">{{ infocuenta[1] }}</td>
												<td style="font: 120% sans-serif; padding-left:10px; width:15%; text-align:Center;">{{ infocuenta[5] }}</td>
												<td style="font: 120% sans-serif; padding-left:10px; width:6%; text-align:Center;">{{ infocuenta[6] }}</td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</transition>
				</div>
				<div v-show="showModalTerceros">
					<transition name="modal">
						<div class="modal-mask">
							<div class="modal-wrapper">
								<div class="modal-container2">
									<table class="inicio ancho">
										<tr>
											<td class="titulos" colspan="2" >SELECCIONAR TERCERO</td>
											<td class="cerrar" style="width:7%" @click="showModalTerceros = false">Cerrar</td>
										</tr>
										<tr>
											<td class="tamano01" style="width:3cm">Documento:</td>
											<td><input type="text" class="form-control" placeholder="Buscar por nombre o documento" v-on:keyup="buscarTerceros" v-model="searchTercero.keywordTercero" style="width:100%" /></td>
										</tr>
									</table>
									<table class='tablamv'>
										<thead>
											<tr style="text-align:Center;">
												<th class="titulosnew02" >Resultados Busqueda</th>
											</tr>
											<tr style="text-align:Center;">
												<th class="titulosnew00" style="width:6%;">Item</th>
												<th class="titulosnew00">Razon Social</th>
												<th class="titulosnew00" style="width:15%;">Primer Apellido</th>
												<th class="titulosnew00" style="width:15%;">Segundo Apellido</th>
												<th class="titulosnew00" style="width:15%;">Primer Nombre</th>
												<th class="titulosnew00" style="width:15%;">Segundo Nombre</th>
												<th class="titulosnew00" style="width:15%; ">Documento</th>
											</tr>
										</thead>
										<tbody>
											<tr v-for="(infotercero,index) in infoterceros" v-on:click="ingresarTercero(infotercero[0], infotercero[1], infotercero[2], infotercero[3], infotercero[4], infotercero[5])" v-bind:class="index % 2 ? 'saludo1a' : 'saludo2'" style='text-rendering: optimizeLegibility; cursor: pointer !important;'>
											<td style="font: 120% sans-serif; padding-left:10px; width:6%;">{{ index + 1 }}</td>
												<td style="font: 120% sans-serif; padding-left:10px;">{{ infotercero[0] }}</td>
												<td style="font: 120% sans-serif; padding-left:10px; width:15%;">{{ infotercero[1] }}</td>
												<td style="font: 120% sans-serif; padding-left:10px; width:15%;">{{ infotercero[2] }}</td>
												<td style="font: 120% sans-serif; padding-left:10px; width:15%;">{{ infotercero[3] }}</td>
												<td style="font: 120% sans-serif; padding-left:10px; width:15%;">{{ infotercero[4] }}</td>
												<td style="font: 120% sans-serif; padding-left:10px; width:15%; text-align:right;">{{ infotercero[5] }}</td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</transition>
				</div>
				<div id="cargando" v-if="loading" class="loading" style="z-index: 9999;">
					<span>Cargando...</span>
				</div>
			</article>
		</section>
		<script src="Librerias/vue/vue.min.js"></script>
		<script src="Librerias/vue/axios.min.js"></script>
		<script src="vue/tesoreria/teso-mediosdepagonew.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
	</body>
</html>