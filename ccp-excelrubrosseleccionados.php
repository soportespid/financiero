<?php
	require_once 'PHPExcel/Classes/PHPExcel.php';
    include '/PHPExcel/Classes/PHPExcel/IOFactory.php';
	require "comun.inc";
	require "funciones.inc";
    ini_set('max_execution_time',99999999);
    header("Content-type: application/json");
	session_start();
	$linkbd=conectar_v7();
	$linkbd -> set_charset("utf8");

	$objPHPExcel = new PHPExcel();
	
	$objPHPExcel->getProperties()
		->setCreator("IDEAL10")
		->setLastModifiedBy("IDEAL10")
		->setTitle("RUBROS SELECCIONADOS")
		->setSubject("CCP")
		->setDescription("CCP")
		->setKeywords("CCP")
		->setCategory("PRESUPUESTO CCP");
	$objPHPExcel->getActiveSheet()->mergeCells('A1:C1');
	$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A1', 'Rubros seleccionados');
	$objFont=$objPHPExcel->getActiveSheet()->getStyle('A1')->getFont();
	$objFont->setName('Courier New');
	$objFont->setSize(15);
	$objFont->setBold(true);
	$objFont->setUnderline(PHPExcel_Style_Font::UNDERLINE_SINGLE);
	$objFont->getColor()->setARGB( PHPExcel_Style_Color::COLOR_WHITE);
	$objAlign=$objPHPExcel->getActiveSheet()->getStyle('A1')->getAlignment(); 
	$objAlign->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
	$objAlign->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);

    $objPHPExcel-> getActiveSheet ()
		-> getStyle ("A2:C2")
		-> getFill ()
		-> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
		-> getStartColor()
		-> setRGB ('368986');

	$objPHPExcel-> getActiveSheet ()
		-> getStyle ("A1")	
		-> getFill ()
		-> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
		-> getStartColor ()
		-> setRGB ('e79a32');
    
	$bordersTitle = array(
		'borders' => array(
			'allborders' => array(
				'style' => PHPExcel_Style_Border::BORDER_THIN,
				'color' => array('argb' => 'ffffffff'),
			)
		),
		'font' => array(
			'bold' => true,
			'size' => 12,
			'color' => array('argb' => 'ffffffff'),
		),
	);

	$borders = array(
		'borders' => array(
			'allborders' => array(
				'style' => PHPExcel_Style_Border::BORDER_THIN,
				'color' => array('argb' => 'FF000000'),
			)
		),
	);

    $borders2 = array(
		'borders' => array(
			'allborders' => array(
				'style' => PHPExcel_Style_Border::BORDER_THIN,
				'color' => array('argb' => 'FF000000'),
			)
		),
        'font' => array(
            'bold' => true
        ),
	);

	$objPHPExcel->getActiveSheet()->getStyle('A2:C2')->applyFromArray($bordersTitle);
	$objPHPExcel->setActiveSheetIndex(0)
        ->setCellValue('A2', 'Cuenta CCPET')
        ->setCellValue('B2', 'Nombre')
        ->setCellValue('C2', 'Tipo');

	$i=3;

    $tamRubros = $_POST['rubro'] !== null ? count($_POST['rubro']) : 0;
    
	for($ii=0; $ii < $tamRubros; $ii++)
	{
		$objPHPExcel->setActiveSheetIndex(0)
		->setCellValueExplicit ("A$i", $_POST['rubro'][$ii], PHPExcel_Cell_DataType :: TYPE_STRING)
		->setCellValueExplicit ("B$i", $_POST['nombreRubro'][$ii], PHPExcel_Cell_DataType :: TYPE_STRING)
		->setCellValueExplicit ("C$i", $_POST['tipo'][$ii], PHPExcel_Cell_DataType :: TYPE_STRING);
		
        if ($_POST['tipo'][$ii] == "A") {
            $objPHPExcel->getActiveSheet()->getStyle("A$i:C$i")->applyFromArray($borders2);
        }else{
			$objPHPExcel->getActiveSheet()->getStyle("A$i:C$i")->applyFromArray($borders);
		}
		
        $i++;
	}

	$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
	$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(20);

	$objPHPExcel->getActiveSheet()->setTitle('Ingresos');
	//header('Content-Type: application/vnd.ms-excel');
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
	header('Content-Disposition: attachment;filename="CuentasIngresosSeleccionadas.xlsx"');
	header('Cache-Control: max-age=0');
	$objWriter=PHPExcel_IOFactory::createWriter($objPHPExcel,'Excel2007');
	$objWriter->save('php://output');
	exit;
?>