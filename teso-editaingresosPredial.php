<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require "comun.inc";
	require "funciones.inc";
	require "funcionesSP.inc.php";
	session_start();
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8"); 
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");

	$scroll=$_GET['scrtop'];
	$totreg=$_GET['totreg'];
	$idcta=$_GET['idcta'];
	$altura=$_GET['altura'];
	$filtro="'".$_GET['filtro']."'";
?>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta charset="utf-8"/>
	<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1"/>
	<meta name="viewport" content="user-scalable=no">
	<title>:: IDEAL 10 - Tesoreria</title>
	<link href="css/css2z.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
	<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
	<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
	<link href="css/tabs2.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
	<script src="sweetalert2/dist/sweetalert2.min.js"></script>
	<link rel="stylesheet" href="sweetalert2/dist/sweetalert2.min.css">
	<script type="text/javascript" src="css/programas.js"></script>
<script>
	function buscacta(e)
		{
			if (document.form2.cuenta.value!="")
			{		
				document.form2.bc.value='1';
				document.getElementById('oculto').value='7';
				document.form2.submit();
			}
		}
	function buscacta2(e,tipo)
	{
		if (tipo == 1)
		{
			document.form2.bc.value='2';
			document.getElementById('oculto').value='7';
			document.form2.submit();
		}
		if(tipo == 2)
		{
			document.form2.bc.value='3';
			document.getElementById('oculto').value='7';
			document.form2.submit();
		}
		if(tipo == 3)
		{
			document.form2.bc.value='4';
			document.getElementById('oculto').value='7';
			document.form2.submit();
			
		}
	}

	function validar(){
		document.getElementById('oculto').value='7';
		document.form2.submit();
	}

	function guardar() {

		var codigo = document.getElementById('codigo').value;
		var nombreIngreso = document.getElementById('nombre').value;
		var tipo = document.getElementById('tipo').value;

		if (codigo != '' && nombreIngreso != '') {
				
			if (tipo == 'C') {

				dcuentas=document.getElementsByName('dcuentas[]');

				if (dcuentas.length > 0) {

					Swal.fire({
						icon: 'question',
						title: 'Seguro que quieres guardar?',
						showDenyButton: true,
						confirmButtonText: 'Guardar',
						denyButtonText: 'Cancelar',
						}).then((result) => {
							if (result.isConfirmed) {
								document.form2.oculto.value='2';
								document.form2.submit();
							} else if (result.isDenied) {
								Swal.fire('Guardar cancelado', '', 'info')
							}
						}
					)
				}
				else {
					Swal.fire('Falta información', 'Debes tener cuenta presupuestal y fuente', 'info');
				}
			}
		}
		else {
			Swal.fire('Falta información', 'Información faltante en cabecera', 'info');
		}
	}

	function agregardetalle(){

		if(document.form2.valor.value!="" && document.form2.concecont.value!=-1 ){
			document.form2.agregadet.value=1;
			document.getElementById('oculto').value='7';
			document.form2.submit();
		 }
		 else {
			 despliegamodalm('visible','2','Faltan datos para Agregar el Registro');
		}
	}

	function eliminar(variable){
		document.getElementById('elimina').value=variable;
		despliegamodalm('visible','4','Esta Seguro de Eliminar el Registro','2');
	}
	function despliegamodal2(_valor)
		{
			document.getElementById("bgventanamodal2").style.visibility=_valor;
			if(_valor=="hidden")
			{
				document.getElementById('ventana2').src="";
			}
			else 
			{
				document.getElementById('ventana2').src="cuentasppto-ventana1.php?";
			}
		}

		function despliegamodal3(_valor)
		{
			document.getElementById("bgventanamodal2").style.visibility=_valor;
			if(_valor=="hidden")
			{
				document.getElementById('ventana2').src="";
			}
			else 
			{
				document.getElementById('ventana2').src="bienesTransportables-ventana.php";
			}
		}
		
		function despliegamodal4(_valor)
		{
			document.getElementById("bgventanamodal2").style.visibility=_valor;
			if(_valor=="hidden")
			{
				document.getElementById('ventana2').src="";
			}
			else 
			{
				document.getElementById('ventana2').src="servicios-ventana.php";
			}
		}

		function despliegamodal5(_valor)
		{
			document.getElementById("bgventanamodal2").style.visibility=_valor;
			if(_valor=="hidden")
			{
				document.getElementById('ventana2').src="";
			}
			else 
			{
				document.getElementById('ventana2').src="cuin-ventana.php";
			}
		}

		function despliegamodal6(_valor,pos)
		{
			document.getElementById("bgventanamodal2").style.visibility=_valor;
			if(_valor=="hidden")
			{
				document.getElementById('ventana2').src="";
			}
			else 
			{
				document.getElementById('ventana2').src="cuentasppto-ventanaingresos.php?pos="+pos;
			}
		}

		function despliegamodal7(_valor)
		{	
			document.getElementById("bgventanamodal2").style.visibility=_valor;
			if(_valor=="hidden")
			{
				document.getElementById('ventana2').src="";
			}
			else 
			{
				document.getElementById('ventana2').src="fuentes-ventana1.php"
			}
		}

		function despliegamodalClasificadores(_valor, pos, clasi)
		{	
			
			var _tip =  document.getElementById("numeroClasificador").value;
			
			if(clasi != 0 || _tip != '')
			document.getElementById("bgventanamodal2").style.visibility=_valor;
			
			//console.log(clasi);
			if(_valor == "hidden")
			{
				document.getElementById('ventana2').src="";
			}
			
			else
			{
				if(_tip != '1' && _tip != '2' && _tip != '3' && _tip == '' && clasi != 1 && clasi != 2 && clasi != 3 && clasi == '')
				{
					alert('Esta cuenta no tiene clasficador');
				}
				else
				{	
					if(clasi != 0)
					{
						switch(clasi)
						{
							case 1:
								document.getElementById('ventana2').src="cuin-ventana2.php?pos="+pos;
								break;
							case 2:
								document.getElementById('ventana2').src="bienesTransportables-ventana2.php?pos="+pos;
								break;
							case 3:
								document.getElementById('ventana2').src="servicios-ventana2.php?pos="+pos;
								break;
						}	
					}

					if(_tip != '')
					{
						switch(_tip)
						{
							case "1":
								document.getElementById('ventana2').src="cuin-ventana2.php?pos="+pos;
								break;
							case "2":
								document.getElementById('ventana2').src="bienesTransportables-ventana2.php?pos="+pos;
								break;
							case "3":
								document.getElementById('ventana2').src="servicios-ventana2.php?pos="+pos;
								break;
		
						}
					}
					
				}
			}
		} 

		function modalCuentaClasificadora(_valor, pos, clasi)
		{
			console.log(_valor);
			console.log(pos);
			console.log(clasi);
			document.getElementById("bgventanamodal2").style.visibility=_valor;
			if(_valor=="hidden")
			{
				document.getElementById('ventana2').src="";
			}
			else 
			{
				switch(clasi)
					{
						case "1":
							document.getElementById('ventana2').src="cuin-ventana2.php?pos="+pos;
							break;
						case "2":
							document.getElementById('ventana2').src="bienesTransportables-ventana2.php?pos="+pos;
							break;
						case "3":
							document.getElementById('ventana2').src="servicios-ventana2.php?pos="+pos;
							break;
					}
			}
		}

	function despliegamodalm(_valor,_tip,mensa,pregunta){
		document.getElementById("bgventanamodalm").style.visibility=_valor;
		if(_valor=="hidden"){document.getElementById('ventanam').src="";}
		else
		{
			switch(_tip)
			{
				case "1":
					document.getElementById('ventanam').src="ventana-mensaje1.php?titulos="+mensa;break;
				case "2":
					document.getElementById('ventanam').src="ventana-mensaje3.php?titulos="+mensa;break;
				case "3":
					document.getElementById('ventanam').src="ventana-mensaje2.php?titulos="+mensa;break;
				case "4":
					document.getElementById('ventanam').src="ventana-consulta1.php?titulos="+mensa+"&idresp="+pregunta;break;	
			}
		}
	}

	function funcionmensaje()
	{
		document.form2.submit();
	}

	function respuestaconsulta(pregunta){
		switch(pregunta){
			case "1":	document.getElementById('oculto').value='2';document.form2.submit();break;
			case "2":	document.getElementById('oculto').value='6';
						document.form2.submit();break;
		}
	}
	 function validafinalizar(e)
	 {
		 var id=e.id;
		 var check=e.checked;
		 if(id=='terceros'){
			document.getElementById('terceros').value=1;
		 }else{
			 document.form2.terceros.checked=false;
		 }
		 document.getElementById('oculto').value='6';
		 document.form2.submit();
	 }
	function buscacuentap()
	{
		document.form2.buscap.value='1';
		document.form2.oculto.value='7';
		document.form2.submit();
	}

	function agregaFuente() {
		
		var fuente = document.getElementById('fuente').value;
		var nombreFuente = document.getElementById('nfuente').value;

		if (fuente != "" && nombreFuente != "") {

			document.form2.agregaFuente.value = 1;
			document.form2.submit();
		}
		else {
			Swal.fire('Falta información', 'Seleccióna la fuente', 'info');
		}
	}

	function eliminarFuente(pos) {

		Swal.fire({
			icon: 'question',
			title: 'Seguro que quieres eliminar?',
			showDenyButton: true,
			confirmButtonText: 'Eliminar',
			denyButtonText: 'Cancelar',
			}).then((result) => {
				if (result.isConfirmed) {
					document.form2.eliminaFuente.value=pos;
					document.form2.submit();
				}
				else {
					Swal.fire('Eliminar cancelado', '', 'info');
				}
			}
		)
	}


</script>

		<script>
			function adelante(scrtop, numpag, limreg, filtro, next){
				var maximo=document.getElementById('maximo').value;
				var actual=document.getElementById('codigo').value;
				if(parseFloat(maximo)>parseFloat(actual)){
					document.getElementById('oculto').value='1';
					document.getElementById('codigo').value=next;
					var idcta=document.getElementById('codigo').value;
					document.form2.action="teso-editaingresosPredial.php?idr="+idcta+"&scrtop="+scrtop+"&numpag="+numpag+"&limreg="+limreg+"&filtro="+filtro;
					document.form2.submit();
				}
			}
		
			
			function atrasc(scrtop, numpag, limreg, filtro, prev){
				var minimo=document.getElementById('minimo').value;
				var actual=document.getElementById('codigo').value;
				if(parseFloat(minimo)<parseFloat(actual)){
					document.getElementById('oculto').value='1';
					document.getElementById('codigo').value=prev;
					var idcta=document.getElementById('codigo').value;
					document.form2.action="teso-editaingresosPredial.php?idr="+idcta+"&scrtop="+scrtop+"&numpag="+numpag+"&limreg="+limreg+"&filtro="+filtro;
					document.form2.submit();
				}
			}
		
			function iratras(scrtop, numpag, limreg, filtro){
				var idrecaudo=document.getElementById('codigo').value;
				location.href="teso-buscaingresosPredial.php?idcta="+idrecaudo;
			}
			function cambiavalor(pos,element){
				document.form2.posicion.value=pos;
				document.form2.valorpos.value=element.value;
				document.form2.submit();
			}
		</script>
<?php titlepag();?>
<?php

	function enocontrarCuentaPresupuestal($cuenta)
	{
		$cuentaDevolver = 0;
		$linkbd=conectar_v7();

		if(isset($cuenta))
		{
			$sqlr = "SELECT clasificadores FROM ccpetprogramarclasificadores WHERE cuenta = '".$cuenta."' ";
			$res = mysqli_query($linkbd, $sqlr);
			$row = mysqli_fetch_row($res);
			$cuentaDevolver = $row[0];
		}
		return $cuentaDevolver;
	}
?>

</head>
<body>
<IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
<span id="todastablas2"></span>
        <?php
		$numpag=$_GET['numpag'];
		$limreg=$_GET['limreg'];
		$scrtop=20*$totreg;
		?>
<table>
	<tr>
		<script>barra_imagenes("teso");</script>
		<?php cuadro_titulos();?>
	</tr>	 
	<tr>
		<?php menu_desplegable("teso");?>
	</tr>
	<tr>
  		<td colspan="3" class="cinta">
			<a href="teso-ingresosPredial.php" class="mgbt"><img src="imagenes/add.png" title="Nuevo"/></a> 
			<a onClick="guardar();" class="mgbt"><img src="imagenes/guarda.png"  title="Guardar"/></a>
			<a href="teso-buscaingresosPredial.php" class="mgbt"> <img src="imagenes/busca.png"  title="Buscar"/></a> 
			<a onClick="mypop=window.open('plan-agenda.php','','');mypop.focus()" class="mgbt"><img src="imagenes/agenda1.png" title="Agenda" /></a>
			<a onClick="mypop=window.open('teso-principal.php','','');mypop.focus();" class="mgbt"><img src="imagenes/nv.png"  title="Nueva ventana"></a> 
			<a onClick="mypop=window.open('<?php echo $url2; ?>','','');mypop.focus();" class="tooltip bottom mgbt"><img src="imagenes/duplicar_pantalla.png" title="Duplicar pesta&ntilde;a"></a>
			<a onClick="iratras(<?php echo $scrtop; ?>, <?php echo $numpag; ?>, <?php echo $limreg; ?>, <?php echo $filtro; ?>)" class="mgbt"><img src="imagenes/iratras.png" title="Atr&aacute;s"></a>
		</td>
	</tr>		  
</table>
	<tr>
		<td colspan="3" class="tablaprin" align="center"> 
		<?php
 			$vigusu=vigencia_usuarios($_SESSION['cedulausu']); 

			if ($_GET['idr']!=""){
				echo "<script>document.getElementById('codrec').value=$_GET[idr];</script>";
			}

			$sqlr="SELECT MIN(CONVERT(codigo, SIGNED INTEGER)), MAX(CONVERT(codigo, SIGNED INTEGER)) FROM tesoingresos_predial ORDER BY CONVERT(codigo, SIGNED INTEGER)";
			$res=mysqli_query($linkbd, $sqlr);
			$r=mysqli_fetch_row($res);
			$_POST['minimo']=$r[0];
			$_POST['maximo']=$r[1];

			if($_POST['oculto']==""){
				if ($_POST['codrec']!="" || $_GET['idr']!=""){
					if($_POST['codrec']!=""){
						$sqlr="SELECT * FROM tesoingresos_predial WHERE codigo='$_POST[codrec]'";
					}
					else{
						$sqlr="SELECT * FROM tesoingresos_predial WHERE codigo ='$_GET[idr]'";
					}
				}
				else{
					$sqlr="SELECT * FROM  tesoingresos_predial ORDER BY CONVERT(codigo, SIGNED INTEGER) DESC";
				}
				$res=mysqli_query($linkbd, $sqlr);
				$row=mysqli_fetch_row($res);
			   	$_POST['codigo']=$row[0];
			}

 			if(($_POST['oculto']!="2")&&($_POST['oculto']!="6")&&($_POST['oculto']!="7")){	
 		 		$linkbd=conectar_V7();
				$sqlr="SELECT * FROM tesoingresos_predial WHERE tesoingresos_predial.codigo='$_POST[codigo]' ";
 				$cont=0;
 				$resp = mysqli_query($linkbd, $sqlr);
				while ($row = mysqli_fetch_row($resp)){	 
					$_POST['codigo']=$row[0];
				 	$_POST['nombre']=$row[1]; 	
					$_POST['tipo']=$row[2];				
				}	 
			}
			if($_POST['oculto']=="7" && $_POST['terceros']=="on")
			{
				$_POST['terceros']=1;
			}
 			if(($_POST['oculto']!="2")&&($_POST['oculto']!="6")&&($_POST['oculto']!="7"))
 			{
				unset($_POST['dcuentas']);
				unset($_POST['dncuentas']);
				unset($_POST['dconceptos']);	 		 		 		 		 
				unset($_POST['dnconceptos']);	
				unset($_POST['dfuentes']);	
				unset($_POST['dvalores']);
				unset($_POST['cuin']); 		 		 		 		 		 	
				unset($_POST['ncuin']);
				unset($_POST['clasificador']);
				unset($_POST['nclasificador']);
				$_POST['dcuentas']=array();	
				$_POST['dncuentas']=array();	
				$_POST['dconceptos']=array();		 		 		 		 		 
				$_POST['dnconceptos']=array();	
				$_POST['dfuentes']=array();	 		 
				$_POST['dvalores']=array();
				$_POST['cuin'] = array();
				$_POST['ncuin'] = array();
				$_POST['clasificador'] = array();
				$_POST['nclasificador'] = array();							
				$_POST['concecont']="";
				$_POST['cuenta']="";
				$_POST['ncuenta']="";
				$sqlr="SELECT precio FROM tesoingresos_predial_precios WHERE tesoingresos_predial_precios.ingreso='$_POST[codigo]' AND estado='S' ";
				$resp = mysqli_query($linkbd, $sqlr);	
				while ($row =mysqli_fetch_row($resp))
				{
					$_POST['precio'] = $row[0];
				}
				
				$sqlr="SELECT tesoingresos_predial.tipo, tesoingresos_predial.codigo, tesoingresos_predial_det.cuentapres, tesoingresos_predial_det.porcentaje, tesoingresos_predial_det.concepto, tesoingresos_predial_det.cuenta_clasificadora, tesoingresos_predial_det.fuente  FROM tesoingresos_predial INNER JOIN tesoingresos_predial_det ON tesoingresos_predial.codigo=tesoingresos_predial_det.codigo WHERE   tesoingresos_predial_det.modulo=4 AND tesoingresos_predial.codigo='$_POST[codigo]'  AND vigencia = (SELECT MAX(vigencia) FROM tesoingresos_predial_det WHERE codigo = '$_POST[codigo]') ORDER BY  tesoingresos_predial_det.cuentapres";
				$cont=0;
				$resp = mysqli_query($linkbd, $sqlr);
				while ($row =mysqli_fetch_row($resp)){	
					
					$sqlr1="SELECT * FROM conceptoscontables WHERE modulo='4' AND tipo='PR' AND codigo ='$row[4]' ";
					$resp1 = mysqli_query($linkbd, $sqlr1);
					$row1 =mysqli_fetch_row($resp1);

					if ($row[0]=='C'){ 
						$_POST['dcuentas'][$cont]=$row[2];
						$_POST['dncuentas'][$cont]=buscaNombreCuentaCCPET($row[2], 1);
						$_POST['dvalores'][$cont]=$row[3];	
						$_POST['dfuentes'][$cont]=$row[6];	
						$_POST['dnconceptos'][$cont]=$row1[0]." ".$row1[1];
						$_POST['dconceptos'][$cont]=$row1[0];
						$_POST['cuentaClasificadora'][$cont] = $row[5];
					}
			
					$cont=$cont+1; 
				}		
			}
			
			$sqln="SELECT * FROM tesoingresos_predial WHERE codigo > '$_POST[codigo]' ORDER BY codigo ASC LIMIT 1";
			$resn=mysqli_query($linkbd, $sqln);
			$row=mysqli_fetch_row($resn);
			$next="'".$row[0]."'";
			
			$sqlp="SELECT * FROM tesoingresos_predial WHERE codigo < '$_POST[codigo]' ORDER BY codigo DESC LIMIT 1";
			$resp=mysqli_query($linkbd, $sqlp);
			$row=mysqli_fetch_row($resp);
			$prev="'".$row[0]."'";
		?>

			<div id="bgventanamodalm" class="bgventanamodalm">
	            <div id="ventanamodalm" class="ventanamodalm">
	                <IFRAME src="" name="ventanam" marginWidth=0 marginHeight=0 frameBorder=0 id="ventanam" frameSpacing=0 style=" width:700px; height:130px; top:200; overflow:hidden;"> 
	                </IFRAME>
	            </div>
	        </div>

 			<form name="form2" method="post" action="">
 				<?php //**** busca cuenta
					if($_POST['bc']!='')
					{
						//var_dump($_POST['cuenta']);
						$nresul=buscaNombreCuentaCCPET($_POST['cuenta'], 1);			

						if($nresul!='')
						{
							$_POST['ncuenta']=utf8_decode($nresul);
						}
						else
						{
							$_POST['ncuenta']="";	
						}
					}

					if($_POST['bc'] == '2')
					{
						$nresul = buscaNombreClasificadorCCPET($_POST['cuin'], '1');

						if($nresul!='')
						{
							$_POST['ncuin']=$nresul;
						}
						else
						{
							$_POST['ncuin']="";	
						}
					}

					if($_POST['bc'] == '3')
					{
						$nresul = buscaNombreClasificadorCCPET($_POST['clasificador'], '2');

						if($nresul!='')
						{
							$_POST['nclasificador']=$nresul;
						}
						else
						{
							$_POST['nclasificador']="";	
						}
					}

					if($_POST['bc'] == '4')
					{
						$nresul = buscaNombreClasificadorCCPET($_POST['clasificadorServicios'], '3');

						if($nresul!='')
						{
							$_POST['nclasificadorServicios']=$nresul;
						}
						else
						{
							$_POST['nclasificadorServicios']="";	
						}
					} 
				?>
		
					<div>
						<table class="inicio">
							<tr>
								<td class="titulos" colspan="12">.: Edita ingresos Predial</td>
								<td class="cerrar" style="width:7%"><a href="teso-principal.php">Cerrar</a></td>
							</tr>

							<tr>
								<td class="textoNewR" style="width:7%">
									<label class="labelR">Código:</label>
								</td>

								<td style="width:10%">
									<a href="#" onClick="atrasc(<?php echo $scrtop; ?>, <?php echo $numpag; ?>, <?php echo $limreg; ?>, <?php echo $filtro; ?>, <?php echo $prev; ?>)">
										<img src="imagenes/back.png" alt="anterior" align="absmiddle">
									</a> 

									<input name="codigo" id="codigo" type="text" value="<?php echo $_POST['codigo']?>" maxlength="2" style="width:30%" onKeyPress="javascript:return solonumeros(event)" onKeyUp="return tabular(event,this)" readonly>       

									<a href="#" onClick="adelante(<?php echo $scrtop; ?>, <?php echo $numpag; ?>, <?php echo $limreg; ?>, <?php echo $filtro; ?>, <?php echo $next; ?>)">
										<img src="imagenes/next.png" alt="siguiente" align="absmiddle">
									</a> 

									<input type="hidden" value="<?php echo $_POST['maximo']?>" name="maximo" id="maximo">
									<input type="hidden" value="<?php echo $_POST['minimo']?>" name="minimo" id="minimo">
									<input type="hidden" value="<?php echo $_POST['codrec']?>" name="codrec" id="codrec">
								</td>

								<td class="textoNewR" style="width:10%">
									<label class="labelR">Nombre Ingreso:</label>
								</td>
								<td style="width:40%" colspan="5">
									<input name="nombre" id="nombre" type="text" style="width:100%" value="<?php echo $_POST['nombre']?>" onKeyUp="return tabular(event,this)">        
								</td>

								<td class="textoNewR">
									<label class="labelR">Tipo:</label>
								</td>
								<td>
									<select name="tipo" id="tipo" onChange="validar()" >
										<option value="C" <?php if($_POST['tipo']=='C') echo "SELECTED"?>>Compuesto</option>
									</select>
									<input name="oculto" id="oculto" type="hidden" value="1">		  
								</td>
							</tr> 
						</table>

						<?php
							if($_POST['tipo']=='C') //**** COMPUESTO
							{
								$linkbd=conectar_v7();
						?>
								<table class="inicio">
									<tr>
										<td colspan="7" class="titulos">Agregar Detalle Ingreso</td>
									</tr>      

									<tr>
										<td class="textoNewR" style="width:10%">
											<label class="labelR">Concepto contable:</label>
										</td>
										<td style="width:100%;" colspan="4">
											<select name="concecont" id="concecont" style="width:62%;">
												<option value="-1">Seleccione ....</option>
													<?php
														$sqlr="SELECT * FROM conceptoscontables WHERE modulo='4' AND tipo='PR' ORDER BY codigo";
														$resp = mysqli_query($linkbd, $sqlr);
														while ($row =mysqli_fetch_row($resp)) 
														{
															$i=$row[0];
															echo "<option value=$row[0] ";
															if($i==$_POST['concecont'])
															{
																echo "SELECTED";
																$_POST['concecontnom']=$row[0]." - ".$row[3]." - ".$row[1];
															}
															echo " >".$row[0]." - ".$row[3]." - ".$row[1]."</option>";	  
														}   
													?>
											</select>
											<input id="concecontnom" name="concecontnom" type="hidden" value="<?php echo $_POST['concecontnom']?>" >
										</td>
									</tr>

									<tr>
										<?php
											if($_POST['terceros']=="")
											{
										?>
												<td class="textoNewR" style="width:10%">
													<label class="labelR">Cuenta Presupuestal:</label>
												</td>
												<td style="width:50%;" valign="middle" >
													<input type="text" id="cuenta" name="cuenta" style="width:20%" onKeyPress="javascript:return solonumeros(event)" onKeyUp="return tabular(event,this)" onBlur="buscacta(event)" value="<?php echo $_POST['cuenta']?>" onClick="document.getElementById('cuenta').focus();document.getElementById('cuenta').select();">
													<input type="hidden" value="0" name="bc" id="bc">
													<a title="Cuentas presupuestales" onClick="despliegamodal2('visible');" style="cursor:pointer;"><img src="imagenes/find02.png" style="width:20px;"/></a>
													<input type="text" name="ncuenta" id="ncuenta" style="width:33%;" value="<?php echo $_POST['ncuenta']?>"  readonly>
												</td>
										<?php
											}
										?>
									</tr>
									<?php
									
										$buscarClasificador = '';

										$sqlr = "SELECT clasificadores FROM ccpetprogramarclasificadores WHERE cuenta = '".$_POST['cuenta']."' ";
										
										$resp = mysqli_query($linkbd, $sqlr);
									
										$row = mysqli_fetch_row($resp);

										if($row[0] == '1')
										{
											$buscarClasificador = '1';
										}
									
										if($row[0] == '2')
										{
											$buscarClasificador = '2';	
										}
										elseif($row[0] == '3')
										{	
											$buscarClasificador = '3';
										}
									?>

									<?php
										if($buscarClasificador == '1') {
									?>

										<tr>
											<td class="textoNewR" style="width:10%">
												<label class="labelR">CUIN:</label>
											</td>
											<td style="width:50%;" valign="middle">
												<input type="text" id="cuin" name="cuin" style="width:20%" onKeyPress="javascript:return solonumeros(event)" onKeyUp="return tabular(event,this)"  value="<?php echo $_POST['cuin']?>" onBlur="buscacta2(event,1)" onClick="document.getElementById('cuin').focus();document.getElementById('cuin').select();">
												<a title="CUIN" onClick="despliegamodal5('visible');" style="cursor:pointer;"><img src="imagenes/find02.png" style="width:20px;"/></a>
												<input type="text" name="ncuin" id="ncuin" style="width:33%;" value="<?php echo $_POST['ncuin']?>"  readonly>
											</td>
										</tr>
						
									<?php
										}

										if($buscarClasificador == '2') {
									?>
											<tr>
												<td class="textoNewR" style="width:10%">
													<label class="labelR">Bienes Transportables:</label>
												</td>
												<td style="width:50%;" valign="middle">
													<input type="text" id="clasificador" name="clasificador" style="width:20%" onKeyPress="javascript:return solonumeros(event)" onKeyUp="return tabular(event,this)"  value="<?php echo $_POST['clasificador']?>" onClick="document.getElementById('clasificador').focus();document.getElementById('clasificador').select();" onBlur="buscacta2(event,2)">
													<a title="Cuentas presupuestales" onClick="despliegamodal3('visible');" style="cursor:pointer;"><img src="imagenes/find02.png" style="width:20px;"/></a>
													<input type="text" name="nclasificador" id="nclasificador" style="width:33%;" value="<?php echo $_POST['nclasificador']?>"  readonly>
												</td>
											</tr>
									<?php
										}

										if($buscarClasificador == '3') {
									?>
											<tr>
												<td class="textoNewR" style="width:10%">
													<label class="labelR">Servicios:</label>
												</td>
												<td style="width:50%;" valign="middle">
													<input type="text" id="clasificadorServicios" name="clasificadorServicios" style="width:20%" onKeyPress="javascript:return solonumeros(event)" onKeyUp="return tabular(event,this)"  value="<?php echo $_POST['clasificadorServicios']?>" onClick="document.getElementById('clasificadorServicios').focus();document.getElementById('clasificadorServicios').select();" onBlur="buscacta2(event,3)">
													<a title="Clasificador Servicios" onClick="despliegamodal4('visible');" style="cursor:pointer;"><img src="imagenes/find02.png" style="width:20px;"/></a>
													<input type="text" name="nclasificadorServicios" id="nclasificadorServicios" style="width:33%;" value="<?php echo $_POST['nclasificadorServicios']?>"  readonly>
												</td>
											</tr>
									<?php
										}

										//Mirar cual tiene datos 
										$cuentaClasificadora;
										if(isset($_POST['cuin']))
										{
											$cuentaClasificadora = $_POST['cuin'];
										}
										elseif(isset($_POST['clasificador']))
										{
											$cuentaClasificadora = $_POST['clasificador'];
										}
										elseif(isset($_POST['clasificadorServicios']))
										{
											$cuentaClasificadora = $_POST['clasificadorServicios'];
										}
										else
										{
											$cuentaClasificadora = '';
										}

										if ($_POST['cuenta'] != '') {
											
									?>
										<tr>	
											<td class="textoNewR" style="width:10%">
												<label class="labelR">Fuente:</label>
											</td>
											<td style="width: 15%;">
												<input type="text" id="fuente" name="fuente" style="width:10%" onKeyPress="javascript:return solonumeros(event)" onKeyUp="return tabular(event,this)" onBlur="buscacta(event)" value="<?php echo $_POST['fuente']?>" autocomplete="off">
												
												<a title="Fuentes presupuestales" onClick="despliegamodal7('visible');" style="cursor:pointer;"><img src="imagenes/find02.png" style="width:20px;"/></a>

												<input type="text" name="nfuente" id="nfuente" style="width:33%" value="<?php echo $_POST['nfuente']?>"  readonly>
											</td>
										</tr>
									<?php 
										}
									?>
									<tr>
										<td class="textoNewR" style="width:10%">
											<label class="labelR">Porcentaje:</label>
										</td>
										<td style="width:20%;">
											<input id="valor" name="valor" type="text" value="<?php echo $_POST['valor']?>" style="width:20%;" onKeyUp="return tabular(event,this)" onKeyPress="javascript:return solonumeros(event)" >% 
											<input type="button" name="agregar" id="agregar" value="   Agregar   " onClick="agregardetalle()" style="margin-left: 4.5%">
											<input type="hidden" value="0" name="agregadet"><input type="hidden" value="0" name="buscap">
											<input type='hidden' name='numeroClasificador' id='numeroClasificador' >	
										</td>
									</tr> 
								</table>
			
								<?php
									//**** busca cuenta
									if($_POST['bc']!='')
									{
										$nresul=buscaNombreCuentaCCPET($_POST['cuenta'], 1);
										if($nresul!='')
										{
											$_POST['ncuenta']=utf8_decode($nresul);
								?>
								<script>
									document.getElementById('agregar').focus();
									document.getElementById('agregar').select();
								</script>
								<?php
										}
										else
										{
											$_POST['ncuenta']="";
								
										}
									}
								?>

								<div class="subpantalla" style="height:48%; width:99.5%;" id="divdet">
									<table class="inicio" style="height:300px;">
										<tr>
											<td class="titulos" colspan="7">Detalle Ingreso</td>
										</tr>
										<tr>

											<td class="titulos2" style="width:8%">Cuenta</td>
											<td class="titulos2" style="width:25%">Nombre Cuenta</td>
											<td class="titulos2" style="width:30%">Concepto</td>
											<td class="titulos2" style="width:20%">Cuenta Clasificadora</td>
											<td class="titulos2" style="width:8%">Fuente</td>
											<td class="titulos2" style="width:6%">Porcentaje</td>
											<td class="titulos2" style="width:5%"><center>
												<img src="imagenes/del.png" >
												<input type='hidden' name='elimina' id='elimina'></center>
												<input type='hidden' name='posicion' id='posicion'  >
												<input type='hidden' name='valorpos' id='valorpos'  >
												
												
											</td>
											
										</tr>
										<?php
											if($_POST['posicion']!=''){
											$pos=$_POST['posicion'];
											$val=$_POST['valorpos'];
											
											
										}
											if ($_POST['elimina']!='')
											{ 
												$posi=$_POST['elimina'];
												unset($_POST['dcuentas'][$posi]);
												unset($_POST['dncuentas'][$posi]);
												unset($_POST['dconceptos'][$posi]);	 		 		 		 		 
												unset($_POST['dnconceptos'][$posi]);	 	
												unset($_POST['dfuentes'][$posi]);	 		 		 		 		 		 
												unset($_POST['dvalores'][$posi]);
												unset($_POST['cuentaClasificadora'][$posi]); 		 		 		 		 		 	
												
												$_POST['dcuentas']= array_values($_POST['dcuentas']); 
												$_POST['dncuentas']= array_values($_POST['dncuentas']); 		 		 
												$_POST['dconceptos']= array_values($_POST['dconceptos']); 
												$_POST['dnconceptos']= array_values($_POST['dnconceptos']); 	
												$_POST['dfuentes']= array_values($_POST['dfuentes']); 		 
												$_POST['dvalores']= array_values($_POST['dvalores']);
												$_POST['cuentaClasificadora'] = array_values($_POST['cuentaClasificadora']);
												
											}
									
											if ($_POST['agregadet']=='1')
											{
												$cuentacred=0;
												$cuentadeb=0;
												$diferencia=0;
												$_POST['dcuentas'][]=$_POST['cuenta'];
												$_POST['dncuentas'][]=$_POST['ncuenta'];
												$_POST['cuentaClasificadora'][] = $cuentaClasificadora;
												$_POST['dconceptos'][]=$_POST['concecont'];		 
												$_POST['dnconceptos'][]=$_POST['concecontnom'];		 		 
												$_POST['dfuentes'][]=$_POST['fuente'];		 		 
												$_POST['dvalores'][]=$_POST['valor'];	

												$_POST['dvalores'][]=$_POST['valor'];	
												$_POST['agregadet']=0;
										?>
										<script>
											document.form2.concecont.select();
										</script>
										
										
										<?php
											}
											if($_POST['buscap']=='1')
											{
												for($x=0;$x<count($_POST['dcuentas']);$x++)
												{
													$sqlr="SELECT nombre FROM cuentasingresosccpet WHERE codigo='".$_POST['dcuentas'][$x]."'";
													$res=mysqli_query($linkbd, $sqlr);
													$row=mysqli_fetch_row($res);
													$_POST['dncuentas'][$x]=$row[0];
												}
											}
											for ($x=0;$x<count($_POST['dcuentas']);$x++)
											{	
												$sqlr = "SELECT clasificadores FROM ccpetprogramarclasificadores WHERE cuenta = '".$_POST['dcuentas'][$x]."'";
												$res = mysqli_query($linkbd, $sqlr);
												$row = mysqli_fetch_row($res);
												
												$numeroclasificador2;

												if($row[0] == '1')
												{
													$numeroclasificador2 = '1';
												}
												if($row[0] == '2')
												{
													$numeroclasificador2 = '2';
												}
												if($row[0] == '3')
												{
													$numeroclasificador2 = '3';
												}
												if(empty($row[0]))
												{
													$numeroclasificador2 = '0';
													
												}

												echo 
													"<tr>

														

														<td class='saludo2'>
															<input name='dcuentas[]' id='dcuentas[]' value='".$_POST['dcuentas'][$x]."' type='text' style='width:100%' onDblClick='despliegamodal6(\"visible\",$x)'>
														</td>

														<td class='saludo2' style='width:10%'>
															<input name='dncuentas[]' value='".$_POST['dncuentas'][$x]."' type='text'style='width:100%' readonly>
														</td>

														<td class='saludo2'>
															<input name='dnconceptos[]' value='".$_POST['dnconceptos'][$x]."' type='text' style='width:100%' onDblClick='llamarventanadeb(this,$x)' readonly>
															<input name='dconceptos[]' value='".$_POST['dconceptos'][$x]."' type='hidden'>
														</td>
											
														<td class='saludo2' style='width:10%'>
															<input name='cuentaClasificadora[]' value='".$_POST['cuentaClasificadora'][$x]."' type='text' style='width:100%' onDblClick='despliegamodalClasificadores(\"visible\",$x,$numeroclasificador2)' readonly>
														</td>

														<td class='saludo2' style='width:8%'>
															<input name='dfuentes[]' value='".$_POST['dfuentes'][$x]."' type='text' style='width:100%' onDblClick='despliegamodalClasificadores(\"visible\",$x,$numeroclasificador2)' readonly>
														</td>

														<td class='saludo2'>
															<input name='dvalores[]' value='".$_POST['dvalores'][$x]."' type='text' style='width:70%' onDblClick='llamarventanacred(this,$x)' readonly>%
														</td>

														<td class='saludo2'>
															<center>
																<a href='#' onclick='eliminar($x)'>
																	<img src='imagenes/del.png'>
																</a>
															</center>
														</td>

													</tr>";
											}	 
										?>
										<tr></tr>
									</table>	
								</div>
						<?php
							}
						?>
					</div>
				</div>
			</div>
							
		
		<div id="bgventanamodal2">
			<div id="ventanamodal2">
				<IFRAME  src="" name="buster" marginWidth=0 marginHeight=0 frameBorder=0 id="ventana2" frameSpacing=0 style="left:500px; width:900px; height:500px; top:200;"> 
				</IFRAME>
			</div>
       	</div>

    </form>
  			<?php
				$oculto=$_POST['oculto'];

				if($_POST['oculto']=='2')
				{
					
			
					$sqlr = "UPDATE tesoingresos_predial SET nombre = '$_POST[nombre]', tipo = '$_POST[tipo]' WHERE codigo = '$_POST[codigo]'";
					
					if (!mysqli_query($linkbd, $sqlr))
					{
						echo "
							<script>
								Swal.fire({
									icon: 'error',
									title: 'Error en guardado de cabecera'
								})
							";
					}
					else
					{
						$fecha=date('Y-m-d h:i:s');
						$sqlr="UPDATE tesoingresos_predial_precios SET estado='N' WHERE ingreso='$_POST[codigo]'";
						mysqli_query($linkbd, $sqlr);

						$sqlr="INSERT INTO tesoingresos_predial_precios (ingreso,precio,fecha,estado) VALUES ('$_POST[codigo]','$_POST[precio]','$fecha','S')";
						mysqli_query($linkbd, $sqlr);

						//Tipo compuesto
						if($_POST['tipo']=='C') {
					
							$sqlr="DELETE FROM tesoingresos_predial_det WHERE tipoconce='C' AND codigo = '$_POST[codigo]' AND vigencia = '$vigusu' ";
						
							if (!mysqli_query($linkbd, $sqlr))
							{
								echo "
									<script>
										Swal.fire({
											icon: 'error',
											title: 'Error en guardado'
										})
									</script>
								";
							}
							else
							{
								for($x=0;$x<count($_POST['dcuentas']);$x++)
								{
									
									$sqlr="INSERT INTO tesoingresos_predial_det (codigo,concepto,modulo,tipoconce,porcentaje,cuentapres,estado,vigencia, cuenta_clasificadora,fuente)VALUES ('$_POST[codigo]','".$_POST['dconceptos'][$x]."','4', 'C', '".$_POST['dvalores'][$x]."', '".$_POST['dcuentas'][$x]."','S', '$vigusu', '".$_POST['cuentaClasificadora'][$x]."','".$_POST['dfuentes'][$x]."' )";
									if (!mysqli_query($linkbd, $sqlr))
									{
										echo "
											<script>
												Swal.fire({
													icon: 'error',
													title: 'Error en guardado de detalle'
												})
											</script>
										";
									}	
										
									
								}
								echo "
									<script>
											despliegamodalm('visible','1','Se ha actualizado con exito');
									</script>";
							}
						}
					}
					
				}
			?> 
		</td>
	</tr>
     
</table>
</body>
</html>