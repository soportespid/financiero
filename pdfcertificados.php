<?php
	require_once "tcpdf/tcpdf_include.php";
	require 'comun.inc';
	class MYPDF extends TCPDF 
	{
		public function Header() 
		{	
			$linkbd = conectar_v7();
			$linkbd -> set_charset("utf8");
			$sqlr = "SELECT * FROM configbasica WHERE estado='S'";
			$res = mysqli_query($linkbd,$sqlr);
			while($row = mysqli_fetch_row($res)){
				$nit = $row[0];
				$rs = $row[1];
			}
			$this->Image('imagenes/escudo.jpg', 22, 12, 25, 23.9, 'JPG', '', 'T', true, 300, '', false, false, 0, false, false, false); 
			$this->SetFont('helvetica','B',7);
			$this->SetY(10);
			$this->RoundedRect(10, 10, 199, 31, 2,'' );
			$this->Cell(0.1);
			$this->Cell(50,31,'','R',0,'L'); 
			$this->SetY(10);
			$this->SetX(58);
			$this->SetFont('helvetica','B',12);
			$this->Cell(142,15,"$rs",0,0,'C'); 
			$this->SetY(16);
			$this->SetX(58);
			$this->SetFont('helvetica','B',11);
			$this->Cell(142,10,"$nit",0,0,'C');
			//*****************************************************************************************************************************
			$this->SetFont('helvetica','B',14);
			$this->SetY(10);
			$this->Cell(50.1);
			$this->Cell(149,31,'',0,1,'C'); 
			$this->SetY(8);
			$this->Cell(50.1);
			//************************************
			$this->SetFont('helvetica','B',10);
			$this->SetY(27);
			$this->Cell(161.1);
			$this->Cell(37.8,14,'','TL',0,'L');
			$this->SetY(27.5);
			$this->Cell(162);
			$this->SetY(29.5);
			$this->Cell(162);
			$this->Cell(35,5,'VIGENCIA F.: '.$_POST['vigencias'],0,0,'L');
			$this->SetY(35.5);
			$this->Cell(162);
			$this->Cell(35,5,'FECHA: '.date('d/m/Y'),0,0,'L');
			
			$this->SetY(27);
			$this->Cell(50.2);
			$this->SetFont('helvetica','B',12);
			$this->Cell(111,14,'CERTIFICADO DE RETENCION','T',0,'C'); 
			$this->ln(1);

			$this->SetFont('times','B',12);
			$this->SetY(46);
			$this->Cell(199,12,'CERTIFICA:',0,0,'C');		
			

			$this->SetY(60);
			$this->SetFont('times','',10);
			$this->cell(0.1);
			if($_POST['fecha1']!="" && $_POST['fecha2']!=""){
				$cond=" y entre el periodo de $_POST[fecha1] al $_POST[fecha2], ";
			}
			$this->MultiCell(199,4,"Durante el año gravable de ".$_POST['vigencias']." ".$cond." practicó en el ".$rs.", Retención en la Fuente al Proveedor ".$_POST['ntercero']." con Nit/Cc ".$_POST['tercero']."\n",0,'J');
			$this->ln(4);
			$this->line(10,75,209,75);

			$this->SetFont('times','B',10);
			$this->SetY(76);
			$this->Cell(0.1);
			$this->Cell(24,5,'CODIGO ',0,1,'C'); 
			$this->SetY(76);
			$this->Cell(24.1);
			$this->Cell(78,5,'RETENCION',0,1,'C');
			$this->SetY(76);
			$this->Cell(102);
			$this->Cell(63,5,'Monto del Pago Sujeto a Retención',0,1,'C');
			$this->SetY(76);
			$this->Cell(165);
			$this->Cell(34,5,'VALOR',0,1,'C');		
			$this->line(10,82,209,82);
			$this->ln(2);
		}
		public function Footer() {
			$this->SetY(-15);
			$this->SetFont('times','I',10);
			$this->Cell(0,10,'Impreso por: Software IDEAL.10 - IDEAL 10 SAS. - Pagina '.$this->PageNo().' de {nb}',0,0,'R'); // el parametro {nb} 	
		}
	}

	$pdf = new MYPDF('P','mm','Letter', true, 'utf8', false);
	$pdf->AddPage();
	$pdf->SetFont('Times','',10);
	$pdf->SetAutoPageBreak(true,20);
	$pdf->SetY(81);   
	$con=0;
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
	$sqlr="select *from configbasica where estado='S'";
	$res=mysqli_query($linkbd, $sqlr);
	while($row=mysqli_fetch_row($res))
	{
	$nit=$row[0];
	$rs=$row[1];
	}
	while ($con<count($_POST['valores']))
	{	if ($con%2==0)
		{$pdf->SetFillColor(245,245,245);
		}
		else
		{$pdf->SetFillColor(255,255,255);
		}
		$pdf->Cell(24,4,''.$_POST['codigo'][$con],0,0,'C',0);
		$pdf->Cell(95,4,substr(''.$_POST['nombres'][$con],0,50),0,0,'L',0);
		$pdf->Cell(40,4,$_POST['valoresret'][$con],0,0,'R',0);
		$pdf->Cell(40,4,''.number_format($_POST['valores'][$con],2),0,0,'R',0);
		$pdf->ln(4);	
		$con=$con+1;
		$total+=$_POST['valores'][$con];
	}
		$pdf->SetFont('helvetica','',10);
		$pdf->ln(4);
		$pdf->SetLineWidth(0.5);
		$pdf->cell(110,5,'','T',0,'R');
		$pdf->cell(55,5,'Total','T',0,'R');
		$pdf->cell(34,5,''.number_format($total,2),'T',0,'R');
		$pdf->SetLineWidth(0.2);
		$pdf->ln(10);
		$v=$pdf->gety();

		$pdf->MultiCell(199,4,"El presente certificado se expide en concordancia con las disposiciones legales contenidas en el artículo 381 del Estatuto Tributario \n",0,'J');
		$pdf->ln(4);

		$pdf->MultiCell(199,4,"Dicha retención fue consignada oportunamente a nombre de La Administración de Impuestos Nacionales en el $rs \n",0,'J');
		$pdf->ln(4);

		$pdf->MultiCell(199,4,"Señor (a) recuerde que Ud. Puede estar obligado a declarar renta por el año gravable de ".$_POST['vigencias'].".\n",0,'J');
		$pdf->ln(4);

		$pdf->MultiCell(199,4,"Para mayor información lo invitamos a que consulte la página en internet (www.dian.gov.co) o acerquese a la Administración más cercana a su domicilio\n",0,'J');
		$pdf->ln(4);

		$pdf->MultiCell(199,4,"NO REQUIERE FIRMA AUTOGRAFA ART. 10 DECRETO 836/91 \n",0,'J');

	$pdf->Output();
?>