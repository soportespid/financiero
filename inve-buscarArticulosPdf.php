<?php
	require_once("tcpdf/tcpdf_include.php");
	require('comun.inc');
	require "funciones.inc";
	date_default_timezone_set("America/Bogota");
	session_start();

    class MYPDF extends TCPDF {

        // Load table data from file

        public function Header() 
		{
			if ($_POST['estado']=='R'){$this->Image('imagenes/reversado02.png',75,41.5,50,15);}
			$linkbd = conectar_v7();
			$linkbd -> set_charset("utf8");
			$sqlr="select *from configbasica where estado='S' ";
			//echo $sqlr;
			$res=mysqli_query($linkbd, $sqlr);
			while($row=mysqli_fetch_row($res))
			{
				$nit=$row[0];
				$rs=$row[1];
				$nalca=$row[6];
			}
			$detallegreso = $_POST['detallegreso'];
			//Parte Izquierda
			$this->Image('imagenes/escudo.jpg',  13, 13, 20, 20, 'JPG', '', 'T', true, 300, '', false, false, 0, false, false, false);// Logo
			$this->SetFont('helvetica','B',8);
			$this->SetY(10);
			$this->RoundedRect(10, 10, 277, 25, 1,'');
			$this->Cell(0.1);
			$this->Cell(26,25,'','R',0,'L'); 
			$this->SetY(8);
			$this->SetX(80);
			$this->SetFont('helvetica','B',9);
			$this->Cell(160,15,strtoupper("$rs"),0,0,'C'); 
			$this->SetFont('helvetica','B',7);
			$this->SetY(12);
			$this->SetX(80);
			$this->Cell(160,15,'NIT: '.$nit,0,0,'C');
			//*****************************************************************************************************************************
			$this->SetFont('helvetica','B',9);			 
			$this->SetY(23);
			$this->SetX(36);
			$this->Cell(251,12,"REPORTE DE ARTICULOS",'T',0,'C'); 
			$mov='';
			if(isset($_POST['movimiento']))
			{
				if(!empty($_POST['movimiento']))
				{
					if($_POST['movimiento']=='401'){$mov="DOCUMENTO DE REVERSION";}
				}
			}

			$this->SetFont('helvetica','B',6);
			
			$this->SetY(10);
			$this->SetX(257);
			$this->Cell(35,6.8," FECHA: ".date("d/m/Y"),"L",0,'L');
			$this->SetY(17);
			$this->SetX(257);
			$this->Cell(35,6," VIGENCIA: ".date("Y"),"L",0,'L');
			//**********************************************************
			$this->SetFont('times','B',10);
			$this->ln(12);
			//**********************************************************
		}
        public function Footer() 
		{
			$linkbd = conectar_v7();
			$linkbd -> set_charset("utf8");
			$sqlr="SELECT direccion,telefono,web,email FROM configbasica WHERE estado='S'";
			$resp=mysqli_query($linkbd, $sqlr);
			$user = $_SESSION['nickusu'];	
			$fecha = date("Y-m-d H:i:s");
			$ip = $_SERVER['REMOTE_ADDR'];
			$useri = $_POST['user'];
			while($row=mysqli_fetch_row($resp))
			{
				$direcc=strtoupper($row[0]);
				$telefonos=$row[1];
				$dirweb=strtoupper($row[3]);
				$coemail=strtoupper($row[2]);
			}
			if($direcc!=''){$vardirec="Dirección: $direcc, ";}
			else {$vardirec="";}
			if($telefonos!=''){$vartelef="Telefonos: $telefonos";}
			else{$vartelef="";}
			if($dirweb!=''){$varemail="Email: $dirweb, ";}
			else {$varemail="";}
			if($coemail!=''){$varpagiw="Pagina Web: $coemail";}
			else{$varpagiw="";}
			$this->SetFont('helvetica', 'I', 8);
			$txt = <<<EOD
			$vardirec $vartelef
			$varemail $varpagiw
			EOD;
			$this->SetFont('helvetica', 'I', 6);
			$this->Cell(277,10,'','T',0,'T');
			$this->ln(2);
			$this->Write(0, $txt , '', 0, 'C', true, 0, false, false, 0);
			//$this->Cell(25, 10, 'Hecho por: '.$useri, 0, false, 'C', 0, '', 0, false, 'T', 'M');
			$this->Cell(25, 10, 'Impreso por: '.$user, 0, false, 'C', 0, '', 0, false, 'T', 'M');
            $this->SetX(25);
			$this->Cell(107, 10, 'IP: '.$ip, 0, false, 'C', 0, '', 0, false, 'T', 'M');
			$this->Cell(35, 10, 'Fecha: '.$fecha, 0, false, 'C', 0, '', 0, false, 'T', 'M');
            $this->Cell(107, 10, 'IDEAL.10 S.A.S', 0, false, 'C', 0, '', 0, false, 'T', 'M');
			$this->Cell(20, 10, 'Pagina '.$this->getAliasNumPage().'/'.$this->getAliasNbPages(), 0, false, 'C', 0, '', 0, false, 'T','M');

			
		}
        // Colored table
        public function ColoredTable($data) {
            // Colors, line width and bold font
            $this->SetFillColor(222, 222, 222);
            $this->SetTextColor(000);
            $this->SetDrawColor(128, 0, 0);
            $this->SetLineWidth(0.3);
            $this->SetFont('helvetica','',6);
            // Header
            $w = array(20, 90, 80,67,20);
            $header =array(
                "Código de artículo",
                "Grupo de inventario",
                "Código CPC",
                "Nombre",
                "Unidad de medida"
            );
            $num_headers = count($header);
            for ($i=0; $i < $num_headers; $i++) { 
                $this->Cell($w[$i], 7, $header[$i], 0, 0, 'C', 1);
            }
            $this->Ln();
            // Color and font restoration
            $this->SetFillColor(245,245,245);
            $this->SetTextColor(0);
            $this->SetFont('');
            // Data
            $fill = 0;
            foreach($data as $row) {
                $this->Cell($w[0], 6, $row['codigo_articulo'], '', 0, 'C', $fill);
                $this->Cell($w[1], 6, $row['grupoinven'], '', 0, 'C', $fill);
                $this->Cell($w[2], 6, $row['codunspsc'], '', 0, 'C', $fill);
                $this->Cell($w[3], 6, strtoupper($row['nombre']), '', 0, 'C', $fill);
                $this->Cell($w[4], 6, $row['unidad'], '', 0, 'C', $fill);
                $this->Ln();
                $fill=!$fill;
            }
            $this->Cell(array_sum($w), 0, '', '');
        }
    }
    
    // create new PDF document
    $pdf = new MYPDF('L','mm','Letter', true, 'UTF-8', false);
    
    // set document information
    $pdf->SetCreator(PDF_CREATOR);
	$pdf->SetAuthor('IDEALSAS');
	$pdf->SetTitle('REPORTE DE ARTICULOS');
	$pdf->SetSubject('REPORTE DE ARTICULOS');
    $pdf->SetKeywords('TCPDF, PDF, example, test, guide');
    
    // set margins
    $pdf->SetMargins(10, 38, 10);// set margins
	$pdf->SetHeaderMargin(38);// set margins
	$pdf->SetFooterMargin(17);// set margins
	$pdf->SetAutoPageBreak(TRUE, 20);
    
    
    // set some language-dependent strings (optional)
    if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
        require_once(dirname(__FILE__).'/lang/eng.php');
        $pdf->setLanguageArray($l);
    }
    
    // ---------------------------------------------------------
    
    // set font
    $pdf->SetFont('helvetica', '', 12);
    
    // add a page
    $pdf->AddPage();
    
    // column titles
    $header = array('Country', 'Capital', 'Area (sq km)', 'Pop. (thousands)');
    
    // data loading
    $linkbd=conectar_v7();
    $search = $_GET['search'];

    $sql = "SELECT codigo,nombre,codunspsc,grupoinven,estado, CONCAT(grupoinven,codigo) as codigo_articulo
            FROM almarticulos 
            WHERE nombre like '$search%' OR CONCAT(grupoinven,codigo) like '$search%' 
            ORDER BY length(codigo),codigo ASC";
	$request = mysqli_fetch_all(mysqli_query($linkbd,$sql),MYSQLI_ASSOC);
    $total = count($request);   
    for ($i=0; $i < $total ; $i++) { 
        $strCodigoGrupo = $request[$i]['grupoinven'];
		$strCodigoArticulo = $strCodigoGrupo.$request[$i]['codigo'];
		$strCodigoBien = $request[$i]['codunspsc'];
		$sql = "SELECT unidad FROM almarticulos_det WHERE articulo = '$strCodigoArticulo'";
		$request[$i]['unidad'] = mysqli_query($linkbd,$sql)->fetch_assoc()['unidad'];
		$request[$i]['grupoinven'] = $strCodigoGrupo." - ".mysqli_query(
			$linkbd,"SELECT nombre FROM almgrupoinv WHERE codigo ='$strCodigoGrupo'"
		)->fetch_assoc()['nombre'];
		$request[$i]['codunspsc'] = $strCodigoBien." - ".mysqli_query(
			$linkbd,
			"SELECT titulo FROM ccpetbienestransportables 
			WHERE version = (SELECT MAX(version) FROM ccpetbienestransportables)  AND LENGTH(grupo) = 7 AND grupo ='$strCodigoBien'"
		)->fetch_assoc()['titulo'];
    }
    // print colored table
    $pdf->ColoredTable($request);
    
    // ---------------------------------------------------------
    
    // close and output PDF document
    $pdf->Output('reporte_recaudo.pdf', 'I');
    
    //============================================================+
    // END OF FILE
    //============================================================+
?> 


