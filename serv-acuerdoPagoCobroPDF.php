<?php
    require_once("tcpdf/tcpdf_include.php");
    require './funciones.inc';
    require './funcionesSP.inc.php';

    session_start();
    class MYPDF extends TCPDF {

        // Load table data from file

        public function Header() 
        {
            if ($_POST['estado']=='R'){$this->Image('imagenes/reversado02.png',75,41.5,50,15);}
            $linkbd = conectar_v7();
            $linkbd -> set_charset("utf8");
            $sqlr="select *from configbasica where estado='S' ";
            //echo $sqlr;
            $res=mysqli_query($linkbd, $sqlr);
            while($row=mysqli_fetch_row($res))
            {
                $nit=$row[0];
                $rs=$row[1];
                $nalca=$row[6];
            }
            //Parte Izquierda
            $this->Image('imagenes/escudo.jpg',  13, 13, 20, 20, 'JPG', '', 'T', true, 300, '', false, false, 0, false, false, false);// Logo
            $this->SetFont('helvetica','B',8);
            $this->SetY(10);
            $this->RoundedRect(10, 10, 190, 25, 1,'');
            $this->Cell(0.1);
            $this->Cell(26,25,'','R',0,'L'); 
            $this->SetY(8);
            $this->SetX(80);
            $this->SetFont('helvetica','B',9);
            $this->Cell(75,15,strtoupper("$rs"),0,0,'C'); 
            $this->SetFont('helvetica','B',7);
            $this->SetY(12);
            $this->SetX(80);
            $this->Cell(75,15,'NIT: '.$nit,0,0,'C');
            //*****************************************************************************************************************************
            $this->SetFont('helvetica','B',9);		
            $this->SetY(23);
            $this->SetX(36);	
            $this->Cell(164,12,"",'T',0,'C');  
            $this->SetY(23);
            $this->SetX(36);
            $this->Cell(160,12,"ACUERDO DE PAGO",'',0,'C'); 
            $this->SetY(10);
            $this->SetX(135);
            $mov='';
            if(isset($_POST['movimiento']))
            {
                if(!empty($_POST['movimiento']))
                {
                    if($_POST['movimiento']=='401'){$mov="DOCUMENTO DE REVERSION";}
                }
            }

            $this->SetFont('helvetica','B',6);
            
            $this->SetY(10);
            $this->SetX(257);
            $this->Cell(35,6.8," FECHA: ".date("d/m/Y"),"L",0,'L');
            $this->SetY(17);
            $this->SetX(257);
            $this->Cell(35,6," VIGENCIA: ".vigencia_usuarios($_SESSION['cedulausu']),"L",0,'L');
            //**********************************************************
            $this->SetFont('times','B',10);
            $this->ln(12);
            //**********************************************************
        }
        public function Footer() {
        
            $linkbd = conectar_v7();
            $linkbd -> set_charset("utf8");
            $sqlr="SELECT direccion,telefono,web,email FROM configbasica WHERE estado='S'";
            $resp=mysqli_query($linkbd, $sqlr);
            $user = $_SESSION['nickusu'];	
            $fecha = date("Y-m-d H:i:s");
            $ip = $_SERVER['REMOTE_ADDR'];
            $useri = $_POST['user'];
            while($row=mysqli_fetch_row($resp))
            {
                $direcc=strtoupper($row[0]);
                $telefonos=$row[1];
                $dirweb=strtoupper($row[3]);
                $coemail=strtoupper($row[2]);
            }
            if($direcc!=''){$vardirec="Dirección: $direcc, ";}
            else {$vardirec="";}
            if($telefonos!=''){$vartelef="Telefonos: $telefonos";}
            else{$vartelef="";}
            if($dirweb!=''){$varemail="Email: $dirweb, ";}
            else {$varemail="";}
            if($coemail!=''){$varpagiw="Pagina Web: $coemail";}
            else{$varpagiw="";}
            $this->SetFont('helvetica', 'I', 8);
            $txt = <<<EOD
            $vardirec $vartelef
            $varemail $varpagiw
            EOD;
            $this->SetFont('helvetica', 'I', 6);
            $this->Cell(277,10,'','T',0,'T');
            $this->ln(2);
            $this->Write(0, $txt , '', 0, 'C', true, 0, false, false, 0);
            //$this->Cell(25, 10, 'Hecho por: '.$useri, 0, false, 'C', 0, '', 0, false, 'T', 'M');
            $this->Cell(25, 10, 'Impreso por: '.$user, 0, false, 'C', 0, '', 0, false, 'T', 'M');
            $this->SetX(25);
            $this->Cell(107, 10, 'IP: '.$ip, 0, false, 'C', 0, '', 0, false, 'T', 'M');
            $this->Cell(35, 10, 'Fecha: '.$fecha, 0, false, 'C', 0, '', 0, false, 'T', 'M');
            $this->Cell(107, 10, 'IDEAL.10 S.A.S', 0, false, 'C', 0, '', 0, false, 'T', 'M');
            $this->Cell(20, 10, 'Pagina '.$this->getAliasNumPage().'/'.$this->getAliasNbPages(), 0, false, 'C', 0, '', 0, false, 'T','M');

            
        }
        // Colored table
        public function ColoredTable($data) {
            // Colors, line width and bold font
            $this->SetFillColor(222, 222, 222);
            $this->SetTextColor(000);
            $this->SetDrawColor(128, 0, 0);
            $this->SetLineWidth(0.3);
            $this->SetFont('helvetica','',6);
            // Header
            $w = array(25, 20, 50,20,20,20,20,50,20,32);
       
            $this->Ln();
            // Color and font restoration
            $this->SetFillColor(245,245,245);
            $this->SetTextColor(0);
            $this->SetFont('');
            // Data
            $fill = 0;
            $total = 0;
            
            $this->Cell(array_sum($w), 0, '', '');
            $this->Ln();
            $this->SetFont('helvetica','B',8);
            $this->SetFillColor(245,245,245);
            $this->Cell(array_sum($w)-$w[9], 6, 'TOTAL:', '', 0, 'R');
            $this->Cell($w[count($w)-1], 6, '$'.number_format($total), '', 0, 'R');
        }
    }
    
    // if($_GET['id']){
        $linkbd = conectar_v7();
        $linkbd->set_charset("utf8");
        $intId = intval($_GET['id']);

              
        $bancos = json_decode($_POST['bancos'],true);

        foreach ($bancos as $key => $data) {
            if ($data["cuentaContable"] == $_POST['banco']) {
                $nameBanco = $data["nombre"];
                $cuentaBancaria = $data["cuentaBancaria"];
            }
        }

        // if($intId > 0){
            // $request = mysqli_query($linkbd,"SELECT *, DATE_FORMAT(fecha_recaudo,'%d/%m/%Y') as fecha_recaudo FROM srv_recaudo_factura WHERE id = $intId")->fetch_assoc();
            // if(!empty($request)){ 
                // $request['valor_factura'] = ceil (consultaValorFactura($request['numero_factura']) / 100) * 100;
                $pdf = new MYPDF('P','mm','Letter', true, 'UTF-8', false);
    
                // set document information
                $pdf->SetCreator(PDF_CREATOR);
                $pdf->SetAuthor('IDEALSAS');
                $pdf->SetTitle('ACUERDO DE PAGO');
                $pdf->SetSubject('REPORTE DE RECAUDO');
                $pdf->SetKeywords('TCPDF, PDF, example, test, guide');
                
                // set margins
                $pdf->SetMargins(10, 38, 10);// set margins
                $pdf->SetHeaderMargin(38);// set margins
                $pdf->SetFooterMargin(17);// set margins
                $pdf->SetAutoPageBreak(TRUE, 20);
                
                
                // set some language-dependent strings (optional)
                if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
                    require_once(dirname(__FILE__).'/lang/eng.php');
                    $pdf->setLanguageArray($l);
                }
                
                // ---------------------------------------------------------
                // add a page
                $pdf->AddPage();
                $pdf->setX(10);
                $pdf->setY(50);
                $pdf->SetFont('helvetica', 'B', 12);
                // $pdf->Cell(55, 6, "Consecutivo de recaudo:", '', 0, 'C', 0);
                $pdf->setY(50);
                $pdf->SetFont('helvetica', '', 12);
                // $pdf->Cell(125, 6, $request['codigo_recaudo'], '', 0, 'C', 0);
                $pdf->setY(55);
                $pdf->SetFont('helvetica', 'B', 12);
                $pdf->Cell(42, 6, "Fecha de recaudo:", '', 0, 'C', 0);
                $pdf->setY(55);
                $pdf->SetFont('helvetica', '', 12);
                $pdf->Cell(110, 6, $_POST["fechaAcuerdo"], '', 0, 'C', 0);
                $pdf->setY(70);
                $pdf->Cell(190, 6, "", 'T', 0, 'C', 0);
                $pdf->setY(75);
                $pdf->SetFont('helvetica', 'B', 12);
                $pdf->Cell(36, 6, "RECIBIDO DE:", '', 0, 'C', 0);
                $pdf->setY(85);
                $pdf->setX(13);
                $pdf->SetFont('helvetica', '', 12);
                $pdf->Cell(25, 6, "Código de usuario:", '', 0, 'L', 0);
                $pdf->setY(85);
                $pdf->SetFont('helvetica', '', 12);
                $pdf->setX(50);
                $pdf->Cell(180, 6, $_POST["codUsuario"], '', 0, 'L', 0);
                $pdf->setY(95);
                $pdf->setX(13);
                $pdf->SetFont('helvetica', '', 12);
                $pdf->Cell(25, 6, "Número de factura:", '', 0, 'L', 0);
                $pdf->setY(95);
                $pdf->SetFont('helvetica', '', 12);
                $pdf->setX(50);
                $pdf->Cell(180, 6, $_POST["numFactura"], '', 0, 'L', 0);
                $pdf->setY(105);
                $pdf->SetFont('helvetica', '', 12);
                $pdf->Cell(25, 6, "Nombre:", '', 0, 'C', 0);
                $pdf->setY(105);
                $pdf->SetFont('helvetica', '', 12);
                $pdf->setX(33);
                $pdf->Cell(180, 6, $_POST["name"], '', 0, 'L', 0);
                $pdf->setY(115);
                $pdf->SetFont('helvetica', '', 12);
                $pdf->Cell(25, 6, "CC/NIT:", '', 0, 'C', 0);
                $pdf->setY(115);
                $pdf->SetFont('helvetica', '', 12);
                $pdf->setX(33);
                $pdf->Cell(180, 6, $_POST["document"], '', 0, 'L', 0);
                $pdf->setY(125);
                $pdf->Cell(190, 6, "", 'T', 0, 'C', 0);
                $pdf->setY(135);
                $pdf->Cell(190, 6, "", 'T', 0, 'C', 0);
                $pdf->setY(145);
                $pdf->SetFont('helvetica', 'B', 12);
                $pdf->Cell(36, 6, "CONCEPTO:", '', 0, 'C', 0);
                $pdf->setY(155);
                $pdf->SetFont('helvetica', '', 11);
                $pdf->setX(15);
                $pdf->Cell(36, 6, "La entidad autoriza el pago imparcial de factura $_POST[numFactura] para realizar acuerdo de pago con la entidad", '', 0, 'L', 0);
                $pdf->setY(165);
                $pdf->setX(15);
                $pdf->SetFont('helvetica', 'B', 12);
                $pdf->Cell(36, 6, "Descuento de intereses:", '', 0, 'L', 0);
                $pdf->setY(165);
                $pdf->setX(65);
                $pdf->SetFont('helvetica', '', 12);
                $pdf->Cell(36, 6, $_POST["porcentajeDes"] . "%", '', 0, 'L', 0);
                $pdf->SetFont('helvetica', 'B', 12);
                $pdf->setY(175);
                $pdf->setX(15);
                $pdf->Cell(36, 6, "Medio de pago:", '', 0, 'L', 0);
                $pdf->setY(175);
                $pdf->setX(50);
                $pdf->SetFont('helvetica', '', 12);
                $pdf->Cell(36, 6, $_POST["medioPago"] . " - " . $cuentaBancaria . " - " . $nameBanco, '', 0, 'L', 0);
                $pdf->SetFont('helvetica', 'B', 12);
                $pdf->setY(185);
                $pdf->setX(15);
                $pdf->Cell(36, 6, "Valor de factura:", '', 0, 'L', 0);
                $pdf->setY(185);
                $pdf->setX(50);
                $pdf->SetFont('helvetica', '', 12);
                $pdf->Cell(36, 6, '$'.number_format($_POST["valueFactura"]), '', 0, 'L', 0);
                $pdf->SetFont('helvetica', 'B', 12);
                $pdf->setY(195);
                $pdf->setX(15);
                $pdf->Cell(36, 6, "Valor de pago:", '', 0, 'L', 0);
                $pdf->setY(195);
                $pdf->setX(50);
                $pdf->SetFont('helvetica', '', 12);
                $pdf->Cell(36, 6, '$'.number_format($_POST["valueAbono"]), '', 0, 'L', 0);
                $pdf->setY(215);
                $pdf->Cell(190, 6, "", 'T', 0, 'C', 0);
                $pdf->SetFont('helvetica', 'B', 12);
                $pdf->setY(250);
                $pdf->setX(50);
                $pdf->Cell(120, 6, "SELLO AUTORIZACIÓN", 'T', 0, 'C', 0);
                if($request['estado']=="REVERSADO"){
                    $pdf->Image('imagenes/reversado02.png',60,120,100,30);
                }
                // column titles
                $header = array('Country', 'Capital', 'Area (sq km)', 'Pop. (thousands)');
                $pdf->Output('recibo_recaudo.pdf', 'I');
                
            // }
        // }
    // }
    
    
?>