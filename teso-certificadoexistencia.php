<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require "comun.inc";
	require "funciones.inc";
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
	session_start();
    date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html lang=es>
	<head>
		<meta charset="utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1"/>
		<meta name="viewport" content="user-scalable=no">
		<title>:: IDEAL 10 - Tesorer&iacute;a</title>
		<link href="favicon.ico" rel="shortcut icon"/>
		<link href="css/css2z.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<script src="vue/vue.min.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script src="css/calendario.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<style>
			input[type=number]::-webkit-inner-spin-button,
			input[type=number]::-webkit-outer-spin-button { 
				-webkit-appearance: none; 
				margin: 0; 
			}
			input[type=number]{
				text-align:right;
				box-sizing: border-box;
				font: 120% sans-serif;
				width: 100% !important;
			}
		</style>
		<script src="sweetalert2/dist/sweetalert2.min.js"></script>
		<link rel="stylesheet" href="sweetalert2/dist/sweetalert2.min.css">
        <script>
            function pdf(codigo, ord, tot){
                document.form2.action="pdfcertificadoexistencia.php?codig="+codigo+"&ord="+ord+"&tot="+tot;
				document.form2.target="_BLANK";
				document.form2.submit(); 
				document.form2.action="";
				document.form2.target="";
            }
        </script>
	</head>
	<body>
		<IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
		<span id="todastablas2"></span>
		<header>
			<table>
				<tr><script>barra_imagenes("teso");</script><?php cuadro_titulos();?></tr>
			</table>
		</header><form name="form2" method="post" action="">
		<section id="myapp" v-cloak >
			<nav>
				<table>
					<tr><?php menu_desplegable("teso");?></tr>
					<tr>
						<td colspan="3" class="cinta">
							<img src="imagenes/add2.png"  class="mgbt1" title="Nuevo">
							<img src="imagenes/guardad.png" title="Guardar"  class="mgbt1">
							<img src="imagenes/buscad.png" class="mgbt1" title="Buscar">
							<img src="imagenes/nv.png" onClick="mypop=window.open('teso-principal.php','',''); mypop.focus();" class="mgbt" title="Nueva Ventana">
							<img src="imagenes/iratras.png" title="Atr&aacute;s" onClick="location.href='teso-gestionpredial.php'" class="mgbt">
						</td>
					</tr>
				</table>
			</nav>
			<article>
				<table class="inicio ancho">
					<tr>
						<td class="titulos" colspan="8" >Certificado de Existencia</td>
						<td class="cerrar" style="width:7%" onClick="location.href='teso-principal.php'">Cerrar</td>
					</tr>
					<tr>
						<td class="tamano01" style="width:3cm;">:&middot; Código Catastral:</td>
						<td style="width:17%;"><input type="text" style="width:100%" onKeyPress="return solonumeros(event)" v-model="searchCodigo"></td>

						<td class="tamano01" style="width:3cm;">:&middot; Nombre:</td>
						<td style="width:17%;"><input type="text" style="width:100%" v-model="searchNombre"></td>

						<td class="tamano01" style="width:3cm;">:&middot; Direcci&oacute;n:</td>
						<td style="width:17%;"><input type="text" style="width:100%" v-model="searchDirecc"></td>

						<td colspan="2" style="padding-bottom:0px"><em class="botonflechaverde" v-on:Click="iniciabuscar();">Buscar</em></td>
						<td></td>
					</tr>
				</table>
				<div class='subpantalla' style='height:66vh; width:99.2%; margin-top:0px; overflow:hidden'>
					<table class='tablamv'>
						<thead>
							<tr style="text-align:Center;">
                            <th class="titulosnew00" style="width:4%;">N°</th>
								<th class="titulosnew00" style="width:20%;">Código Catastral</th>
								<th class="titulosnew00" style="width:4%;">Ord</th>
								<th class="titulosnew00" style="width:4%;">Tot</th>
								<th class="titulosnew00" style="width:10%;">Documento</th>
								<th class="titulosnew00" style="width:20%;">Propietario</th>
								<th class="titulosnew00">Dirección</th>
								<th class="titulosnew00" style="width:15%;">Avaluo</th>
							</tr>
						</thead>
						<tbody>
							<tr v-for="(vcinfo1, index) in infobasico1" v-bind:class="index % 2 ? 'saludo1a' : 'saludo2'"  style='text-rendering: optimizeLegibility; cursor: pointer !important;' v-on:dblClick ="pdf(vcinfo1[0], vcinfo1[1],vcinfo1[2])" >
								<td style="width:4%; font: 120% sans-serif; padding-left:5px; text-align:Center;">{{ index + 1 }}</td>
								<td style="width:20%; font: 120% sans-serif; padding-left:5px; text-align:Center;">{{ vcinfo1[0] }}</td>
								<td style="width:4%; font: 120% sans-serif; padding-left:5px; text-align:Center;">{{ vcinfo1[1] }}</td>
								<td style="width:4%; font: 120% sans-serif; padding-left:5px; text-align:Center;">{{ vcinfo1[2] }}</td>
								<td style="width:10%; font: 120% sans-serif; padding-left:5px; text-align:Center;">{{ vcinfo1[3] }}</td>
								<td style="width:20%; font: 120% sans-serif; padding-left:5px; text-align:justify;">{{ vcinfo1[4] }}</td>
                                <td style="font: 120% sans-serif; padding-left:5px; text-align:justify;">{{ vcinfo1[5] }}</td>
								<td style="width:15%; font: 120% sans-serif; padding-left:5px; text-align:right;">{{ formatonumero(vcinfo1[6]) }}</td>
							</tr>
                        </tbody>
					</table>
				</div>
				<div id="cargando" v-if="loading" class="loading">
					<span>Cargando...</span>
				</div>
			</article>
		</section></form>
		<script src="Librerias/vue/vue.min.js"></script>
		<script src="Librerias/vue/axios.min.js"></script>
		<script src="vue/tesoreria/teso-certificadoexistencia.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
	</body>
</html>