<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require "comun.inc";
	require "funciones.inc";
	require "funcionesSP.inc.php";
	ini_set('max_execution_time',99999999);
	session_start();
	$linkbd = conectar_v7();
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es">
	<head>
		<meta http-equiv="Content-type" content="text/html; charset=utf8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=9"/>
		<title>:: IDEAL 10 - Servicios Públicos</title>
		<link href="css/css2.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
        <link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/cssSP.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<script type="text/javascript" src="css/programas.js"></script>
        <script type="text/javascript" src="css/calendario.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
        <script type="text/javascript" src="jquery-1.11.0.min.js"></script>
		
        <script>
            $(window).load(function() {
                $('#cargando').hide();
            });
        </script>

		<script>
			function despliegamodalm(_valor,_tip,mensa,pregunta)
			{
				document.getElementById("bgventanamodalm").style.visibility=_valor;
				if(_valor == "hidden")
				{
					document.getElementById('ventanam').src = "";
				}
				else
				{
					switch(_tip)
					{
						case "1":	document.getElementById('ventanam').src="ventana-mensaje1.php?titulos="+mensa;
                        break;
						case "2":	document.getElementById('ventanam').src="ventana-mensaje3.php?titulos="+mensa;
                        break;
						case "3":	document.getElementById('ventanam').src="ventana-mensaje2.php?titulos="+mensa;
                        break;
						case "4":	document.getElementById('ventanam').src="ventana-consulta1.php?titulos="+mensa+"&idresp="+pregunta;
                        break;	
					}
				}
			}

			function respuestaconsulta(pregunta)
			{
				switch(pregunta)
				{
					case "1":	document.form2.oculto.value = '2';
								document.form2.submit();
								break;
				}
			}

            function generarFacturacion()
            {
				var Servicio = document.getElementById('servicio').value;
				var FechaInicial = document.getElementById('fecha_inicial').value;
				var FechaFinal = document.getElementById('fecha_final').value;
				var FechaLimite = document.getElementById('fecha_limite').value;
				var FechaImpresion = document.getElementById('fecha_impresion').value;
				var TipoMovimiento = document.getElementById('tipoMovimiento').value;

                if(Servicio != '' && FechaInicial != '' && FechaFinal != '' && FechaLimite != '' && FechaImpresion != '' && TipoMovimiento != '-1')
                {	 
                    document.form2.oculto.value = '3';
                    document.form2.submit();
                }
                else 
                {
                    alert("Falta informacion para poder Agregar");
                }
            }

			function funcionmensaje()
			{
				document.location.href = "serv-generarFacturacionPeriodo.php";
			}

			function guardar()
			{
				var validacion01 = document.getElementById('codban').value;
				var validacion02 = document.getElementById('nomban').value;
				var validacion03 = document.getElementById('servicio').value

				if (validacion01.trim() != '' && validacion02.trim() && validacion03 != '') 
				{
					despliegamodalm('visible','4','Esta Seguro de Guardar','1');
				}
				else 
				{
					despliegamodalm('visible','2','Falta informaci&oacute;n para crear la Anomalia');
				}
			}

			function cambiocheck()
			{
				if(document.getElementById('myonoffswitch').value=='S')
				{
					document.getElementById('myonoffswitch').value='N';
				}
				else
				{
					document.getElementById('myonoffswitch').value='S';
				}

				document.form2.submit();
			}

            function actualizar(){
                document.form2.submit();
            }

			function callprogress(vValor)
			{
				document.getElementById("getprogress").innerHTML = vValor;
				document.getElementById("getProgressBarFill").innerHTML = '<div class="ProgressBarFill" style="width: '+vValor+'%;"></div>';
				document.getElementById("titulog1").style.display='block';
				document.getElementById("progreso").style.display='block';
				document.getElementById("getProgressBarFill").style.display='block';
			}        

			function atras() {
				var link = document.referrer;

				location.href = link;
			}
		</script>

		<?php titlepag();?>

	</head>

	<body>
		<IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
		<span id="todastablas2"></span>
		<table>
			<tr><script>barra_imagenes("serv");</script><?php cuadro_titulos();?></tr>
			<tr><?php menu_desplegable("serv");?></tr>
			<tr>
				<td colspan="3" class="cinta">
					<a href="" class="mgbt"><img src="imagenes/add2.png"/></a>
					<a onclick="guardar()" class="mgbt"><img src="imagenes/guarda.png" title="Guardar"/></a>
					<a href="" class="mgbt"><img src="imagenes/busca.png" title="Buscar"/></a>
					<a onclick="mypop=window.open('plan-agenda.php','','');mypop.focus()" class="mgbt"><img src="imagenes/agenda1.png" title="Agenda" /></a>
					<a onclick="mypop=window.open('serv-principal.php','','');mypop.focus();" class="mgbt"><img src="imagenes/nv.png" title="Nueva ventana"></a>
					<a onclick="mypop=window.open('<?php echo $url2; ?>','','');mypop.focus();" class="mgbt"><img src="imagenes/duplicar_pantalla.png" title="Duplicar pesta&ntilde;a"></a>
					<img src='imagenes/iratras.png' title="Atrás" onclick="atras();" class='mgbt'>
				</td>
			</tr>
		</table>
		<div id="bgventanamodalm" class="bgventanamodalm">
			<div id="ventanamodalm" class="ventanamodalm">
				<IFRAME src="" name="ventanam" marginWidth=0 marginHeight=0 frameBorder=0 id="ventanam" frameSpacing=0 style=" width:700px; height:130px; top:200; overflow:hidden;"></IFRAME>
			</div>
		</div>
		<form name="form2" method="post" action="">

			<?php 
				if(@ $_POST['oculto'] == "")
				{
					$_POST['onoffswitch'] = "N";
					$_POST['codigo'] = selconsecutivo('srvcortes','numero_corte');
					
				}
			?>
            
			<table class="inicio grande">
				<tr>
					<td class="titulos" colspan="6">.: Generar Facturaci&oacute;n</td>
					<td class="cerrar" style="width:10%" onClick="location.href='serv-principal.php'">Cerrar</td>
				</tr>

                <tr>
                    <td class="tamano01" style="width:3cm;text-align:center;">Corte:</td>
					<td style="width:15%;">
						<input type="text" name="codigo" id="codigo" onKeyUp="return tabular(event,this)" value="<?php echo @ $_POST['codigo'];?>" style="width:98%;height:30px;text-align:center" readonly/>
					</td>

                    <td class="tamano01" style="width:3cm;">Servicios a Facturar</td>
                    <td style="width:20%">
                        <select name="servicio" id="servicio" style="width:98%;height:30px;" class="centrarSelect">
                            <option class="aumentarTamaño" value="TODOS">TODOS LOS SERVICIOS</option>
						</select>
                    </td>
                </tr>

                <tr>
                    <td class="tamano01" style="width:3cm;">Fecha Inicial de Corte:</td>
					<td style="width:15%;">
                        <input type="text" name="fecha_inicial" id="fecha_inicial" onchange="" value="<?php echo @ $_POST['fecha_inicial']?>" maxlength="10" onKeyUp="return tabular(event,this)"  id="fc_1198971545" onKeyDown="mascara(this,'/',patron,true)" title="DD/MM/YYYY" style="height:30px;text-align:center;width:85%;" readonly/>
                            <a href="#" onClick="displayCalendarFor('fecha_inicial');" title="Calendario">
                                <img src="imagenes/calendario04.png" style="width:25px;">
                            </a>
                    </td>

                    <td class="tamano01" style="width: 15%;">Fecha Final de Corte:</td>
					<td>
                        <input type="text" name="fecha_final" id="fecha_final" onchange="" value="<?php echo @ $_POST['fecha_final']?>" maxlength="10" onKeyUp="return tabular(event,this)"  id="fc_1198971545" onKeyDown="mascara(this,'/',patron,true)" title="DD/MM/YYYY" style="height:30px;text-align:center;width:85%;" readonly/>
                            <a href="#" onClick="displayCalendarFor('fecha_final');" title="Calendario">
                                <img src="imagenes/calendario04.png" style="width:25px;">
                            </a>
                    </td>
                </tr>
                <tr>
                    <td class="tamano01" style="width:10%;">Fecha Limite de Pago:</td>
					<td>
                        <input type="text" name="fecha_limite" id="fecha_limite" onchange="" value="<?php echo @ $_POST['fecha_limite']?>" maxlength="10" onKeyUp="return tabular(event,this)"  id="fc_1198971545" onKeyDown="mascara(this,'/',patron,true)" title="DD/MM/YYYY" style="height:30px;text-align:center;width:85%;" readonly/>
                            <a href="#" onClick="displayCalendarFor('fecha_limite');" title="Calendario">
                                <img src="imagenes/calendario04.png" style="width:25px;">
                            </a>
                    </td>
					<td class="tamano01" style="width:15%;">Fecha de Impresi&oacute;n:</td>
					<td>
                        <input type="text" name="fecha_impresion" id="fecha_impresion" onchange="" value="<?php echo @ $_POST['fecha_impresion']?>" maxlength="10" onKeyUp="return tabular(event,this)"  id="fc_1198971545" onKeyDown="mascara(this,'/',patron,true)" title="DD/MM/YYYY" style="height:30px;text-align:center;width:85%;" readonly/>
                            <a href="#" onClick="displayCalendarFor('fecha_impresion');" title="Calendario">
                                <img src="imagenes/calendario04.png" style="width:25px;">
                            </a>
                    </td>

                </tr>
				<tr>
					<td class="tamano01" style="width:15%;">Fecha tentativa de corte:</td>
					<td>
                        <input type="text" name="fecha_corte" id="fecha_corte" onchange="" value="<?php echo @ $_POST['fecha_corte']?>" maxlength="10" onKeyUp="return tabular(event,this)"  id="fc_1198971545" onKeyDown="mascara(this,'/',patron,true)" title="DD/MM/YYYY" style="height:30px;text-align:center;width:85%;" readonly/>
                            <a href="#" onClick="displayCalendarFor('fecha_corte');" title="Calendario">
                                <img src="imagenes/calendario04.png" style="width:25px;">
                            </a>
                    </td>
				</tr>
				<tr>
					<td  class="tamano01">Tipo de Movimiento</td>
                    <td colspan="1">
                        <select  name="tipoMovimiento" id="tipoMovimiento" style="width:100%;height:30px;" class="centrarSelect">
							<?php
								$sqlr="SELECT * FROM srvtipo_movimiento WHERE codigo = '01' AND tipo_movimiento = '1' ";
								$resp = mysqli_query($linkbd,$sqlr);
								while ($row =mysqli_fetch_row($resp))
								{
									if(@ $_POST['tipoMovimiento']==$row[0])
									{
										echo "<option class='aumentarTamaño' value='$row[1]$row[0]' SELECTED>$row[1]$row[0] - $row[2]</option>";
									}
									else{echo "<option class='aumentarTamaño' value='$row[1]$row[0]'>$row[1]$row[0] - $row[2]</option>";}
								}
							?>
						</select>
                    </td>

					<td>
					    <input type="button" name="agregar" id="agregar" style="height:30px;" value="Generar Facturaci&oacute;n" onClick="generarFacturacion();" >
                    </td>

					<?php
					echo"
						<td>
							<div id='titulog1' style='display:none; float:left'></div>
							<div id='progreso' class='ProgressBar' style='display:none; float:left'>
								<div class='ProgressBarText'><span id='getprogress'></span>&nbsp;% </div>
								<div id='getProgressBarFill'></div>
							</div>
						</td>";
					?>
				</tr>
			</table>
        
			<div id="cargando" style=" position:absolute;left: 46%; bottom: 45%">
				<img src="imagenes/loading.gif" style=" width: 80px; height: 80px"/>
			</div>

			<input type="hidden" name="oculto" id="oculto" value="1"/>
            
			<?php 

				//variables mostrar factura -- se guardan en una tabla que se usa solo para mostrar la factura ya que una factura una vez emitida no puede ser cambiada.
				$cliente = array();
				$num_factura = array();
				$codUsuario = array();
				$idRuta = array();
				$nombre_suscriptor = array();
				$documento = array();
				$servicio = array();
				$cargo_f = array();
				$consumo_b = array();
				$consumo_c = array();
				$consumo_s = array();
				$subsidio_cf = array();
				$subsidio_cb = array();
				$subsidio_cc = array();
				$subsidio_cs = array();
				$contribucion_cf = array();
				$contribucion_cb = array();
				$contribucion_cc = array();
				$contribucion_cs = array();
				$deuda_anterior = array();
				$acuerdo_pago = array();
				$venta_medidor = array();
				$interes_mora = array();
				$alumbrado = array();
				$abonos = array();

				$corte = $_POST['codigo'];
				$id_cliente = array();
				$numero_facturacion = array();
				$tipoMovimiento = array();
				$fechaMovimiento = array();
				$id_servicio = array();
				$id_tipo_cobro = array();
				$credito = array();
				$debito = array();
				$saldo = array();
				$año = date('Y');
				//srvcortes_detalles
				$id_cliente2 = array();
				$id_corte = array();
				$numero_facturacion2 = array();
				$estado_pago = array();

				//Variables publicas
				$valortotalLectura = 0;
				$cargoFijo = 0;
				$porcentaje = 0;
				$totalSubsidio = 0;
				$contFacturacion = 0;

				//id's de otros cobros, subsidios, exoneraciones y contribución
				$idOtrosCobros    = array();
				$idSubsidios      = array();
				$idExoneraciones  = array();
				$idContribuciones = array();
				

                if(@$_POST['oculto'] == "3")
				{	
					if (@$_POST['onoffswitch'] != 'S')
					{
						$cobro_mora = 'N';
					}
					else 
					{
						$cobro_mora = 'S';
					}

					$validacionCorteActivo = consultaCortesActivos();

					$sqlParametros = "SELECT rango_consumo, aplica_interes FROM srvparametros WHERE id = 1";
					$resParametros = mysqli_query($linkbd,$sqlParametros);
					$rowParametros = mysqli_fetch_row($resParametros);

					$rangoConsumo = $rowParametros[0];
					$aplicaInteres = $rowParametros[1];

					if($validacionCorteActivo == false)
					{
						$sql = "SELECT MAX(version) FROM srvcargo_fijo";
						$row = mysqli_fetch_row(mysqli_query($linkbd, $sql));
						
						$versionTarifas = $row[0];

						$numeroTipoMovimiento = $_POST['tipoMovimiento'];
						$contFacturacion = selconsecutivo('srvcortes_detalle','numero_facturacion');

						$sqlMAXCorte = "SELECT MAX(numero_facturacion) FROM srvcortes_detalle";
						$respMAXCorte = mysqli_query($linkbd,$sqlMAXCorte);
						$rowMAXCorte = mysqli_fetch_row($respMAXCorte);

						$facturaInicial = $rowMAXCorte[0] + 1;

						$sqlCliente = "SELECT id, id_tercero, id_estrato, zona_uso, cod_usuario, id_ruta FROM srvclientes WHERE estado = 'S' ORDER BY id_ruta ASC, codigo_ruta ASC";
						$respCliente = mysqli_query($linkbd,$sqlCliente);
						
						$totalClientes = mysqli_num_rows($respCliente); //total Clientes
						$c = 0;
						$ind = 0;

						while($rowCliente = mysqli_fetch_row($respCliente))
						{		
							//Validaciones	
							$query = "SELECT * FROM srvasignacion_servicio WHERE id_clientes = $rowCliente[0] AND (cargo_fijo = 'S' OR consumo = 'S')";
							$resp = mysqli_query($linkbd,$query);
							$validaServicio = mysqli_num_rows($resp);

							$sqlValidacion02 = "SELECT estado_pago FROM srvcortes_detalle WHERE id_cliente = $rowCliente[0] AND id_corte = $corte-1";
							$rowValidacion02 = mysqli_fetch_assoc(mysqli_query($linkbd, $sqlValidacion02));

							$c ++;	//contador;

							if (($validaServicio > 0) || ($validaServicio == 0 && $rowValidacion02['estado_pago'] == 'V')) 
							{
								//srvcortes_detalles

								$id_cliente2[] 		  = $rowCliente[0];
								$id_corte[] 		  = $_POST['codigo'];
								$numero_facturacion2[] = $contFacturacion;
								$estado_pago[]		  = 'S';

								$sqlServicios = "SELECT id, tarifa_medidor FROM srvservicios ORDER BY id";
								$resServicios = mysqli_query($linkbd,$sqlServicios);
								while($rowServicios = mysqli_fetch_row($resServicios))
								{
									$sqlAsignacionServicio = "SELECT id_servicio, tarifa_medidor, cargo_fijo, consumo, estado, id_estrato FROM srvasignacion_servicio WHERE id_clientes = '$rowCliente[0]' AND id_servicio = $rowServicios[0]";
									$respAsignacionServicio = mysqli_query($linkbd,$sqlAsignacionServicio);
									$rowAsignacionServicio = mysqli_fetch_row($respAsignacionServicio);
								
									$novedadCF = 'S';
									$novedadCS = 'S';

									$sqlNovedades = "SELECT codigo_observacion FROM srv_asigna_novedades WHERE id_cliente = $rowCliente[0] AND corte = $corte AND estado = 'S'";
									$rowNovedades = mysqli_fetch_assoc(mysqli_query($linkbd, $sqlNovedades));

									if ($rowNovedades['codigo_observacion'] != '') {

										$sqlNovedadesCab = "SELECT afecta_facturacion FROM srv_tipo_observacion WHERE codigo_observacion = '$rowNovedades[codigo_observacion]' AND estado = 'S'";
										$rowNovedadesCab = mysqli_fetch_assoc(mysqli_query($linkbd, $sqlNovedadesCab));

										if ($rowNovedadesCab['afecta_facturacion'] == 'S') {

											$sqlNovedadesDet = "SELECT cargo_fijo, consumo FROM srv_observacion_det WHERE codigo_observacion = '$rowNovedades[codigo_observacion]' AND id_servicio = $rowServicios[0] AND estado = 'S'";
											$rowNovedadesDet = mysqli_fetch_assoc(mysqli_query($linkbd, $sqlNovedadesDet));

											$novedadCF = $rowNovedadesDet['cargo_fijo'];
											$novedadCS = $rowNovedadesDet['consumo'];
										}
									}

									//datos basicos factura
									$cliente[$ind] = $rowCliente[0];
									$codUsuario[$ind] = $rowCliente[4];
									$idRuta[$ind] = $rowCliente[5];
									$num_factura[$ind] = $contFacturacion;

									// $sqlTercero = "SELECT nombre1, nombre2, apellido1, apellido2, razonsocial, cedulanit FROM terceros WHERE id_tercero = '$rowCliente[1]' ";
									// $resTercero = mysqli_query($linkbd,$sqlTercero);
									// $rowTercero = mysqli_fetch_row($resTercero);

									// if ($rowTercero != '')
									// {
									// 	if($rowTercero[4] == '')
									// 	{
									// 		$nombreCompleto = $rowTercero[0]. ' ' .$rowTercero[1]. ' ' .$rowTercero[2]. ' ' .$rowTercero[3];
									// 	}
									// 	else
									// 	{
									// 		$nombreCompleto = $rowTercero[4];
									// 	}

									// 	$nombreCompleto = Quitar_Espacios($nombreCompleto);
									// 	$cedula = $rowTercero[5];
									// }
									// else
									// {
										$nombreCompleto = "N/N";
										$cedula = "N/N";
									// }

									$nombre_suscriptor[$ind] = $nombreCompleto;
									$documento[$ind] = $cedula;
									$servicio[$ind] = $rowServicios[0];

									if($rowAsignacionServicio[0] != '')
									{
										if($corte == 1)//cargar saldos iniciales al kardex
										{
											$saldoInicial = 0;

											$query = "SELECT id, saldo FROM srvsaldos_iniciales WHERE id_cliente = $rowCliente[0] AND id_servicio = '$rowAsignacionServicio[0]' AND estado = 'S'";
											$res = mysqli_query($linkbd,$query);
											while($row = mysqli_fetch_row($res)) {

												$saldoInicial += $row[1];

												$query = "UPDATE srvsaldos_iniciales SET estado = 'N' WHERE id = $row[0]";
												mysqli_query($linkbd,$query);
											}
										}

										if ($rowAsignacionServicio[4] == 'S') 
										{
											//Cargo fijo de cada servicio
											if ($rowAsignacionServicio[2] == 'S')
											{
												if ($novedadCF == 'S') {

													$subsidioCargoFijo = 0;
													$contribucionCargoFijo = 0;

													$sqlCargoFijo = "SELECT costo_unidad, subsidio, contribucion FROM srvcargo_fijo WHERE id_servicio = '$rowAsignacionServicio[0]' AND id_estrato = '$rowAsignacionServicio[5]' AND version = $versionTarifas AND estado = 'S'";
													$resCargoFijo = mysqli_query($linkbd,$sqlCargoFijo);
													$rowCargoFijo = mysqli_fetch_row($resCargoFijo);

													if ($rowCargoFijo[1] > 0 || $rowCargoFijo[2] > 0)
													{
														$subsidioCargoFijo = $rowCargoFijo[1] / 100;
														$subsidioCargoFijo = $rowCargoFijo[0] * $subsidioCargoFijo;

														$contribucionCargoFijo = $rowCargoFijo[2] / 100;
														$contribucionCargoFijo = $rowCargoFijo[0] * $contribucionCargoFijo;
													}

													if ($rowCargoFijo[1] == 0 && $rowCargoFijo[2] == 0)
													{
														$subsidioCargoFijo = 0;
														$contribucionCargoFijo = 0;
													}

													$cargo_f[$ind] = $rowCargoFijo[0];
													$subsidio_cf[$ind] = $subsidioCargoFijo;
													$contribucion_cf[$ind] = $contribucionCargoFijo;
													
													$id_cliente[] 		  = $rowCliente[0];
													$numero_facturacion[] = $contFacturacion;
													$tipoMovimiento[]	  = $_POST['tipoMovimiento'];
													$id_servicio[] 		  = $rowAsignacionServicio[0];	
													$id_tipo_cobro[]	  = 1;
													$credito[]		   	  = $rowCargoFijo[0];
													$debito[]		   	  = 0;
													$saldo[] 			  = 0;
												}
												else {

													$cargo_f[$ind] = 0;
													$subsidio_cf[$ind] = 0;
													$contribucion_cf[$ind] = 0;
												}
											}	
											else
											{
												$cargo_f[$ind] = 0;
												$subsidio_cf[$ind] = 0;
												$contribucion_cf[$ind] = 0;
											}
										
											//Tarifa con medidor
											if ($rowServicios[1] == 'S') 
											{

												if ($rowAsignacionServicio[3] == 'S')
												{
													if ($novedadCS == 'S') {

														if ($rangoConsumo == 'multiple')
														{
															$band = 0;
															$totalValorConsumo = 0;
															$consumo = array();
															$subsidio = array();
															$contribucion = array();
															$tarifa1 = 0;
															$totalSubsidioConsumo = 0;
															$totalContribucionConsumo = 0;			
					
															$sqlLectura = "SELECT consumo FROM srvlectura WHERE corte = $corte AND id_cliente = '$rowCliente[0]' AND id_servicio = '$rowAsignacionServicio[0]'";
															$respLectura = mysqli_query($linkbd,$sqlLectura);
															$rowLectura = mysqli_fetch_row($respLectura);
					
															$sqlTarifas = "SELECT rango_final, costo_unidad, subsidio, contribucion FROM srvtarifas WHERE id_servicio = '$rowAsignacionServicio[0]' AND id_estrato = '$rowAsignacionServicio[5]' AND version = $versionTarifas AND estado = 'S'";
															$resTarifas = mysqli_query($linkbd,$sqlTarifas);
															while ($rowTarifas = mysqli_fetch_row($resTarifas))
															{	
																if($band == 0)
																{
																	$consumoRestante = $rowTarifas[0] - $rowLectura[0];
																}
																else
																{
					
																	$consumoRestante = $rowTarifas[0] - $rowLectura[0];
																}									
																
																$totalUnitario = 0;
					
																if ($consumoRestante < 0)
																{
																	$totalUnitario = ($rowTarifas[0]-$tarifa1) * $rowTarifas[1];
																	$totalValorConsumo += ($rowTarifas[0]-$tarifa1) * $rowTarifas[1];
																	$consumo[] = ($rowTarifas[0]-$tarifa1) * $rowTarifas[1];
																	$tarifa1 = $rowTarifas[0];
					
																	if ($rowTarifas[2] > 0)
																	{
																		$porcentajeSubsidioConsumo = $rowTarifas[2] / 100;
																		$totalSubsidioConsumo += $totalUnitario * $porcentajeSubsidioConsumo;
																		$subsidio[] = $totalUnitario * $porcentajeSubsidioConsumo;
																		$contribucion[] = 0;
																	}
					
																	if ($rowTarifas[3] > 0)
																	{
																		$porcentajeContribucionConsumo = $rowTarifas[3] / 100;
																		$totalContribucionConsumo = $totalUnitario * $porcentajeContribucionConsumo;
																		$contribucion[] = $totalContribucionConsumo;
																		$subsidio[] = 0;
																	}
																	
																	if ($rowTarifas[2] == 0 &&  $rowTarifas[3] == 0)
																	{
																		$subsidio[] = 0;
																		$contribucion[] = 0;
																	}

																	$band = 1;
																}
																else
																{
																	$totalUnitario = ($rowLectura[0]-$tarifa1) * $rowTarifas[1];
																	$totalValorConsumo += ($rowLectura[0]-$tarifa1) * $rowTarifas[1];
																	$consumo[] = ($rowLectura[0]-$tarifa1) * $rowTarifas[1];
					
																	if ($rowTarifas[2] > 0)
																	{
																		$porcentajeSubsidioConsumo = $rowTarifas[2] / 100;
																		$totalSubsidioConsumo += $totalUnitario * $porcentajeSubsidioConsumo;
																		$subsidio[] = $totalUnitario * $porcentajeSubsidioConsumo;
																		$contribucion[] = 0;
																	}
																	
																	if ($rowTarifas[3] > 0)
																	{
																		$porcentajeContribucionConsumo = $rowTarifas[3] / 100;
																		$totalContribucionConsumo = $totalUnitario * $porcentajeContribucionConsumo;
																		$contribucion[] = $totalContribucionConsumo;
																		$subsidio[] = 0;
																	}
					
																	break;
																}			
															}

															if($consumo[0] == '')
															{
																$consumo[0] = 0;
																$consumo[1] = 0;
																$consumo[2] = 0;

																$subsidio[0] = 0;
																$subsidio[1] = 0;
																$subsidio[2] = 0;

																$contribucion[0] = 0;
																$contribucion[1] = 0;
																$contribucion[2] = 0;
															}
															
															if($consumo[1] == '')
															{
																$consumo[1] = 0;
																$consumo[2] = 0;

																$subsidio[1] = 0;
																$subsidio[2] = 0;

																$contribucion[1] = 0;
																$contribucion[2] = 0;
															}

															if($consumo[2] == '')
															{
																$consumo[2] = 0;
																$subsidio[2] = 0;
																$contribucion[2] = 0;
															}

															$consumo_b[$ind] = $consumo[0];
															$consumo_c[$ind] = $consumo[1];
															$consumo_s[$ind] = $consumo[2];
															$subsidio_cb[$ind] = $subsidio[0];
															$subsidio_cc[$ind] = $subsidio[1];
															$subsidio_cs[$ind] = $subsidio[2];
															$contribucion_cb[$ind] = $contribucion[0];
															$contribucion_cc[$ind] = $contribucion[1];
															$contribucion_cs[$ind] = $contribucion[2];

															if($totalValorConsumo >  0)
															{
																$id_cliente[] 		  = $rowCliente[0];
																$numero_facturacion[] = $contFacturacion;
																$tipoMovimiento[] 	  = $_POST['tipoMovimiento'];
																$id_servicio[] 		  = $rowAsignacionServicio[0];	
																$id_tipo_cobro[]	  = 2;
																$credito[]		   	  = $totalValorConsumo;
																$debito[]		   	  = 0;
																$saldo[]			  = 0;	
															}
														}
													}
													else {
														$consumo_b[$ind] = 0;
														$consumo_c[$ind] = 0;
														$consumo_s[$ind] = 0;
														$subsidio_cb[$ind] = 0;
														$subsidio_cc[$ind] = 0;
														$subsidio_cs[$ind] = 0;
														$contribucion_cb[$ind] = 0;
														$contribucion_cc[$ind] = 0;
														$contribucion_cs[$ind] = 0;
														$alumbrado[$ind] = 0;
													}
												}
												else
												{
													$consumo_b[$ind] = 0;
													$consumo_c[$ind] = 0;
													$consumo_s[$ind] = 0;
													$subsidio_cb[$ind] = 0;
													$subsidio_cc[$ind] = 0;
													$subsidio_cs[$ind] = 0;
													$contribucion_cb[$ind] = 0;
													$contribucion_cc[$ind] = 0;
													$contribucion_cs[$ind] = 0;
													$alumbrado[$ind] = 0;
												}	
											}
											else 
											{

												if ($rowAsignacionServicio[3] == 'S') 
												{
													if ($novedadCS == 'S') {

														$totalValorConsumo = 0;
														$totalSubsidioConsumo = 0;
														$totalContribucionConsumo = 0;

														$sqlCostoEstandar = "SELECT costo_unidad, subsidio, contribucion FROM srvcostos_estandar WHERE id_servicio = '$rowAsignacionServicio[0]' AND id_estrato = '$rowAsignacionServicio[5]' AND version = $versionTarifas";
														$resCostoEstandar = mysqli_query($linkbd,$sqlCostoEstandar);
														$rowCostoEstandar = mysqli_fetch_row($resCostoEstandar);

														if($rowCostoEstandar[0] != '') {

															
															$totalValorConsumo = $rowCostoEstandar[0];

															if ($rowCostoEstandar[1] > 0) {

																$subsidio = $rowCostoEstandar[1] / 100;
																$totalSubsidioConsumo = $totalValorConsumo * $subsidio;
															}

															if($rowCostoEstandar[2] > 0) {

																$contribucion = $rowCostoEstandar[2] / 100;
																$totalContribucionConsumo = $totalValorConsumo * $contribucion;
															}
															
															$consumo_b[$ind] = $totalValorConsumo;
															$consumo_c[$ind] = 0;
															$consumo_s[$ind] = 0;
															$subsidio_cb[$ind] = $totalSubsidioConsumo;
															$subsidio_cc[$ind] = 0;
															$subsidio_cs[$ind] = 0;
															$contribucion_cb[$ind] = $totalContribucionConsumo;
															$contribucion_cc[$ind] = 0;
															$contribucion_cs[$ind] = 0;
															$alumbrado[$ind] = 0;

															$id_cliente[] 		  = $rowCliente[0];
															$numero_facturacion[] = $contFacturacion;
															$tipoMovimiento[] 	  = $_POST['tipoMovimiento'];
															$id_servicio[] 		  = $rowAsignacionServicio[0];	
															$id_tipo_cobro[]	  = 2;
															$credito[]		   	  = $totalValorConsumo;
															$debito[]		   	  = 0;
															$saldo[]			  = 0;
														}
													}
													else {
														$consumo_b[$ind] = 0;
														$consumo_c[$ind] = 0;
														$consumo_s[$ind] = 0;
														$subsidio_cb[$ind] = 0;
														$subsidio_cc[$ind] = 0;
														$subsidio_cs[$ind] = 0;
														$contribucion_cb[$ind] = 0;
														$contribucion_cc[$ind] = 0;
														$contribucion_cs[$ind] = 0;
														$alumbrado[$ind] = 0;
													}
													
												}
												else
												{
													$consumo_b[$ind] = 0;
													$consumo_c[$ind] = 0;
													$consumo_s[$ind] = 0;
													$subsidio_cb[$ind] = 0;
													$subsidio_cc[$ind] = 0;
													$subsidio_cs[$ind] = 0;
													$contribucion_cb[$ind] = 0;
													$contribucion_cc[$ind] = 0;
													$contribucion_cs[$ind] = 0;
													$alumbrado[$ind] = 0;
												}
											}
																				

											//Otros Cobros
											$venta_medidor[$ind] = 0;
											// $id_cliente[]  		  = $rowCliente[0];
											// $numero_facturacion[] = $contFacturacion;
											// $tipoMovimiento[]  	  = $_POST['tipoMovimiento'];
											// $id_servicio[] 		  = $rowAsignacionServicio[0];	
											// $id_tipo_cobro[]	  = 4;
											// $credito[]		   	  = $rowAsignacionOtroCobro[3];
											// $debito[]		   	  = 0;
											// $saldo[]			  = 0;

											//Abonos
											$abonos[$ind] = 0;
														

											//Subsidio cargo fijo
											if ($subsidioCargoFijo > 0)
											{
												$id_cliente[] 		  = $rowCliente[0];
												$numero_facturacion[] = $contFacturacion;
												$tipoMovimiento[] 	  = $_POST['tipoMovimiento'];
												$id_servicio[] 		  = $rowAsignacionServicio[0];	
												$id_tipo_cobro[]	  = 3;
												$credito[]		   	  = 0;
												$debito[]		      = $subsidioCargoFijo;
												$saldo[]			  = 0;

												$subsidioCargoFijo = 0;
											}
											
											

											//Subsidio consumo
											if ($totalSubsidioConsumo > 0) 
											{
												$id_cliente[] 		  = $rowCliente[0];
												$numero_facturacion[] = $contFacturacion;
												$tipoMovimiento[] 	  = $_POST['tipoMovimiento'];
												$id_servicio[] 		  = $rowAsignacionServicio[0];	
												$id_tipo_cobro[]	  = 4;
												$credito[]		   	  = 0;
												$debito[]		      = $totalSubsidioConsumo;
												$saldo[]			  = 0;

												$totalSubsidioConsumo = 0;
											}
											

											//Contribuciones 
											if ($contribucionCargoFijo != 0 || $totalContribucionConsumo != 0) {

												$totalContribucion = $contribucionCargoFijo + $totalContribucionConsumo;

												$id_cliente[] 		  = $rowCliente[0];
												$numero_facturacion[] = $contFacturacion;
												$tipoMovimiento[] 	  = $_POST['tipoMovimiento'];
												$id_servicio[] 		  = $rowAsignacionServicio[0];	
												$id_tipo_cobro[]	  = 5;
												$credito[]		   	  = $totalContribucion;
												$debito[]		   	  = 0;
												$saldo[]			  = 0;	

												$contribucionCargoFijo = 0;
												$totalContribucionConsumo = 0;
												$totalContribucion = 0;
											}
										}

										

										//Consultar estado de factura a anterior cliente
										
										$saldoAnterior = 0;
										$corteAnterior = $corte - 1;
										$facturaAnt = '';

										$sqlConsultaFacturaAnterior = "SELECT estado_pago, numero_facturacion FROM srvcortes_detalle WHERE id_cliente = $rowCliente[0] AND id_corte = $corteAnterior";
										$resConsultaFacturaAnterior = mysqli_query($linkbd,$sqlConsultaFacturaAnterior);
										$rowConsultaFacturaAnterior = mysqli_fetch_row($resConsultaFacturaAnterior);
									
										$facturaAnt = $rowConsultaFacturaAnterior[1];

										switch ($rowConsultaFacturaAnterior[0]) 
										{
											case 'V':
												$saldoAnterior = consultaValorServicio($facturaAnt, $rowAsignacionServicio[0]);
											break;

											default:
												$saldoAnterior = 0;
											break;
										}

										$deuda = 0;
										$deuda = $saldoAnterior + $saldoInicial;

										if($deuda > 0)
										{
											$deuda                = round($deuda, 2);
											$id_cliente[] 		  = $rowCliente[0];
											$numero_facturacion[] = $contFacturacion;
											$tipoMovimiento[] 	  = $_POST['tipoMovimiento'];
											$id_servicio[] 		  = $rowAsignacionServicio[0];	
											$id_tipo_cobro[]	  = 8;
											$credito[]		   	  = $deuda;
											$debito[]		   	  = 0;

											$deuda_anterior[$ind] = $deuda;
										}

										

										//Interes Moratorio
										if($aplicaInteres == 'S' && $deuda > 0) 
										{
											$sqlCobroMora = "SELECT porcentaje FROM srvparametros WHERE id = 1";
											$resCobroMora = mysqli_query($linkbd,$sqlCobroMora);
											$rowCobroMora = mysqli_fetch_row($resCobroMora);
											$interes = 0;
											$valorInteres = 0;
											$porcentaje = 0;

											$porcentaje = $rowCobroMora[0];

											if ($porcentaje >= 1) {
												$interes = $porcentaje / 100;
											}
											else {
												$interes = $porcentaje / 10;
											}

											$valorInteres = $deuda * $interes;
											$interesAnterior = consultaValorInteres($facturaAnt, $rowAsignacionServicio[0]);
											$valorInteres = $valorInteres + $interesAnterior;

											if($valorInteres > 0)
											{
												$valorInteres         = round($valorInteres, 2);
												$id_cliente[] 		  = $rowCliente[0];
												$numero_facturacion[] = $contFacturacion;
												$tipoMovimiento[] 	  = $_POST['tipoMovimiento'];
												$id_servicio[] 		  = $rowAsignacionServicio[0];	
												$id_tipo_cobro[]	  = 10;
												$credito[]		   	  = $valorInteres;
												$debito[]		   	  = 0;
												$saldo[]			  = 0;
												
												$interes_mora[$ind] = $valorInteres;
											}
										}
										else
										{
											$interes_mora[$ind] = 0;
										}
										
										//Consulta si el cliente tiene acuerdos de pagos activos
										$sqlAcuerdoPago = "SELECT codigo_acuerdo FROM srv_acuerdo_cab WHERE id_cliente = '$rowCliente[0]' AND estado_acuerdo = 'Activo'";
										$resAcuerdoPago = mysqli_query($linkbd,$sqlAcuerdoPago);
										$rowAcuerdoPago = mysqli_fetch_row($resAcuerdoPago);

										if($rowAcuerdoPago[0] != '')
										{
											$sqlDetalleCuotaServicio = "SELECT valor_cuota_servicio, id_servicio FROM srv_acuerdo_det WHERE codigo_acuerdo = $rowAcuerdoPago[0] AND id_servicio = $rowAsignacionServicio[0]";
											$rowDetalleCuotaServicio = mysqli_fetch_row(mysqli_query($linkbd, $sqlDetalleCuotaServicio));

											if($rowDetalleCuotaServicio[1] != '')
											{
												$id_cliente[] 		  = $rowCliente[0];
												$numero_facturacion[] = $contFacturacion;
												$tipoMovimiento[] 	  = $_POST['tipoMovimiento'];
												$id_servicio[] 		  = $rowDetalleCuotaServicio[1];	
												$id_tipo_cobro[]	  = 7;
												$credito[]		   	  = $rowDetalleCuotaServicio[0];
												$debito[]		   	  = 0;
												$saldo[]			  = 0;
	
												$acuerdo_pago[$ind] = $rowDetalleCuotaServicio[0];
											}	 
										}			
										else
										{
											$acuerdo_pago[$ind] = 0;
										}
									}
									else
									{
										$cargo_f[$ind] = 0;
										$consumo_b[$ind] = 0;
										$consumo_c[$ind] = 0;
										$consumo_s[$ind] = 0;
										$subsidio_cf[$ind] = 0;
										$subsidio_cb[$ind] = 0;
										$subsidio_cc[$ind] = 0;
										$subsidio_cs[$ind] = 0;
										$contribucion_cf[$ind] = 0;
										$contribucion_cb[$ind] = 0;
										$contribucion_cc[$ind] = 0;
										$contribucion_cs[$ind] = 0;
										$deuda_anterior[$ind] = 0;
										$acuerdo_pago[$ind] = 0;
										$venta_medidor[$ind] = 0;
										$interes_mora[$ind] = 0;
										$alumbrado[$ind] = 0;
										$abonos[$ind] = 0;
									}

									$ind++;			
								}		

								$contFacturacion++;	
							}

							$porcentaje = $c * 100 / $totalClientes;

							echo"
							<script>
								progres='".round($porcentaje)."';callprogress(progres);
								
							</script>";
							flush();
							ob_flush();
							usleep(5);

							$enumeradorFacturas[] = $facturaInicial;
							$facturaInicial++;
						}
						
						preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST['fecha_inicial'],$f);
						$fechaInicial="$f[3]-$f[2]-$f[1]";

						preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST['fecha_final'],$f);
						$fechaFinal="$f[3]-$f[2]-$f[1]";

						preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST['fecha_limite'],$f);
						$fechaLimite="$f[3]-$f[2]-$f[1]";

						preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST['fecha_corte'],$f);
						$fechaCorte="$f[3]-$f[2]-$f[1]";

						preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST['fecha_impresion'],$f);
						$fechaImpresion="$f[3]-$f[2]-$f[1]";
						$vigencia = $f[3];

						$sqlCorte = "INSERT INTO srvcortes (numero_corte, fecha_inicial, fecha_final, fecha_limite_pago, fecha_impresion, servicio, estado, fecha_corte, version_tarifa) VALUES ('".$_POST['codigo']."', '$fechaInicial', '$fechaFinal', '$fechaLimite', '$fechaImpresion', '".$_POST['servicio']."', 'S', '$fechaCorte', $versionTarifas)";
						mysqli_query($linkbd,$sqlCorte);
						
                        $contador1 = count($id_cliente2);

                        for($x=0; $x < $contador1; $x++)
						{
							$sqlCortesDetalles = "INSERT INTO srvcortes_detalle (id_cliente, id_corte, numero_facturacion, estado_pago) VALUES ('$id_cliente2[$x]', '$id_corte[$x]', '$numero_facturacion2[$x]', '$estado_pago[$x]') ";
							mysqli_query($linkbd,$sqlCortesDetalles);


							$cuotaActual = 0;
							$valorCuota = 0;
							$valorLiquidado = 0;
							$totalCuotas = 0;

							$sqlAcuerdoPago = "SELECT codigo_acuerdo, numero_cuotas, valor_cuota, cuotas_liquidadas, valor_liquidado FROM srv_acuerdo_cab WHERE id_cliente = '$id_cliente2[$x]' AND estado_acuerdo = 'Activo'";
							$resAcuerdoPago = mysqli_query($linkbd,$sqlAcuerdoPago);
							$rowAcuerdoPago = mysqli_fetch_row($resAcuerdoPago);

							if($rowAcuerdoPago[0] != '')
							{
								$codigoAcuerdo = $rowAcuerdoPago[0];
								$valorCuota = $rowAcuerdoPago[2];
								$cuotaActual = $rowAcuerdoPago[3];
								$cuotaActual = $cuotaActual + 1;
								$valorLiquidado = $rowAcuerdoPago[4] + $valorCuota;
								$totalCuotas = $rowAcuerdoPago[1];
								$estadoAcuerdo = "Activo";

								if($totalCuotas == $cuotaActual)
								{
									$estadoAcuerdo = "Finalizado";
								}

								$query = "UPDATE srv_acuerdo_cab SET cuotas_liquidadas = $cuotaActual, valor_liquidado = $valorLiquidado, estado_acuerdo = '$estadoAcuerdo' WHERE codigo_acuerdo = $codigoAcuerdo";
								mysqli_query($linkbd, $query);
							}
						}
						
                        $contador2 = count($id_cliente);

						for($x=0; $x < $contador2; $x++)
						{
							$sqlDetalles_Facturacion = "INSERT INTO srvdetalles_facturacion (corte, id_cliente, numero_facturacion, tipo_movimiento, fecha_movimiento, id_servicio, id_tipo_cobro, credito, debito, saldo, estado) VALUES ('$corte', '$id_cliente[$x]', '$numero_facturacion[$x]', '$tipoMovimiento[$x]', '$fechaImpresion', '$id_servicio[$x]', '$id_tipo_cobro[$x]', '$credito[$x]', '$debito[$x]', '$saldo[$x]','S') ";
							mysqli_query($linkbd,$sqlDetalles_Facturacion);
						}

						$codigoComprobante = '29';

                        $contador3 = count($cliente);
						for($x = 0; $x < $contador3; $x++)
						{
							$sqlVisualizarFactura = "INSERT INTO srvfacturas (corte, cliente, cod_usuario, id_ruta, num_factura, nombre_suscriptor, documento, id_servicio, cargo_f, consumo_b, consumo_c, consumo_s, subsidio_cf, subsidio_cb, subsidio_cc, subsidio_cs, contribucion_cf, contribucion_cb, contribucion_cc, contribucion_cs, deuda_anterior, abonos, acuerdo_pago, venta_medidor, interes_mora, alumbrado, estado) VALUES ('$corte', '$cliente[$x]', '$codUsuario[$x]', '$idRuta[$x]', '$num_factura[$x]', '$nombre_suscriptor[$x]', '$documento[$x]', '$servicio[$x]', '$cargo_f[$x]', '$consumo_b[$x]', '$consumo_c[$x]', '$consumo_s[$x]', '$subsidio_cf[$x]', '$subsidio_cb[$x]', '$subsidio_cc[$x]', '$subsidio_cs[$x]', '$contribucion_cf[$x]', '$contribucion_cb[$x]', '$contribucion_cc[$x]', '$contribucion_cs[$x]', '$deuda_anterior[$x]', '$abonos[$x]', '$acuerdo_pago[$x]', '$venta_medidor[$x]', '$interes_mora[$x]', '$alumbrado[$x]', 'ACTIVO')";
							//echo $sqlVisualizarFactura."<br>";
							mysqli_query($linkbd, $sqlVisualizarFactura);
						}
					}
					else
					{
						echo"<script>despliegamodalm('visible','2','Hay un corte ACTIVO, no puede generar más cortes.');</script>";
					}
				}

				if(@ $_POST['oculto'] == "2")
				{
	
				}
			?>
			
		</form>
		<div id="bgventanamodal2">
			<div id="ventanamodal2">
				<IFRAME  src="" name="buster" marginWidth=0 marginHeight=0 frameBorder=0 id="ventana2" frameSpacing=0 style="left:500px; width:900px; height:500px; top:200;"></IFRAME>
			</div>
		</div>
	</body>
</html>
