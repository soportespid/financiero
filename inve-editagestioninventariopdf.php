<?php
	require_once("tcpdf/tcpdf_include.php");
	require('comun.inc');
	require "funciones.inc";
	date_default_timezone_set("America/Bogota");
	session_start();
	$val = 0;
	class MYPDF extends TCPDF 
	{
		public function Header() 
		{
			if ($_POST['estado']=='R'){$this->Image('imagenes/reversado02.png',75,41.5,50,15);}
			$linkbd = conectar_v7();
			$linkbd -> set_charset("utf8");
			$sqlr="select *from configbasica where estado='S' ";
			//echo $sqlr;
			$res=mysqli_query($linkbd, $sqlr);
			while($row=mysqli_fetch_row($res))
			{
				$nit=$row[0];
				$rs=$row[1];
				
			}
			
			//Parte Izquierda
			$this->Image('imagenes/escudo.jpg',  13, 13, 20, 20, 'JPG', '', 'T', true, 300, '', false, false, 0, false, false, false);// Logo
			$this->SetFont('helvetica','B',8);
			$this->SetY(10);
			$this->RoundedRect(10, 10, 277, 25, 1,'');
			$this->Cell(0.1);
			$this->Cell(26,25,'','R',0,'L'); 
			$this->SetY(8);
			$this->SetX(80);
			$this->SetFont('helvetica','B',9);
			$this->Cell(160,15,strtoupper("$rs"),0,0,'C'); 
			$this->SetFont('helvetica','B',7);
			$this->SetY(12);
			$this->SetX(80);
			$this->Cell(160,15,'NIT: '.$nit,0,0,'C');
			//*****************************************************************************************************************************
			$this->SetFont('helvetica','B',9);			 
			$this->SetY(23);
			$this->SetX(36);
            if($_POST['tipomov']==1){
                $this->Cell(251,12,"Entrada - Gestión de Inventarios",'T',0,'C');
            }
            else{
                if($_POST['tipomov']==2){
                    $this->Cell(251,12,"Salida - Gestión de Inventarios",'T',0,'C');
                }
                else{
                    if($_POST['tipomov']==3){
                        $this->Cell(251,12,"Reversion de entrada - Gestión de Inventarios",'T',0,'C');
                    }else{
                        if($_POST['tipomov']==4){
                            $this->Cell(251,12,'Reversion de Salida - Gestión de Inventarios'.$_POST['tipomov'],'T',0,'C');
                        }
                    }
                }
            } 
            

            $this->SetFont('helvetica','B',6);
			$this->SetY(10);
			$this->SetX(257);
			$this->Cell(30,7," SOLICITUD: ".$_POST['docum'],"L",0,'L');
			$this->SetY(17);
			$this->SetX(257);
			$this->Cell(35,6," FECHA: ".$_POST['fecha'],"L",0,'L');

			

			/*
			$this->SetFont('helvetica','B',10);
			$this->SetY(15);
			$this->Cell(50.1);
			$this->Cell(149,20,$mov,0,0,'C'); 
			//************************************
			$this->SetFont('helvetica','I',7);
			$this->SetY(27);
			$this->Cell(35);
			//$this->multiCell(110.7,3,''.strtoupper($detallegreso),'T','L');
			$this->multiCell(242,3,'','T','L');
			$this->SetFont('helvetica','B',10);
			$this->SetY(27);
			$this->Cell(237);
			$this->Cell(37.8,14,'','TL',0,'L');
			$this->SetY(27);
			$this->Cell(238);
			$this->Cell(35,5,'NUMERO : '.$_POST['idcomp'],0,0,'L');
			$this->SetY(31);
			$this->Cell(238);
			$this->Cell(35,5,'FECHA: '.$_POST['fecha'],0,0,'L');
			$this->SetY(35);
			$this->Cell(238);
			$this->Cell(35,5,'VIGENCIA: '.$_POST['vigencia'],0,0,'L');
			$this->SetY(27);
			$this->Cell(50.2);
			$this->MultiCell(105.7,4,'',0,'L');		
			*/
			//**********************************************************
			$this->SetFont('times','B',10);
			$this->ln(12);
			//**********************************************************
		}
		public function Footer() 
		{
			$linkbd = conectar_v7();
			$linkbd -> set_charset("utf8");
			$sqlr="SELECT direccion,telefono,web,email FROM configbasica WHERE estado='S'";
			$resp=mysqli_query($linkbd, $sqlr);
			$user = $_SESSION['nickusu'];	
			$fecha = date("Y-m-d H:i:s");
			$ip = $_SERVER['REMOTE_ADDR'];
			$useri = $_POST['user'];
			while($row=mysqli_fetch_row($resp))
			{
				$direcc=strtoupper($row[0]);
				$telefonos=$row[1];
				$dirweb=strtoupper($row[3]);
				$coemail=strtoupper($row[2]);
			}
			if($direcc!=''){$vardirec="Dirección: $direcc, ";}
			else {$vardirec="";}
			if($telefonos!=''){$vartelef="Telefonos: $telefonos";}
			else{$vartelef="";}
			if($dirweb!=''){$varemail="Email: $dirweb, ";}
			else {$varemail="";}
			if($coemail!=''){$varpagiw="Pagina Web: $coemail";}
			else{$varpagiw="";}
			$this->SetFont('helvetica', 'I', 8);
			$txt = <<<EOD
			$vardirec $vartelef
			$varemail $varpagiw
			EOD;
			$this->SetFont('helvetica', 'I', 6);
			$this->Cell(277,10,'','T',0,'T');
			$this->ln(2);
			$this->Write(0, $txt , '', 0, 'C', true, 0, false, false, 0);

            $sqlru="SELECT * FROM almginventario WHERE tipomov = '".$_POST['movr']."' AND tiporeg = '".$_POST['entr']."' AND consec = '".$_POST['numero']."'";
            $respo=mysqli_query($linkbd, $sqlru);
            $rowu=mysqli_fetch_row($respo);
            //echo $rowu[6];

            $sqlrcc="SELECT * FROM `usuarios` WHERE `cc_usu` = '$rowu[6]'";
            $respcc=mysqli_query($linkbd, $sqlrcc);
            $rowcc=mysqli_fetch_row($respcc);
            //echo $rowcc[1];

			$this->Cell(50, 10, 'Hecho por: '.$rowcc[1], 00, false, 'C', 0, '', 0, false, 'T', 'M');
			$this->Cell(70, 10, 'Impreso por: '.$user, 0, false, 'C', 0, '', 0, false, 'T', 'M');
			$this->Cell(30, 10, 'IP: '.$ip, 0, false, 'C',0, '', 0, false, 'T', 'M');
			$this->Cell(80, 10, 'Fecha: '.$fecha, 0, false, 'C', 0, '', 0, false, 'T', 'M');
			$this->Cell(33, 10, 'IDEAL.10 S.A.S', 0, false, 'C', 0, '', 0, false, 'T', 'M');
            $this->Cell(25, 10, 'Pagina '.$this->getAliasNumPage().'/'.$this->getAliasNbPages(), 0, false, 'C', 0, '', 0, false, 'T', 'M');

			
		}
	}
	$pdf = new MYPDF('L','mm','Letter', true, 'iso-8859-1', false);
	$pdf->SetDocInfoUnicode (true); 
	// set document information
	$pdf->SetCreator(PDF_CREATOR);
	$pdf->SetAuthor('IDEALSAS');
	$pdf->SetTitle('Gestión de Inventarios');
	$pdf->SetSubject('Gestión de Inventarios');
	$pdf->SetKeywords('TCPDF, PDF, example, test, guide');
	$pdf->SetMargins(10, 38, 10);// set margins
	$pdf->SetHeaderMargin(38);// set margins
	$pdf->SetFooterMargin(17);// set margins
	$pdf->SetAutoPageBreak(TRUE, 20);// set auto page breaks
	// set some language-dependent strings (optional)
	if (@file_exists(dirname(__FILE__).'/lang/spa.php')) 
	{
		require_once(dirname(__FILE__).'/lang/spa.php');
		$pdf->setLanguageArray($l);
	}	
	$pdf->AddPage();

     
    $pdf->SetFillColor(245,245,245);
	$nombre = $_POST['nombre'];
	$lineas = $pdf->getNumLines($nombre, 211);
	$alturadt=(3*$lineas);
	$pdf->SetFillColor(255,255,255);
	$pdf->SetFont('helvetica','B',7);
	$pdf->cell(17,$alturadt,'Consecutivo:','LTB',0,'L',0);
	$pdf->SetFont('helvetica','',7);
	$pdf->cell(9,$alturadt,''.$_POST['numero'],'TB',0,'C',0);	
	
    $pdf->cell(0.2);
	$pdf->SetFont('helvetica','B',7);
	$pdf->MultiCell(25,$alturadt,'Descripción: ', 'LBT', 'J', 0, 0, '', '', true, 0, false, true, $alturadt, 'M');
	$pdf->SetFont('helvetica','',7);
	$pdf->MultiCell(226,$alturadt,$nombre,'RBT','L',false,1,'','',true,0,false,true,$alturadt,'M',true);

    
	$pdf->ln(2);	
	$y=$pdf->GetY();	
	$pdf->SetY($y);

	$y=$pdf->GetY();	
	$pdf->SetFillColor(222,222,222);
	$pdf->SetFont('helvetica','B',7);
	//$pdf->SetY($y);
    $pdf->Cell(0.1);
    $pdf->Cell(20,5,'Item',0,0,'C',1); 
	$pdf->SetY($y);
	$pdf->Cell(21);
	$pdf->Cell(30,5,'UNSPSC',0,0,'C',1);
	$pdf->SetY($y);
	$pdf->Cell(52);
	$pdf->Cell(30,5,'Codigo Articulo ',0,0,'C',1);
	$pdf->SetY($y);
	$pdf->SetFont('helvetica','B',7);
	$pdf->Cell(83);
	$pdf->Cell(65,5,'Nombre del Articulo',0,0,'C',1);
	$pdf->SetY($y);
	$pdf->Cell(149);
	$pdf->MultiCell(32,5,'Cantidad',0,'C',1,1,'','',true,'',false,true,0,'M',true);
	$pdf->SetY($y);
	$pdf->Cell(182);
    $pdf->MultiCell(29,5,'U. Medida',0,'C',1,1,'','',true,'',false,true,0,'M',true);
	$pdf->SetY($y);
	$pdf->Cell(212);
    $pdf->MultiCell(32,5,'Vr. Unitario',0,'C',1,1,'','',true,'',false,true,0,'M',true);
	$pdf->SetY($y);
	$pdf->Cell(245);
    $pdf->MultiCell(32,5,'Vr. Total',0,'C',1,1,'','',true,'',false,true,0,'M',true);
	$pdf->SetFont('helvetica','',6);
	$cont=0;
	$pdf->ln(1);
	
    $con=0;
    $total=0;
    $linkbd = conectar_v7();
    $sql="SELECT almginventario_det.unspsc, almginventario_det.codart, almarticulos.nombre, almginventario_det.cantidad_entrada,almginventario_det.cantidad_salida, almginventario_det.valorunit, almginventario_det.valortotal,almginventario_det.unidad FROM almginventario_det INNER JOIN almarticulos ON almginventario_det.codart=CONCAT(almarticulos.grupoinven, almarticulos.codigo) WHERE almginventario_det.codigo='$_POST[numero]' AND almginventario_det.tipomov='$_POST[tipomov]' AND almginventario_det.tiporeg='$_POST[entr]' ORDER BY almginventario_det.codart";
    $res=mysqli_query($linkbd, $sql);
    $cant=0;
    $cont=1;
    while ($row =mysqli_fetch_row($res)) 
	{
        if($vv>=170)
        { 
            $pdf->AddPage();
            $vv=$pdf->gety();
        }
        if ($con%2==0){
            $pdf->SetFillColor(255,255,255);
        }
        else {
            $pdf->SetFillColor(245,245,245);
        }
        $cant=$row[3];
	    if($cant==0){
            $cant=$row[4];
        }
		
        $termul=strtoupper($row[2]);
		
        $lineasnombres = $pdf ->getNumLines($termul, 66);
		$lineasnombress = $pdf ->getNumLines($row[7], 30);
        $valorMaxs = max($lineasnombres, $lineasnombress);
        $alturas = $valorMaxs * 3;

        $pdf->cell(0.2);
        $pdf->MultiCell(20,$alturas,$cont,0,'C',true,0,'','',true,0,false,true,0,'M',true);
        $pdf->MultiCell(31,$alturas,$row[0],0,'C',true,0,'','',true,0,false,true,0,'M',true);
        $pdf->MultiCell(31,$alturas,$row[1],0,'C',true,0,'','',true,0,false,true,0,'M',true);
        $pdf->MultiCell(66,$alturas,$row[2],0,'L',true,0,'','',true,0,false,true,0,'M',true);
        $pdf->MultiCell(33,$alturas,$cant,0,'C',true,0,'','',true,0,false,true,0,'M',true);
        $pdf->MultiCell(30,$alturas,$row[7],0,'C',true,0,'','',true,0,false,true,0,'M',true);
        $pdf->MultiCell(33,$alturas,'$ '.number_format($row[5]),0,'R',true,0,'','',true,0,false,true,0,'M',true);
        $pdf->MultiCell(33,$alturas,'$ '.number_format($row[6]),0,'R',true,1,'','',true,0,false,true,0,'M',true);
        $total=$total+$row[6];		
        $con=$con+1;
	    $cont+=1;

	}
	//$pdf->ln(4);
	//echo $pdf->GetY();
    $pdf->ln(4);
    $v=$pdf->gety();
    $x=$pdf->getx();
    $pdf->line(10.1,$v-2,287,$v-2);
    $pdf->Cell(185);
    $pdf->SetFont('helvetica','B',7);
    $pdf->SetX(248);
    $pdf->Cell(20,3,'Total',0,0,'C');
    $pdf->Cell(20,3,'$ '.number_format($total),0,1,'C');
  
	$pdf->ln(5);
    $linkbd = conectar_v7();
    $linkbd -> set_charset("utf8");
    $sqlr="SELECT funcionario, nomcargo FROM firmaspdf_det WHERE idfirmas='6' AND estado ='S'";
    $res=mysqli_query($linkbd,$sqlr);
    $rowCargo=mysqli_fetch_row($res);
    $pdf->SetX(25);
    $v=$pdf->gety();
    if($_POST['tipomov']==1)
    {
        $v=$pdf->gety();
		if($v>=155)
		{ 
			$pdf->AddPage();
			$pdf->ln(15);
			$v=$pdf->gety();
		}
        //entrega
        $X1=60;
        $X2=75;
        $pdf->RoundedRect($X1,$v,'80','37','1','1111');
        $alturadef=8;
        $anchodef=15;
        $anchodef2=60;
        
        $pdf->SetX($X1);       
        $pdf->SetFont('helvetica','B',8);
        $pdf->Cell($anchodef,$alturadef,'ENTREGA: ',0,1,'L');
        $pdf->SetFont('helvetica','B',6.2);
        $pdf->SetX($X1);   
        $pdf->Cell($anchodef,$alturadef,'Firma: ','',0,'L');
        $pdf->SetX($X2); 
        $pdf->Cell($anchodef2,$alturadef,'','B',1,'L');
        $pdf->SetX($X1);
        $pdf->Cell($anchodef,$alturadef,'Nombre: ',0,0,'L');
        $pdf->SetX($X2); 
        $pdf->Cell($anchodef2,$alturadef,'','B',1,'L');
        $pdf->SetX($X1);
        $pdf->Cell($anchodef,$alturadef,'C.C: ',0,0,'L');
        $pdf->SetX($X2); 
        $pdf->Cell($anchodef2,$alturadef,'','B',1,'L');
 
        
        //recibe
        $X3=150;
        $X4=165;
        $pdf->SetY($v);
        $pdf->RoundedRect($X3,$v,'80','37','1','1111');
        $pdf->SetX($X3);       
        $pdf->SetFont('helvetica','B',8);
        $pdf->Cell($anchodef,$alturadef,'RECIBIO: ',0,1,'L');
        $pdf->SetFont('helvetica','B',7);
        $pdf->SetX($X3);   
        $pdf->Cell($anchodef,$alturadef,'Firma: ','',0,'L');
        $pdf->SetX($X4); 
        $pdf->Cell($anchodef2,$alturadef,'','B',1,'L');
        $pdf->SetX($X3);    
        $pdf->Cell($anchodef,$alturadef,'Nombre: ','',0,'L');
        $pdf->SetX($X4); 
        $pdf->Cell($anchodef2,$alturadef,$rowCargo[0],0,1,'L');
        $pdf->SetX($X3);   
        $pdf->Cell($anchodef,$alturadef,'Cargo: ','',0,'L');
        $pdf->SetX($X4); 
        $pdf->Cell($anchodef2,$alturadef,$rowCargo[1],0,1,'L');
    }
	$pdf->Output();
?> 


