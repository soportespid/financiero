<?php

	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require 'comun.inc';
	require 'funciones.inc';
	session_start();
	if(empty($_SESSION)){
		header("location: index.php");
	}
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html lang=es>
	<head>
		<meta charset="utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1"/>
		<meta name="viewport" content="user-scalable=no">
		<title>:: IDEAL 10 - Presupuesto</title>
		<link href="favicon.ico" rel="shortcut icon"/>
		<link href="css/css2z.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/style.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
		<link rel="stylesheet" href="sweetalert2/dist/sweetalert2.min.css">
		<script src="sweetalert2/dist/sweetalert2.min.js"></script>
		<script type="text/javascript" src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<style>
			[v-cloak]{display : none;}
		</style>
	</head>
	<body>
		<section id="myapp" v-cloak>
			<input type="hidden" value = "1" ref="pageType">
            <div class="loading-container" v-show="isLoading" >
                <p class="text-loading"data-loading="IDEAL 10">IDEAL 10</p>
            </div>
			<div class="main-container">
				<header>
					<table>
						<tr><script>barra_imagenes("ccpet");</script><?php cuadro_titulos();?></tr>
					</table>
				</header>
				<nav>
					<table>
						<tr><?php menu_desplegable("ccpet");?></tr>
					</table>
					<div class="bg-white group-btn p-1">
						<button type="button" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center" @click="window.location.reload()">
							<span>Nuevo</span>
							<svg viewBox="0 -960 960 960" ><path d="M440-440H200v-80h240v-240h80v240h240v80H520v240h-80v-240Z"/></svg>
						</button>
						<button type="button" @click="save()" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
							<span>Guardar</span>
							<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960"><path d="M840-680v480q0 33-23.5 56.5T760-120H200q-33 0-56.5-23.5T120-200v-560q0-33 23.5-56.5T200-840h480l160 160Zm-80 34L646-760H200v560h560v-446ZM480-240q50 0 85-35t35-85q0-50-35-85t-85-35q-50 0-85 35t-35 85q0 50 35 85t85 35ZM240-560h360v-160H240v160Zm-40-86v446-560 114Z"></path></svg>
						</button>
						<button type="button" onclick="location.href='ccp-registroContratoBuscar.php'" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
							<span>Buscar</span>
							<svg class="fill-black group-hover:fill-white w-5 h-5" xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960"><path d="M784-120 532-372q-30 24-69 38t-83 14q-109 0-184.5-75.5T120-580q0-109 75.5-184.5T380-840q109 0 184.5 75.5T640-580q0 44-14 83t-38 69l252 252-56 56ZM380-400q75 0 127.5-52.5T560-580q0-75-52.5-127.5T380-760q-75 0-127.5 52.5T200-580q0 75 52.5 127.5T380-400Z"></path></svg>
						</button>
						<button type="button" @click="mypop=window.open('ccp-principal.php','',''); mypop.focus();" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
							<span>Nueva ventana</span>
							<svg class="fill-black group-hover:fill-white w-5 h-5" xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960"><path d="M320-120v-80h80v-80H160q-33 0-56.5-23.5T80-360v-400q0-33 23.5-56.5T160-840h640q33 0 56.5 23.5T880-760v400q0 33-23.5 56.5T800-280H560v80h80v80H320ZM160-360h640v-400H160v400Zm0 0v-400 400Z"></path></svg>
						</button>
						<button type="button" @click="mypop=window.open('ccp-registroContratoCrear.php','','');mypop.focus();" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
							<span class="group-hover:text-white">Duplicar pantalla</span>
							<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960"><path d="M640-160v-360H160v360h480Zm80-200v-80h80v-360H320v200h-80v-200q0-33 23.5-56.5T320-880h480q33 0 56.5 23.5T880-800v360q0 33-23.5 56.5T800-360h-80ZM160-80q-33 0-56.5-23.5T80-160v-360q0-33 23.5-56.5T160-600h480q33 0 56.5 23.5T720-520v360q0 33-23.5 56.5T640-80H160Zm400-603ZM400-340Z"></path></svg>
						</button>
						<button type="button" @click="window.location.href='ccp-registroContratoBuscar'" class="btn btn-success btn-success-hover d-flex justify-between align-items-center">
                            <span>Atras</span>
                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960"><path d="M760-200v-160q0-50-35-85t-85-35H273l144 144-57 56-240-240 240-240 57 56-144 144h367q83 0 141.5 58.5T840-360v160h-80Z"></path></svg>
                        </button>
					</div>
				</nav>
				<article>		
					<div ref="rTabs" class="nav-tabs bg-white p-1">
						<div class="nav-item" :class="tabs == '1' ? 'active' : ''" @click="tabs='1';">Información general</div>
						<div class="nav-item" :class="tabs == '2' ? 'active' : ''" @click="tabs='2';">Precontractual</div>
						<div class="nav-item" :class="tabs == '3' ? 'active' : ''" @click="tabs='3';">Contractual</div>
						<div class="nav-item" :class="tabs == '4' ? 'active' : ''" @click="tabs='4';">Postcontractual</div>
					</div>

					<div>
						<div class="bg-white" v-show="tabs == 1">
							<div class="w-100">
								<h2 class="titulos m-0">Registro de contratos y/o resoluciones</h2>
								<div class="d-flex w-100">
									<div class="form-control w-15">
										<label class="form-label">Fecha <span class="text-danger fw-bolder">*</span></label>
										<input type="date" v-model="fecha" class="text-center">
									</div>
									
									<div class="form-control w-25">
										<label class="form-label" for="">CDP<span class="text-danger fw-bolder">*</span></label>
										<input type="text" v-model="codCdp" @change="getDataSolicitud()" class="colordobleclik" @dblclick="modalCdp=true" readonly>
									</div>

									<div class="form-control w-25">
										<label class="form-label" for="">Tipo documento</label>
										<select v-model="tipoDocumento" disabled>
											<option value=""></option>
											<option value="1">Tipo de contrato</option>
											<option value="2">Acto administrativo</option>
										</select>
									</div>

									<div class="form-control w-15" v-show="tipoDocumento==1">
										<label class="form-label" for="">Tipo de Contrato</label>
										<select v-model="tipoContrato" disabled>
											<option value="">Seleccione</option>
											<option value="1">Obra</option>
											<option value="2">Consultora servicio</option>
											<option value="3">Suministro y/o compraventa</option>
											<option value="4">Prestacion de servicios</option>
											<option value="5">Otro</option>
										</select>
									</div>

									<div class="form-control w-15" v-show="tipoDocumento==1">
										<label class="form-label" for="">Número contrato<span class="text-danger fw-bolder">*</span></label>
										<input type="text" v-model="numeroContrato" class="text-center">
									</div>

									<div class="form-control w-15" v-show="tipoDocumento==2">
										<label class="form-label" for="">Acto administrativo<span class="text-danger fw-bolder">*</span></label>
										<input type="text" v-model="numero_acto" class="text-center">
									</div>

									<div class="form-control w-15" v-show="tipoDocumento==2">
										<label class="form-label" for="">Fecha acto adm.<span class="text-danger fw-bolder">*</span></label>
										<input type="date" v-model="fecha_acto" class="text-center">
									</div>
								</div>

								<div class="d-flex">
									<div class="form-control">
										<label class="form-label" for="">Objeto</label>
										<textarea type="text" v-model="objeto" rows="1" class="resize-vertical" style="max-height:100px;"></textarea>
									</div>
								</div>

								<div class="d-flex">
									<div class="form-control">
										<label class="form-label" for="">Tercero<span class="text-danger fw-bolder">*</span></label>
										<div class="d-flex">
											<input type="number" v-model="terceroDoc" @dblclick="modalTercero=true" class="colordobleclik w-25">
											<input type="text" v-model="terceroName" disabled readonly>
										</div>
									</div>

									<div class="form-control w-25">
										<label class="form-label" for="">Banco<span class="text-danger fw-bolder">*</span></label>
										<div class="d-flex">
											<input type="number" v-model="numCuentaBanco" @dblclick="deployModal('bancos')" class="colordobleclik" readonly>
										</div>
									</div>
								</div>
								<div class="d-flex w-100">
									<div class="form-control w-15" v-show="codPaa!=0">
										<label class="form-label" for="">PAA<span class="text-danger fw-bolder">*</span></label>
										<input type="text" v-model="codPaa" class="text-center" disabled readonly>
									</div>
									<div class="form-control w-25" v-show="codPaa!=0">
										<label class="form-label" for="">Modalidad</label>
										<input type="text" v-model="nombreModalidad" disabled readonly>
									</div>

									<div class="form-control w-15">
										<label class="form-label" for="">Valor contrato<span class="text-danger fw-bolder">*</span></label>
										<input type="text" class="text-right" v-model="valorFormat" @input="formatInputNumber('valorFormat', 'valor')">
									</div>

									<div class="form-control w-15" v-show="codPaa!=0">
										<label class="form-label" for="">Duración (Meses)<span class="text-danger fw-bolder">*</span></label>
										<input type="text" v-model="duracion" class="text-center">
									</div>

									<div class="form-control w-15" v-show="codPaa!=0">
										<label class="form-label" for="">Duración (Días)<span class="text-danger fw-bolder">*</span></label>
										<input type="text" v-model="duracion_dias" class="text-center">
									</div>

									<div class="form-control w-15" v-show="codPaa!=0">
										<label class="form-label" for="">Garantias<span class="text-danger fw-bolder">*</span></label>
										<select v-model="garantias">
											<option value="">Seleccione</option>
											<option value="S">Si</option>
											<option value="N">No</option>
										</select>
									</div>
								</div>

								<div class="d-flex">
									<div class="form-control">
										<label class="form-label" for="">Forma de pago<span class="text-danger fw-bolder">*</span></label>
										<input type="text" v-model="formaPago">
									</div>
								</div>

								<div class="bg-white">
									<h2 class="titulos m-0">Items</h2>
									<div class="d-flex w-100">
										<div class="form-control ">
											<label class="form-label" for="">Descripción<span class="text-danger fw-bolder">*</span>:</label>
											<input type="text" v-model="descripcion">
										</div>
										<div class="form-control w-50">
											<label class="form-label" for="">Unidad de Medida<span class="text-danger fw-bolder">*</span>:</label>
											<input type="text" class="text-center" v-model="unidadMedida">
										</div>
										<div class="form-control w-25">
											<label class="form-label" for="">Cantidad<span class="text-danger fw-bolder">*</span>:</label>
											<input type="text" class="text-center" v-model="cantidad" @change="calcularSubTotal()">
										</div>
										<div class="form-control w-25">
											<label class="form-label">Valor unitario <span class="text-danger fw-bolder">*</span>:</label>
											<input type="text" class="text-right" v-model="valorUnitarioFormat" @input="formatInputNumber('valorUnitarioFormat', 'valorUnitario')" @change="calcularSubTotal()">
										</div>
										<div class="form-control w-25">
											<label class="form-label">Subtotal <span class="text-danger fw-bolder">*</span>:</label>
											<input type="text" class="text-right" v-model="viewFormatNumber(subTotal)" disabled>
										</div>
										<div class="form-control justify-between w-25">
											<label for=""></label>
											<button type="button" class="btn btn-primary" @click="addItem()">Agregar Item</button>
										</div>
									</div>
									<div class="table-responsive" style="height: 250px;">
										<table class="table table-hover fw-normal">
											<thead>
												<tr class="text-center">
													<th>No</th>
													<th>Descripción</th>
													<th>Unidad de Medida</th>
													<th>Valor Unitario</th>
													<th>Cantidad</th>
													<th>Subtotal</th>
													<th>Eliminar</th>
												</tr>
											</thead>
											<tbody>
												<tr v-for="(data,index) in arrayItems" :key="index">
													<td>{{index+1}}</td>
													<td>{{data.descripcion}}</td>
													<td class="text-center">{{data.unidad}}</td>
													<td class="text-right">{{viewFormatNumber(data.valor)}}</td>
													<td class="text-center">{{data.cantidad}}</td>
													<td class="text-right">{{viewFormatNumber(data.subtotal)}}</td>
													<td>
														<div class="d-flex justify-center">
															<button type="button" @click="delItem(index)" class="btn btn-danger">x</button>
														</div>
													</td>
												</tr>
											</tbody>
											<tfoot>
												<tr class="bg-secondary fw-bold text-right">
													<td colspan="5">Total</td>
													<td>{{viewFormatNumber(totalItems)}}</td>
												</tr>
											</tfoot>
										</table>
									</div>
								</div>

							</div>
						</div>

						<div class="bg-white" v-show="tabs == 2">
							<h2 class="titulos m-0">Anexos precontractuales</h2>
							<div class="table-responsive">
								<table class="table table-hover fw-normal">
									<thead>
										<tr class="text-center">
											<th style="width: 70%;">Nombre de archivo</th>
											<th>Opción</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td class="text-center">Certificado de plan de compras</td>
											<td class="text-center">
												<input type="file">
											</td>
										</tr>
										<tr>
											<td class="text-center">Certificado de banco de proyectos</td>
											<td class="text-center">
												<input type="file">
											</td>
										</tr>
										<tr>
											<td class="text-center">Certificado de disponibilidad</td>
											<td class="text-center">
												<input type="file">
											</td>
										</tr>
										<tr>
											<td class="text-center">Estudio del sector</td>
											<td class="text-center">
												<input type="file">
											</td>
										</tr>
										<tr>
											<td class="text-center">Estudio previo</td>
											<td class="text-center">
												<input type="file">
											</td>
										</tr>
										<tr>
											<td class="text-center">Matriz de riesgos</td>
											<td class="text-center">
												<input type="file">
											</td>
										</tr>
										<tr>
											<td class="text-center">Otro</td>
											<td class="text-center">
												<input type="file">
											</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>

						<div class="bg-white" v-show="tabs == 3">
							<h2 class="titulos m-0">Anexos contractuales</h2>
							<div class="table-responsive">
								<table class="table table-hover fw-normal">
									<thead>
										<tr class="text-center">
											<th style="width: 70%;">Nombre de archivo</th>
											<th>Opción</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td class="text-center">Pliego de condiciones</td>
											<td class="text-center">
												<input type="file">
											</td>
										</tr>
										<tr>
											<td class="text-center">Resolución de adjudicación</td>
											<td class="text-center">
												<input type="file">
											</td>
										</tr>
										<tr>
											<td class="text-center">Contrato</td>
											<td class="text-center">
												<input type="file">
											</td>
										</tr>
										<tr>
											<td class="text-center">Designacion y/o contrato supervision e interventoria</td>
											<td class="text-center">
												<input type="file">
											</td>
										</tr>
										
										<tr>
											<td class="text-center">Otro</td>
											<td class="text-center">
												<input type="file">
											</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>

						<div class="bg-white" v-show="tabs == 4">
							<h2 class="titulos m-0">Anexos Poscontractuales</h2>
							<div class="table-responsive">
								<table class="table table-hover fw-normal">
									<thead>
										<tr class="text-center">
											<th style="width: 70%;">Nombre de archivo</th>
											<th>Opción</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td class="text-center">Aprobación de polizas</td>
											<td class="text-center">
												<input type="file">
											</td>
										</tr>
										<tr>
											<td class="text-center">Acta de inicio</td>
											<td class="text-center">
												<input type="file">
											</td>
										</tr>
										<tr>
											<td class="text-center">Informes</td>
											<td class="text-center">
												<input type="file">
											</td>
										</tr>
										<tr>
											<td class="text-center">Modificaciones</td>
											<td class="text-center">
												<input type="file">
											</td>
										</tr>
										<tr>
											<td class="text-center">Acta de terminación</td>
											<td class="text-center">
												<input type="file">
											</td>
										</tr>
										<tr>
											<td class="text-center">Acta de liquidación</td>
											<td class="text-center">
												<input type="file">
											</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</article>

				<!-- MODALES -->
				<div v-show="modalCdp" class="modal">
                    <div class="modal-dialog modal-xl">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title">Certificados de disponibiidad presupuestal</h5>
                                <button type="button" @click="modalCdp=false;" class="btn btn-close"><div></div><div></div></button>
                            </div>
                            <div class="modal-body">
								<p class="m-0 ms-2 fw-bold">Señor usuario, use un filtro a la vez<span class="text-danger fw-bolder">*</span></p>
                                <div class="d-flex">
									<div class="form-control w-50">
										<label class="form-label" for="">Area</label>
										<select v-model="selectArea" @change="filter('area')">
											<option value="0">Todas</option>
											<option v-for="area in areas" :value="area.codigo">{{area.nombre}}</option>
										</select>
									</div>

									<div class="form-control w-25">
										<label class="form-label" for="">Fecha inicial</label>
                                        <input type="date" v-model="fechaIni" @change="filterByDate()">
                                    </div>

									<div class="form-control w-25">
										<label class="form-label" for="">Fecha final</label>
                                        <input type="date" v-model="fechaFin" @change="filterByDate()">
                                    </div>

                                    <div class="form-control justify-between">
										<label class="form-label" for=""></label>
                                        <input type="search" v-model="txtSearch" @keyup="filter('modalCdpTxt')" placeholder="Busca código, BPIM o descripción">
                                    </div>

									
                                </div>
                                <div class="overflow-auto max-vh-50 overflow-x-hidden" >
                                    <table class="table table-hover fw-normal">
                                        <thead>
                                            <tr>
                                                <th class="text-center">Código</th>
                                                <th class="text-center">Fecha</th>
                                                <th style="width: 50%;">Objeto</th>
                                                <th class="text-center">BPIM</th>
                                                <th class="text-center">Area</th>
												<th class="text-right">Valor</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr v-for="(cdp,index) in cdps" :key="index" @click="selectItem('cdp', cdp)">
												<td class="text-center">{{cdp.consvigencia}}</td>
												<td class="text-center">{{cdp.fecha}}</td>
												<td>{{cdp.objeto}}</td>
												<td class="text-center">{{cdp.bpim}}</td>
												<td class="text-center">{{cdp.nombre_area}}</td>
												<td class="text-right">{{viewFormatNumber(cdp.valor)}}</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

				<div v-show="modalTercero" class="modal">
                    <div class="modal-dialog modal-xl" >
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title">Terceros</h5>
                                <button type="button" @click="modalTercero=false;" class="btn btn-close"><div></div><div></div></button>
                            </div>
                            <div class="modal-body">
                                <div class="d-flex flex-column">
                                    <div class="form-control m-0 mb-3">
                                        <input type="search" v-model="txtSearch" @keyup="filter('modalTerceroTxt')" placeholder="Busca por documento o nombre">
                                    </div>
                                </div>
                                <div class="overflow-auto max-vh-50 overflow-x-hidden" >
                                    <table class="table table-hover fw-normal">
                                        <thead>
                                            <tr>
                                                <th class="text-center" style="width: 15%;">Documento</th>
                                                <th>Nombre</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr v-for="(tercero,index) in terceros" :key="index" @click="selectItem('tercero', tercero)">
                                                <td class="text-center">{{tercero.cedulanit}}</td>
                                                <td>{{tercero.nombre}}</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

				<div v-show="modalBanco" class="modal">
                    <div class="modal-dialog modal-sm" >
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title">Bancos</h5>
                                <button type="button" @click="modalBanco=false;" class="btn btn-close"><div></div><div></div></button>
                            </div>
                            <div class="modal-body">
                                <div class="d-flex flex-column">
                                    <div class="form-control m-0 mb-3">
                                        <input type="search" v-model="txtSearchBanco" @keyup="filter('txtModalBanco')" placeholder="Busca por documento o nombre">
                                    </div>
                                </div>
                                <div class="overflow-auto max-vh-50 overflow-x-hidden" >
                                    <table class="table table-hover fw-normal">
                                        <thead>
                                            <tr>
                                                <th class="text-center">Número cuenta</th>
                                                <th>Tipo cuenta</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr v-for="(banco,index) in bancos" :key="index" @click="selectItem('banco', banco)">
                                                <td class="text-center">{{banco.numero_cuenta}}</td>
                                                <td>{{banco.tipo_cuenta}}</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

			</div>
		</section>
		<script src="Librerias/vue/vue.min.js"></script>
		<script src="Librerias/vue/axios.min.js"></script>
		<script type="module" src="presupuesto_ccpet/registroContrato/js/registro_functions.js"></script>
	</body>
</html>
