<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");

	require "comun.inc";
	require "funciones.inc";

	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");

	session_start();
    date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html lang=es>
	<head>
		<meta charset="utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1"/>
		<meta name="viewport" content="user-scalable=no">
		<title>:: IDEAL 10 - Contabilidad</title>
		<link href="favicon.ico" rel="shortcut icon"/>
		<link href="css/css2z.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<script src="vue/vue.min.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script src="css/calendario.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<style>
			input[type=number]::-webkit-inner-spin-button,
			input[type=number]::-webkit-outer-spin-button {
				-webkit-appearance: none;
				margin: 0;
			}
			input[type=number]{
				text-align:right;
				box-sizing: border-box;
				font: 120% sans-serif;
				width: 100% !important;
			}
		</style>
		<script src="sweetalert2/dist/sweetalert2.min.js"></script>
        <script>
            window.addEventListener("load",function(){
                const fechaInicial = new Date(new Date().getFullYear(),0,1).toISOString().split("T")[0];
                const fechaFinal = new Date().toISOString().split("T")[0];
                const arrFechaInicial =fechaInicial.split("-");
                const arrFechaFinal =fechaFinal.split("-");
                document.querySelector("#fc_1198971545").value = arrFechaInicial[2]+"/"+arrFechaInicial[1]+"/"+arrFechaInicial[0];
                document.querySelector("#fc_1198971546").value = arrFechaFinal[2]+"/"+arrFechaFinal[1]+"/"+arrFechaFinal[0];
            });
        </script>
		<link rel="stylesheet" href="sweetalert2/dist/sweetalert2.min.css">
	</head>
	<body>
		<header>
			<table>
				<tr><script>barra_imagenes("cont");</script><?php cuadro_titulos();?></tr>
			</table>
		</header>
		<section id="myapp" v-cloak >
			<nav>
				<table>
					<tr><?php menu_desplegable("cont");?></tr>
					<tr>
						<td colspan="3" class="cinta">
							<img src="imagenes/add2.png"  class="mgbt1" title="Nuevo">
							<img src="imagenes/guardad.png" title="Guardar"  class="mgbt1">
							<img src="imagenes/buscad.png" class="mgbt1" title="Buscar">
							<img src="imagenes/nv.png" onClick="mypop=window.open('cont-principal.php','',''); mypop.focus();" class="mgbt" title="Nueva Ventana">
							<img src="imagenes/iratras.png" title="Atr&aacute;s" onClick="location.href='cont-estadoComprobantesComparacion.php'" class="mgbt">
						</td>
					</tr>
				</table>
			</nav>
			<article>
				<table class="inicio ancho">
					<tr>
						<td class="titulos" colspan="8" >Comparar Otros Egresos</td>
						<td class="cerrar" style="width:7%" onClick="location.href='cont-principal.php'">Cerrar</td>
					</tr>
					<tr>
						<td class="textonew01" style="width:3cm;">:&middot; Fecha Inicial:</td>
						<td style="width:10%;"><input type="text" name="fecha"  value="<?php echo $_POST['fecha']?>" onKeyUp="return tabular(event,this)" id="fc_1198971545" title="DD/MM/YYYY" onDblClick="displayCalendarFor('fc_1198971545');" class="colordobleclik" autocomplete="off" onChange="" readonly></td>
						<td class="textonew01" style="width:3cm;" >:&middot; Fecha Final:</td>
						<td style="width:10%;"><input type="text" name="fecha2" value="<?php echo $_POST['fecha2']?>" onKeyUp="return tabular(event,this)" id="fc_1198971546" title="DD/MM/YYYY" onDblClick="displayCalendarFor('fc_1198971546');" class="colordobleclik" autocomplete="off" onChange="" readonly></td>
						<td colspan="2" style="padding-bottom:0px"><em class="botonflechaverde" v-on:Click="iniciabuscar(3);">Buscar</em></td>
						<td></td>
					</tr>
				</table>
					<table class='tablamv'  style='height:66vh; width:99.2%; margin-top:0px; overflow:hidden'>
						<thead>
							<tr style="text-align:Center;">
								<th class="titulosnew02" >Tesoreria</th>
								<th class="titulosnew03"  style="width:24%;">Contabilidad</th>
								<th class="titulosnew05"  style="width:7.9%;"></th>
							</tr>
							<tr style="text-align:Center;">
								<th class="titulosnew00" style="width:4%;">Estado</th>
								<th class="titulosnew00" style="width:6%;">Código</th>
								<th class="titulosnew00" style="width:6%;">Fecha</th>
								<th class="titulosnew00" style="width:10%;">Cuenta</th>
								<th class="titulosnew00" style="width:6%;">CC</th>
								<th class="titulosnew00" >Descripción</th>
								<th class="titulosnew00" style="width:10%;">Valor</th>
								<th class="titulosnew01" style="width:4%;">Estado</th>
								<th class="titulosnew01" style="width:10%;">Debito</th>
								<th class="titulosnew01" style="width:10%;">Credito</th>
								<th class="titulosnew04" style="width:8%;">Diferencia</th>
							</tr>
						</thead>
						<tbody>
							<tr v-for="(vcinfo1, index) in infobasico1" v-bind:class="(vcinfo1[6]+vcinfo1[8]) == 'SS' ? (vcinfo1[11] == 0 ? (index % 2 ? 'contenidonew00' : 'contenidonew01') : 'contenidonew04' ) : ((vcinfo1[6]+vcinfo1[8]) == 'NN' ? (vcinfo1[11] == 0 ? (index % 2 ? 'contenidonew05' : 'contenidonew06') : 'contenidonew03') : 'contenidonew03')"  style='text-rendering: optimizeLegibility; cursor: pointer !important;' v-on:dblClick ="direccionaComprobante('1','15',vcinfo1[0])">
								<td style="width:4%; font: 120% sans-serif; padding-left:5px; text-align:Center;">{{ vcinfo1[6] }}</td>
								<td style="width:6%; font: 120% sans-serif; padding-left:5px; text-align:Center;">{{ vcinfo1[0] }}</td>
								<td style="width:6%; font: 120% sans-serif; padding-left:5px; text-align:Center;">{{ vcinfo1[1] }}</td>
								<td style="width:10%; font: 120% sans-serif; padding-left:5px; text-align:Center;">{{ vcinfo1[2] }}</td>
								<td style="width:6%; font: 120% sans-serif; padding-left:5px; text-align:Center;">{{ vcinfo1[3] }}</td>
								<td style="font: 120% sans-serif; padding-left:5px; text-align:justify;">{{ vcinfo1[5] }}</td>
								<td style="width:10%; font: 120% sans-serif; padding-left:5px; text-align:right;">{{ formatonumero(vcinfo1[4]) }}</td>
								<td style="width:4%; font: 120% sans-serif; padding-left:5px; text-align:Center;">{{ vcinfo1[8] }}</td>
								<td style="width:10%; font: 120% sans-serif; padding-left:5px; text-align:right;">{{ formatonumero(vcinfo1[9]) }}</td>
								<td style="width:10%; font: 120% sans-serif; padding-left:5px; text-align:right;">{{ formatonumero(vcinfo1[10]) }}</td>
								<td style="width:8%; font: 120% sans-serif; padding-left:5px; text-align:right;">{{ formatonumero(vcinfo1[11]) }}</td>
							</tr>
                        </tbody>
					</table>

				<div id="cargando" v-if="loading" class="loading">
					<span>Cargando...</span>
				</div>
			</article>
		</section>
		<script src="Librerias/vue/vue.min.js"></script>
		<script src="Librerias/vue/axios.min.js"></script>
		<script src="vue/contabilidad/cont-compararotrosegresos.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
	</body>
</html>
