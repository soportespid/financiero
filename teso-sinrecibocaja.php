<?php
	require "comun.inc";
  	require "funciones.inc";
  	require "conversor.php";
  	session_start();
  	$linkbd=conectar_v7();
	titlepag();
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	header("Cache-control: private"); // Arregla IE 6
	date_default_timezone_set("America/Bogota");
?>
<html lang="es">
	<head>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
    	<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<title>:: IDEAL 10 - Tesorer&iacute;a</title>
		<link href="favicon.ico" rel="shortcut icon"/>
		<script src="css/programas.js"></script>
		<script src="css/calendario.js"></script>
		<link href="css/css2.css" rel="stylesheet" type="text/css" />
		<link href="css/css3.css" rel="stylesheet" type="text/css" />
		<link href="css/css4.css" rel="stylesheet" type="text/css" />
		<link href="css/tabs.css" rel="stylesheet" type="text/css" />
		<script>
	  		function validar() {

				document.form2.submit();
			}

	  		function guardar()
	  		{
				let recaudadoEn = document.form2.modorec.value;
	   			if(document.form2.fecha.value!='' && recaudadoEn!='') {
					if(recaudadoEn == 'banco'){
						let cuentaBan = document.form2.cb.value;
						if(cuentaBan == ''){
							despliegamodalm('visible','2','Faltan datos para completar el registro');
						}else{
							despliegamodalm('visible','4','Esta Seguro de Guardar','1');
						}
					}else{
						despliegamodalm('visible','4','Esta Seguro de Guardar','1');
					}

				}
		   		else {

			  		despliegamodalm('visible','2','Faltan datos para completar el registro');
			 		document.form2.fecha.focus();
			 		document.form2.fecha.select();
		   		}
	  		}

			function despliegamodalm(_valor,_tip,mensa,pregunta,variable) {

				document.getElementById("bgventanamodalm").style.visibility=_valor;

				if(_valor=="hidden") {

					document.getElementById('ventanam').src="";
				}
				else {

					switch(_tip) {

						case "1":
							document.getElementById('ventanam').src="ventana-mensaje1.php?titulos="+mensa;
						break;

						case "2":
							document.getElementById('ventanam').src="ventana-mensaje3.php?titulos="+mensa;
						break;

						case "3":
							document.getElementById('ventanam').src="ventana-mensaje2.php?titulos="+mensa;
						break;

						case "4":
							document.getElementById('ventanam').src="ventana-consulta1.php?titulos="+mensa+"&idresp="+pregunta;
						break;

						case "5":
							document.getElementById('ventanam').src="ventana-elimina1.php?titulos="+mensa+"&idresp="+pregunta+"&variable="+variable;
						break;
					}
				}
			}

			function respuestaconsulta(pregunta, variable) {

				switch(pregunta) {

					case "1":
						document.getElementById('oculto').value="2";
						document.form2.submit();
					break;

					case "2":
						document.form2.elimina.value=variable;
						vvend=document.getElementById('elimina');
						vvend.value=variable;
						document.form2.submit();
					break;
				}
			}

			function funcionmensaje() {

				document.location.href = "teso-sinrecibocajaver.php?idrecibo="+document.getElementById('idcomp').value+"&scrtop=0&totreg=1&altura=461&numpag=1&limreg=10&filtro=";
			}

		  	function pdf() {

				document.form2.action="teso-pdfsinrecaja.php";
				document.form2.target="_BLANK";
				document.form2.submit();
				document.form2.action="";
				document.form2.target="";
		  	}

		  	function despliegamodal2(_valor, _num) {

				/* document.getElementById("bgventanamodal2").style.visibility=_valor;

				if(_valor=="hidden") {
					document.getElementById('ventana2').src="";
				}
				else {
					document.getElementById('ventana2').src="cuentasbancarias-ventana02.php?tipoc=D&obj01=banco&obj02=nbanco&obj03=&obj04=cb&obj05=ter";
				} */

				document.getElementById("bgventanamodal2").style.visibility=_valor;
				if(_valor=="hidden"){document.getElementById('ventana2').src="";}
				else
				{
					switch(_num)
					{
						case '1':	document.getElementById('ventana2').src="cuentasbancarias-ventana02.php?tipoc=D&obj01=banco&obj02=nbanco&obj03=&obj04=cb&obj05=ter";break;
						case '2':	document.getElementById('ventana2').src="ventana-reconocimientoInterno.php";break;
					}
				}
			}
	  	</script>

  </head>
	<body>
        <IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
        <span id="todastablas2"></span>
  		<table>
            <tr><script>barra_imagenes("teso");</script><?php cuadro_titulos();?></tr>
			<tr><?php menu_desplegable("teso");?></tr>
            <tr>
                <td colspan="3" class="cinta">
					<a onClick="location.href='teso-sinrecibocaja.php'" class="mgbt"><img src="imagenes/add.png" title="Nuevo"/></a>
					<a onClick="guardar()" class="mgbt"><img src="imagenes/guarda.png" title="Guardar"/></a>
					<a class="mgbt" onClick="location.href='teso-buscasinrecibocaja.php'"><img src="imagenes/busca.png" title="Buscar" /></a>
					<a onClick="mypop=window.open('plan-agenda.php','','');mypop.focus()" class="mgbt"><img src="imagenes/agenda1.png" title="Agenda" /></a>
					<a class="mgbt" onClick="mypop=window.open('teso-principal.php','',''); mypop.focus();"><img src="imagenes/nv.png" title="Nueva ventana"></a>
					<a onClick="mypop=window.open('<?php echo $url2; ?>','','');mypop.focus();" class="tooltip bottom mgbt"><img src="imagenes/duplicar_pantalla.png" title="Duplicar pesta&ntilde;a"></a>
				</td>
            </tr>
  		</table>
        <div id="bgventanamodalm" class="bgventanamodalm">
			<div id="ventanamodalm" class="ventanamodalm">
				<IFRAME src="" name="ventanam" marginWidth=0 marginHeight=0 frameBorder=0 id="ventanam" frameSpacing=0 style=" width:700px; height:130px; top:200; overflow:hidden;">
				</IFRAME>
			</div>
		</div>
		<form name="form2" method="post" action="">
			<?php
				//$vigencia=date(Y);
				$vigusu=vigencia_usuarios($_SESSION['cedulausu']);
				$vigencia=$vigusu;

				$sqlr="select cuentacaja from tesoparametros";
				$res=mysqli_query($linkbd,$sqlr);
				while ($row =mysqli_fetch_row($res))
				{
					$_POST['cuentacaja']=$row[0];
				}

				if(!$_POST['oculto'])
				{
					$check1="checked";
					$fec=date("d/m/Y");
					$_POST['vigencia']=$vigencia;
					$sqlr="SELECT cuentacaja FROM tesoparametros";
					$res=mysqli_query($linkbd,$sqlr);
					while ($row =mysqli_fetch_row($res)){$_POST['cuentacaja']=$row[0];}
					$sqlr="select valor_inicial,valor_final, tipo from dominios where nombre_dominio='COBRO_RECIBOS' AND descripcion_valor='$vigusu' and  tipo='S'";
					$res=mysqli_query($linkbd,$sqlr);
					while ($row =mysqli_fetch_row($res))
					{
						$_POST['cobrorecibo']=$row[0];
						$_POST['vcobrorecibo']=$row[1];
						$_POST['tcobrorecibo']=$row[2];
					}
					$sqlr="select max(id_recibos) from tesosinreciboscaja ";
					$res=mysqli_query($linkbd,$sqlr);
					$consec=0;
					while($r=mysqli_fetch_row($res)){$consec=$r[0];}
					$consec+=1;
					$_POST['idcomp']=$consec;
			 		$fec=date("d/m/Y");
					$_POST['fecha']=$fec;
					$_POST['valor']=0;
				}
				switch($_POST['tabgroup1'])
				{
					case 1:	$check1='checked';break;
					case 2:	$check2='checked';break;
					case 3:	$check3='checked';
				}
			?>
  			<input name="encontro" type="hidden" value="<?php echo $_POST['encontro']?>"/>
   			<input name="cobrorecibo" type="hidden" value="<?php echo $_POST['cobrorecibo']?>"/>
   			<input name="vcobrorecibo" type="hidden" value="<?php echo $_POST['vcobrorecibo']?>"/>
   			<input name="tcobrorecibo" type="hidden" value="<?php echo $_POST['tcobrorecibo']?>"/>
   			<input name="codcatastral" type="hidden" value="<?php echo $_POST['codcatastral']?>"/>
   			<?php
   				if($_POST['oculto'])
   				{
					$sqlr="select *from tesosinrecaudos where tesosinrecaudos.id_recaudo=$_POST[idrecaudo] and estado ='S'";
					$_POST['encontro']="";
					$res=mysqli_query($linkbd,$sqlr);
					while ($row =mysqli_fetch_row($res))
					{
						$_POST['concepto']=$row[6];
						$_POST['valorecaudo']=$row[5];
						$_POST['totalc']=$row[5];
						$_POST['tercero']=$row[4];
						$_POST['ntercero']=buscatercero($row[4]);
						$_POST['encontro']=1;
					}
   				}
   			?>
   			<table class="inicio" align="center" >
                <tr>
                    <td class="titulos" colspan="2">Ingresos Propios</td>
                    <td class="cerrar" style="width:5%;"><a onClick="location.href='teso-principal.php'">&nbsp;Cerrar</a></td>
                </tr>
				<tr>
					<td style="width:80%;">
   						<table>
		   					<tr>
			  					<td style="width:10%" class="saludo1" >No Recibo:</td>
			  					<td style="width:10%">
									<input name="cuentacaja" type="hidden" value="<?php echo $_POST['cuentacaja']?>"/>
									<input name="idcomp" id="idcomp" style="text-align: center;" type="text" value="<?php echo $_POST['idcomp']?>" onKeyUp="return tabular(event,this) " readonly/>
			  					</td>
			  					<td style="width:10%" class="saludo1">Fecha:</td>
			  					<td style="width:10%">
									<input type="text" name="fecha"  value="<?php echo $_POST['fecha']?>" onKeyUp="return tabular(event,this)" id="fc_1198971545" title="DD/MM/YYYY" onDblClick="displayCalendarFor('fc_1198971545');" class="colordobleclik" autocomplete="off" onChange="" readonly>
								</td>
			  					<td style="width:10%" class="saludo1">Vigencia:</td>
			  					<td style="width:10%">
									<input type="text" id="vigencia" name="vigencia" onKeyPress="javascript:return solonumeros(event)" onKeyUp="return tabular(event,this)" style="width:80%;" value="<?php echo $_POST['vigencia']?>" onClick="document.getElementById('tipocta').focus();document.getElementById('tipocta').select();" readonly>
                                </td>
								<td></td>
							</tr>
							<tr>

								<?php $sqlr=""; ?>
								<td class="saludo1">No Liquid:</td>
								<td>
									<input class="colordobleclik" type="text" id="idrecaudo" name="idrecaudo" value="<?php echo $_POST['idrecaudo']?>" onKeyUp="return tabular(event,this)" onChange="validar()" ondblClick="despliegamodal2('visible','2');">
								</td>
								<td class="saludo1">Recaudado en:</td>
								<td>
									<select name="modorec" id="modorec" onKeyUp="return tabular(event,this)" style="width:100%;" onChange="validar()" >
										<option value="" >Seleccione...</option>
										<option value="caja" <?php if($_POST['modorec']=='caja') echo "SELECTED"; ?>>Caja</option>
										<option value="banco" <?php if($_POST['modorec']=='banco') echo "SELECTED"; ?>>Banco</option>
									</select>
								</td>

							<?php
							if ($_POST['modorec']=='banco')
							{
								// echo "<tr><td class='saludo1'>Recaudado en:</td><td>";
							  	// echo"<select id='banco' name='banco' style='width:69%;' onChange='validar()' onKeyUp='return tabular(event,this)'>
							   	// <option value=''>Seleccione....</option>";

							  	// $sqlr="select tesobancosctas.estado,tesobancosctas.cuenta,tesobancosctas.ncuentaban,tesobancosctas.tipo,terceros.razonsocial,tesobancosctas.tercero from tesobancosctas,terceros where tesobancosctas.tercero=terceros.cedulanit and tesobancosctas.estado='S' ";
							  	// $res=mysql_query($sqlr,$linkbd);
							  	// while ($row =mysql_fetch_row($res))
							  	// {
									// echo "<option value=$row[1] ";
									// $i=$row[1];
									// if($i==$_POST[banco])
									// {
								   		// echo "SELECTED";
								   		// $_POST[nbanco]=$row[4];
								   		// $_POST[ter]=$row[5];
								   		// $_POST[cb]=$row[2];
									// }
									// echo ">".$row[2]." - Cuenta ".$row[3]."</option>";

								// }
								echo "
					  				<td class='saludo1'>Cuenta :</td>
				                    <td>
				                    	<input type='text' name='cb' id='cb' style='width:80%;' value='$_POST[cb]' />&nbsp;
				                    	<a onClick=\"despliegamodal2('visible', '1');\"  style='cursor:pointer;' title='Listado Cuentas Bancarias'>
				                    		<img src='imagenes/find02.png' style='width:20px;'/>
				                    	</a>

				                    </td>
									<td>
										<input type='text' id='nbanco' name='nbanco' style='width:100%;' value='$_POST[nbanco]' readonly>
									</td>

									<input type='hidden' name='banco' id='banco' value='$_POST[banco]'/>
									<input type='hidden' id='ter' name='ter' value='$_POST[ter]'/>

									</tr>";

							}else{
								echo "</tr>";
							}
						?>
					   	<tr>
							<td class="saludo1">Concepto:</td>
							<td colspan="6">
						 		<!-- <input name="concepto" style="width:100%" type="text" value="<?php echo $_POST['concepto'] ?>" onKeyUp="return tabular(event,this)"> -->
								<textarea name="concepto" id="concepto" style="width:100%; form-sizing: content; max-height: 100px; min-height: 20px; resize: vertical; " class="estilos-scroll"><?php echo $_POST['concepto']?></textarea>
							</td>
					   	</tr>
					   	<tr>
						 	<td class="saludo1">Valor:</td>
						 	<td>
						   		<input type="text" id="valorecaudo" name="valorecaudo" value="<?php echo $_POST['valorecaudo']?>" style="width:100%" onKeyUp="return tabular(event,this)" readonly >
							</td>
							<td  class="saludo1">Documento: </td>
						 	<td>
						   		<input name="tercero" type="text" value="<?php echo $_POST['tercero']?>" style="width:100%" onKeyUp="return tabular(event,this)" readonly>
						 	</td>
						 	<td class="saludo1">Contribuyente:</td>
						 	<td colspan="2">
						   		<input type="text" id="ntercero" name="ntercero" value="<?php echo $_POST['ntercero']?>" style="width:100%" onKeyUp="return tabular(event,this) "  readonly>

						   		<input type="hidden" id="ct" name="ct" value="<?php echo $_POST['ct']?>" >

								<input type="hidden" value="1" name="oculto" id="oculto">
								<input type="hidden" value="<?php echo $_POST['trec']?>"  name="trec">
								<input type="hidden" value="0" name="agregadet">
							</td>
					   	</tr>
					</table>
				</td>
			</tr>
	   	</table>
		<div class="subpantallac7 estilos-scroll" style="resize: vertical;">
		 	<?php
		  	if($_POST['oculto'] && $_POST['encontro']=='1')
		  	{
				$_POST['trec']='OTROS RECAUDOS';
				// echo $_POST[tcobrorecibo];
				$_POST['dcoding']= array();
				$_POST['dncc']= array();
				$_POST['dncoding']= array();
				$_POST['dvalores']= array();
				$_POST['dfuentes'] = array();
				$sqlr="select * from tesosinrecaudos_det where tesosinrecaudos_det.id_recaudo=$_POST[idrecaudo] and estado ='S'";
				$res=mysqli_query($linkbd,$sqlr);
				while ($row =mysqli_fetch_row($res))
				{
					$_POST['dcoding'][]=$row[2];
					$_POST['dncc'][]=$row[5];
					$sqlr2="select nombre from tesoingresos where codigo='".$row[2]."'";
					$res2=mysqli_query($linkbd,$sqlr2);
					$row2 =mysqli_fetch_row($res2);
					$_POST['dncoding'][]=$row2[0];
					$_POST['dvalores'][]=$row[3];
					$_POST['dfuentes'][]=$row[6];
				}
          	}
 			?>
	   		<table class="inicio">
				<tr>
					<td colspan="5" class="titulos">Detalle Ingresos internos</td>
				</tr>
				<tr>
					<td class="titulos2" style="width: 10%;">C&oacute;digo</td>
					<td class="titulos2">Ingreso</td>
					<td class="titulos2" style="width: 10%;">Fuente</td>
					<td class="titulos2" style="width: 15%;">Valor</td>
				</tr>
				<?php
				if ($_POST['elimina']!='')
				{
					//echo "<TR><TD>ENTROS :".$_POST[elimina]."</TD></TR>";
					$posi=$_POST['elimina'];
					unset($_POST['dcoding'][$posi]);
					unset($_POST['dncoding'][$posi]);
					unset($_POST['dvalores'][$posi]);
					$_POST['dcoding']= array_values($_POST['dcoding']);
					$_POST['dncoding']= array_values($_POST['dncoding']);
					$_POST['dvalores']= array_values($_POST['dvalores']);
				}

				if ($_POST['agregadet']=='1')
				{
					$_POST['dcoding'][]=$_POST['codingreso'];
					$_POST['dncoding'][]=$_POST['ningreso'];
					$_POST['dvalores'][]=$_POST['valor'];
					$_POST['agregadet']=0;
					?>
					<script>
						//document.form2.cuenta.focus();
						document.form2.codingreso.value="";
						document.form2.valor.value="";
						document.form2.ningreso.value="";
						document.form2.codingreso.select();
						document.form2.codingreso.focus();
					</script>
		  			<?php
		  		}

		  		$_POST['totalc']=0;
				$co="saludo1a";
				$co2="saludo2";

				for ($x=0;$x<count($_POST['dcoding']);$x++)
				{
		 			echo "
					 	<input name='dcoding[]' value='".$_POST['dcoding'][$x]."' style='width:100%;' type='hidden' readonly>
						<input name='dncoding[]' value='".$_POST['dncoding'][$x]."' style='width:100%;' type='hidden'  readonly>
						<input name='dncc[]' value='".$_POST['dncc'][$x]."' style='width:100%;' type='hidden'>
						<input name='dfuentes[]' value='".$_POST['dfuentes'][$x]."' style='width:100%;' type='hidden' readonly>
						<input name='dvalores[]' value='".$_POST['dvalores'][$x]."' style='width:100%;' type='hidden' readonly>

						<tr class='$co' onMouseOver=\"anterior=this.style.backgroundColor;this.style.backgroundColor='#40b3ff';\" onMouseOut=\"this.style.backgroundColor=anterior\">
							<td>
								".$_POST['dcoding'][$x]."
							</td>
							<td>
								".$_POST['dncoding'][$x]."
							</td>
							<td>
								".$_POST['dfuentes'][$x]."
							</td>
							<td style='text-align:right; padding-right:10px'>$
								".number_format($_POST['dvalores'][$x], 2)."
							</td>
						</tr>";
					$_POST['totalc']=$_POST['totalc']+$_POST['dvalores'][$x];
					$_POST['totalcf']=number_format($_POST['totalc'],2);

					$aux=$co;
					$co=$co2;
					$co2=$aux;
		 		}
 				$resultado = convertir($_POST['totalc']);
				$_POST['letras']=$resultado." PESOS M/CTE";
		 		echo "
					<input name='totalcf' type='hidden' value='$_POST[totalcf]' style='width:100%;' readonly>
					<input name='totalc' type='hidden' value='$_POST[totalc]'>
					<input name='letras' type='hidden' style='width:100%;' value='$_POST[letras]' readonly>

					<tr class='$co'>

						<td colspan='3' style='font-weight:bold; font-size:14px'>Total</td>
						<td style='text-align:right; padding-right:10px'>
							$ ".$_POST['totalcf']."
						</td>
					</tr>
					<tr class='$co2'>
						<td style='font-weight:bold; font-size:14px'>Son:</td>
						<td colspan='4'>
							".$_POST['letras']."
						</td>
					</tr>";
				?>
	   		</table>
		</div>
	  	<?php
		if($_POST['oculto']=='2')
		{
			preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST["fecha"], $fecha);
			$fechaf=$fecha[3]."-".$fecha[2]."-".$fecha[1];

			$sqlValidacionFecha = "SELECT fecha FROM tesosinrecaudos WHERE id_recaudo = '$_POST[idrecaudo]' ";
			$resValidacionFecha = mysqli_query($linkbd,$sqlValidacionFecha);
			$rowValidacionFecha = mysqli_fetch_row($resValidacionFecha);

			$bloq=bloqueos($_SESSION['cedulausu'],$fechaf);
			if($bloq>=1)
			{
				if ($fechaf >= $rowValidacionFecha[0])
				{
					$sqlr="SELECT count(*) FROM tesosinreciboscaja WHERE id_recaudo=$_POST[idrecaudo] AND tipo='3' AND ESTADO='S'";
					$res=mysqli_query($linkbd,$sqlr);
					while($r=mysqli_fetch_row($res))
					{
						$numerorecaudos=$r[0];
					}

					if($numerorecaudos==0)
					{
						preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST["fecha"], $fecha);
						$fechaf=$fecha[3]."-".$fecha[2]."-".$fecha[1];
						$vigPpto = $fecha[3];
						$concecc=0;

						$sqlr="SELECT max(id_recibos) FROM tesosinreciboscaja  ";
						$res=mysqli_query($linkbd,$sqlr);
						while($r=mysqli_fetch_row($res))
						{
							$concecc=$r[0];
						}

						$concecc+=1;

						$sqlr="INSERT INTO comprobante_cab (numerotipo,tipo_comp,fecha,concepto,total,total_debito,total_credito,diferencia,estado) VALUES ($concecc,25,'$fechaf','$_POST[concepto]',0,$_POST[totalc],$_POST[totalc],0,'1')";
						mysqli_query($linkbd,$sqlr);
						$idcomp=mysqli_insert_id($linkbd);

						echo "<input type='hidden' name='ncomp' value='$idcomp'>";

						//DETALLE DEL COMPROBANTE CONTABLE
						for($x=0;$x<count($_POST['dcoding']);$x++)
						{
							//BUSQUEDA INGRESO
							$sqlri = "SELECT * FROM tesoingresos_det WHERE codigo='".$_POST['dcoding'][$x]."' and vigencia=(SELECT MAX(vigencia) FROM tesoingresos_det WHERE codigo = '".$_POST['dcoding'][$x]."' AND estado='S')";
							$resi = mysqli_query($linkbd,$sqlri);
							while($rowi = mysqli_fetch_row($resi))
							{
								$porce = $rowi[5];
								$vi = $_POST['dvalores'][$x]*($porce/100);

								if($rowi[6]!="")
                                {


									if($vi > 0){
										$sqlrFuente = "SELECT fuente FROM tesoingresos_fuentes WHERE codigo = '".$_POST['dcoding'][$x]."' AND estado = 'S'";
										$respFuente = mysqli_query($linkbd, $sqlrFuente);
										$rowFuente = mysqli_fetch_row($respFuente);

										$sqlSeccionPresupuestal = "SELECT id_sp FROM centrocostos_seccionpresupuestal WHERE id_cc = '".$_POST['dncc'][$x]."'";
										$rowSeccionPresupuestal = mysqli_fetch_row(mysqli_query($linkbd, $sqlSeccionPresupuestal));

										$sqlr = "INSERT INTO pptosinrecibocajappto (cuenta, idrecibo, valor, vigencia, fuente, productoservicio, seccion_presupuestal, medio_pago, vigencia_gasto) VALUES('$rowi[6]', $_POST[idcomp], $vi, '$vigPpto', '$rowFuente[0]', '$rowi[7]', '$rowSeccionPresupuestal[0]', 'CSF', '1')";
										mysqli_query($linkbd, $sqlr);
									}

								}


								//busqueda concepto contable
								$sq="SELECT fechainicial FROM conceptoscontables_det WHERE codigo='".$rowi[2]."' AND modulo='4' AND tipo='C' AND cc='".$_POST['dncc'][$x]."' AND fechainicial<'$fechaf' AND cuenta!='' ORDER BY fechainicial ASC";
								$re=mysqli_query($linkbd,$sq);
								while($ro=mysqli_fetch_assoc($re))
								{
									$_POST['fechacausa']=$ro["fechainicial"];
								}

								$sqlrc="SELECT * FROM conceptoscontables_det WHERE estado='S' and modulo='4' AND codigo='".$rowi[2]."' and tipo='C'  and cc='".$_POST['dncc'][$x]."' and fechainicial='".$_POST['fechacausa']."'";
								$resc=mysqli_query($linkbd,$sqlrc);
								while($rowc=mysqli_fetch_row($resc))
								{
									if($_POST['dcoding'][$x]==$_POST['cobrorecibo'])
									{
										if($rowc[7]=='S')
										{
											$columna= $rowc[7];
										}
										else
										{
											$columna= 'N';
										}

										$cuentacont=$rowc[4];
									}
									else
									{
										$columna= $rowc[6];
										$cuentacont=$rowc[4];
									}

									if($columna=='S')
									{
										$valorcred=$_POST['dvalores'][$x]*($porce/100);
										$valordeb=0;

										if($rowc[3]=='N')
										{
											$sqlrpto="Select * from pptocuentas where estado='S' and cuenta='".$rowi[6]."' and vigencia='$vigusu'";
											$respto=mysqli_query($linkbd,$sqlrpto);
											$rowpto=mysqli_fetch_row($respto);

											$vi=$_POST['dvalores'][$x]*($porce/100);

											$sqlr="insert into comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque,valdebito,valcredito,estado,vigencia) values ('25 $concecc','".$cuentacont."','".$_POST['tercero']."','".$rowc[5]."','Ingreso ".strtoupper($_POST['dncoding'][$x])."','',".$valordeb.",".$valorcred.",'1','".$_POST['vigencia']."')";
											mysqli_query($linkbd,$sqlr);

											if($_POST['modorec']=='caja')
											{
												$cuentacb=$_POST['cuentacaja'];
												$cajas=$_POST['cuentacaja'];
												$cbancos="";
											}

											if($_POST['modorec']=='banco')
											{
												$cuentacb=$_POST['banco'];
												$cajas="";
												$cbancos=$_POST['banco'];
											}

											$sqlr="insert into comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque,valdebito,valcredito,estado,vigencia) values ('25 $concecc','".$cuentacb."','".$_POST['tercero']."','".$rowc[5]."','Ingreso ".strtoupper($_POST['dncoding'][$x])."','',".$valorcred.",0,'1','".$_POST['vigencia']."')";
											mysqli_query($linkbd,$sqlr);
										}
									}
								}
							}
						}

						//insercion de cabecera recaudos

						$sqlr="insert into tesosinreciboscaja (id_comp,fecha,vigencia,id_recaudo,recaudado,cuentacaja,cuentabanco,valor,estado,tipo) values($idcomp,'$fechaf',".$vigusu.",$_POST[idrecaudo],'$_POST[modorec]','$cajas','$cbancos','$_POST[totalc]','S','3')";
						if (!mysqli_query($linkbd,$sqlr))
						{
							echo "
								<script>
									despliegamodalm('visible','2','No se pudo ejecutar la petici�n');
									document.getElementById('valfocus').value='2';
								</script>";
						}
						else
						{
							echo "<script>despliegamodalm('visible','1','Se ha almacenado el Recibo de Caja con Exito ');</script>";
							$sqlr="update tesosinrecaudos set estado='P' WHERE ID_RECAUDO=$_POST[idrecaudo]";
							mysqli_query($linkbd,$sqlr);

							?>
								<script>
								document.form2.numero.value="";
								document.form2.valor.value=0;
								</script>
							<?php
						}
					}
					else
					{
						echo "<table ><tr><td class='saludo1'><center>Ya Existe un Recibo de Caja para esta Liquidacion <img src='imagenes/alert.png'></center></td></tr></table>";
					}


					$_POST['ncomp']=$concecc;
					//GUARDAR DETALLE DEL RECIBO DE CAJA
					for($x=0;$x<count($_POST['dcoding']);$x++)
					{
						$sqlr="INSERT into tesosinreciboscaja_det (id_recibos,ingreso,valor,estado) values($concecc,'".$_POST['dcoding'][$x]."',".$_POST['dvalores'][$x].",'S')";
						mysqli_query($linkbd,$sqlr);
					}
				}
				else
				{
					echo "<script>despliegamodalm('visible', '2', 'Fecha de actual documento inferior a la del anterior documento');</script>";
				}
			}
			else
			{
				echo "<script>despliegamodalm('visible', '2', 'Fecha bloqueada');</script>";
			}
		}
		?>
		<div id="bgventanamodal2">
            <div id="ventanamodal2">
                <IFRAME  src="" name="buster" marginWidth=0 marginHeight=0 frameBorder=0 id="ventana2" frameSpacing=0 style="left:500px; width:900px; height:500px; top:200;">
                </IFRAME>
            </div>
       	</div>
	</form>
 	</td></tr>
</table>
</body>
</html>
