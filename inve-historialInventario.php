<?php
	require 'comun.inc';
	require 'funciones.inc';
	session_start();
	cargarcodigopag($_GET['codpag'],$_SESSION['nivel']);
	header('Cache-control: private'); // Arregla IE 6
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	date_default_timezone_set('America/Bogota');
	titlepag();
?>
<!DOCTYPE >
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es">
	<head>
		<meta http-equiv="Content-type" content="text/html; charset=utf8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=9"/>
		<meta name="viewport" content="user-scalable=no">
		<title>:: IDEAL 10 - Almacen</title>
		<link href="css/css2.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/cssSP.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/tabs.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
        <script type="text/javascript" src="css/calendario.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script src="vue/vue.js"></script>
		<script type="text/javascript" src="css/programas.js"></script>        

		<!-- sweetalert2 -->
		<script src="sweetalert2/dist/sweetalert2.min.js"></script>
		<link rel="stylesheet" href="sweetalert2/dist/sweetalert2.min.css">
	</head>
	<body>
		<header>
			<table>
				<tr><script>barra_imagenes("inve");</script><?php cuadro_titulos();?></tr>
			</table>
		</header>

		<form name="form2" method="post" action="">
			<section id="myapp" v-cloak>
				<nav>
					<table>
						<tr><?php menu_desplegable("inve");?></tr>
						<tr>
							<td colspan="3" class="cinta">
								<a href="#" class="mgbt"><img src="imagenes/add2.png"/></a>
								<a class="mgbt"><img src="imagenes/guardad.png" title="Guardar"/></a>
								<a href="#" class="mgbt"><img src="imagenes/buscad.png" title="Buscar"/></a>
								<a onclick="mypop=window.open('plan-agenda.php','','');mypop.focus()" class="mgbt"><img src="imagenes/agenda1.png" title="Agenda" /></a>
								<a onclick="mypop=window.open('inve-principal.php','','');mypop.focus();" class="mgbt"><img src="imagenes/nv.png" title="Nueva ventana"></a>
								<a onclick="mypop=window.open('<?php echo $url2; ?>','','');mypop.focus();" class="mgbt"><img src="imagenes/duplicar_pantalla.png" title="Duplicar pesta&ntilde;a"></a>
								<img src="imagenes/excel.png" v-on:click="excel" title="Excel" class="mgbt">
							</td>
						</tr>
					</table>
				</nav>

				<article>
					<div>
						<table class="inicio grande">
							<tr>
								<td class="titulos" colspan="10">.: Historial de articulos</td>
							</tr>

							<tr>
								<td class="textonew01" style="width: 10%;">.: Código articulo: </td>
								<td>
									<input type="text" name="codArticulo" v-model="codArticulo" style="text-align: center;" readonly>
								</td>

								<td class="textonew01" style="width: 5%;">Nombre: </td>
								<td style="width: 35%;">
									<input type="text" name="nombre" v-model="nombreArticulo" style="width: 100%;" readonly>
								</td>

								<td class="textonew01" style="width: 10%;">Centro costo: </td>
								<td>
									<input type="text" name="cc" v-model="centroCosto" style="text-align: center;" readonly>
								</td>

								<td class="textonew01" style="width: 5%;">Bodega: </td>
								<td>
									<input type="text" name="bodega" v-model="bodeg" style="text-align: center;" readonly>
								</td>
							</tr>
						</table>

						<div id="cargando" v-if="loading" class="loading">
							<span>Cargando...</span>
						</div>

						<div class='subpantalla' style='height:66vh; width:99.2%; margin-top:0px; overflow:hidden'>
							<table class="tablamv">
								<thead>
									<tr>
										<th class="titulosnew00" width="10%">Codigo Articulo</th>
										<th class="titulosnew00">Tipo comprobante</th>
										<th class="titulosnew00" width="10%">Numero</th>
										<th class="titulosnew00" width="10%">Fecha</th>
										<th class="titulosnew00" width="10%">Valor</th>
										<th class="titulosnew00" width="10%">Entrada</th>
										<th class="titulosnew00" width="10%">Salida</th>
										<th class="titulosnew00" width="10%">Saldo</th>
										<th class="titulosnew00" width="10%">Cuenta contable</th>
										<th class="titulosnew00" width="1%"></th>
									</tr>
								</thead>
								

								<tbody>
									<tr v-for="(historial,index) in historial" v-bind:class="index % 2 ? 'saludo1a' : 'saludo2'" style='text-rendering: optimizeLegibility; '>
										<td style="text-align:center; width: 10%;">{{ historial[0] }}</td>
										<td style="text-align:center;">{{ historial[1] }}</td>
										<td style="text-align:center; width: 10%;">{{ historial[2] }}</td>
										<td style="text-align:center; width: 10%;">{{ historial[3] }}</td>
										<td style="text-align:center; width: 10%;">{{ formatonumero(historial[4]) }}</td>
										<td style="text-align:center; width: 10%;">{{ historial[5] }}</td>
										<td style="text-align:center; width: 10%;">{{ historial[6] }}</td>
										<td style="text-align:center; width: 10%;">{{ historial[7] }}</td>
										<td style="text-align:center; width: 10%;">{{ historial[8] }}</td>

										<input type='hidden' name='articulo[]' v-model="historial[0]">	
										<input type='hidden' name='comprobante[]' v-model="historial[1]">	
										<input type='hidden' name='numero[]' v-model="historial[2]">
										<input type='hidden' name='fecha[]' v-model="historial[3]">
										<input type='hidden' name='valor[]' v-model="historial[4]">
										<input type='hidden' name='entrada[]' v-model="historial[5]">
										<input type='hidden' name='salida[]' v-model="historial[6]">
										<input type='hidden' name='saldo[]' v-model="historial[7]">
										<input type='hidden' name='cuentaContable[]' v-model="historial[8]">
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</article>
			</section>
		</form>
		
		<script src="Librerias/vue/vue.min.js"></script>
		<script src="Librerias/vue/axios.min.js"></script>
		<script src="almacen/informeDisponibilidad/inve-historialInventario.js"></script>	
	</body>
</html>