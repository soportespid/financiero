<?php 
	require "comun.inc";
	require "funciones.inc";
	session_start();
	$linkbd = conectar_v7();	
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	header("Cache-control: private"); // Arregla IE 6
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es">
	<head>
	 	<meta http-equiv="Content-Type" content="text/html" charset="iso-8859-1"/>
        <meta http-equiv="X-UA-Compatible" content="IE=9"/>
		<title>:: IDEAL 10 - Servicios P&uacute;bilicos</title>
        <link href="css/css2.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="css/calendario.js"></script>
		<script>
			var anterior;
			function ponprefijo(pref,opc)
			{
				parent.document.form2.exoneracion.value =pref;
				parent.document.form2.nExoneracion.value =opc;
				parent.document.form2.exoneracion.focus();

				parent.despliegamodal2("hidden");	
			} 
		</script> 
		<?php titlepag();?>
	</head>
	<body>
		<?php
		if(!$_POST['tipo'])
			$_POST['tipo'] = $_GET['ti'];
			
		?>
	<form action="" method="post" enctype="multipart/form-data" name="form1">
		<table class="inicio">
			<tr >
				<td height="25" colspan="4" class="titulos" >Buscar Exoneraciones:</td>
				<td class="cerrar"><a onClick="parent.despliegamodal2('hidden');">&nbsp;Cerrar</a></td>
			</tr>
			<tr>
				<td class="saludo1" >:&middot; C&oacute;digo:</td>
				<td colspan="3">
                    <input name="numero" type="text" size="30">
					<input type="hidden" name="tipo"  id="tipo" value="<?php echo $_POST['tipo']?>">
					<input type="hidden" name="pos" id="pos" value="<?php echo $_POST['pos']?>">
					<input type="hidden" name="oculto" id="oculto" value="1" >
					<input type="submit" name="Submit" value="Buscar">
				</td>
			</tr>
			<tr >
				<td colspan="4" align="center">&nbsp;</td>
			</tr>
		</table>
		<div class="subpantalla" style="height:73.5%; width:99.6%; overflow-x:hidden;">
			<table class="inicio">
				<tr >
					<td height="25" colspan="5" class="titulos" >Resultados Busqueda </td>
				</tr>
				<tr >
					<td width="10%" class="titulos2" >C&oacute;digo</td>
					<td width="30%" class="titulos2" >Nombre</td>
					<td width="20%" class="titulos2" >Servicio</td>	  
					<td width="30%" class="titulos2" >Concepto Contable</td>
				</tr>
				<?php
				$vigusu=vigencia_usuarios($_SESSION['cedulausu']); 
				$oculto=$_POST['oculto'];
				//echo $oculto;
				//if($oculto!="")
				{
					$link = conectar_v7();

					$cond = " AND  (id LIKE'%".$_POST['numero']."%')";
					
					$maxVersion = ultimaVersionIngresosCCPET();

					$sqlr = "SELECT id,nombre,concepto,id_servicio FROM srvexoneraciones WHERE estado = 'S' $cond";
					
					$resp = mysqli_query($link,$sqlr);			

					$co = 'saludo1a';
					$co2 = 'saludo2';	
					
					while ($row = mysqli_fetch_row($resp)) 
					{
                        $sqlConceptoContable = "SELECT nombre FROM conceptoscontables WHERE codigo = '$row[2]' AND tipo = 'SE' ";
                        $respConceptoContable = mysqli_query($link, $sqlConceptoContable);
                        $rowConceptoContable = mysqli_fetch_row($respConceptoContable);

                        $sqlServicio = "SELECT nombre FROM srvservicios WHERE id = '$row[3]' ";
                        $respServicio = mysqli_query($link,$sqlServicio);
                        $rowServicio = mysqli_fetch_row($respServicio);

						echo 
						"<tr class='$co' onMouseOver=\"anterior=this.style.backgroundColor;this.style.backgroundColor='#40b3ff';\" onMouseOut=\"this.style.backgroundColor=anterior\" "; 
						
					    echo" onClick=\"javascript:ponprefijo('$row[0]','$row[1]')\"";
					 
						echo"
                            <td></td>
                            <td>$row[0]</td>
                            <td>$row[1]</td>
                            <td>$rowServicio[0]</td>
                            <td>$rowConceptoContable[0]</td>
						</tr> ";
						
						$aux=$co;
						$co=$co2;
						$co2=$aux;
					}
					$_POST['oculto']="";
				}
				?>
			</table>
		</div>
	</form>
</body>
</html> 
