<?php
	require_once 'PHPExcel/Classes/PHPExcel.php';
    include '/PHPExcel/Classes/PHPExcel/IOFactory.php';
	require "comun.inc";
	require "funciones.inc";
    ini_set('max_execution_time',99999999);
    header("Content-type: application/json");
	session_start();
	$linkbd=conectar_v7();

	$objPHPExcel = new PHPExcel();
	
	$objPHPExcel->getProperties()
		->setCreator("IDEAL10")
		->setLastModifiedBy("IDEAL10")
		->setTitle("REPORTE DE ACTIVOS")
		->setSubject("ACTI")
		->setDescription("ACTI")
		->setKeywords("ACTI")
		->setCategory("ACTIVOS FIJOS");
	$objPHPExcel->getActiveSheet()->mergeCells('A1:O1');
	$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A1', 'Reporte de activos fijos');
	$objFont=$objPHPExcel->getActiveSheet()->getStyle('A1')->getFont();
	$objFont->setName('Courier New');
	$objFont->setSize(15);
	$objFont->setBold(true);
	$objFont->setUnderline(PHPExcel_Style_Font::UNDERLINE_SINGLE);
	$objFont->getColor()->setARGB( PHPExcel_Style_Color::COLOR_BLACK);
	$objAlign=$objPHPExcel->getActiveSheet()->getStyle('A1')->getAlignment(); 
	$objAlign->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
	$objAlign->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
	$objPHPExcel-> getActiveSheet ()
		-> getStyle ("A2:O2")
		-> getFill ()
		-> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
		-> getStartColor ()
		-> setRGB ('A6E5F3');
	$objPHPExcel-> getActiveSheet ()
		-> getStyle ("A1")	
		-> getFill ()
		-> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
		-> getStartColor ()
		-> setRGB ('A6E5F3');
	$borders = array(
		'borders' => array(
			'allborders' => array(
				'style' => PHPExcel_Style_Border::BORDER_THIN,
				'color' => array('argb' => 'FF000000'),
			)
		),
	);

    $borders2 = array(
		'borders' => array(
			'allborders' => array(
				'style' => PHPExcel_Style_Border::BORDER_THIN,
				'color' => array('argb' => 'FF000000'),
			)
		),
        'font' => array(
            'bold' => true
        ),
	);

	$objPHPExcel->getActiveSheet()->getStyle('A2:O2')->applyFromArray($borders);
	$objPHPExcel->setActiveSheetIndex(0)
        ->setCellValue('A2', 'Placa')
        ->setCellValue('B2', 'Nombre')
        ->setCellValue('C2', 'Fecha creación')
        ->setCellValue('D2', 'Fecha compra')
        ->setCellValue('E2', 'Fecha activación')   
		->setCellValue('F2', 'Clase')   
        ->setCellValue('G2', 'Grupo')
        ->setCellValue('H2', 'Tipo')
		->setCellValue('I2', 'Dependencia')
		->setCellValue('J2', 'Ubicación')
		->setCellValue('K2', 'Responsable')
        ->setCellValue('L2', 'Valor activo')
        ->setCellValue('M2', 'Valor depreciado')
        ->setCellValue('N2', 'Valor correción')
        ->setCellValue('O2', 'Valor por depreciar');

	$i=3;
    
	for($ii=0;$ii<count ($_POST['placa']);$ii++)
	{
		$objPHPExcel->setActiveSheetIndex(0)
		->setCellValueExplicit ("A$i", $_POST['placa'][$ii], PHPExcel_Cell_DataType :: TYPE_STRING)
		->setCellValueExplicit ("B$i", $_POST['nombre'][$ii], PHPExcel_Cell_DataType :: TYPE_STRING)
		->setCellValueExplicit ("C$i", $_POST['fechaCreacion'][$ii], PHPExcel_Cell_DataType :: TYPE_STRING)
		->setCellValueExplicit ("D$i", $_POST['fechaCompra'][$ii], PHPExcel_Cell_DataType :: TYPE_STRING)
        ->setCellValueExplicit ("E$i", $_POST['fechaActivacion'][$ii], PHPExcel_Cell_DataType :: TYPE_STRING)
		->setCellValueExplicit ("F$i", $_POST['clase'][$ii], PHPExcel_Cell_DataType :: TYPE_STRING)
        ->setCellValueExplicit ("G$i", $_POST['grupo'][$ii], PHPExcel_Cell_DataType :: TYPE_STRING)
        ->setCellValueExplicit ("H$i", $_POST['tipo'][$ii], PHPExcel_Cell_DataType :: TYPE_STRING)
		->setCellValueExplicit ("I$i", $_POST['dependencia'][$ii], PHPExcel_Cell_DataType :: TYPE_STRING)
		->setCellValueExplicit ("J$i", $_POST['ubicacion'][$ii], PHPExcel_Cell_DataType :: TYPE_STRING)
		->setCellValueExplicit ("K$i", $_POST['responsable'][$ii], PHPExcel_Cell_DataType :: TYPE_STRING)
        ->setCellValueExplicit ("L$i", $_POST['valorActivoModulo'][$ii], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
        ->setCellValueExplicit ("M$i", $_POST['valorDepreciado'][$ii], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
        ->setCellValueExplicit ("N$i", $_POST['valorCorrecion'][$ii], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
        ->setCellValueExplicit ("O$i", $_POST['ValorXDepreciar'][$ii], PHPExcel_Cell_DataType :: TYPE_NUMERIC);
		
        if ($_POST['placa'][$ii] == "") {
            $objPHPExcel->getActiveSheet()->getStyle("A$i:O$i")->applyFromArray($borders2);
        }
        else {
            $objPHPExcel->getActiveSheet()->getStyle("A$i:O$i")->applyFromArray($borders);
        }
		
        $i++;
	}

	$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth("30");
	$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth("20");
	$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth("20");
    $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth("20");
    $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('K')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('L')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('N')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('O')->setAutoSize(true);

	$objPHPExcel->getActiveSheet()->setTitle('ACTI');
	//header('Content-Type: application/vnd.ms-excel');
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
	header('Content-Disposition: attachment;filename="reporteDeActivoFijos.xlsx"');
	header('Cache-Control: max-age=0');
	$objWriter=PHPExcel_IOFactory::createWriter($objPHPExcel,'Excel2007');
	$objWriter->save('php://output');
	exit;
?>