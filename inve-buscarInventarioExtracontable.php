<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require 'comun.inc';
	require 'funciones.inc';
	session_start();
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html lang=es>
	<head>
    <meta charset="utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1"/>
		<meta name="viewport" content="user-scalable=no">
		<title>:: IDEAL 10 - Almacen</title>
		<link href="favicon.ico" rel="shortcut icon"/>
		<link href="css/css2z.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
        <link href="css/tabs.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
		<link href="css/style.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" href="sweetalert2/dist/sweetalert2.min.css">
        <script type="text/javascript" src="css/programas.js"></script>
        <script src="sweetalert2/dist/sweetalert2.min.js"></script>
		<script src="vue/vue.min.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script src="css/calendario.js?<?php echo date('d_m_Y_h_i_s');?>"></script>

	</head>
	<body>
		<header>
			<table>
				<tr><script>barra_imagenes("inve");</script><?php cuadro_titulos();?></tr>
			</table>
		</header>

		<form name="form2" method="post" action="">
			<section id="myapp" v-cloak >
				<nav>
					<table>
						<tr><?php menu_desplegable("inve");?></tr>
						<tr>
							<td colspan="3" class="cinta">
								<img src="imagenes/add.png" v-on:click="location.href='inve-crearInventarioExtracontable'" class="mgbt" title="Nuevo">
								<img src="imagenes/guardad.png" @click="" title="Guardar"  class="mgbt">
								<img src="imagenes/busca.png" v-on:click="location.href='inve-buscarInventarioExtracontable'" class="mgbt" title="Buscar">
								<img src="imagenes/nv.png" onClick="mypop=window.open('inve-principal','',''); mypop.focus();" class="mgbt" title="Nueva Ventana">
								<a @click="pdf()"><img src="imagenes/print.png" title="Imprimir" class="mgbt"></a>
							</td>
						</tr>
					</table>
				</nav>

				<article>
					<div>
						<table class="inicio">
							<tr>
								<td class="titulos" colspan="8">.: Inventario extracontable</td>
                            	<td class="cerrar" style="width:4%" onClick="location.href='inve-principal.php'">Cerrar</td>
							</tr>
						</table>
					</div>

                    <div>
                        <table class="inicio">
                            <tr>
                                <td class="textonew01" style="width:3.5cm;">.: Responsable:</td>
                                <td style="width: 11%;">
                                    <input type="text" name="tercero" v-model="tercero" style="text-align: center;" @dblclick="showModalResponsables=true" class="colordobleclik" readonly>
                                </td>
                                <td>
                                    <input type="text" name="nomTercero" v-model="nomTercero" style="width: 100%;" readonly>
                                </td>

								<td class="textonew01" style="width:3.5cm;">.: Dependencia:</td>
								<td style="width: 11%;">
									<select v-model="dependencia" style="width: 90%; font-size: 10px; margin-top:4px">
										<option value="">Seleccionar dependencia</option>
										<option v-for="depen in dependencias" v-bind:value="depen[0]">
											{{ depen[0] }} - {{ depen[1] }}
										</option>
									</select>
								</td>

								<td class="textonew01" style="width:3.5cm;">.: Fecha inicial:</td>
								<td>
									<input type="date" v-model="fechaIni">
								</td>

								<td class="textonew01" style="width:3.5cm;">.: Fecha final:</td>
								<td>
									<input type="date" v-model="fechaFin">
								</td>

                                <td>
									<button type="button" @click="buscarInventario" class="botonflechaverde">Buscar</button>
								</td>
                        </table>

                        <div class="subpantalla" style='height:55vh; width:100%; overflow-x:hidden; resize: vertical;'>
                            <table>        
                                <thead>
                                    <tr>
                                        <th width="6%" class="titulosnew00">Fecha compra</th>
                                        <th width="10%" class="titulosnew00">Articulo</th>
                                        <th width="20%" class="titulosnew00" style="text-align:left;">Nombre articulo</th>
                                        <th width="10%" class="titulosnew00">Unidad medida</th>
										<th width="10%" class="titulosnew00">Responsable</th>
										<th width="10%" class="titulosnew00">Dependencia</th>
                                        <th width="10%"class="titulosnew00">Cantidad</th>
                                        <th width="10%" class="titulosnew00">Valor unitario</th>
                                        <th width="10%" class="titulosnew00">Valor total</th>
										<th width="5%" class="titulosnew00">Eliminar</th>
                                    </tr>
                                </thead>

                                <tbody>
                                    <tr v-for="(articulo,index) in listaArticulos" :key="index" v-on:dblclick="visualizar(articulo)" v-bind:class="(index % 2 ? 'saludo1a' : 'saludo2')" style="font: 100% sans-serif;">
                                        <td style="text-align:center; width:6%;">{{ articulo[1] }}</td>
                                        <td style="text-align:center; width:10%;">{{ articulo[2] }}</td>
                                        <td style="width:25%;">{{ articulo[3] }}</td>
                                        <td style="text-align:center; width:10%;">{{ articulo[4] }}</td>
                                        <td style="text-align:center; width:10%;">{{ articulo[5] }}</td>
                                        <td style="text-align:center; width:10%;">{{ articulo[6] }}</td>
										<td style="text-align:center; width:10%;">{{ articulo[7] }}</td>
                                        <td style="text-align:center; width:10%;">{{ formatonumero(articulo[8]) }}</td>
                                        <td style="text-align:center; width:10%;">{{ formatonumero(articulo[9]) }}</td>
										<td>
                                            <button type="button" class="btn btn-danger btn-sm m-1" @click="deleteArticulo(articulo, index)">
                                                Eliminar
                                            </button>
                                        </td>

										<input type='hidden' name='fecha[]' v-model="articulo[1]">	
										<input type='hidden' name='codArticulo[]' v-model="articulo[2]">	
										<input type='hidden' name='nomArticulo[]' v-model="articulo[3]">	
										<input type='hidden' name='unidad[]' v-model="articulo[4]">	
										<input type='hidden' name='responsable[]' v-model="articulo[5]">	
										<input type='hidden' name='dependencia[]' v-model="articulo[6]">	
										<input type='hidden' name='cantidad[]' v-model="articulo[7]">	
										<input type='hidden' name='valorUnitario[]' v-model="articulo[8]">	
										<input type='hidden' name='valorTotal[]' v-model="articulo[9]">	
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>

					<div id="cargando" v-if="loading" class="loading">
						<span>Cargando...</span>
					</div>
                    
                    <div v-show="showModalResponsables">
						<transition name="modal">
							<div class="modal-mask">
								<div class="modal-wrapper">
									<div class="modal-container">
										<table class="inicio ancho">
											<tr>
												<td class="titulos" colspan="2" >Listado terceros</td>
												<td class="cerrar" style="width:7%" @click="showModalResponsables = false">Cerrar</td>
											</tr>

                                            <tr>
                                                <td class="textonew01" style="text-align:center;">Cedula:</td>
                                                <td>
                                                    <input type="text" v-model="searchCedula" v-on:keyup="filtroCedulaResponsable" placeholder="Buscar por número de cedula" style="width: 100%;">
                                                </td>
                                            </tr>
										</table>
										<table class='tablamv'>
											<thead>
												<tr style="text-align:center;">
													<th class="titulosnew00" style="width: 20%;">Documento</th>
													<th class="titulosnew00">Nombre o razón social</th>
                                                    <th style="width: 1%;"></th>
												</tr>
											</thead>
											<tbody>
												<tr v-for="(tercero,index) in terceros" v-bind:class="index % 2 ? 'saludo1a' : 'saludo2'" v-on:click="seleccionaResponsable(tercero[0], tercero[1])">
													<td style="width:20%; text-align:center;"> {{ tercero[0] }} </td>
													<td style="text-align:left;"> {{ tercero[1] }} </td>
												</tr>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</transition>
					</div>  

				</article>
			</section>
		</form>

		<script src="node_modules/read-excel-file/bundle/read-excel-file.min.js"></script>
		<script src="node_modules/xlsx/dist/xlsx.full.min.js"></script>
        <script src="Librerias/vue/vue.min.js"></script>
		<script src="Librerias/vue/axios.min.js"></script>
		<script type="module" src="almacen/inventarioExtracontable/buscar/inve-buscarInventarioExtracontable.js"></script>
        
	</body>
</html>