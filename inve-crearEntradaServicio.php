<?php
	 header("Cache-control: no-cache, no-store, must-revalidate");
	 header("Content-Type: text/html;charset=utf8");
	 require 'comun.inc';
	 require 'funciones.inc';
	 session_start();
	 if(empty($_SESSION)){
		 header("location: index.php");
	 }
	 $linkbd = conectar_v7();
	 $linkbd -> set_charset("utf8");
	 cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	 date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html lang=es>
	<head>
		<meta charset="utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1"/>
		<meta name="viewport" content="user-scalable=no">
		<title>:: IDEAL 10 - Almacen</title>
		<link href="favicon.ico" rel="shortcut icon"/>
		<link href="css/css2z.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
        <link href="css/tabs.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
		<link href="css/style.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
		<script src="vue/vue.min.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script src="css/calendario.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script src="sweetalert2/dist/sweetalert2.min.js"></script>
		<link rel="stylesheet" href="sweetalert2/dist/sweetalert2.min.css">

		<style>
			input[type=number]::-webkit-inner-spin-button,
			input[type=number]::-webkit-outer-spin-button { 
				-webkit-appearance: none; 
				margin: 0; 
			}
			input[type=number]{
				text-align:right;
				box-sizing: border-box;
				font: 120% sans-serif;
				/* width: 100% !important; */
			}
			[v-cloak]{display : none;}

			.centrarSelect {

				height: 30px !important;
				text-align-last:center !important;
			}

			.aumentarTamaño {

				font-size:15px;
				text-align: center !important;
			}

            .tamano01 {
                text-align: center !important;
            }

			.checkbox-wrapper-31:hover .check {
                stroke-dashoffset: 0;
            }
        
            .checkbox-wrapper-31 {
                position: relative;
                display: inline-block;
                width: 40px;
                height: 40px;
            }
            .checkbox-wrapper-31 .background {
                fill: #ccc;
                transition: ease all 0.6s;
                -webkit-transition: ease all 0.6s;
            }
            .checkbox-wrapper-31 .stroke {
                fill: none;
                stroke: #fff;
                stroke-miterlimit: 10;
                stroke-width: 2px;
                stroke-dashoffset: 100;
                stroke-dasharray: 100;
                transition: ease all 0.6s;
                -webkit-transition: ease all 0.6s;
            }
            .checkbox-wrapper-31 .check {
                fill: none;
                stroke: #fff;
                stroke-linecap: round;
                stroke-linejoin: round;
                stroke-width: 2px;
                stroke-dashoffset: 22;
                stroke-dasharray: 22;
                transition: ease all 0.6s;
                -webkit-transition: ease all 0.6s;
            }
            .checkbox-wrapper-31 input[type=checkbox] {
                position: absolute;
                width: 100%;
                height: 100%;
                left: 0;
                top: 0;
                margin: 0;
                opacity: 0;
                -appearance: none;
            }
            .checkbox-wrapper-31 input[type=checkbox]:hover {
                cursor: pointer;
            }
            .checkbox-wrapper-31 input[type=checkbox]:checked + svg .background {
                fill: #6cbe45;
            }
            .checkbox-wrapper-31 input[type=checkbox]:checked + svg .stroke {
                stroke-dashoffset: 0;
            }
            .checkbox-wrapper-31 input[type=checkbox]:checked + svg .check {
                stroke-dashoffset: 0;
            }
		</style>
	</head>
	<body>
		<header>
			<table>
				<tr><script>barra_imagenes("inve");</script><?php cuadro_titulos();?></tr>
			</table>
		</header>

		<form name="form2" method="post" action="">
			<section id="myapp" v-cloak >
				<nav>
					<table>
						<tr><?php menu_desplegable("inve");?></tr>
						<tr>
							<td colspan="3" class="cinta">
								<img src="imagenes/add.png" v-on:click="location.href='inve-crearEntradaServicio.php'" class="mgbt1" title="Nuevo">
								<img src="imagenes/guarda.png" v-on:click="guardar" title="Guardar"  class="mgbt">
								<img src="imagenes/busca.png" v-on:click="location.href='inve-buscarEntradaServicio.php'" class="mgbt" title="Buscar">
								<img src="imagenes/nv.png" onClick="mypop=window.open('inve-principal.php','',''); mypop.focus();" class="mgbt" title="Nueva Ventana">
							</td>
						</tr>
					</table>
				</nav>

				<article>
					<div>
						<table class="inicio grande">
							<tr>
								<td class="titulos" colspan="8">.: Entrada por servicio</td>
                            	<td class="cerrar" style="width:4%" onClick="location.href='inve-principal.php'">Cerrar</td>
							</tr>
						</table>
					</div>

                    <div class="tabsmeci" style="height:67.5%; width:99.6%">
                        <div class="tab" >
                            <input type="radio" id="tab-1" name="tabgroup1"  v-model="tabgroup1"  value="1" >
                            <label for="tab-1">Entrada servicio</label>
                            <div class="content" style="overflow:hidden;">

                                <table class="inicio">
                                    <tr>
                                        <td class="titulos" colspan="8">.: Crear Entrada Por Servicio</td>
                                    </tr>

                                    <tr>
                                        <td class="textonew01" style="width: 8%;">Consecutivo entrada servicio:</td>
                                        <td style="width: 10%;">
                                            <input type="text" v-model="consecutivo" style="width: 100%; height: 30px; text-align:center;" readonly>
                                        </td>

                                        <td class="textonew01" style="width: 10%;">Fecha: </td>
                                        <td style="width: 10%;">
                                            <input type="text" name="fecha" id="fecha" onchange="" maxlength="10" onKeyUp="return tabular(event,this)" onKeyDown="mascara(this,'/',patron,true)" title="DD/MM/YYYY" style="text-align:center;" autocomplete="off" class="colordobleclik" ondblclick="displayCalendarFor('fecha');">
                                        </td>

                                        <td class="textonew01" style="width: 8%;">Descripción: </td>
                                        <td>
                                            <textarea v-model="descripcion" placeholder="Detalle entrada de servicio" style="width: 100%;"></textarea>
                                        </td>

										<td class="textonew01" style="width: 8%;">Certifica almacenista: </td>
										
										<td style="text-align:center;">    
											<div class="checkbox-wrapper-31">
												<input type="checkbox" v-model="certificaAlm"/>
												<svg viewBox="0 0 35.6 35.6">
													<circle class="background" cx="17.8" cy="17.8" r="17.8"></circle>
													<circle class="stroke" cx="17.8" cy="17.8" r="14.37"></circle>
													<polyline class="check" points="11.78 18.12 15.55 22.23 25.17 12.87"></polyline>
												</svg>
											</div>        
										</td>
										
                                    </div>
										
                                    </tr>
                                </table>

                                <table class="inicio">
                                    <tr>
                                        <td class="titulos" colspan="6">.: Gestión RP</td>
                                    </tr>

                                    <tr>
                                        <td class="textonew01" style="width: 8%;">RP: </td>
                                        <td style="width: 10%;">
                                            <input type="text" v-model="rp" v-on:dblclick="despliegaModalRP" class="colordobleclik" readonly>
                                        </td>

                                        <td class="textonew01" style="width: 8%;">Descripción: </td>
                                        <td colspan="6">
                                            <textarea v-model="descripcionRP" placeholder="Detalle RP" style="width: 100%;" readonly></textarea>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td class="textonew01" style="width: 8%;">Fecha RP: </td>
                                        <td>
                                            <input type="text" v-model="fechaRP" style="text-align: center;" readonly>
                                        </td>

                                        <td class="textonew01" style="width: 8%;">Valor RP: </td>
                                        <td style="width: 10%;">
                                            <input type="text" v-model="formatonumero(valorRP)" readonly>
                                        </td>

                                        <td class="textonew01" style="width: 8%;">Saldo RP: </td>
                                        <td >
                                            <input type="text" v-model="formatonumero(saldoRP)" readonly>
                                        </td>
                                    </tr>
                                </table>

                                <div class='subpantalla' style='height:38vh; width:99.2%; margin-top:0px; overflow-x: hidden;'>
                                    <table class=''>
                                        <thead>
                                            <tr style="text-align:Center;">
                                                <th class="titulosnew00" style="width:7%;">Vig del gasto</th>
                                                <th class="titulosnew00" style="width:7%;">Sec presupuestal</th>
                                                <th class="titulosnew00" style="width:5%;">Bpim</th>
                                                <th class="titulosnew00" style="width:7%;">Cuenta</th>
                                                <th class="titulosnew00" style="width:7%;">Fuente</th>
                                                <th class="titulosnew00" style="width:7%;">Producto/servicio</th>
                                                <th class="titulosnew00" style="width:7%;">Indicador producto</th>
                                                <th class="titulosnew00" style="width:7%;">Politica pública</th>
                                                <th class="titulosnew00" style="width:5%;">Medio de pago</th>
                                                <th class="titulosnew00" style="width:7%;">Valor</th>
                                                <th class="titulosnew00" style="width:7%;">Saldo</th>
                                                <th class="titulosnew00">Valor Entrada</th>
                                            </tr>
                                        </thead>
                                        <tbody style="overflow-y: hidden !important;">
                                            <tr v-for="(detalle, index) in detallesRP" v-bind:class="index % 2 ? 'saludo1a' : 'saludo2'" style='text-rendering: optimizeLegibility; cursor: pointer !important;'>
                                                <td style="width:7%; font: 120% sans-serif; padding-left:5px; text-align:center;">{{ detalle[1] }}</td>
                                                <td style="width:7%; font: 120% sans-serif; padding-left:5px; text-align:center;">{{ detalle[3] }}</td>
                                                <td style="width:5%; font: 120% sans-serif; padding-left:5px; text-align:center;">{{ detalle[4] }}</td>
                                                <td style="width:7%; font: 120% sans-serif; padding-left:5px; text-align:center;">{{ detalle[6] }}</td>
                                                <td style="width:7%; font: 120% sans-serif; padding-left:5px; text-align:center;">{{ detalle[8] }}</td>
                                                <td style="width:7%; font: 120% sans-serif; padding-left:5px; text-align:center;">{{ detalle[10] }}</td>
                                                <td style="width:7%; font: 120% sans-serif; padding-left:5px; text-align:center;">{{ detalle[12] }}</td>
                                                <td style="width:7%; font: 120% sans-serif; padding-left:5px; text-align:center;">{{ detalle[14] }}</td>
                                                <td style="width:5%; font: 120% sans-serif; padding-left:5px; text-align:center;">{{ detalle[15] }}</td>
                                                <td style="width:7%; font: 120% sans-serif; padding-left:5px; text-align:center;">{{ formatonumero(detalle[16]) }}</td>
                                                <td style="width:7%; font: 120% sans-serif; padding-left:5px; text-align:center;">{{ formatonumero(detalle[17]) }}</td>
                                                <td style="font: 120% sans-serif; padding-left:5px; text-align:center;">
                                                    <input type="number" v-model="valorEntrada[index]" v-on:keyup="validaValorMax(index)" style="text-align: center; width: 95%;">
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>

                        <div class="tab" >
                            <input type="radio" id="tab-2" name="tabgroup1" v-model="tabgroup1" value="2" >
                            <label for="tab-2">Entrada articulos automatica</label>
                            <div class="content" style="overflow:hidden;">
                                <table class="inicio grande">
                                    <tr>
                                        <td class="titulos" colspan="8">.: Articulos entrada por servicio</td>
                                    </tr>

									<tr>
										<td class="tamano01" style="width: 8%;">Seleccionar archivo:</td>
										<td>
											<input type="file" id="archivoExcel" @change="subirExcel">
										</td>

										<td>
											<em class="botonflechaverde" @click="bajarExcel">Descargar formato</em>
										</td>
									</tr>
                                </table>

                                <div class='subpantalla' style='height:60vh; width:99.2%; margin-top:0px; overflow:hidden'>
                                    <table class='tablamv'>
                                        <thead>
                                            <tr style="text-align:Center;">
                                                <th class="titulosnew00" style="width:15%;">Codigo articulo</th>
                                                <th class="titulosnew00">Nombre Articulo</th>
                                                <th class="titulosnew00" style="width:5%;">Cantidad</th>
                                                <th class="titulosnew00" style="width:15%;">Valor</th>
												<th class="titulosnew00" style="width:10%;">Marca</th>
												<th class="titulosnew00" style="width:10%;">Modelo</th>
												<th class="titulosnew00" style="width:10%;">Serial</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr v-for="(detalle, index) in detalleArticulo" v-bind:class="index % 2 ? 'saludo1a' : 'saludo2'" style='text-rendering: optimizeLegibility; cursor: pointer !important;'>
												<td style="width:15%; font: 120% sans-serif; padding-left:5px; text-align:center;">
													{{ detalle[0] }}
												</td>

												<td style="font: 120% sans-serif; padding-left:5px; text-align:left;">
													{{ detalle[1] }}
												</td>

												<td style="width:5%; font: 120% sans-serif; padding-left:5px; text-align:right;">
													{{ detalle[2] }}
												</td>

												<td style="width:15%; font: 120% sans-serif; padding-left:5px; text-align:right;">
													{{ formatonumero(detalle[3]) }}
												</td>

												<td style="width:10%; font: 120% sans-serif; padding-left:5px; text-align:right;">
													{{ detalle[4] }}
												</td>

												<td style="width:10%; font: 120% sans-serif; padding-left:5px; text-align:right;">
													{{ detalle[5] }}
												</td>

												<td style="width:10%; font: 120% sans-serif; padding-left:5px; text-align:right;">
													{{ detalle[6] }}
												</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>

						<div class="tab" >
                            <input type="radio" id="tab-3" name="tabgroup1" v-model="tabgroup1" value="3" >
                            <label for="tab-3">Entrada articulos manual</label>
                            <div class="content" style="overflow:hidden;">
                                <table class="inicio grande">
                                    <tr>
                                        <td class="titulos" colspan="8">.: Articulos entrada por servicio</td>
                                    </tr>

									<tr>
										<td class="tamano01" style="width:10%">Digite la cantidad de articulos que requiere: </td>
                                        <td style="width: 15%;">
                                            <input type="number" v-model="cantidadArticulos" style="text-align: center;" v-on:change="llenarCantidadArticulos">
                                        </td>
									</tr>
                                </table>

								<div class='subpantalla' style='height:60vh; width:99.2%; margin-top:0px; overflow:hidden'>
                                    <table class='tablamv'>
                                        <thead>
                                            <tr style="text-align:Center;">
                                                <th class="titulosnew00" style="width:15%;">Codigo articulo</th>
                                                <th class="titulosnew00">Nombre Articulo</th>
                                                <th class="titulosnew00" style="width:15%;">Cantidad</th>
                                                <th class="titulosnew00" style="width:15%;">Valor</th>
												<th class="titulosnew00" style="width:10%;">Marca</th>
												<th class="titulosnew00" style="width:10%;">Modelo</th>
												<th class="titulosnew00" style="width:10%;">Serial</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr v-for="(detalle, index) in detalleArticuloManual" v-bind:class="index % 2 ? 'saludo1a' : 'saludo2'" style='text-rendering: optimizeLegibility; cursor: pointer !important;'>
												<td style="width:15%; font: 120% sans-serif; padding-left:5px; text-align:center;">
													<input type="text" v-model="codigoArticulos[index]" v-on:dblclick="despliegaArticulos(index)" style="text-align: center; width: 95%;" class="colordobleclik">
												</td>

												<td style="font: 120% sans-serif; padding-left:5px; text-align:center;">
													<input type="text" v-model="nombreArticulos[index]" style="text-align: center; width: 95%;">
												</td>

												<td style="width:15%; font: 120% sans-serif; padding-left:5px; text-align:center;">
													<input type="number" v-model="cantidad[index]" v-on:change="validaCantidad(index)" style="text-align: center; width: 95%;">
												</td>

												<td style="width:15%; font: 120% sans-serif; padding-left:5px; text-align:center;">
													<input type="number" v-model="valorArticulos[index]" v-on:change="validaValor(index)" style="text-align: center; width: 95%;">
												</td>

												<td style="width:10%; font: 120% sans-serif; padding-left:5px; text-align:center;">
													<input type="text" v-model="marca[index]" style="text-align: center; width: 95%;">
												</td>

												<td style="width:10%; font: 120% sans-serif; padding-left:5px; text-align:center;">
													<input type="text" v-model="modelo[index]" style="text-align: center; width: 95%;">
												</td>

												<td style="width:10%; font: 120% sans-serif; padding-left:5px; text-align:center;">
													<input type="text" v-model="serial[index]" style="text-align: center; width: 95%;">
												</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>

					<div id="cargando" v-if="loading" class="loading">
						<span>Cargando...</span>
					</div>

                    <div v-show="showRP">
						<transition name="modal">
							<div class="modal-mask">
								<div class="modal-wrapper">
									<div class="modal-container">
										<table class="inicio ancho">
											<tr>
												<td class="titulos" colspan="2" >Listado de registros presupuestales</td>
												<td class="cerrar" style="width:7%" @click="showRP = false">Cerrar</td>
											</tr>
											<tr>
												<td class="tamano01" style="width:3cm">RP:</td>
												<td>
													<input type="text" v-model="searchRP.keywordRP" v-on:keyup="searchMonitorRP" class="form-control" placeholder="Buscar numero RP" style="width:100%" />
												</td>
											</tr>
										</table>
										<table class='tablamv'>
											<thead>
												<tr style="text-align:center;">
													<th class="titulosnew02" >Resultados Busqueda</th>
												</tr>
												<tr style="text-align:center;">
													<th class="titulosnew00" style="width:10%;">Código RP</th>
													<th class="titulosnew00" style="width:12%;">Fecha</th>
                                                    <th class="titulosnew00">Detalle</th> 
                                                    <th class="titulosnew00" style="width:15%;">Valor</th> 
                                                    <th class="titulosnew00" style="width:15%;">Saldo Disponible</th> 
												</tr>
											</thead>
											<tbody>
												<tr v-for="(RP,index) in cabeceraRP" v-on:click="seleccionaRP(RP)" v-bind:class="index % 2 ? 'saludo1a' : 'saludo2'" style='text-rendering: optimizeLegibility; cursor: pointer !important;'>
													<td style="font: 120% sans-serif; padding-left:10px; width:10%; text-align:center;">{{ RP[0] }}</td>
                                                    <td style="font: 120% sans-serif; padding-left:10px; width:12%; text-align:center;">{{ RP[1] }}</td>
													<td style="font: 120% sans-serif; padding-left:10px; text-align:center;">{{ RP[2] }}</td>
                                                    <td style="font: 120% sans-serif; padding-left:10px; width:15%; text-align:center;">{{ formatonumero(RP[3]) }}</td>
                                                    <td style="font: 120% sans-serif; padding-left:10px; width:15%; text-align:center;">{{ formatonumero(RP[4]) }}</td>
												</tr>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</transition>
					</div>

					<div v-show="showArticulos">
						<transition name="modal">
							<div class="modal-mask">
								<div class="modal-wrapper">
									<div class="modal-container">
										<table class="inicio ancho">
											<tr>
												<td class="titulos" colspan="2" >Articulos</td>
												<td class="cerrar" style="width:7%" @click="showArticulos = false">Cerrar</td>
											</tr>
											<tr>
												<td class="tamano01" style="width:3cm">Articulo:</td>
												<td>
													<input type="text" v-model="searchArt.keywordArt" v-on:keyup="searchMonitoArticulo" class="form-control" placeholder="Buscar por código de articulo" style="width:100%" />
												</td>
											</tr>
										</table>
										<table class='tablamv'>
											<thead>
												<tr style="text-align:center;">
													<th class="titulosnew02" >Resultados Busqueda</th>
												</tr>
												<tr style="text-align:center;">
													<th class="titulosnew00" style="width:15%;">Código</th>
													<th class="titulosnew00">Nombre</th>
                                                    <th class="titulosnew00" style="width:15%;">Codigo unspsc</th> 
												</tr>
											</thead>
											<tbody>
												<tr v-for="(articulo,index) in articulos" v-on:click="seleccionaArticulo(articulo)" v-bind:class="index % 2 ? 'saludo1a' : 'saludo2'" style='text-rendering: optimizeLegibility; cursor: pointer !important;'>
													<td style="font: 120% sans-serif; padding-left:10px; width:15%; text-align:center;">{{ articulo[0] }}</td>
                                                    <td style="font: 120% sans-serif; padding-left:10px;">{{ articulo[1] }}</td>
													<td style="font: 120% sans-serif; padding-left:10px; width:15%; text-align:center;">{{ articulo[2] }}</td>
												</tr>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</transition>
					</div>

				</article>
			</section>
		</form>

		<script src="node_modules/read-excel-file/bundle/read-excel-file.min.js"></script>
		<script src="node_modules/xlsx/dist/xlsx.full.min.js"></script>
		<script src="Librerias/vue/vue.min.js"></script>
		<script src="Librerias/vue/axios.min.js"></script>
		<script src="almacen/EntradaServicio/crear/inve-crearEntradaServicio.js?"></script>
        
	</body>
</html>