<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=uf8");
	require "comun.inc";
	require "funciones.inc";
	require "funcionesSP.inc.php";
	session_start();
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
    titlepag();
?>
<!DOCTYPE >
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
		<title>:: IDEAL 10 - Servicios Publicos</title>
		<link href="css/css2n.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/css3n.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/cssSP.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<script type="text/javascript" src="css/calendario.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
        <script type="text/javascript" src="jquery-1.11.0.min.js"></script> 
		<script type="text/javascript" src="css/programas.js"></script>
        <script src="sweetalert2/dist/sweetalert2.min.js"></script>
		<link rel="stylesheet" href="sweetalert2/dist/sweetalert2.min.css">
		<script type="text/javascript" src="css/programas.js"></script>
		<script>
            function generarInforme() {
                
                var fechaInicial = document.getElementById('fechaini').value;
                var fechaFinal = document.getElementById('fechafin').value;

                if (fechaInicial != '' && fechaFinal != '') {

                    if (fechaInicial <= fechaFinal) {

                        document.form2.oculto.value='2';
					    document.form2.submit();
                    } 
                    else {
                        Swal.fire({
                            icon: 'warning',
                            title: 'Fecha inicial debe ser menor a la fecha final'
                        })
                    }
                } 
                else {
                    Swal.fire({
                        icon: 'warning',
                        title: 'Ingrese ambas fechas'
                    })
                }
            }

            function direccionaComprobante(idCat,tipo_compro,num_compro) {
				
				window.open("cont-buscacomprobantes.php?idCat="+idCat+"&tipo_compro="+tipo_compro+"&num_compro="+num_compro);
			}

			function reflejar(diferencia, codigoComprobante) {
				
				// if (diferencia != 0) {
					
				// 	Swal.fire({
				// 		icon: 'question',
				// 		title: 'Seguro que quieres reflejar?',
				// 		showDenyButton: true,
				// 		confirmButtonText: 'Reflejar',
				// 		denyButtonText: 'Cancelar',
				// 		}).then((result) => {
				// 		if (result.isConfirmed) {
				// 			document.form2.oculto.value = '3';
				// 			document.form2.diferencia.value = diferencia;
				// 			document.form2.codigoComprobante.value = codigoComprobante;
				// 			document.form2.submit();
				// 		} else if (result.isDenied) {
				// 			Swal.fire('reflejar cancelado', '', 'info')
				// 		}
				// 	})
				// } 
			}

			function reflejarMasivo() 
			{
				var fechaInicial = document.getElementById('fechaini').value;
                var fechaFinal = document.getElementById('fechafin').value;

				if (fechaInicial != '' && fechaFinal != '') {

					// Swal.fire({
					// 	icon: 'question',
					// 	title: 'Seguro que quieres hacer reflejo masivo?',
					// 	showDenyButton: true,
					// 	confirmButtonText: 'Reflejar todos',
					// 	denyButtonText: 'Cancelar',
					// 	}).then((result) => {
					// 	if (result.isConfirmed) {
					// 		document.form2.oculto.value = '4';
					// 		document.form2.submit();
					// 	} else if (result.isDenied) {
					// 		Swal.fire('reflejo masivo cancelado', '', 'info')
					// 	}
					// })
				}
				else {

					Swal.fire({
                        icon: 'warning',
                        title: 'Ingrese ambas fechas'
                    })
				}
			}

            $(window).load(function () {
				$('#cargando').hide();
			});

            function excel()
			{
				document.form2.action="serv-excel-facturacion-cont-doce.php";
				document.form2.target="_BLANK";
				document.form2.submit(); 
				document.form2.action="";
				document.form2.target="";
			}
		</script>

		<?php

			$scrtop = @ $_GET['scrtop'];
			if($scrtop == "") $scrtop=0;
			echo"<script>
					window.onload=function()
					{
						$('#divdet').scrollTop(".$scrtop.")
					}
				</script>";
		?>
	</head>
	<body>
		<IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
		<span id="todastablas2"></span>
		<table>
			<tr><script>barra_imagenes("cont");</script><?php cuadro_titulos();?></tr>	 
			<tr><?php menu_desplegable("cont");?></tr>
			<tr>
				<td colspan="3" class="cinta">
					<a href="#" class="mgbt"><img src="imagenes/add.png"/></a>
					<a class="mgbt"><img src="imagenes/guardad.png" title="Guardar"/></a>
					<a href="#" class="mgbt"><img src="imagenes/busca.png" title="Buscar"/></a>
					<a onclick="mypop=window.open('plan-agenda.php','','');mypop.focus()" class="mgbt"><img src="imagenes/agenda1.png" title="Agenda" /></a>
					<a onclick="mypop=window.open('cont-principal.php','','');mypop.focus();" class="mgbt"><img src="imagenes/nv.png" title="Nueva ventana"></a>
					<a onclick="mypop=window.open('<?php echo $url2; ?>','','');mypop.focus();" class="mgbt"><img src="imagenes/duplicar_pantalla.png" title="Duplicar pesta&ntilde;a"></a>
                    <a onclick="excel()"><img src="imagenes/excel.png" title="Excel" class="mgbt"></a>
                </td>
                    
				</td>
			</tr>
		</table>

		<div id="bgventanamodalm" class="bgventanamodalm">
			<div id="ventanamodalm" class="ventanamodalm">
				<IFRAME src="" name="ventanam" marginWidth=0 marginHeight=0 frameBorder=0 id="ventanam" frameSpacing=0 style=" width:700px; height:130px; top:200; overflow:hidden;"></IFRAME>
			</div>
		</div>

		<form name="form2" method="post" action="">
			<table class="inicio ancho">
				<tr>
					<td class="titulos" colspan="7">Comparación facturcion servicios públicos - contabilidad</td>
					<td class="cerrar" style="width:7%" onClick="location.href='serv-principal.php'">Cerrar</td>
				</tr>

                <tr>
                    <td  class="tamano01" >Fecha Inicial: </td>
					<td>
						<input type="search" name="fechaini" id="fechaini" title="DD/MM/YYYY" value="<?php echo $_POST['fechaini']; ?>" onKeyUp="return tabular(event,this)" onKeyDown="mascara(this,'/',patron,true)" maxlength="10" class="colordobleclik" placeholder="DD/MM/YYYY" onDblClick="displayCalendarFor('fechaini');" autocomplete="off" onChange="" style="text-align: center;" />
					</td>

					<td class="tamano01" >Fecha Final: </td>
					<td>
						<input type="search" name="fechafin"  id="fechafin" title="DD/MM/YYYY"  value="<?php echo $_POST['fechafin']; ?>" onKeyUp="return tabular(event,this) " onKeyDown="mascara(this,'/',patron,true)" maxlength="10" class="colordobleclik" placeholder="DD/MM/YYYY" onDblClick="displayCalendarFor('fechafin');" autocomplete="off" onChange="" style="text-align: center;" />
					</td> 

					<td style="padding-bottom:0px">
                        <em class="botonflechaverde" id="filtro" onclick="generarInforme();">Generar Informe</em>
                    </td>
				</tr>

				<tr>
					<?php
						echo"
							<td>
								<div id='titulog1' style='display:none; float:left'></div>
								<div id='progreso' class='ProgressBar' style='display:none; float:left'>
									<div class='ProgressBarText'><span id='getprogress'></span>&nbsp;% </div>
									<div id='getProgressBarFill'></div>
								</div>
							</td>";
					?>
				</tr>
			</table>

            <div class="subpantalla" style="height:69%; width:99.6%; margin-top:0px; overflow-x:hidden">
                <table class='inicio'>
                    <tr>
                        <td colspan='90' class='titulos'>Resultados Busqueda:</td>
                    </tr>
                    <tr class='titulos ' style='text-align:center;'>
                        <td colspan="4">Servicios Públicos</td>
                        <td colspan="2">Contabilidad</td>
						<td colspan="2">Herramientas</td>
                    </tr>
                    <tr class="titulos2" style='text-align:center;'>
                        <td>Numero de factura</td>
                        <td>Fecha</td>
                        <td>Cod usuario</td>
                        <td>Valor Facturado</td>
                        <td>Valor contabilidad</td>
                        <td>Diferencia</td>
                        <td>Visualizar</td>
						<td>Reflejar <img src='imagenes/reflejar1.png' class='icoop' title='Reflejar' onclick="reflejarMasivo();"/></td>
                    </tr>

                    <?php 
						$co="zebra1";
						$co2="zebra2";
						$valorTotal = 0;

						$codigoComprobante = 29;
						$sqlIdCat = "SELECT id_cat FROM tipo_comprobante WHERE codigo = '$codigoComprobante'";
						$rowIdCat = mysqli_fetch_assoc(mysqli_query($linkbd, $sqlIdCat));
				

						if ($_POST['fechaini'] != '') {

							$fec = explode("/", $_POST['fechaini']);
							$fechaInicial = $fec[2].'-'.$fec[1].'-'.$fec[0];
						}
						else {
							$fechaInicial = "";
						}

						if ($_POST['fechafin'] != '') {

							$fec = explode("/", $_POST['fechafin']);
							$fechaFinal = $fec[2].'-'.$fec[1].'-'.$fec[0];
						}
						else {
							$fechaFinal = "";
						}
						

						if ($fechaInicial != '' && $fechaFinal != '') {

							$sqlReciboCaja = "SELECT numero_facturacion, SUM(credito), id_cliente, fecha_movimiento FROM srvdetalles_facturacion WHERE estado = 'S' AND fecha_movimiento BETWEEN '$fechaInicial' AND '$fechaFinal' AND tipo_movimiento = '101' AND (id_tipo_cobro = '1' OR id_tipo_cobro = '2') GROUP BY (numero_facturacion) ORDER BY numero_facturacion ASC";
						}
						else {
							$sqlReciboCaja = "";
						}
						
						$resReciboCaja = mysqli_query($linkbd, $sqlReciboCaja);
						while ($rowReciboCaja = mysqli_fetch_assoc($resReciboCaja)) {

							$valorTotal += $rowReciboCaja['SUM(credito)'];
							$valorTotal2 = $rowReciboCaja['SUM(credito)'];

							$sqlComprobanteCab = "SELECT fecha FROM comprobante_cab WHERE tipo_comp = $codigoComprobante AND numerotipo = $rowReciboCaja[numero_facturacion]";
							$rowComprobanteCab = mysqli_fetch_assoc(mysqli_query($linkbd, $sqlComprobanteCab));

							$sqlComprobanteDet = "SELECT SUM(valdebito), SUM(valcredito) FROM comprobante_det WHERE tipo_comp = $codigoComprobante AND numerotipo = $rowReciboCaja[numero_facturacion] AND LEFT(cuenta,2)='13'";
							$rowComprobanteDet = mysqli_fetch_assoc(mysqli_query($linkbd, $sqlComprobanteDet));

							$valorBanco = $rowComprobanteDet['SUM(valdebito)'];

							$sqlUsuario = "SELECT cod_usuario FROM srvclientes WHERE id = $rowReciboCaja[id_cliente]";
							$rowUsuario = mysqli_fetch_assoc(mysqli_query($linkbd, $sqlUsuario));

							$diferencia = $rowReciboCaja['SUM(credito)'] - $valorBanco;

							$estilo = "";

							if ($diferencia <= -100 || $diferencia >= 100) {
								$estilo = "background-color: #FE5050;";
							}

                            echo "
									<input type='hidden' name='numeroFactura[]' value='$rowReciboCaja[numero_facturacion]'>
									<input type='hidden' name='fecha[]' value='$rowReciboCaja[fecha_movimiento]'>
									<input type='hidden' name='codUsuario[]' value='$rowUsuario[cod_usuario]'>
									<input type='hidden' name='valorFactura[]' value='$valorTotal2'>
								";
                   
                    ?>
							<tr class='<?php echo $co ?>' style="text-transform:uppercase; text-align:center;">
								<td> <?php echo $rowReciboCaja['numero_facturacion'] ?> </td>
								<td> <?php echo date('d-m-Y',strtotime($rowReciboCaja['fecha_movimiento'])) ?> </td>
								<td> <?php echo $rowUsuario['cod_usuario'] ?> </td>
								<td> $ <?php echo number_format(round($valorTotal2),2,',','.') ?> </td>
								<td> $ <?php echo number_format($valorBanco,2,',','.') ?> </td>
								<td style="<?php echo $estilo; ?>"> $ <?php echo number_format(round($diferencia),2,',','.') ?> </td>
								<td style='text-align:center;'>
									<img src='imagenes/find02.png' class='icoop' title='Visualizar' onclick="direccionaComprobante(<?php echo $rowIdCat['id_cat'] ?>, <?php echo $codigoComprobante ?>, <?php echo $rowReciboCaja['numero_facturacion'] ?>);" />
								</td>
								<td style='text-align:center;'>
									<img src='imagenes/reflejar1.png' class='icoop' title='Reflejar' onclick="reflejar(<?php echo $diferencia ?>, <?php echo $rowReciboCaja['numero_facturacion'] ?>)"/>
								</td>
							</tr>
							
                    <?php
							$aux=$co;
							$co=$co2;
							$co2=$aux;
						}
                    ?>
							<tr>
								<td colspan='2'></td>
								<td>Totales:</td>
								<td class='saludo3'  style='text-align:right;'><?php echo "$" . number_format(round($valorTotal),2,',','.') ?><input type='hidden' name='valorTotal' value="<?php echo $valorTotal ?>"></td>

							</tr>
                </table>
            </div>

            <input type="hidden" name="oculto" id="oculto" value="1" />
			<input type="hidden" name="diferencia" id="diferencia" value="0" />
			<input type="hidden" name="codigoComprobante" id="codigoComprobante" value="0">

			<?php 
				if ($_POST['oculto'] == '3') {
					
					if ($_POST['codigoComprobante'] != "0") {

						$validacion01 = 1;

						$sqlsp = "SELECT fecha_recaudo, concepto, documento, YEAR(fecha_recaudo) FROM srv_recaudo_factura WHERE codigo_recaudo = '$_POST[codigoComprobante]'";
						$rowsp = mysqli_fetch_assoc(mysqli_query($linkbd, $sqlsp));

						$vigencia = $rowsp['YEAR(fecha_recaudo)'];

						$sqlcont = "SELECT * FROM comprobante_cab WHERE numerotipo = '$_POST[codigoComprobante]' AND tipo_comp = '$codigoComprobante'";
						$rowcont = mysqli_fetch_assoc(mysqli_query($linkbd, $sqlcont));
						$validacion01 = mysqli_num_rows(mysqli_query($linkbd, $sqlcont));

						if ($validacion01 == 0) {
							
							$sqlComprobanteCab = "INSERT INTO comprobante_cab (numerotipo, tipo_comp, fecha, concepto, estado) VALUES ('$_POST[codigoComprobante]', $codigoComprobante, '$rowsp[fecha_recaudo]', '$rowsp[concepto]', '1')";
							mysqli_query($linkbd, $sqlComprobanteCab);
						}

						$sqlServicio = "SELECT cargo_fijo_u, cc, nombre FROM srvservicios WHERE id = 1";
						$resServicio = mysqli_query($linkbd,$sqlServicio);
						$rowServicio = mysqli_fetch_row($resServicio);

						$concepto = concepto_cuentasn2($rowServicio[0],'SS',10,$rowServicio[1],$rowsp['fecha_recaudo']);
						
						for ($i=0; $i < count($concepto); $i++) 
						{ 
							if($concepto[$i][2] == 'S')
							{
								$cuentaDebito = $concepto[$i][0];
							}
						}

						$cuentaBancaria = "111005001";

						if($_POST['diferencia'] > 0) {
							
							$sqlComprobanteDet = "INSERT INTO comprobante_det (id_comp, cuenta, tercero, centrocosto, detalle, valdebito, valcredito, estado, vigencia) VALUES ('$codigoComprobante $_POST[codigoComprobante]', '$cuentaBancaria', '$rowsp[documento]', '$rowServicio[1]', '$rowsp[concepto]', '$_POST[diferencia]', 0, '1', '$vigencia')";
							mysqli_query($linkbd, $sqlComprobanteDet);

							$sqlComprobanteDet = "INSERT INTO comprobante_det (id_comp, cuenta, tercero, centrocosto, detalle, valdebito, valcredito, estado, vigencia) VALUES ('$codigoComprobante $_POST[codigoComprobante]', '$cuentaDebito', '$rowsp[documento]', '$rowServicio[1]', '$rowsp[concepto]', 0, '$_POST[diferencia]', '1', '$vigencia')";
							mysqli_query($linkbd, $sqlComprobanteDet);
						}
						else {

							$_POST['diferencia'] = $_POST['diferencia'] * -1;

							$sqlComprobanteDet = "INSERT INTO comprobante_det (id_comp, cuenta, tercero, centrocosto, detalle, valdebito, valcredito, estado, vigencia) VALUES ('$codigoComprobante $_POST[codigoComprobante]', '$cuentaDebito', '$rowsp[documento]', '$rowServicio[1]', '$rowsp[concepto]', '$_POST[diferencia]', 0, '1', '$vigencia')";
							mysqli_query($linkbd, $sqlComprobanteDet);

							$sqlComprobanteDet = "INSERT INTO comprobante_det (id_comp, cuenta, tercero, centrocosto, detalle, valdebito, valcredito, estado, vigencia) VALUES ('$codigoComprobante $_POST[codigoComprobante]', '$cuentaBancaria', '$rowsp[documento]', '$rowServicio[1]', '$rowsp[concepto]', 0, '$_POST[diferencia]', '1', '$vigencia')";
							mysqli_query($linkbd, $sqlComprobanteDet);
						}
						
						echo "
							<script>
								document.form2.submit();
							</script>";
					}
					else {

						echo "
								<script>
									Swal.fire({
										icon: 'error',
										title: 'Error en reflejar, codigo comprobante'
									})
								</script>
							";
					}	
				}

				if ($_POST['oculto'] == '4') {

					$sqlsp = "SELECT codigo_recaudo, valor_pago, id_cliente, fecha_recaudo, YEAR(fecha_recaudo), documento, concepto FROM srv_recaudo_factura WHERE fecha_recaudo BETWEEN '$fechaInicial' AND '$fechaFinal' ORDER BY codigo_recaudo ASC";
					$ressp = mysqli_query($linkbd, $sqlsp);
					while ($rowsp = mysqli_fetch_assoc($ressp)) {

						$diferencia = 0; 
						$validacion01 = 0;
						
						$sqlComprobanteCab = "SELECT * FROM comprobante_cab WHERE tipo_comp = $codigoComprobante AND numerotipo = $rowsp[codigo_recaudo]";
						$validacion01 = mysqli_num_rows(mysqli_query($linkbd, $sqlComprobanteCab));

						$sqlComprobanteDet = "SELECT SUM(valdebito), SUM(valcredito) FROM comprobante_det WHERE tipo_comp = $codigoComprobante AND numerotipo = $rowsp[codigo_recaudo] AND LEFT(cuenta,2)='11'"; 
						$rowComprobanteDet = mysqli_fetch_assoc(mysqli_query($linkbd, $sqlComprobanteDet));

						$valorBanco = $rowComprobanteDet['SUM(valdebito)'] - $rowComprobanteDet['SUM(valcredito)'];

						$diferencia = $rowsp['valor_pago'] - $valorBanco;

						if ($validacion01 == 0) {

							//crea cabecera en comprobantes
							$sqlComprobanteCab = "INSERT INTO comprobante_cab (numerotipo, tipo_comp, fecha, concepto, estado) VALUES ('$rowsp[codigo_recaudo]', $codigoComprobante, '$rowsp[fecha_recaudo]', '$rowsp[concepto]', '1')";
							mysqli_query($linkbd, $sqlComprobanteCab);
						}

						if ($diferencia != 0) {

							//arregla diferencia

							$sqlServicio = "SELECT cargo_fijo_u, cc, nombre FROM srvservicios WHERE id = 1";
							$resServicio = mysqli_query($linkbd,$sqlServicio);
							$rowServicio = mysqli_fetch_row($resServicio);

							$concepto = concepto_cuentasn2($rowServicio[0],'SS',10,$rowServicio[1],$rowsp['fecha_recaudo']);
							
							for ($i=0; $i < count($concepto); $i++) 
							{ 
								if($concepto[$i][2] == 'S')
								{
									$cuentaDebito = $concepto[$i][0];
								}
							}

							$cuentaBancaria = "111005001";

							if ($diferencia > 0){

								$diferencia = round($diferencia, 2);

								$sqlComprobanteDet = "INSERT INTO comprobante_det (id_comp, cuenta, tercero, centrocosto, detalle, valdebito, valcredito, estado, vigencia) VALUES ('$codigoComprobante $rowsp[codigo_recaudo]', '$cuentaBancaria', '$rowsp[documento]', '$rowServicio[1]', '$rowsp[concepto]', '$diferencia', 0, '1', '$vigencia')";
								mysqli_query($linkbd, $sqlComprobanteDet);

								$sqlComprobanteDet = "INSERT INTO comprobante_det (id_comp, cuenta, tercero, centrocosto, detalle, valdebito, valcredito, estado, vigencia) VALUES ('$codigoComprobante $rowsp[codigo_recaudo]', '$cuentaDebito', '$rowsp[documento]', '$rowServicio[1]', '$rowsp[concepto]', 0, '$diferencia', '1', '$vigencia')";
								mysqli_query($linkbd, $sqlComprobanteDet);
							}
							else {

								$diferencia = $diferencia * -1;
								$diferencia = round($diferencia, 2);

								$sqlComprobanteDet = "INSERT INTO comprobante_det (id_comp, cuenta, tercero, centrocosto, detalle, valdebito, valcredito, estado, vigencia) VALUES ('$codigoComprobante $rowsp[codigo_recaudo]', '$cuentaDebito', '$rowsp[documento]', '$rowServicio[1]', '$rowsp[concepto]', '$diferencia', 0, '1', '$vigencia')";
								mysqli_query($linkbd, $sqlComprobanteDet);

								$sqlComprobanteDet = "INSERT INTO comprobante_det (id_comp, cuenta, tercero, centrocosto, detalle, valdebito, valcredito, estado, vigencia) VALUES ('$codigoComprobante $rowsp[codigo_recaudo]', '$cuentaBancaria', '$rowsp[documento]', '$rowServicio[1]', '$rowsp[concepto]', 0, '$diferencia', '1', '$vigencia')";
								mysqli_query($linkbd, $sqlComprobanteDet);
							}
						}
					}

					echo "
						<script>
							document.form2.submit();
						</script>";
				}
			?>

		</form>

		<div id="bgventanamodal2">
			<div id="ventanamodal2">
				<IFRAME  src="" name="buster" marginWidth=0 marginHeight=0 frameBorder=0 id="ventana2" frameSpacing=0 style="left:500px; width:900px; height:500px; top:200;"></IFRAME>
			</div>
		</div>

	</body>
</html>