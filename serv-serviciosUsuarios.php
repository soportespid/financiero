<?php
		require 'comun.inc';
		require 'funciones.inc';
		session_start();
		cargarcodigopag($_GET['codpag'],$_SESSION['nivel']);
		header('Cache-control: private'); // Arregla IE 6
		header("Cache-control: no-cache, no-store, must-revalidate");
		header("Content-Type: text/html;charset=utf8");
		date_default_timezone_set('America/Bogota');
		titlepag();
?>
<!DOCTYPE >
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es">
	<head>
		<meta http-equiv="Content-type" content="text/html; charset=utf8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=9"/>
		<title>:: IDEAL 10 - Servicios P&uacute;blicos</title>
		<link href="css/css2.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/cssSP.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/tabs.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
		<script src="vue/vue.js"></script>
		<script type="text/javascript" src="css/programas.js"></script>        

		<!-- sweetalert2 -->
		<script src="sweetalert2/dist/sweetalert2.min.js"></script>
		<link rel="stylesheet" href="sweetalert2/dist/sweetalert2.min.css">
	</head>
	<body>
		<div class="subpantalla" style="height:640px; width:99.6%; overflow:hidden;">
			<div id="myapp" style="height:inherit;" v-cloak>
				<div>
					<table>
						<tr>
							<script>barra_imagenes("serv");</script>
							<?php cuadro_titulos();?>
						</tr>

						<tr>
							<?php menu_desplegable("serv");?>
						</tr>

						<tr>
							<td colspan="3" class="cinta">
								<a href="#" class="mgbt"><img src="imagenes/add.png"/></a>
								<a onclick="guardar()" class="mgbt"><img src="imagenes/guarda.png" title="Guardar"/></a>
								<a href="#" class="mgbt"><img src="imagenes/busca.png" title="Buscar"/></a>
								<a onclick="mypop=window.open('plan-agenda.php','','');mypop.focus()" class="mgbt"><img src="imagenes/agenda1.png" title="Agenda" /></a>
								<a onclick="mypop=window.open('serv-principal.php','','');mypop.focus();" class="mgbt"><img src="imagenes/nv.png" title="Nueva ventana"></a>
								<a onclick="mypop=window.open('<?php echo $url2; ?>','','');mypop.focus();" class="mgbt"><img src="imagenes/duplicar_pantalla.png" title="Duplicar pesta&ntilde;a"></a>
							</td>
						</tr>
					</table>
				</div>

				<table class="inicio">
					<tr>
						<td class="titulos" colspan="8">.: Ingresar Asignaci&oacute;n</td>
						<td class="cerrar" style="width:7%" onClick="location.href='serv-principal.php'">Cerrar</td>
					</tr>

					<tr>
						<td class="tamano01" style="width: 3cm;">Usuario:</td>

						<td style="width: 15%;">
							<input type="text" name='cod_usuario' id='cod_usuario' v-model='cod_usuario' v-on:dblclick="modalUsuarios" style="width: 100%; height: 30px;" class="colordobleclik" readonly>
						</td>

						<td colspan="2">
							<input type="text" name="ntercero" id="ntercero" style="width:100%;height:30px;" readonly>
						</td>
					</tr>
				</table>
	
				<!------- Modales -------->

				<div v-show="showModalUsuarios">
				<transition name="modal">
					<div class="modal-mask">
						<div class="modal-wrapper">
							<div class="modal-container2">
								<table class="inicio ancho">
									<tr>
										<td class="titulos" colspan="2">LISTADO DE USUARIOS</td>
										<td class="cerrar" style="width:7%" @click="showModalUsuarios = false">Cerrar</td>
									</tr>
									<tr>
										<td class="tamano01" style="width:3cm">Código Usuario:</td>
										<td><input type="text" class="form-control" placeholder="Buscar por nombre o cod usuario" style="width:100%" /></td>
									</tr>
								</table>
								<table>
									<thead>
										<tr style="text-align: center;">
											<td class='titulos' style="font: 160% sans-serif; border-radius: 5px 0 0 0; width:5%;">ID</td>
											<td class='titulos' style="font: 160% sans-serif;">Cod Usuario</td>
											<td class='titulos' style="font: 160% sans-serif;">Nombre</td>
											<td class='titulos' style="font: 160% sans-serif;">Dirección</td>
											<td class='titulos' style="font: 160% sans-serif; border-radius: 0 5px 0 0;">Estrato</td>
										</tr>
									</thead>
								</table>
								<div style='margin: 0px 5px 5px; border-radius: 0 0 5px 5px; height: 200px; overflow: scroll; overflow-x: hidden; background: white;'>
									<table class='inicio inicio--no-shadow'>
										<tbody>
											<?php
												$co ='zebra1';
												$co2='zebra2';
											?>
											<tr v-for="usuario in usuarios" class='<?php echo $co; ?>' style='text-rendering: optimizeLegibility; cursor: pointer !important; style=\"cursor: hand\"'>
												<td style="font: 120% sans-serif; padding-left:10px; width:20%;">{{ usuario[0] }}</td>
												<td style="font: 120% sans-serif; padding-left:10px; width:20%;">{{ usuario[1] }}</td>
												
												<?php
													$aux=$co;
													$co=$co2;
													$co2=$aux;
												?>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</transition>
			</div>
				
			</div>
		</div>
		
		
		<script src="Librerias/vue/vue.min.js"></script>
		<script src="Librerias/vue/axios.min.js"></script>
		<script src="vue/servicios_publicos/serv-serviciosUsuarios.js?<?php echo date('d_m_Y_h_i_s');?>"></script>	
	</body>
</html>