<?php
    header("Cache-control: no-cache, no-store, must-revalidate");
    header("Content-Type: text/html;charset=iso-8859-1");
    require "comun.inc";
    require "funciones.inc";
    require 'funcionesSP.inc.php';
    session_start();
    $linkbd = conectar_v7();
    $linkbd -> set_charset("utf8");
    cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
    date_default_timezone_set("America/Bogota");
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>:: IDEAL 10 - Clientes Servicios P&uacute;blicos</title>
    <link href="css/css2n.css" rel="stylesheet" type="text/css"/>
    <link href="css/css3n.css" rel="stylesheet" type="text/css"/>
    <link href="css/css4.css" rel="stylesheet" type="text/css"/>
    <link href="css/cssSP.css" rel="stylesheet" type="text/css"/>
    <script type="text/javascript" src="css/programas.js"></script>
    <script type="text/javascript" src="css/calendario.js"></script>
    <?php titlepag();?>

    <script>
        function despliegamodal2(_valor, _table) {

            document.getElementById('bgventanamodal2').style.visibility = _valor;
            var cliente = document.getElementById('cliente').value;
            
            if (_table == '') {
                document.getElementById('ventana2').src = '';
            }
            else if(_table == '') {
                document.getElementById('ventana2').src = '';
            }
        }

        function despliegamodalm(_valor,_tip,mensa,pregunta) {
            document.getElementById("bgventanamodalm").style.visibility=_valor;

            if(_valor=="hidden") {
                document.getElementById('ventanam').src="";
            }
            else {
                switch(_tip) {
                    case "1":	
                        document.getElementById('ventanam').src="ventana-mensaje1.php?titulos="+mensa;
                        break;

                    case "2":	
                        document.getElementById('ventanam').src="ventana-mensaje3.php?titulos="+mensa;
                        break;

                    case "3":	
                        document.getElementById('ventanam').src="ventana-mensaje2.php?titulos="+mensa;
                        break;

                    case "4":	
                        document.getElementById('ventanam').src="ventana-consulta1.php?titulos="+mensa+"&idresp="+pregunta;
                        break;	
                }
            }
        }

        //ventanas servicios publicos
        function respuestaModalBusqueda2() {
            
        }

        function funcionmensaje() {
            document.location.href = "serv-interesMoratorio.php";
        }

        function respuestaconsulta(pregunta) {
            switch(pregunta)
            {
                case "1":	
                    document.form2.oculto.value='2';
                    document.form2.submit();
                break;
            }
        }

        function guardar() {
            var porcentajeMora = parseInt(document.getElementById('valorMoratorio').value);
            var vigencia = document.getElementById('vigencia').value;

            if (porcentajeMora > 0 && porcentajeMora <= 100) {

                if (vigencia != '-1') {
                    despliegamodalm('visible','4','¿Esta seguro de guardar?','1');   
                }
                else {
                    despliegamodalm('visible','2','Seleccione una vigencia');    
                }
            }
            else {
                despliegamodalm('visible','2','Valor moratorio debe ser mayor a 0');
            }
        }

        function actualizar() {
            document.form2.submit();
        }

        function vigenciarecarga() {
            document.form2.oculto.value = '3';
            document.form2.submit();
        }
    </script>
</head>
<body>
    <IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
    <span id="todastablas2"></span>
    <table>
        <tr><script>barra_imagenes("serv");</script><?php cuadro_titulos();?></tr>

        <tr><?php menu_desplegable("serv");?></tr>

        <tr>
            <td colspan="3" class="cinta">
                <a href="" class="mgbt"><img src="imagenes/add.png"/></a>

                <a onclick="guardar()" class="mgbt"><img src="imagenes/guarda.png" title="Guardar"/></a>

                <a href="" class="mgbt"><img src="imagenes/busca.png" title="Buscar"/></a>

                <a onclick="mypop=window.open('plan-agenda.php','','');mypop.focus()" class="mgbt"><img src="imagenes/agenda1.png" title="Agenda" /></a>

                <a onclick="mypop=window.open('serv-principal.php','','');mypop.focus();" class="mgbt"><img src="imagenes/nv.png" title="Nueva ventana"></a>

                <a onclick="mypop=window.open('<?php echo $url2; ?>','','');mypop.focus();" class="mgbt"><img src="imagenes/duplicar_pantalla.png" title="Duplicar pesta&ntilde;a"></a>
            </td>
        </tr>
    </table>

    <div id="bgventanamodalm" class="bgventanamodalm">
        <div id="ventanamodalm" class="ventanamodalm">
            <IFRAME src="" name="ventanam" marginWidth=0 marginHeight=0 frameBorder=0 id="ventanam" frameSpacing=0 style=" width:700px; height:130px; top:200; overflow:hidden;"></IFRAME>
        </div>
    </div>

    <form name="form2" method="post" action="">
        <?php 
            if(@$_POST['oculto'] == "") {				
        
            }

            if(@$_POST['oculto'] == '3') {
                if ($_POST['vigencia'] != '-1') {
                    $sql = "SELECT porcentaje FROM srvtasa_interes_mora WHERE vigencia = '$_POST[vigencia]' AND estado = 'S'";
                    $res = mysqli_query($linkbd, $sql);
                    $row = mysqli_fetch_row($res);
                    
                    if(isset($row[0])) {
                        $_POST['valorMoratorio'] = $row[0];
                    }
                    else {
                        $_POST['valorMoratorio'] = '0';
                    }
                }
                else {
                    $_POST['valorMoratorio'] = '';
                    echo "<script>despliegamodalm('visible','2','Selecciona una vigencia valida.');</script>";
                }
            }
        ?>
        <div >
            <table class="inicio">
                <tr>
                    <td class="titulos" colspan="4">.: Tasa de Interes Moratorio Anual</td>

                    <td class="cerrar" style="width:7%" onclick="location.href='serv-principal.php'">Cerrar</td>
                </tr>
                
                <tr>
                    <td class="tamano01" style="width:3cm; text-align:center;">Vigencia:</td>
                    <td style="width: 35%"> 
						<select name="vigencia" id="vigencia" class="centrarSelect" onchange="vigenciarecarga();">
                            <option class="aumentarTamaño" value="-1">SELECCIONE VIGENCIA</option>
							<?php
                                for($y = 0; $y < 10; $y++)
                                {
                                    $anini = date('Y');
                                    $anfin = $anini - $y;

                                    if($anfin == $_POST['vigencia'])
                                    {
                                        echo "<option class='aumentarTamaño' value='$anfin' SELECTED>$anfin</option>";}
                                    else 
                                    {
                                        echo "<option class='aumentarTamaño' value='$anfin'>$anfin</option>";
                                    }
                                }
							?>
						</select>
					</td>

                    <td class="tamano01" style="width: 4cm; text-align:center;">Porcentaje de Mora:</td>
                    <td>
                        <input type="text" name="valorMoratorio" id="valorMoratorio" value="<?php echo $_POST['valorMoratorio'] ?>" style="height: 30px; text-align:center;">%
                    </td>
                </tr>
            </table>

            <input type="hidden" name="oculto" id="oculto" value="1"/>
        </div>
        <?php 
            if(@$_POST['oculto'] == "2")
            {   
                $fecha = date('Y-m-d');

                //busqueda de la vigencia en tabla
                $sqlValidacionVigencia = "SELECT * FROM srvtasa_interes_mora WHERE vigencia = '$_POST[vigencia]' AND estado = 'S'";
                $resValidacionVigencia = mysqli_query($linkbd,$sqlValidacionVigencia);
                $rowValidacionVigencia = mysqli_fetch_row($resValidacionVigencia);

                if(isset($rowValidacionVigencia[0])) {
                    
                    $queryUpdateVigencia = "UPDATE srvtasa_interes_mora SET estado = 'N' WHERE id = $rowValidacionVigencia[0]";
                    
                    if(mysqli_query($linkbd, $queryUpdateVigencia)) {

                        $queryInteresMora = "INSERT INTO srvtasa_interes_mora (vigencia, porcentaje, fecha_cambio, estado) VALUES ('$_POST[vigencia]', '$_POST[valorMoratorio]', '$fecha', 'S')";

                        if(mysqli_query($linkbd, $queryInteresMora)) {
                            echo "<script>despliegamodalm('visible','1','Se ha almacenado con exito el interes moratorio');</script>";
                        }
                        else {
                            echo "<script>despliegamodalm('visible','2','Error guardado de interes moratorio');</script>";    
                        }
                    }
                    else {
                        echo "<script>despliegamodalm('visible','2','Error en actualización de interes moratorio');</script>";
                    }
                }
                else {
                    $queryInteresMora = "INSERT INTO srvtasa_interes_mora (vigencia, porcentaje, fecha_cambio, estado) VALUES ('$_POST[vigencia]', '$_POST[valorMoratorio]', '$fecha', 'S')";

                    if(mysqli_query($linkbd, $queryInteresMora)) {
                        echo "<script>despliegamodalm('visible','1','Se ha almacenado con exito el interes moratorio');</script>";
                    }
                    else {
                        echo "<script>despliegamodalm('visible','2','Error guardado de interes moratorio');</script>";    
                    }
                }        
            }
        ?>
    </form>
    
    <div id="bgventanamodal2">
        <div id="ventanamodal2">
            <IFRAME  src="" name="buster" marginWidth=0 marginHeight=0 frameBorder=0 id="ventana2" frameSpacing=0 style="left:500px; width:900px; height:500px; top:200;"></IFRAME>
        </div>
    </div>
</body>
</html>