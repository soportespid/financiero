<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=iso-8859-1");
	require 'comun.inc';
	require 'funciones.inc';
	require 'conversor.php';
	require 'validaciones.inc';
	session_start();
	$linkbd=conectar_v7();
	cargarcodigopag($_GET[codpag],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es">
	<head>
		<meta http-equiv="Content-type" content="text/html; charset=iso-8859-1"/>
		<meta http-equiv="X-UA-Compatible" content="IE=9"/>
		<title>:: SPID - Servicios P&uacute;blicos</title>
		<link href="css/css2.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<script type="text/javascript" src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script type="text/javascript" src="css/calendario.js"></script>
		<script>
			function procesos($tip)
			{	
				switch ($tip)
				{
					case 1:	if (document.form2.liquigen.value!='')
							{
								document.form2.oculto.value='1';
								document.form2.submit();
							}
							else{despliegamodalm('visible','2','Faltan seleccionar un periodo de facturaci�n para cerrar');}
							break;
					case 2:	despliegamodalm('visible','4','Se generar cierre de facturaci�n de este periodo, �Esta Seguro??','1');
							break;
				}
 			}
			function despliegamodalm(_valor,_tip,mensa,pregunta)
			{
				document.getElementById("bgventanamodalm").style.visibility=_valor;
				if(_valor=="hidden"){document.getElementById('ventanam').src="";}
				else
				{
					switch(_tip)
					{
						case "1":	document.getElementById('ventanam').src="ventana-mensaje1.php?titulos="+mensa;break;
						case "2":	document.getElementById('ventanam').src="ventana-mensaje3.php?titulos="+mensa;break;
						case "3":	document.getElementById('ventanam').src="ventana-mensaje2.php?titulos="+mensa;break;
						case "4":	document.getElementById('ventanam').src="ventana-consulta1.php?titulos="+mensa+"&idresp="+pregunta;break;	
					}
				}
			}
			function funcionmensaje(){}
			function respuestaconsulta(pregunta)
			{
				switch(pregunta)
				{
					case "1":	document.form2.oculto.value=3;	
  								document.form2.submit();break;
				}
			}
			function callprogress(vValor)
			{
				document.getElementById("getprogress").innerHTML = vValor;
				document.getElementById("getProgressBarFill").innerHTML = '<div class="ProgressBarFill" style="width: '+vValor+'%;"></div>';
				document.getElementById("botcerrar").style.display='none';
				document.getElementById("titulog1").style.display='block';
				document.getElementById("progreso").style.display='block';
				document.getElementById("getProgressBarFill").style.display='block';
				if (vValor==100){document.getElementById("titulog2").style.display='block';}
			} 
		</script>
		<?php 
			titlepag(); 
			if (@$_POST['oculto']=="")
			{
				$_POST['menuact']=@$_GET['menu'];
			}
			if (@$_POST['menuact']=="SI")
			{$boatras="<img src='imagenes/iratras.png' title='Men&uacute; Nomina' onClick=\"location.href='serv-menufacturacion.php'\" class='mgbt'>";}
			else {$boatras="";}
		?>
	</head>
	<body>
		<IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
		<span id="todastablas2"></span>
		<table>
			<tr><script>barra_imagenes('serv');</script><?php cuadro_titulos();?></tr>	 
			<tr><?php menu_desplegable('serv');?></tr>
			<tr>
				<td colspan="3" class="cinta"><img src="imagenes/add.png" title="Nuevo" onClick="location.href='serv-cortefact.php'" class="mgbt"/><img src="imagenes/guarda.png" title="Guardar" onClick="guardar()" class="mgbt"/><img src="imagenes/busca.png" title="Buscar" class="mgbt"/><img src="imagenes/nv.png" title="Nueva Ventana" onClick="mypop=window.open('serv-principal.php','','');mypop.focus();" class="mgbt"/><img src="imagenes/print.png" title="Imprimir" onClick="pdf()" class="mgbt"/><?php echo $boatras;?></td>
			</tr>
		</table>
		<div id="bgventanamodalm" class="bgventanamodalm">
			<div id="ventanamodalm" class="ventanamodalm">
				<IFRAME src="" name="ventanam" marginWidth=0 marginHeight=0 frameBorder=0 id="ventanam" frameSpacing=0 style=" width:700px; height:130px; top:200; overflow:hidden;"> 
				</IFRAME>
			</div>
		</div>
		<form name="form2" method="post" action="">
			<div class="loading" id="divcarga"><span>Cargando...</span></div>
			<?php if(@$_POST['oculto']==""){echo"<script>document.getElementById('divcarga').style.display='none';</script>";}?>
			<table class="inicio">
				<tr>
					<td class="titulos" colspan="13">Corte Facturaci&oacute;n Servicios</td>
					<td  class="cerrar" ><a href="serv-principal.php">Cerrar</a></td>
				</tr>
				<tr>
					<td class="saludo1" style="width:4cm;">Facturaci&oacute;n vigente:</td>
					<td style="width:8%;">
						<select name="liquigen" id="liquigen">
							<option value="">Sel..</option>
							<?php
								$sqlr="Select id_cab from  servliquidaciones_gen where estado='S' ";
								$resp = mysqli_query($linkbd,$sqlr);
								while ($row =mysqli_fetch_row($resp)) 
								{
									if($row[0]==$_POST['liquigen'])
									{
										echo "<option value='$row[0]' SELECTED>$row[0]</option>";
									}
									else{echo "<option value='$row[0]'>$row[0]</option>";}
								}
							?>
						</select>
					</td>
					<td><input type="button" name="buscapredios"  value=" Consultar " onClick="procesos(1)"/>  </td>
				</tr>
			</table>
			<input type="hidden" name="oculto" id="oculto" value="<?php echo @$_POST['oculto'];?>"/>
			<input type="hidden" name="facturacion" id="facturacion" value="<?php echo @$_POST['facturacion'];?>"/>
			<input type="hidden" name="totalreg" id="totalreg" value="<?php echo @$_POST['totalreg'];?>"/>
			<input type="hidden" name="nomarchivo" id="nomarchivo" value="<?php echo @$_POST['nomarchivo']?>"/>
			<input type="hidden" name="menuact" id="menuact" value="<?php echo @$_POST['menuact'];?>"/>
			<?php
				if(@$_POST['oculto']>=1)
				{
					if(@$_POST['oculto']==1)
					{
						$sqlr="SELECT vigencia,mes,mesfin,fecha,servicio,numinicial,numfinal FROM servliquidaciones_gen WHERE id_cab = '".$_POST['liquigen']."'";
						$resp=mysqli_query($linkbd,$sqlr);
						while ($row =mysqli_fetch_row($resp))
						{
							$_POST['oculto']=2;
							echo"<script>document.getElementById('oculto').value=2;</script>";
							$_POST['vigencias']="$row[0]";
							$_POST['periodo']="$row[1]";
							$_POST['periodo2']="$row[2]";
							$_POST['fecha']="$row[3]";
							@$_POST['servicios']=="$row[4]";
							$sqlmm="SELECT MAX(id_factura), MIN(id_factura) FROM servfacturas WHERE liquidaciongen = '".$_POST['liquigen']."'";
							$resmm=mysqli_query($linkbd,$sqlmm);
							$rowmm=mysqli_fetch_row($resmm); 
							$_POST['facturaini']=$rowmm[1];
							$_POST['facturafin']=$rowmm[0];
							if($row[4]==""){$_POST['nomservicio']="TODOS";}
							else
							{
								$sqlrsv="SELECT nombre FROM servservicios WHERE codigo='$row[5]'";
								$resv=mysqli_query($linkbd,$sqlrsv);
								$rowsv =mysqli_fetch_row($resv); 
								$_POST['nomservicio']="$rowsv[0]";
							}
						}	
						$_POST['facturatot']=(float)$_POST['facturafin']-(float)$_POST['facturaini']+1;
						echo "<script>document.getElementById('divcarga').style.display='none';</script>";
					}
					echo"
						<input type='hidden' name='facturaini' id='facturaini' value='".$_POST['facturaini']."'/>
						<input type='hidden' name='facturafin' id='facturafin' value='".$_POST['facturafin']."'/>
						<input type='hidden' name='facturatot' id='facturatot' value='".$_POST['facturatot']."'/>
						<input type='hidden' name='fecha' id='fecha' value='".$_POST['fecha']."'/>
						<input type='hidden' name='vigencias' id='vigencias' value='".$_POST['vigencias']."'/>
						<input type='hidden' name='periodo' id='periodo' value='".$_POST['periodo']."'/>
						<input type='hidden' name='periodo2' id='periodo2' value='".$_POST['periodo2']."'/>
						<input type='hidden' name='servicios' id='servicios' value='".@$_POST['servicios']."'/>
						<input type='hidden' name='nomservicio' id='nomservicio' value='".$_POST['nomservicio']."'/>
					";
					if(@$_POST['servicios']==""){$nombreser="Todos";}
					else {$nombreser=$_POST['nomservicio'];}
					echo"
					<table class='inicio'>
						<tr><td class='titulos' colspan='8'>Facturaci&oacute;n sin Corte</td></tr>
						<tr>
							<td class='titulos2' style='width:8%'>N&deg; Liquidaci&oacute;n</td>
							<td class='titulos2' style='width:8%'>Fecha</td>
							<td class='titulos2' style='width:6%'>Vigencia</td>
							<td class='titulos2' style='width:15%'>Periodos</td>
							<td class='titulos2' style='width:14%'>N&deg; Facturaci&oacute;n</td>
							<td class='titulos2' style='width:8%'>Total Facturas</td>
							<td class='titulos2' style='width:15%'>Sevicios</td>
							<td class='titulos2'>Proceso</td>
						</tr> 
						<tr class='saludo2'>
							<td>".$_POST['liquigen']."</td>
							<td>".$_POST['fecha']."</td>
							<td>".$_POST['vigencias']."</td>
							<td>".mesletras($_POST['periodo'])." - ".mesletras($_POST['periodo2'])."</td>
							<td>".$_POST['facturaini']." - ".$_POST['facturafin']."</td>
							<td>".$_POST['facturatot']."</td>
							<td>$nombreser</td>
							<td>
								<input type='button' name='botcerrar' id='botcerrar' value=' Corte Facturaci�n ' onClick='procesos(2)' style='display:block;'/>
								<div id='titulog1' style='display:none; float:left'></div>
								<div id='progreso' class='ProgressBar' style='display:none; float:left'>
									<div class='ProgressBarText'><span id='getprogress'></span>&nbsp;% </div>
									<div id='getProgressBarFill'></div>
								</div>
							</td>
						</tr>
					</table>";
				}
				if(@$_POST['oculto']==3)
				{
					ini_set('max_execution_time', 7200);
					$_POST['oculto']=2;
					$sqlr="SELECT diasplazo FROM servparametros";
					$row =mysqli_fetch_row(mysqli_query($linkbd,$sqlr));
					$diaplazo=$row[0];
					$facrevi="0";
					$c=0;
					$sqlr="SELECT T1.codusuario,T1.estado,T1.fecha,T1.id_liquidacion,T2.servicio FROM servliquidaciones T1,servliquidaciones_det T2 WHERE T1.id_liquidacion>='".$_POST['facturaini']."' AND T1.liquidaciongen = '".$_POST['liquigen']."' AND (T1.estado='S' OR T1.estado='A') AND T1.id_liquidacion = T2.id_liquidacion ORDER BY T1.id_liquidacion, T1.codusuario, T2.servicio ASC";
					$respn=mysqli_query($linkbd,$sqlr);
					$totalcli=mysqli_affected_rows ($linkbd);
					while ($rown =mysqli_fetch_row($respn)) 
					{
						$c+=1;
						$porcentaje = $c * 100 / $totalcli; 
						echo"<script>progres='".round($porcentaje)."';callprogress(progres);</script>"; 
						flush();
						ob_flush();
						usleep(5);//microsegundos
						//ACTUALIZAR FACTURA
						$sqlrsl="
						SELECT 	valorliquidacion, abono FROM servliquidaciones_det WHERE id_det=(SELECT MAX(id_det) FROM servliquidaciones_det WHERE codusuario='$rown[0]' AND servicio='$rown[4]')";
						$respsl = mysqli_query($linkbd,$sqlrsl);
						$rowsl =mysqli_fetch_row($respsl);
						$saldofin=$rowsl[0]-$rowsl[1];
						$xyx=0;
						$yy=0;
						$datidgen=array();
						$datidliq=array();
						$dattarif=array();
						$datsubsi=array();
						$datsaldo=array();
						$datvalli=array();
						$databono=array();
						$databgen=array();
						$datinter=array();
						$datinabo=array();
						$datestad=array();
						$sqlr="
						SELECT id_det,id_liquidacion,tarifa,subsidio,saldo,valorliquidacion,abono,abonogen,intereses,inabono,estado
						FROM servliquidaciones_det
						WHERE codusuario='$rown[0]' AND servicio='$rown[4]'
						ORDER BY id_liquidacion DESC";
						$resp = mysqli_query($linkbd,$sqlr);
						while (($row =mysqli_fetch_row($resp))&&($xyx<1)) 
						{
							$datidgen[$yy]=$row[0];
							$datidliq[$yy]=$row[1];
							$dattarif[$yy]=$row[2];
							$datsubsi[$yy]=$row[3];
							$datsaldo[$yy]=$row[4];
							$datvalli[$yy]=$row[5];
							$databono[$yy]=$row[6];
							$databgen[$yy]=$row[7];
							$datinter[$yy]=$row[8];
							$datinabo[$yy]=$row[9];
							$datestad[$yy]=$row[10];
							if($row[4]==0){$xyx=2;}
							$yy++;
						}
						$varcont=count($datidgen)-1;
						for ($xf=$varcont;$xf>=0;$xf--)
						{
							if($datestad[$xf]!='PA')
							{
								$sqlrxf="SELECT fecha FROM servliquidaciones WHERE id_liquidacion='$datidliq[$xf]'";
								$rowxf =mysqli_fetch_row(mysqli_query($linkbd,$sqlrxf));
								$fecha = new DateTime($rowxf[0]);
								$fecha->add(new DateInterval('P'.$diaplazo.'D'));
								$fechapl=$fecha->format('Y-m-d H:i:s');
								$segundos=strtotime($fechapl)-strtotime('now');
								$diasmora=abs(intval($segundos/60/60/24));
								$valormes=@$dattarif[$xf]-@$datsubsi[$xf]-@$databgen[$yy];
								$interes=$valormes*0.06*($diasmora/360);
								$sqlrff="UPDATE servliquidaciones_det SET intereses='$interes' WHERE id_det='$datidgen[$xf]'";
								mysqli_query($linkbd,$sqlrff);
							}
						}
						if($facrevi!=$rown[3])
						{
							$sqlrmy="UPDATE servfacturas SET estado='V' WHERE id_factura='$rown[3]'";
							mysqli_query($linkbd,$sqlrmy);
							$sqlrmy="UPDATE servliquidaciones SET estado='V' WHERE factura='$rown[3]'";
							mysqli_query($linkbd,$sqlrmy);
						}
						$facrevi=$rown[3];
					}
					echo"<script>document.getElementById('divcarga').style.display='none';</script>";
					$sqlr="UPDATE servliquidaciones_gen set estado='C' where id_cab='".$_POST['liquigen']."'";
					mysqli_query($linkbd,$sqlr);
				  }
			  ?>
		</form>
	</body>
</html>