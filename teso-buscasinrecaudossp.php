<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");

	require "comun.inc";
	require "funciones.inc";
	require "conversor.php";
	require "validaciones.inc";

	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");

	session_start();
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE > 
<html lang="es">
	<head>
		<meta charset="utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1"/>
		<meta name="viewport" content="user-scalable=no">
		<title>:: IDEAL 10 - Tesorer&iacute;a</title>
		<link href="favicon.ico" rel="shortcut icon"/>
        <link href="css/css2.css" rel="stylesheet" type="text/css" />
        <link href="css/css3.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="css/programas.js"></script>
        <script type="text/javascript" src="css/calendario.js"></script>
		<script>
			//************* ver reporte ************
			//***************************************
			function verep(idfac)
			{
			document.form1.oculto.value=idfac;
			document.form1.submit();
			}
			//************* genera reporte ************
			//***************************************
			function genrep(idfac)
			{
			document.form2.oculto.value=idfac;
			document.form2.submit();
			}
			function buscacta(e)
			{
			if (document.form2.cuenta.value!="")
			{
			document.form2.bc.value='1';
			document.form2.submit();
			}
			}
			function validar()
			{
			document.form2.submit();
			}
			function buscater(e)
			{
			if (document.form2.tercero.value!="")
			{
			document.form2.bt.value='1';
			document.form2.submit();
			}
			}
			function agregardetalle()
			{
			if(document.form2.numero.value!="" &&  document.form2.valor.value>0 &&  document.form2.banco.value!=""  )
			{ 
						document.form2.agregadet.value=1;
			//			document.form2.chacuerdo.value=2;
						document.form2.submit();
			}
			else {
			alert("Falta informacion para poder Agregar");
			}
			}
			//************* genera reporte ************
			//***************************************
			function eliminar(idr)
			{
			if (confirm("Esta Seguro de Eliminar el Recibo de Caja"))
			{
			document.form2.oculto.value=2;
			document.form2.var1.value=idr;
			document.form2.submit();
			}
			}
			//************* genera reporte ************
			//***************************************
			function guardar()
			{

			if (document.form2.fecha.value!='')
			{
			if (confirm("Esta Seguro de Guardar"))
			{
			document.form2.oculto.value=2;
			document.form2.submit();
			}
			}
			else{
			alert('Faltan datos para completar el registro');
			document.form2.fecha.focus();
			document.form2.fecha.select();
			}
			}
			function pdf()
			{
			document.form2.action="teso-pdfconsignaciones.php";
			document.form2.target="_BLANK";
			document.form2.submit(); 
			document.form2.action="";
			document.form2.target="";
			}
		</script>
	</head>
	<body>
	<IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
		<span id="todastablas2"></span>
		<table>
			<tr><script>barra_imagenes("teso");</script><?php cuadro_titulos();?></tr>	 
			<tr><?php menu_desplegable("teso");?></tr>
			<tr>
				<td colspan="3" class="cinta">
					<a href="teso-sinrecaudossp.php" class="mgbt"><img src="imagenes/add.png" title="Nuevo"/></a>
					<a class="mgbt"><img src="imagenes/guardad.png"/></a>
					<a class="mgbt"><img src="imagenes/busca.png" title="Buscar"/></a>
					<a onClick="mypop=window.open('plan-agenda.php','','');mypop.focus()" class="mgbt"><img src="imagenes/agenda1.png" title="Agenda"/></a>
					<a onClick="mypop=window.open('teso-principal.php','','');mypop.focus();" class="mgbt"><img src="imagenes/nv.png" title="Nueva ventana"></a>
					<a onClick="mypop=window.open('<?php echo $url2; ?>','','');mypop.focus();" class="mgbt"><img src="imagenes/duplicar_pantalla.png" title="Duplicar pesta&ntilde;a"></a>
				</td>
			</tr>
		</table>	
		<form name="form2" method="post" action="teso-buscasinrecaudossp.php">
		<table  class="inicio" align="center" >
			<tr>
				<td class="titulos" colspan="6">:. Buscar Recaudos </td>
				<td width="70" class="cerrar" ><a href="teso-principal.php">Cerrar</a></td>
			</tr>
      		<tr>
				<td width="168" class="saludo1">Numero Liquidacion:</td>
				<td width="154" ><input name="numero" type="text" value="" >
				</td>
         		<td width="144" class="saludo1">Concepto Liquidacion: </td>
    			<td width="498" ><input name="nombre" type="text" value="" size="80" ></td>
         		<input name="oculto" type="hidden" value="1"><input name="var1" type="hidden" value=<?php echo $_POST['var1'];?>>
        	</tr>                       
    	</table>    
    	<div class="subpantallap" style="height:68.5%; width:99.6%; overflow-x:hidden;">
			<?php	
			if($_POST['oculto']==2)
			{
				$sqlr="select * from tesosinrecaudossp where id_recaudo=$_POST[var1]";
				$resp = mysqli_query($linkbd, $sqlr);
				$row = mysqli_fetch_row($resp);
				//********Comprobante contable en 000000000000
				$sqlr = "update comprobante_cab set total_debito=0,total_credito=0,estado='0' where tipo_comp=36 and numerotipo=$row[0]";
				mysqli_query($linkbd, $sqlr);
				$sqlr = "update comprobante_det set valdebito=0,valcredito=0 where id_comp='36 $row[0]'";
				mysqli_query($linkbd, $sqlr);		
				$sqlr = "update tesosinrecaudossp set estado='N' where id_recaudo=$row[0]";
				mysqli_query($linkbd, $sqlr);
			}

			$oculto=$_POST['oculto'];

			$crit1=" ";
			$crit2=" ";
			if ($_POST['numero']!="")
			$crit1=" and tesosinrecaudossp.id_recaudo like '%".$_POST['numero']."%' ";
			if ($_POST['nombre']!="")
			$crit2=" and tesosinrecaudossp.concepto like '%".$_POST['nombre']."%'  ";
			//sacar el consecutivo 
			//$sqlr="select *from pptosideforigen where".$crit1.$crit2." order by pptosideforigen.codigo";
			$sqlr="select *from tesosinrecaudossp where tesosinrecaudossp.id_recaudo>-1 ".$crit1.$crit2." order by tesosinrecaudossp.id_recaudo DESC";
			// echo "<div><div>sqlr:".$sqlr."</div></div>";
			$resp = mysqli_query($linkbd, $sqlr);
			$ntr = mysqli_num_rows($resp);
			$con=1;
			echo "<table class='inicio' align='center' ><tr><td colspan='7' class='titulos'>.: Resultados Busqueda:</td></tr><tr><td colspan='2'>Recaudos Encontrados: $ntr</td></tr><tr><td width='150' class='titulos2'>Codigo</td><td class='titulos2'>Nombre</td><td class='titulos2'>Fecha</td><td class='titulos2'>Contribuyente</td><td class='titulos2'>Estado</td><td class='titulos2' width='5%'><center>Anular</td><td class='titulos2' width='5%'><center>Editar</td></tr>";	
			//echo "nr:".$nr;
			$iter='saludo1a';
			$iter2='saludo2';
			while ($row = mysqli_fetch_row($resp)) 
			{
				echo "<tr class='$iter'><td>$row[0]</td><td>$row[6]</td><td>$row[2]</td><td>$row[4]</td><td>$row[7]</td>";
				if ($row[7]=='S')
				echo "<td style='text-align:center;'><a href='#' onClick=eliminar($row[0])><img src='imagenes/anular.png'></a></td>";
				if ($row[7]=='N' || $row[7]=='P')
				echo "<td ></td>";	
				echo "<td style='text-align:center;'><a href='teso-editasinrecaudossp.php?idrecaudo=$row[0]'><img src='imagenes/b_edit.png' style='width:18px' title='Editar'></a></td></tr>";
				$con+=1;
				$aux=$iter;
				$iter=$iter2;
				$iter2=$aux;
 			}
 			echo"</table>";

		?></div>
	</form> 
</body>
</html>