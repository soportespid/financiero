<?php
	require"comun.inc";
	require"funciones.inc";
	require"conversor.php";
	require"serviciospublicos.inc";
	require 'funcionessp.inc';
	session_start();
	$linkbd=conectar_bd();
	cargarcodigopag($_GET[codpag],$_SESSION["nivel"]);
	header("Cache-control: private"); // Arregla IE 6
	header("Content-Type: text/html;charset=iso-8859-1");
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es">
	<head>
	 	<meta http-equiv="Content-type" content="text/html; charset=iso-8859-1"/>
        <meta http-equiv="X-UA-Compatible" content="IE=9"/>
		<title>:: SPID - Servicios Publicos</title>
        <link href="css/css2.css" rel="stylesheet" type="text/css" />
		<link href="css/css3.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="css/programas.js"></script>
		<script type="text/javascript" src="css/calendario.js"></script>
		<script>
			function validar()
			{	
				if (isNaN(form2.cod_usuario.value))
				{
					alert ("Solo se pueden digitar numeros");
					form2.cod_usuario.focus();
				}					
				if(form2.cod_usuario.value!="")
				{
					document.form2.oculto.value='1'
					document.form2.submit();
				}
			}
			function pdf()
			{
				document.form2.action="serv-pdfrecaja.php";
				document.form2.target="_BLANK";
				document.form2.submit(); 
				document.form2.action="";
				document.form2.target="";
			}
			function guardar()
			{
				var validacion01=document.getElementById('concepto').value;
			if (document.form2.fecha.value!='' && validacion01.trim()!='' && document.form2.modorec.value!='' &&((document.form2.modorec.value=='banco' && document.form2.banco.value!='') || (document.form2.modorec.value=='caja' && document.form2.cuentacaja.value!='')) )
  				{despliegamodalm('visible','4','Esta Seguro de Guardar','1');}
 				else {despliegamodalm('visible','2','Falta informacion para Crear la Cuenta');}
			}
			function validar2()
			{
				document.form2.oculto.value='3'
				document.form2.submit();
			}
			function despliegamodal2(_valor)
			{
				document.getElementById("bgventanamodal2").style.visibility=_valor;
				if(_valor=="hidden"){document.getElementById('ventana2').src="";}
				else {document.getElementById('ventana2').src="clientes_ventana01.php?ti=1";}
			}
			function despliegamodalm(_valor,_tip,mensa,pregunta)
			{
				document.getElementById("bgventanamodalm").style.visibility=_valor;
				if(_valor=="hidden"){document.getElementById('ventanam').src="";}
				else
				{
					switch(_tip)
					{
						case "1":
							document.getElementById('ventanam').src="ventana-mensaje1.php?titulos="+mensa;break;
						case "2":
							document.getElementById('ventanam').src="ventana-mensaje3.php?titulos="+mensa;break;
						case "3":
							document.getElementById('ventanam').src="ventana-mensaje2.php?titulos="+mensa;break;
						case "4":
							document.getElementById('ventanam').src="ventana-consulta1.php?titulos="+mensa+"&idresp="+pregunta;break;	
					}
				}
			}
			function funcionmensaje(){document.location.href = "";}
			function respuestaconsulta(pregunta)
			{
				switch(pregunta)
				{
					case "1":	document.form2.oculto.value=2;
								document.form2.submit();
								break;
				}
			}
		</script>
		<?php titlepag();?>
	</head>
	<body>
		<IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
		<span id="todastablas2"></span>
		<table>
			<tr><script>barra_imagenes("serv");</script><?php cuadro_titulos();?></tr>	 
			<tr><?php menu_desplegable("serv");?></tr>
			<tr>
  				<td colspan="3" class="cinta"><a onClick="location.href='serv-abonos.php'" class='mgbt'><img src="imagenes/add.png" title="Nuevo" /></a><a onClick="guardar()" class='mgbt'><img src="imagenes/guarda.png" title"Guardar" /></a><a onClick="location.href='serv-buscarabonos.php'" class='mgbt'><img src="imagenes/busca.png" title="Buscar" /></a><a onClick="mypop=window.open('serv-principal.php','','');mypop.focus();" class='mgbt'><img src="imagenes/nv.png" title="Nueva Ventana"></a><a href="<?php echo "archivos/".$_SESSION[usuario]."reporteclientes.csv"; ?>" target="_blank" class='mgbt'><img src="imagenes/csv.png" title="csv"></a><a class='mgbt' <?php if($_POST[oculto]==2) { ?>onClick="pdf()" <?php } ?>><img src="imagenes/print.png" title="Buscar"/></a></td>
         	</tr>	
		</table>
        <div id="bgventanamodalm" class="bgventanamodalm">
            <div id="ventanamodalm" class="ventanamodalm">
                <IFRAME src="" name="ventanam" marginWidth=0 marginHeight=0 frameBorder=0 id="ventanam" frameSpacing=0 style=" width:700px; height:130px; top:200; overflow:hidden;"> 
                </IFRAME>
            </div>
        </div>
 		<form name="form2" method="post" action="serv-abonos.php">
 			<?php
 				$vigusu=vigencia_usuarios($_SESSION[cedulausu]);
 				$_POST[vigencia]=$vigusu;
				if(!$_POST[oculto])
				{
					$fec=date("d/m/Y");
					$sqlr="select valor_inicial from dominios where nombre_dominio='CUENTA_CAJA'";
					$res=mysql_query($sqlr,$linkbd);
					while ($row =mysql_fetch_row($res)){$_POST[cuentacaja]=$row[0];}
					$_POST[valabono]=0;	
				} 
				$_POST[idcomp]=selconsecutivo('servreciboscaja','id_recibos');
				
 			?>
            <div class="subpantallac4" style="height:74%; width:99.4%; overflow-x:hidden;">
                <table  class="inicio" align="center" >
                    <tr >
                        <td class="titulos" colspan="7">:. Buscar Clientes </td>
                        <td  class="cerrar" ><a href="serv-principal.php">Cerrar</a></td>
                    </tr>
                    <tr>
                        <td class="saludo1" style="width:2.3cm;">N&deg; Abono:</td>
                        <td style="width:10%;">
                            <input type="text" name="idcomp" id="idcomp" value="<?php echo $_POST[idcomp]?>" style="width:100%;" readonly/>
                            <input type="hidden" name="intereses" id="intereses" value="<?php echo $_POST[intereses]?>"/>
                        </td>
                        <td class="saludo1" style="width:2.3cm;">Fecha:</td>
                        <td style="width:35%;"><input type="date"  name="fecha" value="<?php echo $_POST[fecha]; ?>" onKeyUp="return tabular(event,this)"/></td>
                        <td class="saludo1">Vigencia:</td>
                        <td colspan="2" ><input type="text" name="vigencia" id="vigencia" value="<?php echo $_POST[vigencia]?>" onClick="document.getElementById('tipocta').focus();document.getElementById('tipocta').select();" readonly/></td>
                    </tr>     
                    <tr>
                        <td class="saludo1">Recaudado:</td>
                        <td>
                            <input type="hidden" name="tiporec" value='5'/> 
                            <select name="modorec" id="modorec" onKeyUp="return tabular(event,this)" onChange="validar2()" style="width:100%;">
								<option value="" <?php if($_POST[modorec]=='seleccione') echo "SELECTED"; ?>>seleccione</option>
                                <option value="caja" <?php if($_POST[modorec]=='caja') echo "SELECTED"; ?>>Caja</option>
                                <option value="banco" <?php if($_POST[modorec]=='banco') echo "SELECTED"; ?>>Banco</option>
                            </select>
                        </td>
                        <?php
                            if ($_POST[modorec]=='banco')
                            {
                                echo"
                                <td colspan='2'> 
                                    <select id='banco' name='banco'  onChange='validar2()' onKeyUp='return tabular(event,this)' style='width:30%;'/>
                                        <option value=''>Seleccione....</option>";
                                $sqlr="SELECT tesobancosctas.estado,tesobancosctas.cuenta,tesobancosctas.ncuentaban,tesobancosctas.tipo, terceros.razonsocial,tesobancosctas.tercero FROM tesobancosctas,terceros WHERE tesobancosctas.tercero=terceros.cedulanit AND tesobancosctas.estado='S' ";
                                $res=mysql_query($sqlr,$linkbd);
                                while ($row =mysql_fetch_row($res)) 
                                {
                                    if($row[1]==$_POST[banco])
                                    {
                                        echo "<option value='$row[1]' SELECTED>$row[2] - Cuenta $row[3]</option>";
                                        $_POST[nbanco]=$row[4];
                                        $_POST[ter]=$row[5];
                                        $_POST[cb]=$row[2];
                                    }
                                    else {echo "<option value='$row[1]'>$row[2] - Cuenta $row[3]</option>";} 	 
                                }	 	
                                echo"
                                    </select>
                                    <input type='hidden' name='cb' value='$_POST[cb]'/>
                                    <input type='hidden' name='ter' id='ter' value='$_POST[ter]'/> 
                                    <input type='text' id='nbanco' name='nbanco' value='$_POST[nbanco]' style='width:68%;' readonly/>
                                </td>";
                                $bttak=2;
                            }
                            else {$bttak=4;}
                        ?>
                        
                        <td class="saludo1" >Concepto:</td>
                        <td colspan="<?php echo $bttak;?>"><input type="text" name="concepto" id="concepto" value="<?php echo $_POST[concepto] ?>" onKeyUp="return tabular(event,this)" style="width:100%;"/></td>		
                    </tr> 
                    <tr>
                        <td  class="saludo1">Cod. Usuario:</td>
                        <td><input type="text" name="cod_usuario" id="cod_usuario" value="<?php echo $_POST[cod_usuario]?>" style="width:80%;"/>&nbsp;<a href="#" onClick="despliegamodal2('visible');"><img src="imagenes/buscarep.png" title="Buscar" onClick=""/></a></td>
                        <td  class="saludo1">Usuario:</td>
                        <td>
                            <input type="text" name="tercero" id="tercero" value="<?php echo $_POST[tercero]?>" style="width:25%;" readonly >
                            <input type="text" name="ntercero" id="ntercero" value="<?php echo $_POST[ntercero]?>" style="width:73%;" readonly >
                        </td>
                        <td  class="saludo1">Estados: </td>
                        <td colspan="2">
                            <select name="estado_factura" id"estado_factura"> 
                                <option value="">::: Seleccione Estado ::: </option>
                                <option value="S">ACTIVAS</option>
                                <option value="P">PAGADAS </option>
                                <option value="V">VENCIDAS </option>
                                <option value="N">ANULADAS</option>
                            </select>  
                            <input type="hidden" name="oculto" id="oculto" value="<?php echo $_POST[oculto]?>"/>
                            <input type="hidden" name="cuentacaja" id="cuentacaja" value="<?php echo $_POST[cuentacaja]?>"/>  
                            <input type="button" name="buscar" id="buscar" value="  Buscar  " onClick="validar()"/>
                        </td>    
                    </tr>
                    <tr>
                    	<td  class="saludo1">Abono:</td>
                    	<td><input type="text" name="valabono" id="valabono" value="<?php echo $_POST[valabono]?>" onKeyPress='javascript:return solonumeros(event)' style='text-align:right;width:100%;'></td>
                    </tr>                      
                </table>
                <div class="subpantallac4">
                    <?php
                        $oculto=$_POST[oculto];
                        if($_POST[oculto]=='1' || $_POST[oculto]=='2' || $_POST[oculto]=='3')
                        {
                            $crit1=" ";
                            $con=1;
							$ban01=0;
							$valtotal=0;
                            $_POST[ntercero] = buscatercero($_POST[tercero]);
                            if($_POST[oculto]==1)
                            {
                                $_POST[dcoding]= array(); 		 
                                $_POST[dncoding]= array(); 		 
                                $_POST[dvalores]= array(); 
                                $_POST[destratos]= array(); 
                                $_POST[dcargosfijos]= array(); 		 
                                $_POST[dtarifas]= array(); 		 
                                $_POST[dsubsidios]= array(); 
                                $_POST[ddescuentos]= array(); 
                                $_POST[dcontribuciones]= array(); 		 
                                $_POST[dsaldos]= array(); 		 
                                $_POST[dinteres]= array(); 
                                $_POST[dabonos]= array();  
                            }
                            $sqlr1="SELECT servfacturas.id_factura, servfacturas.estado, servfacturas.liquidaciongen, servfacturas.vigencia, servfacturas.mes, servfacturas.mes2, servreciboscaja.estado, servreciboscaja.id_recibos, servreciboscaja.fecha, servreciboscaja.estado, (SELECT SUM(valorliquidacion) FROM servliquidaciones_det WHERE id_liquidacion = servfacturas.id_factura and codusuario='$_POST[cod_usuario]'), (SELECT SUM(saldo) FROM servliquidaciones WHERE id_liquidacion = servfacturas.id_factura and codusuario='$_POST[cod_usuario]') FROM servfacturas LEFT JOIN servreciboscaja ON servfacturas.id_factura = servreciboscaja.id_recaudo WHERE servfacturas.codusuario =  '$_POST[cod_usuario]' and servfacturas.estado LIKE '%$_POST[estado_factura]%' ORDER BY  servfacturas.id_factura DESC, servreciboscaja.id_recibos DESC";
                            //echo "$sqlr1 </br>";
                            $resp1 = mysql_query($sqlr1,$linkbd);
                            $ntr = mysql_num_rows($resp1);
                            echo "
                            <table class='inicio' align='center' >
                                <tr><td colspan='13' class='titulos'>.: Resultados Busqueda:</td></tr>
                                <tr><td colspan='13'>Ingresos Encontrados: $ntr</td></tr>
                                <tr>
                                    <td width='50' class='titulos2'>Item</td>
                                    <td width='70' class='titulos2'>Numero Factura</td>
                                    <td width='50' class='titulos2'>Ciclo</td>
                                    <td width='50' class='titulos2'>Vigencia</td>
                                    <td width='50' class='titulos2'>Mes Inicial</td>
                                    <td width='50' class='titulos2'>Mes Final</td>
                                    <td width='70' class='titulos2'>Documento Usuario</td>
                                    <td width='250'class='titulos2'>Nombre Usuario</td>
                                    <td width='80' class='titulos2'>Valor</td>
                                    <td width='50' class='titulos2'>Estado Factura</td>
                                    <td width='50' class='titulos2'>Recibo Caja</td>
                                    <td width='100' class='titulos2'>Fecha Recibo</td>
                                    <td width='70' class='titulos2'>Estado Recibo</td>
                                </tr>";	
                            $iter='zebra1s';
                            $iter2='zebra2s';
                            $namearch="archivos/".$_SESSION[usuario]."reporteclientes.csv";
                            $Descriptor1 = fopen($namearch,"w+"); 
                            fputcsv($Descriptor1,array("NUMERO FACTURA","CICLO","VIGENCIA","MES INICIAL","MES FINAL","COD USUARIO","NOMBRE USUARIO","VALOR","ESTADO FACTURA","RECIBO CAJA","FECHA RECIBO","ESTADO RECIBO"),';');
                            while ($row =mysql_fetch_row($resp1)) 
                            {	  
                                if ($row[1]=='P'){$estadofac = "<img src='imagenes/dinero3.png' alt='Pagada' />";}
                                if ($row[1]=='V'){$estadofac = "<img src='imagenes/dinerob3.png' alt='Vencida' />";}
                                if ($row[1]=='S'){$estadofac = "<img src='imagenes/confirm.png' alt='Activa' />";}
                                if ($row[1]=='N'){$estadofac = "<img src='imagenes/del2.png' alt='Anulada' />";}
                                if ($row[1]=='A'){$estadofac = "<img src='imagenes/yellow_ball.png' alt='Abonada' />";}
                                if ($row[9]== '' || $row[9] == NULL){$estadorec = "------";}
                                if ($row[9]=='S'){$estadorec ="<img src='imagenes/confirm.png' alt='Pagada' />";}
                                if ($row[7]== '' || $row[7] == NULL){$row[7]= "------";}
                                if ($row[8]== '' || $row[8] == NULL){$row[8]= "------";}
                                $estch="";
                                if( $row[1]=='S' || $row[1]=='A')
                                {
                                    $estch=' ENABLED';	
                                    if($row[0]>$factiva)
                                    {
                                        $factiva=$row[0];
                                        $_POST[idrecaudo]=$factiva;
                                    }
                                }
                                else {$estch=' DISABLED';}	
                                $chk="";
                                if(esta_en_array($_POST[facturas], $row[0]))
                                $chk=" checked";
								if($ban01==0){$valtotal=$row[10];$ban01=1;}
                                echo"
                                <tr class='$iter' style='text-transform:uppercase' onMouseOver=\"anterior=this.style.backgroundColor; this.style.backgroundColor='#40b3ff';\" onMouseOut=\"this.style.backgroundColor=anterior\">
                                    <td>$con</td>
                                    <td>$row[0]</td>
                                    <td>$row[2]</td>
                                    <td>$row[3]</td>
                                    <td>$row[4]</td>
                                    <td>$row[5]</td>
                                    <td>$_POST[tercero]</td>
                                    <td>$_POST[ntercero]</td>
                                    <td align='right'>$ ".number_format(($row[10]),2,",",".")."</td>
                                    <td align='center'>$estadofac</td>
                                    <td align='center'>$row[7]</td>
                                    <td align='center'>$row[8]</td>
                                    <td align='center'>$estadorec</td>
                                </tr>
                                <input type='hidden' name='numeros_fac[]' value='$row[0]'/>
                                <input type='hidden' name='ciclos[]' value='$row[2]'/>
                                <input type='hidden' name='vigencias[]' value='$row[3]'/>
                                <input type='hidden' name='meses[]' value='$row[4]'/>
                                <input type='hidden' name='mesesfinales[]' value='$row[5]'/>
                                <input type='hidden' name='codigos[]' value='$_POST[cod_usuario]'/>
                                <input type='hidden' name='terceros' value='$_POST[tercero]'/>
                                <input type='hidden' name='valores[]' value='$row[10]'>
                                <input type='hidden' name='estados_fac[]' value= '$row[1]'>
                                <input type='hidden' name='recibos_caja[]' value= '$row[7]'>
                                <input type='hidden' name='fechas_recibos[]' value= '$row[8]'>
                                <input type='hidden' name='estados_rec[]' value= '$row[9]'>";
                                fputcsv($Descriptor1,array($row[0],$row[2],$row[3],$row[4],$row[5],$_POST[cod_usuario],$_POST[tercero],$row[10]+$row[11],$row[1],$row[7],$row[8],$row[9]),';');
                                $con+=1;
                                $aux=$iter;
                                $iter=$iter2;
                                $iter2=$aux;
                            }
                            fclose($Descriptor1);
                            echo"</table>";
                        }
                    ?>
                </div>
                <div class="subpantallac5">
                    <table class="inicio">
                        <tr><td class="titulos" colspan="12">Detalle Deuda </td></tr>
                        <input name="idrecaudo" value="<?php echo $_POST[idrecaudo]?>" type="hidden" size="5" readonly>
                        <tr>
                            <td class="titulos2">Codigo</td>
                            <td class="titulos2">Servicio</td>
                            <td class="titulos2">Cargo Fijo</td>
                            <td class="titulos2">Tarifa</td>
                            <td class="titulos2">Subsidio</td>
                            <td class="titulos2">Descuento</td>
                            <td class="titulos2">Contribucion</td>
                            <td class="titulos2">Saldo Anterior</td>
                            <td class="titulos2">Valor Mes</td>
                            <td class="titulos2">Intereses</td>
                            <td class="titulos2">Total</td>
                            <td class="titulos2">Valor Abono</td>
                        </tr>
                        <?php
                            $sqlr="select servliquidaciones_det.servicio, servliquidaciones_det.valorliquidacion, servliquidaciones_det.estrato, servliquidaciones.saldo, servliquidaciones.codusuario, servliquidaciones.tercero, servliquidaciones.id_liquidacion from servliquidaciones, servliquidaciones_det where servliquidaciones.factura=$factiva and servliquidaciones.id_liquidacion=servliquidaciones_det.id_liquidacion AND servliquidaciones.ESTADO='S'";
                            $res=mysql_query($sqlr,$linkbd);
                            //echo "$sqlr </br>";
                            while ($row =mysql_fetch_row($res)) 
                            {	
                                //$_POST[intereses]=$row[3];
								$_POST[intereses]=0;
                                $_POST[codcatastral]=$row[1];		
                                $_POST[concepto]=$row[17].'USUARIO '.$row[4];	
                                $_POST[valorecaudo]=$row[8];		
                                $_POST[totalc]=$row[8];	
                                $_POST[tercero]=$row[5];	
                                $_POST[liquidacion]=$row[6];
                                $_POST[codigousuario]=$row[4];
                                $_POST[ntercero]=buscatercero($row[5]);
                            }
                            if($_POST[oculto]==1 )
                            {
                                $sqlr="SELECT T2.servicio,T2.saldo,T2.estrato,T2.valorliquidacion,T2.cargofijo,T2.tarifa,T2.subsidio,T2.descuento, T2.contribucion,T2.abono FROM servliquidaciones T1, servliquidaciones_det T2
								WHERE T1.factura='$factiva' and T1.id_liquidacion=T2.id_liquidacion ORDER BY T2.servicio ASC";
                                //echo "$sqlr";				 
                                $res=mysql_query($sqlr,$linkbd);
                                while ($row =mysql_fetch_row($res)) 
                                {
                                    $_POST[dcoding][]=$row[0];	
                                    $_POST[dncoding][]=buscar_servicio($row[0]);			 		
                                    $_POST[dcargofijos][]=$row[4];	
                                    $_POST[dtarifas][]=$row[5];	
                                    $_POST[dsubsidios][]=$row[6];	
                                    $_POST[ddescuentos][]=$row[7];							
                                    $_POST[dcontribuciones][]=$row[8];	
                                    $_POST[dsaldos][]=$row[1];	
                                    $_POST[dvalores][]=$row[3];	
                                    
									$_POST[dinteres][]=$row[3]-($row[5]+$row[4]+$row[8]+$row[1]-$row[6]-$row[7]);
									if ($_POST[valabono]==0){$_POST[dabonos][]=$row[3]-$row[9];}	
									else
									{
									$porcentaje=($row[3]*100)/$valtotal;
									$_POST[dabonos][]=round((($_POST[valabono]*$porcentaje)/100));
									}
                                }
                            }			
                            $_POST[totalc]=0;
                            $_POST[totalab]=0;
                            $iter1="zebra1";
                            $iter2="zebra2";
                            $tam=count($_POST[dcoding]);
                            //$valorint=round($_POST[intereses]/$tam,2);
							$valorint=0;
                            for ($x=0;$x<count($_POST[dcoding]);$x++)
                            {		 
                                //$_POST[dinteres][]=$valorint;	
                                $total=$valorint+$_POST[dvalores][$x];
								$toalmed=$_POST[dvalores][$x]-$_POST[dinteres][$x];
                                echo "
                                <input type='hidden' name='dcoding[]' value='".$_POST[dcoding][$x]."'/>
                                <input type='hidden' name='dncoding[]' value='".$_POST[dncoding][$x]."'/>
                                <input type='hidden' name='dcargofijos[]' value='".$_POST[dcargofijos][$x]."'/>
                                <input type='hidden' name='dtarifas[]' value='".$_POST[dtarifas][$x]."'/>
                                <input type='hidden' name='dsubsidios[]' value='".$_POST[dsubsidios][$x]."'/>
                                <input type='hidden' name='ddescuentos[]' value='".$_POST[ddescuentos][$x]."'/>
                                <input type='hidden' name='dcontribuciones[]' value='".$_POST[dcontribuciones][$x]."'/>
                                <input type='hidden' name='dsaldos[]' value='".$_POST[dsaldos][$x]."'/>
                                <input type='hidden' name='dvalores[]' value='".$_POST[dvalores][$x]."'/>
                                <input type='hidden' name='dinteres[]' value='".$_POST[dinteres][$x]."'/>
                                <input type='hidden' name='dtotales[]' value='".$total."'/>
								<input type='hidden' name='dabonos[]' value='".$_POST[dabonos][$x]."'  >
                                <tr class='$iter1' style='text-transform:uppercase' onMouseOver=\"anterior=this.style.backgroundColor; this.style.backgroundColor='#40b3ff';\" onMouseOut=\"this.style.backgroundColor=anterior\">
                                    <td>".$_POST[dcoding][$x]."</td>
                                    <td>".$_POST[dncoding][$x]."</td>
                                    <td>".$_POST[dcargofijos][$x]."</td>
                                    <td style='text-align:right;'>$ ".number_format($_POST[dtarifas][$x],2,',','.')."</td>
                                    <td style='text-align:right;'>$ ".number_format($_POST[dsubsidios][$x],2,',','.')."</td>
                                    <td style='text-align:right;'>$ ".number_format($_POST[ddescuentos][$x],2,',','.')."</td>
                                    <td style='text-align:right;'>$ ".number_format($_POST[dcontribuciones][$x],2,',','.')."</td>
                                    <td style='text-align:right;'>$ ".number_format($_POST[dsaldos][$x],2,',','.')."</td>
                                    <td style='text-align:right;'>$ ".number_format($_POST[dvalores][$x],2,',','.')."</td>
                                    <td style='text-align:right;'>$ ".number_format($_POST[dinteres][$x],2,',','.')."</td>
                                    <td style='text-align:right;'>$ ".number_format($total,2,',','.')."</td>
                                    <td style='text-align:right;'>$ ".number_format($_POST[dabonos][$x],2,',','.')."</td>
                                </tr>";
                                $_POST[totalc]=$_POST[totalc]+$total;
                                $_POST[totalab]+=$_POST[dabonos][$x];
                                $_POST[totalcf]=number_format($_POST[totalc],0);
                                $aux=$iter1;
                                $iter1=$iter2;
                                $iter2=$aux;
                            }
                            $resultado = convertir($_POST[totalc],"PESOS");
                            $_POST[letras]=$resultado." PESOS M/CTE";
                            echo "
							<input type='hidden' name='totalcf' value='$_POST[totalcf]'/>
                           	<input type='hidden' name='totalc' value='$_POST[totalc]'/>
							<input type='hidden' name='totalab' value='$_POST[totalab]'/>
                            <tr class='$iter1'>
                                <td colspan='10'>Total</td>
                                <td  style='text-align:right;'>$ ".number_format($_POST[totalc],2,',','.')."</td>
                                <td  style='text-align:right;'>$ ".number_format($_POST[totalab],2,',','.')."</td>
                            </tr>";
                        ?>
                    </table>
                </div>
            </div>
			<?php 
				if($_POST[oculto]==2)
 				{
  					$bloq=bloqueos($_SESSION[cedulausu],$_POST[fecha]);	
					if($bloq>=1)
					{
	 					$concecc=selconsecutivo('servreciboscaja','id_recibos');
						if($_POST[modorec]=='caja')
			  			{				 
							$cuentacb=$_POST[cuentacaja];
							$cajas=$_POST[cuentacaja];
							$cbancos="";
			  			}
						if($_POST[modorec]=='banco')
			    		{
							$cuentacb=$_POST[banco];				
							$cajas="";
							$cbancos=$_POST[banco];
			    		}
						$sqlr="INSERT INTO servreciboscaja (id_comp,fecha,vigencia,id_recaudo,recaudado,cuentacaja,cuentabanco,valor,estado,tipo) values (0,'$_POST[fecha]','$vigusu',$factiva,'$_POST[modorec]','$cajas','$cbancos',$_POST[totalab],'S','5')";
						mysql_query($sqlr,$linkbd);
		 				$idcomp=mysql_insert_id();
						$tam=count($_POST[dabonos]);
						for($x=0;$x<$tam;$x++)
						{
		 					$dif=$_POST[dtotales][$x]-$_POST[dabonos][$x];
		  					$sqlr="INSERT INTO servreciboscaja_det (id_recibos,ingreso,cargofijo,tarifa,subsidio,descuento,contribucion, saldoanterior,intereses,subtotal,valor,estado) VALUES ($idcomp,'".$_POST[dcoding][$x]."',".$_POST[dcargofijos][$x].",'".$_POST[dtarifas][$x]."',".$_POST[dsubsidios][$x].",'".$_POST[ddescuentos][$x]."',".$_POST[dcontribuciones][$x].",'".$_POST[dsaldos][$x]."',".$_POST[dinteres][$x].", ".$_POST[dvalores][$x].",".$_POST[dabonos][$x].",'S')";
		  					mysql_query($sqlr,$linkbd);  
		 					$sqlr="UPDATE SERVLIQUIDACIONES_det SET ABONO=ABONO+".$_POST[dabonos][$x]." WHERE ID_LIQUIDACION='$factiva' AND servicio='".$_POST[dcoding][$x]."'";
							mysql_query($sqlr,$linkbd);
							$xyx=0;
							$yy=0;
							$datidgen=array(); 
							$datidliq=array();
							$dattarif=array();
							$datsubsi=array();
							$datsaldo=array();
							$datvalli=array();
							$databono=array();
							$databgen=array();
							$datinter=array();
							$datinabo=array();
							$datestad=array();
							$sqlr="
							SELECT id_det,id_liquidacion,tarifa,subsidio,saldo,valorliquidacion,abono,abonogen,intereses,inabono,estado
							FROM servliquidaciones_det 
							WHERE codusuario='$_POST[cod_usuario]' AND servicio='".$_POST[dcoding][$x]."'
							ORDER BY id_det DESC";
							
							$resp = mysql_query($sqlr,$linkbd);
							while (($row =mysql_fetch_row($resp))&&($xyx<1)) 
							{
								$datidgen[$yy]=$row[0];
								$datidliq[$yy]=$row[1];
								$dattarif[$yy]=$row[2];
								$datsubsi[$yy]=$row[3];
								$datsaldo[$yy]=$row[4];
								$datvalli[$yy]=$row[5];
								$databono[$yy]=$row[6];
								$databgen[$yy]=$row[7];
								$datinter[$yy]=$row[8];
								$datinabo[$yy]=$row[9];
								$datestad[$yy]=$row[10];
								if($row[4]==0){$xyx=2;}
								$yy++;
							}
							$totalabonos=$abonres=array_sum($databono);
							$varcont=count($datidgen)-1;
							for ($xf=$varcont;$xf>=0;$xf--)
							{
								$restabonos=$abonres-$databono[$xf]+$datinabo[$xf];
								if($restabonos>=0 && $abonres>0)
								{
									$sqlr3="UPDATE servliquidaciones_det SET inabono=intereses WHERE id_det ='$datidgen[$xf]'";
									mysql_query($sqlr3,$linkbd);
									$valoreal=$dattarif[$xf]-$datsubsi[$xf]+$datsaldo[$xy];
									$restabonos=$abonres-$valoreal;
									if($restabonos>=0 && $abonres>0)
									{
										$abonres=$restabonos;
										$sqlr3="UPDATE servliquidaciones_det SET estado='PA', abonogen='$valoreal' WHERE id_det ='$datidgen[$xf]'";
										mysql_query($sqlr3,$linkbd);
									}
									elseif($abonres>0)
									{
										$sqlr3="UPDATE servliquidaciones_det SET estado='A', abonogen='$abonres' WHERE id_det ='$datidgen[$xf]'";
										mysql_query($sqlr3,$linkbd);
										$abonres=0;
									}
								}
								elseif($abonres>0)
								{
									if($datinter[$xf]>$abonres)
									{
										$sqlr3="UPDATE servliquidaciones_det SET estado='A',inabono='$abonres' WHERE id_det ='$datidgen[$xf]'";
										mysql_query($sqlr3,$linkbd);
									}
									elseif($datinter[$xf]==0)
									{
										$sqlr3="UPDATE servliquidaciones_det SET estado='A',abonogen='$abonres', inabono=intereses WHERE id_det ='$datidgen[$xf]'";
										mysql_query($sqlr3,$linkbd);
									}
									else
									{
										$sqlr3="UPDATE servliquidaciones_det SET estado='A',abonogen='$abonres - intereses', inabono=intereses WHERE id_det ='$datidgen[$xf]'";
										mysql_query($sqlr3,$linkbd);
									}
								}
							}
						}		
						$sqlr="UPDATE SERVLIQUIDACIONES SET ESTADO='A' WHERE FACTURA=$factiva";
						mysql_query($sqlr,$linkbd);
						$sqlr="update servfacturas set estado='A' WHERE id_factura=$factiva";
						mysql_query($sqlr,$linkbd);
						//modificar el saldo del tercero
						$sqlr="update servclientes set intereses=intereses-".array_sum($_POST[dinteres])." WHERE codigo='$_POST[cod_usuario]'";
						mysql_query($sqlr,$linkbd);
					}
					else {echo "<div class='inicio'><img src='imagenes\alert.png'> No Tiene los Permisos para Modificar este Documento</div>";} 
 				}
			?> 
			<div id="bgventanamodal2">
                <div id="ventanamodal2">
                    <IFRAME  src="" name="buster" marginWidth=0 marginHeight=0 frameBorder=0 id="ventana2" frameSpacing=0 style="left:500px; width:900px; height:500px; top:200;"> 
                    </IFRAME>
                </div>
       	 	</div>
 		</form>
	</body>
</html>