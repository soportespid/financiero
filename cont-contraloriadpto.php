<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	ini_set('max_execution_time',3600);

	require "comun.inc";
	require "funciones.inc";
	require "conversor.php";
	require "validaciones.inc";

	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");

	session_start();
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html lang="es">
	<head>
		<meta charset="utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1"/>
		<meta name="viewport" content="user-scalable=no">
		<title>:: IDEAL 10 - Informes</title>
		<link href="favicon.ico" rel="shortcut icon"/>
        <link href="css/css2.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="css/programas.js"></script>
		<script>
		function pagina(){
			alert();


		}
		</script>
    </head>
	<style>

	</style>
    <body>
        <IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
        <span id="todastablas2"></span>
        <table>
            <tr><script>barra_imagenes("info");</script><?php cuadro_titulos();?></tr>
            <tr><?php menu_desplegable("info");?></tr>
        	<tr>
          		<td colspan="3" class="cinta">
					<a class="mgbt"><img src="imagenes/add2.png" /></a>
					<a class="mgbt"><img src="imagenes/guardad.png" style="width:24px;"/></a>
					<a class="mgbt"><img src="imagenes/buscad.png"/></a>
					<a onClick="mypop=window.open('plan-agenda.php','','');mypop.focus()" class="mgbt"><img src="imagenes/agenda1.png" title="Agenda" /></a>
					<a href="#" onClick="mypop=window.open('info-principal.php','','');mypop.focus();" class="mgbt"><img src="imagenes/nv.png" title="Nueva Ventana"></a>
				</td>
        	</tr>
        </table>
 		<form name="form2" method="post" action="">
    		<table class="inicio">
      			<tr>
        			<td class="titulos" colspan="2">.: Informes Contraloria Departamental </td>
        			<td class="cerrar" style="width:7%;" ><a href="info-principal.php">&nbsp;Cerrar</a></td>
      			</tr>
					<td width="70%" style="background-repeat:no-repeat; background-position:center;">
						<ol id="lista2">
							<!-- <li onClick="location.href='cont-formatof01-agr.php'" style="cursor:pointer;">FORMATO_ANIO_MES_F01_AGR</li> -->
							<!-- <li onClick="location.href='cont-formatof01-agr-acumulado.php'" style="cursor:pointer;">FORMATO_ANIO_MES_F01_AGR ACUMULADO</li> -->
							<li onClick="location.href='cont-formatof01-agr-reg.php'" style="cursor:pointer;">FORMATO_ANIO_MES_F01_AGR ADMINISTRACION CENTRAL Y REGALIAS</li>
							<li onClick="location.href='cont-formatof01_balanceprueba.php'" style="cursor:pointer;">FORMATO_ANIO_MES_F01_AGR_ANEXO6_BALANCEPRUEBA</li>
							<li onClick="location.href='ccp-f03cuentasbancarias.php'" style="cursor:pointer;">F03_AGR - CUENTAS BANCARIAS</li>
                        </ol>
					</td>
					<td width="33%" align="center"  >
						<img src="imagenes/contraloriadpto.jpg" >
					</td>
				</tr>
    		</table>
		</form>
	</body>
</html>
