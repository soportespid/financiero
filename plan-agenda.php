<?php //V 1000 12/12/16 ?>
<?php
require "comun.inc";
require "funciones.inc";
$linkbd = conectar_v7();
session_start();
cargarcodigopag($_GET['codpag'], $_SESSION["nivel"]);
header("Cache-control: private"); // Arregla IE 6
date_default_timezone_set("America/Bogota");
?>
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta http-equiv="Content-Type" content="text/html;" />
    <meta http-equiv="X-UA-Compatible" content="IE=9" />
    <title>:: IDEAL 10 - Planeaci&oacute;n Estrat&eacute;gica</title>
    <link href="css/css2.css" rel="stylesheet" type="text/css" />
    <link href="css/css3.css" rel="stylesheet" type="text/css" />
    <link href="css/style.css" rel="stylesheet" type="text/css" />
    <link href="calendario1/css/agenda.css" rel="stylesheet" type="text/css" />
    <link href="calendario1/css/agenda2.css" rel="stylesheet" type="text/css" />
    <link rel="shortcut icon" href="favicon.ico" />
    <script type="text/javascript" src="calendario1/agenda.js"></script>
    <script type="text/javascript" src="css/programas.js"></script>
    <script type="text/javascript">
        function despliegamodal2(_valor) {
            document.getElementById("bgventanamodal2").style.visibility = _valor;
            if (_valor == "hidden") { document.getElementById('todastablas').innerHTML = "" }
        }
        function actulizar(idfecha, idlinea) {
            despliegamodal2("hidden");
            var winat = "actualizar";
            var pagaux = 'calendario1/mensajes-actualizar.php?fecha=' + idfecha + '&' + 'horaini=' + idlinea;
            document.getElementById('todastablas').innerHTML = '<div id="bgventanamodal2"><div id="ventanamodal2"><IFRAME  src="' + pagaux + '" name="buster" marginWidth=0 marginHeight=0 frameBorder=0 id="ventana2" frameSpacing=0 style="left:500px; width:900px; height:500px; top:200;"></IFRAME></div></div>';
            despliegamodal2('visible');
        }
        function prueba() { var ca = document.getElementById('fecha'); ca.focus(); }
        function alertadiaria(afecha) {
            var winat = "alertan";
            var pagaux = 'calendario1/malertas.php?fecha=' + afecha;
            document.getElementById('todastablas').innerHTML = '<div id="bgventanamodal2"><div id="ventanamodal2"><IFRAME  src="' + pagaux + '" name="buster" marginWidth=0 marginHeight=0 frameBorder=0 id="ventana2" frameSpacing=0 style="left:500px; width:900px; height:500px; top:200;"></IFRAME></div></div>';
            despliegamodal2('visible');
        }
        function ventanaNueva2(idlinea) {
            var contlinea = document.getElementById(idlinea).innerHTML;
            var elemento = contlinea.split('/');
            if (contlinea == "") {
                var winat = "nuevo";
                var pagaux = 'calendario1/mensajes.php?fecha=' + document.form2.fecha.value + '&' + 'horaini=' + idlinea;
            }
            else if (elemento[0] != "--") {
                var winat = "actualizar";
                var pagaux = 'calendario1/mensajes-mirar.php?fecha=' + document.form2.fecha.value + '&' + 'horaini=' + idlinea;
            }
            else {
                var winat = "actualizar";
                var pagaux = 'calendario1/mensajes-mirar.php?fecha=' + document.form2.fecha.value + '&' + 'horaini=' + elemento[1];
            }
            document.getElementById('todastablas').innerHTML = '<div id="bgventanamodal2"><div id="ventanamodal2"><IFRAME  src="' + pagaux + '" name="buster" marginWidth=0 marginHeight=0 frameBorder=0 id="ventana2" frameSpacing=0 style="left:500px; width:900px; height:500px; top:200;"></IFRAME></div></div>';
            despliegamodal2('visible');
        }
        var mensaven = 0; var mensatotal = 0; var mensapen = 0;
        //addEventListener('unload',parent.document.revisamensajes(),false);

    </script>
</head>

<body>
    <IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
    <span id="todastablas2"></span>
    <?php //Calcula el total de Mensajes
    $hoy = date('Y-m-d');
    $sqlr = "SELECT * FROM agenda WHERE usrecibe='$_SESSION[cedulausu]'";
    $res = mysqli_query($linkbd, $sqlr);
    while ($rowEmp = mysqli_fetch_assoc($res)) {
        $dosss; ?>
        <script>mensatotal++;</script><?php }
    // calcu vencidos la los mensajes
    $sqlr = "SELECT * FROM agenda WHERE fechaevento<'$hoy' AND usrecibe='$_SESSION[cedulausu]' AND estado='A' ";
    $res = mysqli_query($linkbd, $sqlr);
    while ($rowEmp = mysqli_fetch_assoc($res)) {
        $dosss; ?>
        <script>mensaven++;</script><?php }
    // calcula los eventos pendientes
    $sqlr = "SELECT * FROM agenda WHERE fechaevento>='$hoy' AND usrecibe='$_SESSION[cedulausu]' AND estado='A'";
    $res = mysqli_query($linkbd, $sqlr);
    while ($rowEmp = mysqli_fetch_assoc($res)) {
        $dosss; ?>
        <script>mensapen++;</script><?php } ?>
    <table>
        <tr>
            <script> barra_imagenes("meci");</script><?php cuadro_titulos(); ?>
        </tr>
        <tr><?php menu_desplegable("meci"); ?></tr>
        <tr>
            <script>var pagini = '<?php echo $_GET['pagini']; ?>';</script>
    </table>
    <div class="bg-white group-btn p-1"><button type="button" onclick="window.location.href='plan-agenda.php'"
            class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
            <span>Nuevo</span>
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960">
                <path d="M440-440H200v-80h240v-240h80v240h240v80H520v240h-80v-240Z"></path>
            </svg>
        </button><button type="button" onclick="window.location.href='plan-agendabusca.php'"
            class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
            <span>Buscar</span>
            <svg class="fill-black group-hover:fill-white w-5 h-5" xmlns="http://www.w3.org/2000/svg"
                viewBox="0 -960 960 960">
                <path
                    d="M784-120 532-372q-30 24-69 38t-83 14q-109 0-184.5-75.5T120-580q0-109 75.5-184.5T380-840q109 0 184.5 75.5T640-580q0 44-14 83t-38 69l252 252-56 56ZM380-400q75 0 127.5-52.5T560-580q0-75-52.5-127.5T380-760q-75 0-127.5 52.5T200-580q0 75 52.5 127.5T380-400Z">
                </path>
            </svg>
        </button><button type="button" onclick="mypop=window.open('plan-agenda.php','','');mypop.focus()"
            class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
            <span class="group-hover:text-white">Agenda</span>
            <svg class="fill-black group-hover:fill-white w-5 h-5" xmlns="http://www.w3.org/2000/svg"
                viewBox="0 -960 960 960">
                <path
                    d="M560-564v-68q33-14 67.5-21t72.5-7q26 0 51 4t49 10v64q-24-9-48.5-13.5T700-600q-38 0-73 9.5T560-564Zm0 220v-68q33-14 67.5-21t72.5-7q26 0 51 4t49 10v64q-24-9-48.5-13.5T700-380q-38 0-73 9t-67 27Zm0-110v-68q33-14 67.5-21t72.5-7q26 0 51 4t49 10v64q-24-9-48.5-13.5T700-490q-38 0-73 9.5T560-454ZM260-320q47 0 91.5 10.5T440-278v-394q-41-24-87-36t-93-12q-36 0-71.5 7T120-692v396q35-12 69.5-18t70.5-6Zm260 42q44-21 88.5-31.5T700-320q36 0 70.5 6t69.5 18v-396q-33-14-68.5-21t-71.5-7q-47 0-93 12t-87 36v394Zm-40 118q-48-38-104-59t-116-21q-42 0-82.5 11T100-198q-21 11-40.5-1T40-234v-482q0-11 5.5-21T62-752q46-24 96-36t102-12q58 0 113.5 15T480-740q51-30 106.5-45T700-800q52 0 102 12t96 36q11 5 16.5 15t5.5 21v482q0 23-19.5 35t-40.5 1q-37-20-77.5-31T700-240q-60 0-116 21t-104 59ZM280-494Z">
                </path>
            </svg>
        </button><button type="button" onclick="mypop=window.open('meci-principal.php','','');mypop.focus();"
            class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
            <span>Nueva ventana</span>
            <svg class="fill-black group-hover:fill-white w-5 h-5" xmlns="http://www.w3.org/2000/svg"
                viewBox="0 -960 960 960">
                <path
                    d="M320-120v-80h80v-80H160q-33 0-56.5-23.5T80-360v-400q0-33 23.5-56.5T160-840h640q33 0 56.5 23.5T880-760v400q0 33-23.5 56.5T800-280H560v80h80v80H320ZM160-360h640v-400H160v400Zm0 0v-400 400Z">
                </path>
            </svg>
        </button><button type="button" onclick="mypop=window.open('/financiero/plan-agenda.php','','');mypop.focus();"
            class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
            <span class="group-hover:text-white">Duplicar pantalla</span>
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960">
                <path
                    d="M640-160v-360H160v360h480Zm80-200v-80h80v-360H320v200h-80v-200q0-33 23.5-56.5T320-880h480q33 0 56.5 23.5T880-800v360q0 33-23.5 56.5T800-360h-80ZM160-80q-33 0-56.5-23.5T80-160v-360q0-33 23.5-56.5T160-600h480q33 0 56.5 23.5T720-520v360q0 33-23.5 56.5T640-80H160Zm400-603ZM400-340Z">
                </path>
            </svg>
        </button></div>
    <form name="form2" method="post">
        <?php
        if ($_POST['oculnicio'] == "") {
            $fechamen = $_GET['fechamen'];
        }
        if ($fechamen != "") {
            $_POST['fecha'] = $fechamen;
            $fecha = $fechamen;
        }
        if ($_POST['fecha'] == "") {
            $_POST['fecha'] = date("Y-m-d");
        }
        preg_match("([0-9]{2,4})-([0-9]{1,2})-([0-9]{1,2})", $_POST['fecha'], $fecha);
        $age = $fecha[1];
        $mes = $fecha[2];
        $dia = $fecha[3];
        $lastday = mktime(0, 0, 0, $mes, $dia, $age);
        ?>
        <table class="inicio">
            <tbody>
                <tr>
                    <td id="nfecha" class="titulos" colspan="2"></td>
                    <script>boton_cerrar();</script>
                </tr>
                <tr>
                    <td class="saludo1" colspan="3"> Seleccione el D&iacute;a</td>
                </tr>
                <tr>
                    <td valign="top" align="center">
                        <div class="minicalendario">
                            <div class="minicalendario_age"><a href="#" onClick="prueba() "><?php echo $age ?></a></div>
                            <div class="minicalendario_mes"><?php echo strtoupper(strftime('%B', $lastday)) ?></div>
                            <div class="minicalendario_dia"><?php echo $dia ?></div>
                            <div class="minicalendario_base">::: <?php echo ucfirst(strftime('%A', $lastday)) ?> :::
                            </div>
                        </div>
                        <input type="date" name="fecha" id="fecha" onChange="document.form2.submit()"
                            value="<?php echo $_POST['fecha'] ?>">
                        <input type="hidden" name="oculto" value="0"><br><br>
                        <div>
                            <table class="inicio">
                                <tr>
                                    <td colspan="2" id="pruel" class="saludo3">Clasificaci&oacute;n de Eventos
                                        <?php
                                        $linkbd = conectar_v7();
                                        $sqlr = "SELECT valor_final,descripcion_valor FROM dominios WHERE nombre_dominio = 'PRIORIDAD_EVENTOS_AG'";
                                        $res = mysqli_query($linkbd, $sqlr);
                                        while ($colmensa = mysqli_fetch_assoc($res)) {
                                            $colorprioridad = $colmensa['valor_final'];
                                            $nombreprioridad = $colmensa['descripcion_valor'];
                                            ?>
                                            <script>
                                                document.write('<tr><td width="20px" bgcolor="<?php echo $colorprioridad; ?>"></td> <td class="saludo3"><?php echo $nombreprioridad; ?> </td></tr>');
                                            </script>
                                            <?php
                                        }
                                        ?>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div>
                            <table class="inicio">
                                <tr>
                                    <td class="saludo3">
                                        <label>Eventos Pendientes: </label><label class="etiq2">
                                            <script>document.write(mensapen);</script>
                                        </label>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="saludo3">
                                        <label>Eventos Vencidos: </label><label class="etiq2">
                                            <script>document.write(mensaven);</script>
                                        </label>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="saludo3">
                                        <label>Total Eventos: </label><label class="etiq2">
                                            <script>document.write(mensatotal);</script>
                                        </label>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div>
                            <table class="inicio">
                                <tr>
                                    <td class="saludo3">Recordatorios:</td>
                                    <td>
                                        <a href="#"><img src="imagenes/add.png" alt="Buscar" border="0"
                                                onClick="alertadiaria('<?php echo $_POST['fecha']; ?>');"
                                                style="width:20px;height:20px; -webkit-box-shadow: 2px 3px 4px rgba(0,0,0,.5);"
                                                title="Adicionar" /></a>
                                    </td>
                                    <td>
                                        <a href="plan-alertasbusca.php"><img src="imagenes/busca.png" alt="Buscar"
                                                style="width:19px;height:19px;-webkit-box-shadow: 2px 3px 4px rgba(0,0,0,.5);"
                                                title="Programados" /></a>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </td>
                    <td colspan="1">
                        <div id="agrupar">
                            <aside class="inicio">
                                <article>
                                    <header>
                                        <div id="calendario" style="width:auto; height:400px; overflow:scroll;">
                                            <script>
                                                var body = document.getElementById('calendario'); var nids = 0; var nhoras = 7;
                                                for (var k = 0; k < 17; k++) {
                                                    var tabla = document.createElement("table");
                                                    var tblBody = document.createElement("tbody");
                                                    var cuartosH = 0;
                                                    var atid = "";
                                                    for (var i = 0; i < 4; i++) {
                                                        var hilera = document.createElement("tr");
                                                        if (i == 0) {
                                                            var celda1 = document.createElement("td");
                                                            nhoras++;
                                                            if (nhoras < 13) { var textoCelda = document.createTextNode(nhoras + "am"); }
                                                            else {
                                                                var rhoras = nhoras - 12;
                                                                var textoCelda = document.createTextNode(rhoras + "pm");
                                                            }
                                                            celda1.setAttribute("rowspan", "6");
                                                            celda1.appendChild(textoCelda);
                                                            celda1.setAttribute("class", "choras");
                                                            hilera.appendChild(celda1);
                                                        }
                                                        for (var j = 0; j < 2; j++) {
                                                            var celda = document.createElement("td");
                                                            if (j == 0) {
                                                                nids++;
                                                                if (cuartosH == 0) {
                                                                    var textoCelda = document.createTextNode(":00 - 15");
                                                                    if (nhoras < 10) { atid = "0" + nhoras + ":00:00"; }
                                                                    else { atid = nhoras + ":00:00"; }
                                                                }
                                                                else {
                                                                    var cHh = cuartosH + 15;
                                                                    var textoCelda = document.createTextNode(":" + cuartosH + " - " + cHh);
                                                                    if (nhoras < 10) { atid = "0" + nhoras + ":" + cuartosH + ":00"; }
                                                                    else { atid = nhoras + ":" + cuartosH + ":00"; }
                                                                }
                                                                celda.setAttribute("class", "cminutos");
                                                                cuartosH = cuartosH + 15;
                                                            }
                                                            else {
                                                                celda.setAttribute("id", atid);
                                                                var textoCelda = document.createTextNode("");
                                                                celda.setAttribute("class", "cmensaje");
                                                                celda.setAttribute("onClick", "ventanaNueva2(this.id)");
                                                            }
                                                            celda.appendChild(textoCelda);
                                                            hilera.appendChild(celda);
                                                        }
                                                        tblBody.appendChild(hilera);
                                                    }
                                                    tabla.appendChild(tblBody);
                                                    body.appendChild(tabla);
                                                    tabla.setAttribute("border", "1");
                                                    tabla.setAttribute("class", "tmensajes");
                                                    tblBody.setAttribute("margin", "0");
                                                }
                                                window.addEventListener('load', fecha_actual, false);
                                            </script>
                                        </div>
                                    </header>
                                </article>
                            </aside>
                        </div>
                    </td>
                </tr>
            </tbody>
        </table>
        <input id="horaocul" name="horaocul" type="hidden" value="0">
        <input id="oculnicio" name="oculnicio" type="hidden" value="1">
    </form>
    <span id="todastablas"></span>
    <?php
    $linkbd = conectar_v7();
    $sqlr = "SELECT * FROM agenda WHERE usrecibe='$_SESSION[cedulausu]' AND fechaevento='$_POST[fecha]' ";
    $res = mysqli_query($linkbd, $sqlr);
    while ($rowEmp = mysqli_fetch_assoc($res)) {
        $fechven = $rowEmp['horainicial'];
        $fechsum = explode(":", $fechven);
        $fechfin = explode(":", $rowEmp['horafinal']);
        $conini = date('H:i:s', mktime($fechsum[0], $fechsum[1], 0, 0, 0, 0));
        $confin = date('H:i:s', mktime($fechfin[0], $fechfin[1], 0, 0, 0, 0));
        $nomeven = $rowEmp['evento'];
        $desevento = $rowEmp['descripcion'];
        $prioeven = $rowEmp['prioridad'];
        $sqlr2 = "SELECT valor_final FROM dominios WHERE nombre_dominio='PRIORIDAD_EVENTOS_AG' AND valor_inicial='$prioeven'";
        $res2 = mysqli_query($linkbd, $sqlr2);
        $colmensa = mysqli_fetch_assoc($res2);
        $colorevento = $colmensa['valor_final'];
        $sqlr3 = "SELECT descripcion_valor FROM dominios WHERE nombre_dominio='TIPO_EVENTO_AG' AND valor_inicial='$nomeven'";
        $res3 = mysqli_query($linkbd, $sqlr3);
        $temensa = mysqli_fetch_assoc($res3);
        $texmensaje = $temensa['descripcion_valor'];
        $band1 = 0;
        while ($conini <= $confin) {
            ?>
            <script>
                var ingresat = document.getElementById('<?php echo $conini; ?>');
                var priori = '<?php echo $prioeven; ?>'
                var bandera = '<?php echo $band1; ?>';
                ingresat.style.backgroundColor = '<?php echo $colorevento; ?>';
                if (bandera == '0') {
                    ingresat.style.color = "#FFFFFF";
                    ingresat.innerHTML = ('<?php echo $texmensaje; ?>' + ' - ' + '<?php echo $desevento; ?>');
                }
                else {
                    ingresat.style.color = '<?php echo $colorevento; ?>';
                    ingresat.innerHTML = '--/' + '<?php echo $fechven; ?>';
                }
            </script><?php
            $fechsum[1] = $fechsum[1] + 15;
            $conini = date('H:i:s', mktime($fechsum[0], $fechsum[1], 0, 0, 0, 0));
            $band1 = $band1 + 1;
        }
    }
    ?>
</body>

</html>
