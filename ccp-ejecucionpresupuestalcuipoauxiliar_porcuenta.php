<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require "comun.inc";
	require "funciones.inc";
	sesion();
	$linkbd=conectar_v7();
	$linkbd -> set_charset("utf8");
	cargarcodigopag($_GET[codpag],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
	ini_set('max_execution_time', 720);
?>
<!doctype >
<html lang="es">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=9">
		<title>:: Ideal - Presupuesto</title>
		<link href="css/css2.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css">
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css">
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css">
		<script type="text/javascript" src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script>
			function despliegamodalm(_valor,_tip,mensa,pregunta)
			{
				document.getElementById("bgventanamodalm").style.visibility=_valor;
				if(_valor=="hidden"){document.getElementById('ventanam').src="";}
				else
				{
					switch(_tip)
					{
						case "1":	document.getElementById('ventanam').src="ventana-mensaje1.php?titulos="+mensa;break;
						case "2":	document.getElementById('ventanam').src="ventana-mensaje3.php?titulos="+mensa;break;
						case "3":	document.getElementById('ventanam').src="ventana-mensaje2.php?titulos="+mensa;break;
						case "4":	document.getElementById('ventanam').src="ventana-consulta1.php?titulos="+mensa+"&idresp="+pregunta;break;
					}
				}
			}
			function excell2(cuenta,seccpresupuestal,programatico,cpc,fuente,bpin,vigengasto)
			{
				document.form2.action = "ccp-ejecucionpresupuestalcuipoexcel2.php?cuenta=" + cuenta + "&seccpresupuestal=" + seccpresupuestal + "&programatico=" + programatico + "&cpc=" + cpc + "&fuente=" + fuente + "&bpin=" + bpin + "&vigengasto=" + vigengasto;
				document.form2.target = "_BLANK";
				document.form2.submit();
				document.form2.action = "";
				document.form2.target = "";
			}
			function ir_egreso_nomina(numegreso)
			{
				document.form2.action = "teso-pagonominaver.php?idegre=" + numegreso;
				document.form2.target = "_BLANK";
				document.form2.submit();
				document.form2.action = "";
				document.form2.target = "";
			}
			function ir_egresos(numegreso)
			{
				document.form2.action = "teso-girarchequesver-ccpet.php?idegre=" + numegreso;
				document.form2.target = "_BLANK";
				document.form2.submit();
				document.form2.action = "";
				document.form2.target = "";
			}
			function ir_obligacion_nomina(numobligacion)
			{
				document.form2.action = "hum-liquidarnominamirar.php?idnomi=" + numobligacion;
				document.form2.target = "_BLANK";
				document.form2.submit();
				document.form2.action = "";
				document.form2.target = "";
			}
			function ir_obligaciones(numobligacion)
			{
				document.form2.action = "teso-egresoverccpet.php?idop=" + numobligacion;
				document.form2.target = "_BLANK";
				document.form2.submit();
				document.form2.action = "";
				document.form2.target = "";
			}
			function ir_compromisos(numcompromiso, vigencia)
			{
				document.form2.action = "ccp-rpver.php?is=" + numcompromiso + "&vig=" + vigencia;
				document.form2.target = "_BLANK";
				document.form2.submit();
				document.form2.action = "";
				document.form2.target = "";
			}
			function ir_disponibilidad(numcompromiso, vigencia)
			{
				document.form2.action = "ccp-cdpver.php?is=" + numcompromiso + "&vig=" + vigencia;
				document.form2.target = "_BLANK";
				document.form2.submit();
				document.form2.action = "";
				document.form2.target = "";
			}
		</script>
		<?php
			function buscarfuentecuipo($codigo)
			{
				$linkbd=conectar_v7();
				$linkbd -> set_charset("utf8");
				$sqlmax = "SELECT MAX(version) FROM ccpet_fuentes_cuipo";
				$resmax = mysqli_query($linkbd, $sqlmax);
				$rowmax = mysqli_fetch_row($resmax);
				$sql = "SELECT concuipocpc FROM ccpet_fuentes_cuipo WHERE codigo_fuente = '$codigo' AND version = '$rowmax[0]'";
				$res = mysqli_query($linkbd,$sql);
				$totalcli=mysqli_num_rows($res);
				$row = mysqli_fetch_row($res);
				return($row[0]);
			}
			function buscarcpccuipo($codigo)//0-4
			{
				$linkbd=conectar_v7();
				$linkbd -> set_charset("utf8");
				if(substr($codigo,0,1) <= 4)
				{
					$sqlmax = "SELECT MAX(version) FROM ccpetbienestransportables";
					$resmax = mysqli_query($linkbd, $sqlmax);
					$rowmax = mysqli_fetch_row($resmax);
					$sql = "SELECT concuipocpc FROM ccpetbienestransportables WHERE cpc = '$codigo' AND version = '$rowmax[0]'";
				}
				else
				{
					$sqlmax = "SELECT MAX(version) FROM ccpetservicios";
					$resmax = mysqli_query($linkbd, $sqlmax);
					$rowmax = mysqli_fetch_row($resmax);
					$sql = "SELECT concuipocpc FROM ccpetservicios WHERE cpc = '$codigo' AND version = '$rowmax[0]'";
				}
				$res = mysqli_query($linkbd,$sql);
				$row = mysqli_fetch_row($res);
				return($row[0]);
			}
			function buscarprogramatico($codigo,$tipo)
			{
				$linkbd=conectar_v7();
				$linkbd -> set_charset("utf8");
				$codm1=substr($codigo,0,4);
				if($tipo == '1'){$sqlproducto = "SELECT concuipom1 FROM ccpetproductos WHERE cod_producto LIKE '$codm1%'";}
				else {$sqlproducto = "SELECT concuipom2 FROM ccpetproductos WHERE cod_producto LIKE '$codm1%'";}
				$resproducto = mysqli_query($linkbd,$sqlproducto);
				$rowproducto = mysqli_fetch_row($resproducto);
				return($rowproducto[0]);
			}
			titlepag();
		?>
</head>
	<body>
		<IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
		<span id="todastablas2"></span>
		<?php
				if($_GET['cuenta']!='')
				{
					$_POST['vcuenta'] = $_GET['cuenta'];
					$_POST['vseccpresupuestal'] = $_GET['seccpresupuestal'];
					$_POST['vprogramatico'] = $_GET['programatico'];
					$_POST['vcpc'] = $_GET['cpc'];
					$_POST['vfuente'] = $_GET['fuente'];
					$_POST['vbpin'] = $_GET['bpin'];
					$_POST['vvigengasto'] = $_GET['vigengasto'];
					
				}
				if(!$_POST['oculto1']){$_POST['selcdp'] = '-1';}
			?>
		<table>
			<tr>
				<script>barra_imagenes("ccpet");</script><?php cuadro_titulos(); ?></tr>
			<tr><?php menu_desplegable("ccpet"); ?></tr>
			<tr>
				<td colspan="3" class="cinta"><img src='imagenes/add2.png' title='Nuevo' class="mgbt1"><img src="imagenes/guardad.png" title="Guardar" class="mgbt1"><img src="imagenes/buscad.png" title="Buscar" class="mgbt1"><img src="imagenes/nv.png" title="Nueva Ventana" onClick="mypop=window.open('ccp-principal.php','','');mypop.focus();" class="mgbt"><img src="imagenes/excel.png" title="Excel" onClick="excell2(<?php echo "'".$_POST['vcuenta']."', '".$_POST['vseccpresupuestal']."', '".$_POST['vprogramatico']."', '".$_POST['vcpc']."', '".$_POST['vfuente']."', '".$_POST['vbpin']."', '".$_POST['vvigengasto']."'";?>)" class="mgbt"></td>
			</tr>
		</table>
		<div id="bgventanamodalm" class="bgventanamodalm">
			<div id="ventanamodalm" class="ventanamodalm">
				<IFRAME src="" name="ventanam" marginWidth=0 marginHeight=0 frameBorder=0 id="ventanam" frameSpacing=0 style=" width:700px; height:130px; top:200; overflow:hidden;"></IFRAME>
			</div>
		</div>
		<form name="form2" method="post" action="">
			<input type="hidden" name="vcuenta" id="vcuenta" value="<?php echo $_POST['vcuenta'];?>">
			<input type="hidden" name="vseccpresupuestal" id="vseccpresupuestal" value="<?php echo $_POST['vseccpresupuestal'];?>">
			<input type="hidden" name="vprogramatico" id="vprogramatico" value="<?php echo $_POST['vprogramatico'];?>">
			<input type="hidden" name="vcpc" id="vcpc" value="<?php echo $_POST['vcpc'];?>">
			<input type="hidden" name="vfuente" id="vfuente" value="<?php echo $_POST['vfuente'];?>">
			<input type="hidden" name="vbpin" id="vbpin" value="<?php echo $_POST['vbpin'];?>">
			<input type="hidden" name="vvigengasto" id="vvigengasto" value="<?php echo $_POST['vvigengasto'];?>">
			<table align="center" class="inicio">
				<tr>
					<td class="titulos" colspan="10"><p class="neon_titulos">.: Ejecución Presupuestal Cuenta Auxiliar</p></td>
					<td class="cerrar" style="width:7%" onClick="location.href='ccp-principal.php'">Cerrar</td>
				</tr>
				<tr>
					<td class="tamano01" style="width:2.5cm;">Disponibilidad:</td>
					<td style="width:10%;"><input type="text" name="totaldisponibilidad" id="totaldisponibilidad" value="<?php echo $_POST['totaldisponibilidad'];?>" style="width:98%;height:30px;" readonly></td>
					<td class="tamano01" style="width:2.5cm;">Compromisos:</td>
					<td style="width:10%;"><input type="text" name="totalcompromisos" id="totalcompromisos" value="<?php echo $_POST['totalcompromisos'];?>" style="width:98%;height:30px;" readonly></td>
					<td class="tamano01" style="width:2.5cm;">Obligaciones:</td>
					<td style="width:10%;"><input type="text" name="totalobligaciones" id="totalobligaciones" value="<?php echo $_POST['totalobligaciones'];?>" style="width:98%;height:30px;" readonly></td>
					<td class="tamano01" style="width:2.5cm;">Pagos:</td>
					<td style="width:10%;"><input type="text" name="totalpagos" id="totalpagos" value="<?php echo $_POST['totalpagos'];?>" style="width:98%;height:30px;" readonly></td>
					<td class="tamano01" style="width:2.5cm;">CDP:</td>
					<td>
						<select name="selcdp" id="selcdp" onChange="document.form2.submit();" style="width:98%;height:30px;">
							<option value='-1'>SIN FILTRO</option>
						</select>
					</td>
					<td></td>
				</tr>
			</table>
			<input type="hidden" name="oculto1" id="oculto1" value='1'>
			<?php
				echo "
				<div class='subpantallap' style='height:66.5%; width:99.6%;'>
					<table class='inicio'>
						<tr style='text-align:center;'>
							<td class='titulos2'>Cuenta</td>
							<td class='titulos2'>Descripción</td>
							<td class='titulos2'>Vigencia Del Gasto</td>
							<td class='titulos2'>Sección Presupuestal</td>
							<td class='titulos2'>Programatico MGA</td>
							<td class='titulos2'>CPC</td>
							<td class='titulos2'>Fuentes</td>
							<td class='titulos2'>BPIN</td>
							<td class='titulos2'>Situación de Fondos</td>
							<td class='titulos2'>Politica Publica</td>
							<td class='titulos2'>Tercero CHIP</td>
							<td class='titulos2'>Inicial</td>
							<td class='titulos2'>Adiciones</td>
							<td class='titulos2'>Reducciones</td>
							<td class='titulos2'>Credito</td>
							<td class='titulos2'>Contracredito</td>
							<td class='titulos2'>Definitivo</td>
							<td class='titulos2'>Disponibilidad</td>
							<td class='titulos2'>Compromisos</td>
							<td class='titulos2'>Obligaciones</td>
							<td class='titulos2'>Pagos</td>
							<td class='titulos2'>Tipo Tabla</td>
						</tr>";
				$iter = "zebra1";
				$iter2 = "zebra2";
				unset ($auxttcdp);
				$auxdisponibilidad = $auxcompromisos = $auxobligaciones = $auxpagos = 0;
				$nombretablatemporal = $_SESSION['tablatemporal']."AUX";
				$sqlver="
				SELECT id, codigocuenta, nombrecuenta, seccpresupuestal, vigenciagastos, programatico, bpin, cpc, fuente, situacionfondo, politicapublica, tercerochip, compromisos, obligaciones, pagos, tipotabla, fuente2, cpc2, programatico2, disponibilidad, apropiacionini, apropiaciondef, adiciones, reducciones, trascredito, trascontracredito, tten, tteg, tton, ttob, ttrp, ttcdp, tvigencia
				FROM $nombretablatemporal
				WHERE codigocuenta = '".$_POST['vcuenta']."' AND seccpresupuestal = '".$_POST['vseccpresupuestal']."' AND programatico = '".$_POST['vprogramatico']."' AND cpc = '".$_POST['vcpc']."' AND fuente = '".$_POST['vfuente']."' AND bpin = '".$_POST['vbpin']."' AND vigenciagastos = '".$_POST['vvigengasto']."'
				ORDER BY id ASC";
				$resver = mysqli_query($linkbd,$sqlver);
				while ($rowver = mysqli_fetch_row($resver))
				{
					if(($rowver[31] == $_POST['selcdp']) || ($_POST['selcdp'] == '-1'))
					{
						$sqlredglobal = "SELECT tipo FROM redglobal WHERE codigo = '$rowver[3]'";
						$resredglobal = mysqli_query($linkbd,$sqlredglobal);
						$rowredglobal = mysqli_fetch_row($resredglobal);
						$vardobleclick='';
						if($rowredglobal[0] == 'IN')
						{
							if($rowver[26] != '0'){$vardobleclick="onDblClick=\"ir_egreso_nomina('$rowver[26]');\"";}
							elseif($rowver[27] != '0'){$vardobleclick="onDblClick=\"ir_egresos('$rowver[27]');\"";}
							elseif($rowver[28] != '0'){$vardobleclick="onDblClick=\"ir_obligacion_nomina('$rowver[28]');\"";}
							elseif($rowver[29] != '0'){$vardobleclick="onDblClick=\"ir_obligaciones('$rowver[29]');\"";}
							elseif($rowver[30] != '0'){$vardobleclick="onDblClick=\"ir_compromisos('$rowver[30]','$rowver[32]');\"";}
							elseif($rowver[31] != '0'){$vardobleclick="onDblClick=\"ir_disponibilidad('$rowver[31]','$rowver[32]');\"";}
						}
						echo "
							<tr class='$iter' $vardobleclick>
								<td>$rowver[1]</td>
								<td>$rowver[2]</td>
								<td>$rowver[4]</td>
								<td>$rowver[3]</td>
								<td>$rowver[18] ($rowver[5])</td>
								<td>$rowver[17] ($rowver[7])</td>
								<td>$rowver[16] ($rowver[8])</td>
								<td>$rowver[6]</td>
								<td>$rowver[9]</td>
								<td>$rowver[10]</td>
								<td>$rowver[11]</td>
								<td style='text-align:right;'>$".number_format($rowver[20],0,',','.')."</td>
								<td style='text-align:right;'>$".number_format($rowver[22],0,',','.')."</td>
								<td style='text-align:right;'>$".number_format($rowver[23],0,',','.')."</td>
								<td style='text-align:right;'>$".number_format($rowver[24],0,',','.')."</td>
								<td style='text-align:right;'>$".number_format($rowver[25],0,',','.')."</td>
								<td style='text-align:right;'>$".number_format($rowver[21],0,',','.')."</td>
								<td style='text-align:right;'>$".number_format($rowver[19],0,',','.')."</td>
								<td style='text-align:right;'>$".number_format($rowver[12],0,',','.')."</td>
								<td style='text-align:right;'>$".number_format($rowver[13],0,',','.')."</td>
								<td style='text-align:right;'>$".number_format($rowver[14],0,',','.')."</td>
								<td>$rowver[15]</td>
							</tr>";
						$aux=$iter;
						$iter=$iter2;
						$iter2=$aux;
						$auxdisponibilidad += $rowver[19];
						$auxcompromisos += $rowver[12];
						$auxobligaciones += $rowver[13];
						$auxpagos += $rowver[14];
						}
					$ch = esta_en_array($auxttcdp,$rowver[31]);
					if($ch<1){$auxttcdp[] = $rowver[31];}
					
				}
				echo "
					</table>
				</div>
				<script>
				document.form2.totaldisponibilidad.value = '$".number_format($auxdisponibilidad,0,',','.')."';
				document.form2.totalcompromisos.value = '$".number_format($auxcompromisos,0,',','.')."';
				document.form2.totalobligaciones.value = '$".number_format($auxobligaciones,0,',','.')."';
				document.form2.totalpagos.value = '$".number_format($auxpagos,0,',','.')."';
				</script>";
				for($zz = 0; $zz < count($auxttcdp); $zz++)
				{
					if($_POST['selcdp'] == $auxttcdp[$zz]){$selactiva = "option.selected = true;";}
					else {$selactiva = "";}
					echo"
					<script>
						var select = document.getElementById('selcdp');
						var option = document.createElement('option');
						option.value = '$auxttcdp[$zz]';
						option.text = '$auxttcdp[$zz]';
						$selactiva
						select.appendChild(option).appendChild(option);
					</script>";
				}
			?>
		</form>
	</body>
</html>
