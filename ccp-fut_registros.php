<?php
    header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");

	require 'comun.inc';
	require 'funciones.inc';

	session_start();
    date_default_timezone_set("America/Bogota");

?>
<!DOCTYPE >
<html lang="es">
	<head>
		<meta charset="utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1"/>
		<meta name="viewport" content="user-scalable=no">
		<title>:: Ideal - Presupuesto</title>
		<link href="favicon.ico" rel="shortcut icon"/>
        
		<link href="css/css2.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/css2z.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
        <link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
        <link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
        <script type="text/javascript" src="css/programas.js"></script>
        <script type="text/javascript" src="css/calendario.js"></script>
        <script type="text/javascript" src="JQuery/jquery-2.1.4.min.js"></script>

		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">

		<style>
			.inicio--no-shadow{
				box-shadow: none;
			}

			[v-cloak]{
				display : none;
			}
			
			.seleccionarTodos {
				padding: 4px 0px 4px 10px;
				background: linear-gradient(#337CCF, #1450A3);
				border: 1px solid;
				border-top-color: #0A6EBD;
				border-right-color: #0A6EBD;
				border-bottom-color: #337CCF;
				border-left-color: #337CCF;
				box-shadow: 4px 4px 2px #368eb880;
				border-radius: 5px;
				display: flex;
				align-items: center;
				flex-direction: row;
				text-align: center;
				gap: 5px;
				color: #fff;
				cursor: pointer;
				font-size: 0.8rem;
			}

			.span-check{
				margin-right: 0.5em;
				text-align: center;
				width: 1.5rem;
				height: 1.5rem;
				background-color: rgb(12, 176, 252);
				border: 2px solid;
				border-color: rgb(46, 136, 238);
				border-radius: 5px;
				cursor: pointer;
			}

			.span-check-completed {
				margin-right: 0.5em;
				text-align: center;
				width: 1.5rem;
				height: 1.5rem;
				background-color: rgb(12, 176, 252);
				border: 2px solid;
				border-color: rgb(46, 136, 238);
				border-radius: 5px;
				cursor: pointer;
				background-image: url("data:image/svg+xml,%3csvg viewBox='0 0 16 16' fill='white' xmlns='http://www.w3.org/2000/svg'%3e%3cpath d='M12.207 4.793a1 1 0 010 1.414l-5 5a1 1 0 01-1.414 0l-2-2a1 1 0 011.414-1.414L6.5 9.086l4.293-4.293a1 1 0 011.414 0z'/%3e%3c/svg%3e");
				background-size:auto;
			}
		</style>
    </head>
    <body>
        <IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
        <span id="todastablas2"></span>
        <table>
            <tr><script>barra_imagenes("ccpet");</script><?php cuadro_titulos();?></tr>	 
            <tr><?php menu_desplegable("ccpet");?></tr>
        	<tr>
          		<td colspan="3" class="cinta">
					<a><img src="imagenes/add.png" title="Nuevo" onClick="location.href='ccp-fut_registros.php'" class="mgbt"/></a>
					<a class="mgbt"><img src="imagenes/guardad.png"/></a>
					<a><img src="imagenes/buscad.png" title="Buscar"  class="mgbt"/></a>
					<a href="#" onClick="mypop=window.open('ccp-principal.php','','');mypop.focus();" class="mgbt"><img src="imagenes/nv.png" title="Nueva Ventana"></a>
					<img src="imagenes/iratras.png" title="Atr&aacute;s" onClick="location.href='ccp-menucuipo.php'" class="mgbt"/>
				</td>
        	</tr>
		</table>
		<div class="subpantalla" style="height:78%; width:99.6%; overflow:hidden; resize: vertical;">
			<div id="myapp" v-cloak>
				<div class="row">
					<div class="col-12">
						<h4 style="padding-left:50px; padding-top:5px; padding-bottom:5px; background-color: #0FB0D4">Registros presupuestales FUT - APSB:</h4>
					</div>
				</div>
				<div style="height:100%;">
					
					<div class="row" style="margin: 0px 50px 0; border-radius: 2px; background-color: #E1E2E2; ">
						<div class="col-md-10" style="padding: 12px 0px 0px 30px; font: 140% sans-serif;">
							<label for="">Buscar Rubro. <span style="font-size: 12px; font-weight: bold;"><i>(filtre por fuente, nombre fuente, indicador program&aacute;tico para inversi&oacute;n, c&oacute;digo BPIM, nombre program&aacute;tico para inversi&oacute;n o nombre del proyecto.)</i></span></label>
						</div>
						<div class="col-md-2" style="padding: 4px" v-show="puedeGuardar">
							<button class="col btn btn-success" @click = "guardarRubroSGR">Guardar Cambios</button>
						</div>
					</div>
					<div class="row" style="margin:0px 50px 0; border-radius: 2px; background-color: #E1E2E2;">
						<div class="col-md-10" style="padding: 4px">
							<input v-on:keyup="buscarRubro" v-model="buscar_rubro" type="text" class="form-control" style="height: auto; border-radius:4px;" placeholder="Buscar Rubro.">
						</div>
						<div class="col-md-2" style="padding: 4px">
							<select v-model="vigencia" style="width:100%" v-on:Change="cargarParametros" class="form-control" style = "font-size: 10px; margin-top:4px">
								<option v-for="year in years" :value="year[0]">{{ year[0] }}</option>
							</select>
							<!-- <button v-on:click="buscarRubro" type="submit" class="col btn btn-primary" value="Buscar"  style="height: auto; border-radius:5px;">Buscar</button> -->
						</div>	
					</div>

					<div style="margin: 10px 50px 0">
						<table>
							<thead>
								<tr>
									<td class='titulos' width="10%"  style="font: 160% sans-serif; border-radius: 5px 0px 0px 0px;">Num. RP</td>
									<td class='titulos' width = "10%" style="font: 160% sans-serif;">
										Fecha RP
									</td>
									<td class='titulos' width = "30%" style="text-align: center;">
										Objeto 
									</td>
									<td class='titulos' width = "10%" style="text-align: center;">
										Valor
									</td>
									<td class='titulos' width = "20%" style="text-align: center;">
										Rubro
									</td>
									<td class='titulos' style="text-align: center;">
										Nombre rubro
									</td>
                                    <td class='titulos' width = "10%" style="text-align: center;">
										Actividad
									</td>

                                    <td class='titulos' width = "10%" style="text-align: center;">
										Fuente
									</td>

                                    <td class='titulos' width = "10%" style="text-align: center;">
										Localizacion
									</td>

                                    <td class='titulos' width = "10%" style="text-align: center;">
										Mod Contrato
									</td>

                                    <td class='titulos' width = "10%" style="text-align: center;">
										Estado ejecu
									</td>

									<td class='titulos' width = "1%"></td>
								</tr>
							</thead>
						</table>
					</div>
					<div class="estilos-scroll" style="margin: 0px 50px 20px; border-radius: 0 0 0 5px; height: 62%; overflow: scroll; overflow-x: hidden; background: white;">
						<table class='inicio inicio--no-shadow'>
							<tbody v-if="show_resultados">
								
								<tr v-for="(result, index) in results" :style="estaEnArray(result) == true ? myStyle : ''" v-bind:class="index % 2 ? 'contenidonew00' : 'contenidonew01'" >
									<td width="10%" style="font: 160% sans-serif; padding: 5px;">{{ result[0] }}</td>
									<td colspan="2"  width="10%" style="font: 160% sans-serif;">{{ result[1] }}</td>
									<td colspan="2"  width="30%" style="font: 160% sans-serif;">{{ result[2] }}</td>
									<td colspan="2"  width="10%" style="font: 160% sans-serif;">{{ result[3] }}</td>
									<td colspan="2"  width="20%" style="font: 160% sans-serif;">{{ result[4] }}</td>
									<td colspan="2" style="font: 160% sans-serif;">{{ result[5] }}</td>
									<td width="10%" style="text-align: center;">
										<div v-on:dblclick="agregarRubroSGR(result[0], result[1])" title="Doble click para desplegar ventana emergente" style='text-rendering: optimizeLegibility; text-align:center; background-color: #FBE71B; cursor: pointer !important; border-radius: 5px; height: 5vh; display: flex; flex-direction: column; justify-content: center'>
											{{ registro && registro[result[0]] }} 
										</div>
									</td>
									<td width="10%" style="text-align: center;">
										<div v-on:dblclick="agregarFuenteSGR(result[0], result[1])" title="Doble click para desplegar ventana emergente" style='text-rendering: optimizeLegibility; text-align:center; background-color: #FBE71B; cursor: pointer !important; border-radius: 5px; height: 5vh; display: flex; flex-direction: column; justify-content: center'>
											{{ registro && registro[result[0]+'-1'] }} 
										</div>
									</td>
									<td width="10%" style="text-align: center;">
										<div v-on:dblclick="agregarTipoRecursoSGR(result[0], result[1])" title="Doble click para desplegar ventana emergente" style='text-rendering: optimizeLegibility; text-align:center; background-color: #FBE71B; cursor: pointer !important; border-radius: 5px; height: 5vh; display: flex; flex-direction: column; justify-content: center'>
											{{ registro && registro[result[0]+'-2'] }} 
										</div>
									</td>

									<td width="10%" style="text-align: center;">
										<div v-on:dblclick="agregarTerceroSGR(result[0], result[1])" title="Doble click para desplegar ventana emergente" style='text-rendering: optimizeLegibility; text-align:center; background-color: #FBE71B; cursor: pointer !important; border-radius: 5px; height: 5vh; display: flex; flex-direction: column; justify-content: center'>
											{{ registro && registro[result[0]+'-3'] }} 
										</div>
									</td>
								</tr>
							</tbody>
							<tbody v-else>
								<tr>
									<td width="20%"style="font: 120% sans-serif; padding-left:10px; text-align:center;" colspan="3">Sin resultados</td>
								</tr>
							</tbody>
						</table>
					</div>
					
				</div>

				<div v-show="showModal_cuentas">
                    <transition name="modal">
                        <div class="modal-mask">
                            <div class="modal-wrapper">
                                <div class="modal-dialog modal-lg" style = "max-width: 1200px !important;" role="document">
                                    <div class="modal-content"  style = "width: 1200px !important;" scrollable>
                                        <div class="modal-header">
                                            <h5 class="modal-title">{{ rubroVentanaEmergente }}</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true" @click="showModal_cuentas = false">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <div style="margin: 2px 0 0 0;">
                                                <div class="row" style="margin: 4px; border-radius:4px; background-color: #F6F6F6; ">
                                                    <div class="col-md-2" style="padding: 12px 0px 0px 30px; font: 140% sans-serif;">
                                                        <label for="">Cuenta:</label>
                                                    </div>
                                                    
                                                    <div class="col-md-6 col-md-offset-6" style="padding: 4px">
                                                        <input type="text" class="form-control" placeholder="Buscar por nombre o c&oacute;digo de la cuenta" style="font: sans-serif; " v-on:keyup="searchMonitorCuenta" v-model="searchCuenta.keywordCuenta">
                                                    </div>
                                                </div>
                                                <table>
                                                    <thead>
                                                        <tr>
                                                            <td width="20%" class='titulos' style="font: 120% sans-serif; padding-left:10px; border-radius: 5px 0 0 0;">Cuenta</td>
                                                            <td class='titulos' style="font: 120% sans-serif; ">Nombre</td>
                                                            <td width="20%" class='titulos' style="font: 120% sans-serif; ">Tipo</td>

                                                        </tr>
                                                    </thead>
                                                </table>
                                            </div>
                                            <div style="margin: 2px 0 0 0; border-radius: 0 0 0 6px; height: 40vh; overflow: scroll; overflow-x: hidden; background: white; ">
                                                <table class='inicio inicio--no-shadow'>
                                                    <tbody>
                                                        <?php
                                                            $co ='zebra1';
                                                            $co2='zebra2';
                                                        ?>
                                                        <tr v-for="cuenta in cuentasSGR" v-on:click="seleccionarCuenta(cuenta)" class='<?php echo $co; ?>' style='text-rendering: optimizeLegibility; cursor: pointer !important; style=\"cursor: hand\"'>
                                                            <td width="20%" style="font: 120% sans-serif; padding-left:10px">{{ cuenta[1] }}</td>
                                                            <td  style="font: 120% sans-serif; padding-left:10px">{{ cuenta[2] }}</td>
                                                            <td width="20%" style="font: 120% sans-serif; padding-left:10px">{{ cuenta[3] }}</td>
                                                            <?php
                                                            $aux=$co;
                                                            $co=$co2;
                                                            $co2=$aux;
                                                            ?>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary" @click="showModal_cuentas = false">Cerrar</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </transition>
                </div>

				<div v-show="showModal_fuentes">
                    <transition name="modal">
                        <div class="modal-mask">
                            <div class="modal-wrapper">
                                <div class="modal-dialog modal-lg" style = "max-width: 1200px !important;" role="document">
                                    <div class="modal-content"  style = "width: 1200px !important;" scrollable>
                                        <div class="modal-header">
                                            <h5 class="modal-title">{{ rubroVentanaEmergente }}</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true" @click="showModal_fuentes = false">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <div style="margin: 2px 0 0 0;">
                                                <div class="row" style="margin: 4px; border-radius:4px; background-color: #F6F6F6; ">
                                                    <div class="col-md-2" style="padding: 12px 0px 0px 30px; font: 140% sans-serif;">
                                                        <label for="">Fuente:</label>
                                                    </div>
                                                    
                                                    <div class="col-md-6 col-md-offset-6" style="padding: 4px">
                                                        <input type="text" class="form-control" placeholder="Buscar por nombre o c&oacute;digo de la fuente" style="font: sans-serif; " v-on:keyup="searchMonitorFuente" v-model="searchFuente.keywordFuente">
                                                    </div>
                                                </div>
                                                <table>
                                                    <thead>
                                                        <tr>
                                                            <td width="20%" class='titulos' style="font: 120% sans-serif; padding-left:10px; border-radius: 5px 0 0 0;">Fuente</td>
                                                            <td class='titulos' style="font: 120% sans-serif; ">Nombre</td>

                                                        </tr>
                                                    </thead>
                                                </table>
                                            </div>
                                            <div style="margin: 2px 0 0 0; border-radius: 0 0 0 6px; height: 40vh; overflow: scroll; overflow-x: hidden; background: white; ">
                                                <table class='inicio inicio--no-shadow'>
                                                    <tbody>
                                                        <?php
                                                            $co ='zebra1';
                                                            $co2='zebra2';
                                                        ?>
                                                        <tr v-for="fuente in fuentesSGR" v-on:click="seleccionarFuente(fuente)" class='<?php echo $co; ?>' style='text-rendering: optimizeLegibility; cursor: pointer !important; style=\"cursor: hand\"'>
                                                            <td width="20%" style="font: 120% sans-serif; padding-left:10px">{{ fuente[1] }}</td>
                                                            <td  style="font: 120% sans-serif; padding-left:10px">{{ fuente[2] }}</td>
                                                            <?php
                                                            $aux=$co;
                                                            $co=$co2;
                                                            $co2=$aux;
                                                            ?>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary" @click="showModal_fuentes = false">Cerrar</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </transition>
                </div>

				<div v-show="showModal_tiposRecursos">
                    <transition name="modal">
                        <div class="modal-mask">
                            <div class="modal-wrapper">
                                <div class="modal-dialog modal-lg" style = "max-width: 1200px !important;" role="document">
                                    <div class="modal-content"  style = "width: 1200px !important;" scrollable>
                                        <div class="modal-header">
                                            <h5 class="modal-title">{{ rubroVentanaEmergente }}</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true" @click="showModal_tiposRecursos = false">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <div style="margin: 2px 0 0 0;">
                                                <div class="row" style="margin: 4px; border-radius:4px; background-color: #F6F6F6; ">
                                                    <div class="col-md-2" style="padding: 12px 0px 0px 30px; font: 140% sans-serif;">
                                                        <label for="">Tipo de Recurso:</label>
                                                    </div>
                                                    
                                                    <div class="col-md-6 col-md-offset-6" style="padding: 4px">
                                                        <input type="text" class="form-control" placeholder="Buscar por nombre o c&oacute;digo del tipo de recurso" style="font: sans-serif; " v-on:keyup="searchMonitorTipoRecurso" v-model="searchTipoRecurso.keywordTipoRecurso">
                                                    </div>
                                                </div>
                                                <table>
                                                    <thead>
                                                        <tr>
                                                            <td width="20%" class='titulos' style="font: 120% sans-serif; padding-left:10px; border-radius: 5px 0 0 0;">Fuente</td>
                                                            <td class='titulos' style="font: 120% sans-serif; ">Nombre</td>

                                                        </tr>
                                                    </thead>
                                                </table>
                                            </div>
                                            <div style="margin: 2px 0 0 0; border-radius: 0 0 0 6px; height: 40vh; overflow: scroll; overflow-x: hidden; background: white; ">
                                                <table class='inicio inicio--no-shadow'>
                                                    <tbody>
                                                        <?php
                                                            $co ='zebra1';
                                                            $co2='zebra2';
                                                        ?>
                                                        <tr v-for="tipoRecurso in tiposRecursosSGR" v-on:click="seleccionarTipoRecurso(tipoRecurso)" class='<?php echo $co; ?>' style='text-rendering: optimizeLegibility; cursor: pointer !important; style=\"cursor: hand\"'>
                                                            <td width="20%" style="font: 120% sans-serif; padding-left:10px">{{ tipoRecurso[1] }}</td>
                                                            <td  style="font: 120% sans-serif; padding-left:10px">{{ tipoRecurso[2] }}</td>
                                                            <?php
                                                            $aux=$co;
                                                            $co=$co2;
                                                            $co2=$aux;
                                                            ?>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary" @click="showModal_tiposRecursos = false">Cerrar</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </transition>
                </div>

				<div v-show="showModal_terceros">
                    <transition name="modal">
                        <div class="modal-mask">
                            <div class="modal-wrapper">
                                <div class="modal-dialog modal-lg" style = "max-width: 1200px !important;" role="document">
                                    <div class="modal-content"  style = "width: 1200px !important;" scrollable>
                                        <div class="modal-header">
                                            <h5 class="modal-title">{{ rubroVentanaEmergente }}</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true" @click="showModal_terceros = false">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <div style="margin: 2px 0 0 0;">
                                                <div class="row" style="margin: 4px; border-radius:4px; background-color: #F6F6F6; ">
                                                    <div class="col-md-2" style="padding: 12px 0px 0px 30px; font: 140% sans-serif;">
                                                        <label for="">Tipo de Tercero:</label>
                                                    </div>
                                                    
                                                    <div class="col-md-6 col-md-offset-6" style="padding: 4px">
                                                        <input type="text" class="form-control" placeholder="Buscar por nombre o c&oacute;digo del tercero" style="font: sans-serif; " v-on:keyup="searchMonitorTercero" v-model="searchTercero.keywordTercero">
                                                    </div>
                                                </div>
                                                <table>
                                                    <thead>
                                                        <tr>
                                                            <td width="20%" class='titulos' style="font: 120% sans-serif; padding-left:10px; border-radius: 5px 0 0 0;">Tercero</td>
                                                            <td class='titulos' style="font: 120% sans-serif; ">Nombre</td>

                                                        </tr>
                                                    </thead>
                                                </table>
                                            </div>
                                            <div style="margin: 2px 0 0 0; border-radius: 0 0 0 6px; height: 40vh; overflow: scroll; overflow-x: hidden; background: white; ">
                                                <table class='inicio inicio--no-shadow'>
                                                    <tbody>
                                                        <?php
                                                            $co ='zebra1';
                                                            $co2='zebra2';
                                                        ?>
                                                        <tr v-for="tercero in tercerosSGR" v-on:click="seleccionarTercero(tercero)" class='<?php echo $co; ?>' style='text-rendering: optimizeLegibility; cursor: pointer !important; style=\"cursor: hand\"'>
                                                            <td width="20%" style="font: 120% sans-serif; padding-left:10px">{{ tercero[1] }}</td>
                                                            <td  style="font: 120% sans-serif; padding-left:10px">{{ tercero[2] }}</td>
                                                            <?php
                                                            $aux=$co;
                                                            $co=$co2;
                                                            $co2=$aux;
                                                            ?>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary" @click="showModal_terceros = false">Cerrar</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </transition>
                </div>

                <div id="cargando" v-show="loading" class="loading">
                    <span>Cargando...</span>
                </div>

			</div>	
		</div>

        <script type="module" src="./presupuesto_ccpet/reportes/ccp-fut_registros.js"></script>
        <script src="Librerias/vue/axios.min.js"></script>
        <script src="sweetalert2/dist/sweetalert2.min.js"></script>
        <link rel="stylesheet" href="sweetalert2/dist/sweetalert2.min.css">
        <script src="https://unpkg.com/vue@3/dist/vue.global.js"></script>
	</body>
</html>