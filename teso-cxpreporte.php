<?php //V 1000 12/12/16 ?> 
<?php
require "comun.inc";
require "funciones.inc";
session_start();
cargarcodigopag($_GET['codpag'], $_SESSION["nivel"]);

$linkbd = conectar_v7();
$linkbd -> set_charset("utf8");

?>
<html lang="es">
	<head>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
    	<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<title>:: IDEAL 10 - Tesorer&iacute;a</title>


        <script src="css/programas.js"></script>
        <script src="css/calendario.js"></script>
        <link href="css/css2.css" rel="stylesheet" type="text/css" />
        <link href="css/css3.css" rel="stylesheet" type="text/css" />
        <?php titlepag();?>
    </head>
    <body>
        <IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
        <span id="todastablas2"></span>
        <table>
            <tr>
                <script>barra_imagenes("teso");</script>
                <?php cuadro_titulos();?>
            </tr>	 
            <tr>
                <?php menu_desplegable("teso");?>
            </tr>
            <tr>
                <td colspan="3" class="cinta">
                    <a href="teso-cxpreporte.php" class="mgbt"><img src="imagenes/add.png" alt="Nuevo" title="Nuevo"/></a>
                    <a href="#" class="mgbt"><img src="imagenes/guardad.png" alt="Guardar" title="Guardar"/> 
                    <a onClick="document.form2.submit();" href="#" class="mgbt"><img src="imagenes/busca.png" alt="Buscar" title="Buscar" /></a> 
                    <a href="teso-saldoscxpexcel.php?tercero=<?php echo $_POST['tercero']?>&fecha1=<?php echo $_POST['fecha']?>&fecha2=<?php echo $_POST['fecha2']?>&conanulados=<?php echo $_POST['conanul']?>" target="_blank"><img src="imagenes/excel.png"  class="mgbt" alt="csv"></a>
                    <a href="#" onClick="mypop=window.open('teso-principal.php','','');mypop.focus();" class="mgbt"><img src="imagenes/nv.png" alt="nueva ventana" title="Nueva Ventana"></a>
                    <a href="teso-informestesoreria.php" class="mgbt"><img src="imagenes/iratras.png" title="Atr&aacute;s"></a>
                </td>
            </tr>	
        </table>
        <tr>
            <td colspan="3" class="tablaprin"> 
                <form name="form2" method="post" action="teso-cxpreporte.php">
                    <?php
                        if($_POST['conanul']=='S')
                        {
                            $chkant=' checked ';
                        }
                    ?>
                    <table  class="inicio" align="center" >
                        <tr>
                            <td class="titulos" colspan="7">:. Buscar saldos cxp</td>
                            <td width="70" class="cerrar" ><a href="teso-principal.php">Cerrar</a></td>
                        </tr>
                        <tr>
                            <td  class="saludo1">Fecha Inicial:</td>
                            <td>
                                <input type="hidden" value="<?php echo $ $vigusu ?>" name="vigencias"><input id="fc_1198971545" title="DD/MM/YYYY" name="fecha" type="text" value="<?php echo $_POST['fecha']?>" maxlength="10" size="10" onKeyPress="javascript:return solonumeros(event)" onKeyUp="return tabular(event,this)" onKeyDown="mascara(this,'/',patron,true)"><a href="#" onClick="displayCalendarFor('fc_1198971545');"><img src="imagenes/calendario04.png" style="width:20px" align="absmiddle" border="0"></a>
                            </td>
                            <td class="saludo1">Fecha Final: </td>
                            <td>
                                <input id="fc_1198971546" title="DD/MM/YYYY" name="fecha2" type="text" value="<?php echo $_POST['fecha2']?>" maxlength="10" size="10" onKeyPress="javascript:return solonumeros(event)" onKeyUp="return tabular(event,this)" onKeyDown="mascara(this,'/',patron,true)">   
                                <a href="#" onClick="displayCalendarFor('fc_1198971546');"><img src="imagenes/calendario04.png" style="width:20px" align="absmiddle" border="0"></a> 
                            </td>
                            <td>
                                <input type="button" name="generar" value="Generar" onClick="document.form2.submit()">
                                <input name="oculto" type="hidden" value="1"><input name="var1" type="hidden" value=<?php echo $_POST['var1'];?>></td>
                            </td>
                        </tr>                       
                    </table>    
                </form> 
                <div class="subpantallap">
                    <?php
                    $oculto=$_POST['oculto'];
                    if($_POST['oculto'])
                    {
                        $crit1=" ";
                        $crit2=" ";
                        $fechaf=$_POST['fecha'];
                        $fechaf2=$_POST['fecha2'];

                        preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST['fecha'], $fecha);
						$fechaf = $fecha[3]."-".$fecha[2]."-".$fecha[1];

                        $_POST['vigencia'] = $fecha[3];

                        preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST['fecha2'], $fecha2);
						$fechaf2 = $fecha2[3]."-".$fecha2[2]."-".$fecha2[1];

                        
                        
                        $sqlr="SELECT * FROM tesoordenpago WHERE (estado!='R' OR estado != 'N') AND NOT EXISTS (SELECT 1 FROM tesoegresos WHERE tesoegresos.id_orden = tesoordenpago.id_orden and tesoegresos.estado='S' AND tesoegresos.fecha BETWEEN '$fechaf' AND '$fechaf2') AND tipo_mov='201' AND tesoordenpago.fecha BETWEEN '$fechaf' AND '$fechaf2'  ";

                        $resp = mysqli_query($linkbd, $sqlr);
                        $ntr = mysqli_num_rows($resp);
                        $con=1;
                        $namearch="archivos/".$_SESSION['usuario']."-reporteingresos.csv";
                        $Descriptor1 = fopen($namearch,"w+"); 
                        fputs($Descriptor1,"No RP;FECHA;Detalle;VALOR\r\n");
                        echo "<table class='inicio' align='center' >
                            <tr>
                                <td colspan='17' class='titulos'>.: Resultados Busqueda:</td>
                            </tr>
                            <tr>
                                <td colspan='4'>Recibos de Liquidacion Encontrados: $ntr</td>
                            </tr>
                            <tr>
                                <td class='titulos2' style='width:5%'>N&deg; Orden Pago</td>
                                <td class='titulos2' style='width:5%'>Vigencia</td>
                                <td class='titulos2'>Tercero</td>
                                <td class='titulos2'>Descripci&oacute;n</td>
                                <td class='titulos2'  style='width:8%'>Valor Pagar</td>
                            </tr>";	
                            //echo "nr:".$nr;
                            $iter='saludo1';
                            $iter2='saludo2';
                            while ($row =mysqli_fetch_row($resp)) 
                            {
                                $nomter=buscatercero($row[6]);
                                
                                echo "<tr>
                                <td class='$iter'>".$row[0]."</td>
                                <td class='$iter'>".$row[3]."</td>
                                <td class='$iter'>".$nomter."</td>
                                <td class='$iter'>".$row[7]."</td>
                                <td class='$iter'>$ ".number_format($row[10],2)."</td>
                                </tr>";	
                                $con+=1;
                                $aux=$iter;
                                $iter=$iter2;
                                $iter2=$aux;
                                
                            }
                            echo"</table>";
                    }
                    ?>
                </div>
            </td>
        </tr>     
    </table>
</body>
</html>