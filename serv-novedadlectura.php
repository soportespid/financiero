<?php
require"comun.inc";
require"funciones.inc";
session_start();
$linkbd=conectar_bd(); 
//**niveles menu: Administracion (0) - Consultas (1) - Herramientas (2) - Reportes (3)
?>
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta http-equiv="Content-Type" content="text/html" />
    <title>:: Ideal.10 - Servicios P&uacute;blicos </title>
    <script>
    function despliegamodal2(_valor, _table) {
        document.getElementById('bgventanamodal2').style.visibility = _valor;
        if (_table == 'srvmedidores') {
            document.getElementById('ventana2').src = 'medidores-ventana.php?table=' + _table;
        } else if (_table == 'srvclientes') {
            document.getElementById('ventana2').src = 'ventana-clienteservicio.php?table=' + _table;
        }
    }

    function despliegamodalm(_valor, _tip, mensa, pregunta) {
        document.getElementById("bgventanamodalm").style.visibility = _valor;
        if (_valor == "hidden") {
            document.getElementById('ventanam').src = "";
        } else {
            switch (_tip) {
                case "1":
                    document.getElementById('ventanam').src = "ventana-mensaje1.php?titulos=" + mensa;
                    break;
                case "2":
                    document.getElementById('ventanam').src = "ventana-mensaje3.php?titulos=" + mensa;
                    break;
                case "3":
                    document.getElementById('ventanam').src = "ventana-mensaje2.php?titulos=" + mensa;
                    break;
                case "4":
                    document.getElementById('ventanam').src = "ventana-consulta1.php?titulos=" + mensa + "&idresp=" +
                        pregunta;
                    break;
            }
        }
    }

    function respuestaModalBusqueda2(tabla, id, nombre, serial, referencia) {
        switch (tabla) {
            case 'srvclientes':
                document.getElementById('cliente').value = id;
                document.getElementById('ntercero').value = nombre;
                document.form2.submit();
                break;
            case 'srvmedidores':
                document.getElementById('id_medidor').value = id;
                document.getElementById('serial').value = serial;
                document.getElementById('referencia').value = referencia;
                document.form2.submit();
                break;
        }
    }

    function funcionmensaje() {
        var idban = document.getElementById('codban').value;
        document.location.href = "serv-asignacion.php";
    }

    function respuestaconsulta(pregunta) {
        switch (pregunta) {
            case "1":
                document.form2.oculto.value = '2';
                document.form2.submit();
                break;
        }
    }

    function guardar() {
        var v1 = document.getElementById('codban').value;
        var v3 = document.getElementById('frecuencia').value;
        var v4 = document.getElementById('servicio').value;
        var v5 = document.getElementById('cliente').value;

        if (v1 != "" && v3 != "-1" && v4 != "-1" && v5 > 0) {
            despliegamodalm('visible', '4', 'Esta Seguro de Guardar', '1');
        } else {
            despliegamodalm('visible', '2', 'Falta información para crear la Asignación');
        }
    }

    function actualizar() {
        document.form2.submit();
    }

    function guardar()
    {
        if (document.form2.fecha.value!='' && document.form2.id_medidor.value!=''  && document.form2.cliente.value!=''  && document.form2.anomalia.value!='')
        {
            despliegamodalm('visible', '4', '¿Esta Seguro de Guardar?', '1');
        }
        else {
            despliegamodalm('visible', '2', 'Falta información para agregar la novedad');
        }
    }
    </script>
    <link href="css/css2.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
    <link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
    <link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="css/calendario.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
    <script type="text/javascript" src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
    <?php titlepag();?>
</head>

<body>
    <div id="bgventanamodalm" class="bgventanamodalm">
        <div id="ventanamodalm" class="ventanamodalm">
            <IFRAME src="" name="ventanam" marginWidth=0 marginHeight=0 frameBorder=0 id="ventanam" frameSpacing=0 style=" width:700px; height:130px; top:200; overflow:hidden;"></IFRAME>
        </div>
    </div>
    <table>
        <tr>
            <script>
            barra_imagenes("serv");
            </script><?php cuadro_titulos();?>
        </tr>
        <tr><?php menu_desplegable("serv");?></tr>
        <tr>
            <td colspan="3" class="cinta">
                <a href="serv-novedadlectura.php" class="tooltip right mgbt">
                    <img src="imagenes/add.png" />
                    <span class="tiptext">Nuevo</span>
                </a>
                <a onClick="guardar();" class="tooltip bottom mgbt">
                    <img src="imagenes/guarda.png" />
                    <span class="tiptext">Guardar</span>
                </a>
                <a href="serv-novedadlecturabusca.php" class="tooltip bottom mgbt">
                    <img src="imagenes/busca.png" />
                    <span class="tiptext">Buscar</span>
                </a>
                <a onClick="mypop=window.open('serv-principal.php','','');mypop.focus();" class="tooltip bottom mgbt">
                    <img src="imagenes/nv.png">
                    <span class="tiptext">Nueva Ventana</span>
                </a>
                <a onClick="mypop=window.open('<?php echo $url2; ?>','','');mypop.focus();" class="tooltip bottom mgbt">
                    <img src="imagenes/duplicar_pantalla.png">
                    <span class="tiptext">Duplicar Pesta&ntilde;a</span>
                </a>
            </td>
        </tr>
    </table>
    <tr>
        <td colspan="3">
            <form name="form2" method="post">
                <?php
					
					if($_POST[oculto]=="")
					{
                        $_POST['codban']=selconsecutivo('srvnovedad_lectura','id');
					}					
                ?>
                <table class="inicio ancho">
                    <tr>
                        <td class="titulos" colspan="10">Novedades de Lectura</td>
                        <td class="cerrar"><a href="serv-principal.php">Cerrar</a></td>
                    </tr>
                    <tr>
						<td class="tamano01" style="width:3cm;">C&oacute;digo:</td>
						<td style="width:15%;">
							<input type="text" name="codban" id="codban" onKeyUp="return tabular(event,this)" value="<?php echo @ $_POST['codban'];?>" style="width:100%;height:30px;text-align:center;" readonly/>
						</td>
                        <td class="saludo1">Medidor:</td>
                        <td>
                            <input type="text" name='serial' id='serial' value="<?php echo @$_POST['serial']?>"
                                style="width: 100%; height: 30px;" onclick="despliegamodal2('visible', 'srvmedidores');"
                                class="colordobleclik" readonly>
                            <input type="hidden" name='id_medidor' id='id_medidor'
                                value="<?php echo $_POST['id_medidor']?>">
                        </td>
                        <td colspan="2">
                            <input type="text" name="referencia" id="referencia"
                                value="<?php echo @$_POST['referencia']?>" style="width:100%;height:30px;" readonly>
                        </td>
                        <td class="saludo1">Cliente:</td>
                        <td>
                            <input type="text" name='cliente' id='cliente' value="<?php echo @$_POST['cliente']?>"
                                style="width: 100%; height: 30px;" onclick="despliegamodal2('visible', 'srvclientes');"
                                class="colordobleclik" readonly>
                        </td>
                        <td colspan="2">
                            <input type="text" name="ntercero" id="ntercero" value="<?php echo @$_POST['ntercero']?>"
                                style="width:100%;height:30px;" readonly>
                        </td>
                    </tr>
                    <tr>
                        <td class="saludo1">Fecha:</td>
                        <td>
                            <input type="text" name="fecha" value="<?php echo @ $_POST['fecha']?>" maxlength="10"
                                onKeyPress="javascript:return solonumeros(event)" onKeyUp="return tabular(event,this)"
                                id="fc_1198971545" onKeyDown="mascara(this,'/',patron,true)" title="DD/MM/YYYY"
                                style="width:80%;height:30px;" readonly>&nbsp;
                            <img src="imagenes/calendario04.png" onClick="displayCalendarFor('fc_1198971545');"
                                title="Calendario" class="icobut" />
                        </td>
                        <td class="saludo1" style="width:7%">Anomalia:</td>
                        <td>
                            <select name="anomalia" id="anomalia" onKeyUp="return tabular(event,this)" style="width:100%;text-transform: uppercase;">
                                <option value="" class="elementosmensaje">Seleccione....</option>
                                <?php
									$sqlr="SELECT * FROM srvanomalias ";
									$res=mysql_query($sqlr,$linkbd);
									while ($row = mysql_fetch_row($res)) 
									{
                                        if(@ $_POST['anomalia']==$row[0])
                                        {
                                            echo "<option value='$row[0]' SELECTED>$row[0] - $row[1]</option>";
                                        }
                                        else{
                                            echo "<option value='$row[0]'>$row[0] - $row[1]</option>";
                                        } 
									}
              					?>
                            </select>
                        </td>
                        <td class="saludo1" style="width:7%">Descripci&oacute;n:</td>
                        <td>
							<input type="text" name="descrip" value="<?php echo $_POST[descrip]?>" style="width:75%">
                        </td>
                        <input name="codigo" type="hidden" value="<?php echo $_POST[codigo]?>">
                        <input type="hidden" name="oculto" value="1">
                    </tr>
                </table>
                <?php  
					//********guardar
					if($_POST[oculto]=="2")
					{
                        ereg( "([0-9]{1,2})/([0-9]{1,2})/([0-9]{2,4})", $_POST[fecha],$fecha);
					    $fechaf=$fecha[3]."-".$fecha[2]."-".$fecha[1];	

						$sqlr="INSERT INTO srvnovedad_lectura (id_cliente,id_medidor,id_novedad,fecha,descripcion,estado) VALUES ('$_POST[cliente]', '$_POST[id_medidor]', '$_POST[anomalia]', '$fechaf', '$_POST[descrip]', 'S')";	
						//echo $sqlr;
                        if (!mysqli_query($linkbd, $sqlr))
                        {
                            echo "
                                <script> 
                                    despliegamodalm('visible', '2', 'No se pudo ejecutar la peticion');
                                </script>";
                        }
                        else
                        {
                            echo "
                                <script>
                                    despliegamodalm('visible', '1', 'Se ha almacenado con Exito');
                                </script>";
                        }
					}
				?>
            </form>
            <div id="bgventanamodal2">
                <div id="ventanamodal2">
                    <IFRAME src="" name="buster" marginWidth=0 marginHeight=0 frameBorder=0 id="ventana2" frameSpacing=0
                        style="left:500px; width:900px; height:500px; top:200;"></IFRAME>
                </div>
            </div>
	</body>
</html>