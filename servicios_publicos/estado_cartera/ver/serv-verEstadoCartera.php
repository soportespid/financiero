<?php
    require_once '../../../comun.inc';
    require '../../../funciones.inc';
    require '../../../funcionesSP.inc.php';

    session_start();
    
    if($_POST){
        
        $obj = new EstadoCartera();
        if($_POST['action'] == 'show'){
            $obj->getMovimientos($_POST['codigo']);
        }
    }

    class EstadoCartera{
        private $linkbd;
        private $strCodigoUsuario;
        private $intIdTercero;
        private $intIdBarrio;
        private $intIdEstrato;
        private $intIdCliente;
        public function __construct() {
            $this->linkbd = conectar_v7();
            $this->linkbd->set_charset("utf8");
        }

        public function getMovimientos(string $codigo){ 
            if($codigo!=""){
                $this->strCodigoUsuario = $codigo;
                $sql="SELECT id,id_tercero,id_barrio,id_estrato,estado FROM srvclientes WHERE cod_usuario=$this->strCodigoUsuario";
                $requestUser = mysqli_query($this->linkbd,$sql)->fetch_assoc();
                if(!empty($requestUser)){
                    if($requestUser['estado']=="S"){
                        $this->intIdTercero = $requestUser['id_tercero'];
                        $this->intIdBarrio = $requestUser['id_barrio'];
                        $this->intIdEstrato = $requestUser['id_estrato'];
                        $this->intIdCliente = $requestUser['id'];

                        //DATOS USUARIO
                        $request['usuario']['nombre'] = mysqli_query($this->linkbd,"SELECT CASE WHEN razonsocial IS NULL OR razonsocial = '' 
                        THEN CONCAT(nombre1,' ',nombre2,' ',apellido1,' ',apellido2) 
                        ELSE razonsocial END AS nombre FROM terceros WHERE id_tercero = $this->intIdTercero")->fetch_assoc()['nombre'];
                        
                        $request['usuario']['barrio'] = mysqli_query($this->linkbd,
                        "SELECT nombre FROM srvbarrios WHERE id = $this->intIdBarrio")->fetch_assoc()['nombre'];

                        $request['usuario']['estrato']= mysqli_query($this->linkbd,
                        "SELECT descripcion FROM srvestratos WHERE id = $this->intIdEstrato")->fetch_assoc()['descripcion'];

                        $request['usuario']['direccion']= mysqli_query($this->linkbd,
                        "SELECT direccion FROM srvdireccion_cliente  WHERE id = $this->intIdCliente")->fetch_assoc()['direccion'];
                        //DATOS FACTURAS
                        $arrFacturas = mysqli_fetch_all(mysqli_query($this->linkbd,
                        "SELECT id_corte,numero_facturacion FROM srvcortes_detalle WHERE id_cliente = $this->intIdCliente"),MYSQLI_ASSOC);
                        $rows = count($arrFacturas);
                        for ($i=0; $i < $rows; $i++) { 
                            $idCorte = $arrFacturas[$i]['id_corte'];
                            $arrFacturas[$i]['fecha'] = mysqli_query($this->linkbd,
                            "SELECT fecha_impresion as fecha FROM srvcortes WHERE numero_corte = $idCorte")->fetch_assoc()['fecha'];
                            if($idCorte == 1){
                                $valorFactura =  consultaValorFactura($arrFacturas[$i]['numero_facturacion']);
                            }else{
                                $valorFactura = consultaValorFacturaSinDeuda($arrFacturas[$i]['numero_facturacion']);
                            }
                            $arrFacturas[$i]['intereses'] = consultaValorIntereses($arrFacturas[$i]['numero_facturacion']);
                            $arrFacturas[$i]['valor_factura'] = $valorFactura;
                        }
                        $request['movimientos']['facturas'] = $arrFacturas;
                        $request['movimientos']['pagos'] = mysqli_fetch_all(mysqli_query($this->linkbd,
                        "SELECT codigo_recaudo, valor_pago, fecha_recaudo as fecha 
                        FROM srv_recaudo_factura WHERE estado != 'REVERSADO' AND id_cliente = $this->intIdCliente"),MYSQLI_ASSOC);

                        $request['movimientos']['acuerdos_pago'] = mysqli_fetch_all(mysqli_query($this->linkbd,
                        "SELECT codigo_acuerdo, valor_abonado, fecha_acuerdo as fecha 
                        FROM srv_acuerdo_cab WHERE estado_acuerdo != 'Reversado' AND id_cliente = $this->intIdCliente"),MYSQLI_ASSOC);
                        
                        $request['movimientos'] = $this->calcMovimientos($request['movimientos']);
                        $arrResponse = array("status"=>true,"data"=>$request);
                    }else{
                        $arrResponse = array("status"=>false,"msg"=>"El usuario está inactivo, inténtelo de nuevo.");
                    }
                }else{  
                    $arrResponse = array("status"=>false,"msg"=>"El usuario no existe, inténtelo de nuevo.");
                }
            }else{
                $arrResponse = array("status"=>false,"msg"=>"Error de datos");
            }
            echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            die();
        }
        public function calcMovimientos(array $data){
            $facturas = $data['facturas'];
            $pagos = $data['pagos'];
            $acuerdosPago = $data['acuerdos_pago'];
            $arrMovimientos = array_merge($facturas,$pagos,$acuerdosPago);
            $saldo=0;
            usort($arrMovimientos,function($a,$b){
                $fechaA = new DateTime($a['fecha']);
                $fechaB = new DateTime($b['fecha']);
                if($fechaA == $fechaB){
                    return 0;
                }
                return ($fechaA < $fechaB) ? -1 : 1;
            });
            $arrLength = count($arrMovimientos);
            for ($i=0; $i < $arrLength; $i++) { 
                $arrDate = explode("-",$arrMovimientos[$i]['fecha']);
                $arrMovimientos[$i]['fecha'] = $arrDate[2]."/".$arrDate[1]."/".$arrDate[0];
                $arrMovimientos[$i]['cobro'] = 0;
                $arrMovimientos[$i]['pago'] = 0;
                
                if(isset($arrMovimientos[$i]['numero_facturacion'])){
                    $arrMovimientos[$i]['tipo'] = "FACTURACION";
                    $arrMovimientos[$i]['consecutivo'] = $arrMovimientos[$i]['numero_facturacion'];
                    $valorAnterior = $arrMovimientos[$i-1]['valor_factura'];
                    $valorActual = $arrMovimientos[$i]['valor_factura'];
                    $interes = $arrMovimientos[$i]['intereses'];
                    $interesReal = abs($arrMovimientos[$i-1]['intereses']-$interes);
                    $valorActualTotal = $valorActual+$interesReal;
                    $arrMovimientos[$i]['valor_factura']=$valorActualTotal+$valorAnterior;
                    $arrMovimientos[$i]['cobro'] =$valorActualTotal;
                    $arrMovimientos[$i]['saldo'] =$arrMovimientos[$i]['valor_factura'];
                }elseif(isset($arrMovimientos[$i]['codigo_recaudo'])){
                    $arrMovimientos[$i]['tipo'] = "PAGO DE FACTURA";
                    $arrMovimientos[$i]['consecutivo'] = $arrMovimientos[$i]['codigo_recaudo'];
                    $arrMovimientos[$i]['pago'] =$arrMovimientos[$i]['valor_pago'];
                }elseif(isset($arrMovimientos[$i]['codigo_acuerdo'])){
                    $arrMovimientos[$i]['tipo'] = "ACUERDO DE PAGO";
                    $arrMovimientos[$i]['consecutivo'] = $arrMovimientos[$i]['codigo_acuerdo'];
                    $arrMovimientos[$i]['pago'] =$arrMovimientos[$i]['valor_abonado'];
                }
                $saldo = $arrMovimientos[$i]['saldo'];
                $pago = $arrMovimientos[$i]['pago'];
                if($arrMovimientos['tipo'] == "pago"){  
                    $saldo-=$pago;
                }
                $arrMovimientos[$i]['pago'] = ceil($arrMovimientos[$i]['pago']/100)*100;
                $arrMovimientos[$i]['cobro'] = ceil($arrMovimientos[$i]['cobro']/100)*100;
                $arrMovimientos[$i]['saldo'] = ceil($saldo/100)*100;
            }
            return $arrMovimientos;
        }
    }
?>