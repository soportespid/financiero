const URL = 'servicios_publicos/estado_cartera/ver/serv-verEstadoCartera.php';

var app = new Vue({
    el:'#myapp',
    data() {
        return {
            strCodUsuario:"",
            strNombre:"",
            strBarrio:"",
            strEstrato:"",
            strDireccion:"",
            isLoading:false,
            arrMovimientos:[]
        }
    },
    mounted() {
        
    },
    methods: {
        search: async function(){
            if(this.strCodUsuario !=""){
                this.isLoading = true;
                const formData = new FormData();
                formData.append("action","show");
                formData.append("codigo",this.strCodUsuario);

                const response = await fetch(URL,{method:"POST",body:formData});
                const objData = await response.json();
                console.log(objData);
                if(objData.status){
                    const data = objData.data;
                    this.arrMovimientos = data.movimientos;
                    this.strNombre = data.usuario.nombre;
                    this.strBarrio = data.usuario.barrio;
                    this.strEstrato = data.usuario.estrato;
                    this.strDireccion = data.usuario.direccion;
                }else{
                    Swal.fire("Error",objData.msg,"error");
                }
                this.isLoading = false;
            }else{
                Swal.fire("Atención","Debe ingresar un código de usuario, inténtelo de nuevo.","warning");
            }
        },
        printPDF:function(){
            if(this.strCodUsuario !=""){
                window.open('serv-verEstadoCarteraPdf.php?codigo='+this.strCodUsuario,'_blank');
            }else{
                Swal.fire("Atención","Debe realizar una búsqueda antes de imprimir en PDF, inténtelo de nuevo.","warning");
            }
        },
        printExcel: function(){
            if(this.strCodUsuario !=""){
                window.open('serv-verEstadoCarteraExcel.php?codigo='+this.strCodUsuario,'_blank');
            }else{
                Swal.fire("Atención","Debe realizar una búsqueda antes de exportar a Excel, inténtelo de nuevo.","warning");
            }
        },
        formatNumero: function(valor){
            return new Intl.NumberFormat("en-US", {style: "currency", currency: "USD"}).format(valor);
        },
    }
})