var app = new Vue({
    el: '#myapp',
	data:{

        error: '',
        loading: false,

        tiposConsumo: [],
        rangoInicial: [],
        rangoFinal: [],
    },

    mounted: function(){

        this.traerDatos();
    },

    methods: 
    {
        toFormData: function(obj){
            var form_data = new FormData();
            for(var key in obj){
                form_data.append(key, obj[key]);
            }
            return form_data;
        },

        traerDatos: async function() {

            await axios.post('servicios_publicos/rangoConsumo/serv-rangoConsumo.php?action=rangos')
            .then((response) => {
                app.tiposConsumo = response.data.tiposConsumo;
            }).catch((error) => {
                this.error = true;
                console.log(error)
            }).finally(() => {
                this.llenadoValores();
            });    
        },

        llenadoValores: function() {

            for (let i = 0; i < app.tiposConsumo.length; i++) {
                
                this.rangoInicial[i] = app.tiposConsumo[i][2];
                this.rangoFinal[i] = app.tiposConsumo[i][3];
            }

            this.rangoInicial = Object.values(this.rangoInicial);
            this.rangoFinal = Object.values(this.rangoFinal);
        },

        actualizarRangos: function() {
            Swal.fire({
                icon: 'question',
                title: 'Seguro que quieres actualizar rangos de consumo?',
                showDenyButton: true,
                confirmButtonText: 'Guardar',
                denyButtonText: 'Cancelar',
                }).then((result) => {
                if (result.isConfirmed) 
                {
                    if (result.isConfirmed) {
                        this.loading = true;
                        var formData = new FormData();

                        for (let i = 0; i < this.rangoInicial.length; i++) {
                            
                            formData.append('rangoInicial[]', this.rangoInicial[i]);   
                            formData.append('rangoFinal[]', this.rangoFinal[i]);    
                        }
                        
                        axios.post('servicios_publicos/rangoConsumo/serv-rangoConsumo.php?action=guardar', formData)
                        .then((response) => {
                            
                            if(response.data.insertaBien){
                                this.loading = false;
                                Swal.fire({
                                    icon: 'success',
                                    title: 'Se ha guardado con exito',
                                    showConfirmButton: false,
                                    timer: 1500
                                    }).then((response) => {
                                        app.redireccionar();
                                    });
                            }
                            else {
                                Swal.fire(
                                    'Error!',
                                    'No se pudo guardar.',
                                    'error'
                                );

                                this.loading = false;
                            }
                        });
                        
                    }
                } 
                else if (result.isDenied) 
                {
                    Swal.fire('Guardar cancelado', '', 'info');
                }
            })
        },

        redireccionar: function(){
            location.reload();  
        }
    }
});