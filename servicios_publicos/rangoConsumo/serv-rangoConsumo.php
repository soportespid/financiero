<?php
    require_once '../../comun.inc';
    require '../../funciones.inc';
    require '../../funcionesSP.inc.php';
    session_start();

    $linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");

    $out = array('error' => false);

    $action = "show";

    if(isset($_GET['action'])){
        $action = $_GET['action'];
    }
    
    if ($action == "rangos") {
        
        $tiposConsumo = array();

        $sqlTipoConsumos = "SELECT id, consumo, rango_inicial, rango_final FROM srv_rango_consumo ORDER BY id ASC";
        $resTipoConsumos = mysqli_query($linkbd, $sqlTipoConsumos);
        while ($rowTipoConsumos = mysqli_fetch_row($resTipoConsumos)) {
            
            array_push($tiposConsumo, $rowTipoConsumos);
        }

        $out['tiposConsumo'] = $tiposConsumo;
    }

    if ($action == "guardar") {
        
        $id = 1;

        for ($i=0; $i < count($_POST['rangoInicial']) ; $i++) { 
            
            $rangoInicial = $_POST['rangoInicial'][$i];
            $rangoFinal = $_POST['rangoFinal'][$i];

            $sql = "UPDATE srv_rango_consumo SET rango_inicial = '$rangoInicial', rango_final = '$rangoFinal' WHERE id = $id";
            mysqli_query($linkbd, $sql);
            
            $id++;
        }

        $out['insertaBien'] = true;
    }

    header("Content-type: application/json");
    echo json_encode($out);
    die();