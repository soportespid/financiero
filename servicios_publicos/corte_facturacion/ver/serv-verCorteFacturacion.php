<?php
    require_once '../../../comun.inc';
    require '../../../funciones.inc';
    require '../../../funcionesSP.inc.php';

    session_start();

    $obj = new CorteFacturacion();
    $obj->show();
    class CorteFacturacion{
        private $linkbd;

        public function __construct() {
            $this->linkbd = conectar_v7();
            $this->linkbd->set_charset("utf8");
        }

        public function show(){
            $sql = "SELECT 
            numero_corte as periodo,
            DATE_FORMAT(fecha_inicial,'%d/%m/%Y')  as inicial,
            DATE_FORMAT(fecha_final,'%d/%m/%Y')  as final,
            DATE_FORMAT(fecha_impresion,'%d/%m/%Y')  as impresion,
            DATE_FORMAT(fecha_limite_pago,'%d/%m/%Y')  as limite,
            DATE_FORMAT(fecha_corte,'%d/%m/%Y')  as suspension,
            estado,
            version_tarifa as tarifa
            FROM srvcortes ORDER BY numero_corte DESC";

            $request = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
            if(!empty($request)){
                $arrResponse = array("status"=>true,"data"=>$request);
            }else{
                $arrResponse = array("status"=>false,"msg"=>"No hay datos");
            }
            echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            die();
        }
    }
?>