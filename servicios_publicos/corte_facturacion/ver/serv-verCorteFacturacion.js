const URL = 'servicios_publicos/corte_facturacion/ver/serv-verCorteFacturacion.php';

var app = new Vue({
    el:"#myapp",
    data() {
        return {
            arrData:[]
        }
    },
    mounted() {
        this.getData();
    },
    methods: {
        getData: async function(){
            const response = await fetch(URL);
            const objData = await response.json();

            if(objData.status){
                this.arrData = objData.data;
            }
        },
        printPDF: function(){
            window.open("serv-verCortesFacturacionPdf.php","_blank");
        }
    },
});