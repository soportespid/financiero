<?php

	require_once '../../../comun.inc';
    require '../../../funciones.inc';
    require '../../../funcionesSP.inc.php';
    header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
    session_start();

	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");

    $out = array('error' => false);

    $action = "show";

    if(isset($_GET['action'])){
        $action = $_GET['action'];
    }

    if ($action == "datosIni") {

        $consecutivo = selconsecutivo("srv_prescripciones", "consecutivo");
        $datosClientes = [];

        $sqlClientes = "SELECT id, cod_usuario, cod_catastral, id_tercero, id_estrato, id_barrio FROM srvclientes WHERE estado = 'S' ORDER BY id ASC";
        $resClientes = mysqli_query($linkbd, $sqlClientes);
        while ($rowClientes = mysqli_fetch_assoc($resClientes)) {
            $documento = $nombre = $barrio = $direccion = $estrato = "";
            $valorFactura = $numFactura = 0;
            $datosTemp = [];

            $sqlTercero = "SELECT nombre1, nombre2, apellido1, apellido2, razonsocial, cedulanit FROM terceros WHERE id_tercero = $rowClientes[id_tercero]";
            $resTercero = mysqli_query($linkbd,$sqlTercero);
            $rowTercero = mysqli_fetch_assoc($resTercero);

            if($rowTercero['razonsocial'] == '')
            {
                $nombre = $rowTercero['nombre1']. ' ' .$rowTercero['nombre2']. ' ' .$rowTercero['apellido1']. ' ' .$rowTercero['apellido2'];
            }
            else
            {
                $nombre = $rowTercero['razonsocial'];
            }

            $nombre = Quitar_Espacios($nombre);
            $documento = $rowTercero['cedulanit'];

            $datosTemp[] = $rowClientes['cod_usuario'];
            $datosTemp[] = $documento;
            $datosTemp[] = $nombre;
            $datosTemp[] = $rowClientes['cod_catastral'];
            array_push($datosClientes, $datosTemp);
        }

        $out['datosUsu'] = $datosClientes;
        $out['conse'] = $consecutivo;
    }

    if ($action == "searchUsu") {
        
        $codUsuario = $_GET['cod'];

        $sqlCliente = "SELECT id, id_barrio, id_estrato FROM srvclientes WHERE cod_usuario = '$codUsuario'";
        $rowCliente = mysqli_fetch_assoc(mysqli_query($linkbd, $sqlCliente));

        $barrio = buscaNombreBarrio($rowCliente['id_barrio']);
        $direccion = encuentraDireccionConIdCliente($rowCliente['id']);
        $estrato = buscarNombreEstrato($rowCliente['id_estrato']);

        $sqlCorteDet = "SELECT MAX(numero_facturacion) AS numFactura, estado_pago FROM srvcortes_detalle WHERE id_cliente = $rowCliente[id]";
        $rowCorteDet = mysqli_fetch_assoc(mysqli_query($linkbd, $sqlCorteDet));

        $numFac = $rowCorteDet['numFactura'];
        $valorFac = consultaValorFactura($numFac);
        $totalFactura = ceil($valorFac / 100) * 100;

        $sql = "SELECT MAX(version) FROM srvcargo_fijo";
        $row = mysqli_fetch_row(mysqli_query($linkbd, $sql));
        
        $versionTarifas = $row[0];

        $valorPrescripcion = 0;

        $sqlServicios = "SELECT id_servicio, id_estrato, cargo_fijo, consumo, tarifa_medidor FROM srvasignacion_servicio WHERE id_clientes = $rowCliente[id] AND cargo_fijo = 'S' ORDER BY id_servicio ASC";
        $resServicios = mysqli_query($linkbd, $sqlServicios);
        while ($rowServicios = mysqli_fetch_assoc($resServicios)) {
            
            $valorServicio = 0;

            //Cargo fijo
            if ($rowServicios['cargo_fijo'] == "S") {

                $sqlCargoFijo = "SELECT costo_unidad, subsidio, contribucion FROM srvcargo_fijo WHERE id_servicio = $rowServicios[id_servicio] AND id_estrato = $rowServicios[id_estrato]";
                $rowCargoFijo = mysqli_fetch_assoc(mysqli_query($linkbd, $sqlCargoFijo));

                if ($rowCargoFijo['subsidio'] > 0) {

                    $subsidio = $rowCargoFijo['subsidio'] / 100;
                    $valSub = $rowCargoFijo['costo_unidad'] * $subsidio;
                    $valorServicio += $rowCargoFijo['costo_unidad'] - $valSub;
                }
                else if ($rowCargoFijo['contribucion'] > 0) {
                    $contribucion = $rowCargoFijo['contribucion'] / 100;
                    $valCont = $rowCargoFijo['costo_unidad'] * $contribucion;
                    $valorServicio += $rowCargoFijo['costo_unidad'] + $valCont;
                }
                else {
                    $valorServicio += $rowCargoFijo['costo_unidad'];
                }
            }

            //Consumo
            if ($rowServicios['consumo'] == "S") {

                if ($rowServicios['tarifa_medidor'] == "S") {

                }
                else if ($rowServicios['tarifa_medidor'] == "N") {
                    $sqlCostosEstandar = "SELECT costo_unidad, subsidio, contribucion FROM srvcostos_estandar WHERE id_servicio = $rowServicios[id_servicio] AND id_estrato = $rowServicios[id_estrato]";
                    $rowCostosEstandar = mysqli_fetch_assoc(mysqli_query($linkbd, $sqlCostosEstandar));

                    if ($rowCostosEstandar['subsidio'] > 0) {

                        $subsidio = $rowCostosEstandar['subsidio'] / 100;
                        $valSub = $rowCostosEstandar['costo_unidad'] * $subsidio;
                        $valorServicio += $rowCostosEstandar['costo_unidad'] - $valSub;
                    }
                    else if ($rowCostosEstandar['contribucion'] > 0) {
                        $contribucion = $rowCostosEstandar['contribucion'] / 100;
                        $valCont = $rowCostosEstandar['costo_unidad'] * $contribucion;
                        $valorServicio += $rowCostosEstandar['costo_unidad'] + $valCont;
                    }
                    else {
                        $valorServicio += $rowCargoFijo['costo_unidad'];
                    }
                }
            }

            $valorPrescripcion += $valorServicio;
        }

        $valorPrescripcion = $valorPrescripcion * 60;
        $valorPrescripcion = ceil($valorPrescripcion / 100) * 100;

        if ($totalFactura > $valorPrescripcion) {
            $descuento = $totalFactura - $valorPrescripcion;
            $totalPagar = $totalFactura - $descuento;
        }
        else {
            $descuento = 0;
            $totalPagar = $totalFactura;
        }

        $out['barrio'] = $barrio;
        $out['direccion'] = $direccion;
        $out['estrato'] = $estrato;
        $out['numFac'] = $numFac;
        $out['totalFactura'] = $totalFactura;
        $out['descuento'] = $descuento;
        $out['totalPagar'] = $totalPagar;
    }

    if ($action == "guardar") {

        $consecutivo = $fecha = $resolucion = $codUsu = $numFactura = $deuda = $descuento = $totalPagar = $clienteId = "";

        $consecutivo = $_POST['consec'];
        preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST['fecha'],$f);
        $fecha = "$f[3]-$f[2]-$f[1]";
        $resolucion = $_POST['resolucion'];
        $codUsu = $_POST['codUsu'];
        $numFactura = $_POST['numFactura'];
        $deuda = $_POST['deuda'];
        $descuento = $_POST['descuento'];
        $totalPagar = $_POST['totalPagar'];
        $newFactura = selconsecutivo("srvcortes_detalle", "numero_facturacion");

        $sqlCorte = "SELECT numero_corte FROM srvcortes WHERE estado = 'S'";
        $rowCorte = mysqli_fetch_assoc(mysqli_query($linkbd, $sqlCorte));

        $corte = $rowCorte['numero_corte'];

        $sqlCliente = "SELECT id, id_tercero FROM srvclientes WHERE cod_usuario = '$codUsu'";
        $rowCliente = mysqli_fetch_assoc(mysqli_query($linkbd, $sqlCliente));

        $clienteId = $rowCliente['id'];
        $terceroId = $rowCliente['id_tercero'];
        $nombre = buscaNombreTerceroConId($terceroId);
        $documento = buscaDocumentoTerceroConId($terceroId);

        $insertPrescripcion = "INSERT INTO srv_prescripciones (consecutivo, fecha, resolucion, cliente_id, cod_usuario, num_factura, valor_deuda, descuento, valor_pagar, estado) VALUES ($consecutivo, '$fecha', '$resolucion', $clienteId, '$codUsu', $numFactura, $deuda, $descuento, $totalPagar, 'S')";
        mysqli_query($linkbd, $insertPrescripcion);

        $updateCorteDet = "UPDATE srvcortes_detalle SET estado_pago = 'PC' WHERE numero_facturacion = $numFactura";
        mysqli_query($linkbd, $updateCorteDet);
    
        $insertCorteDet = "INSERT INTO srvcortes_detalle (id_cliente, id_corte, numero_facturacion, estado_pago) VALUES ($clienteId, $corte, $newFactura, 'S')";
        mysqli_query($linkbd, $insertCorteDet);
        
        $sqlServ = "SELECT id_servicio, id_estrato, cargo_fijo, consumo, tarifa_medidor FROM srvasignacion_servicio WHERE id_clientes = $clienteId AND cargo_fijo = 'S' ORDER BY id_servicio ASC";
        $resServ = mysqli_query($linkbd, $sqlServ);
        while ($rowServ = mysqli_fetch_assoc($resServ)) {

            $valorCf = $valorCs = 0;

            $insertFacturas = "INSERT INTO srvfacturas (corte, cliente, cod_usuario, id_ruta, num_factura, nombre_suscriptor, documento, id_servicio, estado) VALUES ($corte, $clienteId, '$codUsu', 0, $newFactura, '$nombre', '$documento', $rowServ[id_servicio], 'ACTIVO')";
            mysqli_query($linkbd, $insertFacturas);


            if ($rowServ['cargo_fijo'] == "S") {

                $sqlCargoFijo = "SELECT costo_unidad, subsidio, contribucion FROM srvcargo_fijo WHERE id_servicio = $rowServ[id_servicio] AND id_estrato = $rowServ[id_estrato]";
                $rowCargoFijo = mysqli_fetch_assoc(mysqli_query($linkbd, $sqlCargoFijo));

                if ($rowCargoFijo['subsidio'] > 0) {

                    $subsidio = $rowCargoFijo['subsidio'] / 100;
                    $valSub = $rowCargoFijo['costo_unidad'] * $subsidio;
                    $valorCf = $rowCargoFijo['costo_unidad'] - $valSub;
                }
                else if ($rowCargoFijo['contribucion'] > 0) {
                    $contribucion = $rowCargoFijo['contribucion'] / 100;
                    $valCont = $rowCargoFijo['costo_unidad'] * $contribucion;
                    $valorCf = $rowCargoFijo['costo_unidad'] + $valCont;
                }
                else {
                    $valorCf = $rowCargoFijo['costo_unidad'];
                }

                $valorCf = $valorCf * 60;

                $updateCf = "UPDATE srvfacturas SET cargo_f = $valorCf WHERE num_factura = $newFactura AND id_servicio = $rowServ[id_servicio]";
                mysqli_query($linkbd, $updateCf);
                
                $insertDet = "INSERT INTO srvdetalles_facturacion (corte, id_cliente, numero_facturacion, tipo_movimiento, id_servicio, id_tipo_cobro, credito, debito, estado) VALUES ($corte, $clienteId, $newFactura, '101', $rowServ[id_servicio], 1, $valorCf, 0, 'S')";
                mysqli_query($linkbd, $insertDet);
            }

            //Consumo
            if ($rowServ['consumo'] == "S") {

                if ($rowServ['tarifa_medidor'] == "S") {

                }
                else if ($rowServ['tarifa_medidor'] == "N") {
                    $sqlCostosEstandar = "SELECT costo_unidad, subsidio, contribucion FROM srvcostos_estandar WHERE id_servicio = $rowServ[id_servicio] AND id_estrato = $rowServ[id_estrato]";
                    $rowCostosEstandar = mysqli_fetch_assoc(mysqli_query($linkbd, $sqlCostosEstandar));

                    if ($rowCostosEstandar['subsidio'] > 0) {

                        $subsidio = $rowCostosEstandar['subsidio'] / 100;
                        $valSub = $rowCostosEstandar['costo_unidad'] * $subsidio;
                        $valorCs = $rowCostosEstandar['costo_unidad'] - $valSub;
                    }
                    else if ($rowCostosEstandar['contribucion'] > 0) {
                        $contribucion = $rowCostosEstandar['contribucion'] / 100;
                        $valCont = $rowCostosEstandar['costo_unidad'] * $contribucion;
                        $valorCs = $rowCostosEstandar['costo_unidad'] + $valCont;
                    }
                    else {
                        $valorCs = $rowCargoFijo['costo_unidad'];
                    }

                    $updateCf = "UPDATE srvfacturas SET consumo_b = $valorCf WHERE num_factura = $newFactura AND id_servicio = $rowServ[id_servicio]";
                    mysqli_query($linkbd, $updateCf);
                    
                    $insertDet = "INSERT INTO srvdetalles_facturacion (corte, id_cliente, numero_facturacion, tipo_movimiento, id_servicio, id_tipo_cobro, credito, debito, estado) VALUES ($corte, $clienteId, $newFactura, '101', $rowServ[id_servicio], 2, $valorCf, 0, 'S')";
                    mysqli_query($linkbd, $insertDet);
                }
            }
        }

        $out['insertaBien'] = true;
    }

    header("Content-type: application/json");
    echo json_encode($out);
    die();