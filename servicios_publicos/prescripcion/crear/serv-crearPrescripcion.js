const URL = 'servicios_publicos/prescripcion/crear/serv-crearPrescripcion.php';
import { filtroEnArrayDeObjetos } from './../../../funciones.js';

var app = new Vue({
    el: '#myapp',
	data:{

        error: '',
        loading: false,
        showUsuarios: false,
        dataUsu: [],
        consec: '',
        resolucion: '',
        codUsu: '',
        document: '',
        name: '',
        codCatastral: '',
        barrio: '',
        direccion: '',
        estrato: '',
        numFactura: '',
        deuda: '',
        descuento: '',
        totalPagar: '',
    },

    mounted: function(){

        this.datosIniciales();
    },

    methods: 
    {
        //Desglosar array
        toFormData: function(obj){
            var form_data = new FormData();
            for(var key in obj){
                form_data.append(key, obj[key]);
            }
            return form_data;
        },

        formatonumero: function(valor){
            return new Intl.NumberFormat("en-US", {style: "currency", currency: "USD"}).format(valor);
        },

        showModalUsu: function() {

            this.showUsuarios = true;
        },

        datosIniciales: async function() {
            
            await axios.post(URL+'?action=datosIni')
            .then((response) => {
                this.dataUsu = response.data.datosUsu;
                this.consec = response.data.conse;
            });
        },

        selecUsu: async function(cod) {

            await axios.post(URL+'?action=searchUsu&cod='+cod[0])
            .then((response) => {
                // console.log(response.data);
                this.barrio = response.data.barrio;
                this.direccion = response.data.direccion;
                this.estrato = response.data.estrato;
                this.numFactura = response.data.numFac;
                this.deuda = response.data.totalFactura;
                this.descuento = response.data.descuento;
                this.totalPagar = response.data.totalPagar;
            });

            this.codUsu = cod[0];
            this.document = cod[1];
            this.name = cod[2];
            this.codCatastral = cod[3];
            this.showUsuarios = false;
        },

        guardar: function() {

            const fecha = document.getElementById('fecha').value;
            if (this.consec != "" && fecha != "" && this.resolucion != "" && this.codUsu != "") {

                if (this.descuento > 0) {
                    var formData = new FormData();
                    formData.append("consec", this.consec);
                    formData.append("fecha", fecha);
                    formData.append("resolucion", this.resolucion);
                    formData.append("codUsu", this.codUsu);
                    formData.append("numFactura", this.numFactura);
                    formData.append("deuda", this.deuda);
                    formData.append("descuento", this.descuento);
                    formData.append("totalPagar", this.totalPagar);
    
                    axios.post(URL + '?action=guardar', formData)
                    .then((response) => {
                        console.log(response.data);
                        if(response.data.insertaBien){
                            Swal.fire({
                                title: "Guardado!",
                                text: "Prescripcion creada con exito!",
                                icon: "success"
                            });

                            this.consec = this.resolucion = this.codUsu = this.document = this.name = this.codCatastral = this.barrio = this.direccion = this.estrato = this.numFactura = this.deuda = this.descuento = this.totalPagar = "";
                            document.getElementById('fecha').value = "";
                            this.datosIniciales();
                        }
                        else{
                        
                            Swal.fire(
                                'Error!',
                                'No se pudo guardar.',
                                'error'
                            );
                        }
                    });
                }
                else {
                    Swal.fire('Error!','El descuente aplicable al usuario es de cero','error');
                }
            }
            else {
                Swal.fire('Error!','Faltan datos para guardar.','error');
            }
        }
    },
});