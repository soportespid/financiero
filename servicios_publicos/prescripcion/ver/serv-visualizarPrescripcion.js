const URL = 'servicios_publicos/prescripcion/ver/serv-visualizarPrescripcion.php';

var app = new Vue({
    el:"#myapp",
    data() {
        return {
            intId:0,
            intConsecutivo:0,
            strDescuento:"",
            strResolucion:"",
            strFecha:"",
            strDocumento:"",
            strNombre:"",
            strCatastral:"",
            strBarrio:"",
            strDireccion:"",
            strEstrato:"",
            strFactura:"",
            strTotal:"",
            strDeuda:"",
        }
    },
    async mounted() {
        await this.getData();
    },
    methods: {
        getData: async function(){
            this.intId = new URLSearchParams(window.location.search).get('id');
            if(this.intId>0){
                const formData = new FormData();
                formData.append("id",this.intId);
                const response = await fetch(URL,{method:"POST",body:formData});
                const objData = await response.json();

                if(objData.status){
                    const data = objData.data;
                    this.intConsecutivo = data.consecutivo;
                    this.strDescuento = this.formatNum(data.descuento);
                    this.strFecha = data.fecha;
                    this.strResolucion = data.resolucion;
                    this.strFactura = data.num_factura;
                    this.strTotal = this.formatNum(data.valor_pagar);
                    this.strDeuda = this.formatNum(data.valor_deuda);
                    this.strCatastral = data.catastro;
                    this.strDireccion = data.direccion;
                    this.strBarrio = data.barrio;
                    this.strDocumento = data.documento;
                    this.strNombre = data.nombre;
                    this.strEstrato = data.estrato;
                }
            }
        },
        printPDF: function(){
            window.open("serv-pdfPrescripciones.php?id="+this.intId);
        },
        formatNum: function(valor){
            return new Intl.NumberFormat("en-US", {style: "currency", currency: "USD"}).format(valor);
        },
    },
})
