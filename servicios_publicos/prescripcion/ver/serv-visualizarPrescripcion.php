<?php
	require_once '../../../comun.inc';
    require '../../../funciones.inc';
    require '../../../funcionesSP.inc.php';

    session_start();

    if($_POST){

        $obj = new Plantilla();
        $obj->show($_POST['id']);
    }

    class Plantilla{
        private $linkbd;
        private $intId;
        public function __construct(){
            $this->linkbd = conectar_v7();
            $this->linkbd->set_charset("utf8");
        }

        public function show($id){
            if($id > 0){
                $this->intId = $id;
                $sql = "SELECT *, DATE_FORMAT(fecha,'%d/%m/%Y') as fecha FROM srv_prescripciones WHERE id = $this->intId";
                $request = mysqli_query($this->linkbd,$sql)->fetch_assoc();
                $intCodigo = $request['cod_usuario'];
                $arrCliente = mysqli_query($this->linkbd,"SELECT id, id_tercero,id_barrio, cod_catastral, id_estrato FROM srvclientes WHERE cod_usuario =$intCodigo")->fetch_assoc();
                $request['barrio'] = buscaNombreBarrio($arrCliente['id_barrio']);
                $request['direccion'] = encuentraDireccionConIdCliente($arrCliente['id']);
                $request['estrato'] = buscarNombreEstrato($arrCliente['id_estrato']);
                $request['nombre'] = buscaNombreTerceroConId($arrCliente['id_tercero']);
                $request['documento'] = buscaDocumentoTerceroConId($arrCliente['id_tercero']);
                $request['catastro'] = $arrCliente['cod_catastral'];

                if(!empty($request)){
                    $arrResponse = array("status"=>true,"data"=>$request);
                }else{
                    $arrResponse = array("status"=>false,"msg"=>"Error, no existe la prescripción");
                }
            }else{
                $arrResponse = array("status"=>false,"msg"=>"Error de datos");
            }
            echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            die();
        }
    }
?>
