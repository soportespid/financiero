<?php
	require_once '../../../comun.inc';
    require '../../../funciones.inc';
    require '../../../funcionesSP.inc.php';

    session_start();

    if($_POST){

        $obj = new Plantilla();
        if($_POST['action'] == 'search'){
            $currentPage = intval($_POST['page']);
            $obj->show(
                $_POST['search'],
                $_POST['dateInicio'],
                $_POST['dateFinal'],
                $currentPage
            );
        }
    }

    class Plantilla{
        private $linkbd;
        private $dateInicio;
        private $dateFinal;
        public function __construct(){
            $this->linkbd = conectar_v7();
            $this->linkbd->set_charset("utf8");
        }

        public function show(string $search,string $fechaInicio, string $fechaFinal,int $currentPage){

            if($fechaInicio !="" && $fechaFinal ==""){
                $arrResponse = array("status"=>false,"msg"=>"Para buscar por fechas, debe llenar ambos campos. Inténtelo de nuevo.");
            }elseif($fechaInicio =="" && $fechaFinal !=""){
                $arrResponse = array("status"=>false,"msg"=>"Para buscar por fechas, debe llenar ambos campos. Inténtelo de nuevo.");
            }else{
                $dateRange ="";
                $perPage = 200;
                $startRows = ($currentPage-1) * $perPage;
                $totalRows = 0;
                if($fechaInicio !="" && $fechaFinal !=""){
                    $arrFechaInicio = explode("/",$fechaInicio);
                    $arrFechaFinal = explode("/",$fechaFinal);

                    $this->dateInicio = date_format(date_create($arrFechaInicio[2]."-".$arrFechaInicio[1]."-".$arrFechaInicio[0]),"Y-m-d");
                    $this->dateFinal = date_format(date_create($arrFechaFinal[2]."-".$arrFechaFinal[1]."-".$arrFechaFinal[0]),"Y-m-d");

                    $dateRange = " AND fecha >= '$this->dateInicio' AND fecha <= '$this->dateFinal'";
                }
                $sql = "SELECT
                        id,
                        consecutivo,
                        DATE(fecha) as fechaorden,
                        DATE_FORMAT(fecha,'%d/%m/%Y') as fecha,
                        cod_usuario,
                        num_factura,
                        descuento,
                        resolucion,
                        valor_deuda,
                        valor_pagar,
                        estado
                        FROM srv_prescripciones
                        WHERE (cod_usuario LIKE '$search%' OR consecutivo LIKE '$search%'
                        OR num_factura LIKE '$search%' OR descuento LIKE '$search%'
                        OR resolucion LIKE '$search%' OR valor_deuda LIKE '$search%'
                        OR valor_pagar LIKE '$search%') $dateRange
                        ORDER BY fechaorden DESC LIMIT $startRows, $perPage
                ";
                $request = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
                $totalRows = intval(mysqli_query($this->linkbd,
                "SELECT count(*) as total
                FROM srv_prescripciones
                WHERE (cod_usuario LIKE '$search%' OR consecutivo LIKE '$search%'
                OR num_factura LIKE '$search%' OR descuento LIKE '$search%'
                OR resolucion LIKE '$search%' OR valor_deuda LIKE '$search%'
                OR valor_pagar LIKE '$search%') $dateRange")->fetch_assoc()['total']);
                $totalPages = $totalRows > 0 ? ceil($totalRows/$perPage) : 1;
                $arrResponse = array("status"=>true,"data"=>$request,"total"=>$totalRows,"total_pages"=>$totalPages);
            }

            echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            die();
        }
    }
?>
