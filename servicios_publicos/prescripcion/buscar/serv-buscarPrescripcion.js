const URL = 'servicios_publicos/prescripcion/buscar/serv-buscarPrescripcion.php';
import { filtroEnArrayDeObjetos } from './../../../funciones.js';

var app = new Vue({
    el: '#myapp',
    data() {
        return {
            arrData:[],
            strSearch:"",
            intResults:0,
            intTotalPages:1,
            intPage:1,
            isLoading: false,
            error: ''
        }
    },

    mounted: function(){
        this.search();
    },

    methods: 
    {
        search:async function(page=1){

            let fechaInicial = document.querySelector("#fechaInicial").value;
            let fechaFinal = document.querySelector("#fechaFinal").value;

            if(page <= 0){
                page = 1;
                this.intPage = page;
            }else if(page > this.intTotalPages){
                page = this.intTotalPages;
                this.intPage = page;
            }

            const formData = new FormData();
            formData.append("action","search");
            formData.append("search",this.strSearch);
            formData.append("dateInicio",fechaInicial);
            formData.append("dateFinal",fechaFinal);
            formData.append("page",page);
            this.isLoading = true;
            const response = await fetch(URL,{method:'POST',body:formData});
            const objData = await response.json();
            if(objData.status){
                this.arrData = objData.data;
                this.intResults = objData.total;
                this.intTotalPages = objData.total_pages;
            }else{
                Swal.fire("Error",objData.msg,"error");
            }
            this.isLoading = false;
        },
        formatNumero: function(valor){
            return new Intl.NumberFormat("en-US", {style: "currency", currency: "USD"}).format(valor);
        },
        view:function(id){
            window.location.href='serv-visualizarPrescripcion.php?id='+id;
        },
    },
});