<?php

	require_once '../../comun.inc';
    require '../../funciones.inc';
    require '../../funcionesSP.inc.php';
    header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
    session_start();

	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");

    $out = array('error' => false);

    $action = "show";

    if(isset($_GET['action'])){
        $action = $_GET['action'];
    }

    if ($action == "datosIni") {

        $periodosLiquidados = [];

        $sql = "SET lc_time_names = 'es_ES'";
        mysqli_query($linkbd,$sql);

        $sqlr = "SELECT numero_corte, UPPER(MONTHNAME(fecha_inicial)), UPPER(MONTHNAME(fecha_final)), YEAR(fecha_inicial), YEAR(fecha_final) FROM srvcortes WHERE numero_corte > 0 ORDER BY numero_corte DESC";
        $resp = mysqli_query($linkbd,$sqlr);
        while ($row = mysqli_fetch_row($resp)) {
            array_push($periodosLiquidados, $row);
        }

        $out['periodos'] = $periodosLiquidados;
    }

    if ($action == "buscaInfo") {

        $corteLiquidado = $_GET['periodo'];

        $servicios = [];
        
        $sqlServ = "SELECT id, nombre, tarifa_medidor FROM srvservicios WHERE estado = 'S'";
        $resServ = mysqli_query($linkbd, $sqlServ);
        $servicios = mysqli_fetch_all($resServ);
        
        $principal = [];
        
        foreach ($servicios as $key => $servicio) {

            $datos = [];
            $cantidadUsu = $consumoM3 = $cargoFijo = $consumo = $subsidio = $contribucion = $subtotal = 0;

            $sqlEstratos = "SELECT id, descripcion FROM srvestratos WHERE estado = 'S' ORDER BY id ASC";
            $resEstratos = mysqli_query($linkbd, $sqlEstratos);
            while ($rowEstratos = mysqli_fetch_row($resEstratos)) {
                
                $datosTemp = [];
                /*
                    Estrato
                    Cantidad de usuarios
                    consumo m3
                    cargo fijo
                    consumo
                    subsidiado
                    contribuciones
                    diferencia 
                */
        
                //Consumo m3
                $sqlLectura = "SELECT COALESCE(SUM(consumo),0) FROM srvlectura AS SL, srvclientes AS SC WHERE SC.id = SL.id_cliente AND SL.corte = $corteLiquidado AND SL.id_servicio = $servicio[0] AND SC.id_estrato = $rowEstratos[0]";
                $rowLectura = mysqli_fetch_row(mysqli_query($linkbd, $sqlLectura));
        
                //Cantidad de usuarios y cargo fijo       
                $sqlCargoFijo = "SELECT COALESCE(COUNT(SF.id_cliente), 0), COALESCE(SUM(SF.credito), 0) FROM srvdetalles_facturacion AS SF, srvclientes AS SC WHERE SC.id = SF.id_cliente AND SF.corte = $corteLiquidado AND SF.id_servicio = $servicio[0] AND SC.id_estrato = $rowEstratos[0] AND SF.id_tipo_cobro = 1 AND SF.tipo_movimiento = '101'";
                $rowCargoFijo = mysqli_fetch_row(mysqli_query($linkbd, $sqlCargoFijo));
        
                //Consumo
                $sqlConsumo = "SELECT COALESCE(SUM(SF.credito), 0) FROM srvdetalles_facturacion AS SF, srvclientes AS SC WHERE SC.id = SF.id_cliente AND SF.corte = $corteLiquidado AND SF.id_servicio = $servicio[0] AND SC.id_estrato = $rowEstratos[0] AND SF.id_tipo_cobro = 2 AND SF.tipo_movimiento = '101'";
                $rowConsumo = mysqli_fetch_row(mysqli_query($linkbd, $sqlConsumo));
        
                //subsidiado
                $sqlSubsidiado = "SELECT COALESCE(SUM(SF.debito), 0) FROM srvdetalles_facturacion AS SF, srvclientes AS SC WHERE SC.id = SF.id_cliente AND SF.corte = $corteLiquidado AND SF.id_servicio = $servicio[0] AND SC.id_estrato = $rowEstratos[0] AND (SF.id_tipo_cobro = 3 OR SF.id_tipo_cobro = 4) AND SF.tipo_movimiento = '101'";
                $rowSubsidiado = mysqli_fetch_row(mysqli_query($linkbd, $sqlSubsidiado));
        
                //Contribucion
                $sqlContribucion = "SELECT COALESCE(SUM(SF.credito), 0) FROM srvdetalles_facturacion AS SF, srvclientes AS SC WHERE SC.id = SF.id_cliente AND SF.corte = $corteLiquidado AND SF.id_servicio = $servicio[0] AND SC.id_estrato = $rowEstratos[0] AND SF.id_tipo_cobro = 5 AND SF.tipo_movimiento = '101'";
                $rowContribucion = mysqli_fetch_row(mysqli_query($linkbd, $sqlContribucion));

                $cantidadUsu += $rowCargoFijo[0];
                $consumoM3 += $rowLectura[0];
                $cargoFijo += $rowCargoFijo[1];
                $consumo += $rowConsumo[0];
                $subsidio += $rowSubsidiado[0];
                $contribucion += $rowContribucion[0];
                $subtotal += $rowSubsidiado[0] - $rowContribucion[0];
        
                $datosTemp[0] = "$rowEstratos[0] - $rowEstratos[1]";
                $datosTemp[1] = $rowCargoFijo[0];
                $datosTemp[2] = $rowLectura[0];
                $datosTemp[3] = $rowCargoFijo[1];
                $datosTemp[4] = $rowConsumo[0];
                $datosTemp[5] = $rowSubsidiado[0];
                $datosTemp[6] = $rowContribucion[0];
                $datosTemp[7] = $rowSubsidiado[0] - $rowContribucion[0];
                array_push($datos, $datosTemp);
            }   

            $datosTemp = [];
            $datosTemp[] = "TOTALES";
            $datosTemp[] = $cantidadUsu;
            $datosTemp[] = $consumoM3;
            $datosTemp[] = $cargoFijo;
            $datosTemp[] = $consumo;
            $datosTemp[] = $subsidio;
            $datosTemp[] = $contribucion;
            $datosTemp[] = $subtotal;
            array_push($datos, $datosTemp);

            $principal[$key][0] = "$servicio[0] - $servicio[1]";        
            $principal[$key][1] = $datos;
        }
    

        $out["principal"] = $principal;
    }

    header("Content-type: application/json");
    echo json_encode($out);
    die();