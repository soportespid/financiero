<?php

	require_once '../../comun.inc';
    require '../../funciones.inc';
    require '../../funcionesSP.inc.php';
    header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
    session_start();

	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");

    $out = array('error' => false);

    $action = "show";

    if(isset($_GET['action'])){
        $action = $_GET['action'];
    }

    function searchEstrato($estratos, $estratoId) {    
        foreach ($estratos as $key => $estrato) {
            if ($estrato["id"] === $estratoId) {
                return $estrato["descripcion"];
            }
        }
    }

    if ($action == "datosIni") {

        $periodosLiquidados = [];

        $sql = "SET lc_time_names = 'es_ES'";
        mysqli_query($linkbd,$sql);

        $sqlr = "SELECT numero_corte, UPPER(MONTHNAME(fecha_inicial)), UPPER(MONTHNAME(fecha_final)), YEAR(fecha_inicial), YEAR(fecha_final) FROM srvcortes WHERE numero_corte > 0 ORDER BY numero_corte DESC";
        $resp = mysqli_query($linkbd,$sqlr);
        while ($row = mysqli_fetch_row($resp)) {
            array_push($periodosLiquidados, $row);
        }

        $out['periodos'] = $periodosLiquidados;
    }

    if ($action == "buscaInfo") {

        $corteLiquidado = $_GET["periodo"];

        $facturas = $estratos = $datos = [];

        $sqlEstratos = "SELECT id, descripcion FROM srvestratos ORDER BY id";
        $resEstratos = mysqli_query($linkbd, $sqlEstratos);
        $estratos = mysqli_fetch_all($resEstratos, MYSQLI_ASSOC);
        
        $sqlFacturas = "SELECT numero_facturacion AS numFactura, id_cliente AS clienteId FROM srvcortes_detalle WHERE id_corte = $corteLiquidado ORDER BY numero_facturacion ASC";
        $resFacturas = mysqli_query($linkbd, $sqlFacturas);
        $facturas = mysqli_fetch_all($resFacturas, MYSQLI_ASSOC);

        foreach ($facturas as $key => $factura) {
            
            $datosUsu = [];
            $codUsuario = $estrato = "";
            $acueducto = $alcantarillado = $aseo = 0;

            $sqlCliente = "SELECT cod_usuario AS codUsuario, id_estrato AS estratoId FROM srvclientes WHERE id = $factura[clienteId]";
            $rowCliente = mysqli_fetch_assoc(mysqli_query($linkbd, $sqlCliente));

            $codUsuario = $rowCliente["codUsuario"];
            $estrato = searchEstrato($estratos, $rowCliente["estratoId"]);

            $sqlFacturaAc = "SELECT subsidio_cf, subsidio_cb, subsidio_cc, subsidio_cs, contribucion_cf, contribucion_cb, contribucion_cc, contribucion_cs FROM srvfacturas WHERE num_factura = $factura[numFactura] AND id_servicio = 1";
            $rowFacturaAc = mysqli_fetch_assoc(mysqli_query($linkbd, $sqlFacturaAc));

            $acueducto = $rowFacturaAc["subsidio_cf"] + $rowFacturaAc["subsidio_cb"] + $rowFacturaAc["subsidio_cc"] + $rowFacturaAc["subsidio_cs"] - $rowFacturaAc["contribucion_cf"] - $rowFacturaAc["contribucion_cb"] - $rowFacturaAc["contribucion_cc"] - $rowFacturaAc["contribucion_cs"];

            $sqlFacturaAlc = "SELECT subsidio_cf, subsidio_cb, subsidio_cc, subsidio_cs, contribucion_cf, contribucion_cb, contribucion_cc, contribucion_cs FROM srvfacturas WHERE num_factura = $factura[numFactura] AND id_servicio = 2";
            $rowFacturaAlc = mysqli_fetch_assoc(mysqli_query($linkbd, $sqlFacturaAlc));

            $alcantarillado = $rowFacturaAlc["subsidio_cf"] + $rowFacturaAlc["subsidio_cb"] + $rowFacturaAlc["subsidio_cc"] + $rowFacturaAlc["subsidio_cs"] - $rowFacturaAlc["contribucion_cf"] - $rowFacturaAlc["contribucion_cb"] - $rowFacturaAlc["contribucion_cc"] - $rowFacturaAlc["contribucion_cs"];

            $sqlFacturaAseo = "SELECT subsidio_cf, subsidio_cb, subsidio_cc, subsidio_cs, contribucion_cf, contribucion_cb, contribucion_cc, contribucion_cs FROM srvfacturas WHERE num_factura = $factura[numFactura] AND id_servicio = 3";
            $rowFacturaAseo = mysqli_fetch_assoc(mysqli_query($linkbd, $sqlFacturaAseo));

            $aseo = $rowFacturaAseo["subsidio_cf"] + $rowFacturaAseo["subsidio_cb"] + $rowFacturaAseo["subsidio_cc"] + $rowFacturaAseo["subsidio_cs"] - $rowFacturaAseo["contribucion_cf"] - $rowFacturaAseo["contribucion_cb"] - $rowFacturaAseo["contribucion_cc"] - $rowFacturaAseo["contribucion_cs"];

            $datosUsu["codUsuario"] = $codUsuario;
            $datosUsu["estrato"] = $estrato;
            $datosUsu["numFactura"] = $factura["numFactura"];
            $datosUsu["acueducto"] = $acueducto;
            $datosUsu["alcantarillado"] = $alcantarillado;
            $datosUsu["aseo"] = $aseo;
            array_push($datos, $datosUsu);
        }

        $out['datos'] = $datos;
    }

    header("Content-type: application/json");
    echo json_encode($out);
    die();