const URL = 'servicios_publicos/informeSubsidios/serv-informeSubsidiosPorUsuario.php';
import { filtroEnArrayDeObjetos } from './../../funciones.js';

var app = new Vue({
    el: '#myapp',
	data:{

        error: '',
        loading: false,
        showUsuarios: false,
        periodo: '',
        periodos: [],
        arrData: [],
    },

    mounted: function(){

        this.datosIniciales();
    },

    methods: 
    {
        //Desglosar array
        toFormData: function(obj){
            var form_data = new FormData();
            for(var key in obj){
                form_data.append(key, obj[key]);
            }
            return form_data;
        },

        formatonumero: function(valor){
            return new Intl.NumberFormat("en-US", {style: "currency", currency: "USD"}).format(valor);
        },

        showModalUsu: function() {

            this.showUsuarios = true;
        },

        datosIniciales: async function() {
            
            await axios.post(URL+'?action=datosIni')
            .then((response) => {
                this.periodos = response.data.periodos;
            });
        },

        buscaDatos: async function() {

            if (this.periodo != "") {
                this.loading = true;
                await axios.post(URL+'?action=buscaInfo&periodo='+this.periodo)
                .then((response) => {
                    this.arrData = response.data.datos;
                });
                this.loading = false;
            }
        },

        printExcel: function(){

            document.form2.action="serv-informeSubsidioUsuarioExcel.php";
            document.form2.target="_BLANK";
            document.form2.submit(); 
        },

        // printPDF:function(){

        //     var text = JSON.stringify(this.principal);

        //     window.open('serv-reporteSubsidiosPDF.php?datos='+text+'&corte='+this.periodo,'_blank');
        // },
    },
});