const URL = 'servicios_publicos/informeSubsidios/serv-informeSubsidios.php';
import { filtroEnArrayDeObjetos } from './../../funciones.js';

var app = new Vue({
    el: '#myapp',
	data:{

        error: '',
        loading: false,
        showUsuarios: false,
        periodo: '',
        periodos: [],
        principal: [],
    },

    mounted: function(){

        this.datosIniciales();
    },

    methods: 
    {
        //Desglosar array
        toFormData: function(obj){
            var form_data = new FormData();
            for(var key in obj){
                form_data.append(key, obj[key]);
            }
            return form_data;
        },

        formatonumero: function(valor){
            return new Intl.NumberFormat("en-US", {style: "currency", currency: "USD"}).format(valor);
        },

        showModalUsu: function() {

            this.showUsuarios = true;
        },

        datosIniciales: async function() {
            
            await axios.post(URL+'?action=datosIni')
            .then((response) => {
                console.log(response.data);
                this.periodos = response.data.periodos;
                // this.principal = response.data.principal;
            });
        },

        buscaDatos: async function() {

            if (this.periodo != "") {
                this.loading = true;
                await axios.post(URL+'?action=buscaInfo&periodo='+this.periodo)
                .then((response) => {
                    console.log(response.data);
                    this.principal = response.data.principal;
                });
                this.loading = false;
            }
        },

        printExcel: function(){

            var text = JSON.stringify(this.principal);
         
            window.open('serv-informeSubsidiosExcel.php?text='+text,'_blank');
        },

        printPDF:function(){

            var text = JSON.stringify(this.principal);

            window.open('serv-reporteSubsidiosPDF.php?datos='+text+'&corte='+this.periodo,'_blank');
        },
    },
});