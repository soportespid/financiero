<?php
    require_once '../../comun.inc';
    require '../../funciones.inc';
    require '../../funcionesSP.inc.php';
    session_start();

    $linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");

    $out = array('error' => false);

    $action = "show";

    if(isset($_GET['action'])){
        $action = $_GET['action'];
    }
    
    if ($action == "inicial") {

        $usuarios = array();
        
        $sqlClientes = "SELECT id, cod_usuario, id_tercero, cod_catastral FROM srvclientes WHERE estado = 'S' ORDER BY id ASC";
        $resClientes = mysqli_query($linkbd, $sqlClientes);
        while ($rowClientes = mysqli_fetch_assoc($resClientes)) {
            
            $datosTemp = [];
            $nombreSuscriptor = "";

            //Datos tercero
            $sqlTercero = "SELECT persona, nombre1, nombre2, apellido1, apellido2, razonsocial, cedulanit FROM terceros WHERE id_tercero = '$rowClientes[id_tercero]'";
            $rowTercero = mysqli_fetch_assoc(mysqli_query($linkbd, $sqlTercero));

            if ($rowTercero['persona'] == 1) {

                $nombreSuscriptor = $rowTercero['razonsocial'];
            }
            else if ($rowTercero['persona'] == 2) {
                
                $nombreSuscriptor = $rowTercero['nombre1'] . " " . $rowTercero['nombre2'] . " " . $rowTercero['apellido1'] . " " . $rowTercero['apellido2'];
            }

            $datosTemp['Codigo'] = $rowClientes['cod_usuario'];
            $datosTemp['Documento'] = $rowTercero['cedulanit'];
            $datosTemp['Catastral'] = $rowClientes['cod_catastral'];
            $datosTemp['Nombre'] = $nombreSuscriptor;
            array_push($usuarios, $datosTemp);
        }

        $out['usuarios'] = $usuarios;
    }

    if ($action == "searchDataValid") {
       
        $codUsuario = "";
        $codUsuario = $_GET["cod"];

        $sqlCorte = "SELECT numero_corte FROM srvcortes WHERE estado = 'S'";
        $rowCorte = mysqli_fetch_assoc(mysqli_query($linkbd, $sqlCorte));

        $sqlCliente = "SELECT id FROM srvclientes WHERE cod_usuario = '$codUsuario' AND estado = 'S'";
        $rowCliente = mysqli_fetch_assoc(mysqli_query($linkbd, $sqlCliente));

        $sqlEstado = "SELECT estado_pago FROM srvcortes_detalle WHERE id_cliente = $rowCliente[id] AND id_corte = $rowCorte[numero_corte] AND estado_pago != 'R' AND estado_pago != 'PC'";
        $rowEstado = mysqli_fetch_assoc(mysqli_query($linkbd, $sqlEstado));

        $out["estadoPago"] = $rowEstado["estado_pago"];
    }

    header("Content-type: application/json");
    echo json_encode($out);
    die();