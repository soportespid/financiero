const URL = 'servicios_publicos/certificados/serv-certificadoPazySalvo.php';
import { filtroEnArrayDeObjetos } from './../../funciones.js';

var app = new Vue({
    el: '#myapp',
	data:{

        error: '',
        loading: false, 
        usuarios: [],
        usuario: '',
        showModalUsuarios: false,
    },

    mounted: function(){

        this.datosIniciales();
    },

    methods: 
    {
        //Desglosar array
        toFormData: function(obj){
            var form_data = new FormData();
            for(var key in obj){
                form_data.append(key, obj[key]);
            }
            return form_data;
        },

        formatonumero: function(valor){
            return new Intl.NumberFormat("en-US", {style: "currency", currency: "USD"}).format(valor);
        },

        datosIniciales: async function() {
            
            this.loading = true;

            await axios.post(URL+'?action=inicial')
            .then((response) => {
                this.usuarios = response.data.usuarios;
            });

            this.loading = false;
        },

        showUsuarios: function() {
            this.showModalUsuarios = true;
        },

        selecUsuario: function(periodo) {
            this.usuario = periodo.Codigo;
            this.showModalUsuarios = false;
        },

        searchData: function() {

            if (this.usuario != "") {

                axios.post(URL+'?action=searchDataValid&cod='+this.usuario).then((response) => {
                    
                    if (response.data.estadoPago == 'P') {
                        window.open('serv-pazysalvoPDF.php?cod='+this.usuario,'_blank');
                    }
                    else {
                        Swal.fire("Error", "El usuario no esta al día en los pagos de factura", "warning");    
                    }
                });
            }
        },

        excel: function() {

            if (this.datosUsuarios != '') {

                document.form2.action="serv-reporteRecaudosPorServicioExcel.php";
				document.form2.target="_BLANK";
				document.form2.submit(); 
            }
            else {
                Swal.fire("Faltan datos", "Generar lecturas primero", "warning");
            }
        },
    },
});