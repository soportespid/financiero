var app = new Vue({
    el: '#myapp',
	data:{

        error: '',
        loading: false,
        servicio: "",
        servicios: [],
        nombreClase: "",
        idClase: "",
        clases: [],
        showClases: false,
        searchClase : {keywordClase: ''},
        tarifas: [],
        valoresCF: [],
        valoresSubsidio: [],
        valoresContribucion: [],
    },

    mounted: function(){

        this.llamaServicios();
    },

    methods: 
    {
        toFormData: function(obj){
            var form_data = new FormData();
            for(var key in obj){
                form_data.append(key, obj[key]);
            }
            return form_data;
        },

        llamaServicios: async function() {
            
            await axios.post('servicios_publicos/cargo_fijo/serv-cargoFijo.php?action=servicios')
            .then((response) => {
                
                app.servicios = response.data.servicios;
            }).catch((error) => {
                this.error = true;
                console.log(error)
            }).finally(() => {
        
            });     
        },

        llamaClases: async function() {

            await axios.post('servicios_publicos/cargo_fijo/serv-cargoFijo.php?action=clases')
            .then((response) => {
                
                app.clases = response.data.clases;
            }).catch((error) => {
                this.error = true;
                console.log(error)
            }).finally(() => {
        
            });     
        },

        despliegaModalClases: function() {

            this.llamaClases();
            this.showClases = true;
        },

        seleccionaClase: function(clase) {

            this.idClase = clase[0];
            this.nombreClase = clase[1];
            this.showClases = false;
        },

        searchMonitorClase: async function(){ 

            var keywordClase = app.toFormData(this.searchClase);

            await axios.post('servicios_publicos/cargo_fijo/serv-cargoFijo.php?action=filtraClases', keywordClase)
            .then((response) => {
                
                app.clases = response.data.clases;
            }).catch((error) => {
                this.error = true;
                console.log(error)
            }).finally(() => {
        
            });  
        },

        buscarTarifas: async function() {

            if (this.idClase != "" && this.nombreClase != "" && this.servicio != "") {
                
                this.loading = true;

                await axios.post('servicios_publicos/cargo_fijo/serv-cargoFijo.php?action=tarifas&servicio='+this.servicio+'&clase='+this.idClase)
                .then((response) => {
                    app.tarifas = response.data.tarifas;
                    app.llenadoValores();
                }).catch((error) => {
                    this.error = true;
                    console.log(error)
                }).finally(() => {
                    this.loading = false;
                });  
            }
            else {
                Swal.fire("Error", "Seleccione servicio y clase de uso", "warning");
            }
        },

        llenadoValores: function() {

            for (let i = 0; i < app.tarifas.length; i++) {
                            
                this.valoresCF[i] = app.tarifas[i][2];
                this.valoresSubsidio[i] = app.tarifas[i][3];
                this.valoresContribucion[i] = app.tarifas[i][4];
            }
        },

        formatonumero: function(valor){
            return new Intl.NumberFormat("en-US", {style: "currency", currency: "USD"}).format(valor);
        },

        guardar: function() {
            if (this.tarifas.length != 0 && this.valoresCF.length != 0 && this.valoresSubsidio.length != 0 && this.valoresContribucion.length != 0 && this.servicio != "" && this.idClase != "") {

                Swal.fire({
                    icon: 'question',
                    title: 'Seguro que quieres guardar?',
                    showDenyButton: true,
                    confirmButtonText: 'Guardar',
                    denyButtonText: 'Cancelar',
                    }).then((result) => {
                        
                    if (result.isConfirmed) {

                        var formData = new FormData();
                        
                        for (let i = 0; i < this.tarifas.length; i++) {
                            
                            formData.append('ubicaciones[]', this.tarifas[i][0]); 
                            formData.append('valorCF[]', this.valoresCF[i]);   
                            formData.append('valorSubsdio[]', this.valoresSubsidio[i]); 
                            formData.append('valorContribucion[]', this.valoresContribucion[i]); 
                        }

                        formData.append("servicio", this.servicio);
                        formData.append("clase", this.idClase);

                        this.loading = true;
                        
                        axios.post('servicios_publicos/cargo_fijo/serv-cargoFijo.php?action=guardar', formData)
                        .then((response) => {
                            console.log(response.data); 
                            if(response.data.insertaBien) {
                                
                                this.loading = false;
                                Swal.fire({
                                    icon: 'success',
                                    title: 'Se ha guardado con exito',
                                    showConfirmButton: false,
                                    timer: 1500
                                    }).then((response) => {
                                        app.redireccionarCrear();
                                    });
                            }
                            else {
                                Swal.fire(
                                    'Error!',
                                    'No se pudo guardar.',
                                    'error'
                                );
                            }
                        });
                    }
                    else if (result.isDenied) 
                    {
                        Swal.fire('Guardar cancelado', '', 'info');
                    }
                });
            }
            else {
                Swal.fire("Error", "Faltan datos por llenar", "warning")
            }
        },

        redireccionarCrear: function()
        {
            location.href = "serv-tarifaCargoFijo.php";
        },

        validaValorCF: async function(item) {

            const valorOriginal = this.tarifas[item][2];

            if (parseFloat(this.valoresCF[item]) < 0) {
    
                this.valoresCF[item] = valorOriginal;
                this.valoresCF = Object.values(this.valoresCF);
                Swal.fire("Error", "Valor de cargo fijo debe ser mayor a cero", "warning");
            }
        },

        validaSubsidio: async function(item) {

            const valorOriginal = this.tarifas[item][3];

            if (parseFloat(this.valoresSubsidio[item]) > 0) {
    
                if (parseFloat(this.valoresContribucion[item]) != 0) {
                
                    this.valoresSubsidio[item] = valorOriginal;
                    this.valoresSubsidio = Object.values(this.valoresSubsidio);
                    Swal.fire("Error", "Valor de contribucion debe ser cero", "warning");
                } 
            }
            else {
                this.valoresSubsidio[item] = valorOriginal;
                this.valoresSubsidio = Object.values(this.valoresSubsidio);
                Swal.fire("Error", "Valor de subsidio debe ser mayor a cero", "warning");
            }
        },

        validaContribucion: async function(item) {

            const valorOriginal = this.tarifas[item][4];

            if (parseFloat(this.valoresContribucion[item]) > 0) {
    
                if (parseFloat(this.valoresContribucion[item]) != 0) {
                
                    this.valoresContribucion[item] = valorOriginal;
                    this.valoresContribucion = Object.values(this.valoresContribucion);
                    Swal.fire("Error", "Valor de subsidio debe ser cero", "warning");
                } 
            }
            else {
                this.valoresContribucion[item] = valorOriginal;
                this.valoresContribucion = Object.values(this.valoresContribucion);
                Swal.fire("Error", "Valor de contribucion debe ser mayor a cero", "warning");
            }
        },
    }
});