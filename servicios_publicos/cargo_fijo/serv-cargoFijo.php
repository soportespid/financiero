<?php
    require_once '../../comun.inc';
    require '../../funciones.inc';
    require '../../funcionesSP.inc.php';
    session_start();

    $linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");

    $out = array('error' => false);

    $action = "show";

    if(isset($_GET['action'])){
        $action = $_GET['action'];
    }
    
    if ($action == "servicios") {

        $servicios = array();
        
        $sql = "SELECT id, nombre FROM srvservicios WHERE estado = 'S'";
        $res = mysqli_query($linkbd, $sql);
        while ($row = mysqli_fetch_row($res)) {

            array_push($servicios, $row);
        }

        $out['servicios'] = $servicios;
    }

    if ($action == "clases") {

        $clases = array();

        $sql = "SELECT id_clase, nombre_clase FROM srv_clases WHERE estado = 'S'";
        $res = mysqli_query($linkbd, $sql);
        while ($row = mysqli_fetch_row($res)) {

            array_push($clases, $row);
        }

        $out['clases'] = $clases;
    }

    if ($action == "filtraClases") {

        $keywordClase = $_POST['keywordClase'];
        
        $clases = array();

        $sql = "SELECT id_clase, nombre_clase FROM srv_clases WHERE estado = 'S' AND concat_ws(' ', id_clase, nombre_clase) LIKE '%$keywordClase%'";
        $res = mysqli_query($linkbd, $sql);
        while ($row = mysqli_fetch_row($res)) {

            array_push($clases, $row);
        }

        $out['clases'] = $clases;
    }

    if ($action == "tarifas") {
        
        $idServicio = $_GET['servicio'];
        $idClase = $_GET['clase'];

        $tarifas = array();

        $sqlUbicaciones = "SELECT id_ubicacion, nombre_ubicacion FROM srv_ubicacion WHERE estado = 'S'";
        $resUbidaciones = mysqli_query($linkbd, $sqlUbicaciones);
        while ($rowUbicaciones = mysqli_fetch_row($resUbidaciones)) {
            
            $datos = array();
            $cargoFijo = 0;
            $subsidio = 0;
            $contribucion = 0;

            $sqlCargoFijo = "SELECT valor, subsidio, contribucion FROM srv_tarifa_cargo_fijo WHERE id_servicio = $idServicio AND id_clase = $idClase AND id_ubicacion = $rowUbicaciones[0]";
            $rowCargoFijo = mysqli_fetch_row(mysqli_query($linkbd, $sqlCargoFijo));

            if ($rowCargoFijo[0] == "") {
                $cargoFijo = 0;
            }else{
                $cargoFijo = $rowCargoFijo[0];
            }

            if ($rowCargoFijo[1] == "") {
                $subsidio = 0;
            }else{
                $subsidio = $rowCargoFijo[1];
            }

            if ($rowCargoFijo[2] == "") {
                $contribucion = 0;
            }else{
                $contribucion = $rowCargoFijo[2];
            }

            array_push($datos, $rowUbicaciones[0]);
            array_push($datos, $rowUbicaciones[1]);
            array_push($datos, $cargoFijo);
            array_push($datos, $subsidio);
            array_push($datos, $contribucion);
            array_push($tarifas, $datos);
        }

        $out['tarifas'] = $tarifas;
        
    }

    if ($action == "guardar") {

        //Borrado de tarifa anterior       
        $sqlBorrado = "DELETE FROM srv_tarifa_cargo_fijo WHERE id_servicio = $_POST[servicio] AND id_clase = $_POST[clase]";
        mysqli_query($linkbd, $sqlBorrado);

        for ($i=0; $i < count($_POST['ubicaciones']); $i++) { 
            
            $sqlTarifasCF = "INSERT INTO srv_tarifa_cargo_fijo (id_servicio, id_clase, id_ubicacion, valor, subsidio, contribucion) VALUES ($_POST[servicio], $_POST[clase], '".$_POST["ubicaciones"][$i]."', '".$_POST["valorCF"][$i]."', '".$_POST["valorSubsdio"][$i]."', '".$_POST["valorContribucion"][$i]."' )";
            mysqli_query($linkbd, $sqlTarifasCF);
        }

        $out['insertaBien'] = true;
    }

    header("Content-type: application/json");
    echo json_encode($out);
    die();