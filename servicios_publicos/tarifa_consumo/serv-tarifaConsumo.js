var app = new Vue({
    el: '#myapp',
	data:{

        error: '',
        loading: false,

        servicio: "",
        servicios: [],
        nombreClase: "",
        idClase: "",
        clases: [],
        showClases: false,
        searchClase : {keywordClase: ''},
        tarifas: [],
        valor: [],
        subsidio: [],
        contribucion: [],
    },

    mounted: function(){

        this.llamaServicios();
    },

    methods: 
    {
        toFormData: function(obj){
            var form_data = new FormData();
            for(var key in obj){
                form_data.append(key, obj[key]);
            }
            return form_data;
        },

        llenadoValores: function() {

            for (let i = 0; i < app.tarifas.length; i++) {
                            
                this.valor[i] = app.tarifas[i][4];
                this.subsidio[i] = app.tarifas[i][5];
                this.contribucion[i] = app.tarifas[i][6];
            }

            this.valor = Object.values(this.valor);
            this.subsidio = Object.values(this.subsidio);
            this.contribucion = Object.values(this.contribucion);
        },

        llamaServicios: async function() {
            
            await axios.post('servicios_publicos/cargo_fijo/serv-cargoFijo.php?action=servicios')
            .then((response) => {
                
                app.servicios = response.data.servicios;
            }).catch((error) => {
                this.error = true;
                console.log(error)
            }).finally(() => {
                
            });     
        },

        llamaClases: async function() {

            await axios.post('servicios_publicos/cargo_fijo/serv-cargoFijo.php?action=clases')
            .then((response) => {
                
                app.clases = response.data.clases;
            }).catch((error) => {
                this.error = true;
                console.log(error)
            }).finally(() => {
        
            });     
        },

        despliegaModalClases: function() {

            this.llamaClases();
            this.showClases = true;
        },

        seleccionaClase: function(clase) {

            this.idClase = clase[0];
            this.nombreClase = clase[1];
            this.showClases = false;
        },

        searchMonitorClase: async function(){ 

            var keywordClase = app.toFormData(this.searchClase);

            await axios.post('servicios_publicos/cargo_fijo/serv-cargoFijo.php?action=filtraClases', keywordClase)
            .then((response) => {
                
                app.clases = response.data.clases;
            }).catch((error) => {
                this.error = true;
                console.log(error)
            }).finally(() => {
        
            });  
        },

        buscarTarifas: async function() {

            if (this.idClase != "" && this.nombreClase != "" && this.servicio != "") {
                
                this.loading = true;
                
                await axios.post('servicios_publicos/tarifa_consumo/serv-tarifaConsumo.php?action=tarifas&servicio='+this.servicio+'&clase='+this.idClase)
                .then((response) => {
                    app.tarifas = response.data.tarifas;
                }).catch((error) => {
                    this.error = true;
                    console.log(error)
                }).finally(() => {
                    this.loading = false;
                    this.llenadoValores();
                });  
            }
            else {
                Swal.fire("Error", "Seleccione servicio y clase de uso", "warning");
            }
        },

        validaValor: function(posicion) {

            const valor = this.tarifas[posicion][4];

            if (parseFloat(this.valor[posicion]) < 0) {
    
                this.valor[posicion] = valor;
                this.valor = Object.values(this.valor);
                Swal.fire("Error", "Valor de cargo fijo debe ser mayor a cero", "warning");
            }
        },

        validaSubsidio: function(item) {

            const valorOriginal = this.tarifas[item][5];

            if (parseFloat(this.subsidio[item]) > 0) {
    
                if (parseFloat(this.contribucion[item]) != 0) {
                
                    this.subsidio[item] = valorOriginal;
                    this.subsidio = Object.values(this.subsidio);
                    Swal.fire("Error", "Valor de contribucion debe ser cero", "warning");
                } 
            }
            else {
                this.subsidio[item] = valorOriginal;
                this.subsidio = Object.values(this.subsidio);
                Swal.fire("Error", "Valor de subsidio debe ser mayor a cero", "warning");
            }
        },

        validaContribucion: function(item) {

            const valorOriginal = this.tarifas[item][6];

            if (parseFloat(this.contribucion[item]) > 0) {
    
                if (parseFloat(this.contribucion[item]) != 0) {
                
                    this.contribucion[item] = valorOriginal;
                    this.contribucion = Object.values(this.contribucion);
                    Swal.fire("Error", "Valor de subsidio debe ser cero", "warning");
                } 
            }
            else {
                this.contribucion[item] = valorOriginal;
                this.contribucion = Object.values(this.contribucion);
                Swal.fire("Error", "Valor de contribucion debe ser mayor a cero", "warning");
            }
        },

        guardar: function() {

            if (this.servicio != '' && this.clase != '' && this.tarifas.length != 0 && this.valor.length != 0 && this.subsidio.length != 0 && this.contribucion.length != 0) {

                Swal.fire({
                    icon: 'question',
                    title: 'Seguro que quieres guardar?',
                    showDenyButton: true,
                    confirmButtonText: 'Guardar',
                    denyButtonText: 'Cancelar',
                    }).then((result) => {
                        
                        if (result.isConfirmed) {

                            var formData = new FormData();
                            
                            for (let i = 0; i < this.tarifas.length; i++) {
                                
                                formData.append('ubicaciones[]', this.tarifas[i][0]); 
                                formData.append('rangos[]', this.tarifas[i][2]); 
                                formData.append('valor[]', this.valor[i]);
                                formData.append('subsidio[]', this.subsidio[i]);
                                formData.append('contribucion[]', this.contribucion[i]); 
                            }

                            formData.append("servicio", this.servicio);
                            formData.append("clase", this.idClase);

                            this.loading = true;
                            
                            axios.post('servicios_publicos/tarifa_consumo/serv-tarifaConsumo.php?action=guardar', formData)
                            .then((response) => {
                                console.log(response.data); 
                                if(response.data.insertaBien) {
                                    
                                    this.loading = false;
                                    Swal.fire({
                                        icon: 'success',
                                        title: 'Se ha guardado con exito',
                                        showConfirmButton: false,
                                        timer: 1500
                                        }).then((response) => {
                                            app.redireccionar();
                                        });
                                }
                                else {
                                    this.loading = false;
                                    Swal.fire(
                                        'Error!',
                                        'No se pudo guardar.',
                                        'error'
                                    );
                                }
                            });
                        }
                        else if (result.isDenied) 
                        {
                            Swal.fire('Guardar cancelado', '', 'info');
                        }
                    });
            }
            else {
                Swal.fire("Error", "Faltan datos por llenar", "warning")
            }
        },

        redireccionar: function(){
            location.reload();  
        }
    }
});