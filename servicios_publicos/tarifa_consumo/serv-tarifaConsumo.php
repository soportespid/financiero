<?php

use Illuminate\Support\Arr;

    require_once '../../comun.inc';
    require '../../funciones.inc';
    require '../../funcionesSP.inc.php';
    session_start();

    $linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");

    $out = array('error' => false);

    $action = "show";

    if(isset($_GET['action'])){
        $action = $_GET['action'];
    }
    
    if ($action == 'tarifas') {
        
        $servicioId = $_GET['servicio'];
        $claseId = $_GET['clase'];
        $tarifas = array();

        $sqlUbicacion = "SELECT id_ubicacion, nombre_ubicacion FROM srv_ubicacion WHERE estado = 'S' ORDER BY id_ubicacion";
        $resUbicacion = mysqli_query($linkbd, $sqlUbicacion);
        while ($rowUbicacion = mysqli_fetch_row($resUbicacion)) {
            
            //Recorre 3 veces

            $sqlRangoConsumo = "SELECT id, consumo FROM srv_rango_consumo ORDER BY id";
            $resRangoConsumo = mysqli_query($linkbd, $sqlRangoConsumo);
            while ($rowRangoConsumo = mysqli_fetch_row($resRangoConsumo)) {
                
                $datos = array();

                // Recorre 3 veces
                $sqlTarifaConsumo = "SELECT valor, subsidio, contribucion FROM srv_tarifa_consumo WHERE id_servicio = $servicioId AND id_ubicacion = $rowUbicacion[0] AND id_clase = $claseId AND id_rango = $rowRangoConsumo[0]";
                $rowTarifaConsumo = mysqli_fetch_row(mysqli_query($linkbd, $sqlTarifaConsumo));

                if ($rowTarifaConsumo == null) {
                    $valor = 0;
                    $subsidio = 0;
                    $contribucion = 0;
                }
                else {
                    $valor = $rowTarifaConsumo[0];
                    $subsidio = $rowTarifaConsumo[1];
                    $contribucion = $rowTarifaConsumo[2];
                }

                array_push($datos, $rowUbicacion[0]);
                array_push($datos, $rowUbicacion[1]);
                array_push($datos, $rowRangoConsumo[0]);
                array_push($datos, $rowRangoConsumo[1]);
                array_push($datos, $valor);
                array_push($datos, $subsidio);
                array_push($datos, $contribucion);
                array_push($tarifas, $datos);
            }
        }

        $out['tarifas'] = $tarifas;
    }

    if ($action == 'guardar') {

        $servicioId = $_POST['servicio'];
        $claseId = $_POST['clase'];


        //Vacia la anteriores tarifas de ese servicio y clase

        $eliminarTarifas = "DELETE FROM srv_tarifa_consumo WHERE id_servicio = $servicioId AND id_clase = $claseId";
        mysqli_query($linkbd, $eliminarTarifas);

        for ($i=0; $i < count($_POST['valor']); $i++) { 
            
            $ubicacionId = 0;
            $rangoId = 0;
            $valor = 0;
            $subsidio = 0;
            $contribucion = 0;

            $ubicacionId = $_POST['ubicaciones'][$i];
            $rangoId = $_POST['rangos'][$i];
            $valor = $_POST['valor'][$i];
            $subsidio = $_POST['subsidio'][$i];
            $contribucion = $_POST['contribucion'][$i];

            $id = selconsecutivo('srv_tarifa_consumo', 'id');

            $sqlTarifaConsumo = "INSERT INTO srv_tarifa_consumo VALUES ($id, $servicioId, $ubicacionId, $claseId, $rangoId, $valor, $subsidio, $contribucion)";
            mysqli_query($linkbd, $sqlTarifaConsumo);
        }

        $out['insertaBien'] = true;
    }

    header("Content-type: application/json");
    echo json_encode($out);
    die();