<?php
    require_once '../../../Librerias/core/Helpers.php';
    require_once '../models/rehacerLiquidacion_model.php';
    session_start();
    header('Content-Type: application/json');

    class rehacerController extends rehacerModel {
        /* crear */
        public function get() {
            if (!empty($_SESSION)) {
                $arrResponse = array("dataUsuarios" => $this->usuarios(), "novedades" => $this->novedades());
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
            die();
        }

        public function getSearch() {
            if (!empty($_SESSION)) {
                $arrResponse = array("data" => $this->getDataSearch());
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
            die();
        }

        public function getVariables() {
            if (!empty($_SESSION)) {
                $usuarioId = $_POST["usuarioId"];
                $arrResponse = array("lectura" => $this->getLectura($usuarioId), "novedad" => $this->getNovedad($usuarioId));
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
            die();
        }

        public function save() {
            if (!empty($_SESSION)) {
                $data = [
                    "usuarioId" => $_POST["usuarioId"],
                    "codUsuario" => $_POST["codUsuario"],
                    "nombre" => $_POST["nombre"],
                    "documento" => $_POST["documento"],
                    "lectura" => (int) $_POST["lectura"],
                    "consumo" => (int) $_POST["consumo"],
                    "novedad" => (string) $_POST["novedad"],
                    "factura" => $_POST["factura"],
                    "fecha" => (string) $_POST["fecha"],
                    "motivo" => (string) $_POST["motivo"]
                ];
                $this->anulaFactura($data["factura"]);
                $this->insertLecturas($data["usuarioId"], $data["lectura"], $data["consumo"]);
                $this->insertNovedad($data["usuarioId"], $data["novedad"]);
                $newFactura = $this->insertFactura($data);
                $this->insertCorteDet($data["usuarioId"], $newFactura);
                $result = $this->insertReliquidacion($data["fecha"], $data["motivo"], $data["usuarioId"], $data["factura"], $newFactura);
                if ($result) {
                    $arrResponse = array("status" => true, "msg" => "Proceso de reliquidación realizado con exito, la nueva factura es $newFactura");
                }
                else {
                    $arrResponse = array("status" => false, "msg" => "Error en guardar, vuelva intentar");
                }
                
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
            die();
        }
        /* buscar */


        /* editar */
    }

    if($_POST){
        $obj = new rehacerController();
        $accion = $_POST["action"];
        if ($accion == "get") {
            $obj->get();
        } else if ($accion == "searchDataUsu") {
            $obj->getVariables();
        } else if ($accion == "save") {
            $obj->save();
        } else if ($accion == "getSearch") {
            $obj->getSearch();
        }
    }
?>