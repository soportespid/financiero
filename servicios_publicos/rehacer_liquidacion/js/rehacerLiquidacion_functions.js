const URL = "servicios_publicos/rehacer_liquidacion/controllers/rehacerLiquidacion_controller.php";

var app = new Vue({
    el:"#myapp",
    data() {
        return {
            isLoading: false,
            txtSearch: '',
            dataSearch_copy: [],
            dataSearch: [],
            /* form */
            fecha: '',
            usuarioId: '',
            codUsuario: '',
            documento: '',
            nombre: '',
            estrato: '',
            motivo: '',
            lectura: '',
            consumo: '',
            novedad: '',
            factura: '',

            /* usuarios */
            modalUsu: false,
            usuarios: [],
            usuariosCopy: [],

            /* novedades */
            modalNovedades: false,
            novedades: [],
            novedadesCopy: [],
        }
    },
    mounted() {
        this.intPageVal = this.$refs.pageType.value;

        if (this.intPageVal == 1) {
            this.get();
        } else if (this.intPageVal == 2) {
            this.getSearch();
        }
    },
    methods: {
        /* metodos para traer información */
        async get() {
            const formData = new FormData();
            formData.append("action","get");
            this.isLoading = true;
            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();
            this.usuarios = this.usuariosCopy = objData.dataUsuarios;
            this.novedades = this.novedadesCopy = objData.novedades;
            this.isLoading = false;
        },

        async getSearch() {
            const formData = new FormData();
            formData.append("action","getSearch");
            // this.isLoading = true;
            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();
            this.dataSearch_copy = this.dataSearch = objData.data;
        },

        /* metodos para procesar información */
        async searchData(option) {
            let search = "";

            switch (option) {
                case "modalUsuario":
                    search = this.txtSearch.toLowerCase();
                    this.usuarios = [...this.usuariosCopy.filter(e=>e.cod_usuario.toLowerCase().includes(search) || e.cedulanit.toLowerCase().includes(search) || e.view_nombre.toLowerCase().includes(search))];
                    break;
                
                case "modalNovedad":
                    search = this.txtSearch.toLowerCase();
                    this.novedades = [...this.novedadesCopy.filter(e=>e.codigo.toLowerCase().includes(search) || e.nombre.toLowerCase().includes(search) )];
                    break;

                case "inputSearch":
                    search = this.txtSearch.toLowerCase();
                    this.dataSearch_copy = [...this.dataSearch.filter(e=>e.id.toLowerCase().includes(search) || e.cod_usuario.toLowerCase().includes(search) )];
                    break;

                default:
                    console.log("Error, opción no encontrada");
                    break;
            }    
        },

        async selectItem(option, item) {
            if (option == "usuario") {
                this.usuarioId = item.usuario_id;
                this.codUsuario = item.cod_usuario;
                this.documento = item.cedulanit;
                this.nombre = item.view_nombre;
                this.estrato = item.nombre_estrato;
                this.factura = item.numero_facturacion;
                const formData = new FormData();
                formData.append("action","searchDataUsu");
                formData.append("usuarioId",this.usuarioId);
                const response = await fetch(URL,{method:"POST",body:formData});
                const objData = await response.json();
                this.lectura = objData.lectura.lectura;
                this.consumo = objData.lectura.consumo;
                this.novedad = objData.novedad.codigo_observacion;
                this.modalUsu = false;
                this.txtSearch = "";;
                this.usuarios = this.usuariosCopy;
            }
            else if (option == "novedad") {
                this.novedad = item.codigo;
                this.modalNovedades = false;
                this.txtSearch = "";
                this.novedades = this.novedadesCopy;
            }
        },
        
        // /* Metodos para guardar o actualizar información */
        async save() {
            if (this.fecha == "" || this.usuarioId == "" || this.motivo == "" || this.lectura == "" || this.consumo == "" || this.novedad == "") {
                Swal.fire("Atención!","Todos los campos con * son obligatorios","warning");
                return false;
            }

            const formData = new FormData();
            formData.append("action","save");
            formData.append("usuarioId",this.usuarioId);
            formData.append("codUsuario",this.codUsuario);
            formData.append("nombre",this.nombre);
            formData.append("documento",this.documento);
            formData.append("lectura",this.lectura);
            formData.append("consumo",this.consumo);
            formData.append("novedad",this.novedad);
            formData.append("factura",this.factura);
            formData.append("fecha",this.fecha);
            formData.append("motivo",this.motivo);
            Swal.fire({
                title:"¿Estás segur@ de guardar?",
                text:"",
                icon: 'warning',
                showCancelButton:true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText:"Sí, guardar",
                cancelButtonText:"No, cancelar"
            }).then(async function(result) {
                if(result.isConfirmed) {
                    const response = await fetch(URL,{method:"POST",body:formData});
                    const objData = await response.json();
                    
                    if (objData.status) {
                        Swal.fire("Guardado",objData.msg,"success");
                        setTimeout(function(){
                            window.location.href="serv-buscaReliquidaciones.php";
                        },1500);
                    }
                    else {
                        Swal.fire("Error!",objData.msg,"warning");
                    }
                }
            });
        },

        /* Metodos para mostrar información */
        formatFecha: function(fecha) {
            // Separar la fecha en componentes
            const parts = fecha.split('-');
            if (parts.length !== 3) {
                throw new Error('Formato de fecha inválido. Use Y-m-d.');
            }
            const year = parts[0];
            const month = parts[1];
            const day = parts[2];
        
            // Retornar la fecha en el formato deseado
            return `${day}/${month}/${year}`;
        },
    },
    computed:{

    }
})
