<?php
    require_once '../../../Librerias/core/Helpers.php';
    require_once '../../../Librerias/core/Mysql.php';
    session_start();
    
    class rehacerModel extends Mysql {
        private $cedula;
        private $vigencia;
        private $periodo;
        private $versionTarifa;

        function __construct(){
            parent::__construct();
            $this->cedula = $_SESSION["cedulausu"];
            $this->vigencia = getVigenciaUsuario($this->cedula);
            $this->periodo = $this->getPeriodo();
            $this->versionTarifa = $this->getVersionTarifa($this->periodo);
        }

        public function getDataSearch() {
            $sql = "SELECT SR.id, SR.fecha, SR.motivo, SR.factura, SR.new_factura, SC.cod_usuario FROM srv_reliquidacion AS SR INNER JOIN srvclientes AS SC ON SR.usuario_id = SC.id";
            $data = $this->select_all($sql);
            return $data;
        }

        public function getPeriodo() {
            $sql = "SELECT MAX(numero_corte) AS periodo FROM srvcortes WHERE estado = 'S'";
            $data = $this->select($sql);
            return $data["periodo"];
        }

        public function getVersionTarifa(int $numPeriodo) {
            $sql = "SELECT version_tarifa FROM srvcortes WHERE numero_corte = $numPeriodo";
            $data = $this->select($sql);
            return $data["version_tarifa"];
        }

        public function novedades() {
            $sql = "SELECT codigo_observacion AS codigo, descripcion AS nombre, afecta_facturacion AS afecta FROM srv_tipo_observacion WHERE estado = 'S'";
            $data = $this->select_all($sql);
            return $data;
        }

        public function usuarios() {
            $sql = "SELECT 
            SCD.id_cliente AS usuario_id,
            SCD.numero_facturacion,
            SU.cod_usuario,
            CONCAT(T.nombre1, ' ', T.nombre2, ' ', T.apellido1, ' ', T.apellido2) AS nombre,
            T.razonsocial,
            T.persona,
            T.cedulanit,
            SE.descripcion AS nombre_estrato
            FROM srvcortes_detalle AS SCD
            INNER JOIN srvclientes AS SU
            ON SCD.id_cliente = SU.id
            INNER JOIN terceros AS T
            ON SU.id_tercero = T.id_tercero
            INNER JOIN srvestratos AS SE
            ON SU.id_estrato = SE.id
            WHERE SCD.id_corte = $this->periodo AND SCD.estado_pago = 'S'
            ORDER BY SCD.id";
            $data = $this->select_all($sql);
            foreach ($data as $key => $d) {

                $data[$key]["view_nombre"] = $d["razonsocial"] != "" ? $d["razonsocial"] : $d["nombre"];
                $data[$key]["view_nombre"] = preg_replace("/\s+/", " ", trim($data[$key]["view_nombre"]));
            }
            return $data;
        }

        public function getLectura(int $usuarioId) {
            $sql = "SELECT lectura_medidor AS lectura, consumo FROM srvlectura WHERE corte = $this->periodo AND id_cliente = $usuarioId AND id_servicio = 1";
            $data = $this->select($sql);
            $data["lectura"] = $data["lectura"] == "" ? 0 : $data["lectura"];
            $data["consumo"] = $data["consumo"] == "" ? 0 : $data["consumo"];
            return $data;
        }

        public function getNovedad(int $usuarioId) {
            $sql = "SELECT codigo_observacion FROM srv_asigna_novedades WHERE corte = $this->periodo AND id_cliente = $usuarioId AND estado = 'S'";
            $data = $this->select($sql);
            $data["codigo_observacion"] = $data["codigo_observacion"] == "" ? 0 : $data["codigo_observacion"];
            return $data;
        }

        public function anulaFactura(int $numFactura) {
            $sql = "UPDATE srvcortes_detalle SET estado_pago = ? WHERE numero_facturacion = $numFactura";
            $result = $this->update($sql, ['N']);
            return $result;
        }

        public function insertLecturas(int $usuarioId, int $lectura, int $consumo) {
            $sql_delete = "DELETE FROM srvlectura WHERE corte = $this->periodo AND id_cliente = $usuarioId";
            $this->delete($sql_delete);

            $sql = "INSERT INTO srvlectura (corte, id_cliente, id_servicio, lectura_medidor, consumo, estado) VALUES (?, ?, ?, ?, ?, ?)";
            $valueAc = [$this->periodo, $usuarioId, 1, $lectura, $consumo, "S"];
            $valueAlc = [$this->periodo, $usuarioId, 2, $lectura, $consumo, "S"];
            $result = $this->insert($sql, $valueAc);
            $this->insert($sql, $valueAlc);
            return $result;
        }

        public function insertNovedad(int $usuarioId, string $codNovedad) {
            //Elimina las novedades que hubieran sido guardadas
            $sql_delete = "DELETE FROM srv_asigna_novedades WHERE corte = $this->periodo AND id_cliente = $usuarioId";
            $this->delete($sql_delete);

            //guardar la nueva novedad asignada
            $sql = "INSERT INTO srv_asigna_novedades (corte, id_cliente, codigo_observacion, estado) VALUES (?, ?, ?, ?)";
            $values = [$this->periodo, $usuarioId, "$codNovedad", "S"];
            $result = $this->insert($sql, $values);

            //valida si la novedad afecta las lecturas
            $sql_novedad = "SELECT afecta_facturacion FROM srv_tipo_observacion WHERE codigo_observacion = '$codNovedad'";
            $novedad = $this->select($sql_novedad);
            if ($novedad["afecta_facturacion"] == "S") {
                $sql = "SELECT cambia_consumo, lectura_consumo AS consumo, id_servicio AS servicio_id FROM srv_observacion_det WHERE codigo_observacion = '$codNovedad' AND estado = 'S'";
                $detalleNovedad = $this->select_all($sql);

                foreach ($detalleNovedad as $detalle) {
                    if ($detalle["cambia_consumo"] == "S") {
                        $update_consumo = "UPDATE srvlectura SET consumo = ? WHERE corte = $this->periodo AND id_cliente = $usuarioId AND id_servicio = $detalle[servicio_id]";
                        $this->update($update_consumo, [$detalle["consumo"]]);
                    }
                }
            }
            return $result;
        }

        private function searchNovedadService(array $data, int $servicioId) {
            $info = [];    
            foreach ($data as $d) {
                if ($d["servicio_id"] == $servicioId) {
                    $info = $d;
                }
            }
            return $info;
        }   

        private function searchTarifasService(array $data, int $servicioId, int $estratoId) {
            $info = [];    
            foreach ($data as $d) {
                if ($d["servicio_id"] == $servicioId && $d["estrato_id"] == $estratoId) {
                    $info = $d;
                }
            }
            return $info;
        }

        private function searchTarifaVariable(array $data, int $servicioId, int $estratoId, int $rangoIni) {
            $info = [];
            // dep($data);
            foreach ($data as $d) {
                if ($d["servicio_id"] == $servicioId && $d["estrato_id"] == $estratoId && $d["rango_inicial"] == $rangoIni) {
                    $info = ["costo_unidad" => $d["costo_unidad"], "subsidio" => $d["subsidio"], "contribucion" => $d["contribucion"]];
                }
            }
            return $info;
        }

        public function insertFactura(array $data) {
            //Inicializa variables generales
            $usuarioId = $data["usuarioId"];
            $codUsuario = $data["codUsuario"];
            $nombre = $data["nombre"];
            $documento = $data["documento"];
            $codNovedad = $data["novedad"];
            $factura = $data["factura"]; 
            $newFactura = searchConsec("srvcortes_detalle", "numero_facturacion");

            /* llama tablas de datos requeridas para el proceso */

            //Tabla de los servicios y clase de uso del usuario
            $sql_servicios = "SELECT SAS.id_servicio AS servicio_id, 
            SAS.id_estrato AS estrato_id, 
            SAS.tarifa_medidor AS tarifa_variable, 
            SAS.cargo_fijo AS status_cargo_fijo, 
            SAS.consumo AS status_consumo,
            SL.consumo
            FROM srvasignacion_servicio AS SAS
            LEFT JOIN srvlectura AS SL
            ON SAS.id_servicio = SL.id_servicio AND SAS.id_clientes = SL.id_cliente AND SL.corte = $this->periodo
            WHERE SAS.id_clientes = $usuarioId AND SAS.estado = 'S' AND (SAS.cargo_fijo = 'S' || SAS.consumo = 'S')
            ORDER BY SAS.id_servicio";
            $servicios = $this->select_all($sql_servicios);

            //la novedad que tiene en el periodo el usuario
            $sql_novedad = "SELECT afecta_facturacion FROM srv_tipo_observacion WHERE codigo_observacion = '$codNovedad' AND estado = 'S'";
            $novedad = $this->select($sql_novedad);

            if ($novedad["afecta_facturacion"] == "S") {
                $sql_novedad_det = "SELECT id_servicio AS servicio_id, cargo_fijo, consumo FROM srv_observacion_det WHERE codigo_observacion = '$codNovedad'";
                $novedadDet = $this->select_all($sql_novedad_det);
            }

            //tabla de cargo fijo
            $sql_cargo_fijo = "SELECT id_servicio AS servicio_id,
            id_estrato AS estrato_id,
            costo_unidad,
            subsidio,
            contribucion
            FROM srvcargo_fijo
            WHERE version = $this->versionTarifa";
            $cargoFijo = $this->select_all($sql_cargo_fijo);

            //tabla consumo variable
            $sql_tarifas_variables = "SELECT id_servicio AS servicio_id,
            id_estrato AS estrato_id,
            rango_inicial,
            rango_final,
            costo_unidad,
            subsidio, 
            contribucion
            FROM srvtarifas
            WHERE version = $this->versionTarifa";
            $tarifasVariables = $this->select_all($sql_tarifas_variables);
            
            //consumo estandar
            $sql_tarifas_estandar = "SELECT id_servicio AS servicio_id,
            id_estrato AS estrato_id,
            costo_unidad,
            subsidio,
            contribucion
            FROM srvcostos_estandar
            WHERE version = $this->versionTarifa";
            $tarifaEstandar = $this->select_all($sql_tarifas_estandar);

            $sql_innsert_factura = "INSERT INTO srvfacturas (corte, cliente, cod_usuario, num_factura, nombre_suscriptor, documento, id_servicio, cargo_f, consumo_b, consumo_c, consumo_s, subsidio_cf, subsidio_cb, subsidio_cc, subsidio_cs, contribucion_cf, contribucion_cb, contribucion_cc, contribucion_cs, deuda_anterior, acuerdo_pago, venta_medidor, interes_mora, estado, desincentivo) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
            $sql_insert_det = "INSERT INTO srvdetalles_facturacion (corte, id_cliente, numero_facturacion, tipo_movimiento, id_servicio, id_tipo_cobro, credito, debito, estado) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";

            foreach ($servicios as $servicio) {
                $novedadCF = "S";
                $novedadCS = "S";
                if ($novedad["afecta_facturacion"] == "S") {
                    $getNovedad = $this->searchNovedadService($novedadDet, $servicio["servicio_id"]);
                    $novedadCF = $getNovedad["cargo_fijo"];
                    $novedadCS = $getNovedad["consumo"];
                } 

                $dataFactura = [
                    "servicioId" => $servicio["servicio_id"],
                    "cargo_f" => 0,
                    "consumo_b" => 0,
                    "consumo_c" => 0,
                    "consumo_s" => 0,
                    "subsidio_cf" => 0,
                    "subsidio_cb" => 0,
                    "subsidio_cc" => 0,
                    "subsidio_cs" => 0,
                    "contribucion_cf" => 0,
                    "contribucion_cb" => 0,
                    "contribucion_cc" => 0,
                    "contribucion_cs" => 0,
                    "deuda_anterior" => 0,
                    "acuerdo_pago" => 0,
                    "venta_medidor" => 0,
                    "interes_mora" => 0,
                    "estado" => "ACTIVO",
                    "desincentivo" => 0
                ];

                /* cargoFijo */
                if ($servicio["status_cargo_fijo"] == "S") {
                    if ($novedadCF == "S") {
                        $getCargoFijo = $this->searchTarifasService($cargoFijo, $servicio["servicio_id"], $servicio["estrato_id"]);
                        $dataFactura["cargo_f"] = $getCargoFijo["costo_unidad"];
                        if ($getCargoFijo["subsidio"] > 0) {
                            $subsidio_cf = $getCargoFijo["subsidio"] / 100;
                            $dataFactura["subsidio_cf"] = round(($getCargoFijo["costo_unidad"] * $subsidio_cf), 2, PHP_ROUND_HALF_DOWN);
                        } else if ($getCargoFijo["contribucion"] > 0) {
                            $contribucion_cf = $getCargoFijo["contribucion"] / 100;
                            $dataFactura["contribucion_cf"] = round(($getCargoFijo["costo_unidad"] * $contribucion_cf), 2, PHP_ROUND_HALF_DOWN);
                        }
                    }
                }
                
                /* consumo */
                if ($servicio["status_consumo"] == "S") {
                    if ($novedadCS == "S") {
                        if ($servicio["tarifa_variable"] == "S") {
                            $rangos = [
                                "rango1" => ["min" => 0, "max" => 16, "consumo" => 0, "costoUnidad" => 0, "subsidio" => 0, "contribucion" => 0],
                                "rango2" => ["min" => 17, "max" => 32, "consumo" => 0, "costoUnidad" => 0, "subsidio" => 0, "contribucion" => 0],
                                "rango3" => ["min" => 33, "max" => 9999, "consumo" => 0, "costoUnidad" => 0, "subsidio" => 0, "contribucion" => 0],
                            ];
                            $consumo = $servicio["consumo"];
                            // echo "consumo: $consumo \n";
                            foreach ($rangos as $key => $rango) {
                                // Calculamos cuánto consumo entra en el rango actual
                                
                                if ($consumo > $rango["max"]) {
                                    $getTarifaVariable = $this->searchTarifaVariable($tarifasVariables, $servicio["servicio_id"], $servicio["estrato_id"], $rango["min"]);
                                    $rangos[$key]["consumo"] = $rango["min"] == 0 ? ($rango["max"] - $rango["min"]) : ($rango["max"] - $rango["min"] + 1);
                                    $rangos[$key]["costoUnidad"] = $getTarifaVariable["costo_unidad"];
                                    $rangos[$key]["subsidio"] = $getTarifaVariable["subsidio"];
                                    $rangos[$key]["contribucion"] = $getTarifaVariable["contribucion"];
                                    
                                } elseif ($consumo >= $rango["min"]) {
                                    $getTarifaVariable = $this->searchTarifaVariable($tarifasVariables, $servicio["servicio_id"], $servicio["estrato_id"], $rango["min"]);
                                    $rangos[$key]["consumo"] = $rango["min"] == 0 ? ($consumo - $rango["min"]) : ($consumo - $rango["min"] + 1);
                                    $rangos[$key]["costoUnidad"] = $getTarifaVariable["costo_unidad"];
                                    $rangos[$key]["subsidio"] = $getTarifaVariable["subsidio"];
                                    $rangos[$key]["contribucion"] = $getTarifaVariable["contribucion"];
                                }
                                $consumo -= $rango["consumo"];  
                                if ($consumo <= 0) break; // Detenemos si ya no hay consumo restante
                            }

                            $dataFactura["consumo_b"] = round($rangos["rango1"]["consumo"] * $rangos["rango1"]["costoUnidad"],2, PHP_ROUND_HALF_DOWN);
                            $dataFactura["consumo_c"] = round($rangos["rango2"]["consumo"] * $rangos["rango2"]["costoUnidad"],2, PHP_ROUND_HALF_DOWN);
                            $dataFactura["consumo_s"] = round($rangos["rango3"]["consumo"] * $rangos["rango3"]["costoUnidad"],2, PHP_ROUND_HALF_DOWN);
                            $dataFactura["subsidio_cb"] = round($dataFactura["consumo_b"] * ($rangos["rango1"]["subsidio"] / 100), PHP_ROUND_HALF_DOWN);
                            $dataFactura["subsidio_cc"] = round($dataFactura["consumo_c"] * ($rangos["rango2"]["subsidio"] / 100), PHP_ROUND_HALF_DOWN);
                            $dataFactura["subsidio_cs"] = round($dataFactura["consumo_s"] * ($rangos["rango3"]["subsidio"] / 100), PHP_ROUND_HALF_DOWN);
                            $dataFactura["contribucion_cb"] = round($dataFactura["consumo_b"] * ($rangos["rango1"]["contribucion"] / 100), PHP_ROUND_HALF_DOWN);
                            $dataFactura["contribucion_cc"] = round($dataFactura["consumo_c"] * ($rangos["rango2"]["contribucion"] / 100), PHP_ROUND_HALF_DOWN);
                            $dataFactura["contribucion_cs"] = round($dataFactura["consumo_s"] * ($rangos["rango3"]["contribucion"] / 100), PHP_ROUND_HALF_DOWN);
                        }
                        else if ($servicio["tarifa_variable"] == "N") {
                            $getConsumoEstandar = $this->searchTarifasService($tarifaEstandar, $servicio["servicio_id"], $servicio["estrato_id"]);
                            $dataFactura["consumo_b"] = $getConsumoEstandar["costo_unidad"];
                            if ($getConsumoEstandar["subsidio"] > 0) {
                                $subsidio_cb = $getConsumoEstandar["subsidio"] / 100;
                                $dataFactura["subsidio_cb"] = round(($getConsumoEstandar["costo_unidad"] * $subsidio_cb), 2, PHP_ROUND_HALF_DOWN);
                            } else if ($getConsumoEstandar["contribucion"] > 0) {
                                $contribucion_cb = $getConsumoEstandar["contribucion"] / 100;
                                $dataFactura["contribucion_cb"] = round(($getConsumoEstandar["costo_unidad"] * $contribucion_cb), 2, PHP_ROUND_HALF_DOWN);
                            }
                        }
                    }
                }

                $sql_data_ant = "SELECT deuda_anterior, acuerdo_pago, venta_medidor, interes_mora, desincentivo FROM srvfacturas WHERE num_factura = $factura AND id_servicio = $servicio[servicio_id]";
                $data_ant = $this->select($sql_data_ant);
                $dataFactura["deuda_anterior"] = $data_ant["deuda_anterior"];
                $dataFactura["interes_mora"] = $data_ant["interes_mora"];
                $dataFactura["acuerdo_pago"] = $data_ant["acuerdo_pago"];
                $dataFactura["venta_medidor"] = $data_ant["venta_medidor"];
                $dataFactura["desincentivo"] = $data_ant["desincentivo"];

                $valueCS = $dataFactura["consumo_b"] + $dataFactura["consumo_c"] + $dataFactura["consumo_s"];
                $subsidioCS = $dataFactura["subsidio_cb"] + $dataFactura["subsidio_cc"] + $dataFactura["subsidio_cs"];
                $contribucion = $dataFactura["contribucion_cf"] + $dataFactura["contribucion_cb"] + $dataFactura["contribucion_cc"] + $dataFactura["contribucion_cs"];

                $valueDetTipo01 = [$this->periodo, $usuarioId, $newFactura, "101", $servicio["servicio_id"], 1, $dataFactura["cargo_f"], 0, "S"];
                $valueDetTipo02 = [$this->periodo, $usuarioId, $newFactura, "101", $servicio["servicio_id"], 2, $valueCS, 0, "S"];
                $valueDetTipo03 = [$this->periodo, $usuarioId, $newFactura, "101", $servicio["servicio_id"], 3, 0, $dataFactura["subsidio_cf"], "S"];
                $valueDetTipo04 = [$this->periodo, $usuarioId, $newFactura, "101", $servicio["servicio_id"], 4, 0, $subsidioCS, "S"];
                $valueDetTipo05 = [$this->periodo, $usuarioId, $newFactura, "101", $servicio["servicio_id"], 5, $contribucion, 0, "S"];
                $valueDetTipo07 = [$this->periodo, $usuarioId, $newFactura, "101", $servicio["servicio_id"], 7, $dataFactura["acuerdo_pago"], 0, "S"];
                $valueDetTipo08 = [$this->periodo, $usuarioId, $newFactura, "101", $servicio["servicio_id"], 8, $dataFactura["deuda_anterior"], 0, "S"];
                $valueDetTipo09 = [$this->periodo, $usuarioId, $newFactura, "101", $servicio["servicio_id"], 9, $dataFactura["venta_medidor"], 0, "S"];
                $valueDetTipo10 = [$this->periodo, $usuarioId, $newFactura, "101", $servicio["servicio_id"], 10, $dataFactura["interes_mora"], 0, "S"];
                $valueDetTipo12 = [$this->periodo, $usuarioId, $newFactura, "101", $servicio["servicio_id"], 12, $dataFactura["desincentivo"], 0, "S"];

                $values = [
                    $this->periodo,
                    $usuarioId,
                    $codUsuario,
                    $newFactura,
                    $nombre,
                    $documento,
                    $servicio["servicio_id"],
                    $dataFactura["cargo_f"],
                    $dataFactura["consumo_b"],
                    $dataFactura["consumo_c"],
                    $dataFactura["consumo_s"],
                    $dataFactura["subsidio_cf"],
                    $dataFactura["subsidio_cb"],
                    $dataFactura["subsidio_cc"],
                    $dataFactura["subsidio_cs"],
                    $dataFactura["contribucion_cf"],
                    $dataFactura["contribucion_cb"],
                    $dataFactura["contribucion_cc"],
                    $dataFactura["contribucion_cs"],
                    $dataFactura["deuda_anterior"],
                    $dataFactura["acuerdo_pago"],
                    $dataFactura["venta_medidor"],
                    $dataFactura["interes_mora"],
                    $dataFactura["estado"],
                    $dataFactura["desincentivo"]
                ];

                $this->insert($sql_innsert_factura, $values);
                $this->insert($sql_insert_det, $valueDetTipo01);
                $this->insert($sql_insert_det, $valueDetTipo02);
                $this->insert($sql_insert_det, $valueDetTipo03);
                $this->insert($sql_insert_det, $valueDetTipo04);
                $this->insert($sql_insert_det, $valueDetTipo05);
                $this->insert($sql_insert_det, $valueDetTipo07);
                $this->insert($sql_insert_det, $valueDetTipo08);
                $this->insert($sql_insert_det, $valueDetTipo09);
                $this->insert($sql_insert_det, $valueDetTipo10);
                $this->insert($sql_insert_det, $valueDetTipo12);
            }

            return $newFactura;
        }

        public function insertCorteDet($usuarioId, $numFactura) {
            $sql = "INSERT INTO srvcortes_detalle (id_cliente, id_corte, numero_facturacion, estado_pago) VALUES (?, ?, ?, ?)";
            $values = [$usuarioId, $this->periodo, $numFactura, "S"];
            $result = $this->insert($sql, $values);
            return $result;
        }

        public function insertReliquidacion(string $fecha, string $motivo, int $usuarioId, int $factura, int $newFactura) {
            $sql = "INSERT INTO srv_reliquidacion (fecha, motivo, usuario_id, factura, new_factura) VALUES (?, ?, ?, ?, ?)";
            $value = [$fecha, $motivo, $usuarioId, $factura, $newFactura];
            $result = $this->insert($sql, $value);

            $sql_funcion = "SELECT id FROM srv_funciones WHERE nombre = 'Reliquidacion'";
            $funcion = $this->select($sql_funcion);
            insertAuditoria("srv_auditoria","id_srv_funciones",$funcion["id"],"Crear",$result);

            return $result;
        }
    }
?>