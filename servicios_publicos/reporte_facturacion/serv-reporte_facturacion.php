<?php
    require_once '../../comun.inc';
    require '../../funciones.inc';
    require '../../funcionesSP.inc.php';
    session_start();

    $linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");

    $out = array('error' => false);

    $action = "show";

    if(isset($_GET['action'])){
        $action = $_GET['action'];
    }
    
    if ($action == "show") {
       
    }

    if ($action == "generarReporte") {
        
        $reporte = array();
        $cortes = array();
        $totales = array();

        $periodoInicial = $_GET['periodoInicial'];
        $periodoFinal = $_GET['periodoFinal'];

        $sqlPeriodos = "SELECT numero_corte FROM srvcortes WHERE numero_corte >= $periodoInicial AND numero_corte <= $periodoFinal";
        //echo $sqlPeriodos;
        $resPeriodos = mysqli_query($linkbd, $sqlPeriodos);
        while ($rowPeriodos = mysqli_fetch_row($resPeriodos)) {

            $total2 = 0;
            $datos2 = array();
            $datosCortes = array();

            $sqlServicios = "SELECT id, nombre FROM srvservicios WHERE estado = 'S'";
            $resServicios = mysqli_query($linkbd, $sqlServicios);
            while ($rowServicios = mysqli_fetch_row($resServicios)) {

                $datos = array();

                $sqlDetalles = "SELECT SUM(cargo_f), SUM(subsidio_cf), SUM(consumo_b+consumo_c+consumo_s), SUM(subsidio_cb+subsidio_cc+subsidio_cs), SUM(contribucion_cf+contribucion_cb+contribucion_cc+contribucion_cc+contribucion_cs) FROM srvfacturas WHERE corte = $rowPeriodos[0] AND id_servicio = $rowServicios[0]";
                $resDetalles = mysqli_query($linkbd, $sqlDetalles);
                $rowDetalles = mysqli_fetch_row($resDetalles);

                $total = 0;
                $total = $rowDetalles[0] - $rowDetalles[1] + $rowDetalles[2] - $rowDetalles[3] + $rowDetalles[4];
                $total2 += $total;

                array_push($datos, $rowPeriodos[0]);
                array_push($datos, $rowServicios[1]);
                array_push($datos, round($rowDetalles[0],2));
                array_push($datos, round($rowDetalles[1],2));
                array_push($datos, round($rowDetalles[2],2));
                array_push($datos, round($rowDetalles[3],2));
                array_push($datos, round($rowDetalles[4],2));
                array_push($datos, round($total,2));
                array_push($reporte, $datos);
            }

            array_push($datos2, $rowPeriodos[0]);
            array_push($datos2, $total2);
            array_push($totales, $datos2);

            
            array_push($datosCortes, $rowPeriodos[0]);
            array_push($cortes, $datosCortes);
        }

        $out['reporte'] = $reporte;
        $out['cortes'] = $cortes;
        $out['totales'] = $totales;
    }

    header("Content-type: application/json");
    echo json_encode($out);
    die();