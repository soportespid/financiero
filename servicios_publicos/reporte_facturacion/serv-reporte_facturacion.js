var app = new Vue({
    el: '#myapp',
	data:{

        error: '',
        loading: false,
        showModalLiquidaciones: false,
        periodoInicial: '',
        periodoFinal: '',
        descripcionPeriodoInicial: '',
        descripcionPeriodoFinal: '',
        datosPeriodosLiquidados: [],
        datosUsuarios: [],
        validacionPeriodo: '',
        informaciones: [],
        cortes: [],
        totales: [],
    },

    mounted: function(){

      
    },

    methods: 
    {
        periodosLiquidados: async function(validacion) {

            this.loading = true;

            await axios.post('servicios_publicos/reporte_cartera/serv-reporte_cartera.php')
            .then((response) => {
                
                app.datosPeriodosLiquidados = response.data.periodosLiquidados;
            }).catch((error) => {
                this.error = true;
                console.log(error)
            }).finally(() => {
                this.loading =  false;
                this.showModalLiquidaciones = true;
                this.validacionPeriodo = validacion;
            });   
        },

        seleccionaPeriodo: function(periodo) {

            if (this.validacionPeriodo == '1') {

                this.periodoInicial = periodo[0];
                this.descripcionPeriodoInicial   = periodo[1];
                this.showModalLiquidaciones = false;
            }

            if (this.validacionPeriodo == '2') {

                this.periodoFinal = periodo[0];
                this.descripcionPeriodoFinal = periodo[1];
                this.showModalLiquidaciones = false;
            }
            
        },

        generarInforme: async function() {

            if (this.periodoInicial != '' && this.periodoFinal != '') {

                if (parseInt(this.periodoInicial) <= parseInt(this.periodoFinal)) {

                    this.loading = true;

                    await axios.post('servicios_publicos/reporte_facturacion/serv-reporte_facturacion.php?action=generarReporte&periodoInicial='+this.periodoInicial+"&periodoFinal="+this.periodoFinal)
                    .then((response) => {
                        
                        app.informaciones = response.data.reporte;
                        app.cortes = response.data.cortes;
                        app.totales = response.data.totales;
                        console.log(response.data);
                    }).catch((error) => {
                        this.error = true;
                        console.log(error)
                    }).finally(() => {
                        this.loading =  false;
                    });   
                }
                else {
                    Swal.fire("Error", "Inconsistencia en la selección de periodos", "error");
                }
            }
            else {
                Swal.fire("Incompleto", "Periodo inicial y final", "warning");
            }
        },

        formatonumero: function(valor){
            return new Intl.NumberFormat("en-US", {style: "currency", currency: "USD"}).format(valor);
        },

        excel: function() {

            if (this.informaciones != '') {

                document.form2.action="serv-excelReporteFacturacion.php";
				document.form2.target="_BLANK";
				document.form2.submit(); 
            }
            else {
                Swal.fire("Faltan datos", "Genera el informe primero", "warning");
            }
        },
    }
});