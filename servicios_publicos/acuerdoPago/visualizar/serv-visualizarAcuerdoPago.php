<?php
    // require '../../../funcionesSP.inc.php';
    require_once '../../../Librerias/core/Helpers.php';
    require_once '../../../Librerias/core/Mysql.php';

    header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
    session_start();

    $out = array('error' => false);

    class searchAcuerdo extends Mysql {
        
        public function visualizarData(){
            if(!empty($_SESSION)){
                $codAcuerdo = $_POST["consecutivo"];
                $data = [];
                $valueFactura = $valorPendiente = 0;
                $nameBanco = "";

                $sqlAcuerdoCab = "SELECT * FROM srv_acuerdo_cab WHERE codigo_acuerdo = $codAcuerdo";
                $acuerdo = $this->select($sqlAcuerdoCab);      

                if ($acuerdo["medio_pago"] == "banco") {
                    $sqlBanco = "SELECT B.cuenta AS cuentaContable, B.ncuentaban AS cuentaBancaria, B.tipo AS tipo, T.razonsocial AS nombre FROM tesobancosctas B JOIN terceros T ON B.tercero = T.cedulanit WHERE B.cuenta = $acuerdo[numero_cuenta]";
                    $banco = $this->select($sqlBanco);

                    $nameBanco = $banco["cuentaBancaria"] . " " . $banco["tipo"] . " " . $banco["nombre"];
                }

                $valorPendiente = $acuerdo["valor_acuerdo"] - $acuerdo["valor_liquidado"];

                $sqlFactura = "SELECT SUM(credito) AS valueCredito, SUM(debito) AS valueDebito FROM srvdetalles_facturacion WHERE numero_facturacion = $acuerdo[codigo_factura] AND tipo_movimiento = '101'";
                $factura = $this->select($sqlFactura);

                $valueFactura = $factura["valueCredito"] - $factura["valueDebito"];
                $valueFactura = ($valueFactura / 100);
                $valueFactura = ceil($valueFactura);
                $valueFactura = $valueFactura * 100;
                
                $data["consecutivo"] = $acuerdo["codigo_acuerdo"];
                $data["fecha"] = $acuerdo["fecha_acuerdo"];
                $data["numFactura"] = $acuerdo["codigo_factura"];
                $data["codUsuario"] = $acuerdo["cod_usuario"];
                $data["documento"] = $acuerdo["documento"];
                $data["name"] = $acuerdo["nombre_suscriptor"];
                $data["valorFactura"] = number_format($valueFactura, 2);
                $data["porceDescuento"] = $acuerdo["descuento_intereses"]."%";
                $data["valorFacturaAcuerdo"] = number_format($acuerdo["valor_factura"], 2);
                $data["valorAbono"] = number_format($acuerdo["valor_abonado"], 2);
                $data["valorAcuerdo"] = number_format($acuerdo["valor_acuerdo"], 2);
                $data["numCuotas"] = $acuerdo["cuotas_liquidadas"] . "/" . $acuerdo["numero_cuotas"];
                $data["valorCuota"] = number_format($acuerdo["valor_cuota"], 2);
                $data["valorPendiente"] = number_format($valorPendiente, 2);
                $data["medioPago"] = $acuerdo["medio_pago"];
                $data["banco"] = $nameBanco;
                $data["estadoAcuerdo"] = $acuerdo["estado_acuerdo"];

                // dep($data);
                echo json_encode($data,JSON_UNESCAPED_UNICODE);
            }
            die();
        }
    }

    if($_POST){
        $obj = new searchAcuerdo();
        if($_POST['action'] == "getData"){
            $obj->visualizarData();
        }
    }
    
    // dep($obj->select_all('SELECT * FROM srv_acuerdo_cab ORDER BY codigo_acuerdo DESC'));

    header("Content-type: application/json");
    echo json_encode($out);
    die();