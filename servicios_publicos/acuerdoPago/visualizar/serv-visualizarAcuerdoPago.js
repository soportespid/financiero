const URL = 'servicios_publicos/acuerdoPago/visualizar/serv-visualizarAcuerdoPago.php';
const URLPDF = 'serv-visualizarAcuerdoPagoPdf.php';

var app = new Vue({
    el:"#myapp",
    data() {
        return {
            isLoading:false,
            id: 0,
            data: [],
        }
    },
    mounted() {
        this.getData();
    },
    methods: {
        getData: async function(){
            this.id = new URLSearchParams(window.location.search).get('id');

            if (this.id > 0) {
                const formData = new FormData();
                formData.append("action","getData");
                formData.append("consecutivo",this.id);
                const response = await fetch(URL,{method:"POST",body:formData});
                const objData = await response.json();
    
                this.data = objData;
            }
        },

        pdf: function() {
            const form = document.createElement("form");
            form.method = "post";
            form.target = "_blank";
            form.action = URLPDF;       
            
            function addField(name,value){
                const input = document.createElement("input");
                input.type="hidden";
                input.name=name;
                input.value = value;
                form.appendChild(input);
            }
         
            addField("data", JSON.stringify(this.data));
        
            document.body.appendChild(form);
            form.submit();
            document.body.removeChild(form);
        },

        formatNumero: function(valor){
            return new Intl.NumberFormat("en-US", {style: "currency", currency: "USD"}).format(valor);      
        },
    },
    computed:{

    }
})
