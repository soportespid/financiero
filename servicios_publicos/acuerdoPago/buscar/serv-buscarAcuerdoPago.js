const URL = 'servicios_publicos/acuerdoPago/buscar/serv-buscarAcuerdoPago.php';
const URLPDF = 'serv-buscarAcuerdoPagoPdf.php';
const URLEXCEL = 'serv-buscarAcuerdoPagoExcel.php';

var app = new Vue({
    el:"#myapp",
    data() {
        return {
            isLoading:false,

            txtSearch: '',
            arrAcuerdos: [],
            arrAcuerdosCopy: [],
            txtResultados: 0,
        }
    },
    mounted() {
        this.getData();
    },
    methods: {
        getData: async function(){

            const formData = new FormData();
            formData.append("action","getData");
            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();

            this.arrAcuerdos = objData;
            this.arrAcuerdosCopy = objData;
            this.txtResultados = this.arrAcuerdosCopy.length;
        },

        searchData: async function() {
            let search = "";
            search = this.txtSearch.toLowerCase();
            
            this.arrAcuerdosCopy = [...this.arrAcuerdos.filter(e=>e.codAcuerdo.toLowerCase().includes(search) || e.nombre.toLowerCase().includes(search) || e.numFactura.toLowerCase().includes(search) || e.codUsuario.toLowerCase().includes(search) )];
            this.txtResultados = this.arrAcuerdosCopy.length;
        },

        downloadFormat: function(format) {
            const form = document.createElement("form");
            form.method = "post";
            form.target = "_blank";
            if (format == 'pdf') {
                form.action = URLPDF;
            }
            else if (format == 'excel') {
                form.action = URLEXCEL;
            }
            
            function addField(name,value){
                const input = document.createElement("input");
                input.type="hidden";
                input.name=name;
                input.value = value;
                form.appendChild(input);
            }
         
            addField("datos", JSON.stringify(this.arrAcuerdos));
        
            document.body.appendChild(form);
            form.submit();
            document.body.removeChild(form);
        },

        formatNumero: function(valor){
            return new Intl.NumberFormat("en-US", {style: "currency", currency: "USD"}).format(valor);      
        },
    },
    computed:{

    }
})
