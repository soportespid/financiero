<?php
    // require '../../../funcionesSP.inc.php';
    require_once '../../../Librerias/core/Helpers.php';
    require_once '../../../Librerias/core/Mysql.php';

    header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
    session_start();

    $out = array('error' => false);

    class searchAcuerdos extends Mysql {
        
        public function getData(){
            if(!empty($_SESSION)){
                $sql = "SELECT codigo_acuerdo AS codAcuerdo, fecha_acuerdo AS fecha, cod_usuario AS codUsuario, nombre_suscriptor AS nombre, codigo_factura AS numFactura, valor_factura AS valueFactura, valor_abonado AS valueAbono, numero_cuotas AS numCuotas, valor_cuota AS valueCuota, cuotas_liquidadas AS numCuotasFacturadas, estado_acuerdo, cuotas_liquidadas AS cuotasFacturadas FROM srv_acuerdo_cab ORDER BY codigo_acuerdo DESC";
                $request = $this->select_all($sql);      
                echo json_encode($request,JSON_UNESCAPED_UNICODE);
            }
            die();
        }
    }

    if($_POST){
        $obj = new searchAcuerdos();
        if($_POST['action'] == "getData"){
            $obj->getData();
        }
    }
    
    // dep($obj->select_all('SELECT * FROM srv_acuerdo_cab ORDER BY codigo_acuerdo DESC'));

    header("Content-type: application/json");
    echo json_encode($out);
    die();