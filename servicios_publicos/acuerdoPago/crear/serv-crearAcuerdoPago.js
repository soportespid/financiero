const URL = 'servicios_publicos/acuerdoPago/crear/serv-crearAcuerdoPago.php';
const URLPDF = 'serv-acuerdoPagoCobroPDF.php';

var app = new Vue({
    el:"#myapp",
    data() {
        return {
            isLoading:false,
            codAcuerdo: 0,
            fechaAcuerdo: '',
            numFactura: '',
            valorFactura: 0,
            valorFacturaAcuerdo: 0,
            valorIntereses: 0,
            descuentoIntereses: false,
            porcentajeDescuento: 0,
            valorAbono: 0,
            valorAcuerdo: 0,
            numeroCuotas: 0,
            valorCuota: 0,
            medioPago: '',
            bancos: [],
            banco: '',
            dataUsu: [],
            porcentajeMinimo: 0,
            tipoMov: '101',
            codAcuerdoRev: '',
            dataUsuRev: [],
            valorRev: 0,
            codAcuerdoFinalizar: '',
            dataUsuFinalizar: [],
        }
    },
    mounted() {
        this.getData();
    },
    methods: {
        getData: async function(){
            await axios.post(URL+'?action=getData').then((response) => {
                this.bancos = response.data.bancos;
                this.codAcuerdo = response.data.consecutivo;
                this.porcentajeMinimo = response.data.porcentajeMinimo;
            });
        },

        searchDataFactura: async function() {
            if (this.numFactura != "") {
                this.dataUsu = [];
                this.valorFactura = this.valorFacturaAcuerdo = this.valorAbono = this.valorAcuerdo = this.numeroCuotas = this.valorCuota = 0;
                this.descuentoIntereses = false;
                this.porcentajeDescuento = 0;
                const formData = new FormData();
                formData.append("numFactura",this.numFactura);
                await axios.post(URL+'?action=dataFactura', formData).then((response) => {
                    if (response.data.error == false) {
                        this.dataUsu = response.data.dataUsuario;
                        this.valorFactura = this.dataUsu.valorFactura;
                        this.valorFacturaAcuerdo = this.dataUsu.valorFacturaAcuerdo;
                        this.valorIntereses = this.dataUsu.valorIntereses;
                    }
                    else {
                        let msgError = response.data.msgError;
                        Swal.fire("Error",msgError,"error");        
                    }
                });
            }
            else {
                Swal.fire("Error","Debes colocar un número de factura","error");
            }
        },

        changeStateDiscount: function() {
            this.valorCuota = this.numeroCuotas = this.valorAcuerdo = this.valorAbono = 0;
            this.valorFacturaAcuerdo = this.dataUsu.valorFacturaAcuerdo;
        },

        calculateDiscount: function() {

            this.porcentajeDescuento = parseFloat(this.porcentajeDescuento);
            this.valorFacturaAcuerdo = parseFloat(this.valorFacturaAcuerdo);
            this.valorIntereses = parseFloat(this.valorIntereses);
            this.valorFacturaAcuerdo = parseFloat(this.dataUsu.valorFacturaAcuerdo);

            if (this.porcentajeDescuento > 0) {
                let porcentaje = 0
                let valorDescuento = 0 
                let valorPago = 0;
                this.valorAbono = this.valorAcuerdo = this.numeroCuotas = this.valorCuota = 0;

                porcentaje = this.porcentajeDescuento / 100;
                valorDescuento = this.valorIntereses * porcentaje; 
                valorPago = this.valorFacturaAcuerdo - valorDescuento;
                valorPago = Math.ceil(valorPago / 100) * 100;
                this.valorFacturaAcuerdo = valorPago;
            }
            else {
                this.valorFacturaAcuerdo = this.dataUsu.valorFacturaAcuerdo;    
                this.valorAbono = this.valorAcuerdo = this.numeroCuotas = this.valorCuota = 0;
            }
        },

        calculateValorAcuerdo: function() {

            let porcentaje = 0;
            let valorMinimo = 0;
            this.valorAcuerdo = this.valorCuota = this.numeroCuotas = 0;
            this.valorAbono = parseFloat(this.valorAbono);
            this.valorFacturaAcuerdo = parseFloat(this.valorFacturaAcuerdo);

            porcentaje = this.porcentajeMinimo / 100;
            valorMinimo = this.valorFacturaAcuerdo * porcentaje;
            valorMinimo = Number(valorMinimo.toFixed(2));
            if (this.valorAbono >= valorMinimo) {
                if (this.valorAbono > 0 && this.numFactura != "" && this.valorFacturaAcuerdo > 0) {
                    if (this.valorAbono < this.valorFacturaAcuerdo) {
    
                        this.valorAcuerdo = this.numeroCuotas = this.valorCuota = 0;
                        this.valorAcuerdo = this.valorFacturaAcuerdo - this.valorAbono;
                        this.valorAcuerdo = Number(this.valorAcuerdo.toFixed(2));
                    }
                    else {
                        Swal.fire("Error","El valor de abono no puede ser mayor o igual al valor de factura","error");
                    }
                }
                else {
                    Swal.fire("Error","El valor de abono debe ser mayor a cero","error");
                }
            }
            else {
                Swal.fire("Error","El valor de abono no cumple con el porcentaje minimo para realizar el acuerdo de pago","error");
            }
        },

        calculateValueCuota: function() {

            this.valorAcuerdo = parseFloat(this.valorAcuerdo);
            this.numeroCuotas = parseInt(this.numeroCuotas);

            if (this.valorAcuerdo > 0 && this.numeroCuotas > 0) {

                let valorCuotaConDecimales;
                valorCuotaConDecimales = this.valorAcuerdo / this.numeroCuotas;
                this.valorCuota = Number(valorCuotaConDecimales.toFixed(2));
            }
            else {
                Swal.fire("Error","El número de cuotas debe ser mayor a cero","error");
            }
        },

        save: async function() {
            if (this.tipoMov == '101') {
                //validaciones de guardado
                if (this.fechaAcuerdo != "" && this.numFactura && this.valorFactura > 0 && this.valorFacturaAcuerdo > 0 && this.valorAbono > 0 && this.valorAcuerdo > 0 && this.numeroCuotas > 0 && this.valorCuota && this.medioPago != "") {
                    
                    let validMethod = false;
                    if (this.medioPago == "banco") {
                        if (this.banco == "") {
                            validMethod == true;
                        }
                    }
    
                    if (validMethod == false) {
                            const formData = new FormData();
                            formData.append("fecha",this.fechaAcuerdo);
                            formData.append("numFactura",this.numFactura);
                            formData.append("codUsuario",this.dataUsu.codUsuario);
                            formData.append("documento",this.dataUsu.documento);
                            formData.append("name",this.dataUsu.name);
                            formData.append("valorFactura",this.valorFactura);
                            formData.append("descuentoIntereses",this.descuentoIntereses);
                            formData.append("porcentajeDescuento",this.porcentajeDescuento);
                            formData.append("valorFacturaAcuerdo",this.valorFacturaAcuerdo);
                            formData.append("valorAbono",this.valorAbono);
                            formData.append("valorAcuerdo",this.valorAcuerdo);
                            formData.append("numeroCuotas",this.numeroCuotas);
                            formData.append("valorCuota",this.valorCuota);
                            formData.append("medioPago",this.medioPago);
                            formData.append("banco",this.banco);

                        Swal.fire({
                            title:"¿Estás segur@ de guardar?",
                            text:"",
                            icon: 'warning',
                            showCancelButton:true,
                            confirmButtonColor: '#3085d6',
                            cancelButtonColor: '#d33',
                            confirmButtonText:"Sí, guardar",
                            cancelButtonText:"No, cancelar"
                        }).then(async function(result){
                            if(result.isConfirmed){
                                await axios.post(URL+'?action=save', formData).then((response) => {
                                    if (response.data.error == false) {
                                        Swal.fire("Guardado","Acuerdo de pago creado","success");
                                        setTimeout(function(){
                                            location.reload();
                                            // window.location.href='teso-abonosPredialEditar.php?id='+objData.consecutivo;
                                        },1500);
                                    }
                                    else {
                                        let msgError = response.data.msgError;
                                        Swal.fire("Error",msgError,"error");        
                                    }
                                });
                            }
                        });
                    }
                    else {
                        Swal.fire("Error","Todos los campos con * son obligatorios","error");    
                    }
                }
                else {
                    Swal.fire("Error","Todos los campos con * son obligatorios","error");
                }
            }
            else if (this.tipoMov == '102') {
                if (this.codAcuerdoRev != "" && this.valorRev !=  "") {
                    const formData = new FormData();
                    formData.append("codAcuerdo",this.codAcuerdoRev);
                    formData.append("numFactura",this.dataUsuRev.numFactura);
                    formData.append("fecha",this.dataUsuRev.fecha);
                    Swal.fire({
                        title:"¿Estás segur@ de anular?",
                        text:"",
                        icon: 'warning',
                        showCancelButton:true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText:"Sí, anular",
                        cancelButtonText:"No, cancelar"
                    }).then(async function(result){
                        if(result.isConfirmed){
                            await axios.post(URL+'?action=saveAnular', formData).then((response) => {
                                if (response.data.error == false) {
                                    Swal.fire("Anulado","Acuerdo de pago anulado","success");
                                    setTimeout(function(){
                                        location.reload();
                                        // window.location.href='teso-abonosPredialEditar.php?id='+objData.consecutivo;
                                    },1500);
                                }
                                else {
                                    let msgError = response.data.msgError;
                                    Swal.fire("Error",msgError,"error");        
                                }
                            });
                        }
                    });
                }
            }
            else if (this.tipoMov == '103') {
                if (this.codAcuerdoFinalizar != "" && this.dataUsuFinalizar.cuotasPendientes > 0 && this.dataUsuFinalizar.codUsuario != "") {
                    const formData = new FormData();
                    formData.append("codAcuerdo",this.codAcuerdoFinalizar);
                    formData.append("cuotasPendientes",this.dataUsuFinalizar.cuotasPendientes);
                    formData.append("codUsuario",this.dataUsuFinalizar.codUsuario);
                    Swal.fire({
                        title:"¿Estás segur@ de finalizar acuerdo?",
                        text:"",
                        icon: 'warning',
                        showCancelButton:true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText:"Sí, anular",
                        cancelButtonText:"No, cancelar"
                    }).then(async function(result){
                        if(result.isConfirmed){
                            await axios.post(URL+'?action=saveFinalizar', formData).then((response) => {
                                // console.log(response.data);
                                if (response.data.error == false) {
                                    Swal.fire("Finalizado","Acuerdo de pago finalizado","success");
                                    setTimeout(function(){
                                        location.reload();
                                        // window.location.href='teso-abonosPredialEditar.php?id='+objData.consecutivo;
                                    },1500);
                                }
                                else {
                                    let msgError = response.data.msgError;
                                    Swal.fire("Error",msgError,"error");        
                                }
                            });
                        }
                    });
                }
            }
        },

        searchAcuerdo: function(){

            if (this.codAcuerdoRev != "") {
                this.dataUsuRev = [];
                const formData = new FormData();
                formData.append("codAcuerdo",this.codAcuerdoRev);
    
                axios.post(URL+'?action=searchAcuerdoPago', formData).then((response) => {
                    if (response.data.error == false) {
                        this.dataUsuRev = response.data.datosUsuRev;
                        this.valorRev = this.dataUsuRev.valor;
                    }
                    else {
                        let msgError = response.data.msgError;
                        Swal.fire("Error",msgError,"error");        
                    }
                });
            }
            else {
                Swal.fire("Error","Debe colocar algún consecutivo de acuerdo de pago","error");
            }
        },

        searchAcuerdoFinalizar: function(){

            if (this.codAcuerdoFinalizar != "") {
                this.dataUsuFinalizar = [];

                const formData = new FormData();
                formData.append("codAcuerdo",this.codAcuerdoFinalizar);
    
                axios.post(URL+'?action=searchCodFinalizar', formData).then((response) => {
                    if (response.data.error == false) {
                        this.dataUsuFinalizar = response.data.datosAcuerdo;
                    }
                    else {
                        let msgError = response.data.msgError;
                        Swal.fire("Error",msgError,"error");        
                    }
                });
            }
            else {
                Swal.fire("Error","Debe colocar algún consecutivo de acuerdo de pago","error");
            }
        },

        pdf: function() {
            if (this.tipoMov == '101') {
                if (this.fechaAcuerdo != "" && this.numFactura != "" && this.medioPago != "" && this.valorFacturaAcuerdo != "" && this.valorAbono != "") {
                    const form = document.createElement("form");
                    form.method = "post";
                    form.target = "_blank";
                    form.action = URLPDF;
        
                    function addField(name,value){
                        const input = document.createElement("input");
                        input.type="hidden";
                        input.name=name;
                        input.value = value;
                        form.appendChild(input);
                    }
        
                    addField("fechaAcuerdo",this.fechaAcuerdo);
                    addField("codUsuario", this.dataUsu.codUsuario);
                    addField("numFactura", this.numFactura);
                    addField("name", this.dataUsu.name);
                    addField("document", this.dataUsu.documento);
                    addField("porcentajeDes", this.porcentajeDescuento);
                    addField("medioPago", this.medioPago);
                    addField("bancos", JSON.stringify(this.bancos));
                    addField("banco", this.banco);
                    addField("valueFactura", this.valorFacturaAcuerdo);
                    addField("valueAbono", this.valorAbono);
        
                    document.body.appendChild(form);
                    form.submit();
                    document.body.removeChild(form);
                }
                else {
                    Swal.fire("Error","Faltan datos * por llenar para generar el PDF","error");
                }
            }
            else {
                Swal.fire("Error","El PDF solo es para la creación del acuerdo","error");
            }
        },

        formatNumero: function(valor){
            return new Intl.NumberFormat("en-US", {style: "currency", currency: "USD"}).format(valor);
            
        },
    },
    computed:{

    }
})
