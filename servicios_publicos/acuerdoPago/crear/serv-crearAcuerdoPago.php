<?php

	require_once '../../../comun.inc';
    require '../../../funciones.inc';
    require '../../../funcionesSP.inc.php';
    header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
    session_start();

	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");

    $out = array('error' => false);

    $action = "show";

    if(isset($_GET['action'])){
        $action = $_GET['action'];
    }

    if ($action == "getData") {
        //consecutivo
        $consecutivo = selconsecutivo("srv_acuerdo_cab", "codigo_acuerdo");

        //bancos
        $bancos = [];
        $sqlBancos = "SELECT B.cuenta AS cuentaContable, B.ncuentaban AS cuentaBancaria, B.tipo AS tipo, T.razonsocial AS nombre FROM tesobancosctas B JOIN terceros T ON B.tercero = T.cedulanit";
        $bancos = mysqli_fetch_all(mysqli_query($linkbd, $sqlBancos), MYSQLI_ASSOC);

        //Porcentaje minimo de abono
        $sqlParametro = "SELECT porcentaje_acuerdo_pago AS porcentaje FROM srvparametros WHERE id = 1";
        $rowParametro = mysqli_fetch_assoc(mysqli_query($linkbd, $sqlParametro));

        $out["consecutivo"] = $consecutivo;
        $out["bancos"] = $bancos;
        $out["porcentajeMinimo"] = $rowParametro["porcentaje"];
    }

    if ($action == "dataFactura") {
        if(!empty($_SESSION)) {
            if(!empty($_POST['numFactura'])){
                $factura = $_POST['numFactura'];
                $valorFactura = $valorIntereses = 0;
                $name = $documento = $codUsuario = "";

                $sqlDataFactura = "SELECT id_cliente AS clienteId, estado_pago FROM srvcortes_detalle WHERE numero_facturacion = $factura";
                $rowDataFactura = mysqli_fetch_assoc(mysqli_query($linkbd, $sqlDataFactura));

                if ($rowDataFactura["estado_pago"] == 'S') {

                    $sqlAcuerdo = "SELECT * FROM srv_acuerdo_cab WHERE id_cliente = $rowDataFactura[clienteId] AND estado_acuerdo = 'Activo'";
                    $resAcuerdo = mysqli_query($linkbd, $sqlAcuerdo);
                    $cantAcuerdo = mysqli_num_rows($resAcuerdo);

                    if ($cantAcuerdo == 0) {
                        
                        $sqlCliente = "SELECT id_tercero AS terceroId, cod_usuario AS codUsuario FROM srvclientes WHERE id = $rowDataFactura[clienteId] AND estado = 'S'";
                        $rowCliente = mysqli_fetch_assoc(mysqli_query($linkbd, $sqlCliente));
                        
                        $valorFactura = searchValueFacturaTotal($factura);
                        $valorIntereses = searchValueIntereses($factura);
                        $valorFacturaAcuerdo = searchValueFacturaSinAproximacion($factura);
                        $documento = buscaDocumentoTerceroConId($rowCliente["terceroId"]);
                        $name = buscaNombreTerceroConId($rowCliente["terceroId"]);
                        $codUsuario = $rowCliente["codUsuario"];

                        $dataUsuario["codUsuario"] = $codUsuario;
                        $dataUsuario["documento"] = $documento;
                        $dataUsuario["name"] = $name;
                        $dataUsuario["valorFactura"] = $valorFactura;
                        $dataUsuario["valorIntereses"] = $valorIntereses;
                        $dataUsuario["valorFacturaAcuerdo"] = $valorFacturaAcuerdo;

                        $out["dataUsuario"] = $dataUsuario;
                        $out["error"] = false;
                    }
                    else {
                        $msgError = "Usuario tiene acuerdo de pago activo";
                        $out["error"] = true;
                        $out["msgError"] = $msgError;
                    }
                }
                else {
                    switch ($rowDataFactura["estado_pago"]) {
                        case 'V':
                            $msgError = "Número de factura se encuentra vencida";
                        break;

                        case 'A':
                            $msgError = "Número de factura ya tiene un acuerdo de pago";
                        break;

                        case 'R':
                            $msgError = "Número de factura fue reversada";
                        break;

                        case 'P':
                            $msgError = "Número de factura ya tiene pago registrado";
                        break;

                        case 'PC':
                            $msgError = "Número de factura fue prescrita";
                        break;
                        
                        default:
                            $msgError = "Error, no es posible usar esta factura para acuerdo de pago";
                        break;
                    }

                    $out["error"] = true;
                    $out["msgError"] = $msgError;
                }
            }
        }
    }

    if ($action == "save") {
        if(!empty($_SESSION)) {
            //inicializacion de variables
            $fechaAcuerdo = $codUsuario = $documento = $name = $medioPago = $banco = $numFactura = $msgError = "";
            $valorFactura = $porcentajeDescuento = $valorFacturaAcuerdo = $valorAbono = $valorAcuerdo = $numeroCuotas = $valorCuota = $clienteId = $consecutivo = 0;
            $descuentoIntereses = false;

            //Asignacion de datos
            $consecutivo = selconsecutivo("srv_acuerdo_cab", "codigo_acuerdo");
            $fechaAcuerdo = $_POST["fecha"];
            $numFactura = $_POST["numFactura"];
            $codUsuario = $_POST["codUsuario"];
            $documento = $_POST["documento"];
            $name = $_POST["name"];
            $medioPago = $_POST["medioPago"];
            $banco = $_POST["banco"];
            $valorFactura = $_POST["valorFactura"];
            $porcentajeDescuento = $_POST["porcentajeDescuento"];
            $valorFacturaAcuerdo = $_POST["valorFacturaAcuerdo"];
            $valorAbono = $_POST["valorAbono"];
            $valorAcuerdo = $_POST["valorAcuerdo"];
            $numeroCuotas = $_POST["numeroCuotas"];
            $valorCuota = $_POST["valorCuota"];
            $clienteId = encontrarIdClienteCodUsuario($codUsuario);
            $descuentoIntereses = $_POST["descuentoIntereses"];
            $fechaComoEntero = strtotime($fechaAcuerdo);
            $vigencia = date("Y", $fechaComoEntero);
            $tipoComprobante = "39";
            $usuario = $_SESSION["usuario"];
            $cedula = $_SESSION["cedulausu"];
            $ip = getRealIP();
            $hoy = date("Y-m-d H:i:s");

            //si el medio de pago es caja
            if ($medioPago == "caja") {
                $sqlCuentaCaja = "SELECT cuentacaja FROM tesoparametros";
                $rowCuentaCaja = mysqli_fetch_assoc(mysqli_query($linkbd,$sqlCuentaCaja));
                $banco = $rowCuentaCaja["cuentacaja"];
            } 

            $sql = "SELECT count(*) as valor FROM dominios dom  
            WHERE dom.nombre_dominio = 'PERMISO_MODIFICA_DOC'  AND dom.valor_final <= '$fechaAcuerdo'  AND dom.valor_inicial =  '$cedula'";
            $request = mysqli_query($linkbd,$sql)->fetch_assoc()['valor'];

            if($request >= 1) {
                //validacion de numero de factura no tenga acuerdo activo
                $sqlValid01 = "SELECT * FROM srv_acuerdo_cab WHERE codigo_factura = $numFactura AND estado_acuerdo != 'Reversado'";
                $rowValid01 = mysqli_query($linkbd, $sqlValid01);
                $resultValid01 = mysqli_num_rows($resultValid01);
    
                if ($resultValid01 == 0) {
                    //save srv
                    $sqlAcuerdoCab = "INSERT INTO srv_acuerdo_cab (codigo_acuerdo, id_cliente, cod_usuario, nombre_suscriptor, documento, fecha_acuerdo, codigo_factura, valor_factura, valor_abonado, valor_acuerdo, valor_cuota, numero_cuotas, medio_pago, numero_cuenta, estado_acuerdo, descuento_intereses) VALUES ($consecutivo, $clienteId, '$codUsuario', '$name', '$documento', '$fechaAcuerdo', $numFactura, $valorFacturaAcuerdo, $valorAbono, $valorAcuerdo, $valorCuota, $numeroCuotas, '$medioPago', '$banco', 'Activo', $porcentajeDescuento)";
                    mysqli_query($linkbd, $sqlAcuerdoCab);
    
                    $sqlComprobanteCabecera = "INSERT INTO comprobante_cab (numerotipo, tipo_comp, fecha, concepto, total_debito, total_credito, estado) VALUES ($consecutivo, '$tipoComprobante', '$fechaAcuerdo', 'Acuerdo de pago de la factura numero $numFactura', 0, 0, '1')";
                    mysqli_query($linkbd, $sqlComprobanteCabecera);
    
                    $sqlAuditoria = "INSERT INTO srv_auditoria (id_srv_funciones, accion, cedula, usuario, ip, fecha_hora, consecutivo) VALUES (5, 'Crear', '$cedula', '$usuario', '$ip', '$hoy', $consecutivo)";
                    mysqli_query($linkbd, $sqlAuditoria);
    
                    $sqlDetalle = "SELECT id_servicio AS servicioId, SUM(credito) AS valueCredito, SUM(debito) AS valueDebito FROM srvdetalles_facturacion WHERE numero_facturacion = $numFactura AND tipo_movimiento = '101' GROUP BY id_servicio;";
                    $resDetalle = mysqli_query($linkbd, $sqlDetalle);
                    $valorAbonadoSub = $valorAbonoSubAnt = 0;
                    while ($rowDetalle = mysqli_fetch_assoc($resDetalle)) {
                        $valorServicio = $abonoServicio = $valorCuotaServicio = 0;
                        $valorServicio = $rowDetalle["valueCredito"] - $rowDetalle["valueDebito"];
    
                        //Valida si tiene descuento de intereses
                        if ($descuentoIntereses == true) {
                            $interesesServicio = $descuento = $porcentaje = 0;
                            $interesesServicio = searchValueInteresServ($numFactura, $rowDetalle["servicioId"]);
                            $porcentaje = $porcentajeDescuento / 100;
                            $descuento = $interesesServicio * $porcentaje;
                            $valorServicio = $valorServicio - $descuento;
                        }
                
                        $valorServicio = round($valorServicio, 2);
                        $abonoValorServicio = ($valorServicio / $valorFacturaAcuerdo) * $valorAbono;
                        $abonoValorServicio = ceil($abonoValorServicio);
                        $valorAbonadoSub = $valorAbonadoSub + $abonoValorServicio;
                        $valorServicioMenosAbono = $valorServicio - $abonoValorServicio;
                        $valorServicioMenosAbono = round($valorServicioMenosAbono, 2);
                        $valorCuotaServicio = $valorServicioMenosAbono / $numeroCuotas;
                        $valorCuotaServicio = round($valorCuotaServicio, 2);
    
                        $sqlAcuerdoDet = "INSERT INTO srv_acuerdo_det (codigo_acuerdo, id_servicio, valor_cuota_servicio) VALUES ($consecutivo, $rowDetalle[servicioId], $valorCuotaServicio)";
                        mysqli_query($linkbd, $sqlAcuerdoDet);
      
                        if ($valorAbonadoSub > $valorAbono) {
                            $abonoValorServicio = $valorAbono - $valorAbonoSubAnt;
                        }

                        //save presupuesto
                        $sqlServicio = "SELECT cuenta, fuente, productoservicio, seccion_presupuestal FROM srvservicios WHERE id = $rowDetalle[servicioId]";
                        $rowServicio = mysqli_fetch_assoc(mysqli_query($linkbd, $sqlServicio));
    
                        $sqlRecaudoPresupuesto = "INSERT INTO srv_acuerdo_ppto (codigo_acuerdo, id_servicio, valor, vigencia, cuenta, fuente, producto_servicio, seccion_presupuestal, medio_pago, vigencia_gasto) VALUES ($consecutivo, $rowDetalle[servicioId], $abonoValorServicio, '$vigencia', '$rowServicio[cuenta]', '$rowServicio[fuente]', '$rowServicio[productoservicio]', '$rowServicio[seccion_presupuestal]', 'CSF', '1')";
                        mysqli_query($linkbd, $sqlRecaudoPresupuesto);
    
                        $sql = "SELECT acuerdo_pago, cc FROM srvservicios WHERE id = $rowDetalle[servicioId]";
                        $res = mysqli_query($linkbd,$sql);
                        $row = mysqli_fetch_row($res);
                    
                        $concepto = concepto_cuentasn2($row[0],'AP',10,$row[1],$fechaAcuerdo);
                    
                        for ($x=0; $x < count($concepto); $x++) { 
                            if($concepto[$x][2] == 'S') { $cuentaAcuerdo = $concepto[$x][0];}
                        }
    
                        //Agrega abono de acuerdo a contabilidad
                        $sqlAbono = "INSERT INTO comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque,valdebito, valcredito,estado,vigencia) 
                            VALUES ('$tipoComprobante $consecutivo', '$banco', '$documento', '$row[1]', 'Abono inicial de acuerdo de pago', '', $abonoValorServicio, 0, '1', '$vigencia')";
                        mysqli_query($linkbd, $sqlAbono);
                        
                        $sqlAbono = "INSERT INTO comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque,valdebito, valcredito,estado,vigencia) 
                            VALUES ('$tipoComprobante $consecutivo', '$cuentaAcuerdo', '$documento', '$row[1]', 'Abono inicial de acuerdo de pago', '', 0, $abonoValorServicio, '1', '$vigencia')";
                        mysqli_query($linkbd,$sqlAbono);

                        $valorAbonoSubAnt = $valorAbonoSubAnt + $abonoValorServicio;
                    }
    
                    //contabilidad
                    $facturas = [];
                    
                    $sql = "SELECT numero_facturacion, estado_pago FROM srvcortes_detalle WHERE id_cliente = $clienteId AND numero_facturacion <= $numFactura ORDER BY numero_facturacion DESC";
                    $res = mysqli_query($linkbd, $sql);
                    while ($row = mysqli_fetch_row($res)) {
                        if ($row[1] == "S" OR $row[1] == "V") {
                            $facturas[] = $row[0];
                        }
                        else if ($row[1] == "P" OR $row[1] == "A" OR $row[1] == "A") {
                            break;
                        }
                    }
    
                    $cargoFijo = $consumo = $subCargoFijo = $subConsumo = $contribucion = $acuerdoPago = $saldoInicial = $interesMoratorio = $desincentivo = [];
    
                    foreach ($facturas as $key => $factura) {
                        $sqlDet = "SELECT id_servicio, id_tipo_cobro, COALESCE(credito,0), COALESCE(debito,0), corte FROM srvdetalles_facturacion WHERE numero_facturacion = $factura AND (credito > 0 OR debito > 0) ORDER BY id_tipo_cobro ASC, id_servicio ASC";
                        $resDet = mysqli_query($linkbd, $sqlDet);
                        while ($rowDet = mysqli_fetch_row($resDet)) {
                            switch ($rowDet[1]) {
                                case 1:
                                    $cargoFijo[$rowDet[0]] += $rowDet[2];
                                break;
                                
                                case 2:
                                    $consumo[$rowDet[0]] += $rowDet[2];
                                break;
                    
                                case 3:
                                    $subCargoFijo[$rowDet[0]] += $rowDet[3];
                                break;
                    
                                case 4:
                                    $subConsumo[$rowDet[0]] += $rowDet[3];
                                break;
                    
                                case 5:
                                    $contribucion[$rowDet[0]] += $rowDet[2];
                                break;
                    
                                case 7:
                                    $acuerdoPago[$rowDet[0]] += $rowDet[2];
                                break;
            
                                case 8:
                                    //Saldos iniciales
                                    if ($rowDet[4] == 1) {
                                        $saldoInicial[$rowDet[0]] = (float) $rowDet[2];
                                    }
                                break;
                    
                                case 10;
                                    if ($factura == $numFactura) {
                                        $interesMoratorio[$rowDet[0]] = (float) $rowDet[2];
                                    }    
                                break;
    
                                case 12:
                                    $desincentivo[$rowDet[0]] += $rowDet[2];
                                break;
                            }
                        }
                    }
    
                    for ($i=1; $i <= count($cargoFijo); $i++) { 
                        //Descuento de subsidios
                        $cargoFijo[$i] = $cargoFijo[$i] - $subCargoFijo[$i];
                        $consumo[$i] = $consumo[$i] - $subConsumo[$i];
                    }
    
                    //guardado
                    for ($i=1; $i <= count($cargoFijo); $i++) { 
    
                        $cargoFijo[$i] = round($cargoFijo[$i], 2);
                        $consumo[$i] = round($consumo[$i], 2);
                        $contribucion[$i] = round($contribucion[$i], 2);
                        $acuerdoPago[$i] = round($acuerdoPago[$i], 2);
                        $interesMoratorio[$i] = round($interesMoratorio[$i], 2);
                        $saldoInicial[$i] = round($saldoInicial[$i], 2);
                        $desincentivo[$i] = round($desincentivo[$i], 2);
                        $cuentaAcuerdo = "";
    
                        $sql = "SELECT acuerdo_pago, cc FROM srvservicios WHERE id = $i";
                        $res = mysqli_query($linkbd,$sql);
                        $row = mysqli_fetch_row($res);
                    
                        $concepto = concepto_cuentasn2($row[0],'AP',10,$row[1],$fechaAcuerdo);
                    
                        for ($x=0; $x < count($concepto); $x++) { 
                            if($concepto[$x][2] == 'S') { $cuentaAcuerdo = $concepto[$x][0];}
                        }
    
                        if ($cargoFijo[$i] > 0) {
                            
                            $sql = "SELECT cargo_fijo_u, cc, nombre FROM srvservicios WHERE id = $i";
                            $res = mysqli_query($linkbd,$sql);
                            $row = mysqli_fetch_row($res);
                        
                            $concepto = concepto_cuentasn2($row[0],'SS',10,$row[1],$fechaAcuerdo);
                        
                            for ($x=0; $x < count($concepto); $x++) { 
                                if($concepto[$x][2] == 'S') { $cuentaCredito = $concepto[$x][0];}
                            }
                    
                            $sqlr="INSERT INTO comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque,valdebito, valcredito,estado,vigencia) 
                                VALUES ('$tipoComprobante $consecutivo', '$cuentaAcuerdo', '$documento', '$row[1]', 'Acuerdo de pago del cargo fijo de $row[2]', '', $cargoFijo[$i], 0, '1', '$vigencia')";
                            mysqli_query($linkbd,$sqlr);
                    
                            $sqlr="INSERT INTO comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque,valdebito, valcredito,estado,vigencia) 
                                VALUES ('$tipoComprobante $consecutivo', '$cuentaCredito', '$documento', '$row[1]', 'Acuerdo de pago del cargo fijo de $row[2]', '', 0, $cargoFijo[$i], '1', '$vigencia')";
                            mysqli_query($linkbd,$sqlr);
                        }
            
                        if ($consumo[$i] > 0) {
            
                            $sql = "SELECT consumo_u, cc, nombre FROM srvservicios WHERE id = $i ";
                            $res = mysqli_query($linkbd,$sql);
                            $row = mysqli_fetch_row($res);
            
                            $concepto = concepto_cuentasn2($row[0],'CL',10,$row[1],$fechaAcuerdo);
            
                            for ($x=0; $x < count($concepto); $x++) { 
                                if($concepto[$x][2] == 'S') { $cuentaCredito = $concepto[$x][0]; }
                            }
            
                            $sqlr="INSERT INTO comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque,valdebito, valcredito,estado,vigencia) 
                                VALUES ('$tipoComprobante $consecutivo', '$cuentaAcuerdo', '$documento', '$row[1]', 'Acuerdo de pago del consumo de $row[2]', '', $consumo[$i], 0, '1', '$vigencia')";
                            mysqli_query($linkbd,$sqlr);
                    
                            $sqlr="INSERT INTO comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque,valdebito, valcredito,estado,vigencia) 
                                VALUES ('$tipoComprobante $consecutivo', '$cuentaCredito', '$documento', '$row[1]', 'Acuerdo de pago del consumo de $row[2]', '', 0, $consumo[$i], '1', '$vigencia')";
                            mysqli_query($linkbd,$sqlr);
                        }
            
                        if ($contribucion[$i] > 0) {
            
                            $sqlServicio = "SELECT contribucion_u, cc, nombre FROM srvservicios WHERE id = $i ";
                            $resServicio = mysqli_query($linkbd,$sqlServicio);
                            $rowServicio = mysqli_fetch_row($resServicio);
            
                            $concepto = concepto_cuentasn2($rowServicio[0],'SC',10,$rowServicio[1],$fechaAcuerdo);
            
                            for ($x=0; $x < count($concepto); $x++) { 
                                if($concepto[$x][2] == 'S') { $cuentaCredito = $concepto[$x][0]; }
                            }
            
                            $sqlr="INSERT INTO comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque,valdebito, valcredito,estado,vigencia) 
                                VALUES ('$tipoComprobante $consecutivo', '$cuentaAcuerdo', '$documento', '$row[1]', 'Acuerdo de pago de la contribuccion de $row[2]', '', $contribucion[$i], 0, '1', '$vigencia')";
                            mysqli_query($linkbd,$sqlr);
                    
                            $sqlr="INSERT INTO comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque,valdebito, valcredito,estado,vigencia) 
                                VALUES ('$tipoComprobante $consecutivo', '$cuentaCredito', '$documento', '$row[1]', 'Acuerdo de pago de la contribuccion de $row[2]', '', 0, $contribucion[$i], '1', '$vigencia')";
                            mysqli_query($linkbd,$sqlr);
                        }
                    
                        if ($interesMoratorio[$i] > 0) {
            
                            if ($descuentoIntereses == true) {
                            
                                $porcentaje = $valorInt = $descuento = 0;
             
                                $porcentaje = $porcentajeDescuento / 100;
                                $valorInt = $interesMoratorio[$i];
                                $descuento = $valorInt * $porcentaje;
            
                                $interesMoratorio[$i] = $interesMoratorio[$i] - $descuento;
                            }   
    
                            if ($interesMoratorio[$i] > 0) {
    
                                $sqlServicio = "SELECT interes_moratorio, cc, nombre FROM srvservicios WHERE id = $i ";
                                $resServicio = mysqli_query($linkbd,$sqlServicio);
                                $rowServicio = mysqli_fetch_row($resServicio);
                
                                $concepto = concepto_cuentasn2($rowServicio[0],'SM',10,$rowServicio[1],$fechaAcuerdo);
                
                                for ($x=0; $x < count($concepto); $x++) 
                                { 
                                    if($concepto[$x][3] == 'S' and $concepto[$x][2] == 'N') {
                                        $cuentaCuatro = $concepto[$x][0];
                                    }
                                    if ($concepto[$x][3] == 'N' and $concepto[$x][2] == 'S') {  
                                        $cuentaCredito = $concepto[$x][0];
                                    }
                                }
                
                                //Liquidación de intereses
                                $sqlr="INSERT INTO comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque,valdebito, valcredito,estado,vigencia) 
                                    VALUES ('$tipoComprobante $consecutivo', '$cuentaCredito', '$documento', '$row[1]', 'Liquido interes moratorio de $row[2]', '', $interesMoratorio[$i], 0, '1', '$vigencia')";
                                mysqli_query($linkbd,$sqlr);
                        
                                $sqlr="INSERT INTO comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque,valdebito, valcredito,estado,vigencia) 
                                    VALUES ('$tipoComprobante $consecutivo', '$cuentaCuatro', '$documento', '$row[1]', 'Liquido interes moratorio de $row[2]', '', 0, $interesMoratorio[$i], '1', '$vigencia')";
                                mysqli_query($linkbd,$sqlr);
                
                                //traslado de intereses
                                $sqlr="INSERT INTO comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque,valdebito, valcredito,estado,vigencia) 
                                    VALUES ('$tipoComprobante $consecutivo', '$cuentaAcuerdo', '$documento', '$row[1]', 'Acuerdo de pago de interes moratorio de $row[2]', '', $interesMoratorio[$i], 0, '1', '$vigencia')";
                                mysqli_query($linkbd,$sqlr);
                        
                                $sqlr="INSERT INTO comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque,valdebito, valcredito,estado,vigencia) 
                                    VALUES ('$tipoComprobante $consecutivo', '$cuentaCredito', '$documento', '$row[1]', 'Acuerdo de pago de interes moratorio de $row[2]', '', 0, $interesMoratorio[$i], '1', '$vigencia')";
                                mysqli_query($linkbd,$sqlr);
                            }
                        }
            
                        if ($saldoInicial[$i] > 0) {
            
                            $sql = "SELECT consumo_u, cc, nombre FROM srvservicios WHERE id = $i ";
                            $res = mysqli_query($linkbd,$sql);
                            $row = mysqli_fetch_row($res);
            
                            $concepto = concepto_cuentasn2($row[0],'CL',10,$row[1],$fechaAcuerdo);
            
                            for ($x=0; $x < count($concepto); $x++) 
                            { 
                                if($concepto[$x][2] == 'S')
                                {
                                    $cuentaCredito = $concepto[$x][0];
                                }
                            }
            
                            $sqlr="INSERT INTO comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque,valdebito, valcredito,estado,vigencia) 
                                VALUES ('$tipoComprobante $consecutivo', '$cuentaAcuerdo', '$documento', '$row[1]', 'Acuerdo de pago del saldo inicial de $row[2]', '', $saldoInicial[$i], 0, '1', '$vigencia')";
                            mysqli_query($linkbd,$sqlr);
                    
                            $sqlr="INSERT INTO comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque,valdebito, valcredito,estado,vigencia) 
                                VALUES ('$tipoComprobante $consecutivo', '$cuentaCredito', '$documento', '$row[1]', 'Acuerdo de pago del saldo inicial de $row[2]', '', 0, $saldoInicial[$i], '1', '$vigencia')";
                            mysqli_query($linkbd,$sqlr);
                        }
    
                        if ($desincentivo[$i] > 0) {
                            
                            $sql = "SELECT consumo_u, cc, nombre FROM srvservicios WHERE id = $i ";
                            $res = mysqli_query($linkbd,$sql);
                            $row = mysqli_fetch_row($res);
            
                            $concepto = concepto_cuentasn2($row[0],'CL',10,$row[1],$fechaAcuerdo);
            
                            for ($x=0; $x < count($concepto); $x++) { 
                                if($concepto[$x][2] == 'S') { $cuentaCredito = $concepto[$x][0]; }
                            }
            
                            $sqlr="INSERT INTO comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque,valdebito, valcredito,estado,vigencia) 
                                VALUES ('$tipoComprobante $consecutivo', '$cuentaAcuerdo', '$documento', '$row[1]', 'Acuerdo de pago del desincentivo de $row[2]', '', $consumo[$i], 0, '1', '$vigencia')";
                            mysqli_query($linkbd,$sqlr);
                    
                            $sqlr="INSERT INTO comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque,valdebito, valcredito,estado,vigencia) 
                                VALUES ('$tipoComprobante $consecutivo', '$cuentaCredito', '$documento', '$row[1]', 'Acuerdo de pago del desincentivo de $row[2]', '', 0, $consumo[$i], '1', '$vigencia')";
                            mysqli_query($linkbd,$sqlr);
                        }
                    }
            
                    $sqlStatusFactura = "UPDATE srvcortes_detalle SET estado_pago = 'A' WHERE numero_facturacion = $numFactura";
                    mysqli_query($linkbd, $sqlStatusFactura);
                }
                else {
                    $msgError = "Ya hay acuerdos activos con este número de factura";
                    $out["error"] = true;
                    $out["msgError"] = $msgError;
                }
            }
            else {
                $msgError = "Fecha de acuerdo de pago se encuentra cerrada";
                $out["error"] = true;
                $out["msgError"] = $msgError;
            }
        }
        else {
            $msgError = "Sesión de usuario cerrada, por favor ingrese a su usuario de nuevo";
            $out["error"] = true;
            $out["msgError"] = $msgError;
        }
    }

    if ($action == "searchAcuerdoPago") {
        if(!empty($_SESSION)) {
            $codAcuerdo = 0;
            $codAcuerdo = $_POST["codAcuerdo"];
    
            $sqlAcuerdo = "SELECT cod_usuario AS codUsuario, codigo_factura AS numFactura, valor_abonado AS valor, fecha_acuerdo AS fecha, cuotas_liquidadas AS cuotasFacturadas, estado_acuerdo AS estadoAcuerdo FROM srv_acuerdo_cab WHERE codigo_acuerdo = $codAcuerdo";
            $rowAcuerdo = mysqli_fetch_assoc(mysqli_query($linkbd, $sqlAcuerdo));

            if ($rowAcuerdo["estadoAcuerdo"] == "Activo") {
                if ($rowAcuerdo["cuotasFacturadas"] == 0) {
                    $datosUsu = [];

                    $datosUsu["codUsu"] = $rowAcuerdo["codUsuario"];
                    $datosUsu["numFactura"] = $rowAcuerdo["numFactura"];
                    $datosUsu["valor"] = $rowAcuerdo["valor"];
                    $datosUsu["fecha"] = $rowAcuerdo["fecha"];

                    $out["datosUsuRev"] = $datosUsu;
                }
                else {
                    $msgError = "Acuerdo de pago ya se han liquidado cuotas";
                    $out["error"] = true;
                    $out["msgError"] = $msgError;
                }
            }
            else {
                if ($rowAcuerdo["estadoAcuerdo"] == "Reversado") {
                    $msgError = "Acuerdo de pago se encuentra reversado";
                    $out["error"] = true;
                    $out["msgError"] = $msgError;
                }
                else if ($rowAcuerdo["estadoAcuerdo"] == "Finalizado") {
                    $msgError = "Acuerdo de pago se encuentra finalizado";
                    $out["error"] = true;
                    $out["msgError"] = $msgError;
                }
            }
        }
        else {
            $msgError = "Sesión de usuario cerrada, por favor ingrese a su usuario de nuevo";
            $out["error"] = true;
            $out["msgError"] = $msgError;
        }
    }

    if ($action == "saveAnular") {
        if(!empty($_SESSION))  {
            $codAcuerdo = $numFactura = 0;
            $fecha = "";
            $tipoComp = 39;
            $codAcuerdo = $_POST["codAcuerdo"];
            $numFactura = $_POST["numFactura"];
            $fecha = $_POST["fecha"];
            $usuario = $_SESSION["usuario"];
            $cedula = $_SESSION["cedulausu"];
            $ip = getRealIP();
            $hoy = date("Y-m-d H:i:s");

            $sql = "SELECT count(*) as valor FROM dominios dom  
            WHERE dom.nombre_dominio = 'PERMISO_MODIFICA_DOC'  AND dom.valor_final <= '$fecha' AND dom.valor_inicial = '$cedula'";
            $request = mysqli_query($linkbd,$sql)->fetch_assoc()['valor'];

            if($request >= 1) {
                $updateAcuerdo = "UPDATE srv_acuerdo_cab SET estado_acuerdo = 'Reversado' WHERE codigo_acuerdo = $codAcuerdo";
                $updateComprobante = "UPDATE comprobante_cab SET estado = 0 WHERE tipo_comp = $tipoComp AND numerotipo = $codAcuerdo";
                $updateStatus = "UPDATE srvcortes_detalle SET estado_pago = 'S' WHERE numero_facturacion = $numFactura";
                $sqlAuditoria = "INSERT INTO srv_auditoria (id_srv_funciones, accion, cedula, usuario, ip, fecha_hora, consecutivo) VALUES (5, 'Anular', '$cedula', '$usuario', '$ip', '$hoy', $codAcuerdo)";

                mysqli_query($linkbd, $updateAcuerdo);
                mysqli_query($linkbd, $updateComprobante);
                mysqli_query($linkbd, $updateStatus);
                mysqli_query($linkbd, $sqlAuditoria);

                $out["error"] = false;
            }
            else {
                $msgError = "Fecha de acuerdo de pago se encuentra cerrada";
                $out["error"] = true;
                $out["msgError"] = $msgError;
            }
        }
        else {
            $msgError = "Sesión de usuario cerrada, por favor ingrese a su usuario de nuevo";
            $out["error"] = true;
            $out["msgError"] = $msgError;
        }
    }

    if ($action == "searchCodFinalizar") {
        if(!empty($_SESSION))  {
            $codAcuerdo = 0;
            $codAcuerdo = $_POST["codAcuerdo"];

            $sqlAcuerdo = "SELECT cod_usuario, codigo_factura, valor_acuerdo, numero_cuotas, valor_cuota, cuotas_liquidadas, valor_liquidado, estado_acuerdo FROM srv_acuerdo_cab WHERE codigo_acuerdo = $codAcuerdo";
            $rowAcuerdo = mysqli_fetch_assoc(mysqli_query($linkbd, $sqlAcuerdo));
            
            if ($rowAcuerdo["cuotas_liquidadas"] >= 1) {
                if ($rowAcuerdo["estado_acuerdo"] == 'Activo') {
                    $valorPendiente = $cuotasPendientes = 0;
                    $datosAcuerdo = [];
                    
                    $valorPendiente = $rowAcuerdo["valor_acuerdo"] - $rowAcuerdo["valor_liquidado"];
                    $cuotasPendientes = $rowAcuerdo["numero_cuotas"] - $rowAcuerdo["cuotas_liquidadas"];
        
                    $datosAcuerdo["codUsuario"] = $rowAcuerdo["cod_usuario"];
                    $datosAcuerdo["numFactura"] = $rowAcuerdo["codigo_factura"];
                    $datosAcuerdo["valorAcuerdo"] = number_format($rowAcuerdo["valor_acuerdo"], 2);
                    $datosAcuerdo["numCuotas"] = $rowAcuerdo["numero_cuotas"];
                    $datosAcuerdo["valorCuota"] = number_format($rowAcuerdo["valor_cuota"], 2);
                    $datosAcuerdo["cuotasFacturadas"] = $rowAcuerdo["cuotas_liquidadas"];
                    $datosAcuerdo["cuotasPendientes"] = $cuotasPendientes;
                    $datosAcuerdo["valorPendiente"] = number_format($valorPendiente, 2);
    
                    $out["datosAcuerdo"] = $datosAcuerdo;
                }
                else {
                    $msgError = "Acuerdo de pago no se encuentra activo";
                    $out["error"] = true;
                    $out["msgError"] = $msgError;    
                }
            }
            else {
                $msgError = "Acuerdo de pago no se ha liquidado ninguna cuota, se puede anular";
                $out["error"] = true;
                $out["msgError"] = $msgError;
            }
        }
        else {
            $msgError = "Sesión de usuario cerrada, por favor ingrese a su usuario de nuevo";
            $out["error"] = true;
            $out["msgError"] = $msgError;
        }
    }

    if ($action == "saveFinalizar") {
        if(!empty($_SESSION))  {
            $codAcuerdo = $cuotasPendientes = $codUsuario = $clienteId = 0;

            $codAcuerdo = $_POST["codAcuerdo"];
            $cuotasPendientes = $_POST["cuotasPendientes"] + 1;
            $codUsuario = $_POST["codUsuario"];
            $clienteId = encontrarIdClienteCodUsuario($codUsuario);
            $usuario = $_SESSION["usuario"];
            $cedula = $_SESSION["cedulausu"];
            $ip = getRealIP();
            $hoy = date("Y-m-d H:i:s");

            //ultimo corte activo
            $sqlCorte = "SELECT numero_corte FROM srvcortes WHERE estado = 'S' LIMIT 1";
            $rowCorte = mysqli_fetch_assoc(mysqli_query($linkbd, $sqlCorte));

            if ($rowCorte["numero_corte"] != ""){

                //busca factura pendiente
                $sqlFactura = "SELECT numero_facturacion AS numFactura, estado_pago FROM srvcortes_detalle WHERE id_corte = $rowCorte[numero_corte] AND id_cliente = $clienteId";
                $rowFactura = mysqli_fetch_assoc(mysqli_query($linkbd, $sqlFactura));

                if ($rowFactura["estado_pago"] == 'S') {
                   
                    $sqlAcuerdoDet = "SELECT id_servicio AS servicioId, valor_cuota_servicio AS valorCuota FROM srv_acuerdo_det WHERE codigo_acuerdo = $codAcuerdo";
                    $cuotasServicios = mysqli_fetch_all(mysqli_query($linkbd, $sqlAcuerdoDet), MYSQLI_ASSOC);
                    
                    foreach ($cuotasServicios as $i => $cuotaServicio) {
                        $valueCuotaServicio = round(($cuotaServicio["valorCuota"] * $cuotasPendientes), 2);
                        $updateValueFactura = "UPDATE srvfacturas SET acuerdo_pago = $valueCuotaServicio WHERE num_factura = $rowFactura[numFactura] AND id_servicio = $cuotaServicio[servicioId]";
                        $updateValueDet = "UPDATE srvdetalles_facturacion SET credito = $valueCuotaServicio WHERE numero_facturacion = $rowFactura[numFactura] AND tipo_movimiento = '101' AND id_servicio = $cuotaServicio[servicioId] AND id_tipo_cobro = 7";
                        $updateStatus = "UPDATE srv_acuerdo_cab SET estado_acuerdo = 'Finalizado' WHERE codigo_acuerdo = $codAcuerdo";
                        $sqlAuditoria = "INSERT INTO srv_auditoria (id_srv_funciones, accion, cedula, usuario, ip, fecha_hora, consecutivo) VALUES (5, 'Finalizado', '$cedula', '$usuario', '$ip', '$hoy', $codAcuerdo)";

                        mysqli_query($linkbd, $updateValueFactura);
                        mysqli_query($linkbd, $updateValueDet);
                        mysqli_query($linkbd, $updateStatus);
                        mysqli_query($linkbd, $sqlAuditoria);
                        $out["error"] = false;
                    }
                }
                else {
                    $msgError = "La factura del usuario no se encuentra pendiente de pago";
                    $out["error"] = true;
                    $out["msgError"] = $msgError;        
                }
            }
            else {
                $msgError = "No se encuentra ningún corte activo";
                $out["error"] = true;
                $out["msgError"] = $msgError;    
            }
        }
        else {
            $msgError = "Sesión de usuario cerrada, por favor ingrese a su usuario de nuevo";
            $out["error"] = true;
            $out["msgError"] = $msgError;
        }
    }

    header("Content-type: application/json");
    echo json_encode($out);
    die();