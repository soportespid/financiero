<?php
    require_once '../../comun.inc';
    require '../../funciones.inc';
    session_start();

    $linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
    ini_set('max_execution_time',9999999999);

    $out = array('error' => false);

    $action = "show";

    if(isset($_GET['action'])){
        $action = $_GET['action'];
    }
    
    if ($action == 'buscarDatos') {
        
        preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST["fechaIni"], $f);
        $fechaIni = $f[3]."-".$f[2]."-".$f[1];

        preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST["fechaFin"], $f);
        $fechaFin = $f[3]."-".$f[2]."-".$f[1];

        $datosRecaudo = array();

        $sqlRecaudos = "SELECT codigo_recaudo, cod_usuario, suscriptor, numero_factura, fecha_recaudo, medio_pago, numero_cuenta FROM srv_recaudo_factura WHERE fecha_recaudo BETWEEN '$fechaIni' AND '$fechaFin' ORDER BY codigo_recaudo ASC";
        $resRecaudos = mysqli_query($linkbd, $sqlRecaudos);
        while ($rowRecaudos = mysqli_fetch_row($resRecaudos)) {

            array_push($datosRecaudo, $rowRecaudos);
        }

        $out['datosRecaudo'] = $datosRecaudo;
    }

    if ($action == 'reflejar') {
        
    }

    header("Content-type: application/json");
    echo json_encode($out);
    die();