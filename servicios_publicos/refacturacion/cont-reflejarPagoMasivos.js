var app = new Vue({
    el: '#myapp',
	data:{

        error: '',
        loading: false,
        recaudos: [],
    },

    mounted: function(){

      
    },

    methods: 
    {
        toFormData: function(obj){
            var form_data = new FormData();
            for(var key in obj){
                form_data.append(key, obj[key]);
            }
            return form_data;
        },

        formatonumero: function(valor){
            return new Intl.NumberFormat("en-US", {style: "currency", currency: "USD"}).format(valor);
        },

        buscarDatos: async function() {

            var fechaIni = document.getElementById('fechaIni').value;
            var fechaFin = document.getElementById('fechaFin').value;

            if (fechaIni != '' && fechaFin != '') {

                var formData = new FormData();
                formData.append("fechaIni", fechaIni);
                formData.append("fechaFin", fechaFin);
                
                await axios.post('servicios_publicos/refacturacion/cont-reflejarPagoMasivos.php?action=buscarDatos', formData)
                .then((response) => {
                    
                    // console.log(response.data);
                }).catch((error) => {

                    this.error = true;
                    console.log(error)

                }).finally(() => {
                    this.loading =  false;
                    
                });  
            }
            else {
                Swal.fire({
                    icon: 'error',
                    title: 'Información incompleta',
                    text: 'Debes llenar el campo de fecha inicial y final.',
                })
            }
        },

        reflejarDocumentos: async function() {

            if (this.recaudos != '') {

            await axios.post('servicios_publicos/refacturacion/cont-reflejarPagoMasivos.php?action=reflejar', formData)
            .then((response) => {
                
                // console.log(response.data);
            }).catch((error) => {

                this.error = true;
                console.log(error)

            }).finally(() => {
                this.loading =  false;
                
            });  
            }
            else {
                Swal.fire({
                    icon: 'error',
                    title: 'Información incompleta',
                    text: 'Debes llenar el campo de fecha inicial y final.',
                })
            }
        },
    }
});