var app = new Vue({
    el: '#myapp',
	data:{

        error: '',
        loading: false,
        showModalLiquidaciones: false,
        periodo: '',
        descripcionPeriodo: '',
        datosPeriodosLiquidados: [],
        datosUsuarios: [],
    },

    mounted: function(){

      
    },

    methods: 
    {
        periodosLiquidados: async function() {

            this.loading = true;

            await axios.post('servicios_publicos/reporte_cartera/serv-reporte_cartera.php')
            .then((response) => {
                
                app.datosPeriodosLiquidados = response.data.periodosLiquidados;
            }).catch((error) => {
                this.error = true;
                console.log(error)
            }).finally(() => {
                this.loading =  false;
                this.showModalLiquidaciones = true;
            });     
        },

        seleccionaPeriodo: function(periodo) {

            this.periodo = periodo[0];
            this.descripcionPeriodo = periodo[1];
            this.showModalLiquidaciones = false;
        },

        buscarInformacionUsurios: async function() {

            if (this.periodo != '') {

                this.loading = true;
                

                await axios.post('servicios_publicos/refacturacion/cont-reflejarFacturacionsp.php?action=reflejar&periodo='+this.periodo)
                .then((response) => {
                    console.log(response.data);
                    if (response.data.guardado) {
                        this.loading =  false;
                        Swal.fire({
                            icon: 'success',
                            title: 'Se ha reflejado con exito',
                            showConfirmButton: false,
                            timer: 1500
                            }).then((response) => {
                                
                            });
                    }
                    else {
                        Swal.fire("Error", "Proceso no completado", "error");
                    }
                }).catch((error) => {

                    this.error = true;
                    console.log(error)

                }).finally(() => {
                    
                });     
            }
            else {
                Swal.fire("Faltan datos", "Selecciona un periodo liquidado", "warning");
            }
        },
    }
});