<?php
    require_once '../../comun.inc';
    require '../../funciones.inc';
    session_start();

    $linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
    ini_set('max_execution_time',9999999999);

    $out = array('error' => false);

    $action = "show";

    function encuentraCuentas($codigo, $tipo, $modulo) {

        $linkbd = conectar_v7();
        $arrayCuentas = array();
        
        $query = "SELECT cuenta, cc, debito, credito FROM conceptoscontables_det WHERE codigo = '$codigo' AND tipo = '$tipo' AND modulo = '$modulo' ORDER BY debito DESC ";
        $respuesta = mysqli_query($linkbd, $query);
        while ($row = mysqli_fetch_row($respuesta)) {

            array_push($arrayCuentas, $row);
        }

        mysqli_close($linkbd);

        return $arrayCuentas;
    }

    if(isset($_GET['action'])){
        $action = $_GET['action'];
    }
    
    if ($action == "show") {
       
    }

    if ($action == "reflejar") {

        $corteId = $_GET['periodo'];

        $sqlCorte = "SELECT fecha_impresion, YEAR(fecha_impresion) FROM srvcortes WHERE numero_corte = $corteId";
        $rowCorte = mysqli_fetch_assoc(mysqli_query($linkbd, $sqlCorte));

        $fechaCorte = $rowCorte['fecha_impresion'];
        $vigencia = $rowCorte['YEAR(fecha_impresion)'];
    
        $idTipoCobro = ("7, 8");
        $facturas = [];

        $deleteCab = [];
        $deleteDet = [];
        $detallesFactura = [];

        //Definidos
        $codigoComprobante = '29';

        $sqlCorteDetalle = "SELECT MIN(numero_facturacion), MAX(numero_facturacion) FROM srvcortes_detalle WHERE id_corte = $corteId ORDER BY id ASC";
        $rowCorteDetalle = mysqli_fetch_row(mysqli_query($linkbd, $sqlCorteDetalle));

        $menor = $rowCorteDetalle[0];
        $mayor = $rowCorteDetalle[1];

        $queryClearCab = "DELETE FROM comprobante_cab WHERE tipo_comp = '$codigoComprobante' AND numerotipo >= '$menor' AND numerotipo <= '$mayor'";
        mysqli_query($linkbd, $queryClearCab);

        $queryClearDet = "DELETE FROM comprobante_det WHERE tipo_comp = '$codigoComprobante' AND numerotipo >= '$menor' AND numerotipo <= '$mayor'";
        mysqli_query($linkbd, $queryClearDet);

        $sqlDetalleCorte = "SELECT numero_facturacion FROM srvcortes_detalle WHERE id_corte = $corteId ORDER BY id ASC";
        $resDetalleCorte = mysqli_query($linkbd, $sqlDetalleCorte);
        while ($rowDetalleCorte = mysqli_fetch_row($resDetalleCorte)) {
            
            array_push($facturas, $rowDetalleCorte);
        }

        foreach ($facturas as $key => $factura) {
            
            $numeroFactura = $factura[0];

            $sqlComprobanteCabecera = "INSERT INTO comprobante_cab (numerotipo, tipo_comp, fecha, concepto, total_debito, total_credito, estado) VALUES ('$numeroFactura', '$codigoComprobante', '$fechaCorte', 'Facturacion numero $numeroFactura', 0, 0, '1') ";
            mysqli_query($linkbd, $sqlComprobanteCabecera);
        }

        $sqlDetalleFacturacion = "SELECT T.cedulanit, DF.numero_facturacion, DF.id_servicio, DF.id_tipo_cobro, DF.credito, DF.debito FROM srvdetalles_facturacion AS DF INNER JOIN srvclientes AS C ON DF.id_cliente = C.id INNER JOIN terceros AS T ON C.id_tercero = T.id_tercero WHERE DF.corte = $corteId AND DF.tipo_movimiento = '101' AND DF.id_tipo_cobro != '7' AND DF.id_tipo_cobro != '8' AND (DF.debito > 0 OR DF.credito > 0) ORDER BY DF.numero_facturacion ASC, DF.id_servicio ASC, DF.id_tipo_cobro ASC";
        $resDetalleFacturacion = mysqli_query($linkbd, $sqlDetalleFacturacion);
        while ($rowDetalleFacturacion = mysqli_fetch_row($resDetalleFacturacion)) {
            
            array_push($detallesFactura, $rowDetalleFacturacion);
        }

        foreach ($detallesFactura as $key => $detalle) {
           
            $cedula = "";
            $numeroFactura = "";
            $servicioId = "";
            $tipoCobroId = "";
            $credito = "";
            $debito = "";

            $cedulaId = $detalle[0];
            $numeroFactura = $detalle[1];
            $servicioId = $detalle[2];
            $tipoCobroId = $detalle[3];
            $credito = $detalle[4];
            $debito = $detalle[5];

            switch ($tipoCobroId) {
                case 1:
                    $sqlServicio = "SELECT cargo_fijo_u, nombre, cc FROM srvservicios WHERE id = '$servicioId'";
                    $rowServicio = mysqli_fetch_assoc(mysqli_query($linkbd, $sqlServicio));

                    $codigo = $rowServicio['cargo_fijo_u'];
                    $servicio = $rowServicio['nombre'];
                    $descripcion = "Cargo fijo";
                    $cc = $rowServicio['cc'];

                    $valor = $credito;
                    $cuentas = concepto_cuentasn2($codigo,'SS',10,$cc,$fechaCorte);

                    for ($i=0; $i < count($cuentas); $i++) { 

                        //debito
                        if($cuentas[$i][3] == 'N' and $cuentas[$i][2] == 'S') {
                            
                            $cuentaDebito = $cuentas[$i][0];

                            $valuesCargoFijo = "INSERT INTO comprobante_det (id_comp, cuenta, tercero, centrocosto, detalle, valdebito, valcredito, estado, vigencia) VALUES ('$codigoComprobante $numeroFactura', '$cuentaDebito', '$cedulaId', '$cc', '$descripcion del servicio $servicio de la factura $numeroFactura', $valor, 0, '1', '$vigencia')";
                            mysqli_query($linkbd, $valuesCargoFijo);
                        }

                        //credito
                        if($cuentas[$i][3] == 'S' and $cuentas[$i][2] == 'N') {
                            
                            $cuentaCredito = $cuentas[$i][0];
        
                            $valuesCargoFijo = "INSERT INTO comprobante_det (id_comp, cuenta, tercero, centrocosto, detalle, valdebito, valcredito, estado, vigencia) VALUES ('$codigoComprobante $numeroFactura', '$cuentaCredito', '$cedulaId', '$cc', '$descripcion del servicio $servicio de la factura $numeroFactura', 0, $valor, '1', '$vigencia')";
                            mysqli_query($linkbd, $valuesCargoFijo);
                        }
                    }
                break;
                
                case 2:
                    $sqlServicio = "SELECT consumo_u, nombre, cc FROM srvservicios WHERE id = '$servicioId' ";
                    $rowServicio = mysqli_fetch_assoc(mysqli_query($linkbd, $sqlServicio));

                    $codigo = $rowServicio['consumo_u'];
                    $servicio = $rowServicio['nombre'];
                    $descripcion = "Consumo";
                    $valor = $credito;
                    $cuentas = concepto_cuentasn2($codigo,'CL',10,$cc,$fechaCorte);

                    for ($i=0; $i < count($cuentas); $i++) { 

                        //debito
                        if($cuentas[$i][3] == 'N' and $cuentas[$i][2] == 'S') {
                            
                            $cuentaDebito = $cuentas[$i][0];

                            $valuesConsumo = "INSERT INTO comprobante_det (id_comp, cuenta, tercero, centrocosto, detalle, valdebito, valcredito, estado, vigencia) VALUES ('$codigoComprobante $numeroFactura', '$cuentaDebito', '$cedulaId', '$cc', '$descripcion del servicio $servicio de la factura $numeroFactura', $valor, 0, '1', '$vigencia')";
                            mysqli_query($linkbd, $valuesConsumo);
                        }

                        //credito
                        if($cuentas[$i][3] == 'S' and $cuentas[$i][2] == 'N') {
                            
                            $cuentaCredito = $cuentas[$i][0];
        
                            $valuesConsumo = "INSERT INTO comprobante_det (id_comp, cuenta, tercero, centrocosto, detalle, valdebito, valcredito, estado, vigencia) VALUES ('$codigoComprobante $numeroFactura', '$cuentaCredito', '$cedulaId', '$cc', '$descripcion del servicio $servicio de la factura $numeroFactura', 0, $valor, '1', '$vigencia')";
                            mysqli_query($linkbd, $valuesConsumo);
                        }
                    }
                break;

                case 3: 
                    $sqlServicio = "SELECT subsidio_cf_u, nombre FROM srvservicios WHERE id = '$servicioId' ";
                    $rowServicio = mysqli_fetch_assoc(mysqli_query($linkbd, $sqlServicio));

                    $codigo = $rowServicio['subsidio_cf_u'];
                    $servicio = $rowServicio['nombre'];
                    $descripcion = "Subsidio cargo fijo";
                    $valor = $debito;
                    $cuentas = concepto_cuentasn2($codigo,'SB',10,$cc,$fechaCorte);
                    
                    for ($i=0; $i < count($cuentas); $i++) { 

                        //debito
                        if($cuentas[$i][3] == 'N' and $cuentas[$i][2] == 'S') {
                            
                            $cuentaDebito = $cuentas[$i][0];

                            $valuesConsumo = "INSERT INTO comprobante_det (id_comp, cuenta, tercero, centrocosto, detalle, valdebito, valcredito, estado, vigencia) VALUES ('$codigoComprobante $numeroFactura', '$cuentaDebito', '$cedulaId', '$cc', '$descripcion del servicio $servicio de la factura $numeroFactura', $valor, 0, '1', '$vigencia')";
                            mysqli_query($linkbd, $valuesConsumo);
                        }

                        //credito
                        if($cuentas[$i][3] == 'S' and $cuentas[$i][2] == 'N') {
                            
                            $cuentaCredito = $cuentas[$i][0];
        
                            $valuesConsumo = "INSERT INTO comprobante_det (id_comp, cuenta, tercero, centrocosto, detalle, valdebito, valcredito, estado, vigencia) VALUES ('$codigoComprobante $numeroFactura', '$cuentaCredito', '$cedulaId', '$cc', '$descripcion del servicio $servicio de la factura $numeroFactura', 0, $valor, '1', '$vigencia')";
                            mysqli_query($linkbd, $valuesConsumo);
                        }
                    }
                break;

                case 4:
                    $sqlServicio = "SELECT subsidio_cs_u, nombre FROM srvservicios WHERE id = '$servicioId' ";
                    $rowServicio = mysqli_fetch_assoc(mysqli_query($linkbd, $sqlServicio));

                    $codigo = $rowServicio['subsidio_cs_u'];
                    $servicio = $rowServicio['nombre'];
                    $descripcion = "Subsidio consumo";
                    $valor = $debito;
                    $cuentas = concepto_cuentasn2($codigo,'SB',10,$cc,$fechaCorte);

                    for ($i=0; $i < count($cuentas); $i++) { 

                        //debito
                        if($cuentas[$i][3] == 'N' and $cuentas[$i][2] == 'S') {
                            
                            $cuentaDebito = $cuentas[$i][0];

                            $valuesConsumo = "INSERT INTO comprobante_det (id_comp, cuenta, tercero, centrocosto, detalle, valdebito, valcredito, estado, vigencia) VALUES ('$codigoComprobante $numeroFactura', '$cuentaDebito', '$cedulaId', '$cc', '$descripcion del servicio $servicio de la factura $numeroFactura', $valor, 0, '1', '$vigencia')";
                            mysqli_query($linkbd, $valuesConsumo);
                        }

                        //credito
                        if($cuentas[$i][3] == 'S' and $cuentas[$i][2] == 'N') {
                            
                            $cuentaCredito = $cuentas[$i][0];
        
                            $valuesConsumo = "INSERT INTO comprobante_det (id_comp, cuenta, tercero, centrocosto, detalle, valdebito, valcredito, estado, vigencia) VALUES ('$codigoComprobante $numeroFactura', '$cuentaCredito', '$cedulaId', '$cc', '$descripcion del servicio $servicio de la factura $numeroFactura', 0, $valor, '1', '$vigencia')";
                            mysqli_query($linkbd, $valuesConsumo);
                        }
                    }
            break;

            case 5:
                $sqlServicio = "SELECT contribucion_u, nombre FROM srvservicios WHERE id = '$servicioId' ";
                $rowServicio = mysqli_fetch_assoc(mysqli_query($linkbd, $sqlServicio));

                $codigo = $rowServicio['contribucion_u'];
                $servicio = $rowServicio['nombre'];
                $descripcion = "Contribución";
                $valor = $credito;
                $cuentas = concepto_cuentasn2($codigo,'SC',10,$cc,$fechaCorte);

                for ($i=0; $i < count($cuentas); $i++) { 

                    //debito
                    if($cuentas[$i][3] == 'N' and $cuentas[$i][2] == 'S') {
                        
                        $cuentaDebito = $cuentas[$i][0];

                        $valuesConsumo = "INSERT INTO comprobante_det (id_comp, cuenta, tercero, centrocosto, detalle, valdebito, valcredito, estado, vigencia) VALUES ('$codigoComprobante $numeroFactura', '$cuentaDebito', '$cedulaId', '$cc', '$descripcion del servicio $servicio de la factura $numeroFactura', $valor, 0, '1', '$vigencia')";
                        mysqli_query($linkbd, $valuesConsumo);
                    }

                    //credito
                    if($cuentas[$i][3] == 'S' and $cuentas[$i][2] == 'N') {
                        
                        $cuentaCredito = $cuentas[$i][0];
    
                        $valuesConsumo = "INSERT INTO comprobante_det (id_comp, cuenta, tercero, centrocosto, detalle, valdebito, valcredito, estado, vigencia) VALUES ('$codigoComprobante $numeroFactura', '$cuentaCredito', '$cedulaId', '$cc', '$descripcion del servicio $servicio de la factura $numeroFactura', 0, $valor, '1', '$vigencia')";
                        mysqli_query($linkbd, $valuesConsumo);
                    }
                }
            break;

            /* case 10:
                $sqlServicio = "SELECT interes_moratorio, nombre FROM srvservicios WHERE id = '$servicioId' ";
                $rowServicio = mysqli_fetch_assoc(mysqli_query($linkbd, $sqlServicio));

                $codigo = $rowServicio['interes_moratorio'];
                $servicio = $rowServicio['nombre'];
                $descripcion = "Interes moratorio";
                $valor = $credito; 
                $cuentas = concepto_cuentasn2($codigo,'SM',10,$cc,$fechaCorte);

                for ($i=0; $i < count($cuentas); $i++) { 

                    //debito
                    if($cuentas[$i][3] == 'N' and $cuentas[$i][2] == 'S') {
                        
                        $cuentaDebito = $cuentas[$i][0];

                        $valuesConsumo = "INSERT INTO comprobante_det (id_comp, cuenta, tercero, centrocosto, detalle, valdebito, valcredito, estado, vigencia) VALUES ('$codigoComprobante $numeroFactura', '$cuentaDebito', '$cedulaId', '$cc', '$descripcion del servicio $servicio de la factura $numeroFactura', $valor, 0, '1', '$vigencia')";
                        mysqli_query($linkbd, $valuesConsumo);
                    }

                    //credito
                    if($cuentas[$i][3] == 'S' and $cuentas[$i][2] == 'N') {
                        
                        $cuentaCredito = $cuentas[$i][0];
    
                        $valuesCons umo = "INSERT INTO comprobante_det (id_comp, cuenta, tercero, centrocosto, detalle, valdebito, valcredito, estado, vigencia) VALUES ('$codigoComprobante $numeroFactura', '$cuentaCredito', '$cedulaId', '$cc', '$descripcion del servicio $servicio de la factura $numeroFactura', 0, $valor, '1', '$vigencia')";
                        mysqli_query($linkbd, $valuesConsumo);
                    }
                }
            break; */

                default:
                    # code...
                    break;
            }
        }
        
        $out['guardado'] = true;
        mysqli_close($linkbd);
        mysqli_free_result($resDetalleCorte);
        mysqli_free_result($resDetalleFacturacion);
        
    }

    header("Content-type: application/json");
    echo json_encode($out);
    die();