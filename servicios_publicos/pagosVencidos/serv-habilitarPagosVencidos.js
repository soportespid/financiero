const URL = 'servicios_publicos/pagosVencidos/serv-habilitarPagosVencidos.php';
import { filtroEnArrayDeObjetos } from './../../funciones.js';

var app = new Vue({
    el: '#myapp',
	data:{

        error: '',
        loading: false, 
        facturaVencida: '',
        facturaDeuda: '',
    },

    mounted: function(){

        
    },

    methods: 
    {
        //Desglosar array
        toFormData: function(obj){
            var form_data = new FormData();
            for(var key in obj){
                form_data.append(key, obj[key]);
            }
            return form_data;
        },

        formatonumero: function(valor){
            return new Intl.NumberFormat("en-US", {style: "currency", currency: "USD"}).format(valor);
        },

        //Funcion que habilita facturada vencida solo que sea al corte anterior al actual y revisa que no tenga pagos
        habilitarFactura: function() {

            if (this.facturaVencida != "") {

                axios.post(URL+'?action=habilitar&id='+this.facturaVencida).then((response) => {
                    console.log(response.data);
                    switch (response.data.status) {
                        case "100":
                            Swal.fire("Habilitar exitoso", "La factura numero " + this.facturaVencida + " ha sido habilitado con exito", "success");    
                            this.facturaVencida = "";
                        break;
                    
                        case "101":
                            Swal.fire("Error", "La factura numero " + this.facturaVencida + " no es del periodo liquidado anterior, no se puede habilitar", "warning");    
                        break;

                        case "102":
                            Swal.fire("Error", "La factura numero " + this.facturaVencida + " no se encuentra vencida", "warning");    
                        break;

                        case "103":
                            Swal.fire("Error", "La factura numero " + this.facturaVencida + " se le encuentran pago registrado", "warning");    
                        break;

                        case "104":
                            Swal.fire("Error", "La factura numero " + this.facturaVencida + " se le encuentran acuerdo de pago registrado", "warning");    
                        break;

                        default:
                            Swal.fire("Error", "La factura numero" + this.facturaVencida + " no se pudo habilitar", "warning");    
                        break;
                    }
                });
            }
        },

        //funcion que borra deuda de factura comprobando si tiene pago la factura anterior
        eliminarDeuda: function(){

            if (this.facturaDeuda != "") {
                axios.post(URL+'?action=eliminar&id='+this.facturaDeuda).then((response) => {
                    console.log(response.data);
                    switch (response.data.status) {
                        case "100":
                            Swal.fire("Quitar deuda", "La factura numero " + this.facturaDeuda + " se le ha quitado la deuda", "success");    
                            this.facturaDeuda = "";
                        break;

                        case "101":
                            Swal.fire("Error", "La factura numero " + this.facturaDeuda + " no esta pendiente de pago", "warning");    
                        break;

                        case "102":
                            Swal.fire("Error", "La factura numero " + this.facturaDeuda + " tiene un pago pendiente en el periodo anterior, registra primero el pago y luego si quita la deuda", "warning");    
                        break;

                        case "103":
                            Swal.fire("Error", "La factura numero " + this.facturaDeuda + " se encuentran pagos registrado a esta factura", "warning");   
                        break;

                        case "104":
                            Swal.fire("Error", "La factura numero " + this.facturaDeuda + " se encuentran acuerdos de pago registrado a esta factura", "warning");   
                        break;
                    
                        default:
                            break;
                    }
                });
            }
        }
    },
});