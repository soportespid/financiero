<?php
    require_once '../../comun.inc';
    require '../../funciones.inc';
    require '../../funcionesSP.inc.php';
    session_start();

    $linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");

    $out = array('error' => false);

    $action = "show";

    if(isset($_GET['action'])){
        $action = $_GET['action'];
    }
    
    if ($action == "habilitar") {
        $factura = $_GET['id'];
       
        $sqlCorte = "SELECT numero_corte FROM srvcortes WHERE estado = 'S'";
        $rowCorte = mysqli_fetch_assoc(mysqli_query($linkbd, $sqlCorte));

        $corteAnt = $rowCorte["numero_corte"] - 1;

        $sqlPago = "SELECT * FROM srv_recaudo_factura WHERE numero_factura = $factura AND estado = 'ACTIVO'";
        $resPago = mysqli_query($linkbd, $sqlPago);
        $cantPago = mysqli_num_rows($resPago);

        $sqlAcuerdo = "SELECT * FROM srv_acuerdo_cab WHERE codigo_factura = $factura AND estado != 'Reversado'";
        $resAcuerdo = mysqli_query($linkbd, $sqlAcuerdo);
        $cantAcuerdo = mysqli_num_rows($resAcuerdo);

        $sqlCorteDet = "SELECT id_corte, estado_pago FROM srvcortes_detalle WHERE numero_facturacion = $factura";
        $rowCorteDet = mysqli_fetch_assoc(mysqli_query($linkbd, $sqlCorteDet));

        if ($rowCorteDet["id_corte"] == $corteAnt) {

            if ($rowCorteDet["estado_pago"] == 'V') {

                if ($cantPago == 0) {

                    if ($cantAcuerdo == 0) {

                        $sqlHabilita = "UPDATE srvcortes_detalle SET estado_pago = 'S' WHERE numero_facturacion = $factura";
                        mysqli_query($linkbd, $sqlHabilita);

                        $usuario = $_SESSION["usuario"];
                        $cedula = $_SESSION["cedulausu"];
                        $ip = getRealIP();
                        $hoy = date("Y-m-d H:i:s");

                        $sqlAuditoria = "INSERT INTO srv_auditoria (id_srv_funciones, accion, cedula, usuario, ip, fecha_hora, consecutivo) VALUES (7, 'Cambia estado', '$cedula', '$usuario', '$ip', '$hoy', $factura)";   
                        mysqli_query($linkbd, $sqlAuditoria);

                        //ok
                        $out["status"] = "100";
                    } 
                    else {
                        //se encuentran acuerdos registrados a esta factura
                        $out["status"] = "104";
                    }
                }
                else {
                    //se encuentran pagos registrados ya a esta factura
                    $out["status"] = "103";
                }
            }
            else {
                //error la factura no se encuentra en estado vencido
                $out["status"] = "102";
            }
        }
        else {
            //error solo puede habilitar facturas del corte anterior al que esta activo
            $out["status"] = "101";
        }
    }

    if ($action == "eliminar") {

        $factura = $_GET['id'];

        $sqlCorte = "SELECT numero_corte FROM srvcortes WHERE estado = 'S'";
        $rowCorte = mysqli_fetch_assoc(mysqli_query($linkbd, $sqlCorte));

        $corteAnt = $rowCorte["numero_corte"] - 1;

        $sqlCorteDet = "SELECT id_cliente, estado_pago FROM srvcortes_detalle WHERE numero_facturacion = $factura";
        $rowCorteDet = mysqli_fetch_assoc(mysqli_query($linkbd, $sqlCorteDet));

        $sqlFacturaAnterior = "SELECT estado_pago AS estadoPagoAnterior FROM srvcortes_detalle WHERE id_cliente = $rowCorteDet[id_cliente] AND id_corte = $corteAnt";
        $rowFacturaAnterior = mysqli_fetch_assoc(mysqli_query($linkbd, $sqlFacturaAnterior));

        $sqlPago = "SELECT * FROM srv_recaudo_factura WHERE numero_factura = $factura AND estado = 'ACTIVO'";
        $resPago = mysqli_query($linkbd, $sqlPago);
        $cantPago = mysqli_num_rows($resPago);

        $sqlAcuerdo = "SELECT * FROM srv_acuerdo_cab WHERE codigo_factura = $factura AND estado != 'Reversado'";
        $resAcuerdo = mysqli_query($linkbd, $sqlAcuerdo);
        $cantAcuerdo = mysqli_num_rows($resAcuerdo);

        if ($rowCorteDet["estado_pago"] == 'S') {

            if ($rowFacturaAnterior["estadoPagoAnterior"] == "P") {

                if ($cantPago == 0) {

                    if ($cantAcuerdo == 0) {

                        //para borrar la deuda y los intereses de una factura deben verse afectadas dos tablas, la primera srvdetalles_facturacion donde los tipos de cobro 8 que es de deuda y tipo de cobro 10 que es de intereses se deben colocar en 0 el valor en credito
                        $sqlDetallesUpdate = "UPDATE srvdetalles_facturacion SET credito = 0 WHERE numero_facturacion = $factura AND tipo_movimiento = '101' AND (id_tipo_cobro = 8 OR id_tipo_cobro = 10)";
                        mysqli_query($linkbd, $sqlDetallesUpdate);

                        //la otra tabla a afectar es la srvfacturas donde ahí afectaremos los campos deuda_anterior y interes_mora donde los dejaremos en 0 cuando sea el numero de factura indicada
                        $sqlFacturas = "UPDATE srvfacturas SET deuda_anterior = 0, interes_mora = 0 WHERE num_factura = $factura";
                        mysqli_query($linkbd, $sqlFacturas);

                        $usuario = $_SESSION["usuario"];
                        $cedula = $_SESSION["cedulausu"];
                        $ip = getRealIP();
                        $hoy = date("Y-m-d H:i:s");

                        $sqlAuditoria = "INSERT INTO srv_auditoria (id_srv_funciones, accion, cedula, usuario, ip, fecha_hora, consecutivo) VALUES (7, 'Elimina deuda', '$cedula', '$usuario', '$ip', '$hoy', $factura)";
                        mysqli_query($linkbd, $sqlAuditoria);

                        $out["status"] = "100";
                    }
                    else {
                        //Se encuentran acuerdos de pago registrados a esta factura
                        $out["status"] = "104";
                    }
                }
                else {
                    //se encuentran pagos registrados de esta factura y no deberia tener pagos ya
                    $out["status"] = "103";    
                }
            }
            else {
                //la factura anterior no esta paga y no se le pueda quitar la deuda a esta factura
                $out["status"] = "102";    
            }
        }
        else {
            //estado de pago no esta pendiente de pago
            $out["status"] = "101";
        }
    }

    header("Content-type: application/json");
    echo json_encode($out);
    die();