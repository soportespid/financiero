const URL = "servicios_publicos/reportes/controllers/recaudoConceptos_controller.php";
const URLPDF = "serv-informeRecaudosConceptosPdf.php";
var app = new Vue({
    el:"#myapp",
    data() {
        return {
            isLoading: false,
            data: [],
            fechaIni: '',
            fechaFin: '',
            servicios: [],
            servicioId: '',
        }
    },
    mounted() {
        this.get();
    },
    methods: {
        /* metodos para traer información */
        async get() {
            const formData = new FormData();
            formData.append("action","get");
            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();
            this.servicios = objData.servicios;
        },

        async searchData() {
            if (this.servicioId == "" || this.fechaIni == "" || this.fechaFin == "") {
                Swal.fire("Atención!","Todos los campos con * son obligatorios","warning");
                return false;
            }

            if (this.fechaIni > this.fechaFin) {
                Swal.fire("Atención!","Fecha inicial no puede ser mayor que la fecha fecha final","warning");
                return false;
            }

            const formData = new FormData();
            formData.append("action","searchData");
            formData.append("servicioId",this.servicioId);
            formData.append("fechaIni",this.fechaIni);
            formData.append("fechaFin",this.fechaFin);
            this.isLoading = true;
            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();
            this.data = objData.data;
            this.isLoading = false;
        },

        /* metodos para procesar información */

        pdf: function () {    
            const form = document.createElement("form");
            form.method = "post";
            form.target = "_blank";
            form.action = URLPDF;       
            
            function addField(name,value){
                const input = document.createElement("input");
                input.type="hidden";
                input.name=name;
                input.value = value;
                form.appendChild(input);
            }

            addField("servicioId", this.servicioId);
            addField("fechaIni", this.fechaIni);
            addField("fechaFin", this.fechaFin);
            addField("data", JSON.stringify(this.data));
        
            document.body.appendChild(form);
            form.submit();
            document.body.removeChild(form);
        },
        
        /* Metodos para guardar o actualizar información */
       
        /* Formatos para mostrar información */
        formatNumero: function(valor){
            return new Intl.NumberFormat("en-US", {style: "currency", currency: "USD"}).format(valor);      
        },
    },
    computed:{

    }
})
