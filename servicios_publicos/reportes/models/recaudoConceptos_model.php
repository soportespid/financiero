<?php
    require_once '../../../Librerias/core/Helpers.php';
    require_once '../../../Librerias/core/Mysql.php';
    session_start();
    
    class conceptosModel extends Mysql {
        public function servicios() {
            $sql = "SELECT id, nombre FROM srvservicios WHERE estado = 'S' ORDER BY id";
            $data = $this->select_all($sql);
            return $data;
        }

        public function searchValuesConcepts(int $servicioId, string $fechaIni, string $fechaFin) {
            $sql = "SELECT
            SUM(SF.cargo_f) - SUM(SF.subsidio_cf) as total_cargo_fijo,
            SUM(SF.consumo_b + SF.consumo_c + SF.consumo_s) - SUM(SF.subsidio_cb + SF.subsidio_cc + SF.subsidio_cs) AS total_consumo,
            SUM(SF.contribucion_cf + SF.contribucion_cb + SF.contribucion_cc + SF.contribucion_cs) AS total_contribucion,
            SUM(SF.venta_medidor) AS total_otros_cobros,
            SUM(SF.interes_mora) AS total_intereses,
            SUM(SF.desincentivo) AS total_desincentivo
            FROM srv_recaudo_factura AS SRF
            INNER JOIN srvfacturas AS SF
            ON SRF.numero_factura = SF.num_factura
            WHERE SRF.fecha_recaudo BETWEEN '$fechaIni' AND '$fechaFin' AND SF.id_servicio = $servicioId";
            $data = $this->select_all($sql);
            return $data;
        }
    }
?>