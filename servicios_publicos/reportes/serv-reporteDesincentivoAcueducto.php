<?php
    require_once '../../comun.inc';
    require '../../funciones.inc';
    require '../../funcionesSP.inc.php';
    session_start();

    $linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");

    $out = array('error' => false);

    $action = "show";

    if(isset($_GET['action'])){
        $action = $_GET['action'];
    }
    
    if ($action == "inicial") {
        $peridosLiquidados = array();

        $sql = "SET lc_time_names = 'es_ES'";
        mysqli_query($linkbd,$sql);

        $sqlCortes = "SELECT numero_corte, UPPER(MONTHNAME(fecha_inicial)), UPPER(MONTHNAME(fecha_final)), YEAR(fecha_inicial), YEAR(fecha_final), fecha_inicial, fecha_final, fecha_impresion FROM srvcortes WHERE numero_corte > 0 ORDER BY numero_corte DESC";
        $resCortes = mysqli_query($linkbd, $sqlCortes);
        while ($rowCortes = mysqli_fetch_row($resCortes)) {

            $datos = array();

            $descripcion = $rowCortes[1] . " " . $rowCortes[3] . " - " . $rowCortes[2] . " " . $rowCortes[4];
            
            array_push($datos, $rowCortes[0]);
            array_push($datos, $descripcion);
            array_push($datos, $rowCortes[5]);
            array_push($datos, $rowCortes[6]);
            array_push($datos, $rowCortes[7]);
            array_push($peridosLiquidados, $datos);
        }

        $out['periodosLiquidados'] = $peridosLiquidados;
    }

    if ($action == "searchData") {
        $periodoId = $_GET['id'];
        $datos = [];
        $facturado = $recaudado = $valorPorRecaudar = 0;

        //Consulta la tabla srvfacturas y consulta la columna desincentivo filtrado por la columna id_servicio y corte de aquí saldra el valor facturado del desicentivo
        $sqlFacturas = "SELECT SUM(desincentivo) AS desincentivo FROM srvfacturas WHERE corte = $periodoId AND id_servicio = 1";
        $resFacturas = mysqli_query($linkbd, $sqlFacturas);
        $rowFacturas = mysqli_fetch_assoc($resFacturas);

        $facturado = (float) $rowFacturas["desincentivo"];

        //Consulta facturas pagas del periodo en srvcortes_detalle y va y consulta cuanto es el valor de desincentivo pagado por estas facturas
        $sqlCorteDet = "SELECT numero_facturacion AS numFactura FROM srvcortes_detalle WHERE id_corte = $periodoId AND estado_pago = 'P'";
        $resCorteDet = mysqli_query($linkbd, $sqlCorteDet);
        while ($rowCorteDet = mysqli_fetch_assoc($resCorteDet)) {
            
            $sqlPago = "SELECT desincentivo FROM srvfacturas WHERE corte = $periodoId AND id_servicio = 1 AND num_factura = $rowCorteDet[numFactura]";
            $resPago = mysqli_query($linkbd, $sqlPago);
            $rowPago = mysqli_fetch_assoc($resPago);

            $recaudado += $rowPago["desincentivo"];
        }

        //Calcula cuanto valor falta por recaudar
        $valorPorRecaudar = $facturado - $recaudado;

        //Guardo datos en array que luego cargo a un array in array para mostrar en el frontend
        $datosTemp = [];
        $datosTemp[] = 1;
        $datosTemp[] = "Acueducto"; 
        $datosTemp[] = $facturado;
        $datosTemp[] = $recaudado;
        $datosTemp[] = $valorPorRecaudar;
        array_push($datos, $datosTemp);

        $out["datos"] = $datos;
    }

    header("Content-type: application/json");
    echo json_encode($out);
    die();