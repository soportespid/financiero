<?php
    require_once '../../comun.inc';
    require '../../funciones.inc';
    require '../../funcionesSP.inc.php';
    session_start();

    $linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");

    $out = array('error' => false);

    $action = "show";

    if(isset($_GET['action'])){
        $action = $_GET['action'];
    }
    
    if ($action == "inicial") {
        $peridosLiquidados = array();

        $sql = "SET lc_time_names = 'es_ES'";
        mysqli_query($linkbd,$sql);

        $sqlCortes = "SELECT numero_corte, UPPER(MONTHNAME(fecha_inicial)), UPPER(MONTHNAME(fecha_final)), YEAR(fecha_inicial), YEAR(fecha_final), fecha_inicial, fecha_final, fecha_impresion FROM srvcortes WHERE numero_corte > 0 ORDER BY numero_corte DESC";
        $resCortes = mysqli_query($linkbd, $sqlCortes);
        while ($rowCortes = mysqli_fetch_row($resCortes)) {

            $datos = array();

            $descripcion = $rowCortes[1] . " " . $rowCortes[3] . " - " . $rowCortes[2] . " " . $rowCortes[4];
            
            array_push($datos, $rowCortes[0]);
            array_push($datos, $descripcion);
            array_push($datos, $rowCortes[5]);
            array_push($datos, $rowCortes[6]);
            array_push($datos, $rowCortes[7]);
            array_push($peridosLiquidados, $datos);
        }

        $out['periodosLiquidados'] = $peridosLiquidados;
    }

    if ($action == "searchData") {
        $periodoId = $_GET['id'];
        $recaudos = [];

        //Busca facturas con recaudo
        $sqlRecaudo = "SELECT RF.codigo_recaudo, RF.numero_factura, RF.fecha_recaudo, RF.medio_pago, RF.valor_pago, RF.cod_usuario, RF.suscriptor FROM srv_recaudo_factura AS RF, srvcortes_detalle AS CD WHERE CD.id_corte = $periodoId AND CD.estado_pago = 'P' AND CD.numero_facturacion = RF.numero_factura ORDER BY RF.fecha_recaudo ASC";
        $resRecaudo = mysqli_query($linkbd, $sqlRecaudo);
        while ($rowRecaudo = mysqli_fetch_row($resRecaudo)) {
            $data = [];
          
            //Valor de este periodo
            $sqlPeriodo = "SELECT COALESCE(SUM(credito),0), COALESCE(SUM(debito),0) FROM srvdetalles_facturacion WHERE numero_facturacion = $rowRecaudo[1] AND tipo_movimiento = '101' AND id_tipo_cobro IN (1,2,3,4,5,9,10)";
            $rowPeriodo = mysqli_fetch_row(mysqli_query($linkbd, $sqlPeriodo));

            $valorMes = $rowPeriodo[0] - $rowPeriodo[1];

            //Deuda
            $sqlDeuda = "SELECT COALESCE(SUM(credito),0), COALESCE(SUM(debito),0) FROM srvdetalles_facturacion WHERE numero_facturacion = $rowRecaudo[1] AND tipo_movimiento = '101' AND id_tipo_cobro IN (7, 8)";
            $rowDeuda = mysqli_fetch_row(mysqli_query($linkbd, $sqlDeuda));

            $deuda = $rowDeuda[0] - $rowDeuda[1];

            $data[] = "Recaudo";
            $data[] = $rowRecaudo[0];
            $data[] = $rowRecaudo[1];
            $data[] = $rowRecaudo[2];
            $data[] = $rowRecaudo[3];
            $data[] = $valorMes;
            $data[] = $deuda;
            $data[] = $rowRecaudo[4];
            $data[] = $rowRecaudo[5];
            $data[] = $rowRecaudo[6];
            array_push($recaudos, $data);
        }

        $sqlAcuerdo = "SELECT AP.codigo_acuerdo, AP.codigo_factura, AP.fecha_acuerdo, AP.medio_pago, AP.valor_abonado, AP.cod_usuario, AP.nombre_suscriptor FROM srv_acuerdo_cab AS AP, srvcortes_detalle AS CD WHERE CD.id_corte = $periodoId AND CD.estado_pago = 'A' AND CD.numero_facturacion = AP.codigo_factura ORDER BY AP.fecha_acuerdo ASC";
        $resAcuerdo = mysqli_query($linkbd, $sqlAcuerdo);
        while ($rowAcuerdo = mysqli_fetch_row($resAcuerdo)) {

            $data = [];

            $data[] = "Acuerdo";
            $data[] = $rowAcuerdo[0];
            $data[] = $rowAcuerdo[1];
            $data[] = $rowAcuerdo[2];
            $data[] = $rowAcuerdo[3];
            $data[] = 0;
            $data[] = $rowAcuerdo[4];
            $data[] = $rowAcuerdo[4];
            $data[] = $rowAcuerdo[5];
            $data[] = $rowAcuerdo[6];
            array_push($recaudos, $data);
        }

        array_multisort(array_column($recaudos, 3), SORT_ASC, $recaudos);

        $out['recaudos'] = $recaudos;
    }

    header("Content-type: application/json");
    echo json_encode($out);
    die();