<?php
    require_once '../../comun.inc';
    require '../../funciones.inc';
    require '../../funcionesSP.inc.php';
    session_start();

    $linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");

    $out = array('error' => false);

    $action = "show";

    if(isset($_GET['action'])){
        $action = $_GET['action'];
    }
    
    if ($action == "inicial") {
        $peridosLiquidados = array();

        $sql = "SET lc_time_names = 'es_ES'";
        mysqli_query($linkbd,$sql);

        $sqlCortes = "SELECT numero_corte, UPPER(MONTHNAME(fecha_inicial)), UPPER(MONTHNAME(fecha_final)), YEAR(fecha_inicial), YEAR(fecha_final), fecha_inicial, fecha_final, fecha_impresion FROM srvcortes WHERE numero_corte > 0 ORDER BY numero_corte DESC";
        $resCortes = mysqli_query($linkbd, $sqlCortes);
        while ($rowCortes = mysqli_fetch_row($resCortes)) {

            $datos = array();

            $descripcion = $rowCortes[1] . " " . $rowCortes[3] . " - " . $rowCortes[2] . " " . $rowCortes[4];
            
            array_push($datos, $rowCortes[0]);
            array_push($datos, $descripcion);
            array_push($datos, $rowCortes[5]);
            array_push($datos, $rowCortes[6]);
            array_push($datos, $rowCortes[7]);
            array_push($peridosLiquidados, $datos);
        }

        $out['periodosLiquidados'] = $peridosLiquidados;
    }

    if ($action == "searchData") {
        $periodoId = $_GET['id'];
        $recaudos = [];
        $totales = [];
        $sqlServ = "SELECT id, nombre FROM srvservicios WHERE estado = 'S' ORDER BY id ASC";
        $resServ = mysqli_query($linkbd, $sqlServ);
        $cant = mysqli_num_rows($resServ);
        $totalPagado = $totalSumConsumo = $totalSumDeuda =  0;
        while ($rowServ = mysqli_fetch_row($resServ)) {
            $totalValorMes = $totalDeuda = $total = 0;
            $datos = [];

            //Busca facturas con recaudo
            $sqlRecaudo = "SELECT RF.codigo_recaudo, RF.numero_factura, RF.fecha_recaudo, RF.medio_pago, RF.valor_pago FROM srv_recaudo_factura AS RF, srvcortes_detalle AS CD WHERE CD.id_corte = $periodoId AND CD.estado_pago = 'P' AND CD.numero_facturacion = RF.numero_factura ORDER BY RF.fecha_recaudo ASC";
            $resRecaudo = mysqli_query($linkbd, $sqlRecaudo);
            while ($rowRecaudo = mysqli_fetch_row($resRecaudo)) {
            
                //Valor de este periodo
                $sqlPeriodo = "SELECT COALESCE(SUM(credito),0), COALESCE(SUM(debito),0) FROM srvdetalles_facturacion WHERE numero_facturacion = $rowRecaudo[1] AND tipo_movimiento = '101' AND id_servicio = $rowServ[0] AND id_tipo_cobro IN (1,2,3,4,5,9,10)";
                $rowPeriodo = mysqli_fetch_row(mysqli_query($linkbd, $sqlPeriodo));

                $valorMes = $rowPeriodo[0] - $rowPeriodo[1];
                $totalValorMes += $valorMes;

                //Deuda
                $sqlDeuda = "SELECT COALESCE(SUM(credito),0), COALESCE(SUM(debito),0) FROM srvdetalles_facturacion WHERE numero_facturacion = $rowRecaudo[1] AND tipo_movimiento = '101' AND id_servicio = $rowServ[0] AND id_tipo_cobro IN (7, 8)";
                $rowDeuda = mysqli_fetch_row(mysqli_query($linkbd, $sqlDeuda));

                $deuda = $rowDeuda[0] - $rowDeuda[1];

                $totalDeuda += $deuda;

                if ($rowServ[0] == 1) {
                    $totalPagado += $rowRecaudo[4];
                }
            }

            $sqlAcuerdo = "SELECT AP.codigo_acuerdo, AP.codigo_factura, AP.fecha_acuerdo, AP.medio_pago, AP.valor_abonado FROM srv_acuerdo_cab AS AP, srvcortes_detalle AS CD WHERE CD.id_corte = $periodoId AND CD.estado_pago = 'A' AND CD.numero_facturacion = AP.codigo_factura ORDER BY AP.fecha_acuerdo ASC";
            $resAcuerdo = mysqli_query($linkbd, $sqlAcuerdo);
            while ($rowAcuerdo = mysqli_fetch_row($resAcuerdo)) {

                $deuda = round($rowAcuerdo[4] / $cant, 2);
                $totalDeuda += $deuda;
                if ($rowServ[0] == 1) {
                    $totalPagado += $rowAcuerdo[4];
                }
            }

            $total = $totalValorMes + $totalDeuda;
            $totalSumConsumo += $totalValorMes;
            $totalSumDeuda += $totalDeuda;

            $datos[] = $rowServ[0];
            $datos[] = $rowServ[1];
            $datos[] = $totalValorMes;
            $datos[] = $totalDeuda;
            $datos[] = $total;
            array_push($recaudos, $datos);
        }
        $data = [];
        $data[] = $totalSumConsumo;
        $data[] = $totalSumDeuda;
        $data[] = $totalPagado;
        $data[] = "";
        $data[] = "";
        array_push($totales, $data);

        $out['recaudos'] = $recaudos;
        $out['totales'] = $totales;
    }

    header("Content-type: application/json");
    echo json_encode($out);
    die();