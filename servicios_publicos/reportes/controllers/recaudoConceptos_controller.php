<?php
    require_once '../../../Librerias/core/Helpers.php';
    require_once '../models/recaudoConceptos_model.php';
    session_start();
    header('Content-Type: application/json');

    class conceptosController extends conceptosModel {
        public function get() {
            if (!empty($_SESSION)) {
                $arrResponse = array("servicios" => $this->servicios());
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
            die();
        }

        public function getData() {
            if (!empty($_SESSION)) {
                $servicioId = (int) $_POST["servicioId"];
                $fechaIni = $_POST["fechaIni"];
                $fechaFin = $_POST["fechaFin"];
                $arrResponse = array("data" => $this->searchValuesConcepts($servicioId, $fechaIni, $fechaFin));
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
            die();
        }
    }

    if($_POST){
        $obj = new conceptosController();
        $accion = $_POST["action"];
        if ($accion == 'get') {
            $obj->get();
        } else if ($accion == "searchData") {
            $obj->getData();
        }
    }
?>