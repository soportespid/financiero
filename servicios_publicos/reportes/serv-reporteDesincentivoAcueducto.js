const URL = 'servicios_publicos/reportes/serv-reporteDesincentivoAcueducto.php';
import { filtroEnArrayDeObjetos } from './../../funciones.js';

var app = new Vue({
    el: '#myapp',
	data:{

        error: '',
        loading: false, 
        datosPeriodosLiquidados: [],
        periodo: '',
        descripcionPeriodo: '',
        showModalLiquidaciones: false,
        datos: [],
    },

    mounted: function(){

        this.datosIniciales();
    },

    methods: 
    {
        //Desglosar array
        toFormData: function(obj){
            var form_data = new FormData();
            for(var key in obj){
                form_data.append(key, obj[key]);
            }
            return form_data;
        },

        formatonumero: function(valor){
            return new Intl.NumberFormat("en-US", {style: "currency", currency: "USD"}).format(valor);
        },

        datosIniciales: async function() {
            
            await axios.post(URL+'?action=inicial')
            .then((response) => {
                this.datosPeriodosLiquidados = response.data.periodosLiquidados;
            });
        },

        modalLiquidaciones: function() {
            this.showModalLiquidaciones = true;
        },

        seleccionaPeriodo: function(periodo) {
            this.periodo = periodo[0];
            this.descripcionPeriodo = periodo[1];
            this.showModalLiquidaciones = false;
        },

        buscaDatos: async function() {
            const periodoId = this.periodo;

            if (periodoId != "") {
                
                this.loading = true;
                await axios.post(URL+'?action=searchData&id='+periodoId)
                .then((response) => {
                    console.log(response.data);
                    this.datos = response.data.datos;
                });
                this.loading = false;
            }
        },

        excel: function() {

            if (this.datos != '') {

            //     document.form2.action="serv-reporteRecaudosPorServicioExcel.php";
			// 	document.form2.target="_BLANK";
			// 	document.form2.submit(); 
            }
            else {
                Swal.fire("Faltan datos", "Generar lecturas primero", "warning");
            }
        },

        printPDF:function(){
            window.open('serv-reporteDesincentivoAcPDF.php?datos='+this.datos+'&corte='+this.descripcionPeriodo,'_blank');
        },
    },
});