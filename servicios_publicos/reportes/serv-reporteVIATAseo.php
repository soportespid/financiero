<?php
    require_once '../../comun.inc';
    require '../../funciones.inc';
    require '../../funcionesSP.inc.php';
    session_start();

    $linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");

    $out = array('error' => false);

    $action = "show";

    if(isset($_GET['action'])){
        $action = $_GET['action'];
    }
    
    if ($action == "inicial") {
        $peridosLiquidados = array();

        $sql = "SET lc_time_names = 'es_ES'";
        mysqli_query($linkbd,$sql);

        $sqlCortes = "SELECT numero_corte, UPPER(MONTHNAME(fecha_inicial)), UPPER(MONTHNAME(fecha_final)), YEAR(fecha_inicial), YEAR(fecha_final), fecha_inicial, fecha_final, fecha_impresion FROM srvcortes WHERE numero_corte > 0 ORDER BY numero_corte DESC";
        $resCortes = mysqli_query($linkbd, $sqlCortes);
        while ($rowCortes = mysqli_fetch_row($resCortes)) {

            $datos = array();

            $descripcion = $rowCortes[1] . " " . $rowCortes[3] . " - " . $rowCortes[2] . " " . $rowCortes[4];
            
            array_push($datos, $rowCortes[0]);
            array_push($datos, $descripcion);
            array_push($datos, $rowCortes[5]);
            array_push($datos, $rowCortes[6]);
            array_push($datos, $rowCortes[7]);
            array_push($peridosLiquidados, $datos);
        }

        $out['periodosLiquidados'] = $peridosLiquidados;
    }

    if ($action == "searchData") {
        $periodoId = $_GET['id'];
        $datos = [];
        $facturado = $recaudado = $valorPorRecaudar = 0;
        $version = 0;

        //Busca la version de tarifas que se esta usando en ese periodo
        $sqlVersion = "SELECT version_tarifa FROM srvcortes WHERE numero_corte = $periodoId";
        $rowVersion = mysqli_fetch_assoc(mysqli_query($linkbd, $sqlVersion));
        $version = $rowVersion["version_tarifa"];

        //Consulta la tabla de datos de aseo y busca la columna viat para comar el valor de ella
        $sqlViat = "SELECT viat FROM srv_aseo WHERE version = $version";
        $rowViat = mysqli_fetch_assoc(mysqli_query($linkbd, $sqlViat));
        $viat = $rowViat["viat"];

        //Consulta tabla para buscar lo facturado en el consumo basico en el servicio de aseo, de este valor solo corresponde un valor en especifico que es el VIAT
        $sqlFacturas = "SELECT consumo_b FROM srvfacturas WHERE corte = $periodoId AND id_servicio = 3 AND consumo_b > 0";
        $resFacturas = mysqli_query($linkbd, $sqlFacturas);
        $cantAseo = mysqli_num_rows($resFacturas);
        
        $facturado = $viat * $cantAseo;

        //Consulta facturas pagas del periodo en srvcortes_detalle y va y consulta cuanto es el valor de consumo_b pagado por estas facturas y calcula con esta el valor VIAT
        $sqlCorteDet = "SELECT numero_facturacion AS numFactura FROM srvcortes_detalle WHERE id_corte = $periodoId AND estado_pago = 'P'";
        $resCorteDet = mysqli_query($linkbd, $sqlCorteDet);
        while ($rowCorteDet = mysqli_fetch_assoc($resCorteDet)) {
            
            $sqlPago = "SELECT consumo_b FROM srvfacturas WHERE corte = $periodoId AND id_servicio = 3 AND num_factura = $rowCorteDet[numFactura] AND consumo_b > 0";
            $resPago = mysqli_query($linkbd, $sqlPago);
            $cant = mysqli_num_rows($resPago);

            if ($cant > 0) {
                $recaudado += $viat;
            }
        }

        //Calcula cuanto valor falta por recaudar
        $valorPorRecaudar = $facturado - $recaudado;

        $datosTemp = [];
        $datosTemp[] = 3;
        $datosTemp[] = "Aseo"; 
        $datosTemp[] = $facturado;
        $datosTemp[] = $recaudado;
        $datosTemp[] = $valorPorRecaudar;
        array_push($datos, $datosTemp);

        $out["datos"] = $datos;
    }

    header("Content-type: application/json");
    echo json_encode($out);
    die();