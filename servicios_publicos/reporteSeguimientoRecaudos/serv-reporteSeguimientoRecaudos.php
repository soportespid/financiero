<?php
    require_once '../../Librerias/core/Helpers.php';
    require_once '../../Librerias/core/Mysql.php';

    header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
    session_start();

    $out = array('error' => false);

    class recaudos extends Mysql {
        
        public function searchValueInteresServ($numFactura, $serviceId) {
            $valorIntereses = 0;

            $sqlValue = "SELECT SUM(credito) AS valueCredito FROM srvdetalles_facturacion WHERE numero_facturacion = $numFactura AND id_servicio = $serviceId AND tipo_movimiento = '101' AND id_tipo_cobro = 10";
            $value = $this->select($sqlValue);
            $valorIntereses = round($value["valueCredito"], 2);
    
            return $valorIntereses;
        }

        public function insertaCont($servicioId, $tipo, $fecha, $consecutivo, $valor, $cuentaDebito, $documento, $vigencia, $conceptoCobro, $porcentajeDescuento, $descripcion, $tipoComp) {

            $tipoComprobante = $tipoComp;
            
            if ($conceptoCobro != "interes_moratorio") {
                $sql_servicio = "SELECT $conceptoCobro, cc, nombre FROM srvservicios WHERE id = $servicioId";
                $servicio = $this->select($sql_servicio);
        
                $sql_concepto = "SELECT cuenta FROM conceptoscontables_det WHERE codigo = '$servicio[$conceptoCobro]' AND tipo = '$tipo' AND cc = '$servicio[cc]' AND modulo = 10 AND fechainicial <= '$fecha' AND debito = 'S'";
                $concepto = $this->select($sql_concepto);
        
                $cuentaCredito = $concepto["cuenta"];
                
                $sql = "INSERT INTO comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,valdebito,valcredito,estado,vigencia) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";

                $value_debito = ["$tipoComprobante $consecutivo", "$cuentaDebito", "$documento", "$servicio[cc]", "Pago de $descripcion de $servicio[nombre]", $valor, 0, "1", "$vigencia"];
                $value_credito = ["$tipoComprobante $consecutivo", "$cuentaCredito", "$documento", "$servicio[cc]", "Pago de $descripcion de $servicio[nombre]", 0, $valor, "1", "$vigencia"];

                $this->insert($sql, $value_debito);
                $this->insert($sql, $value_credito);
            }
            else {
                if ($porcentajeDescuento > 0) {
                    $porcentaje = $descuento = 0;
                    $porcentaje = $porcentajeDescuento / 100;
                    $descuento = $valor * $porcentaje;
                    $valor = $valor - $descuento;
                }   

                $sql_servicio = "SELECT $conceptoCobro, cc, nombre FROM srvservicios WHERE id = $servicioId";
                $servicio = $this->select($sql_servicio);

                $sql_concepto = "SELECT cuenta, debito, credito FROM conceptoscontables_det WHERE codigo = '$servicio[$conceptoCobro]' AND tipo = '$tipo' AND cc = '$servicio[cc]' AND modulo = 10 AND fechainicial <= '$fecha'";

                $conceptos = $this->select_all($sql_concepto);

                foreach ($conceptos as $key => $concepto) {
                    if ($concepto["debito"] == 'S') {
                        $cuentaCuatro = $concepto["cuenta"];
                    }
                    else if ($concepto["credito"] == 'S') {
                        $cuentaCredito = $concepto["cuenta"];
                    }
                }

                $sql = "INSERT INTO comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque,valdebito,valcredito,estado,vigencia) 
                VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

                $value_liquida_debito = ["$tipoComprobante $consecutivo", "$cuentaCuatro", "$documento", "$servicio[cc]", "Liquido de $descripcion de $servicio[nombre]", "", $valor, 0, "1", "$vigencia"];
                
                $value_liquida_credito = ["$tipoComprobante $consecutivo", "$cuentaCredito", "$documento", "$servicio[cc]", "Liquido de $descripcion de $servicio[nombre]", "", 0, $valor, "1", "$vigencia"];

                $value_debito = ["$tipoComprobante $consecutivo", "$cuentaDebito", "$documento", "$servicio[cc]", "Pago de $descripcion de $servicio[nombre]", "", $valor, 0, "1", "$vigencia"];
                
                $value_credito = ["$tipoComprobante $consecutivo", "$cuentaCuatro", "$documento", "$servicio[cc]", "Pago de $descripcion de $servicio[nombre]", "", 0, $valor, "1", "$vigencia"];

                $this->insert($sql, $value_liquida_debito);
                $this->insert($sql, $value_liquida_credito);
                $this->insert($sql, $value_debito);
                $this->insert($sql, $value_credito);
            }
        }

        public function searchData(){
            if(!empty($_SESSION)){
                $comprobanteRecaudo = 30;
                $comprobanteAcuerdo = 39;
                $fechaIni = $_POST["fechaIni"];
                $fechaFin = $_POST["fechaFin"];
                $data = [];

                $sqlRecaudos = "SELECT codigo_recaudo, fecha_recaudo, valor_pago FROM srv_recaudo_factura WHERE fecha_recaudo BETWEEN '$fechaIni' AND '$fechaFin' AND estado = 'ACTIVO'";
                $recaudos = $this->select_all($sqlRecaudos);
                $valorTotalModulo = $valorTotalCont = $valorTotalPresu = 0;
                foreach ($recaudos as $key => $recaudo) {
                    $dataTemp = [];
                    //presupuesto
                    $sqlPresupuesto = "SELECT SUM(valor) AS valuePresupuesto FROM srv_recaudo_factura_ppto WHERE codigo_recaudo = $recaudo[codigo_recaudo]";
                    $presupuesto = $this->select(($sqlPresupuesto));

                    //contabilidad
                    $sqlContabilidad = "SELECT SUM(valdebito) AS valueDebito, SUM(valcredito) AS valueCredito FROM comprobante_det WHERE tipo_comp = $comprobanteRecaudo AND numerotipo = $recaudo[codigo_recaudo] AND LEFT(cuenta,2) = '11'";
                    $contabilidad = $this->select($sqlContabilidad);

                    $valueContabilidad = $contabilidad["valueDebito"] - $contabilidad["valueCredito"];

                    $dataTemp["tipoRecaudo"] = "Recaudo";
                    $dataTemp["consecutivo"] = $recaudo["codigo_recaudo"];
                    $dataTemp["fecha"] = $recaudo["fecha_recaudo"];
                    $dataTemp["value_sp"] = round($recaudo["valor_pago"], 2);
                    $dataTemp["value_ppto"] = round($presupuesto["valuePresupuesto"], 2);
                    $dataTemp["value_cont"] = round($valueContabilidad, 2);
                    $valorTotalModulo += round($recaudo["valor_pago"], 2);
                    $valorTotalCont += round($presupuesto["valuePresupuesto"], 2);
                    $valorTotalPresu += round($presupuesto["valuePresupuesto"], 2);
                    array_push($data, $dataTemp);
                }

                $sqlAcuerdos = "SELECT codigo_acuerdo, fecha_acuerdo, valor_abonado FROM srv_acuerdo_cab WHERE fecha_acuerdo BETWEEN '$fechaIni' AND '$fechaFin' AND estado_acuerdo != 'Reversado'";
                $acuerdos = $this->select_all($sqlAcuerdos);

                foreach ($acuerdos as $key => $acuerdo) {
                    $dataTemp = [];
                    //presupuesto
                    $sqlPresupuesto = "SELECT SUM(valor) AS valuePresupuesto FROM srv_acuerdo_ppto WHERE codigo_acuerdo = $acuerdo[codigo_acuerdo]";
                    $presupuesto = $this->select($sqlPresupuesto);

                    //contabilidad
                    $sqlContabilidad = "SELECT SUM(valdebito) AS valueDebito, SUM(valcredito) AS valueCredito FROM comprobante_det WHERE tipo_comp = $comprobanteAcuerdo AND numerotipo = $acuerdo[codigo_acuerdo] AND LEFT(cuenta,2) = '11'";
                    $contabilidad = $this->select($sqlContabilidad);

                    $valueContabilidad = $contabilidad["valueDebito"] - $contabilidad["valueCredito"];

                    $dataTemp["tipoRecaudo"] = "Acuerdo";
                    $dataTemp["consecutivo"] = $acuerdo["codigo_acuerdo"];
                    $dataTemp["fecha"] = $acuerdo["fecha_acuerdo"];
                    $dataTemp["value_sp"] = round($acuerdo["valor_abonado"], 2);
                    $dataTemp["value_ppto"] = round($presupuesto["valuePresupuesto"], 2);
                    $dataTemp["value_cont"] = round($valueContabilidad, 2);
                    $valorTotalModulo += round($acuerdo["valor_abonado"], 2);
                    $valorTotalPresu += round($presupuesto["valuePresupuesto"], 2);
                    $valorTotalCont +=  round($valueContabilidad, 2);
                    array_push($data, $dataTemp);
                }
            
                $arrResponse = array(
                    "status" => true,
                    "data" => $data,
                    "valorTotalModulo" => $valorTotalModulo,
                    "valorTotalCont" => $valorTotalCont,
                    "valorTotalPresu" => $valorTotalPresu
                );

                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
            die();
        }

        public function reflejaPptoAcuerdo() {
            if(!empty($_SESSION)) {
                $consecutivo = $valorAbonadoSub = $valorAbonoSubAnt = 0;
                $consecutivo = (int) $_POST["consecutivo"];
                $vigencia = "";

                $sqlAcuerdo = "SELECT codigo_factura, valor_abonado, descuento_intereses, valor_factura, fecha_acuerdo FROM srv_acuerdo_cab WHERE codigo_acuerdo = $consecutivo";
                $acuerdo = $this->select($sqlAcuerdo);

                $sql_delete_ppto = "DELETE FROM srv_acuerdo_ppto WHERE codigo_acuerdo = $consecutivo";
                $this->delete($sql_delete_ppto);

                $fechaComoEntero = strtotime($acuerdo["fecha_acuerdo"]);
                $vigencia = date("Y", $fechaComoEntero);

                $sqlDetalle = "SELECT id_servicio AS servicioId, SUM(credito) AS valueCredito, SUM(debito) AS valueDebito FROM srvdetalles_facturacion WHERE numero_facturacion = $acuerdo[codigo_factura] AND tipo_movimiento = '101' GROUP BY id_servicio;";
                $detalles = $this->select_all($sqlDetalle);


                foreach ($detalles as $key => $detalle) {
                    $valorServicio = $abonoValorServicio = 0;
                    $valorServicio = round(($detalle["valueCredito"] - $detalle["valueDebito"]), 2);
    
                    //Valida si tiene descuento de intereses
                    if ($acuerdo["descuento_intereses"] > 0) {
                        $interesesServicio = $descuento = $porcentaje = 0;
                        $interesesServicio = $this->searchValueInteresServ($acuerdo["codigo_factura"], $detalle["servicioId"]);
                        $porcentaje = $acuerdo["descuento_intereses"] / 100;
                        $descuento = $interesesServicio * $porcentaje;
                        $valorServicio = $valorServicio - $descuento;
                    }

                    $valorServicio = round($valorServicio, 2);
                    $abonoValorServicio = ($valorServicio / $acuerdo["valor_factura"]) * $acuerdo["valor_abonado"];
                    $abonoValorServicio = ceil($abonoValorServicio / 100);
                    $abonoValorServicio = $abonoValorServicio * 100; 

                    $valorAbonadoSub = $valorAbonadoSub + $abonoValorServicio;
                    $valorServicioMenosAbono = $valorServicio - $abonoValorServicio;
                    $valorServicioMenosAbono = round($valorServicioMenosAbono, 2);

                    if ($valorAbonadoSub > $acuerdo["valor_abonado"]) {
                        $abonoValorServicio = $acuerdo["valor_abonado"] - $valorAbonoSubAnt;
                    }

                    $sqlServicio = "SELECT cuenta, fuente, productoservicio, seccion_presupuestal FROM srvservicios WHERE id = $detalle[servicioId]";
                    $servicio = $this->select($sqlServicio);
                    
                    $sqlInsertPresu = "INSERT INTO srv_acuerdo_ppto (codigo_acuerdo, id_servicio, valor, vigencia, cuenta, fuente, producto_servicio, seccion_presupuestal, medio_pago, vigencia_gasto) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
                    $datosInsert = [$consecutivo, $detalle['servicioId'], $abonoValorServicio, $vigencia, $servicio['cuenta'], $servicio['fuente'], $servicio['productoservicio'], $servicio['seccion_presupuestal'], 'CSF', '1'];
                    $this->insert($sqlInsertPresu, $datosInsert);

                    $valorAbonoSubAnt = $valorAbonoSubAnt + $abonoValorServicio;
                }

                $sqlPresupuesto = "SELECT SUM(valor) AS valuePresupuesto FROM srv_acuerdo_ppto WHERE codigo_acuerdo = $consecutivo";
                $presupuesto = $this->select($sqlPresupuesto);

                $dataTemp["value_ppto"] = round($presupuesto["valuePresupuesto"], 2);

                echo json_encode($dataTemp,JSON_UNESCAPED_UNICODE);
            }
            
            die();
        }

        public function reflejaPptoRecaudo() {

            if(!empty($_SESSION))  {
                $consecutivo = $valorSubTotal = $valorTotal = 0;
                $consecutivo = (int) $_POST['consecutivo'];
                $vigencia = "";
    
                $sqlRecaudo = "SELECT numero_factura, fecha_recaudo, valor_pago, descuento_intereses FROM srv_recaudo_factura WHERE codigo_recaudo = $consecutivo";
                $recaudo = $this->select($sqlRecaudo);
    
                $sql_delete_ppto = "DELETE FROM srv_recaudo_factura_ppto WHERE codigo_recaudo = $consecutivo";
                $this->delete($sql_delete_ppto);
    
                $fechaComoEntero = strtotime($recaudo["fecha_recaudo"]);
                $vigencia = date("Y", $fechaComoEntero);
    
                $sqlDetalle = "SELECT id_servicio AS servicioId, SUM(credito) AS valueCredito, SUM(debito) AS valueDebito FROM srvdetalles_facturacion WHERE numero_facturacion = $recaudo[numero_factura] AND tipo_movimiento = '101' GROUP BY id_servicio;";
                $detalles = $this->select_all($sqlDetalle);
    
                foreach ($detalles as $key => $detalle) {
                    $valorServicio = 0;
                    $valorServicio = $detalle["valueCredito"] - $detalle["valueDebito"];
    
                    if ($recaudo["descuento_intereses"] > 0) {
                        $interesesServicio = $descuento = $porcentaje = 0;
                        $interesesServicio = $this->searchValueInteresServ($recaudo["numero_factura"], $detalle["servicioId"]);
                        $porcentaje = $recaudo["descuento_intereses"] / 100;
                        $descuento = round(($interesesServicio * $porcentaje), 2);
                        $valorServicio = $valorServicio - $descuento;
                    }
                    $valorServicio = ceil($valorServicio / 100);
                    $valorServicio = $valorServicio * 100;               
                    $valorSubTotal = $valorSubTotal + $valorServicio;
    
                    if ($valorSubTotal > $recaudo["valor_pago"]) {
                        $valorServicio = $recaudo["valor_pago"] - $valorTotal;
                    }
    
                    $sqlServicio = "SELECT cuenta, fuente, productoservicio, seccion_presupuestal FROM srvservicios WHERE id = $detalle[servicioId]";
                    $servicio = $this->select($sqlServicio);
                    
                    $sqlInsertPresu = "INSERT INTO srv_recaudo_factura_ppto (codigo_recaudo, id_servicio, valor, vigencia, cuenta, fuente, producto_servicio, seccion_presupuestal, medio_pago, vigencia_gasto) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
                    $datosInsert = [$consecutivo, $detalle['servicioId'], $valorServicio, $vigencia, $servicio['cuenta'], $servicio['fuente'], $servicio['productoservicio'], $servicio['seccion_presupuestal'], 'CSF', '1'];
                    $this->insert($sqlInsertPresu, $datosInsert);
                    
                    $valorTotal = $valorTotal + $valorServicio;
                }
    
                $sqlPresupuesto = "SELECT SUM(valor) AS valuePresupuesto FROM srv_recaudo_factura_ppto WHERE codigo_recaudo = $consecutivo";
                $presupuesto = $this->select($sqlPresupuesto);
    
                $dataTemp["value_ppto"] = round($presupuesto["valuePresupuesto"], 2);
    
                echo json_encode($dataTemp,JSON_UNESCAPED_UNICODE);
            }

            die();
        }

        public function reflejarMasivoPptoRecaudo() {

            if(!empty($_SESSION))  {
                $fechaIni = $_POST['fechaIni'];
                $fechaFin = $_POST['fechaFin'];
                $vigencia = "";
    
                $sqlRecaudo = "SELECT numero_factura, fecha_recaudo, valor_pago, descuento_intereses, codigo_recaudo FROM srv_recaudo_factura WHERE fecha_recaudo BETWEEN '$fechaIni' AND '$fechaFin'";
                $recaudo = $this->select_all($sqlRecaudo);

                foreach($recaudo as $recaudo) {
                    $consecutivo = $valorSubTotal = $valorTotal = 0;
                    $consecutivo = $recaudo["codigo_recaudo"];

                    $sql_delete_ppto = "DELETE FROM srv_recaudo_factura_ppto WHERE codigo_recaudo = $consecutivo";
                    $this->delete($sql_delete_ppto);

                    $fechaComoEntero = strtotime($recaudo["fecha_recaudo"]);
                    $vigencia = date("Y", $fechaComoEntero);

                    $sqlDetalle = "SELECT id_servicio AS servicioId, SUM(credito) AS valueCredito, SUM(debito) AS valueDebito FROM srvdetalles_facturacion WHERE numero_facturacion = $recaudo[numero_factura] AND tipo_movimiento = '101' GROUP BY id_servicio;";
                    $detalles = $this->select_all($sqlDetalle);

                    foreach ($detalles as $key => $detalle) {
                        $valorServicio = 0;
                        $valorServicio = $detalle["valueCredito"] - $detalle["valueDebito"];
        
                        if ($recaudo["descuento_intereses"] > 0) {
                            $interesesServicio = $descuento = $porcentaje = 0;
                            $interesesServicio = $this->searchValueInteresServ($recaudo["numero_factura"], $detalle["servicioId"]);
                            $porcentaje = $recaudo["descuento_intereses"] / 100;
                            $descuento = round(($interesesServicio * $porcentaje), 2);
                            $valorServicio = $valorServicio - $descuento;
                        }
                        $valorServicio = ceil($valorServicio / 100);
                        $valorServicio = $valorServicio * 100;               
                        $valorSubTotal = $valorSubTotal + $valorServicio;
        
                        if ($valorSubTotal > $recaudo["valor_pago"]) {
                            $valorServicio = $recaudo["valor_pago"] - $valorTotal;
                        }
        
                        $sqlServicio = "SELECT cuenta, fuente, productoservicio, seccion_presupuestal FROM srvservicios WHERE id = $detalle[servicioId]";
                        $servicio = $this->select($sqlServicio);
                        
                        $sqlInsertPresu = "INSERT INTO srv_recaudo_factura_ppto (codigo_recaudo, id_servicio, valor, vigencia, cuenta, fuente, producto_servicio, seccion_presupuestal, medio_pago, vigencia_gasto) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
                        $datosInsert = [$consecutivo, $detalle['servicioId'], $valorServicio, $vigencia, $servicio['cuenta'], $servicio['fuente'], $servicio['productoservicio'], $servicio['seccion_presupuestal'], 'CSF', '1'];
                        $this->insert($sqlInsertPresu, $datosInsert);
                        
                        $valorTotal = $valorTotal + $valorServicio;
                    }

                    if ($valorSubTotal < $recaudo["valor_pago"]) {
                        $valorServicio = $recaudo["valor_pago"] - $valorTotal;
                        $sqlInsertPresu = "INSERT INTO srv_recaudo_factura_ppto (codigo_recaudo, id_servicio, valor, vigencia, cuenta, fuente, producto_servicio, seccion_presupuestal, medio_pago, vigencia_gasto) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
                        $datosInsert = [$consecutivo, 1, $valorServicio, $vigencia, $servicio['cuenta'], $servicio['fuente'], $servicio['productoservicio'], $servicio['seccion_presupuestal'], 'CSF', '1'];
                        $this->insert($sqlInsertPresu, $datosInsert);
                    }
                }

                $status = true;
    
                echo json_encode($status,JSON_UNESCAPED_UNICODE);
            }

            die();
        }

        public function reflejaContRecaudo() {
            if (!empty($_SESSION)) {
                $consecutivo = 0;
                $consecutivo = (int) $_POST['consecutivo'];
                $tipoComprobante = 30;

                $sqlRecaudo = "SELECT numero_factura, fecha_recaudo, valor_pago, descuento_intereses, id_cliente AS clienteId, numero_cuenta, documento FROM srv_recaudo_factura WHERE codigo_recaudo = $consecutivo";
                $recaudo = $this->select($sqlRecaudo);

                $fecha = $recaudo["fecha_recaudo"];
                $cuentaDebito = $recaudo["numero_cuenta"];
                $documento = $recaudo["documento"];
                $fechaComoEntero = strtotime($fecha);
                $vigencia = date("Y", $fechaComoEntero);
                $porcentajeDesc = $recaudo["descuento_interses"];

                $sql_bloqueo = "SELECT count(*) as valor FROM dominios dom WHERE dom.nombre_dominio = 'PERMISO_MODIFICA_DOC'  AND dom.valor_final <= '$recaudo[fecha_recuado]'  AND dom.valor_inicial =  '$_SESSION[cedulausu]'";
                $request = $this->select($sql_bloqueo);

                if ($request >= 1) {
                    $delete_cont = "DELETE FROM comprobante_det WHERE tipo_comp = $tipoComprobante AND numerotipo = $consecutivo";
                    $this->delete($delete_cont);

                    $facturas = $cargoFijo = $consumo = $subCargoFijo = $subConsumo = $contribucion = $acuerdoPago = $saldoInicial = $interesMoratorio = $desincentivo = [];

                    $sql_facturas = "SELECT numero_facturacion, estado_pago FROM srvcortes_detalle WHERE id_cliente = $recaudo[clienteId] AND numero_facturacion <= $recaudo[numero_factura] ORDER BY numero_facturacion DESC";
                    $numFacturas = $this->select_all($sql_facturas);

                    foreach ($numFacturas as $key => $factura) {
                        if ($factura["estado_pago"] == "S" OR $factura["estado_pago"] == "V") {
                            $facturas[] = $factura["numero_facturacion"];
                        }
                        else if ($factura["estado_pago"] == "P" OR $factura["estado_pago"] == "A" OR $factura["estado_pago"] == "A") {
                            if ($factura["numero_facturacion"] == $recaudo["numero_factura"]) {
                                $facturas[] = $factura["numero_facturacion"];
                            }
                            else {
                                break;
                            }
                        }
                    }

                    foreach ($facturas as $key => $factura) {
        
                        $sql_detalles = "SELECT id_servicio AS servicioId, id_tipo_cobro AS tipoCobroId, COALESCE(credito,0) AS valueCredito, COALESCE(debito,0) AS valueDebito, corte FROM srvdetalles_facturacion WHERE numero_facturacion = $factura AND (credito > 0 OR debito > 0) ORDER BY id_tipo_cobro ASC, id_servicio ASC";
                        $detalles = $this->select_all($sql_detalles);

                        foreach ($detalles as $key => $det) {
                            switch ($det["tipoCobroId"]) {
                                case 1:
                                    $cargoFijo[$det["servicioId"]] += $det["valueCredito"];
                                break;
                                
                                case 2:
                                    $consumo[$det["servicioId"]] += $det["valueCredito"];
                                break;
                    
                                case 3:
                                    $subCargoFijo[$det["servicioId"]] += $det["valueDebito"];
                                break;
                    
                                case 4:
                                    $subConsumo[$det["servicioId"]] += $det["valueDebito"];
                                break;
                    
                                case 5:
                                    $contribucion[$det["servicioId"]] += $det["valueDebito"];
                                break;
                    
                                case 7:
                                    $acuerdoPago[$det["servicioId"]] += $det["valueCredito"];
                                break;
            
                                case 8:
                                    //Saldos iniciales
                                    if ($det[4] == 1) {
                                        $saldoInicial[$det["servicioId"]] = (float) $det["valueCredito"];
                                    }
                                break;
                    
                                case 10;
                                    if ($factura == $recaudo["numero_factura"]) {
                                        $interesMoratorio[$det["servicioId"]] = (float) $det["valueCredito"];
                                    }    
                                break;
    
                                case 12:
                                    $desincentivo[$det["servicioId"]] += $det["valueCredito"];
                                break;
                            }
                        }
                    }

                    for ($i=1; $i <= count($cargoFijo); $i++) { 
                        //Descuento de subsidios
                        $cargoFijo[$i] = $cargoFijo[$i] - $subCargoFijo[$i];
                        $consumo[$i] = $consumo[$i] - $subConsumo[$i];
                    }

                    for ($i=1; $i <= count($cargoFijo); $i++) { 

                        $cargoFijo[$i] = round($cargoFijo[$i], 2);
                        $consumo[$i] = round($consumo[$i], 2);
                        $contribucion[$i] = round($contribucion[$i], 2);
                        $acuerdoPago[$i] = round($acuerdoPago[$i], 2);
                        $interesMoratorio[$i] = round($interesMoratorio[$i], 2);
                        $saldoInicial[$i] = round($saldoInicial[$i], 2);
                        $desincentivo[$i] = round($desincentivo[$i], 2);

                        if ($cargoFijo[$i] > 0) {
                            $this->insertaCont($i, "SS", $fecha, $consecutivo, $cargoFijo[$i], $cuentaDebito, $documento, $vigencia, "cargo_fijo_u", $porcentajeDesc, 'cargo fijo', $tipoComprobante);
                        }
                        
                        if ($consumo[$i] > 0) {
                            $this->insertaCont($i, "CL", $fecha, $consecutivo, $consumo[$i], $cuentaDebito, $documento, $vigencia, "consumo_u", $porcentajeDesc, 'consumo', $tipoComprobante);
                        }

                        if ($contribucion[$i] > 0) {
                            $this->insertaCont($i, "SC", $fecha, $consecutivo, $contribucion[$i], $cuentaDebito, $documento, $vigencia, "contribucion_u", $porcentajeDesc, 'contribucion', $tipoComprobante);
                        }

                        if ($acuerdoPago[$i] > 0) {
                            $this->insertaCont($i, "AP", $fecha, $consecutivo, $acuerdoPago[$i], $cuentaDebito, $documento, $vigencia, "acuerdo_pago", $porcentajeDesc, 'acuerdo de pago', $tipoComprobante);
                        }

                        if ($interesMoratorio[$i] > 0) {
                            $this->insertaCont($i, "SM", $fecha, $consecutivo, $interesMoratorio[$i], $cuentaDebito, $documento, $vigencia, "interes_moratorio", $porcentajeDesc, 'interes moratorio', $tipoComprobante);
                        }

                        if ($saldoInicial[$i] > 0) {
                            $this->insertaCont($i, "CL", $fecha, $consecutivo, $saldoInicial[$i], $cuentaDebito, $documento, $vigencia, "consumo_u", $porcentajeDesc, 'saldo inicial', $tipoComprobante);
                        }

                        if ($desincentivo[$i]> 0) {
                            $this->insertaCont($i, "CL", $fecha, $consecutivo, $desincentivo[$i], $cuentaDebito, $documento, $vigencia, "consumo_u", $porcentajeDesc, 'desincentivo', $tipoComprobante);
                        }
                    }

                    $sqlSum = "SELECT SUM(valdebito) AS valueDebito FROM comprobante_det WHERE id_comp = '$tipoComprobante $consecutivo' AND LEFT(cuenta,2) = '11'";
                    $suma = $this->select($sqlSum);

                    $diferencia = $recaudo["valor_pago"] - $suma["valueDebito"];
                    $diferencia = round($diferencia, 2);

                    if ($diferencia > 0) {
                        $this->insertaCont(1, "CL", $fecha, $consecutivo, $diferencia, $cuentaDebito, $documento, $vigencia, "consumo_u", $porcentajeDesc, 'ajuste al peso', $tipoComprobante);
                    }

                    $sqlContabilidad = "SELECT SUM(valdebito) AS valueDebito, SUM(valcredito) AS valueCredito FROM comprobante_det WHERE tipo_comp = $tipoComprobante AND numerotipo = $consecutivo AND LEFT(cuenta,2) = '11'";
                    $contabilidad = $this->select($sqlContabilidad);

                    $valueContabilidad = $contabilidad["valueDebito"] - $contabilidad["valueCredito"];

                    $arrResponse = array("status"=>true, "value_cont"=>$valueContabilidad);
                }
                else {
                    $arrResponse = array("status"=>false,"msg"=>"Fecha de recuaudo bloqueada");
                }
            }
            echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            die();
        }

        public function reflejaContAcuerdo() {
            if (!empty($_SESSION)) {
                $consecutivo = 0;
                $consecutivo = (int) $_POST['consecutivo'];
                $tipoComprobante = 39;

                $sqlAcuerdo = "SELECT codigo_factura AS numero_factura, fecha_acuerdo, valor_abonado, descuento_intereses, id_cliente AS clienteId, numero_cuenta, documento, valor_factura FROM srv_acuerdo_cab WHERE codigo_acuerdo = $consecutivo";
                $acuerdo = $this->select($sqlAcuerdo);
                
                $fecha = $acuerdo["fecha_acuerdo"];
                $cuentaDebito = $acuerdo["numero_cuenta"];
                $documento = $acuerdo["documento"];
                $fechaComoEntero = strtotime($fecha);
                $vigencia = date("Y", $fechaComoEntero);
                $porcentajeDesc = $acuerdo["descuento_interses"];

                $sql_bloqueo = "SELECT count(*) as valor FROM dominios dom WHERE dom.nombre_dominio = 'PERMISO_MODIFICA_DOC'  AND dom.valor_final <= '$fecha'  AND dom.valor_inicial =  '$_SESSION[cedulausu]'";
                $request = $this->select($sql_bloqueo);

                if ($request >= 1) {
                    $delete_cont = "DELETE FROM comprobante_det WHERE tipo_comp = $tipoComprobante AND numerotipo = $consecutivo";
                    $this->delete($delete_cont);

                    $delete_cont_cab = "DELETE FROM comprobante_cab WHERE tipo_comp = $tipoComprobante AND numerotipo = $consecutivo";
                    $this->delete($delete_cont_cab);

                    $sql_cab_cont = "INSERT INTO comprobante_cab (numerotipo, tipo_comp, fecha, concepto, total_debito, total_credito, estado) VALUES (?, ?, ?, ?, ?, ?, ?)";
                    $value_cab_cont = [$consecutivo, "$tipoComprobante", "$fecha", "Acuerdo de pago de la factura numero $acuerdo[numero_factura]", 0, 0, "1"];

                    $this->insert($sql_cab_cont, $value_cab_cont);

                    $facturas = $cargoFijo = $consumo = $subCargoFijo = $subConsumo = $contribucion = $acuerdoPago = $saldoInicial = $interesMoratorio = $desincentivo = [];

                    $sql_facturas = "SELECT numero_facturacion, estado_pago FROM srvcortes_detalle WHERE id_cliente = $acuerdo[clienteId] AND numero_facturacion <= $acuerdo[numero_factura] ORDER BY numero_facturacion DESC";
                    $numFacturas = $this->select_all($sql_facturas);

                    foreach ($numFacturas as $key => $factura) {
                        if ($factura["estado_pago"] == "S" OR $factura["estado_pago"] == "V") {
                            $facturas[] = $factura["numero_facturacion"];
                        }
                        else if ($factura["estado_pago"] == "P" OR $factura["estado_pago"] == "A" OR $factura["estado_pago"] == "A") {
                            if ($factura["numero_facturacion"] == $acuerdo["numero_factura"]) {
                                $facturas[] = $factura["numero_facturacion"];
                            }
                            else {
                                break;
                            }
                        }
                    }
                    
                    foreach ($facturas as $key => $factura) {
        
                        $sql_detalles = "SELECT id_servicio AS servicioId, id_tipo_cobro AS tipoCobroId, COALESCE(credito,0) AS valueCredito, COALESCE(debito,0) AS valueDebito, corte FROM srvdetalles_facturacion WHERE numero_facturacion = $factura AND (credito > 0 OR debito > 0) ORDER BY id_tipo_cobro ASC, id_servicio ASC";
                        $detalles = $this->select_all($sql_detalles);

                        foreach ($detalles as $key => $det) {
                            switch ($det["tipoCobroId"]) {
                                case 1:
                                    $cargoFijo[$det["servicioId"]] += $det["valueCredito"];
                                break;
                                
                                case 2:
                                    $consumo[$det["servicioId"]] += $det["valueCredito"];
                                break;
                    
                                case 3:
                                    $subCargoFijo[$det["servicioId"]] += $det["valueDebito"];
                                break;
                    
                                case 4:
                                    $subConsumo[$det["servicioId"]] += $det["valueDebito"];
                                break;
                    
                                case 5:
                                    $contribucion[$det["servicioId"]] += $det["valueDebito"];
                                break;
                    
                                case 7:
                                    $acuerdoPago[$det["servicioId"]] += $det["valueCredito"];
                                break;
            
                                case 8:
                                    //Saldos iniciales
                                    if ($det[4] == 1) {
                                        $saldoInicial[$det["servicioId"]] = (float) $det["valueCredito"];
                                    }
                                break;
                    
                                case 10;
                                    if ($factura == $acuerdo["numero_factura"]) {
                                        $interesMoratorio[$det["servicioId"]] = (float) $det["valueCredito"];
                                    }    
                                break;
    
                                case 12:
                                    $desincentivo[$det["servicioId"]] += $det["valueCredito"];
                                break;
                            }
                        }
                    }
                    
                    for ($i=1; $i <= count($cargoFijo); $i++) { 
                        //Descuento de subsidios
                        $cargoFijo[$i] = $cargoFijo[$i] - $subCargoFijo[$i];
                        $consumo[$i] = $consumo[$i] - $subConsumo[$i];
                    }

                    for ($i=1; $i <= count($cargoFijo); $i++) { 

                        $cargoFijo[$i] = round($cargoFijo[$i], 2);
                        $consumo[$i] = round($consumo[$i], 2);
                        $contribucion[$i] = round($contribucion[$i], 2);
                        $acuerdoPago[$i] = round($acuerdoPago[$i], 2);
                        $interesMoratorio[$i] = round($interesMoratorio[$i], 2);
                        $saldoInicial[$i] = round($saldoInicial[$i], 2);
                        $desincentivo[$i] = round($desincentivo[$i], 2);

                        $sql_servicio = "SELECT acuerdo_pago, cc, nombre FROM srvservicios WHERE id = $i";
                        $servicio = $this->select($sql_servicio);
        
                        $sql_concepto = "SELECT cuenta FROM conceptoscontables_det WHERE codigo = '$servicio[acuerdo_pago]' AND tipo = 'AP' AND cc = '$servicio[cc]' AND modulo = 10 AND fechainicial <= '$fecha' AND debito = 'S'";
                        $concepto = $this->select($sql_concepto);
        
                        $cuentaAcuerdo = $concepto["cuenta"];

                        if ($cargoFijo[$i] > 0) {
                            $this->insertaCont($i, "SS", $fecha, $consecutivo, $cargoFijo[$i], $cuentaAcuerdo, $documento, $vigencia, "cargo_fijo_u", $porcentajeDesc, 'cargo fijo', $tipoComprobante);
                        }
                        
                        if ($consumo[$i] > 0) {
                            $this->insertaCont($i, "CL", $fecha, $consecutivo, $consumo[$i], $cuentaAcuerdo, $documento, $vigencia, "consumo_u", $porcentajeDesc, 'consumo', $tipoComprobante);
                        }

                        if ($contribucion[$i] > 0) {
                            $this->insertaCont($i, "SC", $fecha, $consecutivo, $contribucion[$i], $cuentaAcuerdo, $documento, $vigencia, "contribucion_u", $porcentajeDesc, 'contribucion', $tipoComprobante);
                        }

                        if ($interesMoratorio[$i] > 0) {
                            $this->insertaCont($i, "SM", $fecha, $consecutivo, $interesMoratorio[$i], $cuentaAcuerdo, $documento, $vigencia, "interes_moratorio", $porcentajeDesc, 'interes moratorio', $tipoComprobante);
                        }

                        if ($saldoInicial[$i] > 0) {
                            $this->insertaCont($i, "CL", $fecha, $consecutivo, $saldoInicial[$i], $cuentaAcuerdo, $documento, $vigencia, "consumo_u", $porcentajeDesc, 'saldo inicial', $tipoComprobante);
                        }

                        if ($desincentivo[$i]> 0) {
                            $this->insertaCont($i, "CL", $fecha, $consecutivo, $desincentivo[$i], $cuentaAcuerdo, $documento, $vigencia, "consumo_u", $porcentajeDesc, 'desincentivo', $tipoComprobante);
                        }
                    }

                    $sqlDetalle = "SELECT id_servicio AS servicioId, SUM(credito) AS valueCredito, SUM(debito) AS valueDebito FROM srvdetalles_facturacion WHERE numero_facturacion = $acuerdo[numero_factura] AND tipo_movimiento = '101' GROUP BY id_servicio;";
                    $detalles = $this->select_all($sqlDetalle);

                    $valorAbonadoSub = $valorAbonoSubAnt = 0;
                    foreach ($detalles as $key => $detalle) {
                        $valorServicio = $abonoServicio = $valorCuotaServicio = 0;
                        $valorServicio = $detalle["valueCredito"] - $detalle["valueDebito"];

                        //Valida si tiene descuento de intereses
                        if ($porcentajeDesc > 0) {
                            $interesesServicio = $descuento = $porcentaje = 0;
                            $interesesServicio = $this->searchValueInteresServ($acuerdo["numero_factura"], $detalle["servicioId"]);
                            $porcentaje = $porcentajeDesc / 100;
                            $descuento = $interesesServicio * $porcentaje;
                            $valorServicio = $valorServicio - $descuento;
                        }

                        $valorServicio = round($valorServicio, 2);
                        $abonoValorServicio = ($valorServicio / $acuerdo["valor_factura"]) * $acuerdo["valor_abonado"];

                        $abonoValorServicio = ceil($abonoValorServicio / 100);
                        $abonoValorServicio = $abonoValorServicio * 100;               

                        $valorAbonadoSub = $valorAbonadoSub + $abonoValorServicio;

                        if ($valorAbonadoSub > $acuerdo["valor_abonado"]) {
                            $abonoValorServicio = $acuerdo["valor_abonado"] - $valorAbonoSubAnt;
                        }

                        $this->insertaCont($detalle["servicioId"], "AP", $fecha, $consecutivo, $abonoValorServicio, $cuentaDebito, $documento, $vigencia, "acuerdo_pago", 0, 'abono inicial', $tipoComprobante);

                        $valorAbonoSubAnt = $valorAbonoSubAnt + $abonoValorServicio;
                    }
                                        
                    $sqlContabilidad = "SELECT SUM(valdebito) AS valueDebito, SUM(valcredito) AS valueCredito FROM comprobante_det WHERE tipo_comp = $tipoComprobante AND numerotipo = $consecutivo AND LEFT(cuenta,2) = '11'";
                    $contabilidad = $this->select($sqlContabilidad);

                    $valueContabilidad = $contabilidad["valueDebito"] - $contabilidad["valueCredito"];

                    $arrResponse = array("status"=>true, "value_cont"=>$valueContabilidad);
                }
                else {
                    $arrResponse = array("status"=>false,"msg"=>"Fecha de acuerdo bloqueada");
                }
            }
            echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            die();
        }
    }

    if($_POST){
        $obj = new recaudos();
        if($_POST['action'] == "searchData"){
            $obj->searchData();
        }
        else if ($_POST["action"] == "reflejaPresu01") {
            $obj->reflejaPptoRecaudo();
        }
        else if ($_POST["action"] == "reflejaPresu02") {
            $obj->reflejaPptoAcuerdo();
        }
        else if ($_POST["action"] == "reflejaCont01") {
            $obj->reflejaContRecaudo();
        }
        else if ($_POST["action"] == "reflejaCont02") {
            $obj->reflejaContAcuerdo();
        } else if ($_POST["action"] == "reflejarMasivoPptoRecaudo") {
            $obj->reflejarMasivoPptoRecaudo();
        }
    }

    header("Content-type: application/json");
    die();