const URL = 'servicios_publicos/reporteSeguimientoRecaudos/serv-reporteSeguimientoRecaudos.php';
const URLPDF = '';
const URLEXCEL = '';

var app = new Vue({
    el:"#myapp",
    data() {
        return {
            isLoading:false,

            fechaIni: '',
            fechaFin: '',
            datos: [],
            valorTotalModulo: 0,
            valorTotalCont: 0,
            valorTotalPresu: 0
        }
    },
    mounted() {
        
    },
    methods: {
        searchData: async function(){

            if (this.fechaIni != "" && this.fechaFin != "") {
                const formData = new FormData();
                formData.append("action","searchData");
                formData.append("fechaIni",this.fechaIni);
                formData.append("fechaFin",this.fechaFin);
                this.isLoading = true;
                const response = await fetch(URL,{method:"POST",body:formData});
                const objData = await response.json();
                this.isLoading = false;
                this.datos = objData.data;
                this.valorTotalModulo = objData.valorTotalModulo;
                this.valorTotalCont = objData.valorTotalCont;
                this.valorTotalPresu = objData.valorTotalPresu;
            }
            else {
                Swal.fire("Error","Debes llenar las fechas de inicio y fin","error");
            }
        },

        reflejarPresu: async function(tipo, consecutivo) {

            const formData = new FormData();
            if (tipo == "Recaudo") {
                formData.append("action","reflejaPresu01");
            }
            else if (tipo == "Acuerdo") {
                formData.append("action","reflejaPresu02");    
            }

            formData.append("consecutivo",consecutivo);
            
            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();
            
            this.datos.forEach(dato => {
                if (dato.tipoRecaudo == tipo && dato.consecutivo == consecutivo) {
                    dato.value_ppto = objData.value_ppto;
                }
            });
        },

        reflejarCont: async function(tipo, consecutivo) {
            
            const formData = new FormData();
            if (tipo == "Recaudo") {
                formData.append("action","reflejaCont01");
            }
            else if (tipo == "Acuerdo") {
                formData.append("action","reflejaCont02");    
            }

            formData.append("consecutivo",consecutivo);
            this.isLoading = true;
            
            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();

            if (objData.status == true) {
                this.datos.forEach(dato => {
                    if (dato.tipoRecaudo == tipo && dato.consecutivo == consecutivo) {
                        dato.value_cont = objData.value_cont;
                    }
                });
                this.isLoading = false;;
            }
            else {
                Swal.fire("Error",objData.msg,"error");
            }
        },

        async refejaMasivoPptoRecaudo() {
            const formData = new FormData();
            formData.append("action","reflejarMasivoPptoRecaudo");
            formData.append("fechaIni",this.fechaIni);
            formData.append("fechaFin",this.fechaFin);
            this.isLoading = true;
            
            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();

            
            this.searchData();
            this.isLoading = false;
        },

        // downloadFormat: function(format) {
        //     const form = document.createElement("form");
        //     form.method = "post";
        //     form.target = "_blank";
        //     if (format == 'pdf') {
        //         form.action = URLPDF;
        //     }
        //     else if (format == 'excel') {
        //         form.action = URLEXCEL;
        //     }
            
        //     function addField(name,value){
        //         const input = document.createElement("input");
        //         input.type="hidden";
        //         input.name=name;
        //         input.value = value;
        //         form.appendChild(input);
        //     }
         
        //     addField("datos", JSON.stringify(this.arrAcuerdos));
        
        //     document.body.appendChild(form);
        //     form.submit();
        //     document.body.removeChild(form);
        // },

        formatNumero: function(valor){
            return new Intl.NumberFormat("en-US", {style: "currency", currency: "USD"}).format(valor);      
        },
    },
    computed:{

    }
})
