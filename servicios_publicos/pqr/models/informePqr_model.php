<?php
    require_once '../../../Librerias/core/Helpers.php';
    require_once '../../../Librerias/core/Mysql.php';
    session_start();
    date_default_timezone_set("America/Bogota");
    
    class informeModel extends Mysql {
        public function servicios() {
            $sql = "SELECT id, nombre FROM srvservicios ORDER BY id";
            $data = $this->select_all($sql);
            return $data;
        }

        public function searchDataInforme(string $servicio, string $fechaIni, string $fechaFin) {
            $nameServicio = strtolower($servicio);

            $sql = "SELECT
            SUBSTRING(SB.centro_poblado, 1, 2) AS dpto,
            SUBSTRING(SB.centro_poblado, 2, 3) AS mnpio,
            SUBSTRING(SB.centro_poblado, 5, 3) AS centroPoblado,
            SRP.consecutivo AS consecutivoRadicado,
            SRP.fecha AS fechaRadicado,
            STT.codigo_tramite AS tipoTramite,
            STC.codigo_causal AS causal,
            SCD.codigo_causal_det AS detalleCausal,
            SC.cod_usuario AS NUID,
            SRP.numero_factura AS numFactura,
            STR.codigo_respuesta AS tipoRespuesta,
            SCP.fecha AS fechaRespuesta,
            SCP.consecutivo AS consecutivoRespuesta,
            STN.codigo_notificacion AS tipoNotificacion
            FROM srv_radicar_pqr AS SRP
            INNER JOIN srvclientes AS SC
            ON SRP.cliente_id = SC.id
            INNER JOIN srvbarrios AS SB
            ON SC.id_barrio = SB.id
            INNER JOIN srv_tipo_tramite AS STT
            ON SRP.tipo_tramite_id = STT.id
            INNER JOIN srv_tipo_causal AS STC
            ON SRP.tipo_causal_id = STC.id  
            INNER JOIN srv_causal_det AS SCD
            ON SRP.causal_det_id = SCD.id
            INNER JOIN srv_respuesta_pqr AS SCP
            ON SRP.id = SCP.pqr_id
            INNER JOIN srv_tipo_respuesta AS STR
            ON SCP.tipo_respuesta_id = STR.id
            INNER JOIN srv_tipo_notificacion AS STN
            ON SCP.tipo_notificacion_id = STN.id
            WHERE SRP.estado <> 'N' AND SCP.estado <> 'N' AND SRP.$nameServicio = 1 AND SRP.fecha BETWEEN '$fechaIni' AND '$fechaFin'";
            $data = $this->select_all($sql);
            return $data;
        }
    }
?>