<?php
    require_once '../../../Librerias/core/Helpers.php';
    require_once '../../../Librerias/core/Mysql.php';
    session_start();
    
    class causalModel extends Mysql {
        public function buscaTipoCausales() {
            $sql = "SELECT id, codigo_causal, grupo_causal FROM srv_tipo_causal ORDER BY id";
            $causales = $this->select_all($sql);

            return $causales;
        }

        public function validaCodigo(int $codigo) {
            $sql = "SELECT COUNT(*) as valor FROM srv_causal_det WHERE codigo_causal_det = '$codigo'";
            $request = $this->select($sql);

            return $request['valor'];
        }

        public function insertaDatos(int $tipoCausalId, int $codigo, string $detalle, string $descripcion, bool $ac, bool $alc, bool $aseo) {
            $sql = "INSERT INTO srv_causal_det (tipo_causal_id, codigo_causal_det, detalle, descripcion, acueducto, alcantarillado, aseo) VALUES (?, ?, ?, ?, ?, ?, ?)";
            $value = [
                $tipoCausalId,
                $codigo,
                strtoupper($detalle),
                strtoupper($descripcion),
                $ac,
                $alc,
                $aseo
            ];

            $request = $this->insert($sql, $value);

            return $request;
        }

        public function buscaDatosGuardados() {
            $sql = "SELECT CD.id,
            TC.grupo_causal as tipoCausal,
            CD.codigo_causal_det as codigo,
            CD.detalle,
            CD.descripcion,
            CD.acueducto,
            CD.alcantarillado,
            CD.aseo
            FROM srv_causal_det as CD
            INNER JOIN srv_tipo_causal as TC ON CD.tipo_causal_id = TC.id
            ORDER BY CD.id";
            $data = $this->select_all($sql);

            return $data;
        }

        public function buscaInformacionEditar(int $id) {
            $sql = "SELECT tipo_causal_id as tipoCausal,
            codigo_causal_det as codigo,
            detalle as nombre,
            descripcion,
            acueducto,
            alcantarillado,
            aseo
            FROM srv_causal_det WHERE id = $id";
            $data = $this->select($sql);
            
            $data["acueducto"] = intval($data["acueducto"]);
            $data["alcantarillado"] = intval($data["alcantarillado"]);
            $data["aseo"] = intval($data["aseo"]);

            return $data;
        }

        public function actualizaDatos(int $id, int $tipoCausalId, int $codigo, string $detalle, string $descripcion, bool $ac, bool $alc, bool $aseo) {
            $sql = "UPDATE srv_causal_det SET tipo_causal_id = ?, codigo_causal_det = ?, detalle = ?, descripcion = ?, acueducto = ?, alcantarillado = ?, aseo = ? WHERE id = $id";
            $value = [
                $tipoCausalId,
                $codigo,
                strtoupper($detalle),
                strtoupper($descripcion),
                $ac,
                $alc,
                $aseo
            ];

            $request = $this->update($sql, $value);

            return $request;
        }
    }
?>