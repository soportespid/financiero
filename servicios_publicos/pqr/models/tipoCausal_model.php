<?php
    require_once '../../../Librerias/core/Helpers.php';
    require_once '../../../Librerias/core/Mysql.php';
    session_start();
    
    class causalModel extends Mysql {
        public function validaCodigo(string $codigo) {
            $sql = "SELECT COUNT(*) as valor FROM srv_tipo_causal WHERE codigo_causal = '$codigo'";
            $request = $this->select($sql);

            return $request['valor'];
        }

        public function insertaDatos(string $codigo, string $nombre) {
            $sql = "INSERT INTO srv_tipo_causal (codigo_causal, grupo_causal) VALUES (?, ?)";
            $value = [
                strtoupper($codigo),
                strtoupper($nombre)
            ];

            $request = $this->insert($sql, $value);

            return $request;
        }

        public function buscaDatosGuardados() {
            $sql = "SELECT id, codigo_causal as codigo, grupo_causal as nombre FROM srv_tipo_causal ORDER BY id";
            $data = $this->select_all($sql);

            return $data;
        }

        public function buscaInformacionEditar(int $id) {
            $sql = "SELECT codigo_causal as codigo, grupo_causal as nombre FROM srv_tipo_causal WHERE id = $id";
            $data = $this->select($sql);

            return $data;
        }

        public function actualizaDatos(int $id, string $codigo, string $nombre) {
            $sql = "UPDATE srv_tipo_causal SET codigo_causal = ?, grupo_causal = ? WHERE id = $id";
            $values = [
                strtoupper($codigo),
                strtoupper($nombre)
            ];

            $request = $this->update($sql, $values);

            return $request;
        }
    }
?>