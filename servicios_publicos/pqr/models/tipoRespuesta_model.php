<?php
    require_once '../../../Librerias/core/Helpers.php';
    require_once '../../../Librerias/core/Mysql.php';
    session_start();
    
    class causalModel extends Mysql {
        public function validaCodigo(int $codigo) {
            $sql = "SELECT COUNT(*) as valor FROM srv_tipo_respuesta WHERE codigo_respuesta = '$codigo'";
            $request = $this->select($sql);

            return $request['valor'];
        }

        public function insertaDatos(int $codigo, string $detalle, string $descripcion) {
            $sql = "INSERT INTO srv_tipo_respuesta (codigo_respuesta, nombre, descripcion) VALUES (?, ?, ?)";
            $value = [
                $codigo,
                strtoupper($detalle),
                strtoupper($descripcion)
            ];

            $request = $this->insert($sql, $value);

            return $request;
        }

        public function buscaDatosGuardados() {
            $sql = "SELECT id,
            codigo_respuesta as codigo,
            nombre,
            descripcion
            FROM srv_tipo_respuesta
            ORDER BY id";
            $data = $this->select_all($sql);

            return $data;
        }

        public function buscaInformacionEditar(int $id) {
            $sql = "SELECT codigo_respuesta as codigo,
            nombre,
            descripcion
            FROM srv_tipo_respuesta WHERE id = $id";
            $data = $this->select($sql);

            return $data;
        }

        public function actualizaDatos(int $id, int $codigo, string $nombre, string $descripcion) {
            $sql = "UPDATE srv_tipo_respuesta SET codigo_respuesta = ?, nombre = ?, descripcion = ? WHERE id = $id";
            $value = [
                $codigo,
                strtoupper($nombre),
                strtoupper($descripcion)
            ];

            $request = $this->update($sql, $value);

            return $request;
        }
    }
?>