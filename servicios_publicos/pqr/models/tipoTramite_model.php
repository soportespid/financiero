<?php
    require_once '../../../Librerias/core/Helpers.php';
    require_once '../../../Librerias/core/Mysql.php';
    session_start();
    
    class tipoTramiteModel extends Mysql {
        public function validaCodigo(int $codigo) {
            $sql = "SELECT COUNT(*) as valor FROM srv_tipo_tramite WHERE codigo_tramite = $codigo";
            $request = $this->select($sql);

            return $request['valor'];
        }

        public function insertaDatos(int $codigo, string $nombre, int $dias) {
            $sql = "INSERT INTO srv_tipo_tramite (codigo_tramite, nombre, tiempo_respuesta) VALUES (?, ?, ?)";
            $value = [
                $codigo,
                strtoupper($nombre),
                $dias
            ];

            $request = $this->insert($sql, $value);

            return $request;
        }

        public function buscaTiposTramites() {
            $sql = "SELECT * FROM srv_tipo_tramite ORDER BY id";
            $data = $this->select_all($sql);

            return $data;
        }

        public function encuentraTipoTramite(int $id) {
            $sql = "SELECT codigo_tramite as codigo, nombre as nombreTramite, tiempo_respuesta as tiempoRespuesta FROM srv_tipo_tramite WHERE id = $id";
            $data = $this->select($sql);

            return $data;
        }

        public function actualizaDatos(int $id, int $codigo, string $nombre, int $dias) {
            $sql = "UPDATE srv_tipo_tramite SET codigo_tramite = ?, nombre = ?, tiempo_respuesta = ? WHERE id = $id";
            $values = [
                $codigo,
                strtoupper($nombre),
                $dias
            ];

            $request = $this->update($sql, $values);

            return $request;
        }
    }
?>