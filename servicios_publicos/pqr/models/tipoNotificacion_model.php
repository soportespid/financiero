<?php
    require_once '../../../Librerias/core/Helpers.php';
    require_once '../../../Librerias/core/Mysql.php';
    session_start();
    
    class notificacionModel extends Mysql {
        public function validaCodigo(int $codigo) {
            $sql = "SELECT COUNT(*) as valor FROM srv_tipo_notificacion WHERE codigo_notificacion = '$codigo'";
            $request = $this->select($sql);

            return $request['valor'];
        }

        public function insertaDatos(int $codigo, string $detalle) {
            $sql = "INSERT INTO srv_tipo_notificacion (codigo_notificacion, nombre) VALUES (?, ?)";
            $value = [
                $codigo,
                strtoupper($detalle)
            ];

            $request = $this->insert($sql, $value);

            return $request;
        }

        public function buscaDatosGuardados() {
            $sql = "SELECT id,
            codigo_notificacion as codigo,
            nombre
            FROM srv_tipo_notificacion
            ORDER BY id";
            $data = $this->select_all($sql);

            return $data;
        }

        public function buscaInformacionEditar(int $id) {
            $sql = "SELECT codigo_notificacion as codigo,
            nombre
            FROM srv_tipo_notificacion WHERE id = $id";
            $data = $this->select($sql);

            return $data;
        }

        public function actualizaDatos(int $id, int $codigo, string $nombre) {
            $sql = "UPDATE srv_tipo_notificacion SET codigo_notificacion = ?, nombre = ? WHERE id = $id";
            $value = [
                $codigo,
                strtoupper($nombre)
            ];

            $request = $this->update($sql, $value);

            return $request;
        }
    }
?>