<?php
    require_once '../../../Librerias/core/Helpers.php';
    require_once '../../../Librerias/core/Mysql.php';
    session_start();
    date_default_timezone_set("America/Bogota");
    
    class radicarModel extends Mysql {
        public function getUsersSp() {
            $sql = "SELECT C.id,
            C.cod_usuario,
            C.cod_catastral,
            DC.direccion,
            UPPER(CONCAT(T.nombre1, ' ', T.nombre2, ' ', T.apellido1, ' ', T.apellido2)) AS nombre, 
            UPPER(T.razonsocial) AS razonsocial,
            T.cedulanit AS documento,
            T.tipodoc,
            T.celular, 
            T.email
            FROM srvclientes AS C
            INNER JOIN terceros AS T
            ON C.id_tercero = T.id_tercero
            INNER JOIN srvdireccion_cliente AS DC
            ON C.id = DC.id_cliente
            WHERE C.estado = 'S'
            ORDER BY C.cod_usuario";
            $data = $this->select_all($sql);

            return $data;
        }

        public function getTipoTramites(){
            $sql = "SELECT id, codigo_tramite, nombre, tiempo_respuesta FROM srv_tipo_tramite ORDER BY id";
            $data = $this->select_all($sql);

            return $data;
        }

        public function getTipoCausal() {
            $sql = "SELECT id, codigo_causal, grupo_causal FROM srv_tipo_causal ORDER BY id";
            $data = $this->select_all($sql);

            return $data;
        }

        public function detalleCausal(int $tipoCausalId) {
            $sql = "SELECT id, 
            codigo_causal_det AS codigoCausalDet, 
            detalle, 
            descripcion, 
            acueducto, 
            alcantarillado, 
            aseo FROM srv_causal_det 
            WHERE tipo_causal_id = $tipoCausalId ORDER BY id";
            $data = $this->select_all($sql);

            return $data;
        }

        public function insertData(int $consecutivo, array $data){
            $sql = "INSERT INTO srv_radicar_pqr (consecutivo, fecha, cliente_id, nombre, documento, celular, email, direccion, tipo_tramite_id, tiempo_respuesta, tipo_causal_id, numero_factura, causal_det_id, acueducto, alcantarillado, aseo, descripcion, estado) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

            $sqlTramite = "SELECT tiempo_respuesta AS dias FROM srv_tipo_tramite WHERE id = $data[tramiteId]";
            $tiempo = $this->select($sqlTramite);

            $values = [
                $consecutivo,
                "$data[fecha]",
                intval($data["usuarioId"]),
                strClean(replaceChar("$data[nombre]")),
                intval($data["documento"]),
                intval($data["celular"]),
                strClean(replaceChar("$data[email]")),
                strClean(replaceChar("$data[direccion]")),
                $data["tramiteId"],
                intval($tiempo["dias"]),
                $data["causalId"],
                intval($data["numFactura"]),
                $data["detalleCausalId"],
                intval($data["acueducto"]),
                intval($data["alcantarillado"]),
                intval($data["aseo"]),
                strClean(replaceChar("$data[descripcion]")),
                'S'
            ];

            $resp = $this->insert($sql, $values);

            $sql_funcion = "SELECT id FROM srv_funciones WHERE nombre = 'Radicar Pqr'";
            $funcion = $this->select($sql_funcion);
            insertAuditoria("srv_auditoria","id_srv_funciones",$funcion["id"],"Crear",$consecutivo);

            return $resp;
        }

        public function insertImages(string $tmp, string $type, int $consecutivo) {
            $id = searchConsec("srv_radicar_pqr_anexos", "id");
            $name = "anexoRadicado".$id.".".$type;
            $url_temp = $tmp;
            $destino = '../../../informacion/proyectos/temp/'.$name;
            $request = move_uploaded_file($url_temp, $destino);

            if ($request) {
                $sql = "INSERT INTO srv_radicar_pqr_anexos (pqr_id, name) VALUES (?, ?)";
                $values = [$consecutivo, $name];

                $request2 = $this->insert($sql, $values);

                return $request2;
            }
        }
        
        public function selectConsecutivos(){
            $sql = "SELECT id FROM srv_radicar_pqr";
            $request = $this->select_all($sql);
            return $request;
        }

        public function searchData(int $id){
            $sql = "SELECT SRP.consecutivo,
            SRP.fecha,
            SC.cod_usuario,
            SRP.nombre,
            SRP.documento,
            SRP.celular,
            SRP.email,
            SRP.direccion,
            STT.nombre as tipoTramite,
            SRP.tiempo_respuesta,
            STC.grupo_causal, 
            SRP.numero_factura,
            SCD.codigo_causal_det,
            SCD.detalle,
            SRP.acueducto,
            SRP.alcantarillado,
            SRP.aseo,
            SRP.descripcion,
            SRP.estado
            FROM srv_radicar_pqr AS SRP
            RIGHT JOIN srvclientes AS SC
            ON SRP.cliente_id = SC.id
            RIGHT JOIN srv_tipo_tramite AS STT
            ON SRP.tipo_tramite_id = STT.id 
            RIGHT JOIN srv_tipo_causal AS STC
            ON SRP.tipo_causal_id = STC.id
            RIGHT JOIN srv_causal_det AS SCD
            ON SRP.causal_det_id = SCD.id
            WHERE SRP.id = $id";
            $data = $this->select($sql);

            $data["acueducto"] = intval($data["acueducto"]);
            $data["alcantarillado"] = intval($data["alcantarillado"]);
            $data["aseo"] = intval($data["aseo"]);

            return $data;
        }

        public function searchImages(int $id) {
            $sql = "SELECT name AS nameImage FROM srv_radicar_pqr_anexos WHERE pqr_id = $id";
            $data = $this->select_all($sql);

            foreach ($data as $i => $d) {
                $data[$i]["url"] = "informacion/proyectos/temp/".$d["nameImage"];
            }
            
            return $data;
        }

        public function searchRadicados() {
            $sql = "SELECT SRP.id,
            SRP.consecutivo,
            SRP.fecha,
            SRP.nombre,
            SRP.documento,
            STT.nombre as tipoTramite,
            SRP.tiempo_respuesta,
            STC.grupo_causal, 
            SCD.detalle,
            SRP.estado
            FROM srv_radicar_pqr AS SRP
            INNER JOIN srv_tipo_tramite AS STT
            ON SRP.tipo_tramite_id = STT.id 
            INNER JOIN srv_tipo_causal AS STC
            ON SRP.tipo_causal_id = STC.id
            INNER JOIN srv_causal_det AS SCD
            ON SRP.causal_det_id = SCD.id
            ORDER BY SRP.consecutivo DESC";
            $data = $this->select_all($sql);

            foreach ($data as $key => $d) {
                $days = "+" . $d["tiempo_respuesta"] . " days";
                $fecha = new DateTime($d["fecha"]);
                $fecha -> modify($days);
                $fechaLimite = $fecha->format("Y-m-d");
                $data[$key]["fechaLimite"]  = $fechaLimite;
                $d["nombre"] = preg_replace("/\s+/", " ", trim($d["nombre"]));
            }

            return $data;
        }

        public function anularDocumento(int $id) {
            $sql = "UPDATE srv_radicar_pqr SET estado = ? WHERE id = $id";
            $request = $this->update($sql, ['N']);

            $sql_consecutivo = "SELECT consecutivo FROM srv_radicar_pqr WHERE id = $id";
            $pqr = $this->select($sql_consecutivo);

            $sql_funcion = "SELECT id FROM srv_funciones WHERE nombre = 'Radicar Pqr'";
            $funcion = $this->select($sql_funcion);

            insertAuditoria("srv_auditoria","id_srv_funciones",$funcion["id"],"Anular",$pqr["consecutivo"]);

            return $request;
        }
    }
?>