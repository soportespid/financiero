<?php
    require_once '../../../Librerias/core/Helpers.php';
    require_once '../../../Librerias/core/Mysql.php';
    session_start();
    date_default_timezone_set("America/Bogota");
    
    class respuestaModel extends Mysql {
        public function documentosRadicados(){
            $sql = "SELECT SRP.id, 
            SRP.consecutivo, 
            STT.nombre AS nombreTramite, 
            STC.grupo_causal AS nombreCausal, 
            SCD.detalle AS detalleCausal,
            SRP.descripcion 
            FROM srv_radicar_pqr AS SRP
            INNER JOIN srv_tipo_tramite AS STT
            ON SRP.tipo_tramite_id = STT.id
            INNER JOIN srv_tipo_causal AS STC
            ON SRP.tipo_causal_id = STC.id
            INNER JOIN srv_causal_det AS SCD
            ON SRP.causal_det_id = SCD.id
            WHERE estado = 'S'
            ORDER BY id";
            $data = $this->select_all($sql);
            return $data;
        }

        public function tipoRespuestas() {
            $sql = "SELECT id, codigo_respuesta, nombre FROM srv_tipo_respuesta ORDER BY id";
            $data = $this->select_all($sql);
            return $data;
        }

        public function tipoNotificaciones() {
            $sql = "SELECT id, codigo_notificacion, nombre FROM srv_tipo_notificacion ORDER BY id";
            $data = $this->select_all($sql);
            return $data;
        }

        public function insertDataRespuesta(int $consecutivo, array $data) {
            $sql = "INSERT INTO srv_respuesta_pqr (consecutivo, fecha, pqr_id, tipo_respuesta_id, tipo_notificacion_id, descripcion, estado) VALUES (?, ?, ?, ?, ?, ?, ?)";

            $values = [
                $consecutivo,
                "$data[fecha]",
                $data["radicadoId"],
                $data["tipoRespuestaId"],
                $data["tipoNotificacionId"],
                strClean(replaceChar("$data[descripcion]")),
                "S"
            ];
            $request = $this->insert($sql, $values);

            $sql_funcion = "SELECT id FROM srv_funciones WHERE nombre = 'Respuesta Pqr'";
            $funcion = $this->select($sql_funcion);
            insertAuditoria("srv_auditoria","id_srv_funciones",$funcion["id"],"Crear",$consecutivo);

            return $request;
        }

        public function updateEstadoRadicado(int $radicadoId) {
            $sql = "UPDATE srv_radicar_pqr SET estado = ? WHERE id = $radicadoId";
            $request = $this->update($sql, ['R']);
            return $request;
        }

        public function insertImages(string $tmp, string $type, int $consecutivo) {
            $id = searchConsec("srv_respuesta_pqr_anexos", "id");
            $name = "anexoRespuesta".$id.".".$type;
            $url_temp = $tmp;
            $destino = '../../../informacion/proyectos/temp/'.$name;
            $request = move_uploaded_file($url_temp, $destino);

            if ($request) {
                $sql = "INSERT INTO srv_respuesta_pqr_anexos (respuesta_id, name) VALUES (?, ?)";
                $values = [$consecutivo, $name];

                $request2 = $this->insert($sql, $values);

                return $request2;
            }
        }

        public function searchDataVisualize(int $id) {
            $sql = "SELECT SRP.id,
            SRP.consecutivo AS consecutivo,
            SRP.fecha,
            SCP.consecutivo AS consecutivoRadicado,
            SCP.descripcion AS descripcionRadicado,
            STR.nombre AS nombreTipoRespuesta,
            STN.nombre AS nombreTipoNotificacion,
            SRP.descripcion AS descripcion,
            SRP.estado,
            SCP.nombre,
            SCP.documento,
            SCP.celular,
            SCP.email,
            SCP.direccion,
            SC.cod_usuario
            FROM srv_respuesta_pqr AS SRP
            INNER JOIN srv_radicar_pqr AS SCP
            ON SRP.pqr_id = SCP.id
            INNER JOIN srv_tipo_respuesta AS STR
            ON SRP.tipo_respuesta_id = STR.id
            INNER JOIN srv_tipo_notificacion AS STN
            ON SRP.tipo_notificacion_id = STN.id
            INNER JOIN srvclientes AS SC
            ON SCP.cliente_id = SC.id
            WHERE SRP.id = $id";
            $data = $this->select($sql);
            return $data;
        }

        public function searchImages(int $id) {
            $sql = "SELECT name AS nameImage FROM srv_respuesta_pqr_anexos WHERE respuesta_id = $id";
            $data = $this->select_all($sql);

            foreach ($data as $i => $d) {
                $data[$i]["url"] = "informacion/proyectos/temp/".$d["nameImage"];
            }
            
            return $data;
        }

        public function selectConsecutivos(){
            $sql = "SELECT id FROM srv_respuesta_pqr";
            $request = $this->select_all($sql);
            return $request;
        }

        public function searchData() {
            $sql = "SELECT SRP.id,
            SRP.consecutivo AS consecutivo,
            SRP.fecha,
            SCP.consecutivo AS consecutivoRadicado,
            STR.nombre AS nombreTipoRespuesta,
            STN.nombre AS nombreTipoNotificacion,
            SRP.estado,
            SCP.nombre,
            SCP.documento
            FROM srv_respuesta_pqr AS SRP
            INNER JOIN srv_radicar_pqr AS SCP
            ON SRP.pqr_id = SCP.id
            INNER JOIN srv_tipo_respuesta AS STR
            ON SRP.tipo_respuesta_id = STR.id
            INNER JOIN srv_tipo_notificacion AS STN
            ON SRP.tipo_notificacion_id = STN.id
            INNER JOIN srvclientes AS SC
            ON SCP.cliente_id = SC.id
            ORDER BY SRP.consecutivo DESC";
            $data = $this->select_all($sql);
            return $data;
        }

        public function anular(int $id, int $radicadoId) {
            $sqlRespuesta = "UPDATE srv_respuesta_pqr SET estado = ? WHERE id = $id";
            $sqlRadicado = "UPDATE srv_radicar_pqr SET estado = ?  WHERE consecutivo = $radicadoId";

            $requestResp = $this->update($sqlRespuesta, ['N']);
            
            if ($requestResp > 0) {
                $this->update($sqlRadicado, ['S']);

                $sql_consecutivo = "SELECT consecutivo FROM srv_respuesta_pqr WHERE id = $id";
                $pqr = $this->select($sql_consecutivo);

                $sql_funcion = "SELECT id FROM srv_funciones WHERE nombre = 'Respuesta Pqr'";
                $funcion = $this->select($sql_funcion);

                insertAuditoria("srv_auditoria","id_srv_funciones",$funcion["id"],"Anular",$pqr["consecutivo"]);
            }

            return $requestResp;
        }
    }
?>