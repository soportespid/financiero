const URL = 'servicios_publicos/pqr/controllers/radicarPqr_controller.php';
const URLPDF = "serv-radicarPqrPdf.php";
const URLEXCEL = "serv-buscaRadicadosPqrExcel.php";

var app = new Vue({
    el:"#myapp",
    data() {
        return {
            isLoading: false,
            radicar: true,
            anexo: false,
            modalUser: false,
            modalDetalleCausal: false,
            
            dataUsuarios: [],
            dataUsuariosCopy: [],
            dataTramites: [],
            dataCausales: [],
            dataCausalDetalles: [],
            dataCausalDetallesCopy: [],

            data: {
                fecha: '',
                usuarioId: 0,
                codigoUsuario: '',
                nombre: '',
                documento: '',
                celular: '',
                email: '',
                direccion: '',
                tramiteId: 0,
                causalId: 0,
                numFactura: 0,
                detalleCausalId: 0,
                codigoDetalleCausal: '',
                detalleCausal: '',
                acueducto: 0,
                alcantarillado: 0,
                aseo: 0,
                descripcion: ''
            },
            images: [],
            imagesPreview: [],

            info: [],
            dataSearch: [],
            dataSearch_copy: [],
            txtSearch: '',
            txtResultados: 0,
            arrConsecutivos:[],
            id: 0,
            fechaActual: '',
        }
    },
    mounted() {
        this.intPageVal = this.$refs.pageType.value;
        if (this.intPageVal == 1) {
            this.getDataCreate();
        } else if (this.intPageVal == 2) {
            this.getDataSearch();
            const fecha = new Date();
            const fechaFormateada = fecha.toISOString().split('T')[0];
            this.fechaActual = fechaFormateada;
        } else {
            this.getDataVisualize();
        }
    },
    methods: {
        //trae datos del crear
        async getDataCreate() {
            const formData = new FormData();
            formData.append("action","getDataCreate");

            this.isLoading = true;
            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();
            this.isLoading = false;

            this.dataUsuarios = this.dataUsuariosCopy = objData.usuarios;
            this.txtResultados = this.dataUsuariosCopy.length;  
            this.dataTramites = objData.tramites;
            this.dataCausales = objData.causales;
        },

        //trae datos guardados en el crear
        async getDataVisualize(){
            this.id = new URLSearchParams(window.location.search).get('id');
            const formData = new FormData();
            formData.append("action","getDataVisualize");
            formData.append("id",this.id);

            this.isLoading = true;
            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();
            if(objData.status) {
                this.info = objData.data;
                this.arrConsecutivos = objData.consecutivos;
                this.images = objData.dataImages;
            }
            this.isLoading = false;
        },

        //trae datos para el buscar
        async getDataSearch(){
            const formData = new FormData();
            formData.append("action","getDataSearch");

            this.isLoading = true;
            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();
            if(objData.status) {
                this.dataSearch = this.dataSearch_copy = objData.data;
                this.txtResultados = this.dataSearch_copy.length;
            }
            this.isLoading = false;
        },

        //valida y despliega modal causal detalles
        async showModalCausal() {
            if (this.data.causalId != 0) {
                const formData = new FormData();
                formData.append("action","getCausalDetalle");
                formData.append("causalId", this.data.causalId);
                const response = await fetch(URL,{method:"POST",body:formData});
                const objData = await response.json();

                this.dataCausalDetallesCopy = this.dataCausalDetalles = objData.detalleCausal;
                this.modalDetalleCausal = true;
            }
            else {
                Swal.fire("Atención!","Debe seleccionar primero un tipo de causal","warning");
            }
        },

        async searchData(option) {
            let search = "";

            switch (option) {
                case "modalUsu":
                    search = this.txtSearch.toLowerCase();
                    this.dataUsuariosCopy = [...this.dataUsuarios.filter(e=>e.cod_usuario.toLowerCase().includes(search) || e.documento.toLowerCase().includes(search) || e.cod_catastral.toLowerCase().includes(search))];
                    this.txtResultados = this.dataUsuariosCopy.length;
                    break;

                case "inputUsu":
                    search = this.data.codigoUsuario.toLowerCase();
                    let dataInfo = [];
                    dataInfo = this.dataUsuariosCopy.filter(e=>e.cod_usuario==search)[0];
                    
                    if (dataInfo === undefined) {
                        Swal.fire("Atención!","Código de usuario no encontrado","warning");
                        this.data.codigoUsuario = "";
                    }
                    else {
                        this.data.usuarioId = dataInfo.id;
                        this.data.codigoUsuario = dataInfo.cod_usuario;
                        this.data.nombre = dataInfo.tipodoc == 31 ? dataInfo.razonsocial : dataInfo.nombre;
                        this.data.documento = dataInfo.documento;
                        this.data.celular = dataInfo.celular;
                        this.data.email = dataInfo.email;
                        this.data.direccion = dataInfo.direccion;
                    }
                    break;
            
                case "modalDet":
                    search = this.txtSearch.toLowerCase();
                    this.dataCausalDetallesCopy = [...this.dataCausalDetalles.filter(e=>e.codigoCausalDet.toLowerCase().includes(search) || e.detalle.toLowerCase().includes(search) )];
                    break;

                case 'inputSearch':
                    search = this.txtSearch.toLowerCase();
                    this.dataSearch_copy = [...this.dataSearch.filter(e=>e.consecutivo.toLowerCase().includes(search) || e.documento.toLowerCase().includes(search) || e.nombre.toLowerCase().includes(search))];
                    this.txtResultados = this.dataSearch_copy.length;
                    break;

                default:
                    console.log("Error, opción no encontrada");
                    break;
            }    
        },

        selectItemModal(item, type) {
            switch (type) {
                case "usuarioModel":
                    this.data.usuarioId = item.id;
                    this.data.codigoUsuario = item.cod_usuario;
                    this.data.nombre = item.tipodoc == 31 ? item.razonsocial : item.nombre;
                    this.data.documento = item.documento;
                    this.data.celular = item.celular;
                    this.data.email = item.email;
                    this.data.direccion = item.direccion;
                    this.dataUsuariosCopy = this.dataUsuarios;
                    this.txtSearch = "";
                    this.modalUser = false;
                    break;

                case "causalModel":
                    this.data.detalleCausalId = item.id;
                    this.data.codigoDetalleCausal = item.codigoCausalDet;
                    this.data.detalleCausal = item.detalle;
                    this.data.acueducto = parseInt(item.acueducto);
                    this.data.alcantarillado = parseInt(item.alcantarillado);
                    this.data.aseo = parseInt(item.aseo);
                    this.txtSearch = "";
                    this.modalDetalleCausal = false;
                    break;
            
                default:
                    console.log("Error tipo de selección no encontrada");
                    break;
            }
        },

        //manejo de imagenes
        previewImages(event) {
            // Limpiar el array de imágenes previo
            this.images = [];
            this.imagesPreview = [];
    
            // Obtener los archivos seleccionados
            const files = event.target.files;

            // Iterar sobre cada archivo y generar una vista previa
            for (let i = 0; i < files.length; i++) {
                const file = files[i];
                // Verificar que el archivo sea una imagen
                if ((file && file.type.startsWith('image/')) || (file && file.type.startsWith('application/pdf'))) {
                    this.images.push(file);
                    const reader = new FileReader();

                    // Al cargar el archivo, agregar la URL al array de imágenes
                    reader.onload = e => {
                        this.imagesPreview.push(e.target.result);
                    };
                    // Leer el archivo como una URL de datos
                    reader.readAsDataURL(file);
                } else {
                    Swal.fire("Atención!","El archivo seleccionado no es una imagen.","warning");
                }
            }
        },

        deleteImageAdded(index) {
            this.imagesPreview.splice(index, 1);
            this.images.splice(index, 1); 
        },

        //guardados
        async saveCreate() {
            if (this.data.fecha == "" || this.data.usuarioId == 0 || this.data.nombre == "" && this.data.documento == "" || this.data.celular == "" || this.data.direccion == "") {
                Swal.fire("Atención!","Todos los campos de la información del usuario con * son obligatorios","warning");
                return false;
            }
            if (this.data.tramiteId == 0 || this.data.causalId == 0 || this.data.detalleCausalId == 0) {
                Swal.fire("Atención!","Todos los campos del tipo de radicación con * son obligatorios","warning");
                return false;
            }
            if (this.data.causalId == 1 && this.data.numFactura == 0) {
                Swal.fire("Atención!","Debes colocar un número de factura","warning");
                return false;
            }
            if (this.data.acueducto == 0 && this.data.alcantarillado == 0 && this.data.aseo == 0) {
                Swal.fire("Atención!","Debes seleccionar algún servicio al cual va dirigido la PQR","warning");
                return false;
            } 
            
            this.data.acueducto = this.data.acueducto === 1 ? 1: 0;
            this.data.alcantarillado = this.data.alcantarillado === 1 ? 1: 0;
            this.data.aseo = this.data.aseo === 1 ? 1: 0;
            const formData = new FormData();
            formData.append("action","saveCreate");
            formData.append("data", JSON.stringify(this.data));
            
            for (let i = 0; i < this.images.length; i++) {
                formData.append('images[]', this.images[i]);
            }

            Swal.fire({
                title:"¿Estás segur@ de guardar?",
                text:"",
                icon: 'warning',
                showCancelButton:true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText:"Sí, guardar",
                cancelButtonText:"No, cancelar"
            }).then(async function(result) {
                if(result.isConfirmed) {
                    // app.isLoading = true;
                    const response = await fetch(URL,{method:"POST",body:formData});
                    const objData = await response.json();
                    app.isLoading = false;
                    if (objData.status) {
                        Swal.fire("Guardado",objData.msg,"success");
                        if(objData.id){
                            setTimeout(function(){
                                window.location.href="serv-visualizaRadicadoPqr"+'?id='+objData.id;
                            },1500);
                        }
                    }
                    else {
                        Swal.fire("Error!",objData.msg,"warning");
                    }
                }
            });
        },
        
        async anulaRadicar(id, index) {
            const formData = new FormData();
            formData.append("action","anulaDocument");
            formData.append("radicarId", parseInt(id));

            Swal.fire({
                title:"¿Estás segur@ de anular?",
                text:"",
                icon: 'warning',
                showCancelButton:true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText:"Sí, anular",
                cancelButtonText:"No, cancelar"
            }).then(async function(result) {
                if(result.isConfirmed) {
                    const response = await fetch(URL,{method:"POST",body:formData});
                    const objData = await response.json();
                    app.isLoading = false;
                    if (objData.status) {
                        Swal.fire("Anulado",objData.msg,"success");
                        app.dataSearch_copy[index].estado = 'N';
                        app.dataSearch[index].estado = 'N';
                    }
                    else {
                        Swal.fire("Error!",objData.msg,"warning");
                    }
                }
            });
        },

        formatNumero: function(valor){
            return new Intl.NumberFormat("en-US", {style: "currency", currency: "USD"}).format(valor);
            
        },

        formatFecha: function(fecha) {
            // Separar la fecha en componentes
            const parts = fecha.split('-');
            if (parts.length !== 3) {
                throw new Error('Formato de fecha inválido. Use Y-m-d.');
            }
            const year = parts[0];
            const month = parts[1];
            const day = parts[2];
        
            // Retornar la fecha en el formato deseado
            return `${day}/${month}/${year}`;
        },

        nextItem:function(type){
            let id = this.id;
            let index = this.arrConsecutivos.findIndex(function(e){return e.id == id});
            if(type=="next" && this.arrConsecutivos[++index]){
                id = this.arrConsecutivos[index++].id;
            }else if(type=="prev" && this.arrConsecutivos[--index]){
                id = this.arrConsecutivos[index--].id;
            }
            window.location.href="serv-visualizaRadicadoPqr"+'?id='+id;
        },

        pdf: function () {    
            const form = document.createElement("form");
            form.method = "post";
            form.target = "_blank";
            form.action = URLPDF;       
            
            function addField(name,value){
                const input = document.createElement("input");
                input.type="hidden";
                input.name=name;
                input.value = value;
                form.appendChild(input);
            }

            addField("data", JSON.stringify(this.info));
        
            document.body.appendChild(form);
            form.submit();
            document.body.removeChild(form);
        },

        excel: function() {
            const form = document.createElement("form");
            form.method = "post";
            form.target = "_blank";
            form.action = URLEXCEL;       
            
            function addField(name,value){
                const input = document.createElement("input");
                input.type="hidden";
                input.name=name;
                input.value = value;
                form.appendChild(input);
            }

            addField("data", JSON.stringify(this.dataSearch_copy));
        
            document.body.appendChild(form);
            form.submit();
            document.body.removeChild(form);
        }
    },
    computed:{

    }
})
