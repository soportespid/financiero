const URL = 'servicios_publicos/pqr/controllers/informePqr_controller.php';
const URLPDF = "serv-respuestaPqrPdf.php";
const URLEXCEL = "serv-informePqrExcel.php";

var app = new Vue({
    el:"#myapp",
    data() {
        return {
            isLoading: false,
            fechaIni: '',
            fechaFin: '',
            servicio: '0',
            servicios: [],
            data: [],
        }
    },
    mounted() {
        this.getData();
    },
    methods: {
        //trae datos del crear
        async getData() {
            const formData = new FormData();
            formData.append("action","get_data");
            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();
            this.servicios = objData.servicios;
        },

        async searchInfo() {
            if (this.servicio == "0" && this.fechaIni == "" && this.fechaFin == "") {
                Swal.fire("Atención!","Todos los campos con * son obligatorios","warning");
                return
            }

            if (this.fechaFin < this.fechaIni) {
                Swal.fire("Atención!","La fecha final no puede ser menor a la fecha inicial","warning");
                return
            }

            const formData = new FormData();
            formData.append("action","search_data");
            formData.append("fechaIni", this.fechaIni);
            formData.append("fechaFin", this.fechaFin);
            formData.append("servicio", this.servicio);
            // this.isLoading = true;
            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();
            this.isLoading = false;
            this.data = objData.data;
        },

        formatFecha: function(fecha) {
            // Separar la fecha en componentes
            const parts = fecha.split('-');
            if (parts.length !== 3) {
                throw new Error('Formato de fecha inválido. Use Y-m-d.');
            }
            const year = parts[0];
            const month = parts[1];
            const day = parts[2];
        
            // Retornar la fecha en el formato deseado
            return `${day}/${month}/${year}`;
        },

        pdf: function () {    
            const form = document.createElement("form");
            form.method = "post";
            form.target = "_blank";
            form.action = URLPDF;       
            
            function addField(name,value){
                const input = document.createElement("input");
                input.type="hidden";
                input.name=name;
                input.value = value;
                form.appendChild(input);
            }

            addField("data", JSON.stringify(this.info));
        
            document.body.appendChild(form);
            form.submit();
            document.body.removeChild(form);
        },

        excel: function() {
            const form = document.createElement("form");
            form.method = "post";
            form.target = "_blank";
            form.action = URLEXCEL;       
            
            function addField(name,value){
                const input = document.createElement("input");
                input.type="hidden";
                input.name=name;
                input.value = value;
                form.appendChild(input);
            }

            addField("data", JSON.stringify(this.data));
            addField("servicio", this.servicio);
        
            document.body.appendChild(form);
            form.submit();
            document.body.removeChild(form);
        }
    },
    computed:{

    }
})
