const URL = 'servicios_publicos/pqr/controllers/respuestaPqr_controller.php';
const URLPDF = "serv-respuestaPqrPdf.php";
const URLEXCEL = "serv-buscaRespuestasPqrExcel.php";

var app = new Vue({
    el:"#myapp",
    data() {
        return {
            isLoading: false,
            respuesta: true,
            anexo: false,
            modalRadicados: false,
            pqrRadicados: [],
            pqrRadicadosCopy: [],
            respuestas: [],
            notificaciones: [],

            data: {
                fecha: '',
                radicadoId: 0,
                consecutivoRadicado: '',
                tipoTramite: '',
                tipoCausal: '',
                detalleCausal: '',
                descripcionRadicado: '',
                tipoRespuestaId: 0,
                tipoNotificacionId: 0,
                descripcion: ''
            },
            images: [],
            imagesPreview: [],

            info: [],
            dataSearch: [],
            dataSearch_copy: [],
            txtSearch: '',
            txtResultados: 0,
            arrConsecutivos:[],
            id: 0,
        }
    },
    mounted() {
        this.intPageVal = this.$refs.pageType.value;
        if (this.intPageVal == 1) {
            this.getDataCreate();
        } else if (this.intPageVal == 2) {
            this.getDataSearch();
        } else {
            this.getDataVisualize();
        }
    },
    methods: {
        //trae datos del crear
        async getDataCreate() {
            const formData = new FormData();
            formData.append("action","getDataCreate");

            this.isLoading = true;
            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();
            this.isLoading = false;

            this.respuestas = objData.respuestas;
            this.notificaciones = objData.notificaciones;
            this.pqrRadicados = this.pqrRadicadosCopy = objData.pqrRadicados;
            this.txtResultados = this.pqrRadicadosCopy.length;
        },

        //trae datos guardados por medio del id en la URL
        async getDataVisualize(){
            this.id = new URLSearchParams(window.location.search).get('id');
            const formData = new FormData();
            formData.append("action","getDataVisualize");
            formData.append("id",this.id);

            this.isLoading = true;
            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();
            if(objData.status) {
                this.info = objData.data;
                this.arrConsecutivos = objData.consecutivos;
                this.images = objData.dataImages;
            }
            this.isLoading = false;
        },

        //trae datos para el buscar
        async getDataSearch(){
            const formData = new FormData();
            formData.append("action","getDataSearch");

            this.isLoading = true;
            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();
            if(objData.status) {
                this.dataSearch = this.dataSearch_copy = objData.data;
                this.txtResultados = this.dataSearch_copy.length;
            }
            this.isLoading = false;
        },

        async searchData(option) {
            let search = "";

            switch (option) {
                case "modalRadicados":
                    search = this.txtSearch.toLowerCase();
                    this.pqrRadicadosCopy = [...this.pqrRadicados.filter(e=>e.consecutivo.toLowerCase().includes(search) || e.nombreTramite.toLowerCase().includes(search) )];
                    this.txtResultados = this.pqrRadicadosCopy.length;
                    break;

                case "inputSearch":
                    search = this.txtSearch.toLowerCase();
                    this.dataSearch_copy = [...this.dataSearch.filter(e=>e.consecutivo.toLowerCase().includes(search) || e.nombre.toLowerCase().includes(search) || e.documento.toLowerCase().includes(search))];
                    this.txtResultados = this.pqrRadicadosCopy.length;
                    break;
                default:
                    console.log("Error, opción no encontrada");
                    break;
            }    
        },

        selectItemModal(item, type) {
            switch (type) {
                case "modalRadicados":
                        this.data.radicadoId = item.id;
                        this.data.consecutivoRadicado = item.consecutivo;
                        this.data.tipoTramite = item.nombreTramite;
                        this.data.tipoCausal = item.nombreCausal;
                        this.data.detalleCausal = item.detalleCausal;
                        this.data.descripcionRadicado = item.descripcion;
                        this.pqrRadicadosCopy = this.pqrRadicados;
                        this.txtSearch = "";
                        this.modalRadicados = false;
                    break;

                default:
                    console.log("Error tipo de selección no encontrada");
                    break;
            }
        },

        //manejo de imagenes
        previewImages(event) {
            // Limpiar el array de imágenes previo
            this.images = [];
            this.imagesPreview = [];
    
            // Obtener los archivos seleccionados
            const files = event.target.files;

            // Iterar sobre cada archivo y generar una vista previa
            for (let i = 0; i < files.length; i++) {
                const file = files[i];
                // Verificar que el archivo sea una imagen
                if ((file && file.type.startsWith('image/')) || (file && file.type.startsWith('application/pdf'))) {
                    this.images.push(file);
                    const reader = new FileReader();

                    // Al cargar el archivo, agregar la URL al array de imágenes
                    reader.onload = e => {
                        this.imagesPreview.push(e.target.result);
                    };
                    // Leer el archivo como una URL de datos
                    reader.readAsDataURL(file);
                } else {
                    Swal.fire("Atención!","El archivo seleccionado no es una imagen.","warning");
                }
            }
        },

        deleteImageAdded(index) {
            this.imagesPreview.splice(index, 1);
            this.images.splice(index, 1);
        },

        async save() {
            if (this.data.fecha == "" && this.data.radicadoId == 0 && this.data.consecutivoRadicado == 0 && this.data.tipoRespuestaId == 0 && this.data.tipoNotificacionId == 0) {
                Swal.fire("Atención!","Todos los campos con * son obligatorios","warning");
                return false;
            }

            const formData = new FormData();
            formData.append("action","saveCreate");
            formData.append("data", JSON.stringify(this.data));
            
            for (let i = 0; i < this.images.length; i++) {
                formData.append('images[]', this.images[i]);
            }

            Swal.fire({
                title:"¿Estás segur@ de guardar?",
                text:"",
                icon: 'warning',
                showCancelButton:true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText:"Sí, guardar",
                cancelButtonText:"No, cancelar"
            }).then(async function(result) {
                if(result.isConfirmed) {
                    const response = await fetch(URL,{method:"POST",body:formData});
                    const objData = await response.json();
                    if (objData.status) {
                        Swal.fire("Guardado",objData.msg,"success");
                        if(objData.id){
                            setTimeout(function(){
                                window.location.href="serv-visualizaRespuestaPqr"+'?id='+objData.id;
                            },1500);
                        }
                    }
                    else {
                        Swal.fire("Error!",objData.msg,"warning");
                    }
                }
            });
        },
        
        async anulaRadicar(id, radicadoId, index) {
            const formData = new FormData();
            formData.append("action","anulaDocument");
            formData.append("id", parseInt(id));
            formData.append("conseRadicado", parseInt(radicadoId));

            Swal.fire({
                title:"¿Estás segur@ de anular?",
                text:"",
                icon: 'warning',
                showCancelButton:true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText:"Sí, anular",
                cancelButtonText:"No, cancelar"
            }).then(async function(result) {
                if(result.isConfirmed) {
                    const response = await fetch(URL,{method:"POST",body:formData});
                    const objData = await response.json();
                    app.isLoading = false;
                    if (objData.status) {
                        Swal.fire("Anulado",objData.msg,"success");
                        app.dataSearch_copy[index].estado = 'N';
                        app.dataSearch[index].estado = 'N';
                    }
                    else {
                        Swal.fire("Error!",objData.msg,"warning");
                    }
                }
            });
        },

        formatNumero: function(valor){
            return new Intl.NumberFormat("en-US", {style: "currency", currency: "USD"}).format(valor);
            
        },

        formatFecha: function(fecha) {
            // Separar la fecha en componentes
            const parts = fecha.split('-');
            if (parts.length !== 3) {
                throw new Error('Formato de fecha inválido. Use Y-m-d.');
            }
            const year = parts[0];
            const month = parts[1];
            const day = parts[2];
        
            // Retornar la fecha en el formato deseado
            return `${day}/${month}/${year}`;
        },

        nextItem:function(type){
            let id = this.id;
            let index = this.arrConsecutivos.findIndex(function(e){return e.id == id});
            if(type=="next" && this.arrConsecutivos[++index]){
                id = this.arrConsecutivos[index++].id;
            }else if(type=="prev" && this.arrConsecutivos[--index]){
                id = this.arrConsecutivos[index--].id;
            }
            window.location.href="serv-visualizaRespuestaPqr"+'?id='+id;
        },

        pdf: function () {    
            const form = document.createElement("form");
            form.method = "post";
            form.target = "_blank";
            form.action = URLPDF;       
            
            function addField(name,value){
                const input = document.createElement("input");
                input.type="hidden";
                input.name=name;
                input.value = value;
                form.appendChild(input);
            }

            addField("data", JSON.stringify(this.info));
        
            document.body.appendChild(form);
            form.submit();
            document.body.removeChild(form);
        },

        excel: function() {
            const form = document.createElement("form");
            form.method = "post";
            form.target = "_blank";
            form.action = URLEXCEL;       
            
            function addField(name,value){
                const input = document.createElement("input");
                input.type="hidden";
                input.name=name;
                input.value = value;
                form.appendChild(input);
            }

            addField("data", JSON.stringify(this.dataSearch_copy));
        
            document.body.appendChild(form);
            form.submit();
            document.body.removeChild(form);
        }
    },
    computed:{

    }
})
