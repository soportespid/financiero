const URL = 'servicios_publicos/pqr/controllers/tipoNotificacion_controller.php';

var app = new Vue({
    el:"#myapp",
    data() {
        return {
            isLoading: false,
            data: {
                codigo: '',
                nombre: ''
            },
            dataSearch: [],
            dataSearch_copy: [],
            txtSearch: '',
            txtResultados: 0,
            id: 0
        }
    },
    mounted() {
        this.intPageVal = this.$refs.pageType.value;

        if (this.intPageVal == 2) {
            this.getDataSearch();
        } else if (this.intPageVal == 3) {
            this.getDataEdit();
        }
    },
    methods: {
        //trae datos del buscar
        async getDataSearch() {
            const formData = new FormData();
            formData.append("action","getDataSearch");
            this.isLoading = true;
            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();
            this.isLoading = false;
            this.dataSearch = objData.data;
            this.dataSearch_copy = objData.data;
            this.txtResultados = this.dataSearch_copy.length;
        },

        async getDataEdit() {
            this.id = new URLSearchParams(window.location.search).get('id');
            if (this.id > 0) {
                const formData = new FormData();
                formData.append("action","getDataEdit");
                formData.append("id",this.id);
                this.isLoading = true;
                const response = await fetch(URL,{method:"POST",body:formData});
                const objData = await response.json();
                this.data = objData.data;
                this.tiposCausales = objData.tiposCausales;
                this.isLoading = false;
            }
        },

        //guardado del crear
        async saveCreate() {
            if (this.data.codigo != "" && this.data.nombre != "") {
                Swal.fire({
                    title:"¿Estás segur@ de guardar?",
                    text:"",
                    icon: 'warning',
                    showCancelButton:true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText:"Sí, guardar",
                    cancelButtonText:"No, cancelar"
                }).then(async function(result) {
                    if(result.isConfirmed) {
                        const formData = new FormData();
                        formData.append("action","saveCreate");
                        formData.append("data", JSON.stringify(app.data));
                        app.isLoading = true;
                        const response = await fetch(URL,{method:"POST",body:formData});
                        const objData = await response.json();
                        app.isLoading = false;
                        if (objData.status) {
                            Swal.fire("Guardado",objData.msg,"success");
                            setTimeout(function(){
                                location.href="serv-editaTipoNotificacion?id="+objData.id;
                            },1000);
                        }
                        else {
                            Swal.fire("Error!",objData.msg,"warning");
                        }
                    }
                });
            }
            else {
                Swal.fire("Atención!","Todos los campos con (*) son obligatorios","warning");
            }
        },

        //guardado del editar
        async saveEdit(){
            if (this.data.codigo != "" && this.data.nombre != "") {
                Swal.fire({
                    title:"¿Estás segur@ de guardar?",
                    text:"",
                    icon: 'warning',
                    showCancelButton:true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText:"Sí, guardar",
                    cancelButtonText:"No, cancelar"
                }).then(async function(result) {
                    if(result.isConfirmed) {
                        const formData = new FormData();
                        formData.append("action","saveEdit");
                        formData.append("id",app.id);
                        formData.append("data", JSON.stringify(app.data));
                        // app.isLoading = true;
                        const response = await fetch(URL,{method:"POST",body:formData});
                        const objData = await response.json();
                        app.isLoading = false;
                        if (objData.status) {
                            Swal.fire("Guardado",objData.msg,"success");
                        }
                        else {
                            Swal.fire("Error!",objData.msg,"warning");
                        }
                    }
                });
            }
            else {
                Swal.fire("Atención!","Todos los campos con (*) son obligatorios","warning");
            }
        },

        async searchData() {
            let search = "";
            search = this.txtSearch.toLowerCase();
            
            this.dataSearch_copy = [...this.dataSearch.filter(e=>e.codigo.toLowerCase().includes(search) || e.nombre.toLowerCase().includes(search))];
            this.txtResultados = this.dataSearch_copy.length;
        },
        
        formatNumero: function(valor){
            return new Intl.NumberFormat("en-US", {style: "currency", currency: "USD"}).format(valor);
            
        },

        formatFecha: function(fecha) {
            // Separar la fecha en componentes
            const parts = fecha.split('-');
            if (parts.length !== 3) {
                throw new Error('Formato de fecha inválido. Use Y-m-d.');
            }
            const year = parts[0];
            const month = parts[1];
            const day = parts[2];
        
            // Retornar la fecha en el formato deseado
            return `${day}/${month}/${year}`;
        },
    },
    computed:{

    }
})
