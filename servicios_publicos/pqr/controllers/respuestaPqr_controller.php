<?php
    require_once '../../../Librerias/core/Helpers.php';
    require_once '../models/respuestaPqr_model.php';
    header('Content-Type: application/json');

    session_start();

    class respuestaController extends respuestaModel {
        public function get_data_create() {
            if (!empty($_SESSION)) {
                $arrResponse = array(
                    "status" => true,
                    "pqrRadicados" => $this->documentosRadicados(),
                    "respuestas" => $this->tipoRespuestas(),
                    "notificaciones" => $this->tipoNotificaciones()
                );

                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
        }

        public function save_create() {
            if (!empty($_SESSION)) {
                if ($_POST) {
                    $consecutivo = searchConsec("srv_respuesta_pqr", "consecutivo");
                    $data = json_decode($_POST["data"], true);
                    $imagenes = $_FILES["images"];

                    $respInsert = $this->insertDataRespuesta($consecutivo, $data);

                    if ($respInsert > 0) {

                        $respStatus = $this->updateEstadoRadicado(intval($data["radicadoId"]));
                        if ($respStatus > 0) {
                            if (count($imagenes) > 0) {
                                foreach ($imagenes["tmp_name"] as $key => $img) {
                                    $type = explode("/", $imagenes["type"][$key]);
                                    $this->insertImages($img, $type[1], $consecutivo);
                                }
    
                                $arrResponse = array("status" => true, "msg" => "Respuesta pqr guardado con exito", "id" => $respInsert);
                            }
                            else {
                                $arrResponse = array("status" => true, "msg" => "Respuesta pqr guardado con exito", "id" => $respInsert);
                            }
                        }
                        else {
                            $arrResponse = array("status" => false, "msg" => "Error en cambio de estado de radicado");
                        }
                    }
                    else {
                        $arrResponse = array("status" => false, "msg" => "Error en guardado de datos");
                    }
                }
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
        }

        public function get_data_visualize(){
            if (!empty($_SESSION)) {
                if ($_POST) {
                    $id = intval($_POST["id"]);
                    $arrResponse = array("status" => true, "data" => $this->searchDataVisualize($id), "dataImages" => $this->searchImages($id), "consecutivos" => $this->selectConsecutivos());
                }
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
        }

        public function get_data_search() {
            if (!empty($_SESSION)) {
                if ($_POST) {
                    $arrResponse = array("status" => true, "data" => $this->searchData());
                }
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
        }

        public function anula_document() {
            if (!empty($_SESSION)) {
                if ($_POST){
                    $id = intval($_POST["id"]);
                    $radicadoId = intval($_POST["conseRadicado"]);

                    $request =  $this->anular($id, $radicadoId);

                    if ($request > 0) {
                        $arrResponse = array("status" => true, "msg" => "Respuesta anulada con exito");
                    }
                    else {
                        $arrResponse = array("status" => false, "msg" => "Error en anular");
                    }
                }
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
        }
    }

    if($_POST){
        $obj = new respuestaController();

        switch ($_POST["action"]) {
            case 'getDataCreate':
                $obj->get_data_create();
                break;

            case 'saveCreate':
                $obj->save_create();
                break;
                
            case 'getDataVisualize':
                $obj->get_data_visualize();
                break;

            case 'getDataSearch':
                $obj->get_data_search();
                break;

            case 'anulaDocument':
                $obj->anula_document();
                break;

            default:
                $arrResponse = ["status" => false, "msg" => "Acción no encontrada"];
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            break;
        }
    }
?>