<?php
    require_once '../../../Librerias/core/Helpers.php';
    require_once '../models/informePqr_model.php';
    header('Content-Type: application/json');

    session_start();

    class informeController extends informeModel {
        public function getData() {
            if (!empty($_SESSION)) {
        
                $arrResponse = array("status" => true, "servicios" => $this->servicios());
        
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
        }

        public function searchDataPqr() {
            if (!empty($_SESSION)) {
                if ($_POST) {
                    $servicio = $_POST["servicio"];
                    $fechaIni = $_POST["fechaIni"];
                    $fechaFin = $_POST["fechaFin"];

                    $arrResponse = array("status" => true, "data" => $this->searchDataInforme($servicio, $fechaIni, $fechaFin));
                }

                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
        }
    }

    if($_POST){
        $obj = new informeController();

        switch ($_POST["action"]) {
            case "get_data":
                $obj->getData();
                break;

            case "search_data":
                $obj->searchDataPqr();
                break;
            default:
                $arrResponse = ["status" => false, "msg" => "Acción no encontrada"];
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            break;
        }
    }
?>