<?php
    require_once '../../../Librerias/core/Helpers.php';
    require_once '../models/tipoCausal_model.php';
    session_start();

    class causalController extends causalModel {

        public function save_create() {
            if (!empty($_SESSION)) {
                $data = json_decode($_POST['data'],true);

                if (!empty($data)) {
                    $codigo = strval($data["codigo"]);
                    $nombre = strClean(replaceChar($data["nombre"]));
    
                    if ($this->validaCodigo($codigo) == 0) {
                        $request = $this->insertaDatos($codigo, $nombre);

                        if ($request > 0) {
                            $arrResponse = array(
                                "status" => true,
                                "msg" => "Guardado de tipo de causal con exito",
                                "id" => $request
                            );
                        }
                        else {
                            $arrResponse = array(
                                "status" => false,
                                "msg" => "Error en guardado, vuelva a intentarlo"
                            );
                        }
                    }
                    else {
                        $arrResponse = array(
                            "status" => false,
                            "msg" => "Código ya esta en uso"
                        );
                    }
                }
                else {
                    $arrResponse = array(
                        "status" => false,
                        "msg" => "Datos no encontrados, vuelve a intentar"
                    );
                }
                
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }

            die();
        }

        public function get_data_search() {
            if (!empty($_SESSION)) {
                $arrResponse = array(
                    "status" => true,
                    "data" => $this->buscaDatosGuardados()
                );

                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
        }

        public function get_data_edit() {
            if (!empty($_SESSION)) {
                $id = intval($_POST["id"]);

                $arrResponse = array(
                    "status" => true,
                    "data" => $this->buscaInformacionEditar($id)
                );

                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
        }

        public function save_edit() {
            if (!empty($_SESSION)) {
                $id = intval($_POST["id"]);
                $data = json_decode($_POST['data'],true);

                $codigo = strval($data["codigo"]);
                $nombre = strClean(replaceChar($data["nombre"]));

                $request = $this->actualizaDatos($id, $codigo, $nombre);

                if ($request > 0) {
                    $arrResponse = array(
                        "status" => true,
                        "msg" => "Guardado de tipo de causal con exito"
                    );
                }
                else {
                    $arrResponse = array(
                        "status" => false,
                        "msg" => "Error en guardado, vuelva a intentarlo"
                    );
                }

                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
        }
    }

    if($_POST){
        $obj = new causalController();

        switch ($_POST["action"]) {
            case 'saveCreate':
                $obj->save_create();
                break;

            case 'getDataSearch':
                $obj->get_data_search();
                break;

            case 'getDataEdit':
                $obj->get_data_edit();
                break;

            case 'saveEdit':
                $obj->save_edit();
                break;
            default:
                $arrResponse = ["status" => false, "msg" => "Acción no encontrada"];
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
                break;
        }
    }
?>