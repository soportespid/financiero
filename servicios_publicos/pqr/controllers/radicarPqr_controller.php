<?php
    require_once '../../../Librerias/core/Helpers.php';
    require_once '../models/radicarPqr_model.php';
        header('Content-Type: application/json');

    session_start();

    class radicarController extends radicarModel {
        public function get_data_create() {
            if (!empty($_SESSION)) {
                $arrResponse = array(
                    "status" => true,
                    "usuarios" => $this->getUsersSp(),
                    "tramites" => $this->getTipoTramites(),
                    "causales" => $this->getTipoCausal()
                );

                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
        }

        public function get_detalle_causal() {
            if (!empty($_SESSION)) {
                if ($_POST) {
                    $causalId = intval($_POST["causalId"]);
                    $arrResponse = array(
                        "status" => true,
                        "detalleCausal" => $this->detalleCausal($causalId)
                    );
                    echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
                }
            }
            die();
        }

        public function save_create() {
            if (!empty($_SESSION)) {
                if ($_POST) {
                    $consecutivoPqr = searchConsec("srv_radicar_pqr", "consecutivo");
                    $data = json_decode($_POST['data'],true);
                    $imagenes = $_FILES["images"];
                    $resp = $this->insertData($consecutivoPqr, $data);

                    if ($resp > 0) {

                        if (count($imagenes) > 0) {
                            foreach ($imagenes["tmp_name"] as $key => $img) {
                                $type = explode("/", $imagenes["type"][$key]);
                                $resp2 = $this->insertImages($img, $type[1], $consecutivoPqr);
                            }
    
                            if ($resp2 > 0) {
                                $arrResponse = array("status" => true, "msg" => "Radicado pqr guardado con exito", "id" => $resp);
                            }
                            else {
                                $arrResponse = array("status" => false, "msg" => "Error en guardado de imagenes");
                            }
                        }
                        else {
                            $arrResponse = array("status" => true, "msg" => "Guardado con exito", "id" => $resp);
                        }
                    }
                    else {
                        $arrResponse = array("status" => false, "msg" => "Error en guardado de datos"
                        );
                    }
                }

                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
        }

        public function get_data_visualize(){
            if (!empty($_SESSION)) {
                if ($_POST) {
                    $id = intval($_POST["id"]);
                    $arrResponse = array(
                        "status" => true,
                        "data" =>  $this->searchData($id),
                        "consecutivos"=>$this->selectConsecutivos(),
                        "dataImages" => $this->searchImages($id)
                    );
                }
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
        }

        public function get_data_search() {
            if (!empty($_SESSION)) {
                if ($_POST) {
                    $arrResponse = array(
                        "status" => true,
                        "data" =>  $this->searchRadicados(),
                    );
                }
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
        }

        public function anula_document() {
            if (!empty($_SESSION)) {
                $id = (int) $_POST["radicarId"];
                $request = $this->anularDocumento($id);
                if ($request > 0) {
                    $arrResponse = array(
                        "status" => true
                    );
                }
                else {
                    $arrResponse = array(
                        "status" => true,
                        "msg" => "Error en anular, vuelva a intentarlo"
                    );
                }

                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
        }
    }

    if($_POST){
        $obj = new radicarController();

        switch ($_POST["action"]) {
            case 'getDataCreate':
                $obj->get_data_create();
                break;

            case 'getCausalDetalle':
                $obj->get_detalle_causal();
                break;

            case 'saveCreate':
                $obj->save_create();
                break;
                
            case 'getDataVisualize':
                $obj->get_data_visualize();
                break;

            case 'getDataSearch':
                $obj->get_data_search();
                break;

            case 'anulaDocument':
                $obj->anula_document();
                break;

            default:
                $arrResponse = ["status" => false, "msg" => "Acción no encontrada"];
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            break;
        }
    }
?>