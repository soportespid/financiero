<?php
    require_once '../../../Librerias/core/Helpers.php';
    require_once '../models/detalleCausal_model.php';
    session_start();

    class causalController extends causalModel {

        public function get_data_create() {
            if (!empty($_SESSION)) {
                $arrResponse = array(
                    "tiposCausales" => $this->buscaTipoCausales()
                );

                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
        }

        public function save_create() {
            if (!empty($_SESSION)) {
                $data = json_decode($_POST['data'],true);

                if (!empty($data)) {
                    $tipoCausalId = intval($data["tipoCausal"]);
                    $codigo = intval($data["codigo"]);
                    $nombre = strClean(replaceChar($data["nombre"]));
                    $descripcion = strClean(replaceChar($data["descripcion"]));
                    $ac = $data["acueducto"];
                    $alc = $data["alcantarillado"];
                    $aseo = $data["aseo"];
    
                    if ($this->validaCodigo($codigo) == 0) {
                        $request = $this->insertaDatos($tipoCausalId, $codigo, $nombre, $descripcion, $ac, $alc, $aseo);

                        if ($request > 0) {
                            $arrResponse = array(
                                "status" => true,
                                "msg" => "Guardado de detalle de causal con exito",
                                "id" => $request
                            );
                        }
                        else {
                            $arrResponse = array(
                                "status" => false,
                                "msg" => "Error en guardado, vuelva a intentarlo"
                            );
                        }
                    }
                    else {
                        $arrResponse = array(
                            "status" => false,
                            "msg" => "Código ya esta en uso"
                        );
                    }
                }
                else {
                    $arrResponse = array(
                        "status" => false,
                        "msg" => "Datos no encontrados, vuelve a intentar"
                    );
                }
                
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }

            die();
        }

        public function get_data_search() {
            if (!empty($_SESSION)) {
                $arrResponse = array(
                    "status" => true,
                    "data" => $this->buscaDatosGuardados()
                );

                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
        }

        public function get_data_edit() {
            if (!empty($_SESSION)) {
                $id = intval($_POST["id"]);

                $arrResponse = array(
                    "status" => true,
                    "tiposCausales" => $this->buscaTipoCausales(),
                    "data" => $this->buscaInformacionEditar($id)
                );

                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
        }

        public function save_edit() {
            if (!empty($_SESSION)) {
                $id = intval($_POST["id"]);
                $data = json_decode($_POST['data'],true);

                $tipoCausalId = intval($data["tipoCausal"]);
                $codigo = intval($data["codigo"]);
                $nombre = strClean(replaceChar($data["nombre"]));
                $descripcion = strClean(replaceChar($data["descripcion"]));
                $ac = $data["acueducto"];
                $alc = $data["alcantarillado"];
                $aseo = $data["aseo"];

                $request = $this->actualizaDatos($id, $tipoCausalId, $codigo, $nombre, $descripcion, $ac, $alc, $aseo);

                if ($request > 0) {
                    $arrResponse = array(
                        "status" => true,
                        "msg" => "Guardado de detalle de causal con exito"
                    );
                }
                else {
                    $arrResponse = array(
                        "status" => false,
                        "msg" => "Error en guardado, vuelva a intentarlo"
                    );
                }

                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
        }
    }

    if($_POST){
        $obj = new causalController();

        switch ($_POST["action"]) {
            case 'saveCreate':
                $obj->save_create();
                break;

            case 'getDataSearch':
                $obj->get_data_search();
                break;

            case 'getDataCreate':
                $obj->get_data_create();
                break;

            case 'getDataEdit':
                $obj->get_data_edit();
                break;

            case 'saveEdit':
                $obj->save_edit();
                break;
            default:
                $arrResponse = ["status" => false, "msg" => "Acción no encontrada"];
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
                break;
        }
    }
?>