const URL = 'servicios_publicos/historicoConsumos/serv-historicoConsumos.php';
import { filtroEnArrayDeObjetos } from './../../funciones.js';

var app = new Vue({
    el: '#myapp',
	data:{

        error: '',
        loading: false,
        codUsuario: '',
        nameUsuario: '',
        clienteId: '',
        datos: [],
    },

    mounted: function(){

        
    },

    methods: 
    {
        //Desglosar array
        toFormData: function(obj){
            var form_data = new FormData();
            for(var key in obj){
                form_data.append(key, obj[key]);
            }
            return form_data;
        },

        formatonumero: function(valor){
            return new Intl.NumberFormat("en-US", {style: "currency", currency: "USD"}).format(valor);
        },

        datosIniciales: async function() {
            
           
        },

        buscarUsuario: async function() {
            await axios.post(URL+'?action=buscaCod&cod='+this.codUsuario)
            .then((response) => {
                console.log(response.data);
                if (response.data.error01 == true) {
                    Swal.fire({
                        icon: 'error',
                        title: 'Error',
                        text: 'Codigo de usuario no encontrado'
                    })
                }
                else {
                    this.nameUsuario = response.data.name;
                    this.clienteId = response.data.id;
                }
            });
        },

        buscarDatos: async function() {

            if (this.clienteId != "") {
                await axios.post(URL+'?action=buscaDatos&id='+this.clienteId)
                .then((response) => {
                    console.log(response.data);
                    this.datos = response.data.datos;
                });
            }
            else {
                Swal.fire({
                    icon: 'error',
                    title: 'Error',
                    text: 'Primero busca el codigo de usuario'
                })
            }
        },

        excel: function() {

            if (this.datos.length > 0) {

                document.form2.action="serv-excelHistoricoConsumos.php";
				document.form2.target="_BLANK";
				document.form2.submit(); 
            }
            else {
                Swal.fire("Faltan datos", "Genera el informe primero", "warning");
            }
        },
    },
});