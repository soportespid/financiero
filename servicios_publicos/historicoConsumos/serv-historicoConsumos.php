<?php

	require_once '../../comun.inc';
    require '../../funciones.inc';
    require '../../funcionesSP.inc.php';
    header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
    session_start();

	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");

    $out = array('error' => false);

    $action = "show";

    if(isset($_GET['action'])){
        $action = $_GET['action'];
    }

    if ($action == "buscaCod") {

        $cod = $_GET['cod'];

        $query = "SELECT id, id_tercero FROM srvclientes WHERE cod_usuario = '$cod'";
        $res = mysqli_query($linkbd, $query);
        $num = mysqli_num_rows($res);

        if ($num > 0) {
            $out['error01'] = false;
            $row = mysqli_fetch_row($res);

            $name = buscaNombreTerceroConId($row[1]);
            $out['id'] = $row[0];
            $out['name'] = $name;
        }
        else {
            $out['error01'] = true;
        }
    }

    if ($action == "buscaDatos") {

        $sql = "SET lc_time_names = 'es_ES'";
        mysqli_query($linkbd,$sql);
        $clienteId = $_GET['id'];
        $datos = [];

        $sqlLecturas = "SELECT corte, id_servicio, lectura_medidor, consumo FROM srvlectura WHERE corte >= 1 AND id_cliente = $clienteId ORDER BY corte ASC, id_servicio ASC";
        $resLecturas = mysqli_query($linkbd, $sqlLecturas);
        while ($rowLecturas = mysqli_fetch_row($resLecturas)) {
            $datosTemp = [];

            //Periodos
            $sqlCorte = "SELECT UPPER(MONTHNAME(fecha_inicial)), UPPER(MONTHNAME(fecha_final)), YEAR(fecha_inicial), YEAR(fecha_final) FROM srvcortes WHERE numero_corte = $rowLecturas[0]";
            $rowCorte = mysqli_fetch_row(mysqli_query($linkbd, $sqlCorte));

            $codigoPeriodo = $rowLecturas[0];
            $periodo = $rowCorte[0] . " " . $rowCorte[2] . " - " . $rowCorte[1] . " " . $rowCorte[3];
            
            //servicios
            $sqlServicio = "SELECT nombre FROM srvservicios WHERE id = $rowLecturas[1]";
            $rowServicio = mysqli_fetch_row(mysqli_query($linkbd, $sqlServicio));

            $servicio = $rowServicio[0];

            //Lectura y consumo
            $lectura = $rowLecturas[2];
            $consumo = $rowLecturas[3];

            //Novedad
            $sqlNovedad = "SELECT codigo_observacion FROM srv_asigna_novedades WHERE corte = $rowLecturas[0] AND id_cliente = $clienteId AND estado = 'S'";
            $rowNovedad = mysqli_fetch_row(mysqli_query($linkbd, $sqlNovedad));

            $sqlBuscaNovedad = "SELECT descripcion FROM srv_tipo_observacion WHERE codigo_observacion = '$rowNovedad[0]'";
            $rowBuscaNovedad = mysqli_fetch_row(mysqli_query($linkbd, $sqlBuscaNovedad));

            $sqlNovedadDet = "SELECT consumo, lectura_consumo FROM srv_observacion_det WHERE codigo_observacion = '$rowNovedad[0]' AND id_servicio = $rowLecturas[1]";
            $rowNovedadDet = mysqli_fetch_row(mysqli_query($linkbd, $sqlNovedadDet));

            if ($rowNovedadDet[0] == "N") {
                $consumo = $rowNovedadDet[1];
            }

            $novedad = $rowBuscaNovedad[0];

            $datosTemp[] = $codigoPeriodo;
            $datosTemp[] = $periodo;
            $datosTemp[] = $servicio;
            $datosTemp[] = $lectura;
            $datosTemp[] = $consumo;
            $datosTemp[] = $novedad;
            array_push($datos, $datosTemp);
        }

        $out['datos'] = $datos;
    }

    header("Content-type: application/json");
    echo json_encode($out);
    die();