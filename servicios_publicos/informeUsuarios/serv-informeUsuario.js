const URL = 'servicios_publicos/informeUsuarios/serv-informeUsuarios.php';

var app = new Vue({
    el: '#myapp',
	data:{

        error: '',
        loading: false,
        
        usuarios: [],
    },

    mounted: function(){

        this.datosIniciales();
    },

    methods: 
    {
        //Desglosar array
        toFormData: function(obj){
            var form_data = new FormData();
            for(var key in obj){
                form_data.append(key, obj[key]);
            }
            return form_data;
        },

        formatonumero: function(valor){
            return new Intl.NumberFormat("en-US", {style: "currency", currency: "USD"}).format(valor);
        },

        traeParametros: function(name){
            //Captura parametros
            name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
            var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
            results = regex.exec(location.search);
            return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
        },

        datosIniciales: async function() {
            
            this.loading = true;

            await axios.post(URL+'?action=datosIniciales')
            .then((response) => {
                
                console.log(response.data);
                this.usuarios = response.data.usuarios;
                this.loading = false;
            });
        },

        bajarExcel: function() {
            const workbook = XLSX.utils.book_new();
            const worksheet = XLSX.utils.json_to_sheet(this.usuarios);
            XLSX.utils.book_append_sheet(workbook, worksheet, "Informe de usuarios");
            XLSX.writeFile(workbook, "InformeDeUsuarios.xlsx");
        },
    },
});