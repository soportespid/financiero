e<?php

use Illuminate\Support\Arr;

	require_once '../../comun.inc';
    require '../../funciones.inc';
    session_start();

	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");

    $out = array('error' => false);

    $action = "show";

    if(isset($_GET['action'])){
        $action = $_GET['action'];
    }

    if ($action == "datosIniciales") {

        $usuarios = array(

        );

        $sqlClientes = "SELECT id, cod_usuario, id_tercero, cod_catastral, id_barrio, id_estrato FROM srvclientes WHERE estado = 'S' ORDER BY id ASC";
        $resClientes = mysqli_query($linkbd, $sqlClientes);
        while ($rowClientes = mysqli_fetch_assoc($resClientes)) {
            
            $datosTemp = [];
            $nombreSuscriptor = "";

            //Datos tercero
            $sqlTercero = "SELECT persona, nombre1, nombre2, apellido1, apellido2, razonsocial, cedulanit FROM terceros WHERE id_tercero = '$rowClientes[id_tercero]'";
            $rowTercero = mysqli_fetch_assoc(mysqli_query($linkbd, $sqlTercero));

            if ($rowTercero['persona'] == 1) {

                $nombreSuscriptor = $rowTercero['razonsocial'];
            }
            else if ($rowTercero['persona'] == 2) {
                
                $nombreSuscriptor = $rowTercero['nombre1'] . " " . $rowTercero['nombre2'] . " " . $rowTercero['apellido1'] . " " . $rowTercero['apellido2'];
            }

            //Dirección
            $sqlDireccion = "SELECT direccion FROM srvdireccion_cliente WHERE id_cliente = $rowClientes[id]";
            $rowDireccion = mysqli_fetch_assoc(mysqli_query($linkbd, $sqlDireccion));

            //Barrio
            $sqlBarrio = "SELECT nombre FROM srvbarrios WHERE id = $rowClientes[id_barrio]";
            $rowBarrio = mysqli_fetch_assoc(mysqli_query($linkbd, $sqlBarrio));

            //Estrato
            $sqlEstrato = "SELECT descripcion FROM srvestratos WHERE id = $rowClientes[id_estrato]";
            $rowEstrato = mysqli_fetch_assoc(mysqli_query($linkbd, $sqlEstrato));

            // array_push($datosTemp, $rowClientes['cod_usuario']);
            // array_push($datosTemp, $rowTercero['cedulanit']);
            // array_push($datosTemp, $rowClientes['cod_catastral']);
            // array_push($datosTemp, $nombreSuscriptor);
            // array_push($datosTemp, $rowDireccion['direccion']);
            // array_push($datosTemp, $rowBarrio['nombre']);
            // array_push($datosTemp, $rowEstrato['descripcion']);

            $datosTemp['Codigo'] = $rowClientes['cod_usuario'];
            $datosTemp['Documento'] = $rowTercero['cedulanit'];
            $datosTemp['Catastral'] = $rowClientes['cod_catastral'];
            $datosTemp['Nombre'] = $nombreSuscriptor;
            $datosTemp['Direccion'] = $rowDireccion['direccion'];
            $datosTemp['Barrio'] = $rowBarrio['nombre'];
            $datosTemp['Estrato'] = $rowEstrato['descripcion'];
    
            array_push($usuarios, $datosTemp);
        }

        $out['usuarios'] = $usuarios;
    }

    header("Content-type: application/json");
    echo json_encode($out);
    die();