<?php

    require_once '../../../comun.inc';
    require '../../../funciones.inc';
    require '../../../funcionesSP.inc.php';
    session_start();

    $linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");

    $out = array('error' => false);

    $action = "show";

    if(isset($_GET['action'])){
        $action = $_GET['action'];
    }
    
    if ($action == 'datosIniciales') {

        //consecutivo
        $clienteId = '';
        $clienteId = selconsecutivo('srvclientes', 'id');

        $out['clienteId'] = $clienteId;

        //datos suscriptores
        $suscriptores = array();

        $sqlSubs = "SELECT id_tercero, cedulanit, persona, nombre1, nombre2, apellido1, apellido2, razonsocial FROM terceros WHERE estado = 'S' ORDER BY id_tercero ASC";
        $resSubs = mysqli_query($linkbd, $sqlSubs);
        if ($resSubs) {
            while ($rowSubs = mysqli_fetch_assoc($resSubs)) {
                
                $datos = array();

                //Persona natural
                if ($rowSubs['persona'] == 2) {

                    $nombreSuscriptor = '';
                    $nombreSuscriptor = $rowSubs['nombre1'] . " " . $rowSubs['nombre2'] . " " . $rowSubs['apellido1'] . " " . $rowSubs['apellido2'];
                    $nombreSuscriptor = Quitar_Espacios($nombreSuscriptor);

                    array_push($datos, $rowSubs['id_tercero']);
                    array_push($datos, $rowSubs['cedulanit']);
                    array_push($datos, $nombreSuscriptor);
                    array_push($suscriptores, $datos);
                }

                //Persona juridica
                if ($rowSubs['persona'] == 1) {
                    array_push($datos, $rowSubs['id_tercero']);
                    array_push($datos, $rowSubs['cedulanit']);
                    array_push($datos, $rowSubs['razonsocial']);
                    array_push($suscriptores, $datos);
                }
            }
        }

        $out['suscriptores'] = $suscriptores;


        //Datos barrios
        $barrios = array();

        $sqlBarrios = "SELECT id, nombre FROM srvbarrios WHERE estado = 'S' ORDER BY id";
        $resBarrios = mysqli_query($linkbd, $sqlBarrios);
        while ($rowBarrios = mysqli_fetch_row($resBarrios)) {
                
            array_push($barrios, $rowBarrios);
        }

        $out['barrios'] = $barrios;

        //Datos Estratos

        $estratos = array();

        $sqlEstratos = "SELECT id, descripcion FROM srvestratos WHERE estado = 'S' ORDER BY id";
        $resEstratos = mysqli_query($linkbd, $sqlEstratos);
        while ($rowEstratos = mysqli_fetch_row($resEstratos)) {
            
            array_push($estratos, $rowEstratos);
        }

        $out['estratos'] = $estratos;

        //Datos rutas

        $rutas = array();

        $sqlRutas = "SELECT id, nombre FROM srvrutas WHERE estado = 'S' ORDER BY id";
        $resRutas = mysqli_query($linkbd, $sqlRutas);
        while ($rowRutas = mysqli_fetch_row($resRutas)) {
            
            array_push($rutas, $rowRutas);
        }

        $out['rutas'] = $rutas;
    }

    if ($action == 'filtraTercero') {
     
        $keywordTercero = $_POST['keywordTercero'];
        $crit1 = "";
        $nombredividido = array();
        $nombredividido = explode(" ", $keywordTercero);
        
        for ($i=0; $i < count($nombredividido); $i++) 
        { 
            $busqueda = '';
            $busqueda = " AND concat_ws(' ', nombre1, nombre2, apellido1, apellido2, razonsocial, cedulanit) LIKE '%$nombredividido[$i]%' ";

            $crit1 = $crit1 . $busqueda;
        }

        $suscriptores = array();

        $sqlSubs = "SELECT id_tercero, cedulanit, persona, nombre1, nombre2, apellido1, apellido2, razonsocial FROM terceros WHERE estado = 'S' $crit1 ORDER BY id_tercero ASC";
        $resSubs = mysqli_query($linkbd, $sqlSubs);
        if ($resSubs) {
            while ($rowSubs = mysqli_fetch_assoc($resSubs)) {
                
                $datos = array();

                //Persona natural
                if ($rowSubs['persona'] == 2) {

                    $nombreSuscriptor = '';
                    $nombreSuscriptor = $rowSubs['nombre1'] . " " . $rowSubs['nombre2'] . " " . $rowSubs['apellido1'] . " " . $rowSubs['apellido2'];
                    $nombreSuscriptor = Quitar_Espacios($nombreSuscriptor);

                    array_push($datos, $rowSubs['id_tercero']);
                    array_push($datos, $rowSubs['cedulanit']);
                    array_push($datos, $nombreSuscriptor);
                    array_push($suscriptores, $datos);
                }

                //Persona juridica
                if ($rowSubs['persona'] == 1) {
                    array_push($datos, $rowSubs['id_tercero']);
                    array_push($datos, $rowSubs['cedulanit']);
                    array_push($datos, $rowSubs['razonsocial']);
                    array_push($suscriptores, $datos);
                }
            }
        }

        $out['suscriptores'] = $suscriptores;
    }

    if ($action == 'filtraBarrio') {
        //Datos barrios

        $keywordBarrio = $_POST['keywordBarrio'];
        $barrios = array();

        $sqlBarrios = "SELECT id, nombre FROM srvbarrios WHERE estado = 'S' AND concat_ws('', id, nombre) LIKE '%$keywordBarrio%' ORDER BY id";
        $resBarrios = mysqli_query($linkbd, $sqlBarrios);
        while ($rowBarrios = mysqli_fetch_row($resBarrios)) {
                
            array_push($barrios, $rowBarrios);
        }

        $out['barrios'] = $barrios;
    }

    if ($action == 'filtraEstrato') {
        //Datos Estratos

        $keywordEstrato = $_POST['keywordEstrato'];
        $estratos = array();

        $sqlEstratos = "SELECT id, descripcion FROM srvestratos WHERE estado = 'S' AND concat_ws('', id, descripcion) LIKE '%$keywordEstrato%' ORDER BY id";
        $resEstratos = mysqli_query($linkbd, $sqlEstratos);
        while ($rowEstratos = mysqli_fetch_row($resEstratos)) {
            
            array_push($estratos, $rowEstratos);
        }

        $out['estratos'] = $estratos;
    }

    if ($action == 'filtraRuta') {

        //Datos rutas
        $keywordRuta = $_POST['keywordRuta'];
        $rutas = array();

        $sqlRutas = "SELECT id, nombre FROM srvrutas WHERE estado = 'S' AND concat_ws('', id, nombre) LIKE '%$keywordRuta%' ORDER BY id";
        $resRutas = mysqli_query($linkbd, $sqlRutas);
        while ($rowRutas = mysqli_fetch_row($resRutas)) {
            
        array_push($rutas, $rowRutas);
        }

        $out['rutas'] = $rutas;
    }

    if ($action == 'validaCodUsuario') {

        $codUsuario = '';
        $codUsuario = $_GET['cod'];


        $sqlCliente = "SELECT * FROM srvclientes WHERE cod_usuario = '$codUsuario'";
        $resCliente = mysqli_query($linkbd, $sqlCliente);
        $valida = mysqli_num_rows($resCliente);

        if ($valida == 0) {
            $out['validaCod'] = false;
        }
        else {
            $out['validaCod'] = true;
        }
    }

    if ($action == 'guardar') {

        $clienteId = $_POST['clienteId'];
        $codUsuario = $_POST['codUsuario'];
        $codCatastral = $_POST['codCatastral'];
        $terceroId = $_POST['terceroId'];
        preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST["fecha"], $fecha);
		$fechaf = $fecha[3]."-".$fecha[2]."-".$fecha[1];
        $barrioId = $_POST['barrioId'];
        $estratoId = $_POST['estratoId'];
        $rutaId = $_POST['rutaId'];
        $ordenRuta = $_POST['ordenRuta'];
        $direccion = $_POST['direccion'];

        $sqlUsuario = "INSERT INTO srvclientes (id, id_tercero, cod_usuario, cod_catastral, fecha_creacion, id_barrio, id_ruta, id_estrato, codigo_ruta, zona_uso, estado) VALUES ($clienteId, $terceroId, '$codUsuario', '$codCatastral', '$fechaf', $barrioId, $rutaId, $estratoId, '$ordenRuta', '1', 'S')";
        mysqli_query($linkbd, $sqlUsuario);

        $sqlDireccion = "INSERT INTO srvdireccion_cliente (id_cliente, direccion) VALUES ('$clienteId', '$direccion')";
        mysqli_query($linkbd, $sqlDireccion);

        $usuario = $_SESSION["usuario"];
        $cedula = $_SESSION["cedulausu"];
        $ip = getRealIP();
        $hoy = date("Y-m-d H:i:s");

        $sqlAuditoria = "INSERT INTO srv_auditoria (id_srv_funciones, accion, cedula, usuario, ip, fecha_hora, consecutivo) VALUES (1, 'Crear', '$cedula', '$usuario', '$ip', '$hoy', $clienteId)";
        mysqli_query($linkbd, $sqlAuditoria);
        
        $out['insertaBien'] = true;
    }

    header("Content-type: application/json");
    echo json_encode($out);
    die();