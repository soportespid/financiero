var app = new Vue({
    el: '#myapp',
	data:{

        error: '',
        loading: false,
        
        clienteId: '',
        codUsuario: '',
        codCatastral: '',
        terceroId: '',
        cedula: '',
        nombre: '',
        direccion: '',
        suscriptores: [],
        showTerceros: false,
        searchTercero : {keywordTercero: ''},
        idBarrio: '',
        nombreBarrio: '',
        showBarrios: false,
        searchBarrio: {keywordBarrio: ''},
        barrios: [],
        idEstrato: '',
        estrato: '',
        showEstratos: false,
        searchEstrato: {keywordEstrato: ''},
        estratos: [],
        idRuta: '',
        ruta: '',
        ordenRuta: '',
        showRutas: false,
        searchRuta: {keywordRuta: ''},
        rutas: [],
    },

    mounted: function(){

        this.datosIniciales();
    },

    methods: 
    {
        toFormData: function(obj){
            var form_data = new FormData();
            for(var key in obj){
                form_data.append(key, obj[key]);
            }
            return form_data;
        },

        datosIniciales: async function() {
            
            await axios.post('servicios_publicos/usuarios/crear/serv-crearUsuario.php?action=datosIniciales')
            .then((response) => {
                //console.log(response.data);
                app.clienteId = response.data.clienteId;
                app.suscriptores = response.data.suscriptores;
                app.barrios = response.data.barrios;
                app.estratos = response.data.estratos;
                app.rutas = response.data.rutas;
            });
        },

        validaCodUsuario: async function() {

            await axios.post('servicios_publicos/usuarios/crear/serv-crearUsuario.php?action=validaCodUsuario&cod='+this.codUsuario)
            .then((response) => {
                
                if (response.data.validaCod == true) {
                    Swal.fire({
                        icon: 'error',
                        title: 'Datos duplicados',
                        text: 'Código de usuario ya existe.',
                    }).then((result) => { 
                        this.codUsuario = '';
                    });
                }
            }).catch((error) => {
                this.error = true;
                console.log(error)
            });
        },

        despliegaSuscriptores: function() {

            this.showTerceros = true;
        },

        searchMonitorTercero: async function() {

            var keywordTercero = app.toFormData(this.searchTercero);

            await axios.post('servicios_publicos/usuarios/crear/serv-crearUsuario.php?action=filtraTercero', keywordTercero)
            .then((response) => {
                console.log(response.data);
                app.suscriptores = response.data.suscriptores;
                
            });
        },

        seleccionaTercero: function(datosTercero) {

            this.terceroId = datosTercero[0];
            this.cedula = datosTercero[1];
            this.nombre = datosTercero[2];
            this.showTerceros = false;
        },

        despliegaBarrios: function() {

            this.showBarrios = true;
        },

        searchMonitorBarrio: async function() {

            var keywordBarrio = app.toFormData(this.searchBarrio);
            
            await axios.post('servicios_publicos/usuarios/crear/serv-crearUsuario.php?action=filtraBarrio', keywordBarrio)
            .then((response) => {
                app.barrios = response.data.barrios;
                
            });
        },

        seleccionaBarrio: function(datosBarrio) {

            this.idBarrio = datosBarrio[0];
            this.nombreBarrio = datosBarrio[1];
            this.showBarrios = false;
        },

        despliegaEstratos: function() {

            this.showEstratos = true;
        },

        searchMonitorEstrato: async function() {

            var keywordEstrato = app.toFormData(this.searchEstrato);
            
            await axios.post('servicios_publicos/usuarios/crear/serv-crearUsuario.php?action=filtraEstrato', keywordEstrato)
            .then((response) => {
                app.estratos = response.data.estratos;
                
            });
        },

        seleccionaEstrato: function(datosEstrato) {

            this.idEstrato = datosEstrato[0];
            this.estrato = datosEstrato[1];
            this.showEstratos = false;
        },

        despliegaRutas: function() {

            this.showRutas = true;
        },

        searchMonitorRuta: async function() {

            var keywordRuta = app.toFormData(this.searchRuta);
            
            await axios.post('servicios_publicos/usuarios/crear/serv-crearUsuario.php?action=filtraRuta', keywordRuta)
            .then((response) => {
                app.rutas = response.data.rutas;
                
            });
        },

        seleccionaRuta: function(datosRuta) {

            this.idRuta = datosRuta[0];
            this.ruta = datosRuta[1];
            this.showRutas = false;
        },


        guardar: function() {

            var fecha = document.getElementById('fecha').value;

            if (this.codUsuario != '' && this.codCatastral != '' && fecha != '' && this.cedula != '' && this.nombre != '' && this.idBarrio != '' && this.nombreBarrio != '' && this.idEstrato != '' && this.estrato != '' && this.idRuta != '' && this.ruta != '' && this.ordenRuta != '' && this.direccion != '') {

                Swal.fire({
                    icon: 'question',
                    title: 'Seguro que quieres guardar?',
                    showDenyButton: true,
                    confirmButtonText: 'Guardar',
                    denyButtonText: 'Cancelar',
                    }).then((result) => {
                    if (result.isConfirmed) 
                    {
                        if (result.isConfirmed) {
                            this.loading = true;
                            var formData = new FormData();

                            formData.append("clienteId", this.clienteId);
                            formData.append("codUsuario", this.codUsuario);
                            formData.append("codCatastral", this.codCatastral);
                            formData.append("fecha", fecha);
                            formData.append("terceroId", this.terceroId);
                            formData.append("barrioId", this.idBarrio);
                            formData.append("estratoId", this.idEstrato);
                            formData.append("rutaId", this.idRuta);
                            formData.append("ordenRuta", this.ordenRuta);
                            formData.append("direccion", this.direccion);
                            
                            axios.post('servicios_publicos/usuarios/crear/serv-crearUsuario.php?action=guardar', formData)
                            .then((response) => {
                                
                                if(response.data.insertaBien){
                                    this.loading = false;
                                    Swal.fire({
                                        icon: 'success',
                                        title: 'Se ha creado el usuario con exito',
                                        showConfirmButton: false,
                                        timer: 1500
                                        }).then((response) => {
                                            this.redireccionar();
                                        });
                                }
                                else {
                                    Swal.fire(
                                        'Error!',
                                        'No se pudo guardar.',
                                        'error'
                                    );
                                }
                            });
                            
                        }
                    } 
                    else if (result.isDenied) 
                    {
                        Swal.fire('Guardar cancelado', '', 'info');
                    }
                })   
            }
            else {
                Swal.fire('Falta información por llenar', '', 'info');
            }
        },

        redireccionar: function()
        {
            location.reload();  
        },
    }
});