<?php
    require_once '../../../comun.inc';
    require '../../../funciones.inc';
    require '../../../funcionesSP.inc.php';
    session_start();

    $linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");

    $out = array('error' => false);

    $action = "show";

    if(isset($_GET['action'])){
        $action = $_GET['action'];
    }
    
    if ($action == "codigoCreacion") {
        
        $codigo = "";
        $codigo = selconsecutivo('srvbarrios', 'id');

        $out['codigoBarrio'] = $codigo;
    }

    if ($action == "ubicacion") {

        $ubicaciones = array();

        $sqlUbicacion = "SELECT id_ubicacion, nombre_ubicacion FROM srv_ubicacion WHERE estado = 'S'";
        $resUbicacion = mysqli_query($linkbd, $sqlUbicacion);
        while ($rowUbicacion = mysqli_fetch_row($resUbicacion)) {
            
            array_push($ubicaciones, $rowUbicacion);
        }

        $out['ubicaciones'] = $ubicaciones;
    }

    if ($action == "departamentos") {

        $departamentos = array();

        $sqlDepartamentos = "SELECT DISTINCT codigo_departamento, nombre_departamento FROM srv_centros_poblados";
        $resDepartamentos = mysqli_query($linkbd, $sqlDepartamentos);
        while ($rowDepartamentos = mysqli_fetch_row($resDepartamentos)) {
            
            array_push($departamentos, $rowDepartamentos);
        }

        $out['departamentos'] = $departamentos;
    }

    if ($action == "filtraDepartamentos") {

        $keywordDepartamento = $_POST['keywordDepartamento'];
        $departamentos = array();

        $sqlDepartamentos = "SELECT DISTINCT codigo_departamento, nombre_departamento FROM srv_centros_poblados WHERE concat_ws(' ', codigo_departamento, nombre_departamento) LIKE '%$keywordDepartamento%'";
        $resDepartamentos = mysqli_query($linkbd, $sqlDepartamentos);
        while ($rowDepartamentos = mysqli_fetch_row($resDepartamentos)) {

            array_push($departamentos, $rowDepartamentos);
        }

        $out['departamentos'] = $departamentos;
    }

    if ($action == "municipios") {
        
        $codigoDepartamento = $_GET['departamento'];
        $municipios = array();
    
        $sqlMunicipios = "SELECT DISTINCT codigo_municipio, nombre_municipio FROM srv_centros_poblados WHERE codigo_municipio LIKE '$codigoDepartamento%'";
        $resMunicipios = mysqli_query($linkbd, $sqlMunicipios);
        while ($rowMunicipios = mysqli_fetch_row($resMunicipios)) {

            array_push($municipios, $rowMunicipios);
        }

        $out['municipios'] = $municipios;
    }

    if ($action == "filtraMunicipios") {

        $keywordMunicipio = $_POST['keywordMunicipio'];
        $codigoDepartamento = $_GET['departamento'];
        $municipios = array();
    
        $sqlMunicipios = "SELECT DISTINCT codigo_municipio, nombre_municipio FROM srv_centros_poblados WHERE codigo_municipio LIKE '$codigoDepartamento%' AND nombre_municipio LIKE '%$keywordMunicipio%'";
        $resMunicipios = mysqli_query($linkbd, $sqlMunicipios);
        while ($rowMunicipios = mysqli_fetch_row($resMunicipios)) {

            array_push($municipios, $rowMunicipios);
        }

        $out['municipios'] = $municipios;

    }

    if ($action == "centroPoblados") {

        $codigoMunicipio = $_GET['municipio'];
        $centrosPoblados = array();

        $sqlCentroPoblado = "SELECT DISTINCT codigo_centro_poblado, nombre_centro_poblado FROM srv_centros_poblados WHERE codigo_centro_poblado LIKE '$codigoMunicipio%'";
        $resCentroPoblado = mysqli_query($linkbd, $sqlCentroPoblado);
        while ($rowCentroPoblado = mysqli_fetch_row($resCentroPoblado)) {

            array_push($centrosPoblados, $rowCentroPoblado);
        }

        $out['centrosPoblados'] = $centrosPoblados;
    }

    if ($action == "filtraCentroPoblado") { 

        $keywordCentroPoblado = $_POST['keywordCentroPoblado'];
        $codigoMunicipio = $_GET['municipio'];
        $centrosPoblados = array();

        $sqlCentroPoblado = "SELECT DISTINCT codigo_centro_poblado, nombre_centro_poblado FROM srv_centros_poblados WHERE codigo_centro_poblado LIKE '$codigoMunicipio%' AND nombre_centro_poblado LIKE '%$keywordCentroPoblado%'";
        $resCentroPoblado = mysqli_query($linkbd, $sqlCentroPoblado);
        while ($rowCentroPoblado = mysqli_fetch_row($resCentroPoblado)) {

            array_push($centrosPoblados, $rowCentroPoblado);
        }

        $out['centrosPoblados'] = $centrosPoblados;
    }

    if ($action == "crearGuardar") {

        $nombreBarrio = "";
        $codigoCentroPoblado = "";
        $id_ubicacion = "";
        $nombreBarrio = $_POST['nombreBarrio'];
        $codigoCentroPoblado = $_POST['centroPoblado'];
        $id_ubicacion = $_POST['ubicacion'];

        $codigoBarrio = selconsecutivo('srvbarrios', 'id');

        $sqlBarrios = "INSERT INTO srvbarrios VALUES ($codigoBarrio, '$nombreBarrio', 'S', '$codigoCentroPoblado', $id_ubicacion)";
        
        if (mysqli_query($linkbd, $sqlBarrios)){

            $out['insertaBien'] = true;
        }
        else {
            $out['insertaBien'] = false;
        }
    }

    if ($action == "buscarBarrios") {

        $barrios = array();

        $sqlBarrios = "SELECT id, nombre, centro_poblado, id_ubicacion FROM srvbarrios WHERE estado = 'S' ORDER BY id ASC";
        $resBarrios = mysqli_query($linkbd, $sqlBarrios);
        while ($rowBarrios = mysqli_fetch_row($resBarrios)) {
            
            $departamento = "";
            $municipio = "";
            $datos = array();
            $departamento = substr($rowBarrios[2], 0, 2);
            $municipio = substr($rowBarrios[2], 0, 5);

            $sqlDepartamento = "SELECT DISTINCT nombre_departamento FROM srv_centros_poblados WHERE codigo_departamento = '$departamento'";
            $rowDepartamento = mysqli_fetch_row(mysqli_query($linkbd, $sqlDepartamento));
            
            $sqlMunicipio = "SELECT DISTINCT nombre_municipio FROM srv_centros_poblados WHERE codigo_municipio = '$municipio'";
            $rowMunicipio = mysqli_fetch_row(mysqli_query($linkbd, $sqlMunicipio));

            $sqlCentroPoblado = "SELECT DISTINCT nombre_centro_poblado FROM srv_centros_poblados WHERE codigo_centro_poblado = '$rowBarrios[2]'";
            $rowCentroPoblado = mysqli_fetch_row(mysqli_query($linkbd, $sqlCentroPoblado));

            $sqlUbicacion = "SELECT nombre_ubicacion FROM srv_ubicacion WHERE id_ubicacion = $rowBarrios[3]";
            $rowUbicacion = mysqli_fetch_row(mysqli_query($linkbd, $sqlUbicacion));

            array_push($datos, $rowBarrios[0]);
            array_push($datos, $rowBarrios[1]);
            array_push($datos, $rowDepartamento[0]);
            array_push($datos, $rowMunicipio[0]);
            array_push($datos, $rowCentroPoblado[0]);
            array_push($datos, $rowUbicacion[0]);
            array_push($barrios, $datos);
        }

        $out['barrios'] = $barrios;
    }

    if ($action == "filtrarBarrios") {

        $barrio = $_GET['barrio'];
        $barrios = array();

        $sqlBarrios = "SELECT id, nombre, centro_poblado FROM srvbarrios WHERE estado = 'S' AND CONCAT_WS(' ', ID, nombre) LIKE '%$barrio%' ORDER BY id ASC";
        $resBarrios = mysqli_query($linkbd, $sqlBarrios);
        while ($rowBarrios = mysqli_fetch_row($resBarrios)) {
            
            $departamento = "";
            $municipio = "";
            $datos = array();
            $departamento = substr($rowBarrios[2], 0, 2);
            $municipio = substr($rowBarrios[2], 0, 5);

            $sqlDepartamento = "SELECT DISTINCT nombre_departamento FROM srv_centros_poblados WHERE codigo_departamento = '$departamento'";
            $rowDepartamento = mysqli_fetch_row(mysqli_query($linkbd, $sqlDepartamento));
            
            $sqlMunicipio = "SELECT DISTINCT nombre_municipio FROM srv_centros_poblados WHERE codigo_municipio = '$municipio'";
            $rowMunicipio = mysqli_fetch_row(mysqli_query($linkbd, $sqlMunicipio));

            $sqlCentroPoblado = "SELECT DISTINCT nombre_centro_poblado FROM srv_centros_poblados WHERE codigo_centro_poblado = '$rowBarrios[2]'";
            $rowCentroPoblado = mysqli_fetch_row(mysqli_query($linkbd, $sqlCentroPoblado));

            array_push($datos, $rowBarrios[0]);
            array_push($datos, $rowBarrios[1]);
            array_push($datos, $rowDepartamento[0]);
            array_push($datos, $rowMunicipio[0]);
            array_push($datos, $rowCentroPoblado[0]);
            array_push($barrios, $datos);
        }

        $out['barrios'] = $barrios;
    }

    if ($action == "eliminarBarrio") {

        $codBarrio = $_GET['cod'];

        //Validacion
        $sqlClientes = "SELECT COUNT(id_barrio) FROM srvclientes WHERE id_barrio = $codBarrio";
        $rowClientes = mysqli_fetch_row(mysqli_query($linkbd, $sqlClientes));

        if ($rowClientes[0] == 0) {
            $out['validacion'] = true;

            $sqlBarrio = "UPDATE srvbarrios SET estado = 'N' WHERE id = $codBarrio";
            if (mysqli_query($linkbd, $sqlBarrio)) {

                $out['eliminacion'] = true;
            }
            else {
                $out['eliminacion'] = false;
            }
        }
        else {
            $out['validacion'] = false;
        }
    }

    header("Content-type: application/json");
    echo json_encode($out);
    die();