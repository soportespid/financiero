var app = new Vue({
    el: '#myapp',
	data:{

        error: '',
        loading: false,
        showDepartamentos: false,
        showMunicipios: false,
        showCentroPoblado: false,
        codigoBarrio: '',
        departamentos: [],
        nombreDepartamento: '',
        codigoDepartamento: '',
        searchDepartamento : {keywordDepartamento: ''},
        searchMunicipio : {keywordMunicipio: ''},
        searchCentroPoblado : {keywordCentroPoblado: ''},
        municipios: [],
        nombreMunicipio: '',
        codigoMunicipio: '',
        centrosPoblados: [],
        nombreCentroPoblado: '',
        codigoCentroPoblado: '',
        nombreBarrio: "",
        barrios: [],
        buscarBarrio: "",
        ubicaciones: [],
        ubicacion: "",
    },

    mounted: function(){

        this.codigoCreacionBarrio();
        this.llamaDepartamentos();
        this.buscarBarrios();
        this.llamaUbicaciones();
    },

    methods: 
    {
        toFormData: function(obj){
            var form_data = new FormData();
            for(var key in obj){
                form_data.append(key, obj[key]);
            }
            return form_data;
        },

        codigoCreacionBarrio: function() {

            axios.post('servicios_publicos/barrios/crear_buscar/serv-barrios.php?action=codigoCreacion')
            .then((response) => {
                
                app.codigoBarrio = response.data.codigoBarrio;
            }).catch((error) => {
                this.error = true;
                console.log(error)
            }).finally(() => {
        
            });     
        },

        llamaUbicaciones: function() {

            axios.post('servicios_publicos/barrios/crear_buscar/serv-barrios.php?action=ubicacion')
            .then((response) => {
                
                app.ubicaciones = response.data.ubicaciones;
            }).catch((error) => {
                this.error = true;
                console.log(error)
            }).finally(() => {
        
            });     
        },

        llamaDepartamentos: async function(){

            await axios.post('servicios_publicos/barrios/crear_buscar/serv-barrios.php?action=departamentos')
            .then((response) => {
                
                app.departamentos = response.data.departamentos;
            }).catch((error) => {
                this.error = true;
                console.log(error)
            }).finally(() => {
        
            });    
        },

        despliegaDepartamentos: function() {

            this.showDepartamentos = true;
        },

        seleccionaDepartamento: function(departamento) {
            this.nombreDepartamento = departamento[1];
            this.codigoDepartamento = departamento[0];
            this.codigoMunicipio = "";
            this.nombreMunicipio = "";
            this.showDepartamentos = false;
        },
        
        searchMonitorDepartamento: function() {

            var keywordDepartamento = app.toFormData(this.searchDepartamento);

            axios.post('servicios_publicos/barrios/crear_buscar/serv-barrios.php?action=filtraDepartamentos', keywordDepartamento)
            .then((response) => {
                
                app.departamentos = response.data.departamentos;
            }).catch((error) => {
                this.error = true;
                console.log(error)
            }).finally(() => {
        
            });  
        },

        llamaMunicipios: async function() {

            if (this.codigoDepartamento != '') {

                this.loading = true;

                await axios.post('servicios_publicos/barrios/crear_buscar/serv-barrios.php?action=municipios&departamento='+this.codigoDepartamento)
                .then((response) => {
                    
                    app.municipios = response.data.municipios;
                }).catch((error) => {
                    this.error = true;
                    console.log(error)
                }).finally(() => {
                    this.loading = false;
                    this.showMunicipios = true;
                });  
            }
            else {
                Swal.fire("Error", "Selecciona primero el departamento", "warning");
            }
        },

        seleccionaMunicipio: function(municipio) {

            this.nombreMunicipio = municipio[1];
            this.codigoMunicipio = municipio[0];
            this.nombreCentroPoblado = "";
            this.codigoCentroPoblado = "";
            this.showMunicipios = false;
        },

        searchMonitorMunicipio: function(){ 

            var keywordMunicipio = app.toFormData(this.searchMunicipio);

            axios.post('servicios_publicos/barrios/crear_buscar/serv-barrios.php?action=filtraMunicipios&departamento='+this.codigoDepartamento, keywordMunicipio)
            .then((response) => {
                
                app.municipios = response.data.municipios;
            }).catch((error) => {
                this.error = true;
                console.log(error)
            }).finally(() => {
        
            });  
        },

        llamaCentroPoblado: async function() {

            if (this.codigoMunicipio != "") {

                this.loading = true;

                await axios.post('servicios_publicos/barrios/crear_buscar/serv-barrios.php?action=centroPoblados&municipio='+this.codigoMunicipio)
                .then((response) => {
                    
                    app.centrosPoblados = response.data.centrosPoblados;
                }).catch((error) => {
                    this.error = true;
                    console.log(error)
                }).finally(() => {
                    this.loading = false;
                    this.showCentroPoblado = true;
                });  
            }
            else {
                Swal.fire("Error", "Selecciona primero el municipio", "warning");
            }
        },

        seleccionaCentroPoblado: function(centroPoblado) {

            this.codigoCentroPoblado = centroPoblado[0];
            this.nombreCentroPoblado = centroPoblado[1];
            this.showCentroPoblado = false;
        },

        searchMonitorCentroPoblado: function() {

            var keywordCentroPoblado = app.toFormData(this.searchCentroPoblado);

            axios.post('servicios_publicos/barrios/crear_buscar/serv-barrios.php?action=filtraCentroPoblado&municipio='+this.codigoMunicipio, keywordCentroPoblado)
            .then((response) => {
                
                app.centrosPoblados = response.data.centrosPoblados;
            }).catch((error) => {
                this.error = true;
                console.log(error)
            }).finally(() => {
        
            });  
        },

        crearGuardar: async function() {
             
            if (this.codigoBarrio != "" && this.nombreBarrio != "" && this.codigoDepartamento != "" && this.codigoMunicipio != "" && this.codigoCentroPoblado != "" && this.ubicacion != "") {

                Swal.fire({
                    icon: 'question',
                    title: 'Seguro que quieres guardar?',
                    showDenyButton: true,
                    confirmButtonText: 'Guardar',
                    denyButtonText: 'Cancelar',
                    }).then((result) => {
                        
                    if (result.isConfirmed) {

                        var formData = new FormData();
                        
                        formData.append("nombreBarrio", this.nombreBarrio);
                        formData.append("centroPoblado", this.codigoCentroPoblado);
                        formData.append("ubicacion", this.ubicacion);

                        this.loading = true;

                        axios.post('servicios_publicos/barrios/crear_buscar/serv-barrios.php?action=crearGuardar', formData)
                        .then((response) => {

                            if(response.data.insertaBien) {
                                
                                this.loading = false;
                                Swal.fire({
                                    icon: 'success',
                                    title: 'Se ha guardado con exito',
                                    showConfirmButton: false,
                                    timer: 1500
                                    }).then((response) => {
                                        app.redireccionarCrear();
                                    });
                            }
                            else {
                                Swal.fire(
                                    'Error!',
                                    'No se pudo guardar.',
                                    'error'
                                );
                            }
                        });
                    }
                    else if (result.isDenied) 
                    {
                        Swal.fire('Guardar cancelado', '', 'info');
                    }
                });
            }
            else {
                Swal.fire("Error", "Faltan datos por llenar", "warning")
            }
        },

        redireccionarCrear: function()
        {
            location.href = "serv-crearBarrio.php";
        },

        buscarBarrios: async function() {

            await axios.post('servicios_publicos/barrios/crear_buscar/serv-barrios.php?action=buscarBarrios')
            .then((response) => {
                //console.log(response.data);
                app.barrios = response.data.barrios;
            }).catch((error) => {
                this.error = true;
                console.log(error)
            }).finally(() => {
        
            }); 
        },

        filtrarBarrios: async function() {

            await axios.post('servicios_publicos/barrios/crear_buscar/serv-barrios.php?action=filtrarBarrios&barrio='+this.buscarBarrio)
            .then((response) => {
                app.barrios = response.data.barrios;
            }).catch((error) => {
                this.error = true;
                console.log(error)
            }).finally(() => {
        
            }); 
        },

        seleccionaBarrio: function(barrio) {

            location.href="serv-editarBarrio.php?cod="+barrio[0];
        },

        eliminarBarrio: function(barrio) {

            Swal.fire({
                icon: 'question',
                title: 'Esta seguro que quiere eliminar?',
                showDenyButton: true,
                confirmButtonText: 'Eliminar',
                denyButtonText: 'Cancelar',
                }).then((result) => {
                    
                if (result.isConfirmed) {

                    axios.post('servicios_publicos/barrios/crear_buscar/serv-barrios.php?action=eliminarBarrio&cod='+barrio[0])
                    .then((response) => {

                        if (response.data.validacion == true) {

                            if (response.data.eliminacion) {

                                Swal.fire({
                                    icon: 'success',
                                    title: 'Se ha eliminado con exito',
                                    showConfirmButton: false,
                                    timer: 1500
                                    }).then((response) => {
                                        location.reload();
                                });
                            }
                            else {
                                Swal.fire("Error", "Eliminar a fallado", "error")
                            }
                        }
                        else {
                            Swal.fire("Error", "Barrio actualmente en uso", "error")
                        }
                    });
                }
                else {
                    Swal.fire('Eliminar cancelado', '', 'info');
                }
            });
        },
    }
});