var app = new Vue({
    el: '#myapp',
	data:{

        error: '',
        loading: false,
        showDepartamentos: false,
        showMunicipios: false,
        showCentroPoblado: false,
        codigoBarrio: '',
        departamentos: [],
        nombreDepartamento: '',
        codigoDepartamento: '',
        searchDepartamento : {keywordDepartamento: ''},
        searchMunicipio : {keywordMunicipio: ''},
        searchCentroPoblado : {keywordCentroPoblado: ''},
        municipios: [],
        nombreMunicipio: '',
        codigoMunicipio: '',
        centrosPoblados: [],
        nombreCentroPoblado: '',
        codigoCentroPoblado: '',
        nombreBarrio: "",
        ubicaciones: [],
        ubicacion: "",
    },

    mounted: function(){

        this.valoresIniciales();
        this.llamaDepartamentos();
        this.llamaUbicaciones();
    },

    methods: 
    {
        toFormData: function(obj){
            var form_data = new FormData();
            for(var key in obj){
                form_data.append(key, obj[key]);
            }
            return form_data;
        },

        traeParametros: function(name){
            //Captura parametros
            name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
            var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
            results = regex.exec(location.search);
            return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
        },

        valoresIniciales: async function() {

            const codigoBarrio = this.traeParametros('cod');
            
            await axios.post('servicios_publicos/barrios/editar/serv-editarBarrio.php?action=traerDatos&codigo='+codigoBarrio)
            .then((response) => {
                
                app.codigoBarrio = codigoBarrio;
                app.nombreBarrio = response.data.nombre;
                app.codigoDepartamento = response.data.codDepartamento;
                app.nombreDepartamento = response.data.departamento;
                app.codigoMunicipio = response.data.codMunicipio;
                app.nombreMunicipio = response.data.municipio;
                app.codigoCentroPoblado = response.data.codCentroPoblado;
                app.nombreCentroPoblado = response.data.centroPoblado;
                app.ubicacion = response.data.ubicacion;
                
            }).catch((error) => {
                this.error = true;
                console.log(error)
            }).finally(() => {
        
            });     
        },

        llamaDepartamentos: async function(){

            await axios.post('servicios_publicos/barrios/crear_buscar/serv-barrios.php?action=departamentos')
            .then((response) => {
                
                app.departamentos = response.data.departamentos;
            }).catch((error) => {
                this.error = true;
                console.log(error)
            }).finally(() => {
        
            });    
        },

        llamaUbicaciones: function() {

            axios.post('servicios_publicos/barrios/crear_buscar/serv-barrios.php?action=ubicacion')
            .then((response) => {
                
                app.ubicaciones = response.data.ubicaciones;
            }).catch((error) => {
                this.error = true;
                console.log(error)
            }).finally(() => {
        
            });     
        },

        despliegaDepartamentos: function() {

            this.showDepartamentos = true;
        },

        seleccionaDepartamento: function(departamento) {
            this.nombreDepartamento = departamento[1];
            this.codigoDepartamento = departamento[0];
            this.codigoMunicipio = "";
            this.nombreMunicipio = "";
            this.showDepartamentos = false;
        },
        
        searchMonitorDepartamento: function() {

            var keywordDepartamento = app.toFormData(this.searchDepartamento);

            axios.post('servicios_publicos/barrios/crear_buscar/serv-barrios.php?action=filtraDepartamentos', keywordDepartamento)
            .then((response) => {
                
                app.departamentos = response.data.departamentos;
            }).catch((error) => {
                this.error = true;
                console.log(error)
            }).finally(() => {
        
            });  
        },

        llamaMunicipios: async function() {

            if (this.codigoDepartamento != '') {

                this.loading = true;

                await axios.post('servicios_publicos/barrios/crear_buscar/serv-barrios.php?action=municipios&departamento='+this.codigoDepartamento)
                .then((response) => {
                    
                    app.municipios = response.data.municipios;
                }).catch((error) => {
                    this.error = true;
                    console.log(error)
                }).finally(() => {
                    this.loading = false;
                    this.showMunicipios = true;
                });  
            }
            else {
                Swal.fire("Error", "Selecciona primero el departamento", "warning");
            }
        },

        seleccionaMunicipio: function(municipio) {

            this.nombreMunicipio = municipio[1];
            this.codigoMunicipio = municipio[0];
            this.nombreCentroPoblado = "";
            this.codigoCentroPoblado = "";
            this.showMunicipios = false;
        },

        searchMonitorMunicipio: function(){ 

            var keywordMunicipio = app.toFormData(this.searchMunicipio);

            axios.post('servicios_publicos/barrios/crear_buscar/serv-barrios.php?action=filtraMunicipios&departamento='+this.codigoDepartamento, keywordMunicipio)
            .then((response) => {
                
                app.municipios = response.data.municipios;
            }).catch((error) => {
                this.error = true;
                console.log(error)
            }).finally(() => {
        
            });  
        },

        llamaCentroPoblado: async function() {

            if (this.codigoMunicipio != "") {

                this.loading = true;

                await axios.post('servicios_publicos/barrios/crear_buscar/serv-barrios.php?action=centroPoblados&municipio='+this.codigoMunicipio)
                .then((response) => {
                    
                    app.centrosPoblados = response.data.centrosPoblados;
                }).catch((error) => {
                    this.error = true;
                    console.log(error)
                }).finally(() => {
                    this.loading = false;
                    this.showCentroPoblado = true;
                });  
            }
            else {
                Swal.fire("Error", "Selecciona primero el municipio", "warning");
            }
        },

        seleccionaCentroPoblado: function(centroPoblado) {

            this.codigoCentroPoblado = centroPoblado[0];
            this.nombreCentroPoblado = centroPoblado[1];
            this.showCentroPoblado = false;
        },

        searchMonitorCentroPoblado: function() {

            var keywordCentroPoblado = app.toFormData(this.searchCentroPoblado);

            axios.post('servicios_publicos/barrios/crear_buscar/serv-barrios.php?action=filtraCentroPoblado&municipio='+this.codigoMunicipio, keywordCentroPoblado)
            .then((response) => {
                
                app.centrosPoblados = response.data.centrosPoblados;
            }).catch((error) => {
                this.error = true;
                console.log(error)
            }).finally(() => {
        
            });  
        },
     
        editarGuardar: function() {

            if (this.codigoBarrio != "" && this.nombreBarrio != "" && this.codigoDepartamento != "" && this.codigoMunicipio != "" && this.codigoCentroPoblado != "" && this.ubicacion != "") {

                Swal.fire({
                    icon: 'question',
                    title: 'Seguro que quieres guardar?',
                    showDenyButton: true,
                    confirmButtonText: 'Guardar',
                    denyButtonText: 'Cancelar',
                    }).then((result) => {
                        
                    if (result.isConfirmed) {

                        
                        var formData = new FormData();

                        formData.append("codigo", this.codigoBarrio);
                        formData.append("nombreBarrio", this.nombreBarrio);
                        formData.append("centroPoblado", this.codigoCentroPoblado);
                        formData.append("ubicacion", this.ubicacion);

                        this.loading = true;

                        axios.post('servicios_publicos/barrios/editar/serv-editarBarrio.php?action=editaGuardar', formData)
                        .then((response) => {

                            if(response.data.insertaBien) {
                                
                                this.loading = false;
                                Swal.fire({
                                    icon: 'success',
                                    title: 'Se ha guardado con exito',
                                    showConfirmButton: false,
                                    timer: 1500
                                    }).then((response) => {
                                        app.redireccionar();
                                    });
                            }
                            else {
                                Swal.fire(
                                    'Error!',
                                    'No se pudo guardar.',
                                    'error'
                                );
                            }
                        });
                    }
                    else if (result.isDenied) 
                    {
                        Swal.fire('Guardar cancelado', '', 'info');
                    }
                });
            }
            else {
                Swal.fire("Error", "Faltan datos por llenar", "warning")
            }
        },

        redireccionar: function() {
            location.reload();
        },
    }
});