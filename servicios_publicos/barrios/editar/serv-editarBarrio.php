<?php
    require_once '../../../comun.inc';
    require '../../../funciones.inc';
    require '../../../funcionesSP.inc.php';
    session_start();

    $linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");

    $out = array('error' => false);

    $action = "show";

    if(isset($_GET['action'])){
        $action = $_GET['action'];
    }
    
    if ($action == "traerDatos") {

        $codigoBarrio = $_GET['codigo'];
        $nombre = "";
        $codDepartamento = "";
        $departamento = "";
        $codMunicipio = "";
        $municipio = "";
        $codCentroPoblado = "";
        $centroPoblado = "";

        $sqlBarrio = "SELECT nombre, centro_poblado, id_ubicacion FROM srvbarrios WHERE id = $codigoBarrio AND estado = 'S'";
        $rowBarrio = mysqli_fetch_row(mysqli_query($linkbd, $sqlBarrio));

        $codDepartamento = substr($rowBarrio[1], 0, 2);
        $codMunicipio = substr($rowBarrio[1], 0, 5);
        $codCentroPoblado = $rowBarrio[1];

        $sqlDepartamento = "SELECT DISTINCT nombre_departamento FROM srv_centros_poblados WHERE codigo_departamento = '$codDepartamento'";
        $rowDepartamento = mysqli_fetch_row(mysqli_query($linkbd, $sqlDepartamento));

        $departamento = $rowDepartamento[0];

        $sqlMunicipio = "SELECT DISTINCT nombre_municipio FROM srv_centros_poblados WHERE codigo_municipio = $codMunicipio";
        $rowMunicipio = mysqli_fetch_row(mysqli_query($linkbd, $sqlMunicipio));

        $municipio = $rowMunicipio[0];

        $sqlCentroPoblado = "SELECT nombre_centro_poblado FROM srv_centros_poblados WHERE codigo_centro_poblado = $codCentroPoblado";
        $rowCentroPoblado = mysqli_fetch_row(mysqli_query($linkbd, $sqlCentroPoblado));

        $centroPoblado = $rowCentroPoblado[0];

        $out['nombre'] = $rowBarrio[0];
        $out['codDepartamento'] = $codDepartamento;
        $out['departamento'] = $departamento;
        $out['codMunicipio'] = $codMunicipio;
        $out['municipio'] = $municipio;
        $out['codCentroPoblado'] = $codCentroPoblado;
        $out['centroPoblado'] = $centroPoblado;
        $out['ubicacion'] = $rowBarrio[2];
    }

    if ($action == "editaGuardar") {

        $codigoBarrio = "";
        $nombreBarrio = "";
        $codCentroPoblado = "";
        $id_ubicacion = "";

        $codigoBarrio = $_POST['codigo'];
        $nombreBarrio = $_POST['nombreBarrio'];
        $codCentroPoblado = $_POST['centroPoblado'];
        $id_ubicacion = $_POST['ubicacion'];

        $sqlBarrio = "UPDATE srvbarrios SET nombre = '$nombreBarrio', centro_poblado = '$codCentroPoblado', id_ubicacion = $id_ubicacion WHERE id = $codigoBarrio";
        if(mysqli_query($linkbd, $sqlBarrio)) {
            $out['insertaBien'] = true;
        }
        else{
            $out['insertaBien'] = false;
        }
    }

    header("Content-type: application/json");
    echo json_encode($out);
    die();