const URL = 'servicios_publicos/servicios/controllers/costoEstandar_controller.php';

var app = new Vue({
    el:"#myapp",
    data() {
        return {
            isLoading:false,
            codigoVersion: '',
            servicios: [],
            servicioId: 0,
            data: [],
            versiones: [],
            version: 0,
        }
    },
    mounted() {
        this.intPageVal = this.$refs.pageType.value;

        if (this.intPageVal == 1) {
            this.getDataCreate();
        } else if (this.intPageVal == 2) {
            this.getDataSearch();
        } else if (this.intPageVal == 3) {
            
        }
    },
    methods: {
        //trae datos iniciales
        async getDataCreate(){
            const formData = new FormData();
            formData.append("action","getDataCreate");
            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();
            this.servicios = objData.servicios;
        },

        async getDataSearch() {
            const formData = new FormData();
            formData.append("action","getDataSearch");
            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();
            this.servicios = objData.servicios;
            this.versiones = objData.versiones;
        },

        //valida codigo y servicio y carga las clases de uso
        dataClaseUso: async function() {

            if (this.codigoVersion != "" && this.servicioId != 0) {

                const formData = new FormData();
                formData.append("action","dataClaseUso");
                formData.append("codigo",this.codigoVersion);
                formData.append("servicioId",this.servicioId);
                const response = await fetch(URL,{method:"POST",body:formData});
                const objData = await response.json();

                if (objData.status) {
                    this.data = objData.data;
                }
                else {
                    Swal.fire("Error!",objData.msg,"warning");    
                }
            }
            else {
                Swal.fire("Atención!","Todos los campos con (*) son obligatorios","warning");
            }
        },

        //buscaTarifasGuardadas
        async searchTarifas() {
            if (this.version != "0" && this.servicioId != 0){
                const formData = new FormData();
                formData.append("action","searchTarifas");
                formData.append("codigo",this.version);
                formData.append("servicioId",this.servicioId);
                // this.isLoading = true;
                const response = await fetch(URL,{method:"POST",body:formData});
                const objData = await response.json();
                this.isLoading = false;

                if (objData.status) {
                    this.data = objData.data;
                }
                else {
                    Swal.fire("Atención!",objData.msg,"warning");    
                }
            }
            else {
                Swal.fire("Atención!","Todos los campos con (*) son obligatorios","warning");
            }
        },

        //guardados
        saveCreate: async function() {
            if (this.data.length > 0 && this.codigoVersion != "" && this.servicioId != 0) {
                    Swal.fire({
                        title:"¿Estás segur@ de guardar?",
                        text:"",
                        icon: 'warning',
                        showCancelButton:true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText:"Sí, guardar",
                        cancelButtonText:"No, cancelar"
                    }).then(async function(result) {
                        if(result.isConfirmed) {
                            const formData = new FormData();
                            formData.append("action","saveCreate");
                            formData.append("codigo",app.codigoVersion);
                            formData.append("servicioId",app.servicioId);
                            formData.append("data", JSON.stringify(app.data));
                            const response = await fetch(URL,{method:"POST",body:formData});
                            const objData = await response.json();

                            if (objData.status) {
                                Swal.fire("Guardado",objData.msg,"success");
                                setTimeout(function(){
                                    window.location.reload();
                                },1000);
                            }
                            else {
                                Swal.fire("Error!",objData.msg,"warning");
                            }
                        }
                    });
            }
            else {
                Swal.fire("Atención!","Todos los campos con (*) son obligatorios","warning");
            }
        },

        formatNumero: function(valor){
            return new Intl.NumberFormat("en-US", {style: "currency", currency: "USD"}).format(valor);
            
        },

        formatFecha: function(fecha) {
            // Separar la fecha en componentes
            const parts = fecha.split('-');
            if (parts.length !== 3) {
                throw new Error('Formato de fecha inválido. Use Y-m-d.');
            }
            const year = parts[0];
            const month = parts[1];
            const day = parts[2];
        
            // Retornar la fecha en el formato deseado
            return `${day}/${month}/${year}`;
        },
    },
    computed:{

    }
})
