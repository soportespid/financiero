<?php
    require_once '../../../Librerias/core/Helpers.php';
    require_once '../../../Librerias/core/Mysql.php';
    session_start();
    
    class serviciosModel extends Mysql {

        function __construct(){
            parent::__construct();
            $this->rangos = array(
                array(
                    "nameRango" => "Consumo basico",
                    "rangoInicial" => 0,
                    "rangoFinal" => 16
                ),

                array(
                    "nameRango" => "Consumo complementario",
                    "rangoInicial" => 17,
                    "rangoFinal" => 32
                ),

                array(
                    "nameRango" => "Consumo suntuario",
                    "rangoInicial" => 33,
                    "rangoFinal" => 99999
                )
            );
        }

        public function servicios() {
            $sql = "SELECT id as servicioId, nombre FROM srvservicios WHERE estado = 'S'";
            $servicios = $this->select_all($sql);
            return $servicios;
        }

        public function validaCrearTarifas(int $version, int $servicioId) {
            $request = false;
            $sql = "SELECT COUNT(*) as valor FROM srvtarifas WHERE version = $version AND id_servicio = $servicioId AND estado = 'S'";
            $valCantidad = $this->select($sql);
            $validacion = intval($valCantidad["valor"]);
            if ($validacion == 0) {
                $request = true;
            }
            
            return $request;
        }

        public function clasesDeUso(int $codigoVersion, int $servicioId) {
            $data = [];

            $sql = "SELECT id_clase as claseId, nombre_clase FROM srv_clases WHERE estado = 'S' ORDER BY id_clase";
            $clases = $this->select_all($sql);
            foreach ($clases as $clase) {
                foreach($this->rangos as $rango) {
                    $dataTemp = [
                        "claseId" => $clase["claseId"],
                        "nombre_clase" => $clase["nombre_clase"],
                        "name_rango" => $rango["nameRango"],
                        "rango_inicial" => $rango["rangoInicial"],
                        "rango_final" => $rango["rangoFinal"],
                        "valor" => 0,
                        "subsidio" => 0,
                        "contribucion" => 0
                    ];

                    array_push($data, $dataTemp);
                }
            }

            return $data;
        }

        public function buscarTarifas(int $codigoVersion, int $servicioId) {
            $data = [];
            $sql = "SELECT id_clase as claseId, nombre_clase FROM srv_clases WHERE estado = 'S' ORDER BY id_clase";
            $clases = $this->select_all($sql);
            foreach ($clases as $clase) {
                foreach($this->rangos as $rango) {
                    $sqlTarifas = "SELECT costo_unidad, subsidio, contribucion FROM srvtarifas WHERE version = $codigoVersion AND id_servicio = $servicioId AND id_estrato = $clase[claseId] AND rango_inicial = $rango[rangoInicial] AND estado = 'S'";
                    $tarifa = $this->select($sqlTarifas);
                    
                    if ($tarifa["costo_unidad"] == "") {
                        $tarifa["costo_unidad"] = 0;
                        $tarifa["subsidio"] = 0;
                        $tarifa["contribucion"] = 0;
                    }

                    $dataTemp = [
                        "claseId" => $clase["claseId"],
                        "nombre_clase" => $clase["nombre_clase"],
                        "name_rango" => $rango["nameRango"],
                        "rango_inicial" => $rango["rangoInicial"],
                        "rango_final" => $rango["rangoFinal"],
                        "valor" => $tarifa["costo_unidad"],
                        "subsidio" => $tarifa["subsidio"],
                        "contribucion" => $tarifa["contribucion"]
                    ];

                    array_push($data, $dataTemp);
                }
            }

            return $data;
        }

        public function insertaCargoFijos(int $codigoVersion, int $servicioId, array $datos) {
            $sqlUpdate = "UPDATE srvtarifas SET estado = ? WHERE version = $codigoVersion AND id_servicio = $servicioId AND estado = 'S'";
            $this->update($sqlUpdate, ['N']);

            foreach ($datos as $dato) {
                if ($dato["subsidio"] )
                $sql = "INSERT INTO srvtarifas (id_servicio, id_estrato, rango_inicial, rango_final, costo_unidad, subsidio, contribucion, estado, version) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";
                $values = [
                    $servicioId,
                    $dato["claseId"],
                    $dato["rango_inicial"],
                    $dato["rango_final"],
                    round($dato["valor"], 2),
                    intval($dato["subsidio"]),
                    intval($dato["contribucion"]),
                    'S',
                    $codigoVersion
                ];

                $request = $this->insert($sql, $values);

                if ($request == 0) {
                    $sqlDelete = "DELETE FROM srvcostos_estandar WHERE version = $codigoVersion AND id_servicio = $servicioId AND estado = 'S'";
                    $this->delete($sqlDelete);
                    break;
                }
            }

            return $request;
        }
        
        public function versiones() {
            $sql = "SELECT DISTINCT version as versionId FROM srvtarifas WHERE estado = 'S'";
            $versiones = $this->select_all($sql);

            return $versiones;
        }
    }
?>