<?php
    require_once '../../../Librerias/core/Helpers.php';
    require_once '../../../Librerias/core/Mysql.php';
    session_start();
    
    class serviciosModel extends Mysql {

        public function servicios() {
            $sql = "SELECT id as servicioId, nombre FROM srvservicios WHERE estado = 'S'";
            $servicios = $this->select_all($sql);
            return $servicios;
        }

        public function validaCrearTarifas(int $version, int $servicioId) {
            $request = false;
            $sql = "SELECT COUNT(*) as valor FROM srvcostos_estandar WHERE version = $version AND id_servicio = $servicioId AND estado = 'S'";
            $valCantidad = $this->select($sql);
            $validacion = intval($valCantidad["valor"]);
            if ($validacion == 0) {
                $request = true;
            }
            
            return $request;
        }

        public function clasesDeUso(int $codigoVersion, int $servicioId) {
            $sql = "SELECT id_clase as claseId, nombre_clase FROM srv_clases WHERE estado = 'S' ORDER BY id_clase";
            $clases = $this->select_all($sql);

            foreach ($clases as $key => $clase) {
                $clases[$key]["valor"] = 0;
                $clases[$key]["subsidio"] = 0;
                $clases[$key]["contribucion"] = 0;
            }

            return $clases;
        }

        public function buscarTarifas(int $codigoVersion, int $servicioId) {
            $sql = "SELECT SC.id_clase as claseId, 
            SC.nombre_clase,
            CE.costo_unidad as valor,
            CE.subsidio,
            CE.contribucion
            FROM srv_clases as SC
            INNER JOIN srvcostos_estandar as CE ON SC.id_clase = CE.id_estrato
            WHERE CE.version = $codigoVersion AND CE.id_servicio = $servicioId AND CE.estado = 'S'
            ORDER BY SC.id_clase";
            $tarifas = $this->select_all($sql);

            return $tarifas;
        }

        public function insertaCargoFijos(int $codigoVersion, int $servicioId, array $datos) {
            $sqlUpdate = "UPDATE srvcostos_estandar SET estado = ? WHERE version = $codigoVersion AND id_servicio = $servicioId AND estado = 'S'";
            $this->update($sqlUpdate, ['N']);

            foreach ($datos as $dato) {
                if ($dato["subsidio"] )
                $sql = "INSERT INTO srvcostos_estandar (id_servicio, id_estrato, costo_unidad, subsidio, contribucion, estado, version) VALUES (?, ?, ?, ?, ?, ?, ?)";
                $values = [
                    $servicioId,
                    $dato["claseId"],
                    round($dato["valor"], 2),
                    intval($dato["subsidio"]),
                    intval($dato["contribucion"]),
                    'S',
                    $codigoVersion
                ];

                $request = $this->insert($sql, $values);

                if ($request == 0) {
                    $sqlDelete = "DELETE FROM srvcostos_estandar WHERE version = $codigoVersion AND id_servicio = $servicioId AND estado = 'S'";
                    $this->delete($sqlDelete);
                    break;
                }
            }

            return $request;
        }
        
        public function versiones() {
            $sql = "SELECT DISTINCT version as versionId FROM srvcostos_estandar WHERE estado = 'S'";
            $versiones = $this->select_all($sql);

            return $versiones;
        }
    }
?>