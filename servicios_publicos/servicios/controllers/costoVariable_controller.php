<?php
    require_once '../../../Librerias/core/Helpers.php';
    require_once '../models/costoVariable_model.php';
    session_start();

    class serviciosController extends serviciosModel {

        public function get_data_create() {
            if (!empty($_SESSION)) {
                $arrResponse = array(
                    "servicios" => $this->servicios(),
                );
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }

            die();
        }

        public function get_data_search() {
            if (!empty($_SESSION)) {
                $arrResponse = array(
                    "servicios" => $this->servicios(),
                    "versiones" => $this->versiones()
                );
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }

            die();
        }

        public function data_clase_uso() {
            if (!empty($_SESSION)) {
                
                $codigoVersion = intval($_POST["codigo"]);
                $servicioId = intval($_POST["servicioId"]);
                if($this->validaCrearTarifas($codigoVersion, $servicioId)) {
                    $arrResponse = array(
                        "status" => true,
                        "data" => $this->clasesDeUso($codigoVersion, $servicioId)
                    );
                }
                else {
                    $arrResponse = array(
                        "status" => true,
                        "data" => $this->buscarTarifas($codigoVersion, $servicioId)
                    );
                }

                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }

            die();
        }

        public function data_tarifas() {
            if (!empty($_SESSION)) {
                $codigoVersion = intval($_POST["codigo"]);
                $servicioId = intval($_POST["servicioId"]);

                if(!$this->validaCrearTarifas($codigoVersion, $servicioId)) {
                    $arrResponse = array(
                        "status" => true,
                        "data" => $this->buscarTarifas($codigoVersion, $servicioId)
                    );
                }
                else {
                    $arrResponse = array(
                        "status" => false,
                        "msg" => "Aún no se ha creado tarifas con este código y servicio"
                    );
                }

                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
        }

        public function save_create() {
            if (!empty($_SESSION)) {
                $codigoVersion = intval($_POST["codigo"]);
                $servicioId = intval($_POST["servicioId"]);
                $data = json_decode($_POST['data'],true);
                $error = true;

                if ($codigoVersion > 0 && $servicioId > 0 && count($data) > 0) {
                    foreach ($data as $d) {
                        if ($d["subsidio"] > 0 && $d["contribucion"] > 0) {
                            $error = false;
                            break;
                        }
                    }
    
                    if ($error) {
                        $request = $this->insertaCargoFijos($codigoVersion, $servicioId, $data);

                        if ($request > 0) {
                            $arrResponse = array(
                                "status" => true,
                                "msg" => "Tarifas de costo estandar guardadas con exito"
                            );
                        }
                        else {
                            $arrResponse = array(
                                "status" => false,
                                "msg" => "Error en guardado, vuelva intentarlo"
                            );
                        }
                    }
                    else {
                        $arrResponse = array(
                            "status" => false,
                            "msg" => "No puede haber valores de subsidio y contribución en una misma clase de uso"
                        );
                    }
                }
                else {
                    $arrResponse = array(
                        "status" => false,
                        "msg" => "Datos de guardado no han llegado, contacte con soporte"
                    );
                }

                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }

            die();
        }
    }

    if($_POST){
        $obj = new serviciosController();

        switch ($_POST["action"]) {
            case 'getDataCreate':
                $obj->get_data_create();
                break;

            case 'getDataSearch':
                $obj->get_data_search();
                break;

            case 'dataClaseUso':
                $obj->data_clase_uso();
                break;

            case 'saveCreate':
                $obj->save_create();
                break;

            case 'searchTarifas':
                $obj->data_tarifas();
                break;

            default:
                $arrResponse = ["status" => false, "msg" => "Acción no encontrada"];
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
                break;
        }
    }
?>