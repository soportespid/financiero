<?php
    
	require_once '../../../comun.inc';
    require '../../../funciones.inc';
    require '../../../funcionesSP.inc.php';

    session_start();
  
    if($_POST){
        
        $obj = new RecaudoFactura();
        if($_POST['action'] == 'search'){
            $currentPage = intval($_POST['page']); 
            $obj->show(
                $_POST['intConsecutivo'],
                $_POST['intCodigoUsuario'],
                $_POST['intNumeroFactura'],
                $_POST['dateInicio'],
                $_POST['dateFinal'],
                $currentPage
            );
        }
    }

    class RecaudoFactura{
        private $linkbd;
        private $strCodigo;
        private $strConsecutivo;
        private $strFactura;
        private $dateInicio;
        private $dateFinal;
        public function __construct(){
            $this->linkbd = conectar_v7();
            $this->linkbd->set_charset("utf8");
        }

        public function show(string $consecutivo,string $codigo,string $factura, string $fechaInicio, string $fechaFinal,int $currentPage){

            if($fechaInicio !="" && $fechaFinal ==""){
                $arrResponse = array("status"=>false,"msg"=>"Para buscar por fechas, debe llenar ambos campos. Inténtelo de nuevo.");
            }elseif($fechaInicio =="" && $fechaFinal !=""){
                $arrResponse = array("status"=>false,"msg"=>"Para buscar por fechas, debe llenar ambos campos. Inténtelo de nuevo.");
            }else{
                $this->strCodigo = $codigo;
                $this->strFactura = $factura;
                $this->strConsecutivo = $consecutivo;
                $dateRange ="";
                $perPage = 200;
                $startRows = ($currentPage-1) * $perPage;
                $totalRows = 0;
                if($fechaInicio !="" && $fechaFinal !=""){
                    $arrFechaInicio = explode("/",$fechaInicio);
                    $arrFechaFinal = explode("/",$fechaFinal);

                    $this->dateInicio = date_format(date_create($arrFechaInicio[2]."-".$arrFechaInicio[1]."-".$arrFechaInicio[0]),"Y-m-d");
                    $this->dateFinal = date_format(date_create($arrFechaFinal[2]."-".$arrFechaFinal[1]."-".$arrFechaFinal[0]),"Y-m-d");

                    $dateRange = " AND fecha_recaudo >= '$this->dateInicio' AND fecha_recaudo <= '$this->dateFinal'";
                }
                $sql = "SELECT 
                        id,
                        codigo_recaudo as consecutivo,
                        DATE(fecha_recaudo) as fecha,
                        DATE_FORMAT(fecha_recaudo,'%d/%m/%Y') as fecha_recaudo,
                        cod_usuario, 
                        suscriptor as nombre,
                        numero_factura,
                        descuento_intereses as descuento,
                        medio_pago,
                        valor_pago,
                        usuario as realiza,
                        estado 
                        FROM srv_recaudo_factura
                        WHERE cod_usuario LIKE '$this->strCodigo%' AND codigo_recaudo LIKE '$this->strConsecutivo%'
                        AND numero_factura LIKE '$this->strFactura%' $dateRange 
                        ORDER BY fecha DESC LIMIT $startRows, $perPage
                        
                ";
                $request = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
                $totalRows = intval(mysqli_query($this->linkbd,
                "SELECT count(*) as total 
                FROM srv_recaudo_factura
                WHERE cod_usuario LIKE '$this->strCodigo%' 
                AND numero_factura LIKE '$this->strFactura%' AND codigo_recaudo LIKE '$this->strConsecutivo%' $dateRange")->fetch_assoc()['total']);
                $totalPages = $totalRows > 0 ? ceil($totalRows/$perPage) : 1;
                $arrResponse = array("status"=>true,"data"=>$request,"total"=>$totalRows,"total_pages"=>$totalPages);
            }
            
            echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            die();
        }
    }