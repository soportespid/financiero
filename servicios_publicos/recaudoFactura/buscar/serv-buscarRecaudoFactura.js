const URL = 'servicios_publicos/recaudoFactura/buscar/serv-buscarRecaudoFactura.php';
import { filtroEnArrayDeObjetos } from './../../../funciones.js';

var app = new Vue({
    el: '#myapp',
    data() {
        return {
            arrData:[],
            intCodigoUsuario:"",
            intNumeroFactura:"",
            intConsecutivo:"",
            intResults:0,
            intTotalPages:1,
            intPage:1,
            isLoading: false,
            error: ''
        }
    },

    mounted: function(){

    },

    methods: 
    {
        search:async function(page=1){

            let fechaInicial = document.querySelector("#fechaInicial").value;
            let fechaFinal = document.querySelector("#fechaFinal").value;

            if(page <= 0){
                page = 1;
                this.intPage = page;
            }else if(page > this.intTotalPages){
                page = this.intTotalPages;
                this.intPage = page;
            }

            const formData = new FormData();
            formData.append("action","search");
            formData.append("intCodigoUsuario",this.intCodigoUsuario);
            formData.append("intNumeroFactura",this.intNumeroFactura);
            formData.append("intConsecutivo",this.intConsecutivo);
            formData.append("dateInicio",fechaInicial);
            formData.append("dateFinal",fechaFinal);
            formData.append("page",page);
            this.isLoading = true;
            const response = await fetch(URL,{method:'POST',body:formData});
            const objData = await response.json();
            if(objData.status){
                this.arrData = objData.data;
                this.intResults = objData.total;
                this.intTotalPages = objData.total_pages;
            }else{
                Swal.fire("Error",objData.msg,"error");
            }
            this.isLoading = false;
        },
        printPDF:function(){
            let fechaInicial = document.querySelector("#fechaInicial").value;
            let fechaFinal = document.querySelector("#fechaFinal").value; 
            window.open('serv-buscarRecaudoFacturaPdf.php?codigo='+
            this.intCodigoUsuario+'&factura='+this.intNumeroFactura+'&consecutivo='+
            this.intConsecutivo+'&dateInicio='+
            fechaInicial+'&dateFinal='+fechaFinal,'_blank');
        },
        printExcel: function(){
            let fechaInicial = document.querySelector("#fechaInicial").value;
            let fechaFinal = document.querySelector("#fechaFinal").value; 
            window.open('serv-buscarRecaudoFacturaExcel.php?codigo='+
            this.intCodigoUsuario+'&factura='+this.intNumeroFactura+'&consecutivo='+
            this.intConsecutivo+'&dateInicio='+
            fechaInicial+'&dateFinal='+fechaFinal,'_blank');
        },
        view:function(id){
            window.location.href='serv-recaudoFacturaVisualizar.php?id='+id;
        },
        //Desglosar array
        toFormData: function(obj){
            var form_data = new FormData();
            for(var key in obj){
                form_data.append(key, obj[key]);
            }
            return form_data;
        },

        formatNumero: function(valor){
            return new Intl.NumberFormat("en-US", {style: "currency", currency: "USD"}).format(valor);
        },
    },
});