const URL = 'servicios_publicos/recaudoFactura/crear/serv-recaudoFactura.php';
import { filtroEnArrayDeObjetos } from './../../../funciones.js';

var app = new Vue({
    el: '#myapp',
	data:{

        error: '',
        loading: false,
        movimiento: 1,
        descuentoIntereses: false,
        showPorcentaje: false,
        porcentaje: '',
        medioPago: '',
        showBanco: false,
        consecutivo: '',
        factura: '',
        codUsuario: '',
        clienteId: '',
        documento: '',
        tercero: '',
        valorFactura: 0,
        valorPago: 0,
        bancos: [],
        banco: '',
        valorIntereses: 0,
        concepto: '',

        consecRev: '',
        codUsuRev: '',
        numFacturaRev: '',
        valorPagoRev: '',
        motivoRev: '',
    },

    mounted: function(){

        this.datosIniciales();
    },

    methods: 
    {
        //Desglosar array
        toFormData: function(obj){
            var form_data = new FormData();
            for(var key in obj){
                form_data.append(key, obj[key]);
            }
            return form_data;
        },

        formatonumero: function(valor){
            return new Intl.NumberFormat("en-US", {style: "currency", currency: "USD"}).format(valor);
        },

        datosIniciales: async function() {
            
            await axios.post(URL+'?action=datosIniciales')
            .then((response) => {
                
                this.bancos = response.data.bancos;
                this.consecutivo = response.data.consecutivo;
            });
        },

        buscaFactura: async function() {

            this.descuentoIntereses = false;
            this.porcentaje = "";
            this.valorFactura = 0;
            this.valorPago = 0;

            var formData = new FormData();
            formData.append("factura", this.factura);

            await axios.post(URL+'?action=buscaFactura', formData)
            .then((response) => {
                
                if (response.data.error == true) {

                    Swal.fire({
                        icon: 'error',
                        title: 'Factura no disponible',
                        text: response.data.mensaje
                    })

                    this.factura = ""; 
                    this.codUsuario = "";
                    this.documento = "";
                    this.tercero = "";
                    this.concepto = "";
                    this.valorFactura = "";
                    this.valorPago = "";
                    this.valorIntereses = "";
                    this.clienteId = "";
                }
                else {
                    this.codUsuario = response.data.codUsuario;
                    this.documento = response.data.documento;
                    this.tercero = response.data.nombre;
                    this.concepto = response.data.concepto;
                    this.valorFactura = response.data.valor;
                    this.valorPago = response.data.valor;
                    this.valorIntereses = response.data.valorIntereses;
                    this.clienteId = response.data.clienteId;
                }
            });
        },

        cambioIntereses: function() {

            if (this.descuentoIntereses == true) {

                this.porcentaje = "";
                this.showPorcentaje = true;
            }
            else {
                this.porcentaje = "";
                this.showPorcentaje = false;
            }
        },

        calcularDescuento: function() {

            if (this.porcentaje != "") {
                
                var porcentaje = this.porcentaje / 100;
                var valorDescuento = this.valorIntereses * porcentaje; 
                var valorPago = this.valorFactura - valorDescuento;
                valorPago = Math.ceil(valorPago / 100) * 100;
                this.valorPago = valorPago;
            }
            else {
                this.valorPago = this.valorFactura;    
            }
        },

        cambioBanco: function() {

            if (this.medioPago == 'banco') {
                this.showBanco = true;
                this.banco = "";
            }
            else{
                this.showBanco = false;
                this.banco = "";
            }
        },

        buscaRecaudoRev: async function() {

            if (this.consecRev != "") {

                this.codUsuRev = "";
                this.numFacturaRev = "";
                this.valorPagoRev = "";
                await axios.post(URL+'?action=buscaRecaudo&consec='+this.consecRev)
                .then((response) => {
                    console.log(response.data);
                    if (response.data.estado  == "S") {
                        this.codUsuRev = response.data.codUsu;
                        this.numFacturaRev = response.data.numFactura;
                        this.valorPagoRev = response.data.valor;
                    }
                    else if (response.data.estado  == "N") {
                        Swal.fire("Error","Factura ya fue anulada","error");
                        this.codUsuRev = "";
                        this.numFacturaRev = "";
                        this.valorPagoRev = "";
                        this.consecRev = "";
                    }
                    else if (response.data.estado == "C") {
                        Swal.fire("Error","Corte liquidado ya fue cerrado","error");
                        this.codUsuRev = "";
                        this.numFacturaRev = "";
                        this.valorPagoRev = "";
                        this.consecRev = "";
                    }
                });
            }
        },

        guardar: async function() {

            if (this.movimiento == "1") {
                const fecha = document.getElementById('fechaRecaudo').value;
                if (this.medioPago != "" && this.banco != "") {
                    Swal.fire({
                        icon: 'question',
                        title: 'Esta seguro que quiere guardar?',
                        showDenyButton: true,
                        confirmButtonText: 'Guardar!',
                        denyButtonText: 'Cancelar',
                    }).then((result) => {
        
                        if (result.isConfirmed) {
        
                            var formData = new FormData();
                            formData.append("fecha", fecha);
                            formData.append("factura", this.factura);
                            formData.append("codUsuario", this.codUsuario);
                            formData.append("clienteId", this.clienteId);
                            formData.append("documento", this.documento);
                            formData.append("nombre", this.tercero);
                            formData.append("valorFactura", this.valorFactura);
                            formData.append("porcentaje", this.porcentaje);
                            formData.append("valorPago", this.valorPago);
                            formData.append("medioPago", this.medioPago);
                            formData.append("banco", this.banco);
                            formData.append("concepto", this.concepto);
            
                            axios.post(URL+'?action=guardar', formData)
                            .then((response) => {
                                // console.log(response.data);
                                if(response.data.insertaBien){
                                    Swal.fire({
                                        icon: 'success',
                                        title: 'Recaudo de factura guardado con exito ',
                                        showConfirmButton: false,
                                        timer: 3500
                                    }).finally(() => {
                                        location.reload();
                                    });
                                }
                                else{
                                
                                    Swal.fire(
                                        'Error!',
                                        'No se pudo guardar.',
                                        'error'
                                    );
                                }
                            });
                        } 
                    })  
                }
                else {
                    Swal.fire({
                        icon: 'error',
                        title: 'Información incompleta',
                        text: 'Falta seleccionar el medio de pago'
                    })
                }
            }
            else {
                if (this.consecRev != "" && this.numFacturaRev != "") {
                    Swal.fire({
                        icon: 'question',
                        title: 'Esta seguro que quiere anular este pago?',
                        showDenyButton: true,
                        confirmButtonText: 'Anular!',
                        denyButtonText: 'Cancelar',
                    }).then((result) => {
        
                        if (result.isConfirmed) {
        
                            var formData = new FormData();
                            formData.append("consecutivo", this.consecRev);
                            formData.append("numFactura", this.numFacturaRev);
            
                            axios.post(URL+'?action=anular', formData)
                            .then((response) => {
                                console.log(response.data);
                                if(response.data.insertaBien){
                                    Swal.fire({
                                        icon: 'success',
                                        title: 'Recaudo anulado con exito',
                                        showConfirmButton: false,
                                        timer: 3500
                                    }).finally(() => {
                                        location.reload();
                                    });
                                }
                                else{
                                
                                    Swal.fire(
                                        'Error!',
                                        'No se pudo guardar.',
                                        'error'
                                    );
                                }
                            });
                        } 
                    })  
                }
            }
        },

    },
});