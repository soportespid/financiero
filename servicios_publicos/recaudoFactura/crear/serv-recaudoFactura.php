<?php

	require_once '../../../comun.inc';
    require '../../../funciones.inc';
    require '../../../funcionesSP.inc.php';
    header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
    session_start();

	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");

    $out = array('error' => false);

    $action = "show";

    if(isset($_GET['action'])){
        $action = $_GET['action'];
    }

    if ($action == "datosIniciales") {

        $bancos = [];
        $sqlBancos = "SELECT B.cuenta,B.tercero,B.ncuentaban,B.tipo,T.razonsocial FROM tesobancosctas B JOIN terceros T ON B.tercero = T.cedulanit";
        $resBancos = mysqli_query($linkbd, $sqlBancos);
        while ($rowBancos = mysqli_fetch_row($resBancos)) {
            
            array_push($bancos, $rowBancos);
        }   

        $out['bancos'] = $bancos;

        $consecutivo = "";
        $consecutivo = selconsecutivo('srv_recaudo_factura','codigo_recaudo');
        $out['consecutivo'] = $consecutivo;
    }

    if ($action == "buscaFactura") {

        $factura = $_POST['factura'];

        $sqlEstado = "SELECT estado_pago, id_cliente FROM srvcortes_detalle WHERE numero_facturacion = $factura";
        $resEstado = mysqli_query($linkbd, $sqlEstado);
        $rowEstado = mysqli_fetch_row($resEstado);

        if ($rowEstado[0] == 'S') {

            $nombre = "";
            $documento = "";
            $codUsuario = "";
            $concepto = "";
            $valor = 0;
            $valorIntereses = 0;
            $clienteId = "";

            $sqlCliente = "SELECT id_tercero, cod_usuario, id FROM srvclientes WHERE id = $rowEstado[1]";
            $resCliente = mysqli_query($linkbd, $sqlCliente);
            $rowCliente = mysqli_fetch_row($resCliente);

            $clienteId = $rowCliente[2];

            $nombre = buscaNombreTerceroConId($rowCliente[0]);
            $documento = buscaDocumentoTerceroConId($rowCliente[0]);
            $codUsuario = $rowCliente[1];

            $sqlValor = "SELECT SUM(credito), SUM(debito) FROM srvdetalles_facturacion WHERE numero_facturacion = $factura AND tipo_movimiento = '101'";
            $resValor = mysqli_query($linkbd, $sqlValor);
            $rowValor = mysqli_fetch_row($resValor);

            $valor = $rowValor[0] - $rowValor[1];
            $valorFactura = ceil ($valor / 100) * 100;

            $sqlIntereses = "SELECT SUM(credito) FROM srvdetalles_facturacion WHERE numero_facturacion = $factura AND tipo_movimiento = '101' AND id_tipo_cobro = 10";
            $resIntereses = mysqli_query($linkbd, $sqlIntereses);
            $rowIntereses = mysqli_fetch_row($resIntereses);

            $valorIntereses = $rowIntereses[0];

            $concepto = "Pago de la factura $factura del codigo de usuario $codUsuario";

            $out['nombre'] = $nombre;
            $out['documento'] = $documento;
            $out['codUsuario'] = $codUsuario;
            $out['concepto'] = $concepto;
            $out['valor'] = $valorFactura;
            $out['valorIntereses'] = $valorIntereses;
            $out['clienteId'] = $clienteId;
        }
        else {
            $mensaje = "";

            switch ($rowEstado[0]) {
                case 'V':
                    $mensaje = "Factura se encuentra vencida";
                break;

                case 'A':
                    $mensaje = "Factura se le realizo un acuerdo de pago";
                break;

                case 'P':
                    $mensaje = "Factura ya fue pagada";
                break;
                
                default:
                    $mensaje = "Factura no disponible";
                break;
            }

            $out['error'] = true;
            $out['mensaje'] = $mensaje;
        }
    }

    if ($action == "buscaRecaudo") {
        
        if (!empty($_SESSION)){
            $consec = $_GET["consec"];

            $sqlRec = "SELECT cod_usuario, numero_factura, valor_pago, estado FROM srv_recaudo_factura WHERE codigo_recaudo = $consec";
            $rowRec = mysqli_fetch_assoc(mysqli_query($linkbd, $sqlRec));

            $sqlFac = "SELECT estado FROM srvcortes_detalle AS DF, srvcortes AS SC WHERE DF.numero_facturacion = $rowRec[numero_factura] AND DF.id_corte = SC.numero_corte";
            $rowFac = mysqli_fetch_assoc(mysqli_query($linkbd, $sqlFac));

            if ($rowRec["estado"] == "REVERSADO") {
                $out["estado"] = "N";
            }
            else if ($rowRec["estado"] == "ACTIVO") {
                if ($rowFac["estado"] == "S") {
                    $out["estado"] = "S";
                    $out["codUsu"] = $rowRec["cod_usuario"];
                    $out["numFactura"] = $rowRec["numero_factura"];
                    $out["valor"] = $rowRec["valor_pago"];
                }
                else  if ($rowFac["estado"] == "C") {
                    $out["estado"] = "C";
                }
            }
        }
    }

    if ($action == "anular") {

        $consecRec = $_POST["consecutivo"];
        $numFactura = $_POST["numFactura"];
        $usuario = $_SESSION["usuario"];
        $cedula = $_SESSION["cedulausu"];
        $ip = getRealIP();
        $hoy = date("Y-m-d H:i:s");
        
        //Anular en contabilidad
        $sqlCont = "UPDATE comprobante_cab SET estado = '0' WHERE tipo_comp = 30 AND numerotipo = $consecRec";
        mysqli_query($linkbd, $sqlCont);

        //Anular en srv
        $sqlSrv = "UPDATE srv_recaudo_factura SET estado = 'REVERSADO' WHERE codigo_recaudo = $consecRec";
        mysqli_query($linkbd, $sqlSrv);

        //habilitar de nuevo factura para pago
        $sqlDet = "UPDATE srvcortes_detalle SET estado_pago = 'S' WHERE numero_facturacion = $numFactura";
        mysqli_query($linkbd, $sqlDet);

        //auditoria
        $sqlAuditoria = "INSERT INTO srv_auditoria (id_srv_funciones, accion, cedula, usuario, ip, fecha_hora, consecutivo) VALUES (4, 'Anular', '$cedula', '$usuario', '$ip', '$hoy', $consecRec)";
        mysqli_query($linkbd, $sqlAuditoria);

        $out["insertaBien"] = true;
    }

    if ($action == "guardar") {
        if(!empty($_SESSION)){
            
            $fec = explode("/", $_POST['fecha']);
            $fecha = $fec[2].'-'.$fec[1].'-'.$fec[0];
            $usuario = $_SESSION['usuario'];
            $cedula = $_SESSION['cedulausu'];
            $sql = "SELECT count(*) as valor FROM dominios dom  
            WHERE dom.nombre_dominio = 'PERMISO_MODIFICA_DOC'  AND dom.valor_final <= '$fecha'  AND dom.valor_inicial =  '$cedula'";
            $request = mysqli_query($linkbd,$sql)->fetch_assoc()['valor'];
            if($request >= 1){
                $tipoComprobante = "30";
                $numFactura = $_POST['factura'];
                $vigencia = $fec[2];
                $clienteId = $_POST['clienteId'];
                $documento = $_POST['documento'];
                $codUsuario = $_POST['codUsuario'];
                $nombre = $_POST['nombre'];
                $medioPago = $_POST['medioPago'];
                $valorFactura = $_POST['valorFactura'];
                $valorPago = $_POST['valorPago'];
                $porcentajeDescuento = $_POST['porcentaje'];
                $concepto = $_POST['concepto'];
                $valorSubtotal = $valorTotal = 0;
                
                //valida que ese número de factura no tenga ningun registro activo en la tabla
                $sql = "SELECT count(*) as valor FROM srv_recaudo_factura WHERE numero_factura = $numFactura AND estado = 'ACTIVO'";
                $numRegistros = mysqli_fetch_assoc(mysqli_query($linkbd, $sql));

                if ($numRegistros["valor"] == 0) {
                    $consecutivo = selconsecutivo('srv_recaudo_factura', 'codigo_recaudo');
            
                    if ($medioPago == "caja") {
                        $sqlCuentaCaja = "SELECT cuentacaja FROM tesoparametros";
                        $resCuentaCaja = mysqli_query($linkbd,$sqlCuentaCaja);
                        $rowCuentaCaja = mysqli_fetch_row($resCuentaCaja);
            
                        $cuentaDebito = $rowCuentaCaja[0];
                    }
                    else if ($medioPago == "banco") {
                        $cuentaDebito = $_POST['banco'];
                    }
            
                    //contabilidad
                    $facturas = [];
                    
                    $sql = "SELECT numero_facturacion, estado_pago FROM srvcortes_detalle WHERE id_cliente = $clienteId AND numero_facturacion <= $numFactura ORDER BY numero_facturacion DESC";
                    $res = mysqli_query($linkbd, $sql);
                    while ($row = mysqli_fetch_row($res)) {
            
                        if ($row[1] == "S" OR $row[1] == "V") {
            
                            $facturas[] = $row[0];
                        }
                        else if ($row[1] == "P" OR $row[1] == "A" OR $row[1] == "A") {
                            
                            break;
                        }
                    }
            
                    $cargoFijo = $consumo = $subCargoFijo = $subConsumo = $contribucion = $acuerdoPago = $saldoInicial = $interesMoratorio = $desincentivo = [];
    
                    foreach ($facturas as $key => $factura) {
            
                        $sqlDet = "SELECT id_servicio, id_tipo_cobro, COALESCE(credito,0), COALESCE(debito,0), corte FROM srvdetalles_facturacion WHERE numero_facturacion = $factura AND (credito > 0 OR debito > 0) ORDER BY id_tipo_cobro ASC, id_servicio ASC";
                        $resDet = mysqli_query($linkbd, $sqlDet);
                        while ($rowDet = mysqli_fetch_row($resDet)) {
                    
                            switch ($rowDet[1]) {
                                case 1:
                                    $cargoFijo[$rowDet[0]] += $rowDet[2];
                                break;
                                
                                case 2:
                                    $consumo[$rowDet[0]] += $rowDet[2];
                                break;
                    
                                case 3:
                                    $subCargoFijo[$rowDet[0]] += $rowDet[3];
                                break;
                    
                                case 4:
                                    $subConsumo[$rowDet[0]] += $rowDet[3];
                                break;
                    
                                case 5:
                                    $contribucion[$rowDet[0]] += $rowDet[2];
                                break;
                    
                                case 7:
                                    $acuerdoPago[$rowDet[0]] += $rowDet[2];
                                break;
            
                                case 8:
                                    //Saldos iniciales
                                    if ($rowDet[4] == 1) {
                                        
                                        $saldoInicial[$rowDet[0]] = (float) $rowDet[2];
                                    }
                                break;
                    
                                case 10;
                                    if ($factura == $numFactura) {
                                        $interesMoratorio[$rowDet[0]] = (float) $rowDet[2];
                                    }    
                                break;
    
                                case 12:
                                    $desincentivo[$rowDet[0]] += $rowDet[2];
                                break;
                            }
                        }
                    }
            
                    for ($i=1; $i <= count($cargoFijo); $i++) { 
            
                        //Descuento de subsidios
                        $cargoFijo[$i] = $cargoFijo[$i] - $subCargoFijo[$i];
                        $consumo[$i] = $consumo[$i] - $subConsumo[$i];
                    }
            
                    $sqlRecaudoFactura = "INSERT INTO srv_recaudo_factura (codigo_recaudo, id_cliente, cod_usuario, suscriptor, documento, numero_factura, fecha_recaudo, medio_pago, numero_cuenta, concepto, valor_pago, usuario, estado, descuento_intereses) VALUES ($consecutivo, $clienteId, '$codUsuario', '$nombre', '$documento', $numFactura, '$fecha', '$medioPago', '$cuentaDebito', '$concepto', $valorPago, '$usuario', 'ACTIVO', '$porcentajeDescuento')";
                    mysqli_query($linkbd, $sqlRecaudoFactura);
            
                    $sqlUpdate = "UPDATE srvcortes_detalle SET estado_pago = 'P' WHERE numero_facturacion = $numFactura";
                    mysqli_query($linkbd, $sqlUpdate);
            
                    $sqlComprobanteCabecera = "INSERT INTO comprobante_cab (numerotipo, tipo_comp, fecha, concepto, total_debito, total_credito, estado) VALUES ($consecutivo, '$tipoComprobante', '$fecha', 'Pago de la factura numero $factura', 0, 0, '1')";
                    mysqli_query($linkbd, $sqlComprobanteCabecera);
            
                    for ($i=1; $i <= count($cargoFijo); $i++) { 
    
                        $cargoFijo[$i] = round($cargoFijo[$i], 2);
                        $consumo[$i] = round($consumo[$i], 2);
                        $contribucion[$i] = round($contribucion[$i], 2);
                        $acuerdoPago[$i] = round($acuerdoPago[$i], 2);
                        $interesMoratorio[$i] = round($interesMoratorio[$i], 2);
                        $saldoInicial[$i] = round($saldoInicial[$i], 2);
                        $desincentivo[$i] = round($desincentivo[$i], 2);
    
                        if ($cargoFijo[$i] > 0) {
                            
                            $sql = "SELECT cargo_fijo_u, cc, nombre FROM srvservicios WHERE id = $i";
                            $res = mysqli_query($linkbd,$sql);
                            $row = mysqli_fetch_row($res);
                        
                            $concepto = concepto_cuentasn2($row[0],'SS',10,$row[1],$fecha);
                        
                            for ($x=0; $x < count($concepto); $x++) 
                            { 
                                if($concepto[$x][2] == 'S')
                                {
                                    $cuentaCredito = $concepto[$x][0];
                                }
                            }
                    
                            $sqlr="INSERT INTO comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque,valdebito, valcredito,estado,vigencia) 
                                VALUES ('$tipoComprobante $consecutivo', '$cuentaDebito', '$documento', '$row[1]', 'Pago del cargo fijo de $row[2]', '', $cargoFijo[$i], 0, '1', '$vigencia')";
                            mysqli_query($linkbd,$sqlr);
                    
                            $sqlr="INSERT INTO comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque,valdebito, valcredito,estado,vigencia) 
                                VALUES ('$tipoComprobante $consecutivo', '$cuentaCredito', '$documento', '$row[1]', 'Pago del cargo fijo de $row[2]', '', 0, $cargoFijo[$i], '1', '$vigencia')";
                            mysqli_query($linkbd,$sqlr);
                        }
            
                        if ($consumo[$i] > 0) {
            
                            $sql = "SELECT consumo_u, cc, nombre FROM srvservicios WHERE id = $i ";
                            $res = mysqli_query($linkbd,$sql);
                            $row = mysqli_fetch_row($res);
            
                            $concepto = concepto_cuentasn2($row[0],'CL',10,$row[1],$fecha);
            
                            for ($x=0; $x < count($concepto); $x++) 
                            { 
                                if($concepto[$x][2] == 'S')
                                {
                                    $cuentaCredito = $concepto[$x][0];
                                }
                            }
            
                            $sqlr="INSERT INTO comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque,valdebito, valcredito,estado,vigencia) 
                                VALUES ('$tipoComprobante $consecutivo', '$cuentaDebito', '$documento', '$row[1]', 'Pago del consumo de $row[2]', '', $consumo[$i], 0, '1', '$vigencia')";
                            mysqli_query($linkbd,$sqlr);
                    
                            $sqlr="INSERT INTO comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque,valdebito, valcredito,estado,vigencia) 
                                VALUES ('$tipoComprobante $consecutivo', '$cuentaCredito', '$documento', '$row[1]', 'Pago del consumo de $row[2]', '', 0, $consumo[$i], '1', '$vigencia')";
                            mysqli_query($linkbd,$sqlr);
                        }
            
                        if ($contribucion[$i] > 0) {
            
                            $sqlServicio = "SELECT contribucion_u, cc, nombre FROM srvservicios WHERE id = $i ";
                            $resServicio = mysqli_query($linkbd,$sqlServicio);
                            $rowServicio = mysqli_fetch_row($resServicio);
            
                            $concepto = concepto_cuentasn2($rowServicio[0],'SC',10,$rowServicio[1],$fecha);
            
                            for ($x=0; $x < count($concepto); $x++) 
                            { 
                                if($concepto[$x][2] == 'S')
                                {
                                    $cuentaCredito = $concepto[$x][0];
                                }
                            }
            
                            $sqlr="INSERT INTO comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque,valdebito, valcredito,estado,vigencia) 
                                VALUES ('$tipoComprobante $consecutivo', '$cuentaDebito', '$documento', '$row[1]', 'Pago de la contribuccion de $row[2]', '', $contribucion[$i], 0, '1', '$vigencia')";
                            mysqli_query($linkbd,$sqlr);
                    
                            $sqlr="INSERT INTO comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque,valdebito, valcredito,estado,vigencia) 
                                VALUES ('$tipoComprobante $consecutivo', '$cuentaCredito', '$documento', '$row[1]', 'Pago de la contribuccion de $row[2]', '', 0, $contribucion[$i], '1', '$vigencia')";
                            mysqli_query($linkbd,$sqlr);
                        }
            
                        if ($acuerdoPago[$i] > 0) {
                            $sqlServicio = "SELECT acuerdo_pago, cc, nombre FROM srvservicios WHERE id = $i ";
                            $resServicio = mysqli_query($linkbd,$sqlServicio);
                            $rowServicio = mysqli_fetch_row($resServicio);
    
                            $concepto = concepto_cuentasn2($row[0],'AP',10,$row[1],$fecha);
                                    
                            for ($x=0; $x < count($concepto); $x++) 
                            { 
                                if($concepto[$x][2] == 'S')
                                {
                                    $cuentaCredito = $concepto[$x][0];
                                }
                            }
            
                            $sqlr="INSERT INTO comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque,valdebito, valcredito,estado,vigencia) 
                                VALUES ('$tipoComprobante $consecutivo', '$cuentaDebito', '$documento', '$row[1]', 'Pago de acuerdo de pago de $row[2]', '', $acuerdoPago[$i], 0, '1', '$vigencia')";
                            mysqli_query($linkbd,$sqlr);
                    
                            $sqlr="INSERT INTO comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque,valdebito, valcredito,estado,vigencia) 
                                VALUES ('$tipoComprobante $consecutivo', '$cuentaCredito', '$documento', '$row[1]', 'Pago de acuerdo de pago de $row[2]', '', 0, $acuerdoPago[$i], '1', '$vigencia')";
                            mysqli_query($linkbd,$sqlr);
                        }
            
                        if ($interesMoratorio[$i] > 0) {
            
                            if ($porcentajeDescuento != "") {
                            
                                $porcentaje = $descuento = 0;
                                $porcentaje = $porcentajeDescuento / 100;
                                $descuento = $interesMoratorio[$i] * $porcentaje;
                                $interesMoratorio[$i] = $interesMoratorio[$i] - $descuento;
                            }   
            
                            $sqlServicio = "SELECT interes_moratorio, cc, nombre FROM srvservicios WHERE id = $i ";
                            $resServicio = mysqli_query($linkbd,$sqlServicio);
                            $rowServicio = mysqli_fetch_row($resServicio);
            
                            $concepto = concepto_cuentasn2($rowServicio[0],'SM',10,$rowServicio[1],$fecha);
            
                            for ($x=0; $x < count($concepto); $x++) 
                            { 
                                if($concepto[$x][3] == 'S' and $concepto[$x][2] == 'N') {
                                    $cuentaCuatro = $concepto[$x][0];
                                }
                                if ($concepto[$x][3] == 'N' and $concepto[$x][2] == 'S') {
                                    $cuentaCredito = $concepto[$x][0];
                                }
                            }
            
                            //Liquidación de intereses
                            $sqlr="INSERT INTO comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque,valdebito, valcredito,estado,vigencia) 
                            VALUES ('$tipoComprobante $consecutivo', '$cuentaCredito', '$documento', '$row[1]', 'Liquido interes moratorio de $row[2]', '', $interesMoratorio[$i], 0, '1', '$vigencia')";
                            mysqli_query($linkbd,$sqlr);
                    
                            $sqlr="INSERT INTO comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque,valdebito, valcredito,estado,vigencia) 
                                VALUES ('$tipoComprobante $consecutivo', '$cuentaCuatro', '$documento', '$row[1]', 'Liquido interes moratorio de $row[2]', '', 0, $interesMoratorio[$i], '1', '$vigencia')";
                            mysqli_query($linkbd,$sqlr);
            
                            //Pago de intereses
                            $sqlr="INSERT INTO comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque,valdebito, valcredito,estado,vigencia) 
                                VALUES ('$tipoComprobante $consecutivo', '$cuentaDebito', '$documento', '$row[1]', 'Pago de interes moratorio de $row[2]', '', $interesMoratorio[$i], 0, '1', '$vigencia')";
                            mysqli_query($linkbd,$sqlr);
                    
                            $sqlr="INSERT INTO comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque,valdebito, valcredito,estado,vigencia) 
                                VALUES ('$tipoComprobante $consecutivo', '$cuentaCredito', '$documento', '$row[1]', 'Pago de interes moratorio de $row[2]', '', 0, $interesMoratorio[$i], '1', '$vigencia')";
                            mysqli_query($linkbd,$sqlr);
                        }
            
                        if ($saldoInicial[$i] > 0) {
            
                            $sql = "SELECT consumo_u, cc, nombre FROM srvservicios WHERE id = $i ";
                            $res = mysqli_query($linkbd,$sql);
                            $row = mysqli_fetch_row($res);
            
                            $concepto = concepto_cuentasn2($row[0],'CL',10,$row[1],$fecha);
            
                            for ($x=0; $x < count($concepto); $x++) 
                            { 
                                if($concepto[$x][2] == 'S')
                                {
                                    $cuentaCredito = $concepto[$x][0];
                                }
                            }
            
                            $sqlr="INSERT INTO comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque,valdebito, valcredito,estado,vigencia) 
                                VALUES ('$tipoComprobante $consecutivo', '$cuentaDebito', '$documento', '$row[1]', 'Pago del saldo inicial de $row[2]', '', $saldoInicial[$i], 0, '1', '$vigencia')";
                            mysqli_query($linkbd,$sqlr);
                    
                            $sqlr="INSERT INTO comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque,valdebito, valcredito,estado,vigencia) 
                                VALUES ('$tipoComprobante $consecutivo', '$cuentaCredito', '$documento', '$row[1]', 'Pago del saldo inicial de $row[2]', '', 0, $saldoInicial[$i], '1', '$vigencia')";
                            mysqli_query($linkbd,$sqlr);
                        }
    
                        if ($desincentivo[$i] > 0) {
    
                            $sql = "SELECT consumo_u, cc, nombre FROM srvservicios WHERE id = $i ";
                            $res = mysqli_query($linkbd,$sql);
                            $row = mysqli_fetch_row($res);
            
                            $concepto = concepto_cuentasn2($row[0],'CL',10,$row[1],$fecha);
            
                            for ($x=0; $x < count($concepto); $x++) 
                            { 
                                if($concepto[$x][2] == 'S')
                                {
                                    $cuentaCredito = $concepto[$x][0];
                                }
                            }
            
                            $sqlr="INSERT INTO comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque,valdebito, valcredito,estado,vigencia) 
                                VALUES ('$tipoComprobante $consecutivo', '$cuentaDebito', '$documento', '$row[1]', 'Pago del desincentivo de $row[2]', '', $consumo[$i], 0, '1', '$vigencia')";
                            mysqli_query($linkbd,$sqlr);
                    
                            $sqlr="INSERT INTO comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque,valdebito, valcredito,estado,vigencia) 
                                VALUES ('$tipoComprobante $consecutivo', '$cuentaCredito', '$documento', '$row[1]', 'Pago del desincentivo de $row[2]', '', 0, $consumo[$i], '1', '$vigencia')";
                            mysqli_query($linkbd,$sqlr);
                        }
            
                        $valorServicio = 0;
                        $valorServicio = $cargoFijo[$i] + $consumo[$i] + $contribucion[$i] + $acuerdoPago[$i] + $interesMoratorio[$i] + $saldoInicial[$i] + $desincentivo[$i];
                        $valorServicio = ceil($valorServicio / 100);
                        $valorServicio = $valorServicio * 100;  
                        $valorTotal += $valorServicio;
                        if ($valorTotal > $valorPago) $valorServicio = $valorPago - $valorSubtotal;
    
                        $sqlServicio = "SELECT cuenta, fuente, productoservicio, seccion_presupuestal FROM srvservicios WHERE id = $i";
                        $rowServicio = mysqli_fetch_row(mysqli_query($linkbd, $sqlServicio));
            
                        $sqlRecaudoPresupuesto = "INSERT INTO srv_recaudo_factura_ppto (codigo_recaudo, id_servicio, valor, vigencia, cuenta, fuente, producto_servicio, seccion_presupuestal, medio_pago, vigencia_gasto) VALUES ($consecutivo, $i, $valorServicio, '$vigencia', '$rowServicio[0]', '$rowServicio[1]', '$rowServicio[2]', '$rowServicio[3]', 'CSF', '1')";
                        mysqli_query($linkbd, $sqlRecaudoPresupuesto);
    
                        $valorSubtotal += $valorServicio;
                    }
            
                    //Diferencia encontrada en valor
                    $sqlSum = "SELECT SUM(valdebito) FROM comprobante_det WHERE id_comp = '$tipoComprobante $consecutivo' AND LEFT(cuenta,2) = '11'";
                    $rowSum = mysqli_fetch_row(mysqli_query($linkbd, $sqlSum));
            
                    $diferencia = $valorUnitario = $subTotal = $unit = 0;
                    $diferencia = $valorPago - $rowSum[0];
                    $diferencia = round($diferencia, 2);
            
                    $sqlServiciosAsignados = "SELECT id_servicio AS servicioId FROM srvasignacion_servicio WHERE id_clientes = $clienteId AND cargo_fijo = 'S' ORDER BY id_servicio";
                    $resServiciosAsignados = mysqli_query($linkbd, $sqlServiciosAsignados);
    
                    $i = 1;
            
                    while ($rowServiciosAsignados = mysqli_fetch_row($resServiciosAsignados)) {
                                
                        $sql = "SELECT cargo_fijo_u, cc, nombre, cuenta, fuente, productoservicio, seccion_presupuestal FROM srvservicios WHERE id = $rowServiciosAsignados[0]";
                        $res = mysqli_query($linkbd,$sql);
                        $row = mysqli_fetch_row($res);
                    
                        if ($i == 1) {
                            $concepto = concepto_cuentasn2($row[0],'SS',10,$row[1],$fecha);
                    
                            for ($x=0; $x < count($concepto); $x++) { 
                                if($concepto[$x][2] == 'S') {
                                    $cuentaCredito = $concepto[$x][0];
                                }
                            }   
    
                            if ($diferencia > 0) {
                                $sqlr="INSERT INTO comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque,valdebito, valcredito,estado,vigencia) 
                                VALUES ('$tipoComprobante $consecutivo', '$cuentaDebito', '$documento', '$row[1]', 'Ajuste al valor', '', $diferencia, 0, '1', '$vigencia')";
                                mysqli_query($linkbd,$sqlr);
                    
                                $sqlr="INSERT INTO comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque,valdebito, valcredito,estado,vigencia) 
                                    VALUES ('$tipoComprobante $consecutivo', '$cuentaCredito', '$documento', '$row[1]', 'Ajuste al valor', '', 0, $diferencia, '1', '$vigencia')";
                                mysqli_query($linkbd,$sqlr);
                            }
                            else {
                                $diferencia = abs($diferencia);
                
                                $sqlSearchDeb = "SELECT id_det, valdebito FROM comprobante_det WHERE id_comp = '$tipoComprobante $consecutivo' AND centrocosto = '$row[1]' ORDER BY valdebito DESC LIMIT 1";
                                $rowSearchDeb = mysqli_fetch_row(mysqli_query($linkbd, $sqlSearchDeb));
                
                                $sqlDeb = "UPDATE comprobante_det SET valdebito = (valdebito - $diferencia) WHERE id_det = $rowSearchDeb[0]";
                                mysqli_query($linkbd, $sqlDeb);
                
                                $sqlSearchCred = "SELECT id_det, valcredito FROM comprobante_det WHERE id_comp = '$tipoComprobante $consecutivo' AND centrocosto = '$row[1]' ORDER BY valcredito DESC LIMIT 1";
                                $rowSearchCred = mysqli_fetch_row(mysqli_query($linkbd, $sqlSearchCred));
                
                                $sqlCred = "UPDATE comprobante_det SET valcredito = (valcredito - $diferencia) WHERE id_det = $rowSearchCred[0]";
                                mysqli_query($linkbd, $sqlCred);
                            }
                        }
    
                        $i++;
                    }
            
                    $usuario = $_SESSION["usuario"];
                    $cedula = $_SESSION["cedulausu"];
                    $ip = getRealIP();
                    $hoy = date("Y-m-d H:i:s");
    
                    $sqlAuditoria = "INSERT INTO srv_auditoria (id_srv_funciones, accion, cedula, usuario, ip, fecha_hora, consecutivo) VALUES (4, 'Crear', '$cedula', '$usuario', '$ip', '$hoy', $consecutivo)";
                    mysqli_query($linkbd, $sqlAuditoria);
            
                    $out['insertaBien'] = true;
                    
                }
        
            }

        }
    }

    header("Content-type: application/json");
    echo json_encode($out);
    die();