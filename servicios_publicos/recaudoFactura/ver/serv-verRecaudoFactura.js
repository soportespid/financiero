const URL = 'servicios_publicos/recaudoFactura/ver/serv-verRecaudoFactura.php';

var app = new Vue({
    el:"#myapp",
    data() {
        return {
            intId:0,
            intDescuento:"",
            strCodRecaudo:"",
            strCodUsuario:"",
            strNumeroFactura:"",
            strFechaRecaudo:"",
            strConcepto:"",
            strDocumento:"",
            strNombre:"",
            strValorPago:"",
            strValorFactura:"",
            strMedioPago:"",
            strEstado:""
        }
    },
    async mounted() {
        await this.getData();
    },
    methods: {
        getData: async function(){
            this.intId = new URLSearchParams(window.location.search).get('id');
            if(this.intId>0){
                const formData = new FormData();
                formData.append("id",this.intId);
                formData.append("action","show");
                const response = await fetch(URL,{method:"POST",body:formData});
                const objData = await response.json();
                if(objData.status){
                    const data = objData.data;
                    this.intDescuento = data.descuento_intereses+"%";
                    this.strCodRecaudo = data.codigo_recaudo;
                    this.strConcepto = data.concepto;
                    this.strDocumento = data.documento;
                    this.strNombre = data.suscriptor;
                    this.strNumeroFactura = data.numero_factura;
                    this.strFechaRecaudo = data.fecha_recaudo;
                    this.strCodUsuario = data.cod_usuario;
                    this.strValorPago = this.formatNumero(data.valor_pago);
                    this.strValorFactura = this.formatNumero(data.valor_factura);
                    this.strMedioPago = data.medio_pago;
                    this.strEstado = data.estado;
                }
                console.log(objData);
            }
        },
        printPDF: function(){
            window.open("serv-recaudoFacturaPdf.php?id="+this.intId+"&codigo_recaudo="+this.strCodRecaudo+"&fecha="+this.strFechaRecaudo);
        },
        formatNumero: function(valor){
            return new Intl.NumberFormat("en-US", {style: "currency", currency: "USD"}).format(valor);
        },
    },
})