<?php
    require_once '../../../comun.inc';
    require '../../../funciones.inc';
    require '../../../funcionesSP.inc.php';

    session_start();

    if($_POST){
        $obj = new RecaudoFactura();
        if($_POST['action'] == "show"){
            $obj->getData(intval($_POST['id']));
        }
    }
    class RecaudoFactura{
        private $linkbd;
        private $intId;

        public function __construct(){
            $this->linkbd = conectar_v7();
            $this->linkbd->set_charset("utf8");
        }

        public function getData(int $id){
            $this->intId = $id;
            if($this->intId > 0){
                $request = mysqli_query($this->linkbd,"SELECT *, DATE_FORMAT(fecha_recaudo,'%d/%m/%Y') as fecha_recaudo FROM srv_recaudo_factura WHERE id = $this->intId")->fetch_assoc();
                if(!empty($request)){
                    $request['valor_factura'] = ceil (consultaValorFactura($request['numero_factura']) / 100) * 100;
                    $arrResponse = array("status"=>true,"data"=>$request);
                }else{
                    $arrResponse = array("status"=>false,"msg"=>"La factura no existe!");
                }
            }else{
                $arrResponse = array("status"=>false,"msg"=>"Error de datos");
            }
            echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            die();
        }
    }
?>