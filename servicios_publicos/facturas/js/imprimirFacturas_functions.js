const URL = 'servicios_publicos/facturas/controllers/imprimirFacturas_controller.php';

var app = new Vue({
    el:"#myapp",
    data() {
        return {
            isLoading: false,
            txtSearch: '',
            txtResultados: 0,
            modalPeriodo: false,
            periodos: [],
            periodos_copy: [],
            rutas: [],
            periodoId: 0,
            periodo: '',
            rutaId: 0,
            namePdf: '',

            data: [],
        }
    },
    mounted() {
        this.getData();
    },
    methods: {
        //trae datos del crear
        async getData() {
            const formData = new FormData();
            formData.append("action","get_data");
            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();
            if (objData.status) {
                this.periodos_copy = this.periodos = objData.periodos;
                this.txtResultados = this.periodos_copy.length;
                this.rutas = objData.rutas;
                this.namePdf = objData.namePdf;
            }
        },

        async searchInfo() {
            if (this.periodoId == 0) {
                Swal.fire("Atención!","Debe seleccionar un periodo facturado","warning");
                return 
            }

            const formData = new FormData();
            formData.append("action","search_facturas");
            formData.append("periodoId",this.periodoId);
            formData.append("rutaId",this.rutaId);
            this.isLoading = true;
            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();
            this.isLoading = false;
            this.data = objData.data;
        },

        searchData(option) {
            let search = "";

            switch (option) {
                case "modalPeriodo":
                    search = this.txtSearch.toLowerCase();
                    this.periodos_copy = [...this.periodos.filter(e=>e.numero_corte.toLowerCase().includes(search) || e.periodoName.toLowerCase().includes(search))];
                    this.txtResultados = this.periodos_copy.length;
                    break;

                    default:
                        console.log("Error, opción no encontrada");
                        break;

            }
        },

        selectItemModal(item, type) {
            switch (type) {
                case "periodoModel":
                    this.periodoId = item.numero_corte;
                    this.periodo = item.periodoName;
                    this.txtSearch = "";
                    this.periodos_copy = this.periodos;
                    this.modalPeriodo = false;
                    break;

                default:
                    console.log("Error tipo de selección no encontrada");
                    break;
            }
        },

        formatFecha: function(fecha) {
            // Separar la fecha en componentes
            const parts = fecha.split('-');
            if (parts.length !== 3) {
                throw new Error('Formato de fecha inválido. Use Y-m-d.');
            }
            const year = parts[0];
            const month = parts[1];
            const day = parts[2];
        
            // Retornar la fecha en el formato deseado
            return `${day}/${month}/${year}`;
        },

        pdf: function () {    
            const form = document.createElement("form");
            form.method = "post";
            form.target = "_blank";
            form.action = this.namePdf;       
            
            function addField(name,value){
                const input = document.createElement("input");
                input.type="hidden";
                input.name=name;
                input.value = value;
                form.appendChild(input);
            }
            const facturas = [];
            for (let index = 0; index < this.data.length; index++) {
                facturas.push(this.data[index].numeroFactura)
            }

            addField("facturas", JSON.stringify(facturas));
            addField("idCorte", this.periodoId);
            
            document.body.appendChild(form);
            form.submit();
            document.body.removeChild(form);
        },
    },
    computed:{

    }
})
