<?php
    require_once '../../../Librerias/core/Helpers.php';
    require_once '../models/imprimirFacturas_model.php';
    header('Content-Type: application/json');

    session_start();

    class imprimirController extends imprimirModel {
        public function getData() {
            if (!empty($_SESSION)) {
                $arrResponse = array("status" => true, "periodos" => $this->periodos(), "rutas" => $this->rutas(), "namePdf" => $this->nombrePdf());
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
        }

        public function search_facturas() {
            if (!empty($_SESSION)) {
                if ($_POST) {
                    $periodoId = $_POST["periodoId"];
                    $rutaId = $_POST["rutaId"];

                    $arrResponse = array("status" => true, "data" => $this->getFacturas($periodoId, $rutaId) );
                }
                
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
        }
    }

    if($_POST){
        $obj = new imprimirController();

        switch ($_POST["action"]) {
            case "get_data":
                $obj->getData();
                break;
            case "search_facturas":
                $obj->search_facturas();
                break;
            default:
                $arrResponse = ["status" => false, "msg" => "Acción no encontrada"];
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            break;
        }
    }
?>