<?php
    require_once '../../../Librerias/core/Helpers.php';
    require_once '../../../Librerias/core/Mysql.php';
    session_start();
    date_default_timezone_set("America/Bogota");
    
    class imprimirModel extends Mysql {
        public function periodos() {
            $sql = "SELECT numero_corte, 
            fecha_inicial, 
            fecha_final, 
            fecha_impresion, 
            fecha_limite_pago, 
            estado,
            UPPER(CONCAT(MONTHNAME(fecha_inicial), ' ', YEAR(fecha_inicial), ' - ', MONTHNAME(fecha_final), ' ', YEAR(fecha_inicial))) AS periodoName
            FROM srvcortes 
            ORDER BY numero_corte DESC";
            $data = $this->select_all($sql);
            return $data;
        }

        public function rutas() {
            $sql = "SELECT id, nombre FROM srvrutas WHERE estado = 'S' ORDER BY id";
            $data = $this->select_all($sql);
            return $data;
        }

        public function nombrePdf() {
            $sql = "SELECT nombre FROM srvarchivospdf WHERE id = 1";
            $pdf = $this->select($sql);
            return $pdf["nombre"];
        }

        public function getFacturas(int $periodoId, int $rutaId) {
            $filtroRuta = $rutaId != 0 ? "AND SC.id_ruta = $rutaId" : "";

            $sql = "SELECT
            SCD.numero_facturacion AS numeroFactura,
            SC.cod_usuario AS codigoUsuario
            FROM srvcortes_detalle AS SCD
            INNER JOIN srvclientes AS SC
            ON SCD.id_cliente = SC.id
            WHERE SCD.id_corte = $periodoId $filtroRuta
            ORDER BY SC.id_ruta ASC, SC.codigo_ruta ASC";
            $data = $this->select_all($sql);
            return $data;
        }
    }
?>