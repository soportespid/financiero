var app = new Vue({
    el: '#myapp',
	data:{

        error: '',
        loading: false,
        showModalLiquidaciones: false,
        periodo: '',
        descripcionPeriodo: '',
        datosPeriodosLiquidados: [],
        datosUsuarios: [],
    },

    mounted: function(){

      
    },

    methods: 
    {
        periodosLiquidados: async function() {

            this.loading = true;

            await axios.post('servicios_publicos/reporte_cartera/serv-reporteCarteraIntereses.php')
            .then((response) => {
                
                app.datosPeriodosLiquidados = response.data.periodosLiquidados;
            }).catch((error) => {
                this.error = true;
                console.log(error)
            }).finally(() => {
                this.loading =  false;
                this.showModalLiquidaciones = true;
            });     
        },

        seleccionaPeriodo: function(periodo) {

            this.periodo = periodo[0];
            this.descripcionPeriodo = periodo[1];
            this.showModalLiquidaciones = false;
        },

        buscarInformacionUsurios: async function() {

            if (this.periodo != '') {

                this.loading = true;

                await axios.post('servicios_publicos/reporte_cartera/serv-reporteCarteraIntereses.php?action=buscaCartera&periodo='+this.periodo)
                .then((response) => {
                    
                    this.datosUsuarios = response.data.datosUsuarios;
                    //console.log(response.data);
                    
                }).catch((error) => {

                    this.error = true;
                    console.log(error)

                }).finally(() => {
                    this.loading =  false;
                });     
            }
            else {
                Swal.fire("Faltan datos", "Selecciona un periodo liquidado", "warning");
            }
        },

        formatonumero: function(valor){
            return new Intl.NumberFormat("en-US", {style: "currency", currency: "USD"}).format(valor);
        },

        excel: function() {

            if (this.datosUsuarios != '') {

                document.form2.action="serv-excelCarteraIntereses.php";
				document.form2.target="_BLANK";
				document.form2.submit(); 
            }
            else {
                Swal.fire("Faltan datos", "Generar lecturas primero", "warning");
            }
        },
    }
});