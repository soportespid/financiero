<?php
    require_once '../../comun.inc';
    require '../../funciones.inc';
    require '../../funcionesSP.inc.php';
    session_start();

    $linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");

    $out = array('error' => false);

    $action = "show";

    if(isset($_GET['action'])){
        $action = $_GET['action'];
    }
    
    if ($action == "show") {
        $peridosLiquidados = array();

        $sql = "SET lc_time_names = 'es_ES'";
        mysqli_query($linkbd,$sql);

        $sqlCortes = "SELECT numero_corte, UPPER(MONTHNAME(fecha_inicial)), UPPER(MONTHNAME(fecha_final)), YEAR(fecha_inicial), YEAR(fecha_final), fecha_inicial, fecha_final FROM srvcortes WHERE numero_corte > 0 ORDER BY numero_corte DESC";
        $resCortes = mysqli_query($linkbd, $sqlCortes);
        while ($rowCortes = mysqli_fetch_row($resCortes)) {

            $datos = array();

            $descripcion = $rowCortes[1] . " " . $rowCortes[3] . " - " . $rowCortes[2] . " " . $rowCortes[4];
            
            array_push($datos, $rowCortes[0]);
            array_push($datos, $descripcion);
            array_push($datos, $rowCortes[5]);
            array_push($datos, $rowCortes[6]);
            array_push($peridosLiquidados, $datos);
        }

        $out['periodosLiquidados'] = $peridosLiquidados;
    }

    if ($action == "buscaCartera") {

        $sqlServicios = "SELECT id FROM srvservicios WHERE nombre = 'ACUEDUCTO' ";
        $resServicios = mysqli_query($linkbd, $sqlServicios);
        $rowServicios = mysqli_fetch_row($resServicios);

        $acueducto = $rowServicios[0];

        $sqlServicios = "SELECT id FROM srvservicios WHERE nombre = 'ALCANTARILLADO' ";
        $resServicios = mysqli_query($linkbd, $sqlServicios);
        $rowServicios = mysqli_fetch_row($resServicios);

        $alcantarillado = $rowServicios[0];

        $sqlServicios = "SELECT id FROM srvservicios WHERE nombre = 'ASEO' ";
        $resServicios = mysqli_query($linkbd, $sqlServicios);
        $rowServicios = mysqli_fetch_row($resServicios);

        $aseo = $rowServicios[0];


        $periodo = $_GET['periodo'];
        $datosUsuarios = array();

        $sqlUsuarios = "SELECT C.cod_usuario, T.cedulanit, T.nombre1, T.nombre2, T.apellido1, T.apellido2, T.razonsocial, E.descripcion, C.id, C.id_barrio FROM srvclientes AS C INNER JOIN terceros AS T ON C.id_tercero = T.id_tercero INNER JOIN srvestratos AS E ON C.id_estrato = E.id WHERE C.estado = 'S' ORDER BY C.id_ruta ASC, C.codigo_ruta ASC";
        $resUsuarios = mysqli_query($linkbd, $sqlUsuarios);
        while ($rowUsuarios = mysqli_fetch_row($resUsuarios)) {

            $valorAcueducto = 0;
            $valorAlcantarillado = 0;
            $valorAseo = 0;
            $valorTotal = 0;
            $diasMora = 0;
            $datos = array();
            $nombreSuscriptor = "";

            if ($rowUsuarios[6] == "") {

                $nombreSuscriptor = $rowUsuarios[2] . " " . $rowUsuarios[3] . " " . $rowUsuarios[4] . " " . $rowUsuarios[5];
            }
            else {
                $nombreSuscriptor = $rowUsuarios[6];
            }

            $nombreSuscriptor = Quitar_Espacios($nombreSuscriptor);

            $sqlDireccion = "SELECT nombre FROM srvbarrios WHERE id = $rowUsuarios[9]";
            $resDireccion = mysqli_query($linkbd, $sqlDireccion);
            $rowDireccion = mysqli_fetch_row($resDireccion);

            $direccion = $rowDireccion[0];

            $sqlValidaPago = "SELECT estado_pago, numero_facturacion FROM srvcortes_detalle WHERE id_cliente = $rowUsuarios[8] AND id_corte = $periodo";
            $resValidaPago = mysqli_query($linkbd, $sqlValidaPago);
            $rowValidaPago = mysqli_fetch_row($resValidaPago);

            if ($rowValidaPago[0] == 'S' || $rowValidaPago[0] == 'V') {

                $sqlDetallesFactura = "SELECT SUM(credito), SUM(debito), id_servicio FROM srvdetalles_facturacion WHERE numero_facturacion = $rowValidaPago[1] AND id_tipo_cobro <> 10 GROUP BY (id_servicio)";
                $resDetallesFactura = mysqli_query($linkbd, $sqlDetallesFactura);
                while ($rowDetallesFactura = mysqli_fetch_row($resDetallesFactura)) {
                    
                    switch ($rowDetallesFactura[2]) {
                        case $acueducto:
                            $valorAcueducto = $rowDetallesFactura[0] - $rowDetallesFactura[1];
                            break;

                        case $alcantarillado:
                            $valorAlcantarillado = $rowDetallesFactura[0] - $rowDetallesFactura[1];
                            break;

                        case $aseo:
                            $valorAseo = $rowDetallesFactura[0] - $rowDetallesFactura[1];
                            break;
                    }
                }
            }

            $interesAcueducto = 0;
            $interesAlcantarillado = 0;
            $interesAseo = 0;

            $sqlValidaPago = "SELECT estado_pago, numero_facturacion FROM srvcortes_detalle WHERE id_cliente = $rowUsuarios[8] AND id_corte = $periodo";
            $resValidaPago = mysqli_query($linkbd, $sqlValidaPago);
            $rowValidaPago = mysqli_fetch_row($resValidaPago);

            if ($rowValidaPago[0] == 'S' || $rowValidaPago[0] == 'V') {

                $sqlDetallesFactura = "SELECT SUM(credito), SUM(debito), id_servicio FROM srvdetalles_facturacion WHERE numero_facturacion = $rowValidaPago[1] AND id_tipo_cobro = 10 GROUP BY (id_servicio)";
                $resDetallesFactura = mysqli_query($linkbd, $sqlDetallesFactura);
                while ($rowDetallesFactura = mysqli_fetch_row($resDetallesFactura)) {
                    
                    switch ($rowDetallesFactura[2]) {
                        case $acueducto:
                            $interesAcueducto = $rowDetallesFactura[0] - $rowDetallesFactura[1];
                            break;

                        case $alcantarillado:
                            $interesAlcantarillado = $rowDetallesFactura[0] - $rowDetallesFactura[1];
                            break;

                        case $aseo:
                            $interesAseo = $rowDetallesFactura[0] - $rowDetallesFactura[1];
                            break;
                    }
                }
            }

            $sqlUltPago = "SELECT id FROM srvcortes_detalle WHERE id_cliente = $rowUsuarios[8] AND estado_pago = 'P' AND id_corte <= $periodo ORDER BY id DESC LIMIT 1";
            $resUltPago = mysqli_query($linkbd, $sqlUltPago);
            $rowUltPago = mysqli_fetch_row($resUltPago); 

            if ($rowUltPago[0] <> "") {

                $sqlMora = "SELECT estado_pago FROM srvcortes_detalle WHERE id_cliente = $rowUsuarios[8] AND estado_pago = 'V' AND id > $rowUltPago[0] AND id_corte <= $periodo";
                $resMora = mysqli_query($linkbd, $sqlMora);
                $mesesVencidos = mysqli_num_rows($resMora);
                $diasMora = 30 * $mesesVencidos;
            }
            else {
                $sqlMora = "SELECT estado_pago FROM srvcortes_detalle WHERE id_cliente = $rowUsuarios[8] AND estado_pago = 'V' AND id_corte <= $periodo";
                $resMora = mysqli_query($linkbd, $sqlMora);
                $mesesVencidos = mysqli_num_rows($resMora);
                $diasMora = 30 * $mesesVencidos;
            }
        
            $valorTotal = $valorAcueducto + $valorAseo + $valorAlcantarillado + $interesAcueducto + $interesAlcantarillado + $interesAseo; 
            $valorTotal = ceil ($valorTotal / 100) * 100;

            array_push($datos, $rowUsuarios[0]);
            array_push($datos, $rowUsuarios[1]);
            array_push($datos, $nombreSuscriptor);
            array_push($datos, $rowUsuarios[7]);
            array_push($datos, $valorAcueducto);
            array_push($datos, $interesAcueducto);
            array_push($datos, $valorAlcantarillado);
            array_push($datos, $interesAlcantarillado);
            array_push($datos, $valorAseo);
            array_push($datos, $interesAseo);
            array_push($datos, $diasMora);
            array_push($datos, $valorTotal);
            array_push($datos, $direccion);
            array_push($datosUsuarios, $datos);
        }

        

        $out['datosUsuarios'] = $datosUsuarios;
    }

    header("Content-type: application/json");
    echo json_encode($out);
    die();