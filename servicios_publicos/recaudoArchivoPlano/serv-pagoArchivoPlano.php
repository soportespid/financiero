<?php

	require_once '../../comun.inc';
    require '../../funciones.inc';
    require '../../funcionesSP.inc.php';
    header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
    session_start();

	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");

    $out = array('error' => false);

    $action = "show";

    if(isset($_GET['action'])){
        $action = $_GET['action'];
    }

    function insertServ($consecutivo, $clienteId, $codUsuario, $nombre, $documento, $numFactura, $fecha, $cuentaDebito, $valorPago, $usuario) {
        $linkbd = conectar_v7();
        $linkbd -> set_charset("utf8");

        $sqlRecaudoFactura = "INSERT INTO srv_recaudo_factura (codigo_recaudo, id_cliente, cod_usuario, suscriptor, documento, numero_factura, fecha_recaudo, medio_pago, numero_cuenta, concepto, valor_pago, usuario, estado, descuento_intereses) VALUES ($consecutivo, $clienteId, '$codUsuario', '$nombre', '$documento', $numFactura, '$fecha', 'banco', '$cuentaDebito', 'Pago completo de la factura $numFactura', $valorPago, '$usuario', 'ACTIVO', '0')";
        mysqli_query($linkbd, $sqlRecaudoFactura);

        $sqlUpdate = "UPDATE srvcortes_detalle SET estado_pago = 'P' WHERE numero_facturacion = $numFactura";
        mysqli_query($linkbd, $sqlUpdate);
    }

    function insertCont($numFactura, $clienteId, $cuentaDebito, $fecha, $documento, $vigencia, $valorPago, $consecutivo) {
        $linkbd = conectar_v7();
        $linkbd -> set_charset("utf8");

        $tipoComprobante = "30";

        $facturas = [];
        
        $sql = "SELECT numero_facturacion, estado_pago FROM srvcortes_detalle WHERE id_cliente = $clienteId AND numero_facturacion <= $numFactura ORDER BY numero_facturacion DESC";
        $res = mysqli_query($linkbd, $sql);
        while ($row = mysqli_fetch_row($res)) {

            if ($row[1] == "S" OR $row[1] == "V") {

                $facturas[] = $row[0];
            }
            else if ($row[1] == "P" OR $row[1] == "A" OR $row[1] == "A") {
                
                break;
            }
        }

        $cargoFijo = [];
        $consumo = [];
        $subCargoFijo = [];
        $subConsumo = [];
        $contribucion = [];
        $acuerdoPago = [];
        $saldoInicial = [];
        $interesMoratorio = [];

        foreach ($facturas as $key => $factura) {

            $sqlDet = "SELECT id_servicio, id_tipo_cobro, COALESCE(credito,0), COALESCE(debito,0), corte FROM srvdetalles_facturacion WHERE numero_facturacion = $factura AND (credito > 0 OR debito > 0) ORDER BY id_servicio ASC, id_tipo_cobro ASC";
            $resDet = mysqli_query($linkbd, $sqlDet);
            while ($rowDet = mysqli_fetch_row($resDet)) {
        
                switch ($rowDet[1]) {
                    case 1:
                        $cargoFijo[$rowDet[0]] += $rowDet[2];
                    break;
                    
                    case 2:
                        $consumo[$rowDet[0]] += $rowDet[2];
                    break;
        
                    case 3:
                        $subCargoFijo[$rowDet[0]] += $rowDet[3];
                    break;
        
                    case 4:
                        $subConsumo[$rowDet[0]] += $rowDet[3];
                    break;
        
                    case 5:
                        $contribucion[$rowDet[0]] += $rowDet[2];
                    break;
        
                    case 7:
                        $acuerdoPago[$rowDet[0]] += $rowDet[2];
                    break;

                    case 8:
                        //Saldos iniciales
                        if ($rowDet[4] == 1) {
                            $saldoInicial[$rowDet[0]] = (float) $rowDet[2];
                        }
                    break;
        
                    case 10;
                        if ($factura == $numFactura) {
                            $interesMoratorio[$rowDet[0]] = (float) $rowDet[2];
                        }    
                    break;
                }
            }
        }

        for ($i=1; $i <= count($cargoFijo); $i++) { 

            //Descuento de subsidios
            $cargoFijo[$i] = $cargoFijo[$i] - $subCargoFijo[$i];
            $consumo[$i] = $consumo[$i] - $subConsumo[$i];
        }

        $sqlComprobanteCabecera = "INSERT INTO comprobante_cab (numerotipo, tipo_comp, fecha, concepto, total_debito, total_credito, estado) VALUES ($consecutivo, '$tipoComprobante', '$fecha', 'Pago de la factura numero $factura', 0, 0, '1')";
        mysqli_query($linkbd, $sqlComprobanteCabecera);

        for ($i=1; $i <= count($cargoFijo); $i++) { 
    
            if ($cargoFijo[$i] > 0) {
                
                $sql = "SELECT cargo_fijo_u, cc, nombre FROM srvservicios WHERE id = $i";
                $res = mysqli_query($linkbd,$sql);
                $row = mysqli_fetch_row($res);
            
                $concepto = concepto_cuentasn2($row[0],'SS',10,$row[1],$fecha);
            
                for ($x=0; $x < count($concepto); $x++) 
                { 
                    if($concepto[$x][2] == 'S')
                    {
                        $cuentaCredito = $concepto[$x][0];
                    }
                }
        
                $sqlr="INSERT INTO comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque,valdebito, valcredito,estado,vigencia) 
                    VALUES ('$tipoComprobante $consecutivo', '$cuentaDebito', '$documento', '$row[1]', 'Pago del cargo fijo de $row[2]', '', $cargoFijo[$i], 0, '1', '$vigencia')";
                mysqli_query($linkbd,$sqlr);
        
                $sqlr="INSERT INTO comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque,valdebito, valcredito,estado,vigencia) 
                    VALUES ('$tipoComprobante $consecutivo', '$cuentaCredito', '$documento', '$row[1]', 'Pago del cargo fijo de $row[2]', '', 0, $cargoFijo[$i], '1', '$vigencia')";
                mysqli_query($linkbd,$sqlr);
            }

            if ($consumo[$i] > 0) {

                $sql = "SELECT consumo_u, cc, nombre FROM srvservicios WHERE id = $i ";
                $res = mysqli_query($linkbd,$sql);
                $row = mysqli_fetch_row($res);

                $concepto = concepto_cuentasn2($row[0],'CL',10,$row[1],$fecha);

                for ($x=0; $x < count($concepto); $x++) 
                { 
                    if($concepto[$x][2] == 'S')
                    {
                        $cuentaCredito = $concepto[$x][0];
                    }
                }

                $sqlr="INSERT INTO comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque,valdebito, valcredito,estado,vigencia) 
                    VALUES ('$tipoComprobante $consecutivo', '$cuentaDebito', '$documento', '$row[1]', 'Pago del consumo de $row[2]', '', $consumo[$i], 0, '1', '$vigencia')";
                mysqli_query($linkbd,$sqlr);
        
                $sqlr="INSERT INTO comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque,valdebito, valcredito,estado,vigencia) 
                    VALUES ('$tipoComprobante $consecutivo', '$cuentaCredito', '$documento', '$row[1]', 'Pago del consumo de $row[2]', '', 0, $consumo[$i], '1', '$vigencia')";
                mysqli_query($linkbd,$sqlr);
            }

            if ($contribucion[$i] > 0) {

                $sqlServicio = "SELECT contribucion_u, cc, nombre FROM srvservicios WHERE id = $i ";
                $resServicio = mysqli_query($linkbd,$sqlServicio);
                $rowServicio = mysqli_fetch_row($resServicio);

                $concepto = concepto_cuentasn2($rowServicio[0],'SC',10,$rowServicio[1],$fecha);

                for ($x=0; $x < count($concepto); $x++) 
                { 
                    if($concepto[$x][2] == 'S')
                    {
                        $cuentaCredito = $concepto[$x][0];
                    }
                }

                $sqlr="INSERT INTO comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque,valdebito, valcredito,estado,vigencia) 
                    VALUES ('$tipoComprobante $consecutivo', '$cuentaDebito', '$documento', '$row[1]', 'Pago de la contribuccion de $row[2]', '', $contribucion[$i], 0, '1', '$vigencia')";
                mysqli_query($linkbd,$sqlr);
        
                $sqlr="INSERT INTO comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque,valdebito, valcredito,estado,vigencia) 
                    VALUES ('$tipoComprobante $consecutivo', '$cuentaCredito', '$documento', '$row[1]', 'Pago de la contribuccion de $row[2]', '', 0, $contribucion[$i], '1', '$vigencia')";
                mysqli_query($linkbd,$sqlr);
            }

            if ($acuerdoPago[$i] > 0) {

                $concepto = concepto_cuentasn2('01','AP',10,'01',$fecha);
						
                for ($x=0; $x < count($concepto); $x++) 
                { 
                    if($concepto[$x][2] == 'S')
                    {
                        $cuentaCredito = $concepto[$x][0];
                    }
                }

                $sqlr="INSERT INTO comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque,valdebito, valcredito,estado,vigencia) 
                    VALUES ('$tipoComprobante $consecutivo', '$cuentaDebito', '$documento', '$row[1]', 'Pago de acuerdo de pago de $row[2]', '', $acuerdoPago[$i], 0, '1', '$vigencia')";
                mysqli_query($linkbd,$sqlr);
        
                $sqlr="INSERT INTO comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque,valdebito, valcredito,estado,vigencia) 
                    VALUES ('$tipoComprobante $consecutivo', '$cuentaCredito', '$documento', '$row[1]', 'Pago de acuerdo de pago de $row[2]', '', 0, $acuerdoPago[$i], '1', '$vigencia')";
                mysqli_query($linkbd,$sqlr);
            }

            if ($interesMoratorio[$i] > 0) {

                $sqlServicio = "SELECT interes_moratorio, cc, nombre FROM srvservicios WHERE id = $i ";
                $resServicio = mysqli_query($linkbd,$sqlServicio);
                $rowServicio = mysqli_fetch_row($resServicio);

                $concepto = concepto_cuentasn2($rowServicio[0],'SM',10,$rowServicio[1],$fecha);

                for ($x=0; $x < count($concepto); $x++) 
                { 
                    if($concepto[$x][3] == 'S' and $concepto[$x][2] == 'N') {
                        $cuentaCuatro = $concepto[$x][0];
                    }
                    if ($concepto[$x][3] == 'N' and $concepto[$x][2] == 'S') {
                        $cuentaCredito = $concepto[$x][0];
                    }
                }

                //Liquidación de intereses
                $sqlr="INSERT INTO comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque,valdebito, valcredito,estado,vigencia) 
                VALUES ('$tipoComprobante $consecutivo', '$cuentaCredito', '$documento', '$row[1]', 'Liquido interes moratorio de $row[2]', '', $interesMoratorio[$i], 0, '1', '$vigencia')";
                mysqli_query($linkbd,$sqlr);
        
                $sqlr="INSERT INTO comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque,valdebito, valcredito,estado,vigencia) 
                    VALUES ('$tipoComprobante $consecutivo', '$cuentaCuatro', '$documento', '$row[1]', 'Liquido interes moratorio de $row[2]', '', 0, $interesMoratorio[$i], '1', '$vigencia')";
                mysqli_query($linkbd,$sqlr);

                //Pago de intereses
                $sqlr="INSERT INTO comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque,valdebito, valcredito,estado,vigencia) 
                    VALUES ('$tipoComprobante $consecutivo', '$cuentaDebito', '$documento', '$row[1]', 'Pago de interes moratorio de $row[2]', '', $interesMoratorio[$i], 0, '1', '$vigencia')";
                mysqli_query($linkbd,$sqlr);
        
                $sqlr="INSERT INTO comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque,valdebito, valcredito,estado,vigencia) 
                    VALUES ('$tipoComprobante $consecutivo', '$cuentaCredito', '$documento', '$row[1]', 'Pago de interes moratorio de $row[2]', '', 0, $interesMoratorio[$i], '1', '$vigencia')";
                mysqli_query($linkbd,$sqlr);
            }

            if ($saldoInicial[$i] > 0) {

                $sql = "SELECT consumo_u, cc, nombre FROM srvservicios WHERE id = $i ";
                $res = mysqli_query($linkbd,$sql);
                $row = mysqli_fetch_row($res);

                $concepto = concepto_cuentasn2($row[0],'CL',10,$row[1],$fecha);

                for ($x=0; $x < count($concepto); $x++) 
                { 
                    if($concepto[$x][2] == 'S')
                    {
                        $cuentaCredito = $concepto[$x][0];
                    }
                }

                $sqlr="INSERT INTO comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque,valdebito, valcredito,estado,vigencia) 
                    VALUES ('$tipoComprobante $consecutivo', '$cuentaDebito', '$documento', '$row[1]', 'Pago del saldo inicial de $row[2]', '', $saldoInicial[$i], 0, '1', '$vigencia')";
                mysqli_query($linkbd,$sqlr);
        
                $sqlr="INSERT INTO comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque,valdebito, valcredito,estado,vigencia) 
                    VALUES ('$tipoComprobante $consecutivo', '$cuentaCredito', '$documento', '$row[1]', 'Pago del saldo inicial de $row[2]', '', 0, $saldoInicial[$i], '1', '$vigencia')";
                mysqli_query($linkbd,$sqlr);
            }
        }

        //Diferencia encontrada en valor
        $sqlSum = "SELECT SUM(valdebito) FROM comprobante_det WHERE id_comp = '$tipoComprobante $consecutivo' AND LEFT(cuenta,2) = '11'";
        $rowSum = mysqli_fetch_row(mysqli_query($linkbd, $sqlSum));

        $diferencia = $valorUnitario = $subTotal = $unit = 0;
        $diferencia = $valorPago - $rowSum[0];

        $sqlServiciosAsignados = "SELECT id_servicio FROM srvasignacion_servicio WHERE id_clientes = $clienteId AND cargo_fijo = 'S'";
        $resServiciosAsignados = mysqli_query($linkbd, $sqlServiciosAsignados);
        $cantServ = mysqli_num_rows($resServiciosAsignados);

        $valorUnitario = round($diferencia/$cantServ, 2);

        while ($rowServiciosAsignados = mysqli_fetch_row($resServiciosAsignados)) {
            
            $subTotal += $valorUnitario;

            $sql = "SELECT cargo_fijo_u, cc, nombre, cuenta, fuente, productoservicio, seccion_presupuestal FROM srvservicios WHERE id = $rowServiciosAsignados[0]";
            $res = mysqli_query($linkbd,$sql);
            $row = mysqli_fetch_row($res);
        
            $concepto = concepto_cuentasn2($row[0],'SS',10,$row[1],$fecha);
        
            for ($x=0; $x < count($concepto); $x++) 
            { 
                if($concepto[$x][2] == 'S')
                {
                    $cuentaCredito = $concepto[$x][0];
                }
            }   

            if ($diferencia > 0) {
                $sqlr="INSERT INTO comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque,valdebito, valcredito,estado,vigencia) 
                VALUES ('$tipoComprobante $consecutivo', '$cuentaDebito', '$documento', '$row[1]', 'Ajuste al valor', '', $valorUnitario, 0, '1', '$vigencia')";
                mysqli_query($linkbd,$sqlr);
    
                $sqlr="INSERT INTO comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque,valdebito, valcredito,estado,vigencia) 
                    VALUES ('$tipoComprobante $consecutivo', '$cuentaCredito', '$documento', '$row[1]', 'Ajuste al valor', '', 0, $valorUnitario, '1', '$vigencia')";
                mysqli_query($linkbd,$sqlr);
            }
            else{
                $valorUnitario = abs($valorUnitario);

                $sqlSearchDeb = "SELECT id_det, valdebito FROM comprobante_det WHERE id_comp = '$tipoComprobante $consecutivo' AND centrocosto = '$row[1]' ORDER BY valdebito DESC LIMIT 1";
                $rowSearchDeb = mysqli_fetch_row(mysqli_query($linkbd, $sqlSearchDeb));

                $sqlDeb = "UPDATE comprobante_det SET valdebito = (valdebito - $valorUnitario) WHERE id_det = $rowSearchDeb[0]";
                mysqli_query($linkbd, $sqlDeb);

                $sqlSearchCred = "SELECT id_det, valcredito FROM comprobante_det WHERE id_comp = '$tipoComprobante $consecutivo' AND centrocosto = '$row[1]' ORDER BY valcredito DESC LIMIT 1";
                $rowSearchCred = mysqli_fetch_row(mysqli_query($linkbd, $sqlSearchCred));

                $sqlCred = "UPDATE comprobante_det SET valcredito = (valcredito - $valorUnitario) WHERE id_det = $rowSearchCred[0]";
                mysqli_query($linkbd, $sqlCred);
            }
        }
    }

    if ($action == "datosIniciales") {

        $sqlCodBarras = "SELECT codigo FROM codigosbarras WHERE tipo = '03' AND estado = 'S'";
        $rowCodBarras = mysqli_fetch_assoc(mysqli_query($linkbd, $sqlCodBarras));

        $out['codBarras'] = $rowCodBarras['codigo'];
    }

    if ($action == "buscaDatos") {
        $datos = [];
        $pagos = $_POST["pagos"];

        foreach ($pagos as $i => $pago) {
            $temp = [];
            
            $consecutivo = $pago[0];
            $numFactura = (int) $pago[1];
            $valorPago = (double) $pago[2];

            $sqlCortesDet = "SELECT id_cliente AS clienteId, estado_pago FROM srvcortes_detalle WHERE numero_facturacion = $numFactura";
            $rowCortesDet = mysqli_fetch_assoc(mysqli_query($linkbd, $sqlCortesDet));

            $sqlCliente = "SELECT cod_usuario AS codUsuario, id_tercero AS terceroId FROM srvclientes WHERE id = $rowCortesDet[clienteId]";
            $rowCliente = mysqli_fetch_assoc(mysqli_query($linkbd, $sqlCliente));

            $name = buscaNombreTerceroConId($rowCliente['terceroId']);
            $document = buscaDocumentoTerceroConId($rowCliente['terceroId']);

            $temp[] = $consecutivo;
            $temp[] = $numFactura;
            $temp[] = $rowCortesDet['clienteId'];
            $temp[] = $document;
            $temp[] = $rowCliente['codUsuario'];
            $temp[] = $name;
            $temp[] = $valorPago;
            $temp[] = $rowCortesDet['estado_pago'];
            array_push($datos, $temp);
        }

        $out['datos'] = $datos;
    }

    if ($action == "guardar") {
     
        $fechaPago = $fechaArchivo = $vigencia = $numeroBanco = $codigoBarras = $cuentaDebito = "";
        $cantPagos = $valorTotalPagos = 0;
        $datosPagos = [];

        $fechaPago = $_POST['fechaPago'];
        $fechaArchivo = $_POST['fechaArchivo'];
        $vigencia = $_POST['vigencia'];
        $numeroBanco = $_POST['numeroBanco'];
        $codigoBarras = $_POST['codigoBarras'];
        $cantPagos = $_POST['cantPagos'];
        $valorTotalPagos = $_POST['valorTotalPagos'];
        $datosPagos = $_POST['datos'];
        $sqlBanco = "SELECT cuenta FROM tesobancosctas WHERE ncuentaban = '$numeroBanco'";
        $rowBanco = mysqli_fetch_row(mysqli_query($linkbd, $sqlBanco));
        $cuentaDebito = $rowBanco[0];
        $usuario = $_SESSION['usuario'];

        $sqlr = "SELECT count(*) From dominios dom  where dom.nombre_dominio = 'PERMISO_MODIFICA_DOC'  and dom.valor_final <= '$fechaPago'  AND dom.valor_inicial =  '$_SESSION[cedulausu]' ";
        $res = mysqli_query($linkbd,$sqlr);
        $row = mysqli_fetch_row($res);

        if ($row[0] > 0) {
            foreach ($datosPagos as $i => $pago) {
                if ($pago[7] == "S") {
                    $consecutivo = selconsecutivo('srv_recaudo_factura', 'codigo_recaudo');
                    insertServ($consecutivo, $pago[2], $pago[4], $pago[5], $pago[3], $pago[1], $fechaPago, $cuentaDebito, $pago[6], $usuario);
                    // insertCont($pago[1], $pago[2], $cuentaDebito, $fechaPago, $pago[3], $vigencia, $pago[6], $consecutivo);
                }
            }

            $out['insertaBien'] = true;
        }
        else {
            $out["fecha"] = true;
        }
    }

    header("Content-type: application/json");
    echo json_encode($out);
    die();