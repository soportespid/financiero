const URL = 'servicios_publicos/recaudoArchivoPlano/serv-pagoArchivoPlano.php';
import { filtroEnArrayDeObjetos } from './../../funciones.js';

var app = new Vue({
    el: '#myapp',
	data:{

        error: '',
        loading: false,
        detalle: [],
        content: [],
        fechaRecaudo: '',
        fechaArchivo: '',
        numeroBanco: '',
        codigoBarras: '',
        codBarrasBase: '',
        pagos: [],
        cantPagos: 0,
        valorTotalPagos: 0,
        datos: [],
        vigencia: '',
    },

    mounted: function(){

        this.datosIniciales();
    },

    methods: 
    {
        //Desglosar array
        toFormData: function(obj){
            var form_data = new FormData();
            for(var key in obj){
                form_data.append(key, obj[key]);
            }
            return form_data;
        },

        formatonumero: function(valor){
            return new Intl.NumberFormat("en-US", {style: "currency", currency: "USD"}).format(valor);
        },

        datosIniciales: async function() {
            
            await axios.post(URL+'?action=datosIniciales')
            .then((response) => {
              
                this.codBarrasBase = response.data.codBarras;
            });
        },

        readFile: async function() {
            this.file = this.$refs.doc.files[0];

            const reader = new FileReader();

            if (this.file.name.includes(".txt")) {
                reader.onload = (res) => {
                    this.content = res.target.result;
                };
                reader.onerror = (err) => console.log(err);
                reader.readAsText(this.file);
            } 
            else {
                this.content = "check the console for file output";
                reader.onload = (res) => {
                    console.log(res.target.result);
                };

                reader.onerror = (err) => console.log(err);
                reader.readAsText(this.file);
            }
        },

        mostrar() {
            this.datos = [];
            this.pagos = [];
            
            const archivo = this.content.split(/\r\n|\r|\n/);
            
            for (let i = 0; i < archivo.length; i++) {
                
                var tipo = archivo[i].substring(0, 2);

                switch (tipo) {
                    case "01":
                        var vig, mes, dia = "";
                        vig = archivo[i].substring(12, 16);
                        mes = archivo[i].substring(16, 18);
                        dia = archivo[i].substring(18, 20);
                        this.fechaRecaudo = vig+"-"+mes+"-"+dia;
                        this.vigencia = vig;
                        vig = archivo[i].substring(40, 44);
                        mes = archivo[i].substring(44, 46);
                        dia = archivo[i].substring(46, 48);
                        this.fechaArchivo = vig+"-"+mes+"-"+dia;
                        this.numeroBanco = archivo[i].substring(29, 40);
                    break;
                
                    case "05":
                        this.codigoBarras = archivo[i].substring(2, 15);
                    break;

                    case "06":
                        var detalles = [];
                        var consecutivo = archivo[i].substring(87, 94);
                        var numfactura = archivo[i].substring(34, 50);
                        var valorpago = archivo[i].substring(50, 62);

                        detalles.push(consecutivo);
                        detalles.push(numfactura);
                        detalles.push(valorpago);

                        this.pagos.push(detalles);
                    break;

                    case "08":
                        
                    break;

                    case "09":
                        this.cantPagos = archivo[i].substring(2, 11);
                        this.valorTotalPagos = archivo[i].substring(11, 29);
                    break;
                }
            }

            if (this.codBarrasBase == this.codigoBarras) {

                var formData = new FormData();

                for(let i=0; i <= this.pagos.length-1; i++) {
                
                    const val = Object.values(this.pagos[i]).length-1;

                    for(let x = 0; x <= val; x++) {
                        formData.append("pagos["+i+"][]", Object.values(this.pagos[i])[x]);
                    }
                }

                axios.post(URL+'?action=buscaDatos', formData)
                .then((response) => {
                    this.datos = response.data.datos;
                });            
            }
            else {
                Swal.fire({
                    icon: 'warning',
                    title: 'Problema en codigo de barras',
                    text: 'El codigo de barras no es el correcto, revisar el archivo!'
                })

                this.fechaRecaudo = "";
                this.fechaArchivo = "";
                this.numeroBanco = "";
                this.codigoBarras = "";
                this.pagos = [];
                this.cantPagos = 0;
                this.valorTotalPagos = 0;
            }
        },

        guardar() {

            if (this.datos.length > 0) {
                Swal.fire({
                    icon: 'question',
                    title: 'Esta seguro que quiere guardar?',
                    showDenyButton: true,
                    confirmButtonText: 'Guardar!',
                    denyButtonText: 'Cancelar',
                }).then((result) => {
    
                    if (result.isConfirmed) {
    
                        var formData = new FormData();
    
                        formData.append("fechaPago", this.fechaRecaudo);
                        formData.append("fechaArchivo", this.fechaArchivo);
                        formData.append("vigencia", this.vigencia);
                        formData.append("numeroBanco", this.numeroBanco);
                        formData.append("codigoBarras", this.codigoBarras);
                        formData.append("cantPagos", this.cantPagos);
                        formData.append("valorTotalPagos", this.valorTotalPagos);
    
                        for(let i=0; i <= this.datos.length-1; i++) {
    
                            const val = Object.values(this.datos[i]).length-1;
    
                            for(let x = 0; x <= val; x++) {
                                formData.append("datos["+i+"][]", Object.values(this.datos[i])[x]);
                            }
                        }
    
                        axios.post(URL + '?action=guardar', formData)
                        .then((response) => {
                            console.log(response.data); 
                            if (response.data.fecha == true) {
                                Swal.fire(
                                    'Error!',
                                    'Fecha de recaudo bloqueado',
                                    'error'
                                );
                            }
                            else {
                                if (response.data.insertaBien == true) {
                                    Swal.fire({
                                        icon: 'success',
                                        title: 'Recaudo de factura guardado con exito ',
                                        showConfirmButton: false,
                                        timer: 3500
                                    }).finally(() => {
                                        location.reload();
                                    });
                                }
                                else {
                                    Swal.fire(
                                        'Error!',
                                        'No se pudo guardar3.',
                                        'error'
                                    );
                                }
                            }
                        });
                    } 
                })
            }
            else {
                Swal.fire(
                    'Error!',
                    'Ingrese un archivo plano',
                    'error'
                );
            }
        }
    }
});