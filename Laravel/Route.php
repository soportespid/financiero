<?php

namespace Laravel;

class Route
{
    public static function currentDomain(): string
    {
        return explode('.', $_SERVER['HTTP_HOST'])[0];
    }

    private static function query_params($data) : string
    {
        $parameters = '';

        foreach($data as $key => $value) {
            $parameters .= '&' . $key . '=' . urlencode($value);
        }

        return substr_replace($parameters, '?', 0, 1);
    }

    public static function get(string $path, array $data): string
    {
        if (strstr($_SERVER['HTTP_HOST'], 'localhost') !== false) {
            $protocol = 'http://';
            $ams_host = '.localhost:8000/';
        } else {
            $ams_host = '.ams.ideal-10.com/';
            $protocol = 'https://';
        }

        return $protocol . static::currentDomain() . $ams_host . $path . static::query_params($data);
    }

    public static function iframe(string $path, array $data = [], bool $r = false): ?string
    {
        $iframe = '<iframe src="' . static::get($path, $data) . '" loading="eager" id="laravel-iframe" class="laravel-iframe"></iframe>';

        if ($r) {
            return $iframe;
        }

        print($iframe);
    }

    public static function header(): void
    {
        $domain = static::currentDomain();

        if (strstr($_SERVER['HTTP_HOST'], 'localhost') !== false) {
            header('Access-Control-Allow-Origin: http://' . $domain . '.localhost:8000');
            header('Content-Security-Policy: default-src \'self\' \'unsafe-inline\' \'unsafe-eval\' http://*.localhost:8000');
        } else {
            header('Access-Control-Allow-Origin: https://' . $domain . '.ams.ideal-10.com');
            header('Content-Security-Policy: default-src \'self\' \'unsafe-inline\' \'unsafe-eval\' https://*.ideal-10.com https://*.ams.ideal-10.com');
        }

        header('Vary: Origin');
    }

    public static function create(string $path, string $modulo, string $titulo): void
    {
        header('Cache-control: no-cache, no-store, must-revalidate');
        header('Content-Type: text/html;charset=utf8');
        static::header();

        require 'comun.inc';
        require 'funciones.inc';

        $linkbd = conectar_v7();
        $linkbd->set_charset('utf8');

        session_start();
        date_default_timezone_set('America/Bogota');

        $username = $_SESSION['nickusu'];
        $result = mysqli_query($linkbd, 'SELECT pass_usu FROM usuarios WHERE usu_usu = \'' . $username . '\';');
        $password = mysqli_fetch_object($result)->pass_usu;

        $date = date('d_m_Y_h_i_s');

        $iframe = static::iframe($path, ['username' => $username, 'password' => $password], true);

        echo <<<EOF
<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8"/>
        <meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1"/>
        <meta name="viewport" content="user-scalable=no">
        <title>{$titulo}</title>
        <link href="favicon.ico" rel="shortcut icon"/>
        <link href="css/css2.css?{$date}" rel="stylesheet" type="text/css" />
        <link href="css/css3.css?{$date}" rel="stylesheet" type="text/css" />
        <script src="css/programas.js?{$date}"></script>
        <script src="https://code.jquery.com/jquery-3.7.1.min.js" integrity="sha256-/JqT3SQfawRcv/BIHPThkBvs0OEvtFFmqPF/lYI/Cxo=" crossorigin="anonymous"></script>

        <style>
            html, body {
                width: 100%;
                height: 100%;
                margin: 0;
            }

            body {
                display: flex;
                flex-direction: column;
            }

            .laravel-iframe {
                flex-grow: 1;
                border: none;
            }
        </style>
    </head>
    <body>
        <span class="cinta" style="display: none;"></span>
        <iframe src="alertas.php" name="alertas" id="alertas" style="display:none"></iframe>
        <table>
            <tr><script>barra_imagenes("$modulo");</script>
EOF;
        cuadro_titulos();
        echo <<<EOF
</tr>
            <tr>
EOF;
        menu_desplegable($modulo);
        echo <<<EOF
</tr>
        </table>

        <div id="loading-container" class="loading-container">
            <p class="text-loading" data-loading="IDEAL 10">IDEAL 10</p>
        </div>

        {$iframe}

        <script>
            $(document).ready(() => {
                $('#loading-container').show();
                $('#laravel-iframe').hide();

                $('#laravel-iframe').ready(() => {
                    $('#loading-container').hide();
                    $('#laravel-iframe').show();
                });

                window.addEventListener('message', (e) => {
                    switch (e.data?.action) {
                        case 'newWindow':
                            window.open('{$modulo}-principal', '_blank').focus();
                            break;
                        case 'duplicateWindow':
                            window.open(window.location, '_blank').focus();
                            break;
                        case 'open':
                            window.open(e.data.url, e.data.target ?? '_blank').focus();
                            break;
                        default:
                            break;
                    }
                });
            });
        </script>
    </body>
</html>
EOF;

        exit;
    }
}
