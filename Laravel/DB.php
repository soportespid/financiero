<?php

namespace Laravel;

use \mysqli;
use \mysqli_result;

class DB
{
    public static function getCredentials(): array
    {
        if (strstr($_SERVER['HTTP_HOST'], 'localhost') !== false) {
            return [
                'hostname' => 'localhost',
                'username' => 'ams',
                'password' => 'localamssecret',
                'database' => 'ams'
            ];
        } else {
            return [
                'hostname' => '147.93.44.25',
                'username' => 'financiero',
                'password' => base64_decode('U29wb3J0ZUlkZWFsMTAq'),
                'database' => 'ams'
            ];
        }
    }

    public static function connectCentral(): mysqli
    {
        $credentials = static::getCredentials();

        $central = new mysqli(
            $credentials['hostname'],
            $credentials['username'],
            $credentials['password']
        );

        if ($central->connect_errno) {
            throw new \Exception('No se pudo conectar a la base de datos central, error: ' . $central->connect_error .'. usando credenciales: ' . print_r($credentials, true));
        }

        $central->select_db($credentials['database']);

        return $central;
    }

    public static function connect(string $tenant_id): mysqli
    {
        $central = static::connectCentral();

        $stmt = $central->prepare('SELECT JSON_VALUE(`data`, \'$.tenancy_db_name\'), JSON_VALUE(`data`, \'$.tenancy_db_username\'), JSON_VALUE(`data`, \'$.tenancy_db_password\') FROM `tenants` WHERE `id` = ? LIMIT 1');
        $stmt->bind_param('s', $tenant_id);
        $stmt->execute();
        $result = $stmt->get_result();

        if ((! $result instanceof mysqli_result) || $result->num_rows === 0) {
            throw new \Exception('No se pudo obtener el nombre de la base de datos del tenant, error: ' . $central->error);
        }

        $tenancy_db_name = $result->fetch_array(MYSQLI_NUM)[0];
        $tenancy_db_username = $result->fetch_array(MYSQLI_NUM)[1];
        $tenancy_db_password = $result->fetch_array(MYSQLI_NUM)[2];

        $central->close();

        $credentials = static::getCredentials();

        $tenant = new mysqli(
            $credentials['hostname'],
            $tenancy_db_username ?? $credentials['username'],
            $tenancy_db_password ?? $credentials['password']
        );

        if ($tenant->connect_errno) {
            throw new \Exception('No se pudo conectar a la base de datos del tenant, error: ' . $tenant->connect_error . '. usando credenciales: ' . print_r($credentials, true));
        }

        $tenant->select_db($tenancy_db_name);

        return $tenant;
    }

    public static function connectWithDomain(string $domain): mysqli
    {

        $central = static::connectCentral();

        $stmt = $central->prepare('SELECT `tenant_id` FROM `domains` WHERE `domain` = ? LIMIT 1');
        $stmt->bind_param('s', $domain);
        $stmt->execute();
        $result = $stmt->get_result();

        if ((! $result instanceof mysqli_result) || $result->num_rows == 0) {
            throw new \Exception('No se pudo obtener el ID del tenant, error: ' . $central->error . '. usando dominio: ' . $domain);
        }

        $tenant_id = $result->fetch_array(MYSQLI_NUM)[0];

        $central->close();

        return static::connect($tenant_id);
    }
}
