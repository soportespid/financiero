<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require 'comun.inc';
	require 'funciones.inc';
	$linkbd = conectar_v7();
    $linkbd -> set_charset("utf8"); 
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es">
	<head>
		<meta http-equiv="Content-Type" content="text/html" charset="utf8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=9"/>
		<title>:: Lados</title>
		<link href="css/css2.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
        <link href="css/cssSP.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<script type="text/javascript" src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script>
			function agregar(codigo, nombre)
			{
				parent.document.form2.nombreLado.value = nombre;
				parent.document.form2.id_lado.value = codigo;
				parent.despliegamodal("hidden");
			}
		</script> 
		<?php titlepag();?>
	</head>
	<body>
		<form name="form2" method="post">
			<table class="inicio ancho" style="width:99.5%">
				<tr>
					<td class="titulos" colspan="3">Lados - Servicios Publicos</td>
					<td class="cerrar" style="width:7%" onClick="parent.despliegamodal('hidden');">Cerrar</td>
				</tr>
				
				<tr>
					<td class="tamano01" style='width:3cm;'>Codigo o Nombre:</td>
					<td>
						<input type="search" name="codnom" id="codnom" value="<?php echo @$_POST['codnom'];?>" style='width:100%;'/>
					</td>

					<td style="padding-bottom:0px;height:35px;"><em class="botonflecha" onClick="document.form2.submit();">Buscar</em></td>
				</tr>
			</table>

			<div class="subpantalla" style="height:82%; width:99.2%; overflow-x:hidden;">
				<table class='inicio' align='center' width='99%'>
					<tr>
						<td colspan='4' class='titulos'>Resultados Busqueda: </td>
					</tr>
					
					<tr>
						<td colspan='4'>Estratos Encontrados: <?php echo $encontrados; ?></td>
					</tr>

					<tr class='titulos2' style='text-align:center;'>
						<td style="width: 10%;">Código</td>
						<td>Nombre Zona</td>
					</tr>

					<?php
						$crit1="";
						if (@$_POST['codnom']!=""){$crit1 = "AND CONCAT_WS(' ', id, nombre) LIKE '%$_POST[codnom]%'";}

						$sql = "SELECT id, nombre FROM srvlados WHERE estado = 'S' $crit1";
						$res = mysqli_query($linkbd,$sql);
						$encontrados = mysqli_num_rows($res);

						$iter  = 'saludo1a';
						$iter2 = 'saludo2';
						$conta = 1;

						while ($row = mysqli_fetch_row($res))
						{
					?>
							<tr class='<?php echo $iter ?>' style='text-align:center; text-transform:uppercase;' onclick="agregar('<?php echo $row[0]; ?>', '<?php echo $row[1]; ?>')">
								<td><?php echo $row[0] ?></td>
								<td><?php echo $row[1] ?></td>
							</tr>
					<?php
							$aux=$iter;
							$iter=$iter2;
							$iter2=$aux;
							$conta++;
						}

						if ($encontrados == 0)
						{
					?>
							<table class='inicio'>
								<tr>
									<td class='saludo1' style='text-align:center;'>
										<img src='imagenes\alert.png' style='width:25px; height: 30px;'> No se han encontrado registros <img src='imagenes\alert.png' style='width:25px; height: 30px;'>
									</td>
								</tr>
							</table>
					<?php
						}
					?>
				</table>
			</div>
		</form>
	</body>
</html>
