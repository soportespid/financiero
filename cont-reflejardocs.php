<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");

	require "comun.inc";
	require "funciones.inc";
	require "conversor.php";
	require "validaciones.inc";

	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");

	session_start();
    date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html lang="es">
	<head>
		<meta charset="utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1"/>
		<meta name="viewport" content="user-scalable=no">
		<title>:: IDEAL 10 - Contabilidad</title>
		<link href="favicon.ico" rel="shortcut icon"/>
		<link href="css/css2.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<script type="text/javascript" src="css/programas.js"></script>
	</head>
	<body>
		<IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
		<span id="todastablas2"></span>
		<table>
			<tr><script>barra_imagenes("cont");</script><?php cuadro_titulos();?></tr>	 
			<tr><?php menu_desplegable("cont");?></tr>
			<tr>
				<td colspan="3" class="cinta">
					<a class="mgbt"><img src="imagenes/add2.png" /></a>
					<a class="mgbt"><img src="imagenes/guardad.png" style="width:24px;"/></a>
					<a class="mgbt"><img src="imagenes/buscad.png"/></a>
					<a onClick="mypop=window.open('plan-agenda.php','','');mypop.focus()" class="mgbt"><img src="imagenes/agenda1.png" title="Agenda" /></a>
					<a href="#" onClick="mypop=window.open('cont-principal.php','','');mypop.focus();" class="mgbt"><img src="imagenes/nv.png" title="Nueva ventana"></a>
                    <a onClick="mypop=window.open('<?php echo $url2; ?>','','');mypop.focus();" class="tooltip bottom mgbt"><img src="imagenes/duplicar_pantalla.png" title="Duplicar pesta&ntilde;a">
				</td>
			</tr>
		</table>
		<form name="form2" method="post" action="">
			<div style="overflow-y: scroll; height: 78%">
				<table class="inicio">
					<tr>
						<td class="titulos" colspan="2">.: Reflejar Documentos Contabilidad</td>
						<td class="cerrar" style="width:7%;" ><a href="cont-principal.php">&nbsp;Cerrar</a></td>
					</tr>
					<tr><td class="titulos2" colspan="3">Reflejar Contabilidad</td></tr>
					<tr>
						<td class='saludo1' width='70%'>
							<ol id="lista2">
								<table>
									<tr>
										<td>
											<li><a href='cont-recibocaja-reflejar.php'>Recibos de Caja</a></li>
											<li><a href='cont-sinrecibocaja-refleja.php'>Ingresos Internos</a></li>
											<!--<li><a href='cont-pagonominaver-reflejar.php'>Pagar N&oacute;mina</a></li> -->
											<!-- <li><a href='cont-girarcheques-reflejar.php'>Egresos</a></li> -->
											<li><a href='cont-pagotercerosvigant-reflejar.php'>Otros Egresos</a></li>
											<li><a href='cont-recaudos-reflejar.php'>Otros Recaudos</a></li> 
											<!-- <li><a href='cont-egreso-reflejar.php'>Liquidaci&oacute;n Cuentas por Pagar</a></li>  -->
											<li><a href='cont-exentos-reflejar.php'>Predial Exentos</a></li>
											<li><a href='cont-exoneracion-reflejar.php'>Predial Exoneraciones</a></li>
											<li><a href='cont-prescripcion-reflejar.php'>Prescripci&oacute;n Predial</a></li>
										</td>
										<td>
											<li><a href='cont-sinrecaudos-reflejar.php'>Liquidaci&oacute;n Ingresos Internos</a></li>
											<li><a href='cont-industriaver-reflejar.php'>Liquidaci&oacute;n Industria y Comercio</a></li> 
											<li><a href='cont-pagoterceros-reflejar.php'>Pago Recaudo Terceros</a></li>
											<li><a href='cont-recaudotransferencia-reflejar.php'>Recaudo Transferencia</a></li>
											<li><a href='cont-recaudotransferencialiquidar-reflejar.php'>Liquidar Recaudo Transferencia</a></li>
											<li><a href='cont-sinrecaudos-reflejarsp.php'>Liquidaci&oacute;n Ingresos Internos SP</a></li>
											<li><a href='cont-sinrecibocaja-reflejasp.php'>Ingresos Internos SP</a></li>
										</td>
										<td>
											<li><a href='cont-salidadirecta-reflejar.php'>Salida directa de almacen </a></li>
											<li><a href='cont-salidareserva-reflejar.php'>Salida por reserva de almacen </a></li>
											<li><a href='cont-entradacompra-reflejar.php'>Entradas por compra</a></li>
											<li><a href='cont-entradaDonacion-reflejar.php'>Entradas por donacion</a></li>
											<li><a href='cont-abono-reflejar.php'>Abonos de predial </a></li>
											<li><a href='cont-ordenactivacion-reflejar.php'>Orden de Activaci&oacute;n </a></li>
											<li><a href='cont-recaudotransferenciasgr-reflejar.php'>Recaudo transferencia SGR </a></li>
										</td>
									</tr>
								</table>
							</ol>
						</td>
						<td colspan="2" rowspan="1" style="background:url(imagenes/reflejar.png); background-repeat:no-repeat; background-position:center; background-size: 100% 100%"></td>
					</tr>
				</table>
			</div>
		</form>
	</body>
</html>
