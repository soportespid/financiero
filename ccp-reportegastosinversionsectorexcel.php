<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require_once 'PHPExcel/Classes/PHPExcel.php';
	require"comun.inc";
	require"funciones.inc";
	session_start();
	$listantr=unserialize($_POST['lista_ntr']);
	$listasector=unserialize($_POST['lista_sector']);
	$listaproyecto=unserialize($_POST['lista_proyecto']);
	$listafuente=unserialize($_POST['lista_fuente']);
	$listarubro=unserialize($_POST['lista_rubro']);
	$listacsf=unserialize($_POST['lista_csf']);
	$listassf=unserialize($_POST['lista_ssf']);
	$listasumcsf=unserialize($_POST['lista_sumcsf']);
	$listasumssf=unserialize($_POST['lista_sumssf']);
	$linkbd=conectar_v7();
	$objPHPExcel = new PHPExcel();
	//----Propiedades----
	$objPHPExcel->getProperties()
		->setCreator("IDEAL10")
		->setLastModifiedBy("IDEAL10")
		->setTitle("Reporte")
		->setSubject("Presupuesto")
		->setDescription("Presupuesto")
		->setKeywords("Presupuesto")
		->setCategory("Presupuesto");
	//----Cuerpo de Documento----
	$objPHPExcel->getActiveSheet()->mergeCells('A1:I1');
	$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A1', 'REPORTE GESTION DE PROYECTOS POR SECTOR');
	$objFont=$objPHPExcel->getActiveSheet()->getStyle('A1')->getFont();
	$objFont->setName('Courier New');
	$objFont->setSize(15);
	$objFont->setBold(true);
	$objFont->setUnderline(PHPExcel_Style_Font::UNDERLINE_SINGLE);
	$objFont->getColor()->setARGB( PHPExcel_Style_Color::COLOR_BLACK);
	$objAlign=$objPHPExcel->getActiveSheet()->getStyle('A1')->getAlignment(); 
	$objAlign->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
	$objAlign->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
	$objPHPExcel-> getActiveSheet ()
		-> getStyle ("A2:I2")
		-> getFill ()
		-> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
		-> getStartColor ()
		-> setRGB ('A6E5F3');
	$objPHPExcel-> getActiveSheet ()
		-> getStyle ("A1")	
		-> getFill ()
		-> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
		-> getStartColor ()
		-> setRGB ('A6E5F3');
	$borders = array(
		'borders' => array(
			'allborders' => array(
				'style' => PHPExcel_Style_Border::BORDER_THIN,
				'color' => array('argb' => 'FF000000'),
			)
		),
	);
	$objPHPExcel->getActiveSheet()->getStyle('A2:I2')->applyFromArray($borders);
	$objPHPExcel->setActiveSheetIndex(0)
			->setCellValue('A2', 'No')
			->setCellValue('B2', 'SECTOR')
			->setCellValue('C2', 'PROYECTO')
			->setCellValue('D2', 'FUENTE')
			->setCellValue('E2', 'RUBRO')
			->setCellValue('F2', 'VALOR CSF')
			->setCellValue('G2', 'VALOR SSF')
			->setCellValue('H2', 'VALOR TOTAL CSF')
			->setCellValue('I2', 'VALOR TOTAL CFF');
	$posi=3;
	for($ii=0;$ii<count ($listantr);$ii++)
	{
		$fposi=$posi +$listantr[$ii] -1;
		$objPHPExcel->getActiveSheet()->mergeCells("A$posi:A$fposi");
		$objPHPExcel->getActiveSheet()->mergeCells("B$posi:B$fposi");
		$objPHPExcel->getActiveSheet()->mergeCells("H$posi:H$fposi");
		$objPHPExcel->getActiveSheet()->mergeCells("I$posi:I$fposi");
		$objAlign=$objPHPExcel->getActiveSheet()->getStyle("A$posi")->getAlignment();
		$objAlign->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$objAlign->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
		$objAlign=$objPHPExcel->getActiveSheet()->getStyle("B$posi")->getAlignment();
		$objAlign->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
		$objAlign=$objPHPExcel->getActiveSheet()->getStyle("H$posi")->getAlignment();
		$objAlign->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
		$objAlign=$objPHPExcel->getActiveSheet()->getStyle("I$posi")->getAlignment();
		$objAlign->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
		$objPHPExcel->setActiveSheetIndex(0)
		->setCellValueExplicit ("A$posi", $ii, PHPExcel_Cell_DataType :: TYPE_STRING)
		->setCellValueExplicit ("B$posi", iconv($_SESSION["VERCARPDFINI"], $_SESSION["VERCARPDFFIN"]."//TRANSLIT",$listasector[$ii]), PHPExcel_Cell_DataType :: TYPE_STRING)
		->setCellValueExplicit ("H$posi", round ($listasumcsf[$ii],0,PHP_ROUND_HALF_UP), PHPExcel_Cell_DataType :: TYPE_NUMERIC)
		->setCellValueExplicit ("I$posi", round ($listasumssf[$ii],0,PHP_ROUND_HALF_UP), PHPExcel_Cell_DataType :: TYPE_NUMERIC);
		$posi=$listantr[$ii]+$posi;
	}
	$i=3;
	for($ii=0;$ii<count ($listaproyecto);$ii++)
	{
		$objPHPExcel->setActiveSheetIndex(0)
		->setCellValueExplicit ("C$i", iconv($_SESSION["VERCARPDFINI"], $_SESSION["VERCARPDFFIN"]."//TRANSLIT",$listaproyecto[$ii]), PHPExcel_Cell_DataType :: TYPE_STRING)
		->setCellValueExplicit ("D$i", iconv($_SESSION["VERCARPDFINI"], $_SESSION["VERCARPDFFIN"]."//TRANSLIT",$listafuente[$ii]), PHPExcel_Cell_DataType :: TYPE_STRING)
		->setCellValueExplicit ("E$i", iconv($_SESSION["VERCARPDFINI"], $_SESSION["VERCARPDFFIN"]."//TRANSLIT",$listarubro[$ii]), PHPExcel_Cell_DataType :: TYPE_STRING)
		->setCellValueExplicit ("F$i", round ($listacsf[$ii],0,PHP_ROUND_HALF_UP), PHPExcel_Cell_DataType :: TYPE_NUMERIC)
		->setCellValueExplicit ("G$i", round ($listassf[$ii],0,PHP_ROUND_HALF_UP), PHPExcel_Cell_DataType :: TYPE_NUMERIC);
		$objPHPExcel->getActiveSheet()->getStyle("A$i:I$i")->applyFromArray($borders);
		
		$i++;
	}
	//----Propiedades de la hoja 1
	$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true); 
	$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true); 
	$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true); 
	$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true); 
	$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true); 
	$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true); 
	$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true); 
	$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true); 
	$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true); 

	$objPHPExcel->getActiveSheet()->setTitle('Reporte');
	$objPHPExcel->setActiveSheetIndex(0);

//----Guardar documento----
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename="Reporte Sectores.xlsx"');
header('Cache-Control: max-age=0');
 
$objWriter=PHPExcel_IOFactory::createWriter($objPHPExcel,'Excel2007');
$objWriter->save('php://output');
exit;

?>