<?php
	require "comun.inc";
	require "funciones.inc";
	require "conversor.php";
	session_start();
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
?>
<html lang="es">
	<head>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
    	<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<title>:: IDEAL 10 - Tesorer&iacute;a</title>
		<link href="favicon.ico" rel="shortcut icon"/>

		<script>

			function validar()
			{
				document.form2.submit();
			}

			function buscater(e)
			{
				if (document.form2.tercero.value!="")
				{
					document.form2.bt.value='1';
					document.form2.submit();
 				}
 			}

			//************* guarda comprobante ************
			//***************************************
			function guardar()
			{
				//despliegamodalm('visible','4','Eta Seguro de Guardar','2');

				ingresos2 = document.getElementsByName('dcoding[]');
				
				let medioDePago = document.getElementById('medioDePago').value;
				let banco = document.getElementById('cb').value;

				
				if(medioDePago == 1)
				{
					if (document.form2.fecha.value!='' && ingresos2.length>0 && document.form2.presupuesto.value!='-1' && banco != '')
					{
						despliegamodalm('visible','4','Esta Seguro de Guardar','2');
						/* if (confirm("Esta Seguro de Guardar"))
						{
							document.form2.oculto.value=2;
							document.form2.submit();
						} */
					}
					else
					{
						despliegamodalm('visible','2',"Falta información para poder guardar");
						document.form2.fecha.focus();
						document.form2.fecha.select();
					}
				}
				else
				{
					let noConta = document.getElementById('mediodepagosgr').value;
					if(noConta == '')
					{
						if(window.confirm("Esta seguro de no contabilizar este recaudo transferencia?"))
						{
							if (document.form2.fecha.value!='' && ingresos2.length>0 && document.form2.presupuesto.value!='-1')
							{
								despliegamodalm('visible','4','Esta Seguro de Guardar','2');
								/* if (confirm("Esta Seguro de Guardar"))
								{
									document.form2.oculto.value=2;
									document.form2.submit();
								} */
							}
							else
							{
								despliegamodalm('visible','2',"Falta información para poder guardar");
								document.form2.fecha.focus();
								document.form2.fecha.select();
							}
						}
					}
					else
					{
						if (document.form2.fecha.value!='' && ingresos2.length>0 && document.form2.presupuesto.value!='-1')
						{
							despliegamodalm('visible','4','Esta Seguro de Guardar','2');
							/* if (confirm("Esta Seguro de Guardar"))
							{
								document.form2.oculto.value=2;
								document.form2.submit();
							} */
						}
						else
						{
							despliegamodalm('visible','2',"Falta información para poder guardar");
							document.form2.fecha.focus();
							document.form2.fecha.select();
						}
					}
					
				}
				
			}

			function pdf()
			{
				document.form2.action="teso-pdfrecaudostrans.php";
				document.form2.target="_BLANK";
				document.form2.submit(); 
				document.form2.action="";
				document.form2.target="";
			}

			function buscaing(e)
			{
				if (document.form2.codingreso.value!="")
				{
					document.form2.bin.value='1';
					document.form2.submit();
				}
			}

			function despliegamodal2(_valor,_num)
			{
				document.getElementById("bgventanamodal2").style.visibility=_valor;
				if(_valor=="hidden"){document.getElementById('ventana2').src="";}
				else 
				{
					switch(_num)
					{
						case '1':	document.getElementById('ventana2').src="cuentasbancarias-ventana02.php?tipoc=D&obj01=banco&obj02=nbanco&obj03=&obj04=cb&obj05=ter";break;
						case '2':	document.getElementById('ventana2').src="teso-liquidacionrecaudo.php";break;
					}
				}
			}

			function despliegamodalm(_valor,_tip,mensa,pregunta)
			{
				document.getElementById("bgventanamodalm").style.visibility=_valor;
				if(_valor=="hidden")
				{
					document.getElementById('ventanam').src="";
					switch(document.getElementById('valfocus').value)
					{
						case "1":	document.getElementById('valfocus').value='';
									document.getElementById('tercero').focus();
									document.getElementById('tercero').select();
									break;
						case "2":	document.getElementById('valfocus').value='';
									document.getElementById('codingreso').focus();
									document.getElementById('codingreso').select();
									break;
					}
				}
				else
				{
					switch(_tip)
					{
						case "1":
							document.getElementById('ventanam').src="ventana-mensaje1.php?titulos="+mensa;break;
						case "2":
							document.getElementById('ventanam').src="ventana-mensaje3.php?titulos="+mensa;break;
						case "3":
							document.getElementById('ventanam').src="ventana-mensaje2.php?titulos="+mensa;break;
						case "4":
							document.getElementById('ventanam').src="ventana-consulta1.php?titulos="+mensa+"&idresp="+pregunta;break;	
					}
				}
			}

			function funcionmensaje()
			{
				var numdocar=document.getElementById('ncomp').value;
				document.location.href = "teso-editarecaudotransferencia.php?idrecaudo="+numdocar;
			}

			function respuestaconsulta(pregunta)
			{
				switch(pregunta)
				{
					case "1":	document.form2.oculto.value='3';
								document.form2.submit();
								break;
					case "2":	document.form2.oculto.value='2';
								document.form2.submit();
								break;
				}
			}

			/* function respuestamensaje()
			{
				location.href="teso-editarecaudotransferencia.php?idrecaudo="+document.form2.idcomp.value;
			} */

		</script>
		<script src="css/programas.js"></script>
		<script src="css/calendario.js"></script>
		<link href="css/css2.css" rel="stylesheet" type="text/css" />
		<link href="css/css3.css" rel="stylesheet" type="text/css" />
		<link href="css/css4.css" rel="stylesheet" type="text/css" />
		<link href="css/tabs.css" rel="stylesheet" type="text/css" />

	</head>
	<body>
		<div id="bgventanamodalm" class="bgventanamodalm">
            <div id="ventanamodalm" class="ventanamodalm">
                <IFRAME src="" name="ventanam" marginWidth=0 marginHeight=0 frameBorder=0 id="ventanam" frameSpacing=0 style=" width:700px; height:130px; top:200; overflow:hidden;"> 
                </IFRAME>
            </div>
        </div>
		<IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
		<span id="todastablas2"></span>
		<table>
			<tr><script>barra_imagenes("teso");</script><?php cuadro_titulos();?></tr>	 
			<tr><?php menu_desplegable("teso");?></tr>
			<tr>
				<td colspan="3" class="cinta">
					<a href="teso-recaudotransferencia.php" class="mgbt" ><img src="imagenes/add.png" title="Nuevo"/></a>  
					<a onClick="guardar()" class="mgbt"><img src="imagenes/guarda.png" title="Guardar" /></a>  
					<a href="teso-buscarecaudotransferencia.php" class="mgbt"><img src="imagenes/busca.png" title="Buscar"/></a> 
					<a onClick="mypop=window.open('plan-agenda.php','','');mypop.focus()" class="mgbt"><img src="imagenes/agenda1.png" title="Agenda" /></a>
					<a onClick="mypop=window.open('teso-principal.php','','');mypop.focus();" class="mgbt"><img src="imagenes/nv.png" title="Nueva ventana"></a>  
					<a onClick="mypop=window.open('<?php echo $url2; ?>','','');mypop.focus();" class="tooltip bottom mgbt"><img src="imagenes/duplicar_pantalla.png" title="Duplicar pesta&ntilde;a"></a>
					<a <?php if($_POST['oculto']==2) { ?> onClick="pdf()"  <?php } ?> class="mgbt"><img src="imagenes/print.png"  title="Buscar"/></a>
				</td>
			</tr>		  
		</table>
		<tr>
			<td colspan="3" class="tablaprin" align="center"> 
				<?php
				
					//*********** 11050501	CAJA PRINCIPAL esta es la cuenta que va a credito en todas las consignacones
					
					if(!$_POST['oculto'])
					{
						$sqlr = "SELECT cuentacaja FROM tesoparametros";
						$res = mysqli_query($linkbd, $sqlr);
						$row = mysqli_fetch_row($res);
						$_POST['cuentacaja']=$row[0];

						$sqlr = "SELECT MAX(id_recaudo) FROM tesorecaudotransferencia";
						$res = mysqli_query($linkbd, $sqlr);
						$r = mysqli_fetch_row($res);
						$_POST['idcomp']=$r[0]+1;	

						$fec=date("d/m/Y");
						$_POST['fecha']=$fec; 		 		  			 
						$_POST['valor']=0;
						$_POST['vigencia'] = date('Y');
					}
					
					switch($_POST['tabgroup1'])
					{
						case 1:
						$check1='checked';
						break;
						case 2:
						$check2='checked';
						break;
						case 3:
						$check3='checked';
					}

					$_POST['dcoding'] = array(); 		 
					$_POST['dncoding'] = array(); 		 
					$_POST['dvalores'] = array(); 
					$_POST['dfuente'] = array();
					$_POST['dcc'] = array();

					$sqlrComprobarExist = "SELECT * FROM tesorecaudotransferencia WHERE idcomp = '".$_POST['idrecaudo']."' AND estado = 'S'";
					$resComprobarExist = mysqli_query($linkbd, $sqlrComprobarExist);
					$cantExits = mysqli_num_rows($resComprobarExist);
					
					if($cantExits > 0)
					{
						echo "<script>despliegamodalm('visible','2',' Ya existe un recaudo transferencia con esta liquidacion.');</script>";
					}
					else
					{
						$sqlr = "SELECT DISTINCT * FROM tesorecaudotransferencialiquidar_det WHERE id_recaudo = '".$_POST['idrecaudo']."'";
						$res = mysqli_query($linkbd, $sqlr);
						
						while ($row = mysqli_fetch_row($res)) 
						{
							$_POST['dcoding'][] = $row[2];	
							$_POST['dncoding'][] = buscaingreso($row[2]);			 
							$_POST['dvalores'][] = $row[3];		
							$_POST['dfuente'][] = $row[5];	
							$_POST['dcc'][] = $row[6];	
						}

						$sqlr = "SELECT DISTINCT * FROM tesorecaudotransferencialiquidar WHERE id_recaudo = '".$_POST['idrecaudo']."'";	
						$res = mysqli_query($linkbd, $sqlr);
						$row = mysqli_fetch_row($res);
						
						$_POST['concepto'] = $row[6];			
						$_POST['tercero'] = $row[7];				 
						$_POST['ntercero'] = buscatercero($row[7]);				 	 
						$_POST['cc'] = $row[8];
						$_POST['medioDePago'] = $row[11];
					}
					
				?>
 				<form name="form2" method="post" action=""> 
 					<?php
						//***** busca tercero
						/* if($_POST['bt']=='1')
						{
							$nresul=buscatercero($_POST['tercero']);
							if($nresul != '')
							{
								$_POST['ntercero'] = $nresul;
							}
							else
							{
								$_POST['ntercero'] = "";
							}
						}
						//******** busca ingreso *****
						//***** busca tercero
						if($_POST['bin'] == '1')
						{
							$nresul = buscaingreso($_POST['codingreso']);
							if($nresul != '')
							{
								$_POST['ningreso'] = $nresul;
							}
							else
							{
								$_POST['ningreso'] = "";
							}
						} */

						//***** busca tercero
						if($_POST['bt'] == '1')
						{
			  				$nresul = buscatercero($_POST['tercero']);
							if($nresul!='')
							{
			  					$_POST['ntercero'] = $nresul;
  								?>
								<script>
			  						document.getElementById('codingreso').focus();document.getElementById('codingreso').select();
								</script>
			  					<?php
			  				}
							else
							{
			  					$_POST['ntercero'] = "";
			  					?>
									<script>
										alert("Tercero Incorrecto o no Existe")				   		  	
										document.form2.tercero.focus();	
									</script>
			  					<?php
			  				}
			 			}
						//*** ingreso
						if($_POST['bin'] == '1')
						{
							$nresul = buscaingreso($_POST['codingreso']);
							if($nresul != '')
							{
			  					$_POST['ningreso'] = $nresul;
  			  					?>
								<script>
									document.getElementById('valor').focus();document.getElementById('valor').select();
								</script>
								<?php
			  				}
							else
							{
								$_POST['codingreso'] = "";
								?>
			  					<script>alert("Codigo Ingresos Incorrecto");document.form2.codingreso.focus();</script>
			  					<?php
			  				}
			 			}
 					?>
 

   					<table class="inicio" align="center" >
						<tr >
							<td class="titulos" style="width:93%;" colspan="3"> Recaudos Transferencias</td>
							<td class="cerrar" style="width:7%;" ><a href="teso-principal.php">Cerrar</a></td>
						</tr>
      					<tr>
      						<td style="width:80%;">
      							<table>
      								<tr>
										<td style="width:10%;" class="saludo1" >No Recaudo:</td>
										<td style="width:10%;">
											<input name="idcomp" id="idcomp" type="text" value="<?php echo $_POST['idcomp']?>" style="width:80%; text-align: center;" onKeyUp="return tabular(event,this)" readonly>
										</td>
										<td style="width:10%;" class="saludo1">Fecha:</td>
										<td style="width:10%;">

											<input type="text" name="fecha"  value="<?php echo $_POST['fecha']?>" onKeyUp="return tabular(event,this)" id="fc_1198971545" title="DD/MM/YYYY" onDblClick="displayCalendarFor('fc_1198971545');" class="colordobleclik" autocomplete="off" onChange="" readonly>
											        
										</td>
										<td style="width:10%;" class="saludo1">No Liquid:</td>
										<td style="width:10%;">
											<input type="text" id="idrecaudo" name="idrecaudo" value="<?php echo $_POST['idrecaudo']?>" style="width:70%;" onKeyUp="return tabular(event,this)" onBlur="validar()" ><a onClick="despliegamodal2('visible','2');" style="cursor:pointer;" title="Listado Ordenes Pago"><img src="imagenes/find02.png" style="width:20px;"/></a> 
										</td>
										<td style="width:8%;" class="saludo1">Centro Costo:</td>
										<td style="width:10%;">
											<select name="cc"  onChange="validar()" style="width:100%;" onKeyUp="return tabular(event,this)">
												<?php
													$sqlr = "SELECT id_cc, nombre FROM centrocosto WHERE estado = 'S' AND entidad = 'S'";
													$res = mysqli_query($linkbd, $sqlr);
													while ($row = mysqli_fetch_row($res)) 
													{
														echo "<option value=$row[0] ";
														$i=$row[0];
														if($i==$_POST['cc'])
														{
															echo "SELECTED";
														}
														echo ">".$row[0]." - ".$row[1]."</option>";	 	 
													}	 	
												?>
											</select>
										</td>

										<td style="width:10%;" class="saludo1">Vigencia:</td>
										<td style="width:5%;">
											<input type="text" id="vigencia" name="vigencia" style="width:100%;" onKeyPress="javas cript:return solonumeros(event)" onKeyUp="return tabular(event,this)"  value="<?php echo $_POST['vigencia']?>" onClick="document.getElementById('tipocta').focus(); document.getElementById('tipocta').select();" readonly>
										</td>
										<td></td>
									</tr>
			        				<tr>
										<td class="saludo1">Concepto Recaudo:</td>
										<td colspan="7">
											<input name="concepto" type="text" value="<?php echo $_POST['concepto']?>" style="width:100%;" onKeyUp="return tabular(event,this)" readonly>
										</td>
										
										<td class="saludo1" >Ingresa presupuesto? </td>
										<td>
											<select name="presupuesto" id="presupuesto" onKeyUp="return tabular(event,this)" style="width:100%">
												<option value="1" <?php if(($_POST['presupuesto']=='1')) echo "SELECTED"; ?>>SI</option>
												<option value="2" <?php if($_POST['presupuesto']=='2') echo "SELECTED"; ?>>NO</option>         
											</select>
										</td>
			        				</tr>
			     					<tr>
						 				<?php
											if($_POST['medioDePago'] != 2)
											{
												?>
												<td style="width:5%;" class="saludo1">Recaudado:</td>
												<td style="width:10%;">
													<?php
														$sqlr = "SELECT TB.estado, TB.cuenta, TB.ncuentaban, TB.tipo, T.razonsocial, TB.tercero FROM tesobancosctas AS TB, terceros AS T WHERE TB.tercero = T.cedulanit AND TB.estado = 'S' ";
														$res = mysqli_query($linkbd, $sqlr);
														while ($row = mysqli_fetch_row($res))
														{
															$i = $row[1];
															$ncb = buscacuenta($row[1]);
															if($i == $_POST['banco'])
															{
																$_POST['nbanco']=$row[4];
																$_POST['ter']=$row[5];
																$_POST['cb']=$row[2];
															}	 
														}
													?>
													<input type="text" name="cb" id="cb" value="<?php echo $_POST['cb']; ?>" style="width:80%;"/>&nbsp;
														<a onClick="despliegamodal2('visible','1');"  style="cursor:pointer;" title="Listado Cuentas Bancarias">	
															<img src='imagenes/find02.png' style='width:20px;'/>
														</a>
													<input name="banco" id="banco" type="hidden" value="<?php echo $_POST['banco']?>" >
													<input type="hidden" id="ter" name="ter" value="<?php echo $_POST['ter']?>" >           
												</td>
												<td colspan="6"> 
													<input type="text" id="nbanco" name="nbanco" value="<?php echo $_POST['nbanco']?>" style="width:100%;" readonly>
												</td>
												<td class="saludo1" >Medio de pago: </td>
												<td>
													<select name="medioDePago" id="medioDePago" disabled onKeyUp="return tabular(event,this)" style="width:100%">
														<option value="1" <?php if(($_POST['medioDePago']=='1')) echo "SELECTED"; ?>>CSF</option>
														<option value="2" <?php if($_POST['medioDePago']=='2') echo "SELECTED"; ?>>SSF</option>         
													</select>
												</td>
											<?php
											}
											else
											{
												$regalias = "MEDIO PAGO SSF";
												?>
												<td class="saludo1">Recaudo: </td>
												<td colspan="3"> 
													<input type="text" id="regalias" value="<?php echo $regalias;?>" style="width:100%;" readonly>
												</td>
												<td class="saludo1" >Tipo: </td>
												<td>
													<select name="medioDePago" id="medioDePago" disabled onKeyUp="return tabular(event,this)" style="width:100%">
														<option value="1" <?php if(($_POST['medioDePago']=='1')) echo "SELECTED"; ?>>CSF</option>
														<option value="2" <?php if($_POST['medioDePago']=='2') echo "SELECTED"; ?>>SSF</option>         
													</select>
												</td>
												<td class="saludo1">Medio de Pago:</td>
												<td>
													<select name="mediodepagosgr" id = "mediodepagosgr" style="width:100%;" onKeyUp="return tabular(event,this)">
														<option value="">No contabiliza </option>
														<?php
															
															$sqlr = "select *from tesomediodepago where estado='S'";
															$res = mysqli_query($linkbd, $sqlr);
															while ($row = mysqli_fetch_row($res)) 
															{
																echo "<option value=$row[0] ";
																$i=$row[0];
																if($i==$_POST['mediodepagosgr'])
																{
																	echo "SELECTED";
																}
																echo ">".$row[0]." - ".$row[1]."</option>";	 	 
															}	 	
														?>
													</select>
												</td>
											<?php
											}
										?>
				 					</tr>
									<tr>
										<td class="saludo1">NIT: </td>
										<td>
											<input name="tercero" type="text" value="<?php echo $_POST['tercero']?>" style="width:100%;" onKeyUp="return tabular(event,this)" onBlur="buscater(event)" readonly>
										</td>
										<td colspan="8">
											<input type="text" id="ntercero" name="ntercero" value="<?php echo $_POST['ntercero']?>" style="width:100%;" onKeyUp="return tabular(event,this) "  readonly>
											<input type="hidden" value="0" name="bt">
											<input type="hidden" id="cb" name="cb" value="<?php echo $_POST['cb']?>" >
											<input type="hidden" id="ct" name="ct" value="<?php echo $_POST['ct']?>" >
											<input type="hidden" value="1" name="oculto">
										</td>
									</tr>
      							</table>
      						</td>
      					</tr>
					</table>
      
     				<div class="subpantalla estilos-scroll" style="resize: vertical;">
	   					<table class="inicio">
	   	   					<tr>
   	      						<td colspan="5" class="titulos">Detalle  Recaudos Transferencia</td>
							</tr>                  
							<tr>
								<td class="titulos2">Codigo</td>
								<td class="titulos2">Ingreso</td>
								<td class="titulos2">Centro costo</td>
								<td class="titulos2">Valor</td>
							</tr>
							<?php
		  					$_POST['totalc']=0;
							for ($x = 0; $x < count($_POST['dcoding']); $x++)
							{
		 						echo "<tr class='saludo1'>
									<td style='width:5%;'>
										<input name='dcoding[]' class='inpnovisibles' value='".$_POST['dcoding'][$x]."' type='text' style='width:100%;' readonly>
									</td>
									<td>
										<input name='dncoding[]' class='inpnovisibles' value='".$_POST['dncoding'][$x]."' type='text' style='width:100%;' readonly>
									</td>
									<td style='width:10%;'>
										<input name='dcc[]' class='inpnovisibles' value='".$_POST['dcc'][$x]."' type='text' style='width:100%;' readonly>
									</td>
									<td style='width:10%;'>
										<input name='dvalores[]' class='inpnovisibles' value='".$_POST['dvalores'][$x]."' type='text' style='width:100%; text-align:right;' readonly>
									</td>
								</tr>";
								$_POST['totalc']=$_POST['totalc']+$_POST['dvalores'][$x];
								$_POST['totalcf']=number_format($_POST['totalc'],2);
		 					}
							$resultado = convertir($_POST['totalc']);
							$_POST['letras']=$resultado." Pesos";
							echo "<tr class='saludo1'>
									<td colspan = '3' style='font-size:18px; text-align: center; color:gray !important; ' class='saludo1'>TOTAL:</td>
									<td>
										<input name='totalcf' type='text' value='$_POST[totalcf]' style='width:100%; text-align:right !important;' readonly>
										<input name='totalc' type='hidden' value='$_POST[totalc]'>
									</td>
								</tr>
								<tr>
									<td class='saludo1'>Son:</td>
									<td>
										<input name='letras' type='text' class='inpnovisibles' value='$_POST[letras]' style='width:100%;'>
									</td>
								</tr>";
							?> 
						</table>
					</div>
					<?php
					if($_POST['oculto']=='2')
					{
						preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST['fecha'], $fecha);
						$fechaf = $fecha[3]."-".$fecha[2]."-".$fecha[1];

						$_POST['vigencia'] = $fecha[3];

						$bloq = bloqueos($_SESSION['cedulausu'], $fechaf);

						if($bloq >= 1)
						{
							
							//***cabecera comprobante
							if($_POST['conSinBanco'] == "NO")
							{
								$_POST['concepto'] = "ESTE DOCUMENTO NO REQUIERE CAUSACION CONTABLE ".$_POST['concepto'];
							}

							$sqlr = "INSERT INTO tesorecaudotransferencia (idcomp, fecha, vigencia, banco, ncuentaban, concepto, tercero, cc, valortotal, estado, presupuesto, mediopago) VALUES ($_POST[idrecaudo], '$fechaf', '".$_POST['vigencia']."', '$_POST[ter]', '$_POST[cb]', '".strtoupper($_POST['concepto'])."', '$_POST[tercero]', '$_POST[cc]', '$_POST[totalc]', 'S', '$_POST[presupuesto]', '$_POST[medioDePago]')";
							
							mysqli_query($linkbd, $sqlr);
							$idrec = mysqli_insert_id($linkbd);

							$_POST['idcomp'] = $idrec;


							$sqlr = "INSERT INTO comprobante_cab (numerotipo, tipo_comp, fecha, concepto, total, total_debito, total_credito, diferencia, estado) VALUES (".$_POST['idcomp'].", 14, '$fechaf', '".strtoupper($_POST['concepto'])."', 0, $_POST[totalc], $_POST[totalc], 0, 1)";
							
							mysqli_query($linkbd, $sqlr);
					
							//******************* DETALLE DEL COMPROBANTE CONTABLE *********************
							echo "<input type='hidden' name='ncomp' id='ncomp' value='$idrec'>";

							if($_POST['medioDePago'] != 2)
							{
								for($x=0; $x < count($_POST['dcoding']); $x++)
								{
									//***** BUSQUEDA INGRESO ********

									$sqlri = "SELECT concepto, porcentaje, cuentapres FROM tesoingresos_det WHERE codigo = '".$_POST['dcoding'][$x]."' AND vigencia = (SELECT MAX(vigencia) FROM tesoingresos_det WHERE codigo = '".$_POST['dcoding'][$x]."')";
									
									/* $sqlri = "Select * from tesoingresos_det where codigo='".$_POST['dcoding'][$x]."'  and vigencia=$vigusu"; */
									$resi = mysqli_query($linkbd, $sqlri);
									//	echo "$sqlri <br>";	    
									while($rowi = mysqli_fetch_row($resi))
									{
										$cuentasContables = concepto_cuentasn2($rowi[0], 'C', 4, $_POST['dcc'][$x], "$fechaf");

										foreach($cuentasContables as $cuentaCont)
										{
											$porce = $rowi[1];
											if($cuentaCont[2] == 'S')
											{
												$valorcred = $_POST['dvalores'][$x]*($porce/100);
												$valordeb = 0;

												$sqlr = "INSERT INTO comprobante_det (id_comp, cuenta, tercero, centrocosto, detalle, cheque, valdebito, valcredito, estado, vigencia)  VALUES ('14 ".$_POST['idcomp']."', '".$cuentaCont[0]."', '".$_POST['tercero']."', '".$_POST['dcc'][$x]."', 'Recaudo Transferencia".strtoupper($_POST['dncoding'][$x])."', '', ".$valordeb.", ".round($valorcred, 2).", '1', '".$_POST['vigencia']."')";
												mysqli_query($linkbd, $sqlr);

												$valordeb = $_POST['dvalores'][$x]*($porce/100);
												$valorcred = 0;

												$sqlr = "INSERT INTO comprobante_det (id_comp, cuenta, tercero, centrocosto, detalle, cheque, valdebito, valcredito, estado, vigencia) VALUES ('14 ".$_POST['idcomp']."', '".$_POST['banco']."', '".$_POST['tercero']."', '".$_POST['dcc'][$x]."', 'Recaudo Transferencia".strtoupper($_POST['dncoding'][$x])."', '', ".round($valordeb, 2).", ".$valorcred.", '1', '".$_POST['vigencia']."')";//echo $sqlr."<br>";
												mysqli_query($linkbd, $sqlr);

												/* $fuenteIngreso = consularFuenteIngresos($rowi[2], $_POST['vigencia']);

												$sqlr = "INSERT INTO pptoingtranppto (cuenta, idrecibo, valor, fuente, vigencia) VALUES ('$rowi[2]', ".$_POST['idcomp'].", $valordeb, $fuenteIngreso, ".$_POST['vigencia'].")";

												mysqli_query($linkbd, $sqlr); */

											}
										}
									}
								}
							}
							elseif($_POST['mediodepagosgr']!='')
							{
								for($x=0;$x<count($_POST['dcoding']);$x++)
								{
									//***** BUSQUEDA INGRESO ********
									$sqlri = "SELECT concepto, porcentaje, cuentapres  FROM tesoingresos_det WHERE codigo = '".$_POST['dcoding'][$x]."' AND vigencia = (SELECT MAX(vigencia) FROM tesoingresos_det WHERE codigo = '".$_POST['dcoding'][$x]."' )";
									$resi = mysqli_query($linkbd, $sqlri);
									//	echo "$sqlri <br>";	    
									while($rowi = mysqli_fetch_row($resi))
									{
										//**** busqueda concepto contable*****

										$cuentasContables = concepto_cuentasn2($rowi[0], 'C', 4, $_POST['dcc'][$x], "$fechaf");

										foreach($cuentasContables as $cuentaCont)
										{
											$porce = $rowi[1];
											if($cuentaCont[2] == 'S')
											{
												$valorcred = $_POST['dvalores'][$x]*($porce/100);
												$valordeb = 0;
												$sqlr = "INSERT INTO comprobante_det (id_comp, cuenta, tercero, centrocosto, detalle, cheque, valdebito, valcredito, estado, vigencia) VALUES ('14 ".$_POST['idcomp']."', '".$cuentaCont[0]."', '".$_POST['tercero']."', '".$_POST['dcc'][$x]."', 'Recaudo Transferencia".strtoupper($_POST['dncoding'][$x])."', '', ".$valordeb.", ".round($valorcred, 2).", '1', '".$_POST['vigencia']."')";
												mysqli_query($linkbd, $sqlr);

												$valordeb = $_POST['dvalores'][$x]*($porce/100);
												$valorcred = 0;
												$sqlrMedioPago = "SELECT cuentacontable FROM tesomediodepago WHERE id='$_POST[mediodepagosgr]' AND estado='S'";
												$resMedioPago = mysqli_query($linkbd, $sqlrMedioPago);
												$rowMedioPago = mysqli_fetch_row($resMedioPago);
												$sqlr = "INSERT INTO comprobante_det (id_comp, cuenta, tercero, centrocosto, detalle, cheque, valdebito, valcredito, estado, vigencia) VALUES ('14 ".$_POST['idcomp']."', '".$rowMedioPago[0]."', '".$_POST['tercero']."', '".$_POST['dcc'][$x]."', 'Recaudo Transferencia".strtoupper($_POST['dncoding'][$x])."', '', ".round($valordeb, 2).", ".$valorcred.", '1', '".$_POST['vigencia']."')";
												mysqli_query($linkbd, $sqlr);

											}
										}
									}
								}
							}
							//************ insercion de cabecera recaudos ************
							
							

							//echo "Conc: $sqlr <br>";
							//************** insercion de consignaciones **************

			
							for($x = 0; $x < count($_POST['dcoding']); $x++)
							{
								$sqlr = "INSERT INTO tesorecaudotransferencia_det (id_recaudo, ingreso, valor, estado) VALUES ($idrec, '".$_POST['dcoding'][$x]."', ".round($_POST['dvalores'][$x], 2).", 'S')";
								if (!mysqli_query($linkbd, $sqlr))
								{
									echo "<table ><tr><td class='saludo1'><center><font color=blue><img src='imagenes/alert.png'> Manejador de Errores de la Clase BD<br><font size=1></font></font><br><p align=center>No se pudo ejecutar la petici�n: <br><font color=red><b>$sqlr</b></font></p>";
									//	 $e =mysql_error($respquery);
									echo "Ocurri� el siguiente problema:<br>";
									//echo htmlentities($e['message']);
									echo "<pre>";
									///echo htmlentities($e['sqltext']);
									// printf("\n%".($e['offset']+1)."s", "^");
									echo "</pre></center></td></tr></table>";
								}
								else
								{
									$fuenteFinanciacion = '';
									if($_POST['dfuente'][$x] == ''){
										$sqlrFuente = "SELECT fuente FROM tesoingresos_fuentes WHERE codigo = '".$_POST['dcoding'][$x]."' AND estado = 'S'";
										$respFuente = mysqli_query($linkbd, $sqlrFuente);
										$rowFuente = mysqli_fetch_row($respFuente);
										$fuenteFinanciacion = $rowFuente[0];
									}else{
										$fuenteFinanciacion = $_POST['dfuente'][$x];
									}

									$sqlSeccionPresupuestal = "SELECT id_sp FROM centrocostos_seccionpresupuestal WHERE id_cc = '".$_POST['dcc'][$x]."'";
									$rowSeccionPresupuestal = mysqli_fetch_row(mysqli_query($linkbd, $sqlSeccionPresupuestal));

									$medio_pa = '';
									if($_POST['medioDePago'] == '1'){
										$medio_pa = 'CSF';
									}else{
										$medio_pa = 'SSF';
									}

									if($_POST['medioDePago'] != 2)
									{
										$sqlri = "SELECT porcentaje, cuentapres, cuenta_clasificadora FROM tesoingresos_det WHERE codigo = '".$_POST['dcoding'][$x]."' AND vigencia = (SELECT MAX(vigencia) FROM tesoingresos_det WHERE codigo = '".$_POST['dcoding'][$x]."' )";
										$resi = mysqli_query($linkbd, $sqlri); 
										while($rowi = mysqli_fetch_row($resi))
										{
											$porce = $rowi[0];
											$vi = $_POST['dvalores'][$x]*($porce/100);
											
											if($_POST['presupuesto'] == "1" && $vi > 0 && $rowi[1] != '')
											{
												//****creacion documento presupuesto ingresos

												$sqlr = "INSERT INTO pptoingtranppto (cuenta, idrecibo, valor, vigencia, fuente, productoservicio, seccion_presupuestal, medio_pago, vigencia_gasto) VALUES ('$rowi[1]', ".$_POST['idcomp'].", ".round($vi, 2).", ".$_POST['vigencia'].", '".$fuenteFinanciacion."', '".$rowi[2]."', '".$rowSeccionPresupuestal[0]."', '".$medio_pa."', '1')";
												mysqli_query($linkbd, $sqlr);
											}
											
										}
									}
									elseif($_POST['mediodepagosgr'] != '')
									{
										$sqlri = "SELECT porcentaje, cuentapres, cuenta_clasificadora FROM tesoingresos_det WHERE codigo = '".$_POST['dcoding'][$x]."' AND vigencia=(SELECT MAX(vigencia) FROM tesoingresos_det WHERE codigo = '".$_POST['dcoding'][$x]."' )";
										
										$resi = mysqli_query($linkbd, $sqlri);
										//	echo "$sqlri <br>";	    
										while($rowi = mysqli_fetch_row($resi))
										{
											$porce = $rowi[0];
											$vi = $_POST['dvalores'][$x]*($porce/100);
											
											if($_POST['presupuesto'] == "1" && $vi > 0 && $rowi[1] != '')
											{
												//****creacion documento presupuesto ingresos

												$sqlr = "INSERT INTO pptoingtranppto (cuenta, idrecibo, valor, vigencia, fuente, productoservicio, seccion_presupuestal, medio_pago, vigencia_gasto) VALUES ('$rowi[1]', ".$_POST['idcomp'].", ".round($vi, 2).", ".$_POST['vigencia'].", '".$fuenteFinanciacion."', '".$rowi[2]."', '".$rowSeccionPresupuestal[0]."', '".$medio_pa."', '1')";

												mysqli_query($linkbd, $sqlr);
											}
										}
									}

									echo "<table  class='inicio'>
										<tr>
											<td class='saludo1'>
												<center>Se ha almacenado el Recaudo con Exito <img src='imagenes/confirm.png'></center>
											</td>
										</tr>
									</table>
									";
									
									?>
									<script>
										document.form2.numero.value="";
										document.form2.valor.value=0;
									</script>
									<?php
								}
							}
							
							if($idrec)
							{
								echo "<script>despliegamodalm('visible','1','Se ha almacenado el Recaudo con Exito');</script>";
							}
						}
						else
						{
							echo "<script>despliegamodalm('visible','2',' La fecha del documento es menor a la fecha de bloqueo.');</script>";
						}
						
					}
					?>	
					<div id="bgventanamodal2">
						<div id="ventanamodal2">
							<IFRAME  src="" name="buster" marginWidth=0 marginHeight=0 frameBorder=0 id="ventana2" frameSpacing=0 style="left:500px; width:900px; height:500px; top:200;"> 
							</IFRAME>
						</div>
					</div>	
				</form>
 			</td>
		</tr>
	</body>
</html> 		