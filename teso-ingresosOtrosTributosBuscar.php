<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require 'comun.inc';
	require 'funciones.inc';
	session_start();
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html lang=es>
	<head>
    <meta charset="utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1"/>
		<meta name="viewport" content="user-scalable=no">
		<title>:: IDEAL 10 - Tesorería</title>
		<link href="favicon.ico" rel="shortcut icon"/>
		<link href="css/css2z.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
        <link href="css/style.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" href="sweetalert2/dist/sweetalert2.min.css">
        <script type="text/javascript" src="css/programas.js"></script>
        <script src="sweetalert2/dist/sweetalert2.min.js"></script>
		<script src="vue/vue.min.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
	</head>
	<body>
		<header>
			<table>
				<tr><script>barra_imagenes("teso");</script><?php cuadro_titulos();?></tr>
			</table>
		</header>

		<form name="form2" method="post" action="">
			<section id="myapp" v-cloak >
                <div id="cargando" v-show="isLoading" class="loading" style="z-index: 10000;">
                    <span>Cargando...</span>
                </div>
				<nav>
					<table>
						<tr><?php menu_desplegable("teso");?></tr>
					</table>
                    <div class="bg-white group-btn p-1">
                        <button type="button" @click="window.location.href='teso-ingresosOtrosTributosCrear'" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
                            <span>Nuevo</span>
                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960">
                                <path d="M440-440H200v-80h240v-240h80v240h240v80H520v240h-80v-240Z"></path>
                            </svg>
                        </button>
                        <button type="button" onclick="mypop=window.open('teso-principal','','');mypop.focus();" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
                            <span>Nueva ventana</span>
                            <svg class="fill-black group-hover:fill-white w-5 h-5" xmlns="http://www.w3.org/2000/svg"
                                viewBox="0 -960 960 960">
                                <path
                                    d="M320-120v-80h80v-80H160q-33 0-56.5-23.5T80-360v-400q0-33 23.5-56.5T160-840h640q33 0 56.5 23.5T880-760v400q0 33-23.5 56.5T800-280H560v80h80v80H320ZM160-360h640v-400H160v400Zm0 0v-400 400Z">
                                </path>
                            </svg>
                        </button>
                        <button type="button" @click="mypop=window.open('<?php echo $url2; ?>','','');mypop.focus();"  class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
                            <span class="group-hover:text-white">Duplicar pantalla</span>
                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960">
                                <path
                                    d="M640-160v-360H160v360h480Zm80-200v-80h80v-360H320v200h-80v-200q0-33 23.5-56.5T320-880h480q33 0 56.5 23.5T880-800v360q0 33-23.5 56.5T800-360h-80ZM160-80q-33 0-56.5-23.5T80-160v-360q0-33 23.5-56.5T160-600h480q33 0 56.5 23.5T720-520v360q0 33-23.5 56.5T640-80H160Zm400-603ZM400-340Z">
                                </path>
                            </svg>
                        </button>
                        <button type="button" @click="printExcel()" class="btn btn-success btn-success-hover d-flex justify-between align-items-center">
                            <span>Exportar Excel</span>
                            <svg class="fill-black group-hover:fill-white w-4 h-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 384 512"><!--!Font Awesome Free 6.5.1 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license/free Copyright 2024 Fonticons, Inc. -->
                                <path d="M48 448V64c0-8.8 7.2-16 16-16H224v80c0 17.7 14.3 32 32 32h80V448c0 8.8-7.2 16-16 16H64c-8.8 0-16-7.2-16-16zM64 0C28.7 0 0 28.7 0 64V448c0 35.3 28.7 64 64 64H320c35.3 0 64-28.7 64-64V154.5c0-17-6.7-33.3-18.7-45.3L274.7 18.7C262.7 6.7 246.5 0 229.5 0H64zm90.9 233.3c-8.1-10.5-23.2-12.3-33.7-4.2s-12.3 23.2-4.2 33.7L161.6 320l-44.5 57.3c-8.1 10.5-6.3 25.5 4.2 33.7s25.5 6.3 33.7-4.2L192 359.1l37.1 47.6c8.1 10.5 23.2 12.3 33.7 4.2s12.3-23.2 4.2-33.7L222.4 320l44.5-57.3c8.1-10.5 6.3-25.5-4.2-33.7s-25.5-6.3-33.7 4.2L192 280.9l-37.1-47.6z">
                                </path>
                            </svg>
                        </button>
                        <button type="button" @click="window.location.href='teso-menuIngresos'" class="btn btn-success btn-success-hover d-flex justify-between align-items-center">
                            <span>Atras</span>
                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960">
                                <path
                                    d="M760-200v-160q0-50-35-85t-85-35H273l144 144-57 56-240-240 240-240 57 56-144 144h367q83 0 141.5 58.5T840-360v160h-80Z">
                                </path>
                            </svg>
                        </button>
                    </div>
				</nav>
				<article class="bg-white">
                    <div>
                        <h2 class="titulos m-0">Buscar ingresos</h2>
                        <div class="d-flex">
                            <div class="form-control">
                                <label class="form-label">Buscar:</label>
                                <div class="d-flex">
                                    <input type="text" @keyup="search()" v-model="strSearch" placeholder="buscar código o nombre de ingreso">
                                    <button type="button" class="btn btn-primary">Buscar</button>
                                </div>
                            </div>
                            <div class="form-control">
                                <label class="form-label">Convenciones:</label>
                                <div class="d-flex">
                                    <span class="badge badge-warning"> Cuenta sin asignar</span>
                                    <span class="badge badge-danger"> Cuenta no habilitada</span>
                                </div>
                            </div>
                        </div>
                        <h2 class="titulos m-0">Resultados: {{ intResults }} </h2>
                        <div class="table-responsive" style="max-height:50vh;">
                            <table class="table table-hover fw-normal">
                                <thead>
                                    <tr class="text-center">
                                        <th>Código</th>
                                        <th>Nombre</th>
                                        <th>Tipo</th>
                                        <th>Presupuesto</th>
                                        <th>Destino/Tercero</th>
                                        <th>Afecta presupuesto</th>
                                        <th>Concepto</th>
                                        <th>Tipo ingreso</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr @dblclick="editItem(data.codigo)" style="height:50px;" v-for="(data,index) in arrData"  :key="index"
                                    :class="[(data.cuentapres == '' && data.is_tercero==2 && data.is_cuenta==2 ? 'bg-warning text-black' : ''),
                                    (data.estado_cuenta == 2  && data.is_tercero== 2 && data.is_cuenta== 2 ? 'bg-danger text-white' : '')]">
                                        <td class="text-center">{{ data.codigo}}</td>
                                        <td >{{ data.nombre}}</td>
                                        <td class="text-center">{{ data.tipo == "S" ? "Simple" : "Compuesto"}}</td>
                                        <td >{{ data.tipo == "S" ? data.cuentapres : ""}}</td>
                                        <td >{{data.terceros}}</td>
                                        <td class="text-center">{{data.is_cuenta == "1" && data.tipo == "S" ? "No" : data.is_cuenta !=null && data.is_cuenta == 2 && data.tipo == "S"? "Si" : ""}}</td>
                                        <td class="text-center">{{ data.concepto}}</td>
                                        <td class="text-center">{{ data.tipoingreso}}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div v-if="arrData !=''" class="inicio">
                        <p style="text-align:center">Página {{ intPage }} de {{ intTotalPages }}</p>
                        <ul style="list-style:none; padding:0;display:flex;justify-content: center;align-items:center">
                            <li v-show="intPage > 1" @click="search(intPage = 1)" style="cursor:pointer;padding:4px 10px;background:#fff; border:1px solid #39c"><< </li>
                            <li v-show="intPage > 1" @click="search(--intPage)" style="cursor:pointer;padding:4px 10px;background:#fff; border:1px solid #39c" ><</li>
                            <li v-show="intPage < intTotalPages" @click="search(++intPage)" style="cursor:pointer;padding:4px 10px;background:#fff; border:1px solid #39c" >></li>
                            <li v-show="intPage < intTotalPages" @click="search(intPage = intTotalPages)" style="cursor:pointer;padding:4px 10px;background:#fff; border:1px solid #39c">>></li>
                        </ul>
                    </div>
                    </div>
				</article>
			</section>
		</form>

		<script src="node_modules/read-excel-file/bundle/read-excel-file.min.js"></script>
		<script src="node_modules/xlsx/dist/xlsx.full.min.js"></script>
        <script src="Librerias/vue/vue.min.js"></script>
		<script src="Librerias/vue/axios.min.js"></script>
		<script type="module" src="tesoreria/otros_tributos/buscar/teso-ingresosOtrosTributosBuscar.js?<?= date('d_m_Y_h_i_s');?>"></script>

	</body>
</html>
