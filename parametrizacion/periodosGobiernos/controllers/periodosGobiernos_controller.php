<?php
    require_once '../../../Librerias/core/Helpers.php';
    require_once '../models/periodosGobierno_model.php';
    session_start();

    class periodoController extends periodosModel {
        public function save() {
            if (!empty($_SESSION)) {
                $vigIni = $_POST["vigIni"];
                $vigFin = $_POST["vigFin"];

                $resp = $this->insertData($vigIni, $vigFin);
                if ($resp > 0) {
                    $arrResponse = array("status" => true, "msg" => "Guardado con exito", "id" => $resp);
                }
                else {
                    $arrResponse = array("status" => false, "msg" => "Error en guardado, intente nuevamente");
                }
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }

            die();
        }

        public function getDataEdit() {
            if (!empty($_SESSION)) {
                $id = $_POST["id"];
                $arrResponse = array("status" => true, "data" => $this->searchDataEdit($id), "consecutivos" => $this->consecutivos());

                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
            die();
        }

        public function saveEdit() {
            if (!empty($_SESSION)) {
                $id = $_POST["id"];
                $vigIni = $_POST["vigIni"];
                $vigFin = $_POST["vigFin"];

                $resp = $this->updateEdit($id, $vigIni, $vigFin);
                if ($resp > 0) {
                    $arrResponse = array("status" => true, "msg" => "Actualizado con exito");
                }
                else {
                    $arrResponse = array("status" => false, "msg" => "Error en guardado, intente nuevamente");
                }
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
            die();
        }

        public function getSearch() {
            if (!empty($_SESSION)) {
                $arrResponse = array("status" => true, "data" => $this->searchData());
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
            die();
        }

        public function changeStatus() {
            if (!empty($_SESSION)) {
                $id = $_POST["id"];
                $estado = $_POST["estado"] == 'S' ? 'N' : 'S';
                $resp = $this->updateStatus($id, $estado);
                if ($resp > 0) {
                    $arrResponse = array("status" => true);
                }
                else {
                    $arrResponse = array("status" => true, "msg" => "Error, vuelva a intentarlo");
                }
                
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
            die();
        }
    }

    if($_POST){
        $obj = new periodoController();

        switch ($_POST["action"]) {
            case 'save':
                $obj->save();
                break;

            case 'get_data_edit':
                $obj->getDataEdit();
                break;

            case 'saveEdit':
                $obj->saveEdit();
                break;

            case 'getDataSearch':
                $obj->getSearch();
                break;
            case 'changeStatus':
                $obj->changeStatus();
                break;
            default:
                $arrResponse = ["status" => false, "msg" => "Acción no encontrada"];
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
                break;
        }
    }
?>