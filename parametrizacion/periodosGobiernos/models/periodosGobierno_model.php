<?php
    require_once '../../../Librerias/core/Helpers.php';
    require_once '../../../Librerias/core/Mysql.php';
    session_start();
    
    class periodosModel extends Mysql {
        public function insertData(string $vigIni, string $vigFin) {
            $sql = "INSERT INTO para_periodos_gobierno (id, vigencia_inicial, vigencia_final, estado) VALUES (?, ?, ? , ?)";
            $consecutivo = searchConsec('para_periodos_gobierno', 'id');
            $values = [$consecutivo, "$vigIni", "$vigFin", 'S'];
            $resp = $this->insert($sql, $values);
            return $resp;
        }

        public function consecutivos() {
            $sql = "SELECT id FROM para_periodos_gobierno";
            $data = $this->select_all($sql);
            return $data;
        }

        public function searchDataEdit(int $id) {
            $sql = "SELECT vigencia_inicial, vigencia_final, estado FROM para_periodos_gobierno WHERE id = $id";
            $data = $this->select($sql);
            return $data;
        }

        public function updateEdit(int $id, string $vigIni, string $vigFin) {
            $sql = "UPDATE para_periodos_gobierno SET vigencia_inicial = ?, vigencia_final = ? WHERE id = $id";
            $resp = $this->update($sql, ["$vigIni", "$vigFin"]);
            return $resp;
        }

        public function searchData() {
            $sql = "SELECT id, vigencia_inicial, vigencia_final, estado FROM para_periodos_gobierno ORDER BY id";
            $data = $this->select_all($sql);
            foreach ($data as $key => $d) {
                $data[$key]["is_status"] = $d["estado"] == 'S' ? 1 : 0;
            }
            return $data;
        }

        public function updateStatus(int $id, string $estado) {
            $sql = "UPDATE para_periodos_gobierno SET estado = ? WHERE id = $id";
            $resp = $this->update($sql, ["$estado"]);
            return $resp;
        }
    }
?>