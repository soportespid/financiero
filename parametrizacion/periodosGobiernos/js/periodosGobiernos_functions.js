const URL = 'parametrizacion/periodosGobiernos/controllers/periodosGobiernos_controller.php';

var app = new Vue({
    el:"#myapp",
    data() {
        return {
            isLoading: false,
            vigenciaIni: '',
            vigenciaFin: '',
            minYear: 2000,
            maxYear: 2099,
            dataSearch: [],
            dataSearch_copy: [],
            txtSearch: '',
            txtResultados: 0,
            id: 0, 
            arrConsecutivos: [],
            estado: '',
        }
    },
    mounted() {
        this.intPageVal = this.$refs.pageType.value;

        if (this.intPageVal == 2) {
            this.getDataSearch();
        } else if (this.intPageVal == 3) {
            this.getDataEdit();
        }
    },
    methods: {
        validateYear(type) {
            let year;
            type == 1 ? year = this.vigenciaIni : year = this.vigenciaFin;

            if (year < this.minYear || year > this.maxYear) {
                Swal.fire("Atención!","El año debe ser entre " + this.minYear + " y " + this.maxYear,"warning");
                type == 1 ? this.vigenciaIni = '' : this.vigenciaFin = '';
            }
        },


        //guardado del crear
        async save() {
            if (this.vigenciaIni == "" && this.vigenciaFin == "") {
                Swal.fire("Atención!","Todos los campos con (*) son obligatorios","warning");
                return false;
            }

            if (parseInt(this.vigenciaIni) > parseInt(this.vigenciaFin)) {
                Swal.fire("Atención!","La vigencia inicial no puede ser menor a la vigencia final","warning");
                return false;
            }

            const formData = new FormData();
            formData.append("action","save");
            formData.append("vigIni", this.vigenciaIni);
            formData.append("vigFin", this.vigenciaFin);

            Swal.fire({
                title:"¿Estás segur@ de guardar?",
                text:"",
                icon: 'warning',
                showCancelButton:true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText:"Sí, guardar",
                cancelButtonText:"No, cancelar"
            }).then(async function(result) {
                if(result.isConfirmed) {
                    const response = await fetch(URL,{method:"POST",body:formData});
                    const objData = await response.json();
                    if (objData.status) {
                        Swal.fire("Guardado",objData.msg,"success");
                        setTimeout(function(){
                            location.href="para-editaPeriodosGobiernos?id="+objData.id;
                        },1000);
                    }
                    else {
                        Swal.fire("Error!",objData.msg,"warning");
                    }
                }
            });
        },

        //guardado del editar

        async saveEdit(){
            if (this.vigenciaIni == "" && this.vigenciaFin == "") {
                Swal.fire("Atención!","Todos los campos con (*) son obligatorios","warning");
                return false;
            }

            if (parseInt(this.vigenciaIni) > parseInt(this.vigenciaFin)) {
                Swal.fire("Atención!","La vigencia inicial no puede ser menor a la vigencia final","warning");
                return false;
            }

            const formData = new FormData();
            formData.append("action","saveEdit");
            formData.append("id", this.id);
            formData.append("vigIni", this.vigenciaIni);
            formData.append("vigFin", this.vigenciaFin);

            Swal.fire({
                title:"¿Estás segur@ de guardar?",
                text:"",
                icon: 'warning',
                showCancelButton:true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText:"Sí, guardar",
                cancelButtonText:"No, cancelar"
            }).then(async function(result) {
                if(result.isConfirmed) {
                    const response = await fetch(URL,{method:"POST",body:formData});
                    const objData = await response.json();
                    if (objData.status) {
                        Swal.fire("Guardado",objData.msg,"success");
                    }
                    else {
                        Swal.fire("Error!",objData.msg,"warning");
                    }
                }
            });
        },

        //trae datos del buscar
        async getDataSearch() {
            const formData = new FormData();
            formData.append("action","getDataSearch");
            this.isLoading = true;
            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();
            this.isLoading = false;
            this.dataSearch = objData.data;
            this.dataSearch_copy = objData.data;
            this.txtResultados = this.dataSearch.length;
        },

        async getDataEdit() {
            this.id = new URLSearchParams(window.location.search).get('id');
            if (this.id > 0) {
                const formData = new FormData();
                formData.append("action","get_data_edit");
                formData.append("id",this.id);
                const response = await fetch(URL,{method:"POST",body:formData});
                const objData = await response.json();
                this.vigenciaIni = objData.data.vigencia_inicial;
                this.vigenciaFin = objData.data.vigencia_final;
                this.estado = objData.data.estado;
                this.arrConsecutivos = objData.consecutivos;
            }
        },

        async searchData() {
            let search = "";
            search = this.txtSearch.toLowerCase();
            
            this.dataSearch= [...this.dataSearch_copy.filter(e=>e.id.toLowerCase().includes(search))];
            this.txtResultados = this.dataSearch.length;
        },

        nextItem:function(type){
            let id = this.id;
            let index = this.arrConsecutivos.findIndex(function(e){return e.id == id});
            if(type=="next" && this.arrConsecutivos[++index]){
                id = this.arrConsecutivos[index++].id;
            }else if(type=="prev" && this.arrConsecutivos[--index]){
                id = this.arrConsecutivos[index--].id;
            }
            window.location.href="para-editaPeriodosGobiernos"+'?id='+id;
        },

        async changeStatus(data, index) {
            const formData = new FormData();
            formData.append("action","changeStatus");
            formData.append("id", data.id);
            formData.append("estado", data.estado);
            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();

            if (objData.status) {
                let estado = data.estado == 'S' ? 'N' : 'S'
                let status = data.is_status == 1 ? 0 : 1;
                this.dataSearch[index].estado = this.dataSearch_copy[index].estado = estado;
                this.dataSearch[index].is_status = this.dataSearch_copy[index].is_status = status;
                
                Swal.fire("Estado actualizado","","success");
            }
            else {
                Swal.fire("Error!",objData.msg,"warning");
            }
        }
    },
    computed:{

    }
})
