<?php //V 1000 12/12/16 ?> 
<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require "comun.inc";
	require "funciones.inc";
	session_start();
	$linkbd=conectar_v7();	
	$linkbd -> set_charset("utf8");		
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es">
	<head>
	 	<meta http-equiv="Content-Type" content="text/html" charset="iso-8859-1"/>
        <meta http-equiv="X-UA-Compatible" content="IE=9"/>
		<title>:: IDEAL 10 - Tesorer&iacute;a</title>
        <link href="css/css2.css" rel="stylesheet" type="text/css" />
        <link href="css/css3.css" rel="stylesheet" type="text/css" />
		<script src="css/programas.js"></script>
        <script src="css/calendario.js"></script>
		<script>
			function eliminar(idr)
			{
				if (confirm("Esta Seguro de Eliminar el Pago a Terceros "+idr))
  				{
  					document.form2.oculto.value=2;
					document.form2.var1.value=idr;
					document.form2.submit();
  				}
			}
            function verUltimaPos(id){
                location.href="teso-editapagoterceros.php?idpago="+id;
            }
			function buscarbotonfiltro()
            {
                if((document.form2.fechaini.value != "" && document.form2.fechafin.value == "") || (document.form2.fechaini.value == "" && document.form2.fechafin.value != "")){
                    alert("Falta digitar fecha");
                }else{
                    document.getElementById('numpos').value=0;
                    document.getElementById('nummul').value=0;
                    document.form2.submit();
                }   
            }
			function cambionumero()
			{
				document.getElementById('numres').value=document.getElementById('renumres').value;
				document.getElementById('nummul').value=0;
				document.getElementById('numpos').value=0;
				document.getElementById('oculto').value=1;
				document.form2.submit(); 
			}
			function numsig()
			{
				document.getElementById('nummul').value=parseInt(document.getElementById('nummul').value)+1;
				document.getElementById('numpos').value=parseInt(document.getElementById('numres').value)*parseInt(document.getElementById('nummul').value);
				document.getElementById('oculto').value=1;
				document.form2.submit(); 
			}
			function numant()
			{
				document.getElementById('nummul').value=parseInt(document.getElementById('nummul').value)-1;
				document.getElementById('numpos').value=parseInt(document.getElementById('numres').value)*parseInt(document.getElementById('nummul').value);
				document.getElementById('oculto').value=1;
				document.form2.submit(); 
			}
			function saltopag(pag)
			{
				document.getElementById('nummul').value=parseInt(pag)-1;
				document.getElementById('numpos').value=parseInt(document.getElementById('numres').value)*parseInt(document.getElementById('nummul').value);
				document.getElementById('oculto').value=1;
				document.form2.submit();
			}
			function crearexcel(){
				document.form2.action="teso-recaudoterceroexcel.php";
				document.form2.target="_BLANK";
				document.form2.submit();
				document.form2.action="";
				document.form2.target="";
			}
		</script>
		<?php titlepag();?>
		<?php 
			if(isset($_GET['fini']) && isset($_GET['ffin'])){
				if(!empty($_GET['fini']) && !empty($_GET['ffin'])){
					$_POST['fecha']=$_GET['fini'];
					$_POST['fecha2']=$_GET['ffin'];
				}
			}
			$fech1=str_split("/",$_POST['fechaini']);
			$fech2=str_split("/",$_POST['fechafin']);
			$f1=$fech1[2]."-".$fech1[1]."-".$fech1[0];
			$f2=$fech2[2]."-".$fech2[1]."-".$fech2[0];
		?>
	</head>
	<body>
		<IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
		<span id="todastablas2"></span>
		<table>
			<tr><script>barra_imagenes("teso");</script><?php cuadro_titulos();?></tr>	 
			<tr><?php menu_desplegable("teso");?></tr>
			<tr>
  				<td colspan="3" class="cinta">
					<a onClick="location.href='teso-pagoterceros.php'" class="mgbt"><img src="imagenes/add.png" title="Nuevo" /></a>
					<a class="mgbt1"><img src="imagenes/guardad.png"/></a>
					<a class="mgbt"><img src="imagenes/buscad.png"/></a>
					<a onClick="mypop=window.open('plan-agenda.php','','');mypop.focus()" class="mgbt"><img src="imagenes/agenda1.png" title="Agenda"/></a>
					<a class="mgbt" onClick="<?php echo paginasnuevas("teso");?>"><img src="imagenes/nv.png" title="Nueva ventana"></a>
					<a onClick="mypop=window.open('<?php echo $url2; ?>','','');mypop.focus();" class="mgbt"><img src="imagenes/duplicar_pantalla.png" title="Duplicar pesta&ntilde;a"></a>
					<a onclick="crearexcel()" class="mgbt"><img src="imagenes/excel.png" title="Excel"></a>
				</td>
       		</tr>	
		</table>
 		<form name="form2" method="post" action="teso-buscapagoterceros.php">
			<?php 
				if($_GET['numpag']!="")
				{
					$oculto=$_POST['oculto'];
					if($oculto!=2)
					{
					$_POST['numres']=$_GET['limreg'];
					$_POST['numpos']=$_GET['limreg']*($_GET['numpag']-1);
					$_POST['nummul']=$_GET['numpag']-1;
					}
				}
				else
				{
					if($_POST['nummul']=="")
					{
						$_POST['numres']=10;
						$_POST['numpos']=0;
						$_POST['nummul']=0;
					}
				}
			?>

			<table  class="inicio" align="center" >
      			<tr >
        			<td class="titulos" colspan="10">:. Buscar Pago Terceros</td>
        			<td class="cerrar" style="width:7%;"><a onClick="location.href='teso-principal.php'">&nbsp;Cerrar</a></td>
      			</tr>
      			<tr >
				  	<td class="tamano01" style='width:4cm;'>Numero pago:</td>
					<td ><input type="search" name="numero" id="numero" value="<?php echo $_POST['numero'];?>" style='width:100%;height:30px;'/></td>
					<td class="tamano01" style='width:4cm;'>Concepto pago:</td>
					<td ><input type="search" name="nombre" id="nombre" value="<?php echo $_POST['nombre'];?>" style='width:100%;height:30px;'/></td>
                    <td class="saludo1" style="width:2cm;">Fecha inicial:</td>
                    <td style="width:10%;"><input name="fechaini"  type="text" value="<?php echo $_POST['fechaini']?>" maxlength="10" onchange="" onKeyPress="javascript:return solonumeros(event)" onKeyUp="return tabular(event,this)"  id="fc_1198971545" onKeyDown="mascara(this,'/',patron,true)" style="width:75%;height:30px;" title="DD/MM/YYYY" placeholder="DD/MM/YYYY"/>&nbsp;<img src="imagenes/calendario04.png" onClick="displayCalendarFor('fc_1198971545');" title="Calendario" class="icobut"/></td>
                    <td class="saludo1" style="width:2cm;">Fecha final:</td>
                    <td style="width:10%;"><input name="fechafin" type="text" value="<?php echo $_POST['fechafin']?>" maxlength="10" onchange="" onKeyPress="javascript:return solonumeros(event)" onKeyUp="return tabular(event,this)"  id="fc_1198971546" onKeyDown="mascara(this,'/',patron,true)" style="width:75%;height:30px;" title="DD/MM/YYYY" placeholder="DD/MM/YYYY"/>&nbsp;<img src="imagenes/calendario04.png" onClick="displayCalendarFor('fc_1198971546');" title="Calendario" class="icobut"/></td>
					<td style="padding-bottom:0px"><em class="botonflecha" onClick="limbusquedas();">Buscar</em></td>
				</tr>                     
   			</table> 

			<input type="hidden" name="oculto" id="oculto" value="1"/>
            <input type="hidden" name="var1" value=<?php echo $_POST['var1'];?>/>
			<input type="hidden" name="fecham1"  id="fecham1" value="<?php echo $_POST['fecham1']; ?>"/>
			<input type="hidden" name="fecham2" id="fecham2" value="<?php echo $_POST['fecham2']; ?>"/>
			<input type="hidden" name="numres" id="numres" value="<?php echo $_POST['numres'];?>"/>
			<input type="hidden" name="numpos" id="numpos" value="<?php echo $_POST['numpos'];?>"/>
			<input type="hidden" name="nummul" id="nummul" value="<?php echo $_POST['nummul'];?>"/>

    		<div class="subpantallap" style="height:68.5%; width:99.6%; overflow-x:hidden;">
				<?php
                    $oculto=$_POST['oculto'];

                    if($_POST['oculto']==2)
                    {
                        $sqlr="select * from tesopagoterceros where id_pago=$_POST[var1]";
                        $resp = mysqli_query($linkbd,$sqlr);
                        $row=mysqli_fetch_row($resp);
                        //********Comprobante contable en 000000000000
                        $sqlr="update comprobante_cab set total_debito=0,total_credito=0,estado='0' where numerotipo=$row[0] and tipo_comp=12";
                        mysqli_query($linkbd,$sqlr);
                        //$sqlr="update comprobante_det set valdebito=0,valcredito=0 where id_comp='12 $row[0]'";
                        // mysql_query($sqlr,$linkbd);	
                        //******** RECIBO DE CAJA ANULAR 'N'	 
                        $sqlr="update tesopagoterceros set estado='N' where id_pago=$row[0]";
                        mysqli_query($linkbd,$sqlr);
                        //echo $sqlr;
                        /*$sqlr="select * from pptoingtranppto where idrecibo=$row[0]";
                        $resp=mysql_query($sqlr,$linkbd);
                        while($r=mysql_fetch_row($resp))
                        {
                            $sqlr="update pptocuentaspptoinicial set ingresos=ingresos-$r[3] where cuenta='$r[1]'";
                            mysql_query($sqlr,$linkbd);
                        }	
                        $sqlr="delete from pptoingtranppto where idrecibo=$row[0]";
                        $resp=mysql_query($sqlr,$linkbd); */
                    }
                   
					$crit1=" ";
					$crit2=" ";
					$crit3=" ";

					if ($_POST['numero']!="")
					{
						$crit1=" AND tesopagoterceros.id_pago LIKE '".$_POST['numero']."' ";
					}

					if ($_POST['nombre']!="")
					{
						$crit2=" AND tesopagoterceros.concepto LIKE '".$_POST['nombre']."'  ";
					}

					if($_POST['fechaini']!='')
					{
						preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/",$_POST['fechaini'],$fecha);
						$fechai="$fecha[3]-$fecha[2]-$fecha[1]";
						if($_POST['fechafin']!='')
						{
							preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/",$_POST['fechafin'],$fecha);
							$fechaf="$fecha[3]-$fecha[2]-$fecha[1]";
							$crit3 = "AND fecha BETWEEN '$fechai' AND '$fechaf'";
						}
						else
						{
							$fechaf=date("Y-m-d");
							$crit3 = "AND fecha BETWEEN '$fechai' AND '$fechaf'";
						}
					}

					//sacar el consecutivo 
					//$sqlr="select *from pptosideforigen where".$crit1.$crit2." order by pptosideforigen.codigo";

					$sqlr="SELECT * FROM tesopagoterceros WHERE tesopagoterceros.id_pago>-1 $crit1 $crit2 $crit3 ORDER BY tesopagoterceros.id_pago DESC";
					$resp = mysqli_query($linkbd,$sqlr);
					$ntr = mysqli_num_rows($resp);
					
					$_POST['numtop']=$ntr;

					$nuncilumnas=ceil($_POST['numtop']/$_POST['numres']);
					
					$cond2="";

					if ($_POST['numres'] != "-1"){ $cond2="LIMIT $_POST[numpos], $_POST[numres]";}

					$sqlr="SELECT * FROM tesopagoterceros WHERE tesopagoterceros.id_pago>-1 $crit1 $crit2 $crit3 ORDER BY tesopagoterceros.id_pago DESC $cond2";
					$resp = mysqli_query($linkbd,$sqlr);
					
					$numcontrol=$_POST['nummul']+1;
                   
					if(($nuncilumnas==$numcontrol)||($_POST['numres']=="-1")){
						$imagenforward="<img src='imagenes/forward02.png' style='width:17px'>";
						$imagensforward="<img src='imagenes/skip_forward02.png' style='width:16px' >";
					}
					else{
						$imagenforward="<img src='imagenes/forward01.png' style='width:17px' title='Siguiente' onClick='numsig()'>";
						$imagensforward="<img src='imagenes/skip_forward01.png' style='width:16px' title='Fin' onClick='saltopag(\"$nuncilumnas\")'>";
					}
					if(($_POST['numpos']==0)||($_POST['numres']=="-1")){
						$imagenback="<img src='imagenes/back02.png' style='width:17px'>";
						$imagensback="<img src='imagenes/skip_back02.png' style='width:16px'>";
					}
					else{
						$imagenback="<img src='imagenes/back01.png' style='width:17px' title='Anterior' onClick='numant();'>";
						$imagensback="<img src='imagenes/skip_back01.png' style='width:16px' title='Inicio' onClick='saltopag(\"1\")'>";
					}

					$con=1;
					
					echo "
					<table class='inicio' align='center' >
					<tr>
						<td colspan='7' class='titulos'>.: Resultados Busqueda:</td>
						<td class='submenu'>
							<select name='renumres' id='renumres' onChange='cambionumero();' style='width:100%'>
								<option value='10'"; if ($_POST['renumres']=='10'){echo 'selected';} echo ">10</option>
								<option value='20'"; if ($_POST['renumres']=='20'){echo 'selected';} echo ">20</option>
								<option value='30'"; if ($_POST['renumres']=='30'){echo 'selected';} echo ">30</option>
								<option value='50'"; if ($_POST['renumres']=='50'){echo 'selected';} echo ">50</option>
								<option value='100'"; if ($_POST['renumres']=='100'){echo 'selected';} echo ">100</option>
								<option value='-1'"; if ($_POST['renumres']=='-1'){echo 'selected';} echo ">Todos</option>
							</select>
                        </td>
					</tr>
					<tr><td colspan='2' id='RecEnc'>Recaudos encontrados: $ntr2</td></tr>
					<tr>
						<td  class='titulos2'>Codigo</td>
						<td class='titulos2'>Concepto</td>
						<td class='titulos2'>Fecha</td>
						<td class='titulos2'>Contribuyente</td>
						<td class='titulos2'>Valor</td>
						<td class='titulos2'>Estado</td>
						<td class='titulos2' width='5%'><center>Anular</td>
						<td class='titulos2' width='5%'><center>Ver</td>
					</tr>";	
					$iter='zebra1';
					$iter2='zebra2';

					if($_POST['fechaini'] == '' && $_POST['fechafin'] == '' && $_POST['numero'] == '' && $_POST['nombre'] == '')
					{
						echo "
						<table class='inicio'>
							<tr>
								<td class='saludo1' style='text-align:center;width:100%;font-size:25px'>Utilice el filtro de busqueda</td>
							</tr>
						</table>";
						$nuncilumnas = 0;
					}
					elseif(mysqli_num_rows($resp) == 0 || mysqli_num_rows($resp) == '0')
					{
						echo "
						<table class='inicio'>
							<tr>
								<td class='saludo1' style='text-align:center;width:100%;font-size:25px'>No hay resultados de su busqueda.</td>
							</tr>
						</table>";
					}
					else
					{
						while ($row =mysqli_fetch_row($resp)) 
						{
							$ntr2 = $ntr;

							echo "<script>document.getElementById('RecEnc').innerHTML = 'Recaudos encontrados: $ntr2'</script>";

							$sqlr1="select sum(valor) from tesopagoterceros_det where id_pago='$row[0]'";
							$res1 = mysqli_query($linkbd,$sqlr1);
							$r1 = mysqli_fetch_row($res1);
							if($_GET['idt']==$row[0]){
								$estilo='background-color:#FF9';
							}
							else{
								$estilo="";
							}
							$nter=buscatercero($row[1]);
							if($row[9]=='S')
								$imgsem="src='imagenes/sema_verdeON.jpg' title='Activo'"; 	 				  
							if($row[9]=='N')
								$imgsem="src='imagenes/sema_rojoON.jpg' title='Inactivo'"; 	

							echo"

							<input type='hidden' name='codigoE[]' value='".$row[0]."'>
							<input type='hidden' name='conceptoE[]' value='".$row[7]."'>
							<input type='hidden' name='fechaE[]' value='".$row[10]."'>
							<input type='hidden' name='nContribuyenteE[]' value='".$nter."'>
							<input type='hidden' name='valorE[]' value='".number_format($r1[0],2)."'>
							";

							if ($row[9]=='S')
							{
								echo"
								<input type='hidden' name='estadoE[]' value='ACTIVO'>";
							}
							if ($row[9]=='N')
							{
								echo"
								<input type='hidden' name='estadoE[]' value='INACTIVO'>";
							}
							if ($row[9]=='R')
							{
								echo"
								<input type='hidden' name='estadoE[]' value='INACTIVO'>";
							}

							echo "
							<tr class='$iter' onDblClick=\"verUltimaPos($row[0])\" style='text-transform:uppercase; $estilo'>
								<td>$row[0]</td>
								<td>$row[7]</td>
								<td>$row[10]</td>
								<td>$nter</td>
								<td>".number_format($r1[0],2)."</td>
								<td style='text-align:center;'><img $imgsem style='width:18px' ></td>";
							if($row[9]=='S')
								{echo "<td style='text-align:center;'><a href='#' onClick=eliminar($row[0])><img src='imagenes/anular.png'></a></td>";}	 
							if($row[9]=='N'){echo "<td></td>";}
							echo "<td style='text-align:center;'><a href='teso-editapagoterceros.php?idpago=$row[0]'><img src='imagenes/buscarep.png'></a></td>
							</tr>";
							$con+=1;
							$aux=$iter;
							$iter=$iter2;
							$iter2=$aux;
							$filas++;	
						}
					}
					echo"</table>
                    <table class='inicio'>
                        <tr>
                            <td style='text-align:center;'>
                                <a href='#'>$imagensback</a>&nbsp;
                                <a href='#'>$imagenback</a>&nbsp;&nbsp;";
                                if($nuncilumnas<=9){$numfin=$nuncilumnas;}
                                else{$numfin=9;}
                                for($xx = 1; $xx <= $numfin; $xx++)
                                {
                                    if($numcontrol<=9){$numx=$xx;}
                                    else{$numx=$xx+($numcontrol-9);}
                                    if($numcontrol==$numx){echo"<a href='#' onClick='saltopag(\"$numx\")'; style='color:#24D915'> $numx </a>";}
                                    else {echo"<a href='#' onClick='saltopag(\"$numx\")'; style='color:#000000'> $numx </a>";}
                                }
                                echo"&nbsp;&nbsp;<a href='#'>$imagenforward</a>
                                    &nbsp;<a href='#'>$imagensforward</a>
                            </td>
                        </tr>
                    </table>";
                	
                ?>
            </div>
		</form> 
	</body>
</html>