<?php 
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require "comun.inc";
	require "funciones.inc";
	session_start();
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html lang="es">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1">
		<meta name="viewport" content="user-scalable=no">
		<title>:: IDEAL10 - Presupuesto CCPET</title>
		<link href="favicon.ico" rel="shortcut icon"/>
        <link href="css/css2.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="css/calendario.js"></script>
		<script>
			var anterior;
			function ponprefijo(pref,opc){alert(pref);
				parent.document.form2.cuenta.value =pref;
				parent.document.form2.ncuenta.value =opc;
				parent.document.form2.cuenta.focus();
				parent.despliegamodal2("hidden");
			} 
		</script> 
		<?php titlepag();?>
	</head>
	<body>
		<?php
		if(!$_POST['tipo'])
			$_POST['tipo'] = $_GET['ti'];
		?>
	<form action="" method="post" enctype="multipart/form-data" name="form1">
		<table class="inicio">
			<tr>
				<td height="25" colspan="4" class="titulos" >Buscar CUENTAS PRESUPUESTALES</td>
				<td class="cerrar"><a onClick="parent.despliegamodal2('hidden');">&nbsp;Cerrar</a></td>
			</tr>

			<tr>
				<td colspan="4" class="titulos2" >:&middot; Por Descripcion </td>
			</tr>

			<tr >
				<td class="saludo1" >:&middot; Numero Cuenta:</td>
				<td  colspan="3"><input name="numero" type="text" size="30">
					<input type="hidden" name="tipo"  id="tipo" value="<?php echo $_POST['tipo']?>">
					<input type="hidden" name="oculto" id="oculto" value="1" >
					<input type="submit" name="Submit" value="Buscar" >
				</td>
			</tr>

			<tr>
				<td colspan="4">&nbsp;</td>
			</tr>

		</table>

		<div class="subpantalla" style="height:73.5%; width:99.6%; overflow-x:hidden;">
			<table class="inicio">
				<tr>
					<td height="25" colspan="5" class="titulos" >Resultados Busqueda </td>
				</tr>
				<tr >
					<td width="32" class="titulos2" >Item</td>
					<td width="76" class="titulos2" >Cuenta </td>
					<td width="140" class="titulos2" >Nombre</td>	  
					<td width="140" class="titulos2" >Padre</td>
					<td width="140" class="titulos2" >Tipo</td>	  	  
				</tr>
				<?php
				$vigusu = vigencia_usuarios($_SESSION['cedulausu']); 
				$oculto = $_POST['oculto'];
				
				//if($oculto == '')
				{
					$cond = "AND (codigo LIKE'%".$_POST['numero']."%' OR nombre LIKE '%".($_POST['numero'])."%')";
					if($_POST['tipo'] == '1'){
						$sqlr = "SELECT codigo, nombre, padre, tipo FROM cuentasingresosccpet WHERE codigo LIKE '%1%' ";
					}
					elseif($_POST['tipo'] == '2'){
						$sqlr = "SELECT codigo, nombre, padre, tipo FROM cuentasccpet WHERE codigo LIKE '%2%' ";
					}
					
					$resp = mysqli_query($linkbd,$sqlr);			
					$co   = 'saludo1a';
					$co2  = 'saludo2';	
					$i = 1;
					
					while ($r = mysqli_fetch_row($resp)){
						echo 
						"<tr class='$co'"; 
						if ($r[3] == 'C')
						{
							echo" onClick=\"ponprefijo('$r[0]','$r[1]')\"";
						} 
						echo">
							<td>$i</td>
							<td>$r[0]</td>
							<td>$r[1]</td>
							<td>$r[2]</td>
							<td>$r[3]</td>
						</tr>";

						$aux = $co;
						$co  = $co2;
						$co2 = $aux;
						$i = 1 + $i;
					}
					$_POST['oculto'] = '';
				}
				?>
			</table>
		</div>
	</form>
</body>
</html> 
