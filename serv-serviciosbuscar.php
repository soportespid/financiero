<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require "comun.inc";
	require "funciones.inc";
	session_start();
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8"); 
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es">
	<head>
		<meta http-equiv="Content-type" content="text/html;" charset="utf8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=9"/>
		<title>:: IDEAL 10 - Servicios P&uacute;blicos</title>
		<link href="css/css2.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<script type="text/javascript" src="jquery-1.11.0.min.js"></script> 
		<script type="text/javascript" src="css/programas.js"></script>
		<script src="sweetalert2/dist/sweetalert2.min.js"></script>
		<link href="css/style.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
		<link rel="stylesheet" href="sweetalert2/dist/sweetalert2.min.css">
		<script>
			function verUltimaPos(idcta, filas, filtro)
			{
				var scrtop=$('#divdet').scrollTop();
				var altura=$('#divdet').height();
				var numpag=$('#nummul').val();
				var limreg=$('#numres').val();
				if((numpag<=0)||(numpag=="")){numpag=0;}
				if((limreg==0)||(limreg=="")){limreg=10;}
				numpag++;
				location.href="serv-servicioseditar.php?idban="+idcta+"&scrtop="+scrtop+"&totreg="+filas+"&altura="+altura+"&numpag="+numpag+"&limreg="+limreg+"&filtro="+filtro;
			}

			function despliegamodalm(_valor,_tip,mensa,pregunta)
			{
				document.getElementById("bgventanamodalm").style.visibility=_valor;
				if(_valor=="hidden"){document.getElementById('ventanam').src="";}
				else
				{
					switch(_tip)
					{
						case "1":	document.getElementById('ventanam').src="ventana-mensaje1.php?titulos="+mensa;break;
						case "2":	document.getElementById('ventanam').src="ventana-mensaje3.php?titulos="+mensa;break;
						case "3":	document.getElementById('ventanam').src="ventana-mensaje2.php?titulos="+mensa;break;
						case "4":	document.getElementById('ventanam').src="ventana-consulta2.php?titulos="+mensa+"&idresp="+pregunta;break;	
					}
				}
			}

			function remove(item)
			{
				Swal.fire({
					title: 'Esta seguro de eliminar?',
					text: "¡No podrás revertir esto!",
					icon: 'warning',
					showCancelButton: true,
					confirmButtonColor: '#3085d6',
					cancelButtonColor: '#d33',
					confirmButtonText: 'Si, eliminarlo!',
					cancelButtonText: 'Cancelar'
					}).then((result) => {
					if (result.isConfirmed) {
						
						document.getElementById('delete').value = item;
                        document.form2.oculto.value = '3';
                        document.form2.submit();
					}
				})
			}
		</script>

		<?php 
			titlepag();

			$scrtop = @$_GET['scrtop'];
			if($scrtop == "") $scrtop=0;

			echo"<script>
				window.onload=function(){
					$('#divdet').scrollTop(".$scrtop.")
				}
			</script>";

			$gidcta = @$_GET['idcta'];

			if(isset($_GET['filtro']))
			$_POST['nombre']=$_GET['filtro'];
		?>
	</head>

	<body>
		<IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
		<span id="todastablas2"></span>
		<table>
			<tr><script>barra_imagenes("serv");</script><?php cuadro_titulos();?></tr>	 
			<tr><?php menu_desplegable("serv");?></tr>
		</table>
		<div class="bg-white group-btn p-1" id="newNavStyle">
			<button type="button" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center" onclick="window.location.href='serv-servicios.php'">
				<span>Nuevo</span>
				<svg viewBox="0 -960 960 960" ><path d="M440-440H200v-80h240v-240h80v240h240v80H520v240h-80v-240Z"/></svg>
			</button>
			<button type="button" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center" onclick="window.location.reload()">
				<span>Buscar</span>
				<svg class="fill-black group-hover:fill-white w-5 h-5" viewBox="0 -960 960 960" ><path d="M784-120 532-372q-30 24-69 38t-83 14q-109 0-184.5-75.5T120-580q0-109 75.5-184.5T380-840q109 0 184.5 75.5T640-580q0 44-14 83t-38 69l252 252-56 56ZM380-400q75 0 127.5-52.5T560-580q0-75-52.5-127.5T380-760q-75 0-127.5 52.5T200-580q0 75 52.5 127.5T380-400Z"/></svg>
			</button>
			<button type="button" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center" onclick="mypop=window.open('serv-principal.php','',''); mypop.focus();">
				<span>Nueva ventana</span>
				<svg class="fill-black group-hover:fill-white w-5 h-5" viewBox="0 -960 960 960" ><path d="M320-120v-80h80v-80H160q-33 0-56.5-23.5T80-360v-400q0-33 23.5-56.5T160-840h640q33 0 56.5 23.5T880-760v400q0 33-23.5 56.5T800-280H560v80h80v80H320ZM160-360h640v-400H160v400Zm0 0v-400 400Z"/></svg>
			</button>
		</div>

		<?php
			if(@ $_POST['oculto'] == "")
			{
				$_POST['numres'] = 10;
				$_POST['numpos'] = 0;
				$_POST['nummul'] = 0;
			}

			if(@ $_GET['numpag'] != "")
			{
				if(@ $_POST['oculto'] != 2)
				{
					$_POST['numres'] = $_GET['limreg'];
					$_POST['numpos'] = $_GET['limreg'] * ($_GET['numpag'] - 1);
					$_POST['nummul'] = $_GET['numpag'] - 1;
				}
			}
			else
			{
				if(@ $_POST['nummul'] == "")
				{
					$_POST['numres'] = 10;
					$_POST['numpos'] = 0;
					$_POST['nummul'] = 0;
				}
			}
		?>

		<div id="bgventanamodalm" class="bgventanamodalm">
			<div id="ventanamodalm" class="ventanamodalm">
				<IFRAME src="" name="ventanam" marginWidth=0 marginHeight=0 frameBorder=0 id="ventanam" frameSpacing=0 style=" width:700px; height:130px; top:200; overflow:hidden;"></IFRAME>
			</div>
		</div>

		<form name="form2" method="post" action="serv-serviciosbuscar.php">

			<input type="hidden" name="numres" id="numres" value="<?php echo @$_POST['numres'];?>"/>
			<input type="hidden" name="numpos" id="numpos" value="<?php echo @$_POST['numpos'];?>"/>
			<input type="hidden" name="nummul" id="nummul" value="<?php echo @$_POST['nummul'];?>"/>
			<input type="hidden" name="oculto" id="oculto" value="1">
			<input type='hidden' name='delete' id='delete' value="<?php echo @$_POST['delete'];?>"/>
			<input type="hidden" name="idestado" id="idestado" value="<?php echo @$_POST['idestado'];?>">

			<?php
				if ($_POST['oculto'] == '3')
				{
					$id = $_POST['delete'];
					$query = "DELETE FROM srvservicios WHERE id = '$id'";
					
					if (mysqli_query($linkbd,$query)) 
					{
						echo "
							<script>
								Swal.fire({
									icon: 'success',
									title: 'Eliminado!',
									text: 'El servicio fue eliminado'
									}).then((result) => {
									if (result.value) {
										document.location.href = 'serv-serviciosbuscar.php';
									} 
								})
							</script>";
					}
				}
			?>

			<table class="inicio">
				<tr>
					<td class="titulos" colspan="3">:: Buscar Servicios</td>
					<td class="cerrar" style="width:7%" onClick="location.href='serv-principal.php'">Cerrar</td>
				</tr>

				<tr>
					<td class="tamano01" style='width:4cm;'>:: C&oacute;digo o Nombre:</td>

					<td>
						<input type="search" name="nombre" id="nombre" value="<?php echo @ $_POST['nombre'];?>" style='width:100%;height:30px;'/>
					</td>

					<td style="padding-bottom:0px">
						<em class="botonflecha" onClick="limbusquedas();">Buscar</em>
					</td>
				</tr> 
			</table>

			<div class="subpantallac5" style="height:69%; width:99.6%; margin-top:0px; overflow-x:hidden" id="divdet">

			<?php
				if (@$_POST['nombre'] != "")
				{
					$crit1="AND concat_ws(' ',id,nombre) LIKE '%".$_POST['nombre']."%'";
				}
				else 
				{
					$crit1='';
				}

				$sqlr = "SELECT * FROM srvservicios WHERE estado = 'S' $crit1";
				$resp = mysqli_query($linkbd,$sqlr);

				$_POST['numtop'] = mysqli_num_rows($resp);
				$nuncilumnas = ceil($_POST['numtop']/$_POST['numres']);
				$cond2 = "";

				if (@$_POST['numres'] != "-1")
				{
					$cond2 = "LIMIT ".$_POST['numpos'].", ".$_POST['numres']; 
				}

				$sqlr = "SELECT id, nombre, unidad_medida, cc FROM srvservicios $crit1 ORDER BY id ASC $cond2";
				$resp = mysqli_query($linkbd,$sqlr);
				
				$con=1;
				$numcontrol = $_POST['nummul'] + 1;

				if(($nuncilumnas == $numcontrol) || (@$_POST['numres'] == "-1"))
				{
					$imagenforward  = "<img src='imagenes/forward02.png' style='width:17px;cursor:default;'>";
					$imagensforward = "<img src='imagenes/skip_forward02.png' style='width:16px;cursor:default;' >";
				}
				else 
				{
					$imagenforward  = "<img src='imagenes/forward01.png' style='width:17px;cursor:pointer;' title='Siguiente' onClick='numsiguiente()'>";
					$imagensforward = "<img src='imagenes/skip_forward01.png' style='width:16px;cursor:pointer;' title='Fin' onClick='saltocol(\"$nuncilumnas\")'>";
				}
				if((@$_POST['numpos'] == 0) || (@$_POST['numres'] == "-1"))
				{
					$imagenback  = "<img src='imagenes/back02.png' style='width:17px;cursor:default;'>";
					$imagensback = "<img src='imagenes/skip_back02.png' style='width:16px;cursor:default;'>";
				}
				else
				{
					$imagenback  = "<img src='imagenes/back01.png' style='width:17px;cursor:pointer;' title='Anterior' onClick='numanterior();'>";
					$imagensback = "<img src='imagenes/skip_back01.png' style='width:16px;cursor:pointer;' title='Inicio' onClick='saltocol(\"1\")'>";
				}
				$con = 1;

				echo "
					<table class='inicio'>
						<tr>
							<td colspan='4' class='titulos'>.: Resultados Busqueda:</td>
							
							<td class='submenu'>
								<select name='renumres' id='renumres' onChange='cambionum();' style='width:100%'>
									<option value='10'"; if (@ $_POST['renumres']=='10'){echo 'selected';} echo ">10</option>
									<option value='20'"; if (@ $_POST['renumres']=='20'){echo 'selected';} echo ">20</option>
									<option value='30'"; if (@ $_POST['renumres']=='30'){echo 'selected';} echo ">30</option>
									<option value='50'"; if (@ $_POST['renumres']=='50'){echo 'selected';} echo ">50</option>
									<option value='100'"; if (@ $_POST['renumres']=='100'){echo 'selected';} echo ">100</option>
									<option value='-1'"; if (@ $_POST['renumres']=='-1'){echo 'selected';} echo ">Todos</option>
								</select>
							</td>
						</tr>

						<tr>
							<td colspan='4'>Servicios Encontrados: ".$_POST['numtop']."</td>
						</tr>

						<tr style='text-align:center;'>
							<td class='titulos2' style='width:5%;'>Código</td>
							<td class='titulos2'>Nombre</td>
							<td class='titulos2'>Unidad de medida</td>
							<td class='titulos2'>Centro de Costos</td>
							<td class='titulos2' style='width:7%;'>Eliminar</td>
						</tr>";

						$iter='saludo1a';
						$iter2='saludo2';
						$filas=1;

						while ($row = mysqli_fetch_row($resp))
						{
							if($gidcta != "")
							{
								if($gidcta == $row[0])
								{
									$estilo = 'background-color:yellow';
								}
								else 
								{
									$estilo = "";
								}
							}

							else
							{
								$estilo = "";
							}

							$idcta = "'$row[0]'";
							$numfil = "'$filas'";
							$filtro = "'".@$_POST['nombre']."'";

							$queryCentroCosto = "SELECT nombre FROM centrocosto WHERE id_cc = '$row[3]'";
							$respCentroCosto = mysqli_query($linkbd,$queryCentroCosto);
							$rowCentroCosto = mysqli_fetch_row($respCentroCosto);

							$centroCosto = $rowCentroCosto[0];

							echo"
							<tr class='$iter' onDblClick=\"verUltimaPos($idcta, $numfil, $filtro)\" style='text-transform:uppercase; text-align:center; $estilo'>
								<td>$row[0]</td>
								<td>$row[1]</td>
								<td>$row[2]</td>
								<td>$centroCosto</td>
								<td style='text-align:center;'>
                                    <a onClick='remove($row[0]);' style='cursor:pointer;'>
                                        <img src='imagenes/newdelete.png' style='height:30px;' title='Eliminar'>
                                    </a>
                                </td>
							</tr>";

							$aux   = $iter;
							$iter  = $iter2;
							$iter2 = $aux;

							$filas++;
						}

						if (@ $_POST['numtop'] == 0)
						{
							echo"

							<table class='inicio'>
								<tr>
									<td class='saludo1' style='text-align:center;width:100%'>
										<img src='imagenes\alert.png' style='width:25px'>No hay coincidencias en la b&uacute;squeda<img src='imagenes\alert.png' style='width:25px'>
									</td>
								</tr>
							</table>";
						}

						echo"
					</table>

					<table class = 'inicio'>
						<tr>
							<td style='text-align:center;'>
								<a>$imagensback</a>&nbsp;
								<a>$imagenback</a>&nbsp;&nbsp;";

								if($nuncilumnas <= 9)
								{
									$numfin = $nuncilumnas;
								}
								else
								{
									$numfin = 9;
								}

								for($xx = 1; $xx <= $numfin; $xx++)
								{
									if($numcontrol <= 9)
									{
										$numx=$xx;
									}
									else
									{
										$numx = $xx + ($numcontrol - 9);
									}

									if($numcontrol == $numx)
									{
										echo"<a onClick='saltocol(\"$numx\")'; style='color:#24D915;cursor:pointer;'> $numx </a>";
									}
									else 
									{
										echo"<a onClick='saltocol(\"$numx\")'; style='color:#000000;cursor:pointer;'> $numx </a>";
									}
								}

				echo"			
								&nbsp;&nbsp;<a>$imagenforward</a>
								&nbsp;<a>$imagensforward</a>
							</td>
						</tr>
					</table>";	
			?>

			<input type="hidden" name="ocules" id="ocules" value="<?php echo @ $_POST['ocules'];?>"/>
			<input type="hidden" name="actdes" id="actdes" value="<?php echo @ $_POST['actdes'];?>"/>
			<input type="hidden" name="numtop" id="numtop" value="<?php echo @ $_POST['numtop'];?>"/>
		</form>

		<div id="bgventanamodal2">
			<div id="ventanamodal2">
				<IFRAME src="" name="buster" marginWidth=0 marginHeight=0 frameBorder=0 id="ventana2" frameSpacing=0 style="left:500px; width:900px; height:500px; top:200;"></IFRAME>
			</div>
		</div>

	</body>
</html>