<?php
require_once ("tcpdf/tcpdf_include.php");
require ('comun.inc');
require "funciones.inc";
date_default_timezone_set("America/Bogota");
session_start();
class MYPDF extends TCPDF
{
    public function Header()
    {
        $linkbd = conectar_v7();
        $linkbd->set_charset("utf8");
        $sqlr = "select *from configbasica where estado='S' ";
        $res = mysqli_query($linkbd, $sqlr);
        while ($row = mysqli_fetch_row($res)) {
            $nit = $row[0];
            $rs = $row[1];
            $nalca = $row[6];
        }
        //Parte Izquierda
        $this->Image('imagenes/escudo.jpg', 13, 13, 20, 20, 'JPG', '', 'T', true, 300, '', false, false, 0, false, false, false);// Logo
        $this->SetFont('helvetica', 'B', 8);
        $this->SetY(10);
        $this->RoundedRect(10, 10, 277, 25, 1, '1111');
        $this->Cell(0.1);
        $this->Cell(26, 25, '', 'R', 0, 'L');
        $this->SetY(8);
        $this->SetX(80);
        $this->SetFont('helvetica', 'B', 9);
        $this->Cell(160, 15, strtoupper("$rs"), 0, 0, 'C');
        $this->SetFont('helvetica', 'B', 7);
        $this->SetY(12);
        $this->SetX(80);
        $this->Cell(160, 15, 'NIT: ' . $nit, 0, 0, 'C');

        $this->SetFont('helvetica', 'B', 9);
        $this->SetY(23);
        $this->SetX(36);
        $this->Cell(251, 12, "Listado Certificados Disponibilidad Presupuestal", 'T', 0, 'C');

        $this->SetFont('helvetica', 'B', 6);

        $this->SetY(10);
        $this->SetX(257);
        $this->Cell(35, 6.8, " FECHA: " . date("d/m/Y"), "L", 0, 'L');
        $this->SetY(17);
        $this->SetX(257);
        $this->Cell(35, 6, " VIGENCIA: " . vigencia_usuarios($_SESSION['cedulausu']), "L", 0, 'L');

        $this->SetFont('times', 'B', 10);
        $this->ln(12);
    }
    public function Footer()
    {
        $linkbd = conectar_v7();
        $linkbd->set_charset("utf8");
        $sqlr = "SELECT direccion,telefono,web,email FROM configbasica WHERE estado='S'";
        $resp = mysqli_query($linkbd, $sqlr);
        $user = $_SESSION['nickusu'];
        $fecha = date("Y-m-d H:i:s");
        $ip = $_SERVER['REMOTE_ADDR'];
        $useri = $_POST['user'];
        while ($row = mysqli_fetch_row($resp)) {
            $direcc = strtoupper($row[0]);
            $telefonos = $row[1];
            $dirweb = strtoupper($row[3]);
            $coemail = strtoupper($row[2]);
        }
        if ($direcc != '') {
            $vardirec = "Dirección: $direcc, ";
        } else {
            $vardirec = "";
        }
        if ($telefonos != '') {
            $vartelef = "Telefonos: $telefonos";
        } else {
            $vartelef = "";
        }
        if ($dirweb != '') {
            $varemail = "Email: $dirweb, ";
        } else {
            $varemail = "";
        }
        if ($coemail != '') {
            $varpagiw = "Pagina Web: $coemail";
        } else {
            $varpagiw = "";
        }
        $this->SetFont('helvetica', 'I', 8);
        $txt = <<<EOD
        $vardirec $vartelef
        $varemail $varpagiw
        EOD;
        $this->SetFont('helvetica', 'I', 6);
        $this->Cell(277, 10, '', 'T', 0, 'T');
        $this->ln(2);
        $this->Write(0, $txt, '', 0, 'C', true, 0, false, false, 0);
        //$this->Cell(25, 10, 'Hecho por: '.$useri, 0, false, 'C', 0, '', 0, false, 'T', 'M');
        $this->Cell(25, 10, 'Impreso por: ' . $user, 0, false, 'C', 0, '', 0, false, 'T', 'M');
        $this->SetX(25);
        $this->Cell(107, 10, 'IP: ' . $ip, 0, false, 'C', 0, '', 0, false, 'T', 'M');
        $this->Cell(35, 10, 'Fecha: ' . $fecha, 0, false, 'C', 0, '', 0, false, 'T', 'M');
        $this->Cell(107, 10, 'IDEAL.10 S.A.S', 0, false, 'C', 0, '', 0, false, 'T', 'M');
        $this->Cell(20, 10, 'Pagina ' . $this->getAliasNumPage() . '/' . $this->getAliasNbPages(), 0, false, 'C', 0, '', 0, false, 'T', 'M');

    }

}
$pdf = new MYPDF('L', 'mm', 'Letter', true, 'UTF-8', false);
$pdf->SetDocInfoUnicode(true);
// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('IDEALSAS');
$pdf->SetTitle('Listado Certificados Disponibilidad Presupuestal');
$pdf->SetMargins(10, 38, 10);// set margins
$pdf->SetHeaderMargin(38);// set margins
$pdf->SetFooterMargin(17);// set margins
$pdf->SetAutoPageBreak(TRUE, 20);// set auto page breaks

$pdf->AddPage();

$y = $pdf->GetY();
$pdf->SetY($y);

$pdf->SetFillColor(222, 222, 222);
$pdf->SetFont('helvetica', 'B', 7);
$pdf->Cell(0.1);
$pdf->Cell(20, 5, 'Vigencia', 0, 0, 'C', 1);

$pdf->SetY($y);
$pdf->Cell(21);
$pdf->Cell(20, 5, 'Número', 0, 0, 'C', 1);

$pdf->SetY($y);
$pdf->Cell(42);
$pdf->Cell(40, 5, 'Valor ', 0, 0, 'C', 1);

$pdf->SetY($y);
$pdf->Cell(83);
$pdf->Cell(70, 5, 'Solicita', 0, 0, 'C', 1);

$pdf->SetY($y);
$pdf->Cell(154);
$pdf->MultiCell(70, 5, 'Objeto', 0, 'C', 1, 1, '', '', true, '', false, true, 0, 'M', true);

$pdf->SetY($y);
$pdf->Cell(225);
$pdf->MultiCell(26, 5, 'Fecha', 0, 'C', 1, 1, '', '', true, '', false, true, 0, 'M', true);

$pdf->SetY($y);
$pdf->Cell(252);
$pdf->MultiCell(25, 5, 'Estado', 0, 'C', 1, 1, '', '', true, '', false, true, 0, 'M', true);

$pdf->SetFont('helvetica', '', 7);
$pdf->ln(1);


$linkbd = conectar_v7();
$linkbd->set_charset("utf8");
$crit1 = " ";
$crit2 = " ";
$crit3 = " ";
$vig = vigencia_usuarios($_SESSION['cedulausu']);
if ($_POST['vigencia'] != "") {
    $crit1 = " AND TB1.vigencia ='$_POST[vigencia]' ";
} else {
    $crit1 = " AND TB1.vigencia ='$vig' ";
}
if ($_POST['numero'] != "") {
    $crit2 = " AND TB1.consvigencia like '%$_POST[numero]%' ";
}
if ($_POST['fechaini'] != "" and $_POST['fechafin'] != "") {
    preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST['fechaini'], $fecha);
    $fechai = $fecha[3] . "-" . $fecha[2] . "-" . $fecha[1];
    preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST['fechafin'], $fecha);
    $fechaf = $fecha[3] . "-" . $fecha[2] . "-" . $fecha[1];
    $crit3 = " AND TB1.fecha between '$fechai' and '$fechaf'  ";
}
$sqlr = "SELECT TB1.* FROM ccpetcdp TB1 WHERE TB1.estado!='A' $crit1 $crit2 $crit3  ORDER BY TB1.consvigencia";
$resp = mysqli_query($linkbd, $sqlr);
while ($row = mysqli_fetch_row($resp)) {
    if ($row[5] == "S") {
        $estado = "Activo";
    } else if ($row[5] == "N") {
        $estado = "Anulado";
    } else if ($row[5] == "C") {
        $estado = "Con Registro";
    }
    if ($con % 2 == 0) {
        $pdf->SetFillColor(255, 255, 255);
    } else {
        $pdf->SetFillColor(245, 245, 245);
    }
    $vv = $pdf->GetY();
    if ($vv >= 180) {
        $pdf->AddPage();
        $vv = $pdf->gety();
    }
    $alturaobjeto = $pdf->getNumLines($row[7], 71);
    $alturasoli = $pdf->getNumLines($row[6], 71);
    $alturamax = max($alturaobjeto, $alturasoli);
    $altura = $alturamax * 3;
    $pdf->cell(0.2);
    $pdf->MultiCell(21, $altura, $row[1], 0, 'C', true, 0, '', '', true, 0, false, true, 0, 'M', true);
    $pdf->MultiCell(21, $altura, $row[2], 0, 'C', true, 0, '', '', true, 0, false, true, 0, 'M', true);
    $pdf->MultiCell(41, $altura, "$ " . number_format((float) $row[4], 2, ",", ".") . " ", 0, 'C', true, 0, '', '', true, 0, false, true, 0, 'M', true);
    $pdf->MultiCell(71, $altura, $row[6], 0, 'C', true, 0, '', '', true, 0, false, true, 0, 'M', true);
    $pdf->MultiCell(71, $altura, $row[7], 0, 'C', true, 0, '', '', true, 0, false, true, 0, 'M', true);
    $pdf->MultiCell(27, $altura, date('d-m-Y', strtotime($row[3])), 0, 'C', true, 0, '', '', true, 0, false, true, 0, 'M', true);
    $pdf->MultiCell(25, $altura, $estado, 0, 'C', true, 1, '', '', true, 0, false, true, 0, 'M', true);
    $con++;
}
$pdf->Output();
