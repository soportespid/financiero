<?php
	require_once 'PHPExcel/Classes/PHPExcel.php';
    include '/PHPExcel/Classes/PHPExcel/IOFactory.php';
	require "comun.inc";
	require "funciones.inc";
    require "Librerias/core/Helpers.php";
    ini_set('max_execution_time',99999999);
    header("Content-type: application/json");
	session_start();
	$linkbd=conectar_v7();

	$servicio = $_POST["servicio"];

	$objPHPExcel = new PHPExcel();
	$total = number_format($_POST["total"], 2);

	$objPHPExcel->getProperties()
		->setCreator("IDEAL10")
		->setLastModifiedBy("IDEAL10")
		->setTitle("Comparativo de fuentes")
		->setSubject("ccp")
		->setDescription("ccp")
		->setKeywords("ccp")
		->setCategory("Presupuesto");
        $objPHPExcel->getActiveSheet()->mergeCells('A1:C1');
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('A1', "PRESUPUESTO TOTAL DE INGRESOS: $".$total);
        $objFont=$objPHPExcel->getActiveSheet()->getStyle('A1')->getFont();
        $objFont=$objPHPExcel->getActiveSheet()->getStyle("A1")->getFont();
        $objFont->setName('Courier New');
        $objFont->setSize(15);
        $objFont->setBold(true);
        $objFont->setUnderline(PHPExcel_Style_Font::UNDERLINE_SINGLE);
        $objFont->getColor()->setARGB( PHPExcel_Style_Color::COLOR_BLACK);
        $objAlign=$objPHPExcel->getActiveSheet()->getStyle("A1")->getAlignment(); 
        $objAlign->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objAlign->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);

        $objPHPExcel-> getActiveSheet ()
            -> getStyle ("A1")	
            -> getFill ()
            -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
            -> getStartColor ()
            -> setRGB ('A6E5F3');
        $borders = array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                    'color' => array('argb' => 'FF000000'),
                )
            ),
        );

    $data = json_decode($_POST['data'], true);

	$i=2;
    
	foreach ($data as $key => $d) {
        if ($d["tipo"] == "T") {
            $objPHPExcel->getSheet(0)
            ->mergeCells("A$i:C$i")    
            ->setCellValue("A$i", "$d[rubro] - $d[nombre]");

            $objFont=$objPHPExcel->getActiveSheet()->getStyle("A$i")->getFont();
            $objFont->setName('Courier New');
            $objFont->setSize(15);
            $objFont->setBold(true);
            $objFont->setUnderline(PHPExcel_Style_Font::UNDERLINE_SINGLE);
            $objFont->getColor()->setARGB( PHPExcel_Style_Color::COLOR_BLACK);
            $objAlign=$objPHPExcel->getActiveSheet()->getStyle("A$i")->getAlignment(); 
            $objAlign->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $objAlign->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);

            $objPHPExcel-> getActiveSheet ()
                -> getStyle ("A$i")	
                -> getFill ()
                -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
                -> getStartColor ()
                -> setRGB ('A6E5F3');
            $borders = array(
                'borders' => array(
                    'allborders' => array(
                        'style' => PHPExcel_Style_Border::BORDER_THIN,
                        'color' => array('argb' => 'FF000000'),
                    )
                ),
            );
        }
        else if ($d["tipo"] == "A") {
            $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValueExplicit ("A$i", $d["rubro"], PHPExcel_Cell_DataType :: TYPE_STRING)
            ->setCellValueExplicit ("B$i", $d["nombre"], PHPExcel_Cell_DataType :: TYPE_STRING)
            ->setCellValueExplicit ("C$i", "$".number_format($d["ingresos"], 2), PHPExcel_Cell_DataType :: TYPE_STRING);

            $objPHPExcel->getActiveSheet()->getStyle("A$i:C$i")->applyFromArray($borders);
            // Aplicar negrilla a la celda A$i
            $objPHPExcel->getActiveSheet()->getStyle("A$i:C$i")->applyFromArray(
                array(
                    'font' => array(
                        'bold' => true
                    )
                )
            );
        } else {
            $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValueExplicit ("A$i", $d["rubro"], PHPExcel_Cell_DataType :: TYPE_STRING)
            ->setCellValueExplicit ("B$i", $d["nombre"], PHPExcel_Cell_DataType :: TYPE_STRING)
            ->setCellValueExplicit ("C$i", "$".number_format($d["ingresos"], 2), PHPExcel_Cell_DataType :: TYPE_STRING);
        }

        $i++;
	}


	$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);

	$objPHPExcel->getActiveSheet()->setTitle('ccp');
	$name = "informe_presupuesto_inicial_ingresos.xlsx";
	//header('Content-Type: application/vnd.ms-excel');
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
	header('Content-Disposition: attachment;filename="'.$name.'"');
	header('Cache-Control: max-age=0');
	$objWriter=PHPExcel_IOFactory::createWriter($objPHPExcel,'Excel2007');
	$objWriter->save('php://output');
	exit;
?>