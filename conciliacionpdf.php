<?php
	require_once("tcpdf/tcpdf_include.php");    
	require('comun.inc');
	session_start();	
	date_default_timezone_set("America/Bogota");
    class MYPDF extends TCPDF 
	{
        public function Header() 
		{
            $linkbd=conectar_v7();
			$linkbd -> set_charset("utf8");
			$sqlr="select *from configbasica where estado='S'";
			$res=mysqli_query($linkbd,$sqlr);
			while($row=mysqli_fetch_row($res))
			{
				$nit=$row[0];
				$rs=$row[1];
				$nalca=$row[6];
			}
			//Parte Izquierda
			$this->Image('imagenes/escudo.jpg', 12, 12, 25, 25, 'JPG', '', 'T', true, 300, '', false, false, 0, false, false, false);// Logo
			$this->SetFont('helvetica','B',8);
			$this->SetY(10);
            $this->RoundedRect(10, 10, 190, 29, 1,'1111' );
			$this->Cell(30,29,'','R',0,'L'); 
			$this->SetY(10);
			$this->SetFont('helvetica','B',12);
			
            $this->SetX(35);
            $this->Cell(149,15,"$rs",0,0,'C');
            $this->SetY(17);

			
			$this->SetX(35);
			$this->SetFont('helvetica','B',11);
			$this->Cell(149,10,"$nit",0,0,'C');
			$this->SetY(21);
			$this->SetX(35);
			$this->Cell(149,14,"CONCILIACION BANCARIA",0,0,'C');
			$this->SetFont('helvetica','',8);
			
        }
        public function Footer() 
		{
            $linkbd = conectar_v7();
			$linkbd -> set_charset("utf8");
			$sqlr="SELECT direccion,telefono,web,email FROM configbasica WHERE estado='S'";
			$resp=mysqli_query($linkbd, $sqlr);
			$user = $_SESSION['nickusu'];	
			$fecha = date("Y-m-d H:i:s");
			$ip = $_SERVER['REMOTE_ADDR'];
			$useri = $_POST['user'];
			while($row=mysqli_fetch_row($resp))
			{
				$direcc=strtoupper($row[0]);
				$telefonos=$row[1];
				$dirweb=strtoupper($row[3]);
				$coemail=strtoupper($row[2]);
			}
			if($direcc!=''){$vardirec="Dirección: $direcc, ";}
			else {$vardirec="";}
			if($telefonos!=''){$vartelef="Telefonos: $telefonos";}
			else{$vartelef="";}
			if($dirweb!=''){$varemail="Email: $dirweb, ";}
			else {$varemail="";}
			if($coemail!=''){$varpagiw="Pagina Web: $coemail";}
			else{$varpagiw="";}
			
			$txt = <<<EOD
			$vardirec $vartelef
			$varemail $varpagiw
			EOD;
			$this->SetFont('helvetica', 'I', 5);
			$this->Cell(190,7,'','T',0,'T');
			$this->ln(1);
			$this->Write(0, $txt , '', 0, 'C', true, 0, false, false, 0);
			//$this->Cell(12, 7, 'Hecho por: '.$useri, 0, false, 'C', 0, '', 0, false, 'T', 'M');
			$this->Cell(15, 7, 'Impreso por: '.$user, 0, false, 'C', 0, '', 0, false, 'T', 'M');
			$this->Cell(57, 7, 'IP: '.$ip, 0, false, 'C', 0, '', 0, false, 'T', 'M');
			$this->Cell(50, 7, 'Fecha: '.$fecha, 0, false, 'C', 0, '', 0, false, 'T', 'M');
			$this->Cell(54, 7, 'IDEAL.10 S.A.S', 0, false, 'C', 0, '', 0, false, 'T', 'M');
			$this->Cell(25, 7, 'Pagina '.$this->getAliasNumPage().'/'.$this->getAliasNbPages(), 0, false, 'C', 0, '', 0, false, 'T', 'M');

        }
    }    
    $pdf = new MYPDF('P','mm','Letter', true, 'utf8', false);
	$pdf->SetDocInfoUnicode (true); 
	// set document information
	$pdf->SetCreator(PDF_CREATOR);
	$pdf->SetAuthor('IDEAL10');
	$pdf->SetTitle('Conciliacion');
	$pdf->SetSubject('conciliacion bancaria');
	$pdf->SetKeywords('conciliacion bancaria');
    $pdf->SetMargins(10, 40, 10);// set margins
    $pdf->SetHeaderMargin(40);// set margins
    $pdf->SetFooterMargin(17);// set margins
	$pdf->SetAutoPageBreak(TRUE, 20);// set auto page breaks
	// set some language-dependent strings (optional)
	if (@file_exists(dirname(__FILE__).'/lang/spa.php')) 
	{
		require_once(dirname(__FILE__).'/lang/spa.php');
		$pdf->setLanguageArray($l);
	}
	$pdf->AddPage();
	$cuent = $_POST['cuenta'].' - '.strtoupper(	$_POST['nbanco']);
    $pdf->SetFillColor(245,245,245);
	$pdf->SetFont('helvetica','B',8);
    $pdf->Cell(24,5,'Cuenta:',0,0,'L',1);
    $pdf->SetFont('helvetica','B',7);
    $pdf->Cell(166,5,$cuent,0,1,'L',0);
	$pdf->ln(1);
	
    $pdf->Cell(0.1);
	$pdf->SetFont('helvetica','B',8);
    $pdf->Cell(24,5,'Fecha Inicial:',0,0,'L',1);
	$pdf->SetFont('helvetica','',7);
    $pdf->Cell(45,5,''.$_POST['fecha'],0,0,'L',0);
    $pdf->SetFont('helvetica','B',8);	
    $pdf->Cell(24,5,'Fecha Final:',0,0,'L',1);
	$pdf->SetFont('helvetica','',7);	
    $pdf->Cell(38,5,''.$_POST['fecha2'],0,0,'L',0);
	$pdf->SetFont('helvetica','B',8);
    $pdf->Cell(32,5,'Saldo Extracto Banco:',0,0,'L',1);
	$pdf->SetFont('helvetica','',7);
    $pdf->Cell(27,5,'$ '.number_format($_POST['extractofis'],2),0,1,'L',0);
	$pdf->ln(1);

    $pdf->Cell(0.1);  
    $pdf->SetFont('helvetica','B',8);
    $pdf->Cell(24,5,'Saldo I Libros:',0,0,'L',1);
    $pdf->SetFont('helvetica','',7);
    $pdf->Cell(45,5,'$ '.number_format($_POST['saldoini'],2),0,0,'L',0);
	$pdf->SetFont('helvetica','B',8);
    $pdf->Cell(24,5,'Saldo F Libros:',0,0,'L',1);
    $pdf->SetFont('helvetica','',7);
    $pdf->Cell(0.1);
    $pdf->Cell(38,6,'$ '.number_format($_POST['saldofin'],2),0,0,'L',0);
	$pdf->SetFont('helvetica','B',8);
    $pdf->Cell(32,5,'Saldo Extracto Calc:',0,0,'L',1);
    $pdf->SetFont('helvetica','',7);
    $pdf->Cell(27,6,'$ '.number_format($_POST['extracto'],2,".",","),0,1,'L',0);

    $pdf->SetFont('helvetica','B',8);
    $pdf->Cell(24,5,'(-)Debitos C:',0,0,'L',1);
    $pdf->SetFont('helvetica','',7);
    $pdf->Cell(45,6,'$ '.number_format($_POST['cdebitos'],2,".",","),0,0,'L',0);
    $pdf->SetFont('helvetica','B',8);
    $pdf->Cell(24,5,'(+)Creditos C:',0,0,'L',1);
    $pdf->SetFont('helvetica','',7);	
    $pdf->Cell(38,6,'$ '.number_format($_POST['ccreditos'],2,".",","),0,0,'L',0);
	$pdf->SetFont('helvetica','B',8);
    $pdf->Cell(32,5,'Diferencia:',0,0,'L',1);
    $pdf->SetFont('helvetica','',7);	
    $pdf->Cell(27,6,'$ '.number_format($_POST['difextracto'],2,".",","),0,1,'L',0);
    
    $pdf->SetFont('helvetica','B',8);
    $pdf->Cell(24,5,'(-)Debitos NC:',0,0,'L',1);
    $pdf->SetFont('helvetica','',7);
    $pdf->Cell(45,6,'$ '.number_format($_POST['debnc'],2,".",","),0,0,'L',0);
    $pdf->SetFont('helvetica','B',8);
    $pdf->Cell(24,5,'(+)Creditos NC:',0,0,'L',1);
    $pdf->SetFont('helvetica','',7);	
    $pdf->Cell(38,6,'$ '.number_format($_POST['crednc'],2,".",","),0,0,'L',0);
	$pdf->SetFont('helvetica','B',8);
	if ($_POST['difextracto'] != '0' or $_POST['extractofis'] == '0'){
		$conci = 'NO CONCILIADO';
	}
	else{
		$conci = 'CONCILIADO';
	}
    $pdf->Cell(59,5,$conci,0,1,'C',1);
	$pdf->RoundedRect(10, 40, 190, 29, 1,'1111' ); 

    $con=0;
	$ch=esta_en_array($_POST['conciliados'], $_POST['detalles'][$y]);
	if($ch!=1)
	{
		$pdf->ln(1);
		$pdf->SetFont('helvetica','B',10);
		$pdf->Cell(190,5,'NO CONCILIADOS:',0,0,'C',0);
		$pdf->ln(7);	
		$yy=$pdf->GetY();	
		$pdf->SetY($yy);
		$yy=$pdf->GetY();	
		$pdf->SetFillColor(222,222,222);
		$pdf->SetFont('helvetica','B',7);
		$pdf->Cell(0.1);
		$pdf->Cell(16,5,'Id',0,0,'C',1); 
		$pdf->SetY($yy);
		$pdf->Cell(17);
		$pdf->Cell(25,5,'Fecha',0,0,'C',1);
		$pdf->SetY($yy);
		$pdf->Cell(43);
		$pdf->Cell(40,5,'Documento',0,0,'C',1);
		$pdf->SetY($yy);
		$pdf->SetFont('helvetica','B',7);
		$pdf->Cell(84);
		$pdf->Cell(30,5,'Tercero',0,0,'C',1);
		$pdf->SetY($yy);
		$pdf->Cell(115);
		$pdf->MultiCell(24,5,'Cheque',0,'C',1,1,'','',true,'',false,true,0,'M',true);
		$pdf->SetY($yy);
		$pdf->Cell(140);
		$pdf->MultiCell(24,5,'Debitos',0,'C',1,1,'','',true,'',false,true,0,'M',true);
		$pdf->SetY($yy);
		$pdf->Cell(165);
		$pdf->MultiCell(25,5,'Creditos',0,'C',1,1,'','',true,'',false,true,0,'M',true);
		$pdf->SetFont('helvetica','',6);
		$pdf->ln(1);
	}
    for($y=0;$y<count($_POST['detalles']);$y++)
	{
        if($vv>=170)
        { 
            $pdf->AddPage();
            $vv=$pdf->gety();
        }
		$ch=esta_en_array($_POST['conciliados'], $_POST['detalles'][$y]);
		if($ch!=1)
		{

			$doc =$_POST['compro2'][$con].' '.$_POST['ncompro'][$con];
			$lineasnombres = $pdf ->getNumLines($doc, 41);
			$lineasnombress = $pdf ->getNumLines($_POST['dterceros'][$con], 31);
			$valorMaxs = max($lineasnombres, $lineasnombress);
			$alturas = $valorMaxs * 3;
			if ($con%2==0){$pdf->SetFillColor(255,255,255);}
			else {$pdf->SetFillColor(245,245,245);}
			$pdf->cell(0.2);
			$pdf->MultiCell(16,$alturas,strtoupper(''.$_POST['detalles'][$con]),0,'C',true,0,'','',true,0,false,true,0,'M',true);
			$pdf->MultiCell(26,$alturas,''.$_POST['dfechas'][$con],0,'C',true,0,'','',true,0,false,true,0,'M',true);
			$pdf->MultiCell(41,$alturas,$_POST['compro2'][$con].' '.$_POST['ncompro'][$con],0,'L',true,0,'','',true,0,false,true,0,'M',true);
			$pdf->MultiCell(31,$alturas,substr(''.$_POST['dterceros'][$con],0,21),0,'L',true,0,'','',true,0,false,true,0,'M',true);
			$pdf->MultiCell(25,$alturas,''.$_POST['dcheques'][$con],0,'C',true,0,'','',true,0,false,true,0,'M',true);
			$pdf->MultiCell(25,$alturas,'$'.number_format($_POST['debitos'][$con],2),0,'R',true,0,'','',true,0,false,true,0,'M',true);
			$pdf->MultiCell(26,$alturas,'$'.number_format($_POST['creditos'][$con],2),0,'R',true,1,'','',true,0,false,true,0,'M',true); 
		}
		$con++;
	}
    $pdf->ln(2);
    $pdf->Cell(140,5,'Total','T',0,'R');
    $pdf->SetX(150);
	$pdf->Cell(25,5,'$'.number_format($_POST['debnc'],2,".",","),'T',0,'C');
	$pdf->Cell(26,5,'$'.number_format($_POST['crednc'],2,".",","),'T',0,'C');
	$pdf->ln(5);
	$checkbox = ($_POST['cbox2'] == 'on') ? '1' : '0';
	if ($checkbox == '1'){
		
			$pdf->SetFont('helvetica','B',10);
			$pdf->Cell(190,5,'CONCILIADOS:','T',0,'C',0);
			$pdf->ln(7);	
			$yyy=$pdf->GetY();	
			$pdf->SetY($yyy);
			$yyy=$pdf->GetY();	
			$pdf->SetFillColor(222,222,222);
			$pdf->SetFont('helvetica','B',7);
			$pdf->Cell(0.1);
			$pdf->Cell(16,5,'Id',0,0,'C',1); 
			$pdf->SetY($yyy);
			$pdf->Cell(17);
			$pdf->Cell(25,5,'Fecha',0,0,'C',1);
			$pdf->SetY($yyy);
			$pdf->Cell(43);
			$pdf->Cell(40,5,'Documento',0,0,'C',1);
			$pdf->SetY($yyy);
			$pdf->SetFont('helvetica','B',7);
			$pdf->Cell(84);
			$pdf->Cell(30,5,'Tercero',0,0,'C',1);
			$pdf->SetY($yyy);
			$pdf->Cell(115);
			$pdf->MultiCell(24,5,'Cheque',0,'C',1,1,'','',true,'',false,true,0,'M',true);
			$pdf->SetY($yyy);
			$pdf->Cell(140);
			$pdf->MultiCell(24,5,'Debitos',0,'C',1,1,'','',true,'',false,true,0,'M',true);
			$pdf->SetY($yyy);
			$pdf->Cell(165);
			$pdf->MultiCell(25,5,'Creditos',0,'C',1,1,'','',true,'',false,true,0,'M',true);
			$pdf->SetFont('helvetica','',6);
			$pdf->ln(1);
		
		$con = 0;
		$tam =count($_POST['detalles']);
		for($yz=0;$yz<$tam;$yz++)
		{
			if($vv>=170)
			{ 
				$pdf->AddPage();
				$vv=$pdf->gety();
			}
			$ch=esta_en_array($_POST['conciliados'], $_POST['detalles'][$yz]);
			if($ch!=0)
			{
	
				$doc =$_POST['compro2'][$con].' '.$_POST['ncompro'][$con];
				$lineasnombres = $pdf ->getNumLines($doc, 41);
				$lineasnombress = $pdf ->getNumLines($_POST['dterceros'][$con], 31);
				$valorMaxs = max($lineasnombres, $lineasnombress);
				$alturas = $valorMaxs * 3;
				if ($con%2==0){$pdf->SetFillColor(255,255,255);}
				else {$pdf->SetFillColor(245,245,245);}
				$pdf->cell(0.2);
				$pdf->MultiCell(16,$alturas,strtoupper(''.$_POST['detalles'][$con]),0,'C',true,0,'','',true,0,false,true,0,'M',true);
				$pdf->MultiCell(26,$alturas,''.$_POST['dfechas'][$con],0,'C',true,0,'','',true,0,false,true,0,'M',true);
				$pdf->MultiCell(41,$alturas,$_POST['compro2'][$con].' '.$_POST['ncompro'][$con],0,'L',true,0,'','',true,0,false,true,0,'M',true);
				$pdf->MultiCell(31,$alturas,substr(''.$_POST['dterceros'][$con],0,21),0,'L',true,0,'','',true,0,false,true,0,'M',true);
				$pdf->MultiCell(25,$alturas,''.$_POST['dcheques'][$con],0,'C',true,0,'','',true,0,false,true,0,'M',true);
				$pdf->MultiCell(25,$alturas,'$'.number_format($_POST['debitos'][$con],2),0,'R',true,0,'','',true,0,false,true,0,'M',true);
				$pdf->MultiCell(26,$alturas,'$'.number_format($_POST['creditos'][$con],2),0,'R',true,1,'','',true,0,false,true,0,'M',true); 
				$totald =  $totald + $_POST['debitos'][$con];
				$totalc =  $totalc + $_POST['creditos'][$con];
			}
			$con++;
		}
		if ($checkbox == '1'){
			$pdf->ln(2);
			$pdf->Cell(140,5,'Total','T',0,'R');
			$pdf->SetX(150);
			$pdf->Cell(25,5,'$'.number_format($totald,2,".",","),'T',0,'C');
			$pdf->Cell(26,5,'$'.number_format($totalc,2,".",","),'T',0,'C');
			$pdf->ln(5);
		}
	}
	
    $pdf->Output();

 