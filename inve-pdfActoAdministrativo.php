<?php
//V 1000 12/12/16  
	require_once("tcpdf/tcpdf_include.php");
	require('comun.inc');
	require "funciones.inc";
	session_start();
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
	date_default_timezone_set("America/New_York");

    $sqlr="SELECT funcionario, nomcargo FROM firmaspdf_det WHERE idfirmas='6' AND estado ='S'";
	$res=mysqli_query($linkbd,$sqlr);
	$rowCargo=mysqli_fetch_row($res);

    $_POST['ppto'][] = $rowCargo[0];
    $_POST['nomcargo'][] = $rowCargo[1];
	
	class MYPDF extends TCPDF 
	{
		public function Header() 
		{
			$linkbd = conectar_v7();
			$linkbd -> set_charset("utf8");
			$sqlr="SELECT nit, razonsocial FROM configbasica WHERE estado='S'";
			$resp=mysqli_query($linkbd,$sqlr);
			while($row=mysqli_fetch_row($resp)){$nit=$row[0];$rs=$row[1];}

            //-------------- RECUADRO DEL ENCABEZADO  --------------------------------
			$this->Image('imagenes/escudo.jpg', 15, 14, 25, 23.9, 'JPG', '', 'T', true, 300, '', false, false, 0, false, false, false);// Logo
			$this->SetFont('helvetica','B',8);
			$this->SetY(10);
			$this->RoundedRect(10, 10, 280, 31, 2.5,''); //Borde del encabezado - Recuadro del encabezado
			$this->Cell(35,31,'','R',0,'L'); //Linea que separa el encabazado verticalmente
			$this->SetY(10);
			$this->SetFont('helvetica','B',12);

			if(strlen($rs)<40)
			{
				$this->SetX(40);
				$this->Cell(230,15,$rs,0,0,'C');  //Posicion Municipio
				$this->SetY(16);
			}
			else
			{
				$this->Cell(61);
				$this->MultiCell(200,15,$rs,0,'C',false,1,'','',true,4,false,true,19,'T',false);
				$this->SetY(18);
			}
			
            //--------------------- CONTENIDO DENTRO DEL RECUADRO DEL ENCABEZADO -----------------------------------------

			$this->SetX(40);  //Posicion Numero Nit
			$this->SetFont('helvetica','B',8);
			$this->Cell(230,12,"NIT: $nit",0,0,'C'); // Posicion Numero Nit
			$this->SetY(27);                    // Posicion Certificado
			$this->SetX(45);                    // Posicion Certificado
			$this->SetFont('helvetica','B',11);
			$this->Cell(192,14,strtoupper($_POST['movimiento']),1,0,'C');        // Recuadro del certificado
			$this->SetFont('helvetica','I',10);
			$this->SetY(27);
			$this->SetX(62);
			$this->SetFont('helvetica','B',10);
			$this->Cell(228,7,"",'T',0,'C',false,0,1); 
			$this->SetFont('helvetica','B',9);
			$this->SetY(27);
			$this->SetX(243);                                      // Posicion Numero  
			$this->Cell(37,5," NUMERO: ".$_POST['consecutivo'],'T',0,'L');
			$this->SetY(31);
			$this->SetX(243);                
            $fechaComoEntero = strtotime($_POST['fecha']);      
            $vigencia = date("Y", $fechaComoEntero);                // Posicion Fecha     
			$this->Cell(35,6," FECHA: ".$_POST['fecha'],0,0,'L');
			$this->SetY(36);
			$this->SetX(243);                                      //  Posicion Vigencia 
			$this->Cell(35,5," VIGENCIA: ".$vigencia,0,0,'L');
		}

		public function Footer() 
		{
			$linkbd = conectar_v7();
			$linkbd -> set_charset("utf8");
			$sqlr="SELECT direccion,telefono,web,email FROM configbasica WHERE estado='S'";
			$resp=mysqli_query($linkbd,$sqlr);
			$user = $_SESSION['nickusu'];	
			$fecha = date("Y-m-d H:i:s");
			$ip = $_SERVER['REMOTE_ADDR'];
			$useri = $_POST['user'];
			while($row=mysqli_fetch_row($resp))
			{
				$direcc=strtoupper($row[0]);
				$telefonos=$row[1];
				$dirweb=strtoupper($row[3]);
				$coemail=$row[2];
			}
			if($direcc!=''){$vardirec="Dirección: $direcc, ";}
			else {$vardirec="";}
			if($telefonos!=''){$vartelef="Telefonos: $telefonos";}
			else{$vartelef="";}
			if($dirweb!=''){$varemail="Email: $dirweb, ";}
			else {$varemail="";}
			if($coemail!=''){$varpagiw="Pagina Web: $coemail";}
			else{$varpagiw="";}
			//$this->SetY(-16);
			$this->SetFont('helvetica', 'I', 8);
			$txt = <<<EOD
			$vardirec $vartelef
			$varemail $varpagiw
			EOD;
			$this->Cell(277,10,'','T',0,'T');
			$this->ln(2);
			$this->Write(0, $txt , '', 0, 'C', true, 0, false, false, 0);
			$this->Cell(25, 10, 'Hecho por: '.$useri, 0, false, 'C', 0, '', 0, false, 'T', 'M');
			$this->Cell(100, 10, 'Impreso por: '.$user, 0, false, 'C', 0, '', 0, false, 'T', 'M');
			$this->Cell(30, 10, 'IP: '.$ip, 0, false, 'C', 0, '', 0, false, 'T', 'M');
			$this->Cell(102, 10, 'Fecha: '.$fecha, 0, false, 'C', 0, '', 0, false, 'T', 'M');
			$this->Cell(5, 10, 'IDEAL.10 S.A.S    Pagina '.$this->getAliasNumPage().'/'.$this->getAliasNbPages(), 0, false, 'C', 0, '', 0, false, 'T', 'M');
			
		}
	}
	
    //----------------------------------------------------------------------

	$pdf = new MYPDF('L','mm','Letter', true, 'iso-8859-1', false);// create new PDF document // Orientacion del pdf
	$pdf->SetDocInfoUnicode (true); 
	// set document information
	$pdf->SetCreator(PDF_CREATOR);
	$pdf->SetAuthor('Ideal10sas');
	$pdf->SetTitle('Acto Administrativo');
	$pdf->SetSubject('Acto administrativo');
	$pdf->SetKeywords('TCPDF, PDF, example, test, guide');
	$pdf->SetMargins(10, 60, 10);// set margins // Posicion de los valores de cada columna
	$pdf->SetHeaderMargin(90);// set margins
	$pdf->SetFooterMargin(20);// set margins
	$pdf->SetAutoPageBreak(TRUE, 20);// set auto page breaks
	// set some language-dependent strings (optional)
	if (@file_exists(dirname(__FILE__).'/lang/spa.php')) 
	{
		require_once(dirname(__FILE__).'/lang/spa.php');
		$pdf->setLanguageArray($l);
	}

	// ---------------------------------------------------------
	$pdf->AddPage();
	$pdf->SetFont('helvetica','I',9);
    $fechaen = explode('-', $_POST['fecha']);
	if($fechaen[1]=='')
		$fechaen = explode('/', $_POST['fecha']);
    $infoinicio=strtoupper("En la ciudad de $_POST[ciudad] a los $fechaen[0] días del mes de $mesen del $fechaen[2] en el lugar $_POST[lugar], se procede a la entrada por ajuste de los bienes, efectuada por $_POST[nomTercero].");

	$pdf->SetFont('times','B',12);
	$pdf->SetY(44);
	$pdf->MultiCell(280,5,'EL SUSCRITO '.$_POST['nomcargo'][0],'','C');
	$pdf->Cell(280,12,'CERTIFICA:',0,0,'C');
	$pdf->SetY(60);
	$pdf->SetFont('times','',10);
	$pdf->cell(0.1);
	$pdf->SetY(65);
	$pdf->MultiCell(280,6,$infoinicio,0,'L',false,1,'','',true,0,false,true,0,'T',false);
	$pdf->ln(4);
	$pdf->SetFont('times','',10);
	$pdf->MultiCell(280,6,strtoupper($_POST['descripcion']),0,'L',false,1,'','',true,0,false,true,0,'T',false);
	$pdf->SetFont('times','',10);
	$pdf->Cell(35,10,$_POST['solicita'],0,1,'C',false,0,0,false,'T','C');
	$pdf->SetFont('times','B',10);
	$pdf->SetFillColor(200, 200, 200);
	$pdf->SetFont('helvetica','B',6);
	$posy=$pdf->GetY();
	$pdf->SetY($posy+1);

	$pdf->Cell(35,5,'Articulo',1,0,'C',true,0,0,false,'C','C');
	$pdf->Cell(50,5,'Nombre articulo.',1,0,'C',true,0,0,false,'C','C');
	$pdf->Cell(30,5,'Unidad de medida',1,0,'C',true,0,0,false,'C','C');
	$pdf->Cell(30,5,'Estado articulo',1,0,'C',true,0,0,false,'C','C');
	$pdf->Cell(40,5,'Cantidad',1,0,'C',true,0,0,false,'C','C');
	$pdf->Cell(40,5,'Valor unitario',1,0,'C',true,0,0,false,'C','C');
	$pdf->Cell(40,5,'Valor total',1,1,'C',true,0,0,false,'C','C');
	$con=0;
	while ($con<count($_POST['articulo']))
	{
		$altura=4;
		$altini=4;
		$ancini=20;
		$altaux=0;
		$colst01=strlen($_POST['articulo'][$con]);
		$colst02=strlen($_POST['nomArticulo'][$con]);
		if($colst01>$colst02){$cantidad_lineas= $colst01;}
		else{$cantidad_lineas= $colst02;}
		if($cantidad_lineas > $ancini)
		{
			$cant_espacios = $cantidad_lineas/$ancini;
			$rendondear=ceil($cant_espacios);
			$altaux=$altini*$rendondear;
		}
		if($altaux>$altura){$altura=$altaux;}

		$v=$pdf->gety();
		if($v>=155){ 
			$pdf->AddPage();
			$pdf->SetFillColor(200, 200, 200);
			$pdf->SetFont('helvetica','B',6);
			$posy=$pdf->GetY();
			$pdf->SetY($posy+1);

			$pdf->Cell(25,5,'Rubro',1,0,'C',true,0,0,false,'C','C');
			$pdf->Cell(30,5,'Sec. presu.',1,0,'C',true,0,0,false,'C','C');
			$pdf->Cell(20,5,'Medio pago',1,0,'C',true,0,0,false,'C','C');
			$pdf->Cell(25,5,'Vig gasto',1,0,'C',true,0,0,false,'C','C');
			$pdf->Cell(40,5,'Proyecto',1,0,'C',true,0,0,false,'C','C');
			$pdf->Cell(25,5,'Programatico',1,0,'C',true,0,0,false,'C','C');
			$pdf->Cell(30,5,'CCPET',1,0,'C',true,0,0,false,'C','C');
			$pdf->Cell(30,5,'Fuente',1,0,'C',true,0,0,false,'C','C');
			$pdf->Cell(25,5,'CPC',1,0,'C',true,0,0,false,'C','C');
			$pdf->Cell(30,5,'Valor',1,1,'C',true,0,0,false,'C','C');
		}

		$pdf->SetFillColor(255,255,255);
		$pdf->SetFont('times','',9);
		$pdf->MultiCell(35,$altura,strtoupper($_POST['articulo'][$con]),1,'C',true,0,'','',true,0,false,true,$altura,'M',false);
		$pdf->MultiCell(50,$altura,strtolower($_POST['nomArticulo'][$con]),1,'C',true,0,'','',true,0,false,true,$altura,'M',false);
		$pdf->MultiCell(30,$altura,$_POST['unidad'][$con],1,'C',true,0,'','',true,0,false,true,$altura,'M',false);
		$pdf->MultiCell(30,$altura,$_POST['estado'][$con],1,'C',true,0,'','',true,0,false,true,$altura,'M',false);
		$pdf->SetFont('times','',8);
		$pdf->MultiCell(40,$altura,$_POST['cantidad'][$con],1,'C',true,0,'','',true,0,false,true,$altura,'M',false);
		$pdf->MultiCell(40,$altura,number_format($_POST['valorUnitario'][$con]),1,'C',true,0,'','',true,0,false,true,$altura,'M',false);
		$pdf->SetFont('times','',9);
		$pdf->Cell(40,$altura,"$ ".number_format($_POST['valorTotal'][$con],2,$_SESSION["spdecimal"],$_SESSION["spmillares"])."   ",1,1,'C',true,0,0,false,'T','C');
		$con++;
	}

    //-------------------  Nombre/ Cargo / Valor total ----------------
	$sql="SELECT user FROM pptocdp WHERE consvigencia='".$_POST['numero']."' AND vigencia='".$_POST['vigencia']."' ";
	$res=mysqli_query($linkbd,$sql);
	$row = mysqli_fetch_row($res);
	$pdf->Cell(225,6,'TOTAL:',0,0,'R',false,0,0,false,'T','C');
	$pdf->setFont('times','B',9);
	$pdf->Cell(40,6,"$ ".number_format($_POST['totalValores'],2,$_SESSION["spdecimal"],$_SESSION["spmillares"])."  ",1,1,'C',false,0,0,false,'T','C');
	$pdf->ln(8);
	$v=$pdf->gety();
	if($v>=180){ 
		$pdf->AddPage();
		$v=$pdf->gety();
	}
	
	for($x=0;$x<count($_POST['ppto']);$x++)
	{
		$pdf->ln(20);
		$v=$pdf->gety();
		if($v>=180){
			$pdf->AddPage();
			$pdf->ln(20);
			$v=$pdf->gety();
		}
		$pdf->setFont('times','B',8);
		if (($x%2)==0) {
			if(isset($_POST['ppto'][$x+1])){
				$pdf->Line(17,$v,107,$v);
				$pdf->Line(112,$v,202,$v);
				$v2=$pdf->gety();
				$pdf->Cell(104,4,''.$_POST['ppto'][$x],0,1,'C',false,0,0,false,'T','C');
				$pdf->Cell(200,4,''.$_POST['nomcargo'][$x],0,1,'C',false,0,0,false,'T','C');
				$pdf->SetY($v2);
				$pdf->Cell(295,4,''.$_POST['ppto'][$x+1],0,1,'C',false,0,0,false,'T','C');
				$pdf->Cell(200,4,''.$_POST['nomcargo'][$x+1],0,1,'C',false,0,0,false,'T','C');
			}else{
				$pdf->Line(100,$v,200,$v);
				$pdf->Cell(280,4,''.$_POST['ppto'][$x],0,1,'C',false,0,0,false,'T','C');
				$pdf->Cell(280,4,''.$_POST['nomcargo'][$x],0,0,'C',false,0,0,false,'T','C');
			}
			$v3=$pdf->gety();
		}
		$pdf->SetY($v3);
		$pdf->SetFont('helvetica','',7);
	}
	// ---------------------------------------------------------
	$pdf->Output('reportecdp.pdf', 'I');//Close and output PDF document
?>
