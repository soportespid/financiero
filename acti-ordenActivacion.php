<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require 'comun.inc';
	require 'funciones.inc';
	session_start();
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html lang=es>
	<head>
    <meta charset="utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1"/>
		<meta name="viewport" content="user-scalable=no">
		<title>:: IDEAL 10 - Activos fijos</title>
		<link href="favicon.ico" rel="shortcut icon"/>
		<link href="css/css2z.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
        <link href="css/style.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
        <link href="css/tabs.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" href="sweetalert2/dist/sweetalert2.min.css">
        <script type="text/javascript" src="css/programas.js"></script>
        <script src="sweetalert2/dist/sweetalert2.min.js"></script>
		<script src="vue/vue.min.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script src="css/calendario.js?<?php echo date('d_m_Y_h_i_s');?>"></script>

        <style>
			[v-cloak]{display : none;}
		</style>
	</head>
	<body>
		<header>
			<table>
				<tr><script>barra_imagenes("acti");</script><?php cuadro_titulos();?></tr>
			</table>
		</header>

		<form name="form2" method="post" action="">
			<section id="myapp" v-cloak>
                <div class="loading-container" v-show="isLoading" >
                    <p class="text-loading"data-loading="IDEAL 10">IDEAL 10</p>
                </div>

				<nav>
					<table>
						<tr><?php menu_desplegable("acti");?></tr>
					</table>

                    <div class="bg-white group-btn p-1" id="newNavStyle">
                        <button type="button" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center" @click="window.location.reload()">
                            <span>Nuevo</span>
                            <svg viewBox="0 -960 960 960" ><path d="M440-440H200v-80h240v-240h80v240h240v80H520v240h-80v-240Z"/></svg>
                        </button>
                        <button type="button" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center" @click="guardar()">
                            <span>Guardar</span>
                            <svg viewBox="0 -960 960 960" ><path d="M840-680v480q0 33-23.5 56.5T760-120H200q-33 0-56.5-23.5T120-200v-560q0-33 23.5-56.5T200-840h480l160 160Zm-80 34L646-760H200v560h560v-446ZM480-240q50 0 85-35t35-85q0-50-35-85t-85-35q-50 0-85 35t-35 85q0 50 35 85t85 35ZM240-560h360v-160H240v160Zm-40-86v446-560 114Z"/></svg>
                        </button>
                        <button type="button" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center" @click="location.href='acti-buscarOrdenesActivacion'">
                            <span>Buscar</span>
                            <svg class="fill-black group-hover:fill-white w-5 h-5" viewBox="0 -960 960 960" ><path d="M784-120 532-372q-30 24-69 38t-83 14q-109 0-184.5-75.5T120-580q0-109 75.5-184.5T380-840q109 0 184.5 75.5T640-580q0 44-14 83t-38 69l252 252-56 56ZM380-400q75 0 127.5-52.5T560-580q0-75-52.5-127.5T380-760q-75 0-127.5 52.5T200-580q0 75 52.5 127.5T380-400Z"/></svg>
                        </button>
                        <button type="button" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center" @click="mypop=window.open('acti-principal.php','',''); mypop.focus();">
                            <span>Nueva ventana</span>
                            <svg class="fill-black group-hover:fill-white w-5 h-5" viewBox="0 -960 960 960" ><path d="M320-120v-80h80v-80H160q-33 0-56.5-23.5T80-360v-400q0-33 23.5-56.5T160-840h640q33 0 56.5 23.5T880-760v400q0 33-23.5 56.5T800-280H560v80h80v80H320ZM160-360h640v-400H160v400Zm0 0v-400 400Z"/></svg>
                        </button>
                        <button type="button" class="btn btn-success d-flex justify-between align-items-center" @click="window.location.href='acti-gestiondelosactivos'">
                            <span>Atrás</span>
                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960"><path d="M760-200v-160q0-50-35-85t-85-35H273l144 144-57 56-240-240 240-240 57 56-144 144h367q83 0 141.5 58.5T840-360v160h-80Z"/></svg>
                        </button>
                    </div>
				</nav>

				<article>
                    <div class="bg-white">
                        <div>
                            <!-- <h2 class="titulos m-0">Tipo de movimiento</h2> -->
                            <div class="d-flex">
                                <div class="form-control">
                                    <select class="w-25" v-model="movimiento">
                                        <option value="1">Entrada</option>
                                        <option value="3">Reversión</option>
                                    </select>
                                </div>
                            </div>
                        </div>
					</div>

                    <div class="bg-white" v-if="movimiento == 1">
                        <h2 class="titulos m-0">Orden de activación</h2>
                        <!-- <p class="m-0 ms-2">Todos los campos con (<span class="text-danger fw-bolder">*</span>) son obligatorios</p> -->
                        
                        <div class="d-flex">
                            <div class="form-control">
                                <label class="form-label m-0" for="">Orden<span class="text-danger fw-bolder">*</span></label>
                                <input type="text" class="text-center" v-model="codigoOrden" readonly>
                            </div>

                            <div class="form-control">
                                <label class="form-label m-0" for="">Fecha<span class="text-danger fw-bolder">*</span></label>
                                <input type="date" class="text-center" v-model="fechaRegistro">
                            </div>

                            <div class="form-control">
                                <label class="form-label m-0" for="">Origen<span class="text-danger fw-bolder">*</span></label>
                                <select v-model="origen" v-on:change="cambioOrigen">
                                    <option value="" selected>Selecciona origen</option>
                                    <option v-for="orig in origenes" v-bind:value="orig[0]">
                                        {{ orig[0] }} - {{ orig[1] }}
                                    </option>
                                </select>
                            </div>

                            <div class="form-control" v-show="origen != 7">
                                <label class="form-label m-0" for="">Documento<span class="text-danger fw-bolder">*</span></label>
                                <input type="text" class="text-center colordobleclik" v-model="documento" @dblclick="buscarDocumentos" readonly>
                            </div>
                            
                            <div class="form-control" v-show="origen != 7">
                                <label class="form-label m-0" for="">Valor<span class="text-danger fw-bolder">*</span></label>
                                <input type="text" class="text-center" v-model="formatonumero(valor)" readonly>
                            </div>
                        </div>

                        <div ref="rTabs" class="nav-tabs bg-white p-1">
                            <div class="nav-item" :class="tabgroup1 == 1 ? 'active' : ''" @click="tabgroup1=1;">Crear activo</div>
                            <div class="nav-item" :class="tabgroup1 == 2 ? 'active' : ''" @click="tabgroup1=2;">Lista activos</div>
                        </div>

                        <div v-show="tabgroup1 == 1">
                            <div class="d-flex">
                                <div class="form-control">
                                    <label class="form-label m-0" for="">Clase<span class="text-danger fw-bolder">*</span></label>
                                    <select v-model="clase" v-on:change="buscaGrupos(clase[0])">
                                        <option value="">Seleccionar clase</option>
                                        <option v-for="clas in clases" v-bind:value="clas">
                                            {{ clas[0] }} - {{ clas[1] }}
                                        </option>
                                    </select>
                                </div>

                                <div class="form-control">
                                    <label class="form-label m-0" for="">Grupo<span class="text-danger fw-bolder">*</span></label>
                                    <select v-model="grupo" v-on:change="buscarTipos(clase[0], grupo[0])">
                                        <option value="">Seleccionar grupo</option>
                                        <option v-for="grup in grupos" v-bind:value="grup">
                                            {{ grup[0] }} - {{ grup[1] }}
                                        </option>
                                    </select>
                                </div>

                                <div class="form-control">
                                    <label class="form-label m-0" for="">Tipo<span class="text-danger fw-bolder">*</span></label>
                                    <select v-model="tipo" v-on:change="formarPlaca(clase[0], grupo[0], tipo[0])">
                                        <option value="">Seleccionar tipo</option>
                                        <option v-for="tip in tipos" v-bind:value="tip">
                                            {{ tip[0] }} - {{ tip[1] }}
                                        </option>
                                    </select>
                                </div>

                                <div class="form-control">
                                    <label class="form-label m-0" for="">Placa activo</label>
                                    <input type="text" v-model="placa" class="text-center" readonly>
                                </div>

                                <div class="form-control">
                                    <label class="form-label m-0" for="">Prototipo<span class="text-danger fw-bolder">*</span></label>
                                    <input type="text" v-model="prototipo" v-on:dblclick="modalPrototipos=true;" class="colordobleclik text-left" readonly>
                                </div>
                            </div>

                            <div class="d-flex">
                                <div class="form-control">
                                    <label class="form-label m-0" for="">Dependencia<span class="text-danger fw-bolder">*</span></label>
                                    <input type="text" v-model="dependencia" v-on:dblclick="modalDependencias=true;" class="colordobleclik text-left" readonly>
                                </div>

                                <div class="form-control">
                                    <label class="form-label m-0" for="">Ubicación<span class="text-danger fw-bolder">*</span></label>
                                    <input type="text" v-model="ubicacion" v-on:dblclick="modalUbicaciones=true;" class="colordobleclik text-left" readonly>
                                </div>

                                <div class="form-control">
                                    <label class="form-label m-0" for="">Centro costos<span class="text-danger fw-bolder">*</span></label>
                                    <select v-model="centroCosto">
                                        <option value="">Seleccionar centro de costos</option>
                                        <option v-for="cc in centroCostos" v-bind:value="cc">
                                            {{ cc[0]}} - {{ cc[1] }}
                                        </option>
                                    </select>
                                </div>

                                <div class="form-control">
                                    <label class="form-label m-0" for="">Disposición activo<span class="text-danger fw-bolder">*</span></label>
                                    <select v-model="disposicion">
                                        <option value="">Seleccionar disposicion</option>
                                        <option v-for="dispo in disposiciones" v-bind:value="dispo">
                                            {{ dispo[0] }} - {{ dispo[1] }}
                                        </option>
                                    </select>
                                </div>
                            </div>

                            <div class="d-flex">
                                <div class="form-control">
                                    <label class="form-label m-0" for="">Nombre activo<span class="text-danger fw-bolder">*</span></label>
                                    <input type="text" v-model="nombreActivo">
                                </div>

                                <div class="form-control w-50">
                                    <label class="form-label m-0" for="">Valor activo:</label>
                                    <div class="form-number number-primary">
                                        <span></span>
                                        <input type="number" class="text-center" v-model="valorActivo">
                                    </div>
                                </div>

                                <div class="form-control w-50">
                                    <label class="form-label m-0" for="">Valor salvamento:</label>
                                    <div class="form-number number-primary">
                                        <span></span>
                                        <input type="number" class="text-center" v-model="valorSalv">
                                    </div>
                                </div>

                                <div class="form-control w-50">
                                    <label class="form-label m-0" for="">Valor desmantelamiento:</label>
                                    <div class="form-number number-primary">
                                        <span></span>
                                        <input type="number" class="text-center" v-model="valorDesman">
                                    </div>
                                </div>
                            </div>

                            <div class="d-flex">
                                <div class="form-control">
                                    <label class="form-label m-0" for="">Referencia</label>
                                    <input type="text" v-model="referencia">
                                </div>

                                <div class="form-control">
                                    <label class="form-label m-0" for="">Modelo</label>
                                    <input type="text" v-model="modelo">
                                </div>

                                <div class="form-control">
                                    <label class="form-label m-0" for="">Serial</label>
                                    <input type="text" v-model="serial">
                                </div>

                                <div class="form-control">
                                    <label class="form-label m-0" for="">Estado<span class="text-danger fw-bolder">*</span></label>
                                    <select v-model="estadoActivo">
                                        <option value="">Seleccionar estado</option>
                                        <option value="bueno">Bueno</option>
                                        <option value="regular">Regular</option>
                                        <option value="malo">Malo</option>
                                    </select>
                                </div>

                                <div class="form-control">
                                    <label class="form-label m-0" for="">Unidad de medida<span class="text-danger fw-bolder">*</span></label>
                                    <select v-model="unidadMedida">
                                        <option value="">Seleccionar unidad</option>
                                        <option v-for="uni in unidades" v-bind:value="uni">
                                            {{ uni[0] }} - {{ uni[1] }}
                                        </option>
                                    </select>
                                </div>
                            </div>

                            <div class="d-flex">
                                <div class="form-control">
                                    <label class="form-label m-0" for="">Fecha compra<span class="text-danger fw-bolder">*</span></label>
                                    <input type="date" class="text-center" v-model="fechaCompra">
                                </div>

                                <div class="form-control">
                                    <label class="form-label m-0" for="">Fecha activacion<span class="text-danger fw-bolder">*</span></label>
                                    <input type="date" class="text-center" v-model="fechaActivacion">
                                </div>

                                <div class="form-control">
                                    <label class="form-label m-0" for="">Depreciacion en bloque<span class="text-danger fw-bolder">*</span></label>
                                    <input type="checkbox" v-model="depreacionBloque">
                                </div>

                                <div class="form-control">
                                    <label class="form-label m-0" for="">Depreciacion individual</label>
                                    <input type="text" v-model="mesesDepreciados" :readonly="depreacionBloque == true" style = "text-align: center;">
                                </div>
                            </div>

                            <div class="d-flex">
                                <div class="form-control w-50">
                                    <label class="form-label m-0" for="">Foto activo</label>
                                    <input type="file" id="foto" accept="image/jpeg,image/png,image/x-eps,application/pdf" @change="obtenerFoto()">
                                </div>
    
                                <div class="form-control w-50">
                                    <label class="form-label m-0" for="">Ficha tecnica</label>
                                    <input type="file" id="ficha" accept="image/jpeg,image/png,image/x-eps,application/pdf" @change="obtenerFicha()">
                                </div>
    
                                <div class="form-control w-25">
                                    <label class="form-label m-0" for="">Responsable<span class="text-danger fw-bolder">*</span></label>
                                    <input type="text" v-model="cedula" @dblclick="despliegaModalResponsable" @change="buscarCedula(cedula)" class="colordobleclik">
                                </div>
    
                                <div class="form-control">
                                    <label class="form-label m-0" for="">Nombre responsable<span class="text-danger fw-bolder">*</span></label>
                                    <input type="text" v-model="responsable" style="width: 100%;" readonly>
                                </div>
                                
                                <div class="form-control justify-between w-25">
                                    <label for=""></label>
                                    <button type="button" class="btn btn-primary" @click="agregarActivo()">Agregar activo</button>
                                </div>
                            </div>
                        </div>

                        <div v-show="tabgroup1 == 2">
                            <div class="table-responsive">
                                <table class="table table-hover fw-normal">
                                    <thead>
                                        <tr>
                                            <th>Placa</th>
                                            <th>Nombre</th>
                                            <th>Clase</th>
                                            <th>Grupo</th>
                                            <th>Tipo</th>
                                            <th>Prototipo</th>
                                            <th>Dependencia</th>
                                            <th>Ubicación</th>
                                            <th>CC</th>
                                            <th>Disposición</th>        
                                            <th>Valor</th>
                                            <th>Referencia</th>
                                            <th>Modelo</th>
                                            <th>Serial</th>
                                            <th>Estado</th>
                                            <th>Fecha compra</th>
                                            <th>Fecha activación</th>
                                            <th>Unidad de medida</th>
                                            <th>Depreciación en bloque</th>
                                            <th>Depreciación individual</th>
                                            <th>Responsable</th>
                                            <th>Foto</th>
                                            <th>Ficha tecnica</th>
                                            <th>Eliminar</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr v-for="(activo,index) in activos">
                                            <td>{{ activo[0] }}</td>
                                            <td>{{ activo[1] }}</td>
                                            <td>{{ activo[2] }}</td>
                                            <td>{{ activo[3] }}</td>
                                            <td>{{ activo[4] }}</td>
                                            <td>{{ activo[5] }}</td>
                                            <td>{{ activo[6] }}</td>
                                            <td>{{ activo[7] }}</td>
                                            <td>{{ activo[8] }}</td>
                                            <td>{{ activo[9] }}</td>
                                            <td>{{ formatonumero(activo[10]) }}</td>
                                            <td>{{ activo[11] }}</td>
                                            <td>{{ activo[12] }}</td>
                                            <td>{{ activo[13] }}</td>
                                            <td>{{ activo[14] }}</td>
                                            <td>{{ activo[15] }}</td>
                                            <td>{{ activo[16] }}</td>
                                            <td>{{ activo[17] }}</td>
                                            <td>{{ activo[18] }}</td>
                                            <td>{{ activo[19] }}</td>
                                            <td>{{ activo[20] }}</td>
                                            <td class="text-center"><img v-bind:src="img" width="40px" height="40px"></td>
                                            <td class="text-center"><img v-bind:src="img2" width="40px" height="40px"></td>
                                            <td class="text-center"><img src="imagenes/eliminar.png" width="40px" height="40px" @click="eliminarActivoAgregado(activo)"></td>
                                        </tr>
                                    </tbody>
                                    <tfoot>
                                        <tr class="fw-bold">
                                            <td class="text-right"colspan="10">Total: </td>
                                            <td>{{ formatonumero(totalActivos) }}</td>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div> 
                        </div>
                    </div>
                
                    <div v-show="movimiento == 3">
                        <div class="bg-white">
                            <h2 class="titulos m-0">Reversar orden de activación</h2>
                            <p class="m-0 ms-2">Todos los campos con (<span class="text-danger fw-bolder">*</span>) son obligatorios</p>

                            <div class="d-flex">
                                <div class="form-control">
                                    <label class="form-label m-0" for="">Fecha reversión<span class="text-danger fw-bolder">*</span></label>
                                    <input type="date" class="text-center" v-model="fechaReversion">
                                </div>

                                <div class="form-control">
                                    <label class="form-label m-0" for="">Orden de activación<span class="text-danger fw-bolder">*</span></label>
                                    <input type="text" v-model="ordenRev" @dblclick="desplegarOrdenes" class="colordobleclik" readonly>
                                </div>

                                <div class="form-control">
                                    <label class="form-label m-0" for="">Fecha orden<span class="text-danger fw-bolder">*</span></label>
                                    <input type="text" v-model="fechaRev" class="text-center" readonly>
                                </div>

                                <div class="form-control">
                                    <label class="form-label m-0" for="">Origen<span class="text-danger fw-bolder">*</span></label>
                                    <input type="text" v-model="origenRev" class="text-center" readonly>
                                </div>

                                <div class="form-control">
                                    <label class="form-label m-0" for="">Documento<span class="text-danger fw-bolder">*</span></label>
                                    <input type="text" v-model="documentoRev" class="text-center" readonly>
                                </div>

                                <div class="form-control">
                                    <label class="form-label m-0" for="">Valor<span class="text-danger fw-bolder">*</span></label>
                                    <input type="text" v-model="formatonumero(valorRev)" readonly>
                                </div>
                            </div>

                            <div class="table-responsive">
                                <table class="table table-hover fw-normal">
                                    <thead>
                                        <tr>
                                            <th>Placa</th>
                                            <th>Nombre</th>
                                            <th>Clase</th>
                                            <th>Grupo</th>
                                            <th>Tipo</th>
                                            <th>Prototipo</th>
                                            <th>Dependencia</th>
                                            <th>Ubicación</th>
                                            <th>Centro costo</th>
                                            <th>Disposición</th>
                                            <th>Valor</th>
                                            <th>Referencia</th>
                                            <th>Modelo</th>
                                            <th>Serial</th>
                                            <th>Estado</th>
                                            <th>Fecha compra</th>
                                            <th>Fecha activación</th>
                                            <th>Unidad de medida</th>
                                            <th>Depreciación en bloque</th>
                                            <th>Depreciación individual</th>
                                            <th>Foto</th>
                                            <th>Ficha tecnica</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr v-for="(activo,index) in ordenesDet">
                                            <td>{{ activo[0] }}</td>
                                            <td>{{ activo[1] }}</td>
                                            <td>{{ activo[2] }}</td>
                                            <td>{{ activo[3] }}</td>
                                            <td>{{ activo[4] }}</td>
                                            <td>{{ activo[5] }}</td>
                                            <td>{{ activo[6] }}</td>
                                            <td>{{ activo[7] }}</td>
                                            <td>{{ activo[8] }}</td>
                                            <td>{{ activo[9] }}</td>
                                            <td>{{ formatonumero(activo[10]) }}</td>
                                            <td>{{ activo[11] }}</td>
                                            <td>{{ activo[12] }}</td>
                                            <td>{{ activo[13] }}</td>
                                            <td>{{ activo[14] }}</td>
                                            <td>{{ activo[15] }}</td>
                                            <td>{{ activo[16] }}</td>
                                            <td>{{ activo[17] }}</td>
                                            <td>{{ activo[18] }}</td>
                                            <td>{{ activo[19] }}</td>
                                            <td class="text-center">
                                                <a v-bind:href="activo[20]" target="_blank">
                                                    <img v-bind:src="activo[22]" width="40px" height="40px">
                                                </a>
                                            </td>
                                            <td class="text-center">
                                                <a v-bind:href="activo[21]" target="_blank">
                                                    <img v-bind:src="activo[23]" width="40px" height="40px">
                                                </a>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                    <div v-show="modalPrototipos" class="modal">
                        <div class="modal-dialog modal-sm" >
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title">Listado de prototipos</h5>
                                    <button type="button" @click="modalPrototipos=false;" class="btn btn-close"><div></div><div></div></button>
                                </div>
                                <div class="modal-body">
                                    <div class="d-flex flex-column">
                                        <div class="form-control m-0 mb-3">
                                            <input type="search" v-model="txtSearchPro" @keyup="searchPrototipo()" placeholder="Busca por codigo o nombre">
                                        </div>
                                    </div>
                                    <div class="overflow-auto max-vh-50 overflow-x-hidden" >
                                        <table class="table table-hover fw-normal">
                                            <thead>
                                                <tr>
                                                    <th>Codigo</th>
                                                    <th>Nombre</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr v-for="(data,index) in prototipos_copy" :key="index" @click="selectPrototipo(data)">
                                                    <td>{{data[0]}}</td>
                                                    <td>{{data[1]}}</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div v-show="modalDependencias" class="modal">
                        <div class="modal-dialog modal-sm" >
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title">Listado de dependencias</h5>
                                    <button type="button" @click="modalDependencias=false;" class="btn btn-close"><div></div><div></div></button>
                                </div>
                                <div class="modal-body">
                                    <div class="d-flex flex-column">
                                        <div class="form-control m-0 mb-3">
                                            <input type="search" v-model="txtSearchDep" @keyup="searchDependencia()" placeholder="Busca por codigo o nombre">
                                        </div>
                                    </div>
                                    <div class="overflow-auto max-vh-50 overflow-x-hidden" >
                                        <table class="table table-hover fw-normal">
                                            <thead>
                                                <tr>
                                                    <th>Codigo</th>
                                                    <th>Nombre</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr v-for="(data,index) in dependencias_copy" :key="index" @click="selectDepen(data)">
                                                    <td>{{data[0]}}</td>
                                                    <td>{{data[1]}}</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div v-show="modalUbicaciones" class="modal">
                        <div class="modal-dialog modal-sm" >
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title">Listado de ubicaciones</h5>
                                    <button type="button" @click="modalUbicaciones=false;" class="btn btn-close"><div></div><div></div></button>
                                </div>
                                <div class="modal-body">
                                    <div class="d-flex flex-column">
                                        <div class="form-control m-0 mb-3">
                                            <input type="search" v-model="txtSearchUbi" @keyup="searchUbicacion()" placeholder="Busca por codigo o nombre">
                                        </div>
                                    </div>
                                    <div class="overflow-auto max-vh-50 overflow-x-hidden" >
                                        <table class="table table-hover fw-normal">
                                            <thead>
                                                <tr>
                                                    <th>Codigo</th>
                                                    <th>Nombre</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr v-for="(data,index) in ubicaciones_copy" :key="index" @click="selectUbicacion(data)">
                                                    <td>{{data[0]}}</td>
                                                    <td>{{data[1]}}</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div v-show="showDocumentos" class="modal">
                        <div class="modal-dialog modal-lg" >
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title">Documentos disponibles para activación</h5>
                                    <button type="button" @click="showDocumentos=false;" class="btn btn-close"><div></div><div></div></button>
                                </div>
                                <div class="modal-body">
                                    <div class="overflow-auto max-vh-50 overflow-x-hidden" >
                                        <table class="table table-hover fw-normal">
                                            <thead>
                                                <tr class="text-center">
                                                    <th>Consecutivo documento</th>
													<th>Descripción</th>
													<th>Fecha</th>
                                                    <th>Valor</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr v-for="(document,index) in documentos" v-on:click="seleccionarDocumento(document)" class="text-center">
                                                    <td> {{ document[0] }} </td>
													<td> {{ document[1] }} </td>
													<td> {{ document[2] }} </td>
                                                    <td class="text-right"> {{ formatonumero(document[3]) }} </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div v-show="showModalResponsables" class="modal">
                        <div class="modal-dialog modal-lg" >
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title">Listado terceros</h5>
                                    <button type="button" @click="showModalResponsables=false;" class="btn btn-close"><div></div><div></div></button>
                                </div>
                                <div class="modal-body">
                                    <div class="d-flex flex-column">
                                        <div class="form-control m-0 mb-3">
                                            <input type="search" v-model="searchCedula" @keyup="filtroCedulaResponsable()" placeholder="Buscar por número de documento o nombre">
                                        </div>
                                    </div>
                                    <div class="overflow-auto max-vh-50 overflow-x-hidden" >
                                        <table class="table table-hover fw-normal">
                                            <thead>
                                                <tr>
                                                    <th class="text-center">Documento</th>
                                                    <th>Nombre o razón social</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr v-for="(tercero,index) in terceros" :key="index" v-on:click="seleccionaResponsable(tercero[0], tercero[1])">
                                                    <td class="text-center">{{tercero[0] }}</td>
                                                    <td>{{ tercero[1] }}</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div v-show="showOrdenes" class="modal">
                        <div class="modal-dialog modal-lg" >
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title">Ordenes activas</h5>
                                    <button type="button" @click="showOrdenes=false;" class="btn btn-close"><div></div><div></div></button>
                                </div>
                                <div class="modal-body">
                                    <div class="overflow-auto max-vh-50 overflow-x-hidden" >
                                        <table class="table table-hover fw-normal">
                                            <thead>
                                                <tr class="text-center">
                                                    <th>Codigo orden</th>
													<th>Fecha</th>
													<th>Origen</th>
                                                    <th>Documento</th>
                                                    <th>Valor</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr v-for="(orden,index) in ordenes" v-on:click="seleccionarOrden(orden)" class="text-center">
                                                    <td> {{ orden[0] }} </td>
													<td> {{ orden[1] }} </td>
                                                    <td> {{ orden[2] }} </td>
													<td> {{ orden[3] }} </td>
                                                    <td> {{ formatonumero(orden[4]) }} </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- <div v-show="showOrdenes">
						<transition name="modal">
							<div class="modal-mask">
								<div class="modal-wrapper">
									<div class="modal-container">
										<table class="inicio ancho">
											<tr>
												<td class="titulos" colspan="2" >Ordenes disponibles</td>
												<td class="cerrar" style="width:7%" @click="showOrdenes = false">Cerrar</td>
											</tr>
										</table>
										<table class='tablamv'>
											<thead>
												<tr style="text-align:center;">
													<th class="titulosnew00" style="width: 20%;">Codigo orden</th>
													<th class="titulosnew00" style="width: 20%;">Fecha</th>
													<th class="titulosnew00" style="width: 10%;">Origen</th>
                                                    <th class="titulosnew00" style="width: 10%;">Documento</th>
                                                    <th class="titulosnew00">Valor</th>
                                                    <th style="width: 1%;"></th>
												</tr>
											</thead>
											<tbody>
												<tr v-for="(orden,index) in ordenes" v-bind:class="index % 2 ? 'saludo1a' : 'saludo2'" v-on:click="seleccionarOrden(orden)">
													<td style="width:20%; text-align:center;"> {{ orden[0] }} </td>
													<td style="width:20%; text-align:center;"> {{ orden[1] }} </td>
                                                    <td style="width:10%; text-align:center;"> {{ orden[2] }} </td>
													<td style="width:10%; text-align:center;"> {{ orden[3] }} </td>
                                                    <td style="text-align:center;"> {{ formatonumero(orden[4]) }} </td>
												</tr>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</transition>
					</div>   -->

				</article>
			</section>
		</form>

		<script src="node_modules/read-excel-file/bundle/read-excel-file.min.js"></script>
		<script src="node_modules/xlsx/dist/xlsx.full.min.js"></script>
        <script src="Librerias/vue/vue.min.js"></script>
		<script src="Librerias/vue/axios.min.js"></script>
		<script type="module" src="activos_fijos/ordenActivacion/crear/acti-ordenActivacion.js?<?= date('d_m_Y_h_i_s');?>"></script>
        
	</body>
</html>