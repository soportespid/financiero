<?php

    header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require 'comun.inc';
	require 'funciones.inc';
	session_start();
    if(empty($_SESSION)){
        header("location: index.php");
    }
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html lang=es>
	<head>
    <meta charset="utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1"/>
		<meta name="viewport" content="user-scalable=no">
		<title>:: IDEAL 10 - Tesorería</title>
		<link href="favicon.ico" rel="shortcut icon"/>
		<link href="css/css2z.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
        <link href="css/tabs.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
        <link href="css/style.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" href="sweetalert2/dist/sweetalert2.min.css">
        <script type="text/javascript" src="css/programas.js"></script>
        <script src="sweetalert2/dist/sweetalert2.min.js"></script>
		<script src="vue/vue.min.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script src="css/calendario.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
	</head>
	<body>
		<header>
			<table>
				<tr><script>barra_imagenes("teso");</script><?php cuadro_titulos();?></tr>
			</table>
		</header>

		<form name="form2" method="post" action="">
			<section id="myapp" v-cloak >
                <div id="cargando" v-show="isLoading" class="loading" style="z-index: 10000;">
                    <span>Cargando...</span>
                </div>
				<nav>
					<table>
						<tr><?php menu_desplegable("teso");?></tr>
                        <tr>
							<td colspan="3" class="cinta">
                                <img src="imagenes/add2.png"  class="mgbt" title="Nuevo">
								<img src="imagenes/guardad.png"   title="Guardar"  class="mgbt">
								<img src="imagenes/busca.png" @click="window.location.reload()"   class="mgbt" title="Buscar">
								<img src="imagenes/nv.png" @click="mypop=window.open('teso-principal.php','',''); mypop.focus();" class="mgbt" title="Nueva Ventana">
							</td>
						</tr>
					</table>
				</nav>
				<article>
                    <div class="inicio">
                        <div>
                            <h2 class="titulos m-0">.: Buscar predio a certificar</h2>
                            <div class="d-flex justify-between">
                                <div class="d-flex justify-between w-50">
                                    <div class="form-control">
                                        <input type="search" @keyup="search()" placeholder="Buscar por codigo catastral, documento, nombre o dirección" v-model="txtSearch">
                                    </div>
                                </div>
                            </div>
                            <h2 class="titulos m-0">.: Resultados: {{ txtResults}} </h2>
                            <div class="overflow-auto max-vh-50 overflow-x-hidden p-2" >
                                <table v-show="arrData.length > 0" class="table fw-normal">
                                    <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>Codigo catastral</th>
                                            <th>Documento</th>
                                            <th>Nombre</th>
                                            <th>Dirección</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr v-for="(data,index) in arrData" :key="index">
                                            <td>{{data.id}}</td>
                                            <td>{{data.codigo_catastro}}</td>
                                            <td>{{data.documento}}</td>
                                            <td>{{data.nombrepropietario}}</td>
                                            <td>{{data.direccion}}</td>
                                            <td class="text-center">
                                                <button v-show="!data.certificado" @click="save(data.codigo_catastro,index)" type="button" class="btn btn-success">
                                                   Certificar
                                                </button>
                                                <button v-show="data.certificado" @click="printPDF(data.codigo_catastro)"
                                                class="btn btn-primary text-white">Ver certificado</button>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div v-if="arrData !=''" class="inicio">
                        <p style="text-align:center">Página {{ intPage }} de {{ intTotalPages }}</p>
                        <ul style="list-style:none; padding:0;display:flex;justify-content: center;align-items:center">
                            <li v-show="intPage > 1" @click="search(intPage = 1)" style="cursor:pointer;padding:4px 10px;background:#fff; border:1px solid #39c"><< </li>
                            <li v-show="intPage > 1" @click="search(--intPage)" style="cursor:pointer;padding:4px 10px;background:#fff; border:1px solid #39c" ><</li>
                            <li v-show="intPage < intTotalPages" @click="search(++intPage)" style="cursor:pointer;padding:4px 10px;background:#fff; border:1px solid #39c" >></li>
                            <li v-show="intPage < intTotalPages" @click="search(intPage = intTotalPages)" style="cursor:pointer;padding:4px 10px;background:#fff; border:1px solid #39c">>></li>
                        </ul>
                    </div>
				</article>
			</section>
		</form>

		<script src="node_modules/read-excel-file/bundle/read-excel-file.min.js"></script>
		<script src="node_modules/xlsx/dist/xlsx.full.min.js"></script>
        <script src="Librerias/vue/vue.min.js"></script>
		<script src="Librerias/vue/axios.min.js"></script>
		<script type="module" src="tesoreria/certificado_predial/buscar/teso-certificadoPredial.js?<?= date('d_m_Y_h_i_s');?>"></script>

	</body>
</html>
