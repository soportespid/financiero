<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require 'comun.inc';
	require 'funciones.inc';
	session_start();
	if(empty($_SESSION)){
		header("location: index.php");
	}
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html lang=es>
	<head>
		<meta charset="utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1"/>
		<meta name="viewport" content="user-scalable=no">
		<title>:: IDEAL 10 - Herramientas MIPG</title>
		<link href="favicon.ico" rel="shortcut icon"/>
		<link href="css/css2z.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/style.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
		<link href="css/tabs.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
		<link rel="stylesheet" href="sweetalert2/dist/sweetalert2.min.css">
		<script src="sweetalert2/dist/sweetalert2.min.js"></script>
		<script type="text/javascript" src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
	</head>
	<body>
		<section id="myapp" v-cloak>
			<div class="loading-container" v-show="isLoading" >
				<p class="text-loading"data-loading="IDEAL 10">IDEAL 10</p>
			</div>
			<div class="main-container">
				<header>
					<table>
						<tr><script>barra_imagenes("meci");</script><?php cuadro_titulos();?></tr>
					</table>
				</header>
				<nav>
					<?php menu_desplegable("meci");?>
					<div class="bg-white group-btn p-1">
						<button type="button" @click="save" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
							<span>Guardar</span>
							<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960"><path d="M840-680v480q0 33-23.5 56.5T760-120H200q-33 0-56.5-23.5T120-200v-560q0-33 23.5-56.5T200-840h480l160 160Zm-80 34L646-760H200v560h560v-446ZM480-240q50 0 85-35t35-85q0-50-35-85t-85-35q-50 0-85 35t-35 85q0 50 35 85t85 35ZM240-560h360v-160H240v160Zm-40-86v446-560 114Z"></path></svg>
						</button>
						<button type="button" onclick="location.href='mipg-tereasIdealBuscar.php'" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
							<span>Buscar</span>
							<svg class="fill-black group-hover:fill-white w-5 h-5" xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960"><path d="M784-120 532-372q-30 24-69 38t-83 14q-109 0-184.5-75.5T120-580q0-109 75.5-184.5T380-840q109 0 184.5 75.5T640-580q0 44-14 83t-38 69l252 252-56 56ZM380-400q75 0 127.5-52.5T560-580q0-75-52.5-127.5T380-760q-75 0-127.5 52.5T200-580q0 75 52.5 127.5T380-400Z"></path></svg>
						</button>
						<button type="button" @click="mypop=window.open('meci-principal.php','',''); mypop.focus();" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
							<span>Nueva ventana</span>
							<svg class="fill-black group-hover:fill-white w-5 h-5" xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960"><path d="M320-120v-80h80v-80H160q-33 0-56.5-23.5T80-360v-400q0-33 23.5-56.5T160-840h640q33 0 56.5 23.5T880-760v400q0 33-23.5 56.5T800-280H560v80h80v80H320ZM160-360h640v-400H160v400Zm0 0v-400 400Z"></path></svg>
						</button>
					</div>
				</nav>
				<article>
					<!--TABS-->
					<div ref="rTabs" class="nav-tabs bg-white p-1">
						<div class="nav-item active" @click="showTab(1)">&nbsp&nbspInformación General&nbsp&nbsp</div>
						<div class="nav-item"  @click="showTab(2)">&nbsp&nbspInformación Remitente&nbsp&nbsp</div>
						<div class="nav-item"  @click="showTab(3)">&nbsp&nbspResponsables&nbsp&nbsp</div>
						<div class="nav-item"  @click="showTab(4)">&nbsp&nbspArchivos Adjuntos&nbsp&nbsp</div>
						<div class="nav-item"  @click="showTab(5)">&nbsp&nbspHistorial&nbsp&nbsp</div>
						<div class="nav-item"  @click="showTab(6)">&nbsp&nbspResponder&nbsp&nbsp</div>
						<div class="nav-item"  @click="showTab(7)">&nbsp&nbspRedirigir&nbsp&nbsp</div>
					</div>
					<!--CONTENIDO TABS-->
					<div ref="rTabsContent" class="nav-tabs-content bg-white">
						<div class="nav-content active"><!--Pestaña 1-->
							<h2 class="titulos m-0">Radicar documento</h2>
							<div class="bg-white">
								<p class="m-2">Todos los campos con (<span class="text-danger fw-bolder">*</span>) son obligatorios</p>
								<div class="d-flex w-100">
									<div class="form-control w-25">
										<label class="form-label" for="">Divipola<span class="text-danger fw-bolder">*</span>:</label>
										<input type="text" class="text-center" v-model="txtDivipola" disabled readonly>
									</div>
									<div class="form-control">
										<label class="form-label" for="">Número Radicación<span class="text-danger fw-bolder">*</span>:</label>
										<div class="d-flex">
											<button type="button" class="btn btn-primary" @click="editItem('prev')"><</button>
											<input type="text" class="text-center" v-model="txtRadicado" disabled readonly>
											<button type="button" class="btn btn-primary" @click="editItem('next')">></button>
										</div>
									</div>
									<div class="form-control">
										<label class="form-label" for="">Fecha<span class="text-danger fw-bolder">*</span>:</label>
										<input type="text" class="text-center" v-model="txtFecha" disabled readonly>
									</div>
									<div class="form-control">
										<label class="form-label" for="">hora<span class="text-danger fw-bolder">*</span>:</label>
										<input type="text" class="text-center" v-model="txtHora" disabled readonly>
									</div>
								</div>
								<div class="d-flex w-100">
									<div class="form-control">
										<label class="form-label" for="">Tipo Radicación<span class="text-danger fw-bolder">*</span>: </label>
										<select id="labelSelectName2" v-model="selectTipoRadica" disabled>
											<option value="0">:::: Seleccione.... :::</option>
											<option v-for="(data,index) in arrTipoRadica" :key="index" :value="data">
											{{index + 1}} - {{ data.nombre}} ({{data.tipoPQR}})
											</option>
										</select>
									</div>
									<div class="form-control w-25">
										<label class="form-label" for="">Fecha Limite<span class="text-danger fw-bolder">*</span>:</label>
										<input type="text" class="text-center" v-model="txtFechaLimite" disabled readonly>
									</div>
								</div>
								<div class="d-flex w-100">
									<div class="form-control">
										<label class="form-label" for="">Descripción<span class="text-danger fw-bolder">*</span>:</label>
										<input type="text" class="text-center" v-model="txtDescripcion" disabled readonly>
									</div>
								</div>
								<div class="d-flex w-100">
									<div class="form-control flex-row w-15 align-items-center">
										<label class="form-label m-0" for="">Telefónica:</label>
										<input type="checkbox" v-model="checkTelefonica" disabled>
									</div>
									<div class="form-control flex-row w-15 align-items-center">
										<label class="form-label m-0" for="">Escrita:</label>
										<input type="checkbox" v-model="checkEscrita" disabled>
									</div>
									
									<div class="form-control flex-row w-15 align-items-center">
										<label class="form-label m-0" for="">Email:</label>
										<input type="checkbox" v-model="checkEmail" disabled>
									</div>
									<div class="form-control w-25">
										<label class="form-label" for="">Formato de Solicitud<span class="text-danger fw-bolder">*</span>:</label>
										<select id="labelselectTipopqr" v-model="selectTipopqr" disabled>
											<option value="O" selected>O - Oficio</option>
											<option value="F" selected>F - Formato PQRSDF</option>
											<option value="E" selected>E - Correo Electrónico/option>
											<option value="P" selected>P - Pagina WEB</option>
										</select>
									</div>
									<div class="form-control">
										<label class="form-label" for="">Folios<span class="text-danger fw-bolder">*</span>:</label>
										<input type="text" class="text-center" v-model="txtFolios" disabled readonly>
									</div>
								</div>
							</div>
						</div>
						<div class="nav-content"><!--Pestaña 2-->
							<h2 class="titulos m-0">Datos del Remitente</h2>
							<div class="bg-white">
								<div class="d-flex w-100">
									<div class="form-control w-25">
										<label class="form-label" for="">Documento Remitente<span class="text-danger fw-bolder">*</span>:</label>
										<input type="text"  class="text-center" v-model="objTercero.documento" disabled readonly>
									</div>
									<div class="form-control">
										<label class="form-label" for="">Nombre Remitente<span class="text-danger fw-bolder">*</span>:</label>
										<input type="text" v-model="objTercero.nombre" disabled readonly>
									</div>
								</div>
								<div class="d-flex w-100">
									<div class="form-control">
										<label class="form-label" for="">Dirección<span class="text-danger fw-bolder">*</span>:</label>
										<input type="text" class="text-center" v-model="objTercero.direccion" disabled readonly>
									</div>
								</div>
								<div class="d-flex w-100">
									<div class="form-control">
										<label class="form-label" for="">Email<span class="text-danger fw-bolder">*</span>:</label>
										<input type="text" class="text-center" v-model="objTercero.email" disabled readonly>
									</div>
								</div>
								<div class="d-flex w-100">
									<div class="form-control w-25">
										<label class="form-label" for="">Telefono<span class="text-danger fw-bolder">*</span>:</label>
										<input type="text" class="text-center" v-model="objTercero.telefono" disabled readonly>
									</div>
									<div class="form-control w-25">
										<label class="form-label" for="">Celular<span class="text-danger fw-bolder">*</span>:</label>
										<input type="text" class="text-center" v-model="objTercero.celular" disabled readonly>
									</div>
									<div class="form-control">
									</div>
								</div>
							</div>
						</div>
						<div class="nav-content"><!--Pestaña 3-->
							<h2 class="titulos m-0">Responsables</h2>
							<div class="bg-white">
								<div class="table-responsive">
									<table class="table table-hover fw-normal">
										<thead>
											<tr class="text-center">
												<th>documento</th>
												<th>nombre</th>
												<th>cargo</th>
												<th>Dependencia</th>
												<th>Responsable</th>
												<th>Estado</th>
											</tr>
										</thead>
										<tbody>
											<tr v-for="(data,index) in listaFuncionarios" :key="index">
												<td class="text-center">{{data.funDocumento}}</td>
												<td>{{data.funNombre}}</td>
												<td class="text-center">{{data.funNombreCargo}}</td>
												<td class="text-center">{{data.funNombreArea}}</td>
												<td class="text-center">
													<span :class="[data.funResponsable == 1 ? 'badge-success' : 'badge-primary']" class="badge">{{ data.funResponsable == 1 ? "Responsable" : "Lector"}}</span>
												</td>
												<td class="text-center">
													<span :class="[data.funEstado =='S' ? 'badge-success' : 'badge-danger']"class="badge">{{ data.funEstado == "S" ? "Activo" : "Inactivo"}}</span>
												</td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
						</div>
						<div class="nav-content"><!--Pestaña 4-->
							<h2 class="titulos m-0">Archivos Adjuntos</h2>
							<div class="bg-white">
								<div class="table-responsive">
									<table class="table table-hover fw-normal">
										<thead>
											<tr class="text-center">
												<th>Nombre del Archivo</th>
												<th>Tamaño</th>
												<th>Acciones</th>
											</tr>
										</thead>
										<tbody>
											<tr v-for="(archivo, index) in listaArchivos" :key="index">
												<td>{{ archivo.name }}</td>
												<td>{{archivo.size}}{{ (archivo.size / 1024).toFixed(2) }} KB</td>
												<td class="text-center"> 
													<div class="form-control justify-between w-50" style="margin: 0 auto;"> 
														<label for=""></label> 
														<button type="button" class="btn btn-primary" @click="descargarArchivo(archivo)"> Descargar </button> 
													</div> 
												</td>
												
											</tr>
										</tbody>
									</table>
								</div>
							</div>
						</div>
						<div class="nav-content"><!--Pestaña 5-->
							<h2 class="titulos m-0">Historial</h2>
							<div class="bg-white">
								<div class="table-responsive">
								<table class="table table-hover fw-normal">
										<thead>
											<tr class="text-center">
												<th>item</th>
												<th>Fecha Rad.</th>
												<th>Hora Rad.</th>
												<th>Fecha Res.</th>
												<th>Hora Res.</th>
												<th>Asignado por</th>
												<th>Contestado por</th>
												<th>Solicitud o Respuesta</th>
												<th>Responsable</th>
												<th>Estado</th>
											</tr>
										</thead>
										<tbody>
											<tr v-for="(data,index) in listaFuncionarios" :key="index">
												<td class="text-center">{{index + 1}}</td>
												<td class="text-center">{{data.funFechasig}}</td>
												<td class="text-center">{{data.funHorasig}}</td>
												<td class="text-center">{{data.funFechares}}</td>
												<td class="text-center">{{data.funHorares}}</td>
												<td class="text-center">{{data.funAsignado}}</td>
												<td class="text-center">{{data.funNombre}}</td>
												<td class="text-center">{{data.funRespuesta}}</td>
												<td class="text-center">
													<span :class="[data.funResponsable == 1 ? 'badge-success' : 'badge-primary']" class="badge">{{ data.funResponsable == 1 ? "Responsable" : "Lector"}}</span>
												</td>
												<td class="text-center">
													<span :class="[data.funColorestado === '0' ? 'badge-warning' : '', data.funColorestado === '1' ? 'badge-success' : '', data.funColorestado === '2' ? 'badge-primary' : '']" class="badge">{{ data.funEstadopro === "LN" ? "Sin Revisar" : (data.funEstadopro === "LS" ? "Revisada" : (data.funEstadopro === "AN" ? "Sin Responder" : (data.funEstadopro === "AC" ? "Contestada" : (data.funEstadopro === "AR" ? "Redirigido" : "Desconocido")))) }}
													</span>
												</td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
						</div>
						<div class="nav-content"><!--Pestaña 6-->
							<h2 class="titulos m-0">Responder a Documento Radicado</h2>
							<div class="bg-white">
								<div class="d-flex">
									<div class="form-control">
										<label class="form-label m-0" for="">Respuesta:</label>
										<textarea class="resize-vertical" id="txtRespuesta" name="txtRespuesta" rows="6" v-model="txtRespuesta"></textarea>
									</div>
								</div>
							</div>
							<h2 class="titulos m-0">Cargar Archivos Adjuntos</h2>
							<div class="bg-white">
								<div class="d-flex w-100">
									<div class="form-control w-75">
										<label class="form-label" for="inputFile">Seleccionar Archivo<span class="text-danger fw-bolder">*</span>:</label>
										<input type="file" id="inputFile" @change="agregarArchivo">
									</div>
									<div class="form-control justify-between w-25">
										
										
									</div>
								</div>
								<div class="table-responsive">
									<table class="table table-hover fw-normal">
										<thead>
											<tr class="text-center">
												<th>Nombre del Archivo</th>
												<th>Tamaño</th>
												<th>Acciones</th>
											</tr>
										</thead>
										<tbody>
											<tr v-for="(archivo, index) in listaArchivos" :key="index">
												<td>{{ archivo.name }}</td>
												<td>{{ (archivo.size / 1024).toFixed(2) }} KB</td>
												<td class="text-center">
													<button type="button" class="btn btn-danger btn-sm m-1" @click="eliminarArchivo(index)">
														Eliminar
													</button>
												</td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
						</div>
						<div class="nav-content"><!--Pestaña 7-->
							<h2 class="titulos m-0">Redirigir Documento Radicado</h2>
							<div class="bg-white">
								<div class="d-flex">
									<div class="form-control">
										<label class="form-label m-0" for="">Concepto:</label>
										<textarea class="resize-vertical" id="txtConcepto" name="txtConcepto" rows="6" v-model="txtConcepto"></textarea>
									</div>
								</div>
							</div>
							<h2 class="titulos m-0">Elegir Nuevo Responsable</h2>
							<div class="d-flex w-100">
								<div class="form-control w-25">
									<label class="form-label" for="">Documento Funcionario<span class="text-danger fw-bolder">*</span>:</label>
									<input type="text"  class="colordobleclik" @dblclick="isModal2=true" @change="search2('documento', 'objFuncionario')" v-model="objFuncionario.documento">
								</div>
								<div class="form-control">
									<label class="form-label" for="">Nombre Funcionario<span class="text-danger fw-bolder">*</span>:</label>
									<input type="text" v-model="objFuncionario.nombre" disabled readonly>
								</div>
								<div class="form-control justify-between w-25">
									<label for=""></label>
								</div>
							</div>
						</div>
					</div>
				</article>
				<!-- MODALES -->
				<div v-show="isModal2" class="modal">
					<div class="modal-dialog modal-lg" >
						<div class="modal-content">
							<div class="modal-header">
								<h5 class="modal-title">Buscar Funcionarios</h5>
								<button type="button" @click="isModal2=false;" class="btn btn-close"><div></div><div></div></button>
							</div>
							<div class="modal-body">
								<div class="d-flex flex-column">
									<div class="form-control m-0 mb-3">
										<input type="search" placeholder="Buscar" v-model="txtBuscar2" @keyup="search2()" id="labelInputName2">
									</div>
									<div class="form-control m-0 mb-3">
										<label class="form-label" for="labelInputName2">Resultados: <span class="fw-bold">{{txtResultados2}}</span></label>
									</div>
								</div>
								<div class="overflow-auto max-vh-50 overflow-x-hidden" >
									<table class="table table-hover fw-normal">
										<thead>
											<tr>
												<th>Item</th>
												<th>Nombre</th>
												<th>Documento</th>
												<th>Cargo</th>
											</tr>
										</thead>
										<tbody>
											<tr v-for="(data,index) in arrFuncionariosCopy" @dblclick="selectItem2(data)" :key="index">
												<td>{{index+1}}</td>
												<td>{{data.nombre}}</td>
												<td>{{data.documento}}</td>
												<td>{{data.nombrecargo}}</td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div> 
			</div>
		</section>
		<script src="node_modules/read-excel-file/bundle/read-excel-file.min.js"></script>
		<script src="node_modules/xlsx/dist/xlsx.full.min.js"></script>
		<script src="Librerias/vue/vue.min.js"></script>
		<script src="Librerias/vue/axios.min.js"></script>
		<script type="module" src="vue/herramientas_mipg/tareas_ideal/responder/mipg-tereasIdealResponder.js?<?= date('d_m_Y_h_i_s');?>"></script>
	</body>
</html>