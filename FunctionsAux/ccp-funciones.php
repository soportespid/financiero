<?php
/**
 * Switch case para seleccionar el proceso el llamado al controlador pertinente
 * Se evalua el valor obtenido de $_POST
 */
include_once ($_SERVER['DOCUMENT_ROOT'].'/financiero/dirs.php');
require_once (CONTROLLERS_PATH.'CcpetAcuerdosController.php');

switch ($_POST['proceso']) {
	case 'CCPETACUERDOS_BUSCAR':
	case 'CCPETACUERDOS_FILTRAR':
		$CcpetAcuerdos = new CcpetAcuerdosController();
		$ResultCcpetAcuerdos = $CcpetAcuerdos->buscarAcuerdos($_POST);
		echo $ResultCcpetAcuerdos;
		break;
	case 'CCPETACUERDOS_GUARDAR';
	case 'CCPETACUERDOS_EDITAR':
		$CcpetAcuerdos = new CcpetAcuerdosController();
		$ResultCcpetAcuerdos = $CcpetAcuerdos->guardarAcuerdos($_POST);
		echo $ResultCcpetAcuerdos;
		break;
	case 'CCPETACUERDOS_ANULAR':
		$CcpetAcuerdos = new CcpetAcuerdosController();
		$ResultCcpetAcuerdos = $CcpetAcuerdos->anularAcuerdos($_POST);
		echo $ResultCcpetAcuerdos;
	default:break;
}
?>