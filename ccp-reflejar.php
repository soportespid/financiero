<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
    header("Content-Type: text/html;charset=utf8");
    require 'comun.inc';
    require 'funciones.inc';
    session_start();
    $linkbd = conectar_v7();
    $linkbd -> set_charset("utf8");
    cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
    date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html lang=es>
	<head>
		<meta charset="utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1"/>
		<meta name="viewport" content="user-scalable=no">
		<title>:: IDEAL 10 - Presupuesto CCPET</title>
		<link href="favicon.ico" rel="shortcut icon"/>
        <link href="css/css2.css" rel="stylesheet" type="text/css" />
        <link href="css/css3.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="css/programas.js"></script>
    </head>
    <body>

        <IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
        <span id="todastablas2"></span>
        <table>
            <tr><script>barra_imagenes("ccpet");</script><?php cuadro_titulos();?></tr>	 
            <tr><?php menu_desplegable("ccpet");?></tr>
        	<tr>
          		<td colspan="3" class="cinta">
					<a class="mgbt"><img src="imagenes/add2.png" /></a>
					<a class="mgbt"><img src="imagenes/guardad.png" style="width:24px;"/></a>
					<a class="mgbt"><img src="imagenes/buscad.png"/></a>
					<a onClick="mypop=window.open('plan-agenda.php','','');mypop.focus()" class="mgbt"><img src="imagenes/agenda1.png" title="Agenda" /></a>
					<a href="#" onClick="mypop=window.open('ccp-principal.php','','');mypop.focus();" class="mgbt"><img src="imagenes/nv.png" title="Nueva Ventana"></a>
				</td>
        	</tr>
        </table>
        <?php
    	$query = "SELECT conta_pago FROM tesoparametros";
		$resultado = mysqli_query($linkbd, $query);
		$arreglo = mysqli_fetch_row($resultado);
		$opcion=$arreglo[0];
        ?>
 		<form name="form2" method="post" action="">
			<table class="inicio">
      			<tr>
        			<td class="titulos" colspan="2">.: Reflejar Documentos Presupuesto CCPET</td>
        			<td class="cerrar" style="width:7%;" ><a href="ccp-principal.php">&nbsp;Cerrar</a></td>
      			</tr>
      			<tr>
					<td class="titulos2" colspan="3">Reflejar Presupuesto</td></tr>
     			<tr>
				<td class='saludo1' width='70%'>
					<ol id="lista2">
						<table>
							<tr>
								<td style="width:50%;">
									
									<li><a href='ccp-recibocaja-reflejarppto.php'>Recibos de Caja	</a></li>
									<li><a href='ccp-sinrecibocaja-reflejar.php'>Ingresos Internos</a></li>
									<li><a href='ccp-recaudotransferencia-reflejar.php'>Recaudos transferencias</a></li>
									<!-- <li><a href='presu-notasbancarias-reflejar.php'>Notas bancarias</a></li> -->
									<!-- <li><a <?php /* if($opcion=="1"){ echo "href='presu-egreso-reflejar.php' "; }else{ echo "href='presu-girarcheques-reflejar.php' "; } */ ?>  >Retenciones</a></li>  -->
									<!-- <li><a href='presu-sinrecibocajasp-reflejar.php'>Ingresos SP</a></li>   -->
								</td>
							
							</tr>
						</table>
					</ol>
				</td>  <td colspan="2" rowspan="1" style="background:url(imagenes/reflejar.png); background-repeat:no-repeat; background-position:center; background-size: 100% 100%"></td>
				</tr>							
    		</table>
		</form>
	</body>
</html>