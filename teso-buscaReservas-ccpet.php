<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=iso-8859-1");
	require "comun.inc";
	require "funciones.inc";
	session_start();
	$linkbd=conectar_v7();
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es">
	<head>
		<meta http-equiv="Content-type" content="text/html;" charset="iso-8859-1"/>
		<meta http-equiv="X-UA-Compatible" content="IE=9"/>
		<title>:: IDEAL 10 - Tesorer&iacute;a</title>
		<link href="css/css2.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<script type="text/javascript" src="jquery-1.11.0.min.js"></script> 
		<script type="text/javascript" src="css/programas.js"></script>
        <script type="text/javascript" src="css/calendario.js"></script>
		<script>
			function verUltimaPos(idcta, filas, filtro)
			{
				var scrtop=$('#divdet').scrollTop();
				var altura=$('#divdet').height();
				var numpag=$('#nummul').val();
				var limreg=$('#numres').val();
				if((numpag<=0)||(numpag=="")){numpag=0;}
				if((limreg==0)||(limreg=="")){limreg=10;}
				numpag++;
				location.href="teso-verReservas-ccpet.php?is="+idcta;
			}
            function buscarbotonfiltro()
            {
                if((document.form2.fecha.value != "" && document.form2.fecha2.value == "") || (document.form2.fecha.value == "" && document.form2.fecha2.value != "")){
                    alert("Falta digitar fecha");
                }else{
                    document.getElementById('numpos').value=0;
                    document.getElementById('nummul').value=0;
                    document.form2.submit();
                }
                
            }
			function despliegamodalm(_valor,_tip,mensa,pregunta)
			{
				document.getElementById("bgventanamodalm").style.visibility=_valor;
				if(_valor=="hidden"){document.getElementById('ventanam').src="";}
				else
				{
					switch(_tip)
					{
						case "1":	document.getElementById('ventanam').src="ventana-mensaje1.php?titulos="+mensa;break;
						case "2":	document.getElementById('ventanam').src="ventana-mensaje3.php?titulos="+mensa;break;
						case "3":	document.getElementById('ventanam').src="ventana-mensaje2.php?titulos="+mensa;break;
						case "4":	document.getElementById('ventanam').src="ventana-consulta2.php?titulos="+mensa+"&idresp="+pregunta;break;	
					}
				}
			}
			function funcionmensaje(){}
			function respuestaconsulta(estado,pregunta)
			{
				if(estado=="S")
				{
					switch(pregunta)
					{
						case "1":	document.form2.cambioestado.value="1";break;
						case "2":	document.form2.cambioestado.value="0";break;
					}
				}
				else
				{
					switch(pregunta)
					{
						case "1":	document.form2.nocambioestado.value="1";break;
						case "2":	document.form2.nocambioestado.value="0";break;
					}
				}
				document.form2.submit();
			}
			function cambioswitch(id,valor)
			{
				document.getElementById('idestado').value=id;
				if(valor==1){despliegamodalm('visible','4','Desea anular este Superavit','1');}
				else{despliegamodalm('visible','4','Desea desanular este Superavit','2');}
			}
			function crearexcel(){
				document.form2.action="teso-buscareservasexcel.php";
				document.form2.target="_BLANK";
				document.form2.submit();
				document.form2.action="";
				document.form2.target="";
			}
		</script>
		<?php 
			titlepag();

            if(isset($_GET['fini']) && isset($_GET['ffin'])){
				if(!empty($_GET['fini']) && !empty($_GET['ffin'])){
					$_POST['fecha']=$_GET['fini'];
					$_POST['fecha2']=$_GET['ffin'];
				}
			}
			preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST['fecha'],$fech1);
			preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST['fecha2'],$fech2);
			$f1=@ $fech1[3]."-".@ $fech1[2]."-".@ $fech1[1];
			$f2=@ $fech2[3]."-".@ $fech2[2]."-".@ $fech2[1];
            
			$scrtop= @ $_GET['scrtop'];
			if($scrtop=="") $scrtop=0;
			echo"<script>
				window.onload=function(){
					$('#divdet').scrollTop(".$scrtop.")
				}
			</script>";
			$gidcta=@ $_GET['idcta'];
			if(isset($_GET['filtro']))
			$_POST['nombre']=$_GET['filtro'];
		?>
	</head>
	<body>
		<IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
		<span id="todastablas2"></span>
		<table>
			<tr><script>barra_imagenes("teso");</script><?php cuadro_titulos();?></tr>	 
			<tr><?php menu_desplegable("teso");?></tr>
			<tr>
				<td colspan="3" class="cinta">
                    <a href="teso-reservas-ccpet.php" class="mgbt" ><img src="imagenes/add.png" title="Nuevo" /></a>
                    <a class="mgbt"><img src="imagenes/guardad.png"/>
                    <a class="mgbt"><img src="imagenes/buscad.png"/></a>
					<a onClick="mypop=window.open('plan-agenda.php','','');mypop.focus()" class="mgbt"><img src="imagenes/agenda1.png" title="Agenda"/></a>
                    <a onClick="mypop=window.open('teso-principal.php','','');mypop.focus();" class="mgbt"><img src="imagenes/nv.png" title="Nueva ventana"></a>
					<a onClick="mypop=window.open('<?php echo $url2; ?>','','');mypop.focus();" class="tooltip bottom mgbt"><img src="imagenes/duplicar_pantalla.png" title="Duplicar pesta&ntilde;a"></a>
					<a onclick="crearexcel()" class="mgbt"><img src="imagenes/excel.png" title="Excel"></a> 
                </td>
			</tr>
		</table>
		<?php
			if(@ $_POST['oculto']=="")
			{
				$_POST['cambioestado']="";
				$_POST['nocambioestado']="";
				$_POST['numres']=10;$_POST['numpos']=0;$_POST['nummul']=0;
			}
			if(@ $_GET['numpag']!="")
			{
				if(@ $_POST['oculto']!=2)
				{
					$_POST['numres']=$_GET['limreg'];
					$_POST['numpos']=$_GET['limreg']*($_GET['numpag']-1);
					$_POST['nummul']=$_GET['numpag']-1;
				}
			}
			else
			{
				if(@ $_POST['nummul']=="")
				{
					$_POST['numres']=10;
					$_POST['numpos']=0;
					$_POST['nummul']=0;
				}
			}
		?>
		<div id="bgventanamodalm" class="bgventanamodalm">
			<div id="ventanamodalm" class="ventanamodalm">
				<IFRAME src="" name="ventanam" marginWidth=0 marginHeight=0 frameBorder=0 id="ventanam" frameSpacing=0 style=" width:700px; height:130px; top:200; overflow:hidden;"></IFRAME>
			</div>
		</div>
		<form name="form2" method="post" action="">
			<input type="hidden" name="numres" id="numres" value="<?php echo @ $_POST['numres'];?>"/>
			<input type="hidden" name="numpos" id="numpos" value="<?php echo @ $_POST['numpos'];?>"/>
			<input type="hidden" name="nummul" id="nummul" value="<?php echo @ $_POST['nummul'];?>"/>
			<input type="hidden" name="oculto" id="oculto" value="1">
			<input type="hidden" name="cambioestado" id="cambioestado" value="<?php echo @ $_POST['cambioestado'];?>">
			<input type="hidden" name="nocambioestado" id="nocambioestado" value="<?php echo @ $_POST['nocambioestado'];?>">
			<input type="hidden" name="idestado" id="idestado" value="<?php echo @ $_POST['idestado'];?>">
			<?php
				if(@ $_POST['cambioestado']!="")
				{
					if(@ $_POST['cambioestado']=="1")
					{
						$sqlr="UPDATE tesoreservas SET estado='S' WHERE id='".$_POST['idestado']."'";
						mysqli_query($linkbd,$sqlr);
					}
					else
					{
						$sqlr="UPDATE tesoreservas SET estado='N' WHERE id='".$_POST['idestado']."'";
						mysqli_query($linkbd,$sqlr);
					}
					echo"<script>document.form2.cambioestado.value=''</script>";
				}
				if(@ $_POST['nocambioestado']!="")
				{
					if(@ $_POST['nocambioestado']=="1"){$_POST['lswitch1'][$_POST['idestado']]=1;}
					else {$_POST['lswitch1'][$_POST['idestado']]=0;}
					echo"<script>document.form2.nocambioestado.value=''</script>";
				}
			?>
			<table class="inicio">
				<tr>
					<td class="titulos" colspan="9">:: Buscar Reservas</td>
					<td class="cerrar" style="width:7%" onClick="location.href='serv-principal.php'">Cerrar</td>
				</tr>
				<tr>
					<td class="tamano01" style='width:4cm;'>:: ID acuerdo:</td>
					<td ><input type="search" name="id" id="id" value="<?php echo @ $_POST['id'];?>" style='width:100%;height:30px;'/></td>
					<td class="tamano01" style='width:4cm;'>:: Numero acuerdo:</td>
					<td ><input type="search" name="numero" id="numero" value="<?php echo @ $_POST['numero'];?>" style='width:100%;height:30px;'/></td>
                    <td class="saludo1" style="width:2cm;">Fecha inicial:</td>
                    <td style="width:10%;"><input name="fecha"  type="text" value="<?php echo @ $_POST['fecha']?>" maxlength="10"  onKeyPress="javascript:return solonumeros(event)" onKeyUp="return tabular(event,this)"  id="fc_1198971545" onKeyDown="mascara(this,'/',patron,true)" style="width:75%;height:30px;" title="DD/MM/YYYY" placeholder="DD/MM/YYYY"/>&nbsp;<img src="imagenes/calendario04.png" onClick="displayCalendarFor('fc_1198971545');" title="Calendario" class="icobut"/></td>
                    <td class="saludo1" style="width:2cm;">Fecha final:</td>
                    <td style="width:10%;"><input name="fecha2" type="text" value="<?php echo @ $_POST['fecha2']?>" maxlength="10"  onKeyPress="javascript:return solonumeros(event)" onKeyUp="return tabular(event,this)"  id="fc_1198971546" onKeyDown="mascara(this,'/',patron,true)" style="width:75%;height:30px;" title="DD/MM/YYYY" placeholder="DD/MM/YYYY"/>&nbsp;<img src="imagenes/calendario04.png" onClick="displayCalendarFor('fc_1198971546');" title="Calendario" class="icobut"/></td>
					<td style="padding-bottom:0px"><em class="botonflecha" onClick="buscarbotonfiltro();">Buscar</em></td>
				</tr> 
			</table>
            <input type="hidden" name="fecham1"  id="fecham1" value="<?php echo  @ $_POST['fecham1']; ?>"/>
    		<input type="hidden" name="fecham2" id="fecham2" value="<?php echo @ $_POST['fecham2']; ?>"/>
			<div class="subpantallac5" style="height:69%; width:99.6%; margin-top:0px; overflow-x:hidden" id="divdet">
			<?php
                @ $_POST['fecham1']=$f1;
                @ $_POST['fecham2']=$f2;

                $crit2='';

				if (@ $_POST['id']!=""){$crit1="AND id LIKE '".$_POST['id']."'";}

				if (@ $_POST['numero']!=""){$crit2="AND id_acuerdo LIKE '".$_POST['numero']."'";}
                    
                $sqlr="SELECT id,id_acuerdo,concepto,fecha,valor,estado FROM tesoreservas WHERE estado = 'S' $crit1 $crit2 ORDER BY id ASC";                             
                if(isset($_POST['fecha']) && isset($_POST['fecha2'])){
                    if(!empty($_POST['fecha']) && !empty($_POST['fecha2'])){
                        $sqlr="SELECT id,id_acuerdo,concepto,fecha,valor,estado FROM tesoreservas WHERE estado = 'S' $crit1 $crit2 AND fecha BETWEEN '$f1' AND '$f2' ORDER BY id ASC";
                    }
                }

                $resp = mysqli_query($linkbd,$sqlr);
				$ntr = mysqli_num_rows($resp);
                @ $_POST['numtop']= $ntr;
                $nuncilumnas=ceil($_POST['numtop']/$_POST['numres']);

                $cond2="";

                if (@ $_POST['numres']!="-1")
                {
                    $cond2="LIMIT ".$_POST['numpos'].", ".$_POST['numres']; 
                }

                $sqlr="SELECT id,id_acuerdo,concepto,fecha,valor,estado FROM tesoreservas WHERE estado = 'S' $crit1 $crit2 ORDER BY id ASC $cond2";
                if(isset($_POST['fecha']) && isset($_POST['fecha2'])){
                    if(!empty($_POST['fecha']) && !empty($_POST['fecha2'])){
                        $sqlr="SELECT id,id_acuerdo,concepto,fecha,valor,estado FROM tesoreservas WHERE estado = 'S' AND fecha BETWEEN '$f1' AND '$f2' $crit1 $crit2 ORDER BY id ASC $cond2";
                    }
                }

				$resp = mysqli_query($linkbd,$sqlr);
				$con=1;
				$numcontrol=$_POST['nummul']+1;
				if(($nuncilumnas==$numcontrol)||(@ $_POST['numres']=="-1"))
				{
					$imagenforward="<img src='imagenes/forward02.png' style='width:17px;cursor:default;'>";
					$imagensforward="<img src='imagenes/skip_forward02.png' style='width:16px;cursor:default;' >";
				}
				else 
				{
					$imagenforward="<img src='imagenes/forward01.png' style='width:17px;cursor:pointer;' title='Siguiente' onClick='numsiguiente()'>";
					$imagensforward="<img src='imagenes/skip_forward01.png' style='width:16px;cursor:pointer;' title='Fin' onClick='saltocol(\"$nuncilumnas\")'>";
				}
				if((@ $_POST['numpos']==0)||(@ $_POST['numres']=="-1"))
				{
					$imagenback="<img src='imagenes/back02.png' style='width:17px;cursor:default;'>";
					$imagensback="<img src='imagenes/skip_back02.png' style='width:16px;cursor:default;'>";
				}
				else
				{
					$imagenback="<img src='imagenes/back01.png' style='width:17px;cursor:pointer;' title='Anterior' onClick='numanterior();'>";
					$imagensback="<img src='imagenes/skip_back01.png' style='width:16px;cursor:pointer;' title='Inicio' onClick='saltocol(\"1\")'>";
				}
				$con=1;
				echo "
					<table class='inicio'>
						<tr>
							<td colspan='5' class='titulos'>.: Resultados Busqueda:</td>
							<td class='submenu'>
                                <select name='renumres' id='renumres' onChange='cambionum();' style='width:100%'>
                                    <option value='10'"; if (@ $_POST['renumres']=='10'){echo 'selected';} echo ">10</option>
                                    <option value='20'"; if (@ $_POST['renumres']=='20'){echo 'selected';} echo ">20</option>
                                    <option value='30'"; if (@ $_POST['renumres']=='30'){echo 'selected';} echo ">30</option>
                                    <option value='50'"; if (@ $_POST['renumres']=='50'){echo 'selected';} echo ">50</option>
                                    <option value='100'"; if (@ $_POST['renumres']=='100'){echo 'selected';} echo ">100</option>
                                    <option value='-1'"; if (@ $_POST['renumres']=='-1'){echo 'selected';} echo ">Todos</option>
                                </select>
						    </td>
						</tr>
						<tr>
							<td colspan='2' id='RecEnc'>Acuerdos encontrados: </td>
						</tr>
						<tr>
                            <td width='5%' style='text-align:center;' class='titulos2'>ID</td>
                            <td width='5%' style='text-align:center;' class='titulos2'>Acuerdo</td>
                            <td style='text-align:center;' class='titulos2'>Concepto</td>
                            <td width='10%' style='text-align:center;' class='titulos2'>Fecha</td>
                            <td width='10%' style='text-align:center;' class='titulos2'>Valor</td>
                            <td class='titulos2' colspan='2' style='width:7%;' >Anular</td>
						</tr>";
						$iter='saludo1a';
						$iter2='saludo2';
						$filas=1;

						if($_POST['fecha'] == '' && $_POST['fecha2'] == '' && $_POST['numero'] == '' && $_POST['id'] == '')
						{
							echo "
							<table class='inicio'>
								<tr>
									<td class='saludo1' style='text-align:center;width:100%;font-size:25px'>Utilice el filtro de busqueda</td>
								</tr>
							</table>";
							$nuncilumnas = 0;
						}
						elseif(mysqli_num_rows($resp) == 0 || mysqli_num_rows($resp) == '0')
						{
							echo "
							<table class='inicio'>
								<tr>
									<td class='saludo1' style='text-align:center;width:100%;font-size:25px'>No hay resultados de su busqueda.</td>
								</tr>
							</table>";
						}
						else
						{
							while ($row =mysqli_fetch_row($resp))
							{
								$ntr2 = $ntr;

								echo "<script>document.getElementById('RecEnc').innerHTML = 'Acuerdos encontrados: $ntr2'</script>";

								if($row[5]=='S')
								{
									$imgsem="src='imagenes/sema_verdeON.jpg' title='Activo'";
									$coloracti="#0F0";
									$_POST['lswitch1'][$row[0]]=0;
								}
								else
								{
									$imgsem="src='imagenes/sema_rojoON.jpg' title='Inactivo'";
									$coloracti="#C00";
									$_POST['lswitch1'][$row[0]]=1;
								}
								if($gidcta!="")
								{
									if($gidcta==$row[0]){$estilo='background-color:yellow';}
									else {$estilo="";}
								}
								else {$estilo="";}
								$idcta="'$row[0]'";
								$numfil="'$filas'";

								echo"

									<input type='hidden' name='idE[]' value='".$row[0]."'>
									<input type='hidden' name='acuerdoE[]' value='".$row[1]."'>
									<input type='hidden' name='conceptoE[]' value='".$row[2]."'>
									<input type='hidden' name='fechaE[]' value='".$row[3]."'>
									<input type='hidden' name='valorE[]' value='".$row[4]."'>
									";

									if ($row[5]=='S')
									{
										echo"
										<input type='hidden' name='estadoE[]' value='ACTIVO'>";
									}
									if ($row[5]=='N')
									{
										echo"
										<input type='hidden' name='estadoE[]' value='INACTIVO'>";
									}
									if ($row[5]=='R')
									{
										echo"
										<input type='hidden' name='estadoE[]' value='INACTIVO'>";
									}

								$filtro="'".@ $_POST['nombre']."'";
								echo"<tr class='$iter' onDblClick=\"verUltimaPos($idcta, $numfil, $filtro)\" style='text-transform:uppercase; $estilo'>
									<td style='text-align:center;'>$row[0]</td>
									<td style='text-align:center;'>$row[1]</td>
									<td>$row[2]</td>
									<td style='text-align:center;'>$row[3]</td>
									<td style='text-align:center;'>$row[4]</td
									<td class='icoop' style='text-align:center;'><img  style='width:20px'/></td>
									<td style='text-align:center;'><input type='range' name='lswitch1[]' value='".$_POST['lswitch1'][$row[0]]."' min ='0' max='1' step ='1' style='background:$coloracti; width:60%' onChange='cambioswitch(\"$row[0]\",\"".$_POST['lswitch1'][$row[0]]."\")'/></td>
								</tr>";
								$aux=$iter;
								$iter=$iter2;
								$iter2=$aux;
								$filas++;
							}
						}
						if (@ $_POST['numtop']==0)
						{
							echo "
							<table class='inicio'>
								<tr>
									<td class='saludo1' style='text-align:center;width:100%'><img src='imagenes\alert.png' style='width:25px'>No hay coincidencias en la b&uacute;squeda<img src='imagenes\alert.png' style='width:25px'></td>
								</tr>
							</table>";
						}
						echo"
					</table>
					<table class='inicio'>
						<tr>
							<td style='text-align:center;'>
								<a>$imagensback</a>&nbsp;
								<a>$imagenback</a>&nbsp;&nbsp;";
				if($nuncilumnas<=9){$numfin=$nuncilumnas;}
				else{$numfin=9;}
				for($xx = 1; $xx <= $numfin; $xx++)
				{
					if($numcontrol<=9){$numx=$xx;}
					else{$numx=$xx+($numcontrol-9);}
					if($numcontrol==$numx){echo"<a onClick='saltocol(\"$numx\")'; style='color:#24D915;cursor:pointer;'> $numx </a>";}
					else {echo"<a onClick='saltocol(\"$numx\")'; style='color:#000000;cursor:pointer;'> $numx </a>";}
				}
				echo"			&nbsp;&nbsp;<a>$imagenforward</a>
								&nbsp;<a>$imagensforward</a>
							</td>
						</tr>
					</table>";	
			?>
			<input type="hidden" name="ocules" id="ocules" value="<?php echo @ $_POST['ocules'];?>">
			<input type="hidden" name="actdes" id="actdes" value="<?php echo @ $_POST['actdes'];?>">
			<input type="hidden" name="numtop" id="numtop" value="<?php echo @ $_POST['numtop'];?>" />
		</form>
		<div id="bgventanamodal2">
			<div id="ventanamodal2">
				<IFRAME  src="" name="buster" marginWidth=0 marginHeight=0 frameBorder=0 id="ventana2" frameSpacing=0 style="left:500px; width:900px; height:500px; top:200;"></IFRAME>
			</div>
		</div>
	</body>
</html>
