<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require "comun.inc";
	require "funciones.inc";
	require "funcionesSP.inc.php";
	session_start();
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es">
	<head>
		<meta http-equiv="Content-type" content="text/html; charset=iso-8859-1"/>
		<meta http-equiv="X-UA-Compatible" content="IE=9"/>
		<title>:: IDEAL 10 - Servicios P&uacute;blicos</title>
		<link href="css/css2.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/cssSP.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/sweetalert.css" rel="stylesheet" type="text/css"/>
		<script type="text/javascript" src="css/sweetalert.js"></script>
    	<script type="text/javascript" src="css/sweetalert.min.js"></script>
		<script type="text/javascript" src="css/programas.js"></script>
		<style>
		</style>
		<script>
			function despliegamodalm(_valor,_tip,mensa,pregunta)
			{
				document.getElementById("bgventanamodalm").style.visibility=_valor;
				if(_valor == "hidden")
				{
					document.getElementById('ventanam').src="";
				}
				else
				{
					switch(_tip)
					{
						case "1":	document.getElementById('ventanam').src="ventana-mensaje1.php?titulos="+mensa;break;
						case "2":	document.getElementById('ventanam').src="ventana-mensaje3.php?titulos="+mensa;break;
						case "3":	document.getElementById('ventanam').src="ventana-mensaje2.php?titulos="+mensa;break;
						case "4":	document.getElementById('ventanam').src="ventana-consulta1.php?titulos="+mensa+"&idresp="+pregunta;break;	
					}
				}
			}

			function funcionmensaje()
			{
				document.location.href = "serv-rutas.php";
			}

			function respuestaconsulta(pregunta)
			{
				switch(pregunta)
				{
					case "1":
						document.form2.oculto.value='2';
						document.form2.submit();
						break;
					case "2":
						document.form2.oculto.value="3";
						document.form2.submit();
						break;
				}
			}

			function guardar()
			{
				var codigo = document.getElementById('codban').value;
				var nombre = document.getElementById('nomban').value;

				if (codigo.trim() && nombre.trim()) 
				{
					despliegamodalm('visible','4','Esta Seguro de Guardar','1');
				}
				else 
				{
					despliegamodalm('visible','2','Falta informacion para crear la Ruta');
				}
			}

			function cambiocheck()
			{
				if(document.getElementById('myonoffswitch').value=='S'){document.getElementById('myonoffswitch').value='N';}
				else{document.getElementById('myonoffswitch').value='S';}
			}
			function fagregar()
			{
				if(document.form2.idbarrio.value != "-1")
				{
					document.form2.oculto.value="4";
					document.form2.submit();
				}
				else
				{
					despliegamodalm('visible','2','Falta seleccionar un barrio para agregar');
				}
			}

			function eliminar(variable)
			{
				document.getElementById('elimina').value = variable;

				despliegamodalm('visible','4','Esta Seguro de Eliminar','2');
			}

			function validateCode() 
			{
				var validacion01 = document.getElementById('codban').value;

				if (validacion01.trim() != '') 
				{
					document.form2.oculto.value = '3';
					document.form2.submit();
				}
				else 
				{
					swal("Error", "Código vacio, digite un código para continuar.", "error");
				}
			}

			function errorCodeRepeated(resultadoBusqueda)
			{	
				swal("Error", "Código en uso, digite otro código que no este en uso.", "error");
			}
		</script>

		<?php titlepag();?>
	</head>

	<body>
		<IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
		<span id="todastablas2"></span>
		<table>
			<tr><script>barra_imagenes("serv");</script><?php cuadro_titulos();?></tr>

			<tr><?php menu_desplegable("serv");?></tr>

			<tr>
				<td colspan="3" class="cinta">
					<a href="serv-rutas.php" class="mgbt"><img src="imagenes/add.png"/></a>

					<a onclick="guardar()" class="mgbt"><img src="imagenes/guarda.png" title="Guardar"/></a>

					<a href="serv-rutasbuscar.php" class="mgbt"><img src="imagenes/busca.png" title="Buscar"/></a>

					<a onclick="mypop=window.open('plan-agenda.php','','');mypop.focus()" class="mgbt"><img src="imagenes/agenda1.png" title="Agenda" /></a>

					<a onclick="mypop=window.open('serv-principal.php','','');mypop.focus();" class="mgbt"><img src="imagenes/nv.png" title="Nueva ventana"></a>

					<a onclick="mypop=window.open('<?php echo $url2; ?>','','');mypop.focus();" class="mgbt"><img src="imagenes/duplicar_pantalla.png" title="Duplicar pesta&ntilde;a"></a>
				</td>
			</tr>
		</table>

		<div id="bgventanamodalm" class="bgventanamodalm">
			<div id="ventanamodalm" class="ventanamodalm">
				<IFRAME src="" name="ventanam" marginWidth=0 marginHeight=0 frameBorder=0 id="ventanam" frameSpacing=0 style=" width:700px; height:130px; top:200; overflow:hidden;"></IFRAME>
			</div>
		</div>

		<form name="form2" method="post" action="">
			<?php 
				if(@$_POST['oculto'] == "")
				{
					$_POST['onoffswitch'] = "S";
				}

				if (@$_POST['oculto'] == '3') 
				{
					$codigo = $_POST['codban'];
					$resultado = validaExistenciaId('srvrutas', $codigo);

					if ($resultado == 0)
					{
						echo "<script>errorCodeRepeated();</script>";
						$_POST['codban'] = '';
					}			
				}
			?>
			<table class="inicio ancho">
				<tr>
					<td class="titulos" colspan="5">.: Ingresar Ruta</td>

					<td class="cerrar" style="width:7%" onClick="location.href='serv-principal.php'">Cerrar</td>
				</tr>

				<tr>
					<td class="tamano01" style="width:3cm;">C&oacute;digo:</td>

					<td style="width:15%;">
						<input type="text" name="codban" id="codban" onKeyUp="return tabular(event,this)" value="<?php echo @$_POST['codban'];?>" style="width:100%;height:30px;text-align:center;" onblur="validateCode();"/>
					</td>

					<td class="tamano01" style="width:3cm;">Nombre:</td>

					<td colspan="2">
						<input type="text" name="nomban" id="nomban" value="<?php echo @$_POST['nomban'];?>" style="width:100%;height:30px;text-transform:uppercase"/>
					</td>
				</tr>

				<tr>
					<td class="tamano01">Estado:</td>

					<td>
						<div class="onoffswitch">
							<input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox" id="myonoffswitch" value="<?php echo @$_POST['onoffswitch'];?>" <?php if(@$_POST['onoffswitch']=='S'){echo "checked";}?> onChange="cambiocheck();"/>

							<label class="onoffswitch-label" for="myonoffswitch">
								<span class="onoffswitch-inner"></span>
								<span class="onoffswitch-switch"></span>
							</label>
						</div>
					</td>
					
					<td class="tamano01">Barrio:</td>
					<td style="width:15%;">
						<select name="idbarrio" id="idbarrio" style="width:100%">
							<option value="-1">:: Seleccione Barrio ::</option>
							<?php
								$sqlr = "SELECT T1.id,T1.nombre FROM srvbarrios AS T1 LEFT JOIN srvrutas_barrios AS T2 ON T1.id=T2.id_barrio AND T2.estado ='S' WHERE T2.id_barrio IS null AND T1.estado = 'S' ORDER BY T1.nombre;";
								$resp = mysqli_query($linkbd,$sqlr);
								while ($row = mysqli_fetch_row($resp))
								{
									if($_POST['idbarrio'] == $row[0])
									{
										echo "<option value = '$row[0]' SELECTED>$row[1]</option>";
										$_POST['nombarrio'] = $row[1];
									}
									else 
									{
										echo "<option value='$row[0]'>$row[1]</option>no";
									}
								}
							?>
						</select>

						<input type="hidden" name="nombarrio" id="nombarrio" value="<?php echo @ $_POST['nombarrio'];?>"/>
					</td>

					<td style="padding-bottom:0px">
						<em class="botonflecha" onClick="fagregar();">Agregar</em>
					</td>
				</tr>
			</table>

			<div class="subpantalla" style="height:60.5%; width:99.6%; overflow-x:hidden;">

				<table class="inicio" width="99%">
					<tr>
						<td class="titulos" colspan="9">Detalles Ruta</td>
					</tr>

					<tr class="titulos2">
						<td style="width:5%">Item</td>
						<td style="width:6%">id</td>
						<td>Barrio</td>
						<td style="width:8%">Eliminar</td>
					</tr>

					<?php
						if (@$_POST['oculto'] == '3')
						{
							$posi = $_POST['elimina'];
							unset($_POST['idbar'][$posi]);
							unset($_POST['nbarrio'][$posi]);
							$_POST['idbar'] = array_values($_POST['idbar']);
							$_POST['nbarrio'] = array_values($_POST['nbarrio']);
							$_POST['elimina'] = '';
						}

						if (@$_POST['oculto'] == '4')//agregar un barrio
						{
							$_POST['idbar'][] = $_POST['idbarrio'];
							$_POST['nbarrio'][] = $_POST['nombarrio'];
							echo"
							<script>
								document.getElementById('idbarrio').value='-1';
								document.getElementById('nombarrio').value='';
							</script>";
						}
					?>

					<input type='hidden' name='elimina' id='elimina'/>

					<?php
						$co="saludo1a";
						$co2="saludo2";

						for($x = 0 ; $x < count($_POST['idbar']) ; $x++)
						{
							echo"
							<input type='hidden' name='idbar[]' value='".$_POST['idbar'][$x]."'/>
							<input type='hidden' name='nbarrio[]' value='".$_POST['nbarrio'][$x]."'/>

							<tr class='$co'>
								<td style='text-align:right;'>".($x+1)."&nbsp;</td>
								<td style='text-align:right;'>".$_POST['idbar'][$x]."&nbsp;</td>
								<td>&nbsp;".$_POST['nbarrio'][$x]."</td>
								<td style='text-align:center;'><img src='imagenes/del.png' onclick='eliminar($x)' class='icoop'></td>
							</tr>";

							$aux = $co;
							$co  = $co2;
							$co2 = $aux;
						}
					?>
				</table>
			</div>

			<input type="hidden" name="oculto" id="oculto" value="1"/>

			<?php 
				if(@$_POST['oculto'] == "2")
				{
					if (@$_POST['onoffswitch'] != 'S')
					{
						$valest = 'N';
					}
					else 
					{
						$valest='S';
					}	

					$sqlr = "INSERT INTO srvrutas (id,nombre,estado) VALUES ('".$_POST['codban']."','".$_POST['nomban']."', '$valest')";

					if (!mysqli_query($linkbd,$sqlr))
					{
						echo"<script>despliegamodalm('visible','2','No se pudo ejecutar la peticion');</script>";
					}
					else 
					{
						for ($x = 0 ; $x < count($_POST['idbar']) ; $x++)
						{
							$idunion = selconsecutivo('srvrutas_barrios','id');

							$sqlt = "INSERT INTO srvrutas_barrios (id,id_ruta,id_barrio,estado) VALUES ('$idunion', '".$_POST['codban']."','".$_POST['idbar'][$x]."','S')";

							mysqli_query($linkbd,$sqlt);
							
						}
						echo "	
						<script>
							despliegamodalm('visible','1','Se ha almacenado con Exito');
					 	</script>";
					}
				}
			?>
		</form>

		<div id="bgventanamodal2">
			<div id="ventanamodal2">
				<IFRAME  src="" name="buster" marginWidth=0 marginHeight=0 frameBorder=0 id="ventana2" frameSpacing=0 style="left:500px; width:900px; height:500px; top:200;"></IFRAME>
			</div>
		</div>
		
	</body>
</html>