<?php
	require"comun.inc";
	require"funciones.inc";
	session_start();
	$linkbd=conectar_bd();	
	cargarcodigopag($_GET[codpag],$_SESSION["nivel"]);
	header("Cache-control: private"); // Arregla IE 6
	header("Content-Type: text/html;charset=iso-8859-1");
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es">
	<head>
	 	<meta http-equiv="Content-type" content="text/html; charset=iso-8859-1"/>
        <meta http-equiv="X-UA-Compatible" content="IE=9"/>
		<title>:: Spid - Servicios P&uacute;blicos</title>
       	<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
        <link href="css/css2.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="css/programas.js"></script>
        <script type="text/javascript" src="css/calendario.js"></script>
		<script>
			function pdf()
			{
				document.form2.action="teso-pdfconsignaciones.php";
				document.form2.target="_BLANK";
				document.form2.submit(); 
				document.form2.action="";
				document.form2.target="";
			}
			function fexportar ()
			{
				document.form2.action="serv-reportegenusuariosexcel.php";
				document.form2.target="_BLANK";
				document.form2.submit(); 
				document.form2.action="";
				document.form2.target="";
			}
			function funordenar(var01)
			{
				if(document.getElementById(''+var01).value==0){document.getElementById(''+var01).value=1;}
				else if(document.getElementById(''+var01).value==1) {document.getElementById(''+var01).value=2;}
				else{document.getElementById(''+var01).value=0;}
				document.form2.submit();
			}
		</script>
		<?php titlepag();?>
	</head>
	<body>
		<IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
		<span id="todastablas2"></span>
		<table>
			<tr><script>barra_imagenes("serv");</script><?php cuadro_titulos();?></tr>	 
			<tr><?php menu_desplegable("serv");?></tr>
			<tr>
            	<td colspan="3" class="cinta"><img src="imagenes/add.png" title="Nuevo" onClick="location.href=' serv-reportegeneralusuarios.php'" class="mgbt"/><img src="imagenes/guardad.png" class="mgbt1"/><img src="imagenes/busca.png" class="mgbt" onClick="document.form2.submit();"/><img src="imagenes/nv.png" title="Nueva Ventana" onClick="mypop=window.open('serv-principal.php','','');mypop.focus();" class="mgbt"/><img src="imagenes/print.png" title="Imprimir" onClick="fimprimir()" class="mgbt"/><img src="imagenes/excel.png" title="Exportar" onClick="fexportar()" class="mgbt"/><img src='imagenes/iratras.png' title="Men&uacute; Nomina"  onClick="location.href=' serv-menureportes.php'" class='mgbt'></td>
  			</tr>	
		</table>
 		<form name="form2" method="post" action="">
        	<?php
                if($_POST[oculto]=="")
				{
					$_POST[numres]=10;$_POST[numpos]=$_POST[nummul]=0;
					$_POST[cel01]=$_POST[cel02]=$_POST[cel03]=$_POST[cel04]=$_POST[cel05]=$_POST[cel06]=$_POST[cel07]=$_POST[cel08]=0;
				}
            ?>
			<table  class="inicio" align="center">
      			<tr>
        			<td class="titulos" colspan="9">Reporte General de Usuarios</td>
                    <td class="cerrar" style='width:7%'><a onClick="location.href='cont-principal.php'">&nbsp;Cerrar</a></td>
      			</tr>
      			<tr >
        			<td class="saludo1"  style='width:3cm;'>C&oacute;digo:</td>
        			<td style="width:10%;"><input type="search" name="numero" value="<?php echo $_POST[numero];?>" style="width:100%;"/></td>
         			<td class="saludo1" style='width:3cm;'>Cod. Catastral: </td>
    				<td style="width:10%;"><input type="search" name="nombre" value="<?php echo $_POST[nombre];?>" style='width:100%;'/></td>
                    <td class="saludo1" style='width:3cm;'>A&ntilde;o: </td>
                    <td>      
      					<select name="annos" >
       						<option value="">Seleccione ...</option>
            				<?php
								$sqlr="SELECT DISTINCT YEAR(fechacreacion) FROM servclientes WHERE estado='S'";
								$res=mysql_query($sqlr,$linkbd);
								while ($row =mysql_fetch_row($res)) 
				   				{
					 				if($row[0]==$_POST[annos]){echo "<option value='$row[0]' SELECTED >$row[0]</option>";}
					  	 			else{echo "<option value='$row[0]'>$row[0]</option>";}
								}	 	
							?>
						</select>  
					</td>
                    <td><input type="button" name="bboton" onClick="limbusquedas();" value="&nbsp;&nbsp;Buscar&nbsp;&nbsp;" /></td>
        		</tr>                       
    		</table>   
     		<input type="hidden" name="oculto" id="oculto"  value="1"/>
            <input type="hidden" name="cel01" id="cel01" value="<?php echo $_POST[cel01];?>"/>
            <input type="hidden" name="cel02" id="cel02" value="<?php echo $_POST[cel02];?>"/>
            <input type="hidden" name="cel03" id="cel03" value="<?php echo $_POST[cel03];?>"/>
            <input type="hidden" name="cel04" id="cel04" value="<?php echo $_POST[cel04];?>"/>
            <input type="hidden" name="cel05" id="cel05" value="<?php echo $_POST[cel05];?>"/>
            <input type="hidden" name="cel06" id="cel06" value="<?php echo $_POST[cel06];?>"/>
            <input type="hidden" name="cel07" id="cel07" value="<?php echo $_POST[cel07];?>"/>
            <input type="hidden" name="cel08" id="cel08" value="<?php echo $_POST[cel08];?>"/>
            <input type="hidden" name="numres" id="numres" value="<?php echo $_POST[numres];?>"/>
    		<input type="hidden" name="numpos" id="numpos" value="<?php echo $_POST[numpos];?>"/>
       		<input type="hidden" name="nummul" id="nummul" value="<?php echo $_POST[nummul];?>"/>
            <div class="subpantalla" style="height:68.5%; width:99.6%; overflow-x:hidden;" id="divdet">
      			<?php
	  				$vigusu=vigencia_usuarios($_SESSION[cedulausu]);
					//if($_POST[oculto])
					{
						$crit1=$crit2=$crit3="";
						$sumceldas=$_POST[cel01] + $_POST[cel02] + $_POST[cel03] + $_POST[cel04] + $_POST[cel05] + $_POST[cel06] + $_POST[cel07] + $_POST[cel08];
						if ($sumceldas >0){$ord00="ORDER BY";}
						else {$ord00="";}
						if($_POST[cel01]==0){$cl01='titulos3';$ord01="";$ico01="";}
						else 
						{
							$cl01='celactiva';
							if($_POST[cel01]==1){$ord01="codigo ASC";$ico01="<img src='imagenes/bullet_arrow_up.png' style='width:24px;'/>";}
							else {$ord01="codigo DESC"; $ico01="<img src='imagenes/bullet_arrow_down.png' style='width:24px;'/>";}
						}
						if($_POST[cel02]==0){$cl02='titulos3';$ord02="";$ico02="";}
						else
						{
							$cl02='celactiva';
							if ($_POST[cel01]>0)
							{
								if($_POST[cel02]==1){$ord02=",nombretercero ASC"; $ico02="<img src='imagenes/bullet_arrow_up.png'  style='width:24px;'/>";}
								else {$ord02=",nombretercero DESC"; $ico02="<img src='imagenes/bullet_arrow_down.png' style='width:24px;'/>";}
							}
							else 
							{
								if($_POST[cel02]==1){$ord02="nombretercero ASC"; $ico02="<img src='imagenes/bullet_arrow_up.png' style='width:24px;'/>";}
								else {$ord02="nombretercero DESC"; $ico02="<img src='imagenes/bullet_arrow_down.png' style='width:24px;'/>";}
							}
						}
						if($_POST[cel03]==0){$cl03='titulos3';$ord03="";$ico03="";}
						else
						{
							$cl03='celactiva';
							if ($_POST[cel01]>0 || $_POST[cel02]>0)
							{
								if($_POST[cel03]==1){$ord03=",direccion ASC"; $ico03="<img src='imagenes/bullet_arrow_up.png'  style='width:24px;'/>";}
								else {$ord03=",direccion DESC"; $ico03="<img src='imagenes/bullet_arrow_down.png' style='width:24px;'/>";}
							}
							else 
							{
								if($_POST[cel03]==1){$ord03="direccion ASC"; $ico03="<img src='imagenes/bullet_arrow_up.png' style='width:24px;'/>";}
								else {$ord03="direccion DESC"; $ico03="<img src='imagenes/bullet_arrow_down.png' style='width:24px;'/>";}
							}
						}
						if($_POST[cel04]==0){$cl04='titulos3';$ord04="";$ico04="";}
						else
						{
							$cl04='celactiva';
							if ($_POST[cel01]>0 || $_POST[cel02]>0 || $_POST[cel03]>0) 
							{
								if($_POST[cel04]==1){$ord04=",codcatastral ASC"; $ico04="<img src='imagenes/bullet_arrow_up.png'  style='width:24px;'/>";}
								else {$ord04=",codcatastral DESC"; $ico04="<img src='imagenes/bullet_arrow_down.png' style='width:24px;'/>";}
							}
							else 
							{
								if($_POST[cel04]==1){$ord04="codcatastral ASC"; $ico04="<img src='imagenes/bullet_arrow_up.png' style='width:24px;'/>";}
								else {$ord04="codcatastral DESC"; $ico04="<img src='imagenes/bullet_arrow_down.png' style='width:24px;'/>";}
							}
						}
						if($_POST[cel05]==0){$cl05='titulos3';$ord05="";$ico05="";}
						else
						{
							$cl05='celactiva';
							if ($_POST[cel01]>0 || $_POST[cel02]>0 || $_POST[cel03]>0 || $_POST[cel04]>0 ) 
							{
								if($_POST[cel05]==1){$ord05=",estrato ASC"; $ico05="<img src='imagenes/bullet_arrow_up.png'  style='width:24px;'/>";}
								else {$ord05=",estrato DESC"; $ico05="<img src='imagenes/bullet_arrow_down.png' style='width:24px;'/>";}
							}
							else 
							{
								if($_POST[cel05]==1){$ord05="estrato ASC"; $ico05="<img src='imagenes/bullet_arrow_up.png' style='width:24px;'/>";}
								else {$ord05="estrato DESC"; $ico05="<img src='imagenes/bullet_arrow_down.png' style='width:24px;'/>";}
							}
							
						}
						if($_POST[cel06]==0){$cl06='titulos3';$ord06="";$ico06="";}
						else
						{
							$cl06='celactiva';
							if ($_POST[cel01]>0 || $_POST[cel02]>0 || $_POST[cel03]>0 || $_POST[cel04]>0 || $_POST[cel05]>0 ) 
							{
								if($_POST[cel06]==1){$ord06=",zona ASC"; $ico06="<img src='imagenes/bullet_arrow_up.png'  style='width:24px;'/>";}
								else {$ord06=",zona DESC"; $ico06="<img src='imagenes/bullet_arrow_down.png' style='width:24px;'/>";}
							}
							else 
							{
								if($_POST[cel06]==1){$ord06="zona ASC"; $ico06="<img src='imagenes/bullet_arrow_up.png' style='width:24px;'/>";}
								else {$ord06="zona DESC"; $ico06="<img src='imagenes/bullet_arrow_down.png' style='width:24px;'/>";}
							}
						}
						if($_POST[cel07]==0){$cl07='titulos3';$ord07="";$ico07="";}
						else
						{
							$cl07='celactiva';
							if ($_POST[cel01]>0 || $_POST[cel02]>0 || $_POST[cel03]>0 || $_POST[cel04]>0 || $_POST[cel05]>0 || $_POST[cel06]>0) 
							{
								if($_POST[cel07]==1){$ord07=",lado ASC"; $ico07="<img src='imagenes/bullet_arrow_up.png'  style='width:24px;'/>";}
								else {$ord07=",lado DESC"; $ico07="<img src='imagenes/bullet_arrow_down.png' style='width:24px;'/>";}
							}
							else 
							{
								if($_POST[cel07]==1){$ord07="lado ASC"; $ico07="<img src='imagenes/bullet_arrow_up.png' style='width:24px;'/>";}
								else {$ord07="lado DESC"; $ico07="<img src='imagenes/bullet_arrow_down.png' style='width:24px;'/>";}
							}
						}
						if($_POST[cel08]==0){$cl08='titulos3';$ord08="";$ico08="";}
						else
						{
							$cl08='celactiva';
							if ($_POST[cel01]>0 || $_POST[cel02]>0 || $_POST[cel03]>0 || $_POST[cel04]>0 || $_POST[cel05]>0 || $_POST[cel06]>0 || $_POST[cel07]>0) 
							{
								if($_POST[cel08]==1){$ord08=",barrio ASC"; $ico08="<img src='imagenes/bullet_arrow_up.png'  style='width:24px;'/>";}
								else {$ord08=",barrio DESC"; $ico08="<img src='imagenes/bullet_arrow_down.png' style='width:24px;'/>";}
							}
							else 
							{
								if($_POST[cel08]==1){$ord08="barrio ASC"; $ico08="<img src='imagenes/bullet_arrow_up.png' style='width:24px;'/>";}
								else {$ord08="barrio DESC"; $ico08="<img src='imagenes/bullet_arrow_down.png' style='width:24px;'/>";}
							}
						}
						
						if ($_POST[numero]!=""){$crit1=" and servclientes.codigo like '%".$_POST[numero]."%' ";}
						if ($_POST[nombre]!=""){$crit2=" and servclientes.codcatastral like '%".$_POST[nombre]."%'  ";}
						if ($_POST[annos]!=""){ $crit3="AND YEAR(fechacreacion) <= $_POST[annos]";}
						$sqlr="SELECT codigo, nombretercero, direccion, codcatastral, barrio, estrato, zona, lado FROM servclientes WHERE servclientes.estado <> ' ' $crit1 $crit2 $crit3";
						$resp = mysql_query($sqlr,$linkbd);
						$_POST[numtop]=mysql_num_rows($resp);
						$nuncilumnas=ceil($_POST[numtop]/$_POST[numres]);
						if ($_POST[numres]!="-1"){$cond2="LIMIT $_POST[numpos], $_POST[numres]";}
						$sqlr="SELECT codigo, nombretercero, direccion, codcatastral, barrio, estrato, zona, lado FROM servclientes WHERE servclientes.estado <> ' ' $crit1 $crit2 $crit3 $ord00 $ord01 $ord02 $ord03 $ord04 $ord05 $ord06 $ord07 $ord08 $cond2";
						//echo $sqlr;
						$resp = mysql_query($sqlr,$linkbd);
						$con=1;
						$numcontrol=$_POST[nummul]+1;
						if(($nuncilumnas==$numcontrol)||($_POST[numres]=="-1"))
						{
							$imagenforward="<img src='imagenes/forward02.png' style='width:17px;cursor:default;'>";
							$imagensforward="<img src='imagenes/skip_forward02.png' style='width:16px;cursor:default;' >";
						}
						else 
						{
							$imagenforward="<img src='imagenes/forward01.png' style='width:17px;cursor:pointer;' title='Siguiente' onClick='numsiguiente()'>";
							$imagensforward="<img src='imagenes/skip_forward01.png' style='width:16px;cursor:pointer;' title='Fin' onClick='saltocol(\"$nuncilumnas\")'>";
						}
						if(($_POST[numpos]==0)||($_POST[numres]=="-1"))
						{
							$imagenback="<img src='imagenes/back02.png' style='width:17px;cursor:default;'>";
							$imagensback="<img src='imagenes/skip_back02.png' style='width:16px;cursor:default;'>";
						}
						else
						{
							$imagenback="<img src='imagenes/back01.png' style='width:17px;cursor:pointer;' title='Anterior' onClick='numanterior();'>";
							$imagensback="<img src='imagenes/skip_back01.png' style='width:16px;cursor:pointer;' title='Inicio' onClick='saltocol(\"1\")'>";
						}
						echo "
						<table class='inicio' align='center' >
							<tr>
								<td colspan='10' class='titulos'>.: Resultados Busqueda:</td>
								<td class='submenu'>
									<select name='renumres' id='renumres' onChange='cambionum();' style='width:100%'>
										<option value='10'"; if ($_POST[renumres]=='10'){echo 'selected';} echo ">10</option>
										<option value='20'"; if ($_POST[renumres]=='20'){echo 'selected';} echo ">20</option>
										<option value='30'"; if ($_POST[renumres]=='30'){echo 'selected';} echo ">30</option>
										<option value='50'"; if ($_POST[renumres]=='50'){echo 'selected';} echo ">50</option>
										<option value='100'"; if ($_POST[renumres]=='100'){echo 'selected';} echo ">100</option>
										<option value='-1'"; if ($_POST[renumres]=='-1'){echo 'selected';} echo ">Todos</option>
									</select>
								</td>
							</tr>
							<tr><td colspan='11'>Clientes Encontrados: $_POST[numtop]</td></tr>
							<tr>
								<td class='$cl01' onClick=\"funordenar('cel01');\">C&oacute;digo $ico01</td>
								<td class='$cl02' onClick=\"funordenar('cel02');\">Tercero $ico02</td>
								<td class='$cl03' onClick=\"funordenar('cel03');\">Direcci&oacute;n $ico03</td>
								<td class='$cl04' onClick=\"funordenar('cel04');\">codcatastral $ico04</td>
								<td class='$cl05' onClick=\"funordenar('cel05');\">Estrato $ico05</td>
								<td class='$cl06' onClick=\"funordenar('cel06');\" style='width:5%'>Zona $ico06</td>
								<td class='$cl07' onClick=\"funordenar('cel07');\" style='width:5%'>Lado $ico07</td>
								<td class='$cl08' onClick=\"funordenar('cel08');\">Barrio $ico08</td>
								<td class='titulos3'  style='width:5%'>Acueducto</td>
								<td class='titulos3'  style='width:5%'>Alcant.</td>
								<td class='titulos3'  style='width:5%'>Aseo</td>
							</tr>";	
						$iter='saludo1a';
						$iter2='saludo2';
 						while ($row =mysql_fetch_row($resp)) 
 						{	
							$barrio=$estrato="";
							$sqlr01="SELECT nombre FROM servbarrios WHERE id='$row[4]'";
							$res01=mysql_query($sqlr01,$linkbd);
							$row01 =mysql_fetch_row($res01);
							$barrio=$row01[0];
 							$sqlr01="SELECT tipo, descripcion FROM servestratos WHERE id='$row[5]'";
							$res01=mysql_query($sqlr01,$linkbd);
							$row01 =mysql_fetch_row($res01);
							$estrato="$row01[0] - $row01[1]";
							$ser01=$ser02=$ser03="NO";
							$sqlr01="SELECT servicio FROM  terceros_servicios WHERE consecutivo='$row[0]'";
							$res01=mysql_query($sqlr01,$linkbd); 
							while ($row01 =mysql_fetch_row($res01)) 
							{
									switch ($row01[0]) 
									{
										case "01": $ser01="SI";break;
										case "02": $ser02="SI";break;
										case "03": $ser03="SI";
									}
							}
  							echo "
							<tr class='$iter'>
								<td>$row[0]</td>
								<td>$row[1]</td>
								<td>$row[2]</td>
								<td>$row[3]</td>
								<td>$estrato</td>
								<td>$row[6]</td>
								<td>$row[7]</td>
								<td>$barrio</td>
								<td>$ser01</td>
								<td>$ser02</td>
								<td>$ser03</td>
							</tr>";
	 						$con+=1;
	 						$aux=$iter;
	 						$iter=$iter2;
	 						$iter2=$aux;
 						}
 					if ($_POST[numtop]==0)
					{
						echo "
						<table class='inicio'>
							<tr>
								<td class='saludo1' style='text-align:center;width:100%'><img src='imagenes\alert.png' style='width:25px'>No hay coincidencias en la b&uacute;squeda $tibusqueda<img src='imagenes\alert.png' style='width:25px'></td>
							</tr>
						</table>";
					}
 					echo"
						</table>
						<table class='inicio'>
							<tr>
								<td style='text-align:center;'>
									<a>$imagensback</a>&nbsp;
									<a>$imagenback</a>&nbsp;&nbsp;";
					if($nuncilumnas<=9){$numfin=$nuncilumnas;}
					else{$numfin=9;}
					for($xx = 1; $xx <= $numfin; $xx++)
					{
						if($numcontrol<=9){$numx=$xx;}
						else{$numx=$xx+($numcontrol-9);}
						if($numcontrol==$numx){echo"<a onClick='saltocol(\"$numx\")'; style='color:#24D915;cursor:pointer;'> $numx </a>";}
						else {echo"<a onClick='saltocol(\"$numx\")'; style='color:#000000;cursor:pointer;'> $numx </a>";}
					}
					echo"			&nbsp;&nbsp;<a>$imagenforward</a>
									&nbsp;<a>$imagensforward</a>
								</td>
							</tr>
						</table>";
					}
				?>
        	</div>
            <input type="hidden" name="numtop" id="numtop" value="<?php echo $_POST[numtop];?>" />
		</form>
	</body>
</html>