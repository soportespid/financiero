<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=iso-8859-1");
	require"comun.inc";
	require"funciones.inc";
	session_start();
	cargarcodigopag($_GET[codpag],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es">
	<head>
		<meta http-equiv="Content-type" content="text/html; charset=iso-8859-1"/>
		<meta http-equiv="X-UA-Compatible" content="IE=9"/>
		<title>:: Ideal - Presupuesto</title>
		<link href="css/css2.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<script type="text/javascript" src="css/programas.js"></script>
		<?php titlepag();?>
	</head>
	<body>
		<IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
		<span id="todastablas2"></span>
		<table>
			<tr><script>barra_imagenes("ccpet");</script><?php cuadro_titulos();?></tr>
			<tr><?php menu_desplegable("ccpet");?></tr>
			<tr>
				<td colspan="3" class="cinta"><img src="imagenes/add2.png" class="mgbt1"/><img src="imagenes/guardad.png" style="width:24px;" class="mgbt1"/><img src="imagenes/buscad.png" class="mgbt1"/><img src="imagenes/nv.png" title="Nueva Ventana" onClick="mypop=window.open('ccp-principal.php','','');mypop.focus();" class="mgbt"></td>
			</tr>
		</table>
		<form name="form2" method="post" action="">
			<table class="inicio">
				<tr>
					<td class="titulos" colspan="2">.: Reportes Gastos de Inversi&oacute;n</td>
					<td class="cerrar" style="width:7%"><a onClick="location.href='ccp-principal.php'">Cerrar</a></td>
				</tr>
				<tr>
					<td  width='45%'>
						<ol id="lista2">
							<li class='icoom' onClick="location.href='ccp-reportegastosinversionsector.php'">Por Sector</li>
							<li class='icoom' onClick="location.href='ccp-reportegastosinversionprogramatico.php'">Reporte inversion por codigo indicador y meta PDM</li>
						</ol>
					</td>
					<td></td>
				</tr>
			</table>
		</form>
	</body>
</html>