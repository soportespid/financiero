<?php

    header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require 'comun.inc';
	require 'funciones.inc';
	session_start();
    if(empty($_SESSION)){
        header("location: index.php");
    }
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html lang=es>
	<head>
    <meta charset="utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1"/>
		<meta name="viewport" content="user-scalable=no">
		<title>:: IDEAL 10 - Tesoreria</title>
		<link href="favicon.ico" rel="shortcut icon"/>
		<link href="css/css2z.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
        <link href="css/tabs.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
        <link href="css/style.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" href="sweetalert2/dist/sweetalert2.min.css">
        <script type="text/javascript" src="css/programas.js"></script>
        <script src="sweetalert2/dist/sweetalert2.min.js"></script>
		<script src="vue/vue.min.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script src="css/calendario.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
        <style>
            .marcaRecibo{
                background-color: #ffc107 !important;
                color:#000;
                font-weight: bold;
            }
        </style>
	</head>
	<body>
		<header>
			<table>
				<tr><script>barra_imagenes("teso");</script><?php cuadro_titulos();?></tr>
			</table>
		</header>

		<form name="form2" method="post" action="">
			<section id="myapp" v-cloak >
                <input type="hidden" value = "3" ref="pageType">
                <div class="loading-container" v-show="isLoading" >
                    <p class="text-loading"data-loading="IDEAL 10">IDEAL 10</p>
                </div>
				<nav>
					<table>
						<tr><?php menu_desplegable("teso");?></tr>
					</table>
                    <div class="bg-white group-btn p-1" id="newNavStyle">
                        <button type="button" @click="window.location.href='teso-ingresoInternoCrear'" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
                            <span>Nuevo</span>
                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960"><path d="M440-440H200v-80h240v-240h80v240h240v80H520v240h-80v-240Z"></path></svg>
                        </button>
                        <button type="button" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center" @click="mypop=window.open('teso-principal.php','',''); mypop.focus();">
                            <span>Nueva ventana</span>
                            <svg class="fill-black group-hover:fill-white w-5 h-5" xmlns="http://www.w3.org/2000/svg"  viewBox="0 -960 960 960" ><path d="M320-120v-80h80v-80H160q-33 0-56.5-23.5T80-360v-400q0-33 23.5-56.5T160-840h640q33 0 56.5 23.5T880-760v400q0 33-23.5 56.5T800-280H560v80h80v80H320ZM160-360h640v-400H160v400Zm0 0v-400 400Z"/></svg>
                        </button>
                        <button type="button" @click="mypop=window.open('teso-ingresoInternoBuscar','','');mypop.focus();" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
                            <span class="group-hover:text-white">Duplicar pantalla</span>
                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960"><path d="M640-160v-360H160v360h480Zm80-200v-80h80v-360H320v200h-80v-200q0-33 23.5-56.5T320-880h480q33 0 56.5 23.5T880-800v360q0 33-23.5 56.5T800-360h-80ZM160-80q-33 0-56.5-23.5T80-160v-360q0-33 23.5-56.5T160-600h480q33 0 56.5 23.5T720-520v360q0 33-23.5 56.5T640-80H160Zm400-603ZM400-340Z"></path></svg>
                        </button>
                    </div>
				</nav>
				<article>
                    <div class="bg-white">
                        <div>
                            <h2 class="titulos m-0">Buscar Comprobantes de ingresos</h2>
                            <div class="d-flex w-75">
                                <div class="form-control">
                                    <label class="form-label">Buscar:</label>
                                    <input 
                                        type="search" 
                                        placeholder="Buscar por ingreso, concepto o cedula" 
                                        @keyup="search('internos')"
                                        v-model="txtSearch">
                                </div>
                                <div class="form-control w-25">
                                    <label class="form-label " for="">Fecha inicial:</label>
                                    <input type="date" v-model="txtFechaInicial" >
                                </div>
                                <div class="form-control w-25">
                                    <label class="form-label" for="">Fecha final:</label>
                                    <input type="date" v-model="txtFechaFinal">
                                </div>
                                <div class="form-control justify-between w-25">
                                    <label class="form-label" for=""></label>
                                    <button type="button" class="btn btn-primary" @click="getSearch()">Buscar</button>
                                </div>
                            </div>
                            <h2 class="titulos m-0">Resultados: {{ txtResultados }} </h2>
                        </div>
                        <div class="table-responsive" style="height:55vh">
                            <table class="table table-hover fw-normal">
                                <thead>
                                    <tr class="text-center">
                                        <th>Código</th>
                                        <th>Liquidacion</th>
                                        <th>Concepto</th>
                                        <th>Fecha</th>
                                        <th>CC/NIT</th>
                                        <th>Nombre</th>
                                        <th>Total</th>
                                        <th>Estado</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr v-for="(data,index) in arrSearchData" :key="index" @dblclick="editItem(data.id_recibos)">
                                        <td class="text-center">{{data.id_recibos}}</td>
                                        <td class="text-center">{{data.id_recaudo}}</td>
                                        <td >{{data.concepto}}</td>
                                        <td class="text-center">{{data.fecha}}</td>
                                        <td>{{data.tercero}}</td>
                                        <td>{{data.nombre}}</td>
                                        <td class="text-right">{{data.total_format}}</td>
                                        <td class="text-center">
                                            <span class="badge" :class="[data.estado == 'S' ? 'badge-success' : data.estado == 'P' ? 'badge-primary' : 'badge-danger']">{{data.estado == 'S' ? 'Activo' : 'Reversado'}}</span>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
				</article>
			</section>
		</form>

		<script src="node_modules/read-excel-file/bundle/read-excel-file.min.js"></script>
		<script src="node_modules/xlsx/dist/xlsx.full.min.js"></script>
        <script src="Librerias/vue/vue.min.js"></script>
		<script src="Librerias/vue/axios.min.js"></script>
		<script type="module" src="tesoreria/ingresos/js/functions_ingreso_interno.js?<?= date('d_m_Y_h_i_s');?>"></script>
	</body>
</html>
