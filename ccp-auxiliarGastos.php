<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require 'comun.inc';
	require 'funciones.inc';
	session_start();
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
	titlepag();
?>

<!DOCTYPE >
<html lang=es>
	<head>
		<meta charset="utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1"/>
		<meta name="viewport" content="user-scalable=no">
		<title>:: IDEAL 10 - Presupuesto</title>
		<link href="css/css2z.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<script src="vue/vue.min.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script src="css/calendario.js?<?php echo date('d_m_Y_h_i_s');?>"></script>

        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">

        <!-- <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
        <script src="./Librerias/bootstrap-5.2.0-beta1-dist/js/bootstrap.bundle.min.js"></script>
        <link rel="stylesheet" href="./Librerias/bootstrap-5.2.0-beta1-dist/css/bootstrap.min.css">
        <script type="text/javascript" src="bootstrap/fontawesome.5.11.2/js/all.js"></script>  -->

        <script>
            function cambiaFecha(){
                var fecha = document.getElementById('fc_1198971545').value;
                fecArray = fecha.split('/');
                document.getElementById('vigencia').value = fecArray[2];
            }
        </script>

        <style>

            .titulo-form{
                background-color: #559CFC;
                height: 30px;
                margin: 1px;
                padding: 2px 4px;
                color: white;
            }

            .fondo-form{
                background-color: #F6F6F6;
                margin-bottom: 0px;
                /* margin-top: 10px; */
                /* padding-top: 4px; */
            }

            input[type=number]::-webkit-inner-spin-button,
			input[type=number]::-webkit-outer-spin-button { 
				-webkit-appearance: none; 
				margin: 0; 
			}
			input[type=number]{
				text-align:right;
				box-sizing: border-box;
				font: 120% sans-serif;
				width: 100% !important;
			}

            .inicio--no-shadow{
				box-shadow: none;
			}

            .modal-mask {
			position: fixed;
			z-index: 9998;
			top: 0;
			left: 0;
			width: 100%;
			height: 100%;
			background-color: rgba(0, 0, 0, .5);
			display: table;
			transition: opacity .3s ease;
			}

			.modal-wrapper {
			display: table-cell;
			vertical-align: middle;
			}
			.modal-body{
				max-height: 500px;
				overflow-y: scroll;
			}

            [v-cloak]{
                display : none;
            }
        </style>
        <script src="sweetalert2/dist/sweetalert2.min.js"></script>
		<link rel="stylesheet" href="sweetalert2/dist/sweetalert2.min.css">
        
    </head>
    <body>
        <header>
			<table>
				<tr><script>barra_imagenes("ccpet");</script><?php cuadro_titulos();?></tr>
			</table>
		</header>
        <section id="myapp" v-cloak >
            <nav>
				<table>
					<tr><?php menu_desplegable("ccpet");?></tr>
					<tr>
						<td colspan="3" class="cinta">
							<img src="imagenes/add.png" onClick="location.href='ccp-auxiliarGastos.php'" class="mgbt" title="Nuevo">
							<img src="imagenes/guardad.png" title="Guardar"  class="mgbt1">
							<img src="imagenes/buscad.png" class="mgbt" title="Buscar">
							<img src="imagenes/nv.png" onClick="mypop=window.open('ccp-principal.php','',''); mypop.focus();" class="mgbt" title="Nueva Ventana">
						</td>
					</tr>
				</table>
			</nav>
        
            <article>

                <table class="inicio ancho">
                    <tr>
                        <td class="titulos" colspan="12" >Auxilar por Cuenta de Gastos</td>
                        <td class="cerrar" onClick="location.href='ccp-principal.php'">Cerrar</td>
                    </tr>
                    <tr>
                        <td class="tamano01">Fecha Ini</td>
                        <td>
                            <input
                                autocomplete="off"
                                class="colordobleclik"
                                id="fc_1198971545"
                                name="fecha"
                                onchange="cambiaFecha();"
                                onDblClick="displayCalendarFor('fc_1198971545');"
                                onKeyUp="return tabular(event,this)"
                                readonly
                                title="DD/MM/YYYY"
                                type="text"
                                value="<?php echo $_POST['fecha']?>">
                            <input
                                id="vigencia"
                                name="vigencia"
                                type="hidden"
                                value="<?php echo $_POST['vigencia']?>">
                        </td>
                        <td class="tamano01">Fecha Fin</td>
                        <td>
                            <input 
                                autocomplete="off"
                                class="colordobleclik"
                                id="fc_1198971546"
                                name="fecha2"
                                onChange=""
                                onDblClick="displayCalendarFor('fc_1198971546');"
                                onKeyUp="return tabular(event,this)"
                                readonly
                                type="text"
                                title="DD/MM/YYYY"
                                value="<?php echo $_POST['fecha2']?>">
                        </td>

                        <td class="tamano01">Tipo gasto</td>
                        <td>
                            <select 
                                class="form-control select"
                                style = "font-size: 10px; margin-top:4px;"
                                v-model="tipoGasto"
                                v-on:change="cambiaTipoDeGasto">
                                <option 
                                    v-bind:value = "tipoDeGasto[0]"
                                    v-for="tipoDeGasto in tiposDeGasto">
                                    {{ tipoDeGasto[1] }}
                                </option>
                            </select>


                            <!-- <input autocomplete="off" class="colordobleclik" id="rubro" name="rubro" type="text" v-model = "rubro"> -->

                        </td>

                        <td class="tamano01">Sec. Presu</td>
                        <td>
                            <select 
                                class="form-control select"
                                style = "font-size: 10px; margin-top:4px;"
                                v-model="selectUnidadEjecutora"
                                v-on:change="cambiaTipoDeGasto">
                                <option
                                    v-for="unidad in unidadesejecutoras"
                                    v-bind:value = "unidad[0]">
                                    {{ unidad[0] }} - {{ unidad[1] }}
                                </option>
                            </select>
                        </td>

                        <td class="tamano01">Medio pago</td>
                        <td>
                            <select 
                                class="form-control select"
                                style = "font-size: 10px; margin-top:4px; width:100%;"
                                v-model="selectMedioPago"
                                v-on:change="cambiaTipoDeGasto">
                                <option
                                    v-for="option in optionsMediosPagos"
                                    v-bind:value = "option.value">
                                    {{ option.text }}
                                </option>
                            </select>
                        </td>

                        <td class="tamano01" colspan="2">Vig. gasto</td>
                        <td>
                            <select 
                                class="form-control select"
                                style = "font-size: 10px; margin-top:4px; width:100%;"
                                v-model="selectVigenciaGasto"
                                v-on:change="cambiaTipoDeGasto">
                                <option
                                    v-for="vigenciaDeGasto in vigenciasdelgasto"
                                    v-bind:value = "vigenciaDeGasto[0]">
                                    {{ vigenciaDeGasto[1] }} - {{ vigenciaDeGasto[2] }}
                                </option>
                            </select>
                        </td>
                        
                        <!-- <td style="width: 10%"></td> -->
                    </tr>
                    <tr>
                        <td class="tamano01">Rubro:</td>
                        <td>
                            
                            <input 
                                autocomplete="off" 
                                class="colordobleclik" 
                                id="rubro" 
                                name="rubro"
                                type="text" 
                                v-model = "rubro"
                                @change = "buscarCta"
                                @dblclick = "ventanaRubro">

                        </td>
                        <td colspan="2">
                            <input 
                                autocomplete="off"
                                id="nombreRubro" 
                                name="nombreRubro" 
                                readonly
                                style="width:100%;"
                                type="text" 
                                v-model = "nombreRubro">
                        </td>

                        <td class="tamano01">Proyecto:</td>
                        <td>
                            <input 
                                autocomplete="off" 
                                class="colordobleclik"
                                readonly
                                title="Bpim del proyecto."
                                type="text"
                                v-model = "codProyecto"
                                @change = "buscarProyecto"
                                @dblclick = "ventanaProyecto">
                        </td>
                        <td colspan="2">
                            <input 
                                autocomplete="off"
                                id="nombreProyecto" 
                                name="nombreProyecto" 
                                readonly
                                style="width:100%;"
                                type="text" 
                                v-model = "nombreProyecto">
                        </td>
                        <td class="tamano01">Program&aacute;tico:</td>
                        <td >
                            <input 
                                autocomplete="off" 
                                class="colordobleclik"
                                readonly
                                type="text" 
                                v-model = "programatico"
                                @change = "buscarProgramatico"
                                @dblclick = "ventanaProgramatico">
                        </td>
                        <td colspan="3">
                            <input 
                                autocomplete="off"
                                id="nombreProgramatico" 
                                name="nombreProgramatico" 
                                readonly
                                style="width:100%;"
                                type="text" 
                                v-model = "nombreProgramatico">
                        </td>

                        

                        
                    </tr>
                    <tr>
                        
                    <td class="tamano01">CPC:</td>
                        <td>
                            
                            <input
                                autocomplete="off"
                                class="colordobleclik"
                                type="text"
                                v-model = "codigoCpc"
                                @change = "buscarCodigoCpc"
                                @dblclick = "ventanaCodigoCpc">

                        </td>
                        
                        <td colspan="2">
                            <input
                                autocomplete="off"
                                readonly
                                style="width:100%;"
                                type="text"
                                v-model = "nombreCodigoCpc">
                        </td>

                        <td class="tamano01">Fuente:</td>
                        <td>
                            
                            <input 
                                autocomplete="off" 
                                class="colordobleclik"
                                type="text" 
                                v-model = "fuente"
                                @change = "buscarFuente"
                                @dblclick = "ventanaFuente">

                        </td>
                        <td colspan="2">
                            <input 
                                autocomplete="off"
                                id="nombreFuente" 
                                name="nombreFuente" 
                                readonly
                                style="width:100%;"
                                type="text" 
                                v-model = "nombreFuente">
                        </td>

                        <td colspan="2" style="padding-bottom:0px;">
                            <em class="botonflechaverde" @click="buscarDocumentos">Buscar</em>
                        </td>
                    </tr>
                    
                </table>

                <div class='subpantalla' style='height:58vh; width:99.2%; margin-top:0px; overflow:hidden'>
                    <table class='tablamv'>
                        <thead>
                            <tr style="text-align:Center;">
                                <th class="titulosnew00" style="width:3%;">Sec. Presupuestal</th>
                                <th class="titulosnew00" style="width:3%;">Medio de pago</th>
                                <th class="titulosnew00" style="width:4%;">Vig. De gasto</th>
                                <th class="titulosnew00" style="width:8%;">Proyecto</th>
                                <th class="titulosnew00" style="width:6%;">Program&aacute;tico</th>
                                <th class="titulosnew00" style="width:10%;">Cuenta</th>
                                <th class="titulosnew00" style="width:6%;">Fuente</th>
                                <th class="titulosnew00" style="width:6%;">CPC</th>
                                <th class="titulosnew00" style="width:4%;">No Comprobante</th>
                                <th class="titulosnew00" style="width:6%;">Fecha</th>
                                <th class="titulosnew00">Concepto</th>
                                <th class="titulosnew00" style="width:4%;">Tipo mov</th>
                                <th class="titulosnew00" style="width:8%;">Entrada</th>
                                <th class="titulosnew00" style="width:8%;">Salida</th>
                                <th class="titulosnew00" style="width:8%;">Saldo</th>
                            </tr>
                        </thead>
                        <tbody style="height: 50vh;">
                            
                            <tr v-show="!existeInformacion">
                                <td colspan="7">
                                    <div style="text-align: center; color:turquoise; font-size:large" class="h4 text-primary text-center">
                                        Utilice los filtros para buscar informaci&oacute;n.
                                    </div>
                                </td>
                            </tr>

                            <!- Presupuesto inicial funcionamiento o deuda !--> 
                            <tr v-for="(detalle, index) in detallesIni" v-bind:class="index % 2 ? 'contenidonew00' : 'contenidonew01'" style='text-rendering: optimizeLegibility; cursor: pointer !important;'>
                                <td  style="width:3%; font: 100% sans-serif; padding-left:2px; text-align:center;">{{ detalle[0] }}</td>
                                <td  style="width:3%; font: 100% sans-serif; padding-left:2px; text-align:center;">{{ detalle[1] }}</td>
                                <td  style="width:4%; font: 100% sans-serif; padding-left:2px; text-align:center;">{{ detalle[2] }}</td>
                                <td  style="width:8%; font: 100% sans-serif; padding-left:2px; text-align:center;">{{ detalle[3] }}</td>
                                <td  style="width:6%; font: 100% sans-serif; padding-left:2px; text-align:center;">{{ detalle[4] }}</td>
                                <td  style="width:10%; font: 100% sans-serif; padding-left:2px; text-align:center;">{{ detalle[5] }}</td>
                                <td  style="width:6%; font: 100% sans-serif; padding-left:2px; text-align:center;">{{ detalle[6] }}</td>
                                <td  style="width:6%; font: 100% sans-serif; padding-left:2px; text-align:center;">{{ detalle[7] }}</td>
                                <td  style="width:4%; font: 100% sans-serif; padding-left:2px; text-align:center;">{{ detalle[8] }}</td>
                                <td  style="width:6%; font: 100% sans-serif; padding-left:2px; text-align:center;">{{ detalle[9] }}</td>
                                <td  style="font: 100% sans-serif; padding-left:2px; text-align:center;">{{ detalle[10] }}</td>
                                <td  style="width:4%; font: 100% sans-serif; padding-left:2px; text-align:center;">{{ detalle[11] }}</td>
                                <td  style="width:8%; font: 100% sans-serif; padding-left:2px; text-align:right;">{{ formatonumero(detalle[12]) }}</td>
                                <td  style="width:8%; font: 100% sans-serif; padding-left:2px; text-align:right;">{{ formatonumero(detalle[13]) }}</td>
                                <td  style="width:8%; font: 100% sans-serif; padding-left:2px; text-align:right;">{{ formatonumero(detalle[14]) }}</td>
                            </tr>
                            <tr class="titulosnew04" style="margin-bottom: 2px;" v-show="existeInformacion">
                                <td colspan="12" style="font: 100% sans-serif; padding-left:2px; text-align:center; font-weight: bold;">Total del presupuesto inicial funcionamiento o deuda</td>
                                <td style="width:8%; font: 100% sans-serif; padding-left:2px; text-align:right;">{{ formatonumero(sumaEntradaIni) }}</td>
                                <td style="width:8%; font: 100% sans-serif; padding-left:2px; text-align:right;">{{ formatonumero(sumaSalidaIni) }}</td>
                                <td style="width:8%; font: 100% sans-serif; padding-left:2px; text-align:right;">{{ formatonumero(sumaSaldoIni) }}</td>
                            </tr>

                            <!- Presupuesto inicial inversion !--> 
                            <tr v-for="(detalle, index) in detallesIniInv" v-bind:class="index % 2 ? 'contenidonew00' : 'contenidonew01'" style='text-rendering: optimizeLegibility; cursor: pointer !important;'>
                                <td  style="width:3%; font: 100% sans-serif; padding-left:2px; text-align:center;">{{ detalle[0] }}</td>
                                <td  style="width:3%; font: 100% sans-serif; padding-left:2px; text-align:center;">{{ detalle[1] }}</td>
                                <td  style="width:4%; font: 100% sans-serif; padding-left:2px; text-align:center;">{{ detalle[2] }}</td>
                                <td  style="width:8%; font: 100% sans-serif; padding-left:2px; text-align:center;">{{ detalle[3] }}</td>
                                <td  style="width:6%; font: 100% sans-serif; padding-left:2px; text-align:center;">{{ detalle[4] }}</td>
                                <td  style="width:10%; font: 100% sans-serif; padding-left:2px; text-align:center;">{{ detalle[5] }}</td>
                                <td  style="width:6%; font: 100% sans-serif; padding-left:2px; text-align:center;">{{ detalle[6] }}</td>
                                <td  style="width:6%; font: 100% sans-serif; padding-left:2px; text-align:center;">{{ detalle[7] }}</td>
                                <td  style="width:4%; font: 100% sans-serif; padding-left:2px; text-align:center;">{{ detalle[8] }}</td>
                                <td  style="width:6%; font: 100% sans-serif; padding-left:2px; text-align:center;">{{ detalle[9] }}</td>
                                <td  style="font: 100% sans-serif; padding-left:2px; text-align:center;">{{ detalle[10] }}</td>
                                <td  style="width:4%; font: 100% sans-serif; padding-left:2px; text-align:center;">{{ detalle[11] }}</td>
                                <td  style="width:8%; font: 100% sans-serif; padding-left:2px; text-align:right;">{{ formatonumero(detalle[12]) }}</td>
                                <td  style="width:8%; font: 100% sans-serif; padding-left:2px; text-align:right;">{{ formatonumero(detalle[13]) }}</td>
                                <td  style="width:8%; font: 100% sans-serif; padding-left:2px; text-align:right;">{{ formatonumero(detalle[14]) }}</td>
                            </tr>
                            <tr class="titulosnew04" style="margin-bottom: 2px;" v-show="existeInformacion">
                                <td colspan="12" style="font: 100% sans-serif; padding-left:2px; text-align:center; font-weight: bold;">Total del presupuesto inicial de inversi&oacute;n</td>
                                <td style="width:8%; font: 100% sans-serif; padding-left:2px; text-align:right;">{{ formatonumero(sumaEntradaIniInv) }}</td>
                                <td style="width:8%; font: 100% sans-serif; padding-left:2px; text-align:right;">{{ formatonumero(sumaSalidaIniInv) }}</td>
                                <td style="width:8%; font: 100% sans-serif; padding-left:2px; text-align:right;">{{ formatonumero(sumaSaldoIniInv) }}</td>
                            </tr>

                            <!- Adicion !--> 
                            <tr v-for="(detalle, index) in detallesAd" v-bind:class="index % 2 ? 'contenidonew00' : 'contenidonew01'" style='text-rendering: optimizeLegibility; cursor: pointer !important;'>
                                <td  style="width:3%; font: 100% sans-serif; padding-left:2px; text-align:center;">{{ detalle[0] }}</td>
                                <td  style="width:3%; font: 100% sans-serif; padding-left:2px; text-align:center;">{{ detalle[1] }}</td>
                                <td  style="width:4%; font: 100% sans-serif; padding-left:2px; text-align:center;">{{ detalle[2] }}</td>
                                <td  style="width:8%; font: 100% sans-serif; padding-left:2px; text-align:center;">{{ detalle[3] }}</td>
                                <td  style="width:6%; font: 100% sans-serif; padding-left:2px; text-align:center;">{{ detalle[4] }}</td>
                                <td  style="width:10%; font: 100% sans-serif; padding-left:2px; text-align:center;">{{ detalle[5] }}</td>
                                <td  style="width:6%; font: 100% sans-serif; padding-left:2px; text-align:center;">{{ detalle[6] }}</td>
                                <td  style="width:6%; font: 100% sans-serif; padding-left:2px; text-align:center;">{{ detalle[7] }}</td>
                                <td  style="width:4%; font: 100% sans-serif; padding-left:2px; text-align:center;">{{ detalle[8] }}</td>
                                <td  style="width:6%; font: 100% sans-serif; padding-left:2px; text-align:center;">{{ detalle[9] }}</td>
                                <td  style="font: 100% sans-serif; padding-left:2px; text-align:center;">{{ detalle[10] }}</td>
                                <td  style="width:4%; font: 100% sans-serif; padding-left:2px; text-align:center;">{{ detalle[11] }}</td>
                                <td  style="width:8%; font: 100% sans-serif; padding-left:2px; text-align:right;">{{ formatonumero(detalle[12]) }}</td>
                                <td  style="width:8%; font: 100% sans-serif; padding-left:2px; text-align:right;">{{ formatonumero(detalle[13]) }}</td>
                                <td  style="width:8%; font: 100% sans-serif; padding-left:2px; text-align:right;">{{ formatonumero(detalle[14]) }}</td>
                            </tr>
                            <tr class="titulosnew04" style="margin-bottom: 2px;" v-show="existeInformacion">
                                <td colspan="12" style="font: 100% sans-serif; padding-left:2px; text-align:center; font-weight: bold;">Total del presupuesto de adici&oacute;n</td>
                                <td style="width:8%; font: 100% sans-serif; padding-left:2px; text-align:right;">{{ formatonumero(sumaEntradaAd) }}</td>
                                <td style="width:8%; font: 100% sans-serif; padding-left:2px; text-align:right;">{{ formatonumero(sumaSalidaAd) }}</td>
                                <td style="width:8%; font: 100% sans-serif; padding-left:2px; text-align:right;">{{ formatonumero(sumaSaldoAd) }}</td>
                            </tr>

                            <!- Reduccion !--> 
                            <tr v-for="(detalle, index) in detallesRed" v-bind:class="index % 2 ? 'contenidonew00' : 'contenidonew01'" style='text-rendering: optimizeLegibility; cursor: pointer !important;'>
                                <td  style="width:3%; font: 100% sans-serif; padding-left:2px; text-align:center;">{{ detalle[0] }}</td>
                                <td  style="width:3%; font: 100% sans-serif; padding-left:2px; text-align:center;">{{ detalle[1] }}</td>
                                <td  style="width:4%; font: 100% sans-serif; padding-left:2px; text-align:center;">{{ detalle[2] }}</td>
                                <td  style="width:8%; font: 100% sans-serif; padding-left:2px; text-align:center;">{{ detalle[3] }}</td>
                                <td  style="width:6%; font: 100% sans-serif; padding-left:2px; text-align:center;">{{ detalle[4] }}</td>
                                <td  style="width:10%; font: 100% sans-serif; padding-left:2px; text-align:center;">{{ detalle[5] }}</td>
                                <td  style="width:6%; font: 100% sans-serif; padding-left:2px; text-align:center;">{{ detalle[6] }}</td>
                                <td  style="width:6%; font: 100% sans-serif; padding-left:2px; text-align:center;">{{ detalle[7] }}</td>
                                <td  style="width:4%; font: 100% sans-serif; padding-left:2px; text-align:center;">{{ detalle[8] }}</td>
                                <td  style="width:6%; font: 100% sans-serif; padding-left:2px; text-align:center;">{{ detalle[9] }}</td>
                                <td  style="font: 100% sans-serif; padding-left:2px; text-align:center;">{{ detalle[10] }}</td>
                                <td  style="width:4%; font: 100% sans-serif; padding-left:2px; text-align:center;">{{ detalle[11] }}</td>
                                <td  style="width:8%; font: 100% sans-serif; padding-left:2px; text-align:right;">{{ formatonumero(detalle[12]) }}</td>
                                <td  style="width:8%; font: 100% sans-serif; padding-left:2px; text-align:right;">{{ formatonumero(detalle[13]) }}</td>
                                <td  style="width:8%; font: 100% sans-serif; padding-left:2px; text-align:right;">{{ formatonumero(detalle[14]) }}</td>
                            </tr>
                            <tr class="titulosnew04" style="margin-bottom: 2px;" v-show="existeInformacion">
                                <td colspan="12" style="font: 100% sans-serif; padding-left:2px; text-align:center; font-weight: bold;">Total del presupuesto de reducci&oacute;n</td>
                                <td style="width:8%; font: 100% sans-serif; padding-left:2px; text-align:right;">{{ formatonumero(sumaEntradaRed) }}</td>
                                <td style="width:8%; font: 100% sans-serif; padding-left:2px; text-align:right;">{{ formatonumero(sumaSalidaRed) }}</td>
                                <td style="width:8%; font: 100% sans-serif; padding-left:2px; text-align:right;">{{ formatonumero(sumaSaldoRed) }}</td>
                            </tr>

                            <!- Traslado Credito !--> 
                            <tr v-for="(detalle, index) in detallesCred" v-bind:class="index % 2 ? 'contenidonew00' : 'contenidonew01'" style='text-rendering: optimizeLegibility; cursor: pointer !important;'>
                                <td  style="width:3%; font: 100% sans-serif; padding-left:2px; text-align:center;">{{ detalle[0] }}</td>
                                <td  style="width:3%; font: 100% sans-serif; padding-left:2px; text-align:center;">{{ detalle[1] }}</td>
                                <td  style="width:4%; font: 100% sans-serif; padding-left:2px; text-align:center;">{{ detalle[2] }}</td>
                                <td  style="width:8%; font: 100% sans-serif; padding-left:2px; text-align:center;">{{ detalle[3] }}</td>
                                <td  style="width:6%; font: 100% sans-serif; padding-left:2px; text-align:center;">{{ detalle[4] }}</td>
                                <td  style="width:10%; font: 100% sans-serif; padding-left:2px; text-align:center;">{{ detalle[5] }}</td>
                                <td  style="width:6%; font: 100% sans-serif; padding-left:2px; text-align:center;">{{ detalle[6] }}</td>
                                <td  style="width:6%; font: 100% sans-serif; padding-left:2px; text-align:center;">{{ detalle[7] }}</td>
                                <td  style="width:4%; font: 100% sans-serif; padding-left:2px; text-align:center;">{{ detalle[8] }}</td>
                                <td  style="width:6%; font: 100% sans-serif; padding-left:2px; text-align:center;">{{ detalle[9] }}</td>
                                <td  style="font: 100% sans-serif; padding-left:2px; text-align:center;">{{ detalle[10] }}</td>
                                <td  style="width:4%; font: 100% sans-serif; padding-left:2px; text-align:center;">{{ detalle[11] }}</td>
                                <td  style="width:8%; font: 100% sans-serif; padding-left:2px; text-align:right;">{{ formatonumero(detalle[12]) }}</td>
                                <td  style="width:8%; font: 100% sans-serif; padding-left:2px; text-align:right;">{{ formatonumero(detalle[13]) }}</td>
                                <td  style="width:8%; font: 100% sans-serif; padding-left:2px; text-align:right;">{{ formatonumero(detalle[14]) }}</td>
                            </tr>
                            <tr class="titulosnew04" style="margin-bottom: 2px;" v-show="existeInformacion">
                                <td colspan="12" style="font: 100% sans-serif; padding-left:2px; text-align:center; font-weight: bold;">Total del presupuesto de traslado credito</td>
                                <td style="width:8%; font: 100% sans-serif; padding-left:2px; text-align:right;">{{ formatonumero(sumaEntradaCred) }}</td>
                                <td style="width:8%; font: 100% sans-serif; padding-left:2px; text-align:right;">{{ formatonumero(sumaSalidaCred) }}</td>
                                <td style="width:8%; font: 100% sans-serif; padding-left:2px; text-align:right;">{{ formatonumero(sumaSaldoCred) }}</td>
                            </tr>

                            <!- Traslado ContraCredito !--> 
                            <tr v-for="(detalle, index) in detallesContraCred" v-bind:class="index % 2 ? 'contenidonew00' : 'contenidonew01'" style='text-rendering: optimizeLegibility; cursor: pointer !important;'>
                                <td  style="width:3%; font: 100% sans-serif; padding-left:2px; text-align:center;">{{ detalle[0] }}</td>
                                <td  style="width:3%; font: 100% sans-serif; padding-left:2px; text-align:center;">{{ detalle[1] }}</td>
                                <td  style="width:4%; font: 100% sans-serif; padding-left:2px; text-align:center;">{{ detalle[2] }}</td>
                                <td  style="width:8%; font: 100% sans-serif; padding-left:2px; text-align:center;">{{ detalle[3] }}</td>
                                <td  style="width:6%; font: 100% sans-serif; padding-left:2px; text-align:center;">{{ detalle[4] }}</td>
                                <td  style="width:10%; font: 100% sans-serif; padding-left:2px; text-align:center;">{{ detalle[5] }}</td>
                                <td  style="width:6%; font: 100% sans-serif; padding-left:2px; text-align:center;">{{ detalle[6] }}</td>
                                <td  style="width:6%; font: 100% sans-serif; padding-left:2px; text-align:center;">{{ detalle[7] }}</td>
                                <td  style="width:4%; font: 100% sans-serif; padding-left:2px; text-align:center;">{{ detalle[8] }}</td>
                                <td  style="width:6%; font: 100% sans-serif; padding-left:2px; text-align:center;">{{ detalle[9] }}</td>
                                <td  style="font: 100% sans-serif; padding-left:2px; text-align:center;">{{ detalle[10] }}</td>
                                <td  style="width:4%; font: 100% sans-serif; padding-left:2px; text-align:center;">{{ detalle[11] }}</td>
                                <td  style="width:8%; font: 100% sans-serif; padding-left:2px; text-align:right;">{{ formatonumero(detalle[12]) }}</td>
                                <td  style="width:8%; font: 100% sans-serif; padding-left:2px; text-align:right;">{{ formatonumero(detalle[13]) }}</td>
                                <td  style="width:8%; font: 100% sans-serif; padding-left:2px; text-align:right;">{{ formatonumero(detalle[14]) }}</td>
                            </tr>
                            <tr class="titulosnew04" style="margin-bottom: 2px;" v-show="existeInformacion">
                                <td colspan="12" style="font: 100% sans-serif; padding-left:2px; text-align:center; font-weight: bold;">Total del presupuesto de traslado contra credito</td>
                                <td style="width:8%; font: 100% sans-serif; padding-left:2px; text-align:right;">{{ formatonumero(sumaEntradaContraCred) }}</td>
                                <td style="width:8%; font: 100% sans-serif; padding-left:2px; text-align:right;">{{ formatonumero(sumaSalidaContraCred) }}</td>
                                <td style="width:8%; font: 100% sans-serif; padding-left:2px; text-align:right;">{{ formatonumero(sumaSaldoContraCred) }}</td>
                            </tr>

                            <tr style="background: #16a085;" style="margin-bottom: 2px;" v-show="existeInformacion">
                                <td colspan="12" style="font: 100% sans-serif; padding-left:2px; text-align:center; font-weight: bold;">Total de la programacion de gastos</td>
                                <td style="width:8%; font: 100% sans-serif; padding-left:2px; text-align:right;">{{ formatonumero(sumaEntradaProgramacion) }}</td>
                                <td style="width:8%; font: 100% sans-serif; padding-left:2px; text-align:right;">{{ formatonumero(sumaSalidaProgramacion) }}</td>
                                <td style="width:8%; font: 100% sans-serif; padding-left:2px; text-align:right;">{{ formatonumero(sumaSaldoProgramacion) }}</td>
                            </tr>

                            <!- CDP !--> 
                            <tr v-for="(detalle, index) in detallesCdp" v-bind:class="index % 2 ? 'contenidonew00' : 'contenidonew01'" style='text-rendering: optimizeLegibility; cursor: pointer !important;'>
                                <td  style="width:3%; font: 100% sans-serif; padding-left:2px; text-align:center;">{{ detalle[0] }}</td>
                                <td  style="width:3%; font: 100% sans-serif; padding-left:2px; text-align:center;">{{ detalle[1] }}</td>
                                <td  style="width:4%; font: 100% sans-serif; padding-left:2px; text-align:center;">{{ detalle[2] }}</td>
                                <td  style="width:8%; font: 100% sans-serif; padding-left:2px; text-align:center;">{{ detalle[3] }}</td>
                                <td  style="width:6%; font: 100% sans-serif; padding-left:2px; text-align:center;">{{ detalle[4] }}</td>
                                <td  style="width:10%; font: 100% sans-serif; padding-left:2px; text-align:center;">{{ detalle[5] }}</td>
                                <td  style="width:6%; font: 100% sans-serif; padding-left:2px; text-align:center;">{{ detalle[6] }}</td>
                                <td  style="width:6%; font: 100% sans-serif; padding-left:2px; text-align:center;">{{ detalle[7] }}</td>
                                <td  style="width:4%; font: 100% sans-serif; padding-left:2px; text-align:center;">{{ detalle[8] }}</td>
                                <td  style="width:6%; font: 100% sans-serif; padding-left:2px; text-align:center;">{{ detalle[9] }}</td>
                                <td  style="font: 100% sans-serif; padding-left:2px; text-align:center;">{{ detalle[10] }}</td>
                                <td  style="width:4%; font: 100% sans-serif; padding-left:2px; text-align:center;">{{ detalle[11] }}</td>
                                <td  style="width:8%; font: 100% sans-serif; padding-left:2px; text-align:right;">{{ formatonumero(detalle[12]) }}</td>
                                <td  style="width:8%; font: 100% sans-serif; padding-left:2px; text-align:right;">{{ formatonumero(detalle[13]) }}</td>
                                <td  style="width:8%; font: 100% sans-serif; padding-left:2px; text-align:right;">{{ formatonumero(detalle[14]) }}</td>
                            </tr>
                            <tr class="titulosnew04" style="margin-bottom: 2px;" v-show="existeInformacion">
                                <td colspan="12" style="font: 100% sans-serif; padding-left:2px; text-align:center; font-weight: bold;">Total del presupuesto de CDP</td>
                                <td style="width:8%; font: 100% sans-serif; padding-left:2px; text-align:right;">{{ formatonumero(sumaEntradaCdp) }}</td>
                                <td style="width:8%; font: 100% sans-serif; padding-left:2px; text-align:right;">{{ formatonumero(sumaSalidaCdp) }}</td>
                                <td style="width:8%; font: 100% sans-serif; padding-left:2px; text-align:right;">{{ formatonumero(sumaSaldoCdp) }}</td>
                            </tr>

                            <!- RP !--> 
                            <tr v-for="(detalle, index) in detallesRp" v-bind:class="index % 2 ? 'contenidonew00' : 'contenidonew01'" style='text-rendering: optimizeLegibility; cursor: pointer !important;'>
                                <td  style="width:3%; font: 100% sans-serif; padding-left:2px; text-align:center;">{{ detalle[0] }}</td>
                                <td  style="width:3%; font: 100% sans-serif; padding-left:2px; text-align:center;">{{ detalle[1] }}</td>
                                <td  style="width:4%; font: 100% sans-serif; padding-left:2px; text-align:center;">{{ detalle[2] }}</td>
                                <td  style="width:8%; font: 100% sans-serif; padding-left:2px; text-align:center;">{{ detalle[3] }}</td>
                                <td  style="width:6%; font: 100% sans-serif; padding-left:2px; text-align:center;">{{ detalle[4] }}</td>
                                <td  style="width:10%; font: 100% sans-serif; padding-left:2px; text-align:center;">{{ detalle[5] }}</td>
                                <td  style="width:6%; font: 100% sans-serif; padding-left:2px; text-align:center;">{{ detalle[6] }}</td>
                                <td  style="width:6%; font: 100% sans-serif; padding-left:2px; text-align:center;">{{ detalle[7] }}</td>
                                <td  style="width:4%; font: 100% sans-serif; padding-left:2px; text-align:center;">{{ detalle[8] }}</td>
                                <td  style="width:6%; font: 100% sans-serif; padding-left:2px; text-align:center;">{{ detalle[9] }}</td>
                                <td  style="font: 100% sans-serif; padding-left:2px; text-align:center;">{{ detalle[10] }}</td>
                                <td  style="width:4%; font: 100% sans-serif; padding-left:2px; text-align:center;">{{ detalle[11] }}</td>
                                <td  style="width:8%; font: 100% sans-serif; padding-left:2px; text-align:right;">{{ formatonumero(detalle[12]) }}</td>
                                <td  style="width:8%; font: 100% sans-serif; padding-left:2px; text-align:right;">{{ formatonumero(detalle[13]) }}</td>
                                <td  style="width:8%; font: 100% sans-serif; padding-left:2px; text-align:right;">{{ formatonumero(detalle[14]) }}</td>
                            </tr>
                            <tr class="titulosnew04" style="margin-bottom: 2px;" v-show="existeInformacion">
                                <td colspan="12" style="font: 100% sans-serif; padding-left:2px; text-align:center; font-weight: bold;">Total del presupuesto de RP</td>
                                <td style="width:8%; font: 100% sans-serif; padding-left:2px; text-align:right;">{{ formatonumero(sumaEntradaRp) }}</td>
                                <td style="width:8%; font: 100% sans-serif; padding-left:2px; text-align:right;">{{ formatonumero(sumaSalidaRp) }}</td>
                                <td style="width:8%; font: 100% sans-serif; padding-left:2px; text-align:right;">{{ formatonumero(sumaSaldoRp) }}</td>
                            </tr>

                            <!- CXP !--> 
                            <tr v-for="(detalle, index) in detallesCxp" v-bind:class="index % 2 ? 'contenidonew00' : 'contenidonew01'" style='text-rendering: optimizeLegibility; cursor: pointer !important;'>
                                <td  style="width:3%; font: 100% sans-serif; padding-left:2px; text-align:center;">{{ detalle[0] }}</td>
                                <td  style="width:3%; font: 100% sans-serif; padding-left:2px; text-align:center;">{{ detalle[1] }}</td>
                                <td  style="width:4%; font: 100% sans-serif; padding-left:2px; text-align:center;">{{ detalle[2] }}</td>
                                <td  style="width:8%; font: 100% sans-serif; padding-left:2px; text-align:center;">{{ detalle[3] }}</td>
                                <td  style="width:6%; font: 100% sans-serif; padding-left:2px; text-align:center;">{{ detalle[4] }}</td>
                                <td  style="width:10%; font: 100% sans-serif; padding-left:2px; text-align:center;">{{ detalle[5] }}</td>
                                <td  style="width:6%; font: 100% sans-serif; padding-left:2px; text-align:center;">{{ detalle[6] }}</td>
                                <td  style="width:6%; font: 100% sans-serif; padding-left:2px; text-align:center;">{{ detalle[7] }}</td>
                                <td  style="width:4%; font: 100% sans-serif; padding-left:2px; text-align:center;">{{ detalle[8] }}</td>
                                <td  style="width:6%; font: 100% sans-serif; padding-left:2px; text-align:center;">{{ detalle[9] }}</td>
                                <td  style="font: 100% sans-serif; padding-left:2px; text-align:center;">{{ detalle[10] }}</td>
                                <td  style="width:4%; font: 100% sans-serif; padding-left:2px; text-align:center;">{{ detalle[11] }}</td>
                                <td  style="width:8%; font: 100% sans-serif; padding-left:2px; text-align:right;">{{ formatonumero(detalle[12]) }}</td>
                                <td  style="width:8%; font: 100% sans-serif; padding-left:2px; text-align:right;">{{ formatonumero(detalle[13]) }}</td>
                                <td  style="width:8%; font: 100% sans-serif; padding-left:2px; text-align:right;">{{ formatonumero(detalle[14]) }}</td>
                            </tr>
                            <tr class="titulosnew04" style="margin-bottom: 2px;" v-show="existeInformacion">
                                <td colspan="12" style="font: 100% sans-serif; padding-left:2px; text-align:center; font-weight: bold;">Total del presupuesto de CXP</td>
                                <td style="width:8%; font: 100% sans-serif; padding-left:2px; text-align:right;">{{ formatonumero(sumaEntradaCxp) }}</td>
                                <td style="width:8%; font: 100% sans-serif; padding-left:2px; text-align:right;">{{ formatonumero(sumaSalidaCxp) }}</td>
                                <td style="width:8%; font: 100% sans-serif; padding-left:2px; text-align:right;">{{ formatonumero(sumaSaldoCxp) }}</td>
                            </tr>

                            <!- Egreso !--> 
                            <tr v-for="(detalle, index) in detallesEgreso" v-bind:class="index % 2 ? 'contenidonew00' : 'contenidonew01'" style='text-rendering: optimizeLegibility; cursor: pointer !important;'>
                                <td  style="width:3%; font: 100% sans-serif; padding-left:2px; text-align:center;">{{ detalle[0] }}</td>
                                <td  style="width:3%; font: 100% sans-serif; padding-left:2px; text-align:center;">{{ detalle[1] }}</td>
                                <td  style="width:4%; font: 100% sans-serif; padding-left:2px; text-align:center;">{{ detalle[2] }}</td>
                                <td  style="width:8%; font: 100% sans-serif; padding-left:2px; text-align:center;">{{ detalle[3] }}</td>
                                <td  style="width:6%; font: 100% sans-serif; padding-left:2px; text-align:center;">{{ detalle[4] }}</td>
                                <td  style="width:10%; font: 100% sans-serif; padding-left:2px; text-align:center;">{{ detalle[5] }}</td>
                                <td  style="width:6%; font: 100% sans-serif; padding-left:2px; text-align:center;">{{ detalle[6] }}</td>
                                <td  style="width:6%; font: 100% sans-serif; padding-left:2px; text-align:center;">{{ detalle[7] }}</td>
                                <td  style="width:4%; font: 100% sans-serif; padding-left:2px; text-align:center;">{{ detalle[8] }}</td>
                                <td  style="width:6%; font: 100% sans-serif; padding-left:2px; text-align:center;">{{ detalle[9] }}</td>
                                <td  style="font: 100% sans-serif; padding-left:2px; text-align:center;">{{ detalle[10] }}</td>
                                <td  style="width:4%; font: 100% sans-serif; padding-left:2px; text-align:center;">{{ detalle[11] }}</td>
                                <td  style="width:8%; font: 100% sans-serif; padding-left:2px; text-align:right;">{{ formatonumero(detalle[12]) }}</td>
                                <td  style="width:8%; font: 100% sans-serif; padding-left:2px; text-align:right;">{{ formatonumero(detalle[13]) }}</td>
                                <td  style="width:8%; font: 100% sans-serif; padding-left:2px; text-align:right;">{{ formatonumero(detalle[14]) }}</td>
                            </tr>
                            <tr class="titulosnew04" style="margin-bottom: 2px;" v-show="existeInformacion">
                                <td colspan="12" style="font: 100% sans-serif; padding-left:2px; text-align:center; font-weight: bold;">Total del presupuesto de Egresos</td>
                                <td style="width:8%; font: 100% sans-serif; padding-left:2px; text-align:right;">{{ formatonumero(sumaEntradaEgreso) }}</td>
                                <td style="width:8%; font: 100% sans-serif; padding-left:2px; text-align:right;">{{ formatonumero(sumaSalidaEgreso) }}</td>
                                <td style="width:8%; font: 100% sans-serif; padding-left:2px; text-align:right;">{{ formatonumero(sumaSaldoEgreso) }}</td>
                            </tr>

                        </tbody>
                    </table>
                </div>
                <div id="cargando" v-if="loading" class="loading">
                    <span>Cargando...</span>
                </div>

                <div v-show="showModal_rubros">
                    <transition name="modal">
                        <div class="modal-mask">
                            <div class="modal-wrapper">
                                <div class="modal-dialog modal-lg" style = "max-width: 1200px !important;" role="document">
                                    <div class="modal-content"  style = "width: 1200px !important;" scrollable>
                                        <div class="modal-header">
                                            <h5 class="modal-title">Rubros CCPET</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true" @click="showModal_rubros = false">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <div style="margin: 2px 0 0 0;">
                                                <div class="row" style="margin: 4px; border-radius:4px; background-color: #F6F6F6; ">
                                                    <div class="col-md-2" style="padding: 12px 0px 0px 30px; font: 140% sans-serif;">
                                                        <label for="">Cuenta:</label>
                                                    </div>
                                                    
                                                    <div class="col-md-6 col-md-offset-6" style="padding: 4px">
                                                        <input type="text" class="form-control" placeholder="Buscar por nombre o c&oacute;digo de la cuenta" style="font: sans-serif; " @keyup="searchMonitorCuenta" v-model="searchCuenta.keywordCuenta">
                                                    </div>
                                                </div>
                                                <table>
                                                    <thead>
                                                        <tr>
                                                            <td width="20%" class='titulos' style="font: 120% sans-serif; padding-left:10px; border-radius: 5px 0 0 0;">Cuenta</td>
                                                            <td class='titulos' style="font: 120% sans-serif; ">Nombre</td>
                                                            <td width="20%" class='titulos' style="font: 120% sans-serif; ">Tipo</td>

                                                        </tr>
                                                    </thead>
                                                </table>
                                            </div>
                                            <div style="margin: 2px 0 0 0; border-radius: 0 0 0 6px; height: 200px; overflow: scroll; overflow-x: hidden; background: white; ">
                                                <table class='inicio inicio--no-shadow'>
                                                    <tbody>
                                                        <?php
                                                            $co ='zebra1';
                                                            $co2='zebra2';
                                                        ?>
                                                        <tr v-for="cuenta in cuentasCcpet" @click="seleccionarCuenta(cuenta)" class='<?php echo $co; ?>' style='text-rendering: optimizeLegibility; cursor: pointer !important; style=\"cursor: hand\"'>
                                                            <td width="20%" style="font: 120% sans-serif; padding-left:10px">{{ cuenta[1] }}</td>
                                                            <td  style="font: 120% sans-serif; padding-left:10px">{{ cuenta[2] }}</td>
                                                            <td width="20%" style="font: 120% sans-serif; padding-left:10px">{{ cuenta[6] }}</td>
                                                            <?php
                                                            $aux=$co;
                                                            $co=$co2;
                                                            $co2=$aux;
                                                            ?>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary" @click="showModal_rubros = false">Cerrar</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </transition>
                </div>

                <div v-show="showModal_fuentes">
                    <transition name="modal">
                        <div class="modal-mask">
                            <div class="modal-wrapper">
                                <div class="modal-dialog modal-lg" style = "max-width: 1200px !important;" role="document">
                                    <div class="modal-content"  style = "width: 1200px !important;" scrollable>
                                        <div class="modal-header">
                                            <h5 class="modal-title">Fuentes CUIPO</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true" @click="showModal_fuentes = false">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <div style="margin: 2px 0 0 0;">
                                                <div class="row" style="margin: 4px; border-radius:4px; background-color: #F6F6F6; ">
                                                    <div class="col-md-3" style="padding: 12px 0px 0px 30px; font: 140% sans-serif;">
                                                        <label for="">C&oacute;digo o nombre:</label>
                                                    </div>
                                                    
                                                    <div class="col-md-6 col-md-offset-6" style="padding: 4px">
                                                        <input type="text" class="form-control" placeholder="Buscar por nombre o c&oacute;digo de la fuente" style="font: sans-serif; " v-on:keyup="searchMonitorFuente" v-model="searchFuente.keywordFuente">
                                                    </div>
                                                </div>
                                                <table>
                                                    <thead>
                                                        <tr>
                                                            <td width="20%" class='titulos' style="font: 120% sans-serif; padding-left:10px; border-radius: 5px 0 0 0;">Fuente</td>
                                                            <td class='titulos' style="font: 120% sans-serif; ">Nombre</td>

                                                        </tr>
                                                    </thead>
                                                </table>
                                            </div>
                                            <div style="margin: 2px 0 0 0; border-radius: 0 0 0 6px; height: 200px; overflow: scroll; overflow-x: hidden; background: white; ">
                                                <table class='inicio inicio--no-shadow'>
                                                    <tbody>
                                                        <?php
                                                            $co ='zebra1';
                                                            $co2='zebra2';
                                                        ?>
                                                        <tr v-for="fuente in fuentesCuipo" v-on:click="seleccionarFuente(fuente)" class='<?php echo $co; ?>' style='text-rendering: optimizeLegibility; cursor: pointer !important; style=\"cursor: hand\"'>
                                                            <td width="20%" style="font: 120% sans-serif; padding-left:10px">{{ fuente[0] }}</td>
                                                            <td  style="font: 120% sans-serif; padding-left:10px">{{ fuente[1] }}</td>
                                                            <?php
                                                            $aux=$co;
                                                            $co=$co2;
                                                            $co2=$aux;
                                                            ?>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary" @click="showModal_fuentes = false">Cerrar</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </transition>
                </div>

                <div v-show="showModal_bienes_transportables">
                    <transition name="modal">
                        <div class="modal-mask">
                            <div class="modal-wrapper">
                                <div class="modal-dialog modal-lg" style = "max-width: 1200px !important;" role="document">
                                    <div class="modal-content"  style = "width: 1200px !important;" scrollable>
                                        <span id="start_page"> </span>
                                        <div class="modal-header">
                                            <div class="row">
                                                <div class="col-12">
                                                    <h5 class="modal-title">Clasificador Bienes transportables Sec. 0-4</h5>
                                                </div> 
                                            </div>
                                            
                                            
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true" @click="showModal_bienes_transportables = false">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <div v-show="mostrarDivision">
                                                <div class="row" style="margin: 2px 0 0 0; border-radius: 5px !important; border-radius:4px; background-color: #E1E2E2; ">
                                                    <div class="col-md-3" style="padding: 12px 0px 0px 30px; font: 140% sans-serif;">
                                                        <label for="">Buscar Producto:</label>
                                                    </div>
                                                    
                                                    <div class="col-md-6" style="padding: 4px">
                                                        <input type="text" class="form-control" style="height: auto; border-radius:2px;" placeholder="Ej: 1800001" v-on:keyup.enter="buscarGeneral"  v-model="searchGeneral.keywordGeneral">
                                                    </div>
                                                    <div class="col-md-2 col-sm-4 col-md-offset-1" style="padding: 4px">
                                                        <button type="submit" class="btn btn-dark" value="Buscar" style="height: auto; border-radius:5px;" v-on:click="buscarGeneral">Buscar</button>
                                                    </div>
                                                </div>
                                                <div style="margin: 2px 0 0 0; border-radius: 0 0 0 6px; height: 200px; overflow: scroll; overflow-x: hidden; background: white; ">
                                                    <table class='inicio inicio--no-shadow'>
                                                        <tbody>
                                                            <?php
                                                                $co ='zebra1';
                                                                $co2='zebra2';
                                                            ?>
                                                            <tr v-for="division in divisiones" v-on:click="buscarGrupo(division)" v-bind:class="division[0] === division_p ? 'background_active_color' : ''" class='<?php echo $co; ?>' style='text-rendering: optimizeLegibility; cursor: pointer !important; style=\"cursor: hand\"'>
                                                                <td width="20%" style="font: 120% sans-serif; padding-left:10px">{{ division[0] }}</td>
                                                                <td width="80%" style="font: 120% sans-serif; padding-left:10px">{{ division[1] }}</td>

                                                                <?php
                                                                $aux=$co;
                                                                $co=$co2;
                                                                $co2=$aux;
                                                                ?>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                            <div v-show="mostrarGrupo">
                                                <div style="margin: 2px 0 0 0;">
                                                    <div class="row" style="margin: 4px; border-radius:4px; background-color: #E1E2E2; ">
                                                        <div class="col-md-2" style="padding: 12px 0px 0px 30px; font: 140% sans-serif;">
                                                            <label for="">Grupos:</label>
                                                        </div>
                                                        
                                                        <div class="col-md-6 col-md-offset-6" style="padding: 4px">
                                                            <input type="text" class="form-control" placeholder="Buscar por nombre o c&oacute;digo de grupo" v-on:keyup="searchMonitorGrupos" v-model="searchGrupo.keywordGrupo">
                                                        </div>
                                                    </div>
                                                    <table>
                                                        <thead>
                                                            <tr>
                                                                <td width="20%" class='titulos' style="font: 120% sans-serif; padding-left:10px; border-radius: 5px 0 0 0;">C&oacute;digo</td>
                                                                <td width="80%" class='titulos' style="font: 120% sans-serif; ">Nombre</td>
                                                            </tr>
                                                        </thead>
                                                    </table>
                                                </div>
                                                <div style="margin: 2px 0 0 0; border-radius: 0 0 0 6px; height: 200px; overflow: scroll; overflow-x: hidden; background: white; ">
                                                    <table class='inicio inicio--no-shadow'>
                                                        <tbody>
                                                            <?php
                                                                $co ='zebra1';
                                                                $co2='zebra2';
                                                            ?>
                                                            <tr v-for="grupo in grupos" v-on:click="buscarClase(grupo)"  v-bind:class="grupo[0] === grupo_p ? 'background_active_color' : ''" class='<?php echo $co; ?>' style='text-rendering: optimizeLegibility; cursor: pointer !important; style=\"cursor: hand\"'>
                                                                <td width="20%" style="font: 120% sans-serif; padding-left:10px">{{ grupo[0] }}</td>
                                                                <td width="80%" style="font: 120% sans-serif; padding-left:10px">{{ grupo[1] }}</td>

                                                                <?php
                                                                $aux=$co;
                                                                $co=$co2;
                                                                $co2=$aux;
                                                                ?>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                            <div v-show="mostrarClase">
                                                <div style="margin: 2px 0 0 0;">
                                                    <div class="row" style="margin: 4px; border-radius:4px; background-color: #E1E2E2; ">
                                                        <div class="col-md-2" style="padding: 12px 0px 0px 30px; font: 140% sans-serif;">
                                                            <label for="">Clases:</label>
                                                        </div>
                                                        
                                                        <div class="col-md-6 col-md-offset-6" style="padding: 4px">
                                                            <input type="text" class="form-control" placeholder="Buscar por nombre o c&oacute;digo de clase" v-on:keyup="searchMonitorClases" v-model="searchClase.keywordClase">
                                                        </div>
                                                    </div>
                                                    <table>
                                                        <thead>
                                                            <tr>
                                                                <td width="20%" class='titulos' style="font: 120% sans-serif; padding-left:10px; border-radius: 5px 0 0 0;">C&oacute;digo</td>
                                                                <td width="80%" class='titulos' style="font: 120% sans-serif; ">Nombre</td>
                                                            </tr>
                                                        </thead>
                                                    </table>
                                                </div>
                                                <div style="margin: 2px 0 0 0; border-radius: 0 0 0 6px; height: 200px; overflow: scroll; overflow-x: hidden; background: white; ">
                                                    <table class='inicio inicio--no-shadow'>
                                                        <tbody>
                                                            <?php
                                                                $co ='zebra1';
                                                                $co2='zebra2';
                                                            ?>
                                                            <tr v-for="clase in clases" v-on:click="buscarSubclase(clase)"  v-bind:class="clase[0] === clase_p ? 'background_active_color' : ''" class='<?php echo $co; ?>' style='text-rendering: optimizeLegibility; cursor: pointer !important; style=\"cursor: hand\"'>
                                                                <td width="20%" style="font: 120% sans-serif; padding-left:10px">{{ clase[0] }}</td>
                                                                <td width="80%" style="font: 120% sans-serif; padding-left:10px">{{ clase[1] }}</td>

                                                                <?php
                                                                $aux=$co;
                                                                $co=$co2;
                                                                $co2=$aux;
                                                                ?>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                            <div v-show="mostrarSubClase">
                                                <div style="margin: 2px 0 0 0;">
                                                    <div class="row" style="margin: 4px; border-radius:4px; background-color: #E1E2E2; ">
                                                        <div class="col-md-2" style="padding: 12px 0px 0px 30px; font: 140% sans-serif;">
                                                            <label for="">Subclase:</label>
                                                        </div>
                                                        
                                                        <div class="col-md-6 col-md-offset-6" style="padding: 4px">
                                                            <input type="text" class="form-control" placeholder="Buscar por nombre o c&oacute;digo de subclase" v-on:keyup="searchMonitorSubClases" v-model="searchSubClase.keywordSubClase">
                                                        </div>
                                                    </div>
                                                    <table>
                                                        <thead>
                                                            <tr>
                                                                <td width="20%" class='titulos' style="font: 120% sans-serif; padding-left:10px; border-radius: 5px 0 0 0;">C&oacute;digo</td>
                                                                <td width="30%" class='titulos' style="font: 120% sans-serif; ">Nombre</td>
                                                                <td width="15%" class='titulos' style="font: 120% sans-serif; ">CIIU Rev. 4 A.C. </td>
                                                                <td width="25%" class='titulos' style="font: 120% sans-serif; ">Sistema Armonizado 2012</td>
                                                                <td width="10%" class='titulos' style="font: 120% sans-serif; ">CPC 2 A.C.</td>
                                                            </tr>
                                                        </thead>
                                                    </table>
                                                </div>
                                                <div style="margin: 2px 0 0 0; border-radius: 0 0 0 6px; height: 200px; overflow: scroll; overflow-x: hidden; background: white; ">
                                                    <table class='inicio inicio--no-shadow'>
                                                        <tbody>
                                                            <?php
                                                                $co ='zebra1';
                                                                $co2='zebra2';
                                                            ?>
                                                            <tr v-for="subclase in subClases" v-on:click="seleccionarSublaseProducto(subclase)"  v-bind:class="subclase[0] === subClase_p ? 'background_active_color' : ''" class='<?php echo $co; ?>' style='text-rendering: optimizeLegibility; cursor: pointer !important; style=\"cursor: hand\"'>
                                                                <td width="20%" style="font: 120% sans-serif; padding-left:10px">{{ subclase[0] }}</td>
                                                                <td width="30%" style="font: 120% sans-serif; padding-left:10px">{{ subclase[1] }}</td>
                                                                <td width="15%" style="font: 120% sans-serif; padding-left:10px">{{ subclase[2] }}</td>
                                                                <td width="25%" style="font: 120% sans-serif; padding-left:10px">{{ subclase[3] }}</td>
                                                                <td width="10%" style="font: 120% sans-serif; padding-left:10px">{{ subclase[4] }}</td>

                                                                <?php
                                                                $aux=$co;
                                                                $co=$co2;
                                                                $co2=$aux;
                                                                ?>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                            <div v-show="mostrarSubClaseProducto">
                                                <div style="margin: 2px 0 0 0;">
                                                    <div class="row" style="margin: 4px; border-radius:4px; background-color: #E1E2E2; ">
                                                        <div class="col-md-2" style="padding: 12px 0px 0px 30px; font: 140% sans-serif;">
                                                            <label for="">Producto:</label>
                                                        </div>
                                                    </div>
                                                    <table>
                                                        <thead>
                                                            <tr>
                                                                <td width="20%" class='titulos' style="font: 120% sans-serif; padding-left:10px; border-radius: 5px 0 0 0;">Subclase</td>
                                                                <td width="60%" class='titulos' style="font: 120% sans-serif; ">Titulo</td>
                                                                <td width="20%" class='titulos' style="font: 120% sans-serif; ">Ud </td>
                                                            </tr>
                                                        </thead>
                                                    </table>
                                                </div>
                                                <div style="margin: 2px 0 0 0; border-radius: 0 0 0 6px; height: 100px; overflow: scroll; overflow-x: hidden; background: white; ">
                                                    <table class='inicio inicio--no-shadow'>
                                                        <tbody>
                                                            <tr v-for="subclase in subClases_captura" v-on:click="seleccionarBienes(subclase)"  
                                                            v-on:dblclick="seleccionarCpc"
                                                            v-bind:class="subclase[0] === subClase_p ? 'background_active_color' : ''" style='text-rendering: optimizeLegibility; cursor: pointer !important; style=\"cursor: hand\"'>
                                                                <td width="20%" style="font: 120% sans-serif; padding-left:10px">{{ subclase[0] }}</td>
                                                                <td width="60%" style="font: 120% sans-serif; padding-left:10px">{{ subclase[1] }}</td>
                                                                <td width="20%" style="font: 120% sans-serif; padding-left:10px">{{ subclase[2] }}</td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                            <span id="end_page"> </span>
                                        </div>
                                        
                                        
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-primary" v-on:click="seleccionarCpc">Seleccionar Producto</button>
                                            <button type="button" class="btn btn-secondary" @click="showModal_bienes_transportables = false">Cerrar</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </transition>
                </div>

                <div v-show="showModal_servicios">
                    <transition name="modal">
                        <div class="modal-mask">
                            <div class="modal-wrapper">
                                <div class="modal-dialog modal-lg" style = "max-width: 1200px !important;" role="document">
                                    <div class="modal-content"  style = "width: 1200px !important;" scrollable>
                                        <div class="modal-header">
                                            <div class="row">
                                                <div class="col-12">
                                                    <h5 class="modal-title">Clasificador Servicios Sec. 5-9</h5>
                                                </div> 
                                            </div>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true" @click="showModal_servicios = false">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            
                                            <div v-show="mostrarDivisionServicios">
                                                <div style="margin: 2px 0 0 0;">
                                                    <div class="row" style="margin: 4px; border-radius:4px; background-color: #E1E2E2; ">
                                                        <div class="col-md-2" style="padding: 12px 0px 0px 30px; font: 140% sans-serif;">
                                                            <label for="">Divisi&oacute;n:</label>
                                                        </div>
                                                        
                                                        <div class="col-md-6 col-md-offset-6" style="padding: 4px">
                                                            <input type="text" class="form-control" placeholder="Buscar por nombre o c&oacute;digo de divisi&oacute;n" v-on:keyup.enter="buscarGeneral2" v-model="searchGeneral2.keywordGeneral">
                                                        </div>
                                                        <div class="col-md-2 col-sm-4 col-md-offset-1" style="padding: 4px">
                                                            <button type="submit" class="btn btn-dark" value="Buscar" style="height: auto; border-radius:5px;" v-on:click="buscarGeneral2">Buscar</button>
                                                        </div>
                                                    </div>
                                                    <table>
                                                        <thead>
                                                            <tr>
                                                                <td width="20%" class='titulos' style="font: 120% sans-serif; border-radius: 5px 0 0 0;">C&oacute;digo</td>
                                                                <td width="80%" class='titulos' style="font: 120% sans-serif;">Nombre</td>
                                                            </tr>
                                                        </thead>
                                                    </table>
                                                </div>
                                                <div style="margin: 2px 0 0 0; border-radius: 0 0 0 6px; height: 200px; overflow: scroll; overflow-x: hidden; background: white; ">
                                                    <table class='inicio inicio--no-shadow'>
                                                        <tbody>
                                                            <?php
                                                                $co ='zebra1';
                                                                $co2='zebra2';
                                                            ?>
                                                            <tr v-for="division in divisionesServicios" v-on:click="buscarGrupoServicios(division)" v-bind:class="division[0] === divisionServicios_p ? 'background_active_color' : ''" class='<?php echo $co; ?>' style='text-rendering: optimizeLegibility; cursor: pointer !important; style=\"cursor: hand\"'>
                                                                <td width="20%" style="font: 120% sans-serif; padding-left:10px">{{ division[0] }}</td>
                                                                <td width="80%" style="font: 120% sans-serif; padding-left:10px">{{ division[1] }}</td>

                                                                <?php
                                                                $aux=$co;
                                                                $co=$co2;
                                                                $co2=$aux;
                                                                ?>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                            <div v-show="mostrarGrupoServicios">
                                                <div style="margin: 2px 0 0 0;">
                                                    <div class="row" style="margin: 4px; border-radius:4px; background-color: #E1E2E2; ">
                                                        <div class="col-md-2" style="padding: 12px 0px 0px 30px; font: 140% sans-serif;">
                                                            <label for="">Grupos:</label>
                                                        </div>
                                                        
                                                        <div class="col-md-6 col-md-offset-6" style="padding: 4px">
                                                            <input type="text" class="form-control" placeholder="Buscar por nombre o c&oacute;digo de grupo" v-on:keyup="searchMonitorGrupos" v-model="searchGrupoServicios.keywordGrupoServicios">
                                                        </div>
                                                    </div>
                                                    <table>
                                                        <thead>
                                                            <tr>
                                                                <td width="20%" class='titulos' style="font: 120% sans-serif; padding-left:10px; border-radius: 5px 0 0 0;">C&oacute;digo</td>
                                                                <td width="80%" class='titulos' style="font: 120% sans-serif; ">Nombre</td>
                                                            </tr>
                                                        </thead>
                                                    </table>
                                                </div>
                                                <div style="margin: 2px 0 0 0; border-radius: 0 0 0 6px; height: 200px; overflow: scroll; overflow-x: hidden; background: white; ">
                                                    <table class='inicio inicio--no-shadow'>
                                                        <tbody>
                                                            <?php
                                                                $co ='zebra1';
                                                                $co2='zebra2';
                                                            ?>
                                                            <tr v-for="grupo in gruposServicios" v-on:click="buscarClaseServicios(grupo)"  v-bind:class="grupo[0] === grupoServicios_p ? 'background_active_color' : ''" class='<?php echo $co; ?>' style='text-rendering: optimizeLegibility; cursor: pointer !important; style=\"cursor: hand\"'>
                                                                <td width="20%" style="font: 120% sans-serif; padding-left:10px">{{ grupo[0] }}</td>
                                                                <td width="80%" style="font: 120% sans-serif; padding-left:10px">{{ grupo[1] }}</td>

                                                                <?php
                                                                $aux=$co;
                                                                $co=$co2;
                                                                $co2=$aux;
                                                                ?>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                            <div v-show="mostrarClaseServicios">
                                                <div style="margin: 2px 0 0 0;">
                                                    <div class="row" style="margin: 4px; border-radius:4px; background-color: #E1E2E2; ">
                                                        <div class="col-md-2" style="padding: 12px 0px 0px 30px; font: 140% sans-serif;">
                                                            <label for="">Clases:</label>
                                                        </div>
                                                        
                                                        <div class="col-md-6 col-md-offset-6" style="padding: 4px">
                                                            <input type="text" class="form-control" placeholder="Buscar por nombre o c&oacute;digo de clase" v-on:keyup="searchMonitorClases" v-model="searchClaseServicios.keywordClaseServicios">
                                                        </div>
                                                    </div>
                                                    <table>
                                                        <thead>
                                                            <tr>
                                                                <td width="20%" class='titulos' style="font: 120% sans-serif; padding-left:10px; border-radius: 5px 0 0 0;">C&oacute;digo</td>
                                                                <td width="80%" class='titulos' style="font: 120% sans-serif; ">Nombre</td>
                                                            </tr>
                                                        </thead>
                                                    </table>
                                                </div>
                                                <div style="margin: 2px 0 0 0; border-radius: 0 0 0 6px; height: 200px; overflow: scroll; overflow-x: hidden; background: white; ">
                                                    <table class='inicio inicio--no-shadow'>
                                                        <tbody>
                                                            <?php
                                                                $co ='zebra1';
                                                                $co2='zebra2';
                                                            ?>
                                                            <tr v-for="clase in clasesServicios" v-on:click="buscarSubclaseServicios(clase)"  v-bind:class="clase[0] === claseServicios_p ? 'background_active_color' : ''" class='<?php echo $co; ?>' style='text-rendering: optimizeLegibility; cursor: pointer !important; style=\"cursor: hand\"'>
                                                                <td width="20%" style="font: 120% sans-serif; padding-left:10px">{{ clase[0] }}</td>
                                                                <td width="80%" style="font: 120% sans-serif; padding-left:10px">{{ clase[1] }}</td>

                                                                <?php
                                                                $aux=$co;
                                                                $co=$co2;
                                                                $co2=$aux;
                                                                ?>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                            <div v-show="mostrarSubClaseServicios">
                                                <div style="margin: 2px 0 0 0;">
                                                    <div class="row" style="margin: 4px; border-radius:4px; background-color: #E1E2E2; ">
                                                        <div class="col-md-2" style="padding: 12px 0px 0px 30px; font: 140% sans-serif;">
                                                            <label for="">Subclase:</label>
                                                        </div>
                                                        
                                                        <div class="col-md-6 col-md-offset-6" style="padding: 4px">
                                                            <input type="text" class="form-control" placeholder="Buscar por nombre o c&oacute;digo de subclase" v-on:keyup="searchMonitorSubClases" v-model="searchSubClase.keywordSubClase">
                                                        </div>
                                                    </div>
                                                    <table>
                                                        <thead>
                                                            <tr>
                                                                <td width="20%" class='titulos' style="font: 120% sans-serif; padding-left:10px; border-radius: 5px 0 0 0;">C&oacute;digo</td>
                                                                <td width="30%" class='titulos' style="font: 120% sans-serif; ">Nombre</td>
                                                                <td width="15%" class='titulos' style="font: 120% sans-serif; ">CIIU Rev. 4 A.C. </td>
                                                                <td width="25%" class='titulos' style="font: 120% sans-serif; ">Sistema Armonizado 2012</td>
                                                                <td width="10%" class='titulos' style="font: 120% sans-serif; ">CPC 2 A.C.</td>
                                                            </tr>
                                                        </thead>
                                                    </table>
                                                </div>
                                                <div style="margin: 2px 0 0 0; border-radius: 0 0 0 6px; height: 200px; overflow: scroll; overflow-x: hidden; background: white; ">
                                                    <table class='inicio inicio--no-shadow'>
                                                        <tbody>
                                                            <?php
                                                                $co ='zebra1';
                                                                $co2='zebra2';
                                                            ?>
                                                            <tr v-for="subclase in subClasesServicios" v-on:click="seleccionarServicios(subclase)"  
                                                            v-on:dblclick="seleccionarServicioCpc"
                                                            v-bind:class="subclase[0] === subClaseServicios_p ? 'background_active_color' : ''" class='<?php echo $co; ?>' style='text-rendering: optimizeLegibility; cursor: pointer !important; style=\"cursor: hand\"'>
                                                                <td width="20%" style="font: 120% sans-serif; padding-left:10px">{{ subclase[0] }}</td>
                                                                <td width="30%" style="font: 120% sans-serif; padding-left:10px">{{ subclase[1] }}</td>
                                                                <td width="15%" style="font: 120% sans-serif; padding-left:10px">{{ subclase[2] }}</td>
                                                                <td width="25%" style="font: 120% sans-serif; padding-left:10px">{{ subclase[3] }}</td>
                                                                <td width="10%" style="font: 120% sans-serif; padding-left:10px">{{ subclase[4] }}</td>

                                                                <?php
                                                                $aux=$co;
                                                                $co=$co2;
                                                                $co2=$aux;
                                                                ?>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                            <span id="end_page_servicios"> </span>
                                        </div>
                                        
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-primary" v-on:click="seleccionarServicioCpc">Seleccionar servicio</button>
                                            <button type="button" class="btn btn-secondary" @click="showModal_servicios = false">Cerrar</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </transition>
                </div>

                <div v-show="showModal_proyectos">
                    <transition name="modal">
                        <div class="modal-mask">
                            <div class="modal-wrapper">
                                <div class="modal-dialog modal-lg" style = "max-width: 1200px !important;" role="document">
                                    <div class="modal-content"  style = "width: 1200px !important;" scrollable>
                                        <div class="modal-header">
                                            <h5 class="modal-title">Proyectos de inversion </h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true" @click="showModal_proyectos = false">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <div style="margin: 2px 0 0 0;">
                                                <div class="row" style="margin: 4px; border-radius:4px; background-color: #F6F6F6; ">
                                                    <div class="col-md-2" style="padding: 12px 0px 0px 30px; font: 140% sans-serif;">
                                                        <label for="">Proyecto:</label>
                                                    </div>
                                                    
                                                    <div class="col-md-6 col-md-offset-6" style="padding: 4px">
                                                        <input type="text" class="form-control" placeholder="Buscar por nombre o BPIM" style="font: sans-serif; " v-on:keyup="searchMonitorProyecto" v-model="searchProyecto.keywordProyecto">
                                                    </div>
                                                </div>
                                                <table>
                                                    <thead>
                                                        <tr>
                                                            <td width="20%" class='titulos' style="font: 120% sans-serif; padding-left:10px; border-radius: 5px 0 0 0;">BPIM</td>
                                                            <td class='titulos' style="font: 120% sans-serif; ">Nombre</td>
                                                        </tr>
                                                    </thead>
                                                </table>
                                            </div>
                                            <div style="margin: 2px 0 0 0; border-radius: 0 0 0 6px; height: 200px; overflow: scroll; overflow-x: hidden; background: white; ">
                                                <table class='inicio inicio--no-shadow'>
                                                    <tbody>
                                                        <?php
                                                            $co ='zebra1';
                                                            $co2='zebra2';
                                                        ?>
                                                        <tr v-for="proyecto in proyectos" v-on:click="seleccionarProyecto(proyecto[2],proyecto[4])" class='<?php echo $co; ?>' style='text-rendering: optimizeLegibility; cursor: pointer !important; style=\"cursor: hand\"'>
                                                            <td width="20%" style="font: 120% sans-serif; padding-left:10px">{{ proyecto[2] }}</td>
                                                            <td  style="font: 120% sans-serif; padding-left:10px">{{ proyecto[4] }}</td>
                                                            <?php
                                                            $aux=$co;
                                                            $co=$co2;
                                                            $co2=$aux;
                                                            ?>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary" @click="showModal_proyectos = false">Cerrar</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </transition>
                </div>

                <div v-show="showModal_programatico">
                    <transition name="modal">
                        <div class="modal-mask">
                            <div class="modal-wrapper">
                                <div class="modal-dialog modal-lg" style = "max-width: 1200px !important;" role="document">
                                    <div class="modal-content"  style = "width: 1200px !important;" scrollable>
                                        <div class="modal-header">
                                            <h5 class="modal-title">Program&aacute;tico </h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true" @click="showModal_programatico = false">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <div style="margin: 2px 0 0 0;">
                                                <div class="row" style="margin: 4px; border-radius:4px; background-color: #F6F6F6; ">
                                                    
                                                </div>
                                                <table>
                                                    <thead>
                                                        <tr>
                                                            <td width="20%" class='titulos' style="font: 120% sans-serif; padding-left:10px; border-radius: 5px 0 0 0;">Program&aacute;tico</td>
                                                            <td class='titulos' style="font: 120% sans-serif; ">Nombre</td>
                                                        </tr>
                                                    </thead>
                                                </table>
                                            </div>
                                            <div style="margin: 2px 0 0 0; border-radius: 0 0 0 6px; height: 200px; overflow: scroll; overflow-x: hidden; background: white; ">
                                                <table class='inicio inicio--no-shadow'>
                                                    <tbody>
                                                        <?php
                                                            $co ='zebra1';
                                                            $co2='zebra2';
                                                        ?>
                                                        <tr v-for="programatico in programaticos" v-on:click="seleccionarProgramatico(programatico)" class='<?php echo $co; ?>' style='text-rendering: optimizeLegibility; cursor: pointer !important; style=\"cursor: hand\"'>
                                                            <td width="20%" style="font: 120% sans-serif; padding-left:10px">{{ programatico[0] }}</td>
                                                            <td  style="font: 120% sans-serif; padding-left:10px">{{ programatico[1] }}</td>
                                                            <?php
                                                            $aux=$co;
                                                            $co=$co2;
                                                            $co2=$aux;
                                                            ?>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary" @click="showModal_programatico = false">Cerrar</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </transition>
                </div>
                <div id="cargando" v-if="loading" class="loading">
                    <span>Cargando...</span>
                </div>



            </article>
        </section>
        <!-- <script type="module" src="./ejemplo.js"></script> -->
        <!-- <script src="Librerias/vue3/dist/vue.global.js"></script> -->
        <script type="module" src="./presupuesto_ccpet/AuxiliarGastos/ccp-auxiliarGastos.js"></script>
		<script src="Librerias/vue/axios.min.js"></script>

    </body>
</html>