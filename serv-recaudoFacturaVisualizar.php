<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require 'comun.inc';
	require 'funciones.inc';
	session_start();
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html lang=es>
	<head>
    <meta charset="utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1"/>
		<meta name="viewport" content="user-scalable=no">
		<title>:: IDEAL 10 - Servicios públicos</title>
		<link href="favicon.ico" rel="shortcut icon"/>
		<link href="css/css2z.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
        <link href="css/tabs.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" href="sweetalert2/dist/sweetalert2.min.css">
        <script type="text/javascript" src="css/programas.js"></script>
        <script src="sweetalert2/dist/sweetalert2.min.js"></script>
		<script src="vue/vue.min.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script src="css/calendario.js?<?php echo date('d_m_Y_h_i_s');?>"></script>

        <style>
            .checkbox-wrapper-31:hover .check {
                stroke-dashoffset: 0;
            }
        
            .checkbox-wrapper-31 {
                position: relative;
                display: inline-block;
                width: 40px;
                height: 40px;
            }
            .checkbox-wrapper-31 .background {
                fill: #ccc;
                transition: ease all 0.6s;
                -webkit-transition: ease all 0.6s;
            }
            .checkbox-wrapper-31 .stroke {
                fill: none;
                stroke: #fff;
                stroke-miterlimit: 10;
                stroke-width: 2px;
                stroke-dashoffset: 100;
                stroke-dasharray: 100;
                transition: ease all 0.6s;
                -webkit-transition: ease all 0.6s;
            }
            .checkbox-wrapper-31 .check {
                fill: none;
                stroke: #fff;
                stroke-linecap: round;
                stroke-linejoin: round;
                stroke-width: 2px;
                stroke-dashoffset: 22;
                stroke-dasharray: 22;
                transition: ease all 0.6s;
                -webkit-transition: ease all 0.6s;
            }
            .checkbox-wrapper-31 input[type=checkbox] {
                position: absolute;
                width: 100%;
                height: 100%;
                left: 0;
                top: 0;
                margin: 0;
                opacity: 0;
                -appearance: none;
            }
            .checkbox-wrapper-31 input[type=checkbox]:hover {
                cursor: pointer;
            }
            .checkbox-wrapper-31 input[type=checkbox]:checked + svg .background {
                fill: #6cbe45;
            }
            .checkbox-wrapper-31 input[type=checkbox]:checked + svg .stroke {
                stroke-dashoffset: 0;
            }
            .checkbox-wrapper-31 input[type=checkbox]:checked + svg .check {
                stroke-dashoffset: 0;
            }
        </style>
	</head>
	<body>
		<header>
			<table>
				<tr><script>barra_imagenes("serv");</script><?php cuadro_titulos();?></tr>
			</table>
		</header>

		<form name="form2" method="post" action="">
			<section id="myapp">
				<nav>
					<table>
						<tr><?php menu_desplegable("serv");?></tr>
						<tr>
							<td colspan="3" class="cinta">
								<img src="imagenes/add.png" v-on:click="location.href='serv-recaudoFactura'" class="mgbt" title="Nuevo">
								<img src="imagenes/busca.png" v-on:click="location.href='serv-buscarRecaudoFactura'" class="mgbt" title="Buscar">
								<img src="imagenes/nv.png" onClick="mypop=window.open('serv-principal','',''); mypop.focus();" class="mgbt" title="Nueva Ventana">
                                <a @click="printPDF()" class="mgbt"><img src="imagenes/print.png" title="Imprimir" /></a>
							</td>
						</tr>
					</table>
				</nav>
				<article>
                    <div>
                        <table class="inicio">
                            <tr>
                                <td class="titulos" colspan="10">.: Detalle de recaudo</td>
                            </tr>
                            
                            <tr>
                                <td class="textonew01" style="width:13%;">.: Consecutivo recaudo:</td>
                                <td style="width: 11%;">
                                    <input type="text" :value="strCodRecaudo" style="text-align: center;" readonly>
                                </td>

                                <td class="textonew01" style="width:10%;">.: Fecha de recaudo:</td>
                                <td>
                                    <input type="text"  :value="strFechaRecaudo" style="text-align:center;"  readonly>
                                </td>
                                <td class="textonew01" style="width:10%;">.: Estado:</td>
                                <td colspan="3" >
                                    <input type="text"  :value="strEstado" style="width: 20%;" readonly>
                                </td>
                                
                            </tr>

                            <tr>
                                <td class="textonew01" style="width:10%;">.: Codigo de usuario:</td>
                                <td style="width: 11%;">
                                    <input type="text"  :value="strCodUsuario" style="text-align:center;" readonly>
                                </td>

                                <td class="textonew01" style="width:13%;">.: Documento tercero:</td>
                                <td style="width: 11%;">
                                    <input type="text"  :value="strDocumento" style="text-align:center;" readonly>
                                </td>
                                <td class="textonew01" style="width:10%;">.: Nombre de tercero:</td>
                                <td colspan="3">
                                    <input type="text"  :value="strNombre" style="width: 95%;" readonly>
                                </td>
                                
                            </tr>

                            <tr>
                                
                                <td class="textonew01" style="width:5%;">.: Medio de pago:</td>
                                <td style="width: 11%;">
                                    <input type="text"  :value="strMedioPago" style="text-align: center;" readonly>
                                </td>
                                <td class="textonew01" style="width:10%;">.: Número de factura:</td>
                                <td style="width: 11%;">
                                    <input type="text"  :value="strNumeroFactura" style="text-align: center;" readonly>
                                </td>
                                <td class="textonew01" style="width:10%;">.: Concepto:</td>
                                <td colspan="3">
                                    <input type="text" :value="strConcepto" style="width: 95%;" readonly>
                                </td>
                            </tr>

                            <tr>
                                <td class="textonew01" style="width:10%;">.: Valor de factura:</td>
                                <td style="width: 11%;">
                                    <input type="text"  :value="strValorFactura" style="text-align: center;" readonly>
                                </td>
                                <td class="textonew01" style="width:13%;">.: Descuento de intereses:</td>
                                <td style="width: 11%;">
                                    <input type="text"  :value="intDescuento" style="text-align: center;" readonly>
                                </td>
                                
                                
                                <td class="textonew01" style="width:10%;">.: Valor de pago:</td>
                                <td colspan="3" >
                                    <input type="text"  :value="strValorPago" style="width: 20%;" readonly>
                                </td>
                            </tr>

                        </table>
                    </div>
				</article>
			</section>
		</form>

		<script src="node_modules/read-excel-file/bundle/read-excel-file.min.js"></script>
		<script src="node_modules/xlsx/dist/xlsx.full.min.js"></script>
        <script src="Librerias/vue/vue.min.js"></script>
		<script src="Librerias/vue/axios.min.js"></script>
		<script type="module" src="servicios_publicos/recaudoFactura/ver/serv-verRecaudoFactura.js"></script>
        
	</body>
</html>