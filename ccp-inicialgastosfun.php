<?php
	require "comun.inc";
	require "funciones.inc";
	session_start();
	cargarcodigopag(@$_GET['codpag'], @$_SESSION['nivel']);
	header("Cache-control: private"); // Arregla IE 6
	date_default_timezone_set("America/Bogota");
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>:: IDEAL 10 - Presupuesto</title>

        <link href="css/css2.css" rel="stylesheet" type="text/css" />
        <link href="css/css3.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="css/programas.js"></script>
        <!-- <script type="text/javascript" src="css/funciones.js"></script> -->
        <link rel="stylesheet" href="bootstrap/css/bootstrap.css">
        <link rel="stylesheet" href="bootstrap/css/estilos.css">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">

        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
        <script type="text/javascript" src="bootstrap/fontawesome.5.11.2/js/all.js"></script>
        <link href="css/style.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
        <?php titlepag();?>

        <style>
            .background_active_color{
                background: #16a085;
            }
            .background_active{
                /* font: 115% sans-serif !important; */
                /*font-weight: 700 !important;*/
                /*font-family: "Constantia", serif !important;*/
                font-family: calibri !important;
                font-weight: bold !important;
                font-size:20px !important;
            }
            .background_active_1{
                /* font: 115% sans-serif !important; */
                /*font-weight: 700 !important;*/
                /*font-family: "Constantia", serif !important;*/
                font-family: helvética !important;
                font-size:20px !important;
            }
            .inicio--no-shadow{
                box-shadow: none;
            }
            .btn-delete{
                background: red;
                color: white;
                border-radius: 5px;
                border: none;
                font-size: 13px;
            }
            .btn-delete:hover, .btn-delete:focus{
                background: white;
                color: red;
            }
            .modal-mask {
            position: fixed;
            z-index: 9998;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
            background-color: rgba(0, 0, 0, .5);
            display: table;
            transition: opacity .3s ease;
            }

            .modal-wrapper {
            display: table-cell;
            vertical-align: middle;
            }
            .modal-body{
                max-height: 296px;
                overflow-y: scroll;
            }
            .modal-intermetio{
                margin: 0 15px;
                font-family: helvética !important;
                font-size: 26px !important;
                padding: 10px 0;
            }
            .modal-intermedio-agregar{
                text-align:right;
                padding: 4px;
                margin-top: 6px;
                margin-right: 20px
            }
            .modal-body_1{
                padding-top: 15px;
                height: 40px;
            }
            .loader-table{
                /* background-color: #dff9fb;
                opacity: .5; */
                display: flex;
                align-items: center;
                justify-content: center;
                height: 75%;
            }
            .spinner{
                border: 4px solid rgba(0, 0, 0, 0.2);
                border-left-color: #39C;
                border-radius: 50%;
                width: 50px;
                height: 50px;
                animation: spin .9s linear infinite;
            }
            @keyframes spin {
                to { transform: rotate(360deg); }
            }
            .modal-container1
            {
                width: 50%;
                margin: 0px auto;
                padding: 20px 30px;
                text-align: left;
                background:linear-gradient(#99bbcc, #B6CEDA);
                border-radius: 10px;
                box-shadow: 0 2px 8px rgba(0, 0, 0, .33);
                transition: all .3s ease;
            }

            [v-cloak]{
                display : none;
            }

        </style>
    </head>
    <body>
        <table>
            <tr><script>barra_imagenes("ccpet");</script><?php cuadro_titulos();?></tr>
            <tr><?php menu_desplegable("ccpet");?></tr>
		</table>
        <div class="bg-white group-btn p-1">
            <button type="button"  onClick="location.href='#'" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
                <span>Nuevo</span>
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960">
                    <path d="M440-440H200v-80h240v-240h80v240h240v80H520v240h-80v-240Z"></path>
                </svg>
            </button>
            <button type="button" onClick="location.href='#'" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
                <span>Guardar</span>
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960">
                    <path d="M840-680v480q0 33-23.5 56.5T760-120H200q-33 0-56.5-23.5T120-200v-560q0-33 23.5-56.5T200-840h480l160 160Zm-80 34L646-760H200v560h560v-446ZM480-240q50 0 85-35t35-85q0-50-35-85t-85-35q-50 0-85 35t-35 85q0 50 35 85t85 35ZM240-560h360v-160H240v160Zm-40-86v446-560 114Z">
                    </path>
                </svg>
            </button>
            <button type="button" v-on:click="location.href='#'" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
                <span>Buscar</span>
                <svg class="fill-black group-hover:fill-white w-5 h-5" xmlns="http://www.w3.org/2000/svg"
                    viewBox="0 -960 960 960">
                    <path
                        d="M784-120 532-372q-30 24-69 38t-83 14q-109 0-184.5-75.5T120-580q0-109 75.5-184.5T380-840q109 0 184.5 75.5T640-580q0 44-14 83t-38 69l252 252-56 56ZM380-400q75 0 127.5-52.5T560-580q0-75-52.5-127.5T380-760q-75 0-127.5 52.5T200-580q0 75 52.5 127.5T380-400Z">
                    </path>
                </svg>
            </button>
            <button type="button" onclick="mypop=window.open('ccp-principal.php','','');mypop.focus();" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
                <span>Nueva ventana</span>
                <svg class="fill-black group-hover:fill-white w-5 h-5" xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960">
                    <path d="M320-120v-80h80v-80H160q-33 0-56.5-23.5T80-360v-400q0-33 23.5-56.5T160-840h640q33 0 56.5 23.5T880-760v400q0 33-23.5 56.5T800-280H560v80h80v80H320ZM160-360h640v-400H160v400Zm0 0v-400 400Z"></path>
                </svg>
            </button>
            <button type="button" onClick="location.href='ccp-capturapresupuestoinicial.php'" class="btn btn-success btn-success-hover d-flex justify-between align-items-center">
                <span>Atras</span>
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960">
                    <path d="M760-200v-160q0-50-35-85t-85-35H273l144 144-57 56-240-240 240-240 57 56-144 144h367q83 0 141.5 58.5T840-360v160h-80Z"></path>
                </svg>
            </button>
        </div>
        <div class="subpantalla" style="height:520px; width:99.6%; overflow:hidden;">
            <div id="myapp" style="height:inherit;" v-cloak>
                <div class="row">
					<div class="col-12">
						<h5 style="padding-left:30px; padding-top:5px; padding-bottom:5px; background-color: #0FB0D4">Agregar presupuesto gastos de funcionamiento:</h5>
					</div>
				</div>
                <div class="row" style="margin: 1px 10px 0px">
                    <div class="col-12">
                        <div class="row" style="border-radius:2px; background-color: #E1E2E2; ">
                            <div class="col-md-2" style="display: grid; align-content:center;">
								<label for="" style="margin-bottom: 0; font-size:15px;">Dependencia: </label>
							</div>
							<div class="col-md-2" style="padding: 4px">
                                <select v-model="selectUnidadEjecutora" v-on:change="buscarGastos" class="form-control select" style = "font-size: 10px; margin-top:4px">
                                    <option v-for="unidad in unidadesejecutoras" v-bind:value="unidad[0]">
                                        {{ unidad[0] }} - {{ unidad[1] }}
                                    </option>
                                </select>
							</div>
                            <div class="col-md-1" style="display: grid; align-content:center;">
								<label for="" style="margin-bottom: 0; font-size:14px;">Medio pago: </label>
							</div>
							<div class="col-md-1" style="padding: 4px">

                                <select v-model="selectMedioPago" v-on:change="buscarGastos" class="form-control select" style = "font-size: 10px; margin-top:4px">
                                    <option v-for="option in optionsMediosPagos" v-bind:value="option.value">
                                        {{ option.text }}
                                    </option>
                                </select>
							</div>

                            <div class="col-md-2" style="display: grid; align-content:center;">
								<label for="" style="margin-bottom: 0; font-size:15px;">Vigencia del gasto: </label>
							</div>
							<div class="col-md-2" style="padding: 4px">
                                <select v-model="selectVigenciaGasto" v-on:change="buscarGastos" class="form-control select" style = "font-size: 10px; margin-top:4px">
                                    <option v-for="vigenciaDeGasto in vigenciasdelgasto" v-bind:value="vigenciaDeGasto[0]">
                                        {{ vigenciaDeGasto[1] }} - {{ vigenciaDeGasto[2] }}
                                    </option>
                                </select>
							</div>

                            <div class="col-md-1" style="display: grid; align-content:center;">
								<label for="" style="margin-bottom: 0; font-size:15px;">Vigencia: </label>
							</div>
							<div class="col-md-1" style="display: grid; align-content:center;">
								<select v-model="vigencia" style="width:100%" v-on:Change="buscarGastos" class="form-control" style = "font-size: 10px; margin-top:4px">
									<option v-for="year in years" :value="year[0]">{{ year[0] }}</option>
								</select>
							</div>

                        </div>

                    </div>
                </div>
                <div class="row">
					<div class="col-12" v-show="mostrar_resultados_gastos">
						<div style="margin: 5px 10px 0">
                            <table>
								<thead>
									<tr>
										<td class='titulos' width="20%"  style="padding-left: 10px; font: 160% sans-serif; border-radius: 5px 0px 0px 0px;">C&oacute;digo</td>
										<td class='titulos' width="40%"  style="font: 160% sans-serif;">Nombre</td>
										<td class='titulos' width="20%" style="font: 160% sans-serif; ">Tipo</td>

										<td class='titulos' width="20%" style="font: 160% sans-serif; border-radius: 0px 5px 0px 0px;">Valor</td>
									</tr>
								</thead>
							</table>
                        </div>
                        <div style="margin: 0px 10px 10px; border-radius: 0 0 0 5px; max-height: 390px; overflow: scroll; overflow-x: hidden; background: white; ">
                            <table class='inicio inicio--no-shadow' style='margin: 10px; border-radius: 4px;'>
                                <tbody v-if="mostrar_resultados_gastos">
                                    <?php
										$co ='zebra1';
										$co2='zebra2';
									?>
                                    <tr v-for="gasto in gastos" v-bind:class="gasto[2] == 'A' ? 'background_active' : 'background_active_1' " class='<?php echo $co; ?>' style="font: 130% sans-serif;">
										<td width="10%" style="padding-left: 10px; ">{{ gasto[0] }}</td>
										<td width="40%" >{{ gasto[1] }}</td>
										<td width="10%" >{{ gasto[2] }}</td>

                                        <td width="30%" v-if="gasto[2] == 'C' " v-on:dblclick="agregarPresupuesto(gasto[0])" style='text-rendering: optimizeLegibility; text-align:center; background-color: #dbeafe;  cursor: pointer !important; border-radius: 3px;'>

											 {{ formatonumero(valorIngresoCuenta[gasto[0]]) }}

                                        </td>

                                        <td v-else style='text-rendering: optimizeLegibility; cursor: hand !important; text-align:center;'>
                                            {{ formatonumero(valorIngresoCuenta[gasto[0]]) }}
                                        </td>

										<?php
										$aux=$co;
										$co=$co2;
										$co2=$aux;
										?>
									</tr>
                                </tbody>
                                <tbody v-else>
									<tr>
										<td width="20%"style="font: 120% sans-serif; padding-left:10px; text-align:center;" colspan="3">Sin resultados</td>
									</tr>
								</tbody>
                            </table>
                        </div>
                    </div>
                </div>

                <div v-show="showModal_fuentes">
					<transition name="modal">
						<div class="modal-mask">
							<div class="modal-wrapper">
								<div class="modal-dialog modal-lg" style = "max-width: 1200px !important;" role="document">
									<div class="modal-content"  style = "width: 100% !important;" scrollable>
										<div class="modal-header">
											<h5 class="modal-title">Fuentes</h5>
											<button type="button" class="close" data-dismiss="modal" aria-label="Close">
												<span aria-hidden="true" @click="showModal_fuentes = false">&times;</span>
											</button>
										</div>
                                        <div class="modal-body">
                                            <div style="margin: 2px 0 0 0;">
                                                <div class="row" style="margin: 4px; border-radius:4px; background-color: #E1E2E2; ">
                                                    <div class="col-md-2" style="padding: 12px 0px 0px 30px; font: 140% sans-serif;">
                                                        <label for="">Fuente:</label>
                                                    </div>

                                                    <div class="col-md-6 col-md-offset-6" style="padding: 4px">
                                                        <input type="text" class="form-control" placeholder="Buscar por nombre o c&oacute;digo de la fuente" v-on:keyup="searchMonitorFuente" v-model="searchFuente.keywordFuente">
                                                    </div>
                                                </div>
                                                <table>
                                                    <thead>
                                                        <tr>
                                                            <td width="20%" class='titulos' style="font: 120% sans-serif; padding-left:10px; border-radius: 5px 0 0 0;">C&oacute;digo</td>
                                                            <td width="40%" class='titulos' style="font: 120% sans-serif; ">Nombre</td>
                                                            <td width="20%" class='titulos' style="font: 120% sans-serif; ">Valor</td>

                                                        </tr>
                                                    </thead>
                                                </table>
                                            </div>
                                            <div style="margin: 2px 0 0 0; border-radius: 0 0 0 6px; height: 200px; overflow: scroll; overflow-x: hidden; background: white; ">
                                                <table class='inicio inicio--no-shadow'>
                                                    <tbody>
                                                        <?php
                                                            $co ='zebra1';
                                                            $co2='zebra2';
                                                        ?>
                                                        <tr v-for="fuente in fuentes" v-on:click="seleccionarFuente(fuente)"  v-on:dblclick= "continuar" v-bind:class="fuente[0] === fuente_p ? 'background_active_color' : ''" class='<?php echo $co; ?>' style='text-rendering: optimizeLegibility; cursor: pointer !important; style=\"cursor: hand\"'>
                                                            <td width="20%" style="font: 120% sans-serif; padding-left:10px">{{ fuente[1] }}</td>
                                                            <td width="40%" style="font: 120% sans-serif; padding-left:10px">{{ fuente[2] }}</td>
                                                            <td width="20%" style="font: 120% sans-serif; padding-left:10px">{{ valorGasto[fuente[1]] }}</td>

                                                            <?php
                                                            $aux=$co;
                                                            $co=$co2;
                                                            $co2=$aux;
                                                            ?>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
										<div class="modal-footer">
											<button type="button" class="btn btn-primary" v-on:click="continuar">Continuar</button>
											<button type="button" class="btn btn-secondary" @click="showModal_fuentes = false">Cerrar</button>
										</div>
									</div>
								</div>
							</div>
						</div>
					</transition>
				</div>

                <div v-show="showModal_Solo_Presupuesto">
					<transition name="modal">
						<div class="modal-mask">
							<div class="modal-wrapper">
								<div class="modal-dialog modal-lg" role="document">
									<div class="modal-content"  style = "width: 700px !important;" scrollable>
										<div class="modal-header">
                                            <div class="row">
                                                <div class="col-10">
                                                    <h5 class="modal-title">Valor presupuesto</h5>
                                                </div>
                                                <div class="col-2">
                                                    <button type="button" class="btn btn-secondary" v-on:click="buscarFuente()">Volver</button>
                                                </div>
                                            </div>
											<button type="button" class="close" data-dismiss="modal" aria-label="Close">
												<span aria-hidden="true" @click="showModal_Solo_Presupuesto = false">&times;</span>
											</button>
										</div>

                                        <div class="modal-body">
                                            <div class="modal-intermedio-agregar">
                                                <div class="row" style="margin-left: 15px;">
                                                    <form class="form-inline">
                                                        <div class="form-group">
                                                            <label for="valorSolo">Valor:</label>
                                                            <input type="number" v-model="valorSolo" v-on:keydown.enter.prevent="guardarValorSolo" class="form-control mx-sm-3">

                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
										<div class="modal-footer">
											<button type="button" class="btn btn-primary" v-on:click="guardarValorSolo">Guardar y Continuar</button>
											<button type="button" class="btn btn-secondary" @click="showModal_Solo_Presupuesto = false">Cerrar</button>
										</div>
									</div>
								</div>
							</div>
						</div>
					</transition>
				</div>

                <div v-show="showModal_bienes_transportables">
					<transition name="modal">
						<div class="modal-mask">
							<div class="modal-wrapper">
								<div class="modal-dialog modal-lg" style = "max-width: 1200px !important;" role="document">
									<div class="modal-content"  style = "width: 1200px !important;" scrollable>
                                        <span id="start_page"> </span>
										<div class="modal-header">
                                            <div class="row">
                                                <div class="col-10">
                                                    <h5 class="modal-title">Clasificador Bienes transportables Sec. 0-4</h5>
                                                </div>
                                                <div class="col-2">
                                                    <button type="button" class="btn btn-secondary" v-on:click="buscarFuente()">Volver</button>
                                                </div>
                                            </div>


											<button type="button" class="close" data-dismiss="modal" aria-label="Close">
												<span aria-hidden="true" @click="showModal_bienes_transportables = false">&times;</span>
											</button>
										</div>
                                        <div class="modal-body">
                                            <div v-show="mostrarDivision">
                                                <div class="row" style="margin: 2px 0 0 0; border-radius: 5px !important; border-radius:4px; background-color: #E1E2E2; ">
                                                    <div class="col-md-3" style="padding: 12px 0px 0px 30px; font: 140% sans-serif;">
                                                        <label for="">Buscar Producto:</label>
                                                    </div>

                                                    <div class="col-md-6" style="padding: 4px">
                                                        <input type="text" class="form-control" style="height: auto; border-radius:2px;" placeholder="Ej: trigo" v-on:keyup.enter="buscarGeneral"  v-model="searchGeneral.keywordGeneral">
                                                    </div>
                                                    <div class="col-md-2 col-sm-4 col-md-offset-1" style="padding: 4px">
                                                        <button type="submit" class="btn btn-dark" value="Buscar" style="height: auto; border-radius:5px;" v-on:click="buscarGeneral">Buscar</button>
                                                    </div>
                                                </div>
                                                <div style="margin: 2px 0 0 0; border-radius: 0 0 0 6px; height: 200px; overflow: scroll; overflow-x: hidden; background: white; ">
                                                    <table class='inicio inicio--no-shadow'>
                                                        <tbody>
                                                            <?php
                                                                $co ='zebra1';
                                                                $co2='zebra2';
                                                            ?>
                                                            <tr v-for="division in divisiones" v-on:click="buscarGrupo(division)" v-bind:class="division[0] === division_p ? 'background_active_color' : ''" class='<?php echo $co; ?>' style='text-rendering: optimizeLegibility; cursor: pointer !important; style=\"cursor: hand\"'>
                                                                <td width="20%" style="font: 120% sans-serif; padding-left:10px">{{ division[0] }}</td>
                                                                <td width="80%" style="font: 120% sans-serif; padding-left:10px">{{ division[1] }}</td>

                                                                <?php
                                                                $aux=$co;
                                                                $co=$co2;
                                                                $co2=$aux;
                                                                ?>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                            <div v-show="mostrarGrupo">
                                                <div style="margin: 2px 0 0 0;">
                                                    <div class="row" style="margin: 4px; border-radius:4px; background-color: #E1E2E2; ">
                                                        <div class="col-md-2" style="padding: 12px 0px 0px 30px; font: 140% sans-serif;">
                                                            <label for="">Grupos:</label>
                                                        </div>

                                                        <div class="col-md-6 col-md-offset-6" style="padding: 4px">
                                                            <input type="text" class="form-control" placeholder="Buscar por nombre o c&oacute;digo de grupo" v-on:keyup="searchMonitorGrupos" v-model="searchGrupo.keywordGrupo">
                                                        </div>
                                                    </div>
                                                    <table>
                                                        <thead>
                                                            <tr>
                                                                <td width="20%" class='titulos' style="font: 120% sans-serif; padding-left:10px; border-radius: 5px 0 0 0;">C&oacute;digo</td>
                                                                <td width="80%" class='titulos' style="font: 120% sans-serif; ">Nombre</td>
                                                            </tr>
                                                        </thead>
                                                    </table>
                                                </div>
                                                <div style="margin: 2px 0 0 0; border-radius: 0 0 0 6px; height: 200px; overflow: scroll; overflow-x: hidden; background: white; ">
                                                    <table class='inicio inicio--no-shadow'>
                                                        <tbody>
                                                            <?php
                                                                $co ='zebra1';
                                                                $co2='zebra2';
                                                            ?>
                                                            <tr v-for="grupo in grupos" v-on:click="buscarClase(grupo)"  v-bind:class="grupo[0] === grupo_p ? 'background_active_color' : ''" class='<?php echo $co; ?>' style='text-rendering: optimizeLegibility; cursor: pointer !important; style=\"cursor: hand\"'>
                                                                <td width="20%" style="font: 120% sans-serif; padding-left:10px">{{ grupo[0] }}</td>
                                                                <td width="80%" style="font: 120% sans-serif; padding-left:10px">{{ grupo[1] }}</td>

                                                                <?php
                                                                $aux=$co;
                                                                $co=$co2;
                                                                $co2=$aux;
                                                                ?>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                            <div v-show="mostrarClase">
                                                <div style="margin: 2px 0 0 0;">
                                                    <div class="row" style="margin: 4px; border-radius:4px; background-color: #E1E2E2; ">
                                                        <div class="col-md-2" style="padding: 12px 0px 0px 30px; font: 140% sans-serif;">
                                                            <label for="">Clases:</label>
                                                        </div>

                                                        <div class="col-md-6 col-md-offset-6" style="padding: 4px">
                                                            <input type="text" class="form-control" placeholder="Buscar por nombre o c&oacute;digo de clase" v-on:keyup="searchMonitorClases" v-model="searchClase.keywordClase">
                                                        </div>
                                                    </div>
                                                    <table>
                                                        <thead>
                                                            <tr>
                                                                <td width="20%" class='titulos' style="font: 120% sans-serif; padding-left:10px; border-radius: 5px 0 0 0;">C&oacute;digo</td>
                                                                <td width="80%" class='titulos' style="font: 120% sans-serif; ">Nombre</td>
                                                            </tr>
                                                        </thead>
                                                    </table>
                                                </div>
                                                <div style="margin: 2px 0 0 0; border-radius: 0 0 0 6px; height: 200px; overflow: scroll; overflow-x: hidden; background: white; ">
                                                    <table class='inicio inicio--no-shadow'>
                                                        <tbody>
                                                            <?php
                                                                $co ='zebra1';
                                                                $co2='zebra2';
                                                            ?>
                                                            <tr v-for="clase in clases" v-on:click="buscarSubclase(clase)"  v-bind:class="clase[0] === clase_p ? 'background_active_color' : ''" class='<?php echo $co; ?>' style='text-rendering: optimizeLegibility; cursor: pointer !important; style=\"cursor: hand\"'>
                                                                <td width="20%" style="font: 120% sans-serif; padding-left:10px">{{ clase[0] }}</td>
                                                                <td width="80%" style="font: 120% sans-serif; padding-left:10px">{{ clase[1] }}</td>

                                                                <?php
                                                                $aux=$co;
                                                                $co=$co2;
                                                                $co2=$aux;
                                                                ?>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                            <div v-show="mostrarSubClase">
                                                <div style="margin: 2px 0 0 0;">
                                                    <div class="row" style="margin: 4px; border-radius:4px; background-color: #E1E2E2; ">
                                                        <div class="col-md-2" style="padding: 12px 0px 0px 30px; font: 140% sans-serif;">
                                                            <label for="">Subclase:</label>
                                                        </div>

                                                        <div class="col-md-6 col-md-offset-6" style="padding: 4px">
                                                            <input type="text" class="form-control" placeholder="Buscar por nombre o c&oacute;digo de subclase" v-on:keyup="searchMonitorSubClases" v-model="searchSubClase.keywordSubClase">
                                                        </div>
                                                    </div>
                                                    <table>
                                                        <thead>
                                                            <tr>
                                                                <td width="20%" class='titulos' style="font: 120% sans-serif; padding-left:10px; border-radius: 5px 0 0 0;">C&oacute;digo</td>
                                                                <td width="30%" class='titulos' style="font: 120% sans-serif; ">Nombre</td>
                                                                <td width="15%" class='titulos' style="font: 120% sans-serif; ">CIIU Rev. 4 A.C. </td>
                                                                <td width="25%" class='titulos' style="font: 120% sans-serif; ">Sistema Armonizado 2012</td>
                                                                <td width="10%" class='titulos' style="font: 120% sans-serif; ">CPC 2 A.C.</td>
                                                            </tr>
                                                        </thead>
                                                    </table>
                                                </div>
                                                <div style="margin: 2px 0 0 0; border-radius: 0 0 0 6px; height: 200px; overflow: scroll; overflow-x: hidden; background: white; ">
                                                    <table class='inicio inicio--no-shadow'>
                                                        <tbody>
                                                            <?php
                                                                $co ='zebra1';
                                                                $co2='zebra2';
                                                            ?>
                                                            <tr v-for="subclase in subClases" v-on:click="seleccionarSublaseProducto(subclase)"  v-bind:class="subclase[0] === subClase_p ? 'background_active_color' : ''" class='<?php echo $co; ?>' style='text-rendering: optimizeLegibility; cursor: pointer !important; style=\"cursor: hand\"'>
                                                                <td width="20%" style="font: 120% sans-serif; padding-left:10px">{{ subclase[0] }}</td>
                                                                <td width="30%" style="font: 120% sans-serif; padding-left:10px">{{ subclase[1] }}</td>
                                                                <td width="15%" style="font: 120% sans-serif; padding-left:10px">{{ subclase[2] }}</td>
                                                                <td width="25%" style="font: 120% sans-serif; padding-left:10px">{{ subclase[3] }}</td>
                                                                <td width="10%" style="font: 120% sans-serif; padding-left:10px">{{ subclase[4] }}</td>

                                                                <?php
                                                                $aux=$co;
                                                                $co=$co2;
                                                                $co2=$aux;
                                                                ?>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                            <div v-show="mostrarSubClaseProducto">
                                                <div style="margin: 2px 0 0 0;">
                                                    <div class="row" style="margin: 4px; border-radius:4px; background-color: #E1E2E2; ">
                                                        <div class="col-md-2" style="padding: 12px 0px 0px 30px; font: 140% sans-serif;">
                                                            <label for="">Producto:</label>
                                                        </div>
                                                    </div>
                                                    <table>
                                                        <thead>
                                                            <tr>
                                                                <td width="20%" class='titulos' style="font: 120% sans-serif; padding-left:10px; border-radius: 5px 0 0 0;">Subclase</td>
                                                                <td width="60%" class='titulos' style="font: 120% sans-serif; ">Titulo</td>
                                                                <td width="20%" class='titulos' style="font: 120% sans-serif; ">Ud </td>
                                                            </tr>
                                                        </thead>
                                                    </table>
                                                </div>
                                                <div style="margin: 2px 0 0 0; border-radius: 0 0 0 6px; height: 100px; overflow: scroll; overflow-x: hidden; background: white; ">
                                                    <table class='inicio inicio--no-shadow'>
                                                        <tbody>
                                                            <tr v-for="subclase in subClases_captura" v-on:click="seleccionarBienes(subclase)"  v-bind:class="subclase[0] === subClase_p ? 'background_active_color' : ''" style='text-rendering: optimizeLegibility; cursor: pointer !important; style=\"cursor: hand\"'>
                                                                <td width="20%" style="font: 120% sans-serif; padding-left:10px">{{ subclase[0] }}</td>
                                                                <td width="60%" style="font: 120% sans-serif; padding-left:10px">{{ subclase[1] }}</td>
                                                                <td width="20%" style="font: 120% sans-serif; padding-left:10px">{{ subclase[2] }}</td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                            <span id="end_page"> </span>
                                        </div>

										<div class="modal-intermedio-agregar">
											<div class="row" style="margin-left: 15px;">
												<form class="form-inline">
													<div class="form-group">
														<label for="valorBienesTranspotables">Valor:</label>
														<input type="number" v-model="valorBienesTranspotables" class="form-control mx-sm-3">

														<button type="button" class="btn btn-primary" v-on:click="agregaBienesTranspotables">Agregar</button>

													</div>
												</form>

												<form class="form-inline" style='padding-left:10px'>
													<div class="form-group">
															<label for="valorTotal">Valor Total:</label>
															<input type="number" v-model="valorTotal" clazss="form-control mx-sm-3" style="margin-left:2px" readonly>
													</div>
												</form>
											</div>
										</div>
                                        <!-- tabla de valores -->
										<div class="modal-body" style='height:140px; paddind: 0px'>
											<table v-if="show_table_search" class="table table-hover" >
												<thead>
													<tr>
														<th>C&oacute;digo</th>
														<th>Nombre</th>
                                                        <th>Ud</th>
														<th>Valor</th>
														<th></th>
                                                        <th>Eliminar</th>
													</tr>
												</thead>
												<tbody>
                                                    <!-- array de datos -->
													<tr v-for="cuenta in cuentasSubClaseAgr" style="font-weight: normal">
														<td>{{ cuenta[0] }}</td>
														<td>{{ cuenta[1] }}</td>
														<td>{{ cuenta[2] }}</td>
                                                        <td>{{ cuenta[3] }}</td>
                                                        <td>

														</td>
														<td>
															<button type="button" class="btn btn-danger" v-on:click="eliminarBienes(cuenta)">Eliminar</button>
														</td>
													</tr>
												</tbody>
											</table>
										</div>
										<div class="modal-footer">
											<button type="button" class="btn btn-primary" v-on:click="guardarBienes">Guardar y continuar</button>
											<button type="button" class="btn btn-secondary" @click="showModal_bienes_transportables = false">Cerrar</button>
										</div>
									</div>
								</div>
							</div>
						</div>
					</transition>
				</div>


                <div v-show="showModal_servicios">
					<transition name="modal">
						<div class="modal-mask">
							<div class="modal-wrapper">
								<div class="modal-dialog modal-lg" style = "max-width: 1200px !important;" role="document">
									<div class="modal-content"  style = "width: 1200px !important;" scrollable>
										<div class="modal-header">
                                            <div class="row">
                                                <div class="col-10">
                                                    <h5 class="modal-title">Clasificador Servicios Sec. 5-9</h5>
                                                </div>
                                                <div class="col-2">
                                                    <button type="button" class="btn btn-secondary" v-on:click="buscarFuente()">Volver</button>
                                                </div>
                                            </div>
											<button type="button" class="close" data-dismiss="modal" aria-label="Close">
												<span aria-hidden="true" @click="showModal_servicios = false">&times;</span>
											</button>
										</div>
                                        <div class="modal-body">

                                            <div v-show="mostrarDivisionServicios">
                                                <div style="margin: 2px 0 0 0;">
                                                    <div class="row" style="margin: 4px; border-radius:4px; background-color: #E1E2E2; ">
                                                        <div class="col-md-2" style="padding: 12px 0px 0px 30px; font: 140% sans-serif;">
                                                            <label for="">Divisi&oacute;n:</label>
                                                        </div>

                                                        <div class="col-md-6 col-md-offset-6" style="padding: 4px">
                                                            <input type="text" class="form-control" placeholder="Buscar por nombre o c&oacute;digo de divisi&oacute;n" v-on:keyup.enter="buscarGeneral2" v-model="searchGeneral2.keywordGeneral">
                                                        </div>
														<div class="col-md-2 col-sm-4 col-md-offset-1" style="padding: 4px">
															<button type="submit" class="btn btn-dark" value="Buscar" style="height: auto; border-radius:5px;" v-on:click="buscarGeneral2">Buscar</button>
														</div>
                                                    </div>
                                                    <table>
                                                        <thead>
                                                            <tr>
                                                                <td width="20%" class='titulos' style="font: 120% sans-serif; border-radius: 5px 0 0 0;">C&oacute;digo</td>
                                                                <td width="80%" class='titulos' style="font: 120% sans-serif;">Nombre</td>
                                                            </tr>
                                                        </thead>
                                                    </table>
                                                </div>
                                                <div style="margin: 2px 0 0 0; border-radius: 0 0 0 6px; height: 200px; overflow: scroll; overflow-x: hidden; background: white; ">
                                                    <table class='inicio inicio--no-shadow'>
                                                        <tbody>
                                                            <?php
                                                                $co ='zebra1';
                                                                $co2='zebra2';
                                                            ?>
                                                            <tr v-for="division in divisionesServicios" v-on:click="buscarGrupoServicios(division)" v-bind:class="division[0] === divisionServicios_p ? 'background_active_color' : ''" class='<?php echo $co; ?>' style='text-rendering: optimizeLegibility; cursor: pointer !important; style=\"cursor: hand\"'>
                                                                <td width="20%" style="font: 120% sans-serif; padding-left:10px">{{ division[0] }}</td>
                                                                <td width="80%" style="font: 120% sans-serif; padding-left:10px">{{ division[1] }}</td>

                                                                <?php
                                                                $aux=$co;
                                                                $co=$co2;
                                                                $co2=$aux;
                                                                ?>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                            <div v-show="mostrarGrupoServicios">
                                                <div style="margin: 2px 0 0 0;">
                                                    <div class="row" style="margin: 4px; border-radius:4px; background-color: #E1E2E2; ">
                                                        <div class="col-md-2" style="padding: 12px 0px 0px 30px; font: 140% sans-serif;">
                                                            <label for="">Grupos:</label>
                                                        </div>

                                                        <div class="col-md-6 col-md-offset-6" style="padding: 4px">
                                                            <input type="text" class="form-control" placeholder="Buscar por nombre o c&oacute;digo de grupo" v-on:keyup="searchMonitorGrupos" v-model="searchGrupoServicios.keywordGrupoServicios">
                                                        </div>
                                                    </div>
                                                    <table>
                                                        <thead>
                                                            <tr>
                                                                <td width="20%" class='titulos' style="font: 120% sans-serif; padding-left:10px; border-radius: 5px 0 0 0;">C&oacute;digo</td>
                                                                <td width="80%" class='titulos' style="font: 120% sans-serif; ">Nombre</td>
                                                            </tr>
                                                        </thead>
                                                    </table>
                                                </div>
                                                <div style="margin: 2px 0 0 0; border-radius: 0 0 0 6px; height: 200px; overflow: scroll; overflow-x: hidden; background: white; ">
                                                    <table class='inicio inicio--no-shadow'>
                                                        <tbody>
                                                            <?php
                                                                $co ='zebra1';
                                                                $co2='zebra2';
                                                            ?>
                                                            <tr v-for="grupo in gruposServicios" v-on:click="buscarClaseServicios(grupo)"  v-bind:class="grupo[0] === grupoServicios_p ? 'background_active_color' : ''" class='<?php echo $co; ?>' style='text-rendering: optimizeLegibility; cursor: pointer !important; style=\"cursor: hand\"'>
                                                                <td width="20%" style="font: 120% sans-serif; padding-left:10px">{{ grupo[0] }}</td>
                                                                <td width="80%" style="font: 120% sans-serif; padding-left:10px">{{ grupo[1] }}</td>

                                                                <?php
                                                                $aux=$co;
                                                                $co=$co2;
                                                                $co2=$aux;
                                                                ?>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                            <div v-show="mostrarClaseServicios">
                                                <div style="margin: 2px 0 0 0;">
                                                    <div class="row" style="margin: 4px; border-radius:4px; background-color: #E1E2E2; ">
                                                        <div class="col-md-2" style="padding: 12px 0px 0px 30px; font: 140% sans-serif;">
                                                            <label for="">Clases:</label>
                                                        </div>

                                                        <div class="col-md-6 col-md-offset-6" style="padding: 4px">
                                                            <input type="text" class="form-control" placeholder="Buscar por nombre o c&oacute;digo de clase" v-on:keyup="searchMonitorClases" v-model="searchClaseServicios.keywordClaseServicios">
                                                        </div>
                                                    </div>
                                                    <table>
                                                        <thead>
                                                            <tr>
                                                                <td width="20%" class='titulos' style="font: 120% sans-serif; padding-left:10px; border-radius: 5px 0 0 0;">C&oacute;digo</td>
                                                                <td width="80%" class='titulos' style="font: 120% sans-serif; ">Nombre</td>
                                                            </tr>
                                                        </thead>
                                                    </table>
                                                </div>
                                                <div style="margin: 2px 0 0 0; border-radius: 0 0 0 6px; height: 200px; overflow: scroll; overflow-x: hidden; background: white; ">
                                                    <table class='inicio inicio--no-shadow'>
                                                        <tbody>
                                                            <?php
                                                                $co ='zebra1';
                                                                $co2='zebra2';
                                                            ?>
                                                            <tr v-for="clase in clasesServicios" v-on:click="buscarSubclaseServicios(clase)"  v-bind:class="clase[0] === claseServicios_p ? 'background_active_color' : ''" class='<?php echo $co; ?>' style='text-rendering: optimizeLegibility; cursor: pointer !important; style=\"cursor: hand\"'>
                                                                <td width="20%" style="font: 120% sans-serif; padding-left:10px">{{ clase[0] }}</td>
                                                                <td width="80%" style="font: 120% sans-serif; padding-left:10px">{{ clase[1] }}</td>

                                                                <?php
                                                                $aux=$co;
                                                                $co=$co2;
                                                                $co2=$aux;
                                                                ?>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                            <div v-show="mostrarSubClaseServicios">
                                                <div style="margin: 2px 0 0 0;">
                                                    <div class="row" style="margin: 4px; border-radius:4px; background-color: #E1E2E2; ">
                                                        <div class="col-md-2" style="padding: 12px 0px 0px 30px; font: 140% sans-serif;">
                                                            <label for="">Subclase:</label>
                                                        </div>

                                                        <div class="col-md-6 col-md-offset-6" style="padding: 4px">
                                                            <input type="text" class="form-control" placeholder="Buscar por nombre o c&oacute;digo de subclase" v-on:keyup="searchMonitorSubClases" v-model="searchSubClase.keywordSubClase">
                                                        </div>
                                                    </div>
                                                    <table>
                                                        <thead>
                                                            <tr>
                                                                <td width="20%" class='titulos' style="font: 120% sans-serif; padding-left:10px; border-radius: 5px 0 0 0;">C&oacute;digo</td>
                                                                <td width="30%" class='titulos' style="font: 120% sans-serif; ">Nombre</td>
                                                                <td width="15%" class='titulos' style="font: 120% sans-serif; ">CIIU Rev. 4 A.C. </td>
                                                                <td width="25%" class='titulos' style="font: 120% sans-serif; ">Sistema Armonizado 2012</td>
                                                                <td width="10%" class='titulos' style="font: 120% sans-serif; ">CPC 2 A.C.</td>
                                                            </tr>
                                                        </thead>
                                                    </table>
                                                </div>
                                                <div style="margin: 2px 0 0 0; border-radius: 0 0 0 6px; height: 200px; overflow: scroll; overflow-x: hidden; background: white; ">
                                                    <table class='inicio inicio--no-shadow'>
                                                        <tbody>
                                                            <?php
                                                                $co ='zebra1';
                                                                $co2='zebra2';
                                                            ?>
                                                            <tr v-for="subclase in subClasesServicios" v-on:click="seleccionarServicios(subclase)"  v-bind:class="subclase[0] === subClaseServicios_p ? 'background_active_color' : ''" class='<?php echo $co; ?>' style='text-rendering: optimizeLegibility; cursor: pointer !important; style=\"cursor: hand\"'>
                                                                <td width="20%" style="font: 120% sans-serif; padding-left:10px">{{ subclase[0] }}</td>
                                                                <td width="30%" style="font: 120% sans-serif; padding-left:10px">{{ subclase[1] }}</td>
                                                                <td width="15%" style="font: 120% sans-serif; padding-left:10px">{{ subclase[2] }}</td>
                                                                <td width="25%" style="font: 120% sans-serif; padding-left:10px">{{ subclase[3] }}</td>
                                                                <td width="10%" style="font: 120% sans-serif; padding-left:10px">{{ subclase[4] }}</td>

                                                                <?php
                                                                $aux=$co;
                                                                $co=$co2;
                                                                $co2=$aux;
                                                                ?>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                            <span id="end_page_servicios"> </span>
                                        </div>

										<div class="modal-intermedio-agregar">
											<div class="row" style="margin-left: 15px;">
												<form class="form-inline">
													<div class="form-group">
														<label for="valorServicios">Valor:</label>
														<input type="number" v-model="valorServicios" class="form-control mx-sm-3">

														<button type="button" class="btn btn-primary" v-on:click="agregaServicios">Agregar</button>

													</div>
												</form>

												<form class="form-inline" style='padding-left:10px'>
													<div class="form-group">
															<label for="valorTotalServicios">Valor Total:</label>
															<input type="number" v-model="valorTotalServicios" class="form-control mx-sm-3" style="margin-left:2px" readonly>
													</div>
												</form>
											</div>
										</div>
										<div class="modal-body" style='height:140px; paddind: 0px'>
											<table v-if="show_table_search" class="table table-hover" >
												<thead>
													<tr>
														<th>C&oacute;digo</th>
														<th>Nombre</th>
														<th>Valor</th>
														<th>Eliminar</th>
													</tr>
												</thead>
												<tbody>
													<tr v-for="cuenta in cuentasSubClaseServiciosAgr" style="font-weight: normal">
														<td>{{ cuenta[0] }}</td>
														<td>{{ cuenta[1] }}</td>
														<td>{{ cuenta[4] }}</td>
														<td>
															<button type="button" class="btn btn-danger" v-on:click="eliminarServicios(cuenta)">Eliminar</button>
														</td>
													</tr>
												</tbody>
											</table>
										</div>
										<div class="modal-footer">
											<button type="button" class="btn btn-primary" v-on:click="guardarServicios">Guardar y continuar</button>
											<button type="button" class="btn btn-secondary" @click="showModal_servicios = false">Cerrar</button>
										</div>
									</div>
								</div>
							</div>
						</div>
					</transition>
				</div>


            </div>
        </div>

        <script src="Librerias/vue/vue.min.js"></script>
		<script src="Librerias/vue/axios.min.js"></script>
		<script src="vue/presupuesto_ccp/ccp-inicialgastosfun.js?<?php echo date('d_m_Y_h_i_s');?>"></script>

    </body>
</html>
