<?php
header("Cache-control: no-cache, no-store, must-revalidate");
header("Content-Type: text/html;charset=utf8");
require "comun.inc";
require "funciones.inc";
require "conversor.php";
sesion();
$linkbd = conectar_v7();
$linkbd->set_charset("utf8");
cargarcodigopag($_GET['codpag'], $_SESSION["nivel"]);
date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE>
<html lang="es">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1">
    <meta name="viewport" content="user-scalable=no">
    <title>:: IDEAL 10 - Tesorer&iacute;a</title>
    <link href="favicon.ico" rel="shortcut icon" />
    <link href="css/css2z.css?<?php echo date('d_m_Y_h_i_s'); ?>" rel="stylesheet" />
    <link href="css/css3.css?<?php echo date('d_m_Y_h_i_s'); ?>" rel="stylesheet" />
    <link href="css/css4.css?<?php echo date('d_m_Y_h_i_s'); ?>" rel="stylesheet" />
    <link href="css/style.css?<?php echo date('d_m_Y_h_i_s'); ?>" rel="stylesheet" />
    <link href="sweetalert2/dist/sweetalert2.min.css" rel="stylesheet">
    <script src="sweetalert2/dist/sweetalert2.min.js"></script>
    <script type="text/javascript" src="css/calendario.js?<?php echo date('d_m_Y_h_i_s'); ?>"></script>
    <script type="text/javascript" src="css/programas.js?<?php echo date('d_m_Y_h_i_s'); ?>"></script>
    <script>
        function buscacta(e) {
            if (document.form2.cuenta.value != "") {
                document.form2.bc.value = '1';
                document.form2.submit();
            }
        }
        function validar() {
            document.form2.submit();
        }
        function buscater(e) {
            if (document.form2.tercero.value != "") {
                document.form2.bt.value = '1';
                document.form2.submit();
            }
        }
        function pdf() {
            document.form2.action = "teso-trasladospdf";
            document.form2.target = "_BLANK";
            document.form2.submit();
            document.form2.action = "";
            document.form2.target = "";
        }

        function despliegamodalm(_valor, _tip, mensa, pregunta) {
            document.getElementById("bgventanamodalm").style.visibility = _valor;
            if (_valor == "hidden") {
                document.getElementById('ventanam').src = "";
            } else {
                switch (_tip) {
                    case "1": document.getElementById('ventanam').src = "ventana-mensaje1.php?titulos=" + mensa; break;
                    case "2": document.getElementById('ventanam').src = "ventana-mensaje3.php?titulos=" + mensa; break;
                    case "3": document.getElementById('ventanam').src = "ventana-mensaje2.php?titulos=" + mensa; break;
                    case "4": document.getElementById('ventanam').src = "ventana-consulta1.php?titulos=" + mensa + "&idresp=" + pregunta; break;
                }
            }
        }
        function funcionmensaje() {
            //document.location.href = "hum-liquidarnominamirar.php?idnomi="+document.form2.idcomp.value;
        }
        function respuestaconsulta(pregunta) {
            switch (pregunta) {
                case "1":
                    document.form2.oculto.value = 2;
                    document.form2.submit();
                    break;
                case "2":
                    document.form2.oculto.value = 9;
                    document.form2.submit();
                    break;
            }
        }
        function atrasc() {
            let codig = document.form2.idcomp.value;
            let minim = document.form2.minimo.value;
            codig = parseFloat(codig) - 1;
            if (codig >= minim) { location.href = "teso-trasladosvisualizar?idr=" + codig + "&dc=" + codig; }
        }
        function adelante() {
            let codig = document.form2.idcomp.value;
            let maxim = document.form2.maximo.value;
            codig = parseFloat(codig) + 1;
            if (codig <= maxim) { location.href = "teso-trasladosvisualizar?idr=" + codig + "&dc=" + codig; }
        }
        function iratras() {
            let codig = document.form2.idcomp.value;
            let scrtop = document.form2.scrtop.value;
            location.href = "teso-buscatraslados?idr=" + codig + "&scrtop=" + scrtop;
        }
    </script>
    <?php titlepag(); ?>
</head>

<body>
    <IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
    <span id="todastablas2"></span>
    <table>
        <tr>
            <script>barra_imagenes("teso");</script><?php cuadro_titulos(); ?>
        </tr>
        <tr><?php menu_desplegable("teso"); ?></tr>
    </table>
    <div class="bg-white group-btn p-1"><button type="button" onclick="location.href='teso-traslados'"
            class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
            <span>Nuevo</span>
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960">
                <path d="M440-440H200v-80h240v-240h80v240h240v80H520v240h-80v-240Z"></path>
            </svg>
        </button><button type="button" onclick="location.href='teso-buscatraslados'"
            class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
            <span>Buscar</span>
            <svg class="fill-black group-hover:fill-white w-5 h-5" xmlns="http://www.w3.org/2000/svg"
                viewBox="0 -960 960 960">
                <path
                    d="M784-120 532-372q-30 24-69 38t-83 14q-109 0-184.5-75.5T120-580q0-109 75.5-184.5T380-840q109 0 184.5 75.5T640-580q0 44-14 83t-38 69l252 252-56 56ZM380-400q75 0 127.5-52.5T560-580q0-75-52.5-127.5T380-760q-75 0-127.5 52.5T200-580q0 75 52.5 127.5T380-400Z">
                </path>
            </svg>
        </button><button type="button" onclick="mypop=window.open('plan-agenda','','');mypop.focus()"
            class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
            <span class="group-hover:text-white">Agenda</span>
            <svg class="fill-black group-hover:fill-white w-5 h-5" xmlns="http://www.w3.org/2000/svg"
                viewBox="0 -960 960 960">
                <path
                    d="M560-564v-68q33-14 67.5-21t72.5-7q26 0 51 4t49 10v64q-24-9-48.5-13.5T700-600q-38 0-73 9.5T560-564Zm0 220v-68q33-14 67.5-21t72.5-7q26 0 51 4t49 10v64q-24-9-48.5-13.5T700-380q-38 0-73 9t-67 27Zm0-110v-68q33-14 67.5-21t72.5-7q26 0 51 4t49 10v64q-24-9-48.5-13.5T700-490q-38 0-73 9.5T560-454ZM260-320q47 0 91.5 10.5T440-278v-394q-41-24-87-36t-93-12q-36 0-71.5 7T120-692v396q35-12 69.5-18t70.5-6Zm260 42q44-21 88.5-31.5T700-320q36 0 70.5 6t69.5 18v-396q-33-14-68.5-21t-71.5-7q-47 0-93 12t-87 36v394Zm-40 118q-48-38-104-59t-116-21q-42 0-82.5 11T100-198q-21 11-40.5-1T40-234v-482q0-11 5.5-21T62-752q46-24 96-36t102-12q58 0 113.5 15T480-740q51-30 106.5-45T700-800q52 0 102 12t96 36q11 5 16.5 15t5.5 21v482q0 23-19.5 35t-40.5 1q-37-20-77.5-31T700-240q-60 0-116 21t-104 59ZM280-494Z">
                </path>
            </svg>
        </button><button type="button" onclick="mypop=window.open('teso-principal','','');mypop.focus();"
            class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
            <span>Nueva ventana</span>
            <svg class="fill-black group-hover:fill-white w-5 h-5" xmlns="http://www.w3.org/2000/svg"
                viewBox="0 -960 960 960">
                <path
                    d="M320-120v-80h80v-80H160q-33 0-56.5-23.5T80-360v-400q0-33 23.5-56.5T160-840h640q33 0 56.5 23.5T880-760v400q0 33-23.5 56.5T800-280H560v80h80v80H320ZM160-360h640v-400H160v400Zm0 0v-400 400Z">
                </path>
            </svg>
        </button><button type="button"
            onclick="mypop=window.open('/financiero/teso-trasladosvisualizar.php','','');mypop.focus();"
            class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
            <span class="group-hover:text-white">Duplicar pantalla</span>
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960">
                <path
                    d="M640-160v-360H160v360h480Zm80-200v-80h80v-360H320v200h-80v-200q0-33 23.5-56.5T320-880h480q33 0 56.5 23.5T880-800v360q0 33-23.5 56.5T800-360h-80ZM160-80q-33 0-56.5-23.5T80-160v-360q0-33 23.5-56.5T160-600h480q33 0 56.5 23.5T720-520v360q0 33-23.5 56.5T640-80H160Zm400-603ZM400-340Z">
                </path>
            </svg>
        </button><button type="button" onclick="pdf()"
            class="btn btn-danger btn-danger-hover d-flex justify-between align-items-center">
            <span>Exportar PDF</span>
            <svg xmlns="http://www.w3.org/2000/svg"
                viewBox="0 0 512 512"><!-- !Font Awesome Free 6.5.1 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license/free Copyright 2024 Fonticons, Inc. -->
                <path
                    d="M64 464l48 0 0 48-48 0c-35.3 0-64-28.7-64-64L0 64C0 28.7 28.7 0 64 0L229.5 0c17 0 33.3 6.7 45.3 18.7l90.5 90.5c12 12 18.7 28.3 18.7 45.3L384 304l-48 0 0-144-80 0c-17.7 0-32-14.3-32-32l0-80L64 48c-8.8 0-16 7.2-16 16l0 384c0 8.8 7.2 16 16 16zM176 352l32 0c30.9 0 56 25.1 56 56s-25.1 56-56 56l-16 0 0 32c0 8.8-7.2 16-16 16s-16-7.2-16-16l0-48 0-80c0-8.8 7.2-16 16-16zm32 80c13.3 0 24-10.7 24-24s-10.7-24-24-24l-16 0 0 48 16 0zm96-80l32 0c26.5 0 48 21.5 48 48l0 64c0 26.5-21.5 48-48 48l-32 0c-8.8 0-16-7.2-16-16l0-128c0-8.8 7.2-16 16-16zm32 128c8.8 0 16-7.2 16-16l0-64c0-8.8-7.2-16-16-16l-16 0 0 96 16 0zm80-112c0-8.8 7.2-16 16-16l48 0c8.8 0 16 7.2 16 16s-7.2 16-16 16l-32 0 0 32 32 0c8.8 0 16 7.2 16 16s-7.2 16-16 16l-32 0 0 48c0 8.8-7.2 16-16 16s-16-7.2-16-16l0-64 0-64z">
                </path>
            </svg>
        </button><button type="button" onclick="iratras()"
            class="btn btn-success btn-success-hover d-flex justify-between align-items-center">
            <span>Atras</span>
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960">
                <path
                    d="M760-200v-160q0-50-35-85t-85-35H273l144 144-57 56-240-240 240-240 57 56-144 144h367q83 0 141.5 58.5T840-360v160h-80Z">
                </path>
            </svg>
        </button>
    </div>
    <div id="bgventanamodalm" class="bgventanamodalm">
        <div id="ventanamodalm" class="ventanamodalm">
            <IFRAME src="" name="ventanam" marginWidth=0 marginHeight=0 frameBorder=0 id="ventanam" frameSpacing=0
                style=" width:700px; height:130px; top:200; overflow:hidden;"></IFRAME>
        </div>
    </div>
    <?php
    if (!$_POST['oculto']) {
        $_POST['idcomp'] = $_GET['idr'];
        $_POST['scrtop'] = $_GET['scrtop'];
        $sqltt = "SELECT codigo FROM redglobal WHERE tipo = 'IN'";
        $restt = mysqli_query($linkbd, $sqltt);
        $rowtt = mysqli_fetch_row($restt);
        $unidadlocal = $rowtt[0];
        $sqlr = "SELECT fecha, concepto, origen, estado FROM tesotraslados_cab WHERE id_consignacion = '" . $_POST['idcomp'] . "'";
        $res = mysqli_query($linkbd, $sqlr);
        $row = mysqli_fetch_row($res);
        $_POST['fecha'] = $row[0];
        $_POST['concepto'] = $row[1];
        $_POST['dorigen'] = $row[2];
        switch ($row[3]) {
            case 'S':
                $_POST['estadoc'] = 'ACTIVO';
                $color = " style='background-color:#0CD02A ;color:#fff'";
                break;
            case 'C':
                $_POST['estadoc'] = 'COMPLETO';
                $color = " style='background-color:#00CCFF ; color:#fff'";
                break;
            case 'N':
                $_POST['estadoc'] = 'ANULADO';
                $color = " style='background-color:#aa0000 ; color:#fff'";
                break;
            case 'R':
                $_POST['estadoc'] = 'REVERSADO';
                $color = " style='background-color:#aa0000 ; color:#fff'";
                break;
        }
        $sqlb = "SELECT MIN(id_consignacion),MAX(id_consignacion) FROM tesotraslados_cab";
        $resb = mysqli_query($linkbd, $sqlb);
        $rowb = mysqli_fetch_array($resb);
        $_POST['maximo'] = $rowb[1];
        $_POST['minimo'] = $rowb[0];
        $sqlorigen = "SELECT base, nombre, usuario FROM redglobal WHERE codigo = '$row[2]'";
        $resorigen = mysqli_query($linkbd, $sqlorigen);
        $roworigen = mysqli_fetch_row($resorigen);
        $_POST['baseorigen'] = $roworigen[0];
        $_POST['norigen'] = $roworigen[1];
        $_POST['userorigen'] = $roworigen[2];
        $linkmulti = conectar_Multi($_POST['baseorigen'], $_POST['userorigen']);
        $linkmulti->set_charset("utf8");
        $sqlr = "SELECT tipo_traslado, destino_ext, ntransaccion, cco, ncuentaban1, tercero1, ccd, ncuentaban2, tercero2, valor FROM tesotraslados WHERE id_trasladocab = '" . $_POST['idcomp'] . "'";
        $res = mysqli_query($linkbd, $sqlr);
        while ($row = mysqli_fetch_row($res)) {
            $_POST['dtipotraslado'][] = $row[0];
            $_POST['ddestino'][] = $row[1];
            $sqldest = "SELECT nombre, base FROM redglobal WHERE codigo = '$row[1]'";
            $resdest = mysqli_query($linkbd, $sqldest);
            $rowdest = mysqli_fetch_row($resdest);
            $_POST['dndestino'][] = $rowdest[0];
            $_POST['dbase'][] = $rowdest[1];
            $_POST['dconsig'][] = $row[2];
            $_POST['dccs'][] = $row[3];
            $sqlcc1 = "SELECT nombre FROM centrocosto WHERE estado = 'S' AND id_cc = '$row[3]'";
            $rescc1 = mysqli_query($linkmulti, $sqlcc1);
            $rowcc1 = mysqli_fetch_row($rescc1);
            $_POST['dccnombre'][] = $rowcc1[0];
            $_POST['dcbs'][] = $row[4];
            $sqlcuen1 = "SELECT razonsocial FROM terceros AS T2 WHERE T2.cedulanit = '$row[5]'";
            $rescuen1 = mysqli_query($linkmulti, $sqlcuen1);
            $rowcuen1 = mysqli_fetch_row($rescuen1);
            $_POST['dnbancos'][] = $rowcuen1[0];
            $_POST['dccs2'][] = $row[6];
            $sqlcc2 = "SELECT nombre FROM centrocosto WHERE estado = 'S' AND id_cc = '$row[6]'";
            if ($row[1] != '') {
                $rescc2 = mysqli_query($linkmulti, $sqlcc2);
            } else {
                $rescc2 = mysqli_query($linkbd, $sqlcc2);
            }
            $rowcc2 = mysqli_fetch_row($rescc2);
            $_POST['dccnombre2'][] = $rowcc2[0];
            $_POST['dcbs2'][] = $row[7];
            $sqlcuen2 = "SELECT razonsocial FROM terceros AS T2 WHERE T2.cedulanit = '$row[8]'";
            if ($row[1] != '') {
                $rescuen2 = mysqli_query($linkmulti, $sqlcuen2);
            } else {
                $rescuen2 = mysqli_query($linkbd, $sqlcuen2);
            }
            $rowcuen2 = mysqli_fetch_row($rescuen2);
            $_POST['dnbancos2'][] = $rowcuen2[0];
            $_POST['dvalores'][] = $row[9];
        }
        if ($_POST['dorigen'] != '') {
            if ($unidadlocal == $_POST['dorigen']) {
                $imagentipo = "<img src='imagenes/salida01.gif' style='height:30px;'>";
            } else {
                $imagentipo = "<img src='imagenes/entrada01.gif' style='height:30px;'>";
            }
        } else {
            $imagentipo = '';
        }
    }
    preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST['fecha'], $fecha);
    $_POST['vigencia'] = $fecha[3];
    $linkmulti = conectar_Multi($_POST['baseorigen'], $_POST['userorigen']);
    $linkmulti->set_charset("utf8");
    ?>
    <form name="form2" method="post" action="">
        <table class="inicio ancho">
            <tr>
                <td class="titulos" colspan="6">.: Agregar Traslados</td>
                <td class="cerrar" style="width:7%" onClick="location.href='teso-principal.php'">Cerrar</td>
            </tr>
            <tr>
                <td class="tamano01" style="width:2.5cm;">Numero Comp:</td>
                <td style="width:12%"><img src="imagenes/back.png" onClick="atrasc()" class="icobut"
                        title="Anterior" />&nbsp;<input type="text" name="idcomp" id="idcomp"
                        value="<?php echo $_POST['idcomp'] ?>" style="width:60%;" readonly>&nbsp;<img
                        src="imagenes/next.png" onClick="adelante()" class="icobut" title="Sigiente" /></td>
                <input type="hidden" name="cuentatraslado" value="<?php echo $_POST['cuentatraslado'] ?>">
                <td class="tamano01" style="width:3cm;">Fecha:</td>
                <td style="width:12%"><input type="text" id="fecha" name="fecha" value="<?php echo $_POST['fecha'] ?>" m
                        style="width:98%;" readonly></td>
                <td style="width:12%">
                    <input name="estadoc" type="text" id="estadoc" value="<?php echo $_POST['estadoc'] ?>" <?php echo $color; ?> readonly>
                </td>
                <td><?php echo $imagentipo; ?></td>
            </tr>
            <tr>
                <td class="tamano01">Concepto Traslado:</td>
                <td colspan="5"><input type="text" name="concepto" value="<?php echo $_POST['concepto'] ?>"
                        onKeyUp="return tabular(event,this)" style="width:100%;height:30px;" readonly></td>
            </tr>
        </table>
        <input type="hidden" name="agregadet" value="0">
        <input type="hidden" name="oculto" value="1">
        <input type="hidden" name="vigencia" value="<?php echo $_POST['vigencia']; ?>">
        <input type='hidden' name='dorigen' value="<?php echo $_POST['dorigen']; ?>">
        <input type='hidden' name='norigen' value="<?php echo $_POST['norigen']; ?>">
        <input type='hidden' name='baseorigen' value="<?php echo $_POST['baseorigen']; ?>">
        <input type='hidden' name='userorigen' value="<?php echo $_POST['userorigen']; ?>">
        <input type="hidden" name="maximo" id="maximo" value="<?php echo $_POST['maximo'] ?>" />
        <input type="hidden" name="minimo" id="minimo" value="<?php echo $_POST['minimo'] ?>" />
        <input type="hidden" name="scrtop" id="scrtop" value="<?php echo $_POST['scrtop'] ?>" />

        <script>
            document.form2.valor.focus();
            document.form2.valor.select();
        </script>
        <div class="subpantalla">
            <table class="inicio">
                <tr>
                    <td colspan="9" class="titulos">Detalle Traslados</td>
                </tr>
                <tr class="titulos2">
                    <td>Tipo</td>
                    <td>No Transacci&oacute;n</td>
                    <td>Origen</td>
                    <td>CC-Origen</td>
                    <td>Cuenta Bancaria Origen</td>
                    <td>Destino</td>
                    <td>CC-Destino</td>
                    <td>Cuenta Bancaria Destino</td>
                    <td>Valor</td>
                </tr>
                <?php
                $_POST['totalc'] = 0;
                $iter = 'saludo1a';
                $iter2 = 'saludo2';
                for ($x = 0; $x < count($_POST['dtipotraslado']); $x++) {
                    switch ($_POST['dtipotraslado'][$x]) {
                        case "INT":
                            $tipotraslado = 'Interno';
                            $nomorigen = $_POST['dorigen'] . " - " . $_POST['norigen'];
                            $nomdestino = $_POST['dorigen'] . " - " . $_POST['norigen'];
                            ;
                            break;
                        case "EXT":
                            $tipotraslado = 'Externo';
                            $nomorigen = $_POST['dorigen'] . " - " . $_POST['norigen'];
                            $nomdestino = $_POST['ddestino'][$x] . " - " . $_POST['dndestino'][$x];
                            break;
                        case "sdf":
                            $tipotraslado = 'No Definido';
                            $nomorigen = '--';
                            $nomdestino = '--';
                    }
                    echo "
								<input type='hidden' name='dnbancos[]' value='" . $_POST['dnbancos'][$x] . "'>
								<input type='hidden' name='dconsig[]' value='" . $_POST['dconsig'][$x] . "'>
								<input type='hidden' name='dccs[]' value='" . $_POST['dccs'][$x] . "'>
								<input type='hidden' name='dccnombre[]' value='" . $_POST['dccnombre'][$x] . "'>
								<input type='hidden' name='dcbs[]' value='" . $_POST['dcbs'][$x] . "'>
								<input type='hidden' name='dbancos[]' value='" . $_POST['dbancos'][$x] . "'>
								<input type='hidden' name='dcts[]' value='" . $_POST['dcts'][$x] . "'>
								<input type='hidden' name='dccs2[]' value='" . $_POST['dccs2'][$x] . "'>
								<input type='hidden' name='dccnombre2[]' value='" . $_POST['dccnombre2'][$x] . "'>
								<input type='hidden' name='dcts2[]' value='" . $_POST['dcts2'][$x] . "'>
								<input type='hidden' name='dnbancos2[]' value='" . $_POST['dnbancos2'][$x] . "'>
								<input type='hidden' name='dbancos2[]' value='" . $_POST['dbancos2'][$x] . "'>
								<input type='hidden' name='dcbs2[]' value='" . $_POST['dcbs2'][$x] . "'>
								<input type='hidden' name='dvalores[]' value='" . $_POST['dvalores'][$x] . "'>
								<input type='hidden' name='dtipotraslado[]' value='" . $_POST['dtipotraslado'][$x] . "'>
								<input type='hidden' name='ddestino[]' value='" . $_POST['ddestino'][$x] . "'>
								<input type='hidden' name='dndestino[]' value='" . $_POST['dndestino'][$x] . "'>
								<input type='hidden' name='dbase[]' value='" . $_POST['dbase'][$x] . "'>
								<input type='hidden' name='dorigen[]' value='" . $_POST['dorigen'][$x] . "'>
							<tr class='$iter'>
								<td>$tipotraslado</td>
								<td>" . $_POST['dconsig'][$x] . "</td>
								<td>$nomorigen</td>
								<td>" . $_POST['dccs'][$x] . " - " . $_POST['dccnombre'][$x] . "</td>
								<td>" . $_POST['dcbs'][$x] . " - " . $_POST['dnbancos'][$x] . "</td>
								<td>$nomdestino</td>
								<td>" . $_POST['dccs2'][$x] . " - " . $_POST['dccnombre2'][$x] . "</td>
								<td>" . $_POST['dcbs2'][$x] . " - " . $_POST['dnbancos2'][$x] . "</td>
								<td style='text-align:right;'>$ " . number_format($_POST['dvalores'][$x], 0) . "&nbsp;</td>
							</tr>";
                    $_POST['totalc'] = $_POST['totalc'] + $_POST['dvalores'][$x];
                    $_POST['totalcf'] = number_format($_POST['totalc'], 2, ".", ",");
                    $aux = $iter;
                    $iter = $iter2;
                    $iter2 = $aux;
                }
                $resultado = convertir($_POST['totalc']);
                $_POST['letras'] = $resultado . " Pesos";
                echo "
						<input type='hidden' name='totalcf' type='text' value='" . $_POST['totalcf'] . "'>
						<input type='hidden' name='totalc' value='" . $_POST['totalc'] . "'>
						<input type='hidden' name='letras' value='" . $_POST['letras'] . "'>
						<tr>
							<td colspan='6'></td>
							<td class='saludo2'>Total</td>
							<td class='saludo2'>" . $_POST['totalcf'] . "</td>
						</tr>
						<tr><td colspan='5'>Son: " . $_POST['letras'] . "</td></tr>";
                ?>
            </table>
        </div>
    </form>
</body>

</html>
