<?php
	require_once 'PHPExcel/Classes/PHPExcel.php';
	require "comun.inc";
	require "funciones.inc";
    ini_set('max_execution_time',99999999);
	session_start();
	$linkbd=conectar_v7();
	$objPHPExcel = new PHPExcel();
	//----Propiedades----
	$objPHPExcel->getProperties()
		->setCreator("IDEAL10")
		->setLastModifiedBy("IDEAL10")
		->setTitle("Balance general")
		->setSubject("balance")
		->setDescription("cont")
		->setKeywords("cont")
		->setCategory("Contabilidad");
	$objPHPExcel->getActiveSheet()->mergeCells('A1:D1');
	$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A1', 'Balance general');
	$objFont=$objPHPExcel->getActiveSheet()->getStyle('A1')->getFont();
	$objFont->setName('Courier New');
	$objFont->setSize(15);
	$objFont->setBold(true);
	$objFont->setUnderline(PHPExcel_Style_Font::UNDERLINE_SINGLE);
	$objFont->getColor()->setARGB( PHPExcel_Style_Color::COLOR_BLACK);
	$objAlign=$objPHPExcel->getActiveSheet()->getStyle('A1')->getAlignment(); 
	$objAlign->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
	$objAlign->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);

    $objPHPExcel->getActiveSheet()->mergeCells('A2:D2');
	$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A2', 'NIT.');
	$objFont=$objPHPExcel->getActiveSheet()->getStyle('A2')->getFont();
	$objFont->setName('Courier New');
	$objFont->setSize(12);
	$objFont->setBold(false);
	$objAlign=$objPHPExcel->getActiveSheet()->getStyle('A2')->getAlignment(); 
	$objAlign->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
	$objAlign->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);


	$objPHPExcel-> getActiveSheet ()
		-> getStyle ("A3:D3")
		-> getFill ()
		-> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
		-> getStartColor ()
		-> setRGB ('A6E5F3');
	$objPHPExcel-> getActiveSheet ()
		-> getStyle ("A1")	
		-> getFill ()
		-> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
		-> getStartColor ()
		-> setRGB ('A6E5F3');
	$borders = array(
		'borders' => array(
			'allborders' => array(
				'style' => PHPExcel_Style_Border::BORDER_THIN,
				'color' => array('argb' => 'FF000000'),
			)
		),
	);
	$objPHPExcel->getActiveSheet()->getStyle('A7:D7')->applyFromArray($borders);
	$objPHPExcel->setActiveSheetIndex(0)
        ->setCellValue('A7', 'Código de recaudo')
        ->setCellValue('B7', 'Fecha')
        ->setCellValue('C7', 'Codigo de usuario')
        ->setCellValue('D7', 'Valor de ingreso');     
	$i=3;
    
	for($ii=0;$ii<count ($_POST['consecutivo']);$ii++)
	{
		$objPHPExcel->setActiveSheetIndex(0)
		->setCellValueExplicit ("A$i", $_POST['consecutivo'][$ii], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
		->setCellValueExplicit ("B$i", $_POST['fecha'][$ii], PHPExcel_Cell_DataType :: TYPE_STRING)
		->setCellValueExplicit ("C$i", $_POST['codUsuario'][$ii], PHPExcel_Cell_DataType :: TYPE_STRING)
		->setCellValueExplicit ("D$i", $_POST['valorIngreso'][$ii], PHPExcel_Cell_DataType :: TYPE_NUMERIC);

        $objPHPExcel->getActiveSheet()->getStyle("A$i:D$i")->applyFromArray($borders);

        $i++;
	}

	$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);

	$objPHPExcel->getActiveSheet()->setTitle('SP');
	header('Content-Type: application/vnd.ms-excel');
	header('Content-Disposition: attachment;filename="ListadoAnticipos.xlsx"');
	header('Cache-Control: max-age=0');
	$objWriter=PHPExcel_IOFactory::createWriter($objPHPExcel,'Excel2007');
	$objWriter->save('php://output');
	exit;
?>