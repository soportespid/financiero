<?php
	require_once 'PHPExcel/Classes/PHPExcel.php';
    include '/PHPExcel/Classes/PHPExcel/IOFactory.php';
	require "comun.inc";
	require "funciones.inc";
    require "funcionesSP.inc.php";
    ini_set('max_execution_time',99999999);
    header("Content-type: application/json");
	session_start();
	$linkbd=conectar_v7();

    $principal = [];
    $principal = json_decode($_GET['text'], true);
    $datos = [];

    foreach ($principal as $key => $princ) {
        
        array_push($datos, $princ);
    }

	$objPHPExcel = new PHPExcel();
	
	$objPHPExcel->getProperties()
		->setCreator("IDEAL10")
		->setLastModifiedBy("IDEAL10")
		->setTitle("LECTURA ACUEDUCTO Y ALCANTARILLADO")
		->setSubject("SP")
		->setDescription("SP")
		->setKeywords("SP")
		->setCategory("SERVICIOS PUBLICOS");

    $borders = array(
        'borders' => array(
            'allborders' => array(
                'style' => PHPExcel_Style_Border::BORDER_THIN,
                'color' => array('argb' => 'FF000000'),
            )
        ),
    );

	$objPHPExcel->getActiveSheet()->mergeCells('A1:H1');
	$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A1', 'REPORTE DE SUBSIDIOS');
	$objFont=$objPHPExcel->getActiveSheet()->getStyle('A1')->getFont();
	$objFont->setName('Courier New');
	$objFont->setSize(15);
	$objFont->setBold(true);
	$objFont->setUnderline(PHPExcel_Style_Font::UNDERLINE_SINGLE);
	$objFont->getColor()->setARGB( PHPExcel_Style_Color::COLOR_BLACK);
	$objAlign=$objPHPExcel->getActiveSheet()->getStyle('A1')->getAlignment(); 
	$objAlign->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
	$objAlign->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
	
	// $objPHPExcel->getActiveSheet()->getStyle('A2:H2')->applyFromArray($borders);
	// $objPHPExcel->setActiveSheetIndex(0)
    //     ->setCellValue('A2', 'Estrato')
    //     ->setCellValue('B2', 'Cantidad de usuarios')
    //     ->setCellValue('C2', 'Consumo m3')
    //     ->setCellValue('D2', 'Cargo fijo')
    //     ->setCellValue('E2', 'Consumo')
    //     ->setCellValue('F2', 'Subsidiado')
    //     ->setCellValue('G2', 'Contribución')
    //     ->setCellValue('H2', 'Saldo');     
	$i=2;
    $j=3;
    $cont = count($datos);
    
    // var_dump($principal[0][1]);

    for ($x=0; $x < $cont; $x++) { 
        
        $cont2 = count($datos[$x][1]);
        $objPHPExcel->getActiveSheet()->mergeCells("A$i:H$i");
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue("A$i", 'SERVICIO: '.$datos[$x][0]);
        $objFont=$objPHPExcel->getActiveSheet()->getStyle("A$i")->getFont();
        $objFont->setName('Courier New');
        $objFont->setBold(true);
        $objFont->setUnderline(PHPExcel_Style_Font::UNDERLINE_SINGLE);
        $objFont->getColor()->setARGB( PHPExcel_Style_Color::COLOR_BLACK);
        $objAlign=$objPHPExcel->getActiveSheet()->getStyle("A$i")->getAlignment(); 
        $objAlign->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objAlign->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        $objPHPExcel-> getActiveSheet ()
            -> getStyle ("A$i")	
            -> getFill ()
            -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
            -> getStartColor ()
            -> setRGB ('A6E5F3');
        $i++;

        $objPHPExcel->getActiveSheet()->getStyle("A$i:H$i")->applyFromArray($borders);
            $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue("A$i", 'Estrato')
                ->setCellValue("B$i", 'Cantidad de usuarios')
                ->setCellValue("C$i", 'Consumo m3')
                ->setCellValue("D$i", 'Cargo fijo')
                ->setCellValue("E$i", 'Consumo')
                ->setCellValue("F$i", 'Subsidiado')
                ->setCellValue("G$i", 'Contribución')
                ->setCellValue("H$i", 'Saldo');     

        $i++;

        for ($y=0; $y < $cont2; $y++) { 
            $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValueExplicit ("A$i", $datos[$x][1][$y][0], PHPExcel_Cell_DataType :: TYPE_STRING)
            ->setCellValueExplicit ("B$i", $datos[$x][1][$y][1], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
            ->setCellValueExplicit ("C$i", $datos[$x][1][$y][2], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
            ->setCellValueExplicit ("D$i", $datos[$x][1][$y][3], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
            ->setCellValueExplicit ("E$i", $datos[$x][1][$y][4], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
            ->setCellValueExplicit ("F$i", $datos[$x][1][$y][5], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
            ->setCellValueExplicit ("G$i", $datos[$x][1][$y][6], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
            ->setCellValueExplicit ("H$i", $datos[$x][1][$y][7], PHPExcel_Cell_DataType :: TYPE_NUMERIC);

            $i++;
        }
    }

	$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);

	$objPHPExcel->getActiveSheet()->setTitle('SP');
	//header('Content-Type: application/vnd.ms-excel');
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
	header('Content-Disposition: attachment;filename="reporteSubsidios.xlsx"');
	header('Cache-Control: max-age=0');
	$objWriter=PHPExcel_IOFactory::createWriter($objPHPExcel,'Excel2007');
	$objWriter->save('php://output');
	exit;
