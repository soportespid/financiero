<?php

    header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require 'comun.inc';
	require 'funciones.inc';
	session_start();
    if(empty($_SESSION)){
        header("location: index.php");
    }
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html lang=es>
	<head>
    <meta charset="utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1"/>
		<meta name="viewport" content="user-scalable=no">
		<title>:: IDEAL 10 - Gestión humana</title>
		<link href="favicon.ico" rel="shortcut icon"/>
		<link href="css/css2z.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
        <link href="css/tabs.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
        <link href="css/style.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" href="sweetalert2/dist/sweetalert2.min.css">
        <script type="text/javascript" src="css/programas.js"></script>
        <script src="sweetalert2/dist/sweetalert2.min.js"></script>
		<script src="vue/vue.min.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script src="css/calendario.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
	</head>
	<body>
		<header>
			<table>
				<tr><script>barra_imagenes("hum");</script><?php cuadro_titulos();?></tr>
			</table>
		</header>

        <main id="myapp" v-cloak>
            <input type="hidden" value = "2" ref="pageType">
            <div class="loading-container" v-show="isLoading" >
                <p class="text-loading"data-loading="IDEAL 10">IDEAL 10</p>
            </div>
            <nav>
                <table>
                    <tr><?php menu_desplegable("hum");?></tr>
                </table>
                <div class="bg-white group-btn p-1" id="newNavStyle">
                    <button type="button" @click="window.location.href='hum-contrato-novedad-crear'" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
                        <span>Nuevo</span>
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960"><path d="M440-440H200v-80h240v-240h80v240h240v80H520v240h-80v-240Z"></path></svg>
                    </button>
                    <button type="button" @click="save()" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
                        <span>Guardar</span>
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960">
                            <path d="M840-680v480q0 33-23.5 56.5T760-120H200q-33 0-56.5-23.5T120-200v-560q0-33 23.5-56.5T200-840h480l160 160Zm-80 34L646-760H200v560h560v-446ZM480-240q50 0 85-35t35-85q0-50-35-85t-85-35q-50 0-85 35t-35 85q0 50 35 85t85 35ZM240-560h360v-160H240v160Zm-40-86v446-560 114Z">
                            </path>
                        </svg>
                    </button>
                    <button type="button" @click="window.location.href='hum-contrato-novedad-buscar'" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
                        <span>Buscar</span>
                        <svg class="fill-black group-hover:fill-white w-5 h-5" xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960"><path d="M784-120 532-372q-30 24-69 38t-83 14q-109 0-184.5-75.5T120-580q0-109 75.5-184.5T380-840q109 0 184.5 75.5T640-580q0 44-14 83t-38 69l252 252-56 56ZM380-400q75 0 127.5-52.5T560-580q0-75-52.5-127.5T380-760q-75 0-127.5 52.5T200-580q0 75 52.5 127.5T380-400Z"></path></svg>
                    </button>
                    <button type="button" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center" @click="mypop=window.open('hum-principal.php','',''); mypop.focus();">
                        <span>Nueva ventana</span>
                        <svg class="fill-black group-hover:fill-white w-5 h-5" xmlns="http://www.w3.org/2000/svg"  viewBox="0 -960 960 960" ><path d="M320-120v-80h80v-80H160q-33 0-56.5-23.5T80-360v-400q0-33 23.5-56.5T160-840h640q33 0 56.5 23.5T880-760v400q0 33-23.5 56.5T800-280H560v80h80v80H320ZM160-360h640v-400H160v400Zm0 0v-400 400Z"/></svg>
                    </button>
                    <button type="button" @click="mypop=window.open('hum-contrato-novedad-crear','','');mypop.focus();" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
                        <span class="group-hover:text-white">Duplicar pantalla</span>
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960"><path d="M640-160v-360H160v360h480Zm80-200v-80h80v-360H320v200h-80v-200q0-33 23.5-56.5T320-880h480q33 0 56.5 23.5T880-800v360q0 33-23.5 56.5T800-360h-80ZM160-80q-33 0-56.5-23.5T80-160v-360q0-33 23.5-56.5T160-600h480q33 0 56.5 23.5T720-520v360q0 33-23.5 56.5T640-80H160Zm400-603ZM400-340Z"></path></svg>
                    </button>
                    <button type="button" @click="window.location.href='hum-contrato-novedad-buscar'" class="btn btn-success btn-success-hover d-flex justify-between align-items-center">
                        <span>Atras</span>
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960"><path d="M760-200v-160q0-50-35-85t-85-35H273l144 144-57 56-240-240 240-240 57 56-144 144h367q83 0 141.5 58.5T840-360v160h-80Z"></path></svg>
                    </button>
                </div>
            </nav>
            <section class="bg-white">
                <h2 class="titulos m-0">Novedad de contrato laboral</h2>
                <div class="d-flex w-75">
                    <div class="form-control">
                        <label class="form-label" for="">Código:</label>
                        <div class="d-flex">
                            <button type="button" class="btn btn-primary" @click="nextItem('prev')"><</button>
                            <input type="text"  style="text-align:center;" v-model="txtConsecutivo" @change="nextItem()">
                            <button type="button" class="btn btn-primary" @click="nextItem('next')">></button>
                        </div>
                    </div>
                    <div class="form-control">
                        <label class="form-label">Tipo de novedad:</label>
                        <select v-model="selectNovedad" disabled>
                            <option value="0">Seleccione</option>
                            <option value="1">Renovar</option>
                            <option value="2">Retirar</option>
                        </select>
                    </div>
                    <div class="form-control" v-if="selectNovedad != 0">
                        <label class="form-label">Contrato laboral:</label>
                        <input type="text" class="text-center" disabled v-model="objContrato.id">
                    </div>
                    <div class="form-control" v-if="selectNovedad == 2">
                        <label class="form-label">Motivo de retiro:</label>
                        <select v-model="selectRetiro" disabled>
                            <option value="0">Seleccione</option>
                            <option v-for="(data,index) in arrMotivos" :key="index" :value="data.id">{{ data.motivo}}</option>
                        </select>
                    </div>
                </div>
                <div v-if="selectNovedad == 1 && objContrato.id != ''">
                    <!--TABS-->
                    <div ref="rTabs" class="nav-tabs p-1">
                        <div class="nav-item active" @click="showTab(1)">Información general</div>
                        <div class="nav-item" @click="showTab(2)">Clausula de obligaciones</div>
                        <div class="nav-item" @click="showTab(3)">Otras cláusulas</div>
                        <div class="nav-item" @click="showTab(4)">Contrato</div>
                    </div>
                    <!--CONTENIDO TABS-->
                    <div ref="rTabsContent" class="nav-tabs-content">
                        <div class="nav-content active">
                            <div class="d-flex">
                                <div :class="[objCargo.requisitos.length > 0 ? 'w-50': 'w-100' ]" class="me-2">
                                    <h2 class="titulos m-0">Contrato renovado</h2>
                                    <div class="d-flex w-100">
                                        <div class="w-50 d-flex">
                                            <div class="form-control">
                                                <label class="form-label">Tipo de contrato:</label>
                                                <select v-model="selectContrato" disabled>
                                                    <option value="1">Indefinido</option>
                                                    <option value="2">Fijo</option>
                                                    <option value="3">Obra o labor</option>
                                                </select>
                                            </div>
                                            <div class="form-control">
                                                <label class="form-label">Periodo de pago:</label>
                                                <select v-model="selectPeriodo" disabled>
                                                    <option v-for="(data,index) in arrPeriodos" :value="data.codigo">{{data.nombre}}</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="w-50 d-flex">
                                            <div class="form-control">
                                                <label class="form-label">Fecha de ingreso:</label>
                                                <input type="date" v-model="txtFechaIngreso" disabled>
                                            </div>
                                            <div class="form-control" v-if="selectContrato == 3">
                                                <label class="form-label">Nombre de la obra o labor:</label>
                                                <input type="text" v-model="txtNombreLabor" disabled>
                                            </div>
                                            <div class="form-control" v-if="selectContrato == 2">
                                                <label class="form-label">Fecha de terminación:</label>
                                                <input type="date" v-model="txtFechaTerminacion" disabled>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="d-flex">
                                        <div class="form-control">
                                            <label class="form-label">Cuenta bancaria <span class="text-danger fw-bolder">*</span>:</label>
                                            <div class="d-flex">
                                                <select v-model="selectBancos" @change="changeCuenta()" disabled>
                                                    <option value="">Seleccione</option>
                                                    <option v-for="(data,index) in objTercero.cuentas" :key="index" :value="data.codigo">
                                                        {{ data.nombre }}
                                                    </option>
                                                </select>
                                                <input type="text" placeholder="Número de cuenta" v-model="txtCuentaBancaria" disabled>
                                            </div>
                                        </div>
                                        <div class="form-control">
                                            <label class="form-label">Tercero:</label>
                                            <div class="d-flex">
                                                <input type="text" class="w-25" :value="objTercero.codigo" disabled >
                                                <input type="text" :value="objTercero.nombre" disabled>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="w-100 d-flex">
                                        <div class="w-50">
                                            <div class="d-flex">
                                                <div class="form-control">
                                                    <label class="form-label">Cargo:</label>
                                                    <input type="text" :value="objCargo.nombre" disabled>
                                                </div>
                                                <div class="form-control w-25">
                                                    <label class="form-label">Escala</label>
                                                    <input type="number" :value="objCargo.escala" class="text-center" disabled>
                                                </div>

                                                <div class="form-control">
                                                    <label class="form-label">Salario</label>
                                                    <input type="text" :value="formatNum(objCargo.valor)" class="text-right" disabled>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="w-50 d-flex">
                                            <div class="form-control">
                                                <label class="form-label">Nivel salarial</label>
                                                <input type="text" :value="objCargo.nivel_salarial"  disabled>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="w-100 d-flex">
                                        <div class="d-flex w-50">
                                            <div class="form-control">
                                                <label class="form-label">Jornada laboral:</label>
                                                <select v-model="selectJornada" disabled>
                                                    <option value="1">Lunes a viernes</option>
                                                    <option value="2">Lunes a sábado</option>
                                                    <option value="3">Lunes a domingo</option>
                                                </select>
                                            </div>
                                            <div class="form-control">
                                                <label class="form-label">Horario:</label>
                                                <div class="d-flex align-items-center justify-between">
                                                    <input type="time" disabled v-model="txtJornadaLvInicio">
                                                    <span class="me-1 ms-1">-</span>
                                                    <input type="time" disabled v-model="txtJornadaLvFinal">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="w-50 d-flex">
                                            <div class="form-control" v-if="selectJornada == 2 || selectJornada == 3">
                                                <label class="form-label">Horario sábado:</label>
                                                <div class="d-flex align-items-center justify-between">
                                                    <input type="time" disabled v-model="txtJornadaLsInicio">
                                                    <span class="me-1 ms-1">-</span>
                                                    <input type="time" disabled v-model="txtJornadaLsFinal">
                                                </div>
                                            </div>
                                            <div class="form-control" v-if="selectJornada == 3">
                                                <label class="form-label">Horario domingo:</label>
                                                <div class="d-flex align-items-center justify-between">
                                                    <input type="time" disabled v-model="txtJornadaLdInicio">
                                                    <span class="me-1 ms-1">-</span>
                                                    <input type="time" disabled v-model="txtJornadaLdFinal">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="w-50">
                                    <h2 class="titulos m-0">Cumplimiento de requisitos del cargo</h2>
                                    <div v-if="objCargo.requisitos.length > 0">
                                        <div class="overflow-auto d-flex mt-2" style="height:50vh">
                                            <div v-for="(area,i) in objCargo.requisitos" :key="i" class="me-4">
                                                <div class="form-control" v-if="area.is_checked" >
                                                    <div class="d-flex align-items-center justify-between" >
                                                        <label class="me-2 fw-bold" :for="'labelCheckName' + i">{{area.nombre}}</label>
                                                    </div>
                                                </div>
                                                <hr>
                                                <div v-if="area.is_checked" class="ms-4" >
                                                    <div v-for="(tipos,j) in area.tipos" :key="j">
                                                        <div class="form-control" v-if="tipos.is_checked" >
                                                            <div class="d-flex align-items-center justify-between" >
                                                                <label class="me-2 text-black-light fw-bold" :for="'labelCheckName' +i+j">{{tipos.nombre}}</label>
                                                            </div>
                                                        </div>
                                                        <div v-if="tipos.is_checked" class="ms-4" >
                                                            <hr>
                                                            <div class="form-control" v-for="(requisitos,k) in tipos.requisitos" :key="k">
                                                                <div class="d-flex align-items-center justify-between" v-if="requisitos.is_checked" >
                                                                    <label class="me-2" :for="'labelCheckName' +i+j+k">{{requisitos.nombre}}</label>
                                                                    <label :for="'labelCheckName' +i+j+k" class="form-switch">
                                                                        <input type="checkbox" :id="'labelCheckName'+i+j+k" disabled :checked="requisitos.checked">
                                                                        <span></span>
                                                                    </label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="nav-content">
                            <div class="d-flex me-1 ms-1">
                                <div class="w-50 me-2">
                                    <h2 class="titulos m-0">Obligaciones empleador</h2>
                                    <div class="overflow-auto" style="height:50vh">
                                        <div class="form-control m-0 mb-1 mt-1 flex-row" v-for="(data,index) in arrObligaciones" :key="index" v-if="data.type == 1">
                                            <input type="text" v-model="data.value" disabled>
                                        </div>
                                    </div>
                                </div>
                                <div class="w-50">
                                    <h2 class="titulos m-0">Obligaciones empleado</h2>
                                    <div class="overflow-auto" style="height:50vh">
                                        <div class="form-control m-0 mb-1 mt-1 flex-row" v-for="(data,index) in arrObligaciones" :key="index" v-if="data.type == 2">
                                            <input type="text" v-model="data.value" disabled>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="nav-content">
                            <h2 class="titulos m-0">Claúsulas creadas</h2>
                            <div class="overflow-auto" style="height:50vh">
                                <div v-for="(data,index) in arrClausulas" :key="index">
                                    <div class="d-flex">
                                        <div class="form-control">
                                            <label class="form-label fw-bold">Artículo.</label>
                                            <textarea  v-model="data.clausula" disabled></textarea>
                                        </div>
                                        <div class="form-control">
                                            <label class="form-label fw-bold">Parágrafo</label>
                                            <div class="d-flex">
                                                <textarea  v-model="data.paragrafo" disabled></textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="nav-content">
                            <div class="d-flex align-items-center flex-column">
                                <p class="fw-bold text-center mb-3">Descargue el contrato generado y carguelo firmado.</p>
                                <button type="button" @click="exportData()" class="btn btn-primary text-center">Descargar contrato</button>
                                <div class="form-control w-50">
                                    <label class="form-label">Contrato firmado:</label>
                                    <a target="_blank" :href="txtRutaAnexo != ''? 'informacion/proyectos/temp/'+txtRutaAnexo : ''" class="mb-3 fw-bolder" :class="[txtRutaAnexo !='' ? 'text-success' : 'text-danger']" >
                                        {{ txtRutaAnexo != "" ? "Ver archivo actual" : "No hay archivo"}}
                                    </a>
                                    <input type="file" @change="uploadFile(this)" accept="application/pdf">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </main>
		<script src="node_modules/read-excel-file/bundle/read-excel-file.min.js"></script>
		<script src="node_modules/xlsx/dist/xlsx.full.min.js"></script>
        <script src="Librerias/vue/vue.min.js"></script>
		<script src="Librerias/vue/axios.min.js"></script>
		<script type="module" src="gestion_humana/contratos/js/functions_contrato_novedad.js?<?= date('d_m_Y_h_i_s');?>"></script>
	</body>
</html>
