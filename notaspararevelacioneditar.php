<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require "comun.inc";
	require "funciones.inc";
	session_start();
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html lang="es">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1">
		<meta name="viewport" content="user-scalable=no">
		<link href="favicon.ico" rel="shortcut icon"/>
		<link href="css/css2.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<script type="text/javascript" src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script> 
			function guardar()
			{
				parent.document.form2.notaf.value =document.form2.texta.value;
				parent.despliegamodal2("hidden");
				parent.document.form2.submit();
			} 
			function editar(){
				document.form2.edita.value=2;
				// alert(document.form2.texta.value);
				document.form2.nota1.value=document.form2.texta.value;
				document.form2.submit();
			}
		</script> 
		<?php titlepag();?>
	</head>
	<body >
		<form action="" method="post" name="form2">
			<?php
				if($_POST['edita']!=2){
					$_POST['nota1']=$_GET['nota'];
					$_POST['modulo']=$_GET['modulo'];
					$_POST['doc']=$_GET['doc'];
					$_POST['tdoc']=$_GET['tdoc'];
					$_POST['valor']=$_GET['valor'];
				}
				if($_POST['edita']==2){
					$sqlr="delete from teso_notasrevelaciones where modulo='$_POST[modulo]' and numero_documento='$_POST[doc]' and tipo_documento='$_POST[tdoc]'";
					mysqli_query($linkbd,$sqlr);
					$date=getdate();
					$h=$date["hours"];
					$min=$date["minutes"];
					$hora=$h.":".$min;
					$sqlr="insert into teso_notasrevelaciones(hora,fecha,usuario,modulo,tipo_documento,numero_documento,valor_total_transaccion,nota) values('$hora','$date','".$_SESSION["usuario"]."','$_POST[modulo]','$_POST[tdoc]','$_POST[doc]','$_POST[valor]','$_POST[nota1]')";
					mysqli_query($linkbd,$sqlr);
				}
			?>
			<table  class="inicio" style="width:99.4%;">
				<tr >
					<td class="titulos" colspan="2">:. Notas para Revelaciones</td>
					<td style="width:7%" class="cerrar" ><a onClick="parent.despliegamodal2('hidden');" style="cursor:pointer;">Cerrar</a></td>
				</tr>
			</table> 
			<div class="subpantalla" style="height:86%; width:99%; overflow-x:hidden;">
				<table>
					<tr >
						<td class="saludo1" colspan="2">:. Nota:</td>
					</tr>
					<tr>
						<td colspan="2">
							<input type="hidden" name="nota1" id="nota1" value="<?php echo $_POST['nota1']?>">
							<input type="hidden" name="modulo" id="modulo" value="<?php echo $_POST['modulo']?>">
							<input type="hidden" name="doc" id="doc" value="<?php echo $_POST['doc']?>">
							<input type="hidden" name="tdoc" id="tdoc" value="<?php echo $_POST['tdoc']?>">
							<input type="hidden" name="valor" id="valor" value="<?php echo $_POST['valor']?>">
							<input type="hidden" name="edita" id="edita" value="1">
							<?php echo "<textarea name='texta' id='texta' rows='24' cols='120' >$_POST[nota1]</textarea>";?>
						</td>
					</tr>
					<tr>
						<td><input type="button" value="Editar" onClick="editar()"><input type="button" value="Guardar" onClick="guardar()" <?php if($_POST['edita']!=2){echo "disabled";} ?>></td>
					</tr>
					
				</table>
			</div>
		<input type="hidden" name="numtop" id="numtop" value="<?php echo $_POST['numtop'];?>" />
</form>
</body>
</html>
