<?php
	require_once("tcpdf/tcpdf_include.php");    
	require('comun.inc');
	require('funciones.inc');
	session_start();	
	date_default_timezone_set("America/Bogota");
    class MYPDF extends TCPDF 
	{
        public function Header() 
		{
            $linkbd=conectar_v7();
			$linkbd -> set_charset("utf8");
			$sqlr = "SELECT * FROM configbasica WHERE estado='S'";
			$res = mysqli_query($linkbd,$sqlr);
			while($row=mysqli_fetch_row($res)){
				$nit=$row[0];
				$rs=$row[1];
			}
			$numnota = $_GET['idnota'];
			$sql = "SELECT estado, tipo_mov, fecha, concepto FROM tesonotasbancarias_cab WHERE id_comp = '$numnota'";
			$res = mysqli_query($linkbd,$sql);
			$row = mysqli_fetch_row($res);
			$fechanota = date('d/m/Y',strtotime($row[2]));
			$vigencia = date('Y',strtotime($row[2]));
			
			$estado = $row[0];
			if ($estado == 'R'){
				$this->Image('imagenes/reversado02.png',140,25,50,15);
			}
			$this->Image('imagenes/escudo.jpg', 12, 12, 25, 25, 'JPG', '', 'T', true, 300, '', false, false, 0, false, false, false);// Logo
			$this->SetFont('helvetica','B',8);
			$this->SetY(10);
            $this->RoundedRect(10, 10, 190, 29, 1,'1001' );
			$this->Cell(30,29,'','R',0,'L'); 
			$this->SetY(10);
			$this->SetX(40);
			$this->SetFont('helvetica','B',12);
			$this->Cell(127,13,"$rs",0,0,'C'); 
			$this->SetY(16);
			$this->SetX(40);
			$this->SetFont('helvetica','B',11);
			$this->Cell(127,10,"$nit",0,0,'C');
			//*****************************************************************************************************************************
			$this->SetFont('helvetica','B',14);
			$this->SetY(10);
			$this->Cell(50.1);
			$this->Cell(149,31,'',0,1,'C'); 
			$this->SetY(8);
			$this->Cell(50.1);
			//************************************
			$this->SetFont('helvetica','B',10);
			$this->SetY(10);
			$this->Cell(157);
			$this->Cell(33,15,'','LB',0,'L');
			$this->SetY(10);
			$this->Cell(157.5);
			$this->Cell(33,5,'Número: '.$numnota,0,0,'L');
			$this->SetY(15);
			$this->Cell(157.5);
			$this->Cell(33,5,'Fecha: '.$fechanota,0,0,'L');
			$this->SetY(20);
			$this->Cell(157.5);
			$this->Cell(33,5,'Vigencia: '.$vigencia,0,0,'L');
            $this->SetY(25);
            $this->SetX(40);
        	$this->SetFont('helvetica','B',12);
            $this->Cell(127,14,'NOTAS BANCARIAS','T',1,'C'); 
			$this->SetFont('helvetica','B',10);
			$linkbd = conectar_v7();
			$linkbd -> set_charset("utf8");
 
			$tipomov = $row[1];
			$detallegreso = $row[3];
			$lineas = $this->getNumLines($detallegreso,160);
			$lineas2 = $lineas * 5;
			$this->Cell(30,$lineas2,'CONCEPTO:','LB',0,'C');
			$this->SetFont('helvetica','',10);
			$this->MultiCell(160,$lineas2,$detallegreso,'RB','L',0,1,'','');

		}
        public function Footer() 
		{
			$linkbd = conectar_v7();
			$linkbd -> set_charset("utf8");
			$sqlr="SELECT direccion,telefono,web,email FROM configbasica WHERE estado='S'";
			$resp=mysqli_query($linkbd, $sqlr);
			$user = $_SESSION['nickusu'];	
			$fecha = date("Y-m-d H:i:s");
			$ip = $_SERVER['REMOTE_ADDR'];
			$useri = $_POST['user'];
			while($row=mysqli_fetch_row($resp))
			{
				$direcc=strtoupper($row[0]);
				$telefonos=$row[1];
				$dirweb=strtoupper($row[3]);
				$coemail=strtoupper($row[2]);
			}
			if($direcc!=''){$vardirec="Dirección: $direcc, ";}
			else {$vardirec="";}
			if($telefonos!=''){$vartelef="Telefonos: $telefonos";}
			else{$vartelef="";}
			if($dirweb!=''){$varemail="Email: $dirweb, ";}
			else {$varemail="";}
			if($coemail!=''){$varpagiw="Pagina Web: $coemail";}
			else{$varpagiw="";}
			
			$txt = <<<EOD
			$vardirec $vartelef
			$varemail $varpagiw
			EOD;
			$this->SetFont('helvetica', 'I', 5);
			$this->Cell(190,7,'','T',0,'T');
			$this->ln(1);
			$this->Write(0, $txt , '', 0, 'C', true, 0, false, false, 0);
			//$this->Cell(12, 7, 'Hecho por: '.$useri, 0, false, 'C', 0, '', 0, false, 'T', 'M');
			$this->Cell(15, 7, 'Impreso por: '.$user, 0, false, 'C', 0, '', 0, false, 'T', 'M');
			$this->Cell(57, 7, 'IP: '.$ip, 0, false, 'C', 0, '', 0, false, 'T', 'M');
			$this->Cell(50, 7, 'Fecha: '.$fecha, 0, false, 'C', 0, '', 0, false, 'T', 'M');
			$this->Cell(54, 7, 'IDEAL.10 S.A.S', 0, false, 'C', 0, '', 0, false, 'T', 'M');
			$this->Cell(25, 7, 'Pagina '.$this->getAliasNumPage().'/'.$this->getAliasNbPages(), 0, false, 'C', 0, '', 0, false, 'T', 'M');

        }
    }
    $pdf = new MYPDF('P','mm','Letter', true, 'utf8', false);
	$pdf->SetDocInfoUnicode (true); 
	// set document information
	$pdf->SetCreator(PDF_CREATOR);
	$pdf->SetAuthor('IDEAL10');
	$pdf->SetTitle('NOTAS BANCARIAS');
	$pdf->SetSubject('NOTAS BANCARIAS');
	$pdf->SetKeywords('NOTAS BANCARIAS');
    $pdf->SetMargins(10, 45, 10);// set margins
    $pdf->SetHeaderMargin(45);// set margins
    $pdf->SetFooterMargin(17);// set margins
	$pdf->SetAutoPageBreak(TRUE, 20);// set auto page breaks
	// set some language-dependent strings (optional)
	if (@file_exists(dirname(__FILE__).'/lang/spa.php')) 
	{
		require_once(dirname(__FILE__).'/lang/spa.php');
		$pdf->setLanguageArray($l);
	}
	$pdf->AddPage();	
	$pdf ->ln(5);
	$yy=$pdf->GetY();	
	$pdf->SetY($yy);
	$yy=$pdf->GetY();	
	$pdf->SetFillColor(222,222,222);
	$pdf->SetFont('helvetica','B',9);
	$pdf->Cell(0.1);
	$pdf->Cell(20,5,'CC',0,0,'C',1); 
	$pdf->SetY($yy);
	$pdf->Cell(21);
	$pdf->Cell(35,5,'DOC BANC.',0,0,'C',1);
	$pdf->SetY($yy);
	$pdf->Cell(57);
	$pdf->Cell(40,5,'BANCO',0,0,'C',1);
	$pdf->SetY($yy);
	$pdf->SetFont('helvetica','B',7);
	$pdf->Cell(98);
	$pdf->Cell(63,5,'GASTO BANCARIO',0,0,'C',1);
	$pdf->SetY($yy);
	$pdf->Cell(162);
	$pdf->MultiCell(28,5,'VALOR',0,'C',1,1,'','',true,'',false,true,0,'M',true);
	$pdf->SetFont('helvetica','',8);
	$pdf->ln(1);
	
	
	$numnota = $_GET['idnota'];
	$linkbd = conectar_v7();
	$sqlr = "SELECT * FROM tesonotasbancarias_det WHERE id_notabancab = '$numnota'";
	$res = mysqli_query($linkbd,$sqlr);
	while($row = mysqli_fetch_assoc($res)){	
		$sqlgb = "SELECT nombre, tipo FROM tesogastosbancarios WHERE codigo = '".$row['gastoban']."'";
		$resgb = mysqli_query($linkbd, $sqlgb);
		$rowgb = mysqli_fetch_row($resgb);
		if($vv>=170)
        { 
            $pdf->AddPage();
            $vv=$pdf->gety();
        }
		if ($con%2==0)
		{
			$pdf->SetFillColor(245,245,245);
		}
		else
		{
			$pdf->SetFillColor(255,255,255);
		}	
		$pdf->MultiCell(21,4,''.$row['cc'],0,'C',true,0,'','',true,0,false,true,0,'M',true);
		$pdf->MultiCell(36,4,''.$row['docban'],0,'C',true,0,'','',true,0,false,true,0,'M',true);
		$pdf->MultiCell(41,4,$row['ncuentaban'],0,'C',true,0,'','',true,0,false,true,0,'M',true);
		$pdf->MultiCell(64,4,$rowgb[1]."-".$row['gastoban']."-".$rowgb[0],0,'L',true,0,'','',true,0,false,true,0,'M',true);
		$pdf->MultiCell(28,4,'$'.number_format($row['valor'],2),0,'R',true,1,'','',true,0,false,true,0,'M',true);
		$con=$con+1;
	}
	$vigusu = vigencia_usuarios($_SESSION['cedulausu']);

	$sqlr = "SELECT id_cargo, id_comprobante FROM pptofirmas WHERE id_comprobante = '20' AND vigencia = '".$vigusu."'";
	$res = mysqli_query($linkbd,$sqlr);
	while($row = mysqli_fetch_assoc($res)){
		if($row["id_cargo"] == '0'){
			$_POST['ppto'][] = buscatercero($_POST['tercero']);
			$_POST['nomcargo'][] = 'BENEFICIARIO';
		}else{
			$sqlr1 = "SELECT cedulanit,(SELECT nombrecargo FROM planaccargos WHERE codcargo='".$row["id_cargo"]."') FROM planestructura_terceros WHERE codcargo='".$row["id_cargo"]."' AND estado='S'";
			$res1 = mysqli_query($linkbd,$sqlr1);
			$row1 = mysqli_fetch_row($res1);
			$_POST['ppto'][] = buscar_empleado($row1[0]);
			$_POST['nomcargo'][] = $row1[1];
		}
	}
	for($x = 0; $x < count($_POST['ppto']); $x++){
		$pdf->ln(20);
		$v = $pdf->gety();
		if($v>=251){ 
			$pdf->AddPage();
			$pdf->ln(20);
			$v = $pdf->gety();
		}
		$pdf->setFont('times','B',8);
		if (($x%2)==0){
			if(isset($_POST['ppto'][$x+1])){
				$pdf->Line(17,$v,107,$v);
				$pdf->Line(112,$v,202,$v);
				$v2=$pdf->gety();
				$pdf->Cell(104,4,''.$_POST['ppto'][$x],0,1,'C',false,0,0,false,'T','C');
				$pdf->Cell(104,4,''.$_POST['nomcargo'][$x],0,1,'C',false,0,0,false,'T','C');
				$pdf->SetY($v2);
				$pdf->Cell(295,4,''.$_POST['ppto'][$x+1],0,1,'C',false,0,0,false,'T','C');
				$pdf->Cell(295,4,''.$_POST['nomcargo'][$x+1],0,1,'C',false,0,0,false,'T','C');
			}else{
				$pdf->Line(50,$v,160,$v);
				$pdf->Cell(190,4,''.$_POST['ppto'][$x],0,1,'C',false,0,0,false,'T','C');
				$pdf->Cell(190,4,''.$_POST['nomcargo'][$x],0,0,'C',false,0,0,false,'T','C');
			}
			$v3=$pdf->gety();
		}
		$pdf->SetY($v3);
		$pdf->SetFont('helvetica','',7);
	}
	$pdf->Output();