<?php
//V 1000 12/12/16 
require('fpdf.php');
require('comun.inc');
session_start();
date_default_timezone_set("America/Bogota");
//*****las variables con los contenidos***********
//**********pdf*******
//$pdf=new FPDF('P','mm','Letter'); 
class PDF extends FPDF
{

//Cabecera de página
function Header()
{	
    $linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
$sqlr="select *from configbasica where estado='S'";
//echo $sqlr;
$res=mysqli_query($linkbd, $sqlr);
while($row=mysqli_fetch_row($res))
 {
  $nit=$row[0];
  $rs=$row[1];
 }
	  //Parte Izquierda
    $this->Image('imagenes/eng.jpg',23,13,25,25);
	$this->SetFont('Arial','B',12);
	$this->SetY(10);
	$this->RoundedRect(10, 10, 260, 31, 2.5,'' );
	$this->Cell(0.1);
    $this->Cell(50,31,'','R',0,'L'); 
	$this->SetY(10);
    $this->Cell(50.1);
    $this->Cell(200,5,''.$rs,0,0,'C'); 
	$this->SetFont('Arial','B',8);
	$this->SetY(15);
    $this->Cell(50.1);
    $this->Cell(200,5,''.$nit,0,0,'C'); //Cuadro Izquierda

    //*****************************************************************************************************************************
	$this->SetFont('Arial','B',12);
	$this->SetY(15);
    $this->Cell(50.1);
    $this->Cell(200,21,'LIBRO DIARIO',0,1,'C'); 
    $this->SetFont('Arial','B',10);
	$this->SetY(29);
    $this->Cell(50.1);
	$this->Cell(200,5,'PERIODO: '.$_POST['periodonom1'].'   Vigencia:'.$_POST['anioV'],0,0,'C');
	$this->SetY(35);
    $this->Cell(50.1);
	$this->Cell(200,5,'ORDENADO POR: '.$_POST['ordenadoPor'],0,0,'C');

//********************************************************************************************************************************
	switch($_POST['orden'])
	 {
		 case 1:
	$this->line(10.1,42,269,42);
	$this->RoundedRect(10,43, 260, 5, 1.2,'' );	
	$this->SetFont('Arial','B',9);
	//$this->SetY(43);
   // $this->Cell(0.1);
   // $this->Cell(15,5,'Fecha ',0,0,'C'); 
		$this->SetY(43);
    	$this->Cell(0.1);
		$this->Cell(45,5,'Tipo Comp - No Comp',0,0,'C');
		$this->SetY(43);
		$this->Cell(45);
		$this->Cell(25,5,'Tercero',0,0,'C');
		$this->SetY(43);
		$this->Cell(70);
		$this->Cell(20,5,'CC',0,0,'C');			
		$this->SetY(43);
		$this->Cell(100);
		$this->Cell(45,5,'Cuenta',0,0,'C');
		$this->SetY(43);
		$this->Cell(150.1);
		$this->Cell(55,5,'Detalle',0,1,'C');
		$this->SetY(43);
		$this->Cell(210.1);
		$this->Cell(35,5,'Debitos',0,1,'C');
		$this->SetY(43);
		$this->Cell(230);
		$this->Cell(35,5,'Creditos',0,0,'C');
	//	$this->line(10.1,49,269,49);
		$this->ln(6);
	 break;
	 case 2:
	 break;
	 case 3:
	 break;
	 }
//***********************************************************************************************************************************
}
//Pie de página
function Footer()
{

    $this->SetY(-13);
	$this->SetFont('Arial','I',10);
	$this->Cell(0,10,'Pagina '.$this->PageNo().' de {nb}',0,0,'R'); // el parametro {nb} 
}
}

//Creación del objeto de la clase heredada
//$pdf=new PDF('P','mm',array(210,140));
$pdf=new PDF('L','mm','Letter'); 
$pdf->AliasNbPages();
$pdf->AddPage();

$pdf->SetFont('Times','',7);

$pdf->SetAutoPageBreak(true,20);

$pdf->SetY(50);   
$con=0;

switch($_POST['orden'])
 {
	case 1:
//while ($con<15)
	while ($con<count($_POST['tipocomps']))
	{	
	if ($con%2==0)
	{$pdf->SetFillColor(255,255,255);
	}
    else
	{$pdf->SetFillColor(245,245,245);
	}	
   // $pdf->Cell(15,4,''.$_POST[fechas][$con],0,0,'L',1);//descrip
    $pdf->Cell(45,4,substr(''.$_POST['tipocomps'][$con].'-'.$_POST['ncomps'][$con],0,50),0,0,'L',1);//descrip
	$pdf->Cell(30,4,$_POST['terceros'][$con].' '.$_POST['nterceros'][$con],0,0,'L',1);//descrip
	$pdf->Cell(8,4,''.$_POST['ccs'][$con],0,0,'R',1);//descrip
    $pdf->Cell(60,4,$_POST['cuentas'][$con].' '.$_POST['ncuentas'][$con],0,0,'L',1);//descrip
    $pdf->Cell(75,4,''.substr($_POST['detalles'][$con],0,65),0,0,'L',1);//descrip
    $pdf->Cell(20,4,''.number_format($_POST['debitos'][$con],2),0,0,'R',1);//descrip
  $pdf->Cell(20,4,''.number_format($_POST['creditos'][$con],2),0,0,'R',1);
	$pdf->ln(4);	
$con=$con+1;
	}
	break;
 }	 
	$pdf->ln(6);	
$pdf->SetFont('Times','B',8);
$pdf->Cell(200,5,'Total','T',0,'R');
/* $pdf->Cell(30,5,''.number_format($_POST['totiniciales'],2,".",","),'T',0,'R'); */
$pdf->Cell(31,5,''.number_format($_POST['sumadebitos'],2,".",","),'T',0,'R');
$pdf->Cell(25,5,''.number_format($_POST['sumacreditos'],2,".",","),'T',0,'R');
/* $pdf->Cell(31,5,''.number_format($_POST['totnuevosaldos'],2,".",","),'T',0,'R'); */

$pdf->Output();
?> 


