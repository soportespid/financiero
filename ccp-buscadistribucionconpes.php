<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");

	require "comun.inc";
	require "funciones.inc";
	require "conversor.php";
	require "validaciones.inc";

	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");

	session_start();
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html lang="es">
	<head>
		<meta charset="utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1"/>
		<meta name="viewport" content="user-scalable=no">
		<title>:: IDEAL 10 - Contabilidad</title>
		<link href="favicon.ico" rel="shortcut icon"/>
        <link href="css/css2.css" rel="stylesheet" type="text/css" />
        <link href="css/css3.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="css/programas.js"></script>
		<script>
			//************* ver reporte ************
			//***************************************
			function verep(idfac)
			{
				document.form1.oculto.value=idfac;
				document.form1.submit();
  			}

			//************* genera reporte ************
			//***************************************
			function genrep(idfac)
			{
				document.form2.oculto.value=idfac;
				document.form2.submit();
			}

			//************* genera reporte ************
			//***************************************
			function guardar()
			{
				if (document.form2.documento.value!='')
  				{
					if (confirm("Esta Seguro de Guardar"))
  					{
						document.form2.oculto.value=2;
						document.form2.submit();
  					}
  				}
  				else{
  					alert('Faltan datos para completar el registro');
  				}
 			}

			function validar(formulario)
			{
				document.form2.action="presu-buscacodigosconpes.php";
				document.form2.submit();
			}

			function cleanForm()
			{
				document.form2.nombre1.value="";
				document.form2.nombre2.value="";
				document.form2.apellido1.value="";
				document.form2.apellido2.value="";
				document.form2.documento.value="";
				document.form2.codver.value="";
				document.form2.telefono.value="";
				document.form2.direccion.value="";
				document.form2.email.value="";
				document.form2.web.value="";
				document.form2.celular.value="";
				document.form2.razonsocial.value="";
			}
		</script>
    </head>
    <body>
        <IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
        <span id="todastablas2"></span>
        <table>
            <tr><script>barra_imagenes("ccpet");</script><?php cuadro_titulos();?></tr>	 
            <tr><?php menu_desplegable("ccpet");?></tr>
       		<tr>
          		<td colspan="3" class="cinta">
					<a href="ccp-distribucionconpes.php" class="mgbt"><img src="imagenes/add.png" title="Nuevo"/></a>
					<a class="mgbt"><img src="imagenes/guardad.png"/></a>
					<a onClick="document.form2.submit();" href="#" class="mgbt"><img src="imagenes/busca.png" title="Buscar"/></a>
					<a href="#" onClick="mypop=window.open('ccp-principal.php','','');mypop.focus();" class="mgbt"><img src="imagenes/nv.png" title="Nueva Ventana"></a>
				</td>
  			</tr>	
		</table>
 		<form name="form2" method="post" action="ccp-buscadistribucionconpes.php">
			<table class="inicio" align="center" >
      			<tr>
					<td class="titulos" colspan="4">:: Buscar  Conpes </td>
					<td class="cerrar" ><a href="ccp-principal.php">Cerrar</a></td>
      			</tr>
      			<tr>
         			<td class="saludo1">Codigo:
        			</td>
        			<td>
						<input name="documento" type="text" id="documento" value="" size="10" maxlength="2">
					</td>
					<td class="saludo1">Nombre:</td>
					<td><input name="nombre" type="text" value="" size="40">
        			<input name="oculto" type="hidden" value="1"> </td>                         
       			</tr>                       
    		</table>    
    		<div class="subpantalla" style="height:68.5%; width:99.6%; overflow-x:hidden;">
				<?php
				$oculto=$_POST['oculto'];
				$crit1=" ";
				$crit2=" ";
				if ($_POST['nombre']!="")
				$crit1=" and presuconpes.nombre_conpes like '%".$_POST['nombre']."%' ";
				if ($_POST['documento']!="")
				$crit2=" and presuconpes.id_conpes like '%$_POST[documento]%' ";

				$sqlr="select *from presuconpes where presuconpes.estado='S' ".$crit1.$crit2." order by presuconpes.id_conpes desc";

				$resp = mysqli_query($linkbd, $sqlr);
				$ntr = mysqli_num_rows($resp);
				$con=1;
				echo "<table class='inicio' align='center' width='80%'>
					<tr>
						<td colspan='8' class='titulos'>.: Resultados Busqueda:</td>
					</tr>
					<tr>
						<td colspan='5'>INGRESOS  CONPES: $ntr</td>
					</tr>
					<tr>
						<td width='5%' class='titulos2'>Codigo</td>
						<td class='titulos2'>Nombre</td>
						<td class='titulos2' width='5%'>Editar</td>
					</tr>";	

				$iter='zebra1';
				$iter2='zebra2';
				while ($row =mysqli_fetch_row($resp)) 
		 		{
			 		echo "<tr class='$iter' onDblClick=\"location.href='ccp-editadistribucionconpes.php?is=$row[0]'\">
					<td>".strtoupper($row[0])."</td>
					<td>".strtoupper($row[1])."</td>
					<td><a href='ccp-editadistribucionconpes.php?is=$row[0]'><center><img src='imagenes/buscarep.png'></center></a></td>
					</tr>";
					$con+=1;
					$aux=$iter;
					$iter=$iter2;
					$iter2=$aux;
		 		}
 				echo"</table>";
				?>
			</div>
		</form>
 
	</body>
</html>