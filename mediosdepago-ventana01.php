<?php 
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require"comun.inc";
	require"funciones.inc";
	session_start();
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html lang="es">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1">
		<title>:: SPID Calidad</title>
		<link href="css/css2.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css">
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css">
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css">
		<script type="text/javascript" src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script>
			function ponprefijo(nombre,cuenta){
				parent.document.getElementById('cb').value = cuenta ;
				parent.document.getElementById('nbanco').value = nombre ;
				parent.despliegamodal2("hidden");
			}
		</script>
		<?php titlepag();?>
	</head>
	<body>
		<form name="form2" method="post">
			<?php
				if($_POST['oculto'] == ""){
					$_POST['vtipo'] = $_GET['tipo'];
				}
			?>
			<table  class="inicio ancho" style="width:99.4%;" >
				<tr>
					<td class="titulos" colspan="4">:: Buscar Cuenta</td>
					<td class="cerrar" style="width:7%" onClick="parent.despliegamodal2('hidden');">Cerrar</td>
				</tr>
				<tr>
					<td class="saludo1" style="width:4cm;">Nombre:</td>
					<td >
						<input type="text" name="nombre" id="nombre" value="<?php echo $_POST['nombre'];?>" style="width:60%;"/>
						<em name="bboton" class="botonflecha" onClick="limbusquedas();" >Buscar</em>
					</td>
				</tr>
			</table> 
			<input type="hidden" name="oculto" id="oculto" value="1"/>
			<input type="hidden" name="vtipo" id="vtipo" value="<?php echo $_POST['vtipo']?>"/>
			<div class="subpantalla" style="height:83.5%; width:99%; overflow-x:hidden;">
				<?php
					$crit1=" ";
					$crit2=" ";
					if ($_POST['nombre'] != "")
					{$crit1="AND nombre LIKE '%".$_POST['nombre']."%'";}

					$sqlr = "SELECT id, nombre, cuentacontable FROM tesomediodepagossf where estado = 'S'  $crit1 ORDER BY id";
					$resp = mysqli_query($linkbd,$sqlr);
					echo "
					<table class='inicio' align='center' width='99%'>
						<tr>
							<td colspan='4' class='titulos'>.: Resultados Busqueda:</td>
						</tr>
						<tr>
							<td class='titulos2' style= 'width:6%;' >ID</td>
							<td class='titulos2' style= 'width:15%;'>CUENTA</td>
							<td class='titulos2' >NOMBRE</td>
						</tr>";	
					$iter = 'saludo1a';
					$iter2 = 'saludo2';
					while ($row = mysqli_fetch_row($resp))
					{
						echo "
						<tr class='$iter' onClick=\"javascript:ponprefijo('$row[1]', '$row[2]')\">
							<td>$con2</td>
							<td>".strtoupper($row[0])."</td>
							<td>".strtoupper($row[2])."</td>
							<td>".strtoupper($row[1])."</td>
						</tr>";
						$aux=$iter;
						$iter=$iter2;
						$iter2=$aux;
					}
					echo "</table>";
				?>
			</div>
		</form>
	</body>
</html>
