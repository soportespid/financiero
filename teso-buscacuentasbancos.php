<?php
header("Cache-control: no-cache, no-store, must-revalidate");
header("Content-Type: text/html;charset=utf8");
require "comun.inc";
require "funciones.inc";
session_start();
$linkbd = conectar_v7();
$linkbd->set_charset("utf8");
cargarcodigopag($_GET['codpag'], $_SESSION["nivel"]);
date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE>
<html lang="es">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1">
    <meta name="viewport" content="user-scalable=no">
    <title>:: IDEAL 10 - Parametrización</title>
    <link href="favicon.ico" rel="shortcut icon" />
    <link href="css/css2z.css?<?php echo date('d_m_Y_h_i_s'); ?>" rel="stylesheet" />
    <link href="css/css3.css?<?php echo date('d_m_Y_h_i_s'); ?>" rel="stylesheet" />
    <link href="css/style.css?<?php echo date('d_m_Y_h_i_s'); ?>" rel="stylesheet" />
    <script type="text/javascript" src="css/programas.js?<?php echo date('d_m_Y_h_i_s'); ?>"></script>
    <script type="text/javascript" src="css/calendario.js?<?php echo date('d_m_Y_h_i_s'); ?>"></script>
    <script type="text/javascript" src="JQuery/jquery-2.1.4.min.js?<?php echo date('d_m_Y_h_i_s'); ?>"></script>
    <script>
        function verUltimaPos(idcta, filas) {
            var scrtop = $('#divdet').scrollTop();
            var altura = $('#divdet').height();
            var numpag = $('#nummul').val();
            var limreg = $('#numres').val();
            if ((numpag <= 0) || (numpag == ""))
                numpag = 0;
            if ((limreg == 0) || (limreg == ""))
                limreg = 10;
            numpag++;
            location.href = "teso-editacuentasbancos.php?idr=" + idcta + "&scrtop=" + scrtop + "&totreg=" + filas + "&altura=" + altura + "&numpag=" + numpag + "&limreg=" + limreg;
        }
        function crearexcel() {
            document.form2.action = "teso-buscacuentasbancosexcel.php";
            document.form2.target = "_BLANK";
            document.form2.submit();
            document.form2.action = "";
            document.form2.target = "";
        }
        function buscacta(e) {
            if (document.form2.cuenta.value != "") {
                document.form2.bc.value = '1';
                document.form2.submit();
            }
        }
        function validar() {
            document.form2.submit();
        }
        function buscater(e) {
            if (document.form2.tercero.value != "") {
                document.form2.bt.value = '1';
                document.form2.submit();
            }
        }
        function agregardetalle() {
            if (document.form2.numero.value != "" && document.form2.valor.value > 0 && document.form2.banco.value != "") {
                document.form2.agregadet.value = 1;
                //document.form2.chacuerdo.value=2;
                document.form2.submit();
            } else {
                alert("Falta informacion para poder Agregar");
            }
        }
        function eliminar(variable) {
            if (confirm("Esta Seguro de Eliminar")) {
                document.form2.elimina.value = variable;
                vvend = document.getElementById('elimina');
                vvend.value = variable;
                document.form2.submit();
            }
        }
        function guardar() {
            if (document.form2.fecha.value != '') {
                if (confirm("Esta Seguro de Guardar")) {
                    document.form2.oculto.value = 2;
                    document.form2.submit();
                }
            } else {
                alert('Faltan datos para completar el registro');
                document.form2.fecha.focus();
                document.form2.fecha.select();
            }
        }
        function pdf() {
            document.form2.action = "teso-pdfconsignaciones.php";
            document.form2.target = "_BLANK";
            document.form2.submit();
            document.form2.action = "";
            document.form2.target = "";
        }
        function cambioswitch(id, valor) {
            if (valor == 1) {
                if (confirm("Desea activar Estado")) { document.form2.cambioestado.value = "1"; }
                else { document.form2.nocambioestado.value = "1" }
            } else {
                if (confirm("Desea Desactivar Estado")) { document.form2.cambioestado.value = "0"; }
                else { document.form2.nocambioestado.value = "0" }
            }
            document.getElementById('idestado').value = id;
            document.form2.submit();
        }
    </script>
    <?php
    $scrtop = $_GET['scrtop'];
    if ($scrtop == "")
        $scrtop = 0;
    echo "<script>
				window.onload=function(){
					$('#divdet').scrollTop(" . $scrtop . ")
				}
			</script>";
    $gidcta = $_GET['idcta'];
    ?>
</head>

<body>
    <IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
    <span id="todastablas2"></span>
    <table>
        <tr>
            <script>barra_imagenes("para");</script><?php cuadro_titulos(); ?>
        </tr>
        <tr><?php menu_desplegable("para"); ?></tr>
    </table>
    <div class="bg-white group-btn p-1"><button type="button" onclick="window.location.href='teso-cuentasbancos.php'"
            class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
            <span>Nuevo</span>
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960">
                <path d="M440-440H200v-80h240v-240h80v240h240v80H520v240h-80v-240Z"></path>
            </svg>
        </button><button type="button" onclick="mypop=window.open('plan-agenda.php','','');mypop.focus()"
            class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
            <span class="group-hover:text-white">Agenda</span>
            <svg class="fill-black group-hover:fill-white w-5 h-5" xmlns="http://www.w3.org/2000/svg"
                viewBox="0 -960 960 960">
                <path
                    d="M560-564v-68q33-14 67.5-21t72.5-7q26 0 51 4t49 10v64q-24-9-48.5-13.5T700-600q-38 0-73 9.5T560-564Zm0 220v-68q33-14 67.5-21t72.5-7q26 0 51 4t49 10v64q-24-9-48.5-13.5T700-380q-38 0-73 9t-67 27Zm0-110v-68q33-14 67.5-21t72.5-7q26 0 51 4t49 10v64q-24-9-48.5-13.5T700-490q-38 0-73 9.5T560-454ZM260-320q47 0 91.5 10.5T440-278v-394q-41-24-87-36t-93-12q-36 0-71.5 7T120-692v396q35-12 69.5-18t70.5-6Zm260 42q44-21 88.5-31.5T700-320q36 0 70.5 6t69.5 18v-396q-33-14-68.5-21t-71.5-7q-47 0-93 12t-87 36v394Zm-40 118q-48-38-104-59t-116-21q-42 0-82.5 11T100-198q-21 11-40.5-1T40-234v-482q0-11 5.5-21T62-752q46-24 96-36t102-12q58 0 113.5 15T480-740q51-30 106.5-45T700-800q52 0 102 12t96 36q11 5 16.5 15t5.5 21v482q0 23-19.5 35t-40.5 1q-37-20-77.5-31T700-240q-60 0-116 21t-104 59ZM280-494Z">
                </path>
            </svg>
        </button><button type="button" onclick="mypop=window.open('para-principal.php','','');mypop.focus();"
            class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
            <span>Nueva ventana</span>
            <svg class="fill-black group-hover:fill-white w-5 h-5" xmlns="http://www.w3.org/2000/svg"
                viewBox="0 -960 960 960">
                <path
                    d="M320-120v-80h80v-80H160q-33 0-56.5-23.5T80-360v-400q0-33 23.5-56.5T160-840h640q33 0 56.5 23.5T880-760v400q0 33-23.5 56.5T800-280H560v80h80v80H320ZM160-360h640v-400H160v400Zm0 0v-400 400Z">
                </path>
            </svg>
        </button><button type="button"
            onclick="mypop=window.open('/financiero/teso-buscacuentasbancos.php','','');mypop.focus();"
            class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
            <span class="group-hover:text-white">Duplicar pantalla</span>
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960">
                <path
                    d="M640-160v-360H160v360h480Zm80-200v-80h80v-360H320v200h-80v-200q0-33 23.5-56.5T320-880h480q33 0 56.5 23.5T880-800v360q0 33-23.5 56.5T800-360h-80ZM160-80q-33 0-56.5-23.5T80-160v-360q0-33 23.5-56.5T160-600h480q33 0 56.5 23.5T720-520v360q0 33-23.5 56.5T640-80H160Zm400-603ZM400-340Z">
                </path>
            </svg>
        </button><button type="button" onclick="crearexcel()"
            class="btn btn-success btn-success-hover d-flex justify-between align-items-center">
            <span>Exportar Excel</span>
            <svg class="fill-black group-hover:fill-white w-4 h-4" xmlns="http://www.w3.org/2000/svg"
                viewBox="0 0 384 512"><!--!Font Awesome Free 6.5.1 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license/free Copyright 2024 Fonticons, Inc. -->
                <path
                    d="M48 448V64c0-8.8 7.2-16 16-16H224v80c0 17.7 14.3 32 32 32h80V448c0 8.8-7.2 16-16 16H64c-8.8 0-16-7.2-16-16zM64 0C28.7 0 0 28.7 0 64V448c0 35.3 28.7 64 64 64H320c35.3 0 64-28.7 64-64V154.5c0-17-6.7-33.3-18.7-45.3L274.7 18.7C262.7 6.7 246.5 0 229.5 0H64zm90.9 233.3c-8.1-10.5-23.2-12.3-33.7-4.2s-12.3 23.2-4.2 33.7L161.6 320l-44.5 57.3c-8.1 10.5-6.3 25.5 4.2 33.7s25.5 6.3 33.7-4.2L192 359.1l37.1 47.6c8.1 10.5 23.2 12.3 33.7 4.2s12.3-23.2 4.2-33.7L222.4 320l44.5-57.3c8.1-10.5 6.3-25.5-4.2-33.7s-25.5-6.3-33.7 4.2L192 280.9l-37.1-47.6z">
                </path>
            </svg>
        </button></div>
    <form name="form2" method="post" action="teso-buscacuentasbancos.php">
        <?php
        if ($_GET['numpag'] != "") {
            $oculto = $_POST['oculto'];
            if ($oculto != 2) {
                $_POST['numres'] = $_GET['limreg'];
                $_POST['numpos'] = $_GET['limreg'] * ($_GET['numpag'] - 1);
                $_POST['nummul'] = $_GET['numpag'] - 1;
            }
        } else {
            if ($_POST['nummul'] == "") {
                $_POST['numres'] = 10;
                $_POST['numpos'] = 0;
                $_POST['nummul'] = 0;
            }
        }
        ?>
        <input type="hidden" name="numres" id="numres" value="<?php echo $_POST['numres']; ?>" />
        <input type="hidden" name="numpos" id="numpos" value="<?php echo $_POST['numpos']; ?>" />
        <input type="hidden" name="nummul" id="nummul" value="<?php echo $_POST['nummul']; ?>" />
        <?php
        if ($_POST['oculto2'] == "") {
            $_POST['oculto2'] = "0";
            $_POST['cambioestado'] = "";
            $_POST['nocambioestado'] = "";
        }
        //*****************************************************************
        if ($_POST['cambioestado'] != "") {
            if ($_POST['cambioestado'] == "1") {
                $sqlr = "UPDATE tesobancosctas SET estado='S' WHERE cuenta='" . $_POST['idestado'] . "'";
                mysqli_fetch_row(mysqli_query($linkbd, $sqlr));
            } else {
                $sqlr = "UPDATE tesobancosctas SET estado='N' WHERE cuenta='" . $_POST['idestado'] . "'";
                mysqli_fetch_row(mysqli_query($linkbd, $sqlr));
            }
        }
        //*****************************************************************
        if ($_POST['nocambioestado'] != "") {
            if ($_POST['nocambioestado'] == "1") {
                $_POST['lswitch1'][$_POST['idestado']] = 1;
            } else {
                $_POST['lswitch1'][$_POST['idestado']] = 0;
            }
            $_POST['nocambioestado'] = "";
        }
        ?>
        <table class="inicio" align="center">
            <tr>
                <td class="titulos" colspan="6">:. Buscar Cuentas Bancarias</td>
                <td class="cerrar" style="width:7%" onClick="location.href='para-principal.php'">Cerrar</td>
            </tr>
            <tr>
            <tr>
                <td class="saludo1">Nit Tercero:</td>
                <td width="139"><input id="tercero" type="text" name="tercero" size="12"
                        onKeyUp="return tabular(event,this)" onBlur="buscater(event)"
                        value="<?php echo $_POST['tercero'] ?>"
                        onClick="document.getElementById('tercero').focus();document.getElementById('tercero').select();"><input
                        type="hidden" value="0" name="bt"> <input type="hidden" name="chacuerdo" value="1"><input
                        type="hidden" value="1" name="oculto"></td>
                <td class="saludo1">Raz&oacute;n Social:</td>
                <td width="298" colspan="2"><input name="ntercero" type="text" value="<?php echo $_POST['ntercero'] ?>"
                        size="45"></td>
                <input name="oculto" id="oculto" type="hidden" value="1">
            </tr>
        </table>
        <input type="hidden" name="oculto2" id="oculto2" value="<?php echo $_POST['oculto2']; ?>">
        <input type="hidden" name="cambioestado" id="cambioestado" value="<?php echo $_POST['cambioestado']; ?>">
        <input type="hidden" name="nocambioestado" id="nocambioestado" value="<?php echo $_POST['nocambioestado']; ?>">
        <input type="hidden" name="idestado" id="idestado" value="<?php echo $_POST['idestado']; ?>">
        <div class="subpantalla" style="height:68.5%; width:99.6%; overflow-x:hidden;" id="divdet">
            <?php
            $oculto = $_POST['oculto'];
            $crit1 = " ";
            $crit2 = " ";
            if ($_POST['tercero'] != "")
                $crit1 = " and tesobancosctas.tercero like '%" . $_POST['tercero'] . "%' ";
            if ($_POST['ntercero'] != "")
                $crit2 = " and terceros.razonsocial like '%" . $_POST['ntercero'] . "%' ";
            //sacar el consecutivo
            $sqlr = "select *from terceros, tesobancosctas, cuentasnicsp where terceros.cedulanit=tesobancosctas.tercero and cuentasnicsp.cuenta=tesobancosctas.cuenta $crit1 $crit2 order by cuentasnicsp.cuenta,terceros.cedulanit";
            $resp = mysqli_query($linkbd, $sqlr);
            $ntr = mysqli_num_rows($resp);
            while ($row = mysqli_fetch_row($resp)) {
                echo "
							<input type='hidden' name='nitercero[]' id='nitercero[]' value='$row[12]'/>
							<input type='hidden' name='rasocial[]' id='rasocial[]' value='$row[5]'/>
							<input type='hidden' name='cuenta[]' id='cuenta[]' value='$row[28]'/>
							<input type='hidden' name='cuentacont[]' id='cuentacont[]' value='$row[27]'/>
							<input type='hidden' name='cuentaban[]' id='cuentaban[]' value='$row[24]'/>
							<input type='hidden' name='tipocuenta[]' id='tipocuenta[]' value='$row[25]'/>";
            }
            $_POST['numtop'] = $ntr;
            $nuncilumnas = ceil($_POST['numtop'] / $_POST['numres']);
            $cond2 = "";
            if ($_POST['numres'] != "-1") {
                $cond2 = "LIMIT $_POST[numpos], $_POST[numres]";
            }
            $sqlr2 = "SELECT * from terceros, tesobancosctas, cuentasnicsp where terceros.cedulanit=tesobancosctas.tercero and cuentasnicsp.cuenta=tesobancosctas.cuenta $crit1 $crit2 order by cuentasnicsp.cuenta,terceros.cedulanit " . $cond2;
            $resp = mysqli_query($linkbd, $sqlr2);
            $con = 1;
            $numcontrol = $_POST['nummul'] + 1;
            if ($nuncilumnas == $numcontrol) {
                $imagenforward = "<img src='imagenes/forward02.png' style='width:17px'>";
                $imagensforward = "<img src='imagenes/skip_forward02.png' style='width:16px' >";
            } else {
                $imagenforward = "<img src='imagenes/forward01.png' style='width:17px' title='Siguiente' onClick='numsiguiente()'>";
                $imagensforward = "<img src='imagenes/skip_forward01.png' style='width:16px' title='Fin' onClick='saltocol(\"$nuncilumnas\")'>";
            }
            if ($_POST['numpos'] == 0) {
                $imagenback = "<img src='imagenes/back02.png' style='width:17px'>";
                $imagensback = "<img src='imagenes/skip_back02.png' style='width:16px'>";
            } else {
                $imagenback = "<img src='imagenes/back01.png' style='width:17px' title='Anterior' onClick='numanterior();'>";
                $imagensback = "<img src='imagenes/skip_back01.png' style='width:16px' title='Inicio' onClick='saltocol(\"1\")'>";
            }
            $con = 1;
            echo "
					<table class='inicio' align='center' >
						<tr>
							<td colspan='8' class='titulos'>.: Resultados Busqueda:</td>
							<td class='submenu'>
								<select name='renumres' id='renumres' onChange='cambionum();' style='width:100%'>
									<option value='10'";
            if ($_POST['renumres'] == '10') {
                echo 'selected';
            }
            echo ">10</option>
									<option value='20'";
            if ($_POST['renumres'] == '20') {
                echo 'selected';
            }
            echo ">20</option>
									<option value='30'";
            if ($_POST['renumres'] == '30') {
                echo 'selected';
            }
            echo ">30</option>
									<option value='50'";
            if ($_POST['renumres'] == '50') {
                echo 'selected';
            }
            echo ">50</option>
									<option value='100'";
            if ($_POST['renumres'] == '100') {
                echo 'selected';
            }
            echo ">100</option>
									<option value='-1'";
            if ($_POST['renumres'] == '-1') {
                echo 'selected';
            }
            echo ">Todos</option>
								</select>
							</td>
						</tr>
						<tr>
							<td colspan='9'>Cuentas Bancarias Encontrados: $ntr</td>
						</tr>
						<tr>
							<td class='titulos2'>Nit Tercero</td>
							<td class='titulos2'>Raz&oacute;n Social</td>
							<td  class='titulos2'>Cuenta</td>
							<td class='titulos2'>Cuenta Contable</td>
							<td class='titulos2'>Cuenta Bancaria</td>
							<td class='titulos2'>Tipo Cuenta</td>
							<td class='titulos2' colspan='2' width='5%'><center>Estado</td>
							<td class='titulos2' width='5%'><center>Editar</td>
						</tr>";
            $iter = 'saludo1a';
            $iter2 = 'saludo2';
            $filas = 1;
            while ($row = mysqli_fetch_row($resp)) {
                if ($row[26] == 'S') {
                    $imgsem = "src='imagenes/sema_verdeON.jpg' title='Activo'";
                    $coloracti = "#0F0";
                    $_POST['lswitch1'][$row[22]] = 0;
                } else {
                    $imgsem = "src='imagenes/sema_rojoON.jpg' title='Inactivo'";
                    $coloracti = "#C00";
                    ;
                    $_POST['lswitch1'][$row[22]] = 1;
                }
                if ($gidcta != "") {
                    if ($gidcta == $row[24]) {
                        $estilo = 'background-color:yellow';
                    } else {
                        $estilo = "";
                    }
                } else {
                    $estilo = "";
                }
                $idcta = "'" . $row[24] . "'";
                $numfil = "'" . $filas . "'";
                $filtro = "'" . $_POST['tercero'] . "'";
                echo "
								<tr class='$iter' onMouseOver=\"anterior=this.style.backgroundColor;this.style.backgroundColor='#40b3ff';\" onMouseOut=\"this.style.backgroundColor=anterior\" onDblClick=\"verUltimaPos($idcta, $numfil, $filtro)\" style='text-transform:uppercase; $estilo' >
							<td>$row[12]</td>
							<td>$row[5]</td>
							<td>$row[28]</td>
							<td>$row[27]</td>
							<td>$row[24]</td>
							<td>$row[25]</td>
							<td style='text-align:center;'><img $imgsem style='width:20px'/></td>
							<td><input type='range' name='lswitch1[]' value='" . $_POST['lswitch1'][$row[22]] . "' min ='0' max='1' step ='1' style='background:$coloracti; width:60%' onChange='cambioswitch(\"" . $row[22] . "\",\"" . $_POST['lswitch1'][$row[22]] . "\")' /></td>";
                $idcta = "'" . $row[24] . "'";
                $numfil = "'" . $filas . "'";
                echo "<td style='text-align:center;'>
										<a onClick=\"verUltimaPos($idcta, $numfil)\" style='cursor:pointer;'>
											<img src='imagenes/b_edit.png' style='width:18px' title='Editar'>
										</a>
									</td>
								</tr>";
                $con += 1;
                $aux = $iter;
                $iter = $iter2;
                $iter2 = $aux;
                $filas++;
            }
            echo "</table>
					<table class='inicio'>
						<tr>
							<td style='text-align:center;'>
								<a href='#'>$imagensback</a>&nbsp;
								<a href='#'>$imagenback</a>&nbsp;&nbsp;";
            if ($nuncilumnas <= 9) {
                $numfin = $nuncilumnas;
            } else {
                $numfin = 9;
            }
            for ($xx = 1; $xx <= $numfin; $xx++) {
                if ($numcontrol <= 9) {
                    $numx = $xx;
                } else {
                    $numx = $xx + ($numcontrol - 9);
                }
                if ($numcontrol == $numx) {
                    echo "<a href='#' onClick='saltocol(\"$numx\")'; style='color:#24D915'> $numx </a>";
                } else {
                    echo "<a href='#' onClick='saltocol(\"$numx\")'; style='color:#000000'> $numx </a>";
                }
            }
            echo "		&nbsp;&nbsp;<a href='#'>$imagenforward</a>
									&nbsp;<a href='#'>$imagensforward</a>
								</td>
							</tr>
						</table>";
            ?>
        </div>
        <input type="hidden" name="ocules" id="ocules" value="<?php echo $_POST['ocules']; ?>">
        <input type="hidden" name="actdes" id="actdes" value="<?php echo $_POST['actdes']; ?>">
    </form>
</body>

</html>
