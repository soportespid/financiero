<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require_once 'PHPExcel/Classes/PHPExcel.php';
	require "comun.inc";
	require "funciones.inc";
	sesion();
	$linkbd=conectar_v7();
	$linkbd -> set_charset("utf8");
	date_default_timezone_set("America/Bogota");
	
	$objPHPExcel = new PHPExcel();
	//----Propiedades----
	$objPHPExcel->getProperties()
		->setCreator("SPID")
		->setLastModifiedBy("SPID")
		->setTitle("Liquidacion de Nomina")
		->setSubject("Nomina")
		->setDescription("Nomina")
		->setKeywords("Nomina")
		->setCategory("Gestion Humana");
	define('FORMAT_CURRENCY_PLN_1', '$ #,0_-');
	//----Cuerpo de Documento----
	$objPHPExcel->getActiveSheet()->mergeCells('A1:V1');
	$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A1', 'EJECUCION PRESUPUESTAL CUIPO PERIODO: '.$_POST['fechai'].' Al '.$_POST['fechaf']);
	$objFont=$objPHPExcel->getActiveSheet()->getStyle('A1')->getFont();
	$objFont->setName('Courier New');
	$objFont->setSize(15);
	$objFont->setBold(true);
	$objFont->setUnderline(PHPExcel_Style_Font::UNDERLINE_SINGLE);
	$objFont->getColor()->setARGB( PHPExcel_Style_Color::COLOR_BLACK);
	$objAlign=$objPHPExcel->getActiveSheet()->getStyle('A1')->getAlignment(); 
	$objAlign->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
	$objAlign->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
	$objPHPExcel-> getActiveSheet ()
		-> getStyle ("A2:V2")
		-> getFill ()
		-> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
		-> getStartColor ()
		-> setRGB ('A6E5F3');
	$objPHPExcel-> getActiveSheet ()
		-> getStyle ("A1")	
		-> getFill ()
		-> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
		-> getStartColor ()
		-> setRGB ('A6E5F3');
	$borders = array(
		'borders' => array(
			'allborders' => array(
				'style' => PHPExcel_Style_Border::BORDER_THIN,
				'color' => array('argb' => 'FF000000'),
			)
		),
	);
	$objPHPExcel->getActiveSheet()->getStyle('A2:V2')->applyFromArray($borders);
	$objPHPExcel->setActiveSheetIndex(0)
			->setCellValue('A2', 'Cuenta')
			->setCellValue('B2', utf8_encode('Descripci�n'))
			->setCellValue('C2', 'Vigencia Del Gasto')
			->setCellValue('D2', utf8_encode('Secci�n Ppresupuestal'))
			->setCellValue('E2', 'Programatico MGA')
			->setCellValue('F2', 'CPC')
			->setCellValue('G2', 'Fuentes')
			->setCellValue('H2', 'BPIN')
			->setCellValue('I2',  utf8_encode('Situaci�n de Fondos'))
			->setCellValue('J2', 'Politica Publica')
			->setCellValue('K2', 'Tercero CHIP')
			->setCellValue('L2', 'Inicial')
			->setCellValue('M2', utf8_encode('Adici�n'))
			->setCellValue('N2', utf8_encode('Reducci�n'))
			->setCellValue('O2', 'Credito')
			->setCellValue('P2', 'Contracredito')
			->setCellValue('Q2', 'Definitivo')
			->setCellValue('R2', 'Disponibilidad')
			->setCellValue('S2', 'Compromisos')
			->setCellValue('T2', 'Obligaciones')
			->setCellValue('U2', 'Pagos')
			->setCellValue('V2', 'Tipo Tabla');
	$i=3;
	$nombretablatemporal = $_SESSION['tablatemporal']."EP";
	if($_GET['programatico'] == ''){$vprogematico = 'NO APLICA';}
	else {$vprogematico = $_GET['programatico'];}
	if($_GET['cpc'] == ''){$vcpc = 'NO APLICA';}
	else {$vcpc = $_GET['cpc'];}
	$sqlver="
	SELECT id, codigocuenta, nombrecuenta, seccpresupuestal, vigenciagastos, programatico, bpin, cpc, fuente, situacionfondo, politicapublica, tercerochip, compromisos, obligaciones, pagos, tipotabla, fuente2, cpc2, programatico2, disponibilidad, apropiacionini, apropiaciondef, adiciones, reducciones, trascredito, trascontracredito
	FROM $nombretablatemporal
	WHERE codigocuenta = '".$_GET['cuenta']."'AND seccpresupuestal = '".$_GET['seccpresupuestal']."' AND programatico = '$vprogematico' AND cpc = '$vcpc' AND fuente2 = '".$_GET['fuente']."' AND bpin = '".$_GET['bpin']."' AND vigenciagastos = '".$_GET['vigengasto']."'
	ORDER BY id ASC";
	$resver = mysqli_query($linkbd,$sqlver);
	while ($rowver = mysqli_fetch_row($resver))
	{
		$objPHPExcel->setActiveSheetIndex(0)
		->setCellValueExplicit ("A$i", $rowver[1], PHPExcel_Cell_DataType :: TYPE_STRING)
		->setCellValueExplicit ("B$i", $rowver[2], PHPExcel_Cell_DataType :: TYPE_STRING)
		->setCellValueExplicit ("C$i", $rowver[4], PHPExcel_Cell_DataType :: TYPE_STRING)
		->setCellValueExplicit ("D$i", $rowver[3], PHPExcel_Cell_DataType :: TYPE_STRING)
		->setCellValueExplicit ("E$i","$rowver[18] ($rowver[5])", PHPExcel_Cell_DataType :: TYPE_STRING)
		->setCellValueExplicit ("F$i","$rowver[17] ($rowver[7])", PHPExcel_Cell_DataType :: TYPE_STRING)
		->setCellValueExplicit ("G$i","$rowver[16] ($rowver[8])", PHPExcel_Cell_DataType :: TYPE_STRING)
		->setCellValueExplicit ("H$i", $rowver[6], PHPExcel_Cell_DataType :: TYPE_STRING)
		->setCellValueExplicit ("I$i", $rowver[9], PHPExcel_Cell_DataType :: TYPE_STRING)
		->setCellValueExplicit ("J$i", $rowver[10], PHPExcel_Cell_DataType :: TYPE_STRING)
		->setCellValueExplicit ("K$i", $rowver[11], PHPExcel_Cell_DataType :: TYPE_STRING)
		->setCellValueExplicit ("L$i", $rowver[20], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
		->setCellValueExplicit ("M$i", $rowver[22], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
		->setCellValueExplicit ("N$i", $rowver[23], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
		->setCellValueExplicit ("O$i", $rowver[24], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
		->setCellValueExplicit ("P$i", $rowver[25], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
		->setCellValueExplicit ("Q$i", $rowver[21], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
		->setCellValueExplicit ("R$i", $rowver[19], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
		->setCellValueExplicit ("S$i", $rowver[12], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
		->setCellValueExplicit ("T$i", $rowver[13], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
		->setCellValueExplicit ("U$i", $rowver[14], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
		->setCellValueExplicit ("V$i", $rowver[15], PHPExcel_Cell_DataType :: TYPE_STRING);
		$objPHPExcel->getActiveSheet()->getStyle("A$i:V$i")->applyFromArray($borders);
		$objPHPExcel->getActiveSheet()->getStyle("L$i:U$i")->getNumberFormat()->setFormatCode(FORMAT_CURRENCY_PLN_1);
		$i++;
	}
	//----Propiedades de la hoja 1
	$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('N')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('O')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('P')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('Q')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('R')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('S')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('T')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('U')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('V')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->setTitle('General');
	$objPHPExcel->setActiveSheetIndex(0);

//----Guardar documento----
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename="Ejecucionpresupuestal.xlsx"');
header('Cache-Control: max-age=0');
 
$objWriter=PHPExcel_IOFactory::createWriter($objPHPExcel,'Excel2007');
$objWriter->save('php://output');
exit;

?>