<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require "comun.inc";
	require "funciones.inc";
	session_start();
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
	$scroll = $_GET['scrtop'];
	$totreg = $_GET['totreg'];
	$idcta = $_GET['idcta'];
	$altura = $_GET['altura'];
	$filtro = "'".$_GET['filtro']."'";
?>
<!DOCTYPE >
<html lang="es">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=9">
		<title>:: SPID - Gestion Humana</title>
		<link href="css/css2.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css">
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css">
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css">
		<link href="css/tabs2.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css">
		<link href="sweetalert2/dist/sweetalert2.min.css" rel="stylesheet">
		<script src="sweetalert2/dist/sweetalert2.min.js"></script>
		<script type="text/javascript" src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script>
			function validar(){
				document.form2.submit();
			}
			function eliminar(variable){
				Swal.fire({
					icon: 'question',
					title: '¿Esta seguro de eliminar?',
					showDenyButton: true,
					confirmButtonText: 'Eliminar',
					confirmButtonColor: '#01CC42',
					denyButtonText: 'Cancelar',
					denyButtonColor: '#FF121A',
				}).then(
					(result) => {
						if (result.isConfirmed){
							document.getElementById('elimina').value = variable;
							document.form2.oculto.value = "3";
							document.form2.submit();
						}else if (result.isDenied){
							Swal.fire({
								icon: 'info',
								title: 'No se Elimino',
								confirmButtonText: 'Continuar',
								confirmButtonColor: '#FF121A',
								timer: 2500
							});
						}
					}
				)
			}
			function guardar(){
				var validacion01 = document.getElementById('nombre').value;
				if (document.form2.codigo.value != '' && validacion01.trim() != ''){
					Swal.fire({
							icon: 'question',
							title: '¿Esta Seguro de guardar?',
							showDenyButton: true,
							confirmButtonText: 'Guardar',
							confirmButtonColor: '#01CC42',
							denyButtonText: 'Cancelar',
							denyButtonColor: '#FF121A',
						}).then(
							(result) => {
								if (result.isConfirmed){
									document.form2.oculto.value = "2";
									document.form2.submit();
								}else if (result.isDenied){
									Swal.fire({
										icon: 'info',
										title: 'No se guardo',
										confirmButtonText: 'Continuar',
										confirmButtonColor: '#FF121A',
										timer: 2500
									});
								}
							}
						)
				} else {
					Swal.fire({
						icon: 'error',
						title: 'Error!',
						text: 'Faltan datos para completar el registro',
						confirmButtonText: 'Continuar',
						confirmButtonColor: '#FF121A',
						timer: 2500
					});
				}
			}	
			function cambiocheck(id){
				switch(id){
					case "1":
						if(document.getElementById('idswparafiscal').value == 'S'){
							document.getElementById('idswparafiscal').value = 'N';
						} else {
							document.getElementById('idswparafiscal').value = 'S';
						}
						break;
					case "2":
						if(document.getElementById('idswprovision').value == 'S'){
							document.getElementById('idswprovision').value = 'N';
						} else {
							document.getElementById('idswprovision').value = 'S';
						}
						break;
					case "3":
						if(document.getElementById('idswsalud').value == 'S'){
							document.getElementById('idswsalud').value = 'N';
						} else {
							document.getElementById('idswsalud').value = 'S';
						}
						break;
					case "4":
						if(document.getElementById('idswpension').value == 'S'){
							document.getElementById('idswpension').value = 'N';
						} else {
							document.getElementById('idswpension').value = 'S';
						}
						break;
					case "5":
						if(document.getElementById('idswarl').value == 'S'){
							document.getElementById('idswarl').value = 'N';
						} else {
							document.getElementById('idswarl').value = 'S';
						}
						break;
				}
				document.form2.submit();
			}
			function adelante(scrtop, numpag, limreg, filtro, next){
				var maximo = document.getElementById('maximo').value;
				var actual = document.getElementById('codigo').value;
				if(parseFloat(maximo)>parseFloat(actual)){
					location.href = "hum-editaVariablesNominaCCPET.php?idr=" + next + "&scrtop=" + scrtop + "&numpag=" + numpag + "&limreg=" +limreg+ "&filtro=" +filtro;
				}
			}
			function atrasc(scrtop, numpag, limreg, filtro, prev){
				var minimo = document.getElementById('minimo').value;
				var actual = document.getElementById('codigo').value;
				if(parseFloat(minimo)<parseFloat(actual)){
					location.href = "hum-editaVariablesNominaCCPET.php?idr=" + prev + "&scrtop=" + scrtop + "&numpag=" + numpag + "&limreg=" + limreg + "&filtro=" + filtro;
				}
			}
			function iratras(scrtop, numpag, limreg, filtro){
				var idcta = document.getElementById('codigo').value;
				location.href = "hum-buscaVariablesNominaCCPET.php?idcta=" + idcta + "&scrtop=" + scrtop + "&numpag=" + numpag + "&limreg=" + limreg + "&filtro=" + filtro;
			}
			function fagregar(){
				if(document.form2.variablepago.value != -1){
					document.form2.oculto.value = "4";
					document.form2.submit();
				} else {
					Swal.fire({
						icon: 'error',
						title: 'Error!',
						text: 'Seleccionar variade de pago',
						confirmButtonText: 'Continuar',
						confirmButtonColor: '#FF121A',
						timer: 2500
					});
				}
			}
		</script>
		<?php titlepag();?>
	</head>
	<body>
		<IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
		<span id="todastablas2"></span>
		<?php
			$numpag = $_GET['numpag'];
			$limreg = $_GET['limreg'];
			$scrtop = 26 * $totreg;
		?>
		<table>
			<tr><script>barra_imagenes("hum");</script><?php cuadro_titulos();?></tr>
			<tr><?php menu_desplegable("hum");?></tr>
			<tr>
				<td colspan="3" class="cinta"><img src="imagenes/add.png" title="Nuevo" onClick="location.href='hum-variablesNominaCCPET.php'" class="mgbt"><img src="imagenes/guarda.png" title="Guardar" onClick="guardar();" class="mgbt"><img src="imagenes/busca.png"  title="Buscar" onClick="location.href='hum-buscaVariablesNominaCCPET.php'" class="mgbt"><img src="imagenes/nv.png" title="nueva ventana" onClick="mypop=window.open('hum-principal.php','','');mypop.focus();" class="mgbt"><img src="imagenes/iratras.png" title="Atr&aacute;s" onClick="iratras(<?php echo "$scrtop, $numpag, $limreg, $filtro"; ?>)" class="mgbt"></td>
			</tr>
		</table>
		<form name="form2" method="post" action="">
			<input type="hidden" name="valfocus" id="valfocus" value="1"/>
			<?php
				$vigencia = date('Y');
				$vigusu = vigencia_usuarios($_SESSION['cedulausu']);
				$vigencia = $vigusu;
				if ($_GET['idr'] != ""){
					echo "<script>document.getElementById('codrec').value = ".$_GET['idr'].";</script>";
				}
				$sqlr = "SELECT MIN(codigo), MAX(codigo) from ccpethumvariables ORDER BY codigo";
				$res = mysqli_query($linkbd,$sqlr);
				$r = mysqli_fetch_row($res);
				$_POST['minimo'] = $r[0];
				$_POST['maximo']= $r[1];
				$sqln = "SELECT codigo FROM ccpethumvariables WHERE codigo > '".$_GET['idr']."' ORDER BY codigo ASC LIMIT 1";//Siguiente
				$resn = mysqli_query($linkbd,$sqln);
				$row = mysqli_fetch_row($resn);
				$next = "'$row[0]'";
				$sqlp = "SELECT codigo FROM ccpethumvariables WHERE codigo < '".$_GET['idr']."' ORDER BY codigo DESC LIMIT 1";//Anterior
				$resp = mysqli_query($linkbd,$sqlp);
				$row = mysqli_fetch_row($resp);
				$prev = "'$row[0]'";
				if($_POST['oculto'] == ""){
					$_POST['tabgroup1'] = 1;
					if ($_POST['codrec'] != "" || $_GET['idr'] != ""){
						if($_POST['codrec'] != ""){
							$sqlr = "select * from ccpethumvariables where codigo = '".$_POST['codrec']."'";
						} else {
							$sqlr = "select * from ccpethumvariables where codigo ='".$_GET['idr']."'";
						}
					} else {
						$sqlr = "select * from ccpethumvariables ORDER BY codigo DESC";
					}
					$res = mysqli_query($linkbd,$sqlr); 
					$row = mysqli_fetch_row($res);
					$_POST['codigo'] = $row[0];
					$sqlr = "SELECT codigo1, codigo2 FROM ccpethumvariables_pro WHERE codigo1 = '".$_POST['codigo']."'";
					$resp = mysqli_query($linkbd,$sqlr);
					while ($row = mysqli_fetch_row($resp)){
						$sqlr1 = "SELECT nombre FROM ccpethumvariables WHERE codigo = '$row[1]'";
						$resp1 = mysqli_query($linkbd,$sqlr1);
						$row1 = mysqli_fetch_row($resp1);
						$_POST['codprovi2'][] = $row[1];
						$_POST['nomprovi2'][] = $row1[0];
					}
				}
				if(($_POST['oculto'] != "2") && ($_POST['oculto'] != "1") && ($_POST['oculto'] != "3") && ($_POST['oculto'] != "4")){
					$chkp = "";
					$sqlr = "SELECT * FROM ccpethumvariables WHERE codigo = '".$_POST['codigo']."'";
					$resp = mysqli_query($linkbd,$sqlr);
					while ($row = mysqli_fetch_row($resp)){
						$_POST['codigo'] = $row[0];
						$_POST['nombre'] = $row[1];
						$_POST['swparafiscal'] = $row[2];
						$_POST['swprovision'] = $row[4];
						$_POST['swsalud'] = $row[5];
						$_POST['swpension'] = $row[6];
						$_POST['swarl'] = $row[7];
						$_POST['funcionamiento'] = $row[8];
						$_POST['funtemporal'] = $row[9];
						$_POST['inversion'] = $row[10];
						$_POST['invtemporal'] = $row[11];
						$_POST['gastoscomer'] = $row[12];
						$_POST['gastoscomertem'] = $row[13];
					}
				}
				switch($_POST['tabgroup1']){
					case 1:	$check1 = 'checked';$check2 = '';break;
					case 2:	$check2 = 'checked';$check1 = '';break;
				}
			?>
			<div class="tabs" style="height:74.5%; width:99.6%" >
				<div class="tab">
					<input type="radio" id="tab-1" name="tabgroup1" value="1" <?php echo $check1;?> >
					<label id="clabel" for="tab-1">Variables</label>
					<div class="content" style="overflow:hidden;">
						<table class="inicio ancho" align="center" >
							<tr>
								<td class="titulos" colspan="10">.: Agregar Variable de Pago</td>
								<td class="cerrar" style="width:7%" onClick="location.href='hum-principal.php'">Cerrar</td>
							</tr>
							<tr>
								<td class="tamano01" style="width:3cm;">Codigo:</td>
								<td style="width:10%;">
									<img src="imagenes/back.png" onClick="atrasc(<?php echo "$scrtop, $numpag, $limreg, $filtro, $prev"; ?>)" class="icobut" title="Anterior"/>
									&nbsp;<input type="text" name="codigo" id="codigo" class="tamano02" value="<?php echo $_POST['codigo']?>" maxlength="2" style="width:35%" onKeyPress="javascript:return solonumeros(event)" onKeyUp="return tabular(event,this)"  readonly>&nbsp;<img src="imagenes/next.png" onClick="adelante(<?php echo "$scrtop, $numpag, $limreg, $filtro, $next" ?>);" class="icobut" title="Sigiente"/> 
									<input type="hidden" value="<?php echo $_POST['codrec']?>" name="codrec" id="codrec">
								</td>
								<td class="tamano01" style="width:3cm;">Nombre Ingreso:</td>
								<td colspan="7"><input type="text" class="tamano02" name="nombre" id="nombre" value="<?php echo $_POST['nombre']?>" style="width:100%;" onKeyUp="return tabular(event,this)"></td>
							</tr>
							<tr>
								<td class="tamano01">Paga Salud:</td>
								<td >
									<div class="swsino">
										<input type="checkbox" name="swsalud" class="swsino-checkbox" id="idswsalud" value="<?php echo $_POST['swsalud'];?>" <?php if($_POST['swsalud'] == 'S'){echo "checked";}?> onChange="cambiocheck('3');"/>
										<label class="swsino-label" for="idswsalud">
											<span class="swsino-inner"></span>
											<span class="swsino-switch"></span>
										</label>
									</div>
								</td>
								<td class="tamano01">Paga Pensi&oacute;n:</td>
								<td style="width:10%;">
									<div class="swsino">
										<input type="checkbox" name="swpension" class="swsino-checkbox" id="idswpension" value="<?php echo $_POST['swpension'];?>" <?php if($_POST['swpension'] == 'S'){echo "checked";}?> onChange="cambiocheck('4');"/>
										<label class="swsino-label" for="idswpension">
											<span class="swsino-inner"></span>
											<span class="swsino-switch"></span>
										</label>
									</div>
								</td>
								<td class="tamano01" style="width:3cm;">Paga ARL:</td>
								<td style="width:10%;">
									<div class="swsino">
										<input type="checkbox" name="swarl" class="swsino-checkbox" id="idswarl" value="<?php echo $_POST['swarl'];?>" <?php if($_POST['swarl'] == 'S'){echo "checked";}?> onChange="cambiocheck('5');"/>
										<label class="swsino-label" for="idswarl">
											<span class="swsino-inner"></span>
											<span class="swsino-switch"></span>
										</label>
									</div>
								</td>
								<td class="tamano01" style="width:3cm;">Paga Parafiscales:</td>
								<td style="width:10%;">
									<div class="swsino">
										<input type="checkbox" name="swparafiscal" class="swsino-checkbox" id="idswparafiscal" value="<?php echo $_POST['swparafiscal'];?>" <?php if($_POST['swparafiscal'] == 'S'){echo "checked";}?> onChange="cambiocheck('1');"/>
										<label class="swsino-label" for="idswparafiscal">
											<span class="swsino-inner"></span>
											<span class="swsino-switch"></span>
										</label>
									</div>
								</td>
								<td class="tamano01" style="width:3cm;">Provisiona:</td>
								<td style="width:10%;">
									<div class="swsino">
										<input type="checkbox" name="swprovision" class="swsino-checkbox" id="idswprovision" value="<?php echo $_POST['swprovision'];?>" <?php if($_POST['swprovision'] == 'S'){echo "checked";}?> onChange="cambiocheck('2');"/>
										<label class="swsino-label" for="idswprovision">
											<span class="swsino-inner"></span>
											<span class="swsino-switch"></span>
										</label>
									</div>
								</td>
							</tr>
							<tr>
								<td class="tamano01" >Funcionamiento: </td>
								<td colspan="3"  valign="middle">
									<select name="funcionamiento" id="funcionamiento" onChange="validar();" style="width:100%;" class="tamano02">
										<option value="-1">Seleccione...</option>
										<?php
											$sqlr="SELECT * FROM conceptoscontables WHERE modulo = '2' AND tipo = 'F' ORDER BY codigo";
											$resp = mysqli_query($linkbd,$sqlr);
											while ($row = mysqli_fetch_row($resp))
											{
												if($row[0] == $_POST['funcionamiento'])
												{
													echo "<option value='$row[0]' SELECTED>$row[0] - $row[3] - $row[1]</option>";
													$_POST['funcionamientonom'] = "$row[0] - $row[3] - $row[1]";
												}
												else{echo "<option value='$row[0]'>$row[0] - $row[3] - $row[1]</option>";}
											}
										?>
									</select>
									<input type="hidden" name="funcionamientonom" id="funcionamientonom" value="<?php echo $_POST['funcionamientonom']?>">
								</td>
								<td class="tamano01" >Funcionamiento Temporal: </td>
								<td colspan="3"  valign="middle">
									<select name="funtemporal" id="funtemporal" onChange="validar();" style="width:100%;" class="tamano02">
										<option value="-1">Seleccione...</option>
										<?php
											$sqlr = "SELECT * FROM conceptoscontables WHERE modulo = '2' AND tipo = 'FT' ORDER BY codigo";
											$resp = mysqli_query($linkbd,$sqlr);
											while ($row = mysqli_fetch_row($resp))
											{
												if($row[0] == $_POST['funtemporal'])
												{
													echo "<option value='$row[0]' SELECTED>$row[0] - $row[3] - $row[1]</option>";
													$_POST['funtemporalnom'] = "$row[0] - $row[3] - $row[1]";
												}
												else{echo "<option value='$row[0]'>$row[0] - $row[3] - $row[1]</option>";}
											}
										?>
									</select>
									<input type="hidden" name="funtemporalnom" id="funtemporalnom" value="<?php echo $_POST['funtemporalnom']?>">
								</td>
							</tr>
							<tr>
								<td class="tamano01" >Inversi&oacute;n: </td>
								<td colspan="3"  valign="middle">
									<select name="inversion" id="inversion" onChange="validar();" style="width:100%;" class="tamano02">
										<option value="-1">Seleccione...</option>
										<?php
											$sqlr="SELECT * FROM conceptoscontables WHERE modulo = '2' AND tipo = 'IN' ORDER BY codigo";
											$resp = mysqli_query($linkbd,$sqlr);
											while ($row = mysqli_fetch_row($resp)) 
											{
												if($row[0] == $_POST['inversion'])
												{
													echo "<option value='$row[0]' SELECTED>$row[0] - $row[3] - $row[1]</option>";
													$_POST['inversionnom'] = "$row[0] - $row[3] - $row[1]";
												}
												else{echo "<option value='$row[0]'>$row[0] - $row[3] - $row[1]</option>";}
											}
										?>
									</select>
									<input type="hidden" name="inversionnom" id="inversionnom" value="<?php echo $_POST['inversionnom']?>">
								</td>
								<td class="tamano01" >Inversi&oacute;n Temporal: </td>
								<td colspan="3"  valign="middle">
									<select name="invtemporal" id="invtemporal" onChange="validar();" style="width:100%;" class="tamano02">
										<option value="-1">Seleccione...</option>
										<?php
											$sqlr="SELECT * FROM conceptoscontables WHERE modulo = '2' AND tipo = 'IT' ORDER BY codigo";
											$resp = mysqli_query($linkbd,$sqlr);
											while ($row = mysqli_fetch_row($resp)) 
											{
												if($row[0] == $_POST['invtemporal'])
												{
													echo "<option value='$row[0]' SELECTED>$row[0] - $row[3] - $row[1]</option>";
													$_POST['invtemporalnom'] = "$row[0] - $row[3] - $row[1]";
												}
												else{echo "<option value='$row[0]'>$row[0] - $row[3] - $row[1]</option>";}
											}
										?>
									</select>
									<input type="hidden" name="invtemporalnom" id="invtemporalnom" value="<?php echo $_POST['invtemporalnom']?>">
								</td>
							</tr>
							<tr>
								<td class="tamano01">Gastos Comercialización: </td>
								<td colspan="3">
									<select name="gastoscomer" id="gastoscomer" onChange="validar();" style="width:100%;" class="tamano02">
										<option value="-1">Seleccione...</option>
										<?php
											$sqlr="SELECT * FROM conceptoscontables WHERE modulo = '2' AND tipo = 'CP' ORDER BY codigo";
											$resp = mysqli_query($linkbd,$sqlr);
											while ($row = mysqli_fetch_row($resp))
											{
												if($row[0] == $_POST['gastoscomer'])
												{
													echo "<option value='$row[0]' SELECTED>$row[0] - $row[3] - $row[1]</option>";
													$_POST['gastoscomernom'] = "$row[0] - $row[3] - $row[1]";
												}
												else{echo "<option value='$row[0]'>$row[0] - $row[3] - $row[1]</option>";}
											}
										?>
									</select>
									<input type="hidden" name="gastoscomernom" id="gastoscomernom" value="<?php echo $_POST['gastoscomernom']?>">
								</td>
								<td class="tamano01">Temporal Gastos Comercialización: </td>
								<td colspan="3">
									<select name="gastoscomertem" id="gastoscomertem" onChange="validar();" style="width:100%;" class="tamano02">
										<option value="-1">Seleccione...</option>
										<?php
											$sqlr="SELECT * FROM conceptoscontables WHERE modulo = '2' AND tipo = 'CT' ORDER BY codigo";
											$resp = mysqli_query($linkbd,$sqlr);
											while ($row = mysqli_fetch_row($resp))
											{
												if($row[0] == $_POST['gastoscomertem'])
												{
													echo "<option value='$row[0]' SELECTED>$row[0] - $row[3] - $row[1]</option>";
													$_POST['gastoscomertemnom'] = "$row[0] - $row[3] - $row[1]";
												}
												else{echo "<option value='$row[0]'>$row[0] - $row[3] - $row[1]</option>";}
											}
										?>
									</select>
									<input type="hidden" name="gastoscomertemnom" id="gastoscomertemnom" value="<?php echo $_POST['gastoscomertemnom']?>">
								</td>
							</tr>
						</table>
					</div>
				</div>
				<div class="tab"  <?php if($_POST['swprovision'] != 'S'){echo "style='display: none;'";}?>>
					<input type="radio" id="tab-2" name="tabgroup1" value="2" <?php echo $check2;?> >
					<label id="clabel" for="tab-2">Provisiona</label>
					<div class="content" style="overflow:hidden;">
					<input type='hidden' name='elimina' id='elimina'/>
						<?php
							if ($_POST['oculto'] == '3'){
								$posi = $_POST['elimina'];
								unset($_POST['codprovi2'][$posi]);
								unset($_POST['nomprovi2'][$posi]);
								$_POST['codprovi2'] = array_values($_POST['codprovi2']);
								$_POST['nomprovi2'] = array_values($_POST['nomprovi2']);
								$_POST['elimina'] = '';
							}
							if($_POST['oculto'] == '4'){
								$sqlr = "SELECT nombre FROM ccpethumvariables WHERE codigo = '".$_POST['variablepago']."'";
								$resp = mysqli_query($linkbd,$sqlr);
								$row = mysqli_fetch_row($resp);
								$_POST['codprovi2'][] = $_POST['variablepago'];
								$_POST['nomprovi2'][] = $row[0];
							}
							for ($x = 0; $x < count($_POST['codprovi2']); $x++){
								echo "
								<input type='hidden' name='codprovi2[]' value='".$_POST['codprovi2'][$x]."'/>
								<input type='hidden' name='nomprovi2[]' value='".$_POST['nomprovi2'][$x]."'/>";
							}
						?>
						<table class="inicio ancho">
							<tr>
								<td class="titulos" colspan="7">:: Seleccionar</td>
								<td class="cerrar" style="width:7%" onClick="location.href='hum-principal.php'">Cerrar</td>
							</tr>
							<tr>
								<td class="saludo1" style="width:2cm;">Variable:</td>
								<td colspan="4">
									<select name="variablepago" id="variablepago" class="tamano02" style="width:100%;">
										<option value="-1">Seleccione ....</option>
										<?php
											$sqlr = "SELECT codigo, nombre FROM ccpethumvariables WHERE estado = 'S'";
											$resp = mysqli_query($linkbd,$sqlr);
											while ($row = mysqli_fetch_row($resp)){
												if(in_array($row[0], $_POST['codprovi2'])){
													$vartip = "S";
												} else {
													$vartip = "N";
												}
												if($_POST['variablepago'] == $row[0]){
													if($vartip == "N"){
														echo "<option value='$row[0]' SELECTED>$row[0] - $row[1]</option>";
														
													}
												} else {
													if($vartip == "N"){
														echo "<option value='$row[0]' >$row[0] - $row[1]</option>";
													}
												}
											}
										?>
									</select>
								</td>
								<td style="height:35px;"><em class="botonflechaverde" onClick="fagregar();">Agragar</em></td>
							</tr>
						</table>
						<table class='tablamv' style="margin-left: 0.2%;">
							<thead>
								<tr style="text-align:left;">
									<th class="titulos">Resultados Busqueda:</th>
								</tr>
								<tr style="text-align:Center;">
									<th class="titulosnew00" style="width:7%;">código</th>
									<th class="titulosnew00">Nombre</th>
									<th class="titulosnew00" style="width:5%;">Eliminar</th>
								</tr>
							</thead>
							<tbody style="max-height: 48.5vh;">
								<?php
									$iter = 'saludo1a';
									$iter2 = 'saludo2';
									for ($x = 0; $x < count($_POST['codprovi2']); $x++){
										echo "
										<tr class='$iter'>
											<td style='text-align:center; width:7%;'>".$_POST['codprovi2'][$x]."</td>
											<td >".$_POST['nomprovi2'][$x]."</td>
											<td style='text-align:center; width:7%;'><img src='imagenes/del.png' onclick='eliminar($x)' class='icoop'></td>
										</tr>";
										$aux = $iter;
										$iter = $iter2;
										$iter2 = $aux;
									}
								?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<input type="hidden" name="maximo" id="maximo" value="<?php echo $_POST['maximo']?>"/>
			<input type="hidden" name="minimo" id="minimo" value="<?php echo $_POST['minimo']?>"/>
			<input type="hidden" name="oculto" id="oculto"  value="1">
			<input type="hidden" name="condeta" id="condeta" value="<?php echo $_POST['condeta'];?>"/>
			<?php
				if($_POST['oculto'] == '2'){
					$_POST['oculto'] = '1';
					if ($_POST['nombre'] != ""){
						$nr = "1";
						if($_POST['swparafiscal'] == "" || $_POST['swparafiscal'] == "N"){
							$valswparafiscal = 'N';
						} else {
							$valswparafiscal = 'S';
						}
						if($_POST['swprovision'] == "" || $_POST['swprovision'] == "N"){
							$valswprovision = 'N';
						} else { 
							$valswprovision = 'S';
						}
						if($_POST['swsalud'] == "" || $_POST['swsalud'] == "N"){
							$valswsalud = 'N';
						} else{
							$valswsalud = 'S';
						}
						if($_POST['swpension'] == "" || $_POST['swpension'] == "N"){
							$valswpesion = 'N';
						} else {
							$valswpesion = 'S';
						}
						if($_POST['swarl'] == "" || $_POST['swarl'] == "N"){
							$valswarl = 'N';
						} else { 
							$valswarl = 'S';
						}
						$sqlr = "UPDATE ccpethumvariables SET nombre = '".$_POST['nombre']."', pparafiscal = '$valswparafiscal', estado = 'S', provision = '$valswprovision', psalud = '$valswsalud', ppension = '$valswpesion', parl = '$valswarl', funcionamiento = '".$_POST['funcionamiento']."', funtemporal = '".$_POST['funtemporal']."', inversion = '".$_POST['inversion']."', invtemporal = '".$_POST['invtemporal']."', gastoscomerc = '".$_POST['gastoscomer']."', gastoscomerctemporal = '".$_POST['gastoscomertem']."' WHERE codigo = '".$_POST['codigo']."'";
						if (!mysqli_query($linkbd,$sqlr)){
							echo "
							<script>
								Swal.fire({
									icon: 'error',
									title: 'Error!',
									text: 'Manejador de Errores de la Clase BD ccpethumvariables, No se pudo ejecutar la petición',
									confirmButtonText: 'Continuar',
									confirmButtonColor: '#FF121A',
									timer: 2500
								});
							</script>";
						}
						$sqlr = "DELETE FROM ccpethumvariables_pro WHERE codigo1 = '".$_POST['codigo']."'";
						mysqli_query($linkbd,$sqlr);
						for ($x = 0; $x < count($_POST['codprovi2']); $x++){
							$sqlr = "INSERT INTO ccpethumvariables_pro (codigo1, codigo2, estado) VALUES ('".$_POST['codigo']."', '".$_POST['codprovi2'][$x]."', 'S')";
							mysqli_query($linkbd,$sqlr);
						}
						echo "
						<script>
							Swal.fire({
								icon: 'success',
								title: 'se guardo exitosamente',
								showConfirmButton: true,
								confirmButtonText: 'Continuar',
								confirmButtonColor: '#01CC42',
								timer: 3500
							});
						</script>";
					} else {
						echo "
						<script>
							Swal.fire({
								icon: 'error',
								title: 'Error!',
								text: 'Falta informacion para Crear la Variable',
								confirmButtonText: 'Continuar',
								confirmButtonColor: '#FF121A',
								timer: 2500
							});
						</script>";
					}
				}
			?>
		</form>
	</body>
</html>