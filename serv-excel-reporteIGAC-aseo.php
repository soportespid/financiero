<?php
	require_once 'PHPExcel/Classes/PHPExcel.php';
    include '/PHPExcel/Classes/PHPExcel/IOFactory.php';
	require "comun.inc";
	require "funciones.inc";
    require "funcionesSP.inc.php";
    ini_set('max_execution_time',99999999);
    header("Content-type: application/json");
	session_start();
	$linkbd=conectar_v7();

	$objPHPExcel = new PHPExcel();
	
	$objPHPExcel->getProperties()
		->setCreator("IDEAL10")
		->setLastModifiedBy("IDEAL10")
		->setTitle("Reporte de recaudos por periodo liquidado")
		->setSubject("SP")
		->setDescription("SP")
		->setKeywords("SP")
		->setCategory("SERVICIOS PUBLICOS");
	// $objPHPExcel->getActiveSheet()->mergeCells('A1:BA1');
	$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A1', "Reporte IGAC aseo periodo $_POST[corte]");
	$objFont=$objPHPExcel->getActiveSheet()->getStyle('A1')->getFont();
	$objFont->setName('Courier New');
	$objFont->setSize(15);
	$objFont->setBold(true);
	$objFont->setUnderline(PHPExcel_Style_Font::UNDERLINE_SINGLE);
	$objFont->getColor()->setARGB( PHPExcel_Style_Color::COLOR_BLACK);
	$objAlign=$objPHPExcel->getActiveSheet()->getStyle('A1')->getAlignment(); 
	$objAlign->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
	$objAlign->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
	$objPHPExcel-> getActiveSheet ()
		-> getStyle ("A2:BA2")
		-> getFill ()
		-> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
		-> getStartColor ()
		-> setRGB ('A6E5F3');
	$objPHPExcel-> getActiveSheet ()
		-> getStyle ("A1")	
		-> getFill ()
		-> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
		-> getStartColor ()
		-> setRGB ('A6E5F3');
	$borders = array(
		'borders' => array(
			'allborders' => array(
				'style' => PHPExcel_Style_Border::BORDER_THIN,
				'color' => array('argb' => 'FF000000'),
			)
		),
	);

    $periodoId = $_POST["corte"];
    $reporte = [];
    
    $sqlCorte = "SELECT fecha_impresion, fecha_inicial, TIMESTAMPDIFF(DAY, fecha_inicial, fecha_final), version_tarifa FROM srvcortes WHERE numero_corte = $periodoId";
    $resCorte = mysqli_query($linkbd,$sqlCorte);
    $rowCorte = mysqli_fetch_row($resCorte);
    
    $fechaExpedicion = date('d-m-Y',strtotime($rowCorte[0]));
    $fechaInicio = date('d-m-Y',strtotime($rowCorte[1]));
    $diasFacturados = $rowCorte[2];
    $version = $rowCorte[3];

    //componentes de aseo
    $sqlComponent = "SELECT * FROM srv_componentes_igac_aseo WHERE id = $version ORDER BY id DESC LIMIT 1";
    $rowComponent = mysqli_fetch_assoc(mysqli_query($linkbd, $sqlComponent));

    $sqlUsuarios = "SELECT CD.id_cliente AS clienteId, C.cod_usuario AS codUsuario, C.cod_catastral AS codCatastral, DC.direccion AS direccion, B.nombre AS nombreBarrio, CD.numero_facturacion AS numFactura, E.cod_clase_uso AS codClaseUso, CD.estado_pago AS estadoPago, B.centro_poblado AS centroPoblado, C.hogar_comunitario AS hogarComunitario, B.id_ubicacion AS ubicacion FROM srvcortes_detalle AS CD INNER JOIN srvclientes AS C ON CD.id_cliente = C.id INNER JOIN srvdireccion_cliente AS DC ON CD.id_cliente = DC.id_cliente INNER JOIN srvbarrios AS B ON C.id_barrio = B.id INNER JOIN srvestratos AS E ON C.id_estrato = E.id WHERE CD.id_corte = $periodoId ORDER BY CD.numero_facturacion ASC";
    $resUsuarios = mysqli_query($linkbd, $sqlUsuarios);
    while ($rowUsuarios = mysqli_fetch_assoc($resUsuarios)) {
        
        $datos = [];
    
        $sqlAsigServ = "SELECT estado_medidor, id_estrato FROM srvasignacion_servicio WHERE id_clientes = $rowUsuarios[clienteId] AND id_servicio = 3";
        $rowAsigServ = mysqli_fetch_assoc(mysqli_query($linkbd, $sqlAsigServ));

        $sqlNovedad = "SELECT TN.estado_medidor AS estadoMedidor FROM srv_asigna_novedades AS SN, srv_tipo_observacion AS TN WHERE SN.corte = $periodoId AND SN.id_cliente = $rowUsuarios[clienteId] AND SN.estado = 'S' AND SN.codigo_observacion = TN.codigo_observacion";
        $rowNovedad = mysqli_fetch_assoc(mysqli_query($linkbd, $sqlNovedad));

        if ($rowNovedad["estadoMedidor"] == 1) {
            $determinacionConsumo = 1;
        }
        else {
            $determinacionConsumo = 2;
        }
    
        $sqlLecturaActual = "SELECT COALESCE(lectura_medidor, '0') AS lectura_medidor, COALESCE(consumo, 0) AS consumo FROM srvlectura WHERE corte = $periodoId AND id_cliente = $rowUsuarios[clienteId] AND id_servicio = 3";
        $rowLecturaActual = mysqli_fetch_assoc(mysqli_query($linkbd,$sqlLecturaActual));
    
        $sqlLecturaAnterior = "SELECT COALESCE(lectura_medidor, 0) AS lectura_medidor FROM srvlectura WHERE corte = $periodoId-1 AND id_cliente = $rowUsuarios[clienteId] AND id_servicio = 3";
        $rowLecturaAnterior = mysqli_fetch_assoc(mysqli_query($linkbd, $sqlLecturaAnterior));
    
        $sqlFactura = "SELECT cargo_f, consumo_b, consumo_c, consumo_s, subsidio_cf, subsidio_cb, subsidio_cc, subsidio_cs, contribucion_cf, contribucion_cb, contribucion_cc, contribucion_cs, deuda_anterior, interes_mora, venta_medidor, acuerdo_pago FROM srvfacturas WHERE num_factura = $rowUsuarios[numFactura] AND id_servicio = 3";
        $rowFactura = mysqli_fetch_assoc(mysqli_query($linkbd, $sqlFactura));
    
        $valorConsumo = $rowFactura["consumo_b"] + $rowFactura["consumo_c"] + $rowFactura["consumo_s"];
        $valorSubsidiado = $rowFactura["subsidio_cf"] + $rowFactura["subsidio_cb"] + $rowFactura["subsidio_cc"] + $rowFactura["subsidio_cs"];
        $valorContribucion = $rowFactura["contribucion_cf"] + $rowFactura["contribucion_cb"] + $rowFactura["contribucion_cc"] + $rowFactura["contribucion_cs"];

        $toneladasResiduosNoAprovechables = $tarifaRecoleccionTransporte = $tarifaDisposicionFinal = $tarifaComercializacion = $tarifaBarrido = 0;

        if ($rowFactura["cargo_f"] > 0) {
            if ($rowComponent["tc"] != "") {
                $tarifaComercializacion = $rowComponent["tc"];
                $tarifaBarrido = $rowComponent["tb"];
            }
            else {
                $tarifaComercializacion = $rowFactura["cargo_f"];
                $tarifaBarrido = 0;
            }
        }

        if ($valorConsumo > 0) {
            if ($rowComponent["trn"] != "") {
                $toneladasResiduosNoAprovechables = $rowComponent["trn"];
                $tarifaRecoleccionTransporte = $rowComponent["trt"];
                $tarifaDisposicionFinal = $rowComponent["tdf"];            
            }
            else {
                $toneladasResiduosNoAprovechables = $valorConsumo;
                $tarifaRecoleccionTransporte = $tarifaDisposicionFinal = 0;
            }
        }
     
        $sqlTarifa = "SELECT costo_unidad, subsidio, contribucion FROM srvcostos_estandar WHERE id_estrato = $rowAsigServ[id_estrato] AND id_servicio = 3 AND version = $version LIMIT 1";
        $rowTarifa = mysqli_fetch_assoc(mysqli_query($linkbd, $sqlTarifa));

        if ($rowTarifa["subsidio"] > 0) {
            $factorSubCont = $rowTarifa["subsidio"] * -1;
            $valorSubCont = $valorSubsidiado * -1;
        }
        else if ($rowTarifa["contribucion"] > 0) {
            $factorSubCont = $rowTarifa["contribucion"];
            $valorSubCont = $valorContribucion;
        }
        else {
            $factorSubCont = $valoSubCont = 0;
        }

        $factorSubCont = $factorSubCont / 100;
    
        $sqlUltPago = "SELECT id FROM srvcortes_detalle WHERE id_cliente = $rowUsuarios[clienteId] AND estado_pago = P ORDER BY id DESC LIMIT 1";
        $resUltPago = mysqli_query($linkbd, $sqlUltPago);
        $rowUltPago = mysqli_fetch_row($resUltPago); 
    
        if ($rowUltPago[0] <> "") {
    
            $sqlMora = "SELECT estado_pago FROM srvcortes_detalle WHERE id_cliente = $rowUsuarios[clienteId] AND estado_pago = 'V' AND id > $rowUltPago[0]";
            $resMora = mysqli_query($linkbd, $sqlMora);
            $mesesVencidos = mysqli_num_rows($resMora);
            $diasMora = 30 * $mesesVencidos;
        }
        else {
            $sqlMora = "SELECT estado_pago FROM srvcortes_detalle WHERE id_cliente = $rowUsuarios[clienteId] AND estado_pago = 'V'";
            $resMora = mysqli_query($linkbd, $sqlMora);
            $mesesVencidos = mysqli_num_rows($resMora);
            $diasMora = 30 * $mesesVencidos;
        }
    
        // $valorServicio = valorServicioIGAC($rowUsuarios["numFactura"], 1);
        $deudaAnterior = $rowFactura["deuda_anterior"] + $rowFactura["acuerdo_pago"];
        $valorMes = $tarifaComercializacion + $tarifaBarrido + $tarifaRecoleccionTransporte + $tarifaDisposicionFinal + $valorSubCont;
        $valorTotal = $valorMes + $deudaAnterior + $rowFactura["interes_mora"];
    
        if ($rowUsuarios["estadoPago"] == "P") {
            $valorPago = $valorTotal;
        }
        else {
            $valorPago = 0;
        }
    
        if ($rowLecturaAnterior["lectura_medidor"] == "") { $lecturaAnterior = 0;} else {$lecturaAnterior = $rowLecturaAnterior["lectura_medidor"];}
        if ($rowLecturaActual["lectura_medidor"] == "") { $lectura = 0;} else {$lectura = $rowLecturaActual["lectura_medidor"];}
        if ($rowLecturaActual["consumo"] == "") { $consumo = 0;} else {$consumo = $rowLecturaActual["consumo"];}

        if ($rowUsuarios["codCatastral"] != "" || strlen($rowUsuarios["codCatastral"]) >= 15) {$infoPredial = 1;} else {$infoPredial = 3;}
        if ($rowUsuarios["ubicacion"] == 1) {$ubicacion = 2;} else {$ubicacion = 1;}

        if ($rowUsuarios["codClaseUso"] <= 6) {
            $codFactorProduccion = $rowUsuarios["codClaseUso"];
        } 
        else if ($rowUsuarios["codClaseUso"] >= 7){
            $codFactorProduccion = 7;
        }

        if ($rowUsuarios["codClaseUso"] >= 1 && $rowUsuarios["codClaseUso"] <= 3) {
            $valueFactorProduccion = 0.95;
        }
        else if ($rowUsuarios["codClaseUso"] == 4) {
            $valueFactorProduccion = 1.0;
        }
        else if ($rowUsuarios["codClaseUso"] == 5) {
            $valueFactorProduccion = 1.09;
        }
        else if ($rowUsuarios["codClaseUso"] == 6) {
            $valueFactorProduccion = 1.54;
        }
        else if ($rowUsuarios["codClaseUso"] >= 10 && $rowUsuarios["codClaseUso"] <= 12) {
            $valueFactorProduccion = 3.12;
        }

        if ($valorConsumo == 0) {
            $codFactorProduccion = 9;
            $valueFactorProduccion = 0;
        }

        $datos[1] = $rowUsuarios["codUsuario"]; //NUID
        $datos[2] = $rowUsuarios["codUsuario"]; //Num cuenta
        $datos[3] = substr($rowUsuarios["centroPoblado"], 0, 2); //cod departamental
        $datos[4] = substr($rowUsuarios["centroPoblado"], 2, 3); //cod municipio
        $datos[5] = substr($rowUsuarios["centroPoblado"], 5, 3); //cod centro poblado
        $datos[6] = $infoPredial; //Inf predial 1 = tiene cod catastral y 3 si no tiene
        $datos[7] = $rowUsuarios["codCatastral"]; //cod catastral
        $datos[8] = $rowUsuarios["direccion"];
        $datos[9] = $fechaExpedicion;
        $datos[10] = 3; //metologia tarifaria
        $datos[11] = 45019; //NUAP;
        $datos[12] = $fechaInicio;
        $datos[13] = $rowUsuarios["numFactura"];
        $datos[14] = ""; //multiusuario
        $datos[15] = 0; //causal refacturación
        $datos[16] = ""; //numero de refacturacion
        $datos[17] = $ubicacion; //1. rural y 2. urbano
        $datos[18] = $rowUsuarios["codClaseUso"];
        $datos[19] = 1; //tipo de aforo
        $datos[20] = 1; //recoleccion puerta a puerta;
        $datos[21] = $codFactorProduccion;
        $datos[22] = $valueFactorProduccion; //valor factor de produccion
        $datos[23] = 2; //inquilinato
        $datos[24] = 2; //vivienda de interes social
        $datos[25] = $rowUsuarios["hogarComunitario"];
        $datos[26] = $diasFacturados;
        $datos[27] = $toneladasResiduosNoAprovechables;
        $datos[28] = ""; //trlu
        $datos[29] = 0.0428; //trlb
        $datos[30] = ""; //toneladas aprovechados
        $datos[31] = ""; //toneladas de rechazo
        $datos[32] = ""; //facturacion conjunta
        $datos[33] = $tarifaComercializacion; //tarifa de comercializacion
        $datos[34] = ""; //tlu
        $datos[35] = $tarifaBarrido; //tarifa de barrido
        $datos[36] = $tarifaRecoleccionTransporte; //tarifa de recoleccion y transporte
        $datos[37] = 0; //tte
        $datos[38] = $tarifaDisposicionFinal; //tarifa de disposicion final
        $datos[39] = ""; //ttl
        $datos[40] = ""; //ta
        $datos[41] = $factorSubCont; //subsidio
        $datos[42] = round($valorSubCont, 2);
        $datos[43] = round($valorMes, 2); //suma de tarifa de comercializacion+tlu+tarifa de barrido+recoleccion y transporte+ tarifa disposicion final - valor subsidio
        $datos[44] = round($deudaAnterior, 2); //deuda
        $datos[45] = round($rowFactura["interes_mora"], 2); //intereses
        $datos[46] = ""; //descuento por calidad
        $datos[47] = ""; //descuento por pqr
        $datos[48] = ""; //dto por compactacion
        $datos[49] = ""; //devolucion por cobros no autorizados
        $datos[50] = ""; //devoluciones pendientes
        $datos[51] = ""; //otras devoluciones
        $datos[52] = round($valorTotal, 2); //total
        $datos[53] = round($valorPago, 2); //recaudo
    
        array_push($reporte, $datos);
    }

	$objPHPExcel->getActiveSheet()->getStyle('A2:BA2')->applyFromArray($borders);
	$objPHPExcel->setActiveSheetIndex(0)
    ->setCellValue('A2', '1')
    ->setCellValue('B2', '2')
    ->setCellValue('C2', '3')
    ->setCellValue('D2', '4')
    ->setCellValue('E2', '5')
    ->setCellValue('F2', '6')
    ->setCellValue('G2', '7')
    ->setCellValue('H2', '8')
    ->setCellValue('I2', '9')
    ->setCellValue('J2', '10')
    ->setCellValue('K2', '11')
    ->setCellValue('L2', '12')
    ->setCellValue('M2', '13')
    ->setCellValue('N2', '14')
    ->setCellValue('O2', '15')
    ->setCellValue('P2', '16')
    ->setCellValue('Q2', '17')
    ->setCellValue('R2', '18')
    ->setCellValue('S2', '19')
    ->setCellValue('T2', '20')
    ->setCellValue('U2', '21')
    ->setCellValue('V2', '22')
    ->setCellValue('W2', '23')
    ->setCellValue('X2', '24')
    ->setCellValue('Y2', '25')
    ->setCellValue('Z2', '26')
    ->setCellValue('AA2', '27')
    ->setCellValue('AB2', '28')
    ->setCellValue('AC2', '29')
    ->setCellValue('AD2', '30')
    ->setCellValue('AE2', '31')
    ->setCellValue('AF2', '32')
    ->setCellValue('AG2', '33')
    ->setCellValue('AH2', '34')
    ->setCellValue('AI2', '35')
    ->setCellValue('AJ2', '36')
    ->setCellValue('AK2', '37')
    ->setCellValue('AL2', '38')
    ->setCellValue('AM2', '39')
    ->setCellValue('AN2', '40')
    ->setCellValue('AO2', '41')
    ->setCellValue('AP2', '42')
    ->setCellValue('AQ2', '43')
    ->setCellValue('AR2', '44')
    ->setCellValue('AS2', '45')
    ->setCellValue('AT2', '46')
    ->setCellValue('AU2', '47')
    ->setCellValue('AV2', '48')
    ->setCellValue('AW2', '49')
    ->setCellValue('AX2', '50')
    ->setCellValue('AY2', '51')
    ->setCellValue('AZ2', '52')
    ->setCellValue('BA2', '53');   
	$i=3;
    
	foreach ($reporte as $key => $report) {

		$objPHPExcel->setActiveSheetIndex(0)
		->setCellValueExplicit ("A$i", $report[1], PHPExcel_Cell_DataType :: TYPE_STRING)
		->setCellValueExplicit ("B$i", $report[2], PHPExcel_Cell_DataType :: TYPE_STRING)
		->setCellValueExplicit ("C$i", $report[3], PHPExcel_Cell_DataType :: TYPE_STRING)
		->setCellValueExplicit ("D$i", $report[4], PHPExcel_Cell_DataType :: TYPE_STRING)
        ->setCellValueExplicit ("E$i", $report[5], PHPExcel_Cell_DataType :: TYPE_STRING)
        ->setCellValueExplicit ("F$i", $report[6], PHPExcel_Cell_DataType :: TYPE_STRING)
        ->setCellValueExplicit ("G$i", $report[7], PHPExcel_Cell_DataType :: TYPE_STRING)
        ->setCellValueExplicit ("H$i", $report[8], PHPExcel_Cell_DataType :: TYPE_STRING)
        ->setCellValueExplicit ("I$i", $report[9], PHPExcel_Cell_DataType :: TYPE_STRING)
        ->setCellValueExplicit ("J$i", $report[10], PHPExcel_Cell_DataType :: TYPE_STRING)
        ->setCellValueExplicit ("K$i", $report[11], PHPExcel_Cell_DataType :: TYPE_STRING)
        ->setCellValueExplicit ("L$i", $report[12], PHPExcel_Cell_DataType :: TYPE_STRING)
        ->setCellValueExplicit ("M$i", $report[13], PHPExcel_Cell_DataType :: TYPE_STRING)
        ->setCellValueExplicit ("N$i", $report[14], PHPExcel_Cell_DataType :: TYPE_STRING)
        ->setCellValueExplicit ("O$i", $report[15], PHPExcel_Cell_DataType :: TYPE_STRING)
        ->setCellValueExplicit ("P$i", $report[16], PHPExcel_Cell_DataType :: TYPE_STRING)
        ->setCellValueExplicit ("Q$i", $report[17], PHPExcel_Cell_DataType :: TYPE_STRING)
        ->setCellValueExplicit ("R$i", $report[18], PHPExcel_Cell_DataType :: TYPE_STRING)
        ->setCellValueExplicit ("S$i", $report[19], PHPExcel_Cell_DataType :: TYPE_STRING)
        ->setCellValueExplicit ("T$i", $report[20], PHPExcel_Cell_DataType :: TYPE_STRING)
        ->setCellValueExplicit ("U$i", $report[21], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
        ->setCellValueExplicit ("V$i", $report[22], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
        ->setCellValueExplicit ("W$i", $report[23], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
        ->setCellValueExplicit ("X$i", $report[24], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
        ->setCellValueExplicit ("Y$i", $report[25], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
        ->setCellValueExplicit ("Z$i", $report[26], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
        ->setCellValueExplicit ("AA$i", $report[27], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
        ->setCellValueExplicit ("AB$i", $report[28], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
        ->setCellValueExplicit ("AC$i", $report[29], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
        ->setCellValueExplicit ("AD$i", $report[30], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
        ->setCellValueExplicit ("AE$i", $report[31], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
        ->setCellValueExplicit ("AF$i", $report[32], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
        ->setCellValueExplicit ("AG$i", $report[33], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
        ->setCellValueExplicit ("AH$i", $report[34], PHPExcel_Cell_DataType :: TYPE_STRING)
        ->setCellValueExplicit ("AI$i", $report[35], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
        ->setCellValueExplicit ("AJ$i", $report[36], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
        ->setCellValueExplicit ("AK$i", $report[37], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
        ->setCellValueExplicit ("AL$i", $report[38], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
        ->setCellValueExplicit ("AM$i", $report[39], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
        ->setCellValueExplicit ("AN$i", $report[40], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
        ->setCellValueExplicit ("AO$i", $report[41], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
        ->setCellValueExplicit ("AP$i", $report[42], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
        ->setCellValueExplicit ("AQ$i", $report[43], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
        ->setCellValueExplicit ("AR$i", $report[44], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
        ->setCellValueExplicit ("AS$i", $report[45], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
        ->setCellValueExplicit ("AT$i", $report[46], PHPExcel_Cell_DataType :: TYPE_STRING)
        ->setCellValueExplicit ("AU$i", $report[47], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
        ->setCellValueExplicit ("AV$i", $report[48], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
        ->setCellValueExplicit ("AW$i", $report[49], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
        ->setCellValueExplicit ("AX$i", $report[50], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
        ->setCellValueExplicit ("AY$i", $report[51], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
        ->setCellValueExplicit ("AZ$i", $report[52], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
        ->setCellValueExplicit ("BA$i", $report[53], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
        ;

        $objPHPExcel->getActiveSheet()->getStyle("A$i:BA$i")->applyFromArray($borders);

        $i++;
	}

	$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->setTitle('SP');
	//header('Content-Type: application/vnd.ms-excel');
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
	header('Content-Disposition: attachment;filename="reporteIgacAseo.xlsx"');
	header('Cache-Control: max-age=0');
	$objWriter=PHPExcel_IOFactory::createWriter($objPHPExcel,'Excel2007');
	$objWriter->save('php://output');
	exit;
?>