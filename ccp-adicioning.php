<?php
	require "comun.inc";
	require "funciones.inc";
	require "validaciones.inc";
	session_start();
	$linkbd = conectar_v7();	
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	header("Cache-control: private"); // Arregla IE 6
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es">
	<head>
	 	<meta http-equiv="Content-Type" content="text/html" charset="iso-8859-1"/>
        <meta http-equiv="X-UA-Compatible" content="IE=9"/>
		<title>:: IDEAL10 - Presupuesto CCPET</title>
        <link href="css/css2.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
        <script type="text/javascript" src="css/calendario.js"></script>
		<script type="text/javascript" src="css/programas.js"></script>		
		<style>
			.message{
				font-family: 'Open Sans', sans-serif;
				font-size: 12px;
				font-weight: bold;
				font-color: black;
				background-color:white;
			}
		</style>
<script>
//Ver reporte

	function verep(idfac)
	{
	document.form1.oculto.value = idfac;
	document.form1.submit();
	}
  
	//Genera reporte
	function genrep(idfac)
	{
		document.form2.oculto.value=idfac;
		document.form2.submit();
	}

  	function despliegamodalm(_valor,_tip,mensa,pregunta)
	{
		document.getElementById("bgventanamodalm").style.visibility = _valor;

		if(_valor == "hidden")
		{
			document.getElementById('ventanam').src='';
		}

		else
		{
			switch(_tip)
			{
				case "1":
					document.getElementById('ventanam').src="ventana-mensaje1.php?titulos="+mensa;
					break;
				case "2":
					document.getElementById('ventanam').src="ventana-mensaje3.php?titulos="+mensa;
					break;
				case "3":
					document.getElementById('ventanam').src="ventana-mensaje2.php?titulos="+mensa;
					break;
				case "4":
					document.getElementById('ventanam').src="ventana-consulta1.php?titulos="+mensa+"&idresp="+pregunta;
					break;	
			}
		}
	}

	function despliegamodal(_valor)
	{
		document.getElementById("bgventanamodal2").style.visibility=_valor;

		if(_valor=="hidden")
		{
			document.getElementById('ventana2').src="";
		}
		else 
		{
			document.getElementById('ventana2').src="fuentes-ventana.php?";
		}
	}

	function despliegamodal2(_valor)
	{
		document.getElementById("bgventanamodal2").style.visibility=_valor;
		var _tip =  document.getElementById("tipocta").value;

		if(_valor == 'hidden')
		{
			document.getElementById('ventana2').src="";
		}
		else
		{
			document.getElementById('ventana2').src="cuentasccpet-ventana.php?ti="+_tip;
		}

	}

	function modalCuentaClasificadora(_valor)
	{
		var clasi =  document.getElementById("clasi").value;
		document.getElementById("bgventanamodal2").style.visibility=_valor;
		if(_valor=="hidden")
		{
			document.getElementById('ventana2').src="";
		}
		else 
		{
			switch(clasi)
				{
					case "1":
						document.getElementById('ventana2').src="adicionCuin-ventana.php";
						break;
					case "2":
						document.getElementById('ventana2').src="bienesTransportables-ventana.php";
						break;
					case "3":
						document.getElementById('ventana2').src="adicionServicios-ventana.php";
						break;
					case "0":
						despliegamodalm('visible','2','No existe clasificador');
						break;
				}
		}
	}

	function respuestaconsulta(pregunta)
	{
		switch(pregunta)
		{
			case "1":	
				document.form2.oculto.value = 2;
				document.form2.submit();
			break;
		}
	}

	function funcionmensaje()
	{
		var _cons = document.getElementById('acuerdo').value;
		document.location.href = "ccp-adicioningver.php?idegre="+_cons+"&scrtop=0&numpag=1&limreg=10&filtro1=&filtro2=";
	}

	//Genera reporte
	function guardar()
	{

		var fechabloqueo   = document.form2.fechabloq.value;
		var fechadocumento = document.form2.fecha.value;
		var nuevaFecha     = fechadocumento.split("/");
		var fechaCompara   = nuevaFecha[2] + "-" + nuevaFecha[1] + "-" + nuevaFecha[0];
		var restric 	   = document.form2.restric.value;
		var diferencia 	   = document.form2.diferencia.value;

		if(restric!=1)
		{
			/* if(diferencia == 0)
			{ */
				if((Date.parse(fechabloqueo)) > (Date.parse(fechaCompara)))
				{
					despliegamodalm('visible','2','Fecha de documento menor que fecha de bloqueo');
				}

				else
				{
					var vigencia = "<?php echo vigencia_usuarios($_SESSION['cedulausu']) ?>";

					if(vigencia==nuevaFecha[2])
					{
						if(document.form2.vigencia.value!='' && document.form2.fecha.value!='' && document.form2.acuerdo.value!='-1' && document.form2.acuerdo.value!='')
						{
							despliegamodalm('visible','4','Esta Seguro de Guardar','1');
						}

						else
						{
							despliegamodalm('visible','2','Faltan datos para completar el registro');
						}
		
					}

					else
					{
						despliegamodalm('visible','2','La fecha del documento debe coincidir con su vigencia');
					}
				}
			/* }
			else
			{
				despliegamodalm('visible','2','La diferencia Tiene que ser igual a 0');
			} */	
		}
		else
		{
			despliegamodalm('visible','2','Algunos rubros sobrepasan el saldo permitido');
		}
	}

	function validar(formulario)
	{
		document.form2.action = "ccp-adicioning.php";
		document.form2.submit();
	}

	function validar2(formulario)
	{
		var compro   = document.form2.tipomov.value;
		var regalias = document.form2.regalias.checked;

		document.form2.bc.value = '4';
		document.form2.submit();

		if(compro == '1')
		{
			document.form2.chacuerdo.value=2;
			document.form2.action="ccp-adicioning.php";
			document.form2.submit();
		}

		else if(compro == '2')
		{
			if(regalias)
			{
				alert("No puede aplicar reduccion a rubros de regalias");
				document.form2.tipomov.value="-1";
			}

			else
			{
				document.form2.chacuerdo.value=2;
				document.form2.action="ccp-adicioning.php";
				document.form2.submit();
			}
		}
	}

	function buscacta(e)
	{
		if(document.form2.cuenta.value != '')
		{
			document.form2.bc.value = '1';
			document.form2.submit();
		}
	}

	function buscaFuente(e)
	{
		if(document.form2.fuente.value != '')
		{
			document.form2.bc.value = '2';
			document.form2.submit();
		}
	}

	function buscaClasificador(e)
	{
		if(document.form2.clasificador.value != '')
		{
			document.form2.bc.value = '3';
			document.form2.submit();
		}
	}

	function agregardetalle()
	{
		vc = document.form2.valorac2.value;

		if(document.form2.cuenta.value != '' && document.form2.tipomov.value != '' && document.form2.tipocta.value != '' && parseFloat(document.form2.valor.value) > 0  && document.form2.debicred.value != '-1' && document.form2.fuente.value != '')
		{ 
			tipoc=document.form2.tipocta.value;
			switch (tipoc)
			{
				case '1':
					suma  = parseFloat(document.form2.valor.value)+parseFloat(document.form2.cuentaing2.value);
					saldo = parseFloat(document.form2.valor2.value);
					valor = parseFloat(document.form2.valor.value);

					if(suma <= vc)
					{
						document.form2.agregadet.value = 1;
						document.form2.submit();
					}

					else
					{
						alert("El Valor supera el Acto Administrativo: " + suma);
					}
				break;

				case '2':
					suma = parseFloat(document.form2.valor.value) + parseFloat(document.form2.cuentagas2.value);

					if(suma <= vc)
					{
						document.form2.agregadet.value = 1;
						document.form2.submit();
					}

					else
					{
						despliegamodalm('visible','2',"El Valor supera el saldo del rubro: " + suma);
					}
				break;
			}
		}

		else
		{
			despliegamodalm('visible','2',"Falta informacion para poder Agregar");
		}
	}

	function eliminar(variable)
	{
		if(confirm("Esta Seguro de Eliminar"))
		{
			document.form2.elimina.value = variable;
			vvend = document.getElementById('elimina');
			vvend.value = variable;
			document.form2.submit();
		}
	}

	function finaliza()
	{
		if(document.form2.fin.checked)
		{
			if (parseFloat(document.form2.valorac2.value)==parseFloat(document.form2.cuentagas2.value) && parseFloat(document.form2.valorac2.value)==parseFloat(document.form2.cuentaing2.value))
			{
				if (confirm("Confirme Guardando el Documento, al completar el Proceso"))
				{
					document.form2.fin.checked=true; 
					document.form2.fin.value=1;
				} 
				else
				{
					document.form2.fin.checked=false; 
				}
			}
			else 
			{
				despliegamodalm('visible','2',"El Total del Acto Administrativo no es igual al de Ingresos y/o Gastos");
				
				document.form2.fin.checked=false; 
			}
		}

		else
		{
			document.form2.fin.checked = false; 
			document.form2.fin.value = 0;
		}
	}

	function capturaTecla(e)
	{ 
		var tcl = (document.all)?e.keyCode:e.which;

		if (tcl == 115)
		{
			alert(tcl);
			return tabular(e,elemento);
		}
	}

	function validaregalias()
	{
		var compro = document.form2.tipomov.value;

		if(compro == '2')
		{
			if(document.form2.regalias.checked)
			{
				alert("No puede aplicar reduccion a rubros de regalias");
				document.form2.regalias.checked = false;
			}
		}
	}

</script>

		<?php titlepag();?>

</head>

    <body>
        <IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
        <span id="todastablas2"></span>
        <table>
            <tr>
				<script>barra_imagenes("ccpet");</script><?php cuadro_titulos();?>
			</tr>

            <tr>
				<?php menu_desplegable("ccpet");?>
			</tr>

        	<tr>
         		<td colspan="5" class="cinta">
					<a href="ccp-adicioning.php" class="mgbt"><img src="imagenes/add.png" title="Nuevo"/></a>
					<a href="#"  onClick="guardar()" class="mgbt"><img src="imagenes/guarda.png" title="Guardar"/></a>
					<a href="ccp-buscaradicioning.php" class="mgbt"><img src="imagenes/busca.png" title="Buscar"/></a>
					<a href="#" class="mgbt" onClick="mypop=window.open('ccp-principal.php','','');mypop.focus();"><img src="imagenes/nv.png" title="Nueva Ventana"></a>
					<a href="menuAdicion.php" class="mgbt"><img src="imagenes/iratras.png" title="Atr&aacute;s"></a>
				</td>
        	</tr>
        </table>

		<?php
			$vigusu = vigencia_usuarios($_SESSION['cedulausu']); 
			$_POST['vigencia'] = vigencia_usuarios($_SESSION['cedulausu']); 
			$vigencia = $vigusu; 

			if(!$_POST['oculto'])
			{
					$fec = date("d/m/Y");

					$_POST['fecha'] 	 = $fec; 	
					$_POST['valor']   	 = 0; 			 
					$_POST['cuentaing']  = 0;
					$_POST['cuentagas']  = 0;
					$_POST['cuentaing2'] = 0;
					$_POST['cuentagas2'] = 0;
			}

			//busca cuenta
			if($_POST['bc'] == '1')
			{
				$nresul  = buscaNombreCuentaCCPET($_POST['cuenta'],$_POST['tipocta']);

				if($nresul!='')
				{
					$_POST['ncuenta']=utf8_decode($nresul);
			 	}
				else
				{
					$_POST['ncuenta']="";	
				}

				
			}

			if($_POST['bc'] == '2')
			{
				$nresul2 = buscaNombreFuenteCCPET($_POST['fuente']);
				
				if($nresul2 != '')
				{
					$_POST['nfuente']=utf8_decode($nresul2);
			 	}
				else
				{
					$_POST['nfuente']="";	
				}
			}

			if($_POST['bc'] == '3')
			{
				$nresul3 = buscaNombreClasificadorCCPET($_POST['clasificador'], $_POST['clasi']);
				
				if($nresul3 != '')
				{
					$_POST['nclasificador']=utf8_decode($nresul3);
			 	}
				else
				{
					$_POST['nclasificador']="";	
				}
			}
			
			if($_POST['chacuerdo'] == '2')
			{
				unset($_POST['dcuentas']);
				unset($_POST['dncuentas']);
				unset($_POST['dgastos']);		 		 		 		 		 						   			 			 		 
				unset($_POST['dingresos']);
				unset($_POST['debcred']);
				unset($_POST['dfuentes']);
				unset($_POST['dclasificador']);
				unset($_POST['dnumclasi']);

				$_POST['dcuentas']   	= array();
				$_POST['dncuentas']  	= array();
				$_POST['debcred']    	= array();
				$_POST['dclasificador'] = array();
				$_POST['dnumclasi']		= array();
				$_POST['dfuentes']	 	= array();
				$_POST['dingresos']  	= array();
				$_POST['dgastos']    	= array();
				$_POST['diferencia'] 	= 0;
				$_POST['cuentagas']  	= 0;
				$_POST['cuentaing']  	= 0;																			
			}	 

			if($_POST['bc'] == '4')
			{
				$cont = 0;

				$sqlr = "SELECT cuenta, tipo, mediopago, fuente, clasificador, cuenta_clasificadora, valor FROM ccpetadiciones WHERE id_acuerdo = '".$_POST['acuerdo']."' ";

				$resp = mysqli_query($linkbd, $sqlr);

				while($row = mysqli_fetch_row($resp))
				{
					$_POST['dcuentas'][]  	  = $row[0];
					$_POST['debcred'][]   	  = $row[2];
					$_POST['dfuentes'][]  	  = $row[3];
					$_POST['dnumclasi'][] 	  = $row[4];
					$_POST['dclasificador'][] = $row[5];

					if($row[1] == 'I')
					{
						$_POST['dingresos'][$cont] = $row[6];

						$_POST['dncuentas'][] = buscaNombreCuentaCCPET($row[0], 1);
					}
					if($row[1] == 'G')
					{
						$_POST['dgastos'][$cont] = $row[6];

						$_POST['dncuentas'][] = buscaNombreCuentaCCPET($row[0], 2);
					}

					$cont++;
				}
			}
		?>

		<div id="bgventanamodalm" class="bgventanamodalm">
            <div id="ventanamodalm" class="ventanamodalm">
                <IFRAME src="" name="ventanam" marginWidth=0 marginHeight=0 frameBorder=0 id="ventanam" frameSpacing=0 style=" width:700px; height:130px; top:200; overflow:hidden;"> 
                </IFRAME>
            </div>
        </div>
 		<form name="form2" method="post" action="ccp-adicioning.php" enctype="multipart/form-data">
	 		<?php
	 			$sesion=$_SESSION['cedulausu'];

	 			$sqlr="SELECT dominios.valor_final FROM usuarios,dominios WHERE usuarios.cc_usu=$sesion AND dominios.NOMBRE_DOMINIO='PERMISO_MODIFICA_DOC' AND dominios.valor_inicial=usuarios.cc_usu ";

				$resp = mysqli_query($linkbd,$sqlr);
				$fechaBloqueo = mysqli_fetch_row($resp);

				echo 
					"<input type='hidden' name='fechabloq' id='fechabloq' value='$fechaBloqueo[0]' />";
	 		?>

   			<table class="inicio" align="center" width="80%" >
      			<tr>
                    <td class="titulos" style="width:95%;" colspan="2">.: Adicion/Reduccion Presupuestal</td>
                    <td class="cerrar" style="width:5%;"><a href=ccp-principal.php">&nbsp;Cerrar</a></td>
      			</tr>

      			<tr>
      				<td style="width:75%;">
      					<table>
      						<tr>
				  				<td style="width:10%;" class="saludo1">Adicion/Reduccion</td>
					 			<td style="width:10%;">
			                     	<select name="tipomov" id="tipomov" onKeyUp="return tabular(event,this)" onChange="validar2()" style="width:100%;">
										<option value="-1" >Seleccione</option>
			          					<option value="1" <?php if($_POST['tipomov'] == '1') echo "SELECTED"; ?>>Adicion</option>
			         					<option value="2" <?php if($_POST['tipomov'] == '2') echo "SELECTED"; ?>>Reduccion</option>
			        				</select>
								</td>

				  				<td style="width:5%;" class="saludo1">Fecha:</td>

			        			<td style="width:15%;">
			        				<input name="fecha" type="text" id="fc_1198971545" title="DD/MM/YYYY"  value="<?php echo $_POST['fecha']; ?>" style="width:75%;" onKeyUp="return tabular(event,this) " onKeyDown="mascara(this,'/',patron,true)"  maxlength="10">
			        				<a href="#" onClick="displayCalendarFor('fc_1198971545');">&nbsp;
			        					<img src="imagenes/calendario04.png" style="width:20px" align="absmiddle" border="0">
			        				</a>
			        			</td>

					 			<td style="width:15%;" class="saludo1">Acto Administrativo:</td>
			          			<td  valign="middle" style="width:20%;">
									<input type="hidden" name="consulta" id="consulta" value="<?php echo $_POST['consulta']?>">
									<select name="acuerdo" style="width:100%;" onChange="validar2()" onKeyUp="return tabular(event,this)">
										<option value="-1">Seleccione</option>
					 					<?php
										
											$_POST['cuentaing2'] = 0;
											$_POST['cuentagas2'] = 0;

			  		   						$sqlr = "SELECT * FROM ccpetacuerdos WHERE estado='S' AND vigencia='".$vigusu."' AND tipo<>'I'";

						       				$tv = 4 + $_POST['tipomov'];

					 						$resp = mysqli_query($linkbd,$sqlr);

							    			while ($row = mysqli_fetch_row($resp)) 
							    			{
												echo 
													"<option value = $row[0] ";

												$i = $row[0];

								 				if($i == $_POST['acuerdo'])
						 						{
									 				echo "SELECTED";

												   	$_POST['vigencia'] = $row[4];
												   	$_POST['valorac']  = $row[$tv];
												   	$_POST['valorac2'] = $row[$tv];
												   	$_POST['valorac']  = number_format($_POST['valorac'],2,'.',',');

									 				//Subrutina para cargar el detalle del acuerdo de adiciones

									 				if($_POST['chacuerdo'] == '2')
									  				{
														if(!empty($_POST['tipomov']))
														{
															if($_POST['tipomov'] == '1')
															{
																$_POST['chacuerdo'] = '';

									 							$sqlr2 = "SELECT * FROM pptoadiciones WHERE id_acuerdo='$i' ORDER BY tipo desc";
														
									 					$resp2 = mysqli_query($linkbd,$sqlr2);

														$contador = 0;

								     					while ($row2 = mysqli_fetch_row($resp2)) 
									   					{
									   						$_POST['dcuentas'][] = $row2[4];	
															$nresul = existecuentain($row2[4]);
															$_POST['dncuentas'][] = $nresul;		

															$sqlrad = "SELECT clasificacion FROM pptocuentas WHERE estado='S' AND cuenta='".$row2[4]."' AND (vigencia='".$vigusu."' OR vigenciaf='$vigusu')";				
															$respc = mysqli_query($linkbd,$sqlrad);

															while($rowc = mysqli_fetch_row($respc)) 
															{
																$tipo = $rowc[0];
															}

										 					if($tipo == 'ingresos')							
									    					{
																$_POST['dingresos'][] = $row2[5];
																$_POST['dgastos'][] = 0;
															}

															if($tipo == 'funcionamiento' || $tipo == 'inversion' || $tipo == 'deuda')	
															{							
									    						$_POST['dgastos'][]   = $row2[5];
																$_POST['dingresos'][] = 0;
															}

															$contador += 1;
									   					}

														$_POST['consulta'] = $contador;

													}

													else
													{
														//Subrutina para cargar el detalle del acuerdo de adiciones

														$_POST['idacuerdo'] = $i;

									 					$sqlr2 = "SELECT * FROM pptoreducciones WHERE id_acuerdo='$i'  ORDER BY tipo desc";

														$resp2 = mysqli_query($linkbd,$sqlr2);

								     					while ($row2 = mysqli_fetch_row($resp2)) 
									   					{
															$_POST['dcuentas'][] = $row2[4];	
															$nresul = existecuentain($row2[4]);
															$_POST['dncuentas'][] = $nresul;		
															$sqlrad = "SELECT clasificacion FROM pptocuentas where estado='S' AND cuenta='".$row2[4]."' AND (vigencia='".$vigusu."' OR vigenciaf='$vigusu')";				
															$tipo = '';

															$respc = mysqli_query($linkbd,$sqlrad);

															while($rowc =mysqli_fetch_row($respc)) 
															{
															 	$tipo = $rowc[0];
															}

										 					if($tipo == 'ingresos')							
									    					{
																$_POST['dingresos'][] = $row2[5];
																$_POST['dgastos'][] = 0;	
															}

															if($tipo == 'funcionamiento' || $tipo == 'inversion' || $tipo == 'deuda')	
															{
									    						$_POST['dgastos'][] = $row2[5];
																$_POST['dingresos'][] = 0;
															}
									   					}	
													}
												}
													  
									 		}//Fin si cambio 
										}
								  			echo ">".$row[1]."-".$row[2]."</option>";	  
									}
											
											
					  				?>
									  
									</select>

			                        <input type="hidden" name="chacuerdo" value="1">		  
			                  	</td>

					  			<td style="width:7%;" class="saludo1">Vigencia:</td>
					  			<td style="width:10%;">
					  				<input type="text" id="vigencia" name="vigencia" style="width:100%; " onKeyPress="javascript:return solonumeros(event)" onKeyUp="return tabular(event,this)"  value="<?php echo $_POST['vigencia']?>" onClick="document.getElementById('tipocta').focus(); document.getElementById('tipocta').select();"  readonly>
					  			</td>
			       			</tr>
			                <tr>
				   				<td style="width:10%;" class="saludo1">
				   					<input type="hidden" value="1" name="oculto">Valor Acuerdo:</td>
			                    <td style="width:10%;">
			                    	<input name="valorac" type="text" value="<?php echo $_POST['valorac']?>" onKeyPress="javascript:return solonumeros(event)" onKeyUp="return tabular(event,this)" onBlur="traercuentas(event)">
			                    	<input type="hidden"  id="valorac2" name="valorac2" value="<?php echo $_POST['valorac2']?>">
			                    </td>
			                    <td style="width:5%;" class="saludo1">Regalias</td>
			          			<td>
			          				<input type="checkbox" name="regalias" id="regalias" onClick="validaregalias()" <?php if(!empty($_POST['regalias'])){echo "CHECKED"; } ?> >
			          			</td>
			          			<td style="width:5%;" class="saludo1">Finalizar</td>
			          			<td>
			          				<input type="checkbox" name="fin" value="1" id="fin" onClick="finaliza()">
			          			</td>
			          			
				    		</tr>
      					</table>
      				</td>
      				
      			</tr>
         	</table>
			
	   		<table class="inicio">
	   			<tr>
	   				<td colspan="8" class="titulos">Cuentas</td>
	   			</tr>
	   			<tr>
                	<td style="width:5%;" class="saludo1">Tipo</td>
	   				<td style="width:10%;">
                    	<select name="tipocta" id="tipocta" onKeyUp="return tabular(event,this)" onChange="validar()" style="width:80%;">
							<option value="-1" >Seleccione</option>
          					<option value="1" <?php if($_POST['tipocta']=='1') echo "SELECTED"; ?>>Ingreso</option>
          					<option value="2" <?php if($_POST['tipocta']=='2') echo "SELECTED"; ?>>Gastos</option>
       					</select>
                   	</td>

					<td style="width:5%;" class="saludo1">Cuenta:</td>
          			<td  valign="middle" style="width:10%;">
          				<input type="text" id="cuenta" name="cuenta" style="width:84%;" onKeyUp="return tabular(event,this)" onBlur="buscacta(event)" value="<?php echo $_POST['cuenta']?>" onClick="document.getElementById('cuenta').focus();document.getElementById('cuenta').select();"> 
						<a title="Fuentes" onClick="despliegamodal2('visible');" style="cursor:pointer;"><img src="imagenes/find02.png" style="width:20px;"/></a>  
          				<input type="hidden" value="" name="bc" id="bc">
          			</td>
					<td>
						<input name="ncuenta" type="text" style="width:50%;" value="<?php echo $_POST['ncuenta']?>" readonly>
					</td>

					  <?php
					if($_POST['tipocta'] == '1')
					{
						$sqlr = " SELECT clasificadores FROM ccpetinicialingresosclasificador WHERE cuenta = '".$_POST['cuenta']."' ";
						$resp = mysqli_query($linkbd, $sqlr);
						
						$numeroClasificadorEncontrado = '';
		
						while($row = mysqli_fetch_row($resp))
						{
							if(isset($row[0]))
							{
								$tieneClasificador = 'S';
								if($row[0] == '1')
								{
									$numeroClasificadorEncontrado = '1';
								}
		
								elseif($row[0] == '2')
								{
									$numeroClasificadorEncontrado = '2';
								}
		
								elseif($row[0] == '3')
								{
									$numeroClasificadorEncontrado = '3';
								}
							}
		
							else
							{
								$numeroClasificadorEncontrado = '0';
								$tieneClasificador = 'N';
							}
						}
					}
		
					elseif($_POST['tipocta'] == '2')
					{
						$sqlr = " SELECT clasificadores FROM ccpetprogramarclasificadoresgastos WHERE cuenta = '".$_POST['cuenta']."' ";
						$resp = mysqli_query($linkbd, $sqlr);
						
						$numeroClasificadorEncontrado = '';
		
						while($row = mysqli_fetch_row($resp))
						{
							if(isset($row[0]))
							{
								$tieneClasificador = 'S';
		
								if($row[0] == '1')
								{
									$numeroClasificadorEncontrado = '1';
								}
		
								elseif($row[0] == '2')
								{
									$numeroClasificadorEncontrado = '2';
								}
		
								elseif($row[0] == '3')
								{
									$numeroClasificadorEncontrado = '3';
								}
							}
		
							else
							{
								$numeroClasificadorEncontrado = '0';
								$tieneClasificador = 'N';
							}
						}
					
					}
			
					$_POST['clasi'] = $numeroClasificadorEncontrado;
					  ?>
					  
                    <td >
						<input type="hidden" value="<?php echo $_POST['clasi'] ?>" name="clasi" id="clasi">
                    </td>
         		</tr>

				<tr>
					<td style="width:5%" class="saludo1">Fuente:</td>
					<td valign="middle" style="width:10%">
						<input type="text" id="fuente" name="fuente" style="width:82%" onKeyUp="return tabular(event,this)" onBlur="buscaFuente(event)" value="<?php echo $_POST['fuente']?>" onClick="document.getElementById('fuente').focus();document.getElementById('fuente').select();">
						<a title="Buscar Fuentes" onClick="despliegamodal('visible');" style="cursor:pointer;"><img src="imagenes/find02.png" style="width:20px;"/></a>
					</td>

					<td colspan="3">
						<input type="text" name="nfuente" style="width:50%" value="<?php echo $_POST['nfuente'] ?>" readonly>
					</td>
				</tr>

				<?php 
					if($tieneClasificador == 'S')
					{
				?>

				<tr>
					<td style="width:5%;" class="saludo1">Clasificador:</td>
					<td valign="middle" style="width:10%;">
						<input type="text" id="clasificador" name="clasificador" style="width:82%" onKeyPress="javascript:return solonumeros(event)" onKeyUp="return tabular(event,this)" onBlur="buscaClasificador(event)" value="<?php echo $_POST['clasificador'] ?>" onClick="document.getElementById('clasificador').focus();document.getElementById('clasificador').select();">
						<a title="Buscar Clasificador" onClick="modalCuentaClasificadora('visible');" style="cursor:pointer;"><img src="imagenes/find02.png" style="width:20px;"/></a>
					</td>
					<td colspan="3">
						<input type="text" name="nclasificador" style="width:50%;" value="<?php echo $_POST['nclasificador']?>" readonly>
					</td>
				</tr>

				<?php } ?>	

		  		<tr> 
					<td class="saludo1">Medio de Pago:</td>
					<td>
						<select name="debicred" style="width:90%;">
							<option value="-1" >Seleccione</option>
		   					<option value="CSF" <?php if($_POST['debicred']=='1') echo "SELECTED"; ?>>CSF</option>
          					<option value="SSF" <?php if($_POST['debicred']=='2') echo "SELECTED"; ?>>SSF</option>
		  				</select>
					</td>
	
		  			<td style="width:5%;" class="saludo1">Valor:</td>
                    <td style="width:10%;">
                    	<input name="valor" type="text" value="<?php echo $_POST['valor']?>" onKeyPress="javascript:return solonumeros(event)" onKeyUp="return tabular(event,this)">
                    </td>

					<?php
					if($_POST['tipomov'] == 2)
					{
					?>
					<td style="width:5%;" class="saludo1">Saldo:</td>
                    <td style="width:10%;">
                    	<input name="valor2" type="text" value="<?php echo $_POST['valor2']?>" onKeyPress="javascript:return solonumeros(event)" onKeyUp="return tabular(event,this)" readonly>
                    </td>
					<?php
					}
					if($_POST['tipomov'] == 1)
					{
					?>
					<input name="valor2" type="hidden" value="<?php echo $_POST['valor2']?>" onKeyPress="javascript:return solonumeros(event)" onKeyUp="return tabular(event,this)">
                    <?php
					}
					?>
          			<td>
          				<input type="button" name="agregar" id="agregar" value="   Agregar   " onClick="agregardetalle()" >
          				<input type="hidden" value="0" name="agregadet">
          			</td>
              	</tr>  
    		</table>
			<?php
			
			function generaMensaje($arreglo,$ingresos,$gastos,$tipo)
			{
				$cont = 0;
				global $linkbd,$vigusu;

				for($i=0;$i<count($arreglo); $i++)
				{
					$saldo = generaSaldo($arreglo[$i],$vigusu,$vigusu);
				    if($tipo == 2)
					{
						if($ingresos[$i] == 0)
						{   //GASTOS
					 		if($gastos[$i] > $saldo)
							{
								$cont++;
					 		}
				 		}

						else
						{  //INGRESOS
					 		if($ingresos[$i] > $saldo)
							{
								$cont++;
					 		}
				 		}
				 	}
				}

				return "Existen $cont rubros con saldo insuficiente para dicha operacion";
			}
			
				if(!$_POST['oculto'])
				{
					 ?><script>document.form2.fecha.focus();</script><?php 
				}

				//Busca cuenta

				$iter = 'zebra1';
                $iter2 = 'zebra2';
				if($_POST['bc'] != '')
			 	{
			  		$nresul = buscaNombreCuentaCCPET($_POST['cuenta'],$_POST['tipocta']);
					  
			  		if($nresul != '')
			   		{
			  			$_POST['ncuenta'] = $nresul;
  			  			?>
							<script>
								document
								document.getElementById('valor').focus();
								document.getElementById('valor').select();
			  				</script>
						<?php
			  		}

			 		
			 	}
			?>
			<input type="hidden" name="mensaje" id="mensaje" value="<?php echo $_POST['mensaje']; ?>" />
			<input type="hidden" name="restric" id="restric"  />
        	<div class="subpantallac" style="height:50%; width:99.6%; overflow-x:scroll;">
			<?php echo $_POST['mensaje']; ?>
				<table class="inicio" width="99%">
        			<tr><td class="titulos" colspan="9">Detalle Comprobantes</td></tr>
					<tr>
						<td class="titulos2">Cuenta</td>
						<td class="titulos2">Nombre Cuenta</td>
						<td class="titulos2">Medio de Pago</td>
						<td class="titulos2">Fuente</td>
						<td class="titulos2">Clasificador</td>
						<td class="titulos2">Codigo Clasificador</td>
        				<td class="titulos2">Ingresos</td>
						<td class="titulos2">Gastos</td>
                        <td class="titulos2"><img src="imagenes/del.png"></td>
					</tr>
					<?php 
					
						if ($_POST['elimina'] != '')
		 				{ 
		 					$posi = $_POST['elimina'];

		  					$cuentagas  = 0;
		  					$cuentaing  = 0;
		   					$diferencia = 0;

		 					unset($_POST['dcuentas'][$posi]);
						   	unset($_POST['dncuentas'][$posi]);
							unset($_POST['debcred'][$posi]);
							unset($_POST['dfuentes'][$posi]);
							unset($_POST['dclasificador'][$posi]);
							unset($_POST['dnumclasi'][$posi]);
						   	unset($_POST['dgastos'][$posi]);		 		 		 		 	 		 		 		 
						   	unset($_POST['dingresos'][$posi]);	

						  	$_POST['dcuentas']  	= array_values($_POST['dcuentas']); 
						   	$_POST['dncuentas'] 	= array_values($_POST['dncuentas']);
							$_POST['debcred']   	= array_values($_POST['debcred']); 
							$_POST['dfuentes']  	= array_values($_POST['dfuentes']);
							$_POST['dclasificador'] = array_values($_POST['dclasificador']);
							$_POST['dnumclasi']		= array_values($_POST['dnumclasi']);
						   	$_POST['dgastos']   	= array_values($_POST['dgastos']); 
						   	$_POST['dingresos'] 	= array_values($_POST['dingresos']); 		
							$_POST['elimina']   	= ''; 		 		 		 
		 				}	 

		 				if ($_POST['agregadet'] == '1')
		 				{
		  					$cuentagas  = 0;
		  					$cuentaing  = 0;
		  					$diferencia = 0;

							$_POST['dcuentas'][]  	  = $_POST['cuenta'];
							$_POST['dncuentas'][] 	  = $_POST['ncuenta'];
							$_POST['debcred'][]   	  = $_POST['debicred'];
							$_POST['dfuentes'][]  	  = $_POST['fuente'];
							$_POST['dclasificador'][] = $_POST['clasificador'];
							$_POST['dnumclasi'][]	  = $_POST['clasi'];

							if($_POST['tipocta'] == '1')
	 	 					{
								$_POST['dgastos'][]   = 0;
							 	$_POST['dingresos'][] = $_POST['valor'];
							}

							if($_POST['tipocta'] == '2')
		 					{
							 	$_POST['dingresos'][] = 0;
							 	$_POST['dgastos'][]   = $_POST['valor'];
		 					}

		 					$_POST['agregadet'] = 0;
		  					?>
							<script>		
                                document.form2.cuenta.value = '';
                                document.form2.ncuenta.value = '';
								document.form2.fuente.value = '';
								document.form2.nfuente.value = '';
								document.form2.clasificador.value = '';
								document.form2.nclasificador.value = '';
								document.form2.valor.value = '';
                                document.form2.tipocta.select();
                                document.form2.tipocta.focus();	
								document.form2.agregadet.value = 0;
                         	</script><?php
		  				}

		$_POST['restric'] = '0';

		$cont = 0;

		for($x=0;$x< count($_POST['dcuentas']);$x++)
		{
			$saldo = generaSaldo($_POST['dcuentas'][$x],$vigusu,$vigusu);
			$stilo = '';

			$sqlr = "SELECT cuenta FROM pptocuentas WHERE cuenta='".$_POST['dcuentas'][$x]."'";

			$result = mysqli_query($linkbd,$sqlr);

			$row1 = mysqli_fetch_row($result);

			if($_POST['tipomov'] == 2)
			{
				if($_POST['dingresos'][$x] == 0)
				{   //GASTOS
					if($_POST['dgastos'][$x] > $saldo)
					{
						$stilo = "background-color:yellow !important ";
						$_POST['mensaje'] = "Error";
						$_POST['restric'] = "1";
						$cont++;
					}

				}

				else
				{  //INGRESOS
					if($_POST['dingresos'][$x]>$saldo)
					{
						$stilo = "background-color:yellow !important ";
						$_POST['mensaje'] = "Error";
						$_POST['restric'] = "1";
						$cont++;
					}
				}
			}
		
		if($row1[0] == '')
		{
			$stilo = "background-color:yellow !important";
		}

		echo 
			"<tr class='$iter' style='$stilo'>

				<td>".$_POST['dcuentas'][$x]."
					<input name='dcuentas[]' value='".$_POST['dcuentas'][$x]."' type='hidden'>
				</td>

				<td>".$_POST['dncuentas'][$x]."
					<input name='dncuentas[]' value='".$_POST['dncuentas'][$x]."' type='hidden'>
				</td>

				<td>".$_POST['debcred'][$x]."
					<input name='debcred[]' value='".$_POST['debcred'][$x]."' type='hidden'>
				</td>

				<td>".$_POST['dfuentes'][$x]."
					<input name='dfuentes[]' value='".$_POST['dfuentes'][$x]."' type='hidden'>
				</td>

				<td>".$_POST['dnumclasi'][$x]."
					<input name='dnumclasi[]' value='".$_POST['dnumclasi'][$x]."' type='hidden'>
				</td>

				<td>".$_POST['dclasificador'][$x]."
					<input name='dclasificador[]' value='".$_POST['dclasificador'][$x]."' type='hidden'>
				</td>

				<td>".$_POST['dingresos'][$x]."
					<input name='dingresos[]' value='".$_POST['dingresos'][$x]."' type='hidden'>
				</td>

				<td>".$_POST['dgastos'][$x]."
					<input name='dgastos[]' value='".$_POST['dgastos'][$x]."' type='hidden'>
				</td>

				<td>
					<a href='#' onclick='eliminar($x)'><img src='imagenes/del.png'></a>
				</td>
			</tr>";

			$aux   	   = $iter;
            $iter  	   = $iter2;
            $iter2 	   = $aux;
		 	$gas   	   = $_POST['dgastos'][$x];
			$ing   	   = $_POST['dingresos'][$x];
			$gas   	   = $gas;
			$ing   	   = $ing;		 
			$cuentagas = $cuentagas + $gas;
			$cuentaing = $cuentaing + $ing;

			$_POST['cuentagas2'] = $cuentagas;
			$_POST['cuentaing2'] = $cuentaing;		 	

			$diferencia = $cuentaing - $cuentagas;
			$total		= number_format($total,2,",","");

			$_POST['diferencia'] = number_format($diferencia,2,".",",");
			$_POST['cuentagas']  = number_format($cuentagas,2,".",",");
			$_POST['cuentaing']  = number_format($cuentaing,2,".",",");	
			
		}
		
		echo 
		"<tr>
			<td >Diferencia:</td>

			<td colspan='5'>
				<input id='diferencia' name='diferencia' value='$_POST[diferencia]' style='width:100%' readonly>
			</td>

			<td class='saludo1' style='width:15%'>
				<input name='cuentaing' id='cuentaing' value='$_POST[cuentaing]' style='width:100%'  readonly>
				<input name='cuentaing2' id='cuentaing2' value='$_POST[cuentaing2]' type='hidden'>
			</td>

			<td class='saludo1' style='width:15%'>
				<input id='cuentagas' name='cuentagas' value='$_POST[cuentagas]' style='width:100%' readonly><input id='cuentagas2' name='cuentagas2' value='$_POST[cuentagas2]' type='hidden'>
			</td>
		</tr>";

		?>

		<input type='hidden' name='elimina' id='elimina'>
		</table>
	</div>
</form>
  <?php

//PARTE PARA INSERTAR Y ACTUALIZAR LA INFORMACION
									 					
	$oculto = $_POST['oculto'];

	if($_POST['oculto'] == '2')
	{
		if ($_POST['acuerdo'] != '')
	 	{
 			$nr = '1';

 			ereg( "([0-9]{1,2})/([0-9]{1,2})/([0-9]{2,4})", $_POST['fecha'],$fecha);

			$fechaf = $fecha[3]."-".$fecha[2]."-".$fecha[1];

			///insercion de las cuentas al ppto inicial

			switch($_POST['tipomov'])
			{
				case 1: //Adiciones 
					$regalias = '';
					
		 			if($_POST['fin'] == '1') //**** si esta completa y finalizado
		    		{
			 			$sqlr = "UPDATE ccpetacuerdos SET estado='F' WHERE id_acuerdo='".$_POST['acuerdo']."'";
	 		
		  	  			mysqli_query($sqlr,$linkbd);	  
					}

					$sqlr = "DELETE FROM ccpetadiciones WHERE vigencia='$_POST[vigencia]' AND id_acuerdo='$_POST[acuerdo]'";	

					mysqli_query($linkbd,$sqlr); 		
					
					for($x=0;$x<count($_POST['dcuentas']);$x++)	
					{
						if($_POST['dingresos'][$x]<=0)
						{
							$valores=$_POST['dgastos'][$x];
							$tc='G';
						}

						else
						{
							$valores = $_POST['dingresos'][$x];
							$tc='I';
						}
						
						//Eliminar anteriores registros para crear el nuevo ppto inicial de la vigencia	

						if($_POST['regalias'] != '') //**** si esta completa y finalizado
			    		{
				 			$regalias = 'S'; 
						}

						$sqlr = "INSERT INTO ccpetadiciones (cuenta, fecha, vigencia, valor, estado, tipo, id_acuerdo, tipomovimiento, fuente, mediopago, clasificador, cuenta_clasificadora) VALUES ('".$_POST['dcuentas'][$x]."', '$fechaf', '".$_POST['vigencia']."', '$valores', 'S', '$tc', '".$_POST['acuerdo']."', '$regalias', '".$_POST['dfuentes'][$x]."', '".$_POST['debcred'][$x]."', '".$_POST['dnumclasi'][$x]."', '".$_POST['dclasificador'][$x]."') ";

						if(!mysqli_query($linkbd,$sqlr))
						{
							echo 
							"<script>
								alert('ERROR EN LA CREACION DE LA ADICION PRESUPUESTAL');document.form2.fecha.focus();
							</script>";
						}

						else
						{  	   
							echo "<table class='inicio'><tr><td class='saludo1'><center>Se ha almacenado el Reduccion de la cuenta ".$_POST['dcuentas'][$x]." con Exito <img src='imagenes\confirm.png'></center></td></tr></table><script>funcionmensaje();</script>";
						}
					}   //FOR
				break;

				case 2:		
					if($_POST['fin'] == '1') //**** si esta completa y finalizado
					{
						$sqlr = "UPDATE pptoacuerdos SET estado='F' WHERE id_acuerdo='".$_POST['acuerdo']."'";
						mysqli_query($linkbd,$sqlr);	  
					}

					$sqlr = "DELETE FROM pptoreducciones WHERE vigencia='$_POST[vigencia]' AND id_acuerdo=$_POST[acuerdo]";	
					mysqli_query($linkbd,$sqlr);  
		
					for($x=0;$x<count($_POST['dcuentas']);$x++)	
					{	
						$sqlr = "DELETE FROM pptoreducciones WHERE cuenta='".$_POST['dcuentas'][$x]."' AND vigencia='$_POST[vigencia]' AND id_acuerdo=$_POST[acuerdo]";	

						mysqli_query($linkbd,$sqlr);   

						if ($_POST['dingresos'][$x]<=0)
						{
							$valores = $_POST['dgastos'][$x];
							$tc = 'G';
						}

						else
						{
							$valores = $_POST['dingresos'][$x];
							$tc = 'I';
						}

						// Eliminar anteriores registros para crear el nuevo ppto inicial de la vigencia			 

						$sqlr = "INSERT INTO pptoreducciones (cuenta,fecha,vigencia,valor,estado,tipo,id_acuerdo)VALUES (TRIM(' ' FROM '".$_POST['dcuentas'][$x]."'),'".$fechaf."','$_POST[vigencia]', $valores,'S','$tc',$_POST[acuerdo])";

					
						if(!mysqli_query($linkbd,$sqlr))
						{
							echo 
							"<script>
								alert('ERROR EN LA CREACION DE LA REDUCCION PRESUPUESTAL');document.form2.fecha.focus();
							</script>";
						}

						else
						{	 
							if($_POST['fin'] == '1' ) //Si esta completa y finalizado
							{
								$sqlr = "UPDATE pptocuentaspptoinicial SET pptodef=pptodef-$valores, SALDOS=SALDOS-$valores, SALDOSCDPRP=SALDOSCDPRP-$valores WHERE cuenta='".$_POST['dcuentas'][$x]."' AND (pptocuentas.vigencia=".$vigusu." OR pptocuentas.vigenciaf=".$vigusu.")";
								
								mysqli_query($linkbd,$sqlr);	  
							} 

							echo 
							"<table class='inicio'>
								<tr>
									<td class='saludo1'>
										<center>
											Se ha almacenado el Reduccion de la cuenta ".$_POST['dcuentas'][$x]." con Exito <img src='imagenes\confirm.png'>
										</center>
									</td>
								</tr>
							</table>";
							
						echo	
							"<script>
								funcionmensaje();
							</script>";
						}
					}   //for
				break;	   
			} //switch
		}//if de acuerdo

		else
		{
			echo 
				"<table>
					<tr>
						<td class='saludo1'>
							<center>
								Falta informacion para Crear el Proceso
							</center>
						</td>
					</tr>
				</table>";

			echo 
				"<script>
					document.form2.fecha.focus();
				</script>";  
		} 
	}//IF de control de guardado
?> 

			<div id="bgventanamodal2">
                <div id="ventanamodal2">
                    <IFRAME  src="" name="buster" marginWidth=0 marginHeight=0 frameBorder=0 id="ventana2" frameSpacing=0 style="left:500px; width:900px; height:500px; top:200;"> 
                    </IFRAME>
                </div>
       	 	</div>	
</body>
</html>