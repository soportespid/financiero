<!--V 1.0 24/02/2015-->
<?php
	require "comun.inc";
	require "funciones.inc";
	session_start();
	$linkbd=conectar_v7();	
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	header("Cache-control: private"); // Arregla IE 6
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es">
	<head>
	 	<meta http-equiv="Content-Type" content="text/html" charset="iso-8859-1"/>
        <meta http-equiv="X-UA-Compatible" content="IE=9"/>
		<title>:: IDEAL 10 - Control de activos</title>
        <link href="css/css2.css" rel="stylesheet" type="text/css" />
        <link href="css/css3.css" rel="stylesheet" type="text/css" />
		<link href="css/style.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
        <script type="text/javascript" src="css/programas.js"></script>
		<script src="sweetalert2/dist/sweetalert2.min.js"></script>
		<link rel="stylesheet" href="sweetalert2/dist/sweetalert2.min.css">
		<script>
			function guardar() {
				Swal.fire({
					icon: 'question',
					title: 'Seguro que quieres guardar?',
					showDenyButton: true,
					confirmButtonText: 'Guardar',
					denyButtonText: 'Cancelar',
					}).then((result) => {
						if (result.isConfirmed) {
							document.form2.oculto.value='2';
							document.form2.submit();
						} else if (result.isDenied) {
							Swal.fire('Guardar cancelado', '', 'info')
						}
					}
				)
			}
		</script>
<?php titlepag();?>
</head>
<body>
<IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
<span id="todastablas2"></span>
<table>
	<tr><script>barra_imagenes("acti");</script><?php cuadro_titulos();?></tr>
   	<tr><?php menu_desplegable("acti");?></tr>
	<tr>
		<td colspan="3" class="cinta">
			<a href="acti-ubicacion.php" class="mgbt"><img src="imagenes/add.png" title="Nuevo"/></a>
			<a class="mgbt" onclick="guardar()"><img src="imagenes/guarda.png"/></a>
			<a href="acti-buscaubicacion.php" class="mgbt"><img src="imagenes/busca.png"  title="Buscar" /></a>
			<a onClick="mypop=window.open('plan-agenda.php','','');mypop.focus()" class="mgbt"><img src="imagenes/agenda1.png" title="Agenda" /></a>
			<a onClick="mypop=window.open('acti-principal.php','','');mypop.focus();" class="mgbt"><img src="imagenes/nv.png" title="Nueva ventana"></a>
            <a onClick="mypop=window.open('<?php echo $url2; ?>','','');mypop.focus();" class="tooltip bottom mgbt"><img src="imagenes/duplicar_pantalla.png" title="Duplicar pesta&ntilde;a"></a>
            <a href="acti-buscaubicacion.php" class="mgbt"><img src="imagenes/iratras.png" title="Atr&aacute;s"></a>
		</td>
	</tr>
</table> 
<form name="form2" method="post" action="acti-ubicacion.php">
  <?php
 if($_POST['oculto']=='')
{
 	$fec=date("d/m/Y");
	$_POST['fecha']=$fec; 	
	$_POST['vigencia']=$vigencia;
	$sqlr="SELECT MAX(CONVERT(id_cc, INT)) from actiubicacion  order by id_cc Desc";
		// echo $sqlr;
	$res=mysqli_query($linkbd,$sqlr);
	$row=mysqli_fetch_row($res);
	$_POST['codigo']=$row[0]+1;
	if(strlen($_POST['codigo'])==1)
	{
		$_POST['codigo']='0'.$_POST['codigo'];
	}
}
?>
     <table class="inicio" align="center" >
      <tr >
        <td class="titulos" colspan="6">.: Agregar ubicaciones</td><td class="cerrar" ><a href="acti-principal.php">Cerrar</a></td>
      </tr>
      <tr  >
	  <td class="saludo1">.: C&oacute;digo:
        </td>
        <td><input name="codigo" type="text" value="<?php echo $_POST['codigo']?>" maxlength="2" size="2" onKeyPress="javascript:return solonumeros(event)" onKeyUp="return tabular(event,this)">
        </td>
        <td class="saludo1">.: Nombre ubicaci&oacute;n:
        </td>
        <td><input name="nombre" type="text" value="<?php echo $_POST['nombre']?>" size="80" onKeyUp="return tabular(event,this)">
        </td>
        <td class="saludo1">.: Activo:
        </td>
        <td><select name="estado" id="estado" >
          <option value="S" selected>SI</option>
          <option value="N">NO</option>
        </select>   <input name="oculto" type="hidden" value="1">     </td>
       </tr>  
    </table>
    </form>
  <?php
	$oculto=$_POST['oculto'];

	if($_POST['oculto'] == "2")
	{
		$linkbd=conectar_v7();
		if ($_POST['nombre']!="") {
			$nr="1";
			$sqlr="INSERT INTO actiubicacion (id_cc,nombre,estado)VALUES ('$_POST[codigo]','".($_POST['nombre'])."', '$_POST[estado]')";
			if (!mysqli_query($linkbd,$sqlr)) {
				echo "<table><tr><td class='saludo1'><center><font color=blue>Manejador de Errores de la Clase BD<br><font size=1></font></font><br><p align=center>No se pudo ejecutar la petici�n: <br><font color=red><b>$sqlr</b></font></p>";
				echo "Ocurri� el siguiente problema:<br>";
				echo "<pre>";
				echo "</pre></center></td></tr></table>";
			}
		else {
			echo "<table><tr><td class='saludo1'><center>Se ha almacenado con Exito <img src='imagenes\confirm.png' ></center></td></tr></table>";
		}
	}
	else {
		echo "<table class='inicio'><tr><td class='saludo1'><center>Falta informacion para Crear la Ubicacion <img src='imagenes\alert.png' ></center></td></tr></table>";
	}
}
?>
</body>
</html>
