<?php 
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require "comun.inc";
	require "funciones.inc";
	session_start();
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html lang="es">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1">
		<meta name="viewport" content="user-scalable=no">
		<title>:: IDEAL 10 - Tesorer&iacute;a</title>
		<link href="favicon.ico" rel="shortcut icon"/>
		<link href="css/css2.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css">
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css">
		<script type="text/javascript" src="css/calendario.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script> 
			function ponprefijo(nomina,registro){ 
				opener.document.form2.orden.value =nomina  ;
				opener.document.form2.rp.value =registro ;
				opener.document.form2.orden.focus();	
				window.close() ;
			} 
		</script> 
	</head>
	<body >
		<form action="" method="post" enctype="multipart/form-data" name="form1">
			<table  class="inicio" align="center" >
				<tr >
					<td class="titulos" colspan="4">:: Buscar Nomina</td>
				</tr>
				<tr >
					<td class="saludo1">:: Numero Nomina:</td>
					<td><input name="numero" type="text" value="" size="40" onKeyPress="javascript:return solonumeros(event)"></td>
					<td><input type="submit" name="Submit" value="Buscar" ></td>
					<input name="vigencia" type="hidden" value="<?php echo $_GET['vigencia']?>">
					<input name="oculto" type="hidden" value="1">
				</tr>
			</table> 
			<div class="subpantalla" style="height:85.5%; width:99.6%; overflow-x:hidden;">
				<?php
					//if($_POST[oculto])
					{
						$crit1=" ";
						$crit2=" ";
						if ($_POST['numero']!=""){$crit1=" and (id_nom like '%".$_POST['numero']."%') ";}
						$sqlr="select id_nom, id_rp, FECHA, ESTADO from humnomina_aprobado where estado='S' $crit1 $crit2 order by id_nom desc";
						$resp = mysqli_query($linkbd,$sqlr);
						$ntr = mysqli_num_rows($resp);
						$con=1;
						echo "
						<table class='inicio' align='center' width='99%'>
						<tr><td colspan='8' class='titulos'>.: Resultados Busqueda:</td></tr>
						<tr><td colspan='8'>Liquidaciones de Nomina Encontrados: $ntr</td></tr>
						<tr>
							<td class='titulos2' width='2%'>Item</td>
							<td class='titulos2' width='30%'>Nomina</td>
							<td class='titulos2' width='10%'>N° RP</td>
							<td class='titulos2' width='10%'>FECHA</td>
							<td class='titulos2' width='4%'>ESTADO</td>
						</tr>";	
						$iter='saludo1a';
						$iter2='saludo2';
						while ($row =mysqli_fetch_row($resp)){
							$detalle=$row[2];
							echo"<tr class='$iter' onClick=\"javascript:ponprefijo('$row[0]','$row[1]')\">
							<td>$con</td>
							<td>$row[0]</td>
							<td>$row[1]</td>
							<td>$row[2]</td>
							<td style='text-transform:uppercase;text-align:center;'>$row[3]</td></tr>";
							$con+=1;
							$aux=$iter;
							$iter=$iter2;
							$iter2=$aux;
						}
						echo"</table>";
					}
				?>
			</div>
		</form>
	</body>
</html>
