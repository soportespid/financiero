<?php
header("Cache-control: no-cache, no-store, must-revalidate");
header("Content-Type: text/html;charset=utf8");

require 'comun.inc';
require 'funciones.inc';

$linkbd = conectar_v7();
$linkbd->set_charset("utf8");

session_start();
date_default_timezone_set("America/Bogota");
ini_set('max_execution_time', 7200);
?>
<!DOCTYPE>
<html lang="es">

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1" />
    <meta name="viewport" content="user-scalable=no">
    <title>:: IDEAL 10 - Contabilidad</title>
    <link href="favicon.ico" rel="shortcut icon" />
    <link href="css/css2.css?<?php echo date('d_m_Y_h_i_s'); ?>" rel="stylesheet" type="text/css" />
    <link href="css/css3.css?<?php echo date('d_m_Y_h_i_s'); ?>" rel="stylesheet" type="text/css" />
    <link href="css/css4.css?<?php echo date('d_m_Y_h_i_s'); ?>" rel="stylesheet" type="text/css" />
    <link href="css/style.css?<?php echo date('d_m_Y_h_i_s'); ?>" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="css/programas.js?<?php echo date('d_m_Y_h_i_s'); ?>"></script>
    <script type="text/javascript" src="css/calendario.js?<?php echo date('d_m_Y_h_i_s'); ?>"></script>
    <script type="text/javascript" src="jquery-1.11.0.min.js"></script>
    <script>
        $(window).load(function () { $('#cargando').hide(); });
        function buscarconciliar() {
            if (document.getElementById('cuenta').value != "-1") {
                var validacion01 = document.getElementById('fc_1198971545').value;
                var validacion02 = document.getElementById('fc_1198971546').value;
                if ((validacion01.trim() != '') && (validacion02.trim() != '')) {
                    var fechaInicial = validacion01;
                    var fechaFinal = validacion02;
                    if (validate_fechaMayorQue(fechaInicial, fechaFinal)) {
                        document.form2.bconc.value = 1;
                        document.form2.submit();
                    }
                    else { despliegamodalm('visible', '2', "La fecha final NO es superior a la fecha inicial"); }
                }
                else { despliegamodalm('visible', '2', "Falta ingresar una fecha de inicio y una fecha final") }
            }
            else { despliegamodalm('visible', '2', "Falta seleccionar una cuenta") }

        }
        function checktodos() {
            cali = document.getElementsByName('conciliados[]');
            for (var i = 0; i < cali.length; i++) {
                if (document.getElementById("todos").checked == true) {
                    cali.item(i).checked = true;
                    document.getElementById("todos").value = 1;
                }
                else {
                    cali.item(i).checked = false;
                    document.getElementById("todos").value = 0;
                }
            }
            sumarconc();
        }
        function marcar(indice, posicion) {
            vvigencias = document.getElementsByName('conciliados[]');
            anterior = document.getElementsByName('conciliadoanterior[]');
            anterior_fecha = document.getElementsByName('conciliadoanterior_fecha[]');
            vtabla = document.getElementById('fila' + indice);
            ca = anterior.length;
            con = 0;
            for (y = 0; y < ca; y++) {
                fechacon = anterior_fecha.item(y).value;
                if ((anterior.item(y).value == vvigencias.item(posicion).value) && (fechacon != '')) {
                    con = con + 1;
                    fechaconv = anterior_fecha.item(y).value;
                }
            }
            if (vvigencias.item(posicion).checked) {
                if (con == 0) {
                    //vtabla.style.backgroundColor="#3399bb";
                }
                else {
                    var menalert = "Ya esta conciliado en la fecha " + fechaconv;
                    despliegamodalm('visible', '2', menalert)
                    vvigencias.item(posicion).checked = false;
                }
            }
            else {
                e = vvigencias.item(posicion).value;
                //vtabla.style.backgroundColor='#ffffff';
                //document.getElementById('fila'+e).style.backgroundColor='#ffffff';
            }
            sumarconc();
        }
        function sumarconc() {
            vvigencias = document.getElementsByName('conciliados[]');
            vdebitos = document.getElementsByName('debitos[]');
            vcreditos = document.getElementsByName('creditos[]');
            sumacd = sumancd = sumacc = sumancc = 0;
            for (x = 0; x < vvigencias.length; x++) {
                if (vvigencias.item(x).checked) {
                    sumacd = sumacd + parseFloat(vdebitos.item(x).value);
                    sumacc = sumacc + parseFloat(vcreditos.item(x).value);
                }
                else {
                    sumancd = sumancd + parseFloat(vdebitos.item(x).value);
                    sumancc = sumancc + parseFloat(vcreditos.item(x).value);
                }
            }
            saldofinal = parseFloat(document.form2.saldofin.value);
            document.form2.ccreditos.valu = (sumacc);
            document.getElementById('ccreditos').value = (sumacc);
            document.form2.ncdebitos.value = (sumancd);
            document.form2.nccreditos.value = (sumancc);
            document.form2.cdebitos.value = (sumacd);
            document.form2.debnc.value = (sumancd);
            document.form2.crednc.value = Math.round((sumancc), 2);
            document.form2.extracto.value = Math.round((saldofinal + sumancc - sumancd));
            document.form2.difextracto.value = Math.round((document.form2.extractofis.value - document.form2.extracto.value), 2);
        }
        function pdf() {
            document.form2.action = "conciliacionpdf";
            document.form2.target = "_BLANK";
            document.form2.submit();
            document.form2.action = "";
            document.form2.target = "";
        }
        function validar() {
            document.form2.bconc.value = 1;
            document.form2.action = "cont-conciliacion";
            document.form2.submit();
        }
        function guardar() {
            if (document.getElementById('cuenta').value != "-1") {
                if (document.getElementById('extractofis').value != "0") {
                    var validacion01 = document.getElementById('fc_1198971545').value;
                    var validacion02 = document.getElementById('fc_1198971546').value;
                    if ((validacion01.trim() != '') && (validacion02.trim() != '')) {
                        despliegamodalm('visible', '4', 'Esta Seguro de Guardar', '1');
                    }
                    else { despliegamodalm('visible', '2', "Falta ingresar una fecha de inicio y una fecha final"); }
                }
                else {
                    document.getElementById('extractofis').style.backgroundColor = 'yellow'
                    despliegamodalm('visible', '2', "Falta digitar el Saldo Extracto Banco:");
                }
            }
            else { despliegamodalm('visible', '2', "Falta seleccionar una cuenta"); }
        }
        function despliegamodalm(_valor, _tip, mensa, pregunta) {
            document.getElementById("bgventanamodalm").style.visibility = _valor;
            if (_valor == "hidden") { document.getElementById('ventanam').src = ""; }
            else {
                switch (_tip) {
                    case "1":
                        document.getElementById('ventanam').src = "ventana-mensaje1.php?titulos=" + mensa; break;
                    case "2":
                        document.getElementById('ventanam').src = "ventana-mensaje3.php?titulos=" + mensa; break;
                    case "3":
                        document.getElementById('ventanam').src = "ventana-mensaje2.php?titulos=" + mensa; break;
                    case "4":
                        document.getElementById('ventanam').src = "ventana-consulta1.php?titulos=" + mensa + "&idresp=" + pregunta; break;
                }
            }
        }
        function funcionmensaje() { }
        function respuestaconsulta(pregunta) {
            switch (pregunta) {
                case "1":
                    document.form2.oculto.value = "2";
                    document.form2.submit();
                    break;
            }
        }

        function excell() {
            document.form2.action = "cont-excelConciliacion";
            document.form2.target = "_BLANK";
            document.form2.submit();
            document.form2.action = "";
            document.form2.target = "";
        }

        function redireccionar() {
            let cuenta = document.getElementById('ncuenta').value;
            let fech = document.getElementById("fc_1198971545").value;
            let fech1 = document.getElementById("fc_1198971546").value;

            let fechPart = fech.split('-');
            let f1 = fechPart[2] + '/' + fechPart[1] + '/' + fechPart[0];

            let fechPart2 = fech1.split('-');
            let f2 = fechPart2[2] + '/' + fechPart2[1] + '/' + fechPart2[0];
            window.open("cont-auxiliarcuenta.php?cod=" + cuenta + "&fec=" + f1 + "&fec1=" + f2);
        }
    </script>

    <script>
        function verificar() {
            var checkbox = document.getElementById('cbox2');
            var text = document.getElementById('txt');

            if (document.getElementById('cbox2').checked) {
                $("#txt").text('SI');
            } else {
                $("#txt").text('NO');
            }
        }
    </script>
</head>

<body>
    <table>
        <tr>
            <script>barra_imagenes("cont");</script>
            <?php cuadro_titulos(); ?>
        </tr>
        <tr>
            <?php menu_desplegable("cont"); ?>
        </tr>
    </table>
    <div class="bg-white group-btn p-1"><button type="button" onclick="location.href='cont-conciliacion'"
            class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
            <span>Nuevo</span>
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960">
                <path d="M440-440H200v-80h240v-240h80v240h240v80H520v240h-80v-240Z"></path>
            </svg>
        </button><button type="button" onclick="guardar()"
            class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
            <span>Guardar</span>
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960">
                <path
                    d="M840-680v480q0 33-23.5 56.5T760-120H200q-33 0-56.5-23.5T120-200v-560q0-33 23.5-56.5T200-840h480l160 160Zm-80 34L646-760H200v560h560v-446ZM480-240q50 0 85-35t35-85q0-50-35-85t-85-35q-50 0-85 35t-35 85q0 50 35 85t85 35ZM240-560h360v-160H240v160Zm-40-86v446-560 114Z">
                </path>
            </svg>
        </button><button type="button" onclick="mypop=window.open('plan-agenda','','');mypop.focus()"
            class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
            <span class="group-hover:text-white">Agenda</span>
            <svg class="fill-black group-hover:fill-white w-5 h-5" xmlns="http://www.w3.org/2000/svg"
                viewBox="0 -960 960 960">
                <path
                    d="M560-564v-68q33-14 67.5-21t72.5-7q26 0 51 4t49 10v64q-24-9-48.5-13.5T700-600q-38 0-73 9.5T560-564Zm0 220v-68q33-14 67.5-21t72.5-7q26 0 51 4t49 10v64q-24-9-48.5-13.5T700-380q-38 0-73 9t-67 27Zm0-110v-68q33-14 67.5-21t72.5-7q26 0 51 4t49 10v64q-24-9-48.5-13.5T700-490q-38 0-73 9.5T560-454ZM260-320q47 0 91.5 10.5T440-278v-394q-41-24-87-36t-93-12q-36 0-71.5 7T120-692v396q35-12 69.5-18t70.5-6Zm260 42q44-21 88.5-31.5T700-320q36 0 70.5 6t69.5 18v-396q-33-14-68.5-21t-71.5-7q-47 0-93 12t-87 36v394Zm-40 118q-48-38-104-59t-116-21q-42 0-82.5 11T100-198q-21 11-40.5-1T40-234v-482q0-11 5.5-21T62-752q46-24 96-36t102-12q58 0 113.5 15T480-740q51-30 106.5-45T700-800q52 0 102 12t96 36q11 5 16.5 15t5.5 21v482q0 23-19.5 35t-40.5 1q-37-20-77.5-31T700-240q-60 0-116 21t-104 59ZM280-494Z">
                </path>
            </svg>
        </button><button type="button" onclick="window.open('cont-principal');"
            class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
            <span>Nueva ventana</span>
            <svg class="fill-black group-hover:fill-white w-5 h-5" xmlns="http://www.w3.org/2000/svg"
                viewBox="0 -960 960 960">
                <path
                    d="M320-120v-80h80v-80H160q-33 0-56.5-23.5T80-360v-400q0-33 23.5-56.5T160-840h640q33 0 56.5 23.5T880-760v400q0 33-23.5 56.5T800-280H560v80h80v80H320ZM160-360h640v-400H160v400Zm0 0v-400 400Z">
                </path>
            </svg>
        </button><button type="button"
            onclick="mypop=window.open('/financiero/cont-conciliacion.php','','');mypop.focus();"
            class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
            <span class="group-hover:text-white">Duplicar pantalla</span>
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960">
                <path
                    d="M640-160v-360H160v360h480Zm80-200v-80h80v-360H320v200h-80v-200q0-33 23.5-56.5T320-880h480q33 0 56.5 23.5T880-800v360q0 33-23.5 56.5T800-360h-80ZM160-80q-33 0-56.5-23.5T80-160v-360q0-33 23.5-56.5T160-600h480q33 0 56.5 23.5T720-520v360q0 33-23.5 56.5T640-80H160Zm400-603ZM400-340Z">
                </path>
            </svg>
        </button><button type="button" onclick="pdf()"
            class="btn btn-danger btn-danger-hover d-flex justify-between align-items-center">
            <span>Exportar PDF</span>
            <svg xmlns="http://www.w3.org/2000/svg"
                viewBox="0 0 512 512"><!-- !Font Awesome Free 6.5.1 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license/free Copyright 2024 Fonticons, Inc. -->
                <path
                    d="M64 464l48 0 0 48-48 0c-35.3 0-64-28.7-64-64L0 64C0 28.7 28.7 0 64 0L229.5 0c17 0 33.3 6.7 45.3 18.7l90.5 90.5c12 12 18.7 28.3 18.7 45.3L384 304l-48 0 0-144-80 0c-17.7 0-32-14.3-32-32l0-80L64 48c-8.8 0-16 7.2-16 16l0 384c0 8.8 7.2 16 16 16zM176 352l32 0c30.9 0 56 25.1 56 56s-25.1 56-56 56l-16 0 0 32c0 8.8-7.2 16-16 16s-16-7.2-16-16l0-48 0-80c0-8.8 7.2-16 16-16zm32 80c13.3 0 24-10.7 24-24s-10.7-24-24-24l-16 0 0 48 16 0zm96-80l32 0c26.5 0 48 21.5 48 48l0 64c0 26.5-21.5 48-48 48l-32 0c-8.8 0-16-7.2-16-16l0-128c0-8.8 7.2-16 16-16zm32 128c8.8 0 16-7.2 16-16l0-64c0-8.8-7.2-16-16-16l-16 0 0 96 16 0zm80-112c0-8.8 7.2-16 16-16l48 0c8.8 0 16 7.2 16 16s-7.2 16-16 16l-32 0 0 32 32 0c8.8 0 16 7.2 16 16s-7.2 16-16 16l-32 0 0 48c0 8.8-7.2 16-16 16s-16-7.2-16-16l0-64 0-64z">
                </path>
            </svg>
        </button><button type="button" onclick="excell()"
            class="btn btn-success btn-success-hover d-flex justify-between align-items-center">
            <span>Exportar Excel</span>
            <svg class="fill-black group-hover:fill-white w-4 h-4" xmlns="http://www.w3.org/2000/svg"
                viewBox="0 0 384 512"><!--!Font Awesome Free 6.5.1 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license/free Copyright 2024 Fonticons, Inc. -->
                <path
                    d="M48 448V64c0-8.8 7.2-16 16-16H224v80c0 17.7 14.3 32 32 32h80V448c0 8.8-7.2 16-16 16H64c-8.8 0-16-7.2-16-16zM64 0C28.7 0 0 28.7 0 64V448c0 35.3 28.7 64 64 64H320c35.3 0 64-28.7 64-64V154.5c0-17-6.7-33.3-18.7-45.3L274.7 18.7C262.7 6.7 246.5 0 229.5 0H64zm90.9 233.3c-8.1-10.5-23.2-12.3-33.7-4.2s-12.3 23.2-4.2 33.7L161.6 320l-44.5 57.3c-8.1 10.5-6.3 25.5 4.2 33.7s25.5 6.3 33.7-4.2L192 359.1l37.1 47.6c8.1 10.5 23.2 12.3 33.7 4.2s12.3-23.2 4.2-33.7L222.4 320l44.5-57.3c8.1-10.5 6.3-25.5-4.2-33.7s-25.5-6.3-33.7 4.2L192 280.9l-37.1-47.6z">
                </path>
            </svg>
        </button></div>
    <div id="bgventanamodalm" class="bgventanamodalm">
        <div id="ventanamodalm" class="ventanamodalm">
            <IFRAME src="" name="ventanam" marginWidth=0 marginHeight=0 frameBorder=0 id="ventanam" frameSpacing=0
                style=" width:700px; height:130px; top:200; overflow:hidden;">
            </IFRAME>
        </div>
    </div>
    <form name="form2" method="post" action="">
        <div class="loading" id="divcarga"><span>Cargando...</span></div>
        <?php
        if ($_POST['oculto'] == "") {
            echo "<script>document.getElementById('divcarga').style.display='none';</script>";
        }

        $vigusu = vigencia_usuarios($_SESSION['cedulausu']);
        if ($_POST['ban'] == "") {
            if (isset($_GET['cod'])) {
                if (!empty($_GET['cod'])) {
                    $_POST['cuenta'] = $_GET['cod'];
                    $_POST['fecha'] = $_GET['fec'];
                    $_POST['fecha2'] = $_GET['fec1'];
                    $_POST['bc'] = '1';
                    $_POST['bconc'] = '1';
                }
            }
        }
        if ($_POST['bconc'] == '1') {
            if (strpos($_POST['fecha'], "/") === true) {
                $arregloini = explode("/", $_POST['fecha']);
                $_POST['fecha'] = $arregloini[2] . "-" . $arregloini[1] . "-" . $arregloini[0];
            }
            if (strpos($_POST['fecha2'], "/") === true) {
                $arregloini2 = explode("/", $_POST['fecha2']);
                $_POST['fecha2'] = $arregloini2[2] . "-" . $arregloini2[1] . "-" . $arregloini2[0];
            }
            $_POST['ncdebitos'] = 0;
            $_POST['ccreditos'] = 0;
            $_POST['nccreditos'] = 0;
            $_POST['saldofin'] = 0;
            $_POST['saldoini'] = 0;
            $_POST['extracto'] = 0;
            $_POST['debnc'] = 0;
            $_POST['crednc'] = 0;
            $_POST['ncompro'] = array();
            $_POST['compro'] = array();
            $_POST['compro2'] = array();
            $_POST['dfechas'] = array();
            $_POST['dterceros'] = array();
            $_POST['conciliados'] = array();
            $_POST['detalles'] = array();
            $_POST['debitos'] = array();
            $_POST['creditos'] = array();
            $_POST['extractofis'] = 0;
            $_POST['difextracto'] = 0;
            preg_match("/([0-9]{4})\-([0-9]{2})\-([0-9]{2})/", $_POST['fecha'], $fecha);
            //$fechaf = "$fecha[3]-$fecha[2]-$fecha[1]";
            $fechafa2 = mktime(0, 0, 0, $fecha[2], $fecha[3], $fecha[1]);
            $agetra = $fecha[1];
            $f1 = $fechafa2;
            preg_match("/([0-9]{4})\-([0-9]{2})\-([0-9]{2})/", $_POST['fecha2'], $fecha);
            //$fechaf2 = "$fecha[3]-$fecha[2]-$fecha[1]";
            $f2 = mktime(0, 0, 0, $fecha[2], $fecha[3], $fecha[1]);
            //********** calcular saldo inicial ***********
            $fechafa = $agetra . "-01-01";
            $fechafa2 = date('Y-m-d', $fechafa2 - ((24 * 60 * 60)));
            //***** buscar saldo inicial ***
            $compinicial = 0;
            $fechaf = $_POST['fecha'];
            $fechaf2 = $_POST['fecha2'];
            $sqlr = "SELECT * FROM conciliacion_cab WHERE cuenta = '" . $_POST['cuenta'] . "' AND periodo1 = '$fechaf' AND periodo2 = '$fechaf2'";
            $res = mysqli_query($linkbd, $sqlr);
            $saldoperant = 0;
            while ($row = mysqli_fetch_row($res)) {
                $_POST['extractofis'] = $row[1];
                $_POST['difextracto'] = round($row[3], 2);
            }
            if ($fechaf <= $fechafa and $fechaf2 > $fechafa) {
            }
            $sqlr = "SELECT T2.cuenta, (sum(T2.valdebito) - sum(T2.valcredito)) AS saldof FROM comprobante_cab AS T1 INNER JOIN comprobante_det AS T2 ON T2.tipo_comp = T1.tipo_comp AND T2.numerotipo = T1.numerotipo WHERE T2.cuenta = '" . $_POST['cuenta'] . "' AND T1.estado = '1' AND T2.cuenta != '' AND T1.fecha BETWEEN '' AND '$fechafa2' ORDER BY T1.tipo_comp, T1.numerotipo, T2.id_det, T1.fecha";
            $res = mysqli_query($linkbd, $sqlr);
            $saldoperant = 0;
            while ($row = mysqli_fetch_row($res)) {
                $saldoperant = $row[1];
            }
            $_POST['saldoini'] = round($compinicial + $saldoperant, 2);
            $_POST['saldofin'] = $_POST['saldoini'];
            $sqlr = "SELECT T2.cuenta, sum(T2.valdebito), sum(T2.valcredito), T1.tipo_comp, T1.numerotipo FROM comprobante_cab AS T1 INNER JOIN comprobante_det AS T2 ON T2.tipo_comp = T1.tipo_comp AND T2.numerotipo = T1.numerotipo WHERE T2.cuenta = '" . $_POST['cuenta'] . "' AND T1.estado = '1' AND T1.tipo_comp <> '7' AND T1.tipo_comp <> '102' AND T2.cuenta != '' AND T1.fecha BETWEEN '$fechaf' AND '$fechaf2' GROUP BY T1.tipo_comp, T1.numerotipo,T2.id_det";
            $res = mysqli_query($linkbd, $sqlr);
            $acumd = 0;
            $acumc = 0;
            while ($row = mysqli_fetch_row($res)) {
                $acumd += $row[1];
                $acumc += $row[2];
            }
            $_POST['saldofin'] += $acumd - $acumc;
            $_POST['ban'] = 1;
        }
        ?>
        <table align="center" class="inicio">
            <tr>
                <td class="titulos" colspan="11">Conciliacion Bancaria NICSP</td>
                <td class="cerrar" style="width:7%;"><a onClick="location.href='cont-principal.php'">&nbsp;Cerrar</a>
                </td>
            </tr>
            <tr>
                <td class="saludo3" style="width:8%">Saldo Extracto Banco:</td>
                <td style="width:8%"><input id="extractofis" name="extractofis" type="text" style="width:100%"
                        value="<?php echo $_POST['extractofis'] ?>" onBlur='marcar(0,0)' /></td>
                <td class="saludo3" style="width:5%">Fecha Inicial:</td>
                <td><input name="fecha" type="text" id="fc_1198971545" title="YYYY-MM-DD"
                        value="<?php echo $_POST['fecha']; ?>" onKeyUp="return tabular(event,this) "
                        onKeyDown="mascara(this,'/',patron,true)" onchange="" maxlength="10" style="width:70%"
                        onBlur="document.form2.submit();">&nbsp;<a onClick="displayCalendarFor('fc_1198971545');"
                        title="Calendario" style='cursor:pointer;'><img src="imagenes/calendario04.png"
                            align="absmiddle" style="width:20px;"></a></td>
                <td class="saludo3" style="width:5%">Fecha Final: </td>
                <td><input name="fecha2" type="text" id="fc_1198971546" title="YYYY-MM-DD"
                        value="<?php echo $_POST['fecha2']; ?>" onKeyUp="return tabular(event,this) "
                        onKeyDown="mascara(this,'/',patron,true)" onchange="" maxlength="10" style="width:70%">&nbsp;<a
                        onClick="displayCalendarFor('fc_1198971546');" title="Calendario" style='cursor:pointer;'><img
                            src="imagenes/calendario04.png" align="absmiddle" style="width:20px;"></a></td>
                <td class="saludo3" style="width:5%">Diferencia:</td>
                <td style="width:8%"><input id="difextracto" name="difextracto" type="text" style="width:100%"
                        value="<?php echo $_POST['difextracto'] ?>" /></td>
                <td colspan="1">
                    <input type="button" style="width:100%; background-color: darkcyan;" name="Auxiliar Contable"
                        value="Auxiliar Contable" onClick="redireccionar()">
                </td>
                <td class="saludo3" style="width:8%">Imprimir Conciliados:</td>
                <td class="saludo3" style="width:8%">
                    <input type="checkbox" name="cbox2" id="cbox2" onclick="verificar()" />
                    <label id="txt">NO</label>
                </td>


            </tr>
            <tr>
                <td class="saludo3">Cuenta:</td>
                <td valign="middle" colspan="7">
                    <input type="hidden" name="oculto" id="oculto" value="1" />
                    <select id="cuenta" name="cuenta" onChange="validar()" onKeyUp="return tabular(event,this)"
                        style="width:100%;">
                        <option value="-1">Seleccione....</option>
                        <?php
                        preg_match("/([0-9]{4})\-([0-9]{2})\-([0-9]{2})/", $_POST['fecha'], $fecha);
                        if ($fecha[1] == '') {
                            $fecha[1] = '2018';
                        }
                        if ($fecha[1] > '2017') {
                            $sqlr = "SELECT cuenta, nombre FROM cuentasnicsp WHERE cuenta LIKE '1110%' AND (tipo = 'Auxiliar' OR tipo = 'AUXILIAR') ORDER BY cuenta";
                            /* $sqlr="SELECT T1.estado, T1.cuenta, T1.ncuentaban, T1.tipo, T2.razonsocial, T1.tercero FROM tesobancosctas AS T1 INNER JOIN terceros AS T2 ON T1.tercero = T2.cedulanit WHERE T1.estado = 'S' ORDER BY T1.cuenta"; */
                            $res = mysqli_query($linkbd, $sqlr);
                            while ($row = mysqli_fetch_row($res)) {
                                $inicioCuenta = substr($row[0], 0, 6);
                                $tipo_cuenta = 'Ahorros';
                                if ($inicioCuenta == '111005') {
                                    $tipo_cuenta = 'Corrientes';
                                }
                                if ($row[0] == $_POST['cuenta']) {
                                    echo "<option value='$row[0]' SELECTED>$row[0] - $row[1] - Cuenta $tipo_cuenta</option>";
                                    $_POST['nbanco'] = $row[1];
                                    /* $_POST['ter'] = $row[5];
                                                                     $_POST['cb'] = $row[2]; */
                                    $_POST['ncuenta'] = $row[0];
                                } else {
                                    echo "<option value='$row[0]'>$row[0] - $row[1] - Cuenta $tipo_cuenta</option>";
                                }
                            }
                        } else {
                            $sqlr = "SELECT T2.estado, T2.cuentant, T2.ncuentaban, T2.tipo, T1.razonsocial, T2.tercero
									FROM terceros AS T1 INNER JOIN tesobancosctas AS T2 ON T2.tercero = T1.cedulanit WHERE T2.estado='S' ORDER BY T2.cuentant";
                            $res = mysqli_query($linkbd, $sqlr);
                            while ($row = mysqli_fetch_row($res)) {
                                if ($row[1] == $_POST['cuenta']) {
                                    echo "<option value='$row[1]' SELECTED>$row[1]-$row[4] $row[2] - Cuenta $row[3]</option>";
                                    $_POST['nbanco'] = $row[4];
                                    $_POST['ter'] = $row[5];
                                    $_POST['cb'] = $row[2];
                                    $_POST['ncuenta'] = $_POST['cuenta'];
                                } else {
                                    echo "<option value='$row[1]'>$row[1]-$row[4] $row[2] - Cuenta $row[3]</option>";
                                }
                            }
                        }
                        ?>
                    </select>
                    <input type="hidden" value="0" name="bc" />
                    <input type="hidden" value="<?php echo $_POST['cb'] ?>" name="cb">
                    <input type="hidden" value="<?php echo $_POST['nbanco'] ?>" name="nbanco">

                </td>
                <td colspan="1"><input type="button" style="width:100%" name="generar" value="Conciliar"
                        onClick="buscarconciliar()"><input type="hidden" value="0" name="bconc" id="bconc"></td>
                <td class="saludo3">Cuenta:</td>
                <td><input name="ncuenta" id="ncuenta" type="text" value="<?php echo $_POST['ncuenta'] ?>"
                        style="width:100%" readonly></td>
            </tr>
            <tr>
                <td class="saludo3">No Conciliados</td>
                <td style="width:5%">Debitos:</td>
                <td colspan="2"><input name="ncdebitos" id="ncdebitos" type="text"
                        value="<?php echo $_POST['ncdebitos'] ?>" style="width:100%" readonly /></td>
                <td style="width:10%">Creditos:</td>
                <td style="width:10%"><input name="nccreditos" id="nccreditos" type="text" style="width:100%" readonly
                        value="<?php echo $_POST['nccreditos'] ?>" /></td>
                <td class="saludo3" style="width:8%">Conciliados</td>
                <td style="width:10%">Debitos:</td>
                <td><input id="cdebitos" name="cdebitos" type="text" readonly style="width:100%"
                        value="<?php echo $_POST['cdebitos'] ?>" /></td>
                <td style="width:5%">Creditos:</td>
                <td><input name="ccreditos" id="ccreditos" type="text" readonly style="width:100%"
                        value="<?php echo $_POST['ccreditos'] ?>" /></td>
            </tr>
            <tr>
                <td class="saludo3">Saldo I Libros</td>
                <td><input name="saldoini" id="saldoini" type="text" readonly value="<?php echo $_POST['saldoini'] ?>"
                        style="width:100%"></td>
                <td class="saludo3">Saldo F Libros</td>
                <td><input name="saldofin" id="saldofin" type="text" readonly value="<?php echo $_POST['saldofin'] ?>"
                        style="width:100%"></td>
                <td class="saludo3">- Debitos NC</td>
                <td><input name="debnc" id="debnc" type="text" readonly value="<?php echo $_POST['debnc'] ?>"
                        style="width:100%"></td>
                <td class="saludo3">+ Creditos NC</td>
                <td><input name="crednc" id="crednc" type="text" readonly value="<?php echo round($_POST['crednc'], 2) ?>"
                        style="width:100%"></td>
                <td class="saludo3" colspan="2">Saldo Extracto Calc:</td>
                <td><input name="extracto" id="extracto" type="text" readonly
                        value="<?php echo round($_POST['extracto'], 2) ?>" style="width:100%"><input name='ban'
                        type="hidden" value='<?php echo $_POST['ban'] ?>'></td>
            </tr>
        </table>
        <?php
        if ($_POST['oculto'] == '2') {
            $fec = date("Y-m-d");
            $fechaf = $_POST['fecha'];
            $fechaf2 = $_POST['fecha2'];
            $sqlr = "DELETE FROM conciliacion_cab WHERE cuenta = '" . $_POST['cuenta'] . "' AND periodo1 ='$fechaf' AND periodo2 = '$fechaf2'";
            mysqli_query($linkbd, $sqlr);
            $sqlr = "INSERT INTO conciliacion_cab (cuenta, extractobanc, extractocalc, diferencia, fecha, vigencia, periodo1, periodo2) values ('" . $_POST['cuenta'] . "', '" . $_POST['extractofis'] . "', '" . $_POST['extracto'] . "', '" . $_POST['difextracto'] . "', '$fec', '$vigusu', '$fechaf', '$fechaf2')";
            mysqli_query($linkbd, $sqlr);
            for ($y = 0; $y < count($_POST['detalles']); $y++) {
                $dnc = esta_en_array($_POST['conciliados'], $_POST['detalles'][$y]);
                if ($dnc != '1') {
                    $sqlr = "DELETE FROM conciliacion WHERE id_comp = '" . $_POST['compro'][$y] . "' AND periodo2 BETWEEN '$fechaf'AND '$fechaf2' AND cuenta = '" . $_POST['cuenta'] . "'";
                    mysqli_query($linkbd, $sqlr);
                }
                for ($x = 0; $x < count($_POST['conciliados']); $x++) {
                    if ($_POST['detalles'][$y] == $_POST['conciliados'][$x]) {
                        $sqlr = "INSERT INTO conciliacion (id_comp, cuenta, id_det, valordeb, valorcred, fecha, vigencia, periodo1, periodo2) values ('" . $_POST['compro'][$y] . "', '" . $_POST['cuenta'] . "', " . $_POST['conciliados'][$x] . ", " . $_POST['debitos'][$y] . ", " . $_POST['creditos'][$y] . ", '$fec', '$vigusu', '$fechaf', '$fechaf2')";
                        if (!mysqli_query($linkbd, $sqlr)) {
                        } else {
                            echo "<script>despliegamodalm('visible','3','Se ha Conciliado el detalle " . $_POST['ncompro'][$y] . " - Id: " . $_POST['conciliados'][$x] . " con Exitoo');</script>";
                        }
                    }
                }
            }
        }
        ?>
        <div class="subpantallac5" style="height:55.8%; width:99.6%; overflow-x:hidden;">
            <?php
            if ($_POST['banco'] != "-1") {
                $fecha124 = explode("-", $_POST['fecha']);
                $f1 = mktime(0, 0, 0, $fecha124[2], $fecha124[1], $fecha124[0]);
                $fecha14 = explode("-", $_POST['fecha2']);
                $f2 = mktime(0, 0, 0, $fecha14[2], $fecha14[1], $fecha14[0]);
                $sumad = 0;
                $sumac = 0;
                $fechaf = $_POST['fecha'];
                $fechaf2 = $_POST['fecha2'];
                $nc = buscacuenta($_POST['cuenta']);
                echo "
						<table class='inicio' >
							<tr><td colspan='11' class='titulos'>Conciliar Cuentas</td></tr>
							<tr>
								<td class='titulos2'>Id</td>
								<td class='titulos2'>Fecha</td>
								<td class='titulos2'>Tipo Comp</td>
								<td class='titulos2'>Comp</td>
								<td class='titulos2'>CC</td>
								<td class='titulos2'>Tercero</td>
								<td class='titulos2'>Detalle</td>
								<td class='titulos2'>Cheque</td>
								<td class='titulos2'>Debito</td>
								<td class='titulos2'>Credito</td>
								<td class='titulos2'><input id='todos' type='checkbox' name='todos' value='1' onClick='checktodos()' $chekt> Act/Des</td>
							</tr>";

                if ($_POST['fecha'] != '2018-01-01') {
                    $ajusteConvergencia = " AND T1.tipo_comp <>'104' ";
                }
                $sqlr = "SELECT T1.id_comp, T1.numerotipo, T1.tipo_comp, T1.fecha, T1.concepto, T2.id_det, T2.cuenta, T2.tercero, T2.centrocosto, T2.cheque, sum(T2.valdebito), sum(T2.valcredito) FROM comprobante_cab AS T1 INNER JOIN comprobante_det AS T2 ON T2.tipo_comp = T1.tipo_comp AND T2.numerotipo = T1.numerotipo WHERE T2.cuenta = '" . $_POST['cuenta'] . "' AND T1.estado = '1' AND T1.tipo_comp <>'7' AND T1.tipo_comp <> '102' AND T1.tipo_comp <> '103'" . $ajusteConvergencia . " AND T1.fecha BETWEEN '' AND '$fechaf2' GROUP BY T2.tipo_comp, T2.numerotipo ORDER BY T1.fecha";
                /*if($_POST['cuenta'] != '' && $_POST['cuenta'] != -1)
                                  {
                                      $sqlr ="DROP VIEW vista_conciliacion";
                                      mysqli_query($linkbd,$sqlr);
                                      $sqlr ="CREATE VIEW vista_conciliacion AS
                                      SELECT T1.id_comp, T1.numerotipo, T1.tipo_comp, T1.fecha, T1.concepto, T2.id_det, T2.cuenta, T2.tercero, T2.centrocosto, T2.cheque, sum(T2.valdebito) AS sumadebito, sum(T2.valcredito) AS sumacredito
                                      FROM comprobante_cab AS T1
                                      INNER JOIN comprobante_det AS T2
                                      ON T2.tipo_comp = T1.tipo_comp AND T2.numerotipo = T1.numerotipo
                                      WHERE T2.cuenta = '".$_POST['cuenta']."' AND T1.estado = '1' AND T1.tipo_comp <>'7' AND T1.tipo_comp <> '102' AND T1.tipo_comp <> '103' AND T1.tipo_comp <>'104' AND T1.fecha BETWEEN '' AND '$fechaf2'
                                      GROUP BY T2.tipo_comp, T2.numerotipo
                                      ORDER BY T1.fecha";
                                      mysqli_query($linkbd,$sqlr);
                                      $sqlr = "SELECT * FROM vista_conciliacion";
                                  }
                                  else {$sqlr = "";}*/
                $res = mysqli_query($linkbd, $sqlr);
                $i = 0;
                $iter = "zebra1";
                $iter2 = "zebra2";
                while ($row = mysqli_fetch_row($res)) {
                    $sqlr = "SELECT * FROM tipo_comprobante WHERE codigo = '$row[2]'";
                    $res2 = mysqli_query($linkbd, $sqlr);
                    $row2 = mysqli_fetch_row($res2);
                    $chk = "";
                    $fecon = "";
                    $concant = "";
                    $ch = esta_en_array($_POST['conciliados'], $row[5]);
                    if ($ch == 1) {
                        $chk = "checked";
                    } else {
                        $chk = "";
                    }
                    preg_match("/([0-9]{4})\-([0-9]{2})\-([0-9]{2})/", $row[3], $fecha);
                    $fcc = mktime(0, 0, 0, $fecha[2], $fecha[3], $fecha[1]);
                    $sqlr = "SELECT count(*),periodo2 FROM conciliacion WHERE id_comp = '$row[2] $row[1]' AND cuenta = '" . $_POST['cuenta'] . "' AND periodo2 BETWEEN '$fechaf' AND '$fechaf2'";
                    $vc = 0;
                    $res3 = mysqli_query($linkbd, $sqlr);
                    $row3 = mysqli_fetch_row($res3);
                    $vc = $row3[0];
                    $ncc = '';
                    if ($vc >= 1) {
                        $chk = "checked";
                        $ncc = 'S';
                        $fecon = $row3[1];
                    } else {
                        $fecon = "";
                        if ($f1 < $fcc) {
                            $ncc = 'S';
                        } else {
                            if ($row3[0] == 0) {
                                $ncc = 'S';
                            } else {
                                $ncc = 'N';
                            }
                        }
                        $sqlr = "SELECT count(*), periodo2 FROM conciliacion WHERE id_comp LIKE '$row[2] $row[1]' AND cuenta = '" . $_POST['cuenta'] . "' AND periodo2 <= '$fechaf'";
                        $res4 = mysqli_query($linkbd, $sqlr);
                        $row4 = mysqli_fetch_row($res4);
                        if ($row4[0] > 0) {
                            $ncc = 'N';
                        }
                    }

                    $nt = "";
                    $sqltercero = "SELECT * FROM terceros WHERE cedulanit = '$row[7]' AND estado = 'S'";
                    $restercero = mysqli_query($linkbd, $sqltercero);
                    while ($rtercero = mysqli_fetch_row($restercero)) {
                        if ($rtercero[16] == '1') {
                            $nt = $rtercero[5];
                        } else {
                            $nt = "$rtercero[3] $rtercero[4] $rtercero[1] $rtercero[2]";
                        }
                    }
                    //$nt = buscatercero($row[7]);
                    if ($ncc == 'S') {
                        echo "
								<tr id='fila$row[10]' class='$iter'>
									<td>$row[5]</td>
									<td>$row[3]<input type='hidden' name='dfechas[]' value='$row[3]'></td>
									<td>$row2[1] <input type='hidden' name='ncompro[]' value='$row2[1]'></td>
									<td>$row[1]<input type='hidden' name='compro[]' value='$row[2] $row[1]'><input type='hidden' name='compro2[]' value='$row[1]'></td>
									<td>$row[8]</td>
									<td>" . substr($nt, 0, 50) . "<input type='hidden' name='dterceros[]' value='$nt'></td>
									<td>" . substr($row[4], 0, 50) . "</td>
									<td>$row[9]</td>
									<td style='text-align:right;'>" . number_format($row[10], 2) . " <input type='hidden' name='debitos[]' value='$row[10]'></td>
									<td style='text-align:right;'>" . number_format($row[11], 2) . " <input type='hidden' name='creditos[]' value='$row[11]'></td>
									<td><input type='hidden' name='detalles[]' value='$row[5]'><input type='checkbox' name='conciliados[]' value='$row[5]' $chk onClick='marcar($row[5],$i);' class='defaultcheckbox'> ";
                        if ($chk != "" and $chk != NULL) {
                            echo "<img src='imagenes/confirm2.png' style='width:14px' title='Conciliado'> $fecon ";
                        } else {
                            $concant = 0;
                            $concant = buscaconciliacion($row[2], $row[1], $fechafa, $fechaf2, $_POST['cuenta']);
                            $feconc = buscaconciliacion_fecha($row[2], $row[1], $fechafa, $fechaf2, $_POST['cuenta']);
                            if ($concant >= 1) {
                                echo "<input type='hidden' name='conciliadoanterior[]'  value='$row[5]'>";
                                echo "<input type='text' name='conciliadoanterior_fecha[]' class='inpnovisibles' size='10'  value='$feconc' readonly>";
                            }
                        }
                        $aux = $iter;
                        $iter = $iter2;
                        $iter2 = $aux;
                        echo "</td></tr>";
                        $sumad += $row[10];
                        $sumac += $row[11];
                        $i += 1;
                    }
                }
                echo "<tr><td colspan='7'></td><td>Totales:</td><td class='saludo3'  style='text-align:right;'>$" . number_format($sumad, 2) . "<input type='hidden' name='tdebito' value='$sumad'></td><td class='saludo3' style='text-align:right;'>$" . number_format($sumac, 2) . "<input type='hidden' name='tcredito' value='$sumac'></td></tr>";
                echo "</table>";
                echo "<script> sumarconc();</script> <script>document.getElementById('divcarga').style.display='none';</script>";
            }
            ?>
        </div>
    </form>
</body>

</html>
