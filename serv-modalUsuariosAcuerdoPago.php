<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require 'comun.inc';
	require 'funciones.inc';
    require 'funcionesSP.inc.php';
	$linkbd = conectar_v7();
    $linkbd -> set_charset("utf8"); 
    titlepag();
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es">
	<head>
		<meta http-equiv="Content-Type" content="text/html" charset="utf8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=9"/>
		<title>:: Usuarios</title>
		<link href="css/css2.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
        <link href="css/cssSP.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<script type="text/javascript" src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
        <script type="text/javascript" src="jquery-1.11.0.min.js"></script> 
		<script src="sweetalert2/dist/sweetalert2.min.js"></script>
		<link rel="stylesheet" href="sweetalert2/dist/sweetalert2.min.css">
		<script>
			function agregar(nombre, documento, numeroFactura, valor, id_cliente, codUsuario, valor2, validaAcuerdo)
			{
				if(validaAcuerdo == 'N')
				{
					parent.document.form2.id_cliente.value = id_cliente;
					parent.document.form2.nombreSuscriptor.value = nombre;
					parent.document.form2.documento.value = documento;
					parent.document.form2.numeroFactura.value = numeroFactura;
					parent.document.form2.codUsuario.value = codUsuario;
					parent.document.form2.valorFactura.value = valor2;
					parent.document.form2.valorFacturaVisible.value = valor;
					parent.document.form2.valorAbono.value = '';
					parent.document.form2.valorvl.value = '';
					parent.document.form2.valorAcuerdo.value = '';
					parent.document.form2.numeroCuotas.value = '';
					parent.document.form2.valorCuota.value = '';
					parent.despliegamodal("hidden");
				}
				else
				{
					Swal.fire({
						icon: 'error',
						title: 'El usuario tiene un acuerdo de pago activo'
					})
				}
				
			}

            $(window).load(function () {
				$('#cargando').hide();
			});
		</script> 
	</head>
	<body>
		<form name="form2" method="post">

			<?php
				if(@ $_POST['oculto'] == "")
				{
					$_POST['numres'] = 10;
					$_POST['numpos'] = 0;
					$_POST['nummul'] = 0;
				}
			?>

			<table class="inicio ancho" style="width:99.5%">
				<tr>
					<td class="titulos" colspan="3">Usuarios - Servicios Publicos</td>
					<td class="cerrar" style="width:7%" onClick="parent.despliegamodal('hidden');">Cerrar</td>
				</tr>
				
				<tr>
					<td class="tamano01" style='width:3cm;'>Código Usuario o Número Factura:</td>
					<td>
						<input type="search" name="codnom" id="codnom" value="<?php echo @$_POST['codnom'];?>" style='width:100%;'/>
					</td>

					<td style="padding-bottom:0px;height:35px;"><em class="botonflecha" onClick="document.form2.submit();">Buscar</em></td>
				</tr>
			</table>

            <div id="cargando" style=" position:absolute;left: 46%; bottom: 45%">
				<img src="imagenes/loading.gif" style=" width: 80px; height: 80px"/>
			</div>

			<div class="subpantalla" style="height:82%; width:99.2%; overflow-x:hidden;">
				<table class='inicio' align='center' width='99%'>	
					<tr>
						<td colspan='4' class='titulos'>.: Resultados Busqueda:</td>
							<td class='submenu' style="width: 10%;">
								<select name="renumres" id="renumres" class="centrarSelect" onchange="cambionum();" style="width: 99%;">
									<option value='10' class="aumentarTamaño" <?php if ($_POST['renumres'] == '10') echo 'selected'; ?> >10</option>
									<option value='20' class="aumentarTamaño" <?php if ($_POST['renumres'] == '20') echo 'selected'; ?>>20</option>
									<option value='30' class="aumentarTamaño" <?php if ($_POST['renumres'] == '30') echo 'selected'; ?>>30</option>
									<option value='50' class="aumentarTamaño" <?php if ($_POST['renumres'] == '50') echo 'selected'; ?>>50</option>
									<option value='100' class="aumentarTamaño" <?php if ($_POST['renumres'] == '100') echo 'selected'; ?>>100</option>
									<option value='-1' class="aumentarTamaño" <?php if ($_POST['renumres'] == '100') echo 'selected'; ?>>Todos</option>
								</select>
							</td>
						</td>
					</tr>

					<tr class='titulos2' style='text-align:center;'>
						<td style="width: 10%;">Código Usuario</td>
						<td>Nombre Suscriptor</td>
						<td style="width: 15%;">Documento Suscriptor</td>
						<td style="width: 15%;">Número Factura</td>
                        <td style="width: 15%;">Valor</td>
					</tr>

					<?php
						$crit1="";
						if (@$_POST['codnom']!="")
                        {
                            $crit1 = "AND C.cod_usuario LIKE '$_POST[codnom]' ";
                        }

						$cond2 = "";

						if (@ $_POST['numres'] != "-1")
						{
							$cond2 = " LIMIT ".$_POST['numpos'].", ".$_POST['numres']; 
						}

                        $corteActual = buscaCorteActivo();

						$sqlUsuarios = "SELECT CD.id_cliente, CD.numero_facturacion, C.cod_usuario, C.id_tercero FROM srvcortes_detalle AS CD INNER JOIN srvclientes AS C ON CD.id_cliente = C.id WHERE CD.id_corte = $corteActual AND CD.estado_pago = 'S' $crit1 ORDER BY CD.numero_facturacion ASC $cond2";
						$resUsuarios = mysqli_query($linkbd,$sqlUsuarios);
						$encontrados = mysqli_num_rows($resUsuarios);

						$iter  = 'saludo1a';
						$iter2 = 'saludo2';
						$conta = 1;

						while ($rowUsuarios = mysqli_fetch_row($resUsuarios))
						{
                            $nombreSuscriptor = buscaNombreTerceroConId($rowUsuarios[3]);
                            $documentoSuscriptor = buscaDocumentoTerceroConId($rowUsuarios[3]);
                            $valor = consultaValorTotalSRVFACTURAS($rowUsuarios[1]);
                            $valor = ($valor / 100);
                            $valor = ceil($valor);
                            $valor = $valor * 100;
                            $valor2 = $valor;
                            $valor = number_format($valor, 2, '.', ',');	

							$acuerdoPago = "N";

							$sqlBuscaAcuerdo = "SELECT estado_acuerdo FROM srv_acuerdo_cab WHERE id_cliente = $rowUsuarios[0] AND estado_acuerdo = 'Activo'";
							$rowBuscaAcuerdo = mysqli_num_rows(mysqli_query($linkbd, $sqlBuscaAcuerdo));

							if($rowBuscaAcuerdo != 0)
							{
								$acuerdoPago = "S";
							}
					?>
							<tr class='<?php echo $iter ?>' style='text-align:center; text-transform:uppercase;' onclick="agregar('<?php echo $nombreSuscriptor; ?>', '<?php echo $documentoSuscriptor; ?>', '<?php echo $rowUsuarios[1]; ?>', '<?php echo $valor; ?>', '<?php echo $rowUsuarios[0]; ?>', '<?php echo $rowUsuarios[2]; ?>', '<?php echo $valor2; ?>', '<?php echo $acuerdoPago; ?>')">

                                <td><?php echo $rowUsuarios[2] ?></td>
                                <td><?php echo $nombreSuscriptor ?></td>
                                <td><?php echo $documentoSuscriptor ?></td>
                                <td><?php echo $rowUsuarios[1] ?></td>
                                <td><?php echo $valor ?></td>
							</tr>
					<?php
							$aux=$iter;
							$iter=$iter2;
							$iter2=$aux;
							$conta++;
						}

						if ($encontrados == 0)
						{
					?>
							<table class='inicio'>
								<tr>
									<td class='saludo1' style='text-align:center;'>
										<img src='imagenes\alert.png' style='width:25px; height: 30px;'> No se han encontrado registros <img src='imagenes\alert.png' style='width:25px; height: 30px;'>
									</td>
								</tr>
							</table>
					<?php
						}
					?>
				</table>
			</div>

			<input type="hidden" name="oculto" id="oculto" value="1">
			<input type="hidden" name="numres" id="numres" value="<?php echo $_POST['numres'];?>"/>
			<input type="hidden" name="numpos" id="numpos" value="<?php echo $_POST['numpos'];?>"/>
			<input type="hidden" name="nummul" id="nummul" value="<?php echo $_POST['nummul'];?>"/>
		</form>
	</body>
</html>
