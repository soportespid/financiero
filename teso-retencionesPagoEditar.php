<?php

    header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require 'comun.inc';
	require 'funciones.inc';
	session_start();
    if(empty($_SESSION)){
        header("location: index.php");
    }
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html lang=es>
	<head>
    <meta charset="utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1"/>
		<meta name="viewport" content="user-scalable=no">
		<title>:: IDEAL 10 - Tesorería</title>
		<link href="favicon.ico" rel="shortcut icon"/>
		<link href="css/css2z.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
        <link href="css/tabs.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
        <link href="css/style.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" href="sweetalert2/dist/sweetalert2.min.css">
        <script type="text/javascript" src="css/programas.js"></script>
        <script src="sweetalert2/dist/sweetalert2.min.js"></script>
		<script src="vue/vue.min.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script src="css/calendario.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
	</head>
	<body>
		<header>
			<table>
				<tr><script>barra_imagenes("teso");</script><?php cuadro_titulos();?></tr>
			</table>
		</header>

		<form name="form2" method="post" action="">
			<section id="myapp" v-cloak >
                <input type="hidden" value = "3" ref="pageType">
                <div class="loading-container" v-show="isLoading" >
                    <p class="text-loading"data-loading="IDEAL 10">IDEAL 10</p>
                </div>
				<nav>
					<table>
						<tr><?php menu_desplegable("teso");?></tr>
					</table>
                    <div class="bg-white group-btn p-1">
                        <button type="button" @click="window.location.href='teso-retencionesPagoCrear'" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
                            <span>Nuevo</span>
                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960">
                                <path d="M440-440H200v-80h240v-240h80v240h240v80H520v240h-80v-240Z"></path>
                            </svg>
                        </button>
                        <button type="button" @click="save()" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
                            <span>Guardar</span>
                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960">
                                <path
                                    d="M840-680v480q0 33-23.5 56.5T760-120H200q-33 0-56.5-23.5T120-200v-560q0-33 23.5-56.5T200-840h480l160 160Zm-80 34L646-760H200v560h560v-446ZM480-240q50 0 85-35t35-85q0-50-35-85t-85-35q-50 0-85 35t-35 85q0 50 35 85t85 35ZM240-560h360v-160H240v160Zm-40-86v446-560 114Z">
                                </path>
                            </svg>
                        </button>
                        <button type="button" @click="window.location.href='teso-retencionesPagoBuscar'" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
                            <span>Buscar</span>
                            <svg class="fill-black group-hover:fill-white w-5 h-5" xmlns="http://www.w3.org/2000/svg"
                                viewBox="0 -960 960 960">
                                <path
                                    d="M784-120 532-372q-30 24-69 38t-83 14q-109 0-184.5-75.5T120-580q0-109 75.5-184.5T380-840q109 0 184.5 75.5T640-580q0 44-14 83t-38 69l252 252-56 56ZM380-400q75 0 127.5-52.5T560-580q0-75-52.5-127.5T380-760q-75 0-127.5 52.5T200-580q0 75 52.5 127.5T380-400Z">
                                </path>
                            </svg>
                        </button>
                        <button type="button" onclick="mypop=window.open('plan-agenda','','');mypop.focus()" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
                            <span class="group-hover:text-white">Agenda</span>
                            <svg class="fill-black group-hover:fill-white w-5 h-5" xmlns="http://www.w3.org/2000/svg"
                                viewBox="0 -960 960 960">
                                <path
                                    d="M560-564v-68q33-14 67.5-21t72.5-7q26 0 51 4t49 10v64q-24-9-48.5-13.5T700-600q-38 0-73 9.5T560-564Zm0 220v-68q33-14 67.5-21t72.5-7q26 0 51 4t49 10v64q-24-9-48.5-13.5T700-380q-38 0-73 9t-67 27Zm0-110v-68q33-14 67.5-21t72.5-7q26 0 51 4t49 10v64q-24-9-48.5-13.5T700-490q-38 0-73 9.5T560-454ZM260-320q47 0 91.5 10.5T440-278v-394q-41-24-87-36t-93-12q-36 0-71.5 7T120-692v396q35-12 69.5-18t70.5-6Zm260 42q44-21 88.5-31.5T700-320q36 0 70.5 6t69.5 18v-396q-33-14-68.5-21t-71.5-7q-47 0-93 12t-87 36v394Zm-40 118q-48-38-104-59t-116-21q-42 0-82.5 11T100-198q-21 11-40.5-1T40-234v-482q0-11 5.5-21T62-752q46-24 96-36t102-12q58 0 113.5 15T480-740q51-30 106.5-45T700-800q52 0 102 12t96 36q11 5 16.5 15t5.5 21v482q0 23-19.5 35t-40.5 1q-37-20-77.5-31T700-240q-60 0-116 21t-104 59ZM280-494Z">
                                </path>
                            </svg>
                        </button>
                        <button type="button" onclick="mypop=window.open('teso-principal','','');mypop.focus();" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
                            <span>Nueva ventana</span>
                            <svg class="fill-black group-hover:fill-white w-5 h-5" xmlns="http://www.w3.org/2000/svg"
                                viewBox="0 -960 960 960">
                                <path
                                    d="M320-120v-80h80v-80H160q-33 0-56.5-23.5T80-360v-400q0-33 23.5-56.5T160-840h640q33 0 56.5 23.5T880-760v400q0 33-23.5 56.5T800-280H560v80h80v80H320ZM160-360h640v-400H160v400Zm0 0v-400 400Z">
                                </path>
                            </svg>
                        </button>
                        <button type="button" @click="window.location.href='teso-retencionesPagoBuscar'" class="btn btn-success btn-success-hover d-flex justify-between align-items-center">
                            <span>Atras</span>
                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960">
                                <path d="M760-200v-160q0-50-35-85t-85-35H273l144 144-57 56-240-240 240-240 57 56-144 144h367q83 0 141.5 58.5T840-360v160h-80Z"></path>
                            </svg>
                        </button>
                    </div>
				</nav>
				<article class="bg-white">
                    <!--TABS-->
                    <div ref="rTabs" class="nav-tabs p-1">
                        <div class="nav-item active" @click="showTab(1)">Retenciones pago</div>
                        <div class="nav-item" v-show="objCuenta.codigo != '' || arrDetallesFuente.length > 0" @click="showTab(2)">Fuentes</div>
                    </div>
                    <!--CONTENIDO TABS-->
                    <div ref="rTabsContent" class="nav-tabs-content">
                        <div class="nav-content active">
                            <div>
                                <h2 class="titulos m-0">Retenciones pago editar</h2>
                                <p class="ms-2">Todos los campos con (<span class="text-danger fw-bolder">*</span>) son obligatorios</p>
                                <div class="d-flex">
                                    <div class="form-control w-25">
                                        <label class="form-label" for="">Código:</label>
                                        <div class="d-flex">
                                            <button type="button" class="btn btn-primary" @click="nextItem('prev')"><</button>
                                            <input type="text"  style="text-align:center;" v-model="txtConsecutivo" @change="nextItem()">
                                            <button type="button" class="btn btn-primary" @click="nextItem('next')">></button>
                                        </div>
                                    </div>
                                    <div class="form-control">
                                        <label class="form-label" for="">Nombre <span class="text-danger fw-bolder">*</span>:</label>
                                        <input type="text" v-model="txtNombre">
                                    </div>
                                    <div class="form-control w-50">
                                        <label class="form-label" for="">Valor de retención <span class="text-danger fw-bolder">*</span>:</label>
                                        <input type="number" v-model="txtRetencion" min="0" max="100" >
                                    </div>
                                    <div class="form-control w-25">
                                        <label class="form-label">Tipo:</label>
                                        <select v-model="selectTipo" @change="changeTipo()">
                                            <option value="S">Simple</option>
                                            <option value="C">Compuesto</option>
                                        </select>
                                    </div>
                                    <div class="form-control w-25">
                                        <label class="form-label">Destino:</label>
                                        <select v-model="selectDestino" >
                                            <option value="">Otros</option>
                                            <option value="N">Nacional</option>
                                            <option value="D">Departamental</option>
                                            <option value="M">Municipal</option>
                                        </select>
                                    </div>
                                    <div class="form-control w-50" v-if="selectDestino == 'D'">
                                        <label class="form-label">Reconoce participación:</label>
                                        <select v-model="selectParticipacion">
                                            <option value="S">Si</option>
                                            <option value="N">No</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="d-flex">
                                    <div class="form-control justify-between">
                                        <label for="labelCheckName1" class="me-2"></label>
                                        <div class="d-flex align-items-center ">
                                            <label for="labelCheckName1" class="me-2">Terceros: </label>
                                            <label for="labelCheckName1" class="form-switch">
                                                <input type="checkbox" id="labelCheckName1" v-model="checkTerceros">
                                                <span></span>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="form-control justify-between">
                                        <label for="labelCheckName2" class="me-2"></label>
                                        <div class="d-flex align-items-center ">
                                            <label for="labelCheckName2" class="me-2">IVA: </label>
                                            <label for="labelCheckName2" class="form-switch">
                                                <input type="checkbox" id="labelCheckName2" v-model="checkIva">
                                                <span></span>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="form-control justify-between">
                                        <label for="labelCheckName3" class="me-2"></label>
                                        <div class="d-flex align-items-center ">
                                            <label for="labelCheckName3" class="me-2">Nómina: </label>
                                            <label for="labelCheckName3" class="form-switch">
                                                <input type="checkbox" id="labelCheckName3" v-model="checkNomina">
                                                <span></span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div>
                                <h2 class="titulos m-0">Detalle retención pago</h2>
                                <div>
                                    <div class="d-flex">
                                        <div class="form-control">
                                            <label class="form-label" for="labelSelectName2">Concepto contable causación:</label>
                                            <select id="labelSelectName2" v-model="selectConceptoCausa">
                                                <option value = "" selected>Seleccione</option>
                                                <option v-for="(data,index) in arrConceptosCausa" :key="index" :value="data.codigo">{{data.codigo+"-"+data.tipo+"-"+data.nombre}}</option>
                                            </select>
                                        </div>
                                        <div class="form-control">
                                            <label class="form-label" for="labelSelectName2">Concepto contable ingresos:</label>
                                            <select id="labelSelectName2" v-model="selectConceptoIngreso">
                                                <option value = "" selected>Seleccione</option>
                                                <option v-for="(data,index) in arrConceptosIngreso" :key="index" :value="data.codigo">{{data.codigo+"-"+data.tipo+"-"+data.nombre}}</option>
                                            </select>
                                        </div>
                                        <div class="form-control">
                                            <label class="form-label" for="labelSelectName2">Concepto contable SGR:</label>
                                            <select id="labelSelectName2" v-model="selectConceptoSgr">
                                                <option value = "" selected>Seleccione</option>
                                                <option v-for="(data,index) in arrConceptosSgr" :key="index" :value="data.codigo">{{data.codigo+"-"+data.tipo+"-"+data.nombre}}</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="d-flex">
                                        <div class="form-control" v-show="!checkTerceros">
                                            <label class="form-label" for="">Cuenta presupuestal:</label>
                                            <div class="d-flex">
                                                <input type="text"  class="colordobleclik w-25" @dblclick="isModal=true" @change="search('cod_cuenta')" v-model="objCuenta.codigo" >
                                                <input type="text" disabled readonly v-model="objCuenta.nombre">
                                            </div>
                                        </div>
                                        <div class="form-control" v-if="isClasificador">
                                            <label class="form-label" for="">{{ txtCuentaClasificadora}}:</label>
                                            <div class="d-flex">
                                                <input type="text"  class="colordobleclik w-25" @dblclick="isModalClasificador=true" @change="search('cod_clasificador')" v-model="objClasificador.codigo" >
                                                <input type="text" disabled readonly v-model="objClasificador.nombre">
                                            </div>
                                        </div>
                                        <div class="d-flex w-100" v-if="selectTipo == 'C'">
                                            <div class="form-control">
                                                <label class="form-label" for="">División de retención <span class="text-danger fw-bolder">*</span>:</label>
                                                <input type="number" v-model="txtCompRetencion" min="0" max="100" >
                                            </div>
                                            <div class="form-control">
                                                <label class="form-label" for="">Destino</label>
                                                <div class="d-flex">
                                                    <select v-model="selectCompDestino">
                                                        <option value="">Otros</option>
                                                        <option value="N">Nacional</option>
                                                        <option value="D">Departamental</option>
                                                        <option value="M">Municipal</option>
                                                    </select>
                                                    <button type="button" @click="add()" class="btn btn-primary">Agregar</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div v-if="selectTipo == 'C'">
                                <h2 class="titulos m-0">Detalle</h2>
                                <div class="table-responsive">
                                    <table  class="table fw-normal">
                                        <thead>
                                            <tr>
                                                <th>Item</th>
                                                <th>Concepto contable causación</th>
                                                <th>Concepto contable ingreso</th>
                                                <th>Concepto contable SGR</th>
                                                <th>Cuenta presupuestal</th>
                                                <th>Cuenta clasificadora</th>
                                                <th>Destino</th>
                                                <th>Porcentaje</th>
                                                <th>Opciones</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr v-for="(data,index) in arrDetalles" :key="index">
                                                <td>{{index+1}}</td>
                                                <td>{{data.causacion.codigo+"-"+data.causacion.tipo+"-"+data.causacion.nombre}}</td>
                                                <td>{{data.ingreso.codigo+"-"+data.ingreso.tipo+"-"+data.ingreso.nombre}}</td>
                                                <td>{{data.sgr.codigo+"-"+data.sgr.tipo+"-"+data.sgr.nombre}}</td>
                                                <td>{{data.cuenta.codigo+"-"+data.cuenta.nombre}}</td>
                                                <td>{{data.cuenta_clasificadora.codigo+"-"+data.cuenta_clasificadora.nombre}}</td>
                                                <td>{{data.destino.nombre}}</td>
                                                <td>{{data.porcentaje}}</td>
                                                <td><button type="button" class="btn btn-sm btn-danger" @click="del(index)">Eliminar</button></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="nav-content">
                            <div class="d-flex w-50">
                                <div class="form-control">
                                    <label class="form-label" for="">Fuente <span class="text-danger fw-bolder">*</span>:</label>
                                    <div class="d-flex">
                                        <input type="text"  class="colordobleclik w-25" @dblclick="isModalFuente=true" @change="search('cod_fuente')" v-model="objFuente.codigo" >
                                        <input type="text" disabled readonly v-model="objFuente.nombre">
                                        <button type="button" class="btn btn-primary" @click="addFuente()">Agregar</button>
                                    </div>
                                </div>
                            </div>
                            <div class="table-responsive table-sm">
                                <table  class="table fw-normal">
                                    <thead>
                                        <tr>
                                            <th>Item</th>
                                            <th>Fuente</th>
                                            <th>Opciones</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr v-for="(data,index) in arrDetallesFuente" :key="index">
                                            <td>{{index+1}}</td>
                                            <td>{{data.codigo+"-"+data.nombre}}</td>
                                            <td><button type="button" class="btn btn-sm btn-danger" @click="delFuente(index)">Eliminar</button></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
				</article>
                <!-- MODALES -->
                <div v-show="isModal" class="modal">
                    <div class="modal-dialog modal-lg" >
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title">Buscar cuentas</h5>
                                <button type="button" @click="isModal=false;" class="btn btn-close"><div></div><div></div></button>
                            </div>
                            <div class="modal-body">
                                <div class="d-flex flex-column">
                                    <div class="form-control m-0 mb-3">
                                        <input type="search" placeholder="Buscar" v-model="txtSearch" @keyup="search('modal')" id="labelInputName">
                                    </div>
                                    <div class="form-control m-0 mb-3">
                                        <label class="form-label" for="labelInputName">Resultados: <span class="fw-bold">{{txtResultados}}</span></label>
                                    </div>
                                </div>
                                <div class="overflow-auto max-vh-50 overflow-x-hidden " >
                                    <table class="table table-hover fw-normal">
                                        <thead>
                                            <tr>
                                                <th>Item</th>
                                                <th>Cuenta</th>
                                                <th>Nombre</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr v-for="(data,index) in arrCuentasCopy" @dblclick="selectItem(data)" :key="index">
                                                <td>{{index+1}}</td>
                                                <td>{{data.codigo}}</td>
                                                <td>{{data.nombre}}</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div v-show="isModalClasificador" class="modal">
                    <div class="modal-dialog modal-lg" >
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title">Buscar {{ txtTituloModal}}</h5>
                                <button type="button" @click="isModalClasificador=false;" class="btn btn-close"><div></div><div></div></button>
                            </div>
                            <div class="modal-body">
                                <div class="d-flex flex-column">
                                    <div class="form-control m-0 mb-3">
                                        <input type="search" placeholder="Buscar" v-model="txtSearchClasificador" @keyup="search('modal_clasificador')" id="labelInputName">
                                    </div>
                                    <div class="form-control m-0 mb-3">
                                        <label class="form-label" for="labelInputName">Resultados: <span class="fw-bold">{{txtResultadosClasificador}}</span></label>
                                    </div>
                                </div>
                                <div class="overflow-auto max-vh-50 overflow-x-hidden " >
                                    <table class="table table-hover fw-normal">
                                        <thead>
                                            <tr>
                                                <th>Item</th>
                                                <th>Cuenta</th>
                                                <th>Nombre</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr v-for="(data,index) in arrMostrarClasificadorasCopy" @dblclick="selectItem(data,'clasificador')" :key="index">
                                                <td>{{index+1}}</td>
                                                <td>{{data.codigo}}</td>
                                                <td>{{data.nombre}}</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div v-show="isModalFuente" class="modal">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title">Buscar fuentes</h5>
                                <button type="button" @click="isModalFuente=false;" class="btn btn-close"><div></div><div></div></button>
                            </div>
                            <div class="modal-body">
                                <div class="d-flex flex-column">
                                    <div class="form-control m-0 mb-3">
                                        <input type="search" placeholder="Buscar" v-model="txtSearchFuente" @keyup="search('modal_fuente')" id="labelInputName">
                                    </div>
                                    <div class="form-control m-0 mb-3">
                                        <label class="form-label" for="labelInputName">Resultados: <span class="fw-bold">{{txtResultadosFuente}}</span></label>
                                    </div>
                                </div>
                                <div class="overflow-auto max-vh-50 overflow-x-hidden " >
                                    <table class="table table-hover fw-normal">
                                        <thead>
                                            <tr>
                                                <th>Item</th>
                                                <th>Fuente</th>
                                                <th>Nombre</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr v-for="(data,index) in arrFuentesCopy" @dblclick="selectItem(data,'fuentes')" :key="index">
                                                <td>{{index+1}}</td>
                                                <td>{{data.codigo}}</td>
                                                <td>{{data.nombre}}</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
			</section>
		</form>
		<script src="node_modules/read-excel-file/bundle/read-excel-file.min.js"></script>
		<script src="node_modules/xlsx/dist/xlsx.full.min.js"></script>
        <script src="Librerias/vue/vue.min.js"></script>
		<script src="Librerias/vue/axios.min.js"></script>
		<script type="module" src="tesoreria/retenciones_pagos/teso-retencionesPagos.js?<?= date('d_m_Y_h_i_s');?>"></script>
	</body>
</html>
