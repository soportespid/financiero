<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require 'comun.inc';
	require 'funciones.inc';
	$linkbd=conectar_v7();
	$linkbd -> set_charset("utf8");
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html lang=es>
	<head>
		<meta charset="utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1"/>
		<meta name="viewport" content="user-scalable=no">
		<title>:: SieS</title>
		<link href="css/css2z.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="sweetalert2/dist/sweetalert2.min.css" rel="stylesheet" />
		<script src="sweetalert2/dist/sweetalert2.min.js"></script>
		<script type="text/javascript" src="css/calendario.js"></script>
		<script type="text/javascript" src="css/programas.js"></script>
		<script>
			function guardar (){
				Swal.fire({
					icon: 'question',
					title: '¿Seguro que desea guardar?',
					showDenyButton: true,
					confirmButtonText: 'Guardar',
					confirmButtonColor: '#01CC42',
					denyButtonText: 'Cancelar',
					denyButtonColor: '#FF121A',
				}).then(
					(result) => {
						if (result.isConfirmed){
							document.form2.oculto.value = 2; 
							document.form2.submit();
						}
						else if (result.isDenied){
							Swal.fire({
								icon: 'info',
								title: 'No se guardo la información',
								confirmButtonText: 'Continuar',
								confirmButtonColor: '#FF121A',
								timer: 2500
							});
						}
					}
				)
			}
			function agregar(){
				if(document.getElementById('dsectorial').value != "" && document.getElementById('desEsp').value != "" && document.getElementById('tNorma').value != "" && document.getElementById('dfuentes').value != "" && document.getElementById('numnorma').value != "" && document.getElementById('porcentaje').value != ""){
					document.form2.oculto.value = "4";
					document.form2.submit();
				}else {
					Swal.fire({
						icon: 'error',
						title: 'Error!',
						text: 'Faltan datos para poder agregar',
						confirmButtonText: 'Continuar',
						confirmButtonColor: '#FF121A',
						timer: 2500
					});
				}
			}
			function eliminar(variable){
				Swal.fire({
					icon: 'question',
					title: '¿Seguro que quiere eliminar?',
					showDenyButton: true,
					confirmButtonText: 'Guardar',
					confirmButtonColor: '#01CC42',
					denyButtonText: 'Cancelar',
					denyButtonColor: '#FF121A',
				}).then(
					(result) => {
						if (result.isConfirmed){
							document.getElementById('elimina').value = variable;
							document.form2.oculto.value="3";
							document.form2.submit();
						}
						else if (result.isDenied){
							Swal.fire({
								icon: 'info',
								title: 'No se elimino',
								confirmButtonText: 'Continuar',
								confirmButtonColor: '#FF121A',
								timer: 2500
							});
						}
					}
				)
			}
		</script> 
		<?php titlepag();?>
	</head>
	<body>
		<form name="form2" method="post">
			<?php
				if($_POST['oculto']==""){
					$_POST['ncuenta'] = $_GET['cuenta'];
					$sql = "SELECT cuenta, detalle_sectorial FROM ccpecuentastiponormas_cab WHERE cuenta = '".$_POST['ncuenta']."'";
					$res = mysqli_query($linkbd,$sql);
					$_POST['nuevodoc'] = mysqli_num_rows($res);
					if($_POST['nuevodoc'] > 0){
						$row = mysqli_fetch_row($res);
						$_POST['dsectorial'] = $row[1];
						$sqld = "SELECT aplicade, tiponorma, numeronorma, fechanorma, detalle_sectorial, fuentes, porcentaje FROM ccpecuentastiponormas WHERE cuenta = '".$_POST['ncuenta']."'";
						$resd = mysqli_query($linkbd,$sqld);
						while ($rowd = mysqli_fetch_row($resd)){
							$_POST['vddestiesp'][] = $rowd[0];
							$_POST['dvtipnorma'][] = $rowd[1];
							$_POST['dvfuentes'][] = $rowd[5];
							$_POST['dvnnorma'][] = $rowd[2];
							$_POST['dvfecha'][] = $rowd[3];
							$_POST['dvprocentaje'][] = $rowd[6];
							switch($rowd[1]) {
								case 0: $_POST['dvtipnormaxx'][] = '0 - NO APLICA';break;
								case 1: $_POST['dvtipnormaxx'][] = '1 - DECRETO';break;
								case 2: $_POST['dvtipnormaxx'][] = '2 - ACUERDO';break;
								case 3: $_POST['dvtipnormaxx'][] = '3 - ORDENANZA';break;
								case 4: $_POST['dvtipnormaxx'][] = '4 - RESOLUCION';break;
								case 5: $_POST['dvtipnormaxx'][] = '5 - LEY';break;
							}
							switch($rowd[5]) {
								case '1.2.2.0.00': $_POST['dvfuentesxx'][] = '1.2.2.0.00 Ingresos corrientes de destinación especifica por acto administrativo';break;
								case '1.3.3.3.15': $_POST['dvfuentesxx'][] = '1.3.3.3.15 R.B. Sobretasa Bomberil';break;
								case '1.2.3.4.02': $_POST['dvfuentesxx'][] = '1.2.3.4.02 ICLD LEY 99 - Destino Ambiental';break;
								case '1.2.3.1.05': $_POST['dvfuentesxx'][] = '1.2.3.1.05 Impuesto - IMPUESTO - SOBRETASA POR EL ALUMBRADO PUBLICO';break;
							}
						}
					}
				}
			?>
			<table class="inicio ancho" style="width:99.5%">
				<tr>
					<td class="titulos" colspan="6">:: Informacion Normas</td>
					<td class="cerrar" style="width:7%;"><a onClick="window.close();">&nbsp;Cerrar</a></td>
				</tr>
				<tr>
					<td class="tamano01" style='width:2.5cm;'>:: Cuenta:</td>
					<td colspan="3"><input type="input" name="ncuenta" id="ncuenta" value="<?php echo $_POST['ncuenta'];?>" style='width:100%;' readonly/></td>
					<td >
				</tr>
				<tr>
					<td class="tamano01">Detalle Sectorial:</td>
					<td colspan="3">
						<select name="dsectorial" id="dsectorial" style="width:100%;height:30px;">
							<option value="">Seleccione ....</option>
							<option value="0" <?php if ($_POST['dsectorial'] == '0') echo "selected" ?>>0 - NO APLICA </option>
							<option value="1" <?php if ($_POST['dsectorial'] == '1') echo "selected" ?>>1 - FSRI-ACUEDUCTO</option>
							<option value="10" <?php if ($_POST['dsectorial'] == '10') echo "selected" ?>>10 - FLS-OTROS GASTOS-INVERSION</option>
							<option value="11" <?php if ($_POST['dsectorial'] == '11') echo "selected" ?>>11 - FLS-OTROS GASTOS-FUNCIONAMIENTO</option>
							<option value="2" <?php if ($_POST['dsectorial'] == '2') echo "selected" ?>>2 - FSRI-ALCANTARILLADO</option>
							<option value="3" <?php if ($_POST['dsectorial'] == '3') echo "selected" ?>>3 - FSRI-ASEO</option>
							<option value="5" <?php if ($_POST['dsectorial'] == '5') echo "selected" ?>>5 - FLS-SALUD PUBLICA</option>
							<option value="6" <?php if ($_POST['dsectorial'] == '6') echo "selected" ?>>6 - FLS-PRESTACION DE SERVICIOS-POBLACION POBRE EN LO NO CUBIERTO CON SUBSIDIOS A LA DEMANDA</option>
							<option value="7" <?php if ($_POST['dsectorial'] == '7') echo "selected" ?>>7 - FLS-PRESTACION DE SERVICIOS-POBLACION MIGRANTE</option>
							<option value="8" <?php if ($_POST['dsectorial'] == '8') echo "selected" ?>>8 - FLS-PRESTACION DE SERVICIOS-SUBSIDIO DE LA OFERTA</option>
							<option value="9" <?php if ($_POST['dsectorial'] == '9') echo "selected" ?>>9 - FLS-ASEGURAMIENTO</option>
						</select>
					</td>
				</tr>
				<tr>
					<td class="tamano01">Destinación Especifica:</td>
					<td>
						<select name="desEsp" id="desEsp" style="width:100%;height:30px;">
							<option value="">Seleccione ....</option>
							<option value="SI" <?php if ($_POST['desEsp'] == 'SI') echo "selected" ?>>SI</option>
							<option value="NO" <?php if ($_POST['desEsp'] == 'NO') echo "selected" ?>>NO</option>
						</select>
					</td>
					<td class="tamano01" style='width:2.5cm;'>Tipo de Norma:</td>
					<td>
						<select name="tNorma" id="tNorma" style="width:100%;height:30px;">
							<option value="">Seleccione ....</option>
							<option value="0" <?php if ($_POST['tNorma'] == '0'){ echo "selected"; $_POST['tNormaxx'] = '0 - NO APLICA';}?>>0 - NO APLICA</option>
							<option value="1" <?php if ($_POST['tNorma'] == '1'){ echo "selected"; $_POST['tNormaxx'] = '1 - DECRETO';}?>>1 - DECRETO</option>
							<option value="2" <?php if ($_POST['tNorma'] == '2'){ echo "selected"; $_POST['tNormaxx'] = '2 - ACUERDO';}?>>2 - ACUERDO</option>
							<option value="3" <?php if ($_POST['tNorma'] == '3'){ echo "selected"; $_POST['tNormaxx'] = '3 - ORDENANZA';}?>>3 - ORDENANZA</option>
							<option value="4" <?php if ($_POST['tNorma'] == '4'){ echo "selected"; $_POST['tNormaxx'] = '4 - RESOLUCION';}?>>4 - RESOLUCION</option>
							<option value="5" <?php if ($_POST['tNorma'] == '5'){ echo "selected"; $_POST['tNormaxx'] = '5 - LEY';}?>>5 - LEY</option>
						</select>
						<input type="hidden" name="tNormaxx" id="tNormaxx" value="<?php echo $_POST['tNormaxx'];?>"/>
					</td>
				</tr>
				<tr>
					<td class="tamano01">Fuentes:</td>
					<td colspan="3">
						<select name="dfuentes" id="dfuentes" style="width:100%;height:30px;">
							<option value="">Seleccione ....</option>
							<option value="1.2.2.0.00" <?php if ($_POST['dfuentes'] == '1.2.2.0.00'){ echo "selected"; $_POST['dfuentesxx'] = '1.2.2.0.00 Ingresos corrientes de destinación especifica por acto administrativo';}?>>1.2.2.0.00 INGRESOS CORRIENTES DE DESTINACION ESPECIFICA POR ACTO ADMINISTRATIVO</option>
							<option value="1.3.3.3.15" <?php if ($_POST['dfuentes'] == '1.3.3.3.15'){ echo "selected"; $_POST['dfuentesxx'] = '1.3.3.3.15 R.B. Sobretasa Bomberil';} ?>>1.3.3.3.15 R.B. SOBRETASA BOMBERIL</option>
							<option value="1.2.3.4.02" <?php if ($_POST['dfuentes'] == '1.2.3.4.02'){ echo "selected"; $_POST['dfuentesxx'] = '1.2.3.4.02 ICLD LEY 99 - Destino Ambiental';}?>>1.2.3.4.02 ICLD LEY 99 - DESTINO AMBIENTAL</option>
							<option value="1.2.3.1.05" <?php if ($_POST['dfuentes'] == '1.2.3.1.05'){ echo "selected"; $_POST['dfuentesxx'] = '1.2.3.1.05 Impuesto - IMPUESTO - SOBRETASA POR EL ALUMBRADO PUBLICO';} ?>>1.2.3.1.05 IMPUESTO - SOBRETASA POR EL ALUMBRADO PUBLICO</option>
							<option value="1.2.3.1.01" <?php if ($_POST['dfuentes'] == '1.2.3.1.01'){ echo "selected"; $_POST['dfuentesxx'] = '1.2.3.1.01 SOBRETASA PARTICIPACION AMBIENTAL';} ?>>1.2.3.1.01 SOBRETASA PARTICIPACION AMBIENTAL</option>
						</select>
						<input type="hidden" name="dfuentesxx" id="dfuentesxx" value="<?php echo $_POST['dfuentesxx'];?>"/>
					</td>
				</tr>
				<tr>
					<td class="tamano01">Numero de la Norma:</td>
					<td><input type="input" name="numnorma" id="numnorma" value="<?php echo $_POST['numnorma'];?>" style='width:100%;height:30px;'/> </td>
					<td class="tamano01">Fecha de la Norma</td>
					<td><input  type='date' name='fechaNorma' id='fechaNorma' alue="<?php echo $_POST['fechaNorma'];?>" maxlength='10' onKeyUp="return tabular(event,this)" title='DD/MM/YYYY' style='width:100%' ></td>
				</tr>
				<tr>
					<td class="tamano01">Porcentaje:</td>
					<td><input type="input" name="porcentaje" id="porcentaje" value="<?php echo $_POST['porcentaje'];?>" style='width:100%;height:30px;'/> </td>
					<td colspan="2" style="padding-bottom:0px"><em class="botonflechaverde" onClick="agregar()">agregar</em></td>
				</tr> 
			</table> 
			<input type="hidden" name="oculto" id="oculto" value="1"/>
			<input type="hidden" name="nuevodoc" id="nuevodoc" value="<?php echo $_POST['nuevodoc']; ?>"/>
			<?php 
				echo "
				<table class='tablamv' style='height:49vh; width:99.6%;'>
					<thead>
						<tr >
							<th class='titulos' style='width:92%;text-align:left;'>.: Resultados Busqueda:</th>
							<th class='titulosnew01b' style='text-align:center;' onClick='guardar()'>Guardar</th>
						</tr>
						<tr style='text-align:Center;'>
							<th class='titulosnew00' style='width:5%;'>No</th>
							<th class='titulosnew00' style='width:10%;'>Destinación</th>
							<th class='titulosnew00' style='width:20%;'>Tipo Norma</th>
							<th class='titulosnew00' style='width:22%'>Fuentes</th>
							<th class='titulosnew00' style='width:10%;'>N° Norma</th>
							<th class='titulosnew00' style='width:15%;'>Fecha</th>
							<th class='titulosnew00' style='width:10%;'>Porcentaje</th>
							<th class='titulosnew00' >Eliminar</th>
						</tr>
					</thead>
					<tbody style='max-height: 36vh !important;'>";	
				if ($_POST['oculto'] == '3'){
					$posi=$_POST['elimina'];
					unset($_POST['vddestiesp'][$posi]);
					unset($_POST['dvtipnorma'][$posi]);
					unset($_POST['dvtipnormaxx'][$posi]);
					unset($_POST['dvfuentes'][$posi]);
					unset($_POST['dvfuentesxx'][$posi]);
					unset($_POST['dvnnorma'][$posi]);
					unset($_POST['dvfecha'][$posi]);
					unset($_POST['dvprocentaje'][$posi]);
					$_POST['vddestiesp'] = array_values($_POST['vddestiesp']);
					$_POST['dvtipnorma'] = array_values($_POST['dvtipnorma']);
					$_POST['dvtipnormaxx'] = array_values($_POST['dvtipnormaxx']);
					$_POST['dvfuentes'] = array_values($_POST['dvfuentes']);
					$_POST['dvfuentesxx'] = array_values($_POST['dvfuentesxx']);
					$_POST['dvnnorma'] = array_values($_POST['dvnnorma']);
					$_POST['dvfecha'] = array_values($_POST['dvfecha']);
					$_POST['dvprocentaje'] = array_values($_POST['dvprocentaje']);
					$_POST['elimina'] ='';
				}
				if($_POST['oculto'] == '4'){
					$_POST['vddestiesp'][] = $_POST['desEsp'];
					$_POST['dvtipnorma'][] = $_POST['tNorma'];
					$_POST['dvtipnormaxx'][] = $_POST['tNormaxx'];
					$_POST['dvfuentes'][] = $_POST['dfuentes'];
					$_POST['dvfuentesxx'][] = $_POST['dfuentesxx'];
					$_POST['dvnnorma'][] = $_POST['numnorma'];
					$_POST['dvfecha'][] = $_POST['fechaNorma'];
					$_POST['dvprocentaje'][] = $_POST['porcentaje'];
					echo"
					<script>
						document.getElementById('desEsp').value = '';
						document.getElementById('tNorma').value = '';
						document.getElementById('tNormaxx').value = '';
						document.getElementById('dfuentes').value = '';
						document.getElementById('dfuentesxx').value = '';
						document.getElementById('numnorma').value = '';
						document.getElementById('fechaNorma').value = '';
						document.getElementById('porcentaje').value = '';
					</script>";
				}
			?>
			<input type='hidden' name='elimina' id='elimina'/>
			<?php
				$iter='saludo1a';
				$iter2='saludo2';
				$conta = 1;
				$totalitem = count($_POST['vddestiesp']);
				for ($x = 0; $x < $totalitem; $x++){
					echo"
					<input type='hidden' name='vddestiesp[]' value='".$_POST['vddestiesp'][$x]."'/>
					<input type='hidden' name='dvtipnorma[]' value='".$_POST['dvtipnorma'][$x]."'/>
					<input type='hidden' name='dvtipnormaxx[]' value='".$_POST['dvtipnormaxx'][$x]."'/>
					<input type='hidden' name='dvfuentes[]' value='".$_POST['dvfuentes'][$x]."'/>
					<input type='hidden' name='dvfuentesxx[]' value='".$_POST['dvfuentesxx'][$x]."'/>
					<input type='hidden' name='dvnnorma[]' value='".$_POST['dvnnorma'][$x]."'/>
					<input type='hidden' name='dvfecha[]' value='".$_POST['dvfecha'][$x]."'/>
					<input type='hidden' name='dvprocentaje[]' value='".$_POST['dvprocentaje'][$x]."'/>
					<tr class='$iter'>
						<td style='font: 120% sans-serif; padding-left:10px; width:5%; text-align:Center;'>$conta</td>
						<td style='font: 120% sans-serif; padding-left:10px; width:10%; text-align:left;'>".$_POST['vddestiesp'][$x]."</td>
						<td style='font: 120% sans-serif; padding-left:10px; width:20%; text-align:left;'>".$_POST['dvtipnormaxx'][$x]."</td>
						<td style='font: 120% sans-serif; padding-left:10px; width:22%; text-align:left;'>".$_POST['dvfuentesxx'][$x]."</td>
						<td style='font: 120% sans-serif; padding-left:10px; width:12%; text-align:left;'>".$_POST['dvnnorma'][$x]."</td>
						<td style='font: 120% sans-serif; padding-left:10px; width:15%; text-align:left;'>".$_POST['dvfecha'][$x]."</td>
						<td style='font: 120% sans-serif; padding-left:10px; width:10%; text-align:left;'>".$_POST['dvprocentaje'][$x]."</td>
						<td style='font: 110% sans-serif; display: flex; justify-content: center;' onclick=\"eliminar($x)\"><div class='garbageX' title='Eliminar'></td>
					</tr>
					";
					$aux=$iter;
					$iter=$iter2;
					$iter2=$aux;
					$conta++;
				}
				echo"
					</tbody>
				</table>";
				if($_POST['oculto'] == 2){
					if($_POST['nuevodoc'] == 0){
						
						$sqlr = "INSERT INTO ccpecuentastiponormas_cab(cuenta, detalle_sectorial, estado) VALUES ('".$_POST['ncuenta']."', '".$_POST['dsectorial']."', 'S')";
						mysqli_query($linkbd,$sqlr);

						$totalitem = count($_POST['vddestiesp']);
						for ($x = 0; $x < $totalitem; $x++){
							$sqlr = "INSERT INTO ccpecuentastiponormas(cuenta, aplicade, tiponorma, numeronorma, fechanorma, estado, detalle_sectorial, fuentes, porcentaje) VALUES ('".$_POST['ncuenta']."', '".$_POST['vddestiesp'][$x]."', '".$_POST['dvtipnorma'][$x]."', '".$_POST['dvnnorma'][$x]."', '".$_POST['dvfecha'][$x]."', 'S', '".$_POST['dsectorial']."', '".$_POST['dvfuentes'][$x]."', '".$_POST['dvprocentaje'][$x]."')";
							mysqli_query($linkbd,$sqlr);
						}

					}else{
						$sqlr = "UPDATE ccpecuentastiponormas_cab SET detalle_sectorial = '".$_POST['dsectorial']."' WHERE cuenta = '".$_POST['ncuenta']."'";
						mysqli_query($linkbd,$sqlr);
						$sqlr = "DELETE ccpecuentastiponormas WHERE cuenta = '".$_POST['ncuenta']."'";
						for ($x = 0; $x < $totalitem; $x++){
							$sqlr = "INSERT INTO ccpecuentastiponormas(cuenta, aplicade, tiponorma, numeronorma, fechanorma, estado, detalle_sectorial, fuentes, porcentaje) VALUES ('".$_POST['ncuenta']."', '".$_POST['vddestiesp'][$x]."', '".$_POST['dvtipnorma'][$x]."', '".$_POST['dvnnorma'][$x]."', '".$_POST['dvfecha'][$x]."', 'S', '".$_POST['dsectorial']."', '".$_POST['dvfuentes'][$x]."', '".$_POST['dvprocentaje'][$x]."')";
							mysqli_query($linkbd,$sqlr);
						}
					}
					echo"
						<script>
							Swal.fire({
								icon: 'success',
								title: 'Se guardo con exito  la información de la cuenta N°:".$_POST['ncuenta']."',
								confirmButtonText: 'Continuar',
								confirmButtonColor: '#FF121A',
								timer: 3000
							});
						</script>";
				}
			?>
		</form>
	</body>
</html>
