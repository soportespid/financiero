<?php

	header("Content-Type: text/html;charset=utf-8");
    require_once 'PHPExcel/Classes/PHPExcel.php';
    require "comun.inc";
    require "funciones.inc";
    require 'funcionesSP.inc.php';
    // ini_set('display_errors', '1');
    // ini_set('display_startup_errors', '1');
    // error_reporting(E_ALL);

    session_start();

    class Plantilla{
        private $linkbd;
        public function __construct() {
            $this->linkbd = conectar_v7();
            $this->linkbd->set_charset("utf8");
        }

        public function searchData() {
            $sql = "SELECT 
            PP.nombre_pdt,
            PI.id,
            PI.nombre,
            PI.tipo, 
            CS.codigo AS codigo_sector,
            PI.recurso_indicativo,
            CONCAT(T.nombre1, ' ', T.nombre2, ' ', T.apellido1, ' ', T.apellido2) AS funcionario
            FROM plan_isgr AS PI
            INNER JOIN plan_pdt AS PP
            ON PI.pdt_id = PP.id
            INNER JOIN ccpetsectores AS CS
            ON PI.sector_id = CS.id
            INNER JOIN terceros AS T
            ON PI.tercero_id = T.id_tercero
            GROUP BY PI.id";
            $data = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
            foreach ($data as $key => $d) {
                $data[$key]["funcionario"] = preg_replace("/\s+/", " ", trim($d["funcionario"]));
            }
            return $data;
        }

        public function ejes() {
            $sql = "SELECT isgr_id, GROUP_CONCAT(eje_estrategico_id ORDER BY eje_estrategico_id ASC) AS ejes_concatenados FROM plan_isgr_det GROUP BY isgr_id";       
            $data = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
            return $data;
        }

        public function bpins() {
            $sql = "SELECT isgr_id, GROUP_CONCAT(bpin ORDER BY bpin ASC) AS bpin_concatenados FROM plan_isgr_bpin GROUP BY isgr_id";
            $data = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
            return $data;
        }
    }
    $obj = new Plantilla();
    $data = $obj->searchData();
    $ejes = $obj->ejes();
    $bpins = $obj->bpins();
    $objPHPExcel = new PHPExcel();

    foreach ($ejes as $key => $eje) {
        $numeros = explode(',', $eje['ejes_concatenados']);
        $numeros_prefijados = array_map(function($numero) {
            return 'LE' . $numero;
        }, $numeros);
        $ejes[$key]['ejes_concatenados'] = implode(',', $numeros_prefijados);
    }

    $objPHPExcel->getSheet(0)->getStyle('A:I')->applyFromArray(
        array(
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
            )
        )
    );
    $objPHPExcel->getSheet(0)->setTitle("ISGR");
    $objPHPExcel->getProperties()
    ->setCreator("IDEAL 10")
    ->setLastModifiedBy("IDEAL 10")
    ->setTitle("Exportar Excel con PHP")
    ->setSubject("Documento de prueba")
    ->setDescription("Documento generado con PHPExcel")
    ->setKeywords("usuarios phpexcel")
    ->setCategory("reportes");

    // ----Cuerpo de Documento----
    $objPHPExcel->getSheet(0)
    ->mergeCells('A1:I1')
    ->mergeCells('A2:I2')
    ->setCellValue('A1', 'PLANEACIÓN ESTRATEGICA')
    ->setCellValue('A2', 'INICIATIVAS SGR CREADAS');
    $objPHPExcel->getSheet(0)
    -> getStyle ("A1")
    -> getFill ()
    -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
    -> getStartColor ()
    -> setRGB ('C8C8C8');
    $objPHPExcel->getSheet(0)
    -> getStyle ("A1:I2")
    -> getFont ()
    -> setBold ( true )
    -> setName ( 'Verdana' )
    -> setSize ( 10 )
    -> getColor ()
    -> setRGB ('000000');
    $objPHPExcel->getSheet(0)
    -> getStyle ('A1:I2')
    -> getAlignment ()
    -> applyFromArray (array ( 'horizontal'  =>  PHPExcel_Style_Alignment :: HORIZONTAL_CENTER ,) );
    $objPHPExcel->getSheet(0)
    -> getStyle ('A3:I3')
    -> getAlignment ()
    -> applyFromArray (array ( 'horizontal'  =>  PHPExcel_Style_Alignment :: VERTICAL_JUSTIFY ,) );
    $objPHPExcel->getSheet(0)
    -> getStyle ("A2")
    -> getFill ()
    -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID);

    $borders = array(
        'borders' => array(
            'allborders' => array(
                'style' => PHPExcel_Style_Border::BORDER_THIN,
            'color' => array('argb' => 'FF000000'),
            )
        ),
    );
    $objPHPExcel->getSheet(0)
    ->setCellValue('A3', "Nombre PDT")
    ->setCellValue('B3', "Código ISGR")
    ->setCellValue('C3', "Nombre")
    ->setCellValue('D3', "Tipo")
    ->setCellValue('E3', "Código sector")
    ->setCellValue('F3', "Código linea estrategica")
    ->setCellValue('G3', "Valor")
    ->setCellValue('H3', "Funcionario")
    ->setCellValue('I3', "BPIN");
    $objPHPExcel->getSheet(0)
        -> getStyle ("A3:I3")
        -> getFill ()
        -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
        -> getStartColor ()
        -> setRGB ('99ddff');
    $objPHPExcel->getSheet(0)->getStyle("A3:I3")->getFont()->setBold(true);
    $objPHPExcel->getSheet(0)->getStyle('A1:I1')->applyFromArray($borders);
    $objPHPExcel->getSheet(0)->getStyle('A2:I2')->applyFromArray($borders);
    $objPHPExcel->getSheet(0)->getStyle('A3:I3')->applyFromArray($borders);


    $objPHPExcel->getSheet(0)->getColumnDimension('A')->setAutoSize(true);
    $objPHPExcel->getSheet(0)->getColumnDimension('B')->setAutoSize(true);
    $objPHPExcel->getSheet(0)->getColumnDimension('C')->setAutoSize(true);
    $objPHPExcel->getSheet(0)->getColumnDimension('D')->setAutoSize(true);
    $objPHPExcel->getSheet(0)->getColumnDimension('E')->setAutoSize(true);
    $objPHPExcel->getSheet(0)->getColumnDimension('F')->setAutoSize(true);
    $objPHPExcel->getSheet(0)->getColumnDimension('G')->setAutoSize(true);
    $objPHPExcel->getSheet(0)->getColumnDimension('H')->setAutoSize(true);
    $objPHPExcel->getSheet(0)->getColumnDimension('I')->setAutoSize(true);

    if(!empty($data)){
        $total = count($data);
        $row = 4;
        for ($i=0; $i < $total ; $i++) {
            $dataEje = "";
            
            foreach ($ejes as $key => $eje) {
                if ($data[$i]["id"] == $eje["isgr_id"]) {
                    $dataEje = $eje["ejes_concatenados"];
                }
            }

            foreach ($bpins as $key => $bpin) {
                if ($data[$i]["id"] == $bpin["isgr_id"]) {
                    $dataBpin = $bpin["bpin_concatenados"];
                }
            }

            $objPHPExcel->getSheet(0)
            ->setCellValue("A$row", $data[$i]['nombre_pdt'])
            ->setCellValue("B$row", "ISGR".$data[$i]['id'])
            ->setCellValue("C$row", $data[$i]['nombre'])
            ->setCellValue("D$row", $data[$i]['tipo'])
            ->setCellValue("E$row", $data[$i]['codigo_sector'])
            ->setCellValue("F$row", $dataEje)
            ->setCellValue("G$row", "$".number_format($data[$i]['recurso_indicativo'], 2))
            ->setCellValue("H$row", $data[$i]['funcionario'])
            ->setCellValue("I$row", $dataBpin);
            $row++;
        }
    }
  
    $objPHPExcel->setActiveSheetIndex(0);
    //----Guardar documento----
    header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment;filename="isgr_creados.xlsx"');
    header('Cache-Control: max-age=0');
    $objWriter=PHPExcel_IOFactory::createWriter($objPHPExcel,'Excel2007');
    $objWriter->save('php://output');
    die();

?>
