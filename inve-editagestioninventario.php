<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");

	require "comun.inc";
	require "funciones.inc";
	require "conversor.php";
	require "validaciones.inc";

	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");

	session_start();
    date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html lang="es">
	<head>
		<meta charset="utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1"/>
		<meta name="viewport" content="user-scalable=no">
		<title>:: IDEAL 10 - Almacen</title>
		<link href="favicon.ico" rel="shortcut icon"/>
		<link href="css/css2.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css">
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css">
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css">
		<script type="text/javascript" src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script type="text/javascript" src="css/calendario.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script>
			function despliegamodalm(_valor,_tip,mensa,pregunta, variable){
				document.getElementById("bgventanamodalm").style.visibility=_valor;
				if(_valor=="hidden"){
					document.getElementById('ventanam').src="";
					switch(document.getElementById('valfocus').value){
						case "1":	document.getElementById('docum').focus();
									document.getElementById('docum').select();
									break;
					}
					document.getElementById('valfocus').value='0';
				}
				else{
					switch(_tip){
						case "1":	document.getElementById('ventanam').src = "ventana-mensaje1.php?titulos="+mensa;break;
						case "2":	document.getElementById('ventanam').src = "ventana-mensaje3.php?titulos="+mensa;break;
						case "3":	document.getElementById('ventanam').src = "ventana-mensaje2.php?titulos="+mensa;break;
						case "4":	document.getElementById('ventanam').src = "ventana-consulta1.php?titulos="+mensa+"&idresp="+pregunta;break;	
						case "5":	document.getElementById('ventanam').src = "ventana-elimina1.php?titulos="+mensa+"&idresp="+pregunta+"&variable="+variable;break;
					}
				}
			}
			function respuestaconsulta(pregunta, variable){
				switch(pregunta){
					case "1":	document.getElementById('oculto').value = "2";
								document.form2.submit();
								break;
				}
			}
			function pdf(){
				var tipo = document.getElementById('tipomov').value;
				if(tipo == '2'){
					var beneficiario = prompt("Digite el numero de firmas que necesita:");
					document.form2.action = "inve-editagestioninventariopdf.php?beneficiario="+beneficiario;
				}
				else {document.form2.action = "inve-editagestioninventariopdf.php";}
				document.form2.target = "_BLANK";
				document.form2.submit(); 
				document.form2.action = "";
				document.form2.target = "";
			}
			function acta(){
				var tipo = document.getElementById('tipomov').value;
				if(tipo == '2'){
					var beneficiario = prompt("Digite el numero de firmas que necesita:");
					document.form2.action = "pdfacta.php?beneficiario="+beneficiario;
				}
				else {document.form2.action = "pdfacta.php";}
				document.form2.target = "_BLANK";
				document.form2.submit(); 
				document.form2.action = "";
				document.form2.target = "";
			}
			function crearexcel(){
				document.form2.action = "inve-editagestioninventarioexcel.php";
				document.form2.target = "_BLANK";
				document.form2.submit();
				document.form2.action = "";
				document.form2.target = "";
			}
			function funcionmensaje(){document.location.href = "inve-gestioninventario.php";}
			function validar(){document.form2.submit();}
			function atrasc(){
				var vid = document.form2.numero.value;
				var vmov = document.form2.movr.value;
				var vent = document.form2.entr.value;
				var minim = document.form2.minimo.value;
				vid=parseFloat(vid)-1;
				if(vid => minim){document.location.href = "inve-editagestioninventario.php?is="+vid+"&mov="+vmov+"&ent="+vent;}
			}
			function adelante(){
				var vid = document.form2.numero.value;
				var vmov = document.form2.movr.value;
				var vent = document.form2.entr.value;
				var maxim = document.form2.maximo.value;
				vid=parseFloat(vid)+1;
				if(vid <= maxim){document.location.href = "inve-editagestioninventario.php?is="+vid+"&mov="+vmov+"&ent="+vent;}
			}
			function despliegamodal2(_valor,_num){
				document.getElementById("bgventanamodal2").style.visibility = _valor;
				if(_valor == "hidden"){document.getElementById('ventana2').src = "";}
				else{
					switch(_num){
						case '0':	document.getElementById('ventana2').src = "tercerosgral-ventana01.php?objeto=doctercero&nobjeto=nombre2"; break;
					
					}
				}
			}
			function guardar(){
				//var doctercero = document.getElementById('doctercero').value;
				//if (doctercero != '')
				{despliegamodalm('visible','4','Esta Seguro de editar el tercero','1','0');}
			//	else {despliegamodalm('visible','2','Faltan ingresar un tercero');}
			}
		</script>
	</head>
	<body>
		<IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
		<span id="todastablas2"></span>
		<table>
			<tr><script>barra_imagenes("inve");</script><?php cuadro_titulos();?></tr>
			<tr><?php menu_desplegable("inve");?></tr>
			<tr>
				<td colspan="3" class="cinta">
					<img src="imagenes/add.png" title="Nuevo" onClick="location.href='inve-gestioninventarioentrada.php'" class="mgbt">
					<img src="imagenes/guarda.png" title="Guardar" onClick="guardar();" class="mgbt">
					<img src="imagenes/busca.png" title="Buscar" class="mgbt" onClick="location.href='inve-buscagestioninventario.php'">
					<img src="imagenes/nv.png" onClick="mypop=window.open('inve-principal.php','','');mypop.focus();" title="nueva ventana" class="mgbt">
					<img src="imagenes/print.png" title="Imprimir" onClick="pdf()" class="mgbt">
					<img src="imagenes/excel.png" title="Excell" onclick="crearexcel()" class="mgbt">
					<img src="imagenes/iratras.png" title="Atr&aacute;s" onClick="location.href='inve-buscagestioninventario.php'" class="mgbt">
				</td>
			</tr>
		</table>
		<div id="bgventanamodalm" class="bgventanamodalm">
			<div id="ventanamodalm" class="ventanamodalm">
				<IFRAME src="" name="ventanam" marginWidth=0 marginHeight=0 frameBorder=0 id="ventanam" frameSpacing=0 style=" width:700px; height:130px; top:200; overflow:hidden;"></IFRAME>
			</div>
		</div>	
		<form name="form2" method="post" action=""> 
			<?php
				if(!$_POST['oculto']){
					$_POST['entr'] = $_GET['ent'];
					$_POST['movr'] = $_GET['mov'];
					$sqlb = "SELECT MIN(consec),MAX(consec) FROM almginventario WHERE tipomov='".$_GET['mov']."' AND tiporeg='".$_GET['ent']."'";
					$resb = mysqli_query($linkbd,$sqlb);
					$rowb = mysqli_fetch_array($resb);
					$_POST['maximo'] = $rowb[1];
					$_POST['minimo'] = $rowb[0];
					$sqlb = "SELECT * FROM almginventario WHERE consec='".$_GET['is']."' AND tipomov='".$_GET['mov']."' AND tiporeg='".$_GET['ent']."' ";
					$resb = mysqli_query($linkbd,$sqlb);
					$rowb = mysqli_fetch_array($resb);
					$_POST['numacta'] = $rowb[11];
					$_POST['vigencia'] = $rowb[12];
					$_POST['fecha'] = date('d/m/Y',strtotime($rowb[1]));
					$_POST['numero'] = $rowb[9];
					$_POST['nombre'] = $rowb[8];
					$_POST['doctercero'] = $rowb[14];
					$_POST['nombre2'] = buscatercero($rowb[14]);
					$_POST['tipomov'] = $rowb[2];
					$_POST['tip'] = $rowb[2];
					$_POST['tipoentra'] = $rowb[2];
					$_POST['docum'] = $rowb[4];
				}
			?>
			<input type="hidden" name="entr" id="entr" value="<?php echo $_POST['entr']?>">
			<input type="hidden" name="movr" id="movr" value="<?php echo $_POST['movr']?>">
			<table class="inicio ancho" align="center" >
				<tr>
					<td class="titulos" colspan="8">.: Gesti&oacute;n de Inventarios </td>
					<td class="cerrar" style="width:7%" onClick="location.href='inve-principal.php'">Cerrar</td>
				</tr>
				<tr>
					<td class="saludo1" style="width:2.2cm">Consecutivo:</td>
					<td style="width: 14%" ><img src="imagenes/back.png" onClick="atrasc()" class="icobut" title="Anterior">&nbsp;<input type="text" id="numero" name="numero" style="width:70%; text-align:center;" value="<?php echo $_POST['numero']?>" onClick="document.getElementById('numero').focus(); document.getElementById('numero').select();">&nbsp;<img src="imagenes/next.png" onClick="adelante()" class="icobut" title="Sigiente"></td>
					<td class="saludo1" style="width:2.5cm">Fecha Registro:</td>
					<td style="width:9%"><input name="fecha" type="text" id="fc_1198971545" title="DD/MM/YYYY" value="<?php echo $_POST['fecha']; ?>" onKeyUp="return tabular(event,this) " onKeyDown="mascara(this,'/',patron,true)" maxlength="10" style="width: 100%" onDblClick="displayCalendarFor('fc_1198971545');" class="colordobleclik" autocomplete="off" onChange="cambiovigencia();"></td>
					<td class="saludo1" style="width:3cm">Tipo de Movimiento: </td>
					<td style="width: 10%">
						<select name="tipomov" id="tipomov" onChange="validar()"  style="width:100%;" >
							<?php
								switch ($_POST['tipomov']){
									case '1': echo"<option value='1' SELECTED>1 - Entrada</option>";break;
									case '2': echo"<option value='2' SELECTED>2 - Salida</option>";break;
									case '3': echo"<option value='3' SELECTED>3 - Reversión de Entrada</option>";break;
									case '4': echo"<option value='4' SELECTED> - Reversión de Salida</option>";break;
								}
							?>
						</select>
						<input type="hidden" name="sw" id="sw" value="<?php echo $_POST['tipomov']?>">
						<input type="hidden" name="tip" value="<?php echo $_POST['tip']?>">
					</td>
				
					<td class="saludo1" style="width:2.5cm">Descripci&oacute;n:</td>
					<td ><input type="text" id="nombre" name="nombre" style="width:100%;" onKeyUp="return tabular(event,this)" value="<?php echo $_POST['nombre']?>" readonly></td>
				</tr>
				
			</table>
			<input type="hidden" name="maximo" id="maximo" value="<?php echo $_POST['maximo'] ?>">
			<input type="hidden" name="minimo" id="minimo" value="<?php echo $_POST['minimo'] ?>">
			<input type="hidden" name="vigencia" id="vigencia" value="<?php echo $_POST['vigencia']?>">
			<table class="inicio ancho">
				<tr><td colspan="7" class="titulos2">Gesti&oacute;n Inventario</td></tr>
				<tr>
					<td class="saludo1" style="width:3cm;">Tipo Entrada:</td>
					<td style="width: 19%">
						<select name="tipoentra" id="tipoentra" style="width: 100%;">
							<?php
								$sqlr="SELECT * FROM almtipomov WHERE tipom = '".$_POST['tipomov']."'";
								$resp = mysqli_query($linkbd,$sqlr);
								while($row = mysqli_fetch_row($resp)){
									if($row[0] == $_POST['tipoentra']){echo "<option value='$row[0]' SELECTED> $row[1]$row[0] - $row[2]</option>";}
								}
							?>
						</select>
					</td>
					<td class="saludo1" style="width: 4cm;">Documento de Cruce:</td>
					<td style="width: 10%"><input type="text" name="docum" id="docum" value="<?php echo $_POST['docum']?>" onKeyPress="javascript:return solonumeros(event)" onKeyUp="return tabular(event,this)" onBlur="guiabuscar('1');" style="width:100%; text-align:center;"></td>
					<td class="saludo1" style="width: 3cm;">Tercero:</td>
					<td style="width: 10%"><input type="text" name="doctercero" id="doctercero" value="<?php echo $_POST['doctercero']?>" style="width:100%" title="Listado Terceros" class="colordobleclik" onDblClick="despliegamodal2('visible','0');" autocomplete="off" readonly></td>
					<td><input type="text" name="nombre2" id="nombre2" value="<?php echo $_POST['nombre2']?>" style="width:100%" readonly></td>
				</tr>
				<tr>
					<td class="saludo1">Cargo:</td>
					<td><input type="text" name="cargo"  style="width:100%;" value="<?php echo $_POST['cargo'];?>"></td>
					<td class="saludo1">Numero de Acta:</td>
					<td><input type="text" name="numacta"  style="width:100%;" value="<?php echo $_POST['numacta'];?>"></td>
					<td colspan="2"><em class="botonflechaverde" onClick="acta();">Generar Acta</em></td>
				</tr>
			</table>
			<input type="hidden" name="oculto" id="oculto" value="1"/>
			<div class="subpantallac" style="height:50%; overflow-x:hidden;">
				<table class='inicio ancho'>
					<tr><td class="titulos" colspan="10">Detalle Gesti&oacute;n Inventario - Entrada</td></tr>
					<tr>
						<td class="titulos2">Codigo UNSPSC</td>
						<td class="titulos2">Codigo Articulo</td>
						<td class="titulos2">Nombre Articulo</td>
						<td class="titulos2">Marca</td>
						<td class="titulos2">Modelo</td>
						<td class="titulos2">Serial</td>
						<td class="titulos2">Cantidad</td>
						<td class="titulos2">U.M</td>
						<td class="titulos2">Valor Unitario</td>
						<td class="titulos2">Valor Total</td>
					</tr>
					<?php
					//FIN BUSQUEDA
						$cant = 0;
						$sqld = "SELECT * FROM almginventario_det WHERE codigo='".$_GET['is']."' AND tipomov='".$_GET['mov']."' AND tiporeg='".$_GET['ent']."'  ORDER BY id_det";
						$resd = mysqli_query($linkbd,$sqld);
						$sumvalortotal = $contart = 0;
						$iter = 'saludo1a';
						$iter2 = 'saludo2';
						while($rowd = mysqli_fetch_array($resd)){
							if($rowd[5] == 0){$cant = $rowd[6];}
							else {$cant = $rowd[5];}
							$crit1 = "WHERE concat_ws(' ', nombre, concat_ws('', grupoinven, codigo)) LIKE '%$rowd[3]'";
							$sqlr = "SELECT * FROM almarticulos $crit1 ORDER BY length(grupoinven),grupoinven ASC, length(codigo),codigo ASC";
							$rart = mysqli_query($linkbd,$sqlr);
							$wart = mysqli_fetch_array($rart);
							if($_POST['marcaart'][$contart]==''){$_POST['marcaart'][$contart] = $rowd[16];}
							if($_POST['modeloart'][$contart]==''){$_POST['modeloart'][$contart] = $rowd[17];}
							if($_POST['serieart'][$contart]==''){$_POST['serieart'][$contart] = $rowd[18];}
							echo"
							<input type='hidden' name='iddetalle[]' value='$rowd[0]'>
							<input type='hidden' name='codunsd[]' value='$rowd[2]'>
							<input type='hidden' name='codinard[]' value='$rowd[3]'>
							<input type='hidden' name='nomartd[]' value='$wart[1]'>
							<input type='hidden' name='cantidadd[]' value='$cant'>
							<input type='hidden' name='unidadd[]' value='$rowd[9]'>
							<input type='hidden' name='valunit[]' value='$rowd[7]'>
							<input type='hidden' name='valtotal[]' value='$rowd[8]'>
							<tr class='$iter'>
								<td style='width:10%'>$rowd[2]</td>
								<td style='width:10%'>$rowd[3]</td>
								<td style='width:10%'>$wart[1]</td>
								<td style='width:10%'><input type='text' name='marcaart[]' value='".$_POST['marcaart'][$contart]."' style='width:100%' class='inpnovisibles2' onChange=''></td>
								<td style='width:10%'><input type='text' name='modeloart[]' value='".$_POST['modeloart'][$contart]."' style='width:100%' class='inpnovisibles2' onChange=''></td>
								<td style='width:10%'><input type='text' name='serieart[]' value='".$_POST['serieart'][$contart]."' style='width:100%' class='inpnovisibles2' onChange=''></td>
								<td style='width:10%'>$cant</td>
								<td style='width:10%'>$rowd[9]</td>
								<td style='text-align:right; width:10%'>$ ".number_format($rowd[7],2,',','.')."</td>
								<td style='text-align:right; width:10%'>$ ".number_format($rowd[8],2,',','.')."</td>
							</tr>";
							$sumvalortotal += $rowd[8];
							$aux = $iter;
							$iter = $iter2;
							$iter2 = $aux;
							$contart++;
						}
						echo"
							<tr class='$iter'>
								<td colspan='9'><h2>Total:</h2></td>
								<td style='text-align:right;'>$ ".number_format($sumvalortotal,2,',','.')."</td>
							</tr>
						</table>
						<input type='hidden' name='totalart' value='$sumvalortotal'>";
					?>
				</table>
			</div>
			<?php
				if($_POST['oculto'] == '2'){
					$sqlr = "UPDATE almginventario SET tercero = '".$_POST['doctercero']."' WHERE tipomov='".$_GET['mov']."' AND tiporeg='".$_GET['ent']."' AND consec = '".$_POST['numero']."'";
					mysqli_query($linkbd,$sqlr);
					for($xy = 0; $xy < count($_POST['iddetalle']);$xy++)
					{
						$sqlr = "UPDATE almginventario_det SET marca = '".$_POST['marcaart'][$xy]."',  modelo = '".$_POST['modeloart'][$xy]."',  serie = '".$_POST['serieart'][$xy]."' WHERE id_det = '".$_POST['iddetalle'][$xy]."'";
						mysqli_query($linkbd,$sqlr);echo $sqlr;
					}
					echo"<script>despliegamodalm('visible','3','Se modifico correctamente el tercero');</script>";
				}
			?>
		</form>
		<div id="bgventanamodal2">
			<div id="ventanamodal2">
				<IFRAME  src="" name="buster" marginWidth=0 marginHeight=0 frameBorder=0 id="ventana2" frameSpacing=0 style="left:500px; width:900px; height:500px; top:200;"></IFRAME>
			</div>
		</div>
	</body>
</html>