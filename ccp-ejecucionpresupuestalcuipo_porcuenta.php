<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require "comun.inc";
	require "funciones.inc";
	sesion();
	$linkbd=conectar_v7();
	$linkbd -> set_charset("utf8");
	cargarcodigopag($_GET[codpag],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
	ini_set('max_execution_time', 720);
?>
<!doctype >
<html lang="es">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=9">
		<title>:: Ideal - Presupuesto</title>
		<link href="css/css2.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css">
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css">
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css">
		<script type="text/javascript" src="css/calendario.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script type="text/javascript" src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script type='text/javascript' src='JQuery/jquery-2.1.4.min.js?<?php echo date('d_m_Y_h_i_s');?>'></script>
		<script>
			function despliegamodal2(_valor,_num)
			{
				document.getElementById("bgventanamodal2").style.visibility=_valor;
				if(_valor=="hidden"){document.getElementById('ventana2').src="";}
				else
				{
					switch(_num)
					{
						case '0':	document.getElementById('ventana2').src="cuentas_presupuestales_degastos.php";break;
					}
				}
			}
			function despliegamodalm(_valor,_tip,mensa,pregunta)
			{
				document.getElementById("bgventanamodalm").style.visibility=_valor;
				if(_valor=="hidden"){document.getElementById('ventanam').src="";}
				else
				{
					switch(_tip)
					{
						case "1":	document.getElementById('ventanam').src="ventana-mensaje1.php?titulos="+mensa;break;
						case "2":	document.getElementById('ventanam').src="ventana-mensaje3.php?titulos="+mensa;break;
						case "3":	document.getElementById('ventanam').src="ventana-mensaje2.php?titulos="+mensa;break;
						case "4":	document.getElementById('ventanam').src="ventana-consulta1.php?titulos="+mensa+"&idresp="+pregunta;break;
					}
				}
			}
			function auxiliarEP(cuenta,seccpresupuestal,programatico,cpc,fuente,bpin,vigengasto)
			{
				document.form2.action = "ccp-ejecucionpresupuestalcuipoauxiliar_porcuenta.php?cuenta=" + cuenta + "&seccpresupuestal=" + seccpresupuestal + "&programatico=" + programatico + "&cpc=" + cpc + "&fuente=" + fuente + "&bpin=" + bpin + "&vigengasto=" + vigengasto;
				document.form2.target = "_BLANK";
				document.form2.submit();
				document.form2.action = "";
				document.form2.target = "";
			}
			function excell()
			{
				document.form2.action = "ccp-ejecucionpresupuestalcuipoexcel.php";
				document.form2.target = "_BLANK";
				document.form2.submit();
				document.form2.action = "";
				document.form2.target = "";
			}
			function excell2(cuenta,seccpresupuestal,programatico,cpc,fuente,bpin,vigengasto)
			{
				document.form2.action = "ccp-ejecucionpresupuestalcuipoexcel2.php?cuenta=" + cuenta + "&seccpresupuestal=" + seccpresupuestal + "&programatico=" + programatico + "&cpc=" + cpc + "&fuente=" + fuente + "&bpin=" + bpin + "&vigengasto=" + vigengasto;
				document.form2.target = "_BLANK";
				document.form2.submit();
				document.form2.action = "";
				document.form2.target = "";
			}
			function callprogress(vValor)
			{
				document.getElementById("getprogress").innerHTML = vValor;
				document.getElementById("getProgressBarFill").innerHTML = '<div class="ProgressBarFill" style="width: '+vValor+'%;"></div>';
				document.getElementById("progreso").style.display='block';
				document.getElementById("getProgressBarFill").style.display='block';
			}
			function callprogress2(vValor)
			{
				document.getElementById("getprogress2").innerHTML = vValor;
				document.getElementById("getProgressBarFill2").innerHTML = '<div class="ProgressBarFill2" style="width: '+vValor+'%;"></div>';
				document.getElementById("progreso2").style.display='block';
				document.getElementById("getProgressBarFill2").style.display='block';
			}
			function detalled(id, cuenta, seccpresupuestal, programatico, cpc, fuente, bpin,vigengasto)
			{
				if($('#detalle'+id).css('display')=='none')
				{
					$('#detalle'+id).css('display','block');
					$('#img'+id).attr('src','imagenes/minus.gif');
				}
				else
				{
					$('#detalle'+id).css('display','none');
					$('#img'+id).attr('src','imagenes/plus.gif');
				}
				var toLoad= 'cpp-reportesejecuciongastos_auxporcuenta.php';
				$.post(toLoad,{cuenta:cuenta,seccpresupuestal:seccpresupuestal,programatico:programatico,cpc:cpc,fuente:fuente,bpin:bpin, vigengasto:vigengasto},function (data){
					$('#detalle'+id).html(data.detalle);
					return false;
				},'json');
			}
			function generarreporte()
			{
				var fechai = document.form2.fechai.value;
				var fechaf = document.form2.fechaf.value;
				var cuenta = document.form2.nomcuenta.value;
				
				if((fechai != '') && (fechaf != '') && (cuenta != ''))
				{
					if (!comparar_dos_fechas(fechai, fechaf))
					{
						if(fechai.substring(6,10) == fechaf.substring(6,10))
						{
							document.form2.oculto.value = '3';
							document.form2.submit();
						}
						else {despliegamodalm('visible','2','fecha inicial y fecha final deben ser del mismo año');}
					}
					else {despliegamodalm('visible','2','La fecha inicial debe ser menor o igual a la fecha final');}
					
					
				}
				else {despliegamodalm('visible','2','Faltan datos fecha inicial y final para general el reporte');}
			}
			function buscacta()
			{
				if (document.form2.numcuenta.value != "")
				{
					document.form2.bc.value=2;
					document.form2.submit();
				}
			}
		</script>
		<?php
			function buscarfuentecuipo($codigo)
			{
				$linkbd=conectar_v7();
				$linkbd -> set_charset("utf8");
				$sqlmax = "SELECT MAX(version) FROM ccpet_fuentes_cuipo";
				$resmax = mysqli_query($linkbd, $sqlmax);
				$rowmax = mysqli_fetch_row($resmax);
				$sql = "SELECT concuipocpc FROM ccpet_fuentes_cuipo WHERE codigo_fuente = '$codigo' AND version = '$rowmax[0]' AND  LENGTH(codigo_fuente) > 6";
				$res = mysqli_query($linkbd,$sql);
				$totalcli=mysqli_num_rows($res);
				$row = mysqli_fetch_row($res);
				return($row[0]);
			}
			function buscarcpccuipo($codigo)//0-4
			{
				$linkbd=conectar_v7();
				$linkbd -> set_charset("utf8");
				if(substr($codigo,0,1) <= 4)
				{
					$sqlmax = "SELECT MAX(version) FROM ccpetbienestransportables";
					$resmax = mysqli_query($linkbd, $sqlmax);
					$rowmax = mysqli_fetch_row($resmax);
					$sql = "SELECT concuipocpc FROM ccpetbienestransportables WHERE cpc = '$codigo' AND version = '$rowmax[0]'";
				}
				else
				{
					$sqlmax = "SELECT MAX(version) FROM ccpetservicios";
					$resmax = mysqli_query($linkbd, $sqlmax);
					$rowmax = mysqli_fetch_row($resmax);
					$sql = "SELECT concuipocpc FROM ccpetservicios WHERE cpc = '$codigo' AND version = '$rowmax[0]'";
				}
				$res = mysqli_query($linkbd,$sql);
				$row = mysqli_fetch_row($res);
				return($row[0]);
			}
			function buscarprogramatico($codigo,$tipo)
			{
				$linkbd=conectar_v7();
				$linkbd -> set_charset("utf8");
				$codm1=substr($codigo,0,4);
				if($tipo == '1'){$sqlproducto = "SELECT concuipom1 FROM ccpetproductos WHERE cod_producto LIKE '$codm1%'";}
				else {$sqlproducto = "SELECT concuipom2 FROM ccpetproductos WHERE cod_producto LIKE '$codm1%'";}
				$resproducto = mysqli_query($linkbd,$sqlproducto);
				$rowproducto = mysqli_fetch_row($resproducto);
				return($rowproducto[0]);
			}
			function fcolorlinea($valor1,$valor2,$valor3)
			{
				if($valor1 == $valor2 && $valor2 == $valor3){$color='';}
				else if($valor1 >= $valor2 && $valor2 >= $valor3){$color="style='background-color:rgba(25,151,255,1.00) !important;'";}
				else {$color="style='background-color:rgba(246,17,21,0.93) !important;'";}
				return $color;
			}
			function calcula_cdp($vigencia,$rp)
			{
				$linkbd=conectar_v7();
				$linkbd -> set_charset("utf8");
				$sql = "SELECT idcdp FROM ccpetrp WHERE vigencia = '$vigencia' AND consvigencia = '$rp'";
				$res = mysqli_query($linkbd,$sql);
				$row = mysqli_fetch_row($res);
				return($row[0]);
			}
			titlepag();
		?>
</head>
	<body>
		<IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
		<span id="todastablas2"></span>
		<table>
			<tr>
				<script>barra_imagenes("ccpet");</script><?php cuadro_titulos(); ?></tr>
			<tr><?php menu_desplegable("ccpet"); ?></tr>
			<tr>
				<td colspan="3" class="cinta"><img src='imagenes/add.png' title='Nuevo' onClick="location.href='ccp-reportescuipo.php'" class="mgbt"><img src="imagenes/guardad.png" title="Guardar" class="mgbt1"><img src="imagenes/busca.png" title="Buscar" onClick="document.form2.submit()" class="mgbt"><img src="imagenes/nv.png" title="Nueva Ventana" onClick="mypop=window.open('ccp-principal.php','','');mypop.focus();" class="mgbt"><img src="imagenes/excel.png" title="Excel" onClick='excell()' class="mgbt"></td>
			</tr>
		</table>
		<div id="bgventanamodalm" class="bgventanamodalm">
			<div id="ventanamodalm" class="ventanamodalm">
				<IFRAME src="" name="ventanam" marginWidth=0 marginHeight=0 frameBorder=0 id="ventanam" frameSpacing=0 style=" width:700px; height:130px; top:200; overflow:hidden;"></IFRAME>
		 	</div>
		</div>
		<form name="form2" method="post" action="">
			<?php
				if($_POST['oculto']=='')
				{
					$_POST['vigeactual'] = vigencia_usuarios($_SESSION['cedulausu']);
					$_POST['secpresupuestal'] = 0;
				}
				$vigusu = $_POST['vigeactual'];
				if($_POST['bc']!='')
				{
					$sqlmax = "SELECT MAX(version) FROM cuentasccpet";
					$resmax = mysqli_query($linkbd, $sqlmax);
					$rowmax = mysqli_fetch_row($resmax);
					$maxVersion = $rowmax[0];
					$sqlcta = "SELECT nombre FROM cuentasccpet WHERE version = '$maxVersion' AND municipio = '1' AND codigo = '".$_POST['numcuenta']."'";
					$rescta = mysqli_query($linkbd,$sqlcta);
					$rowcta = mysqli_fetch_row($rescta);
					$_POST['nomcuenta'] = $rowcta[0];
				}
			?>
			<table align="center" class="inicio">
				<tr>
					<td class="titulos" colspan="9"><p class="neon_titulos">.: Ejecución Presupuestal</p></td>
					<td class="cerrar" style="width:7%" onClick="location.href='ccp-principal.php'">Cerrar</td>
				</tr>
				<tr>
					<td class="tamano01" style="width:2.5cm;">Fecha Inicial:</td>
					<td style="width:12%"><input type="text" name="fechai" value="<?php echo $_POST['fechai'];?>" maxlength="10" onKeyPress="javascript:return solonumeros(event)" onKeyUp="return tabular(event,this)" id="fc_1198971545" onKeyDown="mascara(this,'/',patron,true)" title="DD/MM/YYYY" style="width:80%;height:30px;">&nbsp;<img src="imagenes/calendario04.png" onClick="displayCalendarFor('fc_1198971545');" title="Calendario" class="icobut"></td>
					<td class="tamano01" style="width:2.5cm;">Fecha Final:</td>
					<td style="width:12%"><input type="text" name="fechaf" value="<?php echo $_POST['fechaf'];?>" maxlength="10" onKeyPress="javascript:return solonumeros(event)" onKeyUp="return tabular(event,this)" id="fc_1198971546" onKeyDown="mascara(this,'/',patron,true)" title="DD/MM/YYYY" style="width:80%;height:30px;">&nbsp;<img src="imagenes/calendario04.png" onClick="displayCalendarFor('fc_1198971546');" title="Calendario" class="icobut"></td>
					<td class="tamano01" style="width:2.5cm;">Cuenta:</td>
					<td style="width:10%;"><input type="text" name="numcuenta" id="numcuenta" onDblClick="despliegamodal2('visible','0');" value="<?php echo $_POST['numcuenta'];?>" style="width:98%;height:30px;" class="colordobleclik" onBlur="buscacta();"></td>
					<td><input type="text" name="nomcuenta" id="nomcuenta" value="<?php echo $_POST['nomcuenta'];?>" style="width:100%;height:30px;"></td>
					<td colspan="2"><em class="botonflecha" onClick="generarreporte();">Generar</em></td>
				</tr>
			</table>
			<table align="center" class="inicio">
				<td class="tamano01" style="width:2.5cm;">Vigencia del gasto:</td>
				<td>
					<select name="selvigegasto" id="selvigegasto" style="width:98%;height:30px;">
						<option value='-1'>SIN FILTRO</option>
					</select>
				</td>
				<td class="tamano01" style="width:2.5cm;">Programatico MGA:</td>
				<td>
					<select name="selprogramatico" id="selprogramatico" style="width:98%;height:30px;">
						<option value='-1'>SIN FILTRO</option>
					</select>
				</td>
				<td class="tamano01" style="width:2.5cm;">CPC:</td>
				<td>
					<select name="selcpc" id="selcpc" style="width:98%;height:30px;">
						<option value='-1'>SIN FILTRO</option>
					</select>
				</td>
				<td class="tamano01" style="width:2.5cm;">Fuentes:</td>
				<td>
					<select name="selfuentes" id="selfuentes" style="width:98%;height:30px;">
						<option value='-1'>SIN FILTRO</option>
					</select>
				</td>
				<td class="tamano01" style="width:2.5cm;">BPIN:</td>
				<td>
					<select name="selbpin" id="selbpin" style="width:98%;height:30px;">
						<option value='-1'>SIN FILTRO</option>
					</select>
				</td>
				<td></td>
			</table>
			<input type="hidden" name="vigeactual" id="vigeactual" value="<?php echo $_POST['vigeactual'];?>">
			<input type="hidden" name="oculto" id="oculto" value="1">
			<input type="hidden" name="bc" id="bc" value="0">
			<?php
				if ($_POST['oculto']=='3')
				{
					$acumulado = 0;
					unset ($numerocuenta, $nombrecuenta, $secpresupuestal, $vigenciagasto, $programatico, $varbpin, $varcpc, $varfuentes, $situaciondondos, $politicapublica, $terceroschip, $disponibilidad, $compromisos, $obligaciones, $pagos, $tablasinfo, $varfuentes2, $varcpc2, $programatico2, $apropiaini, $apropiadef, $adiciones, $trascredito, $trascontracredito, $tten, $tteg, $tton, $ttob, $ttrp, $ttcdp, $tvigencia);
					$numid=$c=$idtemporal=0;
					echo "
					<div class='subpantallap' style='height:60.5%; width:99.6%;'>
						<table class='inicio' align='center'>
							<tr>
								<td class='titulos' colspan='22'><p class='neon_titulos' id='titulogento'></p></td>
							</tr>
							<tr>
								<td colspan='6' id='totalcuentas'></td>
								<td colspan='3'>
									<div id='progreso' class='ProgressBar' style='display:none; float:left'>
										<div class='ProgressBarText'><span id='getprogress'></span>&nbsp;% </div>
										<div id='getProgressBarFill'></div>
									</div>
								</td>
								<td colspan='4'>
									<div id='progreso2' class='ProgressBar2' style='display:none; float:left'>
										<div class='ProgressBarText2'><span id='getprogress2'></span>&nbsp;% </div>
										<div id='getProgressBarFill2'></div>
									</div>
								</td>
								<td colspan='5' id='titulosubproceso'></td>
							</tr>
							<tr>
								<td class='titulos2' style='width:2px;'><img src='imagenes/plus.gif'></td>
								<td class='titulos2' colspan='2'>&Aacute;RBOL DE CONCEPTOS</td>
								<td class='titulos2'>VIGENCIA DEL GASTO</td>
								<td class='titulos2'>SECCIÓN PRESUPUESTAL</td>
								<td class='titulos2'>PROGRAMATICO MGA </td>
								<td class='titulos2'>CPC</td>
								<td class='titulos2'>FUENTES DE FINANCIACI&Oacute;N</td>
								<td class='titulos2'>BPIN</td>
								<td class='titulos2'>SITUACION DE FONDOS</td>
								<td class='titulos2'>POLITICA PUBLICA</td>
								<td class='titulos2'>TERCEROS CHIP</td>
								<td class='titulos2'>INICIAL</td>
								<td class='titulos2'>ADICIÓN</td>
								<td class='titulos2'>REDUCCIÓN</td>
								<td class='titulos2'>CREDITO</td>
								<td class='titulos2'>CONTRACREDITO</td>
								<td class='titulos2'>DEFINITIVO</td>
								<td class='titulos2'>DISPONIBILIDAD</td>
								<td class='titulos2'>COMPROMISOS</td>
								<td class='titulos2'>OBLIGACIONES</td>
								<td class='titulos2'>PAGOS</td>
							</tr>";
					preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST['fechai'],$fecha);
					$fechai = "$fecha[3]-$fecha[2]-$fecha[1]";
					preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST['fechaf'],$fecha);
					$fechaf = "$fecha[3]-$fecha[2]-$fecha[1]";
					$nombretablatemporal = $_SESSION['tablatemporal']."AUX";
					if($_SESSION['tablatemporal'] != '' && $_SESSION['tablatemporal'] != null)
					{
						$sqlr = "DROP TABLE $nombretablatemporal";
						mysqli_query($linkbd,$sqlr);
						$sqlr = "CREATE TABLE $nombretablatemporal (id int(11), codigocuenta varchar(100), nombrecuenta varchar(200), seccpresupuestal varchar(10), vigenciagastos varchar(10), programatico varchar(50), bpin varchar(20), cpc varchar(100), fuente varchar(10), situacionfondo varchar(1), politicapublica varchar(10), tercerochip varchar(10), disponibilidad double, compromisos double, obligaciones double, pagos double, tipotabla varchar(100), fuente2 varchar(10), cpc2 varchar(100), programatico2 varchar(50), apropiacionini double, apropiaciondef double, adiciones double, reducciones double, trascredito double, trascontracredito double, tten double, tteg double, tton double, ttob double, ttrp double, ttcdp double, tvigencia varchar(5)) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;";
						mysqli_query($linkbd,$sqlr);
					}
					$sqlredglobal ="SELECT codigo, base, nombre FROM redglobal WHERE tipo ='IN'";
					$resredglobal = mysqli_query($linkbd,$sqlredglobal);
					while ($rowredglobal = mysqli_fetch_row($resredglobal))
					{
						$c=$numid=0;
						echo"
						<script>document.getElementById('titulogento').innerHTML='.: EJECUCI&Oacute;N PRESUPUESTAL: $rowredglobal[2]';</script>";
						$linkmulti=conectar_Multi($rowredglobal[1]);
						$linkmulti -> set_charset("utf8");
						$sqlmax = "SELECT MAX(version) FROM cuentasccpet";
						$resmax = mysqli_query($linkmulti, $sqlmax);
						$rowmax = mysqli_fetch_row($resmax);
						$maxVersion = $rowmax[0];
						$sqlcta ="SELECT codigo,nombre FROM cuentasccpet WHERE version='$maxVersion' AND municipio=1 AND tipo = 'C' AND codigo = '".$_POST['numcuenta']."' ORDER BY id ASC";
						$rescta = mysqli_query($linkmulti,$sqlcta);
						$totalcli=mysqli_num_rows($rescta);
						while ($rowcta = mysqli_fetch_row($rescta))
						{
							$numid++;
							$c++;
							//INICIAL GASTOS BIENES TRANSPORTABLES
							{
								$cx = 0;
								$sqlBienesTrans = "SELECT valor, id, fuente, medioPago, subclase FROM ccpetinicialigastosbienestransportables WHERE cuenta = '$rowcta[0]' AND vigencia = '$vigusu'";
								$resBienesTrans = mysqli_query($linkmulti, $sqlBienesTrans);
								$totalcx = mysqli_num_rows($resBienesTrans);
								while ($rowBienesTrans = mysqli_fetch_row($resBienesTrans))
								{
									$cx++;
									$numerocuenta[] = $rowcta[0];
									$nombrecuenta[] = $rowcta[1];
									$secpresupuestal[] = $rowredglobal[0];
									$vigenciagasto[] = '1';
									$programatico[] = 'NO APLICA';
									$programatico2[] = '';
									$varbpin[] = '0';
									if($rowBienesTrans[4] != '' && $rowBienesTrans[4] != null)
									{
										$varcpc2[] = $rowBienesTrans[4];
										$auxcpc = buscarcpccuipo($rowBienesTrans[4]);
										if($auxcpc != ''){$varcpc[] = $auxcpc;}
										else {$varcpc[] = 'NO APLICA';}
									}
									else 
									{
										$varcpc[] = 'NO APLICA';
										$varcpc2[] = '';
									}
									if($rowBienesTrans[2] != '' && $rowBienesTrans[2] != null)
									{
										$varfuentes2[] = $rowBienesTrans[2];
										$auxfuente = buscarfuentecuipo($rowBienesTrans[2]);
										if($auxfuente != '' && $auxfuente != null){$varfuentes[] = $auxfuente;}
										else {$varfuentes[] = 'NO APLICA';}
									}
									else 
									{
										$varfuentes[] = 'NO APLICA';
										$varfuentes2[] = '';
									}
									if($rowBienesTrans[3] == 'CSF'){$situaciondondos[]='1';}
									else {$situaciondondos[]='2';}
									$politicapublica[] = 'NO APLICA';
									$terceroschip[] = 0;
									$apropiaini[] = round($rowBienesTrans[0]);
									$adiciones[] = 0;
									$reducciones[] = 0;
									$trascredito[] = 0;
									$trascontracredito[] = 0;
									$apropiadef[] = round($rowBienesTrans[0]);
									$disponibilidad[] = 0;
									$compromisos[] = 0;
									$obligaciones[] = 0;
									$pagos[] = 0;
									$tablasinfo[] = "Inicial Bienes Trasportables No: ".$rowBienesTrans[1];
									$tten[] = 0;
									$tteg[] = 0;
									$tton[] = 0;
									$ttob[] = 0;
									$ttrp[] = 0;
									$ttcdp[] = 0;
									$tvigencia[] = $vigusu;
									$porcentaje2 = $cx * 100 / $totalcx;
									echo"
									<script>
										progres2 = '".round($porcentaje2)."';
										callprogress2(progres2);
										document.getElementById('titulosubproceso').innerHTML = 'Bienes Trasportables $rowcta[0]';
									</script>";
									flush();
									ob_flush();
									usleep(5);
								}
								echo "
								<script>
									document.getElementById('progreso2').style.display='none';
									document.getElementById('titulosubproceso').innerHTML='';
								</script>";
							}
							//INICIAL SERVICIOS
							{
								$cx = 0;
								$sqlServicios = "SELECT valor, id, subclase, fuente, medioPago FROM ccpetinicialservicios WHERE cuenta = '$rowcta[0]' AND vigencia = '$vigusu'";
								$resServicios = mysqli_query($linkmulti, $sqlServicios);
								$totalcx = mysqli_num_rows($resServicios);
								while ($rowServicios = mysqli_fetch_row($resServicios))
								{
									$cx++;
									$numerocuenta[] = $rowcta[0];
									$nombrecuenta[] = $rowcta[1];
									$secpresupuestal[] = $rowredglobal[0];
									$vigenciagasto[] = '1';
									$programatico[] = 'NO APLICA';
									$programatico2[] = '';
									$varbpin[] = '0';
									if($rowServicios[2] != '' && $rowServicios[2] != null)
									{
										$varcpc2[] = $rowServicios[2];
										$auxcpc = buscarcpccuipo($rowServicios[2]);
										if($auxcpc != ''){$varcpc[] = $auxcpc;}
										else {$varcpc[] = 'NO APLICA';}
									}
									else 
									{
										$varcpc[] = 'NO APLICA';
										$varcpc2[] = '';
									}
									if($rowServicios[3] != '' && $rowServicios[3] != null)
									{
										$varfuentes2[] = $rowServicios[3];
										$auxfuente = buscarfuentecuipo($rowServicios[3]);
										if($auxfuente != '' && $auxfuente != null){$varfuentes[] = $auxfuente;}
										else {$varfuentes[] = 'NO APLICA';}
									}
									else 
									{
										$varfuentes[] = 'NO APLICA';
										$varfuentes2[] = '';
									}
									if($rowServicios[4] == 'CSF'){$situaciondondos[]='1';}
									else {$situaciondondos[]='2';}
									$politicapublica[] = 'NO APLICA';
									$terceroschip[] = 0;
									$apropiaini[] = round($rowServicios[0]);
									$adiciones[] = 0;
									$reducciones[] = 0;
									$trascredito[] = 0;
									$trascontracredito[] = 0;
									$apropiadef[] = round($rowServicios[0]);
									$disponibilidad[] = 0;
									$compromisos[] = 0;
									$obligaciones[] = 0;
									$pagos[] = 0;
									$tablasinfo[] = "Servicios No: ".$rowServicios[1];
									$tten[] = 0;
									$tteg[] = 0;
									$tton[] = 0;
									$ttob[] = 0;
									$ttrp[] = 0;
									$ttcdp[] = 0;
									$tvigencia[] = $vigusu;
									$porcentaje2 = $cx * 100 / $totalcx;
									echo"
									<script>
										progres2 = '".round($porcentaje2)."';
										callprogress2(progres2);
										document.getElementById('titulosubproceso').innerHTML = 'Inicial Servicios $rowcta[0]';
									</script>";
									flush();
									ob_flush();
									usleep(5);
								}
								echo "
								<script>
									document.getElementById('progreso2').style.display='none';
									document.getElementById('titulosubproceso').innerHTML='';
								</script>";
							}
							//INICIAL VALOR GASTOS
							{
								$cx = 0;
								$sqlValorGastos = "SELECT valor, id, fuente, medioPago FROM ccpetinicialvalorgastos WHERE cuenta = '$rowcta[0]' AND vigencia = '$vigusu'";
								$resValorGastos = mysqli_query($linkmulti, $sqlValorGastos);
								$totalcx = mysqli_num_rows($resValorGastos);
								while ($rowValorGastos = mysqli_fetch_row($resValorGastos))
								{
									$cx++;
									$numerocuenta[] = $rowcta[0];
									$nombrecuenta[] = $rowcta[1];
									$secpresupuestal[] = $rowredglobal[0];
									$vigenciagasto[] = '1';
									$programatico[] = 'NO APLICA';
									$programatico2[] = '';
									$varbpin[] = '0';
									$varcpc[] = 'NO APLICA';
									$varcpc2[] = '';
									if($rowValorGastos[2] != '' && $rowValorGastos[2] != null)
									{
										$varfuentes2[] = $rowValorGastos[2];
										$auxfuente = buscarfuentecuipo($rowValorGastos[2]);
										if($auxfuente != '' && $auxfuente != null){$varfuentes[] = $auxfuente;}
										else {$varfuentes[] = 'NO APLICA';}
									}
									else 
									{
										$varfuentes[] = 'NO APLICA';
										$varfuentes2[] = '';
									}
									if($rowValorGastos[3] == 'CSF'){$situaciondondos[]='1';}
									else {$situaciondondos[]='2';}
									$politicapublica[] = 'NO APLICA';
									$terceroschip[] = 0;
									$apropiaini[] = round($rowValorGastos[0]);
									$adiciones[] = 0;
									$reducciones[] = 0;
									$trascredito[] = 0;
									$trascontracredito[] = 0;
									$apropiadef[] = round($rowValorGastos[0]);
									$disponibilidad[] = 0;
									$compromisos[] = 0;
									$obligaciones[] = 0;
									$pagos[] = 0;
									$tablasinfo[] = "Gastos No: ".$rowValorGastos[1];
									$tten[] = 0;
									$tteg[] = 0;
									$tton[] = 0;
									$ttob[] = 0;
									$ttrp[] = 0;
									$ttcdp[] = 0;
									$tvigencia[] = $vigusu;
									$porcentaje2 = $cx * 100 / $totalcx;
									echo"
									<script>
										progres2 = '".round($porcentaje2)."';
										callprogress2(progres2);
										document.getElementById('titulosubproceso').innerHTML = 'Inicial Gastos $rowcta[0]';
									</script>";
									flush();
									ob_flush();
									usleep(5);
								}
								echo "
								<script>
									document.getElementById('progreso2').style.display='none';
									document.getElementById('titulosubproceso').innerHTML='';
								</script>";
							}
							//PROYECTOS PRESUPUESTO
							{
								$cx = 0;
								$sqlrProyectos = "
								SELECT PD.valorcsf, PD.valorssf, PD.indicador_producto, P.codigo, P.id, PD.vigencia_gasto, PD.indicador_producto, PD.subproducto, PD.subclase, PD.id_fuente, PD.medio_pago, PD.politicas_publicas
								FROM ccpproyectospresupuesto AS P
								INNER JOIN ccpproyectospresupuesto_presupuesto AS PD
								ON P.id = PD.codproyecto AND PD.rubro = '$rowcta[0]'
								WHERE P.vigencia = '$vigusu'";
								$resProyectos = mysqli_query($linkmulti, $sqlrProyectos);
								$totalcx = mysqli_num_rows($resProyectos);
								while ($rowProyectos = mysqli_fetch_row($resProyectos))
								{
									$cx++;
									$numerocuenta[] = $rowcta[0];
									$nombrecuenta[] = $rowcta[1];
									$secpresupuestal[] = $rowredglobal[0];
									if($rowProyectos[5]==''){$vigenciagasto[] = '1';}
									else {$vigenciagasto[] = $rowProyectos[5];}
									if($rowProyectos[6] != '' && $rowProyectos[6] != null)
									{
										$programatico2[] = $rowProyectos[6];
										$auxprogramatico = buscarprogramatico($rowProyectos[6],'2');
										if ($auxprogramatico != ''){$programatico[]= $auxprogramatico;}
										else {$programatico[] = 'NO APLICA';}
									}
									else 
									{
										$programatico[] = 'NO APLICA';
										$programatico2[] = '';
									}
									$varbpin[] = '0';
									
									if($rowProyectos[7] != '' && $rowProyectos[7] != null)
									{
										$varcpc2[] = $rowProyectos[7];
										$auxcpc = buscarcpccuipo($rowProyectos[7]);
										if($auxcpc != ''){$varcpc[] = $auxcpc;}
										else {$varcpc[] = 'NO APLICA';}
									}
									elseif($rowProyectos[8] != '' && $rowProyectos[8] != null)
									{
										$varcpc2[] = $rowProyectos[8];
										$auxcpc = buscarcpccuipo($rowProyectos[8]);
										if($auxcpc != ''){$varcpc[] = $auxcpc;}
										else {$varcpc[] = 'NO APLICA';}
									}
									else
									{
										$varcpc[] = 'NO APLICA';
										$varcpc2[] = '';
									}
									if($rowProyectos[9] != '' && $rowProyectos[9] != null)
									{
										$varfuentes2[] = $rowProyectos[9];
										$auxfuente = buscarfuentecuipo($rowProyectos[9]);
										if($auxfuente != '' && $auxfuente != null){$varfuentes[] = $auxfuente;}
										else {$varfuentes[] = 'NO APLICA';}
									}
									else
									{
										$varfuentes[] = 'NO APLICA';
										$varfuentes2[] = '';
									}
									if($rowProyectos[10] == 'CSF'){$situaciondondos[]='1';}
									else {$situaciondondos[]='2';}
									if($rowProyectos[11]!=''){$politicapublica[] = $rowProyectos[11];}
									else {$politicapublica[] = 'NO APLICA';}
									$terceroschip[] = 0;
									$apropiaini[] = round($rowProyectos[0]+$rowProyectos[1]);
									$adiciones[] = 0;
									$reducciones[] = 0;
									$trascredito[] = 0;
									$trascontracredito[] = 0;
									$apropiadef[] = round($rowProyectos[0]+$rowProyectos[1]);
									$disponibilidad[] = 0;
									$compromisos[] = 0;
									$obligaciones[] = 0;
									$pagos[] = 0;
									$tablasinfo[] = "Proyectos No: ".$rowProyectos[3];
									$tten[] = 0;
									$tteg[] = 0;
									$tton[] = 0;
									$ttob[] = 0;
									$ttrp[] = 0;
									$ttcdp[] = 0;
									$tvigencia[] = $vigusu;
									$porcentaje2 = $cx * 100 / $totalcx;
									echo"
									<script>
										progres2 = '".round($porcentaje2)."';
										callprogress2(progres2);
										document.getElementById('titulosubproceso').innerHTML = 'Inicial Proyectos $rowcta[0]';
									</script>";
									flush();
									ob_flush();
									usleep(5);
								}
								echo "
								<script>
									document.getElementById('progreso2').style.display='none';
									document.getElementById('titulosubproceso').innerHTML='';
								</script>";
							}
							//ADICIONES
							{
								$cx = 0;
								$sqlAdicion = "SELECT valor, id_adicion, vigencia_gasto, indicadorproducto, cuenta_clasificadora, fuente, mediopago FROM ccpetadiciones WHERE cuenta = '$rowcta[0]' AND vigencia = '$vigusu' AND estado != 'N' AND fecha BETWEEN '$fechai' AND '$fechaf'";
								$resAdicion = mysqli_query($linkmulti, $sqlAdicion);
								$totalcx = mysqli_num_rows($resAdicion);
								while ($rowAdicion = mysqli_fetch_row($resAdicion))
								{
									$cx++;
									$numerocuenta[] = $rowcta[0];
									$nombrecuenta[] = $rowcta[1];
									$secpresupuestal[] = $rowredglobal[0];
									if($rowAdicion[2]==''){$vigenciagasto[] = '1';}
									else {$vigenciagasto[] = $rowAdicion[2];}
									if($rowAdicion[3] != '' && $rowAdicion[3] != null)
									{
										$programatico2[] = $rowAdicion[3];
										$auxprogramatico = buscarprogramatico($rowAdicion[3],'2');
										if ($auxprogramatico != ''){$programatico[]= $auxprogramatico;}
										else {$programatico[] = 'NO APLICA';}
									}
									else 
									{
										$programatico[] = 'NO APLICA';
										$programatico2[] = '';
									}
									$varbpin[] = '0';
									if($rowAdicion[4] != '' && $rowAdicion[4] != null)
									{
										$varcpc2[] = $rowAdicion[4];
										$auxcpc = buscarcpccuipo($rowAdicion[4]);
										if($auxcpc != ''){$varcpc[] = $auxcpc;}
										else {$varcpc[] = 'NO APLICA';}
									}
									else 
									{
										$varcpc[] = 'NO APLICA';
										$varcpc2[] = '';
									}
									if($rowAdicion[5] != '' && $rowAdicion[5] != null)
									{
										$varfuentes2[] = $rowAdicion[5];
										$auxfuente = buscarfuentecuipo($rowAdicion[5]);
										if($auxfuente != '' && $auxfuente != null){$varfuentes[] = $auxfuente;}
										else {$varfuentes[] = 'NO APLICA';}
									}
									else 
									{
										$varfuentes[] = 'NO APLICA';
										$varfuentes2[] = '';
									}
									if($rowAdicion[6] == 'CSF'){$situaciondondos[]='1';}
									else {$situaciondondos[]='2';}
									$politicapublica[] = 'NO APLICA';
									$terceroschip[] = 0;
									$apropiaini[] = 0;
									$adiciones[] = round($rowAdicion[0]);
									$reducciones[] = 0;
									$trascredito[] = 0;
									$trascontracredito[] = 0;
									$apropiadef[] = round($rowAdicion[0]);
									$disponibilidad[] = 0;
									$compromisos[] = 0;
									$obligaciones[] = 0;
									$pagos[] = 0;
									$tablasinfo[] = "Adiciones No: ".$rowAdicion[1];
									$tten[] = 0;
									$tteg[] = 0;
									$tton[] = 0;
									$ttob[] = 0;
									$ttrp[] = 0;
									$ttcdp[] = 0;
									$tvigencia[] = $vigusu;
									$porcentaje2 = $cx * 100 / $totalcx;
									echo"
									<script>
										progres2 = '".round($porcentaje2)."';
										callprogress2(progres2);
										document.getElementById('titulosubproceso').innerHTML = 'Adiciones $rowcta[0]';
									</script>";
									flush();
									ob_flush();
									usleep(5);
								}
								echo "
								<script>
									document.getElementById('progreso2').style.display='none';
									document.getElementById('titulosubproceso').innerHTML='';
								</script>";
							}
							//ADICION INVERSION
							{
								$cx = 0;
								$sqlAdicionInversion = "SELECT PD.valorcsf, PD.vigencia_gasto, P.id, PD.indicador_producto, PD.subclase, PD.id_fuente, PD.medio_pago, PD.politicas_publicas, PD.valorssf FROM ccpetadicion_inversion AS P, ccpetadicion_inversion_detalles AS PD, ccpetacuerdos AS CA WHERE P.id = PD.codproyecto AND P.id_acuerdo = CA.id_acuerdo AND CA.fecha BETWEEN '$fechai' AND '$fechaf' AND PD.rubro = '$rowcta[0]' AND P.vigencia = '$vigusu'";
								$resAdicionInversion = mysqli_query($linkmulti, $sqlAdicionInversion);
								$totalcx = mysqli_num_rows($resAdicionInversion);
								while ($rowAdicionInversion = mysqli_fetch_row($resAdicionInversion))
								{
									$cx++;
									$numerocuenta[] = $rowcta[0];
									$nombrecuenta[] = $rowcta[1];
									$secpresupuestal[] = $rowredglobal[0];
									if($rowAdicionInversion[1]==''){$vigenciagasto[] = '1';}
									else {$vigenciagasto[] = $rowAdicionInversion[1];}
									if($rowAdicionInversion[3] != '' && $rowAdicionInversion[3] != null)
									{
										$programatico2[] = $rowAdicionInversion[3];
										$auxprogramatico = buscarprogramatico($rowAdicionInversion[3],'2');
										if ($auxprogramatico != ''){$programatico[]= $auxprogramatico;}
										else {$programatico[] = 'NO APLICA';}
									}
									else 
									{
										$programatico[] = 'NO APLICA';
										$programatico2[] = '';
									}
									$varbpin[] = '0';
									if($rowAdicionInversion[4] != '' && $rowAdicionInversion[4] != null)
									{
										$varcpc2[] = $rowAdicionInversion[4];
										$auxcpc = buscarcpccuipo($rowAdicionInversion[4]);
										if($auxcpc != ''){$varcpc[] = $auxcpc;}
										else {$varcpc[] = 'NO APLICA';}
									}
									else 
									{
										$varcpc[] = 'NO APLICA';
										$varcpc2[] = '';
									}
									if($rowAdicionInversion[5] != '' && $rowAdicionInversion[5] != null)
									{
										$varfuentes2[] = $rowAdicionInversion[5];
										$auxfuente = buscarfuentecuipo($rowAdicionInversion[5]);
										if($auxfuente != '' && $auxfuente != null){$varfuentes[] = $auxfuente;}
										else {$varfuentes[] = 'NO APLICA';}
									}
									else 
									{
										$varfuentes[] = 'NO APLICA';
										$varfuentes2[] = '';
									}
									if($rowAdicionInversion[6] == 'CSF'){$situaciondondos[]='1';}
									else {$situaciondondos[]='2';}
									if($rowAdicionInversion[7]!=''){$politicapublica[] = $rowAdicionInversion[7];}
									else {$politicapublica[] = 'NO APLICA';}
									$terceroschip[] = 0;
									$apropiaini[] = 0;
									$adiciones[] = round($rowAdicionInversion[0]) + round($rowAdicionInversion[8]);
									$reducciones[] = 0;
									$trascredito[] = 0;
									$trascontracredito[] = 0;
									$apropiadef[] = round($rowAdicionInversion[0]) + round($rowAdicionInversion[8]);
									$disponibilidad[] = 0;
									$compromisos[] = 0;
									$obligaciones[] = 0;
									$pagos[] = 0;
									$tablasinfo[] = "Adici&oacute;n Inversion No: ".$rowAdicionInversion[2];
									$tten[] = 0;
									$tteg[] = 0;
									$tton[] = 0;
									$ttob[] = 0;
									$ttrp[] = 0;
									$ttcdp[] = 0;
									$tvigencia[] = $vigusu;
									$porcentaje2 = $cx * 100 / $totalcx;
									echo"
									<script>
										progres2 = '".round($porcentaje2)."';
										callprogress2(progres2);
										document.getElementById('titulosubproceso').innerHTML = 'Adiciones Inversion $rowcta[0]';
									</script>";
									flush();
									ob_flush();
									usleep(5);
								}
								echo "
								<script>
									document.getElementById('progreso2').style.display='none';
									document.getElementById('titulosubproceso').innerHTML='';
								</script>";
							}
							//REDUCCIONES
							{
								$cx = 0;
								$sqlReduccion = "SELECT valor, id_reduccion, indicadorproducto, cuenta_clasificadora, fuente, modopago FROM ccpetreducciones WHERE cuenta = '$rowcta[0]' AND fecha BETWEEN '$fechai' AND '$fechaf' AND vigencia = '$vigusu' AND estado != 'N'";
								$resReduccion = mysqli_query($linkmulti, $sqlReduccion);
								$totalcx = mysqli_num_rows($resReduccion);
								while ($rowReduccion = mysqli_fetch_row($resReduccion))
								{
									$cx++;
									$numerocuenta[] = $rowcta[0];
									$nombrecuenta[] = $rowcta[1];
									$secpresupuestal[] = $rowredglobal[0];
									$vigenciagasto[] = '1';
									if($rowReduccion[2] != '' && $rowReduccion[2] != null)
									{
										$programatico2[] = $rowReduccion[2];
										$auxprogramatico = buscarprogramatico($rowReduccion[2],'2');
										if ($auxprogramatico != ''){$programatico[]= $auxprogramatico;}
										else {$programatico[] = 'NO APLICA';}
									}
									else 
									{
										$programatico[] = 'NO APLICA';
										$programatico2[] = '';
									}
									$varbpin[] = '0';
									if($rowReduccion[3] != '' && $rowReduccion[3] != null)
									{
										$varcpc2[] = $rowReduccion[3];
										$auxcpc = buscarcpccuipo($rowReduccion[3]);
										if($auxcpc != ''){$varcpc[] = $auxcpc;}
										else {$varcpc[] = 'NO APLICA';}
									}
									else 
									{
										$varcpc[] = 'NO APLICA';
										$varcpc2[] = '';
									}
									if($rowReduccion[4] != '' && $rowReduccion[4] != null)
									{
										$varfuentes2[] = $rowReduccion[4];
										$auxfuente = buscarfuentecuipo($rowReduccion[4]);
										if($auxfuente != '' && $auxfuente != null){$varfuentes[] = $auxfuente;}
										else {$varfuentes[] = 'NO APLICA';}
									}
									else 
									{
										$varfuentes[] = 'NO APLICA';
										$varfuentes2[] = '';
									}
									if($rowReduccion[5] == 'CSF'){$situaciondondos[]='1';}
									else {$situaciondondos[]='2';}
									if($rowReduccion[7] != '' && $rowReduccion[7] != null){$politicapublica[] = $rowAdicionInversion[7];}
									else {$politicapublica[] = 'NO APLICA';}
									$terceroschip[] = 0;
									$apropiaini[] = 0;
									$adiciones[] = 0;
									$reducciones[] = round($rowReduccion[0]);
									$trascredito[] = 0;
									$trascontracredito[] = 0;
									$apropiadef[] = -1 * round($rowReduccion[0]);
									$disponibilidad[] = 0;
									$compromisos[] = 0;
									$obligaciones[] = 0;
									$pagos[] = 0;
									$tablasinfo[] = "Reducciones No: ".$rowReduccion[1];
									$tten[] = 0;
									$tteg[] = 0;
									$tton[] = 0;
									$ttob[] = 0;
									$ttrp[] = 0;
									$ttcdp[] = 0;
									$tvigencia[] = $vigusu;
									$porcentaje2 = $cx * 100 / $totalcx;
									echo"
									<script>
										progres2 = '".round($porcentaje2)."';
										callprogress2(progres2);
										document.getElementById('titulosubproceso').innerHTML = 'Reducciones $rowcta[0]';
									</script>";
									flush();
									ob_flush();
									usleep(5);
								}
								echo "
								<script>
									document.getElementById('progreso2').style.display='none';
									document.getElementById('titulosubproceso').innerHTML='';
								</script>";
							}
							//REDUCCIONES 2
							{
								$cx = 0;
								$sqlReduccion = "
								SELECT T1.valor, T1.id, T1.vigencia_gasto, T1.indicador_producto, T1.bpin, T1.clasificador, T1.fuente, T1.medio_pago, T1.politica_publica
								FROM ccpetreduccion AS T1 
								INNER JOIN ccpetacuerdos AS T2
								ON T1.acuerdo = T2.id_acuerdo AND T1.vigencia = T2.vigencia
								WHERE T1.rubro = '$rowcta[0]' AND T1.vigencia = '$vigusu' AND T2.fecha BETWEEN '$fechai' AND '$fechaf'";
								$resReduccion = mysqli_query($linkmulti, $sqlReduccion);
								$totalcx = mysqli_num_rows($resReduccion);
								while ($rowReduccion = mysqli_fetch_row($resReduccion))
								{
									$cx++;
									$numerocuenta[] = $rowcta[0];
									$nombrecuenta[] = $rowcta[1];
									$secpresupuestal[] = $rowredglobal[0];
									if($rowReduccion[2]==''){$vigenciagasto[] = '1';}
									else {$vigenciagasto[] = $rowReduccion[2];}
									if($rowReduccion[3] != '' && $rowReduccion[3] != null)
									{
										$programatico2[] = $rowReduccion[3];
										$auxprogramatico = buscarprogramatico($rowReduccion[3],'2');
										if ($auxprogramatico != ''){$programatico[]= $auxprogramatico;}
										else {$programatico[] = 'NO APLICA';}
									}
									else 
									{
										$programatico[] = 'NO APLICA';
										$programatico2[] = '';
									}
									if($rowReduccion[4] != '' && $rowReduccion[4] != null){$varbpin[] = $rowReduccion[4];}
									else {$varbpin[] = '0';}
									if($rowReduccion[5] != '' && $rowReduccion[5] != null)
									{
										$varcpc2[] = $rowReduccion[5];
										$auxcpc = buscarcpccuipo($rowReduccion[5]);
										if($auxcpc != ''){$varcpc[] = $auxcpc;}
										else {$varcpc[] = 'NO APLICA';}
									}
									else 
									{
										$varcpc[] = 'NO APLICA';
										$varcpc2[] = '';
									}
									if($rowReduccion[6] != '' && $rowReduccion[6] != null)
									{
										$varfuentes2[] = $rowReduccion[6];
										$auxfuente = buscarfuentecuipo($rowReduccion[6]);
										if($auxfuente != '' && $auxfuente != null){$varfuentes[] = $auxfuente;}
										else {$varfuentes[] = 'NO APLICA';}
									}
									else 
									{
										$varfuentes[] = 'NO APLICA';
										$varfuentes2[] = '';
									}
									if($rowReduccion[7] == 'CSF'){$situaciondondos[]='1';}
									else {$situaciondondos[]='2';}
									if($rowReduccion[8] != '' && $rowReduccion[8] != null){$politicapublica[] = $rowAdicionInversion[8];}
									else {$politicapublica[] = 'NO APLICA';}
									$terceroschip[] = 0;
									$apropiaini[] = 0;
									$adiciones[] = 0;
									$reducciones[] = round($rowReduccion[0]);
									$trascredito[] = 0;
									$trascontracredito[] = 0;
									$apropiadef[] = -1 * round($rowReduccion[0]);
									$disponibilidad[] = 0;
									$compromisos[] = 0;
									$obligaciones[] = 0;
									$pagos[] = 0;
									$tablasinfo[] = "Reducciones2 No: ".$rowReduccion[1];
									$tten[] = 0;
									$tteg[] = 0;
									$tton[] = 0;
									$ttob[] = 0;
									$ttrp[] = 0;
									$ttcdp[] = 0;
									$tvigencia[] = $vigusu;
									$porcentaje2 = $cx * 100 / $totalcx;
									echo"
									<script>
										progres2 = '".round($porcentaje2)."';
										callprogress2(progres2);
										document.getElementById('titulosubproceso').innerHTML = 'Reducciones2 $rowcta[0]';
									</script>";
									flush();
									ob_flush();
									usleep(5);
								}
								echo "
								<script>
									document.getElementById('progreso2').style.display='none';
									document.getElementById('titulosubproceso').innerHTML='';
								</script>";
							}
							//TRASLADOS CREDITO
							{
								$cx = 0;
								$sqlCredito = "SELECT valor, id, indicador_producto, productoservicio, fuente, mediopago FROM ccpettraslados WHERE cuenta = '$rowcta[0]' AND fecha BETWEEN '$fechai' AND '$fechaf' AND vigencia = '$vigusu' AND estado != 'N' AND tipo = 'C'";
								$resCredito = mysqli_query($linkmulti, $sqlCredito);
								$totalcx = mysqli_num_rows($resCredito);
								while ($rowCredito = mysqli_fetch_row($resCredito))
								{
									$cx++;
									$numerocuenta[] = $rowcta[0];
									$nombrecuenta[] = $rowcta[1];
									$secpresupuestal[] = $rowredglobal[0];
									$vigenciagasto[] = '1';
									if($rowCredito[2] != '' && $rowCredito[2] != null)
									{
										$programatico2[] = $rowCredito[2];
										$auxprogramatico = buscarprogramatico($rowCredito[2],'2');
										if ($auxprogramatico != ''){$programatico[]= $auxprogramatico;}
										else {$programatico[] = 'NO APLICA';}
									}
									else 
									{
										$programatico[] = 'NO APLICA';
										$programatico2[] = '';
									}
									$varbpin[] = '0';
									if($rowCredito[3] != '' && $rowCredito[3] != null)
									{
										$varcpc2[] = $rowCredito[3];
										$auxcpc = buscarcpccuipo($rowCredito[3]);
										if($auxcpc != ''){$varcpc[] = $auxcpc;}
										else {$varcpc[] = 'NO APLICA';}
									}
									else 
									{
										$varcpc[] = 'NO APLICA';
										$varcpc2[] = '';
									}
									if($rowCredito[4] != '' && $rowCredito[4] != null)
									{
										$varfuentes2[] = $rowCredito[4];
										$auxfuente = buscarfuentecuipo($rowCredito[4]);
										if($auxfuente != '' && $auxfuente != null){$varfuentes[] = $auxfuente;}
										else {$varfuentes[] = 'NO APLICA';}
									}
									else 
									{
										$varfuentes[] = 'NO APLICA';
										$varfuentes2[] = '';
									}
									if($rowCredito[5] == 'CSF'){$situaciondondos[]='1';}
									else {$situaciondondos[]='2';}
									$politicapublica[] = 'NO APLICA';
									$terceroschip[] = 0;
									$apropiaini[] = 0;
									$adiciones[] = 0;
									$reducciones[] = 0;
									$trascredito[] = round($rowCredito[0]);
									$trascontracredito[] = 0;
									$apropiadef[] = round($rowCredito[0]);
									$disponibilidad[] = 0;
									$compromisos[] = 0;
									$obligaciones[] = 0;
									$pagos[] = 0;
									$tablasinfo[] = "Traslados Credito No: ".$rowCredito[1];
									$tten[] = 0;
									$tteg[] = 0;
									$tton[] = 0;
									$ttob[] = 0;
									$ttrp[] = 0;
									$ttcdp[] = 0;
									$tvigencia[] = $vigusu;
									$porcentaje2 = $cx * 100 / $totalcx;
									echo"
									<script>
										progres2 = '".round($porcentaje2)."';
										callprogress2(progres2);
										document.getElementById('titulosubproceso').innerHTML = 'Reducciones2 $rowcta[0]';
									</script>";
									flush();
									ob_flush();
									usleep(5);
								}
								echo "
								<script>
									document.getElementById('progreso2').style.display='none';
									document.getElementById('titulosubproceso').innerHTML='';
								</script>";
							}
							//TRASLADOS CONTRACREDITO
							{
								$cx = 0;
								$sqlContracreditoCredito = "SELECT valor, id, indicador_producto, productoservicio, fuente, mediopago FROM ccpettraslados WHERE cuenta = '$rowcta[0]' AND fecha BETWEEN '$fechai' AND '$fechaf' AND vigencia = '$vigusu' AND estado != 'N' AND tipo = 'R'";
								$resContracreditoCredito = mysqli_query($linkmulti, $sqlContracreditoCredito);
								$totalcx = mysqli_num_rows($resContracreditoCredit);
								while ($rowContracreditoCredito = mysqli_fetch_row($resContracreditoCredito))
								{
									$cx++;
									$numerocuenta[] = $rowcta[0];
									$nombrecuenta[] = $rowcta[1];
									$secpresupuestal[] = $rowredglobal[0];
									$vigenciagasto[] = '1';
									if($rowContracreditoCredito[2] != '' && $rowContracreditoCredito[2] != null)
									{
										$programatico2[] = $rowContracreditoCredito[2];
										$auxprogramatico = buscarprogramatico($rowContracreditoCredito[2],'2');
										if ($auxprogramatico != ''){$programatico[]= $auxprogramatico;}
										else {$programatico[] = 'NO APLICA';}
									}
									else 
									{
										$programatico[] = 'NO APLICA';
										$programatico2[] = '';
									}
									$varbpin[] = '0';
									if($rowContracreditoCredito[3] != '' && $rowContracreditoCredito[3] != null)
									{
										$varcpc2[] = $rowContracreditoCredito[3];
										$auxcpc = buscarcpccuipo($rowContracreditoCredito[3]);
										if($auxcpc != ''){$varcpc[] = $auxcpc;}
										else {$varcpc[] = 'NO APLICA';}
									}
									else 
									{
										$varcpc[] = 'NO APLICA';
										$varcpc2[] = '';
									}
									if($rowContracreditoCredito[4] != '' && $rowContracreditoCredito[4] != null)
									{
										$varfuentes2[] = $rowContracreditoCredito[4];
										$auxfuente = buscarfuentecuipo($rowContracreditoCredito[4]);
										if($auxfuente != '' && $auxfuente != null){$varfuentes[] = $auxfuente;}
										else {$varfuentes[] = 'NO APLICA';}
									}
									else 
									{
										$varfuentes[] = 'NO APLICA';
										$varfuentes2[] = '';
									}
									if($rowContracreditoCredito[5] == 'CSF'){$situaciondondos[]='1';}
									else {$situaciondondos[]='2';}
									$politicapublica[] = 'NO APLICA';
									$terceroschip[] = 0;
									$apropiaini[] = 0;
									$adiciones[] = 0;
									$reducciones[] = 0;
									$trascredito[] = 0;
									$trascontracredito[] = round($rowContracreditoCredito[0]);
									$apropiadef[] = -1 * round($rowContracreditoCredito[0]);
									$disponibilidad[] = 0;
									$compromisos[] = 0;
									$obligaciones[] = 0;
									$pagos[] = 0;
									$tablasinfo[] = "Traslados Contracredito No: ".$rowContracreditoCredito[1];
									$tten[] = 0;
									$tteg[] = 0;
									$tton[] = 0;
									$ttob[] = 0;
									$ttrp[] = 0;
									$ttcdp[] = 0;
									$tvigencia[] = $vigusu;
									$porcentaje2 = $cx * 100 / $totalcx;
									echo"
									<script>
										progres2 = '".round($porcentaje2)."';
										callprogress2(progres2);
										document.getElementById('titulosubproceso').innerHTML = 'Reducciones2 $rowcta[0]';
									</script>";
									flush();
									ob_flush();
									usleep(5);
								}
								echo "
								<script>
									document.getElementById('progreso2').style.display='none';
									document.getElementById('titulosubproceso').innerHTML='';
								</script>";
							}
							//DISPONIBILIDAD (CDP)
							{
								$cx = 0;
								$sqlcdp = "
								SELECT T1.consvigencia, T1.productoservicio, T1.fuente, T1.valor, T1.medio_pago,T1.codigo_politicap, T1.bpim, T1.indicador_producto, T1.tipo_mov, T1.codigo_vigenciag
								FROM ccpetcdp_detalle AS T1
								INNER JOIN ccpetcdp AS T2
								ON T1.consvigencia = T2.consvigencia AND T1.vigencia = T2.vigencia AND T1.tipo_mov = T2.tipo_mov AND T1.cuenta = '$rowcta[0]'
								WHERE T1.vigencia = '$vigusu' AND NOT(T2.estado='N') AND T2.fecha BETWEEN '$fechai' AND '$fechaf' 
								ORDER BY consvigencia";
								$rescdp = mysqli_query($linkmulti, $sqlcdp);
								$totalcx = mysqli_num_rows($rescdp);
								while($rowcdp = mysqli_fetch_row($rescdp))
								{
									$cx++;
									$numerocuenta[] = $rowcta[0];
									$nombrecuenta[] = $rowcta[1];
									$secpresupuestal[] = $rowredglobal[0];
									if($rowcdp[9]==''){$vigenciagasto[] = '1';}
									else {$vigenciagasto[] = $rowcdp[9];}
									if($rowcdp[7] != '' && $rowcdp[7] != null)
									{
										$programatico2[] = $rowcdp[7];
										$auxprogramatico = buscarprogramatico($rowcdp[7],'2');
										if ($auxprogramatico != ''){$programatico[]= $auxprogramatico;}
										else {$programatico[] = 'NO APLICA';}
									}
									else 
									{
										$programatico[] = 'NO APLICA';
										$programatico2[] = '';
									}
									if($rowcdp[6]!=''){$varbpin[] = $rowcdp[6];}
									else {$varbpin[] = 0;}
									if($rowcdp[1] != '' && $rowcdp[1] != null)
									{
										$varcpc2[] = $rowcdp[1];
										$auxcpc = buscarcpccuipo($rowcdp[1]);
										if($auxcpc != ''){$varcpc[] = $auxcpc;}
										else {$varcpc[] = 'NO APLICA';}
									}
									else 
									{
										$varcpc[] = 'NO APLICA';
										$varcpc2[] = '';
									}
									if($rowcdp[2] != '' && $rowcdp[2] != null)
									{
										$varfuentes2[]=$rowcdp[2];
										$auxfuente = buscarfuentecuipo($rowcdp[2]);
										if($auxfuente != '' && $auxfuente != null){$varfuentes[] = $auxfuente;}
										else {$varfuentes[] = 'NO APLICA';}
									}
									else 
									{
										$varfuentes[] = 'NO APLICA';
										$varfuentes2[] = '';
									}
									if($rowcdp[4] == 'CSF'){$situaciondondos[]='1';}
									else {$situaciondondos[]='2';}
									//if($rowcdp[5]!=''){$politicapublica[] = $rowcdp[5];}
									//else {$politicapublica[] = 'NO APLICA';}
									$politicapublica[] = 'NO APLICA';
									$terceroschip[] = 0;
									$apropiaini[] = 0;
									$adiciones[] = 0;
									$reducciones[] = 0;
									$trascredito[] = 0;
									$trascontracredito[] = 0;
									$apropiadef[] = 0;
									if(substr($rowcdp[8],0, 1) == '2'){$disponibilidad[] = round($rowcdp[3]);}
									else {$disponibilidad[] = -1 * round($rowrp[3]);}
									$compromisos[] = 0;
									$obligaciones[] = 0;
									$pagos[] = 0;
									$tablasinfo[] = "CDP No: $rowcdp[0] (Mov: $rowcdp[8])";
									$tten[] = 0;
									$tteg[] = 0;
									$tton[] = 0;
									$ttob[] = 0;
									$ttrp[] = 0;
									$ttcdp[] = $rowcdp[0];
									$tvigencia[] = $vigusu;
									$porcentaje2 = $cx * 100 / $totalcx;
									echo"
									<script>
										progres2 = '".round($porcentaje2)."';
										callprogress2(progres2);
										document.getElementById('titulosubproceso').innerHTML = 'CDP $rowcta[0]';
									</script>";
									flush();
									ob_flush();
									usleep(5);
								}
								echo "
								<script>
									document.getElementById('progreso2').style.display='none';
									document.getElementById('titulosubproceso').innerHTML='';
								</script>";
							}
							//COMPROMISOS (RP)
							{
								$cx=0;
								$sqlrp = "
								SELECT T1.consvigencia, T1.productoservicio, T1.fuente, T1.valor, T1.medio_pago,T1.codigo_politicap, T1.bpim, T1.indicador_producto, T1.tipo_mov, T1.codigo_vigenciag, T2.idcdp
								FROM ccpetrp_detalle AS T1
								INNER JOIN ccpetrp AS T2
								ON T1.consvigencia = T2.consvigencia AND T1.vigencia = T2.vigencia AND T1.tipo_mov = T2.tipo_mov AND T1.cuenta = '$rowcta[0]'
								WHERE T1.vigencia = '$vigusu' AND NOT(T2.estado='N') AND T2.fecha BETWEEN '$fechai' AND '$fechaf' 
								ORDER BY consvigencia";
								$resrp = mysqli_query($linkmulti, $sqlrp);
								$totalcx=mysqli_num_rows($resrp);
								while($rowrp = mysqli_fetch_row($resrp))
								{
									$cx++;
									$numerocuenta[] = $rowcta[0];
									$nombrecuenta[] = $rowcta[1];
									$secpresupuestal[] = $rowredglobal[0];
									if($rowrp[9]==''){$vigenciagasto[] = '1';}
									else {$vigenciagasto[] = $rowrp[9];}
									if($rowrp[7] != '' && $rowrp[7] != null)
									{
										$programatico2[] = $rowrp[7];
										$auxprogramatico = buscarprogramatico($rowrp[7],'2');
										if ($auxprogramatico != ''){$programatico[]= $auxprogramatico;}
										else {$programatico[] = 'NO APLICA';}
									}
									else 
									{
										$programatico[] = 'NO APLICA';
										$programatico2[] = '';
									}
									if($rowrp[6]!=''){$varbpin[] = $rowrp[6];}
									else {$varbpin[] = 0;}
									if($rowrp[1] != '' && $rowrp[1] != null)
									{
										$varcpc2[] = $rowrp[1];
										$auxcpc = buscarcpccuipo($rowrp[1]);
										if($auxcpc != ''){$varcpc[] = $auxcpc;}
										else {$varcpc[] = 'NO APLICA';}
									}
									else 
									{
										$varcpc[] = 'NO APLICA';
										$varcpc2[] = '';
									}
									if($rowrp[2] != '' && $rowrp[2] != null)
									{
										$varfuentes2[]=$rowrp[2];
										$auxfuente = buscarfuentecuipo($rowrp[2]);
										if($auxfuente != '' && $auxfuente != null){$varfuentes[] = $auxfuente;}
										else {$varfuentes[] = 'NO APLICA';}
									}
									else 
									{
										$varfuentes[] = 'NO APLICA';
										$varfuentes2[] = '';
									}
									if($rowrp[4] == 'CSF'){$situaciondondos[]='1';}
									else {$situaciondondos[]='2';}
									//if($rowrp[5]!=''){$politicapublica[] = $rowrp[5];}
									//else {$politicapublica[] = 'NO APLICA';}
									$politicapublica[] = 'NO APLICA';
									$terceroschip[] = 0;
									$apropiaini[] = 0;
									$adiciones[] = 0;
									$reducciones[] = 0;
									$trascredito[] = 0;
									$trascontracredito[] = 0;
									$apropiadef[] = 0;
									$disponibilidad[] = 0;
									if(substr($rowrp[8],0, 1) == '2'){$compromisos[] = round($rowrp[3]);}
									else {$compromisos[] = -1 * round($rowrp[3]);}
									$obligaciones[] = 0;
									$pagos[] = 0;
									$tablasinfo[] = "RP No: $rowrp[0] (Mov: $rowrp[8]), CDP No: $rowrp[10]";
									$tten[] = 0;
									$tteg[] = 0;
									$tton[] = 0;
									$ttob[] = 0;
									$ttrp[] = $rowrp[0];
									$ttcdp[] = $rowrp[10];
									$tvigencia[] = $vigusu;
									$porcentaje2 = $cx * 100 / $totalcx;
									echo"
									<script>
										progres2='".round($porcentaje2)."';callprogress2(progres2);
										document.getElementById('titulosubproceso').innerHTML='RP $rowcta[0]';
									</script>";
									flush();
									ob_flush();
									usleep(5);
								}
								echo "
								<script>
									document.getElementById('progreso2').style.display='none';
									document.getElementById('titulosubproceso').innerHTML='';
								</script>";
							}
							//OBLIGACIONES CXP
							{
								$cx=0;
								$sqlcxp = "
								SELECT T1.id_orden, T1.indicador_producto, T1.bpim, T1.productoservicio , T1.fuente, T1.medio_pago, T1.codigo_politicap, T1.tipo_mov, T1.valor, T2.id_rp, T1.codigo_vigenciag
								FROM tesoordenpago_det AS T1
								INNER JOIN tesoordenpago AS T2
								ON T1.id_orden = T2.id_orden AND T1.tipo_mov = T2.tipo_mov AND T1.cuentap = '$rowcta[0]'
								WHERE T2.vigencia='$vigusu' AND NOT(T2.estado='N') AND T2.fecha BETWEEN '$fechai' AND '$fechaf'
								ORDER BY T1.id_orden";
								$rescxp = mysqli_query($linkmulti, $sqlcxp);
								$totalcx=mysqli_num_rows($rescxp);
								while($rowcxp = mysqli_fetch_row($rescxp))
								{
									$cx++;
									$numerocuenta[] = $rowcta[0];
									$nombrecuenta[] = $rowcta[1];
									$secpresupuestal[] = $rowredglobal[0];
									if($rowcxp[10] == ''){$vigenciagasto[] = '1';}
									else {$vigenciagasto[] = $rowcxp[10];}
									if($rowcxp[1] != '' && $rowcxp[1] != null)
									{
										$programatico2[] = $rowcxp[1];
										$auxprogramatico = buscarprogramatico($rowcxp[1],'2');
										if ($auxprogramatico != ''){$programatico[]= $auxprogramatico;}
										else {$programatico[] = 'NO APLICA';}
									}
									else 
									{
										$programatico[] = 'NO APLICA';
										$programatico2[] = '';
									}
									if($rowcxp[2] != ''){$varbpin[] = $rowcxp[2];}
									else {$varbpin[] = 0;}
									if($rowcxp[3] != '' && $rowcxp[3] != null)
									{
										$varcpc2[] = $rowcxp[3];
										$auxcpc = buscarcpccuipo($rowcxp[3]);
										if($auxcpc != ''){$varcpc[] = $auxcpc;}
										else {$varcpc[] = 'NO APLICA';}
									}
									else 
									{
										$varcpc[] = 'NO APLICA';
										$varcpc2[] = '';
									}
									if($rowcxp[4] != '' && $rowcxp[4] != null)
									{
										$varfuentes2[] = $rowcxp[4];
										$auxfuente = buscarfuentecuipo($rowcxp[4]);
										if($auxfuente != '' && $auxfuente != null){$varfuentes[] = $auxfuente;}
										else {$varfuentes[] = 'NO APLICA';}
									}
									else 
									{
										$varfuentes[] = 'NO APLICA';
										$varfuentes2[]= '';
									}
									if($rowcxp[5] == 'CSF'){$situaciondondos[] = '1';}
									else {$situaciondondos[] = '2';}
									//if($rowcxp[6]!=''){$politicapublica[] = $rowcxp[6];}
									//else {$politicapublica[] = 'NO APLICA';}
									$politicapublica[] = 'NO APLICA';
									$terceroschip[] = 0;
									$apropiaini[] = 0;
									$adiciones[] = 0;
									$reducciones[] = 0;
									$trascredito[] = 0;
									$trascontracredito[] = 0;
									$apropiadef[] = 0;
									$disponibilidad[] = 0;
									$compromisos[] = 0;
									if(substr($rowcxp[7],0, 1) == '2'){$obligaciones[] = round($rowcxp[8]);}
									else {$obligaciones[] = -1 * round($rowcxp[8]);}
									$pagos[] = 0;
									$tablasinfo[] = "CXP No: $rowcxp[0] (Mov: $rowcxp[7], RP: $rowcxp[9])";
									$tten[] = 0;
									$tteg[] = 0;
									$tton[] = 0;
									$ttob[] = $rowcxp[0];
									$ttrp[] = $rowcxp[9];
									$auxttcdp = calcula_cdp($vigusu,$rowcxp[9]);
									if($auxttcdp == ''){$auxttcdp = 0;}
									$ttcdp[] = $auxttcdp ;
									$tvigencia[] = $vigusu;
									$porcentaje2 = $cx * 100 / $totalcx;
									echo"
									<script>
										progres2='".round($porcentaje2)."';callprogress2(progres2);
										document.getElementById('titulosubproceso').innerHTML='CXP $rowcta[0]';
									</script>";
									flush();
									ob_flush();
									usleep(5);
								}
								echo "
								<script>
									document.getElementById('progreso2').style.display='none';
									document.getElementById('titulosubproceso').innerHTML='';
								</script>";
							}
							//OBLIGACIONES NOMINA
							{
								$cx=0;
								$sqlcxpnom = "
								SELECT T1.nomina, T2.indicador, T2.bpin, T2.producto, T2.fuente, T2.medio_pago, T2.valor, T1.rp
								FROM hum_nom_cdp_rp AS T1
								INNER JOIN humnom_presupuestal AS T2 ON T1.nomina = T2.id_nom
								INNER JOIN humnomina AS T3 ON T1.nomina = T3.id_nom
								WHERE T2.cuenta = '$rowcta[0]' AND T1.vigencia = '$vigusu' AND NOT(T1.estado='N' OR T1.estado='R') AND T2.estado = 'P' AND T3.fecha BETWEEN '$fechai' AND '$fechaf'
								ORDER BY T1.nomina";
								$rescxpnom = mysqli_query($linkmulti, $sqlcxpnom);
								$totalcx=mysqli_num_rows($rescxpnom);
								while($rowcxpnom = mysqli_fetch_row($rescxpnom))
								{
									$cx++;
									$numerocuenta[] = $rowcta[0];
									$nombrecuenta[] = $rowcta[1];
									$secpresupuestal[] = $rowredglobal[0];
									$vigenciagasto[] = '1';
									if($rowcxpnom[1] != '' && $rowcxpnom[1] != null)
									{
										$programatico2[] = $rowcxpnom[1];
										$auxprogramatico = buscarprogramatico($rowcxpnom[1],'2');
										if ($auxprogramatico != ''){$programatico[]= $auxprogramatico;}
										else {$programatico[] = 'NO APLICA';}
									}
									else 
									{
										$programatico[] = 'NO APLICA';
										$programatico2[] = '';
									}
									if($rowcxpnom[2] != ''){$varbpin[] = $rowcxpnom[2];}
									else {$varbpin[] = 0;}
									if($rowcxpnom[3] != '' && $rowcxpnom[3] != null)
									{
										$varcpc2[] = $rowcxpnom[3];
										$auxcpc = buscarcpccuipo($rowcxpnom[3]);
										if($auxcpc != ''){$varcpc[] = $auxcpc;}
										else {$varcpc[] = 'NO APLICA';}
									}
									else 
									{
										$varcpc[] = 'NO APLICA';
										$varcpc2[] = '0';
									}
									if($rowcxpnom[4] != '' && $rowcxpnom[4] != null)
									{
										$varfuentes2[] = $rowcxpnom[4];
										$auxfuente = buscarfuentecuipo($rowcxpnom[4]);
										if($auxfuente != '' && $auxfuente != null){$varfuentes[] = $auxfuente;}
										else {$varfuentes[] = 'NO APLICA';}
									}
									else 
									{
										$varfuentes[] = 'NO APLICA';
										$varfuentes2[] = '';
									}
									if($rowcxpnom[5] == 'CSF'){$situaciondondos[] = '1';}
									else {$situaciondondos[] = '2';}
									$politicapublica[] = 'NO APLICA';
									$terceroschip[] = 0;
									$apropiaini[] = 0;
									$adiciones[] = 0;
									$reducciones[] = 0;
									$trascredito[] = 0;
									$trascontracredito[] = 0;
									$apropiadef[] = 0;
									$disponibilidad[] = 0;
									$compromisos[] = 0;
									$obligaciones[] = round($rowcxpnom[6]);
									$pagos[] = 0;
									$tablasinfo[] = "CXPN No: $rowcxpnom[0] (Mov: 201, RP: $rowcxpnom[7])";
									$tten[] = 0;
									$tteg[] = 0;
									$tton[] = $rowcxpnom[0];
									$ttob[] = 0;
									$ttrp[] = $rowcxpnom[7];
									$auxttcdp = calcula_cdp($vigusu,$rowcxpnom[7]);
									if($auxttcdp == ''){$auxttcdp = 0;}
									$ttcdp[] = $auxttcdp ;
									$tvigencia[] = $vigusu;
									$porcentaje2 = $cx * 100 / $totalcx;
									echo"
									<script>
										progres2='".round($porcentaje2)."';callprogress2(progres2);
										document.getElementById('titulosubproceso').innerHTML='CXPN $rowcta[0]';
									</script>";
									flush();
									ob_flush();
									usleep(5);
								}
								echo "
								<script>
									document.getElementById('progreso2').style.display='none';
									document.getElementById('titulosubproceso').innerHTML='';
								</script>";
							}
							//PAGOS EGRESOS
							{
								$cx=0;
								$sqlegreso ="
								SELECT T2.id_egreso, T1.id_orden, T1.indicador_producto, T1.bpim, T1.productoservicio, T1.fuente, T1.medio_pago, T1.codigo_politicap, T2.tipo_mov, T1.valor, T3.id_rp, T1.codigo_vigenciag
								FROM tesoordenpago_det AS T1
								INNER JOIN tesoegresos AS T2 ON T1.id_orden = T2.id_orden
								INNER JOIN tesoordenpago AS T3 ON T1.id_orden = T3.id_orden
								WHERE T1.cuentap = '$rowcta[0]' AND T3.vigencia='$vigusu' AND NOT(T1.estado='N') AND T2.fecha BETWEEN '$fechai' AND '$fechaf'
								ORDER BY T1.id_orden";
								$resegreso = mysqli_query($linkmulti,$sqlegreso);
								$totalcx=mysqli_num_rows($resegreso);
								while($rowegreso = mysqli_fetch_row($resegreso))
								{
									$cx++;
									$numerocuenta[] = $rowcta[0];
									$nombrecuenta[] = $rowcta[1];
									$secpresupuestal[] = $rowredglobal[0];
									if($rowegreso[11] == ''){$vigenciagasto[] = '1';}
									else {$vigenciagasto[] = $rowegreso[11];}
									if($rowegreso[2] != '' && $rowegreso[2] != null)
									{
										$programatico2[] = $rowegreso[2];
										$auxprogramatico = buscarprogramatico($rowegreso[2],'2');
										if ($auxprogramatico != ''){$programatico[]= $auxprogramatico;}
										else {$programatico[] = 'NO APLICA';}
									}
									else 
									{
										$programatico[] = 'NO APLICA';
										$programatico2[] = '';
									}
									if($rowegreso[3] != ''){$varbpin[] = $rowegreso[3];}
									else {$varbpin[] = 0;}
									if($rowegreso[4] != '' && $rowegreso[4] != null)
									{
										$varcpc2[] = $rowegreso[4];
										$auxcpc = buscarcpccuipo($rowegreso[4]);
										if($auxcpc != ''){$varcpc[] = $auxcpc;}
										else {$varcpc[] = 'NO APLICA';}
									}
									else 
									{
										$varcpc[] = 'NO APLICA';
										$varcpc2[] = '0';
									}
									if($rowegreso[5] != '' && $rowegreso[5] != null)
									{
										$varfuentes2[] = $rowegreso[5];
										$auxfuente = buscarfuentecuipo($rowegreso[5]);
										if($auxfuente != '' && $auxfuente != null){$varfuentes[] = $auxfuente;}
										else {$varfuentes[] = 'NO APLICA';}
									}
									else 
									{
										$varfuentes[] = 'NO APLICA';
										$varfuentes2[] = '';
									}
									if($rowegreso[6] == 'CSF'){$situaciondondos[] = '1';}
									else {$situaciondondos[] = '2';}
									//if($rowegreso[7]!=''){$politicapublica[] = $rowegreso[7];}
									//else {$politicapublica[] = 'NO APLICA';}
									$politicapublica[] = 'NO APLICA';
									$terceroschip[] = 0;
									$apropiaini[] = 0;
									$adiciones[] = 0;
									$reducciones[] = 0;
									$trascredito[] = 0;
									$trascontracredito[] = 0;
									$apropiadef[] = 0;
									$disponibilidad[] = 0;
									$compromisos[] = 0;
									$obligaciones[] = 0;
									if(substr($rowegreso[8],0, 1) == '2'){$pagos[] = round($rowegreso[9]);}
									else {$pagos[] = -1 * round($rowegreso[9]);}
									$tablasinfo[] = "Egreso No: $rowegreso[0] (Mov: $rowegreso[8], CXP: $rowegreso[1], RP: $rowegreso[10])";
									$tten[] = 0;
									$tteg[] = $rowegreso[0];
									$tton[] = 0;
									$ttob[] = $rowegreso[1];
									$ttrp[] = $rowegreso[10];
									$auxttcdp = calcula_cdp($vigusu,$rowegreso[10]);
									if($auxttcdp == ''){$auxttcdp = 0;}
									$ttcdp[] = $auxttcdp ;
									$tvigencia[] = $vigusu;
									$porcentaje2 = $cx * 100 / $totalcx;
									echo"
									<script>
										progres2='".round($porcentaje2)."';callprogress2(progres2);
										document.getElementById('titulosubproceso').innerHTML='Egreso $rowcta[0]';
									</script>";
									flush();
									ob_flush();
									usleep(5);
								}
								echo "
								<script>
									document.getElementById('progreso2').style.display='none';
									document.getElementById('titulosubproceso').innerHTML='';
								</script>";
							}
							//PAGOS EGRESOS NOMINA
							{
								$cx=0;
								$sqlegresonom ="
								SELECT T1.id_egreso, T1.id_orden, T2.indicador_producto, T2.bpin, T2.productoservicio, T2.fuente, T2.medio_pago, T2.codigo_politicap, T2.tipo_mov, T2.valordevengado
								FROM tesoegresosnomina AS T1
								INNER JOIN tesoegresosnomina_det AS T2
								ON T1.id_egreso = T2.id_egreso
								WHERE T2.cuentap = '$rowcta[0]' AND T1.vigencia = '$vigusu' AND NOT(T1.estado='N' OR T1.estado='R') AND NOT(T2.tipo='SE' OR T2.tipo='PE' OR T2.tipo='DS' OR T2.tipo='RE' OR T2.tipo='FS') AND T1.fecha BETWEEN '$fechai' AND '$fechaf'";
								$resegresonom = mysqli_query($linkmulti,$sqlegresonom);
								$totalcx=mysqli_num_rows($resegresonom);
								while($rowegresonom = mysqli_fetch_row($resegresonom))
								{
									$cx++;
									$numerocuenta[] = $rowcta[0];
									$nombrecuenta[] = $rowcta[1];
									$secpresupuestal[] = $rowredglobal[0];
									$vigenciagasto[] = '1';
									if($rowegresonom[2] != '' && $rowegresonom[2] != null)
									{
										$programatico2[] = $rowegresonom[2];
										$auxprogramatico = buscarprogramatico($rowegresonom[2],'2');
										if ($auxprogramatico != ''){$programatico[]= $auxprogramatico;}
										else {$programatico[] = 'NO APLICA';}
									}
									else 
									{
										$programatico[] = 'NO APLICA';
										$programatico2[] = '';
									}
									if($rowegresonom[3] != ''){$varbpin[] = $rowegresonom[3];}
									else {$varbpin[] = 0;}
									if($rowegresonom[4] != '' && $rowegresonom[4] != null)
									{
										$varcpc2[] = $rowegresonom[4];
										$auxcpc = buscarcpccuipo($rowegresonom[4]);
										if($auxcpc != ''){$varcpc[] = $auxcpc;}
										else {$varcpc[] = 'NO APLICA';}
									}
									else 
									{
										$varcpc[] = 'NO APLICA';
										$varcpc2[] = '0';
									}
									if($rowegresonom[5] != '' && $rowegresonom[5] != null)
									{
										$varfuentes2[] = $rowegresonom[5];
										$auxfuente = buscarfuentecuipo($rowegresonom[5]);
										if($auxfuente != '' && $auxfuente != null){$varfuentes[] = $auxfuente;}
										else {$varfuentes[] = 'NO APLICA';}
									}
									else 
									{
										$varfuentes[] = 'NO APLICA';
										$varfuentes2[] = '';
									}
									if($rowegresonom[6] == 'CSF'){$situaciondondos[] = '1';}
									else {$situaciondondos[] = '2';}
									//if($rowegresonom[7]!=''){$politicapublica[] = $rowegresonom[7];}
									//else {$politicapublica[] = 'NO APLICA';}
									$politicapublica[] = 'NO APLICA';
									$terceroschip[] = 0;
									$apropiaini[] = 0;
									$adiciones[] = 0;
									$reducciones[] = 0;
									$trascredito[] = 0;
									$trascontracredito[] = 0;
									$apropiadef[] = 0;
									$disponibilidad[] = 0;
									$compromisos[] = 0;
									$obligaciones[] = 0;
									if(substr($rowegresonom[8],0, 1) == '2'){$pagos[] = round($rowegresonom[9]);}
									else {$pagos[] = -1 * round($rowegresonom[9]);}
									$sqlrpnom = "SELECT rp,cdp FROM hum_nom_cdp_rp WHERE nomina = '$rowegresonom[1]'";
									$resrpnom = mysqli_query($linkmulti, $sqlrpnom);
									$rowrpnom = mysqli_fetch_row($resrpnom);
									$tablasinfo[] = "Egreso Nomina No: $rowegresonom[0] (Mov: $rowegresonom[8], CXPN: $rowegresonom[1], RP: $rowrpnom[0])";
									$tten[] = $rowegresonom[0];
									$tteg[] = 0;
									$tton[] = $rowegresonom[1];
									$ttob[] = 0;
									$ttrp[] = $rowrpnom[0];
									$ttcdp[] = $rowrpnom[1];
									$tvigencia[] = $vigusu;
									$porcentaje2 = $cx * 100 / $totalcx;
									echo"
									<script>
										progres2='".round($porcentaje2)."';callprogress2(progres2);
										document.getElementById('titulosubproceso').innerHTML='Egreso Nomina $rowcta[0]';
									</script>";
									flush();
									ob_flush();
									usleep(5);
									echo "
									<script>
										document.getElementById('progreso2').style.display='none';
										document.getElementById('titulosubproceso').innerHTML='';
									</script>";
								}
							}
							$porcentaje = $c * 100 / $totalcli;
							echo"
							<script>
								progres='".round($porcentaje)."';
								callprogress(progres);
								document.getElementById('totalcuentas').innerHTML='Recolectando Información: $numid / $totalcli';
							</script>";
							flush();
							ob_flush();
							usleep(5);
						}
						
					}
					echo "<script>document.getElementById('titulogento').innerHTML='.: EJECUCI&Oacute;N PRESUPUESTAL';</script>";
					$c=0;
					$totalcli=count($numerocuenta);
					for($xx=0;$xx<$totalcli;$xx++)
					{
						$c++;
						$idtemporal++;
						$sqltabla2 = "INSERT INTO $nombretablatemporal (id, codigocuenta, nombrecuenta, seccpresupuestal, vigenciagastos, programatico, bpin, cpc, fuente, situacionfondo, politicapublica, tercerochip, disponibilidad, compromisos, obligaciones, pagos, tipotabla, fuente2, cpc2, programatico2, apropiacionini, apropiaciondef, adiciones, reducciones, trascredito, trascontracredito, tten, tteg, tton, ttob, ttrp, ttcdp, tvigencia) VALUES ('$idtemporal', '$numerocuenta[$xx]', '$nombrecuenta[$xx]', '$secpresupuestal[$xx]', '$vigenciagasto[$xx]', '$programatico[$xx]', '$varbpin[$xx]', '$varcpc[$xx]', '$varfuentes[$xx]', '$situaciondondos[$xx]', '$politicapublica[$xx]', '$terceroschip[$xx]', '$disponibilidad[$xx]', '$compromisos[$xx]', '$obligaciones[$xx]', '$pagos[$xx]', '$tablasinfo[$xx]', '$varfuentes2[$xx]', '$varcpc2[$xx]', '$programatico2[$xx]', '$apropiaini[$xx]', '$apropiadef[$xx]', '$adiciones[$xx]','$reducciones[$xx]', '$trascredito[$xx]', '$trascontracredito[$xx]', '$tten[$xx]', '$tteg[$xx]', '$tton[$xx]', '$ttob[$xx]', '$ttrp[$xx]', '$ttcdp[$xx]', '$tvigencia[$xx]')";
						mysqli_query($linkbd,$sqltabla2);
						$porcentaje = $c * 100 / $totalcli;
						echo"
						<script>
							progres='".round($porcentaje)."';callprogress(progres);
							document.getElementById('totalcuentas').innerHTML='Procesando Información $c / $totalcli';
						</script>";
						flush();
						ob_flush();
						usleep(1);
					}
					$iter = "zebra1";
					$iter2 = "zebra2";
					$c=0;
					$sqlcta ="SELECT codigo,nombre FROM cuentasccpet WHERE version='$maxVersion' AND municipio=1 AND tipo = 'C' ORDER BY id ASC";
					if($_POST['selvigegasto']!='-1')
					{
						if($_POST['selvigegasto']=='NO APLICA'){$filtro1 = " AND vigenciagastos = ''";}
						else {$filtro1 = "AND vigenciagastos= ".$_POST['selvigegasto'];}
					}
					else {$filtro1 = '';}
					if($_POST['selprogramatico']!='-1')
					{
						if($_POST['selprogramatico']=='NO APLICA'){$filtro2 = " AND programatico2 = ''";}
						else {$filtro2 = "AND programatico2 = ".$_POST['selprogramatico'];}
					}
					else {$filtro2 = '';}
					if($_POST['selcpc']!='-1')
					{
						if($_POST['selcpc']=='NO APLICA'){$filtro3 = " AND cpc2 = ''";}
						else {$filtro3 = "AND cpc2 = ".$_POST['selcpc'];}
					}
					else {$filtro3 = '';}
					if($_POST['selfuentes']!='-1')
					{
						if($_POST['selfuentes']=='NO APLICA'){$filtro4 = " AND fuente2 = ''";}
						else {$filtro4 = "AND fuente2 = ".$_POST['selfuentes'];}
					}
					else {$filtro4 = '';}
					if($_POST['selbpin']!='-1')
					{
						if($_POST['selbpin']=='NO APLICA'){$filtro5 = " AND bpin = ''";}
						else {$filtro5 = "AND bpin = ".$_POST['selbpin'];}
					}
					else {$filtro5 = '';}
					
					$sqlver="
					SELECT id, codigocuenta, nombrecuenta, seccpresupuestal, vigenciagastos, programatico, bpin, cpc, fuente, situacionfondo, politicapublica, tercerochip, SUM(compromisos), SUM(obligaciones), SUM(pagos), fuente2, cpc2, programatico2, SUM(disponibilidad), SUM(apropiacionini), SUM(apropiaciondef), SUM(adiciones), SUM(reducciones), SUM(trascredito), SUM(trascontracredito)
					FROM $nombretablatemporal
					WHERE 1 = 1 $filtro1 $filtro2 $filtro3 $filtro4 $filtro5
					GROUP BY codigocuenta, seccpresupuestal, programatico, cpc, fuente, bpin, vigenciagastos
					ORDER BY seccpresupuestal, vigenciagastos, id ASC"; 
					$resver = mysqli_query($linkbd,$sqlver);
					$totalcli = mysqli_num_rows($resver);
					while ($rowver = mysqli_fetch_row($resver))
					{
						$c++;
						$porcentaje = $c * 100 / $totalcli;
						$colorlinea=fcolorlinea($rowver[12],$rowver[13],$rowver[14]);
						echo "
						<tr class='$iter' $colorlinea onDblClick=\"auxiliarEP('$rowver[1]', '$rowver[3]', '$rowver[5]', '$rowver[7]', '$rowver[8]', '$rowver[6]', '$rowver[4]');\">
							<td class='titulos2'>
								<a onClick=\"detalled('$c', '$rowver[1]', '$rowver[3]', '$rowver[5]', '$rowver[7]', '$rowver[8]', '$rowver[6]', '$rowver[4]')\" style='cursor:pointer;'>
									<img id='img$c' src='imagenes/plus.gif'>
								</a>
							</td>
							<td style='width:10%;' title='Cuenta'>$rowver[1]</td>
							<td title='Descripción'>$rowver[2]</td>
							<td title='Vigencia Gasto'>$rowver[4]</td>
							<td title='Sección Presupuestal'>$rowver[3]</td>
							<td title='Programatico MGA'>$rowver[17] ($rowver[5])</td>
							<td title='CPC'>$rowver[16] ($rowver[7])</td>
							<td title='Fuente'>$rowver[15] ($rowver[8])</td>
							<td title='BPIN'>$rowver[6]</td>
							<td title='Situación Fondos'>$rowver[9]</td>
							<td title='Politica Publica'>$rowver[10]</td>
							<td title='Terceros CHIP'>$rowver[11]</td>
							<td style='text-align:right;' title='Inicial'>$".number_format($rowver[19],0,',','.')."</td>
							<td style='text-align:right;' title='Adiciones'>$".number_format($rowver[21],0,',','.')."</td>
							<td style='text-align:right;' title='Reducciones'>$".number_format($rowver[22],0,',','.')."</td>
							<td style='text-align:right;' title='Credito'>$".number_format($rowver[23],0,',','.')."</td>
							<td style='text-align:right;' title='Contracredito'>$".number_format($rowver[24],0,',','.')."</td>
							<td style='text-align:right;' title='Definitivo'>$".number_format($rowver[20],0,',','.')."</td>
							<td style='text-align:right;' title='Disponibilidad'>$".number_format($rowver[18],0,',','.')."</td>
							<td style='text-align:right;' title='Compromisos'>$".number_format($rowver[12],0,',','.')."</td>
							<td style='text-align:right;' title='Obligaciones'>$".number_format($rowver[13],0,',','.')."</td>
							<td style='text-align:right;' title='Pagos'>$".number_format($rowver[14],0,',','.')."</td>
						</tr>
						<tr>
							<td align='center'></td>
							<td colspan='21'>
								<div id='detalle$c' style='display:none'></div>
							</td>
						</tr>
						<script>
							progres='".round($porcentaje)."';callprogress(progres);
							document.getElementById('totalcuentas').innerHTML='Visualizar Información $c / $totalcli';
						</script>";
						flush();
						ob_flush();
						usleep(1);
						$aux=$iter;
						$iter=$iter2;
						$iter2=$aux;
						if(($rowver[12] == 0) && ($rowver[13] == 0) && ($rowver[14] == 0)){}
						else
						{
							if($rowver[5]=='NO APLICA'){$valpromga=0;}
							else {$valpromga=$rowver[5];}
							if($rowver[7]=='NO APLICA'){$valcpcd=0;}
							else {$valcpcd=$rowver[7];}
							if($rowver[8]=='NO APLICA'){$valfuented=0;}
							else {$valfuented=$rowver[8];}
							if($rowver[10]=='NO APLICA'){$valppd=0;}
							else {$valppd=$rowver[10];}
						}
					}
					echo "
						</table>
					</div>
					<script>document.getElementById('progreso').style.display='none';</script>";
					unset ($auxprogramatico2,$auxvigenciagasto,$auxcpc2,$auxvarfuentes2,$auxvarbpin);
					{
						for($zz = 0; $zz < count($programatico2); $zz++)
						{
							$ch = esta_en_array($auxprogramatico2,$programatico2[$zz]);
							if($ch<1){$auxprogramatico2[] = $programatico2[$zz];}
						}
						for($zz = 0; $zz < count($auxprogramatico2); $zz++)
						{
							if($_POST['selprogramatico'] == 'NO APLICA'){$comparador = '';}
							else {$comparador = $_POST['selprogramatico'];}
							if($comparador == $auxprogramatico2[$zz]){$selactiva = "option.selected = true;";}
							else {$selactiva = "";}
							if($auxprogramatico2[$zz] == ''){$numprogramatico = 'NO APLICA';}
							else {$numprogramatico = $auxprogramatico2[$zz];}
							echo"
							<script>
								var select = document.getElementById('selprogramatico');
								var option = document.createElement('option');
								option.value = '$numprogramatico';
								option.text = '$numprogramatico';
								$selactiva
								select.appendChild(option).appendChild(option);
							</script>";
						}
					}//Filtro Programarico
					{
						for($zz = 0; $zz < count($vigenciagasto); $zz++)
						{
							$ch = esta_en_array($auxvigenciagasto,$vigenciagasto[$zz]);
							if($ch<1){$auxvigenciagasto[] = $vigenciagasto[$zz];}
						}
						for($zz = 0; $zz < count($auxvigenciagasto); $zz++)
						{
							if($_POST['selvigegasto'] == 'NO APLICA'){$comparador = '';}
							else {$comparador = $_POST['selvigegasto'];}
							if($comparador == $auxvigenciagasto[$zz]){$selactiva = "option.selected = true;";}
							else {$selactiva = "";}
							if($auxvigenciagasto[$zz] == ''){$numvigenciagasto = 'NO APLICA';}
							else {$numvigenciagasto = $auxvigenciagasto[$zz];}
							echo"
							<script>
								var select = document.getElementById('selvigegasto');
								var option = document.createElement('option');
								option.value = '$numvigenciagasto';
								option.text = '$numvigenciagasto';
								$selactiva
								select.appendChild(option).appendChild(option);
							</script>";
						}
					}//Filtro Vigencia del Gasto
					{
						for($zz = 0; $zz < count($varcpc2); $zz++)
						{
							$ch = esta_en_array($auxcpc2,$varcpc2[$zz]);
							if($ch<1){$auxcpc2[] = $varcpc2[$zz];}
						}
						for($zz = 0; $zz < count($auxcpc2); $zz++)
						{
							if($_POST['selcpc'] == 'NO APLICA'){$comparador = '';}
							else {$comparador = $_POST['selcpc'];}
							if($comparador == $auxcpc2[$zz]){$selactiva = "option.selected = true;";}
							else {$selactiva = "";}
							if($auxcpc2[$zz] == ''){$numcpc = 'NO APLICA';}
							else {$numcpc = $auxcpc2[$zz];}
							if($numcpc != 'NO APLICA')
							{
								echo"
								<script>
									var select = document.getElementById('selcpc');
									var option = document.createElement('option');
									option.value = '$numcpc';
									option.text = '$numcpc';
									$selactiva
									select.appendChild(option).appendChild(option);
								</script>";
							}
						}
					}//Filtro CPC
					{
						for($zz = 0; $zz < count($varfuentes2); $zz++)
						{
							$ch = esta_en_array($auxvarfuentes2,$varfuentes2[$zz]);
							if($ch<1){$auxvarfuentes2[] = $varfuentes2[$zz];}
						}
						for($zz = 0; $zz < count($auxvarfuentes2); $zz++)
						{
							if($_POST['selfuentes'] == 'NO APLICA'){$comparador = '';}
							else {$comparador = $_POST['selfuentes'];}
							if($comparador == $auxvarfuentes2[$zz]){$selactiva = "option.selected = true;";}
							else {$selactiva = "";}
							if($auxvarfuentes2[$zz] == ''){$numfuentes = 'NO APLICA';}
							else {$numfuentes = $auxvarfuentes2[$zz];}
							echo"
							<script>
								var select = document.getElementById('selfuentes');
								var option = document.createElement('option');
								option.value = '$numfuentes';
								option.text = '$numfuentes';
								$selactiva
								select.appendChild(option).appendChild(option);
							</script>";
						}
					}//Filtro Fuentes
					{
						for($zz = 0; $zz < count($varbpin); $zz++)
						{
							$ch = esta_en_array($auxvarbpin,$varbpin[$zz]);
							if($ch<1){$auxvarbpin[] = $varbpin[$zz];}
						}
						for($zz = 0; $zz < count($auxvarbpin); $zz++)
						{
							if($_POST['selbpin'] == 'NO APLICA'){$comparador = '';}
							else {$comparador = $_POST['selbpin'];}
							if($comparador == $auxvarbpin[$zz]){$selactiva = "option.selected = true;";}
							else {$selactiva = "";}
							if($auxvarbpin[$zz] == ''){$numbpin = 'NO APLICA';}
							else {$numbpin = $auxvarbpin[$zz];}
							echo"
							<script>
								var select = document.getElementById('selbpin');
								var option = document.createElement('option');
								option.value = '$numbpin';
								option.text = '$numbpin';
								$selactiva
								select.appendChild(option).appendChild(option);
							</script>";
						}
					}//Filtro Fuentes
				}
			?>
			
		</form>
		<div id="bgventanamodal2">
			<div id="ventanamodal2">
				<IFRAME  src="" name="buster" marginWidth=0 marginHeight=0 frameBorder=0 id="ventana2" frameSpacing=0 style="left:500px; width:900px; height:500px; top:200;"></IFRAME>
			</div>
		</div>
	</body>
</html>
