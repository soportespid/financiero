<?php //V 1000 12/12/16 ?> 
<?php
require "comun.inc";
require "funciones.inc";
require "conversor.php";
session_start();
$linkbd_v7 = conectar_v7();
$linkbd_v7 -> set_charset("utf8");
?>
<html lang="es">
	<head>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
    	<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<title>:: IDEAL 10 - Tesorer&iacute;a</title>

		<script>
			//************* ver reporte ************
			//***************************************
			function verep(idfac)
			{
				document.form1.oculto.value=idfac;
				document.form1.submit();
			}

			//************* genera reporte ************
			//***************************************
			function genrep(idfac)
			{
				document.form2.oculto.value=idfac;
				document.form2.submit();
			}
	
			function buscacta(e)
			{
				if (document.form2.cuenta.value!="")
				{
					document.form2.bc.value='1';
					document.form2.submit();
				}
			}

			function validar()
			{
				document.form2.submit();
			}	
	
			function buscater(e)
			{
				if (document.form2.tercero.value!="")
				{
				document.form2.bt.value='1';
				document.form2.submit();
				}
			}

			function agregardetalle()
			{
				if(document.form2.codingreso.value!="" &&  document.form2.valor.value>0  )
				{ 
					document.form2.agregadet.value=1;
			//			document.form2.chacuerdo.value=2;
					document.form2.submit();
				}
				else 
				{
					alert("Falta informacion para poder Agregar");
				}
			}

			function eliminar(variable)
			{
				if (confirm("Esta Seguro de Eliminar"))
				{
				document.form2.elimina.value=variable;
				//eli=document.getElementById(elimina);
				vvend=document.getElementById('elimina');
				//eli.value=elimina;
				vvend.value=variable;
				document.form2.submit();
				}
			}

			//************* genera reporte ************
			//***************************************
			function guardar()
			{
				ingresos2 = document.getElementsByName('dcoding[]');
				
				let medioDePago = document.getElementById('medioDePago').value;
				let banco = document.getElementById('cb').value;
				
				
				if(medioDePago == 1)
				{
					if (document.form2.fecha.value!='' && ingresos2.length>0 && document.form2.presupuesto.value!='-1' && banco != '')
					{
						despliegamodalm('visible','4','Esta Seguro de Guardar','2');
						/* if (confirm("Esta Seguro de Guardar"))
						{
							document.form2.oculto.value=2;
							document.form2.submit();
						} */
					}
					else
					{
						despliegamodalm('visible','2',"Falta información para poder guardar");
						document.form2.fecha.focus();
						document.form2.fecha.select();
					}
				}
				else
				{
					let noConta = document.getElementById('mediodepagosgr').value;
					if(noConta == '')
					{
						if(window.confirm("Esta seguro de no contabilizar este recaudo transferencia?"))
						{
							if (document.form2.fecha.value!='' && ingresos2.length>0 && document.form2.presupuesto.value!='-1')
							{
								despliegamodalm('visible','4','Esta Seguro de Guardar','2');
								/* if (confirm("Esta Seguro de Guardar"))
								{
									document.form2.oculto.value=2;
									document.form2.submit();
								} */
							}
							else
							{
								despliegamodalm('visible','2',"Falta información para poder guardar");
								document.form2.fecha.focus();
								document.form2.fecha.select();
							}
						}
					}
					else
					{
						if (document.form2.fecha.value!='' && ingresos2.length>0 && document.form2.presupuesto.value!='-1')
						{
							despliegamodalm('visible','4','Esta Seguro de Guardar','2');
							/* if (confirm("Esta Seguro de Guardar"))
							{
								document.form2.oculto.value=2;
								document.form2.submit();
							} */
						}
						else
						{
							despliegamodalm('visible','2',"Falta información para poder guardar");
							document.form2.fecha.focus();
							document.form2.fecha.select();
						}
					}
					
				}
			}

			function pdf()
			{
				document.form2.action="teso-pdfrecaudostrans.php";
				document.form2.target="_BLANK";
				document.form2.submit(); 
				document.form2.action="";
				document.form2.target="";
			}

			function buscater(e)
			{
				if (document.form2.tercero.value!="")
				{
					document.form2.bt.value='1';
					document.form2.submit();
				}
			}

			function buscaing(e)
			{
				if (document.form2.codingreso.value!="")
				{
					document.form2.bin.value='1';
					document.form2.submit();
				}
			}

			function despliegamodal2(_valor)
			{
				document.getElementById("bgventanamodal2").style.visibility=_valor;
				if(_valor=="hidden")
				{
					document.getElementById('ventana2').src="";
				}
				else 
				{
					document.getElementById('ventana2').src="ingresos-ventana.php?ti=I&modulo=4";
				}
			}

			function despliegamodalm(_valor,_tip,mensa,pregunta)
			{
				document.getElementById("bgventanamodalm").style.visibility=_valor;
				if(_valor=="hidden")
				{
					document.getElementById('ventanam').src="";
					switch(document.getElementById('valfocus').value)
					{
						case "1":	document.getElementById('valfocus').value='';
									document.getElementById('tercero').focus();
									document.getElementById('tercero').select();
									break;
						case "2":	document.getElementById('valfocus').value='';
									document.getElementById('codingreso').focus();
									document.getElementById('codingreso').select();
									break;
					}
				}
				else
				{
					switch(_tip)
					{
						case "1":
							document.getElementById('ventanam').src="ventana-mensaje1.php?titulos="+mensa;break;
						case "2":
							document.getElementById('ventanam').src="ventana-mensaje3.php?titulos="+mensa;break;
						case "3":
							document.getElementById('ventanam').src="ventana-mensaje2.php?titulos="+mensa;break;
						case "4":
							document.getElementById('ventanam').src="ventana-consulta1.php?titulos="+mensa+"&idresp="+pregunta;break;	
					}
				}
			}

			function funcionmensaje()
			{
				var numdocar=document.getElementById('idcomp').value;
				document.location.href = "teso-editarecaudotransferencia.php?idrecaudo="+numdocar;
			}

			function respuestaconsulta(pregunta)
			{
				switch(pregunta)
				{
					case "1":	document.form2.oculto.value='3';
								document.form2.submit();
								break;
					case "2":	document.form2.oculto.value='2';
								document.form2.submit();
								break;
				}
			}

			function adelante()
			{
				var maximo=document.form2.maximo.value;
				//alert(maximo);
				var actual=document.form2.idcomp.value;
				if(parseFloat(maximo)>parseFloat(actual))
				{
					var idcta=parseFloat(document.form2.idcomp.value)+1;
					location.href="teso-editarecaudotransferencia.php?idrecaudo="+idcta;
				}
			}
		
			function atrasc()
			{
				var actual=document.form2.idcomp.value;
				if(0<parseFloat(actual))
				{
					var idcta=document.form2.idcomp.value-1;
					location.href="teso-editarecaudotransferencia.php?idrecaudo="+idcta;
				}
			}
		
			function iratras()
			{
				var idcomp=document.form2.idcomp.value;
				location.href="teso-buscarecaudotransferencia.php?id="+idcomp;
			}
		</script>
		<script src="css/programas.js"></script>
		<script src="css/calendario.js"></script>
		<link href="css/css2.css" rel="stylesheet" type="text/css" />
		<link href="css/css3.css" rel="stylesheet" type="text/css" />
		<link href="css/css4.css" rel="stylesheet" type="text/css" />
		<link href="css/tabs.css" rel="stylesheet" type="text/css" />
		<?php titlepag();?>
	</head>
	<body>
		<div id="bgventanamodalm" class="bgventanamodalm">
            <div id="ventanamodalm" class="ventanamodalm">
                <IFRAME src="" name="ventanam" marginWidth=0 marginHeight=0 frameBorder=0 id="ventanam" frameSpacing=0 style=" width:700px; height:130px; top:200; overflow:hidden;"> 
                </IFRAME>
            </div>
        </div>
		<IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
		<span id="todastablas2"></span>
		<table>
			<tr><script>barra_imagenes("teso");</script><?php cuadro_titulos();?></tr>	 
			<tr><?php menu_desplegable("teso");?></tr>
			<tr>
				<td colspan="3" class="cinta">
					<a href="teso-recaudotransferencia.php" class="mgbt" ><img src="imagenes/add.png" title="Nuevo"/></a>
					<a class="mgbt"><img src="imagenes/guardad.png" title="Guardar" /></a>
					<a href="teso-buscarecaudotransferencia.php" class="mgbt"> <img src="imagenes/busca.png" title="Buscar" /></a>
					<a onClick="mypop=window.open('plan-agenda.php','','');mypop.focus()" class="mgbt"><img src="imagenes/agenda1.png" title="Agenda"/></a>
					<a onClick="mypop=window.open('teso-principal.php','','');mypop.focus();" class="mgbt"><img src="imagenes/nv.png" title="Nueva ventana"></a>
					<a onClick="mypop=window.open('<?php echo $url2; ?>','','');mypop.focus();" class="tooltip bottom mgbt"><img src="imagenes/duplicar_pantalla.png" title="Duplicar pesta&ntilde;a"></a>
					<a onClick="pdf()" class="mgbt"> <img src="imagenes/print.png"  title="Imprimir" /></a>
					<a onClick="iratras()" class="mgbt"><img src="imagenes/iratras.png" title="Atr&aacute;s"></a>
				</td>
			</tr>		  
		</table>
		<tr>
			<td colspan="3" class="tablaprin" align="center"> 
				<?php
					
					if($_POST['tabgroup1'] == 1)
					{
						$check1 = "checked";
					}
					if(!$_POST['oculto'])
					{
						$_POST['dcoding'] = [];
						$_POST['dncoding'] = [];
						$_POST['dvalores'] = [];


						$_POST['tabgroup1'] = 1;
						$check1 = "checked";
						
						//** cuenta caja de parametros tesoria, archivos maestros */
						$sqlr = "SELECT cuentacaja FROM tesoparametros";
						$res = mysqli_query($linkbd_v7, $sqlr);
						$row = mysqli_fetch_row($res);
						$_POST['cuentacaja']=$row[0];


						switch($_POST['tabgroup1'])
						{
							case 1:	$check1='checked';break;
							case 2:	$check2='checked';break;
							case 3:	$check3='checked';
						}
						
						$sqlr = "SELECT idcomp, fecha, vigencia, banco, ncuentaban, concepto, tercero, cc, estado, mediopago FROM tesorecaudotransferencia WHERE id_recaudo = $_GET[idrecaudo]";//echo "$sqlr";
						$res = mysqli_query($linkbd_v7, $sqlr);
						$row = mysqli_fetch_row($res);
						
						$_POST['idcomp'] = $_GET['idrecaudo'];
						$_POST['liquidacion'] = $row[0];
						$p1 = substr($row[1],0,4);
						$p2 = substr($row[1],5,2);
						$p3 = substr($row[1],8,2);
						$_POST['fecha'] = $p3."/".$p2."/".$p1;
						$_POST['vigencia'] = $row[2];
						$_POST['ter'] = $row[3];
						$_POST['cb'] = $row[4];
						$_POST['nbanco'] = buscatercero($_POST['ter']);
						$_POST['concepto'] = $row[5];
						$_POST['tercero'] = $row[6];
						$_POST['ntercero'] = buscatercero($row[6]);
						$_POST['cc'] = $row[7];
						$_POST['estadoc'] = $row[8];
						$_POST['mediodepagosgr'] = $row[9];
						
						$total = 0;
						$sqlr_det = "SELECT ingreso, valor FROM tesorecaudotransferencia_det WHERE id_recaudo = $_POST[idcomp] AND estado = 'S'";
						$res_det = mysqli_query($linkbd_v7, $sqlr_det);
						while ($row_det = mysqli_fetch_row($res_det))
						{
							$_POST['dcoding'][] = $row_det[0];
							$_POST['dncoding'][] = buscaingreso($row_det[0]);
							$_POST['dvalores'][] = $row_det[1];
							$total+=$row_det[1];
						}	
					}

					if($_POST['liquidacion'] != '')
					{
						$sqlr_liq = "SELECT medio_pago FROM tesorecaudotransferencialiquidar WHERE id_recaudo = $_POST[liquidacion] AND estado!='N'";
						$res_liq = mysqli_query($linkbd_v7, $sqlr_liq);
						$row_liq = mysqli_fetch_row($res_liq);
						$_POST['medioDePago'] = $row_liq[0];
						if($_POST['medioDePago'] != 2)
						{
							$sqlr_ban = "SELECT cuenta FROM tesobancosctas WHERE ncuentaban = '".$_POST['cb']."'";
							$res_ban = mysqli_query($linkbd_v7, $sqlr_ban);
							$row_ban = mysqli_fetch_row($res_ban);
							$_POST['banco'] = $row_ban[0];
						}
					}
					
				?>
 				<form name="form2" method="post" action=""> 
 					<?php
					 	$sql = "SELECT id_recaudo FROM tesorecaudotransferencia WHERE 1 ORDER BY id_recaudo DESC";
						$res = mysqli_query($linkbd_v7, $sql);
						$row = mysqli_fetch_row($res);
						$_POST['maximo'] = $row[0];
						
 						//***** busca tercero
						if($_POST['bt']=='1')
						{
							$nresul = buscatercero($_POST['tercero']);
							if($nresul != '')
							{
			  					$_POST['ntercero'] = $nresul;
			  				}
							else
							{
			  					$_POST['ntercero'] = "";
			  				}
			 			}
						//******** busca ingreso *****
						//***** busca tercero
						if($_POST['bin'] == '1')
						{
							$nresul = buscaingreso($_POST['codingreso']);
							if($nresul!='')
							{
			  					$_POST['ningreso'] = $nresul;
			  				}
			 				else
			 				{
			  					$_POST['ningreso'] = "";
			  				}
			 			}
 					?>
					<div class="tabsic" style="width:99.6%;">
						<div class="tab">
							<input type="radio" id="tab-1" name="tabgroup1" value="1" <?php echo $check1;?> >
	    					<label for="tab-1">Recaudo Transferencia</label>
	    					<div class="content estilos-scroll" style="overflow-x:hidden;">
						 		<table class="inicio" align="center">
						 			<tr>
									 	<td class="titulos"> Recaudos Transferencias</td>
										<td  class="cerrar" style="width: 100px;"><a href="teso-principal.php">Cerrar</a></td>
									 </tr>
								 </table>
	    						<table class="inicio" align="center">
      								<tr>
										<td class="saludo1" >No Recaudo:</td>
										<td style="width: 10%;">
											<a href="#" onClick="atrasc()">
												<img src="imagenes/back.png" alt="anterior" align="absmiddle">
											</a>
        									<input name="idcomp" id="idcomp" type="text" style="width: 60%; text-align:center;" value="<?php echo $_POST['idcomp']?>" onKeyUp="return tabular(event,this) "  readonly>
        									<input name="ncomp" type="hidden" value="<?php echo $_POST['ncomp']?>">
											<a href="#" onClick="adelante()">
												<img src="imagenes/next.png" alt="siguiente" align="absmiddle">
											</a>
											<input type="hidden" value="a" name="atras" >
											<input type="hidden" value="s" name="siguiente" >
											<input type="hidden" value="<?php echo $_POST['maximo']?>" name="maximo">
        								</td>
										<td class="saludo1">Liquidaci&oacute;n:</td>
										<td>
											<input name="liquidacion" type="text"  value="<?php echo $_POST['liquidacion']?>" onKeyUp="return tabular(event,this) "  readonly>
											<input name="estadoc" type="hidden"  value="<?php echo $_POST['estadoc']?>" onKeyUp="return tabular(event,this) "  readonly>
											<?php 
												if($_POST['estadoc']=="S")
												{
													$valuees="ACTIVO";
													$stylest="width:40%; background-color:#0CD02A; color:white; text-align:center;";
												}
												else if($_POST['estadoc']=="N")
												{
													$valuees="ANULADO";
													$stylest="width:40%; background-color:#FF0000; color:white; text-align:center;";
												}
												else if($_POST['estadoc']=="P")
												{
													$valuees="PAGO";
													$stylest="width:100%; background-color:#0404B4; color:white; text-align:center;";
												}
												echo "<input type='text' name='estado' id='estado' value='$valuees' style='$stylest' readonly />";
											?>
										</td>   
										<td class="saludo1">Fecha:</td>
										<td>
        									<input name="fecha" type="text" id="fc_1198971545" title="DD/MM/YYYY" value="<?php echo $_POST['fecha']; ?>" onKeyUp="return tabular(event,this) " onKeyDown="mascara(this,'/',patron,true)"  maxlength="10" readonly> 
        								</td>
         								<td class="saludo1">Vigencia:</td>
										<td style="width: 10%;">
											<input type="text" id="vigencia" name="vigencia" style="width: 46%;" onKeyPress="javascript:return solonumeros(event)" onKeyUp="return tabular(event,this)"  value="<?php echo $_POST['vigencia']?>" onClick="document.getElementById('tipocta').focus();document.getElementById('tipocta').select();" readonly>
										</td>
									</tr>
									<tr>
										<td class="saludo1">Concepto Recaudo:</td>
										<td colspan="5">
											<textarea name="concepto" id="concepto" style="width:100%; form-sizing: content; max-height: 100px; min-height: 20px; resize: vertical; background-color:#E6F7FF;" class="estilos-scroll" disabled><?php echo $_POST['concepto']?></textarea>
											<input type="hidden" id="concepto" name="concepto" value="<?php echo $_POST['concepto']?>" >
										</td>
										<td class="saludo1" >Ingresa presupuesto? </td>
										<td style="width:14%;">
											<select name="presupuesto" id="presupuesto" onKeyUp="return tabular(event,this)" style="width:100%" disabled>
												<option value="1" <?php if(($_POST['presupuesto']=='1')) echo "SELECTED"; ?>>SI</option>
												<option value="2" <?php if($_POST['presupuesto']=='2') echo "SELECTED"; ?>>NO</option>         
											</select>
										</td>
									</tr>
										
        							<tr>
										<?php
											if($_POST['medioDePago'] != 2)
											{
												?>
         										<td class="saludo1">Recaudado:</td>
												<td style="width:10%">
													<input type="text" name="cb" id="cb" style="width:70%" value="<?php echo $_POST['cb']; ?>" readonly/>&nbsp;
														
													<input name="banco" id="banco" type="hidden" value="<?php echo $_POST['banco']?>" >
													<input type="hidden" id="ter" name="ter" value="<?php echo $_POST['ter']?>" >           
												</td>
												<td colspan="4"> 
													<input type="text" id="nbanco" name="nbanco" value="<?php echo $_POST['nbanco']?>" style="width:100%;" readonly>
												</td>
												<td class="saludo1" >Medio de pago: </td>
												<td>
													<select name="medioDePago" id="medioDePago" onKeyUp="return tabular(event,this)" disabled>
														<?php
															$disable1 = '';
															$disable2 = '';
															if($_POST['medioDePago']=='1')
															{
																$disable1 = 'disabled';
															}
															else
															{
																$disable2 = 'disabled';
															}
														?>
														<option value="1" <?php if(($_POST['medioDePago']=='1')) echo "SELECTED $disable1"; ?>>CSF</option>
														<option value="2" <?php if($_POST['medioDePago']=='2') echo "SELECTED $disable2"; ?>>SSF</option>         
													</select>
												</td>
												<?php
											}
											else
											{
												$regalias = "MEDIO PAGO SSF";
												?>
												<td class="saludo1">Recaudo: </td>
												<td colspan="3"> 
													<input type="text" id="regalias" value="<?php echo $regalias;?>" style="width:100%;" readonly>
												</td>
												<td class="saludo1" >Tipo: </td>
												<td>
													<select name="medioDePago" id="medioDePago" onKeyUp="return tabular(event,this)" style="width:100%">
														<?php
															$disable1 = '';
															$disable2 = '';
															if($_POST['medioDePago']=='1')
															{
																$disable2 = 'disabled';
															}
															else
															{
																$disable1 = 'disabled';
															}
														?>
														<option value="1" <?php if($_POST['medioDePago']=='1') echo "SELECTED"; echo "$disable1";?>>CSF</option>
														<option value="2" <?php if($_POST['medioDePago']=='2') echo "SELECTED "; echo "$disable2";?>>SSF</option>         
													</select>
												</td>
												<td class="saludo1">Medio de Pago:</td>
												<td>
													<select name="mediodepagosgr" id = "mediodepagosgr" style="width:100%;" onKeyUp="return tabular(event,this)" disabled>
														<option value="">No contabiliza </option>
														<?php
															$sqlr = "SELECT id, nombre FROM tesomediodepago WHERE estado = 'S'";
															$res = mysqli_query($linkbd_v7, $sqlr);
															while ($row = mysqli_fetch_row($res)) 
															{
																echo "<option value=$row[0] ";
																$i=$row[0];
																if($i==$_POST['mediodepagosgr'])
																{
																	echo "SELECTED";
																}
																echo ">".$row[0]." - ".$row[1]."</option>";	 	 
															}	 	
														?>
													</select>
												</td>
												<?php
											}
										?>
									</tr>
      								<tr>
										<td class="saludo1">NIT: </td>
										<td>
        									<input name="tercero" type="text" value="<?php echo $_POST['tercero']?>" style="width:100%;" onKeyUp="return tabular(event,this)" readonly>
          								</td>
										<td class="saludo1">Contribuyente:</td>
										<td colspan="3">
											<input type="text" id="ntercero" name="ntercero" value="<?php echo $_POST['ntercero']?>" onKeyUp="return tabular(event,this) " style="width:100%;" readonly>
											<input type="hidden" value="0" name="bt">
											<input type="hidden" id="cb" name="cb" value="<?php echo $_POST['cb']?>" >
											<input type="hidden" id="ct" name="ct" value="<?php echo $_POST['ct']?>" >
											<input type="hidden" value="1" name="oculto">
										</td>
										<td class="saludo1">Centro Costo:</td>
	  									<td>
											<select name="cc" onChange="validar()" onKeyUp="return tabular(event,this)" disabled>
												<?php
													$sqlr = "SELECT id_cc, nombre FROM centrocosto WHERE estado = 'S' AND entidad = 'S'";
													$res = mysqli_query($linkbd_v7, $sqlr);
													while ($row = mysqli_fetch_row($res))
													{
														
														$i=$row[0];
														if($i==$_POST['cc'])
														{
															echo "<option value=$row[0] SELECTED >".$row[0]." - ".$row[1]."</option>";
														} 
														else
														{
															echo "<option value=$row[0] disabled>".$row[0]." - ".$row[1]."</option>";
														}	 
													}
												?>
			   								</select>
	 									</td>
	    							</tr>
    							</table>
	    					</div>
						</div>
						<div class="tab">
							<input type="radio" id="tab-2" name="tabgroup1" value="2" <?php echo $check2;?> >
							<label for="tab-2">Afectacion Presupuestal</label>
							<div class="content" style="overflow-x:hidden;">
								<table class="inicio" style="overflow:scroll">
									<tr><td class="titulos" colspan="3">Detalle Comprobantes</td></tr>
									<tr>
										<td class="titulos2">Cuenta</td>
										<td class="titulos2">Nombre Cuenta</td>
										<td class="titulos2">Valor</td>
									</tr>
									<input type="hidden" id="totaldes" name="totaldes" value="<?php echo $_POST['totaldes']?>" readonly>
						
									<?php
										$totaldes=0;
										$_POST['dcuenta']=array();
										$_POST['ncuenta']=array();
										$_POST['rvalor']=array();

										$iter='saludo1a';
										$iter2='saludo2';
										$cr=0;

										$sqlr = "SELECT cuenta, valor FROM pptoingtranppto WHERE idrecibo = $_POST[idcomp] AND cuenta != ''";
										$resd = mysqli_query($linkbd_v7, $sqlr);
										while($rowd=mysqli_fetch_row($resd))
										{
											$nresult = buscaNombreCuentaCCPET($rowd[0],1);
												echo "<tr class=$iter>
													<td >
														<input name='dcuenta[]' value='$rowd[0]' type='text' size='20' readonly>
													</td>
													<td >
														<input name='ncuenta[]' value='$nresult' type='text' size='55' readonly>
													</td>
													<td >
														<input name='rvalor[]' value='".number_format($rowd[1],2)."' type='text' size='10' readonly>
													</td>
												</tr>";
											$var1 = $rowd[1];
											$cuentavar1 = $cuentavar1+$var1;
											$_POST['varto'] = number_format($cuentavar1,2,".",",");
										}
										echo "<tr class=$iter><td> </td></tr>";
										echo "<tr >
											<td colspan = '2' style='font-size:18px; text-align: center; color:gray !important; ' class='saludo1'>TOTAL:</td>
											<td >
												<input name='varto' id='varto' value='$_POST[varto]' size='10' readonly>
											</td>
										</tr>";
							
									?>
									<input type='hidden' name='contrete' value="<?php echo $_POST['contrete'] ?>" />
								</table>
							</div>
						</div>
					</div> 

					<div class="subpantalla estilos-scroll" style="height:260px; resize: vertical;">
						<table class="inicio">
							<tr>
								<td colspan="4" class="titulos">Detalle Recaudos Transferencia</td>
							</tr>                  
							<tr>
								<td class="titulos2">C&oacute;digo</td>
								<td class="titulos2">Ingreso</td>
								<td class="titulos2">Valor</td>
							</tr>
							<?php
		  						$_POST['totalc']=0;
								for ($x = 0; $x < count($_POST['dcoding']); $x++)
								{
									echo "<tr>
											<td style='width:5%;' class='saludo1' >
												<input name='dcoding[]' class='inpnovisibles' value='".$_POST['dcoding'][$x]."' type='text' style='width:100%;' >
											</td >
											<td style='width:80%;' class='saludo1' >
												<input name='dncoding[]' class='inpnovisibles' value='".$_POST['dncoding'][$x]."' type='text'  style='width:100%;'>
											</td>
											<td style='width:15%;' class='saludo1' >
												<input name='dvalores[]' class='inpnovisibles' value='".$_POST['dvalores'][$x]."' type='text'  style='width:100%;'>
											</td>
										</tr>";
										$_POST['totalc']=$_POST['totalc']+$_POST['dvalores'][$x];
										$_POST['totalcf']=number_format($_POST['totalc'],2);
		 						}
								$resultado = convertir($_POST['totalc']);
								$_POST['letras'] = $resultado." Pesos";
								echo "<tr>
										<td colspan = '2' style='font-size:18px; text-align: center; color:gray !important;' class='saludo1'>TOTAL:</td>
										<td class='saludo1'>
											<input name='totalcf' class='inpnovisibles' type='text' value='$_POST[totalcf]'>
											<input name='totalc' type='hidden' value='$_POST[totalc]'>
										</td>
									</tr>
									<tr>
										<td class='saludo1' style='font-size:14px; text-align: center; color:gray !important;'>SON:</td>
										<td class='saludo1' colspan = '2'>
											<input name='letras' type='text' class='inpnovisibles' value='$_POST[letras]' style='width:100%;' >
										</td>
									</tr>";
							?> 
	   					</table>
					</div>
	  				<?php
					  	
						if($_POST['oculto']=='2')
						{
							preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST['fecha'], $fecha);
							$fechaf = $fecha[3]."-".$fecha[2]."-".$fecha[1];

							$bloq = bloqueos($_SESSION['cedulausu'], $fechaf);

							if($bloq >= 1)
							{
								//*********************CREACION DEL COMPROBANTE CONTABLE ***************************
								$sqlr = "DELETE FROM comprobante_cab WHERE numerotipo = '$_POST[idcomp]' AND tipo_comp = '14'";
								mysqli_query($linkbd_v7, $sqlr);
								
								//***busca el consecutivo del comprobante contable
								$consec=$_POST['idcomp'];
								if ($_POST['estadoc']=='S')
									$esta=1;
								else
									$esta=0;
								//***cabecera comprobante
								$sqlr = "INSERT INTO comprobante_cab (numerotipo, tipo_comp, fecha, concepto, total, total_debito, total_credito, diferencia, estado) VALUES ($consec, 14, '$fechaf', '".strtoupper($_POST['concepto'])."', 0, $_POST[totalc], $_POST[totalc], 0, '$esta')";
								$idcomp = mysqli_insert_id($linkbd_v7);
		
								$sqlr = "DELETE FROM comprobante_det WHERE id_comp = '14 $_POST[idcomp]'";
								mysqli_query($linkbd_v7, $sqlr);
								echo "<input type='hidden' name='ncomp' value='$consec'>";

								$sqlr = "DELETE  FROM tesorecaudotransferencia WHERE id_recaudo = '$consec'";
								mysqli_query($linkbd_v7, $sqlr);

								$sqlr = "DELETE FROM tesorecaudotransferencia_det WHERE id_recaudo = '$consec'";
								mysqli_query($linkbd_v7, $sqlr);

								$sqlr = "DELETE FROM pptoingtranppto WHERE idrecibo = $consec";
								mysqli_query($linkbd_v7, $sqlr);

								//******************* DETALLE DEL COMPROBANTE CONTABLE *********************
								if($_POST['medioDePago'] != 2)
								{
									for($x = 0; $x < count($_POST['dcoding']); $x++)
									{
										//***** BUSQUEDA INGRESO ********
										$sqlri = "SELECT concepto, porcentaje, cuentapres FROM tesoingresos_det WHERE codigo = '".$_POST['dcoding'][$x]."' AND vigencia = $_POST[vigencia]";
										$resi = mysqli_query($linkbd_v7, $sqlri);
										while($rowi = mysqli_fetch_row($resi))
										{
											//**** busqueda concepto contable*****
											$cuentasContables = concepto_cuentasn2($rowi[0], 'C', 4, $_POST['cc'], "$fechaf");
											
											foreach($cuentasContables as $cuentaCont)
											{
												$porce = $rowi[1];
												if($cuentaCont[2] == 'S')
												{
													$valorcred = $_POST['dvalores'][$x]*($porce/100);
													$valordeb = 0;

													$sqlr = "INSERT INTO comprobante_det (id_comp, cuenta, tercero, centrocosto, detalle, cheque, valdebito, valcredito, estado, vigencia) VALUES ('14 ".$_POST['idcomp']."', '".$cuentaCont[0]."', '".$_POST['tercero']."', '".$_POST['cc']."', 'Recaudo Transferencia".strtoupper($_POST['dncoding'][$x])."', '', ".$valordeb.", ".$valorcred.", '$esta', '".$_POST['vigencia']."')";
													mysqli_query($linkbd_v7, $sqlr);

													$valordeb = $_POST['dvalores'][$x]*($porce/100);
													$valorcred = 0;			

													$sqlr = "INSERT INTO comprobante_det (id_comp, cuenta, tercero, centrocosto, detalle, cheque, valdebito, valcredito, estado, vigencia) VALUES ('14 ".$_POST['idcomp']."', '".$_POST['banco']."', '".$_POST['tercero']."', '".$_POST['cc']."', 'Recaudo Transferencia".strtoupper($_POST['dncoding'][$x])."', '', ".$valordeb.", ".$valorcred.", '$esta', '".$_POST['vigencia']."')";

													mysqli_query($linkbd_v7, $sqlr);

													if($_POST['presupuesto'] == "1")
													{
														$sqlr = "INSERT INTO pptoingtranppto (cuenta, idrecibo, valor, vigencia) VALUES ('$rowi[2]', $_POST[idcomp], $valordeb, $_POST[vigencia])";
														mysqli_query($linkbd_v7, $sqlr);
													}
												}
											}

											$sqlr = "INSERT INTO tesorecaudotransferencia_det (id_recaudo, ingreso, valor, estado) VALUES ($_POST[idcomp], '".$_POST['dcoding'][$x]."', '".$_POST['dvalores'][$x]."', '$_POST[estadoc]')";
											mysqli_query($linkbd_v7, $sqlr);

										}
									}
								}
								elseif($_POST['mediodepagosgr']!='')
								{
									for($x = 0; $x < count($_POST['dcoding']); $x++)
									{
										//***** BUSQUEDA INGRESO ********
										$sqlri = "SELECT concepto, porcentaje, cuentapres FROM tesoingresos_det WHERE codigo = '".$_POST['dcoding'][$x]."' AND vigencia = $_POST[vigencia]";
										$resi = mysqli_query($linkbd_v7, $sqlri);
										while($rowi = mysqli_fetch_row($resi))
										{
											//**** busqueda concepto contable*****
											$cuentasContables = concepto_cuentasn2($rowi[0], 'C', 4, $_POST['cc'], "$fechaf");
											
											foreach($cuentasContables as $cuentaCont)
											{
												$porce = $rowi[1];
												if($cuentaCont[2] == 'S')
												{
													$valorcred = $_POST['dvalores'][$x]*($porce/100);
													$valordeb = 0;

													$sqlr = "INSERT INTO comprobante_det (id_comp, cuenta, tercero, centrocosto, detalle, cheque, valdebito, valcredito, estado, vigencia) VALUES ('14 ".$_POST['idcomp']."', '".$cuentaCont[0]."', '".$_POST['tercero']."', '".$_POST['cc']."', 'Recaudo Transferencia".strtoupper($_POST['dncoding'][$x])."', '', ".$valordeb.", ".$valorcred.", '$esta', '".$_POST['vigencia']."')";echo $sqlr;
													mysqli_query($linkbd_v7, $sqlr);

													$valordeb = $_POST['dvalores'][$x]*($porce/100);
													$valorcred = 0;	

													$sqlrMedioPago = "SELECT cuentacontable FROM tesomediodepago WHERE id='$_POST[mediodepagosgr]' AND estado='S'";
													$resMedioPago = mysqli_query($linkbd_v7, $sqlrMedioPago);
													$rowMedioPago = mysqli_fetch_row($resMedioPago);

													$sqlr = "INSERT INTO comprobante_det (id_comp, cuenta, tercero, centrocosto, detalle, cheque, valdebito, valcredito, estado, vigencia) VALUES ('14 ".$_POST['idcomp']."', '".$rowMedioPago[0]."', '".$_POST['tercero']."', '".$_POST['cc']."', 'Recaudo Transferencia".strtoupper($_POST['dncoding'][$x])."', '', ".$valordeb.", ".$valorcred.", '$esta', '".$_POST['vigencia']."')";

													mysqli_query($linkbd_v7, $sqlr);

													if($_POST['presupuesto'] == "1")
													{
														$sqlr = "INSERT INTO pptoingtranppto (cuenta, idrecibo, valor, vigencia) VALUES ('$rowi[2]', $_POST[idcomp], $valordeb, $_POST[vigencia])";
														mysqli_query($linkbd_v7, $sqlr);
													}
												}
											}

											$sqlr = "INSERT INTO tesorecaudotransferencia_det (id_recaudo, ingreso, valor, estado) VALUES ($_POST[idcomp], '".$_POST['dcoding'][$x]."', '".$_POST['dvalores'][$x]."', '$_POST[estadoc]')";
											mysqli_query($linkbd_v7, $sqlr);

										}
									}
								}
								//************ insercion de cabecera recaudos ************
								

								$sqlr = "INSERT INTO tesorecaudotransferencia (id_recaudo, idcomp, fecha, vigencia, banco, ncuentaban, concepto, tercero, cc, valortotal, estado, presupuesto, mediopago) VALUES ($consec, $_POST[liquidacion], '$fechaf', ".$_POST['vigencia'].", '$_POST[ter]', '$_POST[cb]', '".strtoupper($_POST['concepto'])."', '$_POST[tercero]', '$_POST[cc]', '$_POST[totalc]', '$_POST[estadoc]', '$_POST[presupuesto]', '$_POST[medioDePago]')";
								mysqli_query($linkbd_v7, $sqlr);
								
								echo "<table  class='inicio'><tr><td class='saludo1'><center>Se ha actualizado el Recaudo transferencia con Exito <img src='imagenes/confirm.png'></center></td></tr></table>";
							}
							else
							{

								echo "	<script>
											
											despliegamodalm('visible','2',' La fecha del documento es menor a la fecha de bloqueo.');
										</script>";
							}
						}
					?>	

					<div id="bgventanamodal2">
						<div id="ventanamodal2">
							<IFRAME  src="" name="buster" marginWidth=0 marginHeight=0 frameBorder=0 id="ventana2" frameSpacing=0 style="left:500px; width:900px; height:500px; top:200;"> 
							</IFRAME>
						</div>
					</div>	
				</form>
 			</td>
		</tr>
	</body>
</html> 		