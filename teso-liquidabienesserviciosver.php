<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require "comun.inc";
	require "funciones.inc";
	require "conversor.php";
	session_start();
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html lang="es">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1">
		<meta name="viewport" content="user-scalable=no">
		<title>:: IDEAL 10 - Tesorer&iacute;a</title>
		<link href="favicon.ico" rel="shortcut icon"/>
		<link href="css/css2.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
		<link href="css/tabs.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
		<link href="sweetalert2/dist/sweetalert2.min.css" rel="stylesheet"/>
		<script src="sweetalert2/dist/sweetalert2.min.js"></script>
		<script type="text/javascript" src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script type="text/javascript" src="css/calendario.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script>
			function validar() {
				document.form2.submit();
			}
			function buscater(e) {	
				if (document.form2.tercero.value!="") {
					document.form2.bt.value='1';
					document.form2.submit();
				}
			}
			function agregardetalled(){
				if(document.form2.retencion.value!="" &&  document.form2.vporcentaje.value!=""  ){ 
					document.form2.agregadetdes.value=1;
					document.form2.calculaRetencion.value = 1;
					document.form2.submit();
				}else {
					Swal.fire({
						icon: 'error',
						title: 'Error!',
						text: 'Falta informacion para poder Agregar',
						confirmButtonText: 'Continuar',
						confirmButtonColor: '#FF121A',
						timer: 2500
					});
				}
			}
			function eliminard(variable){
				Swal.fire({
					icon: 'question',
					title: '¿Esta Seguro de Eliminar?',
					showDenyButton: true,
					confirmButtonText: 'Guardar',
					confirmButtonColor: '#01CC42',
					denyButtonText: 'Cancelar',
					denyButtonColor: '#FF121A',
				}).then(
					(result) => {
						if (result.isConfirmed){
							document.form2.eliminad.value = variable;
							document.form2.calculaRetencion.value = 1;
							vvend = document.getElementById('eliminad');
							vvend.value = variable;
							document.form2.submit();
						}else if (result.isDenied){
							Swal.fire({
								icon: 'info',
								title: 'No se elimino',
								confirmButtonText: 'Continuar',
								confirmButtonColor: '#FF121A',
								timer: 2500
							});
						}
					}
				)
			}
			function guardar() {
				ingresos2=document.getElementsByName('dcoding[]');
				if (document.form2.fecha.value != '' && ingresos2.length > 0) {
					Swal.fire({
						icon: 'question',
						title: '¿Esta seguro guardar?',
						showDenyButton: true,
						confirmButtonText: 'Guardar',
						confirmButtonColor: '#01CC42',
						denyButtonText: 'Cancelar',
						denyButtonColor: '#FF121A',
					}).then(
						(result) => {
							if (result.isConfirmed){
								document.form2.oculto.value = 2;
								document.form2.submit();
							}else if (result.isDenied){
								Swal.fire({
									icon: 'info',
									title: 'No se guardo',
									confirmButtonText: 'Continuar',
									confirmButtonColor: '#FF121A',
									timer: 2500
								});
							}
						}
					)
				}else {
					Swal.fire({
						icon: 'error',
						title: 'Error!',
						text: 'Faltan datos para completar el registro',
						confirmButtonText: 'Continuar',
						confirmButtonColor: '#FF121A',
						timer: 2500
					});
					document.form2.fecha.focus();
					document.form2.fecha.select();
				}
			}
			function buscater(e){
				if (document.form2.tercero.value!="") {
					document.form2.bt.value='1';
					document.form2.submit();
				}
			}
			function buscaing(e){
				if (document.form2.codingreso.value!="") {
					
					document.form2.bin.value='1';
					document.form2.submit();
				}
			}
			function despliegamodal2(_valor,_tip){
				document.getElementById("bgventanamodal2").style.visibility=_valor;
				if(_valor=="hidden"){
					document.getElementById('ventana2').src="";
				}else {
					if(_tip=='1'){
						document.getElementById('ventana2').src="tercerosgral-ventana01.php?objeto=tercero&nobjeto=ntercero&nfoco=codingreso";
					}else{
						document.getElementById('ventana2').src="bienesservicios-ventana01.php?ti=I&modulo=4";
					}
				}
			}
			function despliegaModalFuentes(_valor) {
				var codigo=document.getElementById('codingreso').value;
				if (codigo != "") {
					document.getElementById("bgventanamodal2").style.visibility=_valor;
					if(_valor=="hidden"){
						document.getElementById('ventana2').src="";
					}else{
						if (codigo != ""){
							document.getElementById('ventana2').src="bienesserviciosfuentes-ventana.php?codigo="+codigo;
						}else{
							Swal.fire({
								icon: 'error',
								title: 'Error!',
								text: 'Seleccione primero el código de ingreso',
								confirmButtonText: 'Continuar',
								confirmButtonColor: '#FF121A',
								timer: 2500
							});
						}
					}
				}else{
					Swal.fire({
						icon: 'error',
						title: 'Error!',
						text: 'Seleccione primero el código de ingreso',
						confirmButtonText: 'Continuar',
						confirmButtonColor: '#FF121A',
						timer: 2500
					});
				}
			}
			function cambiovigencia(){
				let anno = document.getElementById('fc_1198971545').value.split("/");
				document.getElementById('vigencia').value = anno[2];
			}
			function printPDF(){
				console.log();
				document.form2.action="teso-liquidabienesserviciosverPdf.php";
				document.form2.target="_BLANK";
				document.form2.submit(); 
				document.form2.action="";
				document.form2.target="";
				//teso-liquidabienesserviciosverPdf.php?idrecaudo=
			}
		</script>
	</head>
	<body>
		<IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
		<span id="todastablas2"></span>
		<table>
			<tr><script>barra_imagenes("teso");</script><?php cuadro_titulos();?></tr>
			<tr><?php menu_desplegable("teso");?></tr>
			<tr>
				<td colspan="3" class="cinta">
					<img src="imagenes/add.png" title="Nuevo" onClick="location.href='teso-liquidabienesservicios.php'" class="mgbt"/>
					<img src="imagenes/guarda.png" title="Guardar"  onClick="guardar()" class="mgbt"/>
					<img src="imagenes/busca.png" title="Buscar" onClick="location.href='teso-liquidabienesserviciosbuscar.php'" class="mgbt"/>
					<img src="imagenes/agenda1.png" title="Agenda" onClick="mypop=window.open('plan-agenda.php','','');mypop.focus()" class="mgbt"/>
					<img src="imagenes/nv.png" title="Nueva ventana" onClick="mypop=window.open('teso-principal.php','','');mypop.focus();" class="mgbt"/>
					<img src="imagenes/duplicar_pantalla.png" title="Duplicar pesta&ntilde;a" onClick="mypop=window.open('<?php echo $url2; ?>','','');mypop.focus();" class="tooltip bottom mgbt"/>
					<a onClick="printPDF()" class="mgbt"><img src="imagenes/print.png" title="Imprimir" /></a>
					<!--<a onClick="history.go(-1); return false;" class="mgbt"><img src="imagenes/iratras.png" title="Atr&aacute;s"></a>-->
				</td>
			</tr>		  
		</table>
		<div id="bgventanamodalm" class="bgventanamodalm">
			<div id="ventanamodalm" class="ventanamodalm">
				<IFRAME src="" name="ventanam" marginWidth=0 marginHeight=0 frameBorder=0 id="ventanam" frameSpacing=0 style=" width:700px; height:130px; top:200; overflow:hidden;"> 
				</IFRAME>
			</div>
		</div>
		<?php
			if($_POST['oculto'] == "") {
				$_POST['vigencia'] = date('Y');
				$_POST['dcoding'] = array(); 		 
				$_POST['dncoding'] = array(); 		 
				$_POST['dfuente'] = array();
				$_POST['dvalores'] = array();
				$check1="checked";
				$fec = date("d/m/Y");

				$sqlr = "SELECT cuentacaja FROM tesoparametros";
				$res = mysqli_query($linkbd,$sqlr);
				while ($row = mysqli_fetch_row($res)) {
					$_POST['cuentacaja'] = $row[0];
				}
				$consec = selconsecutivo('tesobienesservicios','id_recaudo');
				$_POST['idcomp'] = $consec;	
				$_POST['fecha'] = date("d/m/Y"); 		 		  			 
				$_POST['valor'] = 0;
				
				$sqlrt = "SELECT id_retencion, porcentaje, valor FROM tesobienesservicios_retenciones WHERE id_orden = '".$_GET['idrecaudo']."'";
					$resrt = mysqli_query($linkbd,$sqlrt);
					while ($rowrt = mysqli_fetch_row($resrt)){
						$codrete = '';
						if(strlen($rowrt[0])>1){
							$codrete = $rowrt[0];
						}else{
							$codrete = '0'.$rowrt[0];
						}
						$_POST['ddescuentos'][] = $codrete;
						$_POST['dporcentajes'][] = $rowrt[1];
						$_POST['ddesvalores'][] = $rowrt[2];
						$_POST['ddestipo'][] = 'L';
						$sqlnr = "SELECT nombre FROM tesoretenciones WHERE codigo = '$codrete'";
						$resnr = mysqli_query($linkbd, $sqlnr);
						$rownr = mysqli_fetch_row($resnr);
						$_POST['dndescuentos'][] = $rownr[0];
					}
			}

			if($_POST['tabgroup1']){
				switch($_POST['tabgroup1'])
				{
					case 1:	$check1 = 'checked';break;
					case 2: $check2 = 'checked';break;
				}
			}else{
				$check1="checked";
			}
		?>
		<form name="form2" method="post" action=""> 
			<?php
				
				$idrecaudo= $_GET['idrecaudo'];
				$sqlrg= "SELECT * FROM tesobienesservicios WHERE id_recaudo = '$idrecaudo'";
				$resg =mysqli_query($linkbd, $sqlrg);
				$rowg = mysqli_fetch_row($resg);


				$sqlrdet = "SELECT * FROM tesobienesservicios_det WHERE id_recaudo = '$idrecaudo'";
				$resdet =mysqli_query($linkbd, $sqlrdet);
				$rowdet = mysqli_fetch_row($resdet);
				

				$sql = "SELECT * FROM tesoingresos_ventas WHERE codigo = '$rowdet[2]' AND estado='S' ORDER BY codigo";
				$row = mysqli_fetch_row(mysqli_query($linkbd, $sql));

				if($row[0] != '') {
					$ningreso = $row[1];
					if ($row[6] == 'N') {
						$causacion = 2;
					}else {
						$causacion = 1;
					}
				}

				$nresul = buscatercero($rowg[4]);
					if($nresul != '') {
						$_POST['ntercero'] = $nresul;
					}else{
						$_POST['ntercero'] = "";
					}

				$sqlrncod = "SELECT * FROM ccpet_fuentes_cuipo WHERE codigo_fuente = '$rowdet[6]'";
				$resncod = mysqli_query($linkbd, $sqlrncod);
				$rowncod = mysqli_fetch_row($resncod);
				

			?>
			<div class="tabs" style="height:75%; width:99.7%">
				<div class="tab" >
					<input type="radio" id="tab-1" name="tabgroup1" value="1" <?php echo $check1;?> >
					<label id="clabel" for="tab-1">Orden de pago</label>
					<div class="content" style="overflow:hidden;">
						<table class="inicio ancho">
							<tr>
								<td class="titulos" colspan="2">Liquidar Bienes y Servicios</td>
								<td class="cerrar" style="width:7%" onClick="location.href='teso-principal.php'">Cerrar</td>
							</tr>
							<tr>
								<td>
									<table>
										<tr>
											<td style="width:12%;" class="saludo1" >Numero Liquidación:</td>
											<td  style="width:7%;" >
												<input type="text" name="idcomp" value="<?php echo $_GET['idrecaudo']?>" onKeyUp="return tabular(event,this) " readonly>
											</td>
											<td style="width:5%;"  class="saludo1">Fecha:</td>
											<td  style="width:10%;">
												<input type="text" name="fecha" id="fc_1198971545" title="DD/MM/YYYY"  value="<?php echo $rowg[2]; ?>" style="width:100%;" onKeyUp="return tabular(event,this) " maxlength="10"  class="colordobleclik" autocomplete="off" " readonly> 
											</td>
											<td style="width:5%;" class="saludo1">Vigencia:</td>
											<td style="width:10%;">
												<input type="text" id="vigencia" name="vigencia"  onKeyPress="javascript:return solonumeros(event)" onKeyUp="return tabular(event,this)" style="width:100%;" value="<?php echo $rowg[3]?>" onClick="document.getElementById('tipocta').focus();document.getElementById('tipocta').select();"  readonly>
											</td>
											<td style="width:10%;" class="saludo1">Causacion Contable:</td>
											<td style="width:0%;">
												<?php
													if ($causacion == '1'){
														$causacionn = "Si";
													}
													else {
														$causacionn = "No";
													}
												?>
												<input type="text" name="causacion" value="<?php echo $causacionn?>" onKeyUp="return tabular(event,this) " readonly>     
											</td> 	
										<tr>
										<tr>
											<td class="saludo1">Concepto Liquidacion:</td>
											<td colspan="9" >
												<input type="text" name="concepto" value="<?php echo $rowg[6]?>" style="width:100%;"  onKeyUp="return tabular(event,this)" readonly>
											</td>
										</tr>  
										<tr>
											<td class="saludo1">Contribuyente: </td>
											<td>
												<input type="text" name="tercero" value="<?php echo $rowg[4]?>" style="width:100%;"  onKeyUp="return tabular(event,this)"  class="colordobleclik" autocomplete="off" readonly> 
											</td>
											<td colspan="8">
												<input type="text" id="ntercero" name="ntercero" value="<?php echo $_POST['ntercero']?>" style="width:100%;" onKeyUp="return tabular(event,this)" readonly>
											</td>
										</tr>
										
									</table>
								</td>
							</tr>
						</table>
						<?php
							//***** busca tercero
							if($_POST['bt'] == '1') {
								$nresul = buscatercero($_POST['tercero']);
								if($nresul != '') {
									$_POST['ntercero'] = $nresul;
									echo"
									<script>
										document.getElementById('codingreso').focus();
										document.getElementById('codingreso').select();
									</script>";
								}
								else {
									$_POST['ntercero'] = "";
									echo"
									<script>
										Swal.fire({
											icon: 'error',
											title: 'Error!',
											text: 'Tercero Incorrecto o no Existe',
											confirmButtonText: 'Continuar',
											confirmButtonColor: '#FF121A',
											timer: 2500
										});
										document.form2.tercero.focus();	
									</script>";
								}
							}
							//*** ingreso
						?>
						<div class="subpantallac7" style="height:45%; width:99.5%; overflow-x:hidden;">
							<table class="inicio">
								<tr>
									<td colspan="7" class="titulos">Detalle Liquidacion Recaudos</td>
								</tr>                  
								<tr class="titulos2">
								<td style="width: 5%;">C&oacute;digo</td>
									<td>Ingreso</td>
									<td style="width: 10%;">Centro Costo</td>
									<td style="width: 15%;">Fuente</td>
									<td style="width: 15%;">Valor</td>
									<td style="width: 15%;">Iva</td>
									<td style="width: 15%;">Total</td>
								</tr>
								<?php 	
									$_POST['totalc'] = 0;
									$_POST['detalle'] = [];
									$iter='saludo1a';
									$iter2='saludo2';

									$sqlrdet = "SELECT * FROM tesobienesservicios_det WHERE id_recaudo = '$idrecaudo'";
									$resdet =mysqli_query($linkbd, $sqlrdet);
									while ($rowdet = mysqli_fetch_array($resdet)) {
										$idrec = $rowdet[2];
										$ni= $ningreso;
										$cc = $rowdet[5].' - '.$row[1];
										$fuente = $rowdet[6];									
										for ($x=0; $x<count($fuente);$x++){		 	
											$valor = $rowdet[3];
											$valiva = $rowdet[7];
											$totalgr = $valor + $valiva;
											$total = $total + $totalgr;
											$sqlr="SELECT * FROM centrocosto WHERE estado='S' and id_cc = '$rowdet[5]'";
											$res = mysqli_query($linkbd,$sqlr); 
											$row = mysqli_fetch_row($res);

											echo '
											<input type="hidden" name="detalle['.$x.'][codigo]" value="'.$idrec.'">
											<input type="hidden" name="detalle['.$x.'][ingreso]" value="'.$ni.'">
											<input type="hidden" name="detalle['.$x.'][centro_costo]" value="'.$cc.'">
											<input type="hidden" name="detalle['.$x.'][fuente]" value="'.$fuente.'">
											<input type="hidden" name="detalle['.$x.'][valor]" value="'.$valor.'">
											<input type="hidden" name="detalle['.$x.'][valiva]" value="'.$valiva.'">
											<input type="hidden" name="detalle['.$x.'][totalgr]" value="'.$totalgr.'">
											<tr class='.$iter.'>
												<td>'.$idrec.'</td>
												<td>'.$ni.'</td>
												<td>'.$cc.'</td>
												<td>'.$fuente.'</td>
												<td style="text-align:right;">$'.number_format($valor,2,".",",").'</td>
												<td style="text-align:right;">$'.number_format($valiva,2,".",",").'</td>
												<td style="text-align:right;">$'.number_format($totalgr,2,".",",").'</td>
											</tr>';

											$aux = $iter;
											$iter = $iter2;
											$iter2 = $aux;	 
                                            $_POST['totalc'] = $_POST['totalc'] + $totalgr;
										}
									}
									$resultado = convertir($_POST['totalc']);
									$totalf = number_format($total,2,".",",");
									$letras = $resultado." Pesos";
									echo "
										<input type='hidden' name='totalcf' value='$total' readonly >
										<input type='hidden' name='totalc' value='$_POST[totalc]'>
										
										<tr>
											<td class='saludo2' colspan='6'  style='text-align:right;'>Total</td>
											
											<td class='saludo1' style='text-align:right;'>$$totalf</td>
										</tr>
										<tr>
											<td class='saludo1'>Son:</td>
											<td colspan='5'>
												<input name='letras' type='text' value='$letras' readonly>
											</td>
										</tr>";
								?> 
							</table>
						</div>
					</div>
				</div>
				<div class="tab">
					<input type="radio" id="tab-2" name="tabgroup1" value="2" <?php echo $check2;?> >
					<label id="clabel" for="tab-2">Retenciones</label>
					<div class="content" style="overflow-x:hidden;"> 
						<?php
							if(!$_POST['calculaRetencion']){
								$_POST['calculaRetencion'] = 0;
							}
						?>
						<table class="inicio ancho">
							<tr>
								<td class="titulos" colspan="10">Retenciones</td>
								<td class="cerrar" style="width:7%" onClick="location.href='teso-principal.php'">Cerrar</td>
							</tr>
							<tr>
								<td class="saludo1" style="width:12%;">Retencion y Descuento:</td>
								<td style="width:15%;">
									<select name="retencion" onChange="validar()" onKeyUp="return tabular(event,this)">
										<option value="">Seleccione ...</option>
										<?php
											$sqlr="SELECT * FROM tesoretenciones WHERE estado='S'";
											$res = mysqli_query($linkbd, $sqlr);
											while ($row = mysqli_fetch_row($res)){
												if("$row[0]" == $_POST['retencion']){
													echo "<option value='$row[0]' SELECTED>$row[1] - $row[2]</option>";
													$_POST['nretencion'] = $row[1]." - ".$row[2];
													if($_POST['porcentaje'] == ''){
														$_POST['porcentaje'] = $row[5];
													}
												}else {
													echo "<option value='$row[0]'>$row[1] - $row[2]</option>";
												} 	 
											}
										?>
									</select>
									<input type="hidden" value="<?php echo $_POST['nretencion']?>" name="nretencion">
								</td>
								<input type="hidden" id="contador" name="contador"  value="<?php echo $_POST['contador']; ?>" >
								<input type="hidden" id="oculto12" name="oculto12"  value="<?php echo $_POST['oculto12']; ?>">
								<input type="hidden" name="estado"  value="<?php echo $_POST['estado']; ?>" >
								<td style="width:8%;" class="saludo1">Porcentaje:</td>
								<td style="width:6%;"><input type="text" d="porcentaje" name="porcentaje" value="<?php echo $_POST['porcentaje']?>"></td>
								<td class="saludo1" style="width:4%;">Valor:</td>
								<td style="width:10%;">
									<input type="text" id="vporcentaje" name="vporcentaje" value="<?php echo $_POST['vporcentaje']?>" >
									<input type="hidden" name="calculaRetencion" id="calculaRetencion" value="<?php echo $_POST['calculaRetencion'] ?>">
								</td>
								<td ><input type='hidden' id="totaldes" name="totaldes" value="<?php echo $_POST['totaldes']?>" readonly></td>
								<td colspan="2" style="padding-bottom:0px"><em class="botonflechaverde" onClick="agregardetalled();">Agregar</em></td>
							</tr>
							<input type="hidden" name="agregadetdes" value="0" >
							<?php
								if($_POST['calculaRetencion'] == 0){
									//$_POST['ddescuentos'] = [];
									//$_POST['dndescuentos'] = [];
									//$_POST['dporcentajes'] = [];
									//$_POST['ddesvalores'] = [];
									
									$valorRetencion = 0;
									$sqlrRetencion = "SELECT idretencion, porcentaje FROM ccpetdc_retenciones WHERE iddestino = '$_POST[idDestinoCompra]'";
									$resRetencion = mysqli_query($linkbd, $sqlrRetencion);
									while($rowRetencion = mysqli_fetch_row($resRetencion)){
										$sqlr = "SELECT iva FROM tesoretenciones WHERE id = '$rowRetencion[0]' AND estado = 'S'";
										$res = mysqli_query($linkbd, $sqlr);
										$row = mysqli_fetch_row($res);
										if($row[0] == 1){
											$valorRetencion = round((doubleVal($_POST['iva']) * doubleVal($rowRetencion[1])) / 100);
										}
										else{
											$valorRetencion = round((doubleVal($_POST['base']) * doubleVal($rowRetencion[1])) / 100);
										}

										
										$_POST['ddescuentos'][] = $rowRetencion[0];
										$_POST['dndescuentos'][] = buscaRetencionConCodigo($rowRetencion[0]);
										$_POST['dporcentajes'][] = $rowRetencion[1];
										$_POST['ddesvalores'][] = $valorRetencion;
										
									}
								}
								$_POST['valoregreso'] = $total;
								$_POST['valorretencion'] = $_POST['totaldes'];
								$_POST['valorcheque'] = doubleVal($_POST['valoregreso'])-doubleVal($_POST['valorretencion']);
								if ($_POST['eliminad'] != ''){ 
									$posi = $_POST['eliminad'];
									unset($_POST['ddescuentos'][$posi]);
									unset($_POST['dndescuentos'][$posi]);
									unset($_POST['dporcentajes'][$posi]);
									unset($_POST['ddesvalores'][$posi]);
									$_POST['ddescuentos'] = array_values($_POST['ddescuentos']); 
									$_POST['dndescuentos'] = array_values($_POST['dndescuentos']); 
									$_POST['dporcentajes'] = array_values($_POST['dporcentajes']); 
									$_POST['ddesvalores'] = array_values($_POST['ddesvalores']); 		 	 
								}	 
								if ($_POST['agregadetdes'] == '1'){
									$_POST['ddescuentos'][] = $_POST['retencion'];
									$_POST['dndescuentos'][] = $_POST['nretencion'];
									$_POST['dporcentajes'][] = $_POST['porcentaje'];
									$_POST['ddesvalores'][] = $_POST['vporcentaje'];
									$_POST['agregadetdes'] = '0';
									echo"
									<script>
										document.form2.porcentaje.value = '';
										document.form2.vporcentaje.value = 0;	
										document.form2.retencion.value = '';	
									</script>";
								}
							?>
						</table>
						<table class="inicio" style="overflow:scroll">
							<tr>
								<td class="titulos">Descuento</td>
								<td class="titulos">%</td>
								<td class="titulos">Valor</td>
								<td class="titulos2"><img src="imagenes/del.png" ><input type='hidden' name='eliminad' id='eliminad'></td>
							</tr>
							<?php
								$totaldes = 0;
								$iter = 'saludo1a';
								$iter2 = 'saludo2';
								$sqlrd = "SELECT * FROM tesobienesservicios_retenciones WHERE id_orden = '$_GET[idrecaudo]'";
								$result = mysqli_query($linkbd, $sqlrd);
								$row = mysqli_fetch_row($result);

								if(isset($_POST['ddescuentos'])){
									for ($x = 0; $x<count($_POST['ddescuentos']); $x++){
										echo "
										<input type='hidden' name='dndescuentos[]' value='".$_POST['dndescuentos'][$x]."'>
										<input type='hidden' name='ddescuentos[]' value='".$_POST['ddescuentos'][$x]."' >
										<input type='hidden' name='dporcentajes[]' value='".$_POST['dporcentajes'][$x]."'>
										<input type='hidden' name='ddesvalores[]' value='".$_POST['ddesvalores'][$x]."'>
										<tr class='$iter'>
											<td>".$_POST['dndescuentos'][$x]."</td>
											<td>".$_POST['dporcentajes'][$x]."</td>
											<td>".$_POST['ddesvalores'][$x]."</td>
											<td><a href='#' onclick='eliminard($x)'><img src='imagenes/del.png'></a></td>
										</tr>";
										$totaldes = $totaldes+($_POST['ddesvalores'][$x]);
										$aux = $iter;
										$iter = $iter2;
										$iter2 = $aux;
									}
								}
								if(isset($totaldes)){
									$_POST['valorretencion'] = $totaldes;
									echo"
									<script>
										document.form2.totaldes.value='$totaldes';		
										document.form2.valorretencion.value='$totaldes';
									</script>";
									echo "<tr>
										<td colspan = '2' style='font-size:18px; text-align: center; color:gray !important; ' class='saludo1'>TOTAL:</td>
										<td class='saludo1'>
											$totaldes
										</td>
										<td></td>
									</tr>";	
								}
								$_POST['valorcheque'] = doubleVal($_POST['valoregreso'])-doubleVal($_POST['valorretencion']);
							?>
                            <input type="hidden" name="valorcheque" value="<?php echo $_POST['valorcheque'] ?>">
						</table>
					</div>
				</div>
			</div>
			<?php
				if($_POST['oculto']=='2') {
					preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST['fecha'],$fecha);
					$fechaf=$fecha[3]."-".$fecha[2]."-".$fecha[1];
					$bloq=bloqueos($_SESSION['cedulausu'],$fechaf);	
					if($bloq>=1) {
						$consec = selconsecutivo('tesobienesservicios','id_recaudo');
						if($_POST['causacion'] == '2') {
							$_POST['concepto'] = "ESTE DOCUMENTO NO REQUIERE CAUSACION CONTABLE - ".$_POST['concepto'];
						}
						$sqlr="INSERT INTO comprobante_cab (numerotipo, tipo_comp, fecha, concepto, total, total_debito, total_credito, diferencia, estado) VALUES ($consec, 41, '$fechaf', '".strtoupper($_POST['concepto'])."', 0, $_POST[totalc], $_POST[totalc], 0, '1')";
						mysqli_query($linkbd,$sqlr);
						
						//******************* DETALLE DEL COMPROBANTE CONTABLE *********************
						if($_POST['causacion']!='2'){
							for($x=0;$x<count($_POST['dcoding']);$x++){
								$cuentaiva = '';
								$sql1 = "SELECT * FROM conceptoscontables_det WHERE modulo = '4' AND tipo = 'C' AND fechainicial < '$fechaf' AND cc='".$_POST['dncc'][$x]."' AND codigo = '".$_POST['dvconcepto'][$x]."'AND cuenta != '' ORDER BY fechainicial asc";
								$res1 = mysqli_query($linkbd,$sql1);
								while($row1 = mysqli_fetch_row($res1)){
									if($row1[6] == 'S'){				 
										$valordeb = $_POST['dvalores'][$x];
										$valorcred = 0;
										$cuentaiva = $row1[4];	
									}else{
										$valorcred = $_POST['dvalores'][$x];
										$valordeb = 0;
									}
									$sqlv="INSERT INTO comprobante_det (id_comp,cuenta, tercero, centrocosto, detalle, cheque, valdebito, valcredito, estado, vigencia) VALUES ('41 $consec', '".$row1[4]."', '".$_POST['tercero']."', '".$row1[5]."', 'Causacion ".strtoupper($_POST['dncoding'][$x])."', '', ".$valordeb.", ".$valorcred.", '1', '".$_POST['vigencia']."')";
									mysqli_query($linkbd,$sqlv);
								}
								//IVA
								$sqliva = "SELECT porcentaje FROM conceptoscontables WHERE modulo = '4' AND tipo = 'IV' AND codigo = '".$_POST['dconciva'][$x]."'";
								$resiva = mysqli_query($linkbd,$sqliva);
								$rowiva = mysqli_fetch_row($resiva);
								$valporiva = $rowiva[0];
								$valtotaliva = floatval($_POST['dvalores'][$x]) * (floatval($valporiva) / 100);
								if($valtotaliva > 0){
									$sql2 = "SELECT  * FROM conceptoscontables_det WHERE credito = 'S' AND modulo = '4' AND tipo = 'IV' AND fechainicial < '$fechaf' AND cc = '".$_POST['dncc'][$x]."' AND codigo = '".$_POST['dconciva'][$x]."'AND cuenta != '' ORDER BY fechainicial asc";
									$res2 = mysqli_query($linkbd,$sql2);
									$row2 = mysqli_fetch_row($res2);

									$sqlv="INSERT INTO comprobante_det (id_comp, cuenta, tercero, centrocosto, detalle, cheque, valdebito, valcredito, estado, vigencia) VALUES ('41 $consec', '".$row2[4]."', '".$_POST['tercero']."', '".$row2[5]."', 'IVA ".$_POST['dconciva'][$x]." ".strtoupper($_POST['dncoding'][$x])."', '', 0, $valtotaliva, '1', '".$_POST['vigencia']."')";
									mysqli_query($linkbd,$sqlv);

									$sqlv="INSERT INTO comprobante_det (id_comp, cuenta, tercero, centrocosto, detalle, cheque, valdebito, valcredito, estado, vigencia) VALUES ('41 $consec', '$cuentaiva', '".$_POST['tercero']."', '".$row2[5]."', 'IVA ".$_POST['dconciva'][$x]." ".strtoupper($_POST['dncoding'][$x])."', '', $valtotaliva, 0, '1', '".$_POST['vigencia']."')";
									mysqli_query($linkbd,$sqlv);
								}
								//Retenciones
								for($xy=0;$xy<count($_POST['ddescuentos']);$xy++){
									$codrete = substr($_POST['dndescuentos'][$xy], 0, 2);
									$sql3 = "SELECT * FROM conceptoscontables_det WHERE debito = 'S' AND modulo = '4' AND tipo = 'RI' AND fechainicial < '$fechaf' AND cc = '".$_POST['dncc'][$x]."' AND codigo = '$codrete' AND cuenta != ''";
									$res3 = mysqli_query($linkbd,$sql3);
									$row3 = mysqli_fetch_row($res3);

									$sqlr="INSERT INTO comprobante_det (id_comp, cuenta, tercero, centrocosto, detalle, cheque, valdebito, valcredito, estado, vigencia) VALUES ('41 $consec', '".$row3[4]."', '".$_POST['tercero']."', '".$row3[5]."', 'Retención $codrete ".strtoupper($_POST['dncoding'][$x])."', '', ".$_POST['ddesvalores'][$xy].", 0, '1', '".$_POST['vigencia']."')";
									mysqli_query($linkbd,$sqlr);

									$sqlr="INSERT INTO comprobante_det (id_comp, cuenta, tercero, centrocosto, detalle, cheque, valdebito, valcredito, estado, vigencia) VALUES ('41 $consec', '$cuentaiva', '".$_POST['tercero']."', '".$row3[5]."', 'Retención $codrete ".strtoupper($_POST['dncoding'][$x])."', '', 0, ".$_POST['ddesvalores'][$xy].", '1', '".$_POST['vigencia']."')";
									mysqli_query($linkbd,$sqlr);
								}
							}	
						}
						//************ insercion de cabecera recaudos ************
						$sqlr="INSERT INTO tesobienesservicios (id_recaudo, id_comp, fecha, vigencia, tercero, valortotal, concepto, estado, cc, tipo_causacion) VALUES ($consec, $consec, '$fechaf', ".$_POST['vigencia'].", '$_POST[tercero]', '$_POST[totalc]', '".strtoupper($_POST['concepto'])."', 'S', '".$_POST['cc']."','".$_POST['causacion']."')";	  
						mysqli_query($linkbd,$sqlr);
						//$idrec=mysqli_insert_id($linkbd); 
						//************** insercion de consignaciones **************
						for($x=0;$x<count($_POST['dcoding']);$x++){
							$sqlr = "INSERT INTO tesobienesservicios_det (id_recaudo, ingreso, valor, estado, cc, fuente) VALUES ($consec, '".$_POST['dcoding'][$x]."', ".$_POST['dvalores'][$x].", 'S', '".$_POST['dncc'][$x]."', '".$_POST['dfuente'][$x]."')";	  
							if (!mysqli_query($linkbd,$sqlr)){
								echo "<table class='inicio'><tr><td class='saludo1'><center><font color=blue><img src='imagenes/alert.png'> Manejador de Errores de la Clase BD<br><font ></font></font><br><p align=center>No se pudo ejecutar la petición: <br><font color=red><b>$sqlr</b></font></p>";
								echo "Ocurrió el siguiente problema:<br>";
								echo "<pre>";
								echo "</pre></center></td></tr></table>";
							}else{
								echo "
								<script>
									Swal.fire({
										icon: 'success',
										title: 'Se ha almacenado el Recaudo con Exito',
										confirmButtonText: 'Continuar',
										confirmButtonColor: '#FF121A',
										timer: 3000
									})
								</script>";
							}
						}
						for($x=0;$x<count($_POST['ddescuentos']);$x++){
							$sqlr = "INSERT INTO tesobienesservicios_retenciones (id_retencion, id_orden, porcentaje, valor, estado, causacion, pasa_ppto) VALUES ('".$_POST['ddescuentos'][$x]."', $consec, '".$_POST['dporcentajes'][$x]."', ".$_POST['ddesvalores'][$x].", 'S', '$causacion', '$estado')";
							mysqli_query($linkbd, $sqlr);
						}	
					}else{
						echo "
						<script>
							Swal.fire({
								icon: 'error',
								title: 'Error!',
								text: 'No Tiene los Permisos para Modificar este Documento',
								confirmButtonText: 'Continuar',
								confirmButtonColor: '#FF121A',
								timer: 2500
							});
						</script>";
					} 
				}
			?>	
			<div id="bgventanamodal2">
				<div id="ventanamodal2">
					<IFRAME  src="" name="buster" marginWidth=0 marginHeight=0 frameBorder=0 id="ventana2" frameSpacing=0 style="left:500px; width:900px; height:500px; top:200;"> 
					</IFRAME>
				</div>
			</div>
		</form>
	</body>
</html> 		