<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require "comun.inc";
	require "funciones.inc";
	session_start();
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
	cargarcodigopag($_GET[codpag],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html lang="es">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=9">
		<title>:: IDEAL 10 - Gesti&oacute;n humana</title>
		<link href="css/css2.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css">
		<script type="text/javascript" src="css/programas.js"></script>
		<style>
			input[type='text']{height:30px;}
			input[type='search']{height:30px ;}
			select{height:30px;}
		</style>
		<script>
			function validar(){document.form2.submit();}
			function guardar()
			{
				var validacion01=document.getElementById('nombre').value;
				if (document.form2.codigo.value != '' && validacion01.trim() != '')
 				{despliegamodalm('visible','4','Esta seguro de guardar','2')}
 				else{despliegamodalm('visible','2','Faltan datos para completar el registro');}
			}
			function despliegamodal2(_valor)
			{
				document.getElementById("bgventanamodal2").style.visibility = _valor;
				if(_valor == "hidden"){document.getElementById('ventana2').src = "";}
				else 
				{document.getElementById('ventana2').src = "scuentasppto-ventana01.php?ti=2";}
			}
			function despliegamodalm(_valor,_tip,mensa,pregunta)
			{
				document.getElementById("bgventanamodalm").style.visibility = _valor;
				if(_valor == "hidden"){document.getElementById('ventanam').src = "";}
				else
				{
					switch(_tip)
					{
						case "1":	document.getElementById('ventanam').src = "ventana-mensaje1.php?titulos=" + mensa;break;
						case "2":	document.getElementById('ventanam').src = "ventana-mensaje3.php?titulos=" + mensa;break;
						case "3":	document.getElementById('ventanam').src = "ventana-mensaje2.php?titulos=" + mensa;break;
						case "4":	document.getElementById('ventanam').src = "ventana-consulta1.php?titulos=" + mensa + "&idresp=" + pregunta;break;
					}
				}
			}
			function funcionmensaje(){document.location.href = "";}
			function respuestaconsulta(pregunta)
			{
				switch(pregunta)
				{
					case "1":	document.getElementById('oculto').value = "6";
								document.form2.submit();break;
					case "2":	document.form2.oculto.value=2;
								document.form2.submit();break;
				}
			}
			function cambiocheck(id)
			{
				switch(id)
				{
					case "1":
						if(document.getElementById('idswparafiscal').value == 'S')
						{document.getElementById('idswparafiscal').value = 'N';}
						else {document.getElementById('idswparafiscal').value = 'S';}
						break;
					case "2":
						if(document.getElementById('idswprovision').value == 'S')
						{document.getElementById('idswprovision').value = 'N';}
						else{document.getElementById('idswprovision').value = 'S';}
						break;
					case "3":
						if(document.getElementById('idswsalud').value == 'S')
						{document.getElementById('idswsalud').value = 'N';}
						else{document.getElementById('idswsalud').value = 'S';}
						break;
					case "4":	
						if(document.getElementById('idswpension').value == 'S')
						{document.getElementById('idswpension').value = 'N';}
						else{document.getElementById('idswpension').value = 'S';}
						break;
					case "5":	
						if(document.getElementById('idswarl').value == 'S')
						{document.getElementById('idswarl').value = 'N';}
						else{document.getElementById('idswarl').value = 'S';}
						break;
				}
				document.form2.submit();
			}
		</script>
		<?php titlepag();?>
	</head>
	<body>
		<IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
		<span id="todastablas2"></span>
		<table>
			<tr><script>barra_imagenes("hum");</script><?php cuadro_titulos();?></tr>
			<tr><?php menu_desplegable("hum");?></tr>
			<tr>
				<td colspan="3" class="cinta"><img src="imagenes/add.png" title="Nuevo" onClick="location.href='hum-variablesNominaCCPET.php'" class="mgbt"><img src="imagenes/guarda.png" title="Guardar" onClick="guardar();" class="mgbt"><img src="imagenes/busca.png"  title="Buscar" onClick="location.href='hum-buscaVariablesNominaCCPET.php'" class="mgbt"><img src="imagenes/agenda1.png" title="Agenda" onClick="mypop=window.open('plan-agenda.php','','');mypop.focus()" class="mgbt"><img src="imagenes/nv.png" title="Nueva ventana" onClick="mypop=window.open('hum-principal.php','','');mypop.focus();" class="mgbt"><img src="imagenes/duplicar_pantalla.png" title="Duplicar pesta&ntilde;a" onClick="mypop=window.open('<?php echo $url2; ?>','','');mypop.focus();" class="tooltip bottom mgbt"><img src="imagenes/iratras.png" title="Atr&aacute;s" onClick="location.href='hum-buscaVariablesNominaCCPET.php'" class="mgbt"></td>
			</tr>
		</table>
		<div id="bgventanamodalm" class="bgventanamodalm">
			<div id="ventanamodalm" class="ventanamodalm">
				<IFRAME src="" name="ventanam" marginWidth=0 marginHeight=0 frameBorder=0 id="ventanam" frameSpacing=0 style=" width:700px; height:130px; top:200; overflow:hidden;"> 
				</IFRAME>
			</div>
		</div>
		<?php
			if($_POST['oculto'] == ""){$_POST['condeta']="0";}
			$vigusu = vigencia_usuarios($_SESSION['cedulausu']);
			$vigencia = $vigusu;
			if(!$_POST['oculto'])
			{
				$fec = date("d/m/Y");
				$_POST['fecha'] = $fec;
				$_POST['tipo'] = 'S';
				$_POST['valoradicion'] = 0;
				$_POST['valorreduccion'] = 0;
				$_POST['valortraslados'] = 0;
				$_POST['valor'] = 0;
				$sqlr = "SELECT MAX(RIGHT(codigo,2)) FROM ccpethumvariables ORDER BY codigo Desc";
				$res = mysqli_query($linkbd,$sqlr);
				$row = mysqli_fetch_row($res);
				$_POST['codigo'] = $row[0]+1;
				if(strlen($_POST['codigo']) == 1){$_POST['codigo'] = '0'.$_POST['codigo'];}
			}
		?>
		<form name="form2" method="post" action="">
			<input type="hidden" name="valfocus" id="valfocus" value="1"/>
			<table class="inicio ancho" align="center" >
				<tr>
					<td class="titulos" colspan="10">.: Agregar variable de pago</td>
					<td class="cerrar" style="width:7%" onClick="location.href='hum-principal.php'">Cerrar</td>
				</tr>
				<tr>
					<td class="tamano01" style="width:3cm;">C&oacute;digo:</td>
					<td style="width:5%;"><input type="text" name="codigo" id="codigo" value="<?php echo $_POST['codigo']?>" maxlength="2" style="width:98%;" onKeyPress="javascript:return solonumeros(event)" onKeyUp="return tabular(event,this)" class="tamano02" ></td>
					<td class="tamano01" style="width:4cm;">Nombre ingreso:</td>
					<td colspan="7"><input type="text"  name="nombre" id="nombre" value="<?php echo $_POST['nombre']?>" style="width:100%;" onKeyUp="return tabular(event,this)" class="tamano02"/></td>
				</tr>
				<tr>
					<td class="tamano01">Paga salud:</td>
					<td >
						<div class="swsino">
							<input type="checkbox" name="swsalud" class="swsino-checkbox" id="idswsalud" value="<?php echo $_POST[swsalud];?>" <?php if($_POST[swsalud]=='S'){echo "checked";}?> onChange="cambiocheck('3');"/>
							<label class="swsino-label" for="idswsalud">
								<span class="swsino-inner"></span>
								<span class="swsino-switch"></span>
							</label>
						</div>
					</td>
					<td class="tamano01">Paga Pensi&oacute;n:</td>
					<td style="width:10%;">
						<div class="swsino">
							<input type="checkbox" name="swpension" class="swsino-checkbox" id="idswpension" value="<?php echo $_POST['swpension'];?>" <?php if($_POST['swpension'] == 'S'){echo "checked";}?> onChange="cambiocheck('4');"/>
							<label class="swsino-label" for="idswpension">
								<span class="swsino-inner"></span>
								<span class="swsino-switch"></span>
							</label>
						</div>
					</td>
					<td class="tamano01" style="width:3cm;">Paga ARL:</td>
					<td style="width:10%;">
						<div class="swsino">
							<input type="checkbox" name="swarl" class="swsino-checkbox" id="idswarl" value="<?php echo $_POST['swarl'];?>" <?php if($_POST['swarl'] == 'S'){echo "checked";}?> onChange="cambiocheck('5');"/>
							<label class="swsino-label" for="idswarl">
								<span class="swsino-inner"></span>
								<span class="swsino-switch"></span>
							</label>
						</div>
					</td>
					<td class="tamano01" style="width:3cm;">Paga parafiscales:</td>
					<td style="width:10%;">
						<div class="swsino">
							<input type="checkbox" name="swparafiscal" class="swsino-checkbox" id="idswparafiscal" value="<?php echo $_POST['swparafiscal'];?>" <?php if($_POST['swparafiscal'] == 'S'){echo "checked";}?> onChange="cambiocheck('1');"/>
							<label class="swsino-label" for="idswparafiscal">
								<span class="swsino-inner"></span>
								<span class="swsino-switch"></span>
							</label>
						</div>
					</td>
					<td class="tamano01" style="width:3cm;">Provisiona:</td>
					<td style="width:10%;">
						<div class="swsino">
							<input type="checkbox" name="swprovision" class="swsino-checkbox" id="idswprovision" value="<?php echo $_POST['swprovision'];?>" <?php if($_POST['swprovision'] == 'S'){echo "checked";}?> onChange="cambiocheck('2');"/>
							<label class="swsino-label" for="idswprovision">
								<span class="swsino-inner"></span>
								<span class="swsino-switch"></span>
							</label>
						</div>
					</td>
					
				</tr>
				<tr>
					<td class="tamano01">Funcionamiento: </td>
					<td colspan="3">
						<select name="funcionamiento" id="funcionamiento" onChange="validar();" style="width:100%;" class="tamano02">
							<option value="-1">Seleccione...</option>
							<?php
								$sqlr="SELECT * FROM conceptoscontables WHERE modulo = '2' AND tipo = 'F' ORDER BY codigo";
								$resp = mysqli_query($linkbd,$sqlr);
								while ($row = mysqli_fetch_row($resp))
								{
									if($row[0] == $_POST['funcionamiento'])
									{
										echo "<option value='$row[0]' SELECTED>$row[0] - $row[3] - $row[1]</option>";
										$_POST['funcionamientonom']="$row[0] - $row[3] - $row[1]";
									}
									else{echo "<option value='$row[0]'>$row[0] - $row[3] - $row[1]</option>";}
								}
							?>
						</select>
						<input type="hidden" name="funcionamientonom" id="funcionamientonom" value="<?php echo $_POST['funcionamientonom']?>">
					<td class="tamano01">Temporal Funcionamiento: </td>
					<td colspan="3">
						<select name="funtemporal" id="funtemporal" onChange="validar();" style="width:100%;" class="tamano02">
							<option value="-1">Seleccione...</option>
							<?php
								$sqlr="SELECT * FROM conceptoscontables WHERE modulo='2' AND tipo='FT' ORDER BY codigo";
								$resp = mysqli_query($linkbd,$sqlr);
								while ($row = mysqli_fetch_row($resp))
								{
									if($row[0]==$_POST['funtemporal'])
									{
										echo "<option value='$row[0]' SELECTED>$row[0] - $row[3] - $row[1]</option>";
										$_POST['funtemporalnom'] = "$row[0] - $row[3] - $row[1]";
									}
									else{echo "<option value='$row[0]'>$row[0] - $row[3] - $row[1]</option>";}
								}
							?>
						</select>
						<input type="hidden" name="funtemporalnom" id="funtemporalnom" value="<?php echo $_POST['funtemporalnom']?>">
					</td>
				</tr>
				<tr>
					<td class="tamano01">Inversi&oacute;n: </td>
					<td colspan="3">
						<select name="inversion" id="inversion" onChange="validar();" style="width:100%;" class="tamano02">
							<option value="-1">Seleccione...</option>
							<?php
								$sqlr="SELECT * FROM conceptoscontables WHERE modulo = '2' AND tipo = 'IN' ORDER BY codigo";
								$resp = mysqli_query($linkbd,$sqlr);
								while ($row = mysqli_fetch_row($resp))
								{
									if($row[0] == $_POST['inversion'])
									{
										echo "<option value='$row[0]' SELECTED>$row[0] - $row[3] - $row[1]</option>";
										$_POST['inversionnom'] = "$row[0] - $row[3] - $row[1]";
									}
									else{echo "<option value='$row[0]'>$row[0] - $row[3] - $row[1]</option>";}
								}
							?>
						</select>
						<input type="hidden" name="inversionnom" id="inversionnom" value="<?php echo $_POST[funtemporalnom]?>">
					</td>
					<td class="tamano01">Temporal Inversi&oacute;n: </td>
					<td colspan="3">
						<select name="invtemporal" id="invtemporal" onChange="validar();" style="width:100%;" class="tamano02">
							<option value="-1">Seleccione...</option>
							<?php
								$sqlr="SELECT * FROM conceptoscontables WHERE modulo='2' AND tipo='IT' ORDER BY codigo";
								$resp = mysqli_query($linkbd,$sqlr);
								while ($row = mysqli_fetch_row($resp))
								{
									if($row[0] == $_POST['invtemporal'])
									{
										echo "<option value='$row[0]' SELECTED>$row[0] - $row[3] - $row[1]</option>";
										$_POST['invtemporalnom'] = "$row[0] - $row[3] - $row[1]";
									}
									else{echo "<option value='$row[0]'>$row[0] - $row[3] - $row[1]</option>";}
								}
							?>
						</select>
						<input type="hidden" name="invtemporalnom" id="invtemporalnom" value="<?php echo $_POST['funtemporalnom']?>">
					</td>
				</tr>
				<tr>
					<td class="tamano01">Gastos Comercialización: </td>
					<td colspan="3">
						<select name="gastoscomer" id="gastoscomer" onChange="validar();" style="width:100%;" class="tamano02">
							<option value="-1">Seleccione...</option>
							<?php
								$sqlr="SELECT * FROM conceptoscontables WHERE modulo = '2' AND tipo = 'CP' ORDER BY codigo";
								$resp = mysqli_query($linkbd,$sqlr);
								while ($row = mysqli_fetch_row($resp))
								{
									if($row[0] == $_POST['gastoscomer'])
									{
										echo "<option value='$row[0]' SELECTED>$row[0] - $row[3] - $row[1]</option>";
										$_POST['gastoscomernom'] = "$row[0] - $row[3] - $row[1]";
									}
									else{echo "<option value='$row[0]'>$row[0] - $row[3] - $row[1]</option>";}
								}
							?>
						</select>
						<input type="hidden" name="gastoscomernom" id="gastoscomernom" value="<?php echo $_POST['gastoscomernom']?>">
					</td>
					<td class="tamano01">Temporal Gastos Comercialización: </td>
					<td colspan="3">
						<select name="gastoscomertem" id="gastoscomertem" onChange="validar();" style="width:100%;" class="tamano02">
							<option value="-1">Seleccione...</option>
							<?php
								$sqlr="SELECT * FROM conceptoscontables WHERE modulo = '2' AND tipo = 'CT' ORDER BY codigo";
								$resp = mysqli_query($linkbd,$sqlr);
								while ($row = mysqli_fetch_row($resp))
								{
									if($row[0] == $_POST['gastoscomertem'])
									{
										echo "<option value='$row[0]' SELECTED>$row[0] - $row[3] - $row[1]</option>";
										$_POST['gastoscomertemnom'] = "$row[0] - $row[3] - $row[1]";
									}
									else{echo "<option value='$row[0]'>$row[0] - $row[3] - $row[1]</option>";}
								}
							?>
						</select>
						<input type="hidden" name="gastoscomertemnom" id="gastoscomertemnom" value="<?php echo $_POST['gastoscomertemnom']?>">
					</td>
				</tr>
			</table>
			<input type="hidden" name="oculto" id="oculto"  value="1">
			</div>
			<input type="hidden" name="condeta" id="condeta" value="<?php echo $_POST['condeta'];?>"/>
			<?php
				if($_POST['oculto'] == '2')
				{
					if ($_POST['nombre'] != "")
					{
						$nr = "1";
						if($_POST['swparafiscal'] == "" || $_POST['swparafiscal'] == "N"){$valswparafiscal = 'N';}
						else{$valswparafiscal = 'S';}
						if($_POST['swprovision'] == "" || $_POST['swprovision'] == "N"){$valswprovision = 'N';}
						else{$valswprovision = 'S';}
						if($_POST['swsalud'] == "" || $_POST['swsalud'] == "N"){$valswsalud = 'N';}
						else{$valswsalud = 'S';}
						if($_POST['swpension'] == "" || $_POST['swpension'] == "N"){$valswpesion = 'N';}
						else{$valswpesion = 'S';}
						if($_POST['swarl'] == "" || $_POST['swarl'] == "N"){$valswarl = 'N';}
						else{$valswarl = 'S';}
						$sqlr="INSERT INTO ccpethumvariables (codigo, nombre, pparafiscal, estado, provision, psalud, ppension, parl, funcionamiento, funtemporal, inversion, invtemporal, gastoscomerc, gastoscomerctemporal) VALUES ('".$_POST['codigo']."', '".$_POST['nombre']."', '$valswparafiscal' , 'S', '$valswprovision', '$valswsalud', '$valswpesion', '$valswarl', '".$_POST['funcionamiento']."', '".$_POST['funtemporal']."', '".$_POST['inversion']."', '".$_POST['invtemporal']."', '".$_POST['gastoscomer']."', '".$_POST['gastoscomertemnom']."')";
						mysqli_query($linkbd,$sqlr);
					}
				}
			?> 
			<div id="bgventanamodal2">
				<div id="ventanamodal2">
					<IFRAME  src="" name="buster" marginWidth=0 marginHeight=0 frameBorder=0 id="ventana2" frameSpacing=0 style="left:500px; width:900px; height:500px; top:200;"> 
					</IFRAME>
				</div>
			</div>
		</form>
	</body>
</html>
