<?php

	header("Content-Type: text/html;charset=utf-8");
    require_once 'PHPExcel/Classes/PHPExcel.php';
    require "comun.inc";
    require "funciones.inc";
    require 'funcionesSP.inc.php';

    session_start();

    class Plantilla{
        private $linkbd;
        public function __construct() {
            $this->linkbd = conectar_v7();
            $this->linkbd->set_charset("utf8");
        }
        public function fuentes() {
            $sql = "SELECT id, nombre FROM plan_fuentes_ppi ORDER BY id";
            $data = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
            return $data;
        }

        public function ejesEstrategicos($pdtId) {
            $sql = "SELECT id, nombre FROM plan_eje_estrategico WHERE pdt_id = $pdtId AND estado = 'S' ORDER BY id";
            $data = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
            return $data;
        }

        public function vigencias($pdtId) {
            $vigencias = [];
            $sql_pdt = "SELECT PG.vigencia_inicial, PG.vigencia_final FROM plan_pdt AS PDT INNER JOIN para_periodos_gobierno AS PG ON PDT.periodo_gobierno_id = PG.id WHERE PDT.id = $pdtId";
            $pdt = mysqli_fetch_all(mysqli_query($this->linkbd,$sql_pdt),MYSQLI_ASSOC);
            $i = $pdt[0]["vigencia_inicial"];
            for ($i; $i <= $pdt[0]["vigencia_final"] ; $i++) { 
                $vigencias[] = $i;
            }
            return $vigencias;
        }

    }

    function getExcelColumns($start = 'A', $end = 'BM') {
        $columns = [];
        $current = $start;
    
        // Generar las columnas
        while ($current !== $end) {
            $columns[] = $current;
            $current++;
        }
        $columns[] = $end; // Añadir el último valor
    
        return $columns;
    }
    $pdtId = $_POST["pdtId"];
    $obj = new Plantilla();
    $fuentes = $obj->fuentes();
    $ejesEstrategicos = $obj->ejesEstrategicos($pdtId);
    $vigencias = $obj->vigencias($pdtId);
    $vig01 = $vigencias[0];
    $vig02 = $vigencias[1];
    $vig03 = $vigencias[2];
    $vig04 = $vigencias[3];
    $objPHPExcel = new PHPExcel();

    $objPHPExcel->createSheet(1);
    $objPHPExcel->createSheet(2);
    $objPHPExcel->getSheet(0)->getStyle('A:BK')->applyFromArray(
        array(
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
            )
        )
    );
    $objPHPExcel->getSheet(0)->setTitle("Formato");
    $objPHPExcel->getSheet(1)->setTitle("Lineas estrategicas");
    $objPHPExcel->getSheet(2)->setTitle("Fuentes");
    $objPHPExcel->getProperties()
    ->setCreator("IDEAL 10")
    ->setLastModifiedBy("IDEAL 10")
    ->setTitle("Exportar Excel con PHP")
    ->setSubject("Documento de prueba")
    ->setDescription("Documento generado con PHPExcel")
    ->setKeywords("usuarios phpexcel")
    ->setCategory("reportes");

    //----Cuerpo de Documento----
    $objPHPExcel->getSheet(0)
    ->mergeCells('A1:BK1')
    ->mergeCells('A2:BK2')
    ->setCellValue('A1', 'PLANEACIÓN ESTRATEGICA')
    ->setCellValue('A2', 'PLANTILLA PLAN PLURIANUAL');
    $objPHPExcel->getSheet(0)
    -> getStyle ("A1")
    -> getFill ()
    -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
    -> getStartColor ()
    -> setRGB ('C8C8C8');
    $objPHPExcel->getSheet(0)
    -> getStyle ("A1:BK2")
    -> getFont ()
    -> setBold ( true )
    -> setName ( 'Verdana' )
    -> setSize ( 10 )
    -> getColor ()
    -> setRGB ('000000');
    $objPHPExcel->getSheet(0)
    -> getStyle ('A1:BK2')
    -> getAlignment ()
    -> applyFromArray (array ( 'horizontal'  =>  PHPExcel_Style_Alignment :: HORIZONTAL_CENTER ,) );
    $objPHPExcel->getSheet(0)
    -> getStyle ('A3:BK3')
    -> getAlignment ()
    -> applyFromArray (array ( 'horizontal'  =>  PHPExcel_Style_Alignment :: VERTICAL_JUSTIFY ,) );
    $objPHPExcel->getSheet(0)
    -> getStyle ("A2")
    -> getFill ()
    -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID);

    $borders = array(
        'borders' => array(
            'allborders' => array(
                'style' => PHPExcel_Style_Border::BORDER_THIN,
            'color' => array('argb' => 'FF000000'),
            )
        ),
    );
    $objPHPExcel->getSheet(0)
    ->setCellValue('A3', "Linea estrategica")
    ->setCellValue('B3', "Nombre")
    ->setCellValue('C3', "Código de programa")
    ->setCellValue('D3', "Recursos propios $vig01")
    ->setCellValue('E3', "SGP Educación $vig01")
    ->setCellValue('F3', "SGP Salud $vig01")
    ->setCellValue('G3', "SGP Deporte $vig01")
    ->setCellValue('H3', "SGP Cultura $vig01")
    ->setCellValue('I3', "SGP Libre inversion $vig01")
    ->setCellValue('J3', "SGP Libre destinación $vig01")
    ->setCellValue('K3', "SGP Alimentación escolar $vig01")
    ->setCellValue('L3', "SGP Municipios rio Magdalena $vig01")
    ->setCellValue('M3', "SGP APSB $vig01")
    ->setCellValue('N3', "Credito $vig01")
    ->setCellValue('O3', "Transferencias de capital - Cofinanciacion departamento $vig01")
    ->setCellValue('P3', "Transferencias de capital - Cofinanciacion nacion $vig01")
    ->setCellValue('Q3', "SGR $vig01")
    ->setCellValue('R3', "Otros $vig01")
    ->setCellValue('S3', "Recursos propios $vig02")
    ->setCellValue('T3', "SGP Educación $vig02")
    ->setCellValue('U3', "SGP Salud $vig02")
    ->setCellValue('V3', "SGP Deporte $vig02")
    ->setCellValue('W3', "SGP Cultura $vig02")
    ->setCellValue('X3', "SGP Libre inversion $vig02")
    ->setCellValue('Y3', "SGP Libre destinación $vig02")
    ->setCellValue('Z3', "SGP Alimentación escolar $vig02")
    ->setCellValue('AA3', "SGP Municipios rio Magdalena $vig02")
    ->setCellValue('AB3', "SGP APSB $vig02")
    ->setCellValue('AC3', "Credito $vig02")
    ->setCellValue('AD3', "Transferencias de capital - Cofinanciacion departamento $vig02")
    ->setCellValue('AE3', "Transferencias de capital - Cofinanciacion nacion $vig02")
    ->setCellValue('AF3', "SGR $vig02")
    ->setCellValue('AG3', "Otros $vig02")
    ->setCellValue('AH3', "Recursos propios $vig03")
    ->setCellValue('AI3', "SGP Educación $vig03")
    ->setCellValue('AJ3', "SGP Salud $vig03")
    ->setCellValue('AK3', "SGP Deporte $vig03")
    ->setCellValue('AL3', "SGP Cultura $vig03")
    ->setCellValue('AM3', "SGP Libre inversion $vig03")
    ->setCellValue('AN3', "SGP Libre destinación $vig03")
    ->setCellValue('AO3', "SGP Alimentación escolar $vig03")
    ->setCellValue('AP3', "SGP Municipios rio Magdalena $vig03")
    ->setCellValue('AQ3', "SGP APSB $vig03")
    ->setCellValue('AR3', "Credito $vig03")
    ->setCellValue('AS3', "Transferencias de capital - Cofinanciacion departamento $vig03")
    ->setCellValue('AT3', "Transferencias de capital - Cofinanciacion nacion $vig03")
    ->setCellValue('AU3', "SGR $vig03")
    ->setCellValue('AV3', "Otros $vig03")
    ->setCellValue('AW3', "Recursos propios $vig04")
    ->setCellValue('AX3', "SGP Educación $vig04")
    ->setCellValue('AY3', "SGP Salud $vig04")
    ->setCellValue('AZ3', "SGP Deporte $vig04")
    ->setCellValue('BA3', "SGP Cultura $vig04")
    ->setCellValue('BB3', "SGP Libre inversion $vig04")
    ->setCellValue('BC3', "SGP Libre destinación $vig04")
    ->setCellValue('BD3', "SGP Alimentación escolar $vig04")
    ->setCellValue('BE3', "SGP Municipios rio Magdalena $vig04")
    ->setCellValue('BF3', "SGP APSB $vig04")
    ->setCellValue('BG3', "Credito $vig04")
    ->setCellValue('BH3', "Transferencias de capital - Cofinanciacion departamento $vig04")
    ->setCellValue('BI3', "Transferencias de capital - Cofinanciacion nacion $vig04")
    ->setCellValue('BJ3', "SGR $vig04")
    ->setCellValue('BK3', "Otros $vig04");
    $objPHPExcel->getSheet(0)
        -> getStyle ("A3:BK3")
        -> getFill ()
        -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
        -> getStartColor ()
        -> setRGB ('99ddff');
    $objPHPExcel->getSheet(0)->getStyle("A3:BK3")->getFont()->setBold(true);
    $objPHPExcel->getSheet(0)->getStyle('A1:BK1')->applyFromArray($borders);
    $objPHPExcel->getSheet(0)->getStyle('A2:BK2')->applyFromArray($borders);
    $objPHPExcel->getSheet(0)->getStyle('A3:BK3')->applyFromArray($borders);

    $columns = getExcelColumns('A', 'BK');
    foreach ($columns as $column) {
        $objPHPExcel->getSheet(0)->getColumnDimension("$column")->setAutoSize(true);
    }

    //Hoja conceptos

    $objPHPExcel->getSheet(1)->getStyle('A:B')->applyFromArray(
        array(
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
            )
        )
    );

    $objPHPExcel->getSheet(1)
    ->mergeCells('A1:B1')
    ->mergeCells('A2:B2')
    ->setCellValue('A1', 'PLANEACIÓN ESTRATEGICA')
    ->setCellValue('A2', 'LINEAS ESTRATEGICAS');
    $objPHPExcel->getSheet(1)
    -> getStyle ("A1")
    -> getFill ()
    -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
    -> getStartColor ()
    -> setRGB ('C8C8C8');
    $objPHPExcel->getSheet(1)
    -> getStyle ("A1:A2")
    -> getFont ()
    -> setBold ( true )
    -> setName ( 'Verdana' )
    -> setSize ( 10 )
    -> getColor ()
    -> setRGB ('000000');
    $objPHPExcel->getSheet(1)
    -> getStyle ('A1:A2')
    -> getAlignment ()
    -> applyFromArray (array ( 'horizontal'  =>  PHPExcel_Style_Alignment :: HORIZONTAL_CENTER ,) );
    $objPHPExcel->getSheet(1)
    -> getStyle ('A3:B3')
    -> getAlignment ()
    -> applyFromArray (array ( 'horizontal'  =>  PHPExcel_Style_Alignment :: VERTICAL_JUSTIFY ,) );
    $objPHPExcel->getSheet(1)
    -> getStyle ("A2")
    -> getFill ()
    -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID);

    $borders = array(
        'borders' => array(
            'allborders' => array(
                'style' => PHPExcel_Style_Border::BORDER_THIN,
            'color' => array('argb' => 'FF000000'),
            )
        ),
    );
    $objPHPExcel->getSheet(1)
    ->setCellValue('A3', 'Código')
    ->setCellValue('B3', "Nombre");
    $objPHPExcel->getSheet(1)
        -> getStyle ("A3:B3")
        -> getFill ()
        -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
        -> getStartColor ()
        -> setRGB ('99ddff');
    $objPHPExcel->getSheet(1)->getStyle("A3:B3")->getFont()->setBold(true);
    $objPHPExcel->getSheet(1)->getStyle('A1:B1')->applyFromArray($borders);
    $objPHPExcel->getSheet(1)->getStyle('A2:B2')->applyFromArray($borders);
    $objPHPExcel->getSheet(1)->getStyle('A3:B3')->applyFromArray($borders);

    $objWorksheet = $objPHPExcel->getSheet(1);

    $objPHPExcel->getSheet(1)->getColumnDimension('A')->setAutoSize(true);
    $objPHPExcel->getSheet(1)->getColumnDimension('B')->setAutoSize(true);

    //Hoja de columnas
    $objPHPExcel->getSheet(2)
    ->mergeCells('A1:B1')
    ->mergeCells('A2:B2')
    ->setCellValue('A1', 'CONTABILIDAD')
    ->setCellValue('A2', 'FUENTES');
    $objPHPExcel-> getSheet(2)
    -> getStyle ("A1")
    -> getFill ()
    -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
    -> getStartColor ()
    -> setRGB ('C8C8C8');
    $objPHPExcel-> getSheet(2)
    -> getStyle ("A1:B2")
    -> getFont ()
    -> setBold ( true )
    -> setName ( 'Verdana' )
    -> setSize ( 10 )
    -> getColor ()
    -> setRGB ('000000');
    $objPHPExcel-> getSheet(2)
    -> getStyle ('A1:B2')
    -> getAlignment ()
    -> applyFromArray (array ( 'horizontal'  =>  PHPExcel_Style_Alignment :: HORIZONTAL_CENTER ,) );
    $objPHPExcel-> getSheet(2)
    -> getStyle ('A3:B2')
    -> getAlignment ()
    -> applyFromArray (array ( 'horizontal'  =>  PHPExcel_Style_Alignment :: VERTICAL_JUSTIFY ,) );
    $objPHPExcel-> getSheet(2)
    -> getStyle ("A2")
    -> getFill ()
    -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID);

    $borders = array(
        'borders' => array(
            'allborders' => array(
                'style' => PHPExcel_Style_Border::BORDER_THIN,
            'color' => array('argb' => 'FF000000'),
            )
        ),
    );
    $objPHPExcel->getSheet(2)
    ->setCellValue('A3', 'Código')
    ->setCellValue('B3', "Nombre");
    $objPHPExcel-> getSheet(2)
        -> getStyle ("A3:B3")
        -> getFill ()
        -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
        -> getStartColor ()
        -> setRGB ('99ddff');
    $objPHPExcel->getSheet(2)->getStyle("A3:B3")->getFont()->setBold(true);
    $objPHPExcel->getSheet(2)->getStyle('A1:B1')->applyFromArray($borders);
    $objPHPExcel->getSheet(2)->getStyle('A2:B2')->applyFromArray($borders);
    $objPHPExcel->getSheet(2)->getStyle('A3:B3')->applyFromArray($borders);


    $objPHPExcel->getSheet(2)->getColumnDimension('A')->setAutoSize(true);
    $objPHPExcel->getSheet(2)->getColumnDimension('B')->setAutoSize(true);


    //Formatos
    if(!empty($ejesEstrategicos)){
        $total = count($ejesEstrategicos);
        $row = 4;
        for ($i=0; $i < $total ; $i++) {
            $objPHPExcel->getSheet(1)
            ->setCellValue("A$row", "LE".$ejesEstrategicos[$i]['id'])
            ->setCellValue("B$row", $ejesEstrategicos[$i]['nombre']);
            $row++;
        }
    }

    if(!empty($fuentes)){
        $total = count($fuentes);
        $row = 4;
        for ($i=0; $i < $total ; $i++) {
            $objPHPExcel->getSheet(2)
            ->setCellValue("A$row", $fuentes[$i]['id'])
            ->setCellValue("B$row", $fuentes[$i]['nombre']);
            $row++;
        }
    }
  
    $objPHPExcel->setActiveSheetIndex(0);
    //----Guardar documento----
    header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment;filename="formato_cargue_masivo_plurianual.xlsx"');
    header('Cache-Control: max-age=0');
    $objWriter=PHPExcel_IOFactory::createWriter($objPHPExcel,'Excel2007');
    $objWriter->save('php://output');
    die();

?>
