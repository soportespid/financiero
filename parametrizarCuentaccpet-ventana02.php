<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require "comun.inc";
	require "funciones.inc";
	require "validaciones.inc";
	session_start();
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
?>
<!doctype html>
<html lang="es">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=9">
		<title>:: Ideal</title>
		<link href="css/css2.css" rel="stylesheet" type="text/css">
		<link href="css/css3.css" rel="stylesheet" type="text/css">
		<script type="text/javascript" src="css/programas.js"></script>
		<script type="text/javascript" src="css/calendario.js"></script>
		<script> 
			function ponprefijo(cuentaDeb, cuentaCredito, pos)
			{
				var cuentasDeb = parent.document.form2.elements['cuentaDebito[]'];
				var cuentasCred = parent.document.form2.elements['cuentaCredito[]'];
				//console.log(cuentasDeb);
				if(cuentasDeb.length > 1)
				{
					cuentasDeb[pos].value = cuentaDeb;
					cuentasCred[pos].value = cuentaCredito;
				}
				else
				{
					cuentasDeb.value = cuentaDeb;
					cuentasCred.value = cuentaCredito;
				}
				parent.despliegamodal2("hidden");
			}
		</script>
		<?php titlepag();?>
	</head>
	<body>
		<form name="form2" action="" method="post">
			<?php
				if ($_POST['oculto']=='')
				{
					$_POST['cuenta'] = $_GET['cuenta'];
					$_POST['posicion'] = $_GET['posicion'];
					$_POST['sector'] = $_GET['sector'];
					$_POST['destino'] = $_GET['destino'];
					$_POST['seccion'] = $_GET['seccion'];
					$_POST['numpos'] = 0;
					$_POST['numres'] = 10;
					$_POST['nummul'] = 0;
				}
			?>
			<table class="inicio" style="width:99.4%;">
				<tr>
					<td class="titulos" colspan="4">:: Buscar Cuenta contable</td>
					<td class="cerrar" style="width:7%" onClick="parent.despliegamodal2('hidden');">Cerrar</td>
				</tr>
			</table>
			<input type="hidden" name="cuenta" value="<?php echo $_POST['cuenta']?>">
			<input type="hidden" name="posicion" value="<?php echo $_POST['posicion']?>">
			<input type="hidden" name="sector" value="<?php echo $_POST['sector']?>">
			<input type="hidden" name="destino" id="destino" value="<?php echo $_POST['destino']?>">
			<input type="hidden" name="seccion" id="seccion" value="<?php echo $_POST['seccion']?>">
			<input type="hidden" name="oculto" id="oculto" value="1">
			<input type="hidden" name="numres" id="numres" value="<?php echo $_POST['numres'];?>">
			<input type="hidden" name="numpos" id="numpos" value="<?php echo $_POST['numpos'];?>">
			<input type="hidden" name="nummul" id="nummul" value="<?php echo $_POST['nummul'];?>">
            <div class="subpantalla" style="height:420px !important; width:99%; overflow-x:hidden;">
				<?php
					$crit1="";
					$crit2="";
					$sqrlCuentaSector = "SELECT id_cuenta FROM ccpet_cuentasectores WHERE id_sector = '".$_POST['sector']."'";
					$resCuentaSector = mysqli_query($linkbd, $sqrlCuentaSector);
					$rowCuentaSector = mysqli_fetch_row($resCuentaSector);

                    $countCuentasSectores = mysqli_num_rows($resCuentaSector);

                    $sqrlconceptoContable = "SELECT concepto_cont, tipo FROM ccpetcuentas_concepto_contable WHERE cuenta_p = '".$_POST['cuenta']."' AND estado = 'S'";
                    $resconceptoContable = mysqli_query($linkbd, $sqrlconceptoContable);
                    $rowconceptoContable = mysqli_fetch_row($resconceptoContable);

                    $sec = intval($_POST['seccion']);
                    $sqlr_seccion_cc = "SELECT id_cc FROM centrocostos_seccionpresupuestal WHERE (id_sp = '$sec' OR id_sp = '$_POST[seccion]')";
                    $res_seccion_cc = mysqli_query($linkbd, $sqlr_seccion_cc);
                    $row_seccion_cc = mysqli_fetch_row($res_seccion_cc);

                    /* if(substr($_POST['cuenta'], 0, 5) == '2.1.1'){
                        $sqlrCuentaContable = "SELECT cuenta, debito, credito, cc FROM conceptoscontables_det WHERE codigo = '$rowconceptoContable[0]' AND cc = '$row_seccion_cc[0]' AND tipo = '$rowconceptoContable[1]' GROUP BY cuenta";
                        $resCuentaContable = mysqli_query($linkbd, $sqlrCuentaContable);
                        $con=1;
                        echo "
                        <table class='inicio' align='center' style='width:99%;'>
                            <tr>
                                <td class='titulos2' style='width:3%'>Item</td>
                                <td class='titulos2' style='width:20%'>Cuenta contable</td>
                                <td class='titulos2' style='width:60'>Nombre cuenta</td>
                                <td class='titulos2' style='width:10/'>Centro costo</td>
                            </tr>";
                        $iter='saludo1a';
                        $iter2='saludo2'; 
                        $cuentasDebito = array();
                        $cuentasCredito = array();
                        $cc = array();
                        while ($rowCuentaContable = mysqli_fetch_row($resCuentaContable))
                        {
                            if($rowCuentaContable[1] == 'S'){$cuentasDebito[] = $rowCuentaContable[0];}
                            else {$cuentasCredito[] = $rowCuentaContable[0];}
                            $cc[] = $rowCuentaContable[3];
                        }
                        for($x = 0; $x < count($cuentasDebito); $x++)
                        {
                            $nombreCuentaContable = buscacuenta($cuentasDebito[$x]);

                            $sqlrCuentaContableC = "SELECT cuenta, debito, credito, cc FROM conceptoscontables_det WHERE codigo = '$rowconceptoContable[0]' AND cc = '$row_seccion_cc[0]'  AND tipo = '$rowconceptoContable[1]' AND credito = 'S' GROUP BY cuenta";

                            $resCuentaContableC = mysqli_query($linkbd, $sqlrCuentaContableC);
                            $rowCuentaContableC = mysqli_fetch_row($resCuentaContableC);

                            $cuentasCredito[$x] = $rowCuentaContableC[0] != '' ? $rowCuentaContableC[0] : $cuentasCredito[$x];

                            $con2 = $con + $_POST['numpos'];
                            echo"
                            <tr class='$iter' onClick=\"javascript:ponprefijo('$cuentasDebito[$x]','$cuentasCredito[$x]', '".$_POST['posicion']."')\" style='text-transform:uppercase'>
                                <td>$con2</td>
                                <td>$cuentasDebito[$x]</td>
                                <td>$nombreCuentaContable</td>
                                <td>$cc[$x]</td>
                            </tr>";
                            $con += 1;
                            $aux = $iter;
                            $iter = $iter2;
                            $iter2 = $aux;
                        } 
                        echo"</table>";
                    }
                    else if(substr($_POST['cuenta'], 0, 5) == '2.3.1')
                    {
                        $sqlrCuentaContable = "SELECT cuenta, debito, credito, cc FROM conceptoscontables_det WHERE codigo = '$rowconceptoContable[0]' AND cc = '$row_seccion_cc[0]'  AND tipo = '$rowconceptoContable[1]' AND destino_compra = '".$_POST['destino']."' GROUP BY cuenta";
                        $resCuentaContable = mysqli_query($linkbd, $sqlrCuentaContable);
                        $con = 1;
                        echo "
                        <table class='inicio' align='center' style='width:99%;'>
                            <tr>
                                <td class='titulos2' style='width:3%'>Item</td>
                                <td class='titulos2' style='width:20%'>Cuenta contable</td>
                                <td class='titulos2' style='width:60'>Nombre cuenta</td>
                                <td class='titulos2' style='width:10%'>Centro costo</td>
                            </tr>";
                        $iter = 'saludo1a';
                        $iter2 = 'saludo2'; 
                        $cuentasDebito = array();
                        $cuentasCredito = array();
                        $cc = array();
                        while ($rowCuentaContable = mysqli_fetch_row($resCuentaContable)) 
                        {
                            if($rowCuentaContable[1] == 'S'){$cuentasDebito[] = $rowCuentaContable[0];}
                            else {$cuentasCredito[] = $rowCuentaContable[0];}
                            $cc[] = $rowCuentaContable[3];
                        }

                        for($x = 0; $x < count($cuentasDebito); $x++)
                        {
                            $sqrlCuentaSector = "SELECT id_cuenta FROM ccpet_cuentasectores WHERE id_sector = '".$_POST['sector']."'";
                            $resCuentaSector = mysqli_query($linkbd, $sqrlCuentaSector);
                            while($rowCuentaSector = mysqli_fetch_row($resCuentaSector)){

                                if(substr($cuentasDebito[$x], 0, 2) != 55){
                                    $nombreCuentaContable = buscacuenta($cuentasDebito[$x]);

                                    $sqlrCuentaContableC = "SELECT cuenta, debito, credito, cc FROM conceptoscontables_det WHERE codigo = '$rowconceptoContable[0]' AND cc = '$row_seccion_cc[0]'  AND tipo = '$rowconceptoContable[1]' AND destino_compra = '".$_POST['destino']."' AND credito = 'S' GROUP BY cuenta";

                                    $resCuentaContableC = mysqli_query($linkbd, $sqlrCuentaContableC);
                                    $rowCuentaContableC = mysqli_fetch_row($resCuentaContableC);

                                    $cuentasCredito[$x] = $rowCuentaContableC[0] != '' ? $rowCuentaContableC[0] : $cuentasCredito[$x];

                                    $con2=$con+ $_POST['numpos'];
                                    echo"
                                    <tr class='$iter' onClick=\"javascript:ponprefijo('$cuentasDebito[$x]','$cuentasCredito[$x]', '".$_POST['posicion']."')\" style='text-transform:uppercase'>
                                        <td>$con2</td>
                                        <td>$cuentasDebito[$x]</td>
                                        <td>$nombreCuentaContable</td>
                                        <td>$cc[$x]</td>
                                    </tr>";
                                    $con+=1;
                                    $aux=$iter;
                                    $iter=$iter2;
                                    $iter2=$aux;
                                }
                                else if(substr($cuentasDebito[$x], 0, 4) == $rowCuentaSector[0])
                                {
                                    $nombreCuentaContable = buscacuenta($cuentasDebito[$x]);

                                    $sqlrCuentaContableC = "SELECT cuenta, debito, credito, cc FROM conceptoscontables_det WHERE codigo = '$rowconceptoContable[0]' AND cc = '$row_seccion_cc[0]'  AND tipo = '$rowconceptoContable[1]' AND destino_compra = '".$_POST['destino']."' AND credito = 'S' GROUP BY cuenta";

                                    $resCuentaContableC = mysqli_query($linkbd, $sqlrCuentaContableC);
                                    $rowCuentaContableC = mysqli_fetch_row($resCuentaContableC);

                                    $cuentasCredito[$x] = $rowCuentaContableC[0] != '' ? $rowCuentaContableC[0] : $cuentasCredito[$x];

                                    $con2=$con+ $_POST['numpos'];
                                    echo"
                                    <tr class='$iter' onClick=\"javascript:ponprefijo('$cuentasDebito[$x]','$cuentasCredito[$x]', '".$_POST['posicion']."')\" style='text-transform:uppercase'>
                                        <td>$con2</td>
                                        <td>$cuentasDebito[$x]</td>
                                        <td>$nombreCuentaContable</td>
                                        <td>$cc[$x]</td>
                                    </tr>";
                                    $con+=1;
                                    $aux=$iter;
                                    $iter=$iter2;
                                    $iter2=$aux;
                                }
                            }
                        }
                        echo"</table>";
                    }
                    else
                    { */
                        $sqlrCuentaContable = "SELECT cuenta, asociado_a, cc FROM conceptoscontables_det WHERE codigo = '$rowconceptoContable[0]' AND cc = '$row_seccion_cc[0]'  AND tipo = '$rowconceptoContable[1]' AND debito = 'S' AND destino_compra = '".$_POST['destino']."' GROUP BY cuenta";
                        $resCuentaContable = mysqli_query($linkbd, $sqlrCuentaContable);
                        $con=1;
                        echo "
                        <table class='inicio' align='center' style='width:99%;'>
                            <tr>
                                <td class='titulos2' style='width:3%'>Item</td>
                                <td class='titulos2' style='width:20%'>Cuenta contable</td>
                                <td class='titulos2' style='width:60'>Nombre cuenta</td>
                                <td class='titulos2' style='width:10%'>Centro costo</td>
                            </tr>";
                        $iter='saludo1a';
                        $iter2='saludo2'; 
                        while ($rowCuentaContable = mysqli_fetch_row($resCuentaContable)) 
                        {
                            if($countCuentasSectores > 0){
                                $sqrlCuentaSector = "SELECT id_cuenta FROM ccpet_cuentasectores WHERE id_sector = '".$_POST['sector']."'";
                                $resCuentaSector = mysqli_query($linkbd, $sqrlCuentaSector);
                                while($rowCuentaSector = mysqli_fetch_row($resCuentaSector)){
                                    if(substr($rowCuentaContable[0], 0, 2) != 55){
                                        $nombreCuentaContable = buscacuenta($rowCuentaContable[0]);

                                        $con2=$con+ $_POST['numpos'];
                                        echo"
                                        <tr class='$iter' onClick=\"javascript:ponprefijo('$rowCuentaContable[0]','$rowCuentaContable[1]', '".$_POST['posicion']."')\" style='text-transform:uppercase'>
                                            <td>$con2</td>
                                            <td>$rowCuentaContable[0]</td>
                                            <td>$nombreCuentaContable</td>
                                            <td>$rowCuentaContable[2]</td>
                                        </tr>";
                                        $con+=1;
                                        $aux=$iter;
                                        $iter=$iter2;
                                        $iter2=$aux;

                                    }
                                    else if(substr($rowCuentaContable[0], 0, 4) == $rowCuentaSector[0] || substr($_POST['cuenta'], 0, 3) != '2.3' || $_POST['destino'] != '00')
                                    {
                                        $nombreCuentaContable = buscacuenta($rowCuentaContable[0]);

                                        $con2=$con+ $_POST['numpos'];
                                        echo"
                                        <tr class='$iter' onClick=\"javascript:ponprefijo('$rowCuentaContable[0]','$rowCuentaContable[1]', '".$_POST['posicion']."')\" style='text-transform:uppercase'>
                                            <td>$con2</td>
                                            <td>$rowCuentaContable[0]</td>
                                            <td>$nombreCuentaContable</td>
                                            <td>$rowCuentaContable[2]</td>
                                        </tr>";
                                        $con+=1;
                                        $aux=$iter;
                                        $iter=$iter2;
                                        $iter2=$aux;
                                    }
                                }
                                
                            }else{
                                if(substr($rowCuentaContable[0], 0, 2) != 55){
                                    $nombreCuentaContable = buscacuenta($rowCuentaContable[0]);

                                    $con2=$con+ $_POST['numpos'];
                                    echo"
                                    <tr class='$iter' onClick=\"javascript:ponprefijo('$rowCuentaContable[0]','$rowCuentaContable[1]', '".$_POST['posicion']."')\" style='text-transform:uppercase'>
                                        <td>$con2</td>
                                        <td>$rowCuentaContable[0]</td>
                                        <td>$nombreCuentaContable</td>
                                        <td>$rowCuentaContable[2]</td>
                                    </tr>";
                                    $con+=1;
                                    $aux=$iter;
                                    $iter=$iter2;
                                    $iter2=$aux;
                                }
                                else if(substr($rowCuentaContable[0], 0, 4) == $rowCuentaSector[0] || substr($_POST['cuenta'], 0, 3) != '2.3' || $_POST['destino'] != '00')
                                {
                                    $nombreCuentaContable = buscacuenta($rowCuentaContable[0]);

                                    $con2=$con+ $_POST['numpos'];
                                    echo"
                                    <tr class='$iter' onClick=\"javascript:ponprefijo('$rowCuentaContable[0]','$rowCuentaContable[1]', '".$_POST['posicion']."')\" style='text-transform:uppercase'>
                                        <td>$con2</td>
                                        <td>$rowCuentaContable[0]</td>
                                        <td>$nombreCuentaContable</td>
                                        <td>$rowCuentaContable[2]</td>
                                    </tr>";
                                    $con+=1;
                                    $aux=$iter;
                                    $iter=$iter2;
                                    $iter2=$aux;
                                }
                            }
                            
                        } 
                        echo"</table>";
                    /* } */
                    
				?>
			</div>
		</form>
	</body>
</html>
