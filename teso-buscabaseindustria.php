<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require "comun.inc";
	require "funciones.inc";
	session_start();
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html lang="es">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1">
		<meta name="viewport" content="user-scalable=no">
		<title>:: IDEAL 10 - Tesoreria</title>
		<link href="favicon.ico" rel="shortcut icon"/>
        <link href="css/css2.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
        <link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="sweetalert2/dist/sweetalert2.min.css" rel="stylesheet">
		<script src="sweetalert2/dist/sweetalert2.min.js"></script>
		<script type="text/javascript" src="JQuery/jquery-2.1.4.min.js"></script>
		<script type="text/javascript" src="JQuery/autoNumeric-master/autoNumeric-min.js"></script>
		<script type="text/javascript" src="JQuery/alphanum/jquery.alphanum.js"></script>
		<script>
			function verUltimaPos(idcta, filas, filtro){
				var scrtop=$('#divdet').scrollTop();
				var altura=$('#divdet').height();
				var numpag=$('#nummul').val();
				var limreg=$('#numres').val();
				if((numpag<=0)||(numpag=="")){numpag=0;}
				if((limreg==0)||(limreg=="")){limreg=10;}
				numpag++;
				location.href = "teso-editarbaseindustria.php?idegre="+idcta+"&scrtop="+scrtop+"&totreg="+filas+"&altura="+altura+"&numpag="+numpag+"&limreg="+limreg+"&filtro="+filtro;
			}
			function eliminar(idr){
				if (confirm("Esta Seguro de Anular El Egreso No "+idr)){
					document.form2.oculto.value=2;
					document.form2.var1.value=idr;
					document.form2.submit();
				}
			}
			function pdf(){
				document.form2.action = "teso-pdfconsignaciones.php";
				document.form2.target = "_BLANK";
				document.form2.submit(); 
				document.form2.action = "";
				document.form2.target = "";
			}
			function excell(){
				document.form2.action = "teso-buscabaseindustriaexcel.php";
				document.form2.target = "_BLANK";
				document.form2.submit(); 
				document.form2.action = "";
				document.form2.target = "";
			}
		</script>
		<?php 
			$scrtop=$_GET['scrtop'];
			if($scrtop=="") {$scrtop=0;}
			echo"<script> window.onload=function(){ $('#divdet').scrollTop(".$scrtop.") }</script>";
			$gidcta = $_GET['idcta'];
			if(isset($_GET['filtro']))
			$_POST['nombre'] = $_GET['filtro'];
		?>
	</head>
	<body>
		<IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
		<span id="todastablas2"></span>
		<table>
			<tr><script>barra_imagenes("teso");</script><?php cuadro_titulos();?></tr>	 
			<tr><?php menu_desplegable("teso");?></tr>
			<tr>
				<td colspan="3" class="cinta"><img src="imagenes/add.png" title="Nuevo" onClick="location.href='teso-baseindustria.php'" class="mgbt"/><img src="imagenes/guardad.png" class="mgbt1"/><img src="imagenes/busca.png" title="Buscar" onClick="document.form2.submit();" class="mgbt"/><img src="imagenes/nv.png" title="Nueva Ventana"  onClick="mypop=window.open('teso-principal.php','','');mypop.focus();" class="mgbt"><img src="imagenes/excel.png" title="Excel" onClick='excell()' class="mgbt"/></td>
			</tr>	
		</table>
        <?php
			if($_GET['numpag']!=""){
				$oculto=$_POST['oculto'];
				if($oculto != 2){
					$_POST['numres'] = $_GET['limreg'];
					$_POST['numpos'] = $_GET['limreg']*($_GET['numpag']-1);
					$_POST['nummul'] = $_GET['numpag']-1;
				}
			} else {
				if($_POST['nummul']==""){
					$_POST['numres'] = 10;
					$_POST['numpos'] = 0;
					$_POST['nummul'] = 0;
				}
			}
		?>
		<form name="form2" method="post" action="teso-buscabaseindustria.php">
			<?php
				if($_POST['oculto']==''){
					$_POST['tiporec'] = 1;
				}
			?>
			<input type="hidden" name="numres" id="numres" value="<?php echo $_POST['numres'];?>"/>
			<input type="hidden" name="numpos" id="numpos" value="<?php echo $_POST['numpos'];?>"/>
			<input type="hidden" name="nummul" id="nummul" value="<?php echo $_POST['nummul'];?>"/>
			<table  class="inicio ancho" align="center" >
                <tr >
                    <td class="titulos" colspan="9" >:. Buscar Base Industria y comercio</td>
                    <td class="cerrar" style="width:7%" onClick="location.href='teso-principal.php'">Cerrar</td>
                </tr>
                <tr>
					<td class="saludo1" style="width:2cm;">Consecutivo: </td>
                    <td style="width:15%;"><input type="search" name="consecutivo" id="consecutivo" value="<?php echo $_POST['consecutivo'];?>" style="width:98%;"/></td>
                    <td class="saludo1" style="width:2cm;">N&uacute;mero de documento:</td>
                    <td style="width:15%;"><input type="search" name="cedulanit" id="cedulanit" value="<?php echo $_POST['cedulanit'];?>" style="width:98%;"/></td>
					<td class="saludo1" style="width:2.5cm;">Tipo Recaudo:</td>
					<td >
						<select name="tiporec" id="tiporec" onKeyUp="return tabular(event,this)" onChange="validar()" style="width:95%;height:30px;">
							<option value=""> Seleccione ...</option>
							<option value="1" <?php if($_POST['tiporec']=='1') echo "SELECTED"; ?>>Todos</option>
							<option value="2" <?php if($_POST['tiporec']=='2') echo "SELECTED"; ?>>Pagaron</option>
							<option value="3" <?php if($_POST['tiporec']=='3') echo "SELECTED"; ?>>Deben</option>
						</select>
					</td>
					<td class="tamano01" style="width:2.5cm;">Vigencia:</td>
					<td>
						<select name="vigeactual" style="width:98%;height:30px;">
							<option value="0">Año</option>
							<?php
								for($i=2030;$i>=2000;$i--){
									echo "<option value='$i'";
									if ($_POST['vigeactual'] == $i) echo "selected";
									echo">$i</option>";
								}
							?>
						</select>
					</td>
					<td style=" padding-bottom:0px"><em class="botonflechaverde" onClick="limbusquedas();">Buscar</em></td>
                    <input type="hidden" name="oculto" id="oculto"  value="1">
                    <input type="hidden" name="var1"  value=<?php echo $_POST['var1'];?>>
                </tr>                       
			</table>   
			<div class="subpantallap" style="height:68.5%; width:99.6%; overflow-x:hidden;" id="divdet">
			<?php
				$oculto = $_POST['oculto'];
				$crit1="";
				$crit2="";
				if ($_POST['cedulanit']!="")
				$crit1="and cedulanit='$_POST[cedulanit]'";
				if ($_POST['consecutivo']!="")
				$crit2="and id='$_POST[consecutivo]'";
				$sqlr="SELECT * FROM tesorepresentantelegal WHERE tesorepresentantelegal.id>-1 $crit1 $crit2 ORDER BY tesorepresentantelegal.id DESC";
				$resp = mysqli_query($linkbd,$sqlr);
				$ntr = mysqli_num_rows($resp);
				$_POST['numtop'] = $ntr;
				$con=1;
				echo "
				<table class='inicio' align='center' >
					<tr>
						<td colspan='6' class='titulos'>.: Resultados Busqueda:</td>
					</tr>
					<tr>
						<td colspan='11'>Pagos Encontrados:</td>
					</tr>
					<tr>
						<td  class='titulos2'>Id</td>
						<td  class='titulos2'>Cedula/NIT</td>
						<td class='titulos2'>Nombre</td>
						<td class='titulos2'>Fecha</td>
						<td class='titulos2' width='5%'><center>Estado</td>
						<td class='titulos2' width='5%'><center>Anular</td>
						<td class='titulos2' width='5%'><center>Ver</td>
					</tr>";	
				$iter='zebra1';
				$iter2='zebra2';
				$filas=1;
				while ($row =mysqli_fetch_row($resp)){
					$ntr=buscatercero($row[1]);
					if($gidcta!=""){
						if($gidcta==$row[0]){
							$estilo='background-color:yellow';
						} else{
							$estilo="";
						}
					} else {
						$estilo="";
					}	
					$idcta="'$row[0]'";
					$numfil="'$filas'";
					$filtro="'$_POST[cedulanit]'";
					if($row[3]=='S'){
						$imgsem="src='imagenes/sema_verdeON.jpg' title='Activo'";$coloracti="#0F0";$_POST['lswitch1'][$row[0]]=0;
						$iconanu="<img src='imagenes/anular.png' onClick=\"eliminar($row[0])\" class='icoop' title='Anular'/>";
					} else {
						$imgsem="src='imagenes/sema_rojoON.jpg' title='Inactivo'";$coloracti="#C00";$_POST['lswitch1'][$row[0]]=1;
					}
					if($_POST['tiporec'] != 1){
						$bandera = '';
						$sqlvt = "SELECT valortotal FROM tesoindustria  WHERE estado = 'P' AND tercero = '$row[1]' AND ageliquidado = '".$_POST['vigeactual']."'";
						$resvt = mysqli_query($linkbd,$sqlvt);
						$rowvt = mysqli_fetch_row($resvt);
						if($rowvt[0] != ''){
							if($_POST['tiporec'] == 2){
								$bandera = '1';
							}
						}else{
							if($_POST['tiporec'] == 3){
								$bandera = '1';
							}
						}
					}
					else{
						$bandera = '1';
					}
					if($bandera == '1'){
						echo"
						<tr class='$iter' onDblClick=\"verUltimaPos($idcta, $numfil, $filtro)\" style='text-transform:uppercase; $estilo' >
							<td>$row[0]</td>
							<td>$row[1]</td>
							<td>$ntr</td>
							<td>$row[2]</td>
							<td style='text-align:center;'><img $imgsem style='width:20px'/></td>
							<td style='text-align:center;'>$iconanu</td>
							<td style='text-align:center;'><img src='imagenes/lupa02.png' class='icoop' title='Ver' onClick=\"verUltimaPos($idcta, $numfil, $filtro)\"/></td>
						</tr>";
						echo "
							<input type='hidden' name='id[]' id='id[]' value='".$row[0]."'>
							<input type='hidden' name='cedulaNit[]' id='cedulaNit[]' value='".$row[1]."'>
							<input type='hidden' name='nombre[]' id='nombre[]' value='".$ntr."'>
							<input type='hidden' name='fecha[]' id='fecha[]' value='".$row[2]."'>
							<input type='hidden' name='estado[]' id='estado[]' value='".$row[3]."'>
						";
						$con+=1;
						$filas++;
						$aux=$iter;
						$iter=$iter2;
						$iter2=$aux;
					}
				}
				echo"
				</table>";
                if($_POST['oculto']==2){
                    $sqlr="select * from tesorepresentantelegal where id_egreso=$_POST[var1]";
                    $resp = mysqli_query($linkbd,$sqlr);
                    $row=mysqli_fetch_row($resp);
                    $cedulanit=$row[1];

                    $sqlr="select * from tesoestablecimiento where cedulanit='$cedulanit'";
                    $resp = mysqli_query($linkbd,$sqlr);
                    $row=mysqli_fetch_row($resp);
                    $matricula=$row[2];
                    //*********
                    $sqlr="update tesorepresentantelegal set estado='N' where id='$_POST[var1]'";
                    mysqli_query($linkbd,$sqlr);	 
                    $sqlr="update tesoestablecimiento set estado='N' where cedulanit='$cedulanit'";
                    mysqli_query($linkbd,$sqlr);	 
                    $sqlr="update tesoestablecimientociiu set estado='N' where matricula=' $matricula'";
                    mysqli_query($linkbd,$sqlr);
                } 
			?>
            </div>
		</form> 
	</body>
</html>