<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require "comun.inc";
	require "funciones.inc";
	require "conversor.php";
	session_start();
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html lang="es">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="ie=edge"/>
		<title>:: IDEAL 10 - Gesti&oacute;n humana</title>
		<link href="css/css2.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css">
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css">
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css">
		<link href="sweetalert2/dist/sweetalert2.min.css" rel="stylesheet">
		<script src="sweetalert2/dist/sweetalert2.min.js"></script>
		<script type="text/javascript" src="css/calendario.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script type="text/javascript" src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script>
			function guardar(){
				if(document.form2.totalinca.value>0){
					if (document.form2.tercero.value!='' && document.form2.periodo.value!=''){
						Swal.fire({
							icon: 'question',
							title: '¿Esta Seguro de guardar?',
							showDenyButton: true,
							confirmButtonText: 'Guardar',
							confirmButtonColor: '#01CC42',
							denyButtonText: 'Cancelar',
							denyButtonColor: '#FF121A',
						}).then(
							(result) => {
								if (result.isConfirmed){
									document.form2.oculto.value = "2";
									document.form2.submit();
								}else if (result.isDenied){
									Swal.fire({
										icon: 'info',
										title: 'No se guardo',
										confirmButtonText: 'Continuar',
										confirmButtonColor: '#FF121A',
										timer: 2500
									});
								}
							}
						)
					}else{
						Swal.fire({
							icon: 'error',
							title: 'Error!',
							text: 'Faltan datos para completar la incapacidad',
							confirmButtonText: 'Continuar',
							confirmButtonColor: '#FF121A',
							timer: 2500
						});
					}
				}else{
					Swal.fire({
						icon: 'error',
						title: 'Error!',
						text: 'Falta asignar detalles a la incapacidad',
						confirmButtonText: 'Continuar',
						confirmButtonColor: '#FF121A',
						timer: 2500
					});
				}
				
			}
			function validar(formulario){document.form2.submit();}
			function buscater(e){
				if (document.form2.tercero.value!=""){
					document.form2.bt.value='1';
					document.form2.submit();
				}
			}
			function funcionmensaje()
			{
				var numid=document.getElementById('idcomp').value; 
				document.location.href = "hum-incapacidadeseditar.php?idinca="+numid;
			}			
			function despliegamodal2(_valor,_num){
				document.getElementById("bgventanamodal2").style.visibility = _valor;
				if(_valor=="hidden"){
					document.getElementById('ventana2').src="";
				}else{
					document.getElementById('ventana2').src = "ventanacargafuncionariosretenciones.php?objeto=tercero";
				}
			}
			function fagregar(){
				if(document.form2.vigenciat.value!=""){
					if(document.form2.tipoinca.value!=""){
						if(document.form2.periodo.value!="-1"){
							if(document.form2.diasperiodo.value!=""){
								if(document.form2.porcinca.value!=""){
									if(document.form2.salbas.value!=""){
										document.form2.oculto.value="4";
										document.form2.submit();
									}else{
										Swal.fire({
											icon: 'error',
											title: 'Error!',
											text: 'Falta un funcionario con cargo asignado',
											confirmButtonText: 'Continuar',
											confirmButtonColor: '#FF121A',
											timer: 2500
										});
									}
								}else{
									Swal.fire({
										icon: 'error',
										title: 'Error!',
										text: 'Falta asignar porcentaje de incapacidad',
										confirmButtonText: 'Continuar',
										confirmButtonColor: '#FF121A',
										timer: 2500
									});
								}
							}else{
								Swal.fire({
									icon: 'error',
									title: 'Error!',
									text: 'Falta asignar días de incapacidad',
									confirmButtonText: 'Continuar',
									confirmButtonColor: '#FF121A',
									timer: 2500
								});
							}
						}else{
							Swal.fire({
								icon: 'error',
								title: 'Error!',
								text: 'Falta asignar el mes',
								confirmButtonText: 'Continuar',
								confirmButtonColor: '#FF121A',
								timer: 2500
							});
						}
					}else{
						Swal.fire({
							icon: 'error',
							title: 'Error!',
							text: 'Falta asignar tipo de incapacidad',
							confirmButtonText: 'Continuar',
							confirmButtonColor: '#FF121A',
							timer: 2500
						});
					}
				}else{
					Swal.fire({
						icon: 'error',
						title: 'Error!',
						text: 'Falta activar la vigencia actual',
						confirmButtonText: 'Continuar',
						confirmButtonColor: '#FF121A',
						timer: 2500
					});
				}
			}
			function eliminar(variable){
				Swal.fire({
					icon: 'question',
					title: '¿Esta seguro de eliminar?',
					showDenyButton: true,
					confirmButtonText: 'Guardar',
					confirmButtonColor: '#01CC42',
					denyButtonText: 'Cancelar',
					denyButtonColor: '#FF121A',
				}).then(
					(result) => {
						if (result.isConfirmed){
							document.getElementById('elimina').value = variable;
							document.form2.oculto.value = "3";
							document.form2.submit();
						}else if (result.isDenied){
							Swal.fire({
								icon: 'info',
								title: 'No se Elimino',
								confirmButtonText: 'Continuar',
								confirmButtonColor: '#FF121A',
								timer: 2500
							});
						}
					}
				)
			}
			function cambiocheck(id){
				switch(id){
					case '1':
						if(document.getElementById('idswibc').value == 'S'){
							document.getElementById('idswibc').value = 'N';
						}else{
							document.getElementById('idswibc').value = 'S';
						}
						break;
					case '2':
						if(document.getElementById('idswarl').value == 'S'){
							document.getElementById('idswarl').value = 'N';
						}else{
							document.getElementById('idswarl').value = 'S';
						}
						break;
					case '3':
						if(document.getElementById('idswparafiscal').value == 'S'){
							document.getElementById('idswparafiscal').value = 'N';
						}else{
							document.getElementById('idswparafiscal').value='S';
						}
				}
				document.form2.submit();
			}
		</script>
		<?php titlepag();?>
	</head>
	<body>
		<IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
		<span id="todastablas2"></span>
		<table>
			<tr><script>barra_imagenes("hum");</script><?php cuadro_titulos();?></tr>	 
			<tr><?php menu_desplegable("hum");?></tr>
			<tr>
				<td colspan="3" class="cinta"><img src="imagenes/add.png" title="Nuevo" onClick="location.href='hum-incapacidadesagregar.php'" class="mgbt"><img src="imagenes/guarda.png" title="Guardar" onClick="guardar()" class="mgbt"><img src="imagenes/busca.png" title="Buscar" onClick="location.href='hum-incapacidadesbuscar.php'" class="mgbt"><img src="imagenes/agenda1.png" title="Agenda" onClick="mypop=window.open('plan-agenda.php','','');mypop.focus()" class="mgbt"><img src="imagenes/nv.png" title="Nueva ventana" onClick="mypop=window.open('hum-principal.php','','');mypop.focus();" class="mgbt"><img src="imagenes/duplicar_pantalla.png" title="Duplicar pesta&ntilde;a" onClick="mypop=window.open('<?php echo $url2; ?>','','');mypop.focus();" class="tooltip bottom mgbt"><img src='imagenes/iratras.png' title="Atr&aacute;s" onClick="location.href='hum-menunomina.php'" class="mgbt"></td>
			</tr>
		</table>
		<div id="bgventanamodalm" class="bgventanamodalm">
			<div id="ventanamodalm" class="ventanamodalm">
				<IFRAME src="" name="ventanam" marginWidth=0 marginHeight=0 frameBorder=0 id="ventanam" frameSpacing=0 style=" width:700px; height:130px; top:200; overflow:hidden;"> 
				</IFRAME>
			</div>
		</div>
		<form name="form2" method="post" action="">
			<?php
				if(!$_POST['oculto']){
					date("Y");
					$consec = selconsecutivo('hum_incapacidades','num_inca');
					$_POST['idcomp'] = $consec;
					$_POST['fecha'] = date("Y-m-d");
					$_POST['vigenciat'] = date("Y");
					$sqlr = "SELECT salario FROM admfiscales WHERE vigencia = '".$_POST['vigenciat']."'";
					$resp = mysqli_query($linkbd,$sqlr);
					while ($row = mysqli_fetch_row($resp)){
						$_POST['salmin'] = $row[0];
					}
					$_POST['swibc'] = 'S';
				}
				if($_POST['bt'] == '1'){
					$nresul = buscatercero($_POST['tercero']);
					if($nresul!=''){
						$_POST['ntercero'] = $nresul;
						$sqlr="
						SELECT codfun, GROUP_CONCAT(descripcion ORDER BY CONVERT(valor, SIGNED INTEGER) SEPARATOR '<->')
						FROM hum_funcionarios
						WHERE (item = 'VALESCALA') AND codfun = (SELECT codfun FROM hum_funcionarios WHERE descripcion = '".$_POST['tercero']."' AND estado = 'S') AND estado = 'S'
						GROUP BY codfun";
						$resp2 = mysqli_query($linkbd,$sqlr);
						$row2 = mysqli_fetch_row($resp2);
						$_POST['salbas'] = $row2[1];
						$_POST['codfun'] = $row2[0];
					}
					else {
						$_POST['ntercero'] = "";
					}
				}
				$pf[] = array();
				$pfcp = array();
				if($_POST['vactodos'] == 1){$checkt = " checked";}
				else {$checkt = " ";}
			?>
			<table class="inicio ancho">
				<tr>
					<td class="titulos" colspan="7">:: Incapacidades y Licencias</td>
					<td class="cerrar" style="width:7%" onClick="location.href='hum-principal.php'">Cerrar</td>
				</tr>
				<tr> 
					<td class="saludo1" style="width:3.5cm;">No Incapacidad:</td>
					<td style="width:12%"><input type="text" name="idcomp" id="idcomp" value="<?php echo $_POST['idcomp']?>" style="width:98%;" readonly></td>
					<td class="saludo1"  style="width:4cm;">Fecha registro:</td>
					<td style="width:12%"><input type="date" name="fecha"  value="<?php echo $_POST['fecha']?>" style="width:98%;" readonly></td>
					<td class="saludo1" style="width:3.5cm;">Vigencia:</td>
					<td style="width:14%"><input type="text" name="vigenciat" id="vigenciat" value="<?php echo $_POST['vigenciat'];?>" style="width:100%;" readonly></td>
					<td rowspan="6" colspan="2" style="background:url(imagenes/incapacidad.png); background-repeat:no-repeat; background-position:center; background-size: 60% 100%;" ></td>
				</tr>
				<tr>
					<td class="saludo1">Funcionario:</td>
					<td><input type="text" id="tercero" name="tercero" onKeyPress="javascript:return solonumeros(event)" onKeyUp="return tabular(event,this)" value="<?php echo $_POST['tercero']?>" onChange="buscater(event)" style="width:100%;" class="colordobleclik" autocomplete="off" onDblClick="despliegamodal2('visible');"></td>
					<td colspan="4"><input type="text" name="ntercero" id="ntercero" value="<?php echo $_POST['ntercero']?>" style="width: 100%;"readonly></td>
					<input type="hidden" name="bt" id="bt" value="0"/>
				</tr>
				<tr>
					<td class="saludo1">Cod Incapacidad EPS:</td>
					<td><input type="text" name="incaeps" id="incaeps" value="<?php echo $_POST['incaeps']?>" style="width:98%;"></td>
					<td class="saludo1">Cod Incapacidad Ant. EPS:</td>
					<td><input name="incaepsant" type="text"  value="<?php echo $_POST['incaepsant']?>" style="width:98%;"></td>
					<td class="saludo1">Salario basico:</td>
					<td><input type="text" name="salbas" value="<?php echo $_POST['salbas']?>" style="width:100%;"></td>
				</tr>
				<tr>
					<td class="saludo1">Fecha inicial:</td>
					<td><input type="text" name="fechai" value="<?php echo $_POST['fechai']?>" maxlength="10" onKeyPress="javascript:return solonumeros(event)" onKeyUp="return tabular(event,this)"  id="fc_1198971545" onKeyDown="mascara(this,'/',patron,true)" title="DD/MM/YYYY" style="width:100%;" class="colordobleclik" onDblClick="displayCalendarFor('fc_1198971545');" autocomplete="off" onChange=""></td>
					<td class="saludo1">Fecha final:</td>
					<td><input type="text" name="fechaf" value="<?php echo $_POST['fechaf']?>" maxlength="10" onKeyPress="javascript:return solonumeros(event)" onKeyUp="return tabular(event,this)"  id="fc_1198971546" onKeyDown="mascara(this,'/',patron,true)" title="DD/MM/YYYY" style="width:100%;" class="colordobleclik" onDblClick="displayCalendarFor('fc_1198971546');" autocomplete="off" onChange=""></td>
					<td class="saludo1">Tipo incapacidad:</td>
					<td>
						<select name="tipoinca" id="tipoinca" style="width:100%;">
							<option value="">Seleccione ....</option>
							<?php
								$sqlr = "SELECT * FROM dominios WHERE tipo = 'S' AND nombre_dominio LIKE 'LICENCIAS' ";
								$resp = mysqli_query($linkbd,$sqlr);
								while ($row = mysqli_fetch_row($resp)){
									if($row[1] == $_POST['tipoinca']){
										echo "<option value='$row[1]' SELECTED>$row[0]</option>";
									}else{
										echo "<option value='$row[1]'>$row[0]</option>";
									}
								}
							?>
						</select>
					</td>
				</tr>
				<tr>
					<td class="saludo1">Mes:</td>
					<td>
						<select name="periodo" id="periodo" style="width:98%;">
							<option value="-1">Seleccione ....</option>
							<?php
								$sqlr = "SELECT * FROM meses WHERE estado = 'S'";
								$resp = mysqli_query($linkbd,$sqlr);
								while ($row = mysqli_fetch_row($resp)){
									if($row[0] == $_POST['periodo']){
										echo "<option value='$row[0]' SELECTED>$row[1]</option>";
										$_POST['periodonom'] = $row[1];
										$_POST['periodonom'] = $row[2];
									}else {
										echo "<option value='$row[0]'>$row[1]</option>";
									}
								}
							?>
						</select> 
					</td>
					<td class="saludo1">Dias Incapacidad:</td>
					<td><input type="text" name="diasperiodo" id="diasperiodo" value="<?php echo $_POST['diasperiodo']?>" style="width:98%;"></td>
					<td class="saludo1">Incapacidad (%):</td>
					<td>
						<select name="porcinca" id="porcinca" style="width:100%;">
							<option value="">Seleccione ....</option>
							<?php
								$sqlr="SELECT codigo,porcentaje FROM hum_porcentajesinca WHERE estado='S' ";
								$resp = mysqli_query($linkbd,$sqlr);
								while ($row =mysqli_fetch_row($resp)){
									if($row[0] == $_POST['porcinca']){
										echo "<option value='$row[0]' SELECTED>$row[1]%</option>";
									}else {
										echo "<option value='$row[0]'>$row[1]%</option>";
									}
								}
							?>
						</select>
					</td>
				</tr>
				<tr>
					<td class="saludo1" >Pagar EPS y AFP:</td>
					<td style="width:7%">
						<div class="swsino">
							<input type="checkbox" name="swibc" class="swsino-checkbox" id="idswibc" value="<?php echo $_POST['swibc'];?>" <?php if($_POST['swibc'] == 'S'){echo "checked";}?> onChange="cambiocheck('1');"/>
							<label class="swsino-label" for="idswibc">
								<span class="swsino-inner"></span>
								<span class="swsino-switch"></span>
							</label>
						</div>
					</td>
					<td class="saludo1" >Pagar ARL:</td>
					<td style="width:7%">
						<div class="swsino">
							<input type="checkbox" name="swarl" class="swsino-checkbox" id="idswarl" value="<?php echo $_POST['swarl'];?>" <?php if($_POST['swarl'] == 'S'){echo "checked";}?> onChange="cambiocheck('2');"/>
							<label class="swsino-label" for="idswarl">
								<span class="swsino-inner"></span>
								<span class="swsino-switch"></span>
							</label>
						</div>
					</td>
					<td class="saludo1" >Pagar parafiscales:</td>
					<td style="width:7%">
						<div class="swsino">
							<input type="checkbox" name="swparafiscal" class="swsino-checkbox" id="idswparafiscal" value="<?php echo $_POST['swparafiscal'];?>" <?php if($_POST['swparafiscal'] == 'S'){echo "checked";}?> onChange="cambiocheck('3');"/>
							<label class="swsino-label" for="idswparafiscal">
								<span class="swsino-inner"></span>
								<span class="swsino-switch"></span>
							</label>
						</div>
					</td>
				</tr>
				<tr>
					<td style="height:35px;"><em class="botonflechaverde" onClick="fagregar();">Agragar</em></td>
				</tr>
			</table>
			<input type="hidden" name="codfun" id="codfun" value="<?php echo $_POST['codfun']?>">
			<input type="hidden" name="oculto" id="oculto" value="1">
			<input type="hidden" name="salmin" id="salmin"  value="<?php echo $_POST['salmin']?>">
			<div class="subpantalla" style="height:33%; width:99.5%;overflow-x:hidden"> 
				<table class="inicio" width="99%">
					<tr><td class="titulos" colspan="9">Detalles incapacidad</td></tr>
					<tr>
						<td class="titulos2" style="width:6%">Item</td>
						<td class="titulos2" style="width:6%">Tipo</td>
						<td class="titulos2" style="width:6%">Vigencia</td>
						<td class="titulos2">Mes</td>
						<td class="titulos2">Dias</td>
						<td class="titulos2">%</td>
						<td class="titulos2">Valor Dia</td>
						<td class="titulos2">Valor Total</td>
						<td class="titulos2" style="width:8%">Eliminar</td>
					</tr>
					<?php
						if ($_POST['oculto'] == '3'){
							$posi=$_POST['elimina'];
							unset($_POST['viginca'][$posi]);
							unset($_POST['mesinca'][$posi]);
							unset($_POST['diasinca'][$posi]);
							unset($_POST['valordiainca'][$posi]);
							unset($_POST['valorinca'][$posi]);
							unset($_POST['porvinca'][$posi]);
							unset($_POST['tipinca'][$posi]);
							$_POST['viginca'] = array_values($_POST['viginca']);
							$_POST['mesinca'] = array_values($_POST['mesinca']);
							$_POST['diasinca'] = array_values($_POST['diasinca']);
							$_POST['valordiainca'] = array_values($_POST['valordiainca']);
							$_POST['valorinca'] = array_values($_POST['valorinca']);
							$_POST['porvinca'] = array_values($_POST['porvinca']);
							$_POST['tipinca'] = array_values($_POST['tipinca']);
							$_POST['elimina'] ='';
						}
						if($_POST['oculto'] == '4'){
							switch($_POST['tipoinca']){
								case "EP":
								case "EG":
								case "LR":
									switch($_POST['porcinca']){ 
										case 1:{
											$totaldias = $_POST['diasperiodo'];
											$saldia = $_POST['salbas'] / 30; 
											$diasalmin = ($_POST['salmin'] / 30); 
											if($totaldias > 2){
												$saldo1 = $saldia * 2;
												$saldia2 = ($saldia * 66.667) / 100;
												if($saldia2 >= $diasalmin){
													$saldia2g = $saldia2;
												}else {
													$saldia2g = $diasalmin;
												}
												$saldo2 = $saldia2g * ($totaldias - 2);
												//100%
												$_POST['viginca'][] = $_POST['vigenciat'];
												$_POST['mesinca'][] = $_POST['periodo'];
												$_POST['diasinca'][] = 2;
												$_POST['valordiainca'][] = $saldia;
												$_POST['valorinca'][] = round($saldo1);
												$_POST['porvinca'][] = 100;
												$_POST['tipinca'][] = $_POST['tipoinca'];
												//66,667%
												$_POST['viginca'][] = $_POST['vigenciat'];
												$_POST['mesinca'][] = $_POST['periodo'];
												$_POST['diasinca'][] = $totaldias-2;
												$_POST['valordiainca'][] = $saldia2g;
												$_POST['valorinca'][] = round($saldo2);
												$_POST['porvinca'][] = 66.667;
												$_POST['tipinca'][]=$_POST['tipoinca'];
											}else{
												$saldo1 = ($saldia) * $totaldias;
												$_POST['viginca'][] = $_POST['vigenciat'];
												$_POST['mesinca'][] = $_POST['periodo'];
												$_POST['diasinca'][] = $totaldias;
												$_POST['valordiainca'][] = $saldia;
												$_POST['valorinca'][] = round($saldo1);
												$_POST['porvinca'][] = 100;
												$_POST['tipinca'][] = $_POST['tipoinca'];
											}
										}break;
										case 2:{
											$saldia = $_POST['salbas'] / 30;
											$totaldias = $_POST['diasperiodo'];
											$diasalmin = ($_POST['salmin'] / 30);
											$saldia2 = ($saldia * 66.667) / 100;
											if($saldia2 >= $diasalmin){
												$saldia2g = $saldia2;
											}else {
												$saldia2g = $diasalmin;
											}
											$saldo2 = $saldia2g * ($totaldias);
											//66,667%
											$_POST['viginca'][] = $_POST['vigenciat'];
											$_POST['mesinca'][] = $_POST['periodo'];
											$_POST['diasinca'][] = $totaldias;
											$_POST['valordiainca'][] = $saldia2g;
											$_POST['valorinca'][] = round($saldo2);
											$_POST['porvinca'][] = 66.667;
											$_POST['tipinca'][] = $_POST['tipoinca'];
										}break;
										case 3:{
											$saldia = $_POST['salbas'] / 30;
											$totaldias = $_POST['diasperiodo'];
											$diasalmin = ($_POST['salmin'] / 30);
											$saldia2 = ($saldia * 50) / 100;
											if($saldia2 >= $diasalmin){
												$saldia2g = $saldia2;
											}else {
												$saldia2g = $diasalmin;
											}
											$saldo2 = $saldia2g * ($totaldias);
											//50%
											$_POST['viginca'][] = $_POST['vigenciat'];
											$_POST['mesinca'][] = $_POST['periodo'];
											$_POST['diasinca'][] = $totaldias;
											$_POST['valordiainca'][] = $saldia2g;
											$_POST['valorinca'][] = round($saldo2);
											$_POST['porvinca'][] = 50;
											$_POST['tipinca'][]=$_POST['tipoinca'];
										}break;
										case 4:
										{
											$_POST['viginca'][] = $_POST['vigenciat'];
											$_POST['mesinca'][] = $_POST['periodo'];
											$_POST['diasinca'][] = $_POST['diasperiodo'];
											$_POST['valordiainca'][] = 0;
											$_POST['valorinca'][] = 0;
											$_POST['porvinca'][] = 0;
											$_POST['tipinca'][] = $_POST['tipoinca'];
										}
									}break;
								case "180":
								case "LNR":{
									$_POST['viginca'][] = $_POST['vigenciat'];
									$_POST['mesinca'][] = $_POST['periodo'];
									$_POST['diasinca'][] = $_POST['diasperiodo'];
									$_POST['valordiainca'][] = 0;
									$_POST['valorinca'][] = 0;
									$_POST['porvinca'][] = 0;
									$_POST['tipinca'][] = $_POST['tipoinca'];
								}break;
								case "LM":
								case "LP":{
									$totaldias = $_POST['diasperiodo'];
									$saldia = $_POST['salbas'] / 30;
									$saldo1 = $saldia * $totaldias;
									//100%
									$_POST['viginca'][] = $_POST['vigenciat'];
									$_POST['mesinca'][] = $_POST['periodo'];
									$_POST['diasinca'][] = $totaldias;
									$_POST['valordiainca'][] = $saldia;
									$_POST['valorinca'][] = round($saldo1);
									$_POST['porvinca'][] = 100;
									$_POST['tipinca'][] = $_POST['tipoinca'];
								}break;
							}
						}
					?>
					<input type='hidden' name='elimina' id='elimina'/>
					<?php
						$co = "saludo1a";
						$co2 = "saludo2";
						$sumtotal = 0;
						$_POST['totalinca'] = count($_POST['valorinca']);
						$_POST['mestemp'] = "";
						$_POST['mesletras'] = "";
						$_POST['totaldiasinca'] = 0;
						for ($x = 0; $x < $_POST['totalinca']; $x++){
							echo "
							<input type='hidden' name='mesinca[]' value='".$_POST['mesinca'][$x]."'/>
							<input type='hidden' name='diasinca[]' value='".$_POST['diasinca'][$x]."'/>
							<input type='hidden' name='valordiainca[]' value='".$_POST['valordiainca'][$x]."'/>
							<input type='hidden' name='valorinca[]' value='".$_POST['valorinca'][$x]."'/>
							<input type='hidden' name='porvinca[]' value='".$_POST['porvinca'][$x]."'/>
							<input type='hidden' name='tipinca[]' value='".$_POST['tipinca'][$x]."'/>
							<tr class='$co'>
								<td style='text-align:right;'>".($x+1)."&nbsp;</td>
								<td style='text-align:center;'>".$_POST['tipinca'][$x]."</td>
								<td style='text-align:right;'><input type='text' name='viginca[]' value='".$_POST['viginca'][$x]."' style='text-align:right; width:100%' class='inpnovisibles'/></td>
								<td style='text-align:right;'>".$_POST['mesinca'][$x]."&nbsp;</td>
								<td style='text-align:right;'>".$_POST['diasinca'][$x]."&nbsp;</td>
								<td style='text-align:right;'>".$_POST['porvinca'][$x]."&nbsp;</td>
								<td style='text-align:right;'>$ ".number_format($_POST['valordiainca'][$x],0)."&nbsp;</td>
								<td style='text-align:right;'>$ ".number_format($_POST['valorinca'][$x],0)."&nbsp;</td>
								<td style='text-align:center;'><img src='imagenes/del.png' onclick='eliminar($x)' class='icoop'></td>
							</tr>";
							$_POST['totaldiasinca'] += $_POST['diasinca'][$x];
							$sumtotal += $_POST['valorinca'][$x];
							$aux = $co;
							$co = $co2;
							$co2 = $aux;
							if($_POST['mestemp'] == ""){
								$_POST['mesletras'] = mesletras($_POST['mesinca'][$x]);
								$_POST['mestemp'] = $_POST['mesinca'][$x];
							} elseif($_POST['mestemp'] != $_POST['mesinca'][$x]){
								$_POST['mesletras'] = $_POST['mesletras']." - ".mesletras($_POST['mesinca'][$x]);
								$_POST['mestemp'] = $_POST['mesinca'][$x];
							}
						}
						$totalred = round($sumtotal, 0, PHP_ROUND_HALF_UP);
						$_POST['valinca'] = $totalred;
						$resultado = convertir($totalred);
						echo "
							<tr class='$iter' style='text-align:right;'>
								<td colspan='7'>Total:</td>
								<td>$ ".number_format($sumtotal,0)."</td>
							</tr>
							<tr class='titulos2'>
								<td>Son:</td>
								<td colspan='8'>$resultado PESOS</td>
							</tr>";
					?>
					<input type="hidden" name="valinca" value="<?php echo $_POST['valinca']?>"/>
					<input type="hidden" name="totalinca" id="totalinca" value="<?php echo $_POST['totalinca']?>"/>
					<input type="hidden" name="mesletras" value="<?php echo $_POST['mesletras']?>"/>
					<input type="hidden" name="mestemp" value="<?php echo $_POST['mestemp']?>"/>
					<input type="hidden" name="totaldiasinca" value="<?php echo $_POST['totaldiasinca']?>"/>
				</table>
			</div>
			<?php
				if($_POST['bt'] == '1'){
					$nresul = buscatercero($_POST['tercero']);
					if($nresul != ''){
						$_POST['ntercero'] = $nresul;
					}else{
						$_POST['ntercero'] = "";
						echo"
						<script>
							Swal.fire({
								icon: 'error',
								title: 'Error!',
								text: 'Tercero Incorrecto o no Existe',
								confirmButtonText: 'Continuar',
								confirmButtonColor: '#FF121A',
								timer: 2500
							});
							document.form2.tercero.value = '';
							document.form2.tercero.focus();
						</script>";
					}
				}
				if($_POST['oculto'] == 2){
					$consec = selconsecutivo('hum_incapacidades','num_inca');
					echo"<script>document.getElementById('idcomp').value='$consec'</script>;";
					preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST['fechai'],$fecha);
					$fechai = "$fecha[3]-$fecha[2]-$fecha[1]";
					preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST['fechaf'],$fecha);
					$fechaf = "$fecha[3]-$fecha[2]-$fecha[1]";
					if($_POST['swibc'] == 'S'){$pagoibc = 'S';}
					else {$pagoibc = 'N';}
					if($_POST['swarl'] == 'S'){$pagoarl = 'S';}
					else {$pagoarl = 'N';}
					if($_POST['swparafiscal'] == 'S'){$pagopara = 'S';}
					else {$pagopara = 'N';}
					$sqlr = "INSERT INTO hum_incapacidades(num_inca, fecha, vigencia, doc_funcionario, nom_funcionario, cod_inca_n, cod_inca_a, salario, fecha_ini, fecha_fin, tipo_inca, valor_total, paga_ibc, paga_arl, paga_para, meses, dias_total, estado, cod_fun) VALUES ('$consec', '".$_POST['fecha']."','".$_POST['vigenciat']."', '".$_POST['tercero']."', '".$_POST['ntercero']."', '".$_POST['incaeps']."', '".$_POST['incaepsant']."', '".$_POST['salbas']."', '$fechai', '$fechaf', '".$_POST['tipoinca']."', '".$_POST['valinca']."', '$pagoibc', '$pagoarl', '$pagopara', '".$_POST['mesletras']."', '".$_POST['totaldiasinca']."', 'S','".$_POST['codfun']."')";
					if (mysqli_query($linkbd,$sqlr)){
						for($y = 0; $y < $_POST['totalinca']; $y++){
							$consdet = selconsecutivo('hum_incapacidades_det','id_det');
							$sqlr = "INSERT INTO hum_incapacidades_det (id_det, doc_funcionario, num_inca, tipo_inca, vigencia, mes, dias_inca, porcentaje, valor_dia, valor_total, pagar_ibc, pagar_arl, pagar_para, estado,cod_fun) VALUES ('$consdet', '".$_POST['tercero']."', '$consec', '".$_POST['tipinca'][$y]."', '".$_POST['viginca'][$y]."', '".$_POST['mesinca'][$y]."', '".$_POST['diasinca'][$y]."', '".$_POST['porvinca'][$y]."', '".$_POST['valordiainca'][$y]."', '".$_POST['valorinca'][$y]."', '$pagoibc', '$pagoarl', '$pagopara', 'S','".$_POST['codfun']."')";
							mysqli_query($linkbd,$sqlr);
						}
						echo "
						<script>
							Swal.fire({
								icon: 'success',
								title: 'Incapacidad Almacenada Exitosamente',
								showConfirmButton: true,
								confirmButtonText: 'Continuar',
								confirmButtonColor: '#01CC42',
								timer: 3500
							}).then((response) => {
								location.href = 'hum-incapacidadeseditar.php?idinca=$consec';
							});
						</script>";
					}else {
						echo"
						<script>
							Swal.fire({
								icon: 'error',
								title: 'Error!',
								text: 'Error al Almacenar Incapacidad',
								confirmButtonText: 'Continuar',
								confirmButtonColor: '#FF121A',
								timer: 2500
							});
						</script>";
					}
				}
			?>
		</form>
		<div id="bgventanamodal2">
			<div id="ventanamodal2">
				<IFRAME  src="" name="buster" marginWidth=0 marginHeight=0 frameBorder=0 id="ventana2" frameSpacing=0 style="left:500px; width:900px; height:500px; top:200;"></IFRAME>
			</div>
		</div>
	</body>
</html>
