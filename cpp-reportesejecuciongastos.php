<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require "comun.inc";
	require "funciones.inc";
	sesion();
	$linkbd=conectar_v7();
	$linkbd -> set_charset("utf8");
	cargarcodigopag($_GET[codpag],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
	$cuenta = $_POST['cuenta'];
	$seccpresupuestal = $_POST['seccpresupuestal'];
	$programatico = $_POST['programatico'];
	$cpc = $_POST['cpc'];
	$fuente = $_POST['fuente'];
	$bpin = $_POST['bpin'];
	$vigengasto = $_POST['vigengasto'];
	$tabla = "<table class='inicio'>
	<tr style='text-align:center;'>
		<td class='titulos2'>Cuenta</td>
		<td class='titulos2'>Descripción</td>
		<td class='titulos2'>Vigencia Del Gasto</td>
		<td class='titulos2'>Sección Presupuestal</td>
		<td class='titulos2'>Programatico MGA</td>
		<td class='titulos2'>CPC</td>
		<td class='titulos2'>Fuentes</td>
		<td class='titulos2'>BPIN</td>
		<td class='titulos2'>Situación de Fondos</td>
		<td class='titulos2'>Politica Publica</td>
		<td class='titulos2'>Tercero CHIP</td>
		<td class='titulos2'>Inicial</td>
		<td class='titulos2'>Adiciones</td>
		<td class='titulos2'>Reducciones</td>
		<td class='titulos2'>Credito</td>
		<td class='titulos2'>Contracredito</td>
		<td class='titulos2'>Definitivo</td>
		<td class='titulos2'>Disponibilidad</td>
		<td class='titulos2'>Compromisos</td>
		<td class='titulos2'>Obligaciones</td>
		<td class='titulos2'>Pagos</td>
		<td class='titulos2'>Tipo Tabla&nbsp;&nbsp;<img src='imagenes/excel.png' title='Excel' onClick=\"excell2('$cuenta', '$seccpresupuestal', '$programatico', '$cpc', '$fuente', '$bpin', '$vigengasto')\" style='width:20px; cursor:pointer;'></td>
	</tr>";
	$nombretablatemporal = $_SESSION['tablatemporal']."EP";
	$sqlver="
	SELECT id, codigocuenta, nombrecuenta, seccpresupuestal, vigenciagastos, programatico, bpin, cpc, fuente, situacionfondo, politicapublica, tercerochip, compromisos, obligaciones, pagos, tipotabla, fuente2, cpc2, programatico2, disponibilidad, apropiacionini, apropiaciondef, adiciones, reducciones, trascredito, trascontracredito
	FROM $nombretablatemporal
	WHERE codigocuenta = '$cuenta'AND seccpresupuestal = '$seccpresupuestal' AND programatico = '$programatico' AND cpc = '$cpc' AND fuente = '$fuente' AND bpin = '$bpin' AND vigenciagastos = '$vigengasto'
	ORDER BY id ASC";
	$resver = mysqli_query($linkbd,$sqlver);
	while ($rowver = mysqli_fetch_row($resver))
	{
		$tabla.="<tr class='cssdeta' onMouseOver=\"anterior=this.style.backgroundColor;this.style.backgroundColor='#40b3ff';\"
onMouseOut=\"this.style.backgroundColor=anterior\">
			<td>$rowver[1]</td>
			<td>$rowver[2]</td>
			<td>$rowver[4]</td>
			<td>$rowver[3]</td>
			<td>$rowver[18] ($rowver[5])</td>
			<td>$rowver[17] ($rowver[7])</td>
			<td>$rowver[16] ($rowver[8])</td>
			<td>$rowver[6]</td>
			<td>$rowver[9]</td>
			<td>$rowver[10]</td>
			<td>$rowver[11]</td>
			<td style='text-align:right;'>$".number_format($rowver[20],2,',','.')."</td>
			<td style='text-align:right;'>$".number_format($rowver[22],2,',','.')."</td>
			<td style='text-align:right;'>$".number_format($rowver[23],2,',','.')."</td>
			<td style='text-align:right;'>$".number_format($rowver[24],2,',','.')."</td>
			<td style='text-align:right;'>$".number_format($rowver[25],2,',','.')."</td>
			<td style='text-align:right;'>$".number_format($rowver[21],2,',','.')."</td>
			<td style='text-align:right;'>$".number_format($rowver[19],2,',','.')."</td>
			<td style='text-align:right;'>$".number_format($rowver[12],2,',','.')."</td>
			<td style='text-align:right;'>$".number_format($rowver[13],2,',','.')."</td>
			<td style='text-align:right;'>$".number_format($rowver[14],2,',','.')."</td>
			<td>$rowver[15]</td>
		</tr>";
	}
$tabla.='</table>';
$data_row = array('detalle'=>$tabla);
header('Content-Type: application/json');
echo json_encode($data_row);
?>