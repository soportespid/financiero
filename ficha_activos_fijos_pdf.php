<?php
	require_once("tcpdf/tcpdf_include.php");
	require('comun.inc');
	require"funciones.inc";
	require 'funcionesnomima.inc';
	session_start();
	class MYPDF extends TCPDF {
		public function Header() {
			$linkbd = conectar_v7();
			$linkbd -> set_charset("utf8");
			$sqlr = "SELECT * FROM configbasica WHERE estado='S'";
			$res = mysqli_query($linkbd,$sqlr);
			while($row=mysqli_fetch_row($res)){
				$nit = $row[0];
				$rs = $row[1];
			}
			
			$this->Image('imagenes/escudo.jpg', 22, 12, 25, 23.9, 'JPG', '', 'T', true, 300, '', false, false, 0, false, false, false);// 
			$this->SetFont('helvetica','B',7);
			$this->SetY(10);
			$this->RoundedRect(10, 10, 199, 31, 2,'' );
			$this->Cell(0.1);
			$this->Cell(50,31,'','R',0,'L'); 
			$this->SetY(10);
			$this->Cell(50.1);
			$this->SetFont('helvetica','B',12);
			$this->Cell(149,15,"$rs",0,0,'C'); 
			$this->SetY(16);
			$this->Cell(50.1);
			$this->SetFont('helvetica','B',11);
			$this->Cell(149,10,"$nit",0,0,'C');
			$this->SetY(27);
			$this->Cell(50.2);
			$this->Cell(149,14,'FICHA ACTIVO FIJO','T',0,'C'); 
		}
		public function Footer() 
		{
			$linkbd = conectar_v7();
			$linkbd -> set_charset("utf8");
			$sqlr = "SELECT direccion,telefono,web,email FROM configbasica WHERE estado='S'";
			$resp = mysqli_query($linkbd,$sqlr);
			while($row = mysqli_fetch_row($resp)){
				$direcc = strtoupper($row[0]);
				$telefonos=$row[1];
				$dirweb = strtoupper($row[3]);
				$coemail = strtoupper($row[2]);
			}
			if($direcc!=''){$vardirec="Dirección: $direcc, ";}
			else {$vardirec="";}
			if($telefonos!=''){$vartelef="Telefonos: $telefonos";}
			else{$vartelef="";}
			if($dirweb!=''){$varemail="Email: $dirweb, ";}
			else {$varemail="";}
			if($coemail!=''){$varpagiw="Pagina Web: $coemail";}
			else{$varpagiw="";}
			//$this->SetY(-16);
			$this->SetFont('helvetica', 'I', 8);
			$txt = <<<EOD
$vardirec $vartelef
$varemail $varpagiw
EOD;
			$this->Write(0, $txt, '', 0, 'C', true, 0, false, false, 0);
			//$this->SetY(-13);
			$this->Cell(0, 10, 'Page '.$this->getAliasNumPage().'/'.$this->getAliasNbPages(), 0, false, 'C', 0, '', 0, false, 'T', 'M');
			
		}
	}
	$pdf = new MYPDF('P','mm','Letter', true, 'utf8', false);// create new PDF document
	$pdf->SetDocInfoUnicode (true); 
	// set document information
	$pdf->SetCreator(PDF_CREATOR);
	$pdf->SetAuthor('G&CSAS');
	$pdf->SetTitle('Certificados');
	$pdf->SetSubject('Certificado de Disponibilidad');
	$pdf->SetKeywords('TCPDF, PDF, example, test, guide');
	$pdf->SetMargins(10, 44, 10);// set margins
	$pdf->SetHeaderMargin(44);// set margins
	$pdf->SetFooterMargin(20);// set margins
	$pdf->SetAutoPageBreak(TRUE, 20);// set auto page breaks
	// set some language-dependent strings (optional)
	if (@file_exists(dirname(__FILE__).'/lang/spa.php')){
		require_once(dirname(__FILE__).'/lang/spa.php');
		$pdf->setLanguageArray($l);
	}
	// ---------------------------------------------------------
	$pdf->AddPage();
	$pdf->SetFont('helvetica','B',9);
	$pdf->Cell(15,7,'Placa:','LT',0,'L');
	$pdf->SetFont('helvetica','I',9);
	$pdf->Cell(105,7,$_POST['placa'],'T',1,'L');
	
	$pdf->SetFont('helvetica','B',9);
	$pdf->Cell(15,7,'Nombre:','L',0,'L');
	$pdf->SetFont('helvetica','I',9);
	$pdf->Cell(105,7,$_POST['nombre'],0,1,'L');
	
	$pdf->SetFont('helvetica','B',9);
	$pdf->Cell(30,7,'Fecha de compra:','L',0,'L');
	$pdf->SetFont('helvetica','I',9);
	$pdf->Cell(30,7,$_POST['fechaCompra'],0,0,'L');
	$pdf->SetFont('helvetica','B',9);
	$pdf->Cell(35,7,'Fecha de activación:',0,0,'L');
	$pdf->SetFont('helvetica','I',9);
	$pdf->Cell(25,7,$_POST['fechaActivacion'],0,1,'L');

	$pdf->SetFont('helvetica','B',9);
	$pdf->Cell(30,7,'Fecha registro:','L',0,'L');
	$pdf->SetFont('helvetica','I',9);
	$pdf->Cell(30,7,$_POST['fechaRegistro'],0,0,'L');
	$pdf->SetFont('helvetica','B',9);
	$pdf->Cell(20,7,'Valor activo:',0,0,'L');
	$pdf->SetFont('helvetica','I',9);
	$pdf->Cell(40,7,$_POST['valorActivo'],0,1,'L');

	$pdf->SetFont('helvetica','B',9);
	$pdf->Cell(27,7,'Valor depreciado:','L',0,'L');
	$pdf->SetFont('helvetica','I',9);
	$pdf->Cell(33,7,$_POST['valorDepreciado'],0,0,'L');
	$pdf->SetFont('helvetica','B',9);
	$pdf->Cell(27,7,'Valor corrección:',0,0,'L');
	$pdf->SetFont('helvetica','I',9);
	$pdf->Cell(33,7,$_POST['valorCorreccion'],0,1,'L');

	$pdf->SetFont('helvetica','B',9);
	$pdf->Cell(20,7,'Referencia:','L',0,'L');
	$pdf->SetFont('helvetica','I',9);
	$pdf->Cell(40,7,$_POST['referencia'],0,0,'L');
	$pdf->SetFont('helvetica','B',9);
	$pdf->Cell(20,7,'Modelo:',0,0,'L');
	$pdf->SetFont('helvetica','I',9);
	$pdf->Cell(40,7,$_POST['modelo'],0,1,'L');

	$pdf->SetFont('helvetica','B',9);
	$pdf->Cell(20,7,'Serie:','L',0,'L');
	$pdf->SetFont('helvetica','I',9);
	$pdf->Cell(40,7,$_POST['serial'],0,0,'L');
	$pdf->SetFont('helvetica','B',9);
	$pdf->Cell(20,7,'Estado:',0,0,'L');
	$pdf->SetFont('helvetica','I',9);
	$pdf->Cell(40,7,$_POST['estadoActivo'],0,1,'L');

	$pdf->SetFont('helvetica','B',9);
	$pdf->Cell(16,7,'Clase:','L',0,'L');
	$pdf->SetFont('helvetica','I',9);
	$pdf->Cell(104,7,$_POST['clase'],0,1,'L');

	$pdf->SetFont('helvetica','B',9);
	$pdf->Cell(16,7,'Grupo:','L',0,'L');
	$pdf->SetFont('helvetica','I',9);
	$pdf->Cell(104,7,$_POST['grupo'],0,1,'L');

	$pdf->SetFont('helvetica','B',9);
	$pdf->Cell(16,7,'Tipo:','L',0,'L');
	$pdf->SetFont('helvetica','I',9);
	$pdf->Cell(104,7,$_POST['tipo'],0,1,'L');

	$pdf->SetFont('helvetica','B',9);
	$pdf->Cell(16,7,'Prototipo:','L',0,'L');
	$pdf->SetFont('helvetica','I',9);
	$pdf->Cell(104,7,$_POST['prototipo'],0,1,'L');

	$pdf->SetFont('helvetica','B',9);
	$pdf->Cell(16,7,'Area:','L',0,'L');
	$pdf->SetFont('helvetica','I',9);
	$pdf->Cell(104,7,$_POST['area'],0,1,'L');

	$pdf->SetFont('helvetica','B',9);
	$pdf->Cell(20,7,'Ubicación:','L',0,'L');
	$pdf->SetFont('helvetica','I',9);
	$pdf->Cell(179,7,$_POST['ubicacion'],'R',1,'L');

	$pdf->SetFont('helvetica','B',9);
	$pdf->Cell(30,7,'Centro de Costos:','L',0,'L');
	$pdf->SetFont('helvetica','I',9);
	$pdf->Cell(169,7,$_POST['cc'],'R',1,'L');

	$pdf->SetFont('helvetica','B',9);
	$pdf->Cell(35,7,'Disposición de activo:','L',0,'L');
	$pdf->SetFont('helvetica','I',9);
	$pdf->Cell(164,7,$_POST['disposicion'],'R',1,'L');

	$pdf->SetFont('helvetica','B',9);
	$pdf->Cell(20,7,'Responsable:','BL',0,'L');
	$pdf->SetFont('helvetica','I',9);
	$pdf->Cell(179,7,$_POST['responsable'],'BR',1,'L');

	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
	$sqlr = "SELECT foto FROM acticrearact_det  WHERE placa = '".$_POST['placa']."'";
	$res = mysqli_query($linkbd,$sqlr);
	$row = mysqli_fetch_row($res);
	if($row[0] != ''){
		$nomfoto = $row[0];
		$imagePath = 'informacion/proyectos/temp/' . $nomfoto;
	}else{
		$nomfoto = 'nofoto.jpg';
		$imagePath = 'imagenes/' . $nomfoto;
	}

	$rectX = 130;
	$rectY = 44;
	$rectWidth = 79;
	$rectHeight = 84;

	$imageX = 120;
	$imageY = 44.2;
	$imageWidth = 25;
	$imageHeight = 23.9;

	list($imgWidth, $imgHeight) = getimagesize($imagePath);
	$scaleX = $rectWidth / $imgWidth;
	$scaleY = $rectHeight / $imgHeight;
	$scale = min($scaleX, $scaleY);
	$newWidth = $imgWidth * $scale;
	$newHeight = $imgHeight * $scale;
	$newX = $rectX + ($rectWidth - $newWidth) / 2;
	$newY = $rectY + ($rectHeight - $newHeight) / 2;
	$pdf->Image($imagePath, $newX, $newY, $newWidth, $newHeight, 'JPG', '', 'T', true, 300, '', false, false, 0, false, false, false);
	$pdf->Rect($rectX, $rectY, $rectWidth, $rectHeight);

	$pdf->Output('solicitudcdp.pdf', 'I');
?>