<?php
header("Cache-control: no-cache, no-store, must-revalidate");
header("Content-Type: text/html;charset=utf8");

require 'comun.inc';
require 'funciones.inc';
require "conversor.php";

$linkbd = conectar_v7();
$linkbd->set_charset("utf8");

session_start();
date_default_timezone_set("America/Bogota");

?>
<!DOCTYPE>
<html lang=es>

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1" />
    <meta name="viewport" content="user-scalable=no">
    <title>:: IDEAL 10 - Presupuesto</title>
    <link href="favicon.ico" rel="shortcut icon" />
    <link href="css/css2.css?<?php echo date('d_m_Y_h_i_s'); ?>" rel="stylesheet" type="text/css">
    <link href="css/css3.css?<?php echo date('d_m_Y_h_i_s'); ?>" rel="stylesheet" type="text/css">
    <link href="css/css4.css?<?php echo date('d_m_Y_h_i_s'); ?>" rel="stylesheet" type="text/css">
    <link href="css/tabs.css?<?php echo date('d_m_Y_h_i_s'); ?>" rel="stylesheet" type="text/css">
    <link href="css/style.css?<?php echo date('d_m_Y_h_i_s'); ?>" rel="stylesheet" type="text/css">
    <script type="text/javascript" src="css/programas.js?<?php echo date('d_m_Y_h_i_s'); ?>"></script>
    <script type="text/javascript" src="css/calendario.js?<?php echo date('d_m_Y_h_i_s'); ?>"></script>
    <script type="text/javascript" src="JQuery/jquery-2.1.4.min.js?<?php echo date('d_m_Y_h_i_s'); ?>"></script>
    <script src="sweetalert2/dist/sweetalert2.min.js"></script>
    <link rel="stylesheet" href="sweetalert2/dist/sweetalert2.min.css">
    <script>
        function guardar() {
            if (document.form2.vigencia.value != '' && document.form2.fecha.value != '' && document.form2.solicita.value != '') {
                Swal.fire({
                    icon: 'question',
                    title: '¿Seguro que quieres guardar la información?',
                    showDenyButton: true,
                    confirmButtonText: 'Guardar',
                    denyButtonText: 'Cancelar',
                }).then(
                    (result) => {
                        if (result.isConfirmed) {
                            document.form2.oculto.value = 2;
                            document.form2.submit();
                        }
                        else if (result.isDenied) {
                            Swal.fire('No se guardo la información', '', 'info');
                        }
                    }
                )
            } else {
                Swal.fire(
                    'Error!',
                    'Faltan datos para completar el registro',
                    'error'
                );
                document.form2.fecha.focus();
                document.form2.fecha.select();
            }
        }
        function validar(formulario) {
            var x = document.getElementById("tipomov").value;
            document.form2.movimiento.value = x;
            document.form2.action = "ccp-cdpVisualizar.php";
            document.form2.submit();
        }
        function pdf() {
            document.form2.action = "cdpPdf.php";
            document.form2.target = "_BLANK";
            document.form2.submit();
            document.form2.action = "";
            document.form2.target = "";
        }
        function pdf2() {
            document.form2.action = "ccp-cdpPdf";
            document.form2.target = "_BLANK";
            document.form2.submit();
            document.form2.action = "";
            document.form2.target = "";
        }
        function capturaTecla(e) {
            var tcl = (document.all) ? e.keyCode : e.which;
            if (tcl == 115) {
                alert(tcl);
                return tabular(e, elemento);
            }
        }
        function adelante() {
            if (parseFloat(document.form2.ncomp.value) < parseFloat(document.form2.maximo.value)) {
                document.form2.oculto.value = 1;
                document.form2.ncomp.value = parseFloat(document.form2.ncomp.value) + 1;
                document.form2.idcomp.value = parseFloat(document.form2.idcomp.value) + 1;
                document.form2.action = "ccp-cdpVisualizar.php";
                document.form2.submit();
            }
        }
        function atrasc() {
            if (document.form2.ncomp.value > 1) {
                document.form2.oculto.value = 1;
                document.form2.ncomp.value = document.form2.ncomp.value - 1;
                document.form2.idcomp.value = document.form2.idcomp.value - 1;
                document.form2.action = "ccp-cdpVisualizar.php";
                document.form2.submit();
            }
        }
        function validar2() {
            document.form2.oculto.value = 1;
            document.form2.ncomp.value = document.form2.idcomp.value;
            document.form2.action = "ccp-cdpVisualizar.php";
            document.form2.submit();
        }
        function anular() {
            Swal.fire({
                title: 'ANULAR CDP:',
                input: 'text',
                inputLabel: '¿por que desea anular el CDP?',
                width: 500,
                inputAttributes: {
                    autocapitalize: 'off',
                    step: 'any'
                },
                showCancelButton: true,
                confirmButtonColor: '#01CC42',
                cancelButtonColor: '#FF121A',
                confirmButtonText: 'Aceptar',
                cancelButtonText: 'Cancelar',
                showLoaderOnConfirm: true,
                preConfirm: (observacion) => {
                    let digitos = observacion.length;
                    if (digitos > 10) {
                        procesoanular(observacion);
                    } else {
                        Swal.showValidationMessage(
                            `La descripción debe contener por lo menos 10 caracteres`
                        )
                    }

                },
            });
        }
        function procesoanular(observacion) {
            document.form2.oculto.value = 3;
            document.form2.obanular.value = observacion;
            document.form2.submit();
        }
        var ctrlPressed = false;
        var tecla01 = 17, tecla02 = 37, tecla03 = 39;
        $(document).keydown(function (e) {
            if (e.keyCode == tecla01) { ctrlPressed = true; }
            if (ctrlPressed && (e.keyCode == tecla02)) {
                atrasc();
            }
        });
        $(document).keydown(function (e) {
            if (ctrlPressed && (e.keyCode == tecla03)) {
                adelante();
            }
        });
        $(document).keyup(function (e) {
            if (e.keyCode == tecla01) { ctrlPressed = false; }
        });
    </script>

</head>

<body>
    <IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
    <span id="todastablas2"></span>
    <table>
        <tr>
            <script>barra_imagenes("ccpet");</script><?php cuadro_titulos(); ?>
        </tr>
        <tr><?php menu_desplegable("ccpet"); ?></tr>
    </table>
    <div class="bg-white group-btn p-1">
        <button type="button" onclick="location.href='ccp-gestioncdp.php'" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
            <span>Nuevo</span>
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960">
                <path d="M440-440H200v-80h240v-240h80v240h240v80H520v240h-80v-240Z"></path>
            </svg>
        </button>
        <button type="button" onclick="guardar()" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
            <span>Guardar</span>
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960">
                <path
                    d="M840-680v480q0 33-23.5 56.5T760-120H200q-33 0-56.5-23.5T120-200v-560q0-33 23.5-56.5T200-840h480l160 160Zm-80 34L646-760H200v560h560v-446ZM480-240q50 0 85-35t35-85q0-50-35-85t-85-35q-50 0-85 35t-35 85q0 50 35 85t85 35ZM240-560h360v-160H240v160Zm-40-86v446-560 114Z">
                </path>
            </svg>
        </button>
        <button type="button" onclick="location.href='ccp-buscarcdp.php'" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
            <span>Buscar</span>
            <svg class="fill-black group-hover:fill-white w-5 h-5" xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960">
                <path d="M784-120 532-372q-30 24-69 38t-83 14q-109 0-184.5-75.5T120-580q0-109 75.5-184.5T380-840q109 0 184.5 75.5T640-580q0 44-14 83t-38 69l252 252-56 56ZM380-400q75 0 127.5-52.5T560-580q0-75-52.5-127.5T380-760q-75 0-127.5 52.5T200-580q0 75 52.5 127.5T380-400Z">
                </path>
            </svg>
        </button>
        <button type="button" onclick="window.open('ccp-principal');" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
            <span>Nueva ventana</span>
            <svg class="fill-black group-hover:fill-white w-5 h-5" xmlns="http://www.w3.org/2000/svg"
                viewBox="0 -960 960 960">
                <path
                    d="M320-120v-80h80v-80H160q-33 0-56.5-23.5T80-360v-400q0-33 23.5-56.5T160-840h640q33 0 56.5 23.5T880-760v400q0 33-23.5 56.5T800-280H560v80h80v80H320ZM160-360h640v-400H160v400Zm0 0v-400 400Z">
                </path>
            </svg>
        </button>
        <?php 
            $sqlr = "SELECT orden FROM configbasica";
            $resr = mysqli_query($linkbd, $sqlr);
            $rowr = mysqli_fetch_array($resr);
            $orden = $rowr['orden'];
            if ($orden == 'Dptal') {
                ?>
                    <button type="button" onclick="pdf()" class="btn btn-danger btn-danger-hover d-flex justify-between align-items-center">
            
                        <span>Exportar PDF</span>
                        <svg xmlns="http://www.w3.org/2000/svg"
                            viewBox="0 0 512 512">
                            <path d="M64 464l48 0 0 48-48 0c-35.3 0-64-28.7-64-64L0 64C0 28.7 28.7 0 64 0L229.5 0c17 0 33.3 6.7 45.3 18.7l90.5 90.5c12 12 18.7 28.3 18.7 45.3L384 304l-48 0 0-144-80 0c-17.7 0-32-14.3-32-32l0-80L64 48c-8.8 0-16 7.2-16 16l0 384c0 8.8 7.2 16 16 16zM176 352l32 0c30.9 0 56 25.1 56 56s-25.1 56-56 56l-16 0 0 32c0 8.8-7.2 16-16 16s-16-7.2-16-16l0-48 0-80c0-8.8 7.2-16 16-16zm32 80c13.3 0 24-10.7 24-24s-10.7-24-24-24l-16 0 0 48 16 0zm96-80l32 0c26.5 0 48 21.5 48 48l0 64c0 26.5-21.5 48-48 48l-32 0c-8.8 0-16-7.2-16-16l0-128c0-8.8 7.2-16 16-16zm32 128c8.8 0 16-7.2 16-16l0-64c0-8.8-7.2-16-16-16l-16 0 0 96 16 0zm80-112c0-8.8 7.2-16 16-16l48 0c8.8 0 16 7.2 16 16s-7.2 16-16 16l-32 0 0 32 32 0c8.8 0 16 7.2 16 16s-7.2 16-16 16l-32 0 0 48c0 8.8-7.2 16-16 16s-16-7.2-16-16l0-64 0-64z">
                            </path>
                        </svg>
                    </button>
                <?php
            }else{
                ?>
                    <button type="button" onclick="pdf2()" class="btn btn-danger btn-danger-hover d-flex justify-between align-items-center">
            
                        <span>Exportar PDF</span>
                        <svg xmlns="http://www.w3.org/2000/svg"
                            viewBox="0 0 512 512">
                            <path d="M64 464l48 0 0 48-48 0c-35.3 0-64-28.7-64-64L0 64C0 28.7 28.7 0 64 0L229.5 0c17 0 33.3 6.7 45.3 18.7l90.5 90.5c12 12 18.7 28.3 18.7 45.3L384 304l-48 0 0-144-80 0c-17.7 0-32-14.3-32-32l0-80L64 48c-8.8 0-16 7.2-16 16l0 384c0 8.8 7.2 16 16 16zM176 352l32 0c30.9 0 56 25.1 56 56s-25.1 56-56 56l-16 0 0 32c0 8.8-7.2 16-16 16s-16-7.2-16-16l0-48 0-80c0-8.8 7.2-16 16-16zm32 80c13.3 0 24-10.7 24-24s-10.7-24-24-24l-16 0 0 48 16 0zm96-80l32 0c26.5 0 48 21.5 48 48l0 64c0 26.5-21.5 48-48 48l-32 0c-8.8 0-16-7.2-16-16l0-128c0-8.8 7.2-16 16-16zm32 128c8.8 0 16-7.2 16-16l0-64c0-8.8-7.2-16-16-16l-16 0 0 96 16 0zm80-112c0-8.8 7.2-16 16-16l48 0c8.8 0 16 7.2 16 16s-7.2 16-16 16l-32 0 0 32 32 0c8.8 0 16 7.2 16 16s-7.2 16-16 16l-32 0 0 48c0 8.8-7.2 16-16 16s-16-7.2-16-16l0-64 0-64z">
                            </path>
                        </svg>
                    </button>
                <?php
            }
            ?>
        
        <button type="button" onclick="location.href='ccp-buscarcdp.php'" class="btn btn-success btn-success-hover d-flex justify-between align-items-center">
            <span>Atras</span>
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960">
                <path
                    d="M760-200v-160q0-50-35-85t-85-35H273l144 144-57 56-240-240 240-240 57 56-144 144h367q83 0 141.5 58.5T840-360v160h-80Z">
                </path>
            </svg>
        </button>
    </div>
    <?php
    $vigusu = vigencia_usuarios($_SESSION['cedulausu']);
    if ($_GET['vig'] != '') {
        $vigusu = $_GET['vig'];
    }
    //***************PARTE PARA INSERTAR Y ACTUALIZAR LA INFORMACION
    $oculto = $_POST['oculto'];
    $codMovimiento = '201';
    if (isset($_POST['movimiento'])) {
        if (!empty($_POST['movimiento'])) {
            $codMovimiento = $_POST['movimiento'];
        }
    }
    if (!$_POST['oculto'] || $_POST['oculto'] == '1') {
        $_POST['vigencia'] = $vigusu;
        if ($_GET['is']) {
            $_POST['ncomp'] = $_GET['is'];
            $_POST['idcomp'] = $_GET['is'];
        }
        $sqlr = "SELECT consvigencia FROM  ccpetcdp WHERE vigencia = '$vigusu' AND tipo_mov='$codMovimiento' ORDER BY consvigencia DESC";
        $res = mysqli_query($linkbd, $sqlr);
        $r = mysqli_fetch_row($res);
        $_POST['maximo'] = $r[0];
        $_POST['solicita'] = "";
        $_POST['objeto'] = "";
        $_POST['estadoc'] = "";
        $sqlr = "SELECT DISTINCT * FROM ccpetcdp WHERE ccpetcdp.vigencia = '$vigusu' AND ccpetcdp.consvigencia = " . $_POST['ncomp'] . " AND ccpetcdp.tipo_mov = '$codMovimiento' ";
        $res = mysqli_query($linkbd, $sqlr);
        $_POST['agregadet'] = '';
        $cont = 0;
        while ($row = mysqli_fetch_row($res)) {
            $_POST['vigencia'] = $row[1];
            $_POST['estado'] = $row[5];
            switch ($row[5]) {
                case 'S':
                    $_POST['estadoc'] = 'ACTIVO';
                    $color = " style='background-color:#58afb8 ;color:#fff'";
                    break;
                case 'C':
                    $_POST['estadoc'] = 'COMPLETO';
                    $color = " style='background-color:#00CCFF ; color:#fff'";
                    break;
                case 'N':
                    $_POST['estadoc'] = 'ANULADO';
                    $color = " style='background-color:#aa0000 ; color:#fff'";
                    break;
                case 'R':
                    $_POST['estadoc'] = 'REVERSADO';
                    $color = " style='background-color:#aa0000 ; color:#fff'";
                    break;
            }
            $p1 = substr($row[3], 0, 4);
            $p2 = substr($row[3], 5, 2);
            $p3 = substr($row[3], 8, 2);
            $_POST['fecha'] = $row[3];
            preg_match("/([0-9]{4})\-([0-9]{2})\-([0-9]{2})/", $_POST['fecha'], $fecha);
            $_POST['fecha'] = $fecha[3] . "/" . $fecha[2] . "/" . $fecha[1];
            $_POST['solicita'] = $row[6];
            $_POST['objeto'] = $row[7];
            $_POST['numero'] = $row[2];
            $_POST['user'] = $row[10];
        }
        $_POST['dtipoGasto'] = array();
        $_POST['dsecPresupuestal'] = array();
        $_POST['dmedioPago'] = array();
        $_POST['dvigGasto'] = array();
        $_POST['dcuenta'] = array();
        $_POST['dprogramatico'] = array();
        $_POST['dfuente'] = array();
        $_POST['dproductoservicio'] = array();
        $_POST['ddivipola'] = array();
        $_POST['dchip'] = array();
        $_POST['dbpim'] = array();
        $_POST['dvalor'] = array();
        $sqlr = "SELECT DISTINCT * FROM ccpetcdp_detalle WHERE ccpetcdp_detalle.consvigencia = $_POST[ncomp] AND ccpetcdp_detalle.vigencia='" . $vigusu . "' AND ccpetcdp_detalle.tipo_mov = '$codMovimiento' ORDER BY CUENTA ";
        $res = mysqli_query($linkbd, $sqlr);
        $_POST['agregadet'] = '';
        $cont = 0;
        while ($row = mysqli_fetch_assoc($res)) {
            $_POST['dtipoGasto'][$cont] = $row['tipo_gasto'];
            $sqlSeccionPresupuestal = "SELECT nombre FROM pptoseccion_presupuestal WHERE (id_seccion_presupuestal = '" . intVal($row['seccion_presupuestal']) . "' OR id_seccion_presupuestal = '$row[seccion_presupuestal]')";
            $resSeccionPresupuestal = mysqli_query($linkbd, $sqlSeccionPresupuestal);
            $rowSeccionPresupuestal = mysqli_fetch_row($resSeccionPresupuestal);
            $_POST['dsecPresupuestal'][$cont] = $row['seccion_presupuestal'] . ' - ' . $rowSeccionPresupuestal[0];
            $_POST['dmedioPago'][$cont] = $row['medio_pago'];
            $sqlVigencia = "SELECT nombre FROM ccpet_vigenciadelgasto WHERE codigo = '$row[codigo_vigenciag]'";
            $resVigencia = mysqli_query($linkbd, $sqlVigencia);
            $rowVigencia = mysqli_fetch_row($resVigencia);
            $_POST['dvigGasto'][$cont] = $row['codigo_vigenciag'] . ' - ' . $rowVigencia[0];
            $sqlCuenta = "SELECT nombre FROM cuentasccpet WHERE codigo = '$row[cuenta]'";
            $resCuenta = mysqli_query($linkbd, $sqlCuenta);
            $rowCuenta = mysqli_fetch_row($resCuenta);
            $_POST['dcuenta'][$cont] = $row['cuenta'] . ' - ' . $rowCuenta[0];
            $_POST['dbpim'][$cont] = $row['bpim'];

            $sqlrNomProy = "SELECT nombre FROM ccpproyectospresupuesto WHERE codigo = '" . $row['bpim'] . "' AND vigencia = '" . $row['vigencia'] . "'";
            $respNomProy = mysqli_query($linkbd, $sqlrNomProy);
            $rowNomProy = mysqli_fetch_row($respNomProy);
            $_POST['nombreProyecto'][$cont] = $rowNomProy[0];

            $sqlrIndPro = "SELECT producto FROM ccpetproductos WHERE codigo_indicador = '" . $row['indicador_producto'] . "'";
            $respIndPro = mysqli_query($linkbd, $sqlrIndPro);
            $rowNomIndPro = mysqli_fetch_row($respIndPro);
            $_POST['indpro'][$cont] = $rowNomIndPro[0];

            $nomCpc = '';

            $primerDigitoCpc = substr($row['productoservicio'], 0, 1);
            if ($primerDigitoCpc < 5) {
                $sqlrBienesT = "SELECT titulo FROM ccpetbienestransportables WHERE grupo = '$row[productoservicio]'";
                $respBienesT = mysqli_query($linkbd, $sqlrBienesT);
                $rowBienesT = mysqli_fetch_row($respBienesT);
                $nomCpc = $rowBienesT[0];
            } else {
                $sqlrServicio = "SELECT titulo FROM ccpetservicios WHERE grupo = '$row[productoservicio]'";
                $respServicio = mysqli_query($linkbd, $sqlrServicio);
                $rowServicio = mysqli_fetch_row($respServicio);
                $nomCpc = $rowServicio[0];
            }


            /* if ($_POST['dproductoservicio'][0] < 4){

                               $sqlcp = "SELECT * FROM `ccpetbienestransportables`";
                               $rescp = mysqli_query($linkbd,$sqlcp);
                               $rowcp = mysqli_fetch_row($rescp);
                               $_POST['dproductoservicio'][$cont] = $rowcp;
                               echo $rowcp;
                           }
                           else {
                               $sqlcp = "SELECT * FROM 'ccpetservicios'";
                           } */

            $_POST['nomCpc'][$cont] = $nomCpc;

            $_POST['dprogramatico'][$cont] = $row['indicador_producto'];
            $_POST['dproductoservicio'][$cont] = $row['productoservicio'];
            $_POST['ddivipola'][$cont] = $row['divipola'];
            $_POST['dchip'][$cont] = $row['chip'];
            $sqlFuente = "SELECT nombre FROM ccpet_fuentes_cuipo WHERE codigo_fuente = '$row[fuente]' AND  LENGTH(codigo_fuente) > 6";
            $resFuente = mysqli_query($linkbd, $sqlFuente);
            $rowFuente = mysqli_fetch_row($resFuente);
            $_POST['dfuente'][$cont] = $row['fuente'] . ' - ' . $rowFuente[0];
            $_POST['dvalor'][$cont] = $row['valor'];
            $cont++;
        }
        $sqlrp = "SELECT valor FROM ccpetrp WHERE estado <> 'R' AND estado <> 'N' AND tipo_mov = '201' AND vigencia = '$vigusu' AND idcdp = '" . $_POST['ncomp'] . "'";
        $resrp = mysqli_query($linkbd, $sqlrp);
        $_POST['numrp'] = mysqli_num_rows($resrp);
    }
    ?>
    <form name="form2" method="post" action="">
        <table class="inicio ancho">
            <tr>
                <td class="titulos" colspan="10">.: Certificado Disponibilidad Presupuestal </td>
            </tr>
            <tr>
                <td style="width:9%;" class="saludo1">N&uacute;mero:</td>
                <input type="hidden" name="obanular" id="obanular" value="<?php echo $_POST['obanular'] ?>" />
                <input type="hidden" name="cuentacaja" value="<?php echo $_POST['cuentacaja'] ?>" />
                <input type="hidden" name="ncomp" value="<?php echo $_POST['ncomp'] ?>" />
                <input type="hidden" name="numrp" id='numrp' value="<?php echo $_POST['numrp'] ?>" />
                <input type="hidden" name="atras" value="a" />
                <input type="hidden" value="s" name="siguiente" />
                <input type="hidden" value="<?php echo $_POST['maximo'] ?>" name="maximo" />
                <input name="numero" type="hidden" id="numero" value="<?php echo $_POST['numero'] ?>" readonly />
                <input name="user" type="hidden" id="user" value="<?php echo $_POST['user'] ?>" readonly />
                <td style="width:15%;"><img src="imagenes/back.png" title="Anterior" onClick="atrasc()"
                        class="icobut">&nbsp;<input type="text" name="idcomp" value="<?php echo $_POST['idcomp'] ?>"
                        onKeyUp="return tabular(event,this) " style="width:50%;" onBlur="validar2()" />&nbsp;<img
                        src="imagenes/next.png" title="Siguiente" onClick="adelante()" class="icobut" /></td>
                <td style="width:9%;" class="saludo1">Vigencia:</td>
                <td style="width:10%;"><input style="width:100%;" type="text" name="vigencia"
                        value="<?php echo $_POST['vigencia'] ?>" readonly></td>
                <td class="saludo1" style="width:9%;">Fecha:</td>
                <td style="width:12%;"><input name="fecha" type="text" id="fc_1198971545" title="DD/MM/YYYY"
                        value="<?php echo $_POST['fecha']; ?>" onKeyUp="return tabular(event,this)"
                        onKeyDown="mascara(this,'/',patron,true)" maxlength="10" style="width:80%;" readonly>&nbsp;</a>
                </td>
                <input type="hidden" name="chacuerdo" value="1">
                <td class="saludo1">Estado</td>
                <td>
                    <input name="estadoc" type="text" id="estadoc" value="<?php echo $_POST['estadoc'] ?>" <?php echo $color; ?> readonly>
                </td>
                <td>
                    <select name="tipomov" id="tipomov" onKeyUp="return tabular(event,this)" onChange="validar()"
                        style="float:right">
                        <?php
                        $codMovimiento = '201';
                        if (isset($_POST['movimiento'])) {
                            if (!empty($_POST['movimiento'])) {
                                $codMovimiento = $_POST['movimiento'];
                            }
                        }
                        $sql = "SELECT tipo_mov FROM ccpetcdp where consvigencia=$_POST[ncomp] AND vigencia='$vigusu' ORDER BY tipo_mov";
                        $resultMov = mysqli_query($linkbd, $sql);
                        $movimientos = array();
                        $movimientos["201"]["nombre"] = "201-Documento de Creacion";
                        $movimientos["201"]["estado"] = "";
                        $movimientos["401"]["nombre"] = "401-Reversion Total";
                        $movimientos["401"]["estado"] = "";
                        $movimientos["402"]["nombre"] = "402-Reversion Parcial";
                        $movimientos["402"]["estado"] = "";
                        while ($row = mysqli_fetch_row($resultMov)) {
                            $mov = $movimientos[$row[0]]["nombre"];
                            $movimientos[$codMovimiento]["estado"] = "selected";
                            $state = $movimientos[$row[0]]["estado"];
                            echo "<option value='$row[0]' $state>$mov</option>";
                        }
                        $movimientos[$codMovimiento]["estado"] = "";
                        echo "<input type='hidden' id='movimiento' name='movimiento' value='$_POST[movimiento]' />";
                        ?>
                    </select>
                    <?php
                    if (($_POST['numrp'] == 0) && ($_POST['estadoc'] == 'ACTIVO')) {

                        echo "<td><em class='botonflecharoja' onClick='anular();'>Anular</em></td>";

                    }
                    ?>
                    <input name="estado" type="hidden" id="estado" value="<?php echo $_POST['estado'] ?>">
                </td>
            </tr>
            <tr>
                <td class="saludo1"><input type="hidden" value="1" name="oculto">Solicita:</td>
                <?php
                if (($_POST['numrp'] == 0) && ($_POST['estadoc'] == 'ACTIVO')) {
                    echo "
							<td colspan='8'>
                                <input type='text' name='solicita' id='solicita' onKeyUp='return tabular(event,this)' style='width:100%;' value='" . $_POST['solicita'] . "'>
                            </td>";

                } else {
                    echo "
							<td colspan='8'><input type='text' name='solicita' id='solicita' onKeyUp='return tabular(event,this)' style='width:100%;' value='" . $_POST['solicita'] . "'></td>";
                }
                ?>
            </tr>
            <tr>
                <td class="saludo1">Objeto:</td>
                <td colspan="8">
                    <textarea name="objeto" id="objeto" style="width:100%; form-sizing: content; max-height: 100px; min-height: 20px; resize: vertical; " class="estilos-scroll"><?php echo htmlspecialchars($_POST['objeto'])?></textarea>
                    
                </td>
            </tr>
            <tr>
                <!-- <td class="saludo1">Versión TRD:</td>
                <td>
                    <input type="text" name="versionTRD" id="versionTRD" value="<?php echo $_POST['versionTRD'] ?>"
                        style="width:100%;">
                </td>
                <td class="saludo1">Fecha TRD:</td>
                <td style="width:10%;"><input type="text" name="fechaTRD" id="fc_1198971546" title="DD/MM/YYYY"
                        value="<?php echo $_POST['fechaTRD']; ?>" onKeyUp="return tabular(event,this)"
                        onKeyDown="mascara(this,'/',patron,true)" maxlength="10" style="width:100%;" onChange=""
                        class="colordobleclik" onDblClick="displayCalendarFor('fc_1198971546');" autocomplete="off">
                </td>
                <td class="saludo1">Código TRD:</td>
                <td>
                    <input type="text" name="codigoTRD" id="codigoTRD" value="<?php echo $_POST['codigoTRD'] ?>">
                </td> -->

            </tr>
        </table>
        <?php
        if (!$_POST['oculto']) {
            echo "<script>document.form2.fecha.focus();</script>";
        }
        //**** busca cuenta
        if ($_POST['bc'] != '') {
            $nresul = buscacuentaccpetgastos($_POST['cuenta']);
            if ($nresul != '') {
                $_POST['ncuenta'] = $nresul;
                echo "
						<script>
							document.getElementById('valor').focus();
							document.getElementById('valor').select();
						</script>";
            } else {
                $_POST['ncuenta'] = "";
                echo " <script>alert('Cuenta Incorrecta');document.form2.cuenta.focus();</script>";
            }
        }
        ?>

        <!-- TABLA CDP -->
        <div class="subpantalla" style="height:50%; width:99.6%; overflow-x:hidden;">
            <table class="inicio" width="99%">
                <tr>
                    <td class="titulos" colspan="11">Detalle CDP</td>
                </tr>
                <tr style="text-align: center;">
                    <!-- <td class="titulos2">Tipo de Gasto</td> -->
                    <td class="titulos2">Dependencia</td>
                    <td class="titulos2">Medio de Pago</td>
                    <td class="titulos2">Vig. de Gasto</td>
                    <td class="titulos2">Proyecto</td>
                    <td class="titulos2">Program&aacute;tico</td>
                    <td class="titulos2">Cuenta CCPET</td>
                    <td class="titulos2">Fuente</td>
                    <td class="titulos2">CPC</td>
                    <!-- <td class="titulos2">Divipola</td>
                    <td class="titulos2">CHIP</td> -->
                    <td class="titulos2">Valor</td>
                </tr>
                <?php
                $iter1 = 'saludo1a';
                $iter2 = 'saludo2';
                for ($x = 0; $x < count($_POST['dcuenta']); $x++) {
                    echo "
							<input type='hidden' name='dtipoGasto[]' value='" . $_POST['dtipoGasto'][$x] . "'/>
							<input type='hidden' name='dsecPresupuestal[]' value='" . $_POST['dsecPresupuestal'][$x] . "'/>
							<input type='hidden' name='dmedioPago[]' value='" . $_POST['dmedioPago'][$x] . "'/>
							<input type='hidden' name='dvigGasto[]' value='" . $_POST['dvigGasto'][$x] . "'/>
							<input type='hidden' name='dbpim[]' value='" . $_POST['dbpim'][$x] . "'/>
							<input type='hidden' name='nombreProyecto[]' value='" . $_POST['nombreProyecto'][$x] . "'/>
							<input type='hidden' name='indpro[]' value='" . $_POST['indpro'][$x] . "'/>
							<input type='hidden' name='dprogramatico[]' value='" . $_POST['dprogramatico'][$x] . "'/>
							<input type='hidden' name='dcuenta[]' value='" . $_POST['dcuenta'][$x] . "'/>
							<input type='hidden' name='dfuente[]' value='" . $_POST['dfuente'][$x] . "'/>
							<input type='hidden' name='dproductoservicio[]' value='" . $_POST['dproductoservicio'][$x] . "'/>
							<input type='hidden' name='nomCpc[]' value='" . $_POST['nomCpc'][$x] . "'/>
							<input type='hidden' name='ddivipola[]' value='" . $_POST['ddivipola'][$x] . "'/>
							<input type='hidden' name='dchip[]' value='" . $_POST['dchip'][$x] . "'/>
							<input type='hidden' name='dvalor[]' value='" . $_POST['dvalor'][$x] . "'/>
							<tr style='text-align:center;' class=$iter1>
								<td style='width:10%;></td>
								<td style='width:15%;'>" . $_POST['dsecPresupuestal'][$x] . "</td>
								<td style='width:5%;'>" . $_POST['dmedioPago'][$x] . "</td>
								<td style='width:5%;'>" . $_POST['dvigGasto'][$x] . "</td>
								<td style='width:5%;'>" . $_POST['dbpim'][$x] . " - " . $_POST['nombreProyecto'][$x] . "</td>
								<td style='width:5%;'>" . $_POST['dprogramatico'][$x] . ' - ' . $_POST['indpro'][$x] . "</td>
								<td style='width:20%;'>" . $_POST['dcuenta'][$x] . "</td>
								<td style='width:10%;'>" . $_POST['dfuente'][$x] . "</td>
								<td style='width:20%;'>" . $_POST['dproductoservicio'][$x] . " - " . $_POST['nomCpc'][$x] . "</td>
								<td style='width:10%;text-align:right;'>" . number_format($_POST['dvalor'][$x], 2, $_SESSION["spdecimal"], $_SESSION["spmillares"]) . "</td>
							</tr>";
                    $gas = $_POST['dvalor'][$x];
                    $aux = $iter1;
                    $iter1 = $iter2;
                    $iter2 = $aux;
                    $gas = $gas;
                    $cuentagas = $cuentagas + $gas;
                    $_POST['cuentagas2'] = $cuentagas;
                    $total = number_format($total, 2, $_SESSION["spdecimal"], "");
                    $_POST['cuentagas'] = number_format($cuentagas, 2, $_SESSION["spdecimal"], $_SESSION["spmillares"]);
                    $resultado = convertir($_POST['cuentagas2']);
                    $_POST['letras'] = $resultado . " Pesos";
                }
                echo "
						<input type='hidden' name='cuentagas' id='cuentagas' value='$_POST[cuentagas]'/>
						<input type='hidden' name='cuentagas2' id='cuentagas2' value='$_POST[cuentagas2]'/>
						<input type='hidden' name='letras' id='letras' value='$_POST[letras]'/>
						<tr class=$iter1>
							<td colspan='8' style='text-align:right;'>Total:</td>
							<td style='text-align:right;'>$_POST[cuentagas]</td>
						</tr>
						<tr>
							<td class='saludo1'>Son:</td>
							<td class='saludo1' colspan= '11'>$resultado</td>
						</tr>";
                if ($_POST['oculto'] == '2') {
                    $sqlr = "UPDATE ccpetcdp SET objeto = '" . $_POST['objeto'] . "' WHERE vigencia = '" . $_POST['vigencia'] . "' AND consvigencia = '" . $_POST['idcomp'] . "' AND tipo_mov = '" . $_POST['tipomov'] . "'";
                    mysqli_query($linkbd, $sqlr);
                }
                if ($_POST['oculto'] == '3') {
                    $sqlr = "UPDATE ccpetcdp SET estado = 'N' WHERE vigencia = '" . $_POST['vigencia'] . "' AND consvigencia = '" . $_POST['idcomp'] . "' AND tipo_mov = '" . $_POST['tipomov'] . "'";
                    if (!mysqli_query($linkbd, $sqlr)) {
                        echo "<script>Swal.fire('No se pudo anular', '', 'error');</script>
								";
                    } else {
                        $sqldet = "UPDATE ccpetcdp_detalle SET valor = '0' WHERE vigencia = '" . $_POST['vigencia'] . "' AND consvigencia = '" . $_POST['idcomp'] . "'";
                        mysqli_query($linkbd, $sqldet);
                        cargartrazas('presu', 'ccpetcdp', 'anular', $_POST['idcomp'], '0', '0', $_POST['vigencia'], $_POST['obanular'], $sqlr);
                        cargartrazas('presu', 'ccpetcdp_detalle', 'anular', $_POST['idcomp'], '0', '0', $_POST['vigencia'], $_POST['obanular'], $sqldet);
                        $sqlunion = "SELECT nomina FROM hum_nom_cdp_rp WHERE vigencia = '$vigusu' AND cdp = '" . $_POST['idcomp'] . "'";
                        $resunion = mysqli_query($linkbd, $sqldet);
                        $rowunion = mysqli_num_rows($resunion);
                        if ($rowunion[0] != '') {
                            $sqluni = "UPDATE hum_nom_cdp_rp SET cdp = '0' WHERE nomina = '$rowunion[0]'";
                            mysqli_query($linkbd, $sqluni);
                            cargartrazas('hum', 'hum_nom_cdp_rp', 'anular', $rowunion[0], '0', '0', $_POST['vigencia'], $_POST['obanular'], $sqldet);
                        }
                        echo "
								<script>
									Swal.fire({
										icon: 'success',
										title: 'Se anulo con exito el CDP Nº " . $_POST['idcomp'] . "',
										showConfirmButton: false,
										timer: 2500
									}).then((response) => {
										validar2();
									});
								</script>";
                    }
                }
                ?>
            </table>
        </div>
    </form>
</body>

</html>
