<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require 'comun.inc';
	require 'funciones.inc';
	session_start();
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html lang=es>
	<head>
    <meta charset="utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1"/>
		<meta name="viewport" content="user-scalable=no">
		<title>:: IDEAL 10 - Almacen</title>
		<link href="favicon.ico" rel="shortcut icon"/>
		<link href="css/css2z.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
        <link href="css/tabs.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" href="sweetalert2/dist/sweetalert2.min.css">
        <script type="text/javascript" src="css/programas.js"></script>
        <script src="sweetalert2/dist/sweetalert2.min.js"></script>
		<script src="vue/vue.min.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script src="css/calendario.js?<?php echo date('d_m_Y_h_i_s');?>"></script>

		<style>
			input[type=number]::-webkit-inner-spin-button,
			input[type=number]::-webkit-outer-spin-button { 
				-webkit-appearance: none; 
				margin: 0; 
			}
			input[type=number]{
				text-align:right;
				box-sizing: border-box;
				font: 120% sans-serif;
				/* width: 100% !important; */
			}
			[v-cloak]{display : none;}

			.centrarSelect {

				height: 30px !important;
				text-align-last:center !important;
			}

			.aumentarTamaño {

				font-size:15px;
			}

            .tamano01 {
                text-align: center !important;
            }

            SELECT{
                font-size:11px;
                font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, "Helvetica Neue", Arial, "Noto Sans", sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol", "Noto Color Emoji";
                font-weight:bold;
                outline:none;
                border-radius:3px;
                border:1px solid rgba(0,0,0, 0.2);
                color:222;
                /* background-color:#B2BFD9; */
            }
            option{
                font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, "Helvetica Neue", Arial, "Noto Sans", sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol", "Noto Color Emoji"; 
                font-size: 10px; 
                color: 333;
                /* background-color:#eee; */
                color:#222;
            }
		</style>
	</head>
	<body>
		<header>
			<table>
				<tr><script>barra_imagenes("inve");</script><?php cuadro_titulos();?></tr>
			</table>
		</header>

		<form name="form2" method="post" action="">
			<section id="myapp" v-cloak >
				<nav>
					<table>
						<tr><?php menu_desplegable("inve");?></tr>
						<tr>
							<td colspan="3" class="cinta">
								<img src="imagenes/add.png" v-on:click="location.href='inve-crearInventarioExtracontable'" class="mgbt" title="Nuevo">
								<img src="imagenes/guarda.png" @click="guardar" title="Guardar"  class="mgbt">
								<img src="imagenes/busca.png" v-on:click="location.href='inve-buscarInventarioExtracontable'" class="mgbt" title="Buscar">
								<img src="imagenes/nv.png" onClick="mypop=window.open('inve-principal','',''); mypop.focus();" class="mgbt" title="Nueva Ventana">
							</td>
						</tr>
					</table>
				</nav>

				<article>
					<div>
						<table class="inicio">
							<tr>
								<td class="titulos" colspan="8">.: Inventario extracontable</td>
                            	<td class="cerrar" style="width:4%" onClick="location.href='inve-principal.php'">Cerrar</td>
							</tr>
						</table>
					</div>

                    <div>
                        <table class="inicio">
                            <tr>
                                <td class="textonew01" style="width:3.5cm;">.: Articulo: </td>
                                <td style="width: 11%;">
                                    <input type="text" v-model="codArticulo" @dblclick="despliegaModalArticulos" class="colordobleclik" readonly>
                                </td>
                                <td colspan="6">
                                    <input type="text" v-model="nomArticulo" style="width: 90%;" readonly>
                                </td>
                            </tr>

                            <tr>
                                <td class="textonew01" style="width:3.5cm;">.: Unidad de medida:</td>
                                <td style="width: 11%;">
                                    <input type="text" v-model="unidadMedida" style="text-align: center;" readonly>
                                </td>

                                <td class="textonew01" style="width:3.5cm;">.: Dependencia:</td>
								<td style="width: 11%;">
									<select v-model="dependencia" style="width: 90%; font-size: 10px; margin-top:4px">
										<option value="">Seleccionar dependencia</option>
										<option v-for="depen in dependencias" v-bind:value="depen">
											{{ depen[0] }} - {{ depen[1] }}
										</option>
									</select>
								</td>

                                <td class="textonew01" style="width:2.5cm;">.: Fecha compra:</td>
                                <td style="width: 11%;">
                                    <input type="text" name="fechaCompra"  value="<?php echo $_POST['fechaCompra']?>" onKeyUp="return tabular(event,this)" id="fechaCompra" title="DD/MM/YYYY" onclick="displayCalendarFor('fechaCompra');" class="colordobleclik" autocomplete="off" onChange=""  readonly>
                                </td>
                            </tr>

                            <tr>
                                <td class="textonew01" style="width:3.5cm;">.: Tercero:</td>
                                <td style="width: 11%;">
                                    <input type="text" v-model="tercero" @dblclick="despliegaModalTerceros" class="colordobleclik" readonly>
                                </td>

                                <td colspan="6">
                                    <input type="text" v-model="nomTercero" style="width:90%" readonly>
                                </td>
                            </tr>

                            <tr>
                                <td class="textonew01" style="width:3.5cm;">.: Cantidad:</td>
                                <td style="width: 11%;">
                                    <input type="number" v-model="cantidad" style="text-align: center;" @change="validaCantidad(cantidad)">
                                </td>

                                <td class="textonew01" style="width:3.5cm;">.: Valor unidad: </td>
                                <td style="width: 11%;">
                                    <input type="number" v-model="valorUnidad" style="text-align: center;" @change="validaValorUnidad(valorUnidad)">
                                </td>
                                <td style="text-align: center;">
                                    {{ formatonumero(valorUnidad) }}
                                </td>

								<td class="textonew01" style="width:3.5cm;">.: Valor total: </td>
                                <td style="width: 11%;">
                                    <input type="text" v-model="formatonumero(valorTotal)" style="text-align: center;" readonly>
                                </td>

                                <td>
                                    <input type="button" class="btn btn-primary" value="Agregar Articulo" @click="agregaArticulo">
                                </td>
                            </tr>
                        </table>

                        <div class="subpantalla" style='height:45vh; width:100%; overflow-x:hidden; resize: vertical;'>
                            <table>        
                                <thead>
                                    <tr>
                                        <th width="6%" class="titulosnew00">Fecha compra</th>
                                        <th width="10%" class="titulosnew00">Articulo</th>
                                        <th width="25%" class="titulosnew00">Nombre articulo</th>
                                        <th width="10%" class="titulosnew00">Unidad medida</th>
										<th width="10%" class="titulosnew00">Responsable</th>
										<th width="10%" class="titulosnew00">Dependencia</th>
                                        <th width="10%"class="titulosnew00">Cantidad</th>
                                        <th width="10%" class="titulosnew00">Valor unitario</th>
                                        <th width="10%" class="titulosnew00">Valor total</th>
                                        <th width="6%" class="titulosnew00">Eliminar</th>
                                    </tr>
                                </thead>

                                <tbody>
                                    <tr v-for="(articulo,index) in listaArticulos" v-bind:class="(index % 2 ? 'saludo1a' : 'saludo2')" style="font: 100% sans-serif;">
                                        <td style="text-align:center; width:6%;">{{ articulo[0] }}</td>
                                        <td style="text-align:center; width:10%;">{{ articulo[1] }}</td>
                                        <td style="text-align:center; width:25%;">{{ articulo[2] }}</td>
                                        <td style="text-align:center; width:10%;">{{ articulo[3] }}</td>
                                        <td style="text-align:center; width:10%;">{{ articulo[4] }}</td>
                                        <td style="text-align:center; width:10%;">{{ articulo[5] }}</td>
										<td style="text-align:center; width:10%;">{{ articulo[6] }}</td>
                                        <td style="text-align:center; width:10%;">{{ formatonumero(articulo[7]) }}</td>
                                        <td style="text-align:center; width:10%;">{{ formatonumero(articulo[8]) }}</td>
                                        <td style="text-align:center; width:6%;"><img src="imagenes/eliminar.png" width="40px" height="40px" @click="eliminarArticuloAgregado(articulo)"></td>
                                    </tr>
                                    <tr>
                                        <td class="titulosnew00" style="text-align:right;" colspan="8">Total: </td>
                                        <td style="text-align:center;">{{ formatonumero(totalValores) }}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>

					<div id="cargando" v-if="loading" class="loading">
						<span>Cargando...</span>
					</div>

                    <div v-show="showArticulos">
						<transition name="modal">
							<div class="modal-mask">
								<div class="modal-wrapper">
									<div class="modal-container2">
										<table class="inicio ancho">
											<tr>
												<td class="titulos" colspan="2" >Articulos</td>
												<td class="cerrar" style="width:7%" @click="showArticulos = false">Cerrar</td>
											</tr>
                                            <tr>
                                                <td class="textonew01" style="text-align:center;">Articulo:</td>
                                                <td>
                                                    <input type="text" v-model="searchArticulo" @keyup="filtroArticulos" placeholder="Buscar por código de articulo o nombre" style="width: 100%;">
                                                </td>
                                            </tr>
										</table>
										<table class='tablamv'>
											<thead>
												<tr style="text-align:center;">
													<th class="titulosnew00" style="width: 20%;">Código</th>
													<th class="titulosnew00">Nombre</th>
													<th class="titulosnew00" style="width: 20%;">Unidad</th>
                                                    <th style="width: 1%;"></th>
												</tr>
											</thead>
											<tbody>
												<tr v-for="(articulo,index) in articulos" v-bind:class="index % 2 ? 'saludo1a' : 'saludo2'" v-on:click="seleccionarArticulo(articulo[0],articulo[1],articulo[2])">
													<td style="width:20%; text-align:center;"> {{ articulo[0] }} </td>
													<td style="text-align:center;"> {{ articulo[1] }} </td>
													<td style="width:20%; text-align:center;"> {{ articulo[2] }} </td>
												</tr>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</transition>
					</div> 
                    
                    <div v-show="showModalResponsables">
						<transition name="modal">
							<div class="modal-mask">
								<div class="modal-wrapper">
									<div class="modal-container">
										<table class="inicio ancho">
											<tr>
												<td class="titulos" colspan="2" >Listado terceros</td>
												<td class="cerrar" style="width:7%" @click="showModalResponsables = false">Cerrar</td>
											</tr>

                                            <tr>
                                                <td class="textonew01" style="text-align:center;">Cedula:</td>
                                                <td>
                                                    <input type="text" v-model="searchCedula" v-on:keyup="filtroCedulaResponsable" placeholder="Buscar por número de cedula" style="width: 100%;">
                                                </td>
                                            </tr>
										</table>
										<table class='tablamv'>
											<thead>
												<tr style="text-align:center;">
													<th class="titulosnew00" style="width: 20%;">Documento</th>
													<th class="titulosnew00">Nombre o razón social</th>
                                                    <th style="width: 1%;"></th>
												</tr>
											</thead>
											<tbody>
												<tr v-for="(tercero,index) in terceros" v-bind:class="index % 2 ? 'saludo1a' : 'saludo2'" v-on:click="seleccionaResponsable(tercero[0], tercero[1])">
													<td style="width:20%; text-align:center;"> {{ tercero[0] }} </td>
													<td style="text-align:left;"> {{ tercero[1] }} </td>
												</tr>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</transition>
					</div>  

				</article>
			</section>
		</form>

		<script src="node_modules/read-excel-file/bundle/read-excel-file.min.js"></script>
		<script src="node_modules/xlsx/dist/xlsx.full.min.js"></script>
        <script src="Librerias/vue/vue.min.js"></script>
		<script src="Librerias/vue/axios.min.js"></script>
		<script type="module" src="almacen/inventarioExtracontable/crear/inve-crearInventarioExtracontable.js"></script>
        
	</body>
</html>