<?php

    header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require 'comun.inc';
	require 'funciones.inc';
	session_start();
    if(empty($_SESSION)){
        header("location: index.php");
    }
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html lang=es>
	<head>
    <meta charset="utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1"/>
		<meta name="viewport" content="user-scalable=no">
		<title>:: IDEAL 10 - Tesorería</title>
		<link href="favicon.ico" rel="shortcut icon"/>
		<link href="css/css2z.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
        <link href="css/tabs.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
        <link href="css/style.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" href="sweetalert2/dist/sweetalert2.min.css">
        <script type="text/javascript" src="css/programas.js"></script>
        <script src="sweetalert2/dist/sweetalert2.min.js"></script>
		<script src="vue/vue.min.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script src="css/calendario.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
	</head>
	<body>
		<header>
			<table>
				<tr><script>barra_imagenes("teso");</script><?php cuadro_titulos();?></tr>
			</table>
		</header>

		<form name="form2" method="post" action="">
			<section id="myapp" v-cloak >
                <div class="loading-container" v-show="isLoading" >
                    <p class="text-loading"data-loading="IDEAL 10">IDEAL 10</p>
                </div>
				<nav>
					<table>
						<tr><?php menu_desplegable("teso");?></tr>
					</table>
                    <div class="bg-white group-btn p-1" id="newNavStyle">
                        <button type="button" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center" @click="save()">
                            <span>Guardar</span>
                            <svg xmlns="http://www.w3.org/2000/svg"  viewBox="0 -960 960 960" ><path d="M840-680v480q0 33-23.5 56.5T760-120H200q-33 0-56.5-23.5T120-200v-560q0-33 23.5-56.5T200-840h480l160 160Zm-80 34L646-760H200v560h560v-446ZM480-240q50 0 85-35t35-85q0-50-35-85t-85-35q-50 0-85 35t-35 85q0 50 35 85t85 35ZM240-560h360v-160H240v160Zm-40-86v446-560 114Z"/></svg>
                        </button>
                        <button type="button" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center" @click="window.location.href='teso-abonosPredialBuscar'">
                            <span>Buscar</span>
                            <svg class="fill-black group-hover:fill-white w-5 h-5" xmlns="http://www.w3.org/2000/svg"  viewBox="0 -960 960 960" ><path d="M784-120 532-372q-30 24-69 38t-83 14q-109 0-184.5-75.5T120-580q0-109 75.5-184.5T380-840q109 0 184.5 75.5T640-580q0 44-14 83t-38 69l252 252-56 56ZM380-400q75 0 127.5-52.5T560-580q0-75-52.5-127.5T380-760q-75 0-127.5 52.5T200-580q0 75 52.5 127.5T380-400Z"/></svg>
                        </button>
                        <button type="button" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center" @click="mypop=window.open('teso-principal.php','',''); mypop.focus();">
                            <span>Nueva ventana</span>
                            <svg class="fill-black group-hover:fill-white w-5 h-5" xmlns="http://www.w3.org/2000/svg"  viewBox="0 -960 960 960" ><path d="M320-120v-80h80v-80H160q-33 0-56.5-23.5T80-360v-400q0-33 23.5-56.5T160-840h640q33 0 56.5 23.5T880-760v400q0 33-23.5 56.5T800-280H560v80h80v80H320ZM160-360h640v-400H160v400Zm0 0v-400 400Z"/></svg>
                        </button>
                        <button type="button" class="btn btn-success d-flex justify-between align-items-center" @click="window.location.href='teso-abonosPredialBuscar'">
                            <span>Atrás</span>
                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960"><path d="M760-200v-160q0-50-35-85t-85-35H273l144 144-57 56-240-240 240-240 57 56-144 144h367q83 0 141.5 58.5T840-360v160h-80Z"/></svg>
                        </button>
                    </div>
				</nav>
				<article>
                     <div  class="bg-white">
                         <div>
                            <div class="d-flex w-50">
                                <div class="form-control d-flex flex-row">
                                    <label class="form-label w-25" for="labelSelectName2">Tipo de movimiento:</label>
                                    <select id="labelSelectName2" v-model="selectMovimiento">
                                        <option value="201" selected>Crear documento</option>
                                        <option value="401">Reversar documento</option>
                                    </select>
                                </div>
                            </div>
                            <div v-if="selectMovimiento == '201'">
                                <div>
                                    <h2 class="titulos m-0">Abono Acuerdo Predial Por Cuotas</h2>
                                    <p class="m-0 ms-2">Todos los campos con (<span class="text-danger fw-bolder">*</span>) son obligatorios</p>
                                    <div class="d-flex">
                                        <div class="form-control">
                                            <label class="form-label">Fecha de abono:</label>
                                            <input type="date"  v-model="txtFecha">
                                        </div>
                                        <div class="form-control">
                                            <label class="form-label">No. Acuerdo <span class="text-danger fw-bolder">*</span>:</label>
                                            <input type="text" class="colordobleclik"  v-model="objAcuerdo.id" @change="search('cod_acuerdo')" @dblclick="isModal=true">
                                        </div>
                                        <div class="form-control">
                                            <label class="form-label" for="labelSelectName2">Código catastral</label>
                                            <input type="text" v-model="objAcuerdo.codigo" disabled readonly>
                                        </div>
                                        <div class="form-control">
                                            <label class="form-label" for="labelSelectName2">Documento</label>
                                            <input type="text" v-model="objAcuerdo.documento" disabled readonly>
                                        </div>
                                    </div>
                                    <div class="d-flex">
                                        <div class="form-control">
                                            <label class="form-label">Concepto de abono <span class="text-danger fw-bolder">*</span>:</label>
                                            <input type="text"  v-model="objAcuerdo.concepto">
                                        </div>
                                    </div>
                                    <div class="d-flex">
                                        <div class="form-control">
                                            <label class="form-label" for="labelSelectName2">Recaudado en:</label>
                                            <select id="labelSelectName2" v-model="selectRecaudado">
                                                <option value="banco">Banco</option>
                                                <option value="caja">Caja</option>
                                            </select>
                                        </div>
                                        <div class="form-control" v-show="selectRecaudado == 'banco'">
                                            <label class="form-label">Cuenta <span class="text-danger fw-bolder">*</span>:</label>
                                            <div class="d-flex">
                                                <input type="text" class="colordobleclik w-25" v-model="objCuenta.banco" @change="search('cod_cuenta')" @dblclick = "isModalCuenta=true">
                                                <input type="text"v-model="objCuenta.razonsocial" disabled>
                                            </div>
                                        </div>
                                        <div class="form-control justify-between">
                                            <label class="form-label" for="labelSelectName2"></label>
                                            <button type="button" class="btn btn-primary w-25" @click="isModalCuotas=true" v-show="objAcuerdo.id != ''">Pago de cuotas</button>
                                        </div>
                                    </div>
                                </div>
                                <div>
                                    <h2 class="titulos m-0">Detalle</h2>
                                    <div class="table-responsive">
                                        <table  class="table fw-normal">
                                            <thead>
                                                <tr>
                                                    <th>Vigencia</th>
                                                    <th>Tasa x mil</th>
                                                    <th>Valor predial</th>
                                                    <th>Descuento incentivo</th>
                                                    <th>Recaudo predial</th>
                                                    <th>Intereses predial</th>
                                                    <th>Descuento intereses predial</th>
                                                    <th>Bomberil</th>
                                                    <th>Intereses bomberil</th>
                                                    <th>Ambiental</th>
                                                    <th>Intereses ambiental</th>
                                                    <th>Alumbrado</th>
                                                    <th>Liquidación</th>
                                                    <th>Dias de mora</th>
                                                    <th class="text-center">Estado</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr v-for="(data,index) in objAcuerdo.detalle" :key="index">
                                                    <td>{{data.vigencia}}</td>
                                                    <td>{{data.tasa_por_mil}}</td>
                                                    <td>{{formatNumero(data.predial+data.predial_descuento)}}</td>
                                                    <td>{{formatNumero(data.predial_descuento)}}</td>
                                                    <td>{{formatNumero(data.predial)}}</td>
                                                    <td>{{formatNumero(data.predial_intereses)}}</td>
                                                    <td>{{formatNumero(data.predial_descuento_intereses)}}</td>
                                                    <td>{{formatNumero(data.bomberil)}}</td>
                                                    <td>{{formatNumero(data.bomberil_intereses)}}</td>
                                                    <td>{{formatNumero(data.ambiental)}}</td>
                                                    <td>{{formatNumero(data.ambiental_intereses)}}</td>
                                                    <td>{{formatNumero(data.alumbrado)}}</td>
                                                    <td>{{formatNumero(data.total_liquidacion)}}</td>
                                                    <td>{{data.dias_mora}}</td>
                                                    <td class="text-center" class="text-center"><span :class="[data.estado =='P' ? 'badge-success' : 'badge-warning']"class="badge">{{ data.estado == "P" ? "Pagado" : "Pendiente"}}</span></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div v-if="selectMovimiento == '401'">
                                <div>
                                    <h2 class="titulos m-0">Documento a reversar</h2>
                                    <p class="m-0 ms-2">Todos los campos con (<span class="text-danger fw-bolder">*</span>) son obligatorios</p>
                                    <div class="d-flex">
                                        <div class="form-control w-25">
                                            <label class="form-label">Fecha de abono:</label>
                                            <input type="date"  v-model="txtFechaRev">
                                        </div>
                                        <div class="form-control w-25">
                                            <label class="form-label">No. Abono <span class="text-danger fw-bolder">*</span>:</label>
                                            <input type="text" class="colordobleclik"  v-model="objAbono.idabono" @change="search('cod_abono')" @dblclick="isModalAbono=true">
                                        </div>
                                        <div class="form-control">
                                            <label class="form-label" for="">Descripción <span class="text-danger fw-bolder">*</span></label>
                                            <input type="text" v-model="txtDescripcion">
                                        </div>
                                    </div>
                                </div>
                            </div>
                         </div>
                    </div>
                    <!-- MODALES -->
                    <div v-show="isModal" class="modal">
                        <div class="modal-dialog modal-lg" >
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title">Buscar acuerdos</h5>
                                    <button type="button" @click="isModal=false;" class="btn btn-close"><div></div><div></div></button>
                                </div>
                                <div class="modal-body">
                                    <div class="d-flex flex-column">
                                        <div class="form-control m-0 mb-3">
                                            <input type="search" v-model="txtSearch" @keyup="search('modal')" placeholder="Buscar por código catastral, documento o número de acuerdo">
                                        </div>
                                        <div class="form-control m-0 p-2">
                                            <label class="form-label" for="labelInputName">Resultados: <span class="fw-bold">{{txtResultados}}</span></label>
                                        </div>
                                    </div>
                                    <div class="overflow-auto max-vh-50 overflow-x-hidden " >
                                        <table class="table table-hover fw-normal p-2">
                                            <thead>
                                                <tr>
                                                    <th class="text-center">No. acuerdo</th>
                                                    <th>Código catastral</th>
                                                    <th>Documento</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr v-for="(data,index) in arrAcuerdosCopy" :key="index" @dblclick="selectItem('modal',data)">
                                                    <td class="text-center">{{data.id}}</td>
                                                    <td>{{data.codigo}}</td>
                                                    <td>{{data.documento}}</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div v-show="isModalCuenta" class="modal">
                        <div class="modal-dialog modal-lg" >
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title">Buscar cuentas bancarias</h5>
                                    <button type="button" @click="isModalCuenta=false;" class="btn btn-close"><div></div><div></div></button>
                                </div>
                                <div class="modal-body">
                                    <div class="d-flex flex-column">
                                        <div class="form-control m-0 mb-3">
                                            <input type="search" v-model="txtSearchCuentas" @keyup="search('modal_cuenta')" placeholder="Buscar">
                                        </div>
                                        <div class="form-control m-0 p-2">
                                            <label class="form-label" for="labelInputName">Resultados: <span class="fw-bold">{{txtResultadosCuentas}}</span></label>
                                        </div>
                                    </div>
                                    <div class="overflow-auto max-vh-50 overflow-x-hidden " >
                                        <table class="table table-hover fw-normal">
                                            <thead>
                                                <tr>
                                                    <th>Nombre</th>
                                                    <th>Cuenta</th>
                                                    <th>Cuenta bancaria</th>
                                                    <th>Tipo</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr @dblclick="selectItem('modal_cuenta',data)" v-for="(data,index) in arrCuentasCopy" :key="index">
                                                    <td>{{data.razonsocial}}</td>
                                                    <td>{{data.cuenta}}</td>
                                                    <td>{{data.banco}}</td>
                                                    <td>{{data.tipo}}</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div v-show="isModalCuotas" class="modal">
                        <div class="modal-dialog modal-lg" >
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title">Pago de cuotas</h5>
                                    <button type="button" @click="isModalCuotas=false;" class="btn btn-close"><div></div><div></div></button>
                                </div>
                                <div class="modal-body">
                                    <div class="form-control">
                                        <label class="form-label" for="labelSelectName2">Deuda actual:</label>
                                        <p class="m-0">{{formatNumero(objAcuerdo.debe)}}</p>
                                    </div>
                                    <div class="overflow-auto max-vh-50 overflow-x-hidden " >
                                        <table class="table fw-normal">
                                            <thead>
                                                <th class="d-flex justify-center">
                                                    <div class="d-flex align-items-center ">
                                                        <label for="labelCheckAll" class="form-switch">
                                                            <input type="checkbox" id="labelCheckAll" v-model="isCheckAll" checked @change="changeAll()">
                                                            <span></span>
                                                        </label>
                                                    </div>
                                                </th>
                                                <th class="text-center">No. Cuota</th>
                                                <th class="text-center">Fecha</th>
                                                <th class="text-right">Valor</th>
                                                <th class="text-right">Saldo cuota</th>
                                                <th class="text-center">Estado</th>
                                            </thead>
                                            <tbody>
                                                <tr v-for="(data,index) in objAcuerdo.cuotas" :key="index">
                                                    <td class="d-flex justify-center">
                                                        <div class="d-flex align-items-center" v-if="!data.is_pago">
                                                            <label :for="'labelCheckName'+index" class="form-switch">
                                                                <input type="checkbox" :id="'labelCheckName'+index" @change="changeStatus(index)" :checked="data.is_checked">
                                                                <span></span>
                                                            </label>
                                                        </div>

                                                    </td>
                                                    <td class="text-center">{{data.cuota}}</td>
                                                    <td class="text-center">{{data.fecha}}</td>
                                                    <td class="text-right">{{formatNumero(data.valor)}}</td>
                                                    <td class="text-right">{{formatNumero(data.saldo)}}</td>
                                                    <td class="text-center" class="text-center"><span :class="[data.estado =='P' ? 'badge-success' : 'badge-warning']"class="badge">{{ data.estado == "P" ? "Pagado" : "Pendiente"}}</span></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="d-flex">
                                        <div class="form-control">
                                            <label class="form-label" for="labelSelectName2">Total a pagar</label>
                                            <p class="m-0">{{formatNumero(txtPagoTotal)}}</p>
                                        </div>
                                        <div class="form-control" v-if="objAcuerdo.saldo!=0">
                                            <label class="form-label" for="labelSelectName2">Saldo anterior</label>
                                            <p class="m-0">{{formatNumero(objAcuerdo.saldo)}}</p>
                                        </div>
                                    </div>
                                    <div class="d-flex justify-end">
                                        <button type="button" class="btn btn-primary" @click="isModalCuotas=false;">Cerrar</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div v-show="isModalAbono" class="modal">
                        <div class="modal-dialog modal-lg" >
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title">Buscar abonos</h5>
                                    <button type="button" @click="isModalAbono=false;" class="btn btn-close"><div></div><div></div></button>
                                </div>
                                <div class="modal-body">
                                    <div class="d-flex flex-column">
                                        <div class="form-control m-0 mb-3">
                                            <input type="search" v-model="txtSearchAbono" @keyup="search('modal_abono')" placeholder="Buscar por abono, acuerdo o tercero">
                                        </div>
                                        <div class="form-control m-0 p-2">
                                            <label class="form-label" for="labelInputName">Resultados: <span class="fw-bold">{{txtResultadosAbono}}</span></label>
                                        </div>
                                    </div>
                                    <div class="overflow-auto max-vh-50 overflow-x-hidden " >
                                        <table class="table table-hover fw-normal p-2">
                                            <thead>
                                                <tr>
                                                    <th class="text-center">No. abono</th>
                                                    <th>No. Acuerdo</th>
                                                    <th>Concepto</th>
                                                    <th>Documento</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr v-for="(data,index) in arrAbonosCopy" :key="index" @dblclick="selectItem('modal_abono',data)">
                                                    <td class="text-center">{{data.idabono}}</td>
                                                    <td>{{data.idacuerdo}}</td>
                                                    <td>{{data.concepto}}</td>
                                                    <td>{{data.documento}}</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
				</article>
			</section>
		</form>

		<script src="node_modules/read-excel-file/bundle/read-excel-file.min.js"></script>
		<script src="node_modules/xlsx/dist/xlsx.full.min.js"></script>
        <script src="Librerias/vue/vue.min.js"></script>
		<script src="Librerias/vue/axios.min.js"></script>
		<script type="module" src="tesoreria/predial_abonos/crear/teso-abonosPredialCrear.js?<?= date('d_m_Y_h_i_s');?>"></script>

	</body>
</html>
