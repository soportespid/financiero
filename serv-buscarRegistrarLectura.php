<?php
header("Cache-control: no-cache, no-store, must-revalidate");
header("Content-Type: text/html;charset=iso-8859-1");
require "comun.inc";
require "funciones.inc";
require "funcionesSP.inc.php";
session_start();
$linkbd = conectar_v7();
cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>:: IDEAL 10 - Servicios P&uacute;blicos</title>
    <link href="css/css2.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
    <link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
    <link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
    <link href="css/cssSP.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
    <script type="text/javascript" src="css/calendario.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
    <script type="text/javascript" src="css/programas.js"></script>

    <script>
        function verUltimaPos(idcta, corte, filas, filtro)
			{
				var scrtop=$('#divdet').scrollTop();
				var altura=$('#divdet').height();
				var numpag=$('#nummul').val();
				var limreg=$('#numres').val();
				if((numpag<=0)||(numpag=="")){numpag=0;}
				if((limreg==0)||(limreg=="")){limreg=10;}
				numpag++;
				location.href="serv-verFacturacion.php?idban="+idcta+"&corte="+corte+"&scrtop="+scrtop+"&totreg="+filas+"&altura="+altura+"&numpag="+numpag+"&limreg="+limreg+"&filtro="+filtro;
			}
			function despliegamodalm(_valor,_tip,mensa,pregunta)
			{
				document.getElementById("bgventanamodalm").style.visibility=_valor;
				if(_valor=="hidden"){document.getElementById('ventanam').src="";}
				else
				{
					switch(_tip)
					{
						case "1":	document.getElementById('ventanam').src="ventana-mensaje1.php?titulos="+mensa;break;
						case "2":	document.getElementById('ventanam').src="ventana-mensaje3.php?titulos="+mensa;break;
						case "3":	document.getElementById('ventanam').src="ventana-mensaje2.php?titulos="+mensa;break;
						case "4":	document.getElementById('ventanam').src="ventana-consulta2.php?titulos="+mensa+"&idresp="+pregunta;break;	
					}
				}
			}
			function funcionmensaje(){}
    </script>

    <?php 
        titlepag();
        $scrtop= @ $_GET['scrtop'];
        if($scrtop=="") $scrtop=0;
        echo"<script>
            window.onload=function(){
                $('#divdet').scrollTop(".$scrtop.")
            }
        </script>";
        $gidcta=@ $_GET['idcta'];
        if(isset($_GET['filtro']))
        $_POST['nombre']=$_GET['filtro'];
    ?>
</head>

<body>
    <IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
    <span id="todastablas2"></span>
    <table>
        <tr><script>barra_imagenes("serv");</script><?php cuadro_titulos();?></tr>	 
        <tr><?php menu_desplegable("serv");?></tr>
        <tr>
            <td colspan="3" class="cinta">
                <a class="mgbt"><img src="imagenes/add2.png"/></a>

                <a class="mgbt"><img src="imagenes/guardad.png" title="Guardar"/></a>

                <a href="serv-buscarFacturacion.php" class="mgbt"><img src="imagenes/busca.png" title="Buscar"/></a>

                <a onclick="mypop=window.open('plan-agenda.php','','');mypop.focus()" class="mgbt"><img src="imagenes/agenda1.png" title="Agenda" /></a>

                <a onclick="mypop=window.open('serv-principal.php','','');mypop.focus();" class="mgbt"><img src="imagenes/nv.png" title="Nueva ventana"></a>

                <a onclick="mypop=window.open('<?php echo $url2; ?>','','');mypop.focus();" class="mgbt"><img src="imagenes/duplicar_pantalla.png" title="Duplicar pesta&ntilde;a"></a>

                <a href="serv-menuFacturacion01.php" class="mgbt"><img src="imagenes/iratras.png" title="Atr&aacute;s"></a>
            </td>
        </tr>
    </table>

    <?php
        if(@ $_POST['oculto']=="")
        {
            $_POST['numres']=10;$_POST['numpos']=0;$_POST['nummul']=0;
        }
        if(@ $_GET['numpag']!="")
        {
            if(@ $_POST['oculto']!=2)
            {
                $_POST['numres']=$_GET['limreg'];
                $_POST['numpos']=$_GET['limreg']*($_GET['numpag']-1);
                $_POST['nummul']=$_GET['numpag']-1;
            }
        }
        else
        {
            if(@ $_POST['nummul']=="")
            {
                $_POST['numres']=10;
                $_POST['numpos']=0;
                $_POST['nummul']=0;
            }
        }
    ?>

    <div id="bgventanamodalm" class="bgventanamodalm">
        <div id="ventanamodalm" class="ventanamodalm">
            <IFRAME src="" name="ventanam" marginWidth=0 marginHeight=0 frameBorder=0 id="ventanam" frameSpacing=0 style=" width:700px; height:130px; top:200; overflow:hidden;"></IFRAME>
        </div>
    </div>

    <form name="form2" method="post" action="">
        <input type="hidden" name="numres" id="numres" value="<?php echo @ $_POST['numres'];?>"/>
        <input type="hidden" name="numpos" id="numpos" value="<?php echo @ $_POST['numpos'];?>"/>
        <input type="hidden" name="nummul" id="nummul" value="<?php echo @ $_POST['nummul'];?>"/>
        <input type="hidden" name="oculto" id="oculto" value="1">
        
        <table class="inicio">
            <tr>
                <td class="titulos" colspan="5">:: Buscar Registro de lecturas por Corte</td>
                <td class="cerrar" style="width:7%" onClick="location.href='serv-principal.php'">Cerrar</td>
            </tr>

            <tr>
                <td class="tamano01" style='width:4cm;'>:: Corte:</td>

                <td>
                    <input type="search" name="corte" id="corte" value="<?php echo @ $_POST['corte'];?>" style='width:100%;height:30px;'/>
                </td>
            </tr>
        </table>

        <div class="subpantallac5" style="height:69%; width:99.6%; margin-top:0px; overflow-x:hidden" id="divdet">
        <?php
            if (@$_POST['corte'] != '')
            {
                $crit1 = "WHERE concat_ws('', D.id_cliente, C.cod_usuario) LIKE '%".$_POST['codigoUsuario']."%' AND numero_facturacion = '".$_POST['codigoFactura']."' ";
            }
            else 
            {
                $crit1 = "";
            }

            $sqlr = "SELECT DISTINCT corte, fecha FROM srvlectura";
            $resp = mysqli_query($linkbd,$sqlr);
            $_POST['numtop'] = mysqli_num_rows($resp);
            $nuncilumnas = ceil($_POST['numtop']/$_POST['numres']);
            $cond2="";

            if (@ $_POST['numres']!="-1")
            {
                $cond2="LIMIT ".$_POST['numpos'].", ".$_POST['numres']; 
            }

            $sqlr = "SELECT D.id, D.id_cliente, D.id_corte, D.numero_facturacion, D.estado_pago, C.id_tercero, C.cod_usuario FROM srvcortes_detalle AS D INNER JOIN srvclientes AS C ON D.id_cliente = C.id $crit1 ORDER BY numero_facturacion DESC $cond2";
            $resp = mysqli_query($linkbd,$sqlr);
            $con = 1;
            $numcontrol=$_POST['nummul']+1;

            if(($nuncilumnas==$numcontrol)||(@ $_POST['numres']=="-1"))
            {
                $imagenforward="<img src='imagenes/forward02.png' style='width:17px;cursor:default;'>";
                $imagensforward="<img src='imagenes/skip_forward02.png' style='width:16px;cursor:default;' >";
            }
            else 
            {
                $imagenforward="<img src='imagenes/forward01.png' style='width:17px;cursor:pointer;' title='Siguiente' onClick='numsiguiente()'>";
                $imagensforward="<img src='imagenes/skip_forward01.png' style='width:16px;cursor:pointer;' title='Fin' onClick='saltocol(\"$nuncilumnas\")'>";
            }
            if((@ $_POST['numpos']==0)||(@ $_POST['numres']=="-1"))
            {
                $imagenback="<img src='imagenes/back02.png' style='width:17px;cursor:default;'>";
                $imagensback="<img src='imagenes/skip_back02.png' style='width:16px;cursor:default;'>";
            }
            else
            {
                $imagenback="<img src='imagenes/back01.png' style='width:17px;cursor:pointer;' title='Anterior' onClick='numanterior();'>";
                $imagensback="<img src='imagenes/skip_back01.png' style='width:16px;cursor:pointer;' title='Inicio' onClick='saltocol(\"1\")'>";
            }
            $con=1;
            echo "
                <table class='inicio'>
                    <tr>
                        <td colspan='8' class='titulos'>.: Resultados Busqueda:</td>
                        <td class='submenu'>
                        <select name='renumres' id='renumres' onChange='cambionum();' style='width:100%'>
                            <option value='10'"; if (@$_POST['renumres']=='10'){echo 'selected';} echo ">10</option>
                            <option value='20'"; if (@$_POST['renumres']=='20'){echo 'selected';} echo ">20</option>
                            <option value='30'"; if (@$_POST['renumres']=='30'){echo 'selected';} echo ">30</option>
                            <option value='50'"; if (@$_POST['renumres']=='50'){echo 'selected';} echo ">50</option>
                            <option value='100'"; if (@$_POST['renumres']=='100'){echo 'selected';} echo ">100</option>
                            <option value='-1'"; if (@$_POST['renumres']=='-1'){echo 'selected';} echo ">Todos</option>
                        </select>
                    </td>
                    </tr>
                    <tr><td colspan='9'>Facturas Encontradas: ".$_POST['numtop']."</td></tr>
                    <tr>
                        <td class='titulos2' style='width:5%;height: 30px;'>N&deg; Corte</td>
                        <td class='titulos2' style='width:5%;'>C&oacute;digo Factura</td>
                        <td class='titulos2' style='width:5%;'>ID Cliente</td>
                        <td class='titulos2' style='width:10%;'>Cod usuario</td>
                        <td class='titulos2'>Nombre Usuario</td>
                        <td class='titulos2' style='width:10%;'>N&deg; Documento</td>
                        <td class='titulos2' style='width:15%;'>Dirrecci&oacute;n</td>
                        <td class='titulos2' style='width:5%;'>Total Pago</td>
                        <td class='titulos2' style='width:7%;'>Estado</td>
                    </tr>";
                    $iter='saludo1a';
                    $iter2='saludo2';
                    $filas=1;
                    while ($row = mysqli_fetch_row($resp))
                    {
                        $sqlTerceros = "SELECT nombre1,nombre2,apellido1,apellido2,razonsocial,cedulanit FROM terceros WHERE id_tercero = '$row[5]' ";
                        $respTerceros = mysqli_query($linkbd,$sqlTerceros);
                        $rowTerceros = mysqli_fetch_row($respTerceros);

                        if($rowTerceros[4] != '')
                        {
                            $nombre = $rowTerceros[4];
                        }
                        else
                        {
                            $nombre = $rowTerceros[0].' '.$rowTerceros[1].' '.$rowTerceros[2].' '.$rowTerceros[3];
                        }

                        $sqlDireccion = "SELECT direccion FROM srvdireccion_cliente WHERE id_cliente = '$row[1]' ";
                        $respDireccion = mysqli_query($linkbd,$sqlDireccion);
                        $rowDireccion = mysqli_fetch_row($respDireccion);

                        $sqlDetallesFacturacion = "SELECT SUM(credito), SUM(debito) FROM srvdetalles_facturacion WHERE id_cliente = '$row[1]' AND corte = '$row[2]' AND tipo_movimiento = '101'";
                        $respDetallesFacturacion = mysqli_query($linkbd,$sqlDetallesFacturacion);
                        $rowDetallesFacturacion = mysqli_fetch_row($respDetallesFacturacion);
                        
                        $totalFactura = $rowDetallesFacturacion[0] - $rowDetallesFacturacion[1];

                        switch ($row[4]) 
                        {
                            case 'S': 	$imgsem="<img src='imagenes/sema_amarilloON.jpg' title='Pago Pendiente' style='width:20px;'/>";
                                        break;
                            case 'P':	$imgsem="<img src='imagenes/sema_verdeON.jpg' title='Pago Realizado' style='width:20px;'/>";
                                        break;
                            case 'V':	$imgsem="<img src='imagenes/sema_rojoON.jpg' title='Pago Vencido' style='width:20px;'/>";
                                        break;
                            case 'R':	$imgsem="<img src='imagenes/sema_amarilloOFF.jpg' title='Factura reversada' style='width:20px;'/>";
                                        break;
                            case 'A':	$imgsem="<img src='imagenes/sema_azulON.jpg' title='Acuerdo de Pago' style='width:20px;'/>"
                                        ;break;
                            default:	$imgsem="";
                        }

                        if($gidcta!="")
                        {
                            if($gidcta==$row[0])
                            {
                                $estilo='background-color:yellow';
                            }
                            else
                            {
                                $estilo="";
                            }
                        }
                        else
                        {
                            $estilo="";
                        }

                        $idcta  = "'$row[0]'";
                        $corte  = "'$row[2]'";
                        $numfil = "'$filas'";
                        $filtro = "'".@ $_POST['nombre']."'";

                        echo"
                        <tr class='$iter' onDblClick=\"verUltimaPos($idcta, $corte, $numfil, $filtro)\" style='text-transform:uppercase; $estilo'>
                            <td class='icoop' style='height: 35px;text-align:center;'>$row[2]</td>
                            <td class='icoop' style='text-align:center;'>$row[3]</td>
                            <td class='icoop' style='text-align:center;'>$row[1]</td>
                            <td class='icoop' style='text-align:center;'>$row[6]</td>
                            <td class='icoop'>$nombre</td>
                            <td class='icoop' style='text-align:center;'>$rowTerceros[5]</td>
                            <td class='icoop' style='text-align:center;'>$rowDireccion[0]</td>
                            <td class='icoop' style='text-align:center;'>$totalFactura</td>
                            <td style='text-align:center;'>$imgsem</td>
                        </tr>";

                        $aux = $iter;
                        $iter = $iter2;
                        $iter2 = $aux;
                        $filas++;
                    }
                    if (@ $_POST['numtop']==0)
                    {
                        echo "
                        <table class='inicio'>
                            <tr>
                                <td class='saludo1' style='text-align:center;width:100%'><img src='imagenes\alert.png' style='width:25px'>No hay coincidencias en la b&uacute;squeda<img src='imagenes\alert.png' style='width:25px'></td>
                            </tr>
                        </table>";
                    }
                    echo"
                </table>
                <table class='inicio'>
                    <tr>
                        <td style='text-align:center;'>
                            <a>$imagensback</a>&nbsp;
                            <a>$imagenback</a>&nbsp;&nbsp;";
            if($nuncilumnas<=9){$numfin=$nuncilumnas;}
            else{$numfin=9;}
            for($xx = 1; $xx <= $numfin; $xx++)
            {
                if($numcontrol<=9){$numx=$xx;}
                else{$numx=$xx+($numcontrol-9);}
                if($numcontrol==$numx){echo"<a onClick='saltocol(\"$numx\")'; style='color:#24D915;cursor:pointer;'> $numx </a>";}
                else {echo"<a onClick='saltocol(\"$numx\")'; style='color:#000000;cursor:pointer;'> $numx </a>";}
            }
            echo"			&nbsp;&nbsp;<a>$imagenforward</a>
                            &nbsp;<a>$imagensforward</a>
                        </td>
                    </tr>
                </table>";	
        ?>
        <input type="hidden" name="ocules" id="ocules" value="<?php echo @ $_POST['ocules'];?>">
        <input type="hidden" name="actdes" id="actdes" value="<?php echo @ $_POST['actdes'];?>">
        <input type="hidden" name="numtop" id="numtop" value="<?php echo @ $_POST['numtop'];?>" />
    </form>
    
    <div id="bgventanamodal2">
        <div id="ventanamodal2">
            <IFRAME  src="" name="buster" marginWidth=0 marginHeight=0 frameBorder=0 id="ventana2" frameSpacing=0 style="left:500px; width:900px; height:500px; top:200;"></IFRAME>
        </div>
    </div>
</body>
</html>